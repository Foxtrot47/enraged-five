//
// FileListBuilder.cpp
// 
// Copyright (C) 2005 Rockstar Games.  All Rights Reserved.
//

#include "FileListBuilder.h"

namespace rage
{
CFileListBuilder::CFileListBuilder(void)
{
}

CFileListBuilder::~CFileListBuilder(void)
{
}

ArrayList *CFileListBuilder::GetFileList(String *basePath, String *extension)
{
	ArrayList *list = new ArrayList();

	String *files __gc[] = Directory::GetFiles(basePath, String::Concat("*.", extension));
	String *directories __gc[] = Directory::GetDirectories(basePath);

	for(int i =0; i < files->Length; i++)
	{
		if(files[i]->ToUpper()->EndsWith(extension->ToUpper()))
		{
			list->Add(files[i]);
		}
	}

	for(int i = 0; i < directories->Length; i++)
	{
		list->AddRange(GetFileList(directories[i], extension));
	}

	return list;
}

ArrayList *CFileListBuilder::GetFileList(String *basePath, String *extension, assetManager *assetMgr)
{
	String *workingPath = assetMgr->GetWorkingPath("");

	ArrayList *list = new ArrayList();

	String *files __gc[] = Directory::GetFiles(basePath, String::Concat("*.", extension));
	String *directories __gc[] = Directory::GetDirectories(basePath);

	for(int i =0; i < files->Length; i++)
	{
		if(files[i]->ToUpper()->EndsWith(extension->ToUpper()))
		{
			String *asset = files[i]->ToUpper()->Remove(0, workingPath->Length);

			// make sure this file is actually an asset
			if (assetMgr->ExistsAsAsset(asset))
			{
				list->Add(files[i]);
			}
		}
	}

	for(int i = 0; i < directories->Length; i++)
	{
		list->AddRange(GetFileList(directories[i], extension, assetMgr));
	}

	return list;
}



}

