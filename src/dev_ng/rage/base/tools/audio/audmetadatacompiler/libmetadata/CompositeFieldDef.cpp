//
// CompositeFieldDef.cpp
// 
// Copyright (C) 2005 Rockstar Games.  All Rights Reserved.
//

#include "FieldDef.h"
#include "CompositeFieldDef.h"
#include "ErrorManager.h"

// for FindChildNode()
#include "SoundDef.h"

namespace rage
{
CCompositeFieldDef::CCompositeFieldDef(void) :
m_MaxOccurs(1),
m_FixedSize(false),
m_NativeType(false),
m_OverrideShouldBeAtEndOfStruct(false)
{
}

CCompositeFieldDef *CCompositeFieldDef::ParseCompositeFieldDef(XmlNode *node)
{
	CCompositeFieldDef *def = new CCompositeFieldDef();

	def->m_flags = NULL;
	def->m_fields = new ArrayList();
	def->m_type = FIELD_TYPE_COMPOSITE;
	def->m_NumType = NULL;

	if(node->Attributes->get_ItemOf(S"maxOccurs") != NULL)
	{
		def->m_MaxOccurs = System::Int32::Parse(node->Attributes->get_ItemOf(S"maxOccurs")->Value);
		if(def->m_MaxOccurs <= 255)
		{
			def->m_NumType = "u8";
		}
		else if(def->m_MaxOccurs <= 65535)
		{
			def->m_NumType = "u16";
		}
		else
		{
			def->m_NumType = "u32";
		}
	}

	if(node->Attributes->get_ItemOf(S"fixedSize") != NULL)
	{
		def->m_FixedSize = node->Attributes->get_ItemOf(S"fixedSize")->Value->Equals(S"yes");
	}
	if(node->Attributes->get_ItemOf(S"nativeType") != NULL)
	{
		def->m_NativeType = node->Attributes->get_ItemOf(S"nativeType")->Value->Equals(S"yes");
	}

	// now create field defs from all the child nodes

	for(int i = 0; i < node->ChildNodes->Count; i++)
	{
		CFieldDef *fdef = CFieldDef::ParseFieldDef(node->ChildNodes->Item(i));
		if(fdef != NULL)
		{
			if(fdef->getType() == FIELD_TYPE_BIT)
			{
				if(!def->m_flags)
				{
					def->m_flags = new ArrayList();
				}

				def->m_flags->Add(fdef);
			}
			else
			{
				if(fdef->getType() == FIELD_TYPE_COMPOSITE && def->ShouldBeAtEndOfStruct())
				{
					dynamic_cast<CCompositeFieldDef*>(fdef)->OverrideShouldBeAtEndOfStruct(true);
				}
				def->m_fields->Add(fdef);
			}
		}
	}

	if(def->m_flags)
	{
		const int count = def->m_flags->get_Count();
		if(count <= 8)
		{
			def->m_FlagWidth = 8;
		}
		else if(count <= 16)
		{
			def->m_FlagWidth = 16;
		}
		else if(count <= 32)
		{
			def->m_FlagWidth = 32;
		}
		else if(count <= 64)
		{
			def->m_FlagWidth = 64;
		}
		else
		{
			ERRORMGR->HandleError("Too many bit-fields in a single composite field (limit is 64)");
			def->m_FlagWidth = 64;
		}
	}
	else
		def->m_FlagWidth = 0;
	
	return def;
}

bool CCompositeFieldDef::Serialise(XmlNode *node, BinaryWriter *output, CObjectDef* objectDef, CObjectCompiler *cs)
{
	if(!ShouldBeAtEndOfStruct())
	{
		// we need to search across the entire object, not just the first matching node
		return Compile((node != NULL ? node->ParentNode : NULL), output, objectDef, cs);
	}
	else
	{
		// compilation is done during FinishCompilation
		return true;
	}
}

bool CCompositeFieldDef::FinishCompilation(XmlNode *node, BinaryWriter *output, CObjectDef* objectDef, CObjectCompiler *cs)
{
	// do nothing with ignored fields
	if(ShouldBeAtEndOfStruct())
	{
		return Compile(node, output, objectDef, cs);
	}
	else
	{
		return true;
	}
}

bool CCompositeFieldDef::Compile(XmlNode *node, BinaryWriter *output, CObjectDef* objectDef, CObjectCompiler *cs)
{
	ArrayList *items = new ArrayList();

	// gather all instances of this composite field
	// TODO: eventually I'll change this such that composite fields have a single root node and every
	//	instance is a child of that, but for now they sit at the parent root so we need to search for them.
	if(node != NULL)
	{
		for(int i = 0; i < node->ChildNodes->Count; i++)
		{
			if(getName()->Equals(node->ChildNodes->get_ItemOf(i)->Name))
			{
				items->Add(node->ChildNodes->get_ItemOf(i));
			}
		}
	}

	if(items->Count > m_MaxOccurs)
	{
		ERRORMGR->HandleError(String::Concat("Object field ", getName(), " exceeds maxOccurs in object type definition"));
		return false;
	}

	// now serialise this data

	// start with the numElements field if variable sized
	if(m_MaxOccurs > 1)
	{
		WriteConvertedValue(items->Count.ToString(), CFieldDef::GetFieldType(m_NumType), output, 0, objectDef->GetIsPacked(), cs->IsOutputBigEndian());
	}

	// then serialise each instance
	bool succeeded = true;
	for(int i = 0; i < items->Count; i++)
	{
		if(!SerialiseInstance(dynamic_cast<XmlNode*>(items->get_Item(i)), output, objectDef, cs))
		{
			succeeded = false;
		}
	}

	// pad to fixed size if required
	if(m_FixedSize || m_MaxOccurs == 1)
	{
		for(int i = items->Count; i < m_MaxOccurs; i++)
		{
			if(!SerialiseInstance(NULL, output, objectDef, cs))
			{
				succeeded = false;
			}
		}
	}

	return succeeded;
}
bool CCompositeFieldDef::SerialiseInstance(XmlNode *node, BinaryWriter *output, CObjectDef* objectDef, CObjectCompiler *cs)
{
	const unsigned int largestAlign = GetAlignment();
	
	//	For unpacked data, we need to make sure we end on our largest alignment boundary as well as start
	if (!objectDef->GetIsPacked())
	{
		CFieldDef::AlignOutputToField(output, largestAlign);
	}
	
	int numNodes = (node!=NULL ? node->ChildNodes->Count : 0);

	// flags first
	if(m_flags)
	{
		UInt64 flagVal = 0;
		
		if(node)
		{
			//int bitsToSkip = m_FlagWidth - m_flags->Count;
			for(int i = 0 ; i < m_flags->Count; i++)
			{
				CFieldDef *flag = dynamic_cast<CFieldDef*>(m_flags->get_Item(i));
				XmlNode *value = CUtility::FindChildNode(node, flag->getName());
				if(value)
				{
					if(value->InnerText->ToLower()->Equals("yes"))
					{
						flagVal |= (1i64 << i);
					}
				}
				else
				{
					if (flag->getDefaultValue()->ToLower()->Equals("yes"))
					{
						flagVal |= (1i64 << i);
					}
				}
			}
		}
		switch(m_FlagWidth)
		{
		case 64:
			output->Write(cs->IsOutputBigEndian() ? CUtility::SwapUInt64(flagVal) : flagVal);
			break;
		case 32:
			{
				UInt32 u32 = (UInt32)flagVal;
				output->Write(cs->IsOutputBigEndian() ? CUtility::SwapUInt32(u32) : u32);
			}
			break;
		case 16:
			{
				UInt16 u16 = (UInt16)flagVal;
				output->Write(cs->IsOutputBigEndian() ? CUtility::SwapUInt16(u16) : u16);
			}
			break;
		case 8:
			{
				Byte u8 = (Byte)flagVal;
				output->Write(u8);
			}
			break;
		}			
	}


	for(int i = 0; i < m_fields->Count; i++)
	{
		CFieldDef *field = dynamic_cast<CFieldDef*>(m_fields->get_Item(i));		
		if(!field->ShouldBeIgnored())
		{
			ERRORMGR->PushContext(CONTEXT_TYPE_FIELD, field->getName());
				
			int j = 0;
			for( ; j < numNodes; j++)
			{
				if(field->getName()->Equals(node->ChildNodes->get_ItemOf(j)->Name))
				{
					if(!field->Serialise(node->ChildNodes->get_ItemOf(j), output, objectDef, cs))
					{
						ERRORMGR->PopContext();
						return false;
					}

					break;
				}
			}
			if(j == numNodes)
			{
				// no match was found, try default field value
				if(!field->Serialise(NULL, output, objectDef, cs))
				{
					ERRORMGR->PopContext();
					return false;
				}			
			}

			ERRORMGR->PopContext();
		}		
	}

	//	For unpacked data, we need to make sure we end on our largest alignment boundary as well as start
	if (!objectDef->GetIsPacked())
	{
		CFieldDef::AlignOutputToField(output, largestAlign);
	}

	return true;
}

bool CCompositeFieldDef::OutputExternalStructure(TextWriter *output)
{
	if(ShouldBeIgnored())
		return true;

	for(int i = 0 ;i < m_fields->Count; i++)
	{
		dynamic_cast<CFieldDef*>(m_fields->get_Item(i))->OutputExternalStructure(output);	
	}

	return true;
}

bool CCompositeFieldDef::OutputHelperFunctions(TextWriter * /*output*/)
{
	return true;
}

bool CCompositeFieldDef::OutputStructure(TextWriter *output, int tabLevel)
{
	if(ShouldBeIgnored())
		return true;

	if(m_MaxOccurs != 1)
	{
		WriteTabs(output, tabLevel);
		output->Write("static const rage::");
		output->Write(m_NumType);
		output->Write(" MAX_");
		output->Write(getName()->ToUpper());
		output->Write("S = ");
		output->Write(m_MaxOccurs);
		output->WriteLine(";");

		WriteTabs(output, tabLevel);
		output->Write("rage::");
		output->Write(m_NumType);
		output->Write(" num");
		output->Write(getName());
		output->WriteLine("s;");
	}

	WriteTabs(output, tabLevel);
	
	bool bWriteLineAtEnd = false;

	if (m_units==FIELD_UNIT_VECTOR3||m_units==FIELD_UNIT_VECTOR4)
	{
		bWriteLineAtEnd = false;
		output->Write(m_units==FIELD_UNIT_VECTOR3 ? "rage::Vector3 " : "rage::Vector4 ");
		output->Write(getName());
	}
	else
	{
		bWriteLineAtEnd = true;
		output->Write("struct t");
		output->Write(getName());
		output->WriteLine();
		WriteTabs(output, tabLevel);
		output->WriteLine("{");
		tabLevel++;

		if(m_flags)
		{
			WriteTabs(output, tabLevel);
			output->WriteLine("union");
			WriteTabs(output, tabLevel);
			output->WriteLine("{");
			WriteTabs(output, tabLevel+1);
			output->Write("rage::u");
			output->Write(m_FlagWidth);
			output->WriteLine(" Value;");
			WriteTabs(output, tabLevel+1);
			output->WriteLine("struct ");
			WriteTabs(output, tabLevel+1);
			output->WriteLine("{");
			int paddingBits = m_FlagWidth - m_flags->Count;
			
			output->WriteLine("#if __BE");

			for(int i = m_FlagWidth - 1; i >= 0; i--)
			{
				if(i >= m_FlagWidth - paddingBits)
				{
					WriteTabs(output, tabLevel+2);
					output->Write("bool p");
					output->Write(i);	
					output->WriteLine(":1; // padding");
				}
				else
				{
					CFieldDef *flag = dynamic_cast<CFieldDef*>(m_flags->get_Item(i));

					WriteTabs(output, tabLevel+2);
					output->Write("bool ");
					output->Write(flag->getName());
					output->WriteLine(":1;");
				}
			}

			output->WriteLine("#else // !__BE");

			for(int i = 0; i < m_FlagWidth; i++)
			{
				if(i >= m_FlagWidth - paddingBits)
				{
					WriteTabs(output, tabLevel+2);
					output->Write("bool p");
					output->Write(i);	
					output->WriteLine(":1; // padding");
				}
				else
				{
					CFieldDef *flag = dynamic_cast<CFieldDef*>(m_flags->get_Item(i));

					WriteTabs(output, tabLevel+2);
					output->Write("bool ");
					output->Write(flag->getName());
					output->WriteLine(":1;");
				}
			}


			output->WriteLine("#endif // !__BE");

			WriteTabs(output, tabLevel+1);
			output->WriteLine("}BitFields;");
			WriteTabs(output, tabLevel);
			output->WriteLine("};");
		}

		for(int i = 0; i < m_fields->Count; i++)
		{
			CFieldDef *field = dynamic_cast<CFieldDef*>(m_fields->get_Item(i));
			if(!field->ShouldBeIgnored())
			{
				field->OutputStructure(output, tabLevel);	
			}
		}
		tabLevel--;

		/*else
		{
			WriteTabs(output, tabLevel);
			output->Write("struct t");
			output->Write(getName());
			output->WriteLine();
			WriteTabs(output, tabLevel);
			output->WriteLine("{");
			tabLevel++;

			for(int i = 0 ;i < m_fields->Count; i++)
			{
				dynamic_cast<CFieldDef*>(m_fields->get_Item(i))->OutputStructure(output, tabLevel);	
			}
			tabLevel--;
		}*/


		WriteTabs(output, tabLevel);
		output->Write("}");
		output->Write(getName());
	}

	if(m_MaxOccurs != 1)
	{
		output->Write("[MAX_");
		output->Write(getName()->ToUpper());
//		output->Write(m_MaxOccurs);
		output->Write("S]");
	}

	output->WriteLine(";");
	if (bWriteLineAtEnd)
		output->WriteLine();
	return true;
}

unsigned int CCompositeFieldDef::GetAlignment()
{
	if (m_units==FIELD_UNIT_VECTOR3||m_units==FIELD_UNIT_VECTOR4)
		return 16;

	unsigned int largestAligmment = m_Alignment;
	unsigned int flagAlignment = m_FlagWidth>0 ? (m_FlagWidth / 8) : 1;
	largestAligmment = flagAlignment > largestAligmment ? flagAlignment : largestAligmment;
	for(int i = 0; i < m_fields->Count; i++)
	{
		CFieldDef *field = dynamic_cast<CFieldDef*>(m_fields->get_Item(i));		
		if(!field->ShouldBeIgnored())
		{
			const unsigned int alignment = field->GetAlignment();
			if (alignment > largestAligmment)
				largestAligmment = alignment;
		}		
	}
	return largestAligmment;
}

bool CCompositeFieldDef::ShouldBeAtEndOfStruct()
{
	if(m_OverrideShouldBeAtEndOfStruct)
		return false;
	return (m_MaxOccurs != 1 && !m_FixedSize);
}
}
