//
// FieldDef.cpp
// 
// Copyright (C) 2005 Rockstar Games.  All Rights Reserved.
//

#include "FieldTypes.h"
#include "FieldDef.h"

#include "SimpleFieldDef.h"
#include "CompositeFieldDef.h"
#include "AllocatedSpaceFieldDef.h"


#include "ErrorManager.h"

namespace rage
{

CFieldDef::CFieldDef(void)
{
	m_strName = NULL;
	m_type = FIELD_TYPE_UNSPECIFIED;
	m_units = FIELD_UNIT_UNSPECIFIED;
	m_Ignore = false;
	m_OverrideParent = false;
	m_AllowOverrideControl = false;
	m_Alignment = 1;
}

CFieldDef::~CFieldDef(void)
{
}

unsigned int CFieldDef::GetAlignment(FieldType type)
{
	switch (type)
	{
	case FIELD_TYPE_INT8:
	case FIELD_TYPE_UINT8:
	case FIELD_TYPE_STRING:
	case FIELD_TYPE_FIXED_STRING:
	case FIELD_TYPE_TRISTATE:	// false: true: unspecified (TristateValue)
	case FIELD_TYPE_ENUM:		// enumerated type (u8)
	case FIELD_TYPE_BIT:			// false: true
		return 1;
	case FIELD_TYPE_INT16:
	case FIELD_TYPE_UINT16:
		return 2;
	case FIELD_TYPE_INT32:
	case FIELD_TYPE_UINT32:
	case FIELD_TYPE_HASH:		// hashed string (32bit)
	case FIELD_TYPE_FLOAT:
		return 4;
	case FIELD_TYPE_INT64:
	case FIELD_TYPE_UINT64:
		return 8;
	case FIELD_TYPE_COMPOSITE:
		ERRORMGR->HandleError("Composite field type GetAlignment() not overridden");
		return 0;
	default:
		return 0;
	}
}


CFieldDef *CFieldDef::ParseFieldDef(XmlNode *node)
{
	CFieldDef *def = NULL;
	String *name = NULL;

	if(node->Attributes != NULL)
	{
		if(node->Attributes->get_ItemOf(S"name") != NULL)
		{
			name = node->Attributes->get_ItemOf(S"name")->Value;
		}
		else
		{
			ERRORMGR->HandleError(S"No field name specified");
			return NULL;
		}
	}

	ERRORMGR->PushContext(CONTEXT_TYPE_FIELD, name);

	if(node->Name->CompareTo(S"Field") == 0)
	{
		def = CSimpleFieldDef::ParseSimpleFieldDef(node);
	}
	else if(node->Name->CompareTo(S"CompositeField") == 0)
	{
		def = CCompositeFieldDef::ParseCompositeFieldDef(node);
	}
	else if(node->Name->Equals(S"AllocatedSpaceField"))
	{
		def = CAllocatedSpaceFieldDef::ParseAllocatedSpaceFieldDef(node);
	}

	if(def)
	{
		//
		// pull out common field attributes
		//

		def->m_strName = name;

		if(node->Attributes != NULL)
		{			
			// store default value for this field if there is one
			if(node->Attributes->get_ItemOf(S"default") != NULL)
			{
				def->m_strDefault = node->Attributes->get_ItemOf(S"default")->Value;
			}
			else
			{
				def->m_strDefault = NULL;
			}
		
			if(node->Attributes->get_ItemOf(S"ignore") != NULL)
			{
				if(node->Attributes->get_ItemOf(S"ignore")->Value->Equals(S"yes"))
				{
					def->m_Ignore = true;
				}
			}

			if (node->Attributes->get_ItemOf("align") != NULL)
			{
				def->m_Alignment = UInt32::Parse(node->Attributes->get_ItemOf("align")->Value);
			}

			if(def->m_type != FIELD_TYPE_COMPOSITE)
			{
				if(node->Attributes->get_ItemOf(S"type") != NULL)
				{
					def->m_type = CFieldDef::GetFieldType(node->Attributes->get_ItemOf(S"type")->Value);
					if(def->m_type == FIELD_TYPE_ENUM)
					{
						def->m_EnumName = node->Attributes->get_ItemOf(S"enum")->Value;
					}
				}
				else
				{
					ERRORMGR->HandleWarning(S"No field type specified");
					def->m_type = FIELD_TYPE_UNKNOWN;
				}
			}

			if(!def->ShouldBeIgnored())
			{
				if(node->Attributes->get_ItemOf(S"units") != NULL)
				{
					def->m_units = CFieldDef::GetFieldUnits(node->Attributes->get_ItemOf(S"units")->Value);
					//if(def->m_units == FIELD_UNIT_UNKNOWN)
					//{
					//	ERRORMGR->HandleWarning(S"Unknown unit type specified");
					//}
				}
				else
				{
					// disable unit warnings for composite field types since they'll often not make
					// sense
					// disable unit warnigns for tristate fields since they definitely dont make sense
					//if(def->m_type != FIELD_TYPE_COMPOSITE && def->m_type != FIELD_TYPE_TRISTATE)
					//{
					//	ERRORMGR->HandleWarning(String::Concat(S"No unit specified for field ", def->m_strName));
					//}
					def->m_units = FIELD_UNIT_UNSPECIFIED;
				}
			}	

			if(node->Attributes->get_ItemOf(S"allowOverrideControl") != NULL)
			{
				def->m_AllowOverrideControl = node->Attributes->get_ItemOf(S"allowOverrideControl")->Value->ToLower()->Equals("yes");
			}
		}
	} // if (def)
	
	ERRORMGR->PopContext();
	return def;
}

FieldType CFieldDef::GetFieldType(String *strMixedCase)
{
	String *str = strMixedCase->ToLower();

	if(str->Equals("s8"))
	{
		return FIELD_TYPE_INT8;
	}
	else if(str->Equals("u8"))
	{
		return FIELD_TYPE_UINT8;
	}
	else if(str->Equals("s16"))
	{
		return FIELD_TYPE_INT16;
	}
	else if(str->Equals("u16"))
	{
		return FIELD_TYPE_UINT16;
	}
	else if(str->Equals("s32"))
	{
		return FIELD_TYPE_INT32;
	}
	else if(str->Equals("u32"))
	{
		return FIELD_TYPE_UINT32;
	}
	else if(str->Equals("s64"))
	{
		return FIELD_TYPE_INT64;
	}
	else if(str->Equals("u64"))
	{
		return FIELD_TYPE_UINT64;
	}
	else if(str->Equals("f32"))
	{
		return FIELD_TYPE_FLOAT;
	}
	else if(str->Equals("string"))
	{
		return FIELD_TYPE_STRING;
	}
	else if(str->Equals("cstring"))
	{
		return FIELD_TYPE_FIXED_STRING;
	}
	else if(str->Equals("tristate"))
	{
		return FIELD_TYPE_TRISTATE;
	}
	else if(str->Equals("bit"))
	{
		return FIELD_TYPE_BIT;
	}
	else if(str->Equals("hash"))
	{
		return FIELD_TYPE_HASH;
	}
	else if(str->Equals("enum"))
	{
		return FIELD_TYPE_ENUM;
	}

	return FIELD_TYPE_UNKNOWN;
}

FieldUnit CFieldDef::GetFieldUnits(String *strMixedCase)
{
	String *str = strMixedCase->ToLower();

	if(str->Equals("ms"))
	{
		return FIELD_UNIT_MILLISECONDS;
	}
	else if(str->Equals("cents"))
	{
		return FIELD_UNIT_CENTS;
	}
	else if(str->Equals("mb"))
	{
		return FIELD_UNIT_MILLIBELS;
	}
	else if(str->Equals("0.01units"))
	{
		return FIELD_UNIT_FIXED_POINT;
	}
	else if(str->Equals("degrees"))
	{
		return FIELD_UNIT_DEGREES;
	}
	else if(str->Equals("percentage"))
	{
		return FIELD_UNIT_PERCENTAGE;
	}
	else if(str->Equals("waveref"))
	{
		return FIELD_UNIT_WAVE_REFERENCE;
	}
	else if(str->Equals("objectref"))
	{
		return FIELD_UNIT_OBJECT_REFERENCE;
	}
	else if(str->Equals("stringtableindex"))
	{
		return FIELD_UNIT_STRING_TABLE_INDEX;
	}
	else if(str->Equals("vector3"))
	{
		return FIELD_UNIT_VECTOR3;
	}
	else if(str->Equals("vector4"))
	{
		return FIELD_UNIT_VECTOR4;
	}
	else
	{
		return FIELD_UNIT_UNKNOWN;
	}
}

unsigned int CFieldDef::GetFieldSize(String *str)
{
	return GetFieldSize(GetFieldType(str));
}
unsigned int CFieldDef::GetFieldSize(FieldType type)
{
	switch (type)
	{
	case FIELD_TYPE_INT8:
	case FIELD_TYPE_UINT8:
	case FIELD_TYPE_ENUM:
		return 1;
	case FIELD_TYPE_INT16:
	case FIELD_TYPE_UINT16:
		return 2;
	case FIELD_TYPE_INT32:
	case FIELD_TYPE_UINT32:
	case FIELD_TYPE_HASH:
	case FIELD_TYPE_FLOAT:
		return 4;
	case FIELD_TYPE_INT64:
	case FIELD_TYPE_UINT64:
		return 8;
	case FIELD_TYPE_TRISTATE:
		return 0;
	case FIELD_TYPE_STRING:
	case FIELD_TYPE_FIXED_STRING:
	case FIELD_TYPE_COMPOSITE:
	case FIELD_TYPE_BIT:
	case FIELD_TYPE_UNSPECIFIED:
	case FIELD_TYPE_UNKNOWN:
	default:
		ERRORMGR->HandleError("Field type size is not known.");
		return 0;
	}
}

bool CFieldDef::WriteConvertedValue(String *str, FieldType type, BinaryWriter *output, int length, bool bPacked, bool bBigEndian)
{
	SByte bv;
	Byte ubv;
	UInt16 uiv16;
	Int16 iv16;
	Int32 iv32;
	UInt32 uiv32;
	Single sv;
	Int64 iv64;
	UInt64 uiv64;

	SByte dataArray __gc[] = new SByte __gc[1];
	Byte udataArray __gc[] = new Byte __gc[1];
	Single floatArray [] = new Single[4];
	floatArray[0] = floatArray[1] = floatArray[2] = floatArray[3] = 0.0f;

	try
	{
		switch(type)
		{
		case FIELD_TYPE_HASH:
			if (!bPacked)
				AlignOutputToField(output, type);
			if(str == NULL)
			{
				iv32 = 0;
			}
			else
			{
				iv32 = CObjectCompiler::ComputeHash(str->ToUpper());
			}

			if(bBigEndian)
			{
				iv32 = CUtility::SwapInt32(iv32);
			}
			output->Write(iv32);
			break;
		case FIELD_TYPE_STRING:
			if (!bPacked)
				AlignOutputToField(output, 1);
			if(str == NULL)
			{
				str = String::Empty;
			}

			output->Write(str);
			break;
		case FIELD_TYPE_FIXED_STRING:
			{		
				if (!bPacked)
					AlignOutputToField(output, 1);
				if(str == NULL)
				{
					str = String::Empty;
				}

				Byte asciiBytes __gc[] = System::Text::Encoding::ASCII->GetBytes(str);
				int stringLength = Math::Min(length, str->Length);
				output->Write(asciiBytes,0,stringLength);
				// pad with zeros
				Byte p = 0;
				for(int i = 0; i < length-stringLength; i++)
				{
					output->Write(p);
				}
			}
			break;
		case FIELD_TYPE_UINT8:
			if (!bPacked)
				AlignOutputToField(output, type);
			if(str == NULL)
			{
				ubv = 0;
			}
			else
			{
				ubv = Byte::Parse(str);
			}
			// having to go to ridiculous lengths to ensure Write() only writes 1 byte:
			udataArray[0] = ubv;
			output->Write(udataArray);
			break;
		case FIELD_TYPE_INT8:
			if (!bPacked)
				AlignOutputToField(output, type);
			if(str == NULL)
			{
				bv = 0;
			}
			else
			{
				bv = SByte::Parse(str);
			}
			// having to go to ridiculous lengths to ensure Write() only writes 1 byte:
			dataArray[0] = bv;
			output->Write(dataArray);
			break;
		case FIELD_TYPE_UINT16:			
			if (!bPacked)
				AlignOutputToField(output, type);
			if(str == NULL)
			{
				uiv16 = 0;
			}
			else
			{
				uiv16 = UInt16::Parse(str);
				if(bBigEndian)
				{
					uiv16 = CUtility::SwapUInt16(uiv16);
				}
			}
			output->Write(uiv16);
			break;
		case FIELD_TYPE_INT16:
			if (!bPacked)
				AlignOutputToField(output, type);
			if(str == NULL)
			{
				iv16 = 0;
			}
			else
			{
				if(!Int16::TryParse(str,&iv16))
				{
					iv16 = 0;
					ERRORMGR->HandleWarning(String::Format("Invalid value for s16: {0}",str));
				}
				
				if(bBigEndian)
				{
					iv16 = CUtility::SwapUInt16(iv16);
				}
			}
		
			output->Write(iv16);
			break;
		case FIELD_TYPE_UINT32:
			if (!bPacked)
				AlignOutputToField(output, type);
			if(str == NULL)
			{
				uiv32 = 0;
			}
			else
			{
				uiv32 = UInt32::Parse(str);
				if(bBigEndian)
				{
					uiv32 = CUtility::SwapUInt32(uiv32);
				}
			}
			output->Write(uiv32);
			break;
		case FIELD_TYPE_INT32:
			if (!bPacked)
				AlignOutputToField(output, type);
			if(str == NULL)
			{
				iv32 = 0;
			}
			else
			{
				iv32 = Int32::Parse(str);
				if(bBigEndian)
				{
					iv32 = CUtility::SwapInt32(iv32);
				}
			}
			output->Write(iv32);
			break;
		case FIELD_TYPE_UINT64:
			if (!bPacked)
				AlignOutputToField(output, type);
			if(str == NULL)
			{
				uiv64 = 0;
			}
			else
			{
				uiv64 = UInt64::Parse(str);
				if(bBigEndian)
				{
					uiv64 = CUtility::SwapUInt64(uiv64);
				}
			}
			output->Write(uiv64);
			break;
		case FIELD_TYPE_INT64:
			if (!bPacked)
				AlignOutputToField(output, type);
			if(str == NULL)
			{
				iv64 = 0;
			}
			else
			{
				iv64 = Int64::Parse(str);
				if(bBigEndian)
				{
					iv64 = CUtility::SwapUInt64(iv64);
				}
			}
			output->Write(iv64);
			break;
		case FIELD_TYPE_FLOAT:
			if (!bPacked)
				AlignOutputToField(output, type);
			if(str == NULL)
			{
				sv = 0.f;
			}
			else
			{
				sv = Single::Parse(str);
				if(bBigEndian)
				{
					sv = CUtility::SwapFloat(sv);
				}
			}
			output->Write(sv);
			break;
		default:
			return false;
		}

	}
	catch(Exception *ex)
	{
		ERRORMGR->HandleException(ex);
		return false;
	}

	return true;
}

unsigned int CFieldDef::AlignOutputToField(BinaryWriter *output, unsigned int alignBytes)
{
	if(alignBytes > 1)
	{
		// pad to alignment boundary
		const unsigned int streamPos = (unsigned int)output->BaseStream->Position;
		
		// subtract 8 from actual file offset to account for the metadata block size and version
		const unsigned int boundary = (streamPos-8) % alignBytes;
		if (boundary > 0)
		{
			const unsigned int padding = alignBytes - boundary;
			for (unsigned int pd = 0; pd < padding; pd++)
			{
				//	Write a single 0 byte to the stream
				output->Write(false);
			}
		}
	}

	return (unsigned int)output->BaseStream->Position;
}

unsigned int CFieldDef::AlignOutputToField(BinaryWriter *output, FieldType type)
{
	return AlignOutputToField(output, GetAlignment(type));
}

String *CFieldDef::getTypeString(FieldType type)
{
	switch(type)
	{
	case FIELD_TYPE_INT8:
		return S"rage::s8";
	case FIELD_TYPE_INT16:
		return S"rage::s16";
	case FIELD_TYPE_INT32:
		return S"rage::s32";
	case FIELD_TYPE_INT64:
		return S"rage::s64";
	case FIELD_TYPE_UINT8:
		return S"rage::u8";
	case FIELD_TYPE_UINT16:
		return S"rage::u16";
	case FIELD_TYPE_UINT32:
		return S"rage::u32";
	case FIELD_TYPE_UINT64:
		return S"rage::u64";
	case FIELD_TYPE_FLOAT:
		return S"rage::f32";
	case FIELD_TYPE_STRING:
		return S"char*";
	case FIELD_TYPE_FIXED_STRING:
		return S"char";
	case FIELD_TYPE_HASH:
		return S"rage::u32";
	case FIELD_TYPE_ENUM:
		return S"rage::s8";
	/*case FIELD_TYPE_CUSTOM:
		this->m_EnumName*/
	default:
		return S"void*";
	}
}

void CFieldDef::WriteTabs(TextWriter *output, int level)
{
	for(int i = 0 ; i < level; i++)
	{
		output->Write("\t");
	}
}

TristateValue CFieldDef::GetTristateValue(XmlNode *node)
{
	String* pText = node ? node->InnerText : getDefaultValue();
	
	if(pText == NULL)
	{
		return TRISTATE_VALUE_UNSPECIFIED;
	}
	else
	{
		if(pText->ToLower()->Equals("yes"))
		{
			return TRISTATE_VALUE_TRUE;
		}
		else
		{
			// default to false if the field is present (but not yes)
			return TRISTATE_VALUE_FALSE;
		}
	}
}


}
