//
// Utility.cpp
// 
// Copyright (C) 2005 Rockstar Games.  All Rights Reserved.
//


#include "Utility.h"

namespace rage
{



float CUtility::SwapFloat(float f)
{
	union
	{
		float f;
		unsigned char b[4];
	} dat1, dat2;

	dat1.f = f;
	dat2.b[0] = dat1.b[3];
	dat2.b[1] = dat1.b[2];
	dat2.b[2] = dat1.b[1];
	dat2.b[3] = dat1.b[0];
	return dat2.f;
}

UInt32 CUtility::SwapUInt32(UInt32 i)
{
	unsigned char b1, b2, b3, b4;

	b1 = (unsigned char)(i & 255);
	b2 = (unsigned char)(( i >> 8 ) & 255);
	b3 = (unsigned char)(( i>>16 ) & 255);
	b4 = (unsigned char)(( i>>24 ) & 255);

	return ((int)b1 << 24) + ((int)b2 << 16) + ((int)b3 << 8) + b4;
}
Int32 CUtility::SwapInt32(Int32 i)
{
	unsigned char b1, b2, b3, b4;

	b1 = (unsigned char)(i & 255);
	b2 = (unsigned char)(( i >> 8 ) & 255);
	b3 = (unsigned char)(( i>>16 ) & 255);
	b4 = (unsigned char)(( i>>24 ) & 255);

	return ((int)b1 << 24) + ((int)b2 << 16) + ((int)b3 << 8) + b4;
}

UInt64 CUtility::SwapUInt64(UInt64 i)
{
	union
	{
		__int64 i;
		unsigned char b[8];
	} dat1, dat2;

	dat1.i = i;
	dat2.b[0] = dat1.b[7];
	dat2.b[1] = dat1.b[6];
	dat2.b[2] = dat1.b[5];
	dat2.b[3] = dat1.b[4];
	dat2.b[4] = dat1.b[3];
	dat2.b[5] = dat1.b[2];
	dat2.b[6] = dat1.b[1];
	dat2.b[7] = dat1.b[0];

	return dat2.i;
}
Int64 CUtility::SwapInt64(Int64 i)
{
	union
	{
		__int64 i;
		unsigned char b[8];
	} dat1, dat2;

	dat1.i = i;
	dat2.b[0] = dat1.b[7];
	dat2.b[1] = dat1.b[6];
	dat2.b[2] = dat1.b[5];
	dat2.b[3] = dat1.b[4];
	dat2.b[4] = dat1.b[3];
	dat2.b[5] = dat1.b[2];
	dat2.b[6] = dat1.b[1];
	dat2.b[7] = dat1.b[0];

	return dat2.i;
}

UInt16 CUtility::SwapUInt16(UInt16 s)
{
  unsigned char b1, b2;
  
  b1 = (unsigned char)(s & 255);
  b2 = (unsigned char)((s >> 8) & 255);

  return (b1 << 8) + b2;
}
Int16 CUtility::SwapInt16(Int16 s)
{
	unsigned char b1, b2;

	b1 = (unsigned char)(s & 255);
	b2 = (unsigned char)((s >> 8) & 255);

	return (b1 << 8) + b2;
}

XmlNode *CUtility::FindChildNode(XmlNode *node, String *name)
{
	for(int i = 0 ; i < node->ChildNodes->Count; i++)
	{
		if(node->ChildNodes->get_ItemOf(i)->Name->Equals(name))
		{
			return node->ChildNodes->get_ItemOf(i);
		}
	}
	return NULL;
}


}
