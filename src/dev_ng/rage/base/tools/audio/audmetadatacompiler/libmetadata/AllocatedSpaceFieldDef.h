#ifndef __ALLOCATEDSPACEFIELDDEF_H
#define __ALLOCATEDSPACEFIELDDEF_H

#using <mscorlib.dll>
#using <system.xml.dll>

using namespace System;
using namespace System::Xml;
using namespace System::Collections;
using namespace System::IO;


#include "FieldDef.h"

namespace rage
{

public __gc class CAllocatedSpaceFieldDef : public CFieldDef
{
public:
	CAllocatedSpaceFieldDef(void);

	virtual bool Serialise(XmlNode *node, BinaryWriter *output, CObjectDef* objectDef, CObjectCompiler *cs);
	virtual bool OutputStructure(TextWriter *output, int tabLevel);
	virtual unsigned int GetAlignment()
	{
		return 1;
	}

	virtual bool IsDefaultValue(XmlNode *)
	{
		return false;
	}

	virtual bool CanGenerateDefaultConstructor()
	{
		return false;
	}
	static CAllocatedSpaceFieldDef *ParseAllocatedSpaceFieldDef(XmlNode *node);
private:
	int m_maxSize;
	String *m_initialValue;
};

}
#endif
