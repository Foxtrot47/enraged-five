//
// AllocatedSpaceFieldDef.cpp
// 
// Copyright (C) 2005 Rockstar Games.  All Rights Reserved.
//
#include "AllocatedSpaceFieldDef.h"
#include "ErrorManager.h"
#include "FieldDef.h"
#include "SoundDef.h"

namespace rage
{

CAllocatedSpaceFieldDef::CAllocatedSpaceFieldDef(void) :
m_initialValue(0),
m_maxSize(0)
{

}

CAllocatedSpaceFieldDef *CAllocatedSpaceFieldDef::ParseAllocatedSpaceFieldDef(XmlNode *node)
{
	CAllocatedSpaceFieldDef *def = new CAllocatedSpaceFieldDef();

	if(node->Attributes->get_ItemOf(S"max") != NULL)
	{
		def->m_maxSize = System::Int32::Parse(node->Attributes->get_ItemOf(S"max")->Value);
		if(def->m_maxSize > 254)
		{
			ERRORMGR->HandleWarning(S"AllocatedSpaceField::max is greater than 254 - increase size of numElems field");
		}
	}
	else
	{
		ERRORMGR->HandleError(S"Missing max from allocated space field definition");
		//delete def;
		return NULL;
	}	

	if(node->Attributes->get_ItemOf(S"initialValue") != NULL)
	{
		def->m_initialValue = node->Attributes->get_ItemOf(S"initialValue")->Value;
	}
	else
	{
		ERRORMGR->HandleError(S"Missing initialValue from allocated space field definition");
		//delete def;
		return NULL;
	}	

	return def;
}


bool CAllocatedSpaceFieldDef::Serialise(XmlNode *node, BinaryWriter *output, CObjectDef* objectDef, CObjectCompiler *cs)
{
	// do nothing with ignored fields
	if(ShouldBeIgnored())
		return true;

	String *numElems;

	if(node != NULL)
	{
		numElems = node->InnerText;
	}
	else
	{
		ERRORMGR->HandleWarning("Using default value for numElems");
		numElems = m_strDefault;
	}


	WriteConvertedValue(numElems, FIELD_TYPE_UINT8, output, 0, objectDef->GetIsPacked(), cs->IsOutputBigEndian());

	for(unsigned i = 0; i < UInt32::Parse(numElems); i++)
	{
		WriteConvertedValue(m_initialValue, m_type, output, 0, objectDef->GetIsPacked(), cs->IsOutputBigEndian());
	}

	return true;
}

bool CAllocatedSpaceFieldDef::OutputStructure(TextWriter *output, int tabLevel)
{
	if(ShouldBeIgnored())
		return true;

	WriteTabs(output, tabLevel);
	output->Write("rage::u8 num");
	output->Write(getName());
	output->WriteLine("Elems;");
	

	WriteTabs(output, tabLevel);
	output->Write(getTypeString(m_type));
	output->Write(" ");
	output->Write(getName());
	output->Write("[");
	output->Write(m_maxSize);
	output->WriteLine("];");

	return true;
}

}
