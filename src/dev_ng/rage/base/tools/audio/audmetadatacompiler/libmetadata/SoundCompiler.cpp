//
// SoundCompiler.cpp
// 
// Copyright (C) 2005 Rockstar Games.  All Rights Reserved.
//

#include "SchemaManager.h"
#include "SoundCompiler.h"
#include "SoundDef.h"
#include "ErrorManager.h"
#include "FieldDef.h"
#include "Utility.h"

namespace rage
{

CObjectCompiler::CObjectCompiler(bool bBigEndian, String *classFactoryName, String *classFactoryReturnType, bool usesBucketAllocator, bool use3ByteNameTableOffset)
{
	m_SchemaManager = new CSchemaManager(classFactoryName,classFactoryReturnType, usesBucketAllocator);
	m_bBigEndian = bBigEndian;	
	m_IsOutputAligned = true;
	m_TemplateManager = new CTemplateManager();
	m_Use3ByteNameTableOffset = use3ByteNameTableOffset;
	Init();
}

CObjectCompiler::CObjectCompiler(bool bBigEndian, String *classFactoryName, String *classFactoryReturnType, bool use3ByteNameTableOffset)
{
	m_SchemaManager = new CSchemaManager(classFactoryName,classFactoryReturnType, false);
	m_bBigEndian = bBigEndian;	
	m_IsOutputAligned = true;
	m_TemplateManager = new CTemplateManager();
	m_Use3ByteNameTableOffset = use3ByteNameTableOffset;
	Init();
}

void CObjectCompiler::Init()
{
	m_ObjectLookup = new ArrayList();
	m_ObjectHashLookup = new Hashtable();
	m_ObjectReferences = new ArrayList();
	m_BankReferences = new ArrayList();
	m_StringTable = new ArrayList();	

	m_ObjectNameToCompile = NULL;
}

bool CObjectCompiler::LoadTypeDefinitions(String *typeDefinitions, String *classPrefix, String *classIncludePath)
{
	if(!m_SchemaManager->LoadObjectDefinitions(typeDefinitions, classPrefix, classIncludePath ))
	{
		return false;
	}

	m_TypeDefinitions = typeDefinitions;
	m_ClassPrefix = classPrefix;
	m_ClassIncludePath = classIncludePath;

	return true;
}

bool CObjectCompiler::ReLoadTypeDefinitions()
{
	delete m_SchemaManager;
	m_SchemaManager = new CSchemaManager(NULL,NULL, false);
	return LoadTypeDefinitions(m_TypeDefinitions, m_ClassPrefix, m_ClassIncludePath);
}

bool CObjectCompiler::LoadTemplates(XmlDocument *xmlDoc)
{
	return m_TemplateManager->LoadTemplates(xmlDoc, this);
}

bool CObjectCompiler::CompileObject(XmlNode *node, BinaryWriter *output)
{
	CObjectDef *sd = m_SchemaManager->GetObjectDefinition(node->Name);
	if(sd == NULL)
	{
		return false;
	}

	return sd->SerialiseInstance(node, output, this);
}

bool CObjectCompiler::CompileObjects(String *strPath, BinaryWriter *output)
{
	XmlDocument *xmlDoc = new XmlDocument();

	try
	{
		xmlDoc->Load(strPath);
	}
	catch(Exception *ex)
	{
		ERRORMGR->HandleException(ex);
		return false;
	}

	return CompileObjects(xmlDoc, output);
}

void CObjectCompiler::CheckForWarningsAndErrors(XmlNode *node)
{
	if(node->Name->Equals(S"Warning"))
	{
		ERRORMGR->HandleWarning(node->InnerText);
	}
	else if(node->Name->Equals(S"Error"))
	{
		ERRORMGR->HandleError(node->InnerText);
	}

	for(Int32 i = 0 ; i < node->ChildNodes->Count; i++)
	{
		CheckForWarningsAndErrors(node->ChildNodes->get_ItemOf(i));
	}
}

bool CObjectCompiler::CompileObjects(XmlDocument *xmlDoc, BinaryWriter *output)
{
	bool bRet = false;
	UInt32 streamPos = 0;

	if(!xmlDoc->DocumentElement->Name->Equals(S"Objects"))
	{
		ERRORMGR->HandleError("Xml document doesn't have root node Objects");
		return false;
	}
	// search for any templates to process within this document
	ArrayList *templatesToProcess = new ArrayList();
	for(Int32 i = 0; i < xmlDoc->DocumentElement->ChildNodes->Count; i++)
	{
		XmlNode *node = xmlDoc->DocumentElement->ChildNodes->get_ItemOf(i);
		if(node->Name->Equals(S"TemplateInstance"))
		{
			templatesToProcess->Add(node);
		}
	}
	// process them - note that this will add and remove nodes from the document, hence the seperate loop
	for(Int32 i = 0 ; i < templatesToProcess->Count; i++)
	{
		if(!m_TemplateManager->ProcessObject(dynamic_cast<XmlNode*>(templatesToProcess->get_Item(i))))
		{
			continue;
		}
	}

	// compile the preprocessed objects
	for(Int32 i = 0; i < xmlDoc->DocumentElement->ChildNodes->Count; i++)
	{
		XmlNode *node= xmlDoc->DocumentElement->ChildNodes->get_ItemOf(i);
		
		// locate the matching object definition
		CObjectDef *def = m_SchemaManager->GetObjectDefinition(node->Name);
		if(!def)
			return false;
runValidators:
		if(def->GetValidators()->Count > 0)
		{
			for(Int32 v=0; v < def->GetValidators()->Count; v++)
			{
				XmlDocument *tempDoc = new XmlDocument();
				tempDoc->LoadXml(node->OuterXml);
				XmlTextWriter *results = new XmlTextWriter(new MemoryStream(),System::Text::Encoding::ASCII);

				dynamic_cast<XslCompiledTransform*>(def->GetValidators()->get_Item(v))->Transform(tempDoc,results);
				MemoryStream *mem = dynamic_cast<MemoryStream*>(results->BaseStream);
				String *res = System::Text::Encoding::ASCII->GetString(mem->GetBuffer(),0,mem->GetBuffer()->Length);
				tempDoc->LoadXml(res);
				node = dynamic_cast<XmlNode*>(tempDoc->DocumentElement);

				CheckForWarningsAndErrors(node);
			}
		}

		// locate the matching object definition again, so that a validator can change an object type
		CObjectDef *newDef = m_SchemaManager->GetObjectDefinition(node->Name);
		if(!newDef)
			return false;
		if(newDef != def)
		{
			def = newDef;
			goto runValidators;
		}

		String *name = def->getInstanceName(node);

		if(name == NULL)
		{
			ERRORMGR->HandleError(S"Object instance with no name");
			continue;
		}

		if(m_ObjectNameToCompile != NULL && !m_ObjectNameToCompile->Equals(name))
		{
			continue;
		}

		ERRORMGR->PushContext(CONTEXT_TYPE_OBJECT, name);

		if(m_ObjectHashLookup->ContainsKey(name))
		{
			ERRORMGR->HandleError("Object already exists with the same name - ignoring this instance");
			ERRORMGR->PopContext();
			continue;
		}

		streamPos = (UInt32)output->BaseStream->Position;
		
		// pad to alignment boundary
		if(IsOutputAligned() && def->GetAlignment() > 1)
		{
			streamPos = CFieldDef::AlignOutputToField(output, def->GetAlignment());
		}

		//	For unpacked data, we need to respect a structure's natural alignment
		if (!def->GetIsPacked())
		{
			streamPos = CFieldDef::AlignOutputToField(output, def->GetLargestAlignment());
		}

		if(def->SerialiseInstance(node, output, this))
		{
			// subtract 8 from actual file offset to account for the metadata block size and version
			RecordCompiledObject(name, streamPos - 8, (Int32)(output->BaseStream->Position - streamPos));
			bRet = true;

		} // object compiled ok
		else
		{
			bRet = false;
		}

		ERRORMGR->PopContext();
	}


	return bRet;

}
void CObjectCompiler::RecordCompiledObject(String *name, Int32 offset, Int32 bytes)
{
	CompiledObject *compS = new CompiledObject;
	compS->name = name;
	compS->offset = offset;
	compS->bytes = bytes;
	m_ObjectLookup->Add(compS);
	m_ObjectHashLookup->Add(name, compS);
	ERRORMGR->RecordObjectSize(name, (UInt32)bytes);
}

char *StrToUpper(String *str)
{
	String *s = str->ToUpper();
	char utf8 __gc[] = Text::Encoding::UTF8->GetBytes(s);
    char __pin *nameStr = &utf8[0];
	return nameStr;
}

bool CObjectCompiler::SerialiseObjectLookup(BinaryWriter *output)
{

	char *str;

	if(m_bBigEndian)
	{
		output->Write(CUtility::SwapInt32(m_ObjectLookup->Count));
	}
	else
	{
		output->Write(m_ObjectLookup->Count);
	}

	// compute total string length for the cases where we need to keep it kicking around (categories/dev builds)
	UInt32 stringLen = 0;
	for(Int32 i = 0; i < m_ObjectLookup->Count; i++)
	{
		stringLen += dynamic_cast<CompiledObject*>(m_ObjectLookup->get_Item(i))->name->Length + 1;
	}

	if(m_bBigEndian)
	{
		output->Write(CUtility::SwapUInt32(stringLen));
	}
	else
	{
		output->Write(stringLen);
	}

	for(Int32 i = 0; i < m_ObjectLookup->Count; i++)
	{
		CompiledObject *cs = dynamic_cast<CompiledObject*>(m_ObjectLookup->get_Item(i));
		str = StrToUpper(cs->name);
		output->Write(str);

		if(m_bBigEndian)
		{
			output->Write(CUtility::SwapInt32(cs->offset));
		}
		else
		{
			output->Write(cs->offset);
		}

		if(m_bBigEndian)
		{
			output->Write(CUtility::SwapInt32(cs->bytes));
		}
		else
		{
			output->Write(cs->bytes);
		}
	}

	// serialise object references
	if(m_bBigEndian)
	{
		output->Write(CUtility::SwapInt32(m_ObjectReferences->Count));
	}
	else
	{
		output->Write(m_ObjectReferences->Count);
	}

	for(Int32 i = 0;  i < m_ObjectReferences->Count; i++)
	{
		// gotta love managed c++ for this shit...
		Int64 v64 = *dynamic_cast<__box Int64*>(m_ObjectReferences->get_Item(i));
		Int32 val = (Int32)v64;
		if(m_bBigEndian)
		{
			output->Write(CUtility::SwapInt32(val));
		}
		else
		{
			output->Write(val);
		}

	}

	// serialise bank name references
	if(m_bBigEndian)
	{
		output->Write(CUtility::SwapInt32(m_BankReferences->Count));
	}
	else
	{
		output->Write(m_BankReferences->Count);
	}

	for(Int32 i = 0;  i < m_BankReferences->Count; i++)
	{
		// gotta love managed c++ for this shit...
		Int64 v64 = *dynamic_cast<__box Int64*>(m_BankReferences->get_Item(i));
		Int32 val = (Int32)v64;
		if(m_bBigEndian)
		{
			output->Write(CUtility::SwapInt32(val));
		}
		else
		{
			output->Write(val);
		}

	}
	return true;
}

bool CObjectCompiler::OutputHelperFunctions(TextWriter *output, String *metadataType, String *fileName, String *headerFile, String *namespaceName)
{
	output->WriteLine("/******************************************************************");
	output->WriteLine();
	output->WriteLine(String::Concat(fileName, " - automatically generated metadata structure helper functions"));
	output->WriteLine();
	OutputGeneratedString(output);
	output->WriteLine();
	output->WriteLine("*******************************************************************/");
	output->WriteLine();

	output->Write("#include \"");
	output->Write(headerFile);
	output->WriteLine("\"");

	output->WriteLine("#include \"string/stringhash.h\"");

	output->WriteLine();
	output->Write("namespace ");
	output->Write(namespaceName);
	output->WriteLine(" {");


	m_SchemaManager->OutputHelperFunctions(output);

	output->WriteLine();

	m_SchemaManager->OutputBaseTypeLookups(output, metadataType, true);

	output->WriteLine("}");
	output->WriteLine();

	return true;
}

bool CObjectCompiler::OutputObjectDefs(TextWriter *output, String *metadataType, String *fileName, String *namespaceName)
{
	output->WriteLine("/******************************************************************");
	output->WriteLine();
	output->WriteLine(String::Concat(fileName, " - automatically generated metadata structure definitions"));
	output->WriteLine();
	OutputGeneratedString(output);
	output->WriteLine();
	output->WriteLine("*******************************************************************/");
	output->WriteLine();

	String *includeGuard = String::Concat("AUD_", fileName->ToUpper()->Replace(".", "_"));
	String *fileNameNoExtension = fileName->Substring(0, fileName->Length-2);

	output->WriteLine(String::Concat("#ifndef ", includeGuard));
	output->WriteLine(String::Concat("#define ", includeGuard));
	output->WriteLine();

	if (m_SchemaManager->GetIncludePaths()->Count)
	{
		output->WriteLine("// Includes");
		for (int i = 0; i < m_SchemaManager->GetIncludePaths()->Count; ++i)
		{
			output->WriteLine(String::Concat("#include \"", dynamic_cast<String*>(m_SchemaManager->GetIncludePaths()->get_Item(i)), "\""));
		}
		output->WriteLine();
	}

	output->WriteLine("// handy macros for dealing with packed tristates");
	output->WriteLine("#ifndef AUD_GET_TRISTATE_VALUE");
	output->WriteLine("#define AUD_GET_TRISTATE_VALUE(flagvar, flagid) (TristateValue)((flagvar >> (flagid<<1)) & 0x03)");
	output->WriteLine("#define AUD_SET_TRISTATE_VALUE(flagvar, flagid, trival) (TristateValue)((flagvar |= ((trival&0x03) << (flagid<<1))))");
	output->WriteLine();

	output->Write("namespace ");
	output->WriteLine("rage");
	output->WriteLine("{");

	output->WriteLine("// tristate values");
	output->WriteLine("enum TristateValue");
	output->WriteLine("{");
	output->WriteLine("\t\tAUD_TRISTATE_FALSE,");
	output->WriteLine("\t\tAUD_TRISTATE_TRUE,");
	output->WriteLine("\t\tAUD_TRISTATE_UNSPECIFIED,");
	output->WriteLine("};");

	output->WriteLine("} // namespace rage");

	output->WriteLine("#endif // !defined AUD_GET_TRISTATE_VALUE");

	output->Write("namespace ");
	output->WriteLine(namespaceName);
	output->WriteLine("{");

	output->Write("#define ");
	output->Write(fileNameNoExtension->ToUpper());
	output->Write("_SCHEMA_VERSION ");
	output->WriteLine(GetSchemaVersion());

	m_SchemaManager->OutputObjectDefIDs(output, fileNameNoExtension);
	output->WriteLine();
	m_SchemaManager->OutputGetAddressOfMacro(output, fileNameNoExtension);
	
	if (m_SchemaManager->GetPackingCurrentlyEnabled())
	{
		output->WriteLine("// disable struct alignment");
		output->WriteLine("#if !__SPU");
		output->WriteLine("#pragma pack(push, r1, 1)");
		output->WriteLine("#endif // !__SPU");
	}
	output->WriteLine();

	m_SchemaManager->OutputEnumDefs(output);

	m_SchemaManager->OutputObjectDefs(output);

	output->WriteLine();
	
	if (m_SchemaManager->GetPackingCurrentlyEnabled())
	{
		output->WriteLine("// re - enable struct alignment");
		output->WriteLine("#if !__SPU");
		output->WriteLine("#pragma pack(pop, r1)");
		output->WriteLine("#endif // __SPU");
	}

	m_SchemaManager->OutputBaseTypeLookups(output, metadataType, false);

	output->WriteLine();
	output->WriteLine();
	
	output->Write("} // namespace ");
	output->WriteLine(namespaceName);
	output->WriteLine(String::Concat("#endif // ", includeGuard));

	return true;
}

bool CObjectCompiler::OutputClassFactory(TextWriter *output, String *fileName, String *namespaceName)
{
	output->WriteLine("/******************************************************************");
	output->WriteLine();
	output->WriteLine(String::Concat(fileName, " - automatically generated metadata structure definitions"));
	output->WriteLine();
	OutputGeneratedString(output);
	output->WriteLine();
	output->WriteLine("*******************************************************************/");
	output->WriteLine();

	String *includeGuard = String::Concat("AUD_", fileName->ToUpper()->Replace(".", "_"));

	output->WriteLine(String::Concat("#ifndef ", includeGuard));
	output->WriteLine(String::Concat("#define ", includeGuard));
	output->WriteLine();

	m_SchemaManager->OutputClassFactoryFunction(output, namespaceName);

	output->WriteLine();
	output->WriteLine(String::Concat("#endif // ", includeGuard));

	return true;
}

void CObjectCompiler::OutputGeneratedString(TextWriter *output)
{
	output->Write("Generated on ");
	output->Write(DateTime::Now.ToString());
	output->Write(" by ");
	output->Write(Environment::UserName);
	output->Write(" on machine ");
	output->WriteLine(Environment::MachineName);
}

void CObjectCompiler::SerialiseStringTable(BinaryWriter *output)
{
	//UInt32 totalStringLength = 0, totalLookupSize = 0;
	UInt32 currentOffset = 0;

	// store number of entries
	if(!m_bBigEndian)
		output->Write(m_StringTable->Count);
	else
		output->Write(CUtility::SwapInt32(m_StringTable->Count));

	// serialize offset table
	for(Int32 i = 0; i < m_StringTable->Count; i++)
	{
		String *s = static_cast<String*>(m_StringTable->get_Item(i));
		if(!m_bBigEndian)
			output->Write(currentOffset);
		else
			output->Write(CUtility::SwapInt32(currentOffset));

		currentOffset += s->Length + 1; // account for trailing 0
	}	

	// serialize string table
	for(Int32 i = 0 ; i < m_StringTable->Count; i++)
	{
		// convert to null terminated string
		unsigned char bytes __gc[] = System::Text::Encoding::ASCII->GetBytes(static_cast<String*>(m_StringTable->get_Item(i)));
		output->Write(bytes);
		bytes[0] = 0;
		output->Write(bytes, 0, 1);
	}
}

Int32 CObjectCompiler::FindObjectOffset(String *name)
{
	if(name == NULL)
	{
		return -1;
	}
	CompiledObject *cs = dynamic_cast<CompiledObject*>(m_ObjectHashLookup->get_Item(name));
	if(cs == NULL)
	{
		return -1;
	}
	else
	{
		return cs->offset;
	}
}

Int32 CObjectCompiler::FindObjectSize(String *name)
{
	CompiledObject *cs = dynamic_cast<CompiledObject*>(m_ObjectHashLookup->get_Item(name));
	if(cs == NULL)
	{
		return -1;
	}
	else
	{
		return cs->bytes;
	}
}

void CObjectCompiler::RecordNewObject(String *objectName, Int32 offset, Int32 size)
{
	RecordCompiledObject(objectName, offset, size);
}

void CObjectCompiler::UpdateObjectSizeAndOffset(String *objectName, Int32 offset, Int32 size)
{
	CompiledObject *cs = dynamic_cast<CompiledObject*>(m_ObjectHashLookup->get_Item(objectName));
	if(cs)
	{
		cs->bytes = size;
		cs->offset = offset;
	}
}


// taken from rage atl/map.cpp
UInt32 CObjectCompiler::ComputeHash(String *value)
{
	if(!value || value->Equals(""))
	{
		return 0;
	}

	//Convert .Net Wave name to a system string and then to a Rage hash code.
    char utf8 __gc[] = Text::Encoding::UTF8->GetBytes(value);
    char __pin *nameStr = &utf8[0];

	// This is the one-at-a-time hash from this page:
	// http://burtleburtle.net/bob/hash/doobs.html
	// (borrowed from /soft/swat/src/swcore/string2key.cpp)

	bool quotes = (*nameStr == '\"');
	UInt32 key = 0;

	if (quotes) 
		nameStr++;

	while (*nameStr && (!quotes || *nameStr != '\"'))
	{
		char character = *nameStr++;
		if (character >= 'A' && character <= 'Z') 
			character += 'a'-'A';
		else if (character == '\\')
			character = '/';

		key += character;
		key += (key << 10);	//lint !e701
		key ^= (key >> 6);	//lint !e702
	}
	key += (key << 3);	//lint !e701
	key ^= (key >> 11);	//lint !e702
	key += (key << 15);	//lint !e701

	// The original swat code did several tests at this point to make
	// sure that the resulting value was representable as a valid
	// floating-point number (not a negative zero, or a NaN or Inf)
	// and also reserved a nonzero value to indicate a bad key.
	// We don't do this here for generality but the caller could
	// obviously add such checks again in higher-level code.
	return key;
}

}
