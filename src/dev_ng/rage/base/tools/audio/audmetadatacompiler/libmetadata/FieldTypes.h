#pragma once 

namespace rage
{
enum FieldType
{
	FIELD_TYPE_INT8 = 0,
	FIELD_TYPE_UINT8,
	FIELD_TYPE_INT16,
	FIELD_TYPE_UINT16,
	FIELD_TYPE_INT32,
	FIELD_TYPE_UINT32,
	FIELD_TYPE_INT64,
	FIELD_TYPE_UINT64,
	FIELD_TYPE_FLOAT,
	FIELD_TYPE_STRING,
	FIELD_TYPE_FIXED_STRING,
	FIELD_TYPE_TRISTATE,	// false, true, unspecified (TristateValue)
	FIELD_TYPE_COMPOSITE,
	FIELD_TYPE_BIT,			// false, true
	FIELD_TYPE_HASH,		// hashed string (32bit)
	FIELD_TYPE_ENUM,		// enumerated type (u8)
	FIELD_TYPE_CUSTOM,
	FIELD_TYPE_UNSPECIFIED,
	FIELD_TYPE_UNKNOWN
};

enum FieldUnit
{
	FIELD_UNIT_MILLISECONDS = 0,  // ms
	FIELD_UNIT_CENTS,			  // cents
	FIELD_UNIT_MILLIBELS,		  // mB
	FIELD_UNIT_FIXED_POINT,		  // 0.01units
	FIELD_UNIT_DEGREES,			  // degrees
	FIELD_UNIT_PERCENTAGE,		  // percentage 0-100
	FIELD_UNIT_WAVE_REFERENCE,	  // wave reference
	FIELD_UNIT_OBJECT_REFERENCE,	  // object reference
	FIELD_UNIT_STRING_TABLE_INDEX, // 32 bit index into string table
	FIELD_UNIT_VECTOR3,
	FIELD_UNIT_VECTOR4,
	FIELD_UNIT_UNSPECIFIED,		  // no units attrib specified
	FIELD_UNIT_UNKNOWN			  // unrecognised units attrib
};

__value enum TristateValue
{
	TRISTATE_VALUE_FALSE = 0,
	TRISTATE_VALUE_TRUE,
	TRISTATE_VALUE_UNSPECIFIED
};

}
