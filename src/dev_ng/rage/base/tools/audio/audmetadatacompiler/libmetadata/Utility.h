//
// Utility.h
// 
// Copyright (C) 2005 Rockstar Games.  All Rights Reserved.
//

#pragma once
using namespace System;
using namespace System::Xml;
using namespace System::Collections;
using namespace System::IO;
namespace rage
{

public __gc class CUtility
{
public:
	CUtility() {}

	// endian conversion routines
	static float SwapFloat(float f);
	static UInt32 SwapUInt32(UInt32 i);
	static Int32 SwapInt32(Int32 i);
	static UInt16 SwapUInt16(UInt16 s);
	static Int16 SwapInt16(Int16 s);
	static UInt64 SwapUInt64(UInt64 i);
	static Int64 SwapInt64(Int64 i);
	static XmlNode *FindChildNode(XmlNode *node, String *name);
};

}
