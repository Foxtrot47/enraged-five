//
// Enum.cpp
// 
// Copyright (C) 2005 Rockstar Games.  All Rights Reserved.
//

#include "Enum.h"
#include "SoundCompiler.h"

namespace rage
{

CEnum::CEnum()
{
	m_NameList = new ArrayList();
	m_DescriptionList = new ArrayList();
	m_InitialValue = 0;
	m_BitsetStyle = false;
}

CEnum *CEnum::ParseEnum(XmlNode *node)
{
	CEnum *e = new CEnum();

	if(node->Attributes->get_ItemOf("name") == NULL)
	{
		ERRORMGR->HandleError("Enum definition missing name");
		return NULL;
	}
	else
	{
		e->m_Name = node->Attributes->get_ItemOf("name")->Value;
		if(node->Attributes->get_ItemOf("initialValue"))
		{
			e->m_InitialValue = Int32::Parse(node->Attributes->get_ItemOf("initialValue")->Value);
		}
		if(node->Attributes->get_ItemOf("bitset"))
		{
			e->m_BitsetStyle = true;
		}
		for(int i = 0; i < node->ChildNodes->Count; i++)
		{
			if(node->ChildNodes->get_ItemOf(i)->Name->Equals("Value"))
			{
				e->m_NameList->Add(node->ChildNodes->get_ItemOf(i)->InnerText->Trim());
				if(node->ChildNodes->get_ItemOf(i)->Attributes->get_ItemOf("description"))
				{
					e->m_DescriptionList->Add(node->ChildNodes->get_ItemOf(i)->Attributes->get_ItemOf("description")->Value);
				}
				else
				{
					e->m_DescriptionList->Add(S"");
				}
			}
		}
	}
	return e;
}

bool CEnum::OutputStructure(TextWriter *output)
{
	

	if(m_BitsetStyle)
	{
		
		output->Write("enum ");
		output->WriteLine(GetName());
		output->WriteLine("{");
		/*output->WriteLine("#if __BE");
		for(int i = 0;i < m_NameList->Count; i++)
		{
			output->Write("\t\t");
			output->Write(m_NameList->get_Item(i));

			output->Write(" = (1<<");
			output->Write(m_NameList->Count-i-1);
			output->Write("),");

			if(!m_DescriptionList->get_Item(i)->Equals(S""))
			{
				output->Write("\t// ");
				output->Write(m_DescriptionList->get_Item(i));
			}

			output->WriteLine();
		}
	
		output->WriteLine("#else");*/

		for(int i = 0;i < m_NameList->Count; i++)
		{
			output->Write("\t\t");
			output->Write(m_NameList->get_Item(i));

			output->Write(" = (1<<");
			output->Write(i);
			output->Write("),");

			if(!m_DescriptionList->get_Item(i)->Equals(S""))
			{
				output->Write("\t// ");
				output->Write(m_DescriptionList->get_Item(i));
			}

			output->WriteLine();
		}


		//output->WriteLine("#endif");

		output->WriteLine("};");
		output->WriteLine();
	}
	else
	{
		bool isInitialValue = true;
		output->Write("enum ");
		output->WriteLine(GetName());
		output->WriteLine("{");
		for(int i = 0;i < m_NameList->Count; i++)
		{
			output->Write("\t\t");
			output->Write(m_NameList->get_Item(i));
			if(isInitialValue)
			{
				output->Write(" = ");
				output->Write(m_InitialValue);
				isInitialValue = false;
			}
			output->Write(",");

			if(!m_DescriptionList->get_Item(i)->Equals(S""))
			{
				output->Write("\t// ");
				output->Write(m_DescriptionList->get_Item(i));
			}

			output->WriteLine();
		}
		if (m_NameList->Count)
		{
			output->Write("\t\t");
			output->Write("NUM_");
			output->Write(GetName()->ToUpper());
			output->WriteLine(",");

			output->Write("\t\t");
			output->Write(GetName()->ToUpper());
			output->Write("_MAX = NUM_");
			output->Write(GetName()->ToUpper());
			output->WriteLine(",");
		}
		output->WriteLine("};");
		output->WriteLine();
	}
	
	// helper function prototypes
	output->Write("const char *");
	output->Write(GetName());
	output->Write("_ToString(const ");
	output->Write(GetName());
	output->WriteLine(" val);");

	output->WriteLine();
	output->Write(GetName());
	output->Write(" ");
	output->Write(GetName());
	output->Write("_Parse(const char *str, const ");
	output->Write(GetName());
	output->WriteLine(" defaultVal);");

	return true;
}

bool CEnum::OutputHelperFunctions(TextWriter *output)
{
	// debug friendly string list
	output->Write("const char *");
	output->Write(GetName());
	output->Write("_ToString(const ");
	output->Write(GetName());
	output->WriteLine(" val)");
	output->WriteLine("{");

	output->WriteLine("\tswitch(val)");
	output->WriteLine("\t{");

	for(int i = 0;i < m_NameList->Count; i++)
	{
		output->Write("\t\tcase ");
		output->Write(m_NameList->get_Item(i));
		output->Write(": return \"");
		output->Write(m_NameList->get_Item(i));
		output->WriteLine("\";");
	}

	if(!m_BitsetStyle)
	{
		output->Write("\t\tcase NUM_");
		output->Write(GetName()->ToUpper());
		output->Write(": return \"NUM_");
		output->Write(GetName()->ToUpper());
		output->WriteLine("\";");
	}

	output->WriteLine("\t}");
	output->WriteLine("\treturn NULL;");
	output->WriteLine("}");

	// string -> enum parse function

	output->WriteLine();
	output->Write(m_Name);
	output->Write(" ");
	output->Write(m_Name);
	output->Write("_Parse(const char *str, const ");
	output->Write(m_Name);
	output->WriteLine(" defaultVal)");
	output->WriteLine("{");

	output->WriteLine("\tconst u32 strHash = atStringHash(str);");
	output->WriteLine("\tswitch(strHash)");
	output->WriteLine("\t{");

	for(int i = 0;i < m_NameList->Count; i++)
	{
		output->Write("\t\tcase ");
		output->Write(CObjectCompiler::ComputeHash(static_cast<String*>(m_NameList->get_Item(i))));
		output->Write("U: return ");
		output->Write(m_NameList->get_Item(i));
		output->WriteLine(";");
	}
	
	output->WriteLine("\t}");
	output->WriteLine("\treturn defaultVal;");
	output->WriteLine("}");
	output->WriteLine();

	return true;
}

} // namespace rage
