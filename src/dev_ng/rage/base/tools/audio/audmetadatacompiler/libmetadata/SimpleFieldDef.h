#pragma once

#using <mscorlib.dll>
#using <system.xml.dll>

using namespace System;
using namespace System::Xml;
using namespace System::Collections;
using namespace System::IO;

namespace rage
{
public __gc class CSimpleFieldDef :	public CFieldDef
{
public:
	CSimpleFieldDef(void);

	virtual bool Serialise(XmlNode *node, BinaryWriter *output, CObjectDef* objectDef, CObjectCompiler *cs);
	virtual bool OutputStructure(TextWriter *output, int tabLevel);
	virtual bool IsDefaultValue(XmlNode *node);
	virtual unsigned int GetAlignment();
	
	String *GetValue(XmlNode *node);

	static CSimpleFieldDef *ParseSimpleFieldDef(XmlNode *node);

private:

	int m_Length;

};
}
