//
// CompositeFieldDef.h
// 
// Copyright (C) 2005 Rockstar Games.  All Rights Reserved.
//

#pragma once

#using <mscorlib.dll>
#using <system.xml.dll>

using namespace System;
using namespace System::Xml;
using namespace System::Collections;
using namespace System::IO;

#include "FieldDef.h"

namespace rage
{
public __gc class CCompositeFieldDef :
	public CFieldDef
{
public:
	CCompositeFieldDef(void);
	
	virtual bool Serialise(XmlNode *node, BinaryWriter *output, CObjectDef* objectDef, CObjectCompiler *cs);
	virtual bool FinishCompilation(XmlNode *node, BinaryWriter *output, CObjectDef* objectDef, CObjectCompiler *cs);
	virtual unsigned int GetAlignment();

	// PURPOSE
	//	Generate C structure representing this composite field
	virtual bool OutputStructure(TextWriter *output, int tabLevel);
	virtual bool OutputExternalStructure(TextWriter *output);
	virtual bool OutputHelperFunctions(TextWriter *output);

	// PURPOSE
	//	Instantiate CCompositeFieldDef from the supplied Xml node (taken from the ObjectDefinition)
	static CCompositeFieldDef *ParseCompositeFieldDef(XmlNode *node);

	virtual bool ShouldBeAtEndOfStruct();
	virtual bool IsDefaultValue(XmlNode *)
	{
		return false;
	}

	ArrayList *GetFields()
	{
		return m_fields;
	}

	int GetMaxOccurs()
	{
		return m_MaxOccurs;
	}

	String* GetNumType()
	{
		return m_NumType;
	}

	bool GetIsNativeType()
	{
		return m_NativeType;
	}

	void OverrideShouldBeAtEndOfStruct(bool should) { m_OverrideShouldBeAtEndOfStruct = should; }
private:

	// PURPOSE
	//	Serialise Xml composite field instance to binary metadata by serialising its
	//	component fields
	bool SerialiseInstance(XmlNode *node, BinaryWriter *output, CObjectDef* objectDef, CObjectCompiler *cs);
	bool Compile(XmlNode *node, BinaryWriter *output, CObjectDef* objectDef, CObjectCompiler *cs);
	ArrayList *m_fields;
	ArrayList *m_flags;
	int m_MaxOccurs;
	int m_FlagWidth;
	String *m_NumType;
	bool m_FixedSize;
	bool m_NativeType;
	bool m_OverrideShouldBeAtEndOfStruct;
};

}
