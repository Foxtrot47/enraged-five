// This is the main project file for VC++ application project 
// generated using an Application Wizard.

#include "stdafx.h"

#using <mscorlib.dll>

// for RAGE version info
#include "../../../../../build/version.h"

using namespace System;
using namespace System::IO;
using namespace System::Collections;
using namespace System::Xml;
using namespace rage;

#define NULL 0

void BuildProjectMetadata(String *workingPath, String *platformTag, audProjectSettings *projectSettings, String *metadataName, String *namespaceName, String *softRoot, bool shouldOutputCode, bool buildOutputPath)
{	
	CErrorManager::GetErrorManager()->PushContext(rage::ContextType::CONTEXT_TYPE_APPLICATION, String::Concat("Initializing ", projectSettings->GetProjectName(), " ", metadataName));

	projectSettings->SetCurrentPlatformByTag(platformTag);


	audMetadataType *metadataType = NULL;

	for(int i =0  ; i < projectSettings->GetMetadataTypes()->Length; i++)
	{
		if(projectSettings->GetMetadataTypes()[i]->Type->ToLower()->Equals(metadataName->ToLower()))
		{
			metadataType = projectSettings->GetMetadataTypes()[i];
			break;
		}
	}

	if(metadataType == NULL)
	{
		CErrorManager::GetErrorManager()->HandleError("Metadata section not found in project settings file");
		CErrorManager::GetErrorManager()->PopContext();
		return;
	}

	//	Use the namespace name defined in the metadata type
	if (metadataType->NameSpaceName && metadataType->NameSpaceName != "")
		namespaceName = metadataType->NameSpaceName;

	CObjectCompiler *compiler = new CObjectCompiler(projectSettings->GetCurrentPlatform()->IsBigEndian, metadataType->ClassFactoryFunctionName, metadataType->ClassFactoryFunctionReturnType, metadataType->UsesBucketAllocator, true);

	//set current directory
	String *workingDirectory = String::Concat(workingPath, projectSettings->GetSoundXmlPath());
	System::IO::Directory::SetCurrentDirectory(workingDirectory);
	CErrorManager::GetErrorManager()->HandleInfo(String::Concat("Current Directory ",Directory::GetCurrentDirectory()));

	//Load Type definitions
	for(int i = 0 ; i < metadataType->ObjectDefinitions->Length; i++)
	{
		String *typeDefs = String::Concat(workingPath, projectSettings->GetSoundXmlPath(),metadataType->ObjectDefinitions[i]->DefinitionsFile);
		if(!compiler->LoadTypeDefinitions(typeDefs, metadataType->ObjectDefinitions[i]->ClassPrefix, metadataType->ObjectDefinitions[i]->ClassIncludePath))
		{
			CErrorManager::GetErrorManager()->HandleError(String::Concat(S"Failed to load object type definitions from ",typeDefs));
			return;
		}
	}

	CErrorManager::GetErrorManager()->HandleInfo(S"Successfully loaded object type definitions");
	CErrorManager::GetErrorManager()->PopContext();

	if(metadataType->TemplatePath != NULL)
	{
		// Load templates
		CErrorManager::GetErrorManager()->PushContext(CONTEXT_TYPE_APPLICATION,"Loading templates");
		CErrorManager::GetErrorManager()->HandleInfo(S"Loading templates");
		ArrayList *templateFileList = CFileListBuilder::GetFileList(metadataType->TemplatePath, "xml");
		for(int i = 0; i < templateFileList->Count; i++)
		{
			String *templateFileName = dynamic_cast<String*>(templateFileList->get_Item(i));
			XmlDocument *doc = new XmlDocument();
			CErrorManager::GetErrorManager()->PushContext(CONTEXT_TYPE_FILE,templateFileName);

			doc->Load(templateFileName);
			compiler->LoadTemplates(doc);

			CErrorManager::GetErrorManager()->PopContext();
		}

		CErrorManager::GetErrorManager()->HandleInfo(S"Successfully loaded template definitions");
		CErrorManager::GetErrorManager()->PopContext();
	}


	//Cycle through every metadatafile that is equal to metadataname

	audMetadataFile *files []= projectSettings->GetMetadataSettings();
	for(int i=0;i<files->Count;i++)
	{
		// clear out the object look/bank name table
		compiler->Init();

		String *dataInput = String::Concat(workingDirectory,files[i]->DataPath);

		if(files[i]->Type->ToLower()->Equals(metadataName->ToLower()))
		{
			String *dataOutput = String::Concat(workingPath, projectSettings->ResolvePath(files[i]->OutputFile));

			if (buildOutputPath)
			{
				String *outputPath = String::Concat(workingPath, projectSettings->GetCurrentPlatform()->BuildOutput, "config\\");
				Directory::CreateDirectory(outputPath);
			}		

			// if output code was specified dont bother compiling objects
			if(!shouldOutputCode && dataOutput != NULL && dataInput != NULL)
			{
				Int32 size = 0;
				Int64 pos;

				CErrorManager::GetErrorManager()->HandleInfo(String::Concat(S"Writing metadata to: ", dataOutput));

				BinaryWriter *output = new BinaryWriter(new FileStream(dataOutput, System::IO::FileMode::Create, System::IO::FileAccess::Write));

				// store schema version as first four bytes
				if(projectSettings->GetCurrentPlatform()->IsBigEndian)
				{
					output->Write(CUtility::SwapInt32(compiler->GetSchemaVersion()));
				}
				else
				{
					output->Write(compiler->GetSchemaVersion());
				}

				// store size of metadata (will rewrite later)
				output->Write(size);

				ArrayList *fileList = CFileListBuilder::GetFileList(dataInput, "xml");

				for(int i = 0; i < fileList->Count; i++)
				{
					String *file = dynamic_cast<String*>(fileList->get_Item(i));

					CErrorManager::GetErrorManager()->PushContext(CONTEXT_TYPE_FILE, file);

					int numSoundsAlreadyCompiled = CErrorManager::GetErrorManager()->GetNumObjects();


					if(compiler->CompileObjects(file, output))
					{	
						Int32* compiledSounds = new Int32((CErrorManager::GetErrorManager()->GetNumObjects() - numSoundsAlreadyCompiled));
						//CErrorManager::GetErrorManager()->HandleInfo(String::Concat(S"Compiled ", compiledSounds->ToString(), S" objects from ", file->Substring(file->LastIndexOf("\\")+1)));
					}
					else
					{
						//CErrorManager::GetErrorManager()->HandleError(S"CompileObject Failed!");
					}

					CErrorManager::GetErrorManager()->PopContext();
				}


				CErrorManager::GetErrorManager()->PushContext(CONTEXT_TYPE_APPLICATION, "Finalizing");

				//size of metadata = position - (sizeof(size) + sizeof(version))
				pos = output->BaseStream->Position - 8;

				// metadata size is after version
				output->Seek(4, SeekOrigin::Begin);
				if(projectSettings->GetCurrentPlatform()->IsBigEndian)
				{
					output->Write(CUtility::SwapInt32((Int32)pos));
				}
				else
				{
					output->Write((int)pos);
				}

				output->Seek(0, SeekOrigin::End);
				CErrorManager::GetErrorManager()->HandleInfo(S"Writing string table...");

				size = 0;
				output->Write(size);
				pos = output->BaseStream->Position;

				compiler->SerialiseStringTable(output);

				size = (int)(output->BaseStream->Position - pos);
				// pos is currently pointing just after size, so that the calculated size doesnt include sizeof(size). subtract 4 to point to
				// size
				output->Seek((int)pos-4, SeekOrigin::Begin);
				if(projectSettings->GetCurrentPlatform()->IsBigEndian)
				{
					output->Write(CUtility::SwapInt32(size));
				}
				else
				{
					output->Write(size);
				}
				output->Seek(0, SeekOrigin::End);

				CErrorManager::GetErrorManager()->HandleInfo(S"Writing object map...");
				compiler->SerialiseObjectLookup(output);


				output->Close();

				if(metadataType->SideBySideVersioning && compiler->GetSchemaVersion() != ~0U)
				{
					//	Now create the side by side version by simply making a copy
					String* dataOutputVersioned = dataOutput->Concat(dataOutput, __box(compiler->GetSchemaVersion())->ToString());
					CErrorManager::GetErrorManager()->HandleInfo(String::Concat(S"Copying ", dataOutput, " to ", dataOutputVersioned));
					File::Copy(dataOutput, dataOutputVersioned, true);
				}

				CErrorManager::GetErrorManager()->PopContext();
			}

			// generate struct definitions for sound types if -codeoutput has been specified
			if(shouldOutputCode)
			{
				CErrorManager::GetErrorManager()->HandleInfo(S"Writing header file...");
				String *fullCodePath = softRoot==NULL ? metadataType->CodePath : String::Concat(softRoot, "\\", metadataType->CodePath);
				StreamWriter *structDef = new StreamWriter(fullCodePath);
				String *fileName = metadataType->CodePath->Substring(metadataType->CodePath->LastIndexOf("\\")+1);
				compiler->OutputObjectDefs(structDef, metadataType->TypeNotUpper, fileName, namespaceName);
				structDef->Close();

				CErrorManager::GetErrorManager()->HandleInfo(S"Writing helper functions...");

				StreamWriter *helperFunctions = new StreamWriter(fullCodePath->Replace(".h",".cpp"));
				compiler->OutputHelperFunctions(helperFunctions, metadataType->TypeNotUpper, fileName->Replace(".h",".cpp"), fileName, namespaceName);
				helperFunctions->Close();

				if(metadataType->ClassFactoryCodePath != NULL)
				{
					CErrorManager::GetErrorManager()->HandleInfo(S"Writing class factory functions...");
					fileName = metadataType->ClassFactoryCodePath->Substring(metadataType->ClassFactoryCodePath->LastIndexOf("\\")+1);
					fullCodePath = softRoot==NULL ? metadataType->ClassFactoryCodePath : String::Concat(softRoot, "\\", metadataType->ClassFactoryCodePath);
					StreamWriter *classFactory = new StreamWriter(fullCodePath);
					compiler->OutputClassFactory(classFactory, fileName, namespaceName);
					classFactory->Close();
				}
			}
		}
	}
}

int _tmain()
{
	String *projectFileName = NULL, *platformTag = NULL, *metadataName = NULL, *workingPath = NULL, *softRoot = NULL;
	String *namespaceName = "rage";
	bool xmlOutput = false, codeOutput = false, buildOutputPath = false;

	String *args __gc[] = Environment::GetCommandLineArgs();
	
	for(int i = 0 ; i < args->Count; i++)
	{
		if(args[i]->ToLower()->Equals("-project"))
		{
			projectFileName = args[i+1];
			i++;
		}
		else if(args[i]->ToLower()->Equals("-platform"))
		{
			platformTag = args[i+1];
			i++;
		}
		else if(args[i]->ToLower()->Equals("-metadata"))
		{
			metadataName = args[i+1];
			i++;
		}
		else if(args[i]->ToLower()->Equals("-xmloutput"))
		{
			xmlOutput = true;
		}
		else if(args[i]->ToLower()->Equals("-codeoutput"))
		{
			codeOutput = true;
		}
		else if(args[i]->ToLower()->Equals("-workingpath"))
		{
			workingPath = args[i+1];
			i++;
		}
		else if(args[i]->ToLower()->Equals("-buildoutpath"))
		{
			buildOutputPath = true;
		}
		else if(args[i]->ToLower()->Equals("-namespace"))
		{
			namespaceName = args[i+1];
			i++;
		}
		else if(args[i]->ToLower()->Equals("-softroot"))
		{
			softRoot = args[i+1];
			i++;
		}
	}

	CErrorManager::InitClass(xmlOutput);
	CErrorManager::GetErrorManager()->HandleInfo(S"RAGE Audio Metadata Compiler");
	
	CErrorManager::GetErrorManager()->HandleInfo(String::Format(S"RAGE {0}.{1} (release {2})", __box(RAGE_MAJOR_VERSION), __box(RAGE_MINOR_VERSION), __box(RAGE_RELEASE)));

	if(projectFileName == NULL || platformTag == NULL || metadataName == NULL ||
		workingPath == NULL)
	{
		CErrorManager::GetErrorManager()->HandleError("Usage: mc.exe -project <projectSettings.xml> -platform <platformTag> -metadata <MetadataName> -workingPath <workingpath> [-codeoutput] [-xmloutput]\n"
			"e.g.\n"
			"mc.exe -project jimmy\\audio\\dev\\projectSettings.xml -platform XENON -metadata Sounds -workingpath x:\\");
		return -1;
	}

	audProjectSettings *projectSettings = new audProjectSettings(String::Concat(workingPath,projectFileName));

	try
	{
		BuildProjectMetadata(workingPath, platformTag, projectSettings, metadataName, namespaceName, softRoot, codeOutput, buildOutputPath);
	}
	catch(Exception *ex)
	{
		CErrorManager::GetErrorManager()->HandleException(ex);
	}
	CErrorManager::GetErrorManager()->PrintSummary();
	CErrorManager::GetErrorManager()->ShutdownClass();
	return 0;
}
