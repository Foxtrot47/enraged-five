
using namespace System;
using namespace System::Collections;
using namespace rage;

const int g_WaveBankFileBufferBytes = 50*1024*1024;

ref class Bank
{
public:

	Bank(String ^bankPath);
	void LoadBank();
	void LoadStreamingBank();
	
	unsigned int GetNoOfWaves(void)
	{
		return m_NumWaves;
	};

	int GetBankSize(void)
	{
		return m_BankSize;
	};

	unsigned int GetCustomMetadataSize(void)
	{
		return m_CustomMetadataSize;
	}

	unsigned int GetWaveMetadataLookupSize(void)
	{
		return m_WaveMetaDataLookupSize;
	}

	unsigned int GetMetaDataSize(void)
	{
		return m_MetadataSize;
	}

	Hashtable ^GetWaves(void)
	{
		return m_Waves;
	}

private:
	int m_BankSize;
	unsigned int m_NumWaves;
	unsigned int m_MetadataSize;
	unsigned int m_WaveMetaDataLookupSize;
	unsigned int m_CustomMetadataSize;	
	Hashtable ^m_Waves;
	String ^m_Path;
};
