using namespace System;
using namespace System::Collections;
using namespace System::Windows::Forms;

ref class ListViewItemComparer : System::Collections::IComparer
    {
	public:
        
		ListViewItemComparer()
        {
            col = 0;
			order = ::SortOrder::Ascending;
        }
        
		ListViewItemComparer(int column, SortOrder order)
        {
            col = column;
            this->order = order;
        }
	
		virtual int Compare(Object ^x, Object ^y)
        {
            int returnVal = -1;
            try
            {
				unsigned int numX = UInt32::Parse(((ListViewItem^)x)->SubItems[col]->Text);
				unsigned int numY = UInt32::Parse(((ListViewItem^)y)->SubItems[col]->Text);

                if (numX == numY) { returnVal = 0; }
                else if (numX > numY) { returnVal = 1; }
                else { returnVal = -1; }
            }
            catch(Exception^)
            {

				returnVal = String::Compare(((ListViewItem^)x)->SubItems[col]->Text,
                                        ((ListViewItem^)y)->SubItems[col]->Text);
            }
            // Determine whether the sort order is descending.
			if (order == ::SortOrder::Descending)
            {
                // Invert the value returned by String.Compare.
                returnVal *= -1;
            }
            return returnVal;
        }

	private:
		int col;
        SortOrder order;
    };
