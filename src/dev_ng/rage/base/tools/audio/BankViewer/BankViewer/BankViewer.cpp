// BankViewer.cpp : main project file.

#include "stdafx.h"
#include "Form1.h"

using namespace BankViewer;

[STAThreadAttribute]
int main(array<System::String ^> ^args)
{	
	// Enabling Windows XP visual effects before any controls are created
	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false); 

	// Create the main window and run it
	if(args->Length == 0)
	{
		Application::Run(gcnew Form1());
	}
	else
	{
		Application::Run(gcnew Form1(args[0]));
	}

	return 0;
}

