#include "stdafx.h"
#include "Wave.h"
#include "audiohardware/wavedefs.h"
#include "audiosoundtypes/wavemetadatamarkerdefs.h"
#include "PlatformSpecific.h"

using namespace rage;

Wave::Wave()
{
	m_Size = 0;
	m_MetadataSize=0;
	m_CustomMetadata=nullptr;
	m_SampleData=nullptr;
}

unsigned int Wave::GetNameHash()
	{
		return m_NameHash;
	}

void Wave::SetNameHash(unsigned int nameHash)
	{
		m_NameHash = nameHash;
	}

void Wave::SetWaveName(String ^name)
	{
		m_Name = name;
	}

String^ Wave::GetWaveName()
	{
		return m_Name;
	}

unsigned int Wave::GetSize()
	{
		return m_Size;
	}

void Wave::SetSize(unsigned int size)
	{
		m_Size = size;
	}
	
void Wave::SetXmlNode(XmlNode ^node)
	{
		m_Node = node;
	}

void Wave::SetWaveSampleData(array<Byte> ^waveSampleData)
	{
		m_SampleData = waveSampleData;
	}

array<Byte> ^Wave::GetWaveSampleData()
{
	return m_SampleData;
}

void Wave::SetCustomMetadata(array<Byte> ^customMetadata)
	{
		m_CustomMetadata = customMetadata;
		ExtractCustomMetadata();
	}

unsigned int Wave::GetCustomMetadataSize()
	{
		if(m_CustomMetadata)
		{
			return m_CustomMetadata->Length;
		}
		else
		{
			return 0;
		}
	}

unsigned int Wave::GetMetadataSize()
	{
		return m_MetadataSize;
	}

void Wave::SetMetadataSize(unsigned int metadataSize)
	{
		m_MetadataSize = metadataSize;
	}

unsigned int Wave::GetSampleRate()
	{
		return m_SampleRate;
	}

void Wave::SetSampleRate(unsigned int sampleRate)
	{
		m_SampleRate = sampleRate;
	}

array<Marker ^> ^Wave::GetMarkers()
{
	return m_Markers;
}

array<Byte> ^Wave::GetVisemeData()
{
	return m_VisemeData;
}
unsigned int Wave::HashString(const char *string)
	{
		// This is the one-at-a-time hash from this page:
		// http://burtleburtle.net/bob/hash/doobs.html
		// (borrowed from /soft/swat/src/swcore/string2key.cpp)

		bool quotes = (*string == '\"');
		unsigned int key = 0;

		if (quotes) 
			string++;

		while(*string && (!quotes || *string != '\"'))
		{
			char character = *string++;
			if (character >= 'A' && character <= 'Z')
			{
				character += 'a'-'A';
			}
			else if (character == '\\')
			{
				character = '/';
			}

			key += character;
			key += (key << 10);
			key ^= (key >> 6);
		}

		key += (key << 3);
		key ^= (key >> 11);
		key += (key << 15);

		return key;

	}

void Wave::ExtractCustomMetadata()
{
	int index=0;
	while(index<m_CustomMetadata->Length)
	{
		array<Byte> ^waveCustomMetadataHeader = gcnew array<Byte>(sizeof(audCustomMetadataHeader));
		Array::Copy(m_CustomMetadata,index,waveCustomMetadataHeader,0,sizeof(audCustomMetadataHeader));
		pin_ptr<Byte> waveCustomMetadataHeaderPinned = &waveCustomMetadataHeader[0];
		audCustomMetadataHeader *header = (audCustomMetadataHeader*)waveCustomMetadataHeaderPinned;

		unsigned int dataType = (unsigned int)audPlatformSpecific::FixEndian(header->typeHash);
		unsigned int dataSize = (unsigned int)audPlatformSpecific::FixEndian(header->size);

		if(dataType.Equals(HashString(WaveMetadataMarkerType)))
		{
			unsigned int numMarkers = dataSize/sizeof(audWaveMetadataMarker);
			m_Markers = gcnew array<Marker ^>(numMarkers);
			for (unsigned int i=0;i<numMarkers;i++)
			{
				unsigned int markerOffset = index + sizeof(audCustomMetadataHeader)+(i*sizeof(audWaveMetadataMarker));
				array<Byte> ^markerBytes = gcnew array<Byte>(sizeof(audWaveMetadataMarker));
				Array::Copy(m_CustomMetadata,markerOffset,markerBytes,0,sizeof(audWaveMetadataMarker));
				pin_ptr<Byte> markerBytesPinned = &markerBytes[0];
				audWaveMetadataMarker *metadataMarker = (audWaveMetadataMarker*)markerBytesPinned;

				Marker ^marker = gcnew Marker();
				marker->CategoryHash = (unsigned int)audPlatformSpecific::FixEndian(metadataMarker->categoryHash);
				marker->Data = (unsigned int)audPlatformSpecific::FixEndian(metadataMarker->data);
				marker->NameHash = (unsigned int)audPlatformSpecific::FixEndian(metadataMarker->nameHash);
				marker->TimeOffset = (unsigned int)audPlatformSpecific::FixEndian(metadataMarker->timeOffset);
				marker->Value =  audPlatformSpecific::FixEndian(metadataMarker->value);
				m_Markers[i]=marker;

				metadataMarker=0;
			}
		}
		else if(dataType.Equals(HashString(WaveMetadataArbitraryDataType)))
		{
			unsigned int dataOffset = index+sizeof(audCustomMetadataHeader);
			array<Byte> ^arbitraryData = gcnew array<Byte>(dataSize);
			Array::Copy(m_CustomMetadata,dataOffset,arbitraryData,0,dataSize);
			m_VisemeData = arbitraryData;
			break;
		}

		index = sizeof(audCustomMetadataHeader)+dataSize;
	}

}
