#pragma once

#include "Bank.h"
#include "Wave.h"
#include "ListViewComparer.h"
#include "frmMarkers.h"


using namespace System::Xml;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::IO;
using namespace System::Drawing;
using namespace Microsoft::Win32;
using namespace audAssetManagement2;
using namespace ProjectLoader2;

namespace BankViewer {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Runtime::InteropServices;



	/// <summary>
	/// Summary for Form1
	///
	/// WARNING: If you change the name of this class, you will need to change the
	///          'Resource File Name' property for the managed resource compiler tool
	///          associated with all .resx files this class depends on.  Otherwise,
	///          the designers will not be able to interact properly with localized
	///          resources associated with this form.
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:

		Form1()
		{
			m_SortColumn = -1;
			InitializeComponent();
			if(!LoadSettings())
			{
				this->Close();
			}
		}

		Form1(String ^bankPath)
		{
			m_SortColumn = -1;
			InitializeComponent();
			if(!LoadSettings())
			{
				this->Close();
			}
			mnuReopenStreamingBank->Visible=true;
			mnuReopenBank->Visible = false;
			m_CurrentBankPath = bankPath->ToUpper();
			LoadBank();
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->m_ListView = (gcnew System::Windows::Forms::ListView());
			this->m_HashValue = (gcnew System::Windows::Forms::ColumnHeader());
			this->m_Name = (gcnew System::Windows::Forms::ColumnHeader());
			this->m_Size = (gcnew System::Windows::Forms::ColumnHeader());
			this->m_MetadataSize = (gcnew System::Windows::Forms::ColumnHeader());
			this->m_WaveCustomMetadata = (gcnew System::Windows::Forms::ColumnHeader());
			this->m_SampleRate = (gcnew System::Windows::Forms::ColumnHeader());
			this->m_ContextMenuStrip = (gcnew System::Windows::Forms::ContextMenuStrip(this->components));
			this->mnuShowMarkers = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->mnuSaveViseme = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolStrip1 = (gcnew System::Windows::Forms::ToolStrip());
			this->toolStripLabel1 = (gcnew System::Windows::Forms::ToolStripLabel());
			this->txtFind = (gcnew System::Windows::Forms::ToolStripTextBox());
			this->panel1 = (gcnew System::Windows::Forms::Panel());
			this->m_CustomMetadata = (gcnew System::Windows::Forms::Label());
			this->m_WaveMetadataLookup = (gcnew System::Windows::Forms::Label());
			this->m_Metadata = (gcnew System::Windows::Forms::Label());
			this->m_BankSize = (gcnew System::Windows::Forms::Label());
			this->m_NoOfWaves = (gcnew System::Windows::Forms::Label());
			this->m_BankPath = (gcnew System::Windows::Forms::Label());
			this->lblNoWaves = (gcnew System::Windows::Forms::Label());
			this->lblCustomMetadata = (gcnew System::Windows::Forms::Label());
			this->lblWaveMetadaLookup = (gcnew System::Windows::Forms::Label());
			this->lblMetadata = (gcnew System::Windows::Forms::Label());
			this->lblBankSize = (gcnew System::Windows::Forms::Label());
			this->lblBankPath = (gcnew System::Windows::Forms::Label());
			this->m_OpenFileDialog = (gcnew System::Windows::Forms::OpenFileDialog());
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->mnuFile = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->mnuOpen = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->mnuOpenBank = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->mnuOpenStreamingBank = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->mnuReopen = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->mnuReopenBank = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->mnuReopenStreamingBank = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->mnuExit = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->m_SaveFileDialog = (gcnew System::Windows::Forms::SaveFileDialog());
			this->m_ContextMenuStrip->SuspendLayout();
			this->toolStrip1->SuspendLayout();
			this->panel1->SuspendLayout();
			this->menuStrip1->SuspendLayout();
			this->SuspendLayout();
			// 
			// m_ListView
			// 
			this->m_ListView->AllowDrop = true;
			this->m_ListView->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
				| System::Windows::Forms::AnchorStyles::Left) 
				| System::Windows::Forms::AnchorStyles::Right));
			this->m_ListView->Columns->AddRange(gcnew cli::array< System::Windows::Forms::ColumnHeader^  >(6) {this->m_HashValue, this->m_Name, 
				this->m_Size, this->m_MetadataSize, this->m_WaveCustomMetadata, this->m_SampleRate});
			this->m_ListView->ContextMenuStrip = this->m_ContextMenuStrip;
			this->m_ListView->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->m_ListView->FullRowSelect = true;
			this->m_ListView->Location = System::Drawing::Point(0, 48);
			this->m_ListView->Name = L"m_ListView";
			this->m_ListView->Size = System::Drawing::Size(823, 342);
			this->m_ListView->TabIndex = 2;
			this->m_ListView->UseCompatibleStateImageBehavior = false;
			this->m_ListView->View = System::Windows::Forms::View::Details;
			this->m_ListView->ColumnClick += gcnew System::Windows::Forms::ColumnClickEventHandler(this, &Form1::m_ListView_OnColumnClick);
			this->m_ListView->ItemDrag += gcnew System::Windows::Forms::ItemDragEventHandler(this, &Form1::m_ListView_OnItemDrag);
			// 
			// m_HashValue
			// 
			this->m_HashValue->Text = L"Hash Value";
			this->m_HashValue->Width = 133;
			// 
			// m_Name
			// 
			this->m_Name->Text = L"Name";
			this->m_Name->Width = 221;
			// 
			// m_Size
			// 
			this->m_Size->Text = L"Size";
			this->m_Size->Width = 74;
			// 
			// m_MetadataSize
			// 
			this->m_MetadataSize->Text = L"Metadata Size";
			this->m_MetadataSize->Width = 115;
			// 
			// m_WaveCustomMetadata
			// 
			this->m_WaveCustomMetadata->Text = L"Custom Metadata";
			this->m_WaveCustomMetadata->Width = 125;
			// 
			// m_SampleRate
			// 
			this->m_SampleRate->Text = L"Sample Rate";
			this->m_SampleRate->Width = 105;
			// 
			// m_ContextMenuStrip
			// 
			this->m_ContextMenuStrip->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->mnuShowMarkers, 
				this->mnuSaveViseme});
			this->m_ContextMenuStrip->Name = L"m_ContextMenuStrip";
			this->m_ContextMenuStrip->Size = System::Drawing::Size(172, 48);
			// 
			// mnuShowMarkers
			// 
			this->mnuShowMarkers->Name = L"mnuShowMarkers";
			this->mnuShowMarkers->Size = System::Drawing::Size(171, 22);
			this->mnuShowMarkers->Text = L"Display Markers";
			this->mnuShowMarkers->Click += gcnew System::EventHandler(this, &Form1::mnuShowMarkers_OnClick);
			// 
			// mnuSaveViseme
			// 
			this->mnuSaveViseme->Name = L"mnuSaveViseme";
			this->mnuSaveViseme->Size = System::Drawing::Size(171, 22);
			this->mnuSaveViseme->Text = L"Save Viseme Data";
			this->mnuSaveViseme->Click += gcnew System::EventHandler(this, &Form1::mnuSaveViseme_OnClick);
			// 
			// toolStrip1
			// 
			this->toolStrip1->GripStyle = System::Windows::Forms::ToolStripGripStyle::Hidden;
			this->toolStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->toolStripLabel1, 
				this->txtFind});
			this->toolStrip1->Location = System::Drawing::Point(0, 24);
			this->toolStrip1->Name = L"toolStrip1";
			this->toolStrip1->RenderMode = System::Windows::Forms::ToolStripRenderMode::System;
			this->toolStrip1->Size = System::Drawing::Size(823, 25);
			this->toolStrip1->TabIndex = 3;
			this->toolStrip1->Text = L"toolStrip1";
			// 
			// toolStripLabel1
			// 
			this->toolStripLabel1->Name = L"toolStripLabel1";
			this->toolStripLabel1->Size = System::Drawing::Size(31, 22);
			this->toolStripLabel1->Text = L"Find:";
			// 
			// txtFind
			// 
			this->txtFind->Name = L"txtFind";
			this->txtFind->Size = System::Drawing::Size(100, 25);
			this->txtFind->TextChanged += gcnew System::EventHandler(this, &Form1::On_TextChanged);
			// 
			// panel1
			// 
			this->panel1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left) 
				| System::Windows::Forms::AnchorStyles::Right));
			this->panel1->Controls->Add(this->m_CustomMetadata);
			this->panel1->Controls->Add(this->m_WaveMetadataLookup);
			this->panel1->Controls->Add(this->m_Metadata);
			this->panel1->Controls->Add(this->m_BankSize);
			this->panel1->Controls->Add(this->m_NoOfWaves);
			this->panel1->Controls->Add(this->m_BankPath);
			this->panel1->Controls->Add(this->lblNoWaves);
			this->panel1->Controls->Add(this->lblCustomMetadata);
			this->panel1->Controls->Add(this->lblWaveMetadaLookup);
			this->panel1->Controls->Add(this->lblMetadata);
			this->panel1->Controls->Add(this->lblBankSize);
			this->panel1->Controls->Add(this->lblBankPath);
			this->panel1->Location = System::Drawing::Point(0, 391);
			this->panel1->Name = L"panel1";
			this->panel1->Size = System::Drawing::Size(823, 135);
			this->panel1->TabIndex = 4;
			// 
			// m_CustomMetadata
			// 
			this->m_CustomMetadata->AutoSize = true;
			this->m_CustomMetadata->Location = System::Drawing::Point(153, 116);
			this->m_CustomMetadata->Name = L"m_CustomMetadata";
			this->m_CustomMetadata->Size = System::Drawing::Size(0, 13);
			this->m_CustomMetadata->TabIndex = 11;
			// 
			// m_WaveMetadataLookup
			// 
			this->m_WaveMetadataLookup->AutoSize = true;
			this->m_WaveMetadataLookup->Location = System::Drawing::Point(157, 94);
			this->m_WaveMetadataLookup->Name = L"m_WaveMetadataLookup";
			this->m_WaveMetadataLookup->Size = System::Drawing::Size(0, 13);
			this->m_WaveMetadataLookup->TabIndex = 10;
			// 
			// m_Metadata
			// 
			this->m_Metadata->AutoSize = true;
			this->m_Metadata->Location = System::Drawing::Point(135, 72);
			this->m_Metadata->Name = L"m_Metadata";
			this->m_Metadata->Size = System::Drawing::Size(0, 13);
			this->m_Metadata->TabIndex = 9;
			// 
			// m_BankSize
			// 
			this->m_BankSize->AutoSize = true;
			this->m_BankSize->Location = System::Drawing::Point(78, 49);
			this->m_BankSize->Name = L"m_BankSize";
			this->m_BankSize->Size = System::Drawing::Size(0, 13);
			this->m_BankSize->TabIndex = 8;
			// 
			// m_NoOfWaves
			// 
			this->m_NoOfWaves->AutoSize = true;
			this->m_NoOfWaves->Location = System::Drawing::Point(100, 27);
			this->m_NoOfWaves->Name = L"m_NoOfWaves";
			this->m_NoOfWaves->Size = System::Drawing::Size(0, 13);
			this->m_NoOfWaves->TabIndex = 7;
			// 
			// m_BankPath
			// 
			this->m_BankPath->AutoSize = true;
			this->m_BankPath->Location = System::Drawing::Point(50, 6);
			this->m_BankPath->Name = L"m_BankPath";
			this->m_BankPath->Size = System::Drawing::Size(0, 13);
			this->m_BankPath->TabIndex = 6;
			// 
			// lblNoWaves
			// 
			this->lblNoWaves->AutoSize = true;
			this->lblNoWaves->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->lblNoWaves->Location = System::Drawing::Point(13, 27);
			this->lblNoWaves->Name = L"lblNoWaves";
			this->lblNoWaves->Size = System::Drawing::Size(89, 13);
			this->lblNoWaves->TabIndex = 5;
			this->lblNoWaves->Text = L"No. of Waves:";
			// 
			// lblCustomMetadata
			// 
			this->lblCustomMetadata->AutoSize = true;
			this->lblCustomMetadata->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->lblCustomMetadata->Location = System::Drawing::Point(13, 116);
			this->lblCustomMetadata->Name = L"lblCustomMetadata";
			this->lblCustomMetadata->Size = System::Drawing::Size(142, 13);
			this->lblCustomMetadata->TabIndex = 4;
			this->lblCustomMetadata->Text = L"Bank Custom Metadata:";
			// 
			// lblWaveMetadaLookup
			// 
			this->lblWaveMetadaLookup->AutoSize = true;
			this->lblWaveMetadaLookup->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, 
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			this->lblWaveMetadaLookup->Location = System::Drawing::Point(12, 94);
			this->lblWaveMetadaLookup->Name = L"lblWaveMetadaLookup";
			this->lblWaveMetadaLookup->Size = System::Drawing::Size(147, 13);
			this->lblWaveMetadaLookup->TabIndex = 3;
			this->lblWaveMetadaLookup->Text = L"Wave Metadata Lookup:";
			// 
			// lblMetadata
			// 
			this->lblMetadata->AutoSize = true;
			this->lblMetadata->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->lblMetadata->Location = System::Drawing::Point(12, 72);
			this->lblMetadata->Name = L"lblMetadata";
			this->lblMetadata->Size = System::Drawing::Size(125, 13);
			this->lblMetadata->TabIndex = 2;
			this->lblMetadata->Text = L"Bank Metadata Size:";
			// 
			// lblBankSize
			// 
			this->lblBankSize->AutoSize = true;
			this->lblBankSize->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->lblBankSize->Location = System::Drawing::Point(12, 49);
			this->lblBankSize->Name = L"lblBankSize";
			this->lblBankSize->Size = System::Drawing::Size(68, 13);
			this->lblBankSize->TabIndex = 1;
			this->lblBankSize->Text = L"Bank Size:";
			// 
			// lblBankPath
			// 
			this->lblBankPath->AutoEllipsis = true;
			this->lblBankPath->AutoSize = true;
			this->lblBankPath->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->lblBankPath->Location = System::Drawing::Point(12, 6);
			this->lblBankPath->Name = L"lblBankPath";
			this->lblBankPath->Size = System::Drawing::Size(44, 13);
			this->lblBankPath->TabIndex = 0;
			this->lblBankPath->Text = L"Bank: ";
			// 
			// m_OpenFileDialog
			// 
			this->m_OpenFileDialog->FileName = L"openFileDialog1";
			// 
			// menuStrip1
			// 
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) {this->mnuFile});
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->RenderMode = System::Windows::Forms::ToolStripRenderMode::System;
			this->menuStrip1->Size = System::Drawing::Size(823, 24);
			this->menuStrip1->TabIndex = 5;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// mnuFile
			// 
			this->mnuFile->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {this->mnuOpen, this->mnuReopen, 
				this->mnuExit});
			this->mnuFile->Name = L"mnuFile";
			this->mnuFile->Size = System::Drawing::Size(35, 20);
			this->mnuFile->Text = L"File";
			this->mnuFile->Click += gcnew System::EventHandler(this, &Form1::mnuFile_OnClick);
			// 
			// mnuOpen
			// 
			this->mnuOpen->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->mnuOpenBank, 
				this->mnuOpenStreamingBank});
			this->mnuOpen->Name = L"mnuOpen";
			this->mnuOpen->Size = System::Drawing::Size(126, 22);
			this->mnuOpen->Text = L"Open";
			// 
			// mnuOpenBank
			// 
			this->mnuOpenBank->Name = L"mnuOpenBank";
			this->mnuOpenBank->Size = System::Drawing::Size(159, 22);
			this->mnuOpenBank->Text = L"Normal Bank";
			this->mnuOpenBank->Click += gcnew System::EventHandler(this, &Form1::mnuOpenBank_Click);
			// 
			// mnuOpenStreamingBank
			// 
			this->mnuOpenStreamingBank->Name = L"mnuOpenStreamingBank";
			this->mnuOpenStreamingBank->Size = System::Drawing::Size(159, 22);
			this->mnuOpenStreamingBank->Text = L"Streaming Bank";
			this->mnuOpenStreamingBank->Click += gcnew System::EventHandler(this, &Form1::mnuOpenStreamingBank_Click);
			// 
			// mnuReopen
			// 
			this->mnuReopen->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->mnuReopenBank, 
				this->mnuReopenStreamingBank});
			this->mnuReopen->Name = L"mnuReopen";
			this->mnuReopen->Size = System::Drawing::Size(152, 22);
			this->mnuReopen->Text = L"Re-open";
			this->mnuReopen->Visible = false;
			this->mnuReopen->MouseMove += gcnew System::Windows::Forms::MouseEventHandler(this, &Form1::mnuReOpen_OnMouseMove);
			// 
			// mnuReopenBank
			// 
			this->mnuReopenBank->Name = L"mnuReopenBank";
			this->mnuReopenBank->Size = System::Drawing::Size(174, 22);
			this->mnuReopenBank->Text = L"As Normal Bank";
			this->mnuReopenBank->Click += gcnew System::EventHandler(this, &Form1::mnuReopenBank_Click);
			// 
			// mnuReopenStreamingBank
			// 
			this->mnuReopenStreamingBank->Name = L"mnuReopenStreamingBank";
			this->mnuReopenStreamingBank->Size = System::Drawing::Size(174, 22);
			this->mnuReopenStreamingBank->Text = L"As Streaming Bank";
			this->mnuReopenStreamingBank->Click += gcnew System::EventHandler(this, &Form1::mnuReopernStreamingBank_Click);
			// 
			// mnuExit
			// 
			this->mnuExit->Name = L"mnuExit";
			this->mnuExit->Size = System::Drawing::Size(126, 22);
			this->mnuExit->Text = L"Exit";
			this->mnuExit->Click += gcnew System::EventHandler(this, &Form1::mnuExit_OnClick);
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(823, 523);
			this->Controls->Add(this->panel1);
			this->Controls->Add(this->toolStrip1);
			this->Controls->Add(this->menuStrip1);
			this->Controls->Add(this->m_ListView);
			this->MainMenuStrip = this->menuStrip1;
			this->Name = L"Form1";
			this->Text = L"Bank Viewer";
			this->m_ContextMenuStrip->ResumeLayout(false);
			this->toolStrip1->ResumeLayout(false);
			this->toolStrip1->PerformLayout();
			this->panel1->ResumeLayout(false);
			this->panel1->PerformLayout();
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

	private: static FrmMarkers ^markerList = gcnew FrmMarkers();
	private: System::Windows::Forms::ListView^  m_ListView;
	private: System::Windows::Forms::ColumnHeader^  m_HashValue;
	private: System::Windows::Forms::ToolStrip^  toolStrip1;
	private: System::Windows::Forms::ToolStripLabel^  toolStripLabel1;
	private: System::Windows::Forms::ToolStripTextBox^  txtFind;
	private: System::Windows::Forms::ColumnHeader^  m_Size;
	private: System::Windows::Forms::ColumnHeader^  m_MetadataSize;
	private: System::Windows::Forms::ColumnHeader^  m_SampleRate;
	private: System::Windows::Forms::Panel^  panel1;
	private: System::Windows::Forms::Label^  lblBankPath;
	private: System::Windows::Forms::Label^  lblCustomMetadata;
	private: System::Windows::Forms::Label^  lblWaveMetadaLookup;
	private: System::Windows::Forms::Label^  lblMetadata;
	private: System::Windows::Forms::Label^  lblBankSize;
	private: System::Windows::Forms::Label^  lblNoWaves;
	private: System::Windows::Forms::Label^  m_BankPath;
	private: System::Windows::Forms::Label^  m_CustomMetadata;
	private: System::Windows::Forms::Label^  m_WaveMetadataLookup;
	private: System::Windows::Forms::Label^  m_Metadata;
	private: System::Windows::Forms::Label^  m_BankSize;
	private: System::Windows::Forms::Label^  m_NoOfWaves;
	private: System::Windows::Forms::ColumnHeader^  m_WaveCustomMetadata;			 
	private: System::Windows::Forms::ColumnHeader^  m_Name;
	private: System::Windows::Forms::OpenFileDialog^  m_OpenFileDialog;
	private: System::Windows::Forms::MenuStrip^  menuStrip1;
	private: System::Windows::Forms::ToolStripMenuItem^  mnuFile;

	private: System::Windows::Forms::ToolStripMenuItem^  mnuOpen;
	private: System::Windows::Forms::ToolStripMenuItem^  mnuExit;
	private: System::Windows::Forms::ToolStripMenuItem^  mnuOpenBank;
	private: System::Windows::Forms::ToolStripMenuItem^  mnuOpenStreamingBank;
	private: System::Windows::Forms::ToolStripMenuItem^  mnuReopen;
	private: System::Windows::Forms::ToolStripMenuItem^  mnuReopenBank;
	private: System::Windows::Forms::ToolStripMenuItem^  mnuReopenStreamingBank;
	private: System::Windows::Forms::ContextMenuStrip^  m_ContextMenuStrip;
	private: System::Windows::Forms::ToolStripMenuItem^  mnuShowMarkers;
	private: System::Windows::Forms::ToolStripMenuItem^  mnuSaveViseme;
	private: System::Windows::Forms::SaveFileDialog^  m_SaveFileDialog;
	private: System::ComponentModel::IContainer^  components;

			 Bank ^m_CurrentBank;
			 String ^m_CurrentBankPath;
			 bool bIsStreamingBank;
			 Hashtable ^m_HashStrings; 

			 audProjectSettings ^m_ProjectSettings;
			 assetManager ^m_AssetManager;

			 bool LoadSettings()
			 {
				 frmProjectLoader^ projectLoader = gcnew frmProjectLoader("ProjectList.xml");
				 projectLoader->ShowDialog();
				 audProjectEntry^ projectEntry = projectLoader->Project;
				 m_AssetManager = projectLoader->AssetManager;	
					try
					{
						String^ projectSettingsPath = m_AssetManager->GetWorkingPath(projectEntry->ProjectSettings);
						m_AssetManager->GetLatest(projectSettingsPath,false);
						m_ProjectSettings = gcnew audProjectSettings(projectSettingsPath);						
					}
					catch(Exception^ ex)
					{
						MessageBox::Show(ex->Message);
						return false;
					}
					return true;
			 }

			 void LoadXml()
			 {
				 m_HashStrings = gcnew Hashtable();
				 // build up hash table of wave names and hash values

				 //Get BuildInfo Path 
				 String ^fileName = nullptr;
				 for(int i=0;i<m_ProjectSettings->GetPlatformSettings()->Count;i++)
				 {
					 PlatformSetting ^ps = (PlatformSetting ^)m_ProjectSettings->GetPlatformSettings()[i];
					 if(m_CurrentBankPath->ToUpper()->Contains(ps->PlatformTag->ToUpper()))
					 {
						 fileName = String::Concat(ps->BuildInfo,"BuiltWaves.XML");
						 break;						
					 }
				 }
				 //no file name found
				 if(fileName == nullptr)
				 {
					 throw gcnew Exception("Could not find output path");
				 }

				 //Grab latest Built waves
				 String ^path = m_AssetManager->GetWorkingPath(fileName);
				 m_AssetManager->GetLatest(path, false);
				 XmlDocument ^doc = gcnew XmlDocument();
				 doc->Load(path);

				 int i = m_CurrentBankPath->IndexOf("SFX");
				 if(i==-1)
				 {
					 throw gcnew Exception("Could not find SFX in bank folder path");
				 }

				 String ^Bankpath = m_CurrentBankPath->Substring(i+4);
				 array<String ^> ^elements = Bankpath->Split('\\');
				 GetPack(elements[0],elements[elements->Length-1],doc->DocumentElement);
			 }

			 void LoadBank()
			 {
				 m_CurrentBank = gcnew Bank(m_CurrentBankPath);
				 ReloadBank();
			 }

			 void ReloadBank()
			 {
				 if(bIsStreamingBank)
				 {
					 m_CurrentBank->LoadStreamingBank();
				 }
				 else
				 {
					 m_CurrentBank->LoadBank();
				 }
				 m_BankSize->Text = m_CurrentBank->GetBankSize().ToString();
				 m_NoOfWaves->Text = m_CurrentBank->GetNoOfWaves().ToString();
				 m_BankPath->Text = m_CurrentBankPath;
				 m_CustomMetadata->Text = m_CurrentBank->GetCustomMetadataSize().ToString();
				 m_Metadata->Text = m_CurrentBank->GetMetaDataSize().ToString();
				 m_WaveMetadataLookup->Text = m_CurrentBank->GetWaveMetadataLookupSize().ToString();

				 LoadXml();
				 LoadListView();
			 }

			 void LoadListView(void)
			 {
				 Hashtable ^waves = m_CurrentBank->GetWaves();
				 IEnumerator ^Key = waves->Keys->GetEnumerator();
				 m_ListView->Items->Clear();
				 while(Key->MoveNext())
				 {
					 Wave ^wave = (Wave^)waves[Key->Current];
					 ListViewItem ^li = gcnew ListViewItem(wave->GetNameHash().ToString());
					 if(m_HashStrings->ContainsKey(wave->GetNameHash().ToString()))
					 {
						 li->SubItems->Add(m_HashStrings[wave->GetNameHash().ToString()]->ToString());
					 }
					 else
					 {
						 li->SubItems->Add("NA");
					 }
					 li->SubItems->Add(wave->GetSize().ToString());				
					 li->SubItems->Add(wave->GetMetadataSize().ToString());
					 li->SubItems->Add(wave->GetCustomMetadataSize().ToString());
					 li->SubItems->Add(wave->GetSampleRate().ToString());
					 if(wave->GetMarkers())
					 {
						 li->ForeColor = Color::Red;
					 }
					 m_ListView->Items->Add(li);
				 }
			 }

			 void GetPack(String ^pack, String ^bank, XmlNode ^n)
			 {				
				 XmlNode ^child = n->FirstChild;

				 while(child)
				 {
					 if(child->Attributes["name"] && child->Attributes["name"]->Value->Equals(pack)
						 && child->Name->Equals("Pack"))
					 {					
						 GetBank(bank,child);						

						 break;
					 }

					 child = child->NextSibling;
				 }

			 }

			 void GetBank(String ^name,XmlNode ^node)
			 {
				 XmlNode ^child = node->FirstChild;
				 while(child)
				 {
					 if(child->Name->Equals("Bank"))
					 {
						 if(child->Attributes["name"] && child->Attributes["name"]->Value->Equals(name))
						 {					
							 GetWaves(child);						

							 break;
						 }
					 }
					 else
					 {
						 GetBank(name,child);
					 }
					 child = child->NextSibling;
				 }
			 }

			 void GetWaves(XmlNode ^node)
			 {
				 XmlNode ^child = node->FirstChild;
				 while(child)
				 {
					 if(child->Name->Equals("Wave"))
					 {
						 XmlNode ^tag = child->FirstChild;
						 bool hasRename = false;
						 while(tag)
						 {
							 if(tag->Attributes["name"] && tag->Attributes["name"]->Value->Equals("rename")
								 && tag->Attributes["value"])
							 {
								 m_HashStrings->Add(child->Attributes["nameHash"]->Value,tag->Attributes["value"]->Value);
								 hasRename=true;
								 break;
							 }

							 tag= tag->NextSibling;
						 }

						 if(!hasRename)
						 {
							 m_HashStrings->Add(child->Attributes["nameHash"]->Value,child->Attributes["name"]->Value);
						 }
					 }
					 else
					 {
						 GetWaves(child);
					 }
					 child = child->NextSibling;
				 }
			 }




	private: System::Void On_TextChanged(System::Object^  sender, System::EventArgs^  e) {

				 const char* str = (const char*)(Marshal::StringToHGlobalAnsi(txtFind->Text)).ToPointer();
				 String ^hashValue = HashString(str).ToString();

				 for(int i=0; i<m_ListView->Items->Count;i++)
				 {
					 if(m_ListView->Items[i]->Text->Equals(hashValue))
					 {
						 //hash values should be unique
						 m_ListView->Items[i]->Selected=true;
						 m_ListView->Items[i]->EnsureVisible();
					 }
					 else
					 {
						 m_ListView->Items[i]->Selected=false;
					 }
				 }
			 }


	private:
		//Stolen from Rage::atStringHash constructor.
		unsigned int HashString(const char *string)
		{
			// This is the one-at-a-time hash from this page:
			// http://burtleburtle.net/bob/hash/doobs.html
			// (borrowed from /soft/swat/src/swcore/string2key.cpp)

			bool quotes = (*string == '\"');
			unsigned int key = 0;

			if (quotes) 
				string++;

			while(*string && (!quotes || *string != '\"'))
			{
				char character = *string++;
				if (character >= 'A' && character <= 'Z')
				{
					character += 'a'-'A';
				}
				else if (character == '\\')
				{
					character = '/';
				}

				key += character;
				key += (key << 10);
				key ^= (key >> 6);
			}

			key += (key << 3);
			key ^= (key >> 11);
			key += (key << 15);

			return key;

		}

	private: System::Void mnuExit_OnClick(System::Object^  sender, System::EventArgs^  e) {
				 this->Close();
			 }

			 //PURPOSE: Allows users to drag the wave sample data to explorer
	private: System::Void m_ListView_OnItemDrag(System::Object^  sender, System::Windows::Forms::ItemDragEventArgs^  e) {
				 if(!bIsStreamingBank){
					 //Get list of all selected files
					 IEnumerator ^items =  m_ListView->SelectedItems->GetEnumerator();
					 array<String ^> ^aFiles = gcnew array<String ^>(m_ListView->SelectedItems->Count);

					 int i=0;
					 //cycle through files creating an appropriately named .bin file for each wave sample data
					 //these files are placed in a temporary directory for now
					 while(items->MoveNext())
					 {

						 ListViewItem ^lvi = (ListViewItem^)items->Current;
						 String ^path = Path::GetTempPath() + lvi->SubItems[1]->Text;
						 Hashtable ^waves = m_CurrentBank->GetWaves();
						 //look up hashtable using hash value as key
						 Wave ^selectedWave = (Wave ^)waves[lvi->Text];

						 if(selectedWave && selectedWave->GetWaveSampleData()){
							 //retrieve sample data and write to file
							 path = path->ToLower()->Replace(".wav",".bin");
							 FileStream ^fs = gcnew FileStream(path,FileMode::Create,FileAccess::Write);
							 BinaryWriter ^bw = gcnew BinaryWriter(fs);
							 bw->Write(selectedWave->GetWaveSampleData());
							 bw->Flush();
							 bw->Close();
							 fs->Close();

							 aFiles[i]=path;
						 }
						 i++;
					 }
					 DataObject ^da = gcnew DataObject();
					 da->SetData(::DataFormats::FileDrop,aFiles);
					 //this will move the .bin files from the temp directory to the selected directory
					 m_ListView->DoDragDrop(da, DragDropEffects::Move);

				 }
			 }


	private: System::Void mnuOpenBank_Click(System::Object^  sender, System::EventArgs^  e) {
				 m_OpenFileDialog->FileName="";
				 if(m_OpenFileDialog->ShowDialog() == ::DialogResult::OK)
				 {
					 bIsStreamingBank = false;
					 m_CurrentBankPath = m_OpenFileDialog->FileName->ToUpper();
					 m_AssetManager->GetLatest(m_CurrentBankPath, false);
					 LoadBank();
				 }
			 }
	private: System::Void mnuOpenStreamingBank_Click(System::Object^  sender, System::EventArgs^  e) {
				 if(m_OpenFileDialog->ShowDialog() == ::DialogResult::OK)
				 {
					 bIsStreamingBank = true;
					 m_CurrentBankPath = m_OpenFileDialog->FileName->ToUpper();
					 m_AssetManager->GetLatest(m_CurrentBankPath, false);
					 LoadBank();
				 }
			 }
	private: System::Void mnuReopenBank_Click(System::Object^  sender, System::EventArgs^  e) {
				 bIsStreamingBank = false;
				 ReloadBank();
			 }
	private: System::Void mnuReopernStreamingBank_Click(System::Object^  sender, System::EventArgs^  e) {
				 bIsStreamingBank = true;
				 ReloadBank();
			 }
	private: System::Void mnuFile_OnClick(System::Object^  sender, System::EventArgs^  e) {
				 if(!m_CurrentBankPath)
				 {
					 mnuReopen->Visible=false;
				 }
				 else
				 {
					 mnuReopen->Visible=true;
				 }
			 }

	private: int m_SortColumn;
	private: System::Void m_ListView_OnColumnClick(System::Object^  sender, System::Windows::Forms::ColumnClickEventArgs^  e) {
				 // Determine whether the column is the same as the last column clicked.
				 if (e->Column != m_SortColumn)
				 {
					 // Set the sort column to the new column.
					 m_SortColumn = e->Column;
					 // Set the sort order to ascending by default.
					 m_ListView->Sorting = ::SortOrder::Ascending;
				 }
				 else
				 {
					 // Determine what the last sort order was and change it.
					 if (m_ListView->Sorting == ::SortOrder::Ascending)
						 m_ListView->Sorting = ::SortOrder::Descending;
					 else
						 m_ListView->Sorting = ::SortOrder::Ascending;
				 }

				 // Call the sort method to manually sort.
				 m_ListView->BeginUpdate();
				 m_ListView->Cursor  = ::Cursors::WaitCursor;
				 m_ListView->Sort();
				 m_ListView->EndUpdate();
				 m_ListView->Cursor = ::Cursors::Default;
				 // Set the ListViewItemSorter property to a new ListViewItemComparer object.
				 m_ListView->ListViewItemSorter = gcnew ListViewItemComparer(e->Column, m_ListView->Sorting);

			 }


	private: System::Void mnuShowMarkers_OnClick(System::Object^  sender, System::EventArgs^  e) {
				 ListViewItem ^lvi = m_ListView->SelectedItems[0];
				 Wave ^wave = (Wave ^)m_CurrentBank->GetWaves()[lvi->Text];
				 if(wave->GetMarkers())
				 {
					 markerList->Init(wave->GetMarkers());
					 markerList->Show();
				 }

			 }
	private: System::Void mnuSaveViseme_OnClick(System::Object^  sender, System::EventArgs^  e) {
				 ListViewItem ^lvi = m_ListView->SelectedItems[0];
				 Wave ^wave = (Wave ^)m_CurrentBank->GetWaves()[lvi->Text];

				 m_SaveFileDialog->FileName = lvi->SubItems[1]->Text->Replace(".WAV","");

				 if(wave->GetVisemeData()!=nullptr)
				 {
					 if(m_CurrentBankPath->ToUpper()->Contains("XENON"))
					 {
						 m_SaveFileDialog->FileName +=".xnim";
						 m_SaveFileDialog->Filter = "Xenon Viseme (*.xnim)|*xnim|All files (*.*)|*.*";
						 m_SaveFileDialog->FilterIndex = 1 ;
					 }
					 else if(m_CurrentBankPath->ToUpper()->Contains("PS3"))
					 {
						 m_SaveFileDialog->FileName +=".cnim";
						 m_SaveFileDialog->Filter = "PS3 Viseme (*.cnim)|*cnim|All files (*.*)|*.*";
						 m_SaveFileDialog->FilterIndex = 1 ;
					 }
					 else if(m_CurrentBankPath->ToUpper()->Contains("PC"))
					 {
						 m_SaveFileDialog->Filter = "PC Viseme All files (*.*)|*.*";
						 m_SaveFileDialog->FilterIndex = 1 ;
					 }
				 }	

				 if(m_SaveFileDialog->ShowDialog() == ::DialogResult::OK)
				 {
					 Stream ^stream ;
					 if((stream = m_SaveFileDialog->OpenFile())!=nullptr)
					 {
						 BinaryWriter ^bw = gcnew BinaryWriter(stream);
						 bw->Write(wave->GetVisemeData());
						 bw->Flush();
						 bw->Close();
						 stream->Close();
					 }
				 }
			 }


	private: System::Void mnuReOpen_OnMouseMove(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
				 if(bIsStreamingBank)
				 {
					 mnuReopenStreamingBank->Visible=false;
					 mnuReopenBank->Visible = true;
				 }
				 else
				 {
					 mnuReopenStreamingBank->Visible=true;
					 mnuReopenBank->Visible = false;
				 }
			 }
	};
}

