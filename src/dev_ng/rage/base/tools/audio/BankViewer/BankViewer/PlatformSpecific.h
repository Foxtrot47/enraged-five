#ifndef PLATFORM_SPECIFIC_H
#define PLATFORM_SPECIFIC_H

using namespace System;


//
// PURPOSE
//  A utility class containing types and helper functions for target build platforms.
//
public ref class audPlatformSpecific
{
public:
	//
	// PURPOSE
	//  Defines the target build platforms supported by the Wave Builder. These constants are used by the member
	//	functions to provide platform-specific assistance.
	// SEE ALSO
	//  InitClass
	//
	enum class audBuildPlatforms
	{
		BUILD_PLATFORM_PC,
		BUILD_PLATFORM_XENON,
		BUILD_PLATFORM_PS3,
		//Add new platforms here.
		NUM_BUILD_PLATFORMS
	};

	//
	// PURPOSE
	//  Stores the client's target build platform and populates internal data structures.
	// PARAMS
	//  buildPlatform	- The client's target build platform for which assistance is required.
	// SEE ALSO
	//  audBuildPlatforms
	//
	static void InitClass(String ^buildPlatform);
	//
	// PURPOSE
	//  An accessor function for the current build platform.
	// RETURNS
	//  Returns the build platform.
	//
	static audBuildPlatforms GetBuildPlatform(void)
	{
		return sm_BuildPlatform;
	}
	//
	// PURPOSE
	//  Determines whether or not the build platform is big endian.
	// RETURNS
	//  Returns true if the build platform is big endian.
	//
	static bool IsPlatformBigEndian(void)
	{
		return sm_BigEndianPlatforms[(int)sm_BuildPlatform];
	}
	//
	// PURPOSE
	//  Automatically fixes the endianness of an item of 16 bit data for the client's target build platform.
	// PARAMS
	//	value	- The 16-bit data to be endian-fixed.
	// RETURNS
	//	The endian-fixed data.
	//
	static UInt16 FixEndian(UInt16 value);
	//
	// PURPOSE
	//  Automatically fixes the endianness of an item of 32 bit data for the client's target build platform.
	// PARAMS
	//	value	- The 32-bit data to be endian-fixed.
	// RETURNS
	//	The endian-fixed data.
	//
	static UInt32 FixEndian(UInt32 value);
	//
	// PURPOSE
	//  Automatically fixes the endianness of an item of 32 bit floating-point data for the client's target build
	//	platform.
	// PARAMS
	//	value	- The 32-bit floating-point data to be endian-fixed.
	// RETURNS
	//	The endian-fixed data.
	//
	static Single FixEndian(Single value);
	//
	// PURPOSE
	//  Automatically fixes the endianness of an item of 64 bit data for the client's target build platform.
	// PARAMS
	//	value	- The 64-bit data to be endian-fixed.
	// RETURNS
	//	The endian-fixed data.
	//
	static unsigned __int64 FixEndian(unsigned __int64 value);
	//
	// PURPOSE
	//  Flips the endianness of an item of 16 bit data.
	// PARAMS
	//	value	- The 16-bit data to be endian-flipped.
	// RETURNS
	//	The endian-flipped data.
	//
	static UInt16 FlipEndian(UInt16 value);
	//
	// PURPOSE
	//  Flips the endianness of an item of 32 bit data.
	// PARAMS
	//	value	- The 32-bit data to be endian-flipped.
	// RETURNS
	//	The endian-flipped data.
	//
	static UInt32 FlipEndian(UInt32 value);
	//
	// PURPOSE
	//  Flips the endianness of an item of 32 bit floating-point data.
	// PARAMS
	//	value	- The 32-bit floating-point data to be endian-flipped.
	// RETURNS
	//	The endian-flipped data.
	//
	static Single audPlatformSpecific::FlipEndian(Single value);
	//
	// PURPOSE
	//  Flips the endianness of an item of 64 bit data.
	// PARAMS
	//	value	- The 64-bit data to be endian-flipped.
	// RETURNS
	//	The endian-flipped data.
	//
	static unsigned __int64 audPlatformSpecific::FlipEndian(unsigned __int64 value);

private:
	static audBuildPlatforms sm_BuildPlatform;
	static array<bool> ^sm_BigEndianPlatforms;
};


#endif // PLATFORM_SPECIFIC_H
