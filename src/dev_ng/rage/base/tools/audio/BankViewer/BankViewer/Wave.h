using namespace System;
using namespace System::Xml;

#ifndef WAVE_H
#define WAVE_H
public ref struct Marker
{
	unsigned int CategoryHash;
	unsigned int Data;
	unsigned int NameHash;
	unsigned int TimeOffset;
	float Value;
};

ref class Wave
{
public:
	Wave();
	unsigned int GetNameHash();
	void SetNameHash(unsigned int nameHash);
	void SetWaveName(String ^name);
	String ^GetWaveName();
	unsigned int GetSize();
	void SetSize(unsigned int size);
	void SetXmlNode(XmlNode ^node);
	void SetWaveSampleData(array<Byte> ^waveSampleData);
	array<Byte> ^GetWaveSampleData();
	void SetCustomMetadata(array<Byte> ^customMetadata);
	unsigned int GetCustomMetadataSize();
	unsigned int GetMetadataSize();
	void SetMetadataSize(unsigned int metadataSize);
	unsigned int GetSampleRate();
	void SetSampleRate(unsigned int sampleRate);
	static unsigned int HashString(const char *string);
	array<Marker ^> ^GetMarkers();
	array<Byte> ^GetVisemeData();

private:
	static const char* WaveMetadataMarkerType = "WAVE_METADATA_MARKER";
	static const char* WaveMetadataArbitraryDataType = "WAVE_METADATA_ARBITRARY_DATA";

	XmlNode ^m_Node;
	unsigned int m_NameHash;
	String ^m_Name;
	unsigned int m_Size;
	unsigned int m_MetadataSize;
	unsigned int m_SampleRate;
	array<Byte> ^m_SampleData;
	array<Byte> ^m_CustomMetadata;
	array<Marker ^> ^m_Markers;
	array<Byte> ^m_VisemeData;
	void ExtractCustomMetadata();
};
#endif
