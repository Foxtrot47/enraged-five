#include "stdafx.h"
#include "audiohardware/wavedefs.h"
#include "audiosoundtypes/wavemetadatamarkerdefs.h"
#include "Bank.h"
#include "memory.h"
#include "PlatformSpecific.h"
#include "Wave.h"

using namespace System::IO;
using namespace System::Collections;
using namespace rage;

Bank::Bank(String ^bankPath)
{
	m_Path = bankPath;
	
	//Initialise platform specific class. Deduce platform from bank path
	if(m_Path->ToUpper()->Contains("XENON") 
		|| m_Path->ToUpper()->Contains("XBOX")
		|| m_Path->ToUpper()->Contains("X360"))
	{
		audPlatformSpecific::InitClass("XENON");
	}
	else if(m_Path->ToUpper()->Contains("PS3"))
	{
		audPlatformSpecific::InitClass("PS3");
	}
	else if(m_Path->ToUpper()->Contains("PC"))
	{
		audPlatformSpecific::InitClass("PC");
	}
	else
	{
		throw gcnew Exception("Couldn't determine platform");
	}

}


void Bank::LoadStreamingBank()
{
	//Read in bank
	FileStream ^bankStream = File::Open(m_Path,FileMode::Open,FileAccess::Read);
	BufferedStream ^bankFileBufStreamIn = gcnew BufferedStream(bankStream, g_WaveBankFileBufferBytes);
	BinaryReader ^bankFileIn = gcnew BinaryReader(bankFileBufStreamIn);

	//store bank size
	m_BankSize = (int)bankStream->Length;
	array<Byte> ^waveBankBytes = bankFileIn->ReadBytes(m_BankSize);

	//close file stream
	bankFileIn->Close();
	bankStream->Close();
	
	//Extract Wave Bank metadata.
	pin_ptr<Byte> waveBankBytesPinned = &waveBankBytes[0];	
	audStreamingWaveBankMetadata *waveBankMetadata = (audStreamingWaveBankMetadata *)waveBankBytesPinned;

	m_NumWaves = audPlatformSpecific::FixEndian(waveBankMetadata->base.numWaves);
	unsigned int loadBlockSeekTableSizeBytes = audPlatformSpecific::FixEndian(waveBankMetadata->numLoadBlocks) * sizeof(audLoadBlockSeekTableEntry);
	m_MetadataSize = sizeof(audStreamingWaveBankMetadata) + loadBlockSeekTableSizeBytes;

	//Extract wave metadata lookup
	unsigned int waveMetadataLookupOffsetBytes = (unsigned int)audPlatformSpecific::FixEndian(waveBankMetadata->base.waveMetadataLookupOffsetBytes);
	audMetadataLookupEntry *waveMetadataLookup = new audMetadataLookupEntry[audPlatformSpecific::FixEndian(waveBankMetadata->base.numWaves)];
	memcpy(waveMetadataLookup, waveBankBytesPinned + waveMetadataLookupOffsetBytes, m_NumWaves *sizeof(audMetadataLookupEntry));

	m_WaveMetaDataLookupSize = m_NumWaves*sizeof(audMetadataLookupEntry);
	//Calculate length of concatenated Wave metadata.
	unsigned int waveMetadataLengthBytes = 0;
	for(unsigned int waveIndex=0; waveIndex<m_NumWaves; waveIndex++)
	{
		waveMetadataLengthBytes += audPlatformSpecific::FixEndian(waveMetadataLookup[waveIndex].metadataLengthBytes);
	}

	//Extract Wave metadata.
	unsigned int waveMetadataStartOffsetBytes = waveMetadataLookupOffsetBytes + (m_NumWaves *sizeof(audMetadataLookupEntry));
	array<Byte> ^waveMetadataBytes = gcnew array<Byte>(waveMetadataLengthBytes);
	Array::Copy(waveBankBytes, waveMetadataStartOffsetBytes, waveMetadataBytes, 0, waveMetadataLengthBytes);


	unsigned int numCustomMetadataElements = audPlatformSpecific::FixEndian(waveBankMetadata->base.numCustomMetadataElements);
	unsigned int customMetadataLookupOffsetBytes = (unsigned int)audPlatformSpecific::FixEndian(waveBankMetadata->base.customMetadataLookupOffsetBytes);

	audMetadataLookupEntry *customMetadataLookup = NULL;
	//Extract custom metadata lookup table
	customMetadataLookup = new audMetadataLookupEntry[numCustomMetadataElements];
	memcpy(customMetadataLookup, waveBankBytesPinned + customMetadataLookupOffsetBytes,
	numCustomMetadataElements * sizeof(audMetadataLookupEntry));	

	//Calculate length of concatenated custom metadata.
	m_CustomMetadataSize = 0;
	Hashtable customMetadataValues = gcnew Hashtable();
	for(unsigned int elementIndex=0; elementIndex<numCustomMetadataElements; elementIndex++)
	{
		unsigned int value = audPlatformSpecific::FixEndian(customMetadataLookup[elementIndex].metadataLengthBytes);
		unsigned int hash = audPlatformSpecific::FixEndian(customMetadataLookup[elementIndex].nameHash);
		m_CustomMetadataSize += value;
		customMetadataValues.Add(hash,value);
	}

		//extract custom metadata
	unsigned int customMetadataStartOffsetBytes = customMetadataLookupOffsetBytes + (numCustomMetadataElements *
	sizeof(audMetadataLookupEntry));
	array<Byte> ^customMetadataList = gcnew array<Byte>(m_CustomMetadataSize);
	Array::Copy(waveBankBytes, customMetadataStartOffsetBytes, customMetadataList, 0, m_CustomMetadataSize);

	//Get waves
	m_Waves = gcnew Hashtable();
	for(unsigned int i=0; i<m_NumWaves; i++)
	{
			Wave ^wave = gcnew Wave();
			wave->SetNameHash(audPlatformSpecific::FixEndian(waveMetadataLookup[i].nameHash));			
			wave->SetMetadataSize(audPlatformSpecific::FixEndian(waveMetadataLookup[i].metadataLengthBytes));

			//Get offset and use it to pull out waveMetadata
			unsigned int waveMetadataOffset = (unsigned int)audPlatformSpecific::FixEndian(waveMetadataLookup[i].metadataOffsetBytes);
			array<Byte> ^metadataBytes = gcnew array<Byte>(sizeof(audWaveMetadataBase));
			Array::Copy(waveMetadataBytes,waveMetadataOffset,metadataBytes,0,sizeof(audWaveMetadataBase));
			pin_ptr<Byte> waveMetadataBytesPinned = &metadataBytes[0];
			audWaveMetadataBase *waveMetadata = (audWaveMetadataBase *)waveMetadataBytesPinned;
			
			//Get sample rate and wave size from wave metadata
			wave->SetSampleRate(audPlatformSpecific::FixEndian(waveMetadata->sampleRate));
			wave->SetSize(audPlatformSpecific::FixEndian(waveMetadata->lengthBytes));
			for(unsigned int elementIndex=0; elementIndex<numCustomMetadataElements; elementIndex++)
			{
				if(wave->GetNameHash().Equals(audPlatformSpecific::FixEndian(customMetadataLookup[elementIndex].nameHash)))
				{
				unsigned int waveCustomMetadataOffsetBytes = (unsigned int)audPlatformSpecific::FixEndian(customMetadataLookup[elementIndex].metadataOffsetBytes);
				unsigned int waveCustonMetadataSize = (unsigned int)audPlatformSpecific::FixEndian(customMetadataLookup[elementIndex].metadataLengthBytes);
				array<Byte> ^waveCustomMetadata = gcnew array<Byte>(waveCustonMetadataSize);
				Array::Copy(customMetadataList,waveCustomMetadataOffsetBytes,waveCustomMetadata,0,waveCustonMetadataSize);
				wave->SetCustomMetadata(waveCustomMetadata);
				break;
				}
			}
					
			waveMetadata=0;
			
	
			m_Waves->Add(wave->GetNameHash().ToString(),wave);
	}

		waveBankMetadata=0;
		waveMetadataLookup=0;
		customMetadataLookup=0;
}

void Bank::LoadBank()
{
	//Adapted from wavebank.cpp

	//Read in bank
	FileStream ^bankStream = File::Open(m_Path,FileMode::Open,FileAccess::Read);
	BufferedStream ^bankFileBufStreamIn = gcnew BufferedStream(bankStream, g_WaveBankFileBufferBytes);
	BinaryReader ^bankFileIn = gcnew BinaryReader(bankFileBufStreamIn);

	//store bank size
	m_BankSize = (int)bankStream->Length;
	array<Byte> ^waveBankBytes = bankFileIn->ReadBytes(m_BankSize);

	//close file stream
	bankFileIn->Close();
	bankStream->Close();
	
	//Extract Wave Bank metadata.
	pin_ptr<Byte> waveBankBytesPinned = &waveBankBytes[0];	
	audWaveBankMetadata *waveBankMetadata = (audWaveBankMetadata *)waveBankBytesPinned;

	//store number of waves
	m_NumWaves = audPlatformSpecific::FixEndian(waveBankMetadata->numWaves);

	m_MetadataSize = sizeof(audWaveBankMetadata);

	//Extract Wave metadata lookup table
	unsigned int waveMetadataLookupOffsetBytes = (unsigned int)audPlatformSpecific::FixEndian(waveBankMetadata->waveMetadataLookupOffsetBytes);
	audMetadataLookupEntry *waveMetadataLookup = new audMetadataLookupEntry[m_NumWaves];
	memcpy(waveMetadataLookup, waveBankBytesPinned + waveMetadataLookupOffsetBytes, m_NumWaves *sizeof(audMetadataLookupEntry));

	m_WaveMetaDataLookupSize = m_NumWaves*sizeof(audMetadataLookupEntry);

	//Calculate length of concatenated Wave metadata.
	unsigned int waveMetadataLengthBytes = 0;
	for(unsigned int waveIndex=0; waveIndex<m_NumWaves; waveIndex++)
	{
		waveMetadataLengthBytes += audPlatformSpecific::FixEndian(waveMetadataLookup[waveIndex].metadataLengthBytes);
	}

	//Extract Wave metadata.
	unsigned int waveMetadataStartOffsetBytes = waveMetadataLookupOffsetBytes + (m_NumWaves *sizeof(audMetadataLookupEntry));
	array<Byte> ^waveMetadataBytes = gcnew array<Byte>(waveMetadataLengthBytes);
	Array::Copy(waveBankBytes, waveMetadataStartOffsetBytes, waveMetadataBytes, 0, waveMetadataLengthBytes);

	unsigned int numCustomMetadataElements = (unsigned int)audPlatformSpecific::FixEndian(waveBankMetadata->numCustomMetadataElements);
	unsigned int customMetadataLookupOffsetBytes = (unsigned int)audPlatformSpecific::FixEndian(waveBankMetadata->customMetadataLookupOffsetBytes);

	audMetadataLookupEntry *customMetadataLookup = NULL;
	//Extract custom metadata lookup table (into a new buffer - so it can be resized later.)
	customMetadataLookup = new audMetadataLookupEntry[numCustomMetadataElements];
	memcpy(customMetadataLookup, waveBankBytesPinned + customMetadataLookupOffsetBytes,
	numCustomMetadataElements * sizeof(audMetadataLookupEntry));

	//Calculate length of concatenated custom metadata.
	m_CustomMetadataSize = 0;

	for(unsigned int elementIndex=0; elementIndex<numCustomMetadataElements; elementIndex++)
	{
		unsigned int waveCustonMetadataSize = audPlatformSpecific::FixEndian(customMetadataLookup[elementIndex].metadataLengthBytes);
		m_CustomMetadataSize += waveCustonMetadataSize;
	}

	//extract custom metadata
	unsigned int customMetadataStartOffsetBytes = customMetadataLookupOffsetBytes + (numCustomMetadataElements *
	sizeof(audMetadataLookupEntry));
	array<Byte> ^customMetadataList = gcnew array<Byte>(m_CustomMetadataSize);
	Array::Copy(waveBankBytes, customMetadataStartOffsetBytes, customMetadataList, 0, m_CustomMetadataSize);

	//Extract Wave sample data.
	unsigned int waveSampleDataStartOffsetBytes = audPlatformSpecific::FixEndian(waveBankMetadata->waveDataStartOffsetBytes);
	unsigned int waveSampleDataLengthBytes = m_BankSize - waveSampleDataStartOffsetBytes;
	array<Byte> ^waveSampleDataBytes  = gcnew array<Byte>(waveSampleDataLengthBytes);
	Array::Copy(waveBankBytes, waveSampleDataStartOffsetBytes, waveSampleDataBytes, 0, waveSampleDataLengthBytes);
		
	//Get wave stats
	m_Waves = gcnew Hashtable();
	for(unsigned int i=0; i<m_NumWaves; i++)
	{
			Wave ^wave = gcnew Wave();
			wave->SetNameHash(audPlatformSpecific::FixEndian(waveMetadataLookup[i].nameHash));			
			wave->SetMetadataSize(audPlatformSpecific::FixEndian(waveMetadataLookup[i].metadataLengthBytes));

			//Get offset and use it to pull out waveMetadata
			unsigned int waveMetadataOffset = (unsigned int)audPlatformSpecific::FixEndian(waveMetadataLookup[i].metadataOffsetBytes);
			array<Byte> ^metadataBytes = gcnew array<Byte>(sizeof(audWaveMetadataBase));
			Array::Copy(waveMetadataBytes,waveMetadataOffset,metadataBytes,0,sizeof(audWaveMetadataBase));
			pin_ptr<Byte> waveMetadataBytesPinned = &metadataBytes[0];
			audWaveMetadataBase *waveMetadata = (audWaveMetadataBase *)waveMetadataBytesPinned;
			
			//Get sample rate and wave size from wave metadata
			wave->SetSampleRate(audPlatformSpecific::FixEndian(waveMetadata->sampleRate));
			wave->SetSize(audPlatformSpecific::FixEndian(waveMetadata->lengthBytes));
		
			for(unsigned int elementIndex=0; elementIndex<numCustomMetadataElements; elementIndex++)
			{
				if(wave->GetNameHash().Equals(audPlatformSpecific::FixEndian(customMetadataLookup[elementIndex].nameHash)))
				{
				unsigned int waveCustomMetadataOffsetBytes = (unsigned int)audPlatformSpecific::FixEndian(customMetadataLookup[elementIndex].metadataOffsetBytes);
				unsigned int waveCustonMetadataSize = (unsigned int)audPlatformSpecific::FixEndian(customMetadataLookup[elementIndex].metadataLengthBytes);
				array<Byte> ^waveCustomMetadata = gcnew array<Byte>(waveCustonMetadataSize);
				Array::Copy(customMetadataList,waveCustomMetadataOffsetBytes,waveCustomMetadata,0,waveCustonMetadataSize);
				wave->SetCustomMetadata(waveCustomMetadata);
				break;
				}
			}

			//Extract wave sample data
			unsigned int waveSampleDataOffsetBytes = (unsigned int)audPlatformSpecific::FixEndian(waveMetadata->waveDataOffsetBytes);
			//Ensure that we use the actual wave size (aligned to the packet size.)
			unsigned int waveSampleDataSize = (unsigned int)System::Math::Ceiling((float)wave->GetSize() /
													 (float)g_StreamingPacketBytes) * g_StreamingPacketBytes;

			array<Byte> ^waveSampleData = gcnew array<Byte>(waveSampleDataSize);
			Array::Copy(waveSampleDataBytes,waveSampleDataOffsetBytes,waveSampleData,0,waveSampleDataSize);
			wave->SetWaveSampleData(waveSampleData);
			
	
			m_Waves->Add(wave->GetNameHash().ToString(),wave);
			
			waveMetadata=0;
			
	}
			waveMetadataLookup=0;
			customMetadataLookup=0;
			waveBankMetadata=0;
			

}
