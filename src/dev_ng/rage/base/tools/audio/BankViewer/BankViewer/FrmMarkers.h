#pragma once

#include "Wave.h"

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;


namespace BankViewer {

	/// <summary>
	/// Summary for frmMarkers
	///
	/// WARNING: If you change the name of this class, you will need to change the
	///          'Resource File Name' property for the managed resource compiler tool
	///          associated with all .resx files this class depends on.  Otherwise,
	///          the designers will not be able to interact properly with localized
	///          resources associated with this form.
	/// </summary>
	public ref class FrmMarkers : public System::Windows::Forms::Form
	{
	public:
		FrmMarkers(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			this->Closing += gcnew System::ComponentModel::CancelEventHandler(this, &FrmMarkers::Form_Closing);
		}

		void Init(array<Marker^> ^markers)
		{
			m_ListView->Items->Clear();
			for(int i=0; i<markers->Length;i++)
			{
				ListViewItem ^lvi = gcnew ListViewItem(markers[i]->CategoryHash.ToString());
				lvi->SubItems->Add(markers[i]->Data.ToString());
				lvi->SubItems->Add(markers[i]->NameHash.ToString());
				lvi->SubItems->Add(markers[i]->TimeOffset.ToString());
				lvi->SubItems->Add(markers[i]->Value.ToString());
				m_ListView->Items->Add(lvi);
			}
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~FrmMarkers()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::ListView^  m_ListView;
	private:
		void Form_Closing( Object^ sender, System::ComponentModel::CancelEventArgs^ e )
       
			   {
					e->Cancel=true;
					this->Hide();
			   }

	protected: 
	private: System::Windows::Forms::ColumnHeader^  m_CategoryHash;
	private: System::Windows::Forms::ColumnHeader^  m_Data;
	private: System::Windows::Forms::ColumnHeader^  m_NameHash;
	private: System::Windows::Forms::ColumnHeader^  m_TimeOffset;
	private: System::Windows::Forms::ColumnHeader^  m_Value;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->m_ListView = (gcnew System::Windows::Forms::ListView());
			this->m_CategoryHash = (gcnew System::Windows::Forms::ColumnHeader());
			this->m_Data = (gcnew System::Windows::Forms::ColumnHeader());
			this->m_NameHash = (gcnew System::Windows::Forms::ColumnHeader());
			this->m_TimeOffset = (gcnew System::Windows::Forms::ColumnHeader());
			this->m_Value = (gcnew System::Windows::Forms::ColumnHeader());
			this->SuspendLayout();
			// 
			// m_ListView
			// 
			this->m_ListView->Columns->AddRange(gcnew cli::array< System::Windows::Forms::ColumnHeader^  >(5) {this->m_CategoryHash, 
				this->m_Data, this->m_NameHash, this->m_TimeOffset, this->m_Value});
			this->m_ListView->Dock = System::Windows::Forms::DockStyle::Fill;
			this->m_ListView->Location = System::Drawing::Point(0, 0);
			this->m_ListView->Name = L"m_ListView";
			this->m_ListView->Size = System::Drawing::Size(398, 313);
			this->m_ListView->TabIndex = 0;
			this->m_ListView->UseCompatibleStateImageBehavior = false;
			this->m_ListView->View = System::Windows::Forms::View::Details;
			// 
			// m_CategoryHash
			// 
			this->m_CategoryHash->Text = L"Category Hash";
			this->m_CategoryHash->Width = 99;
			// 
			// m_Data
			// 
			this->m_Data->Text = L"Data";
			this->m_Data->Width = 69;
			// 
			// m_NameHash
			// 
			this->m_NameHash->Text = L"Name Hash";
			this->m_NameHash->Width = 88;
			// 
			// m_TimeOffset
			// 
			this->m_TimeOffset->Text = L"Time Offset";
			this->m_TimeOffset->Width = 76;
			// 
			// m_Value
			// 
			this->m_Value->Text = L"Value";
			this->m_Value->Width = 56;
			// 
			// frmMarkers
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(398, 313);
			this->Controls->Add(this->m_ListView);
			this->Name = L"frmMarkers";
			this->Text = L"Markers";
			this->ResumeLayout(false);

		}
#pragma endregion
	};
}
