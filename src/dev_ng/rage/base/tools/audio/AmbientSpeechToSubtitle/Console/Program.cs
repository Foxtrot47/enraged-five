﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using AmbientSpeechToSubtitle;
using rage.ToolLib;
using rage.ToolLib.CmdLine;
using Rockstar.AssetManager.Interfaces;

namespace AmbientSpeechSubtitleCreator
{
    class Program : CommandLineConfiguration
    {
        [Description("Perforce server path to AmbientSpeechContext.xlsx")]
        [OptionalParameter]
        [DefaultValue(@"//rdr3/docs/Design/Ambient/Ambient_Speech_Context.xlsx")]
        public string AmbientSpeechContext { get; set; }

        [Description("The subtitle folder")]
        [OptionalParameter]
        [DefaultValue(@"//rdr3/assets/GameText/American/")]
        public string SubtitleFolder { get; set; }

        [Description("the connection info for perforce ex. rsgedip4s1:1666")]
        [OptionalParameter]
        public string AssetServer { get; set; }

        [Description("the perforce user name")]
        [OptionalParameter]
        public string AssetUsername { get; set; }

        [Description("the perforce password")]
        [OptionalParameter]
        [DefaultValue("")]
        public string AssetPassword { get; set; }

        [Description("the perforce Host")]
        [OptionalParameter]
        public string AssetHost { get; set; }

        [Description("the perforce Workspace")]
        [OptionalParameter]
        public string AssetWorkspace { get; set; }

        [Description("the perforce depotRoot")]
        [OptionalParameter]
        public string DepotRoot { get; set; }

        [Description("Set to yes if batch process so as not to wait after output")]
        [OptionalParameter]
        [DefaultValue("no")]
        public string BatchProcess { get; set; }

        [Description("Set for placeholder color")]
        [OptionalParameter]
        [DefaultValue("ORANGE")]
        public string PlaceholderColor { get; set; }

        [Description("Ped voice Masterlist Xls location")]
        [OptionalParameter]
        [DefaultValue("//rdr3/docs/Audio/RDR3_Ped Voice Masterlist.xlsx")]
        public string PedVoiceXLS { get; set; }


        static void Main(string[] args)
        {
            Program config;
            try
            {
                config = new Program(args);
            }
            catch (rage.ToolLib.CmdLine.CommandLineConfiguration.ParameterException ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.Usage);
            }
        }

        public Program(string[] args)
            : base(args)
        {
            try
            {
                XlsToSubtitle xlsToSubtitle;
                string depotPath = Path.Combine(SubtitleFolder, "americanAmbientDialogue.txt");

                if (!string.IsNullOrEmpty(AssetServer) &&
                    !string.IsNullOrEmpty(AssetUsername))
                {
                    AutoAssetManager.Instance.ConnectWithCredentials(AssetUsername, AssetPassword, AssetWorkspace,
                        AssetServer, AssetHost, DepotRoot);
                }

                IChangeList changelist =
                    AutoAssetManager.Instance.AssetManager.CreateChangeList(
                        "American Ambient Dialogue Update \nbuddy:AudioMotherOfGod");
                if (AutoAssetManager.Instance.AssetManager.ExistsAsAsset(depotPath))
                {
                    AutoAssetManager.Instance.AssetManager.GetLatest(
                        depotPath, true);
                    if (!AutoAssetManager.Instance.AssetManager.IsCheckedOut(depotPath))
                    {
                        changelist.CheckoutAsset(depotPath, false);
                    }
                    else
                    {
                        IAsset asset = AutoAssetManager.Instance.AssetManager.GetCheckedOutAsset(depotPath);
                        AutoAssetManager.Instance.AssetManager.MoveAsset(asset, changelist);
                    }
                }

                try
                {
                    AutoAssetManager.Instance.AssetManager.GetLatest(AmbientSpeechContext, false);
                    string localPathAmbientSpeechText =
                        AutoAssetManager.Instance.AssetManager.GetLocalPath(AmbientSpeechContext);
                    string localFolder = AutoAssetManager.Instance.AssetManager.GetLocalPath(SubtitleFolder);
                    AutoAssetManager.Instance.AssetManager.GetLatest(PedVoiceXLS, false);
                    string voiceListPath = AutoAssetManager.Instance.AssetManager.GetLocalPath(PedVoiceXLS);

                    xlsToSubtitle = new XlsToSubtitle(localPathAmbientSpeechText,
                        Path.Combine(localFolder, "americanAmbientDialogue.txt"), voiceListPath, PlaceholderColor);
                    xlsToSubtitle.CreateTextFile();
                    Console.WriteLine("Exported to " + xlsToSubtitle.SubtitlePath);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Process failed: " + ex.Message);
                    changelist.Delete();
                    Environment.ExitCode = -1;
                    return;
                }
                if (BatchProcess.Equals("no", StringComparison.InvariantCultureIgnoreCase))
                {
                    Console.ReadLine();
                }
                else
                {

                    if (!AutoAssetManager.Instance.AssetManager.ExistsAsAsset(depotPath))
                    {
                        changelist.MarkAssetForAdd(xlsToSubtitle.SubtitlePath);
                    }

                    changelist.RevertUnchanged();
                    if (changelist.Assets.Count > 0)
                    {
                        changelist.Submit();
                        Console.WriteLine("Submitted in changelist" + changelist.Number);
                    }
                    else
                    {
                        changelist.Delete();
                        Console.WriteLine("Nothing to submit");
                    }

                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Process failed with error {0} \n Stacktrace: \n {1} \n InnerException {2}", e.Message, e.StackTrace, e.InnerException.Message);
                Environment.ExitCode = -1;
                Console.ReadLine();
            }
        }
    }

}
