﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace AmientSpeechSubtitle
{
    class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Starting ambient speech parsing of " + args[0]);
            try
            {
                var xlsToSubtitle = new AmbientSpeechToSubtitle.XlsToSubtitle(args[0], args[1], args[2], args[3]);

                xlsToSubtitle.CreateTextFile();
                Environment.Exit(0);
            }
            catch (Exception e)
            {
                Console.WriteLine("Parsing failed with error: " + e.Message);
                Console.Write(e.StackTrace);
                Environment.Exit(-1);
            }
            
        }
    }
}
