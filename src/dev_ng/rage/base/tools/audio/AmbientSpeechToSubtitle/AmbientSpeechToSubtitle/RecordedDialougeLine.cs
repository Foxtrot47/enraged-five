﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using rage.ToolLib;

namespace AmbientSpeechToSubtitle
{
    public class RecordedDialougeLine
    {
        public string VoiceName;
        public string ContextId;
        public int Variation = 01;
        public string Subtitle;
        public string Content;
        public string Location;

        public RecordedDialougeLine(string voiceName, string contextId, string subtitle, string content, string location, int variation = 1)
        {
            VoiceName = voiceName;
            ContextId = contextId;
            Subtitle = subtitle;
            Variation = variation;
            Content = content;
            Location = location;
        }

        public static uint GetLookUpHash(string voiceName, string contextId)
        {
            uint voiceNameHash = new Hash { Value = voiceName }.Key;
            uint contextHash = new Hash { Value = contextId }.Key;

            uint lookupHash = voiceNameHash ^ contextHash;
            if (voiceNameHash == contextHash)
            {
                lookupHash = voiceNameHash;
            }
            return lookupHash;
        }


    }
}
