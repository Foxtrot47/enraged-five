﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmbientSpeechToSubtitle
{
    static class VoiceNameHacker
    {

        public static string Convert(this string voicename)
        {
            Hashtable voiceConverterHashtable = (ConfigurationManager.GetSection("VoiceNameConvertor") as Hashtable);
            if (voiceConverterHashtable != null)
            {
                Dictionary<string, string> voiceconverterDicitonary = voiceConverterHashtable.Cast<DictionaryEntry>()
                    .ToDictionary(n => n.Key.ToString(), n => n.Value.ToString());

                foreach (KeyValuePair<string, string> keyValuePair in voiceconverterDicitonary)
                {
                    if (voicename.Equals(keyValuePair.Key, StringComparison.CurrentCultureIgnoreCase))
                    {
                        voicename = keyValuePair.Value;
                    }
                }
            }

            return voicename;
        }
    }
}
