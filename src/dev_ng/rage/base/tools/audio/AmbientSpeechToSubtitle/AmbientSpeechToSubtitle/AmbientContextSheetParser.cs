﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using Excel;

namespace AmbientSpeechToSubtitle
{
    public class AmbientContextSheetParser
    {
        public enum Columns
        {
            FinalDialogeLine,
            ContextIdentifier,
            VoiceName,
            Content,
            Location,
            Recorded,
            PlaceholderDialogueLine

        }
        public string PlaceholderColor { get; private set; }
        public const string FinalDialogeLine = "FINAL DIALOGUE LINE";
        public const string ContextIdentifier = "CONTEXT IDENTIFIER";
        public const string VoiceName = "VOICE NAME";
        public const string Recorded = "RECORDED?";
        public const string Content = "CONTENT";
        public const string Location = "LOCATION";
        public const string PlaceholderDialogueLine = "PLACEHOLDER DIALOGUE LINE";

        private Dictionary<Columns, int> _columnNumberDictionary;
        private string _previousContent = "";
        private string _previousLocation = "";
        private List<string> _voicelist; 
        public AmbientContextSheetParser(string placeholderColor, List<string> voicelist)
        {
            PlaceholderColor = placeholderColor;
            _voicelist = voicelist;
        }

        public List<RecordedDialougeLine> DialougeLines = new List<RecordedDialougeLine>();

        private void PopulateColumnNumberDictionary(Worksheet worksheet)
        {
            if (worksheet.Rows == null)
            { return; }
            _columnNumberDictionary = new Dictionary<Columns, int>();

            for (int i = 0; i < worksheet.Rows[0].Cells.Length; i++)
            {
                Cell cell = worksheet.Rows[0].Cells[i];
                if (cell == null)
                {
                    continue;
                }
                switch (cell.Text.Trim().ToUpper())
                {
                    case ContextIdentifier:
                        _columnNumberDictionary[Columns.ContextIdentifier] = i;
                        break;
                    case FinalDialogeLine:
                        _columnNumberDictionary[Columns.FinalDialogeLine] = i;
                        break;
                    case Recorded:
                        _columnNumberDictionary[Columns.Recorded] = i;
                        break;
                    case VoiceName:
                        _columnNumberDictionary[Columns.VoiceName] = i;
                        break;
                    case Content:
                        _columnNumberDictionary[Columns.Content] = i;
                        break;
                    case Location:
                        _columnNumberDictionary[Columns.Location] = i;
                        break;
                    case PlaceholderDialogueLine:
                        _columnNumberDictionary[Columns.PlaceholderDialogueLine] = i;
                        break;
                }
            }
        }

        public void AddDialougeLines(Worksheet worksheet)
        {


            PopulateColumnNumberDictionary(worksheet);
            if (_columnNumberDictionary.Count != Enum.GetNames(typeof(Columns)).Length)
            {
                return;
            }
            
            foreach (Row row in worksheet.Rows)
            {
                if (row.Cells == null || row.Cells[_columnNumberDictionary[Columns.VoiceName]] == null || row.Cells[_columnNumberDictionary[Columns.ContextIdentifier]] == null)
                {
                    continue;
                }

                bool recorded = row.Cells[_columnNumberDictionary[Columns.Recorded]] != null &&
                                row.Cells[_columnNumberDictionary[Columns.Recorded]].Text.Equals("yes",
                                    StringComparison.InvariantCultureIgnoreCase);


                if (string.IsNullOrEmpty(row.Cells[_columnNumberDictionary[Columns.VoiceName]].Text) ||
                      string.IsNullOrEmpty(row.Cells[_columnNumberDictionary[Columns.ContextIdentifier]].Text))
                {
                    continue;
                }
                if ((recorded && (row.Cells[_columnNumberDictionary[Columns.FinalDialogeLine]] == null || string.IsNullOrEmpty(row.Cells[_columnNumberDictionary[Columns.FinalDialogeLine]].Text))) ||
                    (!recorded && (row.Cells[_columnNumberDictionary[Columns.PlaceholderDialogueLine]] == null || string.IsNullOrEmpty(row.Cells[_columnNumberDictionary[Columns.PlaceholderDialogueLine]].Text))))
                {
                    continue;
                }

                if (!_voicelist.Contains(row.Cells[_columnNumberDictionary[Columns.VoiceName]].Text.Trim(),
                    StringComparer.InvariantCultureIgnoreCase))
                {
                    continue;
                }

                int variation = 1 + DialougeLines.Count(
                    p =>
                        p.ContextId.Equals(
                            row.Cells[_columnNumberDictionary[Columns.ContextIdentifier]].Text.Trim(),
                            StringComparison.InvariantCultureIgnoreCase) &&
                            p.VoiceName.Equals(
                            row.Cells[_columnNumberDictionary[Columns.VoiceName]].Text.Trim(),
                            StringComparison.InvariantCultureIgnoreCase)

                            );
                string subtitle = "";
                if (recorded)
                {
                    subtitle = row.Cells[_columnNumberDictionary[Columns.FinalDialogeLine]].Text.Trim();
                }
                else
                {
                    subtitle = string.Format("~COLOR_{0}~ {1}", PlaceholderColor.ToUpper().Trim(),
                        row.Cells[_columnNumberDictionary[Columns.PlaceholderDialogueLine]
                            ].Text.Trim());
                }

                if (row.Cells[_columnNumberDictionary[Columns.Content]] != null &&
                    !string.IsNullOrEmpty(row.Cells[_columnNumberDictionary[Columns.Content]].Text))
                {
                    _previousContent = row.Cells[_columnNumberDictionary[Columns.Content]].Text.Trim();
                }

                string content = _previousContent;

                if (row.Cells[_columnNumberDictionary[Columns.Location]] != null &&
                    !string.IsNullOrEmpty(row.Cells[_columnNumberDictionary[Columns.Location]].Text))
                {
                    _previousLocation = row.Cells[_columnNumberDictionary[Columns.Location]].Text.Trim();
                }
                string location = _previousLocation;
                if (location == string.Empty || content == string.Empty)
                {
                    continue;
                }
                RecordedDialougeLine recordedDialougeLine = new RecordedDialougeLine(
                        row.Cells[_columnNumberDictionary[Columns.VoiceName]].Text.Trim(),
                        row.Cells[_columnNumberDictionary[Columns.ContextIdentifier]].Text.Trim(),
                      subtitle, content, location, variation);
                DialougeLines.Add(recordedDialougeLine);

            }

        }

    }
}
