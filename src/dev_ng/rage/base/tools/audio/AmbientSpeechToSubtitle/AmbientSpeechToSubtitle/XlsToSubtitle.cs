﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel;

namespace AmbientSpeechToSubtitle
{
    public class XlsToSubtitle
    {
        public string AmbientContextXls;
        public string SubtitlePath;
        public string VoicelistXls;

        public XlsToSubtitle(string ambientContextXls, string subtitlePath, string voiceListPath, string placeholderColor = "ORANGE")
        {
            AmbientContextXls = ambientContextXls;
            SubtitlePath = subtitlePath;
            PlaceholderColor = placeholderColor;
            VoicelistXls = voiceListPath;
        }

        public string PlaceholderColor { get; private set; }

        public void CreateTextFile()
        {
            List<string> voicelist = GetVoiceList();

            AmbientContextSheetParser ambientContextSheetParser = new AmbientContextSheetParser(PlaceholderColor, voicelist);

            foreach (
                Worksheet worksheet in Workbook.Worksheets(AmbientContextXls))
            {
                if (worksheet.Rows == null)
                {
                    continue;
                }
                
                ambientContextSheetParser.AddDialougeLines(worksheet);
            }

            SubtitleTextFile subtitleTextFile = new SubtitleTextFile(ambientContextSheetParser.DialougeLines);
            subtitleTextFile.Save(SubtitlePath);
        }

        public List<string> GetVoiceList()
        {
            List<string> voiceList = new List<string>();
            foreach (
                Worksheet worksheet in Workbook.Worksheets(VoicelistXls))
            {
                foreach (var row in worksheet.Rows)
                {
                    if (row.Cells[0] != null && !string.IsNullOrEmpty(row.Cells[0].Text))
                    {
                        if (row.Cells[0].Text.Trim().ToUpper() != "VOICE NAME")
                        {
                            voiceList.Add(row.Cells[0].Text.Trim().ToUpper());
                        }
                    }
                }

            }
            return voiceList;
        }
    }
}
