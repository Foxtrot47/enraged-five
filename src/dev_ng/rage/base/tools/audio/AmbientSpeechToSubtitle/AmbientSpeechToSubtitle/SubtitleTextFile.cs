﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmbientSpeechToSubtitle
{
    public class SubtitleTextFile
    {
        private List<RecordedDialougeLine> _recordedDialougeLines;

        public SubtitleTextFile(IEnumerable<RecordedDialougeLine> recordedDialougeLines)
        {
            _recordedDialougeLines = recordedDialougeLines.ToList();
        }

        private string GetSubtitleText()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendLine("start");
            foreach (string voicename in _recordedDialougeLines.Select(p => p.VoiceName).Distinct())
            {

                stringBuilder.AppendLine(voicename.Convert());
            }
            stringBuilder.AppendLine("end");
            stringBuilder.AppendLine();
            string previousVoicename = string.Empty;
            foreach (RecordedDialougeLine recordedDialougeLine in _recordedDialougeLines)
            {
                if (!previousVoicename.Equals(recordedDialougeLine.VoiceName.Convert(), StringComparison.InvariantCultureIgnoreCase))
                {
                    stringBuilder.AppendLine(string.Format("{{{0}}}", recordedDialougeLine.VoiceName.Convert()));
                    stringBuilder.AppendLine();
                    previousVoicename = recordedDialougeLine.VoiceName.Convert();
                }


                stringBuilder.AppendLine(string.Format("[{0}_{1}:{2}]", RecordedDialougeLine.GetLookUpHash(recordedDialougeLine.VoiceName.Convert(), recordedDialougeLine.ContextId).ToString("X"), recordedDialougeLine.Variation.ToString("00"),
                    recordedDialougeLine.VoiceName.Convert()));
                stringBuilder.AppendLine(string.Format("{{{0} - {1} - {2}}}", recordedDialougeLine.Content, recordedDialougeLine.Location, recordedDialougeLine.ContextId));
                stringBuilder.AppendLine("~z~" + recordedDialougeLine.Subtitle);
                stringBuilder.AppendLine();
            }

            return stringBuilder.ToString();

        }

        public void Save(string path)
        {
            using (StreamWriter streamWriter = new StreamWriter(File.Create(path), Encoding.UTF8))
            {
                streamWriter.Write(GetSubtitleText());
                streamWriter.Close();

            }
        }
    }
}
