using System;
using System.Xml;

namespace audpwlgen
{

	class audPendingWaveListGenerator
	{

		static void ProcessNode(XmlNode node, string platformName)
		{
			string platform = null;
			
			if(node.Name == "Wave" || node.Name == "Pack" || node.Name == "Bank" || node.Name == "BankFolder" || node.Name == "WaveFolder" || node.Name == "Tag")
			{
				string name = node.Attributes["name"].Value;
				if(node.Attributes["platform"] != null)
				{
					platform = platformName;
				}
				string val = null;
				if(node.Attributes["value"] != null)
				{
					val = node.Attributes["value"].Value;
				}

				node.Attributes.RemoveAll();

				node.Attributes.Append(node.OwnerDocument.CreateAttribute("name"));
				node.Attributes["name"].Value = name;
				if(val != null)
				{
					node.Attributes.Append(node.OwnerDocument.CreateAttribute("value"));
					node.Attributes["value"].Value = val;
				}
				if(platform != null)
				{
					node.Attributes.Append(node.OwnerDocument.CreateAttribute("platform"));
					node.Attributes["platform"].Value = platform;
				}

				node.Attributes.Append(node.OwnerDocument.CreateAttribute("operation"));
				node.Attributes["operation"].Value = "add";
			}

			for(int i = 0 ; i < node.ChildNodes.Count; i++)
			{
				ProcessNode(node.ChildNodes[i], platformName);
			}
		}

		static void BuildPendingWaveList(XmlDocument builtWaves, XmlDocument pendingWaves, string platformName)
		{
			XmlNode node = pendingWaves.ImportNode(builtWaves.DocumentElement, true);
			XmlNode parentNode = pendingWaves.CreateElement("PendingWaves");
			
			for(int i = 0 ; i < node.ChildNodes.Count; i++)
			{
				parentNode.AppendChild(node.ChildNodes[i].CloneNode(true));
			}

			pendingWaves.AppendChild(parentNode);

			for(int i = 0 ; i < parentNode.ChildNodes.Count; i++)
			{
				ProcessNode(parentNode.ChildNodes[i], platformName);
			}
		}

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{
			if(args.Length != 3)
			{
				Console.WriteLine("Usage:\npwlgen <path-to-BuiltWaveList.xml> <path-to-new-PendingWaveList.xml> <NewPlatformName>\n");
			}
			else
			{
				string builtWaves = args[0];
				string pendingWaves = args[1];
				string platformName = args[2];

				XmlDocument builtWavesDoc = new XmlDocument();
				builtWavesDoc.Load(builtWaves);
				XmlDocument pendingWavesDoc = new XmlDocument();
				

				BuildPendingWaveList(builtWavesDoc, pendingWavesDoc, platformName);

				pendingWavesDoc.Save(pendingWaves);
			}

			
		}
	}
}
