﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GranularPlayback
{
	public partial class WobbleGenerator : Form
	{
		class Wobble
		{
			public List<WobbleDataPoint> m_DataPoints;

			public Wobble()
			{
				m_DataPoints = new List<WobbleDataPoint>();

				for (int loop = 0; loop < 10; loop++)
				{
					WobbleDataPoint dataPoint = new WobbleDataPoint();
					dataPoint.grainIndex = loop;
					dataPoint.pitch = 0;
					dataPoint.volume = 1.0f;
					dataPoint.selected = false;

					m_DataPoints.Add(dataPoint);
				}
			}
		}

		class WobbleDataPoint
		{
			public int grainIndex;
			public float volume;
			public int pitch;
			public bool selected;
		};

		bool m_LeftMouseDown;
		RectangleF m_VisibleClipBoundsVol;
		RectangleF m_VisibleClipBoundsPitch;
		Point m_PrevMousePosition;
		List<Wobble> m_Wobbles;
		int m_CurrentWobbleIndex;

		public WobbleGenerator()
		{
			InitializeComponent();

			m_LeftMouseDown = false;
			m_Wobbles = new List<Wobble>();
			m_Wobbles.Add(new Wobble());
			m_CurrentWobbleIndex = 0;
			loopListBox.DataSource = m_Wobbles;
		}

		private void wobbleGraphVol_MouseDown(object sender, MouseEventArgs e)
		{
			m_PrevMousePosition = e.Location;
			m_LeftMouseDown = true;

			foreach (WobbleDataPoint dataPoint in m_Wobbles[m_CurrentWobbleIndex].m_DataPoints)
			{
				dataPoint.selected = false;
			}

			foreach (WobbleDataPoint dataPoint in m_Wobbles[m_CurrentWobbleIndex].m_DataPoints)
			{
				float grainFraction = dataPoint.grainIndex / (float)(m_Wobbles[m_CurrentWobbleIndex].m_DataPoints.Count - 1);
				float volumeFraction = dataPoint.volume / 2.0f;

				int rectangleWidth = 10;
				Point thisPoint = new Point((int)(m_VisibleClipBoundsVol.Width * grainFraction), (int)(m_VisibleClipBoundsVol.Height - (m_VisibleClipBoundsVol.Height * volumeFraction)));

				if (e.Location.X > thisPoint.X - rectangleWidth / 2 &&
				   e.Location.X < thisPoint.X + rectangleWidth / 2 &&
				   e.Location.Y < thisPoint.Y + rectangleWidth / 2 &&
				   e.Location.Y < thisPoint.Y + rectangleWidth / 2)
				{
					dataPoint.selected = true;
					break;
				}
			}
		}

		private void wobbleGraphPitch_MouseDown(object sender, MouseEventArgs e)
		{
			m_PrevMousePosition = e.Location;
			m_LeftMouseDown = true;

			foreach (WobbleDataPoint dataPoint in m_Wobbles[m_CurrentWobbleIndex].m_DataPoints)
			{
				dataPoint.selected = false;
			}

			foreach (WobbleDataPoint dataPoint in m_Wobbles[m_CurrentWobbleIndex].m_DataPoints)
			{
				float grainFraction = dataPoint.grainIndex / (float)(m_Wobbles[m_CurrentWobbleIndex].m_DataPoints.Count - 1);
				float volumeFraction = dataPoint.volume / 2.0f;

				int rectangleWidth = 10;
				Point thisPoint = new Point((int)(m_VisibleClipBoundsPitch.Width * grainFraction), (int)(m_VisibleClipBoundsPitch.Height - (m_VisibleClipBoundsPitch.Height * volumeFraction)));

				if (e.Location.X > thisPoint.X - rectangleWidth / 2 &&
				   e.Location.X < thisPoint.X + rectangleWidth / 2 &&
				   e.Location.Y < thisPoint.Y + rectangleWidth / 2 &&
				   e.Location.Y < thisPoint.Y + rectangleWidth / 2)
				{
					dataPoint.selected = true;
					break;
				}
			}
		}

		private void wobbleGraphVol_MouseUp(object sender, MouseEventArgs e)
		{
			m_LeftMouseDown = false;
		}

		private void wobbleGraphPitch_MouseUp(object sender, MouseEventArgs e)
		{
			m_LeftMouseDown = false;
		}

		private void wobbleGraphVol_MouseMove(object sender, MouseEventArgs e)
		{
			int xDelta = e.Location.X - m_PrevMousePosition.X;
			int yDelta = e.Location.Y - m_PrevMousePosition.Y;

			float volumeFraction = -(yDelta / m_VisibleClipBoundsVol.Height);
			float volumeChange = 2.0f * volumeFraction;

			foreach (WobbleDataPoint dataPoint in m_Wobbles[m_CurrentWobbleIndex].m_DataPoints)
			{
				if (dataPoint.selected && m_LeftMouseDown)
				{
					dataPoint.volume += volumeChange;

					if (dataPoint.volume > 2.0f)
					{
						dataPoint.volume = 2.0f;
					}
					if (dataPoint.volume < 0.0f)
					{
						dataPoint.volume = 0;
					}
				}
			}

			m_PrevMousePosition = e.Location;
		}

		private void wobbleGraphPitch_MouseMove(object sender, MouseEventArgs e)
		{
			int xDelta = e.Location.X - m_PrevMousePosition.X;
			int yDelta = e.Location.Y - m_PrevMousePosition.Y;

			float pitchFraction = -(yDelta / m_VisibleClipBoundsPitch.Height);
			int pitchChange = (int)(2400 * pitchFraction);

			foreach (WobbleDataPoint dataPoint in m_Wobbles[m_CurrentWobbleIndex].m_DataPoints)
			{
				if (dataPoint.selected && m_LeftMouseDown)
				{
					dataPoint.pitch += pitchChange;

					if (dataPoint.pitch > 1200)
					{
						dataPoint.volume = 1200;
					}
					if (dataPoint.volume < -1200)
					{
						dataPoint.volume = -1200;
					}
				}
			}

			m_PrevMousePosition = e.Location;
		}

		private void wobbleGraphVol_Paint(object sender, PaintEventArgs pea)
		{
			var grfx = pea.Graphics;
			m_VisibleClipBoundsVol = grfx.VisibleClipBounds;

			grfx.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb(200, 200, 200)),
				0,
				0,
				grfx.VisibleClipBounds.Width,
				grfx.VisibleClipBounds.Height);


			Font arial = new Font(new FontFamily("Arial"), 10, FontStyle.Regular);
			SolidBrush textBrush = new SolidBrush(Color.Black);

			grfx.DrawLine(Pens.Black, 0, m_VisibleClipBoundsVol.Height / 2, m_VisibleClipBoundsVol.Width, m_VisibleClipBoundsVol.Height / 2);

			Point prevPoint = new Point();

			foreach (WobbleDataPoint dataPoint in m_Wobbles[m_CurrentWobbleIndex].m_DataPoints)
			{
				float grainFraction = dataPoint.grainIndex / (float)(m_Wobbles[m_CurrentWobbleIndex].m_DataPoints.Count - 1);
				float volumeFraction = dataPoint.volume / 2.0f;

				int rectangleWidth = 10;
				Point thisPoint = new Point((int)(grfx.VisibleClipBounds.Width * grainFraction), (int)(grfx.VisibleClipBounds.Height - (grfx.VisibleClipBounds.Height * volumeFraction)));

				Pen pen = new Pen(Color.DarkOliveGreen);

				if (dataPoint.selected)
				{
					pen = new Pen(Color.Red);
				}
				
				if (m_LeftMouseDown && dataPoint.selected)
				{
					SolidBrush fillBrush = new SolidBrush(Color.Red);
					grfx.FillRectangle(fillBrush,
								 thisPoint.X - rectangleWidth / 2,
								 thisPoint.Y - rectangleWidth / 2,
								 rectangleWidth, rectangleWidth);
				}
				else
				{
					grfx.DrawRectangle(pen,
								 thisPoint.X - rectangleWidth / 2,
								 thisPoint.Y - rectangleWidth / 2,
								 rectangleWidth, rectangleWidth);
				}

				if (dataPoint != m_Wobbles[m_CurrentWobbleIndex].m_DataPoints.First())
				{
					grfx.DrawLine(new Pen(Color.DarkOrange), prevPoint, thisPoint);
				}

				prevPoint = thisPoint;
			}
		}

		private void wobbleGraphPitch_Paint(object sender, PaintEventArgs pea)
		{
			var grfx = pea.Graphics;
			m_VisibleClipBoundsPitch = grfx.VisibleClipBounds;

			grfx.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb(200, 200, 200)),
				0,
				0,
				grfx.VisibleClipBounds.Width,
				grfx.VisibleClipBounds.Height);


			Font arial = new Font(new FontFamily("Arial"), 10, FontStyle.Regular);
			SolidBrush textBrush = new SolidBrush(Color.Black);

			grfx.DrawLine(Pens.Black, 0, m_VisibleClipBoundsPitch.Height / 2, m_VisibleClipBoundsPitch.Width, m_VisibleClipBoundsPitch.Height / 2);

			Point prevPoint = new Point();

			foreach (WobbleDataPoint dataPoint in m_Wobbles[m_CurrentWobbleIndex].m_DataPoints)
			{
				float grainFraction = dataPoint.grainIndex / (float)(m_Wobbles[m_CurrentWobbleIndex].m_DataPoints.Count - 1);
				float pitchFraction = (dataPoint.pitch + 1200) / 2400.0f;

				int rectangleWidth = 10;
				Point thisPoint = new Point((int)(grfx.VisibleClipBounds.Width * grainFraction), (int)(grfx.VisibleClipBounds.Height - (grfx.VisibleClipBounds.Height * pitchFraction)));

				Pen pen = new Pen(Color.DarkOliveGreen);

				if (dataPoint.selected)
				{
					pen = new Pen(Color.Red);
				}

				if (m_LeftMouseDown && dataPoint.selected)
				{
					SolidBrush fillBrush = new SolidBrush(Color.Red);
					grfx.FillRectangle(fillBrush,
								 thisPoint.X - rectangleWidth / 2,
								 thisPoint.Y - rectangleWidth / 2,
								 rectangleWidth, rectangleWidth);
				}
				else
				{
					grfx.DrawRectangle(pen,
								 thisPoint.X - rectangleWidth / 2,
								 thisPoint.Y - rectangleWidth / 2,
								 rectangleWidth, rectangleWidth);
				}

				if (dataPoint != m_Wobbles[m_CurrentWobbleIndex].m_DataPoints.First())
				{
					grfx.DrawLine(new Pen(Color.DarkOrange), prevPoint, thisPoint);
				}

				prevPoint = thisPoint;
			}
		}

		private void volumeAxis_Paint(object sender, PaintEventArgs pea)
		{
			var grfx = pea.Graphics;
			var visBounds = grfx.VisibleClipBounds;
			Font arial = new Font(new FontFamily("Arial"), 7, FontStyle.Regular);
			SolidBrush textBrush = new SolidBrush(Color.Black);

			for (int loop = 0; loop < 20; loop += 4)
			{
				float yCoord = visBounds.Height - ((visBounds.Height / 20) * loop);
				float thisPitch = loop;

				string timeString = String.Format("{0:0.###}", thisPitch - 10);
				grfx.DrawString(timeString, arial, textBrush, new PointF((visBounds.Width / 2) - 3, yCoord + 4));
				grfx.DrawLine(Pens.Black, visBounds.Width / 2, yCoord, visBounds.Width, yCoord);
			}
		}

		private void pitchAxis_Paint(object sender, PaintEventArgs pea)
		{
			var grfx = pea.Graphics;
			var visBounds = grfx.VisibleClipBounds;
			Font arial = new Font(new FontFamily("Arial"), 7, FontStyle.Regular);
			SolidBrush textBrush = new SolidBrush(Color.Black);

			for (int loop = 0; loop < 2400; loop += 400)
			{
				float yCoord = visBounds.Height - ((visBounds.Height / 2400) * loop);
				float thisPitch = loop;

				string timeString = String.Format("{0:0.###}", thisPitch - 1200);
				grfx.DrawString(timeString, arial, textBrush, new PointF((visBounds.Width / 2) - 3, yCoord + 4));
				grfx.DrawLine(Pens.Black, visBounds.Width / 2, yCoord, visBounds.Width, yCoord);
			}
		}

		private void timer1_Tick(object sender, EventArgs e)
		{
			wobbleGraphVol.Refresh();
			wobbleGraphPitch.Refresh();
			volumeAxis.Refresh();
			pitchAxis.Refresh();
		}

		private void button2_Click(object sender, EventArgs e)
		{
			WobbleDataPoint newPoint = new WobbleDataPoint();
			newPoint.grainIndex = m_Wobbles[m_CurrentWobbleIndex].m_DataPoints.Count;
			newPoint.volume = 1.0f;
			newPoint.pitch = 0;
			m_Wobbles[m_CurrentWobbleIndex].m_DataPoints.Add(newPoint);
		}

		private void button1_Click(object sender, EventArgs e)
		{
			if (m_Wobbles[m_CurrentWobbleIndex].m_DataPoints.Count > 2)
			{
				m_Wobbles[m_CurrentWobbleIndex].m_DataPoints.RemoveAt(m_Wobbles[m_CurrentWobbleIndex].m_DataPoints.Count - 1);
			}
		}

		private void button3_Click(object sender, EventArgs e)
		{
			m_Wobbles.Add(new Wobble());
		}

		private void button4_Click(object sender, EventArgs e)
		{
			m_Wobbles.Remove(m_Wobbles[m_CurrentWobbleIndex]);
		}
	}
}
