﻿namespace GranularPlayback
{
	partial class WobbleGenerator
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.wobbleGraphVol = new System.Windows.Forms.PictureBox();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.wobbleGraphPitch = new System.Windows.Forms.PictureBox();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.volumeAxis = new System.Windows.Forms.PictureBox();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.pitchAxis = new System.Windows.Forms.PictureBox();
			this.loopListBox = new System.Windows.Forms.ListBox();
			this.button3 = new System.Windows.Forms.Button();
			this.button4 = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.wobbleGraphVol)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.wobbleGraphPitch)).BeginInit();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.volumeAxis)).BeginInit();
			this.groupBox2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pitchAxis)).BeginInit();
			this.SuspendLayout();
			// 
			// wobbleGraphVol
			// 
			this.wobbleGraphVol.Location = new System.Drawing.Point(186, 37);
			this.wobbleGraphVol.Name = "wobbleGraphVol";
			this.wobbleGraphVol.Size = new System.Drawing.Size(789, 191);
			this.wobbleGraphVol.TabIndex = 0;
			this.wobbleGraphVol.TabStop = false;
			this.wobbleGraphVol.MouseMove += new System.Windows.Forms.MouseEventHandler(this.wobbleGraphVol_MouseMove);
			this.wobbleGraphVol.MouseDown += new System.Windows.Forms.MouseEventHandler(this.wobbleGraphVol_MouseDown);
			this.wobbleGraphVol.Paint += new System.Windows.Forms.PaintEventHandler(this.wobbleGraphVol_Paint);
			this.wobbleGraphVol.MouseUp += new System.Windows.Forms.MouseEventHandler(this.wobbleGraphVol_MouseUp);
			// 
			// timer1
			// 
			this.timer1.Enabled = true;
			this.timer1.Interval = 10;
			this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(871, 481);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(52, 30);
			this.button1.TabIndex = 1;
			this.button1.Text = "-";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(929, 481);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(52, 30);
			this.button2.TabIndex = 2;
			this.button2.Text = "+";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// wobbleGraphPitch
			// 
			this.wobbleGraphPitch.Location = new System.Drawing.Point(186, 259);
			this.wobbleGraphPitch.Name = "wobbleGraphPitch";
			this.wobbleGraphPitch.Size = new System.Drawing.Size(789, 210);
			this.wobbleGraphPitch.TabIndex = 3;
			this.wobbleGraphPitch.TabStop = false;
			this.wobbleGraphPitch.MouseMove += new System.Windows.Forms.MouseEventHandler(this.wobbleGraphPitch_MouseMove);
			this.wobbleGraphPitch.MouseDown += new System.Windows.Forms.MouseEventHandler(this.wobbleGraphPitch_MouseDown);
			this.wobbleGraphPitch.Paint += new System.Windows.Forms.PaintEventHandler(this.wobbleGraphPitch_Paint);
			this.wobbleGraphPitch.MouseUp += new System.Windows.Forms.MouseEventHandler(this.wobbleGraphPitch_MouseUp);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.volumeAxis);
			this.groupBox1.Location = new System.Drawing.Point(139, 18);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(842, 216);
			this.groupBox1.TabIndex = 4;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Volume";
			// 
			// volumeAxis
			// 
			this.volumeAxis.Location = new System.Drawing.Point(6, 19);
			this.volumeAxis.Name = "volumeAxis";
			this.volumeAxis.Size = new System.Drawing.Size(42, 191);
			this.volumeAxis.TabIndex = 0;
			this.volumeAxis.TabStop = false;
			this.volumeAxis.Paint += new System.Windows.Forms.PaintEventHandler(this.volumeAxis_Paint);
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.pitchAxis);
			this.groupBox2.Location = new System.Drawing.Point(139, 240);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(842, 235);
			this.groupBox2.TabIndex = 5;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Pitch";
			// 
			// pitchAxis
			// 
			this.pitchAxis.Location = new System.Drawing.Point(6, 19);
			this.pitchAxis.Name = "pitchAxis";
			this.pitchAxis.Size = new System.Drawing.Size(42, 210);
			this.pitchAxis.TabIndex = 6;
			this.pitchAxis.TabStop = false;
			this.pitchAxis.Paint += new System.Windows.Forms.PaintEventHandler(this.pitchAxis_Paint);
			// 
			// loopListBox
			// 
			this.loopListBox.FormattingEnabled = true;
			this.loopListBox.Location = new System.Drawing.Point(12, 24);
			this.loopListBox.Name = "loopListBox";
			this.loopListBox.Size = new System.Drawing.Size(121, 446);
			this.loopListBox.TabIndex = 6;
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(12, 476);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(121, 24);
			this.button3.TabIndex = 7;
			this.button3.Text = "Add Wobble";
			this.button3.UseVisualStyleBackColor = true;
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// button4
			// 
			this.button4.Location = new System.Drawing.Point(12, 506);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(121, 24);
			this.button4.TabIndex = 8;
			this.button4.Text = "Delete Wobble";
			this.button4.UseVisualStyleBackColor = true;
			this.button4.Click += new System.EventHandler(this.button4_Click);
			// 
			// WobbleGenerator
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(994, 601);
			this.Controls.Add(this.button4);
			this.Controls.Add(this.button3);
			this.Controls.Add(this.loopListBox);
			this.Controls.Add(this.wobbleGraphPitch);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.wobbleGraphVol);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.groupBox2);
			this.Name = "WobbleGenerator";
			this.Text = "WobbleGenerator";
			((System.ComponentModel.ISupportInitialize)(this.wobbleGraphVol)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.wobbleGraphPitch)).EndInit();
			this.groupBox1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.volumeAxis)).EndInit();
			this.groupBox2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.pitchAxis)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.PictureBox wobbleGraphVol;
		private System.Windows.Forms.Timer timer1;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.PictureBox wobbleGraphPitch;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.PictureBox volumeAxis;
		private System.Windows.Forms.PictureBox pitchAxis;
		private System.Windows.Forms.ListBox loopListBox;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Button button4;
	}
}