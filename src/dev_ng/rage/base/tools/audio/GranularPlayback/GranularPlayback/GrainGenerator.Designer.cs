﻿namespace GranularPlayback
{
	partial class GrainGenerator
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.granuleProgressBar = new System.Windows.Forms.ProgressBar();
			this.zeroCrossingBiasCombo = new System.Windows.Forms.NumericUpDown();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.startEndBiasCombo = new System.Windows.Forms.NumericUpDown();
			this.generateGranulesBtn = new System.Windows.Forms.Button();
			this.loadPitchDataDlg = new System.Windows.Forms.OpenFileDialog();
			this.label3 = new System.Windows.Forms.Label();
			this.searchPercentageUpDown = new System.Windows.Forms.NumericUpDown();
			this.redrawTimer = new System.Windows.Forms.Timer(this.components);
			this.pitchView = new System.Windows.Forms.PictureBox();
			this.pitchViewXAxis = new System.Windows.Forms.PictureBox();
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.loadPitchDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.loadPitchDataToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.savePitchDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.quitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.undoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.prevGrainView = new System.Windows.Forms.PictureBox();
			this.fixedWidthComboBox = new System.Windows.Forms.CheckBox();
			this.label4 = new System.Windows.Forms.Label();
			this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.smoothSelectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.smoothMoreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.flattenSelectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.savePitchDataDlg = new System.Windows.Forms.SaveFileDialog();
			this.grainPreviewLabel = new System.Windows.Forms.Label();
			this.pitchViewYAxis = new System.Windows.Forms.PictureBox();
			this.showPreviewCheckBox = new System.Windows.Forms.CheckBox();
			this.expectedMatchBiasUpDown = new System.Windows.Forms.NumericUpDown();
			this.label5 = new System.Windows.Forms.Label();
			this.lowerPitchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.raisePitchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			((System.ComponentModel.ISupportInitialize)(this.zeroCrossingBiasCombo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.startEndBiasCombo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.searchPercentageUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pitchView)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pitchViewXAxis)).BeginInit();
			this.menuStrip1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.prevGrainView)).BeginInit();
			this.contextMenuStrip.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pitchViewYAxis)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.expectedMatchBiasUpDown)).BeginInit();
			this.SuspendLayout();
			// 
			// granuleProgressBar
			// 
			this.granuleProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.granuleProgressBar.Location = new System.Drawing.Point(12, 475);
			this.granuleProgressBar.Name = "granuleProgressBar";
			this.granuleProgressBar.Size = new System.Drawing.Size(1145, 34);
			this.granuleProgressBar.Step = 1;
			this.granuleProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
			this.granuleProgressBar.TabIndex = 0;
			// 
			// zeroCrossingBiasCombo
			// 
			this.zeroCrossingBiasCombo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.zeroCrossingBiasCombo.DecimalPlaces = 2;
			this.zeroCrossingBiasCombo.Location = new System.Drawing.Point(1092, 521);
			this.zeroCrossingBiasCombo.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
			this.zeroCrossingBiasCombo.Name = "zeroCrossingBiasCombo";
			this.zeroCrossingBiasCombo.Size = new System.Drawing.Size(65, 20);
			this.zeroCrossingBiasCombo.TabIndex = 5;
			this.zeroCrossingBiasCombo.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// label1
			// 
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(991, 523);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(95, 13);
			this.label1.TabIndex = 2;
			this.label1.Text = "Zero Crossing Bias";
			// 
			// label2
			// 
			this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(837, 523);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(76, 13);
			this.label2.TabIndex = 4;
			this.label2.Text = "Start/End Bias";
			// 
			// startEndBiasCombo
			// 
			this.startEndBiasCombo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.startEndBiasCombo.DecimalPlaces = 2;
			this.startEndBiasCombo.Location = new System.Drawing.Point(919, 521);
			this.startEndBiasCombo.Name = "startEndBiasCombo";
			this.startEndBiasCombo.Size = new System.Drawing.Size(66, 20);
			this.startEndBiasCombo.TabIndex = 4;
			this.startEndBiasCombo.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// generateGranulesBtn
			// 
			this.generateGranulesBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.generateGranulesBtn.Location = new System.Drawing.Point(12, 515);
			this.generateGranulesBtn.Name = "generateGranulesBtn";
			this.generateGranulesBtn.Size = new System.Drawing.Size(131, 28);
			this.generateGranulesBtn.TabIndex = 2;
			this.generateGranulesBtn.Text = "Go!";
			this.generateGranulesBtn.UseVisualStyleBackColor = true;
			this.generateGranulesBtn.Click += new System.EventHandler(this.generateGranulesBtn_Click);
			// 
			// loadPitchDataDlg
			// 
			this.loadPitchDataDlg.Filter = "Pitch analysis file (*.paf)|*paf|MATLAB Pitch analysis file (*.csv)|*.csv";
			this.loadPitchDataDlg.FileOk += new System.ComponentModel.CancelEventHandler(this.loadPitchDataDlg_FileOk);
			// 
			// label3
			// 
			this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(628, 523);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(102, 13);
			this.label3.TabIndex = 7;
			this.label3.Text = "Search Freedom (%)";
			// 
			// searchPercentageUpDown
			// 
			this.searchPercentageUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.searchPercentageUpDown.DecimalPlaces = 2;
			this.searchPercentageUpDown.Location = new System.Drawing.Point(736, 521);
			this.searchPercentageUpDown.Name = "searchPercentageUpDown";
			this.searchPercentageUpDown.Size = new System.Drawing.Size(84, 20);
			this.searchPercentageUpDown.TabIndex = 3;
			this.searchPercentageUpDown.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
			// 
			// redrawTimer
			// 
			this.redrawTimer.Enabled = true;
			this.redrawTimer.Interval = 10;
			this.redrawTimer.Tick += new System.EventHandler(this.redrawTimer_Tick);
			// 
			// pitchView
			// 
			this.pitchView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.pitchView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.pitchView.Location = new System.Drawing.Point(46, 47);
			this.pitchView.Name = "pitchView";
			this.pitchView.Size = new System.Drawing.Size(1111, 394);
			this.pitchView.TabIndex = 9;
			this.pitchView.TabStop = false;
			this.pitchView.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pitchView_MouseMove);
			this.pitchView.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pitchView_MouseDown);
			this.pitchView.Paint += new System.Windows.Forms.PaintEventHandler(this.pitchView_Paint);
			this.pitchView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pitchView_MouseUp);
			// 
			// pitchViewXAxis
			// 
			this.pitchViewXAxis.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.pitchViewXAxis.Location = new System.Drawing.Point(46, 440);
			this.pitchViewXAxis.Name = "pitchViewXAxis";
			this.pitchViewXAxis.Size = new System.Drawing.Size(1111, 35);
			this.pitchViewXAxis.TabIndex = 10;
			this.pitchViewXAxis.TabStop = false;
			this.pitchViewXAxis.Paint += new System.Windows.Forms.PaintEventHandler(this.pitchViewXAxis_Paint);
			// 
			// menuStrip1
			// 
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadPitchDataToolStripMenuItem,
            this.editToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(1169, 24);
			this.menuStrip1.TabIndex = 13;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// loadPitchDataToolStripMenuItem
			// 
			this.loadPitchDataToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadPitchDataToolStripMenuItem1,
            this.savePitchDataToolStripMenuItem,
            this.quitToolStripMenuItem});
			this.loadPitchDataToolStripMenuItem.Name = "loadPitchDataToolStripMenuItem";
			this.loadPitchDataToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
			this.loadPitchDataToolStripMenuItem.Text = "File";
			// 
			// loadPitchDataToolStripMenuItem1
			// 
			this.loadPitchDataToolStripMenuItem1.Name = "loadPitchDataToolStripMenuItem1";
			this.loadPitchDataToolStripMenuItem1.Size = new System.Drawing.Size(157, 22);
			this.loadPitchDataToolStripMenuItem1.Text = "Load Pitch Data";
			this.loadPitchDataToolStripMenuItem1.Click += new System.EventHandler(this.loadPitchDataToolStripMenuItem1_Click);
			// 
			// savePitchDataToolStripMenuItem
			// 
			this.savePitchDataToolStripMenuItem.Name = "savePitchDataToolStripMenuItem";
			this.savePitchDataToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
			this.savePitchDataToolStripMenuItem.Text = "Save Pitch Data";
			this.savePitchDataToolStripMenuItem.Click += new System.EventHandler(this.savePitchDataToolStripMenuItem_Click);
			// 
			// quitToolStripMenuItem
			// 
			this.quitToolStripMenuItem.Name = "quitToolStripMenuItem";
			this.quitToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
			this.quitToolStripMenuItem.Text = "Quit";
			this.quitToolStripMenuItem.Click += new System.EventHandler(this.quitToolStripMenuItem_Click);
			// 
			// editToolStripMenuItem
			// 
			this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.undoToolStripMenuItem});
			this.editToolStripMenuItem.Name = "editToolStripMenuItem";
			this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
			this.editToolStripMenuItem.Text = "Edit";
			// 
			// undoToolStripMenuItem
			// 
			this.undoToolStripMenuItem.Name = "undoToolStripMenuItem";
			this.undoToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
			this.undoToolStripMenuItem.Text = "Undo";
			this.undoToolStripMenuItem.Click += new System.EventHandler(this.undoToolStripMenuItem_Click);
			// 
			// prevGrainView
			// 
			this.prevGrainView.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.prevGrainView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.prevGrainView.Location = new System.Drawing.Point(820, 279);
			this.prevGrainView.Name = "prevGrainView";
			this.prevGrainView.Size = new System.Drawing.Size(328, 155);
			this.prevGrainView.TabIndex = 14;
			this.prevGrainView.TabStop = false;
			this.prevGrainView.Paint += new System.Windows.Forms.PaintEventHandler(this.prevGrainView_Paint);
			// 
			// fixedWidthComboBox
			// 
			this.fixedWidthComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.fixedWidthComboBox.AutoSize = true;
			this.fixedWidthComboBox.Location = new System.Drawing.Point(1029, 408);
			this.fixedWidthComboBox.Name = "fixedWidthComboBox";
			this.fixedWidthComboBox.Size = new System.Drawing.Size(110, 17);
			this.fixedWidthComboBox.TabIndex = 1;
			this.fixedWidthComboBox.Text = "Draw Fixed Width";
			this.fixedWidthComboBox.UseVisualStyleBackColor = true;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(43, 31);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(107, 13);
			this.label4.TabIndex = 16;
			this.label4.Text = "Pitch (Hz) vs Time (s)";
			// 
			// contextMenuStrip
			// 
			this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.smoothSelectionToolStripMenuItem,
            this.smoothMoreToolStripMenuItem,
            this.flattenSelectionToolStripMenuItem,
            this.lowerPitchToolStripMenuItem,
            this.raisePitchToolStripMenuItem});
			this.contextMenuStrip.Name = "contextMenuStrip";
			this.contextMenuStrip.Size = new System.Drawing.Size(153, 136);
			// 
			// smoothSelectionToolStripMenuItem
			// 
			this.smoothSelectionToolStripMenuItem.Name = "smoothSelectionToolStripMenuItem";
			this.smoothSelectionToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
			this.smoothSelectionToolStripMenuItem.Text = "Smooth";
			this.smoothSelectionToolStripMenuItem.Click += new System.EventHandler(this.smoothSelectionToolStripMenuItem_Click);
			// 
			// smoothMoreToolStripMenuItem
			// 
			this.smoothMoreToolStripMenuItem.Name = "smoothMoreToolStripMenuItem";
			this.smoothMoreToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
			this.smoothMoreToolStripMenuItem.Text = "Smooth More";
			this.smoothMoreToolStripMenuItem.Click += new System.EventHandler(this.smoothMoreToolStripMenuItem_Click);
			// 
			// flattenSelectionToolStripMenuItem
			// 
			this.flattenSelectionToolStripMenuItem.Name = "flattenSelectionToolStripMenuItem";
			this.flattenSelectionToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
			this.flattenSelectionToolStripMenuItem.Text = "Flatten";
			this.flattenSelectionToolStripMenuItem.Click += new System.EventHandler(this.flattenSelectionToolStripMenuItem_Click);
			// 
			// savePitchDataDlg
			// 
			this.savePitchDataDlg.Filter = "Pitch analysis file (*.paf)|*.paf";
			this.savePitchDataDlg.FileOk += new System.ComponentModel.CancelEventHandler(this.savePitchDataDlg_FileOk);
			// 
			// grainPreviewLabel
			// 
			this.grainPreviewLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.grainPreviewLabel.AutoSize = true;
			this.grainPreviewLabel.Location = new System.Drawing.Point(838, 290);
			this.grainPreviewLabel.Name = "grainPreviewLabel";
			this.grainPreviewLabel.Size = new System.Drawing.Size(73, 13);
			this.grainPreviewLabel.TabIndex = 18;
			this.grainPreviewLabel.Text = "Grain Preview";
			// 
			// pitchViewYAxis
			// 
			this.pitchViewYAxis.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)));
			this.pitchViewYAxis.Location = new System.Drawing.Point(12, 47);
			this.pitchViewYAxis.Name = "pitchViewYAxis";
			this.pitchViewYAxis.Size = new System.Drawing.Size(34, 393);
			this.pitchViewYAxis.TabIndex = 11;
			this.pitchViewYAxis.TabStop = false;
			this.pitchViewYAxis.Paint += new System.Windows.Forms.PaintEventHandler(this.pitchViewYAxis_Paint);
			// 
			// showPreviewCheckBox
			// 
			this.showPreviewCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.showPreviewCheckBox.AutoSize = true;
			this.showPreviewCheckBox.Checked = true;
			this.showPreviewCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.showPreviewCheckBox.Location = new System.Drawing.Point(149, 522);
			this.showPreviewCheckBox.Name = "showPreviewCheckBox";
			this.showPreviewCheckBox.Size = new System.Drawing.Size(122, 17);
			this.showPreviewCheckBox.TabIndex = 19;
			this.showPreviewCheckBox.Text = "Show Grain Preview";
			this.showPreviewCheckBox.UseVisualStyleBackColor = true;
			this.showPreviewCheckBox.CheckedChanged += new System.EventHandler(this.showPreviewCheckBox_CheckedChanged);
			// 
			// expectedMatchBiasUpDown
			// 
			this.expectedMatchBiasUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.expectedMatchBiasUpDown.DecimalPlaces = 2;
			this.expectedMatchBiasUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.expectedMatchBiasUpDown.Location = new System.Drawing.Point(538, 521);
			this.expectedMatchBiasUpDown.Name = "expectedMatchBiasUpDown";
			this.expectedMatchBiasUpDown.Size = new System.Drawing.Size(84, 20);
			this.expectedMatchBiasUpDown.TabIndex = 20;
			// 
			// label5
			// 
			this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(424, 523);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(108, 13);
			this.label5.TabIndex = 21;
			this.label5.Text = "Expected Match Bias";
			// 
			// lowerPitchToolStripMenuItem
			// 
			this.lowerPitchToolStripMenuItem.Name = "lowerPitchToolStripMenuItem";
			this.lowerPitchToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
			this.lowerPitchToolStripMenuItem.Text = "Lower Pitch";
			this.lowerPitchToolStripMenuItem.Click += new System.EventHandler(this.lowerPitchToolStripMenuItem_Click);
			// 
			// raisePitchToolStripMenuItem
			// 
			this.raisePitchToolStripMenuItem.Name = "raisePitchToolStripMenuItem";
			this.raisePitchToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
			this.raisePitchToolStripMenuItem.Text = "Raise Pitch";
			this.raisePitchToolStripMenuItem.Click += new System.EventHandler(this.raisePitchToolStripMenuItem_Click);
			// 
			// GrainGenerator
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange;
			this.ClientSize = new System.Drawing.Size(1169, 555);
			this.Controls.Add(this.expectedMatchBiasUpDown);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.showPreviewCheckBox);
			this.Controls.Add(this.menuStrip1);
			this.Controls.Add(this.grainPreviewLabel);
			this.Controls.Add(this.pitchViewYAxis);
			this.Controls.Add(this.fixedWidthComboBox);
			this.Controls.Add(this.prevGrainView);
			this.Controls.Add(this.searchPercentageUpDown);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.pitchView);
			this.Controls.Add(this.generateGranulesBtn);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.startEndBiasCombo);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.zeroCrossingBiasCombo);
			this.Controls.Add(this.granuleProgressBar);
			this.Controls.Add(this.pitchViewXAxis);
			this.MainMenuStrip = this.menuStrip1;
			this.Name = "GrainGenerator";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Grain Generator";
			((System.ComponentModel.ISupportInitialize)(this.zeroCrossingBiasCombo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.startEndBiasCombo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.searchPercentageUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pitchView)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pitchViewXAxis)).EndInit();
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.prevGrainView)).EndInit();
			this.contextMenuStrip.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.pitchViewYAxis)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.expectedMatchBiasUpDown)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ProgressBar granuleProgressBar;
		private System.Windows.Forms.NumericUpDown zeroCrossingBiasCombo;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.NumericUpDown startEndBiasCombo;
		private System.Windows.Forms.Button generateGranulesBtn;
		private System.Windows.Forms.OpenFileDialog loadPitchDataDlg;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.NumericUpDown searchPercentageUpDown;
		private System.Windows.Forms.Timer redrawTimer;
		private System.Windows.Forms.PictureBox pitchView;
		private System.Windows.Forms.PictureBox pitchViewXAxis;
		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.ToolStripMenuItem loadPitchDataToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem loadPitchDataToolStripMenuItem1;
		private System.Windows.Forms.PictureBox prevGrainView;
		private System.Windows.Forms.CheckBox fixedWidthComboBox;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
		private System.Windows.Forms.ToolStripMenuItem flattenSelectionToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem smoothSelectionToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem savePitchDataToolStripMenuItem;
		private System.Windows.Forms.SaveFileDialog savePitchDataDlg;
		private System.Windows.Forms.ToolStripMenuItem quitToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem smoothMoreToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem undoToolStripMenuItem;
		private System.Windows.Forms.Label grainPreviewLabel;
		private System.Windows.Forms.PictureBox pitchViewYAxis;
		private System.Windows.Forms.CheckBox showPreviewCheckBox;
		private System.Windows.Forms.NumericUpDown expectedMatchBiasUpDown;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.ToolStripMenuItem lowerPitchToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem raisePitchToolStripMenuItem;
	}
}