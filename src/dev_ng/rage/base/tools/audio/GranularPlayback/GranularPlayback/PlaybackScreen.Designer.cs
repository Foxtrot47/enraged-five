﻿namespace GranularPlayback
{
	partial class PlaybackScreen
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PlaybackScreen));
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.fileMenu = new System.Windows.Forms.ToolStripMenuItem();
			this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.loadWaveFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.loadGrainDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.loadLoopDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.saveGrainDataToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.saveLoopDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.quitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.analyseZeroCrossingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.pitchAnalysisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.engineSimulatorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.wobbleGeneratorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.loopsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.clearAllLoopsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.selectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.selectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.reduceSelectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.reduceBy75ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.reduceBy66ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.reduceBy50ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.reduceBy33ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.reduceBy25ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.removeUnselectedGrainsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.debugToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.generateTestSweepToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.generateFlattenedSweepToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.clearGrainDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.loadWaveDlg = new System.Windows.Forms.OpenFileDialog();
			this.waveView = new System.Windows.Forms.PictureBox();
			this.redrawTimer = new System.Windows.Forms.Timer(this.components);
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.waveViewTimeAxis = new System.Windows.Forms.PictureBox();
			this.loadGrainDlg = new System.Windows.Forms.OpenFileDialog();
			this.drawGrainBoundaryCheck = new System.Windows.Forms.CheckBox();
			this.drawSamplePositionsCheckbox = new System.Windows.Forms.CheckBox();
			this.playGrainButton = new System.Windows.Forms.Button();
			this.playbackOrderCombo = new System.Windows.Forms.ComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.saveGrainDlg = new System.Windows.Forms.SaveFileDialog();
			this.rightClickMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.markAsLoopToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.deleteLoopToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.applyNameHashToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.drawLoopPositionsCheck = new System.Windows.Forms.CheckBox();
			this.flattenedWavFileDialog = new System.Windows.Forms.SaveFileDialog();
			this.saveLoopData = new System.Windows.Forms.SaveFileDialog();
			this.loadLoopData = new System.Windows.Forms.OpenFileDialog();
			this.grainPlaybackStyle = new System.Windows.Forms.ComboBox();
			this.label3 = new System.Windows.Forms.Label();
			this.flattenedRPMValue = new System.Windows.Forms.NumericUpDown();
			this.label4 = new System.Windows.Forms.Label();
			this.sampleInterpolationCombo = new System.Windows.Forms.ComboBox();
			this.label2 = new System.Windows.Forms.Label();
			this.submixNameHash = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.chopSelectedGrainsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.menuStrip1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.waveView)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.waveViewTimeAxis)).BeginInit();
			this.rightClickMenu.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.flattenedRPMValue)).BeginInit();
			this.SuspendLayout();
			// 
			// menuStrip1
			// 
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenu,
            this.toolsToolStripMenuItem,
            this.loopsToolStripMenuItem,
            this.selectionToolStripMenuItem,
            this.debugToolStripMenuItem,
            this.helpToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(1123, 24);
			this.menuStrip1.TabIndex = 0;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// fileMenu
			// 
			this.fileMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.quitToolStripMenuItem});
			this.fileMenu.Name = "fileMenu";
			this.fileMenu.Size = new System.Drawing.Size(37, 20);
			this.fileMenu.Text = "File";
			// 
			// loadToolStripMenuItem
			// 
			this.loadToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadWaveFileToolStripMenuItem,
            this.loadGrainDataToolStripMenuItem,
            this.loadLoopDataToolStripMenuItem});
			this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
			this.loadToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
			this.loadToolStripMenuItem.Text = "Load";
			// 
			// loadWaveFileToolStripMenuItem
			// 
			this.loadWaveFileToolStripMenuItem.Name = "loadWaveFileToolStripMenuItem";
			this.loadWaveFileToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
			this.loadWaveFileToolStripMenuItem.Text = "Load Wave File";
			this.loadWaveFileToolStripMenuItem.Click += new System.EventHandler(this.loadWaveFileToolStripMenuItem_Click);
			// 
			// loadGrainDataToolStripMenuItem
			// 
			this.loadGrainDataToolStripMenuItem.Name = "loadGrainDataToolStripMenuItem";
			this.loadGrainDataToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
			this.loadGrainDataToolStripMenuItem.Text = "Load Grain Data";
			this.loadGrainDataToolStripMenuItem.Click += new System.EventHandler(this.loadGrainDataToolStripMenuItem_Click);
			// 
			// loadLoopDataToolStripMenuItem
			// 
			this.loadLoopDataToolStripMenuItem.Name = "loadLoopDataToolStripMenuItem";
			this.loadLoopDataToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
			this.loadLoopDataToolStripMenuItem.Text = "Load Loop Data";
			this.loadLoopDataToolStripMenuItem.Click += new System.EventHandler(this.loadLoopDataToolStripMenuItem_Click);
			// 
			// saveToolStripMenuItem
			// 
			this.saveToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveGrainDataToolStripMenuItem1,
            this.saveLoopDataToolStripMenuItem});
			this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
			this.saveToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
			this.saveToolStripMenuItem.Text = "Save";
			// 
			// saveGrainDataToolStripMenuItem1
			// 
			this.saveGrainDataToolStripMenuItem1.Name = "saveGrainDataToolStripMenuItem1";
			this.saveGrainDataToolStripMenuItem1.Size = new System.Drawing.Size(156, 22);
			this.saveGrainDataToolStripMenuItem1.Text = "Save Grain Data";
			this.saveGrainDataToolStripMenuItem1.Click += new System.EventHandler(this.saveGrainDataToolStripMenuItem1_Click);
			// 
			// saveLoopDataToolStripMenuItem
			// 
			this.saveLoopDataToolStripMenuItem.Name = "saveLoopDataToolStripMenuItem";
			this.saveLoopDataToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
			this.saveLoopDataToolStripMenuItem.Text = "Save Loop Data";
			this.saveLoopDataToolStripMenuItem.Click += new System.EventHandler(this.saveLoopDataToolStripMenuItem_Click);
			// 
			// quitToolStripMenuItem
			// 
			this.quitToolStripMenuItem.Name = "quitToolStripMenuItem";
			this.quitToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
			this.quitToolStripMenuItem.Text = "Quit";
			this.quitToolStripMenuItem.Click += new System.EventHandler(this.quitToolStripMenuItem_Click);
			// 
			// toolsToolStripMenuItem
			// 
			this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.analyseZeroCrossingsToolStripMenuItem,
            this.pitchAnalysisToolStripMenuItem,
            this.engineSimulatorToolStripMenuItem,
            this.wobbleGeneratorToolStripMenuItem});
			this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
			this.toolsToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
			this.toolsToolStripMenuItem.Text = "Tools";
			// 
			// analyseZeroCrossingsToolStripMenuItem
			// 
			this.analyseZeroCrossingsToolStripMenuItem.Name = "analyseZeroCrossingsToolStripMenuItem";
			this.analyseZeroCrossingsToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
			this.analyseZeroCrossingsToolStripMenuItem.Text = "Grain Generator";
			this.analyseZeroCrossingsToolStripMenuItem.Click += new System.EventHandler(this.analyseZeroCrossingsToolStripMenuItem_Click);
			// 
			// pitchAnalysisToolStripMenuItem
			// 
			this.pitchAnalysisToolStripMenuItem.Name = "pitchAnalysisToolStripMenuItem";
			this.pitchAnalysisToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
			this.pitchAnalysisToolStripMenuItem.Text = "Pitch Analysis";
			this.pitchAnalysisToolStripMenuItem.Click += new System.EventHandler(this.pitchAnalysisToolStripMenuItem_Click);
			// 
			// engineSimulatorToolStripMenuItem
			// 
			this.engineSimulatorToolStripMenuItem.Name = "engineSimulatorToolStripMenuItem";
			this.engineSimulatorToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
			this.engineSimulatorToolStripMenuItem.Text = "Engine Simulator";
			this.engineSimulatorToolStripMenuItem.Click += new System.EventHandler(this.engineSimulatorToolStripMenuItem_Click);
			// 
			// wobbleGeneratorToolStripMenuItem
			// 
			this.wobbleGeneratorToolStripMenuItem.Name = "wobbleGeneratorToolStripMenuItem";
			this.wobbleGeneratorToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
			this.wobbleGeneratorToolStripMenuItem.Text = "Wobble Generator";
			this.wobbleGeneratorToolStripMenuItem.Click += new System.EventHandler(this.wobbleGeneratorToolStripMenuItem_Click);
			// 
			// loopsToolStripMenuItem
			// 
			this.loopsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clearAllLoopsToolStripMenuItem});
			this.loopsToolStripMenuItem.Name = "loopsToolStripMenuItem";
			this.loopsToolStripMenuItem.Size = new System.Drawing.Size(51, 20);
			this.loopsToolStripMenuItem.Text = "Loops";
			// 
			// clearAllLoopsToolStripMenuItem
			// 
			this.clearAllLoopsToolStripMenuItem.Name = "clearAllLoopsToolStripMenuItem";
			this.clearAllLoopsToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
			this.clearAllLoopsToolStripMenuItem.Text = "Clear All Loops";
			this.clearAllLoopsToolStripMenuItem.Click += new System.EventHandler(this.clearAllLoopsToolStripMenuItem_Click);
			// 
			// selectionToolStripMenuItem
			// 
			this.selectionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.selectAllToolStripMenuItem,
            this.reduceSelectionToolStripMenuItem,
            this.removeUnselectedGrainsToolStripMenuItem,
            this.chopSelectedGrainsToolStripMenuItem});
			this.selectionToolStripMenuItem.Name = "selectionToolStripMenuItem";
			this.selectionToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
			this.selectionToolStripMenuItem.Text = "Selection";
			// 
			// selectAllToolStripMenuItem
			// 
			this.selectAllToolStripMenuItem.Name = "selectAllToolStripMenuItem";
			this.selectAllToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
			this.selectAllToolStripMenuItem.Text = "Select all Grains";
			this.selectAllToolStripMenuItem.Click += new System.EventHandler(this.selectAllToolStripMenuItem_Click);
			// 
			// reduceSelectionToolStripMenuItem
			// 
			this.reduceSelectionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.reduceBy75ToolStripMenuItem1,
            this.reduceBy66ToolStripMenuItem1,
            this.reduceBy50ToolStripMenuItem,
            this.reduceBy33ToolStripMenuItem,
            this.reduceBy25ToolStripMenuItem1});
			this.reduceSelectionToolStripMenuItem.Name = "reduceSelectionToolStripMenuItem";
			this.reduceSelectionToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
			this.reduceSelectionToolStripMenuItem.Text = "Reduce Grain Selection";
			// 
			// reduceBy75ToolStripMenuItem1
			// 
			this.reduceBy75ToolStripMenuItem1.Name = "reduceBy75ToolStripMenuItem1";
			this.reduceBy75ToolStripMenuItem1.Size = new System.Drawing.Size(154, 22);
			this.reduceBy75ToolStripMenuItem1.Text = "Reduce by 75%";
			this.reduceBy75ToolStripMenuItem1.Click += new System.EventHandler(this.reduceBy75ToolStripMenuItem1_Click);
			// 
			// reduceBy66ToolStripMenuItem1
			// 
			this.reduceBy66ToolStripMenuItem1.Name = "reduceBy66ToolStripMenuItem1";
			this.reduceBy66ToolStripMenuItem1.Size = new System.Drawing.Size(154, 22);
			this.reduceBy66ToolStripMenuItem1.Text = "Reduce by 66%";
			this.reduceBy66ToolStripMenuItem1.Click += new System.EventHandler(this.reduceBy66ToolStripMenuItem1_Click);
			// 
			// reduceBy50ToolStripMenuItem
			// 
			this.reduceBy50ToolStripMenuItem.Name = "reduceBy50ToolStripMenuItem";
			this.reduceBy50ToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
			this.reduceBy50ToolStripMenuItem.Text = "Reduce by 50%";
			this.reduceBy50ToolStripMenuItem.Click += new System.EventHandler(this.reduceBy50ToolStripMenuItem_Click);
			// 
			// reduceBy33ToolStripMenuItem
			// 
			this.reduceBy33ToolStripMenuItem.Name = "reduceBy33ToolStripMenuItem";
			this.reduceBy33ToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
			this.reduceBy33ToolStripMenuItem.Text = "Reduce by 33%";
			this.reduceBy33ToolStripMenuItem.Click += new System.EventHandler(this.reduceBy33ToolStripMenuItem_Click);
			// 
			// reduceBy25ToolStripMenuItem1
			// 
			this.reduceBy25ToolStripMenuItem1.Name = "reduceBy25ToolStripMenuItem1";
			this.reduceBy25ToolStripMenuItem1.Size = new System.Drawing.Size(154, 22);
			this.reduceBy25ToolStripMenuItem1.Text = "Reduce by 25%";
			this.reduceBy25ToolStripMenuItem1.Click += new System.EventHandler(this.reduceBy25ToolStripMenuItem1_Click);
			// 
			// removeUnselectedGrainsToolStripMenuItem
			// 
			this.removeUnselectedGrainsToolStripMenuItem.Name = "removeUnselectedGrainsToolStripMenuItem";
			this.removeUnselectedGrainsToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
			this.removeUnselectedGrainsToolStripMenuItem.Text = "Chop Unselected Grains";
			this.removeUnselectedGrainsToolStripMenuItem.Click += new System.EventHandler(this.removeUnselectedGrainsToolStripMenuItem_Click);
			// 
			// debugToolStripMenuItem
			// 
			this.debugToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.generateTestSweepToolStripMenuItem,
            this.generateFlattenedSweepToolStripMenuItem,
            this.clearGrainDataToolStripMenuItem});
			this.debugToolStripMenuItem.Name = "debugToolStripMenuItem";
			this.debugToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
			this.debugToolStripMenuItem.Text = "Debug";
			// 
			// generateTestSweepToolStripMenuItem
			// 
			this.generateTestSweepToolStripMenuItem.Name = "generateTestSweepToolStripMenuItem";
			this.generateTestSweepToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
			this.generateTestSweepToolStripMenuItem.Text = "Generate Test Sweep";
			this.generateTestSweepToolStripMenuItem.Click += new System.EventHandler(this.generateTestSweepToolStripMenuItem_Click);
			// 
			// generateFlattenedSweepToolStripMenuItem
			// 
			this.generateFlattenedSweepToolStripMenuItem.Name = "generateFlattenedSweepToolStripMenuItem";
			this.generateFlattenedSweepToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
			this.generateFlattenedSweepToolStripMenuItem.Text = "Generate Flattened Sweep";
			this.generateFlattenedSweepToolStripMenuItem.Click += new System.EventHandler(this.generateFlattenedSweepToolStripMenuItem_Click);
			// 
			// clearGrainDataToolStripMenuItem
			// 
			this.clearGrainDataToolStripMenuItem.Name = "clearGrainDataToolStripMenuItem";
			this.clearGrainDataToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
			this.clearGrainDataToolStripMenuItem.Text = "Clear Grain Data";
			this.clearGrainDataToolStripMenuItem.Click += new System.EventHandler(this.clearGrainDataToolStripMenuItem_Click);
			// 
			// helpToolStripMenuItem
			// 
			this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
			this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
			this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
			this.helpToolStripMenuItem.Text = "Help";
			// 
			// aboutToolStripMenuItem
			// 
			this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
			this.aboutToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
			this.aboutToolStripMenuItem.Text = "About";
			this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
			// 
			// loadWaveDlg
			// 
			this.loadWaveDlg.Filter = "Wave files (*.wav)|*.wav";
			this.loadWaveDlg.Title = "Select a wave file to load";
			this.loadWaveDlg.FileOk += new System.ComponentModel.CancelEventHandler(this.loadWaveDlg_FileOk);
			// 
			// waveView
			// 
			this.waveView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.waveView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.waveView.Location = new System.Drawing.Point(18, 46);
			this.waveView.Name = "waveView";
			this.waveView.Size = new System.Drawing.Size(1088, 346);
			this.waveView.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.waveView.TabIndex = 2;
			this.waveView.TabStop = false;
			this.waveView.MouseLeave += new System.EventHandler(this.waveView_MouseLeave);
			this.waveView.MouseMove += new System.Windows.Forms.MouseEventHandler(this.waveView_MouseMove);
			this.waveView.MouseDown += new System.Windows.Forms.MouseEventHandler(this.waveView_MouseDown);
			this.waveView.Paint += new System.Windows.Forms.PaintEventHandler(this.waveView_Paint);
			this.waveView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.waveView_MouseUp);
			this.waveView.MouseEnter += new System.EventHandler(this.waveView_MouseEnter);
			// 
			// redrawTimer
			// 
			this.redrawTimer.Enabled = true;
			this.redrawTimer.Interval = 10;
			this.redrawTimer.Tick += new System.EventHandler(this.timer1_Tick);
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.AutoSize = true;
			this.groupBox1.Location = new System.Drawing.Point(18, 27);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(1197, 365);
			this.groupBox1.TabIndex = 3;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Wave Data";
			// 
			// waveViewTimeAxis
			// 
			this.waveViewTimeAxis.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.waveViewTimeAxis.Location = new System.Drawing.Point(18, 390);
			this.waveViewTimeAxis.Name = "waveViewTimeAxis";
			this.waveViewTimeAxis.Size = new System.Drawing.Size(1088, 42);
			this.waveViewTimeAxis.TabIndex = 0;
			this.waveViewTimeAxis.TabStop = false;
			this.waveViewTimeAxis.Paint += new System.Windows.Forms.PaintEventHandler(this.waveViewAxis_Paint);
			// 
			// loadGrainDlg
			// 
			this.loadGrainDlg.Filter = "Grain files (*.grn|*.grn|Grain files (*.csv)|*.csv";
			this.loadGrainDlg.Title = "Select a grain data file to load";
			this.loadGrainDlg.FileOk += new System.ComponentModel.CancelEventHandler(this.loadGrainDlg_FileOk);
			// 
			// drawGrainBoundaryCheck
			// 
			this.drawGrainBoundaryCheck.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.drawGrainBoundaryCheck.AutoSize = true;
			this.drawGrainBoundaryCheck.Checked = true;
			this.drawGrainBoundaryCheck.CheckState = System.Windows.Forms.CheckState.Checked;
			this.drawGrainBoundaryCheck.Location = new System.Drawing.Point(134, 440);
			this.drawGrainBoundaryCheck.Name = "drawGrainBoundaryCheck";
			this.drawGrainBoundaryCheck.Size = new System.Drawing.Size(135, 17);
			this.drawGrainBoundaryCheck.TabIndex = 4;
			this.drawGrainBoundaryCheck.Text = "Draw Grain Boundaries";
			this.drawGrainBoundaryCheck.UseVisualStyleBackColor = true;
			this.drawGrainBoundaryCheck.CheckedChanged += new System.EventHandler(this.drawGrainBoundaryCheck_CheckedChanged);
			// 
			// drawSamplePositionsCheckbox
			// 
			this.drawSamplePositionsCheckbox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.drawSamplePositionsCheckbox.AutoSize = true;
			this.drawSamplePositionsCheckbox.Location = new System.Drawing.Point(135, 468);
			this.drawSamplePositionsCheckbox.Name = "drawSamplePositionsCheckbox";
			this.drawSamplePositionsCheckbox.Size = new System.Drawing.Size(134, 17);
			this.drawSamplePositionsCheckbox.TabIndex = 5;
			this.drawSamplePositionsCheckbox.Text = "Draw Sample Positions";
			this.drawSamplePositionsCheckbox.UseVisualStyleBackColor = true;
			this.drawSamplePositionsCheckbox.CheckedChanged += new System.EventHandler(this.drawSamplePositionsCheckbox_CheckedChanged);
			// 
			// playGrainButton
			// 
			this.playGrainButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.playGrainButton.Location = new System.Drawing.Point(18, 438);
			this.playGrainButton.Name = "playGrainButton";
			this.playGrainButton.Size = new System.Drawing.Size(110, 77);
			this.playGrainButton.TabIndex = 6;
			this.playGrainButton.Text = "Play";
			this.playGrainButton.UseVisualStyleBackColor = true;
			this.playGrainButton.Click += new System.EventHandler(this.playGrainButton_Click);
			// 
			// playbackOrderCombo
			// 
			this.playbackOrderCombo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.playbackOrderCombo.FormattingEnabled = true;
			this.playbackOrderCombo.Items.AddRange(new object[] {
            "Sequential",
            "Random",
            "There & back",
            "Reverse",
            "Mix & Match"});
			this.playbackOrderCombo.Location = new System.Drawing.Point(923, 438);
			this.playbackOrderCombo.Name = "playbackOrderCombo";
			this.playbackOrderCombo.Size = new System.Drawing.Size(183, 21);
			this.playbackOrderCombo.TabIndex = 7;
			this.playbackOrderCombo.Text = "Sequential";
			this.playbackOrderCombo.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
			// 
			// label1
			// 
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(812, 441);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(108, 13);
			this.label1.TabIndex = 8;
			this.label1.Text = "Grain Playback Order";
			// 
			// saveGrainDlg
			// 
			this.saveGrainDlg.Filter = "Grain files (*.grn)|*.grn";
			this.saveGrainDlg.FileOk += new System.ComponentModel.CancelEventHandler(this.saveGrainDlg_FileOk);
			// 
			// rightClickMenu
			// 
			this.rightClickMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.markAsLoopToolStripMenuItem,
            this.deleteLoopToolStripMenuItem,
            this.applyNameHashToolStripMenuItem});
			this.rightClickMenu.Name = "rightClickMenu";
			this.rightClickMenu.Size = new System.Drawing.Size(162, 70);
			// 
			// markAsLoopToolStripMenuItem
			// 
			this.markAsLoopToolStripMenuItem.Name = "markAsLoopToolStripMenuItem";
			this.markAsLoopToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
			this.markAsLoopToolStripMenuItem.Text = "Create Loop";
			this.markAsLoopToolStripMenuItem.Click += new System.EventHandler(this.markAsLoopToolStripMenuItem_Click);
			// 
			// deleteLoopToolStripMenuItem
			// 
			this.deleteLoopToolStripMenuItem.Name = "deleteLoopToolStripMenuItem";
			this.deleteLoopToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
			this.deleteLoopToolStripMenuItem.Text = "Delete Loop";
			this.deleteLoopToolStripMenuItem.Click += new System.EventHandler(this.deleteLoopToolStripMenuItem_Click);
			// 
			// applyNameHashToolStripMenuItem
			// 
			this.applyNameHashToolStripMenuItem.Name = "applyNameHashToolStripMenuItem";
			this.applyNameHashToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
			this.applyNameHashToolStripMenuItem.Text = "Set Submix Type";
			this.applyNameHashToolStripMenuItem.Click += new System.EventHandler(this.applyNameHashToolStripMenuItem_Click);
			// 
			// drawLoopPositionsCheck
			// 
			this.drawLoopPositionsCheck.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.drawLoopPositionsCheck.AutoSize = true;
			this.drawLoopPositionsCheck.Checked = true;
			this.drawLoopPositionsCheck.CheckState = System.Windows.Forms.CheckState.Checked;
			this.drawLoopPositionsCheck.Location = new System.Drawing.Point(134, 498);
			this.drawLoopPositionsCheck.Name = "drawLoopPositionsCheck";
			this.drawLoopPositionsCheck.Size = new System.Drawing.Size(83, 17);
			this.drawLoopPositionsCheck.TabIndex = 17;
			this.drawLoopPositionsCheck.Text = "Draw Loops";
			this.drawLoopPositionsCheck.UseVisualStyleBackColor = true;
			this.drawLoopPositionsCheck.CheckedChanged += new System.EventHandler(this.drawLoopPositionsCheck_CheckedChanged);
			// 
			// flattenedWavFileDialog
			// 
			this.flattenedWavFileDialog.Filter = "Wave files (*.wav)|*.wav";
			this.flattenedWavFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.flattenedWavFileDialog_FileOk);
			// 
			// saveLoopData
			// 
			this.saveLoopData.Filter = "Loop data files (*.lpd)|*.lpd";
			this.saveLoopData.FileOk += new System.ComponentModel.CancelEventHandler(this.saveLoopData_FileOk);
			// 
			// loadLoopData
			// 
			this.loadLoopData.FileName = "openFileDialog1";
			this.loadLoopData.Filter = "Loop data files (*.lpd)|*.lpd";
			this.loadLoopData.FileOk += new System.ComponentModel.CancelEventHandler(this.loadLoopData_FileOk);
			// 
			// grainPlaybackStyle
			// 
			this.grainPlaybackStyle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.grainPlaybackStyle.FormattingEnabled = true;
			this.grainPlaybackStyle.Items.AddRange(new object[] {
            "Standard",
            "Pitch Flattened to Average",
            "Pitch Flattened to RPM"});
			this.grainPlaybackStyle.Location = new System.Drawing.Point(923, 464);
			this.grainPlaybackStyle.Name = "grainPlaybackStyle";
			this.grainPlaybackStyle.Size = new System.Drawing.Size(183, 21);
			this.grainPlaybackStyle.TabIndex = 22;
			this.grainPlaybackStyle.Text = "Standard";
			this.grainPlaybackStyle.SelectedIndexChanged += new System.EventHandler(this.grainPlaybackStyle_SelectedIndexChanged);
			// 
			// label3
			// 
			this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(821, 467);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(99, 13);
			this.label3.TabIndex = 23;
			this.label3.Text = "Grain Preview Style";
			// 
			// flattenedRPMValue
			// 
			this.flattenedRPMValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.flattenedRPMValue.Location = new System.Drawing.Point(1003, 491);
			this.flattenedRPMValue.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
			this.flattenedRPMValue.Name = "flattenedRPMValue";
			this.flattenedRPMValue.Size = new System.Drawing.Size(103, 20);
			this.flattenedRPMValue.TabIndex = 24;
			this.flattenedRPMValue.Value = new decimal(new int[] {
            5000,
            0,
            0,
            0});
			this.flattenedRPMValue.ValueChanged += new System.EventHandler(this.flattenedRPMValue_ValueChanged);
			// 
			// label4
			// 
			this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(919, 493);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(78, 13);
			this.label4.TabIndex = 25;
			this.label4.Text = "Flattened RPM";
			// 
			// sampleInterpolationCombo
			// 
			this.sampleInterpolationCombo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.sampleInterpolationCombo.FormattingEnabled = true;
			this.sampleInterpolationCombo.Items.AddRange(new object[] {
            "Linear",
            "Cosine",
            "Cubic",
            "Sinc"});
			this.sampleInterpolationCombo.Location = new System.Drawing.Point(384, 438);
			this.sampleInterpolationCombo.Name = "sampleInterpolationCombo";
			this.sampleInterpolationCombo.Size = new System.Drawing.Size(70, 21);
			this.sampleInterpolationCombo.TabIndex = 26;
			this.sampleInterpolationCombo.Text = "Linear";
			this.sampleInterpolationCombo.SelectedIndexChanged += new System.EventHandler(this.sampleInterpolationCombo_SelectedIndexChanged);
			// 
			// label2
			// 
			this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(275, 441);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(103, 13);
			this.label2.TabIndex = 27;
			this.label2.Text = "Sample Interpolation";
			// 
			// submixNameHash
			// 
			this.submixNameHash.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.submixNameHash.Location = new System.Drawing.Point(678, 438);
			this.submixNameHash.Name = "submixNameHash";
			this.submixNameHash.Size = new System.Drawing.Size(128, 20);
			this.submixNameHash.TabIndex = 28;
			// 
			// label5
			// 
			this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(604, 441);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(68, 13);
			this.label5.TabIndex = 29;
			this.label5.Text = "Submix Type";
			// 
			// chopSelectedGrainsToolStripMenuItem
			// 
			this.chopSelectedGrainsToolStripMenuItem.Name = "chopSelectedGrainsToolStripMenuItem";
			this.chopSelectedGrainsToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
			this.chopSelectedGrainsToolStripMenuItem.Text = "Chop Selected Grains";
			this.chopSelectedGrainsToolStripMenuItem.Click += new System.EventHandler(this.chopSelectedGrainsToolStripMenuItem_Click);
			// 
			// PlaybackScreen
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1123, 527);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.submixNameHash);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.sampleInterpolationCombo);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.flattenedRPMValue);
			this.Controls.Add(this.grainPlaybackStyle);
			this.Controls.Add(this.drawLoopPositionsCheck);
			this.Controls.Add(this.waveView);
			this.Controls.Add(this.waveViewTimeAxis);
			this.Controls.Add(this.playGrainButton);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.drawSamplePositionsCheckbox);
			this.Controls.Add(this.playbackOrderCombo);
			this.Controls.Add(this.drawGrainBoundaryCheck);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.menuStrip1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "PlaybackScreen";
			this.Text = "GEARS tool";
			this.MouseWheel += new System.Windows.Forms.MouseEventHandler(this.waveView_MouseWheel);
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.window_Closed);
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.waveView)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.waveViewTimeAxis)).EndInit();
			this.rightClickMenu.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.flattenedRPMValue)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.ToolStripMenuItem fileMenu;
		private System.Windows.Forms.OpenFileDialog loadWaveDlg;
		private System.Windows.Forms.PictureBox waveView;
		private System.Windows.Forms.Timer redrawTimer;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.OpenFileDialog loadGrainDlg;
		private System.Windows.Forms.CheckBox drawGrainBoundaryCheck;
		private System.Windows.Forms.CheckBox drawSamplePositionsCheckbox;
		private System.Windows.Forms.Button playGrainButton;
		private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
		private System.Windows.Forms.ComboBox playbackOrderCombo;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ToolStripMenuItem quitToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem analyseZeroCrossingsToolStripMenuItem;
		private System.Windows.Forms.PictureBox waveViewTimeAxis;
		private System.Windows.Forms.SaveFileDialog saveGrainDlg;
		private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem loadWaveFileToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem loadGrainDataToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem saveGrainDataToolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem selectionToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem reduceSelectionToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem reduceBy75ToolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem reduceBy66ToolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem reduceBy50ToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem reduceBy33ToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem reduceBy25ToolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem selectAllToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem pitchAnalysisToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem engineSimulatorToolStripMenuItem;
		private System.Windows.Forms.ContextMenuStrip rightClickMenu;
		private System.Windows.Forms.ToolStripMenuItem markAsLoopToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem deleteLoopToolStripMenuItem;
		private System.Windows.Forms.CheckBox drawLoopPositionsCheck;
		private System.Windows.Forms.ToolStripMenuItem loopsToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem clearAllLoopsToolStripMenuItem;
		private System.Windows.Forms.SaveFileDialog flattenedWavFileDialog;
		private System.Windows.Forms.ToolStripMenuItem debugToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem generateTestSweepToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem generateFlattenedSweepToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem saveLoopDataToolStripMenuItem;
		private System.Windows.Forms.SaveFileDialog saveLoopData;
		private System.Windows.Forms.ToolStripMenuItem loadLoopDataToolStripMenuItem;
		private System.Windows.Forms.OpenFileDialog loadLoopData;
		private System.Windows.Forms.ComboBox grainPlaybackStyle;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.NumericUpDown flattenedRPMValue;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.ComboBox sampleInterpolationCombo;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.ToolStripMenuItem wobbleGeneratorToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem clearGrainDataToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem applyNameHashToolStripMenuItem;
		private System.Windows.Forms.TextBox submixNameHash;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.ToolStripMenuItem removeUnselectedGrainsToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem chopSelectedGrainsToolStripMenuItem;

	}
}

