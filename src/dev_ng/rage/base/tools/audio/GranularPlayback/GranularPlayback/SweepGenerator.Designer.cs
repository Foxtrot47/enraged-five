﻿namespace GranularPlayback
{
	partial class SweepGenerator
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.sweepCurve = new System.Windows.Forms.PictureBox();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.generateSweepBtn = new System.Windows.Forms.Button();
			this.sweepLengthUpDown = new System.Windows.Forms.NumericUpDown();
			this.label1 = new System.Windows.Forms.Label();
			this.deletePointBtn = new System.Windows.Forms.Button();
			this.addPointBtn = new System.Windows.Forms.Button();
			this.selectedPointTime = new System.Windows.Forms.NumericUpDown();
			this.selectedPointRPM = new System.Windows.Forms.NumericUpDown();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.numCylindersCombo = new System.Windows.Forms.NumericUpDown();
			this.label4 = new System.Windows.Forms.Label();
			this.saveWaveDialog = new System.Windows.Forms.SaveFileDialog();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.sweepCurve)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.sweepLengthUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.selectedPointTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.selectedPointRPM)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numCylindersCombo)).BeginInit();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.AutoSize = true;
			this.groupBox1.Controls.Add(this.sweepCurve);
			this.groupBox1.Location = new System.Drawing.Point(12, 12);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(1055, 550);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Sweep Curve";
			// 
			// sweepCurve
			// 
			this.sweepCurve.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.sweepCurve.Dock = System.Windows.Forms.DockStyle.Fill;
			this.sweepCurve.Location = new System.Drawing.Point(3, 16);
			this.sweepCurve.Name = "sweepCurve";
			this.sweepCurve.Size = new System.Drawing.Size(1049, 531);
			this.sweepCurve.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.sweepCurve.TabIndex = 0;
			this.sweepCurve.TabStop = false;
			this.sweepCurve.MouseLeave += new System.EventHandler(this.sweepCurve_MouseLeave);
			this.sweepCurve.MouseMove += new System.Windows.Forms.MouseEventHandler(this.sweepCurve_MouseMove);
			this.sweepCurve.MouseDown += new System.Windows.Forms.MouseEventHandler(this.sweepCurve_MouseDown);
			this.sweepCurve.Paint += new System.Windows.Forms.PaintEventHandler(this.sweepCurve_Paint);
			this.sweepCurve.MouseUp += new System.Windows.Forms.MouseEventHandler(this.sweepCurve_MouseUp);
			// 
			// timer1
			// 
			this.timer1.Enabled = true;
			this.timer1.Interval = 10;
			this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
			// 
			// generateSweepBtn
			// 
			this.generateSweepBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.generateSweepBtn.Location = new System.Drawing.Point(12, 566);
			this.generateSweepBtn.Name = "generateSweepBtn";
			this.generateSweepBtn.Size = new System.Drawing.Size(161, 38);
			this.generateSweepBtn.TabIndex = 1;
			this.generateSweepBtn.Text = "Generate Sweep";
			this.generateSweepBtn.UseVisualStyleBackColor = true;
			this.generateSweepBtn.Click += new System.EventHandler(this.generateSweepBtn_Click);
			// 
			// sweepLengthUpDown
			// 
			this.sweepLengthUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.sweepLengthUpDown.DecimalPlaces = 2;
			this.sweepLengthUpDown.Location = new System.Drawing.Point(1012, 577);
			this.sweepLengthUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.sweepLengthUpDown.Name = "sweepLengthUpDown";
			this.sweepLengthUpDown.Size = new System.Drawing.Size(52, 20);
			this.sweepLengthUpDown.TabIndex = 2;
			this.sweepLengthUpDown.Value = new decimal(new int[] {
            9,
            0,
            0,
            0});
			this.sweepLengthUpDown.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
			// 
			// label1
			// 
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(891, 579);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(115, 13);
			this.label1.TabIndex = 3;
			this.label1.Text = "Desired Sweep Length";
			// 
			// deletePointBtn
			// 
			this.deletePointBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.deletePointBtn.Location = new System.Drawing.Point(665, 566);
			this.deletePointBtn.Name = "deletePointBtn";
			this.deletePointBtn.Size = new System.Drawing.Size(82, 38);
			this.deletePointBtn.TabIndex = 4;
			this.deletePointBtn.Text = "Delete Point";
			this.deletePointBtn.UseVisualStyleBackColor = true;
			this.deletePointBtn.Click += new System.EventHandler(this.deletePointBtn_Click);
			// 
			// addPointBtn
			// 
			this.addPointBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.addPointBtn.Location = new System.Drawing.Point(582, 566);
			this.addPointBtn.Name = "addPointBtn";
			this.addPointBtn.Size = new System.Drawing.Size(77, 38);
			this.addPointBtn.TabIndex = 5;
			this.addPointBtn.Text = "Add Point";
			this.addPointBtn.UseVisualStyleBackColor = true;
			this.addPointBtn.Click += new System.EventHandler(this.addPointBtn_Click);
			// 
			// selectedPointTime
			// 
			this.selectedPointTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.selectedPointTime.DecimalPlaces = 2;
			this.selectedPointTime.Location = new System.Drawing.Point(379, 577);
			this.selectedPointTime.Name = "selectedPointTime";
			this.selectedPointTime.Size = new System.Drawing.Size(79, 20);
			this.selectedPointTime.TabIndex = 6;
			this.selectedPointTime.ValueChanged += new System.EventHandler(this.selectedPointTime_ValueChanged);
			// 
			// selectedPointRPM
			// 
			this.selectedPointRPM.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.selectedPointRPM.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
			this.selectedPointRPM.Location = new System.Drawing.Point(501, 577);
			this.selectedPointRPM.Maximum = new decimal(new int[] {
            15000,
            0,
            0,
            0});
			this.selectedPointRPM.Name = "selectedPointRPM";
			this.selectedPointRPM.Size = new System.Drawing.Size(75, 20);
			this.selectedPointRPM.TabIndex = 7;
			this.selectedPointRPM.ValueChanged += new System.EventHandler(this.selectedPointRPM_ValueChanged);
			// 
			// label2
			// 
			this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(343, 579);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(30, 13);
			this.label2.TabIndex = 8;
			this.label2.Text = "Time";
			// 
			// label3
			// 
			this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(464, 579);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(31, 13);
			this.label3.TabIndex = 9;
			this.label3.Text = "RPM";
			// 
			// numCylindersCombo
			// 
			this.numCylindersCombo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.numCylindersCombo.Location = new System.Drawing.Point(833, 577);
			this.numCylindersCombo.Name = "numCylindersCombo";
			this.numCylindersCombo.Size = new System.Drawing.Size(52, 20);
			this.numCylindersCombo.TabIndex = 10;
			this.numCylindersCombo.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
			// 
			// label4
			// 
			this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(753, 579);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(74, 13);
			this.label4.TabIndex = 11;
			this.label4.Text = "Num Cylinders";
			// 
			// saveWaveDialog
			// 
			this.saveWaveDialog.Filter = "Wave files (*.wav)|*.wav";
			this.saveWaveDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.saveWaveDialog_FileOk);
			// 
			// SweepGenerator
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1079, 616);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.numCylindersCombo);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.selectedPointRPM);
			this.Controls.Add(this.selectedPointTime);
			this.Controls.Add(this.addPointBtn);
			this.Controls.Add(this.deletePointBtn);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.generateSweepBtn);
			this.Controls.Add(this.sweepLengthUpDown);
			this.Controls.Add(this.groupBox1);
			this.Name = "SweepGenerator";
			this.Text = "Sweep Generator";
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.sweepCurve)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.sweepLengthUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.selectedPointTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.selectedPointRPM)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numCylindersCombo)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.PictureBox sweepCurve;
		private System.Windows.Forms.Timer timer1;
		private System.Windows.Forms.Button generateSweepBtn;
		private System.Windows.Forms.NumericUpDown sweepLengthUpDown;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button deletePointBtn;
		private System.Windows.Forms.Button addPointBtn;
		private System.Windows.Forms.NumericUpDown selectedPointTime;
		private System.Windows.Forms.NumericUpDown selectedPointRPM;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.NumericUpDown numCylindersCombo;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.SaveFileDialog saveWaveDialog;
	}
}