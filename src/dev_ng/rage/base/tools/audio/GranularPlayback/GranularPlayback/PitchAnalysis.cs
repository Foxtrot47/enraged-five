﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Drawing.Drawing2D;
using System.Threading;

using MathNet.Numerics;
using MathNet.Numerics.Transformations;

namespace GranularPlayback
{
	public partial class PitchAnalysis : Form
	{
		private GrainPlayer m_GrainPlayer;
		private float[] m_YFourierSignal;
		private float[] m_PrevYFourierSignal;
		private float m_FourierGraphStepRate;
		private int m_PreviewCursorPosInSamples = 0;
		private float m_RPMCursor;
		private int m_LastFFTDrawPos;
		private float m_LastFourierScaleValue;
		private float m_PitchResolution = 0.0f;
		int m_FftSampleLength = 2048;
		bool m_WaveCursorBeingDragged = false;
		bool m_RpmCursorBeingDragged = false;
		bool m_ShowGrainDialog = false;
		Thread m_GenerationThread;
		bool m_GenerationThreadCancelled = false;
		GrainGenerator m_GrainGenerator;
		private ComplexFourierTransformation m_ComplexFourierTransform;
		private RealFourierTransformation m_RealFourierTransform;

		int m_ThreadPitchTrackingStart = 0;
		int m_ThreadPitchTrackingEnd = 0;

		public PitchAnalysis(GrainPlayer wavePlayer, GrainGenerator grainGenerator)
		{
			m_ComplexFourierTransform = new ComplexFourierTransformation();
			m_RealFourierTransform = new RealFourierTransformation();
			m_GrainGenerator = grainGenerator;
			InitializeComponent();
			m_GrainPlayer = wavePlayer;
			AnalyseCursorPos();
		}

		static ulong upper_power_of_two(ulong v)
		{
			v--;
			v |= v >> 1;
			v |= v >> 2;
			v |= v >> 4;
			v |= v >> 8;
			v |= v >> 16;
			v++;
			return v;
		}

		/// <summary>
		/// Fourier analyse the given sets of data
		/// </summary>
		/// <param name="data"></param>
		/// <param name="pastData"></param>
		public void Analyse(double[] data, double[] pastData)
		{
			ulong numFFTPoints = (ulong) m_GrainPlayer.m_WaveFile.Format.SampleRate;
			numFFTPoints = upper_power_of_two(numFFTPoints);
			float fourierScale = (float)fourierScaleUpDown.Value;
			double[] hannWindow = new double[data.Length];

			Complex[] complexData = new Complex[numFFTPoints];
			Complex[] prevComplexData = new Complex[numFFTPoints];

			// Apply a hann window to the data
			for(int loop = 0; loop < data.Length; loop++)
			{
				hannWindow[loop] = 0.5f * (1 - Math.Cos((2.0f * Math.PI * loop) / (data.Length)));
				complexData[loop] = data[loop] * hannWindow[loop];
				prevComplexData[loop] = pastData[loop] * hannWindow[loop];
			}

			m_ComplexFourierTransform.Convention = TransformationConvention.Matlab;
			m_ComplexFourierTransform.TransformForward(complexData);
			m_ComplexFourierTransform.TransformForward(prevComplexData);

			m_YFourierSignal = new float[numFFTPoints / 2];
			m_PrevYFourierSignal = new float[numFFTPoints / 2];
			m_FourierGraphStepRate = m_GrainPlayer.m_WaveFile.Format.SampleRate / (float)numFFTPoints;

			for (int loop = 0; loop < (int)numFFTPoints / 2; loop++)
			{
				m_YFourierSignal[loop] = (float)complexData[loop].Modulus;
				m_YFourierSignal[loop] *= 2.0f;
				m_YFourierSignal[loop] *= fourierScale;

				m_PrevYFourierSignal[loop] = (float)prevComplexData[loop].Modulus;
				m_PrevYFourierSignal[loop] *= 2.0f;
				m_PrevYFourierSignal[loop] *= fourierScale;
			}
		}

		/// <summary>
		/// Convert a hz value to a number of samples
		/// </summary>
		/// <param name="hz"></param>
		/// <param name="sampleRate"></param>
		/// <returns></returns>
		private static int hzToPeriodInSamples(double hz, int sampleRate)
		{
			return (int)(1 / (hz / (double)sampleRate));
		}

		/// <summary>
		/// Convert a number of samples to hertz
		/// </summary>
		/// <param name="period"></param>
		/// <param name="sampleRate"></param>
		/// <returns></returns>
		private static double periodInSamplesToHz(int period, int sampleRate)
		{
			return 1 / (period / (double)sampleRate);
		}

		/// <summary>
		/// Round to a given value
		/// </summary>
		/// <param name="input"></param>
		/// <param name="roundValue"></param>
		/// <returns></returns>
		private static float Round2(double input, double roundValue)
		{
			return (float) (Math.Round(input/roundValue) * roundValue);
		}

		/// <summary>
		/// Threaded pitch tracking process
		/// </summary>
		public void TrackPitch()
		{
			m_GrainGenerator.Reset();

			if(m_PitchResolution > 0.0f)
			{
				m_ThreadPitchTrackingStart = m_PreviewCursorPosInSamples;
				m_ThreadPitchTrackingEnd = m_PreviewCursorPosInSamples;

				TrackPitchDirection(false);
				TrackPitchDirection(true);

				m_ThreadPitchTrackingStart = 0;
				m_ThreadPitchTrackingEnd = 0;
			}
		}
		
		/// <summary>
		/// Track the pitch in a given direction (forward or back) from the marked position
		/// </summary>
		/// <param name="forwards"></param>
		public void TrackPitchDirection(bool forwards)
		{
			// HL: This function is written in a slightly odd way, as I've basically delved into the original
			// matlab code and reproduced exactly what it was doing there line by line so as to ensure this tool behaves identically.
			// At some point it might be worth tidying this up (reducing the for loops, better variable names, etc.), but only once
			// we're sure that everything is behaving as expected
			List<int> pitchMapOffsets = new List<int>();
			List<double> pitchMapValues = new List<double>();

			int frameSizeMS = (int)frameSizeUpDown.Value;
			int stepSize = (int)stepSizeUpDown.Value;
			int frameSizeSamples = (int)((frameSizeMS/1000.0f) * m_GrainPlayer.m_WaveFile.Format.SampleRate);

			int startIndex = m_PreviewCursorPosInSamples;
			int endIndex = startIndex + frameSizeSamples;

			int upperBoundHz = (int)upperBoundUpDown.Value;
			int lowerBoundHz = (int)lowerBoundUpDown.Value;

			double[] hannWindow = new double[frameSizeSamples];

			for (int loop = 0; loop < frameSizeSamples; loop++)
			{
				hannWindow[loop] = 0.5f * (1 - Math.Cos((2.0f * Math.PI * loop) / (frameSizeSamples)));
			}

			float maxShift = (float)Math.Round(m_GrainPlayer.m_WaveFile.Format.SampleRate / m_PitchResolution) * 2;
			float maxFreq = (float)Math.Round(m_GrainPlayer.m_WaveFile.Format.SampleRate / (m_PitchResolution + upperBoundHz));
			float minFreq = (float)Math.Round(m_GrainPlayer.m_WaveFile.Format.SampleRate / (m_PitchResolution - lowerBoundHz));
			int numFFTPoints = (int)upper_power_of_two((ulong)((2 * frameSizeSamples) - 1));

			while (startIndex >= 0 && endIndex < m_GrainPlayer.m_WaveFile.Data.ChannelData.Length)
			{
				// Start of MATLAB pitchEstimateXcorr() implementation
				Complex[] complexData = new Complex[numFFTPoints];

				for(int loop = 0; loop < frameSizeSamples; loop++)
				{
					complexData[loop] = m_GrainPlayer.m_WaveFile.Data.ChannelData[0, startIndex + loop] * hannWindow[loop];
				}

				// Start of MATLAB xCorr() implementationX
				m_ComplexFourierTransform.Convention = TransformationConvention.Matlab;
				m_ComplexFourierTransform.TransformForward(complexData);

				double[] complexDataModulusSquaredInput = new double[numFFTPoints];
				double[] complexDataModulusSquared = new double[numFFTPoints];
				double[] complexDataDummy = new double[numFFTPoints];

				for (int loop = 0; loop < numFFTPoints; loop++)
				{
					complexDataModulusSquaredInput[loop] = complexData[loop].ModulusSquared;
				}

				m_RealFourierTransform.Convention = TransformationConvention.Matlab;
				m_RealFourierTransform.TransformBackward(complexDataModulusSquaredInput, complexDataDummy, out complexDataModulusSquared);

				int[] lagValues = new int[(int)maxShift * 2];

				for(int loop = 0; loop < maxShift * 2; loop++)
				{
					lagValues[loop] = -(int)maxShift + loop;
				}

				List<double> finalValues = new List<double>();

				if(maxShift >= frameSizeSamples)
				{
					for (int loop = 0; loop < maxShift - frameSizeSamples; loop++)
					{
						finalValues.Add(0.0f);
					}

					for (int loop = numFFTPoints - frameSizeSamples; loop < numFFTPoints; loop++)
					{
						finalValues.Add(complexDataModulusSquared[loop]);
					}

					for (int loop = 0; loop < frameSizeSamples; loop++)
					{
						finalValues.Add(complexDataModulusSquared[loop]);
					}

					for (int loop = 0; loop < maxShift - frameSizeSamples; loop++)
					{
						finalValues.Add(0.0f);
					}
				}
				else
				{
					for (int loop = (int)(complexDataModulusSquared.Length - maxShift); loop < complexDataModulusSquared.Length; loop++)
					{
						finalValues.Add(complexDataModulusSquared[loop]);
					}

					for (int loop = 0; loop < maxShift; loop++)
					{
						finalValues.Add(complexDataModulusSquared[loop]);
					}
				}

				for (int loop = 0; loop < finalValues.Count; loop++)
				{
					finalValues[loop] /= finalValues[(int)maxShift];
				}
				// End of MATLAB xCorr() implementation

				List<double> testValues = new List<double>();

				for(int loop = (int)maxShift; loop < 2 * maxShift; loop++ )
				{
					testValues.Add(finalValues[loop]);
				}

				double maxOffsetValue = double.MinValue;
				int maxOffsetIndex = -1;
				int index = 0;
				for (int loop = (int)maxFreq; loop < minFreq; loop++)
				{
					if (testValues[loop] > maxOffsetValue)
					{
						maxOffsetValue = testValues[loop];
						maxOffsetIndex = index;
					}

					index++;
				}

				double xCorrOffset = maxFreq + maxOffsetIndex;
				double finalPitchValue = (m_GrainPlayer.m_WaveFile.Format.SampleRate / (xCorrOffset - 1));
				pitchMapOffsets.Add(startIndex);
				pitchMapValues.Add(finalPitchValue);
				// End of MATLAB pitchEstimateXcorr() implementation

				if (forwards)
				{
					m_GrainGenerator.AddPitchDataBack(startIndex, (float)finalPitchValue);
					startIndex += stepSize;
				}
				else
				{
					m_GrainGenerator.AddPitchDataFront(startIndex, (float)finalPitchValue);
					startIndex -= stepSize;
				}

				endIndex = startIndex + frameSizeSamples;
				maxShift = (float)Math.Round(m_GrainPlayer.m_WaveFile.Format.SampleRate / Round2(finalPitchValue, 0.1)) * 4;
				maxFreq = (float)Math.Round(m_GrainPlayer.m_WaveFile.Format.SampleRate / (finalPitchValue + upperBoundHz));
				minFreq = (float)Math.Round(m_GrainPlayer.m_WaveFile.Format.SampleRate / (finalPitchValue - lowerBoundHz));

				// This is just for debug rendering purposes
				if (startIndex < m_ThreadPitchTrackingStart)
				{
					m_ThreadPitchTrackingStart = startIndex;
				}

				if (endIndex > m_ThreadPitchTrackingEnd)
				{
					m_ThreadPitchTrackingEnd = endIndex;
				}
			}
		}

		/// <summary>
		/// Analyse the data at the current cursor position
		/// </summary>
		void AnalyseCursorPos()
		{
			if(m_GrainPlayer.m_WaveFile != null)
			{
				double[] data = new double[m_FftSampleLength];
				double[] pastData = new double[m_FftSampleLength];
				int index = 0;

				for (int loop = m_PreviewCursorPosInSamples; loop < m_PreviewCursorPosInSamples + m_FftSampleLength; loop++)
				{
					data[index] = m_GrainPlayer.m_WaveFile.Data.ChannelData[0, loop];

					int prevDataIndex = loop - m_FftSampleLength;

					if (prevDataIndex >= 0)
					{
						pastData[index] = m_GrainPlayer.m_WaveFile.Data.ChannelData[0, loop - m_FftSampleLength];
					}
					else
					{
						pastData[index] = 0.0f;
					}

					index++;
				}

				Analyse(data, pastData);
			}
		}

		/// <summary>
		/// Redraw the form, reanalyse cursor pos if needed
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void redrawTimer_Tick(object sender, EventArgs e)
		{
			fourierTransform.Refresh();
			fourierXAxis.Refresh();
			wavePreviewBox.Refresh();
			wavePreviewAxis.Refresh();

			if(m_GenerationThread == null ||
			   !m_GenerationThread.IsAlive )
			{
				if (m_ShowGrainDialog)
				{
					m_ShowGrainDialog = false;
					trackPitchBtn.Text = "Track Pitch";
					suggestFrequencyBtn.Enabled = true;
					numCylindersUpDown.Enabled = true;
					fourierScaleUpDown.Enabled = true;
					upperBoundUpDown.Enabled = true;
					lowerBoundUpDown.Enabled = true;
					stepSizeUpDown.Enabled = true;
					frameSizeUpDown.Enabled = true;

					if(!m_GenerationThreadCancelled)
					{
						m_GrainGenerator.ShowDialog();
					}
					else
					{
						m_GenerationThreadCancelled = false;
						m_ThreadPitchTrackingStart = 0;
						m_ThreadPitchTrackingEnd = 0;
					}
				}
				else if (m_LastFFTDrawPos != m_PreviewCursorPosInSamples ||
						 m_LastFourierScaleValue != (float)fourierScaleUpDown.Value)
				{
					AnalyseCursorPos();
					m_LastFFTDrawPos = m_PreviewCursorPosInSamples;
					m_LastFourierScaleValue = (float)fourierScaleUpDown.Value;
				}
			}
		}

		/// <summary>
		/// Adjust the wave preview cursor position
		/// </summary>
		/// <param name="xCoord"></param>
		private void AdjustWavePreviewCursor(int xCoord)
		{
			float fractionSelected = xCoord / (float)wavePreviewBox.Width;
			m_PreviewCursorPosInSamples = (int)(m_GrainPlayer.m_WaveFile.Data.NumSamples * fractionSelected);

			if (m_PreviewCursorPosInSamples < 0)
			{
				m_PreviewCursorPosInSamples = 0;
			}
			else if (m_PreviewCursorPosInSamples + m_FftSampleLength >= m_GrainPlayer.m_WaveFile.Data.NumSamples)
			{
				m_PreviewCursorPosInSamples = (int)(m_GrainPlayer.m_WaveFile.Data.NumSamples - m_FftSampleLength);
			}
		}

		/// <summary>
		/// Callback for dragging the wave view cursor
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void wavePreview_MouseMove(object sender, MouseEventArgs e)
		{
			if(m_WaveCursorBeingDragged)
			{
				AdjustWavePreviewCursor(e.Location.X);
			}
		}

		/// <summary>
		/// Callback for clicking the wave view
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void wavePreview_MouseUp(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)
			{
				m_WaveCursorBeingDragged = false;
			}
		}

		/// <summary>
		/// Callback for clicking the wave view
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void wavePreview_MouseDown(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)
			{
				m_WaveCursorBeingDragged = true;
				AdjustWavePreviewCursor(e.Location.X);
			}
		}

		/// <summary>
		/// Callback for clicking the fourier transform
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void fourierTransform_MouseDown(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)
			{
				m_RpmCursorBeingDragged = true;
				AdjustRPMCursor(e.Location.X);
			}
		}

		/// <summary>
		/// Callback for clicking the fourier transform
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void fourierTransform_MouseUp(object sender, MouseEventArgs e)
		{
			m_RpmCursorBeingDragged = false;
		}

		/// <summary>
		/// Callback for dragging the mouse over the fourier transform
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void fourierTransform_MouseMove(object sender, MouseEventArgs e)
		{
			if(m_RpmCursorBeingDragged)
			{
				AdjustRPMCursor(e.Location.X);
			}
		}

		/// <summary>
		/// Adjust the RPM cursor
		/// </summary>
		/// <param name="xCoord"></param>
		private void AdjustRPMCursor(int xCoord)
		{
			float fractionSelected = xCoord / (float)fourierTransform.Width;
			float selectedHertz = 1000 * fractionSelected;

			if (selectedHertz < 0)
			{
				selectedHertz = 0;
			}
			else if (selectedHertz > 1000)
			{
				selectedHertz = 1000;
			}

			m_PitchResolution = selectedHertz / (float) numCylindersUpDown.Value;
			m_RPMCursor = selectedHertz * 120.0f;
		}

		/// <summary>
		/// Update the fourier transform view
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="pea"></param>
		private void fourierTransform_Paint(object sender, PaintEventArgs pea)
		{
			if(m_YFourierSignal == null)
			{
				return;
			}

			Int32 lastx = 0, lasty = 0, lastyInv = 0;
			var p = Pens.DarkBlue;
			var grfx = pea.Graphics;
			var visBounds = grfx.VisibleClipBounds;

			grfx.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb(200, 200, 200)),
							0,
							0,
							visBounds.Width,
							visBounds.Height);


			var gp = new GraphicsPath();
			var invgp = new GraphicsPath();

			for (var i = 0; i * m_FourierGraphStepRate < 1000; i++)
			{
				float transform = m_YFourierSignal[i];
				transform /= 1600.0f;				

				// now 0 -> 1.0f
				var y = (Int32)((0.5f + transform) * visBounds.Height);
				var yInv = (Int32)((0.5f - transform) * visBounds.Height);
				var x = (Int32)(((i * m_FourierGraphStepRate) / (float)1000) * visBounds.Width);

				if (i >= 1)
				{
					gp.AddLine(lastx, lasty, x, y);
					invgp.AddLine(lastx, lastyInv, x, yInv);
				}

				lastx = x;
				lasty = y;
				lastyInv = yInv;
			}

			// draw paths
			grfx.DrawPath(p, gp);
			grfx.DrawPath(p, invgp);

			if (m_PitchResolution > 0.0f)
			{
				for (float xCoord = m_PitchResolution; xCoord < 1000.0f; xCoord += m_PitchResolution)
				{
					int linePos = (int)((xCoord / 1000.0f) * visBounds.Width);
					grfx.DrawLine(Pens.Blue,
							   linePos,
							   0,
							   linePos,
							   visBounds.Height);
				}
			}

			// draw cursor position
			int cursorXPos = (int)(((m_RPMCursor / 120.0f)/1000.0f) * visBounds.Width);

			grfx.DrawLine(Pens.Red,
					   cursorXPos,
					   0,
					   cursorXPos,
					   visBounds.Height);

			Font arial = new Font(new FontFamily("Arial"), 10, FontStyle.Regular);
			SolidBrush textBrush = new SolidBrush(Color.Black);

			grfx.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb(210, 255, 255, 255)),
					0,
					0,
					420.0f,
					17.0f);

			float sampleTime = m_PreviewCursorPosInSamples / (float)m_GrainPlayer.m_WaveFile.Format.SampleRate;
			grfx.DrawString("Time: " + String.Format("{0:0.###}", sampleTime) + " Pitch Resolution:" + m_PitchResolution + " RPM: " + (m_PitchResolution * 120.0f), arial, textBrush, new PointF(0.0f, 0.0f));
		}

		/// <summary>
		/// Update the fourier view Hz axis
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="pea"></param>
		private void fourierXAxis_Paint(object sender, PaintEventArgs pea)
		{
			var grfx = pea.Graphics;
			var visBounds = grfx.VisibleClipBounds;
			Font arial = new Font(new FontFamily("Arial"), 10, FontStyle.Regular);
			SolidBrush textBrush = new SolidBrush(Color.Black);

			for (int loop = 0; loop < 10; loop++)
			{
				float xCoord = visBounds.Width * loop / 10.0f;
				float thisHz = 100 * loop;

				string timeString = String.Format("{0:0.###}", thisHz);
				grfx.DrawString(timeString, arial, textBrush, new PointF(xCoord + 1, 5));
				grfx.DrawLine(Pens.Black, xCoord, 5 + visBounds.Height / 2, xCoord, 0);
			}
		}

		/// <summary>
		/// Update the wave view 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="pea"></param>
		private void wavePreview_Paint(object sender, PaintEventArgs pea)
		{
			if(m_GrainPlayer.m_WaveFile == null)
			{
				return;
			}

			Int32 lastx = 0, lasty = 0;
			var g = pea.Graphics;
			var visBounds = g.VisibleClipBounds;

			var gp = new GraphicsPath();
			var p = Pens.DarkBlue;

			g.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb(200, 200, 200)),
							0,
							0,
							visBounds.Width,
							visBounds.Height);

			int stepRate = 15;

			for (var i = 0; i < m_GrainPlayer.m_WaveFile.Data.NumSamples; i += stepRate)
			{
				float sample = m_GrainPlayer.m_WaveFile.Data.ChannelData[0, i];

				// scale from -1.0f -> 1.0f to 0 -> visBounds.Height
				sample *= -1.0f;
				sample += 1.0f;
				sample /= 2.0f;

				// now 0 -> 1.0f
				var y = (Int32)(sample * visBounds.Height);
				var x = (Int32)(i * (visBounds.Width / (float)m_GrainPlayer.m_WaveFile.Data.NumSamples));

				if (i >= 1)
				{
					gp.AddLine(lastx, lasty, x, y);
				}

				lastx = x;
				lasty = y;
			}

			// draw path
			g.DrawPath(p, gp);

			// draw cursor position
			int cursorXPos = (int)((m_PreviewCursorPosInSamples / (float)m_GrainPlayer.m_WaveFile.Data.NumSamples) * visBounds.Width);

			g.DrawLine(Pens.Black,
					   cursorXPos,
					   0,
					   cursorXPos,
					   visBounds.Height);

			float processingStart = m_ThreadPitchTrackingStart / (float)m_GrainPlayer.m_WaveFile.Data.NumSamples;
			float processingEnd = m_ThreadPitchTrackingEnd / (float)m_GrainPlayer.m_WaveFile.Data.NumSamples;

			g.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb(96, 0, 255, 0)),
									processingStart * visBounds.Width,
									0,
									processingEnd * visBounds.Width - processingStart * visBounds.Width,
									visBounds.Height);
		}

		/// <summary>
		/// Update the wave view time axis
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="pea"></param>
		private void wavePreviewAxis_Paint(object sender, PaintEventArgs pea)
		{
			if (m_GrainPlayer.m_WaveFile == null)
			{
				return;
			}

			var grfx = pea.Graphics;
			var visBounds = grfx.VisibleClipBounds;
			Font arial = new Font(new FontFamily("Arial"), 10, FontStyle.Regular);
			SolidBrush textBrush = new SolidBrush(Color.Black);

			int startSampleIndex = 0;
			int endSampleIndex = (int)(m_GrainPlayer.m_WaveFile.Data.NumSamples);

			float startSampleTime = startSampleIndex / (float)m_GrainPlayer.m_WaveFile.Format.SampleRate;
			float endSampleTime = endSampleIndex / (float)m_GrainPlayer.m_WaveFile.Format.SampleRate;

			for (int loop = 0; loop < 10; loop++)
			{
				float xCoord = visBounds.Width / 10 * loop;
				float thisTime = startSampleTime + ((endSampleTime - startSampleTime) / 10) * loop;

				string timeString = String.Format("{0:0.###}", thisTime);
				grfx.DrawString(timeString, arial, textBrush, new PointF(xCoord + 1, 3));
				grfx.DrawLine(Pens.Black, xCoord, visBounds.Height / 2, xCoord, 0);
			}
		}

		/// <summary>
		/// Callback for changing the cylinder count
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void numCylindersUpDown_ValueChanged(object sender, EventArgs e)
		{
			m_PitchResolution = (m_RPMCursor/120.0f) / (float)numCylindersUpDown.Value;
		}

		/// <summary>
		/// Kick off a new pitch tracking thread
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void trackPitchBtn_Click(object sender, EventArgs e)
		{
			if (m_GenerationThread != null &&
				m_GenerationThread.IsAlive)
			{
				m_GenerationThreadCancelled = true;
				m_GenerationThread.Abort();
				m_GenerationThread = null;
			}
			else
			{
				trackPitchBtn.Text = "Cancel";
				suggestFrequencyBtn.Enabled = false;
				numCylindersUpDown.Enabled = false;
				fourierScaleUpDown.Enabled = false;
				upperBoundUpDown.Enabled = false;
				lowerBoundUpDown.Enabled = false;
				stepSizeUpDown.Enabled = false;
				frameSizeUpDown.Enabled = false;
				m_GenerationThread = new Thread(new ThreadStart(TrackPitch));
				m_GenerationThread.Start();
				m_ShowGrainDialog = true;
			}
		}

		private void suggestFrequencyBtn_Click(object sender, EventArgs e)
		{
			float proportionThroughWave = m_PreviewCursorPosInSamples / (float) m_GrainPlayer.m_WaveFile.Data.NumSamples;

			// At a rough estimate, most of our recording will probably be vehicles revving steadily from idle up to 8000-odd RPM,
			// so we know roughly the area in which we want to be looking
			float estimatedRPM = 1500 + (7000 * proportionThroughWave);
			int estimatedHz = (int)(estimatedRPM / 120.0f) * (int)numCylindersUpDown.Value;
			float maxTransform = 0.0f;
			int maxTransformHz = 0;

			for (var i = 0; i * m_FourierGraphStepRate < 1000; i++)
			{
				float transform = m_YFourierSignal[i];

				if (transform > maxTransform)
				{
					int hzValue = (Int32)((i * m_FourierGraphStepRate));

					// Make sure this lies near our expected results
					if(hzValue > estimatedHz - 50 &&
					   hzValue < estimatedHz + 50)
					{
						maxTransform = transform;
						maxTransformHz = hzValue;
					}
				}
			}

			m_PitchResolution = maxTransformHz / (float)numCylindersUpDown.Value;
			m_RPMCursor = maxTransformHz * 120.0f;
		}
	}
}
