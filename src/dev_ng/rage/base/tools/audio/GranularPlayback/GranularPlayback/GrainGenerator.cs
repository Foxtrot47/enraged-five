﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Threading;
using Wavelib;
using Excel = Microsoft.Office.Interop.Excel;
using Microsoft.DirectX.DirectInput;

namespace GranularPlayback
{
	public partial class GrainGenerator : Form
	{
		class PitchData
		{
			public int sampleIndex;
			public float pitch;
		}

		List<PitchData> m_PitchData;
		List<PitchData> m_OldPitchData;

		float m_MaxPitch;
		string m_PitchDataFilename;
		GrainPlayer m_GrainPlayer;
		PlaybackScreen m_PlaybackScreen;
		int m_GranuleBarProgress;
		Thread m_GenerationThread;

		// Rendering stuff
		float m_Yscale = 1.0f;
		float m_Yzoom = 1.0f;
		float m_Xscale = 1.0f;
		float m_Xzoom = 1.0f;
		int m_Xoffset = 0;
		float m_WindowWidth = 0.0f;
		int m_PitchSelectionCursor = 0;
		float m_GranuleGenerationCursor = 0.0f;
		int m_LongestGrainSoFar = 0;

		// Selection stuff
		bool m_SelectingRegion = false;
		int m_RegionSelectCursorStart = 0;
		int m_RegionSelectCursorEnd = 0;
		RectangleF m_MainDisplayVisBounds;

		public GrainGenerator(PlaybackScreen playbackScreen)
		{
			InitializeComponent();
			m_PitchData = new List<PitchData>();
			m_OldPitchData = new List<PitchData>();
			m_GrainPlayer = playbackScreen.m_GrainPlayer;
			m_PlaybackScreen = playbackScreen;
			m_GranuleBarProgress = 0;
			generateGranulesBtn.Enabled = false;
			m_PitchDataFilename = null;

			this.KeyPreview = true;
			this.KeyDown += new KeyEventHandler(GrainGenerator_KeyDown);
		}

		void GrainGenerator_KeyDown(object sender, KeyEventArgs e)
		{
			switch(e.KeyCode)
			{
				case Keys.Oemplus:
					ModifySelection(1);
					break;

				case Keys.OemMinus:
					ModifySelection(-1);
					break;
			}
		}

		private void loadPitchDataBtn_Click(object sender, EventArgs e)
		{
			loadPitchDataDlg.ShowDialog();
		}

		public bool IsPitchDataLoaded()
		{
			return m_PitchData.Count > 0;
		}

		private void loadPitchDataDlg_FileOk(object sender, CancelEventArgs e)
		{
			OpenFileDialog dialog = sender as OpenFileDialog;
			m_PitchDataFilename = dialog.FileName;
			LoadPitchData();
			generateGranulesBtn.Enabled = true;
		}

		private void generateGranulesBtn_Click(object sender, EventArgs e)
		{
			if (m_GenerationThread != null &&
				m_GenerationThread.IsAlive)
			{
				m_GenerationThread.Abort();
				m_GenerationThread = null;
			}
			else
			{
				m_GrainPlayer.Pause();
				m_GrainPlayer.Reset();
				m_GenerationThread = new Thread(new ThreadStart(GenerateGranules));
				m_GenerationThread.Start();
			}
		}

		private void prevGrainView_Paint(object sender, PaintEventArgs pea)
		{
			var g = pea.Graphics;
			var visBounds = g.VisibleClipBounds;
			Font arial = new Font(new FontFamily("Arial"), 10, FontStyle.Regular);

			Int32 lastx = 0, lasty = (Int32)visBounds.Height/2;
			var gp = new GraphicsPath();
			var p = Pens.DarkBlue;
			int numGrains = m_GrainPlayer.GetNumGrains();

			if (m_GenerationThread == null || !m_GenerationThread.IsAlive)
			{
				if (m_PitchData.Count > m_PitchSelectionCursor)
				{
					numGrains = m_GrainPlayer.GetGrainIndexForSample(m_PitchData[m_PitchSelectionCursor].sampleIndex) - 1;
				}
			}

			if(numGrains > 2)
			{
				int grainStart = m_GrainPlayer.GetGrainPos(numGrains - 2);
				int grainEnd = m_GrainPlayer.GetGrainPos(numGrains - 1);
				int numSamples = grainEnd - grainStart;
				int stepRate = 10;
				int xIndex = 0;

				// Try to keep the preview window the same so we can visualise the granules
				// getting shorter as the pitch increases
				if (numSamples > m_LongestGrainSoFar)
				{
					m_LongestGrainSoFar = numSamples;
				}

				bool drawFixedWidth = fixedWidthComboBox.Checked;

				if (drawFixedWidth)
				{
					stepRate = 2;
				}

				grainPreviewLabel.Text = "Grain Preview " + "(" + (grainEnd - grainStart).ToString() + " samples)";

				for (var i = grainStart; i < grainEnd && i < m_GrainPlayer.m_WaveFile.Data.ChannelData.Length; i += stepRate, xIndex += stepRate)
				{
					float sample = m_GrainPlayer.m_WaveFile.Data.ChannelData[0, i];

					// scale from -1.0f -> 1.0f to 0 -> visBounds.Height
					sample *= -1.0f;
					sample += 1.0f;
					sample /= 2.0f;

					// now 0 -> 1.0f
					var y = (Int32)(sample * visBounds.Height);
					var x = 0;

					if(drawFixedWidth)
						x = (Int32)(((float)xIndex / (float)numSamples) * visBounds.Width);
					else
						x = (Int32)(((float)xIndex / (float)m_LongestGrainSoFar) * visBounds.Width);

					if (x > visBounds.Width &&
					   lastx > visBounds.Width)
					{
						break;
					}

					if (i >= 1)
					{
						gp.AddLine(lastx, lasty, x, y);
						//g.DrawLine(p, lastx, lasty, x, y);
					}

					lastx = x;
					lasty = y;
				}

				// draw path
				g.DrawPath(p, gp);

				g.DrawLine(Pens.Blue, 0, visBounds.Height / 2, visBounds.Width, visBounds.Height / 2);
			}
		}

		private void pitchViewXAxis_Paint(object sender, PaintEventArgs pea)
		{
			var grfx = pea.Graphics;
			var visBounds = grfx.VisibleClipBounds;
			Font arial = new Font(new FontFamily("Arial"), 10, FontStyle.Regular);
			SolidBrush textBrush = new SolidBrush(Color.Black);

			float startSampleTime = 0.0f;
			float endSampleTime = 10.0f;

			if (m_PitchData.Count != 0 &&
				m_GrainPlayer.m_WaveFile != null)
			{
				int startSampleIndex = (int)(m_Xoffset);
				int endSampleIndex = (int)(m_Xoffset + (int)(m_PitchData.Last().sampleIndex / m_Xzoom));

				startSampleTime = startSampleIndex / (float)m_GrainPlayer.m_WaveFile.Format.SampleRate;
				endSampleTime = endSampleIndex / (float)m_GrainPlayer.m_WaveFile.Format.SampleRate;
			}

			for (int loop = 0; loop < endSampleTime; loop++)
			{
				float xCoord = visBounds.Width / endSampleTime * loop;
				float thisTime = loop;

				string timeString = String.Format("{0:0.###}", thisTime);
				grfx.DrawString(timeString, arial, textBrush, new PointF(xCoord, 0));
				grfx.DrawLine(Pens.Black, xCoord, visBounds.Height / 2, xCoord, 0);
			}
		}

		private void pitchViewYAxis_Paint(object sender, PaintEventArgs pea)
		{
			float maxPitch = 100;

			if (m_PitchData.Count != 0 &&
				m_GrainPlayer.m_WaveFile != null)
			{
				maxPitch = m_MaxPitch + 20;
			}

			var grfx = pea.Graphics;
			var visBounds = grfx.VisibleClipBounds;
			Font arial = new Font(new FontFamily("Arial"), 10, FontStyle.Regular);
			SolidBrush textBrush = new SolidBrush(Color.Black);

			for (int loop = 0; loop < maxPitch; loop += 10)
			{
				float yCoord = visBounds.Height - ((visBounds.Height / maxPitch) * loop);
				float thisPitch = loop;

				string timeString = String.Format("{0:0.###}", thisPitch);
				grfx.DrawString(timeString, arial, textBrush, new PointF((visBounds.Width / 2 )- 3, yCoord + 4));
				grfx.DrawLine(Pens.Black, visBounds.Width / 2, yCoord, visBounds.Width, yCoord);
			}
		}

		private void pitchView_Paint(object sender, PaintEventArgs e)
		{
			if (m_PitchData.Count == 0)
			{
				return;
			}

			float maxPitch = 100;

			if (m_PitchData.Count != 0 &&
				m_GrainPlayer.m_WaveFile != null)
			{
				maxPitch = m_MaxPitch + 20;
			}

			var p = new Pen(new SolidBrush(Color.DarkBlue), 1.0f);

			var g = e.Graphics;
			var visBounds = g.VisibleClipBounds;
			m_MainDisplayVisBounds = visBounds;
			m_WindowWidth = visBounds.Width;

			m_Yscale = 1.0f * m_Yzoom;
			// scale x so that the entire sample fits in the visbounds
			m_Xscale = (visBounds.Width / m_PitchData.Count) * m_Xzoom;

			Int32 lastx = 0, lasty = 0;
			var gp = new GraphicsPath();

			g.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb(240, 240, 255)),
							0,
							0,
							(Int32)((m_PitchData.Count - m_Xoffset) * m_Xscale),
							visBounds.Height);

			p = Pens.DarkBlue;

			for (var i = m_Xoffset; i < m_PitchData.Count; i++)
			{
				float pitchValue = m_PitchData[i].pitch;

				// scale from 0.0f -> 1.0f
				float pitchProportion = 1.0f - pitchValue / maxPitch;

				// now 0 -> 1.0f
				var y = (Int32)(pitchProportion * visBounds.Height);
				var x = i;

				x = (Int32)((x - m_Xoffset) * m_Xscale);

				y = (Int32)(y * m_Yscale);

				if (x > visBounds.Width &&
				   lastx > visBounds.Width)
				{
					break;
				}

				if (i >= 1)
				{
					gp.AddLine(lastx, lasty, x, y);
					//g.DrawLine(p, lastx, lasty, x, y);
				}

				//g.FillEllipse(new SolidBrush(Color.Black), x, y, 3, 3);

				lastx = x;
				lasty = y;
			}

			// draw path
			g.DrawPath(p, gp);

			// draw cursor position
			g.DrawLine(Pens.Black,
					   (m_PitchSelectionCursor - m_Xoffset) * m_Xscale,
					   0,
					   (m_PitchSelectionCursor - m_Xoffset) * m_Xscale,
					   visBounds.Height);

			// progress cursor
			if (m_GenerationThread != null &&
				m_GenerationThread.IsAlive)
			{
				int samplesInWindow = (int)m_GrainPlayer.m_WaveFile.Data.NumSamples;
				float fractionSelected = m_GranuleGenerationCursor/ (float)samplesInWindow;
				int finalCursorPos = (int)(m_WindowWidth * fractionSelected);

				g.DrawLine(Pens.Red,
						   finalCursorPos,
						   0,
						   finalCursorPos,
						   visBounds.Height);
			}
			else
			{
				int boxWidth = Math.Abs(m_RegionSelectCursorEnd - m_RegionSelectCursorStart);
				int boxStart = Math.Min(m_RegionSelectCursorStart, m_RegionSelectCursorEnd);

				g.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb(96, 255, 0, 0)),
						boxStart * m_Xscale,
						0,
						boxWidth * m_Xscale,
						visBounds.Height);
			}

			// info box
			g.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb(210, 255, 255, 255)),
				0,
				0,
				420.0f,
				17.0f);

			Font arial = new Font(new FontFamily("Arial"), 10, FontStyle.Regular);
			SolidBrush textBrush = new SolidBrush(Color.Black);
			float rpm = m_PitchData[m_PitchSelectionCursor].pitch * 60 * 2.0f;
			string rpmString = rpm.ToString() + " RPM";

			float sampleIndex = m_PitchData[m_PitchSelectionCursor].sampleIndex;
			float milliseconds = sampleIndex / m_GrainPlayer.m_WaveFile.Format.SampleRate;
			milliseconds *= 1000;

			g.DrawString("Sample " + sampleIndex + " @ " + String.Format("{0:0.###}", milliseconds) + "ms: " + String.Format("{0:0.###}", m_PitchData[m_PitchSelectionCursor].pitch) + " Hz / " + rpmString, arial, textBrush, new PointF(0.0f, 0.0f));
		}

		private void pitchView_MouseDown(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)
			{
				m_SelectingRegion = true;
				
				if (m_WindowWidth > 0)
				{
					int pitchValuesInWindow = (int)(m_PitchData.Count / m_Xzoom);
					float fractionSelected = e.Location.X / m_WindowWidth;
					m_RegionSelectCursorStart = (int)(m_Xoffset + (pitchValuesInWindow * fractionSelected));
					m_RegionSelectCursorEnd = m_RegionSelectCursorStart;
				}
			}
			else if(e.Button == MouseButtons.Right)
			{
				int offset = 50;
				contextMenuStrip.Show(e.Location.X + this.Location.X + offset, e.Location.Y + this.Location.Y + offset);
			}
		}

		private void pitchView_MouseUp(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)
			{
				m_SelectingRegion = false;
				
				if (m_WindowWidth > 0)
				{
					int pitchValuesInWindow = (int)(m_PitchData.Count / m_Xzoom);
					float fractionSelected = e.Location.X / m_WindowWidth;
					m_RegionSelectCursorEnd = (int)(m_Xoffset + (pitchValuesInWindow * fractionSelected));
				}
			}
		}

		private void pitchView_MouseMove(object sender, MouseEventArgs e)
		{
			if(m_WindowWidth > 0)
			{
				int pitchValuesInWindow = (int)(m_PitchData.Count / m_Xzoom);
				float fractionSelected = e.Location.X / m_WindowWidth;
				m_PitchSelectionCursor = (int)(m_Xoffset + (pitchValuesInWindow * fractionSelected));

				if (m_PitchSelectionCursor >= m_PitchData.Count)
				{
					m_PitchSelectionCursor = m_PitchData.Count - 1;
				}

				if (m_PitchSelectionCursor < 0)
					m_PitchSelectionCursor = 0;

				if(m_SelectingRegion)
				{
					m_RegionSelectCursorEnd = m_PitchSelectionCursor;
				}
			}
		}

		private void GenerateGranules()
		{
			CalculateZeroCrossings();
			m_GenerationThread.Abort();
			m_GenerationThread = null;
		}

		private void LoadPitchData()
		{
			m_PitchData.Clear();
			m_MaxPitch = 0.0f;

			if(m_PitchDataFilename.EndsWith("paf"))
			{
				LoadPitchDataTextFile();
			}
			else
			{
				LoadPitchDataExcelFile();
			}
		}

		public void Reset()
		{
			m_PitchSelectionCursor = 0;
			m_PitchData.Clear();
			m_OldPitchData.Clear();
			m_MaxPitch = 0.0f;
		}

		void LoadPitchDataTextFile()
		{
			System.IO.StreamReader textReader = new System.IO.StreamReader(m_PitchDataFilename);
			string line;

			while ((line = textReader.ReadLine()) != null)
			{
				PitchData newPitchData = new PitchData();
				newPitchData.sampleIndex = Convert.ToInt32(line);
				line = textReader.ReadLine();
				newPitchData.pitch = Convert.ToSingle(line);
				m_PitchData.Add(newPitchData);

				if (newPitchData.pitch > m_MaxPitch)
				{
					m_MaxPitch = newPitchData.pitch;
				}
			}

			textReader.Close();
		}

		void LoadPitchDataExcelFile()
		{
			Excel.Application xlApp;
			Excel.Workbook xlWorkBook;
			Excel.Worksheet xlWorkSheet;
			Excel.Range range;

			string str;
			int rCnt = 0;
			int cCnt = 0;

			xlApp = new Excel.ApplicationClass();
			xlWorkBook = xlApp.Workbooks.Open(m_PitchDataFilename, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
			xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

			range = xlWorkSheet.UsedRange;

			for (rCnt = 1; rCnt <= range.Rows.Count; rCnt++)
			{
				PitchData pitchData = new PitchData();

				for (cCnt = 1; cCnt <= range.Columns.Count; cCnt++)
				{
					str = (string)(range.Cells[rCnt, cCnt] as Excel.Range).Value2;

					// Yeah, this is daft, but quicker than trying to figure out excel syntax!
					int commaPos = str.IndexOf(',');
					string leftSide = str;
					string rightSide = str;
					leftSide = leftSide.Remove(commaPos, str.Length - commaPos);
					rightSide = rightSide.Remove(0, commaPos + 1);

					pitchData.sampleIndex = (int)double.Parse(leftSide, System.Globalization.NumberStyles.AllowExponent | System.Globalization.NumberStyles.AllowDecimalPoint);
					pitchData.pitch = float.Parse(rightSide);

					if(pitchData.pitch > m_MaxPitch)
					{
						m_MaxPitch = pitchData.pitch;
					}
				}

				m_PitchData.Add(pitchData);
			}

			xlWorkBook.Close(true, null, null);
			xlApp.Quit();

			releaseObject(xlWorkSheet);
			releaseObject(xlWorkBook);
			releaseObject(xlApp);
		}

		void CalculateZeroCrossings()
		{
			m_LongestGrainSoFar = 0;
			m_GranuleBarProgress = 0;

			m_GrainPlayer.ClearGrainData();

			float searchPercentage = (float)searchPercentageUpDown.Value; // How far we can deviate from the expected block end position
			float startEndFactor = (float)startEndBiasCombo.Value;        // How much significance to place on matching start/end DC values
			float zeroCrossingFactor = (float)zeroCrossingBiasCombo.Value;    // How much significance to place on start/ends being zero crossings

			while (m_GrainPlayer.GetFinalGrainPos() * sizeof(short) < m_GrainPlayer.m_WaveFile.Data.RawData.Length)
			{
				int previous = m_GrainPlayer.GetFinalGrainPos();

				float nearestPitch = FindNearestPitch(previous);
				m_GranuleGenerationCursor = previous;

				int blockLengthMs = (int)Math.Round(1000.0f / nearestPitch);
				int blockLength = (int)((m_GrainPlayer.m_WaveFile.Format.SampleRate / 1000) * blockLengthMs);
				int searchRange = (int)((blockLength * searchPercentage) / 100);

				int probableMatch = previous + blockLength;
				int minMatch = probableMatch - searchRange;
				int maxMatch = probableMatch + searchRange;
				int bestMatch = probableMatch;
				double leastError = float.MaxValue;

				for (int match = minMatch; match < maxMatch; match++)
				{
					float expectedMatchBias = 1.0f + Math.Abs((float)expectedMatchBiasUpDown.Value * (1.0f - ((probableMatch - match) / (float)(probableMatch - minMatch))));

					/* Sine version - seems to work less well
					float expectedMatchBias = Math.Abs((1.0f - ((probableMatch - match) / (float)(probableMatch - minMatch))));
					expectedMatchBias = 1.0f + ((float)expectedMatchBiasUpDown.Value * (float)Math.Sin((Math.PI / 2) * expectedMatchBias));
					*/

					double error = 0;
					int convolutionPeriod = match - previous;

					for (int i = 0; i < convolutionPeriod; i++)
					{
						error -= expectedMatchBias * ((double)m_GrainPlayer.ReadSampleFromSampleIndex(previous + i) * (double)m_GrainPlayer.ReadSampleFromSampleIndex(match + i));
					}

					// Get error per sample
					error /= (float)convolutionPeriod;

					// Add a bias towards ending on the same value as we started ...
					float diff = (float)m_GrainPlayer.ReadSampleFromSampleIndex(previous) - (float)m_GrainPlayer.ReadSampleFromSampleIndex(match);
					float diff2 = diff * diff; // Kind of in the same sort of units as the 'error' as that is now per sample
					error += diff2 * startEndFactor;

					// Look for zero crossings?
					error += ((float)m_GrainPlayer.ReadSampleFromSampleIndex(match) * (float)m_GrainPlayer.ReadSampleFromSampleIndex(match)) * zeroCrossingFactor;

					if (error < leastError)
					{
						leastError = error;
						bestMatch = match;
					}
				}

				if (bestMatch == previous)
				{
					break;
				}

				m_GrainPlayer.AddGrainData(bestMatch);
				m_GranuleBarProgress = (int)(100 * ((float)m_GrainPlayer.GetFinalGrainPos() * sizeof(short) / (float)m_GrainPlayer.m_WaveFile.Data.RawData.Length));
			}

			m_GranuleBarProgress = 100;
		}

		public float FindNearestPitch(int sampleIndex)
		{
			// Todo - can this be improved? Maybe work out average pitch across the 
			// entire grain?
			if(m_PitchData.Count > 0)
			{
				for (int loop = 0; loop < m_PitchData.Count - 1; loop++)
				{
					if (loop == 0 && sampleIndex < m_PitchData[loop].sampleIndex)
					{
						return m_PitchData[loop].pitch;
					}
					else
					{
						if (sampleIndex >= m_PitchData[loop].sampleIndex &&
							sampleIndex < m_PitchData[loop + 1].sampleIndex)
						{
							float proportion = (sampleIndex - m_PitchData[loop].sampleIndex) / (float)(m_PitchData[loop + 1].sampleIndex - m_PitchData[loop].sampleIndex);

							// Linear interpolation
							//return m_PitchData[loop].pitch + ((m_PitchData[loop + 1].pitch - m_PitchData[loop].pitch) * proportion);

							// Cosine interpolation
							float prevPrevPitch = m_PitchData[loop].pitch;
							float prevPitch = m_PitchData[loop].pitch;
							float nextPitch = m_PitchData[loop + 1].pitch;
							float nextNextPitch = m_PitchData[loop + 1].pitch;
								
							if(loop > 0)
							{
								prevPrevPitch = m_PitchData[loop - 1].pitch;
							}
							
							if(loop + 2 < m_PitchData.Count)
							{
								nextNextPitch = m_PitchData[loop + 2].pitch;
							}

							double mu = proportion;
							double mu2 = mu * mu;
							double a0 = nextNextPitch - nextPitch - prevPrevPitch + prevPitch;
							double a1 = prevPrevPitch - prevPitch - a0;
							double a2 = nextPitch - prevPrevPitch;
							double a3 = prevPitch;

							return (float)(a0 * mu * mu2 + a1 * mu2 + a2 * mu + a3);
						}
					}
				}

				return m_PitchData.Last().pitch;
			}

			return 0.0f;
		}

		public void AddPitchDataFront(int sampleIndex, float pitchValue)
		{
			PitchData data = new PitchData();
			data.pitch = pitchValue;
			data.sampleIndex = sampleIndex;
			m_PitchData.Insert(0, data);

			if (data.pitch > m_MaxPitch)
			{
				m_MaxPitch = data.pitch;
			}
		}

		public void AddPitchDataBack(int sampleIndex, float pitchValue)
		{
			PitchData data = new PitchData();
			data.pitch = pitchValue;
			data.sampleIndex = sampleIndex;
			m_PitchData.Add(data);

			if (data.pitch > m_MaxPitch)
			{
				m_MaxPitch = data.pitch;
			}
		}

		private void releaseObject(object obj)
		{
			System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
			obj = null;
		}

		private void redrawTimer_Tick(object sender, EventArgs e)
		{
			granuleProgressBar.Value = m_GranuleBarProgress;
			granuleProgressBar.Refresh();

			prevGrainView.Refresh();
			pitchView.Refresh();
			pitchViewXAxis.Refresh();
			pitchViewYAxis.Refresh();

			if(m_GenerationThread != null &&
				m_GenerationThread.IsAlive)
			{
				generateGranulesBtn.Text = "Cancel";
				searchPercentageUpDown.Enabled = false;
				startEndBiasCombo.Enabled = false;
				zeroCrossingBiasCombo.Enabled = false;
			}
			else
			{
				generateGranulesBtn.Text = "Generate Granules";

				if (m_PitchData.Count > 0)
				{
					generateGranulesBtn.Enabled = true;
				}
				else
				{
					generateGranulesBtn.Enabled = false;
				}

				searchPercentageUpDown.Enabled = true;
				startEndBiasCombo.Enabled = true;
				zeroCrossingBiasCombo.Enabled = true;
				m_GranuleBarProgress = 0;
			}
		}

		private void loadPitchDataToolStripMenuItem1_Click(object sender, EventArgs e)
		{
			loadPitchDataDlg.ShowDialog();
		}

		private void flattenSelectionToolStripMenuItem_Click(object sender, EventArgs e)
		{
			SmoothSelection(1.0f);
		}

		private void smoothSelectionToolStripMenuItem_Click(object sender, EventArgs e)
		{
			SmoothSelection(0.1f);
		}

		private void smoothMoreToolStripMenuItem_Click(object sender, EventArgs e)
		{
			SmoothSelection(0.5f);
		}

		void SmoothSelection(float severity)
		{
			Backup();

			int regionStart = Math.Min(m_RegionSelectCursorStart, m_RegionSelectCursorEnd);
			int regionEnd = Math.Max(m_RegionSelectCursorStart, m_RegionSelectCursorEnd);

			if(regionEnd >= m_PitchData.Count)
			{
				regionEnd = m_PitchData.Count - 1;
			}

			if(regionStart < 0)
			{
				regionStart = 0;
			}

			int regionLength = regionEnd - regionStart;
			int index = 0;

			for (int loop = regionStart; loop < regionEnd; loop++)
			{
				float desiredPitch = m_PitchData[regionStart].pitch +
										 (m_PitchData[regionEnd].pitch - m_PitchData[regionStart].pitch) * (index / (float)regionLength);

				float pitchDifference = m_PitchData[loop].pitch - desiredPitch;

				m_PitchData[loop].pitch -= (pitchDifference * severity);

				index++;
			}
		}

		void ModifySelection(float amount)
		{
			Backup();

			int regionStart = Math.Min(m_RegionSelectCursorStart, m_RegionSelectCursorEnd);
			int regionEnd = Math.Max(m_RegionSelectCursorStart, m_RegionSelectCursorEnd);

			if (regionEnd > m_PitchData.Count)
			{
				regionEnd = m_PitchData.Count;
			}

			if (regionStart < 0)
			{
				regionStart = 0;
			}

			for (int loop = regionStart; loop < regionEnd; loop++)
			{
				m_PitchData[loop].pitch += amount;
			}
		}

		private void savePitchDataDlg_FileOk(object sender, CancelEventArgs e)
		{
			SaveFileDialog dialog = sender as SaveFileDialog;
			System.IO.StreamWriter fileStream = System.IO.File.CreateText(dialog.FileName);

			float desiredPitchReadingsPerSecond = 4.0f;
			float actualPitchReadingsPerSecond = m_PitchData.Count / (m_GrainPlayer.m_WaveFile.Data.NumSamples / (float)m_GrainPlayer.m_WaveFile.Format.SampleRate);

			int stepRate = 1;

			if(actualPitchReadingsPerSecond > desiredPitchReadingsPerSecond)
			{
				stepRate = (int)(actualPitchReadingsPerSecond / desiredPitchReadingsPerSecond);
			}

			// Scale down the pitch readings to a sensible amount for game consumption
			for (int loop = 0; loop < m_PitchData.Count; loop += stepRate)
			{
				fileStream.WriteLine(m_PitchData[loop].sampleIndex);
				fileStream.WriteLine(m_PitchData[loop].pitch);
			}

			fileStream.Close();
		}

		private void savePitchDataToolStripMenuItem_Click(object sender, EventArgs e)
		{
			string fileName = m_PlaybackScreen.m_CurrentFileName.Replace(".WAV", ".paf");
			savePitchDataDlg.FileName = fileName;
			savePitchDataDlg.ShowDialog();
		}

		private void quitToolStripMenuItem_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		void Backup()
		{
			m_OldPitchData.Clear();
			foreach(PitchData pitchData in m_PitchData)
			{
				PitchData copiedData = new PitchData();
				copiedData.pitch = pitchData.pitch;
				copiedData.sampleIndex = pitchData.sampleIndex;
				m_OldPitchData.Add(copiedData);
			}
		}

		void Undo()
		{
			if (m_OldPitchData.Count > 0)
			{
				m_PitchData.Clear();

				foreach (PitchData pitchData in m_OldPitchData)
				{
					PitchData copiedData = new PitchData();
					copiedData.pitch = pitchData.pitch;
					copiedData.sampleIndex = pitchData.sampleIndex;
					m_PitchData.Add(copiedData);
				}

				m_OldPitchData.Clear();
			}
		}

		private void undoToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Undo();
		}

		private void showPreviewCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			if(showPreviewCheckBox.Checked)
			{
				grainPreviewLabel.Visible = true;
				fixedWidthComboBox.Visible = true;
				prevGrainView.Visible = true;
			}
			else
			{
				grainPreviewLabel.Visible = false;
				fixedWidthComboBox.Visible = false;
				prevGrainView.Visible = false;
			}
		}

		private void lowerPitchToolStripMenuItem_Click(object sender, EventArgs e)
		{
			ModifySelection(-1);
		}

		private void raisePitchToolStripMenuItem_Click(object sender, EventArgs e)
		{
			ModifySelection(1);
		}
	}
}
