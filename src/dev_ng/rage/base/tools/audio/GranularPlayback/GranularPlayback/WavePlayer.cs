﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;

using Microsoft.DirectX.DirectSound;
using Excel = Microsoft.Office.Interop.Excel;

using Wavelib;

namespace GranularPlayback
{
	public class WavePlayer
	{
		public bwWaveFile m_WaveFile;

		private readonly Device m_Device;
		private System.IO.MemoryStream m_waveDataStream;

		private Microsoft.DirectX.DirectSound.Buffer m_Buffer;
		private Microsoft.DirectX.DirectSound.Buffer m_GrainBuffer;
		private int m_GrainBufferBytes;
		private System.Timers.Timer m_GrainFeedTimer;
		private int m_NextGrainWrite;
		private System.Threading.Mutex m_Mutex;

		private float m_Xscale;
		private float m_Xzoom = 1.0f;
		private float m_Yscale;
		private float m_Yzoom = 1.0f;
		private int m_PlaybackCursor = 0;
		private int m_Xoffset = 0;
		private float m_WindowWidth = 0;

		private bool[] m_SelectedGrains;
		private int m_NumSelectedGrains;
		private int m_SelectedGrainIndex = 0;
		private int m_GrainSelectionCursor = 0;
		private int m_MaxLatencyMs = 1;

		private int m_GrainRepetition = 1;
		private int m_RepetitionCounter = 0;

		public bool m_DrawGrainBoundaries = true;
		public bool m_DrawSamplePositions = false;

		public Point m_LastMousePoint;
		public bool m_ViewBeingDragged = false;
		public bool m_GrainCursorBeingDragged = false;
		public bool m_MouseOverView = false;

		Control m_Owner;
		Random m_RandomGenerator;

		enum GrainPlaybackOrder
		{
			PlaybackOrderSequential,
			PlaybackOrderRandom,
			PlaybackOrderWalk,
			PlaybackOrderReverse,
		};

		enum audGrainPlayerMode
		{
			OverlapAndAdd,
			BlendEdges,
			Crossfade,
		};

		struct audGrainState
		{
			public int data;
			public int grainLength;
			public int samplesLeft;
			public float windowInterp;
			public float windowInterpStep;
			public float averageBlendVal;
			public bool isPlaying;
		};

		private List<int> m_GrainData;
		audGrainState[] m_ActiveGrains = new audGrainState[2];
		int m_ActiveGrainIndex = 0;
		int m_LastGrainIndex = 0;
		public int m_CrossFadePercentage = 0;
		int m_CrossFade = 0;
		float m_EnvAttackValue = 0.0f;
		float m_EnvReleaseValue = 0.0f;
		float m_StepVal = 0.0f;
		float m_OverlapFraction = 0.0f;
		public float m_WindowAmount = 0.1f;

		audGrainPlayerMode m_GrainPlayerMode = audGrainPlayerMode.BlendEdges;
		GrainPlaybackOrder m_GrainPlaybackOrder = GrainPlaybackOrder.PlaybackOrderSequential;

		int m_WalkPlaybackStepDirection = 1;
		public float m_SimulatedRPMProportion = 0.0f;
		public bool m_SimulatorActive = false;

		/// <summary>
		/// Helper lerp function - same as Rage
		/// </summary>
		/// <param name="t"></param>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns></returns>
		float Lerp(float t, float a, float b)
		{
			return (a+(b-a)*t);
		}

		/// <summary>
		/// Wave player constructor
		/// </summary>
		/// <param name="owner"></param>
		public WavePlayer(Control owner)
        {
			m_Owner = owner;
			m_Device = new Device();
			m_Device.SetCooperativeLevel(owner, CooperativeLevel.Priority);
			m_GrainData = new List<int>();
			m_RandomGenerator = new Random();
			m_Mutex = new System.Threading.Mutex();
        }

		/// <summary>
		/// Play the whole wave file from start to end
		/// </summary>
		/// <param name="priority"></param>
		/// <param name="flags"></param>
        public void Play(int priority, BufferPlayFlags flags)
        {
			if (!IsBufferNull())
            {
                m_Buffer.Play(priority, flags);
            }
        }

		/// <summary>
		/// Kick off the granular playback of the file
		/// </summary>
		public void PlayGranular()
		{
			if(!IsGrainBufferNull())
			{
				if(m_GrainData.Count == 0)
				{
					MessageBox.Show("No grain data has been loaded!", "Cannot play grain");
				}
				else
				{
					m_SelectedGrainIndex = 0;

					// start halfway through the first grain
					m_ActiveGrainIndex = 0;
					m_LastGrainIndex = 0;
					int numSamples = m_GrainData[1] - m_GrainData[0];
					int sampleIndex = m_GrainData[0];
					m_ActiveGrains[m_ActiveGrainIndex].samplesLeft = numSamples;
					m_ActiveGrains[m_ActiveGrainIndex].data = sampleIndex;
					m_ActiveGrains[m_ActiveGrainIndex].windowInterpStep = 1.0f / (float)numSamples;
					m_ActiveGrains[m_ActiveGrainIndex].windowInterp = 0.0f;
					m_ActiveGrains[m_ActiveGrainIndex].grainLength = numSamples;
					m_ActiveGrains[m_ActiveGrainIndex].isPlaying = true;
					m_ActiveGrains[1].isPlaying = false;

					FeedGranular(m_GrainBufferBytes);
					m_GrainFeedTimer.Enabled = true;
					m_GrainBuffer.Play(0, BufferPlayFlags.Looping);
				}
			}
		}

		/// <summary>
		/// Reset all the grain data
		/// </summary>
		public void ClearGrainData()
		{
			m_GrainData.Clear();
		}

		/// <summary>
		/// Add a new grain start point
		/// </summary>
		/// <param name="grainPos"></param>
		public void AddGrainData(int grainPos)
		{
			m_GrainData.Add(grainPos);
			m_SelectedGrains = new bool[m_GrainData.Count];
		}

		/// <summary>
		/// Get the final grain start position in the sequence
		/// </summary>
		/// <returns></returns>
		public int GetFinalGrainPos()
		{
			if (m_GrainData.Count > 0)
			{
				return m_GrainData.Last();
			}

			return 1;
		}

		/// <summary>
		/// Get the number of grains
		/// </summary>
		/// <returns></returns>
		public int GetNumGrains()
		{
			return m_GrainData.Count;
		}

		/// <summary>
		/// Get the grain at the given position
		/// </summary>
		/// <param name="index"></param>
		/// <returns></returns>
		public int GetGrainPos(int index)
		{
			return m_GrainData[index];
		}

		/// <summary>
		/// Get the grain index for the given sample
		/// </summary>
		/// <returns></returns>
		public int GetGrainIndexForSample(int sampleIndex)
		{
			int prevGrainData = -1;
			for(int loop = 0; loop < m_GrainData.Count; loop++)
			{
				if(prevGrainData < sampleIndex &&
					m_GrainData[loop] >= sampleIndex)
				{
					return loop;
				}

				prevGrainData = m_GrainData[loop];
			}

			return -1;
		}

		/// <summary>
		/// Timer callback when we need to buffer more granular data. This code has been ported
		/// from the Rage implementation of GrainPlayer.cpp, so as to ensure parity between the two
		/// </summary>
		/// <param name="bufferBytes"></param>
		void FeedGranular(int bufferBytes)
		{
			m_Mutex.WaitOne();

			int mixSamples = BytesToSampleIndex(bufferBytes);
			float[] destBuffer = new float[mixSamples];

			for (int i = 0; i < mixSamples; i++)
			{
				// is the active grain finished?
				if (m_ActiveGrains[m_ActiveGrainIndex].samplesLeft == 0)
				{
					// next grain becomes the active grain
					m_ActiveGrainIndex = (m_ActiveGrainIndex + 1) % 2;

					// regardless of overlap fraction at this stage if the grain hasn't yet been kicked off
					// we need to do it.
					m_ActiveGrains[m_ActiveGrainIndex].isPlaying = true;
				}

				int nextActiveIndex = (m_ActiveGrainIndex + 1) % 2;

				// do we need to pick a new next grain?
				if (m_ActiveGrains[nextActiveIndex].samplesLeft == 0)
				{
					m_RepetitionCounter++;

					if(m_RepetitionCounter >= m_GrainRepetition)
					{
						m_RepetitionCounter = 0;

						if (m_SimulatorActive)
						{
							m_GrainPlaybackOrder = GrainPlaybackOrder.PlaybackOrderRandom;

							int desiredSampleIndex = (int)(m_WaveFile.Data.NumSamples * m_SimulatedRPMProportion);

							for (int loop = 2; loop < m_GrainData.Count - 2; loop++)
							{
								if (m_GrainData[loop] > desiredSampleIndex)
								{
									ResetSelectedGrains();
									//m_SelectedGrains[loop-2] = true;
									m_SelectedGrains[loop-1] = true;
									m_SelectedGrains[loop] = true;
									m_SelectedGrains[loop+1] = true;
									//m_SelectedGrains[loop+2] = true;
									m_NumSelectedGrains = 3;
									break;
								}
							}
						}

						if (m_GrainPlaybackOrder == GrainPlaybackOrder.PlaybackOrderSequential)
						{
							m_SelectedGrainIndex++;

							// Played all the grains? Loop back to the start
							if (m_SelectedGrainIndex >= m_NumSelectedGrains)
							{
								m_SelectedGrainIndex = 0;
							}
						}
						else if (m_GrainPlaybackOrder == GrainPlaybackOrder.PlaybackOrderRandom)
						{
							int originalIndex = m_SelectedGrainIndex;

							// Make sure we don't pick the same grain more than once
							while (m_SelectedGrainIndex == originalIndex &&
								   m_NumSelectedGrains > 1)
							{
								m_SelectedGrainIndex = m_RandomGenerator.Next(0, m_NumSelectedGrains);
							}
						}
						else if (m_GrainPlaybackOrder == GrainPlaybackOrder.PlaybackOrderWalk)
						{
							m_SelectedGrainIndex += m_WalkPlaybackStepDirection;

							if (m_SelectedGrainIndex < 0)
							{
								m_SelectedGrainIndex = 0;
								m_WalkPlaybackStepDirection = 1;
							}
							else if (m_SelectedGrainIndex >= m_NumSelectedGrains)
							{
								m_SelectedGrainIndex = m_NumSelectedGrains - 1;
								m_WalkPlaybackStepDirection = -1;
							}
						}
						else if (m_GrainPlaybackOrder == GrainPlaybackOrder.PlaybackOrderReverse)
						{
							m_SelectedGrainIndex--;

							// Played all the grains? Loop back to the start
							if (m_SelectedGrainIndex < 0)
							{
								m_SelectedGrainIndex = m_NumSelectedGrains - 1;
							}
						}
					}

					if (m_SelectedGrainIndex >= m_NumSelectedGrains)
					{
						m_SelectedGrainIndex = m_NumSelectedGrains - 1;
					}

					if (m_SelectedGrainIndex < 0)
					{
						m_SelectedGrainIndex = 0;
					}

					int grainIndex = GetSelectedGrain(m_SelectedGrainIndex);

					if (grainIndex >= m_GrainData.Count - 1)
					{
						grainIndex = m_GrainData.Count - 2;
					}

					if (grainIndex < 0)
					{
						grainIndex = 0;
					}

					m_LastGrainIndex = grainIndex;

					int numSamples = m_GrainData[grainIndex + 1] - m_GrainData[grainIndex];
					int startSampleIndex = m_GrainData[grainIndex];
					m_ActiveGrains[nextActiveIndex].data = startSampleIndex;

					if (m_GrainPlayerMode == audGrainPlayerMode.OverlapAndAdd)
					{
						// each grain is played back to back
						m_ActiveGrains[nextActiveIndex].samplesLeft = numSamples * 2;
					}
					else
					{
						m_ActiveGrains[nextActiveIndex].samplesLeft = numSamples;
					}

					m_ActiveGrains[nextActiveIndex].windowInterp = 0.0f;
					m_ActiveGrains[nextActiveIndex].windowInterpStep = 1.0f / (float)m_ActiveGrains[nextActiveIndex].samplesLeft;
					m_ActiveGrains[nextActiveIndex].grainLength = numSamples;

					// take the mean of the first sample of this grain and last sample of the previous grain
					short firstOfThisGrain = ReadSampleFromSampleIndex(m_ActiveGrains[nextActiveIndex].data);
					short lastOfPrevGrain = ReadSampleFromSampleIndex(m_ActiveGrains[m_ActiveGrainIndex].data + m_ActiveGrains[m_ActiveGrainIndex].grainLength - 1);

					m_ActiveGrains[nextActiveIndex].averageBlendVal = ((firstOfThisGrain + lastOfPrevGrain) >> 1) / 32767.0f;
					m_ActiveGrains[nextActiveIndex].isPlaying = false;
				}

				const float divisor = 1.0f / 32767.0f;

				// if we have more samples left than the grain length then subtract an entire grain length 
				int sampleOffset = (m_ActiveGrains[m_ActiveGrainIndex].samplesLeft > m_ActiveGrains[m_ActiveGrainIndex].grainLength
					? m_ActiveGrains[m_ActiveGrainIndex].grainLength * 2 - m_ActiveGrains[m_ActiveGrainIndex].samplesLeft :
				m_ActiveGrains[m_ActiveGrainIndex].grainLength - m_ActiveGrains[m_ActiveGrainIndex].samplesLeft);

				short activeGrainSampleS16 = ReadSampleFromSampleIndex(m_ActiveGrains[m_ActiveGrainIndex].data + sampleOffset);
				float activeGrainSample = (float)activeGrainSampleS16 * divisor;

				if (m_GrainPlayerMode == audGrainPlayerMode.OverlapAndAdd)
				{
					// hamming window
					//const f32 windowGain = (0.53836f - 0.46164f * Cosf(2.f * PI * m_ActiveGrains[m_ActiveGrainIndex].windowInterp));
					//const f32 nextGrainWindowed = nextGrainSample * (0.53836f - 0.46164f * Cosf(2.f * PI * m_ActiveGrains[nextActiveIndex].windowInterp));

					// hann window
					float windowGain = (float)(0.5f * (1.0f - Math.Cos(2.0f * Math.PI * m_ActiveGrains[m_ActiveGrainIndex].windowInterp)));
					float activeGrainWindowed = activeGrainSample * windowGain;

					// move through interp
					m_ActiveGrains[m_ActiveGrainIndex].windowInterp += m_ActiveGrains[m_ActiveGrainIndex].windowInterpStep;
					m_ActiveGrains[m_ActiveGrainIndex].samplesLeft--;

					// see if we should start playing the next grain
					if (!m_ActiveGrains[nextActiveIndex].isPlaying && m_ActiveGrains[m_ActiveGrainIndex].windowInterp >= m_OverlapFraction)
					{
						m_ActiveGrains[nextActiveIndex].isPlaying = true;
					}

					float nextGrainWindowed = 0.0f;
					if (m_ActiveGrains[nextActiveIndex].isPlaying)
					{
						int sampleOffset2 = (m_ActiveGrains[nextActiveIndex].samplesLeft > m_ActiveGrains[nextActiveIndex].grainLength
							? m_ActiveGrains[nextActiveIndex].grainLength * 2 - m_ActiveGrains[nextActiveIndex].samplesLeft :
						m_ActiveGrains[nextActiveIndex].grainLength - m_ActiveGrains[nextActiveIndex].samplesLeft);
						short nextGrainSampleS16 = ReadSampleFromSampleIndex(m_ActiveGrains[nextActiveIndex].data + sampleOffset2);
						float nextGrainSample = (float)nextGrainSampleS16 * divisor;
						nextGrainWindowed = (float)(nextGrainSample * (0.5f * (1.0f - Math.Cos(2.0f * Math.PI * m_ActiveGrains[nextActiveIndex].windowInterp))));
						m_ActiveGrains[nextActiveIndex].windowInterp += m_ActiveGrains[nextActiveIndex].windowInterpStep;
						m_ActiveGrains[nextActiveIndex].samplesLeft--;
					}

					destBuffer[i] = activeGrainWindowed + nextGrainWindowed;
				}
				else if (m_GrainPlayerMode == audGrainPlayerMode.BlendEdges)
				{
					// blend edges method
					if (m_ActiveGrains[m_ActiveGrainIndex].samplesLeft < m_CrossFade)
					{
						if (m_ActiveGrains[m_ActiveGrainIndex].windowInterp >= 1.0f - m_WindowAmount)
						{
							float blendFactor = (1.0f - m_ActiveGrains[m_ActiveGrainIndex].windowInterp) / m_WindowAmount;

							// head towards the average sample value
							destBuffer[i] = Lerp(blendFactor, m_ActiveGrains[nextActiveIndex].averageBlendVal, activeGrainSample);
						}
						else if (m_ActiveGrains[m_ActiveGrainIndex].windowInterp <= m_WindowAmount)
						{
							float blendFactor = m_ActiveGrains[m_ActiveGrainIndex].windowInterp / m_WindowAmount;
							// head from the average sample val
							destBuffer[i] = Lerp(blendFactor, activeGrainSample, m_ActiveGrains[m_ActiveGrainIndex].averageBlendVal);
						}
						else
						{
							destBuffer[i] = activeGrainSample;
						}
					}
					else
					{
						destBuffer[i] = activeGrainSample;
					}

					// move through interp
					m_ActiveGrains[m_ActiveGrainIndex].windowInterp += m_ActiveGrains[m_ActiveGrainIndex].windowInterpStep;
					m_ActiveGrains[m_ActiveGrainIndex].samplesLeft--;
				}
				else if (m_GrainPlayerMode == audGrainPlayerMode.Crossfade)
				{
					if (m_ActiveGrains[m_ActiveGrainIndex].samplesLeft < m_CrossFade)
					{
						if (!m_ActiveGrains[nextActiveIndex].isPlaying)
						{
							m_ActiveGrains[nextActiveIndex].isPlaying = true;
							m_EnvAttackValue = 0.0f;
							m_EnvReleaseValue = 1.0f;

							m_StepVal = (float)1 / m_CrossFade;
						}

						int sampleOffset2 = (m_ActiveGrains[nextActiveIndex].samplesLeft > m_ActiveGrains[nextActiveIndex].grainLength
							? m_ActiveGrains[nextActiveIndex].grainLength * 2 - m_ActiveGrains[nextActiveIndex].samplesLeft :
							m_ActiveGrains[nextActiveIndex].grainLength - m_ActiveGrains[nextActiveIndex].samplesLeft);
						short nextGrainSampleS16 = ReadSampleFromSampleIndex(m_ActiveGrains[nextActiveIndex].data + sampleOffset2);
						float nextGrainSample = (float)nextGrainSampleS16 * divisor;

						float activeGrainWindowed = activeGrainSample * m_EnvReleaseValue;
						float nextGrainWindowed = nextGrainSample * m_EnvAttackValue;

						if (m_EnvReleaseValue > 0.0f)
						{
							m_EnvReleaseValue -= m_StepVal;
						}
						else
						{
							m_EnvReleaseValue = 0.0f;
						}
						if (m_EnvAttackValue < 1.0f)
						{
							m_EnvAttackValue += m_StepVal;
						}
						else
						{
							m_EnvAttackValue = 1.0f;
						}

						m_ActiveGrains[nextActiveIndex].windowInterp += m_ActiveGrains[nextActiveIndex].windowInterpStep;
						m_ActiveGrains[nextActiveIndex].samplesLeft--;

						destBuffer[i] = activeGrainWindowed + nextGrainWindowed;
					}
					else
					{
						destBuffer[i] = activeGrainSample;
						m_CrossFade = (int)(m_ActiveGrains[m_ActiveGrainIndex].grainLength * (m_CrossFadePercentage / 100.0f));
					}

					// move through interp
					m_ActiveGrains[m_ActiveGrainIndex].windowInterp += m_ActiveGrains[m_ActiveGrainIndex].windowInterpStep;
					m_ActiveGrains[m_ActiveGrainIndex].samplesLeft--;
				}
			}

			List<byte> finalData = new List<byte>();

			for (int loop = 0; loop < destBuffer.Length; loop++ )
			{
				short sample = (short)(destBuffer[loop] * 32767);
				byte[] sampleData = BitConverter.GetBytes(sample);

				for (int bytes = 0; bytes < sampleData.Length; bytes++)
				{
					finalData.Add(sampleData[bytes]);
				}
			}

			m_GrainBuffer.Write(m_NextGrainWrite, finalData.ToArray(), LockFlag.None);
			m_NextGrainWrite += finalData.Count;

			if (m_NextGrainWrite >= m_GrainBufferBytes)
				m_NextGrainWrite -= m_GrainBufferBytes;

			m_Mutex.ReleaseMutex();
		}

		/// <summary>
		/// Timer callback
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void GrainTimerElapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
			if (IsPlayingGranular())
			{
				FeedGranular(GetPlayedSize());
			}
		}

		/// <summary>
		/// Check how many bytes of data have been played since we last checked
		/// </summary>
		/// <returns></returns>
		private int GetPlayedSize()
		{
			int pos = m_GrainBuffer.PlayPosition;
			return pos < m_NextGrainWrite ? pos + m_GrainBufferBytes - m_NextGrainWrite : pos - m_NextGrainWrite;
		}

		/// <summary>
		/// Check if the standard plaback buffer is valid
		/// </summary>
		/// <returns></returns>
        public bool IsBufferNull()
        {
            return m_Buffer == null;
        }

		/// <summary>
		/// Check if the granular playback buffer is valid
		/// </summary>
		/// <returns></returns>
		public bool IsGrainBufferNull()
		{
			return m_GrainBuffer == null;
		}

		/// <summary>
		/// Check if the stanadrd playback buffer is playing
		/// </summary>
		/// <returns></returns>
		public bool IsPlaying()
		{
			if (m_Buffer != null)
			{
				return m_Buffer.PlayPosition != 0;
			}

			return false;
		}

		/// <summary>
		/// Check if the granular playback buffer is playing
		/// </summary>
		/// <returns></returns>
		public bool IsPlayingGranular()
		{
			if (m_GrainBuffer != null)
			{
				return m_GrainBuffer.PlayPosition != 0;
			}

			return false;
		}

		/// <summary>
		/// Get the current playback position of the standard buffer
		/// </summary>
		/// <returns></returns>
        public int GetPlaybackPosition()
        {
            return m_Buffer.PlayPosition;
        }

		/// <summary>
		/// Convert a sample index to a byte index
		/// </summary>
		/// <param name="samples"></param>
		/// <returns></returns>
		public int SamplesToByteIndex(int samples)
		{
			return samples * ((m_WaveFile.Format.SignificantBitsPerSample / 8) * m_WaveFile.Format.NumChannels);
		}

		/// <summary>
		/// Convert a byte index to a sample index
		/// </summary>
		/// <param name="bytes"></param>
		/// <returns></returns>
		public int BytesToSampleIndex(int bytes)
		{
			return bytes / ((m_WaveFile.Format.SignificantBitsPerSample / 8) * m_WaveFile.Format.NumChannels);
		}

		/// <summary>
		/// Reset all the playback variables
		/// </summary>
		public void Reset()
		{
			ResetSelectedGrains();
			m_SelectedGrainIndex = 0;
		}

		/// <summary>
		/// Read the sample at the given byte index
		/// </summary>
		/// <param name="byteIndex"></param>
		/// <returns></returns>
		public short ReadSampleFromByteIndex(int byteIndex)
		{
			if (byteIndex < m_WaveFile.Data.RawData.Length)
			{
				return BitConverter.ToInt16(m_WaveFile.Data.RawData, byteIndex);
			}
			else
			{
				return 0;
			}
		}

		/// <summary>
		/// Read the sample at the given sample index
		/// </summary>
		/// <param name="sampleIndex"></param>
		/// <returns></returns>
		public short ReadSampleFromSampleIndex(int sampleIndex)
		{
			int byteIndex = SamplesToByteIndex(sampleIndex);
			return ReadSampleFromByteIndex(byteIndex);
		}

		/// <summary>
		/// Stop all audio from playing
		/// </summary>
        public void Stop()
        {
			if (!IsBufferNull())
			{
				m_Buffer.Stop();
				m_Buffer.SetCurrentPosition(0);
			}

			if (!IsGrainBufferNull())
			{
				m_GrainBuffer.Stop();
				m_GrainBuffer.SetCurrentPosition(0);
				m_NextGrainWrite = 0;
			}
        }

		/// <summary>
		/// Set the order in which grains get played back
		/// </summary>
		/// <param name="index"></param>
		public void SetGrainPlaybackOrder(int index)
		{
			m_GrainPlaybackOrder = (GrainPlaybackOrder)index;
		}

		/// <summary>
		/// Set the crossfade style for transitioning between grains
		/// </summary>
		/// <param name="index"></param>
		public void SetGrainPlaybackStyle(int index)
		{
			m_GrainPlayerMode = (audGrainPlayerMode)index;
		}

		/// <summary>
		/// Create a new granular playback buffer
		/// </summary>
		/// <param name="desc"></param>
		/// <param name="format"></param>
		public void CreateGranularBuffer(BufferDescription desc, WaveFormat format)
		{
			desc.BufferBytes = format.AverageBytesPerSecond / 10;
			m_GrainBuffer = new Microsoft.DirectX.DirectSound.Buffer(desc, m_Device);
			m_GrainBufferBytes = m_GrainBuffer.Caps.BufferBytes;

			m_GrainFeedTimer = new System.Timers.Timer(m_MaxLatencyMs);
			m_GrainFeedTimer.Enabled = false;
			m_GrainFeedTimer.Elapsed += new System.Timers.ElapsedEventHandler(GrainTimerElapsed);
		}

		/// <summary>
		/// Create a standard playback buffer
		/// </summary>
		/// <param name="filePath"></param>
        public void CreatePlaybackBuffer(string filePath)
        {
			m_WaveFile = new bwWaveFile(filePath);

			if(m_WaveFile.Format.NumChannels > 1)
			{
				MessageBox.Show("The selected file has " + m_WaveFile.Format.NumChannels + " channels. Only mono assets are supported", "Invalid Format");
				m_WaveFile = null;
				return;
			}

			if(m_WaveFile.Data == null)
			{
				MessageBox.Show("The selected file could not be loaded. It does not appear to contain valid audio data", "Unexpected Error");
				return;
			}

			m_waveDataStream = new System.IO.MemoryStream(m_WaveFile.Data.RawData);

            var desc = new BufferDescription();
            var format = new WaveFormat();

            format.AverageBytesPerSecond = (int) m_WaveFile.Format.AverageBytesPerSecond;
            format.BitsPerSample = (short) m_WaveFile.Format.SignificantBitsPerSample;
            format.BlockAlign = (short) m_WaveFile.Format.BlockAlign;
            format.Channels = (short) m_WaveFile.Format.NumChannels;
            format.SamplesPerSecond = (int) m_WaveFile.Format.SampleRate;

            desc.Format = format;
            desc.BufferBytes = (int)(m_WaveFile.Data.NumSamples * (format.BitsPerSample / 8) * format.Channels);

            desc.CanGetCurrentPosition = true;
            desc.Control3D = false;
            desc.ControlEffects = false;
            desc.ControlFrequency = false;
            desc.ControlPan = false;
            desc.ControlPositionNotify = false;
            desc.ControlVolume = false;
            desc.DeferLocation = false;
            desc.GlobalFocus = true;
            desc.Guid3DAlgorithm = Guid.Empty;
            desc.PrimaryBuffer = false;

			m_Buffer = new Microsoft.DirectX.DirectSound.Buffer(filePath, desc, m_Device);
			CreateGranularBuffer(desc, format);
        }

		/// <summary>
		/// Update the wave image
		/// </summary>
		/// <param name="pea"></param>
		public void DrawWave(PaintEventArgs pea)
		{
			if (!IsBufferNull())
			{
				var grfx = pea.Graphics;
				var visBounds = grfx.VisibleClipBounds;
				m_WindowWidth = visBounds.Width;

				m_Yscale = 1.0f * m_Yzoom;
				// scale x so that the entire sample fits in the visbounds
				m_Xscale = (visBounds.Width / m_WaveFile.Data.NumSamples) * m_Xzoom;

				RenderWaveView(grfx, visBounds);
			}
		}

		/// <summary>
		/// Update the axis for the wave image
		/// </summary>
		/// <param name="pea"></param>
		public void DrawAxis(PaintEventArgs pea)
		{
			var grfx = pea.Graphics;
			var visBounds = grfx.VisibleClipBounds;
			Font arial = new Font(new FontFamily("Arial"), 10, FontStyle.Regular);
			SolidBrush textBrush = new SolidBrush(Color.Black);

			float startSampleTime = 0.0f;
			float endSampleTime = 10.0f;

			if (!IsBufferNull())
			{
				int startSampleIndex = (int)(m_Xoffset);
				int endSampleIndex = (int)(m_Xoffset + (int)(m_WaveFile.Data.NumSamples / m_Xzoom));

				startSampleTime = startSampleIndex / (float)m_WaveFile.Format.SampleRate;
				endSampleTime = endSampleIndex / (float)m_WaveFile.Format.SampleRate;
			}

			for (int loop = 0; loop < 10; loop++)
			{
				float xCoord = visBounds.Width / 10 * loop;
				float thisTime = startSampleTime + ((endSampleTime - startSampleTime)/10) * loop;

				string timeString = String.Format("{0:0.###}", thisTime);
				grfx.DrawString(timeString, arial, textBrush, new PointF(xCoord + 1, 3));
				grfx.DrawLine(Pens.Black, xCoord, visBounds.Height/2, xCoord, 0);
			}
		}

		/// <summary>
		/// Draw the wave
		/// </summary>
		/// <param name="g"></param>
		/// <param name="visBounds"></param>
		private void RenderWaveView(Graphics g, RectangleF visBounds)
		{
			Int32 lastx = 0, lasty = 0;
			var gp = new GraphicsPath();
			var p = Pens.DarkBlue;

			g.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb(200, 200, 200)),
							0,
							0,
							(Int32)((m_WaveFile.Data.NumSamples - m_Xoffset) * m_Xscale),
							visBounds.Height);

			// Default step rate
			int stepRate = 15;
			float zoomPercentage = (m_Xzoom - 100)/100.0f;

			if (zoomPercentage > 1.0f)
				zoomPercentage = 1.0f;

			if (zoomPercentage < 0.0f)
				zoomPercentage = 0.0f;

			// As we zoom in more, increase the accuracy
			stepRate -= (int)((stepRate - 1) * zoomPercentage);

			for (var i = m_Xoffset; i < m_WaveFile.Data.NumSamples; i += stepRate)
			{
				float sample = m_WaveFile.Data.ChannelData[0, i];

				// scale from -1.0f -> 1.0f to 0 -> visBounds.Height
				sample *= -1.0f;
				sample += 1.0f;
				sample /= 2.0f;

				// now 0 -> 1.0f
				var y = (Int32)(sample * visBounds.Height);
				var x = i;

				x = (Int32)((x - m_Xoffset) * m_Xscale);

				y = (Int32)(y * m_Yscale);

				if(x > visBounds.Width &&
				   lastx > visBounds.Width)
				{
					break;
				}

				if (i >= 1)
				{
					gp.AddLine(lastx, lasty, x, y);
					//g.DrawLine(p, lastx, lasty, x, y);
				}

				if (m_DrawSamplePositions)
				{
					g.FillEllipse(new SolidBrush(Color.Black), x-2, y-2, 4, 4);	
				}

				lastx = x;
				lasty = y;
			}

			// draw path
			g.DrawPath(p, gp);

			if (m_DrawGrainBoundaries)
			{
				for (var grainLoop = 0; grainLoop < m_GrainData.Count; grainLoop++)
				{
					var x = (Int32)((m_GrainData[grainLoop] - m_Xoffset) * m_Xscale);

					if (x > visBounds.Width)
					{
						break;
					}

					g.DrawLine(Pens.HotPink, x, visBounds.Height, x, 0);
				}
			}

			// draw selected grain
			if (m_GrainData.Count > 1)
			{
				if(m_NumSelectedGrains > 0)
				{
					int selectedGrainIndex = GetSelectedGrain(0);

					for (int loop = 0; loop < m_NumSelectedGrains; loop++)
					{
						if (selectedGrainIndex >= 0)
						{
							if (selectedGrainIndex + 1 < m_GrainData.Count)
							{
								g.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb(96, 255, 0, 0)),
									(m_GrainData[selectedGrainIndex] - m_Xoffset) * m_Xscale,
									0,
									(m_GrainData[selectedGrainIndex + 1] - m_GrainData[selectedGrainIndex]) * m_Xscale,
									visBounds.Height);
							}
						}

						selectedGrainIndex = GetNextSelectedGrain(selectedGrainIndex + 1);
					}
					
					selectedGrainIndex = GetSelectedGrain(m_SelectedGrainIndex);

					if (selectedGrainIndex >= 0 &&
						selectedGrainIndex + 1 < m_GrainData.Count)
					{
						if (IsPlayingGranular() &&
							selectedGrainIndex + 1 < m_GrainData.Count)
						{
							g.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb(140, 255, 0, 0)),
								(m_GrainData[selectedGrainIndex] - m_Xoffset) * m_Xscale,
								0,
								(m_GrainData[selectedGrainIndex + 1] - m_GrainData[selectedGrainIndex]) * m_Xscale,
								visBounds.Height);
						}

						Font arial = new Font(new FontFamily("Arial"), 10, FontStyle.Regular);
						SolidBrush textBrush = new SolidBrush(Color.Black);

						g.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb(210, 255, 255, 255)),
								0,
								0,
								420.0f,
								17.0f);

						PlaybackScreen parentScreen = m_Owner as PlaybackScreen;
						float pitch = parentScreen.m_GrainGenerator.FindNearestPitch(m_GrainData[selectedGrainIndex]);
						float rpm = pitch * 60 * 2.0f;
						string rpmString = rpm.ToString() + " RPM";

						if (!parentScreen.m_GrainGenerator.IsPitchDataLoaded())
						{
							rpmString = "";
						}

						float grainSamples = m_GrainData[selectedGrainIndex + 1] - m_GrainData[selectedGrainIndex];
						float milliseconds = grainSamples / m_WaveFile.Format.SampleRate;
						milliseconds *= 1000;

						g.DrawString("Grain " + selectedGrainIndex + "/" + m_GrainData.Count + ": " + grainSamples.ToString() + " samples " + String.Format("{0:0.###}", milliseconds) + "ms " + rpmString, arial, textBrush, new PointF(0.0f, 0.0f));
					}
				}
			}

			// draw x axis
			g.DrawLine(p, 0, visBounds.Height / 2, visBounds.Width, visBounds.Height / 2);

			// draw cursor position
			g.DrawLine(Pens.Black,
					   (m_PlaybackCursor - m_Xoffset) * m_Xscale,
					   0,
					   (m_PlaybackCursor - m_Xoffset) * m_Xscale,
					   visBounds.Height);

			// draw a line to show the end of the wave
			g.DrawLine(p, lastx, 0, lastx, visBounds.Height);

			var zoom = (int)(m_Xzoom * 100);
		}

		/// <summary>
		/// Zoom in on the wave view
		/// </summary>
		public void ZoomIn()
		{
			m_Xzoom *= 1.1f;
			RefocusOnMousePos();
		}

		/// <summary>
		/// Zoom out on the wave view
		/// </summary>
		public void ZoomOut()
		{
			if (m_Xzoom > 1.0f)
			{
				m_Xzoom *= 0.9f;

				if(m_Xzoom < 1.0f )
				{
					m_Xzoom = 1.0f;
				}

				RefocusOnMousePos();
			}
		}

		/// <summary>
		/// Refocus on where the mouse currently is
		/// </summary>
		public void RefocusOnMousePos()
		{
			if(m_WaveFile != null)
			{
				int samplesInWindow = (int)(m_WaveFile.Data.NumSamples / m_Xzoom);
				float zoomTarget = m_LastMousePoint.X / m_WindowWidth;

				// This is a bit rubbish - zooms to centre - should really zoom to the mouse cursor pos
				float zoomTargetSamples = m_Xoffset + (samplesInWindow * zoomTarget) - (samplesInWindow / 2);
				m_Xoffset = (int)(zoomTargetSamples);

				if (m_Xoffset + samplesInWindow > m_WaveFile.Data.NumSamples)
				{
					m_Xoffset = (int)(m_WaveFile.Data.NumSamples - samplesInWindow);
				}

				if (m_Xoffset < 0)
				{
					m_Xoffset = 0;
				}
			}
		}

		/// <summary>
		/// Callback when the cursor position is changed
		/// </summary>
		/// <param name="xCoord"></param>
		/// <param name="groupSelect"></param>
		public void AdjustCursor(int xCoord, bool groupSelect, bool undoSelect)
		{
			if(m_WaveFile != null)
			{
				int samplesInWindow = (int)(m_WaveFile.Data.NumSamples / m_Xzoom);
				float fractionSelected = xCoord / m_WindowWidth;
				int newGrainSelectionCursor = (int)(m_Xoffset + (samplesInWindow * fractionSelected));

				int minSelection = Math.Min(newGrainSelectionCursor, m_GrainSelectionCursor);
				int maxSelection = Math.Max(newGrainSelectionCursor, m_GrainSelectionCursor);

				for (int loop = 0; loop < m_GrainData.Count; loop++)
				{
					if (!groupSelect && !undoSelect)
					{
						if (m_GrainData[loop] > newGrainSelectionCursor &&
							loop > 0)
						{
							ResetSelectedGrains();
							SelectGrain(loop - 1);
							m_SelectedGrainIndex = 0;
							break;
						}
					}
					else if (undoSelect)
					{
						if (m_GrainData[loop] > newGrainSelectionCursor &&
							   loop > 0)
						{
							if (m_SelectedGrains[loop - 1])
							{
								UnSelectGrain(loop - 1);
							}
							else
							{
								SelectGrain(loop - 1);
							}
							
							break;
						}
					}
					else if(groupSelect)
					{
						if(m_GrainData[loop] > minSelection &&
						   m_GrainData[loop] < maxSelection)
						{
							if (newGrainSelectionCursor < m_GrainSelectionCursor)
							{
								if (loop > 0)
								{
									SelectGrain(loop - 1);
								}
							}
							else
							{
								SelectGrain(loop);
							}
						}
					}
				}

				m_GrainSelectionCursor = newGrainSelectionCursor;
			}
		}

		/// <summary>
		/// Scroll the view from side to side
		/// </summary>
		/// <param name="distance"></param>
		public void AdjustXOffset(float distance)
		{
			int samplesInWindow = (int)(m_WaveFile.Data.NumSamples / m_Xzoom);
			float fractionTravelled = distance / m_WindowWidth;
			m_Xoffset -= ((int)(samplesInWindow * fractionTravelled) * 5);

			if(m_Xoffset + samplesInWindow > m_WaveFile.Data.NumSamples)
			{
				m_Xoffset = (int)(m_WaveFile.Data.NumSamples - samplesInWindow);
			}

			if (m_Xoffset < 0)
			{
				m_Xoffset = 0;
			}
		}

		/// <summary>
		/// Refresh the cursor position according to where the playback point is
		/// </summary>
		public void UpdateCursor()
		{
			int playPosition;
			int writePosition;
			m_Buffer.GetCurrentPosition(out playPosition, out writePosition);
			m_PlaybackCursor = BytesToSampleIndex(playPosition);
		}

		/// <summary>
		/// Reduce the number of selected grains by chopping out every Xth one
		/// </summary>
		/// <param name="stepRate"></param>
		public void ReduceGrainSelection(int grainsToRemove, int stepRate)
		{
			for(int loop = 0; loop < m_SelectedGrains.Length; loop+= stepRate)
			{
				for (int grainRemoveLoop = 0; grainRemoveLoop < grainsToRemove && loop < m_SelectedGrains.Length; grainRemoveLoop++, loop++)
				{
					UnSelectGrain(loop);
				}
			}

			m_SelectedGrainIndex = 0;
		}

		/// <summary>
		/// Select everything
		/// </summary>
		public void SelectAll()
		{
			m_SelectedGrainIndex = 0;
			m_NumSelectedGrains = 0;

			for(int loop = 0; loop < m_SelectedGrains.Length; loop++)
			{
				m_SelectedGrains[loop] = true;
				m_NumSelectedGrains++;
			}
		}

		/// <summary>
		/// Load new grain data
		/// </summary>
		/// <param name="fileName"></param>
		public void LoadGrainData(string fileName)
		{
			m_GrainData.Clear();

			if(fileName.EndsWith("grn"))
			{
				LoadGrainDataText(fileName);
			}
			else
			{
				LoadGrainDataExcel(fileName);
			}

			m_SelectedGrains = new bool[m_GrainData.Count];
			SelectGrain(0);
		}

		void ResetSelectedGrains()
		{
			if (m_SelectedGrains != null)
			{
				for (int loop = 0; loop < m_SelectedGrains.Length; loop++)
				{
					m_SelectedGrains[loop] = false;
				}
			}

			m_NumSelectedGrains = 0;
		}

		void SelectGrain(int index)
		{
			if(!m_SelectedGrains[index])
			{
				m_SelectedGrains[index] = true;
				m_NumSelectedGrains++;
			}
		}

		void UnSelectGrain(int index)
		{
			if (m_SelectedGrains[index])
			{
				m_SelectedGrains[index] = false;
				m_NumSelectedGrains--;
			}
		}

		int GetSelectedGrain(int index)
		{
			int numFound = 0;

			for(int loop = 0; loop < m_GrainData.Count; loop++)
			{
				if(m_SelectedGrains[loop])
				{
					if(numFound == index)
					{
						return loop;
					}

					numFound++;
				}
			}

			return -1;
		}

		int GetNextSelectedGrain(int startIndex)
		{
			for (int loop = startIndex; loop < m_GrainData.Count; loop++)
			{
				if (m_SelectedGrains[loop])
				{
					return loop;
				}
			}

			return -1;
		}

		public void SetGrainRepetition(int repetition)
		{
			m_GrainRepetition = repetition;
		}

		/// <summary>
		/// Load grain data from a raw text file
		/// </summary>
		/// <param name="fileName"></param>
		void LoadGrainDataText(string fileName)
		{
			System.IO.StreamReader textReader = new System.IO.StreamReader(fileName);
			string line;

			while ((line = textReader.ReadLine()) != null)
			{
				m_GrainData.Add(Convert.ToInt32(line));
			}

			textReader.Close();
		}

		/// <summary>
		/// Use this to load grain data from an excel file (ie. MATLAB generated) 
		/// </summary>
		/// <param name="fileName"></param>
		public void LoadGrainDataExcel(string fileName)
		{
			Excel.Application xlApp;
			Excel.Workbook xlWorkBook;
			Excel.Worksheet xlWorkSheet;
			Excel.Range range;

			string str;
			int rCnt = 0;
			int cCnt = 0;

			xlApp = new Excel.ApplicationClass();
			xlWorkBook = xlApp.Workbooks.Open(fileName, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
			xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

			range = xlWorkSheet.UsedRange;

			for (rCnt = 1; rCnt <= range.Rows.Count; rCnt++)
			{
				for (cCnt = 1; cCnt <= range.Columns.Count; cCnt++)
				{
					str = (string)(range.Cells[rCnt, cCnt] as Excel.Range).Value2;
					m_GrainData.Add((int)(float.Parse(str)));
				}
			}

			xlWorkBook.Close(true, null, null);
			xlApp.Quit();

			releaseObject(xlWorkSheet);
			releaseObject(xlWorkBook);
			releaseObject(xlApp);
		}

		/// <summary>
		/// Save grain data
		/// </summary>
		/// <param name="fileName"></param>
		public void SaveGrainData(string fileName)
		{
			System.IO.StreamWriter fileStream = System.IO.File.CreateText(fileName);

			for(int loop = 0; loop < m_GrainData.Count; loop++)
			{
				fileStream.WriteLine(m_GrainData[loop]);
			}

			fileStream.Close();
		}

		/// <summary>
		/// Helper function for releasing excel objects
		/// </summary>
		/// <param name="obj"></param>
		private void releaseObject(object obj)
		{
			System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
			obj = null;
		} 
	}
}
