﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;

using Microsoft.DirectX.DirectSound;
using Excel = Microsoft.Office.Interop.Excel;

using Wavelib;

namespace GranularPlayback
{
	public class GrainPlayer
	{
		public float m_GlobalVolumeScale = 1.0f;
		public bwWaveFile m_WaveFile;

		private readonly Device m_Device;
		private System.IO.MemoryStream m_waveDataStream;
		private Microsoft.DirectX.DirectSound.Buffer m_GrainBuffer;
		private int m_GrainBufferBytes;
		private System.Timers.Timer m_GrainFeedTimer;
		private int m_NextGrainWrite;
		private System.Threading.Mutex m_Mutex;

		private float m_Xscale;
		private float m_Xzoom = 1.0f;
		private float m_Yscale;
		private float m_Yzoom = 1.0f;
		private int m_PlaybackCursor = 0;
		private int m_Xoffset = 0;
		private float m_WindowWidth = 0;

		private int m_GrainSelectionCursor = 0;
		private double m_MaxLatencyMs = 0.05;
		private int m_GrainRepetition = 1;

		public float m_GlobalPitchScale = 1.0f;
		public bool m_DrawGrainBoundaries = true;
		public bool m_DrawLoopPositions = true;
		public bool m_DrawSamplePositions = false;

		public int m_ChangeRateForMaxLoops = 0;
		public int m_ChangeRateForMaxGrains = 0;

		float m_LoopVolumeScale = 0.0f;
		public float m_SimulatorPitchScale = 1.0f;

		public Point m_LastMousePoint;
		public bool m_ViewBeingDragged = false;
		public bool m_GrainCursorBeingDragged = false;
		public bool m_MouseOverView = false;

		double m_CurrentGrainPlaybackFraction = 0.0;

		Control m_Owner;
		public Random m_RandomGenerator;

		public enum GrainPlaybackOrder
		{
			PlaybackOrderSequential,
			PlaybackOrderRandom,
			PlaybackOrderWalk,
			PlaybackOrderReverse,
			PlaybackOrderMixNMatch,
		};

		public enum audPlaybackState
		{
			PlaybackStatePlaying,
			PlaybackStatePaused,
		}

		public enum GrainPreviewStyle
		{
			PreviewStyleStandard,
			PreviewStyleFlattenedToAverage,
			PreviewStyleFlattenedToRPM,
		}

		public enum SimulatorPlaybackStyle
		{
			PlaybackStyleLoopsAndGrains,
			PlaybackStyleLoopsOnly,
			PlaybackStyleGrainsOnly,
		}

		public enum SimulatorGrainSelectionStyle
		{
			PlaybackStyleFractionBetweenClosestLoops,
			PlaybackStyleFractionThroughSweep,
			PlaybackStyleClosestPitchMatch,
		}

		public enum SampleInterpolationMethod
		{
			SampleInterpolationLinear,
			SampleInterpolationCosine,
			SampleInterpolationCubic,
			SampleInterpolationSinc,
		}

		public SimulatorGrainSelectionStyle m_SimulatorGrainSelectionStyle = SimulatorGrainSelectionStyle.PlaybackStyleFractionBetweenClosestLoops;
		public SimulatorPlaybackStyle m_SimulatorPlaybackStyle = SimulatorPlaybackStyle.PlaybackStyleLoopsAndGrains;
		public GrainPreviewStyle m_GrainPreviewStyle = GrainPreviewStyle.PreviewStyleStandard;
		public SampleInterpolationMethod m_SampleInterpolationMethod = SampleInterpolationMethod.SampleInterpolationLinear;
		public audPlaybackState m_PlaybackState;
		public List<int> m_GrainData;

		private audGranularSubmix m_debugSubmix;
		public List<audGranularSubmix> m_SynchronisedLoops;
		public audGranularSubmix m_GranularSubmix;

		public int m_CrossFadePercentage = 0;

		GrainPlaybackOrder m_GrainPlaybackOrder = GrainPlaybackOrder.PlaybackOrderSequential;

		public float m_SimulatedRPMProportion;
		public int m_SimulatedRPM;
		public int m_PrevSimulatedRPM;
		public float m_RPMChangeRate;
		public bool m_SimulatorActive = false;
		public float m_FlattenedRPMValue = 5000;

		/// <summary>
		/// Helper lerp function - same as Rage
		/// </summary>
		/// <param name="t"></param>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns></returns>
		public float Lerp(float t, float a, float b)
		{
			return (a+(b-a)*t);
		}

		/// <summary>
		/// Wave player constructor
		/// </summary>
		/// <param name="owner"></param>
		public GrainPlayer(Control owner)
        {
			m_Owner = owner;
			m_Device = new Device();
			m_Device.SetCooperativeLevel(owner, CooperativeLevel.Priority);
			m_GrainData = new List<int>();
			m_SynchronisedLoops = new List<audGranularSubmix>();
			m_RandomGenerator = new Random();
			m_Mutex = new System.Threading.Mutex();
			m_PlaybackState = audPlaybackState.PlaybackStatePaused;
        }

		/// <summary>
		/// Kick off the granular playback of the file
		/// </summary>
		public void PlayGranular()
		{
			if(!IsGrainBufferNull())
			{
				if(m_GrainData.Count == 0)
				{
					MessageBox.Show("No grain data has been loaded!", "Cannot play grain");
				}
				else
				{
					// start halfway through the first grain
					m_PlaybackState = audPlaybackState.PlaybackStatePlaying;
					FeedGranular(m_GrainBufferBytes);
				}
			}
		}

		/// <summary>
		/// Reset all the grain data
		/// </summary>
		public void ClearGrainData()
		{
			m_GrainData.Clear();
			m_SynchronisedLoops.Clear();
		}

		/// <summary>
		/// Add a new grain start point
		/// </summary>
		/// <param name="grainPos"></param>
		public void AddGrainData(int grainPos)
		{
			m_GrainData.Add(grainPos);
			m_debugSubmix = new audGranularSubmix(this, null, m_GrainData.Count, m_GrainPlaybackOrder, audGranularSubmix.GranularSubmixType.SubmixTypeSynchronisedLoop);
			m_GranularSubmix = new audGranularSubmix(this, null, m_GrainData.Count, GrainPlaybackOrder.PlaybackOrderRandom, audGranularSubmix.GranularSubmixType.SubmixTypePureGranular);
		}

		/// <summary>
		/// Get the final grain start position in the sequence
		/// </summary>
		/// <returns></returns>
		public int GetFinalGrainPos()
		{
			if (m_GrainData.Count > 0)
			{
				return m_GrainData.Last();
			}

			return 1;
		}

		/// <summary>
		/// Get the number of grains
		/// </summary>
		/// <returns></returns>
		public int GetNumGrains()
		{
			return m_GrainData.Count;
		}

		/// <summary>
		/// Get the grain at the given position
		/// </summary>
		/// <param name="index"></param>
		/// <returns></returns>
		public int GetGrainPos(int index)
		{
			return m_GrainData[index];
		}

		/// <summary>
		/// Remove all the unselected grains
		/// </summary>
		public void RemoveGrains(bool selected)
		{
			List<float> rawData = new List<float>((int)m_WaveFile.Data.NumSamples);

			for (int loop = 0; loop < m_WaveFile.Data.NumSamples; loop++ )
			{
				rawData.Add(m_WaveFile.Data.ChannelData[0, loop]);
			}
				
			for(int grainLoop = 0; grainLoop < m_GrainData.Count - 1; )
			{
				bool grainIsValid = m_debugSubmix.m_SelectedGrains[grainLoop] == !selected;

				if(!grainIsValid)
				{
					int dataToTrim = m_GrainData[grainLoop + 1] - m_GrainData[grainLoop];

					if (m_GrainData[grainLoop] + dataToTrim < rawData.Count)
					{
						rawData.RemoveRange(m_GrainData[grainLoop], dataToTrim);
					}

					m_GrainData.RemoveAt(grainLoop);

					for (int syncLoop = 0; syncLoop < m_SynchronisedLoops.Count; syncLoop++)
					{
						m_SynchronisedLoops[syncLoop].OnGrainRemoved(grainLoop);
					}

					m_debugSubmix.OnGrainRemoved(grainLoop);

					for (int remainingGrains = grainLoop; remainingGrains < m_GrainData.Count; remainingGrains++)
					{
						m_GrainData[remainingGrains] -= dataToTrim;
					}
				}
				else
				{
					grainLoop++;
				}
			}

			byte[] byteData = new byte[rawData.Count * sizeof(Int16)];
			int byteIndex = 0;

			for(int loop = 0; loop < rawData.Count; loop++)
			{
				byte[] sampleAsByte = BitConverter.GetBytes((Int16)(rawData[loop] * Int16.MaxValue));

				for(int byteLoop = 0; byteLoop < sampleAsByte.Length; byteLoop++)
				{
					byteData[byteIndex] = sampleAsByte[byteLoop];
					byteIndex++;
				}
			}

			m_WaveFile.Data.ChannelData = new float[1, rawData.Count];

			for (int loop = 0; loop < rawData.Count; loop++)
			{
				m_WaveFile.Data.ChannelData[0, loop] = rawData[loop];
			}

			m_WaveFile.Data.RawData = byteData;
			m_WaveFile.Data.NumSamples = (uint)rawData.Count;
			m_WaveFile.Save("grainreduced.wav");
			SaveGrainData("grainreduced.grn");
			SaveLoopData("grainreduced.lpd");
		}

		/// <summary>
		/// Get the grain index for the given sample
		/// </summary>
		/// <returns></returns>
		public int GetGrainIndexForSample(int sampleIndex)
		{
			int prevGrainData = -1;
			for(int loop = 0; loop < m_GrainData.Count; loop++)
			{
				if(prevGrainData < sampleIndex &&
					m_GrainData[loop] >= sampleIndex)
				{
					return loop;
				}

				prevGrainData = m_GrainData[loop];
			}

			return -1;
		}

		/// <summary>
		/// Get the grain index for the given sample
		/// </summary>
		/// <returns></returns>
		public int GetClosestGrainToRPM(float rpm)
		{
			if (m_SimulatorGrainSelectionStyle == SimulatorGrainSelectionStyle.PlaybackStyleFractionBetweenClosestLoops && m_SynchronisedLoops.Count > 0)
			{
				int nearestLoopAbove = -1;
				float nearestLoopAboveVolumeScale = 1.0f;
				int nearestLoopBelow = -1;
				float nearestLoopBelowVolumeScale = 1.0f;

				CalculateNearestLoops(rpm, out nearestLoopAbove, out nearestLoopBelow, out nearestLoopAboveVolumeScale, out nearestLoopBelowVolumeScale);

				if (nearestLoopAbove >= 0 && nearestLoopBelow >= 0)
				{
					float fractionBetween = (rpm - m_SynchronisedLoops[nearestLoopBelow].m_AverageRPM) / (m_SynchronisedLoops[nearestLoopAbove].m_AverageRPM - m_SynchronisedLoops[nearestLoopBelow].m_AverageRPM);
					return (int)(m_SynchronisedLoops[nearestLoopBelow].m_AverageGrainIndex + ((m_SynchronisedLoops[nearestLoopAbove].m_AverageGrainIndex - m_SynchronisedLoops[nearestLoopBelow].m_AverageGrainIndex) * fractionBetween));
				}
				else if (nearestLoopAbove >= 0)
				{
					return m_SynchronisedLoops[nearestLoopAbove].m_AverageGrainIndex;
				}
				else if (nearestLoopBelow >= 0)
				{
					return m_SynchronisedLoops[nearestLoopBelow].m_AverageGrainIndex;
				}
			}
			else if (m_SimulatorGrainSelectionStyle == SimulatorGrainSelectionStyle.PlaybackStyleClosestPitchMatch)
			{
				float minDiff = -1.0f;
				int minIndex = 0;

				for (int grainIndex = 0; grainIndex < m_GrainData.Count - 1; grainIndex++)
				{
					int grainLengthSamples = m_GrainData[grainIndex + 1] - m_GrainData[grainIndex];
					float grainLengthSeconds = grainLengthSamples / (float)m_WaveFile.Format.SampleRate;
					float hertz = 1 / grainLengthSeconds;
					float grainRpm = hertz * 60.0f * 2.0f;

					float rpmDiff = Math.Abs(rpm - grainRpm);

					if (minDiff < 0.0f || rpmDiff < minDiff)
					{
						minDiff = rpmDiff;
						minIndex = grainIndex;
					}
				}

				return minIndex;
			}
			else if(m_SimulatorGrainSelectionStyle == SimulatorGrainSelectionStyle.PlaybackStyleFractionThroughSweep ||
					(m_SimulatorGrainSelectionStyle == SimulatorGrainSelectionStyle.PlaybackStyleFractionBetweenClosestLoops && m_SynchronisedLoops.Count == 0))
			{
				int minRPM = 0;
				int maxRPM = 0;
				CalculateMinMaxRPM(out minRPM, out maxRPM);

				float fraction = (rpm - minRPM) / (float)(maxRPM - minRPM);

				if(fraction < 0.0f) fraction = 0.0f;
				if(fraction > 1.0f) fraction = 1.0f;

				return (int)((m_GrainData.Count - 1) * fraction);
			}

			return 0;
		}

		/// <summary>
		/// Timer callback when we need to buffer more granular data. This code has been ported
		/// from the Rage implementation of GrainPlayer.cpp, so as to ensure parity between the two
		/// </summary>
		/// <param name="bufferBytes"></param>
		void FeedGranular(int bufferBytes)
		{
			if (bufferBytes <= 0)
			{
				return;
			}

			m_Mutex.WaitOne();

			try
			{
				int mixSamples = BytesToSampleIndex(bufferBytes);

				// Temp - turn off regular grain player during simulator
				float[] destBuffer = new float[mixSamples];

				if(m_SimulatorActive)
				{
					int nearestLoopAbove = -1;
					float nearestLoopAboveVolumeScale = 1.0f;
					int nearestLoopBelow = -1;
					float nearestLoopBelowVolumeScale = 1.0f;

					float currentRateHz = m_SimulatedRPM / 60.0f;
					currentRateHz /= 2.0f;
					currentRateHz *= m_SimulatorPitchScale;

					int currentDesiredSamplesPerGranule = (int)((float)m_WaveFile.Format.SampleRate / currentRateHz);
					double currentGranuleFractionPerBuffer = mixSamples / (double)currentDesiredSamplesPerGranule;
					double currentGranuleFractionPerSample = currentGranuleFractionPerBuffer / mixSamples;

					float prevRateHz = m_PrevSimulatedRPM / 60.0f;
					prevRateHz /= 2.0f;
					prevRateHz *= m_SimulatorPitchScale;

					int prevDesiredSamplesPerGranule = (int)((float)m_WaveFile.Format.SampleRate / prevRateHz);
					double prevGranuleFractionPerBuffer = mixSamples / (double)prevDesiredSamplesPerGranule;
					double prevGranuleFractionPerSample = prevGranuleFractionPerBuffer / mixSamples;
					
					float stepRateRPM = (m_SimulatedRPM - m_PrevSimulatedRPM) / (float)mixSamples;
					double granuleFractionStepRate = (currentGranuleFractionPerSample - prevGranuleFractionPerSample) / mixSamples;

					float desiredLoopVolumeScale = 1.0f - (Math.Abs(m_RPMChangeRate) - m_ChangeRateForMaxLoops) / (float)(m_ChangeRateForMaxGrains - m_ChangeRateForMaxLoops);

					// Add in a bit of smoothing from loop - grain
					m_LoopVolumeScale += (desiredLoopVolumeScale - m_LoopVolumeScale) * 0.1f;
					float grainVolumeScale = 1.0f - m_LoopVolumeScale;

					if (m_SimulatorPlaybackStyle == SimulatorPlaybackStyle.PlaybackStyleLoopsOnly)
					{
						m_LoopVolumeScale = 1.0f;
						grainVolumeScale = 0.0f;
					}
					else if (m_SimulatorPlaybackStyle == SimulatorPlaybackStyle.PlaybackStyleGrainsOnly)
					{
						m_LoopVolumeScale = 0.0f;
						grainVolumeScale = 1.0f;
					}

					if (m_LoopVolumeScale > 1.0f)
					{
						m_LoopVolumeScale = 1.0f;
					}
					else if (m_LoopVolumeScale < 0.0f)
					{
						m_LoopVolumeScale = 0.0f;
					}

					if (grainVolumeScale > 1.0f)
					{
						grainVolumeScale = 1.0f;
					}
					else if (grainVolumeScale < 0.0f)
					{
						grainVolumeScale = 0.0f;
					}

					CalculateNearestLoops(m_SimulatedRPM, out nearestLoopAbove, out nearestLoopBelow, out nearestLoopAboveVolumeScale, out nearestLoopBelowVolumeScale);

					for (int loop = 0; loop < m_SynchronisedLoops.Count; loop++)
					{
						float volumeScale = 0.0f;

						if (loop == nearestLoopAbove)
						{
							volumeScale = nearestLoopAboveVolumeScale;
						}
						else if(loop == nearestLoopBelow)
						{
							volumeScale = nearestLoopBelowVolumeScale;
						}

						if(volumeScale > 0.0f)
						{
							float[] loopOutput = m_SynchronisedLoops[loop].FeedSynchronised(bufferBytes, m_LoopVolumeScale * volumeScale * m_GlobalVolumeScale, m_CurrentGrainPlaybackFraction, prevGranuleFractionPerSample, granuleFractionStepRate, m_PrevSimulatedRPM, stepRateRPM);

							for (int sample = 0; sample < destBuffer.Length; sample++)
							{
								destBuffer[sample] += loopOutput[sample];
							}
						}
						else
						{
							m_SynchronisedLoops[loop].m_LastVolumeScale = 0.0f;
						}
					}

					float[] granularOutput = m_GranularSubmix.FeedSynchronised(bufferBytes, grainVolumeScale * m_GlobalVolumeScale, m_CurrentGrainPlaybackFraction, prevGranuleFractionPerSample, granuleFractionStepRate, m_PrevSimulatedRPM, stepRateRPM);

					for (int sample = 0; sample < destBuffer.Length; sample++)
					{
						destBuffer[sample] += granularOutput[sample];

						if (destBuffer[sample] > 1.0f)
						{
							destBuffer[sample] = 1.0f;
						}
						else if (destBuffer[sample] < -1.0f)
						{
							destBuffer[sample] = -1.0f;
						}

						m_CurrentGrainPlaybackFraction += prevGranuleFractionPerSample;
						prevGranuleFractionPerSample += granuleFractionStepRate;

						if(m_CurrentGrainPlaybackFraction >= 1.0)
						{
							m_CurrentGrainPlaybackFraction -= 1.0;
						}
					}
				}
				else
				{
					if (m_GrainPreviewStyle != GrainPreviewStyle.PreviewStyleStandard)
					{
						float currentRateHz = 0.0f;

						if (m_GrainPreviewStyle == GrainPreviewStyle.PreviewStyleFlattenedToRPM)
						{
							currentRateHz = m_FlattenedRPMValue / 60.0f;
						}
						else
						{
							m_debugSubmix.CalculateAverageRPM();
							currentRateHz = m_debugSubmix.m_AverageRPM / 60.0f;
						}

						currentRateHz /= 2.0f;
						int currentDesiredSamplesPerGranule = (int)((float)m_WaveFile.Format.SampleRate / currentRateHz);
						double currentGranuleFractionPerBuffer = mixSamples / (double)currentDesiredSamplesPerGranule;
						double currentGranuleFractionPerSample = currentGranuleFractionPerBuffer / mixSamples;

						destBuffer = m_debugSubmix.FeedSynchronised(bufferBytes, 1.0f * m_GlobalVolumeScale, m_CurrentGrainPlaybackFraction, currentGranuleFractionPerSample, 0.0f, m_debugSubmix.m_AverageRPM, 0.0f);

						for (int sample = 0; sample < destBuffer.Length; sample++)
						{
							m_CurrentGrainPlaybackFraction += currentGranuleFractionPerSample;

							if (m_CurrentGrainPlaybackFraction >= 1.0)
							{
								m_CurrentGrainPlaybackFraction -= 1.0;
							}
						}
					}
					else
					{
						destBuffer = m_debugSubmix.Feed(bufferBytes, 1.0f * m_GlobalVolumeScale, 0.0f, 0.0f);
					}
				}

				List<byte> finalData = new List<byte>();

				for (int loop = 0; loop < destBuffer.Length; loop++)
				{
					short sample = (short)(destBuffer[loop] * 32767);
					byte[] sampleData = BitConverter.GetBytes(sample);

					for (int bytes = 0; bytes < sampleData.Length; bytes++)
					{
						finalData.Add(sampleData[bytes]);
					}
				}

				m_GrainBuffer.Write(m_NextGrainWrite, finalData.ToArray(), LockFlag.None);
				m_NextGrainWrite += finalData.Count;

				if (m_NextGrainWrite >= m_GrainBufferBytes)
					m_NextGrainWrite -= m_GrainBufferBytes;

				m_PrevSimulatedRPM = m_SimulatedRPM;
			}
			catch (System.ArgumentNullException e)
			{
				m_Mutex.ReleaseMutex();
				return;
			}

			m_Mutex.ReleaseMutex();
		}

		/// <summary>
		/// Timer callback
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void GrainTimerElapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
			FeedGranular(GetPlayedSize());
		}

		/// <summary>
		/// Check how many bytes of data have been played since we last checked
		/// </summary>
		/// <returns></returns>
		private int GetPlayedSize()
		{
			int pos = m_GrainBuffer.PlayPosition;
			return pos < m_NextGrainWrite ? pos + m_GrainBufferBytes - m_NextGrainWrite : pos - m_NextGrainWrite;
		}

		/// <summary>
		/// Check if the granular playback buffer is valid
		/// </summary>
		/// <returns></returns>
		public bool IsGrainBufferNull()
		{
			return m_GrainBuffer == null;
		}

		/// <summary>
		/// Check if the granular playback buffer is playing
		/// </summary>
		/// <returns></returns>
		public bool IsPlayingGranular()
		{
			if (m_GrainBuffer != null &&
				m_PlaybackState == audPlaybackState.PlaybackStatePlaying)
			{
				return true;
			}

			return false;
		}

		/// <summary>
		/// Create a new loop using the marked up area
		/// </summary>
		public void CreateNewLoop()
		{
			audGranularSubmix loopData = new audGranularSubmix(this, m_debugSubmix.m_SelectedGrains, m_debugSubmix.m_NumSelectedGrains, m_GrainPlaybackOrder, audGranularSubmix.GranularSubmixType.SubmixTypeSynchronisedLoop);
			m_SynchronisedLoops.Add(loopData);
		}

		/// <summary>
		/// Toggle the exclusions on the selected grains
		/// </summary>
		public void DeleteLoop()
		{
			int selectedGrain = m_debugSubmix.GetSelectedGrain(0);

			for(int selectedGrainLoop = 0; selectedGrainLoop < m_debugSubmix.m_NumSelectedGrains; selectedGrainLoop++)
			{
				if(selectedGrain >= 0)
				{
					for (int loop = 0; loop < m_SynchronisedLoops.Count; loop++)
					{
						if(m_SynchronisedLoops[loop].m_SelectedGrains[selectedGrain])
						{
							m_SynchronisedLoops.RemoveAt(loop);
							loop--;
						}
					}

					selectedGrain = m_debugSubmix.GetNextSelectedGrain(selectedGrain);
				}
			}
		}

		/// <summary>
		/// Set the hash name on the selected loop
		/// </summary>
		public void SetSubmixTypeHash(uint nameHash)
		{
			int selectedGrain = m_debugSubmix.GetSelectedGrain(0);

			for (int selectedGrainLoop = 0; selectedGrainLoop < m_debugSubmix.m_NumSelectedGrains; selectedGrainLoop++)
			{
				if (selectedGrain >= 0)
				{
					for (int loop = 0; loop < m_SynchronisedLoops.Count; loop++)
					{
						if (m_SynchronisedLoops[loop].m_SelectedGrains[selectedGrain])
						{
							m_SynchronisedLoops[loop].SetSubmixTypeHash(nameHash);
						}
					}

					selectedGrain = m_debugSubmix.GetNextSelectedGrain(selectedGrain);
				}
			}
		}

		/// <summary>
		/// Clear any existing loops
		/// </summary>
		public void ClearAllLoops()
		{
			m_SynchronisedLoops.Clear();
		}

		/// <summary>
		/// Convert a sample index to a byte index
		/// </summary>
		/// <param name="samples"></param>
		/// <returns></returns>
		public int SamplesToByteIndex(int samples)
		{
			return samples * ((m_WaveFile.Format.SignificantBitsPerSample / 8) * m_WaveFile.Format.NumChannels);
		}

		/// <summary>
		/// Convert a byte index to a sample index
		/// </summary>
		/// <param name="bytes"></param>
		/// <returns></returns>
		public int BytesToSampleIndex(int bytes)
		{
			return bytes / ((m_WaveFile.Format.SignificantBitsPerSample / 8) * m_WaveFile.Format.NumChannels);
		}

		/// <summary>
		/// Reset all the playback variables
		/// </summary>
		public void Reset()
		{
			ResetSelectedGrains();
		}

		/// <summary>
		/// Read the sample at the given sample index
		/// </summary>
		/// <param name="sampleIndex"></param>
		/// <returns></returns>
		public float ReadSampleFromSampleIndex(int sampleIndex)
		{
			if (sampleIndex >= 0 && 
				sampleIndex < m_WaveFile.Data.ChannelData.Length)
			{
				return m_WaveFile.Data.ChannelData[0, sampleIndex];
			}

			return 0;
		}

		/// <summary>
		/// Read the sample at the given sample index
		/// </summary>
		/// <param name="sampleIndex"></param>
		/// <returns></returns>
		public float ReadSampleFromSampleIndex(double sampleIndex)
		{
			if(m_SampleInterpolationMethod == SampleInterpolationMethod.SampleInterpolationLinear)
			{
				float prevSample = ReadSampleFromSampleIndex((int)Math.Floor(sampleIndex));
				float nextSample = ReadSampleFromSampleIndex((int)Math.Ceiling(sampleIndex));
				return (float)(prevSample + ((nextSample - prevSample) * (sampleIndex - Math.Floor(sampleIndex))));
			}
			else if(m_SampleInterpolationMethod == SampleInterpolationMethod.SampleInterpolationCosine)
			{
				float prevSample = ReadSampleFromSampleIndex((int)Math.Floor(sampleIndex));
				float nextSample = ReadSampleFromSampleIndex((int)Math.Ceiling(sampleIndex));
				double mu = sampleIndex - Math.Floor(sampleIndex);

				double mu2 = (1 - Math.Cos((mu * Math.PI))) / 2;
				return((float)(prevSample*(1-mu2)+nextSample*mu2));
			}
			else if(m_SampleInterpolationMethod == SampleInterpolationMethod.SampleInterpolationCubic)
			{
				float prevPrevSample = ReadSampleFromSampleIndex((int)Math.Floor(sampleIndex) - 1);
				float prevSample = ReadSampleFromSampleIndex((int)Math.Floor(sampleIndex));
				float nextSample = ReadSampleFromSampleIndex((int)Math.Ceiling(sampleIndex));
				float nextNextSample = ReadSampleFromSampleIndex((int)Math.Ceiling(sampleIndex) + 1);
				double mu = sampleIndex - Math.Floor(sampleIndex);

				double mu2 = mu * mu;
				double a0 = nextNextSample - nextSample - prevPrevSample + prevSample;
				double a1 = prevPrevSample - prevSample - a0;
				double a2 = nextSample - prevPrevSample;
				double a3 = prevSample;

				return (float)(a0 * mu * mu2 + a1 * mu2 + a2 * mu + a3);
			}
			else
			{
				int p = (int)sampleIndex;

				double total = 0.0;
				int window = 7;
				float oneOverWindow = 1.0f / window;
				float multiplierToGetPowerFactor = 2.6f * oneOverWindow; // windowing is 2.6^(-1) at 40% of the window

				int minp = p - window;
				int maxp = p + window;
				if (minp < 0)
				{
					minp = 0;
				}
				if (maxp >= m_WaveFile.Data.ChannelData.Length)
				{
					maxp = m_WaveFile.Data.ChannelData.Length - 1;
				}

				for (int i = minp; i <= maxp; i++)
				{
					double offset = i - sampleIndex;
					double window_prop = Math.Abs(offset * multiplierToGetPowerFactor);

					double sinc = 1.0;
					if (offset != 0.0f)
					{
						double pi_offset = Math.PI * offset;
						sinc = Math.Sin(pi_offset) / pi_offset;
					}

					double windowing = Math.Pow(2.6, -window_prop * window_prop);

					total += sinc * windowing * m_WaveFile.Data.ChannelData[0, i];
				}

				return (float)(total);
			}
		}

		/// <summary>
		/// Pause audio generation playing
		/// </summary>
        public void Pause()
        {
			m_PlaybackState = audPlaybackState.PlaybackStatePaused;
        }

		/// <summary>
		/// Stop all audio from playing
		/// </summary>
		public void Kill()
		{
			if (!IsGrainBufferNull())
			{
				Pause();
				m_GrainFeedTimer.Enabled = false;

				m_Mutex.WaitOne();
				m_GrainBuffer.Stop();
				m_GrainBuffer.SetCurrentPosition(0);
				m_NextGrainWrite = 0;
				m_Mutex.ReleaseMutex();
			}
		}

		/// <summary>
		/// Set the order in which grains get played back
		/// </summary>
		/// <param name="index"></param>
		public void SetGrainPlaybackOrder(int index)
		{
			m_GrainPlaybackOrder = (GrainPlaybackOrder)index;
			m_debugSubmix.m_GrainPlaybackOrder = m_GrainPlaybackOrder;
		}

		/// <summary>
		/// Set the grain preview style
		/// </summary>
		/// <param name="index"></param>
		public void SetGrainPreviewStyle(int index)
		{
			m_GrainPreviewStyle = (GrainPreviewStyle)index;

		}

		/// <summary>
		/// Create a standard playback buffer
		/// </summary>
		/// <param name="filePath"></param>
        public void CreatePlaybackBuffer(string filePath)
        {
			m_WaveFile = new bwWaveFile(filePath);

			if(m_WaveFile.Format.NumChannels > 1)
			{
				MessageBox.Show("The selected file has " + m_WaveFile.Format.NumChannels + " channels. Only mono assets are supported", "Invalid Format");
				m_WaveFile = null;
				return;
			}

			if(m_WaveFile.Data == null)
			{
				MessageBox.Show("The selected file could not be loaded. It does not appear to contain valid audio data", "Unexpected Error");
				return;
			}

			if (m_waveDataStream != null)
			{
				m_waveDataStream.Dispose();
			}
			
			m_waveDataStream = new System.IO.MemoryStream(m_WaveFile.Data.RawData);

            var desc = new BufferDescription();
            var format = new WaveFormat();

            format.AverageBytesPerSecond = (int) m_WaveFile.Format.AverageBytesPerSecond;
            format.BitsPerSample = (short) m_WaveFile.Format.SignificantBitsPerSample;
            format.BlockAlign = (short) m_WaveFile.Format.BlockAlign;
            format.Channels = (short) m_WaveFile.Format.NumChannels;
            format.SamplesPerSecond = (int) m_WaveFile.Format.SampleRate;

            desc.Format = format;
            desc.BufferBytes = (int)(m_WaveFile.Data.NumSamples * (format.BitsPerSample / 8) * format.Channels);

            desc.CanGetCurrentPosition = true;
            desc.Control3D = false;
            desc.ControlEffects = false;
            desc.ControlFrequency = false;
            desc.ControlPan = false;
            desc.ControlPositionNotify = false;
            desc.ControlVolume = false;
            desc.DeferLocation = false;
            desc.GlobalFocus = true;
            desc.Guid3DAlgorithm = Guid.Empty;
            desc.PrimaryBuffer = false;

			desc.BufferBytes = format.AverageBytesPerSecond / 20;
			m_GrainBuffer = new Microsoft.DirectX.DirectSound.Buffer(desc, m_Device);
			m_GrainBufferBytes = m_GrainBuffer.Caps.BufferBytes;

			m_GrainFeedTimer = new System.Timers.Timer(m_MaxLatencyMs);
			m_GrainFeedTimer.Enabled = false;
			m_GrainFeedTimer.Elapsed += new System.Timers.ElapsedEventHandler(GrainTimerElapsed);

			// Just add one single mammoth grain so that we can preview the data without needing to load any proper grain data
			m_GrainData.Clear();
			m_SynchronisedLoops.Clear();
			m_GrainData.Add(0);
			m_GrainData.Add((int)m_WaveFile.Data.NumSamples);
			m_debugSubmix = new audGranularSubmix(this, null, 1, m_GrainPlaybackOrder, audGranularSubmix.GranularSubmixType.SubmixTypeSynchronisedLoop);
			m_GranularSubmix = new audGranularSubmix(this, null, 0, GrainPlaybackOrder.PlaybackOrderRandom, audGranularSubmix.GranularSubmixType.SubmixTypePureGranular);

			FeedGranular(m_GrainBufferBytes);
			m_GrainFeedTimer.Enabled = true;
			m_GrainBuffer.Play(0, BufferPlayFlags.Looping);
        }

		/// <summary>
		/// Update the wave image
		/// </summary>
		/// <param name="pea"></param>
		public void DrawWave(PaintEventArgs pea, bool highQuality)
		{
			if (!IsGrainBufferNull())
			{
				var grfx = pea.Graphics;
				var visBounds = grfx.VisibleClipBounds;
				m_WindowWidth = visBounds.Width;

				m_Yscale = 1.0f * m_Yzoom;
				// scale x so that the entire sample fits in the visbounds
				m_Xscale = (visBounds.Width / m_WaveFile.Data.NumSamples) * m_Xzoom;

				RenderWaveView(grfx, visBounds, highQuality);
			}
		}

		/// <summary>
		/// Update the axis for the wave image
		/// </summary>
		/// <param name="pea"></param>
		public void DrawAxis(PaintEventArgs pea)
		{
			var grfx = pea.Graphics;
			var visBounds = grfx.VisibleClipBounds;
			Font arial = new Font(new FontFamily("Arial"), 10, FontStyle.Regular);
			SolidBrush textBrush = new SolidBrush(Color.Black);

			float startSampleTime = 0.0f;
			float endSampleTime = 10.0f;

			if (!IsGrainBufferNull())
			{
				int startSampleIndex = (int)(m_Xoffset);
				int endSampleIndex = (int)(m_Xoffset + (int)(m_WaveFile.Data.NumSamples / m_Xzoom));

				startSampleTime = startSampleIndex / (float)m_WaveFile.Format.SampleRate;
				endSampleTime = endSampleIndex / (float)m_WaveFile.Format.SampleRate;
			}

			for (int loop = 0; loop < 10; loop++)
			{
				float xCoord = visBounds.Width / 10 * loop;
				float thisTime = startSampleTime + ((endSampleTime - startSampleTime)/10) * loop;

				string timeString = String.Format("{0:0.###}", thisTime);
				grfx.DrawString(timeString, arial, textBrush, new PointF(xCoord + 1, 3));
				grfx.DrawLine(Pens.Black, xCoord, visBounds.Height/2, xCoord, 0);
			}
		}

		/// <summary>
		/// Draw the wave
		/// </summary>
		/// <param name="g"></param>
		/// <param name="visBounds"></param>
		private void RenderWaveView(Graphics g, RectangleF visBounds, bool highQuality)
		{
			Int32 lastx = 0, lasty = 0;
			var gp = new GraphicsPath();
			var p = Pens.DarkBlue;

			g.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb(200, 200, 200)),
							0,
							0,
							(Int32)((m_WaveFile.Data.NumSamples - m_Xoffset) * m_Xscale),
							visBounds.Height);

			// Default step rate
			int stepRate = 30;

			if(!highQuality)
			{
				stepRate = 150;
			}

			float zoomPercentage = (m_Xzoom - 100)/100.0f;

			if (zoomPercentage > 1.0f)
				zoomPercentage = 1.0f;

			if (zoomPercentage < 0.0f)
				zoomPercentage = 0.0f;

			// As we zoom in more, increase the accuracy
			stepRate -= (int)((stepRate - 1) * zoomPercentage);

			for (var i = m_Xoffset; i < m_WaveFile.Data.NumSamples; i += stepRate)
			{
				float sample = m_WaveFile.Data.ChannelData[0, i];

				// scale from -1.0f -> 1.0f to 0 -> visBounds.Height
				sample *= -1.0f;
				sample += 1.0f;
				sample /= 2.0f;

				// now 0 -> 1.0f
				var y = (Int32)(sample * visBounds.Height);
				var x = i;

				x = (Int32)((x - m_Xoffset) * m_Xscale);

				y = (Int32)(y * m_Yscale);

				if(x > visBounds.Width &&
				   lastx > visBounds.Width)
				{
					break;
				}

				if (i >= 1)
				{
					gp.AddLine(lastx, lasty, x, y);
					//g.DrawLine(p, lastx, lasty, x, y);
				}

				if (m_DrawSamplePositions)
				{
					g.FillEllipse(new SolidBrush(Color.Black), x-2, y-2, 4, 4);	
				}

				lastx = x;
				lasty = y;
			}

			// draw path
			g.DrawPath(p, gp);

			if (m_DrawGrainBoundaries)
			{
				for (var grainLoop = 0; grainLoop < m_GrainData.Count; grainLoop++)
				{
					var x = (Int32)((m_GrainData[grainLoop] - m_Xoffset) * m_Xscale);

					if (x > visBounds.Width)
					{
						break;
					}

					g.DrawLine(Pens.HotPink, x, visBounds.Height, x, 0);
				}
			}

			if (m_DrawLoopPositions)
			{
				for(int loop = 0; loop < m_SynchronisedLoops.Count; loop++)
				{
					int selectedGrainIndex = m_SynchronisedLoops[loop].GetSelectedGrain(0);

					for (int grainLoop = 0; grainLoop < m_SynchronisedLoops[loop].m_NumSelectedGrains; grainLoop++)
					{
						if (selectedGrainIndex >= 0)
						{
							if (selectedGrainIndex + 1 < m_GrainData.Count)
							{
								g.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb(96, 0, 0, 255)),
									(m_GrainData[selectedGrainIndex] - m_Xoffset) * m_Xscale,
									0,
									(m_GrainData[selectedGrainIndex + 1] - m_GrainData[selectedGrainIndex]) * m_Xscale,
									visBounds.Height);
							}
						}

						selectedGrainIndex = m_SynchronisedLoops[loop].GetNextSelectedGrain(selectedGrainIndex);
					}

					if (m_SimulatorActive)
					{
						g.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb(140, 0, 0, 255)),
							(m_GrainData[m_SynchronisedLoops[loop].m_PrevSelectedGrain] - m_Xoffset) * m_Xscale,
							0,
							(m_GrainData[m_SynchronisedLoops[loop].m_PrevSelectedGrain + 1] - m_GrainData[m_SynchronisedLoops[loop].m_PrevSelectedGrain]) * m_Xscale,
							visBounds.Height);
					}

					/*
					for(int grain = 0; grain < m_SynchronisedLoops[loop].m_SelectedGrains.Length; grain++)
					{
						int selectedGrainIndex = m_SynchronisedLoops[loop].m_SelectedGrains[grain];

						if (selectedGrainIndex >= 0)
						{
							if (selectedGrainIndex + 1 < m_GrainData.Count)
							{
								g.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb(64, 0, 255, 0)),
									(m_GrainData[selectedGrainIndex] - m_Xoffset) * m_Xscale,
									0,
									(m_GrainData[selectedGrainIndex + 1] - m_GrainData[selectedGrainIndex]) * m_Xscale,
									visBounds.Height);
							}
						}
					}
					*/
				}
			}

			// draw cursor position
			if(m_GrainData.Count <= 2)
			{
				g.DrawLine(Pens.Black,
					   (m_PlaybackCursor - m_Xoffset) * m_Xscale,
					   0,
					   (m_PlaybackCursor - m_Xoffset) * m_Xscale,
					   visBounds.Height);
			}

			// draw selected grain
			if (m_GrainData.Count > 2)
			{
				if(m_debugSubmix.m_NumSelectedGrains > 0)
				{
					int selectedGrainIndex = GetSelectedGrain(0);

					if (!m_SimulatorActive)
					{
						for (int loop = 0; loop < m_debugSubmix.m_NumSelectedGrains; loop++)
						{
							if (selectedGrainIndex >= 0)
							{
								if (selectedGrainIndex + 1 < m_GrainData.Count)
								{
									g.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb(96, 0, 255, 0)),
										(m_GrainData[selectedGrainIndex] - m_Xoffset) * m_Xscale,
										0,
										(m_GrainData[selectedGrainIndex + 1] - m_GrainData[selectedGrainIndex]) * m_Xscale,
										visBounds.Height);
								}
							}

							selectedGrainIndex = GetNextSelectedGrain(selectedGrainIndex);
						}
					}

					if (!m_SimulatorActive)
					{
						selectedGrainIndex = GetSelectedGrain(m_debugSubmix.m_SelectedGrainIndex);
					}
					else
					{
						selectedGrainIndex = m_GranularSubmix.m_PrevSelectedGrain;
					}

					if (selectedGrainIndex >= 0 &&
						selectedGrainIndex + 1 < m_GrainData.Count)
					{
						if (IsPlayingGranular() &&
							selectedGrainIndex + 1 < m_GrainData.Count)
						{
							g.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb(140, 0, 255, 0)),
								(m_GrainData[selectedGrainIndex] - m_Xoffset) * m_Xscale,
								0,
								(m_GrainData[selectedGrainIndex + 1] - m_GrainData[selectedGrainIndex]) * m_Xscale,
								visBounds.Height);
						}

						Font arial = new Font(new FontFamily("Arial"), 10, FontStyle.Regular);
						SolidBrush textBrush = new SolidBrush(Color.Black);

						g.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb(210, 255, 255, 255)),
								0,
								0,
								420.0f,
								17.0f);

						PlaybackScreen parentScreen = m_Owner as PlaybackScreen;

						int samplesThisGranule = m_GrainData[selectedGrainIndex + 1] - m_GrainData[selectedGrainIndex];
						float granulesPerSecond = (m_WaveFile.Format.SampleRate / (float)samplesThisGranule);
						float rpm = granulesPerSecond * 2.0f * 60.0f;
						string rpmString = rpm.ToString() + " RPM";

						float grainSamples = m_GrainData[selectedGrainIndex + 1] - m_GrainData[selectedGrainIndex];
						float milliseconds = grainSamples / m_WaveFile.Format.SampleRate;
						milliseconds *= 1000;

						g.DrawString("Grain " + selectedGrainIndex + "/" + m_GrainData.Count + ": " + grainSamples.ToString() + " samples " + String.Format("{0:0.###}", milliseconds) + "ms " + rpmString, arial, textBrush, new PointF(0.0f, 0.0f));
					}
				}
			}

			// draw x axis
			g.DrawLine(p, 0, visBounds.Height / 2, visBounds.Width, visBounds.Height / 2);

			// draw a line to show the end of the wave
			g.DrawLine(p, lastx, 0, lastx, visBounds.Height);

			var zoom = (int)(m_Xzoom * 100);
		}

		/// <summary>
		/// Zoom in on the wave view
		/// </summary>
		public void ZoomIn()
		{
			m_Xzoom *= 1.1f;
			RefocusOnMousePos();
		}

		/// <summary>
		/// Zoom out on the wave view
		/// </summary>
		public void ZoomOut()
		{
			if (m_Xzoom > 1.0f)
			{
				m_Xzoom *= 0.9f;

				if(m_Xzoom < 1.0f )
				{
					m_Xzoom = 1.0f;
				}

				RefocusOnMousePos();
			}
		}

		/// <summary>
		/// Refocus on where the mouse currently is
		/// </summary>
		public void RefocusOnMousePos()
		{
			if(m_WaveFile != null)
			{
				int samplesInWindow = (int)(m_WaveFile.Data.NumSamples / m_Xzoom);
				float zoomTarget = m_LastMousePoint.X / m_WindowWidth;

				// This is a bit rubbish - zooms to centre - should really zoom to the mouse cursor pos
				float zoomTargetSamples = m_Xoffset + (samplesInWindow * zoomTarget) - (samplesInWindow / 2);
				m_Xoffset = (int)(zoomTargetSamples);

				if (m_Xoffset + samplesInWindow > m_WaveFile.Data.NumSamples)
				{
					m_Xoffset = (int)(m_WaveFile.Data.NumSamples - samplesInWindow);
				}

				if (m_Xoffset < 0)
				{
					m_Xoffset = 0;
				}
			}
		}

		/// <summary>
		/// Callback when the cursor position is changed
		/// </summary>
		/// <param name="xCoord"></param>
		/// <param name="groupSelect"></param>
		public void AdjustCursor(int xCoord, bool groupSelect, bool undoSelect)
		{
			if(m_WaveFile != null)
			{
				int samplesInWindow = (int)(m_WaveFile.Data.NumSamples / m_Xzoom);
				float fractionSelected = xCoord / m_WindowWidth;
				int newGrainSelectionCursor = (int)(m_Xoffset + (samplesInWindow * fractionSelected));

				int minSelection = Math.Min(newGrainSelectionCursor, m_GrainSelectionCursor);
				int maxSelection = Math.Max(newGrainSelectionCursor, m_GrainSelectionCursor);

				for (int loop = 0; loop < m_GrainData.Count; loop++)
				{
					if (!groupSelect && !undoSelect)
					{
						if (m_GrainData[loop] > newGrainSelectionCursor &&
							loop > 0)
						{
							ResetSelectedGrains();
							SelectGrain(loop - 1);
							break;
						}
					}
					else if (undoSelect)
					{
						if (m_GrainData[loop] > newGrainSelectionCursor &&
							   loop > 0)
						{
							if (m_debugSubmix.m_SelectedGrains[loop - 1])
							{
								UnSelectGrain(loop - 1);
							}
							else
							{
								SelectGrain(loop - 1);
							}
							
							break;
						}
					}
					else if(groupSelect)
					{
						if(m_GrainData[loop] > minSelection &&
						   m_GrainData[loop] < maxSelection)
						{
							if (newGrainSelectionCursor < m_GrainSelectionCursor)
							{
								if (loop > 0)
								{
									SelectGrain(loop - 1);
								}
							}
							else
							{
								SelectGrain(loop);
							}
						}
					}
				}

				m_GrainSelectionCursor = newGrainSelectionCursor;
			}
		}

		/// <summary>
		/// Scroll the view from side to side
		/// </summary>
		/// <param name="distance"></param>
		public void AdjustXOffset(float distance)
		{
			int samplesInWindow = (int)(m_WaveFile.Data.NumSamples / m_Xzoom);
			float fractionTravelled = distance / m_WindowWidth;
			m_Xoffset -= ((int)(samplesInWindow * fractionTravelled) * 5);

			if(m_Xoffset + samplesInWindow > m_WaveFile.Data.NumSamples)
			{
				m_Xoffset = (int)(m_WaveFile.Data.NumSamples - samplesInWindow);
			}

			if (m_Xoffset < 0)
			{
				m_Xoffset = 0;
			}
		}

		/// <summary>
		/// Reduce the number of selected grains by chopping out every Xth one
		/// </summary>
		/// <param name="stepRate"></param>
		public void ReduceGrainSelection(int grainsToRemove, int stepRate)
		{
			if (m_debugSubmix.m_SelectedGrains != null)
			{
				for (int loop = 0; loop < m_debugSubmix.m_SelectedGrains.Count; loop += stepRate)
				{
					for (int grainRemoveLoop = 0; grainRemoveLoop < grainsToRemove && loop < m_debugSubmix.m_SelectedGrains.Count; grainRemoveLoop++, loop++)
					{
						bool grainOwnedByLoop = false;

						for(int syncLoop = 0; syncLoop < m_SynchronisedLoops.Count; syncLoop++)
						{
							if (m_SynchronisedLoops[syncLoop].m_SelectedGrains[loop])
							{
								grainOwnedByLoop = true;
								break;
							}
						}

						if (!grainOwnedByLoop)
						{
							UnSelectGrain(loop);
						}
					}
				}
			}
		}

		/// <summary>
		/// Select everything
		/// </summary>
		public void SelectAll()
		{
			if (m_debugSubmix.m_SelectedGrains != null)
			{
				for (int loop = 0; loop < m_debugSubmix.m_SelectedGrains.Count; loop++)
				{
					m_debugSubmix.SelectGrain(loop);
				}
			}
		}

		/// <summary>
		/// Load new grain data
		/// </summary>
		/// <param name="fileName"></param>
		public void LoadGrainData(string fileName)
		{
			if(fileName.EndsWith("grn"))
			{
				m_GrainData.Clear();
				m_SynchronisedLoops.Clear();
				LoadGrainDataText(fileName);
			}
			else
			{
				return;
			}

			m_debugSubmix = new audGranularSubmix(this, null, m_GrainData.Count, m_GrainPlaybackOrder, audGranularSubmix.GranularSubmixType.SubmixTypeSynchronisedLoop);
			m_GranularSubmix = new audGranularSubmix(this, null, m_GrainData.Count, GrainPlaybackOrder.PlaybackOrderRandom, audGranularSubmix.GranularSubmixType.SubmixTypePureGranular);

			SelectGrain(0);
		}

		void ResetSelectedGrains()
		{
			m_debugSubmix.ResetSelectedGrains();
		}

		void SelectGrain(int index)
		{
			m_debugSubmix.SelectGrain(index);
		}

		void UnSelectGrain(int index)
		{
			m_debugSubmix.UnSelectGrain(index);
		}

		int GetSelectedGrain(int index)
		{
			return m_debugSubmix.GetSelectedGrain(index);
		}

		int GetNextSelectedGrain(int startIndex)
		{
			return m_debugSubmix.GetNextSelectedGrain(startIndex);
		}

		public void SetGrainRepetition(int repetition)
		{
			m_GrainRepetition = repetition;
		}

		public void EnableSimulator(bool enable)
		{
			m_SimulatorActive = enable;
		}

		/// <summary>
		/// Work out the nearest two loops that we should be playing, and what their volumes should be
		/// </summary>
		/// <param name="currentRPM"></param>
		/// <param name="nearestLoopAbove"></param>
		/// <param name="nearestLoopBelow"></param>
		/// <param name="nearestLoopAboveVolumeScale"></param>
		/// <param name="nearestLoopBelowVolumeScale"></param>
		void CalculateNearestLoops(float rpm, out int nearestLoopAbove, out int nearestLoopBelow, out float nearestLoopAboveVolumeScale, out float nearestLoopBelowVolumeScale)
		{
			nearestLoopAbove = -1;
			int nearestLoopAboveRPM = 10000;
			nearestLoopAboveVolumeScale = 1.0f;
			nearestLoopBelow = -1;
			int nearestLoopBelowRPM = -1;
			nearestLoopBelowVolumeScale = 1.0f;

			if(m_SimulatorGrainSelectionStyle == SimulatorGrainSelectionStyle.PlaybackStyleFractionBetweenClosestLoops)
			{
				for (int loop = 0; loop < m_SynchronisedLoops.Count; loop++)
				{
					if (m_SynchronisedLoops[loop].m_AverageRPM <= rpm &&
					   m_SynchronisedLoops[loop].m_AverageRPM > nearestLoopBelowRPM)
					{
						nearestLoopBelowRPM = m_SynchronisedLoops[loop].m_AverageRPM;
						nearestLoopBelow = loop;
					}
					else if (m_SynchronisedLoops[loop].m_AverageRPM > rpm &&
							m_SynchronisedLoops[loop].m_AverageRPM < nearestLoopAboveRPM)
					{
						nearestLoopAboveRPM = m_SynchronisedLoops[loop].m_AverageRPM;
						nearestLoopAbove = loop;
					}
				}

				// If we found a loop both above and below, we need to crossfade between them
				if (nearestLoopAbove >= 0 &&
				   nearestLoopBelow >= 0)
				{
					nearestLoopAboveVolumeScale = (rpm - m_SynchronisedLoops[nearestLoopBelow].m_AverageRPM) / (float)(m_SynchronisedLoops[nearestLoopAbove].m_AverageRPM - m_SynchronisedLoops[nearestLoopBelow].m_AverageRPM);
					
					if(nearestLoopAboveVolumeScale < 0.0f)
					{
						nearestLoopAboveVolumeScale = 0.0f;
					}

					if (nearestLoopAboveVolumeScale > 1.0f)
					{
						nearestLoopAboveVolumeScale = 1.0f;
					}

					nearestLoopBelowVolumeScale = 1.0f - nearestLoopAboveVolumeScale;
				}
			}
			else
			{
				int currentGrainIndex = GetClosestGrainToRPM(rpm);

				for (int loop = 0; loop < m_SynchronisedLoops.Count; loop++)
				{
					if (m_SynchronisedLoops[loop].m_AverageGrainIndex <= currentGrainIndex &&
					   m_SynchronisedLoops[loop].m_AverageGrainIndex > nearestLoopBelowRPM)
					{
						nearestLoopBelowRPM = m_SynchronisedLoops[loop].m_AverageGrainIndex;
						nearestLoopBelow = loop;
					}
					else if (m_SynchronisedLoops[loop].m_AverageGrainIndex > currentGrainIndex &&
							m_SynchronisedLoops[loop].m_AverageGrainIndex < nearestLoopAboveRPM)
					{
						nearestLoopAboveRPM = m_SynchronisedLoops[loop].m_AverageGrainIndex;
						nearestLoopAbove = loop;
					}
				}

				if (nearestLoopAbove >= 0 &&
				   nearestLoopBelow >= 0)
				{
					nearestLoopAboveVolumeScale = (currentGrainIndex - m_SynchronisedLoops[nearestLoopBelow].m_AverageGrainIndex) / (float)(m_SynchronisedLoops[nearestLoopAbove].m_AverageGrainIndex - m_SynchronisedLoops[nearestLoopBelow].m_AverageGrainIndex);
					nearestLoopBelowVolumeScale = 1.0f - nearestLoopAboveVolumeScale;
				}
			}
		}

		public void CalculateMinMaxRPM(out int minRPM, out int maxRPM)
		{
			minRPM = -1;
			maxRPM = -1;

			for (int loop = 0; loop < m_GrainData.Count - 2; loop++)
			{
				int grainLengthSamples = m_GrainData[loop + 1] - m_GrainData[loop];
				float grainLengthSeconds = grainLengthSamples / (float)m_WaveFile.Format.SampleRate;
				float hertz = 1 / grainLengthSeconds;
				int rpm = (int)(hertz * 60.0f * 2.0f);

				if(loop == 0)
				{
					minRPM = rpm;
					maxRPM = rpm;
				}
				else
				{
					if (rpm < minRPM)
					{
						minRPM = rpm;
					}
					else if(rpm > maxRPM)
					{
						maxRPM = rpm;
					}
				}
			}
		}

		/// <summary>
		/// Load grain data from a raw text file
		/// </summary>
		/// <param name="fileName"></param>
		void LoadGrainDataText(string fileName)
		{
			System.IO.StreamReader textReader = new System.IO.StreamReader(fileName);
			string line;

			while ((line = textReader.ReadLine()) != null)
			{
				m_GrainData.Add(Convert.ToInt32(line));
			}

			textReader.Close();
		}

		/// <summary>
		/// Use this to load grain data from an excel file (ie. MATLAB generated) 
		/// </summary>
		/// <param name="fileName"></param>
		public void LoadGrainDataExcel(string fileName)
		{
			Excel.Application xlApp;
			Excel.Workbook xlWorkBook;
			Excel.Worksheet xlWorkSheet;
			Excel.Range range;

			string str;
			int rCnt = 0;
			int cCnt = 0;

			xlApp = new Excel.ApplicationClass();
			xlWorkBook = xlApp.Workbooks.Open(fileName, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
			xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

			range = xlWorkSheet.UsedRange;

			for (rCnt = 1; rCnt <= range.Rows.Count; rCnt++)
			{
				for (cCnt = 1; cCnt <= range.Columns.Count; cCnt++)
				{
					str = (string)(range.Cells[rCnt, cCnt] as Excel.Range).Value2;
					m_GrainData.Add((int)(float.Parse(str)));
				}
			}

			xlWorkBook.Close(true, null, null);
			xlApp.Quit();

			releaseObject(xlWorkSheet);
			releaseObject(xlWorkBook);
			releaseObject(xlApp);
		}

		/// <summary>
		/// Save grain data
		/// </summary>
		/// <param name="fileName"></param>
		public void SaveGrainData(string fileName)
		{
			System.IO.StreamWriter fileStream = System.IO.File.CreateText(fileName);

			for(int loop = 0; loop < m_GrainData.Count; loop++)
			{
				fileStream.WriteLine(m_GrainData[loop]);
			}

			fileStream.Close();
		}

		/// <summary>
		/// Load loop data from a raw text file
		/// </summary>
		/// <param name="fileName"></param>
		public void LoadLoopData(string fileName)
		{
			ClearAllLoops();

			System.IO.StreamReader textReader = new System.IO.StreamReader(fileName);
			string line = textReader.ReadLine();
			int numLoops = Convert.ToInt32(line);

			for (int loop = 0; loop < numLoops; loop++)
			{
				line = textReader.ReadLine();
				GrainPlaybackOrder playbackOrder = (GrainPlaybackOrder)Convert.ToInt32(line);

				line = textReader.ReadLine();
				int numGrains = Convert.ToInt32(line);

				line = textReader.ReadLine();
				uint submixTypeHash = Convert.ToUInt32(line);

				audGranularSubmix submix = new audGranularSubmix(this, null, m_GrainData.Count, playbackOrder, audGranularSubmix.GranularSubmixType.SubmixTypeSynchronisedLoop);
				submix.m_SubmixTypeHash = submixTypeHash;

				for(int grain = 0; grain < numGrains; grain++)
				{
					line = textReader.ReadLine();
					int grainIndex = Convert.ToInt32(line);
					submix.SelectGrain(grainIndex);
				}

				submix.CalculateAverageRPM();
				m_SynchronisedLoops.Add(submix);
			}

			textReader.Close();
		}

		/// <summary>
		/// Save loop data
		/// </summary>
		/// <param name="fileName"></param>
		public void SaveLoopData(string fileName)
		{
			System.IO.StreamWriter fileStream = System.IO.File.CreateText(fileName);

			fileStream.WriteLine(m_SynchronisedLoops.Count);

			for (int loop = 0; loop < m_SynchronisedLoops.Count; loop++)
			{
				fileStream.WriteLine((int)m_SynchronisedLoops[loop].m_GrainPlaybackOrder);
				fileStream.WriteLine(m_SynchronisedLoops[loop].m_NumSelectedGrains);
				fileStream.WriteLine(m_SynchronisedLoops[loop].m_SubmixTypeHash);

				for (int grainIndex = 0; grainIndex < m_SynchronisedLoops[loop].m_SelectedGrains.Count; grainIndex++)
				{
					if(m_SynchronisedLoops[loop].m_SelectedGrains[grainIndex])
					{
						fileStream.WriteLine(grainIndex);
					}
				}
			}

			fileStream.Close();
		}

		/// <summary>
		/// Helper function for releasing excel objects
		/// </summary>
		/// <param name="obj"></param>
		private void releaseObject(object obj)
		{
			System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
			obj = null;
		} 
	}
}
