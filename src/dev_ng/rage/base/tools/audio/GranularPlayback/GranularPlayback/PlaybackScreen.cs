﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Wavelib;

namespace GranularPlayback
{
	public partial class PlaybackScreen : Form
	{
		public GrainPlayer m_GrainPlayer;
		public GrainGenerator m_GrainGenerator;
		public PitchAnalysis m_PitchAnalysis;
		WobbleGenerator m_WobbleGenerator;
		public string m_CurrentFileName;

		public PlaybackScreen()
		{
			InitializeComponent();
			m_GrainPlayer = new GrainPlayer(this);
			m_GrainGenerator = new GrainGenerator(this);
			m_WobbleGenerator = new WobbleGenerator();
			m_PitchAnalysis = new PitchAnalysis(m_GrainPlayer, m_GrainGenerator);
		}

		private void loadWaveDlg_FileOk(object sender, CancelEventArgs e)
		{
			OpenFileDialog dialog = sender as OpenFileDialog;

			if (dialog != null)
			{
				m_GrainPlayer.Kill();
				m_GrainPlayer.CreatePlaybackBuffer(dialog.FileName);
			}

			m_CurrentFileName = dialog.FileName.ToUpper();
			this.Text = "GEARS Tool - " + dialog.FileName;

			string grainFileName = m_CurrentFileName.Replace(".WAV", ".grn");

			if(System.IO.File.Exists(grainFileName))
			{
				m_GrainPlayer.LoadGrainData(grainFileName);

				string loopFileName = grainFileName.Replace(".grn", ".lpd");

				if (System.IO.File.Exists(loopFileName))
				{
					m_GrainPlayer.LoadLoopData(loopFileName);
				}
			}
		}

		private void loadGrainDlg_FileOk(object sender, CancelEventArgs e)
		{
			OpenFileDialog dialog = sender as OpenFileDialog;

			if (dialog != null)
			{
				m_GrainPlayer.LoadGrainData(dialog.FileName);
			}
		}

		private void playGrainButton_Click(object sender, EventArgs e)
		{
			if (!m_GrainPlayer.IsGrainBufferNull())
			{
				if (m_GrainPlayer.IsPlayingGranular())
				{
					m_GrainPlayer.Pause();
				}
				else
				{
					m_GrainPlayer.PlayGranular();
				}
			}
		}

		private void window_Closed(object sender, FormClosedEventArgs e)
		{
			m_GrainPlayer.Kill();
		}

		private void waveView_Paint(object sender, PaintEventArgs e)
		{
			var p = new Pen(new SolidBrush(Color.Crimson), 1.0f);
			m_GrainPlayer.DrawWave(e, !m_GrainPlayer.IsPlayingGranular() && !m_GrainGenerator.Visible && !m_PitchAnalysis.Visible);
		}

		private void waveViewAxis_Paint(object sender, PaintEventArgs e)
		{
			var p = new Pen(new SolidBrush(Color.Crimson), 1.0f);
			m_GrainPlayer.DrawAxis(e);
		}

		private void waveView_MouseDown(object sender, MouseEventArgs e)
		{
			waveView.Focus();
			
			if(e.Button == MouseButtons.Middle)
			{
				m_GrainPlayer.m_ViewBeingDragged = true;
				m_GrainPlayer.m_LastMousePoint = e.Location;
			}
			else if(e.Button == MouseButtons.Left)
			{
				m_GrainPlayer.AdjustCursor(e.Location.X, Control.ModifierKeys == Keys.Shift, Control.ModifierKeys == Keys.Control);
				m_GrainPlayer.m_GrainCursorBeingDragged = true;
			}
			else if(e.Button == MouseButtons.Right)
			{			
				int offset = 50;
				rightClickMenu.Show(e.Location.X + this.Location.X, e.Location.Y + this.Location.Y + offset);
			}
		}

		private void waveView_MouseUp(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Middle)
			{
				m_GrainPlayer.m_ViewBeingDragged = false;
			}
			else if(e.Button == MouseButtons.Left)
			{
				m_GrainPlayer.m_GrainCursorBeingDragged = false;
			}
		}

		private void waveView_MouseEnter(object sender, EventArgs e)
		{
			waveView.Focus();
			m_GrainPlayer.m_MouseOverView = true;
		}

		private void waveView_MouseLeave(object sender, EventArgs e)
		{
			m_GrainPlayer.m_ViewBeingDragged = false;
			m_GrainPlayer.m_MouseOverView = false;
		}

		private void waveView_MouseMove(object sender, MouseEventArgs e)
		{
			if(m_GrainPlayer.m_ViewBeingDragged)
			{
				float dragDistance = e.Location.X - m_GrainPlayer.m_LastMousePoint.X;
				m_GrainPlayer.AdjustXOffset(dragDistance);
			}

			if (m_GrainPlayer.m_GrainCursorBeingDragged &&
				Control.ModifierKeys != Keys.Shift &&
				Control.ModifierKeys != Keys.Control)
			{
				m_GrainPlayer.AdjustCursor(e.Location.X, false, false);
			}

			m_GrainPlayer.m_LastMousePoint = e.Location;
		}

		private void waveView_MouseWheel(object sender, MouseEventArgs e)
		{
			if(m_GrainPlayer.m_MouseOverView)
			{
				if (e.Delta > 0)
					m_GrainPlayer.ZoomIn();
				else
					m_GrainPlayer.ZoomOut();

				waveView.Refresh();
				waveViewTimeAxis.Refresh();
			}
		}

		private void timer1_Tick(object sender, EventArgs e)
		{
			if(!m_GrainPlayer.IsGrainBufferNull())
			{
				waveView.Refresh();
				waveViewTimeAxis.Refresh();

				if (m_GrainPlayer.IsPlayingGranular())
				{
					playGrainButton.Text = "Stop";
				}
				else
				{
					playGrainButton.Text = "Play";
				}
			}
		}

		private void drawGrainBoundaryCheck_CheckedChanged(object sender, EventArgs e)
		{
			m_GrainPlayer.m_DrawGrainBoundaries = !m_GrainPlayer.m_DrawGrainBoundaries;
		}

		private void drawSamplePositionsCheckbox_CheckedChanged(object sender, EventArgs e)
		{
			m_GrainPlayer.m_DrawSamplePositions = !m_GrainPlayer.m_DrawSamplePositions;
		}

		private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
		{
			m_GrainPlayer.SetGrainPlaybackOrder(playbackOrderCombo.SelectedIndex);
		}

		private void quitToolStripMenuItem_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void analyseZeroCrossingsToolStripMenuItem_Click(object sender, EventArgs e)
		{
			m_GrainGenerator.ShowDialog();
		}

		private void saveGrainDataToolStripMenuItem_Click(object sender, EventArgs e)
		{
			string fileName = m_CurrentFileName.Replace(".WAV", ".grn");
			saveGrainDlg.FileName = fileName;
			saveGrainDlg.ShowDialog();
		}

		private void loadWaveFileToolStripMenuItem_Click(object sender, EventArgs e)
		{
			loadWaveDlg.ShowDialog();
		}

		private void loadGrainDataToolStripMenuItem_Click(object sender, EventArgs e)
		{
			loadGrainDlg.ShowDialog();
		}

		private void saveGrainDataToolStripMenuItem1_Click(object sender, EventArgs e)
		{
			string fileName = System.IO.Path.GetFileName(m_CurrentFileName.Replace(".WAV", ".grn"));
			saveGrainDlg.FileName = fileName;

			saveGrainDlg.ShowDialog();
		}

		private void saveGrainDlg_FileOk(object sender, CancelEventArgs e)
		{
			if (m_GrainPlayer != null)
			{
				SaveFileDialog dialog = sender as SaveFileDialog;
				m_GrainPlayer.SaveGrainData(dialog.FileName);
			}
		}

		private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
		{
			About aboutScreen = new About();
			aboutScreen.ShowDialog();
		}

		private void reduceBy75ToolStripMenuItem1_Click(object sender, EventArgs e)
		{
			if (m_GrainPlayer != null)
			{
				m_GrainPlayer.ReduceGrainSelection(3, 1);
			}
		}

		private void reduceBy66ToolStripMenuItem1_Click(object sender, EventArgs e)
		{
			if (m_GrainPlayer != null)
			{
				m_GrainPlayer.ReduceGrainSelection(2,1);
			}
		}

		private void reduceBy50ToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if (m_GrainPlayer != null)
			{
				m_GrainPlayer.ReduceGrainSelection(1,1);
			}
		}

		private void reduceBy33ToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if (m_GrainPlayer != null)
			{
				m_GrainPlayer.ReduceGrainSelection(1,2);
			}
		}

		private void reduceBy25ToolStripMenuItem1_Click(object sender, EventArgs e)
		{
			if (m_GrainPlayer != null)
			{
				m_GrainPlayer.ReduceGrainSelection(1,3);
			}
		}

		private void selectAllToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if (m_GrainPlayer != null)
			{
				m_GrainPlayer.SelectAll();
			}
		}

		private void pitchAnalysisToolStripMenuItem_Click(object sender, EventArgs e)
		{
			m_PitchAnalysis.ShowDialog();
		}

		private void engineSimulatorToolStripMenuItem_Click(object sender, EventArgs e)
		{
			m_GrainPlayer.Pause();
			EngineSimulator simulator = new EngineSimulator(m_GrainPlayer);
			simulator.ShowDialog();
		}

		private void markAsLoopToolStripMenuItem_Click(object sender, EventArgs e)
		{
			m_GrainPlayer.CreateNewLoop();
		}

		private void drawLoopPositionsCheck_CheckedChanged(object sender, EventArgs e)
		{
			m_GrainPlayer.m_DrawLoopPositions = drawLoopPositionsCheck.Checked;
		}

		private void clearAllLoopsToolStripMenuItem_Click(object sender, EventArgs e)
		{
			m_GrainPlayer.ClearAllLoops();
		}

		private void flattenedWavFileDialog_FileOk(object sender, CancelEventArgs e)
		{
			if(m_GrainPlayer.GetNumGrains() > 2)
			{
				SaveFileDialog dialog = sender as SaveFileDialog;
				bwWaveFile waveFile = new bwWaveFile();
				List<byte> data = new List<byte>();

				int samplesPerMinute = 48000 * 60;
				int revolutionsPerMinute = 5000; // Just default to 5000 rpm for now
				float desiredPitch = (revolutionsPerMinute / 60.0f) / 2.0f;
				int samplesPerFiring = (samplesPerMinute / revolutionsPerMinute) * 2;
				int totalSamples = 0;

				for (int loop = 0; loop < m_GrainPlayer.GetNumGrains() - 1; loop++)
				{
					int grainStart = m_GrainPlayer.GetGrainPos(loop);
					int grainEnd = m_GrainPlayer.GetGrainPos(loop + 1);

					int grainSampleLength = grainEnd - grainStart;
					float stepRate = grainSampleLength / (float)samplesPerFiring;

					//float pitch = m_GrainGenerator.FindNearestPitch(grainStart);
					//float stepRate = desiredPitch/pitch;

					for (float i = grainStart; i < grainEnd; i += stepRate)
					{
						short result = (short)(m_GrainPlayer.ReadSampleFromSampleIndex(i) * 32767.0f);
						data.AddRange(BitConverter.GetBytes(result));
						totalSamples++;

						//pitch = m_GrainGenerator.FindNearestPitch((int)i);
						//stepRate = desiredPitch/pitch;
					}
				}

				bwDataChunk dataChunk = new bwDataChunk(data.ToArray(), (uint)totalSamples);
				waveFile.Data = dataChunk;

				bwFormatChunk formatChunk = new bwFormatChunk(1, 1, 48000, 96000, 2, 16, new byte[0]);
				waveFile.Format = formatChunk;

				waveFile.Save(dialog.FileName);
			}
		}

		private void generateTestSweepToolStripMenuItem_Click(object sender, EventArgs e)
		{
			SweepGenerator generator = new SweepGenerator();
			generator.ShowDialog();
		}

		private void generateFlattenedSweepToolStripMenuItem_Click(object sender, EventArgs e)
		{
			flattenedWavFileDialog.ShowDialog();
		}

		private void saveLoopDataToolStripMenuItem_Click(object sender, EventArgs e)
		{
			string fileName = System.IO.Path.GetFileName(m_CurrentFileName.Replace(".WAV", ".lpd"));
			saveLoopData.FileName = fileName;

			saveLoopData.ShowDialog();
		}

		private void saveLoopData_FileOk(object sender, CancelEventArgs e)
		{
			if (m_GrainPlayer != null)
			{
				SaveFileDialog dialog = sender as SaveFileDialog;
				m_GrainPlayer.SaveLoopData(dialog.FileName);
			}
		}

		private void loadLoopDataToolStripMenuItem_Click(object sender, EventArgs e)
		{
			loadLoopData.ShowDialog();
		}

		private void loadLoopData_FileOk(object sender, CancelEventArgs e)
		{
			if (m_GrainPlayer != null)
			{
				OpenFileDialog dialog = sender as OpenFileDialog;
				m_GrainPlayer.LoadLoopData(dialog.FileName);
			}
		}

		private void grainPlaybackStyle_SelectedIndexChanged(object sender, EventArgs e)
		{
			if(m_GrainPlayer != null)
			{
				m_GrainPlayer.SetGrainPreviewStyle(grainPlaybackStyle.SelectedIndex);
			}
		}

		private void flattenedRPMValue_ValueChanged(object sender, EventArgs e)
		{
			m_GrainPlayer.m_FlattenedRPMValue = (float) flattenedRPMValue.Value;
		}

		private void deleteLoopToolStripMenuItem_Click(object sender, EventArgs e)
		{
			m_GrainPlayer.DeleteLoop();
		}

		private void sampleInterpolationCombo_SelectedIndexChanged(object sender, EventArgs e)
		{
			m_GrainPlayer.m_SampleInterpolationMethod = (GrainPlayer.SampleInterpolationMethod) sampleInterpolationCombo.SelectedIndex;
		}

		private void wobbleGeneratorToolStripMenuItem_Click(object sender, EventArgs e)
		{
			m_WobbleGenerator.ShowDialog();
		}

		private void clearGrainDataToolStripMenuItem_Click(object sender, EventArgs e)
		{
			m_GrainPlayer.ClearAllLoops();
			m_GrainPlayer.ClearGrainData();

			m_GrainPlayer.m_GrainData.Add(0);
			m_GrainPlayer.m_GrainData.Add((int)m_GrainPlayer.m_WaveFile.Data.NumSamples);
		}

		private void applyNameHashToolStripMenuItem_Click(object sender, EventArgs e)
		{
			m_GrainPlayer.SetSubmixTypeHash(ComputeHash(submixNameHash.Text));
		}

		// taken from rage atl/map.cpp
		uint ComputeHash(string str)
		{
			if (str == null || str.Equals(""))
			{
				return 0;
			}

			//Convert .Net Wave name to a system string and then to a Rage hash code.
			byte[] utf8 = System.Text.Encoding.UTF8.GetBytes(str);


			// This is the one-at-a-time hash from this page:
			// http://burtleburtle.net/bob/hash/doobs.html
			// (borrowed from /soft/swat/src/swcore/string2key.cpp)
			uint key = 0;

			for (int i = 0; i < str.Length; i++)
			{
				byte character = utf8[i];
				if (character >= 'A' && character <= 'Z')
					character += 'a' - 'A';
				else if (character == '\\')
					character = (byte)'/';

				key += character;
				key += (key << 10);	//lint !e701
				key ^= (key >> 6);	//lint !e702
			}
			key += (key << 3);	//lint !e701
			key ^= (key >> 11);	//lint !e702
			key += (key << 15);	//lint !e701

			// The original swat code did several tests at this point to make
			// sure that the resulting value was representable as a valid
			// floating-point number (not a negative zero, or a NaN or Inf)
			// and also reserved a nonzero value to indicate a bad key.
			// We don't do this here for generality but the caller could
			// obviously add such checks again in higher-level code.
			return key;
		}

		private void removeUnselectedGrainsToolStripMenuItem_Click(object sender, EventArgs e)
		{
			m_GrainPlayer.RemoveGrains(false);
		}

		private void chopSelectedGrainsToolStripMenuItem_Click(object sender, EventArgs e)
		{
			m_GrainPlayer.RemoveGrains(true);
		}
	}
}
