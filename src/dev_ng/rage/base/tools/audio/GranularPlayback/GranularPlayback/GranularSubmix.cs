﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GranularPlayback
{
	public class audGranularSubmix
	{
		public List<bool> m_SelectedGrains;
		public int m_NumSelectedGrains;

		public int m_SelectedGrainIndex;
		public int m_AverageRPM;
		public int m_AverageGrainIndex;
		public float m_LastVolumeScale;
		public int m_PrevSelectedGrain;

		struct audGrainState
		{
			public int data;
			public int grainLength;
			public double sampleIndex;
		};

		audGrainState[] m_ActiveGrains = new audGrainState[2];
		audGrainState m_PrevGrain;
		int m_ActiveGrainIndex = 0;
		int m_WalkPlaybackStepDirection = 1;
		public uint m_SubmixTypeHash = 0;

		public enum GranularSubmixType
		{
			SubmixTypeSynchronisedLoop,
			SubmixTypePureGranular,
		};

		GranularSubmixType m_submixType;

		float m_EnvAttackValue = 0.0f;
		float m_EnvReleaseValue = 0.0f;
		float m_StepVal = 0.0f;

		public GrainPlayer.GrainPlaybackOrder m_GrainPlaybackOrder;

		GrainPlayer.GrainPlaybackOrder m_MixAndMatchPlaybackOrder;
		int m_NumGrainsSelectedWithMixNMatch = 0;

		GrainPlayer m_Parent;

		public audGranularSubmix(GrainPlayer parent, List<bool> selectedGrains, int numSelectedGrains, GrainPlayer.GrainPlaybackOrder playbackOrder, GranularSubmixType submixType)
		{
			m_Parent = parent;
			m_GrainPlaybackOrder = playbackOrder;
			m_submixType = submixType;

			if (selectedGrains == null)
			{
				m_SelectedGrains = new List<bool>(numSelectedGrains);

				for (int loop = 0; loop < numSelectedGrains; loop++)
				{
					m_SelectedGrains.Add(false);
				}
				
				m_NumSelectedGrains = 0;
			}
			else
			{
				m_SelectedGrains = selectedGrains;
				m_NumSelectedGrains = numSelectedGrains;
			}

			CalculateAverageRPM();

			// Kick off the loop
			m_SelectedGrainIndex = 0;
			m_SelectedGrainIndex = 0;

			// start halfway through the first grain
			m_ActiveGrainIndex = 0;

			int numSamples = 0;

			if(m_Parent.m_GrainData.Count > 1)
			{
				numSamples = m_Parent.m_GrainData[1] - m_Parent.m_GrainData[0];
			}
			
			int sampleIndex = m_Parent.m_GrainData[0];
			m_ActiveGrains[m_ActiveGrainIndex].sampleIndex = 0;
			m_ActiveGrains[m_ActiveGrainIndex].data = sampleIndex;
			m_ActiveGrains[m_ActiveGrainIndex].grainLength = numSamples;
		}

		public int GetSelectedGrain(int index)
		{
			int numFound = 0;

			for (int loop = 0; loop < m_SelectedGrains.Count; loop++)
			{
				if (m_SelectedGrains[loop])
				{
					if (numFound == index)
					{
						return loop;
					}

					numFound++;
				}
			}

			return -1;
		}

		public int GetNextSelectedGrain(int startIndex)
		{
			for (int loop = startIndex + 1; loop < m_SelectedGrains.Count; loop++)
			{
				if (m_SelectedGrains[loop])
				{
					return loop;
				}
			}

			return -1;
		}

		public void ResetSelectedGrains()
		{
			if (m_SelectedGrains != null)
			{
				for (int loop = 0; loop < m_SelectedGrains.Count; loop++)
				{
					m_SelectedGrains[loop] = false;
				}
			}

			m_NumSelectedGrains = 0;
			m_SelectedGrainIndex = 0;
		}

		public void SelectGrain(int index)
		{
			if (!m_SelectedGrains[index])
			{
				m_SelectedGrains[index] = true;
				m_NumSelectedGrains++;
			}
		}

		public void UnSelectGrain(int index)
		{
			if (m_SelectedGrains[index])
			{
				m_SelectedGrains[index] = false;
				m_NumSelectedGrains--;

				if(m_SelectedGrainIndex >= m_NumSelectedGrains)
				{
					m_SelectedGrainIndex = m_NumSelectedGrains - 1;
				}
			}
		}

		public void CalculateAverageRPM()
		{
			if (m_NumSelectedGrains > 0)
			{
				int grainIndex = GetSelectedGrain(0);

				float totalRPM = 0.0f;
				int totalGrainIndex = 0;

				for (int loop = 0; loop < m_NumSelectedGrains; loop++)
				{
					if (grainIndex + 1 < m_Parent.m_GrainData.Count && grainIndex >= 0)
					{
						int grainLengthSamples = m_Parent.m_GrainData[grainIndex + 1] - m_Parent.m_GrainData[grainIndex];
						float grainLengthSeconds = grainLengthSamples / (float)m_Parent.m_WaveFile.Format.SampleRate;
						float hertz = 1 / grainLengthSeconds;
						totalRPM += hertz * 60.0f * 2.0f;
						totalGrainIndex += grainIndex;
						grainIndex = GetNextSelectedGrain(grainIndex);
					}
				}

				m_AverageRPM = (int)(totalRPM / m_NumSelectedGrains);
				m_AverageGrainIndex = (int)(totalGrainIndex / (float)m_NumSelectedGrains);
			}
		}

		public void SetSubmixTypeHash(uint hash)
		{
			m_SubmixTypeHash = hash;
		}

		public void OnGrainRemoved(int index)
		{
			m_SelectedGrains.RemoveAt(index);
			CalculateAverageRPM();
		}

		public float[] FeedSynchronised(int bufferBytes, float volumeScale, double currentGranularFraction, double granularFractionStepRate, double granularFractionStepRateChange, float currentRPM, float rpmStepRate)
		{
			int mixSamples = m_Parent.BytesToSampleIndex(bufferBytes);
			float[] destBuffer = new float[mixSamples];
			float sampleStepRate = 1.0f * m_Parent.m_GlobalPitchScale;
			m_LastVolumeScale = volumeScale;

			int samplesThisGranule = m_ActiveGrains[m_ActiveGrainIndex].grainLength;
			int samplesPrevGranule = m_ActiveGrains[(m_ActiveGrainIndex + 1) % 2].grainLength;

			m_ActiveGrains[m_ActiveGrainIndex].sampleIndex = samplesThisGranule * currentGranularFraction;
			m_ActiveGrains[(m_ActiveGrainIndex + 1) % 2].sampleIndex = samplesPrevGranule * currentGranularFraction;
			
			for (int i = 0; i < mixSamples; i++)
			{
				if (m_Parent.m_PlaybackState == GrainPlayer.audPlaybackState.PlaybackStatePaused)
				{
					destBuffer[i] = 0.0f;
					continue;
				}

				// is the active grain finished?
				if (currentGranularFraction >= 1.0)
				{
					currentGranularFraction -= 1.0;
					m_PrevGrain = m_ActiveGrains[m_ActiveGrainIndex];

					// next grain becomes the active grain
					m_ActiveGrainIndex = (m_ActiveGrainIndex + 1) % 2;

					int grainIndex;

					if (m_submixType == GranularSubmixType.SubmixTypeSynchronisedLoop)
					{
						grainIndex = SelectNewGrain();
					}
					else
					{
						grainIndex = m_Parent.GetClosestGrainToRPM(currentRPM);
					}

					int numSamples = m_Parent.m_GrainData[grainIndex + 1] - m_Parent.m_GrainData[grainIndex];
					int startSampleIndex = m_Parent.m_GrainData[grainIndex];
					m_ActiveGrains[m_ActiveGrainIndex].data = startSampleIndex;
					m_ActiveGrains[m_ActiveGrainIndex].sampleIndex = numSamples * currentGranularFraction;
					m_ActiveGrains[m_ActiveGrainIndex].grainLength = numSamples;
					m_PrevSelectedGrain = grainIndex;
				}

				int nextActiveIndex = (m_ActiveGrainIndex + 1) % 2;
				double activeSampleOffset = m_ActiveGrains[m_ActiveGrainIndex].sampleIndex;
				float activeGrainSample = m_Parent.ReadSampleFromSampleIndex(m_ActiveGrains[m_ActiveGrainIndex].data + activeSampleOffset);
				float activeGrainVol = 1.0f;
				float prevGrainSample = 0.0f;

				// Do a quick 1/20th granule crossfade to the next granule
				if(currentGranularFraction < 0.05f)
				{
					double prevGrainSampleOffset = m_PrevGrain.data + (m_PrevGrain.grainLength * (currentGranularFraction + 1.0f));
					prevGrainSample = m_Parent.ReadSampleFromSampleIndex(prevGrainSampleOffset);
					activeGrainVol = (float) currentGranularFraction / 0.05f;
				}

				destBuffer[i] = (activeGrainVol * activeGrainSample) + ((1.0f - activeGrainVol) * prevGrainSample);
				destBuffer[i] *= volumeScale;

				currentGranularFraction += granularFractionStepRate;
				granularFractionStepRate += granularFractionStepRateChange;
				currentRPM += rpmStepRate;

				m_ActiveGrains[m_ActiveGrainIndex].sampleIndex = m_ActiveGrains[m_ActiveGrainIndex].grainLength * currentGranularFraction;
			}

			return destBuffer;
		}

		public float[] Feed(int bufferBytes, float volumeScale, float rpm, float rpmStepRate)
		{
			int mixSamples = m_Parent.BytesToSampleIndex(bufferBytes);
			float[] destBuffer = new float[mixSamples];
			float sampleStepRate = 1.0f * m_Parent.m_GlobalPitchScale;
			m_LastVolumeScale = volumeScale;

			for (int i = 0; i < mixSamples; i++)
			{
				if (m_Parent.m_PlaybackState == GrainPlayer.audPlaybackState.PlaybackStatePaused)
				{
					destBuffer[i] = 0.0f;
					continue;
				}

				// is the active grain finished?
				if (m_ActiveGrains[m_ActiveGrainIndex].sampleIndex >= m_ActiveGrains[m_ActiveGrainIndex].grainLength)
				{
					// next grain becomes the active grain
					m_PrevGrain = m_ActiveGrains[m_ActiveGrainIndex];

					m_ActiveGrainIndex = (m_ActiveGrainIndex + 1) % 2;

					int grainIndex = SelectNewGrain();

					int numSamples = m_Parent.m_GrainData[grainIndex + 1] - m_Parent.m_GrainData[grainIndex];
					int startSampleIndex = m_Parent.m_GrainData[grainIndex];
					m_ActiveGrains[m_ActiveGrainIndex].data = startSampleIndex;
					m_ActiveGrains[m_ActiveGrainIndex].sampleIndex = 0;
					m_ActiveGrains[m_ActiveGrainIndex].grainLength = numSamples;
				}

				double sampleOffset = m_ActiveGrains[m_ActiveGrainIndex].sampleIndex;
				float activeGrainSample = m_Parent.ReadSampleFromSampleIndex(m_ActiveGrains[m_ActiveGrainIndex].data + sampleOffset);
				float activeGrainVol = 1.0f;
				float prevGrainSample = 0.0f;

				// Do a quick 1/20th granule crossfade to the next granule
				if (m_ActiveGrains[m_ActiveGrainIndex].sampleIndex / m_ActiveGrains[m_ActiveGrainIndex].grainLength < 0.05f)
				{
					double prevGrainSampleOffset = m_PrevGrain.data + m_PrevGrain.grainLength + m_ActiveGrains[m_ActiveGrainIndex].sampleIndex;
					prevGrainSample = m_Parent.ReadSampleFromSampleIndex(prevGrainSampleOffset);
					activeGrainVol = (float)(m_ActiveGrains[m_ActiveGrainIndex].sampleIndex / m_ActiveGrains[m_ActiveGrainIndex].grainLength) / 0.05f;
				}

				destBuffer[i] = (activeGrainSample * activeGrainVol) + (prevGrainSample * (1.0f - activeGrainVol)); ;

				m_ActiveGrains[m_ActiveGrainIndex].sampleIndex += sampleStepRate;
				destBuffer[i] *= volumeScale;
				rpm += rpmStepRate;
			}

			return destBuffer;
		}

		/// <summary>
		/// Choose a new grain depending on the current playback method
		/// </summary>
		/// <returns></returns>
		int SelectNewGrain()
		{
			GrainPlayer.GrainPlaybackOrder playbackOrder = m_GrainPlaybackOrder;

			if(m_GrainPlaybackOrder == GrainPlayer.GrainPlaybackOrder.PlaybackOrderMixNMatch)
			{
				playbackOrder = m_MixAndMatchPlaybackOrder;
				m_NumGrainsSelectedWithMixNMatch++;

				if (m_MixAndMatchPlaybackOrder == GrainPlayer.GrainPlaybackOrder.PlaybackOrderWalk)
				{
					if (m_NumGrainsSelectedWithMixNMatch >= m_NumSelectedGrains * 2)
					{
						m_MixAndMatchPlaybackOrder = (GrainPlayer.GrainPlaybackOrder)m_Parent.m_RandomGenerator.Next(0, (int)GrainPlayer.GrainPlaybackOrder.PlaybackOrderMixNMatch - 1);
						m_NumGrainsSelectedWithMixNMatch = 0;
					}
				}
				else
				{
					if (m_NumGrainsSelectedWithMixNMatch >= m_NumSelectedGrains)
					{
						m_MixAndMatchPlaybackOrder = (GrainPlayer.GrainPlaybackOrder)m_Parent.m_RandomGenerator.Next(0, (int)GrainPlayer.GrainPlaybackOrder.PlaybackOrderMixNMatch - 1);
						m_NumGrainsSelectedWithMixNMatch = 0;
					}
				}
			}

			if (playbackOrder == GrainPlayer.GrainPlaybackOrder.PlaybackOrderSequential)
			{
				m_SelectedGrainIndex++;

				// Played all the grains? Loop back to the start
				if (m_SelectedGrainIndex >= m_NumSelectedGrains)
				{
					m_SelectedGrainIndex = 0;
				}
			}
			else if (playbackOrder == GrainPlayer.GrainPlaybackOrder.PlaybackOrderRandom)
			{
				int originalIndex = m_SelectedGrainIndex;

				// Make sure we don't pick the same grain more than once
				while (m_SelectedGrainIndex == originalIndex &&
					   m_NumSelectedGrains > 1)
				{
					m_SelectedGrainIndex = m_Parent.m_RandomGenerator.Next(0, m_NumSelectedGrains);
				}
			}
			else if (playbackOrder == GrainPlayer.GrainPlaybackOrder.PlaybackOrderWalk)
			{
				m_SelectedGrainIndex += m_WalkPlaybackStepDirection;

				if (m_SelectedGrainIndex < 0)
				{
					m_SelectedGrainIndex = 0;
					m_WalkPlaybackStepDirection = 1;
				}
				else if (m_SelectedGrainIndex >= m_NumSelectedGrains)
				{
					m_SelectedGrainIndex = m_NumSelectedGrains - 1;
					m_WalkPlaybackStepDirection = -1;
				}
			}
			else if (playbackOrder == GrainPlayer.GrainPlaybackOrder.PlaybackOrderReverse)
			{
				m_SelectedGrainIndex--;

				// Played all the grains? Loop back to the start
				if (m_SelectedGrainIndex < 0)
				{
					m_SelectedGrainIndex = m_NumSelectedGrains - 1;
				}
			}

			if (m_SelectedGrainIndex >= m_NumSelectedGrains)
			{
				m_SelectedGrainIndex = m_NumSelectedGrains - 1;
			}

			if (m_SelectedGrainIndex < 0)
			{
				m_SelectedGrainIndex = 0;
			}

			int grainIndex = GetSelectedGrain(m_SelectedGrainIndex);

			if (grainIndex >= m_Parent.m_GrainData.Count - 1)
			{
				grainIndex = m_Parent.m_GrainData.Count - 2;
			}

			if (grainIndex < 0)
			{
				grainIndex = 0;
			}

			return grainIndex;
		}
	}
}
