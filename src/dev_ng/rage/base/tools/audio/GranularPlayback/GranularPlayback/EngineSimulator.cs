﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.DirectX.DirectInput;

namespace GranularPlayback
{
	public partial class EngineSimulator : Form
	{
		float m_CurrentRevs = 0.0f;
		float m_PrevRevs = 0.0f;
		float m_RevChangeFactor = 400.0f;
		GrainPlayer m_GrainPlayer;
		List<float> m_RPMValues;
		List<GranularVolumeInfo> m_GranularVolumes;
		List<List<float>> m_LoopVolumes;
		int m_MaxRPMValues = 100;
		float m_LastThrottleValue;
		Device m_gamepadDevice;
		JoystickState m_gamepadState;
		float m_currentGamepadThrottle;
		float m_ThrottleMovementSmoother = 0.3f;

		struct GranularVolumeInfo
		{
			public float m_RPM;
			public float m_Volume;
		}

		public EngineSimulator(GrainPlayer waveplayer)
		{
			InitializeComponent();
			m_GrainPlayer = waveplayer;
			m_RPMValues = new List<float>();
			m_LoopVolumes = new List<List<float>>();
			m_GranularVolumes = new List<GranularVolumeInfo>();
			m_GrainPlayer.m_SimulatorPlaybackStyle = (GrainPlayer.SimulatorPlaybackStyle)0;
			sampleInterpolationCombo.SelectedIndex = (int)m_GrainPlayer.m_SampleInterpolationMethod;

			for(int loop = 0; loop < m_GrainPlayer.m_SynchronisedLoops.Count; loop++)
			{
				m_LoopVolumes.Add(new List<float>());
			}

			int minRPM = 1000;
			int maxRPM = 9000;
			waveplayer.CalculateMinMaxRPM(out minRPM, out maxRPM);

			if (minRPM > minRPMUpDown.Minimum &&
				minRPM < minRPMUpDown.Maximum)
			{
				minRPMUpDown.Value = minRPM;
			}

			if (maxRPM > maxRPMUpDown.Minimum &&
				maxRPM < maxRPMUpDown.Maximum)
			{
				maxRPMUpDown.Value = maxRPM;
			}

			DeviceList controllerList = Manager.GetDevices(DeviceClass.GameControl, EnumDevicesFlags.AttachedOnly);

			if (controllerList.Count > 0)
			{
				controllerList.MoveNext();

				DeviceInstance deviceInstance = (DeviceInstance)controllerList.Current;

				m_gamepadDevice = new Device(deviceInstance.InstanceGuid);
				m_gamepadDevice.SetCooperativeLevel(this, CooperativeLevelFlags.Background | CooperativeLevelFlags.NonExclusive);
				m_gamepadDevice.SetDataFormat(DeviceDataFormat.Joystick);
				m_gamepadDevice.Acquire();
			}
		}

		private void window_Closed(object sender, FormClosedEventArgs e)
		{
			m_GrainPlayer.Pause();
			simUpdateTimer.Enabled = false;
			m_GrainPlayer.m_SimulatorActive = false;
		}

		private void simUpdateTimer_Tick(object sender, EventArgs e)
		{
			SimulateEngine();

			if(m_GrainPlayer.IsPlayingGranular())
			{
				playButton.Text = "Stop";
			}
			else
			{
				playButton.Text = "Play";
			}

			RPMGraph.Refresh();
			rpmViewAxis.Refresh();
		}

		private void SimulateEngine()
		{
			if(m_GrainPlayer.IsGrainBufferNull())
			{
				return;
			}

			if (m_gamepadDevice != null)
			{
				try
				{
					m_gamepadDevice.Poll();
					m_gamepadState = m_gamepadDevice.CurrentJoystickState;
				}
				catch (Exception err)
				{
				}

				if (m_gamepadState.Z < 32767)
				{
					float throttleAmount = 1.0f - (m_gamepadState.Z / 32767.0f);
					m_currentGamepadThrottle += (throttleAmount - m_currentGamepadThrottle) * m_ThrottleMovementSmoother;
					throttleSlider.Value = (int)(100.0f - (100.0f * m_currentGamepadThrottle));
				}
				else if (m_currentGamepadThrottle > 0.0f)
				{
					m_currentGamepadThrottle *= (1.0f - m_ThrottleMovementSmoother);
					throttleSlider.Value = (int)(100.0f - (100.0f * m_currentGamepadThrottle));

					if (m_currentGamepadThrottle < 0.001f)
					{
						m_currentGamepadThrottle = 0.0f;
					}
				}
			}

			m_PrevRevs = m_CurrentRevs;

			if (throttleSlider.Value > 100) throttleSlider.Value = 100;

			float newThrottleValue = (float)(100.0f - throttleSlider.Value) / 100.0f;
			float throttleValue = m_LastThrottleValue + ((newThrottleValue - m_LastThrottleValue) * 0.1f);
			float inertiaValue = (float)(100.0f - intertiaSlider.Value) / 100.0f;
			float maxRPM = (float)maxRPMUpDown.Value;
			float minRPM = (float)minRPMUpDown.Value;
			float desiredRevs = minRPM + ((maxRPM - minRPM) * throttleValue);
			float oldRevs = m_CurrentRevs;
			m_LastThrottleValue = throttleValue;

			if (m_CurrentRevs < desiredRevs)
			{
				m_CurrentRevs += inertiaValue * m_RevChangeFactor;

				if(m_CurrentRevs > desiredRevs)
				{
					m_CurrentRevs = desiredRevs;
				}
			}
			else if (m_CurrentRevs > desiredRevs)
			{
				m_CurrentRevs -= inertiaValue * m_RevChangeFactor;

				if (m_CurrentRevs < desiredRevs)
				{
					m_CurrentRevs = desiredRevs;
				}
			}

			if(m_CurrentRevs < minRPM)
			{
				m_CurrentRevs = minRPM;
			}
			if (m_CurrentRevs > maxRPM)
			{
				m_CurrentRevs = maxRPM;
			}

			m_GrainPlayer.m_SimulatedRPMProportion = (m_CurrentRevs - minRPM) / (maxRPM - minRPM);
			m_GrainPlayer.m_SimulatedRPM = (int)m_CurrentRevs;
			m_RPMValues.Add(m_CurrentRevs);

			if(m_RPMValues.Count > m_MaxRPMValues)
			{
				m_RPMValues.RemoveAt(0);
			}

			GranularVolumeInfo granularVolumeInfo;
			granularVolumeInfo.m_RPM = m_CurrentRevs;
			granularVolumeInfo.m_Volume = m_GrainPlayer.m_GranularSubmix.m_LastVolumeScale;

			m_GranularVolumes.Add(granularVolumeInfo);

			if (m_GranularVolumes.Count > m_MaxRPMValues)
			{
				m_GranularVolumes.RemoveAt(0);
			}

			for(int loop = 0; loop < m_GrainPlayer.m_SynchronisedLoops.Count; loop++)
			{
				m_LoopVolumes[loop].Add(m_GrainPlayer.m_SynchronisedLoops[loop].m_LastVolumeScale);

				if(m_LoopVolumes[loop].Count > m_MaxRPMValues)
				{
					m_LoopVolumes[loop].RemoveAt(0);
				}
			}

			m_GrainPlayer.m_ChangeRateForMaxGrains = (int) changeRateForMaxGrainsUpDown.Value;
			m_GrainPlayer.m_ChangeRateForMaxLoops = (int) changeRateForMaxLoopsUpDown.Value;

			float changeRate = m_CurrentRevs - m_PrevRevs;
			float chageRatePerSec = changeRate / 0.016f;
			m_GrainPlayer.m_RPMChangeRate = chageRatePerSec;
		}

		private void playButton_Click(object sender, EventArgs e)
		{
			if(m_GrainPlayer.IsPlayingGranular())
			{
				m_GrainPlayer.EnableSimulator(false);
				m_GrainPlayer.Pause();
			}
			else
			{
				m_GrainPlayer.EnableSimulator(true);
				m_GrainPlayer.PlayGranular();
			}
		}

		private void RPMGraph_Paint(object sender, PaintEventArgs e)
		{
			var p = new Pen(new SolidBrush(Color.DarkBlue), 1.0f);

			var g = e.Graphics;
			var visBounds = g.VisibleClipBounds;
			
			int lastx = 0, lasty = 0;
			var gp = new GraphicsPath();

			g.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb(240, 240, 255)),
							0,
							0,
							visBounds.Width,
							visBounds.Height);

			p = Pens.DarkBlue;

			for (int loop = 0; loop < m_GrainPlayer.m_SynchronisedLoops.Count; loop++ )
			{
				var y = visBounds.Height * (1.0f - (m_GrainPlayer.m_SynchronisedLoops[loop].m_AverageRPM / 10000.0f));

				for (var i = 0; i < m_MaxRPMValues && i < m_RPMValues.Count; i++)
				{	
					var x = visBounds.Width * (i / (float)m_MaxRPMValues);

					if(m_LoopVolumes[loop][i] != 0.0f)
					{
						g.FillEllipse(new System.Drawing.SolidBrush(Color.FromArgb((int)(255 * m_LoopVolumes[loop][i]), 255, 0, 0)), x - 2, y - 2, 4, 4);
					}
				}
			}

			for (var i = 0; i < m_MaxRPMValues && i < m_RPMValues.Count; i++)
			{
				var x = visBounds.Width * (i / (float)m_MaxRPMValues);
				var y = visBounds.Height * (1.0f - (m_GranularVolumes[i].m_RPM / 10000.0f));

				g.FillEllipse(new System.Drawing.SolidBrush(Color.FromArgb((int)(255 * m_GranularVolumes[i].m_Volume), 0, 255, 0)), x - 2, y - 2, 5, 5);
			}

			for (var i = 0; i < m_MaxRPMValues && i < m_RPMValues.Count; i++)
			{
				var y = visBounds.Height * (1.0f - (m_RPMValues[i] / 10000));
				var x = visBounds.Width * (i / (float)m_MaxRPMValues);

				if (x > visBounds.Width &&
				   lastx > visBounds.Width)
				{
					break;
				}

				if (i >= 1)
				{
					gp.AddLine(lastx, lasty, x, y);
				}

				lastx = (int)x;
				lasty = (int)y;
			}

			// draw path
			g.DrawPath(p, gp);

			Font arialBold = new Font(new FontFamily("Arial"), 20, FontStyle.Bold);
			SolidBrush textBrush = new SolidBrush(Color.Black);
			g.DrawString(((int)m_CurrentRevs).ToString() + " RPM", arialBold, textBrush, new PointF(0.0f, 0.0f));

			Font arial = new Font(new FontFamily("Arial"), 10, FontStyle.Regular);
			float changeRate = m_CurrentRevs - m_PrevRevs;
			float chageRatePerSec = changeRate / 0.016f;
			string changeRateString = String.Format("{0:0.#}", chageRatePerSec);

			if(changeRate > 0)
			{
				changeRateString = "+" + changeRateString;
			}

			g.DrawString("Change Rate: " + changeRateString, arial, textBrush, new PointF(0.0f, 30.0f));
		}

		private void RPMAxis_Paint(object sender, PaintEventArgs pea)
		{
			var grfx = pea.Graphics;
			var visBounds = grfx.VisibleClipBounds;
			Font arial = new Font(new FontFamily("Arial"), 10, FontStyle.Regular);
			SolidBrush textBrush = new SolidBrush(Color.Black);

			for (int loop = 0; loop < 10; loop ++)
			{
				float yCoord = visBounds.Height - ((visBounds.Height / 10.0f) * loop);
				string timeString = String.Format("{0:0.###}", loop * 1000);
				grfx.DrawString(timeString, arial, textBrush, new PointF((visBounds.Width / 2) - 10, yCoord + 4));
				grfx.DrawLine(Pens.Black, visBounds.Width / 2, yCoord, visBounds.Width, yCoord);
			}
		}

		private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
		{
			m_GrainPlayer.m_SimulatorPlaybackStyle = (GrainPlayer.SimulatorPlaybackStyle)simulatorPlaybackCombo.SelectedIndex;
		}

		private void crossfadePercentage_ValueChanged(object sender, EventArgs e)
		{
			m_GrainPlayer.m_CrossFadePercentage = (int)crossfadePercentage.Value;
		}

		private void comboBox1_SelectedIndexChanged_1(object sender, EventArgs e)
		{
			m_GrainPlayer.m_SimulatorGrainSelectionStyle = (GrainPlayer.SimulatorGrainSelectionStyle)grainSelectionMethodCombo.SelectedIndex;
		}

		private void comboBox1_SelectedIndexChanged_2(object sender, EventArgs e)
		{
			m_GrainPlayer.m_SampleInterpolationMethod = (GrainPlayer.SampleInterpolationMethod)sampleInterpolationCombo.SelectedIndex;
		}

		private void pitchScaleUpDown_ValueChanged(object sender, EventArgs e)
		{
			m_GrainPlayer.m_SimulatorPitchScale = (float)pitchScaleUpDown.Value;
		}
	}
}
