﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Wavelib;

namespace GranularPlayback
{
	public partial class SweepGenerator : Form
	{
		class RPMDataPoint
		{
			public float time;
			public float rpm;
			public bool selected;
		};

		List<RPMDataPoint> m_RPMValues;

		float m_DesiredSweepLength = 9.0f;
		float m_MaxRPM = 15000.0f;
		bool m_LeftMouseDown = false;

		RectangleF m_VisibleClipBounds;
		Point m_PrevMousePosition;

		public SweepGenerator()
		{
			InitializeComponent();

			m_RPMValues = new List<RPMDataPoint>();

			for (int loop = 0; loop <= m_DesiredSweepLength; loop++)
			{
				RPMDataPoint dataPoint = new RPMDataPoint();
				dataPoint.time = loop;
				dataPoint.rpm = 1000 + (loop * 1000.0f);
				dataPoint.selected = false;
				m_RPMValues.Add(dataPoint);
			}
		}

		private void sweepCurve_Paint(object sender, PaintEventArgs e)
		{
			var p = new Pen(new SolidBrush(Color.Crimson), 1.0f);
			DrawSweepCurve(e);
		}

		private void sweepCurve_MouseDown(object sender, MouseEventArgs e)
		{
			m_PrevMousePosition = e.Location;
			m_LeftMouseDown = true;

			foreach (RPMDataPoint dataPoint in m_RPMValues)
			{
				dataPoint.selected = false;
			}

			foreach (RPMDataPoint dataPoint in m_RPMValues)
			{
				float timeFraction = dataPoint.time / m_DesiredSweepLength;
				float rpmFraction = dataPoint.rpm / m_MaxRPM;

				int rectangleWidth = 10;
				Point thisPoint = new Point((int)(m_VisibleClipBounds.Width * timeFraction), (int)(m_VisibleClipBounds.Height - (m_VisibleClipBounds.Height * rpmFraction)));

				if(e.Location.X > thisPoint.X - rectangleWidth / 2 &&
				   e.Location.X < thisPoint.X + rectangleWidth / 2 &&
				   e.Location.Y < thisPoint.Y + rectangleWidth / 2 &&
				   e.Location.Y < thisPoint.Y + rectangleWidth / 2)
				{
					dataPoint.selected = true;
					selectedPointRPM.Value = new System.Decimal(dataPoint.rpm);
					selectedPointTime.Value = new System.Decimal(dataPoint.time);
					break;
				}
			}
		}

		private void sweepCurve_MouseUp(object sender, MouseEventArgs e)
		{
			m_LeftMouseDown = false;
		}

		private void sweepCurve_MouseMove(object sender, MouseEventArgs e)
		{
			int xDelta = e.Location.X - m_PrevMousePosition.X;
			int yDelta = e.Location.Y - m_PrevMousePosition.Y;

			float timeFraction = xDelta / m_VisibleClipBounds.Width;
			float timeChange = m_DesiredSweepLength * timeFraction;

			float rpmFraction = -(yDelta / m_VisibleClipBounds.Height);
			float rpmChange = m_MaxRPM * rpmFraction;

			foreach (RPMDataPoint dataPoint in m_RPMValues)
			{
				if (dataPoint.selected && m_LeftMouseDown)
				{
					dataPoint.rpm += rpmChange;

					if (dataPoint.rpm > m_MaxRPM)
					{
						dataPoint.rpm = m_MaxRPM;
					}
					if (dataPoint.rpm < 0)
					{
						dataPoint.rpm = 0;
					}

					// First/last points are clamped to each edge of the screen
					if(dataPoint != m_RPMValues.First() &&
						dataPoint != m_RPMValues.Last() )
					{
						dataPoint.time += timeChange;

						if (dataPoint.time > m_DesiredSweepLength)
						{
							dataPoint.time = m_DesiredSweepLength;
						}
						if (dataPoint.time < 0)
						{
							dataPoint.time = 0;
						}
					}

					selectedPointRPM.Value = new System.Decimal(dataPoint.rpm);
					selectedPointTime.Value = new System.Decimal(dataPoint.time);
				}
			}

			m_PrevMousePosition = e.Location;
		}

		private void sweepCurve_MouseLeave(object sender, EventArgs e)
		{
			m_LeftMouseDown = false;
		}

		private void timer1_Tick(object sender, EventArgs e)
		{
			sweepCurve.Refresh();
		}

		private void DrawSweepCurve(PaintEventArgs pea)
		{
			m_RPMValues.Sort(delegate(RPMDataPoint d1, RPMDataPoint d2) { return d1.time.CompareTo(d2.time); });
			
			var grfx = pea.Graphics;
			m_VisibleClipBounds = grfx.VisibleClipBounds;

			grfx.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb(128, 128, 128, 128)),
				0,
				0,
				m_VisibleClipBounds.Width,
				m_VisibleClipBounds.Height);

			
			Font arial = new Font( new FontFamily( "Arial" ), 10, FontStyle.Regular );
			SolidBrush textBrush = new SolidBrush(Color.Black);

			for(int loop = 0; loop <= m_DesiredSweepLength; loop++)
			{
				PointF point = new PointF((m_VisibleClipBounds.Width * loop / m_DesiredSweepLength) - 5, m_VisibleClipBounds.Height - 14);
				grfx.DrawString(loop.ToString(), arial, textBrush, point);
			}

			for (int loop = 1000; loop <= m_MaxRPM; loop += 1000)
			{
				PointF point = new PointF(1, m_VisibleClipBounds.Height - (m_VisibleClipBounds.Height * loop / m_MaxRPM));
				grfx.DrawString(loop.ToString(), arial, textBrush, point);
			}

			Point prevPoint = new Point();

			foreach(RPMDataPoint dataPoint in m_RPMValues)
			{
				float timeFraction = dataPoint.time / m_DesiredSweepLength;
				float rpmFraction = dataPoint.rpm / m_MaxRPM;

				int rectangleWidth = 10;
				Point thisPoint = new Point((int)(m_VisibleClipBounds.Width * timeFraction), (int)(m_VisibleClipBounds.Height - (m_VisibleClipBounds.Height * rpmFraction)));

				Pen pen = new Pen(Color.DarkOliveGreen);

				if(dataPoint.selected)
				{
					pen = new Pen(Color.Red);
				}

				if (m_LeftMouseDown && dataPoint.selected)
				{
					SolidBrush fillBrush = new SolidBrush(Color.Red);
					grfx.FillRectangle(fillBrush,
								 thisPoint.X - rectangleWidth / 2,
								 thisPoint.Y - rectangleWidth / 2,
								 rectangleWidth, rectangleWidth);
				}
				else
				{
					grfx.DrawRectangle(pen,
								 thisPoint.X - rectangleWidth / 2,
								 thisPoint.Y - rectangleWidth / 2,
								 rectangleWidth, rectangleWidth);
				}

				if (dataPoint != m_RPMValues.First())
				{
					grfx.DrawLine(new Pen(Color.DarkOrange), prevPoint, thisPoint);
				}

				prevPoint = thisPoint;
			}
		}

		private void numericUpDown1_ValueChanged(object sender, EventArgs e)
		{
			float sweepGrowthAmount = (float)sweepLengthUpDown.Value / m_DesiredSweepLength;

			foreach (RPMDataPoint dataPoint in m_RPMValues)
			{
				dataPoint.time *= sweepGrowthAmount;
			}

			m_DesiredSweepLength = (float)sweepLengthUpDown.Value;
		}

		private void addPointBtn_Click(object sender, EventArgs e)
		{
			int numPointsAfterAdd = m_RPMValues.Count + 1;
			float scaleFraction = 1.0f - (1.0f / numPointsAfterAdd);

			foreach (RPMDataPoint existingPoint in m_RPMValues)
			{
				if (existingPoint != m_RPMValues.First() &&
				   existingPoint != m_RPMValues.Last())
				{
					existingPoint.time *= scaleFraction;
					existingPoint.rpm *= scaleFraction;
				}
			}

			RPMDataPoint dataPoint = new RPMDataPoint();
			dataPoint.rpm = m_RPMValues.Last().rpm;
			dataPoint.time = (m_RPMValues.Last().time + m_RPMValues[m_RPMValues.Count - 2].time) / 2.0f;
			m_RPMValues.Add(dataPoint);
		}

		private void deletePointBtn_Click(object sender, EventArgs e)
		{
			if (m_RPMValues.Count > 2)
			{
				int numPointsAfterAdd = m_RPMValues.Count - 1;
				float scaleFraction = 1.0f + (1.0f / numPointsAfterAdd);

				foreach (RPMDataPoint existingPoint in m_RPMValues)
				{
					if (existingPoint != m_RPMValues.First() &&
					   existingPoint != m_RPMValues.Last())
					{
						existingPoint.time *= scaleFraction;
						existingPoint.rpm *= scaleFraction;

						if (existingPoint.rpm > m_MaxRPM)
						{
							existingPoint.rpm = m_MaxRPM;
						}
						if (existingPoint.time > m_DesiredSweepLength)
						{
							existingPoint.time = m_DesiredSweepLength;
						}
					}
				}

				m_RPMValues.RemoveAt(m_RPMValues.Count - 2);
			}
		}

		private void selectedPointTime_ValueChanged(object sender, EventArgs e)
		{
			foreach (RPMDataPoint existingPoint in m_RPMValues)
			{
				if(existingPoint.selected)
				{
					existingPoint.time = (float)selectedPointTime.Value;
				}
			}
		}

		private void selectedPointRPM_ValueChanged(object sender, EventArgs e)
		{
			foreach (RPMDataPoint existingPoint in m_RPMValues)
			{
				if (existingPoint.selected)
				{
					existingPoint.rpm = (float)selectedPointRPM.Value;
				}
			}
		}

		private void generateSweepBtn_Click(object sender, EventArgs e)
		{
			saveWaveDialog.ShowDialog();
		}

		private void saveWaveDialog_FileOk(object sender, CancelEventArgs e)
		{
			SaveFileDialog dialog = sender as SaveFileDialog;
			bwWaveFile waveFile = new bwWaveFile();

			int numSamples = (int)(48000 * m_DesiredSweepLength);
			List<byte> data = new List<byte>(numSamples * 2);// We're using shorts - so 2 bytes per sample
			float cycle = 0.0f;
			int currentRPMNode = 1;

			for (int i = 0; i < numSamples; i++)
			{
				float sampleTime = i / 48000.0f;

				while (sampleTime > m_RPMValues[currentRPMNode].time)
				{
					currentRPMNode++;
				}

				float timeFraction = (sampleTime - m_RPMValues[currentRPMNode - 1].time) / (m_RPMValues[currentRPMNode].time - m_RPMValues[currentRPMNode - 1].time);
				float desiredRPM = m_RPMValues[currentRPMNode - 1].rpm + ((m_RPMValues[currentRPMNode].rpm - m_RPMValues[currentRPMNode - 1].rpm) * timeFraction);
				float rotationsPerSecond = desiredRPM / 60.0f;
				float granulesPerSecond = rotationsPerSecond * 0.5f;
				float granulesPerSample = granulesPerSecond / 48000.0f;
				float radiansPerSample = granulesPerSample * 2.0f * (float)Math.PI * (int)numCylindersCombo.Value;
				cycle += radiansPerSample;

				if (cycle > 2.0f * Math.PI)
				{
					cycle -= 2.0f * (float)Math.PI;
				}

				short result = (short)(32000 * Math.Sin(cycle));
				data.AddRange(BitConverter.GetBytes(result));
			}

			bwDataChunk dataChunk = new bwDataChunk(data.ToArray(), (uint)numSamples);
			waveFile.Data = dataChunk;

			bwFormatChunk formatChunk = new bwFormatChunk(1, 1, 48000, 96000, 2, 16, new byte[0]);
			waveFile.Format = formatChunk;

			waveFile.Save(dialog.FileName);
		}
	}
}
