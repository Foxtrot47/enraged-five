﻿namespace GranularPlayback
{
	partial class EngineSimulator
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.throttleSlider = new System.Windows.Forms.VScrollBar();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.intertiaSlider = new System.Windows.Forms.VScrollBar();
			this.simUpdateTimer = new System.Windows.Forms.Timer(this.components);
			this.minRPMUpDown = new System.Windows.Forms.NumericUpDown();
			this.maxRPMUpDown = new System.Windows.Forms.NumericUpDown();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.playButton = new System.Windows.Forms.Button();
			this.RPMGraph = new System.Windows.Forms.PictureBox();
			this.rpmViewAxis = new System.Windows.Forms.PictureBox();
			this.changeRateForMaxGrainsUpDown = new System.Windows.Forms.NumericUpDown();
			this.changeRateForMaxLoopsUpDown = new System.Windows.Forms.NumericUpDown();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.simulatorPlaybackCombo = new System.Windows.Forms.ComboBox();
			this.label7 = new System.Windows.Forms.Label();
			this.crossfadePercentage = new System.Windows.Forms.NumericUpDown();
			this.label8 = new System.Windows.Forms.Label();
			this.grainSelectionMethodCombo = new System.Windows.Forms.ComboBox();
			this.label9 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.sampleInterpolationCombo = new System.Windows.Forms.ComboBox();
			this.label11 = new System.Windows.Forms.Label();
			this.pitchScaleUpDown = new System.Windows.Forms.NumericUpDown();
			((System.ComponentModel.ISupportInitialize)(this.minRPMUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.maxRPMUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.RPMGraph)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.rpmViewAxis)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.changeRateForMaxGrainsUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.changeRateForMaxLoopsUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.crossfadePercentage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pitchScaleUpDown)).BeginInit();
			this.SuspendLayout();
			// 
			// throttleSlider
			// 
			this.throttleSlider.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.throttleSlider.Location = new System.Drawing.Point(1203, 25);
			this.throttleSlider.Maximum = 110;
			this.throttleSlider.Name = "throttleSlider";
			this.throttleSlider.Size = new System.Drawing.Size(21, 407);
			this.throttleSlider.TabIndex = 0;
			this.throttleSlider.Value = 100;
			// 
			// label1
			// 
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(1193, 442);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(43, 13);
			this.label1.TabIndex = 1;
			this.label1.Text = "Throttle";
			// 
			// label2
			// 
			this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(1151, 442);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(36, 13);
			this.label2.TabIndex = 3;
			this.label2.Text = "Inertia";
			// 
			// intertiaSlider
			// 
			this.intertiaSlider.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.intertiaSlider.Location = new System.Drawing.Point(1154, 25);
			this.intertiaSlider.Name = "intertiaSlider";
			this.intertiaSlider.Size = new System.Drawing.Size(21, 407);
			this.intertiaSlider.TabIndex = 2;
			this.intertiaSlider.Value = 100;
			// 
			// simUpdateTimer
			// 
			this.simUpdateTimer.Enabled = true;
			this.simUpdateTimer.Interval = 16;
			this.simUpdateTimer.Tick += new System.EventHandler(this.simUpdateTimer_Tick);
			// 
			// minRPMUpDown
			// 
			this.minRPMUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.minRPMUpDown.Location = new System.Drawing.Point(1138, 475);
			this.minRPMUpDown.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
			this.minRPMUpDown.Name = "minRPMUpDown";
			this.minRPMUpDown.Size = new System.Drawing.Size(92, 20);
			this.minRPMUpDown.TabIndex = 4;
			this.minRPMUpDown.Value = new decimal(new int[] {
            1500,
            0,
            0,
            0});
			// 
			// maxRPMUpDown
			// 
			this.maxRPMUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.maxRPMUpDown.Location = new System.Drawing.Point(1138, 501);
			this.maxRPMUpDown.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
			this.maxRPMUpDown.Name = "maxRPMUpDown";
			this.maxRPMUpDown.Size = new System.Drawing.Size(92, 20);
			this.maxRPMUpDown.TabIndex = 5;
			this.maxRPMUpDown.Value = new decimal(new int[] {
            9000,
            0,
            0,
            0});
			// 
			// label3
			// 
			this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(1078, 477);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(51, 13);
			this.label3.TabIndex = 6;
			this.label3.Text = "Min RPM";
			// 
			// label4
			// 
			this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(1078, 503);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(54, 13);
			this.label4.TabIndex = 7;
			this.label4.Text = "Max RPM";
			// 
			// playButton
			// 
			this.playButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.playButton.Location = new System.Drawing.Point(56, 461);
			this.playButton.Name = "playButton";
			this.playButton.Size = new System.Drawing.Size(139, 64);
			this.playButton.TabIndex = 8;
			this.playButton.Text = "Play";
			this.playButton.UseVisualStyleBackColor = true;
			this.playButton.Click += new System.EventHandler(this.playButton_Click);
			// 
			// RPMGraph
			// 
			this.RPMGraph.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.RPMGraph.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.RPMGraph.Location = new System.Drawing.Point(56, 25);
			this.RPMGraph.Name = "RPMGraph";
			this.RPMGraph.Size = new System.Drawing.Size(1082, 430);
			this.RPMGraph.TabIndex = 9;
			this.RPMGraph.TabStop = false;
			this.RPMGraph.Paint += new System.Windows.Forms.PaintEventHandler(this.RPMGraph_Paint);
			// 
			// rpmViewAxis
			// 
			this.rpmViewAxis.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)));
			this.rpmViewAxis.Location = new System.Drawing.Point(12, 25);
			this.rpmViewAxis.Name = "rpmViewAxis";
			this.rpmViewAxis.Size = new System.Drawing.Size(47, 430);
			this.rpmViewAxis.TabIndex = 10;
			this.rpmViewAxis.TabStop = false;
			this.rpmViewAxis.Paint += new System.Windows.Forms.PaintEventHandler(this.RPMAxis_Paint);
			// 
			// changeRateForMaxGrainsUpDown
			// 
			this.changeRateForMaxGrainsUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.changeRateForMaxGrainsUpDown.Location = new System.Drawing.Point(348, 475);
			this.changeRateForMaxGrainsUpDown.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
			this.changeRateForMaxGrainsUpDown.Name = "changeRateForMaxGrainsUpDown";
			this.changeRateForMaxGrainsUpDown.Size = new System.Drawing.Size(92, 20);
			this.changeRateForMaxGrainsUpDown.TabIndex = 13;
			this.changeRateForMaxGrainsUpDown.Value = new decimal(new int[] {
            6000,
            0,
            0,
            0});
			// 
			// changeRateForMaxLoopsUpDown
			// 
			this.changeRateForMaxLoopsUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.changeRateForMaxLoopsUpDown.Location = new System.Drawing.Point(348, 498);
			this.changeRateForMaxLoopsUpDown.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
			this.changeRateForMaxLoopsUpDown.Name = "changeRateForMaxLoopsUpDown";
			this.changeRateForMaxLoopsUpDown.Size = new System.Drawing.Size(92, 20);
			this.changeRateForMaxLoopsUpDown.TabIndex = 14;
			this.changeRateForMaxLoopsUpDown.Value = new decimal(new int[] {
            3000,
            0,
            0,
            0});
			// 
			// label5
			// 
			this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(201, 500);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(140, 13);
			this.label5.TabIndex = 15;
			this.label5.Text = "Change Rate for Max Loops";
			// 
			// label6
			// 
			this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(201, 477);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(141, 13);
			this.label6.TabIndex = 16;
			this.label6.Text = "Change Rate for Max Grains";
			// 
			// simulatorPlaybackCombo
			// 
			this.simulatorPlaybackCombo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.simulatorPlaybackCombo.FormattingEnabled = true;
			this.simulatorPlaybackCombo.Items.AddRange(new object[] {
            "Loops and Grains",
            "Loops Only",
            "Grains Only"});
			this.simulatorPlaybackCombo.Location = new System.Drawing.Point(542, 474);
			this.simulatorPlaybackCombo.Name = "simulatorPlaybackCombo";
			this.simulatorPlaybackCombo.Size = new System.Drawing.Size(113, 21);
			this.simulatorPlaybackCombo.TabIndex = 17;
			this.simulatorPlaybackCombo.Text = "Loops and Grains";
			this.simulatorPlaybackCombo.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
			// 
			// label7
			// 
			this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(446, 477);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(90, 13);
			this.label7.TabIndex = 18;
			this.label7.Text = "Playback Method";
			// 
			// crossfadePercentage
			// 
			this.crossfadePercentage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.crossfadePercentage.Location = new System.Drawing.Point(564, 500);
			this.crossfadePercentage.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
			this.crossfadePercentage.Name = "crossfadePercentage";
			this.crossfadePercentage.Size = new System.Drawing.Size(91, 20);
			this.crossfadePercentage.TabIndex = 19;
			this.crossfadePercentage.ValueChanged += new System.EventHandler(this.crossfadePercentage_ValueChanged);
			// 
			// label8
			// 
			this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(446, 503);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(112, 13);
			this.label8.TabIndex = 20;
			this.label8.Text = "Crossfade Percentage";
			// 
			// grainSelectionMethodCombo
			// 
			this.grainSelectionMethodCombo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.grainSelectionMethodCombo.FormattingEnabled = true;
			this.grainSelectionMethodCombo.Items.AddRange(new object[] {
            "Fraction between closest loops",
            "Fraction through sweep",
            "Closest pitch match"});
			this.grainSelectionMethodCombo.Location = new System.Drawing.Point(785, 474);
			this.grainSelectionMethodCombo.Name = "grainSelectionMethodCombo";
			this.grainSelectionMethodCombo.Size = new System.Drawing.Size(173, 21);
			this.grainSelectionMethodCombo.TabIndex = 21;
			this.grainSelectionMethodCombo.Text = "Fraction between closest loops";
			this.grainSelectionMethodCombo.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged_1);
			// 
			// label9
			// 
			this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(661, 477);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(118, 13);
			this.label9.TabIndex = 22;
			this.label9.Text = "Grain Selection Method";
			// 
			// label10
			// 
			this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(676, 503);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(103, 13);
			this.label10.TabIndex = 23;
			this.label10.Text = "Sample Interpolation";
			// 
			// sampleInterpolationCombo
			// 
			this.sampleInterpolationCombo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.sampleInterpolationCombo.FormattingEnabled = true;
			this.sampleInterpolationCombo.Items.AddRange(new object[] {
            "Linear",
            "Cosine",
            "Cubic",
            "Sinc"});
			this.sampleInterpolationCombo.Location = new System.Drawing.Point(785, 500);
			this.sampleInterpolationCombo.Name = "sampleInterpolationCombo";
			this.sampleInterpolationCombo.Size = new System.Drawing.Size(64, 21);
			this.sampleInterpolationCombo.TabIndex = 24;
			this.sampleInterpolationCombo.Text = "Linear";
			this.sampleInterpolationCombo.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged_2);
			// 
			// label11
			// 
			this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point(855, 503);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(61, 13);
			this.label11.TabIndex = 26;
			this.label11.Text = "Pitch Scale";
			// 
			// pitchScaleUpDown
			// 
			this.pitchScaleUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.pitchScaleUpDown.DecimalPlaces = 2;
			this.pitchScaleUpDown.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
			this.pitchScaleUpDown.Location = new System.Drawing.Point(922, 501);
			this.pitchScaleUpDown.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
			this.pitchScaleUpDown.Name = "pitchScaleUpDown";
			this.pitchScaleUpDown.Size = new System.Drawing.Size(91, 20);
			this.pitchScaleUpDown.TabIndex = 25;
			this.pitchScaleUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.pitchScaleUpDown.ValueChanged += new System.EventHandler(this.pitchScaleUpDown_ValueChanged);
			// 
			// EngineSimulator
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1242, 531);
			this.Controls.Add(this.label11);
			this.Controls.Add(this.pitchScaleUpDown);
			this.Controls.Add(this.sampleInterpolationCombo);
			this.Controls.Add(this.label10);
			this.Controls.Add(this.label9);
			this.Controls.Add(this.grainSelectionMethodCombo);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.crossfadePercentage);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.simulatorPlaybackCombo);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.changeRateForMaxLoopsUpDown);
			this.Controls.Add(this.changeRateForMaxGrainsUpDown);
			this.Controls.Add(this.RPMGraph);
			this.Controls.Add(this.rpmViewAxis);
			this.Controls.Add(this.playButton);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.maxRPMUpDown);
			this.Controls.Add(this.minRPMUpDown);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.intertiaSlider);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.throttleSlider);
			this.Name = "EngineSimulator";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Engine Simulator";
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.window_Closed);
			((System.ComponentModel.ISupportInitialize)(this.minRPMUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.maxRPMUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.RPMGraph)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.rpmViewAxis)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.changeRateForMaxGrainsUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.changeRateForMaxLoopsUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.crossfadePercentage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pitchScaleUpDown)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.VScrollBar throttleSlider;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.VScrollBar intertiaSlider;
		private System.Windows.Forms.Timer simUpdateTimer;
		private System.Windows.Forms.NumericUpDown minRPMUpDown;
		private System.Windows.Forms.NumericUpDown maxRPMUpDown;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Button playButton;
		private System.Windows.Forms.PictureBox RPMGraph;
		private System.Windows.Forms.PictureBox rpmViewAxis;
		private System.Windows.Forms.NumericUpDown changeRateForMaxGrainsUpDown;
		private System.Windows.Forms.NumericUpDown changeRateForMaxLoopsUpDown;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.ComboBox simulatorPlaybackCombo;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.NumericUpDown crossfadePercentage;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.ComboBox grainSelectionMethodCombo;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.ComboBox sampleInterpolationCombo;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.NumericUpDown pitchScaleUpDown;
	}
}