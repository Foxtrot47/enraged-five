﻿namespace GranularPlayback
{
	partial class PitchAnalysis
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.fourierTransform = new System.Windows.Forms.PictureBox();
			this.redrawTimer = new System.Windows.Forms.Timer(this.components);
			this.wavePreviewBox = new System.Windows.Forms.PictureBox();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.wavePreviewAxis = new System.Windows.Forms.PictureBox();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.fourierXAxis = new System.Windows.Forms.PictureBox();
			this.trackPitchBtn = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.fourierScaleUpDown = new System.Windows.Forms.NumericUpDown();
			this.numCylindersUpDown = new System.Windows.Forms.NumericUpDown();
			this.label2 = new System.Windows.Forms.Label();
			this.upperBoundUpDown = new System.Windows.Forms.NumericUpDown();
			this.label3 = new System.Windows.Forms.Label();
			this.lowerBoundLabel = new System.Windows.Forms.Label();
			this.lowerBoundUpDown = new System.Windows.Forms.NumericUpDown();
			this.label4 = new System.Windows.Forms.Label();
			this.frameSizeUpDown = new System.Windows.Forms.NumericUpDown();
			this.label5 = new System.Windows.Forms.Label();
			this.stepSizeUpDown = new System.Windows.Forms.NumericUpDown();
			this.suggestFrequencyBtn = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.fourierTransform)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.wavePreviewBox)).BeginInit();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.wavePreviewAxis)).BeginInit();
			this.groupBox2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fourierXAxis)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fourierScaleUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numCylindersUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.upperBoundUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lowerBoundUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.frameSizeUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.stepSizeUpDown)).BeginInit();
			this.SuspendLayout();
			// 
			// fourierTransform
			// 
			this.fourierTransform.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.fourierTransform.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.fourierTransform.Location = new System.Drawing.Point(8, 19);
			this.fourierTransform.Name = "fourierTransform";
			this.fourierTransform.Size = new System.Drawing.Size(1025, 374);
			this.fourierTransform.TabIndex = 0;
			this.fourierTransform.TabStop = false;
			this.fourierTransform.MouseMove += new System.Windows.Forms.MouseEventHandler(this.fourierTransform_MouseMove);
			this.fourierTransform.MouseDown += new System.Windows.Forms.MouseEventHandler(this.fourierTransform_MouseDown);
			this.fourierTransform.Paint += new System.Windows.Forms.PaintEventHandler(this.fourierTransform_Paint);
			this.fourierTransform.MouseUp += new System.Windows.Forms.MouseEventHandler(this.fourierTransform_MouseUp);
			// 
			// redrawTimer
			// 
			this.redrawTimer.Enabled = true;
			this.redrawTimer.Interval = 10;
			this.redrawTimer.Tick += new System.EventHandler(this.redrawTimer_Tick);
			// 
			// wavePreviewBox
			// 
			this.wavePreviewBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.wavePreviewBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.wavePreviewBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.wavePreviewBox.Location = new System.Drawing.Point(6, 19);
			this.wavePreviewBox.Name = "wavePreviewBox";
			this.wavePreviewBox.Size = new System.Drawing.Size(1027, 81);
			this.wavePreviewBox.TabIndex = 1;
			this.wavePreviewBox.TabStop = false;
			this.wavePreviewBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.wavePreview_MouseMove);
			this.wavePreviewBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.wavePreview_MouseDown);
			this.wavePreviewBox.Paint += new System.Windows.Forms.PaintEventHandler(this.wavePreview_Paint);
			this.wavePreviewBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.wavePreview_MouseUp);
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.wavePreviewBox);
			this.groupBox1.Controls.Add(this.wavePreviewAxis);
			this.groupBox1.Location = new System.Drawing.Point(12, 443);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(1033, 128);
			this.groupBox1.TabIndex = 2;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Wave Preview";
			// 
			// wavePreviewAxis
			// 
			this.wavePreviewAxis.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.wavePreviewAxis.Location = new System.Drawing.Point(6, 97);
			this.wavePreviewAxis.Name = "wavePreviewAxis";
			this.wavePreviewAxis.Size = new System.Drawing.Size(1027, 25);
			this.wavePreviewAxis.TabIndex = 2;
			this.wavePreviewAxis.TabStop = false;
			this.wavePreviewAxis.Paint += new System.Windows.Forms.PaintEventHandler(this.wavePreviewAxis_Paint);
			// 
			// groupBox2
			// 
			this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox2.Controls.Add(this.fourierTransform);
			this.groupBox2.Controls.Add(this.fourierXAxis);
			this.groupBox2.Location = new System.Drawing.Point(12, 12);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(1033, 426);
			this.groupBox2.TabIndex = 3;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Fourier Transform - Amplitude vs. Frequency (Hz)";
			// 
			// fourierXAxis
			// 
			this.fourierXAxis.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.fourierXAxis.Location = new System.Drawing.Point(8, 389);
			this.fourierXAxis.Name = "fourierXAxis";
			this.fourierXAxis.Size = new System.Drawing.Size(1025, 36);
			this.fourierXAxis.TabIndex = 1;
			this.fourierXAxis.TabStop = false;
			this.fourierXAxis.Paint += new System.Windows.Forms.PaintEventHandler(this.fourierXAxis_Paint);
			// 
			// trackPitchBtn
			// 
			this.trackPitchBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.trackPitchBtn.Location = new System.Drawing.Point(18, 584);
			this.trackPitchBtn.Name = "trackPitchBtn";
			this.trackPitchBtn.Size = new System.Drawing.Size(142, 61);
			this.trackPitchBtn.TabIndex = 1;
			this.trackPitchBtn.Text = "Track Pitch";
			this.trackPitchBtn.UseVisualStyleBackColor = true;
			this.trackPitchBtn.Click += new System.EventHandler(this.trackPitchBtn_Click);
			// 
			// label1
			// 
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(883, 620);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(69, 13);
			this.label1.TabIndex = 2;
			this.label1.Text = "Fourier Scale";
			// 
			// fourierScaleUpDown
			// 
			this.fourierScaleUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.fourierScaleUpDown.DecimalPlaces = 1;
			this.fourierScaleUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.fourierScaleUpDown.Location = new System.Drawing.Point(958, 618);
			this.fourierScaleUpDown.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
			this.fourierScaleUpDown.Name = "fourierScaleUpDown";
			this.fourierScaleUpDown.Size = new System.Drawing.Size(85, 20);
			this.fourierScaleUpDown.TabIndex = 3;
			this.fourierScaleUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// numCylindersUpDown
			// 
			this.numCylindersUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.numCylindersUpDown.Location = new System.Drawing.Point(573, 618);
			this.numCylindersUpDown.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
			this.numCylindersUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.numCylindersUpDown.Name = "numCylindersUpDown";
			this.numCylindersUpDown.Size = new System.Drawing.Size(50, 20);
			this.numCylindersUpDown.TabIndex = 2;
			this.numCylindersUpDown.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
			this.numCylindersUpDown.ValueChanged += new System.EventHandler(this.numCylindersUpDown_ValueChanged);
			// 
			// label2
			// 
			this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(493, 620);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(74, 13);
			this.label2.TabIndex = 7;
			this.label2.Text = "Num Cylinders";
			// 
			// upperBoundUpDown
			// 
			this.upperBoundUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.upperBoundUpDown.Location = new System.Drawing.Point(266, 618);
			this.upperBoundUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.upperBoundUpDown.Name = "upperBoundUpDown";
			this.upperBoundUpDown.Size = new System.Drawing.Size(55, 20);
			this.upperBoundUpDown.TabIndex = 8;
			this.upperBoundUpDown.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
			// 
			// label3
			// 
			this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(168, 620);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(92, 13);
			this.label3.TabIndex = 9;
			this.label3.Text = "Upper Bound (Hz)";
			// 
			// lowerBoundLabel
			// 
			this.lowerBoundLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.lowerBoundLabel.AutoSize = true;
			this.lowerBoundLabel.Location = new System.Drawing.Point(168, 594);
			this.lowerBoundLabel.Name = "lowerBoundLabel";
			this.lowerBoundLabel.Size = new System.Drawing.Size(92, 13);
			this.lowerBoundLabel.TabIndex = 11;
			this.lowerBoundLabel.Text = "Lower Bound (Hz)";
			// 
			// lowerBoundUpDown
			// 
			this.lowerBoundUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.lowerBoundUpDown.Location = new System.Drawing.Point(264, 592);
			this.lowerBoundUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.lowerBoundUpDown.Name = "lowerBoundUpDown";
			this.lowerBoundUpDown.Size = new System.Drawing.Size(57, 20);
			this.lowerBoundUpDown.TabIndex = 10;
			this.lowerBoundUpDown.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
			// 
			// label4
			// 
			this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(345, 620);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(81, 13);
			this.label4.TabIndex = 11;
			this.label4.Text = "Frame Size (ms)";
			// 
			// frameSizeUpDown
			// 
			this.frameSizeUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.frameSizeUpDown.Location = new System.Drawing.Point(432, 618);
			this.frameSizeUpDown.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.frameSizeUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.frameSizeUpDown.Name = "frameSizeUpDown";
			this.frameSizeUpDown.Size = new System.Drawing.Size(55, 20);
			this.frameSizeUpDown.TabIndex = 10;
			this.frameSizeUpDown.Value = new decimal(new int[] {
            80,
            0,
            0,
            0});
			// 
			// label5
			// 
			this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(327, 594);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(99, 13);
			this.label5.TabIndex = 13;
			this.label5.Text = "Step Size (samples)";
			// 
			// stepSizeUpDown
			// 
			this.stepSizeUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.stepSizeUpDown.Location = new System.Drawing.Point(432, 592);
			this.stepSizeUpDown.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.stepSizeUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.stepSizeUpDown.Name = "stepSizeUpDown";
			this.stepSizeUpDown.Size = new System.Drawing.Size(55, 20);
			this.stepSizeUpDown.TabIndex = 12;
			this.stepSizeUpDown.Value = new decimal(new int[] {
            480,
            0,
            0,
            0});
			// 
			// suggestFrequencyBtn
			// 
			this.suggestFrequencyBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.suggestFrequencyBtn.Location = new System.Drawing.Point(886, 584);
			this.suggestFrequencyBtn.Name = "suggestFrequencyBtn";
			this.suggestFrequencyBtn.Size = new System.Drawing.Size(158, 27);
			this.suggestFrequencyBtn.TabIndex = 14;
			this.suggestFrequencyBtn.Text = "Suggest Frequency";
			this.suggestFrequencyBtn.UseVisualStyleBackColor = true;
			this.suggestFrequencyBtn.Click += new System.EventHandler(this.suggestFrequencyBtn_Click);
			// 
			// PitchAnalysis
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1063, 657);
			this.Controls.Add(this.suggestFrequencyBtn);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.stepSizeUpDown);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.lowerBoundLabel);
			this.Controls.Add(this.frameSizeUpDown);
			this.Controls.Add(this.lowerBoundUpDown);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.upperBoundUpDown);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.numCylindersUpDown);
			this.Controls.Add(this.fourierScaleUpDown);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.trackPitchBtn);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Name = "PitchAnalysis";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Pitch Analysis";
			((System.ComponentModel.ISupportInitialize)(this.fourierTransform)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.wavePreviewBox)).EndInit();
			this.groupBox1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.wavePreviewAxis)).EndInit();
			this.groupBox2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fourierXAxis)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fourierScaleUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numCylindersUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.upperBoundUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lowerBoundUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.frameSizeUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.stepSizeUpDown)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.PictureBox fourierTransform;
		private System.Windows.Forms.Timer redrawTimer;
		private System.Windows.Forms.PictureBox wavePreviewBox;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Button trackPitchBtn;
		private System.Windows.Forms.PictureBox fourierXAxis;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.NumericUpDown fourierScaleUpDown;
		private System.Windows.Forms.NumericUpDown numCylindersUpDown;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.PictureBox wavePreviewAxis;
		private System.Windows.Forms.NumericUpDown upperBoundUpDown;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label lowerBoundLabel;
		private System.Windows.Forms.NumericUpDown lowerBoundUpDown;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.NumericUpDown frameSizeUpDown;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.NumericUpDown stepSizeUpDown;
		private System.Windows.Forms.Button suggestFrequencyBtn;
	}
}