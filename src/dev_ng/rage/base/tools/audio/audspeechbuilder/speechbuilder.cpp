//
// tools/audspeechbuilder/speechbuilder.cpp
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved.
//
// This is the main DLL file.
//

#include "stdafx.h"

#include "speechbuilder.h"

#include "audbuildmanager/builddefs.h"

namespace audSpeechBuild {

const int g_SpeechLookupFileBufferBytes = 50*1024*1024;

audSpeechBuilder::audSpeechBuilder()
{
}

void audSpeechBuilder::Build(audAssetManager *assetManager, audProjectSettings *projectSettings,
	String *buildPlatform, audBuildClient *buildClient, XmlDocument *builtWavesXml, XmlDocument *, bool shouldBuildLocally,
	audBuildComponent * buildComponent, bool)
{
	m_AssetManager = assetManager;
	m_ProjectSettings = projectSettings;
	m_BuildPlatform = buildPlatform;
	m_BuildClient = buildClient;
	m_BuiltWavesXml = builtWavesXml;
	(shouldBuildLocally);
	m_ExcludeList = new ArrayList();
    m_IncludeList = new ArrayList();
	m_VoiceTable = new Hashtable();
	m_ContextTable = new Hashtable();

	m_TotalNumContexts = 0;
	m_TotalNumVariationDataElements = 0;
	m_OutputPath = buildComponent->m_OutputPath;
	if(!m_OutputPath)
	{
		m_OutputPath = g_SpeechLookupFilename;
	}
	m_VoiceOutput = buildComponent->m_SecondOutputPath;
	m_ContextOutput = buildComponent->m_ThirdOutputPath;

	if(buildComponent->m_InputPath)
	{
		try
		{
			XmlDocument *doc = new XmlDocument();
			doc->Load(buildComponent->m_InputPath);

			for( int i =0; i<doc->DocumentElement->ChildNodes->Count; i++)
			{
				XmlNode *childNode = doc->DocumentElement->ChildNodes->Item(i);
				if(childNode->Name->Equals(S"Include"))
				{
					m_IncludeList->Add(childNode->InnerText);
				}
				else if(childNode->Name->Equals(S"Exclude"))
				{
					m_ExcludeList->Add(childNode->InnerText);
				}
			}
		}

		catch(Exception *e)
		{
			throw new audBuildException(e->ToString());
		}
	}

	try
	{
		m_BuildClient->ReportProgress(-1, -1, S"Starting Speech build", true);

		Init();
		ParseBuiltWavesXml();
		PrepareAssets();
		WriteVoiceLookupFile();
		WriteContextLookupFile();
		WriteSpeechLookupTables();
		CommitAssets();
		Shutdown();

		m_BuildClient->ReportProgress(-1, -1, S"Finished Speech build", true);
	}
	catch(audBuildException *e)
	{
		ReleaseAssets();
		Shutdown();

		//Pass internal exceptions upwards to Build Manager.
		throw(e);
	}
	catch(Exception *e)
	{
		ReleaseAssets();
		Shutdown();

		//Pass internal exceptions upwards to Build Manager.
		audBuildException *exception = new audBuildException(
			String::Concat(S"Error building Speech Lookup Tables - ", e->ToString()));
		throw(exception);
	}
}

void audSpeechBuilder::Init(void)
{
	CErrorManager::InitClass(false);
	audPlatformSpecific::InitClass(m_BuildPlatform);

	m_VoiceLookupList = new ArrayList();
	m_ObjectCompiler = new CObjectCompiler(audPlatformSpecific::IsPlatformBigEndian(), "", "", m_ProjectSettings->Use3ByteNameTableOffset(),"");
	m_ObjectCompiler->Init();

	//Generate commonly-used paths.
	m_AssetBuildDataPath = m_ProjectSettings->GetBuildOutputPath();
	m_LocalBuildDataPath = m_AssetManager->GetWorkingPath(m_AssetBuildDataPath);
	if(m_LocalBuildDataPath == 0)
	{
		audBuildException *exception = new audBuildException(S"Failed to find working path for Build Data folder");
		throw(exception);
	}
}

void audSpeechBuilder::ParseBuiltWavesXml(void)
{
	XmlNode *builtNode = m_BuiltWavesXml->DocumentElement->FirstChild;
	while(builtNode)
	{
		ParseBankNodes(builtNode, S"");
		builtNode = builtNode->NextSibling;
	}
}

void audSpeechBuilder::ParseBankNodes(XmlNode *node, String *packName)
{
	if(node->Name->Equals(S"Bank"))
	{
		XmlAttribute *nameAttribute = node->Attributes->get_ItemOf(S"name");
		if(nameAttribute)
		{
			String *bankName = String::Concat(packName, S"\\", nameAttribute->Value);

			//Search for voice tags beneath this node.
			ParseSubBankNodes(node, packName, bankName);
		}
	}
	else
	{
		if(node->Name->Equals(S"Pack"))
		{
			XmlAttribute *nameAttribute = node->Attributes->get_ItemOf(S"name");
			if(nameAttribute)
			{
				packName = nameAttribute->Value;
			}
		}

		XmlNode *childNode = node->FirstChild;
		while(childNode)
		{
			ParseBankNodes(childNode, packName);
			childNode = childNode->NextSibling;
		}
	}
}

void audSpeechBuilder::ParseSubBankNodes(XmlNode *node, String *packName, String *bankName)
{
	//Check if we have a voice tag as an immediate child.
	String *voiceName = FindVoiceTag(node);
	if(voiceName)
	{
		bool bAllow = true;;
		//check include List
		if(!m_IncludeList->Count==0)
		{
			bool found = false;
			IEnumerator* includeEnum = m_IncludeList->GetEnumerator();
			while(includeEnum->MoveNext())
			{
				if(packName->StartsWith(__try_cast<String*>(includeEnum->Current)))
				{
					found = true;
					break;
				}
			}
			if(!found)
			{
				bAllow=false;
			}
		}
		//Check Exlude List
		if(bAllow && m_ExcludeList->Count !=0)
		{
			bool found = false;
			IEnumerator* excludeEnum = m_ExcludeList->GetEnumerator();
			while(excludeEnum->MoveNext())
			{
				if(packName->StartsWith(__try_cast<String*>(excludeEnum->Current)))
				{
					found = true;
					break;
				}
			}
			if(found)
			{
				bAllow=false;
			}
		}
		//allow is in include list or no include list and not in exclude list or no exclude list
		if(bAllow)
		{
			u32 nameHash = m_ObjectCompiler->ComputeHash(voiceName);
			if(!m_VoiceTable->ContainsKey(voiceName))
			{
				m_VoiceTable->Add(voiceName,nameHash.ToString());
			}
			//Check if we have already added this voice to the lookup table (for another bank.)
			audVoiceLookupEntryManaged *currentVoiceEntry = 0;
			IEnumerator* voiceEnum = m_VoiceLookupList->GetEnumerator();
			while(voiceEnum->MoveNext())
			{
				audVoiceLookupEntryManaged *voiceEntry = __try_cast<audVoiceLookupEntryManaged *>(voiceEnum->Current);
				if(voiceEntry->m_Entry.nameHash == nameHash)
				{
					//Found it, so add to this voice's context table.
					currentVoiceEntry = voiceEntry;
				}
			}

			if(!currentVoiceEntry)
			{
				//This is a new voice, so create and initialize an entry for it.
				currentVoiceEntry = new audVoiceLookupEntryManaged();

				currentVoiceEntry->m_Entry.nameHash = nameHash;
				currentVoiceEntry->contextList = new ArrayList();
				currentVoiceEntry->m_Entry.numContexts = 0;

				m_VoiceLookupList->Add(currentVoiceEntry);
			}

			AddVoiceContextsToTable(node, bankName, currentVoiceEntry->contextList);
		}
	}
	else
	{
		//This node does not have a voice tag so parse sub nodes
		XmlNode *childNode = node->FirstChild;
		while(childNode)
		{
			ParseSubBankNodes(childNode, packName, bankName);
			childNode = childNode->NextSibling;
		}
	}

}

String *audSpeechBuilder::FindVoiceTag(XmlNode *parentNode)
{
	String *voiceName = 0;

	XmlNode *childNode = parentNode->FirstChild;
	while(childNode)
	{
		if(childNode->Name->Equals(S"Tag"))
		{
			XmlAttribute *nameAttribute = childNode->Attributes->get_ItemOf(S"name");
			if(nameAttribute)
			{
				if(nameAttribute->Value->Equals(S"voice"))
				{
					XmlAttribute *valueAttribute = childNode->Attributes->get_ItemOf(S"value");
					if(valueAttribute)
					{
						voiceName = valueAttribute->Value;
						break;
					}
				}
			}
		}

		childNode = childNode->NextSibling;
	}

	return voiceName;
}

void audSpeechBuilder::AddVoiceContextsToTable(XmlNode *parentNode, String *bankName, ArrayList *contextList)
{
	
	String* delimStr = S"_";
	Char delimiter[] = delimStr->ToCharArray();
	String* delimStrDot = S".";
	Char delimiterDot[] = delimStrDot->ToCharArray();

	XmlNode *childNode = parentNode->FirstChild;
	while(childNode)
	{
		if(childNode->Name->Equals(S"Wave"))
		{
			XmlAttribute *nameAttribute = childNode->Attributes->get_ItemOf(S"name");
			if(nameAttribute)
			{
				String *waveName = nameAttribute->Value;

				//Extract context name from wave name using '_' delimiter.
				String *splitName[] = waveName->Split(delimiter);
				String *contextName = S"";
				for(int i=0; i<splitName->Count - 1; i++)
				{
					contextName = String::Concat(contextName, S"_", splitName[i]);
				}

				//Extract variation name from separated wave name using '.' delimiter.
				String *splitVariation[] = splitName[splitName->Count - 1]->Split(delimiterDot);
				String *variationName = splitVariation[0];

				u8 variationNumber;
				try
				{
					variationNumber = Byte::Parse(variationName);	
				}
				catch(FormatException *e)
				{
					e = NULL;
					variationNumber = 1;
				}
				catch(Exception *e)
				{
					e = NULL;
					String *errorString = S"Speech Build Error: Invalid variation number (";
					errorString = String::Concat(errorString, bankName, S"\\", waveName, S")");
					audBuildException *exception = new audBuildException(errorString);
					throw(exception);
				}

				u32 nameHash = m_ObjectCompiler->ComputeHash(contextName->Substring(1));

				//Check if we have already added this context to the lookup table.
				audVoiceContextLookupEntryManaged *currentContextEntry = 0;
				IEnumerator* contextEnum = contextList->GetEnumerator();
				while(contextEnum->MoveNext())
				{
					audVoiceContextLookupEntryManaged *contextEntry =
						__try_cast<audVoiceContextLookupEntryManaged *>(contextEnum->Current);
					if(contextEntry->m_Entry.nameHash == nameHash)
					{
						//Found it, so add a variation to this context's entry.
						currentContextEntry = contextEntry;
						currentContextEntry->m_Entry.numVariations++;

						u8 variationData;
						if(FindVariationDataTag(childNode, &variationData, bankName, waveName))
						{
							//Only add variation data if we have some, as it takes up additional space.
							currentContextEntry->variationDataList->Add(__box(variationData));
							currentContextEntry->variationNumberList->Add(__box(variationNumber));
							m_TotalNumVariationDataElements++;
						}
					}
				}

				if(!currentContextEntry)
				{
					//new context so add to hashtable
					if(!m_ContextTable->Contains(contextName->Substring(1)))
					{
						m_ContextTable->Add(contextName->Substring(1),nameHash.ToString());
					}
					//This is a new context, so create and initialize an entry for it.
					currentContextEntry = new audVoiceContextLookupEntryManaged();

					currentContextEntry->m_Entry.nameHash = nameHash;
					currentContextEntry->m_Entry.numVariations = 1;
					currentContextEntry->variationDataList = new ArrayList();
					currentContextEntry->variationNumberList = new ArrayList();

					currentContextEntry->m_Entry.contextData = FindContextDataTag(childNode, bankName, waveName);

					u8 variationData;
					if(FindVariationDataTag(childNode, &variationData, bankName, waveName))
					{
						//Only add variation data if we have some, as it takes up additional space.
						currentContextEntry->variationDataList->Add(__box(variationData));
						currentContextEntry->variationNumberList->Add(__box(variationNumber));
						m_TotalNumVariationDataElements++;
					}

					//Ensure bank name is in the name table and get the index.
					int bankNameIndex = m_ObjectCompiler->AddToStringTable(bankName);
					currentContextEntry->m_Entry.bankNameIndex = /*(u64)*/ bankNameIndex;

					contextList->Add(currentContextEntry);

					m_TotalNumContexts++;
				}
			}
		}
		else //!childNode->Name->Equals(S"Wave")
		{
			//Parse this node's children looking for Waves.
			AddVoiceContextsToTable(childNode, bankName, contextList);
		}

		childNode = childNode->NextSibling;
	
	}
}

u8 audSpeechBuilder::FindContextDataTag(XmlNode *childNode, String *bankName, String *waveName)
{
	u8 contextData = 0;

	XmlNode *parentNode = childNode->ParentNode;
	while(parentNode)
	{
		if(parentNode->Name->Equals(S"Tag"))
		{
			XmlAttribute *nameAttribute = parentNode->Attributes->get_ItemOf(S"name");
			if(nameAttribute)
			{
				if(nameAttribute->Value->Equals(S"contextData"))
				{
					XmlAttribute *valueAttribute = childNode->Attributes->get_ItemOf(S"value");
					if(valueAttribute)
					{
						try
						{
							contextData = Byte::Parse(valueAttribute->get_Value());	
						}
						catch(Exception *e)
						{
							e = NULL;
							String *errorString = S"Speech Build Error: Invalid contextData (";
							errorString = String::Concat(errorString, bankName, S"\\", waveName, S")");
							audBuildException *exception = new audBuildException(errorString);
							throw(exception);
						}
						break;
					}
				}
			}
		}

		parentNode = parentNode->ParentNode;
	}

	return contextData;
}

bool audSpeechBuilder::FindVariationDataTag(XmlNode *parentNode, u8 *variationData, String *bankName, String *waveName)
{
	*variationData = 0;

	//Check all the child nodes for a variationData tag.
	XmlNode *childNode = parentNode->FirstChild;
	while(childNode)
	{
		if(childNode->Name->Equals(S"Tag"))
		{
			XmlAttribute *nameAttribute = childNode->Attributes->get_ItemOf(S"name");
			if(nameAttribute)
			{
				if(nameAttribute->Value->Equals(S"variationData"))
				{
					XmlAttribute *valueAttribute = childNode->Attributes->get_ItemOf(S"value");
					if(valueAttribute)
					{
						try
						{
							*variationData = Byte::Parse(valueAttribute->get_Value());	
							return true;
						}
						catch(Exception *e)
						{
							e = NULL;
							String *errorString = S"Speech Build Error: Invalid variationData (";
							errorString = String::Concat(errorString, bankName, S"\\", waveName, S")");
							audBuildException *exception = new audBuildException(errorString);
							throw(exception);
						}
					}
				}
			}
		}

		childNode = childNode->NextSibling;
	}

	//Now check upwards through the parent nodes.
	parentNode = parentNode->ParentNode;
	while(parentNode)
	{
		if(parentNode->Name->Equals(S"Tag"))
		{
			XmlAttribute *nameAttribute = parentNode->Attributes->get_ItemOf(S"name");
			if(nameAttribute)
			{
				if(nameAttribute->Value->Equals(S"variationData"))
				{
					XmlAttribute *valueAttribute = childNode->Attributes->get_ItemOf(S"value");
					if(valueAttribute)
					{
						try
						{
							*variationData = Byte::Parse(valueAttribute->get_Value());	
							return true;
						}
						catch(Exception *e)
						{
							e = NULL;
							String *errorString = S"Speech Build Error: Invalid variationData (";
							errorString = String::Concat(errorString, bankName, S"\\", waveName, S")");
							audBuildException *exception = new audBuildException(errorString);
							throw(exception);
						}
					}
				}
			}
		}

		parentNode = parentNode->ParentNode;
	}

	return false;
}

void audSpeechBuilder::PrepareAssets(void)
{
	m_BuildClient->ReportProgress(-1, -1, S"Checking-out Speech Lookup Data", true);

	String *localConfigPath = String::Concat(m_LocalBuildDataPath, g_ConfigPath);

	//Ensure that all output paths exist locally.
	Directory::CreateDirectory(m_LocalBuildDataPath);
	Directory::CreateDirectory(localConfigPath);

	String *configPath = new String(g_ConfigPath);
	if(!m_AssetManager->SimpleCheckOut(String::Concat(m_AssetBuildDataPath, configPath, m_OutputPath),
		S"Building Speech"))
	{
		audBuildException *exception = new audBuildException(S"Failed to check-out Speech Lookup Data");
		throw(exception);
	}

	if(m_VoiceOutput){
		if(m_AssetManager->ExistsAsAsset(m_VoiceOutput)){
			if(!m_AssetManager->SimpleCheckOut(m_VoiceOutput,S"Building Speech"))
			{
				audBuildException *exception = new audBuildException(S"Failed to check-out Voice Lookup");
				throw(exception);
			}
		}
	}

	if(m_ContextOutput){
		if(m_AssetManager->ExistsAsAsset(m_ContextOutput)){
			if(!m_AssetManager->SimpleCheckOut(m_ContextOutput,S"Building Speech"))
			{
				audBuildException *exception = new audBuildException(S"Failed to check-out Voice Lookup");
				throw(exception);
			}
		}
	}

	

}

void audSpeechBuilder::WriteVoiceLookupFile(void)
{
	if(m_VoiceOutput)
	{
		try
		{
			FileStream * voiceFileStream = File::Open(m_VoiceOutput,FileMode::Create);
			StreamWriter *sw = new StreamWriter(voiceFileStream);
			ICollection *keys = m_VoiceTable->get_Keys();
			IEnumerator *Key = keys->GetEnumerator();
			while(Key->MoveNext())
			{
				String *keyString = __try_cast<String *> (Key->Current);
				sw->WriteLine(String::Concat(keyString,S",",m_VoiceTable->get_Item(keyString)));
			}
			sw->Flush();
			sw->Close();
			voiceFileStream->Close();
		}
		catch(Exception *e)
		{
			audBuildException *exception = new audBuildException(e->ToString());
			throw(exception);
		}
	}
}

void audSpeechBuilder::WriteContextLookupFile(void)
{
	if(m_ContextOutput)
	{
		try
		{
			FileStream * voiceFileStream = File::Open(m_ContextOutput,FileMode::Create);
			StreamWriter *sw = new StreamWriter(voiceFileStream);
			ICollection *keys = m_ContextTable->get_Keys();
			IEnumerator *Key = keys->GetEnumerator();
			while(Key->MoveNext())
			{
				String *keyString = __try_cast<String *> (Key->Current);
				sw->WriteLine(String::Concat(keyString,S",",m_ContextTable->get_Item(keyString)));
			}
			sw->Flush();
			sw->Close();
			voiceFileStream->Close();
		}
		catch(Exception *e)
		{
			audBuildException *exception = new audBuildException(e->ToString());
			throw(exception);
		}
	}
}

void audSpeechBuilder::WriteSpeechLookupTables(void)
{
	BinaryWriter *speechLookupFile=0;

	try
	{
		FileStream *speechLookupFileStream = new FileStream(String::Concat(m_LocalBuildDataPath, g_ConfigPath, m_OutputPath), 
															FileMode::Create);
		speechLookupFile = new BinaryWriter(speechLookupFileStream,System::Text::Encoding::ASCII);


		SerializeVoiceContextVariationData(speechLookupFile);
		SerializeVoiceContextLookupTable(speechLookupFile);
		SerializeVoiceLookupTable(speechLookupFile);

		m_ObjectCompiler->SerialiseStringTable(speechLookupFile);

		//Flush data to disk.
		speechLookupFile->Flush();
	}
	__finally
	{
		if(speechLookupFile)
		{
			speechLookupFile->Close();
		}
	}
}

void audSpeechBuilder::SerializeVoiceContextVariationData(BinaryWriter *speechLookupFile)
{
	//Write total number of variation data elements (for all voices and contexts.)
	speechLookupFile->Write(audPlatformSpecific::FixEndian(m_TotalNumVariationDataElements));

	u64 variationDataStartPosition = speechLookupFile->get_BaseStream()->get_Position();

	IEnumerator* voiceEnum = m_VoiceLookupList->GetEnumerator();
	while(voiceEnum->MoveNext())
	{
		audVoiceLookupEntryManaged *voiceEntry = __try_cast<audVoiceLookupEntryManaged *>(voiceEnum->Current);

		IEnumerator* voiceContextEnum = voiceEntry->contextList->GetEnumerator();
		while(voiceContextEnum->MoveNext())
		{
			audVoiceContextLookupEntryManaged *voiceContextEntry = __try_cast<audVoiceContextLookupEntryManaged *>
				(voiceContextEnum->Current);

			if(voiceContextEntry->variationDataList->get_Count() > 0)
			{
				//Compute start offset for this voice/context's variation data.
				voiceContextEntry->m_Entry.variationDataOffsetBytes = (s32)(speechLookupFile->get_BaseStream()->get_Position() -
					variationDataStartPosition);

				//Write the variation data.

				//Order variation data by ascending variation number.
				u32 prevLowestVariationNumber=0, currentLowestVariationNumber;
				Byte *variationDataToAdd;
				for(int numAddedToLookup=0; numAddedToLookup<voiceContextEntry->variationNumberList->Count; numAddedToLookup++)
				{
					variationDataToAdd = NULL;
					currentLowestVariationNumber = 4294967295;
					IEnumerator* variationNumberEnum = voiceContextEntry->variationNumberList->GetEnumerator();
					IEnumerator* variationDataEnum = voiceContextEntry->variationDataList->GetEnumerator();
					while(variationNumberEnum->MoveNext() && variationDataEnum->MoveNext())
					{
						Byte *variationNumber = __try_cast<Byte *>(variationNumberEnum->Current);
						Byte *variationData = __try_cast<Byte *>(variationDataEnum->Current);

						if(((u32)(*variationNumber) > prevLowestVariationNumber) && ((u32)(*variationNumber) < currentLowestVariationNumber))
						{
							currentLowestVariationNumber = (u32)(*variationNumber);
							variationDataToAdd = variationData;
						}
					}

					speechLookupFile->Write(*variationDataToAdd);

					prevLowestVariationNumber = currentLowestVariationNumber;
				}
			}
			else
			{
				//We don't have any variation data for this voice/context.
				voiceContextEntry->m_Entry.variationDataOffsetBytes = -1;
			}
		}
	}
}

void audSpeechBuilder::SerializeVoiceContextLookupTable(BinaryWriter *speechLookupFile)
{
	//Write total number of speech contexts (for all voices.)
	speechLookupFile->Write(audPlatformSpecific::FixEndian(m_TotalNumContexts));

	u64 coxtextStartPosition = speechLookupFile->get_BaseStream()->get_Position();

	IEnumerator* voiceEnum = m_VoiceLookupList->GetEnumerator();
	while(voiceEnum->MoveNext())
	{
		audVoiceLookupEntryManaged *voiceEntry = __try_cast<audVoiceLookupEntryManaged *>(voiceEnum->Current);

		//Compute start offset for this voice's contexts.
		voiceEntry->m_Entry.contextsOffsetBytes = (u32)(speechLookupFile->get_BaseStream()->get_Position() -
			coxtextStartPosition);
		voiceEntry->m_Entry.numContexts = (u16)voiceEntry->contextList->Count;

		//Order contexts by ascending name hash.
		u32 prevLowestNameHash=0, currentLowestNameHash;
		audVoiceContextLookupEntryManaged *voiceContextEntryToAdd=0;
		for(int numAddedToLookup=0; numAddedToLookup<voiceEntry->contextList->Count; numAddedToLookup++)
		{
			currentLowestNameHash = 4294967295;
			IEnumerator* voiceContextEnum = voiceEntry->contextList->GetEnumerator();
			while(voiceContextEnum->MoveNext())
			{
				audVoiceContextLookupEntryManaged *voiceContextEntry = __try_cast<audVoiceContextLookupEntryManaged *>
					(voiceContextEnum->Current);

				u32 nameHash = voiceContextEntry->m_Entry.nameHash;
				if((nameHash > prevLowestNameHash) && (nameHash < currentLowestNameHash))
				{
					currentLowestNameHash = nameHash;
					voiceContextEntryToAdd = voiceContextEntry;
				}
			}

			//Write speech context elements.
			speechLookupFile->Write((u32)audPlatformSpecific::FixEndian((u32)voiceContextEntryToAdd->m_Entry.
				bankNameIndex));
			speechLookupFile->Write((s32)audPlatformSpecific::FixEndian((u32)voiceContextEntryToAdd->m_Entry.
				variationDataOffsetBytes));
			speechLookupFile->Write((u32)audPlatformSpecific::FixEndian((u32)voiceContextEntryToAdd->m_Entry.
				nameHash));
			speechLookupFile->Write(voiceContextEntryToAdd->m_Entry.contextData);
			speechLookupFile->Write(voiceContextEntryToAdd->m_Entry.numVariations);

			prevLowestNameHash = currentLowestNameHash;
		}
	}
}

void audSpeechBuilder::SerializeVoiceLookupTable(BinaryWriter *speechLookupFile)
{
	//Write number of voices.
	speechLookupFile->Write(audPlatformSpecific::FixEndian((unsigned int)m_VoiceLookupList->Count));

	//Order voices by ascending name hash.
	u32 prevLowestNameHash=0, currentLowestNameHash;
	audVoiceLookupEntryManaged *voiceEntryToAdd=0;
	for(int numAddedToLookup=0; numAddedToLookup<m_VoiceLookupList->Count; numAddedToLookup++)
	{
		currentLowestNameHash = 4294967295;
		IEnumerator* voiceEnum = m_VoiceLookupList->GetEnumerator();
		while(voiceEnum->MoveNext())
		{
			audVoiceLookupEntryManaged *voiceEntry = __try_cast<audVoiceLookupEntryManaged *>
				(voiceEnum->Current);

			u32 nameHash = voiceEntry->m_Entry.nameHash;
			if((nameHash > prevLowestNameHash) && (nameHash < currentLowestNameHash))
			{
				currentLowestNameHash = nameHash;
				voiceEntryToAdd = voiceEntry;
			}
		}

		//Write voice elements.
		speechLookupFile->Write((u32)audPlatformSpecific::FixEndian((u32)voiceEntryToAdd->m_Entry.contextsOffsetBytes));
		speechLookupFile->Write((u32)audPlatformSpecific::FixEndian((u32)voiceEntryToAdd->m_Entry.nameHash));
		speechLookupFile->Write((u16)audPlatformSpecific::FixEndian((u16)voiceEntryToAdd->m_Entry.numContexts));

		prevLowestNameHash = currentLowestNameHash;
	}
}

void audSpeechBuilder::ReleaseAssets(void)
{
	m_BuildClient->ReportProgress(-1, -1, S"Undoing check-out of Speech Lookup Data", true);

	String *configPath = new String(g_ConfigPath);
	if(!m_AssetManager->SimpleUndoCheckOut(String::Concat(m_AssetBuildDataPath, configPath, m_OutputPath)))
	{
		audBuildException *exception = new audBuildException(S"Failed to undo check-out of Speech Lookup Data");
		throw(exception);
	}
}

void audSpeechBuilder::CommitAssets(void)
{
	m_BuildClient->ReportProgress(-1, -1, S"Checking-in / importing Speech Lookup Data", true);

	//import voice lookup
	if(m_VoiceOutput)
	{
		if(!m_AssetManager->CheckInOrImport(m_VoiceOutput,m_VoiceOutput, S"Speech Build"))
		{
			audBuildException *exception = new audBuildException(S"Failed to import/check-in Voice Lookup File");
			throw(exception);
		}
	}

	//import voice lookup
	if(m_ContextOutput)
	{
		if(!m_AssetManager->CheckInOrImport(m_ContextOutput,m_ContextOutput, S"Speech Build"))
		{
			audBuildException *exception = new audBuildException(S"Failed to import/check-in Voice Lookup File");
			throw(exception);
		}
	}

	//Check that the BuildData folder exists in the project database.
	if(m_AssetManager->ExistsAsAsset(m_AssetBuildDataPath))
	{
		//Check that the Config folder exists in the project database.
		if(m_AssetManager->ExistsAsAsset(String::Concat(m_AssetBuildDataPath, g_ConfigPath)))
		{
			if(!m_AssetManager->CheckInOrImport(String::Concat(m_LocalBuildDataPath, g_ConfigPath, m_OutputPath),
				String::Concat(m_AssetBuildDataPath, g_ConfigPath, m_OutputPath), S"Speech Build"))
			{
				audBuildException *exception = new audBuildException(S"Failed to import/check-in Speech Lookup Data");
				throw(exception);
			}			
		}
		else
		{
			//The Config folder does not exist in the project database, so simply import it.
			if(!m_AssetManager->Import(String::Concat(m_LocalBuildDataPath, g_ConfigPath), m_AssetBuildDataPath,
				S"Speech Build"))
			{
				audBuildException *exception = new audBuildException(S"Failed to import Config folder");
				throw(exception);
			}
		}
	}
	else //The BuildData folder does not exist in the project database, so import the whole thing.
	{
		if(!m_AssetManager->Import(m_LocalBuildDataPath, m_AssetBuildDataPath->Substring(0,
			m_AssetBuildDataPath->LastIndexOf('\\')), S"Speech Build"))
		{
			audBuildException *exception = new audBuildException(
				S"Failed to import BuildData folder");
			throw(exception);
		}
	}
}

void audSpeechBuilder::Shutdown(void)
{
}

} // namespace audSpeechBuild
