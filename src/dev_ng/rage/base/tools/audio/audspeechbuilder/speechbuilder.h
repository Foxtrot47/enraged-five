//
// tools/audspeechbuilder/speechbuilder.h
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved.
//

#ifndef SPEECH_BUILDER_H
#define SPEECH_BUILDER_H

#include "audiosoundtypes/speechdefs.h"

using namespace audAssetManagement;
using namespace audBuildCommon;
using namespace rage;
using namespace System;
using namespace System::Collections;
using namespace System::IO;
using namespace System::Xml;

namespace audSpeechBuild
{
	//
	// PURPOSE
	//	Managed wrapper class for the voice lookup entry structure.
	//
	public __gc class audVoiceLookupEntryManaged
	{
	public:
		audVoiceLookupEntryManaged() {};

		audVoiceLookupEntry m_Entry;
		ArrayList *contextList;
	};
	//
	// PURPOSE
	//	Managed wrapper class for the voice context lookup entry structure.
	//
	public __gc class audVoiceContextLookupEntryManaged
	{
	public:
		audVoiceContextLookupEntryManaged() {};

		audVoiceContextLookupEntry m_Entry;
		ArrayList *variationDataList;
		ArrayList *variationNumberList;
	};

	//
	// PURPOSE
	//  Performs the full speech lookup build, including interaction with the asset management system.
	// SEE ALSO
	//  audAssetManager, audVoiceLookupEntryManaged, audVoiceContextLookupEntryManaged, audBuildException,
	//		audPlatformSpecific
	//
	public __gc class audSpeechBuilder : public audPostBuilderBase
	{
	public:
		audSpeechBuilder();
		// PURPOSE
		//  Post-builds the speech lookup tables for the Wave assets listed in the Built Waves List.
		// PARAMS
		//  assetManager		- The Asset Manager instance to be used for accessing the requisite audio assets.
		//  projectSettings		- Defines the project for which audio assets are to be built.
		//	buildPlatform		- The name of the target build platform.
		//	buildClient			- An encapsulation of the connection to the build client.
		//  builtWavesXml		- An XML document containing a list of all currently built Waves assets (with associated
		//							metadata tags.)
		//	shouldBuildLocally	- If true, the build is to be performed locally, which may require special handling.
		//	buildComponent		- An encapsulation of this build step that includes custom settings.
		//
		void Build(audAssetManager *assetManager, audProjectSettings *projectSettings, String *buildPlatform,
			audBuildClient *buildClient, XmlDocument *builtWavesXml, XmlDocument *, bool shouldBuildLocally,
			audBuildComponent *buildComponent, bool isDeferredBuild);

	private:
		//
		// PURPOSE
		//  Initializes the Asset Manager working paths.
		// SEE ALSO
		//  audAssetManager
		//
		void Init(void);
		//
		// PURPOSE
		//  Parses the XML Built Waves List produced by the Wave Builder to generate in-memory speech lookup tables.
		//
		void ParseBuiltWavesXml(void);
		//
		// PURPOSE
		//  Searches the specified node in the XML Built Waves List for Bank nodes, in order to generate
		//	in-memory speech lookup tables.
		// PARAMS
		//	node		- The Built Waves node to be parsed.
		//	packName	- The name of the Pack node currently being parsed, or an empty string if no Pack node has yet
		//					been found
		//
		void ParseBankNodes(XmlNode *node, String *packName);
		//
		// PURPOSE
		//  Searches the specified (sub-Wave Bank) node in the XML Built Waves List for voice tags, in order to generate
		//	in-memory speech lookup tables.
		// PARAMS
		//	node		- The Built Waves node to be parsed.
		//	packName	- The name of the Pack node currently being parsed.
		//	bankName	- The name of the Bank node currently being parsed.
		//
		void ParseSubBankNodes(XmlNode *node, String *packName, String *bankName);
		//
		// PURPOSE
		//  Searches the immediate children of the specified node for a voice tag.
		// PARAMS
		//	parentNode	- The node whose immediate children are to be searched for a voice tag.
		// RETURNS
		//	The voice name contained within the voice tag, or a NULL string if no voice tag was found.
		//
		String *FindVoiceTag(XmlNode *parentNode);
		//
		// PURPOSE
		//  Extracts the speech contexts from the names of all Waves within the specified XML node and adds them to the
		//	specified context list.
		// PARAMS
		//	parentNode	- The Built Waves node to be parsed for Waves (for context extraction.)
		//	bankName	- The name of this node's parent Bank, which is referenced by each context in the list.
		//	contextList	- The list into which new speech contexts are added.
		//
		void AddVoiceContextsToTable(XmlNode *parentNode, String *bankName, ArrayList *contextList);
		//
		// PURPOSE
		//  Searches upwards through the parent nodes of the specified node for a contextData tag.
		// PARAMS
		//	childNode	- The node whose parent nodes are to be searched for a contextData tag.
		//	bankName	- The name of the wave bank being processed.
		//	waveName	- The name of the wave being processed.
		// RETURNS
		//	The value of the contextData tag, or 0 if no contextData tag was found.
		//
		u8 FindContextDataTag(XmlNode *childNode, String *bankName, String *waveName);
		//
		// PURPOSE
		//  Searches the immediate children of the specified node for a variationData tag. If this fails, a search
		//	is then performed upwards through the parent nodes.
		// PARAMS
		//	parentNode		- The node whose immediate children are to be searched for a variationData tag.
		//	variationData	- Used to return the value of the located variationData tag.
		//	bankName		- The name of the wave bank being processed.
		//	waveName		- The name of the wave being processed.
		// RETURNS
		//	True if a variationData tag was found.
		//
		bool FindVariationDataTag(XmlNode *parentNode, u8 *variationData, String *bankName, String *waveName);
		//
		// PURPOSE
		//  Exclusively checks-out the Speech Lookup file from the asset management system.
		// SEE ALSO
		//  audAssetManager
		//
		void PrepareAssets(void);
		//
		//PURPOSE
		// Writes the Hastable of filenames and their hash values to file
		//
		void WriteVoiceLookupFile(void);
		//
		//PURPOSE
		// Writes the Hastable of contexts and their hash values to file
		//
		void WriteContextLookupFile(void);
		//
		// PURPOSE
		//  Writes the in-memory lookup tables to the Speech Lookup file..
		//
		void WriteSpeechLookupTables(void);
		//
		// PURPOSE
		//  Writes the in-memory voice context variation data to the Speech Lookup file.
		// PARAMS
		//	speechLookupFile	- The speech lookup file to be written to.
		//
		void SerializeVoiceContextVariationData(BinaryWriter *speechLookupFile);
		//
		// PURPOSE
		//  Writes the in-memory voice context lookup table to the Speech Lookup file.
		// PARAMS
		//	speechLookupFile	- The speech lookup file to be written to.
		//
		void SerializeVoiceContextLookupTable(BinaryWriter *speechLookupFile);
		//
		// PURPOSE
		//  Writes the in-memory voice lookup table to the Speech Lookup file.
		// PARAMS
		//	speechLookupFile	- The speech lookup file to be written to.
		//
		void SerializeVoiceLookupTable(BinaryWriter *speechLookupFile);
		//
		// PURPOSE
		//  Undoes the check-out of all assets from the asset management system.
		// SEE ALSO
		//  audAssetManager
		//
		void ReleaseAssets(void);
		//
		// PURPOSE
		//  Checks-in / imports the built Speech Lookup file.
		// SEE ALSO
		//  audAssetManager
		//
		void CommitAssets(void);
		//
		// PURPOSE
		//  Restores the original Asset Manager working paths.
		// SEE ALSO
		//  audAssetManager
		//
		void Shutdown(void);

		u32 m_TotalNumContexts;
		u32 m_TotalNumVariationDataElements;
		String *m_BuildPlatform;
		String *m_AssetBuildDataPath;
		String *m_LocalBuildDataPath;
		XmlDocument *m_BuiltWavesXml;
		ArrayList *m_VoiceLookupList;
		audAssetManager *m_AssetManager;
		audProjectSettings *m_ProjectSettings;
		audBuildClient *m_BuildClient;
		CObjectCompiler *m_ObjectCompiler;
		ArrayList *m_ExcludeList;
		ArrayList *m_IncludeList;
		String *m_OutputPath;
		Hashtable* m_VoiceTable;
		Hashtable *m_ContextTable;
		String *m_VoiceOutput;
		String *m_ContextOutput;
	};

} // namespace audSpeechBuild

#endif // SPEECH_BUILDER_H
