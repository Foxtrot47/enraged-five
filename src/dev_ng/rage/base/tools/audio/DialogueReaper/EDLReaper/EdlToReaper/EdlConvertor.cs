﻿using Reaper;

namespace EDLReaper.EdlToReaper
{
    public class EdlConvertor
    {
        private string _audioBranch = @"//rdr3/audio/dev/";

        public string AudioBranch
        {
            get { return _audioBranch; }
            set { _audioBranch = value; }
        }

        public string Log { get; private set; }
        public ReaperProject Convert(string edlLocation)
        {
            TracksExtractor tracksExtractor = new TracksExtractor(AudioBranch);
            LoadingIndicatorVM.Instance.SetText("Starting track extraction...");
            ReaperProject reaperProject = new ReaperProject(tracksExtractor.GetTracksFromFile(edlLocation));
            this.Log += tracksExtractor.Log.ToString();
            return reaperProject;

        }
    }
}
