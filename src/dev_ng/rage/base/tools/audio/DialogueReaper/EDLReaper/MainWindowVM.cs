﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using DStarToReaper;
using GalaSoft.MvvmLight.Command;
using Microsoft.Win32;
using Reaper;
using RSG.Editor.Controls;
using WPFToolLib.Extensions;

namespace EDLReaper
{
    public class MainWindowVm : INotifyPropertyChanged
    {
        private EdlToReaper.EdlConvertor _EDLReaper;
        private string _edlLocation = @"X:\rdr3\audio\audio_assets_from_ny\timing_edl\";
        private bool _isLoading = false;
        private string _saveAsPath;

        public MainWindowVm()
        {

            BrowseFileCommand = new RelayCommand<BrowsFileParameters>(SetObjectToOpenFileDialouge);
            SaveFileCommand = new RelayCommand(SaveAs, () => !string.IsNullOrEmpty(EdlLocation) &&
                                                             Path.GetExtension(EdlLocation)
                                                                 .Equals(".txt",
                                                                     StringComparison.InvariantCultureIgnoreCase));
            SelectedItemsChanged = new RelayCommand<DialogueRoot>(p =>
            {
                SelectedDialogueRoot = p;
                SaveFileCommand.RaiseCanExecuteChanged();
            });
            DStarToReaper.Helper.StartWaveListAcquisition(AudioBranch);
        }

        public DialogueRoot SelectedDialogueRoot
        {
            get { return _selectedDialogueRoot; }
            set
            {
                _selectedDialogueRoot = value;
                PropertyChanged.Raise(this, "SelectedDialogueRoot");
                if (_selectedDialogueRoot == null) return;
                IsLoading = true;
                LoadingIndicatorVM.Instance.SetText("Searching for EDL file");
                string edlLocation = string.Empty;
                var task = Task.Factory.StartNew(() =>
                 {
                     edlLocation = DStarConvertor.GetEdlPath(DStarPath, _selectedDialogueRoot.Root);

                     IsLoading = false;
                 }
                 );
                task.ContinueWith(z => Application.Current.Dispatcher.Invoke(() => EdlLocation = edlLocation));
               
            }
        }

        private string _dStarPath = @"X:\rdr3\assets\Dialogue\American\";

        public RelayCommand SaveFileCommand { get; private set; }

        private EdlToReaper.EdlConvertor EDLReaper
        {
            get { return _EDLReaper ?? (_EDLReaper = new EdlToReaper.EdlConvertor()); }
        }



        public string AudioBranch
        {
            get { return EDLReaper.AudioBranch; }
            set
            {
                if (value.IsAudioBranch())
                {
                    EDLReaper.AudioBranch = value;
                    DStarToReaper.Helper.StartWaveListAcquisition(AudioBranch);
                 
                }
                PropertyChanged.Raise(this, "AudioBranch");
            }
        }

        public string EdlLocation
        {
            get { return _edlLocation; }
            set
            {
                _edlLocation = value;
                PropertyChanged.Raise(this, "EdlLocation");
                SaveFileCommand.RaiseCanExecuteChanged();
            }
        }


        public RelayCommand<BrowsFileParameters> BrowseFileCommand { get; private set; }

        public bool IsLoading
        {
            get { return _isLoading; }
            set
            {
                _isLoading = value;
                PropertyChanged.Raise(this, "IsLoading");
                PropertyChanged.Raise(this, "IsEnabled");
            }
        }

        public bool IsEnabled
        {
            get { return !IsLoading; }
        }

        private bool IsPathDstar()
        {
            return (!string.IsNullOrEmpty(DStarPath) &&
                    Path.GetExtension(DStarPath)
                        .Equals(".dstar2",
                            StringComparison.InvariantCultureIgnoreCase));
        }

        private List<DialogueRoot> _dialogueRoots;


        private DStarConvertor _dStarConvertor;
        private DialogueRoot _selectedDialogueRoot;

        private DStarConvertor DStarConvertor
        {
            get { return _dStarConvertor ?? (_dStarConvertor = new DStarConvertor()); }
        }

        public List<DialogueRoot> DialogueRoots
        {
            get { return _dialogueRoots; }

            set
            {
                _dialogueRoots = value;
                this.PropertyChanged.Raise(this, "DialogueRoots");
            }
        }

        public string DStarPath
        {
            get { return _dStarPath; }

            set
            {
                _dStarPath = value;
                PropertyChanged.Raise(this, "DStarPath");
                SaveFileCommand.RaiseCanExecuteChanged();
                if (IsPathDstar())
                {
                    IsLoading = true;
                    Task.Factory.StartNew(() =>
                    {
                        DialogueRoots =
                            DStarConvertor.GetRootList(DStarPath)
                                .ConvertAll(
                                    p => new DialogueRoot { UnRecorded = p.Lines.All(r => !r.Recorded), Root = p.Root });
                        IsLoading = false;
                    });
                }
                else
                {
                    DialogueRoots = new List<DialogueRoot>();
                }
            }
        }

        public RelayCommand<DialogueRoot> SelectedItemsChanged { get; private set; }

        public event PropertyChangedEventHandler PropertyChanged;

        private void SetObjectToOpenFileDialouge(BrowsFileParameters browsFileParameters)
        {
            var propertyToSet = browsFileParameters.ParameterName;
            var filter = browsFileParameters.Filter;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            PropertyInfo propertyInfo = GetType().GetProperty(propertyToSet);
            string currentPath = (string)propertyInfo.GetValue(this);
            openFileDialog.Filter = filter;
            if (File.Exists(currentPath))
            {
                openFileDialog.InitialDirectory = Path.GetDirectoryName(currentPath);
            }
            else if (Directory.Exists(currentPath))
            {
                openFileDialog.InitialDirectory = currentPath;
            }
            openFileDialog.ShowDialog();


            propertyInfo.SetValue(this, Convert.ChangeType(openFileDialog.FileName, propertyInfo.PropertyType), null);
        }

        private void SaveAs()
        {
            Task.Factory.StartNew(() =>
            {
                try
                {
                    LoadingIndicatorVM.Instance.SetText("Loading started..");
                    IsLoading = true;
                    ReaperProject rpp = EDLReaper.Convert(EdlLocation);
                    LoadingIndicatorVM.Instance.SetText("Creating reaper project..");
                    var filename = Path.Combine(Path.GetTempPath(), Path.GetFileName(EdlLocation));
                    filename = Path.ChangeExtension(filename, ".rpp");

                    rpp.Save(filename);
                    Process.Start(new ProcessStartInfo(filename));

                    LoadingIndicatorVM.Instance.SetText("Creating logfile..");
                    string logfile = Path.ChangeExtension(filename, ".txt");
                    File.WriteAllText(logfile, EDLReaper.Log);
                    Process.Start(new ProcessStartInfo(logfile));

                    LoadingIndicatorVM.Instance.SetText("Starting watching..");
                    TempFileWatcher tempfileWatcher = new TempFileWatcher(EdlLocation, filename);
                    tempfileWatcher.StartWatching();
                    IsLoading = false;
                }

                catch (Exception e)
                {
                    IsLoading = false;
                    StringBuilder message = new StringBuilder();
                    message.AppendLine(e.Message);
                    message.AppendLine();
                    message.AppendLine(e.StackTrace);
                    Application.Current.Dispatcher.Invoke(() => RsMessageBox.Show(message.ToString(), "Errors encountered"));
                }
            });
        }

    }
}