﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using RSG.Editor.Controls;
using RSG.Editor.Controls.Perforce;
using RSG.Text.Model;

namespace EDLReaper
{
    public class TempFileWatcher
    {
        public string EdlFile;
        public string TempFile;

        public TempFileWatcher(string edlfile, string tempfile)
        {
            EdlFile = edlfile;
            TempFile = tempfile;
        }

        public void StartWatching()
        {
            _fileSystemWatcher = new FileSystemWatcher(Path.GetDirectoryName(TempFile));
            _fileSystemWatcher.Changed += TempFileChanged;
            _fileSystemWatcher.Created += TempFileChanged;
            _fileSystemWatcher.Filter = Path.GetFileName(TempFile);
            _fileSystemWatcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.CreationTime | NotifyFilters.Size;
            _fileSystemWatcher.EnableRaisingEvents = true;
        }

        private FileSystemWatcher _fileSystemWatcher;
        
        [DllImport("user32.dll")]
        static extern IntPtr GetForegroundWindow();

        private void TempFileChanged(object sender, FileSystemEventArgs e)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                string message = string.Format("Reaper session associated with edl file:\n {0}\n has ben updated.\n" +
                                               "Do you want to check out and update the edl file?", EdlFile);
                _fileSystemWatcher.EnableRaisingEvents = false;
                MessageBoxResult result = RsMessageBox.Show(GetForegroundWindow(), message, "Change detected",
                    MessageBoxButton.YesNo);
                _fileSystemWatcher.EnableRaisingEvents = true;
                if (result == MessageBoxResult.Yes)
                {
                    try
                    {
                        PerforceService p4Service = new PerforceService();
                        p4Service.EditFiles(new[] { EdlFile });
                        int changelistnumber = p4Service.GetFileMetaData(new List<string> { EdlFile })[0].Change;
                        if (changelistnumber <= 0)
                        {
                            changelistnumber =
                                p4Service.CreateChangelist("Edl Timing Changes for " +
                                                           Path.GetFileNameWithoutExtension(EdlFile));

                            p4Service.MoveFilesIntoChangelist(new[] { EdlFile }, changelistnumber);
                        }
                        string updateTracks = ReaperToEDL.Helper.UpdateTracks(
                            Reaper.Helper.ParseReaperProject(TempFile),
                            EdlFile);
                        File.WriteAllText(EdlFile, updateTracks);
                        RsMessageBox.Show(GetForegroundWindow(),
                            string.Format("Please check changelist {0} for updated file.", changelistnumber),
                            "Completed Succesfully");
                    }
                    catch (Exception ex)
                    {
                        RsMessageBox.Show(GetForegroundWindow(), string.Format("Error {0}\nStacktrace: {1}", ex.Message, ex.StackTrace),
                            "Errors Encountered");
                    }

                }
            }
                );
        }
    }
}
