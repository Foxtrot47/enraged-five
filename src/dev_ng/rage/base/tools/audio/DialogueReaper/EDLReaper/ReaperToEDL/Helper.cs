﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using Reaper;

namespace EDLReaper.ReaperToEDL
{
    public static class Helper
    {
        public static string UpdateTracks(ReaperProject project, string pathToEdl)
        {
            var edl = new StringBuilder();
            using (var streamReader = new StreamReader(new FileStream(pathToEdl, FileMode.Open, FileAccess.Read)))
            {
                var trackListing = false;
                var currentTrack = string.Empty;
                while (!streamReader.EndOfStream)
                {
                    var currentLine = streamReader.ReadLine();

                    if (currentLine != null && currentLine.Contains("T R A C K  L I S T I N G"))
                    {
                        trackListing = true;
                    }

                    if (trackListing)
                    {
                        if (currentLine != null && currentLine.Contains("TRACK NAME:"))
                        {
                            currentTrack = currentLine.Replace("TRACK NAME:", string.Empty).Trim(' ', '\r', '\t');
                        }
                        var currenReaperTrack = project.Tracks.FirstOrDefault(p => p.Name.Equals(currentTrack));
                        if (currenReaperTrack != null)
                        {
                            foreach (var clip in currenReaperTrack.Clips)
                            {
                                if (currentLine != null && currentLine.Contains(clip.ClipName))
                                {
                                    currentLine = UpdateClip(clip, currentLine);
                                }
                            }
                        }
                    }
                    edl.AppendLine(currentLine);
                }
            }

            return edl.ToString();
        }

        private static string UpdateClip(ReaperTrack.Clip clip, string line)
        {
            var parts = line.Split(
                new[] { ' ', '\t' },
                StringSplitOptions.RemoveEmptyEntries);

            if (parts.Length > 2)
            {
                //// Channel = 0
                //// Event  = 1
                //// Clip Name = 2
            }

            if (parts.Length > 3)
            {
                var time = TimeSpan.FromSeconds(clip.Position);
                parts[3] = time.ToDynString();
            }

            if (parts.Length > 4)
            {
                //// End Time
                var endtime = TimeSpan.FromSeconds(clip.Position + clip.Length);
                parts[4] = endtime.ToDynString();
            }

            if (parts.Length > 5)
            {
                //// Duration
                var duration = TimeSpan.FromSeconds(clip.Length);
                parts[5] = duration.ToDynString();
            }

            if (parts.Length > 6)
            {
                //// State
            }
            if (parts.Length < 7)
            {
                Array.Resize(ref parts, 7);
            }

            return string.Format("{0}       	{1}       	{2}                  	{3}      	{4}      	{5}      	{6}", parts);

        }

        private static string ToDynString(this TimeSpan timeSpan)
        {
            var sb = new StringBuilder();

            if ((int)timeSpan.TotalHours > 0)
            {
                sb.Append((int)timeSpan.TotalHours);
                sb.Append(":");
            }

            sb.Append(timeSpan.Minutes.ToString("#0"));
            sb.Append(":");
            sb.Append(timeSpan.Seconds.ToString("00"));
            sb.Append(".");
            sb.Append(timeSpan.Milliseconds.ToString("000"));

            return sb.ToString();
        }
    }
}