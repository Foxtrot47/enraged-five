﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EDLReaper
{
    public class BrowsFileParameters
    {
        public string ParameterName { get; set; }
        public string Filter { get; set; }
    }
}
