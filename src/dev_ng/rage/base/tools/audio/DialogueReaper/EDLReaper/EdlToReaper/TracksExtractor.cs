﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Markup;
using Perforce.P4;
using Reaper;
using RSG.Editor.Controls.Perforce;
using RSG.Text.Model;
using File = Perforce.P4.File;
using Helper = DStarToReaper.Helper;

namespace EDLReaper.EdlToReaper
{
    public class TracksExtractor
    {
        private readonly string _audioBranch;
        private List<string> _wavelist; 
        public TracksExtractor(string audioBranch)
        {
            _audioBranch = audioBranch;
            Helper.CurrentAction += LoadingIndicatorVM.Instance.SetText;
      
        }

        public StringBuilder Log = new StringBuilder();

      
        public IEnumerable<ReaperTrack> GetTracksFromFile(string inputFile)
        {
            LoadingIndicatorVM.Instance.SetText("Acquiring waves list...");
            
            List<ReaperTrack> reaperTracks = new List<ReaperTrack>();
            TrackListings trackListings = new TrackListings();
            trackListings.LoadRecordingFile(inputFile);
           
            foreach (Track track in trackListings.Tracks)
            {
                var reaperTrack = new ReaperTrack { Clips = GetTrackClips(track, inputFile), Name = track.Name };
                if (reaperTrack.Clips.Count > 0)
                {
                    reaperTracks.Add(reaperTrack);
                }
            }

            return reaperTracks;
        }

        private List<ReaperTrack.Clip> GetTrackClips(Track track, string inputFile)
        {
            List<ReaperTrack.Clip> clips = new List<ReaperTrack.Clip>();
            foreach (var line in track.RecordedData)
            {
                try
                {
                    clips.Add(new ReaperTrack.Clip
                    {
                        ClipName = line.ClipName,
                        Position = TimeSpanFromString(line.StartTime).TotalSeconds,
                        Length = TimeSpanFromString(line.Duration).TotalSeconds,
                        SourceWave = Helper.GetClipDepotWavPath(line.ClipName)
                    });
                }
                catch (Exception e)
                {
                    Log.AppendLine(string.Format("Clip: {0} not included because of {1}", line.ClipName, e.Message));
                }

            }

            return clips;
        }

        private TimeSpan TimeSpanFromString(string time)
        {
            string[] splitduration = time.Split(':');

            int hours, minutes, seconds, milliseconds;

            seconds = int.Parse(splitduration[splitduration.Length - 1].Split('.')[0]);
            milliseconds = int.Parse(splitduration[splitduration.Length - 1].Split('.')[1]);
            minutes = 0;
            hours = 0;
            if (splitduration.Length == 2)
            {
                minutes = int.Parse(splitduration[splitduration.Length - 2]);
            }
            if (splitduration.Length == 3)
            {
                hours = int.Parse(splitduration[splitduration.Length - 3]);
            }
            return new TimeSpan(0, hours, minutes, seconds, milliseconds);

        }
       }
}
