﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml;
using DStarToReaper;
using GalaSoft.MvvmLight.Command;
using Microsoft.Win32;
using Reaper;
using RSG.Editor.Controls;
using WPFToolLib.Extensions;

namespace DialougeReaper
{
    public class MainWindowVm : INotifyPropertyChanged
    {
        private DStarConvertor _dStarConvertor;
        private string _dStarPath = @"X:\rdr3\assets\Dialogue\American\";
        private bool _isLoading;
        private string _saveAsPath;

        public MainWindowVm()
        {
            BrowseFileCommand = new RelayCommand<string>(SetObjectToOpenFileDialouge);
            BrowseFolderCommand = new RelayCommand<string>(SetObjectToFolderDialouge);
            SaveFileCommand = new RelayCommand(SaveAs, () => (IsPathDstar() && SelectedDialogueRoots.Count > 0));
            SelectedDialogueRoots = new List<string>();
            SelectedItemsChanged = new RelayCommand<IList>(p =>
            {
                SelectedDialogueRoots =
                    p.Cast<object>().Where(r => r is DialogueRoot).Cast<DialogueRoot>().Select(s => s.Root).ToList();
                SaveFileCommand.RaiseCanExecuteChanged();
            });
            DStarConvertor.NonRecordedFileEncountered += p => _nonRecordedFiles.Add(p);
            DStarToReaper.Helper.CurrentAction += LoadingIndicatorVM.Instance.SetText;
            DStarToReaper.Helper.StartWaveListAcquisition(AudioBranch);
        }

        private bool IsPathDstar()
        {
            return (!string.IsNullOrEmpty(DStarPath) &&
                    Path.GetExtension(DStarPath)
                        .Equals(".dstar2",
                            StringComparison.InvariantCultureIgnoreCase));
        }

        private List<string> _nonRecordedFiles;

        public RelayCommand SaveFileCommand { get; private set; }

        private DStarConvertor DStarConvertor
        {
            get { return _dStarConvertor ?? (_dStarConvertor = new DStarConvertor()); }
        }

        public string ConfigurationPath
        {
            get { return DStarConvertor.ConfigurationPath; }
            set
            {
                DStarConvertor.ConfigurationPath = value;
                PropertyChanged.Raise(this, "ConfigurationPath");
            }
        }

        public string EdlTimingsDirectory
        {
            get { return DStarConvertor.EdlTimingsDirectory; }
            set
            {
                DStarConvertor.EdlTimingsDirectory = value;
                PropertyChanged.Raise(this, "EdlTimingsDirectory");
            }
        }

        public string AudioBranch
        {
            get { return DStarConvertor.AudioBranch; }
            set
            {
                if (value.IsAudioBranch())
                {
                    DStarConvertor.AudioBranch = value;
                    DStarToReaper.Helper.StartWaveListAcquisition(AudioBranch);
                 
                }
                PropertyChanged.Raise(this, "AudioBranch");
            }
        }

        public bool UseEdl
        {
            get { return DStarConvertor.UseEdl; }
            set
            {
                DStarConvertor.UseEdl = value;
                PropertyChanged.Raise(this, "UseEdl");
            }
        }

        public string DStarPath
        {
            get { return _dStarPath; }
            set
            {

                _dStarPath = value;
                PropertyChanged.Raise(this, "DStarPath");
                SaveFileCommand.RaiseCanExecuteChanged();
                if (IsPathDstar())
                {
                    IsLoading = true;
                    Task.Factory.StartNew(() =>
                    {
                        DialogueRoots =
                            DStarConvertor.GetRootList(DStarPath)
                                .ConvertAll(
                                    p => new DialogueRoot { UnRecorded = !p.Lines.All(r => r.Recorded), Root = p.Root });
                        IsLoading = false;
                    });
                }
                else
                {
                    DialogueRoots = new List<DialogueRoot>();
                }
            }
        }

        public RelayCommand<string> BrowseFileCommand { get; private set; }
        public RelayCommand<string> BrowseFolderCommand { get; private set; }

        public bool IsLoading
        {
            get { return _isLoading; }
            set
            {
                _isLoading = value;
                PropertyChanged.Raise(this, "IsLoading");
                PropertyChanged.Raise(this, "IsEnabled");
            }
        }

        public bool IsEnabled
        {
            get { return !IsLoading; }
        }

        public List<DialogueRoot> DialogueRoots
        {
            get { return _dialogueRoots; }

            set
            {
                _dialogueRoots = value;
                this.PropertyChanged.Raise(this, "DialogueRoots");
            }
        }

        public List<string> SelectedDialogueRoots
        {
            get { return _selectedDialogueRoots; }
            set
            {
                _selectedDialogueRoots = value;
                this.PropertyChanged.Raise(this, "SelectedDialogueRoots");
            }
        }

        public RelayCommand<IList> SelectedItemsChanged { get; private set; }

        public double SecondsBetweenLine
        {
            get { return DStarConvertor.SecondsBetweenLine; }
            set
            {
                DStarConvertor.SecondsBetweenLine = value;
                PropertyChanged.Raise(this, "SecondsBetweenLine");
            }
        }
        public double SecondsBetweenConv
        {
            get { return DStarConvertor.SecondsBetweenConv; }
            set
            {
                DStarConvertor.SecondsBetweenConv = value;
                PropertyChanged.Raise(this, "SecondsBetweenConv");
            }
        }

        private List<DialogueRoot> _dialogueRoots;
        private List<string> _selectedDialogueRoots;


        public event PropertyChangedEventHandler PropertyChanged;

        private void SetObjectToOpenFileDialouge(string propertyToSet)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            PropertyInfo propertyInfo = GetType().GetProperty(propertyToSet);
            string currentPath = (string)propertyInfo.GetValue(this);
            if (File.Exists(currentPath))
            {
                openFileDialog.InitialDirectory = Path.GetDirectoryName(currentPath);
            }
            else if (Directory.Exists(currentPath))
            {
                openFileDialog.InitialDirectory = currentPath;
            }
            openFileDialog.ShowDialog();


            propertyInfo.SetValue(this, Convert.ChangeType(openFileDialog.FileName, propertyInfo.PropertyType), null);
        }

        private void SetObjectToFolderDialouge(string propertyToSet)
        {
            var dialog = new System.Windows.Forms.FolderBrowserDialog();
            PropertyInfo propertyInfo = GetType().GetProperty(propertyToSet);
            string currentPath = (string)propertyInfo.GetValue(this);
            if (Directory.Exists(currentPath))
            {
                dialog.SelectedPath = currentPath;
            }
            var result = dialog.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                propertyInfo.SetValue(this, Convert.ChangeType(dialog.SelectedPath, propertyInfo.PropertyType), null);
            }
        }

        private void SaveAs()
        {
            _nonRecordedFiles = new List<string>();
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.AddExtension = true;
            saveFileDialog.DefaultExt = ".rpp";
            saveFileDialog.FileName = Path.GetFileNameWithoutExtension(DStarPath);
            saveFileDialog.Filter = "Reaper projects (.rpp)|*.rpp";

            saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            bool? result = saveFileDialog.ShowDialog();
            if (result == true)
            {
                Task.Factory.StartNew(() =>
                {
                    try
                    {
                        IsLoading = true;
                        ReaperProject rpp = DStarConvertor.ConvertToReaperProject(DStarPath, SelectedDialogueRoots);
                        rpp.Save(saveFileDialog.FileName);
                        Process.Start(new ProcessStartInfo(saveFileDialog.FileName));
                        IsLoading = false;
                    }

                    catch (Exception e)
                    {
                        IsLoading = false;
                        StringBuilder message = new StringBuilder();
                        if (_nonRecordedFiles.Count > 0)
                        {
                            foreach (var file in _nonRecordedFiles)
                            {
                                message.Append(
                                    string.Format("{0}, ", file));
                            }
                            message.Append(" have not been recorded.");
                            message.AppendLine();
                        }
                        if (UseEdl)
                        {
                            message.AppendLine("This Dstar2 file might not have an edl file assosiated with it");
                            message.AppendLine();
                        }
                        message.AppendLine(e.Message);
                        message.AppendLine();
                        message.AppendLine(e.StackTrace);
                        Application.Current.Dispatcher.Invoke(() => RsMessageBox.Show(message.ToString(), "Errors encountered"));
                    }
                });
            }
        }
    }
}