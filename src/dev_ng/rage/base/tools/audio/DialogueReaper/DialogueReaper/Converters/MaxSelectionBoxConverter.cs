﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace DialougeReaper.Converters
{
    public class MaxSelectionBoxConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (double)value - 325;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}