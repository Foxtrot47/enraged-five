﻿using System.ComponentModel;
using System.Windows;
using WPFToolLib.Extensions;

namespace DialougeReaper
{
    public class LoadingIndicatorVM : INotifyPropertyChanged
    {
        private LoadingIndicatorVM()
        {
         }

        public static LoadingIndicatorVM Instance
        {
            get
            {
                if (_instance == null)
                {
                    Application.Current.Dispatcher.Invoke(
                () =>
                {
                    _instance = new LoadingIndicatorVM();
                });
                }
                return _instance;
            }
        }

        public string Text
        {
            get { return _text; }
            set
            {

                _text = value;



            }
        }
        private static LoadingIndicatorVM _instance;
        private string _text = "Loading...";

        public void SetText(string text)
        {
            Application.Current.Dispatcher.Invoke(
                () =>
                {
                    Text = text;

                    PropertyChanged.Raise(this, "Text");
                });

        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}