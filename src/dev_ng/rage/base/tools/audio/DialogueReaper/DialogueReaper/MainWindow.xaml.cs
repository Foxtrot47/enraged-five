﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RSG.Editor.Controls;

namespace DialougeReaper
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : RsWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = new MainWindowVm();
        }

        void SelectAll(bool select)
        {
            if (select)
            {
                DialogueRootListBox.SelectAll();
            }
            else
            {
                DialogueRootListBox.UnselectAll();
            }

            MainWindowVm mainWindowVm = DataContext as MainWindowVm;
            if (mainWindowVm != null)
                mainWindowVm.SelectedItemsChanged.Execute(DialogueRootListBox.SelectedItems);
        }

        private void CheckedChanged(object sender, RoutedEventArgs e)
        {
            bool check = DialogueRootListBox.SelectedItems.Count < DialogueRootListBox.Items.Count;
            SelectAll(check);

        }
    }
}
