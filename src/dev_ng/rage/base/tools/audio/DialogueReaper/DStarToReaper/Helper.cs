﻿#region

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Perforce.P4;
using RSG.Editor.Controls.Perforce;
using RSG.Text.Model;
using File = System.IO.File;

#endregion

namespace DStarToReaper
{
    public static class Helper
    {

        public static Action<string> CurrentAction;

        public static LineRecordedData GetRecordedData(string filename, TrackListings trackListings)
        {
            return trackListings.Tracks.SelectMany(track => track.RecordedData).
                FirstOrDefault(recordedData => String.Equals(filename, recordedData.ClipName));
        }

        public static TrackListings CreateTrackListings(string directory)
        {

            TrackListings trackListings = new TrackListings();
            if (Directory.Exists(directory))
            {
                string[] filenames = Directory.GetFiles(directory, "*.txt", SearchOption.AllDirectories);
                foreach (string filename in filenames)
                {
                    trackListings.LoadRecordingFile(filename);
                }
            }

            return trackListings;
        }

        private static bool _working;
        private static string _lastAudioBranch = string.Empty;
        private static Lookup<string, string> GetListOfDepotWaves(string audioBranch)
        {
            _working = true;
            Server srv = new Server(new ServerAddress(null));
            Repository p4 = new Repository(srv);
            p4.Connection.Connect(new Options());

            GetDepotFilesCmdOptions opts =
                new GetDepotFilesCmdOptions(GetDepotFilesCmdFlags.NotDeleted, 0);
            string wavesDepotPath = Path.Combine(audioBranch, "Assets",
                "Waves");
            wavesDepotPath = audioBranch.Contains(":") ? String.Format("//{0}", wavesDepotPath.Replace('\\', '/').Substring(3)) : wavesDepotPath.Replace('\\', '/');
            FileSpec fs = new FileSpec(new DepotPath(wavesDepotPath + "/..."), null);

            IList<Perforce.P4.File> target = p4.GetFiles(opts, fs);
            List<string> files = target.Select(p => p.DepotPath.Path.ToUpper()).ToList<string>();

            p4.Connection.Disconnect();
            Lookup<string, string> lookup = (Lookup<string, string>)files.ToLookup(Path.GetFileName);
            return lookup;
        }



        public static bool IsAudioBranch(this string audioBranch)
        {
            var wavesDepotPath = Path.Combine(audioBranch, "projectSettings.xml");
            wavesDepotPath = audioBranch.Contains(":") ? String.Format("//{0}", wavesDepotPath.Replace('\\', '/').Substring(3)) : wavesDepotPath.Replace('\\', '/');
            Server srv = new Server(new ServerAddress(null));
            Repository p4 = new Repository(srv);
            p4.Connection.Connect(new Options());
            try
            {
                IList<Perforce.P4.File> result = p4.GetFiles(new List<FileSpec> { new Perforce.P4.FileSpec(new DepotPath(wavesDepotPath), VersionSpec.Head) }, new Options());
                return (result != null && result.Count > 0);
            }
            catch (Exception)
            {

                return false;
            }
          
        }

        public static void StartWaveListAcquisition(string branch)
        {
            if (!branch.ToUpper().Contains("AUDIO") || _lastAudioBranch.Equals(branch, StringComparison.InvariantCultureIgnoreCase))
            {
                return;
            }
            _lastAudioBranch = branch;
            if (_getWavesListTask != null)
            {
                _getWavesListTask.Dispose();
            }
            _getWavesListTask =
                Task.Factory.StartNew(() => GetListOfDepotWaves(branch)).ContinueWith(p =>
                {
                    _waveslist = p.Result;
                    _working = false;
                });

        }

        private static Lookup<string, string> _waveslist;

        private static Task _getWavesListTask;

        public static string GetClipDepotWavPath(string clipName)
        {
            CurrentAction("Acquiring _waveslist...");
            while (_waveslist == null || _working)
            {

            }
            CurrentAction(string.Format("Loading clip {0} ....", clipName));
            // The wav file gets postfixed with _01.
            string lineFilenameWithPostfix = string.Format("{0}.WAV", clipName);

            int variation;
            if (int.TryParse(clipName.Split('_').Last(), out variation) != true)
            {
                throw new Exception(string.Format("{0} does not contain a valid variation (skipping)", clipName));
            }
            string wavesDepotPath = _waveslist[lineFilenameWithPostfix.ToUpper()].FirstOrDefault();

            if (string.IsNullOrEmpty(wavesDepotPath))
            {
                throw new FileNotFoundException(string.Format("{0} couldn't be found", lineFilenameWithPostfix));
            }

            PerforceService service = new PerforceService();

            IList<string> result = service.GetLocalPathsFromDepotPaths(new List<string> { wavesDepotPath });
            service.GetLatest(result);

            return result[0];
        }
    }
}