﻿#region

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using NAudio.Wave.SampleProviders;
using Reaper;
using RSG.Editor.Controls.Perforce;
using RSG.Text.Model;

#endregion

namespace DStarToReaper
{
    public class DStarConvertor
    {
        private readonly DialogueConfigurations _configuration;
        private readonly PerforceService _perforceService = new PerforceService();
        private string _configurationPath;
        private TrackListings _trackListings;
        public Action<string> NonRecordedFileEncountered;
        public DStarConvertor(string configurationPath = @"X:\rdr3\assets\Dialogue\Config\Configurations.dstarconfig",
            bool useEdl = false, string edlTimingsDirectory = @"x:\rdr3\audio\audio_assets_from_ny\timing_edl\",
            string audioBranch = @"X:\rdr3\audio\DEV\")
        {
            _configuration = new DialogueConfigurations();
            SecondsBetweenConv = 120;
            SecondsBetweenLine = 4;
            ConfigurationPath = configurationPath;
            UseEdl = useEdl;
            EdlTimingsDirectory = edlTimingsDirectory;
            AudioBranch = audioBranch;


        }

        public bool UseEdl { get; set; }
        public string AudioBranch { get; set; }

        public string ConfigurationPath
        {
            get { return _configurationPath; }
            set
            {
                _configurationPath = value;

            }
        }

        public string EdlTimingsDirectory { get; set; }

        public List<Conversation> GetRootList(string fullPath)
        {
            _perforceService.GetLatest(new List<string> { _configurationPath });
            _configuration.LoadConfigurations(_configurationPath);

            using (var reader = XmlReader.Create(fullPath))
            {
                var dialogue = new Dialogue(reader, _configuration);

                return dialogue.Conversations.ToList();
            }
        }

        public string GetEdlPath(string fullPath, string root)
        {
            var conversation = GetRootList(fullPath).FirstOrDefault(p => p.Root == root);
            if (conversation == null)
            {
                return EdlTimingsDirectory;
            }
            var recordedLines = conversation.Lines.Where(p => p.Recorded);
            if (!recordedLines.Any())
            {
                return EdlTimingsDirectory;
            }
            var clipnames = recordedLines.Select(p => p.AudioFilepath);
            var edlSubDir = Directory.GetDirectories(EdlTimingsDirectory);

            foreach (string directory in edlSubDir.Where(p => (new DirectoryInfo(p)).Name.Contains(root.Split('_')[0])))
            {
                foreach (string file in Directory.GetFiles(directory, "*.txt"))
                {
                    TrackListings ts = new TrackListings();
                    ts.LoadRecordingFile(file);
                    IEnumerable<Track> validTracks =
                        ts.Tracks.Where(p => p.RecordedData.Any(r => clipnames.Contains(r.ClipName)));
                    if (validTracks.Any())
                    {
                        return file;
                    }
                }
            }


            // if not found there:
            foreach (string file in Directory.GetFiles(EdlTimingsDirectory, "*.txt", SearchOption.AllDirectories))
            {
                TrackListings ts = new TrackListings();
                ts.LoadRecordingFile(file);
                IEnumerable<Track> validTracks =
                    ts.Tracks.Where(p => p.RecordedData.Any(r => clipnames.Contains(r.ClipName)));
                if (validTracks.Any())
                {
                    return file;
                }
            }

            return EdlTimingsDirectory;
        }

        public ReaperProject ConvertToReaperProject(string fullPath, IList<string> selectedDialogueRoots)
        {
            var reaperProject = new ReaperProject();
            _perforceService.GetLatest(new List<string> { _configurationPath });
            _configuration.LoadConfigurations(_configurationPath);

            if (UseEdl && _trackListings == null)
            {
                _perforceService.GetLatest(new List<string> { EdlTimingsDirectory });
                _trackListings = Helper.CreateTrackListings(EdlTimingsDirectory);
            }

            Dialogue dialogue;
            using (var reader = XmlReader.Create(fullPath))
            {
                dialogue = new Dialogue(reader, _configuration);
            }

            var trackDictionary = new Dictionary<int, ReaperTrack>();

            double conversationStartPosition = 0;
            foreach (var conversation in dialogue.Conversations)
            {
                if (!selectedDialogueRoots.Contains(conversation.Root))
                { continue; }
                double cuePosition = 0;

                reaperProject.Markers.Add(new ReaperMarker
                {
                    Name = conversation.Root,
                    Position = conversationStartPosition
                });

                foreach (var line in conversation.Lines)
                {
                    if (line.Recorded == false)
                    {
                        NonRecordedFileEncountered(line.AudioFilepath);
                        continue;
                    }
                    
                    string wavfile = string.Empty;
                   
                    try
                    {
                        wavfile = Helper.GetClipDepotWavPath(
                     line.AudioFilepath);
                    }
                    catch (Exception)
                    {
                        continue;
                    }



                    DstarReaperClip reaperClip;
                    if (UseEdl)
                    {
                        reaperClip =
                            new DstarReaperClip(Helper.GetRecordedData(line.AudioFilepath, _trackListings), wavfile,
                                conversationStartPosition);
                        cuePosition = (reaperClip.Length + reaperClip.Position) - conversationStartPosition;
                    }
                    else
                    {
                        reaperClip = new DstarReaperClip(line, wavfile, conversationStartPosition + cuePosition);
                        cuePosition += reaperClip.Length + SecondsBetweenLine;
                    }


                    if (!trackDictionary.ContainsKey(line.Speaker) || trackDictionary[line.Speaker] == null)
                    {
                        if (_configuration.GetCharacter(
                            dialogue.CharacterMappings.Mappings[line.Speaker].CharacterId) == null)
                        {
                            trackDictionary[line.Speaker] = new ReaperTrack
                            {
                                Name = line.Speaker.ToString()

                            };
                        }
                        else
                        {


                            trackDictionary[line.Speaker] = new ReaperTrack
                            {
                                Name =
                                    _configuration.GetCharacter(
                                        dialogue.CharacterMappings.Mappings[line.Speaker].CharacterId).Name
                            };
                        }
                    }
                    if (trackDictionary[line.Speaker].Clips == null)
                    {
                        trackDictionary[line.Speaker].Clips = new List<ReaperTrack.Clip>();
                    }
                    trackDictionary[line.Speaker].Clips.Add(reaperClip);
                }

                conversationStartPosition += Math.Ceiling(cuePosition + SecondsBetweenConv);
            }

            reaperProject.Tracks = trackDictionary.Values.ToList();
            return reaperProject;
        }

        public double SecondsBetweenConv { get; set; }
        public double SecondsBetweenLine { get; set; }
    }
}