﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using NAudio.Wave;
using Reaper;
using RSG.Text.Model;

namespace DStarToReaper
{
    public class DstarReaperClip : ReaperTrack.Clip
    {
        public DstarReaperClip(LineRecordedData line, string wavfile, double delay = 0)
            : base()
        {

            ClipName = line.ClipName;
            Position = line.StartDateTime.Value.TimeOfDay.TotalSeconds + delay;
            Length = line.EndDateTime.Value.TimeOfDay.TotalSeconds -
                     line.StartDateTime.Value.TimeOfDay.TotalSeconds;

            SourceWave = wavfile;

        }


        public DstarReaperClip(ILine line, string wavfileLocation, double position)
            : base()
        {
            WaveFileReader wavfile = new WaveFileReader(wavfileLocation);
            ClipName = line.AudioFilepath;
            Position = position;
            Length = (wavfile.SampleCount / (double)wavfile.WaveFormat.Channels) / (double)wavfile.WaveFormat.SampleRate;
            SourceWave = wavfileLocation;

        }
    }
}
