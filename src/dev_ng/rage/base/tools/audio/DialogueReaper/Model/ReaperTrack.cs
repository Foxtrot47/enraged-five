﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Reaper
{
    public class ReaperTrack
    {
        public class Clip
        {
            public string ClipName { get; set; }
            public double Position { get; set; }
            public double Length { get; set; }
            public string SourceWave { get; set; }
        }

        public string Name { get; set; }
        public List<Clip> Clips { get; set; }

        public ReaperTrack()
        {
            Clips = new List<Clip>();
        }
        
      }
}
