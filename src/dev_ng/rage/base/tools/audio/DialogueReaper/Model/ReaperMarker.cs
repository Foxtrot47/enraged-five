﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reaper
{
    public class ReaperMarker
    {
        public string Name { get; set; }
        public double Position { get; set; }
    }
}
