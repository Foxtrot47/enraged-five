﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.IO;
namespace Reaper
{
    public class ReaperProject
    {
        public List<ReaperMarker> Markers { get; set; }
        public List<ReaperTrack> Tracks { get; set; }
        public string ReaperVersion { get; set; }

        public ReaperProject(IEnumerable<ReaperTrack> tracks, string reaperVersion = "4.13")
        {
            Tracks = tracks.ToList();
            ReaperVersion = reaperVersion;
            Markers = new List<ReaperMarker>();
        }

        public ReaperProject(string reaperVersion = "4.13")
        {
            Markers = new List<ReaperMarker>();
            Tracks = new List<ReaperTrack>();
            ReaperVersion = reaperVersion;
        }


        public void Save(string filePath)
        {
            ReaperProjectTemplate projectTemplate = new ReaperProjectTemplate();
            projectTemplate.Session = new ConcurrentDictionary<string, object>();
            projectTemplate.Session["ReaperProject"] = this;
            projectTemplate.Initialize();
            File.WriteAllText(filePath, projectTemplate.TransformText());
        }


    }
}
