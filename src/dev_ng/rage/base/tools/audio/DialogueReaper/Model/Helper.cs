﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Reaper
{
    public static class Helper
    {

        public static string GetCleanString(this string dirtyString)
        {
            return new String(dirtyString.Where(p => (Char.IsLetterOrDigit(p) || p == '.' || p == '_')).ToArray());
        }

        public static ReaperProject ParseReaperProject(string path)
        {
            ReaperProject reaperProject = new ReaperProject();
            string reaperProjectString;
            using (var sr = new StreamReader(new FileStream(path, FileMode.Open)))
            {
                reaperProjectString = sr.ReadToEnd();
            }

            #region Markers

            var result = reaperProjectString.Split(new[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

            var markersStrings = result.Where(p => p.Contains("MARKER")).ToArray();
            reaperProject.Markers = new List<ReaperMarker>();
            for (var i = 0; i < markersStrings.Length; i++)
            {
                reaperProject.Markers.Add(new ReaperMarker
                {
                    Name = markersStrings[i].Trim().Split(' ')[3],
                    Position = double.Parse(markersStrings[i].Trim().Split(' ')[2])
                });
            }

            #endregion

            #region Project
            var reaperSplit = reaperProjectString.Split('<');
            for (int i = 0; i < reaperSplit.Length; i++)
            {
                switch (reaperSplit[i].Split(' ')[0].ToUpper())
                {
                    case "REAPER_PROJECT":
                        reaperProject.ReaperVersion = reaperSplit[i].Split(new char[] { ' ', '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries)[2].Replace("\"", "");
                        break;
                    case "TRACK":
                        var reaperTrack = new ReaperTrack();
                        var trackContent = reaperSplit[i].Split(new[] { "\n", "\r\n" },
                            StringSplitOptions.RemoveEmptyEntries);

                        var nameLine = trackContent.FirstOrDefault(p => p.Contains("NAME"));
                        reaperTrack.Name = nameLine.Trim().Split(' ')[1].Replace("\"", string.Empty);

                        while (i + 1 < reaperSplit.Length && reaperSplit[i + 1].Split(' ')[0].ToUpper() != "TRACK")
                        {
                            i++;
                            ReaperTrack.Clip reaperClip = null;
                            if (reaperSplit[i].Split(' ', '\r', '\n')[0].ToUpper().Equals("ITEM"))
                            {
                                reaperClip = new ReaperTrack.Clip();
                                var itemContent = reaperSplit[i].Split(new[] { "\n", "\r\n" },
                                    StringSplitOptions.RemoveEmptyEntries);
                                reaperClip.ClipName =
                                    itemContent.FirstOrDefault(p => p.Contains("NAME")).Trim().Split(' ')[1];
                                reaperClip.ClipName = reaperClip.ClipName.GetCleanString();

                                reaperClip.Position =
                                    double.Parse(itemContent.FirstOrDefault(p => p.Contains("POSITION")).Trim().Split(' ')[1].GetCleanString());
                                reaperClip.Length =
                                    double.Parse(
                                        itemContent.FirstOrDefault(p => p.Contains("LENGTH")).Trim().Split(' ')[1].GetCleanString());
                            }
                            if (reaperClip != null && i + 1 < reaperSplit.Length &&
                                reaperSplit[i + 1].Split(new[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries)[0]
                                    .ToUpper() == "SOURCE WAVE")
                            {
                                reaperClip.SourceWave = reaperSplit[i + 1].Split(new[] { "\n", "\r\n" },
                                    StringSplitOptions.RemoveEmptyEntries)
                                    .FirstOrDefault(p => p.Contains("FILE"))
                                    .Trim()
                                    .Split(' ')[1].Replace("\"", string.Empty);
                            }
                            if (reaperClip != null)
                            {
                                reaperTrack.Clips.Add(reaperClip);
                            }
                        }
                        reaperProject.Tracks.Add(reaperTrack);
                        break;
                }
            }
            #endregion
            return reaperProject;


        }


    }
}