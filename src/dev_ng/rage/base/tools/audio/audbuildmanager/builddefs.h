//
// tools/audbuildmanager/builddefs.h
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved.
//

#ifndef BUILD_DEFS_H
#define BUILD_DEFS_H

//Within BuildInfo folder:
const char g_PendingWaveListFilename[]		= "PendingWaves.xml";
const char g_BuiltWavesLogFilename[]		= "BuiltWaves.xml";
//Within BuildData folder:
const char g_WavePaksPath[]					= "Sfx\\";
const char g_ConfigPath[]					= "Config\\";
//Within Config folder:
const char g_SpeechLookupFilename[]			= "speech.dat";

#endif // BUILD_DEFS_H
