//
// tools/audbuildmanager/serverthread.cpp
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved.
//

#include "stdafx.h"

#include "serverthread.h"

#include "audbuildmanagerWinService.h"
#include "buildthread.h"

#include "clientObject.h"

using namespace Microsoft::Win32;
using namespace System::Net;
using namespace System::Net::Sockets;
using namespace System::Text;
using namespace System::Threading;

const int g_DefaultServerPort = 44100;
const int g_ClientResponseBytesTimeoutMs = 5000;

const int g_MaxClientCommandBytes = 1024;

//The ThreadProc method is called when the thread starts.
void audServerThread::ThreadProc()
{
	Int32 serverPort = g_DefaultServerPort;
	Byte clientCommandBytes[];

	TcpListener *serverSocket = NULL;
	TcpClient *newClient = NULL;
	NetworkStream *stream = NULL;

	try
	{
		//Create a build thread instance in advance of a client request.
		audBuildThread *buildThreadInstance = new audBuildThread();
		//Read server port from registry (initialize if not found).
		RegistryKey *regKey = Registry::LocalMachine->OpenSubKey(
			"SOFTWARE\\Rockstar Games\\RAGE Audio\\Build Manager", true);
		if(!regKey)
		{
			regKey = Registry::LocalMachine->CreateSubKey(
				"SOFTWARE\\Rockstar Games\\RAGE Audio\\Build Manager");
		}
		Object *portObj = regKey->GetValue("serverPort");
		if(portObj)
		{
            serverPort = Int32::Parse(portObj->ToString());
		}
		else
		{
			regKey->SetValue("serverPort", __box(serverPort));
		}

		buildThreadInstance->m_EventLog = m_EventLog;
		buildThreadInstance->m_RegKey = regKey;


		
		//Allocate memory for buffer to receive client commands.
		clientCommandBytes = new Byte[g_MaxClientCommandBytes];

		try
		{
			serverSocket = new TcpListener(IPAddress::Any, serverPort);
			//Start listening for client requests.
			serverSocket->Start();
		}
		catch(Exception *exception)
		{
			Console::WriteLine(exception->ToString());
			Environment::Exit(-1);
		}

		//Enter the listening loop.
		bool update = true;
        while(update)
		{
			if(serverSocket->Pending())
			{
				//Perform a blocking call to accept client request.
	            newClient = serverSocket->AcceptTcpClient();
				//Get a stream Object* for reading and writing.
				stream = newClient->GetStream();
				
				//Give each connection a thread
				clientObject *clientDetails = new clientObject(newClient,stream);
				Object *o = __try_cast<Object *>(clientDetails);

				ThreadPool::QueueUserWorkItem(new WaitCallback(buildThreadInstance,&audBuildThread::ThreadProc),o);
			}

			Thread::Sleep(1000);
		}
	}
	catch(Exception *exception)
	{
		if(serverSocket)
		{
			serverSocket->Stop();
			serverSocket = NULL;
		}

		GC::Collect();

		//Exit on any non-abort exception.
		if(exception->GetType()->ToString()->CompareTo(S"System.Threading.ThreadAbortException"))
		{
			m_EventLog->WriteEntry(exception->ToString(), EventLogEntryType::Error);
			Environment::Exit(-1); //Service will be automatically restarted by SCM.
		}
	}
}
