//
// tools/audbuildmanager/audbuildmanagerWinService.h
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved.
//

#pragma once

using namespace System;
using namespace System::Collections;
using namespace System::ComponentModel;
using namespace System::ServiceProcess;
using namespace System::Threading;


namespace audbuildmanager
{
	/// <summary> 
	/// Summary for audbuildmanagerWinService
	/// </summary>
	///
	/// WARNING: If you change the name of this class, you will need to change the 
	///          'Resource File Name' property for the managed resource compiler tool 
	///          associated with all .resx files this class depends on.  Otherwise,
	///          the designers will not be able to interact properly with localized
	///          resources associated with this form.
	public __gc class audbuildmanagerWinService : public System::ServiceProcess::ServiceBase 
	{
	public:
		audbuildmanagerWinService()
		{
			m_ServerThread = NULL;

			InitializeComponent();    
		}
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		void Dispose(bool disposing)
		{
			if (disposing && components)
			{
				components->Dispose();
			}
			__super::Dispose(disposing);
		}
		
	protected:
		/// <summary>
		/// Set things in motion so your service can do its work.
		/// </summary>
		void OnStart(String* args[]);

		/// <summary>
		/// Stop this service.
		/// </summary>
		void OnStop();

	private: System::Diagnostics::EventLog *  MyEventLog;
			 Thread *m_ServerThread;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container *components;

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>	
		void InitializeComponent(void)
		{
			this->MyEventLog = new System::Diagnostics::EventLog();
			(__try_cast<System::ComponentModel::ISupportInitialize *  >(this->MyEventLog))->BeginInit();
			// 
			// MyEventLog
			// 
			this->MyEventLog->Log = S"Application";
			this->MyEventLog->Source = S"Audio Build Manager";
//			this->MyEventLog->EntryWritten += new System::Diagnostics::EntryWrittenEventHandler(this, eventLog1_EntryWritten);
			// 
			// audbuildmanagerWinService
			// 
			this->CanPauseAndContinue = true;
			this->ServiceName = S"audbuildmanagerWinService";
			(__try_cast<System::ComponentModel::ISupportInitialize *  >(this->MyEventLog))->EndInit();

		}		
	private: System::Void eventLog1_EntryWritten(System::Object *  sender, System::Diagnostics::EntryWrittenEventArgs *  e)
			 {
				 (sender);
				 (e);
			 }

	};
}
