#include "stdafx.h"
#include "clientObject.h"

clientObject::clientObject(TcpClient *clientSocket,NetworkStream *clientStream)
{
	m_ClientSocket = clientSocket;
	m_ClientStream = clientStream;
}

TcpClient *clientObject::GetSocket(){
	return m_ClientSocket;
}

NetworkStream *clientObject::GetStream()
{
	return m_ClientStream;
}


