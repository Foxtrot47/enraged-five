//
// tools/audbuildmanager/ProjectInstaller.h
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved.
//

#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Configuration::Install;


namespace audbuildmanager
{
	[RunInstaller(true)]	

	/// <summary> 
	/// Summary for ProjectInstaller
	/// </summary>
	public __gc class ProjectInstaller : public System::Configuration::Install::Installer
	{
	public: 
		ProjectInstaller(void)
		{
			InitializeComponent();
		}
        
	protected: 
		void Dispose(Boolean disposing)
		{
			if (disposing && components)
			{
				components->Dispose();
			}
			__super::Dispose(disposing);
		}
	private: System::ServiceProcess::ServiceProcessInstaller *  serviceProcessInstaller;
	private: System::ServiceProcess::ServiceInstaller *  serviceInstaller;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container* components;
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>		
		void InitializeComponent(void)
		{
			this->serviceProcessInstaller = new System::ServiceProcess::ServiceProcessInstaller();
			this->serviceInstaller = new System::ServiceProcess::ServiceInstaller();
			// 
			// serviceProcessInstaller
			// 
			this->serviceProcessInstaller->Account = System::ServiceProcess::ServiceAccount::LocalSystem;
			this->serviceProcessInstaller->Password = 0;
			this->serviceProcessInstaller->Username = 0;
			// 
			// serviceInstaller
			// 
			this->serviceInstaller->ServiceName = S"Audio Build Manager";
			this->serviceInstaller->StartType = System::ServiceProcess::ServiceStartMode::Automatic;
			// 
			// ProjectInstaller
			// 
			System::Configuration::Install::Installer* __mcTemp__1[] = new System::Configuration::Install::Installer*[2];
			__mcTemp__1[0] = this->serviceProcessInstaller;
			__mcTemp__1[1] = this->serviceInstaller;
			this->Installers->AddRange(__mcTemp__1);

		}
	};
}
