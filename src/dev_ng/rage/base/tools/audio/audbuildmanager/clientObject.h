using namespace System;
using namespace System::IO;
using namespace System::Net::Sockets;

public __gc class clientObject
{
private:
	TcpClient *m_ClientSocket;
	NetworkStream *m_ClientStream;

public:
	clientObject(TcpClient *clientSocket, NetworkStream *clientStream);
	TcpClient *GetSocket();
	NetworkStream *GetStream();
};


