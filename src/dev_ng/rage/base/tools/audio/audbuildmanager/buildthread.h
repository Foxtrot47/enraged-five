//
// tools/audbuildmanager/buildthread.h
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved.
//

#ifndef BUILD_THREAD_H
#define BUILD_THREAD_H

using namespace audAssetManagement;
using namespace audBuildCommon;
using namespace Microsoft::Win32;
using namespace rage;
using namespace System;
using namespace System::Diagnostics;
using namespace System::IO;
using namespace System::Net::Sockets;
using namespace System::Xml;


//
// PURPOSE
//  A worker thread that performs the full wave asset build, including interaction with the asset management
//	system.
// SEE ALSO
//  audAssetManager, audBuildException
//
public __gc class audBuildThread
{
public:

	//
	// PURPOSE
	//  Worker function, called when the thread is started by the audServerThread.
	// SEE ALSO
	//  audServerThread
	//
	void ThreadProc(Object* stateInfo);

	audBuildClient *m_BuildClient;
	EventLog *m_EventLog;
	RegistryKey *m_RegKey;
	//String *m_DefaultSettings[];
	String *m_LocalHostname;

private:
	//
	// PURPOSE
	//  Initializes the build process by reading settings from the registry and initializing the audAssetManager.
	// SEE ALSO
	//  audAssetManager::Init
	//
	void Init(void);
	//
	// PURPOSE
	//  Parses an array of strings containing build settings.
	// PARAMS
	//  buildSettings - An array of strings containing build settings in the form of paired switches and values.
	//
	void ParseBuildSettings(String *buildSettings[]);
	//
	// PURPOSE
	//  Updates and locks the Pending Wave List and Built Waves List within the asset management system.
	// SEE ALSO
	//  audAssetManager
	//
	void PrepareAssets(void);
	//
	// PURPOSE
	//  Loads the Pending Waves and the Built Waves List XML.
	//
	void LoadWaveLists(void);
	//
	// PURPOSE
	//  Writes the Pending Waves and the Built Waves List XML back to files (post-build.)
	//
	void SaveWaveLists(void);
	//
	// PURPOSE
	//  Undoes the check-out of the Pending Wave List and Built Waves List from the asset management system.
	// SEE ALSO
	//  audAssetManager
	//
	void ReleaseAssets(void);
	//
	// PURPOSE
	//  Checks-in the Pending Wave List and Built Waves List (checking-in the Pending Wave List last).
	// SEE ALSO
	//  audAssetManager
	//
	void CommitAssets(void);
	//
	// PURPOSE
	//  Shuts-down the audAssetManager.
	// SEE ALSO
	//  audAssetManager
	//
	void Shutdown(void);
	bool m_IsLocalBuild;
	bool m_IsDeferredBuild;
	int m_AssetManagerType;
	static Object *lockObj = new Object();
	String *m_AssetServerName, *m_AssetUserName, *m_AssetProjectName, *m_AssetPasswordStr, *m_AssetProjectSettingsPath;
	String *m_BuildPlatform;
	String *m_AssetBuildInfoPath, *m_LocalBuildInfoPath;
	XmlDocument *m_PendingWavesXml, *m_BuiltWavesXml;
	XmlDocument *m_ConstPendingWavesXml;	// it's not const, but lets pretend it is...
	audAssetManager *m_AssetManager;
	audProjectSettings *m_ProjectSettings;

	NetworkStream *m_ClientStream;
	TcpClient *m_ClientSocket;
};

#endif // BUILD_THREAD_H
