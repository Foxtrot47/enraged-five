//
// tools/audbuildmanager/serverthread.h
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved.
//
#ifndef SERVER_THREAD_H
#define SERVER_THREAD_H

using namespace System;
using namespace System::Diagnostics;
using namespace System::Threading;

//
// PURPOSE
//  A worker thread that handles client connections to the Build Manager service, limiting to one
//	simultaneous client connection and spawning an audBuildThread to handle each build request.
// SEE ALSO
//  audBuildThread
//
public __gc class audServerThread
 {
 public:
	//
	// PURPOSE
	//  Worker function, called when the thread is started by the service.
	// SEE ALSO
	//  audbuildmanagerWinService
	//
	void ThreadProc();
	EventLog *m_EventLog;
	//String *m_DefaultSettings[];
 };

#endif // SERVER_THREAD_H
