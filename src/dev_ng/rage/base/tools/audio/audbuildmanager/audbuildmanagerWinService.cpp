//
// tools/audbuildmanager/audbuildmanagerWinService.cpp
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved.
//

#include "stdafx.h"

#include "audbuildmanagerWinService.h"

#include "serverthread.h"
#include <string.h>

using namespace audbuildmanager;
using namespace System::Text;

//To install the service, type: "audbuildmanager.exe -Install"
int _tmain(int argc, _TCHAR* argv[])
{
	if (argc >= 2)
	{
		if (argv[1][0] == _T('/'))
		{
			argv[1][0] = _T('-');
		}

		if (_tcsicmp(argv[1], _T("-Install")) == 0)
		{
			//Install this Windows Service using InstallUtil.exe
			String* myargs[] = System::Environment::GetCommandLineArgs();
			String* args[] = new String*[myargs->Length - 1];
			args[0] = (myargs[0]);
			Array::Copy(myargs, 2, args, 1, args->Length - 1);
			AppDomain* dom = AppDomain::CreateDomain(S"execDom");
			Type* type = __typeof(System::Object);
			String* path = type->get_Assembly()->get_Location();
			StringBuilder* sb = new StringBuilder(path->Substring(0, path->LastIndexOf(S"\\")));
			sb->Append(S"\\InstallUtil.exe");
			dom->ExecuteAssembly(sb->ToString(), 0, args);
		}

		else if (_tcsicmp(argv[1], _T("-Uninstall")) == 0)
		{
			//Install this Windows Service using InstallUtil.exe
			String* myargs[] = System::Environment::GetCommandLineArgs();
			String* args[] = new String*[myargs->Length];
			args[0] = S"-u";
			args[1] = (myargs[0]);
			AppDomain* dom = AppDomain::CreateDomain(S"execDom");
			Type* type = __typeof(System::Object);
			String* path = type->get_Assembly()->get_Location();
			StringBuilder* sb = 
				new StringBuilder(path->Substring(0, path->LastIndexOf(S"\\")));
			sb->Append(S"\\InstallUtil.exe");
			dom->ExecuteAssembly(sb->ToString(), 0, args);
		}

		else if (_tcsicmp(argv[1], _T("-CommandLine")) == 0)
		{
			Console::WriteLine("audBuildManager: running in command line mode");
			//Command-line debug support.
			audServerThread *serverThreadInstance = new audServerThread();
			Thread *serverThread = new Thread(new ThreadStart(serverThreadInstance,
				&audServerThread::ThreadProc));

			System::Diagnostics::EventLog *eventLog = new System::Diagnostics::EventLog();
			(__try_cast<System::ComponentModel::ISupportInitialize *  >(eventLog))->BeginInit();
			eventLog->Log = S"Application";
			eventLog->Source = S"Audio Build Manager";
			//eventLog->EntryWritten += new System::Diagnostics::EntryWrittenEventHandler(this, eventLog1_EntryWritten);
			(__try_cast<System::ComponentModel::ISupportInitialize *  >(eventLog))->EndInit();

			serverThreadInstance->m_EventLog = eventLog;
			//Start the thread.  On a uniprocessor, the thread does not get 
			//any processor time until the main thread yields.
			serverThread->Start();
			Thread::Sleep(0);
		}
	}
	else 
	{
		ServiceBase::Run(new audbuildmanagerWinService());    
	}
	return 0;
}

void audbuildmanagerWinService::OnStart(String* /*args*/[])
{
	audServerThread *serverThreadInstance = new audServerThread();
	m_ServerThread = new Thread(new ThreadStart(serverThreadInstance,
		&audServerThread::ThreadProc));
	serverThreadInstance->m_EventLog = MyEventLog;
	//Start the thread.  On a uniprocessor, the thread does not get 
	//any processor time until the main thread yields.
	m_ServerThread->Start();
	Thread::Sleep(0);
}

void audbuildmanagerWinService::OnStop()
{
	if(m_ServerThread)
	{
		m_ServerThread->Abort();
		//Wait for the thread to terminate.
        m_ServerThread->Join();
		m_ServerThread = NULL;
	}
}
