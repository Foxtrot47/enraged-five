//
// tools/audbuildmanager/buildthread.cpp
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved.
//

#include "stdafx.h"
#include "buildthread.h"
#include "builddefs.h"
#include "assetmanagertypes.h"
#include "clientObject.h"

using namespace System::Reflection;
using namespace System::Text;
using namespace System::Threading;

//The ThreadProc method is called when the thread starts.
void audBuildThread::ThreadProc(Object* stateInfo)
{

	clientObject *clientObj = __try_cast<clientObject *>(stateInfo);
	
	enum BUILDSTATE {SUCCESSFUL,EXCEPTION_THREAD_ABORT,EXCEPTION_HANDLED,EXCEPTION_UNHANDLED};
	BUILDSTATE buildState = SUCCESSFUL;
	String *outputMessage;

	bool gotLock = false;
	gotLock = Monitor::TryEnter(lockObj,5000);
	if(gotLock){
		try
		{		
			////lock here
			
			Byte responseBytes[] = Text::Encoding::ASCII->GetBytes(S"CONNECTED\n");

			try
			{
					clientObj->GetStream()->Write(responseBytes, 0, responseBytes->Length);
			}
			catch(Exception *)
			{
				throw new audBuildException(S"Lost connection with client on start up, client should retry");
			}
			
			m_AssetManager = NULL;
			m_BuildClient = NULL;

			m_BuildClient = new audBuildClient(clientObj->GetStream(),m_EventLog);

			Init();	
			PrepareAssets();
			LoadWaveLists();

			//Perform pre-build steps.
			IEnumerator* preBuilderEnum = m_ProjectSettings->GetPreBuilders()->GetEnumerator();
			while(preBuilderEnum->MoveNext())
			{
				audBuildComponent *preBuildComponent = __try_cast<audBuildComponent *>(preBuilderEnum->Current);
				if(preBuildComponent && preBuildComponent->m_LibraryPath && preBuildComponent->m_InstanceType)
				{
					Assembly *preBuilderAssembly = Assembly::LoadFile(preBuildComponent->m_LibraryPath);
					audPreBuilderBase *preBuilder = dynamic_cast<audPreBuilderBase *>(preBuilderAssembly->CreateInstance(
						preBuildComponent->m_InstanceType));
					if(preBuilder)
					{
						preBuilder->Build(m_AssetManager, m_ProjectSettings, m_BuildPlatform,
							m_BuildClient, m_PendingWavesXml,m_BuiltWavesXml, m_IsLocalBuild, preBuildComponent, m_IsDeferredBuild);
					}
					else
					{
						throw new audBuildException(String::Concat("Couldn't instantiate pre-build step: ",
							preBuildComponent->m_InstanceType));
					}
				}
			}

			if(m_ProjectSettings->GetPreBuilders()->Count > 0)
			{
				// Update const pending waves to reflect changes made by the pre build step
				//Load the *const* Pending Waves List.
				m_ConstPendingWavesXml->DocumentElement->RemoveAll();
				for(int i =0;i<m_PendingWavesXml->DocumentElement->ChildNodes->Count;i++)
				{
					XmlNode *newNode = m_ConstPendingWavesXml->ImportNode(m_PendingWavesXml->DocumentElement->ChildNodes->Item(i)->CloneNode(true),true);
					m_ConstPendingWavesXml ->DocumentElement->AppendChild(newNode);
				}
			}


			//Perform build steps.
			IEnumerator* builderEnum = m_ProjectSettings->GetBuilders()->GetEnumerator();
			while(builderEnum->MoveNext())
			{
				audBuildComponent *buildComponent = __try_cast<audBuildComponent *>(builderEnum->Current);
				if(buildComponent && buildComponent->m_LibraryPath && buildComponent->m_InstanceType)
				{
					Assembly *builderAssembly = Assembly::LoadFile(buildComponent->m_LibraryPath);
					audBuilderBase *builder = dynamic_cast<audBuilderBase *>(builderAssembly->CreateInstance(
						buildComponent->m_InstanceType));
					if(builder)
					{
						builder->Build(m_AssetManager, m_ProjectSettings, m_BuildPlatform, m_BuildClient,
							m_PendingWavesXml, m_BuiltWavesXml, m_IsLocalBuild, buildComponent, m_IsDeferredBuild);
					}
					else
					{
						throw new audBuildException(String::Concat("Couldn't instantiate build step: ",
							buildComponent->m_InstanceType));
					}
				}
			}

			// save wave lists after main build steps (but before post-build)
			SaveWaveLists();

			// check in wave lists
			CommitAssets();

			//Perform post-build steps.
			IEnumerator* postBuilderEnum = m_ProjectSettings->GetPostBuilders()->GetEnumerator();
			while(postBuilderEnum->MoveNext())
			{
				audBuildComponent *postBuildComponent = __try_cast<audBuildComponent *>(postBuilderEnum->Current);
				if(postBuildComponent && postBuildComponent->m_LibraryPath && postBuildComponent->m_InstanceType)
				{
					Assembly *postBuilderAssembly = Assembly::LoadFile(postBuildComponent->m_LibraryPath);
					System::Object *obj = postBuilderAssembly->CreateInstance(postBuildComponent->m_InstanceType);
					audPostBuilderBase *postBuilder = dynamic_cast<audPostBuilderBase *>(obj);
					if(postBuilder)
					{
						try
						{
							postBuilder->Build(m_AssetManager, m_ProjectSettings, m_BuildPlatform,
								m_BuildClient, m_BuiltWavesXml, m_ConstPendingWavesXml, m_IsLocalBuild, postBuildComponent, m_IsDeferredBuild);
						}
						catch(audBuildException *e)
						{
							throw(e);
						}
						catch(Exception *e)
						{
							audBuildException *exception = new audBuildException(e->ToString());
							if(m_IsDeferredBuild){
								m_BuildClient->SendMessage(exception->m_clientErrorMsg);
								m_EventLog->WriteEntry(exception->m_logErrorMsg, EventLogEntryType::Error);
							}
							else
							{
								throw exception;
							}
						}
					}
					else
					{
						throw new audBuildException(String::Concat("Couldn't instantiate post-build step: ",
							postBuildComponent->m_InstanceType));
					}
				}
			}

			Shutdown();

		}


		catch(ThreadAbortException* ) //Catch thread abort exceptions.
		{
			buildState = EXCEPTION_THREAD_ABORT;
			outputMessage = S"Build was Aborted";
		}
		catch(audBuildException *exception)
		{
			buildState = EXCEPTION_HANDLED;
			outputMessage = exception->m_clientErrorMsg;
		}
		catch(Exception *exception)
		{
			buildState = EXCEPTION_UNHANDLED;
			outputMessage = exception->ToString();
		}
		
		if(buildState!=SUCCESSFUL)
		{
			
			try
			{
				if(m_BuildClient)
				{
					m_BuildClient->SendMessage(outputMessage);
				}			
				m_EventLog->WriteEntry(outputMessage, EventLogEntryType::Error);
			}
			catch(Exception *e)
			{
				Console::WriteLine(String::Concat("Failed to write out errors: ",e->ToString()));
			}
		
			if(m_AssetManager)
			{
				ReleaseAssets();
				Shutdown();
			}
		}
		
			m_EventLog->WriteEntry(S"Build complete");
	
			if(m_BuildClient)
			{
				m_BuildClient->SendMessage(S"DONE\n");
			}
	}
	//end of locked code
	else
	{
		/////Busy message
		String *msg = String::Concat("BUSY ", m_LocalHostname, "\n");			
		Byte responseBytes[] = Text::Encoding::ASCII->GetBytes(msg);
		try
		{
		clientObj->GetStream()->Write(responseBytes, 0, responseBytes->Length);
		clientObj->GetStream()->Close();
		}
		catch(Exception *)
		{
			m_EventLog->WriteEntry(S"Could not send busy message back to client");
		}
	}


	//clean up connection
	if(clientObj && clientObj->GetStream())
	{
		clientObj->GetStream()->Close();
	}

	m_BuildClient = NULL;
	
	if(gotLock)
	{
		Monitor::Exit(lockObj);
	}

}

void audBuildThread::Init(void)
{
	//Check for debug defaults (entered as service start parameters.)
	bool gotSettings = false;
	String *delimiters = S"=,";


	//Wait for client to send build settings.
	String *responseStr = m_BuildClient->WaitForReponse();
	if(responseStr != NULL)
	{
		if(responseStr->StartsWith(S"CONFIG "))
		{
			responseStr = responseStr->Substring(7); //Remove 'CONFIG ' preamble.
			String *delim = S"\r\n\0 ";
			responseStr = responseStr->Trim(delim->ToCharArray());
			String *clientSettings[] = responseStr->Split(delimiters->ToCharArray());
			if(clientSettings->get_Count() == 20)
			{
				ParseBuildSettings(clientSettings);
				m_EventLog->WriteEntry(String::Concat(S"Building - '", responseStr, S"'"),
					EventLogEntryType::Information);
				gotSettings = true;
			}
		}
	}


	if(!gotSettings)
	{
		audBuildException *exception = new audBuildException(S"Invalid client build settings");
		throw(exception);
	}

	m_BuildClient->ReportProgress(-1, -1, S"Connecting to Asset Management System", true);
	m_AssetManager = audAssetManager::CreateInstance(m_AssetManagerType);
	m_EventLog->WriteEntry(Environment::get_UserName());
	m_BuildClient->ReportProgress(-1, -1, S"Logging-in to Asset Management Project", true);
	//Login.
	if(!m_AssetManager || !m_AssetManager->Init(m_AssetServerName, m_AssetProjectName, m_AssetUserName,
		m_AssetPasswordStr, m_IsLocalBuild))
	{
		audBuildException *exception = new audBuildException(S"Failed to initialise Asset Manager");
		throw(exception);
	}

	String *trimmedAssetProjectSettingsPath = m_AssetProjectSettingsPath->Substring(0, m_AssetProjectSettingsPath->LastIndexOf('\\'));
	String *workingProjectSettingsPath = m_AssetManager->GetWorkingPath(trimmedAssetProjectSettingsPath);
	if(workingProjectSettingsPath == 0)
	{
		audBuildException *exception = new audBuildException(S"Failed to find working path for project settings");
		throw(exception);
	}

	//Ensure that project settings working paths exists locally.
	Directory::CreateDirectory(workingProjectSettingsPath);

	//Get latest project settings.
	if(!m_AssetManager->GetLatest(m_AssetManager->GetWorkingPath(m_AssetProjectSettingsPath)))
	{
		audBuildException *exception = new audBuildException(S"Failed to get latest project settings");
		throw(exception);
	}

	String *workingProjectSettingsFullPath = m_AssetManager->GetWorkingPath(m_AssetProjectSettingsPath);
	if(workingProjectSettingsFullPath == 0)
	{
		audBuildException *exception = new audBuildException(S"Failed to find full working path for project settings");
		throw(exception);
	}

	//Load (updated) local project settings.
	m_ProjectSettings = new rage::audProjectSettings(workingProjectSettingsFullPath);
	//Set the requested build platform.
	m_ProjectSettings->SetCurrentPlatformByTag(m_BuildPlatform);

	m_EventLog->WriteEntry(S"Build started");
	m_BuildClient->SendMessage(S"START\n");
	m_AssetManager->CreateCurrentChangelist(String::Concat(S"WAVE Build performed by ",m_LocalHostname));

	//Generate commonly-used paths.
	m_AssetBuildInfoPath = m_ProjectSettings->GetBuildInfoPath();
	m_LocalBuildInfoPath = m_AssetManager->GetWorkingPath(m_AssetBuildInfoPath);
	if(m_LocalBuildInfoPath == 0)
	{
		audBuildException *exception = new audBuildException(S"Failed to find working path for Build Info folder");
		throw(exception);
	}
}

void audBuildThread::ParseBuildSettings(String *buildSettings[])
{
	int count = buildSettings->get_Count();

	int index = Array::IndexOf(buildSettings, S"server");
	if((index > -1) && (index < count - 1))
	{
		m_AssetServerName = __try_cast<String *>(buildSettings->get_Item(index + 1));
	}
	index = Array::IndexOf(buildSettings, S"project");
	if((index > -1) && (index < count - 1))
	{
		m_AssetProjectName = __try_cast<String *>(buildSettings->get_Item(index + 1));
	}
	index = Array::IndexOf(buildSettings, S"username");
	if((index > -1) && (index < count - 1))
	{
		m_AssetUserName = __try_cast<String *>(buildSettings->get_Item(index + 1));
	}
	index = Array::IndexOf(buildSettings, S"password");
	if((index > -1) && (index < count - 1))
	{
		m_AssetPasswordStr = __try_cast<String *>(buildSettings->get_Item(index + 1));
	}
	index = Array::IndexOf(buildSettings, S"projectsettings");
	if((index > -1) && (index < count - 1))
	{
		m_AssetProjectSettingsPath = __try_cast<String *>(buildSettings->get_Item(index + 1));
	}
	index = Array::IndexOf(buildSettings, S"buildplatform");
	if((index > -1) && (index < count - 1))
	{
		m_BuildPlatform = __try_cast<String *>(buildSettings->get_Item(index + 1));
	}
	index = Array::IndexOf(buildSettings, S"localbuild");
	if((index > -1) && (index < count - 1))
	{
		String *buildLocal = __try_cast<String *>(buildSettings->get_Item(index + 1));
		m_IsLocalBuild = buildLocal->ToUpper()->Equals(S"YES");
	}
	index = Array::IndexOf(buildSettings, S"assetmanagertype");
	if((index > -1) && (index < count - 1))
	{
		String *assetManagerType = __try_cast<String *>(buildSettings->get_Item(index + 1));
		m_AssetManagerType = Int32::Parse(assetManagerType);
	}
	index = Array::IndexOf(buildSettings, S"localhost");
	if ((index > -1) && (index < count - 1))
	{
		m_LocalHostname = __try_cast<String*>(buildSettings->get_Item(index + 1));
	}
	index = Array::IndexOf(buildSettings, S"deferred");
	if((index > -1) && (index < count - 1))
	{
		String *buildDeferred = __try_cast<String *>(buildSettings->get_Item(index + 1));
		m_IsDeferredBuild = buildDeferred->ToUpper()->Equals(S"YES");
	}
}

void audBuildThread::PrepareAssets(void)
{
	m_BuildClient->ReportProgress(-1, -1, S"Checking-out wave lists", true);

	//Ensure that all output paths exist locally.
	Directory::CreateDirectory(m_LocalBuildInfoPath);

	if(!m_IsLocalBuild && m_AssetManager->ExistsAsAsset(String::Concat(m_AssetBuildInfoPath, g_PendingWaveListFilename)))
	{
		//Check if Pending Wave List is locked by another user, listening to client response if required.
		bool retry;
		do
		{
			retry = false;
			if(m_AssetManager->IsLocked(String::Concat(m_AssetBuildInfoPath, g_PendingWaveListFilename)))
			{
				//Ask client whether or not to retry.
				m_BuildClient->SendMessage(S"QUERY \"Assets locked by another user - retry?\"\n");

				String *responseStr = m_BuildClient->WaitForReponse();
				if((responseStr != NULL) && responseStr->StartsWith(S"YES"))
				{
					retry = true;
				}

				if(!retry)
				{
					//Client does not wish to retry, so terminate build.
					audBuildException *exception = new audBuildException(S"Build was aborted");
					throw(exception);
				}
			}
		} while(retry);

		//Check-out Pending Wave List (if necessary).
		if(!m_AssetManager->IsCheckedOut(String::Concat(m_AssetBuildInfoPath, g_PendingWaveListFilename)))
		{
			if(!m_AssetManager->CheckOut(String::Concat(m_AssetBuildInfoPath, g_PendingWaveListFilename),
				S"Building Waves"))
			{
				audBuildException *exception = new audBuildException(
					S"Failed to check-out Pending Wave List");
				throw(exception);
			}
		}
	}

	//Check-out Built Waves Log (if necessary).
	if(!m_AssetManager->SimpleCheckOut(String::Concat(m_AssetBuildInfoPath, g_BuiltWavesLogFilename),
		S"Building Waves"))
	{
		audBuildException *exception = new audBuildException(
			S"Failed to check-out Built Waves Log");
		throw(exception);
	}
}

void audBuildThread::LoadWaveLists(void)
{
	try
	{
		//Load Pending Waves List.
		m_PendingWavesXml = new XmlDocument();
		m_PendingWavesXml->Load(String::Concat(m_LocalBuildInfoPath, g_PendingWaveListFilename));
		XmlElement *pendingRoot = m_PendingWavesXml->get_DocumentElement();
		if(!pendingRoot || !pendingRoot->get_Name()->Equals(S"PendingWaves"))
		{
			audBuildException *exception = new audBuildException(S"Invalid Pending Waves XML file");
			throw(exception);
		}

		//Load the *const* Pending Waves List.
		m_ConstPendingWavesXml = new XmlDocument();
		m_ConstPendingWavesXml->Load(String::Concat(m_LocalBuildInfoPath, g_PendingWaveListFilename));
		XmlElement *constPendingRoot = m_ConstPendingWavesXml->get_DocumentElement();
		if (!constPendingRoot || !constPendingRoot->get_Name()->Equals(S"PendingWaves"))
		{
			audBuildException *exception = new audBuildException(S"Invalid Pending Waves XML file");
			throw(exception);
		}

		//Load Built Waves Log if it exists.
		m_BuiltWavesXml = new XmlDocument();
		FileInfo *BuiltWavesFileInfo = new FileInfo(String::Concat(m_LocalBuildInfoPath, g_BuiltWavesLogFilename));
		if(BuiltWavesFileInfo->Exists)
		{
			m_BuiltWavesXml->Load(String::Concat(m_LocalBuildInfoPath, g_BuiltWavesLogFilename));
			XmlElement *builtRoot = m_BuiltWavesXml->get_DocumentElement();
			if(!builtRoot || !builtRoot->get_Name()->Equals(S"BuiltWaves"))
			{
				audBuildException *exception = new audBuildException(S"Invalid Built Waves XML file");
				throw(exception);
			}
		}
		else
		{
			//Generate new Built Waves Log.
			m_BuiltWavesXml->AppendChild(m_BuiltWavesXml->CreateElement(S"BuiltWaves"));
		}
	}
	catch(DirectoryNotFoundException *e)
	{
		audBuildException *exception = new audBuildException(
			S"No Pending Waves to be built");
		e = NULL;
		throw(exception);
	}
	catch(FileNotFoundException *e)
	{
		audBuildException *exception = new audBuildException(
			S"No Pending Waves to be built");
		e = NULL;
		throw(exception);
	}
}

void audBuildThread::SaveWaveLists(void)
{
	if(m_IsLocalBuild)
	{
		return;
	}

	try
	{
		m_PendingWavesXml->Save(String::Concat(m_LocalBuildInfoPath, g_PendingWaveListFilename));
		m_BuiltWavesXml->Save(String::Concat(m_LocalBuildInfoPath, g_BuiltWavesLogFilename));
	}
	catch(DirectoryNotFoundException *e)
	{
		audBuildException *exception = new audBuildException(S"Error writing Wave List XML");
		e = NULL;
		throw(exception);
	}
	catch(FileNotFoundException *e)
	{
		audBuildException *exception = new audBuildException(S"Error writing Wave List XML");
		e = NULL;
		throw(exception);
	}
}

void audBuildThread::ReleaseAssets(void)
{
	m_BuildClient->ReportProgress(-1, -1, S"Undoing check-out of wave lists", true);

	//
	//Deal with Wave Banks in wave build process.
	//

	//Undo check-out of Built Waves Log.
	if (m_AssetManager->GetAssetManagementType() != audAssetManagement::ASSET_MANAGER_PERFORCE)
	{
		if(!m_AssetManager->SimpleUndoCheckOut(String::Concat(m_AssetBuildInfoPath, g_BuiltWavesLogFilename)))
		{
			audBuildException *exception = new audBuildException(S"Failed to undo check-out of Built Waves Log");
			throw(exception);
		}

		//Undo check-out of Pending Wave List.
		if(!m_AssetManager->SimpleUndoCheckOut(String::Concat(m_AssetBuildInfoPath, g_PendingWaveListFilename)))
		{
			audBuildException *exception = new audBuildException(S"Failed to undo check-out of Pending Wave List");
			throw(exception);
		}
	}

	//Remove all files from the pending changelist and delete it.
	m_AssetManager->DeleteCurrentChangelist();
}

void audBuildThread::CommitAssets(void)
{
	m_BuildClient->ReportProgress(-1, -1, S"Checking-in / importing wave lists", true);

	//
	//Deal with Wave Banks in wave build process.
	//

	//Check that the parent asset folder exists in the project database.
	if(m_AssetManager->ExistsAsAsset(m_AssetBuildInfoPath))
	{
		//Check-in Built Waves Log.
		if(!m_AssetManager->CheckInOrImport(String::Concat(m_LocalBuildInfoPath, g_BuiltWavesLogFilename),
			String::Concat(m_AssetBuildInfoPath, g_BuiltWavesLogFilename), S"Wave Build"))
		{
			audBuildException *exception = new audBuildException(S"Failed to check-in/import Built Waves Log");
			throw(exception);
		}

		//Check-in Pending Wave List.
		if(!m_AssetManager->CheckInOrImport(String::Concat(m_LocalBuildInfoPath, g_PendingWaveListFilename),
			String::Concat(m_AssetBuildInfoPath, g_PendingWaveListFilename), S"Wave Build"))
		{
			audBuildException *exception = new audBuildException(S"Failed to check-in/import Pending Wave List");
			throw(exception);
		}
	}
	else //Parent asset folder does not exist in the project database, so simply import the parent folder.
	{
		if(!m_AssetManager->Import(m_LocalBuildInfoPath, m_AssetBuildInfoPath->Substring(0,
			m_AssetBuildInfoPath->LastIndexOf('\\')), S"Wave Build"))
		{
			audBuildException *exception = new audBuildException(
				S"Failed to import BuildInfo folder");
			throw(exception);
		}
	}
}

void audBuildThread::Shutdown(void)
{
	m_AssetManager->CheckInCurrentChangelist();
	m_AssetManager->Shutdown();
}
