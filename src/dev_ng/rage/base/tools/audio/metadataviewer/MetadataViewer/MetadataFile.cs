﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MetadataFile.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The metadata object.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MetadataViewer
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Windows;
    using System.Xml.Linq;

    using BinaryReader = rage.ToolLib.Reader.BinaryReader;

    /// <summary>
    /// The metadata object.
    /// </summary>
    public class MetadataObject
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        public byte[] Data { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the name hash.
        /// </summary>
        public uint NameHash { get; set; }

        /// <summary>
        /// Gets or sets the offset.
        /// </summary>
        public uint Offset { get; set; }

        /// <summary>
        /// Gets or sets the size.
        /// </summary>
        public uint Size { get; set; }

        /// <summary>
        /// Gets the type id.
        /// </summary>
        public byte TypeId
        {
            get
            {
                if (this.Data != null)
                {
                    return this.Data[0];
                }
                else
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// Gets or sets the type name.
        /// </summary>
        public string TypeName { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The to string.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public override string ToString()
        {
            return this.Name;
        }

        #endregion
    }

    /// <summary>
    /// The metadata file.
    /// </summary>
    public class MetadataFile
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MetadataFile"/> class.
        /// </summary>
        /// <param name="filePath">
        /// The file path.
        /// </param>
        /// <param name="isBigEndian">
        /// The is big endian.
        /// </param>
        /// <param name="schemaPath">
        /// The schema path.
        /// </param>
        public MetadataFile(string filePath, bool isBigEndian, string schemaPath)
        {
            this.FileName = filePath;
            this.IsBigEndian = isBigEndian;
            this.SchemaPath = schemaPath;

            try
            {
                var br = new BinaryReader(new FileStream(filePath, FileMode.Open, FileAccess.Read), isBigEndian);
                this.SchemaVersion = br.ReadUInt();
                this.DataSize = br.ReadUInt();

                // Skip over the data
                br.Seek(this.DataSize, SeekOrigin.Current);

                this.LoadStringTable(br);
                this.LoadObjects(br);
                this.LoadObjectData(br, schemaPath);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed to open file: " + ex.Message);
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the data size.
        /// </summary>
        public uint DataSize { get; private set; }

        /// <summary>
        /// Gets the file name.
        /// </summary>
        public string FileName { get; private set; }

        /// <summary>
        /// Gets a value indicating whether is big endian.
        /// </summary>
        public bool IsBigEndian { get; private set; }

        /// <summary>
        /// Gets the schema path.
        /// </summary>
        public string SchemaPath { get; private set; }

        /// <summary>
        /// Gets the object lookup.
        /// </summary>
        public Dictionary<string, MetadataObject> ObjectLookup { get; private set; }

        /// <summary>
        /// Gets the objects.
        /// </summary>
        public List<MetadataObject> Objects { get; private set; }

        /// <summary>
        /// Gets the schema version.
        /// </summary>
        public uint SchemaVersion { get; private set; }

        /// <summary>
        /// Gets the string table.
        /// </summary>
        public List<string> StringTable { get; private set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Validate the current metadata file.
        /// This is only applicable to release file versions, which are compared
        /// to the development version.
        /// </summary>
        /// <returns>
        /// Value indicating whether metadata file is valid <see cref="bool"/>.
        /// </returns>
        public bool Validate()
        {
            var ext = Path.GetExtension(this.FileName);
            if (null == ext || !ext.ToLower().EndsWith(".rel"))
            {
                // Only want to validate release file versions
                return true;
            }

            // Remove the .rel extension
            var devFilePath = this.FileName.Substring(0, this.FileName.Length - 4);

            if (!File.Exists(devFilePath))
            {
                MessageBox.Show(
                    "Failed to find dev version of file: " + devFilePath,
                    "Validation Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return false;
            }

            // Check for any objects
            if (!this.Objects.Any())
            {
                MessageBox.Show(
                    "No objects found in metadata file",
                    "Validation Warning",
                    MessageBoxButton.OK,
                    MessageBoxImage.Warning);
                return true;
            }

            var devFile = new MetadataFile(devFilePath, this.IsBigEndian, this.SchemaPath);

            // Check both files have same number of objects
            if (this.Objects.Count != devFile.Objects.Count)
            {
                MessageBox.Show(
                    "Release version of file has " + this.Objects.Count + " object(s) but the dev version has "
                    + devFile.Objects.Count + " object(s) - these should match",
                    "Validation Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return false;
            }

            // Check file sizes
            var relFileSize = new FileInfo(this.FileName).Length;
            var devFileSize = new FileInfo(devFilePath).Length;

            // We know that the release file doesn't include 3 byte name table offset for each object
            var knownDiff = 3 * this.Objects.Count;

            if (devFileSize != relFileSize + knownDiff)
            {
                MessageBox.Show(
                    "Release file size (" + relFileSize + " bytes) does not match expected size (" + (devFileSize - knownDiff)
                    + " bytes)",
                    "Validation Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return false;
            }

            // Compare objects
            for (var i = 0; i < this.Objects.Count; i++)
            {
                var relData = this.Objects[i].Data;
                var devData = devFile.Objects[i].Data;

                // Remove 3 byte name table offset from dev data
                var tmp = devData.ToList();
                tmp.RemoveRange(1, 3);
                devData = tmp.ToArray();

                // Check data lengths are now equal
                if (relData.Length != devData.Length)
                {
                    MessageBox.Show(
                        "Object data lengths do not match: " + this.Objects[i].Name,
                        "Validation Failed",
                        MessageBoxButton.OK,
                        MessageBoxImage.Error);
                    return false;
                }

                // Compare byte by byte
                var comparer = EqualityComparer<byte>.Default;
                for (var j = 0; j < relData.Length; j++)
                {
                    if (!comparer.Equals(relData[j], devData[j]))
                    {
                        MessageBox.Show(
                        "Byte " + j + " of object data does not match dev version: " + this.Objects[i].Name,
                        "Validation Failed",
                        MessageBoxButton.OK,
                        MessageBoxImage.Error);
                        return false;
                    }
                }
            }

            return true;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The load object data.
        /// </summary>
        /// <param name="br">
        /// The br.
        /// </param>
        /// <param name="schemaPath">
        /// The schema path.
        /// </param>
        private void LoadObjectData(BinaryReader br, string schemaPath)
        {
            var typeNames = new List<string>();
            if (schemaPath != null)
            {
                var schemaFile = XDocument.Load(schemaPath);
                foreach (var e in schemaFile.Root.Elements("TypeDefinition"))
                {
                    typeNames.Add(e.Attribute("name").Value);
                }
            }

            foreach (var o in this.Objects)
            {
                br.Seek(o.Offset + 8, SeekOrigin.Begin);
                o.Data = br.ReadBytes((int)o.Size, false);
                if (typeNames.Count > o.TypeId)
                {
                    o.TypeName = typeNames[o.TypeId];
                }
            }
        }

        /// <summary>
        /// The load objects.
        /// </summary>
        /// <param name="br">
        /// The br.
        /// </param>
        private void LoadObjects(BinaryReader br)
        {
            uint numObjects = br.ReadUInt();
            uint stringSize = br.ReadUInt();

            this.Objects = new List<MetadataObject>((int)numObjects);
            this.ObjectLookup = new Dictionary<string, MetadataObject>((int)numObjects);

            for (uint i = 0; i < numObjects; i++)
            {
                var nameLength = br.ReadByte();
                var nameBytes = br.ReadBytes(nameLength, false);
                var m = new MetadataObject
                {
                    Name = System.Text.Encoding.ASCII.GetString(nameBytes),
                    Offset = br.ReadUInt(),
                    Size = br.ReadUInt()
                };

                this.Objects.Add(m);

                this.ObjectLookup.Add(m.Name, m);
            }
        }

        /// <summary>
        /// The load string table.
        /// </summary>
        /// <param name="br">
        /// The br.
        /// </param>
        private void LoadStringTable(BinaryReader br)
        {
            uint stringTableSize = br.ReadUInt();
            uint numStrings = br.ReadUInt();

            br.Seek(numStrings * 4, SeekOrigin.Current);

            this.StringTable = new List<string>((int)numStrings);
            for (uint i = 0; i < numStrings; i++)
            {
                this.StringTable.Add(br.ReadCString());
            }
        }

        #endregion
    }
}