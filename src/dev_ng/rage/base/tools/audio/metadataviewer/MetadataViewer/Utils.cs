﻿// -----------------------------------------------------------------------
// <copyright file="Utils.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace MetadataViewer
{
    using System;

    /// <summary>
    /// Utils.
    /// </summary>
    public class Utils
    {
        /// <summary>
        /// Reverse byte order (32-bit).
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// Reversed 32-bit <see cref="uint"/>.
        /// </returns>
        public static uint ReverseBytes(uint value)
        {
            return (value & 0x000000FFU) << 24 | (value & 0x0000FF00U) << 8 |
                   (value & 0x00FF0000U) >> 8 | (value & 0xFF000000U) >> 24;
        }
    }
}
