﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Interaction logic for MainWindow.xaml
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using MetadataLib;
using rage;

namespace MetadataViewer
{
    using System.IO;
    using System.Windows;
    using System.Configuration;

    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Fields

        /// <summary>
        /// The m_ metadata file.
        /// </summary>
        private MetadataFile m_MetadataFile;

        private audProjectSettings projectSettings = null;


        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();
            try
            {
                projectSettings = new audProjectSettings(ConfigurationManager.AppSettings["projectSettingsPath"]);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        #endregion

        #region Methods

        static string GetDefinitionsPathForMetadataType(string metadataFilePath, audProjectSettings settings)
        {
            audMetadataFile metadataConfig = GetMetadataConfig(metadataFilePath, settings);
            if (metadataConfig == null) return "";
            foreach (audMetadataType metadataType in settings.GetMetadataTypes())
            {
                if (metadataConfig.Type.ToUpper().Equals(metadataType.Type.ToUpper()))
                {
                    return settings.GetBackupWorkingPath() + settings.GetSoundXmlPath() + metadataType.ObjectDefinitions[0].DefinitionsFile;
                }
            }
            return "";
        }

        static string GetAssetPathForMetadataType(MetadataFile metadataFile, audProjectSettings settings)
        {
            if(metadataFile == null) return "";
            audMetadataFile metadataConfig = GetMetadataConfig(metadataFile.FilePath, settings);
            if (metadataConfig == null) return "";
            else return settings.GetBackupWorkingPath() + settings.GetSoundXmlPath() + metadataConfig.DataPath;
        }

        private static audMetadataFile GetMetadataConfig(string metadataFilePath, audProjectSettings settings)
        {
            foreach(audMetadataFile metadataFileSettings in settings.GetMetadataSettings()) {
                foreach(PlatformSetting platformSettings in settings.GetPlatformSettings()) {
                    string platformSpecificMetadataFileOutput = settings.GetBackupWorkingPath() + metadataFileSettings.OutputFile.Replace("{Platform}", platformSettings.PlatformTag);
                    if(metadataFilePath.ToUpper().StartsWith(platformSpecificMetadataFileOutput.ToUpper())) {
                        return metadataFileSettings;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// The open menu item_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void OpenMenuItem_Click(object sender, RoutedEventArgs e)
        {
            var fileBrowser = new Microsoft.Win32.OpenFileDialog
            {
                Filter = "Dev (*.dat*)|*.dat*|Release (*.rel)|*.rel",
                Multiselect = false
            };

            var result = fileBrowser.ShowDialog();

            if (result != true)
            {
                return;
            }

            var filePath = fileBrowser.FileName;

            this.m_MetadataFile = new MetadataFile(
                filePath,
                true,
                GetDefinitionsPathForMetadataType(filePath, projectSettings));
            this.m_MetadataFile.Validate();

            this.objectGrid.ItemsSource = this.m_MetadataFile.Objects;
        }

        /// <summary>
        /// The save as menu item_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void SaveAsMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (null == this.m_MetadataFile)
            {
                MessageBox.Show("No metadata file currently open");
                return;
            }

            var saveDialog = new Microsoft.Win32.SaveFileDialog()
            {
                Filter = "csv (*.csv*)|*.csv",
            };

            var result = saveDialog.ShowDialog();

            if (result != true)
            {
                return;
            }

            var outputFileName = saveDialog.FileName;

            var sw = new StreamWriter(outputFileName);
            sw.WriteLine("Name,Type,Offset,Size");
            foreach (var o in this.m_MetadataFile.Objects)
            {
                sw.WriteLine("{0},{1},{2},{3}", o.Name, o.TypeName, o.Offset, o.Size);
            }

            sw.Close();
        }

        /// <summary>
        /// The view tree menu item click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void ViewTreeMenuItem_Click(object sender, RoutedEventArgs e)
        {
            var window = new ExplorerWindow(this.m_MetadataFile, GetAssetPathForMetadataType(this.m_MetadataFile, this.projectSettings));
            window.Show();
        }

        #endregion
    }
}