﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Explorer.xaml.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Interaction logic for Window1.xaml
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MetadataViewer
{
    using System.IO;
    using System.Windows;
    using System.Windows.Controls;
    using System.Xml.Linq;

    using MetadataLib;

    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class ExplorerWindow : Window
    {
        #region Fields

        /// <summary>
        /// The m_ asset path.
        /// </summary>
        private readonly string m_AssetPath;

        /// <summary>
        /// The m_ file.
        /// </summary>
        private readonly MetadataFile m_File;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ExplorerWindow"/> class.
        /// </summary>
        /// <param name="file">
        /// The file.
        /// </param>
        /// <param name="assetPath">
        /// The asset path.
        /// </param>
        public ExplorerWindow(MetadataFile file, string assetPath)
        {
            this.m_File = file;
            this.m_AssetPath = assetPath;

            this.InitializeComponent();
        }

        #endregion

        #region Methods

        /// <summary>
        /// The add objects.
        /// </summary>
        /// <param name="fileName">
        /// The file name.
        /// </param>
        /// <param name="parentItem">
        /// The parent item.
        /// </param>
        private void AddObjects(string fileName, TreeViewItem parentItem)
        {
            var doc = XDocument.Load(fileName);
            foreach (var o in doc.Root.Elements())
            {
                var tvi = new TreeViewItem();
                var name = o.Attribute("name").Value;

                uint size = 0;
                if (this.m_File.ObjectLookup.ContainsKey(name))
                {
                    size = this.m_File.ObjectLookup[name].Size;
                }

                tvi.Header = string.Format("{0} ({1})", name, size);
                tvi.Tag = size;
                parentItem.Items.Add(tvi);
            }
        }

        /// <summary>
        /// The build tree.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="items">
        /// The items.
        /// </param>
        private void BuildTree(string path, ItemCollection items)
        {
            foreach (var d in Directory.GetDirectories(path))
            {
                var tvi = new TreeViewItem();
                var fileName = d.Substring(d.LastIndexOf('\\') + 1);
                items.Add(tvi);
                this.BuildTree(d, tvi.Items);

                uint size = 0;
                foreach (TreeViewItem t in tvi.Items)
                {
                    size += (uint)t.Tag;
                }

                tvi.Header = string.Format("{0} ({1})", fileName, size);
                tvi.Tag = size;
            }

            foreach (var f in Directory.GetFiles(path, "*.xml"))
            {
                var tvi = new TreeViewItem();
                var fileName = f.Substring(f.LastIndexOf('\\') + 1);
                fileName = fileName.Remove(fileName.LastIndexOf('.'));

                this.AddObjects(f, tvi);

                uint size = 0;
                foreach (TreeViewItem t in tvi.Items)
                {
                    size += (uint)t.Tag;
                }

                tvi.Header = string.Format("{0} ({1})", fileName, size);
                tvi.Tag = size;
                items.Add(tvi);
            }
        }

        /// <summary>
        /// The dock panel_ loaded.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void DockPanel_Loaded(object sender, RoutedEventArgs e)
        {
            this.BuildTree(this.m_AssetPath, this.TreeView.Items);
        }

        #endregion
    }
}