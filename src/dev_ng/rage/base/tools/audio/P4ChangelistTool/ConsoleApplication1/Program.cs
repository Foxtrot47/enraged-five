﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Perforce.P4;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {

            try
            {
                string description = String.Join(" ", args);

                Server srv = new Server(new ServerAddress(null));
                Repository p4 = new Repository(srv);
                p4.Connection.Connect(new Options());

                Changelist myChange = new Changelist();
                myChange.Description = description;
                myChange.Files = new List<FileMetaData>();
                myChange = p4.CreateChangelist(myChange, new Options());
                Environment.ExitCode = myChange.Id;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Environment.ExitCode = -1;
            }

        }
    }
}
