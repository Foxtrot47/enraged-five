﻿using rage.Fields;
using rage.ToolLib.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using rage.Transform;
using rage.Types;

namespace rage.AutoTaggingTransformer
{
    public class AutoTaggingSoundRefTransformer : IGeneralTransformer
    {

        public TransformResult Transform(ILog log, XElement element, ITypeDefinition typeDefinition)
        {
            TransformResult result = new TransformResult { Result = element };

            ProcessFields(typeDefinition.AllFields, element, typeDefinition.Name);

            return result;
        }

        private void ProcessFields(IEnumerable<ICommonFieldDefinition> fieldDefinitions, XElement fieldInstancesParent, string namePath)
        {
            foreach (ICommonFieldDefinition field in fieldDefinitions)
            {
                CompositeFieldDefinition compositeField = field as CompositeFieldDefinition;
                if (compositeField!=null)
                {
                    foreach (XElement compositeFieldInstance in fieldInstancesParent.Elements())
                    {
                        ProcessFields(compositeField.AllFields, compositeFieldInstance, namePath+"."+compositeField.Name);
                    }
                }

                BasicFieldDefinition basicField = field as BasicFieldDefinition;
                if (basicField != null)
                {
                    if (basicField.Tagged && basicField.Unit == Units.ObjectRef &&
                        !string.IsNullOrEmpty(basicField.AllowedType) &&
                        basicField.AllowedType.Equals("Sound", StringComparison.OrdinalIgnoreCase))
                    {
                        //find field instance
                        XElement instanceField = fieldInstancesParent.Element(basicField.Name);
                        if (instanceField == null)
                        {
                            string defaultValue = basicField.Default;
                            if (defaultValue == null) defaultValue = "";
                            instanceField = new XElement(basicField.Name, defaultValue);
                            fieldInstancesParent.Add(instanceField);
                        }
                        string tag = namePath + "." + basicField.Name;
                        AddTag(instanceField, tag);
                    }
                }

            }
        }

        private void AddTag(XElement field, string tag)
        {
            if (field.Attribute(FieldInstanceTokens.tags.ToString()) == null)
            {
                field.Add(new XAttribute(FieldInstanceTokens.tags.ToString(), tag));
            }
            else
            {
                string tags = field.Attribute(FieldInstanceTokens.tags.ToString()).Value;
                foreach (string existingTag in tags.Split(new[] {','}))
                {
                    existingTag.Trim();
                    if (existingTag.Equals(tag)) return;
                }
                field.Attribute(FieldInstanceTokens.tags.ToString()).Value = tags + "," + tag;
            }
        }

        public bool Init(ILog log, params string[] args)
        {
            return true;
        }
    }
}
