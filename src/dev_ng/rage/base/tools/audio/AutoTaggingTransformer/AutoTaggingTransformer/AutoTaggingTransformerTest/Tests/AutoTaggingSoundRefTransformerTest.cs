﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using rage.Compiler;
using rage.Reflection;
using rage.ToolLib.Logging;
using rage.Types;

namespace rage.AutoTaggingTransformer.Tests
{
    public class AutoTaggingSoundRefTransformerTest
    {

        public void runTest()
        {
            ILog log = new TextLog(new ConsoleLogWriter(), new ContextStack());
            
            var typeDefinitionsLoader = new TypeDefinitionsLoader(log,
                                                                    new Reflector(log, new string[0]), 
                                                                    new CompiledObjectLookup(),
                                                                    new StringTable(), 
                                                                    new TagTable(), 
                                                                    "",
                                                                    "",
                                                                    (x) => { return null; },
                                                                    (x) => { return null; },
                                                                    0,
                                                                    false,
                                                                    false);
            typeDefinitionsLoader.LoadContent(@"
                <TypeDefinitions version=""192"" DefaultPacked=""no"" AlwaysGenerateFlags=""no"" mode=""2"">
                    <TypeDefinition name=""TestAudioSetting"" group=""Physics"">	
                        <Field name=""TestSound"" default=""NULL_SOUND"" type=""hash"" units=""ObjectRef"" allowedType=""Sound"" tagged=""yes""/>
                    </TypeDefinition>
                </TypeDefinitions>
            ", "dummyBasePath");

            ITypeDefinition testAudioSettingTypeDef = typeDefinitionsLoader.TypeDefinitions.First().Value;
            
            AutoTaggingSoundRefTransformer transformer = new AutoTaggingSoundRefTransformer();

            foreach (XElement elemnt in getHardCodedTestData())
            {
                transformer.Transform(log, elemnt, testAudioSettingTypeDef);
            }
        }

        private static IEnumerable<XElement> getHardCodedTestData()
        {
            return XDocument.Parse(@"
                <Objects>
                  <TestAudioSetting name=""OFF_MISSION_SOUNDS"">
                      <TestSound>SCRIPT_Taxi_Vomit_TAXI_SICK_01</TestSound>
                  </TestAudioSetting> 
                </Objects>
            ").Root.Elements();
        }
    }
}
