﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using rage.ToolLib.Logging;

namespace rage.AutoTaggingTransformer.Tests
{
    public class AutoTaggingSoundSetTransformerTest
    {
        public void runTest()
        {
            ILog log = new TextLog(new ConsoleLogWriter(), new ContextStack());
            AutoTaggingSoundSetTransformer transformer = new AutoTaggingSoundSetTransformer();



            transformer.Init(log, new string[0]);
            foreach (XElement elemnt in getHardCodedTestData())
            {
                transformer.Transform(log, elemnt);
            }
        }

        //e.g. X:\rdr3\audio\dev\ASSETS\OBJECTS\Core\Audio\SOUNDS\SCRIPT_SOUNDSETS.XML
        private static IEnumerable<XElement> getDataFromFile(string filePath)
        {
            return XDocument.Load(filePath, LoadOptions.None).Root.Elements();
        }

        private static IEnumerable<XElement> getHardCodedTestData()
        {
            return XDocument.Parse(@"
                <Objects>
                  <SoundSet name=""OFF_MISSION_SOUNDS"">
                    <Sounds>
                      <Sound>NULL_SOUND</Sound>
                      <Name>RAISE_LOWER_BET_MASTER</Name>
                    </Sounds>
                    <Sounds>
                      <Sound>NULL_SOUND</Sound>
                      <Name>HUD_MINIGAME_WIN_MASTER</Name>
                    </Sounds>
                    <Sounds>
                      <Sound>SCRIPT_Taxi_Vomit_TAXI_SICK_01</Sound>
                      <Name>TEST_SOUND</Name>
                    </Sounds>
                    <TagPrefix>OffMission</TagPrefix>
                  </SoundSet>  
                </Objects>
            ").Root.Elements();
        }
    }
}
