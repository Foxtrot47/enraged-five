﻿using rage.Fields;
using rage.ToolLib.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using rage.Transform;
using rage.Types;

namespace rage.AutoTaggingTransformer
{
    [CanTransform("SoundSet")]
    public class AutoTaggingSoundSetTransformer: ITransformer
    {

        public TransformResult Transform(ILog log, XElement element)
        {
            TransformResult result = new TransformResult { Result = element };

            XElement tagPrefixElement = element.Element("TagPrefix");
            if (tagPrefixElement == null || string.IsNullOrEmpty(tagPrefixElement.Value)) return result;
            string prefix = tagPrefixElement.Value;
            foreach (XElement soundsElement in element.Elements("Sounds"))
            {
                XElement nameElement = soundsElement.Element("Name");
                if (nameElement != null && !string.IsNullOrEmpty(nameElement.Value))
                {
                    XElement soundElement = soundsElement.Element("Sound");
                    if(soundElement != null) AddTag(soundElement, prefix+"."+nameElement.Value);
                }

            }

            return result;
        }

        private void AddTag(XElement field, string tag)
        {
            if (field.Attribute(FieldInstanceTokens.tags.ToString()) == null)
            {
                field.Add(new XAttribute(FieldInstanceTokens.tags.ToString(), tag));
            }
            else
            {
                string tags = field.Attribute(FieldInstanceTokens.tags.ToString()).Value;
                foreach (string existingTag in tags.Split(new[] {','}))
                {
                    existingTag.Trim();
                    if (existingTag.Equals(tag)) return;
                }
                field.Attribute(FieldInstanceTokens.tags.ToString()).Value = tags + "," + tag;
            }
        }

        public bool Init(ILog log, params string[] args)
        {
            return true;
        }

        public void Shutdown()
        {
            return;
        }
    }
}
