﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using rage.AutoTaggingTransformer.Tests;

namespace AutoTaggingTransformerTest
{
    public class Program
    {
        static void Main(string[] args)
        {
            AutoTaggingSoundRefTransformerTest soundRefTest = new AutoTaggingSoundRefTransformerTest();
            soundRefTest.runTest();

            AutoTaggingSoundSetTransformerTest soundSetTest = new AutoTaggingSoundSetTransformerTest();
            soundSetTest.runTest();
        }
    }
}
