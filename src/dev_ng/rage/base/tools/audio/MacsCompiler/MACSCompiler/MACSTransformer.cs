﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.IO;

using HashComputer;
using rage;
using rage.ToolLib.Logging;
using rage.Transform;

namespace rage.MACSCompiler
{

    [CanTransform("ModelAudioCollisionSettings")]
    public class MACSTransformer : ITransformer
    {

        public bool Init(ILog log, params string[] args)
        {
            log.Information("ModelAudioCollisionSettings init");
            return true;
        }

        public TransformResult Transform(ILog log, XElement element)
        {
            TransformResult result = new TransformResult { Result = null };

            string macs = element.Attribute("name").Value;
            if (macs == null )
            {
                return result;
            }
            else
            {
                try
                {
                    XElement nameHashElem = element.Element("NameHash");
                    if (nameHashElem == null)
                    {
                        nameHashElem = new XElement("NameHash", macs);
                        element.Add(nameHashElem);
                    }
                    result.Result = element;
                }
                catch (System.Exception ex)
                {
                    log.Exception(ex);
                }
            }
            return result;
        }

        public void Shutdown()
        {

        }

    }
}
