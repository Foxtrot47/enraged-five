// audmetadatabuilder.cpp : main project file.

using namespace rage;

#include "stdafx.h"
#include "audbuildmanager/builddefs.h"
#include "audmetadatabuilder.h"

#define NULL 0

namespace audMetadataBuild
{

	audMetadataBuilder::audMetadataBuilder()
	{
	}

	void audMetadataBuilder::Build(audAssetManager *assetManager, 
								   audProjectSettings *projectSettings, 
								   String *buildPlatform, 
								   audBuildClient *buildClient, 
								   XmlDocument * /* builtWavesXml */, 
								   XmlDocument * /* pendingWavesXml */, 
								   bool  /* shouldBuildLocally */, 
								   audBuildComponent * buildComponent,
								   bool)
	{
		m_AssetMgr = assetManager;
		m_BuildPlatform = buildPlatform;
		m_ProjectSettings = projectSettings;
		m_BuildClient = buildClient;
		m_BuildComponent = buildComponent;
		audMetadataType *metadataType = NULL;
		String *outputFile = NULL;

		try
		{
			m_BuildClient->ReportProgress(0, -1, S"Starting Metadata build", true);

			Init();

			audMetadataType *types[] = m_ProjectSettings->GetMetadataTypes();
			for (int i=0; i<types->Count; i++)
			{
				metadataType = types[i];

				String *workingPath = m_AssetMgr->GetWorkingPath("\\");
				// perforce returns forward slashes...replace them
				workingPath = workingPath->Replace(S"/",S"\\");
				CObjectCompiler *compiler = new CObjectCompiler(m_ProjectSettings->GetCurrentPlatform()->IsBigEndian,
															  types[i]->ClassFactoryFunctionName,
															  types[i]->ClassFactoryFunctionReturnType,
															  m_ProjectSettings->Use3ByteNameTableOffset(),
															  "");

				for (int j=0; j<types[i]->ObjectDefinitions->Count; ++j)
				{
					String *fileName = types[i]->ObjectDefinitions[j]->DefinitionsFile->Substring(types[i]->ObjectDefinitions[j]->DefinitionsFile->LastIndexOf("\\")+1);
										
						String *msg = String::Concat("Loading type definitions from ", fileName);
						m_BuildClient->ReportProgress((int)((j/(float)types[i]->ObjectDefinitions->Count)*100.f), -1,msg,true);
					

					String *typeDefs = String::Concat(workingPath,
													  m_ProjectSettings->GetSoundXmlPath(),
													  types[i]->ObjectDefinitions[j]->DefinitionsFile);

					if (!compiler->LoadTypeDefinitions(typeDefs,
													   types[i]->ObjectDefinitions[j]->ClassPrefix,
													  types[i]->ObjectDefinitions[j]->ClassIncludePath))
					{
						String *errString = String::Concat(S"Failed to load type definitions from ", typeDefs);
						m_BuildClient->ReportProgress(-1, -1, errString, true);

						audBuildException *e = new audBuildException(errString);
						throw(e);
					}
				}

				if(metadataType->TemplatePath != NULL)
				{
					// Load templates
					String *msg = String::Concat("Loading templates from ", metadataType->TemplatePath);
					m_BuildClient->ReportProgress(-1, -1,msg,true);

					ArrayList *templateFileList = CFileListBuilder::GetFileList(metadataType->TemplatePath, "xml");
					for(int i = 0; i < templateFileList->Count; i++)
					{
						String *templateFileName = dynamic_cast<String*>(templateFileList->get_Item(i));
						XmlDocument *doc = new XmlDocument();

						doc->Load(templateFileName);
						if (!compiler->LoadTemplates(doc))
						{
							String *errString = String::Concat(S"Failed to load templates from ", metadataType->TemplatePath);
							m_BuildClient->ReportProgress(-1, -1, errString, true);

							audBuildException *e = new audBuildException(errString);
							throw(e);
						}
					}
				}
				audMetadataFile *files[] = m_ProjectSettings->GetMetadataSettings();
				for(int k=0;k<files->Count;k++)
				{					
					if(files[k]->Type->ToUpper()->Equals(types[i]->Type->ToUpper()))
					{
						// clear out the object look/bank name table
						compiler->Init();

						outputFile = String::Concat(m_ProjectSettings->ResolvePath(files[k]->OutputFile));
						String *dir = System::IO::Path::GetDirectoryName(outputFile);
						if(!System::IO::Directory::Exists(dir))
						{
							System::IO::Directory::CreateDirectory(dir);
						}

						PrepareAssets(files[k], outputFile);						
						String* outputFileVersioned = NULL;
						if(types[i]->SideBySideVersioning && compiler->GetSchemaVersion()!=~0U)
						{
							outputFileVersioned = String::Concat(outputFile, __box(compiler->GetSchemaVersion())->ToString());
							PrepareAssets(files[k], outputFileVersioned);						
						}
						BuildMetadata(workingPath, compiler, files[k], outputFile, outputFileVersioned);
						CommitAssets(files[k], outputFile);
						if (outputFileVersioned)
							CommitAssets(files[k], outputFileVersioned);
					}
				}
			}

			Shutdown();

			m_BuildClient->ReportProgress(100, -1, S"Finished Metadata build", true);
		}
		catch (audBuildException *e)
		{
			m_BuildClient->ReportProgress(-1, -1, e->m_clientErrorMsg, true);
			ReleaseAssets(outputFile);
			Shutdown();

			throw(e);
		}
		catch (Exception* e)
		{
			ReleaseAssets(outputFile);
			Shutdown();

			throw(new audBuildException(e->ToString()));
						
		}
	}

	void audMetadataBuilder::HandleCompilerMessage(String *msg, String *)
	{
		m_BuildClient->ReportProgress(-1,-1,msg,true);
	}

	void audMetadataBuilder::HandleCompilerException(String *msg, String *context)
	{
		throw new audBuildException(String::Concat(context,": ", msg));
	}

	void audMetadataBuilder::Init()
	{
		// get latest xml files prior to metadata build
		m_AssetMgr->GetLatest( m_ProjectSettings->GetSoundXmlPath() );

		// setup the error manager
		CErrorManager::InitClass(false);
		if(m_BuildComponent->m_EnabledStrictMode)
		{
			CErrorManager::GetErrorManager()->add_OnErrorMessage(new MessageHandler(this,&audMetadataBuilder::HandleCompilerException));
			CErrorManager::GetErrorManager()->add_OnWarningMessage(new MessageHandler(this,&audMetadataBuilder::HandleCompilerException));
		}
		else
		{
			CErrorManager::GetErrorManager()->add_OnErrorMessage(new MessageHandler(this,&audMetadataBuilder::HandleCompilerMessage));
			CErrorManager::GetErrorManager()->add_OnWarningMessage(new MessageHandler(this,&audMetadataBuilder::HandleCompilerMessage));
		}
		
		CErrorManager::GetErrorManager()->add_OnInformationMessage(new MessageHandler(this,&audMetadataBuilder::HandleCompilerMessage));
		CErrorManager::GetErrorManager()->add_OnExceptionMessage(new MessageHandler(this,&audMetadataBuilder::HandleCompilerException));

		System::IO::Directory::SetCurrentDirectory(m_AssetMgr->GetWorkingPath(m_ProjectSettings->GetSoundXmlPath()));
	}

	void audMetadataBuilder::PrepareAssets(audMetadataFile *, String *outputFile)
	{
		// get latest xml files prior to metadata build
		m_AssetMgr->GetLatest( m_ProjectSettings->GetSoundXmlPath() );

		// check out the current dat files for each of the metadata sections
		// only if it exists - if this is the first build with a new schema version then the file won't exist
		// in asset management yet
		if(m_AssetMgr->ExistsAsAsset(outputFile))
		{
			AssetManagementOperation(AM_CHECKOUT, outputFile);
		}
	}

	void audMetadataBuilder::BuildMetadata(String *workingPath, CObjectCompiler *compiler, audMetadataFile *file, String *outFile, String* outputFileVersioned)
	{
		String *inFile = String::Concat(workingPath,
										m_ProjectSettings->GetSoundXmlPath(),
										file->DataPath);

	
		if (outFile == NULL || inFile == NULL)
		{
			audBuildException *e = new audBuildException(S"Input or output file is null!");
			throw(e);
		}

		Int32 size = 0;

		String *metadataFile = String::Concat(workingPath, outFile);
		System::IO::BinaryWriter *writer = new System::IO::BinaryWriter(new System::IO::FileStream(metadataFile, System::IO::FileMode::Create, System::IO::FileAccess::Write));
		if (m_ProjectSettings->GetCurrentPlatform()->IsBigEndian)
		{
			writer->Write(CUtility::SwapInt32(compiler->GetSchemaVersion()));
		}
		else
		{
			writer->Write(compiler->GetSchemaVersion());
		}

		writer->Write(size);
		ArrayList *soundList = CFileListBuilder::GetFileList(inFile, "xml", m_AssetMgr);
		for (int cnt=0; cnt<soundList->Count; ++cnt)
		{
			String *obj = dynamic_cast<String*>(soundList->get_Item(cnt));

			m_BuildClient->ReportProgress((int)((cnt/(float)soundList->Count)*100.f), -1, obj, false);

			if (!compiler->CompileObjects(obj, writer))
			{
				//writer->Close();
				//String *errStr = String::Concat(S"Failed to compile ", obj);
				//audBuildException *e = new audBuildException(errStr);
				//throw(e);
				System::Threading::Thread::Sleep(1000);
			}
		}

		Int64 pos = writer->BaseStream->Position - 8;
		writer->Seek(4, System::IO::SeekOrigin::Begin);
		if (m_ProjectSettings->GetCurrentPlatform()->IsBigEndian)
		{
			writer->Write(CUtility::SwapInt32((Int32)pos));
		}
		else
		{
			writer->Write((int)pos);
		}

		writer->Seek(0, System::IO::SeekOrigin::End);
		size = 0;
		writer->Write(size);
		pos = writer->BaseStream->Position;

		compiler->SerialiseStringTable(writer);
		size = (int)(writer->BaseStream->Position - pos);
		writer->Seek((int)pos-4, System::IO::SeekOrigin::Begin);

		if (m_ProjectSettings->GetCurrentPlatform()->IsBigEndian)
		{
			writer->Write(CUtility::SwapInt32(size));
		}
		else
		{
			writer->Write(size);
		}
		writer->Seek(0, System::IO::SeekOrigin::End);

		compiler->SerialiseObjectLookup(writer);
		writer->Close();
	
		if(outputFileVersioned)
		{
			//	Now let's make a copy of our compiled metadata file and append the version number on it
			String *metadataFileVersioned = String::Concat(workingPath, outputFileVersioned);
			CErrorManager::GetErrorManager()->HandleInfo(String::Concat(S"Copying ", metadataFile, " to ", metadataFileVersioned));
			System::IO::File::Copy(metadataFile, metadataFileVersioned, true);
		}
	}

	void audMetadataBuilder::CommitAssets(audMetadataFile *, String *outputFile)
	{
		// commit!
		AssetManagementOperation(AM_CHECKIN, outputFile);
	}

	void audMetadataBuilder::Shutdown()
	{
	}

	void audMetadataBuilder::ReleaseAssets(String *outputFile)
	{
		// revert!
		// TODO: this is a hack and i'm in a hurry - wont build if i use the enum
		if(m_AssetMgr->GetAssetManagementType()!=2/*audAssetManagement::ASSET_MANAGER_PERFORCE*/)
		{
			AssetManagementOperation(AM_REVERT, outputFile);
		}
	}

	void audMetadataBuilder::AssetManagementOperation(const eAssetMgmtOp op, String *datFile)
	{
		switch (op)
		{
		case AM_REVERT:
			if (!m_AssetMgr->SimpleUndoCheckOut(datFile))
			{
				String *errMsg = String::Concat(S"Unable to undo checkout on ", datFile);
				audBuildException *ex = new audBuildException(errMsg);
				throw(ex);
			}
			break;
		case AM_CHECKIN:
			if (!m_AssetMgr->CheckInOrImport(0, datFile, S"Metadata builder"))
			{
				String *errMsg = String::Concat(S"Unable to checkin/import ", datFile);
				audBuildException *ex = new audBuildException(errMsg);
				throw(ex);
			}
			break;
		case AM_CHECKOUT:
			if (m_AssetMgr->IsCheckedOut(datFile) || !m_AssetMgr->SimpleCheckOut(datFile, S"Metadata builder"))
			{
				String *errMsg = String::Concat(S"Unable to checkout ", datFile);
				audBuildException *ex = new audBuildException(errMsg);
				throw(ex);
			}
			break;	
		}
	}
};

