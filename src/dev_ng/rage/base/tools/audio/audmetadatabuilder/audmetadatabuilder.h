#pragma once

using namespace audAssetManagement;
using namespace audBuildCommon;
using namespace rage;
using namespace System;
using namespace System::Xml;
using namespace System::Collections;

namespace audMetadataBuild
{
	public __gc class audMetadataBuilder : public audPostBuilderBase
	{
	public:
		audMetadataBuilder();

		void Build(audAssetManager *assetManager, audProjectSettings *projectSettings, String *buildPlatform,
				   audBuildClient *buildClient, XmlDocument *builtWavesXml, XmlDocument *pendingWavesXml, bool shouldBuildLocally,
				   audBuildComponent *buildComponent, bool isDeferredBuild);

		void HandleCompilerMessage(String *msg, String *ctxt);
		void HandleCompilerException(String *msg, String *ctxt);

	private:
		__value enum eAssetMgmtOp
		{
			AM_CHECKOUT,
			AM_CHECKIN,
			AM_REVERT
		};

		void Init();
		void PrepareAssets(audMetadataFile *file, String *outputFile);
		void BuildMetadata(String *workingPath, CObjectCompiler *compiler, audMetadataFile *file, String *outputFile, String* outputFileVersioned);
		void CommitAssets(audMetadataFile *file, String *outputFile);
		void ReleaseAssets(String *outputFile);
		void Shutdown();

		

		void AssetManagementOperation(const eAssetMgmtOp op, String *file);

		audAssetManager *m_AssetMgr;
		audProjectSettings *m_ProjectSettings;
		audBuildClient *m_BuildClient;
		audBuildComponent * m_BuildComponent;
		String *m_BuildPlatform;
	};
};

