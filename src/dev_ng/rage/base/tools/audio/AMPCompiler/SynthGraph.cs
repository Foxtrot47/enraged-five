﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace rage.AMPCompiler
{
    class SynthGraph
    {
        public readonly List<ModuleInstance> ModuleInstances;
        public readonly List<ModuleInstance> AudioOutputs;
        public readonly List<ModuleInstance> AudioInputs;

        public readonly ModuleInstanceFactory InstanceFactory;
        public readonly string Name;
        public readonly RuntimeContext RuntimeContext;

        int m_NumProcessingStages;

        public SynthGraph(XElement element, ModuleInstanceFactory typeLoader)
        {
            InstanceFactory = typeLoader;
            AudioInputs = new List<ModuleInstance>();
            AudioOutputs = new List<ModuleInstance>();
            RuntimeContext = new RuntimeContext();

            Name = element.Attribute("name").Value;

            IEnumerable<XElement> moduleElements = element.Element("Module").Elements("Instance");
            if (moduleElements.Count() == 0)
            {
                // old school format
                moduleElements = element.Elements("Module");
            }
            ModuleInstances = CreateModuleInstances(moduleElements);
            m_NumProcessingStages = 0;

            if (element.Element("ExposedVariable") != null)
            {
                var exposedVars = element.Element("ExposedVariable").Elements("Instance");
                foreach (var e in exposedVars)
                {
                    string variableName = e.Element("Key").Value;
                    int moduleId = int.Parse(e.Element("ModuleId").Value);

                    VariableInput variableInput = ModuleInstances[moduleId] as VariableInput;
                    if (variableInput != null)
                    {
                        int variableId = RuntimeContext.InputVariables.Add(variableName, variableInput.Inputs[0].StaticValue);
                        variableInput.VariableId = variableId;
                    }
                    else
                    {
                        throw new Exception(string.Format("ExposedVariable element with name {0} attached to module id {1} which is not of type VariableInput", variableName, moduleId));
                    }
                }
            }

            // Sort into processing order (processing stage ascending)
            // NOTE: this will be recalculated after graph processing, but for debugging
            // purposes it's handy to sort the graph before we start.
            ModuleInstances.Sort((x, y) => x.ProcessingStage - y.ProcessingStage);

        }

        List<ModuleInstance> CreateModuleInstances(IEnumerable<XElement> moduleElements)
        {
            List<ModuleInstance> modules = new List<ModuleInstance>();

            modules.AddRange(from me in moduleElements select InstanceFactory.Create(me));

            // Set up connections - this relies on modules being left in loaded order
            foreach (ModuleInstance module in modules)
            {
                if(module.IsAudioInput)
                {
                    AudioInputs.Add(module);
                }
                else if (module.IsAudioOutput)
                {
                    AudioOutputs.Add(module);
                }
            }
            foreach (ModuleInstance module in modules)
            {
                module.SetupConnections(modules);
            }

            return modules;
        }

        // PURPOSE
        //  Remove any module instances who's type is marked DebugOnly, fixing up any
        //  connections as necessary.
        public void RemoveDebugOnlyModules()
        {
            // Remove any modules marked as debug only, fixing up the graph as necessary
            // Examples - junction pin, constant need rewiring, fft and chart recorder don't
            List<ModuleInstance> modulesToRemove = new List<ModuleInstance>();
            foreach (ModuleInstance module in ModuleInstances)
            {
                if (module.DebugOnly)
                {
                    modulesToRemove.Add(module);

                    if(module.Inputs.Length >= 1)
                    {
                        foreach (OutputPinInstance pin in module.Outputs)
                        {
                            // Reconnect to this modules input source
                            // Note: we assume that the first input pin is the one we need to
                            // pass through.
                            List<InputPinInstance> outputDestinations = pin.OutputDestinations.ToList();
                            foreach (InputPinInstance destinationPin in outputDestinations)
                            {
                                destinationPin.Disconnect();
                                if (module.Inputs[0].IsConnected)
                                {   
                                    destinationPin.Connect(module.Inputs[0].OtherPin);
                                }
                                else
                                {
                                    // Pass our static input value through
                                    destinationPin.StaticValue = module.Inputs[0].StaticValue;
                                    //destinationPin.SpecifiedFormat = module.Outputs[0].InstanceFormat;
                                }
                            }
                        }                        
                    }
                    foreach (InputPinInstance pin in module.Inputs)
                    {
                        // Ensure this module is fully disconnected from the graph.
                        pin.Disconnect();
                    }
                }
            }

            foreach (ModuleInstance module in modulesToRemove)
            {
                RemoveModule(module);
            }
        }

        // PURPOSE
        //  Remove any modules not ultimately connected to an output
        public void RemoveDisconnectedModules()
        {
            List<ModuleInstance> connectedModules = new List<ModuleInstance>();

            foreach(ModuleInstance m in AudioOutputs)
            {
                CreateRecursiveModuleList(m, connectedModules);
            }

            var disconnectedModules = ModuleInstances.Except(connectedModules).ToList();
            foreach(var m in disconnectedModules)
            {
                if (m.ShouldRemoveWhenDisconnected())
                {
                    RemoveModule(m);
                }
            }
        }

        void CreateRecursiveModuleList(ModuleInstance currentNode, List<ModuleInstance> moduleList)
        {
            moduleList.Add(currentNode);
            foreach(var input in currentNode.Inputs)
            {
                if (input.IsConnected)
                {
                    if (!moduleList.Contains(input.OtherPin.ParentModule))
                    {
                        CreateRecursiveModuleList(input.OtherPin.ParentModule, moduleList);
                    }
                }
            }
        }

        // PURPOSE
        //  Insert built-in modules in the graph where required
        public void ExpandBuiltInImplementations()
        {
            ModuleInstance[] modules = ModuleInstances.ToArray();

            foreach (ModuleInstance m in modules)
            {
                if (!m.HasExpanded)
                {
                    m.ExpandBuiltInImplementations(this);
                    m.HasExpanded = true;
                }
            }
        }

        // PURPOSE
        //  Compute the processing stages so the graph can be processed iteratively
        public void ComputeProcessingStages()
        {
            // Reset all values
            ModuleInstances.ForEach(m => m.ProcessingStage = -1);

            // Check that graph doesn't contain any infinite recursion
            ModuleInstances.ForEach(i => i.CheckForInfiniteRecursion());

            // Traverse the graph recursively, from back to front
            AudioOutputs.ForEach(m => m.ComputeProcessingStage(0));

            // At this point ProcessingStage is the number of nodes away from the output
            // We want to invert this to give us the order to process each node
            m_NumProcessingStages = ModuleInstances.Max(m => m.ProcessingStage);
            ModuleInstances.Where(m => m.ProcessingStage != -1).ToList().ForEach(m => m.ProcessingStage = m_NumProcessingStages - m.ProcessingStage);

            // Now we can sort into processing order (processing stage ascending)
            ModuleInstances.Sort((x, y) => x.ProcessingStage - y.ProcessingStage);
        }

        public T CreateModuleInstance<T>() where T : ModuleInstance
        {
            T newModule = Activator.CreateInstance(typeof(T)) as T;
            AddModule(newModule);
            return newModule;   
        }

        public void AllocateBuffersAndRegisters()
        {
            ModuleInstances.ForEach(m => m.AllocateBuffers(RuntimeContext));
        }

        public void AllocateConstants()
        {
            ModuleInstances.ForEach(m => m.AllocateConstants(RuntimeContext));
        }

        // PURPOSE
        //  Evaluate expressions offline where possible
        public void EvaluateOffline()
        {
            ModuleInstances.ForEach(m => m.EvaluateOffline());
        }

        // PURPOSE
        //  Apply final stage optimisation after all buffers/registers/constants have been allocated
        public void FinalOptimisation()
        {
            ModuleInstances.ForEach(m => m.FinalOptimisation());
        }

        // PURPOSE
        //  Insert built in conversion modules where necessary
        public void InsertConversionModules()
        {
            List<ModuleInstance> modules = ModuleInstances.ToList();
            foreach(var m in modules)
            {
                foreach(var pin in m.Inputs)
                {
                    if(pin.IsConnected)
                    {
                        if (pin.InstanceFormat != PinFormat.Either &&
                            pin.InstanceFormat != pin.OtherPin.InstanceFormat)
                        {
                            // need to insert a conversion module
                            ModuleInstance conversionModule;
                            if (pin.InstanceFormat == PinFormat.Normalized)
                            {
                                conversionModule = CreateModuleInstance<ConvertToNormalized>();
                            }
                            else
                            {
                                conversionModule = CreateModuleInstance<ConvertToSignal>();
                            }

                            // search to see if we have already created one we can use instead
                            bool needToConnect = true;
                            foreach (var existingDestination in pin.OtherPin.OutputDestinations)
                            {
                                if (existingDestination.ParentModule.GetType() == conversionModule.GetType())
                                {
                                    pin.Connect(existingDestination.ParentModule.Outputs[0]);
                                    needToConnect = false;
                                    break;
                                }
                            }

                            if (needToConnect)
                            {
                                conversionModule.Inputs[0].Connect(pin.OtherPin);
                                pin.Connect(conversionModule.Outputs[0]);
                            }
                        }
                    }
                }
            }
        }

        public void InsertCopyModules()
        {
            List<ModuleInstance> modules = ModuleInstances.ToList();
            foreach(var m in modules)
            {
                foreach(OutputPinInstance outputPin in m.Outputs)
                {   
                    int numOutputDestinations = outputPin.OutputDestinations.Count;
                    if (numOutputDestinations > 1 && outputPin.State == PinState.Dynamic)
                    {
                        var copyInstance = CreateModuleInstance<Copy>();
                        var destinations = outputPin.OutputDestinations.ToList();
                        for (int i = 0; i < numOutputDestinations; i++)
                        {
                            var d = destinations[i];
                            d.Connect(copyInstance.Outputs[i]);
                        }
                        copyInstance.Inputs[0].Connect(outputPin);
                    }
                }
                
            }
        }

        public void InsertSampleModules()
        {
            List<ModuleInstance> modules = ModuleInstances.ToList();
            foreach (var m in modules)
            {
                foreach (InputPinInstance input in m.Inputs)
                {
                    if (input.IsConnected && m.GetInputState(input.Index) == PinState.Static && input.OtherPin.State == PinState.Dynamic)
                    {
                        SampleBuffer sampleModule = null;
                        // See if we have already created a sample module we can use
                        foreach(var existingDest in input.OtherPin.OutputDestinations)
                        {
                            if (existingDest.ParentModule.GetType() == typeof(SampleBuffer))
                            {
                                sampleModule = existingDest.ParentModule as SampleBuffer;
                                break;
                            }
                        }
                        if (sampleModule == null)
                        {
                            sampleModule = CreateModuleInstance<SampleBuffer>();
                            sampleModule.Inputs[0].Connect(input.OtherPin);
                        }
                        input.Disconnect();
                        input.Connect(sampleModule.Outputs[0]);
                    }
                    else if (m.GetInputState(input.Index) == PinState.Dynamic && (!input.IsConnected || input.OtherPin.State == PinState.Static))
                    {
                        // Upsample static -> dynamic
                        HoldSample sampleModule = null;

                        // may not be connected
                        if (input.OtherPin != null)
                        {
                            foreach (var existingDest in input.OtherPin.OutputDestinations)
                            {
                                if (existingDest.ParentModule.GetType() == typeof(HoldSample))
                                {
                                    sampleModule = existingDest.ParentModule as HoldSample;
                                    break;
                                }
                            }
                        }

                        if (sampleModule == null)
                        {
                            sampleModule = CreateModuleInstance<HoldSample>();
                            if (input.IsConnected)
                            {
                                sampleModule.Inputs[0].Connect(input.OtherPin);
                                input.Disconnect();
                            }
                            else
                            {
                                sampleModule.Inputs[0].StaticValue = input.StaticValue;
                                sampleModule.Inputs[0].InstanceFormat = input.Format;
                            }
                        }
                        
                        input.Connect(sampleModule.Outputs[0]);                        
                    }
                }
            }
        }

        public void DebugPrint()
        {
            Console.WriteLine("Synth: {0}", Name);
            Console.WriteLine("Buffers: {0}", RuntimeContext.BufferPool.PeakUsage);
            Console.WriteLine("Registers: {0}", RuntimeContext.RegisterPool.PeakUsage);
            Console.Write("Audio Inputs: ");
            AudioInputs.ForEach(o => Console.Write("{0} ", o.Inputs[0].Buffer));
            Console.WriteLine();
            Console.Write("Audio Outputs: ");
            AudioOutputs.ForEach(o => Console.Write("{0} ", o.Inputs[0].Buffer));
            Console.WriteLine();
            Console.Write("Variables: ");
            RuntimeContext.InputVariables.Variables.ForEach(o => Console.Write("{0} ", o));
            Console.WriteLine();
            Console.WriteLine("Constants: {0}", RuntimeContext.ConstantAllocator.Constants.Count());
            RuntimeContext.ConstantAllocator.Constants.ForEach(c => Console.WriteLine("\t{0}: {1}", c, c.Value));

            
            Console.WriteLine();

            ModuleInstances.ForEach(m => m.DebugPrint());
        }

        void AddModule(ModuleInstance module)
        {
            ModuleInstances.Add(module);
            if(module.IsAudioOutput)
            {
                AudioOutputs.Add(module);
            }
            else if(module.IsAudioInput)
            {
                AudioInputs.Add(module);
            }
        }

        void RemoveModule(ModuleInstance module)
        {
            foreach(InputPinInstance p in module.Inputs)
            {
                p.Disconnect();
            }
            ModuleInstances.Remove(module);
            AudioInputs.Remove(module);
            AudioOutputs.Remove(module);
        }

        public XElement Serialize()
        {
            XElement root = new XElement("CompiledSynth", new XAttribute("name", Name));

            root.Add(new XElement("NumBuffers",RuntimeContext.BufferPool.PeakUsage));
            root.Add(new XElement("NumRegisters", RuntimeContext.RegisterPool.PeakUsage));

            root.Add(SerializeOutputs());
            root.Add(SerializeConstants());
            root.Add(SerializeVariables());
            root.Add(SerializeProcess());

            return root;
        }

        XElement SerializeOutputs()
        {
            return new XElement("Outputs",
                                    (from o in AudioOutputs
                                     where o.Inputs[0].Buffer != null
                                        select new XElement("Instance", 
                                            new XElement("BufferId", o.Inputs[0].Buffer.BufferId))));
        }

        XElement SerializeConstants()
        {
            return new XElement("Constants",
                (from c in RuntimeContext.ConstantAllocator.Constants
                 where c.Index > 1 // ignore 0 and 1
                 select new XElement("Instance",
                     new XElement("Value", c.Value))));
        }

        XElement SerializeVariables()
        {
            return new XElement("InputVariables",
                                    (from o in RuntimeContext.InputVariables.Variables
                                        select new XElement("Instance", 
                                            new XElement("Name", o.Name),
                                            new XElement("InitialValue", o.InitialValue))));
        }

        XElement SerializeProcess()
        {
            return new XElement("Operations",
                from m in ModuleInstances
                where m.ShouldSerialize
                 select new XElement("Instance",
                     m.Serialize()));
        }
    }
}
