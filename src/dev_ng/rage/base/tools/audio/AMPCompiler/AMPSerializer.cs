﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;
using System.IO;
using System.CodeDom.Compiler;
using System.Diagnostics;

using rage.Serialization;
using rage.ToolLib;
using rage.ToolLib.Writer;
using rage.Fields;


namespace rage.AMPCompiler
{
    using rage.ToolLib.Logging;

    [CanSerialize(typeof(CompiledOperationList))]
    public class AMPSerializer : ISerializer
    {
        #region ISerializer Members

        Dictionary<IntermediateOpcodes, SynthOpcodeIds> scalarInstructionMap;
        Dictionary<IntermediateOpcodes, SynthOpcodeIds> bufferInstructionMap;
        Dictionary<IntermediateOpcodes, SynthOpcodeIds> bufferScalarInstructionMap;
        List<SynthOpcodeIds> replacingOpcodes;
        Dictionary<SynthOpcodeIds, int> opcodeStateRequirements;

        enum ResourceLimits
        {
            kMaxRegisters = 48,
            kMaxBuffers = 16,
	        kMaxStateBlocks = 32,
	        kMaxVariables = 16,
        };

        OpcodeTimings m_OpcodeTimings;
        public AMPSerializer()
        {
            scalarInstructionMap = CreateScalarInstructionMap();
            bufferInstructionMap = CreateBufferInstructionMap();
            bufferScalarInstructionMap = CreateBufferScalarInstructionMap();
            replacingOpcodes = CreateReplacingOpcodeList();
            opcodeStateRequirements = CreateOpcodeStateRequirements();

            string assemblyPath = new Uri(Assembly.GetExecutingAssembly().CodeBase).AbsolutePath;
            Configuration cfg = ConfigurationManager.OpenExeConfiguration(assemblyPath);
            string optTimingsCsv = cfg.AppSettings.Settings["OptTimingsCsv"].Value;
            m_OpcodeTimings = new OpcodeTimings(optTimingsCsv);
        }
        public uint Alignment
        {
            get { return 4; }
        }

        public int Compare(XElement x, XElement y)
        {
            return 0;
        }

        Dictionary<IntermediateOpcodes, SynthOpcodeIds> CreateScalarInstructionMap()
        {
            Dictionary<IntermediateOpcodes, SynthOpcodeIds> instructionMap = new Dictionary<IntermediateOpcodes, SynthOpcodeIds>();

            instructionMap.Add(IntermediateOpcodes.COPY, SynthOpcodeIds.COPY_SCALAR);
            instructionMap.Add(IntermediateOpcodes.CONVERT_TO_SIGNAL, SynthOpcodeIds.CONVERT_SCALAR_TO_SIGNAL);
            instructionMap.Add(IntermediateOpcodes.CONVERT_TO_NORMALIZED, SynthOpcodeIds.CONVERT_SCALAR_TO_NORMALIZED);
            instructionMap.Add(IntermediateOpcodes.MULTIPLY, SynthOpcodeIds.MULTIPLY_SCALAR_SCALAR);
            instructionMap.Add(IntermediateOpcodes.SUM, SynthOpcodeIds.SUM_SCALAR_SCALAR);
            instructionMap.Add(IntermediateOpcodes.SUBTRACT, SynthOpcodeIds.SUBTRACT_SCALAR_SCALAR);
            instructionMap.Add(IntermediateOpcodes.DIVIDE, SynthOpcodeIds.DIVIDE_SCALAR_SCALAR);
            instructionMap.Add(IntermediateOpcodes.HARDKNEE, SynthOpcodeIds.HARDKNEE_SCALAR);
            instructionMap.Add(IntermediateOpcodes.ABS, SynthOpcodeIds.ABS_SCALAR);
            instructionMap.Add(IntermediateOpcodes.CEIL, SynthOpcodeIds.CEIL_SCALAR);
            instructionMap.Add(IntermediateOpcodes.FLOOR, SynthOpcodeIds.FLOOR_SCALAR);
            instructionMap.Add(IntermediateOpcodes.ROUND, SynthOpcodeIds.ROUND_SCALAR);
            instructionMap.Add(IntermediateOpcodes.SIGN, SynthOpcodeIds.SIGN_SCALAR);
            instructionMap.Add(IntermediateOpcodes.MODULUS, SynthOpcodeIds.MODULUS_SCALAR);
            instructionMap.Add(IntermediateOpcodes.MAX, SynthOpcodeIds.MAX_SCALAR);
            instructionMap.Add(IntermediateOpcodes.POWER, SynthOpcodeIds.POWER_SCALAR);
            instructionMap.Add(IntermediateOpcodes.LERP, SynthOpcodeIds.LERP_SCALAR);
            instructionMap.Add(IntermediateOpcodes.HARD_CLIP, SynthOpcodeIds.HARD_CLIP_SCALAR_SCALAR);
            instructionMap.Add(IntermediateOpcodes.SOFT_CLIP, SynthOpcodeIds.SOFT_CLIP_SCALAR_SCALAR);
            instructionMap.Add(IntermediateOpcodes.ENVELOPE_FOLLOWER, SynthOpcodeIds.ENVELOPE_FOLLOWER_SCALAR);

            instructionMap.Add(IntermediateOpcodes.BiquadCoefficients_LowPass2Pole, SynthOpcodeIds.BiquadCoefficients_LowPass2Pole);
            instructionMap.Add(IntermediateOpcodes.BiquadCoefficients_HighPass2Pole, SynthOpcodeIds.BiquadCoefficients_HighPass2Pole);
            instructionMap.Add(IntermediateOpcodes.BiquadCoefficients_LowPass4Pole, SynthOpcodeIds.BiquadCoefficients_LowPass4Pole);
            instructionMap.Add(IntermediateOpcodes.BiquadCoefficients_HighPass4Pole, SynthOpcodeIds.BiquadCoefficients_HighPass4Pole);
            instructionMap.Add(IntermediateOpcodes.BiquadCoefficients_BandPass, SynthOpcodeIds.BiquadCoefficients_BandPass);
            instructionMap.Add(IntermediateOpcodes.BiquadCoefficients_BandStop, SynthOpcodeIds.BiquadCoefficients_BandStop);
            instructionMap.Add(IntermediateOpcodes.BiquadCoefficients_PeakingEQ, SynthOpcodeIds.BiquadCoefficients_PeakingEQ);
            instructionMap.Add(IntermediateOpcodes.BiquadCoefficients_LowShelf2Pole, SynthOpcodeIds.BiquadCoefficients_LowShelf2Pole);
            instructionMap.Add(IntermediateOpcodes.BiquadCoefficients_LowShelf4Pole, SynthOpcodeIds.BiquadCoefficients_LowShelf4Pole);
            instructionMap.Add(IntermediateOpcodes.BiquadCoefficients_HighShelf2Pole, SynthOpcodeIds.BiquadCoefficients_HighShelf2Pole);
            instructionMap.Add(IntermediateOpcodes.BiquadCoefficients_HighShelf4Pole, SynthOpcodeIds.BiquadCoefficients_HighShelf4Pole);


            instructionMap.Add(IntermediateOpcodes.OSC_RAMP, SynthOpcodeIds.OSC_RAMP_SCALAR);
            instructionMap.Add(IntermediateOpcodes.SINE, SynthOpcodeIds.SINE_SCALAR);
            instructionMap.Add(IntermediateOpcodes.COS, SynthOpcodeIds.COS_SCALAR);
            instructionMap.Add(IntermediateOpcodes.TRI, SynthOpcodeIds.TRI_SCALAR);
            instructionMap.Add(IntermediateOpcodes.SQUARE, SynthOpcodeIds.SQUARE_SCALAR);
            instructionMap.Add(IntermediateOpcodes.SAW, SynthOpcodeIds.SAW_SCALAR);

            instructionMap.Add(IntermediateOpcodes.RANDOM, SynthOpcodeIds.RANDOM);
            instructionMap.Add(IntermediateOpcodes.RANDOMIZE_ONINIT, SynthOpcodeIds.RANDOMIZE_ONINIT);

            instructionMap.Add(IntermediateOpcodes.OnePole_LPF, SynthOpcodeIds.OnePole_LPF_SCALAR);
            instructionMap.Add(IntermediateOpcodes.OnePole_HPF, SynthOpcodeIds.OnePole_HPF_SCALAR);

            instructionMap.Add(IntermediateOpcodes.RESCALE, SynthOpcodeIds.RESCALE_SCALAR);

            instructionMap.Add(IntermediateOpcodes.TIMED_TRIGGER_INT, SynthOpcodeIds.TIMED_TRIGGER_INT);
            instructionMap.Add(IntermediateOpcodes.TIMED_TRIGGER_OS, SynthOpcodeIds.TIMED_TRIGGER_OS);
            instructionMap.Add(IntermediateOpcodes.TIMED_TRIGGER_RET, SynthOpcodeIds.TIMED_TRIGGER_RET);

            instructionMap.Add(IntermediateOpcodes.NOTE_TO_FREQ, SynthOpcodeIds.NOTE_TO_FREQ_SCALAR);
            instructionMap.Add(IntermediateOpcodes.SAH_STATIC_OUTPUT, SynthOpcodeIds.SAH_STATIC_SCALAR);

            instructionMap.Add(IntermediateOpcodes.GATE, SynthOpcodeIds.GATE_SCALAR_SCALAR);
            instructionMap.Add(IntermediateOpcodes.HOLD, SynthOpcodeIds.HOLD_SAMPLE);

            instructionMap.Add(IntermediateOpcodes.Switch_Norm, SynthOpcodeIds.Switch_Norm_Scalar);
            instructionMap.Add(IntermediateOpcodes.Switch_Index, SynthOpcodeIds.Switch_Index_Scalar);
            instructionMap.Add(IntermediateOpcodes.Switch_Lerp, SynthOpcodeIds.Switch_Lerp_Scalar);
            instructionMap.Add(IntermediateOpcodes.Switch_EqualPower, SynthOpcodeIds.Switch_EqualPower_Scalar);

            instructionMap.Add(IntermediateOpcodes.Allpass, SynthOpcodeIds.Allpass_Static);
            
            return instructionMap;
        }

        Dictionary<IntermediateOpcodes, SynthOpcodeIds> CreateBufferInstructionMap()
        {
            Dictionary<IntermediateOpcodes, SynthOpcodeIds> instructionMap = new Dictionary<IntermediateOpcodes, SynthOpcodeIds>();

            instructionMap.Add(IntermediateOpcodes.COPY, SynthOpcodeIds.COPY_BUFFER);
            instructionMap.Add(IntermediateOpcodes.CONVERT_TO_SIGNAL, SynthOpcodeIds.CONVERT_BUFFER_TO_SIGNAL);
            instructionMap.Add(IntermediateOpcodes.CONVERT_TO_NORMALIZED, SynthOpcodeIds.CONVERT_BUFFER_TO_NORMALIZED);
            instructionMap.Add(IntermediateOpcodes.MULTIPLY, SynthOpcodeIds.MULTIPLY_BUFFER_BUFFER);
            instructionMap.Add(IntermediateOpcodes.SUM, SynthOpcodeIds.SUM_BUFFER_BUFFER);
            instructionMap.Add(IntermediateOpcodes.SUBTRACT, SynthOpcodeIds.SUBTRACT_BUFFER_BUFFER);
            instructionMap.Add(IntermediateOpcodes.DIVIDE, SynthOpcodeIds.DIVIDE_BUFFER_BUFFER);
            instructionMap.Add(IntermediateOpcodes.HARDKNEE, SynthOpcodeIds.HARDKNEE_BUFFER);// runtime handles second operand type
            instructionMap.Add(IntermediateOpcodes.ABS, SynthOpcodeIds.ABS_BUFFER);
            instructionMap.Add(IntermediateOpcodes.CEIL, SynthOpcodeIds.CEIL_BUFFER);
            instructionMap.Add(IntermediateOpcodes.FLOOR, SynthOpcodeIds.FLOOR_BUFFER);
            instructionMap.Add(IntermediateOpcodes.ROUND, SynthOpcodeIds.ROUND_BUFFER);
            instructionMap.Add(IntermediateOpcodes.SIGN, SynthOpcodeIds.SIGN_BUFFER);
            instructionMap.Add(IntermediateOpcodes.MODULUS, SynthOpcodeIds.MODULUS_BUFFER);// runtime handles second operand type
            instructionMap.Add(IntermediateOpcodes.MAX, SynthOpcodeIds.MAX_BUFFER);
            instructionMap.Add(IntermediateOpcodes.POWER, SynthOpcodeIds.POWER_BUFFER);// runtime handles second operand type
            instructionMap.Add(IntermediateOpcodes.LERP, SynthOpcodeIds.LERP_BUFFER);
            instructionMap.Add(IntermediateOpcodes.HARD_CLIP, SynthOpcodeIds.HARD_CLIP_BUFFER_BUFFER);
            instructionMap.Add(IntermediateOpcodes.SOFT_CLIP, SynthOpcodeIds.SOFT_CLIP_BUFFER_BUFFER);
            instructionMap.Add(IntermediateOpcodes.SINE, SynthOpcodeIds.SINE_BUFFER);
            instructionMap.Add(IntermediateOpcodes.COS, SynthOpcodeIds.COS_BUFFER);
            instructionMap.Add(IntermediateOpcodes.TRI, SynthOpcodeIds.TRI_BUFFER);
            instructionMap.Add(IntermediateOpcodes.SQUARE, SynthOpcodeIds.SQUARE_BUFFER);
            instructionMap.Add(IntermediateOpcodes.SAW, SynthOpcodeIds.SAW_BUFFER);
                      
            instructionMap.Add(IntermediateOpcodes.OnePole_LPF, SynthOpcodeIds.OnePole_LPF_BUFFER_BUFFER);
            instructionMap.Add(IntermediateOpcodes.OnePole_HPF, SynthOpcodeIds.OnePole_HPF_BUFFER_BUFFER);

            instructionMap.Add(IntermediateOpcodes.RESCALE, SynthOpcodeIds.RESCALE_BUFFER_BUFFER);

            instructionMap.Add(IntermediateOpcodes.ENV_GEN_EXP_INT, SynthOpcodeIds.ENV_GEN_EXP_INT_BUFFER);
            instructionMap.Add(IntermediateOpcodes.ENV_GEN_EXP_OS, SynthOpcodeIds.ENV_GEN_EXP_OS_BUFFER);
            instructionMap.Add(IntermediateOpcodes.ENV_GEN_EXP_RET, SynthOpcodeIds.ENV_GEN_EXP_RET_BUFFER);
            instructionMap.Add(IntermediateOpcodes.ENV_GEN_LIN_INT, SynthOpcodeIds.ENV_GEN_LIN_INT_BUFFER);
            instructionMap.Add(IntermediateOpcodes.ENV_GEN_LIN_OS, SynthOpcodeIds.ENV_GEN_LIN_OS_BUFFER);
            instructionMap.Add(IntermediateOpcodes.ENV_GEN_LIN_RET, SynthOpcodeIds.ENV_GEN_LIN_RET_BUFFER);

            instructionMap.Add(IntermediateOpcodes.READ_INPUT_0, SynthOpcodeIds.READ_INPUT_0);
            instructionMap.Add(IntermediateOpcodes.READ_INPUT_1, SynthOpcodeIds.READ_INPUT_1);
            instructionMap.Add(IntermediateOpcodes.READ_INPUT_2, SynthOpcodeIds.READ_INPUT_2);
            instructionMap.Add(IntermediateOpcodes.READ_INPUT_3, SynthOpcodeIds.READ_INPUT_3);
            instructionMap.Add(IntermediateOpcodes.READ_INPUT_4, SynthOpcodeIds.READ_INPUT_4);
            instructionMap.Add(IntermediateOpcodes.READ_INPUT_5, SynthOpcodeIds.READ_INPUT_5);
            instructionMap.Add(IntermediateOpcodes.READ_INPUT_6, SynthOpcodeIds.READ_INPUT_6);
            instructionMap.Add(IntermediateOpcodes.READ_INPUT_7, SynthOpcodeIds.READ_INPUT_7);

            instructionMap.Add(IntermediateOpcodes.NOTE_TO_FREQ, SynthOpcodeIds.NOTE_TO_FREQ_BUFFER);

            instructionMap.Add(IntermediateOpcodes.GATE, SynthOpcodeIds.GATE_BUFFER_BUFFER);

            instructionMap.Add(IntermediateOpcodes.Switch_Norm, SynthOpcodeIds.Switch_Norm_Buffer);
            instructionMap.Add(IntermediateOpcodes.Switch_Index, SynthOpcodeIds.Switch_Index_Buffer);
            instructionMap.Add(IntermediateOpcodes.Switch_Lerp, SynthOpcodeIds.Switch_Lerp_Buffer);
            instructionMap.Add(IntermediateOpcodes.Switch_EqualPower, SynthOpcodeIds.Switch_EqualPower_Buffer);

            instructionMap.Add(IntermediateOpcodes.Allpass, SynthOpcodeIds.Allpass_Buffer);

            return instructionMap;
        }

        Dictionary<IntermediateOpcodes, SynthOpcodeIds> CreateBufferScalarInstructionMap()
        {
            Dictionary<IntermediateOpcodes, SynthOpcodeIds> instructionMap = new Dictionary<IntermediateOpcodes, SynthOpcodeIds>();

            instructionMap.Add(IntermediateOpcodes.MULTIPLY, SynthOpcodeIds.MULTIPLY_BUFFER_SCALAR);
            instructionMap.Add(IntermediateOpcodes.SUM, SynthOpcodeIds.SUM_BUFFER_SCALAR);
            instructionMap.Add(IntermediateOpcodes.SUBTRACT, SynthOpcodeIds.SUBTRACT_BUFFER_SCALAR);
            instructionMap.Add(IntermediateOpcodes.DIVIDE, SynthOpcodeIds.DIVIDE_BUFFER_SCALAR);
            instructionMap.Add(IntermediateOpcodes.HARDKNEE, SynthOpcodeIds.HARDKNEE_BUFFER);// runtime handles second operand type
            instructionMap.Add(IntermediateOpcodes.MODULUS, SynthOpcodeIds.MODULUS_BUFFER);// runtime handles second operand type
            instructionMap.Add(IntermediateOpcodes.LERP, SynthOpcodeIds.LERP_BUFFER);
            instructionMap.Add(IntermediateOpcodes.POWER, SynthOpcodeIds.POWER_BUFFER);// runtime handles second operand type
            instructionMap.Add(IntermediateOpcodes.HARD_CLIP, SynthOpcodeIds.HARD_CLIP_BUFFER_SCALAR);
            instructionMap.Add(IntermediateOpcodes.SOFT_CLIP, SynthOpcodeIds.SOFT_CLIP_BUFFER_SCALAR);
            instructionMap.Add(IntermediateOpcodes.ENVELOPE_FOLLOWER, SynthOpcodeIds.ENVELOPE_FOLLOWER_BUFFER);
            instructionMap.Add(IntermediateOpcodes.OnePole_LPF, SynthOpcodeIds.OnePole_LPF_BUFFER_SCALAR);
            instructionMap.Add(IntermediateOpcodes.OnePole_HPF, SynthOpcodeIds.OnePole_HPF_BUFFER_SCALAR);
            
            instructionMap.Add(IntermediateOpcodes.RESCALE, SynthOpcodeIds.RESCALE_BUFFER_SCALAR);

            instructionMap.Add(IntermediateOpcodes.GATE, SynthOpcodeIds.GATE_BUFFER_SCALAR);
            return instructionMap;
        }

        List<SynthOpcodeIds> CreateReplacingOpcodeList()
        {
            List<SynthOpcodeIds> replacingOpcodes = new List<SynthOpcodeIds>();

            replacingOpcodes.Add(SynthOpcodeIds.CONVERT_BUFFER_TO_SIGNAL);        
            replacingOpcodes.Add(SynthOpcodeIds.CONVERT_BUFFER_TO_NORMALIZED);
            replacingOpcodes.Add(SynthOpcodeIds.MULTIPLY_BUFFER_BUFFER);
            replacingOpcodes.Add(SynthOpcodeIds.MULTIPLY_BUFFER_SCALAR);
            replacingOpcodes.Add(SynthOpcodeIds.SUM_BUFFER_BUFFER);
            replacingOpcodes.Add(SynthOpcodeIds.SUM_BUFFER_SCALAR);
            replacingOpcodes.Add(SynthOpcodeIds.SUBTRACT_BUFFER_BUFFER);
            replacingOpcodes.Add(SynthOpcodeIds.SUBTRACT_BUFFER_SCALAR);
            replacingOpcodes.Add(SynthOpcodeIds.DIVIDE_BUFFER_BUFFER);
            replacingOpcodes.Add(SynthOpcodeIds.DIVIDE_BUFFER_SCALAR);
            replacingOpcodes.Add(SynthOpcodeIds.HARDKNEE_BUFFER);
            replacingOpcodes.Add(SynthOpcodeIds.ABS_BUFFER);
            replacingOpcodes.Add(SynthOpcodeIds.CEIL_BUFFER);
            replacingOpcodes.Add(SynthOpcodeIds.FLOOR_BUFFER);
            replacingOpcodes.Add(SynthOpcodeIds.ROUND_BUFFER);
            replacingOpcodes.Add(SynthOpcodeIds.SIGN_BUFFER);
            replacingOpcodes.Add(SynthOpcodeIds.MODULUS_BUFFER);
            replacingOpcodes.Add(SynthOpcodeIds.MAX_BUFFER);
            replacingOpcodes.Add(SynthOpcodeIds.MODULUS_BUFFER);           
            replacingOpcodes.Add(SynthOpcodeIds.HARD_CLIP_BUFFER_BUFFER);
            replacingOpcodes.Add(SynthOpcodeIds.HARD_CLIP_BUFFER_SCALAR);
            replacingOpcodes.Add(SynthOpcodeIds.SOFT_CLIP_BUFFER_BUFFER);
            replacingOpcodes.Add(SynthOpcodeIds.SOFT_CLIP_BUFFER_SCALAR);
            replacingOpcodes.Add(SynthOpcodeIds.BiquadProcess_2Pole);
            replacingOpcodes.Add(SynthOpcodeIds.BiquadProcess_4Pole);
            replacingOpcodes.Add(SynthOpcodeIds.OSC_RAMP_BUFFER_BUFFER);
            replacingOpcodes.Add(SynthOpcodeIds.SINE_BUFFER);
            replacingOpcodes.Add(SynthOpcodeIds.COS_BUFFER);
            replacingOpcodes.Add(SynthOpcodeIds.TRI_BUFFER);
            replacingOpcodes.Add(SynthOpcodeIds.SQUARE_BUFFER);
            replacingOpcodes.Add(SynthOpcodeIds.SAW_BUFFER);

            replacingOpcodes.Add(SynthOpcodeIds.OnePole_LPF_BUFFER_BUFFER);
            replacingOpcodes.Add(SynthOpcodeIds.OnePole_LPF_BUFFER_SCALAR);
            replacingOpcodes.Add(SynthOpcodeIds.OnePole_HPF_BUFFER_BUFFER);
            replacingOpcodes.Add(SynthOpcodeIds.OnePole_HPF_BUFFER_SCALAR);

            replacingOpcodes.Add(SynthOpcodeIds.RESCALE_BUFFER_BUFFER);
            replacingOpcodes.Add(SynthOpcodeIds.RESCALE_BUFFER_SCALAR);

            replacingOpcodes.Add(SynthOpcodeIds.LERP_BUFFER);
            replacingOpcodes.Add(SynthOpcodeIds.MAX_BUFFER);
            replacingOpcodes.Add(SynthOpcodeIds.POWER_BUFFER);

            replacingOpcodes.Add(SynthOpcodeIds.NOTE_TO_FREQ_BUFFER);

            replacingOpcodes.Add(SynthOpcodeIds.ENVELOPE_FOLLOWER_BUFFER);

            replacingOpcodes.Add(SynthOpcodeIds.DECIMATE);
            replacingOpcodes.Add(SynthOpcodeIds.GATE_BUFFER_BUFFER);
            replacingOpcodes.Add(SynthOpcodeIds.GATE_BUFFER_SCALAR);

            replacingOpcodes.Add(SynthOpcodeIds.SMALLDELAY_FRAC);
            replacingOpcodes.Add(SynthOpcodeIds.SMALLDELAY_STATIC);
            replacingOpcodes.Add(SynthOpcodeIds.SMALLDELAY_FRAC_FB);
            replacingOpcodes.Add(SynthOpcodeIds.SMALLDELAY_STATIC_FB);

            replacingOpcodes.Add(SynthOpcodeIds.COMPRESSOR_EG);
            replacingOpcodes.Add(SynthOpcodeIds.AW_FILTER);
            replacingOpcodes.Add(SynthOpcodeIds.LERP_THREE_BUFFERS);

            replacingOpcodes.Add(SynthOpcodeIds.Switch_Norm_Buffer);
            replacingOpcodes.Add(SynthOpcodeIds.Switch_Index_Buffer);
            replacingOpcodes.Add(SynthOpcodeIds.Switch_Lerp_Buffer);
            replacingOpcodes.Add(SynthOpcodeIds.Switch_EqualPower_Buffer);

            replacingOpcodes.Add(SynthOpcodeIds.Allpass_Buffer);
            replacingOpcodes.Add(SynthOpcodeIds.Allpass_Static);

            return replacingOpcodes;
        }

        Dictionary<SynthOpcodeIds, int> CreateOpcodeStateRequirements()
        {
            Dictionary<SynthOpcodeIds, int> opcodeStateRequirements = new Dictionary<SynthOpcodeIds, int>();

            opcodeStateRequirements.Add(SynthOpcodeIds.BiquadProcess_2Pole, 1);
            opcodeStateRequirements.Add(SynthOpcodeIds.BiquadProcess_4Pole, 1);

            opcodeStateRequirements.Add(SynthOpcodeIds.OSC_RAMP_BUFFER_BUFFER, 1);
            opcodeStateRequirements.Add(SynthOpcodeIds.OSC_RAMP_BUFFER_SCALAR, 1);
            opcodeStateRequirements.Add(SynthOpcodeIds.OSC_RAMP_SCALAR, 1);
            opcodeStateRequirements.Add(SynthOpcodeIds.RANDOM, 1);
            opcodeStateRequirements.Add(SynthOpcodeIds.RANDOMIZE_ONINIT, 1);
            opcodeStateRequirements.Add(SynthOpcodeIds.TRIGGER_LATCH, 1);
            opcodeStateRequirements.Add(SynthOpcodeIds.TRIGGER_DIFF, 1);
            
            opcodeStateRequirements.Add(SynthOpcodeIds.OnePole_LPF_BUFFER_BUFFER, 1);
            opcodeStateRequirements.Add(SynthOpcodeIds.OnePole_LPF_BUFFER_SCALAR, 1);
            opcodeStateRequirements.Add(SynthOpcodeIds.OnePole_LPF_SCALAR, 1);
            opcodeStateRequirements.Add(SynthOpcodeIds.OnePole_HPF_BUFFER_BUFFER, 1);
            opcodeStateRequirements.Add(SynthOpcodeIds.OnePole_HPF_BUFFER_SCALAR, 1);
            opcodeStateRequirements.Add(SynthOpcodeIds.OnePole_HPF_SCALAR, 1);

            opcodeStateRequirements.Add(SynthOpcodeIds.ENVELOPE_FOLLOWER_BUFFER, 1);
            opcodeStateRequirements.Add(SynthOpcodeIds.ENVELOPE_FOLLOWER_SCALAR, 1);

            opcodeStateRequirements.Add(SynthOpcodeIds.ENV_GEN_EXP_INT_BUFFER, 1);
            opcodeStateRequirements.Add(SynthOpcodeIds.ENV_GEN_EXP_OS_BUFFER, 1);
            opcodeStateRequirements.Add(SynthOpcodeIds.ENV_GEN_EXP_RET_BUFFER, 1);
            opcodeStateRequirements.Add(SynthOpcodeIds.ENV_GEN_LIN_INT_BUFFER, 1);
            opcodeStateRequirements.Add(SynthOpcodeIds.ENV_GEN_LIN_OS_BUFFER, 1);
            opcodeStateRequirements.Add(SynthOpcodeIds.ENV_GEN_LIN_RET_BUFFER, 1);

            opcodeStateRequirements.Add(SynthOpcodeIds.TIMED_TRIGGER_INT, 1);
            opcodeStateRequirements.Add(SynthOpcodeIds.TIMED_TRIGGER_OS, 1);
            opcodeStateRequirements.Add(SynthOpcodeIds.TIMED_TRIGGER_RET, 1);

            opcodeStateRequirements.Add(SynthOpcodeIds.SAH_STATIC_SCALAR, 1);

            opcodeStateRequirements.Add(SynthOpcodeIds.DECIMATE, 1);
            opcodeStateRequirements.Add(SynthOpcodeIds.COUNTER, 1);
            opcodeStateRequirements.Add(SynthOpcodeIds.COUNTER_TRIGGER, 1);

            opcodeStateRequirements.Add(SynthOpcodeIds.SMALLDELAY_FRAC, 1);
            opcodeStateRequirements.Add(SynthOpcodeIds.SMALLDELAY_STATIC, 1);
            opcodeStateRequirements.Add(SynthOpcodeIds.SMALLDELAY_FRAC_FB, 1);
            opcodeStateRequirements.Add(SynthOpcodeIds.SMALLDELAY_STATIC_FB, 1);

            opcodeStateRequirements.Add(SynthOpcodeIds.MAX_SCALAR, 1);
            opcodeStateRequirements.Add(SynthOpcodeIds.MAX_BUFFER, 1);

            opcodeStateRequirements.Add(SynthOpcodeIds.COMPRESSOR_EG, 1);
            opcodeStateRequirements.Add(SynthOpcodeIds.AW_FILTER, 1);

            opcodeStateRequirements.Add(SynthOpcodeIds.Allpass_Buffer, 1);
            opcodeStateRequirements.Add(SynthOpcodeIds.Allpass_Static, 1);

            return opcodeStateRequirements;
        }

        private void LogAssert(bool condition, string description, ILog logger)
        {
            if (!condition)
            {
                logger.Warning("Assertion failed: "+description);
            }
        }

        public bool Serialize(ILog log, IWriter output, XElement element, ICommonFieldDefinition fieldDefinition)
        {
            bool includeCostEstimate = true;// && fieldDefinition.TypeDefinition.Version > 8;

            uint startOffset = output.Tell();
            float costEstimate = 0;
            
            // Placeholder - will come back and overwrite these values
            output.Write(startOffset); // programSizeBytes
            output.Write(startOffset); // num state blocks
            if (includeCostEstimate)
            {
                output.Write(startOffset); // cost estimate
            }

            uint headerSize = output.Tell() - startOffset;
            
            int numStateBlocks = 0;

            var operations = element.Elements("Instance");
            foreach (var operationInstance in operations)
            {
                var operation = operationInstance.Elements().First();
                string opName = operation.Name.LocalName;
                IntermediateOpcodes typeId = Enum<IntermediateOpcodes>.Parse(opName);

                // Encode the number of inputs and outputs in the opcode, for operations like Copy where it is variable.
                // This also allows us to perform some sanity checking at runtime.
                uint numInputs = (uint)operation.Element("Inputs").Elements().Count();
                uint numOutputs = (uint)operation.Element("Outputs").Elements().Count();
                if(numInputs > 16 || numOutputs > 16)
                {
                    throw new InvalidDataException(string.Format("Too many inputs/outputs: {0}/{0}", numInputs, numOutputs));
                }

                SynthReference[] inputs = (from e in operation.Element("Inputs").Elements() 
                                                        select EncodeReference(e)).ToArray();
                SynthReference[] outputs = (from e in operation.Element("Outputs").Elements()
                                                    select EncodeReference(e)).ToArray();

                SynthOpcodeIds opcode = SynthOpcodeIds.END;
                switch (typeId)
                {
                    case IntermediateOpcodes.SAMPLE:
                        LogAssert(numInputs == 1, "number of "+typeId+" inputs != 1", log);
                        LogAssert(numOutputs == 1, "number of "+typeId+" outputs != 1", log);
                        LogAssert(inputs[0].Type == ReferenceType.Buffer, typeId+" input type  isn't a buffer", log);
                        LogAssert(outputs[0].Type == ReferenceType.Register, typeId+" output type  isn't a register", log);
                        opcode = SynthOpcodeIds.SAMPLE_BUFFER;
                        break;
                    
                    // Unary
                    case IntermediateOpcodes.COPY:
                        LogAssert(numInputs == 1, "number of "+typeId+" inputs != 1", log);
                        LogAssert(numOutputs >= 2, "number of "+typeId+" outputs < 2", log);
                                                
                        if (inputs[0].IsScalar())
                        {
                            opcode = scalarInstructionMap[typeId];
                        }
                        else
                        {
                            opcode = bufferInstructionMap[typeId];
                        }
                        LogAssert(inputs[0].Encode() == outputs[0].Encode(), typeId+" input and output encoding differ", log);
                        for (int i = 0; i < numOutputs; i++)
                        {
                            LogAssert(inputs[0].IsScalar() == outputs[i].IsScalar(), typeId+" input and output "+i+" have different values for IsScalar", log);
                        }
                                                
                        break;

                    case IntermediateOpcodes.READ_INPUT_0:
                    case IntermediateOpcodes.READ_INPUT_1:
                    case IntermediateOpcodes.READ_INPUT_2:
                    case IntermediateOpcodes.READ_INPUT_3:
                    case IntermediateOpcodes.READ_INPUT_4:
                    case IntermediateOpcodes.READ_INPUT_5:
                    case IntermediateOpcodes.READ_INPUT_6:
                    case IntermediateOpcodes.READ_INPUT_7:
                        LogAssert(numInputs == 0, "number of "+typeId+" inputs != 0", log);
                        LogAssert(numOutputs == 1, "number of "+typeId+" outputs != 1", log);
                        LogAssert(outputs[0].Type == ReferenceType.Buffer, typeId+" output type  isn't a buffer", log);
                        opcode = bufferInstructionMap[typeId];
                        break;

                    case IntermediateOpcodes.HOLD:
                        LogAssert(numInputs == 1, "number of "+typeId+" inputs != 1", log);
                        LogAssert(numOutputs == 1, "number of "+typeId+" outputs != 1", log);
                        LogAssert(inputs[0].IsScalar(), typeId+" input type isn't scalar", log);
                        LogAssert(!outputs[0].IsScalar(), typeId+" output type is scalar", log);
                        if (inputs[0].IsScalar())
                        {
                            opcode = scalarInstructionMap[typeId];
                        }
                        else
                        {
                            opcode = bufferInstructionMap[typeId];
                        }
                        break;
                    case IntermediateOpcodes.CONVERT_TO_SIGNAL:
                    case IntermediateOpcodes.CONVERT_TO_NORMALIZED:
                    case IntermediateOpcodes.ABS:
                    case IntermediateOpcodes.FLOOR:
                    case IntermediateOpcodes.CEIL:
                    case IntermediateOpcodes.ROUND:
                    case IntermediateOpcodes.SIGN:
                    case IntermediateOpcodes.SINE:
                    case IntermediateOpcodes.COS:
                    case IntermediateOpcodes.TRI:
                    case IntermediateOpcodes.SAW:
                    case IntermediateOpcodes.SQUARE:
                    case IntermediateOpcodes.NOTE_TO_FREQ:
                        LogAssert(numInputs == 1, "number of "+typeId+" inputs != 1", log);
                        LogAssert(numOutputs == 1, "number of "+typeId+" outputs != 1", log);
                        LogAssert(inputs[0].IsScalar() == outputs[0].IsScalar(), typeId+" input and output have different values for IsScalar", log);
                        if (inputs[0].IsScalar())
                        {
                            opcode = scalarInstructionMap[typeId];
                        }
                        else
                        {
                            opcode = bufferInstructionMap[typeId];
                        }
                        break;
                        case IntermediateOpcodes.SUBTRACT:
                        LogAssert(numInputs == 2, "number of "+typeId+" inputs != 2", log);
                        LogAssert(numOutputs == 1, "number of "+typeId+" outputs != 1", log);
                        
                        if (inputs[0].IsScalar() && inputs[1].IsScalar())
                        {
                            opcode = scalarInstructionMap[typeId];
                        }
                        else
                        {
                            if (inputs[1].IsScalar())
                            {
                                opcode = bufferScalarInstructionMap[typeId];
                            }
                            else if (inputs[0].IsScalar())
                            {
                                opcode = SynthOpcodeIds.SUBTRACT_SCALAR_BUFFER;
                            }
                            else
                            {
                                opcode = bufferInstructionMap[typeId];
                            }
                        }
                        break;
                        case IntermediateOpcodes.MULTIPLY:
                        case IntermediateOpcodes.SUM:
                        LogAssert(numInputs == 2, "number of "+typeId+" inputs != 2", log);
                        LogAssert(numOutputs == 1, "number of "+typeId+" outputs != 1", log);

                        if (inputs[0].IsScalar() != outputs[0].IsScalar())
                        {
                            // Reorder so that the scalar input is second param
                            SynthReference i0 = inputs[0];
                            SynthReference i1 = inputs[1];
                            inputs[0] = i1;
                            inputs[1] = i0;
                        }

                        LogAssert(inputs[0].IsScalar() == outputs[0].IsScalar(), typeId+" input[0] and output have different values for IsScalar", log);
                        if (inputs[0].IsScalar())
                        {
                            opcode = scalarInstructionMap[typeId];
                        }
                        else
                        {
                            if (inputs[1].IsScalar())
                            {
                                opcode = bufferScalarInstructionMap[typeId];
                            }
                            else
                            {
                                opcode = bufferInstructionMap[typeId];
                            }
                        }
                        break;

                    // 2 in, 1 out                    
                    case IntermediateOpcodes.RANDOMIZE_ONINIT:
                    case IntermediateOpcodes.HARDKNEE:
                    case IntermediateOpcodes.MODULUS:
                    case IntermediateOpcodes.POWER:
                    case IntermediateOpcodes.HARD_CLIP:
                    case IntermediateOpcodes.SOFT_CLIP:                   
                    case IntermediateOpcodes.OnePole_HPF:
                    case IntermediateOpcodes.OnePole_LPF:
                    case IntermediateOpcodes.DIVIDE:
                    case IntermediateOpcodes.SAH_STATIC_OUTPUT:
                    case IntermediateOpcodes.GATE:
                        LogAssert(numInputs == 2, "number of "+typeId+" inputs != 2", log);
                        LogAssert(numOutputs == 1, "number of "+typeId+" outputs != 1", log);
                        LogAssert(inputs[0].IsScalar() == outputs[0].IsScalar(), typeId+" input[0] and output have different values for IsScalar", log);
                        if (inputs[0].IsScalar())
                        {
                            opcode = scalarInstructionMap[typeId];
                        }
                        else
                        {
                            if (inputs[1].IsScalar())
                            {
                                opcode = bufferScalarInstructionMap[typeId];
                            }
                            else
                            {
                                opcode = bufferInstructionMap[typeId];
                            }
                        }
                        break;

                    case IntermediateOpcodes.LERP:
                    {
                        LogAssert(numInputs == 3, "number of "+typeId+" inputs != 3", log);
                        LogAssert(numOutputs == 1, "number of "+typeId+" outputs != 1", log);

                        
                        if (inputs[0].IsScalar())
                        {
                            LogAssert(inputs[1].IsScalar() == inputs[2].IsScalar(), typeId+" input[1] and inputs[2] have different values for IsScalar", log);

                            if (inputs[1].IsScalar())
                            {
                                // lerp two scalars
                                LogAssert(outputs[0].IsScalar(), typeId+" output type isn't scalar", log);
                                opcode = SynthOpcodeIds.LERP_SCALAR;
                            }
                            else
                            {
                                // lerp two buffers with one scalar
                                LogAssert(!outputs[0].IsScalar(), typeId+" output type is scalar", log);
                                opcode = SynthOpcodeIds.LERP_BUFFER_BUFFER;
                            }
                        }
                        else
                        {
                            if (!inputs[1].IsScalar())
                            {
                                LogAssert(!outputs[0].IsScalar(), typeId+" output type is scalar", log);
                                LogAssert(!inputs[2].IsScalar(), typeId+" inputs[2] type is scalar", log);
                                // Fully dynamic LERP
                                opcode = SynthOpcodeIds.LERP_THREE_BUFFERS;
                            }
                            else
                            {
                                LogAssert(!outputs[0].IsScalar(), typeId+" output type is scalar", log);
                                LogAssert(inputs[1].IsScalar(), typeId+" input[1] type isn't scalar", log);
                                LogAssert(inputs[2].IsScalar(), typeId+" input[2] type isn't scalar", log);
                                // Lerp a single buffer between two scalar values
                                opcode = SynthOpcodeIds.LERP_BUFFER;
                            }
                        }
                    }
                        break;
                    // 3 in, 1 out
                    case IntermediateOpcodes.RANDOM:
                    case IntermediateOpcodes.ENVELOPE_FOLLOWER:
                        LogAssert(numInputs == 3, "number of "+typeId+" inputs != 3", log);
                        LogAssert(numOutputs == 1, "number of "+typeId+" outputs != 1", log);

                        LogAssert(inputs[0].IsScalar() == outputs[0].IsScalar(), typeId+" input[0] and output have different values for IsScalar", log);
                        if (inputs[0].IsScalar())
                        {
                            LogAssert(inputs[1].IsScalar(), typeId+" input[1] type isn't scalar", log);
                            LogAssert(inputs[2].IsScalar(), typeId+" input[2] type isn't scalar", log);
                            opcode = scalarInstructionMap[typeId];
                        }
                        else
                        {
                            if (inputs[1].IsScalar())
                            {
                                LogAssert(inputs[2].IsScalar(), typeId+" input[2] type isn't scalar", log);
                                opcode = bufferScalarInstructionMap[typeId];
                            }
                            else
                            {
                                opcode = bufferInstructionMap[typeId];
                            }
                        }
                        break;
                    // 5 in, 1 out
                    case IntermediateOpcodes.RESCALE:
                        LogAssert(numInputs == 5, "number of "+typeId+" inputs != 5", log);
                        LogAssert(numOutputs == 1, "number of "+typeId+" outputs != 1", log);

                        // Reorder inputs
                        SynthReference signal = inputs[2];
                        SynthReference minInput = inputs[0];
                        SynthReference maxInput = inputs[1];
                        SynthReference minOutput = inputs[3];
                        SynthReference maxOutput = inputs[4];

                        inputs[0] = signal;
                        inputs[1] = minInput;
                        inputs[2] = maxInput;
                        inputs[3] = minOutput;
                        inputs[4] = maxOutput;

                        LogAssert(inputs[0].IsScalar() == outputs[0].IsScalar(), typeId+" input[0] and output have different values for IsScalar", log);
                        if (inputs[0].IsScalar())
                        {
                            // Everything should be static
                            LogAssert(inputs[1].IsScalar(), typeId+" input[1] type isn't scalar", log);
                            LogAssert(inputs[2].IsScalar(), typeId+" input[2] type isn't scalar", log);
                            LogAssert(inputs[3].IsScalar(), typeId+" input[3] type isn't scalar", log);
                            LogAssert(inputs[4].IsScalar(), typeId+" input[4] type isn't scalar", log);
                            opcode = scalarInstructionMap[typeId];
                        }
                        else
                        {
                            if (inputs[1].IsScalar())
                            {
                                // All inputs other than 0 should be static
                                LogAssert(inputs[2].IsScalar(), typeId+" input[2] type isn't scalar", log);
                                LogAssert(inputs[3].IsScalar(), typeId+" input[3] type isn't scalar", log);
                                LogAssert(inputs[4].IsScalar(), typeId+" input[4] type isn't scalar", log);
                                opcode = bufferScalarInstructionMap[typeId];
                            }
                            else
                            {
                                // All inputs should be dynamic
                                LogAssert(!inputs[1].IsScalar(), typeId+" input[1] type is scalar", log);
                                LogAssert(!inputs[2].IsScalar(), typeId+" input[2] type is scalar", log);
                                LogAssert(!inputs[3].IsScalar(), typeId+" input[3] type is scalar", log);
                                LogAssert(!inputs[4].IsScalar(), typeId+" input[4] type is scalar", log);
                                opcode = bufferInstructionMap[typeId];
                            }
                        }
                        break;
                    case IntermediateOpcodes.NOISE:
                        LogAssert(numInputs == 0, "number of "+typeId+" inputs != 0", log);
                        LogAssert(numOutputs == 1, "number of "+typeId+" outputs != 1", log);
                        LogAssert(outputs[0].Type == ReferenceType.Buffer, typeId+" output type isn't a buffer", log);
                        opcode = SynthOpcodeIds.NOISE;
                        break;
                    case IntermediateOpcodes.BiquadProcess_2Pole:
                        LogAssert(numInputs == 6, "number of "+typeId+" inputs != 6", log);
                        LogAssert(numOutputs == 1, "number of "+typeId+" outputs != 1", log);
                        LogAssert(outputs[0].Type == ReferenceType.Buffer, typeId+" output type isn't a buffer", log);
                        opcode = SynthOpcodeIds.BiquadProcess_2Pole;
                        break;
                    case IntermediateOpcodes.BiquadProcess_4Pole:
                        LogAssert(numInputs == 6, "number of "+typeId+" inputs != 6", log);
                        LogAssert(numOutputs == 1, "number of "+typeId+" outputs != 1", log);
                        LogAssert(outputs[0].Type == ReferenceType.Buffer, typeId+" output type isn't a buffer", log);
                        opcode = SynthOpcodeIds.BiquadProcess_4Pole;
                        break;

                    // These use either bandwidth or resonance, not both
                    case IntermediateOpcodes.BiquadCoefficients_LowPass2Pole:
                    case IntermediateOpcodes.BiquadCoefficients_HighPass2Pole:
                    case IntermediateOpcodes.BiquadCoefficients_LowPass4Pole:
                    case IntermediateOpcodes.BiquadCoefficients_HighPass4Pole:
                    case IntermediateOpcodes.BiquadCoefficients_BandPass:
                    case IntermediateOpcodes.BiquadCoefficients_BandStop:
                        LogAssert(numOutputs == 5, "number of "+typeId+" outputs != 5", log);
                        LogAssert(numInputs == 2, "number of "+typeId+" inputs != 2", log);
                        LogAssert(outputs[0].Type == ReferenceType.Register, typeId+" output[0] type isn't a register", log);
                        LogAssert(outputs[1].Type == ReferenceType.Register, typeId+" output[1] type isn't a register", log);
                        LogAssert(outputs[2].Type == ReferenceType.Register, typeId+" output[2] type isn't a register", log);
                        LogAssert(outputs[3].Type == ReferenceType.Register, typeId+" output[3] type isn't a register", log);
                        LogAssert(outputs[4].Type == ReferenceType.Register, typeId+" output[4] type isn't a register", log);
                        LogAssert(inputs[0].IsScalar(), typeId+" input[0] type isn't scalar", log);
                        LogAssert(inputs[1].IsScalar(), typeId+" input[1] type isn't scalar", log);
                        
                        opcode = scalarInstructionMap[typeId];
                        break;
                    // PeakingEQ needs special handling since it requires bandwidth and resonance
                    case IntermediateOpcodes.BiquadCoefficients_PeakingEQ:
                    case IntermediateOpcodes.BiquadCoefficients_LowShelf2Pole:
                    case IntermediateOpcodes.BiquadCoefficients_LowShelf4Pole:
                    case IntermediateOpcodes.BiquadCoefficients_HighShelf2Pole:
                    case IntermediateOpcodes.BiquadCoefficients_HighShelf4Pole:
                        LogAssert(numOutputs == 5, "number of "+typeId+" outputs != 5", log);
                        LogAssert(numInputs == 3, "number of "+typeId+" inputs != 3", log);
                        LogAssert(outputs[0].Type == ReferenceType.Register, typeId+" output[0] type isn't a register", log);
                        LogAssert(outputs[1].Type == ReferenceType.Register, typeId+" output[1] type isn't a register", log);
                        LogAssert(outputs[2].Type == ReferenceType.Register, typeId+" output[2] type isn't a register", log);
                        LogAssert(outputs[3].Type == ReferenceType.Register, typeId+" output[3] type isn't a register", log);
                        LogAssert(outputs[4].Type == ReferenceType.Register, typeId+" output[4] type isn't a register", log);
                        LogAssert(inputs[0].IsScalar(), typeId+" input[0] type isn't scalar", log);
                        LogAssert(inputs[1].IsScalar(), typeId+" input[1] type isn't scalar", log);
                        LogAssert(inputs[2].IsScalar(), typeId+" input[2] type isn't scalar", log);

                        opcode = scalarInstructionMap[typeId];
                        break;

                    case IntermediateOpcodes.OSC_RAMP:
                        LogAssert(numInputs == 1, "number of "+typeId+" inputs != 1", log);
                        LogAssert(numOutputs == 1, "number of "+typeId+" outputs != 1", log);
                        if (outputs[0].IsScalar())
                        {
                            LogAssert(inputs[0].IsScalar(), typeId+" input type isn't scalar", log);
                            opcode = SynthOpcodeIds.OSC_RAMP_SCALAR;
                        }
                        else
                        {
                            if (inputs[0].IsScalar())
                            {
                                opcode = SynthOpcodeIds.OSC_RAMP_BUFFER_SCALAR;
                            }
                            else
                            {
                                opcode = SynthOpcodeIds.OSC_RAMP_BUFFER_BUFFER;
                            }
                        }
                        break;
                    case IntermediateOpcodes.TRIGGER_LATCH:
                        LogAssert(numInputs == 2, "number of "+typeId+" inputs != 2", log);
                        LogAssert(numOutputs == 1, "number of "+typeId+" outputs != 1", log);
                        LogAssert(outputs[0].Type == ReferenceType.Register, typeId+" first output type isn't a register", log);
                        opcode = SynthOpcodeIds.TRIGGER_LATCH;
                        break;
                    case IntermediateOpcodes.TRIGGER_DIFF:
                        LogAssert(numInputs == 2, "number of "+typeId+" inputs != 2", log);
                        LogAssert(numOutputs == 1, "number of "+typeId+" outputs != 1", log);
                        LogAssert(outputs[0].Type == ReferenceType.Register, typeId+" output[0] type isn't a register", log);
                        LogAssert(inputs[0].Type == ReferenceType.Register, typeId+" input[0] type isn't a register", log);
                        LogAssert(inputs[1].Type == ReferenceType.Register, typeId+" input[1] type isn't a register", log);
                        opcode = SynthOpcodeIds.TRIGGER_DIFF;
                        break;
                        
                    case IntermediateOpcodes.ENV_GEN_EXP_INT:
                    case IntermediateOpcodes.ENV_GEN_EXP_OS:
                    case IntermediateOpcodes.ENV_GEN_EXP_RET:
                    case IntermediateOpcodes.ENV_GEN_LIN_INT:
                    case IntermediateOpcodes.ENV_GEN_LIN_OS:
                    case IntermediateOpcodes.ENV_GEN_LIN_RET:
                        LogAssert(numInputs == 7, "number of "+typeId+" inputs != 7", log);
                        LogAssert(numOutputs == 1 || numOutputs == 2, "number of "+typeId+" outputs not 1 or 2", log);
                        LogAssert(outputs[0].Type == ReferenceType.Buffer, typeId+" output[0] type isn't a buffer", log);
                        if(numInputs == 2)
                        {
                            LogAssert(outputs[1].Type == ReferenceType.Register, typeId+" output[1] type isn't a register", log);
                        }
                        for (int i = 0; i < numInputs; i++)
                        {
                            LogAssert(inputs[i].IsScalar(), typeId+" input["+i+"] type isn't scalar", log);
                        }
                        opcode = bufferInstructionMap[typeId];                
                        break;

                    case IntermediateOpcodes.READ_VARIABLE:
                        LogAssert(numInputs == 1, "number of "+typeId+" inputs != 1", log);
                        LogAssert(numOutputs == 1, "number of "+typeId+" outputs != 1", log);
                        LogAssert(outputs[0].Type == ReferenceType.Register, typeId+" output type isn't a register", log);
                        LogAssert(inputs[0].Type == ReferenceType.Variable, typeId+" input type isn't a variable", log);
                        opcode = SynthOpcodeIds.READ_VARIABLE;
                        break;
                    case IntermediateOpcodes.FINISHED_TRIGGER:
                        LogAssert(numInputs == 2, "number of "+typeId+" inputs != 2", log);
                        LogAssert(numOutputs == 0, "number of "+typeId+" outputs != 0", log);
                        LogAssert(inputs[0].Type == ReferenceType.Buffer, typeId+" input type isn't a buffer", log);
                        LogAssert(inputs[1].IsScalar(), typeId+" input[1] type isn't scalar", log);
                        numInputs = 1;
                        inputs[0] = inputs[1];
                        opcode = SynthOpcodeIds.FINISHED_TRIGGER;
                        break;

                    case IntermediateOpcodes.TIMED_TRIGGER_INT:
                    case IntermediateOpcodes.TIMED_TRIGGER_OS:
                    case IntermediateOpcodes.TIMED_TRIGGER_RET:
                        LogAssert(numInputs == 6, "number of "+typeId+" inputs != 6", log);
                        LogAssert(numOutputs == 5, "number of "+typeId+" outputs != 5", log);
                        for (int i = 0; i < numInputs; i++)
                        {
                            LogAssert(inputs[i].IsScalar(), typeId+" input["+i+"] type isn't scalar", log);
                        }
                        for (int i = 0; i < numOutputs; i++)
                        {
                            LogAssert(outputs[i].Type == ReferenceType.Register, typeId+" output["+i+"] type isn't a register", log);
                        }
                        opcode = scalarInstructionMap[typeId];
                        break;
                    case IntermediateOpcodes.DECIMATE:
                        opcode = SynthOpcodeIds.DECIMATE;
                        break;
                    case IntermediateOpcodes.COUNTER:
                        opcode = SynthOpcodeIds.COUNTER;
                        break;
                    case IntermediateOpcodes.COUNTER_TRIGGER:
                        opcode = SynthOpcodeIds.COUNTER_TRIGGER;
                        break;

                    case IntermediateOpcodes.SMALLDELAY_FRAC:
                        opcode = SynthOpcodeIds.SMALLDELAY_FRAC;
                        break;
                    case IntermediateOpcodes.SMALLDELAY_STATIC:
                        opcode = SynthOpcodeIds.SMALLDELAY_STATIC;
                        break;
                    case IntermediateOpcodes.SMALLDELAY_FRAC_FB:
                        opcode = SynthOpcodeIds.SMALLDELAY_FRAC_FB;
                        break;
                    case IntermediateOpcodes.SMALLDELAY_STATIC_FB:
                        opcode = SynthOpcodeIds.SMALLDELAY_STATIC_FB;
                        break;
                    case IntermediateOpcodes.MAX:
                        if (inputs[0].IsScalar())
                            opcode = scalarInstructionMap[typeId];
                        else
                            opcode = bufferInstructionMap[typeId];
                        break;
                    case IntermediateOpcodes.COMPRESSOR_EG:
                        opcode = SynthOpcodeIds.COMPRESSOR_EG;
                        break;
                    case IntermediateOpcodes.AW_FILTER:
                        opcode = SynthOpcodeIds.AW_FILTER;
                        break;
                    case IntermediateOpcodes.Switch_Norm:
                    case IntermediateOpcodes.Switch_Index:
                    case IntermediateOpcodes.Switch_Lerp:
                    case IntermediateOpcodes.Switch_EqualPower:
                        if (inputs[1].IsScalar())
                        {
                            opcode = scalarInstructionMap[typeId];
                        }
                        else
                        {
                            opcode = bufferInstructionMap[typeId];
                            // Need to reorder so that we can use replacing mode
                            var t = inputs[0];
                            var firstInput = inputs[1];

                            inputs[0] = firstInput;
                            inputs[1] = t;
                        }
                        break;

                    case IntermediateOpcodes.Allpass:

                        if (inputs[1].IsScalar())
                        {
                            opcode = scalarInstructionMap[typeId];
                        }
                        else
                        {
                            opcode = bufferInstructionMap[typeId];
                        }
                        break;
                }

                bool isReplacing = replacingOpcodes.Contains(opcode);
                if (isReplacing)
                {
                    LogAssert(inputs[0].Type == outputs[0].Type && inputs[0].Id == outputs[0].Id, typeId+" input and ouput have different types or different Ids", log);
                }

                if (opcode == SynthOpcodeIds.END)
                {
                    Console.WriteLine("Failed to map intermediate instruction {0} with inputs:", typeId);
                    foreach (var i in inputs)
                    {
                        Console.WriteLine(i.ToString());
                    }
                    Console.WriteLine("Outputs:");
                    foreach (var i in outputs)
                    {
                        Console.WriteLine(i.ToString());
                    }
                }

                // opcode format; [ operation: 8 bits | numInputs: 4 bits | numOutputs: 4 bits ]
                uint encodedOpcode = ((uint)opcode << 8) | (numInputs << 4) | (numOutputs);
                
                output.Write((ushort)encodedOpcode);

                // Then inputs, 16bits per input
                for(uint i = 0; i < numOutputs; i++)
                {
                    output.Write(outputs[i].Encode());
                }

                // Then outputs, 16bits per output - don't repeat the first output if opcode is replacing
                uint startIndex = 0;
                if (isReplacing)
                {
                    startIndex = 1;
                }
                for (uint i = startIndex; i < numInputs; i++)
                {
                    output.Write(inputs[i].Encode());
                }
        
                // Finally optional state blocks
                if (opcodeStateRequirements.ContainsKey(opcode))
                {
                    int numStateBlocksForOpcode = opcodeStateRequirements[opcode];
                    ushort stateBlockStartId = (ushort)numStateBlocks;
                    output.Write(stateBlockStartId);
                    numStateBlocks += numStateBlocksForOpcode;

                    if (numStateBlocks > (int)ResourceLimits.kMaxStateBlocks)
                    {
                        //throw new Exception("Too many state blocks; limit is " + ((int)ResourceLimits.kMaxStateBlocks).ToString());
                    }
                }

                costEstimate += EstimateCost(opcode);
            }

            // End of program marker
            output.Write((ushort)0xffff);

            // Pad to next DWORD
            if((output.Tell() & 3) != 0)
            {
                LogAssert(((output.Tell()+2) & 3) == 0, "((output.Tell()+2) & 3) is not 0", log);
                output.Write((ushort)0);
            }

            // Overwrite the size value
            uint endOffset = output.Tell();
            output.Seek(startOffset, SeekOrigin.Begin);
            // program size should not include header (programSizeBytes, numStateBlocks, costEstimate etc)
            output.Write(endOffset - startOffset - headerSize);
            output.Write(numStateBlocks);
            if (includeCostEstimate)
            {
                output.Write((uint)costEstimate);
            }
            output.Seek(endOffset, SeekOrigin.Begin);
            return true;
        }

        enum ReferenceType
        {
            Buffer = 0,
            Register,
            Constant,
            Variable
        };

        class SynthReference
        {
            public readonly ReferenceType Type;
            public readonly uint Id;
            public SynthReference(ReferenceType type, uint id)
            {
                Type = type;
                Id = id;
            }

            public ushort Encode()
            {
                ushort val = (ushort)((uint)Type << 8);
                val |= (byte)Id;
                return val;
            }

            public bool IsScalar()
            {
                return Type != ReferenceType.Buffer;
            }

            public override string ToString()
            {
                return Type.ToString() + "_" + Id.ToString();
            }
        };

        SynthReference EncodeReference(XElement reference)
        {
            ReferenceType t;
            ushort id;
            if(reference.Name.LocalName == "Static")
            {
                string idString = reference.Attribute("id").Value;
                if(idString[0] == 'R')
                {
                    t = ReferenceType.Register;
                }
                else
                {
                    t = ReferenceType.Constant;
                }

                id = ushort.Parse(idString.Substring(1));
            }
            else if (reference.Name.LocalName == "Variable")
            {
                t = ReferenceType.Variable;
                id = ushort.Parse(reference.Attribute("id").Value);
            }
            else
            {
                t = ReferenceType.Buffer;
                id = ushort.Parse(reference.Attribute("id").Value);
            }

            return new SynthReference(t, id);
        }

        public string WriteConstuctorInitializerListEntry(TextWriter writer, string value)
        {
            return null;
        }

        public void WriteStructureEntry(TextWriter writer)
        {
            writer.WriteLine("rage::u32 programSizeBytes;");
            writer.WriteLine("rage::u32 numStateBlocks;");
            writer.WriteLine("rage::u32 costEstimate;");
            writer.Write("rage::u16 ");
        }

        public void WriteFunctions(IndentedTextWriter writer, ICommonFieldDefinition fieldDefinition)
        {
            // do nothing
        }

        #endregion

        float EstimateCost(SynthOpcodeIds opcode)
        {
            return m_OpcodeTimings.EstimateCost(opcode.ToString());
        }
    }
      
}
