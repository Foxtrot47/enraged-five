﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;

namespace rage.AMPCompiler
{
    class AudioInput : ModuleInstance
    {
        public AudioInput(XElement element)
            : base(element)
        {

        }

        public AudioInput()
        {

        }

        public override int NumInputs
        {
            get { return 0; }
        }

        public override int NumOutputs
        {
            get { return 1; }
        }

        public override int NumFields
        {
            get { return 1; }
        }

        public override PinFormat GetInputFormat(int index)
        {
            return PinFormat.Signal;
        }

        public override PinFormat GetOutputFormat(int index)
        {
            return PinFormat.Signal;
        }

        public override PinState GetOutputState(int index)
        {
            return PinState.Dynamic;
        }

        public override IntermediateOpcodes Instruction
        {
            get 
            {
                int inputId = (int)FieldValues[0];
                if (inputId >= 0 && inputId < 8)
                {
                    return (IntermediateOpcodes)((int)IntermediateOpcodes.READ_INPUT_0 + inputId);
                }
                else
                {
                    return IntermediateOpcodes.INVALID;
                }
            }
        }

        public override bool IsAudioInput
        {
            get
            {
                return true;
            }
        }
    }
}
