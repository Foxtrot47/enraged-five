﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace rage.AMPCompiler
{
    class Gate : ModuleInstance
    {
        public Gate(XElement element)
            : base(element)
        {

        }

        public Gate()
        {

        }

        public override int NumInputs
        {
            get { return 2; }
        }

        public override int NumOutputs
        {
            get { return 1; }
        }

        public override int NumFields
        {
            get { return 0; }
        }

        public override PinFormat GetInputFormat(int index)
        {
            if (index == 0)
                return PinFormat.Either;
            return PinFormat.Normalized;
        }

        public override PinFormat GetOutputFormat(int index)
        {
            return PinFormat.Normalized;
        }

        public override PinState GetOutputState(int index)
        {
            if (Inputs[0].IsConnected)
            {
                return Inputs[0].OtherPin.State;
            }
            return PinState.Static;
        }

        public override IntermediateOpcodes Instruction
        {
            get { return IntermediateOpcodes.GATE; }
        }
    }
}


