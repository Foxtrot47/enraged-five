﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;

namespace rage.AMPCompiler
{
    class SampleAndHold : ModuleInstance
    {
        public SampleAndHold(XElement element)
            : base(element)
        {

        }

        public SampleAndHold()
        {

        }

        public override int NumInputs
        {
            get { return 2; }
        }

        public override int NumOutputs
        {
            get { return 1; }
        }

        public override int NumFields
        {
            get { return 1; }
        }

        public override PinFormat GetInputFormat(int index)
        {
            if(index == 1)
                return PinFormat.Normalized;
            return PinFormat.Either;
        }

        public override PinFormat GetOutputFormat(int index)
        {
            if (Inputs[0].IsConnected)
            {
                return Inputs[0].OtherPin.InstanceFormat;
            }
            return PinFormat.Normalized;
        }

        public override PinState GetInputState(int index)
        {
            return PinState.Static;
        }

        public override PinState GetOutputState(int index)
        {
            return PinState.Static;
        }

        public override void ExpandBuiltInImplementations(SynthGraph graph)
        {
            if (!Inputs[1].IsConnected && Inputs[1].StaticValue >= 1.0f)
            {              
                if (Inputs[0].IsConnected)
                {
                    if (Inputs[0].State == PinState.Dynamic)
                    {
                        var sample = graph.CreateModuleInstance<SampleBuffer>();
                        RewireInput(sample.Inputs[0], Inputs[0]);
                        RewireOutputs(sample.Outputs[0], Outputs[0]);
                    }
                    else
                    {
                        // Trigger pin is not connected and is high; pass through static input directly
                        RewireOutputs(Inputs[0].OtherPin, Outputs[0]);
                    }
                }
                else
                {
                    PushStaticValueToDestinations(Inputs[0].StaticValue, Outputs[0].OutputDestinations, Inputs[0].InstanceFormat);
                }
            }
        }

        bool IsStaticOutput
        {
            get
            {
                // bool DynamicOutput
                return FieldValues[0] == 0.0f;
            }
        }

        public override IntermediateOpcodes Instruction
        {
            get
            {
                if (IsStaticOutput)
                {
                    return IntermediateOpcodes.SAH_STATIC_OUTPUT;
                }
                else
                {
                    return IntermediateOpcodes.INVALID;
                }
            }
        }

    }
}
