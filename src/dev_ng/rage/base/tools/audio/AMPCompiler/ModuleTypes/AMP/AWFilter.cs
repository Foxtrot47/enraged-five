﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;

namespace rage.AMPCompiler
{
    class AWFilter : ModuleInstance
    {
        public AWFilter(XElement element)
            : base(element)
        {

        }

        public AWFilter()
        {

        }

        public override int NumInputs
        {
            get { return 4; }
        }

        public override int NumOutputs
        {
            get { return 1; }
        }

        public override int NumFields
        {
            get { return 0; }
        }

        public override PinFormat GetInputFormat(int index)
        {
            if (index == 0)
                return PinFormat.Signal;
            return PinFormat.Normalized;
        }

        public override PinFormat GetOutputFormat(int index)
        {
            return PinFormat.Signal;
        }

        public override PinState GetOutputState(int index)
        {
            return PinState.Dynamic;
        }
        
        const int GainInput = 3;

        public override PinState GetInputState(int index)
        {
            if(index != GainInput)
                return PinState.Dynamic;
            return PinState.Either;
        }

        public override bool ShouldSerializeInput(int index)
        {
            if (index == GainInput)
                return false;
            return true;
        }

        public override void ExpandBuiltInImplementations(SynthGraph graph)
        {
            // Amplitude pin maps to output scaling
            ImplementAs_ScaleOutput(GainInput, graph);
        }

        public override IntermediateOpcodes Instruction
        {
            get
            {
                return IntermediateOpcodes.AW_FILTER;
            }
        }

    }
}
