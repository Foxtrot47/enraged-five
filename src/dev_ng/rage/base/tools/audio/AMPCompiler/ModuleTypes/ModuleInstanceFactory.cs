﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

using rage.ToolLib.Logging;
using rage.ToolLib;

namespace rage.AMPCompiler
{
    class ModuleInstanceFactory
    {
        ILog m_Log;
        Dictionary<ModuleTypeId, Type> m_TypeMap;
        
        public ModuleInstanceFactory(ILog log)
        {
            m_Log = log;

            m_TypeMap = new Dictionary<ModuleTypeId, Type>();
            m_TypeMap.Add(ModuleTypeId.SYNTH_AUDIOINPUT, typeof(AudioInput));
            m_TypeMap.Add(ModuleTypeId.SYNTH_AUDIOOUTPUT, typeof(AudioOutput));
            m_TypeMap.Add(ModuleTypeId.SYNTH_MULTIPLIER, typeof(MultiplierModule));
            m_TypeMap.Add(ModuleTypeId.SYNTH_DIVIDER, typeof(Divider));
            m_TypeMap.Add(ModuleTypeId.SYNTH_ADDER, typeof(AddModule));
            m_TypeMap.Add(ModuleTypeId.SYNTH_SUBTRACTER, typeof(Subtract));
            m_TypeMap.Add(ModuleTypeId.SYNTH_NOISEGENERATOR, typeof(NoiseGenerator));
            m_TypeMap.Add(ModuleTypeId.SYNTH_BIQUADFILTER, typeof(BiquadFilter));
            m_TypeMap.Add(ModuleTypeId.SYNTH_MIXER, typeof(Mixer));
            m_TypeMap.Add(ModuleTypeId.SYNTH_CONSTANT, typeof(ConstantModule));
            m_TypeMap.Add(ModuleTypeId.SYNTH_JUNCTIONPIN, typeof(Junction));
            m_TypeMap.Add(ModuleTypeId.SYNTH_FFT, typeof(FFT));
            m_TypeMap.Add(ModuleTypeId.SYNTH_FLOOR, typeof(Floor));
            m_TypeMap.Add(ModuleTypeId.SYNTH_DIGITAL_OSCILLATOR, typeof(DigitalOscillator));
            m_TypeMap.Add(ModuleTypeId.SYNTH_ENVELOPEFOLLOWER, typeof(EnvelopeFollower));
            m_TypeMap.Add(ModuleTypeId.SYNTH_RESCALER, typeof(Rescaler));
            m_TypeMap.Add(ModuleTypeId.SYNTH_CONVERTER, typeof(Converter));
            m_TypeMap.Add(ModuleTypeId.SYNTH_1POLE, typeof(OnePole));
            m_TypeMap.Add(ModuleTypeId.SYNTH_INVERTER, typeof(Inverter));
            m_TypeMap.Add(ModuleTypeId.SYNTH_CLIPPER, typeof(Clipper));
            m_TypeMap.Add(ModuleTypeId.SYNTH_HARDKNEE, typeof(HardKnee));
            m_TypeMap.Add(ModuleTypeId.SYNTH_ABS, typeof(Abs));
            m_TypeMap.Add(ModuleTypeId.SYNTH_VARIABLEINPUT, typeof(VariableInput));
            m_TypeMap.Add(ModuleTypeId.SYNTH_ENVELOPEGENERATOR, typeof(EnvelopeGenerator));
            m_TypeMap.Add(ModuleTypeId.SYNTH_TIMEDTRIGGER, typeof(TimedTrigger));
            m_TypeMap.Add(ModuleTypeId.SYNTH_TRIGGER, typeof(TriggerModule));
            m_TypeMap.Add(ModuleTypeId.SYNTH_POWER, typeof(Power));
            m_TypeMap.Add(ModuleTypeId.SYNTH_RANDOMIZER, typeof(Randomizer));
            m_TypeMap.Add(ModuleTypeId.SYNTH_SWITCH, typeof(Switch));
            m_TypeMap.Add(ModuleTypeId.SYNTH_MODULUS, typeof(Modulus));
            m_TypeMap.Add(ModuleTypeId.SYNTH_NOTE2FREQ, typeof(NoteToFreq));
            m_TypeMap.Add(ModuleTypeId.SYNTH_SAMPLEANDHOLD, typeof(SampleAndHold));
            m_TypeMap.Add(ModuleTypeId.SYNTH_DECIMATOR, typeof(Decimator));
            m_TypeMap.Add(ModuleTypeId.SYNTH_COUNTER, typeof(CounterModule));
            m_TypeMap.Add(ModuleTypeId.SYNTH_GATE, typeof(GateModule));
            m_TypeMap.Add(ModuleTypeId.SYNTH_SMALLDELAY, typeof(SmallDelay));
            m_TypeMap.Add(ModuleTypeId.SYNTH_MAX, typeof(Max));
            m_TypeMap.Add(ModuleTypeId.SYNTH_ROUND, typeof(Round));
            m_TypeMap.Add(ModuleTypeId.SYNTH_MIDIIN, typeof(MidiIn));
            m_TypeMap.Add(ModuleTypeId.SYNTH_MIDICCIN, typeof(MidiCCIn));
            m_TypeMap.Add(ModuleTypeId.SYNTH_COMPRESSOR, typeof(CompressorEG));
            m_TypeMap.Add(ModuleTypeId.SYNTH_CHARTRECORDER, typeof(ChartRecorder));
            m_TypeMap.Add(ModuleTypeId.SYNTH_AWNOISEGENERATOR, typeof(AWNoise));
            m_TypeMap.Add(ModuleTypeId.SYNTH_AWFILTER, typeof(AWFilter));
            m_TypeMap.Add(ModuleTypeId.SYNTH_SIGN, typeof(Sign));
            m_TypeMap.Add(ModuleTypeId.SYNTH_ALLPASS, typeof(Allpass));
        }

        public Type FindType(ModuleTypeId typeId)
        {
            if (m_TypeMap.ContainsKey(typeId))
                return m_TypeMap[typeId];
            throw new Exception("Cannot handle module " + typeId.ToString());
        }

       
       public ModuleInstance Create(XElement m)
        {        
            Type typeId;
            ModuleTypeId moduleType = (ModuleTypeId)(int.Parse(m.Element("TypeId").Value));
            typeId = FindType(moduleType);
            return Activator.CreateInstance(typeId, m) as ModuleInstance;
        }
    }
}
