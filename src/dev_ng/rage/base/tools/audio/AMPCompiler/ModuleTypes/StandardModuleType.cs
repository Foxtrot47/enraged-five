﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;

using rage.ToolLib;

namespace rage.AMPCompiler
{
    [DebuggerDisplay("Type: {TypeId}")]
    class StandardModuleType : IModuleType
    {
        string m_ExplicitTypeId;
        public ModuleTypeId TypeId
        {
            get
            {
                if (m_ExplicitTypeId != null)
                {
                    return Enum<ModuleTypeId>.Parse(m_ExplicitTypeId);
                }
                return ModuleTypeLoader.ParseTypeId(Name);
            }
        }

        public new bool DebugOnly
        {
            get
            {
                if (Element.Attribute("DebugOnly") != null)
                {
                    return true;
                }
                return false;
            }
        }

        public IntermediateOpcodes Instruction
        {
            get
            {
                if (IntermediateOpcode != null)
                {
                    return Enum<IntermediateOpcodes>.Parse(IntermediateOpcode);
                }

                return Enum<IntermediateOpcodes>.Parse(Name);
            }
        }
        public readonly XElement Element;
        string IntermediateOpcode;

        public IEnumerable<ModulePin> Inputs { get { return m_Inputs; } }
        public IEnumerable<ModulePin> Outputs { get { return m_Outputs; } }
        ModulePin[] m_Inputs;
        ModulePin[] m_Outputs;

        public IEnumerable<Field> Fields { get { return m_Fields; } }
        Field[] m_Fields;

        public readonly string Name;

        public StandardModuleType(XElement element)
        {
            Element = element;
            Name = Element.Name.LocalName;

            XAttribute typeIdAttrib = Element.Attribute("TypeId");
            if (typeIdAttrib != null)
            {
                m_ExplicitTypeId = typeIdAttrib.Value;
            }

            if (Element.Element("intermediateOpcode") != null)
            {
                IntermediateOpcode = Element.Element("intermediateOpcode").Value;
            }

            m_Inputs = (from m in Element.Elements("Inputs").Elements() select new ModulePin(m)).ToArray();
            m_Outputs = (from m in Element.Elements("Outputs").Elements() select new ModulePin(m)).ToArray();
            int fieldIndex = 0;
            m_Fields = (from m in Element.Elements("Fields").Elements() select new Field(m, fieldIndex++)).ToArray();
        }

        virtual public void ExpandBuiltInImplementations(SynthGraph graph, ModuleInstance instance)
        {
           
        }

       
        

        virtual public PinState ComputeOutputState(OutputPinInstance pin)
        {
            // If the output definition specifies a state
            if(pin.PinDefinition.State != PinState.Either)
            {
                return pin.PinDefinition.State;
            }
            // When not specified, we'll assume that all outputs be dynamic if we have any
            // dynamic inputs.
            int dynamicInputs = pin.ParentModule.Inputs.Count(i => i.InstanceState == PinState.Dynamic);
            if(dynamicInputs >= 1)
            {
                return PinState.Dynamic;
            }
            else
            {
                return PinState.Static;
            }    
        }

        virtual public void EvaluateOffline(ModuleInstance instance)
        {

        }
    }
}
