﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace rage.AMPCompiler
{
    class Switch : ModuleInstance
    {
        public Switch(XElement element)
            : base(element)
        {

        }

        public Switch()
        {

        }

        public override int NumInputs
        {
            get { return 17; }
        }

        public override int NumOutputs
        {
            get { return 1; }
        }

        public override int NumFields
        {
            get { return 2; }
        }

        public override PinFormat GetInputFormat(int index)
        {
            if (index == 0)
            {
                return PinFormat.Normalized;
            }
            else
            {
                bool hasSignalInputs = false;
                bool first = true;
                foreach (InputPinInstance input in Inputs)
                {
                    if (first)
                    {
                        first = false;
                        continue;
                    }

                    if (input.IsConnected)
                    {
                        if (input.OtherPin.InstanceFormat == PinFormat.Signal)
                        {
                            hasSignalInputs = true;
                            break;
                        }
                    }
                }
                // force all inputs to match
                return hasSignalInputs ? PinFormat.Signal : PinFormat.Normalized; ;
            }
        }

        public override PinFormat GetOutputFormat(int index)
        {
            return GetInputFormat(1);
        }

        public override PinState GetInputState(int index)
        {
            if (index == 0)
            {
                return PinState.Static;
            }
            else
            {
                bool hasDynamicInputs = false;
                bool first = true;
                foreach (InputPinInstance input in Inputs)
                {
                    if (first)
                    {
                        first = false;
                        continue;
                    }

                    if (input.IsConnected)
                    {
                        if (input.OtherPin.State == PinState.Dynamic)
                        {
                            hasDynamicInputs = true;
                            break;
                        }
                    }
                }
                // force all inputs to match
                return hasDynamicInputs ? PinState.Dynamic : PinState.Static;
            }
        }

        public override PinState GetOutputState(int index)
        {
            return GetInputState(1);
        }

        int NumSwitchValues
        {
            get
            {
                return (int)FieldValues[0];
            }
        }

        enum SwitchModeIds
        {
            RescaledInput = 0,
            NormalizedInput,
            InterpInput,
            EqualPower,
        };

        SwitchModeIds SwitchMode
        {
            get
            {
                return (SwitchModeIds)((int)FieldValues[1]);
            }
        }


        public override IntermediateOpcodes Instruction
        {
            get 
            {
                switch (SwitchMode)
                {
                    case SwitchModeIds.NormalizedInput:
                        return IntermediateOpcodes.Switch_Norm;
                    case SwitchModeIds.RescaledInput:
                        return IntermediateOpcodes.Switch_Index;
                    case SwitchModeIds.InterpInput:
                        return IntermediateOpcodes.Switch_Lerp;
                    case SwitchModeIds.EqualPower:
                        return IntermediateOpcodes.Switch_EqualPower;
                    default:
                        return IntermediateOpcodes.INVALID;
                }                
            }
        }

        public override void ExpandBuiltInImplementations(SynthGraph graph)
        {
            if (Inputs[0].IsConnected == false)
            {
                // We can evaluate this expression offline
                switch (SwitchMode)
                {
                    case SwitchModeIds.RescaledInput:
                        {
                            int inputIndex = (int)Inputs[0].StaticValue % NumSwitchValues;
                            RewireOutputs(Inputs[inputIndex+1].OtherPin, Outputs[0]);
                        }
                        break;
                    case SwitchModeIds.NormalizedInput:
                        {
                            int inputIndex = (int)(Inputs[0].StaticValue * NumSwitchValues);
                            if (inputIndex >= NumSwitchValues)
                                inputIndex = NumSwitchValues - 1;
                            RewireOutputs(Inputs[inputIndex+1].OtherPin, Outputs[0]);
                        }
                        break;
                    case SwitchModeIds.InterpInput:
                        {
                            int index0 = (int)Inputs[0].StaticValue % NumSwitchValues;
                            float interpFactor = Inputs[0].StaticValue - (float)Math.Floor(Inputs[0].StaticValue);
                            if (index0 >= NumSwitchValues - 1 || interpFactor == 0.0f)
                            {
                                RewireOutputs(Inputs[index0 + 1].OtherPin, Outputs[0]);
                            }
                            else
                            {
                                var lerp = graph.CreateModuleInstance<Lerp>();
                                RewireOutputs(lerp.Outputs[0], Outputs[0]);
                                lerp.Inputs[0].StaticValue = interpFactor;
                                RewireInput(lerp.Inputs[1], Inputs[index0 + 1]);
                                RewireInput(lerp.Inputs[2], Inputs[index0 + 2]);
                            }
                        }
                        break;
                    case SwitchModeIds.EqualPower:
                        {                           
                            double t = Inputs[0].StaticValue;
		                    int index0 = (int)t % NumSwitchValues;
		                    double interpFactor = t - Math.Floor(t);

		                    if(index0 >= NumSwitchValues- 1 || interpFactor == 0.0)
		                    {
                                RewireOutputs(Inputs[index0 + 1].OtherPin, Outputs[0]);
		                    }
		                    else
		                    {
			                    double angle = interpFactor * Math.PI * 0.5;
			                    double linVol2 = Math.Sin(angle);
			                    double linVol1 = Math.Cos(angle);

                                var mul1 = graph.CreateModuleInstance<Multiplier>();
                                RewireInput(mul1.Inputs[0], Inputs[index0 + 1]);
                                mul1.Inputs[1].StaticValue = (float)linVol1;
                                mul1.Inputs[1].InstanceFormat = PinFormat.Normalized;

                                var mul2 = graph.CreateModuleInstance<Multiplier>();
                                RewireInput(mul2.Inputs[0], Inputs[index0 + 2]);
                                mul2.Inputs[1].StaticValue = (float)linVol2;
                                mul2.Inputs[1].InstanceFormat = PinFormat.Normalized;

                                var add = graph.CreateModuleInstance<Adder>();
                                add.Inputs[0].Connect(mul1.Outputs[0]);
                                add.Inputs[1].Connect(mul2.Outputs[0]);
                                RewireOutputs(add.Outputs[0], Outputs[0]);
		                    }
                        }
                        break;
                }
            } 
        }
    }
}


