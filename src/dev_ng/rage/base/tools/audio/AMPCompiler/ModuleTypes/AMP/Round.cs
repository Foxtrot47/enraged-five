﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;

namespace rage.AMPCompiler
{
    class Round : ModuleInstance
    {
        public Round(XElement element)
            : base(element)
        {

        }

        public Round()
        {

        }

        public override int NumInputs
        {
            get { return 1; }
        }

        public override int NumOutputs
        {
            get { return 1; }
        }

        public override int NumFields
        {
            get { return 0; }
        }

        public override PinFormat GetInputFormat(int index)
        {
            return PinFormat.Either;
        }

        public override PinFormat GetOutputFormat(int index)
        {
            if (Inputs[0].IsConnected)
            {
                return Inputs[0].OtherPin.InstanceFormat;
            }
            return PinFormat.Normalized;
        }

        public override PinState GetOutputState(int index)
        {
            return Inputs[0].State;
        }

        public override IntermediateOpcodes Instruction
        {
            get { return IntermediateOpcodes.ROUND; }
        }

        public override void EvaluateOffline()
        {
            if (Inputs[0].IsConnected == false)
            {
                double result = Math.Round(Inputs[0].StaticValue);
                PushStaticValueToDestinations(result, Outputs[0].OutputDestinations, GetOutputFormat(0));
            }
        }
    }
}
