﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace rage.AMPCompiler
{
    class AddModule : ModuleInstance
    {
        public AddModule(XElement element)
            : base(element)
        {

        }

        public AddModule()
        {

        }

        public override int NumInputs
        {
            get { return 16; }
        }

        public override int NumOutputs
        {
            get { return 1; }
        }

        public override int NumFields
        {
            get { return 0; }
        }

        public override PinFormat GetInputFormat(int index)
        {
            return PinFormat.Either;
        }

        public override PinFormat GetOutputFormat(int index)
        {
            PinFormat format = PinFormat.Normalized;
            foreach (InputPinInstance input in m_Inputs)
            {
                if (input.IsConnected)
                {
                    if (input.OtherPin.InstanceFormat == PinFormat.Signal)
                    {
                        format = PinFormat.Signal;
                    }
                }
            }
            return format;
        }

        public override PinState GetOutputState(int index)
        {
            PinState state = PinState.Static;
            foreach (InputPinInstance input in m_Inputs)
            {
                if (input.IsConnected)
                {
                    if (input.OtherPin.State == PinState.Dynamic)
                    {
                        state = PinState.Dynamic;
                    }
                }
            }
            return state;
        }

        public override IntermediateOpcodes Instruction
        {
            get { return IntermediateOpcodes.INVALID; }
        }

        public override void ExpandBuiltInImplementations(SynthGraph graph)
        {
            List<OutputPinInstance> outputsToSum = new List<OutputPinInstance>();

            float staticPart = 0.0f;
            foreach (InputPinInstance input in Inputs)
            {
                if (input.IsConnected)
                {
                    outputsToSum.Add(input.OtherPin);
                    input.Disconnect();
                }
                else
                {
                    staticPart += input.StaticValue;
                }
            }
            while (outputsToSum.Count > 1)
            {
                List<OutputPinInstance> cascadedOutputsToSum = new List<OutputPinInstance>();

                for (int i = 0; i < outputsToSum.Count / 2; i++)
                {
                    int firstPinIndex = i * 2;
                    var adder = graph.CreateModuleInstance<Adder>();

                    adder.Inputs[0].Connect(outputsToSum[firstPinIndex]);
                    adder.Inputs[1].Connect(outputsToSum[firstPinIndex + 1]);

                    cascadedOutputsToSum.Add(adder.Outputs[0]);
                }

                // If we have an odd number of outputs we'll leave one behind - ensure it gets
                // included in the next cascade.
                if (outputsToSum.Count % 2 != 0)
                {
                    cascadedOutputsToSum.Add(outputsToSum.Last());
                }

                outputsToSum = cascadedOutputsToSum;
            }

            if (outputsToSum.Count > 0)
            {
                if (staticPart != 0.0f)
                {
                    var cascadedOutput = outputsToSum[0];
                    var finalAdder = graph.CreateModuleInstance<Adder>();
                    finalAdder.Inputs[0].Connect(cascadedOutput);
                    finalAdder.Inputs[1].StaticValue = staticPart;

                    outputsToSum[0] = finalAdder.Outputs[0];
                }

                List<InputPinInstance> destinations = Outputs[0].OutputDestinations.ToList();
                foreach (InputPinInstance destination in destinations)
                {
                    destination.Connect(outputsToSum[0]);
                }
            }
            else if (staticPart != 0.0f)
            {
                PushStaticValueToDestinations(staticPart, Outputs[0].OutputDestinations, PinFormat.Normalized);
            }
        }
    }
}
