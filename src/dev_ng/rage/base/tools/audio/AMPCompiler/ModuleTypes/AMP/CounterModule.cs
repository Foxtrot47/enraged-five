﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;

namespace rage.AMPCompiler
{
    class CounterModule : ModuleInstance
    {
        public CounterModule(XElement element)
            : base(element)
        {

        }

        public CounterModule()
        {

        }

        public override int NumInputs
        {
            get { return 4; }
        }

        public override int NumOutputs
        {
            get { return 1; }
        }

        public override int NumFields
        {
            get { return 0; }
        }

        public override PinFormat GetInputFormat(int index)
        {
            return PinFormat.Normalized;
        }

        public override PinFormat GetOutputFormat(int index)
        {
            return PinFormat.Normalized;
        }

        public override PinState GetOutputState(int index)
        {
            return PinState.Static;
        }

        public override PinState GetInputState(int index)
        {
            return PinState.Static;
        }

        public override void ExpandBuiltInImplementations(SynthGraph graph)
        {
            if (Outputs[0].OutputDestinations.Count > 0)
            {
                var counter = graph.CreateModuleInstance<Counter>();
                RewireOutput(Outputs[0], counter);
                
            }

            if (Outputs[1].OutputDestinations.Count > 0)
            {
                var trigger = graph.CreateModuleInstance<CounterTrigger>();
                RewireOutput(Outputs[1], trigger);
            }
        }

        void RewireOutput(OutputPinInstance output, ModuleInstance module)
        {
            RewireOutputs(module.Outputs[0], output);
            for (int i = 0; i < Inputs.Count(); i++)
            {
                RewireInput(module.Inputs[i], Inputs[i]);
            }
        }

        public override IntermediateOpcodes Instruction
        {
            get
            {
                return IntermediateOpcodes.INVALID;
            }
        }

    }
}
