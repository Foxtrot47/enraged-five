﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace rage.AMPCompiler
{
    class Mixer : ModuleInstance
    {
        public Mixer(XElement element)
            : base(element)
        {

        }

        public Mixer()
        {

        }

        public override int NumInputs
        {
            get { return 16; }
        }

        public override int NumOutputs
        {
            get { return 1; }
        }

        public override int NumFields
        {
            get { return 0; }
        }

        public override PinFormat GetInputFormat(int index)
        {
            int maskedIndex = index & 1;
            if (maskedIndex == 0)
            {
                return PinFormat.Signal;
            }
            return PinFormat.Normalized;
        }

        public override PinFormat GetOutputFormat(int index)
        {
            return PinFormat.Signal;
        }

        public override PinState GetOutputState(int index)
        {
           return PinState.Dynamic;
        }

        public override IntermediateOpcodes Instruction
        {
            get { return IntermediateOpcodes.INVALID; }
        }

        public override bool ShouldSerialize
        {
            get
            {
                return false;
            }
        }

        public override void ExpandBuiltInImplementations(SynthGraph graph)
        {
            List<OutputPinInstance> outputsToSum = new List<OutputPinInstance>();

            for(int i = 0; i < Inputs.Count(); i += 2)
            {
                int signalPinIndex = i;
                int gainPinIndex = i + 1;

                if (Inputs[signalPinIndex].IsConnected)
                {
                    if (Inputs[gainPinIndex].IsConnected
                        || Inputs[gainPinIndex].StaticValue != 1.0f)
                    {
                        // gain pin is connected or non-unity; need to scale the input
                        var scaleModule = graph.CreateModuleInstance<Multiplier>();

                        scaleModule.Inputs[0].Connect(Inputs[signalPinIndex].OtherPin);
                        Inputs[signalPinIndex].Disconnect();

                        // route gain into scale module
                        if (Inputs[gainPinIndex].IsConnected)
                        {
                            scaleModule.Inputs[1].Connect(Inputs[gainPinIndex].OtherPin);
                            Inputs[gainPinIndex].Disconnect();
                        }
                        else
                        {
                            scaleModule.Inputs[1].StaticValue = Inputs[gainPinIndex].StaticValue;
                        }
                        
                        outputsToSum.Add(scaleModule.Outputs[0]);
                    }
                    else
                    {
                        outputsToSum.Add(Inputs[signalPinIndex].OtherPin);
                        Inputs[signalPinIndex].Disconnect();
                    }
                }
            }

            if (outputsToSum.Count == 1)
            {
                RewireOutputs(outputsToSum[0], Outputs[0]);
            }
            else
            {
                while (outputsToSum.Count > 1)
                {
                    List<OutputPinInstance> cascadedOutputsToSum = new List<OutputPinInstance>();

                    for (int i = 0; i < outputsToSum.Count / 2; i++)
                    {
                        int firstPinIndex = i * 2;
                        var sumModule = graph.CreateModuleInstance<Adder>();

                        sumModule.Inputs[0].Connect(outputsToSum[firstPinIndex]);
                        sumModule.Inputs[1].Connect(outputsToSum[firstPinIndex + 1]);

                        cascadedOutputsToSum.Add(sumModule.Outputs[0]);
                    }

                    // If we have an odd number of outputs we'll leave one behind - ensure it gets
                    // included in the next cascade.
                    if (outputsToSum.Count % 2 != 0)
                    {
                        cascadedOutputsToSum.Add(outputsToSum.Last());
                    }

                    outputsToSum = cascadedOutputsToSum;
                }

                List<InputPinInstance> destinations = Outputs[0].OutputDestinations.ToList();
                foreach (InputPinInstance destination in destinations)
                {
                    destination.Connect(outputsToSum[0]);
                }
            }
        }
    }
}
