﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace rage.AMPCompiler
{
    class Randomizer : ModuleInstance
    {
        public Randomizer(XElement element)
            : base(element)
        {

        }

        public Randomizer()
        {

        }

        public override int NumInputs
        {
            get { return 3; }
        }

        public override int NumOutputs
        {
            get { return 1; }
        }

        public override int NumFields
        {
            get { return 2; }
        }

        public override PinFormat GetInputFormat(int index)
        {
            return PinFormat.Normalized;
        }

        public override PinFormat GetOutputFormat(int index)
        {
            return PinFormat.Normalized;
        }

        public override PinState GetOutputState(int index)
        {
           return PinState.Static;
        }

        public override PinState GetInputState(int index)
        {
            return PinState.Static;
        }

        public override void ExpandBuiltInImplementations(SynthGraph graph)
        {
            if (QuantizeMode == QuantizeModes.Quantized)
            {
                // Stick a round op on the output
                var r = graph.CreateModuleInstance<Round>();
                RewireOutputs(r.Outputs[0], Outputs[0]);
                r.Inputs[0].Connect(Outputs[0]);
            }

            if (TriggerMode == TriggerModes.OnInit)
            {
                // replace with RandomizeOnInit module
                var m = graph.CreateModuleInstance<RandomizeOnInit>();
                RewireInput(m.Inputs[0], Inputs[1]);
                RewireInput(m.Inputs[1], Inputs[2]);
                RewireOutputs(m.Outputs[0], Outputs[0]);
            }
        }

        enum QuantizeModes
        {
            Normal = 0,
            Quantized
        }

        enum TriggerModes
        {
            Triggered = 0,
            OnInit
        }

        QuantizeModes QuantizeMode
        {
            get
            {
                if (FieldValues.Count() > 0)
                {
                    if (FieldValues[0] == 1.0)
                    {
                        return QuantizeModes.Quantized;
                    }
                }
                return QuantizeModes.Normal;
            }
        }

        TriggerModes TriggerMode
        {
            get
            {
                if (FieldValues.Count() > 1)
                {
                    if (FieldValues[1] == 1.0)
                    {
                        return TriggerModes.OnInit;
                    }
                }
                return TriggerModes.Triggered;
            }
        }

        public override IntermediateOpcodes Instruction
        {
            get 
            { 
                return IntermediateOpcodes.RANDOM;
            }
        }
    }
}
