﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;

namespace rage.AMPCompiler
{
    class Clipper : ModuleInstance
    {
        public Clipper(XElement element)
            : base(element)
        {

        }

        public Clipper()
        {

        }

        public override int NumInputs
        {
            get { return 2; }
        }

        public override int NumOutputs
        {
            get { return 1; }
        }

        public override int NumFields
        {
            get { return 1; }
        }

        public override PinFormat GetInputFormat(int index)
        {
            if (index == 1)
                return PinFormat.Normalized; // normalized threshold

            return PinFormat.Either;
        }

        public override PinFormat GetOutputFormat(int index)
        {
            // Our output matches input0
            if (Inputs[0].IsConnected)
            {
                return Inputs[0].OtherPin.InstanceFormat;
            }
            return PinFormat.Normalized;
        }

        public override PinState GetOutputState(int index)
        {
            return Inputs[0].State;
        }

        bool IsSoftClip
        {
            get
            {
                return FieldValues[0] == 1.0f;
            }
        }

        public override IntermediateOpcodes Instruction
        {
            get 
            {
                if (IsSoftClip)
                {
                    return IntermediateOpcodes.SOFT_CLIP;
                }
                return IntermediateOpcodes.HARD_CLIP; 
            }
        }

    }
}
