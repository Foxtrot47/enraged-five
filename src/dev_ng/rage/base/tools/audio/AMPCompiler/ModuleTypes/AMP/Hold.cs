﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;

namespace rage.AMPCompiler
{
    class HoldSample : ModuleInstance
    {
        public HoldSample(XElement element)
            : base(element)
        {

        }

        public HoldSample()
        {

        }

        public override int NumInputs
        {
            get { return 1; }
        }

        public override int NumOutputs
        {
            get { return 1; }
        }

        public override int NumFields
        {
            get { return 0; }
        }

        public override PinFormat GetInputFormat(int index)
        {
            return PinFormat.Either;
        }

        public override PinFormat GetOutputFormat(int index)
        {
            if (Inputs[0].IsConnected)
            {
                return Inputs[0].OtherPin.InstanceFormat;
            }
            return Inputs[0].InstanceFormat;
        }

        public override PinState GetOutputState(int index)
        {
            return PinState.Dynamic;
        }

        public override PinState GetInputState(int index)
        {
            return PinState.Static;
        }

        public override IntermediateOpcodes Instruction
        {
            get { return IntermediateOpcodes.HOLD; }
        }
    }
}
