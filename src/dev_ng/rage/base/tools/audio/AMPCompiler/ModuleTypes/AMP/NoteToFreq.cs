﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;

namespace rage.AMPCompiler
{
    class NoteToFreq : ModuleInstance
    {
        public NoteToFreq(XElement element)
            : base(element)
        {

        }

        public NoteToFreq()
        {

        }

        public override int NumInputs
        {
            get { return 1; }
        }

        public override int NumOutputs
        {
            get { return 1; }
        }

        public override int NumFields
        {
            get { return 0; }
        }

        public override PinFormat GetInputFormat(int index)
        {
            return PinFormat.Normalized;
        }

        public override PinFormat GetOutputFormat(int index)
        {
            return PinFormat.Normalized;
        }        

        public override PinState GetOutputState(int index)
        {
            return Inputs[0].State;
        }

        public override void EvaluateOffline()
        {
            if (!m_Inputs[0].IsConnected)
            {
                double result = Math.Pow(2.0, (m_Inputs[0].StaticValue + 36.3763) / 12);
                PushStaticValueToDestinations(result, Outputs[0].OutputDestinations, PinFormat.Normalized);
            }
        }

        public override IntermediateOpcodes Instruction
        {
            get
            {
                return IntermediateOpcodes.NOTE_TO_FREQ;
            }
        }

    }
}
