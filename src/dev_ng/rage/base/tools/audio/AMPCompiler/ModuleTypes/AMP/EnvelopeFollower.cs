﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace rage.AMPCompiler
{
    class EnvelopeFollower : ModuleInstance
    {
        public EnvelopeFollower(XElement element)
            : base(element)
        {

        }

        public EnvelopeFollower()
        {

        }

        public override int NumInputs
        {
            get { return 3; }
        }

        public override int NumOutputs
        {
            get { return 1; }
        }

        public override int NumFields
        {
            get { return 1; }
        }

        public override PinState GetInputState(int index)
        {
            if (index == 0)
            {
                if (Mode == EnvelopeFollowerMode.ForceDynamic)
                {
                    return PinState.Dynamic;
                }
                return PinState.Either;
            }
            else
            {
                return PinState.Static;
            }
        }

        public override PinFormat GetInputFormat(int index)
        {
            if (index == 0)
                return PinFormat.Either;
            return PinFormat.Normalized;
        }

        public override PinFormat GetOutputFormat(int index)
        {
            return PinFormat.Normalized;
        }

        public override PinState GetOutputState(int index)
        {
            if (Mode == EnvelopeFollowerMode.ForceDynamic)
                return PinState.Dynamic;

            if (Inputs[0].IsConnected)
            {
                return Inputs[0].OtherPin.State;
            }
            return PinState.Static;
        }

        public override void ExpandBuiltInImplementations(SynthGraph graph)
        {
            // For signal input we need to insert an Abs()
            if (Inputs[0].IsConnected && Inputs[0].OtherPin.InstanceFormat == PinFormat.Signal)
            {
                var absModule = graph.CreateModuleInstance<Abs>();
                RewireInput(absModule.Inputs[0], Inputs[0]);
                Inputs[0].Connect(absModule.Outputs[0]);
            }
        }

        enum EnvelopeFollowerMode
        {
            Normal = 0,
            ForceDynamic,
        }

        EnvelopeFollowerMode Mode
        {
            get
            {
                if (FieldValues != null && FieldValues.Count() > 0 && FieldValues[0] == 1.0f)
                {
                    return EnvelopeFollowerMode.ForceDynamic;
                }
                return EnvelopeFollowerMode.Normal;
            }
        }

        public override IntermediateOpcodes Instruction
        {
            get { return IntermediateOpcodes.ENVELOPE_FOLLOWER; }
        }
    }
}


