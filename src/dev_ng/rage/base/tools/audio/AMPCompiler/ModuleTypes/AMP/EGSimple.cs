﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace rage.AMPCompiler
{
    class EGSimple : ModuleInstance
    {
        public EGSimple(XElement element)
            : base(element)
        {

        }

        public EGSimple()
        {

        }
        enum synthEnvelopeGeneratorInputs
        {
            kEnvelopeGeneratorPredelay = 0,
            kEnvelopeGeneratorAttack,
            kEnvelopeGeneratorDecay,
            kEnvelopeGeneratorSustain,
            kEnvelopeGeneratorHold,
            kEnvelopeGeneratorRelease,
            kEnvelopeGeneratorTrigger,

            kEnvelopeGeneratorNumInputs
        };
        int InputIndex(synthEnvelopeGeneratorInputs inputId)
        {
            return (int)inputId;
        }

        public override int NumInputs
        {
            get { return InputIndex(synthEnvelopeGeneratorInputs.kEnvelopeGeneratorNumInputs); }
        }

        public override int NumOutputs
        {
            get { return 2; }
        }

        public override PinState GetInputState(int index)
        {
            return PinState.Static;
        }

        public override PinFormat GetInputFormat(int index)
        {            
            return PinFormat.Normalized;
        }

        public override PinFormat GetOutputFormat(int index)
        {
            return PinFormat.Normalized;
        }

        public override PinState GetOutputState(int index)
        {
            if (index == 0)
            {
                return PinState.Dynamic;
            }
            return PinState.Static;
        }

        enum synthEnvelopeGeneratorFields
        {
            kReleaseType = 0,
            kTriggerMode,

            kEnvelopeGeneratorNumFields
        }

        enum synthEnvelopeReleaseType
        {
            Linear = 0,
            Exponential
        };

        enum synthEnvelopeTriggerMode
        {
            OneShot = 0,
            Retrigger,
            Interruptible
        };

        int FieldIndex(synthEnvelopeGeneratorFields fieldId)
        {
            return (int)fieldId;
        }

        public override int NumFields
        {
            get { return FieldIndex(synthEnvelopeGeneratorFields.kEnvelopeGeneratorNumFields); }
        }

        synthEnvelopeReleaseType ReleaseType
        {
            get
            {
                return (synthEnvelopeReleaseType)m_FieldValues[FieldIndex(synthEnvelopeGeneratorFields.kReleaseType)];
            }
        }

        synthEnvelopeTriggerMode TriggerMode
        {
            get
            {
                return (synthEnvelopeTriggerMode)m_FieldValues[FieldIndex(synthEnvelopeGeneratorFields.kTriggerMode)];
            }
        }

        public override IntermediateOpcodes Instruction
        {
            get
            {
                switch (TriggerMode)
                {
                    case synthEnvelopeTriggerMode.Interruptible:
                        if (ReleaseType == synthEnvelopeReleaseType.Exponential)
                            return IntermediateOpcodes.ENV_GEN_EXP_INT;
                        return IntermediateOpcodes.ENV_GEN_LIN_INT;
                    case synthEnvelopeTriggerMode.OneShot:
                        if (ReleaseType == synthEnvelopeReleaseType.Exponential)
                            return IntermediateOpcodes.ENV_GEN_EXP_OS;
                        return IntermediateOpcodes.ENV_GEN_LIN_OS;
                    case synthEnvelopeTriggerMode.Retrigger:
                        if (ReleaseType == synthEnvelopeReleaseType.Exponential)
                            return IntermediateOpcodes.ENV_GEN_EXP_RET;
                        return IntermediateOpcodes.ENV_GEN_LIN_RET;
                    default:
                        return IntermediateOpcodes.INVALID;
                }

            }
        }
    }
}
