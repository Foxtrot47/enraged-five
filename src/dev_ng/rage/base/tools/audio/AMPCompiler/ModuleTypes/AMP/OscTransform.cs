﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;

namespace rage.AMPCompiler
{
    class OscTransform : ModuleInstance
    {
        public OscTransform(XElement element)
            : base(element)
        {

        }

        public OscTransform()
        {

        }

        public override int NumInputs
        {
            get { return 1; }
        }

        public override int NumOutputs
        {
            get { return 1; }
        }

        public override int NumFields
        {
            get { return 0; }
        }

        public override PinFormat GetInputFormat(int index)
        {
            return PinFormat.Normalized;
        }

        public override PinFormat GetOutputFormat(int index)
        {
            return PinFormat.Signal;
        }

        public override PinState GetOutputState(int index)
        {
            return Inputs[0].State;
        }

        public DigitalOscillator.Waveforms Waveform
        {
            get;
            set;
        }
        public override IntermediateOpcodes Instruction
        {
            get 
            {
                switch (Waveform)
                {
                    case DigitalOscillator.Waveforms.Square:
                        return IntermediateOpcodes.SQUARE;
                    case DigitalOscillator.Waveforms.Saw:
                        return IntermediateOpcodes.SAW;
                    case DigitalOscillator.Waveforms.Sine:
                        return IntermediateOpcodes.SINE;
                    case DigitalOscillator.Waveforms.Cosine:
                        return IntermediateOpcodes.COS;
                    case DigitalOscillator.Waveforms.Triangle:
                        return IntermediateOpcodes.TRI;
                }
                return IntermediateOpcodes.INVALID;
            }
        }
    }
}
