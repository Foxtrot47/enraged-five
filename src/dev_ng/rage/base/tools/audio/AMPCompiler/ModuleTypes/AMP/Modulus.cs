﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;

namespace rage.AMPCompiler
{
    class Modulus : ModuleInstance
    {
        public Modulus(XElement element)
            : base(element)
        {

        }

        public Modulus()
        {

        }

        public override int NumInputs
        {
            get { return 2; }
        }

        public override int NumOutputs
        {
            get { return 1; }
        }

        public override int NumFields
        {
            get { return 0; }
        }

        public override PinFormat GetInputFormat(int index)
        {
            // All of our inputs should match our output format, which is based on Input0
            return PinFormat.Normalized;
        }

        public override PinFormat GetOutputFormat(int index)
        {
            return PinFormat.Normalized;
        }

        public override PinState GetOutputState(int index)
        {
            return Inputs[0].State;
        }

        public override void EvaluateOffline()
        {
            if (!Inputs[0].IsConnected && !Inputs[1].IsConnected)
            {
                double input = Inputs[0].StaticValue;
                double modulo = Inputs[1].StaticValue;
                // IEEERemainder matches our runtime modulus better than a 'true' modulus implementation
                double result = modulo == 0.0 ? 0.0 : Math.IEEERemainder(input, modulo);
                PushStaticValueToDestinations(result, Outputs[0].OutputDestinations, PinFormat.Normalized);
            }
        }

        public override IntermediateOpcodes Instruction
        {
            get
            {
                return IntermediateOpcodes.MODULUS;
            }
        }

    }
}
