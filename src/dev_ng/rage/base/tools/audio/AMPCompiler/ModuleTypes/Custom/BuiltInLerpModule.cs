﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace rage.AMPCompiler
{
    class BuiltInLerpModule : StandardModuleType
    {
        public BuiltInLerpModule(XElement element)
            : base(element)
        {

        }

        public override void EvaluateOffline(ModuleInstance instance)
        {
            if (instance.Inputs[0].IsConnected == false &&
                instance.Inputs[1].IsConnected == false &&
                instance.Inputs[2].IsConnected == false)
            {
                // We can evaluate this expression offline; all inputs are constant

                float min = instance.Inputs[1].StaticValue;
                float max = instance.Inputs[2].StaticValue;
                float t = instance.Inputs[0].StaticValue;
                float result = min + t * (max - min);

                var destinations = instance.Outputs[0].OutputDestinations.ToList();
                foreach (InputPinInstance destination in destinations)
                {
                    destination.Disconnect();
                    destination.StaticValue = result;
                    destination.SpecifiedFormat = instance.Outputs[0].InstanceFormat;
                }
            }
        }
    }
}


