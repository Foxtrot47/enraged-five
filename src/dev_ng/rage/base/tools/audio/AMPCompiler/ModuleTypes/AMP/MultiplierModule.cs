﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace rage.AMPCompiler
{
    class MultiplierModule : ModuleInstance
    {
        public MultiplierModule(XElement element)
            : base(element)
        {

        }

        public MultiplierModule()
        {

        }

        public override int NumInputs
        {
            get { return 16; }
        }

        public override int NumOutputs
        {
            get { return 1; }
        }

        public override int NumFields
        {
            get { return 0; }
        }

        public override PinFormat GetInputFormat(int index)
        {
            return PinFormat.Either;
        }

        public override PinFormat GetOutputFormat(int index)
        {
            PinFormat format = PinFormat.Normalized;
            foreach (InputPinInstance input in m_Inputs)
            {
                if (input.IsConnected)
                {
                    if (input.OtherPin.InstanceFormat == PinFormat.Signal)
                    {
                        format = PinFormat.Signal;
                    }
                }
            }
            return format;
        }

        public override PinState GetOutputState(int index)
        {
            PinState state = PinState.Static;
            foreach (InputPinInstance input in m_Inputs)
            {
                if (input.IsConnected)
                {
                    if (input.OtherPin.State == PinState.Dynamic)
                    {
                        state = PinState.Dynamic;
                    }
                }
            }
            return state;
        }

        public override IntermediateOpcodes Instruction
        {
            get { return IntermediateOpcodes.INVALID; }
        }

        public override void ExpandBuiltInImplementations(SynthGraph graph)
        {
            List<OutputPinInstance> outputsToMultiply = new List<OutputPinInstance>();

            float staticPart = 1.0f;
            foreach(InputPinInstance input in Inputs)
            {
                if (input.IsConnected)
                {
                    outputsToMultiply.Add(input.OtherPin);
                    input.Disconnect();
                }
                else
                {
                    staticPart *= input.StaticValue;
                }
            }
            while (outputsToMultiply.Count > 1)
            {
                List<OutputPinInstance> cascadedOutputsToMultiply = new List<OutputPinInstance>();

                for (int i = 0; i < outputsToMultiply.Count / 2; i++)
                {
                    int firstPinIndex = i * 2;
                    var multiplier = graph.CreateModuleInstance<Multiplier>();

                    multiplier.Inputs[0].Connect(outputsToMultiply[firstPinIndex]);
                    multiplier.Inputs[1].Connect(outputsToMultiply[firstPinIndex + 1]);

                    cascadedOutputsToMultiply.Add(multiplier.Outputs[0]);
                }

                // If we have an odd number of outputs we'll leave one behind - ensure it gets
                // included in the next cascade.
                if (outputsToMultiply.Count % 2 != 0)
                {
                    cascadedOutputsToMultiply.Add(outputsToMultiply.Last());
                }

                outputsToMultiply = cascadedOutputsToMultiply;
            }

            if (outputsToMultiply.Count == 0)
            {
                PushStaticValueToDestinations(staticPart, Outputs[0].OutputDestinations, GetOutputFormat(0));
            }
            else
            {
                if (staticPart != 1.0f)
                {
                    var cascadedOutput = outputsToMultiply[0];
                    var finalMultiplier = graph.CreateModuleInstance<Multiplier>();
                    finalMultiplier.Inputs[0].Connect(cascadedOutput);
                    finalMultiplier.Inputs[1].StaticValue = staticPart;

                    outputsToMultiply[0] = finalMultiplier.Outputs[0];
                }

                List<InputPinInstance> destinations = Outputs[0].OutputDestinations.ToList();
                foreach (InputPinInstance destination in destinations)
                {
                    destination.Connect(outputsToMultiply[0]);
                }
            }
        }
    }
}
