﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;

namespace rage.AMPCompiler
{
    class AWNoise : ModuleInstance
    {
        public AWNoise(XElement element)
            : base(element)
        {

        }

        public AWNoise()
        {

        }

        public override int NumInputs
        {
            get { return 3; }
        }

        public override int NumOutputs
        {
            get { return 1; }
        }

        public override int NumFields
        {
            get { return 0; }
        }

        public override PinFormat GetInputFormat(int index)
        {            
            return PinFormat.Normalized;
        }

        public override PinFormat GetOutputFormat(int index)
        {
            return PinFormat.Signal;
        }

        public override PinState GetOutputState(int index)
        {
            return PinState.Dynamic;
        }

        public override void ExpandBuiltInImplementations(SynthGraph graph)
        {
            var filter = graph.CreateModuleInstance<AWFilter>();
            var noise = graph.CreateModuleInstance<NoiseGenerator>();

            filter.Inputs[0].Connect(noise.Outputs[0]);
            noise.Inputs[0].StaticValue = 1.0f;

            RewireInput(filter.Inputs[1], Inputs[0]);
            RewireInput(filter.Inputs[2], Inputs[1]);
            RewireInput(filter.Inputs[3], Inputs[2]);
            RewireOutputs(filter.Outputs[0], Outputs[0]);
        }
               
        public override IntermediateOpcodes Instruction
        {
            get
            {
                return IntermediateOpcodes.INVALID;
            }
        }

    }
}
