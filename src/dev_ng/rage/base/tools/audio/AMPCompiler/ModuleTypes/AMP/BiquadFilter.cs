﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace rage.AMPCompiler
{
    class BiquadFilter : ModuleInstance
    {
        public BiquadFilter(XElement element)
            : base(element)
        {

        }

        public BiquadFilter()
        {

        }

        enum InputIds : int
        {
            Signal = 0,
            Gain,
            Freq,
            Bandwidth,
            Resonance,
            NumInputs
        }

        static int InputIndex(InputIds input)
        {
            return (int)input;
        }


        enum Fields
        {
            MinFreq = 0,
            MaxFreq,
            MinReso,
            MaxReso,
            MinWidth,
            MaxWidth,
            Mode,
            NumFields
        }

        static int FieldIndex(Fields field)
        {
            return (int)field;
        }

        public enum BiquadModes
        {
            LowPass2Pole = 0,
            HighPass2Pole,
            BandPass2Pole,
            BandStop2Pole,

            kFirst4PoleMode,
            LowPass4Pole = kFirst4PoleMode,
            HighPass4Pole,
            BandPass4Pole,
            BandStop4Pole,
            PeakingEQ,

            LowShelf2Pole,
            LowShelf4Pole,
            HighShelf2Pole,
            HighShelf4Pole,
        }

        static bool Is4Pole(BiquadModes mode)
        {
            switch (mode)
            {
                case BiquadModes.LowPass2Pole:
                case BiquadModes.HighPass2Pole:
                case BiquadModes.BandPass2Pole:
                case BiquadModes.BandStop2Pole:
                case BiquadModes.HighShelf2Pole:
                case BiquadModes.LowShelf2Pole:
                    return false;
                case BiquadModes.LowPass4Pole:
                case BiquadModes.HighPass4Pole:
                case BiquadModes.BandPass4Pole:
                case BiquadModes.BandStop4Pole:
                case BiquadModes.HighShelf4Pole:
                case BiquadModes.LowShelf4Pole:
                case BiquadModes.PeakingEQ:
                    return true;
            }

            return false;
        }
        BiquadModes GetMode()
        {
            return (BiquadModes)m_FieldValues[FieldIndex(Fields.Mode)];
        }

        public override int NumInputs
        {
            get { return (int)InputIds.NumInputs; }
        }

        public override int NumOutputs
        {
            get { return 1; }
        }

        public override int NumFields
        {
            get { return (int)Fields.NumFields; }
        }

        public override PinFormat GetInputFormat(int index)
        {
            if ((InputIds)index == InputIds.Signal)
            {
                return PinFormat.Signal;
            }
            return PinFormat.Normalized;
        }

        public override PinFormat GetOutputFormat(int index)
        {
            return PinFormat.Signal;
        }

        public override PinState GetOutputState(int index)
        {
            return PinState.Dynamic;
        }

        public override PinState GetInputState(int index)
        {
            if(index == InputIndex(InputIds.Freq) || index == InputIndex(InputIds.Bandwidth) || index == InputIndex(InputIds.Resonance))
            {
                return PinState.Static;
            }
            return PinState.Either;
        }

        public override IntermediateOpcodes Instruction
        {
            get { return IntermediateOpcodes.INVALID; }
        }

        public override bool ShouldSerialize
        {
            get
            {
                return false;
            }
        }

        public override void ExpandBuiltInImplementations(SynthGraph graph)
        {
            // Gain -> scale output and input pre-scaling
            ImplementAs_ScaleOutput(InputIndex(InputIds.Gain), graph);
            ImplementAs_LerpInput(InputIndex(InputIds.Freq), graph, m_FieldValues[FieldIndex(Fields.MinFreq)], m_FieldValues[FieldIndex(Fields.MaxFreq)]);
            ImplementAs_LerpInput(InputIndex(InputIds.Bandwidth), graph, m_FieldValues[FieldIndex(Fields.MinWidth)], m_FieldValues[FieldIndex(Fields.MaxWidth)]);
            ImplementAs_LerpInput(InputIndex(InputIds.Resonance), graph, m_FieldValues[FieldIndex(Fields.MinReso)], m_FieldValues[FieldIndex(Fields.MaxReso)]);

            // Convert into two modules; computing coefficients and process
            BiquadCoefficients coefficientsModule = graph.CreateModuleInstance <BiquadCoefficients>();

            // Set up the coefficients module with the correct mode
            BiquadModes mode = GetMode();
            coefficientsModule.Mode = mode;

            // Frequency
            if(coefficientsModule.ShouldSerializeInput(0))
                RewireInput(coefficientsModule.Inputs[0], Inputs[2]);
            // Bandwidth
            if (coefficientsModule.ShouldSerializeInput(1))
                RewireInput(coefficientsModule.Inputs[1], Inputs[3]);
            // Resonance
            if (coefficientsModule.ShouldSerializeInput(2))
                RewireInput(coefficientsModule.Inputs[2], Inputs[4]);

            BiquadProcess processModule = graph.CreateModuleInstance<BiquadProcess>();

            // Set up the correct mode - 2 or 4 pole
            processModule.Is4Pole = Is4Pole(mode);

            // Signal
            RewireInput(processModule.Inputs[0], Inputs[0]);
            // Connect coefficients
            for (int i = 0; i < 5; i++)
            {
                processModule.Inputs[i + 1].Connect(coefficientsModule.Outputs[i]);
            }

            // Rewire outputs
            InputPinInstance[] destinations = Outputs[0].OutputDestinations.ToArray();
            foreach (InputPinInstance destination in destinations)
            {
                destination.Disconnect();
                destination.Connect(processModule.Outputs[0]);
            }
        }

    }
}
