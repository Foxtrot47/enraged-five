﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;

namespace rage.AMPCompiler
{
    class Allpass : ModuleInstance
    {
        public Allpass(XElement element)
            : base(element)
        {

        }

        public Allpass()
        {

        }

        public override int NumInputs
        {
            get { return 3; }
        }

        public override int NumOutputs
        {
            get { return 1; }
        }

        public override int NumFields
        {
            get { return (int)Fields.NumFields; }
        }

        enum Fields 
        {
            MinFrequency = 0,
            MaxFrequency,

            NumFields
        }

        public override PinFormat GetInputFormat(int index)
        {
            if (index == 0)
                return PinFormat.Signal;
            return PinFormat.Normalized;
        }

        public override PinFormat GetOutputFormat(int index)
        {
           return PinFormat.Signal;
        }

        public override PinState GetOutputState(int index)
        {
            return PinState.Dynamic;
        }

        enum InputIds
        {
            Signal = 0,
            Gain,
            Cutoff,
        }

        int InputIndex(InputIds id)
        {
            return (int)id;
        }

        int FieldIndex(Fields field)
        {
            return (int)field;
        }

        public override void ExpandBuiltInImplementations(SynthGraph graph)
        {
            ImplementAs_LerpInput(InputIndex(InputIds.Cutoff), graph, m_FieldValues[FieldIndex(Fields.MinFrequency)], m_FieldValues[FieldIndex(Fields.MaxFrequency)]);
            ImplementAs_ScaleOutput(InputIndex(InputIds.Gain), graph);
        }

        public override bool ShouldSerializeInput(int index)
        {
            return index == InputIndex(InputIds.Signal) || index == InputIndex(InputIds.Cutoff);
        }

        public override IntermediateOpcodes Instruction
        {
            get { return IntermediateOpcodes.Allpass; }
        }
    }
}
