﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace rage.AMPCompiler
{
    class Divider : ModuleInstance
    {
        public Divider(XElement element)
            : base(element)
        {

        }

        public Divider()
        {

        }

        public override int NumInputs
        {
            get { return 2; }
        }

        public override int NumOutputs
        {
            get { return 1; }
        }

        public override int NumFields
        {
            get { return 0; }
        }

        public override PinFormat GetInputFormat(int index)
        {
            return PinFormat.Either;
        }

        public override PinFormat GetOutputFormat(int index)
        {
            PinFormat format = PinFormat.Normalized;
            foreach (InputPinInstance input in m_Inputs)
            {
                if (input.IsConnected)
                {
                    if (input.OtherPin.InstanceFormat == PinFormat.Signal)
                    {
                        format = PinFormat.Signal;
                    }
                }
            }
            return format;
        }

        public override PinState GetOutputState(int index)
        {
            PinState state = PinState.Static;
            foreach (InputPinInstance input in m_Inputs)
            {
                if (input.IsConnected)
                {
                    if (input.OtherPin.State == PinState.Dynamic)
                    {
                        state = PinState.Dynamic;
                    }
                }
            }
            return state;
        }

        public override IntermediateOpcodes Instruction
        {
            get { return IntermediateOpcodes.DIVIDE; }
        }

        public override void EvaluateOffline()
        {
            if (Inputs[0].IsConnected == false &&
                Inputs[1].IsConnected == false)
            {
                // We can evaluate this expression offline; all inputs are constant

                float x = Inputs[0].StaticValue;
                float y = Inputs[1].StaticValue;

                float result = y == 0.0f ? 0.0f : x / y;

                PushStaticValueToDestinations(result, Outputs[0].OutputDestinations, GetOutputFormat(0));
            }
            else if (Inputs[1].IsConnected == false)
            {
                // Dividing something by 1 is a no-op, so rewire to pass input straight through
                int staticIndex = 1;
                int connectedIndex = 0;
                if (Inputs[staticIndex].StaticValue == 1.0f)
                {
                    RewireOutputs(Inputs[connectedIndex].OtherPin, Outputs[0]);
                    Inputs[connectedIndex].Disconnect();
                }
                else if (Inputs[staticIndex].StaticValue == 0.0f)
                {
                    // in AMP's world, x/0 == 0
                    PushStaticValueToDestinations(0.0f, Outputs[0].OutputDestinations, GetOutputFormat(0));
                }
            }
            else if (Inputs[0].IsConnected == false && Inputs[0].StaticValue == 0.0f)
            {
                // 0 / x = 0
                PushStaticValueToDestinations(0.0f, Outputs[0].OutputDestinations, GetOutputFormat(0));
            }
        }
    }
}
