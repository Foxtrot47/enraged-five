﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;

namespace rage.AMPCompiler
{
    class VariableInput : ModuleInstance
    {
        public VariableInput(XElement element)
            : base(element)
        {
            VariableId = -1;
        }

        public VariableInput()
        {

        }

        public override int NumInputs
        {
            get { return 1; }
        }

        public override int NumOutputs
        {
            get { return 1; }
        }

        public override int NumFields
        {
            get { return 0; }
        }

        public override PinFormat GetInputFormat(int index)
        {
            return PinFormat.Normalized;
        }

        public override PinFormat GetOutputFormat(int index)
        {
            return PinFormat.Normalized;
        }

        public override PinState GetOutputState(int index)
        {
            return PinState.Static;
        }

        public override bool ShouldSerializeInput(int index)
        {
            return false;
        }

        public override IntermediateOpcodes Instruction
        {
            get { return IntermediateOpcodes.READ_VARIABLE; }
        }

        public override void EvaluateOffline()
        {
            if(VariableId == -1)
            {
                // We've not been exposed properly (ie don't have a variable name)
                // Act like a constant
                PushStaticValueToDestinations(m_Inputs[0].StaticValue, Outputs[0].OutputDestinations, PinFormat.Normalized);
            }
        }

        public override XElement Serialize()
        {
            string operation = Instruction.ToString();

            XElement node = new XElement(operation);

            XElement inputs = new XElement("Inputs");
            node.Add(inputs);
            inputs.Add(new XElement("Variable", new XAttribute("id", VariableId)));
            
            XElement outputs = new XElement("Outputs");
            node.Add(outputs);
            
            if (Outputs[0].OutputDestinations.Count() > 0)
            {
                // We don't support multiple destinations per output at this stage.
                var destination = Outputs[0].OutputDestinations.First();
                outputs.Add(new XElement("Static", new XAttribute("id", destination.StaticInput.ToString())));                    
            }

            return node;
        }

        public override void DebugPrint()
        {
            Console.Write("{1}{0}: ", Instruction, ShouldSerialize ? "" : "(skipping) ");


            Console.WriteLine("V{0} => {1}", VariableId, Outputs[0].OutputDestinations.First().StaticInput.ToString());
        }   
        public int VariableId
        {
            get;
            set;
        }
    }
}
