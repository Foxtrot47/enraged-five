﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;

namespace rage.AMPCompiler
{
    class NoiseGenerator : ModuleInstance
    {
        public NoiseGenerator(XElement element) : base(element)
        {

        }

        public NoiseGenerator()
        {

        }

        public override int NumInputs
        {
            get { return 1; }
        }

        public override int NumOutputs
        {
            get { return 1; }
        }

        public override int NumFields
        {
            get { return 0; }
        }

        public override PinFormat GetInputFormat(int index)
        {
            return PinFormat.Normalized;
        }

        public override PinFormat GetOutputFormat(int index)
        {
            return PinFormat.Signal;
        }

        public override PinState GetOutputState(int index)
        {
            return PinState.Dynamic;
        }

        public override bool ShouldSerializeInput(int index)
        {
            return false;
        }

        public override IntermediateOpcodes Instruction
        {
            get { return IntermediateOpcodes.NOISE; }
        }

        public override void ExpandBuiltInImplementations(SynthGraph graph)
        {
            // Implement gain as a multiplier on the output
            ImplementAs_ScaleOutput(0, graph);
        }
    }
}
