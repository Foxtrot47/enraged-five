﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace rage.AMPCompiler
{
    class BuiltInMixerModule : StandardModuleType
    {
        public BuiltInMixerModule(XElement element)
            : base(element)
        {

        }

        public override void ExpandBuiltInImplementations(SynthGraph graph, ModuleInstance instance)
        {
            List<OutputPinInstance> outputsToSum = new List<OutputPinInstance>();

            for(int i = 0; i < instance.Inputs.Count(); i += 2)
            {
                int signalPinIndex = i;
                int gainPinIndex = i + 1;

                if (instance.Inputs[signalPinIndex].IsConnected ||
                    instance.Inputs[signalPinIndex].StaticValue != 0.0f)
                {

                    if (instance.Inputs[gainPinIndex].IsConnected
                        || instance.Inputs[gainPinIndex].StaticValue != 1.0f)
                    {
                        // gain pin is connected or non-unity; need to scale the input
                        ModuleInstance scaleModule = graph.CreateModuleInstance(ModuleTypeId.SYNTH_BUILTIN_SCALE);

                        scaleModule.Inputs[0].Connect(instance.Inputs[signalPinIndex].OtherPin);
                        instance.Inputs[signalPinIndex].Disconnect();

                        // route gain into scale module
                        if (instance.Inputs[gainPinIndex].IsConnected)
                        {
                            scaleModule.Inputs[1].Connect(instance.Inputs[gainPinIndex].OtherPin);
                            instance.Inputs[gainPinIndex].Disconnect();
                        }
                        else
                        {
                            scaleModule.Inputs[1].StaticValue = instance.Inputs[gainPinIndex].StaticValue;
                        }
                        
                        outputsToSum.Add(scaleModule.Outputs[0]);
                    }
                    else
                    {
                        outputsToSum.Add(instance.Inputs[signalPinIndex].OtherPin);
                        instance.Inputs[signalPinIndex].Disconnect();
                    }
                }
            }

            while(outputsToSum.Count > 1)
            {
                List<OutputPinInstance> cascadedOutputsToSum = new List<OutputPinInstance>();
                
                for(int i = 0; i < outputsToSum.Count / 2; i++)
                {
                    int firstPinIndex = i * 2;
                    ModuleInstance sumModule = graph.CreateModuleInstance(ModuleTypeId.SYNTH_BUILTIN_SUM);

                    sumModule.Inputs[0].Connect(outputsToSum[firstPinIndex]);
                    sumModule.Inputs[1].Connect(outputsToSum[firstPinIndex + 1]);

                    cascadedOutputsToSum.Add(sumModule.Outputs[0]);
                }

                // If we have an odd number of outputs we'll leave one behind - ensure it gets
                // included in the next cascade.
                if(outputsToSum.Count % 2 != 0)
                {
                    cascadedOutputsToSum.Add(outputsToSum.Last());
                }

                outputsToSum = cascadedOutputsToSum;
            }

            foreach(InputPinInstance destination in instance.Outputs[0].OutputDestinations)
            {
                destination.Connect(outputsToSum[0]);
            }
        }
    }
}
