﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace rage.AMPCompiler
{
    class Power : ModuleInstance
    {
        public Power(XElement element)
            : base(element)
        {

        }

        public Power()
        {

        }

        public override int NumInputs
        {
            get { return 2; }
        }

        public override int NumOutputs
        {
            get { return 1; }
        }

        public override int NumFields
        {
            get { return 0; }
        }

        public override PinFormat GetInputFormat(int index)
        {
            return PinFormat.Either;
        }

        public override PinFormat GetOutputFormat(int index)
        {
            if (Inputs[0].IsConnected)
            {
                return Inputs[0].OtherPin.InstanceFormat;
            }
            return PinFormat.Normalized;
        }

        public override PinState GetOutputState(int index)
        {
            PinState state = PinState.Static;
            foreach (InputPinInstance input in m_Inputs)
            {
                if (input.IsConnected)
                {
                    if (input.OtherPin.State == PinState.Dynamic)
                    {
                        state = PinState.Dynamic;
                    }
                }
            }
            return state;
        }

        public override void EvaluateOffline()
        {
            if (!Inputs[0].IsConnected && !Inputs[1].IsConnected)
            {
                float result = (float)Math.Pow(Inputs[0].StaticValue, Inputs[1].StaticValue);
                PushStaticValueToDestinations(result, Outputs[0].OutputDestinations, GetOutputFormat(0));
            }
            else if (Inputs[1].IsConnected == false)
            {
                if (Inputs[1].StaticValue == 0.0f)
                {
                    // x^0 = 1
                    PushStaticValueToDestinations(1.0f, Outputs[0].OutputDestinations, GetOutputFormat(0));
                    Inputs[1].Disconnect();
                }
                else if (Inputs[1].StaticValue == 1.0f)
                {
                    // x^1 = x
                    RewireOutputs(Inputs[0].OtherPin, Outputs[0]);
                    Inputs[1].Disconnect();
                }
            }
        }

        public override void ExpandBuiltInImplementations(SynthGraph graph)
        {
            // For signal input we need to insert an Abs() and multiply the output by Sign()
            if (Inputs[0].IsConnected && Inputs[0].OtherPin.InstanceFormat == PinFormat.Signal)
            {
                var signModule = graph.CreateModuleInstance<Sign>();
                DuplicateInput(signModule.Inputs[0], Inputs[0]);

                var absModule = graph.CreateModuleInstance<Abs>();
                RewireInput(absModule.Inputs[0], Inputs[0]);
                Inputs[0].Connect(absModule.Outputs[0]);

                var multModule = graph.CreateModuleInstance<Multiplier>();
                RewireOutputs(multModule.Outputs[0], Outputs[0]);
                multModule.Inputs[0].Connect(Outputs[0]);
                multModule.Inputs[1].Connect(signModule.Outputs[0]);
            }
        }

        public override IntermediateOpcodes Instruction
        {
            get { return IntermediateOpcodes.POWER; }
        }

    }
}
