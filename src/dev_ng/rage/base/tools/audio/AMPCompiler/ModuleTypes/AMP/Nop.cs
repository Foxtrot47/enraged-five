﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace rage.AMPCompiler
{
    class Nop : ModuleInstance
    {
        public Nop(XElement element)
            : base(element)
        {

        }

        public Nop()
        {

        }

        public override int NumInputs
        {
            get { return 16; }
        }

        public override int NumOutputs
        {
            get { return 0; }
        }

        public override int NumFields
        {
            get { return 0; }
        }

        public override PinFormat GetInputFormat(int index)
        {
            return PinFormat.Either;
        }

        public override PinFormat GetOutputFormat(int index)
        {
            return PinFormat.Normalized;
        }

        public override PinState GetOutputState(int index)
        {
            return PinState.Either;
        }

        public override IntermediateOpcodes Instruction
        {
            get { return IntermediateOpcodes.INVALID; }
        }

        public override bool ShouldRemoveWhenDisconnected()
        {
            return false;
        }

        public override void FinalOptimisation()
        {
            Finished = true;
        }
        bool Finished = false;
        public override bool ShouldSerialize
        {
            get
            {
                return !Finished;
            }
        }
    }
}
