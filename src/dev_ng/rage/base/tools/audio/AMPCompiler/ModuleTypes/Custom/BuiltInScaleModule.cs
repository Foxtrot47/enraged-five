﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace rage.AMPCompiler
{
    class BuiltInScaleModule : StandardModuleType
    {
        
        public BuiltInScaleModule(XElement element) : base(element)
        {
            
        }

        public override void EvaluateOffline(ModuleInstance instance)
        {
            if(instance.Inputs[0].IsConnected == false &&
                instance.Inputs[1].IsConnected == false)
            {
                // We can evaluate this expression offline; all inputs are constant
                float result = instance.Inputs[0].StaticValue * instance.Inputs[1].StaticValue;

                var destinations = instance.Outputs[0].OutputDestinations.ToList();
                foreach(InputPinInstance destination in destinations)
                {
                    destination.Disconnect();
                    destination.StaticValue = result;
                    destination.SpecifiedFormat = instance.Outputs[0].InstanceFormat;
                }
            }
        }
    }
}

