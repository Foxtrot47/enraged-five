﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;

namespace rage.AMPCompiler
{
    class Copy : ModuleInstance
    {
        public Copy(XElement element)
            : base(element)
        {

        }

        public Copy()
        {

        }

        public override int NumInputs
        {
            get { return 1; }
        }

        public override int NumOutputs
        {
            get { return 16; }
        }

        public override int NumFields
        {
            get { return 0; }
        }

        public override PinFormat GetInputFormat(int index)
        {
            return PinFormat.Either;
        }

        public override PinFormat GetOutputFormat(int index)
        {
            if (Inputs[0].IsConnected)
            {
                return Inputs[0].OtherPin.InstanceFormat;
            }
            return Inputs[0].InstanceFormat;
        }

        public override PinState GetOutputState(int index)
        {
            return Inputs[0].State;
        }

        public override IntermediateOpcodes Instruction
        {
            get { return IntermediateOpcodes.COPY; }
        }

        public override void EvaluateOffline()
        {
            /*if (m_Inputs[0].IsConnected == false)
            {
                foreach(OutputPinInstance o in Outputs)
                {
                    PushStaticValueToDestinations(m_Inputs[0].StaticValue, o.OutputDestinations, PinFormat.Normalized);
                }
            }
            else
            {
                // Reshuffle to ensure we don't have any gaps
                for (int i = 0; i < m_Outputs.Count(); i++)
                {
                    if (Outputs[i].OutputDestinations.Count == 0)
                    {
                        for (int j = i; j < Outputs.Count(); j++)
                        {
                            if (Outputs[j].OutputDestinations.Count > 0)
                            {
                                List<InputPinInstance> shufflingDestinations = Outputs[j].OutputDestinations.ToList();
                                foreach (var dest in shufflingDestinations)
                                {
                                    dest.Connect(Outputs[i]);
                                }
                                break;
                            }
                        }
                    }
                }

                if (m_Inputs[0].IsConnected)
                {
                    if (m_Outputs[0].OutputDestinations.Count == 1)
                    {
                        RewireInput(m_Outputs[0].OutputDestinations[0], Inputs[0]);
                    }
                }
            }*/
        }
    }
}
