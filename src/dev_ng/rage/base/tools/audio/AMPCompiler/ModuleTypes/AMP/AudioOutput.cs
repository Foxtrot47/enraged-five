﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;

namespace rage.AMPCompiler
{
    class AudioOutput : ModuleInstance
    {
        public AudioOutput(XElement element)
            : base(element)
        {

        }

        public AudioOutput()
        {

        }

        public override int NumInputs
        {
            get { return 2; }
        }

        public override int NumOutputs
        {
            get { return 0; }
        }

        public override int NumFields
        {
            get { return 0; }
        }

        public override PinFormat GetInputFormat(int index)
        {
            if (index == 0)
            {
                return PinFormat.Signal;
            }
            return PinFormat.Normalized;
        }

        public override PinFormat GetOutputFormat(int index)
        {
            return PinFormat.Signal;
        }

        public override PinState GetOutputState(int index)
        {
            return PinState.Dynamic;
        }

        public override bool ShouldSerializeInput(int index)
        {
            return true;
        }

        public override void ExpandBuiltInImplementations(SynthGraph graph)
        {
            if (Inputs[1].IsConnected && Inputs[1].State == PinState.Dynamic)
            {
                var trigger = graph.CreateModuleInstance<TriggerModule>();
                RewireInput(trigger.Inputs[0], Inputs[1]);
                trigger.Inputs[1].StaticValue = 1.0f;
                Inputs[1].Connect(trigger.Outputs[0]);
                trigger.Mode = TriggerModule.TriggerMode.Latching;
            }
        }

        public override IntermediateOpcodes Instruction
        {
            get { return IntermediateOpcodes.FINISHED_TRIGGER; }
        }

        bool m_SerializeFinishTrigger = true;
        public override void FinalOptimisation()
        {
            m_SerializeFinishTrigger = Inputs[1].IsConnected || Inputs[1].StaticValue >= 1.0f;
        }

        public override bool ShouldSerialize
        {
            get
            {
                return m_SerializeFinishTrigger;
            }
        }

        public override bool IsAudioOutput
        {
            get
            {
                return true;
            }
        }
    }
}
