﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;

namespace rage.AMPCompiler
{
    class ConstantModule : ModuleInstance
    {
        public ConstantModule(XElement element)
            : base(element)
        {

        }

        public ConstantModule()
        {

        }

        public override int NumInputs
        {
            get { return 1; }
        }

        public override int NumOutputs
        {
            get { return 1; }
        }

        public override int NumFields
        {
            get { return 0; }
        }

        public override PinFormat GetInputFormat(int index)
        {
            return PinFormat.Normalized;
        }

        public override PinFormat GetOutputFormat(int index)
        {
            return PinFormat.Normalized;
        }

        public override PinState GetOutputState(int index)
        {
            return PinState.Static;
        }

        public override IntermediateOpcodes Instruction
        {
            get { return IntermediateOpcodes.INVALID; }
        }

        public override bool DebugOnly
        {
            get
            {
                return true;
            }
        }
    }
}
