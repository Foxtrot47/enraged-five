﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;

using rage.ToolLib;

namespace rage.AMPCompiler
{
    public enum ModuleTypeId
    {
        SYNTH_AUDIOOUTPUT = 0,
        SYNTH_ANALOGUE_OSCILLATOR,
        SYNTH_ADDER,
        SYNTH_1POLE,
        SYNTH_BIQUADFILTER,
        SYNTH_CLIPPER,
        SYNTH_CONVERTER,
        SYNTH_DELAYLINE,
        SYNTH_DIVIDER,
        SYNTH_ENVELOPEFOLLOWER,
        SYNTH_ENVELOPEGENERATOR,
        SYNTH_EXPONENTIAL,
        SYNTH_FFT,
        SYNTH_HARDKNEE,
        SYNTH_GATE,
        SYNTH_VARIABLEINPUT,
        SYNTH_INVERTER,
        SYNTH_MIXER,
        SYNTH_SAMPLEPLAYER,
        SYNTH_MULTIPLIER,
        SYNTH_NOISEGENERATOR,
        SYNTH_PINKINGFILTER,
        SYNTH_PWMNOISEGENERATOR,
        SYNTH_RANDOMIZER,
        SYNTH_RECTIFIER,
        SYNTH_RESCALER,
        SYNTH_SAMPLEANDHOLD,
        SYNTH_SUBTRACTER,
        SYNTH_SVFILTER,
        SYNTH_DIGITAL_OSCILLATOR,
        SYNTH_SEQUENCER,
        SYNTH_NOTE2FREQ,
        SYNTH_SWITCH,
        SYNTH_POLYCURVE,
        SYNTH_AWNOISEGENERATOR,
        SYNTH_DECIMATOR,
        SYNTH_REVERSE_SWITCH,
        SYNTH_TRIGGER,
        SYNTH_AUDIOINPUT,
        SYNTH_JUNCTIONPIN,
        SYNTH_ABS,
        SYNTH_FLOOR,
        SYNTH_CEIL,
        SYNTH_ROUND,
        SYNTH_SIGN,
        SYNTH_COUNTER,
        SYNTH_MODULUS,
        SYNTH_FEEDBACKPIN,
        SYNTH_POWER,
        SYNTH_FRAMESMOOTHER,
        SYNTH_MIDIIN,
        SYNTH_MIDICCIN,
        SYNTH_TREMOLO,
        SYNTH_CONSTANT,
        SYNTH_MAX,
        SYNTH_MIN,
        SYNTH_CHARTRECORDER,
        SYNTH_TIMEDTRIGGER,

        SYNTH_FVCOMBFILTER,
        SYNTH_FVALLPASSFILTER,

        SYNTH_MULTITAPDELAY,
        SYNTH_DATTORROREVERB,
        SYNTH_PROGENITORREVERB,
        SYNTH_FREEVERB,
        SYNTH_MVERB,


        SYNTH_SMALLDELAY,
        SYNTH_COMPRESSOR,
        SYNTH_AWFILTER,

        SYNTH_READTIME,

        SYNTH_ALLPASS,
    }


    enum IntermediateOpcodes
    {
        COPY = 0,
        CONVERT_TO_SIGNAL,
        CONVERT_TO_NORMALIZED,
        SAMPLE,
        HOLD,
        MULTIPLY,
        SUM,
        SUBTRACT,
        DIVIDE,
        RESCALE,

        HARDKNEE,
        
        NOISE,
        RANDOM,
        RANDOMIZE_ONINIT,
     
        ABS,
        FLOOR,
        CEIL,
        ROUND,
        SIGN,
        MODULUS,
        POWER,
        MAX,
        
        LERP,

        HARD_CLIP,
        SOFT_CLIP,

        ENVELOPE_FOLLOWER,

        BiquadCoefficients_LowPass2Pole,
        BiquadCoefficients_HighPass2Pole,
        BiquadCoefficients_BandPass,
        BiquadCoefficients_BandStop,
        BiquadCoefficients_LowPass4Pole,
        BiquadCoefficients_HighPass4Pole,
        BiquadCoefficients_PeakingEQ,
        BiquadCoefficients_LowShelf2Pole,
        BiquadCoefficients_LowShelf4Pole,
        BiquadCoefficients_HighShelf2Pole,
        BiquadCoefficients_HighShelf4Pole,

        BiquadProcess_2Pole,
        BiquadProcess_4Pole,

        AW_FILTER,

        OnePole_LPF,
        OnePole_HPF,

        OSC_RAMP,
        SINE,
        COS,
        TRI,
        SQUARE, 
        SAW,

        TRIGGER_LATCH,
        TRIGGER_DIFF,

        ENV_GEN_LIN_INT,
        ENV_GEN_LIN_OS,
        ENV_GEN_LIN_RET,
        ENV_GEN_EXP_INT,
        ENV_GEN_EXP_OS,
        ENV_GEN_EXP_RET,

        TIMED_TRIGGER_INT,
        TIMED_TRIGGER_OS,
        TIMED_TRIGGER_RET,

        SWITCH_INTERP,
        SWITCH,

        READ_VARIABLE,
        FINISHED_TRIGGER,

        READ_INPUT_0,
        READ_INPUT_1,
        READ_INPUT_2,
        READ_INPUT_3,
        READ_INPUT_4,
        READ_INPUT_5,
        READ_INPUT_6,
        READ_INPUT_7,

        NOTE_TO_FREQ,
        SAH_STATIC_OUTPUT,

        DECIMATE,
        COUNTER,
        COUNTER_TRIGGER,

        GATE,

        SMALLDELAY_FRAC,
        SMALLDELAY_STATIC,
        SMALLDELAY_FRAC_FB,        
        SMALLDELAY_STATIC_FB,

        COMPRESSOR_EG,

        Switch_Norm,
        Switch_Index,
        Switch_Lerp,
        Switch_EqualPower,

        Allpass,

        NUM_INTERMEDIATE_OPCODES,
        INVALID
    }

    enum SynthOpcodeIds
    {
        COPY_BUFFER = 0,
        COPY_SCALAR,

        CONVERT_BUFFER_TO_SIGNAL,
        CONVERT_SCALAR_TO_SIGNAL,
        CONVERT_BUFFER_TO_NORMALIZED,
        CONVERT_SCALAR_TO_NORMALIZED,

        SAMPLE_BUFFER,

        MULTIPLY_BUFFER_BUFFER,
        MULTIPLY_BUFFER_SCALAR,
        MULTIPLY_SCALAR_SCALAR,

        SUM_BUFFER_BUFFER,
        SUM_BUFFER_SCALAR,
        SUM_SCALAR_SCALAR,

        SUBTRACT_BUFFER_BUFFER,
        SUBTRACT_BUFFER_SCALAR,
        SUBTRACT_SCALAR_BUFFER,
        SUBTRACT_SCALAR_SCALAR,

        DIVIDE_BUFFER_BUFFER,
        DIVIDE_BUFFER_SCALAR,
        DIVIDE_SCALAR_SCALAR,

        RESCALE_BUFFER_BUFFER,
        RESCALE_BUFFER_SCALAR,
        RESCALE_SCALAR,

        HARDKNEE_BUFFER,
        HARDKNEE_SCALAR,

        NOISE,
        RANDOM,

        ABS_BUFFER,
        ABS_SCALAR,
        FLOOR_BUFFER,
        FLOOR_SCALAR,
        CEIL_BUFFER,
        CEIL_SCALAR,
        ROUND_BUFFER,
        ROUND_SCALAR,
        SIGN_BUFFER,
        SIGN_SCALAR,
        MODULUS_BUFFER,
        MODULUS_SCALAR,
        POWER_SCALAR,
        POWER_BUFFER,
        MAX_SCALAR,
        MAX_BUFFER,
        COMPRESSOR_EG,
        UNUSED_1,

        LERP_BUFFER,
        LERP_BUFFER_BUFFER,
        LERP_SCALAR,

        HARD_CLIP_BUFFER_BUFFER,
        HARD_CLIP_BUFFER_SCALAR,
        HARD_CLIP_SCALAR_SCALAR,
        SOFT_CLIP_BUFFER_BUFFER,
        SOFT_CLIP_BUFFER_SCALAR,
        SOFT_CLIP_SCALAR_SCALAR,

        ENVELOPE_FOLLOWER_BUFFER,
        ENVELOPE_FOLLOWER_SCALAR,

        BiquadCoefficients_LowPass2Pole,
        BiquadCoefficients_HighPass2Pole,
        BiquadCoefficients_BandPass,
        BiquadCoefficients_BandStop,
        BiquadCoefficients_LowPass4Pole,
        BiquadCoefficients_HighPass4Pole,
        BiquadCoefficients_PeakingEQ,

        BiquadProcess_2Pole,
        BiquadProcess_4Pole,

        OnePole_LPF_BUFFER_BUFFER,
        OnePole_LPF_BUFFER_SCALAR,
        OnePole_LPF_SCALAR,
        OnePole_HPF_BUFFER_BUFFER,
        OnePole_HPF_BUFFER_SCALAR,
        OnePole_HPF_SCALAR,

        OSC_RAMP_BUFFER_BUFFER,
        OSC_RAMP_BUFFER_SCALAR,
        OSC_RAMP_SCALAR,

        SINE_BUFFER,
        SINE_SCALAR,
        COS_BUFFER,
        COS_SCALAR,
        TRI_BUFFER,
        TRI_SCALAR,
        SQUARE_BUFFER,
        SQUARE_SCALAR, 
        SAW_BUFFER,
        SAW_SCALAR,
       
        TRIGGER_LATCH,


        ENV_GEN_LIN_INT_BUFFER,
		ENV_GEN_LIN_OS_BUFFER,
		ENV_GEN_LIN_RET_BUFFER,
		ENV_GEN_EXP_INT_BUFFER,
		ENV_GEN_EXP_OS_BUFFER,
		ENV_GEN_EXP_RET_BUFFER,

        TIMED_TRIGGER_INT,
        TIMED_TRIGGER_OS,
        TIMED_TRIGGER_RET,

        READ_VARIABLE,
        FINISHED_TRIGGER,

        READ_INPUT_0,
        READ_INPUT_1,
        READ_INPUT_2,
        READ_INPUT_3,
        READ_INPUT_4,
        READ_INPUT_5,
        READ_INPUT_6,
        READ_INPUT_7,

        NOTE_TO_FREQ_SCALAR,
        NOTE_TO_FREQ_BUFFER,

        SAH_STATIC_SCALAR,

        DECIMATE,
        COUNTER,
        COUNTER_TRIGGER,

        GATE_BUFFER_BUFFER,
        GATE_BUFFER_SCALAR,
        GATE_SCALAR_SCALAR,

        SMALLDELAY_FRAC,
        SMALLDELAY_STATIC,
        SMALLDELAY_FRAC_FB,
        SMALLDELAY_STATIC_FB,

        TRIGGER_DIFF,
        RANDOMIZE_ONINIT,
        HOLD_SAMPLE,
        AW_FILTER,

        LERP_THREE_BUFFERS,

        BiquadCoefficients_LowShelf2Pole,
        BiquadCoefficients_LowShelf4Pole,
        BiquadCoefficients_HighShelf2Pole,
        BiquadCoefficients_HighShelf4Pole,

        Switch_Norm_Buffer,
        Switch_Index_Buffer,
        Switch_Lerp_Buffer,
        Switch_EqualPower_Buffer,
        Switch_Norm_Scalar,
        Switch_Index_Scalar,
        Switch_Lerp_Scalar,
        Switch_EqualPower_Scalar,

        Allpass_Static,
        Allpass_Buffer,

        NUM_OP_CODES,

        END = 0xff
    }

    public enum PinFormat
    {
        Signal,
        Normalized,
        Either
    }

    public enum PinState
    {
        Static,
        Dynamic,
        Either
    }
}
