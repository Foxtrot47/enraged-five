﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace rage.AMPCompiler
{
    class Rescaler : ModuleInstance
    {
        public Rescaler(XElement element)
            : base(element)
        {

        }

        public Rescaler()
        {
            
        }

        public enum InputIds
        {
            MinInput = 0,
            MaxInput,
            Input,
            MinOutput,
            MaxOutput,
            NumInputs
        }
        static int InputIndex(InputIds input)
        {
            return (int)input;
        }

        public override int NumInputs
        {
            get { return InputIndex(InputIds.NumInputs); }
        }

        public override int NumOutputs
        {
            get { return 1; }
        }

        public override int NumFields
        {
            get { return 1; }
        }

        public override PinFormat GetInputFormat(int index)
        {
            return PinFormat.Normalized;
        }
        
        public override PinState GetInputState(int index)
        {
            // Force all of our inputs to match 'Input' if static
            if (Inputs[InputIndex(InputIds.Input)].State == PinState.Static)
                return PinState.Static;
            return PinState.Either;
        }

        public override PinFormat GetOutputFormat(int index)
        {
            if (Inputs[InputIndex(InputIds.Input)].IsConnected)
            {
                return Inputs[InputIndex(InputIds.Input)].OtherPin.InstanceFormat;
            }
            return PinFormat.Normalized;
        }

        public override PinState GetOutputState(int index)
        {
            if (Inputs[InputIndex(InputIds.Input)].IsConnected)
            {
                return Inputs[InputIndex(InputIds.Input)].OtherPin.State;
            }
            return PinState.Static;
        }

        public override IntermediateOpcodes Instruction
        {
            get { return IntermediateOpcodes.RESCALE; }
        }

        public override void ExpandBuiltInImplementations(SynthGraph graph)
        {
            if (Inputs[InputIndex(InputIds.MinInput)].IsConnected == false &&
                Inputs[InputIndex(InputIds.MaxInput)].IsConnected == false)
            {
                if (Inputs[InputIndex(InputIds.MinInput)].StaticValue == 0.0f &&
                    Inputs[InputIndex(InputIds.MaxInput)].StaticValue == 1.0f)
                {
                    // Implement as a Lerp
                    var lerpModule = graph.CreateModuleInstance<Lerp>();
                    RewireInput(lerpModule.Inputs[0], Inputs[InputIndex(InputIds.Input)]);
                    RewireInput(lerpModule.Inputs[1], Inputs[InputIndex(InputIds.MinOutput)]);
                    RewireInput(lerpModule.Inputs[2], Inputs[InputIndex(InputIds.MaxOutput)]);
                    RewireOutputs(lerpModule.Outputs[0], Outputs[0]);
                }
            }
        }

        public override void EvaluateOffline()
        {

            if (Inputs[0].IsConnected == false && Inputs[1].IsConnected == false &&
                Inputs[3].IsConnected == false && Inputs[4].IsConnected == false &&
                Inputs[2].IsConnected == true)
            {
                if (Inputs[0].StaticValue == Inputs[3].StaticValue &&
                    Inputs[1].StaticValue == Inputs[4].StaticValue)
                {
                    // Input range matches ouput range; no-op
                    RewireOutputs(Inputs[2].OtherPin, Outputs[0]);
                }
            }

            foreach (InputPinInstance input in Inputs)
            {
                if (input.IsConnected)
                {
                    return;
                }
            }
            
            // We can evaluate this expression offline; all inputs are constant

            float minInput = Inputs[InputIndex(InputIds.MinInput)].StaticValue;
            float maxInput = Inputs[InputIndex(InputIds.MaxInput)].StaticValue;
            float inputVal = Inputs[InputIndex(InputIds.Input)].StaticValue;
            float minOutput = Inputs[InputIndex(InputIds.MinOutput)].StaticValue;
            float maxOutput = Inputs[InputIndex(InputIds.MaxOutput)].StaticValue;

            float normalizedInput = (inputVal - minInput) / (maxInput - minInput);
            float result = minOutput + normalizedInput * (maxOutput - minOutput);
            
            PushStaticValueToDestinations(result, Outputs[0].OutputDestinations, GetOutputFormat(0));
        }
        
    }
}


