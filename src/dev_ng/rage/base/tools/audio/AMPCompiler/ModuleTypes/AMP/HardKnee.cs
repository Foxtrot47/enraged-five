﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;

namespace rage.AMPCompiler
{
    class HardKnee : ModuleInstance
    {
        public HardKnee(XElement element)
            : base(element)
        {

        }

        public HardKnee()
        {

        }

        public override int NumInputs
        {
            get { return 2; }
        }

        public override int NumOutputs
        {
            get { return 1; }
        }

        public override int NumFields
        {
            get { return 0; }
        }

        public override PinFormat GetInputFormat(int index)
        {
            return PinFormat.Normalized;
        }

        public override PinFormat GetOutputFormat(int index)
        {
            return PinFormat.Normalized;
        }

        public override PinState GetOutputState(int index)
        {
            return Inputs[0].State;
        }

        public override IntermediateOpcodes Instruction
        {
            get
            {
                return IntermediateOpcodes.HARDKNEE;
            }
        }

    }
}
