﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;

namespace rage.AMPCompiler
{
    class Decimator : ModuleInstance
    {
        public Decimator(XElement element)
            : base(element)
        {

        }

        public Decimator()
        {

        }

        public override int NumInputs
        {
            get { return 3; }
        }

        public override int NumOutputs
        {
            get { return 1; }
        }

        public override int NumFields
        {
            get { return 0; }
        }

        public override PinFormat GetInputFormat(int index)
        {
            if (index == 0)
                return PinFormat.Signal;
            return PinFormat.Normalized;
        }

        public override PinFormat GetOutputFormat(int index)
        {
            return PinFormat.Signal;
        }

        public override PinState GetOutputState(int index)
        {
            return PinState.Dynamic;
        }

        public override PinState GetInputState(int index)
        {
            if (index == 0)
                return PinState.Dynamic;
            return PinState.Static;
        }

        public override IntermediateOpcodes Instruction
        {
            get
            {
                return IntermediateOpcodes.DECIMATE;
            }
        }

    }
}
