﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace rage.AMPCompiler
{
    class Adder : ModuleInstance
    {
        public Adder(XElement element)
            : base(element)
        {

        }

        public Adder()
        {

        }

        public override int NumInputs
        {
            get { return 2; }
        }

        public override int NumOutputs
        {
            get { return 1; }
        }

        public override int NumFields
        {
            get { return 0; }
        }

        public override PinFormat GetInputFormat(int index)
        {
            return PinFormat.Either;
        }

        public override PinFormat GetOutputFormat(int index)
        {
            PinFormat format = PinFormat.Normalized;
            foreach (InputPinInstance input in m_Inputs)
            {
                if (input.IsConnected)
                {
                    if (input.OtherPin.InstanceFormat == PinFormat.Signal)
                    {
                        format = PinFormat.Signal;
                    }
                }
            }
            return format;
        }

        public override PinState GetOutputState(int index)
        {
            PinState state = PinState.Static;
            foreach (InputPinInstance input in m_Inputs)
            {
                if (input.IsConnected)
                {
                    if (input.OtherPin.State == PinState.Dynamic)
                    {
                        state = PinState.Dynamic;
                    }
                }
            }
            return state;
        }

        public override void EvaluateOffline()
        {
            if (Inputs[0].IsConnected == false && Inputs[1].IsConnected == false)
            {
                float result = Inputs[0].StaticValue + Inputs[1].StaticValue;
                PushStaticValueToDestinations(result, Outputs[0].OutputDestinations, GetOutputFormat(0));
            }
            else if (Inputs[0].IsConnected == false || Inputs[1].IsConnected == false)
            {
                // Adding 0 to something is a no-op, so rewire to pass input straight through
                int staticIndex = Inputs[0].IsConnected ? 1 : 0;
                int connectedIndex = Inputs[0].IsConnected ? 0 : 1;
                if (Inputs[staticIndex].StaticValue == 0.0f)
                {
                    RewireOutputs(Inputs[connectedIndex].OtherPin, Outputs[0]);
                    Inputs[connectedIndex].Disconnect();
                }
            }
        }

        public override IntermediateOpcodes Instruction
        {
            get { return IntermediateOpcodes.SUM; }
        }

    }
}
