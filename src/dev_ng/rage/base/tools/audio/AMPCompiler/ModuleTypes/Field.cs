﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

using rage.ToolLib;

namespace rage.AMPCompiler
{
    class Semantic
    {
        public enum ParamType
        {
            Scale_Min,
            Scale_Max,
            Implementation_Map,
        };
        public readonly int InputIndex;
        public readonly ParamType Param;
        
        public readonly string[] ImplementationMap;

        public Semantic(XElement element)
        {
            XAttribute inputPin = element.Attribute("inputPin");
            if (inputPin != null)
            {
                InputIndex = int.Parse(inputPin.Value);
            }
            else
            {
                InputIndex = -1;
            }
            Param = Enum<ParamType>.Parse(element.Attribute("param").Value);

            ImplementationMap = (from imp in element.Elements("Implementation")
                                 select imp.Value).ToArray();
        }
    }

    class Field
    {
        public readonly Semantic Semantic;
        public readonly string Name;
        public readonly int Index;

        public Field(XElement element, int index)
        {
            Index = index;
            if(element.Attribute("name") != null)
            {
                Name = element.Attribute("name").Value;
            }
            XElement semanticElement = element.Element("Semantic");
            if(semanticElement != null)
            {
                Semantic = new Semantic(semanticElement);
            }
        }
    }
}
