﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace rage.AMPCompiler
{
    class TimedTrigger : ModuleInstance
    {
        public TimedTrigger(XElement element)
            : base(element)
        {

        }

        public TimedTrigger()
        {

        }

        enum TimedTriggerInputId
        {
            kTimedTriggerInputTrigger = 0,
            kTimedTriggerPredelay,
            kTimedTriggerTime1,
            kTimedTriggerTime2,
            kTimedTriggerTime3,
            kTimedTriggerTime4,


            kTimedTriggerNumInputs
        }
        int InputIndex(TimedTriggerInputId inputId)
        {
            return (int)inputId;
        }

        public override int NumInputs
        {
            get { return InputIndex(TimedTriggerInputId.kTimedTriggerNumInputs); }
        }
                

        public override int NumOutputs
        {
            get { return 5; }
        }

        public override int NumFields
        {
            get { return 1; }
        }

        public override PinState GetInputState(int index)
        {
            return PinState.Static;
        }

        public override PinFormat GetInputFormat(int index)
        {
            return PinFormat.Normalized;
        }

        public override PinFormat GetOutputFormat(int index)
        {
            return PinFormat.Normalized;
        }

        public override PinState GetOutputState(int index)
        {
            return PinState.Static;
        }

        public enum TriggerMode
        {
            OneShot = 0,
            Retrigger,
            Interruptible,
        }

        public override void ExpandBuiltInImplementations(SynthGraph graph)
        {
            int count = 0;
            ModuleInstance nop = null;
            foreach(OutputPinInstance output in Outputs)
            {
                if(output.OutputDestinations.Count == 0)
                {
                    if (nop == null)
                    {
                        nop = graph.CreateModuleInstance<Nop>();
                    }
                    nop.Inputs[count++].Connect(output);
                }
            }
        }

        public TriggerMode Mode
        {
            get
            {
                return (TriggerMode)m_FieldValues[0];
            }
            set
            {
                m_FieldValues[0] = (float)value;
            }
        }

        public override IntermediateOpcodes Instruction
        {
            get 
            { 
                switch(Mode)
                {
                    case TriggerMode.Interruptible:
                        return IntermediateOpcodes.TIMED_TRIGGER_INT;
                    case TriggerMode.Retrigger:
                        return IntermediateOpcodes.TIMED_TRIGGER_RET;
                    case TriggerMode.OneShot:
                        return IntermediateOpcodes.TIMED_TRIGGER_OS;
                }
                return IntermediateOpcodes.INVALID;                
            }
        }
    }
}


