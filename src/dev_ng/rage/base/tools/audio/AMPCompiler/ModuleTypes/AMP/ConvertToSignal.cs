﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;

namespace rage.AMPCompiler
{
    class ConvertToSignal : ModuleInstance
    {
        public ConvertToSignal(XElement element)
            : base(element)
        {

        }

        public ConvertToSignal()
        {

        }

        public override int NumInputs
        {
            get { return 1; }
        }

        public override int NumOutputs
        {
            get { return 1; }
        }

        public override int NumFields
        {
            get { return 0; }
        }

        public override PinFormat GetInputFormat(int index)
        {
            return PinFormat.Either;
        }

        public override PinFormat GetOutputFormat(int index)
        {
            return PinFormat.Signal;
        }

        public override PinState GetOutputState(int index)
        {
            return Inputs[0].State;
        }

        public override IntermediateOpcodes Instruction
        {
            get { return IntermediateOpcodes.CONVERT_TO_SIGNAL; }
        }

        public override void ExpandBuiltInImplementations(SynthGraph graph)
        {
            if (Inputs[0].IsConnected && Inputs[0].OtherPin.ParentModule.GetType() == typeof(ConvertToNormalized))
            {
                ConvertToNormalized otherModule = Inputs[0].OtherPin.ParentModule as ConvertToNormalized;
                RewireOutputs(otherModule.Inputs[0].OtherPin, Outputs[0]);
            }
        }

        public override void EvaluateOffline()
        {
            if (!Inputs[0].IsConnected)
            {
                float result = (Inputs[0].StaticValue * 2.0f) - 1.0f;
                PushStaticValueToDestinations(result, Outputs[0].OutputDestinations, PinFormat.Signal);
            }
        }
    }
}
