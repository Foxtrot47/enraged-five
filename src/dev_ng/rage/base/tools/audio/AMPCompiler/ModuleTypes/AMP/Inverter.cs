﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;

namespace rage.AMPCompiler
{
    class Inverter : ModuleInstance
    {
        public Inverter(XElement element)
            : base(element)
        {

        }

        public Inverter()
        {

        }

        public override int NumInputs
        {
            get { return 1; }
        }

        public override int NumOutputs
        {
            get { return 1; }
        }

        public override int NumFields
        {
            get { return 0; }
        }

        public override PinFormat GetInputFormat(int index)
        {
            return PinFormat.Either;
        }

        public override PinFormat GetOutputFormat(int index)
        {
            if (Inputs[0].IsConnected)
            {
                return Inputs[0].OtherPin.InstanceFormat;
            }
            return PinFormat.Normalized;
        }

        public override PinState GetOutputState(int index)
        {
            return Inputs[0].State;
        }

        public override IntermediateOpcodes Instruction
        {
            get 
            {   
                return IntermediateOpcodes.INVALID;
            }
        }

        public override void ExpandBuiltInImplementations(SynthGraph graph)
        {
            ModuleInstance module;
            if (GetOutputFormat(0) == PinFormat.Normalized)
            {
                // output = 1.f - input
                module = graph.CreateModuleInstance<Subtract>();
                module.Inputs[0].StaticValue = 1.0f;
            }
            else
            {
                // output = -1 * input
                module = graph.CreateModuleInstance<Multiplier>();
                module.Inputs[0].StaticValue = -1.0f;
            }
            RewireInput(module.Inputs[1], Inputs[0]);
            RewireOutputs(module.Outputs[0], Outputs[0]);
        }
    }
}
