﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;

namespace rage.AMPCompiler
{
    class Junction : ModuleInstance
    {
        public Junction(XElement element)
            : base(element)
        {

        }

        public Junction()
        {

        }

        public override int NumInputs
        {
            get { return 1; }
        }

        public override int NumOutputs
        {
            get { return 1; }
        }

        public override int NumFields
        {
            get { return 0; }
        }

        public override PinFormat GetInputFormat(int index)
        {
            if (Inputs[0].IsConnected)
            {
                return Inputs[0].Format;
            }
            return PinFormat.Normalized;
        }

        public override PinFormat GetOutputFormat(int index)
        {
            return GetInputFormat(0);
        }

        public override PinState GetOutputState(int index)
        {
            if (Inputs[0].IsConnected)
            {
                return Inputs[0].State;
            }
            return PinState.Static;
        }

        public override IntermediateOpcodes Instruction
        {
            get { return IntermediateOpcodes.INVALID; }
        }

        public override bool DebugOnly
        {
            get
            {
                return true;
            }
        }
    }
}
