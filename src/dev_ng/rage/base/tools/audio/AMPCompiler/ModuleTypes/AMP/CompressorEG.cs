﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;

namespace rage.AMPCompiler
{
    class CompressorEG : ModuleInstance
    {
        public CompressorEG(XElement element)
            : base(element)
        {

        }

        public CompressorEG()
        {

        }
       
        public override int NumInputs
        {
            get { return 5; }
        }

        public override int NumOutputs
        {
            get { return 1; }
        }

        public override int NumFields
        {
            get { return 0; }
        }
       
        public override PinFormat GetInputFormat(int index)
        {
            if (index == 0)
                return PinFormat.Signal;
            return PinFormat.Normalized;
        }

        public override PinFormat GetOutputFormat(int index)
        {
            return PinFormat.Normalized;
        }

        public override PinState GetInputState(int index)
        {
            if (index == 0)
                return PinState.Dynamic;
            // For now only support static inputs
            return PinState.Static;
        }
            
        public override PinState GetOutputState(int index)
        {
            return PinState.Dynamic;
        }

        public override IntermediateOpcodes Instruction
        {
            get
            {
                return IntermediateOpcodes.COMPRESSOR_EG;
            }
        }
    }
}
