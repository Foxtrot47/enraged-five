﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace rage.AMPCompiler
{
    class SmallDelay : ModuleInstance
    {
        public SmallDelay(XElement element)
            : base(element)
        {

        }

        public SmallDelay()
        {

        }

        public override int NumInputs
        {
            get { return 3; }
        }

        public override int NumOutputs
        {
            get { return 1; }
        }

        public override int NumFields
        {
            get { return 0; }
        }

        public override PinFormat GetInputFormat(int index)
        {
            if (index == 0)
                return PinFormat.Either;
            return PinFormat.Normalized;
        }

        public override PinFormat GetOutputFormat(int index)
        {
            if (index == 0)
            {
                if (Inputs[0].IsConnected)
                    return Inputs[0].OtherPin.InstanceFormat;
            }
            return PinFormat.Signal;
        }

        public override PinState GetOutputState(int index)
        {
            return PinState.Dynamic;
        }

        public override PinState GetInputState(int index)
        {
            if (index == 0)
            {
                return PinState.Dynamic;
            }
            else
            {
                return PinState.Static;
            }
        }

        enum InterpMode
        {
            Fractional = 0,
            NonInterpolating,
        }

        InterpMode Mode
        {
            get
            {
                if (FieldValues == null || FieldValues.Count() < 1 || FieldValues[0] == 0.0f)
                {
                    return InterpMode.Fractional;
                }
                return InterpMode.NonInterpolating;
            }
        }


        public override IntermediateOpcodes Instruction
        {
            get 
            {
                // Check feedback pin; if 0 and static then we can use the cheaper version
                if (Inputs.Count() < 3 || (!Inputs[2].IsConnected && Inputs[2].StaticValue == 0.0f))
                {
                    // No feedback required
                    if (Mode == InterpMode.Fractional)
                    {
                        return IntermediateOpcodes.SMALLDELAY_FRAC;
                    }
                    return IntermediateOpcodes.SMALLDELAY_STATIC;
                }
                else
                {
                    if (Mode == InterpMode.Fractional)
                        return IntermediateOpcodes.SMALLDELAY_FRAC_FB;
                    return IntermediateOpcodes.SMALLDELAY_STATIC_FB;
                }
            }
        }
    }
}
