﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace rage.AMPCompiler
{
    class GateModule : ModuleInstance
    {
        public GateModule(XElement element)
            : base(element)
        {

        }

        public GateModule()
        {

        }

        public override int NumInputs
        {
            get { return 2; }
        }

        public override int NumOutputs
        {
            get { return 2; }
        }

        public override int NumFields
        {
            get { return 0; }
        }

        public override PinFormat GetInputFormat(int index)
        {
            if (index == 0)
                return PinFormat.Either;
            return PinFormat.Normalized;
        }

        public override PinFormat GetOutputFormat(int index)
        {
            if (index == 0)
            {
                if (Inputs[0].IsConnected)
                    return Inputs[0].OtherPin.InstanceFormat;
            }
            return PinFormat.Normalized;
        }

        public override PinState GetOutputState(int index)
        {
            if (Inputs[0].IsConnected)
            {
                return Inputs[0].OtherPin.State;
            }
            return PinState.Static;
        }

        public override void ExpandBuiltInImplementations(SynthGraph graph)
        {
            // For signal input we need to insert an Abs()
            if (Inputs[0].IsConnected && Inputs[0].OtherPin.InstanceFormat == PinFormat.Signal)
            {
                var absModule = graph.CreateModuleInstance<Abs>();
                RewireInput(absModule.Inputs[0], Inputs[0]);
                Inputs[0].Connect(absModule.Outputs[0]);
            }

            var module = graph.CreateModuleInstance<Gate>();
            for (int i = 0; i < Inputs.Count(); i++)
            {
                RewireInput(module.Inputs[i], Inputs[i]);
            }
            // Hook up gate value output
            RewireOutputs(module.Outputs[0], Outputs[1]);

            if (Outputs[0].OutputDestinations.Count > 0)
            {
                // Implement gated signal output via multiply
                var multiplier = graph.CreateModuleInstance<Multiplier>();
                RewireOutputs(multiplier.Outputs[0], Outputs[0]);
                multiplier.Inputs[0].Connect(module.Outputs[0]);
                if (module.Inputs[0].IsConnected)
                {
                    multiplier.Inputs[1].Connect(module.Inputs[0].OtherPin);
                }
                else
                {
                    multiplier.Inputs[1].StaticValue = module.Inputs[0].StaticValue;
                }
            }
        }

        public override IntermediateOpcodes Instruction
        {
            get { return IntermediateOpcodes.INVALID; }
        }
    }
}
