﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;

namespace rage.AMPCompiler
{
    class DigitalOscillator : ModuleInstance
    {
        public DigitalOscillator(XElement element)
            : base(element)
        {

        }

        public DigitalOscillator()
        {

        }

        enum InputIds
        {
            Amplitude = 0,
            Frequency,
            Phase,
            HardSync,
            NumInputs
        }

        static int InputIndex(InputIds id)
        {
            return (int)id;
        }

        enum Fields
        {
            MinFrequency = 0,
            MaxFrequency,
            Mode,
            StaticProcessing,
            NumFields
        }
        static int FieldIndex(Fields id) { return (int)id; }

        public override int NumInputs
        {
            get { return InputIndex(InputIds.NumInputs); }
        }

        public override int NumOutputs
        {
            get { return 1; }
        }

        public override int NumFields
        {
            get { return FieldIndex(Fields.NumFields); }
        }

        public override PinFormat GetInputFormat(int index)
        {
            return PinFormat.Normalized;
        }

        public override PinFormat GetOutputFormat(int index)
        {
            return PinFormat.Signal;
        }

        public override PinState GetOutputState(int index)
        {
            return IsStaticMode ? PinState.Static : PinState.Dynamic;
        }

        public bool IsStaticMode
        {
            get
            {
                return m_FieldValues[FieldIndex(Fields.StaticProcessing)] == 1.0f;
            }
        }

        public Waveforms Waveform
        {
            get
            {
                return (Waveforms)m_FieldValues[FieldIndex(Fields.Mode)];
            }
        }

        public enum Waveforms
        {
            Square = 0, 
            Triangle,
            Saw,
            Inverted_Saw,
            Sine,
            Cosine,
            NumWaveforms
        }

        public override IntermediateOpcodes Instruction
        {
            get { return IntermediateOpcodes.INVALID; }
        }

        public override void ExpandBuiltInImplementations(SynthGraph graph)
        {
            ImplementAs_LerpInput(InputIndex(InputIds.Frequency), graph, m_FieldValues[FieldIndex(Fields.MinFrequency)], m_FieldValues[FieldIndex(Fields.MaxFrequency)]);
            ImplementAs_ScaleOutput(InputIndex(InputIds.Amplitude), graph);

            // TODO: IsStaticMode with a dynamic phase input
            OutputPinInstance phasePin = null;
            if (Inputs[InputIndex(InputIds.Phase)].IsConnected)
            {
                phasePin = Inputs[InputIndex(InputIds.Phase)].OtherPin;                
            }
            else
            {
                // Create a ramp generator
                var phaseGenerator = graph.CreateModuleInstance<RampGenerator>();
                phasePin = phaseGenerator.Outputs[0];
                phaseGenerator.IsStaticMode = IsStaticMode;
                // hook up frequency
                RewireInput(phaseGenerator.Inputs[0], Inputs[InputIndex(InputIds.Frequency)]);
            }

            Waveforms mode = Waveform;
            if (mode == Waveforms.Inverted_Saw)
            {
                // Use the ramp directly, converted to signal
                var converter = graph.CreateModuleInstance<ConvertToSignal>();
                converter.Inputs[0].Connect(phasePin);
                RewireOutputs(converter.Outputs[0], Outputs[0]);
            }
            else
            {
                var transformModule = graph.CreateModuleInstance<OscTransform>();
                transformModule.Inputs[0].Connect(phasePin);
                transformModule.Waveform = mode;
                RewireOutputs(transformModule.Outputs[0], Outputs[0]);
            }
            
        }
    }
}
