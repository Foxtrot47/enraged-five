﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace rage.AMPCompiler
{
    class BiquadProcess : ModuleInstance
    {
        public BiquadProcess(XElement element)
            : base(element)
        {

        }

        public BiquadProcess()
        {

        }

        enum InputIds
        {
            Signal = 0,
            a0,
            a1,
            a2,
            b1,
            b2,
            NumInputs
        }

        static int InputIndex(InputIds input)
        {
            return (int)input;
        }


        public override int NumInputs
        {
            get { return (int)InputIds.NumInputs; }
        }

        public override int NumOutputs
        {
            get { return 1; }
        }

        public override int NumFields
        {
            get { return 0; }
        }

        public override PinFormat GetInputFormat(int index)
        {
            if (index == InputIndex(InputIds.Signal))
            {
                return PinFormat.Signal;
            }
            // Coefficients are 'normalized' format
            return PinFormat.Normalized;
        }

        public override PinFormat GetOutputFormat(int index)
        {
            return PinFormat.Signal;
        }

        public override PinState GetOutputState(int index)
        {
            return PinState.Dynamic;
        }

        public override IntermediateOpcodes Instruction
        {
            get { return Is4Pole ? IntermediateOpcodes.BiquadProcess_4Pole : IntermediateOpcodes.BiquadProcess_2Pole; }
        }

        public bool Is4Pole { get; set; }

    }
}
