﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace rage.AMPCompiler
{
    class Lerp : ModuleInstance
    {
        public Lerp(XElement element)
            : base(element)
        {

        }

        public Lerp()
        {

        }
        
        public override int NumInputs
        {
            get { return 3; }
        }

        public override int NumOutputs
        {
            get { return 1; }
        }

        public override int NumFields
        {
            get { return 0; }
        }

        public override PinFormat GetInputFormat(int index)
        {
            return PinFormat.Normalized;
        }

        public override PinFormat GetOutputFormat(int index)
        {
            return GetInputFormat(0);
        }

        public override PinState GetOutputState(int index)
        {
            PinState state = PinState.Static;
            foreach (InputPinInstance input in m_Inputs)
            {
                if (input.IsConnected)
                {
                    if (input.OtherPin.State == PinState.Dynamic)
                    {
                        state = PinState.Dynamic;
                    }
                }
            }
            return state;
        }

        public override IntermediateOpcodes Instruction
        {
            get { return IntermediateOpcodes.LERP; }
        }

        public override void ExpandBuiltInImplementations(SynthGraph graph)
        {
            if (Inputs[0].IsConnected && Inputs[1].IsConnected == false && Inputs[2].IsConnected == false)
            {
                if (Inputs[1].StaticValue == 0.0f && Inputs[2].StaticValue == 1.0f)
                {
                    // Lerp(t,0,1) is a no-op
                    RewireOutputs(Inputs[0].OtherPin, Outputs[0]);
                }
            }
            else if (Inputs[1].IsConnected == false && Inputs[1].StaticValue == 0.0f)
            {
                // Lerp with min == 0 is a multiply
                var multiply = graph.CreateModuleInstance<Multiplier>();
                RewireInput(multiply.Inputs[0], Inputs[0]);
                RewireInput(multiply.Inputs[1], Inputs[2]);
                RewireOutputs(multiply.Outputs[0], Outputs[0]);
            }
            else if (Inputs[2].IsConnected == false && Inputs[2].StaticValue == 1.0f)
            {
                var t = Inputs[0];
                var min = Inputs[1];
                // t(max - min) + min, when max = 1
                // = t - t*min + min
                var multiply = graph.CreateModuleInstance<Multiplier>();

                DuplicateInput(multiply.Inputs[0], t);
                DuplicateInput(multiply.Inputs[1], min);

                // t - multiply
                var subtract = graph.CreateModuleInstance<Subtract>();
                RewireInput(subtract.Inputs[0], t);
                subtract.Inputs[1].Connect(multiply.Outputs[0]);

                // min + subtract
                var sum = graph.CreateModuleInstance<Adder>();
                RewireInput(sum.Inputs[0], min);
                sum.Inputs[1].Connect(subtract.Outputs[0]);

                RewireOutputs(sum.Outputs[0], Outputs[0]);
            }
            else if (Inputs[1].IsConnected == false && Inputs[1].StaticValue == 1.0f)
            {
                var t = Inputs[0];
                var max = Inputs[2];
                // t(max-min) + min, when min = 1
                // t(max - 1) + 1

                // can't simplify, but implementing with separate math modules saves upsampling

                var subtract = graph.CreateModuleInstance<Subtract>();
                RewireInput(subtract.Inputs[0], max);
                subtract.Inputs[1].StaticValue = 1.0f;

                var multiply = graph.CreateModuleInstance<Multiplier>();
                RewireInput(multiply.Inputs[0], t);
                multiply.Inputs[1].Connect(subtract.Outputs[0]);

                var add = graph.CreateModuleInstance<Adder>();
                add.Inputs[0].Connect(multiply.Outputs[0]);
                add.Inputs[1].StaticValue = 1.0f;

                RewireOutputs(add.Outputs[0], Outputs[0]);
            }
            else
            {
                // Full-on Lerp; upsample if min/max states are unmatched
                if (Inputs[1].State != Inputs[2].State)
                {
                    var hold = graph.CreateModuleInstance<HoldSample>();
                    var staticIn = Inputs[1].State == PinState.Static ? 1 : 2;
                    RewireInput(hold.Inputs[0], Inputs[staticIn]);
                    Inputs[staticIn].Connect(hold.Outputs[0]);
                }
            }
        } 

        public override void EvaluateOffline()
        {
            if (Inputs[0].IsConnected == false &&
                Inputs[1].IsConnected == false &&
                Inputs[2].IsConnected == false)
            {
                // We can evaluate this expression offline; all inputs are constant

                float min = Inputs[1].StaticValue;
                float max = Inputs[2].StaticValue;
                float t = Inputs[0].StaticValue;
                float result = min + t * (max - min);

                var destinations = Outputs[0].OutputDestinations.ToList();
                PushStaticValueToDestinations(result, destinations, GetOutputFormat(0));
            }
        }
    }
}


