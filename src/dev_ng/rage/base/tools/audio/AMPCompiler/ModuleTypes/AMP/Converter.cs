﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;

namespace rage.AMPCompiler
{
    class Converter : ModuleInstance
    {
        public Converter(XElement element)
            : base(element)
        {

        }

        public Converter()
        {

        }

        public override int NumInputs
        {
            get { return 1; }
        }

        public override int NumOutputs
        {
            get { return 1; }
        }

        public override int NumFields
        {
            get { return 0; }
        }

        public override PinFormat GetInputFormat(int index)
        {
            if (Inputs[0].IsConnected)
            {
                return Inputs[0].OtherPin.InstanceFormat;
            }
            return PinFormat.Normalized;
        }

        public override PinFormat GetOutputFormat(int index)
        {
            PinFormat sourceFormat = GetInputFormat(0);
            if (sourceFormat == PinFormat.Normalized)
            {
                return PinFormat.Signal;
            }
            return PinFormat.Normalized;
        }

        public override PinState GetOutputState(int index)
        {
            return Inputs[0].State;
        }

        public override IntermediateOpcodes Instruction
        {
            get { return IntermediateOpcodes.INVALID; }
        }

        public override void ExpandBuiltInImplementations(SynthGraph graph)
        {
            PinFormat destFormat = GetOutputFormat(0);

            ModuleInstance module = null;
            if (destFormat == PinFormat.Normalized)
            {
                module = graph.CreateModuleInstance<ConvertToNormalized>();
            }
            else
            {
                module = graph.CreateModuleInstance<ConvertToSignal>();
            }

            RewireInput(module.Inputs[0], Inputs[0]);
            RewireOutputs(module.Outputs[0], Outputs[0]);
        }


        
    }
}
