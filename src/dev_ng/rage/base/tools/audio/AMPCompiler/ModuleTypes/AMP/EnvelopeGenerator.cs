﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;

namespace rage.AMPCompiler
{
    class EnvelopeGenerator : ModuleInstance
    {
        public EnvelopeGenerator(XElement element)
            : base(element)
        {

        }

        public EnvelopeGenerator()
        {

        }
        enum synthEnvelopeGeneratorInputs
        {
            kEnvelopeGeneratorPredelay = 0,
            kEnvelopeGeneratorAttack,
            kEnvelopeGeneratorDecay,
            kEnvelopeGeneratorSustain,
            kEnvelopeGeneratorHold,
            kEnvelopeGeneratorRelease,
            kEnvelopeGeneratorTrigger,

            kEnvelopeGeneratorNumInputs
        };
        int InputIndex(synthEnvelopeGeneratorInputs inputId)
        {
            return (int)inputId;
        }

        public override int NumInputs
        {
            get { return InputIndex(synthEnvelopeGeneratorInputs.kEnvelopeGeneratorNumInputs); }
        }

        enum synthEnvelopeGeneratorOutputs
        {
            kEnvelopeGeneratorEnvelope = 0,

            kEnvelopeGeneratorAttackActive,
            kEnvelopeGeneratorDecayActive,
            kEnvelopeGeneratorHoldActive,
            kEnvelopeGeneratorReleaseActive,

            kEnvelopeGeneratorFinished,
            kEnvelopeGeneratorNumOutputs
        };

        int OutputIndex(synthEnvelopeGeneratorOutputs outputId)
        {
            return (int)outputId;
        }

        public override int NumOutputs
        {
            get { return OutputIndex(synthEnvelopeGeneratorOutputs.kEnvelopeGeneratorNumOutputs); }
        }

        enum synthEnvelopeGeneratorFields
        {
            kReleaseType = 0,
            kTriggerMode,

            kEnvelopeGeneratorNumFields
        }

        enum synthEnvelopeReleaseType
        {
            Linear = 0,
            Exponential
        };

        enum synthEnvelopeTriggerMode
        {
            OneShot = 0,
            Retrigger,
            Interruptible
        };

        int FieldIndex(synthEnvelopeGeneratorFields fieldId)
        {
            return (int)fieldId;
        }

        public override int NumFields
        {
            get { return FieldIndex(synthEnvelopeGeneratorFields.kEnvelopeGeneratorNumFields); }
        }

        public override PinFormat GetInputFormat(int index)
        {
            return PinFormat.Normalized;
        }

        public override PinFormat GetOutputFormat(int index)
        {
            return PinFormat.Normalized;
        }

        public override PinState GetInputState(int index)
        {
            // For now only support static inputs
            return PinState.Static;
        }

        public override PinState GetOutputState(int index)
        {
            // Also for now, only support dynamic output
            if(index == OutputIndex(synthEnvelopeGeneratorOutputs.kEnvelopeGeneratorEnvelope))
            {
                return PinState.Dynamic;
            }
            // Finished trigger is static
            return PinState.Static;
        }

        public override void ExpandBuiltInImplementations(SynthGraph graph)
        {
            // implement phase active outputs via TimedTrigger module
            if (Outputs.Count() == OutputIndex(synthEnvelopeGeneratorOutputs.kEnvelopeGeneratorNumOutputs))
            {
                bool hasTriggerOutputs = false;
                for (int i = OutputIndex(synthEnvelopeGeneratorOutputs.kEnvelopeGeneratorAttackActive); i <= OutputIndex(synthEnvelopeGeneratorOutputs.kEnvelopeGeneratorReleaseActive); i++)
                {
                    if (Outputs[i].OutputDestinations.Count > 0)
                    {
                        hasTriggerOutputs = true;
                    }
                }

                if (hasTriggerOutputs)
                {
                    var triggerModule = graph.CreateModuleInstance<TimedTrigger>();

                    // Map envelope trigger mode to TimedTrigger trigger mode
                    switch (TriggerMode)
                    {
                        case synthEnvelopeTriggerMode.Retrigger:
                            triggerModule.Mode = TimedTrigger.TriggerMode.Retrigger;
                            break;
                        case synthEnvelopeTriggerMode.Interruptible:
                            triggerModule.Mode = TimedTrigger.TriggerMode.Interruptible;
                            break;
                        case synthEnvelopeTriggerMode.OneShot:
                            triggerModule.Mode = TimedTrigger.TriggerMode.OneShot;
                            break;
                    }

                    // Inputs need to be connected to both
                    DuplicateInput(triggerModule.Inputs[0], Inputs[InputIndex(synthEnvelopeGeneratorInputs.kEnvelopeGeneratorTrigger)]);
                    DuplicateInput(triggerModule.Inputs[1], Inputs[InputIndex(synthEnvelopeGeneratorInputs.kEnvelopeGeneratorPredelay)]);
                    DuplicateInput(triggerModule.Inputs[2], Inputs[InputIndex(synthEnvelopeGeneratorInputs.kEnvelopeGeneratorAttack)]);
                    DuplicateInput(triggerModule.Inputs[3], Inputs[InputIndex(synthEnvelopeGeneratorInputs.kEnvelopeGeneratorDecay)]);
                    DuplicateInput(triggerModule.Inputs[4], Inputs[InputIndex(synthEnvelopeGeneratorInputs.kEnvelopeGeneratorHold)]);
                    DuplicateInput(triggerModule.Inputs[5], Inputs[InputIndex(synthEnvelopeGeneratorInputs.kEnvelopeGeneratorRelease)]);
                    // Disconnect all trigger outputs from EnvelopeGenerator other than 'Finished'
                    RewireOutputs(triggerModule.Outputs[0], Outputs[OutputIndex(synthEnvelopeGeneratorOutputs.kEnvelopeGeneratorAttackActive)]);
                    RewireOutputs(triggerModule.Outputs[1], Outputs[OutputIndex(synthEnvelopeGeneratorOutputs.kEnvelopeGeneratorDecayActive)]);
                    RewireOutputs(triggerModule.Outputs[2], Outputs[OutputIndex(synthEnvelopeGeneratorOutputs.kEnvelopeGeneratorHoldActive)]);
                    RewireOutputs(triggerModule.Outputs[3], Outputs[OutputIndex(synthEnvelopeGeneratorOutputs.kEnvelopeGeneratorReleaseActive)]);
                }
            }

            // Replace this intance with an EGSimple module
            var eg = graph.CreateModuleInstance<EGSimple>();
            for (int i = 0; i < Inputs.Count(); i++)
            {
                RewireInput(eg.Inputs[i], Inputs[i]);
            }
            RewireOutputs(eg.Outputs[0], Outputs[OutputIndex(synthEnvelopeGeneratorOutputs.kEnvelopeGeneratorEnvelope)]);
            RewireOutputs(eg.Outputs[1], Outputs[OutputIndex(synthEnvelopeGeneratorOutputs.kEnvelopeGeneratorFinished)]);
            eg.FieldValues[0] = FieldValues[0];
            eg.FieldValues[1] = FieldValues[1];
        }

        synthEnvelopeReleaseType ReleaseType
        {
            get
            {
                return (synthEnvelopeReleaseType)m_FieldValues[FieldIndex(synthEnvelopeGeneratorFields.kReleaseType)];
            }
        }

        synthEnvelopeTriggerMode TriggerMode
        {
            get
            {
                return (synthEnvelopeTriggerMode)m_FieldValues[FieldIndex(synthEnvelopeGeneratorFields.kTriggerMode)];
            }
        }

        public override IntermediateOpcodes Instruction
        {
            get 
            {
                return IntermediateOpcodes.INVALID;

            }
        }
    }
}
