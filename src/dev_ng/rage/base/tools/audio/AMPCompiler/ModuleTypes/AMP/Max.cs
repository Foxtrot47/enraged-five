﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;

namespace rage.AMPCompiler
{
    class Max : ModuleInstance
    {
        public Max(XElement element)
            : base(element)
        {

        }

        public Max()
        {

        }

        public override int NumInputs
        {
            get { return 2; }
        }

        public override int NumOutputs
        {
            get { return 1; }
        }

        public override int NumFields
        {
            get { return 1; }
        }

        public override PinFormat GetInputFormat(int index)
        {
            if (index == 1)
                return PinFormat.Normalized;
            return PinFormat.Either;
        }

        public override PinFormat GetOutputFormat(int index)
        {
            if (Inputs[0].IsConnected)
            {
                return Inputs[0].OtherPin.InstanceFormat;
            }
            return PinFormat.Normalized;
        }

        public override PinState GetOutputState(int index)
        {
            if (Inputs[0].State == PinState.Static)
                return PinState.Static;
            //return OutputMode == Mode.Static ? PinState.Static : PinState.Dynamic;
            return PinState.Dynamic;
        }

        public override PinState GetInputState(int index)
        {
            if (index == 1)
            {
                return PinState.Static;
            }
            return PinState.Either;
        }

       /* public enum Mode
        {
            Static = 0,
            Dynamic,
        }

        public Mode OutputMode
        {
            get
            {
                if (FieldValues != null && FieldValues.Count() > 0 && FieldValues[0] == 1.0f)
                {
                    return Mode.Dynamic;
                }
                return Mode.Static;
            }
        }

        public override void ExpandBuiltInImplementations(SynthGraph graph)
        {
            if (OutputMode == Mode.Static && Inputs[0].State == PinState.Dynamic)
            {
                var sah = graph.CreateModuleInstance<SampleAndHold>();
                RewireInput(sah.Inputs[0], Inputs[0]);
                Inputs[0].Connect(sah.Outputs[0]);
            }
        }*/

        public override IntermediateOpcodes Instruction
        {
            get { return IntermediateOpcodes.MAX; }
        }
    }
}
