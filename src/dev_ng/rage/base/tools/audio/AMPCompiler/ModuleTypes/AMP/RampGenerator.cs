﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;

namespace rage.AMPCompiler
{
    class RampGenerator : ModuleInstance
    {
        public RampGenerator(XElement element)
            : base(element)
        {

        }

        public RampGenerator()
        {

        }

        public override int NumInputs
        {
            get { return 1; }
        }

        public override int NumOutputs
        {
            get { return 1; }
        }

        public override int NumFields
        {
            get { return 0; }
        }

        public override PinFormat GetInputFormat(int index)
        {
            return PinFormat.Normalized;
        }

        public override PinFormat GetOutputFormat(int index)
        {
            return PinFormat.Normalized;
        }

        public override PinState GetOutputState(int index)
        {
            return IsStaticMode ? PinState.Static : PinState.Dynamic;
        }

        public override PinState GetInputState(int index)
        {
            return IsStaticMode ? PinState.Static : PinState.Either;
        }

        public override IntermediateOpcodes Instruction
        {
            get { return IntermediateOpcodes.OSC_RAMP; }
        }

        public bool IsStaticMode
        {
            get;
            set;
        }
    }
}
