﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace rage.AMPCompiler
{
    class BuiltInBiquadCoefficientsModule : StandardModuleType
    {

        public InstructionId Instruction
        {
            get
            {
                switch(Mode)
                {
                    case LowPass2Pole:
                        return InstructionId.BiquadCoefficients_LowPass2Pole;
                    case HighPass2Pole:
                            return InstructionId.BiquadCoefficients_HighPass2Pole;
                    case BandPass2Pole:
                            return InstructionId.BiquadCoefficients_BandPass2Pole;
                    case BandStop2Pole:
                            return InstructionId.BiquadCoefficients_BandStop2Pole;              
                    case LowPass4Pole:
                            return InstructionId.BiquadCoefficients_LowPass4Pole;
                    case HighPass4Pole:
                            return InstructionId.BiquadCoefficients_HighPass4Pole;
                    case BandPass4Pole:
                            return InstructionId.BiquadCoefficients_BandPass4Pole;
                    case BandStop4Pole:
                            return InstructionId.BiquadCoefficients_BandStop4Pole;
                    case PeakingEQ:
                            return InstructionId.BiquadCoefficients_PeakingEQ;
                }
            }
        }

        public BiquadModes Mode
        {
            get 
            { 
                return (BiquadModes)instance.Fields[0].Value; 
            }
        }

        struct BiquadCoefficients
        {
            public double a0, a1, a2;
            public double b1, b2;
        }

        public BuiltInBiquadCoefficientsModule(XElement element)
            : base(element)
        {

        
        }

        public override void EvaluateOffline(ModuleInstance instance)
        {
            if (instance.Inputs[0].IsConnected == false &&
               instance.Inputs[1].IsConnected == false &&
               instance.Inputs[2].IsConnected == false)
            {
                // We can evaluate this expression offline; all inputs are constant

                BiquadCoefficients coefficients = ComputeCoefficients(Mode, 
                                                                        instance.Inputs[0].StaticValue, 
                                                                        instance.Inputs[1].StaticValue, 
                                                                        instance.Inputs[2].StaticValue);

                PushStaticValueToDestinations(coefficients.a0, instance.Outputs[0].OutputDestinations);
                PushStaticValueToDestinations(coefficients.a1, instance.Outputs[1].OutputDestinations);
                PushStaticValueToDestinations(coefficients.a2, instance.Outputs[2].OutputDestinations);
                PushStaticValueToDestinations(coefficients.b1, instance.Outputs[3].OutputDestinations);
                PushStaticValueToDestinations(coefficients.b2, instance.Outputs[4].OutputDestinations);
                
            }
        }

        void PushStaticValueToDestinations(double value, List<InputPinInstance> destinationList)
        {
            InputPinInstance[] destinations = destinationList.ToArray();

            foreach (InputPinInstance destination in destinations)
            {
                destination.Disconnect();
                destination.StaticValue = (float)value;
                destination.SpecifiedFormat = PinFormat.Normalized;
            }
        }

        public static bool Is4Pole(BiquadModes mode)
        {
            return mode >= BiquadModes.kFirst4PoleMode;
        }

        public enum BiquadModes
        {
            LowPass2Pole = 0,
            HighPass2Pole,
            BandPass2Pole,
            BandStop2Pole,

            kFirst4PoleMode,
            LowPass4Pole = kFirst4PoleMode,
            HighPass4Pole,
            BandPass4Pole,
            BandStop4Pole,
            PeakingEQ
        }

        BiquadCoefficients ComputeCoefficients(BiquadModes mode, float frequencyInput, float bandwidthInput, float resonanceInput)
        {
            bool is4Pole = mode >= BiquadModes.kFirst4PoleMode;
            double fs = 48000;

            double frequency = frequencyInput;
            double bandwidth = bandwidthInput;
            double resonance = resonanceInput;

            //Clamp the bandwidth to ensure we don't go below DC - and into a world of filter
            //stability pain.
            
            // dont allow any mode other than HPF to reach 0hz
            if(mode != BiquadModes.HighPass2Pole && mode != BiquadModes.HighPass4Pole)
            {
                frequency = Math.Max(0.01, frequency);
            }
            
            bandwidth = Math.Min(bandwidth, frequency * 2.0);

            BiquadCoefficients coeffs = new BiquadCoefficients();
            
            double omega = 2.0 * Math.PI * frequency / fs;
          
            switch (mode)
            {
                case BiquadModes.LowPass2Pole:
                case BiquadModes.LowPass4Pole:
                {
                    //Compensate for cascaded sections.
                    double resFactor = is4Pole ? 0.5 : 1.0;
                    double cs = Math.Cos(omega);
                    double sn = Math.Sin(omega);
                    double alpha = sn * Math.Sinh(0.5f / (resonance * resFactor));

                    coeffs.a0 = (1.0 - cs) / 2.0;
                    coeffs.a1 = 1.0 - cs;
                    coeffs.a2 = coeffs.a0;
                    double b0 = 1.0 + alpha;
                    coeffs.b1 = -2.0 * cs;
                    coeffs.b2 = 1.0 - alpha;

                    //Normalize so b0 = 1.0.
                    coeffs.a0 /= b0;
                    coeffs.a1 /= b0;
                    coeffs.a2 /= b0;
                    coeffs.b1 /= b0;
                    coeffs.b2 /= b0;
                }
                break;

                case BiquadModes.HighPass2Pole:
                case BiquadModes.HighPass4Pole:
                {
                    //Compensate for cascaded sections.
                    double  resFactor = is4Pole ? 0.5 : 1.0;
                    double cs = Math.Cos(omega);
                    double sn = Math.Sin(omega);
                    double alpha = sn * Math.Sinh(0.5 / (resonance * resFactor));

                    coeffs.a0 = (1.0 + cs) / 2.0;
                    coeffs.a1 = -1.0 - cs;
                    coeffs.a2 = coeffs.a0;
                    double b0 = 1.0 + alpha;
                    coeffs.b1 = -2.0 * cs;
                    coeffs.b2 = 1.0 - alpha;

                    //Normalize so b0 = 1.0.
                    coeffs.a0 /= b0;
                    coeffs.a1 /= b0;
                    coeffs.a2 /= b0;
                    coeffs.b1 /= b0;
                    coeffs.b2 /= b0;
                }
                break;

                case BiquadModes.BandPass2Pole:
                case BiquadModes.BandPass4Pole:
                {
                    double c = (1.0 / Math.Tan(Math.PI * (bandwidth / fs)));
                    double d = (2.0 * Math.Cos(omega));

                    coeffs.a0 = (1.0 / (1.0 + c));
                    coeffs.a1 = 0.0;
                    coeffs.a2 = -coeffs.a0;
                    coeffs.b1 = -coeffs.a0 * c * d;
                    coeffs.b2 = coeffs.a0 * (c - 1.0);
                }
                break;

                case BiquadModes.BandStop2Pole:
                case BiquadModes.BandStop4Pole:
                {
                    double c = Math.Tan(Math.PI * (bandwidth / fs));
                    double d = 2.0 * Math.Cos(omega);

                    coeffs.a0 = (1.0 / (1.0 + c));
                    coeffs.a1 = -coeffs.a0 * d;
                    coeffs.a2 = coeffs.a0;
                    coeffs.b1 = coeffs.a1;
                    coeffs.b2 = coeffs.a0 * (1.0 - c);
                }
                break;

                case BiquadModes.PeakingEQ:
                {
                    double Q = frequency / bandwidth;
                    double alpha = Math.Sin(omega) / (2.0 * Q);
                    double cs = Math.Cos(omega);
                    //Since we're using resonance as the gain/cut for this peak, and 0dB of resonance for the other filter
                    //modes equates to -3.01dB at Fc, our desired gain/cut for this filter is actually 3.01 dB higher than 
                    //the linear m_Resonance value stored in our settings structure.  That's why we multiply by 1.4142.
                    double A = resonance * 1.4142;

                    coeffs.a0 = 1.0 + alpha * A;
                    coeffs.a1 = -2.0 * cs;
                    coeffs.a2 = 1.0 - alpha * A;
                    double b0 = 1.0 + alpha / A;
                    coeffs.b1 = -2.0 * cs;
                    coeffs.b2 = 1.0 - alpha / A;
                   
                    //Normalize so b0 = 1.0.
                    coeffs.a0 /= b0;
                    coeffs.a1 /= b0;
                    coeffs.a2 /= b0;
                    coeffs.b1 /= b0;
                    coeffs.b2 /= b0;
                }
                break;
            }

            return coeffs;
        }
    }
}
