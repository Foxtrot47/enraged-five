﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;

namespace rage.AMPCompiler
{
    class OnePole : ModuleInstance
    {
        public OnePole(XElement element)
            : base(element)
        {

        }

        public OnePole()
        {

        }

        enum InputIds
        {
            Signal = 0, 
            Gain,
            Cutoff,
            
            NumInputIds
        }
        static int InputIndex(InputIds input)
        {
            return (int)input;
        }
        public override int NumInputs
        {
            get { return InputIndex(InputIds.NumInputIds); }
        }

        public override int NumOutputs
        {
            get { return 1; }
        }

        enum Fields
        {
            MinFreq = 0,
            MaxFreq,
            IsHighpass,
            NumFields
        }
        static int FieldIndex(Fields field)
        {
            return (int)field;
        }
        public override int NumFields
        {
            get { return FieldIndex(Fields.NumFields); }
        }

        public override PinFormat GetInputFormat(int index)
        {
            if (index == 0)
                return PinFormat.Signal;
            return PinFormat.Normalized;
        }

        public override PinFormat GetOutputFormat(int index)
        {
            return PinFormat.Signal;
        }

        public override PinState GetOutputState(int index)
        {
            return Inputs[0].State;
        }

        public override bool ShouldSerializeInput(int index)
        {
            return index == InputIndex(InputIds.Signal) || index == InputIndex(InputIds.Cutoff);
        }

        public override void ExpandBuiltInImplementations(SynthGraph graph)
        {
            ImplementAs_LerpInput(InputIndex(InputIds.Cutoff), graph, FieldValues[FieldIndex(Fields.MinFreq)], FieldValues[FieldIndex(Fields.MaxFreq)]);
            ImplementAs_ScaleOutput(InputIndex(InputIds.Gain), graph);
        }

        public override IntermediateOpcodes Instruction
        {
            get 
            {
                if (m_FieldValues[FieldIndex(Fields.IsHighpass)] == 1.0f)
                {
                    return IntermediateOpcodes.OnePole_HPF;
                }
                return IntermediateOpcodes.OnePole_LPF; 
            }
        }
    }
}
