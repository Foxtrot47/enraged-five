﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace rage.AMPCompiler
{
    class RandomizeOnInit : ModuleInstance
    {
        public RandomizeOnInit(XElement element)
            : base(element)
        {

        }

        public RandomizeOnInit()
        {

        }

        public override int NumInputs
        {
            get { return 2; }
        }

        public override int NumOutputs
        {
            get { return 1; }
        }

        public override int NumFields
        {
            get { return 0; }
        }

        public override PinFormat GetInputFormat(int index)
        {
            return PinFormat.Normalized;
        }

        public override PinFormat GetOutputFormat(int index)
        {
            return PinFormat.Normalized;
        }

        public override PinState GetOutputState(int index)
        {
            return PinState.Static;
        }

        public override PinState GetInputState(int index)
        {
            return PinState.Static;
        }

        public override IntermediateOpcodes Instruction
        {
            get
            {
                return IntermediateOpcodes.RANDOMIZE_ONINIT;
            }
        }
    }
}
