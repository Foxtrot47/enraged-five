﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace rage.AMPCompiler
{
    class TriggerModule : ModuleInstance
    {
        public TriggerModule(XElement element)
            : base(element)
        {

        }

        public TriggerModule()
        {

        }

        public override int NumInputs
        {
            get { return 2; }
        }

        public override int NumOutputs
        {
            get { return 1; }
        }

        public override int NumFields
        {
            get { return 1; }
        }

        public override PinFormat GetInputFormat(int index)
        {
            return PinFormat.Normalized;
        }

        public override PinFormat GetOutputFormat(int index)
        {
            return PinFormat.Normalized;
        }

        public override PinState GetOutputState(int index)
        {
            return PinState.Static;            
        }

        public override PinState GetInputState(int index)
        {
            if (Mode == TriggerMode.Difference)
            {
                return PinState.Static;
            }
            else
            {
                return PinState.Either;
            }
        }

        public enum TriggerMode
        {
            Difference = 0,
            Latching
        }

        public TriggerMode Mode
        {
            get
            {
                if (FieldValues[0] == 1.0f)
                    return TriggerMode.Latching;
                return TriggerMode.Difference;
            }
            set
            {
                FieldValues[0] = value == TriggerMode.Latching ? 1.0f : 0.0f;
            }
        }

        public override IntermediateOpcodes Instruction
        {
            get { return Mode == TriggerMode.Latching ? IntermediateOpcodes.TRIGGER_LATCH : IntermediateOpcodes.TRIGGER_DIFF; }
        }
    }
}


