﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace rage.AMPCompiler
{
    class BiquadCoefficients : ModuleInstance
    {
        public BiquadCoefficients(XElement element)
            : base(element)
        {

        }

        public BiquadCoefficients()
        {

        }

        enum InputIds
        {
            Frequency = 0,
            Bandwidth,
            Resonance,
            NumInputs
        }

        enum OutputIds
        {
            a0 = 0,
            a1,
            a2,
            b1,
            b2,
            NumOutputs
        }

        struct BiquadCoefficientValues
        {
            public double a0, a1, a2;
            public double b1, b2;
        }

        static int InputIndex(InputIds input)
        {
            return (int)input;
        }

        static int OutputIndex(OutputIds output)
        {
            return (int)output;
        }

        public override int NumInputs
        {
            get { return InputIndex(InputIds.NumInputs); }
        }

        public override int NumOutputs
        {
            get { return OutputIndex(OutputIds.NumOutputs); }
        }

        public override int NumFields
        {
            get { return 0; }
        }

        public override PinState GetInputState(int index)
        {
            return PinState.Static;
        }

        public override PinFormat GetInputFormat(int index)
        {
            // Coefficients are 'normalized' format
            return PinFormat.Normalized;
        }

        public override PinFormat GetOutputFormat(int index)
        {
            return PinFormat.Normalized;
        }

        public override PinState GetOutputState(int index)
        {
            return PinState.Static;
        }

        public BiquadFilter.BiquadModes Mode { get; set; }
        public override IntermediateOpcodes Instruction
        {
            get
            {
                switch (Mode)
                {
                    case BiquadFilter.BiquadModes.BandPass2Pole:
                    case BiquadFilter.BiquadModes.BandPass4Pole:
                        return IntermediateOpcodes.BiquadCoefficients_BandPass;
                    case BiquadFilter.BiquadModes.LowPass2Pole:
                        return IntermediateOpcodes.BiquadCoefficients_LowPass2Pole;
                    case BiquadFilter.BiquadModes.LowPass4Pole:
                        return IntermediateOpcodes.BiquadCoefficients_LowPass4Pole;
                    case BiquadFilter.BiquadModes.HighPass2Pole:
                        return IntermediateOpcodes.BiquadCoefficients_HighPass2Pole;
                    case BiquadFilter.BiquadModes.HighPass4Pole:
                        return IntermediateOpcodes.BiquadCoefficients_HighPass4Pole;
                    case BiquadFilter.BiquadModes.BandStop2Pole:
                    case BiquadFilter.BiquadModes.BandStop4Pole:
                        return IntermediateOpcodes.BiquadCoefficients_BandStop;
                    case BiquadFilter.BiquadModes.PeakingEQ:
                        return IntermediateOpcodes.BiquadCoefficients_PeakingEQ;
                    case BiquadFilter.BiquadModes.LowShelf2Pole:
                        return IntermediateOpcodes.BiquadCoefficients_LowShelf2Pole;
                    case BiquadFilter.BiquadModes.LowShelf4Pole:
                        return IntermediateOpcodes.BiquadCoefficients_LowShelf4Pole;
                    case BiquadFilter.BiquadModes.HighShelf2Pole:
                        return IntermediateOpcodes.BiquadCoefficients_HighShelf2Pole;
                    case BiquadFilter.BiquadModes.HighShelf4Pole:
                        return IntermediateOpcodes.BiquadCoefficients_HighShelf4Pole;
                }
                return IntermediateOpcodes.INVALID;
            }
        }

        public override bool ShouldSerializeInput(int index)
        {
            switch (Mode)
            {
                case BiquadFilter.BiquadModes.BandPass2Pole:
                case BiquadFilter.BiquadModes.BandPass4Pole:
                case BiquadFilter.BiquadModes.BandStop2Pole:
                case BiquadFilter.BiquadModes.BandStop4Pole:
                    return index != InputIndex(InputIds.Resonance);
                case BiquadFilter.BiquadModes.LowPass2Pole:
                case BiquadFilter.BiquadModes.LowPass4Pole:
                case BiquadFilter.BiquadModes.HighPass2Pole:
                case BiquadFilter.BiquadModes.HighPass4Pole:
                    return index != InputIndex(InputIds.Bandwidth);
                case BiquadFilter.BiquadModes.PeakingEQ:
                case BiquadFilter.BiquadModes.LowShelf2Pole:
                case BiquadFilter.BiquadModes.LowShelf4Pole:
                case BiquadFilter.BiquadModes.HighShelf2Pole:
                case BiquadFilter.BiquadModes.HighShelf4Pole:
                    return true;
            }

            return false;
        }

        public override void EvaluateOffline()
        {
            if (Inputs[0].IsConnected == false &&
               Inputs[1].IsConnected == false &&
               Inputs[2].IsConnected == false)
            {
                // We can evaluate this expression offline; all inputs are constant

                BiquadCoefficientValues coefficients = ComputeCoefficients(Mode,
                                                                        Inputs[0].StaticValue,
                                                                        Inputs[1].StaticValue,
                                                                        Inputs[2].StaticValue);

                PushStaticValueToDestinations(coefficients.a0, Outputs[0].OutputDestinations, PinFormat.Normalized);
                PushStaticValueToDestinations(coefficients.a1, Outputs[1].OutputDestinations, PinFormat.Normalized);
                PushStaticValueToDestinations(coefficients.a2, Outputs[2].OutputDestinations, PinFormat.Normalized);
                PushStaticValueToDestinations(coefficients.b1, Outputs[3].OutputDestinations, PinFormat.Normalized);
                PushStaticValueToDestinations(coefficients.b2, Outputs[4].OutputDestinations, PinFormat.Normalized);

            }
        }

        BiquadCoefficientValues ComputeCoefficients(BiquadFilter.BiquadModes mode, float frequencyInput, float bandwidthInput, float resonanceInput)
        {
            bool is4Pole = mode >= BiquadFilter.BiquadModes.kFirst4PoleMode;
            double fs = 48000;

            double frequency = frequencyInput;
            double bandwidth = bandwidthInput;
            double resonance = resonanceInput;

            //Clamp the bandwidth to ensure we don't go below DC - and into a world of filter
            //stability pain.

            // dont allow any mode other than HPF to reach 0hz
            if (mode != BiquadFilter.BiquadModes.HighPass2Pole && mode != BiquadFilter.BiquadModes.HighPass4Pole)
            {
                frequency = Math.Max(0.01, frequency);
            }

            bandwidth = Math.Min(bandwidth, frequency * 2.0);

            BiquadCoefficientValues coeffs = new BiquadCoefficientValues();

            double omega = 2.0 * Math.PI * frequency / fs;

            switch (mode)
            {
                case BiquadFilter.BiquadModes.LowPass2Pole:
                case BiquadFilter.BiquadModes.LowPass4Pole:
                    {
                        //Compensate for cascaded sections.
                        double resFactor = is4Pole ? 0.5 : 1.0;
                        double cs = Math.Cos(omega);
                        double sn = Math.Sin(omega);
                        double alpha = sn * Math.Sinh(0.5f / (resonance * resFactor));

                        coeffs.a0 = (1.0 - cs) / 2.0;
                        coeffs.a1 = 1.0 - cs;
                        coeffs.a2 = coeffs.a0;
                        double b0 = 1.0 + alpha;
                        coeffs.b1 = -2.0 * cs;
                        coeffs.b2 = 1.0 - alpha;

                        //Normalize so b0 = 1.0.
                        coeffs.a0 /= b0;
                        coeffs.a1 /= b0;
                        coeffs.a2 /= b0;
                        coeffs.b1 /= b0;
                        coeffs.b2 /= b0;
                    }
                    break;

                case BiquadFilter.BiquadModes.HighPass2Pole:
                case BiquadFilter.BiquadModes.HighPass4Pole:
                    {
                        //Compensate for cascaded sections.
                        double resFactor = is4Pole ? 0.5 : 1.0;
                        double cs = Math.Cos(omega);
                        double sn = Math.Sin(omega);
                        double alpha = sn * Math.Sinh(0.5 / (resonance * resFactor));

                        coeffs.a0 = (1.0 + cs) / 2.0;
                        coeffs.a1 = -1.0 - cs;
                        coeffs.a2 = coeffs.a0;
                        double b0 = 1.0 + alpha;
                        coeffs.b1 = -2.0 * cs;
                        coeffs.b2 = 1.0 - alpha;

                        //Normalize so b0 = 1.0.
                        coeffs.a0 /= b0;
                        coeffs.a1 /= b0;
                        coeffs.a2 /= b0;
                        coeffs.b1 /= b0;
                        coeffs.b2 /= b0;
                    }
                    break;

                case BiquadFilter.BiquadModes.BandPass2Pole:
                case BiquadFilter.BiquadModes.BandPass4Pole:
                    {
                        double c = (1.0 / Math.Tan(Math.PI * (bandwidth / fs)));
                        double d = (2.0 * Math.Cos(omega));

                        coeffs.a0 = (1.0 / (1.0 + c));
                        coeffs.a1 = 0.0;
                        coeffs.a2 = -coeffs.a0;
                        coeffs.b1 = -coeffs.a0 * c * d;
                        coeffs.b2 = coeffs.a0 * (c - 1.0);
                    }
                    break;

                case BiquadFilter.BiquadModes.BandStop2Pole:
                case BiquadFilter.BiquadModes.BandStop4Pole:
                    {
                        double c = Math.Tan(Math.PI * (bandwidth / fs));
                        double d = 2.0 * Math.Cos(omega);

                        coeffs.a0 = (1.0 / (1.0 + c));
                        coeffs.a1 = -coeffs.a0 * d;
                        coeffs.a2 = coeffs.a0;
                        coeffs.b1 = coeffs.a1;
                        coeffs.b2 = coeffs.a0 * (1.0 - c);
                    }
                    break;

                case BiquadFilter.BiquadModes.PeakingEQ:
                    {
                        double Q = frequency / bandwidth;
                        double alpha = Math.Sin(omega) / (2.0 * Q);
                        double cs = Math.Cos(omega);
                        //Since we're using resonance as the gain/cut for this peak, and 0dB of resonance for the other filter
                        //modes equates to -3.01dB at Fc, our desired gain/cut for this filter is actually 3.01 dB higher than 
                        //the linear m_Resonance value stored in our settings structure.  That's why we multiply by 1.4142.
                        double A = resonance * 1.4142;

                        coeffs.a0 = 1.0 + alpha * A;
                        coeffs.a1 = -2.0 * cs;
                        coeffs.a2 = 1.0 - alpha * A;
                        double b0 = 1.0 + alpha / A;
                        coeffs.b1 = -2.0 * cs;
                        coeffs.b2 = 1.0 - alpha / A;

                        //Normalize so b0 = 1.0.
                        coeffs.a0 /= b0;
                        coeffs.a1 /= b0;
                        coeffs.a2 /= b0;
                        coeffs.b1 /= b0;
                        coeffs.b2 /= b0;
                    }
                    break;
                case BiquadFilter.BiquadModes.LowShelf2Pole:
                case BiquadFilter.BiquadModes.LowShelf4Pole:
                    {
                        double clampedBandwidth = Math.Min(bandwidth, frequency * 2.0);
                        double Q = frequency / clampedBandwidth;
                        double alpha = Math.Sin(omega) / (2.0 * Q);
                        double cs = Math.Cos(omega);

                        double A = resonance;

                        double sa = 2.0 * Math.Sqrt(A) * alpha;

                        double b0 = A * ((A + 1.0) - (A - 1.0) * cs + sa);
                        double b1 = 2.0 * A * ((A - 1.0) - (A + 1.0) * cs);
                        double b2 = A * ((A + 1.0) - (A - 1.0) * cs - sa);
                        double a0 = (A + 1.0) + (A - 1.0) * cs + sa;
                        double a1 = -2.0 * ((A - 1.0) + (A + 1.0) * cs);
                        double a2 = (A + 1.0) + (A - 1.0) * cs - sa;

                        // Normalize so that b0 = 1.0
                        coeffs.a0 = a0 / b0;
                        coeffs.a1 = a1 / b0;
                        coeffs.a2 = a2 / b0;
                        coeffs.b1 = b1 / b0;
                        coeffs.b2 = b2 / b0;
                    }
                    break;
                case BiquadFilter.BiquadModes.HighShelf2Pole:
                case BiquadFilter.BiquadModes.HighShelf4Pole:
                    {
                        double clampedBandwidth = Math.Min(bandwidth, frequency * 2.0);
                        double Q = frequency / clampedBandwidth;
                        double alpha = Math.Sin(omega) / (2.0 * Q);
                        double cs = Math.Cos(omega);

                        double A = resonance;

                        double sa = 2.0 * Math.Sqrt(A) * alpha;

                        double b0 = A * ((A + 1.0) + (A - 1.0) * cs + sa);
                        double b1 = -2.0 * A * ((A - 1.0) + (A + 1.0) * cs);
                        double b2 = A * ((A + 1.0) + (A - 1.0) * cs - sa);
                        double a0 = (A + 1.0) - (A - 1.0) * cs + sa;
                        double a1 = 2.0 * ((A - 1.0) - (A + 1.0) * cs);
                        double a2 = (A + 1.0) - (A - 1.0) * cs - sa;

                        // Normalize so that b0 = 1.0
                        coeffs.a0 = a0 / b0;
                        coeffs.a1 = a1 / b0;
                        coeffs.a2 = a2 / b0;
                        coeffs.b1 = b1 / b0;
                        coeffs.b2 = b2 / b0;
                    }
                    break;
            }

            return coeffs;
        }
    }
}
