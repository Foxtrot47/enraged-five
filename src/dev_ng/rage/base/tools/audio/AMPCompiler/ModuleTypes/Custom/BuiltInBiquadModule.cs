﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace rage.AMPCompiler
{
    class BuiltInBiquadModule : StandardModuleType
    {
        public InstructionId Instruction
        {
            get
            {
               if(BuiltInBiquadCoefficientsModule.Is4Pole(Mode))
               {
                   return InstructionId.BiquadProcess_4Pole;
               }
               else
               {
                   return InstructionId.BiquadProcess_2Pole;
               }
            }
        }

        public BuiltInBiquadCoefficientsModule.BiquadModes Mode
        {
            get
            {
               return (BuiltInBiquadCoefficientsModule.BiquadModes)instance.Fields[6].Value;
            }
        }

        public BuiltInBiquadModule(XElement element) : base(element)
        {

        
        }

        public override void ExpandBuiltInImplementations(SynthGraph graph, ModuleInstance instance)
        {
            // Gain -> scale output and input pre-scaling
            base.ExpandBuiltInImplementations(graph, instance);

            // Convert into two modules; computing coefficients and process
            ModuleInstance coefficientsModule = graph.CreateModuleInstance(ModuleTypeId.SYNTH_BUILTIN_BIQUADCOEFFICIENTS);

            // Set up the coefficients module with the correct mode
            
            coefficientsModule.Fields[0].Value = (float)Mode;

            // Frequency
            RewireInput(coefficientsModule.Inputs[0], instance.Inputs[2]);
            // Bandwidth
            RewireInput(coefficientsModule.Inputs[1], instance.Inputs[3]);
            // Resonance
            RewireInput(coefficientsModule.Inputs[2], instance.Inputs[4]);

            ModuleInstance processModule = graph.CreateModuleInstance(ModuleTypeId.SYNTH_BUILTIN_BIQUADPROCESS);

            // Set up the correct mode - 2 or 4 pole
            bool is4Pole = BuiltInBiquadCoefficientsModule.Is4Pole(mode);
            processModule.Fields[0].Value = is4Pole ? 1.0f : 0.0f;

            // Signal
            RewireInput(processModule.Inputs[0], instance.Inputs[0]);
            // Connect coefficients
            for(int i = 0; i < 5; i++)
            {
                processModule.Inputs[i + 1].Connect(coefficientsModule.Outputs[i]);
            }
            
            // Rewire outputs
            InputPinInstance[] destinations = instance.Outputs[0].OutputDestinations.ToArray();
            foreach(InputPinInstance destination in destinations)
            {
                destination.Disconnect();
                destination.Connect(processModule.Outputs[0]);
            }
        }

        void RewireInput(InputPinInstance newInput, InputPinInstance oldInput)
        {
            if (oldInput.IsConnected)
            {
                newInput.Connect(oldInput.OtherPin);
                oldInput.Disconnect();
            }
            else            
            {
                newInput.StaticValue = oldInput.StaticValue;
                newInput.SpecifiedFormat = oldInput.InstanceFormat;
            }                        
        }
    }
}
