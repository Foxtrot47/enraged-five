﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace rage.AMPCompiler
{
    class OpcodeTimings
    {
        Dictionary<string, float> OpcodeTimes;

        public OpcodeTimings(string csvPath)
        {
            OpcodeTimes = new Dictionary<string, float>();
            StreamReader reader = new StreamReader(csvPath);
            
            // Skip header
            reader.ReadLine();
            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine();
                string[] tokens = line.Split(',');

                OpcodeTimes.Add(tokens[0], float.Parse(tokens[3]));
            }
        }

        public float EstimateCost(string opcodeName)
        {
            if (OpcodeTimes.ContainsKey(opcodeName))
            {
                return OpcodeTimes[opcodeName];
            }
            return 0.0f;
        }

    }
}
