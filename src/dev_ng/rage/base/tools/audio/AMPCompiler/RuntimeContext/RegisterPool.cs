﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace rage.AMPCompiler
{
    class RegisterPool
    {
        List<Register> m_ActiveRegisters;
        int m_NextBufferId;

        public int PeakUsage { get { return m_ActiveRegisters.Count; } }

        public RegisterPool()
        {
            m_ActiveRegisters = new List<Register>();
            m_NextBufferId = 0;
        }

        public Register Allocate(int allocationStage, int freeStage)
        {
            var availableRegisters = (from b in m_ActiveRegisters 
                                    where b.FreeStage < allocationStage
                                    select b);

            if(availableRegisters.Count() > 0)
            {
                Register allocatedRegister = availableRegisters.First();
                allocatedRegister.FreeStage = freeStage;
                return allocatedRegister;
            }

            Register newRegister = new Register(allocationStage, freeStage, m_NextBufferId++);

            m_ActiveRegisters.Add(newRegister);
            return newRegister;
        }
    }
}
