﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace rage.AMPCompiler
{
    class Register : IStaticValueStore
    {
        public int AllocationStage { get; set; }
        public int FreeStage { get; set; }

        public readonly int RegisterId;

        public Register(int allocationStage, int freeStage, int registerId)
        {
            AllocationStage = allocationStage;
            FreeStage = freeStage;
            RegisterId = registerId;
        }

        public override string ToString()
        {
            return string.Format("R{0}", RegisterId);
        }
    }
}
