﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace rage.AMPCompiler
{
    class ConstantAllocator
    {
        public readonly List<Constant> Constants;
        public ConstantAllocator()
        {
            Constants = new List<Constant>();
            // First two slots are always 0 and 1
            Allocate(0.0f);
            Allocate(1.0f);
        }

        public Constant Allocate(float value)
        {
            Constant constant = Constants.Find(c => c.Value == value);
            if(constant == null)
            {
                constant = new Constant(Constants.Count, value);
                Constants.Add(constant);
            }
            return constant;
        }
    }
}
