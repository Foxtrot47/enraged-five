﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace rage.AMPCompiler
{
    class Buffer
    {
        public int AllocationStage { get; set; }
        public int FreeStage { get; set; }

        public PinFormat Format { get; set; }

        public readonly int BufferId;

        List<int> m_References;

        public void AddReference(int allocationStage)
        {
            m_References.Add(allocationStage);
        }

        public int GetNumReferences(int allocationStage)
        {
            return m_References.Count(m => m == allocationStage);
        }

        public Buffer(int allocationStage, int freeStage, PinFormat format, int bufferId)
        {
            m_References = new List<int>();
            AddReference(allocationStage);
            AllocationStage = allocationStage;
            FreeStage = freeStage;
            Format = format;
            BufferId = bufferId;
        }

        public override string ToString()
        {
            return string.Format("B{0}", BufferId);
        }
    }
}
