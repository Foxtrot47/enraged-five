﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace rage.AMPCompiler
{
    class BufferPool
    {
        List<Buffer> m_ActiveBuffers;
        int m_NextBufferId;

        public int PeakUsage { get { return m_ActiveBuffers.Count; } }

        public BufferPool()
        {
            m_ActiveBuffers = new List<Buffer>();
            m_NextBufferId = 0;
        }

        public Buffer AllocateBuffer(PinFormat format, int allocationStage, int freeStage)
        {
            var availableBuffers = (from b in m_ActiveBuffers 
                                    where b.FreeStage < allocationStage && b.Format == format
                                    select b);

            if(availableBuffers.Count() > 0)
            {
                Buffer allocatedBuffer = availableBuffers.First();
                allocatedBuffer.FreeStage = freeStage;
                allocatedBuffer.AddReference(allocationStage);
                return allocatedBuffer;
            }

            Buffer newBuffer = new Buffer(allocationStage, freeStage, format, m_NextBufferId++);
            
            m_ActiveBuffers.Add(newBuffer);
            return newBuffer;
        }
    }
}
