﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace rage.AMPCompiler
{
    class Constant : IStaticValueStore
    {
        public readonly int Index;
        public readonly float Value;

        public Constant(int index, float value)
        {
            Value = value;
            Index = index;
        }

        public override string ToString()
        {
            return string.Format("C{0}", Index);
        }
    }
}
