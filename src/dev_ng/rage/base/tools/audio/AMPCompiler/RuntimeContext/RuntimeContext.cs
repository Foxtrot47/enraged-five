﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace rage.AMPCompiler
{
    class RuntimeContext
    {
        public readonly BufferPool BufferPool;
        public readonly ConstantAllocator ConstantAllocator;
        public readonly RegisterPool RegisterPool;
        public readonly InputVariables InputVariables;

        public RuntimeContext()
        {
            ConstantAllocator = new ConstantAllocator();
            BufferPool = new BufferPool();
            RegisterPool = new RegisterPool();
            InputVariables = new InputVariables();
        }
    }
}
