﻿using System;
using System.Collections.Generic;
using System.Text;

namespace rage.AMPCompiler
{
    class InputVariables
    {
        public class VariableInputDefinition
        {
            public string Name {get; set;}
            public float InitialValue { get; set; }

            public override string ToString()
            {
                return Name + ": " + InitialValue.ToString();
            }
        }

        public InputVariables()
        {
            m_Variables = new List<VariableInputDefinition>();
        }

        public int Count
        {
            get { return m_Variables.Count; }
        }
        public VariableInputDefinition this [int index]
        {
            get
            {
                return m_Variables[index];
            }
        }
        
        public List<VariableInputDefinition> Variables
        {
            get { return m_Variables; }
        }

        List<VariableInputDefinition> m_Variables;

        public int Add(string name, float initialValue)
        {
            for (int i = 0; i < m_Variables.Count; i++)
            {
                if (m_Variables[i].Name.Equals(name, StringComparison.OrdinalIgnoreCase))
                {
                    return i;
                }
            }

            int ret = m_Variables.Count;
            VariableInputDefinition newVar = new VariableInputDefinition { Name = name, InitialValue = initialValue };
            m_Variables.Add(newVar);
            return ret;
        }
    }

    
}
