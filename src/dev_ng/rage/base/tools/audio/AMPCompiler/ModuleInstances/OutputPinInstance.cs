﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;

namespace rage.AMPCompiler
{
    //[DebuggerDisplay("{ParentModule.TypeId} Output:{Index}")]
    class OutputPinInstance
    {
        public readonly List<InputPinInstance> OutputDestinations;
        public readonly ModuleInstance ParentModule;
        public readonly XElement Element;
        
        public readonly int Index;

        public bool ShouldSerialize
        {
            get
            {
                return ParentModule.ShouldSerializeOutput(Index);
            }
        }

        public PinFormat InstanceFormat
        {
            get
            {
                return ParentModule.GetOutputFormat(Index);
            }
        }

        public PinState State
        {
            get
            {
                return ParentModule.GetOutputState(Index);
            }
        }

        public OutputPinInstance(ModuleInstance parent, XElement element, int index)
        {
            Element = element;
            ParentModule = parent;
            OutputDestinations = new List<InputPinInstance>();
            Index = index;
        }
    }
}
