﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;

namespace rage.AMPCompiler
{
    [DebuggerDisplay("{Definition.Name} = {Value}")]
    class FieldInstance
    {
        public readonly Field Definition;
        public float Value { get; set; }

        public FieldInstance(Field definition, float value)
        {
            Value = value;
            Definition = definition;
        }
    }
}
