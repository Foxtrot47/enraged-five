﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;

namespace rage.AMPCompiler
{
    //[DebuggerDisplay("{DebugString}")]
    class InputPinInstance
    {
        public readonly ModuleInstance ParentModule;
        public float StaticValue{ get; set; }
        public readonly XElement Element;
        public readonly int Index;

        public bool ShouldSerialize
        {
            get
            {
                return ParentModule.ShouldSerializeInput(Index);
            }
        }

        public OutputPinInstance OtherPin { get { return m_OtherPin; } }
        OutputPinInstance m_OtherPin;

        public bool IsConnected { get { return m_OtherPin != null; } }
        public bool HasStaticValue { get { return !IsConnected; } }
        public IStaticValueStore StaticInput { get; set; }
        public Buffer Buffer { get; set; }

        PinFormat m_InstanceFormat = PinFormat.Either;
        public PinFormat InstanceFormat
        {
            get
            {
                if (m_InstanceFormat == PinFormat.Either)
                    return Format;
                return m_InstanceFormat;
            }
            set
            {
                m_InstanceFormat = value;
            }
        }

        public PinFormat Format { get { return ParentModule.GetInputFormat(Index); } }
        public PinState State 
        { 
            get 
            { 
                if (IsConnected) 
                { 
                    return OtherPin.State; 
                } 
                return PinState.Static; 
            } 
        }

        public string DebugString
        {
            get
            {
                string baseStr = string.Format("{0} Input:{1}", ParentModule.Instruction, Index); 
                if(IsConnected)
                {
                    baseStr += string.Format(" <= {0}:{1}", OtherPin.ParentModule.Instruction, OtherPin.Index);
                }
                else
                {
                    baseStr += string.Format(" Value: {0}", StaticValue);
                }
                return baseStr;
            }
        }
       
        public InputPinInstance(ModuleInstance parent, XElement element, int index)
        {
            Element = element;
            ParentModule = parent;
            if (Element != null && element.Element("StaticValue") != null)
            {                
                StaticValue = float.Parse(element.Element("StaticValue").Value);
            }
            Index = index;
            InstanceFormat = PinFormat.Either;
        }

        public void SetupConnection(List<ModuleInstance> moduleInstances)
        {
            int otherPinIndex = int.Parse(Element.Element("OtherPin").Value);
            int otherModuleIndex = int.Parse(Element.Element("OtherModule").Value);

            if (otherModuleIndex != -1)
            {
                Connect(moduleInstances[otherModuleIndex].Outputs[otherPinIndex]);
            }
        }

        public void Connect(OutputPinInstance otherPin)
        {
            if (IsConnected)
            {
                Disconnect();
            }
            m_OtherPin = otherPin;
            m_OtherPin.OutputDestinations.Add(this);
        }

        public void Disconnect()
        {
            if (m_OtherPin != null)
            {
                m_OtherPin.OutputDestinations.Remove(this);
                m_OtherPin = null;
            }
        }
    }
}
