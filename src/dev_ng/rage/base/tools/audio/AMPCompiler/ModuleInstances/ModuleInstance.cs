﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;

using rage.ToolLib;

namespace rage.AMPCompiler
{
    //[DebuggerDisplay("Type: {ModuleType} Stage: {ProcessingStage}")]
    abstract class ModuleInstance
    {
        public virtual bool ShouldSerialize { get { return true; } }
        public virtual bool DebugOnly { get { return false; } }

        public abstract IntermediateOpcodes Instruction { get; }

        public virtual void ExpandBuiltInImplementations(SynthGraph graph) { }
        public virtual void EvaluateOffline() { }

        public virtual bool ShouldSerializeInput(int index) { return true; }
        public virtual bool ShouldSerializeOutput(int index) { return true; }
        
        public abstract PinFormat GetInputFormat(int index);
        public abstract PinFormat GetOutputFormat(int index);
        public abstract PinState GetOutputState(int index);
        public virtual PinState GetInputState(int index) { return PinState.Either; }

        public virtual void FinalOptimisation() { }

        public abstract int NumInputs { get; }
        public abstract int NumOutputs { get; }
        public abstract int NumFields { get; }

        public virtual bool IsAudioInput { get { return false; } }
        public virtual bool IsAudioOutput { get { return false; } }

        public readonly XElement Element;
        
        protected InputPinInstance[] m_Inputs;
        public InputPinInstance[] Inputs { get { return m_Inputs; } }
        protected OutputPinInstance[] m_Outputs;
        public OutputPinInstance[] Outputs { get { return m_Outputs; } }

        protected float[] m_FieldValues;
        public float[] FieldValues { get { return m_FieldValues; } }
        
        public int ProcessingStage { get; set; }

        public bool HasExpanded { get; set; }

        public virtual bool ShouldRemoveWhenDisconnected() { return true; }

        public ModuleInstance(XElement element)
        {
            Element = element;
            
            ProcessingStage = int.Parse(Element.Element("ProcessingStage").Value);

            int inputIndex = 0;
            XElement inputPinRoot = Element.Element("InputPin");
            if (inputPinRoot != null)
            {
                if (inputPinRoot.Elements("Instance").Count() > 0)
                {
                    m_Inputs = (from m in inputPinRoot.Elements("Instance")
                                select new InputPinInstance(this, m, inputIndex++)).ToArray();
                }
                else
                {
                    // Old school format
                    m_Inputs = (from m in Element.Elements("InputPin")
                                select new InputPinInstance(this, m, inputIndex++)).ToArray();
                }
            }
            else
            {
                m_Inputs = new InputPinInstance[0];
            }
            int outputIndex = 0;
            XElement outputPinRoot = Element.Element("OutputPin");
            if (outputPinRoot != null)
            {
                if (outputPinRoot.Elements("Instance").Count() > 0)
                {
                    m_Outputs = (from m in outputPinRoot.Elements("Instance")
                                 select new OutputPinInstance(this, m, outputIndex++)).ToArray();
                }
                else
                {
                    m_Outputs = (from m in Element.Elements("OutputPin")
                                 select new OutputPinInstance(this, m, outputIndex++)).ToArray();
                }
            }
            else
            {
                m_Outputs = new OutputPinInstance[0];
            }

            XElement fieldRoot = Element.Element("FieldValue");
            if (fieldRoot != null)
            {
                if (fieldRoot.Elements("Instance").Count() > 0)
                {
                    m_FieldValues = (from m in fieldRoot.Elements("Instance")
                                select float.Parse(m.Element("Value").Value)).ToArray();
                }
                else
                {
                    m_FieldValues = (from m in Element.Elements("FieldValue")
                                select float.Parse(m.Element("Value").Value)).ToArray();
                }
            }
            else
            {
                m_FieldValues = null;
            }
        }

        public ModuleInstance()
        {
            List<InputPinInstance> inputs = new List<InputPinInstance>();
            for (int i = 0; i < NumInputs; i++)
            {
                inputs.Add(new InputPinInstance(this, null, i));
            }
            m_Inputs = inputs.ToArray();

            List<OutputPinInstance> outputs = new List<OutputPinInstance>();
            for (int i = 0; i < NumOutputs; i++)
            {
                outputs.Add(new OutputPinInstance(this, null, i));
            }
            m_Outputs = outputs.ToArray();

            m_FieldValues = new float[NumFields];

            Element = null;
            ProcessingStage = -1;
        }

        public void SetupConnections(List<ModuleInstance> moduleInstances)
        {
            foreach(InputPinInstance input in Inputs)
            {
                input.SetupConnection(moduleInstances);
            }
        }

        public void CheckForInfiniteRecursion(ModuleInstance currentModuleInstance = null)
        {
            if (null != currentModuleInstance)
            {
                if (currentModuleInstance == this)
                {
                    throw new Exception("Circular link detected in module instance");
                }
            }
            else
            {
                // First module instance in localized graph
                currentModuleInstance = this;
            }

            foreach (var output in currentModuleInstance.Outputs)
            {
                foreach (var destination in output.OutputDestinations)
                {
                    this.CheckForInfiniteRecursion(destination.ParentModule);
                }
            }
        }

        public void ComputeProcessingStage(int currentStage)
        {
            if(ProcessingStage == -1 || ProcessingStage < currentStage)
            {
                ProcessingStage = currentStage;
                foreach(var input in Inputs)
                {
                    if(input.IsConnected)
                    {
                        input.OtherPin.ParentModule.ComputeProcessingStage(currentStage + 1);
                    }
                }
            }
        }

        public void AllocateBuffers(RuntimeContext runtimeContext)
        {
            // We need a buffer for each output destination
            for(int i = 0; i < Outputs.Length; i++)
            {
                if (Outputs[i].State == PinState.Dynamic)
                {
                    foreach (InputPinInstance destination in Outputs[i].OutputDestinations)
                    {
                        if (destination.ShouldSerialize)
                        {
                            destination.Buffer = AllocateBufferForOutput(runtimeContext, i, destination);
                        }
                    }
                }
                else
                {
                    // Register outputs can be shared between all destinations
                    // If this output is marked 'ShouldSerialize' then we need to allocate a destination regardless of
                    // whether or not it's connected
                    if (Outputs[i].ShouldSerialize)
                    {
                        int lastDependentStage = ProcessingStage;
                        foreach (InputPinInstance destination in Outputs[i].OutputDestinations)
                        {
                            if (destination.ShouldSerialize)
                            {
                                lastDependentStage = Math.Max(lastDependentStage, destination.ParentModule.ProcessingStage);
                            }
                        }

                        Register r = runtimeContext.RegisterPool.Allocate(ProcessingStage, lastDependentStage);
                        foreach (InputPinInstance destination in Outputs[i].OutputDestinations)
                        {
                            if (destination.ShouldSerialize)
                            {
                                destination.StaticInput = r;
                            }
                        }
                    }
                }            
            }
        }

        Buffer AllocateBufferForOutput(RuntimeContext runtimeContext, int outputIndex, InputPinInstance destination)
        {
            // See if we have any input buffers we can reuse
            foreach(InputPinInstance inputPin in Inputs)
            {
                // If we hold the only reference to this buffer we can grab it for our output
                if(inputPin.Buffer != null && inputPin.Buffer.FreeStage == ProcessingStage && inputPin.Buffer.GetNumReferences(ProcessingStage) == 0)
                {
                    // We can reuse this buffer
                    inputPin.Buffer.FreeStage = destination.ParentModule.ProcessingStage;
                    inputPin.Buffer.AddReference(ProcessingStage);
                    return inputPin.Buffer;
                }
            }
            return runtimeContext.BufferPool.AllocateBuffer(Outputs[outputIndex].InstanceFormat, ProcessingStage, destination.ParentModule.ProcessingStage);
        }

        public void AllocateConstants(RuntimeContext runtimeContext)
        {
            foreach(InputPinInstance pin in Inputs)
            {
                if(pin.ShouldSerialize && pin.HasStaticValue)
                {
                    pin.StaticInput = runtimeContext.ConstantAllocator.Allocate(pin.StaticValue);
                }
            }
        }

        
        public virtual void DebugPrint()
        {        
            Console.Write("{1}{0}: ", Instruction, ShouldSerialize ? "" : "(skipping) ");
        
            foreach(InputPinInstance input in Inputs)
            {
                if(ShouldSerializeInput(input.Index))
                {
                    if (input.IsConnected && input.Buffer != null)
                    {
                        Console.Write("B{0},", input.Buffer.BufferId);
                    }
                    else
                    {
                        Console.Write("{0},", input.StaticInput);
                    }
                }
            }

            Console.Write(" => ");
            foreach(OutputPinInstance output in Outputs)
            {
                if(output.ShouldSerialize)
                {
                    foreach (InputPinInstance destination in output.OutputDestinations)
                    {
                        if (destination.Buffer != null)
                        {
                            Console.Write("B{0},", destination.Buffer.BufferId);
                        }
                        else
                        {
                            Console.Write("{0},", destination.StaticInput);
                        }
                    }
                }
            }

            Console.WriteLine();
        }      
 
        public virtual XElement Serialize()
        {
            string operation = Instruction.ToString();

            XElement node = new XElement(operation);

            XElement inputs = new XElement("Inputs");
            node.Add(inputs);

            foreach (InputPinInstance input in Inputs)
            {
                if (ShouldSerializeInput(input.Index))
                {
                    if (input.IsConnected && input.Buffer != null)
                    {
                        inputs.Add(new XElement("Buffer", new XAttribute("id",input.Buffer.BufferId)));
                    }
                    else
                    {
                        inputs.Add(new XElement("Static", new XAttribute("id", input.StaticInput.ToString())));
                    }
                }
            }

            XElement outputs = new XElement("Outputs");
            node.Add(outputs);
            foreach (OutputPinInstance output in Outputs)
            {
                if (output.ShouldSerialize && output.OutputDestinations.Count() > 0)
                {
                    // We don't support multiple destinations per output at this stage.
                    var destination = output.OutputDestinations.First();
                    if (destination.ShouldSerialize)
                    {
                        if (destination.Buffer != null)
                        {
                            outputs.Add(new XElement("Buffer", new XAttribute("id", destination.Buffer.BufferId)));
                        }
                        else
                        {
                            outputs.Add(new XElement("Static", new XAttribute("id", destination.StaticInput.ToString())));
                        }
                    }
                }
            }
            return node;
        }

        protected void ImplementAs_ScaleOutput(int inputIndex, SynthGraph graph)
        {
            // No need to do anything if the scaling value is 1.0
            if (Inputs[inputIndex].IsConnected || Inputs[inputIndex].StaticValue != 1.0f)
            {
                ModuleInstance scaleModule = graph.CreateModuleInstance<Multiplier>();

                // Connect the new module between the output of this module instance and all of it's destinations
                foreach (OutputPinInstance outputPin in Outputs)
                {
                    List<InputPinInstance> outputDestinations = outputPin.OutputDestinations.ToList();
                    foreach (InputPinInstance destinationInputPin in outputDestinations)
                    {
                        destinationInputPin.Disconnect();
                        destinationInputPin.Connect(scaleModule.Outputs[0]);
                    }
                }
                scaleModule.Inputs[0].Connect(Outputs[0]);
                
                // Connect gain input pin to scaler input
                if (Inputs[inputIndex].IsConnected)
                {
                    scaleModule.Inputs[1].Connect(Inputs[inputIndex].OtherPin);
                    Inputs[inputIndex].Disconnect();
                }
                else
                {
                    scaleModule.Inputs[1].StaticValue = Inputs[inputIndex].StaticValue;
                }
                scaleModule.Inputs[1].InstanceFormat = PinFormat.Normalized;
            }
        }

        protected void ImplementAs_LerpInput(int inputIndex, SynthGraph graph, float scaleMin, float scaleMax)
        {
            if (scaleMin == 0.0f)
            {
                // If min=0 then we need a scale operation (input*max)
                // if max=1 then we can pass input straight through.
                if (scaleMax != 1.0f)
                {
                    ModuleInstance scaleModule = graph.CreateModuleInstance<Multiplier>();
                    // Connect new module between input source and input pin
                    if (Inputs[inputIndex].IsConnected)
                    {
                        scaleModule.Inputs[0].Connect(Inputs[inputIndex].OtherPin);
                        Inputs[inputIndex].Disconnect();
                    }
                    else
                    {
                        scaleModule.Inputs[0].StaticValue = Inputs[inputIndex].StaticValue;
                        scaleModule.Inputs[0].InstanceFormat = Inputs[inputIndex].InstanceFormat;
                    }
                    Inputs[inputIndex].Connect(scaleModule.Outputs[0]);
                    scaleModule.Inputs[1].StaticValue = scaleMax;
                }
            }
            else if (scaleMin != 0.0f || scaleMax != 1.0f)
            {
                ModuleInstance lerpModule = graph.CreateModuleInstance<Lerp>();
                // Connect new module between input source and input pin
                if (Inputs[inputIndex].IsConnected)
                {
                    lerpModule.Inputs[0].Connect(Inputs[inputIndex].OtherPin);
                    Inputs[inputIndex].Disconnect();
                }
                else
                {
                    lerpModule.Inputs[0].StaticValue = Inputs[inputIndex].StaticValue;
                    lerpModule.Inputs[0].InstanceFormat = Inputs[inputIndex].InstanceFormat;
                }
                Inputs[inputIndex].Connect(lerpModule.Outputs[0]);
                lerpModule.Inputs[1].StaticValue = scaleMin;
                lerpModule.Inputs[2].StaticValue = scaleMax;
            }
        }

        protected static void RewireInput(InputPinInstance newInput, InputPinInstance oldInput)
        {
            if (oldInput.IsConnected)
            {
                newInput.Connect(oldInput.OtherPin);
                oldInput.Disconnect();
            }
            else
            {
                newInput.StaticValue = oldInput.StaticValue;
                newInput.InstanceFormat = oldInput.Format;
            }
        }

        protected static void DuplicateInput(InputPinInstance newInput, InputPinInstance oldInput)
        {
            if (oldInput.IsConnected)
            {
                newInput.Connect(oldInput.OtherPin);
            }
            else
            {
                newInput.StaticValue = oldInput.StaticValue;
                newInput.InstanceFormat = oldInput.Format;
            }
        }

        protected static void RewireOutputs(OutputPinInstance newOutput, OutputPinInstance oldOutput)
        {
            var destinations = oldOutput.OutputDestinations.ToList();
            foreach (var d in destinations)
            {
                d.Disconnect();
                d.Connect(newOutput);
            }
        }

        protected static void PushStaticValueToDestinations(double value, List<InputPinInstance> destinationList, PinFormat format)
        {
            InputPinInstance[] destinations = destinationList.ToArray();

            foreach (InputPinInstance destination in destinations)
            {
                destination.Disconnect();
                destination.StaticValue = (float)value;
                destination.InstanceFormat = format;
            }
        }
    }
}
