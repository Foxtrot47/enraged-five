﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.IO;

using rage;
using rage.ToolLib.Logging;
using rage.Transform;

namespace rage.AMPCompiler
{
    [CanTransform("ModularSynth")]
    public class AMPTransformer : ITransformer
    {
        ModuleInstanceFactory m_InstanceFactory;

        public bool Init(ILog log, params string[] args)
        {
            log.Information("AMPTransformer init");
            m_InstanceFactory = new ModuleInstanceFactory(log);
            return true;
        }

        public TransformResult Transform(ILog log, XElement element)
        {
            TransformResult result = new TransformResult{Result = null};
            
            string name = element.Attribute("name").Value;

            XElement exportElement = element.Element("ExportForGame");
            if (exportElement == null || exportElement.Value != "yes")
            {
                return result;
            }
            try
            {
                SynthGraph graph = new SynthGraph(element, m_InstanceFactory);

                graph.RemoveDebugOnlyModules();
                graph.RemoveDisconnectedModules();

                // Iterate until stable
                int numModulesLastTime = 0;
                bool finished = false;
                do
                {
                    numModulesLastTime = graph.ModuleInstances.Count;
                    graph.InsertConversionModules();
                    graph.InsertSampleModules();
                    graph.ExpandBuiltInImplementations();
                    graph.EvaluateOffline();                    
                    
                    finished = (graph.ModuleInstances.Count == numModulesLastTime);
                    graph.RemoveDisconnectedModules();
                    finished &= (graph.ModuleInstances.Count == numModulesLastTime);

                } while (!finished);

                graph.InsertCopyModules();

                graph.ComputeProcessingStages();
                graph.AllocateBuffersAndRegisters();
                graph.AllocateConstants();

                graph.FinalOptimisation();

                //graph.DebugPrint();                

                XElement outputXml = graph.Serialize();

                result.Result = outputXml;
            }
            catch (System.Exception ex)
            {
                log.Exception(ex);
            }
            
            return result;
        }

        public void Shutdown()
        {
           
        }
    }
}
