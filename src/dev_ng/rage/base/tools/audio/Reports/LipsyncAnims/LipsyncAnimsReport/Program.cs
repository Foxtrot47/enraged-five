﻿namespace LipsyncAnimsReport
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml.Linq;

    using Rockstar.AssetManager.Infrastructure.Enums;
    using Rockstar.AssetManager.Interfaces;
    using Rockstar.AssetManager.Main;

    public class Program
    {
        public static void Main(string[] args)
        {
            var packs = new List<string>
            {
                "SS_AC",
                "SS_DE",
                "SS_FF",
                "SS_GM",
                "SS_NP",
                "SS_QR",
                "SS_ST",
                "SS_UZ",
            };

            var lipsyncAnimRoot = @"X:\gta5\audio\dev\assets\LipsyncAnims\";
            var builtWavesRoot = @"X:\gta5\audio\dev\build\ps3\BuiltWaves\";
            var tempPath = Path.GetTempPath();

            Console.WriteLine("Enter P4 username:");
            var p4User = Console.ReadLine();

            Console.WriteLine("Enter P4 password:");
            var p4Password = Console.ReadLine();

            Console.WriteLine("Enter P4 workspace:");
            var p4Workspace = Console.ReadLine();

            IAssetManager assetManager;

            try
            {
                assetManager = AssetManagerFactory.GetInstance(
                    AssetManagerType.Perforce, null, "rsgedip4s1:1666", p4Workspace, p4User, p4Password, "depot//");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed to initialize asset manager: {0}", ex.Message);
                Console.ReadKey();
                return;
            }

            foreach (var pack in packs)
            {
                Console.WriteLine("Processing pack {0}...", pack);

                var lipsyncAnimPath = Path.Combine(lipsyncAnimRoot, pack);
                var builtWavesPath = builtWavesRoot + pack + "_PACK_FILE.xml";
                var outputPath = Path.Combine(tempPath, pack + "_lipsync_anims.csv");

                var writer = new StreamWriter(outputPath);

                var doc = XDocument.Load(builtWavesPath);

                foreach (var waveElem in doc.Descendants("Wave"))
                {
                    var found = false;
                    foreach (var chunkElem in waveElem.Descendants("Chunk"))
                    {
                        if (chunkElem.Attribute("name").Value == "LIPSYNC")
                        {
                            found = true;
                        }
                    }

                    if (!found)
                    {
                        continue;
                    }

                    var bankElem = waveElem.Ancestors("Bank").First();
                    var timestampStr = bankElem.Attribute("timeStamp").Value;
                    var bankBuilt = DateTime.Parse(timestampStr);

                    var fileNameNoExt = Path.GetFileNameWithoutExtension(waveElem.Attribute("name").Value);

                    var wavePath = GetPath(waveElem);
                    var animPath = Path.Combine(lipsyncAnimPath, wavePath, fileNameNoExt);
                    var ccdFilePath = animPath + ".ccd";
                    var wcdFilePath = animPath + ".wcd";
                    var xcdFilePath = animPath + ".xcd";

                    var valid = true;

                    if (!File.Exists(ccdFilePath) || !File.Exists(wcdFilePath) || !File.Exists(xcdFilePath))
                    {
                        valid = false;
                    }
                    else
                    {
                        var ccdFileStatus = assetManager.GetFileStatus(ccdFilePath);
                        if (ccdFileStatus.LastModTime > bankBuilt)
                        {
                            valid = false;
                        }
                    }

                    if (!valid)
                    {
                        writer.WriteLine(Path.Combine(wavePath, waveElem.Attribute("name").Value));
                    }
                }

                writer.Close();

                Console.WriteLine("Saved output to file {0}", outputPath);
            }

            Console.WriteLine("Complete!");
            Console.ReadKey();
        }

        private static string GetPath(XElement elem)
        {
            var path = string.Empty;
            var currElem = elem.Parent;
            while (currElem != null)
            {
                if (currElem.Name == "Pack")
                {
                    break;
                }

                var name = currElem.Attribute("name").Value;

                if (!string.IsNullOrEmpty(path))
                {
                    path = "\\" + path;
                }

                path = name + path;

                currElem = currElem.Parent;
            }

            return path;
        }
    }
}
