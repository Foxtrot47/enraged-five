﻿#region

using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using LoudnessReport;

#endregion

namespace MusicLoudnessReport
{
    public class ReportMailer
    {
        public void SendMessage(LoudnessReport.LoudnessReport loudnessReport, List<string> recieptlist)
        {
            MailMessage mailMessage = new MailMessage();
            mailMessage.Subject = "Music Loudness Report";

            mailMessage.From = new MailAddress("loudnessReporter@rockstarnorth.com", "Loudness Reporter");

            foreach (string recieptiant in recieptlist)
            {
                mailMessage.To.Add(recieptiant);
            }
            mailMessage.ReplyToList.Add("Danjeli.Schembri@rockstarnorth.com");
            mailMessage.ReplyToList.Add("Alastair.MacGregor@rockstarnorth.com");

            //Attachments
            ContentType musicLoudness = new ContentType();
            musicLoudness.MediaType = MediaTypeNames.Text.Plain;
            string dateToday = DateTime.Now.Date.ToString("d");
            musicLoudness.Name = "Music_Loudness_" + dateToday + ".csv";
            mailMessage.Attachments.Add(new Attachment(GenerateStreamFromString(loudnessReport.LoudnessCsv),
                musicLoudness));


            mailMessage.IsBodyHtml = true;
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendLine(string.Format(@"<h1>Music Loudness Report - {0}</h1>", dateToday));
            stringBuilder.AppendLine(
                @"<p>Please find attached csv file containing all the loudness data&nbsp;for music:</p>");
            stringBuilder.AppendLine(@"<h2>Report Results</h2>");

            stringBuilder.AppendLine(string.Format(@"<h3>{0}</h3>", "Music Loudness Summary"));
            stringBuilder.AppendLine(string.Format(@"<p>The loudness range for is {0:n} LU</p>",
                loudnessReport.Range));
            if (loudnessReport.LoudestWaves != null && loudnessReport.QuitestWaves != null)
            {
                stringBuilder.AppendLine(@"<h3>Loudest Waves</h3>");

                stringBuilder.AppendLine(
                    @"<table border=""1"" cellpadding=""1"" cellspacing=""1""><thead><tr><th scope=""col"" style=""text-align: center;"">Wave Name</th><th scope=""col"" style=""text-align: center;"">Bank Name</th><th scope=""col"" style=""text-align: center;"">Loudness in LUFS</th></tr></thead><tbody>");
                foreach (Tuple<Wave, double> loudness in loudnessReport.LoudestWaves)
                {
                    stringBuilder.AppendLine(
                        string.Format(
                            @"<tr><td style=""text-align: center;"">{0}</td><td style=""text-align: center;"">{1}</td><td style=""text-align: center;"">{2:N}</td></tr>",
                            loudness.Item1.WaveName, loudness.Item1.BankName, loudness.Item2));
                }
                stringBuilder.AppendLine(@"</table>");

                stringBuilder.AppendLine(@"<h3>Quietest Waves</h3>");
                stringBuilder.AppendLine(
                    @"<table border=""1"" cellpadding=""1"" cellspacing=""1""><thead><tr><th scope=""col"" style=""text-align: center;"">Wave Name</th><th scope=""col"" style=""text-align: center;"">Bank Name</th><th scope=""col"" style=""text-align: center;"">Loudness in LUFS</th></tr></thead><tbody>");
                foreach (Tuple<Wave, double> loudness in loudnessReport.QuitestWaves)
                {
                    stringBuilder.AppendLine(
                        string.Format(
                            @"<tr><td style=""text-align: center;"">{0}</td><td style=""text-align: center;"">{1}</td><td style=""text-align: center;"">{2:N}</td></tr>",
                            loudness.Item1.WaveName, loudness.Item1.BankName, loudness.Item2));
                }
                stringBuilder.AppendLine(@"</table>");
            }
            stringBuilder.AppendLine(@"</tbody></table></td></tr></tbody></table>");


            mailMessage.Body = stringBuilder.ToString();

            SmtpClient client = new SmtpClient("smtp.rockstar.t2.corp")
            {
                Credentials = CredentialCache.DefaultNetworkCredentials
            };
            client.Send(mailMessage);
        }

        public static void ErrorMessageSender(Exception exception)
        {
            MailMessage mailMessage = new MailMessage();
            mailMessage.Subject = "Loudness Report Error";
            mailMessage.From = new MailAddress("loudnessreporterror@rockstarnorth.com");
            mailMessage.To.Add("Danjeli.schembri@rockstarnorth.com");
            mailMessage.To.Add("ben.durrenberger@rockstarnorth.com");
            mailMessage.To.Add("Alastair.MacGregor@rockstarnorth.com");

            mailMessage.Body = string.Format("Error message: {0} \n\n Error source: {1} \n\n Error Stack trace: {2} ",
                exception.Message, exception.Source, exception.StackTrace);

            File.WriteAllText(Path.GetTempFileName() + "log.txt", mailMessage.Body);
            SmtpClient client = new SmtpClient("smtp.rockstar.t2.corp")
            {
                Credentials = CredentialCache.DefaultNetworkCredentials
            };
            client.Send(mailMessage);
        }

        public MemoryStream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }
    }
}