﻿namespace LoudnessReport
{
    public class Wave
    {
        public string WaveName, BankName;

        public Wave(string wavename, string bankname)
        {
            WaveName = wavename;
            BankName = bankname;
        }
    }
}