﻿#region

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using LoudnessReport;
using rage.ToolLib.CmdLine;

#endregion

namespace MusicLoudnessReport
{
    internal class Program : CommandLineConfiguration
    {
        [Description("Built waves location")]
        [OptionalParameter]
        [DefaultValue(@"//rdr3/audio/dev/build/pc/BuiltWaves/")]
        public string BuiltWaves { get; set; }

        [Description("Perforce username")]
        [OptionalParameter]
        public string UserName { get; set; }

        [Description("Perforce password")]
        [OptionalParameter]
        public string Password { get; set; }

        [Description("Perforce Client")]
        [OptionalParameter]
        public string P4Client { get; set; }

        [Description("Perforce Server")]
        [OptionalParameter]
        public string P4Server { get; set; }

        public Program(string[] args)
            : base(args)
        {
            LoudnessReportGenerator loudnessReportGenerator = new LoudnessReportGenerator();
            loudnessReportGenerator.BuiltWavesLocation = BuiltWaves;
            loudnessReportGenerator.ConnectionSettings = new PerforceConnection
            {
                P4Client = P4Client,
                P4Server = P4Server,
                Password = Password,
                UserName = UserName
            };
            loudnessReportGenerator.GetLatest();
            LoudnessReport.LoudnessReport loudnessReport = loudnessReportGenerator.GenerateReport(new[] {"MUSIC_"});
            ReportMailer reportMailer = new ReportMailer();
            List<string> recieptlist =
                File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Receiptiants.txt"))
                    .Split(new[] {Environment.NewLine, ","}, StringSplitOptions.None)
                    .ToList();
            reportMailer.SendMessage(loudnessReport, recieptlist);
        }

        private static void Main(string[] args)
        {
            Program config;
            try
            {
                config = new Program(args);
            }
            catch (ParameterException ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.Usage);
            }
        }
    }
}