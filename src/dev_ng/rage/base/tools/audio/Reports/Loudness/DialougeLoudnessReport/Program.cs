﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LoudnessReport;
using rage.ToolLib.CmdLine;

namespace DialougeLoudnessReport
{
    public class Program : CommandLineConfiguration
    {
        [Description("Built waves location")]
        [CommandLineConfiguration.OptionalParameter]
        [DefaultValue(@"//rdr3/audio/dev/build/pc/BuiltWaves/")]
        public string BuiltWaves { get; set; }

        [Description("Perforce username")]
        [CommandLineConfiguration.OptionalParameterAttribute]
        public string UserName { get; set; }

        [Description("Perforce password")]
        [CommandLineConfiguration.OptionalParameterAttribute]
        public string Password { get; set; }

        [Description("Perforce Client")]
        [CommandLineConfiguration.OptionalParameterAttribute]
        public string P4Client { get; set; }


        [Description("Perforce Server")]
        [CommandLineConfiguration.OptionalParameterAttribute]
        public string P4Server { get; set; }

        static void Main(string[] args)
        {
            Program config;
            try
            {
                config = new Program(args);
            }
            catch (rage.ToolLib.CmdLine.CommandLineConfiguration.ParameterException ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.Usage);
            }

        }

        public Program(string[] args)
            : base(args)
        {
            LoudnessReportGenerator loudnessReportGenerator = new LoudnessReportGenerator();
            loudnessReportGenerator.BuiltWavesLocation = BuiltWaves;
            loudnessReportGenerator.ConnectionSettings = new PerforceConnection
            {
                P4Client = P4Client,
                P4Server = P4Server,
                Password = Password,
                UserName = UserName
            };
            Dictionary<string, LoudnessReport.LoudnessReport> loudnessReports =
                new Dictionary<string, LoudnessReport.LoudnessReport>();
            loudnessReportGenerator.GetLatest();
            loudnessReports["Ambient Speech"] = loudnessReportGenerator.GenerateReport(new[] { "S_Misc", "PEDS", "S_RE" });
            loudnessReports["Scripted Speech"] = loudnessReportGenerator.GenerateReport(new[] { "SS_" });
            loudnessReports["Pain"] = loudnessReportGenerator.GenerateReport(new[] { "PAIN_S" });
            ReportMailer reportMailer = new ReportMailer();
            List<string> recieptlist = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Receiptiants.txt")).Split(new string[] { Environment.NewLine, "," }, StringSplitOptions.None).ToList();
            reportMailer.SendMessage(loudnessReports, recieptlist);

        }
    }
}
