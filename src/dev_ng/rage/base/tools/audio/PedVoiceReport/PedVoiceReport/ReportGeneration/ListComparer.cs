﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion

namespace PedVoiceReport.ReportGeneration
{
    public class ListComparer
    {
        public static Dictionary<string, string> ExcelRaceToRaveRace = new Dictionary<string, string>
        {
            {"Caucasian", "White"},
            {"African American", "Black"},
            {"Asian", "Chinese"},
            {"Hispanic", "Latino"},
            {"Native American", "NativeAmerican"}
        };

        public HashSet<string> GenerateWarnings(PedInfoParser report)
        {

            List<R2PVG> raveR2Pvgs = report.R2PvgList;
            Dictionary<string, List<PedVoiceRow>> pedVoiceExcelDictionary = report.PedVoiceMasterExcel;

            HashSet<string> stringBuilder = new HashSet<string>();

            foreach (string pedVoiceName in pedVoiceExcelDictionary.Keys)
            {

                if (string.IsNullOrEmpty(pedVoiceExcelDictionary[pedVoiceName][0].Ethnicity))
                {
                    // this is a story character
                    if (!report.PvgList.Select(p => p.Name.Replace("_PVG", string.Empty)).Contains(pedVoiceName))
                    {
                        stringBuilder.Add(
                            string.Format(
                                "{0} Does not have a PVG associated with it in rave.",
                                pedVoiceName));
                        continue;
                    }

                }

                IEnumerable<string> nameList = raveR2Pvgs.Select(p => p.Name.Replace("_R2PVG", string.Empty).ToUpper());

                if (nameList.Contains(pedVoiceName) && pedVoiceExcelDictionary[pedVoiceName].Any(p => p.IsDeleted))
                {
                    stringBuilder.Add(string.Format("{0} is struck off in the ped master voicelist excel sheet, but it has an R2PVG attached to it. Please verify.", pedVoiceName));
                    continue;
                }
                
                if (pedVoiceExcelDictionary[pedVoiceName].Any(p => p.IsDeleted))
                {
                    continue;
                }

                if (!nameList.Contains(pedVoiceName) && !pedVoiceExcelDictionary[pedVoiceName].Any(p => p.IsDeleted))
                {
                    if (
                        !pedVoiceExcelDictionary[pedVoiceName].Select(p => p.SecondaryVoiceName)
                            .Where(p => p != null)
                            .Any(p => p.Equals("POOL VOICE", StringComparison.InvariantCultureIgnoreCase)))
                        stringBuilder.Add(string.Format("{0} is not set in rave as a PedRaceToPVG", pedVoiceName));

                    continue;
                }


                R2PVG raveR2Pvg = raveR2Pvgs.First(p => p.Name.Replace("_R2PVG", string.Empty).ToUpper() == pedVoiceName);
                List<PedVoiceRow> pedVoiceRows = pedVoiceExcelDictionary[pedVoiceName];

                foreach (PedVoiceRow pedVoiceRow in pedVoiceRows)
                {
                    if (!ExcelRaceToRaveRace.ContainsKey(pedVoiceRow.Ethnicity) || string.IsNullOrEmpty(pedVoiceRow.Ethnicity))
                    {
                        stringBuilder.Add(string.Format("In ped voicelist excel sheet: {0} has ethnicity set to \"{1}\" which couldn't be read by the report.", pedVoiceRow.PrimaryVoiceName,
                            pedVoiceRow.Ethnicity));
                        continue;
                    }

                    if (!raveR2Pvg.Race2Voice.Keys.Contains(ExcelRaceToRaveRace[pedVoiceRow.Ethnicity].ToUpper()) || raveR2Pvg.Race2Voice[ExcelRaceToRaveRace[pedVoiceRow.Ethnicity].ToUpper()] == null)
                    {
                        stringBuilder.Add(string.Format("{0} should contain race: {1} in rave", raveR2Pvg.Name,
                            pedVoiceRow.Ethnicity));
                        continue;
                    }

                    PedVoiceGroups pvg = raveR2Pvg.Race2Voice[ExcelRaceToRaveRace[pedVoiceRow.Ethnicity].ToUpper()];

                    if (pedVoiceRow.VoiceType == VoiceType.Primary)
                    {
                        if (pvg.PrimaryVoices == null || (!string.IsNullOrEmpty(pedVoiceRow.PrimaryVoiceName) &&
                                                          !pvg.PrimaryVoices.Select(p => p.Name.ToUpper())
                                                              .Contains(pedVoiceRow.PrimaryVoiceName.ToUpper())))
                        {
                            stringBuilder.Add(string.Format("{0} should contain primary voice: {1} in rave",
                                pvg.Name,
                                pedVoiceRow.PrimaryVoiceName));
                        }
                    }

                    if (pedVoiceRow.VoiceType == VoiceType.Mini)
                    {
                        if (pvg.SecodaryVoices == null || (!string.IsNullOrEmpty(pedVoiceRow.PrimaryVoiceName) &&
                                                           !pvg.SecodaryVoices.Select(p => p.Name.ToUpper())
                                                               .Contains(pedVoiceRow.PrimaryVoiceName.ToUpper())))
                        {
                            stringBuilder.Add(string.Format("{0} should contain mini voice: {1} in rave",
                                pvg.Name,
                                pedVoiceRow.PrimaryVoiceName));
                        }
                    }

                    if (!string.IsNullOrEmpty(pedVoiceRow.SecondaryVoiceName) &&
                        !(pvg.PoolVoices.Contains(pedVoiceRow.SecondaryVoiceName,
                            StringComparer.InvariantCultureIgnoreCase) ||
                          pvg.PoolVoices.Contains(pedVoiceRow.SecondaryVoiceName + "_PVG",
                              StringComparer.InvariantCultureIgnoreCase)))
                    {
                        stringBuilder.Add(string.Format("{0} should contain Pool: {1} in rave", pvg.Name,
                            pedVoiceRow.SecondaryVoiceName));
                    }
                }
            }


            return stringBuilder;
        }
    }
}