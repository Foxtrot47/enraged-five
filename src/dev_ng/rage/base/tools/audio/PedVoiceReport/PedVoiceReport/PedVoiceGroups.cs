﻿using System.Collections.Generic;

namespace PedVoiceReport
{
    public class PedVoiceGroups
    {
        public string Name;
        public List<PedVoice> PrimaryVoices = new List<PedVoice>();
        public List<PedVoice> SecodaryVoices = new List<PedVoice>();
        public List<string> PoolVoices = new List<string>();

      }
}