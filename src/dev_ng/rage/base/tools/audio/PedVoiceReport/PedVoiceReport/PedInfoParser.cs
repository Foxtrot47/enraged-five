﻿#region

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using Excel;

#endregion

namespace PedVoiceReport
{
    public class PedInfoParser
    {
        private readonly string _builtwavesXmlLocation;
        private readonly string _gameObjectsLocation;
        private readonly string _pedVoiceMasterList;

        private readonly Dictionary<string, List<XElement>> _xmlElementDictionary =
            new Dictionary<string, List<XElement>>
            {
                {"PEDVOICEGROUPS", new List<XElement>()},
                {"PEDRACETOPVG", new List<XElement>()},
                {"PEDVOICE", new List<XElement>()}
            };

        public HashSet<string> DeletedVoicesStillInRave;
        public int NumberOfHookedUpPeds;
        public List<PedVoice> PedVoiceList = new List<PedVoice>();
        public Dictionary<string, List<PedVoiceRow>> PedVoiceMasterExcel;
        public HashSet<string> PedVoicesInRaveWaves;
        public List<PedVoiceGroups> PvgList = new List<PedVoiceGroups>();
        public List<R2PVG> R2PvgList = new List<R2PVG>();

        public PedInfoParser(string pedVoiceMasterList, string gameObjectsLocation, string builtwavesXmlLocation)
        {
            _pedVoiceMasterList = pedVoiceMasterList;
            _gameObjectsLocation = gameObjectsLocation;
            _builtwavesXmlLocation = builtwavesXmlLocation;
            PedVoiceMasterExcel = new Dictionary<string, List<PedVoiceRow>>();
        }

        public void ParsePedVoiceMasterList()
        {
            List<Worksheet> worksheets = Workbook.Worksheets(_pedVoiceMasterList).ToList();

            Worksheet pedVoices2 = worksheets[1];
            foreach (Row row in pedVoices2.Rows)
            {
                if (row.Cells[0] != null && !string.IsNullOrEmpty(row.Cells[0].Text))
                {
                    PedVoiceMasterExcel[row.Cells[0].Text] = new List<PedVoiceRow>
                    {
                        new PedVoiceRow
                        {
                            PrimaryVoiceName = row.Cells[0].Text,
                            Ethnicity = null,
                            VoiceType = VoiceType.Primary
                        }
                    };
                }
            }

            Worksheet pedVoices = worksheets.FirstOrDefault();
            foreach (Row row in pedVoices.Rows)
            {
                PedVoiceRow pvr = new PedVoiceRow();
                pvr.PrimaryVoiceName = row.Cells[0].Text;
                if (row.Cells[4] != null && !string.IsNullOrEmpty(row.Cells[4].Text))
                    pvr.SecondaryVoiceName = row.Cells[4].Text;
                if (row.Cells[5] != null && !string.IsNullOrEmpty(row.Cells[5].Text))
                    pvr.Ethnicity = row.Cells[5].Text;

                if (row.Cells[3] != null && !row.Cells[3].Text.ToUpper().Equals("FULL"))
                {
                    pvr.VoiceType = VoiceType.Mini;
                }
                else
                {
                    pvr.VoiceType = VoiceType.Primary;
                }
                if (!string.IsNullOrWhiteSpace(row.Cells[0].StyleNumber))
                {
                    int style = 0;
                    int.TryParse(row.Cells[0].StyleNumber, out style);
                    pvr.IsDeleted =
                        Workbook.StyleSheet.fonts[int.Parse(Workbook.StyleSheet.CellXfs[style].fontId)].Strike;
                }
                string pedName = string.Empty;
                try
                {
                    pedName = pvr.PrimaryVoiceName.Substring(5, pvr.PrimaryVoiceName.Length - 5);
                }
                catch (Exception)
                {
                    continue;
                }


                if (!pedName.Any(char.IsDigit))
                {
                    continue;
                }
                string race = Regex.Match(pedName, "([A-Z]+_[0-9][0-9]$)").Value;

                if (!string.IsNullOrEmpty(race) && pedName.Contains(race))
                {
                    pedName = pedName.Replace(race, string.Empty).TrimEnd('_').ToUpper();
                }
                else
                {
                    pedName = pedName.ToUpper();
                }
                if (
                    !PedVoiceMasterExcel.ContainsKey(pedName))
                {
                    PedVoiceMasterExcel[
                        pedName] = new List<PedVoiceRow>();
                }

                PedVoiceMasterExcel[pedName].Add(pvr);
            }
        }

        private void PopulateXmlDictionary()
        {
            foreach (string file in Directory.GetFiles(_gameObjectsLocation, "*.xml", SearchOption.AllDirectories))
            {
                XDocument xdoc = XDocument.Load(file);
                foreach (XElement xElement in xdoc.Descendants())
                {
                    string objectType = xElement.Name.LocalName.ToUpper();
                    if (_xmlElementDictionary.ContainsKey(objectType))
                    {
                        _xmlElementDictionary[objectType].Add(xElement);
                    }
                }
            }
        }

        private Tuple<int, int> GetNumberOfWaves(string pedVoice, List<XDocument> xdocs)
        {
            foreach (XDocument xdoc in xdocs)
            {
                XElement bank =
                    xdoc.Descendants("Bank")
                        .FirstOrDefault(
                            p => p.Attribute("name").Value.Equals(pedVoice, StringComparison.InvariantCultureIgnoreCase));

                if (bank != null)
                {
                    return new Tuple<int, int>(bank.Descendants("Wave").Count(),
                        bank.Descendants("Wave").Distinct(new ContextCompare()).Count());
                }
            }
            return new Tuple<int, int>(-1, -1);
        }

        private HashSet<string> GetVoicesInRaveWaves(string[] primaryVoices, List<XDocument> xdocs)
        {
            List<string> banks = new List<string>();
            foreach (XDocument xdoc in xdocs)
            {
                banks.AddRange(
                    xdoc.Descendants("Bank")
                        .Where(
                            p =>
                                primaryVoices.Contains(p.Attribute("name").Value,
                                    StringComparer.InvariantCultureIgnoreCase) &&
                                p.Element("Wave") != null).Select(element => element.Attribute("name").Value));
            }
            HashSet<string> hashset = new HashSet<string>(banks);
            return hashset;
        }

        public void ParseGameObjects()
        {
            PopulateXmlDictionary();
            //Load xdocs for list of whole files;
            List<XDocument> xdocs =
                Directory.GetFiles(_builtwavesXmlLocation, "*.xml", SearchOption.AllDirectories)
                    .Select(XDocument.Load)
                    .ToList();

            foreach (XElement xElement in _xmlElementDictionary["PEDVOICE"])
            {
                PedVoice pedVoice = new PedVoice
                {
                    Name = xElement.Attribute(XName.Get("name")).Value
                };
                Tuple<int, int> numbers = GetNumberOfWaves(pedVoice.Name, xdocs);
                pedVoice.NumberOfWaves = numbers.Item1;
                pedVoice.NumberOfContexts = numbers.Item2;
                PedVoiceList.Add(pedVoice);
            }

            PedVoicesInRaveWaves =
                GetVoicesInRaveWaves(
                    PedVoiceMasterExcel.Values.SelectMany(p => p).Select(p => p.PrimaryVoiceName).ToArray(), xdocs);

            string[] deletedVoices = PedVoiceMasterExcel.Values.SelectMany(p => p)
                .Where(p => p.IsDeleted)
                .Select(p => p.PrimaryVoiceName)
                .ToArray();
            DeletedVoicesStillInRave =
                GetVoicesInRaveWaves(deletedVoices, xdocs);
            foreach (XElement xElement in _xmlElementDictionary["PEDVOICEGROUPS"])
            {
                PedVoiceGroups pvg = new PedVoiceGroups
                {
                    Name = xElement.Attribute(XName.Get("name")).Value
                };
                XElement primaryVoices = xElement.Descendants(XName.Get("PrimaryVoice")).FirstOrDefault();

                if (primaryVoices != null)
                {
                    pvg.PrimaryVoices = new List<PedVoice>();
                    IEnumerable<string> voices = primaryVoices.Descendants().Select(p => p.Value);
                    foreach (string voice in voices)
                    {
                        pvg.PrimaryVoices.Add(
                            PedVoiceList.FirstOrDefault(
                                p => p.Name.Equals(voice, StringComparison.InvariantCultureIgnoreCase)));
                    }
                }

                XElement secondaryVoices = xElement.Descendants(XName.Get("MiniVoice")).FirstOrDefault();
                if (secondaryVoices != null)
                {
                    pvg.SecodaryVoices = new List<PedVoice>();
                    IEnumerable<string> svoices = secondaryVoices.Descendants().Select(p => p.Value);
                    foreach (string voice in svoices)
                    {
                        pvg.SecodaryVoices.Add(
                            PedVoiceList.FirstOrDefault(
                                p => p.Name.Equals(voice, StringComparison.InvariantCultureIgnoreCase)));
                    }
                }


                XElement poolPvg = xElement.Descendants(XName.Get("PoolPVG")).FirstOrDefault();
                if (poolPvg != null)
                {
                    pvg.PoolVoices = new List<string>();
                    IEnumerable<string> ppvg = poolPvg.Descendants().Select(p => p.Value);
                    pvg.PoolVoices = ppvg.ToList();
                }

                PvgList.Add(pvg);
            }

            foreach (XElement xElement in _xmlElementDictionary["PEDRACETOPVG"])
            {
                R2PVG r2pvg = new R2PVG {Name = xElement.Attribute(XName.Get("name")).Value};
                foreach (XElement descendant in xElement.Descendants())
                {
                    r2pvg.Race2Voice[descendant.Name.ToString().ToUpper()] =
                        PvgList.FirstOrDefault(
                            p => p.Name.Equals(descendant.Value, StringComparison.InvariantCultureIgnoreCase));
                }
                R2PvgList.Add(r2pvg);
            }
        }
    }
}