﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PedVoiceReport
{
    public class PedVoiceRow
    {
        public string PrimaryVoiceName, Ethnicity, SecondaryVoiceName;
        public VoiceType VoiceType;
        public bool IsDeleted = false;
    }

    public enum VoiceType
    {
        Primary,
        Mini

    }
}

