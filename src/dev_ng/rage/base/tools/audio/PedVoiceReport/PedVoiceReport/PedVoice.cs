﻿using System.Collections.Specialized;

namespace PedVoiceReport
{
    public class PedVoice
    {
        public string Name;
        public int NumberOfWaves;
        public int NumberOfContexts;
    }
}