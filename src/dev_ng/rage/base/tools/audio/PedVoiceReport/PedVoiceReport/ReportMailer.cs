﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Runtime.InteropServices;
using Stream = Perforce.P4.Stream;

namespace PedVoiceReport
{
    public class ReportMailer
    {
        public void SendMessage(PedInfoParser report, HashSet<string> pedswarning, HashSet<string> warnings, string csv1,
            string csv2, List<string> recieptlist)
        {
            MailMessage mailMessage = new MailMessage();
            mailMessage.Subject = "Ped Voice Report";

            mailMessage.From = new MailAddress("pedvoicereport@rockstarnorth.com", "Ped Voice Report");

            foreach (string recieptiant in recieptlist)
            {
                mailMessage.To.Add(recieptiant);
            }
            mailMessage.ReplyToList.Add("Danjeli.Schembri@rockstarnorth.com");
            mailMessage.ReplyToList.Add("Alastair.MacGregor@rockstarnorth.com");

            //Attachments
            ContentType ct = new ContentType();
            ct.MediaType = MediaTypeNames.Text.Plain;
            string dateToday = DateTime.Now.Date.ToString("d");
            ct.Name = "Ped_List_in_Rave_" + dateToday + ".csv";
            mailMessage.Attachments.Add(new Attachment(GenerateStreamFromString(csv1), ct));

            ContentType ct2 = new ContentType();
            ct2.MediaType = MediaTypeNames.Text.Plain;
            ct2.Name = "Ped_List_in_Rave_Alt_" + dateToday + ".csv";
            mailMessage.Attachments.Add(new Attachment(GenerateStreamFromString(csv2), ct2));

            ContentType ctWarning1 = new ContentType();
            ctWarning1.MediaType = MediaTypeNames.Text.Html;
            ctWarning1.Name = "Warnings_from_Peds_meta" + dateToday + ".html";
            mailMessage.Attachments.Add(new Attachment(GenerateStreamFromString(string.Concat(string.Format("<h1>Ped Warnings from ped meta files on: {0} </h1><p>", dateToday), string.Join("<br>", pedswarning) + "<p>")), ctWarning1));



            ContentType ctWarning2 = new ContentType();
            ctWarning2.MediaType = MediaTypeNames.Text.Html;
            ctWarning2.Name = "Warnings_from_Rave" + dateToday + ".html";
            mailMessage.Attachments.Add(new Attachment(GenerateStreamFromString(string.Concat(string.Format("<h1>Ped Warnings from rave on: {0} </h1><p>", dateToday), string.Join("<br>", warnings) + "<p>")), ctWarning2));

            int numberOfUnassosiatedPvgs =
                report.R2PvgList.SelectMany(p => p.Race2Voice.Values).ToList().Except(report.PvgList).Count();
            List<string> voicesHookedToPvg = report.PedVoiceList.Where(p => p.NumberOfContexts > 0).Select(r => r.Name).Distinct().ToList();

            int voiceCount = report.PedVoicesInRaveWaves.Distinct().ToList().Count;
            List<string> unattachedVoices = report.PedVoicesInRaveWaves.Except(voicesHookedToPvg).ToList();
            int nmberOfHookedPeds = report.NumberOfHookedUpPeds;
            string summary =
                string.Format(@"# Warnings in this report: {0} <br># PVGs in RAVE: {1} <br># R2PVGs in RAVE: {2} <br># PVGs not associated with R2PVGs: {3}  <br># Voices in RAVE Waves: {4}<br># Voices hooked up to PVGs: {5}<br># Peds hooked up to R2PVGs: {6}",
                warnings.Count + pedswarning.Count, report.PvgList.Count, report.R2PvgList.Count, numberOfUnassosiatedPvgs, voiceCount, voicesHookedToPvg.Count, nmberOfHookedPeds);

            mailMessage.IsBodyHtml = true;
            mailMessage.Body =
                string.Format(
                "<h1>Ped Voice Report for {0}</h1><p>Please find generated lists and warnings attached.<br>Lists contain info about rave objects and waves related to peds on the date of report generation. </p> <p>Warnings in the report are split between problems encountered in peds meta and those encountered in rave <p> <h3><span style=\"font-size: 1.5em;\">Summary:</span></h3><p>&nbsp;</p><p style=\"margin-left: 30px;\">{1}</p>" +
                (unattachedVoices.Count > 0 ? "<h3><span style=\"font-size: 1.5em;\">Voices in RAVE Waves not hooked up to PVGs:</span></h3><p>&nbsp;</p><p style=\"margin-left: 30px;\">{2}</p>" : string.Empty) +
                (report.DeletedVoicesStillInRave.Count > 0 ? "<h3><span style=\"font-size: 1.5em;\">Voices in RAVE Waves which are marked as deleted in peds meta data:</span></h3><p>&nbsp;</p><p style=\"margin-left: 30px;\">{3}</p>" : string.Empty),
                    dateToday, summary, string.Join("<br>", unattachedVoices), string.Join("<br>", report.DeletedVoicesStillInRave));

            SmtpClient client = new SmtpClient("smtp.rockstar.t2.corp") { Credentials = CredentialCache.DefaultNetworkCredentials };
            client.Send(mailMessage);
        }

        public static void ErrorMessageSender(Exception exception)
        {
            MailMessage mailMessage = new MailMessage();
            mailMessage.Subject = "Ped Voice Report - errors encountered";
            mailMessage.From = new MailAddress("pedvoicereporterror@rockstarnorth.com");
            mailMessage.To.Add("Danjeli.schembri@rockstarnorth.com");
            mailMessage.To.Add("ben.durrenberger@rockstarnorth.com");
            mailMessage.To.Add("Alastair.MacGregor@rockstarnorth.com");

            mailMessage.Body = string.Format("Error message: {0} \n\n Error source: {1} \n\n Error Stack trace: {2} ",
                exception.Message, exception.Source, exception.StackTrace);

            File.WriteAllText(Path.GetTempFileName() + "log.txt", mailMessage.Body);
            SmtpClient client = new SmtpClient("smtp.rockstar.t2.corp") { Credentials = CredentialCache.DefaultNetworkCredentials };
            client.Send(mailMessage);
        }

        public MemoryStream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }
    }
}