﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Excel;
using PedVoiceReport.ReportGeneration;
using Perforce.P4;
using rage.ToolLib.CmdLine;

namespace PedVoiceReport
{
    public class Program : CommandLineConfiguration
    {
        [Description("Perforce server path to RDR3_Ped PedVoice Masterlist.xlsx")]
        [CommandLineConfiguration.OptionalParameterAttribute]
        [DefaultValue(@"//rdr3/docs/Audio/RDR3_Ped Voice Masterlist.xlsx")]
        public string PedVoiceMasterList { get; set; }

        [Description("Perforce server path to speech game objects folder")]
        [OptionalParameter]
        [DefaultValue(@"//rdr3/audio/dev/ASSETS/OBJECTS/Core/Audio/GAMEOBJECTS/SPEECH")]
        public string SpeechGameObjectsFolder { get; set; }


        [Description("Peds meta data file")]
        [OptionalParameter]
        [DefaultValue(@"//rdr3/assets/export/data/metapeds.pso.meta")]
        public string PedsMetaData { get; set; }

        [Description("Peds meta data file")]
        [OptionalParameter]
        [DefaultValue(@"//rdr3/assets/export/data/peds.pso.meta")]
        public string PedsMetaDataAlt { get; set; }

        [Description("Built waves location")]
        [CommandLineConfiguration.OptionalParameter]
        [DefaultValue(@"//rdr3/audio/dev/build/pc/BuiltWaves/")]
        public string BuiltWaves { get; set; }

        [Description("Perforce username")]
        [CommandLineConfiguration.OptionalParameterAttribute]
        public string UserName { get; set; }

        [Description("Perforce password")]
        [CommandLineConfiguration.OptionalParameterAttribute]
        public string Password { get; set; }

        [Description("Perforce Client")]
        [CommandLineConfiguration.OptionalParameterAttribute]
        public string P4Client { get; set; }


        [Description("Perforce Server")]
        [CommandLineConfiguration.OptionalParameterAttribute]
        public string P4Server { get; set; }

        static void Main(string[] args)
        {
            Program config;
            try
            {
                config = new Program(args);
            }
            catch (rage.ToolLib.CmdLine.CommandLineConfiguration.ParameterException ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.Usage);
            }
        }

        private string _pedVoicelistLocal;

        public Program(string[] args)
            : base(args)
        {
            try
            {
                ConnectToPerforce();
                _pedVoicelistLocal = GetLatestLocalPath(PedVoiceMasterList);
                var gameObjectsLocation = GetLatestLocalPath(SpeechGameObjectsFolder);
                var builtWavesLocation = GetLatestLocalPath(BuiltWaves);
                var pedsmetadata = new[] { GetLatestLocalPath(PedsMetaData), GetLatestLocalPath(PedsMetaDataAlt) };

                PedInfoParser report = new PedInfoParser(_pedVoicelistLocal, gameObjectsLocation, builtWavesLocation);
                report.ParsePedVoiceMasterList();
                report.ParseGameObjects();
                ListComparer listComparer = new ListComparer();
                HashSet<string> warnings = listComparer.GenerateWarnings(report);
                CsvGenerator csvGenerator = new CsvGenerator();
                csvGenerator.CreateCsvFromRave(report.R2PvgList);
                string firstCsv = csvGenerator.CommaSeperatedValues;
                string secondCsv = csvGenerator.CommaSeperatedValuesAlt;

                PedsMetaDataChecker pmdc = new PedsMetaDataChecker();
                var pedsmetaWarnings = pmdc.GetListOfErrors(pedsmetadata, report);
                report.NumberOfHookedUpPeds = pmdc.NumberOfHookedUpPeds;

                ReportMailer reportMailer = new ReportMailer();
                List<string> recieptlist = System.IO.File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory,"Receiptiants.txt")).Split(new string[] { Environment.NewLine, "," }, StringSplitOptions.None).ToList();
                
               
                
                reportMailer.SendMessage(report, pedsmetaWarnings, warnings, firstCsv, secondCsv, recieptlist);
            }
            catch (Exception e)
            {
                ReportMailer.ErrorMessageSender(e);
            }

        }

        private Repository _repository;

        private string GetLatestLocalPath(string depotPath)
        {
            if (Path.HasExtension(depotPath))
            {
                var filelist = new List<FileSpec> { new FileSpec(new DepotPath(depotPath)) };
                _perforceConnection.Client.SyncFiles(filelist, null);
                var files = _repository.GetFileMetaData(filelist, new Options());

                return files[0].ClientPath.Path;
            }
            else
            {
                depotPath = depotPath.TrimEnd('/');
                FileSpec fs = new FileSpec(new DepotPath(string.Concat(depotPath, "/...")));

                IList<FileMetaData> files = _repository.GetFileMetaData(new Options(), fs);
                _perforceConnection.Client.SyncFiles(files.Select(p => new FileSpec(p.DepotPath, p.ClientPath, p.LocalPath, null)).ToList(), new Options());
                var directoryname = depotPath.Split('/').Last();
                Match match = Regex.Match(files[0].ClientPath.Path, string.Format("(.*{0})", directoryname));
                return match.Value;
            }
        }

        private Connection _perforceConnection;

        private void ConnectToPerforce()
        {
            ServerAddress serverAddress = new ServerAddress(P4Server);
            Server server = new Server(serverAddress);
            _repository = new Repository(server);
            _perforceConnection = _repository.Connection;

            if (!string.IsNullOrEmpty(UserName))
            {
                // use the connection varaibles for this connection
                _perforceConnection.UserName = UserName;
                _perforceConnection.Client = new Client();
                _perforceConnection.Client.Name = P4Client;
                _perforceConnection.Login(Password);
            }


            _perforceConnection.Connect(new Options());
        }
    }
}
