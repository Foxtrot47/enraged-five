﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

#endregion

namespace PedVoiceReport.ReportGeneration
{
    public class PedsMetaDataChecker
    {
        public int NumberOfHookedUpPeds;
        
        public HashSet<string> GetListOfErrors(IEnumerable<string> pedsMetaDataFiles, PedInfoParser report)
        {
            List<R2PVG> r2PvgList = report.R2PvgList;
            HashSet<string> stringBuilder = new HashSet<string>();
            List<string> r2PvgNames = r2PvgList.Select(p => p.Name).ToList();
            List<string> setPvGs = new List<string>();
            foreach (string pedsMetaDataFile in pedsMetaDataFiles)
            {
                XDocument xdoc = XDocument.Load(pedsMetaDataFile);
                IEnumerable<XElement> items = xdoc.Descendants("Item");
                foreach (XElement item in items)
                {
                    XElement pvgElement = item.Element("PedVoiceGroup");
                    XElement nameElement = item.Element("Name");
                    if (nameElement == null || pvgElement == null)
                    {
                        continue;
                    }

                    if (!r2PvgNames.Contains(pvgElement.Value))
                    {
                        stringBuilder.Add(string.Format("{0} : is not an r2pvg in rave.",
                            pvgElement.Value));
                        continue;
                    }

                    setPvGs.Add(pvgElement.Value);
                    
                    if (!nameElement.Value.Equals(pvgElement.Value.Replace("_R2PVG", string.Empty), StringComparison.InvariantCultureIgnoreCase))
                    {
                        stringBuilder.Add(string.Format("{0} : is not of the same name as it's ped voice group {1}, consider renaming.",
                            nameElement.Value, pvgElement.Value));
                    }
                    
                }
            }

            List<string> missingPvgs = r2PvgNames.Except(setPvGs, StringComparer.InvariantCultureIgnoreCase).ToList();
            NumberOfHookedUpPeds = r2PvgNames.Union(setPvGs, StringComparer.InvariantCultureIgnoreCase).Distinct().Count();
            foreach (string missingPvg in missingPvgs)
            {
                stringBuilder.Add(string.Format("{0} : is not yet used in peds meta file but is available in rave.\n",
                            missingPvg));
            }
            return stringBuilder;
        }
    }
}