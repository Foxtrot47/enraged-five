﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace PedVoiceReport.ReportGeneration
{
    public class CsvGenerator
    {
        public string CommaSeperatedValues;
        public string CommaSeperatedValuesAlt;
        public void CreateCsvFromRave(List<R2PVG> raveR2Pvgs)
        {
            StringBuilder stringbuilder1 = new StringBuilder();
            StringBuilder stringbuilder2 = new StringBuilder();
            stringbuilder1.AppendLine(string.Format("{0},{1},{2},{3},{4},{5},{6}", "Ped Name", "Ethnicity",
                       "Primary Voice Name", "Number of Contexts", "Number of Waves", "Secondary Voice Name", "Pool"));


            stringbuilder2.AppendLine(string.Format("{0},{1},{2},{3},{4},{5},{6}", "Ped Name", "Ethnicity",
                        "Voice", "Number of Contexts", "Number of Waves", "Type", "Pool"));

            foreach (R2PVG r2Pvg in raveR2Pvgs)
            {

                foreach (KeyValuePair<string, PedVoiceGroups> race in r2Pvg.Race2Voice)
                {
                    string pedName, ethnicity, primaryVoiceName = string.Empty, secondaryVoiceName = string.Empty, pool = string.Empty;

                    int numContexts = 0;
                    int numWaves = 0;
                    pedName = r2Pvg.Name.Replace("_R2PVG", string.Empty);
                    ethnicity = race.Key;
                    PedVoiceGroups pvg = race.Value;
                    if (pvg == null) continue;
                    foreach (PedVoice primaryVoice in pvg.PrimaryVoices)
                    {
                        if (!string.IsNullOrEmpty(primaryVoiceName))
                        {
                            primaryVoiceName += ", ";
                        }
                        primaryVoiceName += primaryVoice.Name;
                        numWaves += primaryVoice.NumberOfWaves;
                        numContexts += primaryVoice.NumberOfContexts;
                    }

                    foreach (PedVoice secondaryvoice in pvg.SecodaryVoices)
                    {
                        if (!string.IsNullOrEmpty(secondaryVoiceName))
                        {
                            secondaryVoiceName += ", ";
                        }
                        secondaryVoiceName += secondaryvoice.Name;
                        numWaves += secondaryvoice.NumberOfWaves;
                        numContexts += secondaryvoice.NumberOfContexts;
                    }

                    foreach (string poolvoice in pvg.PoolVoices)
                    {
                        if (!string.IsNullOrEmpty(pool))
                        {
                            pool += ", ";
                        }
                        pool += poolvoice;
                    }

                    stringbuilder1.AppendLine(string.Format("{0},{1},{2},{3},{4},{5},{6}", pedName, ethnicity,
                        primaryVoiceName, numContexts < 0 ? "Not Found" : numContexts.ToString(),
                        numWaves < 0 ? "Not Found" : numWaves.ToString(), secondaryVoiceName, pool));

                    string type = string.IsNullOrEmpty(primaryVoiceName) ? "Secondary" : "Primary";

                    stringbuilder2.AppendLine(string.Format("{0},{1},{2},{3},{4},{5},{6}", pedName, ethnicity,
                        string.IsNullOrEmpty(primaryVoiceName) ? secondaryVoiceName : primaryVoiceName, numContexts < 0 ? "Not Found" : numContexts.ToString(),
                        numWaves < 0 ? "Not Found" : numWaves.ToString(), type, pool));

                }
            }
            CommaSeperatedValues = stringbuilder1.ToString();
            CommaSeperatedValuesAlt = stringbuilder2.ToString();
        }


    }
}