﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;

namespace PedVoiceReport
{
    public class ContextCompare : IEqualityComparer<XElement>
    {
        public bool Equals(XElement x, XElement y)
        {
            var xname = x.Attribute("name").Value;
            xname = xname.Substring(0, xname.Length - 7);
            var yname = y.Attribute("name").Value;
            yname = yname.Substring(0, yname.Length - 7);
            return xname.Equals(yname, StringComparison.InvariantCultureIgnoreCase);
        }

        public int GetHashCode(XElement x)
        {
            var xname = x.Attribute("name").Value;
            return xname.Substring(0, xname.Length - 7).GetHashCode();

        }
    }
}