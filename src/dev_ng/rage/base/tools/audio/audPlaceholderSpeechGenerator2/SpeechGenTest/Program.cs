﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Program.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Speech gen test console application.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SpeechGenTest
{
    using System;

    using SpeechGenLib.Processing;

    /// <summary>
    /// Speech gen test console application.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// The main.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        public static void Main(string[] args)
        {
            TestWaveCompressorProcessor();
        }

        /// <summary>
        /// Test the wave compressor processor.
        /// </summary>
        private static void TestWaveCompressorProcessor()
        {
            var wavePath = @"C:\temp\waves\placeholder\PAP3_AIAB_01.WAV";
            //var wavePath = @"C:\temp\waves\placeholder\REAC2_BKAB_01.WAV";

            var processor = new WaveCompressorProcessor(wavePath);
            var success = processor.Process();

            if (success)
            {
                Console.WriteLine("Success!");
            }
            else
            {
                Console.WriteLine("Failed");
                if (null != processor.Error)
                {
                    Console.WriteLine(processor.Error.Message);
                }
            }
        }
    }
}
