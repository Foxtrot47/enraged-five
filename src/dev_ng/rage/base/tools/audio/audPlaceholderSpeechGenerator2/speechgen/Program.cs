﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Program.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The speech generator program.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace speechgen {
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Reflection;

    using Microsoft.Win32;

    using rage;
    using rage.ToolLib;
    using rage.ToolLib.Logging;

    using Rockstar.AssetManager.Infrastructure.Enums;
    using Rockstar.AssetManager.Main;

    using SpeechGenLib.SpeechGeneration;
    using System.Diagnostics;

    /// <summary>
    /// The speech generator program.
    /// </summary>
    public class Program {
        #region Constants

        /// <summary>
        /// The registry key.
        /// </summary>
        private const string RegKey = "Software\\Rockstar Games\\RAGE Audio\\Placeholder Speech Generation\\";

        /// <summary>
        /// The registry key value.
        /// </summary>
        private const string RegKeyValue = "LastBuildTime";

        #endregion

        #region Methods


        /// <summary>
        /// The main entry point.
        /// </summary>
        /// <param name="args">
        /// The aruments.
        /// </param>
        /// <returns>
        /// The return result <see cref="int"/>.
        /// </returns>
        public static int Main(string[] args) {
            var screenLog = new TextLog(new ConsoleLogWriter(), new ContextStack());

            // Parse command line arguments
            var cmdLineArgs = new CmdLineArgs(screenLog);
            if(!cmdLineArgs.Parse(args)) {
                return -1;
            }

            // If help was requested it has already been displayed, so just return
            if(cmdLineArgs.HelpRequested) {
                return 0;
            }

            var loggingToFile = false;
            ILogWriter logWriter;
            if(!string.IsNullOrEmpty(cmdLineArgs.LogFile)) {
                loggingToFile = true;
                logWriter = new FileLogWriter(cmdLineArgs.LogFile);
            }
            else {
                logWriter = new ConsoleLogWriter();
            }

            int exitCode=0;
            using(logWriter) {
                ILog log = new TextLog(logWriter, new ContextStack());

                var speechGenVersionInfo = string.Concat("SpeechGen v", GetVersion());
                log.Write(speechGenVersionInfo);
                log.Write(string.Empty);

                if(loggingToFile) {
                    screenLog.Write(speechGenVersionInfo);
                    screenLog.Write(string.Empty);
                    screenLog.Write(
                        string.Format(
                            "Working... (check \"{0}\" for the complete log once finished)", cmdLineArgs.LogFile));
                    screenLog.Write(string.Empty);
                }

                // Create the asset manager
                var assetMgr = AssetManagerFactory.GetInstance(
                    AssetManagerType.Perforce,
                    null,
                    cmdLineArgs.PerforceHost,
                    cmdLineArgs.PerforceClient,
                    cmdLineArgs.PerforceUsername,
                    cmdLineArgs.PerforcePassword,
                    cmdLineArgs.PerforceDepotRoot,
                    null);

                if(assetMgr == null) {
                    log.Error("Failed to create asset manager to connect to Perforce.");
                    return -1;
                }

                try {
                    var currentBuildTime = DateTime.Now;
                    var registryKey = Registry.LocalMachine.OpenSubKey(RegKey, true)
                                      ?? Registry.LocalMachine.CreateSubKey(RegKey);

                    var lastBuildTime =
                        DateTime.Parse((string)registryKey.GetValue(RegKeyValue, DateTime.MinValue.ToString()));
                    log.WriteFormatted("Last successful build: {0}", lastBuildTime);

                    var projSettingsFile = string.Concat(cmdLineArgs.WorkingPath, cmdLineArgs.ProjectSettings);
                    var projSettings = new audProjectSettings(projSettingsFile);

                    String audioDataBuilderDefaultParam = " -project "+cmdLineArgs.ProjectSettings+" -temppath "+cmdLineArgs.TempPath+" -p4host "+cmdLineArgs.PerforceHost+" -p4client "+cmdLineArgs.PerforceClient+" -p4user "+cmdLineArgs.PerforceUsername+" -p4passwd "+cmdLineArgs.PerforcePassword+" -p4depotroot "+cmdLineArgs.PerforceDepotRoot;
                    foreach(audPlaceholderSpeechGeneration speechGeneration in projSettings.PlaceholderSpeechGenerationSettings) {
                        log.WriteFormatted("Build packs for {0}:", speechGeneration.Name);
                        foreach(audPlaceholderSpeechPack placeholderSpeechPack in speechGeneration.PlaceholderSpeechPacks) {
                            String audioDataBuilderParam = audioDataBuilderDefaultParam + " -pack " + placeholderSpeechPack.Name;

                            log.WriteFormatted("calling {0}", cmdLineArgs.DataBuilderPath + " " + audioDataBuilderParam);
                            ProcessStartInfo p = new ProcessStartInfo(cmdLineArgs.DataBuilderPath, audioDataBuilderParam);
                            p.WorkingDirectory = cmdLineArgs.DataBuilderPath.Substring(0, cmdLineArgs.DataBuilderPath.LastIndexOf(@"\"));
                            Process databuilder = Process.Start(p);
                            databuilder.WaitForExit();
                            if (databuilder.ExitCode != 0)
                            {
                                log.WriteFormatted("Error building packs for {0}", speechGeneration.Name);
                                log.WriteFormatted("Exit code was {0}", databuilder.ExitCode);
                                return databuilder.ExitCode;
                            }
                        }
                    }

                    foreach(audPlaceholderSpeechGeneration speechGeneration in projSettings.PlaceholderSpeechGenerationSettings) {
                        
                        log.WriteFormatted("Placeholder speech generation for {0}", speechGeneration.Name);
                        foreach(audPlaceholderSpeechPack placeholderSpeechPack in speechGeneration.PlaceholderSpeechPacks) {

                            string builtWavesPackListFile;
                            List<string> pendingWavesFiles;

                            GetWaveListFilePaths(placeholderSpeechPack.Name, cmdLineArgs.WorkingPath, projSettings, out pendingWavesFiles, out builtWavesPackListFile);

                            SpeechGenerationParams speechGenerationParams = new SpeechGenerationParams {
                                ProjectName = projSettings.GetProjectName(),
                                PackName = placeholderSpeechPack.Name,
                                BankFolderName = placeholderSpeechPack.BankFolder,
                                Rebuild = cmdLineArgs.Rebuild,
                                BuiltWavesPackListFile = builtWavesPackListFile,
                                PendingWavesFiles = pendingWavesFiles,
                                DialoguePath =
                               string.Concat(cmdLineArgs.WorkingPath, speechGeneration.DialoguePath),
                                CharacterVoiceConfig =
                               string.Concat(
                                        cmdLineArgs.WorkingPath, speechGeneration.CharacterVoiceConfig),
                                OutputPath =
                               string.Concat(
                                        cmdLineArgs.WorkingPath, projSettings.GetWaveInputPath()),
                                PerforceSettings = InitP4Settings(cmdLineArgs),
                                MaxFileSize = placeholderSpeechPack.MaxFileSize,
                                LastBuildTime = lastBuildTime,
                                ExcludePlaceholderConversation = speechGeneration.ExcludePlaceholderConversation,
                            };
                            var speechGenerator = new SpeechGenerator(log, assetMgr, speechGenerationParams);

                            string result;
                            var genExitCode = speechGenerator.Generate();
                            if(genExitCode >= 0) {
                                result = "Succeeded.";
                            }
                            else {
                                result = "Failed.";
                                exitCode = -1;
                            }

                            log.Write(result);
                            if(loggingToFile) {
                                screenLog.Write(result);
                            }
                        }
                    }
                    if (exitCode != -1) {
                        registryKey.SetValue(RegKeyValue, currentBuildTime, RegistryValueKind.String);
                        registryKey.Close();
                    }
                }
                catch(Exception ex) {
                    log.Exception(ex);
                    if(loggingToFile) {
                        screenLog.Exception(ex);
                    }

                    exitCode = -1;
                }
            }

            return exitCode;
        }

        /// <summary>
        /// Initialize the perforce connection settings.
        /// </summary>
        /// <param name="cmdLineArgs">
        /// The command line arguments.
        /// </param>
        /// <returns>
        /// The P4 settings <see cref="PerforceConnectionSettings"/>.
        /// </returns>
        private static PerforceConnectionSettings InitP4Settings(CmdLineArgs cmdLineArgs) {
            return new PerforceConnectionSettings {
                Host = cmdLineArgs.PerforceHost,
                Client = cmdLineArgs.PerforceClient,
                Username = cmdLineArgs.PerforceUsername,
                Password = cmdLineArgs.PerforcePassword
            };
        }

        /// <summary>
        /// Get the version.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private static string GetVersion() {
            return Assembly.GetEntryAssembly().GetName().Version.ToString();
        }


        private static void GetWaveListFilePaths(string packName,
            string workingPath,
            audProjectSettings projectSettings,
            out List<string> pendingWavesFiles,
            out string builtWavesFile) {
            pendingWavesFiles = new List<string>();
            builtWavesFile = null;

            var pendingWaves = string.Format("PendingWaves\\{0}.xml", packName);
            foreach(PlatformSetting platformSetting in projectSettings.GetPlatformSettings()) {
                if(!platformSetting.IsActive) {
                    continue;
                }

                var buildPath = string.Concat(workingPath, platformSetting.BuildInfo);
                if (builtWavesFile == null) {
                    builtWavesFile = Path.Combine(buildPath, "BuiltWaves\\BuiltWaves_PACK_LIST.xml");
                }

                pendingWavesFiles.Add(Path.Combine(buildPath, pendingWaves));
            }
        }

        #endregion
    }
}