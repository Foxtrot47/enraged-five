// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CmdLineArgs.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The command line args.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace speechgen
{
    using System;
    using System.Text;

    using rage.ToolLib.CmdLine;
    using rage.ToolLib.Logging;

    /// <summary>
    /// The command line args.
    /// </summary>
    public class CmdLineArgs
    {
        #region Constants

        /// <summary>
        /// The rebuild.
        /// </summary>
        private const string RebuildArgName = "rebuild";

        /// <summary>
        /// The help argument name.
        /// </summary>
        private const string HelpArgName = "help";

        /// <summary>
        /// The question mark argument name.
        /// </summary>
        private const string QuestionMarkArgName = "?";

        /// <summary>
        /// The log file argument name.
        /// </summary>
        private const string LogFileArgName = "logfile";

        /// <summary>
        /// The P4 client.
        /// </summary>
        private const string P4ClientArgName = "p4client";

        /// <summary>
        /// The P4 depot root argument name.
        /// </summary>
        private const string P4DepotRootArgName = "p4depotroot";

        /// <summary>
        /// The P4 host argument name.
        /// </summary>
        private const string P4HostArgName = "p4host";

        /// <summary>
        /// The P4 password argument name.
        /// </summary>
        private const string P4PasswordArgName = "p4passwd";

        /// <summary>
        /// The P4 user argument name.
        /// </summary>
        private const string P4UserArgName = "p4user";

        /// <summary>
        /// The project argument name.
        /// </summary>
        private const string ProjectArgName = "project";

        /// <summary>
        /// The working path argument name.
        /// </summary>
        private const string WorkingPathArgName = "workingpath";

        /// <summary>
        /// The temp path argument name.
        /// </summary>
        private const string TempPathArgName = "temppath";
        private const string DefaultTempPath = @"D:\Temp";

        /// <summary>
        /// The databuilder path argument name.
        /// </summary>
        private const string DataBuilderPathArgName = "databuilder";
        private const string DefaultDataBuilderPath = @"X:\gta5\tools\bin\audio\AudioDataBuilder\audioDataBuilder.exe";

        #endregion

        #region Fields

        /// <summary>
        /// The log.
        /// </summary>
        private readonly ILog log;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CmdLineArgs"/> class.
        /// </summary>
        /// <param name="log">
        /// The log.
        /// </param>
        public CmdLineArgs(ILog log)
        {
            if (log == null)
            {
                throw new ArgumentNullException("log");
            }

            this.log = log;
            this.PerforcePassword = string.Empty;
        }

        #endregion

        #region Public Properties


        /// <summary>
        /// Gets a value indicating whether force rebuild.
        /// </summary>
        public bool Rebuild { get; private set; }

        /// <summary>
        /// Gets a value indicating whether help requested.
        /// </summary>
        public bool HelpRequested { get; private set; }

        /// <summary>
        /// Gets the log file.
        /// </summary>
        public string LogFile { get; private set; }

        /// <summary>
        /// Gets the perforce client.
        /// </summary>
        public string PerforceClient { get; private set; }

        /// <summary>
        /// Gets the perforce depot root.
        /// </summary>
        public string PerforceDepotRoot { get; private set; }

        /// <summary>
        /// Gets the perforce host.
        /// </summary>
        public string PerforceHost { get; private set; }

        /// <summary>
        /// Gets the perforce password.
        /// </summary>
        public string PerforcePassword { get; private set; }

        /// <summary>
        /// Gets the perforce username.
        /// </summary>
        public string PerforceUsername { get; private set; }

        /// <summary>
        /// Gets the project settings.
        /// </summary>
        public string ProjectSettings { get; private set; }

        /// <summary>
        /// Gets the working path.
        /// </summary>
        public string WorkingPath { get; private set; }

        /// <summary>
        /// Gets the working path.
        /// </summary>
        public string TempPath { get; private set; }

        /// <summary>
        /// Gets the databuilder path.
        /// </summary>
        public string DataBuilderPath { get; private set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Parse the command line arguments.
        /// </summary>
        /// <param name="args">
        /// The arguments.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public bool Parse(string[] args)
        {
            var cmdLineParser = new CmdLineParser(args);
            try
            {
                this.HelpRequested = cmdLineParser[HelpArgName] != null || cmdLineParser[QuestionMarkArgName] != null;
                if (this.HelpRequested)
                {
                    this.log.Write(GetUsage());
                    return true;
                }

                this.SetForceRebuild(cmdLineParser);

                this.SetLogFile(cmdLineParser);

                if (!this.SetProject(cmdLineParser))
                {
                    return false;
                }

                if (!this.SetWorkingPath(cmdLineParser))
                {
                    return false;
                }

                this.SetTempPath(cmdLineParser);
                this.SetDataBuilderPath(cmdLineParser);

                return this.SetPerforceInfo(cmdLineParser);
            }
            catch (Exception ex)
            {
                this.log.Exception(ex);
                return false;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Get the description of the arguments accepted by the program.
        /// </summary>
        /// <returns>
        /// The full description <see cref="string"/>.
        /// </returns>
        private static string GetUsage()
        {
            var builder = new StringBuilder();
            builder.Append(Environment.NewLine);
            builder.Append("Generates synthesized placeholder speech for dialogue.");
            builder.Append(Environment.NewLine);
            builder.Append(Environment.NewLine);

            builder.Append("Usage:");
            builder.Append(Environment.NewLine);
            builder.Append("SPEECHGEN -workingPath <dir> -project <file> -p4host <server:port>");
            builder.Append(Environment.NewLine);
            builder.Append("-p4client <workspace> -p4user <user_name> [-p4passwd <password>] [-rebuild]");
            builder.Append(Environment.NewLine);
            builder.Append("[-logfile <file>] [-help/-?]");
            builder.Append(Environment.NewLine);
            builder.Append(Environment.NewLine);

            builder.Append("-workingPath\tthe root directory for all relative paths.");
            builder.Append(Environment.NewLine);
            builder.Append("-databuilder\tthe databuilder location ("+DefaultDataBuilderPath+" by default).");
            builder.Append(Environment.NewLine);
            builder.Append("-tempPath\tthe temporary directory for building packs ("+DefaultTempPath+" by default).");
            builder.Append(Environment.NewLine);
            builder.Append("-project\tthe path to the project settings file.");
            builder.Append(Environment.NewLine);
            builder.Append("-p4host\t\tthe connection info for perforce ex. rsgedip4s1:1666.");
            builder.Append(Environment.NewLine);
            builder.Append("-p4client\tthe perforce client workspace ex. EDIW-HBUNYAN.");
            builder.Append(Environment.NewLine);
            builder.Append("-p4user\t\tthe perforce user name ex. hughel.bunyan.");
            builder.Append(Environment.NewLine);
            builder.Append("-p4passwd\tthe perforce password.");
            builder.Append(Environment.NewLine);
            builder.Append("-depotroot\tthe depot root.");
            builder.Append(Environment.NewLine);
            builder.Append("-rebuild\tforce all the lines to be generated.");
            builder.Append(Environment.NewLine);
            builder.Append("-logfile\tspecifies the path to the file used for logging.");
            builder.Append(Environment.NewLine);
            builder.Append("-help or -?\tdisplay usage information.");
            builder.Append(Environment.NewLine);
            return builder.ToString();
        }

        /// <summary>
        /// Set the force rebuild flag.
        /// </summary>
        /// <param name="cmdLineParser">
        /// The command line parser.
        /// </param>
        private void SetForceRebuild(CmdLineParser cmdLineParser)
        {
            if (!string.IsNullOrEmpty(cmdLineParser[RebuildArgName]))
            {
                this.Rebuild = true;
            }
        }

        /// <summary>
        /// Set the log file path.
        /// </summary>
        /// <param name="cmdLineParser">
        /// The command line parser.
        /// </param>
        private void SetLogFile(CmdLineParser cmdLineParser)
        {
            if (!string.IsNullOrEmpty(cmdLineParser[LogFileArgName]))
            {
                this.LogFile = cmdLineParser[LogFileArgName];
            }
        }

        /// <summary>
        /// Set the perforce info.
        /// </summary>
        /// <param name="cmdLineParser">
        /// The command line parser.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool SetPerforceInfo(CmdLineParser cmdLineParser)
        {
            this.PerforceHost = cmdLineParser[P4HostArgName];
            if (string.IsNullOrEmpty(this.PerforceHost))
            {
                this.WriteError("Missing \"-p4host\" command line argument");
                return false;
            }

            this.PerforceClient = cmdLineParser[P4ClientArgName];
            if (string.IsNullOrEmpty(this.PerforceHost))
            {
                this.WriteError("Missing \"-p4client\" command line argument");
                return false;
            }

            this.PerforceUsername = cmdLineParser[P4UserArgName];
            if (string.IsNullOrEmpty(this.PerforceUsername))
            {
                this.WriteError("Missing \"-p4user\" command line argument");
                return false;
            }

            this.PerforcePassword = string.Empty;
            var passwd = cmdLineParser[P4PasswordArgName];
            if (!string.IsNullOrEmpty(passwd))
            {
                this.PerforcePassword = passwd;
            }

            this.PerforceDepotRoot = cmdLineParser[P4DepotRootArgName];
            if (string.IsNullOrEmpty(this.PerforceDepotRoot))
            {
                this.WriteError("Missing \"-p4depotroot\" command line argument");
                return false;
            }

            return true;
        }

        /// <summary>
        /// Set the project path.
        /// </summary>
        /// <param name="cmdLineParser">
        /// The command line parser.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool SetProject(CmdLineParser cmdLineParser)
        {
            this.ProjectSettings = cmdLineParser[ProjectArgName];
            if (string.IsNullOrEmpty(this.ProjectSettings))
            {
                this.WriteError("Missing \"-project\" command line argument.");
                return false;
            }

            return true;
        }

        /// <summary>
        /// Set the working path.
        /// </summary>
        /// <param name="cmdLineParser">
        /// The command line parser.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool SetWorkingPath(CmdLineParser cmdLineParser)
        {
            this.WorkingPath = cmdLineParser[WorkingPathArgName];
            if (string.IsNullOrEmpty(this.WorkingPath))
            {
                this.WriteError("Missing \"-workingpath\" command line argument.");
                return false;
            }

            return true;
        }

        /// <summary>
        /// Set the working path.
        /// </summary>
        /// <param name="cmdLineParser">
        /// The command line parser.
        /// </param>
        private void SetTempPath(CmdLineParser cmdLineParser) {
            this.TempPath = cmdLineParser[TempPathArgName];
            if(string.IsNullOrEmpty(this.TempPath)) {
                this.TempPath = DefaultTempPath;
            }
        }


        /// <summary>
        /// Set the databuilder path.
        /// </summary>
        /// <param name="cmdLineParser">
        /// The command line parser.
        /// </param>
        private void SetDataBuilderPath(CmdLineParser cmdLineParser) {
            this.DataBuilderPath = cmdLineParser[DataBuilderPathArgName];
            if(string.IsNullOrEmpty(this.DataBuilderPath)) {
                this.DataBuilderPath = DefaultDataBuilderPath;
            }
        }


        /// <summary>
        /// Write error to log.
        /// </summary>
        /// <param name="msg">
        /// The error message.
        /// </param>
        private void WriteError(string msg)
        {
            this.log.Write(string.Empty);
            this.log.Error(msg);
            this.log.Write(GetUsage());
        }

        #endregion
    }
}