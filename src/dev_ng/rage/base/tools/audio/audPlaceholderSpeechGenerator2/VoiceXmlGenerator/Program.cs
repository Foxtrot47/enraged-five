﻿using System;
using System.IO;
using System.Xml.Linq;
using rage.ToolLib.Logging;
using SpeechGenLib.Voices;

namespace VoiceXmlGenerator
{
    internal class Program
    {
        private const string XML_EXTENSION = ".XML";

        private static void Main(string[] args)
        {
            var log = new TextLog(new ConsoleLogWriter(), new ContextStack());

            if (args.Length != 1)
            {
                log.Error("Missing output file argument.");
                Environment.ExitCode = -1;
                return;
            }

            var fileName = args[0];
            var extension = Path.GetExtension(fileName).ToUpper();
            if (extension != XML_EXTENSION)
            {
                log.Error("Output file must be an XML file.");
                Environment.ExitCode = -1;
                return;
            }

            var voiceRepository = new VoiceRepository(log);

            var voicesElement = new XElement("Voices");
            foreach (var voice in voiceRepository.VoiceNames)
            {
                var voiceElement = new XElement("Voice", voice);
                voiceElement.SetAttributeValue("rate", 1.0);
                voicesElement.Add(voiceElement);
            }

            try
            {
                var doc = new XDocument(voicesElement);
                doc.Save(args[0]);
                log.Write("Finished successfully.");
            }
            catch (Exception ex)
            {
                log.Exception(ex);
                Environment.ExitCode = -1;
                return;
            }
            Environment.ExitCode = 0;
        }
    }
}