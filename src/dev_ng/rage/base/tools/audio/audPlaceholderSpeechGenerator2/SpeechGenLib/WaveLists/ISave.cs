namespace SpeechGenLib.WaveLists
{
    public interface ISave
    {
        bool Save(string file);
    }
}