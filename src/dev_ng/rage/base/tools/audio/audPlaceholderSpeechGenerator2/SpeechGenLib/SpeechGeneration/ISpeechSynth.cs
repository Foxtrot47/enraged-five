using System.Collections.Generic;

namespace SpeechGenLib.SpeechGeneration
{
    public interface ISpeechSynth
    {
        bool Synthesize(IList<LineToBuild> linesToBuild);
    }
}