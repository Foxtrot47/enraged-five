﻿using rage.ToolLib.Repository;

namespace SpeechGenLib.WaveLists
{
    public interface IWave : IRepositoryItem<string>,
                             IParse,
                             IOperation,
                             ISerialize
    {
    }
}