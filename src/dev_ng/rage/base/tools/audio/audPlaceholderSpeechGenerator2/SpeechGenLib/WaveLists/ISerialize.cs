using System.Xml.Linq;

namespace SpeechGenLib.WaveLists
{
    public interface ISerialize
    {
        XElement Serialize();
    }
}