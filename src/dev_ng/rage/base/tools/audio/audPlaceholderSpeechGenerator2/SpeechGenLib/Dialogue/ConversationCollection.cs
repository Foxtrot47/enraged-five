using System.Collections.Generic;

namespace SpeechGenLib.Dialogue
{
    public class ConversationCollection : List<IConversation>,
                                          IConversationCollection
    {
        #region IConversationCollection Members

        public string Id
        {
            get { return this[0].Id; }
        }

        #endregion
    }
}