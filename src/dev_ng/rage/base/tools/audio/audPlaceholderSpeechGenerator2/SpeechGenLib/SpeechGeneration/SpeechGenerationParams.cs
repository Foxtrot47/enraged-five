// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SpeechGenerationParams.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The speech generation params.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SpeechGenLib.SpeechGeneration
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The speech generation parameters.
    /// </summary>
    public class SpeechGenerationParams
    {
        #region Public Properties
        
        /// <summary>
        /// Gets or sets the bank folder name.
        /// </summary>
        public string BankFolderName { get; set; }

        /// <summary>
        /// Gets or sets the built waves pack list file.
        /// </summary>
        public string BuiltWavesPackListFile { get; set; }

        /// <summary>
        /// Gets or sets the character voice config.
        /// </summary>
        public string CharacterVoiceConfig { get; set; }

        /// <summary>
        /// Gets or sets the dialogue path.
        /// </summary>
        public string DialoguePath { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to rebuild.
        /// </summary>
        public bool Rebuild { get; set; }

        /// <summary>
        /// Gets or sets the last build time.
        /// </summary>
        public DateTime LastBuildTime { get; set; }

        /// <summary>
        /// Gets or sets the max file size.
        /// </summary>
        public uint MaxFileSize { get; set; }

        /// <summary>
        /// Gets or sets the output path.
        /// </summary>
        public string OutputPath { get; set; }

        /// <summary>
        /// Gets or sets the pack name.
        /// </summary>
        public string PackName { get; set; }

        /// <summary>
        /// Gets or sets the pending waves files.
        /// </summary>
        public List<string> PendingWavesFiles { get; set; }

        /// <summary>
        /// Gets or sets the perforce settings.
        /// </summary>
        public PerforceConnectionSettings PerforceSettings { get; set; }

        /// <summary>
        /// Gets or sets the project name.
        /// </summary>
        public string ProjectName { get; set; }

        /// <summary>
        /// Gets or sets the ExcludePlaceholderConversation flag
        /// </summary>
        public bool ExcludePlaceholderConversation { get; set; }

        #endregion
    }
}