using System;
using System.Collections.Generic;
using System.Speech.Synthesis;
using rage.ToolLib.Logging;

namespace SpeechGenLib.Voices
{
    public class VoiceRepository : IVoiceRepository
    {
        private const string FORMAT_DEFAULT_VOICE = "Default voice set to: \"{0}\".";
        private const string FORMAT_FOUND_VOICE = "Found voice: \"{0}\".";
        private const string SEARCHING_FOR_VOICES = "Searching for voices...";
        private const string NO_VOICES = "There are no voices installed on this machine.";

        private readonly HashSet<string> m_voices;

        public VoiceRepository(ILog log)
        {
            if (log == null)
            {
                throw new ArgumentNullException("log");
            }
            m_voices = new HashSet<string>();

            log.Write(SEARCHING_FOR_VOICES);

            FindVoices(log);

            if (!HasVoices)
            {
                throw new Exception(NO_VOICES);
            }
            log.WriteFormatted(FORMAT_DEFAULT_VOICE, DefaultVoiceName);
            log.Write(string.Empty);
        }

        #region IVoiceRepository Members

        public bool HasVoices
        {
            get { return DefaultVoiceName != null; }
        }

        public string DefaultVoiceName { get; private set; }

        public IEnumerable<string> VoiceNames
        {
            get { return m_voices; }
        }

        public bool Exists(string voiceName)
        {
            if (string.IsNullOrEmpty(voiceName))
            {
                return false;
            }
            return m_voices.Contains(voiceName);
        }

        #endregion

        private void FindVoices(ILog log)
        {
            var synthesizer = new SpeechSynthesizer();
            var voices = synthesizer.GetInstalledVoices();
            foreach (var voice in voices)
            {
                if (IsVoiceUsable(synthesizer, voice))
                {
                    var voiceName = voice.VoiceInfo.Name;
                    if (DefaultVoiceName == null)
                    {
                        DefaultVoiceName = voiceName;
                    }
                    m_voices.Add(voiceName);
                    log.WriteFormatted(FORMAT_FOUND_VOICE, voiceName);
                }
            }
        }

        private static bool IsVoiceUsable(SpeechSynthesizer synthesizer, InstalledVoice voice)
        {
            try
            {
                synthesizer.SelectVoice(voice.VoiceInfo.Name);
            }
            catch
            {
                return false;
            }
            return true;
        }
    }
}