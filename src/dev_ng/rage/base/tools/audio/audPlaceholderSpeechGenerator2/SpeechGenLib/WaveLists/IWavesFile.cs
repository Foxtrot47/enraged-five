using rage.ToolLib.Repository;

namespace SpeechGenLib.WaveLists
{
    public interface IWavesFile : IRepository<string, IPack>,
                                  ILoad,
                                  ISave
    {
    }
}