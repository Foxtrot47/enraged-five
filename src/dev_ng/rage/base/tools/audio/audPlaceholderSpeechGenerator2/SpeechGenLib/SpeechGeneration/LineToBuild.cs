namespace SpeechGenLib.SpeechGeneration
{
    [System.Serializable]
    public class LineToBuild
    {
        public string Speaker { get; set; }
        public string Text { get; set; }
        public string FileName { get; set; }
        public uint MaxFileSize { get; set; }
    }
}