namespace SpeechGenLib.SpeechGeneration
{
    public class PerforceConnectionSettings
    {
        public string Host { get; set; }
        public string Client { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}