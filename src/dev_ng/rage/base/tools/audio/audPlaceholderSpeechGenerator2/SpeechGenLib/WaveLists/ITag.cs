using rage.ToolLib.Repository;

namespace SpeechGenLib.WaveLists
{
    public interface ITag : IRepositoryItem<string>,
                            IParse,
                            ISerialize,
                            IOperation
    {
        string Value { get; }
    }
}