using System.Xml.Linq;

namespace SpeechGenLib.WaveLists
{
    public interface IParse
    {
        bool Parse(XElement root);
    }
}