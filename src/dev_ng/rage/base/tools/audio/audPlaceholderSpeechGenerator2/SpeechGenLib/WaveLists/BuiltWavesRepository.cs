using rage.ToolLib.Logging;

namespace SpeechGenLib.WaveLists
{
    public class BuiltWavesRepository : WavesFile,
                                        IBuiltWavesRepository
    {
        private const string BUILT_WAVES = "BuiltWaves";

        public BuiltWavesRepository(ILog log, string packName) : base(log, BUILT_WAVES, packName)
        {
        }
    }
}