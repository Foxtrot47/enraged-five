using System;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using rage.ToolLib.Logging;
using rage.ToolLib.Repository;

namespace SpeechGenLib.WaveLists
{
    public class WavesFile : Repository<string, IPack>,
                             IWavesFile
    {
        private const string FORMAT_FILE_LOADED = "Loaded: {0}.";

        private const string FORMAT_MISSING_ROOT_ELEMENT =
            "The file provided does not have the expected \"{0}\" root element.";

        private const string INVALID_FILENAME = "Invalid file name.";
        private const string NAME = "name";
        private const string FILE_MUST_BE_LOADED_FIRST = "A file must be loaded before you can save it.";
        protected readonly ILog m_log;
        protected readonly string m_packName;
        protected readonly string m_rootElementName;
        protected XDocument m_document;

        protected WavesFile(ILog log, string rootElementName, string packName)
        {
            if (log == null)
            {
                throw new ArgumentNullException("log");
            }
            if (string.IsNullOrEmpty(rootElementName))
            {
                throw new ArgumentNullException("rootElementName");
            }
            if (string.IsNullOrEmpty(packName))
            {
                throw new ArgumentNullException("packName");
            }
            m_log = log;
            m_rootElementName = rootElementName;
            m_packName = packName;
        }

        #region IWavesFile Members

        public bool IsLoaded { get; private set; }

        public bool Load(string file)
        {
            Unload();

            if (string.IsNullOrEmpty(file) ||
                !File.Exists(file))
            {
                m_log.Error(INVALID_FILENAME);
                return false;
            }

            try
            {
                m_document = XDocument.Load(file);
                if (m_document.Root == null ||
                    m_document.Root.Name != m_rootElementName)
                {
                    m_log.Error(FORMAT_MISSING_ROOT_ELEMENT, m_rootElementName);
                    return false;
                }

                if (!Parse(m_document.Root))
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                m_log.Exception(ex);
                return false;
            }

            m_log.WriteFormatted(FORMAT_FILE_LOADED, file);
            IsLoaded = true;
            return true;
        }

        public void Unload()
        {
            IsLoaded = false;
            m_itemLookup.Clear();
        }

        public bool Save(string file)
        {
            if (!IsLoaded)
            {
                m_log.Error(FILE_MUST_BE_LOADED_FIRST);
                return false;
            }

            if (string.IsNullOrEmpty(file))
            {
                m_log.Error(INVALID_FILENAME);
                return false;
            }

            foreach (var item in Items)
            {
                var pack = item.Serialize();
                if (pack != null)
                {
                    m_document.Root.Add(pack);
                }
            }

            try
            {
                m_document.Save(file);
            }
            catch (Exception ex)
            {
                m_log.Exception(ex);
                return false;
            }
            return true;
        }

        #endregion

        private bool Parse(XContainer root)
        {
            if (string.IsNullOrEmpty(m_packName))
            {
                foreach (var packElement in root.Elements())
                {
                    if (!ParsePack(packElement))
                    {
                        return false;
                    }
                }
            }
            else
            {
                var packElement =
                    (from element in root.Elements() where element.Attribute(NAME).Value == m_packName select element).
                        FirstOrDefault();
                if (packElement != null)
                {
                    if (!ParsePack(packElement))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        private bool ParsePack(XElement packElement)
        {
            var pack = new Pack(m_log);
            if (!pack.Parse(packElement))
            {
                return false;
            }
            Add(pack);
            return true;
        }
    }
}