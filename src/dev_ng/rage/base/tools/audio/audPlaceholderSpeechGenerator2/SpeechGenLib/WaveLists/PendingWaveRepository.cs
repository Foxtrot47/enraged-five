using System.Linq;
using rage.ToolLib.Logging;

namespace SpeechGenLib.WaveLists
{
    public class PendingWaveRepository : WavesFile,
                                         IPendingWaveRepository
    {
        private const string FORMAT_WAVES_ARE_PENDING =
            "There are pending waves for pack: \"{0}\". Run this tool again after they have been built.";

        private const string PENDING_WAVES = "PendingWaves";

        public PendingWaveRepository(ILog log, string packName) : base(log, PENDING_WAVES, packName)
        {
        }

        #region IPendingWaveRepository Members

        public bool Check()
        {
            if (!IsLoaded)
            {
                return false;
            }

            if (Items.Count() > 0)
            {
                m_log.Error(FORMAT_WAVES_ARE_PENDING, m_packName);
                return false;
            }
            return true;
        }

        #endregion
    }
}