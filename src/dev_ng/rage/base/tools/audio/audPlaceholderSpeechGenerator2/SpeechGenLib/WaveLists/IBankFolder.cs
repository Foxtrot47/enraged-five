using System.Collections.Generic;
using rage.ToolLib.Repository;

namespace SpeechGenLib.WaveLists
{
    public interface IBankFolder : IRepositoryItem<string>,
                                   IParse,
                                   ISerialize,
                                   IOperation
    {
        IEnumerable<IBankFolder> BankFolders { get; }
        IEnumerable<IBank> Banks { get; }
        IEnumerable<ITag> Tags { get; }

        bool TryGetBankFolder(string name, out IBankFolder item);

        void Add(IBankFolder item);

        void Remove(IBankFolder item);

        bool TryGetBank(string name, out IBank item);

        void Add(IBank item);

        void Remove(IBank item);

        bool TryGetTag(string name, out ITag item);

        void Add(ITag item);

        void Remove(ITag item);
    }
}