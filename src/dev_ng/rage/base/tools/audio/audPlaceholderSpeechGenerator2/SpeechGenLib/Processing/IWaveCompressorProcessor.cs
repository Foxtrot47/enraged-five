﻿// -----------------------------------------------------------------------
// <copyright file="IWaveCompressorProcessor.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace SpeechGenLib.Processing
{
    using System;

    /// <summary>
    /// Wave compressor processor interface.
    /// </summary>
    public interface IWaveCompressorProcessor
    {
        /// <summary>
        /// Gets the error.
        /// </summary>
        Exception Error { get; }

        /// <summary>
        /// Process the wave.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        bool Process();
    }
}
