﻿using System.Xml.Linq;
using rage.ToolLib.Logging;

namespace SpeechGenLib.WaveLists
{
    using System;
    using System.Collections.Generic;

    public class Pack : BankFolder,
                        IPack
    {
        private const string PlaceholderSpeechPackPrefix = "PS_";

        public Pack(ILog log) : base(log)
        {
        }

        public Pack(ILog log, string id, WaveListOperation operation) : base(log, id, operation)
        {
            this.AllowedBankNameStartLetters = new List<string>();

            if (!string.IsNullOrEmpty(id) && id.ToUpper().StartsWith(PlaceholderSpeechPackPrefix))
            {
                var substring = id.Substring(3).ToUpper();
                if (substring.Length != 2)
                {
                    throw new Exception("Pack name not in expected format - unable to initialize pack: " + id);
                }

                this.AllowedBankNameStartLetters.Add(substring[0].ToString());
                var currChar = substring[0];
                while (currChar != substring[1])
                {
                    this.AllowedBankNameStartLetters.Add((++currChar).ToString());
                }
            }
            else
            {
                log.WriteFormatted("Packname {0} is not recognised as a name range pack (e.g. PS_AD). Generation of all banks enabled.", id);
                char[] alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
                foreach (char letter in alphabet)
                {
                    this.AllowedBankNameStartLetters.Add(letter.ToString());   
                }
            }
        }

        #region IPack Members
        
        public IList<string> AllowedBankNameStartLetters { get; private set; }

        public override XElement Serialize()
        {
            var element = new XElement("Pack");
            if (DoSerialization(element) ||
                Operation != WaveListOperation.none)
            {
                return element;
            }
            return null;
        }

        #endregion
    }
}