using System.IO;

namespace SpeechGenLib.Processing
{
    public class TextToSpeechFileSizeLimitedProcessor : TextToSpeechFileProcessor
    {
        private const string FORMAT_LINE_TOO_BIG = "Line: {0} was too long at {1} bytes.";
        private const string LINE_TOO_BIG = "Line was too long.";
        private readonly uint m_maxFileSizeInBytes;

        public TextToSpeechFileSizeLimitedProcessor(string voiceToUse, string fileName, uint maxFileSizeInBytes)
            : base(voiceToUse, fileName)
        {
            m_maxFileSizeInBytes = maxFileSizeInBytes;
        }

        public override bool Process(string speechText, out string output)
        {
            bool result = base.Process(speechText, out output);
            if (result)
            {
                if (m_maxFileSizeInBytes != 0)
                {
                    var originalFileInfo = new FileInfo(m_fileName);
                    if (originalFileInfo.Length > m_maxFileSizeInBytes)
                    {
                        output = string.Format(FORMAT_LINE_TOO_BIG, m_fileName, originalFileInfo.Length);
                        string dummyOutput;
                        result = base.Process(output, out dummyOutput);
                        if (result)
                        {
                            var lineTooBigFileInfo = new FileInfo(m_fileName);
                            if (lineTooBigFileInfo.Length > m_maxFileSizeInBytes)
                            {
                                result = base.Process(LINE_TOO_BIG, out dummyOutput);
                            }
                        }
                        else
                        {
                            result = base.Process(LINE_TOO_BIG, out dummyOutput);
                        }
                    }
                }

                var fileInfo = new FileInfo(m_fileName);
                if (fileInfo.Length == 0)
                {
                    string dummyOutput;
                    result = base.Process("Blank Line", out dummyOutput);
                }
            }
            return result;
        }
    }
}