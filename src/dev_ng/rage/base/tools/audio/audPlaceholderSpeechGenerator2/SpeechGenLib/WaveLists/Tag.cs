using System;
using System.Xml.Linq;
using rage.ToolLib.Logging;

namespace SpeechGenLib.WaveLists
{
    public class Tag : ITag
    {
        private const string INVALID_ROOT = "This object cannot parse a \"{0}\" element.";
        private const string NAME = "name";
        private const string MISSING_NAME_ATTRIBUTE = "Tag element missing \"name\" attribute.";
        private const string VALUE = "value";
        private const string OPERATION = "operation";
        private readonly ILog m_log;

        public Tag(ILog log)
        {
            if (log == null)
            {
                throw new ArgumentNullException("log");
            }
            m_log = log;
        }

        public Tag(ILog log, string id, string value, WaveListOperation operation) : this(log)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("id");
            }
            Id = id;
            Value = value;
            Operation = operation;
        }

        #region ITag Members

        public string Id { get; private set; }
        public string Value { get; private set; }
        public WaveListOperation Operation { get; private set; }

        public bool Parse(XElement root)
        {
            if (root.Name !=
                WaveListParseTokens.Tag.ToString())
            {
                m_log.Error(INVALID_ROOT, root.Name);
                return false;
            }

            var nameAttrib = root.Attribute(NAME);
            if (nameAttrib != null &&
                !string.IsNullOrEmpty(nameAttrib.Value))
            {
                Id = nameAttrib.Value;
            }
            else
            {
                m_log.Error(MISSING_NAME_ATTRIBUTE);
                return false;
            }

            var valueAttrib = root.Attribute(VALUE);
            if (valueAttrib != null &&
                !string.IsNullOrEmpty(valueAttrib.Value))
            {
                Value = valueAttrib.Value;
            }
            else
            {
                Value = null;
            }
            return true;
        }

        public XElement Serialize()
        {
            var hasValidOperation = Operation != WaveListOperation.none;
            if (hasValidOperation)
            {
                var element = new XElement(WaveListParseTokens.Tag.ToString());
                element.SetAttributeValue(OPERATION, Operation.ToString());
                element.SetAttributeValue(NAME, Id);
                if (!string.IsNullOrEmpty(Value))
                {
                    element.SetAttributeValue(VALUE, Value);
                }
                return element;
            }
            return null;
        }

        #endregion
    }
}