using System;

namespace SpeechGenLib.Processing
{
    public interface ITextToSpeechProcessor
    {
        Exception Error { get; }

        bool Process(string speechText, out string output);
    }
}