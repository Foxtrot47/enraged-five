using System;
using System.Collections.Generic;
using System.Linq;
using SpeechGenLib.WaveLists;

namespace SpeechGenLib.SpeechGeneration
{
    public class WaveAssetCleanup
    {
        private const string FORMAT_PATH_SEGMENT = "{0}\\{1}";
        private readonly string m_outputPath;

        public WaveAssetCleanup(string outputPath)
        {
            if (string.IsNullOrEmpty(outputPath))
            {
                throw new ArgumentNullException("outputPath");
            }
            m_outputPath = outputPath;
        }

        public IEnumerable<string> GetWavesToDelete(IPack pendingPack)
        {
            var wavesToDelete = new List<string>();
            var wavesToRemove = GetRemovedWaves(pendingPack);
            foreach (var waveToRemove in wavesToRemove)
            {
                wavesToDelete.Add(string.Format("{0}{1}\\{2}", m_outputPath, pendingPack.Id, waveToRemove));
            }
            return wavesToDelete;
        }

        private static IEnumerable<string> GetRemovedWaves(IBankFolder pendingBankFolder)
        {
            var removedWaves = new List<string>();
            foreach (var bankFolder in pendingBankFolder.BankFolders)
            {
                var wavesToRemove = GetRemovedWaves(bankFolder);
                foreach (var waveToRemove in wavesToRemove)
                {
                    removedWaves.Add(string.Format(FORMAT_PATH_SEGMENT, bankFolder.Id, waveToRemove));
                }
            }

            foreach (var bank in pendingBankFolder.Banks)
            {
                var wavesToRemove = GetRemovedWaves(bank);
                foreach (var waveToRemove in wavesToRemove)
                {
                    removedWaves.Add(string.Format(FORMAT_PATH_SEGMENT, bank.Id, waveToRemove));
                }
            }
            return removedWaves;
        }

        private static IEnumerable<string> GetRemovedWaves(IWaveFolder pendingWaveFolder)
        {
            var removedWaves =
                new List<string>(from wave in pendingWaveFolder.Waves
                                 where wave.Operation == WaveListOperation.remove
                                 select wave.Id);
            foreach (var waveFolder in pendingWaveFolder.WaveFolders)
            {
                var wavesToRemove = GetRemovedWaves(waveFolder);
                foreach (var waveToRemove in wavesToRemove)
                {
                    removedWaves.Add(string.Format(FORMAT_PATH_SEGMENT, waveFolder.Id, waveToRemove));
                }
            }
            return removedWaves;
        }
    }
}