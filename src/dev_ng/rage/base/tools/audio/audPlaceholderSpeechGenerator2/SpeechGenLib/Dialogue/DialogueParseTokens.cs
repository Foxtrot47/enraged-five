namespace SpeechGenLib.Dialogue
{
    public enum DialogueParseTokens
    {
        MissionDialogue,
        Conversation,
        Line,
        MoCap,
// ReSharper disable InconsistentNaming
        id,
        root,
        guid,
        dialogue,
        filename,
        timestamp,
        random,
        cutscene
// ReSharper restore InconsistentNaming
    }
}