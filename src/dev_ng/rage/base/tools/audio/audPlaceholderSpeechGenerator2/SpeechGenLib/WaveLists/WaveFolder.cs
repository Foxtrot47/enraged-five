﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;
using rage.ToolLib.Logging;
using rage.ToolLib.Repository;

namespace SpeechGenLib.WaveLists
{
    public class WaveFolder : IWaveFolder
    {
        private const string ILLEGAL_CHILD_ELEMENT = "Illegal \"{0}\" child element encountered.";
        private const string NAME = "name";
        private const string FORMAT_MISSING_NAME_ATTRIBUTE = "{0} element missing \"name\" attribute.";
        private const string FORMAT_UNRECOGNIZED_OPERATION = "Unrecognized operation \"{0}\".";
        private const string OPERATION = "operation";
        private const string WAVE_FOLDER = "WaveFolder";

        private readonly ILog m_log;
        private readonly IRepository<string, ITag> m_tags;
        private readonly IRepository<string, IWaveFolder> m_waveFolders;
        private readonly IRepository<string, IWave> m_waves;

        public WaveFolder(ILog log)
        {
            if (log == null)
            {
                throw new ArgumentNullException("log");
            }
            m_log = log;

            m_waveFolders = new Repository<string, IWaveFolder>();
            m_waves = new Repository<string, IWave>();
            m_tags = new Repository<string, ITag>();
            Operation = WaveListOperation.none;
        }

        public WaveFolder(ILog log, string id, WaveListOperation operation) : this(log)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("id");
            }
            if (operation == WaveListOperation.none)
            {
                throw new ArgumentException(
                    "operation: This constructor should only be used if specifying a valid operation");
            }
            Id = id;
            Operation = operation;
        }

        #region IWaveFolder Members

        public string Id { get; private set; }

        public WaveListOperation Operation { get; private set; }

        public virtual bool Parse(XElement root)
        {
            if (!ParseNameAttribute(root))
            {
                return false;
            }

            if (!ParseOperationAttribute(root))
            {
                return false;
            }

            foreach (var child in root.Elements())
            {
                try
                {
                    var childName =
                        (WaveListParseTokens) Enum.Parse(typeof (WaveListParseTokens), child.Name.ToString());

                    switch (childName)
                    {
                        case WaveListParseTokens.Tag:
                            {
                                var tag = new Tag(m_log);
                                if (!tag.Parse(child))
                                {
                                    return false;
                                }
                                m_tags.Add(tag);
                                break;
                            }
                        case WaveListParseTokens.WaveFolder:
                            {
                                var waveFolder = new WaveFolder(m_log);
                                if (!waveFolder.Parse(child))
                                {
                                    return false;
                                }
                                m_waveFolders.Add(waveFolder);
                                break;
                            }
                        case WaveListParseTokens.Wave:
                            {
                                var wave = new Wave(m_log);
                                if (!wave.Parse(child))
                                {
                                    return false;
                                }
                                m_waves.Add(wave);
                                break;
                            }
                    }
                }
                catch
                {
                    m_log.Error(ILLEGAL_CHILD_ELEMENT, child.Name);
                    return false;
                }
            }
            return true;
        }

        public virtual XElement Serialize()
        {
            var element = new XElement(WAVE_FOLDER);
            if (DoSerialization(element) ||
                Operation != WaveListOperation.none)
            {
                return element;
            }
            return null;
        }

        #endregion

        #region Wave Folders

        public IEnumerable<IWaveFolder> WaveFolders
        {
            get { return m_waveFolders.Items; }
        }

        public bool TryGetWaveFolder(string name, out IWaveFolder waveFolder)
        {
            return m_waveFolders.TryGetItem(name, out waveFolder);
        }

        public void Add(IWaveFolder waveFolder)
        {
            m_waveFolders.Add(waveFolder);
        }

        public void Remove(IWaveFolder waveFolder)
        {
            m_waveFolders.Remove(waveFolder);
        }

        #endregion

        #region Waves

        public IEnumerable<IWave> Waves
        {
            get { return m_waves.Items; }
        }

        public bool TryGetWave(string name, out IWave wave)
        {
            return m_waves.TryGetItem(name, out wave);
        }

        public void Add(IWave wave)
        {
            m_waves.Add(wave);
        }

        public void Remove(IWave wave)
        {
            m_waves.Remove(wave);
        }

        #endregion

        #region Tags

        public IEnumerable<ITag> Tags
        {
            get { return m_tags.Items; }
        }

        public bool TryGetTag(string name, out ITag tag)
        {
            return m_tags.TryGetItem(name, out tag);
        }

        public void Add(ITag tag)
        {
            m_tags.Add(tag);
        }

        public void Remove(ITag tag)
        {
            m_tags.Remove(tag);
        }

        #endregion

        protected bool DoSerialization(XElement element)
        {
            if (Operation != WaveListOperation.none)
            {
                element.SetAttributeValue(OPERATION, Operation.ToString());
            }
            element.SetAttributeValue(NAME, Id);

            var childAdded = false;
            foreach (var tag in m_tags.Items)
            {
                var child = tag.Serialize();
                if (child != null)
                {
                    childAdded = true;
                    element.Add(child);
                }
            }

            foreach (var waveFolder in m_waveFolders.Items)
            {
                var child = waveFolder.Serialize();
                if (child != null)
                {
                    childAdded = true;
                    element.Add(child);
                }
            }

            foreach (var wave in m_waves.Items)
            {
                var child = wave.Serialize();
                if (child != null)
                {
                    childAdded = true;
                    element.Add(child);
                }
            }
            return childAdded;
        }

        private bool ParseNameAttribute(XElement root)
        {
            var nameAttrib = root.Attribute(NAME);
            if (nameAttrib != null &&
                !string.IsNullOrEmpty(nameAttrib.Value))
            {
                Id = nameAttrib.Value;
            }
            else
            {
                m_log.Error(FORMAT_MISSING_NAME_ATTRIBUTE, root.Name);
                return false;
            }
            return true;
        }

        private bool ParseOperationAttribute(XElement root)
        {
            var operationAttrib = root.Attribute(OPERATION);
            if (operationAttrib != null &&
                !string.IsNullOrEmpty(operationAttrib.Value))
            {
                try
                {
                    Operation = (WaveListOperation) Enum.Parse(typeof (WaveListOperation), operationAttrib.Value, true);
                }
                catch
                {
                    m_log.Error(FORMAT_UNRECOGNIZED_OPERATION, operationAttrib.Value);
                    return false;
                }
            }
            return true;
        }
    }
}