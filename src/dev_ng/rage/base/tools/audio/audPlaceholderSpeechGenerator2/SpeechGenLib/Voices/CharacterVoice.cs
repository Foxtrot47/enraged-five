using System;

namespace SpeechGenLib.Voices
{
    public class CharacterVoice
    {
        public CharacterVoice(string characterName, string voiceName, float? voiceRate)
        {
            if (string.IsNullOrEmpty(characterName))
            {
                throw new ArgumentException("characterName");
            }
            if (string.IsNullOrEmpty(voiceName))
            {
                throw new ArgumentException("voiceName");
            }
            CharacterName = characterName;
            VoiceName = voiceName;
            VoiceRate = voiceRate != null ? (voiceRate.Value == 1 ? null : voiceRate) : voiceRate;
        }

        public string CharacterName { get; private set; }
        public string VoiceName { get; private set; }
        public float? VoiceRate { get; private set; }
    }
}