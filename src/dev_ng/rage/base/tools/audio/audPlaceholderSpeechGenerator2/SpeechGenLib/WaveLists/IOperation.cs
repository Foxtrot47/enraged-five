namespace SpeechGenLib.WaveLists
{
    public interface IOperation
    {
        WaveListOperation Operation { get; }
    }
}