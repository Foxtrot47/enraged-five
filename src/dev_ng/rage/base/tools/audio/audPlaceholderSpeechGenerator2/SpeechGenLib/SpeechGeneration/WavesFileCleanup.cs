using System;
using System.Collections.Generic;
using rage.ToolLib.Logging;
using SpeechGenLib.Dialogue;
using SpeechGenLib.Voices;
using SpeechGenLib.WaveLists;

namespace SpeechGenLib.SpeechGeneration
{
    public class WavesFileCleanup
    {
        private readonly ILog m_log;
        private readonly ICharacterVoiceRepository m_characterVoiceRepository;
        private readonly IDialogueRepository m_dialogueRepository;

        public WavesFileCleanup(ILog log, ICharacterVoiceRepository characterVoiceRepository, IDialogueRepository dialogueRepository)
        {
            if (log == null)
            {
                throw new ArgumentNullException("log");
            }
            if (characterVoiceRepository == null)
            {
                throw new ArgumentNullException("characterVoiceRepository");
            }
            if (dialogueRepository == null)
            {
                throw new ArgumentNullException("dialogueRepository");
            }
            m_log = log;
            m_characterVoiceRepository = characterVoiceRepository;
            m_dialogueRepository = dialogueRepository;
        }

        public bool Cleanup(IBankFolder builtBankFolder, IBankFolder pendingBankFolder)
        {
            var changed = false;
            if(builtBankFolder==null) return changed;
            foreach (var builtBank in builtBankFolder.Banks)
            {
                IDialogue dialogue;
                if (m_dialogueRepository.TryGetDialogue(builtBank.Id, out dialogue))
                {
                    if (CleanupBank(dialogue, builtBank, pendingBankFolder))
                    {
                        changed = true;
                    }
                }
                else
                {
                    if (DeleteBank(builtBank, pendingBankFolder))
                    {
                        changed = true;
                    }
                }
            }
            return changed;
        }

        private bool CleanupBank(IDialogue dialogue, IBank builtBank, IBankFolder pendingBankFolder)
        {
            var shouldAdd = false;
            IBank pendingBank;
            if (!pendingBankFolder.TryGetBank(builtBank.Id, out pendingBank))
            {
                pendingBank = new Bank(m_log, builtBank.Id, WaveListOperation.modify);
                shouldAdd = true;
            }

            var changed = false;
            foreach (var builtWaveFolder in builtBank.WaveFolders)
            {
                IConversationCollection conversationCollection;
                if (dialogue.TryGetConversationCollection(builtWaveFolder.Id, out conversationCollection))
                {
                    if (CleanupConversationWaveFolder(conversationCollection, builtWaveFolder, pendingBank))
                    {
                        changed = true;
                    }
                }
                else
                {
                    if (DeleteWaveFolder(builtWaveFolder, pendingBank))
                    {
                        changed = true;
                    }
                }
            }

            if (changed && shouldAdd)
            {
                pendingBankFolder.Add(pendingBank);
            }
            return changed;
        }

        private bool CleanupConversationWaveFolder(IEnumerable<IConversation> conversationCollection,
                                                   IWaveFolder builtWaveFolder,
                                                   IBank pendingBank)
        {
            var shouldAdd = false;
            IWaveFolder pendingConversationFolder;
            if (!pendingBank.TryGetWaveFolder(builtWaveFolder.Id, out pendingConversationFolder))
            {
                pendingConversationFolder = new WaveFolder(m_log, builtWaveFolder.Id, WaveListOperation.modify);
                shouldAdd = true;
            }

            var changed = false;
            var characterLines = BuildListGenerationUtility.CreateCharacterLines(conversationCollection);
            foreach (var builtCharacterFolder in builtWaveFolder.WaveFolders)
            {
                IList<ILine> lines = null;
                foreach (var entry in characterLines)
                {
                    var characterName = m_characterVoiceRepository.GetCharacterName(entry.Key);
                    if (characterName == builtCharacterFolder.Id)
                    {
                        lines = entry.Value;
                        break;
                    }
                }

                if (lines != null &&
                    lines.Count > 0)
                {
                    if (CleanupCharacterWaveFolder(lines, builtCharacterFolder, pendingConversationFolder))
                    {
                        changed = true;
                    }
                }
                else
                {
                    if (DeleteWaveFolder(builtCharacterFolder, pendingConversationFolder))
                    {
                        changed = true;
                    }
                }
            }

            if (changed && shouldAdd)
            {
                pendingBank.Add(pendingConversationFolder);
            }
            return changed;
        }

        private bool CleanupCharacterWaveFolder(IEnumerable<ILine> lines,
                                                IWaveFolder builtCharacterFolder,
                                                IWaveFolder pendingConversationFolder)
        {
            var shouldAdd = false;
            IWaveFolder pendingCharacterFolder;
            if (!pendingConversationFolder.TryGetWaveFolder(builtCharacterFolder.Id, out pendingCharacterFolder))
            {
                pendingCharacterFolder = new WaveFolder(m_log, builtCharacterFolder.Id, WaveListOperation.modify);
                shouldAdd = true;
            }

            var linesLookup = new HashSet<string>();
            foreach (var line in lines)
            {
                linesLookup.Add(line.WaveFileName);
            }

            var changed = false;
            foreach (var builtWave in builtCharacterFolder.Waves)
            {
                if (!linesLookup.Contains(builtWave.Id))
                {
                    pendingCharacterFolder.Add(new Wave(m_log, builtWave.Id, WaveListOperation.remove));
                    changed = true;
                }
            }

            if (changed && shouldAdd)
            {
                pendingConversationFolder.Add(pendingCharacterFolder);
            }
            return changed;
        }

        private bool DeleteBank(IWaveFolder builtBank, IBankFolder pendingBankFolder)
        {
            var pendingBank = new Bank(m_log, builtBank.Id, WaveListOperation.remove);
            pendingBankFolder.Add(pendingBank);
            return DeleteWaveFolders(builtBank, pendingBank);
        }

        private bool DeleteWaveFolder(IWaveFolder builtWaveFolder, IWaveFolder pendingParentWaveFolder)
        {
            var pendingWaveFolder = new WaveFolder(m_log, builtWaveFolder.Id, WaveListOperation.remove);
            pendingParentWaveFolder.Add(pendingWaveFolder);
            return DeleteWaveFolders(builtWaveFolder, pendingWaveFolder);
        }

        private bool DeleteWaveFolders(IWaveFolder builtWaveFolder, IWaveFolder pendingWaveFolder)
        {
            var changed = false;
            foreach (var builtTag in builtWaveFolder.Tags)
            {
                pendingWaveFolder.Add(new Tag(m_log, builtTag.Id, null, WaveListOperation.remove));
                changed = true;
            }

            foreach (var builtFolder in builtWaveFolder.WaveFolders)
            {
                var pendingFolder = new WaveFolder(m_log, builtFolder.Id, WaveListOperation.remove);
                if (DeleteWaveFolders(builtFolder, pendingFolder))
                {
                    changed = true;
                }
                pendingWaveFolder.Add(pendingFolder);
            }

            if (DeleteWaves(builtWaveFolder, pendingWaveFolder))
            {
                changed = true;
            }
            return changed;
        }

        private bool DeleteWaves(IWaveFolder builtWaveFolder, IWaveFolder pendingWaveFolder)
        {
            var changed = false;
            foreach (var builtWave in builtWaveFolder.Waves)
            {
                pendingWaveFolder.Add(new Wave(m_log, builtWave.Id, WaveListOperation.remove));
                changed = true;
            }
            return changed;
        }
    }
}