using System;
using System.Collections.Generic;
using System.IO;
using rage.ToolLib.Logging;
using SpeechGenLib.Processing;

namespace SpeechGenLib.SpeechGeneration
{
    public class SingleThreadSpeechSynth : ISpeechSynth
    {
        private const string FORMAT_ERROR_GENERATING_FILE = "Error generating: {0}.";

        private const string FORMAT_ERROR_COMPRESSING_FILE = "Error compressing: {0}.";

        private const string FORMAT_COMPLETED_SPEECH_SYNTHESIS =
            "Completed speech synthesis. Processing took: {0} for {1} lines.";

        private const string STARTED_SPEECH_SYNTHESIS = "Started speech synthesis...";
        private readonly ILog m_log;

        public SingleThreadSpeechSynth(ILog log)
        {
            if (log == null)
            {
                throw new ArgumentNullException("log");
            }
            m_log = log;
        }

        #region ISpeechSynth Members

        public bool Synthesize(IList<LineToBuild> linesToBuild)
        {
            m_log.Write(STARTED_SPEECH_SYNTHESIS);

            var start = DateTime.Now;

            int numLinesToBuild = linesToBuild.Count;
            int numLinesBuilt = 0;
            bool showProgress = Environment.UserInteractive;
            string numLinesToBuildStr = numLinesToBuild.ToString().PadLeft(5);

            foreach (var lineToBuild in linesToBuild)
            {
                var directory = Path.GetDirectoryName(lineToBuild.FileName);
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                if (showProgress)
                {                    
                    string numLinesBuiltStr = numLinesBuilt.ToString().PadLeft(5);
                    int percentComplete = (numLinesBuilt * 100) / numLinesToBuild;
                    string completionStr = percentComplete.ToString().PadLeft(3);
                                        
                    Console.SetCursorPosition(0, Console.CursorTop);
                    Console.Write("Synthesized {0} / {1}: {2}% complete - {3} {4}", numLinesBuiltStr, numLinesToBuildStr, completionStr, lineToBuild.Speaker, Path.GetFileNameWithoutExtension(lineToBuild.FileName));
                }

                var processor = new TextToSpeechFileSizeLimitedProcessor(lineToBuild.Speaker,
                                                                         lineToBuild.FileName,
                                                                         lineToBuild.MaxFileSize);
                string output;
                if (!processor.Process(lineToBuild.Text, out output))
                {
                    m_log.Error(FORMAT_ERROR_GENERATING_FILE, lineToBuild.FileName);
                    if (processor.Error != null)
                    {
                        m_log.Exception(processor.Error);
                    }
                    return false;
                }

                if (!string.IsNullOrEmpty(output))
                {
                    m_log.Warning(output);
                }

                var postProcessor = new WaveCompressorProcessor(lineToBuild.FileName);
                if (!postProcessor.Process())
                {
                    this.m_log.Error(FORMAT_ERROR_COMPRESSING_FILE, lineToBuild.FileName);
                    if (null != postProcessor.Error)
                    {
                        this.m_log.Exception(postProcessor.Error);
                    }

                    return false;
                }
                
                numLinesBuilt++;
            }

            var end = DateTime.Now;
            m_log.WriteFormatted(FORMAT_COMPLETED_SPEECH_SYNTHESIS, end - start, linesToBuild.Count);
            return true;
        }

        #endregion
    }
}