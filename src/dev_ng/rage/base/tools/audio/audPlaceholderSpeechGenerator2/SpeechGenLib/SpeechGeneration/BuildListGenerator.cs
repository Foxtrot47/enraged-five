using System;
using System.Collections.Generic;
using System.IO;
using rage.ToolLib.Logging;
using SpeechGenLib.Dialogue;
using SpeechGenLib.Voices;
using SpeechGenLib.WaveLists;

namespace SpeechGenLib.SpeechGeneration
{
    public class BuildListGenerator : IBuildListGenerator
    {
        private const string VOICE = "voice";
        private const string FORMAT_VOICE_TAG_NAME = "{0}_PLACEHOLDER";
        private const string FORMAT_UNDEFINED_CHARACTER = "Undefined character: {0} referenced in conversation: {1}. Skipping character lines...";
        private const string FORMAT_RATE = "<prosody rate=\"{0}\">{1}</prosody>";
        private readonly IBuiltWavesRepository m_builtWavesRepository;
        private readonly ICharacterVoiceRepository m_characterVoiceRepository;
        private readonly IDialogueRepository m_dialogueRepository;
        private readonly SpeechGenerationParams m_generationParams;
        private readonly ILog m_log;
        private readonly string m_outputPath;
        private readonly WaveAssetCleanup m_waveAssetCleanup;
        private readonly WavesFileCleanup m_wavesFileCleanup;

        public BuildListGenerator(ILog log,
                                  ICharacterVoiceRepository characterVoiceRepository,
                                  IBuiltWavesRepository builtWavesRepository,
                                  IDialogueRepository dialogueRepository,
                                  SpeechGenerationParams generationParams)
        {
            if (log == null)
            {
                throw new ArgumentNullException("log");
            }
            if (characterVoiceRepository == null)
            {
                throw new ArgumentNullException("characterVoiceRepository");
            }
            if (builtWavesRepository == null)
            {
                throw new ArgumentNullException("builtWavesRepository");
            }
            if (dialogueRepository == null)
            {
                throw new ArgumentNullException("dialogueRepository");
            }
            if (generationParams == null)
            {
                throw new ArgumentNullException("generationParams");
            }
            if (string.IsNullOrEmpty(generationParams.OutputPath) || string.IsNullOrEmpty(generationParams.PackName) ||
                string.IsNullOrEmpty(generationParams.BankFolderName))
            {
                throw new ArgumentException("OutputPath, PackName and BankFolderName cannot be null or empty strings.");
            }

            m_log = log;
            m_characterVoiceRepository = characterVoiceRepository;
            m_builtWavesRepository = builtWavesRepository;
            m_dialogueRepository = dialogueRepository;
            m_generationParams = generationParams;

            m_outputPath = generationParams.OutputPath;
            m_waveAssetCleanup = new WaveAssetCleanup(m_outputPath);
            m_wavesFileCleanup = new WavesFileCleanup(log, characterVoiceRepository, dialogueRepository);
        }

        #region IBuildListGenerator Members

        public BuildListResults Generate()
        {
            var results = new BuildListResults();

            IPack builtPack;
            var pendingPack = CreatePendingPack(m_generationParams.PackName, out builtPack);

            IBankFolder builtBankFolder;
            var pendingBankFolder = CreatePendingBankFolder(
                m_generationParams.BankFolderName,
                builtPack,
                out builtBankFolder);
            if(builtBankFolder==null) m_log.Warning("Couldn't find built bank folder");
            var bankFolderPath = Path.Combine(m_outputPath, m_generationParams.PackName, m_generationParams.BankFolderName);

            ProcessDialogues(results, pendingPack, bankFolderPath, builtBankFolder, pendingBankFolder);
            var needsBuild = m_wavesFileCleanup.Cleanup(builtBankFolder, pendingBankFolder);
            if (results.LinesToBuild.Count > 0 || needsBuild)
            {
                pendingPack.Add(pendingBankFolder);
                results.Pack = pendingPack;

                if (needsBuild)
                {
                    results.WavesToDelete.AddRange(m_waveAssetCleanup.GetWavesToDelete(pendingPack));
                }
            }
            return results;
        }

        #endregion

        private static bool BankBelongsInPack(string bankId, IPack pack)
        {
            if (string.IsNullOrEmpty(bankId))
            {
                throw new Exception("Expected bank name to be able to determine if it belongs in pack");
            }

            var startChar = bankId.Substring(0, 1).ToUpper();
            return pack.AllowedBankNameStartLetters.Contains(startChar);
        }

        private void ProcessDialogues(BuildListResults results,
            IPack pendingPack,
            string bankFolderPath,
            IBankFolder builtBankFolder,
            IBankFolder pendingBankFolder)
        {
            foreach (var dialogue in m_dialogueRepository.Dialogues)
            {
                // Only want to include banks that belong in the current pending pack for this run
                if (!BankBelongsInPack(dialogue.Id, pendingPack))
                {
                    continue;
                }

                IBank builtBank;
                var pendingBank = CreatePendingBank(dialogue.Id, builtBankFolder, out builtBank);

                if (builtBank == null || dialogue.TimeStamp >= m_generationParams.LastBuildTime ||
                    m_generationParams.Rebuild)
                {
                    var bankLines = new List<LineToBuild>();
                    var bankPath = Path.Combine(bankFolderPath, dialogue.Id);

                    ProcessConversations(dialogue, bankLines, bankPath, builtBank, pendingBank);

                    if (bankLines.Count > 0)
                    {
                        results.LinesToBuild.AddRange(bankLines);
                        pendingBankFolder.Add(pendingBank);
                    }
                }
            }
        }

        private void ProcessConversations(IDialogue dialogue,
                                          List<LineToBuild> bankLines,
                                          string bankPath,
                                          IBank builtBank,
                                          IBank pendingBank)
        {
            foreach (var conversationCollection in dialogue.ConversationCollections)
            {
                IWaveFolder builtWaveFolder;
                var pendingWaveFolder = CreatePendingWaveFolder(conversationCollection.Id,
                                                                builtBank,
                                                                out builtWaveFolder);
                var waveFolderLines = new List<LineToBuild>();
                var waveFolderPath = Path.Combine(bankPath, conversationCollection.Id);

                ProcessLines(conversationCollection, builtWaveFolder, waveFolderPath, waveFolderLines, pendingWaveFolder);

                if (waveFolderLines.Count > 0)
                {
                    bankLines.AddRange(waveFolderLines);
                    pendingBank.Add(pendingWaveFolder);
                }
            }
        }

        private void ProcessLines(IConversationCollection conversationCollection,
                                  IWaveFolder builtWaveFolder,
                                  string waveFolderPath,
                                  List<LineToBuild> waveFolderLines,
                                  IWaveFolder pendingWaveFolder)
        {
            var characterLines = BuildListGenerationUtility.CreateCharacterLines(conversationCollection);
            foreach (var entry in characterLines)
            {
                var characterName = m_characterVoiceRepository.GetCharacterName(entry.Key);
                if (characterName == null)
                {
                    m_log.Warning(FORMAT_UNDEFINED_CHARACTER, entry.Key, conversationCollection.Id);
                    continue;
                }

                IWaveFolder builtCharacterWaveFolder;
                var pendingCharacterWaveFolder = CreatePendingWaveFolder(characterName,
                                                                         builtWaveFolder,
                                                                         out builtCharacterWaveFolder);
                pendingCharacterWaveFolder.Add(CreatePendingTag(VOICE,
                                                                string.Format(FORMAT_VOICE_TAG_NAME, characterName),
                                                                builtCharacterWaveFolder));
                var characterWaveFolderLines = new List<LineToBuild>();
                var characterWaveFolderPath = Path.Combine(waveFolderPath, characterName);

                foreach (var line in entry.Value)
                {
                    if (string.IsNullOrEmpty(line.Text))
                    {
                        continue;
                    }

                    IWave builtWave;
                    var pendingWave = CreatePendingWave(line.WaveFileName, builtCharacterWaveFolder, out builtWave);

                    if (builtWave == null || line.TimeStamp >= m_generationParams.LastBuildTime ||
                        m_generationParams.Rebuild)
                    {
                        float? rate;
                        var speaker = m_characterVoiceRepository.GetVoiceNameAndRate(entry.Key, out rate);
                        var lineToBuild = new LineToBuild
                                              {
                                                  Speaker = speaker,
                                                  Text = GetText(line, rate),
                                                  FileName = Path.Combine(characterWaveFolderPath, line.WaveFileName),
                                                  MaxFileSize = m_generationParams.MaxFileSize
                                              };
                        characterWaveFolderLines.Add(lineToBuild);
                        pendingCharacterWaveFolder.Add(pendingWave);
                    }
                }

                if (characterWaveFolderLines.Count > 0)
                {
                    waveFolderLines.AddRange(characterWaveFolderLines);
                    pendingWaveFolder.Add(pendingCharacterWaveFolder);
                }
            }
        }

        private static string GetText(ILine line, float? rate)
        {
            if (rate != null)
            {
                return string.Format(FORMAT_RATE, rate.Value, line.Text);
            }
            return line.Text;
        }

        private IPack CreatePendingPack(string id, out IPack builtPack)
        {
            builtPack = GetPack(id);
            return new Pack(m_log, id, builtPack == null ? WaveListOperation.add : WaveListOperation.modify);
        }

        private IBankFolder CreatePendingBankFolder(string id, IPack builtPack, out IBankFolder builtBankFolder)
        {
            builtBankFolder = GetBankFolder(id, builtPack);
            return new BankFolder(m_log, id, builtBankFolder == null ? WaveListOperation.add : WaveListOperation.modify);
        }

        private IBank CreatePendingBank(string id, IBankFolder builtBankFolder, out IBank builtBank)
        {
            builtBank = GetBank(id, builtBankFolder);
            return new Bank(m_log, id, builtBank == null ? WaveListOperation.add : WaveListOperation.modify);
        }

        private IWaveFolder CreatePendingWaveFolder(string id,
                                                    IWaveFolder builtParentWaveFolder,
                                                    out IWaveFolder builtWaveFolder)
        {
            builtWaveFolder = GetWaveFolder(id, builtParentWaveFolder);
            return new WaveFolder(m_log, id, builtWaveFolder == null ? WaveListOperation.add : WaveListOperation.modify);
        }

        private IWave CreatePendingWave(string id, IWaveFolder builtWaveFolder, out IWave builtWave)
        {
            builtWave = GetWave(id, builtWaveFolder);
            return new Wave(m_log, id, builtWave == null ? WaveListOperation.add : WaveListOperation.modify);
        }

        private ITag CreatePendingTag(string id, string value, IWaveFolder builtWaveFolder)
        {
            var builtTag = GetTag(id, builtWaveFolder);
            return new Tag(m_log, id, value, builtTag == null ? WaveListOperation.add : WaveListOperation.modify);
        }

        private IPack GetPack(string id)
        {
            IPack builtPack;
            m_builtWavesRepository.TryGetItem(id, out builtPack);
            return builtPack;
        }

        private static IBankFolder GetBankFolder(string id, IBankFolder parent)
        {
            IBankFolder bankFolder = null;
            if (parent != null)
            {
                parent.TryGetBankFolder(id, out bankFolder);
            }
            return bankFolder;
        }

        private static IBank GetBank(string id, IBankFolder bankFolder)
        {
            IBank bank = null;
            if (bankFolder != null)
            {
                bankFolder.TryGetBank(id, out bank);
            }
            return bank;
        }

        private static IWaveFolder GetWaveFolder(string id, IWaveFolder parentFolder)
        {
            IWaveFolder waveFolder = null;
            if (parentFolder != null)
            {
                parentFolder.TryGetWaveFolder(id, out waveFolder);
            }
            return waveFolder;
        }

        private static IWave GetWave(string id, IWaveFolder builtWaveFolder)
        {
            IWave wave = null;
            if (builtWaveFolder != null)
            {
                builtWaveFolder.TryGetWave(id, out wave);
            }
            return wave;
        }

        private static ITag GetTag(string id, IWaveFolder builtWaveFolder)
        {
            ITag tag = null;
            if (builtWaveFolder != null)
            {
                builtWaveFolder.TryGetTag(id, out tag);
            }
            return tag;
        }
    }
}