using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using rage.ToolLib.Logging;
using rage.ToolLib.Repository;
using SpeechGenLib.SpeechGeneration;

namespace SpeechGenLib.Dialogue
{
    public class Dialogue : IDialogue
    {
        private const string FORMAT_ID = "{0}_PLACEHOLDER";
        private const string FORMAT_MISSING_ROOT_ELEMENT = "Missing <{0}> root element.";
        private const string FORMAT_MISSING_ATTRIBUTE = "Missing \"{0}\" attribute on \"{1}\" element.";
        private const string INVALID_ATTRIBUTE_VALUE = "Invalid value for \"{0}\" attribute on \"{1}\" element.";
        private readonly IRepository<string, IConversationCollection> m_conversationCollections;
        private readonly ILog m_log;

        public Dialogue(ILog log)
        {
            if (log == null)
            {
                throw new ArgumentNullException("log");
            }
            m_log = log;
            m_conversationCollections = new Repository<string, IConversationCollection>();
            TimeStamp = DateTime.MinValue;
        }

        #region IDialogue Members

        public string Id { get; private set; }

        public DateTime TimeStamp { get; private set; }

        public IEnumerable<IConversationCollection> ConversationCollections
        {
            get { return m_conversationCollections.Items; }
        }

        public bool Parse(XDocument document, bool excludePlaceholderConversation)
        {
            var dialogueElement = document.Root;

            if (dialogueElement == null ||
                dialogueElement.Name != DialogueParseTokens.MissionDialogue.ToString())
            {
                m_log.Error(FORMAT_MISSING_ROOT_ELEMENT, DialogueParseTokens.MissionDialogue);
                return false;
            }

            var idAttribute = dialogueElement.Attribute(DialogueParseTokens.id.ToString());
            if (idAttribute == null)
            {
                m_log.Error(FORMAT_MISSING_ATTRIBUTE, DialogueParseTokens.id, dialogueElement.Name);
                return false;
            }

            if (string.IsNullOrEmpty(idAttribute.Value))
            {
                m_log.Error(INVALID_ATTRIBUTE_VALUE, DialogueParseTokens.id, dialogueElement.Name);
            }

            Id = string.Format(FORMAT_ID, idAttribute.Value.ToUpper());

            var conversations =
                from conversation in dialogueElement.Descendants(DialogueParseTokens.Conversation.ToString())
                where (conversation.Attribute("category").Value != DialogueParseTokens.MoCap.ToString()
                && (null == conversation.Attribute(DialogueParseTokens.cutscene.ToString()) || 
                conversation.Attribute(DialogueParseTokens.cutscene.ToString()).Value.ToUpper() != "TRUE"))
                && (conversation.Attribute("placeholder")!=null && conversation.Attribute("placeholder").Value.Equals("False") || !excludePlaceholderConversation)
                select conversation;

            foreach (var conversationElement in conversations)
            {
                var conversation = new Conversation(m_log);
                if (!conversation.Parse(conversationElement))
                {
                    return false;
                }

                IConversationCollection conversationCollection;
                if (TryGetConversationCollection(conversation.Id, out conversationCollection))
                {
                    conversationCollection.Add(conversation);
                }
                else
                {
                    conversationCollection = new ConversationCollection {conversation};
                    m_conversationCollections.Add(conversationCollection);
                }
            }
            RemoveDuplicatesAndCalculateTimestamp();
            return true;
        }

        public bool TryGetConversationCollection(string id, out IConversationCollection conversationCollection)
        {
            return m_conversationCollections.TryGetItem(id, out conversationCollection);
        }

        #endregion

        private void RemoveDuplicatesAndCalculateTimestamp()
        {
            var lineFileNames = new HashSet<string>();
            foreach (var conversationCollection in m_conversationCollections.Items)
            {
                foreach (var conversation in conversationCollection)
                {
                    var linesToRemove = new List<ILine>();
                    foreach (var line in conversation.Lines)
                    {
                        if (line.TimeStamp > TimeStamp)
                        {
                            TimeStamp = line.TimeStamp;
                        }

                        if (lineFileNames.Contains(line.WaveFileName))
                        {
                            m_log.Warning(
                                "Detected multiple lines with the same \"filename\" attribute value of \"{0}\" in dialogue \"{1}\". Only the first line will have speech generated.",
                                line.WaveFileName,
                                Id);
                            linesToRemove.Add(line);
                            continue;
                        }
                        lineFileNames.Add(line.WaveFileName);
                    }

                    if (linesToRemove.Count > 0)
                    {
                        foreach (var line in linesToRemove)
                        {
                            conversation.Remove(line);
                        }
                    }
                }
            }
        }
    }
}