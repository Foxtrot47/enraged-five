using System.Collections.Generic;
using rage.ToolLib.Repository;

namespace SpeechGenLib.Dialogue
{
    public interface IConversationCollection : IRepositoryItem<string>,
                                               IList<IConversation>
    {
    }
}