﻿using System;
using rage.ToolLib.WorkPool;

namespace SpeechGenLib.SpeechGeneration
{
    internal class BuildState : IWorkItem
    {
        public LineToBuild Line { get; set; }

        #region IWorkItem Members

        public Exception Error { get; set; }

        public WorkItemOutput Output { get; set; }

        public bool IsFinished { get; set; }

        #endregion
    }
}