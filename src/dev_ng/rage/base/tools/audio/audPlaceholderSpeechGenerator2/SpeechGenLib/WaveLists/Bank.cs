﻿using System.Xml.Linq;
using rage.ToolLib.Logging;

namespace SpeechGenLib.WaveLists
{
    public class Bank : WaveFolder,
                        IBank
    {
        private const string BANK = "Bank";

        public Bank(ILog log) : base(log)
        {
        }

        public Bank(ILog log, string id, WaveListOperation operation) : base(log, id, operation)
        {
        }

        #region IBank Members

        public override XElement Serialize()
        {
            var element = new XElement(BANK);
            if (DoSerialization(element) ||
                Operation != WaveListOperation.none)
            {
                return element;
            }
            return null;
        }

        #endregion
    }
}