using System;
using System.Collections.Generic;
using System.Xml.Linq;
using rage.ToolLib.Logging;

namespace SpeechGenLib.Voices
{
    public class CharacterVoiceRepository : ICharacterVoiceRepository
    {
        private const string FORMAT_DEFAULT_VOICE_USED =
            "Voice not specified for character: \"{0}\". Using default voice: \"{1}\".";

        private const string FORMAT_DEFAULT_VOICE_USED_INSTEAD =
            "Voice: \"{0}\" is not installed on this machine. Using voice: \"{1}\" instead.";

        private const string LOADING_CHARACTERS = "Loading characters...";

        private readonly ILog m_log;
        private readonly IDictionary<Guid, CharacterVoice> m_lookup;
        private readonly IVoiceRepository m_voiceRepository;

        public CharacterVoiceRepository(ILog log, IVoiceRepository voiceRepository)
        {
            if (log == null)
            {
                throw new ArgumentNullException("log");
            }
            if (voiceRepository == null)
            {
                throw new ArgumentNullException("voiceRepository");
            }
            m_log = log;
            m_voiceRepository = voiceRepository;
            m_lookup = new Dictionary<Guid, CharacterVoice>();
        }

        #region ICharacterVoiceRepository Members

        public string GetVoiceNameAndRate(Guid characterId, out float? voiceRate)
        {
            voiceRate = null;
            CharacterVoice characterVoice;
            if (m_lookup.TryGetValue(characterId, out characterVoice))
            {
                voiceRate = characterVoice.VoiceRate;
                return characterVoice.VoiceName;
            }
            return m_voiceRepository.DefaultVoiceName;
        }

        public string GetCharacterName(Guid characterId)
        {
            CharacterVoice characterVoice;
            if (m_lookup.TryGetValue(characterId, out characterVoice))
            {
                return characterVoice.CharacterName;
            }
            return null;
        }

        public bool Load(string characterVoiceConfig)
        {
            m_log.Write(LOADING_CHARACTERS);
            XDocument document;
            try
            {
                document = XDocument.Load(characterVoiceConfig);
            }
            catch (Exception ex)
            {
                m_log.Exception(ex);
                return false;
            }
            return CreateLookup(document, GetVoiceRates(document));
        }

        private bool CreateLookup(XContainer document, IDictionary<string, float?> voiceRates)
        {
            var succeeded = true;
            var defaultVoiceName = m_voiceRepository.DefaultVoiceName;
            foreach (var characterElement in document.Descendants(CharacterParseTokens.Character.ToString()))
            {
                Guid id;
                string voiceName, characterName;
                ParseAttributes(characterElement, out id, out characterName, out voiceName);
                if (id == Guid.Empty)
                {
                    succeeded = false;
                    break;
                }

                float? voiceRate = null;
                if (!m_voiceRepository.Exists(voiceName))
                {
                    WriteWarning(defaultVoiceName, voiceName, characterName);
                    voiceName = defaultVoiceName;
                }
                else
                {
                    voiceRates.TryGetValue(voiceName, out voiceRate);
                }
                m_lookup.Add(id, new CharacterVoice(characterName, voiceName, voiceRate));
            }
            m_log.Write(string.Empty);
            return succeeded;
        }

        private static IDictionary<string, float?> GetVoiceRates(XContainer document)
        {
            var voiceRates = new Dictionary<string, float?>();
            foreach (var voiceElement in document.Descendants(CharacterParseTokens.Voice.ToString()))
            {
                var rateAttribute = voiceElement.Attribute(CharacterParseTokens.rate.ToString());
                if (rateAttribute != null)
                {
                    float rate;
                    if (float.TryParse(rateAttribute.Value, out rate))
                    {
                        voiceRates.Add(voiceElement.Value, Math.Min(Math.Max(rate, 0), 3));
                    }
                    else
                    {
                        voiceRates.Add(voiceElement.Value, null);
                    }
                }
                else
                {
                    voiceRates.Add(voiceElement.Value, null);
                }
            }
            return voiceRates;
        }

        #endregion

        private void WriteWarning(string defaultVoiceName, string voiceName, string characterName)
        {
            if (string.IsNullOrEmpty(voiceName))
            {
                m_log.Warning(FORMAT_DEFAULT_VOICE_USED, characterName, defaultVoiceName);
            }
            else
            {
                m_log.Warning(FORMAT_DEFAULT_VOICE_USED_INSTEAD, voiceName, defaultVoiceName);
            }
        }

        private static void ParseAttributes(XElement characterElement,
                                            out Guid id,
                                            out string characterName,
                                            out string voiceName)
        {
            id = Guid.Empty;
            characterName = null;
            voiceName = null;

            foreach (var attribute in characterElement.Attributes())
            {
                try
                {
                    var attributeType =
                        (CharacterParseTokens)
                        Enum.Parse(typeof (CharacterParseTokens), attribute.Name.ToString(), true);
                    switch (attributeType)
                    {
                        case CharacterParseTokens.Guid:
                            {
                                id = new Guid(attribute.Value);
                                break;
                            }
                        case CharacterParseTokens.Name:
                            {
                                characterName = attribute.Value.ToUpper();
                                break;
                            }
                        case CharacterParseTokens.Voice:
                            {
                                if (!string.IsNullOrEmpty(attribute.Value))
                                {
                                    voiceName = attribute.Value;
                                }
                                break;
                            }
                    }
                }
                catch
                {
                }
            }
        }
    }
}