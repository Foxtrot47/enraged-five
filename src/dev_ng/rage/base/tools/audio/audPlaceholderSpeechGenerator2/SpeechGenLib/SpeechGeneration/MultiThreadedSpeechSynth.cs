using System;
using System.Collections.Generic;
using System.Linq;
using rage.ToolLib.Logging;
using rage.ToolLib.WorkPool;

namespace SpeechGenLib.SpeechGeneration
{
    public class MultiThreadedSpeechSynth : ISpeechSynth
    {
        private const int SLEEP_TIME = 1000;
        private const int ITEMS_PER_PROCESSOR = 3;
        private const string FORMAT_ERROR_GENERATING_FILE = "Error generating: {0}.";

        private const string FORMAT_COMPLETED_SPEECH_SYNTHESIS =
            "Completed speech synthesis. Processing took: {0} for {1} lines.";

        private const string STARTED_SPEECH_SYNTHESIS = "Started speech synthesis...";
        private readonly ILog m_log;

        public MultiThreadedSpeechSynth(ILog log)
        {
            if (log == null)
            {
                throw new ArgumentNullException("log");
            }
            m_log = log;
        }

        #region ISpeechSynth Members

        public bool Synthesize(IList<LineToBuild> linesToBuild)
        {
            m_log.Write(STARTED_SPEECH_SYNTHESIS);

            var start = DateTime.Now;

            var buildStates =
                linesToBuild.Select<LineToBuild, IWorkItem>(lineToBuild => new BuildState {Line = lineToBuild}).ToList();

            var workPool = new MultiThreadedSpeechSynthWorkPool(m_log, SLEEP_TIME, ITEMS_PER_PROCESSOR);

            if (!workPool.Process(buildStates))
            {
                var buildState = workPool.ErrorItem as BuildState;
                m_log.Error(FORMAT_ERROR_GENERATING_FILE, buildState.Line.FileName);
                if (buildState.Error != null)
                {
                    m_log.Exception(buildState.Error);
                }
                return false;
            }

            var end = DateTime.Now;
            m_log.WriteFormatted(FORMAT_COMPLETED_SPEECH_SYNTHESIS, end - start, linesToBuild.Count);
            return true;
        }

        #endregion
    }
}