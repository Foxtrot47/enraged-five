namespace SpeechGenLib.WaveLists
{
    public interface IPendingWaveRepository : IWavesFile
    {
        bool Check();
    }
}