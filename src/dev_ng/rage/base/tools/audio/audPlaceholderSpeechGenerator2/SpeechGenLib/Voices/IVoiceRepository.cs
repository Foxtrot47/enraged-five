using System.Collections.Generic;

namespace SpeechGenLib.Voices
{
    public interface IVoiceRepository
    {
        bool HasVoices { get; }

        string DefaultVoiceName { get; }

        IEnumerable<string> VoiceNames { get; }

        bool Exists(string voiceName);
    }
}