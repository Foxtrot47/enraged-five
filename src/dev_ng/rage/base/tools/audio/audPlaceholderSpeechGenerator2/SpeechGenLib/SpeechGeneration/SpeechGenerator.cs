// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SpeechGenerator.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The speech generator.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SpeechGenLib.SpeechGeneration
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Xml.Linq;

    using rage.ToolLib.Logging;
    using rage.ToolLib.WavesFiles;

    using Rockstar.AssetManager.Interfaces;

    using SpeechGenLib.Dialogue;
    using SpeechGenLib.Voices;
    using SpeechGenLib.WaveLists;

    using Wavelib;

    /// <summary>
    /// The speech generator.
    /// </summary>
    public class SpeechGenerator : ISpeechGenerator
    {
        #region Constants

        /// <summary>
        /// The max speech generation attempts per file.
        /// </summary>
        private const int MaxSpeechGenAttemptsPerFile = 10;

        /// <summary>
        /// The max file delete attempts.
        /// </summary>
        private const int MaxFileDeleteAttempts = 10;

        #endregion

        #region Fields

        /// <summary>
        /// The asset manager.
        /// </summary>
        private readonly IAssetManager assetManager;

        /// <summary>
        /// The build list generator.
        /// </summary>
        private readonly IBuildListGenerator buildListGenerator;

        /// <summary>
        /// The built waves.
        /// </summary>
        private readonly IBuiltWavesRepository builtWaves;

        /// <summary>
        /// The character voice repository.
        /// </summary>
        private readonly ICharacterVoiceRepository characterVoiceRepository;

        /// <summary>
        /// The dialogue repository.
        /// </summary>
        private readonly IDialogueRepository dialogueRepository;

        /// <summary>
        /// The speech generation parameters.
        /// </summary>
        private readonly SpeechGenerationParams generationParams;

        /// <summary>
        /// The log.
        /// </summary>
        private readonly ILog log;

        /// <summary>
        /// The pending waves lookup.
        /// </summary>
        private readonly IDictionary<string, IPendingWaveRepository> pendingWavesLookup;

        /// <summary>
        /// The speech synth.
        /// </summary>
        private readonly ISpeechSynth speechSynth;

        /// <summary>
        /// The current retry count.
        /// </summary>
        private int currentRetryCount;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SpeechGenerator"/> class.
        /// </summary>
        /// <param name="log">
        /// The log.
        /// </param>
        /// <param name="assetManager">
        /// The asset manager.
        /// </param>
        /// <param name="generationParams">
        /// The generation parameters.
        /// </param>
        public SpeechGenerator(ILog log, IAssetManager assetManager, SpeechGenerationParams generationParams)
        {
            if (log == null)
            {
                throw new ArgumentNullException("log");
            }

            if (assetManager == null)
            {
                throw new ArgumentNullException("assetManager");
            }

            if (generationParams == null)
            {
                throw new ArgumentNullException("generationParams");
            }

            this.log = log;
            this.assetManager = assetManager;
            this.generationParams = generationParams;

            this.dialogueRepository = new DialogueRepository(log,generationParams.ExcludePlaceholderConversation);
            this.characterVoiceRepository = new CharacterVoiceRepository(log, new VoiceRepository(log));
            this.builtWaves = new BuiltWavesRepository(this.log, generationParams.PackName);
            this.speechSynth = new ExternalProcessSpeechSynth(this.log);

            this.buildListGenerator = new BuildListGenerator(
                log, 
                this.characterVoiceRepository, 
                this.builtWaves, 
                this.dialogueRepository, 
                generationParams);

            this.pendingWavesLookup = new Dictionary<string, IPendingWaveRepository>();

            foreach (var pendingWavesFile in generationParams.PendingWavesFiles)
            {
                this.pendingWavesLookup.Add(pendingWavesFile, new PendingWaveRepository(log, generationParams.PackName));
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Generate the speech files.
        /// </summary>
        /// <returns>
        /// The return result <see cref="int"/>.
        /// </returns>
        public int Generate()
        {
            // Get latest data
            if (!this.LoadData())
            {
                return -1;
            }

            // Generate wave files
            this.log.Write("Generating wave files...");
            var results = this.buildListGenerator.Generate();

            if (results.Pack != null)
            {
                this.log.Write(string.Format("processing pack {0}...", results.Pack.Id));

                // Create change list
                var description = string.Format(
                    "{0} - Placeholder Speech Generated Assets for {1} pack - (All Platforms)",
                    this.generationParams.ProjectName,
                    this.generationParams.PackName);

                var changeList = this.assetManager.CreateChangeList(description);

                try
                {
                    // Checkout existing files from perforce
                    this.log.Write("Checking out existing assets...");
                    var filesToAdd = this.CheckoutAssets(results, changeList);

                    // Generate wave files
                    this.log.Write(string.Format("Generating {0} wave file(s)...", results.LinesToBuild.Count));
                    if (!this.GenerateWaveFiles(results.LinesToBuild, changeList))
                    {
                        this.log.Error("Failed to generate wave files");
                        this.RevertChangelist(changeList);
                        return -1;
                    }

                    // Run post-process tasks
                    this.log.Write("Performing post process tasks...");
                    
                    this.UpdatePendingWaves(results);
                    this.AddNewAssets(changeList, filesToAdd);
                    this.RemoveUnusedAssets(changeList, results.WavesToDelete);

                    // Revert any unchanged assets in change list
                    this.log.Write("Reverting unchanged files...");
                    if (!changeList.RevertUnchanged())
                    {
                        this.log.Error("Reverting of unchanged files failed");
                    }

                    // Submit change list
                    var fileCount = changeList.Assets.Count;
                    if (fileCount > 0)
                    {
                        this.log.Write(string.Format("Submitting {0} files...", fileCount));
                        if (!changeList.Submit())
                        {
                            this.log.Error("Failed to submit files");
                            this.RevertChangelist(changeList);
                        }
                    }
                    else
                    {
                        this.log.Write("Empty change list after reverting unchanged - nothing to submit");
                        changeList.Delete();
                    }
                }
                catch (Exception ex)
                {
                    this.log.Exception(ex);
                    this.RevertChangelist(changeList);
                    return -1;
                }
            }

            return 0;
        }

        #endregion

        #region Methods
        
        /// <summary>
        /// Checkout the assets.
        /// </summary>
        /// <param name="buildListResults">
        /// The build list results.
        /// </param>
        /// <param name="changeList">
        /// The change list.
        /// </param>
        /// <returns>
        /// The list of new file paths to add <see cref="IEnumerable"/>.
        /// </returns>
        private IEnumerable<string> CheckoutAssets(BuildListResults buildListResults, IChangeList changeList)
        {
            this.log.Write("Checking out existing assets...");
            var filesToAdd = new List<string>();

            // Checkout and lock the builtwaves file to avoid anyone
            // making changes while process is running
            if (this.assetManager.ExistsAsAsset(this.generationParams.BuiltWavesPackListFile))
            {
                changeList.CheckoutAsset(this.generationParams.BuiltWavesPackListFile, true);
            }
            else
            {
                this.log.Warning(
                    "Failed to find and lock built waves pack list file: "
                    + this.generationParams.BuiltWavesPackListFile);
            }

            // Checkout and lock (or add to add list) pending wave files
            foreach (var pendingWavesFile in this.pendingWavesLookup.Keys)
            {
                if (this.assetManager.ExistsAsAsset(pendingWavesFile))
                {
                    this.log.Write("Checking out file: " + pendingWavesFile);
                    changeList.CheckoutAsset(pendingWavesFile, true);
                }
                else
                {
                    this.log.Write("Storing reference of new file: " + pendingWavesFile);
                    filesToAdd.Add(pendingWavesFile);
                }
            }

            // Checkout and lock (or add to add list) wave files to build
            foreach (var fileName in buildListResults.LinesToBuild.Select(line => line.FileName))
            {
                if (this.assetManager.ExistsAsAsset(fileName))
                {
                    this.log.Write("Checking out file: " + fileName);
                    changeList.CheckoutAsset(fileName, true);
                    if (!File.Exists(fileName))
                    {
                        this.log.Error("Checked out asset but still does not exist locally: " + fileName);
                    }
                }
                else
                {
                    this.log.Write("Storing reference of new file: " + fileName);
                    filesToAdd.Add(fileName);
                }
            }

            this.WriteBlankLine();
            return filesToAdd;
        }

        /// <summary>
        /// Generate the speech wave files.
        /// </summary>
        /// <param name="linesToBuild">
        /// The lines to build.
        /// </param>
        /// <param name="changeList">
        /// The change list.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool GenerateWaveFiles(IList<LineToBuild> linesToBuild, IChangeList changeList)
        {
            this.log.Write("Generating wave files...");

            // Generate wave files
            var success = this.speechSynth.Synthesize(linesToBuild);
            
            if (!success)
            {
                this.RevertChangelist(changeList);
                return false;
            }

            // Validate processed lines
            var failedLines = this.ValidateProcessedLines(linesToBuild);
            var failedLinesCount = failedLines.Count;
            if (failedLinesCount > 0)
            {
                this.log.Warning("{0} file(s) failed speech generation", failedLinesCount);
                if (++this.currentRetryCount <= MaxSpeechGenAttemptsPerFile)
                {
                    this.log.Write(string.Format("Retrying speech generation for {0} file(s)", failedLinesCount));
                    this.log.Write("Waiting 10s for retry...");
                    Thread.Sleep(10000);

                    // Re-generate failed wave files
                    if (!this.GenerateWaveFiles(failedLines, changeList))
                    {
                        return false;
                    }
                }
                else
                {
                    this.log.Error(
                        "Failed to succesfully process all lines to build after " + MaxSpeechGenAttemptsPerFile + " attempt(s)");
                    return false;
                }
            }

            this.WriteBlankLine();
            return true;
        }

        /// <summary>
        /// Validate the processed lines.
        /// </summary>
        /// <param name="processedLines">
        /// The processed lines.
        /// </param>
        /// <returns>
        /// The invalid lines to be reprocessed <see cref="IList"/>.
        /// </returns>
        private IList<LineToBuild> ValidateProcessedLines(ICollection<LineToBuild> processedLines)
        {
            this.log.Write(string.Format("Validating {0} generated wave file(s)...", processedLines.Count));
            var failedLines = new List<LineToBuild>();

            // This validation is required to ensure the files generated
            // by the speech generator exist and are valid wave files (the
            // process can be a little flakey and unrealiable).
            foreach (var line in processedLines)
            {
                var fileName = line.FileName;
                this.log.Write("Validating file: " + fileName);

                // Check that file exists.
                if (!File.Exists(fileName))
                {
                    this.log.Warning("File expected but not found - marking for re-run: {0}", fileName);
                    failedLines.Add(line);
                    continue;
                }

                var isValid = true;

                try
                {
                    // Check for valid wave file.
                    // This step is necessary due to unreliable wave generation process.
                    var waveFile = new bwWaveFile(fileName, false);
                    var rawData = waveFile.Data.RawData;

                    // Invalid wave files have data chunk of size 0
                    if (rawData.Length == 0)
                    {
                        this.log.Warning(
                            "Corrupt audio file detected (data size of 0) - marking for re-run: {0}", fileName);
                        failedLines.Add(line);
                        isValid = false;
                    }
                }
                catch (Exception)
                {
                    this.log.Warning("Corrupt audio file detected - marking for re-run: {0}", fileName);
                    failedLines.Add(line);
                    isValid = false;
                }

                if (isValid || !File.Exists(fileName))
                {
                    continue;
                }

                // We allow several attempts to delete the corrupt file here as
                // there is sometimes still a process locking the file, which causes
                // the re-generation step to fail later on
                for (var i = 1; i <= MaxFileDeleteAttempts; i++)
                {
                    this.log.Information("Deleting corrupt audio file (attempt {0}): {1}", i, fileName);
                    try
                    {
                        File.Delete(fileName);
                    }
                    catch (Exception)
                    {
                        this.log.Error("Failed to delete corrupt audio file: {0}", fileName);
                        if (i < 10)
                        {
                            this.log.Information(
                                "Waiting 10s to retry deletion of corrupt audio file: {0}", fileName);
                            Thread.Sleep(10 * 1000);
                        }
                        else
                        {
                            this.log.Error("Failed to delete corrupt audio file after 10 attempts: {0}", fileName);
                        }
                    }
                }
            }

            this.log.Write(string.Format("{0} generated wave file(s) failed validation", failedLines.Count));
            return failedLines;
        }

        /// <summary>
        /// The load data.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool LoadData()
        {
            this.log.Write("Loading data...");
            if (!this.LoadWaveListFiles())
            {
                return false;
            }

            if (!this.LoadCharacterVoices())
            {
                return false;
            }

            if (!this.LoadDialogue())
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Load the wave list files.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool LoadWaveListFiles()
        {
            var directory = Path.GetDirectoryName(this.generationParams.BuiltWavesPackListFile) + "\\";
            this.assetManager.GetLatest(directory, false);

            foreach (var pendingWaves in this.pendingWavesLookup)
            {
                this.assetManager.GetLatest(string.Concat(Path.GetDirectoryName(pendingWaves.Key), "\\"), false);

                if (!this.assetManager.ExistsAsAsset(pendingWaves.Key))
                {
                    if (File.Exists(pendingWaves.Key))
                    {
                        var attribs = File.GetAttributes(pendingWaves.Key);
                        if ((attribs & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                        {
                            File.SetAttributes(pendingWaves.Key, FileAttributes.Normal);
                        }
                    }

                    var doc = new XDocument(new XElement("PendingWaves"));
                    doc.Save(pendingWaves.Key);
                }

                if (!pendingWaves.Value.Load(pendingWaves.Key) || !pendingWaves.Value.Check())
                {
                    return false;
                }
            }

            var tmpPath = Path.Combine(Path.GetDirectoryName(this.generationParams.BuiltWavesPackListFile), "Temp\\");
            var combiner = new WavesFileCombiner();
            var builtWavesFile = combiner.Run(this.generationParams.BuiltWavesPackListFile, tmpPath, null);
            if (!this.builtWaves.Load(builtWavesFile))
            {
                return false;
            }

            this.WriteBlankLine();
            return true;
        }

        /// <summary>
        /// Load the character voices.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool LoadCharacterVoices()
        {
            this.assetManager.GetLatest(this.generationParams.CharacterVoiceConfig, false);
            return this.characterVoiceRepository.Load(this.generationParams.CharacterVoiceConfig);
        }

        /// <summary>
        /// Load the dialogue.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool LoadDialogue()
        {
            this.assetManager.GetLatest(this.generationParams.DialoguePath, false);
            return this.dialogueRepository.Load(this.generationParams.DialoguePath);
        }

        /// <summary>
        /// Add new assets.
        /// </summary>
        /// <param name="changeList">
        /// The change list.
        /// </param>
        /// <param name="filesToAdd">
        /// The files to add.
        /// </param>
        private void AddNewAssets(IChangeList changeList, IEnumerable<string> filesToAdd)
        {
            this.log.Write("Adding new assets to source control...");
            foreach (var file in filesToAdd)
            {
                if (File.Exists(file))
                {
                    this.log.Write("Marking file for add: " + file);
                    changeList.MarkAssetForAdd(file);
                }
                else
                {
                    this.log.Warning("File marked for add but does not exist: {0}", file);
                }
            }

            this.WriteBlankLine();
        }

        /// <summary>
        /// Remove any unused assets.
        /// </summary>
        /// <param name="changeList">
        /// The change list.
        /// </param>
        /// <param name="wavesToDelete">
        /// The waves to delete.
        /// </param>
        private void RemoveUnusedAssets(IChangeList changeList, IEnumerable<string> wavesToDelete)
        {
            this.log.Write("Deleting unused assets from source control...");
            foreach (var waveToDelete in wavesToDelete)
            {
                try
                {
                    if (this.assetManager.ExistsAsAsset(waveToDelete))
                    {
                        changeList.MarkAssetForDelete(waveToDelete);
                    }
                }
                catch (Exception ex)
                {
                    this.log.Warning("Failed to mark asset for delete: " + waveToDelete + ": " + ex.Message);
                }
            }
        }

        /// <summary>
        /// Revert the change list.
        /// </summary>
        /// <param name="changeList">
        /// The change list.
        /// </param>
        private void RevertChangelist(IChangeList changeList)
        {
            if (null == changeList)
            {
                return;
            }

            try
            {
                this.log.Write(string.Format("Reverting pending changelist {0}...", changeList.NumberAsString));
                changeList.Revert(true);
            }
            catch (Exception ex)
            {
                this.log.Exception(ex);
            }
        }

        /// <summary>
        /// Update pending waves.
        /// </summary>
        /// <param name="buildListResults">
        /// The build list results.
        /// </param>
        private void UpdatePendingWaves(BuildListResults buildListResults)
        {
            this.log.Write("Updating PendingWaves.xml files...");
            foreach (var pendingWavesEntry in this.pendingWavesLookup)
            {
                var path = pendingWavesEntry.Key;
                if (File.Exists(path))
                {
                    pendingWavesEntry.Value.Add(buildListResults.Pack);
                    pendingWavesEntry.Value.Save(pendingWavesEntry.Key);
                }
                else
                {
                    this.log.Warning("Skipping pending wave entry as file does not exist: {0}", path);
                }
            }

            this.WriteBlankLine();
        }

        /// <summary>
        /// The write blank line.
        /// </summary>
        private void WriteBlankLine()
        {
            this.log.Write(string.Empty);
        }

        #endregion
    }
}