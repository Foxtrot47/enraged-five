using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;
using rage.ToolLib.Logging;
using rage.ToolLib.Repository;

namespace SpeechGenLib.Dialogue
{
    public class DialogueRepository : IDialogueRepository
    {
        private const string DSTAR_FILTER = "*.dstar";
        private const string INVALID_DSTAR_PATH = "Invalid <DialoguePath> setting in project settings file.";
        private const string FORMAT_DUPLICATE_DIALOGUE = "Duplicate dialogue: \"{0}\" detected.";
        private const string FORMAT_LOADING = "Loading: {0}.";
        private const string LOADING_DIALOGUE = "Loading dialogue...";
        private readonly IRepository<string, IDialogue> m_dialogues;
        private readonly ILog m_log;
        private readonly bool m_excludePlaceholderConversation;

        public DialogueRepository(ILog log, bool excludePlaceholderConversation)
        {
            if (log == null)
            {
                throw new ArgumentNullException("log");
            }
            m_log = log;
            m_excludePlaceholderConversation = excludePlaceholderConversation;
            m_dialogues = new Repository<string, IDialogue>();
        }

        #region IDialogueRepository Members

        public IEnumerable<IDialogue> Dialogues
        {
            get { return m_dialogues.Items; }
        }

        public bool Load(string path)
        {
            m_log.Write(LOADING_DIALOGUE);

            if (string.IsNullOrEmpty(path) ||
                !Directory.Exists(path))
            {
                m_log.Error(INVALID_DSTAR_PATH);
                return false;
            }

            var dStarFiles = Directory.GetFiles(path, DSTAR_FILTER, SearchOption.AllDirectories);
            foreach (var dStarFile in dStarFiles)
            {
                m_log.WriteFormatted(FORMAT_LOADING, dStarFile);
                XDocument doc;
                try
                {
                    doc = XDocument.Load(dStarFile);
                }
                catch (Exception ex)
                {
                    m_log.Exception(ex);
                    return false;
                }

                var dialogue = new Dialogue(m_log);
                if (!dialogue.Parse(doc, m_excludePlaceholderConversation))
                {
                    return false;
                }

                IDialogue temp;
                if (m_dialogues.TryGetItem(dialogue.Id, out temp))
                {
                    m_log.Warning(FORMAT_DUPLICATE_DIALOGUE, dialogue.Id);
                }
                m_dialogues.Add(dialogue);
            }
            m_log.Write(string.Empty);
            return true;
        }

        public bool TryGetDialogue(string dialogueId, out IDialogue dialogue)
        {
            return m_dialogues.TryGetItem(dialogueId, out dialogue);
        }

        #endregion
    }
}