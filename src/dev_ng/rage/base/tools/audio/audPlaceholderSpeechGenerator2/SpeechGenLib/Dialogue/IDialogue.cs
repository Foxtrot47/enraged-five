using System;
using System.Collections.Generic;
using System.Xml.Linq;
using rage.ToolLib.Repository;

namespace SpeechGenLib.Dialogue
{
    public interface IDialogue : IRepositoryItem<string>
    {
        DateTime TimeStamp { get; }

        IEnumerable<IConversationCollection> ConversationCollections { get; }

        bool Parse(XDocument document, bool excludePlaceholderConversation);

        bool TryGetConversationCollection(string id, out IConversationCollection conversationCollection);
    }
}