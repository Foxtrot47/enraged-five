﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;
using rage.ToolLib.Logging;
using rage.ToolLib.Repository;

namespace SpeechGenLib.WaveLists
{
    public class BankFolder : IBankFolder
    {
        private const string ILLEGAL_CHILD_ELEMENT = "Illegal \"{0}\" child element encountered.";
        private const string NAME = "name";
        private const string FORMAT_MISSING_NAME_ATTRIBUTE = "{0} element missing \"name\" attribute.";
        private const string FORMAT_UNRECOGNIZED_OPERATION = "Unrecognized operation \"{0}\".";
        private const string OPERATION = "operation";
        private const string BANK_FOLDER = "BankFolder";
        private readonly IRepository<string, IBankFolder> m_bankFolders;
        private readonly IRepository<string, IBank> m_banks;
        private readonly ILog m_log;
        private readonly IRepository<string, ITag> m_tags;

        public BankFolder(ILog log)
        {
            if (log == null)
            {
                throw new ArgumentNullException("log");
            }
            m_log = log;

            m_bankFolders = new Repository<string, IBankFolder>();
            m_banks = new Repository<string, IBank>();
            m_tags = new Repository<string, ITag>();
            Operation = WaveListOperation.none;
        }

        public BankFolder(ILog log, string id, WaveListOperation operation) : this(log)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("id");
            }
            if (operation == WaveListOperation.none)
            {
                throw new ArgumentException(
                    "operation: This constructor should only be used if specifying a valid operation");
            }
            Id = id;
            Operation = operation;
        }

        #region IBankFolder Members

        public string Id { get; private set; }

        public WaveListOperation Operation { get; private set; }

        public bool Parse(XElement root)
        {
            if (!ParseNameAttribute(root))
            {
                return false;
            }

            if (!ParseOperationAttribute(root))
            {
                return false;
            }

            foreach (var child in root.Elements())
            {
                try
                {
                    var childName =
                        (WaveListParseTokens) Enum.Parse(typeof (WaveListParseTokens), child.Name.ToString());

                    switch (childName)
                    {
                        case WaveListParseTokens.Tag:
                            {
                                var tag = new Tag(m_log);
                                if (!tag.Parse(child))
                                {
                                    return false;
                                }
                                m_tags.Add(tag);
                                break;
                            }
                        case WaveListParseTokens.BankFolder:
                            {
                                var bankFolder = new BankFolder(m_log);
                                if (!bankFolder.Parse(child))
                                {
                                    return false;
                                }
                                m_bankFolders.Add(bankFolder);
                                break;
                            }
                        case WaveListParseTokens.Bank:
                            {
                                var bank = new Bank(m_log);
                                if (!bank.Parse(child))
                                {
                                    return false;
                                }
                                m_banks.Add(bank);
                                break;
                            }
                        default:
                            {
                                m_log.Error(ILLEGAL_CHILD_ELEMENT, childName);
                                return false;
                            }
                    }
                }
                catch
                {
                    m_log.Error(ILLEGAL_CHILD_ELEMENT, child.Name);
                    return false;
                }
            }
            return true;
        }

        public virtual XElement Serialize()
        {
            var element = new XElement(BANK_FOLDER);
            if (DoSerialization(element) ||
                Operation != WaveListOperation.none)
            {
                return element;
            }
            return null;
        }

        #endregion

        #region Bank Folders

        public IEnumerable<IBankFolder> BankFolders
        {
            get { return m_bankFolders.Items; }
        }

        public bool TryGetBankFolder(string name, out IBankFolder item)
        {
            return m_bankFolders.TryGetItem(name, out item);
        }

        public void Add(IBankFolder item)
        {
            m_bankFolders.Add(item);
        }

        public void Remove(IBankFolder item)
        {
            m_bankFolders.Remove(item);
        }

        #endregion

        #region Banks

        public IEnumerable<IBank> Banks
        {
            get { return m_banks.Items; }
        }

        public bool TryGetBank(string name, out IBank item)
        {
            return m_banks.TryGetItem(name, out item);
        }

        public void Add(IBank item)
        {
            m_banks.Add(item);
        }

        public void Remove(IBank item)
        {
            m_banks.Remove(item);
        }

        #endregion

        #region Tags

        public IEnumerable<ITag> Tags
        {
            get { return m_tags.Items; }
        }

        public bool TryGetTag(string name, out ITag item)
        {
            return m_tags.TryGetItem(name, out item);
        }

        public void Add(ITag item)
        {
            m_tags.Add(item);
        }

        public void Remove(ITag item)
        {
            m_tags.Remove(item);
        }

        #endregion

        protected bool DoSerialization(XElement element)
        {
            if (Operation != WaveListOperation.none)
            {
                element.SetAttributeValue(OPERATION, Operation.ToString());
            }
            element.SetAttributeValue(NAME, Id);

            var childAdded = false;
            foreach (var tag in m_tags.Items)
            {
                var child = tag.Serialize();
                if (child != null)
                {
                    childAdded = true;
                    element.Add(child);
                }
            }

            foreach (var bankFolder in m_bankFolders.Items)
            {
                var child = bankFolder.Serialize();
                if (child != null)
                {
                    childAdded = true;
                    element.Add(child);
                }
            }

            foreach (var bank in m_banks.Items)
            {
                var child = bank.Serialize();
                if (child != null)
                {
                    childAdded = true;
                    element.Add(child);
                }
            }
            return childAdded;
        }

        private bool ParseNameAttribute(XElement root)
        {
            var nameAttrib = root.Attribute(NAME);
            if (nameAttrib != null &&
                !string.IsNullOrEmpty(nameAttrib.Value))
            {
                Id = nameAttrib.Value;
            }
            else
            {
                m_log.Error(FORMAT_MISSING_NAME_ATTRIBUTE, root.Name);
                return false;
            }
            return true;
        }

        private bool ParseOperationAttribute(XElement root)
        {
            var operationAttrib = root.Attribute(OPERATION);
            if (operationAttrib != null &&
                !string.IsNullOrEmpty(operationAttrib.Value))
            {
                try
                {
                    Operation = (WaveListOperation) Enum.Parse(typeof (WaveListOperation), operationAttrib.Value, true);
                }
                catch
                {
                    m_log.Error(FORMAT_UNRECOGNIZED_OPERATION, operationAttrib.Value);
                    return false;
                }
            }
            return true;
        }
    }
}