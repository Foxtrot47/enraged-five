using System;
using System.Collections.Generic;
using System.Xml.Linq;
using rage.ToolLib.Logging;
using rage.ToolLib.Repository;

namespace SpeechGenLib.Dialogue
{
    public class Conversation : IConversation
    {
        private const string FORMAT_MISSING_ATTRIBUTE = "Missing \"{0}\" attribute on \"{1}\" element.";
        private const string INVALID_ATTRIBUTE_VALUE = "Invalid value for \"{0}\" attribute on \"{1}\" element.";
        private readonly IRepository<string, ILine> m_lines;
        private readonly ILog m_log;

        public Conversation(ILog log)
        {
            if (log == null)
            {
                throw new ArgumentNullException("log");
            }
            m_log = log;
            m_lines = new Repository<string, ILine>();
        }

        #region IConversation Members

        public string Id { get; private set; }

        public bool Random { get; private set; }

        public IEnumerable<ILine> Lines
        {
            get { return m_lines.Items; }
        }

        public bool Parse(XElement conversationElement)
        {
            var rootAttribute = conversationElement.Attribute(DialogueParseTokens.root.ToString());
            if (rootAttribute == null)
            {
                m_log.Error(FORMAT_MISSING_ATTRIBUTE, DialogueParseTokens.root, conversationElement.Name);
                return false;
            }

            if (string.IsNullOrEmpty(rootAttribute.Value))
            {
                m_log.Error(INVALID_ATTRIBUTE_VALUE, DialogueParseTokens.root, conversationElement.Name);
                return false;
            }

            Id = rootAttribute.Value.ToUpper();

            if (Id.Contains(" "))
            {
                m_log.Warning("Conversation ID contains whitespace: ", Id);
                Id = Id.Replace(" ", "");
            }

            var randomAttribute = conversationElement.Attribute(DialogueParseTokens.random.ToString());
            if (randomAttribute != null)
            {
                Random = bool.Parse(randomAttribute.Value);
            }

            int lineSuffixNumber = 1;
            foreach (var lineElement in conversationElement.Descendants(DialogueParseTokens.Line.ToString()))
            {
                string linesuffix = "_" + lineSuffixNumber.ToString("00");
                var line = new Line(m_log, Random, linesuffix);
                if (!line.Parse(lineElement))
                {
                    return false;
                }

                if (!line.Id.StartsWith(Line.SFX))
                {
                    m_lines.Add(line);
                }
                if (Random) lineSuffixNumber++;
            }
            return true;
        }

        public void Remove(ILine line)
        {
            m_lines.Remove(line);
        }

        #endregion
    }
}