namespace SpeechGenLib.WaveLists
{
    public enum WaveListParseTokens
    {
        PendingWaves,
        BuiltWaves,
        Tag,
        Pack,
        BankFolder,
        Bank,
        WaveFolder,
        Wave,
        Chunk,
    }
}