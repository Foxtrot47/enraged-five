namespace SpeechGenLib.SpeechGeneration
{
    public interface ISpeechGenerator
    {
        int Generate();
    }
}