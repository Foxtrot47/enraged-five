namespace SpeechGenLib.WaveLists
{
    public interface ILoad
    {
        bool IsLoaded { get; }

        bool Load(string file);

        void Unload();
    }
}