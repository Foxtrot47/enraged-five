namespace SpeechGenLib.SpeechGeneration
{
    public interface IBuildListGenerator
    {
        BuildListResults Generate();
    }
}