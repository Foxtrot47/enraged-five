﻿using System.Collections.Generic;
using rage.ToolLib.Repository;

namespace SpeechGenLib.WaveLists
{
    public interface IWaveFolder : IRepositoryItem<string>,
                                   IParse,
                                   IOperation,
                                   ISerialize
    {
        IEnumerable<IWaveFolder> WaveFolders { get; }
        IEnumerable<IWave> Waves { get; }
        IEnumerable<ITag> Tags { get; }

        bool TryGetWaveFolder(string name, out IWaveFolder item);

        void Add(IWaveFolder item);

        void Remove(IWaveFolder item);

        bool TryGetWave(string name, out IWave item);

        void Add(IWave item);

        void Remove(IWave item);

        bool TryGetTag(string name, out ITag item);

        void Add(ITag item);

        void Remove(ITag item);
    }
}