using System.Collections.Generic;
using System.Xml.Linq;
using rage.ToolLib.Repository;

namespace SpeechGenLib.Dialogue
{
    public interface IConversation : IRepositoryItem<string>
    {
        bool Random { get; }

        IEnumerable<ILine> Lines { get; }

        bool Parse(XElement conversationElement);

        void Remove(ILine line);
    }
}