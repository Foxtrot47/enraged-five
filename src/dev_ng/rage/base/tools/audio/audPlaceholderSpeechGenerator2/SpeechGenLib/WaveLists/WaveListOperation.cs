namespace SpeechGenLib.WaveLists
{
    public enum WaveListOperation
    {
// ReSharper disable InconsistentNaming
        none,
        add,
        modify,
        remove
// ReSharper restore InconsistentNaming
    }
}