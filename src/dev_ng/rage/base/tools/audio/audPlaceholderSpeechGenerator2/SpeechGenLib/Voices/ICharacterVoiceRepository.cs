using System;

namespace SpeechGenLib.Voices
{
    public interface ICharacterVoiceRepository
    {
        string GetVoiceNameAndRate(Guid characterId, out float? voiceRate);

        string GetCharacterName(Guid characterId);

        bool Load(string characterVoiceConfig);
    }
}