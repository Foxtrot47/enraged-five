using System;
using System.Collections.Generic;
using System.IO;
using rage.ToolLib.Logging;
using SpeechGenLib.Processing;
using System.Diagnostics;

namespace SpeechGenLib.SpeechGeneration
{
    public class ExternalProcessSpeechSynth : ISpeechSynth
    {
        private const string FORMAT_ERROR_COMPRESSING_FILE = "Error compressing: {0}.";

        private const string FORMAT_COMPLETED_SPEECH_SYNTHESIS =
            "Completed speech synthesis. Processing took: {0} for {1} lines.";

        private const string STARTED_SPEECH_SYNTHESIS = "Started speech synthesis...";
        private readonly ILog m_log;

        public ExternalProcessSpeechSynth(ILog log)
        {
            if (log == null)
            {
                throw new ArgumentNullException("log");
            }
            m_log = log;
        }

        #region ISpeechSynth Members

        public bool Synthesize(IList<LineToBuild> linesToBuild)
        {
            m_log.Write(STARTED_SPEECH_SYNTHESIS);

            var start = DateTime.Now;

            int numLinesToBuild = linesToBuild.Count;
            int numLinesBuilt = 0;
            bool showProgress = Environment.UserInteractive;
            string numLinesToBuildStr = numLinesToBuild.ToString().PadLeft(5);

            foreach (var lineToBuild in linesToBuild)
            {
                var directory = Path.GetDirectoryName(lineToBuild.FileName);
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                if (showProgress)
                {                    
                    string numLinesBuiltStr = numLinesBuilt.ToString().PadLeft(5);
                    int percentComplete = (numLinesBuilt * 100) / numLinesToBuild;
                    string completionStr = percentComplete.ToString().PadLeft(3);
                                        
                    Console.SetCursorPosition(0, Console.CursorTop);
                    Console.Write("Synthesized {0} / {1}: {2}% complete - {3} {4}", numLinesBuiltStr, numLinesToBuildStr, completionStr, lineToBuild.Speaker, Path.GetFileNameWithoutExtension(lineToBuild.FileName));
                }


                Process buildProcess = new Process();
                String parameters = "-s " + lineToBuild.MaxFileSize;
                parameters += " -t \""+lineToBuild.Text+"\"";
                parameters += " -f \"" + lineToBuild.FileName + "\"";
                parameters += " -v \"" + lineToBuild.Speaker + "\"";

                buildProcess.StartInfo = new ProcessStartInfo("TextToSpeech.exe", parameters);

                buildProcess.OutputDataReceived += new DataReceivedEventHandler(TextToSpeechOutputDataReceived);
                buildProcess.EnableRaisingEvents = true;
                buildProcess.StartInfo.CreateNoWindow = true;
                buildProcess.StartInfo.UseShellExecute = false;
                buildProcess.StartInfo.RedirectStandardOutput = true;
                buildProcess.StartInfo.RedirectStandardError = true;

                try
                {
                    buildProcess.Start();
                }
                catch (Exception ex) {
                    this.m_log.Error("Exception occurred while starting the process (" + ex.Message + ")");
                    return false;
                }
                buildProcess.BeginOutputReadLine();
                buildProcess.WaitForExit();
                if (buildProcess.ExitCode != 0) {
                    this.m_log.Error("TextToSpeech.exe exited with an error");
                    return false;
                }

                var postProcessor = new WaveCompressorProcessor(lineToBuild.FileName);
                if (!postProcessor.Process())
                {
                    this.m_log.Error(FORMAT_ERROR_COMPRESSING_FILE, lineToBuild.FileName);
                    if (null != postProcessor.Error)
                    {
                        this.m_log.Exception(postProcessor.Error);
                    }

                    return false;
                }
                
                numLinesBuilt++;
            }

            var end = DateTime.Now;
            m_log.WriteFormatted(FORMAT_COMPLETED_SPEECH_SYNTHESIS, end - start, linesToBuild.Count);
            return true;
        }

        void TextToSpeechOutputDataReceived(object o, DataReceivedEventArgs arg) {
            this.m_log.Information(arg.Data);
        }

        #endregion
    }
}