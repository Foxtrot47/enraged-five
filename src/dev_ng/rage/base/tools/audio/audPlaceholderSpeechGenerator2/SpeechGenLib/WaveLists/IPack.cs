namespace SpeechGenLib.WaveLists
{
    using System.Collections.Generic;

    public interface IPack : IBankFolder
    {
        IList<string> AllowedBankNameStartLetters { get; }
    }
}