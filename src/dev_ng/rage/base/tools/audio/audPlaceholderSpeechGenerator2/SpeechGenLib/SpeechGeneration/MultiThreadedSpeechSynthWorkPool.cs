using System;
using System.IO;
using System.Threading;
using rage.ToolLib.Logging;
using rage.ToolLib.WorkPool;
using SpeechGenLib.Processing;

namespace SpeechGenLib.SpeechGeneration
{
    internal class MultiThreadedSpeechSynthWorkPool : WorkPool
    {
        public MultiThreadedSpeechSynthWorkPool(ILog log, int sleepTime, int workItemsPerProcessor)
            : base(log, sleepTime, workItemsPerProcessor)
        {
        }

        protected override void DoWork(object state)
        {
            var buildState = state as BuildState;
            try
            {
                var lineToBuild = buildState.Line;
                var directory = Path.GetDirectoryName(lineToBuild.FileName);
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                var processor = new TextToSpeechFileSizeLimitedProcessor(lineToBuild.Speaker,
                                                                         lineToBuild.FileName,
                                                                         lineToBuild.MaxFileSize);
                string output;
                if (!processor.Process(lineToBuild.Text, out output))
                {
                    m_lock.TryEnterWriteLock(Timeout.Infinite);
                    buildState.Error = processor.Error;
                    m_lock.ExitWriteLock();
                }
                else if (!string.IsNullOrEmpty(output))
                {
                    buildState.Output = new WorkItemOutput {Type = WorkItemOutputTypes.Warning, Message = output};
                }

                var postProcessor = new WaveCompressorProcessor(lineToBuild.FileName);
                if (!postProcessor.Process())
                {
                    m_lock.TryEnterWriteLock(Timeout.Infinite);
                    buildState.Error = postProcessor.Error;
                    m_lock.ExitWriteLock();
                }
            }
            catch (Exception ex)
            {
                m_lock.TryEnterWriteLock(Timeout.Infinite);
                buildState.Error = ex;
                m_lock.ExitWriteLock();
            }
            finally
            {
                m_lock.TryEnterWriteLock(Timeout.Infinite);
                buildState.IsFinished = true;
                m_lock.ExitWriteLock();
            }
        }
    }
}