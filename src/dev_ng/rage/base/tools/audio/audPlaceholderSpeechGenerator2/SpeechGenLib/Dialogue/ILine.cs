using System;
using System.Xml.Linq;
using rage.ToolLib.Repository;

namespace SpeechGenLib.Dialogue
{
    public interface ILine : IRepositoryItem<string>
    {
        bool Random { get; }

        Guid Speaker { get; }

        string Text { get; }

        string WaveFileName { get; }

        DateTime TimeStamp { get; }

        bool Parse(XElement lineElement);
    }
}