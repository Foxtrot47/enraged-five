﻿using System;
using System.Xml.Linq;
using rage.ToolLib.Logging;

namespace SpeechGenLib.WaveLists
{
    public class Wave : IWave
    {
        private const string NAME = "name";
        private const string FORMAT_MISSING_NAME_ATTRIBUTE = "{0} element missing \"name\" attribute.";
        private const string OPERATION = "operation";
        private const string WAVE = "Wave";
        private readonly ILog m_log;

        public Wave(ILog log)
        {
            if (log == null)
            {
                throw new ArgumentNullException("log");
            }
            m_log = log;
            Operation = WaveListOperation.none;
        }

        public Wave(ILog log, string id, WaveListOperation operation) : this(log)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("id");
            }
            if (operation == WaveListOperation.none)
            {
                throw new ArgumentException(
                    "operation: This constructor should only be used if specifying a valid operation");
            }
            Id = id;
            Operation = operation;
        }

        #region IWave Members

        public string Id { get; private set; }

        public WaveListOperation Operation { get; private set; }

        public bool Parse(XElement root)
        {
            if (!ParseNameAttribute(root))
            {
                return false;
            }
            return true;
        }

        public XElement Serialize()
        {
            var hasValidOperation = Operation != WaveListOperation.none;
            if (hasValidOperation)
            {
                var element = new XElement(WAVE);
                element.SetAttributeValue(OPERATION, Operation.ToString());
                element.SetAttributeValue(NAME, Id);
                return element;
            }
            return null;
        }

        #endregion

        private bool ParseNameAttribute(XElement root)
        {
            var nameAttrib = root.Attribute(NAME);
            if (nameAttrib != null &&
                !string.IsNullOrEmpty(nameAttrib.Value))
            {
                Id = nameAttrib.Value;
            }
            else
            {
                m_log.Error(FORMAT_MISSING_NAME_ATTRIBUTE, root.Name);
                return false;
            }
            return true;
        }
    }
}