using System;
using System.Xml.Linq;
using rage.ToolLib.Logging;

namespace SpeechGenLib.Dialogue
{
    public class Line : ILine
    {
        public const string SFX = "SFX_";
        private const string FORMAT_MISSING_ATTRIBUTE = "Missing \"{0}\" attribute on \"{1}\" element.";
        private const string INVALID_ATTRIBUTE_VALUE = "Invalid value for \"{0}\" attribute on \"{1}\" element.";
        private const string BLANK_LINE = "Blank Line";
        private const string BLANK_LINE_WARNING =
            "Generating \"Blank Line\" for line: {0} with speaker: {1}. Invalid dialogue attribute was: {2}";

        private readonly ILog m_log;
        private readonly string m_linesuffix;

        public Line(ILog log, bool random, string linesuffix)
        {
            if (log == null)
            {
                throw new ArgumentNullException("log");
            }
            m_log = log;
            Random = random;
            m_linesuffix = linesuffix;
        }

        #region ILine Members

        public string Id { get; private set; }
        public bool Random { get; private set; }
        public Guid Speaker { get; private set; }
        public string Text { get; private set; }
        public string WaveFileName { get { return Id +".WAV"; } }
        public DateTime TimeStamp { get; private set; }

        public bool Parse(XElement lineElement)
        {
            string id;
            XAttribute fileNameAttrib;
            if (ParseAttribute(lineElement, DialogueParseTokens.filename, out fileNameAttrib))
            {
                id = fileNameAttrib.Value.ToUpper()+m_linesuffix;
                if (id.StartsWith(SFX))
                {
                    // This is a special line that we do not need to process so early out
                    Id = id;
                    return true;
                }
            }
            else
            {
                return false;
            }

            XAttribute guidAttrib;
            if (!ParseAttribute(lineElement, DialogueParseTokens.guid, out guidAttrib))
            {
                return false;
            }

            string text;
            XAttribute dialogueAttrib;
            if (ParseAttribute(lineElement, DialogueParseTokens.dialogue, out dialogueAttrib))
            {
                CheckForValidDialogueXml(dialogueAttrib);
                text = dialogueAttrib.Value.Trim();
                //remove invalid chars (like \")
                text = text.Replace("\"", "");
            }
            else
            {
                m_log.Warning(BLANK_LINE_WARNING, id, guidAttrib.Value, dialogueAttrib);
                text = BLANK_LINE;
            }

            var timeStampValue = DateTime.MaxValue;
            XAttribute timeStampAttrib;
            if (ParseAttribute(lineElement, DialogueParseTokens.timestamp, out timeStampAttrib))
            {
                DateTime timeStamp;
                if (DateTime.TryParse(timeStampAttrib.Value, out timeStamp))
                {
                    timeStampValue = timeStamp;
                }
            }

            Id = id;
            Speaker = new Guid(guidAttrib.Value);
            Text = text;
            TimeStamp = timeStampValue;
            return true;
        }

        #endregion

        private static void CheckForValidDialogueXml(XAttribute textAttribute)
        {
            try
            {
                var tempElem = new XElement("root");
                tempElem.SetAttributeValue(textAttribute.Name, textAttribute.Value);
                XDocument.Parse(tempElem.ToString());
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Invalid XML detected in dialogue line!", ex);
            }
        }

        private bool ParseAttribute(XElement lineElement, DialogueParseTokens attributeName, out XAttribute attribute)
        {
            attribute = lineElement.Attribute(attributeName.ToString());
            if (attribute == null)
            {
                m_log.Error(FORMAT_MISSING_ATTRIBUTE, attributeName, lineElement.Name);
                return false;
            }

            if (string.IsNullOrEmpty(attribute.Value))
            {
                m_log.Error(INVALID_ATTRIBUTE_VALUE, attributeName, lineElement.Name);
                return false;
            }
            return true;
        }
    }
}