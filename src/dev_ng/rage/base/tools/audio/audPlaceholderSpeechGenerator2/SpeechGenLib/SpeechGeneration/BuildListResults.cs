using System.Collections.Generic;
using SpeechGenLib.WaveLists;

namespace SpeechGenLib.SpeechGeneration
{
    public class BuildListResults
    {
        public BuildListResults()
        {
            LinesToBuild = new List<LineToBuild>();
            WavesToDelete = new List<string>();
        }

        public List<LineToBuild> LinesToBuild { get; private set; }
        public IPack Pack { get; set; }
        public List<string> WavesToDelete { get; private set; }
    }
}