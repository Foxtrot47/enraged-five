using System;
using System.Collections.Generic;
using SpeechGenLib.Dialogue;

namespace SpeechGenLib.SpeechGeneration
{
    public static class BuildListGenerationUtility
    {

        public static IEnumerable<KeyValuePair<Guid, IList<ILine>>> CreateCharacterLines(
            IEnumerable<IConversation> conversations)
        {
            var actorLines = new Dictionary<Guid, IList<ILine>>();
            foreach (var conversation in conversations)
            {
                foreach (var line in conversation.Lines)
                {
                    IList<ILine> lineList;
                    if (!actorLines.TryGetValue(line.Speaker, out lineList))
                    {
                        lineList = new List<ILine>();
                        actorLines.Add(line.Speaker, lineList);
                    }
                    lineList.Add(line);
                }
            }
            return actorLines;
        }

    }
}