using System.Collections.Generic;

namespace SpeechGenLib.Dialogue
{
    public interface IDialogueRepository
    {
        IEnumerable<IDialogue> Dialogues { get; }

        bool Load(string path);

        bool TryGetDialogue(string dialogueId, out IDialogue dialogue);
    }
}