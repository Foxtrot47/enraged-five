﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using rage.ToolLib.CmdLine;
using SpeechGenLib.Processing;

namespace TextToSpeech
{
    class Program
    {
        static void Main(string[] args)
        {
            CmdLineParser parameterParser = new CmdLineParser(args);

            if (parameterParser.Arguments.Count == 0) {
                displayHelp();
                return;
            }

            string voice = "";
            string filename = "";
            uint maxFilesize = 0;
            string text = "";

            if (parameterParser.Arguments.ContainsKey("v")) {
                if (!parameterParser.Arguments["v"].Equals("true")) {
                    //the voice parameter is not ""
                    voice = parameterParser.Arguments["v"];
                }
            }
            
            if (parameterParser.Arguments.ContainsKey("f")) {
                filename = parameterParser.Arguments["f"];
            }
            else {
                Console.Error.WriteLine("No filename specified");
                Environment.ExitCode = - 1;
                return;
            }

            if (parameterParser.Arguments.ContainsKey("s")) {
                try {
                    maxFilesize = uint.Parse(parameterParser.Arguments["s"]);
                }
                catch {
                    Console.Error.WriteLine("Size could not be converted into a number");
                    Environment.ExitCode = -1;
                    return;
                }
            }
            else {
                Console.Error.WriteLine("No max size specified");
                Environment.ExitCode = -1;
                return;
            }

            if (parameterParser.Arguments.ContainsKey("t")) {
                text = parameterParser.Arguments["t"];
            }
            else {
                Console.Error.WriteLine("No text specified");
                Environment.ExitCode = -1;
                return;
            }


            TextToSpeechFileSizeLimitedProcessor processor = new TextToSpeechFileSizeLimitedProcessor(
                voice,
                filename,
                maxFilesize);

            string output;
            if (!processor.Process(text, out output))
            {
                Console.Error.WriteLine("Error generating "+filename);
                if (processor.Error != null)
                {
                    Console.Error.WriteLine(processor.Error.Message);
                }
                Environment.ExitCode = -1;
                return;
            }
            Console.Out.WriteLine(output);
        }

        private static void displayHelp() {
            Console.Out.WriteLine("Options:");
            Console.Out.WriteLine("     -v voice");
            Console.Out.WriteLine("     -f filename");
            Console.Out.WriteLine("     -s max file size");
            Console.Out.WriteLine("     -t text");
        }
    }
}
