﻿using System;
using System.Xml.Serialization;

namespace Excel
{
    /// <summary>
    /// (c) 2014 Vienna, Dietmar Schoder
    /// 
    /// Code Project Open License (CPOL) 1.02
    /// 
    /// Edited by Danjeli.Schembri.
    /// Deals with an Excel Worksheet in an xlsx-file
    /// </summary>
    [Serializable()]
    [XmlType(Namespace = "http://schemas.openxmlformats.org/spreadsheetml/2006/main")]
    [XmlRoot("worksheet", Namespace = "http://schemas.openxmlformats.org/spreadsheetml/2006/main")]
    public class Worksheet
    {
        [XmlArray("sheetData")]
        [XmlArrayItem("row")]
        public Row[] Rows;
        [XmlIgnore]
        public int NumberOfColumns; // Total number of columns in this Worksheet

        public static int MaxColumnIndex = 0; // Temporary variable for import

        public Worksheet()
        {
        }

        public void ExpandRows()
        {
            if (Rows == null)
            {
                return;
            }
            foreach (var row in Rows)
                row.ExpandCells(NumberOfColumns);
        }
    }
}
