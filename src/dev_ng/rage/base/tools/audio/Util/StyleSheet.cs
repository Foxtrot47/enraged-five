﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace Excel
{
    [Serializable]
    [XmlType(Namespace = "http://schemas.openxmlformats.org/spreadsheetml/2006/main")]
    [XmlRoot("styleSheet",Namespace = "http://schemas.openxmlformats.org/spreadsheetml/2006/main")]
    public class StyleSheet
    {

        [XmlArray("fonts")]
        [XmlArrayItem("font")]
        public Font[] fonts;
        
        [XmlArray("cellXfs")]
        [XmlArrayItem("xf")]
        public Xf[] CellXfs;

    }


    public class Xf
    {
        [XmlAttribute("fontId")]
        public string fontId;
    }

    
    public class Font
    {
        [XmlElement("strike")]
        public string content { get { return ""; } set { Strike = true; } }

        public bool Strike;
    }

}
