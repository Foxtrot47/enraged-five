﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtraAWCChecker
{
    class AWCHelper
    {
        public static bool CheckAWCInXML(FileInfo xml, string awcFileName)
        {
            return PackFileHelper.GetBankNames(xml).Contains(awcFileName, StringComparer.CurrentCultureIgnoreCase);
        }

        public static FileInfo GetPackFile(FileInfo file)
        {
            string fileName = file.Directory.Name + "_PACK_FILE.XML";
            //dev_ng/runtime/<platform>/sfx
            //dev_ng/build/<platform>/BuiltWaves/...PACK_FILE.xml
            string platform = file.Directory.Parent.Parent.Name;
            string directory = file.Directory.Parent.Parent.Parent.Parent.FullName + "\\build\\" + platform + "\\BuiltWaves\\";

            return new FileInfo(directory + fileName);
        }
    }
}
