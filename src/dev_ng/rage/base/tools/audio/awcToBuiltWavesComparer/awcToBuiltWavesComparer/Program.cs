﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;

namespace ExtraAWCChecker
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 2)
            {
                if (!Directory.Exists(Path.GetDirectoryName(args[1])))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(args[1]));
                }
                ExtraAWCs extraAWCList = new ExtraAWCs(args[0]);

                string joined = string.Join("\n", extraAWCList.extraAWC.ToArray());
                File.WriteAllText(args[1], joined);
            }
            else
            {
                Console.WriteLine(@"The application requires two arguments: SFX path (such as x:\gta5\audio\dev_ng\runtime\) and a csv file output path");
            }
        }
    }
}
