﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;


namespace ExtraAWCChecker
{
    class PackFileHelper
    {
        static private Dictionary<string, List<string>> fileInfoBankNames = new Dictionary<string, List<string>>();

        static public IEnumerable<string> GetBankNames(FileInfo fileInfo)
        {
            if (!fileInfoBankNames.ContainsKey(fileInfo.FullName))
            {
                List<string> bankNames = new List<string>();
                if (IsPackFile(fileInfo))
                {
                    XmlDocument xdoc = new XmlDocument();
                    xdoc.Load(fileInfo.FullName);
                    XmlNodeList list = xdoc.DocumentElement.SelectNodes("//Bank");
                    foreach (XmlNode xmlNode in list)
                    {
                        bankNames.Add(xmlNode.Attributes["name"].Value);
                    }
                }
                fileInfoBankNames[fileInfo.FullName] = bankNames;
            }

            return fileInfoBankNames[fileInfo.FullName];
        }

        static private bool IsPackFile(FileInfo fileInfo)
        {
            if (fileInfo.Exists && fileInfo.FullName.EndsWith("PACK_FILE.xml", StringComparison.CurrentCultureIgnoreCase))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
