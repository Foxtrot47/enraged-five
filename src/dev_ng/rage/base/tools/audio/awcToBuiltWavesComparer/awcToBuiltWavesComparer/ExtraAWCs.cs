﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtraAWCChecker
{
    class ExtraAWCs
    {
        public List<string> extraAWC = new List<string>();

        public ExtraAWCs(string path)
        {
            GetExtraAWCs(path);
        }

        public void GetExtraAWCs(string path)
        {
            foreach (FileInfo file in GetFilesInDirectory(path))
            {
                bool exists = true;
                FileInfo packFile = AWCHelper.GetPackFile(file);
                if (packFile != null)
                {
                    exists = false;
                }
                exists = AWCHelper.CheckAWCInXML(packFile, Path.GetFileNameWithoutExtension(file.FullName));

                if (!exists)
                {
                    extraAWC.Add(file.FullName);
                }
            }
        }

        private List<FileInfo> GetFilesInDirectory(string path)
        {
            DirectoryInfo dir = new DirectoryInfo(path);
            RecursiveFileSearch.WalkDirectoryTree(dir, "*.awc");

            return RecursiveFileSearch.filesInDirectory;
        }
    }
}
