//
// tools/audwavreader/listchunk.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "listchunk.h"
#include <windows.h>

audListChunk::audListChunk(unsigned int chunkid, unsigned int chunkSize, unsigned int chunkOffset, void *data) :
audRiffChunk(chunkid, chunkSize, chunkOffset, data)
{
	m_Items = data;
}

audListChunk::~audListChunk(void)
{
}

audLabel *audListChunk::GetNextLabel(audLabel *current)
{
	if(!m_Items)
	{
		return NULL;
	}

	bool isPassedCurrent = (current == NULL);
	unsigned int offset = 4;

	while(offset < m_ChunkSize)
	{
		//Note: All items have the first to elements in common, so cast to a label for now.
		audLabel *item = (audLabel *)(((unsigned char *)m_Items) + offset);

		//Force chunkID string to upper case.
		char *chunkIdString = (char *)&(item->id);
		for(int i=0; i<4; i++)
		{
			chunkIdString[i] = (char)toupper(chunkIdString[i]);
		}

		unsigned int chunkID = *((unsigned int *)chunkIdString);

		if(chunkID == LABEL_CHUNK_ID)
		{
			//This item is a label.

			if(isPassedCurrent)
			{
				return item;
			}
			else if(item == current)
			{
				isPassedCurrent = true;
			}
		}

		offset += item->size + 8 + (item->size % 2);
	}

	return NULL;
}
