//
// tools/audwavreader/testharness/main.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//
#include "../wavreader.h"
#include <windows.h>

void main()
{
	audWavReader wavReader;
	if(wavReader.Init("c:\\test.wav"))
	{
		audFmtChunk *fmtChunk = (audFmtChunk*)wavReader.GetChunk(FMT_CHUNK_ID);
		audSmplChunk *smplChunk = (audSmplChunk*)wavReader.GetChunk(SMPL_CHUNK_ID);
		audCueChunk *cueChunk = (audCueChunk*)wavReader.GetChunk(CUE_CHUNK_ID);

		audCuePoint *point = cueChunk->GetCuePoint(1);
		
		if(fmtChunk)
		{
			delete fmtChunk;
		}
		if(smplChunk)
		{
			delete smplChunk;
		}
		if(cueChunk)
		{
			delete cueChunk;
		}
	}
}
