//
// tools/audwavreader/smplchunk.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "smplchunk.h"

audSmplChunk::audSmplChunk(unsigned int chunkID, unsigned int chunkSize, unsigned int chunkOffset, void *data) :
audRiffChunk(chunkID, chunkSize, chunkOffset, data)
{
	m_SamplerChunk = (audSamplerHeader*)m_Data;
}

audSmplChunk::~audSmplChunk(void)
{
}
