//
// tools/audwavreader/riffchunkfactory.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef RIFF_CHUNK_FACTORY_H
#define RIFF_CHUNK_FACTORY_H

#include "riffchunk.h"

class audRiffChunkFactory
{
public:
	static audRiffChunk *GetRiffChunk(unsigned int chunkID, unsigned int chunkSize, unsigned int chunkOffset,
		void *data);
};

#endif // RIFF_CHUNK_FACTORY_H
