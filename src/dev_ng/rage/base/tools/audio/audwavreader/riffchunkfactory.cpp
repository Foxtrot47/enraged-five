//
// tools/audwavreader/riffchunkfactory.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "riffchunkfactory.h"

#include "chunkdefs.h"
#include "cuechunk.h"
#include "fmtchunk.h"
#include "listchunk.h"
#include "riffchunk.h"
#include "smplchunk.h"

#include "ctype.h"

audRiffChunk *audRiffChunkFactory::GetRiffChunk(unsigned int chunkID, unsigned int chunkSize,
	unsigned int chunkOffset, void *data)
{
	//Force chunkID string to upper case.
	char *chunkIdString = (char *)&chunkID;
	for(int i=0; i<4; i++)
	{
		chunkIdString[i] = (char)toupper(chunkIdString[i]);
	}

	chunkID = *((unsigned int *)chunkIdString);

	switch(chunkID)
	{
	case FMT_CHUNK_ID:
		return new audFmtChunk(chunkID, chunkSize, chunkOffset, data);
	case SMPL_CHUNK_ID:
		return new audSmplChunk(chunkID, chunkSize, chunkOffset, data);
	case CUE_CHUNK_ID:
		return new audCueChunk(chunkID, chunkSize, chunkOffset, data);
	case LIST_CHUNK_ID:
		return new audListChunk(chunkID, chunkSize, chunkOffset, data);
	default:
		return new audRiffChunk(chunkID, chunkSize, chunkOffset, data);
	}
}
