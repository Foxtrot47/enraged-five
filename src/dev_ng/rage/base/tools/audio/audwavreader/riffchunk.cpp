//
// tools/audwavreader/riffchunk.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "riffchunk.h"

audRiffChunk::audRiffChunk(unsigned int chunkID, unsigned int chunkSize, unsigned int chunkOffset, void *data) :
m_ChunkID(chunkID),
m_ChunkSize(chunkSize),
m_ChunkOffset(chunkOffset),
m_Data(data)
{
}

audRiffChunk::~audRiffChunk(void)
{
}
