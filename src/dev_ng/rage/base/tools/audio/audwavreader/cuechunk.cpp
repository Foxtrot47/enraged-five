//
// tools/audwavreader/cuechunk.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "cuechunk.h"
#include <windows.h>

audCueChunk::audCueChunk(unsigned int chunkid, unsigned int chunkSize, unsigned int chunkOffset, void *data) :
audRiffChunk(chunkid, chunkSize, chunkOffset, data)
{
	m_CueHeader = (audCueHeader*)m_Data;
}

audCueChunk::~audCueChunk(void)
{
}

audCuePoint *audCueChunk::GetCuePoint(unsigned int id)
{
	if(!m_CueHeader || m_CueHeader->numCuePoints == 0)
	{
		return NULL;
	}

	for(unsigned int i = 0; i < m_CueHeader->numCuePoints; i++)
	{
		if(m_CueHeader->cuePoints[i].id == id)
		{
			return &(m_CueHeader->cuePoints[i]);
		}
	}

	return NULL;
}
