//
// tools/audwavreader/fmtchunk.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "fmtchunk.h"

audFmtChunk::audFmtChunk(unsigned int chunkID, unsigned int chunkSize, unsigned int chunkOffset, void *data) :
audRiffChunk(chunkID, chunkSize, chunkOffset, data)
{
	m_FmtHeader = (audFmtHeader*)data;
}

audFmtChunk::~audFmtChunk(void)
{
}
