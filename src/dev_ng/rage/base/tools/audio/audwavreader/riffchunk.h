//
// tools/audwavreader/riffchunk.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef RIFF_CHUNK_H
#define RIFF_CHUNK_H

class audRiffChunk
{
public:
	audRiffChunk(unsigned int chunkID, unsigned int chunkSize, unsigned int chunkOffset, void *data);
	virtual ~audRiffChunk(void);

	unsigned int GetChunkID()
	{
		return m_ChunkID;
	}

	unsigned int GetChunkSize()
	{
		return m_ChunkSize;
	}

	unsigned int GetChunkOffset()
	{
		return m_ChunkOffset;
	}

	void *GetData()
	{
		return m_Data;
	}

protected:

	unsigned int m_ChunkID;
	unsigned int m_ChunkSize;
	unsigned int m_ChunkOffset;
	void *m_Data;

};

#endif // RIFF_CHUNK_H
