//
// tools/audwavreader/listchunk.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef LIST_CHUNK_H
#define LIST_CHUNK_H

#include "chunkdefs.h"
#include "riffchunk.h"

class audListChunk : public audRiffChunk
{
public:
	audListChunk(unsigned int chunkID, unsigned int chunkSize, unsigned int chunkOffset, void *data);
	~audListChunk(void);

	audLabel *audListChunk::GetNextLabel(audLabel *current=0);

	void *GetItems()
	{
		return m_Items;
	}

private:

	void *m_Items;
};

#endif // LIST_CHUNK_H
