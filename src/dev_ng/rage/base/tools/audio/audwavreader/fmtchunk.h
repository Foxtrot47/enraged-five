//
// tools/audwavreader/fmtchunk.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef FMT_CHUNK_H
#define FMT_CHUNK_H

#include "chunkdefs.h"
#include "riffchunk.h"

class audFmtChunk : public audRiffChunk
{
public:
	audFmtChunk(unsigned int chunkID, unsigned int chunkSize, unsigned int chunkOffset, void *data);
	~audFmtChunk(void);

	audFmtHeader *GetFmtHeader()
	{
		return m_FmtHeader;
	}


private:
	audFmtHeader *m_FmtHeader;
};

#endif // FMT_CHUNK_H
