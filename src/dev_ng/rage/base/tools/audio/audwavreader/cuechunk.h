//
// tools/audwavreader/cuechunk.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CUE_CHUNK_H
#define CUE_CHUNK_H

#include "chunkdefs.h"
#include "riffchunk.h"

class audCueChunk :	public audRiffChunk
{
public:
	audCueChunk(unsigned int chunkID, unsigned int chunkSize, unsigned int chunkOffset, void *data);
	~audCueChunk(void);

	audCuePoint *GetCuePoint(unsigned int id);

	audCueHeader *GetCueHeader()
	{
		return m_CueHeader;
	}

private:

	audCueHeader *m_CueHeader;
};

#endif // CUE_CHUNK_H
