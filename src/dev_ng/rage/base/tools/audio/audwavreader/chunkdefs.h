//
// tools/audwavreader/chunkdefs.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CHUNK_DEFS_H
#define CHUNK_DEFS_H

#pragma warning(push)
#pragma warning(disable:4200)	// unsized arrays

//We now force upper case.
#define RIFF_CHUNK_ID			(unsigned int)'FFIR'
#define WAVE_CHUNK_ID			(unsigned int)'EVAW'
#define FMT_CHUNK_ID			(unsigned int)' TMF'
#define DATA_CHUNK_ID			(unsigned int)'ATAD'
#define SMPL_CHUNK_ID			(unsigned int)'LPMS'
#define CUE_CHUNK_ID			(unsigned int)' EUC'
#define LIST_CHUNK_ID			(unsigned int)'TSIL'
#define LABEL_CHUNK_ID			(unsigned int)'LBAL'
#define ARB_DATA_XENON_CHUNK_ID	(unsigned int)'BRAX'
#define ARB_DATA_PS3_CHUNK_ID	(unsigned int)'BRAP'
#define ARB_DATA_PC_CHUNK_ID	(unsigned int)'BRAW'

typedef struct
{
	unsigned short compressionCode; 
    unsigned short numChannels; 
    unsigned int sampleRate; 
    unsigned int avgBytesPerSec; 
    unsigned short blockAlign; 
    unsigned short bitsPerSample; 

}audFmtHeader;

typedef struct
{
	unsigned int chunkId;
	unsigned int size;
	unsigned int typeId;
}audRiffHeader;

typedef struct
{
	unsigned char id[4];
	unsigned int size;
}ChunkHeader;

typedef struct {
  long  dwIdentifier;
  long  dwType;
  long  dwStart;
  long  dwEnd;
  long  dwFraction;
  long  dwPlayCount;
} audSampleLoop;

typedef struct {
  long           dwManufacturer;
  long           dwProduct;
  long           dwSamplePeriod;
  long           dwMIDIUnityNote;
  long           dwMIDIPitchFraction;
  long           dwSMPTEFormat;
  long           dwSMPTEOffset;
  long           cSampleLoops;
  long           cbSamplerData;
  audSampleLoop	 Loops[];
} audSamplerHeader;

typedef struct {
	unsigned int id;
	unsigned int position;
	unsigned int dataChunkId;
	unsigned int chunkStart;
	unsigned int blockStart;
	unsigned int sampleOffset;
}audCuePoint;

typedef struct {
	unsigned int numCuePoints;
	audCuePoint cuePoints[];
}audCueHeader;

typedef struct {
	unsigned int id;
	unsigned int size;
	unsigned int cuePointId;
	char *text;
}audLabel;

#pragma warning(pop)
#endif // CHUNK_DEFS_H
