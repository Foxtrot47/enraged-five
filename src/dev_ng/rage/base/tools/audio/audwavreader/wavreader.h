//
// tools/audwavreader/wavreader.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef WAV_READER_H
#define WAV_READER_H

#include "ChunkDefs.h"
#include "CueChunk.h"
#include "FmtChunk.h"
#include "ListChunk.h"
#include "RiffChunk.h"
#include "SmplChunk.h"
#include <vector>
#include <windows.h>

// maximum number of chunks we can have in a wav file
const int g_MaxCunks = 16;

class audWavReader
{
public:
	audWavReader();
	~audWavReader();

	bool Init(char *fileName);
	audRiffChunk *GetChunk(DWORD chunkID);
	audRiffChunk *GetNextChunk(DWORD chunkID, audRiffChunk *current=NULL);

	bool IsInitialised()
	{
		return m_Initialised;
	}

	unsigned int GetChunkCount()
	{
		return m_ChunkCount;
	}

	unsigned int GetTotalSize()
	{
		if(m_RiffHeader)
			return m_RiffHeader->size;
		else
			return 0;

	}

private:

	bool ParseHeader();

	char *m_PreloadedWaveData;
	audRiffHeader *m_RiffHeader;
	unsigned int m_ChunkCount;
	//CLinkList<audRiffChunk*> m_ChunkList;
	std::vector<audRiffChunk*> m_ChunkList;
	bool m_Initialised;

	HANDLE m_FileHandle;
};

#endif // WAV_READER_H
