//
// tools/audwavreader/smplchunk.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef SMPL_CHUNK_H
#define SMPL_CHUNK_H

#include "chunkdefs.h"
#include "riffchunk.h"

class audSmplChunk : public audRiffChunk
{
public:
	audSmplChunk(unsigned int chunkID, unsigned int chunkSize, unsigned int chunkOffset, void *data);
	~audSmplChunk(void);

	audSamplerHeader *GetSamplerHeader()
	{
		return m_SamplerChunk;
	}

private:
	audSamplerHeader *m_SamplerChunk;
};

#endif // SMPL_CHUNK_H
