//
// tools/audwavreader/wavreader.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "wavreader.h"

#include "riffchunkfactory.h"
#include <vector>
#include <windows.h>

audWavReader::audWavReader() :
m_Initialised(FALSE),
m_ChunkCount(0),
m_RiffHeader(NULL),
m_PreloadedWaveData(NULL)
{

}

audWavReader::~audWavReader()
{
	if(m_PreloadedWaveData)
	{
		delete[] m_PreloadedWaveData;
	}
	else if(m_RiffHeader)
	{
		delete m_RiffHeader;
	}

	for(unsigned int i = 0; i < m_ChunkList.size(); i++)
	{
		delete m_ChunkList[i];
	}
}

bool audWavReader::Init(char *fileName)
{
	m_Initialised = FALSE;

	m_FileHandle = CreateFile(fileName, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);

	if(m_FileHandle != INVALID_HANDLE_VALUE)
	{
		m_Initialised = ParseHeader();
		CloseHandle(m_FileHandle);
	}

	return m_Initialised;
}

bool audWavReader::ParseHeader()
{
	void *data;
	DWORD totalWaveBytes, bytesRead=0, offset=0;
	DWORD *chunkSize, *chunkID;

	if((totalWaveBytes = GetFileSize(m_FileHandle, NULL)) <= 0)
	{
		return FALSE;
	}

	m_PreloadedWaveData = new char[totalWaveBytes];
	if(!ReadFile(m_FileHandle, (void *)m_PreloadedWaveData, totalWaveBytes, &bytesRead, NULL) ||
		(bytesRead != totalWaveBytes))
	{
		return FALSE;
	}

	m_RiffHeader = (audRiffHeader *)m_PreloadedWaveData;
	bytesRead = 12;

	if(m_RiffHeader->chunkId != RIFF_CHUNK_ID || m_RiffHeader->typeId != WAVE_CHUNK_ID)
	{
		return FALSE;
	}

	offset += bytesRead;

	while(offset < m_RiffHeader->size)
	{
		chunkSize = (DWORD *)(m_PreloadedWaveData + offset + 4);
		chunkID = (DWORD *)(m_PreloadedWaveData + offset);
		data = (void *)(m_PreloadedWaveData + offset + 8);

		m_ChunkList.push_back(audRiffChunkFactory::GetRiffChunk(*chunkID, *chunkSize, offset, data));
		m_ChunkCount++;

		offset += *chunkSize + 8 + (*chunkSize % 2);
	}

	return TRUE;	
}

audRiffChunk *audWavReader::GetChunk(DWORD chunkID)
{
	return GetNextChunk(chunkID);
}

audRiffChunk *audWavReader::GetNextChunk(DWORD chunkID, audRiffChunk *current)
{
	bool isPassedCurrent = (current == NULL);

	for(unsigned int i = 0; i < m_ChunkList.size(); i++)
	{
		if(isPassedCurrent && (m_ChunkList[i]->GetChunkID() == chunkID))
		{
			return m_ChunkList[i];
		}
		else if(m_ChunkList[i] == current)
		{
			isPassedCurrent = true;
		}
	}

	return NULL;
}
