namespace WaveSlotSizeInspector
{
    using System;
    using System.Collections;
    using System.Windows.Forms;

    class ListViewItemComparer : IComparer
    {
        private int col;
        private SortOrder order;
        public ListViewItemComparer()
        {
            col = 0;
            order = SortOrder.Ascending;
        }
        public ListViewItemComparer(int column, SortOrder order)
        {
            col = column;
            this.order = order;
        }
        public int Compare(object x, object y)
        {
            int returnVal = -1;
            try
            {
                int numX = Int32.Parse(((ListViewItem)x).SubItems[col].Text);
                int numY = Int32.Parse(((ListViewItem)y).SubItems[col].Text);

                if (numX == numY) { returnVal = 0; }
                else if (numX > numY) { returnVal = 1; }
                else { returnVal = -1; }
            }
            catch
            {

                returnVal = String.Compare(((ListViewItem)x).SubItems[col].Text,
                                        ((ListViewItem)y).SubItems[col].Text);
            }
            // Determine whether the sort order is descending.
            if (order == SortOrder.Descending)
            {
                // Invert the value returned by String.Compare.
                returnVal *= -1;
            }
            return returnVal;
        }
    }
}
