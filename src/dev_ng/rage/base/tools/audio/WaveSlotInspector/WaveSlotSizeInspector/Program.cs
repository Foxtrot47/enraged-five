namespace WaveSlotSizeReporter
{
    using System;
    using System.Windows.Forms;

    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            var mainForm = new frmMain(args);
            if (mainForm.Init())
            {
                Application.Run(mainForm);
            }
        }
    }
}