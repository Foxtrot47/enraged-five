namespace WaveSlotSizeInspector
{
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Windows.Forms;

    using WaveSlotSizeReporter;

    public partial class frmScratch : Form
    {

        public frmScratch()
        {
            InitializeComponent();
            m_ListView.ColumnClick += new ColumnClickEventHandler(m_ListView_ColumnClick);
        }


        public void Init(List<HeaderSize> headers)
        {
            int m_Largest = 0;
            string m_Name = "";
            int average = 0;

            foreach (HeaderSize hs in headers)
            {
                ListViewItem lvi = m_ListView.Items.Add(new ListViewItem(hs.Name));
                lvi.SubItems.Add(hs.Size.ToString());

                average += hs.Size;
                if (hs.Size > m_Largest)
                {
                    m_Largest = hs.Size;
                    m_Name = hs.Name;
                }
            }
            average = (int)average / headers.Count;
            lblAverage.Text = "Average Header Size: " + average.ToString();
            lblLargestSize.Text = "Largest Header Size: " + m_Largest.ToString();
            lblLargestName.Text = "Largest Header Name: " + m_Name;
            lblDiff.Text = "Difference: " + (m_Largest - average);

        }      

        protected override void OnClosing(CancelEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
            //base.OnClosing(e);
        }

        private int m_SortColumn = -1;

        private void m_ListView_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            // Determine whether the column is the same as the last column clicked.
            if (e.Column != m_SortColumn)
            {
                // Set the sort column to the new column.
                m_SortColumn = e.Column;
                // Set the sort order to ascending by default.
                m_ListView.Sorting = SortOrder.Ascending;
            }
            else
            {
                // Determine what the last sort order was and change it.
                if (m_ListView.Sorting == SortOrder.Ascending)
                    m_ListView.Sorting = SortOrder.Descending;
                else
                    m_ListView.Sorting = SortOrder.Ascending;
            }

            // Call the sort method to manually sort.
            m_ListView.BeginUpdate();
            m_ListView.Cursor = Cursor = Cursors.WaitCursor;
            m_ListView.Sort();
            m_ListView.EndUpdate();
            m_ListView.Cursor = Cursor = Cursors.Default;
            // Set the ListViewItemSorter property to a new ListViewItemComparer
            // object.
            this.m_ListView.ListViewItemSorter = new ListViewItemComparer(e.Column, m_ListView.Sorting);

        }
    }
}