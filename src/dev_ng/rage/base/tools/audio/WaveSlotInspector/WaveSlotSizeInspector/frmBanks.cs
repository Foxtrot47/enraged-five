namespace WaveSlotSizeInspector
{
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Windows.Forms;

    using WaveSlotSizeReporter;

    public partial class frmBanks : Form
    {
        private WaveSlotStats m_WavesSlotStats;
        private int m_SortColumn = -1;
        private string m_Type;

        public frmBanks()
        {
            InitializeComponent();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
            //base.OnClosing(e);
        }

        public void Init(string type, WaveSlotStats wss)
        {
            m_Type = type;
            m_WavesSlotStats = wss;
            BuildList();
            ShowDetails();
        }

        private void ShowDetails()
        {
            this.Text = "Bank: " + m_WavesSlotStats.Name + " Type: " + m_WavesSlotStats.Type;
            lblName.Text = m_WavesSlotStats.Name;

            switch (m_Type)
            {
                case "Bank":
                    lblAverage.Text = "Average Bank Size: " + m_WavesSlotStats.AverageSize;
                    lblLargestSize.Text = "largest Bank Size: " + m_WavesSlotStats.MaxSize;
                    lblLargestName.Text = "Largest Bank Name: " + m_WavesSlotStats.MaxName;
                    var difference = m_WavesSlotStats.MaxSize - m_WavesSlotStats.AverageSize;
                    lblDiff.Text = "Difference: " + difference;
                    break;
                case "Header":
                    lblAverage.Text = "Average Header Size: " + m_WavesSlotStats.AverageHeader;
                    lblLargestSize.Text = "Largest Header Size: " + m_WavesSlotStats.LargestHeader;
                    lblLargestName.Text = "Bank with largest Header: " + m_WavesSlotStats.LargestHeaderName;
                    lblDiff.Text = "Difference: " + (m_WavesSlotStats.LargestHeader - m_WavesSlotStats.AverageHeader);
                    break;
            }
            
        }

        private void BuildList()
        {
            m_ListView.Items.Clear();

            switch (m_Type)
            {
                case "Bank":
                    if (m_WavesSlotStats.Type == "WAVE")
                    {
                        this.InitHeaders("Wave Name", "Total Size", "Wave Size", "Bank Header Size");

                        foreach (var entry in m_WavesSlotStats.Elements)
                        {
                            var item = new ListViewItem(entry.Path);
                            item.SubItems.Add(entry.FullSize.ToString());
                            item.SubItems.Add(entry.WaveSize.ToString());
                            item.SubItems.Add(entry.HeaderSize.ToString());
                            m_ListView.Items.Add(item);
                        }
                    }
                    else
                    {
                        this.InitHeaders("Bank Name", "Bank Size");

                        foreach (var entry in m_WavesSlotStats.Elements)
                        {
                            var item = new ListViewItem(entry.Path);
                            item.SubItems.Add(entry.FullSize.ToString());
                            m_ListView.Items.Add(item);
                        }
                    }
                    
                    break;
                case "Header":
                    this.InitHeaders("Bank Name", "Header Size");

                    foreach (KeyValuePair<string, int> kvp in m_WavesSlotStats.BankHeaders)
                    {
                        ListViewItem item = new ListViewItem(kvp.Key);
                        item.SubItems.Add(kvp.Value.ToString());
                        m_ListView.Items.Add(item);
                    }
                    break;
            }
        }

        private void InitHeaders(params string[] headers)
        {
            m_ListView.Columns.Clear();
            foreach (var header in headers)
            {
                var column = new ColumnHeader { Text = header, Width = 160 };
                m_ListView.Columns.Add(column);
            }
        }

        private void m_ListView_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            // Determine whether the column is the same as the last column clicked.
            if (e.Column != m_SortColumn)
            {
                // Set the sort column to the new column.
                m_SortColumn = e.Column;
                // Set the sort order to ascending by default.
                m_ListView.Sorting = SortOrder.Ascending;
            }
            else
            {
                // Determine what the last sort order was and change it.
                if (m_ListView.Sorting == SortOrder.Ascending)
                    m_ListView.Sorting = SortOrder.Descending;
                else
                    m_ListView.Sorting = SortOrder.Ascending;
            }

            // Call the sort method to manually sort.
            m_ListView.BeginUpdate();
            m_ListView.Cursor = Cursor = Cursors.WaitCursor;
            m_ListView.Sort();
            m_ListView.EndUpdate();
            m_ListView.Cursor = Cursor = Cursors.Default;
            // Set the ListViewItemSorter property to a new ListViewItemComparer
            // object.
            this.m_ListView.ListViewItemSorter = new ListViewItemComparer(e.Column, m_ListView.Sorting);

        }
    }
}