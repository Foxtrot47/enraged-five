﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WaveSlotSizeInspector
{
    public partial class frmListOfWaveSlotLogs : Form
    {
        public frmListOfWaveSlotLogs(List<string> errors)
        {
            InitializeComponent();
            this.listBox1.Items.AddRange(errors.ToArray());
        }

    }
}
