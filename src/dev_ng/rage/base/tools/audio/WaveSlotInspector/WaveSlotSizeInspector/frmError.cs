namespace WaveSlotSizeReporter
{
    using System;
    using System.Windows.Forms;

    public partial class frmError : Form
    {
        public frmError(string message, string details)
        {
            InitializeComponent();
            this.txtMessage.Text = "Error: "+message;
            this.txtDetails.Text = "Details: " + details;
        }

        private void btn_Ok_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}