namespace WaveSlotSizeReporter
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Drawing;
    using System.Windows.Forms;
    using System.Xml;

    using ProjectLoader2;

    using rage;

    using Rockstar.AssetManager.Interfaces;

    using WaveSlotSizeInspector;

    public partial class frmMain : Form
    {
        private IAssetManager m_AssetManager;
        private audProjectSettings m_ProjectSettings;
        private Dictionary<string, ArrayList> m_AllWaveSlotStats;
        private int m_SortColumn = -1;
        private string m_CurrentPlatform;
        private Dictionary<string, HeaderSize> m_MaxHeader;
        private Dictionary<string, List<HeaderSize>> m_MaxHeaders;
        private Dictionary<string, Dictionary<string, int>> m_StaticSizes;

        private static frmBanks sm_SubDisplay = new frmBanks();
        private static frmScratch sm_Scratch = new frmScratch();

        private List<string> waveSlotLogs = new List<string>();

        public frmMain(string[] args)
        {
            InitializeComponent();
            m_AllWaveSlotStats = new Dictionary<string, ArrayList>();
            m_MaxHeaders = new Dictionary<string, List<HeaderSize>>();
            m_MaxHeader = new Dictionary<string, HeaderSize>();
            m_StaticSizes = new Dictionary<string, Dictionary<string, int>>();
            
        }

        public bool Init()
        {
            try
            {
                var pl =
                    new frmProjectLoader(
                        string.Concat(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                                      @"\Rave\ProjectList.xml"));
                pl.ShowDialog();

                if (pl.IsCancelled)
                {
                    return false;
                }

                m_AssetManager = pl.AssetManager;
                m_ProjectSettings = new audProjectSettings(pl.Project.ProjectSettings);

                foreach (PlatformSetting ps in m_ProjectSettings.GetPlatformSettings())
                {
                    if (!ps.IsActive)
                    {
                        continue;
                    }

                    m_MaxHeaders.Add(ps.PlatformTag, new List<HeaderSize>());
                    m_MaxHeader.Add(ps.PlatformTag, new HeaderSize("", 0));
                    m_StaticSizes.Add(ps.PlatformTag, new Dictionary<string, int>());
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return false;
            }

            if (m_AssetManager != null && m_ProjectSettings != null)
            {
                frmSplashScreen.ShowScreen();

                foreach (PlatformSetting ps in m_ProjectSettings.GetPlatformSettings())
                {
                    if (!ps.IsActive)
                    {
                        continue;
                    }

                    try
                    {
                        WaveSlotBuilder waveSlotBuilder = new WaveSlotBuilder(m_AssetManager, m_ProjectSettings, ps);
                        waveSlotLogs.AddRange(waveSlotBuilder.LoggedErrors);
                        m_AllWaveSlotStats.Add(ps.PlatformTag, waveSlotBuilder.WaveSlotStatistics);
                        m_Platform.Items.Add(ps.PlatformTag);
                        

                    }
                    catch (Exception e)
                    {
                        frmError errorBox = new frmError("Could not load wave slot information for "
                                                            + ps.PlatformTag, e.ToString());
                        errorBox.ShowDialog();
                        return false;
                    }

                }

                if (m_Platform.Items.Count > 0)
                {
                    m_Platform.SelectedIndex = 0;
                    m_CurrentPlatform = m_Platform.Text;
                    LoadStaticSlotInfo();
                    DisplayStats(m_CurrentPlatform);
                }

                frmSplashScreen.CloseScreen();
            }
            else
            {
                string message = "";
                if (m_ProjectSettings == null)
                {
                    message = "Could not load project settings";
                }
                else
                {
                    message = "Could not initialise asset manager";
                }
                frmError errorBox = new frmError(message, null);
                errorBox.ShowDialog();
                return false;
            }

            if (waveSlotLogs.Count > 0)
            {
                new frmListOfWaveSlotLogs(waveSlotLogs).Show();
            }

            return true;
        }


        private bool ContainsHeader(string platform, HeaderSize headerSize)
        {
            foreach (HeaderSize hs in m_MaxHeaders[platform])
            {
                if (hs.Name == headerSize.Name)
                {
                    return true;
                }
            }
            return false;
        }

        private void LoadStaticSlotInfo()
        {
            if (m_ProjectSettings.GetStaticWaveSlotPaths() != null)
            {
                foreach (KeyValuePair<string, string> kvp in m_ProjectSettings.GetStaticWaveSlotPaths())
                {
                    m_AssetManager.GetLatest(kvp.Value, false);
                    var doc = new XmlDocument();
                    doc.Load(m_AssetManager.GetLocalPath(kvp.Value));
                    foreach (XmlNode node in doc.DocumentElement.ChildNodes)
                    {
                        string name = "";
                        int size = Int32.MinValue;

                        foreach (XmlNode setting in node.ChildNodes)
                        {
                            switch (setting.Name)
                            {
                                case "Name":
                                    name = setting.InnerText;
                                    break;
                                case "Size":
                                    try
                                    {
                                        size = Int32.Parse(setting.Attributes["value"].Value);
                                    }
                                    catch (Exception e)
                                    {
                                        EventLog.WriteEntry("WaveSlotInspector", e.Message, EventLogEntryType.Error);
                                    }
                                    break;
                            }
                        }

                        if (name != "" &&
                            size != Int32.MinValue)
                        {
                            m_StaticSizes[kvp.Key].Add(name, size);
                        }
                    }
                }
            }
        }

        public void DisplayStats(string platform)
        {
            m_ListView.BeginUpdate();
            m_ListView.SuspendLayout();
            m_ListView.Cursor = Cursor = Cursors.WaitCursor;

            m_ListView.Columns.Clear();
            m_ListView.Columns.Add("Slot Name", 150);
            m_ListView.Columns.Add("Slot Type", 100);
            m_ListView.Columns.Add("Slot Size", 100);
            m_ListView.Columns.Add("Average Element Size", 120);
            m_ListView.Columns.Add("Largest Element Size", 120);
            m_ListView.Columns.Add("Largest Element Name", 150);
            m_ListView.Columns.Add("Difference", 75);


            m_Summary.Text = platform + ": " + m_AllWaveSlotStats[platform].Count + " Wave Slots,";
            m_ListView.Items.Clear();
            int total = 0;
            List<int> headersizes = new List<int>();
            foreach (WaveSlotStats wss in m_AllWaveSlotStats[platform])
            {
                System.Diagnostics.Debug.WriteLine(wss.Name);
                System.Diagnostics.Debug.WriteLine(wss.LargestHeaderName);
                System.Diagnostics.Debug.WriteLine(wss.LargestHeader.ToString());
                headersizes.Add(wss.LargestHeader);

                ListViewItem item = new ListViewItem(wss.Name);
                item.SubItems.Add(wss.Type);
                item.SubItems.Add(wss.SlotSize.ToString());
                if (m_StaticSizes[platform].ContainsKey(wss.Name))
                {
                    if (m_StaticSizes[platform][wss.Name] < wss.SlotSize)
                    {
                        item.ForeColor = Color.Red;
                    }
                }
                item.SubItems.Add(wss.AverageSize.ToString());
                item.SubItems.Add(wss.MaxSize.ToString());
                item.SubItems.Add(wss.MaxName);
                var difference = wss.MaxSize - wss.AverageSize;
                item.SubItems.Add(difference.ToString());
                m_ListView.Items.Add(item);
                total += wss.SlotSize;

                HeaderSize hs = new HeaderSize(wss.LargestHeaderName, wss.LargestHeader);

                if (wss.LargestHeader > m_MaxHeader[platform].Size)
                {
                    m_MaxHeader[platform] = hs;
                }

                if (m_MaxHeaders[platform].Count == 0)
                {
                    m_MaxHeaders[platform].Add(hs);
                }
                else
                {
                    if (hs.Size >= m_MaxHeaders[platform][0].Size)
                    {
                        if (!ContainsHeader(platform, hs))
                        {
                            m_MaxHeaders[platform].Insert(0, hs);
                        }
                    }
                    else if (hs.Size <= m_MaxHeaders[platform][m_MaxHeaders[platform].Count - 1].Size)
                    {
                        if (!ContainsHeader(platform, hs))
                        {
                            m_MaxHeaders[platform].Insert(m_MaxHeaders[platform].Count, hs);
                        }
                    }
                    else
                    {
                        for (int i = 0; i < m_MaxHeaders[platform].Count; i++)
                        {
                            if (hs.Size < m_MaxHeaders[platform][i].Size && hs.Size >= m_MaxHeaders[platform][i + 1].Size)
                            {
                                if (!ContainsHeader(platform, hs))
                                {
                                    m_MaxHeaders[platform].Insert(i + 1, hs);
                                }
                            }
                        }
                    }
                }

                //only keep biggest 10
                if (m_MaxHeaders[platform].Count == 11)
                {
                    m_MaxHeaders[platform].RemoveAt(10);
                }
            }

            headersizes.Sort();
            m_Summary.Text += " Total Size: " + (total / 1048576) + "MB (" + (total / 1024) + "KB)";

            // Add scratch header
            ListViewItem scratch = new ListViewItem("SCRATCH_HEADER_WAVE_SLOT");
            scratch.SubItems.Add("Wave");
            scratch.SubItems.Add(m_MaxHeader[platform].Size.ToString());
            scratch.SubItems.Add("");
            scratch.SubItems.Add(m_MaxHeader[platform].Size.ToString());
            scratch.SubItems.Add(m_MaxHeader[platform].Name);
            scratch.SubItems.Add("");
            scratch.Tag = "SCRATCH";
            m_ListView.Items.Add(scratch);

            m_ListView.Sort();
            m_ListView.EndUpdate();
            m_ListView.ResumeLayout();
            m_ListView.Cursor = Cursor = Cursors.Default;
        }



        private void m_PlatformChanged(object sender, EventArgs e)
        {
            if (m_Platform.Items.Count > 0)
            {
                m_CurrentPlatform = m_Platform.Text;
                DisplayStats(m_CurrentPlatform);
            }
        }

        private void m_ListView_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            // Determine whether the column is the same as the last column clicked.
            if (e.Column != m_SortColumn)
            {
                // Set the sort column to the new column.
                m_SortColumn = e.Column;
                // Set the sort order to ascending by default.
                m_ListView.Sorting = SortOrder.Ascending;
            }
            else
            {
                // Determine what the last sort order was and change it.
                if (m_ListView.Sorting == SortOrder.Ascending)
                    m_ListView.Sorting = SortOrder.Descending;
                else
                    m_ListView.Sorting = SortOrder.Ascending;
            }

            // Call the sort method to manually sort.
            m_ListView.BeginUpdate();
            m_ListView.Cursor = Cursor = Cursors.WaitCursor;
            m_ListView.Sort();
            m_ListView.EndUpdate();
            m_ListView.Cursor = Cursor = Cursors.Default;
            // Set the ListViewItemSorter property to a new ListViewItemComparer
            // object.
            this.m_ListView.ListViewItemSorter = new ListViewItemComparer(e.Column, m_ListView.Sorting);

        }

        private void ShowHeaders(object sender, EventArgs e)
        {
            if (m_ListView.SelectedItems[0].Tag != null && m_ListView.SelectedItems[0].Tag.ToString() == "SCRATCH")
            {
                sm_Scratch.Init(m_MaxHeaders[m_CurrentPlatform]);
                sm_Scratch.Show();
            }
            else
            {
                foreach (WaveSlotStats wss in m_AllWaveSlotStats[m_CurrentPlatform])
                {
                    if (wss.Name == m_ListView.SelectedItems[0].Text)
                    {
                        sm_SubDisplay.Init("Header", wss);
                        sm_SubDisplay.Show();
                        break;
                    }
                }
            }
        }

        private void ShowBanks(object sender, EventArgs e)
        {
            foreach (WaveSlotStats wss in m_AllWaveSlotStats[m_CurrentPlatform])
            {
                if (wss.Name == m_ListView.SelectedItems[0].Text)
                {
                    sm_SubDisplay.Init("Bank", wss);
                    sm_SubDisplay.Show();
                    break;
                }
            }
        }

        private void m_ContextMenu_Opening(object sender, CancelEventArgs e)
        {
            if (m_ListView.SelectedItems.Count > 0)
            {
                if (m_ListView.SelectedItems[0].Tag != null && m_ListView.SelectedItems[0].Tag.ToString() == "SCRATCH")
                {
                    mnuShowBanks.Visible = false;
                }
                else
                {
                    mnuShowBanks.Visible = true;
                }
            }
            else
            {
                e.Cancel = true;
            }

        }

    }

    public class HeaderSize
    {
        private string m_Name;
        private int m_Size;

        public string Name
        {
            get { return m_Name; }
        }
        public int Size
        {
            get { return m_Size; }
        }

        public HeaderSize(string name, int size)
        {
            m_Name = name;
            m_Size = size;
        }
    }

}