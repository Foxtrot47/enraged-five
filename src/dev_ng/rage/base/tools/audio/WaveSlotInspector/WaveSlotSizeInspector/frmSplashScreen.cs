﻿namespace WaveSlotSizeInspector
{
    using System.Threading;
    using System.Windows.Forms;

    /// <summary>
    /// The splash screen form.
    /// </summary>
    public partial class frmSplashScreen : Form
    {
        private static frmSplashScreen instance;
        private static Thread splashScreenThread;

        /// <summary>
        /// Initializes a new instance of the <see cref="frmSplashScreen"/> class.
        /// </summary>
        public frmSplashScreen()
        {
            this.InitializeComponent();
        }

        public static void ShowScreen()
        {
            if (instance != null)
            {
                return;
            }

            splashScreenThread = new Thread(ShowForm);
            CheckForIllegalCrossThreadCalls = false;
            splashScreenThread.IsBackground = true;
            splashScreenThread.SetApartmentState(ApartmentState.STA);
            splashScreenThread.Start();
        }

        public static void CloseScreen()
        {
            if (null != instance)
            {
                instance.Close();
                instance = null;
            }
            
            splashScreenThread = null;
        }

        private static void ShowForm()
        {
            instance = new frmSplashScreen();
            Application.Run(instance);
        }
    }
}
