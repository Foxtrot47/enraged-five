namespace WaveSlotSizeReporter
{
    using System;
    using System.Collections;
    using System.IO;
    using System.Xml.Linq;

    using rage;
    using rage.ToolLib.WavesFiles;

    using Rockstar.AssetManager.Interfaces;
    using rage.ToolLib.Logging;
    using System.Collections.Generic;

    class WaveSlotBuilder
    {
        private List<string> loggedErrors = new List<string>();
        public List<string> LoggedErrors { get { return loggedErrors; } }

        public WaveSlotBuilder(IAssetManager assetMgr, audProjectSettings projectSettings, PlatformSetting platform)
        {
            WaveSlotStatistics = new ArrayList();

            //wave
            try
            {
                assetMgr.GetLatest(projectSettings.GetWaveSlotSettings(), false);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Failed to get latest wave slot settings Xml file.  Check project settings.", ex);
            }

            string builtWavesAssetsPath = Path.Combine(platform.BuildInfo, "BuiltWaves\\");
            string builtWavesAssetsWorkingPath = assetMgr.GetLocalPath(builtWavesAssetsPath);
            assetMgr.GetLatest(builtWavesAssetsWorkingPath, false);

            var builtWavesPackListFile = Path.Combine(builtWavesAssetsWorkingPath, "BuiltWaves_PACK_LIST.xml");

            var combiner = new WavesFileCombiner();
            var builtWavesXml = combiner.Run(builtWavesPackListFile, null);

            combiner.Run(builtWavesPackListFile, "C:\\temp\\built_waves_combined_" + platform + ".xml", null); // do not commit

            var waveSlotSettingsFile = assetMgr.GetLocalPath(projectSettings.GetWaveSlotSettings());
            assetMgr.GetLatest(waveSlotSettingsFile, false);

            TextLog logger = new TextLog(new ListLogWriter(out loggedErrors), new ContextStack());
            var waveSlotSettings = new WaveSlotSettings(logger, XDocument.Load(waveSlotSettingsFile), builtWavesXml, projectSettings);
            foreach (var slot in waveSlotSettings.Slots)
            {
                if (slot.Name != "TEST")
                {
                    WaveSlotStatistics.Add(new WaveSlotStats(slot.Name, slot.Size, slot.LoadType.ToString(), slot.LargestElement, slot.AverageSize, slot.Elements, slot.BankHeaders));
                }
            }
        }

        public ArrayList WaveSlotStatistics { get; private set; }
    }
}
