namespace WaveSlotSizeInspector
{
    using System.Windows.Forms;

    partial class frmBanks
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.m_ListView = new System.Windows.Forms.ListView();
            this.lblName = new System.Windows.Forms.Label();
            this.lblLargestName = new System.Windows.Forms.Label();
            this.lblDiff = new System.Windows.Forms.Label();
            this.lblLargestSize = new System.Windows.Forms.Label();
            this.lblAverage = new System.Windows.Forms.Label();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.m_ListView);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.lblName);
            this.splitContainer1.Panel2.Controls.Add(this.lblLargestName);
            this.splitContainer1.Panel2.Controls.Add(this.lblDiff);
            this.splitContainer1.Panel2.Controls.Add(this.lblLargestSize);
            this.splitContainer1.Panel2.Controls.Add(this.lblAverage);
            this.splitContainer1.Size = new System.Drawing.Size(402, 456);
            this.splitContainer1.SplitterDistance = 329;
            this.splitContainer1.TabIndex = 0;
            // 
            // m_ListView
            // 
            this.m_ListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_ListView.Location = new System.Drawing.Point(0, 0);
            this.m_ListView.Name = "m_ListView";
            this.m_ListView.Size = new System.Drawing.Size(402, 329);
            this.m_ListView.TabIndex = 0;
            this.m_ListView.UseCompatibleStateImageBehavior = false;
            this.m_ListView.View = System.Windows.Forms.View.Details;
            this.m_ListView.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.m_ListView_ColumnClick);
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.Location = new System.Drawing.Point(12, 9);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(102, 13);
            this.lblName.TabIndex = 0;
            this.lblName.Text = "Wave Slot Name";
            // 
            // lblLargestName
            // 
            this.lblLargestName.AutoSize = true;
            this.lblLargestName.Location = new System.Drawing.Point(12, 78);
            this.lblLargestName.Name = "lblLargestName";
            this.lblLargestName.Size = new System.Drawing.Size(120, 13);
            this.lblLargestName.TabIndex = 4;
            this.lblLargestName.Text = "Largest Element Name: ";
            // 
            // lblDiff
            // 
            this.lblDiff.AutoSize = true;
            this.lblDiff.Location = new System.Drawing.Point(12, 104);
            this.lblDiff.Name = "lblDiff";
            this.lblDiff.Size = new System.Drawing.Size(62, 13);
            this.lblDiff.TabIndex = 1;
            this.lblDiff.Text = "Difference: ";
            // 
            // lblLargestSize
            // 
            this.lblLargestSize.AutoSize = true;
            this.lblLargestSize.Location = new System.Drawing.Point(12, 55);
            this.lblLargestSize.Name = "lblLargestSize";
            this.lblLargestSize.Size = new System.Drawing.Size(112, 13);
            this.lblLargestSize.TabIndex = 3;
            this.lblLargestSize.Text = "Largest Element Size: ";
            // 
            // lblAverage
            // 
            this.lblAverage.AutoSize = true;
            this.lblAverage.Location = new System.Drawing.Point(12, 31);
            this.lblAverage.Name = "lblAverage";
            this.lblAverage.Size = new System.Drawing.Size(117, 13);
            this.lblAverage.TabIndex = 2;
            this.lblAverage.Text = "Average Element Size: ";
            // 
            // frmBanks
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(402, 456);
            this.Controls.Add(this.splitContainer1);
            this.Name = "frmBanks";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label lblLargestSize;
        private System.Windows.Forms.Label lblAverage;
        private System.Windows.Forms.Label lblDiff;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.ListView m_ListView;
        private System.Windows.Forms.Label lblLargestName;
    }
}