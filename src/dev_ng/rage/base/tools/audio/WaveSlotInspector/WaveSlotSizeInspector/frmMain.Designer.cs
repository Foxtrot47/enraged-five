using System.Collections;
using System.Windows.Forms;
using System;
namespace WaveSlotSizeReporter
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.m_Platform = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.m_Summary = new System.Windows.Forms.ToolStripLabel();
            this.m_ListView = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader4 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader5 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader6 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader7 = new System.Windows.Forms.ColumnHeader();
            this.m_ContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuShowBanks = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuShowHeaders = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1.SuspendLayout();
            this.toolStripContainer1.BottomToolStripPanel.SuspendLayout();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            this.m_ContextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_Platform});
            this.toolStrip1.Location = new System.Drawing.Point(3, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip1.Size = new System.Drawing.Size(135, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // m_Platform
            // 
            this.m_Platform.Name = "m_Platform";
            this.m_Platform.Size = new System.Drawing.Size(121, 25);
            this.m_Platform.SelectedIndexChanged += new System.EventHandler(this.m_PlatformChanged);
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.BottomToolStripPanel
            // 
            this.toolStripContainer1.BottomToolStripPanel.Controls.Add(this.toolStrip2);
            this.toolStripContainer1.BottomToolStripPanel.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this.m_ListView);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(856, 501);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.LeftToolStripPanelVisible = false;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.RightToolStripPanelVisible = false;
            this.toolStripContainer1.Size = new System.Drawing.Size(856, 551);
            this.toolStripContainer1.TabIndex = 3;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolStrip1);
            this.toolStripContainer1.TopToolStripPanel.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            // 
            // toolStrip2
            // 
            this.toolStrip2.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_Summary});
            this.toolStrip2.Location = new System.Drawing.Point(3, 0);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip2.Size = new System.Drawing.Size(3, 25);
            this.toolStrip2.TabIndex = 0;
            // 
            // m_Summary
            // 
            this.m_Summary.Name = "m_Summary";
            this.m_Summary.Size = new System.Drawing.Size(0, 22);
            // 
            // m_ListView
            // 
            this.m_ListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7});
            this.m_ListView.ContextMenuStrip = this.m_ContextMenu;
            this.m_ListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_ListView.Location = new System.Drawing.Point(0, 0);
            this.m_ListView.Name = "m_ListView";
            this.m_ListView.Size = new System.Drawing.Size(856, 501);
            this.m_ListView.TabIndex = 2;
            this.m_ListView.UseCompatibleStateImageBehavior = false;
            this.m_ListView.View = System.Windows.Forms.View.Details;
            this.m_ListView.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.m_ListView_ColumnClick);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Slot Name";
            this.columnHeader1.Width = 150;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Slot Type";
            this.columnHeader2.Width = 100;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Slot Size";
            this.columnHeader3.Width = 100;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Average Element Size";
            this.columnHeader4.Width = 120;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Largest Element Size";
            this.columnHeader5.Width = 120;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Largest Element Name";
            this.columnHeader6.Width = 150;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Difference";
            this.columnHeader7.Width = 75;
            // 
            // m_ContextMenu
            // 
            this.m_ContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuShowBanks,
            this.mnuShowHeaders});
            this.m_ContextMenu.Name = "m_ContextMenu";
            this.m_ContextMenu.ShowImageMargin = false;
            this.m_ContextMenu.Size = new System.Drawing.Size(130, 48);
            this.m_ContextMenu.Opening += new System.ComponentModel.CancelEventHandler(this.m_ContextMenu_Opening);
            // 
            // mnuShowBanks
            // 
            this.mnuShowBanks.Name = "mnuShowBanks";
            this.mnuShowBanks.Size = new System.Drawing.Size(129, 22);
            this.mnuShowBanks.Text = "Show Banks";
            this.mnuShowBanks.Click += new System.EventHandler(this.ShowBanks);
            // 
            // mnuShowHeaders
            // 
            this.mnuShowHeaders.Name = "mnuShowHeaders";
            this.mnuShowHeaders.Size = new System.Drawing.Size(129, 22);
            this.mnuShowHeaders.Text = "Show Headers";
            this.mnuShowHeaders.Click += new System.EventHandler(this.ShowHeaders);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(856, 551);
            this.Controls.Add(this.toolStripContainer1);
            this.Name = "frmMain";
            this.Text = "Wave Slot Inspector";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.toolStripContainer1.BottomToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.BottomToolStripPanel.PerformLayout();
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.m_ContextMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        
        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripComboBox m_Platform;
        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private ListView m_ListView;
        private ColumnHeader columnHeader1;
        private ColumnHeader columnHeader2;
        private ColumnHeader columnHeader3;
        private ColumnHeader columnHeader4;
        private ColumnHeader columnHeader5;
        private ColumnHeader columnHeader6;
        private ColumnHeader columnHeader7;
        private ToolStrip toolStrip2;
        private ToolStripLabel m_Summary;
        private ContextMenuStrip m_ContextMenu;
        private ToolStripMenuItem mnuShowBanks;
        private ToolStripMenuItem mnuShowHeaders;
    }
    
}

