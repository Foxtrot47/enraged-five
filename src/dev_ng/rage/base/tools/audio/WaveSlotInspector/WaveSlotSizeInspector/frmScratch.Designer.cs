namespace WaveSlotSizeInspector
{
    partial class frmScratch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_ListView = new System.Windows.Forms.ListView();
            this.colName = new System.Windows.Forms.ColumnHeader();
            this.colSize = new System.Windows.Forms.ColumnHeader();
            this.lblLargestName = new System.Windows.Forms.Label();
            this.lblDiff = new System.Windows.Forms.Label();
            this.lblLargestSize = new System.Windows.Forms.Label();
            this.lblAverage = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // m_ListView
            // 
            this.m_ListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_ListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colName,
            this.colSize});
            this.m_ListView.Location = new System.Drawing.Point(-3, 0);
            this.m_ListView.Name = "m_ListView";
            this.m_ListView.Size = new System.Drawing.Size(362, 273);
            this.m_ListView.TabIndex = 0;
            this.m_ListView.UseCompatibleStateImageBehavior = false;
            this.m_ListView.View = System.Windows.Forms.View.Details;
            // 
            // colName
            // 
            this.colName.Text = "Name";
            // 
            // colSize
            // 
            this.colSize.Text = "Size";
            // 
            // lblLargestName
            // 
            this.lblLargestName.AutoSize = true;
            this.lblLargestName.Location = new System.Drawing.Point(12, 330);
            this.lblLargestName.Name = "lblLargestName";
            this.lblLargestName.Size = new System.Drawing.Size(117, 13);
            this.lblLargestName.TabIndex = 9;
            this.lblLargestName.Text = "Largest Header Name: ";
            // 
            // lblDiff
            // 
            this.lblDiff.AutoSize = true;
            this.lblDiff.Location = new System.Drawing.Point(12, 356);
            this.lblDiff.Name = "lblDiff";
            this.lblDiff.Size = new System.Drawing.Size(62, 13);
            this.lblDiff.TabIndex = 6;
            this.lblDiff.Text = "Difference: ";
            // 
            // lblLargestSize
            // 
            this.lblLargestSize.AutoSize = true;
            this.lblLargestSize.Location = new System.Drawing.Point(12, 307);
            this.lblLargestSize.Name = "lblLargestSize";
            this.lblLargestSize.Size = new System.Drawing.Size(109, 13);
            this.lblLargestSize.TabIndex = 8;
            this.lblLargestSize.Text = "Largest Header Size: ";
            // 
            // lblAverage
            // 
            this.lblAverage.AutoSize = true;
            this.lblAverage.Location = new System.Drawing.Point(12, 283);
            this.lblAverage.Name = "lblAverage";
            this.lblAverage.Size = new System.Drawing.Size(114, 13);
            this.lblAverage.TabIndex = 7;
            this.lblAverage.Text = "Average Header Size: ";
            // 
            // frmScratch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(357, 395);
            this.Controls.Add(this.lblLargestName);
            this.Controls.Add(this.lblDiff);
            this.Controls.Add(this.lblLargestSize);
            this.Controls.Add(this.lblAverage);
            this.Controls.Add(this.m_ListView);
            this.Name = "frmScratch";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView m_ListView;
        private System.Windows.Forms.ColumnHeader colName;
        private System.Windows.Forms.ColumnHeader colSize;
        private System.Windows.Forms.Label lblLargestName;
        private System.Windows.Forms.Label lblDiff;
        private System.Windows.Forms.Label lblLargestSize;
        private System.Windows.Forms.Label lblAverage;
    }
}