namespace WaveSlotSizeReporter
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using rage;

    public class WaveSlotStats
    {
        private int m_largestHeader = Int32.MinValue;

        public WaveSlotStats(string name,
                             int slotSize,
                             string type,
                             KeyValuePair<string, int> largestElement,
                             long averageSize,
                             List<WaveSlotEntry> elements,
                             List<KeyValuePair<string, int>> bankHeaders)
        {
            Name = name;
            SlotSize = slotSize;
            Type = type;
            MaxSize = largestElement.Value;
            MaxName = largestElement.Key;
            AverageSize = averageSize;
            Elements = elements;
            BankHeaders = bankHeaders;
        }

        public string Name { get; private set; }

        public int SlotSize { get; private set; }

        public string Type { get; private set; }

        public int MaxSize { get; private set; }

        public string MaxName { get; private set; }

        public long AverageSize { get; private set; }

        public List<WaveSlotEntry> Elements { get; set; }

        public List<KeyValuePair<string, int>> BankHeaders { get; private set; }

        public int LargestHeader
        {
            get
            {
                if (m_largestHeader >= Int32.MinValue)
                {
                    foreach (var kvp in BankHeaders)
                    {
                        if (kvp.Value > m_largestHeader)
                        {
                            m_largestHeader = kvp.Value;
                            LargestHeaderName = kvp.Key;
                        }
                    }
                }
                return m_largestHeader;
            }
        }

        public string LargestHeaderName { get; private set; }

        public int AverageHeader
        {
            get
            {
                var total = BankHeaders.Sum(kvp => kvp.Value);
                return total / BankHeaders.Count;
            }
        }
    }
}