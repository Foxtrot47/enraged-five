﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using NLog;

namespace sequentialOverlapSoundUpdater {
    class Program {

        private static Logger logger = LogManager.GetCurrentClassLogger();
        
        static void Main(string[] args) {
            if (args.Length != 1)
            {
                Console.WriteLine("Please specify directory path");
                return;
            }

            string path = args[0];
            if (!Directory.Exists(path))
            {
                Console.WriteLine("Specified directory does not exist");
                return;
            }

            logger.Info("Start updating SequentialOverlapSounds in "+path);
            processDirectory(path);
            
            Console.WriteLine("Press enter to exit");
            Console.ReadLine();
        }

        private static void processDirectory(string path)
        {
            foreach(string subDir in Directory.GetDirectories(path)) processDirectory(subDir);

            foreach (string file in Directory.GetFiles(path))
            {
                if(!file.ToUpper().EndsWith(".XML")) continue;

                bool fileHasBeenChanged = false;
                XDocument doc = XDocument.Load(file);
                if(!doc.Root.Name.LocalName.Equals("Objects")) continue;
                foreach (XElement sequentialOverlapSound in doc.Root.Descendants("SequentialOverlapSound"))
                {
                    string seqOverlapSoundName = "Unnamed SequentialOverlapSound";
                    if (sequentialOverlapSound.Attribute("name") == null) logger.Warn(file+": SequentialOverlapSound without name found.");
                    else seqOverlapSoundName = sequentialOverlapSound.Attribute("name").Value;

                    XElement existingOverlapModeElement = sequentialOverlapSound.Element("OverlapMode");
                    XElement triggerOnChildReleasElement = sequentialOverlapSound.Element("TriggerOnChildRelease");
                    
                    XElement delayTimeIsRemainingElement = sequentialOverlapSound.Element("DelayTimeIsRemaining");
                    XElement delayTimeIsPercentageElement = sequentialOverlapSound.Element("DelayTimeIsPercentage");

                    XElement overlapModeElement = new XElement("OverlapMode");

                    if (triggerOnChildReleasElement != null && valueTrue(triggerOnChildReleasElement))
                    {
                        overlapModeElement.Value = "AUD_SEQOL_TRIGGERONCHILDRELEASE";
                    }
                    else
                    {

                        if (delayTimeIsPercentageElement != null && valueTrue(delayTimeIsPercentageElement))
                        {
                            if (delayTimeIsRemainingElement != null && valueTrue(delayTimeIsRemainingElement))
                            {
                                overlapModeElement.Value = "AUD_SEQOL_AUD_SEQOL_REMAININTDELAYTIMEPERCENTAGE";
                            }
                            else
                            {
                                overlapModeElement.Value = "AUD_SEQOL_DELAYTIMEPERCENTAGE";
                            }
                        }
                        else
                        {
                            if (delayTimeIsRemainingElement != null && valueTrue(delayTimeIsRemainingElement))
                            {
                                overlapModeElement.Value = "AUD_SEQOL_REMAININTDELAYTIME";
                            }
                            else
                            {
                                overlapModeElement.Value = "AUD_SEQOL_DELAYTIME";
                            }
                        }
                    }

                    if (existingOverlapModeElement != null &&
                        (triggerOnChildReleasElement != null || delayTimeIsRemainingElement != null ||
                         delayTimeIsPercentageElement != null))
                    {
                        logger.Warn(file+": OverlapMode is set on "+seqOverlapSoundName+" but old elements are still present!");
                        continue;
                    }
                    
                    if (existingOverlapModeElement == null &&
                        (triggerOnChildReleasElement == null || delayTimeIsRemainingElement == null ||
                         delayTimeIsPercentageElement == null))
                    {
                        logger.Warn(file+": missing old element on "+seqOverlapSoundName+" assuming element is set to \"no\"");
                    }

                    //file sound has already been updated
                    if (existingOverlapModeElement != null) continue;

                    sequentialOverlapSound.AddFirst(overlapModeElement);
                    if(triggerOnChildReleasElement!=null) triggerOnChildReleasElement.Remove();
                    if(delayTimeIsRemainingElement != null) delayTimeIsRemainingElement.Remove();
                    if(delayTimeIsPercentageElement != null) delayTimeIsPercentageElement.Remove();
                    fileHasBeenChanged = true;
                }
                if (fileHasBeenChanged)
                {
                    //make file writable
                    new FileInfo(file) {IsReadOnly = false};
                    doc.Save(file);
                }
            }

        }

        private static bool valueTrue(XElement element)
        {
            string value = element.Value.ToLower();
            return value.Equals("yes") || value.Equals("true");
        }
    }
}
