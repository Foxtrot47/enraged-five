﻿using System;
using System.Collections.Generic;
using rage.ToolLib.CmdLine;
using System.Collections.Specialized;

namespace GrabP4
{
    class Program
    {
        private const string EXPORTPATH = "exportpath";
        private const string CHANGELIST = "changelist";
        private const string USERNAME = "username";
        private const string PASSWORD = "password";
        private const string WORKSPACE = "workspace";
        private const string SERVER = "server";
        private const string EXTENSION = "extension";
        private const string BASE_PATH = "basepath";

        static void Main(string[] args)
        {
            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.UnhandledException += new UnhandledExceptionEventHandler(GlobalExeptionHandler);

            CmdLineParser cmdLineParser = new CmdLineParser(args);

            GrabAssetsHandler p4Handler = null;

            StringDictionary arguments = cmdLineParser.Arguments;

            if (!arguments.ContainsKey(EXPORTPATH) || !arguments.ContainsKey(CHANGELIST))
            {
                throw new Exception("Cannot run GrabP4 without exportpath and changelist arguments.");
            }

            if (arguments.ContainsKey(USERNAME) && arguments.ContainsKey(PASSWORD) && arguments.ContainsKey(WORKSPACE) && arguments.ContainsKey(SERVER))
            {
                p4Handler = new GrabAssetsHandler(arguments[USERNAME], arguments[PASSWORD], arguments[WORKSPACE], arguments[SERVER]);
            }
            else
            {
                p4Handler = new GrabAssetsHandler("");
            }

            //Check that the main arguments are valid
            if (!IsValidNumber(arguments[CHANGELIST]))
            {
                throw new Exception("Invalid use: The first argument should be the CL number");
            }

            if (!IsValidDirectory(arguments[EXPORTPATH]))
            {
                throw new Exception("Invalid use: The second argument should be a directory");
            }

            //Check for extension
            string extension = "";
            if (arguments.ContainsKey(EXTENSION))
            {
                extension = arguments[EXTENSION];
            }

            string basePath = "";
            if (arguments.ContainsKey(BASE_PATH))
            {
                basePath = arguments[BASE_PATH];
            }

            //Run P4 copy Changleist to folder
            p4Handler.CopyChangeListToFolder(arguments[CHANGELIST], arguments[EXPORTPATH], extension, arguments[BASE_PATH]);
        }


        //Checks if a string is a valid number
        private static bool IsValidNumber(string argument)
        {
            uint number;
            return uint.TryParse(argument, out number);
        }

        //Check if a string is a valid directory
        private static bool IsValidDirectory(string argument)
        {
            bool uriIsWellFormed = IsValidUri(argument);
            bool isADirectory = argument.ToCharArray()[argument.Length - 1] == '\\';
            return (uriIsWellFormed && isADirectory);
        }

        //Check if a string is a valid uri
        private static bool IsValidUri(string uri)
        {
            try
            {
                new Uri(uri);
                return true;
            }
            catch
            {
                return false;
            }
        }


        private static void GlobalExeptionHandler(object sender, UnhandledExceptionEventArgs e)
        {
            Console.WriteLine(e.ExceptionObject.ToString());
            Environment.Exit(1);
        }
    }
}
