﻿using rage;
using Rockstar.AssetManager.Infrastructure.Enums;
using Rockstar.AssetManager.Interfaces;
using Rockstar.AssetManager.Main;
using Rockstar.AssetManager.Perforce;
using System;
using System.Collections.Generic;
using System.IO;

namespace GrabP4
{
    class GrabAssetsHandler
    {
        private IAssetManager assetManager;

        /// <summary>
        /// Constructor for p4Handler
        /// </summary>
        /// <param name="password">
        /// user password
        /// </param>
        public GrabAssetsHandler(string password)
        {
            audProjectList projectList = new audProjectList(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) +
@"\Rave\ProjectList.xml");
            if (projectList == null)
            {
                throw new Exception("It seems like rave is not installed on your system, please log on with credentials");
            }

            //Create asset manager from project list
            ConnectAssetManagerWithProjectList(projectList, password);
        }

        public GrabAssetsHandler(string user, string password, string workspace, string server)
        {
            ConnectWithCredentials(user, password, workspace, server);
        }

        public void ConnectWithCredentials(string user, string password, string workspace, string server)
        {
            //Create asset manager from project list
            assetManager = AssetManagerFactory.GetInstance(
    AssetManagerType.Perforce,
    null,
   server,
    workspace,
    user,
    password,
    "//",
    null,
    "GrabP4");
            //Check if connection worked
            bool connected = assetManager.Connect();
            if (!connected)
            {
                throw new Exception("Connection with username" + user + " to " + server + "failed");
            }
        }

        /// <summary>
        /// Copies the contents of a changelist to a folder
        /// </summary>
        /// <param name="changelistNumber">
        /// The changelist number
        /// </param>
        /// <param name="folder">
        /// The folder to which the files will be copied
        /// </param>
        public void CopyChangeListToFolder(string changelistNumber, string folder, string extension, string basePath)
        {
            //Get list of files from changelist
            IList<string> listOfFiles = assetManager.GetChangeListFiles(changelistNumber, true);


            //Copy files from perforce to a specific folder
            foreach (string file in listOfFiles)
            {
                string fileName = Path.GetFileName(file);

                if (!string.IsNullOrEmpty(basePath))
                {
                    if (!file.Contains(basePath))
                    {
                        continue;
                    }

                    fileName = file.Substring(basePath.Length);
                    if (folder.Contains("\\"))
                    {
                        fileName = fileName.Replace("/", "\\");
                    }
                }

                if (string.IsNullOrEmpty(extension) || Path.GetExtension(file) == extension)
                {
                    var args = new List<string> { "-o", folder + fileName, file + "@=" + changelistNumber };
                    var request = new P4RunCmdRequest("print", args);
                    assetManager.RequestHandler.ActionRequest(request);
                }
            }

            //Write changelist content 
            IChangeList changelist = assetManager.GetChangeList(changelistNumber, true);

            try
            {
                File.WriteAllText(folder + changelistNumber + "_contents.txt", "Changelist " + changelistNumber + "\n Created on: " +
changelist.Created + "\n User:" + changelist.Username + "\n Description: " + changelist.Description);

            }
            catch
            {
                Console.WriteLine("Could not write to file");
            }

            //Final message
            Console.WriteLine("Files in CL" + changelistNumber + " have been written to location: " + folder);
        }

        /// <summary>
        /// Creates asset manager from project list
        /// </summary>
        /// <param name="projectList">
        /// The project list (usually available in localAppData folder
        /// </param>
        /// <returns>
        /// The asset manager instance
        /// </returns>
        private void ConnectAssetManagerWithProjectList(audProjectList projectList, string userPassword)
        {
            ConnectWithCredentials(projectList.Projects[0].UserName, userPassword, projectList.Projects[0].AssetProject, projectList.Projects[0].AssetServer);
        }

    }
}
