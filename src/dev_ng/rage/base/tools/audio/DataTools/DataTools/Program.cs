﻿namespace DataTools
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var updater = new ClothingXmlUpdater();
            updater.Run();
        }
    }
}
