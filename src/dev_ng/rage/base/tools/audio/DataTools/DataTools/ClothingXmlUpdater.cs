﻿// -----------------------------------------------------------------------
// <copyright file="ClothingXmlUpdater.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace DataTools
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml.Linq;

    /// <summary>
    /// Clothing XML Updater.
    /// </summary>
    public class ClothingXmlUpdater
    {
        /// <summary>
        /// The character directory path.
        /// </summary>
        private const string CharacterDirPath = @"X:\gta5\assets\metadata\characters\";

        /// <summary>
        /// The upper clothing path.
        /// </summary>
        private const string UpperClothingPath = @"x:\gta5\audio\dev\assets\Objects\Core\Audio\GameObjects\CLOTH\UPPER.XML";

        /// <summary>
        /// The lower clothing path.
        /// </summary>
        private const string LowerClothingPath = @"x:\gta5\audio\dev\assets\Objects\Core\Audio\GameObjects\CLOTH\LOWER.XML";

        /// <summary>
        /// The extras clothing path.
        /// </summary>
        private const string ExtrasClothingPath = @"x:\gta5\audio\dev\ASSETS\OBJECTS\CORE\AUDIO\GAMEOBJECTS\CLOTH\EXTRAS.XML";

        /// <summary>
        /// The new shoes directory path.
        /// </summary>
        private const string NewShoesDirPath = @"x:\gta5\audio\dev\assets\Objects\Core\Audio\GAMEOBJECTS\FOOTSTEPS\NEW_SHOES\";

        /// <summary>
        /// The name mappings.
        /// </summary>
        private IDictionary<string, string> nameMappings;

        /// <summary>
        /// Run the updater.
        /// </summary>
        public void Run()
        {
            Console.WriteLine("Make sure you have grabbed latest data and checked out required files. Press Enter to start...");
            Console.ReadKey();

            this.LoadData();

            foreach (var file in Directory.GetFiles(CharacterDirPath, "*.meta", SearchOption.AllDirectories))
            {
                var fileInfo = new FileInfo(file);
                if (fileInfo.IsReadOnly)
                {
                    Console.WriteLine("File is not writable: " + file);
                    continue;
                }

                var doc = XDocument.Load(file);
                var changesMade = doc.Descendants("pedXml_audioID").Aggregate(
                    false, (current, audioIdElem) => this.UpdateElem(audioIdElem) || current);

                changesMade = doc.Descendants("pedXml_audioID2").Aggregate(
                    changesMade, (current, audioIdElem) => this.UpdateElem(audioIdElem) || current);

                if (changesMade)
                {
                    doc.Save(file);
                }
            }

            Console.WriteLine("Complete");
            Console.ReadKey();
        }

        /// <summary>
        /// Load the data.
        /// </summary>
        private void LoadData()
        {
            this.nameMappings = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);

            var upperClothingDoc = XDocument.Load(UpperClothingPath);
            foreach (var elem in upperClothingDoc.Descendants("ClothAudioSettings"))
            {
                var fullName = elem.Attribute("name").Value;
                var name = fullName.Substring(fullName.IndexOf("_") + 1);
                var prefix = fullName.Substring(0, fullName.IndexOf("_"));

                this.nameMappings.Add(name, prefix);
            }

            var lowerClothingDoc = XDocument.Load(LowerClothingPath);
            foreach (var elem in lowerClothingDoc.Descendants("ClothAudioSettings"))
            {
                var fullName = elem.Attribute("name").Value;
                var name = fullName.Substring(fullName.IndexOf("_") + 1);
                var prefix = fullName.Substring(0, fullName.IndexOf("_"));

                this.nameMappings.Add(name, prefix);
            }

            var extraClothingDoc = XDocument.Load(ExtrasClothingPath);
            foreach (var elem in extraClothingDoc.Descendants("ClothAudioSettings"))
            {
                var fullName = elem.Attribute("name").Value;
                var name = fullName.Substring(fullName.IndexOf("_") + 1);
                var prefix = fullName.Substring(0, fullName.IndexOf("_"));

                this.nameMappings.Add(name, prefix);
            }

            foreach (var file in Directory.GetFiles(NewShoesDirPath, "*.xml", SearchOption.AllDirectories))
            {
                var doc = XDocument.Load(file);
                foreach (var elem in doc.Descendants("ShoeAudioSettings"))
                {
                    var fullName = elem.Attribute("name").Value;
                    var name = fullName.Substring(fullName.IndexOf("_") + 1);
                    var prefix = fullName.Substring(0, fullName.IndexOf("_"));

                    this.nameMappings.Add(name, prefix);
                }
            }
        }

        /// <summary>
        /// The update elem.
        /// </summary>
        /// <param name="elem">
        /// The elem.
        /// </param>
        /// <returns>
        /// Indication of changes made <see cref="bool"/>.
        /// </returns>
        private bool UpdateElem(XElement elem)
        {
            var objName = elem.Value;

            if (objName.ToUpper() == "NONE")
            {
                return false;
            }

            if (!this.nameMappings.ContainsKey(objName))
            {
                Console.WriteLine("Name not found: " + objName);
                return false;
            }

            var prefix = this.nameMappings[objName];
            elem.Value = string.Format("{0}_{1}", prefix, objName);

            return true;
        }
    }
}
