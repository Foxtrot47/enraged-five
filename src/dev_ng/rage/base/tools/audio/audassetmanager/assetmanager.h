//
// tools/audassetmanager/assetmanager.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//
#ifndef ASSET_MANAGER_H
#define ASSET_MANAGER_H

#include "asset.h"
#include "assetmanagertypes.h"

using namespace System;
using namespace System::IO;

namespace audAssetManagement
{
	//
	// PURPOSE
	//  Defines the basic interface inherited by all system-specific Asset Manager implementations and provides
	//	a method for instantiating these managers.
	// NOTES
	//  The Asset Manager interface is documented purely at this level to avoid duplication in the inherited
	//	implementations.
	// SEE ALSO
	//  audAssetManagerTypes
	//
	public __gc class audAssetManager abstract
	{
public:
		//
		// PURPOSE
		//  Instantiates a specific Asset Manager implementation, as specified by the 'type' parameter, and
		//	returns the instance as a pointer to this base class.
		// PARAMS
		//  The type of Asset Manager to instantiate (see assetmanagertypes.h.)
		// RETURNS
		//  A base class pointer to the instantiated manager or NULL if an invalid type was specified.
		// SEE ALSO
		//  audAssetManagerTypes
		//
		static audAssetManager *CreateInstance(audAssetManagerTypes type);
		//
		// PURPOSE
		//  Disables the read-only attribute of the asset(s) located at the specified local path.
		// PARAMS
		//	localPath	- The local path of the asset(s) to be made writeable.
		//
		virtual void MakeLocalAssetWriteable(String *localPath);
		//
		// PURPOSE
		//  Enforces the read-only attribute of the asset(s) located at the specified local path.
		// PARAMS
		//	localPath	- The local path of the asset(s) to be made read-only.
		//
		virtual void MakeLocalAssetReadOnly(String *localPath);
		//
		// PURPOSE
		//  Sets or clears the read-only attribute of the specified file.
		// PARAMS
		//	fileInfo			- Encapsulates the file to be configured.
		//	shouldBeWriteable	- If true, the read-only attribute of the file is cleared.
		//
		virtual void SetLocalFileWriteableState(FileInfo *fileInfo, bool shouldBeWriteable);
		//
		// PURPOSE
		//  Sets or clears the read-only attribute of the specified folder and all subfolders and files recursively.
		// PARAMS
		//	dirInfo				- Encapsulates the folder to be configured.
		//	shouldBeWriteable	- If true, the read-only attribute of the folder and all subfolders and files is cleared
		//							recursively.
		//
		virtual void SetLocalFolderWriteableState(DirectoryInfo *dirInfo, bool shouldBeWriteable);
		//
		// PURPOSE
		//  Initializes the manager by logging-in to the asset management system.
		// PARAMS
		//  serverName			- The name of the remote server that hosts the asset management database.
		//	projectName			- The name of the database project to connect to.
		//	userName			- The username to be used to connect to the database project.
		//	passwordStr			- The password to be used to connect to the database project. Can be an empty string if no
		//							password is required.
		//	shouldWorkLocally	- If true, the manager gets the latest assets (not overwriting any locally writeable assets)
		//							but does permanently affect any of the assets in the database (check-out, check-in,
		//							import and delete operations are handled specially.)
		// RETURNS
		//  Returns true if the operation succeeded and false if the operation failed.
		//
		virtual bool Init(String *serverName, String *projectName, String *userName, String *passwordStr,
			bool shouldWorkLocally) = 0;
		//
		// PURPOSE
		//  Shuts-down the manager by logging-out of the asset management system.
		// RETURNS
		//  Returns true if the operation succeeded and false if the operation failed.
		//
		virtual bool Shutdown(void) = 0;
		//
		// PURPOSE
		//  Shuts-down the manager by logging-out of the asset management system and re-initializes the manager by logging-in
		//	to the asset management system.
		// RETURNS
		//  Returns true if the operation succeeded and false if the operation failed.
		//
		virtual bool Reconnect(void) = 0;
		//
		// PURPOSE
		//  Gets the local working path for a given asset path in the project database.
		// PARAMS
		//  assetPath			- The asset path for which the local working path is to be queried.
		// RETURNS
		//  The local working path of the specified asset, or a NULL string if the operation failed.
		//
		virtual String *GetWorkingPath(String *assetPath) = 0;
		//
		// PURPOSE
		//  Sets the local working path for a given asset path in the project database.
		// PARAMS
		//  assetPath			- The asset path to be configured.
		//	localWorkingPath	- The local working path to be used.
		// RETURNS
		//  Returns true if the operation succeeded and false if the operation failed.
		//
		virtual bool SetWorkingPath(String *assetPath, String *localWorkingPath) = 0;
		//
		// PURPOSE
		//  Determines if a specified asset (or path) is up to date on the local disk.
		// PARAMS
		//	assetPath	- The asset path to be queried.
		// RETURNS
		//  Returns true if the asset (or path) is up to date on the local disk.
		//
		virtual bool IsUpToDate(String *assetPath) = 0;
		//
		// PURPOSE
		//  Recursively pulls the latest versions of the assets located at the specified path in the project
		//	database.
		// PARAMS
		//  assetPath	- The root asset path to be pulled locally.
		// RETURNS
		//  Returns true if the operation succeeded and false if the operation failed.
		// NOTES
		//  Silently overwrites local write-enabled assets, but does not overwrite checked-out assets.
		//
		virtual bool GetLatest(String *assetPath) = 0;
		//
		// PURPOSE
		//  Recursively pulls the latest versions of the assets located at the specified path in the project
		//	database.
		// PARAMS
		//  assetPath					- The root asset path to be pulled locally.
		//	shouldDefaultResponses		- Specifies whether all responses should be defaulted (inhibiting dialogs.)
		// RETURNS
		//  Returns true if the operation succeeded and false if the operation failed.
		// NOTES
		//  If responses are defaulted, this function may silently overwrite local write-enabled assets, but it never
		//	overwrites checked-out assets.
		//
		virtual bool GetLatest(String *assetPath, bool shouldDefaultResponses) = 0;
		//
		// PURPOSE
		//  Determines if a specified asset (or path) exists in the project database.
		// PARAMS
		//  assetPath	- The asset path to be queried.
		// RETURNS
		//  Returns true if the asset (or path) exists in the project database.
		//
		virtual bool ExistsAsAsset(String *assetPath) = 0;
		//
		// PURPOSE
		//  Determines if a specified asset (or path) is locked by another user.
		// PARAMS
		//  assetPath	- The asset path to be queried.
		// RETURNS
		//  Returns true if the asset (or path) is locked by another user.
		//
		virtual bool IsLocked(String *assetPath) = 0;
		//
		// PURPOSE
		//  Returns the name of the current owner of a locked asset.
		// PARAMS
		//  assetPath	- The asset path to be queried.
		// RETURNS
		//  Returns the name of the current owner of the asset if locked, or an empty string if the asset is not locked.
		//
		virtual String *GetLockedOwner(String *assetPath) = 0;
		//
		// PURPOSE
		//  Determines if a specified asset (or path) is checked-out by the current user.
		// PARAMS
		//  assetPath	- The asset path to be queried.
		// RETURNS
		//  Returns true if the asset (or path) is checked-out by the current user.
		//
		virtual bool IsCheckedOut(String *assetPath) = 0;
		//
		// PURPOSE
		//  Recursively checks-out the asset(s) located at the specified path in the project database, leaving
		//	a comment if specified.
		// PARAMS
		//  assetPath	- The asset path to be checked-out.
		//	comment		- A check-out comment to be logged. Can be an empty string if no comment is required.
		// RETURNS
		//  Returns true if the operation succeeded and false if the operation failed.
		// NOTES
		//  Silently overwrites local write-enabled assets.
		//
		virtual bool CheckOut(String *assetPath, String *comment)=0;
		//as above but doesn't lock the assets
		virtual bool LocalCheckOut(String *assetPath, String *comment)=0;
		//
		// PURPOSE
		//	A helper function that performs the functions of CheckOut() only if the result of ExistsAsAsset() is
		//	true and the result of IsCheckedOut() is false.
		// PARAMS
		//  assetPath	- The asset path to be checked-out.
		//	comment		- A check-out comment to be logged. Can be an empty string if no comment is required.
		// RETURNS
		//  Returns true if the operation succeeded and false if the operation failed.
		// NOTES
		//  Silently overwrites local write-enabled assets.
		//
		virtual bool SimpleCheckOut(String *assetPath, String *comment) = 0;
		//
		// PURPOSE
		//  Recursively undoes the check-out of the asset(s) located at the specified path in the project
		//	database.
		// PARAMS
		//  assetPath	- The asset path for which the check-out is to be undone.
		// RETURNS
		//  Returns true if the operation succeeded and false if the operation failed.
		// NOTES
		//  Silently overwrites any local changes to the asset(s).
		//
		virtual bool UndoCheckOut(String *assetPath) = 0;
		//
		// PURPOSE
		//  A helper function that performs the functions of UndoCheckOut() only if the result of ExistsAsAsset()
		//	is true.
		// PARAMS
		//  assetPath	- The asset path for which the check-out is to be undone.
		// RETURNS
		//  Returns true if the operation succeeded and false if the operation failed.
		// NOTES
		//  Silently overwrites any local changes to the asset(s).
		//
		virtual bool SimpleUndoCheckOut(String *assetPath) = 0;
		//
		// PURPOSE
		//  Recursively checks-in the asset(s) located at the specified path in the project database, leaving
		//	a comment if specified.
		// PARAMS
		//  assetPath	- The asset path to be checked-in.
		//	comment		- A check-in comment to be logged. Can be an empty string if no comment is required.
		// RETURNS
		//  Returns true if the operation succeeded and false if the operation failed.
		//
		virtual bool CheckIn(String *assetPath, String *comment) = 0;
		//
		// PURPOSE
		//  Recursively imports the asset(s) located at the specified local path into the specified asset path
		//	in the project database, leaving a comment if specified.
		// PARAMS
		//	localPath	- The local path to be imported.
		//  assetPath	- The asset path destination for the imported asset(s) (or path).
		//	comment		- An import comment to be logged. Can be an empty string if no comment is required.
		// RETURNS
		//  Returns true if the operation succeeded and false if the operation failed.
		//
		virtual bool Import(String *localPath, String *assetPath, String *comment) = 0;
		//
		// PURPOSE
		//  A helper function that performs the functions of CheckIn() if the result of ExistsAsAsset()
		//	is true or the functions of Import() if the result of ExistsAsAsset() is false.
		// PARAMS
		//	localPath	- The local path to be imported (if necessary).
		//  assetPath	- The asset path to be checked-in / the destination for the imported asset(s) (or path).
		//	comment		- An check-in / import comment to be logged. Can be an empty string if no comment is
		//					required.
		// RETURNS
		//  Returns true if the operation succeeded and false if the operation failed.
		//
		virtual bool CheckInOrImport(String *localPath, String *assetPath, String *comment) = 0;
		//
		// PURPOSE
		//  Recursively deletes the asset(s) located at the specified local and database paths.
		// PARAMS
		//	localPath	- The local path of the asset(s) to be deleted.
		//  assetPath	- The database path of the asset(s) to be deleted.
		//	deleteLocal	- Defines whether or not the local copy of the asset(s) is to be deleted (along with the
		//					copy in the project database.)
		// RETURNS
		//  Returns true if the operation succeeded and false if the operation failed.
		// NOTES
		//  Silently deletes the asset(s) if they are checked-out by the current user, but does not delete the
		//	asset(s) if locked by another user.
		//
		virtual bool DeleteAsset(String *localPath, String *assetPath, bool deleteLocal) = 0;
		//
		// PURPOSE
		//	Renames the asset in the project database.
		// PARAMS
		//	assetPath	- The path of the asset to be renamed.
		//	newName		- The new name for the asset.
		// RETURNS
		//	Returns true if the operation succeeded and false if the operation failed.
		virtual bool RenameAsset(String *assetPath, String *newName) = 0;
		//
		// PURPOSE
		//	Creates a new folder in the project database at the specified path.
		// PARAMS
		//  folderPath	- The path of the folder to be created.
		//	comment		- A comment to be logged. Can be an empty string if no comment is required.
		// RETURNS
		//  Returns true if the operation succeeded and false if the operation failed.
		//
		virtual bool CreateFolder(String *folderPath, String *comment) = 0;
		//
		// PURPOSE
		//  Provides the type of asset management instance of this object
		// RETURNS
		//  The enum in assetmanagertypes.h based on which asset manager type this object is
		virtual int GetAssetManagementType() = 0;
		//
		// PURPOSE
		//	Creates a changelist that is used until CheckInCurrentChangelist() is called.
		// PARAMS
		// RETURNS
		//  Returns true if the operation succeeded and false if the operation failed.
		//
		virtual bool CreateCurrentChangelist(String *comment) = 0;
		virtual bool CheckInCurrentChangelist() = 0;
		virtual bool DeleteCurrentChangelist() = 0;
	};
}

#endif // ASSET_MANAGER_H
