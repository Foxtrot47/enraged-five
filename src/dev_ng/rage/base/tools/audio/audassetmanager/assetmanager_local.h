//
// tools/audassetmanager/assetmanager_local.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//
#ifndef ASSET_MANAGER_LOCAL_H
#define ASSET_MANAGER_LOCAL_H

#include "assetmanager.h"

using namespace System;

namespace audAssetManagement
{
	//
	// PURPOSE
	//  The local filesystem implementation of the Asset Manager.
	// NOTES
	//  Acts as a dummy Asset Management system and allows direct access to the local assets. See audAssetManager for
	//	documentation.
	// SEE ALSO
	//  audAssetManager
	//
	public __gc class audAssetManagerLocal : public audAssetManager
	{
public:
		audAssetManagerLocal();
		bool Init(String *serverName, String *projectName, String *userName, String *passwordStr, bool shouldWorkLocally);
		bool Shutdown(void);
		bool Reconnect(void);
		String *GetWorkingPath(String *assetPath);
		bool SetWorkingPath(String *assetPath, String *localWorkingPath);
		bool IsUpToDate(String *assetPath);
		bool GetLatest(String *assetPath);
		bool GetLatest(String *assetPath, bool shouldDefaultResponses);
		bool ExistsAsAsset(String *assetPath);
		bool IsLocked(String *assetPath);
		String *GetLockedOwner(String *assetPath);
		bool IsCheckedOut(String *assetPath);
		bool LocalCheckOut(String *assetPath, String *comment);
		bool CheckOut(String *assetPath, String *comment);
		bool SimpleCheckOut(String *assetPath, String *comment);
		bool UndoCheckOut(String *assetPath);
		bool SimpleUndoCheckOut(String *assetPath);
		bool CheckIn(String *assetPath, String *comment);
		bool Import(String *localPath, String *assetPath, String *comment);
		bool CheckInOrImport(String *localPath, String *assetPath, String *comment);
		bool DeleteAsset(String *localPath, String *assetPath, bool deleteLocal);
		bool RenameAsset(String *oldName, String *newName);
		bool CreateFolder(String *folderPath, String *comment);
		bool CreateCurrentChangelist(String *comment);
		bool CheckInCurrentChangelist();
		bool DeleteCurrentChangelist();

		int GetAssetManagementType()  { return audAssetManagement::ASSET_MANAGER_LOCAL; }

private:
		String *m_LocalWorkingPath;
		String *m_AssetWorkingPath;
	};
}

#endif // ASSET_MANAGER_LOCAL_H
