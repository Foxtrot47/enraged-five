//
// tools/audassetmanager/assetmanager_ab7.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//
#ifndef ASSET_MANAGER_AB7_H
#define ASSET_MANAGER_AB7_H

#include "assetmanager.h"

using namespace ::NAMESPACELib;
using namespace ::NxNXMLHELPERLib;
using namespace System;

namespace audAssetManagement
{
	//
	// PURPOSE
	//  The Alienbrain Version 7 implementation of the Asset Manager.
	// NOTES
	//  Uses the COM DLLs provided by Alienbrain to issue the commands. See audAssetManager for documentation.
	// SEE ALSO
	//  audAssetManager
	//
	public __gc class audAssetManagerAb7 : public audAssetManager
	{
public:
		audAssetManagerAb7();
		bool Init(String *serverName, String *projectName, String *userName, String *passwordStr, bool shouldWorkLocally);
		bool Shutdown(void);
		bool Reconnect(void);
		String *GetWorkingPath(String *assetPath);
		bool SetWorkingPath(String *assetPath, String *localWorkingPath);
		bool IsUpToDate(String *assetPath);
		bool GetLatest(String *assetPath);
		bool GetLatest(String *assetPath, bool shouldDefaultResponses);
		bool ExistsAsAsset(String *assetPath);
		bool IsLocked(String *assetPath);
		String *GetLockedOwner(String *assetPath);
		bool IsCheckedOut(String *assetPath);
		bool LocalCheckOut(String *assetPath, String *comment);
		bool CheckOut(String *assetPath, String *comment);
		bool SimpleCheckOut(String *assetPath, String *comment);
		bool UndoCheckOut(String *assetPath);
		bool SimpleUndoCheckOut(String *assetPath);
		bool CheckIn(String *assetPath, String *comment);
		bool Import(String *localPath, String *assetPath, String *comment);
		bool CheckInOrImport(String *localPath, String *assetPath, String *comment);
		bool DeleteAsset(String *localPath, String *assetPath, bool deleteLocal);
		bool RenameAsset(String *oldName, String *newName);
		bool CreateFolder(String *folderPath, String *comment);
		bool CreateCurrentChangelist(String *comment);
		bool CheckInCurrentChangelist();
		bool DeleteCurrentChangelist();

		int GetAssetManagementType()  { return audAssetManagement::ASSET_MANAGER_AB7; }

	private:
		bool m_ShouldWorkLocally;
		String *m_ProjectName;
		String *m_ServerName;
		String *m_UserName;
		String *m_PasswordStr;
		NxNNamespaceHelperClass *m_NxnNamespace;
		NxNXMLParameterClass *m_NxnParam;
	};
}

#endif // ASSET_MANAGER_AB7_H
