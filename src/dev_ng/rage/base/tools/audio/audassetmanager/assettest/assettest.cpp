//
// tools/audassetmanager/assettest/assettest.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//
#include "stdafx.h"
#using <mscorlib.dll>

#include "assetmanagertypes.h"

using namespace audAssetManagement;
using namespace System;

const int g_AssetManagerType = ASSET_MANAGER_PERFORCE;

const char g_DefaultAssetServerName[] =			"jjess";	//"COLIN_ENTWISTLE";//
const char g_DefaultAssetProjectName[] =		"jjess";	//"RAGEAudio_Demo";//
const char g_DefaultAssetUserName[] =			"jessup";
const char g_DefaultAssetPasswordStr[] =		"";
const char g_NewFolderName[] =					"folder";

const char g_DefaultAssetPath[] =	"audio//projectList.xml";
const char g_DefaultNewAssetPath[] = "audio//G#1.wav";

//TODO: perforce m_AssetWorkingPath is not handled by our tools correctly


void main(void)
{
	audAssetManager *assetManager = audAssetManager::CreateInstance(g_AssetManagerType);
	//Login.
	if(!assetManager || !assetManager->Init(g_DefaultAssetServerName, g_DefaultAssetProjectName,
		g_DefaultAssetUserName, g_DefaultAssetPasswordStr, false))
	{
		Console::WriteLine(S"Failed to initialise Asset Manager");
		return;
	}

// 	String *assetPath = String::Concat(g_DefaultAssetBuildVersionPath, S"Test1");
// 	if(!assetManager->DeleteAsset(assetManager->GetWorkingPath(assetPath), assetPath, true))
// 	{
// 		Console::WriteLine(S"Failed to delete asset");
// 		return;
// 	}
//  if(assetManager->IsUpToDate(g_DefaultAssetPath))
//  {
//  	Console::WriteLine(S"Up to date");
//  }
//  else
//  {
//  	Console::WriteLine(S"Not up to date");
//  }
// 	if (assetManager->CheckIn(g_DefaultAssetPath, "no comment"))
// 	{
// 		Console::WriteLine(S"check in");
// 	}

  	if (!assetManager->CheckOut(g_DefaultAssetPath, "no comment"))
  	{
  		Console::WriteLine(S"failed to checkout");
  		return;
  	}
  	if (!assetManager->IsCheckedOut(g_DefaultAssetPath))
  	{
  		Console::WriteLine(S"failed ischeckedout");
  		return;
  	}

//  if (!assetManager->UndoCheckOut(g_DefaultAssetPath))
//  {
//  	Console::WriteLine(S"failed undocheckout");
//  	return;
//  }
// 	if (!assetManager->CheckIn(g_DefaultAssetPath))
// 	{
// 		Console::WriteLine(S"failed to checkin");
// 		return;
// 	}

// 	assetManager->RenameAsset(g_DefaultAssetPath, g_DefaultNewAssetPath);
//	assetManager->Import("", g_DefaultNewAssetPath, "");

	if (assetManager->IsUpToDate(g_DefaultNewAssetPath))
	{
		Console::WriteLine(S"up to date!");
	}

	if(!assetManager->Shutdown())
	{
		Console::WriteLine(S"Failed to shutdown Asset Manager");
		return;
	}

	return;
}
