//
// tools/audassetmanager/assetmanager_perforce.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//
#ifndef ASSET_MANAGER_PERFORCE_H
#define ASSET_MANAGER_PERFORCE_H

#include "assetmanager.h"

#include "customclientuser_perforce.h"

using namespace System;

namespace audAssetManagement
{
	//
	// PURPOSE
	//  The Perforce implementation of the Asset Manager.
	// NOTES
	//  Uses the API libraries provided by Perforce to issue the commands. See audAssetManager for documentation.
	// SEE ALSO
	//  audAssetManager
	//
	public __gc class audAssetManagerPerforce : public audAssetManager
	{
public:
		audAssetManagerPerforce();
		~audAssetManagerPerforce();
		bool Init(String *serverName, String *projectName, String *userName, String *passwordStr, bool shouldWorkLocally);
		bool Shutdown(void);
		bool Reconnect(void);
		String *GetWorkingPath(String *assetPath);
		//
		// PURPOSE
		//  Sets the local working path for a given asset path in the project database.
		// PARAMS
		//  assetPath			- The asset path to be configured.
		//	localWorkingPath	- The local working path to be used.
		// RETURNS
		//  Returns true if the operation succeeded and false if the operation failed.
		// NOTES
		//	This functionality is not supported for Perforce.
		//
		bool SetWorkingPath(String *assetPath, String *localWorkingPath);
		bool IsUpToDate(String *assetPath);
		bool GetLatest(String *assetPath);
		bool GetLatest(String *assetPath, bool shouldDefaultResponses);
		bool ExistsAsAsset(String *assetPath);
		bool ExistsAsDir(String *assetPath);
		bool IsLocked(String *assetPath);
		String *GetLockedOwner(String *assetPath);
		bool IsCheckedOut(String *assetPath);
		bool LocalCheckOut(String *assetPath, String *comment);
		bool CheckOut(String *assetPath, String *comment);
		bool SimpleCheckOut(String *assetPath, String *comment);
		bool UndoCheckOut(String *assetPath);
		bool SimpleUndoCheckOut(String *assetPath);
		bool CheckIn(String *assetPath, String *comment);
		bool Import(String *localPath, String *assetPath, String *comment);
		bool CheckInOrImport(String *localPath, String *assetPath, String *comment);
		bool DeleteAsset(String *localPath, String *assetPath, bool deleteLocal);
		bool RenameAsset(String *oldName, String *newName);
		bool CreateFolder(String *folderPath, String *comment);
		bool CreateCurrentChangelist(String *comment);
		bool CheckInCurrentChangelist();
		bool DeleteCurrentChangelist();

		int GetAssetManagementType() { return audAssetManagement::ASSET_MANAGER_PERFORCE; }

		String* PathFixUp( String* pPath );
		String *CreateValidPath(String *assetPath);
		String *CreateImportValidPath(String *assetPath);

	protected:
		void CreateNewChangelist(const char *assetPath);
		void SetCommentString(String *comment);
		void InvokePerforceCmd(const int cmd, const char *p4cmd, const int cnt, const char *cmd1, const char* cmd2, const char *cmd3, const char *cmd4);
		bool IsLockedByMe(String *assetPath);

	private:
		bool m_ShouldWorkLocally;
		String *m_HostName;
		String *m_ClientName;
		String *m_UserName;
		String *m_Password;
		CustomClientUser *m_ClientUser;
		ClientApi *m_Client;
		String *m_AssetWorkingPath;
		String *m_ChangeListNumber;

		String *m_MasterChangelistNumber;
		bool m_bMasterChangelistCreated;
		bool Reopen(const char* changeList, char *assetPath);

	};
}

#endif // ASSET_MANAGER_PERFORCE_H
