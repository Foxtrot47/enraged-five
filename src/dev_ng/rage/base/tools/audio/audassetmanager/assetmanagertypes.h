//
// tools/audassetmanager/assetmanagertypes.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//
#ifndef ASSET_MANAGER_TYPES_H
#define ASSET_MANAGER_TYPES_H

namespace audAssetManagement
{
	//
	// PURPOSE
	//  Defines the separate asset management systems supported by the Asset Manager. These constants should
	//	map to individual system-specific implementations of the base audAssetManager class.
	// SEE ALSO
	//  audAssetManager
	//
	enum audAssetManagerTypes
	{
		ASSET_MANAGER_AB6,
		ASSET_MANAGER_AB7,
		ASSET_MANAGER_PERFORCE,
		ASSET_MANAGER_LOCAL,

		//Add new Asset Manager types here.
		NUM_BUILD_PLATFORMS
	};
}

#endif // ASSET_MANAGER_TYPES_H
