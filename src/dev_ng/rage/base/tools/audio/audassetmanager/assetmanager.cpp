//
// tools/audassetmanager/assetmanager.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//
#include "stdafx.h"
#include "assetmanager_local.h"
#include "assetmanager_perforce.h"

using namespace audAssetManagement;

audAssetManager *audAssetManager::CreateInstance(audAssetManagerTypes type)
{
	audAssetManager __gc *assetManager = 0;

	switch(type)
	{
 		case ASSET_MANAGER_PERFORCE:
 			assetManager = new audAssetManagerPerforce();
 			break;

		case ASSET_MANAGER_LOCAL:
			assetManager = new audAssetManagerLocal();
			break;

		//case ASSET_MANAGER_VSS:

		//Add new Asset Manager types here.
	}

	return assetManager;
}

void audAssetManager::MakeLocalAssetWriteable(String *localPath)
{
	if(!localPath || !localPath->Length)
	{
		return;
	}

	IO::FileInfo *localFileInfo = new IO::FileInfo(localPath);
	if(localFileInfo->Exists)
	{
		SetLocalFileWriteableState(localFileInfo, true);
	}
	else
	{
		//This could be a directory rather than a file...
		IO::DirectoryInfo *localDirectoryInfo = new IO::DirectoryInfo(localPath);
		if(localDirectoryInfo->Exists)
		{
			SetLocalFolderWriteableState(localDirectoryInfo, true);
		}
	}
}

void audAssetManager::MakeLocalAssetReadOnly(String *localPath)
{
	if(!localPath || !localPath->Length)
	{
		return;
	}

	FileInfo *localFileInfo = new FileInfo(localPath);
	if(localFileInfo->Exists)
	{
		SetLocalFileWriteableState(localFileInfo, false);
	}
	else
	{
		//This could be a directory rather than a file...
		DirectoryInfo *localDirectoryInfo = new DirectoryInfo(localPath);
		if(localDirectoryInfo->Exists)
		{
			SetLocalFolderWriteableState(localDirectoryInfo, false);
		}
	}
}

void audAssetManager::SetLocalFileWriteableState(FileInfo *fileInfo, bool shouldBeWriteable)
{
	int attributes;

	if(shouldBeWriteable)
	{
		//Remove readonly attribute.
		if((fileInfo->Attributes & FileAttributes::ReadOnly) != 0)
		{
			attributes = static_cast<int>(fileInfo->Attributes);
			attributes -= static_cast<int>(FileAttributes::ReadOnly);
			fileInfo->Attributes = static_cast<FileAttributes>(attributes);
		}
	}
	else
	{
		//Add readonly attribute.
		if((fileInfo->Attributes & FileAttributes::ReadOnly) == 0)
		{
			attributes = static_cast<int>(fileInfo->Attributes);
			attributes += static_cast<int>(FileAttributes::ReadOnly);
			fileInfo->Attributes = static_cast<FileAttributes>(attributes);
		}
	}
}

void audAssetManager::SetLocalFolderWriteableState(DirectoryInfo *dirInfo, bool shouldBeWriteable)
{
	int attributes;

	if(shouldBeWriteable)
	{
		//Remove readonly attribute.
		if((dirInfo->Attributes & FileAttributes::ReadOnly) != 0)
		{
			attributes = static_cast<int>(dirInfo->Attributes);
			attributes -= static_cast<int>(FileAttributes::ReadOnly);
			dirInfo->Attributes = static_cast<FileAttributes>(attributes);
		}
	}
	else
	{
		//Add readonly attribute.
		if((dirInfo->Attributes & FileAttributes::ReadOnly) == 0)
		{
			attributes = static_cast<int>(dirInfo->Attributes);
			attributes += static_cast<int>(FileAttributes::ReadOnly);
			dirInfo->Attributes = static_cast<FileAttributes>(attributes);
		}
	}

    FileSystemInfo *fileSystemInfos __gc[] = dirInfo->GetFileSystemInfos();

    IEnumerator *infoEnum = fileSystemInfos->GetEnumerator();
    FileSystemInfo *fileSystemInfo;
    while(infoEnum->MoveNext())
	{
        fileSystemInfo = __try_cast<FileSystemInfo *>(infoEnum->Current);

        if(dynamic_cast<DirectoryInfo *>(fileSystemInfo) != 0)
		{
            SetLocalFolderWriteableState(dynamic_cast<DirectoryInfo*>(fileSystemInfo), shouldBeWriteable);
		}
        else if(dynamic_cast<FileInfo *>(fileSystemInfo) != 0)
		{
            SetLocalFileWriteableState(dynamic_cast<FileInfo*>(fileSystemInfo), shouldBeWriteable);
        }
    }
}
