#include "assetmanager.h"

using namespace System;
using namespace System::IO;
using namespace NUnit::Framework;
using namespace audAssetManagement;

namespace NUnitTest
{
	[TestFixture]
	public __gc class audAssetManagmentTest
	{
		audAssetManager *m_AssetManager;
		ArrayList *m_Files;

	public:
		[SetUp] void Init();

		[Test] void CheckOutIn();
		[Test] void CheckOutUndo();
		[Test] void GetLatest();

	};
}
