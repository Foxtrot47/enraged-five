//
// tools/audassetmanager/customclientuser_perforce.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//
#ifndef CUSTOM_CLIENT_USER_PERFORCE_H
#define CUSTOM_CLIENT_USER_PERFORCE_H

#include "p4api/clientapi.h"
#include <string>
#include <hash_map>
#include <iostream>
#include <xutility>

//////////////////////////////////////////////////////////////////////////
// if you love managed code, you'll dig STL
class stringhasher : public stdext::hash_compare<std::string>
{
public:
	size_t operator() (const std::string& s) const
	{
		size_t h = 0;
		std::string::const_iterator p, p_end;
		for(p = s.begin(), p_end = s.end(); p != p_end; ++p)
		{
			h = 31 * h + (*p);
		}
		return h;
	}

	bool operator() (const std::string& s1, const std::string& s2) const
	{
		return s1 < s2;
	}
};

// alias these data structures
typedef stdext::hash_map<std::string, std::string, stringhasher>	HASH_S_S;
typedef std::pair<std::string, std::string>							PAIR_S_S;
typedef std::pair<HASH_S_S::iterator, bool>							PAIR_I_B;
typedef std::list<std::string>										LIST_S;


//////////////////////////////////////////////////////////////////////////
// struct to cache fstat info from perforce
struct P4FileStatInfo
{
	P4FileStatInfo();

	void Reset();
	bool IsFileUpToDate() const;
	bool IsAsset() const;
	bool IsOpen() const;

	bool m_OurLock;

	enum { kMaxString=256 };

	char action[kMaxString];
	char actionOwner[kMaxString];
	char change[kMaxString];
	char clientFile[kMaxString];
	char depotFile[kMaxString];
	char haveRev[kMaxString];
	char headAction[kMaxString];
	char headRev[kMaxString];
};

//////////////////////////////////////////////////////////////////////////
// bunch of perforce operations
enum P4Command 
{
	p4None = 0,
	p4Info,
	p4Sync,
	p4IsLocked,
	p4ExistsAsAsset,				//headAction
	p4ActionOwner,
	p4Edit,
	p4Revert,
	p4Submit,
	p4Add,
	p4Change,
	p4Dir,
	p4Lock,
	p4Delete,
	p4Fstat,
	p4Describe,
	p4Changes,
	p4ChangesNext,
	p4Login,
	P4Reopen,					// internal use only
};

// PURPOSE
//  A custom implementation of the Perforce client user for the Asset Manager.
// NOTES
//  Overrides the OutputStat method to enable parsing of data returned by the Perforce server.
//
class CustomClientUser : ClientUser
{
public:
	CustomClientUser();

	// PURPOSE
	//  Returns the value string extracted in the call to OutputStat made during the processing of the last Perforce server
	//	command.
	// RETURNS
	//	The most recent value string extracted based upon a previously specified tag name, or NULL if no value is
	//	available.
	const char* GetStringResult();
	int	GetIntResult();
	
	//PURPOSE
	//returns the error message from the error caused by the last command
	std::string GetErrorMessage();

	//PURPOSE
	//returns the information message from the lat command
	std::string GetInfoMessage();

	//PURPOSE
	//Clears Error messages, info messages, changelist info and file info
	void ClearResults();

	// PURPOSE
	//  sets the current command to process, after invoking m_Client->Run()
	void SetCommand(P4Command eCommand);

	// PURPOSE
	//  Returns true if an error occurred during the processing of the last Perforce server command.
	// RETURNS
	//	True if an error occurred during the processing of the last Perforce server command.
	bool HasErrorOccurred();

	// PURPOSE
	//  init routines
	void SetClient(const char* clientName);
	void SetUser(const char* userName);
	void SetPassword(const char* password);
	void SetFindResult(const char* findResult);
	void SetCommentString(const char *comment);

	// PURPOSE
	//  these functions return a number and string name of each pending change list
	const int GetNumPendingChangeLists() const;
	const char* GetPendingChangeList(const int idx); 

	// PURPOSE
	//  methods to query whether a file is in a change list, or create an association
	const char* GetChangeListNumber(const char *hashKey);
 	bool ChangeListExists(const char *hashKey);
	const char* GetChangeListNumber() const { return m_ChangeListNumber; }

	// PURPOSE
	//  routines for gathering status on a particular file after running a p4FStat command
	const P4FileStatInfo & GetFileInfo() const	{ return m_FileInfo; }

	//PURPOSE
	//  Generates the changelist -> local filename association
	void BuildHashMap(ClientApi *pAPI);
	void UndoCheckout(ClientApi *pAPI, const char *hashKey);
	void AssociateFileHash(const char *hashKey);
	void UnassociateFileHash(const char *hashKey);

protected:
	enum 
	{ 
		g_MaxTagNameLength = 100,
		g_MaxValueStringLength = 100,
		g_MaxNameStringLength = 100,
		g_MaxComment = 1024,
	};

	// PURPOSE
	//  Overrides the base class implementation to support extraction of the variable associated with the name set by a
	//	previous call to SetTagNameToExtract. Called automatically by the Perforce server in response to the running of a
	//	server command.
	// PARAMS
	//	varList	- A Perforce API StrDict object containing a list of tagged variables returned by the last command.
	virtual void OutputStat(StrDict *varList);

	// PURPOSE
	//  Overrides the base class implementation to inhibit printing to stdout and to flag that no error has occurred.
	virtual void OutputBinary(const char *data, int length);
	virtual void OutputInfo(char level, const char *data);
	virtual void OutputText(const char *data, int length);
	virtual void OutputError(const char *error);

	// PURPOSE
	//  Overrides for error cases and status information
	void InputData( StrBuf* pMyData, Error* pError );
	void Prompt( const StrPtr &msg, StrBuf &rsp, int noEcho, Error *e);
	void ErrorPause(char *errBuf, Error *e);
	void HandleError( Error *err );
	void Message( Error *err );
	void Finished();

	// Helper functions
	void MakeUpperCase( char* szPath );
	void AddPendingChangeLists(const StrPtr *pStr);
	void SetChangeListNumber( const char* changeListNumber );
	void EmptyChangelistFile() { m_ChangelistFiles.empty(); }

	//////////////////////////////////////////////////////////////////////////
	// data members
	P4Command m_Command;

	bool m_HasErrorOccurred;

	// result storage
	int  m_IntResult;
	char m_StringResult[g_MaxTagNameLength];
	char m_FindResult[g_MaxTagNameLength];
	char m_Comment[g_MaxComment];

	// settings
	char m_ClientName[g_MaxNameStringLength];
	char m_UserName[g_MaxNameStringLength];
	char m_Password[g_MaxNameStringLength];

	// change lists
	bool m_bChangeListExists;	
	char m_ChangeListNumber[g_MaxValueStringLength];

	// struct to cache fstat info
	P4FileStatInfo m_FileInfo;

	// data structs
	HASH_S_S m_FileHash;			// hash to map a full path file to a change list
	HASH_S_S::iterator	m_FileHashIter;

	LIST_S m_Changelist;			// list of all pending change lists
	LIST_S m_ChangelistFiles;		// list of all files found in pending change lists

	std::string m_ErrorMessage;
	std::string m_InfoMessage;
};


#endif // CUSTOM_CLIENT_USER_PERFORCE_H
