#include "stdafx.h"
#include "assetmanager_test.h"

namespace NUnitTest{

	void audAssetManagmentTest::Init()
	{
		m_AssetManager = audAssetManager::CreateInstance(audAssetManagerTypes::ASSET_MANAGER_PERFORCE);
		m_AssetManager->Init(S"RSGEDIP4D01:1666", S"audiobuildclient-erika", S"erika.birse",S"",false);
	}

	void audAssetManagmentTest::CheckOutIn()
	{
		for(int i=0; i<100; i++)
		{
			String *s = String::Concat(S"Test comment ",i.ToString());
			Assert::AreEqual(m_AssetManager->CheckOut(S"X:\\audio\\GTA4_EP1_AUDIO\\PLATFORMDATA\\XENON\\BUILDINFO\\BUILTWAVES.XML",S""),true, S"Failed to check-in properly, error");
			FileAttributes fa = File::GetAttributes(S"X:\\audio\\GTA4_EP1_AUDIO\\PLATFORMDATA\\XENON\\BUILDINFO\\BUILTWAVES.XML");
			Assert::AreEqual((fa & FileAttributes::ReadOnly),0,S"File is Checked out but marked readonly");
			Assert::AreEqual(m_AssetManager->IsCheckedOut(S"X:\\audio\\GTA4_EP1_AUDIO\\PLATFORMDATA\\XENON\\BUILDINFO\\BUILTWAVES.XML"),true, S"Failed to check-out properly, no error");
			Assert::AreEqual(m_AssetManager->CheckIn(S"X:\\audio\\GTA4_EP1_AUDIO\\PLATFORMDATA\\XENON\\BUILDINFO\\BUILTWAVES.XML",s),true, S"Failed to check-in properly, error");
			Assert::AreEqual(m_AssetManager->IsCheckedOut(S"X:\\audio\\GTA4_EP1_AUDIO\\PLATFORMDATA\\XENON\\BUILDINFO\\BUILTWAVES.XML"),false, S"Failed to check-in properly, no error");
			fa = File::GetAttributes(S"X:\\audio\\GTA4_EP1_AUDIO\\PLATFORMDATA\\XENON\\BUILDINFO\\BUILTWAVES.XML");
			Assert::AreNotEqual((fa & FileAttributes::ReadOnly),0,S"File is Checked in but is writable");
			
		}
	}

	void audAssetManagmentTest::CheckOutUndo()
	{
		for(int i =0; i<100;i++)
		{
			Assert::AreEqual(m_AssetManager->CheckOut(S"X:\\audio\\GTA4_EP1_AUDIO\\PLATFORMDATA\\XENON\\BUILDINFO\\BUILTWAVES.XML",S""),true, S"Failed to check-in properly, error");
			Assert::AreEqual(m_AssetManager->IsCheckedOut(S"X:\\audio\\GTA4_EP1_AUDIO\\PLATFORMDATA\\XENON\\BUILDINFO\\BUILTWAVES.XML"),true, S"Failed to check-out properly, no error");
			Assert::AreEqual(m_AssetManager->UndoCheckOut(S"X:\\audio\\GTA4_EP1_AUDIO\\PLATFORMDATA\\XENON\\BUILDINFO\\BUILTWAVES.XML"),true, S"Failed to undo check-out properly, error");
			Assert::AreEqual(m_AssetManager->IsCheckedOut(S"X:\\audio\\GTA4_EP1_AUDIO\\PLATFORMDATA\\XENON\\BUILDINFO\\BUILTWAVES.XML"),false, S"Failed to undo check-out properly, no error");	
		}
	}

	void audAssetManagmentTest::GetLatest()
	{
		for(int i =0;i<100;i++)
		{
		FileAttributes fa = File::GetAttributes(S"X:\\audio\\GTA4_EP1_AUDIO\\PLATFORMDATA\\XENON\\BUILDINFO\\PendingWAVES.XML");
		fa = (FileAttributes)(fa | !FileAttributes::ReadOnly);
		File::SetAttributes(S"X:\\audio\\GTA4_EP1_AUDIO\\PLATFORMDATA\\XENON\\BUILDINFO\\PendingWAVES.XML",fa);
		Assert::AreEqual(m_AssetManager->GetLatest(S"X:\\audio\\GTA4_EP1_AUDIO\\PLATFORMDATA\\XENON\\BUILDINFO\\PendingWAVES.XML"),true,S"Claimed to get latest on a writable file");
		fa = (FileAttributes)(fa | FileAttributes::ReadOnly);
		File::SetAttributes(S"X:\\audio\\GTA4_EP1_AUDIO\\PLATFORMDATA\\XENON\\BUILDINFO\\PendingWAVES.XML",fa);
		}

	}

}



