//
// tools/audassetmanager/assetmanager_local.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//
#include "stdafx.h"

#include "assetmanager_local.h"

using namespace audAssetManagement;

audAssetManagerLocal::audAssetManagerLocal() : m_LocalWorkingPath("x:\\"), m_AssetWorkingPath("")
{
}

bool audAssetManagerLocal::Init(String *serverName, String *projectName, String *userName, String *passwordStr,
	bool shouldWorkLocally)
{
	(serverName);
	(projectName);
	(userName);
	(passwordStr);
	(shouldWorkLocally);

	return true;
}

bool audAssetManagerLocal::Shutdown(void)
{
	return true;
}

bool audAssetManagerLocal::Reconnect(void)
{
	return true;
}

String *audAssetManagerLocal::GetWorkingPath(String *assetPath)
{
	String *workingPath = 0;

	if(m_LocalWorkingPath && m_AssetWorkingPath)
	{
		if(assetPath->StartsWith("\\"))
		{
			assetPath = assetPath->Substring(1);	
		}
		if(assetPath->ToLower()->StartsWith(m_AssetWorkingPath->ToLower()))
		{
			if(m_AssetWorkingPath->Length > 0)
			{
				//Strip base asset working path.
				assetPath = assetPath->ToLower()->Replace(m_AssetWorkingPath->ToLower(), S"");
			}

			workingPath = m_LocalWorkingPath;
			//Ensure that we get a '\' between the paths.
			if(!workingPath->EndsWith(S"\\"))
			{
				workingPath = String::Concat(workingPath, S"\\");
			}

			workingPath = String::Concat(workingPath, assetPath);
		}
	}

	return workingPath;
}

bool audAssetManagerLocal::SetWorkingPath(String *assetPath, String *localWorkingPath)
{
	m_AssetWorkingPath = assetPath;
	m_LocalWorkingPath = localWorkingPath;
	return true;
}

bool audAssetManagerLocal::IsUpToDate(String *assetPath)
{
	assetPath = 0;
	return true;
}

bool audAssetManagerLocal::GetLatest(String *assetPath)
{
	assetPath = 0;
	return true;
}

bool audAssetManagerLocal::GetLatest(String *assetPath, bool shouldDefaultResponses)
{
	assetPath = 0;
	shouldDefaultResponses = false;
	return true;
}

bool audAssetManagerLocal::ExistsAsAsset(String *assetPath)
{
	bool exists = false;

	//Check if local asset exists.
	String *localPath = GetWorkingPath(assetPath);
	if(localPath)
	{
		IO::FileInfo *localFileInfo = new IO::FileInfo(localPath);
		if(localFileInfo->Exists)
		{
			exists = true;
		}
		else
		{
			//This could be a directory rather than a file.
			IO::DirectoryInfo *localDirectoryInfo = new IO::DirectoryInfo(localPath);
			exists = localDirectoryInfo->Exists;
		}
	}

	return exists;
}

bool audAssetManagerLocal::IsLocked(String *assetPath)
{
	assetPath = 0;
	return false;
}

String *audAssetManagerLocal::GetLockedOwner(String *assetPath)
{
	String *owner = S"";
	assetPath = 0;
	return owner;
}

bool audAssetManagerLocal::IsCheckedOut(String *assetPath)
{
	String *workingPath = GetWorkingPath(assetPath);
	if(workingPath)
	{
		FileAttributes fa = File::GetAttributes(workingPath);
		return !(fa & FileAttributes::ReadOnly);
	}

	return false;
}
bool audAssetManagerLocal::LocalCheckOut(String *assetPath, String *comment)
{
	comment = 0;
	String *workingPath = GetWorkingPath(assetPath);
	if(workingPath)
	{
		FileAttributes fa = File::GetAttributes(workingPath);
		fa = (FileAttributes)(fa & !FileAttributes::ReadOnly);
		File::SetAttributes(workingPath, fa);
		return true;
	}
	return false;
}

bool audAssetManagerLocal::CheckOut(String *assetPath, String *comment)
{
	comment = 0;
	String *workingPath = GetWorkingPath(assetPath);
	if(workingPath)
	{
		FileAttributes fa = File::GetAttributes(workingPath);
		fa = (FileAttributes)(fa & !FileAttributes::ReadOnly);
		File::SetAttributes(workingPath, fa);
		return true;
	}
	return false;
}

bool audAssetManagerLocal::SimpleCheckOut(String *assetPath, String *comment)
{
	assetPath = 0;
	comment = 0;
	return true;
}

bool audAssetManagerLocal::UndoCheckOut(String *assetPath)
{
	assetPath = 0;
	return true;
}

bool audAssetManagerLocal::SimpleUndoCheckOut(String *assetPath)
{
	assetPath = 0;
	return true;
}

bool audAssetManagerLocal::CheckIn(String *assetPath, String *comment)
{
	String *workingPath = GetWorkingPath(assetPath);
	if(workingPath)
	{
		FileAttributes fa = File::GetAttributes(workingPath);
		fa = (FileAttributes)(fa | FileAttributes::ReadOnly);
		File::SetAttributes(workingPath, fa);
		return true;
	}
	comment = 0;
	return false;
}

bool audAssetManagerLocal::Import(String *localPath, String *assetPath, String *comment)
{
	localPath = 0;
	assetPath = 0;
	comment = 0;
	return true;
}

bool audAssetManagerLocal::CheckInOrImport(String *localPath, String *assetPath, String *comment)
{
	localPath = 0;
	assetPath = 0;
	comment = 0;
	return true;
}

//
// NOTES
//	deleteLocal has no effect as the asset-managed and local versions are one and the same.
//
bool audAssetManagerLocal::DeleteAsset(String *localPath, String *assetPath, bool deleteLocal)
{
	(assetPath);
	(deleteLocal);

	//
	//Delete the local file/folder directly.
	//

	MakeLocalAssetWriteable(localPath);

	FileInfo *localFileInfo = new FileInfo(localPath);
	if(localFileInfo->Exists)
	{
		localFileInfo->Delete();

		return true;
	}
	else
	{
		//This could be a directory rather than a file.
		DirectoryInfo *localDirectoryInfo = new DirectoryInfo(localPath);
		if(localDirectoryInfo->Exists)
		{
			localDirectoryInfo->Delete(true);

			return true;
		}
	}

	return false;
}

bool audAssetManagerLocal::RenameAsset(String *assetPath, String *newName)
{
	//Rename local asset.
	String *localPath = GetWorkingPath(assetPath);
	if(localPath)
	{
		//Generate new local path.
		int splitIndex = localPath->LastIndexOf('\\');
		String *newPath = localPath->Substring(0, splitIndex);
		newPath = String::Concat(newPath, S"\\", newName);

		IO::FileInfo *localFileInfo = new IO::FileInfo(localPath);
		if(localFileInfo->Exists)
		{
			localFileInfo->MoveTo(newPath);
			return true;
		}
		else
		{
			//This could be a directory rather than a file.
			IO::DirectoryInfo *localDirectoryInfo = new IO::DirectoryInfo(localPath);
			if(localDirectoryInfo->Exists)
			{
				localDirectoryInfo->MoveTo(newPath);
				return true;
			}
		}
	}

	return false;
}

bool audAssetManagerLocal::CreateFolder(String *folderPath, String *comment)
{
	comment = 0;

	//Create a local folder.
	String *localPath = GetWorkingPath(folderPath);
	if(localPath)
	{
		IO::DirectoryInfo* localDirectoryInfo = IO::Directory::CreateDirectory(localPath);
		if(localDirectoryInfo->Exists)
		{
			return true;
		}
	}

	return false;
}

bool audAssetManagerLocal::CreateCurrentChangelist(String *)
{
	return true;
}

bool audAssetManagerLocal::CheckInCurrentChangelist()
{
	return true;
}

bool audAssetManagerLocal::DeleteCurrentChangelist()
{
	return true;
}
