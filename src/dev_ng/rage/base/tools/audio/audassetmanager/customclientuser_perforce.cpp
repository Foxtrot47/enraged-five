//
// tools/audassetmanager/customclientuser_perforce.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "stdafx.h"

#include "customclientuser_perforce.h"

#define STRSAFE_LIB
#include <strsafe.h>
#include <string.h>
#include <ctype.h>

using namespace System;
using namespace System::Diagnostics;

P4FileStatInfo::P4FileStatInfo()
{
	Reset();
}

void P4FileStatInfo::Reset()
{
	action[0] = '\0';
	actionOwner[0] = '\0';
	change[0] = '\0';
	clientFile[0] = '\0';
	depotFile[0] = '\0';
	haveRev[0] = '\0';
	headAction[0] = '\0';
	headRev[0] = '\0';

	m_OurLock = false;
}

bool P4FileStatInfo::IsFileUpToDate() const
{
	// file is up to date if head/have revs are same
	if (!strcmp(headRev,haveRev))
		return true;

	// file is up to date if we're adding it
	if (!_stricmp(action, "add"))
		return true;

	return false;
}

bool P4FileStatInfo::IsAsset() const
{
	// valid asset if the head action isn't 'delete' or something other than 'add'
	if (strlen(headAction))
	{
		if (0==_stricmp(headAction,"delete")) 
			return false;

		if ((0==_stricmp(headAction,"add"))		|| (0==_stricmp(headAction,"edit")) ||
			(0==_stricmp(headAction,"branch"))	|| (0==_stricmp(headAction,"integrate")))
			return true;
	}

	return false;
}

bool P4FileStatInfo::IsOpen() const
{
	if (strlen(action))
	{
		if ((0==_stricmp(action,"delete")) || (0==_stricmp(action,"add")) | (0==_stricmp(action,"edit")))
			return true;
	}

	return false;
}

CustomClientUser::CustomClientUser()
{
	m_StringResult[0] = '\0';
	m_IntResult = 0;
	m_HasErrorOccurred = false;
	m_Command = p4None;
	m_ChangeListNumber[0] = '\0';
	m_bChangeListExists = false;
	m_Comment[0] = '\0';

	// reset data structures
	m_Changelist.empty();
	m_ChangelistFiles.empty();
	m_FileHash.empty();

	::ClientUser();
}

void CustomClientUser::AddPendingChangeLists(const StrPtr *pStr)
{
	m_Changelist.push_back(pStr->Text());
}

const int CustomClientUser::GetNumPendingChangeLists() const
{
	return (int)m_Changelist.size();
}

				static const char *REMEMBER_ME;
				static const char *HUH;

void CustomClientUser::OutputStat(StrDict *varList)
{
	StrPtr *varStrPtr;

	switch( m_Command )
	{
	case p4Info:
		{
			ClearResults();
			varStrPtr = varList->GetVar( "clientRoot" );
			if( varStrPtr )
			{
				StringCbCopyN( m_StringResult, sizeof( m_StringResult ), varStrPtr->Text(), varStrPtr->Length() );

				if (!_stricmp("null", m_StringResult))
				{
					StringCbPrintfA( m_StringResult, sizeof(m_StringResult), "T:");
				}

				StringCbCat( m_StringResult, sizeof(m_StringResult), "\\");
				m_IntResult = 0;
			}
		}
		break;

	case p4IsLocked:
		{
			// To check for lock
			//		headAction = edit or add
			//		ourlock exists
			ClearResults();
			varStrPtr = varList->GetVar( "otherLock" );
			if(varStrPtr)
			{
				varStrPtr = varList->GetVar( "otherOpen" );
				if( varStrPtr )
				{
					// loop through otherOpen count to see who has it locked
					int sNumberOfTimesOpened = atol( varStrPtr->Text() );
					int sCount = 0;
					char szTageName[g_MaxTagNameLength];
					while( sCount < sNumberOfTimesOpened )
					{
						StringCbPrintfA( szTageName, sizeof(szTageName), "otherLock%d", sCount );
						varStrPtr = varList->GetVar( szTageName );

						if( varStrPtr )
						{
							char *pszClientName = varStrPtr->Text();
							if( pszClientName[0] != '\0' )
							{
								System::Diagnostics::Debug::Write(S"File Locked by : ");
								System::Diagnostics::Debug::WriteLine( pszClientName );
								StringCbCopyN( m_StringResult, sizeof( m_StringResult ), varStrPtr->Text(), varStrPtr->Length() );
							}
						}
						sCount++;
					}
				}
			}
		}
		break;
	case p4ExistsAsAsset:
		{
			ClearResults();
			varStrPtr = varList->GetVar( "headAction" );
			if( varStrPtr )
			{
				StringCbCopyN( m_StringResult, sizeof( m_StringResult ), varStrPtr->Text(), varStrPtr->Length() );
				m_IntResult = 0;
			}
			varStrPtr = varList->GetVar( "action" );
			if( varStrPtr )
			{
				StringCbCopyN( m_StringResult, sizeof( m_StringResult ), varStrPtr->Text(), varStrPtr->Length() );
				m_IntResult = 0;
			}

		}
		break;
	case p4ActionOwner:
		{
			ClearResults();

			varStrPtr = varList->GetVar( "actionOwner" );
			if( varStrPtr )
			{
				StringCbCopyN( m_StringResult, sizeof( m_StringResult ), varStrPtr->Text(), varStrPtr->Length() );
				m_IntResult = 0;
			}
		}
		break;
	case p4Dir:
		{
			// In order for this to work you must first call ClearResults and SetFindResult before running the dirs command.
			varStrPtr = varList->GetVar( "dir" );
			if( varStrPtr )
			{
				char szPath[1024];
				StringCbCopyN( szPath, sizeof( szPath ), varStrPtr->Text(), varStrPtr->Length() );
				MakeUpperCase( szPath );
				if( szPath[0] != '\0' && ( strcmp( &szPath[7], &m_FindResult[2] ) == 0 ) )	// we remove //DEPOT from szPath and we remove the drive letter from m_FindResult
				{
					StringCbCopyN( m_StringResult, sizeof( m_StringResult ), m_FindResult, sizeof(m_FindResult) );
					m_IntResult = 1;
					
					//String* szMessage( varStrPtr->Text() );
					System::Diagnostics::Debug::Write(S"Dir found : ");
					System::Diagnostics::Debug::WriteLine( szPath );
				}
			}
		}
		break;
	case p4Fstat:
		{
			ClearResults();
			m_FileInfo.Reset();

			//varList->Save(stdout);

			StrRef field;
			StrRef result;
			int idx = 0;

			while (varList->GetVar(idx, field, result))
			{
				const char *text = field.Text();

				// store off result of fstat
				if (!strcmp(text, "action"))
					StringCbCopyN(m_FileInfo.action, sizeof(m_FileInfo.action), result.Text(), result.Length());
				else if (!strcmp(text, "actionOwner"))
					StringCbCopyN(m_FileInfo.actionOwner, sizeof(m_FileInfo.actionOwner), result.Text(), result.Length());
				else if (!strcmp(text, "change"))
					StringCbCopyN(m_FileInfo.change, sizeof(m_FileInfo.change), result.Text(), result.Length());
				else if (!strcmp(text, "clientFile"))
				{
					StringCbCopyN(m_FileInfo.clientFile, sizeof(m_FileInfo.clientFile), result.Text(), result.Length());

					// need to fixup the client file to sync with the asset manager's path changes or the hash won't match
					for (unsigned int i=0; i<strlen(m_FileInfo.clientFile); ++i)
					{
						m_FileInfo.clientFile[i] = (char)toupper(m_FileInfo.clientFile[i]);
						if (m_FileInfo.clientFile[i] == '\\')
							m_FileInfo.clientFile[i] = '/';
					}
				}
				else if (!strcmp(text, "depotFile"))
					StringCbCopyN(m_FileInfo.depotFile, sizeof(m_FileInfo.depotFile), result.Text(), result.Length());
				else if (!strcmp(text, "haveRev"))
					StringCbCopyN(m_FileInfo.haveRev, sizeof(m_FileInfo.haveRev), result.Text(), result.Length());
				else if (!strcmp(text, "headAction"))
					StringCbCopyN(m_FileInfo.headAction, sizeof(m_FileInfo.headAction), result.Text(), result.Length());
				else if (!strcmp(text, "headRev"))
					StringCbCopyN(m_FileInfo.headRev, sizeof(m_FileInfo.headRev), result.Text(), result.Length());
				else if (!strcmp(text, "ourLock"))
					m_FileInfo.m_OurLock = true;

				++idx;
			}
		}
		break;
	case p4Describe:
		{
			ClearResults();

			//varList->Save(stdout);

			StrBuf field;
			field.Set("depotFile");

			int idx = 0;
			do 
			{
				varStrPtr = varList->GetVar(field, idx);
				if (varStrPtr)
				{
					m_ChangelistFiles.push_back(varStrPtr->Text());
					++idx;
				}
			}
			while(varStrPtr);
		}
		break;
	case p4Changes:
		{
			// empty the list
			m_Changelist.empty();
			
			//varList->Save(stdout);

			varStrPtr = varList->GetVar("change");
			if (varStrPtr)
			{
				AddPendingChangeLists(varStrPtr);
				m_Command = p4ChangesNext;
			}
		}
		break;
	case p4ChangesNext:
		{
			varStrPtr = varList->GetVar("change");
			if (varStrPtr)
			{
				AddPendingChangeLists(varStrPtr);
			}
		}
		break;
	case p4Submit:
		{
			ClearResults();
			m_bChangeListExists = false;
			m_ChangeListNumber[0] = '\0';
		}
		break;
	default:
		ClearResults();
	}
}

void CustomClientUser::MakeUpperCase( char* szPath )
{
	char *p;
	for(p = szPath; p<szPath + strlen(szPath); p++)
	{
		*p = (char)toupper(*p);
	}
}

const char *CustomClientUser::GetStringResult(void)
{
	if(m_StringResult[0] == '\0')
	{
		return 0;
	}
	else
	{
		return m_StringResult;
	}
}

int CustomClientUser::GetIntResult(void)
{
	return m_IntResult;
}

void CustomClientUser::ClearResults()
{
	m_StringResult[0] = '\0';
	m_IntResult = 0;
	m_ErrorMessage.clear();
	m_InfoMessage.clear();
}

void CustomClientUser::OutputBinary(const char * /*data*/, int /*length*/)
{
	m_HasErrorOccurred = false;
}

void CustomClientUser::OutputInfo(char /*level*/, const char *data)
{
	String* szMessage( data );
	System::Diagnostics::Debug::WriteLine( szMessage );
	m_InfoMessage = data;
	m_HasErrorOccurred = false;

	switch(m_Command)
	{
	case p4Change:
		SetChangeListNumber( data );
		break;
	}
}

void CustomClientUser::OutputText(const char *data, int /*length*/)
{
	String* szMessage( data );
	System::Diagnostics::Debug::Write(S"Perforce Client Msg: ");
	System::Diagnostics::Debug::WriteLine( szMessage );
	m_InfoMessage = data;
	m_HasErrorOccurred = false;
}

void CustomClientUser::SetFindResult( const char* findResult )
{
	StringCbCopyA(m_FindResult, g_MaxTagNameLength, findResult);
}

void CustomClientUser::SetChangeListNumber( const char* changeListNumber )
{
	char szUnused0[64];
	char szUnused1[64];

	sscanf_s( changeListNumber, "%s %s %s", szUnused0, sizeof(szUnused0), m_ChangeListNumber, sizeof(m_ChangeListNumber), szUnused1, sizeof(szUnused1));
	m_bChangeListExists = true;
}

void CustomClientUser::OutputError(const char *error)
{
	static const char *THE_ERROR_YO = error;

	String* szErrorMessage( error );
	System::Diagnostics::Debug::Write(S"Perforce Client Error: ");
	System::Diagnostics::Debug::WriteLine( szErrorMessage );
	m_InfoMessage = error;
}

void CustomClientUser::Prompt( const StrPtr&, StrBuf &rsp, int, Error* ) {
	rsp.Set( m_Password );
}


void CustomClientUser::InputData( StrBuf* pMyData, Error* /*pError*/ )
{
	switch(m_Command)
	{
	case p4Change:
		{
			char szClientForm[1024];
			StringCbPrintfA( szClientForm, sizeof(szClientForm), "Change: new\nClient: %s\nUser: %s\nStatus: new\nDescription: %s\n", m_ClientName, m_UserName, m_Comment );
			pMyData->Set( szClientForm );
		}
		break;
	case p4Submit:
		{
			char szClientForm[1024];
			StringCbPrintfA( szClientForm, sizeof(szClientForm), "Change: new\nClient: %s\nUser: %s\nStatus: new\nDescription: %s\n", m_ClientName, m_UserName, m_StringResult );
			pMyData->Set( szClientForm );
		}
		break;
	case p4Login:
		{
			char szClientForm[1024];
			StringCbPrintfA(szClientForm, sizeof(szClientForm), "{0}", m_Password);
			pMyData->Set( szClientForm );
		}
		break;
	}
}

void CustomClientUser::ErrorPause(char *errBuf, Error * /*e*/ )
{
	String* szErrorMessage( errBuf );
	System::Diagnostics::Debug::Write(S"Perforce Client Error: ");
	System::Diagnostics::Debug::WriteLine( szErrorMessage );
	m_ErrorMessage = errBuf;
	m_HasErrorOccurred = true;
}

void CustomClientUser::HandleError( Error *pError )
{
	StrBuf strBuffer;
	pError->Fmt( strBuffer, EF_NEWLINE );

	if( strstr( strBuffer.Text(), "up-to-date." ) )
	{
		OutputText( strBuffer.Text(), strBuffer.Length() );
		return;
	}
	OutputError( strBuffer.Text() );
	m_HasErrorOccurred = true;
}

void CustomClientUser::Message( Error *pError )
{
	if( pError->IsInfo() )
	{
		// Info
		StrBuf strBuffer;
		pError->Fmt( strBuffer, EF_PLAIN );
		OutputInfo( (char)pError->GetGeneric() + '0', strBuffer.Text() );
	}
	else
	{
		// warn, failed, fatal
		HandleError( pError );
	}
}

void CustomClientUser::SetClient( const char* clientName )
{
	StringCbCopyA(m_ClientName, g_MaxNameStringLength, clientName);
}

void CustomClientUser::SetUser( const char* userName )
{
	StringCbCopyA(m_UserName, g_MaxNameStringLength, userName);
}

void CustomClientUser::SetPassword( const char* password )
{
	StringCbCopyA(m_Password, g_MaxNameStringLength, password);
}

void CustomClientUser::Finished() 
{
	switch( m_Command )
	{
	case p4Dir:
		{
			System::Diagnostics::Debug::WriteLine( S"Finished p4Dir" );
			m_HasErrorOccurred = false;
		}
		break;
	case p4Submit:
		{
			System::Diagnostics::Debug::WriteLine( S"Finished p4Submit" );
			if (!m_HasErrorOccurred)
			{
				m_bChangeListExists = false;
				m_ChangeListNumber[0] = '\0';
			}
		}
		break;
	}
}

const char *CustomClientUser::GetPendingChangeList(const int idx)
{
	LIST_S::iterator iter = m_Changelist.begin();

	unsigned int cnt = 0;
	// return ith list entry based on idx
	for ( ; cnt < m_Changelist.size(); iter++, cnt++)
	{
		if ((unsigned int)idx == cnt)
			return (*iter).c_str();
	}

	return NULL;
}

bool CustomClientUser::HasErrorOccurred()
{
	bool bTemp = m_HasErrorOccurred; 
	m_HasErrorOccurred = false; 
	return bTemp;
}

std::string CustomClientUser::GetErrorMessage()
{
	std::string temp = m_ErrorMessage;
	//clear message
	m_ErrorMessage = "";
	return temp;
}

std::string CustomClientUser::GetInfoMessage()
{
	std::string temp = m_InfoMessage;
	//clear message
	m_InfoMessage = "";
	return temp;
}

void CustomClientUser::SetCommand(P4Command eCommand)
{
	// remove any status from the last operation
	ClearResults();

	m_Command = eCommand;
}
void CustomClientUser::BuildHashMap(ClientApi *pAPI)
{
	m_FileHash.empty();

	const int kMaxCmd = 4;
	char argv[kMaxCmd][P4FileStatInfo::kMaxString];
	char *pArgv[kMaxCmd];

	// setup the arg list
	for (int i=0; i<kMaxCmd; ++i)
	{
		pArgv[i] = &argv[i][0];
	}

	// build pending changelists
	SetCommand(p4Changes);
	StringCbPrintfA(argv[0],sizeof(argv[0]),"-spending");
	StringCbPrintfA(argv[1],sizeof(argv[1]),"-u%s", m_UserName);
	StringCbPrintfA(argv[2],sizeof(argv[2]),"-c%s", m_ClientName);
	pAPI->SetArgv(3, pArgv);
	pAPI->Run("changes", (ClientUser*)this);



	// clear out previous changelist struct
	EmptyChangelistFile();

	// build changelist association
	// this command simply gets all the files in a changelist
	const int numChangelists = GetNumPendingChangeLists();
	for (int i=0; i<numChangelists; ++i)
	{
		// get description of each change
		SetCommand(p4Describe);
		StringCbPrintfA(argv[0],sizeof(argv[0]),"%s", GetPendingChangeList(i));
		pAPI->SetArgv(1, pArgv);
		pAPI->Run("describe", (ClientUser*)this);
	}

	// grab status info for each file in a changelist
	// this command determines which change list each file is in, and the client filename
	unsigned int cnt = 0;
	LIST_S::iterator iter = m_ChangelistFiles.begin();
	for ( ; cnt < m_ChangelistFiles.size(); iter++, cnt++)
	{
		SetCommand(p4Fstat);
		StringCbPrintfA(argv[0], sizeof(argv[0]), "%s", (*iter).c_str());
		pAPI->SetArgv(1, pArgv);
		pAPI->Run("fstat", (ClientUser*)this);

		if (m_FileInfo.change[0] != '\0')
		{
			PAIR_S_S item;
			item.first = m_FileInfo.clientFile;
			item.second = m_FileInfo.change;

			PAIR_I_B success;
			success = m_FileHash.insert(item);
			if (!success.second)
			{
				m_HasErrorOccurred = true;
				m_IntResult = -1;
				StringCbPrintfA(m_StringResult, sizeof(m_StringResult), "%s", m_FileInfo.clientFile);
				return;
			}
		}
	}
}

const char* CustomClientUser::GetChangeListNumber(const char *assetPath)
{
	std::string key(assetPath);

	m_FileHashIter = m_FileHash.find(key);
	if (m_FileHashIter != m_FileHash.end())
	{
		return m_FileHashIter->second.c_str();
	}

	return NULL;
}

bool CustomClientUser::ChangeListExists(const char *assetPath)
{
	std::string key(assetPath);
	
	m_FileHashIter = m_FileHash.find(key);
	if (m_FileHashIter != m_FileHash.end())
	{
		return true;
	}

	return false;
}

void CustomClientUser::AssociateFileHash(const char *hashKey)
{
	PAIR_S_S item;
	item.first = hashKey;
	item.second = m_ChangeListNumber;

	PAIR_I_B success;
	success = m_FileHash.insert(item);

	// its possible to have a hash collision, report it here
	if (!success.second)
	{
		m_HasErrorOccurred = true;
		m_IntResult = -1;
		StringCbPrintfA(m_StringResult, sizeof(m_StringResult), "%s", hashKey);
	}
}


void CustomClientUser::UnassociateFileHash(const char *hashKey)
{
	m_FileHash.erase(hashKey);
}

void CustomClientUser::UndoCheckout(ClientApi *pAPI, const char *hashKey)
{
	const int kMaxCmd = 4;
	char argv[kMaxCmd][P4FileStatInfo::kMaxString];
	char *pArgv[kMaxCmd];

	// setup the arg list
	for (int i=0; i<kMaxCmd; ++i)
	{
		pArgv[i] = &argv[i][0];
	}

	// revert the entire changelist contents (should be just one file)
	StringCbPrintfA( argv[0], sizeof(argv[0]), "-c%s", GetChangeListNumber(hashKey) );
	StringCbPrintfA( argv[1], sizeof(argv[1]), "%s", hashKey );
	SetCommand(p4Revert);
	pAPI->SetArgv(2, pArgv);
	pAPI->Run("revert", (ClientUser*)this);

	// remove the change list
	StringCbPrintfA( argv[0], sizeof(argv[0]), "-d");
	StringCbPrintfA( argv[1], sizeof(argv[1]), "%s", GetChangeListNumber(hashKey) );
	SetCommand(p4None);	// don't care to do any internal processing
	pAPI->SetArgv(2, pArgv);
	pAPI->Run("change", (ClientUser*)this);

	// remove hash entry
	UnassociateFileHash(hashKey);
}

void CustomClientUser::SetCommentString(const char *comment)
{
	StringCbPrintfA( m_Comment, sizeof(m_Comment), "%s", comment);
}

