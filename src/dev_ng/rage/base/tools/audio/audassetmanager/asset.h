//
// tools/audassetmanager/asset.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//
#ifndef ASSET_H
#define ASSET_H

using namespace System;
using namespace System::Collections;

namespace audAssetManagement
{
	//
	// PURPOSE
	//	Encapsulates a hierarchically managed asset, providing both a hashtable and simple list of any child
	//	assets.
	// SEE ALSO
	//  audAssetManager
	//
	public __gc class audAsset : public Object
	{
public:
		//
		// PURPOSE
		//  Stores the asset information and creates the list and hashtable used to store child assets.
		// PARAMS
		//  name		- The name of the asset in the asset management system.
		//	isFolder	- Describes whether or not the asset is a folder.
		//
		audAsset(String *name, bool isFolder) : Object()
		{
			m_Name = name;
			m_IsFolder = isFolder;
			m_ChildAssetList = new ArrayList();
			m_ChildAssetHashTable = new Hashtable();
		}
		//
		// PURPOSE
		//  Determines if the asset is a folder.
		// RETURNS
		//  Returns true if the asset is a folder.
		//
		bool IsFolder(void)
		{
			return m_IsFolder;
		}

		ArrayList *m_ChildAssetList;
		Hashtable *m_ChildAssetHashTable;

private:
		bool m_IsFolder;
		String *m_Name;
	};
}

#endif // ASSET_H
