//
// tools/audassetmanager/assetmanager_perforce.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//
#include "stdafx.h"

#include "assetmanager_perforce.h"
#include <vcclr.h>

#define STRSAFE_LIB
#include <strsafe.h>

using namespace audAssetManagement;
using namespace System::Diagnostics;
using namespace System::Runtime::InteropServices;
using namespace System::Text;
using namespace System::IO;


audAssetManagerPerforce::audAssetManagerPerforce() : m_HostName(0), m_ClientName(0), m_UserName(0), m_Password(0)
{
	m_ClientUser = new CustomClientUser();
	m_Client = new ClientApi();
	m_ChangeListNumber = NULL;
	m_bMasterChangelistCreated = false;
	m_MasterChangelistNumber = NULL;
}

audAssetManagerPerforce::~audAssetManagerPerforce()
{
	delete m_ClientUser;
	delete m_Client;
}

bool audAssetManagerPerforce::Init(String *hostName, String *clientName, String *userName, String *passwordStr,	bool shouldWorkLocally)
{
	m_HostName = hostName;
	m_ClientName = clientName;
	m_UserName = userName;
	m_Password = passwordStr;
	m_ShouldWorkLocally = shouldWorkLocally;

	Error error;

	//Connect to server.
	m_Client->SetProtocol("tag", "");
	m_Client->SetProtocol("api", "58");

	IntPtr hostPtr = Marshal::StringToHGlobalAnsi(hostName);
	if(hostPtr != 0)
	{
        char *hostString = (char *)hostPtr.ToPointer();
		m_Client->SetPort(hostString); //Expected format: "myserver:myport".
		Marshal::FreeHGlobal(hostPtr);
	}

	m_Client->Init(&error);

	//m_ClientUser->Message(&error);
	if(error.Test())
	{
		StrBuf msg;
	    error.Fmt(&msg);
	    fprintf(stderr, "%s\n", msg.Text());
		return false;
	}

	m_Client->SetVersion("2005.2");


	IntPtr clientPtr = Marshal::StringToHGlobalAnsi(clientName);
	if(clientPtr != 0)
	{
        char *clientString = (char *)clientPtr.ToPointer();
		m_Client->SetClient(clientString);
		m_ClientUser->SetClient(clientString);
		Marshal::FreeHGlobal(clientPtr);
	}

	IntPtr userPtr = Marshal::StringToHGlobalAnsi(userName);
	if(userPtr != 0)
	{
        char *userString = (char *)userPtr.ToPointer();
		m_Client->SetUser(userString);
		m_ClientUser->SetUser(userString);
		Marshal::FreeHGlobal(userPtr);
	}

	if(passwordStr->Length > 0)
	{
		IntPtr passPtr = Marshal::StringToHGlobalAnsi(passwordStr);
		if (passPtr != 0)
		{
			const char *passString = (char *)passPtr.ToPointer();
			m_ClientUser->SetPassword(passString);
		}

		//clear reults and set command used to process output
		m_ClientUser->ClearResults();
		m_ClientUser->SetCommand(p4Login);
		
		m_Client->Run("login", (ClientUser *)m_ClientUser);

		if(m_ClientUser->HasErrorOccurred())
		{
			throw new Exception(m_ClientUser->GetErrorMessage().c_str());
		}
	}

	//Run a test command to check for any client connection errors.
	InvokePerforceCmd(p4Info, "info", 0, NULL, NULL, NULL, NULL);
 	m_AssetWorkingPath = m_ClientUser->GetStringResult();

	// build our hash for all files pending in changelists
	m_ClientUser->BuildHashMap(m_Client);

	const bool errorClientUser = m_ClientUser->HasErrorOccurred();
	const bool errorClient = (m_Client->Dropped()!=0);

	if (errorClient || errorClientUser)
	{
		fprintf(stderr, "%s\n", m_ClientUser->GetStringResult());
		String *szMessage(m_ClientUser->GetStringResult());
		System::Diagnostics::Debug::Write(S"Error Building Hashmap: ");
		System::Diagnostics::Debug::WriteLine(szMessage);
	}

	return (!errorClientUser && !errorClient);
}

bool audAssetManagerPerforce::Shutdown(void)
{
	Error error;

	//Close connection.
	m_Client->Final( &error );

	if(error.Test())
	{
		StrBuf msg;
	    error.Fmt(&msg);
	    fprintf(stderr, "%s\n", msg.Text());
	    return false;
	}

	return true;
}

bool audAssetManagerPerforce::Reconnect(void)
{
	Shutdown(); //Don't bother checking the return value here, as we could be reconnecting due to a lost connection.
	return Init(m_HostName, m_ClientName, m_UserName, m_Password, m_ShouldWorkLocally);
}

String *audAssetManagerPerforce::CreateImportValidPath(String *assetPath)
{
	assetPath = PathFixUp(assetPath);

	if( assetPath->ToUpper()->Contains( m_AssetWorkingPath->ToUpper() ) )
	{
		return assetPath;
	}

	if ( assetPath->ToUpper()->Contains( ":") )
	{
		return assetPath;
	}

	String* pstrFullPath( String::Concat( m_AssetWorkingPath, assetPath ) );
	String* pathString = new String( pstrFullPath->ToCharArray() );
	pathString = PathFixUp(pathString);

	// make everything uppercase, so the hash won't miss accidently
	pathString = pathString->ToUpper();

	return pathString;
}

String *audAssetManagerPerforce::CreateValidPath(String *assetPath)
{
	assetPath = PathFixUp(assetPath);

	// because perforce cannot handle these special characters,
	// replace them with ASCII escape codes.  oddly, you should not
	// do this during import/add because perforce will do the substitution
	// behind the scenes and get confused (particularly because % is also
	// a special character).

	//**Colin - We cannot substitute for '%', as this breaks when CreateValidPath
	//is called more than once for the same string - which currently happens in
	//several functions**

	//assetPath = assetPath->Replace(S"%", S"%25");		 // do % first since the others embed pct

	assetPath = assetPath->Replace(S"@", S"%40");
	assetPath = assetPath->Replace(S"#", S"%23");
	assetPath = assetPath->Replace(S"*", S"%2A");

	// make everything uppercase, so the hash won't miss accidently
	assetPath = assetPath->ToUpper();

	if( assetPath->Contains( m_AssetWorkingPath->ToUpper() ) )
	{
		return assetPath;
	}

	if ( assetPath->Contains( S":") )
	{
		return assetPath;
	}

	String* pstrFullPath = GetWorkingPath( assetPath );
	String* pathString = new String( pstrFullPath->ToCharArray() );
	pathString = PathFixUp(pathString);

	return pathString;
}

String *audAssetManagerPerforce::GetWorkingPath(String *assetPath)
{
	assetPath = PathFixUp(assetPath);

	// make everything uppercase, so the hash won't miss accidently
	assetPath = assetPath->ToUpper();

	if( assetPath->Contains( m_AssetWorkingPath->ToUpper() ) )
	{
		return assetPath;
	}

	if ( assetPath->Contains( S":") )
	{
		return assetPath;
	}

	String* pstrFullPath( String::Concat( m_AssetWorkingPath->ToUpper(), assetPath ) );
	String* pathString = new String( pstrFullPath->ToCharArray() );
	pathString = PathFixUp(pathString);

	return pathString;
}

bool audAssetManagerPerforce::SetWorkingPath(String * /*assetPath*/, String * /*localWorkingPath*/)
{
	//NOTE: This functionality is not supported for Perforce.
	return false;
}

bool audAssetManagerPerforce::IsUpToDate(String *assetPath)
{
	String *validAssetPath = CreateValidPath(assetPath);

	bool returnVal = false;
	IntPtr assetPathPtr = Marshal::StringToHGlobalAnsi(validAssetPath);
	if(assetPathPtr != 0)
	{
		char *assetPathString;
		assetPathString = (char *)assetPathPtr.ToPointer();

		m_ClientUser->SetCommand( p4Fstat );
		m_Client->SetArgv(1, (char *const *)&assetPathString);
		m_Client->Run("fstat", (ClientUser *)m_ClientUser);

		returnVal = m_ClientUser->GetFileInfo().IsFileUpToDate();
	}
	Marshal::FreeHGlobal(assetPathPtr);

	return returnVal;
}

String* audAssetManagerPerforce::PathFixUp( String* pPath )
{
	// This function will look at a path and make it perforce friendly.
	pPath = pPath->Replace( "\\", "/" );	// this will change \\/ -> //
	pPath = pPath->Replace( "//", "/" );	// this will change // -> / and //// -> //
	pPath = pPath->Replace( "//", "/" );	// if there are // left they will go to /
	pPath = pPath->ToUpper();

	return pPath;
}

bool audAssetManagerPerforce::GetLatest(String *assetPath)
{
	return GetLatest(assetPath, true);
}

bool audAssetManagerPerforce::GetLatest(String *assetPath, bool /*shouldDefaultResponses*/)
{
	bool returnVal = false;

	bool bIsADirectory = false;

	String *validAssetPath = CreateValidPath(assetPath);

	bIsADirectory = ExistsAsDir(validAssetPath);

	if( bIsADirectory )
	{
		if(!validAssetPath->EndsWith(S"/"))
		{
			validAssetPath = String::Concat( validAssetPath, S"/" );
		}

		validAssetPath = String::Concat( validAssetPath, S"..." );
	}

	IntPtr assetPathPtr = Marshal::StringToHGlobalAnsi(validAssetPath);
	if(assetPathPtr != 0)
	{
		char *assetPathString;
		assetPathString = (char *)assetPathPtr.ToPointer();

		m_ClientUser->SetCommand( p4Sync );
		m_Client->SetArgv(1, (char *const *)&assetPathString);
		m_Client->Run("sync", (ClientUser *)m_ClientUser);

		returnVal = !m_ClientUser->HasErrorOccurred();
	}
	Marshal::FreeHGlobal(assetPathPtr);

	return returnVal;
}


bool audAssetManagerPerforce::ExistsAsAsset(String *assetPath)
{
	bool exists = false;

	String *validAssetPath = CreateValidPath(assetPath);

	// - the assumption is that directories are checked
	//   using ExistsAsDir not ExistsAsAsset.
	// - don't consider whether a file is also a directory

// 	exists = ExistsAsDir(validAssetPath);
// 
// 	if( exists )
// 		return exists;

	if(m_ShouldWorkLocally)
	{
		//Check if local asset exists.
		String *localPath = GetWorkingPath(validAssetPath);
		if(localPath)
		{
			IO::FileInfo *localFileInfo = new IO::FileInfo(localPath);
			if(localFileInfo->Exists)
			{
				exists = true;
			}
			else
			{
				//This could be a directory rather than a file.
				IO::DirectoryInfo *localDirectoryInfo = new IO::DirectoryInfo(localPath);
				exists = localDirectoryInfo->Exists;
			}
		}
	}
	else
	{
		IntPtr assetPathPtr = Marshal::StringToHGlobalAnsi(validAssetPath);
		if(assetPathPtr != 0)
		{
			char *assetPathString = (char *)assetPathPtr.ToPointer();

			m_ClientUser->SetCommand( p4Fstat );
			m_Client->SetArgv(1, &assetPathString);
			m_Client->Run("fstat", (ClientUser *)m_ClientUser);

			if(!m_ClientUser->HasErrorOccurred())
			{
				exists = m_ClientUser->GetFileInfo().IsAsset();
			}
		}
		Marshal::FreeHGlobal(assetPathPtr);
	}

	return exists;
}

bool audAssetManagerPerforce::ExistsAsDir(String *assetPath)
{
	bool exists = false;

	String *tempValidAssetPath = CreateValidPath(assetPath);
	String *localValidAssetPath = tempValidAssetPath;

	// we most likely have a directory so we must ignore the last / 
	if(tempValidAssetPath->EndsWith(S"/"))
	{
		int splitIndex = tempValidAssetPath->LastIndexOf('/');
		tempValidAssetPath = tempValidAssetPath->Substring(0, splitIndex);
	}

	// perforce doesn't track directories, just files inside the folders

	//Check if local asset exists.
	String *localPath = GetWorkingPath(localValidAssetPath);
	if(localPath)
	{
		//This could be a directory rather than a file.
		IO::DirectoryInfo *localDirectoryInfo = new IO::DirectoryInfo(localPath);
		exists = localDirectoryInfo->Exists;
	}

	if(!exists)
	{
		// This is a bit of a perforce hack. Perforce won't tell you if a directory exists,
		// but it will give you a list of all of the things inside a directory, including
		// other directories. So we are going to check if our directory exists within it's 
		// parent directory. To do this we remove everything after the last /, ie. go up one folder, 
		// or remove the filename, which is ok because the path we are 
		// looking for will not appear inside itself. 
		int splitIndex = tempValidAssetPath->LastIndexOf('/');
		String *validAssetPath = String::Concat( tempValidAssetPath->Substring(0, splitIndex), S"/*" );

		IntPtr assetPathPtr = Marshal::StringToHGlobalAnsi(validAssetPath);
		if(assetPathPtr != 0)
		{
			char *assetPathString = (char *)assetPathPtr.ToPointer();

			String *tempFindAssetPath = PathFixUp(tempValidAssetPath);
			tempFindAssetPath = tempFindAssetPath->ToUpper();

			IntPtr findAssetPathPtr = Marshal::StringToHGlobalAnsi(tempFindAssetPath);
			if( findAssetPathPtr != 0 )
			{
				char *pszFindAssetPathString = (char *)findAssetPathPtr.ToPointer();
				m_ClientUser->SetFindResult( pszFindAssetPathString );
				Marshal::FreeHGlobal(findAssetPathPtr);
			}
			else
			{
				return false;
			}

			InvokePerforceCmd(p4Dir, "dirs", 1, assetPathString, NULL, NULL, NULL);

			if(!m_ClientUser->HasErrorOccurred())
			{
				if( m_ClientUser->GetIntResult() )
					exists = true;
			}
		}
		Marshal::FreeHGlobal(assetPathPtr);
	}

	return exists;
}

bool audAssetManagerPerforce::IsLocked(String *assetPath)
{
	return (GetLockedOwner(assetPath)->get_Length() > 0);
}

bool audAssetManagerPerforce::IsLockedByMe(String *assetPath)
{
	bool isLocked=false;

	String *validAssetPath = CreateValidPath(assetPath);

	IntPtr assetPathPtr = Marshal::StringToHGlobalAnsi(validAssetPath);
	if(assetPathPtr != 0)
	{
		char *assetPathString = (char *)assetPathPtr.ToPointer();

		bool isOpen = false;

		m_ClientUser->SetCommand( p4Fstat );
		m_Client->SetArgv(1, &assetPathString);
		m_Client->Run("fstat", (ClientUser *)m_ClientUser);

		if(!m_ClientUser->HasErrorOccurred())
		{
			isOpen = m_ClientUser->GetFileInfo().IsOpen();
			if( isOpen && m_ClientUser->GetFileInfo().m_OurLock )
				isLocked = true;
		}
	}
	Marshal::FreeHGlobal(assetPathPtr);

	return isLocked;
}

String *audAssetManagerPerforce::GetLockedOwner(String *assetPath)
{
	String *owner=S"";

	String *validAssetPath = CreateValidPath(assetPath);

	IntPtr assetPathPtr = Marshal::StringToHGlobalAnsi(validAssetPath);
	if(assetPathPtr != 0)
	{
		char *assetPathString = (char *)assetPathPtr.ToPointer();

		m_ClientUser->SetCommand( p4IsLocked );
		m_Client->SetArgv(1, &assetPathString);
		m_Client->Run("fstat", (ClientUser *)m_ClientUser);

		if(!m_ClientUser->HasErrorOccurred())
		{
			owner = new String(m_ClientUser->GetStringResult());
		}
	}
	Marshal::FreeHGlobal(assetPathPtr);

	return owner;
}

bool audAssetManagerPerforce::IsCheckedOut(String *assetPath)
{
	bool isCheckedOut=false;

	String *validAssetPath = CreateValidPath(assetPath);

	IntPtr assetPathPtr = Marshal::StringToHGlobalAnsi(validAssetPath);
	if(assetPathPtr != 0)
	{
		char *assetPathString = (char *)assetPathPtr.ToPointer();

		m_ClientUser->SetCommand( p4Fstat );
		m_Client->SetArgv(1, &assetPathString);
		m_Client->Run("fstat", (ClientUser *)m_ClientUser);

		if(!m_ClientUser->HasErrorOccurred())
		{
			String *action = new String(m_ClientUser->GetFileInfo().action);
			if(action->ToUpper()->Equals(S"EDIT") || action->ToUpper()->Equals(S"ADD"))
			{
				//The asset is being editted, so check if the current user is the owner.
				m_ClientUser->SetCommand( p4ActionOwner );
				m_Client->SetArgv(1, &assetPathString);
				m_Client->Run("fstat", (ClientUser *)m_ClientUser);

				if(!m_ClientUser->HasErrorOccurred())
				{
					String *owner = new String(m_ClientUser->GetStringResult());
					owner = owner->ToUpper();
					isCheckedOut = (owner && owner->Equals(m_UserName->ToUpper()));
				}
			}
		}
	}
	Marshal::FreeHGlobal(assetPathPtr);

	return isCheckedOut;
}

bool audAssetManagerPerforce::LocalCheckOut(String *assetPath, String *comment)
{
	bool returnVal = false;

	String *validAssetPath = CreateValidPath(assetPath);

	if(m_ShouldWorkLocally)
	{
		//Simply get the latest version of the asset(s), without overwriting writeable local assets and make writeable.
		GetLatest(validAssetPath);
		MakeLocalAssetWriteable(GetWorkingPath(validAssetPath));
		returnVal = true;
	}
	else
	{
		// before we can check out a file, because we need to have exclusivity, we first 
		// need to make sure that no one else has the file opened for edit

		IntPtr assetPathPtr = Marshal::StringToHGlobalAnsi(validAssetPath);
		if(assetPathPtr != 0)
		{
			SetCommentString(comment);

			char *assetPathString = (char *)assetPathPtr.ToPointer();
			if(assetPathString!= 0)
			{
				// grab latest version of the file, prior to locking
				GetLatest(validAssetPath);

				char argumentString[256];
				if (m_bMasterChangelistCreated)
				{
					StringCbPrintfA( argumentString, sizeof(argumentString), "-c%s", m_MasterChangelistNumber );
				}
				else
				{
					CreateNewChangelist(assetPathString);
					StringCbPrintfA( argumentString, sizeof(argumentString), "-c%s", m_ClientUser->GetChangeListNumber(assetPathString) );
				}

				InvokePerforceCmd(p4Edit, "edit", 2, argumentString, assetPathString, NULL, NULL);

				if(!m_ClientUser->HasErrorOccurred() && IsCheckedOut(assetPath))
				{
					returnVal=true;					
				}

				//DO NOT LOCK

				if(!returnVal && !m_bMasterChangelistCreated)
				{
					//We failed to check-out this asset, so revert the new (individual) changelist.
					char argStr[64];
					StringCbPrintfA(argStr, sizeof(argStr), "%s", m_ClientUser->GetChangeListNumber(assetPathString));
					InvokePerforceCmd(p4None, "change", 2, "-d", argStr, NULL, NULL);
				}
			}
		}
		Marshal::FreeHGlobal(assetPathPtr);
	}

	return returnVal;
}

bool audAssetManagerPerforce::CheckOut(String *assetPath, String *comment)
{
	bool returnVal = false;

	String *validAssetPath = CreateValidPath(assetPath);

	if(m_ShouldWorkLocally)
	{
		//Simply get the latest version of the asset(s), without overwriting writeable local assets and make writeable.
		GetLatest(validAssetPath);
		MakeLocalAssetWriteable(GetWorkingPath(validAssetPath));
		returnVal = true;
	}
	else
	{
		// before we can check out a file, because we need to have exclusivity, we first 
		// need to make sure that no one else has the file opened for edit

		IntPtr assetPathPtr = Marshal::StringToHGlobalAnsi(validAssetPath);
		if(assetPathPtr != 0)
		{
			SetCommentString(comment);

			char *assetPathString = (char *)assetPathPtr.ToPointer();
			if(assetPathString!= 0)
			{
				// grab latest version of the file, prior to locking
				GetLatest(validAssetPath);

				char argumentString[256];
				if (m_bMasterChangelistCreated)
				{
					StringCbPrintfA( argumentString, sizeof(argumentString), "-c%s", m_MasterChangelistNumber );
				}
				else
				{
					CreateNewChangelist(assetPathString);
					StringCbPrintfA( argumentString, sizeof(argumentString), "-c%s", m_ClientUser->GetChangeListNumber(assetPathString) );
				}

				InvokePerforceCmd(p4Edit, "edit", 2, argumentString, assetPathString, NULL, NULL);

				if(!m_ClientUser->HasErrorOccurred() && IsCheckedOut(assetPath))
				{

					InvokePerforceCmd(p4Lock, "lock", 1, assetPathString, NULL, NULL, NULL);

					if(!m_ClientUser->HasErrorOccurred())
					{
						returnVal = IsLockedByMe(assetPathString);

						// this code may have been necessary because we weren't consistently making the assetPath uppercase.  so, as a 
						// something of a 'catch-all' -- we associateFileHash at this point.  in truth, the createNewChangelist call
						// above would build the hash association for us -- making this code trigger a false error condition because
						// the hash entry already exists.
						//						if (returnVal)
						//						{
						//							// no need to add this to the hash
						//							if (!m_bMasterChangelistCreated)
						//							{
						//								m_ClientUser->AssociateFileHash(assetPathString);
						//							}
						//
						//							if (m_ClientUser->HasErrorOccurred())
						//							{
						//								String *szMessage(assetPathString);
						//								System::Diagnostics::Debug::Write(S"CheckOut: error associating file with hash: ");
						//								System::Diagnostics::Debug::WriteLine(szMessage);
						//							}
						//						}

						if(!returnVal)
						{
							//We could not lock the asset, so revert our check-out.
							m_ClientUser->UndoCheckout(m_Client, assetPathString);
						}
					}

				}

				if(!returnVal && !m_bMasterChangelistCreated)
				{
					//We failed to check-out this asset, so revert the new (individual) changelist.
					char argStr[64];
					StringCbPrintfA(argStr, sizeof(argStr), "%s", m_ClientUser->GetChangeListNumber(assetPathString));
					InvokePerforceCmd(p4None, "change", 2, "-d", argStr, NULL, NULL);
				}
			}
		}
		Marshal::FreeHGlobal(assetPathPtr);
	}

	return returnVal;
}

bool audAssetManagerPerforce::SimpleCheckOut(String *assetPath, String *comment)
{
	// in the case of .lock files from RAVE, perforce will allow you to checkout/lock
	// a file that doesn't physically exist on the file system.  so, during SimpleCheckOut
	// we need to check if it exists as an asset prior to checkout.  previously, the code
	// assumed that the return val was true and (ExistsAsAsset && !IsCheckedOut) also
	// where true prior to calling CheckOut.  so, we could not tell whether SimpleCheckOut
	// failed because the asset didn't physically exist.

	if (ExistsAsAsset(assetPath))
	{
		if (!IsCheckedOut(assetPath))
		{
			return CheckOut(assetPath, comment);
		}
		else
		{
			return true;
		}
	}
	else
	{
		return false;
	}
}

bool audAssetManagerPerforce::UndoCheckOut(String *assetPath)
{
	bool returnVal = true;

	String *validAssetPath = CreateValidPath(assetPath);

	if(m_ShouldWorkLocally)
	{
		//Simply make the asset(s) read-only.
		MakeLocalAssetReadOnly(GetWorkingPath(validAssetPath));
	}
	else if(IsCheckedOut(validAssetPath))
	{
		IntPtr assetPathPtr = Marshal::StringToHGlobalAnsi(validAssetPath);
		if(assetPathPtr != 0)
		{
			char *assetPathString = (char *)assetPathPtr.ToPointer();

			m_ClientUser->UndoCheckout(m_Client, assetPathString);

			returnVal = !m_ClientUser->HasErrorOccurred();
		}
		Marshal::FreeHGlobal(assetPathPtr);
	}

	return returnVal;
}

bool audAssetManagerPerforce::SimpleUndoCheckOut(String *assetPath)
{
	bool returnVal = true;

	if(ExistsAsAsset(assetPath))
	{
		returnVal = UndoCheckOut(assetPath);
	}

	return returnVal;
}

bool audAssetManagerPerforce::Reopen(const char* changeList, char *assetPath)
{
	InvokePerforceCmd(P4Reopen, "reopen", 3, "-c", changeList, assetPath, NULL);
	return !m_ClientUser->HasErrorOccurred();	
}


bool audAssetManagerPerforce::CheckIn(String *assetPath, String *comment )
{
	bool returnVal = true;

	String *validAssetPath = CreateValidPath(assetPath);

	if(m_ShouldWorkLocally)
	{
		//Simply make the asset(s) read-only.
		MakeLocalAssetReadOnly(GetWorkingPath(assetPath));
	}
	else
	{
		if (!m_bMasterChangelistCreated)
		{
			//get asset path as char*
			IntPtr assetPathPtr = Marshal::StringToHGlobalAnsi(validAssetPath);

			if(assetPathPtr != 0)
			{
				char* assetPathString = (char *)assetPathPtr.ToPointer();
				if(comment)
				{
					//set comment string
					SetCommentString(comment);
					//store original changelist no
					const char *originalChangeListNumber = m_ClientUser->GetChangeListNumber(assetPathString);
					if (!originalChangeListNumber)
					{
						return false;
					}

					//create new changeList
					char argumentString[256];
					char* pArgumentString = &argumentString[0];

					// build changelist
					m_ClientUser->SetCommand( p4Change );
					StringCbPrintfA( argumentString, sizeof(argumentString), "-i" );
					m_Client->SetArgv(1, &pArgumentString);
					m_Client->Run("change", (ClientUser *)m_ClientUser);
					//ensure an error hasn't occurred
					if(m_ClientUser->HasErrorOccurred())
					{
						return false;
					}

					// save off our change number
					const char* newChangelistNumber = m_ClientUser->GetChangeListNumber();

					//move file into new changeList
					Reopen(newChangelistNumber, assetPathString);				
					if(m_ClientUser->HasErrorOccurred())
					{
						//if file has not been moved delete changelist
						InvokePerforceCmd(p4None, "change", 2, "-d", newChangelistNumber, NULL, NULL);
						return false;
					}

					//if move was successful submit and clean up
					InvokePerforceCmd(p4Submit, "submit", 2, "-c", newChangelistNumber, NULL, NULL);
					if(m_ClientUser->HasErrorOccurred())
					{
						return false;
					}

					//delete old changelist
					InvokePerforceCmd(p4None, "change", 2, "-d", originalChangeListNumber, NULL, NULL);
					if(m_ClientUser->HasErrorOccurred())
					{
						return false;
					}

					//remove from hash table
					m_ClientUser->UnassociateFileHash(assetPathString);

				}
				else
				{

					const char *changeListNumer = m_ClientUser->GetChangeListNumber(assetPathString);
					if (changeListNumer)
					{
						InvokePerforceCmd(p4Submit, "submit", 2, "-c", m_ClientUser->GetChangeListNumber(assetPathString), NULL, NULL);
					}

					m_ClientUser->UnassociateFileHash(assetPathString);

				}
			}

			Marshal::FreeHGlobal(assetPathPtr);

			/// Double check that the asset has been checked in rather than relying on parsing the message
			returnVal = !IsCheckedOut(assetPath);

			//if is check out check if unlocked
			if(returnVal)
			{
				returnVal = !IsLocked(assetPath);
			}
		}
	}

	return returnVal;
}

void audAssetManagerPerforce::CreateNewChangelist(const char *assetPath)
{
	// See if a change list exists
	if( !m_ClientUser->ChangeListExists(assetPath) )
	{
		char argumentString[256];
		char* pArgumentString = &argumentString[0];

		m_ClientUser->SetCommand( p4Change );
		StringCbPrintfA( argumentString, sizeof(argumentString), "-i" );
		m_Client->SetArgv(1, &pArgumentString);
		m_Client->Run("change", (ClientUser *)m_ClientUser);

		m_ClientUser->AssociateFileHash(assetPath);
		if (m_ClientUser->HasErrorOccurred())
		{
			String *szMessage(assetPath);
			System::Diagnostics::Debug::Write(S"CheckOut: error associating file with hash: ");
			System::Diagnostics::Debug::WriteLine(szMessage);
		}
	}
}

bool audAssetManagerPerforce::Import(String * /*localPath*/, String *assetPath, String *comment)
{
	//NOTE: Needs CheckIn to function correctly (without a form!)
	bool returnVal = true;

	String *validAssetPath = CreateImportValidPath(assetPath);


	if(m_ShouldWorkLocally)
	{
		//Simply make the asset(s) read-only.
		MakeLocalAssetReadOnly(GetWorkingPath(validAssetPath));
		return true;
	}
	else
	{
		// we don't import folders
		if (ExistsAsDir(validAssetPath))
		{
			return true;
		}

		IntPtr assetPathPtr = Marshal::StringToHGlobalAnsi(validAssetPath);

		if(assetPathPtr != 0)
		{
			char argumentString[64];
			char* assetPathString = (char *)assetPathPtr.ToPointer();

			SetCommentString(comment);

			if (m_bMasterChangelistCreated)
			{
				// if this asset was already checked out, unassociate it with our internal database
				if (m_ClientUser->GetChangeListNumber(assetPathString))
				{
					m_ClientUser->UnassociateFileHash(assetPathString);
				}

				StringCbPrintfA( argumentString, sizeof(argumentString), "-c%s", m_MasterChangelistNumber );
				InvokePerforceCmd(p4Add, "add", 3, argumentString, "-f", assetPathString, NULL);
			}
			else
			{
				CreateNewChangelist(assetPathString);

				// add this file to the depot
				StringCbPrintfA( argumentString, sizeof(argumentString), "-c%s", m_ClientUser->GetChangeListNumber(assetPathString) );
				InvokePerforceCmd(p4Add, "add", 3, argumentString, "-f", assetPathString, NULL);

				// submit the add immediately
				InvokePerforceCmd(p4Submit, "submit", 1, argumentString, NULL, NULL, NULL);

				// remove association
				m_ClientUser->UnassociateFileHash(assetPathString);
			}

			returnVal = !m_ClientUser->HasErrorOccurred();
		}
		Marshal::FreeHGlobal(assetPathPtr);
	}

	return returnVal;
}

bool audAssetManagerPerforce::CheckInOrImport(String *localPath, String *assetPath, String *comment)
{
	bool returnVal;

	if(ExistsAsAsset(assetPath))
	{
		returnVal = CheckIn(assetPath, comment);
	}
	else
	{
		returnVal = Import(localPath, assetPath, comment);
	}

	return returnVal;
}

bool audAssetManagerPerforce::DeleteAsset(String *localPath, String *assetPath, bool deleteLocal)
{
	String *validAssetPath = CreateValidPath(assetPath);
	String *validPath = CreateValidPath(localPath);
	// Perforce does not require the deletion of empty folders, check rmdir in the client spec to have it done automatically.
	bool exists = false;
	exists = ExistsAsDir(validAssetPath);
	if( exists )
	{
		return exists;
	}

	// if the asset is open for add, or edit, you can not delete, but must revert first.
	if( IsCheckedOut( assetPath ) )
	{
		UndoCheckOut( assetPath );
	}

	if(m_ShouldWorkLocally || !ExistsAsAsset(assetPath))
	{
		if(deleteLocal)
		{
			FileInfo *fileInfo = new FileInfo(validPath);
			if(fileInfo->Exists){
				int attributes;
				//ensure file is not read-only before deleting
				if((fileInfo->Attributes & FileAttributes::ReadOnly) != 0)
				{
					attributes = static_cast<int>(fileInfo->Attributes);
					attributes -= static_cast<int>(FileAttributes::ReadOnly);
					fileInfo->Attributes = static_cast<FileAttributes>(attributes);
				}
				fileInfo->Delete();
			}
		}

		return true;
	}

	bool returnVal = true;

	IntPtr assetPathPtr = Marshal::StringToHGlobalAnsi(validAssetPath);
	if(assetPathPtr != 0)
	{
		char argumentString[256];
		char *assetPathString = (char *)assetPathPtr.ToPointer();

		// grab latest version of the file prior to deleting, as Perforce requires this.
		GetLatest(validAssetPath);

		m_ClientUser->SetCommentString("delete asset");

		if (m_bMasterChangelistCreated)
		{
			StringCbPrintfA( argumentString, sizeof(argumentString), "-c%s", m_MasterChangelistNumber );
			InvokePerforceCmd(p4Delete, "delete", 2, argumentString, assetPathString, NULL, NULL);
		}
		else
		{
			CreateNewChangelist(assetPathString);
			StringCbPrintfA( argumentString, sizeof(argumentString), "-c%s", m_ClientUser->GetChangeListNumber(assetPathString) );
			InvokePerforceCmd(p4Delete, "delete", 2, argumentString, assetPathString, NULL, NULL);

			// submit this change immediately
			InvokePerforceCmd(p4Submit, "submit", 2, "-c", m_ClientUser->GetChangeListNumber(assetPathString), NULL, NULL);
		}

		// unassociate
		m_ClientUser->UnassociateFileHash(assetPathString);

		returnVal = !m_ClientUser->HasErrorOccurred();
	}
	Marshal::FreeHGlobal(assetPathPtr);

	return returnVal;
}

bool audAssetManagerPerforce::RenameAsset(String *assetPath, String *newName)
{
	String *validAssetPath = CreateValidPath(assetPath);
// 	String *newAssetPath = CreateValidPath(newName);

	String *localPath = GetWorkingPath(assetPath);

	//Generate new local path.
	int splitIndex = localPath->LastIndexOf('\\');
	if (splitIndex < 0)
		splitIndex = localPath->LastIndexOf('/');

	String *newPath = localPath->Substring(0, splitIndex);
	newPath = String::Concat(newPath, S"/", newName);

	if(m_ShouldWorkLocally)
	{
		if(localPath)
		{
			MakeLocalAssetWriteable(localPath);

			IO::FileInfo *localFileInfo = new IO::FileInfo(localPath);
			if(localFileInfo->Exists)
			{
				localFileInfo->MoveTo(newPath);
				MakeLocalAssetReadOnly(newPath);
				return true;
			}
			else
			{
				//This could be a directory rather than a file.
				IO::DirectoryInfo *localDirectoryInfo = new IO::DirectoryInfo(localPath);
				if(localDirectoryInfo->Exists)
				{
					localDirectoryInfo->MoveTo(newPath);
					MakeLocalAssetReadOnly(newPath);
					return true;
				}
			}
		}

		return false;
	}
	else
	{
		char *assetPathString = NULL;
		IntPtr assetPathPtr = Marshal::StringToHGlobalAnsi(validAssetPath);
		if(assetPathPtr != 0)
		{
			 assetPathString = (char *)assetPathPtr.ToPointer();
		}

		m_ClientUser->SetCommentString("rename asset");
		CreateNewChangelist(assetPathString);

		// rename an asset in perforce takes several steps:
		//  - p4 integrate -c# -d fromFile toFile
		//  - p4 delete -c# fromFile
		//  - p4 submit -c#

		char argv[4][1024];
		char *pArgv[4];
		pArgv[0] = &argv[0][0];
		pArgv[1] = &argv[1][0];
		pArgv[2] = &argv[2][0];
		pArgv[3] = &argv[3][0];

		// integrate step
		StringCbPrintfA(argv[0], sizeof(argv[0]), "-c%s", m_ClientUser->GetChangeListNumber(assetPathString));
		StringCbPrintfA(argv[1], sizeof(argv[1]), "-d");
		StringCbPrintfA(argv[2], sizeof(argv[2]), "%s", validAssetPath);
		StringCbPrintfA(argv[3], sizeof(argv[3]), "%s", newPath);
		m_Client->SetArgv(4, pArgv);
		m_Client->Run("integrate", (ClientUser*)m_ClientUser);

		// delete step
		StringCbPrintfA(argv[1], sizeof(argv[1]), "%s", validAssetPath);
		m_Client->SetArgv(2, pArgv);
		m_Client->Run("delete", (ClientUser*)m_ClientUser);

		// submit step
		m_Client->SetArgv(1, pArgv);
		m_Client->Run("submit", (ClientUser*)m_ClientUser);
	}

	return true;
}

bool audAssetManagerPerforce::CreateFolder(String *folderPath, String * /*comment*/)
{
	// perforce does not track empty directories in asset management, just files.
	// when a file is imported, it's associated directory is implicitly added as well.

	String *validAssetPath = CreateValidPath(folderPath);
	String *localPath = GetWorkingPath(validAssetPath);
	if(localPath)
	{
		IO::DirectoryInfo* localDirectoryInfo = IO::Directory::CreateDirectory(localPath);
		if (localDirectoryInfo->Exists)
		{
			return true;
		}
	}

	return false;
}


bool audAssetManagerPerforce::CreateCurrentChangelist(String *comment)
{
	bool retval = false;

	// See if a change list exists
	if (!m_bMasterChangelistCreated)
	{
		// set our comment
		SetCommentString(comment);

		char argumentString[256];
		char* pArgumentString = &argumentString[0];

		// build changelist
		m_ClientUser->SetCommand( p4Change );
		StringCbPrintfA( argumentString, sizeof(argumentString), "-i" );
		m_Client->SetArgv(1, &pArgumentString);
		m_Client->Run("change", (ClientUser *)m_ClientUser);

		// save off our change number
		m_MasterChangelistNumber = m_ClientUser->GetChangeListNumber();
		m_bMasterChangelistCreated = true;

		retval = !m_ClientUser->HasErrorOccurred();
	}

	return retval;
}

bool audAssetManagerPerforce::CheckInCurrentChangelist()
{
	bool retval = false;

	if (m_bMasterChangelistCreated)
	{
		// submit
		char argStr[64];
		StringCbPrintfA( argStr, sizeof(argStr), "-c%s", m_MasterChangelistNumber );
		InvokePerforceCmd(p4Submit, "submit", 1, argStr, NULL, NULL, NULL);

		retval = !m_ClientUser->HasErrorOccurred();
		// reset state if not failed. If failed we need this info to revert
		if(retval){
			m_bMasterChangelistCreated = false;
			m_MasterChangelistNumber = NULL;
		}
	}

	return retval;
}

bool audAssetManagerPerforce::DeleteCurrentChangelist()
{
	bool retval = true;

	if (m_bMasterChangelistCreated)
	{
		// revert files in the changelist
		char argStr[64];
		char hostStr[64];
		StringCbPrintfA(argStr, sizeof(argStr), "-c%s", m_MasterChangelistNumber );
		StringCbPrintfA(hostStr, sizeof(hostStr), "//%s/...", m_ClientName);
		InvokePerforceCmd(p4Revert, "revert", 2, argStr, hostStr, NULL, NULL);

		StringCbPrintfA(argStr, sizeof(argStr), "%s", m_MasterChangelistNumber );
		InvokePerforceCmd(p4None, "change", 2, "-d", argStr, NULL, NULL);

		retval = !m_ClientUser->HasErrorOccurred();

		m_bMasterChangelistCreated = false;
		m_MasterChangelistNumber = NULL;
	}

	return retval;
}

void audAssetManagerPerforce::SetCommentString(String *comment)
{
	IntPtr commentStrPtr = Marshal::StringToHGlobalAnsi(comment);
	if (commentStrPtr != 0)
	{
		char *commentPtr = (char*)commentStrPtr.ToPointer();
		m_ClientUser->SetCommentString(commentPtr);
	}
	Marshal::FreeHGlobal(commentStrPtr);
}


void audAssetManagerPerforce::InvokePerforceCmd(const int cmd, const char *p4cmd, const int cnt, const char *cmd1, const char* cmd2, const char *cmd3, const char *cmd4)
{
	const int kMaxCmd = 4;
	char argv[kMaxCmd][1024];
	char *pArgv[kMaxCmd];

	// setup the arg list
	for (int i=0; i<kMaxCmd; ++i)
	{
		pArgv[i] = &argv[i][0];

		if (i<cnt)
		{
			switch(i)
			{
				case 0:	strncpy_s(argv[0], sizeof(argv[0]), cmd1, _TRUNCATE); break;
				case 1:	strncpy_s(argv[1], sizeof(argv[1]), cmd2, _TRUNCATE); break;
				case 2:	strncpy_s(argv[2], sizeof(argv[2]), cmd3, _TRUNCATE); break;
				case 3:	strncpy_s(argv[3], sizeof(argv[3]), cmd4, _TRUNCATE); break;
				default: break;
			}
		}
	}

	// invoke the command
	m_ClientUser->SetCommand((P4Command)cmd);
	m_Client->SetArgv(cnt, pArgv);
	m_Client->Run(p4cmd, (ClientUser*)m_ClientUser);
}
