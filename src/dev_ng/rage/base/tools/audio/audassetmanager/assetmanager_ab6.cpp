//
// tools/audassetmanager/assetmanager_ab6.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//
#include "stdafx.h"
#include "assetmanager_ab6.h"

#pragma warning( push )
#pragma warning( disable : 4564 )	// disable default param warnings with NxN usage

using namespace audAssetManagement;
using namespace System::Diagnostics;

audAssetManagerAb6::audAssetManagerAb6() : m_ServerName(0), m_ProjectName(0), m_UserName(0), m_PasswordStr(0), m_ShouldWorkLocally(false)
{
	m_NxnNamespace = new NxNNamespaceHelperClass();
	m_NxnParam = new NxNXMLParameterClass();
}

bool audAssetManagerAb6::Init(String *serverName, String *projectName, String *userName, String *passwordStr,
	bool shouldWorkLocally)
{
	m_ServerName = serverName;
	m_ProjectName = projectName;
	m_UserName = userName;
	m_PasswordStr = passwordStr;
	m_ShouldWorkLocally = shouldWorkLocally;

	m_NxnParam->Reset();
	String *command = S"ProjectLoadEx";
	m_NxnParam->Command = command;
	m_NxnParam->set_ParamIn(S"Hostname", serverName); 
	m_NxnParam->set_ParamIn(S"Name", projectName);
	m_NxnParam->set_ParamIn(S"Username", userName); 
	m_NxnParam->set_ParamIn(S"Password", passwordStr); 
	m_NxnParam->set_ParamIn(S"ShowMainDialog", S"0");
	m_NxnParam->set_ParamIn(S"ShowDialog", S"0");
	m_NxnParam->set_ParamIn(S"TimeSyncPolicy", S"Always");
	m_NxnParam->SetProgressDialog(S"", (NxNProgressDialogType)7);
	m_NxnParam->VerboseLevel = 4;

	m_NxnParam->XML = m_NxnNamespace->RunCommand(S"\\Workspace\\", command, m_NxnParam->XML, 0);
	return (m_NxnParam->WasSuccessful == 1);
}

bool audAssetManagerAb6::Shutdown(void)
{
	m_NxnParam->Reset();
	String *command = S"ProjectUnloadEx";
	m_NxnParam->Command = command;
	m_NxnParam->set_ParamIn(S"Name", m_ProjectName);
	m_NxnParam->set_ParamIn(S"ShowMainDialog", S"0");
	m_NxnParam->set_ParamIn(S"ShowDialog", S"0");
	m_NxnParam->SetProgressDialog(S"", (NxNProgressDialogType)7);
	m_NxnParam->VerboseLevel = 4;

	m_NxnNamespace->Flush(S"\\Workspace\\", true, 1, "");
	m_NxnParam->XML = m_NxnNamespace->RunCommand(S"\\Workspace\\", command, m_NxnParam->XML, 0);
	return (m_NxnParam->WasSuccessful == 1);
}

bool audAssetManagerAb6::Reconnect(void)
{
	Shutdown(); //Don't bother checking the return value here, as we could be reconnecting due to a lost connection.
	return Init(m_ServerName, m_ProjectName, m_UserName, m_PasswordStr, m_ShouldWorkLocally);
}

String *audAssetManagerAb6::GetWorkingPath(String *assetPath)
{
	m_NxnNamespace->Flush(S"\\Workspace\\", true, 1, "");
	String *workingPath = m_NxnNamespace->GetProperty(String::Concat(S"\\Workspace\\", m_ProjectName, S"\\", assetPath),
		"LocalPath");

	if(workingPath == 0)
	{
		int index = assetPath->LastIndexOf('\\');
		if(index <= 0)
		{
			System::Diagnostics::Debug::Write(S"GetWorkingPath() failed with ");
			System::Diagnostics::Debug::WriteLine(assetPath);
			return 0;
		}
		else
		{
			String *trimmedAssetPath = assetPath->Substring(0, index);
			while(workingPath == 0)
			{
				workingPath = GetWorkingPath(trimmedAssetPath);
				if(workingPath == 0)
				{
					index = trimmedAssetPath->LastIndexOf('\\');
					if(index <= 0)
					{
						System::Diagnostics::Debug::Write(S"GetWorkingPath() failed with ");
						System::Diagnostics::Debug::WriteLine(assetPath);
						return 0;
					}

					trimmedAssetPath = trimmedAssetPath->Substring(0, index);
				}
			}
		}

		workingPath = String::Concat(workingPath, assetPath->Substring(index + 1));
	}

	return workingPath;
}

bool audAssetManagerAb6::SetWorkingPath(String *assetPath, String *localWorkingPath)
{
	//Trim any final '\' from paths (to satisfy Alienbrain).
	String *backslashStr = S"\\";
	String *trimmedAssetPath = assetPath->TrimEnd(backslashStr->ToCharArray());
	String *trimmedLocalWorkingPath = localWorkingPath->TrimEnd(backslashStr->ToCharArray());

	m_NxnParam->Reset();
	String *command = S"SetWorkingPath";
	m_NxnParam->Command = command;
	if(trimmedLocalWorkingPath->Length)
	{
		m_NxnParam->set_ParamIn(S"Path", trimmedLocalWorkingPath);
	}
	m_NxnParam->set_ParamIn(S"ShowMainDialog", S"0");
	m_NxnParam->set_ParamIn(S"ShowDialog", S"0");
	m_NxnParam->SetProgressDialog(S"", (NxNProgressDialogType)7);
	m_NxnParam->VerboseLevel = 4;

	m_NxnNamespace->Flush(S"\\Workspace\\", true, 1, "");
	NxNNamespaceCollectionObj *object = __try_cast<NxNNamespaceCollectionObj *>(m_NxnNamespace->GetNamespaceObj(
		String::Concat(S"\\Workspace\\", m_ProjectName, S"\\", trimmedAssetPath)));
	if(object == 0)
	{
		return false;
	}
	m_NxnParam->XML = object->RunCommand(command, m_NxnParam->XML, 0);
	return (m_NxnParam->WasSuccessful == 1);
}

bool audAssetManagerAb6::IsUpToDate(String *assetPath)
{
	bool returnVal=false;

	m_NxnNamespace->Flush(S"\\Workspace\\", true, 1, "");
	String *localPath = GetWorkingPath(assetPath);
	if(localPath)
	{
		System::IO::FileInfo *localFileInfo = new System::IO::FileInfo(localPath);
		if(localFileInfo->Exists)
		{
			String *clientModificationTimeString = m_NxnNamespace->GetProperty(String::Concat(S"\\Workspace\\",
				m_ProjectName, S"\\", assetPath), "CMOT");
			String *serverModificationTimeString = m_NxnNamespace->GetProperty(String::Concat(S"\\Workspace\\",
				m_ProjectName, S"\\", assetPath), "SMOT");

			if(clientModificationTimeString->Equals(serverModificationTimeString))
			{
				returnVal = true;
			}
		}
	}

	return returnVal;
}

bool audAssetManagerAb6::GetLatest(String *assetPath)
{
	return GetLatest(assetPath, true);
}

bool audAssetManagerAb6::GetLatest(String *assetPath, bool shouldDefaultResponses)
{
	m_NxnParam->Reset();
	String *command = S"GetLatest";
	m_NxnParam->Command = command;
	if(shouldDefaultResponses)
	{
		m_NxnParam->set_ParamIn(S"OverwriteCheckedOut", S"1");
		if(m_ShouldWorkLocally)
		{
			//Don't overwrite local asset(s) if writeable.
			m_NxnParam->set_ParamIn(S"OverwriteWritable", S"1");
		}
		else
		{
			m_NxnParam->set_ParamIn(S"OverwriteWritable", S"2");
		}
		m_NxnParam->set_ParamIn(S"MergeIfNecessary", S"1");
		m_NxnParam->HideResponseDialog(S"GetLatest_ServerCallFailed", 3);
		m_NxnParam->HideResponseDialog(S"GetLatest_LocalPathInvalid", 3);
		m_NxnParam->HideResponseDialog(S"GetLatest_ReadOnlyFailed", 3);
		m_NxnParam->HideResponseDialog(S"GetLatest_MkDirFailed", 3);
		m_NxnParam->HideResponseDialog(S"GetLatest_TransferFailed", 3);
		m_NxnParam->HideResponseDialog(S"GetLatest_LabelVersionOffline", 3);
		m_NxnParam->HideResponseDialog(S"GetLatest_MergingFailed", 3);
		m_NxnParam->HideResponseDialog(S"GetLatest_DbFileIsFolderOnLocalDisk", 3);
		m_NxnParam->HideResponseDialog(S"GetLatest_DbFolderIsFileOnLocalDisk", 3);
	}

	m_NxnParam->set_ParamIn(S"ShowMainDialog", S"0");
	m_NxnParam->set_ParamIn(S"ShowDialog", S"0");
	m_NxnParam->SetProgressDialog(S"", (NxNProgressDialogType)7);
	m_NxnParam->VerboseLevel = 4;

	m_NxnNamespace->Flush(S"\\Workspace\\", true, 1, "");
	NxNNamespaceCollectionObj *object = __try_cast<NxNNamespaceCollectionObj *>(m_NxnNamespace->GetNamespaceObj(
		String::Concat(S"\\Workspace\\", m_ProjectName, S"\\", assetPath)));
	if(object == 0)
	{
		return false;
	}
	m_NxnParam->XML = object->RunCommand(command, m_NxnParam->XML, 0);
	return (m_NxnParam->WasSuccessful == 1);
}

bool audAssetManagerAb6::ExistsAsAsset(String *assetPath)
{
	if(m_ShouldWorkLocally)
	{
		//Check if local asset exists.
		bool exists = false;
		String *localPath = GetWorkingPath(assetPath);
		if(localPath)
		{
			IO::FileInfo *localFileInfo = new IO::FileInfo(localPath);
			if(localFileInfo->Exists)
			{
				exists = true;
			}
			else
			{
				//This could be a directory rather than a file.
				IO::DirectoryInfo *localDirectoryInfo = new IO::DirectoryInfo(localPath);
				exists = localDirectoryInfo->Exists;
			}
		}

		return exists;
	}
	else
	{
		m_NxnNamespace->Flush(S"\\Workspace\\", true, 1, "");
		String *serverPath = m_NxnNamespace->GetProperty(String::Concat(S"\\Workspace\\", m_ProjectName, S"\\", assetPath),
			"ServerPath");
		return (serverPath != 0);
	}
}

bool audAssetManagerAb6::IsLocked(String *assetPath)
{
	bool returnVal=false;

	m_NxnNamespace->Flush(S"\\Workspace\\", true, 1, "");
	String *flagsString = m_NxnNamespace->GetProperty(String::Concat(S"\\Workspace\\", m_ProjectName, S"\\", assetPath),
		"WkspcFlags");
	if(flagsString)
	{
		int flags = Int32::Parse(flagsString);
		if(flags & 16)
		{
			returnVal = true;
		}
	}

	return returnVal;
}

String *audAssetManagerAb6::GetLockedOwner(String *assetPath)
{
	String *owner=S"";

	if(IsLocked(assetPath))
	{
		m_NxnNamespace->Flush(S"\\Workspace\\", true, 1, "");
		owner = m_NxnNamespace->GetProperty(String::Concat(S"\\Workspace\\", m_ProjectName, S"\\", assetPath),
			"Locked By");
	}

	return owner;
}

bool audAssetManagerAb6::IsCheckedOut(String *assetPath)
{
	bool returnVal=false;

	m_NxnNamespace->Flush(S"\\Workspace\\", true, 1, "");
	String *flagsString = m_NxnNamespace->GetProperty(String::Concat(S"\\Workspace\\", m_ProjectName, S"\\", assetPath),
		"WkspcFlags");
	if(flagsString)
	{
		int flags = Int32::Parse(flagsString);
		if(flags & 32)
		{
			returnVal = true;
		}
	}

	return returnVal;
}

bool audAssetManagerAb6::LocalCheckOut(String *, String *)
{
 ///To Do
	return false;
}
	

bool audAssetManagerAb6::CheckOut(String *assetPath, String *comment)
{
	if(m_ShouldWorkLocally)
	{
		//Simply get the latest version of the asset(s), without overwriting writeable local assets and make writeable.
		GetLatest(assetPath);
		MakeLocalAssetWriteable(GetWorkingPath(assetPath));
		return true;
	}
	else
	{
		//Trim any final '\' from full asset path (to satisfy Alienbrain).
		String *backslashStr = S"\\";
		String *trimmedAssetPath = assetPath->TrimEnd(backslashStr->ToCharArray());

		if(IsLocked(trimmedAssetPath))
		{
			//All audio check-outs should be exclusive.
			return false;
		}

		//Ensure that any existing local copy of the asset is read-only - to avoid an Alienbrain dialog.
		String *localPath = GetWorkingPath(trimmedAssetPath);
		MakeLocalAssetReadOnly(localPath);

		m_NxnParam->Reset();
		String *command = S"CheckOut";
		m_NxnParam->Command = command;
		String *quotesStr = S"\" ";
		if((comment = comment->TrimEnd(quotesStr->ToCharArray()))->Length)
		{
			m_NxnParam->set_ParamIn(S"Comment", comment);
		}
		m_NxnParam->set_ParamIn(S"AllowMultipleCheckout", S"0");
		m_NxnParam->HideResponseDialog(S"CheckOut_Failed", 3);
		m_NxnParam->HideResponseDialog(S"WorkingPathNotSet", 2);
		m_NxnParam->HideResponseDialog(S"CheckOut_CantCreateOldTemp", 3);
		m_NxnParam->HideResponseDialog(S"CheckOut_CantDeleteNewTemp", 3);
		m_NxnParam->HideResponseDialog(S"CheckOut_CantRevertOldTemp", 3);
		m_NxnParam->HideResponseDialog(S"CheckOut_CantActivateNewTemp", 3);
		m_NxnParam->HideResponseDialog(S"CheckOut_CantDeleteOldTemp", 3);
		m_NxnParam->HideResponseDialog(S"CheckOut_CantMakeWriteable", 3);
		m_NxnParam->HideResponseDialog(S"CantDeleteHandOver", 3);
		m_NxnParam->HideResponseDialog(S"CantCreateLocalDir", 3);
		m_NxnParam->HideResponseDialog(S"CantMakeDirWriteable", 3);
		m_NxnParam->HideResponseDialog(S"Checkout_DbFileIsFolderOnLocalDisk", 3);
		m_NxnParam->set_ParamIn(S"ShowMainDialog", S"0");
		m_NxnParam->set_ParamIn(S"ShowDialog", S"0");
		m_NxnParam->SetProgressDialog(S"", (NxNProgressDialogType)7);
		m_NxnParam->VerboseLevel = 4;

		m_NxnNamespace->Flush(S"\\Workspace\\", true, 1, "");
		NxNNamespaceCollectionObj *object = __try_cast<NxNNamespaceCollectionObj *>(m_NxnNamespace->GetNamespaceObj(
			String::Concat(S"\\Workspace\\", m_ProjectName, S"\\", trimmedAssetPath)));
		if(object == 0)
		{
			return false;
		}
		m_NxnParam->XML = object->RunCommand(command, m_NxnParam->XML, 0);
		return (m_NxnParam->WasSuccessful == 1);
	}
}

bool audAssetManagerAb6::SimpleCheckOut(String *assetPath, String *comment)
{
	bool returnVal=true;

	if(ExistsAsAsset(assetPath) && !IsCheckedOut(assetPath))
	{
		returnVal = CheckOut(assetPath, comment);
	}

	return returnVal;
}

bool audAssetManagerAb6::UndoCheckOut(String *assetPath)
{
	if(m_ShouldWorkLocally)
	{
		//Simply make the asset(s) read-only.
		MakeLocalAssetReadOnly(GetWorkingPath(assetPath));
		return true;
	}
	else
	{
		if(!IsCheckedOut(assetPath))
		{
			return true;
		}

		m_NxnParam->Reset();
		String *command = S"UndoCheckOut";
		m_NxnParam->Command = command;
		m_NxnParam->set_ParamIn(S"UpdatePolicy", S"2");
		m_NxnParam->set_ParamIn(S"RevertOverwritePolicy", S"2");
		m_NxnParam->HideResponseDialog(S"UndoCheckOut_Failed", 3);
		m_NxnParam->HideResponseDialog(S"CheckedOutBySomeoneElseMultiple", 2);
		m_NxnParam->HideResponseDialog(S"CantDeleteFile", 3);
		m_NxnParam->HideResponseDialog(S"CantMakeReadOnly", 3);
		m_NxnParam->set_ParamIn(S"ShowMainDialog", S"0");
		m_NxnParam->set_ParamIn(S"ShowDialog", S"0");
		m_NxnParam->SetProgressDialog(S"", (NxNProgressDialogType)7);
		m_NxnParam->VerboseLevel = 4;

		m_NxnNamespace->Flush(S"\\Workspace\\", true, 1, "");
		NxNNamespaceCollectionObj *object = __try_cast<NxNNamespaceCollectionObj *>(m_NxnNamespace->GetNamespaceObj(
			String::Concat(S"\\Workspace\\", m_ProjectName, S"\\", assetPath)));
		if(object == 0)
		{
			return false;
		}
		m_NxnParam->XML = object->RunCommand(command, m_NxnParam->XML, 0);
		return (m_NxnParam->WasSuccessful == 1);
	}
}

bool audAssetManagerAb6::SimpleUndoCheckOut(String *assetPath)
{
	bool returnVal=true;

	if(ExistsAsAsset(assetPath))
	{
		returnVal = UndoCheckOut(assetPath);
	}

	return returnVal;
}

bool audAssetManagerAb6::CheckIn(String *assetPath, String *comment)
{
	if(m_ShouldWorkLocally)
	{
		//Simply make the asset(s) read-only.
		MakeLocalAssetReadOnly(GetWorkingPath(assetPath));
		return true;
	}
	else
	{
		//Trim any final '\' from full asset path (to satisfy Alienbrain).
		String *backslashStr = S"\\";
		String *trimmedAssetPath = assetPath->TrimEnd(backslashStr->ToCharArray());

		m_NxnParam->Reset();
		String *command = S"CheckIn";
		m_NxnParam->Command = command;
		String *quotesStr = S"\" ";
		if(comment && (comment = comment->TrimEnd(quotesStr->ToCharArray()))->Length)
		{
			m_NxnParam->set_ParamIn(S"Comment", comment);
		}
		m_NxnParam->HideResponseDialog(S"LocalFileDoesNotExist", 3);
		m_NxnParam->HideResponseDialog(S"CheckIn_Failed", 3);
		m_NxnParam->HideResponseDialog(S"CantMakeReadOnly", 3);
		m_NxnParam->HideResponseDialog(S"CantReplaceWithMergeResult", 3);
		m_NxnParam->set_ParamIn(S"ShowMainDialog", S"0");
		m_NxnParam->set_ParamIn(S"ShowDialog", S"0");
		m_NxnParam->SetProgressDialog(S"", (NxNProgressDialogType)7);
		m_NxnParam->VerboseLevel = 4;

		m_NxnNamespace->Flush(S"\\Workspace\\", true, 1, "");
		NxNNamespaceCollectionObj *object = __try_cast<NxNNamespaceCollectionObj *>(m_NxnNamespace->GetNamespaceObj(
			String::Concat(S"\\Workspace\\", m_ProjectName, S"\\", trimmedAssetPath)));
		if(object == 0)
		{
			return false;
		}
		m_NxnParam->XML = object->RunCommand(command, m_NxnParam->XML, 0);
		return (m_NxnParam->WasSuccessful == 1);
	}
}

bool audAssetManagerAb6::Import(String *localPath, String *assetPath, String *comment)
{
	if(m_ShouldWorkLocally)
	{
		//Simply make the asset(s) read-only.
		MakeLocalAssetReadOnly(GetWorkingPath(assetPath));
		return true;
	}
	else
	{
		//Trim asset name (if present) and final '\' from full asset path (to satisfy Alienbrain).
		String *assetRootPath = assetPath->Substring(0, assetPath->LastIndexOf('\\'));

		m_NxnParam->Reset();
		String *command = S"Import";
		m_NxnParam->Command = command;
		m_NxnParam->set_ParamIn(S"LocalPath", localPath);
		String *quotesStr = S"\" ";
		if((comment = comment->TrimEnd(quotesStr->ToCharArray()))->Length)
		{
			m_NxnParam->set_ParamIn(S"Comment", comment);
		}
		m_NxnParam->set_ParamIn(S"Merge", S"0");
		m_NxnParam->set_ParamIn(S"ShowMainDialog", S"0");
		m_NxnParam->set_ParamIn(S"ShowDialog", S"0");
		m_NxnParam->set_ParamIn(S"GetLocal", S"0");
		m_NxnParam->HideResponseDialog(S"Import_Failed", 3);
		m_NxnParam->HideResponseDialog(S"Import_LocalMkDirFailed", 3);
		m_NxnParam->HideResponseDialog(S"Import_GetLocalFailed", 3);
		m_NxnParam->HideResponseDialog(S"Import_FileExists", 3);
		m_NxnParam->HideResponseDialog(S"Import_InvalidFilename", 3);
		m_NxnParam->SetProgressDialog(S"", (NxNProgressDialogType)7);
		m_NxnParam->VerboseLevel = 4;

		m_NxnNamespace->Flush(S"\\Workspace\\", true, 1, "");
		NxNNamespaceCollectionObj *object = __try_cast<NxNNamespaceCollectionObj *>(m_NxnNamespace->GetNamespaceObj(
			String::Concat(S"\\Workspace\\", m_ProjectName, S"\\", assetRootPath)));
		if(object == 0)
		{
			return false;
		}
		m_NxnParam->XML = object->RunCommand(command, m_NxnParam->XML, 0);
		return (m_NxnParam->WasSuccessful == 1);
	}
}

bool audAssetManagerAb6::CheckInOrImport(String *localPath, String *assetPath, String *comment)
{
	bool returnVal;

	if(ExistsAsAsset(assetPath))
	{
		returnVal = CheckIn(assetPath, comment);
	}
	else
	{
		returnVal = Import(localPath, assetPath, comment);
	}

	return returnVal;
}

bool audAssetManagerAb6::DeleteAsset(String *localPath, String *assetPath, bool deleteLocal)
{
	if(deleteLocal)
	{
		if(localPath && localPath->Length)
		{
			//
			//Delete the local file directly to avoid any Alienbrain dialog problems.
			//

			MakeLocalAssetWriteable(localPath);

			FileInfo *localFileInfo = new FileInfo(localPath);
			if(localFileInfo->Exists)
			{
				localFileInfo->Delete();
			}
			else
			{
				//This could be a directory rather than a file...
				DirectoryInfo *localDirectoryInfo = new DirectoryInfo(localPath);
				if(localDirectoryInfo->Exists)
				{
					localDirectoryInfo->Delete(true);
				}
			}
		}
	}

	if(m_ShouldWorkLocally || !ExistsAsAsset(assetPath))
	{
		return true;
	}

	m_NxnParam->Reset();
	String *command = S"RemoveFromControl";
	m_NxnParam->Command = command;
	m_NxnParam->HideResponseDialog(S"DeleteWithUndo_AreYouSure", 1);
	m_NxnParam->HideResponseDialog(S"Delete_SystemFiles", 3);
	m_NxnParam->HideResponseDialog(S"Delete_Locked", 2);
	m_NxnParam->HideResponseDialog(S"Delete_CheckedOut", 1);
	m_NxnParam->HideResponseDialog(S"DeleteWithUndo_Failed", 3);
	m_NxnParam->HideResponseDialog(S"DeleteWithUndo_DeleteLocalFailed", 3);
	m_NxnParam->HideResponseDialog(S"Delete_DbFileIsFolderOnLocalDisk", 3);
	m_NxnParam->HideResponseDialog(S"Delete_DbFolderIsFileOnLocalDisk", 3);
	m_NxnParam->set_ParamIn(S"ShowMainDialog", S"0");
	m_NxnParam->set_ParamIn(S"ShowDialog", S"0");
	m_NxnParam->SetProgressDialog(S"", (NxNProgressDialogType)7);
	m_NxnParam->VerboseLevel = 4;

	m_NxnNamespace->Flush(S"\\Workspace\\", true, 1, "");
	NxNNamespaceCollectionObj *object = __try_cast<NxNNamespaceCollectionObj *>(m_NxnNamespace->GetNamespaceObj(
		String::Concat(S"\\Workspace\\", m_ProjectName, S"\\", assetPath)));
	if(object == 0)
	{
		return false;
	}
	m_NxnParam->XML = object->RunCommand(command, m_NxnParam->XML, 0);
	return (m_NxnParam->WasSuccessful == 1);
}

bool audAssetManagerAb6::RenameAsset(String *assetPath, String *newName)
{
	if(m_ShouldWorkLocally)
	{
		//
		//Rename local asset.
		//
		String *localPath = GetWorkingPath(assetPath);
		if(localPath)
		{
			//Generate new local path.
			int splitIndex = localPath->LastIndexOf('\\');
			String *newPath = localPath->Substring(0, splitIndex);
			newPath = String::Concat(newPath, S"\\", newName);

			MakeLocalAssetWriteable(localPath);

			IO::FileInfo *localFileInfo = new IO::FileInfo(localPath);
			if(localFileInfo->Exists)
			{
				localFileInfo->MoveTo(newPath);
				MakeLocalAssetReadOnly(newPath);
				return true;
			}
			else
			{
				//This could be a directory rather than a file.
				IO::DirectoryInfo *localDirectoryInfo = new IO::DirectoryInfo(localPath);
				if(localDirectoryInfo->Exists)
				{
					localDirectoryInfo->MoveTo(newPath);
					MakeLocalAssetReadOnly(newPath);
					return true;
				}
			}
		}

		return false;
	}
	else
	{
		m_NxnParam->Reset();
		String *command = S"Rename";
		m_NxnParam->Command = command;
		m_NxnParam->set_ParamIn(S"Name", newName);
		m_NxnParam->set_ParamIn(S"ShowMainDialog", S"0");
		m_NxnParam->set_ParamIn(S"ShowDialog", S"0");
		m_NxnParam->SetProgressDialog(S"", (NxNProgressDialogType)7);
		m_NxnParam->VerboseLevel = 4;

		m_NxnNamespace->Flush(S"\\Workspace\\", true, 1, "");
		NxNNamespaceCollectionObj *object = __try_cast<NxNNamespaceCollectionObj *>(m_NxnNamespace->GetNamespaceObj(
			String::Concat(S"\\Workspace\\", m_ProjectName, S"\\", assetPath)));
		if(object == 0)
		{
			return false;
		}
		m_NxnParam->XML = object->RunCommand(command, m_NxnParam->XML, 0);
		return (m_NxnParam->WasSuccessful == 1);
	}
}

bool audAssetManagerAb6::CreateFolder(String *folderPath, String *comment)
{
	(comment);

	if(m_ShouldWorkLocally)
	{
		//
		//Create folder locally.
		//
		String *localPath = GetWorkingPath(folderPath);
		if(localPath)
		{
			IO::DirectoryInfo* localDirectoryInfo = IO::Directory::CreateDirectory(localPath);
			if(localDirectoryInfo->Exists)
			{
				return true;
			}
		}

		return false;
	}
	else
	{
		//Split folder path into a parent path and new folder name.
		String *backslashStr = S"\\";
		String *trimmedFolderPath = folderPath->TrimEnd(backslashStr->ToCharArray());
		int splitIndex = trimmedFolderPath->LastIndexOf('\\');
		String *folderParentPath = trimmedFolderPath->Substring(0, splitIndex);
		String *folderName = trimmedFolderPath->Substring(splitIndex + 1);

		m_NxnParam->Reset();
		String *command = S"NewFolder";
		m_NxnParam->Command = command;
		m_NxnParam->set_ParamIn(S"Name", folderName);
		m_NxnParam->set_ParamIn(S"ShowMainDialog", S"0");
		m_NxnParam->set_ParamIn(S"ShowDialog", S"0");
		m_NxnParam->SetProgressDialog(S"", (NxNProgressDialogType)7);
		m_NxnParam->VerboseLevel = 4;

		m_NxnNamespace->Flush(S"\\Workspace\\", true, 1, "");
		NxNNamespaceCollectionObj *object = __try_cast<NxNNamespaceCollectionObj *>(m_NxnNamespace->GetNamespaceObj(
			String::Concat(S"\\Workspace\\", m_ProjectName, S"\\", folderParentPath)));
		if(object == 0)
		{
			return false;
		}
		m_NxnParam->XML = object->RunCommand(command, m_NxnParam->XML, 0);
		return (m_NxnParam->WasSuccessful == 1);
	}
}

bool audAssetManagerAb6::CreateCurrentChangelist(String *)
{
	return true;	// not implemented
}

bool audAssetManagerAb6::CheckInCurrentChangelist()
{
	return true;	// not implemented
}

bool audAssetManagerAb6::DeleteCurrentChangelist()
{
	return true;	// not implemented
}

#pragma warning (pop)
