﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace DStarChecker
{
    internal class Program
    {
        private const string DSTAR_FILTER = "*.dstar";
        private const string INVALID_DSTAR_PATH = "Invalid dialogue path supplied.";
        private const string INVALID_CONFIG_PATH = "Invalid config file supplied.";

        private static readonly HashSet<string> ms_emptyConversationRoots = new HashSet<string>();

        private static readonly Dictionary<string, HashSet<string>> ms_emptyDialogueLines =
            new Dictionary<string, HashSet<string>>();

        private static readonly Dictionary<string, List<XElement>> ms_invalidDialogueLines =
            new Dictionary<string, List<XElement>>();

        private static readonly Dictionary<string, HashSet<string>> ms_missingTimestampDialogueLines =
            new Dictionary<string, HashSet<string>>();

        private static readonly Dictionary<string, HashSet<string>> ms_duplicateLineFilenames =
            new Dictionary<string, HashSet<string>>();

        private static readonly Dictionary<string, Dictionary<Guid, HashSet<string>>> ms_unknownCharacters =
            new Dictionary<string, Dictionary<Guid, HashSet<string>>>();

        private static readonly Dictionary<string, HashSet<string>> ms_badRandomConversations =
            new Dictionary<string, HashSet<string>>();

        private static readonly Dictionary<string, HashSet<string>> ms_randomConversationsWithMultipleVoices =
            new Dictionary<string, HashSet<string>>();

        private static readonly Dictionary<Guid, string> ms_characters = new Dictionary<Guid, string>();

        private static void Main(string[] args)
        {
            if (args.Length != 2)
            {
                Console.WriteLine(
                    "Invalid number of arguments. Expected <dialogue_path> <config_file>.\nE.g. X:\\gta5\\assets\\Dialogue\\American\\ X:\\gta5\\assets\\Dialogue\\Config\\Configurations.xml");
                return;
            }

            var dialoguePath = args[0];
            if (string.IsNullOrEmpty(dialoguePath) ||
                !Directory.Exists(dialoguePath))
            {
                Console.WriteLine(INVALID_DSTAR_PATH);
                return;
            }

            var configFile = args[1];
            if (string.IsNullOrEmpty(configFile) ||
                !File.Exists(configFile))
            {
                Console.WriteLine(INVALID_CONFIG_PATH);
                return;
            }

            LoadCharacters(configFile);

            var dStarFiles = Directory.GetFiles(dialoguePath, DSTAR_FILTER, SearchOption.AllDirectories);
            foreach (var dStarFile in dStarFiles)
            {
                CheckFile(dStarFile);
            }

            WriteReports();
        }

        private static void LoadCharacters(string configFile)
        {
            var config = XDocument.Load(configFile);
            var characters = config.Root.Descendants("Character");
            foreach (var character in characters)
            {
                ms_characters.Add(new Guid(character.Attribute("guid").Value), character.Attribute("name").Value);
            }
        }

        private static void WriteReports()
        {
            if (ms_emptyConversationRoots.Count > 0)
            {
                Console.WriteLine("*** Files with missing or empty \"root\" attributes on Conversations ***\n");
                foreach (var entry in ms_emptyConversationRoots)
                {
                    var root = Path.GetPathRoot(entry);
                    Console.WriteLine(string.Concat(root != null ? entry.Replace(root, "\\") : entry));
                }
            }
            else
            {
                Console.WriteLine("There were no conversations with missing or empty \"root\" attributes!");
            }
            Console.WriteLine();

            if (ms_emptyDialogueLines.Count > 0)
            {
                Console.WriteLine("*** Files with missing or empty \"dialogue\" attributes ***\n");
                foreach (var entry in ms_emptyDialogueLines)
                {
                    var root = Path.GetPathRoot(entry.Key);
                    Console.WriteLine(string.Concat(root != null ? entry.Key.Replace(root, "\\") : entry.Key, " :"));
                    foreach (var conversationName in entry.Value)
                    {
                        Console.WriteLine(conversationName);
                    }
                }
            }
            else
            {
                Console.WriteLine("There were no lines with missing or empty \"dialogue\" attributes!");
            }
            Console.WriteLine();

            if (ms_invalidDialogueLines.Count > 0)
            {
                Console.WriteLine("*** Files with illegal text in their \"dialogue\" attributes ***\n");
                foreach (var entry in ms_invalidDialogueLines)
                {
                    var root = Path.GetPathRoot(entry.Key);
                    Console.WriteLine(root != null ? entry.Key.Replace(root, "\\") : entry.Key);
                }
            }
            else
            {
                Console.WriteLine("There were no lines with invalid dialogue text!");
            }
            Console.WriteLine();

            if (ms_missingTimestampDialogueLines.Count > 0)
            {
                Console.WriteLine("*** Files with missing or invalid \"timestamp\" attributes ***\n");
                foreach (var entry in ms_missingTimestampDialogueLines)
                {
                    var root = Path.GetPathRoot(entry.Key);
                    Console.WriteLine(string.Concat(root != null ? entry.Key.Replace(root, "\\") : entry.Key, " :"));
                    foreach (var conversationName in entry.Value)
                    {
                        Console.WriteLine(conversationName);
                    }
                }
            }
            else
            {
                Console.WriteLine("There were no lines with missing or empty \"timestamp\" attributes!");
            }
            Console.WriteLine();

            if (ms_duplicateLineFilenames.Count > 0)
            {
                Console.WriteLine("*** Lines with duplicate \"filename\" attributes ***\n");
                foreach (var entry in ms_duplicateLineFilenames)
                {
                    var root = Path.GetPathRoot(entry.Key);
                    Console.WriteLine(string.Concat(root != null ? entry.Key.Replace(root, "\\") : entry.Key, " :"));
                    foreach (var duplicateFileName in entry.Value)
                    {
                        Console.WriteLine(duplicateFileName);
                    }
                }
            }
            else
            {
                Console.WriteLine("There were no lines with duplicate \"filename\" attributes within a dialogue!");
            }
            Console.WriteLine();

            if (ms_unknownCharacters.Count > 0)
            {
                Console.WriteLine("*** Conversations containing lines that reference undefined characters ***\n");
                foreach (var entry in ms_unknownCharacters)
                {
                    var root = Path.GetPathRoot(entry.Key);
                    Console.WriteLine(string.Concat(root != null ? entry.Key.Replace(root, "\\") : entry.Key, " :"));
                    foreach (var undefinedCharacterEntry in entry.Value)
                    {
                        Console.WriteLine("Undefined character with GUID: {0} referenced in conversations -",
                                          undefinedCharacterEntry.Key);
                        foreach (var conversation in undefinedCharacterEntry.Value)
                        {
                            Console.WriteLine(conversation);
                        }
                    }
                    Console.WriteLine();
                }
            }
            else
            {
                Console.WriteLine("There were no lines that reference undefined characters!");
            }
            Console.WriteLine();

            if (ms_badRandomConversations.Count > 0)
            {
                Console.WriteLine("*** Conversations that have random=\"True\" but do not number lines properly ***\n");
                foreach (var entry in ms_badRandomConversations)
                {
                    var root = Path.GetPathRoot(entry.Key);
                    Console.WriteLine(string.Concat(root != null ? entry.Key.Replace(root, "\\") : entry.Key, " :"));
                    foreach (var badConversation in entry.Value)
                    {
                        Console.WriteLine(badConversation);
                    }
                    Console.WriteLine();
                }
            }
            else
            {
                Console.WriteLine("There were no random conversations that do not number lines properly!");
            }
            Console.WriteLine();

            if (ms_randomConversationsWithMultipleVoices.Count > 0)
            {
                Console.WriteLine("*** Conversations that have random=\"True\" but use multiple speakers ***\n");
                foreach (var entry in ms_randomConversationsWithMultipleVoices)
                {
                    var root = Path.GetPathRoot(entry.Key);
                    Console.WriteLine(string.Concat(root != null ? entry.Key.Replace(root, "\\") : entry.Key, " :"));
                    foreach (var conversation in entry.Value)
                    {
                        Console.WriteLine(conversation);
                    }
                    Console.WriteLine();
                }
            }
            else
            {
                Console.WriteLine("There were no random conversations that have multiple speakers!");
            }
            Console.WriteLine();
        }

        private static void CheckFile(string file)
        {
            XDocument doc = null;
            try
            {
                doc = XDocument.Load(file);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            if (doc != null)
            {
                var conversations = doc.Descendants("Conversation");
                var lines = doc.Descendants("Line");

                // Check for conversations with a missing or empty "root" attribute
                CheckForMissingOrEmptyConversationRoots(file, lines);

                // Check for lines with a missing or empty "dialogue" attribute
                CheckForMissingOrEmptyDialogue(file, lines);

                // Check for lines where the "dialogue" attribute contains illegal text
                CheckForIllegalDialogue(file, lines);

                // Check for lines that don't have a "timestamp" attribute
                CheckForMissingTimestampDialogue(file, lines);

                // Find lines with the same filename attribute in a dialogue
                CheckForDuplicateFilenames(file, lines);

                // Check for characters that are not defined in the Configurations.xml file
                CheckForUnknownCharacters(file, lines);

                // Check for conversations marked random="True" that do not have properly numbered filenames
                CheckForBadRandomConversations(file, conversations);

                // Check for conversations marked random="True" that have more than one voice in the conversation
                CheckForRandomConversationsWithMultipleVoices(file, conversations);
            }
        }

        private static void CheckForRandomConversationsWithMultipleVoices(string file,
                                                                          IEnumerable<XElement> conversations)
        {
            var multipleVoiceConversations = new HashSet<string>();
            foreach (var conversation in conversations)
            {
                var random = bool.Parse(conversation.Attribute("random").Value);
                if (random)
                {
                    var conversationName = conversation.Attribute("root").Value;
                    var lines = conversation.Descendants("Line").ToList();
                    for (var i = 0; i < lines.Count - 1; ++i)
                    {
                        if (lines[i].Attribute("guid").Value != lines[i + 1].Attribute("guid").Value &&
                            !multipleVoiceConversations.Contains(conversationName))
                        {
                            multipleVoiceConversations.Add(conversationName);
                            break;
                        }
                    }
                }
            }

            if (multipleVoiceConversations.Count > 0)
            {
                ms_randomConversationsWithMultipleVoices.Add(file, multipleVoiceConversations);
            }
        }

        private static void CheckForBadRandomConversations(string file, IEnumerable<XElement> conversations)
        {
            var badConversations = new HashSet<string>();
            foreach (var conversation in conversations)
            {
                var random = bool.Parse(conversation.Attribute("random").Value);
                if (random)
                {
                    var conversationName = conversation.Attribute("root").Value;
                    var lines = conversation.Descendants("Line").ToList();
                    foreach (var line in lines)
                    {
                        var filename = line.Attribute("filename").Value;
                        var index = filename.LastIndexOf("_");
                        if (index < 0)
                        {
                            badConversations.Add(conversationName);
                            break;
                        }

                        var number = filename.Substring(index + 1, filename.Length - (index + 1));
                        uint temp;
                        if (!uint.TryParse(number, out temp))
                        {
                            badConversations.Add(conversationName);
                            break;
                        }
                    }
                }
            }

            if (badConversations.Count > 0)
            {
                ms_badRandomConversations.Add(file, badConversations);
            }
        }

        private static void CheckForUnknownCharacters(string file, IEnumerable<XElement> lines)
        {
            var unknownCharacterConversations = new Dictionary<Guid, HashSet<string>>();
            foreach (var line in lines)
            {
                var guid = new Guid(line.Attribute("guid").Value);
                if (!ms_characters.ContainsKey(guid))
                {
                    HashSet<string> conversations;
                    if (!unknownCharacterConversations.TryGetValue(guid, out conversations))
                    {
                        conversations = new HashSet<string>();
                        unknownCharacterConversations.Add(guid, conversations);
                    }

                    var conversationName = line.Ancestors("Conversation").First().Attribute("root").Value;
                    if (!conversations.Contains(conversationName))
                    {
                        conversations.Add(conversationName);
                    }
                }
            }

            if (unknownCharacterConversations.Count > 0)
            {
                ms_unknownCharacters.Add(file, unknownCharacterConversations);
            }
        }

        private static void CheckForDuplicateFilenames(string file, IEnumerable<XElement> lines)
        {
            var duplicatedFilenames = new HashSet<string>();
            var fileNames = new HashSet<string>();
            foreach (var line in lines)
            {
                var fileNameValue = line.Attribute("filename").Value;
                if (!string.IsNullOrEmpty(fileNameValue))
                {
                    if (fileNames.Contains(fileNameValue))
                    {
                        if (!duplicatedFilenames.Contains(fileNameValue))
                        {
                            duplicatedFilenames.Add(fileNameValue);
                        }
                    }
                    else
                    {
                        fileNames.Add(fileNameValue);
                    }
                }
            }

            if (duplicatedFilenames.Count > 0)
            {
                ms_duplicateLineFilenames.Add(file, duplicatedFilenames);
            }
        }

        private static void CheckForMissingTimestampDialogue(string file, IEnumerable<XElement> lines)
        {
            var missingTimestampLines = lines.Where(IsMissingTimestamp).ToList();
            if (missingTimestampLines.Count > 0)
            {
                var conversations = new HashSet<string>();
                foreach (var line in missingTimestampLines)
                {
                    var conversation = line.Ancestors("Conversation").FirstOrDefault();
                    if (conversation != null)
                    {
                        var name = conversation.Attribute("root") == null ? null : conversation.Attribute("root").Value;
                        if (name != null &&
                            !conversations.Contains(name))
                        {
                            conversations.Add(name);
                        }
                    }
                }
                ms_missingTimestampDialogueLines.Add(file, conversations);
            }
        }

        private static void CheckForIllegalDialogue(string file, IEnumerable<XElement> lines)
        {
            var invalidLines = lines.Where(HasIllegalTextInDialogue).ToList();
            if (invalidLines.Count > 0)
            {
                ms_invalidDialogueLines.Add(file, invalidLines);
            }
        }

        private static void CheckForMissingOrEmptyConversationRoots(string file, IEnumerable<XElement> lines)
        {
            foreach (var line in lines)
            {
                var conversation = line.Ancestors("Conversation").FirstOrDefault();
                if (conversation != null)
                {
                    var name = conversation.Attribute("root") == null ? null : conversation.Attribute("root").Value;
                    if (string.IsNullOrEmpty(name) &&
                        !ms_emptyConversationRoots.Contains(file))
                    {
                        ms_emptyConversationRoots.Add(file);
                    }
                }
            }
        }

        private static void CheckForMissingOrEmptyDialogue(string file, IEnumerable<XElement> lines)
        {
            var emptyLines = lines.Where(HasEmptyDialogue).ToList();
            if (emptyLines.Count > 0)
            {
                var conversations = new HashSet<string>();
                foreach (var line in emptyLines)
                {
                    var conversation = line.Ancestors("Conversation").FirstOrDefault();
                    if (conversation != null)
                    {
                        var name = conversation.Attribute("root") == null ? null : conversation.Attribute("root").Value;
                        if (!string.IsNullOrEmpty(name) &&
                            !conversations.Contains(name))
                        {
                            conversations.Add(name);
                        }
                    }
                }
                ms_emptyDialogueLines.Add(file, conversations);
            }
        }

        private static bool IsMissingTimestamp(XElement line)
        {
            DateTime temp;
            return
                !(line.Attribute("timestamp") != null && DateTime.TryParse(line.Attribute("timestamp").Value, out temp));
        }

        private static bool HasEmptyDialogue(XElement line)
        {
            return !(line.Attribute("dialogue") != null && (!string.IsNullOrEmpty(line.Attribute("dialogue").Value)));
        }

        private static bool HasIllegalTextInDialogue(XElement line)
        {
            if (!HasEmptyDialogue(line))
            {
                var text = string.Concat("<root>", line.Attribute("dialogue").Value, "</root>");
                try
                {
                    XDocument.Parse(text);
                }
                catch (Exception)
                {
                    return true;
                }
            }
            return false;
        }
    }
}