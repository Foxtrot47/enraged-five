﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataEncryptionChecker
{
    class DataEncryptionChecker
    {
        List<string> unencryptedFiles = new List<string>();
        public DataEncryptionChecker(string path, string writeFileTo)
        {
            foreach (FileInfo file in GetFilesInDirectory(path))
            {
                try
                {
                    unencryptedFiles.Add(analyseFile(file));

                }
                catch(Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }

            string joined = string.Join("\n", unencryptedFiles.ToArray());
            File.WriteAllText(writeFileTo, joined);

        }


        private List<FileInfo> GetFilesInDirectory(string path)
        {
            DirectoryInfo dir = new DirectoryInfo(path);
            RecursiveFileSearch.WalkDirectoryTree(dir, "*.awc");

            return RecursiveFileSearch.filesInDirectory;
        }

        private string analyseFile(FileInfo file)
        {
            bool ADAT = false;
            bool encryptionBit = false;
            byte[] b = new byte[4];
            UTF8Encoding temp = new UTF8Encoding(true);
            using (FileStream fileStream = file.OpenRead())
            {
                fileStream.Read(b, 0, b.Length);
                ADAT = temp.GetString(b).Equals("ADAT");

                
                fileStream.Read(b, 0, b.Length);

                //Check bit 19
                BitArray bits = new BitArray(b);
                encryptionBit = (bool)bits[19];

                //Double Check
                UInt32 secondDWord = BitConverter.ToUInt32(b,0); 
                UInt32 encryptMask = 1<<19;            
                
                UInt32 encrypt = secondDWord & encryptMask;      
                if (encrypt == 0)
                {
                    if (encryptionBit != false)
                    {
                        Console.WriteLine("The process has had errenous values in file:" + file.FullName );
                    }
                    encryptionBit = false;
                }
                
            }
            AWCFileInfo awc = new AWCFileInfo() { path = file.FullName, bank = file.Directory.Name };
			//if (ADAT == false)
			//{
			//   awc.ecrypted = "Y";
			//}
			//else 
			if(!encryptionBit)
            {
                awc.ecrypted = "N";
            }
            else
            {
                awc.ecrypted = "Y";
            }
            
            return (awc.path + "," + awc.ecrypted + "," + awc.bank);
        }


    }
}
