﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataEncryptionChecker
{
    class Program
    {

        static void Main(string[] args)
        {
            if (args.Length == 2)
            {
                if (Directory.Exists(args[0]))
                {
                    try
                    {
                        if (!Directory.Exists(Path.GetDirectoryName(args[1])))
                        {
                            Directory.CreateDirectory(Path.GetDirectoryName(args[1]));
                        }
                        DataEncryptionChecker Dec = new DataEncryptionChecker(args[0], args[1]);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
                else
                {
                    Console.WriteLine("The directory provided in the first argument does not exist, please try again");
                }
            }
            else
            {
                Console.WriteLine("DataEncryptionChecker requires exactly 2 arguments: \n 1st arg is the path of the awc files \n 2nd arg is where you want the comma seperated values to go");
            }
        }
    }
}
