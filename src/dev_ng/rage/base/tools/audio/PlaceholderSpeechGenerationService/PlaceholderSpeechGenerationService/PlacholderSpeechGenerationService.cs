﻿using System;
using System.Diagnostics;
using System.IO;
using System.ServiceProcess;
using System.Text;
using System.Timers;

namespace PlaceholderSpeechGenerationService
{
    public partial class PlacholderSpeechGenerationService : ServiceBase
    {
        private const string EXE = ".exe";
        private const string BAT = ".bat";

        private string m_buildExecutable, m_rebuildExecutable;
        private DateTime m_nextBuildTime, m_nextRebuildTime;
        private Timer m_buildTimer, m_rebuildTimer;

        public PlacholderSpeechGenerationService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            if (args.Length == 4)
            {
                try
                {
                    const string BUILD = "Build";
                    const string REBUILD = "Rebuild";

                    var buildExecutable = args[0];
                    var rebuildExecutable = args[2];
                    if (CheckExecutable(buildExecutable, BUILD) &&
                        CheckExecutable(rebuildExecutable, REBUILD))
                    {
                        TimeSpan buildTime, rebuildTime;
                        if (CheckTime(args[1], BUILD, out buildTime) &&
                            CheckTime(args[3], REBUILD, out rebuildTime))
                        {
                            m_buildExecutable = buildExecutable;
                            m_rebuildExecutable = rebuildExecutable;

                            SetNextRunTimes(buildTime, rebuildTime);

                            var now = DateTime.Now;

                            var nextBuildInterval = m_nextBuildTime < now ? 1000 : (m_nextBuildTime - now).TotalMilliseconds;
                            m_buildTimer = new Timer(nextBuildInterval);
                            m_buildTimer.Elapsed += OnBuildTimerElapsed;
                            m_buildTimer.Start();

                            m_rebuildTimer = new Timer((m_nextRebuildTime - now).TotalMilliseconds);
                            m_rebuildTimer.Elapsed += OnRebuildTimerElapsed;
                            m_rebuildTimer.Start();
                        }
                    }
                }
                catch (Exception)
                {
                    EventLog.WriteEntry("Invalid arguments. They should be: <build_executable> <build_time> <rebuild_executable> <rebuild_time>.",
                                        EventLogEntryType.Error);
                    Stop();
                }
            }
            else
            {
                EventLog.WriteEntry("Incorrect number of arguments. They should be: <build_executable> <build_time> <rebuild_executable> <rebuild_time>.",
                                    EventLogEntryType.Error);
                Stop();
            }
        }

        protected override void OnStop()
        {
            if (m_buildTimer != null)
            {
                m_buildTimer.Stop();
                m_buildTimer.Dispose();
                m_buildTimer = null;
            }

            if (m_rebuildTimer != null)
            {
                m_rebuildTimer.Stop();
                m_rebuildTimer.Dispose();
                m_rebuildTimer = null;
            }
        }

        private void SetNextRunTimes(TimeSpan buildTime, TimeSpan rebuildTime)
        {
            var today = DateTime.Today;
            m_nextBuildTime = today + buildTime;

            var rebuildDayOffset = 7; // start out assuming today is Sunday
            switch (today.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    {
                        rebuildDayOffset = 6;
                        break;
                    }
                case DayOfWeek.Tuesday:
                    {
                        rebuildDayOffset = 5;
                        break;
                    }
                case DayOfWeek.Wednesday:
                    {
                        rebuildDayOffset = 4;
                        break;
                    }
                case DayOfWeek.Thursday:
                    {
                        rebuildDayOffset = 3;
                        break;
                    }
                case DayOfWeek.Friday:
                    {
                        rebuildDayOffset = 2;
                        break;
                    }
                case DayOfWeek.Saturday:
                    {
                        rebuildDayOffset = 1;
                        break;
                    }
            }
            m_nextRebuildTime = today + TimeSpan.FromDays(rebuildDayOffset) + rebuildTime;
        }

        private bool CheckExecutable(string executable, string executableType)
        {
            var extension = Path.GetExtension(executable).ToLower();
            if ((extension == EXE || extension == BAT) &&
                File.Exists(executable))
            {
                EventLog.WriteEntry(string.Format("{0} executable set to: {1}.", executableType, executable));
                return true;
            }
            EventLog.WriteEntry(string.Format("Invalid {0} executable specified: {1}.", executableType, executable),
                                EventLogEntryType.Error);
            Stop();
            return false;
        }

        private bool CheckTime(string timeString, string executableType, out TimeSpan time)
        {
            if (TimeSpan.TryParse(timeString, out time) &&
                time.TotalDays <= 1)
            {
                EventLog.WriteEntry(string.Format("{0} executable scheduled to run at: {1}.", executableType, time));
                return true;
            }
            EventLog.WriteEntry(
                string.Format("Invalid {0} executable time specified: {1}.", executableType, timeString),
                EventLogEntryType.Error);
            Stop();
            return false;
        }

        private void OnBuildTimerElapsed(object sender, ElapsedEventArgs e)
        {
            m_buildTimer.Stop();

            var currentBuildRun = m_nextBuildTime;
            switch (currentBuildRun.DayOfWeek)
            {
                case DayOfWeek.Saturday:
                    {
                        m_nextBuildTime = currentBuildRun + TimeSpan.FromDays(2);
                        break;
                    }
                case DayOfWeek.Sunday:
                    {
                        m_nextBuildTime = currentBuildRun + TimeSpan.FromDays(1);
                        break;
                    }
                default:
                    {
                        Run(m_buildExecutable);
                        m_nextBuildTime = currentBuildRun +
                                         TimeSpan.FromDays(currentBuildRun.DayOfWeek == DayOfWeek.Friday ? 3 : 1);
                        break;
                    }
            }
            EventLog.WriteEntry(string.Format("Next build scheduled for {0}.", m_nextBuildTime));

            m_buildTimer.Interval = (m_nextBuildTime - DateTime.Now).TotalMilliseconds;
            m_buildTimer.Start();
        }

        private void OnRebuildTimerElapsed(object sender, ElapsedEventArgs e)
        {
            m_rebuildTimer.Stop();

            Run(m_rebuildExecutable);
            m_nextRebuildTime = m_nextRebuildTime + TimeSpan.FromDays(7);

            EventLog.WriteEntry(string.Format("Next rebuild scheduled for {0}.", m_nextRebuildTime));

            m_rebuildTimer.Interval = (m_nextRebuildTime - DateTime.Now).TotalMilliseconds;
            m_rebuildTimer.Start();
        }

        private void Run(string executable)
        {
            StringBuilder output;
            var result = RunExe(out output, executable);
            EventLog.WriteEntry(output.ToString(), result ? EventLogEntryType.Information : EventLogEntryType.Error);
        }

        private static bool RunExe(out StringBuilder output, string executable)
        {
            var psi = new ProcessStartInfo(executable)
                          {
                              CreateNoWindow = true,
                              UseShellExecute = false,
                              RedirectStandardOutput = true,
                              WorkingDirectory = Path.GetDirectoryName(executable)
                          };

            var process = new Process {StartInfo = psi};
            process.Start();

            output = new StringBuilder();
            output.Append(Environment.NewLine);
            output.Append("***OUTPUT***");
            output.Append(Environment.NewLine);
            output.Append(process.StandardOutput.ReadToEnd());

            process.WaitForExit();
            return process.ExitCode == 0;
        }
    }
}