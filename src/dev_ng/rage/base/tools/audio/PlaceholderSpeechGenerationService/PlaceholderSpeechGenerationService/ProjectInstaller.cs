﻿using System.ComponentModel;
using System.Configuration.Install;

namespace PlaceholderSpeechGenerationService
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();
        }
    }
}