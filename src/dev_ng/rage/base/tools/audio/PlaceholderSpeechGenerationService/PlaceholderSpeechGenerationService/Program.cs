﻿using System.ServiceProcess;

namespace PlaceholderSpeechGenerationService
{
    internal static class Program
    {
        /// <summary>
        ///   The main entry point for the application.
        /// </summary>
        private static void Main()
        {
            var servicesToRun = new ServiceBase[] {new PlacholderSpeechGenerationService {CanStop = true}};
            ServiceBase.Run(servicesToRun);
        }
    }
}