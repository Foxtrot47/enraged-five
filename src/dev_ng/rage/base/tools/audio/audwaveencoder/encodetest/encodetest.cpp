// This is the main project file for VC++ application project 
// generated using an Application Wizard.

#include "stdafx.h"

#include "waveencoder_xenon.h"

#include "wavreader.h"

#using <mscorlib.dll>

using namespace audWaveEncoding;
using namespace System;

int _tmain()
{
    /*FileStream *xmaFileStream = File::Open(String::Concat(pakFolderPath, m_Name), FileMode::Create);
	BufferedStream *xmaFileBufStream = new BufferedStream(bankFileStream, xmaFileStream);
	BinaryWriter *xmaFile = new BinaryWriter(xmaFileBufStream);*/

	String*  arguments[] = Environment::GetCommandLineArgs();

	if(arguments->Count != 2)
	{
		Console::WriteLine(S"Usage: encodetest source\n");
		return -1;
	}

	String *sourceFilename = arguments->get_Item(1)->ToString()->ToUpper();
	if(!sourceFilename->EndsWith(".WAV"))
	{
		sourceFilename = String::Concat(sourceFilename, S".WAV");
	}

	audWavReader wavReader;
	if(!wavReader.Init((char *)(System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(sourceFilename).
		ToPointer()), true))
	{
		Console::WriteLine(String::Concat("Could not load source file - ", sourceFilename));
		return -1;
	}

	audFmtChunk *fmtChunk = (audFmtChunk *)wavReader.GetChunk(FMT_CHUNK_ID);
	audSmplChunk *smplChunk = (audSmplChunk *)wavReader.GetChunk(SMPL_CHUNK_ID);
	audRiffChunk *dataChunk = (audRiffChunk *)wavReader.GetChunk(DATA_CHUNK_ID);

	audFmtHeader *formatHeader = fmtChunk->GetFmtHeader();
	if(formatHeader->compressionCode != WAVE_FORMAT_PCM)
	{
		Console::WriteLine(String::Concat(S"Wave resource is not PCM - ", sourceFilename));
		return -1;
	}
	if(formatHeader->numChannels != 1)
	{
		Console::WriteLine(String::Concat(S"Wave resource is not mono - ", sourceFilename));
		return -1;
	}
	if(formatHeader->bitsPerSample != 16)
	{
		Console::WriteLine(String::Concat(S"Wave resource is not 16-bit - ", sourceFilename));
		return -1;
	}

	int loopStartOffsetSamples;
	if((smplChunk != NULL) && (smplChunk->GetSamplerHeader()->cSampleLoops == 1))
	{
		loopStartOffsetSamples = smplChunk->GetSamplerHeader()->Loops[0].dwStart;
	}
	else
	{
		loopStartOffsetSamples = -1;
	}

	//Create a suitable encoder, or simply fix endianness if the target platform does not require compression.
	audWaveEncoder *encoder = new audWaveEncoderXenon();
	if(encoder)
	{
		audWaveMetadataBase waveMetadata = {0};
		waveMetadata.waveData = dataChunk->GetData();
		waveMetadata.lengthBytes = dataChunk->GetChunkSize();
		waveMetadata.lengthSamples = waveMetadata.lengthBytes / 2;
		waveMetadata.sampleRate = (unsigned short)(formatHeader->sampleRate);
		void *waveMetadataOut = NULL;
		unsigned int waveMetadataOutLengthBytes = 0;
		void *waveSampleDataOut = NULL;
		unsigned int waveSampleDataOutLengthBytes = 0;

		if(!encoder->Encode(&waveMetadata, 0, &waveMetadataOut, waveMetadataOutLengthBytes, &waveSampleDataOut,
			waveSampleDataOutLengthBytes))
		{
			Console::WriteLine(String::Concat(S"Error compressing Wave file - ", sourceFilename));
			return -1;
		}

		delete[] waveMetadataOut;
		delete[] waveSampleDataOut;
	}

	return 0;
}
