//
// tools/audwaveencoder/waveencoder.h
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved.
//

#ifndef WAVE_ENCODER_H
#define WAVE_ENCODER_H

#include "memory.h"
#include "audiohardware/wavedefs.h"

using namespace rage;

namespace audWaveEncoding
{
	public __gc class audWaveMetadataBaseWrapper
	{
	public:
		audWaveMetadataBaseWrapper(void *metadata)
		{
			m_Metadata = (audWaveMetadataBase *)metadata;
		}
		~audWaveMetadataBaseWrapper()
		{
		}
		audWaveMetadataBase *GetMetadata(void)
		{
			return m_Metadata;
		}
	private:
		audWaveMetadataBase *m_Metadata;
	};

	//
	// PURPOSE
	//  Defines the basic interface inherited by all platform-specific Wave Encoder implementations.
	// SEE ALSO
	//  audWaveEncoderXenon
	//
	public __gc class audWaveEncoder
	{
	public:
		//
		// PURPOSE
		//  Encodes a buffer of raw wave data using an algorithm specific to the encoder type.
		// PARAMS
		//  waveMetadataIn				- A Wave metadata structure that specifies the parameters and content of the buffer
		//									of raw wave data to be encoded.
		//	waveSampleDataOffsetBytes	- The offset into the wave sample data at which the output data will ultimately be
		//									stored (for inclusion in the Wave Metadata).
		//	waveMetadataOut				- Used to return a buffer of arbitrary metadata. This buffer is allocated
		//									internally using new[] and should be deleted after use.
		//	waveMetadataOutLengthBytes	- Used to return the length of the output metadata, in bytes.
		//	waveSampleDataOut			- Used to return a buffer of encoded sample data. This buffer is allocated
		//									internally using new[] and should be deleted after use.
		//	waveMetadataOutLengthBytes	- Used to return the length of the output sample data, in bytes.
		//	compression					- The compression setting to be applied (1-100.)
		//	preloopPadding				- Used to return the extent of any zero padding added to a preloop (if present),
		//									in samples.
		// RETURNS
		//	Returns true if the encode process succeeded and false if the process failed.
		// SEE ALSO
		//  audWaveEncoderXenon
		//
		virtual bool Encode(audWaveMetadataBaseWrapper *waveMetadataIn, unsigned int waveSampleDataOffsetBytes,
			void **waveMetadataOut, unsigned int &waveMetadataOutLengthBytes, void **waveSampleDataOut,
			unsigned int &waveSampleDataOutLengthBytes, int compression, int &preloopPadding, unsigned int &outBitsInLastPacket);
		//
		// PURPOSE
		//  Resamples a buffer of raw wave data to the specified sample rate.
		// PARAMS
		//  waveMetadataIn		- A Wave metadata structure that specifies the parameters and content of the buffer of raw
		//							wave data to be resampled.
		//	targetSampleRate	- The target sample rate.
		// RETURNS
		//	Returns true if the resampling process succeeded and false if the process failed.
		virtual bool Resample(audWaveMetadataBaseWrapper *waveMetadataIn, int targetSampleRate);
		//
		// PURPOSE
		//  Aligns pre-loop and loop lengths via resampling and zero-padding.
		// PARAMS
		//	waveMetadata				- Contains a full set of metadata associated with the looping Wave to be aligned.
		//	alignmentSamples			- The size of the alignment block to be used, in samples.
		//	preloopPadding				- Used to return the extent of any zero padding added to a preloop (if present),
		//									in samples.
		// RETURNS
		//	Returns true if the alignment process succeeded and false if the process failed.
		//
		virtual bool AlignLoop(audWaveMetadataBase *waveMetadata, unsigned int alignmentSamples, int &preloopPadding);

	protected:
		//
		// PURPOSE
		//  Flips the endianness of an item of 16-bit data.
		// PARAMS
		//	value	- The 16-bit data to be endian-flipped.
		// RETURNS
		//	The endian-flipped data.
		//
		unsigned short FlipEndian(unsigned short value);
		//
		// PURPOSE
		//  Flips the endianness of an item of 32-bit data.
		// PARAMS
		//	value	- The 32-bit data to be endian-flipped.
		// RETURNS
		//	The endian-flipped data.
		//
		unsigned int FlipEndian(unsigned int value);
		//
		// PURPOSE
		//  Flips the endianness of an item of 64-bit data.
		// PARAMS
		//	value	- The 64-bit data to be endian-flipped.
		// RETURNS
		//	The endian-flipped data.
		//
		unsigned __int64 FlipEndian(unsigned __int64 value);

	};
}

#endif // WAVE_ENCODER_H
