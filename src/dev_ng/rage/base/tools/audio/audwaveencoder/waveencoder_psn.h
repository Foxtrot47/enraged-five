//
// tools/audwaveencoder/waveencoder_psn.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef WAVE_ENCODER_PSN_H
#define WAVE_ENCODER_PSN_H

#include "waveencoder.h"

using namespace System;

namespace audWaveEncoding
{
	//
	// PURPOSE
	//  The PS3 Wave Encoder.
	// SEE ALSO
	//  audWaveEncoder
	public __gc class audWaveEncoderPSN : public audWaveEncoder
	{
	public:
		audWaveEncoderPSN(String *fileFullLocalPath);

		virtual bool Encode(
			audWaveMetadataBaseWrapper *waveMetadataIn, 
			unsigned int waveSampleDataOffsetBytes,
			void **waveMetadataOut, 
			unsigned int &waveMetadataOutLengthBytes, 
			void **waveSampleDataOut,
			unsigned int &waveSampleDataOutLengthBytes, 
			int compression, 
			int &preloopPadding,
			unsigned int &outBitsInLastPacket);

	private:
		bool PreProcessLoop(audWaveMetadataBase *waveMetadata, int &preloopPadding);
		bool EncodeMp3(audWaveMetadataBase *waveMetadata, void **waveSampleDataOut,
			unsigned int &waveSampleDataOutLengthBytes, int compression, int sampleRate,
			unsigned int &samplesPerFrame);
		bool GenerateSeekTable(unsigned char *waveSampleDataOut, unsigned int waveSampleDataOutLengthBytes,
			unsigned int samplesPerFrame, unsigned short **seekTable, unsigned int &numFrames);
		void PostProcessMp3Frames(audWaveMetadataBase *waveMetadata, void **waveSampleDataOut,
			unsigned int &waveSampleDataOutLengthBytes, unsigned int samplesPerFrame,
			unsigned short *seekTable, unsigned int &numFrames, unsigned short **trimmedSeekTable);
		void WriteMp3File(audWaveMetadataBase *waveMetadata, void *waveSampleDataOut,
			unsigned int waveSampleDataOutLengthBytes);
	};
}

#endif // WAVE_ENCODER_PSN_H
