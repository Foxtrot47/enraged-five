#include "Windows.h"
class CXMAClean
{
public:
	HRESULT CXMAClean::CleanFile(bool ContainsXMAHeader, bool CleanFile, const TCHAR* File, DWORD *CountOfBitsInLastPacket, DWORD *CountOfBits);
	HRESULT CXMAClean::CleanStream(bool ContainsXMAHeader, bool CleanStream, BYTE* Data, DWORD Length, DWORD *CountOfBitsInLastPacket, DWORD *CountOfBits);
};

HRESULT STDAPICALLTYPE CopyBits(LPVOID pDest, DWORD cbDest, DWORD cbitDestOffset, 
                 LPVOID pSrc, DWORD cbitSrcOffset,
                 DWORD cbitsToCopy );
