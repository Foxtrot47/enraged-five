//
// tools/audwaveencoder/waveencoder_xenon.h
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved.
//

#ifndef WAVE_ENCODER_XENON_H
#define WAVE_ENCODER_XENON_H

#include "waveencoder.h"

namespace audWaveEncoding
{
	//
	// PURPOSE
	//  The Xbox 360 (Xenon) Wave Encoder.
	// NOTES
	//  Uses the XMAEncoder static library provided by Microsoft. See audWaveEncoder for generic encode
	//	documentation.
	// SEE ALSO
	//  audWaveEncoder
	//
	public __gc class audWaveEncoderXenon : public audWaveEncoder
	{
	public:
		//
		// PURPOSE
		//  Encodes a buffer of raw wave data using the XMAEncoder static library.
		// PARAMS
		//  waveMetadataIn				- A Wave metadata structure that specifies the parameters and content of the
		//									buffer of raw wave data to be encoded.
		//	waveSampleDataOffsetBytes	- The offset into the wave sample data at which the output data will ultimately be
		//									stored (for inclusion in the Wave Metadata).
		//	waveMetadataOut				- Used to return a buffer of arbitrary metadata. This buffer is allocated
		//									internally using new[] and should be deleted after use.
		//	waveMetadataOutLengthBytes	- Used to return the length of the output metadata, in bytes.
		//	waveSampleDataOut			- Used to return a buffer of encoded sample data. This buffer is allocated
		//									internally using new[] and should be deleted after use.
		//	waveMetadataOutLengthBytes	- Used to return the length of the output sample data, in bytes.
		//	compression					- The compression setting to be applied (1-100.)
		//	preloopPadding				- Used to return the extent of any zero padding added to a preloop (if present),
		//									in samples.
		// RETURNS
		//	Returns true if the encode process succeeded and false if the process failed.
		//
		bool Encode(audWaveMetadataBaseWrapper *waveMetadataIn, unsigned int waveSampleDataOffsetBytes,
			void **waveMetadataOut, unsigned int &waveMetadataOutLengthBytes, void **waveSampleDataOut,
			unsigned int &waveSampleDataOutLengthBytes, int compression, int &preloopPadding, unsigned int &outBitsInLastPacket);
	};
}

#endif // WAVE_ENCODER_XENON_H
