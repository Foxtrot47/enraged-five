﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Linq;

using rage;
using rage.ToolLib.Logging;
using rage.Transform;

namespace rage.SoundTransformer
{
    [CanTransform("Sound")]
    public class SoundTransformer : ITransformer
    {
        ILog m_Log;
        SoundValidator m_Validator;
        
        public bool Init(ILog log, params string[] args)
        {
            m_Log = log;
            m_Validator = new SoundValidator(log, args);
            

            log.Information("SoundTransformer initialised");            
            return true;
        }


        public TransformResult Transform(ILog log, XElement element)
        {
            TransformResult result = new TransformResult { Result = element };

            if (!VariableRouter.Instance.Transform(element))
            {
                return null;
            }
            
            if (!m_Validator.Validate(element))
            {
                return null;
            }

            return result;
        }

        public void Shutdown()
        {

        }
    }
}
