﻿namespace rage.SoundTransformer
{
    using System.Xml.Linq;

    using rage.Generator;
    using rage.ToolLib.Logging;
    using rage.Types;

    public class VariableRouterGenerator : IGenerator
    {
        #region IGenerator Members

        public bool Init(ILog log, string workingPath, audProjectSettings projectSettings, ITypeDefinitionsManager typeDefManager, params string[] args)
        {
           return true;
        }

        public XDocument Generate(ILog log, XDocument inputDoc)
        {
            return VariableRouter.Instance.Generate(inputDoc);
        }

        public void Shutdown()
        {
            
        }

        #endregion
    }
}
