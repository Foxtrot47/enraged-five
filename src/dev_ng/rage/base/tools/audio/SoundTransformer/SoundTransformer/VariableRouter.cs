﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

using rage.ToolLib.Logging;

namespace rage.SoundTransformer
{
    class VariableRouter
    {
        static VariableRouter sm_Instance = new VariableRouter();

        public static VariableRouter Instance
        {
            get { return sm_Instance; }
        }

        Dictionary<string, string> m_VariableMap;

        public VariableRouter()
        {
            m_VariableMap = new Dictionary<string, string>() 
            { 
                {"VolumeVariable", "PARAM_DESTINATION_VOLUME"},
                {"PitchVariable", "PARAM_DESTINATION_PITCH"},
                {"PanVariable", "PARAM_DESTINATION_PAN"},          
                {"FilterCutoffVariable", "PARAM_DESTINATION_LPF"},
                {"VolumeCurveScaleVariable", "PARAM_DESTINATION_ROLLOFF"},
                {"HPFCutoffVariable", "PARAM_DESTINATION_HPF"},
                {"ElevationPanVariable", "PARAM_DESTINATION_ELEVATIONPAN"},
                {"HapticSendVariable", "PARAM_DESTINATION_HAPTICSENDLEVEL"}
            };
        }

        IEnumerable<XElement> FindPopulatedVariables(XElement element)
        {
            return element.Elements().Where(x => m_VariableMap.ContainsKey(x.Name.LocalName) && !string.IsNullOrEmpty(x.Value));
        }

        public bool Transform(XElement element)
        {
            var variableFields = FindPopulatedVariables(element);
            bool hasPopulatedVars = variableFields.Any();
            bool needsRename = element.Name != "WrapperSound" && hasPopulatedVars;

            if (needsRename)
            {
                var nameAttr = element.Attribute("name");
                nameAttr.SetValue(ComputeRoutedName(nameAttr.Value));
            }

            if (element.Name == "WrapperSound" && hasPopulatedVars)
            {
                // map from base sound names to Wrapper Sound names
                MapWrapperVariables(element, variableFields);
            }

            return true;
        }

        XElement CreateRouterSound(XElement originalSound, IEnumerable<XElement> variableFields)
        {
            string originalName = originalSound.Attribute("name").Value;
            XElement router = new XElement("WrapperSound", new XAttribute("name", originalName));
            router.Add(variableFields); // at this stage we want the original, un-mapped names
            router.Add(new XElement("SoundRef", ComputeRoutedName(originalName)));
            return router;
        }

        IEnumerable<XElement> FixupFieldNames(IEnumerable<XElement> variableFields)
        {
            List<XElement> elements = new List<XElement>();

            XElement root = new XElement("VariableRefs");
            foreach (var x in variableFields)
            {
                string newName = m_VariableMap[x.Name.LocalName];
                root.Add(new XElement("Instance", 
                            new XElement("VR_Ref", x.Value),
                            new XElement("Destination", newName)));
            }

            elements.Add(root);
            return elements;            
        }

        void MapWrapperVariables(XElement s, IEnumerable<XElement> variableFields)
        {
            s.Add(FixupFieldNames(variableFields));
        }

        public XDocument Generate(XDocument doc)
        {
            var sounds = doc.Element("Objects").Elements();
            var newElements = new List<XElement>();

            foreach (var s in sounds)
            {
                if (s.Name != "WrapperSound")
                {
                    var variableFields = FindPopulatedVariables(s);
                    if (variableFields.Any())
                    {
                        newElements.Add(CreateRouterSound(s, variableFields));
                    }
                }
            }

            XElement root = new XElement("Objects", newElements);
            return new XDocument(root);
        }

        string ComputeRoutedName(string originalName)
        {
            return string.Concat(originalName, ".VR");
        }
    }
}
