﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

using rage.ToolLib.Logging;

namespace rage.SoundTransformer
{
    class SoundValidator
    {
        ILog m_Log;
        public SoundValidator(ILog log, params string[] args)
        {
            m_Log = log;
        }

        public bool Validate(XElement element)
        {
            // check that preDelayVariance is no bigger than preDelay
            var predelayElement = element.Element("preDelay");
            var predelayVarianceElement = element.Element("preDelayVariance");
            if (predelayVarianceElement != null && predelayElement != null)
            {
                int preDelayVariance = int.Parse(predelayVarianceElement.Value);
                int preDelay = int.Parse(predelayElement.Value);
                if (preDelayVariance > preDelay)
                {
                    element.Add(new XElement("Warning", "predelay variance is greater than predelay - clamping"));
                    predelayVarianceElement.SetValue(predelayElement.Value);
                }
            }

            var startOffsetElement = element.Element("StartOffset");
            var startOffsetVarianceElement = element.Element("StartOffsetVariance");
            var startOffsetPercentageElement = element.Element("StartOffsetPercentage");

            if (startOffsetElement != null)
            {
                int startOffset = int.Parse(startOffsetElement.Value);
                // check that percentage start offsets are within range
                if (startOffsetPercentageElement != null && startOffsetPercentageElement.Value == "yes")
                {
                    if (startOffset < 0 || startOffset > 100)
                    {
                        element.Add(new XElement("Warning", "percentage start offset outwith valid range - ignoring"));
                        startOffsetElement.Value = "0";
                    }
                }                

                if (startOffsetVarianceElement != null)
                {
                    int startOffsetVariance = int.Parse(startOffsetVarianceElement.Value);
                    if (startOffsetVariance > startOffset)
                    {
                        element.Add(new XElement("Warning", "Start offset variance is greater than start offset - clamping"));
                        startOffsetVarianceElement.SetValue(startOffsetElement.Value);
                    }
                }
            }

            return true;
        }
    }
}
