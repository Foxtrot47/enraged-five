﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using rage.ToolLib.CmdLine;
using System.Configuration;
using rage.ToolLib.WavesFiles;

namespace CodecComparer
{
    class Program
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private static void Resample(string inputFile, int sampleRate, string outputFile)
        {
            string outputDir = new FileInfo(outputFile).Directory.FullName;
            if (!Directory.Exists(outputDir)) Directory.CreateDirectory(outputDir);

            string soxPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["soxPath"]);

            try
            {
                logger.Trace(CmdRunner.run(soxPath, inputFile + " " + outputFile + " rate " + sampleRate));
            }
            catch (CmdRunner.CmdRunnerException ex)
            {
                logger.Error(ex.StdErr);
                throw ex;
            }
        }

        static int Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += (sender, eventArgs) =>
            {
                logger.Error(eventArgs.ExceptionObject.ToString());
                Environment.Exit(-1);
            };

            ProgramConfiguration config;
            try
            {
                config = new ProgramConfiguration(args);
            }
            catch (CommandLineConfiguration.ParameterException ex)
            {
                logger.Error(ex.Message);
                logger.Info(ex.Usage);
                return -1;
            }

            
            List<string> resampledAssets = new List<string>();

            foreach(string waveFile in Directory.EnumerateFiles(config.InputFolder, "*.*", SearchOption.TopDirectoryOnly).Where(f => f.EndsWith(".wav", StringComparison.OrdinalIgnoreCase)))
            {
                string wave32khz = waveFile.ToLower().Replace(config.InputFolder.ToLower(), config.OutputFolder).Replace(".wav", "32khz.wav");
                string wave48khz = waveFile.ToLower().Replace(config.InputFolder.ToLower(), config.OutputFolder).Replace(".wav", "48khz.wav");
                logger.Trace("Resample "+waveFile+" to 32khz and 48khz");
                Resample(waveFile, 32000, wave32khz);
                Resample(waveFile, 48000, wave48khz);
                resampledAssets.Add(wave32khz);
                resampledAssets.Add(wave48khz);
            }

            //run xma encoder
            //RunXmaEncoder(resampledAssets);

            List<string> resampledStereoAssets = CreateStereoWaves(resampledAssets);
            List<string> resampledMultiChannelAssets = CreateMultiChannelWaves(resampledAssets);

            /*
            //run Atrac9 encoder
            int[] atrac9_1ChBitrates = new[] {36, 48, 60, 72, 84, 96};
            int[] atrac9_2ChBitrates = new[] {72, 96, 120, 144, 168, 192 };
            int[] atrac9_8ChBitrates = new[] {336, 420, 504 };
            
            resampledAssets.ForEach(w => Atrac9Encode(w, atrac9_1ChBitrates));
            resampledStereoAssets.ForEach(w => Atrac9Encode(w, atrac9_2ChBitrates));
            resampledMultiChannelAssets.ForEach(w => Atrac9Encode(w, atrac9_8ChBitrates));
            */

            /*
            //run xwma ecoder
            int[] xwma_32khz1ChBitrates = new[] {20000};
            int[] xwma_48khz1ChBitrates = new[] { 32000, 48000 };
            int[] xwma_32khz2ChBitrates = new[] { 32000, 48000 };
            int[] xwma_48khz2ChBitrates = new[] { 48000, 64000, 96000, 160000, 192000 };

            resampledAssets.ForEach(w => XwmaEncode(w, w.Contains("32khz")?xwma_32khz1ChBitrates:xwma_48khz1ChBitrates));
            resampledStereoAssets.ForEach(w => XwmaEncode(w, w.Contains("32khz") ? xwma_32khz2ChBitrates : xwma_48khz2ChBitrates));
            //8 channels not supported by xwma
            //resampledMultiChannelAssets.ForEach(w => XwmaEncode(w, w.Contains("32khz") ? xwma_32khz8ChBitrates : xwma_48khz8ChBitrates));
            */

            /*
            //rum mp3 encoder
            int[] mp3_Bitrates = new[] { 64, 128, 192, 259, 320 };
            int[] mp3_vbrQuality = new[] {0, 2, 4, 6};
            
            resampledAssets.ForEach(w => Mp3Encode(w, mp3_Bitrates, mp3_vbrQuality));
            resampledStereoAssets.ForEach(w => Mp3Encode(w, mp3_Bitrates, mp3_vbrQuality));
            //8 channels not supported by mp3
            //resampledMultiChannelAssets.ForEach(w => Mp3Encode(w, mp3_Bitrates, mp3_vbrQuality));
            */

            /*
            //run ogg encoder
            int[] ogg_32khzBitrates = new[] { 64, 128, 160 };
            int[] ogg_48khzBitrates = new[] { 64, 128, 160, 192 };
            int[] ogg_8ChBitrates = new[] { 256, 320 };
            int[] ogg_Quality = new[] { 10, 8, 6, 4, 2 };

            resampledAssets.ForEach(w => OggEncode(w, w.Contains("32khz") ? ogg_32khzBitrates : ogg_48khzBitrates, ogg_Quality));
            resampledStereoAssets.ForEach(w => OggEncode(w, w.Contains("32khz") ? ogg_32khzBitrates : ogg_48khzBitrates, ogg_Quality));
            resampledMultiChannelAssets.ForEach(w => OggEncode(w, ogg_8ChBitrates, ogg_Quality));
            */

            //run aac encoder
            int[] aac_32khzBitrates = new[] { 64, 128, 152 };
            int[] aac_48khzBitrates = new[] { 64, 128, 160, 192 };
            int[] aac_Quality = new[] { 10, 100, 500 };

            resampledAssets.ForEach(w => AacEncode(w, w.Contains("32khz") ? aac_32khzBitrates : aac_48khzBitrates, aac_Quality));
            resampledStereoAssets.ForEach(w => AacEncode(w, w.Contains("32khz") ? aac_32khzBitrates : aac_48khzBitrates, aac_Quality));
            resampledMultiChannelAssets.ForEach(w => AacEncode(w, w.Contains("32khz") ? aac_32khzBitrates : aac_48khzBitrates, aac_Quality));

            return 0;
        }

        private static void AacEncode(string inputFile, int[] bitrates, int[] qualities)
        {
            string aacOutputDir = Path.Combine(new FileInfo(inputFile).Directory.FullName, "aac");
            string outputFileBaseName = Path.GetFileName(inputFile).Replace(".wav", "");
            AacEncode(inputFile, bitrates, qualities, aacOutputDir, outputFileBaseName);
        }

        private static void AacEncode(string inputFile, int[] bitrates, int[] qualities, string outputFolder, string outputFileBaseName)
        {
            if (!Directory.Exists(outputFolder)) Directory.CreateDirectory(outputFolder);
            string aacPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory,
                    ConfigurationManager.AppSettings["aacPath"]);

            foreach (int bitrate in bitrates)
            {
                foreach (int quality in qualities)
                {
                    string brOutputFile = Path.Combine(outputFolder, outputFileBaseName + "QualityPercentage"+quality+ "VBitrate" + bitrate + ".m4a");
                    Stopwatch sw = new Stopwatch();
                    sw.Start();
                    try
                    {
                        logger.Trace(CmdRunner.run(aacPath, "-q "+quality+" -b " + bitrate + " " + inputFile + " -o " + brOutputFile));
                    }
                    catch (CmdRunner.CmdRunnerException ex)
                    {
                        logger.Error(ex.StdErr);
                        throw ex;
                    }
                    sw.Stop();
                    logger.Info(Path.GetFileName(brOutputFile) + ", "+quality+"%, " + bitrate + ", " + sw.Elapsed + ", " +
                                (new FileInfo(brOutputFile)).Length);
                }
            }
        }


        private static void OggEncode(string inputFile, int[] bitrates, int[] qualities)
        {
            string oggOutputDir = Path.Combine(new FileInfo(inputFile).Directory.FullName, "ogg");
            string outputFileBaseName = Path.GetFileName(inputFile).Replace(".wav", "");
            OggEncode(inputFile, bitrates, qualities, oggOutputDir, outputFileBaseName);
        }

        private static void OggEncode(string inputFile, int[] bitrates, int[] qualities, string outputFolder, string outputFileBaseName)
        {
            if (!Directory.Exists(outputFolder)) Directory.CreateDirectory(outputFolder);
            string oggPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory,
                    ConfigurationManager.AppSettings["oggPath"]);

            foreach (int bitrate in bitrates)
            {
                string brOutputFile = Path.Combine(outputFolder, outputFileBaseName + "VBitrate" + bitrate + ".ogg");
                Stopwatch sw = new Stopwatch();
                sw.Start();
                try
                {
                    logger.Trace(CmdRunner.run(oggPath, "-b " + bitrate + " " + inputFile + " -o " + brOutputFile));
                }
                catch (CmdRunner.CmdRunnerException ex)
                {
                    logger.Error(ex.StdErr);
                    throw ex;
                }
                sw.Stop();
                logger.Info(Path.GetFileName(brOutputFile) + ", " + bitrate + ", " + sw.Elapsed + ", " + (new FileInfo(brOutputFile)).Length);
            }
            foreach (int quality in qualities)
            {
                string qualityOutputFile = Path.Combine(outputFolder, outputFileBaseName + "Quality" + quality + ".ogg");
                Stopwatch sw = new Stopwatch();
                sw.Start();
                try
                {
                    logger.Trace(CmdRunner.run(oggPath, "-q " + quality + " " + inputFile + " -o " + qualityOutputFile));
                }
                catch (CmdRunner.CmdRunnerException ex)
                {
                    logger.Error(ex.StdErr);
                    throw ex;
                }
                sw.Stop();
                logger.Info(Path.GetFileName(qualityOutputFile) + ", Quality" + quality + ", " + sw.Elapsed + ", " + (new FileInfo(qualityOutputFile)).Length);
            }
        }



        private static void Mp3Encode(string inputFile, int[] bitrates, int[] vbrQualities)
        {
            string mp3OutputDir = Path.Combine(new FileInfo(inputFile).Directory.FullName, "mp3");
            string outputFileBaseName = Path.GetFileName(inputFile).Replace(".wav", "");
            Mp3Encode(inputFile, bitrates, vbrQualities, mp3OutputDir, outputFileBaseName);
        }

        private static void Mp3Encode(string inputFile, int[] bitrates, int[] vbrQualities, string outputFolder, string outputFileBaseName)
        {
            if (!Directory.Exists(outputFolder)) Directory.CreateDirectory(outputFolder);
            string mp3Path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory,
                    ConfigurationManager.AppSettings["mp3Path"]);

            foreach (int bitrate in bitrates)
            {
                string cbrOutputFile = Path.Combine(outputFolder, outputFileBaseName + "CBR" + bitrate + ".mp3");
                Stopwatch sw = new Stopwatch();
                sw.Start();
                try
                {
                    logger.Trace(CmdRunner.run(mp3Path, "-b " + bitrate + " " + inputFile + " " + cbrOutputFile));
                }
                catch (CmdRunner.CmdRunnerException ex)
                {
                    logger.Error(ex.StdErr);
                    throw ex;
                }
                sw.Stop();
                logger.Info(Path.GetFileName(cbrOutputFile) + ", " + bitrate + ", " + sw.Elapsed + ", " + (new FileInfo(cbrOutputFile)).Length);
            }
            foreach (int bitrate in bitrates)
            {
                string abrOutputFile = Path.Combine(outputFolder, outputFileBaseName + "ABR" + bitrate + ".mp3");
                Stopwatch sw = new Stopwatch();
                sw.Start();
                try
                {
                    logger.Trace(CmdRunner.run(mp3Path, "--abr " + bitrate + " " + inputFile + " " + abrOutputFile));
                }
                catch (CmdRunner.CmdRunnerException ex)
                {
                    logger.Error(ex.StdErr);
                    throw ex;
                }
                sw.Stop();
                logger.Info(Path.GetFileName(abrOutputFile) + ", " + bitrate + ", " + sw.Elapsed + ", " + (new FileInfo(abrOutputFile)).Length);
            }
            foreach (int quality in vbrQualities)
            {
                string vbrOutputFile = Path.Combine(outputFolder, outputFileBaseName + "VBR" + quality + ".mp3");
                Stopwatch sw = new Stopwatch();
                sw.Start();
                try
                {
                    logger.Trace(CmdRunner.run(mp3Path, "-V" + quality + " " + inputFile + " " + vbrOutputFile));
                }
                catch (CmdRunner.CmdRunnerException ex)
                {
                    logger.Error(ex.StdErr);
                    throw ex;
                }
                sw.Stop();
                logger.Info(Path.GetFileName(vbrOutputFile) + ", VBR" + quality + ", " + sw.Elapsed + ", " + (new FileInfo(vbrOutputFile)).Length);
            }
        }

        private static void XwmaEncode(string inputFile, int[] bitrates)
        {
            string xwmaOutputDir = Path.Combine(new FileInfo(inputFile).Directory.FullName, "xwma");
            string outputFileBaseName = Path.GetFileName(inputFile).Replace(".wav", "");
            XwmaEncode(inputFile, bitrates, xwmaOutputDir, outputFileBaseName);
        }

        private static void XwmaEncode(string inputFile, int[] bitrates, string outputFolder, string outputFileBaseName)
        {
            if (!Directory.Exists(outputFolder)) Directory.CreateDirectory(outputFolder);
            foreach (int bitrate in bitrates)
            {
                string xwmaPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory,
                    ConfigurationManager.AppSettings["xwmaPath"]);

                string outputFile = Path.Combine(outputFolder, outputFileBaseName + "Bitrate" + bitrate + ".xwma");
                Stopwatch sw = new Stopwatch();
                sw.Start();
                try
                {
                    logger.Trace(CmdRunner.run(xwmaPath, " -b " + bitrate + " " + inputFile + " " + outputFile));
                }
                catch (CmdRunner.CmdRunnerException ex)
                {
                    logger.Error(ex.StdErr);
                    throw ex;
                }
                sw.Stop();
                logger.Info(Path.GetFileName(outputFile) + ", " + bitrate + ", " + sw.Elapsed + ", " + (new FileInfo(outputFile)).Length);
            }
        }

        private static void Atrac9Encode(string inputFile, int[] bitrates)
        {
            string atrac9OutputDir = Path.Combine(new FileInfo(inputFile).Directory.FullName, "atrac9");
            string outputFileBaseName = Path.GetFileName(inputFile).Replace(".wav", "");
            Atrac9Encode(inputFile, bitrates, atrac9OutputDir, outputFileBaseName);
        }

        private static void Atrac9Encode(string inputFile, int[] bitrates, string outputFolder, string outputFileBaseName)
        {
            if (!Directory.Exists(outputFolder)) Directory.CreateDirectory(outputFolder);
            foreach (int bitrate in bitrates)
            {
                string atrac9Path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory,
                    ConfigurationManager.AppSettings["atrac9Path"]);

                string outputFile = Path.Combine(outputFolder, outputFileBaseName + "Bitrate" + bitrate + ".at9");
                Stopwatch sw = new Stopwatch();
                sw.Start();
                try
                {
                    logger.Trace(CmdRunner.run(atrac9Path, " -e -br " + bitrate + " " + inputFile + " " + outputFile));
                }
                catch (CmdRunner.CmdRunnerException ex)
                {
                    logger.Error(ex.StdErr);
                    throw ex;
                }
                sw.Stop();
                logger.Info(Path.GetFileName(outputFile) + ", " + bitrate + ", "+sw.Elapsed+", " + (new FileInfo(outputFile)).Length);
            }
        }

        private static List<string> CreateStereoWaves(List<string> resampledAssets)
        {
            List<string> result = new List<string>();
            foreach (string rightChannelFile in resampledAssets.Where(w => w.Contains("right")))
            {
                string[] channels = new[] { rightChannelFile, rightChannelFile.Replace("right", "left") };
                string outputFileName = Path.GetFileName(rightChannelFile);
                outputFileName = outputFileName.Substring(0, outputFileName.LastIndexOf("right")) + outputFileName.Substring(outputFileName.LastIndexOf("right") + "right".Length).Replace(".wav", channels.Length+"Channels.wav");
                string resultFilename = Path.Combine(new FileInfo(rightChannelFile).Directory.FullName, "wav",
                    outputFileName);
                result.Add(resultFilename);
                CombineWaveChannels(channels, resultFilename);
            }
            return result;
        }

        private static List<string> CreateMultiChannelWaves(List<string> resampledAssets)
        {
            List<string> result = new List<string>();
            foreach (string firstRightChannelFile in resampledAssets.Where(w => w.Contains("_1_right")))
            {
                List<string> channels = new List<string>();
                string outputFileName = Path.GetFileName(firstRightChannelFile);
                outputFileName = outputFileName.Substring(0, outputFileName.LastIndexOf("_1_right")) + outputFileName.Substring(outputFileName.LastIndexOf("_1_right") + "_1_right".Length);
                for (int i = 1; i <= 12; i++)
                {
                    string rightChannel = firstRightChannelFile.Replace("_1_right", "_" + i + "_right");
                    string leftChannel = firstRightChannelFile.Replace("_1_right", "_" + i + "_left");
                    channels.Add(rightChannel);
                    channels.Add(leftChannel);
                    if (i % 4 == 0)
                    {
                        string resultFileName = Path.Combine(new FileInfo(firstRightChannelFile).Directory.FullName,
                            "wav", outputFileName.Replace(".wav", "") + i/4 + "_" + channels.Count() + "Channels.wav");
                        result.Add(resultFileName);
                        CombineWaveChannels(channels.ToArray(), resultFileName);
                        channels.Clear();
                    }
                }
            }
            return result;
        }

        private static void CombineWaveChannels(string[] channelFiles, string outputFile)
        {
            string outputDir = new FileInfo(outputFile).Directory.FullName;
            if (!Directory.Exists(outputDir)) Directory.CreateDirectory(outputDir);

            string soxPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["soxPath"]);
            string inputChannels = String.Join(" ", channelFiles);

            try
            {
                logger.Trace(CmdRunner.run(soxPath, "-M "+inputChannels + " " + outputFile));
            }
            catch (CmdRunner.CmdRunnerException ex)
            {
                logger.Error(ex.StdErr);
                throw ex;
            }
        }

        private static void RunXmaEncoder(List<string> resampledAssets)
        {
            int[] blockSizeSettings = new[] { 8, 512 };
            int[] compressionSettings = new[] { 10, 30, 50, 70, 90 };
            foreach (string inputFile in resampledAssets)
            {
                string outputDir = Path.Combine(new FileInfo(inputFile).Directory.FullName, "xma");
                string outputFileBaseName = Path.GetFileName(inputFile).Replace(".wav", "");
                XmaEncode(new string[] { inputFile }, blockSizeSettings, compressionSettings, outputDir, outputFileBaseName);
            }
            
            foreach (string rightChannelFile in resampledAssets.Where(w => w.Contains("right")))
            {
                string[] channels = new[] {rightChannelFile, rightChannelFile.Replace("right", "left")};
                string outputFileBaseName = Path.GetFileName(rightChannelFile);
                outputFileBaseName = outputFileBaseName.Substring(0, outputFileBaseName.LastIndexOf("right")) + outputFileBaseName.Substring(outputFileBaseName.LastIndexOf("right")+"right".Length).Replace(".wav", "");
                XmaEncode(channels, blockSizeSettings, compressionSettings, Path.Combine(new FileInfo(rightChannelFile).Directory.FullName, "xma"), outputFileBaseName);
            }
            
            foreach (string firstRightChannelFile in resampledAssets.Where(w => w.Contains("_1_right")))
            {
                List<string> channels = new List<string>();
                string outputFileBaseName = Path.GetFileName(firstRightChannelFile);
                outputFileBaseName = outputFileBaseName.Substring(0, outputFileBaseName.LastIndexOf("_1_right")) + outputFileBaseName.Substring(outputFileBaseName.LastIndexOf("_1_right") + "_1_right".Length).Replace(".wav", "");
                for (int i = 1; i <= 12; i++)
                {
                    string rightChannel = firstRightChannelFile.Replace("_1_right", "_" + i + "_right");
                    string leftChannel = firstRightChannelFile.Replace("_1_right", "_" + i + "_left");
                    channels.Add(rightChannel);
                    channels.Add(leftChannel);
                    if (i % 4 == 0)
                    {
                        XmaEncode(channels.ToArray(), blockSizeSettings, compressionSettings, Path.Combine(new FileInfo(firstRightChannelFile).Directory.FullName, "xma"), outputFileBaseName + i / 4 + "_");
                        channels.Clear();
                    }
                }
            }
        }

        private static void XmaEncode(string[] channelFiles, int[] blockSizes, int[] compressions, string outputFolder, string outputFileBaseName)
        {
            if (!Directory.Exists(outputFolder)) Directory.CreateDirectory(outputFolder);
            foreach (int blockSize in blockSizes)
            {
                foreach (int compression in compressions)
                {
                    string xmaPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory,
                        ConfigurationManager.AppSettings["xmaPath"]);

                    string outputFile = Path.Combine(outputFolder, outputFileBaseName+channelFiles.Length+"Channels" + "BlockSize" + blockSize + "Compression" + compression + ".xma");
                    string inputChannels = String.Join(" ", channelFiles);
                    Stopwatch sw = new Stopwatch();
                    sw.Start();
                    try
                    {
                        logger.Trace(CmdRunner.run(xmaPath, inputChannels + " /BlockSize " + blockSize + " /TargetFile " + outputFile + " /Quality " + compression));
                    }
                    catch (CmdRunner.CmdRunnerException ex)
                    {
                        logger.Error(ex.StdErr);
                        throw ex;
                    }
                    sw.Stop();
                    logger.Info(Path.GetFileName(outputFile) + ", " + blockSize + ", " + compression + ", " + sw.Elapsed + ", " + (new FileInfo(outputFile)).Length);
                }
            }
        }
    }
}
