﻿using System.IO;
using rage.ToolLib.CmdLine;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodecComparer
{
    public class ProgramConfiguration: CommandLineConfiguration
    {
        public ProgramConfiguration(string[] args)
            : base(args)
        {
        }

        private string outputFolder;
        [Description("Specify where the result is going to be saved")]
        public string OutputFolder 
        {
            get { return outputFolder; }
            set
            {
                if (!Directory.Exists(value))
                    throw new ParameterException(value + " is not a valid folder", this.GetUsage());
                outputFolder = value;
            }
        }

        private string inputFolder;
        [Description("All Waves in the input folder are getting processed")]
        public string InputFolder
        {
            get { return inputFolder; }
            set
            {
                if (!Directory.Exists(value))
                    throw new ParameterException(value + " is not a valid folder", this.GetUsage());
                inputFolder = value;
            }
        }
    }
}
