﻿#region

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Rockstar.Audio.Data;

#endregion

namespace Rockstar.Audio.Pipeline.EBU128
{
    public class LoudnessMeter
    {
        private double getChannelWeight(int channelIndex, List<ChannelMapping.ChannelPosition> channelMap)
        {
            ChannelMapping.ChannelPosition position = channelMap[channelIndex];
            switch (position)
            {
                case ChannelMapping.ChannelPosition.CENTER:
                case ChannelMapping.ChannelPosition.LEFT:
                case ChannelMapping.ChannelPosition.RIGHT:
                    return 1;
                case ChannelMapping.ChannelPosition.SURROUND_LEFT:
                case ChannelMapping.ChannelPosition.SURROUND_RIGHT:
                    return 1.41;
                default:
                    return 0;
            }
        }

        public LoudnessValues GetLoudnessValues(WaveSample waveFile)
        {
            LoudnessValues result = new LoudnessValues();

            List<ChannelMapping.ChannelPosition> channelMap =
                ChannelMapping.GetDefaultChannelPositions(waveFile.NumChannels);

            Ebur128LoudnessFilter filter = new Ebur128LoudnessFilter((int) waveFile.SampleRate);


            float[][] filteredChannelData = filter.Filter(waveFile, channelMap);
            double[] squaredSumSignal = new double[waveFile.NumSamples];
            for (int sampleIndex = 0; sampleIndex < waveFile.NumSamples; sampleIndex++)
            {
                squaredSumSignal[sampleIndex] = 0;
                for (int channel = 0; channel < waveFile.NumChannels; channel++)
                {
                    double channelWeight = getChannelWeight(channel, channelMap);
                    squaredSumSignal[sampleIndex] += (Math.Pow(filteredChannelData[channel][sampleIndex], 2)*
                                                      channelWeight);
                    //use unfiltered data for testing
                    //squaredSumSignal[sampleIndex] += (Math.Pow(waveFile.ChannelData[channel][sampleIndex], 2)*channelWeight);
                }
            }

            int momentaryFrameSize = (int) Math.Round(0.4*waveFile.SampleRate);
            int momentarySlidingSpeed = (int) Math.Round(0.1*waveFile.SampleRate); //75% overlap

            //TODO check if the momentary loudness should be calculated on the filtered signal or the unfiltered signal
            result.MomentaryLoudness = getLoudnessPerFrame(squaredSumSignal, momentaryFrameSize, momentarySlidingSpeed);

            List<double> squareMeanPerFrameMomentary = getSquareMeanPerFrame(squaredSumSignal, momentaryFrameSize,
                momentarySlidingSpeed);
            double integratedLoudnessThreshold = getRelativeThresholdForIntegratedLoudness(squareMeanPerFrameMomentary);
            result.IntegratedLoudness = getGatedLoudness(squareMeanPerFrameMomentary, integratedLoudnessThreshold);

            int shortTermFrameSize = (int) Math.Round(3.0*waveFile.SampleRate);
            int shortTermSlidingSpeed = (int) Math.Round(shortTermFrameSize/4.0); //75% overlap

            //TODO check if the short term loudness should be calculated on the filtered signal or the unfiltered signal
            result.ShortTermLoudness = getLoudnessPerFrame(squaredSumSignal, shortTermFrameSize, shortTermSlidingSpeed);
            double shortTermLoudnessThreshold = getRelativeThresholdForIntegratedLoudness(result.ShortTermLoudness);
            List<double> gatedShortTermLoudness = new List<double>();
            foreach (double loudness in result.ShortTermLoudness)
            {
                if (loudness >= shortTermLoudnessThreshold) gatedShortTermLoudness.Add(loudness);
            }

            List<double> squareMeanPerFrameShortTerm = getSquareMeanPerFrame(squaredSumSignal, shortTermFrameSize,
                shortTermSlidingSpeed);
            result.LoudnessRange = getLoudnessRange(squareMeanPerFrameShortTerm);

            return result;
        }

        private List<double> getLoudnessPerFrame(double[] squaredSummedUpSignal, int frameSize, int frameSlidingSpeed)
        {
            List<double> loudnessList = new List<double>();
            int framePosition = 0;
            while (framePosition + frameSize <= squaredSummedUpSignal.Length)
            {
                //signal power is mean of squared signal (signal has already been squared)
                double squaredMeanValue = 0;
                for (int i = framePosition; i < framePosition + frameSize; i++)
                {
                    squaredMeanValue += squaredSummedUpSignal[i];
                }
                double loudness = getLoudness(squaredMeanValue/frameSize);
                loudnessList.Add(loudness);
                framePosition += frameSlidingSpeed;
            }
            return loudnessList;
        }

        private List<double> getSquareMeanPerFrame(double[] squaredSummedUpSignal, int frameSize, int frameSlidingSpeed)
        {
            List<double> squareMeanList = new List<double>();
            int framePosition = 0;
            while (framePosition + frameSize <= squaredSummedUpSignal.Length)
            {
                double squaredMeanValue = 0;
                for (int i = framePosition; i < framePosition + frameSize; i++)
                {
                    squaredMeanValue += squaredSummedUpSignal[i];
                }
                squareMeanList.Add(squaredMeanValue/frameSize);
                framePosition += frameSlidingSpeed;
            }
            return squareMeanList;
        }

        private double getRelativeThresholdForIntegratedLoudness(IEnumerable squarMeanList)
        {
            //ignore values below -70 LKFS and computed mean of the rest 
            double sqareMeanSum = 0;
            int sumCount = 0;
            foreach (double squareMean in squarMeanList)
            {
                double loudness = getLoudness(squareMean);
                if (loudness >= -70.0)
                {
                    sqareMeanSum += squareMean;
                    sumCount++;
                }
            }

            //relative threshold = gated loudness - 10 
            double loudnessThreashold = getLoudness(sqareMeanSum/sumCount) - 10;
            return loudnessThreashold;
        }

        private double? getLoudnessRange(IEnumerable squarMeanList)
        {
            //ignore values below -70 LKFS and computed mean of the rest 
            double sqareMeanSum = 0;
            int sumCount = 0;
            List<double> absoluteGatedSquareMeanList = new List<double>();
            foreach (double squareMean in squarMeanList)
            {
                double loudness = getLoudness(squareMean);
                if (loudness >= -70.0)
                {
                    absoluteGatedSquareMeanList.Add(squareMean);
                    sqareMeanSum += squareMean;
                    sumCount++;
                }
            }

            //relative threshold = gated loudness - 20 
            double relativeLoudnessThreashold =
                getLoudness(absoluteGatedSquareMeanList.Sum()/absoluteGatedSquareMeanList.Count) - 20;

            List<double> relativeGatedLoudnessList = new List<double>();

            foreach (double squareMean in absoluteGatedSquareMeanList)
            {
                double loudness = getLoudness(squareMean);
                if (loudness >= relativeLoudnessThreashold)
                {
                    relativeGatedLoudnessList.Add(loudness);
                }
            }

            relativeGatedLoudnessList.Sort();
            if (relativeGatedLoudnessList.Count == 0) return null;
            //get range between lower 10% and upper 95%
            return relativeGatedLoudnessList[relativeGatedLoudnessList.Count*95/100] -
                   relativeGatedLoudnessList[relativeGatedLoudnessList.Count*10/100];
        }

        private double getGatedLoudness(IEnumerable squarMeanList, double gatingThreshold)
        {
            double sqareMeanSum = 0;
            int sumCount = 0;
            foreach (double squareMean in squarMeanList)
            {
                double loudness = getLoudness(squareMean);
                if (loudness >= gatingThreshold)
                {
                    sqareMeanSum += squareMean;
                    sumCount++;
                }
            }
            return getLoudness(sqareMeanSum/sumCount);
        }

        private double getLoudness(double sqareMeanValue)
        {
            return -0.691 + 10*Math.Log10(sqareMeanValue);
        }

        public class LoudnessValues
        {
            public double IntegratedLoudness;
            public double? LoudnessRange;
            public List<double> MomentaryLoudness = new List<double>();
            public List<double> ShortTermLoudness = new List<double>();
        }
    }
}