﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rockstar.Audio.Data;
using Wavelib;
using LoudnessMeter = Rockstar.Audio.Pipeline.EBU128.LoudnessMeter;
namespace Rockstar.Audio.Pipeline
{
    public class PeakNormalise : IPipelineStageProcessor
    {
        const double DEFAULT_THRESHOLD = -2.0;
        const string TOOL_IDENTIFIER = "peaknormalise-1.0";

        public string ToolIdentifier
        {
            get
            {
                return TOOL_IDENTIFIER;
            }
        }

        public const string DESIRED_PEAK_THRESHOLD = "DesiredPeakThreshold";
        public const string HEADROOM = "Headroom";
        public const string PEAK_THRESHOLD = "PeakThreshold";
        public const string INTEGRATED_LOUDNESS = "IntegratedLoudness";
        public const string LOUDNESS_RANGE = "LoudnessRange";
        public const string ADD_SILENCE_MARKERS = "AddSilenceMarkers";
        public const string SILENCE_VOLUME_THRESHOLD = "SilenceVolumeThreshold";
        public const string SILENCE_LENGTH_THRESHOLD = "SilenceLengthThreshold";


        public Package Process(Package package)
        {
            double desiredPeakLevel = package.Properties.GetValue(DESIRED_PEAK_THRESHOLD, DEFAULT_THRESHOLD);

            foreach (var packageFile in package.Files)
            {
                packageFile.LoadObject();
                if (!(packageFile.Object is PackageWaveObject)) continue;

                WaveSample waveFile = (packageFile.Object as PackageWaveObject).SampleData;

                LoudnessMeter.LoudnessValues loudness = (new LoudnessMeter()).GetLoudnessValues(waveFile);
                packageFile.Properties.SetValue(INTEGRATED_LOUDNESS, Math.Round(loudness.IntegratedLoudness, 3));
                if (loudness.LoudnessRange != null)
                {
                    packageFile.Properties.SetValue(LOUDNESS_RANGE, Math.Round(loudness.LoudnessRange.Value, 3));
                }
                // Allow threshold to be overwritten per-input
                double peakLevel = packageFile.Properties.GetValue(DESIRED_PEAK_THRESHOLD, desiredPeakLevel);

                SilenceMarkerParameters silenceMarkerParameters = null;
                if(packageFile.Properties.GetValue(ADD_SILENCE_MARKERS, false))
                {
                    silenceMarkerParameters = new SilenceMarkerParameters(
                        packageFile.Properties.GetValue(SILENCE_VOLUME_THRESHOLD, -64.0f),
                        packageFile.Properties.GetValue(SILENCE_LENGTH_THRESHOLD, 0.15f),
                        waveFile.SampleRate
                        );
                }

                var headroom = Normalise(waveFile, peakLevel, silenceMarkerParameters);
                
                packageFile.Properties.SetValue(HEADROOM, headroom);
                packageFile.Properties.SetValue(PEAK_THRESHOLD, peakLevel);
            }

            return package;
        }


        private class SilenceMarkerParameters
        {
            public int SileneceThreshold;
            public int SilenceRegionLengthThreshold;

            public SilenceMarkerParameters(double silenceThresholdVolumeDecibels, double silenceRegionLengthThresholdSeconds, uint sampleRate)
            {
                SileneceThreshold = (int)(short.MaxValue * Math.Pow(10.0, silenceThresholdVolumeDecibels / 20.0));
                SilenceRegionLengthThreshold = (int)(sampleRate * silenceRegionLengthThresholdSeconds);
            }
        }

        static double Normalise(WaveSample waveFile, double desiredPeakLevel, SilenceMarkerParameters silenceMarkerParameters=null)
        {
            double maxSampleLevel = 0.0;
            int currentSilenceRegionStart = -1;
           
            for (int channel = 0; channel < waveFile.NumChannels; channel++)
            {
                for (int sample = 0; sample < waveFile.NumSamples; sample++)
                {
                    double absSampleValue = Math.Abs(waveFile.ChannelData[channel][sample]);
                    maxSampleLevel = Math.Max(maxSampleLevel, absSampleValue);
                    
                    if (silenceMarkerParameters!=null)
                    {
                        //interleaved index
                        int sampleIndex = sample*waveFile.NumChannels + channel;
                        if (absSampleValue <= silenceMarkerParameters.SileneceThreshold)
                        {
                            if (currentSilenceRegionStart == -1) currentSilenceRegionStart = sampleIndex;
                        }
                        else if (currentSilenceRegionStart != -1)
                        {
                            if (sampleIndex - currentSilenceRegionStart > silenceMarkerParameters.SilenceRegionLengthThreshold)
                            {
                                waveFile.Markers.Add(new bwMarker(string.Format("SILENCE:{0}", (sampleIndex - currentSilenceRegionStart) / (float)waveFile.SampleRate), (uint)currentSilenceRegionStart));
                            }
                            currentSilenceRegionStart = -1;
                        }
                    }
                }
            }
            //Calculate scaling factor
            double maxNormalisedLevel = Math.Pow(10.0, desiredPeakLevel / 20.0);
            var scalingFactor = (float)(maxNormalisedLevel / maxSampleLevel);
                        
            for (int channel = 0; channel < waveFile.NumChannels; channel++)
            {
                for (int sample = 0; sample < waveFile.NumSamples; sample++)
                {
                    waveFile.ChannelData[channel][sample] *= scalingFactor;
                }
            }
            double headroom = Math.Log10(scalingFactor) * 20.0;
            return headroom;
        }
    }
}
