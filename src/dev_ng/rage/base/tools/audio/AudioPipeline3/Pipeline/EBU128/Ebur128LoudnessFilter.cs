﻿#region

using System;
using System.Collections.Generic;
using Rockstar.Audio.Data;

#endregion

namespace Rockstar.Audio.Pipeline.EBU128
{
    public class Ebur128LoudnessFilter
    {
        public double[] FilterA = new double[5];
        public double[] FilterB = new double[5];
        public int SampleRate { get; private set; }

        public Ebur128LoudnessFilter(int sampleRate)
        {
            SampleRate = sampleRate;
            // NOTE: ITU-R BS.1770-3 provides precomputed values for filter coefficients.
            // However, tests done by Music Technology Group on reference files revealed 
            // incorrect integrated loudness when using these values.
            // https://github.com/MTG/essentia/blob/master/src/algorithms/temporal/loudnessebur128filter.cpp#L79
            // Therefore, instead of hardcoding the coeffcients, 
            // we use a formula to compute them for any sample rate taken from: 
            // https://github.com/jiixyj/libebur128/blob/v1.0.2/ebur128/ebur128.c#L82

            double f0 = 1681.974450955533;
            double G = 3.999843853973347;
            double Q = 0.7071752369554196;

            double K = Math.Tan(Math.PI*f0/sampleRate);
            double Vh = Math.Pow(10.0, G/20.0);
            double Vb = Math.Pow(Vh, 0.4996667741545416);
            double a0 = 1.0 + K/Q + K*K;

            double[] filterPA = new double[3];
            filterPA[0] = 1;
            filterPA[1] = 2.0*(K*K - 1.0)/a0;
            filterPA[2] = (1.0 - K/Q + K*K)/a0;

            double[] filterPB = new double[3];
            filterPB[0] = (Vh + Vb*K/Q + K*K)/a0;
            filterPB[1] = 2.0*(K*K - Vh)/a0;
            filterPB[2] = (Vh - Vb*K/Q + K*K)/a0;

            f0 = 38.13547087602444;
            Q = 0.5003270373238773;
            K = Math.Tan(Math.PI*f0/sampleRate);

            double[] filterRA = new double[3];
            filterRA[0] = 1;
            filterRA[1] = 2.0*(K*K - 1.0)/(1.0 + K/Q + K*K);
            filterRA[2] = (1.0 - K/Q + K*K)/(1.0 + K/Q + K*K);

            double[] filterRB = new double[3];
            filterRB[0] = 1;
            filterRB[1] = -2;
            filterRB[2] = 1;


            FilterB[0] = filterPB[0]*filterRB[0];
            FilterB[1] = filterPB[0]*filterRB[1] + filterPB[1]*filterRB[0];
            FilterB[2] = filterPB[0]*filterRB[2] + filterPB[1]*filterRB[1] + filterPB[2]*filterRB[0];
            FilterB[3] = filterPB[1]*filterRB[2] + filterPB[2]*filterRB[1];
            FilterB[4] = filterPB[2]*filterRB[2];

            FilterA[0] = filterPA[0]*filterRA[0];
            FilterA[1] = filterPA[0]*filterRA[1] + filterPA[1]*filterRA[0];
            FilterA[2] = filterPA[0]*filterRA[2] + filterPA[1]*filterRA[1] + filterPA[2]*filterRA[0];
            FilterA[3] = filterPA[1]*filterRA[2] + filterPA[2]*filterRA[1];
            FilterA[4] = filterPA[2]*filterRA[2];
        }

        public float[][] Filter(WaveSample waveFile, List<ChannelMapping.ChannelPosition> channelMap)
        {
            double scalingFactor = 1.0f;
            float[][] resultChannelData = new float[waveFile.NumChannels][];
            for (int c = 0; c < waveFile.NumChannels; c++)
            {
                resultChannelData[c] = new float[waveFile.NumSamples];
            }

            double[,] FilterState = new double[5, 5];

            for (int channel = 0; channel < waveFile.NumChannels; channel++)
            {
                int ci = (int) channelMap[channel] - 1;
                if (ci < 0) continue;
                if (ci > 4) ci = 0; /* dual mono */
                for (int sample = 0; sample < waveFile.NumSamples; sample++)
                {
                    FilterState[ci, 0] = waveFile.ChannelData[channel][sample]/scalingFactor
                                         - FilterA[1]*FilterState[ci, 1]
                                         - FilterA[2]*FilterState[ci, 2]
                                         - FilterA[3]*FilterState[ci, 3]
                                         - FilterA[4]*FilterState[ci, 4];
                    resultChannelData[channel][sample] = (float) (
                        FilterB[0]*FilterState[ci, 0]
                        + FilterB[1]*FilterState[ci, 1]
                        + FilterB[2]*FilterState[ci, 2]
                        + FilterB[3]*FilterState[ci, 3]
                        + FilterB[4]*FilterState[ci, 4]
                        );
                    FilterState[ci, 4] = FilterState[ci, 3];
                    FilterState[ci, 3] = FilterState[ci, 2];
                    FilterState[ci, 2] = FilterState[ci, 1];
                    FilterState[ci, 1] = FilterState[ci, 0];
                }
            }

            return resultChannelData;
        }
    }
}