﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using rage.ToolLib.Encryption;
using Rockstar.Audio.Data;
using rage.ToolLib;

namespace Rockstar.Audio.Pipeline
{
    public class BankBuilder : IPipelineStageProcessor
    {
        public enum PackingTypes
        {
            // Chunks sorted by object, allowing a single contiguous read to grab all chunks for any given object
            WaveLoaded,
            // Chunks sorted by alignment requirement
            BankLoaded,
            // Chunks sorted by alignment, but ensuring that the global object data chunk is last
            Stream,
        }

        public class FilesPerAsset
        {
            public PackageFile dataFile = null;
            public PackageFile midFile = null;
            public PackageFileCollection otherFiles = new PackageFileCollection();
        }

        const string TOOL_IDENTIFIER = "bankbuilder-1.0";

        public const string BANK_NAME = "BankName";
        public const string BANK_PACKING_TYPE = "PackingType";
        public const string BANK_ISBIGENDIAN = "IsBigEndian";
        public const string BANK_PLATFORM = "Platform";
        public const string BANK_ENCRYPTIONKEY = "EncryptionKey";

        const string BANK_FILE_EXT = "awc";

        #region IPipelineStageProcessor Members

        public string ToolIdentifier
        {
            get { return TOOL_IDENTIFIER; }
        }

        public Package Process(Package package)
        {
            string bankName = package.Properties[BANK_NAME];
            string platform = package.Properties[BANK_PLATFORM];
            IEncrypter encrypter = EncrypterFactory.Create(package.Properties[BANK_ENCRYPTIONKEY]);

            PackageFile bankPackageFile = new PackageFile() { ObjectName = bankName, ObjectType = BANK_FILE_EXT };

            Dictionary<string, FilesPerAsset> filesPerAssetLookup = createFilesPerAssetLookupDictionary(package.Files);

            bool isBigEndian = package.Properties.GetValue(BANK_ISBIGENDIAN, false);
            Container container = null;
            switch (Enum<PackingTypes>.Parse(package.Properties[BANK_PACKING_TYPE], true))
            {
                case PackingTypes.BankLoaded:
                    container = new ContainerBankLoaded(encrypter.IsEncrypting);
                    break;
                case PackingTypes.Stream:
                    container = new ContainerStreamed(encrypter.IsEncrypting);
                    break;
                case PackingTypes.WaveLoaded:
                    container = new ContainerWaveLoaded(encrypter.IsEncrypting);
                    break;
            }
            if (container == null) throw new Exception("No container implementation found for " + package.Properties[BANK_PACKING_TYPE]);


            foreach (var filesPerAssetLookupEntry in filesPerAssetLookup)
            {
                ContainerObject obj = null;
                bool useGlobalObject = (container is ContainerStreamed) && filesPerAssetLookupEntry.Key.Equals(bankName);
                if (useGlobalObject)
                {
                    obj = (container as ContainerStreamed).getGlobalObject();
                }
                else
                {
                    obj = new ContainerObject(filesPerAssetLookupEntry.Key);
                    container.Objects.Add(obj);
                }

                //for non streaming objects the first two chunks should be the data and the mid chunk
                if (!(container is ContainerStreamed))
                {
                    if(filesPerAssetLookupEntry.Value.dataFile!=null)
                        obj.Chunks.Add(CreateChunk(filesPerAssetLookupEntry.Value.dataFile, platform, (container is ContainerWaveLoaded), encrypter));
                    if (filesPerAssetLookupEntry.Value.midFile != null)
                        obj.Chunks.Add(CreateChunk(filesPerAssetLookupEntry.Value.midFile, platform, (container is ContainerWaveLoaded), encrypter));
                }

                bool formatChunkAdded = false;
                foreach (PackageFile inputPackageFile in filesPerAssetLookupEntry.Value.otherFiles)
                {
                    string chunkType = inputPackageFile.ObjectType.ToUpper();
                    if (chunkType.Equals(ChunkType.FORMAT.ToString())) formatChunkAdded = true;
                    //only LIPSYNC64 is used for these platforms
                    if (chunkType.Equals(ChunkType.LIPSYNC) &&
                        (platform.ToUpper().Equals(Platform.XBOXONE.ToString()) ||
                         platform.ToUpper().Equals(Platform.PS4.ToString()) ||
                         platform.ToUpper().Equals(Platform.PC.ToString()) ||
                         platform.ToUpper().Equals(Platform.PS5.ToString()) ||
                         platform.ToUpper().Equals(Platform.XBOXSERIES.ToString()))) continue;

                    obj.Chunks.Add(CreateChunk(inputPackageFile, platform, (container is ContainerWaveLoaded), encrypter));
                }

                if (!(container is ContainerStreamed) && 
                    filesPerAssetLookupEntry.Value.midFile == null && 
                    !formatChunkAdded) 
                    throw new Exception("Wave missing format chunk - " + filesPerAssetLookupEntry.Key);

                //for streaming banks only the global object gets data and mid chunk
                //data and mid chunk should be the last chunks for the global object
                if (useGlobalObject)
                {
                    if(filesPerAssetLookupEntry.Value.dataFile!=null)
                        obj.Chunks.Add(CreateChunk(filesPerAssetLookupEntry.Value.dataFile, platform, (container is ContainerWaveLoaded), encrypter));
                    if (filesPerAssetLookupEntry.Value.midFile != null)
                        obj.Chunks.Add(CreateChunk(filesPerAssetLookupEntry.Value.midFile, platform, (container is ContainerWaveLoaded), encrypter));
                }

            }

            bankPackageFile.Object = new PackageWaveContainer(container);

            using (var memStream = new MemoryStream())
            {
                bankPackageFile.Object.SerializeToStream(memStream, false);
                if (memStream.Length >= 100000000 && !(container is ContainerStreamed))
                    throw new Exception(bankName + " container exceeds 100,000,000 bytes");
            }
            package.Files.Add(bankPackageFile);

            return package;
        }

        #endregion

        public static Chunk CreateChunk(PackageFile packageFile, string platform, bool waveloaded, IEncrypter encrypter)
        {
            packageFile.LoadObject();

            using (var memStream = new MemoryStream())
            {
                packageFile.Object.SerializeToStream(memStream, false);

                // Underlying stream is larger than written data
                var truncatedBuffer = new byte[memStream.Length];
                memStream.Seek(0, SeekOrigin.Begin);
                memStream.Read(truncatedBuffer, 0, truncatedBuffer.Length);

                // Wave-loaded banks need their wave chunks encrypted individually
                if (waveloaded) encrypter.Encrypt(truncatedBuffer, true);

                return Chunk.ChunkFactory(packageFile.ObjectType, truncatedBuffer, platform, waveloaded);
            }
        }

        private Dictionary<string, FilesPerAsset> createFilesPerAssetLookupDictionary(PackageFileCollection inputPackageFiles)
        {
            Dictionary<string, FilesPerAsset> filesPerAssetLookup = new Dictionary<string, FilesPerAsset>();
            foreach (var inputPackageFile in inputPackageFiles)
            {
                if (!filesPerAssetLookup.ContainsKey(inputPackageFile.ObjectName)) filesPerAssetLookup.Add(inputPackageFile.ObjectName, new FilesPerAsset());
                try
                {
                    switch (Enum<ChunkType>.Parse(inputPackageFile.ObjectType, true))
                    {
                        case ChunkType.DATA:
                            filesPerAssetLookup[inputPackageFile.ObjectName].dataFile = inputPackageFile;
                            break;
                        case ChunkType.MID:
                            filesPerAssetLookup[inputPackageFile.ObjectName].midFile = inputPackageFile;
                            break;
                        default:
                            filesPerAssetLookup[inputPackageFile.ObjectName].otherFiles.Add(inputPackageFile);
                            break;
                    }
                }
                catch (ArgumentException ex)
                {
                    //object type is not a supported chunk type - so the object will be created without a chunk
                }
            }
            return filesPerAssetLookup;
        }

    }
}
