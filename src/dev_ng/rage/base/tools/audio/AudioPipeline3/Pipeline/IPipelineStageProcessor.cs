﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rockstar.Audio.Pipeline
{
    public interface IPipelineStageProcessor
    {
        string ToolIdentifier { get; }
        Package Process(Package package);
    }
}
