﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using rage.ToolLib;
using Rockstar.Audio.Data;
using Wavelib;

namespace Rockstar.Audio.Pipeline
{
    public class WaveEncode : IPipelineStageProcessor
    {
        public const string COMPRESSION_TYPE = "Compression.Type";
        public const string COMPRESSION_QUALITY = "Compression.Quality";
        public const string COMPRESSION_BLOCKSIZE = "Compression.BlockSize";
        public const string ITERATIVE_ENCODING = "Compression.IterativeEncoding";
        public const string IS_STREAM = "IsStream";
        public const string HAS_LIPSYNC = "HasLipsync";
        public const string HEADROOM = PeakNormalise.HEADROOM;
        public const string FIRSTPEAKSAMPLE = PeakEnvelope.FIRSTPEAKSAMPLE;
        public const string PRESERVE_TRANSIENT = "PreserveTransient";

        const int DEFAULT_XMA_QUALITY = 50;
        const int DEFAULT_XMA_BLOCKSIZE = 2;
        const string DEFAULT_XMA_ITERATIVEENCODING = "disabled";

        public enum CompressionType
        {
            XMA2,
            MP3,
            PCM,
            ADPCM,
        }

        const string TOOL_IDENTIFIER = "waveencoder-1.0";

        #region IPipelineStageProcessor Members

        public string ToolIdentifier
        {
            get { return TOOL_IDENTIFIER; }
        }

        private PackageWaveFormatObject CreateFormatChunk(PackageFile inputFile, WaveEncoder.EncoderResult encodedData)
        {
            return CreateFormatChunk(inputFile, encodedData.Format, encodedData.LoopPointSample, encodedData.SampleRate, encodedData.LengthSamples, encodedData.LoopBegin, encodedData.LoopEnd,
                encodedData.PlayBegin, encodedData.PlayEnd);
        }

        PackageWaveFormatObject CreateFormatChunk(PackageFile inputFile, PackageWaveFormatObject.FormatType format, int loopPointSample, ushort sampleRate, uint lengthSamples, ushort loopBegin, ushort loopEnd, byte playBegin, ushort playEnd)
        {
            PackageWaveObject waveObject = inputFile.Object as PackageWaveObject;
            WaveSample inputWave = waveObject.SampleData;

            PackageWaveFormatObject formatChunk = new PackageWaveFormatObject();

            formatChunk.LoopPointSamples = loopPointSample;
            formatChunk.SampleRate = sampleRate;
            formatChunk.LengthSamples = lengthSamples;

            formatChunk.Headroom = (ushort)(inputFile.Properties.GetValue(HEADROOM, 0.0) * 100);
            formatChunk.FirstPeakSample = (ushort)(inputFile.Properties.GetValue(FIRSTPEAKSAMPLE, 0.0));
            formatChunk.HasCustomLipsync = inputFile.Properties.GetValue(HAS_LIPSYNC, false);

            formatChunk.Format = format;

            //TODO check if stream check & resetting sample rate is needed
            //bool isStream = inputFile.Properties.GetValue(PARAM_IS_STREAM, false);
            //formatChunk.LengthSamples = isStream? encodedData.LoopBegin+encodedData.LoopLength: encodedData.SamplesEncoded;
            //formatChunk.SampleRate = (ushort)encodedData.SampleRate;

            formatChunk.LoopBegin = loopBegin;
            formatChunk.LoopEnd = loopEnd;
            formatChunk.PlayBegin = playBegin;
            formatChunk.PlayEnd = playEnd;
            return formatChunk;
        }

        public Package Process(Package package)
        {
            List<PackageFile> chunkFiles = new List<PackageFile>();

            foreach (var inputFile in package.Files)
            {
                inputFile.LoadObject();
                var packageWaveObj = inputFile.Object as PackageWaveObject;
                if (packageWaveObj == null) continue;
             
                //check if peaknormalisation has been done
                if(!inputFile.Properties.ContainsKey(HEADROOM)) throw new Exception("Peaknormalisation has not been done for file "+inputFile.ObjectName+", please normalise before encoding");
                //check if peak envelope has been calculated
                if (!inputFile.Properties.ContainsKey(FIRSTPEAKSAMPLE)) throw new Exception("Peak envelope has not been calculated for file " + inputFile.ObjectName + ", please calculate peak envelope before encoding");

                PackageWaveFormatObject formatChunk = null;

                int compression = (int)inputFile.Properties.GetValue(COMPRESSION_QUALITY, DEFAULT_XMA_QUALITY);
                bool isStream = inputFile.Properties.GetValue(IS_STREAM, false);
                int loopStart = packageWaveObj.SampleData.LoopStart;
                int loopEnd = packageWaveObj.SampleData.LoopEnd;
                if (isStream)
                {
                    loopStart = 0;
                    loopEnd = packageWaveObj.SampleData.NumSamples;
                }

                WaveEncoder.EncoderResult encodingResult = null;
                switch (Enum<CompressionType>.Parse(inputFile.Properties[COMPRESSION_TYPE]))
                {
                    case CompressionType.XMA2:
                        int blocksize = (int) inputFile.Properties.GetValue(COMPRESSION_BLOCKSIZE, DEFAULT_XMA_BLOCKSIZE);
                        string iterativeEncoding = inputFile.Properties.GetValue(ITERATIVE_ENCODING, DEFAULT_XMA_ITERATIVEENCODING);
                            
                        if (!isStream &&
                            !string.Equals(iterativeEncoding, "disabled", StringComparison.OrdinalIgnoreCase))
                        {
                            //use iterative encoding
                            // iterative encoding                               
                            int lowerBound = compression - 3;
                            int upperBound = compression + 15;

                            // Allow parameters to be overridden via tag
                            if (!string.IsNullOrEmpty(iterativeEncoding))
                            {
                                var encodingParams = rage.ToolLib.Utility.ParseKeyValueString(iterativeEncoding);
                                if (encodingParams.ContainsKey("decrease"))
                                {
                                    lowerBound = compression - Math.Abs(int.Parse(encodingParams["decrease"]));
                                }
                                if (encodingParams.ContainsKey("increase"))
                                {
                                    upperBound = compression + Math.Abs(int.Parse(encodingParams["increase"]));
                                }
                            }

                            // Don't allow lowerBound to go below 1
                            lowerBound = Math.Max(1, lowerBound);
                            // Don't allow upperBound above 100
                            upperBound = Math.Min(100, upperBound);

                            TransformBlock<WaveEncoder.XMAEncoderParams, WaveEncoder.EncoderResult> xmaEncoderBlock = new TransformBlock<WaveEncoder.XMAEncoderParams, WaveEncoder.EncoderResult>(
                                xmaParameters => Pipeline.WaveEncoder.EncodeXMA(xmaParameters),
                                new ExecutionDataflowBlockOptions { MaxDegreeOfParallelism = Environment.ProcessorCount }
                            );
                            // pick highest encoding in the requested range without increasing size ...
                            int numEncodes = Math.Max(1, upperBound - lowerBound);
                            for (var i = 0; i < numEncodes; i++)
                            {
                                int currentCompression = lowerBound + i;
                                xmaEncoderBlock.Post(new WaveEncoder.XMAEncoderParams(
                                    packageWaveObj.SampleData.GetGameChannelMappedInterleavedShortData(),
                                    packageWaveObj.SampleData.NumChannels, 
                                    (int)packageWaveObj.SampleData.SampleRate,
                                    loopStart * packageWaveObj.SampleData.NumChannels,
                                    loopEnd * packageWaveObj.SampleData.NumChannels, 
                                    currentCompression,
                                    blocksize));
                            }
                            xmaEncoderBlock.Complete();
                            for (var i = 0; i < numEncodes; i++)
                            {
                                try
                                {
                                    WaveEncoder.EncoderResult encodedData = xmaEncoderBlock.Receive();
                                    if (encodingResult == null || encodedData.EncodedData.Length == encodingResult.EncodedData.Length)
                                        encodingResult = encodedData;
                                }
                                catch (InvalidOperationException)
                                {
                                    throw new Exception("Error encoding xma: " + xmaEncoderBlock.Completion.Exception.InnerException.Message, xmaEncoderBlock.Completion.Exception.InnerException);
                                }
                            }
                        }
                        else
                        {
                            //non iterative encoding
                            encodingResult = WaveEncoder.EncodeXMA(
                                packageWaveObj.SampleData.GetGameChannelMappedInterleavedShortData(), 
                                packageWaveObj.SampleData.NumChannels, 
                                (int)packageWaveObj.SampleData.SampleRate, 
                                loopStart * packageWaveObj.SampleData.NumChannels, 
                                loopEnd * packageWaveObj.SampleData.NumChannels, 
                                compression, 
                                blocksize);
                        }

                        if(encodingResult==null) throw new Exception("Encoder returned null!");

                        //add datachunk
                        chunkFiles.Add(new PackageFile() { ObjectName = inputFile.ObjectName, ObjectType = ChunkType.DATA.ToString(), Object = new PackageGenericObject(encodingResult.EncodedData) });
                        //add seektable
                        chunkFiles.Add(new PackageFile() { ObjectName = inputFile.ObjectName, ObjectType = ChunkType.SEEKTABLE.ToString(), Object = encodingResult.SeekTable });

                        formatChunk = CreateFormatChunk(inputFile, encodingResult);
                        break;
                    case CompressionType.MP3:
                        bool preserveTransient = inputFile.Properties.GetValue(PRESERVE_TRANSIENT, false);
                        encodingResult = WaveEncoder.EncodeMP3(
                            packageWaveObj.SampleData.GetGameChannelMappedInterleavedShortData(), 
                            packageWaveObj.SampleData.NumChannels, 
                            (int)packageWaveObj.SampleData.SampleRate, 
                            compression, 
                            loopStart * packageWaveObj.SampleData.NumChannels, 
                            preserveTransient);
                            
                        //add datachunk
                        chunkFiles.Add(new PackageFile() { ObjectName = inputFile.ObjectName, ObjectType = ChunkType.DATA.ToString(), Object = new PackageGenericObject(encodingResult.EncodedData) });
                        //add seektable
                        chunkFiles.Add(new PackageFile() { ObjectName = inputFile.ObjectName, ObjectType = ChunkType.SEEKTABLE.ToString(), Object = encodingResult.SeekTable });
                            
                        formatChunk = CreateFormatChunk(inputFile, encodingResult);
                            
                        break;
                    case CompressionType.PCM:
                        bwWaveFile waveFileForPcm =
                            bwWaveFile.Create16BitFromChannelData(packageWaveObj.SampleData.ChannelData,
                                packageWaveObj.SampleData.SampleRate, packageWaveObj.SampleData.LoopStart,
                                packageWaveObj.SampleData.LoopEnd, packageWaveObj.SampleData.Markers);
                        //add datachunk
                        chunkFiles.Add(new PackageFile() { ObjectName = inputFile.ObjectName, ObjectType = ChunkType.DATA.ToString(), Object = new PackageGenericObject(waveFileForPcm.Data.RawData) });
                        //no seektable chunk gets added for pcm

                        formatChunk = CreateFormatChunk(inputFile, PackageWaveFormatObject.FormatType.PCM_S16, packageWaveObj.SampleData.LoopStart, (ushort)packageWaveObj.SampleData.SampleRate, (uint)packageWaveObj.SampleData.NumSamples, (ushort)packageWaveObj.SampleData.LoopStart, (ushort)packageWaveObj.SampleData.LoopEnd, (byte)0, (ushort)0);
                        break;
                    case CompressionType.ADPCM:
                        bwWaveFile waveFileForADPCM =
                            bwWaveFile.Create16BitFromChannelData(packageWaveObj.SampleData.ChannelData,
                                packageWaveObj.SampleData.SampleRate, packageWaveObj.SampleData.LoopStart,
                                packageWaveObj.SampleData.LoopEnd, packageWaveObj.SampleData.Markers);
                        byte[] adpcmEncodedData = WaveEncoder.EncodeADPCM(waveFileForADPCM.Data.RawData);
                        //add datachunk
                        chunkFiles.Add(new PackageFile() { ObjectName = inputFile.ObjectName, ObjectType = ChunkType.DATA.ToString(), Object = new PackageGenericObject(adpcmEncodedData) });
                        //no seektable chunk gets added for pcm

                        formatChunk = CreateFormatChunk(inputFile, PackageWaveFormatObject.FormatType.ADPCM, packageWaveObj.SampleData.LoopStart, (ushort)packageWaveObj.SampleData.SampleRate, (uint)packageWaveObj.SampleData.NumSamples, (ushort)packageWaveObj.SampleData.LoopStart, (ushort)packageWaveObj.SampleData.LoopEnd, (byte)0, (ushort)0);
                        break;
                    default:
                        throw new Exception("Compression Type " + inputFile.Properties[COMPRESSION_TYPE]+" not supported by the WaveEncoder module.");
                }

                //add markerchunk
                if (packageWaveObj.SampleData.Markers.Count > 0)
                {
                    PackageWaveMarkerObject markerChunk = new PackageWaveMarkerObject();
                    foreach (bwMarker marker in packageWaveObj.SampleData.Markers)
                    {
                        markerChunk.Add(new PackageWaveMarkerObject.WaveMetadataMarker(marker.Name, marker.Position));
                    }
                    chunkFiles.Add(new PackageFile() { ObjectName = inputFile.ObjectName, ObjectType = ChunkType.MARKERS.ToString(), Object = markerChunk });
                }
                //add formatchunk
                chunkFiles.Add(new PackageFile() { ObjectName = inputFile.ObjectName, ObjectType = ChunkType.FORMAT.ToString(), Object = formatChunk });
            }

            foreach(PackageFile chunkFile in chunkFiles) package.Files.Add(chunkFile);
            return package;
        }

        #endregion
    }
}
