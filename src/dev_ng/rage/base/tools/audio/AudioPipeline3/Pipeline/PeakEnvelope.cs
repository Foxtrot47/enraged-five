﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rockstar.Audio.Data;

namespace Rockstar.Audio.Pipeline
{
    public class PeakEnvelope : IPipelineStageProcessor
    {

        const string TOOL_IDENTIFIER = "peakenvelope-1.0";
        const string PEAK_FILE_EXT = "peak";

        public const string FIRSTPEAKSAMPLE = "FirstPeakSample";
        
        #region IPipelineStageProcessor Members

        public string ToolIdentifier
        {
            get { return TOOL_IDENTIFIER; }
        }

        public Package Process(Package package)
        {
            List<PackageFile> peakFiles = new List<PackageFile>();

            foreach (PackageFile packageFile in package.Files)
            {
                packageFile.LoadObject();
                if (!(packageFile.Object is PackageWaveObject)) continue;

                ushort[] envelope;
                ushort firstPeakSample = ComputePeakEnvelope((packageFile.Object as PackageWaveObject).SampleData, false, out envelope);

                if (envelope == null) envelope = new ushort[0];

                var outputFile = new PackageFile() { ObjectName = packageFile.ObjectName, ObjectType = PEAK_FILE_EXT, Object = new PackageGenericObject(envelope) };
                packageFile.Properties[FIRSTPEAKSAMPLE] = firstPeakSample.ToString();
                peakFiles.Add(outputFile);
            }

            foreach(PackageFile peakFile in peakFiles) package.Files.Add(peakFile);
            return package;
        }

        ushort ComputePeakEnvelope(WaveSample waveData, bool isStreaming, out ushort[] envelope)
        {
            const int windowSize = 4096;
            double overallPeak = 0;
            List<double> peakSamples = new List<double>();
            int numPeakSamples = Math.Max(1, waveData.NumSamples / windowSize);
            for (int peakSampleIndex = 0; peakSampleIndex < numPeakSamples; peakSampleIndex++)
            {
                double maxSampleLevel = 0;
                for (int i = 0; i < windowSize; i++)
                {
                    var currentSampleIndex = i + peakSampleIndex * windowSize;
                    if (currentSampleIndex >= waveData.NumSamples)
                    {
                        break;
                    }
                    for (int channelIndex = 0; channelIndex > waveData.NumChannels; channelIndex++)
                    {
                        var currentSample = waveData.ChannelData[channelIndex][currentSampleIndex];
                        var absValue = Math.Abs(currentSample);
                        maxSampleLevel = Math.Max(maxSampleLevel, absValue);
                    }
                    
                    overallPeak = Math.Max(maxSampleLevel, overallPeak);
                }
                peakSamples.Add(maxSampleLevel);
            }

            // Don't generate peak file for streams
            bool generatePeakData = peakSamples.Count >= 2 && !isStreaming;
            if (generatePeakData)
            {
                envelope = new ushort[peakSamples.Count - 1];

                // Don't repeat the first sample; start from index 1
                for (int i = 1; i < peakSamples.Count; i++)
                {
                    envelope[i - 1] = (ushort)(peakSamples[i] * ushort.MaxValue);
                }
            }
            else
            {
                envelope = null;
            }

            // If the full peak table is turned off for this asset (for example with streams) then store the highest
            // peak, not the first, in the header.
            return (ushort)(ushort.MaxValue * (generatePeakData ? peakSamples[0] : overallPeak));            
        }
        #endregion
    }
}
