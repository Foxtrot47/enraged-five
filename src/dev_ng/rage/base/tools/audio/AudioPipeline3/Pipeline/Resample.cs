﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rockstar.Audio.Data;

namespace Rockstar.Audio.Pipeline
{
    public class Resample : IPipelineStageProcessor
    {
        #region IPipelineStageProcessor Members

        const string TOOL_IDENTIFIER = "resampler-1.0";
        public string ToolIdentifier
        {
            get { return TOOL_IDENTIFIER; }
        }

        public const string TARGETED_SAMPLERATE = "TargetedSampleRate";
        public const string LOOP_ALIGNMENT = "TargetedLoopAlignment";
        public const string RESAMPLED_SAMPLE_RATE = "SampleRateAfterResampling";
        public const string NUMBER_OF_SAMPLES = "NumberOfSamples";

        public Package Process(Package package)
        {
            foreach(var packageFile in package.Files)
            {
                packageFile.LoadObject();
                if (!(packageFile.Object is PackageWaveObject)) continue;

                var inputSample = (packageFile.Object as PackageWaveObject).SampleData;

                double targetSampleRate = packageFile.Properties.GetValue(TARGETED_SAMPLERATE, (double)inputSample.SampleRate);
                double loopAlignment = packageFile.Properties.GetValue(LOOP_ALIGNMENT, 1.0);

                double ratio = targetSampleRate / (double)inputSample.SampleRate;

                // if requested, resample loop to required alignment - zero-pad pre/post loop as required
                int prerollSamples = 0;
                int postrollSamples = 0;
                int loopStart = inputSample.LoopStart, loopEnd = inputSample.LoopEnd;
                if (inputSample.IsLooping && loopAlignment != 1.0)
                {
                    int loopAlignSamples = (int)loopAlignment;
                    // current loop length scaled by requested ratio
                    var currentLoopLength = (int)(((inputSample.LoopEnd - inputSample.LoopStart) + 1) * ratio + 0.5);
                    var extraLoopSamplesRequired = loopAlignSamples - (currentLoopLength % loopAlignSamples);
                    if (extraLoopSamplesRequired != loopAlignSamples)
                    {
                        // Calculate a new resampling ratio based on the desired loop length
                        var newRatio = (currentLoopLength + extraLoopSamplesRequired) / (double)(loopEnd - loopStart + 1);                        
                        targetSampleRate *= newRatio / ratio;
                        ratio = newRatio;
                    }

                    var preloopLen = (int)(loopStart * ratio + 0.5);
                    prerollSamples = loopAlignSamples - (preloopLen % loopAlignSamples);
                    if (prerollSamples == loopAlignSamples)
                    {
                        prerollSamples = 0;
                    }
                                        
                    var postLoopLen = (int)(ratio * (inputSample.NumSamples - (loopEnd+1)) + 0.5);
                    postrollSamples = loopAlignSamples - (postLoopLen % loopAlignSamples);
                    if (postrollSamples == loopAlignSamples)
                    {
                        postrollSamples = 0;
                    }

                    loopStart = (int)(loopStart * ratio + 0.5);
                    loopEnd = (int)(loopEnd * ratio + 0.5);
                    loopStart += prerollSamples;
                    loopEnd += prerollSamples;
                }
                else if (loopAlignment != 1.0)
                {
                    // zero-pad one-shots as necessary to meet alignment requirement
                    
                    int loopAlignSamples = (int)loopAlignment;
                    // current length scaled by requested ratio
                    var currentLength = (int)(inputSample.NumSamples * ratio + 0.5);
                    var extraSamplesRequired = loopAlignSamples - (currentLength % loopAlignSamples);

                    postrollSamples = extraSamplesRequired == loopAlignSamples ? 0 : extraSamplesRequired;
                }

                float[][] resampledChannelData = new float[inputSample.NumChannels][];
                for (int channelIndex = 0; channelIndex > inputSample.NumChannels; channelIndex++)
                {
                    resampledChannelData[channelIndex] = Resampler.Process(inputSample.ChannelData[0], ratio, (int)(targetSampleRate), prerollSamples, postrollSamples);
                }
                
                // Update loop points for new sample rate
                if (loopEnd >= resampledChannelData[0].Length || !inputSample.IsLooping)
                {
                    loopEnd = resampledChannelData[0].Length - 1;
                }

                var outputSample = new WaveSample((uint)targetSampleRate, resampledChannelData, loopStart, loopEnd);

                // Update marker positions for new sample rate
                foreach (var m in inputSample.Markers)
                {
                    var resampledPos = (int)(m.Position * ratio + 0.5);
                    if (resampledPos >= outputSample.NumSamples)
                    {
                        resampledPos = outputSample.NumSamples - 1;
                    }
                    outputSample.Markers.Add(new Wavelib.bwMarker(m.Name, (uint)resampledPos));
                }

                packageFile.Object = new PackageWaveObject(outputSample);
                packageFile.Properties.SetValue(RESAMPLED_SAMPLE_RATE, targetSampleRate);
                packageFile.Properties.SetValue(NUMBER_OF_SAMPLES, outputSample.NumSamples);
            }

            return package;
        }

        #endregion
    }
}
