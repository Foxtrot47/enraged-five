#include <string>
public class LameWrapper
{
	public:
		static void EncodeBuffer(unsigned char*& encodeBuffer, unsigned int& encodeBufferSize, int numChannels, int samplesPerFrame, int compression, int inputLength, int sampleRate, short* inputSamples);
		static void DeleteBuffer(void *buffer);

};

public struct Error
{
	explicit Error(std::string const& message) : message_(message) { }
	char const* what() const throw() { return message_.c_str(); }

private:
	std::string message_;
};