﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using rage.ToolLib;
using Wavelib;
using UnmanagedWaveEncoder = WaveEncoder;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            var encoder = new UnmanagedWaveEncoder.MP3Encoder();

            bwWaveFile waveFile = new bwWaveFile("C:\\Users\\ben.durrenberger\\Desktop\\dragdroptest\\newMP3Encoder\\RESIDENT\\FEET_MATERIALS\\M1F1-float32-AFsp.wav", false);
            float[][] channelData = waveFile.Data.CreateFloatArray();

            var convertedInput = (from x in channelData[0] select (short)Utility.DitherAndRoundSample16Bit(x * short.MaxValue)).ToArray<short>();
            int loopStart = -1;
            if (waveFile.IsLooping())
                loopStart = (int)waveFile.Sample.Loops[0].Start;

            encoder.EncodeMonoBuffer(convertedInput, 20, (int)waveFile.Format.SampleRate, loopStart, true);


            byte[] data = encoder.GetEncodedData();
            using (var fs = new FileStream("C:\\Users\\ben.durrenberger\\Desktop\\dragdroptest\\newMP3Encoder\\RESIDENT\\FEET_MATERIALS\\WOOD_SOLID_WALK_HEEL_04.WAV.managed.mp3", FileMode.Create, FileAccess.Write))
            {
                //endianness is handled in the bankbuilder so save all files as little endian
                fs.Write(data, 0, data.Length);
            }
            
            short[] seekTable = encoder.GetSeekTable();
            using (var fs = new FileStream("C:\\Users\\ben.durrenberger\\Desktop\\dragdroptest\\newMP3Encoder\\RESIDENT\\FEET_MATERIALS\\WOOD_SOLID_WALK_HEEL_04.WAV.managed.seektable", FileMode.Create, FileAccess.Write))
            {
                //endianness is handled in the bankbuilder so save all files as little endian
                byte[] Data = new byte[seekTable.Length * 2];
                using (var ms = new MemoryStream(Data))
                {
                    foreach (var b in seekTable)
                    {
                        ms.Write(BitConverter.GetBytes(b), 0, 2);
                    }
                }
                fs.Write(Data, 0, Data.Length);
            }
        }
    }
}
