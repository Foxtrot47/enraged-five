// ManagedMP3Encoder.h

#pragma once

using namespace System;

namespace WaveEncoder {

	public ref class MP3Encoder
	{
	public:
		static array<unsigned char> ^MP3Encoder::EncodePcmData(array<short> ^inputSamples, int numChannels, int compression, int sampleRate, int samplesPerFrame);
	};
}