#include "stdafx.h"
#include "lame.h"
#include "lame_global_flags.h"
#include "LameWrapper.h"
#include "memory.h"
#include "math.h"
#include <string>

using namespace System::Collections::Generic;
using namespace System::IO;

void LameWrapper::EncodeBuffer(unsigned char*& encodeBuffer, unsigned int& encodeBufferSize, int numChannels, int samplesPerFrame, int compression, int inputLength, int sampleRate, short* inputSamples)
{
	if(numChannels > 2) throw Error(std::to_string(numChannels)+" channels found. Only mono and stereo encoding is supported");
	//Map compression setting to MP3 VBR quality - 0(best) to 9(worst).
	int vbrQuality = 10 - (int)ceil((float)compression / 10.0f);
	if(vbrQuality == 10)
	{
		vbrQuality = 9; //Compression setting 0 will give the same compression as 1 to 10.
	}

	lame_global_flags *globalFlags = lame_init();
	lame_set_bWriteVbrTag(globalFlags, 0); //No header.
	lame_set_num_samples(globalFlags, inputLength);

	
	lame_set_in_samplerate(globalFlags, sampleRate);
	lame_set_num_channels(globalFlags, numChannels);
	//lame_set_out_samplerate(globalFlags, mp3SampleRate);
	lame_set_quality(globalFlags, 0); //Use a better (but slower) quality compression algorithm.
	lame_set_lowpassfreq(globalFlags, -1); //Disable low-pass filtering.
	lame_set_highpassfreq(globalFlags, -1); //Disable high-pass filtering.
	lame_set_disable_reservoir(globalFlags, 1); //Disable the bit reservoir to allow seeking/looping to an arbitrary frame.
	//set vbr settings
	lame_set_VBR_q(globalFlags, vbrQuality);
	lame_set_VBR(globalFlags, vbr_default);

	lame_init_params(globalFlags);


	if(0 == lame_get_version(globalFlags))
	{
		// For MPEG-II, only 576 samples per frame per channel
		if(samplesPerFrame != 576)
		{
			throw Error("576 samples per frame per channel expected for MPEG-II but found "+std::to_string(samplesPerFrame)+" samples per frame");
		}
	}
	else
	{
		// For MPEG-I, 1152 samples per frame per channel
		if(samplesPerFrame != 1152)
		{
			throw Error("1152 samples per frame per channel expected for MPEG-I but found "+std::to_string(samplesPerFrame)+" samples per frame");
		}
	}

	int encodedBufferBytes = (5 * inputLength / 4) + 7200;
	encodeBuffer = new unsigned char [encodedBufferBytes];

	int bytesEncoded;
	encodeBufferSize = 0;
	
	for(unsigned int inputSampleOffset=0; inputSampleOffset<inputLength; inputSampleOffset+=(samplesPerFrame*numChannels))
	{
		
		if(numChannels==1)
		{
			bytesEncoded = lame_encode_buffer(globalFlags, inputSamples + inputSampleOffset, NULL, samplesPerFrame, encodeBuffer + encodeBufferSize, encodedBufferBytes - encodeBufferSize);
		}
		else
		{
			bytesEncoded = lame_encode_buffer_interleaved(globalFlags, inputSamples + inputSampleOffset, samplesPerFrame, encodeBuffer + encodeBufferSize, encodedBufferBytes - encodeBufferSize);
		}

		if(bytesEncoded < 0)
		{
			lame_close(globalFlags);
			delete[] encodeBuffer;
			throw Error(("LAME error encoding MP3: lame_encode_flush returned "+std::to_string(bytesEncoded)));
		}

		encodeBufferSize += bytesEncoded;
	}

	bytesEncoded = lame_encode_flush(globalFlags, encodeBuffer + encodeBufferSize, encodedBufferBytes - encodeBufferSize);

	if(bytesEncoded < 0)
	{
		lame_close(globalFlags);
		delete[] encodeBuffer;
		throw Error(("LAME error encoding MP3: lame_encode_flush returned "+std::to_string(bytesEncoded)));
	}

	encodeBufferSize += bytesEncoded;
	lame_close(globalFlags);
}

void LameWrapper::DeleteBuffer(void *buffer)
{
	delete[] buffer;
}

