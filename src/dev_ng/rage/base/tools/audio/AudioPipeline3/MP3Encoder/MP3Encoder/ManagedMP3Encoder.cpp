#include "stdafx.h"
#include "ManagedMP3Encoder.h"
#include "MP3Encoder.h"
#include "LameWrapper.h"

using namespace System;
using namespace System::Runtime::InteropServices;

namespace WaveEncoder
{	
	array<unsigned char> ^MP3Encoder::EncodePcmData(array<short> ^inputSamples, int numChannels, int compression, int sampleRate, int samplesPerFrame)
	{
		pin_ptr<short> inputSamplesPinned = &inputSamples[inputSamples->GetLowerBound(0)];

		unsigned int encodedDataSize = -1;
		unsigned char* encodedData;
		try
		{
			LameWrapper::EncodeBuffer(encodedData, encodedDataSize, numChannels, samplesPerFrame, compression, inputSamples->Length, sampleRate, inputSamplesPinned);
			array<unsigned char> ^managedEncodedData = gcnew array<unsigned char>(encodedDataSize);
			Marshal::Copy( (IntPtr)encodedData, managedEncodedData, 0, encodedDataSize);
			LameWrapper::DeleteBuffer(encodedData);
			return managedEncodedData;
		}
		catch(Error const& err){
			throw gcnew Exception(gcnew String(err.what()));
		}
	}
}