﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using rage.ToolLib;
using UnmanagedWaveEncoder = WaveEncoder;

namespace Rockstar.Audio.Pipeline
{
    public class WaveEncoder
    {
        public class EncoderResult
        {
            public EncoderResult(byte[] encodedData, PackageSeekTableObject seekTable, PackageWaveFormatObject.FormatType format, ushort sampleRate, int loopPointSample, uint lengthSamples, Byte playBegin, ushort playEnd, ushort loopBegin, ushort loopEnd)
            {
                EncodedData = encodedData;
                SeekTable = seekTable;
                Format = format;
                LoopPointSample = loopPointSample;
                SampleRate = sampleRate;
                LengthSamples = lengthSamples;
                PlayBegin = playBegin;
                PlayEnd = playEnd;
                LoopBegin = loopBegin;
                LoopEnd = loopEnd;
            }

            public readonly byte[] EncodedData;
            public readonly PackageSeekTableObject SeekTable;
            public readonly PackageWaveFormatObject.FormatType Format;
            public readonly int LoopPointSample;
            public readonly ushort SampleRate;
            public readonly uint LengthSamples;
            public readonly Byte PlayBegin;
            public readonly ushort PlayEnd;
            public readonly ushort LoopBegin;
            public readonly ushort LoopEnd;
        }

        public class XMAEncoderParams
        {
            public XMAEncoderParams(short[] data, int numChannels, int sampleRate, int loopStart, int loopEnd, int compression,
                int blockSize)
            {
                this.Data = data;
                this.SampleRate = sampleRate;
                this.NumChannels = numChannels;
                this.LoopStart = loopStart;
                this.LoopEnd = loopEnd;
                this.Compression = compression;
                this.BlockSize = blockSize;
            }
            public short[] Data { get; private set; }
            public int NumChannels { get; private set; }
            public int SampleRate { get; private set; }
            public int LoopStart { get; private set; }
            public int LoopEnd { get; private set; }
            public int Compression { get; private set; }
            public int BlockSize { get; private set; }
        }

        public static EncoderResult EncodeXMA(XMAEncoderParams parameters)
        {
            return EncodeXMA(parameters.Data, parameters.NumChannels, parameters.SampleRate, parameters.LoopStart, parameters.LoopEnd, parameters.Compression, parameters.BlockSize);
        }

        public static EncoderResult EncodeXMA(short[] data, int numChannels, int sampleRate, int loopStart, int loopEnd, int compression, int blockSize)
        {
            var encoderState = new UnmanagedWaveEncoder.XMA2Encoder();

            encoderState.EncodeBuffer(data, numChannels, compression, sampleRate, loopStart, loopEnd, blockSize);
            UnmanagedWaveEncoder.XMA2Header header = encoderState.GetHeaderInfo();

            var calculatedLoopBegin = header.LoopBegin / 128;
            if (calculatedLoopBegin > UInt16.MaxValue ||
                calculatedLoopBegin * 128 != header.LoopBegin)
            {
                throw new Exception("Invalid Loop Begin");
            }

            var calculatedLoopEnd = (header.LoopBegin + header.LoopLength) / 128;
            if (calculatedLoopEnd > UInt16.MaxValue ||
                calculatedLoopEnd * 128 != (header.LoopBegin + header.LoopLength))
            {
                throw new Exception("Invalid Loop End");
            }

            var playBegin = header.PlayBegin;
            if (playBegin > Byte.MaxValue ||
                playBegin * 128 != header.PlayBegin)
            {
                //throw new Exception(string.Format("Invalid Play Begin: {0}", playBegin));
                Console.WriteLine("Warning: Invalid play begin: " + playBegin);
                playBegin = 0;
            }

            var playEnd = (header.PlayBegin + header.PlayLength) / 128;
            if (playEnd > UInt16.MaxValue ||
                playEnd * 128 != (header.PlayBegin + header.PlayLength))
            {
                //throw new Exception("Invalid Play end");
                Console.Out.WriteLine("warning: Invalid play end: " + playEnd);
                playEnd = 0;
            }
            List<ushort> seekTable = new List<ushort>();
            return new EncoderResult(encoderState.GetEncodedData(), new PackageSeekTableObject(encoderState.GetSeekTable()), PackageWaveFormatObject.FormatType.XMA2, (ushort)header.SampleRate, (int)calculatedLoopBegin, header.SamplesEncoded, (Byte)playBegin, (ushort)playEnd, (ushort)calculatedLoopBegin, (ushort)calculatedLoopEnd);
        }

        public static EncoderResult EncodeMP3(short[] data, int numChannels, int sampleRate, int compression, int loopOffsetSamples, bool preserveTransients)
        {
            uint originalLengthSamples = (uint)data.Length;
            UnmanagedWaveEncoder.PCMData inputPcmData = new UnmanagedWaveEncoder.PCMData(data, numChannels, sampleRate, loopOffsetSamples);
            UnmanagedWaveEncoder.MP3Data mp3EncodedData = new UnmanagedWaveEncoder.MP3Data(inputPcmData, compression, preserveTransients);

            //this code was in the C++ MP3Encoder source code in the old pipeline:
            // HACK: store post-encode headroom in LoopBegin field as millibels 
            // ushort loopBegin = (ushort)(float)encoder.GetPostEncodeHeadroom();
            //this code was in the source code using an MP3Encoder.exe in the old pipeline (storing the return value):
            ushort loopBegin = 1;
            ushort loopEnd = (ushort) (preserveTransients ? 1 : 0);
            ushort[] seekTable = mp3EncodedData.SeekTable;
            return new EncoderResult(mp3EncodedData.Data,
                new PackageSeekTableObject((ushort[]) (object) seekTable), PackageWaveFormatObject.FormatType.MP3,
                (ushort)sampleRate, loopOffsetSamples, originalLengthSamples, 0, 0, loopBegin, loopEnd);
        }

        public static byte[] EncodeADPCM(byte[] data)
        {
            return UnmanagedWaveEncoder.ADPCMEncoder.EncodeADPCM(data);
        }
    }
}
