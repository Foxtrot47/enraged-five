﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaveEncoder
{
    public class PCMData
    {
        private short[] data;

        public short[] Data
        {
            get { return data; }
        }

        private int numChannels;

        public int NumChannels
        {
            get { return numChannels; }
        }

        private int sampleRate;

        public int SampleRate
        {
            get { return sampleRate; }
        }

        private int loopStart;

        public int LoopStart
        {
            get { return loopStart; }
        }

        public PCMData(short[] pcmData, int numChannels, int sampleRate, int loopStart)
        {
            this.data = pcmData;
            this.numChannels = numChannels;
            this.sampleRate = sampleRate;
            this.loopStart = loopStart;
        }
    }
}
