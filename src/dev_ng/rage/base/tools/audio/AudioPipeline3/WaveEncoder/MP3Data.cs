﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnmanagedWaveEncoder = WaveEncoder;

namespace WaveEncoder
{
    public class MP3Data
    {
        private readonly int headroom;
        public int Headroom {
            get { return headroom; }
        }

        private readonly byte[] encodedData;
        public byte[] Data {
            get { return encodedData; }
        }

        private readonly ushort[] seekTable;
        public ushort[] SeekTable {
            get { return seekTable; }
        }

        //loopStartOffsetSamples should be the start of the first loop if theres only one loop, 0 if the whole file should be looped, or -1 otherwise
        public MP3Data(PCMData pcmData, int compression, bool preserveTransients)
        {
            int originalDataLength = pcmData.Data.Length;
            pcmData = PreprocessData(pcmData, preserveTransients);

            // We currently only support 1152 samples per frame
		    int samplesPerFrame = 1152;
		    // Match to the closest valid samplerate
		    // Don't allow MP3 sample rate to go below 32kHz, otherwise we end up with 576 sample frames which our
		    // runtime doesn't support
		    int mp3SampleRate = pcmData.SampleRate;
		    if(mp3SampleRate <= 32000)
		    {
			    mp3SampleRate = 32000;
		    }
		    else if(mp3SampleRate <= 44100)
		    {
			    mp3SampleRate = 44100;
		    }
		    else
		    {
			    mp3SampleRate = 48000;
		    }

		    //Zero-pad the raw wave data (into a new buffer) to ensure it aligns to the frame size.
		    int paddingSamples = (samplesPerFrame*pcmData.NumChannels) - (pcmData.Data.Length % (samplesPerFrame*pcmData.NumChannels));
		    if(paddingSamples == (samplesPerFrame*pcmData.NumChannels))
		    {
			    paddingSamples = 0;
		    }

		    //add a fade in data packet with silence to allow for mp3 windowing 
		    if(pcmData.LoopStart==0)
		    {
			    paddingSamples += (samplesPerFrame*pcmData.NumChannels);
		    }

		
		    //it seems padding gets added at the end
		    IEnumerable<short> paddedInputSamples = pcmData.Data.Concat(new short[paddingSamples]);

		    /*
		    original code, adds padding at the end?
		    unsigned int unpaddedLength = inputSamples->Length;
		    inputLength += paddingSamples;

		    short *paddedBuffer = new short[inputLength];
		    memcpy(paddedBuffer, inputSamples, unpaddedLength*sizeof(short));
		    if(paddingSamples > 0)
		    {
			    memset(paddedBuffer + unpaddedLength, 0, paddingSamples*sizeof(short));
		    }
		    */

            encodedData = UnmanagedWaveEncoder.MP3Encoder.EncodePcmData(paddedInputSamples.ToArray(), pcmData.NumChannels, compression, mp3SampleRate, samplesPerFrame);
            
            seekTable = MP3Parser.GenerateSeekTable(encodedData).ToArray();

            Tuple<byte[], ushort[]> postProcessData = PostProcessMp3Frames(encodedData, samplesPerFrame, seekTable, pcmData.LoopStart, originalDataLength);
            encodedData = postProcessData.Item1;
            seekTable = postProcessData.Item2;
        }

        private PCMData PreprocessData(PCMData pcmData, bool preserveTransients)
        {
            IEnumerable<short> preprocessedPcmData = pcmData.Data;
            int preprocessedLoopStart = pcmData.LoopStart;

            if (pcmData.LoopStart == -1 && preserveTransients)
            {
                // Add one MP3 frame of silence to improve initial transient performance
                int samplesPadding = (1152 + 64)*pcmData.NumChannels;
                preprocessedPcmData = (new short[samplesPadding]).Concat(pcmData.Data);
            }
            else if (pcmData.LoopStart == 0)
	        {
		        //Concatenate 3 full loops to present to the MP3 encoder.
	            preprocessedPcmData = pcmData.Data.Concat(pcmData.Data).Concat(pcmData.Data);
	        }
            else if (pcmData.LoopStart > 0)
	        {
		        //Concatenate 3 full loops onto the preloop to present to the MP3 encoder.
		        // - moving the loop point to the end of the first loop.
                int loopLengthSamples = pcmData.Data.Length - pcmData.LoopStart;
	            preprocessedPcmData =
                    pcmData.Data.Concat(pcmData.Data.Skip(pcmData.LoopStart).Take(loopLengthSamples))
                        .Concat(pcmData.Data.Skip(pcmData.LoopStart).Take(loopLengthSamples));
		        //Move the loop point to the end of the first loop.
                preprocessedLoopStart=pcmData.LoopStart + loopLengthSamples;
	        }
            PCMData result = new PCMData(preprocessedPcmData.ToArray(), pcmData.NumChannels, pcmData.SampleRate, preprocessedLoopStart);
            return result;
        }


        Tuple<byte[], ushort[]> PostProcessMp3Frames(byte[] mp3Data, int samplesPerFrame, ushort[] seekTable, int loopStart, int pcmDataLength)
        {
            int numFrames = seekTable.Length;
	        if(loopStart == 0)
	        {
		        //Extract the central loop from the 3 concatenated loops we encoded.
		        int startFrame = ((numFrames - 2) / 3) + 1; //Trim the frame of silence at the start and end.
		        int endFrame = ((numFrames - 2) * 2 / 3) + 1; //Trim the frame of silence at the start and end.

		        int startOffsetBytes = 0;
		        int sizeBytes = 0;
		        for(int i=0; i<startFrame; i++)
		        {
			        startOffsetBytes += seekTable[i];
		        }

		        for(int i=startFrame; i<endFrame; i++)
		        {
			        sizeBytes += seekTable[i];
		        }
	            return new Tuple<byte[], ushort[]>(
	                mp3Data.Skip(startOffsetBytes).Take(sizeBytes).ToArray(),
	                seekTable.Skip(startFrame).Take(seekTable.Length - startFrame).ToArray()
	                );
	        }
	        else if(loopStart > 0)
	        {
		        //Cut the 3rd loop off the end of the preloop and 3 loops we encoded.
		        int loopLengthSamples = (pcmDataLength - loopStart) / 2;
		        int endSample = loopStart + loopLengthSamples;
		        numFrames = (endSample / samplesPerFrame) + 1; //Take the frame of silence at the start into account.

		        int waveSampleDataOutLengthBytes = 0;
		        //Strip the first frame of silent MP3 data.
		        for(int i=1; i<numFrames; i++)
		        {
			        waveSampleDataOutLengthBytes += seekTable[i];
		        }
	            return new Tuple<byte[], ushort[]>(
	                mp3Data.Skip(seekTable[0]).Take(waveSampleDataOutLengthBytes).ToArray(),
	                seekTable.Skip(1).Take(seekTable.Length - 1).ToArray()
                    );
	        }
	        else if (numFrames > 2)
	        {
	            //Strip the first and last frames of silent MP3 data.
	            int waveSampleDataOutLengthBytes = mp3Data.Length;
	            waveSampleDataOutLengthBytes -= seekTable[0] + seekTable[numFrames - 1];
	            return new Tuple<byte[], ushort[]>(
	                mp3Data.Skip(seekTable[0]).Take(waveSampleDataOutLengthBytes).ToArray(),
	                seekTable.Skip(1).Take(seekTable.Length - 2).ToArray()
                    );
	        }
	        else
	        {
                return new Tuple<byte[], ushort[]>(
                    (byte[])mp3Data.Clone(),
                    (ushort[])seekTable.Clone()
                    );
	        }
        }



        //Class has been imported from the old pipeline
        public class MP3Parser
        {
            //private static int[] ms_validSampleRates = {8000, 11025, 12000, 16000, 22050, 24000, 32000, 44100, 48000};

            private static readonly int[] ms_mpegVersionTable = {
                                                               (int) audMpegVersions.Mpeg2_5, -1,
                                                               (int) audMpegVersions.Mpeg2, (int) audMpegVersions.Mpeg1
                                                           };

            private static readonly int[] ms_mpegLayerTable = {
                                                             -1, (int) audMpegLayers.Layer3, (int) audMpegLayers.Layer2,
                                                             (int) audMpegLayers.Layer1
                                                         };

            private static readonly int[] ms_mpegSlotBytes = {
                                                            4, //LAYER1
                                                            1, //LAYER2
                                                            1 //LAYER3
                                                        };

            private static readonly int[][] ms_mpeg1BitrateTable = {
                                                                  new[] {-1, -1, -1}, new[] {32, 32, 32},
                                                                  new[] {64, 48, 40}, new[] {96, 56, 48},
                                                                  new[] {128, 64, 56}, new[] {160, 80, 64},
                                                                  new[] {192, 96, 80}, new[] {224, 112, 96},
                                                                  new[] {256, 128, 112}, new[] {288, 160, 128},
                                                                  new[] {320, 192, 160}, new[] {352, 224, 192},
                                                                  new[] {384, 256, 224}, new[] {416, 320, 256},
                                                                  new[] {448, 384, 320}, new[] {-1, -1, -1}
                                                              };

            private static readonly int[][] ms_mpeg2BitrateTable = {
                                                                  new[] {-1, -1, -1}, new[] {32, 8, 8}, new[] {48, 16, 16},
                                                                  new[] {56, 24, 24}, new[] {64, 32, 32},
                                                                  new[] {80, 40, 40}, new[] {96, 48, 48},
                                                                  new[] {112, 56, 56}, new[] {128, 64, 64},
                                                                  new[] {144, 80, 80}, new[] {160, 96, 96},
                                                                  new[] {176, 112, 112}, new[] {192, 128, 128},
                                                                  new[] {224, 144, 144}, new[] {256, 160, 160},
                                                                  new[] {-1, -1, -1}
                                                              };

            private static readonly int[][] ms_mpegSampleRateTable = {
                                                                    new[] {44100, 22050, 11025},
                                                                    new[] {48000, 24000, 12000}, new[] {32000, 16000, 8000}
                                                                    , new[] {-1, -1, -1}
                                                                };

            public static List<ushort> GenerateSeekTable(byte[] mp3Data)
            {
                var packetOffsets = new List<ushort>();
                for (var byteOffset = 0; byteOffset < mp3Data.Length; byteOffset++)
                {
                    //Find the frame sync.
                    if ((mp3Data[byteOffset + 0] == 0xFF) &&
                        ((mp3Data[byteOffset + 1] & 0xE0) == 0xE0))
                    {
                        /*unsigned int privateBit = frame[2] & 0x1;
                        unsigned int channelMode = (frame[3] >> 6) & 0x3;
                        unsigned int modeExtension = (frame[3] >> 4) & 0x3;
                        unsigned int copyright = (frame[3] >> 3) & 0x1;
                        unsigned int original = (frame[3] >> 2) & 0x1;
                        unsigned int emphasis = frame[3] & 0x3;*/

                        var versionIndex = (mp3Data[byteOffset + 1] >> 3) & 0x3;
                        var version = ms_mpegVersionTable[versionIndex];
                        if (version == -1)
                        {
                            throw new Exception("Invalid MPEG version");
                        }

                        var layerIndex = (mp3Data[byteOffset + 1] >> 1) & 0x3;
                        var layer = ms_mpegLayerTable[layerIndex];
                        if (layer < 0)
                        {
                            throw new Exception("Error: Invalid MPEG layer");
                        }

                        var bitrateIndex = (mp3Data[byteOffset + 2] >> 4) & 0xF;
                        int bitrate;
                        if (version == (int)audMpegVersions.Mpeg1)
                        {
                            bitrate = ms_mpeg1BitrateTable[bitrateIndex][layer] * 1000;
                        }
                        else
                        {
                            bitrate = ms_mpeg2BitrateTable[bitrateIndex][layer] * 1000;
                        }

                        if (bitrate < 0)
                        {
                            throw new Exception("Error: Invalid MPEG bitrate");
                        }

                        var sampleRateIndex = (mp3Data[byteOffset + 2] >> 2) & 0x3;
                        var sampleRate = ms_mpegSampleRateTable[sampleRateIndex][version];
                        if (sampleRate < 0)
                        {
                            throw new Exception("Error: Invalid MPEG sample rate");
                        }

                        var padding = (mp3Data[byteOffset + 2] >> 1) & 0x1;
                        var paddingBytes = padding * ms_mpegSlotBytes[layer];

                        var samplesPerFrame = 1152;
                        var frameBytes = ((samplesPerFrame / 8 * bitrate) / sampleRate) + paddingBytes;

                        var protection = mp3Data[byteOffset + 1] & 0x01;
                        if (protection == 0)
                        {
                            //There should be a 16-bit CRC at the end of the header.
                            //   frameBytes += 2;
                        }

                        packetOffsets.Add((ushort)frameBytes);

                        //Jump to the end of this frame.
                        byteOffset += frameBytes - 1;
                    }
                }
                return packetOffsets;
            }

            private enum audMpegLayers
            {
                Layer1,
                Layer2,
                Layer3
            } ;

            private enum audMpegVersions
            {
                Mpeg1,
                Mpeg2,
                Mpeg2_5
            } ;
        }
    }
}
