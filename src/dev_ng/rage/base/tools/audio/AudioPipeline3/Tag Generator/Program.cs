﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using NLog;
using rage;
using Rockstar.AssetManager.Infrastructure.Enums;
using Rockstar.AssetManager.Interfaces;
using Rockstar.AssetManager.Main;

namespace Tag_Generator
{
    class Program
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        static int Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += (sender, eventArgs) =>
            {
                logger.Error(eventArgs.ExceptionObject.ToString());
                Environment.Exit(-1);
            };

            ProgramConfiguration config;
            try
            {
                config = new ProgramConfiguration(args);
            }
            catch (rage.ToolLib.CmdLine.CommandLineConfiguration.ParameterException ex)
            {
                logger.Error(ex.Message);
                logger.Info(ex.Usage);
                return -1;
            }

            IAssetManager assetMgr = AssetManagerFactory.GetInstance(AssetManagerType.Perforce, null, config.AssetServer, config.AssetProject,
                config.AssetUsername, config.AssetPassword, config.DepotRoot);
            IChangeList changelist = assetMgr.CreateChangeList("Auto generated tag files");

            audProjectSettings projectSettings = new audProjectSettings(config.ProjectSettings);

            foreach (PlatformSetting platformSetting in projectSettings.GetPlatformSettings())
            {
                if (!platformSetting.IsActive) continue;

                if (config.GenerateBuiltTags) GenerateBuiltTags(changelist, assetMgr, projectSettings, platformSetting.PlatformTag);
                if (config.GeneratePendingTags) GeneratePendingTags(changelist, assetMgr, projectSettings, platformSetting.PlatformTag);
            }
            
            changelist.RevertUnchanged();
            if (config.SubmitGeneratedTags && changelist.Assets.Count > 0) changelist.Submit();

            return 0;
        }

        private static void GeneratePendingTags(IChangeList changelist, IAssetManager assetManager,
            audProjectSettings projectSettings, string platform)
        {
            projectSettings.SetCurrentPlatformByTag(platform);
            logger.Info("Getting latest PendingWaves...");
            string pendingWavesFolder = Path.Combine(projectSettings.GetCurrentPlatform().BuildInfo, "PendingWaves\\");
            assetManager.GetLatest(pendingWavesFolder, false);
            pendingWavesFolder = assetManager.GetLocalPath(pendingWavesFolder);
            foreach (string file in Directory.GetFiles(pendingWavesFolder))
            {
                //only process xml files that exists in the depot
                if(!file.EndsWith(".xml", StringComparison.OrdinalIgnoreCase) && !assetManager.ExistsAsAsset(file)) continue;

                XDocument pendingWaves = XDocument.Load(file);
                if (null == pendingWaves.Root || pendingWaves.Root.Name != "PendingWaves")
                    throw new Exception("BuiltWaves root element expected in "+file);
                XElement packElement = pendingWaves.Root.Elements("Pack").Single();

                string packName = packElement.Attribute("name").Value;
                string assetPath = Path.Combine(assetManager.GetLocalPath(projectSettings.GetWaveInputPath()), packName);
                logger.Log(LogLevel.Info, "Modifying tags for pending pack " + packName);
                AddTypeFile(packElement, assetPath, changelist, assetManager);
                AddElementTags(packElement, assetPath, platform, changelist, assetManager);
                ProcessBankFolders(packElement, assetPath, platform, changelist, assetManager);
                ProcessBanks(packElement, assetPath, platform, changelist, assetManager);
            }

            
        }

        private static void GenerateBuiltTags(IChangeList changelist, IAssetManager assetManager, audProjectSettings projectSettings, string platform)
        {
            projectSettings.SetCurrentPlatformByTag(platform);
            logger.Info("Getting latest BuiltWaves...");
            string builtWavesFolder = Path.Combine(projectSettings.GetCurrentPlatform().BuildInfo, "BuiltWaves\\");
            assetManager.GetLatest(builtWavesFolder, false);
            builtWavesFolder = assetManager.GetLocalPath(builtWavesFolder);
            string builtWavesPackListPath = Path.Combine(builtWavesFolder, "BuiltWaves_PACK_LIST.xml");
            XDocument builtWavesPackList = XDocument.Load(builtWavesPackListPath);
            if (null == builtWavesPackList.Root || builtWavesPackList.Root.Name != "BuiltWaves")
                throw new Exception("BuiltWaves root element expected in BuiltWaves_Pack_List.xml");
            foreach (XElement packFile in builtWavesPackList.Root.Elements("PackFile"))
            {
                string builtWavesPath = Path.Combine(builtWavesFolder, packFile.Value);
                XDocument builtWaves = XDocument.Load(builtWavesPath);
                XElement packElement = builtWaves.Root;
                if (packElement == null || packElement.Name != "Pack")
                    throw new Exception("Pack root element expected in " + packFile.Value);
                
                string packName = packElement.Attribute("name").Value;
                string assetPath = Path.Combine(assetManager.GetLocalPath(projectSettings.GetWaveInputPath()), packName);
                logger.Log(LogLevel.Info, "Adding tags for built pack "+packName);
                AddTypeFile(packElement, assetPath, changelist, assetManager);
                AddElementTags(packElement, assetPath, platform, changelist, assetManager);
                ProcessBankFolders(packElement, assetPath, platform, changelist, assetManager);
                ProcessBanks(packElement, assetPath, platform, changelist, assetManager);
            }
        }

        private static void ProcessBankFolders(XElement element, string outputPath, string platform,
            IChangeList changelist, IAssetManager assetManager)
        {
            foreach (XElement bfElement in element.Elements("BankFolder"))
            {
                if(bfElement.Attribute("name")==null || string.IsNullOrEmpty(bfElement.Attribute("name").Value))
                    throw new Exception("BankFolder element without a name found!");
                string bankFolderName = bfElement.Attribute("name").Value;

                string bfOutputPath = Path.Combine(outputPath, bankFolderName);
                AddTypeFile(bfElement, bfOutputPath, changelist, assetManager);
                AddElementTags(bfElement, bfOutputPath, platform, changelist, assetManager);
                ProcessBankFolders(bfElement, bfOutputPath, platform, changelist, assetManager);
                ProcessBanks(bfElement, bfOutputPath, platform, changelist, assetManager);
            }
        }

        private static void ProcessBanks(XElement element, string outputPath, string platform,
            IChangeList changelist, IAssetManager assetManager)
        {
            foreach (XElement bankElement in element.Elements("Bank"))
            {
                if (bankElement.Attribute("name") == null || string.IsNullOrEmpty(bankElement.Attribute("name").Value))
                    throw new Exception("Bank element without a name found!");
                string bankName = bankElement.Attribute("name").Value;

                string bankOutputPath = Path.Combine(outputPath, bankName);
                AddTypeFile(bankElement, bankOutputPath, changelist, assetManager);
                AddElementTags(bankElement, bankOutputPath, platform, changelist, assetManager);
                ProcessWaveFolders(bankElement, bankOutputPath, platform, changelist, assetManager);
            }
        }

        private static void ProcessWaveFolders(XElement element, string outputPath, string platform,
            IChangeList changelist, IAssetManager assetManager)
        {
            foreach (XElement waveFolderElement in element.Elements("WaveFolder"))
            {
                if (waveFolderElement.Attribute("name") == null || string.IsNullOrEmpty(waveFolderElement.Attribute("name").Value))
                    throw new Exception("WaveFolder element without a name found!");
                string waveFolderName = waveFolderElement.Attribute("name").Value;

                string waveFolderOutputPath = Path.Combine(outputPath, waveFolderName);
                AddTypeFile(waveFolderElement, waveFolderOutputPath, changelist, assetManager);
                AddElementTags(waveFolderElement, waveFolderOutputPath, platform, changelist, assetManager);
            }
        }

        private static void AddElementTags(XElement element, string outputPath, string platform, IChangeList changelist, IAssetManager assetManager)
        {
            foreach (XElement tagElement in element.Elements("Tag"))
            {
                string tagPath = Path.Combine(outputPath, "tag." + tagElement.Attribute("name").Value + "." + platform);
                string tagValue = tagElement.Attribute("value") != null ? tagElement.Attribute("value").Value : "";

                if (tagElement.Attribute("operation") != null &&
                    tagElement.Attribute("operation").Value.Equals("remove"))
                {
                    //if tag gets deleted
                    if (assetManager.ExistsAsAsset(tagPath)) changelist.MarkAssetForDelete(tagPath);
                }
                else
                {
                    //if tag gets added or modified
                    if (assetManager.ExistsAsAsset(tagPath))
                    {
                        changelist.CheckoutAsset(tagPath, true);
                        Directory.CreateDirectory(Path.GetDirectoryName(tagPath));
                        File.WriteAllText(tagPath, tagValue);
                    }
                    else
                    {
                        Directory.CreateDirectory(Path.GetDirectoryName(tagPath));
                        File.WriteAllText(tagPath, tagValue);
                        changelist.MarkAssetForAdd(tagPath);
                    }
                }
            }
        }

        private static void AddTypeFile(XElement typeElement, string outputPath, IChangeList changelist,
            IAssetManager assetManager)
        {
            string typePath = Path.Combine(outputPath, typeElement.Name.ToString());

            if (typeElement.Attribute("operation") != null &&
                typeElement.Attribute("operation").Value.Equals("remove"))
            {
                //if tag gets deleted
                if (assetManager.ExistsAsAsset(typePath)) changelist.MarkAssetForDelete(typePath);
            }
            else
            {
                //if tag gets added
                if (!assetManager.ExistsAsAsset(typePath))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(typePath));
                    File.WriteAllText(typePath, "");
                    changelist.MarkAssetForAdd(typePath);
                }
            }
        }
    }
}
