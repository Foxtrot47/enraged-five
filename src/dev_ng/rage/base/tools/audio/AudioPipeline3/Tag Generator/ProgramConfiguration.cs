﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using rage.ToolLib.CmdLine;

namespace Tag_Generator
{
    public class ProgramConfiguration : CommandLineConfiguration
    {
        public ProgramConfiguration(string[] args)
            : base(args)
        {
        }

        [Description("the connection info for perforce ex. rsgedip4s1:1666")]
        public string AssetServer { get; set; }

        [Description("the perforce project")]
        public string AssetProject { get; set; }

        [Description("the perforce user name")]
        public string AssetUsername { get; set; }

        [Description("the perforce password")]
        [OptionalParameter]
        [DefaultValue("")]
        public string AssetPassword { get; set; }

        [Description("path to the projectSettings.xml configuration")]
        public string ProjectSettings { get; set; }

        [Description("the depot root")]
        public string DepotRoot { get; set; }
        
        [Description("Generate built tags")]
        [OptionalParameter]
        [DefaultValue(true)]
        public bool GenerateBuiltTags { get; set; }

        [Description("Generate pending tags")]
        [OptionalParameter]
        [DefaultValue(false)]
        public bool GeneratePendingTags { get; set; }

        [Description("Submit generated tags")]
        [OptionalParameter]
        [DefaultValue(false)]
        public bool SubmitGeneratedTags { get; set; }
    }
}
