﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using rage.ToolLib.Writer;

namespace Rockstar.Audio.Pipeline
{
    public class PackageSeekTableObject: IPackageObject
    {

        private UInt16[] SeektableShort { get; set; }
        private UInt32[] SeektableInt { get; set; }

        public PackageSeekTableObject(ushort[] seekTable)
        {
            SeektableShort = seekTable;
            SeektableInt = null;
        }

        public PackageSeekTableObject(uint[] seekTable)
        {
            SeektableInt = seekTable;
            SeektableShort = null;
        }

        public void SerializeToStream(System.IO.Stream stream, bool isBigEndian)
        {
            using (var bw = new BinaryWriter(stream, Encoding.ASCII, isBigEndian, true))
            {
                if (SeektableShort != null) foreach (UInt16 entry in SeektableShort) bw.Write(entry);
                else if (SeektableInt != null) foreach (UInt32 entry in SeektableInt) bw.Write(entry);
            }
        }
    }
}
