﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Configuration;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.IO;

namespace Rockstar.Audio.Pipeline
{
    public class Package
    {
        public const string OUTPUT_MANIFEST = "output.xml";
        public const string INPUT_MANIFEST = "input.xml";

        PackageFileCollection m_Files;
        PropertyCollection m_Properties;
        string m_FileLocation;
        
        public Package()
        {
            m_Properties = new PropertyCollection();
            m_Files = new PackageFileCollection();
        }

        public Package(string fileLocation)
        {
            m_FileLocation = fileLocation;
            m_Properties = new PropertyCollection();
            m_Files = new PackageFileCollection();
        }

        #region IPackage Members
        
        public XDocument SerializeMetadata()
        {
            // Order and format of serialisation is important
            var props = Properties.Serialize();
            var files = new XElement("Files",
                   from f in Files.OrderBy(x => x.FileName) select f.SerializeMetadata());

            return new XDocument(new XElement("Package", props, files));
        }

        public PackageFileCollection Files
        {
            get { return m_Files; }
        }

        public IPropertyCollection Properties
        {
            get { return m_Properties; }
        }

        public string FileLocation
        {
            get
            {
                return m_FileLocation;
            }
        }

        public bool HasFileHashes
        {
            get
            {
                // Return true when all package files have computed hash values
                return m_Files.All(f => f.Hash != null);
            }
        }

        #endregion

        public static Package CreatePackageFromManifest(string path)
        {
            return CreatePackageFromManifest(path, null);
        }

        public static Package CreatePackageFromManifest(string path, string fileLocation)
        {
            var p = new Package(fileLocation);

            var doc = XDocument.Load(path);
            if(!doc.Root.Name.LocalName.Equals("Package"))
            {
                throw new ArgumentException("Package manifest has invalid root element; should be <Package>", doc.Root.Name.LocalName);
            }

            p.Properties.Load(doc.Root.Element("Properties"));
            p.m_Files.AddRange(from f in doc.Root.Element("Files").Elements() select new PackageFile(f, fileLocation));

            return p;
        }

        public void WriteFiles(string destination, bool isBigEndian=false)
        {
            foreach (var f in Files)
            {
                if (f.Object != null)
                {
                    var destPath = Path.Combine(destination, f.FileName);
                    using (FileStream fs = new FileStream(destPath, FileMode.Create, FileAccess.Write))
                    {
                        f.Object.SerializeToStream(fs, isBigEndian);
                    }
                }
                else
                {
                    File.Copy(Path.Combine(m_FileLocation, f.FileName), Path.Combine(destination, f.FileName), true);
                }
            }
        }

    }
}
