﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Rockstar.Audio.Data;
using rage.ToolLib.Writer;

namespace Rockstar.Audio.Pipeline
{
    public class PackageWaveContainer : IPackageObject
    {
        public readonly Container Container;

        public PackageWaveContainer(string filePath)
        {
            throw new NotImplementedException();
        }

        public PackageWaveContainer(Container container)
        {
            Container = container;
        }

        #region IPackageObject Members

        public void SerializeToStream(Stream stream, bool isBigEndian)
        {
            using(var bw = new rage.ToolLib.Writer.BinaryWriter(stream, Encoding.Default, isBigEndian, true))
            {
                Container.Serialize(bw);
            }
        }

        #endregion
    }
}
