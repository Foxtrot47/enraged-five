﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using rage.ToolLib;
using rage.ToolLib.Writer;

namespace Rockstar.Audio.Pipeline
{
    public class PackageWaveMarkerObject: List<PackageWaveMarkerObject.WaveMetadataMarker>, IPackageObject
    {
        [StructLayout(LayoutKind.Explicit)]
        struct FloatConverter
        {
            [FieldOffset(0)]
            public float floatNumber;
            [FieldOffset(0)]
            public UInt32 fixedNumber;
        }

        public class WaveMetadataMarker
        {
            private FloatConverter markerNameValue;

            public string Label { get; private set; }
            public UInt32 CategoryHash { get; set; }
            public UInt32 NameHash
            {
                get { return markerNameValue.fixedNumber; }
                set { markerNameValue.fixedNumber = value; }
            }
            public float Value
            {
                get { return markerNameValue.floatNumber; }
                set { markerNameValue.floatNumber = value; }
            }
            public UInt32 Data { get; set; }
            public UInt32 TimeOffset { get; set; }

            public UInt32 SortKey { get; set; }

            public WaveMetadataMarker(string label, UInt32 timeOffset)
            {
                markerNameValue = new FloatConverter();
                this.Label = label;
                List<string> splitStrings = label.Split(':').ToList();
                if (splitStrings.Count >= 1)
                {
                    var h = new Hash { Value = splitStrings[0] };
                    CategoryHash = h.Key;
                }
                if (splitStrings.Count >= 2)
                {
                    float f;
                    if (float.TryParse(splitStrings[1], out f))
                    {
                        FloatConverter fc = new FloatConverter();
                        fc.floatNumber = f;

                        // Note: NameHash/Value is a union
                        NameHash = fc.fixedNumber;
                    }
                    else
                    {
                        var h = new Hash { Value = splitStrings[1] };
                        NameHash = h.Key;
                    }
                }
                if (splitStrings.Count >= 3)
                {
                    UInt32 valOut;
                    if (UInt32.TryParse(splitStrings[2], out valOut))
                    {
                        Data = valOut;
                    }
                }

                TimeOffset = timeOffset;
                SortKey = timeOffset;
            }
        }

        public void SerializeToStream(System.IO.Stream stream, bool isBigEndian)
        {
            using (var bw = new BinaryWriter(stream, Encoding.ASCII, isBigEndian, true))
            {
                var sortedList = from m in this
                                 orderby m.SortKey
                                 select m;

                foreach (WaveMetadataMarker marker in sortedList)
                {
                    bw.Write(marker.CategoryHash);
                    bw.Write(marker.NameHash);
                    bw.Write(marker.TimeOffset);
                    bw.Write(marker.Data);
                }
            }
        }
    }
}
