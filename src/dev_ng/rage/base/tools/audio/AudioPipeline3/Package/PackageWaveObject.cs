﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rockstar.Audio.Data;

namespace Rockstar.Audio.Pipeline
{
    public class PackageWaveObject : IPackageObject
    {
        public readonly WaveSample SampleData;

        public PackageWaveObject(string filePath)
        {
            SampleData = new WaveSample(filePath);
        }

        public PackageWaveObject(WaveSample sampleData)
        {
            SampleData = sampleData;
        }

        #region IPackageObject Members

        public void SerializeToStream(Stream stream, bool isBigEndian=false)
        {
            SampleData.SerializeToStream(stream);
        }

        #endregion
    }
}
