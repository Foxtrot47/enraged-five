﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Rockstar.Audio.Pipeline
{
    public class PackageGenericObject : IPackageObject
    {
        public byte[] Data { get; set; }

        public PackageGenericObject(string filePath)
        {
            using (var fs = new FileStream(filePath, FileMode.Open, FileAccess.Read))
            {
                Data = new byte[fs.Length];
                fs.Read(Data, 0, (int)fs.Length);
            }
        }

        public PackageGenericObject(byte[] buffer)
        {
            Data = buffer;
        }

        public PackageGenericObject(uint[] buffer)
        {
            Data = new byte[buffer.Length * 4];
            using (var ms = new MemoryStream(Data))
            {
                foreach (var b in buffer)
                {
                    ms.Write(BitConverter.GetBytes(b), 0, 4);
                }
            }
        }

        public PackageGenericObject(ushort[] buffer)
        {
            Data = new byte[buffer.Length * 2];
            using (var ms = new MemoryStream(Data))
            {
                foreach (var b in buffer)
                {
                    ms.Write(BitConverter.GetBytes(b), 0, 2);
                }
            }
        }
        #region IPackageObject Members

        public void SerializeToStream(System.IO.Stream stream, bool isBigEndian)
        {
            if(isBigEndian) throw new Exception("Serializing big endian is not supported by PackageGenericObject. As the data is stored as a byte array the original data format is unknown.");
            stream.Write(Data, 0, Data.Length);
        }

        #endregion
    }
}
