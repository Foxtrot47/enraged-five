﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using rage.ToolLib.Writer;

namespace Rockstar.Audio.Pipeline
{
    public class PackageWaveFormatObject : IPackageObject
    {
        public enum FormatType
        {
            PCM_S16,
            PCM_F32,
            XMA2,
            MP3,
            ADPCM
        }

        enum InternalFormatType
         {
             // All platforms
             AUD_FORMAT_PCM_S16_LITTLE = 0,
             AUD_FORMAT_PCM_S16_BIG,
             AUD_FORMAT_PCM_F32_LITTLE,
             AUD_FORMAT_PCM_F32_BIG,
             AUD_FORMAT_ADPCM,
             // Xenon only
             AUD_FORMAT_XMA2,
             AUD_FORMAT_XWMA,
             // PS3/PC
             AUD_FORMAT_MP3,
             // PC only
             AUD_FORMAT_OGG,
             AUD_FORMAT_AAC,
             AUD_FORMAT_WMA,
             //to permit range checking
             AUD_NUMFORMATS,
         }

         public UInt32 LengthSamples { get; set; }
         /// <summary>
         /// Loop point in samples
         /// </summary>
         public Int32 LoopPointSamples { get; set; }
         /// <summary>
         /// Sample rate
         /// </summary>
         public UInt16 SampleRate { get; set; }
         /// <summary>
         /// Headroom
         /// </summary>
         public UInt16 Headroom { get; set; }
         /// <summary>
         /// Loop start bits, XMA only
         /// </summary>
         public UInt16 LoopBegin { get; set; }
         /// <summary>
         /// Loop end bits, XMA only
         /// </summary>
         public UInt16 LoopEnd { get; set; }
         /// <summary>
         /// Play end, XMA only
         /// </summary>
         public UInt16 PlayEnd { get; set; }
         /// <summary>
         /// Play begin, XMA only
         /// </summary>
         public Byte PlayBegin { get; set; }
         /// <summary>
         /// Format as specified in audiohardware/decoder.h
         /// </summary>
         public FormatType Format { get; set; }

         /// <summary>
         /// Peak sample value [0,65535] for the first sample block
         /// </summary>
         public UInt16 FirstPeakSample { get; set; }

         /// <summary>
         /// Set whether we should mark padding with a value to indicate custom lipsync (for in-game debugging)
         /// </summary>
         public bool HasCustomLipsync { get; set; }


        public void SerializeToStream(System.IO.Stream stream, bool isBigEndian)
        {
 	         using (var bw = new BinaryWriter(stream, Encoding.ASCII, isBigEndian, true))
             {

                bw.Write(LengthSamples);
                bw.Write(LoopPointSamples);
                bw.Write(SampleRate);
                bw.Write(Headroom);

                //XMA

                bw.Write(LoopBegin);
                bw.Write(LoopEnd);
                bw.Write(PlayEnd);
                bw.Write(PlayBegin);
                 switch (Format)
                 {
                     case FormatType.MP3:
                         bw.Write(((byte)InternalFormatType.AUD_FORMAT_MP3));
                         break;
                    case FormatType.XMA2:
                         bw.Write(((byte)InternalFormatType.AUD_FORMAT_XMA2));
                         break;
                    case FormatType.PCM_S16:
                         if(isBigEndian)
                             bw.Write(((byte) InternalFormatType.AUD_FORMAT_PCM_S16_BIG));
                         else
                             bw.Write(((byte) InternalFormatType.AUD_FORMAT_PCM_S16_LITTLE));
                         break;
                    case FormatType.ADPCM:
                         bw.Write((byte)InternalFormatType.AUD_FORMAT_ADPCM);
                         break;
                     default:
                         throw new Exception("Serialization for FormatType "+Format+" not implemented");
                 }
                 

                bw.Write(FirstPeakSample);

                // Pad to dword alignment
                bw.Write((ushort)(HasCustomLipsync ? 1 : 0));
            }
        }

    }
}
