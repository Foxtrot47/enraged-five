﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Rockstar.Audio.Pipeline
{
    public class PropertyCollection : Dictionary<string, string>, IPropertyCollection
    {
        public PropertyCollection(XElement props)
        {
            Load(props);
        }

        public PropertyCollection()
        {

        }

        public void Load(XElement props)
        {
            Clear();
            foreach (var prop in props.Elements())
            {
                Add(prop.Name.LocalName, prop.Value);
            }
        }

        public XElement Serialize()
        {
            return new XElement("Properties",
                from p in this.OrderBy(x => x.Key) select new XElement(p.Key, p.Value));
        }

        public void SetValue(string name, double value)
        {
            this[name] = value.ToString(); 
        }

        public void SetValue(string name, string value)
        {
            this[name] = value; 
        }

        public void SetValue(string name, bool value)
        {
            this[name] = value.ToString();
        }

        public double GetValue(string name, double defaultValue)
        {
            double ret = defaultValue;
            if (ContainsKey(name))
            {
                double.TryParse(this[name], out ret);
            }
            return ret;
        }

        public string GetValue(string name, string defaultValue)
        {
            if (ContainsKey(name))
            {
                return this[name];
            }
            return defaultValue;
        }

        public bool GetValue(string name, bool defaultValue)
        {
            bool ret = defaultValue;
            if (ContainsKey(name))
            {
                bool.TryParse(this[name], out ret);
            }
            return ret;
        }
    }
}
