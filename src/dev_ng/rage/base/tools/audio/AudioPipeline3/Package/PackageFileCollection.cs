﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rockstar.Audio.Pipeline
{
    public class PackageFileCollection : IList<PackageFile>
    {
        List<PackageFile> m_Files;

        public PackageFileCollection()
        {
            m_Files =  new List<PackageFile>();
        }

        #region ICollection<IPackageFile> Members

        public void Add(PackageFile file)
        {            
            m_Files.Add(file);
        }

        public void AddRange(IEnumerable<PackageFile> collection)
        {
            foreach (var f in collection)
            {
                Add(f);
            }
        }

        public void Clear()
        {
            m_Files.Clear();
        }

        public bool Contains(PackageFile item)
        {
            return m_Files.Contains(item);
        }

        public void CopyTo(PackageFile[] array, int arrayIndex)
        {
            m_Files.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return m_Files.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(PackageFile item)
        {
            return m_Files.Remove(item);
        }

        #endregion

        #region IEnumerable<IPackageFile> Members

        public IEnumerator<PackageFile> GetEnumerator()
        {
            return m_Files.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return m_Files.GetEnumerator();
        }

        #endregion

        #region IList<IPackageFile> Members

        public int IndexOf(PackageFile item)
        {
            return m_Files.IndexOf(item);
        }

        public void Insert(int index, PackageFile item)
        {
            m_Files.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            m_Files.RemoveAt(index);
        }

        public PackageFile this[int index]
        {
            get
            {
                return m_Files[index];
            }
            set
            {
                m_Files[index] = value;
            }
        }

        #endregion
    }
}
