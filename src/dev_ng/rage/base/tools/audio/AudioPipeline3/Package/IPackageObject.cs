﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rockstar.Audio.Pipeline
{
    public interface IPackageObject
    {
        void SerializeToStream(Stream filePath, bool isBigEndian);
    }
}
