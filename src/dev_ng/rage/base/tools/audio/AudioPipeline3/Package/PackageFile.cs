﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.IO;

namespace Rockstar.Audio.Pipeline
{
    public class PackageFile
    {
        public PackageFile(XElement elem, string fileLocation)
        {
            if (!elem.Name.LocalName.Equals("File"))
            {
                throw new ArgumentException("PackageFile element with invalid name; should be <File>", elem.Name.LocalName);
            }

            var fileName = elem.Element("Name").Value;
            SetObjectNameAndTypeFromFileName(fileName);

            Hash = elem.Element("Hash").Value;
            Size = long.Parse(elem.Element("Size").Value);
            m_Properties = new PropertyCollection(elem.Element("Properties"));

            m_FilePath = Path.Combine(fileLocation, fileName);
        }

        public void SetObjectNameAndTypeFromFileName(string fileName)
        {
            ObjectName = Path.GetFileNameWithoutExtension(fileName);
            ObjectType = Path.GetExtension(fileName).Substring(1);
        }

        public PackageFile()
        {

        }

        public PackageFile(string filePath)
        {
            SetObjectNameAndTypeFromFileName(filePath);
            Size = new FileInfo(filePath).Length;
            Hash = DataHash.ComputeHashForFile(filePath);
            m_FilePath = filePath;
        }

        string m_FilePath;

        #region IPackageFile Members

        public string ObjectName
        {
            get;
            set;
        }

        public string ObjectType
        {
            get;
            set;
        }

        public string FileName
        {
            get
            {
                return string.Format("{0}.{1}", ObjectName, ObjectType);
            }
        }

        public string Hash
        {
            get;
            set;
        }

        public long Size
        {
            get;
            set;
        }

        public IPackageObject Object
        {
            get;
            set;
        }
        
        PropertyCollection m_Properties = new PropertyCollection();
        public IPropertyCollection Properties
        {
            get { return m_Properties; }
        }

        public System.Xml.Linq.XElement SerializeMetadata()
        {
            return new XElement("File",
                                    new XElement("Name", FileName),
                                    new XElement("Hash", Hash),
                                    new XElement("Size", Size),
                                    m_Properties.Serialize());
        }

        public void LoadObject()
        {
            if (Object == null)
            {
                Object = PackageObjectFactory.Create(m_FilePath);
            }
        }

        public void SaveObject(Stream stream, bool isBigEndian=false)
        {
            // Serialize to a memory stream first so that we can calculate the hash value without
            // disk IO
            // Note: DataHashStream() would be a good solution here except that some serialization relies
            // on seeking, ie bwWaveFile.
            using (var hashStream = new MemoryStream())
            {
                Object.SerializeToStream(hashStream, isBigEndian);

                // Note: the underlying buffer will be larger than the contents
                Size = hashStream.Length;

                var buf = hashStream.GetBuffer();

                Hash = DataHash.ComputeHash(buf, (int) hashStream.Length);

                if (stream != null) stream.Write(buf, 0, (int) hashStream.Length);
            }
        }

        /// <summary>
        /// Updates the hash field based on the in-memory Object
        /// </summary>
        public void CalculateHash()
        {
            SaveObject(null);
        }

        #endregion
    }
}
