﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Rockstar.Audio.Pipeline
{
    class PackageObjectFactory
    {
        public static IPackageObject Create(string file)
        {
            string ext = Path.GetExtension(file);
            if (ext.Equals(".wav", StringComparison.OrdinalIgnoreCase))
            {
                return new PackageWaveObject(file);
            }
            else if (ext.Equals(".awc", StringComparison.OrdinalIgnoreCase))
            {
                return new PackageWaveContainer(file);
            }
            else
            {
                return new PackageGenericObject(file);
            }
        }
    }
}
