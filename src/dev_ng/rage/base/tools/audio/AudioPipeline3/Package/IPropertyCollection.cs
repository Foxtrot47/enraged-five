﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Rockstar.Audio.Pipeline
{
    public interface IPropertyCollection : IDictionary<string, string>
    {
        XElement Serialize();
        void Load(XElement elem);

        void SetValue(string name, double value);
        void SetValue(string name, string value);
        void SetValue(string name, bool value);

        double GetValue(string name, double defaultValue);
        string GetValue(string name, string defaultValue);
        bool GetValue(string name, bool defaultValue);
    }
}
