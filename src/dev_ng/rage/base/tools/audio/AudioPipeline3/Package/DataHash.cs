﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Security.Cryptography;
using System.IO;

namespace Rockstar.Audio.Pipeline
{
    public class DataHash
    {
        static HashAlgorithm sm_HashAlgorithm = MD5.Create();

        public static string ComputeHash(XDocument doc)
        {
            return ComputeHash(doc.ToString());
        }

        public static string ComputeHash(string str)
        {
            return ComputeHash(System.Text.Encoding.UTF8.GetBytes(str));
        }
        
        public static string ComputeHash(byte[] buf)
        {
            return ComputeHash(buf, buf.Length);
        }

        public static string ComputeHash(byte[] buf, int length)
        {
            byte[] hash;
            lock (sm_HashAlgorithm)
            {
                hash = sm_HashAlgorithm.ComputeHash(buf, 0, length);
            }
            return ConvertToString(hash);
        }

        public static string ComputeHashForFile(string filePath)
        {
            byte[] hash;
            using(var stream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
            {
                lock (sm_HashAlgorithm)
                {
                    hash = sm_HashAlgorithm.ComputeHash(stream);
                }
            }
            return ConvertToString(hash);
        }

        public static string ConvertToString(byte[] hash)
        {
            var sb = new StringBuilder(hash.Length * 2);
            foreach (byte b in hash)
            {
                sb.AppendFormat("{0:X2}", b);
            }
            return sb.ToString();
        }
    }

    public class DataHashStream : Stream
    {
        Stream BaseStream;
        HashAlgorithm m_HashAlgorithm;

        public int Count { get; private set; }

        public DataHashStream(Stream baseStream)
        {
            BaseStream = baseStream;
            m_HashAlgorithm = MD5.Create();
            m_HashAlgorithm.Initialize();
        }

        public override bool CanRead
        {
            get { return BaseStream.CanRead; }
        }

        public override bool CanSeek
        {
            get { return false; }
        }

        public override bool CanWrite
        {
            get { return BaseStream.CanWrite; }
        }

        public override void Flush()
        {
            BaseStream.Flush();
        }

        public override long Length
        {
            get { return BaseStream.Length; }
        }

        public override long Position
        {
            get
            {
                return BaseStream.Position;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            return BaseStream.Read(buffer, offset, count);
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            throw new NotImplementedException();
        }

        public override void SetLength(long value)
        {
            BaseStream.SetLength(value);
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            m_HashAlgorithm.TransformBlock(buffer, offset, count, null, 0);
            BaseStream.Write(buffer, offset, count);
            Count += count;
        }

        public string FinalizeHash()
        {
            m_HashAlgorithm.TransformFinalBlock(new byte[0], 0, 0);
            return DataHash.ConvertToString(m_HashAlgorithm.Hash);
        }        
    }

}
