﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegacyPeakNormalise
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var r = new LegacyPeakNormalise(args);
                r.Execute();
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.ToString());
                Environment.ExitCode = -1;
            }
        }
    }
}
