﻿using System;
using System.Collections.Generic;
using audAssetBuilderCommon;
using System.IO;
using Wavelib;
using Rockstar.Audio.Pipeline;

namespace LegacyPeakNormalise
{
    using rage.ToolLib;

    public class LegacyPeakNormalise : WaveBuildBase
    {
        private const double DESIRED_PEAK_LEVEL = -2.0;
        const string SILENCE_VOLUME_THRESHOLD_ATTRIBUTE_NAME = "VOLUME";
        const string SILENCE_LENGTH_THRESHOLD_ATTRIBUTE_NAME = "DURATION";

        public LegacyPeakNormalise(string[] args)
            : base(args)
        {
        }
        public override void Run()
        {
            int dataSize = WaveFile.Data.RawData.Length;

            if (dataSize <= 0)
            {
                throw new ApplicationException("Length is less than or equal to 0. What's going on here?");
            }

            var inputPackage = new Package();
            var inputWaveFile = new PackageFile();
            inputWaveFile.Object = new PackageWaveObject(new WaveSample(WaveFile));
            inputWaveFile.SetObjectNameAndTypeFromFileName(WaveFile.FileName);
            inputPackage.Files.Add(inputWaveFile);

            string markSilence = WaveProperties.BuiltAsset.MarkSilence;
            if (markSilence != null)
            {
                inputWaveFile.Properties.SetValue(PeakNormalise.ADD_SILENCE_MARKERS, true);
                var silenceParams = Utility.ParseKeyValueString(markSilence);
                if (silenceParams.ContainsKey(SILENCE_VOLUME_THRESHOLD_ATTRIBUTE_NAME))
                {
                    inputWaveFile.Properties.SetValue(PeakNormalise.SILENCE_VOLUME_THRESHOLD, float.Parse(silenceParams[SILENCE_VOLUME_THRESHOLD_ATTRIBUTE_NAME]));
                }
                if (silenceParams.ContainsKey(SILENCE_LENGTH_THRESHOLD_ATTRIBUTE_NAME))
                {
                    inputWaveFile.Properties.SetValue(PeakNormalise.SILENCE_LENGTH_THRESHOLD, float.Parse(silenceParams[SILENCE_LENGTH_THRESHOLD_ATTRIBUTE_NAME]));
                }
            }

            inputWaveFile.Properties.SetValue(Rockstar.Audio.Pipeline.PeakNormalise.DESIRED_PEAK_THRESHOLD, DESIRED_PEAK_LEVEL);
            inputWaveFile.CalculateHash();

            var outputPackage = ProcessPackage(new CacheClient(), inputPackage, new Rockstar.Audio.Pipeline.PeakNormalise());

            PackageFile outputFile = outputPackage.Files[0];
            // Need an explicit load here in case we are fetching from the cache
            outputFile.LoadObject();
            WaveFile = (outputFile.Object as PackageWaveObject).SampleData.Create16BitWaveFile();


            WaveProperties.BuiltAsset.HeadRoom = (int)(100.0 * double.Parse(outputFile.Properties[Rockstar.Audio.Pipeline.PeakNormalise.HEADROOM]));
            WaveProperties.BuiltAsset.IntegratedLoudness = double.Parse(outputFile.Properties[Rockstar.Audio.Pipeline.PeakNormalise.INTEGRATED_LOUDNESS]);
            if (outputFile.Properties.ContainsKey(Rockstar.Audio.Pipeline.PeakNormalise.LOUDNESS_RANGE))
            {
                WaveProperties.BuiltAsset.LoudnessRange = double.Parse(outputFile.Properties[Rockstar.Audio.Pipeline.PeakNormalise.LOUDNESS_RANGE]);
            }
        }

        static Package ProcessPackage(CacheClient cache, Package inputPackage, IPipelineStageProcessor processor)
        {
            inputPackage.Properties["Tool"] = processor.ToolIdentifier;
            var cacheEntry = cache.Access(inputPackage);
            if (cacheEntry != null)
            {
                Console.WriteLine("Found result in cache");
            }
            else
            {
                var outputPackage = processor.Process(inputPackage);
                cacheEntry = new CacheEntry(inputPackage, outputPackage);
                cache.Write(cacheEntry);
            }
            return cacheEntry.Output;
        }
    }
}

