﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using audAssetBuilderCommon;
using rage;
using Rockstar.Audio.Data;
using Rockstar.Audio.Pipeline;
using rage.ToolLib;

namespace LegacyWaveEncoder
{
    public class LegacyWaveEncoder: WaveBuildBase
    {
        public LegacyWaveEncoder(string[] args)
            : base(args)
        {
        }

        public override void Run()
        {
            var inputPackage = new Package();
            var inputWaveFile = new PackageFile();
            inputWaveFile.Object = new PackageWaveObject(new WaveSample(WaveFile));
            inputWaveFile.SetObjectNameAndTypeFromFileName(WaveFile.FileName);
            inputPackage.Files.Add(inputWaveFile);

            audProjectSettings projectSettings = new audProjectSettings(ProjectSettingsPath);
            projectSettings.SetCurrentPlatformByTag(Platform);
            string platformEncoder = projectSettings.GetCurrentPlatform().Encoder;
            bool isBigEndian = projectSettings.GetCurrentPlatform().IsBigEndian;

            //double check that wave does not need to be pcm
            //We are checking that the number of samples in a wave against (aligment samples * 1.5) so that the xma encoder 
            //doesn't resample it down 128 and cause us issues
            if (WaveProperties.BuiltAsset.Compression == 101)
            {
                //uncompressed files don't get encoded
                inputWaveFile.Properties.SetValue(WaveEncode.COMPRESSION_TYPE, WaveEncode.CompressionType.PCM.ToString());
            }
            else if (platformEncoder.Equals("PCMENCODER", StringComparison.OrdinalIgnoreCase) ||
                WaveFile.Data.NumSamples <= (projectSettings.GetCurrentPlatform().AlignmentSamples * 1.5))
            {
                if (WaveProperties.BuiltAsset.Compression == 1 || WaveProperties.BuiltAsset.CompressionType.Equals("ADPCM", StringComparison.OrdinalIgnoreCase))
                {
                    inputWaveFile.Properties.SetValue(WaveEncode.COMPRESSION_TYPE, WaveEncode.CompressionType.ADPCM.ToString());
                }
                else if (WaveProperties.BuiltAsset.CompressionType.Equals("ATRAC9", StringComparison.OrdinalIgnoreCase))
                {
                    throw new Exception("ATRAC9Encoder not implemented for new pipeline");
                }
                else if (WaveFile.Data.NumSamples >= 1152 && WaveProperties.BuiltAsset.CompressionType.Equals("MP3", StringComparison.OrdinalIgnoreCase))
                {
                    inputWaveFile.Properties.SetValue(Rockstar.Audio.Pipeline.WaveEncode.COMPRESSION_TYPE, WaveEncode.CompressionType.MP3.ToString());
                }
                else if (WaveFile.Data.NumSamples >= 128 && WaveProperties.BuiltAsset.CompressionType.Equals("XMA2", StringComparison.OrdinalIgnoreCase))
                {
                    inputWaveFile.Properties.SetValue(WaveEncode.COMPRESSION_TYPE, WaveEncode.CompressionType.XMA2.ToString());
                }
                else
                {
                    inputWaveFile.Properties.SetValue(WaveEncode.COMPRESSION_TYPE, WaveEncode.CompressionType.PCM.ToString());
                }
            }
            else
            {
                switch (platformEncoder.ToUpper())
                {
                    case "XMAENCODER":
                        inputWaveFile.Properties.SetValue(Rockstar.Audio.Pipeline.WaveEncode.COMPRESSION_TYPE, WaveEncode.CompressionType.XMA2.ToString());
                        break;
                    case "PS3ENCODER":
                        inputWaveFile.Properties.SetValue(Rockstar.Audio.Pipeline.WaveEncode.COMPRESSION_TYPE, WaveEncode.CompressionType.MP3.ToString());
                        break;
                }
            }
            if (!inputWaveFile.Properties.Keys.Contains(WaveEncode.COMPRESSION_TYPE))
            {
                throw new Exception(String.Concat("Unable to initialise encoder of type ",
                                                  projectSettings.GetCurrentPlatform().Encoder.ToUpper()));
            }

            WaveProperties.BuiltAsset.FirstPeakSample = ComputePeakEnvelope();

            inputWaveFile.Properties.SetValue(WaveEncode.COMPRESSION_BLOCKSIZE, WaveProperties.BuiltAsset.BlockSize);
            inputWaveFile.Properties.SetValue(WaveEncode.COMPRESSION_QUALITY, WaveProperties.BuiltAsset.Compression);
            inputWaveFile.Properties.SetValue(WaveEncode.ITERATIVE_ENCODING, WaveProperties.BuiltAsset.IterativeEncoding);
            inputWaveFile.Properties.SetValue(WaveEncode.IS_STREAM, StreamingBlockBytes!=0);
            inputWaveFile.Properties.SetValue(WaveEncode.HAS_LIPSYNC, WaveProperties.BuiltAsset.CustomLipsync);
            inputWaveFile.Properties.SetValue(WaveEncode.HEADROOM, ((double)WaveProperties.BuiltAsset.HeadRoom/100.0));
            inputWaveFile.Properties.SetValue(WaveEncode.FIRSTPEAKSAMPLE, WaveProperties.BuiltAsset.FirstPeakSample);
            inputWaveFile.Properties.SetValue(WaveEncode.PRESERVE_TRANSIENT, WaveProperties.BuiltAsset.PreserveTransient);
            
            var outputPackage = ProcessPackage(new CacheClient(), inputPackage, new WaveEncode());
            //WaveProperties.BuiltAsset.SampleRate = (int)WaveFile.Format.SampleRate;
            foreach (PackageFile file in outputPackage.Files)
            {
                if (file.Object is PackageWaveObject)
                {
                    PackageWaveObject waveObj = (PackageWaveObject) file.Object;
                    WaveFile = waveObj.SampleData.Create16BitWaveFile();
                    //WaveProperties.BuiltAsset.Size = (uint) waveObj.SampleData.ChannelData[0].Length; //wrong size for multichannel assets
                    //WaveProperties.BuiltAsset.SampleRate = (int) waveObj.SampleData.SampleRate;
                }
                else
                {
                    if (file.Object is PackageWaveFormatObject)
                    {
                        PackageWaveFormatObject formatChunk = (PackageWaveFormatObject) file.Object;
                        WaveProperties.BuiltAsset.NumberOfSamples = formatChunk.LengthSamples;
                        WaveProperties.BuiltAsset.SampleRate = formatChunk.SampleRate;
                    }
                    else if (file.Object is PackageWaveMarkerObject)
                    {
                        PackageWaveMarkerObject markerChunk = (PackageWaveMarkerObject) file.Object;
                        var markersChunkProperties = this.WaveProperties.BuiltAsset.Chunks.FirstOrDefault(c => c.Name == "MARKERS");
                        if (null == markersChunkProperties)
                        {
                            if (this.WaveProperties.BuiltAsset.AddOrUpdateChunk("MARKERS", 0)) 
                                markersChunkProperties = this.WaveProperties.BuiltAsset.Chunks.First(c => c.Name == "MARKERS");
                            else throw new Exception("Failed to add new MARKERS chunk");
                        }
                        markersChunkProperties.ChunkMarkers.Clear();
                        foreach (PackageWaveMarkerObject.WaveMetadataMarker marker in markerChunk)
                        {
                            uint timeOffset = (uint)(marker.TimeOffset/(this.WaveProperties.BuiltAsset.SampleRate/1000.0f));
                            markersChunkProperties.AddMarker(marker.Label, timeOffset.ToString());
                        }
                    }
                    else if (Enum<ChunkType>.Parse(file.ObjectType, true) == ChunkType.DATA)
                    {
                        WaveProperties.BuiltAsset.Size = (uint) ((PackageGenericObject) file.Object).Data.Length;
                    }
                    using (var fs = new FileStream(Path.Combine(OutputDirectory, file.FileName), FileMode.Create, FileAccess.Write))
                    {

                        if (file.ObjectType.Equals(ChunkType.SEEKTABLE.ToString()) || file.ObjectType.Equals(ChunkType.FORMAT.ToString()))
                        {
                            file.SaveObject(fs, isBigEndian);
                        }
                        else
                        {
                            //endianness is handled in the bankbuilder so save all other files as little endian
                            file.SaveObject(fs, false);
                        }
                    }
                }
            }

        }

        static Package ProcessPackage(CacheClient cache, Package inputPackage, IPipelineStageProcessor processor)
        {
            inputPackage.Properties["Tool"] = processor.ToolIdentifier;
            var cacheEntry = cache.Access(inputPackage);
            if (cacheEntry != null)
            {
                Console.WriteLine("Found result in cache");
            }
            else
            {
                var outputPackage = processor.Process(inputPackage);
                cacheEntry = new CacheEntry(inputPackage, outputPackage);
                cache.Write(cacheEntry);
            }
            return cacheEntry.Output;
        }

        int ComputePeakEnvelope()
        {
            int dataSize = WaveFile.Data.RawData.Length;
            int numSamples = dataSize / 2;

            const int windowSize = 4096;
            int overallPeak = 0;
            List<int> peakSamples = new List<int>();
            int numPeakSamples = Math.Max(1, numSamples / windowSize);
            for (int peakSampleIndex = 0; peakSampleIndex < numPeakSamples; peakSampleIndex++)
            {
                int maxSampleLevel = 0;
                for (int i = 0; i < windowSize; i++)
                {
                    int sampleByteOffset = 2 * (i + windowSize * peakSampleIndex);
                    if (sampleByteOffset >= dataSize)
                    {
                        break;
                    }
                    int currentSample = BitConverter.ToInt16(WaveFile.Data.RawData, sampleByteOffset);
                    int absValue = Math.Min(ushort.MaxValue, Math.Abs(currentSample) * 2);
                    maxSampleLevel = Math.Max(maxSampleLevel, absValue);

                    overallPeak = Math.Max(maxSampleLevel, overallPeak);
                }
                peakSamples.Add(maxSampleLevel);
            }

            // Don't generate peak file for streams
            bool generatePeakData = peakSamples.Count >= 2 && StreamingBlockBytes == 0;
            if (generatePeakData)
            {
                string outputFile = string.Concat(OutputDirectory, Path.GetFileNameWithoutExtension(WaveFile.FileName), ".peak");
                //overwrites peak file if it exists
                if (File.Exists(outputFile)) new FileInfo(outputFile) { IsReadOnly = false };
                FileStream outputStream = new FileStream(outputFile, FileMode.Create);
                BinaryWriter writer = new BinaryWriter(outputStream);

                // Don't repeat the first sample; start from index 1
                for (int i = 1; i < peakSamples.Count; i++)
                {
                    ushort sample = PlatformSpecific.FixEndian((UInt16)(peakSamples[i]), Platform);
                    writer.Write(sample);
                }
                writer.Close();
                outputStream.Close();
            }

            // If the full peak table is turned off for this asset (for example with streams) then store the highest
            // peak, not the first, in the header.
            return generatePeakData ? peakSamples[0] : overallPeak;
        }
    }
}
