﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Rockstar.Audio.Pipeline;

using Wavelib;
namespace PipelineStageHost
{
    class Program
    {

        static string[] FindFiles(string basePath, string mask)
        {
            var files = new List<string>();
            FindFiles(basePath, mask, ref files);
            return files.ToArray<string>();
        }

        static void FindFiles(string basePath, string mask, ref List<string> files)
        {
            foreach (var f in Directory.GetFiles(basePath, mask))
            {
                files.Add(f);
            }
            foreach (var f in Directory.GetDirectories(basePath))
            {
                FindFiles(f, mask, ref files);
            }
        }

        static void Main(string[] args)
        {
            var outputPath = @"x:\testpipeline";

            var cache = new CacheClient(@"x:\audiocache");

            var processedWaves = new List<Package>();
            var inputPath = @"X:\gta5\audio\dev\assets\Waves\RESIDENT\VEHICLES";
            var inputFiles = FindFiles(inputPath, "*.wav");
            Parallel.ForEach(inputFiles, f => 
                {
                    var inputPackage = new Package(Path.GetDirectoryName(f));
                    inputPackage.Files.Add(new PackageFile(f));
                    processedWaves.Add(ProcessWavePipeline(cache, inputPackage));
                });
            
            /*foreach (var f in inputFiles)
            {
                var inputPackage = new Package(inputPath);
                inputPackage.Files.Add(new PackageFile(f));
                processedWaves.Add(ProcessWavePipeline(cache, inputPackage));
            }*/

            IPipelineStageProcessor processor = new BankBuilder();
            var bankInputPackage = new Package();
            foreach (var w in processedWaves)
            {
                foreach (var f in w.Files)
                {
                    bankInputPackage.Files.Add(f);
                }
            }

            bankInputPackage.Properties.Add(BankBuilder.BANK_NAME, "vehicles");
            bankInputPackage.Properties.Add(BankBuilder.BANK_PACKING_TYPE, "BankLoaded");

            var output = ProcessPackage(cache, bankInputPackage, processor);

            output.WriteFiles(outputPath);
        }

        static Package ProcessWavePipeline(CacheClient cache, Package inputPackage)
        {           
            inputPackage.Properties.SetValue(PeakNormalise.PEAK_THRESHOLD, -2.0);
                        
            var intermediateResult = ProcessPackage(cache, inputPackage, new PeakNormalise());
            
            foreach (var f in intermediateResult.Files)
            {
                f.Properties.SetValue(Resample.TARGETED_SAMPLERATE, 32000.0);
                f.Properties.SetValue(Resample.LOOP_ALIGNMENT, 128.0);
            }
            
            intermediateResult = ProcessPackage(cache, intermediateResult, new Resample());
            var peakEnvelopes = ProcessPackage(cache, intermediateResult, new PeakEnvelope());

            //disable Wave Encode to enable building the solution without building the native libraries

            /**
            foreach (var f in intermediateResult.Files)
            {
                // The wave encoder requires the FirstPeakSample property from the PeakEnvelope()
                var peakEnv = (from x in peakEnvelopes.Files where x.ObjectName == f.ObjectName select x).First();
                f.Properties[PeakEnvelope.OUTPUT_PROP_FIRSTPEAKSAMPLE] = peakEnv.Properties[PeakEnvelope.OUTPUT_PROP_FIRSTPEAKSAMPLE];

                f.Properties.SetValue(WaveEncode.PARAM_COMPRESSION_TYPE, WaveEncode.CompressionType.XMA.ToString());
            }

            var output = ProcessPackage(cache, intermediateResult, new WaveEncode());

            // Add the peak envelopes to the output package
            output.Files.AddRange(peakEnvelopes.Files);
            return output;
            **/
            
            return intermediateResult;
        }

        static Package ProcessPackage(CacheClient cache, Package inputPackage, IPipelineStageProcessor processor)
        {
            inputPackage.Properties["Tool"] = processor.ToolIdentifier;
            var cacheEntry = cache.Access(inputPackage);
            if (cacheEntry != null)
            {
                Console.WriteLine("Found result in cache");               
            }
            else
            {
                var outputPackage = processor.Process(inputPackage);
                cacheEntry = new CacheEntry(inputPackage, outputPackage);
                cache.Write(cacheEntry);
            }
            return cacheEntry.Output;
        }
    }
}
