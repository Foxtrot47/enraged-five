#include "stdafx.h"
#include "XMA2Encoder.h"

#include <stdio.h>
#include <malloc.h>
#include <winerror.h>

// From XDK
#include "xmaencoder.h"
#pragma comment(lib, "xmaencoder.lib")

HRESULT XMA2Encoder::Encode( const short *inputSamples, 
							int numChannels,
							DWORD numInputSamples, 
							DWORD compression,
							DWORD sampleRate,
							DWORD blockSizeKB,
							int loopStart,
							int loopEnd,
							void **encodedBuffer,
							DWORD *encodedBufferSize,
							DWORD **seekTable,
							DWORD *seekTableSize,
							XMA2HeaderInfo &headerInfo)
{

	if(numChannels > XMA_MAXINPUTCHANNELCOUNT) throw Error(std::to_string(numChannels)+" channels are not supported. Maximum channels supported are "+std::to_string(XMA_MAXINPUTCHANNELCOUNT));

	XMA2WAVEFORMATEX *format = NULL;
	DWORD formatSize;
	XMAENCODERSTREAM streamDef;
	memset(&streamDef, 0, sizeof(streamDef));

	/*
	typedef struct XMAENCODERSTREAM
{
    WAVEFORMATEXTENSIBLE Format;
    LPCVOID              pBuffer;
    DWORD                BufferSize;
    DWORD                LoopStart;
    DWORD                LoopLength;
    BYTE                 SpeakerAssignment[XMA_MAXINPUTCHANNELCOUNT];
} XMAENCODERSTREAM, *LPXMAENCODERSTREAM;
typedef const XMAENCODERSTREAM* LPCXMAENCODERSTREAM;
*/
	const bool isLooping = loopStart != -1;

	streamDef.pBuffer = inputSamples;
	streamDef.BufferSize = numInputSamples * sizeof(short);
	streamDef.LoopLength = isLooping ? loopEnd - loopStart : 0;
	streamDef.LoopStart = isLooping ? loopStart : 0;

	streamDef.SpeakerAssignment[0] = XMA_SPEAKER_LEFT;
	streamDef.SpeakerAssignment[1] = XMA_SPEAKER_RIGHT;
	

	/*
	    typedef struct tWAVEFORMATEX
		{
			WORD wFormatTag;        // Integer identifier of the format
			WORD nChannels;         // Number of audio channels
			DWORD nSamplesPerSec;   // Audio sample rate
			DWORD nAvgBytesPerSec;  // Bytes per second (possibly approximate)
			WORD nBlockAlign;       // Size in bytes of a sample block (all channels)
			WORD wBitsPerSample;    // Size in bits of a single per-channel sample
			WORD cbSize;            // Bytes of extra data appended to this struct
		} WAVEFORMATEX;
	*/
	streamDef.Format.Format.wFormatTag = WAVE_FORMAT_PCM;
	streamDef.Format.Format.nChannels = numChannels;
	streamDef.Format.Format.nSamplesPerSec = sampleRate;
	streamDef.Format.Format.nBlockAlign = sizeof(short);	
	streamDef.Format.Format.wBitsPerSample = 16;
	streamDef.Format.Format.nAvgBytesPerSec = streamDef.Format.Format.nSamplesPerSec * streamDef.Format.Format.nBlockAlign * streamDef.Format.Format.nChannels;
	streamDef.Format.Format.cbSize = sizeof(WAVEFORMATEXTENSIBLE) - sizeof(WAVEFORMATEX);

	streamDef.Format.dwChannelMask = SPEAKER_FRONT_LEFT;
	streamDef.Format.Samples.wSamplesPerBlock = 1;
	streamDef.Format.Samples.wValidBitsPerSample = 16;
	
	streamDef.Format.SubFormat = KSDATAFORMAT_SUBTYPE_PCM;
		
	
	*encodedBuffer = NULL;
	*encodedBufferSize = 0;
	formatSize = 0;
	*seekTable = NULL;
	*seekTableSize = 0;

	HRESULT hr = XAudio2XMAEncoder(1, 
									&streamDef, 
									compression, 
									(isLooping ? XMAENCODER_LOOP : 0), 
									blockSizeKB, 
									encodedBuffer, 
									encodedBufferSize, 
									&format, 
									&formatSize,
									seekTable, 
									seekTableSize);
	if(format)
	{
		headerInfo.BlockCount = format->BlockCount;
		headerInfo.BytesPerBlock = format->BytesPerBlock;
		headerInfo.ChannelMask = format->ChannelMask;
		headerInfo.EncoderVersion = format->EncoderVersion;
		headerInfo.LoopBegin = format->LoopBegin;
		headerInfo.LoopCount = format->LoopCount;
		headerInfo.LoopLength = format->LoopLength;
		headerInfo.NumStreams = format->NumStreams;
		headerInfo.PlayBegin = format->PlayBegin;
		headerInfo.PlayLength = format->PlayLength;
		headerInfo.SampleRate = format->wfx.nSamplesPerSec;
		headerInfo.SamplesEncoded = format->SamplesEncoded;
		free(format);
	}
	else
	{
		memset(&headerInfo, 0, sizeof(headerInfo));
	}
	return hr;
}

HRESULT XMA2Encoder::Encode( const float *inputSamples, 
							int numChannels,
							DWORD numInputSamples, 
							DWORD compression,
							DWORD sampleRate,
							DWORD blockSizeKB,
							int loopStart,
							int loopEnd,
							void **encodedBuffer,
							DWORD *encodedBufferSize,
							DWORD **seekTable,
							DWORD *seekTableSize,
							XMA2HeaderInfo &headerInfo)
{
	if(numChannels > XMA_MAXINPUTCHANNELCOUNT) throw Error(std::to_string(numChannels)+" channels are not supported. Maximum channels supported are "+std::to_string(XMA_MAXINPUTCHANNELCOUNT));


	XMA2WAVEFORMATEX *format = NULL;
	DWORD formatSize;
	XMAENCODERSTREAM streamDef;
	memset(&streamDef, 0, sizeof(streamDef));

	/*
	typedef struct XMAENCODERSTREAM
{
    WAVEFORMATEXTENSIBLE Format;
    LPCVOID              pBuffer;
    DWORD                BufferSize;
    DWORD                LoopStart;
    DWORD                LoopLength;
    BYTE                 SpeakerAssignment[XMA_MAXINPUTCHANNELCOUNT];
} XMAENCODERSTREAM, *LPXMAENCODERSTREAM;
typedef const XMAENCODERSTREAM* LPCXMAENCODERSTREAM;
*/
	streamDef.pBuffer = inputSamples;
	streamDef.BufferSize = numInputSamples * sizeof(float);
	streamDef.LoopLength = loopEnd - loopStart;
	streamDef.LoopStart = loopStart;

	streamDef.SpeakerAssignment[0] = XMA_SPEAKER_LEFT;
	streamDef.SpeakerAssignment[1] = XMA_SPEAKER_RIGHT;
	
	/*
	    typedef struct tWAVEFORMATEX
    {
        WORD wFormatTag;        // Integer identifier of the format
        WORD nChannels;         // Number of audio channels
        DWORD nSamplesPerSec;   // Audio sample rate
        DWORD nAvgBytesPerSec;  // Bytes per second (possibly approximate)
        WORD nBlockAlign;       // Size in bytes of a sample block (all channels)
        WORD wBitsPerSample;    // Size in bits of a single per-channel sample
        WORD cbSize;            // Bytes of extra data appended to this struct
    } WAVEFORMATEX;
	*/
	streamDef.Format.Format.wFormatTag = WAVE_FORMAT_IEEE_FLOAT;
	streamDef.Format.Format.nChannels = numChannels;
	streamDef.Format.Format.nSamplesPerSec = sampleRate;
	streamDef.Format.Format.nBlockAlign = sizeof(float);	
	streamDef.Format.Format.wBitsPerSample = 32;
	streamDef.Format.Format.nAvgBytesPerSec = streamDef.Format.Format.nSamplesPerSec * streamDef.Format.Format.nBlockAlign * streamDef.Format.Format.nChannels;
	streamDef.Format.Format.cbSize = sizeof(WAVEFORMATEXTENSIBLE) - sizeof(WAVEFORMATEX);

	streamDef.Format.dwChannelMask = SPEAKER_FRONT_LEFT;
	streamDef.Format.Samples.wSamplesPerBlock = 1;
	streamDef.Format.Samples.wValidBitsPerSample = 32;
	
	streamDef.Format.SubFormat = KSDATAFORMAT_SUBTYPE_IEEE_FLOAT;

	*encodedBuffer = NULL;
	*encodedBufferSize = 0;
	formatSize = 0;
	*seekTable = NULL;
	*seekTableSize = 0;

	HRESULT hr = XAudio2XMAEncoder(1, 
									&streamDef, 
									compression, 
									(loopStart != -1 ? XMAENCODER_LOOP : 0), 
									blockSizeKB, 
									encodedBuffer, 
									encodedBufferSize, 
									&format, 
									&formatSize,
									seekTable, 
									seekTableSize);
	if(format)
	{
		headerInfo.BlockCount = format->BlockCount;
		headerInfo.BytesPerBlock = format->BytesPerBlock;
		headerInfo.ChannelMask = format->ChannelMask;
		headerInfo.EncoderVersion = format->EncoderVersion;
		headerInfo.LoopBegin = format->LoopBegin;
		headerInfo.LoopCount = format->LoopCount;
		headerInfo.LoopLength = format->LoopLength;
		headerInfo.NumStreams = format->NumStreams;
		headerInfo.PlayBegin = format->PlayBegin;
		headerInfo.PlayLength = format->PlayLength;
		headerInfo.SampleRate = format->wfx.nSamplesPerSec;
		headerInfo.SamplesEncoded = format->SamplesEncoded;
		free(format);
	}
	else
	{
		memset(&headerInfo, 0, sizeof(headerInfo));
	}
	return hr;
}