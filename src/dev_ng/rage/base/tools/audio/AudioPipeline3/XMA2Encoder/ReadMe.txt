========================================================================
    STATIC LIBRARY : XMA2EncoderLib Project Overview
========================================================================

AppWizard has created this XMA2EncoderLib library project for you.

This file contains a summary of what you will find in each of the files that
make up your XMA2EncoderLib application.


XMA2EncoderLib.vcxproj
    This is the main project file for VC++ projects generated using an Application Wizard.
    It contains information about the version of Visual C++ that generated the file, and
    information about the platforms, configurations, and project features selected with the
    Application Wizard.

XMA2EncoderLib.vcxproj.filters
    This is the filters file for VC++ projects generated using an Application Wizard. 
    It contains information about the association between the files in your project 
    and the filters. This association is used in the IDE to show grouping of files with
    similar extensions under a specific node (for e.g. ".cpp" files are associated with the
    "Source Files" filter).


/////////////////////////////////////////////////////////////////////////////

StdAfx.h, StdAfx.cpp
    These files are used to build a precompiled header (PCH) file
    named XMA2EncoderLib.pch and a precompiled types file named StdAfx.obj.

/////////////////////////////////////////////////////////////////////////////
Other notes:

AppWizard uses "TODO:" comments to indicate parts of the source code you
should add to or customize.

Please install the Microsoft Durango SDK before compiling the encoder.
The project should be configured as a Dynamic library with Common Language Runtime Support.
VC++ include directories should contain "$(DurangoXDK)\PC\include", 
VC++ library directory should contain "$(DurangoXDK)\PC\lib\amd64;$(VCInstallDir)lib\amd64" 
and the linker should have "xmaencoder.lib" as additional dependency. 
The project platform needs to be x64.

/////////////////////////////////////////////////////////////////////////////
