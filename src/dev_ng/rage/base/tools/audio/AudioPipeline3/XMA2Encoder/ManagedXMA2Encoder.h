// ManagedXMA2Encoder.h

#pragma once

using namespace System;

namespace WaveEncoder {

	public ref class XMA2Header
	{
	public:
		unsigned short NumStreams;        // Number of audio streams (1 or 2 channels each)
		unsigned int ChannelMask;       // Spatial positions of the channels in this file,
		// stored as SPEAKER_xxx values (see audiodefs.h)
		unsigned int SamplesEncoded;    // Total number of PCM samples the file decodes to
		unsigned int BytesPerBlock;     // XMA block size (but the last one may be shorter)
		unsigned int PlayBegin;         // First valid sample in the decoded audio
		unsigned int PlayLength;        // Length of the valid part of the decoded audio
		unsigned int LoopBegin;         // Beginning of the loop region in decoded sample terms
		unsigned int LoopLength;        // Length of the loop region in decoded sample terms
		unsigned char LoopCount;         // Number of loop repetitions; 255 = infinite
		unsigned char EncoderVersion;    // Version of XMA encoder that generated the file
		unsigned short BlockCount;        // XMA blocks in file (and entries in its seek table)

		unsigned int SampleRate;
	};


	public ref class XMA2Encoder
	{
	public:
		void EncodeBuffer(array<short> ^inputSamples, int numChannels, int compression, int sampleRate, int loopStart, int loopEnd, int blockSize);
		void EncodeBuffer(array<float> ^inputSamples, int numChannels, int compression, int sampleRate, int loopStart, int loopEnd, int blockSize);

		array<unsigned char> ^GetEncodedData() { return m_EncodedData; }
		array<unsigned int> ^GetSeekTable() { return m_SeekTable; }

		XMA2Header ^GetHeaderInfo() { return %m_HeaderInfo; }
	private:

		array<unsigned char> ^m_EncodedData;
		array<unsigned int> ^m_SeekTable;
		XMA2Header m_HeaderInfo;
	};
}
