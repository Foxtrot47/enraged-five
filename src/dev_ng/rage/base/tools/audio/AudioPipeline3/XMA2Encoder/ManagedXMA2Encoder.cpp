// This is the main DLL file.

#include "stdafx.h"

#include "ManagedXMA2Encoder.h"
#include "XMA2Encoder.h"
#include <malloc.h>
#include <winerror.h>

//#pragma comment(lib, "xmaencoder.lib")

using namespace System::Runtime::InteropServices;

namespace WaveEncoder
{
	void XMA2Encoder::EncodeBuffer(array<short> ^inputSamples, int numChannels, int compression, int sampleRate, int loopStart, int loopEnd, int blockSize)
	{
		const pin_ptr<short> inputSamplesPinned = &inputSamples[inputSamples->GetLowerBound(0)];
		void *encodedBuffer;
		DWORD encodedBufferSize;
		DWORD *seekTable;
		DWORD seekTableSize;
		XMA2HeaderInfo headerInfo;
		HRESULT hr = ::XMA2Encoder::Encode(inputSamplesPinned, 
			numChannels,
			inputSamples->Length, 
			compression, 
			sampleRate,
			blockSize,
			loopStart,
			loopEnd,
			&encodedBuffer, 
			&encodedBufferSize, 
			&seekTable, 
			&seekTableSize,
			headerInfo);

		if(!SUCCEEDED(hr))
		{
			Marshal::ThrowExceptionForHR(hr);		
		}

		m_EncodedData = gcnew array<unsigned char>(encodedBufferSize);
		m_SeekTable = gcnew array<unsigned int>(seekTableSize/4);

		pin_ptr<unsigned char> outputDataPinned = &m_EncodedData[m_EncodedData->GetLowerBound(0)];
		memcpy(outputDataPinned, encodedBuffer, encodedBufferSize);
		free(encodedBuffer);

		pin_ptr<unsigned int> seekTablePinned = &m_SeekTable[m_SeekTable->GetLowerBound(0)];
		memcpy(seekTablePinned, seekTable, seekTableSize);
		free(seekTable);

		m_HeaderInfo.BlockCount = headerInfo.BlockCount;
		m_HeaderInfo.BytesPerBlock = headerInfo.BytesPerBlock;
		m_HeaderInfo.ChannelMask = headerInfo.ChannelMask;
		m_HeaderInfo.EncoderVersion = headerInfo.EncoderVersion;
		m_HeaderInfo.LoopBegin = headerInfo.LoopBegin;		
		m_HeaderInfo.LoopCount = headerInfo.LoopCount;
		m_HeaderInfo.LoopLength = headerInfo.LoopLength;
		m_HeaderInfo.NumStreams	 = headerInfo.NumStreams;
		m_HeaderInfo.PlayBegin = headerInfo.PlayBegin;
		m_HeaderInfo.PlayLength = headerInfo.PlayLength;
		m_HeaderInfo.SampleRate = headerInfo.SampleRate;
		m_HeaderInfo.SamplesEncoded = headerInfo.SamplesEncoded;
	}

	void XMA2Encoder::EncodeBuffer(array<float> ^inputSamples, int numChannels, int compression, int sampleRate, int loopStart, int loopEnd, int blockSize)
	{
		const pin_ptr<float> inputSamplesPinned = &inputSamples[inputSamples->GetLowerBound(0)];
		void *encodedBuffer;
		DWORD encodedBufferSize;
		DWORD *seekTable;
		DWORD seekTableSize;
		XMA2HeaderInfo headerInfo;
		HRESULT hr = ::XMA2Encoder::Encode(inputSamplesPinned, 
			numChannels,
			inputSamples->Length, 
			compression, 
			sampleRate,
			blockSize,
			loopStart,
			loopEnd,
			&encodedBuffer, 
			&encodedBufferSize, 
			&seekTable, 
			&seekTableSize,
			headerInfo);

		if(!SUCCEEDED(hr))
		{
			Marshal::ThrowExceptionForHR(hr);		
		}

		m_EncodedData = gcnew array<unsigned char>(encodedBufferSize);
		m_SeekTable = gcnew array<unsigned int>(seekTableSize);

		pin_ptr<unsigned char> outputDataPinned = &m_EncodedData[m_EncodedData->GetLowerBound(0)];
		memcpy(outputDataPinned, encodedBuffer, encodedBufferSize);
		free(encodedBuffer);

		pin_ptr<unsigned int> seekTablePinned = &m_SeekTable[m_SeekTable->GetLowerBound(0)];
		memcpy(seekTablePinned, seekTable, seekTableSize);
		free(seekTable);

		m_HeaderInfo.BlockCount = headerInfo.BlockCount;
		m_HeaderInfo.BytesPerBlock = headerInfo.BytesPerBlock;
		m_HeaderInfo.ChannelMask = headerInfo.ChannelMask;
		m_HeaderInfo.EncoderVersion = headerInfo.EncoderVersion;
		m_HeaderInfo.LoopBegin = headerInfo.LoopBegin;		
		m_HeaderInfo.LoopCount = headerInfo.LoopCount;
		m_HeaderInfo.LoopLength = headerInfo.LoopLength;
		m_HeaderInfo.NumStreams	 = headerInfo.NumStreams;
		m_HeaderInfo.PlayBegin = headerInfo.PlayBegin;
		m_HeaderInfo.PlayLength = headerInfo.PlayLength;
		m_HeaderInfo.SampleRate = headerInfo.SampleRate;
		m_HeaderInfo.SamplesEncoded = headerInfo.SamplesEncoded;
	}
}