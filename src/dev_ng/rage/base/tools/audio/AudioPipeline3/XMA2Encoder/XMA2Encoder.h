#pragma once

#define _AMD64_
#include "WinDef.h"
#include <string>

struct XMA2HeaderInfo
{
    WORD  NumStreams;        // Number of audio streams (1 or 2 channels each)
    DWORD ChannelMask;       // Spatial positions of the channels in this file,
                             // stored as SPEAKER_xxx values (see audiodefs.h)
    DWORD SamplesEncoded;    // Total number of PCM samples the file decodes to
    DWORD BytesPerBlock;     // XMA block size (but the last one may be shorter)
    DWORD PlayBegin;         // First valid sample in the decoded audio
    DWORD PlayLength;        // Length of the valid part of the decoded audio
    DWORD LoopBegin;         // Beginning of the loop region in decoded sample terms
    DWORD LoopLength;        // Length of the loop region in decoded sample terms
    BYTE  LoopCount;         // Number of loop repetitions; 255 = infinite
    BYTE  EncoderVersion;    // Version of XMA encoder that generated the file
    WORD  BlockCount;        // XMA blocks in file (and entries in its seek table)

	DWORD SampleRate;
};

class XMA2Encoder
{
public:
	static HRESULT Encode(	const short *inputSamples, 
							int numChannels,
							DWORD numInputSamples, 
							DWORD compression,
							DWORD sampleRate,
							DWORD blockSizeKB,
							int loopStart,
							int loopEnd,
							void **encodedBuffer,
							DWORD *encodedBufferSize,
							DWORD **seekTable,
							DWORD *seekTableSize,
							XMA2HeaderInfo &headerInfo);

	static HRESULT Encode(	const float *inputSamples, 
							int numChannels,
							DWORD numInputSamples, 
							DWORD compression,
							DWORD sampleRate,
							DWORD blockSizeKB,
							int loopStart,
							int loopEnd,
							void **encodedBuffer,
							DWORD *encodedBufferSize,
							DWORD **seekTable,
							DWORD *seekTableSize,
							XMA2HeaderInfo &headerInfo);
};

public struct Error
{
	explicit Error(std::string const& message) : message_(message) { }
	char const* what() const throw() { return message_.c_str(); }

private:
	std::string message_;
};

