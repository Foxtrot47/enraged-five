#include "IMA_ADPCM.h"
#include <string>

public class ADPCMEncoder
{
public:
	static void EncodeADPCMStream(unsigned char*& encodeBuffer, unsigned int& encodeBufferSize, int inputLength, unsigned char* inputData);
	static void DeleteBuffer(void *buffer);

};

public struct Error
{
	explicit Error(std::string const& message) : message_(message) { }
	char const* what() const throw() { return message_.c_str(); }

private:
	std::string message_;
};