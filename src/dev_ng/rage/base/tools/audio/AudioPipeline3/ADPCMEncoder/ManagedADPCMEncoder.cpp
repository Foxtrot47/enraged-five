#include "stdafx.h"
#include "ManagedADPCMEncoder.h"
#include "ADPCMEncoder.h"

using namespace System;
using namespace System::Runtime::InteropServices;

namespace WaveEncoder
{
	array<unsigned char> ^ADPCMEncoder::EncodeADPCM(array<unsigned char> ^inputData)
	{
		pin_ptr<unsigned char> inputDataPinned = &inputData[inputData->GetLowerBound(0)];

		unsigned int encodedDataSize = -1;
		unsigned char* encodedData;
		try
		{
			::ADPCMEncoder::EncodeADPCMStream(encodedData, encodedDataSize, inputData->Length, inputDataPinned);
			array<unsigned char> ^managedEncodedData = gcnew array<unsigned char>(encodedDataSize);
			Marshal::Copy( (IntPtr)encodedData, managedEncodedData, 0, encodedDataSize);
			::ADPCMEncoder::DeleteBuffer(encodedData);
			return managedEncodedData;
		}
		catch(Error const& err){
			throw gcnew Exception(gcnew String(err.what()));
		}
	}
}
