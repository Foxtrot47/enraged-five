// ADPCMEncoder.h

#pragma once

using namespace System;

namespace WaveEncoder {

	public ref class ADPCMEncoder
	{
	public:
		static array<unsigned char> ^ADPCMEncoder::EncodeADPCM(array<unsigned char> ^inputSamples);
	};
}