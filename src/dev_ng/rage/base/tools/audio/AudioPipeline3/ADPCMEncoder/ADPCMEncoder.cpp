#include "stdafx.h"
#include "ADPCMEncoder.h"
#include "memory.h"
#include "math.h"
#include <string>

using namespace System::Collections::Generic;
using namespace System::IO;


// Every 2k packet has it's own predicted value and step index
void ADPCMEncoder::EncodeADPCMStream(unsigned char*& encodeBuffer, unsigned int& encodeBufferSize, int inputLength, unsigned char* inputData)
{
	// For each 2048 bytes of encoded data we have a 4 byte header, 
	// that means we have 2048-4 bytes of adpcm data = 2044*4 bytes pcm = 8176 bytes pcm data

	IMA_ADPCM adpcm;

	char* tempWaveSampleDataOut = new char[(inputLength/2)];	// allocate way too much memory for encoded file(double)
	if( !tempWaveSampleDataOut )
		throw Error("Could not allocate memory for temp buffer");

	char* tempDataPtr = tempWaveSampleDataOut;

	// Calculate hoe many full blocks we need to encode.
	// Each block is 2048 bytes the first 4 bytes holds the Predicted Value and Step Index for the block.
	// That leaves 2044 bytes of encoded ADPCM data or 8176 bytes of PCM data which is 4088 PCM samples.
	int iBlocks = inputLength / 8176;	 

	uint8_t* pIn = (uint8_t*)inputData;

	uint32_t encodedBytesPCM = 0;
	uint32_t totalLength = 0; //adpcm bytes
	for( int i=0; i<iBlocks; i++ )
	{
		int16_t pv;
		uint8_t sv;

		uint32_t length = 0;

		int16_t* ptr = (int16_t*)pIn;

		adpcm.EncodeInit(ptr[0], ptr[1]);

		pv = adpcm.PredictedValue;
		sv = adpcm.StepIndex;

		// store predicted value and step index in first 4 bytes
		uint16_t* ppp = (uint16_t*)tempDataPtr;
		ppp[0] = (uint16_t)sv;
		tempDataPtr+=2;
		int16_t* ippp = (int16_t*)tempDataPtr;
		ippp[0] = (int16_t)pv;
		tempDataPtr+=2;

		length = adpcm.Encode((uint8_t*)tempDataPtr, 0, (const int16_t*)pIn, 8176);	// 8176 is PCM bytes we're encoding
		length /= 8; // returned length was in bits, we need bytes

		totalLength += (length+4);
		pIn += 8176;
		tempDataPtr += length;	// Length should be 2044, which is the number of ADPCM bytes output by the encoder.
		encodedBytesPCM += 8176;
	}

	// Check to see if we have any remaining data, this is our final non full block.
	if( encodedBytesPCM < inputLength )	
	{
		int bytesToEncode = inputLength - encodedBytesPCM;
		//Assert(bytesToEncode < 8176); // Make sure we have less than a full block

		int16_t pv;
		uint8_t sv;

		uint32_t length = 0;

		int16_t* ptr = (int16_t*)pIn;

		adpcm.EncodeInit(ptr[0], ptr[1]);

		pv = adpcm.PredictedValue;
		sv = adpcm.StepIndex;

		// store predicted value and step index in first 4 bytes
		uint16_t* ppp = (uint16_t*)tempDataPtr;
		ppp[0] = (uint16_t)sv;
		tempDataPtr+=2;
		int16_t* ippp = (int16_t*)tempDataPtr;
		ippp[0] = (int16_t)pv;
		tempDataPtr+=2;

		length = adpcm.Encode((uint8_t*)tempDataPtr, 0, (const int16_t*)pIn, bytesToEncode);	// 8176 is PCM bytes we're encoding
		length /= 8; // returned length was in bits, we need bytes

		totalLength += (length+4);
		pIn += bytesToEncode;
		tempDataPtr += length;	// Number of ADPCM bytes we have encoded.
		encodedBytesPCM += length*4;

	}

	encodeBuffer = new unsigned char[totalLength];
	if( !encodeBuffer )
	{
		delete[] tempWaveSampleDataOut;
		throw Error("Could not allocate memory for result buffer");
	}
	encodeBufferSize = totalLength;
	memcpy(encodeBuffer, tempWaveSampleDataOut, totalLength);

	//tempDataPtr = tempWaveSampleDataOut;

	//char fileName[256];
	//sprintf(fileName, "c:\\bm_wav%d.raw", g_AdpcmFileNoBS++);
	//FILE* fp = fopen(fileName, "wb");
	//if(fp)
	//{
	//	u8* decode = (u8*)new char[totalLength*4];
	//	memset( decode, 0, totalLength*4 );
	//	for(int i=0; i<iBlocks; i++)
	//	{
	//		IMA_ADPCM a;
	//		s16* v = (s16*)tempDataPtr;
	//		a.PredictedValue = v[1];
	//		a.StepIndex = (u8)v[0];
	//		tempDataPtr += 4;
	//		int bytesDecoded = a.Decode((int16_t*)decode, (uint8_t*)tempDataPtr,0,2044);
	//		fwrite(decode,bytesDecoded,1,fp);
	//		tempDataPtr += 2044;
	//	}
	//	fclose(fp);
	//	delete [] decode;
	//}

	delete [] tempWaveSampleDataOut;
}

void ADPCMEncoder::DeleteBuffer(void *buffer)
{
	delete[] buffer;
}