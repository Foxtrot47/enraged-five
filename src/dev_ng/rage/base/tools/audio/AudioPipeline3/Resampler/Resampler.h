// Resampler.h

#pragma once

using namespace System;
#include "soxr.h"

namespace Rockstar
{
	namespace Audio
	{
		namespace Pipeline
		{
			public ref class Resampler
			{

			private:
				static bool ProcessNative(const float *const input, float *const output, int inputLength, int outputLength, double ratio, int targetSampleRate, int prerollSamples, int postrollSamples)
				{
					/*
						From SoX FAQ:
						Phase setting: if resampling to < 40k, use intermediate phase (-I), otherwise use linear phase (-L, or don't specify; linear phase is the default).
						Quality setting: if resampling (or changing speed, as it amounts to the same thing) at/to > 16 bit depth (i.e. most commonly 24-bit), use VHQ (-v), otherwise, use HQ (-h, or don't specify).
					*/
			
					soxr_quality_spec_t qualitySpec = soxr_quality_spec(SOXR_VHQ,
																		SOXR_INTERMEDIATE_PHASE);
					soxr_io_spec_t ioSpec = soxr_io_spec(SOXR_FLOAT32_I, SOXR_FLOAT32_I);
					size_t odone = 0;
					size_t idone = 0;
					soxr_error_t error = soxr_oneshot(1.0, ratio, 1, /* Rates and # of chans. */
						input, inputLength, &idone,                              /* Input. */
						output + prerollSamples, outputLength - prerollSamples - postrollSamples, &odone,
						&ioSpec, &qualitySpec, NULL);                             /* Default configuration.*/

					for(int i = 0; i < prerollSamples; i++)
					{
						output[i] = 0.0f;
					}
					// This will include postroll
					for(size_t i = odone + prerollSamples; i < (size_t)outputLength; i++)
					{
						output[i] = 0.0f;
					}

					return !!error;	
				}
			public:
				static array<float> ^Process(array<float>^ inputSamples, double rate, int targetSampleRate, int prerollSamples, int postrollSamples)
				{
					int outputLength = (int)(rate * inputSamples->Length + 0.5);
					array<float> ^outputSamples = gcnew array<float>(outputLength + prerollSamples + postrollSamples);

					
					pin_ptr<float> outputArrayPinned = &outputSamples[outputSamples->GetLowerBound(0)];
					const pin_ptr<float> inputArrayPinned = &inputSamples[inputSamples->GetLowerBound(0)];

					ProcessNative(inputArrayPinned, outputArrayPinned, inputSamples->Length, outputSamples->Length, rate, targetSampleRate, prerollSamples, postrollSamples);
					
					return outputSamples;
				}

			};

		}
	}
}
