﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegacyResampler
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var r = new LegacyResampler.Resampler(args);
                r.Execute();
            }             
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.ToString());
                Environment.ExitCode = -1;
            }
        }
    }
}
