﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using audAssetBuilderCommon;
using rage;
using Rockstar.Audio.Pipeline;

namespace LegacyResampler
{
    class Resampler : WaveBuildBase
    {
        private static int[] validPS3SampleRates =
        {
            8000,
            11025,
            12000,
            16000,
            22050,
            24000,
            32000,
            44100,
            48000
        };

        public Resampler(string[] args)
            : base(args)
        {
        }

        public override void Run()
        {
	        var projectSettings = new audProjectSettings(ProjectSettingsPath);
	        projectSettings.SetCurrentPlatformByTag(Platform);

	        var alignmentSamples = projectSettings.GetCurrentPlatform().AlignmentSamples;
            var encoder = projectSettings.GetCurrentPlatform().Encoder;

	        if(WaveProperties == null)
	        {
		        throw new Exception(string.Concat("No wave properties file found for ", WaveFile.FileName));
	        }		

	        // Populate the source asset properties
	        WaveProperties.SourceAsset.SampleRate = (int)WaveFile.Format.SampleRate;
	        WaveProperties.SourceAsset.Size = WaveFile.Data.RawData.Length;
	        WaveProperties.SourceAsset.NumberOfSamples = WaveFile.Data.NumSamples;
	
	        // if there was no sampleRate tag then use the source sample rate
	        if (!WaveProperties.BuiltAsset.HasValidSampleRate())
	        {
		        WaveProperties.BuiltAsset.SampleRate = WaveProperties.SourceAsset.SampleRate;
	        }

            //ensure its a valid sample rate for the ps3encoder
            if (StreamingBlockBytes != 0 && encoder.Equals("PS3ENCODER", StringComparison.OrdinalIgnoreCase))
	        {
		        int sampleRate = validPS3SampleRates[0];
		        // Original Comment taken from MResampler.cpp in the old pipeline: 
		        // TODO: Investigate this, as we're clamping to 32kHz as a minimum to ensure 1152 sample frames
		        // this is really a temp work around, since support for 576 sample frames would allow us to utilize
		        // sample rates all the way down to 8kHz.
                for (int i = 6; i < validPS3SampleRates.Length; i++)
		        {
                    if (WaveProperties.BuiltAsset.SampleRate <= validPS3SampleRates[i])
			        {
                        WaveProperties.BuiltAsset.SampleRate = validPS3SampleRates[i];
				        break;
			        }
		        }
	        }

	        if(WaveFile.Data.NumSamples > (uint)alignmentSamples)
	        {               
                var inputPackage = new Package();
                var inputWaveFile = new PackageFile();
                inputWaveFile.Object = new PackageWaveObject(new WaveSample(WaveFile));
                inputWaveFile.SetObjectNameAndTypeFromFileName(WaveFile.FileName);
                inputPackage.Files.Add(inputWaveFile);

                if((encoder.Equals("PCMENCODER", StringComparison.OrdinalIgnoreCase) && string.IsNullOrEmpty(WaveProperties.BuiltAsset.CompressionType)) ||
                        WaveProperties.BuiltAsset.Compression == 101)
                {
                    alignmentSamples = 1;
                }

                var inputFile = inputPackage.Files.First();
                inputFile.Properties.SetValue(Resample.LOOP_ALIGNMENT, alignmentSamples);
                inputFile.Properties.SetValue(Resample.TARGETED_SAMPLERATE, WaveProperties.BuiltAsset.SampleRate);
                inputFile.CalculateHash();

                var outputPackage = ProcessPackage(new CacheClient(), inputPackage, new Resample());

                var outputFile = outputPackage.Files.First();
                // Need an explicit load here in case we are fetching from the cache
                outputFile.LoadObject();
                WaveFile = (outputPackage.Files.First().Object as PackageWaveObject).SampleData.Create16BitWaveFile();

                // Set number of samples after processing
                WaveProperties.BuiltAsset.NumberOfSamplesPostResampling = WaveFile.Data.NumSamples;
	        }
        }

        static Package ProcessPackage(CacheClient cache, Package inputPackage, IPipelineStageProcessor processor)
        {
            inputPackage.Properties["Tool"] = processor.ToolIdentifier;
            var cacheEntry = cache.Access(inputPackage);
            if (cacheEntry != null)
            {
                Console.WriteLine("Found result in cache");
            }
            else
            {
                var outputPackage = processor.Process(inputPackage);
                cacheEntry = new CacheEntry(inputPackage, outputPackage);
                cache.Write(cacheEntry);
            }
            return cacheEntry.Output;
        }
    }
}
