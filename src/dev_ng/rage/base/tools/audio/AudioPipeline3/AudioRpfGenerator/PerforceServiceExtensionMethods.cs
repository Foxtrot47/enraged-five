﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Perforce.P4;
using RSG.Editor.Controls.Perforce;

namespace AudioRpfGenerator
{
    public static class PerforceServiceExtensionMethods
    {
        public static void GetLatest(this PerforceService p4Service, string path, bool force=false)
        {
            List<string> paths = new List<string>();
            paths.Add(path);
            p4Service.GetLatest(paths, force);
        }

        public static string GetDepotPathsFromLocalPath(this PerforceService p4Service, string path)
        {
            List<string> paths = new List<string>();
            paths.Add(path);
            return p4Service.GetDepotPathsFromLocalPaths(paths).First();
        }

        public static string GetLocalPathsFromDepotPath(this PerforceService p4Service, string path)
        {
            List<string> paths = new List<string>();
            paths.Add(path);
            return p4Service.GetLocalPathsFromDepotPaths(paths).First();
        }
    }
}
