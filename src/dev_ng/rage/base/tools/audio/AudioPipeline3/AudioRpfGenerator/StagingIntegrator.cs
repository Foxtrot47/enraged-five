﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using rage;
using RSG.Base.Logging;
using RSG.Configuration;
using RSG.Interop.Perforce;

namespace AudioRpfGenerator
{
    public class StagingIntegrator
    {
        private RSG.Base.Logging.Universal.IUniversalLog Log;
        public StagingIntegrator(RSG.Base.Logging.Universal.IUniversalLog log)
        {
            Log = log;
        }

        public void runIntegration(ITarget target, audRpfGeneratorSettings rpfGenSettings)
        {
            string project = target.Branch.Project.Name;
            string branch = target.Branch.Name;
            string platform = target.Platform.GetAudioPlatform();

            this.Log.Message(project+" "+branch+": Integrating "+platform+" data into stating");

            string integrateToolWorkingPath = rpfGenSettings.P4IntegrateExe;
            string integrateToolExeName = rpfGenSettings.P4IntegrateWorkingPath;
            string integrateToolArgs = "-resolve accepttheirs -branch " + project + "_audio_to_staging_%" + platform;
            integrateToolArgs += getP4Parameters(target);

            this.Log.Message(Path.Combine(integrateToolWorkingPath, integrateToolExeName)+" "+integrateToolArgs);
            try
            {
                string output = CmdRunner.run(Path.Combine(integrateToolWorkingPath, integrateToolExeName),
                    integrateToolArgs,
                    integrateToolWorkingPath);
                this.Log.Message(output);
            }
            catch (CmdRunner.CmdRunnerException ex)
            {
                this.Log.Message(ex.StdErr);
                throw ex;
            }
        }

        private string getP4Parameters(ITarget target)
        {
            using (P4 p4 = new P4(delegate { return true; }))
            {
                p4.Connect();
                FileMapping[] mappings = FileMapping.Create(p4, new string[] { target.Branch.Audio });
                string depotAudioDataPath = mappings.Select(item => item.DepotFilename).First();

                if (!depotAudioDataPath.StartsWith("//")) throw new Exception("depot path " + depotAudioDataPath + " (for " + target.Branch.Audio + ") doen't not start with //");
                List<string> depotPathElements = new List<string>(
                    depotAudioDataPath.Substring(2)
                        .Split(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar)
                        .Reverse());

                foreach (string folder in target.Branch.Audio.Split(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar).Reverse())
                {
                    if (depotPathElements.Any() && folder == depotPathElements.First())
                        depotPathElements.RemoveAt(0);
                    else break;
                }
                string depotAudioRoot = "//" + string.Join("/", depotPathElements);
                return " -p4host " + p4.Port + " -p4client " + p4.ClientName + " -p4user " + p4.Username + " -p4depotroot " + depotAudioRoot;
            }
        }

    }
}
