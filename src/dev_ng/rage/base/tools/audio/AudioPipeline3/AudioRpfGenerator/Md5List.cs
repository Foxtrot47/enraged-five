﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace AudioRpfGenerator
{
    public class Md5List
    {
        private Dictionary<string, string> data = new Dictionary<string, string>();
        private string path;
        public Md5List(string rpfMd5Path)
        {
            path = rpfMd5Path;
            if (File.Exists(rpfMd5Path))
            {
                foreach (string line in File.ReadAllLines(rpfMd5Path))
                {
                    string[] parts = line.Split(',');
                    if (parts.Length != 2) throw new Exception("MD5 file content not in expected format (file,md5)");
                    data[parts[0]] = parts[1];
                }
            }
        }

        public void addOrUpdateEntry(string rpfPath)
        {
            byte[] hash = MD5.Create().ComputeHash(File.ReadAllBytes(rpfPath));
            data[rpfPath]=BitConverter.ToString(hash).Replace("-", ""); 
        }

        public void save()
        {
            File.WriteAllLines(path, data.Select(entry => entry.Key + "," + entry.Value).ToArray());
        }
    }
}
