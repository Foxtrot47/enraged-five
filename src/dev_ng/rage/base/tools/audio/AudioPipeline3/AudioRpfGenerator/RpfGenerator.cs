﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using rage;
using RSG.Configuration;

namespace AudioRpfGenerator
{
    public class RpfGenerator
    {
        private RSG.Base.Logging.Universal.IUniversalLog Log;

        public string Project { get; private set; }
        public string Branch { get; private set; }
        public string Platform { get; private set; }
        public string OutputPlatform { get; private set; }
        public string StagingPath { get; private set; }

        public string PackageReleaseScriptPath { get; private set; }
        public string PackageMainScriptPath { get; private set; }

        public string RageBuilderPath { get; private set; }

        public RpfGenerator(RSG.Base.Logging.Universal.IUniversalLog log, ITarget target)
        {
            Log = log;
            Project = target.Branch.Project.Name;
            Branch = target.Branch.Name;
            Platform = target.Platform.GetAudioPlatform();
            OutputPlatform = target.Platform.GetRpfOutputPlatform();
            StagingPath = Path.Combine(target.Branch.Audio, "staging");

            string toolsRoot = Environment.GetEnvironmentVariable("RS_TOOLSROOT");
            if (toolsRoot == null) throw new Exception("RS_TOOLSROOT path not found in environment");
            RageBuilderPath = Path.Combine(toolsRoot, @"bin\ragebuilder_x64_0378.exe");

            string rubyLib = Environment.GetEnvironmentVariable("RUBY_LIB");
            if (rubyLib == null) throw new Exception("RUBY_LIB path not found in environment");

            switch (Platform.ToLower())
            {
                case "ps4":
                    PackageReleaseScriptPath = Path.Combine(rubyLib,
                        @"util\ragebuilder\package_audio_orbis_release.rbs");
                    PackageMainScriptPath = Path.Combine(rubyLib, @"util\ragebuilder\package_audio_orbis.rbs");
                    break;
                case "xboxone":
                    PackageReleaseScriptPath = Path.Combine(rubyLib,
                        @"util\ragebuilder\package_audio_durango_release.rbs");
                    PackageMainScriptPath = Path.Combine(rubyLib, @"util\ragebuilder\package_audio_durango.rbs");
                    break;
                case "pc":
                    PackageReleaseScriptPath = Path.Combine(rubyLib,
                        @"util\ragebuilder\package_audio_win64_release.rbs");
                    PackageMainScriptPath = Path.Combine(rubyLib, @"util\ragebuilder\package_audio_win64.rbs");
                    break;
                default:
                    throw new Exception("Audio platform " + Platform + " not supported/configured for ragebuilder");
            }

        }

        public void runRpfBuild(audRpfGeneratorSettings rpfGenSettings)
        {
            Log.Message(Project + " " + Branch + ": Building RPFs for " + Platform);

            string rpfRootPath = Path.Combine(rpfGenSettings.RpfRootPath, Branch);
            if (!Directory.Exists(rpfRootPath)) throw new Exception("RPF output path does not exist: " + rpfRootPath);

            string audioOutputPath = Path.Combine(rpfRootPath, OutputPlatform, "audio");
            if (!Directory.Exists(audioOutputPath))
            {
                Log.Message("Path does not exist - creating directory: " + audioOutputPath);
                Directory.CreateDirectory(audioOutputPath);
            }
            Log.Message("Using audio output path: " + audioOutputPath);
            string sfxOutputPath = Path.Combine(audioOutputPath, "sfx");
            if (!Directory.Exists(sfxOutputPath))
            {
                Log.Message("Path does not exist - creating directory: " + sfxOutputPath);
                Directory.CreateDirectory(sfxOutputPath);
            }
            Log.Message("Using sfx output path: " + sfxOutputPath);


            Md5List md5List = new Md5List(Path.Combine(rpfRootPath, OutputPlatform, "md5_list.csv"));

            generateRpfs(sfxOutputPath, md5List, rpfGenSettings);
            generateAudioRpf(Path.Combine(audioOutputPath, "audio.rpf"), PackageMainScriptPath, md5List);
            generateAudioRpf(Path.Combine(audioOutputPath, "audio_rel.rpf"), PackageReleaseScriptPath, md5List);

        }

        private void generateAudioRpf(string outputPath, string packageScriptPath, Md5List md5List)
        {
            string rootPath = Path.Combine(StagingPath, Platform);
            string rageBuilderArgs = packageScriptPath + " -pack " + outputPath + " -rootpath " + rootPath + @"\ -extignore .awc";
            this.Log.Message(RageBuilderPath + " " + rageBuilderArgs);
            try
            {
                this.Log.Message(CmdRunner.run(RageBuilderPath, rageBuilderArgs));
            }
            catch (CmdRunner.CmdRunnerException ex)
            {
                this.Log.Message(ex.StdErr);
                throw ex;
            }
            if (File.Exists(outputPath))
            {
                md5List.addOrUpdateEntry(outputPath);
                md5List.save();
            }
        }

        private void generateRpfs(string sfxOutputPath, Md5List md5List, audRpfGeneratorSettings rpfGenSettings, bool rebuild=false)
        {
            if(rebuild) Log.Message("Generating all audio RPF files...");
            else Log.Message("Generating latest audio RPF files...");

            string inputPath = Path.Combine(StagingPath, Platform, "sfx");
            if (!Directory.Exists(inputPath)) throw new Exception("Path does not exist: " + inputPath);
            Log.Message("Using input path " + inputPath);

            List<string> devPackListEntries = new List<string>();
            List<string> relPackListEntries = new List<string>();
            foreach (string packPath in Directory.GetDirectories(inputPath))
            {
                string packName = (new DirectoryInfo(packPath)).Name;

                if (rpfGenSettings.DevIgnorePackList.Any(pattern => Regex.IsMatch(packName, pattern, RegexOptions.IgnoreCase)) &&
                    !rpfGenSettings.IgnoreExceptionPackList.Any(pattern => Regex.IsMatch(packName, pattern, RegexOptions.IgnoreCase)))
                {
                    Log.Message("Skipping pack for dev pack list: " + packName);
                }
                else
                {
                    devPackListEntries.Add(packName);
                }

                if (rpfGenSettings.ReleaseIgnorePackList[Platform].Any(pattern => Regex.IsMatch(packName, pattern, RegexOptions.IgnoreCase)) &&
                    !rpfGenSettings.IgnoreExceptionPackList.Any(pattern => Regex.IsMatch(packName, pattern, RegexOptions.IgnoreCase)))
                {
                    Log.Message("Skipping pack for release pack list: " + packName);
                }
                else
                {
                    relPackListEntries.Add(packName);
                }

                string rpfPath = Path.Combine(sfxOutputPath, packName + ".rpf");

                if (rebuild || packRequiresBuild(rpfPath, packPath))
                {
                    buildPack(rpfPath, packPath);
                    if (File.Exists(rpfPath))
                    {
                        md5List.addOrUpdateEntry(rpfPath);
                        md5List.save();
                    }
                }
                else Log.Message("Skipping pack - build not required: " + packName);
            }

            string configPath = Path.Combine(StagingPath, Platform, "config");
            if (!Directory.Exists(configPath)) Directory.CreateDirectory(configPath);

            string packListPathMain = Path.Combine(configPath, "packlist.txt");
            Log.Message("Using main packlist path " + packListPathMain);

            string packListPathRelease = Path.Combine(configPath, "packlist_rel.txt");
            Log.Message("Using release packlist path " + packListPathRelease);

            File.WriteAllLines(packListPathMain, devPackListEntries);
            File.WriteAllLines(packListPathRelease, relPackListEntries);
        }


        private bool packRequiresBuild(string rpfPath, string packPath)
        {
            if (!File.Exists(rpfPath)) return true;
            foreach (string file in Directory.GetFiles(packPath))
            {
                if (file.ToLower().EndsWith(".awc"))
                {
                    //If AWC file is newer than RPF file, it needs to be built
                    if ((new FileInfo(file)).CreationTime > (new FileInfo(rpfPath)).CreationTime) return true;
                }
            }
            return false;
        }


        private void buildPack(string rpfPath, string packPath)
        {
            Log.Message("Building pack " + (new DirectoryInfo(packPath)).Name + "...");
            string rageBuilderArgs = PackageMainScriptPath + " -pack " + rpfPath + " -rootpath " + packPath +" -extignore .rpf";
            this.Log.Message(RageBuilderPath + " " + rageBuilderArgs);
            try
            {
                this.Log.Message(CmdRunner.run(RageBuilderPath, rageBuilderArgs));
            }
            catch (CmdRunner.CmdRunnerException ex)
            {
                this.Log.Message(ex.StdErr);
                throw ex;
            }
        }

    }
}
