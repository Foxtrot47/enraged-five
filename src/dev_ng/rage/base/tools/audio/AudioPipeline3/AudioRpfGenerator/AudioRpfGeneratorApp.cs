﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Perforce.P4;
using rage;
using RSG.Base.Extensions;
using RSG.Base.OS;
using RSG.Configuration;
using RSG.Editor.Controls;
using RSG.Editor.Controls.Perforce;
using RSG.Interop.Perforce;

namespace AudioRpfGenerator
{
    class AudioRpfGeneratorApp: RsConsoleApplication
    {
        static int Main(string[] args)
        {
            AudioRpfGeneratorApp app = new AudioRpfGeneratorApp();
            return app.Run();
        }

        

        protected override int ConsoleMain()
        {
            this.Log.Message("-------------------------------");
            this.Log.Message("| Running Audio RPF Generator |");
            this.Log.Message("-------------------------------");
            
            try
            {
                PerforceService p4Service = new PerforceService();

                string buildPath = Path.Combine(this.CommandOptions.Branch.Audio, "build");
                this.Log.Message("Get latest on " +buildPath);
                //p4Service.GetLatest(Path.Combine(buildPath, "..."), true);

                string runtimePath = Path.Combine(this.CommandOptions.Branch.Audio, "runtime");
                this.Log.Message("Get latest on " + runtimePath);
                //p4Service.GetLatest(Path.Combine(runtimePath, "..."), true);

                string stagingPath = Path.Combine(this.CommandOptions.Branch.Audio, "staging");
                this.Log.Message("Get latest on "+stagingPath);
                //p4Service.GetLatest(Path.Combine(stagingPath, "..."), true);

                string projectSettingsPath = Path.Combine(this.CommandOptions.Branch.Audio, "projectSettings.xml");
                this.Log.Message("Get latest on "+projectSettingsPath);
                //p4Service.GetLatest(projectSettingsPath, true);

                string lipsyncAnimsPath = Path.Combine(this.CommandOptions.Branch.Audio, "assets", "LipsyncAnims");
                this.Log.Message("Get latest on "+lipsyncAnimsPath);
                //p4Service.GetLatest(Path.Combine(lipsyncAnimsPath, "..."), true);

                string objectsPath = Path.Combine(this.CommandOptions.Branch.Audio, "assets", "Objects");
                this.Log.Message("Get latest on "+objectsPath);
                //p4Service.GetLatest(Path.Combine(objectsPath, "..."));

                string wavesPath = Path.Combine(this.CommandOptions.Branch.Audio, "assets", "Waves");
                this.Log.Message("Get latest on "+wavesPath);
                //p4Service.GetLatest(Path.Combine(wavesPath, "..."));
                
                audProjectSettings projSettings = new audProjectSettings(projectSettingsPath);
                RpfGenerator rpfGenerator = new RpfGenerator(this.Log);
                StagingIntegrator stagingIntegrator = new StagingIntegrator(this.Log);

                foreach (var platformTarget in CommandOptions.Branch.Targets)
                {
                    if (!platformTarget.Value.Enabled)
                    {
                        this.Log.Warning(platformTarget.Value.Branch.Project.Name + " " + platformTarget.Value.Branch.Name +
                                         ": Platform " + platformTarget.Value.Platform.ToString() + " is disabled!");
                        continue;
                    }
                    stagingIntegrator.runIntegration(platformTarget.Value, projSettings.RpfGeneratorSettings);
                    rpfGenerator.runRpfBuild(platformTarget.Value, projSettings.RpfGeneratorSettings);
                }
            }
            catch (Exception ex)
            {
                this.Log.Error(ex.Message);
                return RSG.Base.OS.ExitCode.Failure;
            }
            return RSG.Base.OS.ExitCode.Success;
        }
        

    }
}
