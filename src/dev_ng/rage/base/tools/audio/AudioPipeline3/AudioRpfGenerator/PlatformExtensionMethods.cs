﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Platform;

namespace AudioRpfGenerator
{
    public static class PlatformExtensionMethods
    {
        public static string GetAudioPlatform(this Platform platform)
        {
            if (platform.ToString().Equals("win64", StringComparison.CurrentCultureIgnoreCase))
                return "PC";
            return platform.ToString();
        }

        public static string GetRpfOutputPlatform(this Platform platform)
        {
            if (platform.GetAudioPlatform().Equals("PC", StringComparison.CurrentCultureIgnoreCase))
                return "X64";
            return platform.GetAudioPlatform();
        }
    }
}
