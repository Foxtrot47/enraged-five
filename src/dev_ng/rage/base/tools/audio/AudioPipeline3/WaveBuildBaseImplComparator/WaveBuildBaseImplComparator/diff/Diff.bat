@echo off
IF %1.==. GOTO ParamError
IF %2.==. GOTO ParamError

fc %1 %2 /n

GOTO End1

:ParamError
  ECHO Please specifiy two files to compare
GOTO End1

:End1