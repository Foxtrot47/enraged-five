﻿using System;
using System.Collections.Generic;
using System.Diagnostics.PerformanceData;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ResamplerComparator;
using Rockstar.Audio.Pipeline;

namespace CacheConcurrencyTester
{
    class Program
    {
        static void Main(string[] args)
        {
            string cacheDir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "cache");
            if (!Directory.Exists(cacheDir)) Directory.CreateDirectory(cacheDir);

            string inputFileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "input\\test.txt");
                
            //logger.writeLine("Set environment variable RS_AUDIO_CACHE to "+cacheDir);
            //Environment.SetEnvironmentVariable("RS_AUDIO_CACHE", cacheDir);

            int numThreads = 100;
            CountdownEvent countdownEvent = new CountdownEvent(numThreads);

            // Start workers.
            for (var i = 0; i < numThreads; i++)
            {
                int i1 = i; //copy to local variable
                new Thread(() => processPackage(i1, countdownEvent, cacheDir, inputFileName)).Start();
            }

            // Wait for workers.
            countdownEvent.Wait();
            Console.WriteLine("Press enter to exit");
            Console.ReadLine();
        }


        private static void processPackage(int attemptCount, CountdownEvent countdownEvent, string cacheDir, string inputFileName)
        {
            CacheClient cache = new CacheClient(cacheDir);

            var inputPackage = new Package();
            var inputFile = new PackageFile(inputFileName);
            inputFile.Object = new PackageGenericObject(File.ReadAllBytes(inputFileName));
            inputPackage.Files.Add(inputFile);

            if (attemptCount < 50) inputFile.Properties.SetValue(dummyTxtFileProcessor.PARAM_APPENDTEXT, "Processing finished!");
            else inputFile.Properties.SetValue(dummyTxtFileProcessor.PARAM_APPENDTEXT, "Second processing finished!");
            inputFile.CalculateHash();

            IPipelineStageProcessor dummyProcessor = new dummyTxtFileProcessor();

            inputPackage.Properties["Tool"] = dummyProcessor.ToolIdentifier;
            var cacheEntry = cache.Access(inputPackage);
            if (cacheEntry != null)
            {
                Console.WriteLine("Thread " + attemptCount + " Found result in cache. (" + Thread.CurrentThread.ManagedThreadId+")");
            }
            else
            {
                var outputPackage = dummyProcessor.Process(inputPackage);
                cacheEntry = new CacheEntry(inputPackage, outputPackage);
                cache.Write(cacheEntry);
                Console.WriteLine("Thread " + attemptCount + " finished processing. (" + Thread.CurrentThread.ManagedThreadId + ")");
            }

            // Signal the CountdownEvent.
            countdownEvent.Signal();
        }

    }

    public class dummyTxtFileProcessor : IPipelineStageProcessor
    {
        public const string PARAM_APPENDTEXT = "AppendText";

        public string ToolIdentifier
        {
            get { return "dummyTxtFileProcessor-1.0"; }
        }

        public IPackage Process(IPackage input)
        {
            var outputPackage = new Package();
            foreach (var inputFile in input.Files)
            {
                inputFile.LoadObject();
                var outputFile = new PackageFile() { ObjectName = inputFile.ObjectName, ObjectType = inputFile.ObjectType };
                var inputTxt = (inputFile.Object as PackageGenericObject).Data;

                string appendText = inputFile.Properties.GetValue(PARAM_APPENDTEXT, "No text to append specified in parameters");
                byte[] appendTxt = Encoding.Default.GetBytes(appendText);
                byte[] newData = new byte[inputTxt.Length + appendTxt.Length];
                Array.Copy(inputTxt, newData, inputTxt.Length);
                Array.Copy(appendTxt, 0, newData, inputTxt.Length, appendTxt.Length);
                outputFile.Object = new PackageGenericObject(newData);

                outputPackage.Files.Add(outputFile);
            }
            return outputPackage;
        }
    }
}
