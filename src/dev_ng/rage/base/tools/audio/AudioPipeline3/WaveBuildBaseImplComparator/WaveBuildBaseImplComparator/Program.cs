﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using audAssetBuilderProperties;
using ResamplerComparator;

namespace WaveBuildBaseImplComarator
{
    class Program
    {

        static void Main(string[] args)
        {

            //Configure WaveBuildBaseImplComperator

            string baseWaveName = "PH_SONG_14_8_RIGHT.WAV";
            string projectSettingsFile = @"X:\rdr3\audio\dev\projectSettings.xml";
            
            string inputDir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "data\\input");
            string outputDir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "data\\output");

            


            string logFile = outputDir + "ComparatorLog.txt";
            using (StreamWriter logWriter = new StreamWriter(logFile))
            {
                Logger logger = new Logger(logWriter);
                logger.writeLine("WaveBuildBaseImplComarator Log");
                logger.writeLine("------------------------------");
                logger.writeLine("Date/Time: " + DateTime.Now.ToUniversalTime());
                logger.writeLine();

                compareResamplers(baseWaveName, projectSettingsFile, inputDir, outputDir, logger);
                //comparePeaknormalisers(baseWaveName, projectSettingsFile, inputDir, outputDir, logger);

            }
            Console.WriteLine("Press enter to exit...");
            Console.ReadLine();
        }


        public static void comparePeaknormalisers(string baseWaveName, string projectSettingsFile, string inputDir, string outputDir, Logger logger)
        {
            string waveBuildBaseImpl1 = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "OldPipelinePeaknormaliser\\audPeakNormalise.exe");
            string waveBuildBaseImpl2 = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "NewPipelinePeaknormaliser\\LegacyPeakNormalise.exe");

            logger.writeLine("Compare: as " + getImplAlias(waveBuildBaseImpl1) + ": " + waveBuildBaseImpl1);
            logger.writeLine("with:    as " + getImplAlias(waveBuildBaseImpl2) + ": " + waveBuildBaseImpl2);

            XmlConiguration xmlConfig = new XmlConiguration();
            xmlConfig.needGranularProcessing = false;
            xmlConfig.blockSize = null;
            xmlConfig.compression = "40";
            xmlConfig.samplerate = "22050";
            xmlConfig.compressionType = null;

            RunBuildBaseImplSetupParams runBuildBaseImplSetupConfiguration = new RunBuildBaseImplSetupParams();
            runBuildBaseImplSetupConfiguration.inputDir = inputDir;
            runBuildBaseImplSetupConfiguration.baseWaveName = baseWaveName;
            runBuildBaseImplSetupConfiguration.round = 1;
            runBuildBaseImplSetupConfiguration.outputDir = outputDir;
            runBuildBaseImplSetupConfiguration.platform = "PS4";
            runBuildBaseImplSetupConfiguration.projectSettingsFile = projectSettingsFile;
            runBuildBaseImplSetupConfiguration.xmlContent = xmlConfig;

            RunBuildBaseImplInfo impl1RunInfo = setupAndRunWaveBuildBaseImplementation(runBuildBaseImplSetupConfiguration, waveBuildBaseImpl1, logger);

            RunBuildBaseImplInfo impl2RunInfo = setupAndRunWaveBuildBaseImplementation(runBuildBaseImplSetupConfiguration, waveBuildBaseImpl2, logger);

            if (!(impl1RunInfo.success && impl2RunInfo.success))
            {
                logger.writeLine("Error occurred during execution. Comparator aborted.");
            }

            CompareWaveProperties(impl1RunInfo.outputWaveFilePath, impl2RunInfo.outputWaveFilePath, logger);
            CompareXml(impl1RunInfo.outputXmlFilePath, impl2RunInfo.outputXmlFilePath, logger);
        }


        private static void compareResamplers(string baseWaveName, string projectSettingsFile, string inputDir, string outputDir, Logger logger)
        {
            string[] platforms = { "XBOX360", "PC", "PS3", "Durango", "PS4" };
            string[] sampleRates = { "22050", "32000", "24000", "12000", "20500" };
            string platform = platforms[0];

            bool waveNeedsGranularProcessing = false;

            //string builtSampleRate = "22050";
            string compression = "40";
            string blockSize = null;
            string compressionType = null;

            string waveBuildBaseImpl1 = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "OldPipelineResampler\\audResampler.exe");
            string waveBuildBaseImpl2 = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "NewPipelineResampler\\LegacyResampler.exe");

            logger.writeLine("Compare: as " + getImplAlias(waveBuildBaseImpl1) + ": " + waveBuildBaseImpl1);
            logger.writeLine("with:    as " + getImplAlias(waveBuildBaseImpl2) + ": " + waveBuildBaseImpl2);

            int round = 0;

            //foreach (string platform in platforms)
            foreach (string builtSampleRate in sampleRates)
            {
                XmlConiguration xmlConfig = new XmlConiguration();
                xmlConfig.needGranularProcessing = waveNeedsGranularProcessing;
                xmlConfig.blockSize = blockSize;
                xmlConfig.compression = compression;
                xmlConfig.samplerate = builtSampleRate;
                xmlConfig.compressionType = compressionType;

                RunBuildBaseImplSetupParams runBuildBaseImplSetupConfiguration = new RunBuildBaseImplSetupParams();
                runBuildBaseImplSetupConfiguration.inputDir = inputDir;
                runBuildBaseImplSetupConfiguration.baseWaveName = baseWaveName;
                runBuildBaseImplSetupConfiguration.round = round;
                runBuildBaseImplSetupConfiguration.outputDir = outputDir;
                runBuildBaseImplSetupConfiguration.platform = platform;
                runBuildBaseImplSetupConfiguration.projectSettingsFile = projectSettingsFile;
                runBuildBaseImplSetupConfiguration.xmlContent = xmlConfig;


                RunBuildBaseImplInfo impl1RunInfo = setupAndRunWaveBuildBaseImplementation(runBuildBaseImplSetupConfiguration, waveBuildBaseImpl1, logger);

                RunBuildBaseImplInfo impl2RunInfo = setupAndRunWaveBuildBaseImplementation(runBuildBaseImplSetupConfiguration, waveBuildBaseImpl2, logger);

                if (!(impl1RunInfo.success && impl2RunInfo.success))
                {
                    logger.writeLine("Error occurred during execution. Comparator aborted.");
                    break;
                }

                logger.writeLine();
                CompareWaveProperties(impl1RunInfo.outputWaveFilePath, impl2RunInfo.outputWaveFilePath, logger);
                CompareXml(impl1RunInfo.outputXmlFilePath, impl2RunInfo.outputXmlFilePath, logger);

                round++;
            }
        }


        public struct XmlConiguration
        {
            public bool needGranularProcessing;
            public string compression;
            public string samplerate;
            public string blockSize;
            public string compressionType;
        }

        public struct RunBuildBaseImplSetupParams
        {
            public string inputDir;
            public string baseWaveName;
            public int round;
            public string outputDir;
            public string platform;
            public string projectSettingsFile;
            public XmlConiguration xmlContent;
        }

        public struct RunBuildBaseImplInfo
        {
            public bool success;
            public string outputWaveFilePath;
            public string outputXmlFilePath;
        }

        private static RunBuildBaseImplInfo setupAndRunWaveBuildBaseImplementation(RunBuildBaseImplSetupParams runBuildBaseImplSetupConfiguration, string waveBuildBaseImpl, Logger logger)
        {
            string waveName = createWaveName(runBuildBaseImplSetupConfiguration.baseWaveName, runBuildBaseImplSetupConfiguration.round, waveBuildBaseImpl);
            string waveFilePath = Path.Combine(runBuildBaseImplSetupConfiguration.inputDir, waveName);
            string xmlFilePath = waveFilePath.Replace(".WAV", ".XML").Replace(".wav", ".XML");
            if (File.Exists(xmlFilePath))
            {
                File.SetAttributes(xmlFilePath, FileAttributes.Normal);
                File.Delete(xmlFilePath);
            }
            if (File.Exists(waveFilePath)) File.SetAttributes(waveFilePath, FileAttributes.Normal);
            File.Copy(Path.Combine(runBuildBaseImplSetupConfiguration.inputDir, runBuildBaseImplSetupConfiguration.baseWaveName), waveFilePath, true);
            CreateXmlConfig(runBuildBaseImplSetupConfiguration.xmlContent, waveName, waveFilePath, xmlFilePath);
            

            bool success = runWaveBuildBaseImplementations(waveBuildBaseImpl, waveFilePath, runBuildBaseImplSetupConfiguration.outputDir, runBuildBaseImplSetupConfiguration.platform,
                runBuildBaseImplSetupConfiguration.projectSettingsFile, logger);

            RunBuildBaseImplInfo resultInfo = new RunBuildBaseImplInfo();
            resultInfo.success = success;
            resultInfo.outputWaveFilePath = Path.Combine(runBuildBaseImplSetupConfiguration.outputDir, waveName);
            resultInfo.outputXmlFilePath = Path.Combine(runBuildBaseImplSetupConfiguration.outputDir, waveName.Replace(".WAV", ".XML").Replace(".wav", ".XML"));

            //keep files in case of error, so they can be inspected
            if (success)
            {
                File.SetAttributes(waveFilePath, FileAttributes.Normal);
                File.SetAttributes(xmlFilePath, FileAttributes.Normal);
                File.Delete(waveFilePath);
                File.Delete(xmlFilePath);
            }

            return resultInfo;
        }

        private static void CompareWaveProperties(string wavePath1, string wavePath2, Logger logger)
        {
            string waveDiff = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "diff\\WaveDiff.bat");
            ProcessStartInfo psInfo = new ProcessStartInfo(waveDiff);
            psInfo.Arguments = "\"" + wavePath1 + "\" \"" + wavePath2 + "\"";
            psInfo.RedirectStandardOutput = true;
            psInfo.RedirectStandardError = true;
            psInfo.UseShellExecute = false;
            Process p = new Process();
            p.StartInfo = psInfo;
            p.Start();

            p.WaitForExit();

            if (p.ExitCode == 0)
            {
                logger.writeLine("Comparing Wave properties (using MediaInfo)");

                string line;
                while ((line = p.StandardOutput.ReadLine()) != null)
                {
                    logger.writeLine(">      "+line);
                }
                logger.writeLine();
                
            }
            else
            {
                string stderrx = p.StandardError.ReadToEnd();
                logger.writeLine("Comparing Wave properties failed");
                logger.writeLine("> Exit code : " + p.ExitCode);
                logger.writeLine("> Error : " + stderrx);
                logger.writeLine();
            }
        }

        private static void CompareXml(string xmlPath1, string xmlPath2, Logger logger)
        {
            string diff = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "diff\\Diff.bat");
            ProcessStartInfo psInfo = new ProcessStartInfo(diff);
            psInfo.Arguments = "\"" + xmlPath1 + "\" \"" + xmlPath2 + "\"";
            psInfo.RedirectStandardOutput = true;
            psInfo.RedirectStandardError = true;
            psInfo.UseShellExecute = false;
            Process p = new Process();
            p.StartInfo = psInfo;
            p.Start();

            p.WaitForExit();

            if (p.ExitCode == 0)
            {
                logger.writeLine("Comparing XML files");

                string line;
                while ((line = p.StandardOutput.ReadLine()) != null)
                {
                    logger.writeLine(">      " + line);
                }
                logger.writeLine();

            }
            else
            {
                logger.writeLine("Comparing XML files failed");
                logger.writeLine("> Exit code : " + p.ExitCode);
                logger.writeLine("> Error : " + p.StandardError.ReadToEnd());
                logger.writeLine();
            }
        }


        private static void CreateXmlConfig(XmlConiguration conf, string waveName, string waveFilePath, string xmlFilePath)
        {
            WaveProperties xmlBuilder = new WaveProperties();
            xmlBuilder.SourceAsset.Name = waveName;
            xmlBuilder.SourceAsset.Path = waveFilePath;
            xmlBuilder.SourceAsset.Granular = conf.needGranularProcessing;

            if (!string.IsNullOrEmpty(conf.samplerate))
                xmlBuilder.BuiltAsset.SampleRate = int.Parse(conf.samplerate);
            if (!string.IsNullOrEmpty(conf.compression))
                xmlBuilder.BuiltAsset.Compression = int.Parse(conf.compression);
            if (!string.IsNullOrEmpty(conf.compressionType))
                xmlBuilder.BuiltAsset.CompressionType = conf.compressionType;
            xmlBuilder.BuiltAsset.BlockSize = string.IsNullOrEmpty(conf.blockSize)? 2 : int.Parse(conf.blockSize);
            xmlBuilder.Save(xmlFilePath);
        }

        private static string createWaveName(string baseWaveName, int round, string waveBuildBaseImpl)
        {
            string waveName = baseWaveName.Replace(".WAV", "").Replace(".wav", "") + "_" + getImplAlias(waveBuildBaseImpl) + "_" + round + ".WAV";
            return waveName;
        }

        private static string getImplAlias(string waveBuildBaseImpl)
        {
            return waveBuildBaseImpl.Substring(waveBuildBaseImpl.LastIndexOf("\\") + 1,
                    waveBuildBaseImpl.LastIndexOf(".") - (waveBuildBaseImpl.LastIndexOf("\\") + 1));
        }


        private static bool runWaveBuildBaseImplementations(string waveBuildBaseImpl, string inputFile, string outputDir, string platform, string projectSettings, Logger logger, string compressionType = null, string streamingBlockBytes = null, string extraParameters = null)
        {

            StringBuilder waveBuildBaseImplParams = new StringBuilder("-inputFile \"" + inputFile + "\"");
            waveBuildBaseImplParams.Append(" -outputDir \"" + outputDir + "\"");
            waveBuildBaseImplParams.Append(" -platform "+platform);
            waveBuildBaseImplParams.Append(" -projectSettings \""+projectSettings +"\"");

            if (!string.IsNullOrEmpty(streamingBlockBytes))
            {
                waveBuildBaseImplParams.Append(" -streamingBlockBytes "+streamingBlockBytes);
            }
            
            if (!string.IsNullOrEmpty(compressionType))
            {
                waveBuildBaseImplParams.Append(" -compressionType "+compressionType);
            }

            if (!string.IsNullOrEmpty(extraParameters))
            {
                waveBuildBaseImplParams.Append(" " + extraParameters);
            }

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            ProcessStartInfo psInfo = new ProcessStartInfo(waveBuildBaseImpl);
            psInfo.Arguments = waveBuildBaseImplParams.ToString();
            psInfo.RedirectStandardOutput = true;
            psInfo.RedirectStandardError = true;
            psInfo.UseShellExecute = false;
            Process p = new Process();
            p.StartInfo = psInfo;
            p.Start();

            string stdoutx = p.StandardOutput.ReadToEnd();
            string stderrx = p.StandardError.ReadToEnd();  

            p.WaitForExit();

            stopwatch.Stop();

            logger.writeLine();
            logger.writeLine("executing " + getImplAlias(waveBuildBaseImpl)+" with "+waveBuildBaseImplParams.ToString());
            logger.writeLine("> took "+stopwatch.Elapsed.TotalSeconds+" seconds");

            if (p.ExitCode != 0)
            {
                logger.writeLine("> Exit code : " + p.ExitCode);
                logger.writeLine("> Stdout : " + stdoutx);
                logger.writeLine("> Stderr : " + stderrx);
                logger.writeLine();
                return false;
            }
            return true;
        }
    }
}
