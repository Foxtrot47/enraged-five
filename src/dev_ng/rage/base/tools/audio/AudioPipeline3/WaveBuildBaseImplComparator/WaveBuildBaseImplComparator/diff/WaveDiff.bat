@echo off
IF %1.==. GOTO ParamError
IF %2.==. GOTO ParamError

set out1=%~nx1_PROPERTIES
set out2=%~nx2_PROPERTIES

"%~dp0MediaInfo" %1 1> %out1%
"%~dp0MediaInfo" %2 1> %out2%

fc %out1% %out2% /n


del %out1%
del %out2%

GOTO End1

:ParamError
  ECHO Please specifiy two wave files to compare
GOTO End1

:End1