﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResamplerComparator
{
    public class Logger
    {
        private bool logToConsole = true;
        private StreamWriter writer;

        public Logger(StreamWriter writer, bool logToConsole = true)
        {
            this.writer = writer;
        }

        public void writeLine(string message="")
        {
            if(logToConsole) Console.WriteLine(message);
            writer.WriteLine(message);
        }
    }
}
