﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using rage.ToolLib.Reader;
using rage.ToolLib.Writer;
using System.Collections.Specialized;

namespace Rockstar.Audio.Data
{
    public class ContainerBankLoaded: Container
    {
        public ContainerBankLoaded(bool hasEncryptedData)
            : base(hasEncryptedData)
        {
            HasFastLookupTable = true;
        }

        protected override ContainerSerializationInfo CalculateChunkOrderAndOffsets(long dataOffset)
        {
            //Create array of chunk/objectId objects
            var chunks = new List<ChunkSerializationInfo>();
            foreach (var o in Objects)
            {
                foreach (var c in o.Chunks) chunks.Add(new ChunkSerializationInfo(o, c));
            }
            chunks.Sort(Comparer<ChunkSerializationInfo>.Create((a, b) => a.Chunk.Alignment.CompareTo(b.Chunk.Alignment)));

            return CalculateChunkOffsets(dataOffset, chunks);
        }

        protected ContainerSerializationInfo CalculateChunkOffsets(long dataOffset, List<ChunkSerializationInfo> orderedChunks)
        {
            ContainerSerializationInfo calculatedOffsetPerChunk = new ContainerSerializationInfo();

            // calculate chunk offsets		
            long currentOffset = dataOffset;
            foreach (var c in orderedChunks)
            {
                // ensure chunk is aligned as requested
                while ((currentOffset % c.Chunk.Alignment) != 0)
                {
                    currentOffset++;
                }
                c.ChunkOffset = currentOffset;
                calculatedOffsetPerChunk.Add(c.Chunk, c);
                currentOffset += c.Chunk.Data.Length;
            }

            return calculatedOffsetPerChunk;
        }


        protected override void SerializeData(IWriter stream, long dataOffset, ContainerSerializationInfo chunkSerializationInfo)
        {
            long currentOffset = dataOffset;
            byte padByte = 0;
            foreach (ChunkSerializationInfo c in chunkSerializationInfo.getOrderedChunkInfo())
            {
                //pad required alignment
                while (currentOffset < c.ChunkOffset)
                {
                    stream.Write(padByte);
                    currentOffset++;
                }

                stream.BinWriter.Write(c.Chunk.Data);

                //save chunk offset
                c.Chunk.SerialisedAtOffset = c.ChunkOffset;

                currentOffset += c.Chunk.Data.Length;
                //save object data size
                if (c.Object.SerialisedSize != null) c.Object.SerialisedSize += c.Chunk.Data.Length;
                else c.Object.SerialisedSize = c.Chunk.Data.Length;
            }
        }

    }
}
