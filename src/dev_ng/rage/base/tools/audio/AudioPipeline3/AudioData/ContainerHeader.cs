﻿using System;
using System.Data;

namespace Rockstar.Audio.Data
{
    public class ContainerHeader
        {
            private const uint MAGIC = 0x54414441;
            private const uint HEADER_SIZE = 16;
            private const uint VERSION = 0xFF000001;

            private const int Flag_FastLookupTable = 1 << 16;
            private const int Flag_ContiguousObjectPacking = 1 << 17;
            private const int Flag_StreamingObjectPacking = 1 << 18;
            private const int Flag_EncryptedData = 1 << 19;

            public uint ObjectCount { get; private set; }
            public uint ChunkCount { get; private set; }
            public bool HasFastLookupTable { get; private set; }
            public bool HasContiguousObjectPacking { get; private set; }
            public bool HasStreamingObjectPacking { get; private set; }
            public bool HasEncryptedData { get; private set; }
            public long Size { get { return HEADER_SIZE; } }

            public ContainerHeader(uint objectCount, uint chunkCount, bool hasFastLookupTable, bool hasContiguousObjectPacking, bool hasStreamingObjectPacking, bool hasEncryptedData)
            {
                ObjectCount = objectCount;
                ChunkCount = chunkCount;
                HasFastLookupTable = hasFastLookupTable;
                HasContiguousObjectPacking = hasContiguousObjectPacking;
                HasStreamingObjectPacking = hasStreamingObjectPacking;
                HasEncryptedData = hasEncryptedData;
            }

            public ContainerHeader(rage.ToolLib.Reader.IReader stream)
            {
                uint magic = stream.ReadUInt();
                if (magic != MAGIC) throw new Exception(MAGIC + " expected");
                int t = ContainerObject.NameHashMask;
                int t2 = ContainerObject.MaxNumDataTableEntries;
                uint packedVersionAndFlags = stream.ReadUInt();
                if (!((packedVersionAndFlags & VERSION) == VERSION)) throw new Exception("Version does not match. Version " + VERSION + " expected.");
                HasFastLookupTable = (packedVersionAndFlags & Flag_FastLookupTable) == Flag_FastLookupTable;
                HasContiguousObjectPacking = (packedVersionAndFlags & Flag_ContiguousObjectPacking) == Flag_ContiguousObjectPacking;
                HasStreamingObjectPacking = (packedVersionAndFlags & Flag_StreamingObjectPacking) == Flag_StreamingObjectPacking;
                HasEncryptedData = (packedVersionAndFlags & Flag_EncryptedData) == Flag_EncryptedData;
                
                ObjectCount = stream.ReadUInt();
                uint dataOffset = stream.ReadUInt();
                dataOffset -= HEADER_SIZE;
                dataOffset -= ObjectCount*ContainerObjectTable.ENTRY_SIZE;
                if (HasFastLookupTable) dataOffset -= ObjectCount * ContainerFastLookupTable.ENTRY_SIZE;
                ChunkCount = dataOffset/ContainerDataTable.ENTRY_SIZE;
            }

            public void Serialize(rage.ToolLib.Writer.IWriter stream)
            {
                stream.Write(MAGIC);

                uint packedVersionAndFlags = VERSION;
                if (HasFastLookupTable) packedVersionAndFlags |= Flag_FastLookupTable;
                if (HasContiguousObjectPacking) packedVersionAndFlags |= Flag_ContiguousObjectPacking;
                if (HasStreamingObjectPacking) packedVersionAndFlags |= Flag_StreamingObjectPacking;
                if (HasEncryptedData) packedVersionAndFlags |= Flag_EncryptedData;

                stream.Write(packedVersionAndFlags);
                stream.Write((uint)ObjectCount);

                uint dataOffset = HEADER_SIZE; // fixed file header
                dataOffset += ContainerObjectTable.ENTRY_SIZE * ObjectCount; // object table size
                if (HasFastLookupTable) dataOffset += ObjectCount * ContainerFastLookupTable.ENTRY_SIZE;
                dataOffset += ContainerDataTable.ENTRY_SIZE * ChunkCount; // object chunk table

                stream.Write(dataOffset);
            }
        }
}
