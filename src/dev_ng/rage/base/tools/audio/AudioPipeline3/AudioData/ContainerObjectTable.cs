﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using rage.ToolLib.Reader;

namespace Rockstar.Audio.Data
{
    public class ContainerObjectTable
    {
        public const uint ENTRY_SIZE = 4;
        public long Size { get { return packedTableEntries.Count * ENTRY_SIZE; } }

        private List<uint> packedTableEntries = new List<uint>();

        public ReadOnlyCollection<uint> PackedTableEntries {
            get { return packedTableEntries.AsReadOnly(); }
        }

        public ContainerObjectTable(List<ContainerObject> objects)
        {
            foreach (var o in objects)
            {
                if (o.Chunks.Count > ContainerObject.MaxNumDataTableEntries)
                {
                    throw new InvalidDataException(string.Format("Object {0} has {1} chunks; max is {2}", o.Name, o.Chunks.Count, ContainerObject.MaxNumDataTableEntries));
                }
                packedTableEntries.Add(o.PackedTableEntry);
            }
        }

        public ContainerObjectTable(IReader stream, uint objectCount)
        {
            for (int i = 0; i < objectCount; i++) packedTableEntries.Add(stream.ReadUInt());
        }

        public void Serialize(rage.ToolLib.Writer.IWriter stream)
        {
            foreach (uint e in packedTableEntries) stream.Write(e);
        }

        public uint getNumberOfChunks(uint packedTableEntry)
        {
            return (packedTableEntry & ContainerObject.NameHashMask);
        }
    }
}
