﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rockstar.Audio.Data
{
    public enum Platform
    {
        PC,
        PS3,
        PS4,
        XBOX360,
        XBOXONE,
        PS5,
        XBOXSERIES
    }
}
