﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using rage.ToolLib;

namespace Rockstar.Audio.Data
{
    public class ContainerObject
    {
        public const int NameHashWidth = 29;
	    public const int NameHashMask = (1<<NameHashWidth)-1;
        public const int NumDataTableEntriesMask = ~NameHashMask;
	    public const int NumDataTableEntriesWidth = 3;
	    public const int MaxNumDataTableEntries = 1<<NumDataTableEntriesWidth;

        public string Name { get; private set; }
        public readonly List<Chunk> Chunks;

        private long? serialisedSize = null;
        public long? SerialisedSize
        {
            get { return serialisedSize; }
            set { serialisedSize = value; }
        }

        public uint NameHash
        {
            get
            {
                var h = new Hash();
                h.Value = Name;
                return h.Key & NameHashMask;
            }
        }

        public uint PackedTableEntry
        {
            get
            {
                //u32 DECLARE_BITFIELD_2(objectNameHash, kObjectNameHashWidth, numDataTableEntries, kNumDataTableEntriesWidth);
                return (NameHash & ContainerObject.NameHashMask)
                        | (uint)(Chunks.Count << ContainerObject.NameHashWidth);
            }
        }

        public ContainerObject(string name)
        {
            Chunks = new List<Chunk>();
            Name = name;
        }

        public Chunk FindChunk(string typeName)
        {
            var h = new Hash();
            h.Value = typeName;
            uint maskedHash = h.Key & Chunk.TypeHashMask;
            foreach (var c in Chunks)
            {
                if (c.TypeHash == maskedHash)
                {
                    return c;
                }
            }
            return null;
        }

    }
}
