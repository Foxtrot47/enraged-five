﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using rage.ToolLib;
using Wavelib;

namespace Rockstar.Audio.Data
{
    public class WaveSample
    {
        public float[][] ChannelData { get; private set;}
        public uint SampleRate { get; private set; }
        public int NumChannels
        {
            get
            {
                return ChannelData.Length;
            }
        }

        public int NumSamples
        {
            get
            {
                return ChannelData[0].Length;
            }
        }

        public int LoopStart { get; private set; }
        public int LoopEnd { get; private set; }

        public bool IsLooping { get { return LoopStart != -1; } }
        public bwMarkerList Markers { get; private set; }

        public WaveSample(uint sampleRate, float[][] channelData, int loopStart, int loopEnd)
        {
            Markers = new bwMarkerList();
            ChannelData = channelData;
            LoopStart = loopStart;
            LoopEnd = loopEnd;
        }

        public WaveSample(string filePath)
            : this(new bwWaveFile(filePath, false))
        {         
        }

        public WaveSample(bwWaveFile waveFile)
        {
            SampleRate = waveFile.Format.SampleRate;
            ChannelData = waveFile.Data.CreateFloatArray();
            Markers = waveFile.Markers;

            if (waveFile.IsLooping())
            {
                if(waveFile.Sample.Loops.First().Start % waveFile.Format.NumChannels != 0 ||
                  waveFile.Sample.Loops.First().End % waveFile.Format.NumChannels != 0)
                    throw new Exception("Unexpected loop position, not aligned to channels for interleaved data");
                LoopStart = (int) waveFile.Sample.Loops.First().Start / waveFile.Format.NumChannels;
                LoopEnd = (int) waveFile.Sample.Loops.First().End / waveFile.Format.NumChannels;
            }
            else
            {
                LoopStart = LoopEnd = -1;
            }
        }

        public short[] GetGameChannelMappedInterleavedShortData(bool dither = true)
        {
            return GetInterleavedShortData(ChannelMapping.GetDefaultChannelPositions(NumChannels), ChannelMapping.GetGameChannelPositions(NumChannels));
        }

        public short[] GetInterleavedShortData(List<ChannelMapping.ChannelPosition> inputMap, List<ChannelMapping.ChannelPosition> outputMap, bool dither = true)
        {
            return CreateInterleavedShortArray(ChannelMapping.mapChannels(ChannelData, inputMap, outputMap), dither);
        }

        public short[] GetInterleavedShortData(bool dither=true)
        {
            return CreateInterleavedShortArray(ChannelData, dither);
        }

        private short[] CreateInterleavedShortArray(float[][] channelData, bool dither)
        {
            var sampleData = new short[NumChannels * NumSamples];

            for (int sampleIndex = 0; sampleIndex < NumSamples; sampleIndex++)
            {
                for (int channelIndex = 0; channelIndex < NumChannels; channelIndex++)
                {
                    float sampleValue = channelData[channelIndex][sampleIndex];
                    short shortValue = dither
                        ? (short)Utility.DitherAndRoundSample16Bit(sampleValue * short.MaxValue)
                        : (short)Utility.RoundSample16Bit(sampleValue * short.MaxValue);
                    sampleData[(sampleIndex * NumChannels) + channelIndex] = shortValue;
                }
            }
            return sampleData;
        }

        public bwWaveFile CreateWaveFile()
        {
            return bwWaveFile.CreateFromChannelData(ChannelData, SampleRate, LoopStart, LoopEnd, Markers);
        }

        public bwWaveFile Create16BitWaveFile()
        {
            return bwWaveFile.Create16BitFromChannelData(ChannelData, SampleRate, LoopStart, LoopEnd, Markers);
        }

        public void SerializeToStream(Stream stream)
        {
            var waveFile = CreateWaveFile();
            waveFile.Markers = Markers;
            waveFile.Save(stream);
        }

        public void Serialize16BitFileToStream(Stream stream)
        {
            var waveFile = Create16BitWaveFile();
            waveFile.Markers = Markers;
            waveFile.Save(stream);
        }
    }
}
