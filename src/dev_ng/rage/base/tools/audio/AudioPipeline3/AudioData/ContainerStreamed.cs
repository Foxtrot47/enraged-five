﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using rage.ToolLib.Writer;

namespace Rockstar.Audio.Data
{
    public class IndexedChunkSerializationInfo: Container.ChunkSerializationInfo
    {
        public IndexedChunkSerializationInfo(int ObjectId, ContainerObject Object, Chunk Chunk)
            : base(Object, Chunk){this.ObjectId = ObjectId;}
        public int ObjectId;
    }

    public class ContainerStreamed: ContainerBankLoaded
    {
        public ContainerStreamed(bool hasEncryptedData)
            : base(hasEncryptedData)
        {
            //Stream loaded containers have a fast lookup table too, so comment that line
            //HasFastLookupTable = false;
            HasStreamingObjectPacking = true;
            Objects.Add(new ContainerObject(""));
        }

        public ContainerObject getGlobalObject()
        {
            return Objects.First();
        }

        protected override ContainerSerializationInfo CalculateChunkOrderAndOffsets(long dataOffset)
        {
            
            //Create array of chunk/objectId objects
            var chunks = new List<ChunkSerializationInfo>();
            int index = 0;
            foreach (var o in Objects)
            {
                foreach (var c in o.Chunks)
                {
                    chunks.Add(new IndexedChunkSerializationInfo(index, o, c));
                }
                index++;
            }

            //sort by alignment
            chunks.Sort(Comparer<ChunkSerializationInfo>.Create((a, b) =>
                // global object data chunk should be last
                    ((IndexedChunkSerializationInfo)a).ObjectId == 0 && a.Chunk.TypeHash == 0x5E ? 1 :
                    a.Chunk.Alignment.CompareTo(b.Chunk.Alignment)
                ));

            return base.CalculateChunkOffsets(dataOffset, chunks);
        }
    }
}
