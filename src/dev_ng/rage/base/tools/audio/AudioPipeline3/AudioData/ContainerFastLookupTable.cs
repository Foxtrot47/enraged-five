﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using rage.ToolLib.Reader;
using rage.ToolLib.Writer;

namespace Rockstar.Audio.Data
{
    public class ContainerFastLookupTable
    {
        public const uint ENTRY_SIZE = 2;
        public long Size { get { return offsets.Count * ENTRY_SIZE; } }

        private List<ushort> offsets=new List<ushort>();

        public IReadOnlyCollection<ushort> Offsets {
            get { return offsets.AsReadOnly(); }
        }

        public ContainerFastLookupTable(List<ContainerObject> objects)
        {
            int currentIndex = 0;
            foreach (var o in objects)
            {
                offsets.Add((ushort)currentIndex);
                currentIndex += o.Chunks.Count;
            }
        }

        public ContainerFastLookupTable(IReader stream, uint objectCount)
        {
            for (int i = 0; i < objectCount; i++)offsets.Add(stream.ReadUShort());
        }

        public void Serialize(IWriter stream)
        {
            foreach (ushort o in offsets) stream.Write(o);
        }
    }
}
