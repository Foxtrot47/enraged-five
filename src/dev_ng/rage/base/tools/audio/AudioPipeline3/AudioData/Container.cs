﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using rage.ToolLib.Writer;

namespace Rockstar.Audio.Data
{
    public abstract class Container
    {
        public class ChunkSerializationInfo
        {
            public ChunkSerializationInfo(ContainerObject Object, Chunk Chunk, long ChunkOffset) : this(Object, Chunk)
            {
                this.ChunkOffset = ChunkOffset;
            }
            public ChunkSerializationInfo(ContainerObject Object, Chunk Chunk) { this.Object = Object; this.Chunk = Chunk; }
            public ContainerObject Object;
            public Chunk Chunk;
            public long ChunkOffset;
        }

        public class ContainerSerializationInfo
        {
            private OrderedDictionary orderedChunkInfoStorage = new OrderedDictionary();

            public ChunkSerializationInfo this[Chunk index]
            {
                get { return (ChunkSerializationInfo)orderedChunkInfoStorage[index]; }
            }

            public void Add(Chunk key, ChunkSerializationInfo value)
            {
                orderedChunkInfoStorage.Add(key, value);
            }

            public IEnumerable<ChunkSerializationInfo> getOrderedChunkInfo()
            {
                return orderedChunkInfoStorage.Values.Cast<ChunkSerializationInfo>();
            }
        }

        [NonSerialized]
        private long? serialisedDataOffset = null;
        public long SerialisedDataOffset
        {
            get
            {
                if (serialisedDataOffset == null) throw new Exception("Container has not been serialised.");
                return serialisedDataOffset.Value;
            }
            set { serialisedDataOffset = value; }
        }

        public readonly List<ContainerObject> Objects;

        protected bool HasFastLookupTable { get; set; }
        protected bool HasEncryptedData { get; set; }
        protected bool HasContiguousObjectPacking { get; set; }
        protected bool HasStreamingObjectPacking { get; set; }


        public Container(bool hasEncryptedData)
        {
            HasEncryptedData = hasEncryptedData;
            HasContiguousObjectPacking = false;
            HasStreamingObjectPacking = false;
            Objects = new List<ContainerObject>();
        }

        public void Serialize(rage.ToolLib.Writer.IWriter stream)
        {
            //Objects need to be sorted
            Objects.Sort((p1, p2) => p1.NameHash.CompareTo(p2.NameHash));
            
            uint chunkCount = Objects.Aggregate<ContainerObject, uint>(0, (current, o) => current + (uint)o.Chunks.Count);
            ContainerHeader header = new ContainerHeader((uint)Objects.Count, chunkCount, HasFastLookupTable, HasContiguousObjectPacking, HasStreamingObjectPacking, HasEncryptedData);
            header.Serialize(stream);
            long dataOffset = header.Size;

            if (HasFastLookupTable)
            {
                ContainerFastLookupTable fastLookupTable = new ContainerFastLookupTable(Objects);
                fastLookupTable.Serialize(stream);
                dataOffset += fastLookupTable.Size;
            }

            ContainerObjectTable objectTable = new ContainerObjectTable(Objects);
            objectTable.Serialize(stream);
            dataOffset += objectTable.Size;

            long dataChunkTableOffset = 0;
            foreach(ContainerObject o in Objects)dataChunkTableOffset += o.Chunks.Count * ContainerDataTable.ENTRY_SIZE;
            dataOffset += dataChunkTableOffset;
            this.SerialisedDataOffset = dataOffset;
            
            ContainerSerializationInfo serializationInfo = CalculateChunkOrderAndOffsets(dataOffset);
            ContainerDataTable dataTable = new ContainerDataTable(true, serializationInfo, Objects, dataOffset);
            dataTable.Serialize(stream);

            SerializeData(stream, dataOffset, serializationInfo);
        }

        /*
         * The resulting ContainerSerializationInfo will be used to lookup information for a specific
         * chunk and ts content will be serialized in the provided order
         */
        protected abstract ContainerSerializationInfo CalculateChunkOrderAndOffsets(long dataOffset);

        /*
         * The implementation of this method should serialize the chunks contained in serializationInfo in the 
         * provided order with the given offset
         */
        protected abstract void SerializeData(IWriter stream, long dataOffset,
            ContainerSerializationInfo serializationInfo);
    }
}
