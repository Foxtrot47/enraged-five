﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using rage.ToolLib.Reader;
using rage.ToolLib.Writer;

namespace Rockstar.Audio.Data
{
    public class ContainerDataTable
    {
        public const int ENTRY_SIZE = 8;

        public long Size { get { return packDataTableEntries.Count * ENTRY_SIZE; } }

        private List<ulong> packDataTableEntries=new List<ulong>();

        public IReadOnlyCollection<ulong> PackDataTableEntries
        {
            get { return packDataTableEntries.AsReadOnly(); }
        }

        public ContainerDataTable(IReader reader, uint chunkCount)
        {
            for (int i = 0; i < chunkCount; i++) packDataTableEntries.Add(reader.ReadULong());
        }

        public ContainerDataTable(bool sortChunksByHash, Container.ContainerSerializationInfo chunkSerializationInfo, List<ContainerObject> objects, long dataOffset)
        {

            //write lookup table for chunks
            // This must be sequential per object, regardless of how the chunks themselves are sorted
            foreach (var o in objects)
            {
                // sort the chunk lookup by hash to enable fast searching at runtime
                foreach (var c in sortChunksByHash? o.Chunks.OrderBy(c => c.TypeHash).ToList(): o.Chunks)
                {
                    packDataTableEntries.Add(c.PackDataTableEntry(chunkSerializationInfo[c].ChunkOffset));
                }
            }
        }

        public void Serialize(IWriter stream)
        {
            foreach (ulong o in packDataTableEntries) stream.Write(o);
        }
    }
}
