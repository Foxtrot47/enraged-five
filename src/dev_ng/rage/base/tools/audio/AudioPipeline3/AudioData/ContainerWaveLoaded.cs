﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using rage.ToolLib.Writer;

namespace Rockstar.Audio.Data
{
    public class ContainerWaveLoaded: Container
    {

        public ContainerWaveLoaded(bool hasEncryptedData)
            : base(hasEncryptedData)
        {
            HasContiguousObjectPacking = true;
        }

        protected override ContainerSerializationInfo CalculateChunkOrderAndOffsets(long dataOffset)
        {
            ContainerSerializationInfo serializatonInfo = new ContainerSerializationInfo();

            long localDataOffset = dataOffset;

            foreach (var o in Objects)
            {
                long firstChunkOffset = 0;
                bool isFirstChunk = true;

                foreach (var c in o.Chunks)
                {
                    // ensure chunk is aligned as requested
                    long chunkDataOffsetBytes = localDataOffset;

                    if (isFirstChunk)
                    {
                        while ((chunkDataOffsetBytes % c.Alignment) != 0)
                        {
                            chunkDataOffsetBytes++;
                            localDataOffset++;
                        }
                        // align all chunks for this object relative to the first chunk, rather than the container base address
                        firstChunkOffset = localDataOffset;
                        isFirstChunk = false;
                    }
                    else
                    {
                        while (((chunkDataOffsetBytes - firstChunkOffset) % c.Alignment) != 0)
                        {
                            chunkDataOffsetBytes++;
                            localDataOffset++;
                        }
                    }

                    if (c.Data.Length > Chunk.MaxDataSizeBytes)
                    {
                        throw new Exception(string.Format("Object {0} chunk {1} is too large - {2}, limit is {3}",
                            o.Name, c.TypeHash, c.Data.Length, Chunk.MaxDataSizeBytes));
                    }

                    localDataOffset += c.Data.Length;
                    serializatonInfo.Add(c, new ChunkSerializationInfo(o, c, chunkDataOffsetBytes));
                }
            }

            return serializatonInfo;
        }


        protected override void SerializeData(IWriter stream, long dataOffset, ContainerSerializationInfo serializationInfo)
        {
            long localDataOffset = dataOffset;
            long firstChunkOffset = 0;
            
            byte padByte = 0;
            List<ContainerObject> serialisedObjects = new List<ContainerObject>();
            ContainerObject currentObject = null;

            foreach (ChunkSerializationInfo c in serializationInfo.getOrderedChunkInfo())
            {
                if (c.Object != currentObject)
                {
                    currentObject = c.Object;
                    if(serialisedObjects.Contains(currentObject)) throw new Exception("Chunks of object "+currentObject.Name+" are not packed contiguously");
                    else serialisedObjects.Add(currentObject);
                    firstChunkOffset = localDataOffset;
                }

                // pad to required alignment
                while (localDataOffset < c.ChunkOffset)
                {
                    stream.Write(padByte);
                    localDataOffset++;
                }

                stream.BinWriter.Write(c.Chunk.Data);
                //save chunk offset
                c.Chunk.SerialisedAtOffset = c.ChunkOffset;

                localDataOffset += c.Chunk.Data.Length;

                //update object data size
                c.Object.SerialisedSize = localDataOffset - firstChunkOffset;
            }

        }

    }
}
