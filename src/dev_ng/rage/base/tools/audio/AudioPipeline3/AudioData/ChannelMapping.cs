﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rockstar.Audio.Data
{
    public class ChannelMapping
    {
        public enum ChannelPosition
        {
            UNUSED,
            LEFT,
            RIGHT,
            CENTER,
            LFE,
            SURROUND_LEFT,
            SURROUND_RIGHT,
            BACK_SURROUND_LEFT,
            BACK_SURROUND_RIGHT,
            DUAL_MONO
        }

        /**
         * According to EBU mapping of ProTools
         */
        public static List<ChannelPosition> GetDefaultChannelPositions(int channels)
        {
            ChannelPosition[] channelMap =
                Enumerable.Repeat<ChannelPosition>(ChannelPosition.UNUSED, channels).ToArray();
            
            if (channels == 3)
            {
                channelMap[0] = ChannelPosition.LEFT;
                channelMap[1] = ChannelPosition.CENTER;
                channelMap[2] = ChannelPosition.RIGHT;
            }
            else if (channels == 4)
            {
                channelMap[0] = ChannelPosition.LEFT;
                channelMap[1] = ChannelPosition.RIGHT;
                channelMap[2] = ChannelPosition.SURROUND_LEFT;
                channelMap[3] = ChannelPosition.SURROUND_RIGHT;
            }
            else if (channels == 5)
            {
                channelMap[0] = ChannelPosition.LEFT;
                channelMap[1] = ChannelPosition.CENTER;
                channelMap[2] = ChannelPosition.RIGHT;
                channelMap[3] = ChannelPosition.SURROUND_LEFT;
                channelMap[4] = ChannelPosition.SURROUND_RIGHT;
            }
            else if (channels == 6)
            {
                channelMap[0] = ChannelPosition.LEFT;
                channelMap[1] = ChannelPosition.RIGHT;
                channelMap[2] = ChannelPosition.CENTER;
                channelMap[3] = ChannelPosition.LFE;
                channelMap[4] = ChannelPosition.SURROUND_LEFT;
                channelMap[5] = ChannelPosition.SURROUND_RIGHT;
            }
            else if (channels == 7)
            {
                channelMap[0] = ChannelPosition.LEFT;
                channelMap[1] = ChannelPosition.CENTER;
                channelMap[2] = ChannelPosition.RIGHT;
                channelMap[3] = ChannelPosition.SURROUND_LEFT;
                channelMap[4] = ChannelPosition.SURROUND_RIGHT;
                channelMap[5] = ChannelPosition.BACK_SURROUND_LEFT;
                channelMap[6] = ChannelPosition.BACK_SURROUND_RIGHT;
            }
            {
                for (int i = 0; i < channels; ++i)
                {
                    switch (i)
                    {
                        case 0: channelMap[i] = ChannelPosition.LEFT; break;
                        case 1: channelMap[i] = ChannelPosition.CENTER; break;
                        case 2: channelMap[i] = ChannelPosition.RIGHT; break;
                        case 3: channelMap[i] = ChannelPosition.SURROUND_LEFT; break;
                        case 4: channelMap[i] = ChannelPosition.SURROUND_RIGHT; break;
                        case 5: channelMap[i] = ChannelPosition.BACK_SURROUND_LEFT; break;
                        case 6: channelMap[i] = ChannelPosition.BACK_SURROUND_RIGHT; break;
                        case 7: channelMap[i] = ChannelPosition.LFE; break;
                        default: channelMap[i] = ChannelPosition.UNUSED; break;
                    }
                }
            }
            return new List<ChannelPosition>(channelMap);
        }

        public static List<ChannelPosition> GetGameChannelPositions(int channels)
        {
            ChannelPosition[] channelMap =
                Enumerable.Repeat<ChannelPosition>(ChannelPosition.UNUSED, channels).ToArray();

            for (int i = 0; i < channels; ++i)
            {
                switch (i)
                {
                    case 0: channelMap[i] = ChannelPosition.LEFT; break;
                    case 1: channelMap[i] = ChannelPosition.RIGHT; break;
                    case 2: channelMap[i] = ChannelPosition.BACK_SURROUND_LEFT; break;
                    case 3: channelMap[i] = ChannelPosition.BACK_SURROUND_RIGHT; break;
                    case 4: channelMap[i] = ChannelPosition.CENTER; break;
                    case 5: channelMap[i] = ChannelPosition.LFE; break;
                    case 6: channelMap[i] = ChannelPosition.SURROUND_LEFT; break;
                    case 7: channelMap[i] = ChannelPosition.SURROUND_RIGHT; break;
                    default: channelMap[i] = ChannelPosition.UNUSED; break;
                }
            }
            return new List<ChannelPosition>(channelMap);
        }

        public static float[][] mapChannels(float[][] inputData, List<ChannelPosition> inputChannelMap,
            List<ChannelPosition> outputChannelMap)
        {
            if(inputData.Length != inputChannelMap.Count || inputData.Length != outputChannelMap.Count) throw new Exception("Size of channel maps are not matching the input data.");
            float[][] outputData = new float[inputData.Length][];
            for (int channelIndex = 0; channelIndex < inputData.Length; channelIndex++) outputData[channelIndex] = new float[0];

            for (int channelIndex = 0; channelIndex < inputData.Length; channelIndex++)
            {
                ChannelPosition inputPosition = inputChannelMap[channelIndex];
                if (inputPosition == ChannelPosition.UNUSED) continue;
                int outputChannel = outputChannelMap.IndexOf(inputPosition);
                if(outputChannel == -1) throw new Exception("No mapping for input channel position "+inputPosition+" found in output channel map.");
                outputData[outputChannel] = inputData[channelIndex];
            }
            return outputData;
        }
    }
}
