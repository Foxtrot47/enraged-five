﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using rage.ToolLib;

namespace Rockstar.Audio.Data
{
    public enum ChunkType
    {
        DATA,
        FORMAT,
        MARKERS,
        SEEKTABLE,
        STREAMFORMAT,
        LIPSYNC,
        LIPSYNC64,
        GRANULARGRAINS,
        GRANULARLOOPS,
        GRANULARPITCH,
        MID,
        PEAK,
        GESTURE
    }

    public class Chunk
    {
        public byte[] Data { get; private set; }
        public int Alignment { get; private set; }

        public uint TypeHash
        {
            get { return type.Key & TypeHashMask; }
        }

        public string TypeName 
        {
            get { return type.Value; }
        }

        public const int DataOffsetWidth = 28;
	    public const int MaxChunkDataOffsetBytes = 1<<DataOffsetWidth;
        public const int DataOffsetMask = (MaxChunkDataOffsetBytes - 1);
	    public const int DataSizeBytesWidth = 28;
	    public const int MaxDataSizeBytes = 1<<DataSizeBytesWidth;
        public const int DataSizeMask = (MaxDataSizeBytes - 1);
	    public const int TypeHashWidth = 8;
	    public const int TypeHashMask = (1<<TypeHashWidth)-1;

        private Hash type;

        [NonSerialized]
        private long? serialisedAtOffset = null;
        public long SerialisedAtOffset
        {
            get
            {
                if (serialisedAtOffset == null) throw new Exception("Object has not been serialised.");
                return serialisedAtOffset.Value;
            }
            set { serialisedAtOffset = value; }
        }

        public static Chunk ChunkFactory(string fileExtension, byte[] data, string platform, bool waveloaded)
        {
            Chunk newChunk=null;

            switch (Enum<ChunkType>.Parse(fileExtension, true))
            {
                case ChunkType.STREAMFORMAT:
                case ChunkType.SEEKTABLE:
                case ChunkType.PEAK:
                case ChunkType.FORMAT:
                    newChunk = new Chunk(fileExtension.ToUpper(), data, 1);
                    break;
                case ChunkType.MARKERS:
                case ChunkType.GRANULARGRAINS:
                case ChunkType.GRANULARLOOPS:
                case ChunkType.GRANULARPITCH:
                case ChunkType.GESTURE:
                    newChunk = new Chunk(fileExtension.ToUpper(), data, 4);
                    break;
                case ChunkType.LIPSYNC:
                case ChunkType.LIPSYNC64:
                    newChunk = new Chunk(fileExtension.ToUpper(), data, 16);
                    break;
                case ChunkType.DATA:
                case ChunkType.MID:
                    newChunk = new Chunk(fileExtension.ToUpper(), data,
                        platform.ToUpper().Equals(Platform.XBOX360.ToString()) ||
                        platform.ToUpper().Equals(Platform.XBOXONE.ToString()) ||
                        platform.ToUpper().Equals(Platform.XBOXSERIES.ToString())
                            ? (waveloaded ? 1 : 2048)
                            : (waveloaded ? 1 : 16));
                    break;
            }

            if (newChunk == null)
                throw new Exception("could not create chunk for file extension ." + fileExtension);

            return newChunk;
        }

        private Chunk(string typeName, byte[] data, int alignment=1)
        {
            type = new Hash();
            type.Value = typeName;
            Alignment = alignment;
            Data = data;
        }

        public ulong PackDataTableEntry(long dataOffset)
        {
            ulong offset = (ulong)(dataOffset & DataOffsetMask);
            ulong size = (ulong)(Data.Length & DataSizeMask);
            ulong type = (ulong)(TypeHash & TypeHashMask);

            return offset |
                    (size<<DataOffsetWidth) | 
                    (type << (DataOffsetWidth+DataSizeBytesWidth));
        }

    }
}
