﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.ConstrainedExecution;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using rage;
using rage.ToolLib;
using Rockstar.AssetManager.Interfaces;
using Rockstar.Audio.Data;
using Rockstar.Audio.Pipeline;
using Wavelib;

namespace Rockstar.Audio.AudioAssetBuilder.XmlBuildExecutor
{
    public class XmlBuiltBankNodeInfo
    {
        private XElement bankNode;

        public XmlBuiltBankNodeInfo(XElement bankNode)
        {
            this.bankNode = bankNode;
        }

        public string BankName
        {
            get { return bankNode.Attribute("name").Value; }
        }

        public string BankPath
        {
            get { return GetPath(bankNode); }
        }

        public string BankPackName
        {
            get
            {
                var parent = bankNode.Parent;
                do parent = parent.Parent;
                while (!parent.Name.LocalName.Equals("Pack"));

                return parent.Attribute("name").Value;
            }
        }

        /**
         * Helper class that collects additional information from output package files
         */
        public class XmlObjectInfo
        {
            public class XmlMarkerInfo
            {
                public uint TimeOffset;
                public string Name;

                public XmlMarkerInfo(uint timeOffset, string name)
                {
                    Name = name;
                    TimeOffset = timeOffset;
                }
            }
            public class XmlChunkInfo
            {
                public string Name;
                public int Size;
                public List<XmlMarkerInfo> Markers = new List<XmlMarkerInfo>();
                public XmlChunkInfo(string name, int size)
                {
                    Name = name;
                    Size = size;
                }
            }

            public string FileName = null;
            public uint? NameHash = null;
            public uint? BuiltSize = null;
            public double? BuiltSamplerate = null;
            public double? IntegratedLoudness = null;
            public double? LoudnessRange = null;
            public List<XmlChunkInfo> Chunks = new List<XmlChunkInfo>();

            public XmlObjectInfo(ContainerObject containerObj, Package outputPackage)
            {
                PackageFile correspondingPackageWaveFile = (from packageFile in outputPackage.Files
                                                            where (packageFile.ObjectName == containerObj.Name) && (packageFile.Object is PackageWaveObject)
                                                            select packageFile).Single();
                FileName = Path.GetFileName(correspondingPackageWaveFile.FileName);
                NameHash = containerObj.NameHash;
                BuiltSamplerate = correspondingPackageWaveFile.Properties.GetValue(Resample.RESAMPLED_SAMPLE_RATE, 0);
                if (correspondingPackageWaveFile.Properties.ContainsKey(PeakNormalise.INTEGRATED_LOUDNESS))
                {
                    double result;
                    if (double.TryParse(correspondingPackageWaveFile.Properties[PeakNormalise.INTEGRATED_LOUDNESS], out result)) IntegratedLoudness = result;
                }
                if (correspondingPackageWaveFile.Properties.ContainsKey(PeakNormalise.LOUDNESS_RANGE))
                {
                    double result;
                    if (double.TryParse(correspondingPackageWaveFile.Properties[PeakNormalise.LOUDNESS_RANGE],
                        out result)) LoudnessRange = result;
                }

                foreach (Chunk chunk in containerObj.Chunks)
                {
                    XmlChunkInfo currentChunkInfo = new XmlChunkInfo(chunk.TypeName, chunk.Data.Length);
                    Chunks.Add(currentChunkInfo);
                    PackageFile correspondingPackageFile = (from packageFile in outputPackage.Files
                                                            where (packageFile.ObjectName == containerObj.Name) && (packageFile.ObjectType == chunk.TypeName)
                                                            select packageFile).SingleOrDefault();
                    if (correspondingPackageFile == null) continue;
                    if (correspondingPackageFile.Object is PackageWaveMarkerObject)
                    {
                        PackageWaveMarkerObject markerChunk = (PackageWaveMarkerObject)correspondingPackageFile.Object;
                        foreach (PackageWaveMarkerObject.WaveMetadataMarker marker in markerChunk)
                        {
                            if (BuiltSamplerate == null) throw new Exception("");
                            uint timeOffset = (uint)(marker.TimeOffset / (BuiltSamplerate / 1000.0f));
                            currentChunkInfo.Markers.Add(new XmlMarkerInfo(timeOffset, marker.Label));
                        }
                    }
                }
                BuiltSize = (uint)containerObj.SerialisedSize;
            }

        }

        public void updateBuiltBankNodeInfo(Package outputPackage, string outputFile)
        {
            long bankHeaderSize = 0;
            Container builtContainer = ((PackageWaveContainer)outputPackage.Files.Last().Object).Container;
            XmlObjectInfo globalObjectInfo = null;
            BankBuilder.PackingTypes containerPackingType = Enum<BankBuilder.PackingTypes>.Parse(outputPackage.Properties[BankBuilder.BANK_PACKING_TYPE], true);
            if (containerPackingType == BankBuilder.PackingTypes.Stream)
            {
                ContainerObject globalObject = ((ContainerStreamed)builtContainer).getGlobalObject();
                globalObjectInfo = new XmlObjectInfo(globalObject, outputPackage);

                Chunk dataChunk = globalObject.FindChunk(ChunkType.DATA.ToString());
                if (dataChunk == null) throw new Exception("No data chunk found for global object in " + outputFile);

                // header size for streaming banks is whatever gets us to the start of the data chunk
                // Note that this will include whatever padding was necessary to align the data 
                // chunk, in practise this should only waste a few bytes so probably isn't worth worrying 
                // about.
                bankHeaderSize = (uint)dataChunk.SerialisedAtOffset;
            }
            else
            {
                foreach (ContainerObject obj in builtContainer.Objects)
                {
                    XmlObjectInfo xmlObjInfo = new XmlObjectInfo(obj, outputPackage);
                    updateBuiltWaveInfo(xmlObjInfo);
                }
                bankHeaderSize = (uint)builtContainer.SerialisedDataOffset;
            }

            updateBuiltBankInfo(bankHeaderSize, DateTime.Now, (new FileInfo(outputFile)).Length, Utility.FileMd5(outputFile), globalObjectInfo);
        }

        public void updateBuiltBankInfo(long headerSize, DateTime timeStamp, long size, string awcMd5, XmlObjectInfo xmlGlobalObjInfo = null)
        {
            string bankName = BankName;

            bankNode.Attributes().Remove();
            bankNode.Add(new XAttribute("name", bankName),
                            new XAttribute("headerSize", headerSize),
                            new XAttribute("timeStamp", timeStamp),
                            new XAttribute("builtSize", size));

            if (!string.IsNullOrWhiteSpace(awcMd5))
            {
                bankNode.Add(new XAttribute("awcMd5", awcMd5));
            }

            //remove old chunk xml
            bankNode.Elements("Chunk").Remove();
            if (xmlGlobalObjInfo == null) return;
            foreach (XmlObjectInfo.XmlChunkInfo chunk in xmlGlobalObjInfo.Chunks)
            {
                var chunkElem = new XElement(
                    "Chunk", new XAttribute("name", chunk.Name), new XAttribute("size", chunk.Size));
                bankNode.Add(chunkElem);
                foreach (var chunkMarker in chunk.Markers)
                {
                    var chunkMarkerElem = new XElement("Marker");
                    chunkMarkerElem.SetAttributeValue("timeMs", chunkMarker.TimeOffset);
                    chunkMarkerElem.SetValue(chunkMarker.Name);
                    chunkElem.Add(chunkMarkerElem);
                }
            }
        }

        public void updateBuiltWaveInfo(XmlObjectInfo xmlObjInfo)
        {
            XElement waveNode = (from wave in bankNode.Descendants("Wave")
                                 where wave.Attribute("name").Value == xmlObjInfo.FileName
                                 select wave).Single();
            string sourceSize = waveNode.Attribute("sourceSize").Value;
            string sourceSampleRate = waveNode.Attribute("sourceSampleRate") != null
                ? waveNode.Attribute("sourceSampleRate").Value
                : "";
            waveNode.Attributes().Remove();

            waveNode.SetAttributeValue("nameHash", xmlObjInfo.NameHash);
            waveNode.SetAttributeValue("sourceSize", sourceSize);
            if (!xmlObjInfo.FileName.ToUpper().EndsWith(".MID"))
            {
                waveNode.SetAttributeValue("sourceSampleRate", sourceSampleRate);
                waveNode.SetAttributeValue("builtSampleRate", xmlObjInfo.BuiltSamplerate);
                double? integratedLoudness = xmlObjInfo.IntegratedLoudness;
                if (integratedLoudness != null) waveNode.SetAttributeValue("integratedLoudness", integratedLoudness);
                double? loudnessRange = xmlObjInfo.LoudnessRange;
                if (loudnessRange != null) waveNode.SetAttributeValue("loudnessRange", loudnessRange);
            }
            waveNode.SetAttributeValue("builtName", xmlObjInfo.FileName);

            // interleaved streams don't have discrete built size
            if (xmlObjInfo.BuiltSize != null)
            {
                waveNode.SetAttributeValue("builtSize", xmlObjInfo.BuiltSize);
            }

            //remove old chunk xml
            waveNode.Elements("Chunk").Remove();

            foreach (XmlObjectInfo.XmlChunkInfo chunk in xmlObjInfo.Chunks)
            {
                var chunkElem = new XElement(
                    "Chunk", new XAttribute("name", chunk.Name), new XAttribute("size", chunk.Size));
                waveNode.Add(chunkElem);
                foreach (var chunkMarker in chunk.Markers)
                {
                    var chunkMarkerElem = new XElement("Marker");
                    chunkMarkerElem.SetAttributeValue("timeMs", chunkMarker.TimeOffset);
                    chunkMarkerElem.SetValue(chunkMarker.Name);
                    chunkElem.Add(chunkMarkerElem);
                }
            }
        }

        public BankBuildConfiguration getBankBuildConfiguration(IAssetManager assetManager, audProjectSettings projectSettings, XmlWaveSlotSettingsInfo xmlWaveSlotSettingsInfo)
        {
            BankBuildConfiguration bankBuildConfig = new BankBuildConfiguration();
            bankBuildConfig.BankName = BankName;
            bankBuildConfig.BankPath = BankPath;
            bankBuildConfig.EncryptionKey = projectSettings.GetCurrentPlatform().EncryptionKey;
            bankBuildConfig.IsBigEndian = projectSettings.GetCurrentPlatform().IsBigEndian;
            bankBuildConfig.Platform = projectSettings.GetCurrentPlatform().PlatformTag;
            bankBuildConfig.PackingType = xmlWaveSlotSettingsInfo.GetBankPackingType(bankNode);

            // pull out streaming tag if one exists
            var streamingTag = FindTag(bankNode, "streamingBlockBytes");
            bool isStream = streamingTag != null && 0 < int.Parse(streamingTag.Attribute("value").Value);

            if (isStream && bankBuildConfig.PackingType != BankBuilder.PackingTypes.Stream)
            {
                throw new Exception(string.Format("Streaming Bank: {0} is not associated with a streaming wave slot",
                    Path.Combine(BankPackName, BankName)));
            }

            foreach (XElement waveNode in bankNode.Descendants("Wave"))
            {
                if (FindTag(waveNode, "DoNotBuild") != null) continue;
                string fileName = Path.Combine(assetManager.GetLocalPath(projectSettings.GetWaveInputPath()), GetPath(waveNode));
                if (!fileName.ToUpper().EndsWith("WAV"))
                {
                    bankBuildConfig.GenericChunkFiles.Add(fileName);
                    continue;
                }

                WaveBuildConfiguration waveBuildConfig = new WaveBuildConfiguration();

                waveBuildConfig.FileName = fileName;
                waveBuildConfig.AlignmentSamples = projectSettings.GetCurrentPlatform().AlignmentSamples;
                string blockSizeTagValue = FindTagValue(waveNode, "blockSize");
                waveBuildConfig.CompressionBlockSize = string.IsNullOrEmpty(blockSizeTagValue)
                    ? 2
                    : int.Parse(blockSizeTagValue);
                string compressionTagValue = FindTagValue(waveNode, "compression");
                waveBuildConfig.CompressionQuality = string.IsNullOrEmpty(compressionTagValue)
                    ? 0
                    : int.Parse(compressionTagValue);
                string compressionTypeTagValue = FindTagValue(waveNode, "compressionType");
                waveBuildConfig.CompressionType = string.IsNullOrEmpty(compressionTypeTagValue)
                    ? WaveEncode.CompressionType.PCM
                    : Enum<WaveEncode.CompressionType>.Parse(compressionTypeTagValue);
                waveBuildConfig.DesiredPeakLevel = -2;
                waveBuildConfig.HasLipsync =
                    assetManager.ExistsAsAsset(
                        waveBuildConfig.FileName.ToUpper()
                            .Replace("\\WAVES\\", "\\LIPSYNCANIMS\\")
                            .Replace(".WAV", ".CUSTOM_LIPSYNC"));
                waveBuildConfig.IsStream = isStream;
                waveBuildConfig.IterativeEncoding = FindTagValue(waveNode, "iterativeEncoding");
                waveBuildConfig.NeedsGranularProcessing = assetManager.ExistsAsAsset(
                    waveBuildConfig.FileName.ToUpper()
                        .Replace(".WAV", ".GRN"));
                string perserveTransientTagValue = FindTagValue(waveNode, "preserveTransient");
                waveBuildConfig.PerserveTransient = waveBuildConfig.NeedsGranularProcessing ||
                                                    (!string.IsNullOrEmpty(perserveTransientTagValue) &&
                                                     perserveTransientTagValue != "0");
                string markSilence = FindTagValue(waveNode, "markSilence");
                if (markSilence != String.Empty)
                {
                    waveBuildConfig.AddSilenceMarkers = true;
                    var silenceParams = Utility.ParseKeyValueString(markSilence);
                    if (silenceParams.ContainsKey("VOLUME"))
                        waveBuildConfig.SilenceVolumeThreshold = float.Parse(silenceParams["VOLUME"]);
                    if (silenceParams.ContainsKey("DURATION"))
                        waveBuildConfig.SilenceLengthTreshold = float.Parse(silenceParams["DURATION"]);
                }

                int samplerate = 0;
                if (int.TryParse(FindTagValue(waveNode, "samplerate"), out samplerate))
                {
                    waveBuildConfig.SampleRate = samplerate;
                }
                else
                {
                    waveBuildConfig.SampleRate = (int) new bwWaveFile(waveBuildConfig.FileName).Format.SampleRate;
                }
                bankBuildConfig.WaveBuildConfigurations.Add(waveBuildConfig);
            }

            return bankBuildConfig;
        }

        private static XElement FindTag(XElement element, string tagName)
        {
            foreach (var e in element.AncestorsAndSelf())
            {
                foreach (var tag in e.Elements("Tag"))
                {
                    if (String.Compare(tag.Attribute("name").Value, tagName, StringComparison.OrdinalIgnoreCase) == 0)
                    {
                        return tag;
                    }
                }
            }
            return null;
        }

        private static string FindTagValue(XElement element, string tagName)
        {
            XElement tagElement = FindTag(element, tagName);
            if (tagElement != null && tagElement.Attribute("value") != null) return tagElement.Attribute("value").Value;
            else return string.Empty;
        }

        private static string GetPath(XElement element)
        {
            var pathBuilder = new StringBuilder();
            foreach (var parent in element.AncestorsAndSelf().InDocumentOrder())
            {
                if (parent.Attribute("name") != null)
                {
                    if (!string.IsNullOrEmpty(pathBuilder.ToString())) pathBuilder.Append("\\");
                    pathBuilder.Append(parent.Attribute("name").Value);
                }
            }
            return pathBuilder.ToString();
        }
    }
}
