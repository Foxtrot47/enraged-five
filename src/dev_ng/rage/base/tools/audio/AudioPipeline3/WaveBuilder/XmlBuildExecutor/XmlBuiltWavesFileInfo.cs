﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.XPath;
using NLog;
using rage.ToolLib.WavesFiles;

namespace Rockstar.Audio.AudioAssetBuilder.XmlBuildExecutor
{
    public class XmlBuiltWavesFileInfo : XmlBuiltWavesInfo
    {
        public readonly string Path;

        public void save()
        {
           builtWaves.Save(Path);
        }

        public XmlBuiltWavesFileInfo(string path)
            : base(XDocument.Load(path))
        {
            Path = path;
        }
    }
}
