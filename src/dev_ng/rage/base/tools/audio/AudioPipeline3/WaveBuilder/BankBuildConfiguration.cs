﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rockstar.Audio.Pipeline;

namespace Rockstar.Audio.AudioAssetBuilder
{
    public class BankBuildConfiguration
    {
        public string BankName { get; set; }
        public string BankPath { get; set; }
        public BankBuilder.PackingTypes PackingType { get; set; }
        public bool IsBigEndian { get; set; }
        public string EncryptionKey { get; set; }
        public string Platform { get; set; }
        public List<WaveBuildConfiguration> WaveBuildConfigurations { get; set; }
        public List<string> GenericChunkFiles { get; set; }

        public BankBuildConfiguration()
	    {
		    WaveBuildConfigurations = new List<WaveBuildConfiguration>();
            GenericChunkFiles = new List<string>();
	    }
    }
}
