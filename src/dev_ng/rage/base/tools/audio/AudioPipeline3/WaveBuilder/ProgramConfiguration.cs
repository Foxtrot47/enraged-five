﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using rage.ToolLib.CmdLine;

namespace Rockstar.Audio.AudioAssetBuilder
{
    public class ProgramConfiguration: CommandLineConfiguration
    {
        public ProgramConfiguration(string[] args) : base(args)
        {
        }

        public string P4Client { get; set; }

        [Description("the connection info for perforce ex. rsgedip4s1:1666")]
        public string AssetServer { get; set; }

        [Description("the perforce project")]
        public string AssetProject { get; set; }

        [Description("the perforce user name")]
        public string AssetUsername { get; set; }

        [Description("the perforce password")]
        [OptionalParameter]
        [DefaultValue("")]
        public string AssetPassword { get; set; }

        [Description("path to the projectSettings.xml configuration")]
        public string ProjectSettings { get; set; }

        [Description("the target platform tag")]
        public string Platform { get; set; }

        [Description("an optional changelist number to add the build result to")]
        [OptionalParameter]
        public string Change { get; set; }

        [Description("the depot root")]
        public string DepotRoot { get; set; }

        [Description("the pack to build")]
        public string Pack { get; set; }

        [Description("optional changelist to sync the build to")]
        [OptionalParameter]
        public string SyncNumber { get; set; }
    }
}
