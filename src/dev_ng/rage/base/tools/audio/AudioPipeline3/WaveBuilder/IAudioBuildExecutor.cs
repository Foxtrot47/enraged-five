﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using rage;
using Rockstar.AssetManager.Interfaces;

namespace Rockstar.Audio.AudioAssetBuilder
{
    public abstract class IAudioBuildExecutor
    {
        public abstract void ExecuteAudioBuild(string packName, string platform, IChangeList changeList, string syncToChangelist, IPipelineProcessor pipelineProcessor);
    }
}
