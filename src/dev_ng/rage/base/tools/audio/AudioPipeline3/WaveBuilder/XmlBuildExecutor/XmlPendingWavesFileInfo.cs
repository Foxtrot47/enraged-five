﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using rage.ToolLib;

namespace Rockstar.Audio.AudioAssetBuilder.XmlBuildExecutor
{
    public class XmlPendingWavesFileInfo
    {
        private XDocument pendingWavesDoc;
        public readonly string Path;

        public XmlPendingWavesFileInfo(string pendingWavesPath)
        {
            pendingWavesDoc = XDocument.Load(pendingWavesPath);
            Path = pendingWavesPath;

            if (null == pendingWavesDoc.Root || pendingWavesDoc.Root.Name != "PendingWaves")
            {
                throw new Exception("Pending wave doc invalid: " + pendingWavesPath + Environment.NewLine + "no <PendingWaves> root found.");
            }
            if (pendingWavesDoc.Root.Elements("Pack").Count() > 1)
            {
                throw new Exception("Pending wave doc invalid: " + pendingWavesPath + Environment.NewLine + "multiple packs found in file.");
            }
        }

        public bool isEmpty()
        {
            return !pendingWavesDoc.Root.Elements("Pack").Any();
        }

        public string getPackName()
        {
            return pendingWavesDoc.Root.Elements("Pack").Single().Attribute("name").Value;
        }

        public enum Operation
        {
            add,
            remove,
            modify
        }

        public Operation GetPackOperation()
        {
            return Enum<Operation>.Parse(pendingWavesDoc.Root.Elements("Pack").Single().Attribute("operation").Value);
        }

        public XElement getPackElement()
        {
            return pendingWavesDoc.Root.Elements("Pack").Single();
        }

        public void Save()
        {
            this.pendingWavesDoc.Save(Path);
        }
    }
}
