﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using NLog;
using rage;
using rage.ToolLib;
using rage.ToolLib.Encryption;
using rage.ToolLib.WavesFiles;
using Rockstar.AssetManager.Interfaces;
using Rockstar.Audio.AudioAssetBuilder.XmlBuildExecutor;
using Rockstar.Audio.Data;
using Rockstar.Audio.Pipeline;

namespace Rockstar.Audio.AudioAssetBuilder
{
    public class XmlAudioBuildExecutor: IAudioBuildExecutor
    {
        private audProjectSettings projectSettings;
        private IAssetManager assetManager;
        private Logger logger = LogManager.GetCurrentClassLogger();


        public XmlAudioBuildExecutor(audProjectSettings projectSettings, IAssetManager assetManager)
        {
            this.projectSettings = projectSettings;
            this.assetManager = assetManager; 
        }

        public override void ExecuteAudioBuild(string packName, string platform, IChangeList changeList, string syncToChangelist, IPipelineProcessor pipelineProcessor)
        {
            projectSettings.SetCurrentPlatformByTag(platform);
            logger.Info("Getting latest BuiltWaves...");
            string builtWavesFolder = Path.Combine(projectSettings.GetCurrentPlatform().BuildInfo, "BuiltWaves\\");
            assetManager.GetLatest(builtWavesFolder, false);
            builtWavesFolder = assetManager.GetLocalPath(builtWavesFolder);
            string builtWavesFilePath = Path.Combine(builtWavesFolder, string.Format("{0}_PACK_FILE.XML", packName.ToUpper()));

            logger.Info("Getting latest PendingWaves...");
            string pendingWavesFolder = Path.Combine(projectSettings.GetCurrentPlatform().BuildInfo, "PendingWaves\\");
            assetManager.GetLatest(pendingWavesFolder, false);
            pendingWavesFolder = assetManager.GetLocalPath(pendingWavesFolder);
            string pendingWavesFilePath = Path.Combine(pendingWavesFolder, string.Format("{0}.XML", packName));

            XmlPendingWavesFileInfo xmlPendingWavesInfo = new XmlPendingWavesFileInfo(pendingWavesFilePath);

            IAsset pendingWavesAsset = changeList.CheckoutAsset(pendingWavesFilePath, true);
            if (pendingWavesAsset == null) throw new Exception("Failed to checkout pending waves file: " + pendingWavesFilePath);
            

            if (xmlPendingWavesInfo.isEmpty())
            {
                logger.Warn("No pending waves to build.");
                return;
            }

            if (packName != xmlPendingWavesInfo.getPackName())
                throw new Exception("Pending wave doc invalid: " + pendingWavesFilePath + Environment.NewLine + packName + " name expected but " + xmlPendingWavesInfo.getPackName() + " found.");

            var builtWavesPackListPath = Path.Combine(builtWavesFolder, "BuiltWaves_PACK_LIST.xml");
            
            if (xmlPendingWavesInfo.GetPackOperation()==XmlPendingWavesFileInfo.Operation.add) addPackFile(builtWavesFilePath, builtWavesPackListPath, changeList);
            else if (xmlPendingWavesInfo.GetPackOperation()==XmlPendingWavesFileInfo.Operation.remove) removePackFile(builtWavesFilePath, builtWavesPackListPath, changeList);
            else
            {
                if (assetManager.ExistsAsAsset(builtWavesFilePath) && !assetManager.IsCheckedOut(builtWavesFilePath)) changeList.CheckoutAsset(builtWavesFilePath, true);
                else throw new Exception("Existing pack: "+packName+" does not have a corresponding built waves file: "+builtWavesFilePath);
            }

            XmlBuiltWavesFileInfo xmlBuiltWaves = new XmlBuiltWavesFileInfo(builtWavesFilePath);
            List<XmlBuiltWavesFileInfo.LogMessage> messages = xmlBuiltWaves.updateBuiltWaves(xmlPendingWavesInfo.getPackElement());

            foreach (var logMessage in messages) logger.Log(logMessage.Level, logMessage.Message);

            xmlBuiltWaves.save();
            if (!xmlPendingWavesInfo.isEmpty())
            {
                xmlPendingWavesInfo.Save();
                //note: maybe throw exception?
                logger.Warn("Not all pending elements could be processed");
            }
            else
            {
                pendingWavesAsset.Revert();
                changeList.MarkAssetForDelete(pendingWavesFilePath);
            }

            RemoveBanks(xmlBuiltWaves.getDeletedBanks().Keys.ToArray(), changeList);

            List<BankBuildConfiguration> buildConfiguration = GetBuildConfiguration(xmlBuiltWaves.getModifiedBanks().Values);

            //get latest wave assets
            if (syncToChangelist == null) syncToChangelist = "";
            foreach (BankBuildConfiguration bankConfig in buildConfiguration)
            {
                try
                {
                    assetManager.GetLatest(Path.Combine(projectSettings.GetWaveInputPath(), bankConfig.BankPath, "..."), false, syncToChangelist);
                }
                catch (Exception)
                {
                    assetManager.GetLatest(Path.Combine(projectSettings.GetWaveInputPath(), bankConfig.BankPath, "..."), true, syncToChangelist);
                }
            }

            try
            {
                List<Package> resultPackages = pipelineProcessor.Process(buildConfiguration);
                string bankOutputDir = Path.Combine(assetManager.GetLocalPath(projectSettings.GetBuildOutputPath()), "SFX", packName);
                if (!Directory.Exists(bankOutputDir)) Directory.CreateDirectory(bankOutputDir);

                foreach (Package outputPackage in resultPackages)
                {
                    //write awc file
                    PackageFile bankFile = outputPackage.Files.Last();

                    bankFile.LoadObject(); //explicit load in case we got the file from the cache
                    string outputFilePath = Path.Combine(bankOutputDir, bankFile.FileName);
                    if (assetManager.ExistsAsAsset(outputFilePath) && !assetManager.IsCheckedOut(outputFilePath)) changeList.CheckoutAsset(outputFilePath, true);

                    using (FileStream fs = new FileStream(outputFilePath, FileMode.Create, FileAccess.Write))
                    {
                        bankFile.SaveObject(fs);
                    }
                    if (!assetManager.ExistsAsAsset(outputFilePath)) changeList.MarkAssetForAdd(outputFilePath);

                    //update builtWaves xml
                    XmlBuiltBankNodeInfo nodeInfoToUpdate = (from modifiedBank in xmlBuiltWaves.getModifiedBanks() 
                                                                where modifiedBank.Value.BankName == bankFile.ObjectName 
                                                                select modifiedBank.Value).Single();

                    nodeInfoToUpdate.updateBuiltBankNodeInfo(outputPackage, outputFilePath);

                    // Optional whole-file encryption for bank-loaded containers
                    if (Enum<BankBuilder.PackingTypes>.Parse(outputPackage.Properties[BankBuilder.BANK_PACKING_TYPE], true) == BankBuilder.PackingTypes.BankLoaded)
                    {
                        IEncrypter encrypter = EncrypterFactory.Create(projectSettings.GetCurrentPlatform().EncryptionKey);
                        if (encrypter.IsEncrypting) EncryptFile(outputFilePath, encrypter);
                    }
                }
            }
            catch (Exception ex)
            {
                changeList.Revert(true);
                throw ex;
            }

        }

        private void addPackFile(string builtWavesFilePath, string builtWavesPackListPath, IChangeList changeList)
        {
            if (!assetManager.IsCheckedOut(builtWavesPackListPath))
            {
                if (changeList.CheckoutAsset(builtWavesPackListPath, true) == null)
                    throw new Exception("Failed to checkout built waves pack list: " + builtWavesPackListPath);
            }

            if (File.Exists(builtWavesFilePath))
            {
                var fileInfo = new FileInfo(builtWavesFilePath);
                if (fileInfo.IsReadOnly)
                {
                    fileInfo.IsReadOnly = false;
                }
            }

            var doc = new XDocument(new XElement("BuiltWaves"));
            doc.Save(builtWavesFilePath);
            changeList.MarkAssetForAdd(builtWavesFilePath);
        }

        private void removePackFile(string builtWavesFilePath, string builtWavesPackListPath, IChangeList changeList)
        {
            if (!assetManager.IsCheckedOut(builtWavesPackListPath))
            {
                if (changeList.CheckoutAsset(builtWavesPackListPath, true) == null)
                    throw new Exception("Failed to checkout built waves pack list: " + builtWavesPackListPath);
            }

            changeList.MarkAssetForDelete(builtWavesFilePath);
        }

        private void RemoveBanks(string[] relativeBankPaths, IChangeList changeList)
        {
            foreach (string bankPath in relativeBankPaths)
            {
                // Output Directory for bank, contains bank and xml
                var bankOuputPath =
                    Path.Combine(
                        this.assetManager.GetLocalPath(this.projectSettings.GetBuildOutputPath()),
                        "SFX",
                        bankPath);

                // Output File path for bank
                var bankOutputFile = String.Concat(bankOuputPath, ".awc");
                changeList.MarkAssetForDelete(bankOutputFile);
            }
        }

        private List<BankBuildConfiguration> GetBuildConfiguration(IEnumerable<XmlBuiltBankNodeInfo> modifiedBankNodes)
        {
            List<BankBuildConfiguration> buildConfiguration = new List<BankBuildConfiguration>();
            assetManager.GetLatest(projectSettings.GetWaveSlotSettings(), false);
            XmlWaveSlotSettingsInfo xmlWaveSlotSettingsInfo = new XmlWaveSlotSettingsInfo(assetManager.GetLocalPath(projectSettings.GetWaveSlotSettings()));
            foreach (XmlBuiltBankNodeInfo bankInfo in modifiedBankNodes)
            {
                buildConfiguration.Add(bankInfo.getBankBuildConfiguration(assetManager, projectSettings, xmlWaveSlotSettingsInfo));
            }
            return buildConfiguration;
        }

        private void EncryptFile(string filePath, IEncrypter encrypter)
        {
            byte[] data;
            using (var f = new FileStream(filePath, FileMode.Open, FileAccess.Read))
            {
                data = new byte[f.Length];
                f.Read(data, 0, (int)f.Length);
                f.Close();
            }

            data = encrypter.Encrypt(data, true);

            using (var f = new FileStream(filePath, FileMode.Create, FileAccess.Write))
            {
                f.Write(data, 0, data.Length);
                f.Close();
            }
        }
        
    }
}
