﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rockstar.Audio.Pipeline;

namespace Rockstar.Audio.AudioAssetBuilder
{
    public class WaveBuildConfiguration
    {
        private double desiredPeakLevel = -2.0;
        public double DesiredPeakLevel
        {
            get { return desiredPeakLevel; }
            set { desiredPeakLevel = value; }
        }

        public string FileName { get; set; }
        public int SampleRate { get; set; }
        public int AlignmentSamples { get; set; }
        public WaveEncode.CompressionType CompressionType { get; set; }
        public int CompressionQuality { get; set; }
        public int CompressionBlockSize { get; set; }
        public string IterativeEncoding { get; set; }
        public bool IsStream { get; set; }
        public bool HasLipsync { get; set; }
        public bool PerserveTransient { get; set; }
        public bool NeedsGranularProcessing { get; set; }
        public bool AddSilenceMarkers { get; set; }
        public double? SilenceVolumeThreshold { get; set; }
        public double? SilenceLengthTreshold { get; set; }
    }
}
