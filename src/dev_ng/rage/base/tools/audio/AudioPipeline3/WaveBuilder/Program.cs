﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using rage;
using Rockstar.AssetManager.Infrastructure.Enums;
using Rockstar.AssetManager.Interfaces;
using Rockstar.AssetManager.Main;
using Rockstar.Audio.Pipeline;

namespace Rockstar.Audio.AudioAssetBuilder
{
    class Program
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public static int Main(string[] args)
        {
            /*
            //start of test code here
            IPipelineProcessor pipelineProcessor = new LocalPipelineProcessor(new CacheClient());
            BankBuildConfiguration bankConfig = new BankBuildConfiguration();
            bankConfig.BankName = "JOHN";
            bankConfig.IsBigEndian = false;
            bankConfig.Platform = "PS4";
            bankConfig.PackingType = BankBuilder.PackingTypes.WaveLoaded;
            bankConfig.EncryptionKey = "";
            bankConfig.WaveBuildConfigurations = new List<WaveBuildConfiguration>();

            WaveBuildConfiguration waveConfig1 = new WaveBuildConfiguration();
            waveConfig1.AlignmentSamples = 1152;
            waveConfig1.SampleRate = 32000;
            waveConfig1.CompressionBlockSize = 2;
            waveConfig1.CompressionQuality = 50;
            waveConfig1.CompressionType = WaveEncode.CompressionType.MP3;
            waveConfig1.IsStream = false;
            waveConfig1.IterativeEncoding = "disabled";
            waveConfig1.PerserveTransient = false;
            waveConfig1.HasLipsync = true;
            waveConfig1.FileName = @"x:\rdr3\AUDIO\DEV\ASSETS\WAVES\SS_AM\SCRIPTED_SPEECH\JOHN\JOHN_MARSTON_TEST\JOHN_MARSTON_01.WAV";

            WaveBuildConfiguration waveConfig2 = new WaveBuildConfiguration();
            waveConfig2.AlignmentSamples = 1152;
            waveConfig2.SampleRate = 32000;
            waveConfig2.CompressionBlockSize = 2;
            waveConfig2.CompressionQuality = 50;
            waveConfig2.CompressionType = WaveEncode.CompressionType.MP3;
            waveConfig2.IsStream = false;
            waveConfig2.IterativeEncoding = "disabled";
            waveConfig2.PerserveTransient = false;
            waveConfig2.HasLipsync = true;
            waveConfig2.FileName = @"x:\rdr3\AUDIO\DEV\ASSETS\WAVES\SS_AM\SCRIPTED_SPEECH\JOHN\JOHN_MARSTON_TEST\JOHN_MARSTON_02.WAV";

            WaveBuildConfiguration waveConfig3 = new WaveBuildConfiguration();
            waveConfig3.AlignmentSamples = 1152;
            waveConfig3.SampleRate = 32000;
            waveConfig3.CompressionBlockSize = 2;
            waveConfig3.CompressionQuality = 50;
            waveConfig3.CompressionType = WaveEncode.CompressionType.MP3;
            waveConfig3.IsStream = false;
            waveConfig3.IterativeEncoding = "disabled";
            waveConfig3.PerserveTransient = false;
            waveConfig3.HasLipsync = true;
            waveConfig3.FileName = @"x:\rdr3\AUDIO\DEV\ASSETS\WAVES\SS_AM\SCRIPTED_SPEECH\JOHN\JOHN_MARSTON_TEST\JOHN_MARSTON_03.WAV";

            bankConfig.WaveBuildConfigurations.Add(waveConfig1);
            bankConfig.WaveBuildConfigurations.Add(waveConfig2);
            bankConfig.WaveBuildConfigurations.Add(waveConfig3);

            Package output = pipelineProcessor.Process(bankConfig);

            PackageFile awcFile = output.Files.Last();
            using (FileStream fs = new FileStream(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "testAwc.awc"), FileMode.Create, FileAccess.Write))
            {
                awcFile.Object.SerializeToStream(fs, false);
            }
            
            return 0;
            //end of test code here
            */
            AppDomain.CurrentDomain.UnhandledException += (sender, eventArgs) =>
            {
                logger.Error(eventArgs.ExceptionObject.ToString());
                Environment.Exit(-1);
            };

            ProgramConfiguration config;
            try
            {
                config = new ProgramConfiguration(args);
            }
            catch (rage.ToolLib.CmdLine.CommandLineConfiguration.ParameterException ex)
            {
                logger.Error(ex.Message);
                logger.Info(ex.Usage);
                return -1;
            }

            IAssetManager assetMgr = AssetManagerFactory.GetInstance(AssetManagerType.Perforce, null, config.AssetServer, config.AssetProject,
                config.AssetUsername, config.AssetPassword, config.DepotRoot);

            IChangeList buildChangeList = string.IsNullOrEmpty(config.Change)
                ? assetMgr.CreateChangeList("Build Manager")
                : assetMgr.GetChangeList(config.Change);


            audProjectSettings projectSettings = new audProjectSettings(config.ProjectSettings);
            projectSettings.SetCurrentPlatformByTag(config.Platform);

            IAudioBuildExecutor audioBuildExecutor = new XmlAudioBuildExecutor(projectSettings, assetMgr);
            IPipelineProcessor pipelineProcessor = new LocalPipelineProcessor(new CacheClient());
            audioBuildExecutor.ExecuteAudioBuild(config.Pack, config.Platform, buildChangeList, config.SyncNumber, pipelineProcessor);


            return 0;
        }
    }
}
