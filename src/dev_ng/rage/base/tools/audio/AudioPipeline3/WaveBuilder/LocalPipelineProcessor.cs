﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using rage.ToolLib;
using Rockstar.Audio.Data;
using Rockstar.Audio.Pipeline;
using Wavelib;

namespace Rockstar.Audio.AudioAssetBuilder
{
    public class LocalPipelineProcessor : IPipelineProcessor, INotifyPropertyChanged
    {
        public class MultiException : Exception
        {
            public MultiException(string message, ICollection<Exception> innerExceptions)
                : base(message, innerExceptions.FirstOrDefault())
            {
                InnerExceptions = innerExceptions;
            }
            public ICollection<Exception> InnerExceptions = new ReadOnlyCollection<Exception>(new List<Exception>());
        }

        private CacheClient cache;
        private string _currentStatus;

        public string CurrentStatus
        {
            get { return _currentStatus; }
            set
            {
                lock (this)
                {
                    _currentStatus = value;
                    OnPropertyChanged();
                }
            }
        }

        public LocalPipelineProcessor(CacheClient cache)
        {
            this.cache = cache;
        }

        public Package Process(BankBuildConfiguration bankConfiguration)
        {
            return (Process(new List<BankBuildConfiguration>(new BankBuildConfiguration[] { bankConfiguration }))).First();
        }


        public List<Package> Process(List<BankBuildConfiguration> bankBuildConfigurations)
        {
            TransformBlock<BankBuildConfiguration, Package> bankBlock = new TransformBlock<BankBuildConfiguration, Package>(
                bankBuildConfiguration => processBankBlock(bankBuildConfiguration),
                new ExecutionDataflowBlockOptions { MaxDegreeOfParallelism = Environment.ProcessorCount }
            );

            foreach (BankBuildConfiguration bankBuildConfiguration in bankBuildConfigurations)
            {
                //invoke wave build pipeline
                bankBlock.Post(bankBuildConfiguration);
            }
            bankBlock.Complete();

            List<Package> builtBankPackages = new List<Package>();
            for (int i = 0; i < bankBuildConfigurations.Count; i++)
            {
                try
                {
                    builtBankPackages.Add(bankBlock.Receive());
                }
                catch (InvalidOperationException)
                {
                    ICollection<Exception> exceptions = bankBlock.Completion.Exception.Flatten().InnerExceptions;
                    throw new MultiException("Error processing banks" + (exceptions.Count < 1 ? " (" + (exceptions.Count - 1) + " more Exceptions)" : "") + ": " + "\n" + exceptions.First().Message, exceptions);
                }
            }
            return builtBankPackages;
        }

        private Package processBankBlock(BankBuildConfiguration bankBuildConfiguration)
        {
            
            TransformBlock<WaveBuildConfiguration, Package> waveBlock = new TransformBlock<WaveBuildConfiguration, Package>(
                waveBuildConfiguration => processWaveBlock(waveBuildConfiguration),
                new ExecutionDataflowBlockOptions { MaxDegreeOfParallelism = Environment.ProcessorCount }
            );

            Package inputPackage = new Package();
            inputPackage.Properties.SetValue(BankBuilder.BANK_NAME, bankBuildConfiguration.BankName);
            inputPackage.Properties.SetValue(BankBuilder.BANK_PACKING_TYPE, bankBuildConfiguration.PackingType.ToString());
            inputPackage.Properties.SetValue(BankBuilder.BANK_ISBIGENDIAN, bankBuildConfiguration.IsBigEndian);
            inputPackage.Properties.SetValue(BankBuilder.BANK_ENCRYPTIONKEY, bankBuildConfiguration.EncryptionKey);
            inputPackage.Properties.SetValue(BankBuilder.BANK_PLATFORM, bankBuildConfiguration.Platform);

            foreach (string genericChunkFile in bankBuildConfiguration.GenericChunkFiles)
            {

                PackageFile inputFile = new PackageFile();
                inputFile.Object = new PackageGenericObject(genericChunkFile);
                inputFile.SetObjectNameAndTypeFromFileName(genericChunkFile);
                if (Enum<ChunkType>.TryParse(inputFile.ObjectType, true) == null) throw new Exception(genericChunkFile + " is not a supported chunk type");
                inputPackage.Files.Add(inputFile);
            }

            int numberOfWaves = bankBuildConfiguration.WaveBuildConfigurations.Count;
            int currentWaveNumber = 0;
            foreach (WaveBuildConfiguration waveBuildConfiguration in bankBuildConfiguration.WaveBuildConfigurations)
            {
                //invoke wave build pipeline
                waveBlock.Post(waveBuildConfiguration);
            }

            waveBlock.Complete();

            for (int i = 0; i < bankBuildConfiguration.WaveBuildConfigurations.Count; i++)
            {
                Package wavePipelineOutputPackage;
                try
                {
                    CurrentStatus = string.Format("{1:P2} Processing: {0} ", bankBuildConfiguration.WaveBuildConfigurations[i].FileName, ++currentWaveNumber / (double)numberOfWaves);
                    wavePipelineOutputPackage = waveBlock.Receive();
                    
                }
                catch (InvalidOperationException)
                {
                    ICollection<Exception> exceptions = waveBlock.Completion.Exception.Flatten().InnerExceptions;
                    throw new MultiException("Bank " + bankBuildConfiguration.BankName + " could not be processed" + (exceptions.Count < 1 ? " (" + (exceptions.Count - 1) + " more Exceptions)" : "") + ": " + "\n" + exceptions.First().Message, exceptions);
                }
                foreach (PackageFile file in wavePipelineOutputPackage.Files)
                {
                    inputPackage.Files.Add(file);
                }
            }
            CurrentStatus = string.Format("{0} Waves Processed", numberOfWaves);
            return ProcessPackage(this.cache, inputPackage, new BankBuilder());
        }


        private Package processWaveBlock(WaveBuildConfiguration waveBuildConfiguration)
        {
            
            //implement wave processing pipeline
            var peakNormaliseBlock =
                new TransformBlock<Package, Package>(
                    inputPack => ProcessPackage(cache, inputPack, new PeakNormalise()));
            var resampleBlock = new TransformBlock<Package, Package>(inputPack =>
            {
                if (inputPack.Properties.GetValue(Resample.TARGETED_SAMPLERATE, 0) >
                    inputPack.Properties.GetValue(Resample.LOOP_ALIGNMENT, 0))
                    return ProcessPackage(cache, inputPack, new Resample());
                else
                    return inputPack;
            });
            var peakEnvelopeBlock =
                new TransformBlock<Package, Package>(
                    inputPack => ProcessPackage(cache, inputPack, new PeakEnvelope()));
            var encodeBlock = new TransformBlock<Package, Package>(inputPack =>
            {
                //overwrite encoder settings with special cases
                //1. if there are less than 1152 samples for mp3 fall back to pcm
                if (inputPack.Properties.GetValue(WaveEncode.COMPRESSION_TYPE,
                    WaveEncode.CompressionType.PCM.ToString()).Equals(WaveEncode.CompressionType.MP3.ToString())
                    && inputPack.Properties.GetValue(Resample.NUMBER_OF_SAMPLES, 0) < (1152 * 1.5))
                {
                    inputPack.Properties.SetValue(WaveEncode.COMPRESSION_TYPE,
                        WaveEncode.CompressionType.PCM.ToString());
                }
                //2. if there are less than 128 * 1.5 (heuristic value) samples for xma fall back to pcm
                if (inputPack.Properties.GetValue(WaveEncode.COMPRESSION_TYPE,
                    WaveEncode.CompressionType.PCM.ToString()).Equals(WaveEncode.CompressionType.MP3.ToString())
                    && inputPack.Properties.GetValue(Resample.NUMBER_OF_SAMPLES, 0) < (128 * 1.5))
                {
                    inputPack.Properties.SetValue(WaveEncode.COMPRESSION_TYPE,
                        WaveEncode.CompressionType.PCM.ToString());
                }
                return ProcessPackage(cache, inputPack, new WaveEncode());
            });

            DataflowLinkOptions linkOptions = new DataflowLinkOptions { PropagateCompletion = true };
            peakNormaliseBlock.LinkTo(resampleBlock, linkOptions);
            resampleBlock.LinkTo(peakEnvelopeBlock, linkOptions);
            peakEnvelopeBlock.LinkTo(encodeBlock, linkOptions);
            /*
            peakNormaliseBlock.Completion.ContinueWith(t =>
            {
                if (t.IsFaulted) ((IDataflowBlock) resampleBlock).Fault(t.Exception);
                else resampleBlock.Complete();
            });
            resampleBlock.Completion.ContinueWith(t =>
            {
                if (t.IsFaulted) ((IDataflowBlock) peakEnvelopeBlock).Fault(t.Exception);
                else peakEnvelopeBlock.Complete();
            });
            peakEnvelopeBlock.Completion.ContinueWith(t =>
            {
                if (t.IsFaulted) ((IDataflowBlock) encodeBlock).Fault(t.Exception);
                else encodeBlock.Complete();
            });
             * */

            //setup input package
            Package inputPackage = new Package();
            var inputWaveFile = new PackageFile();
            WaveSample inputWaveSample = new WaveSample(new bwWaveFile(waveBuildConfiguration.FileName, false));
            inputWaveFile.Object = new PackageWaveObject(inputWaveSample);
            inputWaveFile.SetObjectNameAndTypeFromFileName(waveBuildConfiguration.FileName);
            inputPackage.Files.Add(inputWaveFile);

            inputWaveFile.Properties.SetValue(PeakNormalise.DESIRED_PEAK_THRESHOLD,
                waveBuildConfiguration.DesiredPeakLevel);
            if (waveBuildConfiguration.AddSilenceMarkers)
            {
                inputWaveFile.Properties.SetValue(PeakNormalise.ADD_SILENCE_MARKERS, waveBuildConfiguration.AddSilenceMarkers);
                if (waveBuildConfiguration.SilenceVolumeThreshold != null) inputWaveFile.Properties.SetValue(PeakNormalise.SILENCE_VOLUME_THRESHOLD, waveBuildConfiguration.SilenceVolumeThreshold.Value);
                if (waveBuildConfiguration.SilenceLengthTreshold != null) inputWaveFile.Properties.SetValue(PeakNormalise.SILENCE_LENGTH_THRESHOLD, waveBuildConfiguration.SilenceLengthTreshold.Value);
            }
            inputWaveFile.Properties.SetValue(Resample.LOOP_ALIGNMENT, waveBuildConfiguration.AlignmentSamples);
          
            inputWaveFile.Properties.SetValue(Resample.TARGETED_SAMPLERATE, waveBuildConfiguration.SampleRate);
            //initialize RESAMPLED_SAMPLE_RATE and NUMBER_OF_SAMPLES with the actual sample rate in order to have a valid value even if resampling gets skipped
            inputWaveFile.Properties.SetValue(Resample.RESAMPLED_SAMPLE_RATE, inputWaveSample.SampleRate);
            inputWaveFile.Properties.SetValue(Resample.NUMBER_OF_SAMPLES, inputWaveSample.NumSamples);
            inputWaveFile.Properties.SetValue(WaveEncode.COMPRESSION_TYPE,
                waveBuildConfiguration.CompressionType.ToString());
            inputWaveFile.Properties.SetValue(WaveEncode.COMPRESSION_QUALITY,
                waveBuildConfiguration.CompressionQuality);
            inputWaveFile.Properties.SetValue(WaveEncode.COMPRESSION_BLOCKSIZE,
                waveBuildConfiguration.CompressionBlockSize);
            inputWaveFile.Properties.SetValue(WaveEncode.HAS_LIPSYNC, waveBuildConfiguration.HasLipsync);
            inputWaveFile.Properties.SetValue(WaveEncode.IS_STREAM, waveBuildConfiguration.IsStream);
            inputWaveFile.Properties.SetValue(WaveEncode.ITERATIVE_ENCODING,
                waveBuildConfiguration.IterativeEncoding);
            inputWaveFile.Properties.SetValue(WaveEncode.PRESERVE_TRANSIENT,
                waveBuildConfiguration.PerserveTransient);
            inputWaveFile.CalculateHash();

            //start processing

            peakNormaliseBlock.Post(inputPackage);
            peakNormaliseBlock.Complete();
       
            
            try
            {
                return encodeBlock.Receive();
            }
            catch (InvalidOperationException)
            {
                Exception ex = encodeBlock.Completion.Exception.Flatten().InnerException;
                throw new Exception("Wave " + waveBuildConfiguration.FileName + " could not be processed: " + ex.Message, ex);
            }
        }


        static Package ProcessPackage(CacheClient cache, Package inputPackage, IPipelineStageProcessor processor)
        {
            inputPackage.Properties["Tool"] = processor.ToolIdentifier;
            var cacheEntry = cache.Access(inputPackage);
            if (cacheEntry != null)
            {
                Console.WriteLine("Found result in cache");
            }
            else
            {
                var outputPackage = processor.Process(inputPackage);
                cacheEntry = new CacheEntry(inputPackage, outputPackage);
                cache.Write(cacheEntry);
            }
            return cacheEntry.Output;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
