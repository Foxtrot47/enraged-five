﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.XPath;
using NLog;
using rage.ToolLib.WavesFiles;

namespace Rockstar.Audio.AudioAssetBuilder.XmlBuildExecutor
{
    public class XmlBuiltWavesInfo
    {
        protected XDocument builtWaves;
        private Dictionary<string, XmlBuiltBankNodeInfo> modifiedBanks = new Dictionary<string, XmlBuiltBankNodeInfo>();
        private Dictionary<string, XElement> deletedBanks = new Dictionary<string, XElement>();

        /// <summary>
        /// Returns the node paths to all modified banks
        /// </summary>
        public Dictionary<string, XmlBuiltBankNodeInfo> getModifiedBanks()
        {
            return modifiedBanks;
        }

        /// <summary>
        /// Returns the relative paths to all deleted banks
        /// </summary>
        public Dictionary<string, XElement> getDeletedBanks()
        {
            return deletedBanks;
        }

        public XmlBuiltWavesInfo(XDocument builtwaves)
        {
            this.builtWaves = builtwaves;
        }

        public List<LogMessage> updateBuiltWaves(XElement pendingPackElement)
        {
            return UpdateBuiltWaves(pendingPackElement, "/", false);
        }

        public class LogMessage
        {
            public LogMessage(LogLevel level, string message)
            {
                this.Level = level;
                this.Message = message;
            }

            public LogLevel Level;
            public string Message;
        }

        private List<LogMessage> UpdateBuiltWaves(XElement pendingElement, string parentNodePath, bool hasHitBankLevel)
        {
            List<LogMessage> messages = new List<LogMessage>();
            // update nodePath
            var sb = new StringBuilder(parentNodePath);
            sb.Append("/");
            sb.Append(pendingElement.Name);
            sb.Append("[@name='");
            sb.Append(pendingElement.Attribute("name").Value);
            sb.Append("']");

            var nodePath = sb.ToString();

            // use nodePath to find corresponding
            var builtElement = this.builtWaves.Root.XPathSelectElement(nodePath);

            if (!hasHitBankLevel)
            {
                switch (pendingElement.Name.ToString())
                {
                    case "Bank":
                        hasHitBankLevel = true;
                        if (pendingElement.Attribute("operation") != null
                            && pendingElement.Attribute("operation").Value == "remove")
                        {
                            var bankName = pendingElement.Attribute("name").Value;
                            var bankPath = System.IO.Path.Combine(
                                pendingElement.Ancestors("Pack").First().Attribute("name").Value, bankName);

                            // Ensure that any prior operations that would have led to this bank being rebuilt are ignored
                            if (this.modifiedBanks.ContainsKey(nodePath))
                            {
                                this.modifiedBanks.Remove(nodePath);
                            }
                            if (!deletedBanks.ContainsKey(bankPath)) deletedBanks.Add(bankPath, builtElement);
                        }
                        else if (pendingElement.Attribute("operation") != null
                                 && pendingElement.Attribute("operation").Value == "modify")
                        {
                            if (!this.modifiedBanks.ContainsKey(nodePath))
                            {
                                this.modifiedBanks.Add(nodePath, new XmlBuiltBankNodeInfo(builtElement));
                            }
                        }
                        //adding banks is handled further down where the add operation is handled (builtElement needs to be created first)
                        break;
                    case "Tag":
                        var element = builtWaves.Root.XPathSelectElement(parentNodePath);
                        if (element != null)
                        {
                            markBanksAsModified(builtWaves.Root.XPathSelectElement(parentNodePath), parentNodePath);
                        }

                        break;
                }
            }

            var opAttribute = pendingElement.Attribute("operation");
            if (opAttribute != null)
            {
                switch (opAttribute.Value)
                {
                    case "add":
                        if (!(pendingElement.Name == "Tag" && pendingElement.Attribute("name").Value == "rebuild"))
                        {
                            // the element we are trying to add should not exist
                            if (builtElement != null)
                            {
                                messages.Add(new LogMessage(LogLevel.Warn,
                                    "Item being added already exists: " +
                                    pendingElement.Attribute("name").Value));
                            }
                            else
                            {
                                var newElement = new XElement(pendingElement.Name, pendingElement.Attributes());
                                newElement.Attribute("operation").Remove();

                                var builtParent = parentNodePath == "/"
                                    ? this.builtWaves.Root
                                    : this.builtWaves.Root.XPathSelectElement(parentNodePath);
                                if (builtParent != null)
                                {
                                    messages.Add(new LogMessage(LogLevel.Trace,
                                        "Adding node with parent: " + parentNodePath + " , element: " + newElement));
                                    if (pendingElement.Attribute("name").Value == "DoNotBuild")
                                    {
                                        builtParent.SetAttributeValue("builtSize", "0");
                                        foreach (var element in builtParent.Descendants())
                                        {
                                            if (element.Name == "Chunk")
                                            {
                                                element.SetAttributeValue("size", "0");
                                            }
                                        }
                                    }

                                    builtParent.Add(newElement);
                                    if (pendingElement.Name.ToString() == "Bank" &&
                                        !this.modifiedBanks.ContainsKey(nodePath))
                                    {
                                        this.modifiedBanks.Add(nodePath, new XmlBuiltBankNodeInfo(newElement));
                                    }
                                }
                                else
                                {
                                    messages.Add(new LogMessage(LogLevel.Warn,
                                        "Warning: trying to add node with no parent: " + parentNodePath + ", " +
                                        newElement));
                                }
                            }
                        }

                        break;
                    case "remove":

                        // If the element we are deleting does not exist its ancestor must have been deleted
                        if (builtElement != null)
                        {
                            messages.Add(new LogMessage(LogLevel.Trace,
                                "Removing built element " + builtElement + ", with parent: " + parentNodePath));
                            builtElement.Remove();
                        }

                        break;
                    case "modify":
                        if (pendingElement.Name == "Tag")
                        {
                            // need to copy the modified tag value over to built waves
                            var szTag = pendingElement.Attribute("value").Value;
                            if (builtElement != null)
                            {
                                messages.Add(new LogMessage(LogLevel.Trace,
                                    "Setting tag " + szTag + " on built element " + builtElement));
                                builtElement.Attribute("value").SetValue(pendingElement.Attribute("value").Value);
                            }
                            else
                            {
                                messages.Add(new LogMessage(LogLevel.Warn,
                                    "Modify tag with no builtElement: " + pendingElement));
                            }
                        }
                        else
                        {
                            var nameAttribute = pendingElement.Attribute("name");
                            messages.Add(new LogMessage(LogLevel.Trace,
                                "Operation: " + opAttribute.Value + " on parent node " + parentNodePath + ", wave: " +
                                (nameAttribute != null ? nameAttribute.Value : string.Empty)));
                        }

                        break;
                }
            }

            var children = pendingElement.Elements().ToList();


            foreach (var child in children)
            {
                messages.AddRange(this.UpdateBuiltWaves(child, nodePath, hasHitBankLevel));
            }


            if (!pendingElement.Nodes().Any())
            {
                pendingElement.Remove();
            }
            else if (pendingElement.Attribute("operation") != null)
            {
                pendingElement.Attribute("operation").Remove();
            }
            return messages;
        }

        private void markBanksAsModified(XElement builtElement, string path)
        {
            if (builtElement.Name == "Bank")
            {
                if (!this.modifiedBanks.ContainsKey(path))
                {
                    this.modifiedBanks.Add(path, new XmlBuiltBankNodeInfo(builtElement));
                }
            }
            else
            {
                foreach (XElement child in builtElement.Nodes())
                {
                    var sb = new StringBuilder(path);
                    sb.Append("/");
                    sb.Append(child.Name);
                    sb.Append("[@name='");
                    sb.Append(child.Attribute("name").Value);
                    sb.Append("']");

                    this.markBanksAsModified(child, sb.ToString());
                }
            }

        }

        public XmlBuiltBankNodeInfo GetBuiltBankInfo(string bankname)
        {
            XElement builtElement =
                this.builtWaves.Root.Document.Descendants("Bank")
                    .FirstOrDefault(
                        p => p.Attribute("name").Value.Equals(bankname, StringComparison.InvariantCultureIgnoreCase));

            if (builtElement == null)
            {
                throw new Exception(string.Format("Bank with name: {0} not found in builtwaves.", bankname));
            }
            return new XmlBuiltBankNodeInfo(builtElement);
        }
    }
}
