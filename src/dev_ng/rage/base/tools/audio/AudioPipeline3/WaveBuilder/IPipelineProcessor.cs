﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Rockstar.Audio.Pipeline;

namespace Rockstar.Audio.AudioAssetBuilder
{
    public interface IPipelineProcessor
    {
        Package Process(BankBuildConfiguration bankConfiguration);
        List<Package> Process(List<BankBuildConfiguration> bankConfigurations);
    }
}
