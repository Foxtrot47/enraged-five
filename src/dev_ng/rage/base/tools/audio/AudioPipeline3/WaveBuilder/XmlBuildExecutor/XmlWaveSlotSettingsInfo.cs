﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using rage.ToolLib;
using Rockstar.Audio.Pipeline;

namespace Rockstar.Audio.AudioAssetBuilder.XmlBuildExecutor
{
    public class XmlWaveSlotSettingsInfo
    {

        private XDocument waveSlotSettings;

        public XmlWaveSlotSettingsInfo(string waveSlotSettingsPath)
        {
            waveSlotSettings = XDocument.Load(waveSlotSettingsPath);
        }

        private enum LoadType
        {
            Wave,
            Bank,
            Stream
        }

        public BankBuilder.PackingTypes GetBankPackingType(XElement bankNode)
        {
            string bankFolderName = "";
            var parent = bankNode;
            do
            {
                parent = parent.Parent;
                if (!bankFolderName.Equals("")) bankFolderName = parent.Attribute("name").Value+"\\"+bankFolderName;
                else bankFolderName = parent.Attribute("name").Value;
            } while (!parent.Name.LocalName.Equals("Pack"));

            string packName = parent.Attribute("name").Value;
            string bankPath = Path.Combine(packName, bankNode.Attribute("name").Value);

            var loadables = waveSlotSettings.Root.Descendants("Loadable");
            var loadTypes = new List<string>();
            foreach (var loadable in loadables)
            {
                var name = loadable.Attribute("name").Value;
                if (name.Equals(bankPath, StringComparison.OrdinalIgnoreCase) ||
                    name.Equals(packName, StringComparison.OrdinalIgnoreCase) ||
                    name.Equals(bankFolderName, StringComparison.OrdinalIgnoreCase) ||
                    bankFolderName.ToUpper().StartsWith(name.ToUpper()))
                {
                    loadTypes.Add(loadable.Parent.Attribute("loadType").Value);
                }
            }

            if (loadTypes.Count == 0)
            {
                throw new ApplicationException(string.Format("Bank: {0} is not associated with a wave slot", bankPath));
            }

            loadTypes = loadTypes.Distinct().ToList();
            if (loadTypes.Count > 1)
            {
                throw new ApplicationException(string.Format("More than one load type associated with bank: {0}", bankPath));
            }

            LoadType loadType = Enum<LoadType>.Parse(loadTypes[0]);
            switch (loadType)
            {
                case LoadType.Bank:
                    return BankBuilder.PackingTypes.BankLoaded;
                case LoadType.Stream:
                    return BankBuilder.PackingTypes.Stream;
                case LoadType.Wave:
                    return BankBuilder.PackingTypes.WaveLoaded;
            }
            //default, should never be reached
            return BankBuilder.PackingTypes.WaveLoaded;
        }

    }
}
