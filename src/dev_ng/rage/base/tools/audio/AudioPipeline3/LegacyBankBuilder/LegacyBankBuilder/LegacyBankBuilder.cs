﻿using System.IO;
using System.Xml.Linq;
using audAssetBuilderCommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using audAssetBuilderProperties;
using rage;
using rage.ToolLib;
using rage.ToolLib.Encryption;
using Rockstar.Audio.Data;
using Rockstar.Audio.Pipeline;

namespace LegacyBankBuilder
{
    public class LegacyBankBuilder: BankBuildBase
    {
        private string Pack;
        private string BankPath;

        public LegacyBankBuilder(string[] args)
            : base(args)
        {
            var tokens = InputDirectory.ToUpper().Split('\\');
            Pack = tokens[tokens.Length - 3];
            BankPath = Pack + "\\" + tokens[tokens.Length - 2];
        }

        public override void Run()
        {
            audProjectSettings projSettings = new audProjectSettings(ProjectSettingsPath);
            projSettings.SetCurrentPlatformByTag(Platform);

            string packingType = BankPackingType;
            if (string.IsNullOrEmpty(packingType))
            {
                LoadType loadType = GetBankLoadType();
                packingType = GetPackingType(loadType).ToString();
            }
            

            var inputPackage = new Package();
            inputPackage.Properties.SetValue(BankBuilder.BANK_NAME, Name);
            inputPackage.Properties.SetValue(BankBuilder.BANK_PACKING_TYPE, packingType);
            inputPackage.Properties.SetValue(BankBuilder.BANK_ISBIGENDIAN, projSettings.GetCurrentPlatform().IsBigEndian);
            inputPackage.Properties.SetValue(BankBuilder.BANK_ENCRYPTIONKEY, projSettings.GetCurrentPlatform().EncryptionKey);
            inputPackage.Properties.SetValue(BankBuilder.BANK_PLATFORM, Platform);

            

            Dictionary<string, WaveProperties>  objectWaveProperties = new Dictionary<string, WaveProperties>();
            BankProperties bankProperties = new BankProperties();

            //add all objects to the input package
            var dir = new DirectoryInfo(InputDirectory);
            foreach (var file in dir.GetFiles())
            {
                bool isWavePropertiesFile = String.Compare(file.Extension, ".xml", true) == 0;
                //add all chunk data if file is a wave properties xml file, or if file is data file of a streaming bank
                if (isWavePropertiesFile || file.Name.ToUpper().Equals(Name.ToUpper()+"."+ChunkType.DATA))
                {
                    var pathWithoutExtension = Path.Combine(Path.GetDirectoryName(file.FullName), Path.GetFileNameWithoutExtension(file.FullName));

                    //add all other recognised files
                    foreach(string fileExtension in Enum.GetNames(typeof(ChunkType)))
                    {
                        string filename = pathWithoutExtension + "." + fileExtension;
                        if (File.Exists(filename))
                        {
                            long fileSize = (new FileInfo(filename)).Length;
                            if(fileSize==0) continue;
                            PackageFile inputFile = new PackageFile();
                            inputFile.Object = new PackageGenericObject(filename);
                            inputFile.SetObjectNameAndTypeFromFileName(filename);
                            inputPackage.Files.Add(inputFile);
                        }
                    }

                    if (isWavePropertiesFile)
                    {
                        //add properties file to input files
                        PackageFile wavePropertiesFile = new PackageFile();
                        wavePropertiesFile.Object = new PackageGenericObject(file.FullName);
                        wavePropertiesFile.SetObjectNameAndTypeFromFileName(file.FullName);
                        inputPackage.Files.Add(wavePropertiesFile);
                        objectWaveProperties.Add(Path.GetFileNameWithoutExtension(file.FullName), bankProperties.AddWaveProperties(new WaveProperties(file.FullName).RootNode));
                    }
                }

            }

            var outputPackage = ProcessPackage(new CacheClient(), inputPackage, new BankBuilder());
            
            //the last file in the package is the newly generated awc file
            PackageFile outputFile = outputPackage.Files.Last();
            // Need an explicit load here in case we are fetching from the cache
            outputFile.LoadObject();

            string outputFilePath = Path.Combine(OutputDirectory, outputFile.FileName);
            using (FileStream fs = new FileStream(outputFilePath, FileMode.Create, FileAccess.Write))
            {
                outputFile.SaveObject(fs);
            }

            //write bank properties
            //---------------------

            // Waves within interleaved streams don't exist as discrete objects
            if (packingType != BankBuilder.PackingTypes.Stream.ToString())
            {
                foreach (var kvp in objectWaveProperties)
                {
                    ContainerObject correspondingContainerObject =
                        ((PackageWaveContainer) outputFile.Object).Container.Objects.First(
                            x => x.Name.Equals(kvp.Key));
                    objectWaveProperties[kvp.Key].BuiltAsset.NameHash = correspondingContainerObject.NameHash;
                    foreach (Chunk chunk in correspondingContainerObject.Chunks)
                    {
                        objectWaveProperties[kvp.Key].BuiltAsset.AddOrUpdateChunk(chunk.TypeName, chunk.Data.Length);
                    }
                    objectWaveProperties[kvp.Key].BuiltAsset.Size = (uint)correspondingContainerObject.SerialisedSize;
                }

                bankProperties.HeaderSize = (uint)((PackageWaveContainer) outputFile.Object).Container.SerialisedDataOffset;
            }
            else
            {
                ContainerObject globalObject =
                    ((ContainerStreamed) ((PackageWaveContainer) outputFile.Object).Container).getGlobalObject();
                foreach (Chunk chunk in globalObject.Chunks) bankProperties.AddOrUpdateChunk(chunk.TypeName, chunk.Data.Length);
                
                
                Chunk dataChunk = globalObject.FindChunk(ChunkType.DATA.ToString());
                if (dataChunk == null) throw new Exception("No data chunk found for global object in " + outputFile);
                
                // header size for streaming banks is whatever gets us to the start of the data chunk
                // Note that this will include whatever padding was necessary to align the data 
                // chunk, in practise this should only waste a few bytes so probably isn't worth worrying 
                // about.
                bankProperties.HeaderSize = (uint)dataChunk.SerialisedAtOffset;
                
                Console.WriteLine("Streaming bank, header size: {0}", bankProperties.HeaderSize);
            }

            bankProperties.Size = (new FileInfo(outputFilePath)).Length;
            bankProperties.DateTime = DateTime.Now;
            bankProperties.MetadataSize = 8*1024; //value copied from old pipeline implementation, was only used for ps3
            bankProperties.AwcMd5 = Utility.FileMd5(outputFilePath);

            // bank properties must be written to build folder, not final output path
            var bankPropertiesPath = InputDirectory.Trim('\\') + ".xml";
            bankProperties.Save(bankPropertiesPath);

            // Optional whole-file encryption for bank-loaded containers
            if (packingType == BankBuilder.PackingTypes.BankLoaded.ToString())
            {
                IEncrypter encrypter = EncrypterFactory.Create(projSettings.GetCurrentPlatform().EncryptionKey);
                if (encrypter.IsEncrypting) EncryptFile(outputFilePath, encrypter);
            }
        }

        static Package ProcessPackage(CacheClient cache, Package inputPackage, IPipelineStageProcessor processor)
        {
            inputPackage.Properties["Tool"] = processor.ToolIdentifier;
            var cacheEntry = cache.Access(inputPackage);
            if (cacheEntry != null)
            {
                Console.WriteLine("Found result in cache");
            }
            else
            {
                var outputPackage = processor.Process(inputPackage);
                cacheEntry = new CacheEntry(inputPackage, outputPackage);
                cache.Write(cacheEntry);
            }
            return cacheEntry.Output;
        }

        private enum LoadType
        {
            Wave,
            Bank,
            Stream
        }

        private void EncryptFile(string filePath, IEncrypter encrypter)
        {
            byte[] data;
            using (var f = new FileStream(filePath, FileMode.Open, FileAccess.Read))
            {
                data = new byte[f.Length];
                f.Read(data, 0, (int)f.Length);
                f.Close();
            }

            data = encrypter.Encrypt(data, true);

            using (var f = new FileStream(filePath, FileMode.Create, FileAccess.Write))
            {
                f.Write(data, 0, data.Length);
                f.Close();
            }
        }

        private LoadType GetBankLoadType()
        {
            var waveSlotSettings = XDocument.Load(WaveSlotSettingsPath);
            var loadables = waveSlotSettings.Root.Descendants("Loadable");
            var loadTypes = new List<string>();
            foreach (var loadable in loadables)
            {
                var name = loadable.Attribute("name").Value;
                if (name.Equals(BankPath, StringComparison.OrdinalIgnoreCase) ||
                    name.Equals(Pack, StringComparison.OrdinalIgnoreCase) ||
                    name.Equals(BankFolderName, StringComparison.OrdinalIgnoreCase) ||
                    (BankFolderName!=null && BankFolderName.ToUpper().StartsWith(name.ToUpper())))
                {
                    loadTypes.Add(loadable.Parent.Attribute("loadType").Value);
                }
            }

            if (loadTypes.Count == 0)
            {
                throw new ApplicationException(string.Format("Bank: {0} is not associated with a wave slot", BankPath));
            }

            loadTypes = loadTypes.Distinct().ToList();
            if (loadTypes.Count > 1)
            {
                throw new ApplicationException(string.Format("More than one load type associated with bank: {0}", BankPath));
            }

            return Enum<LoadType>.Parse(loadTypes[0]);
        }

        private BankBuilder.PackingTypes GetPackingType(LoadType loadType)
        {  
            bool isStreamingBank = StreamingBlockBytes > 0;
            if (isStreamingBank && loadType != LoadType.Stream)
            {
                throw new ApplicationException(
                    string.Format("Streaming Bank: {0} is not associated with a streaming wave slot", BankPath));
            }

            if (isStreamingBank)
            {
                // Streaming banks require special packing
                return BankBuilder.PackingTypes.Stream;
            }
            return (loadType == LoadType.Bank ? BankBuilder.PackingTypes.BankLoaded : BankBuilder.PackingTypes.WaveLoaded);
        }


    }
}
