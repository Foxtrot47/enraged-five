﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Rockstar.Audio.Pipeline;

namespace cachetester
{
    class Program
    {
        static void Main(string[] args)
        {
            var client = new CacheClient(@"x:\audiocache\");
            Console.WriteLine("Created default cache client.  Path: {0}, IsReadable: {1}, IsWriteable: {2}", client.CacheLocation, client.IsReadable, client.IsWriteable);

            var testInputPackage = new Package(null);

            testInputPackage.Files.Add(new PackageFile() { ObjectName = "test", ObjectType = "wav", Size = 12345, Hash = "123456" });
            testInputPackage.Properties.Add("TestProp2", "TestValue2");
            testInputPackage.Properties.Add("A_TestProp1", "TestValue1");
            testInputPackage.Properties.Add("Tool", "resampler-1.0");

            Console.WriteLine("Input package:");
            Console.WriteLine(testInputPackage.SerializeMetadata().ToString());
            Console.WriteLine("-------------------------");

            var cacheResult = client.Access(testInputPackage);
            if (cacheResult != null)
            {
                Console.WriteLine("Found result in cache:");
                Console.WriteLine(cacheResult.Output.SerializeMetadata().ToString());
                Console.WriteLine("-------------------------");
            }
            else
            {
                Console.WriteLine("No hit in cache, will try to add");
                var outputPackage = new Package(null);
                outputPackage.Files.Add(new PackageFile() { ObjectName = "test", ObjectType = "wav", Size = 12435, Hash = "567890" });
                outputPackage.Properties.Add("Headroom", "-2.0");
                client.Write(new CacheEntry(testInputPackage, outputPackage));
            }
        }
    }
}
