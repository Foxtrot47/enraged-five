﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rockstar.Audio.Pipeline
{
    public class CacheEntry
    {
        public CacheEntry(Package input, Package output)
        {
            Input = input;
            Output = output;
        }

        public Package Input
        {
            get;
            private set;
        }

        public Package Output
        {
            get;
            private set;
        }
    }
}
