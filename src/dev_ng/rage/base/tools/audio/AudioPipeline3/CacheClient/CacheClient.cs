﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Security.Cryptography;

namespace Rockstar.Audio.Pipeline
{
    public class CacheClient
    {
        public bool IsWriteable {get; private set;}
        public bool IsReadable { get; private set; }
        
        const string RS_AUDIO_CACHE_VAR = "RS_AUDIO_CACHE";
        const string OUTPUT_FOLDER_NAME = "output";
        const string CACHE_RO_CONTROL = "readonly.txt";
        const string CACHE_WO_CONTROL = "writeonly.txt";
        const string CACHE_DISABLE_CONTROL = "disabled.txt";

        private string m_CacheLocation;
        public string CacheLocation
        {
            get
            {
                return m_CacheLocation;
            }
            set
            {
                m_CacheLocation = value;
                bool isAvailable = !string.IsNullOrEmpty(CacheLocation) && Directory.Exists(CacheLocation);
                if (isAvailable)
                {
                    var di = new DirectoryInfo(CacheLocation);
                    IsWriteable = (di.Exists && di.Attributes != FileAttributes.ReadOnly);
                    IsReadable = true;
                    
                    if (File.Exists(Path.Combine(CacheLocation, CACHE_DISABLE_CONTROL)))
                    {
                        IsReadable = false;
                        IsWriteable = false;
                    }
                    else
                    {
                        if (File.Exists(Path.Combine(CacheLocation, CACHE_RO_CONTROL)))
                        {
                            IsWriteable = false;
                        }
                        if (File.Exists(Path.Combine(CacheLocation, CACHE_WO_CONTROL)))
                        {
                            IsReadable = false;
                        }
                    }
                }
                else
                {
                    IsWriteable = false;
                    IsReadable = false;
                }
            }
        }

        public CacheClient()
        {            
            CacheLocation = Environment.GetEnvironmentVariable(RS_AUDIO_CACHE_VAR);
        }

        public CacheClient(string location)
        {
            CacheLocation = location;
        }

        /// <summary>
        /// Lookup the specified input package in the cache
        /// </summary>
        /// <param name="inputPackage">Metadata describing the input package</param>
        /// <returns>Cache entry information, or null if the input package result has not been cached</returns>
        public CacheEntry Access(Package inputPackage)
        {
            if(!IsReadable)
            {
                return null;
            }
            if (!inputPackage.HasFileHashes)
            {
                return null;
            }
            var contentHash = DataHash.ComputeHash(inputPackage.SerializeMetadata());
            var path = ComputeCacheLocation(inputPackage.Properties["Tool"], contentHash);
            if (!Directory.Exists(path))
            {
                return null;
            }
            // inputPackage should be identical to a new package created from the input manifest
            return new CacheEntry(inputPackage,
                                    Package.CreatePackageFromManifest(Path.Combine(path, Package.OUTPUT_MANIFEST), Path.Combine(path, OUTPUT_FOLDER_NAME)));
        }

        string ComputeCacheLocation(string toolIdentifier, string contentHash)
        {
            return Path.Combine(CacheLocation,
                                    toolIdentifier,
                                    contentHash.Substring(0, 2),
                                    contentHash.Substring(2, 2),
                                    contentHash);
        }

        /// <summary>
        /// Writes the specified result package to the cache
        /// </summary>
        /// <param name="cacheEntry">Package metadata describing the input and result</param>
        /// <returns>true on success</returns>
        public bool Write(CacheEntry entry)
        {
            if (!IsWriteable)
            {
                return false;
            }
            if (!entry.Input.HasFileHashes)
            {
                return false;
            }
            bool success = false;
            try
            {
                var contentHash = DataHash.ComputeHash(entry.Input.SerializeMetadata());
                var path = ComputeCacheLocation(entry.Input.Properties["Tool"], contentHash);
                if (Directory.Exists(path))
                {
                    // Cannot write to the destination if it already exists
                    return false;
                }

                // Create a temporary directory to write to, so it can be published to the cache atomically once all writes have completed.
                string tempFolder = path + ".tmp";
                Directory.CreateDirectory(tempFolder);
                
                string outputFilePath = Path.Combine(tempFolder, OUTPUT_FOLDER_NAME);

                Directory.CreateDirectory(outputFilePath);
                entry.Input.SerializeMetadata().Save(Path.Combine(tempFolder, Package.INPUT_MANIFEST));

                // serialize Output package objects to disk before writing the output manifest to ensure sizes and hashes reflect
                // serialized data.
                foreach (var f in entry.Output.Files)
                {
                    var fs = new FileStream(Path.Combine(outputFilePath, f.FileName), FileMode.Create, FileAccess.Write);
                    f.SaveObject(fs);
                    fs.Close();
                }

                entry.Output.SerializeMetadata().Save(Path.Combine(tempFolder, Package.OUTPUT_MANIFEST));             

                Directory.Move(tempFolder, path);
                success = true;
            }
            catch(Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.ToString());
            }
            return success;
        }

    }
}
