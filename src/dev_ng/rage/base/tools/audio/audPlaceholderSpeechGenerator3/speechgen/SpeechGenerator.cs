﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Xml;
using NLog;
using Rockstar.AssetManager.Interfaces;
using RSG.Text.Model;
using speechgen.SpeechSynth;
using speechgen.WavesFileXmlParsing;
using Wavelib;

namespace speechgen
{
    public class SpeechGenerator
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private IAssetManager assetManager;
        private readonly ISpeechSynth speechSynth;

        private const int MaxFileDeleteAttempts = 10;
        private const int MaxSpeechGenAttemptsPerFile = 10;

        public SpeechGenerator(IAssetManager assetMgr, ISpeechSynth speechSynth)
        {
            this.assetManager = assetMgr;
            this.speechSynth = speechSynth;
        }

        public void Generate(
            WavesFilesRepository placeholderSpeechPackWavesFilesRepository,
            string placeholderSpeechBankfolderName, 
            WavesFilesRepository recordedSpeechPackWavesFilesRepository, 
            string recordedSpeechBankfolderName, 
            string projectName, 
            string dstarFilesRootPath,
            string characterVoiceConfigPath, 
            string outputPath, 
            DateTime lastBuildTime, 
            uint maxFileSize,
            bool excludePlaceholderConversation, 
            bool rebuild)
        {
            logger.Info("Generating wave files...");

            Pack builtRecordedSpeechPack = recordedSpeechPackWavesFilesRepository.GetReferenceBuiltContent();
            BankFolder builtRecordedSpeechBankFolder = builtRecordedSpeechPack == null ? null : builtRecordedSpeechPack.GetBankfolder(recordedSpeechBankfolderName);
            if (builtRecordedSpeechBankFolder == null) logger.Warn("Could not find built bank folder " + recordedSpeechBankfolderName);

            Pack builtPlaceholderSpeechPack = placeholderSpeechPackWavesFilesRepository.GetReferenceBuiltContent();
            if (builtPlaceholderSpeechPack == null) logger.Warn("Could not find built pack " + placeholderSpeechPackWavesFilesRepository.PackName);
            else if (!builtPlaceholderSpeechPack.IsPlaceholderSpeechPack) logger.Warn("Pack " + placeholderSpeechPackWavesFilesRepository.PackName + " has not been recognised as a name range pack (e.g. PS_AD). Generation of all banks enabled.");
            Pack pendingPlaceholderSpeechPack = new Pack(placeholderSpeechPackWavesFilesRepository.PackName, builtPlaceholderSpeechPack == null ? WaveListOperation.add : WaveListOperation.modify);
            
            BankFolder builtPlaceholderSpeechBankFolder = builtPlaceholderSpeechPack==null? null: builtPlaceholderSpeechPack.GetBankfolder(placeholderSpeechBankfolderName);
            if (builtPlaceholderSpeechBankFolder==null) logger.Warn("Could not find built bank folder " + placeholderSpeechBankfolderName);
            BankFolder pendingPlaceholderSpeechBankFolder = new BankFolder(placeholderSpeechBankfolderName, builtPlaceholderSpeechBankFolder ==null? WaveListOperation.add : WaveListOperation.modify);

            DialogueConfigurations configurations = new DialogueConfigurations();
            configurations.LoadConfigurations(characterVoiceConfigPath);
            
            List<LineToBuild> linesToBuild = new List<LineToBuild>();

            foreach (var dStarFile in Directory.GetFiles(dstarFilesRootPath, "*.dstar2", SearchOption.AllDirectories))
            {
                Dialogue dialogue = new Dialogue(XmlReader.Create(dStarFile), configurations);
                // Only want to include banks that belong in the current pending pack for this run
                if (!BankBelongsInPack(dialogue.Id, pendingPlaceholderSpeechPack)) continue;

                string placeholderSpeechBankName = dialogue.Id + "_PLACEHOLDER";
                Bank builtPlaceholderSpeechBank = builtPlaceholderSpeechBankFolder == null ? null : builtPlaceholderSpeechBankFolder.GetBank(placeholderSpeechBankName);
                Bank pendingPlaceholderSpeechBank = new Bank(placeholderSpeechBankName, builtPlaceholderSpeechBank == null ? WaveListOperation.add : WaveListOperation.modify);

                bool addPendingPlaceholderSpeechBank = false;
                
                //check if generation can be skipped
                if (builtPlaceholderSpeechBank != null && !HasUnbuiltLines(dialogue, lastBuildTime) && !rebuild)
                    continue;

                foreach (Conversation conversation in dialogue.Conversations)
                {
                    if(conversation.IsPlaceholder && excludePlaceholderConversation) continue;
                        
                    WaveFolder builtPlaceholderSpeechWaveFolder = builtPlaceholderSpeechBank == null? null: builtPlaceholderSpeechBank.GetWaveFolder(conversation.Root);
                    WaveFolder pendingPlaceholderSpeechWaveFolder = new WaveFolder(conversation.Root, builtPlaceholderSpeechWaveFolder==null? WaveListOperation.add:WaveListOperation.modify);

                    Dictionary<Guid, IList<ILine>> linesPerCharacter = TextModelHelper.getLinesPerCharacter(conversation);
                    bool addPendingWaveFolder = false;
                    foreach (KeyValuePair<Guid, IList<ILine>> characterLine in linesPerCharacter)
                    {
                        string characterName = configurations.GetCharacterName(characterLine.Key);
                        if (string.IsNullOrEmpty(characterName))
                        {
                            logger.Warn("Undefined character name: " + characterLine.Key + " referenced in conversation: " + dialogue.Id + ". Skipping character lines... ("+dStarFile+")");
                            continue;
                        }
                        string voiceName = TextModelHelper.GetVoiceName(characterLine.Key, configurations);
                        if (string.IsNullOrEmpty(voiceName))
                        {
                            logger.Warn("No voice name found for character " + characterName+ " referenced in conversation: " + dialogue.Id + ". Skipping character lines... ("+dStarFile+")");
                            continue;
                        }

                        WaveFolder builtCharacterWaveFolder = builtPlaceholderSpeechWaveFolder == null ? null : builtPlaceholderSpeechWaveFolder.GetWaveFolder(characterName);
                        WaveFolder pendingCharacterWaveFolder = new WaveFolder(characterName, builtCharacterWaveFolder == null ? WaveListOperation.add : WaveListOperation.modify);
                        
                        string tagName = string.Format("{0}_PLACEHOLDER", characterName);
                        pendingCharacterWaveFolder.Add(new Tag("voice", tagName,
                                                            (builtCharacterWaveFolder!=null && builtCharacterWaveFolder.GetTag("voice")!=null)? WaveListOperation.modify: WaveListOperation.add));

                        bool addPendingCharacterWavefolder = false;
                        foreach (ILine line in characterLine.Value)
                        {
                            if (string.IsNullOrEmpty(line.Dialogue)) continue;

                            Wave builtWave = builtCharacterWaveFolder==null? null: builtCharacterWaveFolder.GetWave(line.AudioFilepath+".WAV");
                            Wave pendingWave = new Wave(line.AudioFilepath+".WAV", builtWave==null? WaveListOperation.add : WaveListOperation.modify);

                            if (builtWave != null && line.LastModifiedTime < lastBuildTime && !rebuild) continue;

                            var lineToBuild = new LineToBuild
                            {
                                Speaker = voiceName,
                                Text = line.Dialogue.Replace("\"", ""),
                                FileName = Path.Combine(outputPath, placeholderSpeechPackWavesFilesRepository.PackName, placeholderSpeechBankfolderName, dialogue.Id+"_PLACEHOLDER", conversation.Root, characterName, line.AudioFilepath + ".WAV"),
                                MaxFileSize = maxFileSize
                            };
                            linesToBuild.Add(lineToBuild);
                            pendingCharacterWaveFolder.Add(pendingWave);
                            addPendingCharacterWavefolder = true;
                            addPendingWaveFolder = true;
                            addPendingPlaceholderSpeechBank = true;
                        }

                        if (addPendingCharacterWavefolder) pendingPlaceholderSpeechWaveFolder.Add(pendingCharacterWaveFolder);
                    }
                    if (addPendingWaveFolder) pendingPlaceholderSpeechBank.Add(pendingPlaceholderSpeechWaveFolder);
                }
                if (addPendingPlaceholderSpeechBank) pendingPlaceholderSpeechBankFolder.Add(pendingPlaceholderSpeechBank);

            }

            WavesFileCleanup cleaner = new WavesFileCleanup(configurations, dstarFilesRootPath);
            bool unusedElementsHaveBeenRemoved = cleaner.Cleanup(linesToBuild, excludePlaceholderConversation, builtPlaceholderSpeechBankFolder, pendingPlaceholderSpeechBankFolder, builtRecordedSpeechBankFolder);

            if (linesToBuild.Count == 0 && !unusedElementsHaveBeenRemoved) return;

            pendingPlaceholderSpeechPack.Add(pendingPlaceholderSpeechBankFolder);

            logger.Info(string.Format("processing pack {0}...", pendingPlaceholderSpeechPack.Id));

            // Create change list
            string description = string.Format(
                "{0} - Placeholder Speech Generated Assets for {1} pack - (All Platforms)",
                projectName,
                placeholderSpeechPackWavesFilesRepository.PackName);


            logger.Info("Create changelist...");
            IChangeList changeList = this.assetManager.CreateChangeList(description);
            if (changeList == null) throw new Exception("Could not create changelist \"" + description + "\"");
                
            try
            {
                //checkout assets, mark assests for delete, and mark assets for adding
                PopulateChangelist(outputPath, placeholderSpeechPackWavesFilesRepository, linesToBuild, pendingPlaceholderSpeechPack, changeList);

                // Generate wave files
                logger.Info(string.Format("Generating {0} wave file(s)...", linesToBuild.Count));
                GenerateWaveFiles(linesToBuild);

                logger.Info("Updating PendingWaves.xml files...");
                placeholderSpeechPackWavesFilesRepository.SetPendingWavesContent(pendingPlaceholderSpeechPack);

                // Revert any unchanged assets in change list
                logger.Info("Reverting unchanged files...");
                if (!changeList.RevertUnchanged())
                {
                    logger.Error("Reverting of unchanged files failed");
                }

                // Submit change list
                var fileCount = changeList.Assets.Count;
                if (fileCount > 0)
                {
                    logger.Info(string.Format("Submitting " + fileCount + " files..."));
                    if (!changeList.Submit())
                    {
                        throw new Exception("Failed to submit files.");
                    }
                }
                else
                {
                    logger.Info("Empty change list after reverting unchanged - nothing to submit");
                    changeList.Delete();
                }
            }
            catch (Exception ex)
            {
                changeList.Revert(true);
                throw;
            }
        }

        private void GenerateWaveFiles(List<LineToBuild> linesToBuild, int currentRetryCount=0)
        {
            // Generate wave files
            bool success = this.speechSynth.Synthesize(linesToBuild);

            if (!success) throw new Exception("Could not synthesize lines");

            // Validate processed lines
            var failedLines = this.ValidateProcessedLines(linesToBuild);
            var failedLinesCount = failedLines.Count;
            if (failedLinesCount > 0)
            {
                logger.Warn(failedLinesCount+" file(s) failed speech generation");
                if (++currentRetryCount <= MaxSpeechGenAttemptsPerFile)
                {
                    logger.Info("Retrying speech generation for "+failedLinesCount+" file(s)");
                    logger.Info("Waiting 10s for retry...");
                    Thread.Sleep(10000);

                    // Re-generate failed wave files
                    this.GenerateWaveFiles(failedLines, currentRetryCount);
                }
                else
                {
                    throw new Exception("Failed to succesfully process all lines to build after " + MaxSpeechGenAttemptsPerFile + " attempt(s)");
                }
            }
        }

        private List<LineToBuild> ValidateProcessedLines(List<LineToBuild> processedLines)
        {
            logger.Info(string.Format("Validating {0} generated wave file(s)...", processedLines.Count));
            List<LineToBuild> failedLines = new List<LineToBuild>();

            // This validation is required to ensure the files generated
            // by the speech generator exist and are valid wave files (the
            // process can be a little flakey and unrealiable).
            foreach (var line in processedLines)
            {
                string fileName = line.FileName;
                logger.Info("Validating file: " + fileName);

                // Check that file exists.
                if (!File.Exists(fileName))
                {
                    logger.Warn("File expected but not found - marking for re-run: {0}", fileName);
                    failedLines.Add(line);
                    continue;
                }

                bool isValid = true;

                try
                {
                    // Check for valid wave file.
                    // This step is necessary due to unreliable wave generation process.
                    var waveFile = new bwWaveFile(fileName, false);
                    var rawData = waveFile.Data.RawData;

                    // Invalid wave files have data chunk of size 0
                    if (rawData.Length == 0)
                    {
                        logger.Warn(
                            "Corrupt audio file detected (data size of 0) - marking for re-run: {0}", fileName);
                        failedLines.Add(line);
                        isValid = false;
                    }
                }
                catch (Exception)
                {
                    logger.Warn("Corrupt audio file detected - marking for re-run: {0}", fileName);
                    failedLines.Add(line);
                    isValid = false;
                }

                if (isValid) continue;

                // We allow several attempts to delete the corrupt file here as
                // there is sometimes still a process locking the file, which causes
                // the re-generation step to fail later on
                for (var i = 1; i <= MaxFileDeleteAttempts; i++)
                {
                    logger.Info("Deleting corrupt audio file (attempt {0}): {1}", i, fileName);
                    try
                    {
                        File.Delete(fileName);
                    }
                    catch (Exception)
                    {
                        logger.Error("Failed to delete corrupt audio file: {0}", fileName);
                        if (i < 10)
                        {
                            logger.Info(
                                "Waiting 10s to retry deletion of corrupt audio file: {0}", fileName);
                            Thread.Sleep(10 * 1000);
                        }
                        else
                        {
                            logger.Error("Failed to delete corrupt audio file after 10 attempts: {0}", fileName);
                        }
                    }
                }
            }

            logger.Info(string.Format("{0} generated wave file(s) failed validation", failedLines.Count));
            return failedLines;
        }


        private void PopulateChangelist(string outputPath, WavesFilesRepository wavesFilesRepository, List<LineToBuild> linesToBuild, Pack pendingPack, IChangeList changeList)
        {
            logger.Info("Checking out/adding pending waves files...");
            // Checkout and lock (or add to add list) pending wave files
            wavesFilesRepository.CheckoutOrAddPendingWavesFiles(changeList);

            // Checkout and lock (or add to add list) wave files to build
            foreach (var fileName in linesToBuild.Select(line => line.FileName))
            {
                if (this.assetManager.ExistsAsAsset(fileName))
                {
                    logger.Info("Checking out file: " + fileName);
                    changeList.CheckoutAsset(fileName, true);
                    if (!File.Exists(fileName))
                    {
                        logger.Error("Checked out asset but still does not exist locally: " + fileName);
                    }
                }
                else
                {
                    logger.Info("Marking file for add: " + fileName);
                    changeList.MarkAssetForAdd(fileName, "binary");
                }
            }

            //delete files
            foreach (var waveToDelete in pendingPack.getAllWavesPendingForDeletion(outputPath))
            {
                try
                {
                    if (this.assetManager.ExistsAsAsset(waveToDelete))
                    {
                        logger.Info("Marking file for delete: "+ waveToDelete);
                        changeList.MarkAssetForDelete(waveToDelete);
                    }
                }
                catch (Exception ex)
                {
                    logger.Warn("Failed to mark asset for delete: " + waveToDelete + ": " + ex.Message);
                }
            }

        }

        private static bool BankBelongsInPack(string bankId, Pack pack)
        {
            if (string.IsNullOrEmpty(bankId))
            {
                throw new Exception("Expected bank name to be able to determine if it belongs in pack");
            }

            var startChar = bankId.Substring(0, 1).ToUpper();
            return pack.AllowedBankNameStartLetters.Contains(startChar);
        }

        private bool HasUnbuiltLines(Dialogue dialogue, DateTime lastBuiltTime)
        {
            DateTime newestLineModification = DateTime.MinValue;
            foreach (Conversation conversation in dialogue.Conversations)
            {
                foreach (ILine line in conversation.Lines)
                {
                    if (line.LastModifiedTime > newestLineModification) newestLineModification = line.LastModifiedTime;
                }
            }
            return newestLineModification > lastBuiltTime;
        }
    }
}
