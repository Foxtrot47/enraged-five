﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using speechgen.Processing;

namespace speechgen.SpeechSynth
{
    public class ExternalProcessSpeechSynth : ISpeechSynth
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private const string FORMAT_ERROR_COMPRESSING_FILE = "Error compressing: {0}.";

        private const string FORMAT_COMPLETED_SPEECH_SYNTHESIS =
            "Completed speech synthesis. Processing took: {0} for {1} lines.";

        private const string STARTED_SPEECH_SYNTHESIS = "Started speech synthesis...";

        #region ISpeechSynth Members

        public bool Synthesize(IList<LineToBuild> linesToBuild)
        {
            logger.Info(STARTED_SPEECH_SYNTHESIS);

            var start = DateTime.Now;

            int numLinesToBuild = linesToBuild.Count;
            int numLinesBuilt = 0;
            bool showProgress = Environment.UserInteractive;
            string numLinesToBuildStr = numLinesToBuild.ToString().PadLeft(5);

            foreach (var lineToBuild in linesToBuild)
            {
                var directory = Path.GetDirectoryName(lineToBuild.FileName);
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                if (showProgress)
                {
                    string numLinesBuiltStr = numLinesBuilt.ToString().PadLeft(5);

                    logger.Info("Synthesized "+ numLinesBuiltStr+" / "+ numLinesToBuildStr+": "+ lineToBuild.Speaker+" "+ Path.GetFileNameWithoutExtension(lineToBuild.FileName));
                }


                Process buildProcess = new Process();
                String parameters = "-s " + lineToBuild.MaxFileSize;
                parameters += " -t \"" + lineToBuild.Text + "\"";
                parameters += " -f \"" + lineToBuild.FileName + "\"";
                parameters += " -v \"" + lineToBuild.Speaker + "\"";

                buildProcess.StartInfo = new ProcessStartInfo("TextToSpeech.exe", parameters);

                buildProcess.OutputDataReceived += new DataReceivedEventHandler(TextToSpeechOutputDataReceived);
                buildProcess.EnableRaisingEvents = true;
                buildProcess.StartInfo.CreateNoWindow = true;
                buildProcess.StartInfo.UseShellExecute = false;
                buildProcess.StartInfo.RedirectStandardOutput = true;
                buildProcess.StartInfo.RedirectStandardError = true;

                try
                {
                    buildProcess.Start();
                }
                catch (Exception ex)
                {
                    logger.Error("Exception occurred while starting the process (" + ex.Message + ")");
                    return false;
                }
                buildProcess.BeginOutputReadLine();
                buildProcess.WaitForExit();
                if (buildProcess.ExitCode != 0)
                {
                    logger.Error("TextToSpeech.exe exited with an error");
                    return false;
                }

                var postProcessor = new WaveCompressorProcessor(lineToBuild.FileName);
                if (!postProcessor.Process())
                {
                    logger.Error(FORMAT_ERROR_COMPRESSING_FILE, lineToBuild.FileName);
                    if (null != postProcessor.Error)
                    {
                        logger.Error(postProcessor.Error);
                    }

                    return false;
                }

                numLinesBuilt++;
            }

            var end = DateTime.Now;
            logger.Info(string.Format(FORMAT_COMPLETED_SPEECH_SYNTHESIS, end - start, linesToBuild.Count));
            return true;
        }

        void TextToSpeechOutputDataReceived(object o, DataReceivedEventArgs arg)
        {
            logger.Info(arg.Data);
        }

        #endregion
    }
}
