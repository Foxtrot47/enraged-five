﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using NLog.Config;
using NLog.Targets;
using rage.ToolLib.CmdLine;

namespace speechgen
{
    public class Configuration: CommandLineConfiguration
    {
        public Configuration(string[] args) : base(args) { }

        /// <summary>
        /// Gets a value indicating whether force rebuild.
        /// </summary>
        [Description("force all the lines to be generated")]
        [OptionalParameter]
        [DefaultValue(false)]
        public bool Rebuild { get; set; }

        private string logFile="";
        /// <summary>
        /// Gets the location of the logfile.
        /// </summary>
        [Description("specifies the path to the file used for logging")]
        [OptionalParameter]
        public string LogFile 
        { 
            get
            {
                return logFile;
            } 
            set
            {
                if (string.IsNullOrEmpty(value)) return;
                logFile = value;
                //add logfile as target to nlog
                FileTarget logTarget = new FileTarget { FileName = logFile };
                logTarget.Layout = "${longdate} ${uppercase:${level}} ${message}";
                LoggingRule loggingRule = new LoggingRule("*", LogLevel.Info, logTarget);
                LogManager.Configuration.AddTarget("logFile", logTarget);
                LogManager.Configuration.LoggingRules.Add(loggingRule);
                LogManager.Configuration.Reload();
            } 
        }

        /// <summary>
        /// Gets a value indicating whether help requested.
        /// </summary>
        [Description("display usage information")]
        [OptionalParameter]
        [DefaultValue(false)]
        public bool Help { get; set; }

        /// <summary>
        /// Gets the perforce client.
        /// </summary>
        [Description("the perforce client workspace")]
        public string P4Client { get; set; }

        /// <summary>
        /// Gets the perforce depot root.
        /// </summary>
        [Description("the depot root")]
        public string P4DepotRoot { get; set; }

        /// <summary>
        /// Gets the perforce host.
        /// </summary>
        [Description("the connection info for perforce ex. rsgedip4s1:1666")]
        public string P4Host { get; set; }

        /// <summary>
        /// Gets the perforce password.
        /// </summary>
        [Description("the perforce password")]
        [OptionalParameter]
        [DefaultValue("")]
        public string P4Passwd { get; set; }

        /// <summary>
        /// Gets the perforce username.
        /// </summary>
        [Description("the perforce user name")]
        public string P4User { get; set; }

        /// <summary>
        /// Gets the project settings.
        /// </summary>
        [Description("the path to the project settings file")]
        public string Project { get; set; }

        /// <summary>
        /// Gets the working path.
        /// </summary>
        [Description("the root directory for all relative paths")]
        public string WorkingPath { get; set; }

        /// <summary>
        /// Gets the working path.
        /// </summary>
        [Description("the temporary directory for building packs")]
        [OptionalParameter]
        [DefaultValue(@"D:\Temp")]
        public string TempPath { get; set; }

        /// <summary>
        /// Gets the databuilder path.
        /// </summary>
        [Description("the databuilder location")]
        public string DataBuilder { get; set; }

        /// <summary>
        /// The registry key.
        /// </summary>
        [IsNotParameter]
        public readonly string RegKey = "Software\\Rockstar Games\\RAGE Audio\\Placeholder Speech Generation\\";

        /// <summary>
        /// The registry key value.
        /// </summary>
        [IsNotParameter]
        public readonly string RegKeyValue = "LastBuildTime";
    }
}
