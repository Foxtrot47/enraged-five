﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace speechgen.SpeechSynth
{
    public interface ISpeechSynth
    {
        bool Synthesize(IList<LineToBuild> linesToBuild);
    }
}
