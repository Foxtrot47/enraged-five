﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;
using rage.ToolLib.Repository;

namespace speechgen.WavesFileXmlParsing
{
    public class BankFolder : IRepositoryItem<string>
    {
        private readonly IRepository<string, BankFolder> bankFolders = new Repository<string, BankFolder>();
        private readonly IRepository<string, Bank> banks = new Repository<string, Bank>();
        private readonly IRepository<string, Tag> tags = new Repository<string, Tag>();

        public BankFolder(XElement bankFolderElement)
        {
            Operation = WaveListOperation.none;
            Parse(bankFolderElement);
        }

        public BankFolder(string id, WaveListOperation operation)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("id");
            }
            if (operation == WaveListOperation.none)
            {
                throw new ArgumentException(
                    "operation: This constructor should only be used if specifying a valid operation");
            }
            Id = id;
            Operation = operation;
        }

        public string Id { get; private set; }

        public WaveListOperation Operation { get; private set; }

        protected void Parse(XElement root)
        {
            this.Id = ParseNameAttribute(root);
            this.Operation = ParseOperationAttribute(root);

            foreach (var child in root.Elements())
            {
                try
                {
                    var childName =
                        (WaveListParseTokens)Enum.Parse(typeof(WaveListParseTokens), child.Name.ToString());

                    switch (childName)
                    {
                        case WaveListParseTokens.Tag:
                            {
                                tags.Add(new Tag(child));
                                break;
                            }
                        case WaveListParseTokens.BankFolder:
                            {
                                bankFolders.Add(new BankFolder(child));
                                break;
                            }
                        case WaveListParseTokens.Bank:
                            {
                                banks.Add(new Bank(child));
                                break;
                            }
                        default:
                            {
                                throw new Exception("Illegal \"" + childName + "\" child element encountered.");
                            }
                    }
                }
                catch
                {
                    throw new Exception("Illegal \"" + child.Name + "\" child element encountered.");
                }
            }
        }

        public virtual XElement Serialize()
        {
            var element = new XElement("BankFolder");
            if (DoSerialization(element) ||
                Operation != WaveListOperation.none)
            {
                return element;
            }
            return null;
        }

        public IEnumerable<BankFolder> BankFolders
        {
            get { return bankFolders.Items; }
        }

        public bool TryGetBankFolder(string name, out BankFolder item)
        {
            return bankFolders.TryGetItem(name, out item);
        }

        public bool BankfolderExists(string name)
        {
            BankFolder folder;
            return bankFolders.TryGetItem(name, out folder);
        }

        public BankFolder GetBankfolder(string name)
        {
            BankFolder folder;
            bankFolders.TryGetItem(name, out folder);
            return folder;
        }

        public void Add(BankFolder item)
        {
            bankFolders.Add(item);
        }

        public void Remove(BankFolder item)
        {
            bankFolders.Remove(item);
        }

        public IEnumerable<Bank> Banks
        {
            get { return banks.Items; }
        }

        public bool TryGetBank(string name, out Bank item)
        {
            return banks.TryGetItem(name, out item);
        }

        public bool BankExists(string name)
        {
            Bank bank;
            return banks.TryGetItem(name, out bank);
        }

        public Bank GetBank(string name)
        {
            Bank bank;
            banks.TryGetItem(name, out bank);
            return bank;
        }

        public void Add(Bank item)
        {
            banks.Add(item);
        }

        public void Remove(Bank item)
        {
            banks.Remove(item);
        }

        public IEnumerable<Tag> Tags
        {
            get { return tags.Items; }
        }

        public bool TryGetTag(string name, out Tag item)
        {
            return tags.TryGetItem(name, out item);
        }

        public void Add(Tag item)
        {
            tags.Add(item);
        }

        public void Remove(Tag item)
        {
            tags.Remove(item);
        }

        protected bool DoSerialization(XElement element)
        {
            if (Operation != WaveListOperation.none)
            {
                element.SetAttributeValue("operation", Operation.ToString());
            }
            element.SetAttributeValue("name", Id);

            var childAdded = false;
            foreach (var tag in tags.Items)
            {
                var child = tag.Serialize();
                if (child != null)
                {
                    childAdded = true;
                    element.Add(child);
                }
            }

            foreach (var bankFolder in bankFolders.Items)
            {
                var child = bankFolder.Serialize();
                if (child != null)
                {
                    childAdded = true;
                    element.Add(child);
                }
            }

            foreach (var bank in banks.Items)
            {
                var child = bank.Serialize();
                if (child != null)
                {
                    childAdded = true;
                    element.Add(child);
                }
            }
            return childAdded;
        }

        private string ParseNameAttribute(XElement root)
        {
            var nameAttrib = root.Attribute("name");
            if (nameAttrib != null &&
                !string.IsNullOrEmpty(nameAttrib.Value))
            {
                return nameAttrib.Value;
            }
            else
            {
                throw new Exception(root.Name+" element missing \"name\" attribute.");
            }
        }

        private WaveListOperation ParseOperationAttribute(XElement root)
        {
            var operationAttrib = root.Attribute("operation");
            if (operationAttrib != null &&
                !string.IsNullOrEmpty(operationAttrib.Value))
            {
                try
                {
                    return (WaveListOperation)Enum.Parse(typeof(WaveListOperation), operationAttrib.Value, true);
                }
                catch
                {
                    throw new Exception("Unrecognized operation \""+operationAttrib.Value+"\".");
                }
            }
            return WaveListOperation.none;
        }

        public IEnumerable<string> getAllWavesPendingForDeletion(string outputPath)
        {
            var removedWaves = new List<string>();
            foreach (var bankFolder in this.BankFolders)
            {
                removedWaves.AddRange(bankFolder.getAllWavesPendingForDeletion(Path.Combine(outputPath, this.Id)));
            }

            foreach (var bank in this.Banks)
            {
                removedWaves.AddRange(bank.getAllWavesPendingForDeletion(Path.Combine(outputPath, this.Id)));
            }
            return removedWaves;
        }
    }
}
