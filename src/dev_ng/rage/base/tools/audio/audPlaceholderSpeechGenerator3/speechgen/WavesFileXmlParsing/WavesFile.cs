﻿using System;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using rage.ToolLib.Repository;

namespace speechgen.WavesFileXmlParsing
{
    public class WavesFile : Repository<string, Pack>
    {
        protected XDocument document;


        public WavesFile(string fileName, string packContainerElementName = null)
        {
            Load(fileName, packContainerElementName);
        }

        public bool IsLoaded { get; private set; }

        private void Load(string file, string packContainerElementName)
        {
            Unload();

            if (string.IsNullOrEmpty(file) ||
                !File.Exists(file))
            {
                throw new Exception("Invalid file name.");
            }

            document = XDocument.Load(file);
            if (document.Root == null ||
                (!string.IsNullOrEmpty(packContainerElementName) && document.Root.Name != packContainerElementName))
            {
                throw new Exception("The file provided does not have the expected \"" + packContainerElementName + "\" root element.");
            }

            if(!string.IsNullOrEmpty(packContainerElementName)) AddChildPacks(document.Root);
            else Add(new Pack(document.Root));

            IsLoaded = true;
        }

        private void Unload()
        {
            IsLoaded = false;
            m_itemLookup.Clear();
        }

        public void Save(string file)
        {
            if (!IsLoaded)
            {
                throw new Exception("A file must be loaded before you can save it.");
            }

            if (string.IsNullOrEmpty(file))
            {
                throw new Exception("Invalid file name.");
            }

            foreach (var item in Items)
            {
                var pack = item.Serialize();
                if (pack != null)
                {
                    document.Root.Add(pack);
                }
            }
            document.Save(file);
        }

        private void AddChildPacks(XContainer root)
        {
            foreach (var packElement in root.Elements())
            {
                Add(new Pack(packElement));
            }
        }

        public bool PackExists(string packName)
        {
            return this.m_itemLookup.ContainsKey(packName);
        }

        public Pack GetPack(string packName)
        {
            Pack result = null;
            this.TryGetItem(packName, out result);
            return result;
        }

    }
}
