﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using rage;
using rage.ToolLib.WavesFiles;
using Rockstar.AssetManager.Interfaces;

namespace speechgen.WavesFileXmlParsing
{
    public class WavesFilesRepository
    {
        private IAssetManager assetManager;
        private Dictionary<string, WavesFile> pendingWavesPerPlatformLookup;
        private WavesFile referenceBuiltWaves;

        public string PackName { get; private set; }

        public WavesFilesRepository(ArrayList platforms, string workingPath, IAssetManager assetManager, string packName)
        {
            this.assetManager = assetManager;
            this.PackName = packName;

            string referenceBuiltWavesFile = null;
            List<string> pendingWavesFiles = new List<string>();

            string pendingWaves = string.Format("PendingWaves\\{0}.xml", PackName);
            foreach (PlatformSetting platformSetting in platforms)
            {
                if (!platformSetting.IsActive) continue;

                string buildPath = string.Concat(workingPath, platformSetting.BuildInfo);
                if (string.IsNullOrEmpty(referenceBuiltWavesFile))
                {
                    referenceBuiltWavesFile = Path.Combine(buildPath, "BuiltWaves\\" + packName + "_PACK_FILE.XML");
                }
                pendingWavesFiles.Add(Path.Combine(buildPath, pendingWaves));
            }

            pendingWavesPerPlatformLookup = LoadPendingWaves(pendingWavesFiles);
            referenceBuiltWaves = LoadBuiltWaves(referenceBuiltWavesFile);
        }

        private Dictionary<string, WavesFile> LoadPendingWaves(List<string> pendingWavesFiles)
        {
            Dictionary<string, WavesFile> pendingWavesLookup = new Dictionary<string, WavesFile>();

            foreach (string pendingWavesFile in pendingWavesFiles)
            {
                this.assetManager.GetLatest(string.Concat(Path.GetDirectoryName(pendingWavesFile), "\\"), false);
                //create file
                if (!this.assetManager.ExistsAsAsset(pendingWavesFile))
                {
                    if (File.Exists(pendingWavesFile))
                    {
                        var attribs = File.GetAttributes(pendingWavesFile);
                        if ((attribs & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                        {
                            File.SetAttributes(pendingWavesFile, FileAttributes.Normal);
                        }
                    }

                    var doc = new XDocument(new XElement("PendingWaves"));
                    doc.Save(pendingWavesFile);
                }
                WavesFile pendingWaves = new WavesFile(pendingWavesFile, "PendingWaves");
                if (pendingWaves.IsLoaded && pendingWaves.Items.Any()) throw new Exception("There are pending waves for pack: \"" + PackName + "\". Run this tool again after they have been built.");

                pendingWavesLookup.Add(pendingWavesFile, pendingWaves);
            }

            return pendingWavesLookup;
        }

        private WavesFile LoadBuiltWaves(string referenceBuiltWavesFile)
        {
            var directory = Path.GetDirectoryName(referenceBuiltWavesFile) + "\\";
            this.assetManager.GetLatest(directory, false);
            WavesFile builtWaves = new WavesFile(referenceBuiltWavesFile);
            return builtWaves;
        }

        public void SetPendingWavesContent(Pack pendingPack)
        {
            foreach (var pendingWavesEntry in pendingWavesPerPlatformLookup)
            {
                pendingWavesEntry.Value.Add(pendingPack);
                pendingWavesEntry.Value.Save(pendingWavesEntry.Key);
            }
        }

        public void CheckoutOrAddPendingWavesFiles(IChangeList changeList)
        {
            foreach (var pendingWavesFile in pendingWavesPerPlatformLookup.Keys)
            {
                if (this.assetManager.ExistsAsAsset(pendingWavesFile))
                {
                    changeList.CheckoutAsset(pendingWavesFile, true);
                }
                else
                {
                    changeList.MarkAssetForAdd(pendingWavesFile);
                }
            }
        }

        public Pack GetReferenceBuiltContent()
        {
            return referenceBuiltWaves.GetPack(PackName);
        }

    }
}
