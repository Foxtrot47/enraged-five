﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace speechgen.Processing
{
    public interface ITextToSpeechProcessor
    {
        Exception Error { get; }

        bool Process(string speechText, out string output);
    }
}
