﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace speechgen.WavesFileXmlParsing
{
    public enum WaveListParseTokens
    {
        PendingWaves,
        BuiltWaves,
        Tag,
        Pack,
        BankFolder,
        Bank,
        WaveFolder,
        Wave,
        Chunk,
    }
}
