﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace speechgen.Processing
{
    public class WaveCompressorProcessor
    {
        /// <summary>
        /// The SoX EXE path.
        /// </summary>
        private const string SoxExePath = @"sox.exe";

        /// <summary>
        /// The wave path.
        /// </summary>
        private readonly string wavePath;

        /// <summary>
        /// Initializes a new instance of the <see cref="WaveCompressorProcessor"/> class.
        /// </summary>
        /// <param name="wavePath">
        /// The path of the wave to be processed.
        /// </param>
        public WaveCompressorProcessor(string wavePath)
        {
            this.wavePath = wavePath;
        }

        /// <summary>
        /// Gets the error.
        /// </summary>
        public Exception Error { get; private set; }

        /// <summary>
        /// Process the wave.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public bool Process()
        {
            this.Error = null;

            // Check for existing input WAV file
            if (string.IsNullOrEmpty(this.wavePath))
            {
                this.Error = new Exception("Input WAV file not specified");
                return false;
            }

            // Check for valid file name
            var ext = Path.GetExtension(this.wavePath);
            if (string.IsNullOrEmpty(ext) || ext.ToLower() != ".wav")
            {
                this.Error = new Exception("Input file not of type WAV");
                return false;
            }

            // GetTempFileName creates a .tmp file, but we need a .wav
            // so create both here, and delete both after use
            var tmpOutputPath = Path.GetTempFileName();
            var tempOutputWavePath = tmpOutputPath + ".wav";

            var args = new List<string>
            {
                this.wavePath,
                tempOutputWavePath,
                "compand", // Compand the dynamic range of the audio
                "0.0,0.1", // Attack and decay params (secs)
                "6:-70,-35,-6", // Maximum possible signal amplitude
                "0", // Additional gain applied at all points on the transfer function (dB)
                "-6", // Initial level to be assumed for each channel (dB)
                "0.0", // Delay between signal being analysed and fed to volume adjuster (secs)
            };

            // Run wave compression
            if (!this.ProcessWave(args))
            {
                return false;
            }

            // Replace original wave file with compressed version
            try
            {
                // Mark sure file is writable
                new FileInfo(this.wavePath) { IsReadOnly = false };
                File.Copy(tempOutputWavePath, this.wavePath, true);

                // Delete temp file
                File.Delete(tmpOutputPath);
                File.Delete(tempOutputWavePath);
            }
            catch (Exception ex)
            {
                this.Error = ex;
                return false;
            }

            return true;
        }

        /// <summary>
        /// Compress the wave.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool ProcessWave(IEnumerable<string> args)
        {
            // Start the external SoX process
            try
            {
                var process = new Process();
                var startInfo = new ProcessStartInfo
                {
                    FileName = SoxExePath,
                    Arguments = string.Join(" ", args),
                    WindowStyle = ProcessWindowStyle.Hidden,
                    CreateNoWindow = true,
                    UseShellExecute = false,
                };

                process.StartInfo = startInfo;
                process.Start();
                process.WaitForExit();

                if (process.ExitCode != 0)
                {
                    this.Error = new Exception("SoX process failed with exit code: " + process.ExitCode);
                    return false;
                }
            }
            catch (Exception ex)
            {
                this.Error = ex;
                return false;
            }

            return true;
        }
    }
}
