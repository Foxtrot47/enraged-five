﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Speech.Synthesis;
using System.Text;
using System.Threading.Tasks;

namespace speechgen.Processing
{
    public class TextToSpeechSpeakerProcessor : ITextToSpeechProcessor
    {
        private const string FORMAT_SSML =
            "<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='en'> {0} </speak>";

        private readonly string m_voiceToUse;

        public TextToSpeechSpeakerProcessor(string voiceToUse)
        {
            if (voiceToUse == null)
            {
                throw new ArgumentNullException("voiceToUse");
            }
            m_voiceToUse = voiceToUse;
        }

        public Exception Error { get; private set; }

        public bool Process(string speechText, out string output)
        {
            output = null;
            Error = null;

            if (speechText == null)
            {
                Error = new ArgumentNullException("speechText");
                return false;
            }

            try
            {
                using (var synth = new SpeechSynthesizer())
                {
                    synth.SelectVoice(m_voiceToUse);
                    synth.SpeakSsml(string.Format(FORMAT_SSML, speechText));
                }
                return true;
            }
            catch (Exception ex)
            {
                Error = ex;
                return false;
            }
        }
    }
}
