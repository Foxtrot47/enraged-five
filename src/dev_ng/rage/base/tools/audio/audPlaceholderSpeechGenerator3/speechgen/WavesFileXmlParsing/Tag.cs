﻿using System;
using System.Xml.Linq;
using rage.ToolLib.Repository;

namespace speechgen.WavesFileXmlParsing
{
    public class Tag : IRepositoryItem<string>
    {

        public Tag(XElement tagElement)
        {
            Parse(tagElement);
        }

        public Tag(string id, string value, WaveListOperation operation)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("id");
            }
            Id = id;
            Value = value;
            Operation = operation;
        }

        public string Id { get; private set; }
        public string Value { get; private set; }
        public WaveListOperation Operation { get; private set; }

        private void Parse(XElement root)
        {
            if (root.Name !=
                WaveListParseTokens.Tag.ToString())
            {
                throw new Exception("This object cannot parse a \"" + root.Name + "\" element.");
            }

            var nameAttrib = root.Attribute("name");
            if (nameAttrib != null &&
                !string.IsNullOrEmpty(nameAttrib.Value))
            {
                Id = nameAttrib.Value;
            }
            else
            {
                throw new Exception("Tag element missing \"name\" attribute.");
            }

            var valueAttrib = root.Attribute("value");
            if (valueAttrib != null &&
                !string.IsNullOrEmpty(valueAttrib.Value))
            {
                Value = valueAttrib.Value;
            }
            else
            {
                Value = null;
            }
        }

        public XElement Serialize()
        {
            var hasValidOperation = Operation != WaveListOperation.none;
            if (hasValidOperation)
            {
                var element = new XElement(WaveListParseTokens.Tag.ToString());
                element.SetAttributeValue("operation", Operation.ToString());
                element.SetAttributeValue("name", Id);
                if (!string.IsNullOrEmpty(Value))
                {
                    element.SetAttributeValue("value", Value);
                }
                return element;
            }
            return null;
        }
    }
}
