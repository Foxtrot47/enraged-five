﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using Microsoft.Win32;
using NLog;
using NLog.Config;
using NLog.Targets;
using rage;
using rage.ToolLib.CmdLine;
using Rockstar.AssetManager.Infrastructure.Enums;
using Rockstar.AssetManager.Interfaces;
using Rockstar.AssetManager.Main;
using speechgen.SpeechSynth;
using speechgen.WavesFileXmlParsing;

namespace speechgen
{
    internal class Program
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public static int Main(string[] args)
        {
            Configuration config;
            try
            {
                config = new Configuration(args);
            }
            catch (rage.ToolLib.CmdLine.CommandLineConfiguration.ParameterException ex) 
            {
                logger.Error(ex.Message);
                logger.Info(ex.Usage);
                return -1;
            }
            if (config.Help)
            {
                logger.Info(config.GetUsage());
                return 0;
            }

            logger.Info("SpeechGen v" + Assembly.GetEntryAssembly().GetName().Version);
            logger.Info("With arguments: " + String.Join(",", args));

            try
            {

                DateTime currentBuildTime = DateTime.Now;
                RegistryKey speechGeneratorRegistryKey = Registry.LocalMachine.OpenSubKey(config.RegKey, true)
                                  ?? Registry.LocalMachine.CreateSubKey(config.RegKey);
                DateTime lastBuildTime =
                    DateTime.Parse((string) speechGeneratorRegistryKey.GetValue(config.RegKeyValue, DateTime.MinValue.ToString()));
                logger.Info("Last successful build: " + lastBuildTime);

                BuildPlaceholderSpeechPacks(config);
                GenerateSpeech(config, lastBuildTime);

                speechGeneratorRegistryKey.SetValue(config.RegKeyValue, currentBuildTime, RegistryValueKind.String);
                speechGeneratorRegistryKey.Close();
            }
            catch(Exception ex)
            {
                logger.Error(ex);
                return -1;
            }
            return 0;
        }


        private static void BuildPlaceholderSpeechPacks(Configuration config)
        {
            audProjectSettings projectSettings = new audProjectSettings(Path.Combine(config.WorkingPath, config.Project));

            String audioDataBuilderDefaultParam = " -project " + config.Project + " -temppath " +
                                                      config.TempPath + " -p4host " + config.P4Host + " -p4client " +
                                                      config.P4Client + " -p4user " + config.P4User +
                                                  " -p4passwd " + config.P4Passwd + " -p4depotroot " +
                                                  config.P4DepotRoot;

            foreach (audPlaceholderSpeechGeneration speechGeneration in projectSettings.PlaceholderSpeechGenerationSettings)
            {
                logger.Info("Build packs for {0}:", speechGeneration.Name);
                foreach (audPlaceholderSpeechPack placeholderSpeechPack in speechGeneration.PlaceholderSpeechPacks)
                {
                    String audioDataBuilderParam = audioDataBuilderDefaultParam + " -pack " + placeholderSpeechPack.Name;

                    logger.Info("calling {0}", config.DataBuilder + " " + audioDataBuilderParam);
                    ProcessStartInfo p = new ProcessStartInfo(config.DataBuilder, audioDataBuilderParam);
                    p.WorkingDirectory = config.DataBuilder.Substring(0, config.DataBuilder.LastIndexOf(@"\"));
                    Process databuilder = Process.Start(p);
                    databuilder.WaitForExit();
                    if (databuilder.ExitCode != 0)
                    {
                        throw new Exception("Cound not build packs for " + speechGeneration.Name + ". Exit code was " + databuilder.ExitCode);
                    }
                }
            }
        }

        private static void GenerateSpeech(Configuration config, DateTime lastBuildTime)
        {
            audProjectSettings projectSettings = new audProjectSettings(Path.Combine(config.WorkingPath, config.Project));
            IAssetManager assetMgr = AssetManagerFactory.GetInstance(AssetManagerType.Perforce,
                        null,
                        config.P4Host,
                        config.P4Client,
                        config.P4User,
                        config.P4Passwd,
                        config.P4DepotRoot,
                        null);

            if (assetMgr == null) throw new Exception("Failed to create asset manager to connecto to Perforce");

            foreach (audPlaceholderSpeechGeneration speechGeneration in projectSettings.PlaceholderSpeechGenerationSettings)
            {
                logger.Info("Placeholder speech generation for {0}", speechGeneration.Name);
                foreach (audPlaceholderSpeechPack placeholderSpeechPack in speechGeneration.PlaceholderSpeechPacks)
                {
                    WavesFilesRepository placeholderSpeechPackWavesFilesRepository = new WavesFilesRepository(projectSettings.GetPlatformSettings(), config.WorkingPath, assetMgr, placeholderSpeechPack.Name);
                    WavesFilesRepository recordedSpeechPackWavesFilesRepository = placeholderSpeechPack.Name.ToUpper().Equals(placeholderSpeechPack.AssociatedSpeechPack.ToUpper()) ? placeholderSpeechPackWavesFilesRepository : new WavesFilesRepository(projectSettings.GetPlatformSettings(), config.WorkingPath, assetMgr, placeholderSpeechPack.AssociatedSpeechPack);

                    string dialoguePath = assetMgr.GetLocalPath(Path.Combine(config.WorkingPath, speechGeneration.DialoguePath));
                    string characterVoiceConfigPath = assetMgr.GetLocalPath(Path.Combine(config.WorkingPath, speechGeneration.CharacterVoiceConfig));
                    string outputPath = assetMgr.GetLocalPath(Path.Combine(config.WorkingPath, projectSettings.GetWaveInputPath()));

                    assetMgr.GetLatest(characterVoiceConfigPath, false);
                    assetMgr.GetLatest(dialoguePath, false);

                    SpeechGenerator speechGenerator = new SpeechGenerator(assetMgr, new ExternalProcessSpeechSynth());

                    speechGenerator.Generate(
                        placeholderSpeechPackWavesFilesRepository,
                        placeholderSpeechPack.BankFolder,
                        recordedSpeechPackWavesFilesRepository,
                        placeholderSpeechPack.AssociatedSpeechBankfolder,
                        projectSettings.GetProjectName(),
                        dialoguePath,
                        characterVoiceConfigPath,
                        outputPath,
                        lastBuildTime,
                        placeholderSpeechPack.MaxFileSize,
                        speechGeneration.ExcludePlaceholderConversation,
                        config.Rebuild
                        );
                }
            }
        }

    }
}
