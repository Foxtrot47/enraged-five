﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using rage.ToolLib.Repository;

namespace speechgen.WavesFileXmlParsing
{
    public class WaveFolder : IRepositoryItem<string>
    {
        private readonly IRepository<string, Tag> tags = new Repository<string, Tag>();
        private readonly IRepository<string, WaveFolder> waveFolders = new Repository<string, WaveFolder>();
        private readonly IRepository<string, Wave> waves = new Repository<string, Wave>();

        public WaveFolder(XElement waveFolderElement)
        {
            Operation = WaveListOperation.none;
            Parse(waveFolderElement);
        }

        public WaveFolder(string id, WaveListOperation operation)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("id");
            }
            if (operation == WaveListOperation.none)
            {
                throw new ArgumentException(
                    "operation: This constructor should only be used if specifying a valid operation");
            }
            Id = id;
            Operation = operation;
        }

        public string Id { get; private set; }

        public WaveListOperation Operation { get; private set; }

        protected void Parse(XElement root)
        {
            Id = ParseNameAttribute(root);
            Operation = ParseOperationAttribute(root);

            foreach (var child in root.Elements())
            {
                try
                {
                    var childName =
                        (WaveListParseTokens)Enum.Parse(typeof(WaveListParseTokens), child.Name.ToString());

                    switch (childName)
                    {
                        case WaveListParseTokens.Tag:
                            {
                                tags.Add(new Tag(child));
                                break;
                            }
                        case WaveListParseTokens.WaveFolder:
                            {
                                waveFolders.Add(new WaveFolder(child));
                                break;
                            }
                        case WaveListParseTokens.Wave:
                            {
                                waves.Add(new Wave(child));
                                break;
                            }
                    }
                }
                catch
                {
                    throw new Exception("Illegal \"" + child.Name + "\" child element encountered.");
                }
            }
        }

        public virtual XElement Serialize()
        {
            var element = new XElement("WaveFolder");
            if (DoSerialization(element) ||
                Operation != WaveListOperation.none)
            {
                return element;
            }
            return null;
        }

        public IEnumerable<WaveFolder> WaveFolders
        {
            get { return waveFolders.Items; }
        }

        public bool TryGetWaveFolder(string name, out WaveFolder waveFolder)
        {
            return waveFolders.TryGetItem(name, out waveFolder);
        }

        public WaveFolder GetWaveFolder(string name)
        {
            WaveFolder waveFolder;
            waveFolders.TryGetItem(name, out waveFolder);
            return waveFolder;
        }

        public void Add(WaveFolder waveFolder)
        {
            waveFolders.Add(waveFolder);
        }

        public void Remove(WaveFolder waveFolder)
        {
            waveFolders.Remove(waveFolder);
        }

        public IEnumerable<Wave> Waves
        {
            get { return waves.Items; }
        }

        public bool TryGetWave(string name, out Wave wave)
        {
            return waves.TryGetItem(name, out wave);
        }

        public Wave GetWave(string name)
        {
            Wave wave;
            waves.TryGetItem(name, out wave);
            return wave;
        }

        public void Add(Wave wave)
        {
            waves.Add(wave);
        }

        public void Remove(Wave wave)
        {
            waves.Remove(wave);
        }

        public IEnumerable<Tag> Tags
        {
            get { return tags.Items; }
        }

        public bool TryGetTag(string name, out Tag tag)
        {
            return tags.TryGetItem(name, out tag);
        }

        public Tag GetTag(string name)
        {
            Tag tag;
            tags.TryGetItem(name, out tag);
            return tag;
        }

        public void Add(Tag tag)
        {
            tags.Add(tag);
        }

        public void Remove(Tag tag)
        {
            tags.Remove(tag);
        }

        protected bool DoSerialization(XElement element)
        {
            if (Operation != WaveListOperation.none)
            {
                element.SetAttributeValue("operation", Operation.ToString());
            }
            element.SetAttributeValue("name", Id);

            var childAdded = false;
            foreach (var tag in tags.Items)
            {
                var child = tag.Serialize();
                if (child != null)
                {
                    childAdded = true;
                    element.Add(child);
                }
            }

            foreach (var waveFolder in waveFolders.Items)
            {
                var child = waveFolder.Serialize();
                if (child != null)
                {
                    childAdded = true;
                    element.Add(child);
                }
            }

            foreach (var wave in waves.Items)
            {
                var child = wave.Serialize();
                if (child != null)
                {
                    childAdded = true;
                    element.Add(child);
                }
            }
            return childAdded;
        }

        private string ParseNameAttribute(XElement root)
        {
            var nameAttrib = root.Attribute("name");
            if (nameAttrib != null &&
                !string.IsNullOrEmpty(nameAttrib.Value))
            {
                return nameAttrib.Value;
            }
            else
            {
                throw new Exception(root.Name+" element missing \"name\" attribute.");
            }
        }

        private WaveListOperation ParseOperationAttribute(XElement root)
        {
            var operationAttrib = root.Attribute("operation");
            if (operationAttrib != null &&
                !string.IsNullOrEmpty(operationAttrib.Value))
            {
                try
                {
                    return (WaveListOperation)Enum.Parse(typeof(WaveListOperation), operationAttrib.Value, true);
                }
                catch
                {
                    throw new Exception("Unrecognized operation \""+operationAttrib.Value+"\".");
                }
            }
            return WaveListOperation.none;
        }

        public IEnumerable<string> getAllWavesPendingForDeletion(string outputPath)
        {
            var removedWaves = new List<string>(from wave in this.Waves
                                 where wave.Operation == WaveListOperation.remove
                                 select Path.Combine(outputPath, wave.Id));
            foreach (WaveFolder waveFolder in this.WaveFolders)
            {
                removedWaves.AddRange(waveFolder.getAllWavesPendingForDeletion(Path.Combine(outputPath, this.Id)));
            }
            return removedWaves;
        }
    }
}
