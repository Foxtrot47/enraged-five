﻿using System;
using System.Xml.Linq;
using rage.ToolLib.Repository;

namespace speechgen.WavesFileXmlParsing
{
    public class Wave : IRepositoryItem<string>
    {

        public Wave(XElement waveElement)
        {
            Operation = WaveListOperation.none;
            Parse(waveElement);
        }

        public Wave(string id, WaveListOperation operation)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("id");
            }
            if (operation == WaveListOperation.none)
            {
                throw new ArgumentException(
                    "operation: This constructor should only be used if specifying a valid operation");
            }
            Id = id;
            Operation = operation;
        }

        public string Id { get; private set; }

        public WaveListOperation Operation { get; private set; }

        private void Parse(XElement root)
        {
            this.Id = ParseNameAttribute(root);
        }

        public XElement Serialize()
        {
            var hasValidOperation = Operation != WaveListOperation.none;
            if (hasValidOperation)
            {
                var element = new XElement("Wave");
                element.SetAttributeValue("operation", Operation.ToString());
                element.SetAttributeValue("name", Id);
                return element;
            }
            return null;
        }

        private string ParseNameAttribute(XElement root)
        {
            var nameAttrib = root.Attribute("name");
            if (nameAttrib == null || string.IsNullOrEmpty(nameAttrib.Value))
            {
                throw new Exception(root.Name + " element missing \"name\" attribute.");
            }
            return nameAttrib.Value;
        }
    }
}
