﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using speechgen.Processing;

namespace speechgen.SpeechSynth
{
    public class SingleThreadSpeechSynth : ISpeechSynth
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private const string FORMAT_ERROR_GENERATING_FILE = "Error generating: {0}.";

        private const string FORMAT_ERROR_COMPRESSING_FILE = "Error compressing: {0}.";

        private const string FORMAT_COMPLETED_SPEECH_SYNTHESIS =
            "Completed speech synthesis. Processing took: {0} for {1} lines.";

        private const string STARTED_SPEECH_SYNTHESIS = "Started speech synthesis...";

        #region ISpeechSynth Members

        public bool Synthesize(IList<LineToBuild> linesToBuild)
        {
            logger.Info(STARTED_SPEECH_SYNTHESIS);

            var start = DateTime.Now;

            int numLinesToBuild = linesToBuild.Count;
            int numLinesBuilt = 0;
            bool showProgress = Environment.UserInteractive;
            string numLinesToBuildStr = numLinesToBuild.ToString().PadLeft(5);

            foreach (var lineToBuild in linesToBuild)
            {
                var directory = Path.GetDirectoryName(lineToBuild.FileName);
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                if (showProgress)
                {
                    string numLinesBuiltStr = numLinesBuilt.ToString().PadLeft(5);
                    int percentComplete = (numLinesBuilt * 100) / numLinesToBuild;
                    string completionStr = percentComplete.ToString().PadLeft(3);

                    Console.SetCursorPosition(0, Console.CursorTop);
                    Console.Write("Synthesized {0} / {1}: {2}% complete - {3} {4}", numLinesBuiltStr, numLinesToBuildStr, completionStr, lineToBuild.Speaker, Path.GetFileNameWithoutExtension(lineToBuild.FileName));
                }

                var processor = new TextToSpeechFileSizeLimitedProcessor(lineToBuild.Speaker,
                                                                         lineToBuild.FileName,
                                                                         lineToBuild.MaxFileSize);
                string output;
                if (!processor.Process(lineToBuild.Text, out output))
                {
                    logger.Error(FORMAT_ERROR_GENERATING_FILE, lineToBuild.FileName);
                    if (processor.Error != null)
                    {
                        logger.Error(processor.Error);
                    }
                    return false;
                }

                if (!string.IsNullOrEmpty(output))
                {
                    logger.Warn(output);
                }

                var postProcessor = new WaveCompressorProcessor(lineToBuild.FileName);
                if (!postProcessor.Process())
                {
                    logger.Error(FORMAT_ERROR_COMPRESSING_FILE, lineToBuild.FileName);
                    if (null != postProcessor.Error)
                    {
                        logger.Error(postProcessor.Error);
                    }

                    return false;
                }

                numLinesBuilt++;
            }

            var end = DateTime.Now;
            logger.Info(FORMAT_COMPLETED_SPEECH_SYNTHESIS, end - start, linesToBuild.Count);
            return true;
        }

        #endregion
    }
}
