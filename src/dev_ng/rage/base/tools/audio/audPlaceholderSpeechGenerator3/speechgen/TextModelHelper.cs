﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Text.Model;

namespace speechgen
{
    public class TextModelHelper
    {
        public static Dictionary<Guid, IList<ILine>> getLinesPerCharacter(Conversation conversation)
        {
            Dictionary<Guid, IList<ILine>> speakerLines = new Dictionary<Guid, IList<ILine>>();

            foreach (ILine line in conversation.Lines)
            {
                if(!speakerLines.ContainsKey(line.CharacterId)) speakerLines.Add(line.CharacterId, new List<ILine>());
                speakerLines[line.CharacterId].Add(line);
            }
            return speakerLines;
        }

        public static string GetVoiceName(Guid characterId, DialogueConfigurations configurations)
        {
            DialogueCharacter dc = configurations.GetCharacter(characterId);
            if (dc != null)
            {
                foreach (DialoguePlaceholderVoice voice in configurations.PlaceholderVoices)
                {
                    if (voice.Id.Equals(dc.Voice)) return voice.Name;
                }
            }
            return null;
        }
    }
}
