﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Speech.Synthesis;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace speechgen.Processing
{
    public class TextToSpeechFileProcessor : ITextToSpeechProcessor
    {
        private const string FORMAT_SSML =
            "<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='en'> {0} </speak>";

        protected readonly string m_fileName;
        private readonly string m_voiceToUse;

        public TextToSpeechFileProcessor(string voiceToUse, string fileName)
        {
            if (string.IsNullOrEmpty(voiceToUse))
            {
                throw new ArgumentNullException("voiceToUse");
            }
            if (string.IsNullOrEmpty(fileName))
            {
                throw new ArgumentNullException("fileName");
            }
            m_voiceToUse = voiceToUse;
            m_fileName = fileName;
        }

        #region ITextToSpeechProcessor Members

        public Exception Error { get; private set; }

        public virtual bool Process(string speechText, out string output)
        {
            output = null;
            Error = null;

            if (speechText == null)
            {
                Error = new ArgumentNullException("speechText");
                return false;
            }

            speechText = this.SafeCheckText(speechText);

            if (File.Exists(m_fileName))
            {
                try
                {
                    File.SetAttributes(m_fileName, FileAttributes.Normal);
                }
                catch (Exception)
                {
                    // Still want to try synth if this fails
                }
            }

            try
            {
                using (var synth = new SpeechSynthesizer())
                {
                    synth.SelectVoice(m_voiceToUse);
                    synth.SetOutputToWaveFile(m_fileName);
                    synth.SpeakSsml(string.Format(FORMAT_SSML, speechText));
                }
                return true;
            }
            catch (Exception ex)
            {
                Error = new Exception("Speech synth failed (voice: " + m_voiceToUse + ", text: " + speechText + "): " + ex.Message);
                return false;
            }
        }

        #endregion

        private string SafeCheckText(string text)
        {
            var safeText = Regex.Replace(text, "&", "and");
            return safeText;
        }
    }
}
