﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using RSG.Text.Model;
using speechgen.WavesFileXmlParsing;

namespace speechgen
{
    public class WavesFileCleanup
    {
        private Dictionary<string, Dialogue> dialogues;
        private DialogueConfigurations dialogueConfigurations;

        public WavesFileCleanup(DialogueConfigurations dialogueConfigurations, string dstarFilesRootPath) 
        {
            this.dialogueConfigurations = dialogueConfigurations;
            dialogues = new Dictionary<string, Dialogue>();

            string[] dStarFiles = Directory.GetFiles(dstarFilesRootPath, "*.dstar2", SearchOption.AllDirectories);
            foreach (var dStarFile in dStarFiles)
            {
                Dialogue dialogue = new Dialogue(XmlReader.Create(dStarFile), dialogueConfigurations);
                string placeholderSpeechBankName = dialogue.Id + "_PLACEHOLDER";
                if (!dialogues.ContainsKey(placeholderSpeechBankName))
                    dialogues.Add(placeholderSpeechBankName, dialogue);
                else throw new Exception("Multiple dialogues with the name " + dialogue.Name + " found");
            }
        }


        //remove automatically built placeholder speech elements that are  no longer required
        public bool Cleanup(List<LineToBuild> linesToBuild, bool excldePlaceholderSpeech, BankFolder builtPlaceholderSpeechBankFolder, BankFolder pendingPlaceholderSpeechBankFolder, BankFolder builtRecordedSpeechBankFolder)
        {
            RemoveBuiltRecordedSpeechLines(linesToBuild, builtRecordedSpeechBankFolder, pendingPlaceholderSpeechBankFolder);
            bool recordedPSLinesRemoved = RemoveRecordedPlaceholderSpeechLines(builtRecordedSpeechBankFolder, builtPlaceholderSpeechBankFolder, pendingPlaceholderSpeechBankFolder);
            bool deletedPSLinesRemoved = RemoveDeletedPlaceholderSpeechLines(builtPlaceholderSpeechBankFolder, pendingPlaceholderSpeechBankFolder);
            return recordedPSLinesRemoved || deletedPSLinesRemoved;
        }

        //remove already built lines from pending placeholder speech bankfolder
        private bool RemoveRecordedPlaceholderSpeechLines(BankFolder builtRecordedSpeechBankFolder, BankFolder builtPlaceholderSpeechBankFolder, BankFolder pendingPlaceholderSpeechBankFolder) 
        {
            bool changed = false;
            if (builtPlaceholderSpeechBankFolder != null && builtRecordedSpeechBankFolder != null) 
            {
                foreach (Bank builtPlaceholderSpeechDialogueBank in builtPlaceholderSpeechBankFolder.Banks) 
                {
                    foreach (Bank builtRecordedSpeechDialogueBank in builtRecordedSpeechBankFolder.Banks) 
                    {
                        if ((builtRecordedSpeechDialogueBank.Id.ToUpper()+"_PLACEHOLDER").Equals(builtPlaceholderSpeechDialogueBank.Id.ToUpper())) 
                        {
                            bool dialogueBankChanged = false;

                            Bank pendingPlaceholderSpeechDialogueBank=null;
                            bool newPendingPlaceholderSpeechDialogueBank = !pendingPlaceholderSpeechBankFolder.TryGetBank(builtPlaceholderSpeechDialogueBank.Id, out pendingPlaceholderSpeechDialogueBank);
                            if (newPendingPlaceholderSpeechDialogueBank) pendingPlaceholderSpeechDialogueBank = new Bank(builtPlaceholderSpeechDialogueBank.Id, WaveListOperation.modify);
                            
                            foreach (WaveFolder builtPlaceholderSpeechConversationWaveFolder in builtPlaceholderSpeechDialogueBank.WaveFolders)
                            {
                                bool conversationWaveFolderChanged = false;

                                WaveFolder pendingPlaceholderSpeechConversationWaveFolder = null;
                                bool newPendingPlaceholderSpeechConversationWaveFolder = !pendingPlaceholderSpeechDialogueBank.TryGetWaveFolder(builtPlaceholderSpeechConversationWaveFolder.Id, out pendingPlaceholderSpeechConversationWaveFolder);
                                if (newPendingPlaceholderSpeechConversationWaveFolder) pendingPlaceholderSpeechConversationWaveFolder = new WaveFolder(builtPlaceholderSpeechConversationWaveFolder.Id, WaveListOperation.modify);

                                foreach (WaveFolder builtPlaceholderSpeechCharacterWaveFolder in builtPlaceholderSpeechConversationWaveFolder.WaveFolders)
                                {
                                    foreach (WaveFolder builtRecordedSpeechCharacterWaveFolder in builtRecordedSpeechDialogueBank.WaveFolders)
                                    {
                                        if (builtPlaceholderSpeechCharacterWaveFolder.Id.ToUpper().Equals(builtRecordedSpeechCharacterWaveFolder.Id.ToUpper()))
                                        {
                                            WaveFolder pendingPlaceholderSpeechCharacterWaveFolder = null;
                                            bool newPendingPlaceholderSpeechCharacterWaveFolder = !pendingPlaceholderSpeechDialogueBank.TryGetWaveFolder(builtRecordedSpeechCharacterWaveFolder.Id, out pendingPlaceholderSpeechCharacterWaveFolder);
                                            if (newPendingPlaceholderSpeechCharacterWaveFolder) pendingPlaceholderSpeechCharacterWaveFolder = new WaveFolder(builtRecordedSpeechCharacterWaveFolder.Id, WaveListOperation.modify);
                                            
                                            bool characterFolderChanged = RemoveRecordedPlaceholderSpeechLines(builtRecordedSpeechCharacterWaveFolder, builtPlaceholderSpeechCharacterWaveFolder, pendingPlaceholderSpeechCharacterWaveFolder);
                                            conversationWaveFolderChanged = conversationWaveFolderChanged || characterFolderChanged;
                                            if (characterFolderChanged && newPendingPlaceholderSpeechCharacterWaveFolder) pendingPlaceholderSpeechConversationWaveFolder.Add(pendingPlaceholderSpeechCharacterWaveFolder);
                                        }
                                    }
                                }
                                dialogueBankChanged = dialogueBankChanged || conversationWaveFolderChanged;
                                if (conversationWaveFolderChanged && newPendingPlaceholderSpeechConversationWaveFolder) pendingPlaceholderSpeechDialogueBank.Add(pendingPlaceholderSpeechConversationWaveFolder);
                            }
                            changed = dialogueBankChanged || changed;
                            if (dialogueBankChanged && newPendingPlaceholderSpeechDialogueBank) pendingPlaceholderSpeechBankFolder.Add(pendingPlaceholderSpeechDialogueBank);
                        }
                    }
                }
            }
            return changed;
        }

        private bool RemoveRecordedPlaceholderSpeechLines(WaveFolder builtRecordedSpeechCharacterWaveFolder, WaveFolder builtPlaceholderSpeechCharacterWaveFolder, WaveFolder pendingPlaceholderSpeechCharacterWaveFolder) 
        {
            bool changed = false;
            foreach (Wave builtRecordedSpeechWave in builtRecordedSpeechCharacterWaveFolder.Waves)
            {
                foreach (Wave builtPlaceholderSpeechWave in builtPlaceholderSpeechCharacterWaveFolder.Waves)
                {
                    if (builtPlaceholderSpeechWave.Id.ToUpper().Equals(builtRecordedSpeechWave.Id.ToUpper()))
                    {
                        pendingPlaceholderSpeechCharacterWaveFolder.Add(new Wave(builtPlaceholderSpeechWave.Id, WaveListOperation.remove));
                        changed = true;
                    }
                }
            }
            return changed;
        }


        //remove built recorded lines from pending placeholder speech bankfolder
        private void RemoveBuiltRecordedSpeechLines(List<LineToBuild> linesToBuild, BankFolder builtRecordedSpeechBankFolder, BankFolder pendingPlaceholderSpeechBankFolder)
        {
            if (builtRecordedSpeechBankFolder == null) return;
            foreach (Bank builtRecordedDialogueBank in builtRecordedSpeechBankFolder.Banks)
            {
                Bank emptyBankToRemove = null;
                foreach (Bank pendingPlaceholderSpeechDialogueBank in pendingPlaceholderSpeechBankFolder.Banks)
                {
                    if ((builtRecordedDialogueBank.Id.ToUpper()+"_PLACEHOLDER").Equals(pendingPlaceholderSpeechDialogueBank.Id.ToUpper()))
                    {
                        List<WaveFolder> emptyWaveFoldersToRemove = new List<WaveFolder>();
                        //placeholder speech has the character wavefolder in an additional conversationroot wavefolder (recorded speech has the character wavefolder in the dialogue bank)
                        foreach (WaveFolder pendingConversationRootWaveFolder in pendingPlaceholderSpeechDialogueBank.WaveFolders)
                        {
                            if (RemoveBuiltLines(linesToBuild, builtRecordedDialogueBank, pendingConversationRootWaveFolder))
                            {
                                emptyWaveFoldersToRemove.Add(pendingConversationRootWaveFolder);
                            }
                        }
                        foreach (WaveFolder emptyWaveFolder in emptyWaveFoldersToRemove) pendingPlaceholderSpeechDialogueBank.Remove(emptyWaveFolder);
                        if (!System.Linq.Enumerable.Any<WaveFolder>(pendingPlaceholderSpeechDialogueBank.WaveFolders)) emptyBankToRemove = pendingPlaceholderSpeechDialogueBank;
                        break;
                    }
                }
                if (emptyBankToRemove != null) pendingPlaceholderSpeechBankFolder.Remove(emptyBankToRemove);
            }
        }

        //returns whether the pendingPlaceholderSpeechWaveFolder is empty and therefore can be removed
        private bool RemoveBuiltLines(List<LineToBuild> linesToBuild, WaveFolder builtSpeechWaveFolder, WaveFolder pendingPlaceholderSpeechWaveFolder) 
        {
            bool allPendingPlaceholderSpeechSubFoldersAreEmpty = true;
            foreach (WaveFolder builtSpeechWaveSubFolder in builtSpeechWaveFolder.WaveFolders)
            {
                WaveFolder emptyFolderToRemove = null;
                foreach (WaveFolder pendingPlaceholderSpeechWaveSubFolder in pendingPlaceholderSpeechWaveFolder.WaveFolders)
                {
                    if (builtSpeechWaveSubFolder.Id.ToUpper().Equals(pendingPlaceholderSpeechWaveSubFolder.Id.ToUpper()))
                    {
                        if (RemoveBuiltLines(linesToBuild, builtSpeechWaveSubFolder, pendingPlaceholderSpeechWaveSubFolder))
                        {
                            emptyFolderToRemove = pendingPlaceholderSpeechWaveSubFolder;
                        }
                        allPendingPlaceholderSpeechSubFoldersAreEmpty = allPendingPlaceholderSpeechSubFoldersAreEmpty && emptyFolderToRemove!=null;
                    }
                }
                if (emptyFolderToRemove!=null) pendingPlaceholderSpeechWaveFolder.Remove(emptyFolderToRemove);
            }
            foreach (Wave builtWave in builtSpeechWaveFolder.Waves)
            {
                Wave pendingWaveToRemove = null;
                foreach (Wave pendingWave in pendingPlaceholderSpeechWaveFolder.Waves)
                {
                    if (builtWave.Id.ToUpper().Equals(pendingWave.Id.ToUpper()))
                    {
                        pendingWaveToRemove = pendingWave;
                        //remove line from linesToBuild - assume filename is unique
                        linesToBuild.RemoveAll(x => x.FileName.EndsWith(pendingPlaceholderSpeechWaveFolder.Id +"\\"+ pendingWave.Id));
                    }
                }
                if (pendingWaveToRemove != null) pendingPlaceholderSpeechWaveFolder.Remove(pendingWaveToRemove);
            }
            return !System.Linq.Enumerable.Any<Wave>(pendingPlaceholderSpeechWaveFolder.Waves) && allPendingPlaceholderSpeechSubFoldersAreEmpty;
        }

        //returns whether there have been changes (deleting of any built placeholder speech lines) or not
        private bool RemoveDeletedPlaceholderSpeechLines(BankFolder builtPlaceholderSpeechBankFolder, BankFolder pendingPlaceholderSpeechBankFolder)
        {
            bool placeholderSpeechBankFolderChanges = false;
            foreach (Bank builtPlaceholderSpeechBank in builtPlaceholderSpeechBankFolder.Banks)
            {
                Bank pendingPlaceholderSpeechBank = null;
                bool newPendingPlaceholderSpeechBank = !pendingPlaceholderSpeechBankFolder.TryGetBank(builtPlaceholderSpeechBank.Id, out pendingPlaceholderSpeechBank);
                if (newPendingPlaceholderSpeechBank) pendingPlaceholderSpeechBank = new Bank(builtPlaceholderSpeechBank.Id, WaveListOperation.modify);

                bool placeholderSpeechBankChanges = false;
                if (dialogues.ContainsKey(builtPlaceholderSpeechBank.Id))
                {
                    placeholderSpeechBankChanges = RemoveDeletedPlaceholderSpeechLines(dialogues[builtPlaceholderSpeechBank.Id], builtPlaceholderSpeechBank, pendingPlaceholderSpeechBank);
                }
                else
                {
                    if (!newPendingPlaceholderSpeechBank) pendingPlaceholderSpeechBankFolder.Remove(pendingPlaceholderSpeechBank);
                    pendingPlaceholderSpeechBankFolder.Add(new Bank(builtPlaceholderSpeechBank.Id, WaveListOperation.remove));
                    placeholderSpeechBankFolderChanges = true;
                }
                if (placeholderSpeechBankChanges && newPendingPlaceholderSpeechBank) pendingPlaceholderSpeechBankFolder.Add(pendingPlaceholderSpeechBank);
                placeholderSpeechBankFolderChanges = placeholderSpeechBankFolderChanges || placeholderSpeechBankChanges;
            }
            return placeholderSpeechBankFolderChanges;
        }

        private bool RemoveDeletedPlaceholderSpeechLines(Dialogue dialogue, WaveFolder builtPlaceholderSpeechBank, WaveFolder pendingPlaceholderSpeechBank)
        {
            bool placeholderSpeechWaveFolderChanges = false;
            foreach (WaveFolder builtPlaceholderSpeechConversationFolder in builtPlaceholderSpeechBank.WaveFolders)
            {
                WaveFolder pendingPlaceholderSpeechConversationFolder = null;
                bool newPendingPlaceholderSpeechConversationFolder = !pendingPlaceholderSpeechBank.TryGetWaveFolder(builtPlaceholderSpeechConversationFolder.Id, out pendingPlaceholderSpeechConversationFolder);
                if (newPendingPlaceholderSpeechConversationFolder) pendingPlaceholderSpeechConversationFolder = new WaveFolder(builtPlaceholderSpeechConversationFolder.Id, WaveListOperation.modify);

                bool placeholderSpeechWaveSubFolderChanges = false;
                Conversation conversation = getConversation(dialogue, builtPlaceholderSpeechConversationFolder.Id);
                if (conversation != null)
                {
                    placeholderSpeechWaveSubFolderChanges = RemoveDeletedPlaceholderSpeechLines(conversation, builtPlaceholderSpeechConversationFolder, pendingPlaceholderSpeechConversationFolder);
                }
                else
                {
                    if (!newPendingPlaceholderSpeechConversationFolder) pendingPlaceholderSpeechBank.Remove(pendingPlaceholderSpeechConversationFolder);
                    pendingPlaceholderSpeechBank.Add(new WaveFolder(builtPlaceholderSpeechConversationFolder.Id, WaveListOperation.remove));
                    placeholderSpeechWaveFolderChanges = true;
                }
                if (placeholderSpeechWaveSubFolderChanges && newPendingPlaceholderSpeechConversationFolder) pendingPlaceholderSpeechBank.Add(pendingPlaceholderSpeechConversationFolder);
                placeholderSpeechWaveFolderChanges = placeholderSpeechWaveFolderChanges || placeholderSpeechWaveSubFolderChanges;

            }
            return placeholderSpeechWaveFolderChanges;
        }

        private Conversation getConversation(Dialogue dialogue, string conversationRoot) 
        {
            foreach (Conversation conversation in dialogue.Conversations) if (conversation.Root.ToUpper().Equals(conversationRoot.ToUpper())) return conversation;
            return null;
        }

        private bool RemoveDeletedPlaceholderSpeechLines(Conversation conversation, WaveFolder builtPlaceholderSpeechConversationFolder, WaveFolder pendingPlaceholderSpeechConversationFolder)
        {
            var linesPerCharacter = TextModelHelper.getLinesPerCharacter(conversation);
            bool placeholderSpeechWaveFolderChanges = false;
            foreach (WaveFolder builtPlaceholderSpeechCharacterFolder in builtPlaceholderSpeechConversationFolder.WaveFolders)
            {
                WaveFolder pendingPlaceholderSpeechCharacterFolder = null;
                bool newPendingPlaceholderSpeechCharacterFolder = !pendingPlaceholderSpeechConversationFolder.TryGetWaveFolder(builtPlaceholderSpeechCharacterFolder.Id, out pendingPlaceholderSpeechCharacterFolder);
                if (newPendingPlaceholderSpeechCharacterFolder) pendingPlaceholderSpeechCharacterFolder = new WaveFolder(builtPlaceholderSpeechCharacterFolder.Id, WaveListOperation.modify);

                bool placeholderSpeechWaveSubFolderChanges = false;
                Guid characterId = dialogueConfigurations.GetCharacterId(builtPlaceholderSpeechCharacterFolder.Id);
                if (linesPerCharacter.ContainsKey(characterId))
                {
                    placeholderSpeechWaveSubFolderChanges = RemoveDeletedPlaceholderSpeechLines(linesPerCharacter[characterId], builtPlaceholderSpeechCharacterFolder, pendingPlaceholderSpeechCharacterFolder);
                }
                else
                {
                    if (!newPendingPlaceholderSpeechCharacterFolder) pendingPlaceholderSpeechConversationFolder.Remove(pendingPlaceholderSpeechCharacterFolder);
                    pendingPlaceholderSpeechConversationFolder.Add(new WaveFolder(builtPlaceholderSpeechCharacterFolder.Id, WaveListOperation.remove));
                    placeholderSpeechWaveFolderChanges = true;
                }
                if (placeholderSpeechWaveSubFolderChanges && newPendingPlaceholderSpeechCharacterFolder) pendingPlaceholderSpeechConversationFolder.Add(pendingPlaceholderSpeechCharacterFolder);
                placeholderSpeechWaveFolderChanges = placeholderSpeechWaveFolderChanges || placeholderSpeechWaveSubFolderChanges;

            }
            return placeholderSpeechWaveFolderChanges;
        }

        private bool RemoveDeletedPlaceholderSpeechLines(IList<ILine> lines, WaveFolder builtPlaceholderSpeechCharacterFolder, WaveFolder pendingPlaceholderSpeechCharacterFolder)
        {
            bool linesHaveBeenDeleted = false;
            
            var linesLookup = new HashSet<string>();
            foreach (var line in lines)
            {
                linesLookup.Add(line.AudioFilepath + ".WAV");
            }

            foreach (Wave wave in builtPlaceholderSpeechCharacterFolder.Waves) 
            {
                if (!linesLookup.Contains(wave.Id))
                {
                    pendingPlaceholderSpeechCharacterFolder.Add(new Wave(wave.Id, WaveListOperation.remove));
                    linesHaveBeenDeleted = true;
                }
            }

            return linesHaveBeenDeleted;
        }

        
    }
}
