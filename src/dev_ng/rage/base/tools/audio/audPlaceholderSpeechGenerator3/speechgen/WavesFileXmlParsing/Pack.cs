﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;

namespace speechgen.WavesFileXmlParsing
{
    public class Pack : BankFolder
    {
        public IList<string> AllowedBankNameStartLetters { get; private set; }

        public bool IsPlaceholderSpeechPack { get; private set; }

        private const string PlaceholderSpeechPackPrefix = "PS_";

        public Pack(XElement packElement): base(packElement)
        {
            initializeProperties();
        }

        public Pack(string id, WaveListOperation operation)
            : base(id, operation)
        {
            initializeProperties();
        }

        private void initializeProperties()
        {
            this.AllowedBankNameStartLetters = new List<string>();

            if (!string.IsNullOrEmpty(Id) && Id.ToUpper().StartsWith(PlaceholderSpeechPackPrefix))
            {
                IsPlaceholderSpeechPack = true;
                var substring = Id.Substring(3).ToUpper();
                if (substring.Length != 2)
                {
                    throw new Exception("Pack name not in expected format - unable to initialize pack: " + Id);
                }

                this.AllowedBankNameStartLetters.Add(substring[0].ToString());
                var currChar = substring[0];
                while (currChar != substring[1])
                {
                    this.AllowedBankNameStartLetters.Add((++currChar).ToString());
                }
            }
            else
            {
                IsPlaceholderSpeechPack = false;
                char[] alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
                foreach (char letter in alphabet)
                {
                    this.AllowedBankNameStartLetters.Add(letter.ToString());
                }
            }
        }

        public override XElement Serialize()
        {
            var element = new XElement("Pack");
            if (DoSerialization(element) ||
                Operation != WaveListOperation.none)
            {
                return element;
            }
            return null;
        }

        public IEnumerable<string> getAllWavesPendingForDeletion(string outputPath)
        {
            List<string> wavesToDelete = new List<string>();
            wavesToDelete.AddRange(base.getAllWavesPendingForDeletion(Path.Combine(outputPath, this.Id)));
            return wavesToDelete;
        }
    }
}
