﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using NLog.Config;
using NLog.Targets;
using rage.ToolLib.CmdLine;

namespace speechgen
{
    public class CmdLineArgs
    {
        #region Constants

        /// <summary>
        /// The rebuild.
        /// </summary>
        public const string RebuildArgName = "rebuild";

        /// <summary>
        /// The help argument name.
        /// </summary>
        public const string HelpArgName = "help";

        /// <summary>
        /// The question mark argument name.
        /// </summary>
        public const string QuestionMarkArgName = "?";

        /// <summary>
        /// The log file argument name.
        /// </summary>
        public const string LogFileArgName = "logfile";

        /// <summary>
        /// The P4 client.
        /// </summary>
        public const string P4ClientArgName = "p4client";

        /// <summary>
        /// The P4 depot root argument name.
        /// </summary>
        public const string P4DepotRootArgName = "p4depotroot";

        /// <summary>
        /// The P4 host argument name.
        /// </summary>
        public const string P4HostArgName = "p4host";

        /// <summary>
        /// The P4 password argument name.
        /// </summary>
        public const string P4PasswordArgName = "p4passwd";

        /// <summary>
        /// The P4 user argument name.
        /// </summary>
        public const string P4UserArgName = "p4user";

        /// <summary>
        /// The project argument name.
        /// </summary>
        public const string ProjectArgName = "project";

        /// <summary>
        /// The working path argument name.
        /// </summary>
        public const string WorkingPathArgName = "workingpath";

        /// <summary>
        /// The temp path argument name.
        /// </summary>
        public const string TempPathArgName = "temppath";
        public const string DefaultTempPath = @"D:\Temp";

        /// <summary>
        /// The databuilder path argument name.
        /// </summary>
        public const string DataBuilderPathArgName = "databuilder";

        #endregion

        /// <summary>
        /// Get the description of the arguments accepted by the program.
        /// </summary>
        /// <returns>
        /// The full description <see cref="string"/>.
        /// </returns>
        public static string GetUsage()
        {
            var builder = new StringBuilder();
            builder.Append(Environment.NewLine);
            builder.Append("Generates synthesized placeholder speech for dialogue.");
            builder.Append(Environment.NewLine);
            builder.Append(Environment.NewLine);

            builder.Append("Usage:");
            builder.Append(Environment.NewLine);
            builder.Append("SPEECHGEN -"+WorkingPathArgName+" <dir> -"+ProjectArgName+" <file> -"+P4HostArgName+" <server:port>");
            builder.Append(Environment.NewLine);
            builder.Append("-"+P4ClientArgName+" <workspace> -"+P4UserArgName+" <user_name> [-"+P4PasswordArgName+" <password>] [-"+RebuildArgName+"]");
            builder.Append(Environment.NewLine);
            builder.Append("[-"+LogFileArgName+" <file>] [-"+HelpArgName+"/-"+QuestionMarkArgName+"]");
            builder.Append(Environment.NewLine);
            builder.Append(Environment.NewLine);

            builder.Append("-" + WorkingPathArgName + "\tthe root directory for all relative paths.");
            builder.Append(Environment.NewLine);
            builder.Append("-" + DataBuilderPathArgName + "\tthe databuilder location.");
            builder.Append(Environment.NewLine);
            builder.Append("-" + TempPathArgName + "\tthe temporary directory for building packs (" + DefaultTempPath + " by default).");
            builder.Append(Environment.NewLine);
            builder.Append("-" + ProjectArgName + "\tthe path to the project settings file.");
            builder.Append(Environment.NewLine);
            builder.Append("-" + P4HostArgName + "\t\tthe connection info for perforce ex. rsgedip4s1:1666.");
            builder.Append(Environment.NewLine);
            builder.Append("-" + P4ClientArgName + "\tthe perforce client workspace ex. EDIW-HBUNYAN.");
            builder.Append(Environment.NewLine);
            builder.Append("-" + P4UserArgName + "\t\tthe perforce user name ex. hughel.bunyan.");
            builder.Append(Environment.NewLine);
            builder.Append("-" + P4PasswordArgName + "\tthe perforce password.");
            builder.Append(Environment.NewLine);
            builder.Append("-" + P4DepotRootArgName + "\tthe depot root.");
            builder.Append(Environment.NewLine);
            builder.Append("-" + RebuildArgName + "\tforce all the lines to be generated.");
            builder.Append(Environment.NewLine);
            builder.Append("-" + LogFileArgName + "\tspecifies the path to the file used for logging.");
            builder.Append(Environment.NewLine);
            builder.Append("-"+HelpArgName+" or -"+QuestionMarkArgName+"\tdisplay usage information.");
            builder.Append(Environment.NewLine);
            return builder.ToString();
        }
    }

    public class MissingArgumentException: Exception
    {
        public MissingArgumentException(string argumentName): base("Missing \"-"+argumentName+"\" command line argument.")
        {
        }
    }
}
