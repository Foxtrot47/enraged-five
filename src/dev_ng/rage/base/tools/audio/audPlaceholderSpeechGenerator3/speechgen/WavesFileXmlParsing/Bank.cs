﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace speechgen.WavesFileXmlParsing
{
    public class Bank : WaveFolder    
    {

        public Bank(XElement bankElement): base(bankElement)
        {
        }

        public Bank(string id, WaveListOperation operation)
            : base(id, operation)
        {
        }

        public override XElement Serialize()
        {
            var element = new XElement("Bank");
            if (DoSerialization(element) ||
                Operation != WaveListOperation.none)
            {
                return element;
            }
            return null;
        }
    }
}
