using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace RAGEShaderManager
{
    public static class ConsoleOutput
    {
        public static void PrintError(string errorString)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("ERROR: " + errorString);
            Debug.WriteLine("ERROR: " + errorString);
            Console.ResetColor();
        }

        public static void PrintWarning(string warningString)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("WARNING: " + warningString);
            Debug.WriteLine("WARNING: " + warningString);
            Console.ResetColor();
        }

        public static void PrintOutput(string outputString)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(outputString);
            Debug.WriteLine(outputString);
            Console.ResetColor();
        }

        public static void PrintOutput(string outputString, ConsoleColor colour)
        {
            Console.ForegroundColor = colour;
            Console.WriteLine(outputString);
            Debug.WriteLine(outputString);
            Console.ResetColor();
        }
    }
}
