﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace RAGEShaderManager.Util
{
    static public class FileUtil
    {
        /// <summary>
        /// Returns the full path of a file if it is found in any directory below the root directory passed in
        /// </summary>
        /// <param name="rootdir"></param>
        /// <param name="basefilename"></param>
        static public string ResolveFullPath( string rootdir, string basefilename )
        {
            string fullpath = Path.Combine(rootdir, basefilename);
            if (File.Exists(fullpath))
                return fullpath;

            foreach ( string d in Directory.GetDirectories( rootdir ) )
            {
                fullpath = Path.Combine( d, basefilename );
                if( File.Exists( fullpath ) )
                    return fullpath;

                fullpath = ResolveFullPath( d, basefilename );
                if (fullpath != null)
                    return fullpath;
            }
            return null;
        }
        
    }
}
