using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using RAGEShaderManager.Configuration;
using System.Windows.Forms;

namespace RAGEShaderManager.Dependencies
{
    public class ShaderDependencies
    {
        #region Singleton Instance
        private static ShaderDependencies _instance;

        public static ShaderDependencies Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new ShaderDependencies();

                return _instance;
            }
        }
        #endregion

        #region Constructor
        public ShaderDependencies()
        {
        }
        #endregion

        #region Check Dependency
        public bool CheckDependency(DateTime sourceDateTime, ref Dictionary<string, DateTime> dependencyList, bool verbose)
        {
            foreach (KeyValuePair<string, DateTime> kvp in dependencyList)
            {
                if (!File.Exists(kvp.Key))
                {
                    ConsoleOutput.PrintWarning(String.Format("Reason to rebuild: {0} - doesn't exist", kvp.Key));
                    return true;
                }

                int compare = sourceDateTime.CompareTo(kvp.Value);

                if (compare < 0)
                {
                    if (verbose)
                    {
                        ConsoleOutput.PrintWarning(String.Format("Reason to rebuild: {0}, expected {1} but time was {2}", kvp.Key, sourceDateTime.ToString(), kvp.Value.ToString()));
                    }
                    else
                    {
                        ConsoleOutput.PrintWarning(String.Format("Reason to rebuild: {0}", kvp.Key));
                    }
                    return true;
                }
            }

            return false;
        }

        public bool CheckDependency(string outputFilename, ref Dictionary<string, DateTime> dependencyList, bool verbose)
        {
            if (!File.Exists(outputFilename))
            {
                ConsoleOutput.PrintWarning(String.Format("Doesn't exist: {0}", outputFilename));
                return true;
            }

            FileInfo fileInfo = new FileInfo(outputFilename);
            DateTime fileTimeInfo = fileInfo.LastWriteTime;

            return CheckDependency(fileTimeInfo, ref dependencyList, verbose);
        }

        public DateTime GetLatestModifiedFileDateTime(ref Dictionary<string, DateTime> dependencyList)
        {
            DateTime result = new DateTime(1974, 1, 25, 15, 00, 00);

            foreach (KeyValuePair<string, DateTime> kvp in dependencyList)
            {
                int compare = result.CompareTo(kvp.Value);

                if (compare < 0)
                {
                    result = kvp.Value;
                }
            }

            return result;
        }

        public DateTime GetLatestModifiedFileDateTime(string filename)
        {
            DateTime result = new DateTime(1974, 1, 25, 15, 00, 00);

            if (!File.Exists(filename))
                return result;

            StreamReader reader = new StreamReader(filename);
            FileInfo fileInfo = new FileInfo(filename);
            result = fileInfo.LastWriteTime;

            reader.Close();

            return result;
        }

        #endregion

        public Dictionary<string, List<string>> dependencyTree;

        #region Gather Dependencies
        private string ResolveShaderName(string shaderFilename, string addIncludePath, string parentFilename)
        {
            string fullShaderName = shaderFilename;

            if (shaderFilename.Contains("../rage") || shaderFilename.Contains("..\\rage"))
            {
                int index = shaderFilename.IndexOf("rage") + 4;
                string ragePathFixup = "%RAGE_DIR%" + shaderFilename.Substring(index);
                fullShaderName = Environment.ExpandEnvironmentVariables(ragePathFixup);
            }
            else
            {
                if (parentFilename != null)
                {
                    string parentDirectory = Path.GetDirectoryName(parentFilename);
                    fullShaderName = Path.Combine(parentDirectory, shaderFilename);
                }
                else
                {
                    fullShaderName = shaderFilename;
                }
            }

            string altFilePath = addIncludePath == null ? "" : Path.Combine(addIncludePath, shaderFilename);
            if (File.Exists(altFilePath))
            {
                fullShaderName = altFilePath;
            }
            else if (!File.Exists(fullShaderName))
            {
                ConsoleOutput.PrintError("Cannot find shader dependency file: " + fullShaderName);
                return null;
            }


            return Path.GetFullPath(fullShaderName);
        }

        public void AppendExtraDependencies(string shaderName, string platform, PlatformConfigEntry platformConfigEntry, ref Dictionary<string, DateTime> includeList)
        {
            // Build up a path to the final output file so we can use it for dependencies.
            // Uses some of the global variables specified in the config XML file.
            string shaderNameNoExt = Path.GetFileNameWithoutExtension(shaderName);
            string outputShader = Path.Combine(ShaderConfigurations.Instance.ShaderOutputDir, platformConfigEntry.ArgumentList["%OUTPUTDIR%"]);
            outputShader = Path.Combine(outputShader, shaderNameNoExt + "." + platformConfigEntry.ArgumentList["%SHADEREXT%"]);

            string intermediateFolder = Path.Combine(ShaderConfigurations.Instance.ShaderBuildOutput, platformConfigEntry.ArgumentList["%OUTPUTDIR%"]);
            string sourceFolder = Path.Combine(ShaderConfigurations.Instance.ShaderOutputDir, platformConfigEntry.ArgumentList["%OUTPUTDIR%"] + "_source");

            string hlslFile = intermediateFolder + "\\" + shaderNameNoExt + ".hlsl";
            string iFile = intermediateFolder + "\\" + shaderNameNoExt + ".i";
            string shaderSourceFile = sourceFolder + "\\" + shaderNameNoExt + ".hlsl";

            if (platform.ToUpper().Contains("PSN"))
            {
                hlslFile = intermediateFolder + "\\" + shaderNameNoExt + ".cg";

                includeList.Add(hlslFile, GetLatestModifiedFileDateTime(hlslFile));
                includeList.Add(iFile, GetLatestModifiedFileDateTime(iFile));
            }
            // Max only has 1 intermediate file because it just uses FXC as a method to compile and error check the preprocessed .fx shader files.
            else if (platform.ToUpper().Contains("MAX"))
            {
                string oFile = intermediateFolder + "\\" + shaderNameNoExt + ".o";
                includeList.Add(oFile, GetLatestModifiedFileDateTime(iFile));
            }
            else
            {
                includeList.Add(hlslFile, GetLatestModifiedFileDateTime(hlslFile));
                includeList.Add(iFile, GetLatestModifiedFileDateTime(iFile));
            }

            // If this configuration is going to generate a source file then put it in the dependency list.
            if (platformConfigEntry.HasArgument("%GENERATESOURCE%"))
            {
                bool generateSource = platformConfigEntry.ArgumentList["%GENERATESOURCE%"] == "true" ? true : false;
                if (generateSource)
                    includeList.Add(shaderSourceFile, GetLatestModifiedFileDateTime(shaderSourceFile));
            }
        }

        public void InitDependencyList()
        {
            dependencyTree = new Dictionary<string, List<string>>();
        }

        private bool BuildDependencies(string shaderName, ref Dictionary<string, DateTime> includeList)
        {
            bool result = true;

            if (!includeList.ContainsKey(shaderName))
            {
                StreamReader reader = new StreamReader(shaderName);
                FileInfo fileInfo = new FileInfo(shaderName);
                DateTime fileTimeInfo = fileInfo.LastWriteTime;

                includeList.Add(shaderName, fileTimeInfo);

                if (dependencyTree[shaderName] != null)
                {
                    foreach (string depend in dependencyTree[shaderName])
                    {
                        result |= BuildDependencies(depend, ref includeList );
                    }
                }

                reader.Close();
            }

            return result;
        }


        public bool BuildDependencies(string shaderFilename, string addIncludePath, ref Dictionary<string, DateTime> includeList)
        {
            string fullShaderName = ResolveShaderName(shaderFilename, addIncludePath, null);

            if (fullShaderName == null)
                return false;

            if( !dependencyTree.ContainsKey(fullShaderName) )
                return false;

            return BuildDependencies(fullShaderName, ref includeList);
        }

        public bool BuildDependencyTree(string shaderFilename, string parentFilename, string addIncludePath, ref int callDepth, int maxCallDepth, string platform)
        {
            if (callDepth > maxCallDepth)
            {
                ConsoleOutput.PrintWarning("Dependency file check recursivity depth is too high, Found an include in a comment block? (" + shaderFilename + "," + parentFilename + ")");
                return false;
            }

            string fullShaderName = ResolveShaderName(shaderFilename, addIncludePath, parentFilename);

            if (fullShaderName == null)
                return false;

            bool result = true;

            if (!dependencyTree.ContainsKey(fullShaderName))
            {
                dependencyTree.Add(fullShaderName, null);

                StreamReader reader = new StreamReader(fullShaderName);

                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine().Trim();

                    if (line.StartsWith("#pragma RSMignore"))
                    {
                        if (line.Split(' ')[2] == platform)
                        {
                            ConsoleOutput.PrintOutput("Skipping " + shaderFilename);
                            dependencyTree.Remove(fullShaderName);

                            return false;
                        }
                    }
                    if (line.StartsWith("#include"))
                    {
                        string resolvedIncludeFile = ResolveIncludeFile(line);
                        callDepth++;
                        result |= BuildDependencyTree(resolvedIncludeFile, fullShaderName, addIncludePath, ref callDepth, maxCallDepth, platform);
                    }
                }

                reader.Close();
            }

            if (parentFilename != null)
            {
                if (dependencyTree[parentFilename] == null)
                    dependencyTree[parentFilename] = new List<string>();

                dependencyTree[parentFilename].Add(fullShaderName);
            }

            return result;
        }

        public void DumpDependencyTree( )
        {
            foreach (KeyValuePair<string, List<string>> kvp in dependencyTree)
            {
                ConsoleOutput.PrintOutput(string.Format("{0}", kvp.Key));
                if (kvp.Value != null)
                {
                    foreach (string str in kvp.Value)
                    {
                        ConsoleOutput.PrintOutput(string.Format("   {0}", str));
                    }
                }
            } 
        }

        private string ResolveIncludeFile(string includeLine)
        {
            string dependency = includeLine.Substring(includeLine.IndexOf("\"") + 1, (includeLine.LastIndexOf("\"") - includeLine.IndexOf("\"")) - 1);
            return dependency;
        }
        #endregion
    }
}
