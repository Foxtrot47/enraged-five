using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

using RAGEShaderManager.Build;
using RAGEShaderManager.Configuration;
using RAGEShaderManager.Dependencies;

namespace RAGEShaderManager.VCProjBuild
{
    public class VCProjBuilder
    {
        #region Singleton Instance
        private static VCProjBuilder _instance;

        public static VCProjBuilder Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new VCProjBuilder();

                return _instance;
            }
        }
        #endregion

        #region Main Configurations
        Dictionary<string, PlatformConfigEntry> PC_PlatformConfigEntries = new Dictionary<string, PlatformConfigEntry>();
        Dictionary<string, PlatformConfigEntry> Xenon_PlatformConfigEntries = new Dictionary<string, PlatformConfigEntry>();
        Dictionary<string, PlatformConfigEntry> PSN_PlatformConfigEntries = new Dictionary<string, PlatformConfigEntry>();
        Dictionary<string, PlatformConfigEntry> Max_PlatformConfigEntries = new Dictionary<string, PlatformConfigEntry>();
        #endregion

        #region Build Project File
        public void BuildVCProjFile(string projFilename, string shaderSourceDirectory, string VCVersion, string addIncludePath)
        {
            // Make sure our arguments are valid, and the required files are in the right place before we start.
            if (!ValidateRequiredData(shaderSourceDirectory, VCVersion))
                return;

            // Set the current working directory.
            Directory.SetCurrentDirectory(shaderSourceDirectory);

            // Pull in the basic platform configurations so we can build up any output directories we need so dependencies work properly.
            ShaderConfigurations.Instance.GetPlatformConfig("win32_30", "ALL", ref PC_PlatformConfigEntries);
            ShaderConfigurations.Instance.GetPlatformConfig("fxl_final", "ALL", ref Xenon_PlatformConfigEntries);
            ShaderConfigurations.Instance.GetPlatformConfig("psn", "ALL", ref PSN_PlatformConfigEntries);
            ShaderConfigurations.Instance.GetPlatformConfig("fx_max", "ALL", ref Max_PlatformConfigEntries);

            string VCFilename = Path.Combine(shaderSourceDirectory, string.Format("VS_Project\\{0}_{1}.vcproj", projFilename, VCVersion));

            // Build up paths and read in our templates.
            string headerVersion = "header.txt";

            if (VCVersion == "2008")
                headerVersion = "header_2008.txt";

            string headerFile = Path.Combine(shaderSourceDirectory, headerVersion);
            string footerFile = Path.Combine(shaderSourceDirectory, "footer.txt");
            string headerTemplateFile = Path.Combine(shaderSourceDirectory, "headerFileTemplate.txt");
            string shaderTemplateFile = Path.Combine(shaderSourceDirectory, "shaderFileTemplate.txt");

            string headerText = ReadTemplateFile(headerFile);
            string footerText = ReadTemplateFile(footerFile);
            string headerTemplateText = ReadTemplateFile(headerTemplateFile);
            string shaderTemplateText = ReadTemplateFile(shaderTemplateFile);

            List<string> headerFiles = new List<string>();
            List<string> fxFiles = new List<string>();

            // Gather all our .fx and .fxh files in the shader source directory.
            GatherFXHeaderFiles(shaderSourceDirectory, ref headerFiles);
            GatherFXFiles(shaderSourceDirectory, ref fxFiles);

            // Build out all the header templates and the shader templates.
            string newHeaderTemplate = BuildHeaderTemplate(ref headerFiles, ref headerTemplateText);
            string newShaderTemplate = BuildShaderTemplate(ref fxFiles, ref shaderTemplateText, addIncludePath);

            // Write out all the templates we've built out.
            StreamWriter writer = new StreamWriter(VCFilename);

            writer.Write(headerText);
            writer.Write("\t\t<Filter Name=\"Headers\">\r\n");
            writer.Write(newHeaderTemplate);
            writer.Write("\t\t</Filter>\r\n");
            writer.Write(newShaderTemplate);
            writer.Write(footerText);

            writer.Close();
        }
        #endregion

        #region Build Header and Shader Templates
        private string BuildHeaderTemplate(ref List<string> headerFiles, ref string headerTemplateText)
        {
            string template = "";

            // For the number of header files we have, create a template for each one
            // and replace the %filename% tag with the current header file.
            foreach (string file in headerFiles)
                template += headerTemplateText.Replace("%filename%", file) + "\r\n";

            return template;
        }

        private string BuildShaderTemplate(ref List<string> shaderFiles, ref string shaderTemplateText, string addIncludePath)
        {
            string template = "";
            //string RageDir = Environment.ExpandEnvironmentVariables("%RAGE_DIR%");

            //// For the number of shader files we have, use the template to build up each one.
            //foreach(string file in shaderFiles)
            //{
            //    string dependencyString = "";

            //    // Build up a list of all the dependencies for this specific shader.
            //    Dictionary<string, DateTime> dependencyList = new Dictionary<string, DateTime>();
            //    int callDepth = 0;
            //    ShaderDependencies.Instance.GatherDependencies(file, null, ref dependencyList, addIncludePath, ref callDepth, 128, "");

            //    // Go through the dependency list and any file that references the rage directory replace
            //    // the local rage path to one that uses the $(RAGE_DIR) environment variable so when people check it in it's all the same.
            //    foreach (KeyValuePair<string, DateTime> depend in dependencyList)
            //    {
            //        string newName = depend.Key.Replace(RageDir, "$(RAGE_DIR)");
            //        dependencyString += newName + ";";
            //    }

            //    // Grab all the output files for the main platforms. For most platforms there should only be one output file
            //    // but for PC there are multiple configurations so make sure each outputfile is stated so dependencies work properly.
            //    string PCOutputs = GatherOutputFiles(ref PC_PlatformConfigEntries, Path.GetFileNameWithoutExtension(file));
            //    string XenonOutputs = GatherOutputFiles(ref Xenon_PlatformConfigEntries, Path.GetFileNameWithoutExtension(file));
            //    string PS3Outputs = GatherOutputFiles(ref PSN_PlatformConfigEntries, Path.GetFileNameWithoutExtension(file));
            //    string MaxOutputs = GatherOutputFiles(ref Max_PlatformConfigEntries, Path.GetFileNameWithoutExtension(file));

            //    // Add the shader filename into the %filename% entry in the template.
            //    string currentTemplate = shaderTemplateText.Replace("%filename%", file);
                
            //    // Go through and replace all the output paths with the ones we pulled from the configuration file.
            //    // Each one is separate for the given platform.
            //    currentTemplate = currentTemplate.Replace("%win32_30_outputs%", PCOutputs);
            //    currentTemplate = currentTemplate.Replace("%fxl_final_outputs%", XenonOutputs);
            //    currentTemplate = currentTemplate.Replace("%psn_outputs%", PS3Outputs);
            //    currentTemplate = currentTemplate.Replace("%max_outputs%", MaxOutputs);

            //    // Replace the %dep% entry with the actual dependencies that were just generated for this specific shader.
            //    template += currentTemplate.Replace("%dep%", dependencyString) + "\r\n";
            //}

            return template;
        }

        private string ReadTemplateFile(string filename)
        {
            // Just a basic function to read a file completely into memory.
            StreamReader reader = new StreamReader(filename);
            string fileText = reader.ReadToEnd();
            reader.Close();

            return fileText;
        }
        #endregion

        #region Create Output File List
        private string GatherOutputFiles(ref Dictionary<string, PlatformConfigEntry> platformConfig, string shaderNoExt)
        {
            string outputFileList = "";

            // For each platform configuration for the given platform, build up an output string with all the possible
            // output files for this shader. So far this is just for PC because it can have multiple output files.
            foreach (KeyValuePair<string, PlatformConfigEntry> config in platformConfig)
            {
                string outputDir = config.Value.ArgumentList["%OUTPUTDIR%"];
                string shaderExt = config.Value.ArgumentList["%SHADEREXT%"];
                string outputFilename = string.Format("$(OutDir)/{0}/{1}.{2}", outputDir, shaderNoExt, shaderExt);

                outputFileList += outputFilename + ";";
            }

            return outputFileList;
        }
        #endregion

        #region Gather FXH and FX Files
        private void GatherFXHeaderFiles(string shaderSourceDirectory, ref List<string> headerFiles)
        {
            DirectoryInfo di = new DirectoryInfo(shaderSourceDirectory);
            FileInfo[] fi = di.GetFiles("*.fxh");
            FileInfo[] fi_h = di.GetFiles("*.h");

            // Build up a list of all the .fxh and .h files in the shader source directory.
            // These are used to build a template for each file.
            foreach (FileInfo file in fi)
                headerFiles.Add(file.Name);

            foreach (FileInfo file in fi_h)
                headerFiles.Add(file.Name);

        }

        private void GatherFXFiles(string shaderSourceDirectory, ref List<string> fxFiles)
        {
            // Build up a list of all the .fx files in the shader source directory.
            // These are used to build a template for each file.
            DirectoryInfo di = new DirectoryInfo(shaderSourceDirectory);
            FileInfo[] fi = di.GetFiles("*.fx");

            foreach (FileInfo file in fi)
                fxFiles.Add(file.Name);
        }
        #endregion

        #region Validation
        private bool ValidateRequiredData(string shaderSourceDirectory, string VCVersion)
        {
            bool error = false;

            if (!Directory.Exists(shaderSourceDirectory))
            {
                ConsoleOutput.PrintError(string.Format("Could not find shader directory: {0}", shaderSourceDirectory));
                error = true;
            }

            if (VCVersion != "2005" && VCVersion != "2008")
            {
                ConsoleOutput.PrintError(string.Format("Visual Studio version {0} not supported. 2005 or 2008 only supported.", VCVersion));
                error = true;
            }

            string headerFile = Path.Combine(shaderSourceDirectory, "header.txt");
            string footerFile = Path.Combine(shaderSourceDirectory, "footer.txt");
            string headerTemplateFile = Path.Combine(shaderSourceDirectory, "headerFileTemplate.txt");
            string shaderTemplateFile = Path.Combine(shaderSourceDirectory, "shaderFileTemplate.txt");
            
            if (!File.Exists(headerFile))
            {
                ConsoleOutput.PrintError(string.Format("Missing header file: {0}", headerFile));
                error = true;
            }

            if (!File.Exists(footerFile))
            {
                ConsoleOutput.PrintError(string.Format("Missing footer file: {0}", footerFile));
                error = true;
            }

            if (!File.Exists(headerTemplateFile))
            {
                ConsoleOutput.PrintError(string.Format("Missing header template file: {0}", headerTemplateFile));
                error = true;
            }

            if (!File.Exists(shaderTemplateFile))
            {
                ConsoleOutput.PrintError(string.Format("Missing shader template file: {0}", shaderTemplateFile));
                error = true;
            }

            return !error;
        }
        #endregion
    }
}
