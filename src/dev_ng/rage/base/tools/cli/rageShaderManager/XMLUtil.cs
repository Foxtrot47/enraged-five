using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading;
using System.Xml;

namespace RAGEShaderManager
{
    public class XmlUtil
    {
        private XmlUtil()
        { }

        #region Loading Functions
        public static XmlNode LoadXmlFile(string filename)
        {
            XmlDocument doc = new XmlDocument();
            
            if (String.IsNullOrEmpty(filename))
                return null;

            doc.Load(filename);

            return GetRootXmlNode(doc);
        }

        public static XmlNode LoadXmlFromText(string xmlString)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlString);
            return GetRootXmlNode(doc);
        }
        #endregion

        #region Find Node Functions
        public static XmlNode FindNodeInRoot(XmlDocument doc, string nodeName)
        {
            if (doc == null)
                return null;

            for (int i = 0; i < doc.ChildNodes.Count; i++)
            {
                if (String.Compare(doc.ChildNodes[i].Name, nodeName) == 0)
                    return doc.ChildNodes[i];
            }

            return null;
        }

        public static XmlNode FindNode(XmlNode node, string nodeName)
        {
            if (node == null)
                return null;

            for (int i = 0; i < node.ChildNodes.Count; i++)
            {
                if (String.Compare(node.ChildNodes[i].Name, nodeName) == 0)
                    return node.ChildNodes[i];

                XmlNode childNode = FindNode(node.ChildNodes[i], nodeName);
                if (childNode != null)
                    return childNode;
            }

            return null;
        }
        #endregion

        #region Get Node Functions
        public static XmlNode GetRootXmlNode(XmlNode node)
        {
            if (node == null)
                throw new ArgumentNullException("node", "Invalid XmlNode");

            if (node.NodeType != XmlNodeType.Document)
                return GetRootXmlNode(node.OwnerDocument);

            for (int i = 0; i < node.ChildNodes.Count; i++)
            {
                if (node.ChildNodes[i].NodeType != XmlNodeType.XmlDeclaration && 
                    node.ChildNodes[i].NodeType != XmlNodeType.ProcessingInstruction)
                {
                    return node;
                }
            }

            return node;
        }

        public static XmlNode GetChildNode(XmlNode rootNode, string childName)
        {
            if (rootNode == null)
                throw new ArgumentNullException("rootNode", "Invalid root XmlNode");

            for (int i = 0; i < rootNode.ChildNodes.Count; i++)
            {
                if (rootNode.ChildNodes[i].Name == childName)
                    return rootNode.ChildNodes[i];

                if (rootNode.ChildNodes[i].ChildNodes != null && rootNode.ChildNodes[i].ChildNodes.Count > 0)
                {
                    XmlNode childNode = GetChildNode(rootNode.ChildNodes[i], childName);
                    if (childNode != null)
                        return childNode;
                }
            }

            return null;
        }

        public static XmlNode GetChildNode(XmlNode rootNode, string childAttrName, string childAttrValue)
        {
            if (rootNode == null)
                throw new ArgumentNullException("rootNode", "Invalid root XmlNode");

            for (int i = 0; i < rootNode.ChildNodes.Count; i++)
            {
                if (GetAttributeValue(rootNode.ChildNodes[i], childAttrName) == childAttrValue)
                    return rootNode.ChildNodes[i];

                if (rootNode.ChildNodes != null && rootNode.ChildNodes.Count > 0)
                {
                    XmlNode childNode = GetChildNode(rootNode.ChildNodes[i], childAttrName, childAttrValue);
                    if (childNode != null)
                        return childNode;
                }
            }

            return null;
        }
        #endregion

        #region Get Attribute Value Functions
        public static string GetAttributeValue(XmlNode node, string attrName)
        {
            if (node == null || node.Attributes == null)
                return "";

            for (int i = 0; i < node.Attributes.Count; i++)
            {
                if (String.Compare(node.Attributes[i].Name, attrName) == 0)
                    return node.Attributes[i].Value == null ? "" : node.Attributes[i].Value;
            }

            return "";
        }

        public static string GetAttributeValue(XmlReader reader, string attrName)
        {
            string attr = reader.GetAttribute(attrName);
            return attr == null ? "" : attr;
        }

        public static float GetXmlFloatValue(XmlNode node)
        {
            string str = GetAttributeValue(node, "value");
            return Convert.ToSingle(str, NumberFormatInfo.InvariantInfo);
        }

        public static int GetXmlIntValue(XmlNode node)
        {
            string str = GetAttributeValue(node, "value");
            return Convert.ToInt32(str);
        }
        #endregion

        #region Xml Document Creation Functions
        public static XmlDocument CreateXmlDocument()
        {
            XmlDocument doc = new XmlDocument();
            doc.InsertBefore(doc.CreateXmlDeclaration("1.0", "UTF-8", null), doc.DocumentElement);
            return doc;
        }

        public static XmlDocument GetXmlDocument(XmlNode node)
        {
            if (node == null)
                throw new ArgumentNullException("node", "Invalid root XmlNode");

            XmlDocument doc = node as XmlDocument;

            if (doc == null)
                doc = node.OwnerDocument;

            return doc;
        }

        public static XmlNode CreateParentNode(XmlDocument doc, string nodeName)
        {
            XmlElement parentNode = doc.CreateElement(nodeName);
            doc.AppendChild(parentNode);
            return parentNode;
        }

        public static XmlNode CreateChildNode(XmlNode parentNode, string childNodeName)
        {
            XmlNode childNode = GetXmlDocument(parentNode).CreateElement(childNodeName);
            parentNode.AppendChild(childNode);
            return childNode;
        }

        public static void AddAttribute(XmlNode node, string name, string value)
        {
            XmlAttribute attr = GetXmlDocument(node).CreateAttribute(name);
            attr.Value = value;
            node.Attributes.Append(attr);
        }

        public static void AddAttribute(XmlNode node, List<string> names, List<string> values)
        {
            for (int i = 0; i < names.Count; i++)
            {
                XmlAttribute attr = GetXmlDocument(node).CreateAttribute(names[i]);
                attr.Value = values[i];
                node.Attributes.Append(attr);
            }
        }
        #endregion
    }
}
