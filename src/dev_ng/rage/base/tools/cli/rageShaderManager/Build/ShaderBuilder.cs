using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Text.RegularExpressions;
using Microsoft.Win32;

using RAGEShaderManager.Configuration;
using RAGEShaderManager.Dependencies;

namespace RAGEShaderManager.Build
{
    public class ShaderBuilder
    {
        #region Singleton Instance
        private static ShaderBuilder _instance;

        public static ShaderBuilder Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new ShaderBuilder();

                return _instance;
            }
        }
        #endregion

        #region Variables
        ShaderToolConfig preprocessorConfig_;
        ShaderToolConfig silentPreprocessorConfig;
        ShaderToolConfig templateGeneratorConfig;

        public bool DebugShowToolArguments = false;
        public ProcessPriorityClass ToolPriority = ProcessPriorityClass.BelowNormal;

        private StringBuilder errorBuffer = new StringBuilder();
        private StringBuilder outputBuffer = new StringBuilder();

        private List<string> preloadListShaders;
        #endregion

        #region Constructor
        public ShaderBuilder()
        {
            preloadListShaders = new List<string>();

            ShaderConfigurations.Instance.GetToolConfig("Preprocessor", ref preprocessorConfig_);
            ShaderConfigurations.Instance.GetToolConfig("SilentPreprocessor", ref silentPreprocessorConfig);
            ShaderConfigurations.Instance.GetToolConfig("MaterialPresetTemplateGenerator", ref templateGeneratorConfig);
        }
        #endregion

        #region Preload.list Functionality
        public bool LoadPreloadListFile(string filePath)
        {
            if (!File.Exists(filePath))
                return false;

            StreamReader preloadReader = new StreamReader(filePath);
            string line;

            while (!preloadReader.EndOfStream)
            {
                line = preloadReader.ReadLine();

                if (!string.IsNullOrEmpty(line))
                {
                    string shaderEntry = Path.GetFileNameWithoutExtension(line);
                    preloadListShaders.Add(shaderEntry.ToLower());
                }
            }

            preloadReader.Close();

            if (preloadListShaders.Count == 0)
            {
                ConsoleOutput.PrintError(String.Format("Preload list file: {0} contains no shader entries", filePath));
                return false;
            }

            return true;            
        }

        public bool IsPreloadShader(string shader)
        {
            string filename = Path.GetFileNameWithoutExtension(shader);
            return preloadListShaders.Contains(filename.ToLower());
        }
        #endregion

        #region Build Shader
        public int BuildShader(string shaderPath, PlatformConfigEntry config, bool useSilentPreprocessor)
        {
            string intermediateOutput = Path.Combine(ShaderConfigurations.Instance.ShaderBuildOutput, config.ArgumentList["%OUTPUTDIR%"]);
            string finalOutput = Path.Combine(ShaderConfigurations.Instance.ShaderOutputDir, config.ArgumentList["%OUTPUTDIR%"]);

            // Create an intermediate build folder if it doesn't exist already.
            if (!Directory.Exists(intermediateOutput))
            {
                ConsoleOutput.PrintOutput(String.Format("Creating intermediate folder: {0}", intermediateOutput));
                Directory.CreateDirectory(intermediateOutput);
            }

            // Create the final output folder if it doesn't exist already.
            if (!Directory.Exists(finalOutput))
            {
                ConsoleOutput.PrintOutput(String.Format("Creating final folder: {0}", finalOutput));
                Directory.CreateDirectory(finalOutput);
            }

            // Execute the build steps to build the actual shaders out.
            // Check for any error codes returning from the tools and report them appropriately.
            int preprocessorErrorCode = ExecutePreprocessor(shaderPath, intermediateOutput, config, useSilentPreprocessor);
            if (preprocessorErrorCode != 0)
            {
                ConsoleOutput.PrintError(String.Format("Error in preprocessor on shader: {0}\nExit Code: {1}", shaderPath, preprocessorErrorCode));
                return 1;
            }
            else
            {
                if (config.CompileToolName != "")
                {
                    int buildErrorCode = ExecuteShaderBuild(shaderPath, intermediateOutput, config);
                    if (buildErrorCode != 0)
                    {
                        ConsoleOutput.PrintError(String.Format("Error executing {0} on shader: {1}", config.CompileToolName, shaderPath));
                        return 1;
                    }

                    if (!File.Exists(shaderPath))
                    {
                        ConsoleOutput.PrintError(String.Format("Could not create shader: {0}", shaderPath));
                        return 1;
                    }

                    FileInfo fi = new FileInfo(shaderPath);

                    if (fi.Length == 0)
                    {
                        ConsoleOutput.PrintError(String.Format("Shader file is 0 KB: {0}", shaderPath));
                        return 1;
                    }
                }
            }

            return 0;
        }
        #endregion

        #region Expand Arguments
        public string ExpandGlobalArguments(string originalArgumentString)
        {
            string newArgumentString = "";

            newArgumentString = originalArgumentString;

            // Go through all the global tokens and replace any occurrence with the actual path/variable.
            foreach (KeyValuePair<string, string> token in ShaderConfigurations.Instance.GlobalArguments)
                newArgumentString = newArgumentString.Replace(token.Key, token.Value);

            // As a final step, resolve any environment variables in the argument string.
            newArgumentString = Environment.ExpandEnvironmentVariables(newArgumentString);

            // Check to see if we still have any variables wrapped in %% even after the substitution.
            // This means a variable could not be resolved so we should bail out.
            MatchCollection output = Regex.Matches(newArgumentString, @"\%(.*?)\%");
            bool isArgStringValid = true;

            for (int i = 0; i < output.Count; i++)
            {
                ConsoleOutput.PrintError(String.Format("{0} -> Could not resolve this variable. Please check your configuration file.", output[0]));
                isArgStringValid = false;
            }

            // Make sure the argument string is valid.
            if (isArgStringValid)
                return newArgumentString;

            // If the argument string is invalid then return a null string to let the builder know
            // that we can't proceed.
            return null;
        }

        public string ExpandArguments(string originalArgumentString, string shaderName, PlatformConfigEntry config)
        {
            string newArgumentString = "";
            string sourceShader = "";

            if (File.Exists(shaderName))
                sourceShader = shaderName;
            else
                sourceShader = Path.Combine(ShaderConfigurations.Instance.ShaderSourceDir, shaderName + ".fx");

            shaderName = Path.GetFileNameWithoutExtension(shaderName);

            // Setup some global variable arguments.
            if (ShaderConfigurations.Instance.GlobalArguments.ContainsKey("%SHADERNAME%"))
                ShaderConfigurations.Instance.GlobalArguments["%SHADERNAME%"] = shaderName;
            else
                ShaderConfigurations.Instance.GlobalArguments.Add("%SHADERNAME%", shaderName);

            if (ShaderConfigurations.Instance.GlobalArguments.ContainsKey("%SOURCESHADER%"))
                ShaderConfigurations.Instance.GlobalArguments["%SOURCESHADER%"] = sourceShader;
            else
                ShaderConfigurations.Instance.GlobalArguments.Add("%SOURCESHADER%", sourceShader);

            // Go through all the tokens and replace any occurrence with the actual path/variable. 
            newArgumentString = originalArgumentString;
            foreach (KeyValuePair<string, string> token in config.ArgumentList)
                newArgumentString = newArgumentString.Replace(token.Key, token.Value);

            // Expand the global arguments and any environment variables.
            return ExpandGlobalArguments(newArgumentString);
        }
        #endregion

        #region Execute Build Processes
        private void fixupEnvironmentVariables()
        {
            StringDictionary envVars = new StringDictionary();

            foreach (string key in Environment.GetEnvironmentVariables(EnvironmentVariableTarget.Machine).Keys)
            {
                envVars.Add(key, key);
            }

            foreach (string key in Environment.GetEnvironmentVariables(EnvironmentVariableTarget.User).Keys)
            {
                envVars.Remove(key);
                envVars.Add(key, key);
            }

            foreach (string key in Environment.GetEnvironmentVariables(EnvironmentVariableTarget.Process).Keys)
            {
                if (envVars.ContainsKey(key) && !envVars[key].Equals(key, StringComparison.Ordinal))
                {
                    // We have an env var in the process that is in a different case at the user or machine level.
                    // Add the correct case version.
                    // Simply resetting the name will have no effect because Windows will preserve the case in which
                    // it was initially created. We must remove it and then re-add it.
                    // To remove a environment variable in a process, you must set the value of it to empty string.
                    string value = Environment.GetEnvironmentVariable(key, EnvironmentVariableTarget.Process);

                    Environment.SetEnvironmentVariable(envVars[key], "", EnvironmentVariableTarget.Process);

                    // Now set the environment variable using the correct case.
                    Environment.SetEnvironmentVariable(envVars[key], value, EnvironmentVariableTarget.Process);
                }
            }
        }

        private bool CheckShaderLastLine(string shaderPath)
        {
            StreamReader reader = new StreamReader(shaderPath);
            string lastLine = "";

            while (!reader.EndOfStream)
            {
                char c = (char)reader.Read();
                if (c == '\n')
                {
                    lastLine = "";
                }
                else
                {
                    lastLine += c;
                }
            }

            reader.Close();

            return lastLine.StartsWith("#");
        }

        // Hooked this up but current unused because of the cyclical dependency on the win32_30 shaders.
        // Currently this is only hooked up for an Incredibuild build but uses a different method.
        // Leaving it here in case it can be utilized in the future.
        public int ExecuteTemplateGeneration(string shaderPath, PlatformConfigEntry config)
        {
            string shaderName = Path.GetFileNameWithoutExtension(shaderPath);

            Process templateProcess = new Process();

            errorBuffer.Remove(0, errorBuffer.Length);
            outputBuffer.Remove(0, outputBuffer.Length);

            string processArgs = ExpandArguments(templateGeneratorConfig.Arguments, shaderName, config);

            templateProcess.StartInfo.UseShellExecute = false;
            templateProcess.StartInfo.RedirectStandardOutput = true;
            templateProcess.StartInfo.RedirectStandardError = true;
            templateProcess.EnableRaisingEvents = true;

            templateProcess.StartInfo.FileName = Path.GetFullPath(templateGeneratorConfig.Path);
            templateProcess.StartInfo.Arguments = processArgs;
            templateProcess.StartInfo.CreateNoWindow = true;

            templateProcess.OutputDataReceived += new DataReceivedEventHandler(buildProcess_OutputDataReceived);

            if (DebugShowToolArguments)
            {
                ConsoleOutput.PrintOutput(String.Format("Template Generation Arguments: {0}", templateProcess.StartInfo.Arguments));
                ConsoleOutput.PrintOutput(String.Format("Template Generation Working Dir: {0}", templateProcess.StartInfo.WorkingDirectory));
            }

            templateProcess.Start();
            templateProcess.BeginOutputReadLine();
            errorBuffer.Append(templateProcess.StandardError.ReadToEnd());
            templateProcess.WaitForExit();

            if (errorBuffer.Length > 0)
                ConsoleOutput.PrintError(errorBuffer.ToString());
            if (outputBuffer.Length > 0)
                ConsoleOutput.PrintOutput(outputBuffer.ToString());

            if (templateProcess.ExitCode == 0)
                ConsoleOutput.PrintOutput("Successfully generated material template...");

            return templateProcess.ExitCode;
        }

        public int ExecutePreprocessor(string shaderPath, string intermediateOutputPath, PlatformConfigEntry config, bool useSilentPreprocessor)
        {
            string shaderName = Path.GetFileNameWithoutExtension(shaderPath);
            string workingDirectory = Path.GetDirectoryName(shaderPath);

            Process preprocessorProcess = new Process();

            errorBuffer.Remove(0, errorBuffer.Length);
            outputBuffer.Remove(0, outputBuffer.Length);

            ShaderToolConfig preprocessor = useSilentPreprocessor ? silentPreprocessorConfig : preprocessorConfig_;

            string processArgs = ExpandArguments(preprocessor.Arguments, shaderPath, config);

            if (processArgs == null)
                return 1;

            fixupEnvironmentVariables();

#if false
            #region Visual Studio Path Resolving
            string VS8Path = "";
            string VS9Path = "";
            string VSToolsPath = "";

            try
            {
                RegistryKey VS864Bit = Registry.LocalMachine;
                VS864Bit = VS864Bit.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\VisualStudio\\8.0");
                VS8Path = (string)VS864Bit.GetValue("InstallDir");
                VS864Bit.Close();
            }
            catch (System.Exception)
            {
            }

            if (string.IsNullOrEmpty(VS8Path))
            {
                try
                {
                    RegistryKey VS832Bit = Registry.LocalMachine;
                    VS832Bit = VS832Bit.OpenSubKey("SOFTWARE\\Microsoft\\VisualStudio\\8.0");
                    VS8Path = (string)VS832Bit.GetValue("InstallDir");
                    VS832Bit.Close();
                }
                catch (System.Exception)
                {
                }
            }

            try
            {
                RegistryKey VS964Bit = Registry.LocalMachine;
                VS964Bit = VS964Bit.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\VisualStudio\\9.0");
                VS9Path = (string)VS964Bit.GetValue("InstallDir");
                VS964Bit.Close();
            }
            catch (System.Exception)
            {
            }

            if (string.IsNullOrEmpty(VS9Path))
            {
                try
                {
                    RegistryKey VS932Bit = Registry.LocalMachine;
                    VS932Bit = VS932Bit.OpenSubKey("SOFTWARE\\Microsoft\\VisualStudio\\9.0");
                    VS9Path = (string)VS932Bit.GetValue("InstallDir");
                    VS932Bit.Close();
                }
                catch (System.Exception)
                {
                }
            }

            if (string.IsNullOrEmpty(VS8Path) && string.IsNullOrEmpty(VS9Path))
            {
                ConsoleOutput.PrintError("Could not find Visual Studio install directories in the registry.\nCan not pre-process shaders.");
                return 1;
            }
            else
            {
                if (!string.IsNullOrEmpty(VS8Path))
                    VSToolsPath = VS8Path;
                if (!string.IsNullOrEmpty(VS9Path))
                    VSToolsPath = VS9Path;
            }

            //string compilerPath = Path.GetFullPath(VSToolsPath + "\\..\\..\\VC\\Bin\\" + preprocessor.Path);
            
            string dllPath = Path.GetFullPath(VSToolsPath + "..\\IDE\\");

            if (!File.Exists(compilerPath))
            {
                ConsoleOutput.PrintError(string.Format("Could not resolve VS compiler path: {0}", compilerPath));
                return 1;
            }
            if (!Directory.Exists(dllPath))
            {
                ConsoleOutput.PrintError(string.Format("Could not resolve VS DLL path: {0}", dllPath));
                return 1;
            }

            string extraPath = "";
            extraPath = preprocessorProcess.StartInfo.EnvironmentVariables["PATH"];
            extraPath += ";" + dllPath;
            preprocessorProcess.StartInfo.EnvironmentVariables["PATH"] = extraPath;
            #endregion

            // avoid last line finishing with #endif //...
            // If not wave freaks the fuck out...
            if (CheckShaderLastLine(shaderPath))
            {
                ConsoleOutput.PrintError(String.Format("{0}:last line of file ends with a preprocessor directive and without a newline.", shaderPath));
                return 1;
            }
#endif

            string compilerPath = Path.GetFullPath(preprocessor.Path);

            preprocessorProcess.StartInfo.UseShellExecute = false;
            preprocessorProcess.StartInfo.RedirectStandardOutput = true;
            preprocessorProcess.StartInfo.RedirectStandardError = true;
            preprocessorProcess.EnableRaisingEvents = true;

            preprocessorProcess.StartInfo.WorkingDirectory = workingDirectory;
            preprocessorProcess.StartInfo.FileName = compilerPath;
            preprocessorProcess.StartInfo.Arguments = processArgs;
            preprocessorProcess.StartInfo.CreateNoWindow = true;
            //preprocessorProcess.PriorityClass = ToolPriority;

            preprocessorProcess.OutputDataReceived += new DataReceivedEventHandler(preprocessorProcess_OutputDataReceived);

            if (DebugShowToolArguments)
            {
                ConsoleOutput.PrintOutput(String.Format("Preprocessor Arguments: {0}", preprocessorProcess.StartInfo.Arguments));
                ConsoleOutput.PrintOutput(String.Format("Preprocessor Working Dir: {0}", preprocessorProcess.StartInfo.WorkingDirectory));
            }

            bool exceptionOccured = false;

            try
            {
                preprocessorProcess.Start();
                preprocessorProcess.BeginOutputReadLine();
                errorBuffer.Append(preprocessorProcess.StandardError.ReadToEnd());
                preprocessorProcess.WaitForExit();
            }
            catch (OutOfMemoryException)
            {
                exceptionOccured = true;

                // Because we're blowing up the buffer, we can't read the whole thing,
                // so just grab the first 100 lines and hopefully it should be enough to track down the problem.
                int checkCount = 0;
                string errorMessage = "";
                while (!preprocessorProcess.StandardError.EndOfStream && checkCount < 100)
                {
                    errorMessage += preprocessorProcess.StandardError.ReadLine() + "\n";
                    checkCount++;
                }

                preprocessorProcess.Kill();

                ConsoleOutput.PrintError("Shader pre-processing ran out of memory...\nThis is usually an error in the shader.");
                ConsoleOutput.PrintError("Showing first 100 errors:");
                ConsoleOutput.PrintError(errorMessage);
            }

            // Bail out now seeing as the rest of the data will be hosed.
            if (exceptionOccured)
                return 1;

            string outputFilename = ExpandArguments(config.ArgumentList["%WAVEOUTPUT%"], shaderName, config);

            if (errorBuffer.Length > 0)
                ConsoleOutput.PrintOutput(errorBuffer.ToString());

            outputBuffer.Replace("\n", "\r\n");

            try
            {
                StreamWriter writer = new StreamWriter(outputFilename);
                writer.Write(outputBuffer.ToString());
                writer.Close();

                return preprocessorProcess.ExitCode;
            }
            catch (System.Exception)
            {
                ConsoleOutput.PrintError("Failed to save " + outputFilename + ". Is the file write protected ?");
                return 1;
            }
        }

        void preprocessorProcess_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            outputBuffer.Append(e.Data + "\n");
        }

        public int ExecuteShaderBuild(string shaderPath, string intermediateOutputPath, PlatformConfigEntry config)
        {
            ShaderToolConfig buildToolConfig = new ShaderToolConfig();
            ShaderConfigurations.Instance.GetToolConfig(config.CompileToolName, ref buildToolConfig);
            string shaderName = Path.GetFileNameWithoutExtension(shaderPath);

            Process buildProcess = new Process();

            errorBuffer.Remove(0, errorBuffer.Length);
            outputBuffer.Remove(0, outputBuffer.Length);

            string arg = ExpandArguments(buildToolConfig.Arguments, shaderPath, config);

            if (arg == null)
                return 1;

            bool warningAsError = false;
            if (config.HasArgument("%WARNINGSASERRORS%"))
                warningAsError = config.ArgumentList["%WARNINGSASERRORS%"] == "true" ? true : false;

            bool continueOnError = false;
            if (config.HasArgument("%CONTINUEONERROR%"))
                continueOnError = config.ArgumentList["%CONTINUEONERROR%"] == "true" ? true : false;

            bool generateSource = false;
            if (config.HasArgument("%GENERATESOURCE%"))
                generateSource = config.ArgumentList["%GENERATESOURCE%"] == "true" ? true : false;

            // Check variable dependency ?
            bool checkVariableDependency = false; // Forced off for now: it pisses me off.
            if (config.HasArgument("%CHECKVARDEP%"))
                checkVariableDependency = config.ArgumentList["%CHECKVARDEP%"] == "true" ? true : false;

            buildProcess.StartInfo.UseShellExecute = false;
            buildProcess.StartInfo.RedirectStandardOutput = true;
            buildProcess.StartInfo.RedirectStandardError = true;
            buildProcess.EnableRaisingEvents = true;
            buildProcess.StartInfo.WorkingDirectory = ShaderConfigurations.Instance.ShaderBuildOutput;
            buildProcess.StartInfo.FileName = buildToolConfig.Path;
            buildProcess.StartInfo.Arguments = arg;
            buildProcess.StartInfo.CreateNoWindow = true;
            //buildProcess.PriorityClass = ToolPriority;

            buildProcess.OutputDataReceived += new DataReceivedEventHandler(buildProcess_OutputDataReceived);

            if (DebugShowToolArguments)
            {
                ConsoleOutput.PrintOutput(String.Format("Shader Build Arguments: {0}", buildProcess.StartInfo.Arguments));
                ConsoleOutput.PrintOutput(String.Format("Shader Build Working Dir: {0}", buildProcess.StartInfo.WorkingDirectory));
            }

            try
            {
                buildProcess.Start();
                buildProcess.BeginOutputReadLine();
                errorBuffer.Append(buildProcess.StandardError.ReadToEnd());
                buildProcess.WaitForExit();
            }
            catch (OutOfMemoryException)
            {
                // Because we're blowing up the buffer, we can't read the whole thing,
                // so just grab the first 100 lines and hopefully it should be enough to track down the problem.
                int checkCount = 0;
                string errorMessage = "";
                while (!buildProcess.StandardError.EndOfStream && checkCount < 100)
                {
                    errorMessage += buildProcess.StandardError.ReadLine() + "\n";
                    checkCount++;
                }

                buildProcess.Kill();

                ConsoleOutput.PrintError("Shader build ran out of memory...");
                ConsoleOutput.PrintError("Showing first 100 errors:");
                ConsoleOutput.PrintError(errorMessage);
            }

            int exitCode = buildProcess.ExitCode;

            if (outputBuffer.ToString().Contains("error") && !continueOnError)
                exitCode = 1;

            if (outputBuffer.ToString().Contains("warning") && warningAsError)
                exitCode = 1;

            if (outputBuffer.Length == 1 && errorBuffer.Length == 0)
            {
                ConsoleOutput.PrintError("AutoRecover - No output was generated for this shader.\nProbably a bad machine in the Incredibuild farm.\nMake sure the remote machine has the proper version of DirectX");
                exitCode = 1;
            }

            ConsoleOutput.PrintOutput(outputBuffer.ToString(), ConsoleColor.White);

            if (errorBuffer.Length > 0)
                ConsoleOutput.PrintError(errorBuffer.ToString());

            // Only copy source shaders if the build completed.
            if (exitCode == 0 && generateSource)
            {
                // I use the WaveOutput option as the basis for where fx2cg is writing out the intermediate hlsl files so make sure it exists.
                if (config.HasArgument("%WAVEOUTPUT%"))
                {
                    // Build up the path to the intermediate hlsl file.
                    string intermediateFile = ExpandArguments(config.ArgumentList["%WAVEOUTPUT%"], shaderName, config);
                    string hlslFile = Path.GetDirectoryName(intermediateFile) + "/" + Path.GetFileNameWithoutExtension(intermediateFile) + ".hlsl";

                    // Make sure the hlsl file exists as this will be our "source" shader.
                    if (!File.Exists(hlslFile))
                    {
                        ConsoleOutput.PrintWarning(string.Format("Could not generate source shader: {0}", hlslFile));
                    }
                    else
                    {
                        // Build up the path to where the source shader will be copied too and create the _source directory if it doesn't exist.
                        string configOutputDir = ExpandArguments(config.ArgumentList["%OUTPUTDIR%"], shaderName, config) + "_source";
                        string sourceDirectory = Path.Combine(ShaderConfigurations.Instance.ShaderOutputDir, configOutputDir);
                        string sourceFile = Path.Combine(sourceDirectory, shaderName + ".hlsl");

                        if (!Directory.Exists(sourceDirectory))
                            Directory.CreateDirectory(sourceDirectory);

                        File.Copy(hlslFile, sourceFile, true);

                        ConsoleOutput.PrintOutput(string.Format("Source shader created: {0}", sourceFile));
                    }
                }
                else
                {
                    ConsoleOutput.PrintWarning("Could not generate source shader because \"WaveOutput\" variable does not exist.");
                }
            }

            // Only check and copy dependency file if the build completed
            if (exitCode == 0 && checkVariableDependency)
            {
                if (config.HasArgument("%WAVEOUTPUT%"))
                {
                    // Build up the path to the intermediate hlsl file.
                    string intermediateFile = ExpandArguments(config.ArgumentList["%WAVEOUTPUT%"], shaderName, config);
                    string newDepFile = Path.GetDirectoryName(intermediateFile) + "/" + Path.GetFileNameWithoutExtension(intermediateFile) + ".sdep";
                    
                    string depDirectory = Path.Combine(ShaderConfigurations.Instance.ShaderOutputDir, "dep");
                    string depOutDirectory = Path.Combine(depDirectory, ExpandArguments(config.ArgumentList["%OUTPUTDIR%"], shaderName, config));
                    string oldDepFile = Path.Combine(depOutDirectory, shaderName + ".sdep");

                    if (!Directory.Exists(depOutDirectory))
                        Directory.CreateDirectory(depOutDirectory);

                    if (CheckVariableDependencyFile(newDepFile, oldDepFile))
                    {
                        ConsoleOutput.PrintWarning(string.Format("Shader variables have changed for shader: {0}. Any asset using this shader will need to be rebuilt.", shaderName));

                        // Copy the new file when we have detected a change.
                        if (File.Exists(newDepFile))
                            File.Copy(newDepFile, oldDepFile, true);
                    }
                }
            }

            return exitCode;
        }

        public bool CheckVariableDependencyFile(string newDepFile, string oldDepFile)
        {
            // Make sure the new and old dependency files are found otherwise we can't check properly.
            if (!File.Exists(newDepFile))
            {
                ConsoleOutput.PrintWarning(string.Format("New shader variable file could not be found - {0}", newDepFile));
                return true;
            }
            if (!File.Exists(oldDepFile))
            {
                ConsoleOutput.PrintWarning(string.Format("Old shader variable file could not be found - {0}", oldDepFile));
                return true;
            }

            StreamReader newFileReader = new StreamReader(newDepFile);
            StreamReader oldFileReader = new StreamReader(oldDepFile);

            Dictionary<string, int> CurrentVariables = new Dictionary<string, int>();
            Dictionary<string, int> NewVariables = new Dictionary<string, int>();

            // Gather the old shader variables.
            while (!oldFileReader.EndOfStream)
            {
                string entry = oldFileReader.ReadLine();

                if (string.IsNullOrEmpty(entry))
                    continue;

                string[] entries = entry.Split(';');
                if (entries.Length != 2)
                {
                    ConsoleOutput.PrintError(string.Format("Improper shader dependency file format: {0}", oldDepFile));
                    return true;
                }

                string varName = entries[0];
                int reg = int.Parse(entries[1]);

                CurrentVariables.Add(varName, reg);
            }

            oldFileReader.Close();

            // Gather the new shader variables.
            while (!newFileReader.EndOfStream)
            {
                string entry = newFileReader.ReadLine();

                if (string.IsNullOrEmpty(entry))
                    continue;

                string[] entries = entry.Split(';');
                if (entries.Length != 2)
                {
                    ConsoleOutput.PrintError(string.Format("Improper shader dependency file format: {0}", oldDepFile));
                    return true;
                }

                string varName = entries[0];
                int reg = int.Parse(entries[1]);

                NewVariables.Add(varName, reg);
            }

            newFileReader.Close();

            // Do a quick check to see if the counts don't match because that is our first indication something changed.
            if (CurrentVariables.Count != NewVariables.Count)
            {
                ConsoleOutput.PrintWarning(string.Format("Shader variable count doesn't match previous count. Old Count: {0} New Count: {1}", CurrentVariables.Count, NewVariables.Count));
                return true;
            }

            // Next, go through each variable and make sure they match.
            foreach (KeyValuePair<string, int> kvp in CurrentVariables)
            {
                // Make sure we have the same named variables.
                if (NewVariables.ContainsKey(kvp.Key))
                {
                    string var = kvp.Key;
                    int oldReg = kvp.Value;
                    int newReg = NewVariables[kvp.Key];

                    // Make sure the register numbers match as well.
                    if (newReg != oldReg)
                    {
                        ConsoleOutput.PrintWarning(string.Format("Shader Variable: {0} has changed. Old Register: {1} New Register: {2}", var, oldReg, newReg));
                        return true;
                    }
                }
                else
                {
                    ConsoleOutput.PrintWarning(string.Format("Shader Variable: {0} doesn't exist in newest shader.", kvp.Key));
                    return true;
                }
            }

            // Nothing changed....
            return false;
        }

        void buildProcess_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            outputBuffer.Append(e.Data + "\n");
        }
        #endregion
    }
}
