﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Diagnostics;

using RAGEShaderManager.Configuration;

namespace RAGEShaderManager.Build
{
    class SNDBSBuilder
    {
        #region Singleton Instance
        private static SNDBSBuilder _instance;

        public static SNDBSBuilder Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new SNDBSBuilder();

                return _instance;
            }
        }
        #endregion

        #region Variables
        private StreamWriter configStreamWriter;

        private string _toolPath;
        private string _shaderConfigFilename;

        private StringBuilder outputBuffer = new StringBuilder();
        private StringBuilder errorBuffer = new StringBuilder();
        #endregion

        #region Build
        public void InitializeBuild(string configFilename, string shaderConfigFilename, string toolPath)
        {
            configStreamWriter = new StreamWriter(configFilename);
            _toolPath = toolPath;
            _shaderConfigFilename = shaderConfigFilename;
        }

        public void AddShaderEntry(string platform, string platformConfig, string shaderPath, string outputFilename, string extraOutputFiles, ref Dictionary<string, DateTime> dependencyListDictionary, bool forceLocal, string shaderIntermediateDir)
        {
            List<string> dependencyList = new List<string>();
            foreach (KeyValuePair<string, DateTime> kvp in dependencyListDictionary)
                dependencyList.Add(kvp.Key);

            string dependencyArgs = "$$C ";

            // Build up the dependency list string.
            foreach (string value in dependencyList)
                dependencyArgs = dependencyArgs + " $$I:" + value;

            // Add the final shader as an output
            dependencyArgs += (" $$I:" + outputFilename);
            dependencyArgs += (" $$O:" + outputFilename);

            // Add the extra output dependency files.
            string[] extraOutputFilesSplit = extraOutputFiles.Split(new char[]{';'});
            foreach(string extraOutput in extraOutputFilesSplit)
                dependencyArgs += (" $$O:" +extraOutput);

            string processEntry = string.Format("{0} -action=cleanbuild -platform={1} -nodependencies -platformConfig={2} -shader=$$I:{3} -config=$$I:{4} {5}", _toolPath, platform, platformConfig, shaderPath, _shaderConfigFilename, dependencyArgs);
            configStreamWriter.WriteLine(processEntry);
        }

        public int ExecuteBuild(string SNDBSFilename, string platform)
        {
            configStreamWriter.Close();

            string SCERootDir = Environment.ExpandEnvironmentVariables("%SCE_ROOT_DIR%");
            string SCEOrbisDir = Environment.ExpandEnvironmentVariables("%SCE_ORBIS_SDK_DIR%");
            string extraTemplateDir = string.Format("{0}\\host_tools\\bin", SCEOrbisDir);

            string dbsRunExecutable = Path.Combine(SCERootDir, "Common\\SN-DBS\\bin\\dbsrun.exe");

            if (!File.Exists(dbsRunExecutable))
            {
                ConsoleOutput.PrintError("Could not find SN-DBS install. Please install SN-DBS and make sure you have the SCE_ROOT_DIR setup properly.");
                return 1;
            }

            Process DBSProcess = new Process();
            DBSProcess.StartInfo.WorkingDirectory = ShaderConfigurations.Instance.ShaderBuildOutput;
            DBSProcess.StartInfo.FileName = dbsRunExecutable;

            DBSProcess.StartInfo.Arguments = string.Format("dbsbuild -v -k -templates \"{2}\" -p \"{0} - Shader Compile\" -s {1}", platform, SNDBSFilename, extraTemplateDir);

            DBSProcess.StartInfo.UseShellExecute = false;
            DBSProcess.StartInfo.RedirectStandardOutput = true;
            DBSProcess.StartInfo.RedirectStandardError = true;
            DBSProcess.EnableRaisingEvents = true;
            DBSProcess.StartInfo.CreateNoWindow = true;
            DBSProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;

            DBSProcess.OutputDataReceived += new DataReceivedEventHandler(buildProcess_OutputDataReceived);

            DBSProcess.Start();
            DBSProcess.BeginOutputReadLine();
            errorBuffer.Append(DBSProcess.StandardError.ReadToEnd());

            DBSProcess.WaitForExit();

            if (outputBuffer.Length > 0)
            {
                StreamWriter outputWriter = new StreamWriter("X:\\DBSBuildOutput.txt");
                outputWriter.Write(outputBuffer.ToString());
                outputWriter.Close();

                ConsoleOutput.PrintOutput(outputBuffer.ToString());
            }

            if (errorBuffer.Length > 0)
            {
                StreamWriter errorWriter = new StreamWriter("X:\\DBSBuildErrors.txt");
                errorWriter.Write(errorBuffer.ToString());
                errorWriter.Close();

                ConsoleOutput.PrintError(errorBuffer.ToString());
            }

            return DBSProcess.ExitCode;
        }

        void buildProcess_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            outputBuffer.Append(e.Data + "\n");
        }
        #endregion
    }
}
