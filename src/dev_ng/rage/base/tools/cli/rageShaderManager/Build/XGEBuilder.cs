using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;
using System.Diagnostics;
using Microsoft.Win32;

using RAGEShaderManager.Configuration;

namespace RAGEShaderManager.Build
{
    public class XGEBuilder
    {
        #region Singleton Instance
        private static XGEBuilder _instance;

        public static XGEBuilder Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new XGEBuilder();

                return _instance;
            }
        }
        #endregion

        #region Variables
        private XmlDocument tempBuildDoc;
        private XmlNode     parentNode;
        private XmlNode     environmentsNode;
        private XmlNode     projectsNode;
        private XmlNode     rootTaskGroup;
        private int         timeLimit;

        private Dictionary<string, XmlNode> ConfigGroupNodes;
        #endregion

        #region Properties
        public int TimeLimit
        {
            get { return timeLimit; }
            set { timeLimit = value; }
        }
        #endregion

        #region Build Functions
        public void InitializeBuild(ref List<string> configNames, string configFilename, string toolPath, string extraParams, string action, bool singleCore)
        {
            // Keep a list of XML nodes so specific configurations can be grouped into TaskGroup's for XGE.
            ConfigGroupNodes = new Dictionary<string, XmlNode>();

            // Create default XGE XML information.
            tempBuildDoc = XmlUtil.CreateXmlDocument();            
            parentNode = XmlUtil.CreateParentNode(tempBuildDoc, "BuildSet");
            XmlUtil.AddAttribute(parentNode, "FormatVersion", "1");

            // Create our parent "Environments" XML node.
            environmentsNode = XmlUtil.CreateChildNode(parentNode, "Environments");

            // Build the "Environment" node which will contain tool declarations.
            BuildEnvironmentNode(configFilename, toolPath, extraParams, action, singleCore);

            // Build the "Project" node which will contain all our source files, dependencies, etc.
            BuildProjectsNode(ref configNames);
        }

        public void CheckIncredibuildDXEntry()
        {
            try
            {
                RegistryKey IBDXKey = Registry.CurrentUser.OpenSubKey("Software\\Xoreax\\Incredibuild\\Builder", true);

                if (IBDXKey == null)
                {
                    ConsoleOutput.PrintWarning("Could not find registry key: \"HKEY_CURRENT_USER\\Software\\Xoreax\\Incredibuild\\Builder\"\nThis is needed to properly build DX shaders.");
                    return;
                }

                string RegistryKeyValue = (string)IBDXKey.GetValue("FullyVirtualizeDirectX");

                if (RegistryKeyValue == "1" || RegistryKeyValue == null)
                    IBDXKey.SetValue("FullyVirtualizeDirectX", "0");

                IBDXKey.Close();
            }
            catch (System.Exception)
            {
                ConsoleOutput.PrintWarning("Could not modify Incredibuild registry entry to properly build DirectX components.\nPlease make sure you have access to the registry.\nYou may need to modify UAC privelages to be able to access the registry.");
            }
        }

        public string ConvertOSVersionToXGEString(OSVersion osVersion)
        {
            // Convert our OS enum to the XGE string equivalent.
            // This is defined in the xgConsole User Manual.
            switch(osVersion)
            {
                case OSVersion.OS_WINDOWS2000:
                    return "2000";
                case OSVersion.OS_XP:
                    return "XP";
                case OSVersion.OS_VISTA:
                    return "VISTA";
                case OSVersion.OS_WINDOWS7:
                    return "7";
                case OSVersion.OS_INVALID:
                default:
                    return "XP";
            }
        }

        public int ExecuteBuild(bool rebuild, OSVersion maxOSVersion, bool openMonitor, string platform, bool justclean, string XGEXMLFilename, bool useIDEMonitor)
        {
            // Xoreax support had suggested that because we are having issues with DX versions on peoples machine we turn
            // off the "FullyVirtualizeDirectX" registry key which seemed to fix bad builds. This step makes sure the registry key
            // is turned off before building. It only needs to be done on the local machine and not on the build clients.
            CheckIncredibuildDXEntry();

            // Write out the XML file that we will use to feed to the xgConsole.exe
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            //if(XGEXMLFilename == null || XGEXMLFilename == "")
            //    XGEXMLFilename = System.IO.Path.GetTempPath() + "XGEShaderBuild.xml";

            XmlWriter writer = XmlWriter.Create(XGEXMLFilename, settings);
            tempBuildDoc.Save(writer);
            writer.Close();

            // Choose whether we want to build or rebuild.
            string buildType = "/build";
            if (justclean)
                buildType = "/clean";
            else if (rebuild)
                buildType = "/rebuild";

            string openMonitorOpt = "";
            if (openMonitor)
                openMonitorOpt = "/openmonitor";

            string useIDEMonitorOpt = "";
            if (useIDEMonitor)
                useIDEMonitorOpt = "/useidemonitor";

            // Build up our minimum allowed OS string.
            // This string type is defined in the "xgConsole Command Line Interface Manual"
            string minimumAllowedOS = ConvertOSVersionToXGEString(maxOSVersion);

            // For people who have never built shaders before, this needs to be here otherwise the directory is invalid.
            // This is usually created when a build goes through but because the XGE comes before that, this needs to happen here.
            if (!Directory.Exists(ShaderConfigurations.Instance.ShaderBuildOutput))
                Directory.CreateDirectory(ShaderConfigurations.Instance.ShaderBuildOutput);

            // Call the xgConsole.exe with our XML file to build out the shaders.
            // The tool will not wait for this to finish because at this point the RAGEShaderManager has done what it needs to.
            Process IBProcess = new Process();
            IBProcess.StartInfo.WorkingDirectory = ShaderConfigurations.Instance.ShaderBuildOutput;
            IBProcess.StartInfo.FileName = "xgConsole.exe";

            string MaxCPUOption = "";
            int maxCPUVal = 0;
            if (ShaderConfigurations.Instance.GetRegistryOption("MaxCPUS", ref maxCPUVal))
                MaxCPUOption = string.Format("/MAXCPUS={0}", maxCPUVal);
            
            // Set the minimum OS version that can be used to build. This will be based off the highest version we have set in our
            // XML file because other configurations might not be able to handle them.
            IBProcess.StartInfo.Arguments = string.Format("\"{0}\" {1} {2} {7} /MaxWinVer=\"{3}\" /MinWinVer=\"{4}\" /USEIDEMONITOR /showcmd /showtime /showagent /title=\"Shader Build: {5}\" {6}", XGEXMLFilename, buildType, openMonitorOpt, minimumAllowedOS, minimumAllowedOS, platform, MaxCPUOption, useIDEMonitorOpt);

            IBProcess.StartInfo.UseShellExecute = false;
            IBProcess.StartInfo.RedirectStandardOutput = true;
            IBProcess.StartInfo.CreateNoWindow = true;
            IBProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;

            IBProcess.Start();
            StreamReader outreader = IBProcess.StandardOutput;
            string output = outreader.ReadToEnd();
            outreader.Close();

            IBProcess.WaitForExit();
            ConsoleOutput.PrintOutput(output);
            return IBProcess.ExitCode;
        }
        #endregion

        #region XML Creation Functions
        private void BuildEnvironmentNode(string configFilename, string toolPath, string extraParams, string action, bool singleCore)
        {
            if (environmentsNode == null)
                return;

            // Create our base environment node.
            XmlNode envNode = XmlUtil.CreateChildNode(environmentsNode, "Environment");
            XmlUtil.AddAttribute(envNode, "Name", "Shaders");

            // Create our tool nodes for RAGEShaderManager.
            XmlNode envToolsNode = XmlUtil.CreateChildNode(envNode, "Tools");

            // Make the remote and local tool node.
            for (int nToolIndex = 0; nToolIndex < 2; nToolIndex++)
            {
                // Index=0 is the remote config, Index=1 is the local config
                bool bAllowRemote = nToolIndex == 0 ? true : false;

                // Fill in the tool node with the arguments need. Some will come from this tool
                // and some will come from the XGE interface (ex. $(SourceFilename)).
                XmlNode rageShaderManagerTool = XmlUtil.CreateChildNode(envToolsNode, "Tool");
                XmlUtil.AddAttribute(rageShaderManagerTool, "Name", bAllowRemote ? "RAGEShaderManager" : "RAGEShaderManagerLocal");
                
                if (singleCore)
                {
                    XmlUtil.AddAttribute(rageShaderManagerTool, "SingleInstancePerAgent", "true");
                }

                XmlUtil.AddAttribute(rageShaderManagerTool, "AllowRemote", bAllowRemote ? "True" : "False");
                XmlUtil.AddAttribute(rageShaderManagerTool, "GroupPrefix", "Compiling Shaders");
                XmlUtil.AddAttribute(rageShaderManagerTool, "OutputPrefix", "Compiling $(SourceFilename)");
                XmlUtil.AddAttribute(rageShaderManagerTool, "Params", string.Format("-shader=$(SourcePath) -config={0} -action {1} -noDependencies {2}", configFilename, action, extraParams));
                XmlUtil.AddAttribute(rageShaderManagerTool, "Path", toolPath);
                XmlUtil.AddAttribute(rageShaderManagerTool, "OutputFileMasks", "*.*");
                XmlUtil.AddAttribute(rageShaderManagerTool, "AutoRecover", "D3D11.DLL;AutoRecover");

                if (bAllowRemote)
                {
                    // Add the time limit argument to the tool only if we specify a timelimit value.
                    if (timeLimit != 0)
                    {
                        // Time limit on the commandline for rageShaderManager is in minutes but for IB it's in seconds so take it into account now.
                        int secondTimeLimit = timeLimit * 60;
                        XmlUtil.AddAttribute(rageShaderManagerTool, "TimeLimit", secondTimeLimit.ToString());
                    }
                }
            }

            ShaderToolConfig materialPresetToolConfig = new ShaderToolConfig();
            if (!ShaderConfigurations.Instance.GetToolConfig("MaterialPresetTemplateGenerator", ref materialPresetToolConfig))
            {
                ConsoleOutput.PrintError("Could not get material preset template generator tool entry.");
                return;
            }

            string templateToolArguments = ShaderBuilder.Instance.ExpandGlobalArguments(materialPresetToolConfig.Arguments);
            string templateToolPath = ShaderBuilder.Instance.ExpandGlobalArguments(materialPresetToolConfig.Path);

            // Create material template generation tool entry.
            XmlNode materialTemplateGeneratorTool = XmlUtil.CreateChildNode(envToolsNode, "Tool");
            XmlUtil.AddAttribute(materialTemplateGeneratorTool, "Name", "MaterialPresetTemplateGenerator");
            XmlUtil.AddAttribute(materialTemplateGeneratorTool, "AllowRemote", "True");
            XmlUtil.AddAttribute(materialTemplateGeneratorTool, "GroupPrefix", "Generating Material Preset Templates");
            XmlUtil.AddAttribute(materialTemplateGeneratorTool, "OutputPrefix", "Generating Material Preset Templates");
            XmlUtil.AddAttribute(materialTemplateGeneratorTool, "Params", templateToolArguments);
            XmlUtil.AddAttribute(materialTemplateGeneratorTool, "Path", templateToolPath);
            XmlUtil.AddAttribute(materialTemplateGeneratorTool, "OutputFileMasks", "*.*");
        }

        private void BuildProjectsNode(ref List<string> configNames)
        {
            if (parentNode == null)
                return;

            // Setup the base project node.
            projectsNode = XmlUtil.CreateChildNode(parentNode, "Project");
            XmlUtil.AddAttribute(projectsNode, "Name", "FX Shader Compiling");
            XmlUtil.AddAttribute(projectsNode, "Env", "Shaders");

            // Setup the base task group.
            rootTaskGroup = XmlUtil.CreateChildNode(projectsNode, "TaskGroup");
            XmlUtil.AddAttribute(rootTaskGroup, "Name", "BuildShaders");
            XmlUtil.AddAttribute(rootTaskGroup, "Env", "Shaders");

            // Go through each shader configuration and create a new taskgroup for each configuration.
            foreach (string configName in configNames)
            {
                XmlNode node = XmlUtil.CreateChildNode(rootTaskGroup, "TaskGroup");
                XmlUtil.AddAttribute(node, "Name", string.Format("{0} Shaders", configName));
                XmlUtil.AddAttribute(node, "Tool", "RAGEShaderManager");

                ConfigGroupNodes.Add(configName, node);
            }
        }

        public void AddShaderEntry(string platform, string platformConfig, string shaderPath, string outputFilename, string extraOutputFiles, ref List<string> dependencyList, bool forceLocal)
        {
            // Make sure we have a valid XML node for this platform configuration.
            if (!ConfigGroupNodes.ContainsKey(platformConfig))
                return;

            // Create the task for this source file.
            XmlNode shaderTaskGroup = ConfigGroupNodes[platformConfig];
            XmlNode node = XmlUtil.CreateChildNode(shaderTaskGroup, "Task");
            XmlUtil.AddAttribute(node, "SourceFile", shaderPath);

            if (forceLocal)
            {
                XmlUtil.AddAttribute(node, "Tool", "RAGEShaderManagerLocal");
            }
                
            string inputFiles = "";

            // Build up the dependency list string.
            foreach (string value in dependencyList)
            {
                if (string.IsNullOrEmpty(inputFiles))
                    inputFiles = value;
                else
                    inputFiles = inputFiles + ";" + value;
            }

            string shaderName = Path.GetFileName(shaderPath);

            outputFilename = string.Format("{0};{1}", outputFilename, extraOutputFiles);

            // Add our file to this configuration group.
            // Build up the InputFiles (dependencies) and OutputFiles (final compiled shader).
            // Build up the caption so we know what file is being built for what configuration when it shows up in the XGE build window.
            // Build up the parameter list and inherit the ones that are already coming in from the "Tool" node.
            XmlUtil.AddAttribute(node, "InputFiles", inputFiles);
            XmlUtil.AddAttribute(node, "OutputFiles", outputFilename);
            XmlUtil.AddAttribute(node, "Caption", string.Format("{0} - {1}", shaderName, platformConfig));
            XmlUtil.AddAttribute(node, "Params", string.Format("$(inherited:params) -platform {0} -platformConfig {1}", platform, platformConfig));
        }

        public void AddShaderEntry(string platform, string platformConfig, string shaderPath, string outputFilename, string extraOutputFiles, ref Dictionary<string, DateTime> dependencyListDictionary, bool forceLocal)
        {
            List<string> dependencyList = new List<string>();
            foreach (KeyValuePair<string, DateTime> kvp in dependencyListDictionary)
            {
                dependencyList.Add(kvp.Key);
            }

            AddShaderEntry(platform, platformConfig, shaderPath, outputFilename, extraOutputFiles, ref dependencyList, forceLocal);
        }

        public void AddMaterialPresetTemplateGeneratorEntry()
        {
            if (projectsNode == null)
                return;

            XmlNode materialTemplateGroup = XmlUtil.CreateChildNode(projectsNode, "TaskGroup");
            XmlUtil.AddAttribute(materialTemplateGroup, "Name", "BuildMaterialPresetTemplates");
            XmlUtil.AddAttribute(materialTemplateGroup, "Env", "Shaders");
            XmlUtil.AddAttribute(materialTemplateGroup, "Tool", "MaterialPresetTemplateGenerator");

            // We are dependent on the actual shaders building first.
            XmlUtil.AddAttribute(materialTemplateGroup, "DependsOn", "BuildShaders");

            // This will always cause a rebuild of the material templates.
            // Would like to hook up better dependencies at some point.
            XmlNode node = XmlUtil.CreateChildNode(materialTemplateGroup, "Task");
            XmlUtil.AddAttribute(node, "Caption", "Generating material preset templates...");
            XmlUtil.AddAttribute(node, "SourceFile", "dummy.txt");

        }
        #endregion
    }
}
