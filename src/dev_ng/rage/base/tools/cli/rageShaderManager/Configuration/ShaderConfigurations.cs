using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;
using Microsoft.Win32;
using RAGEShaderManager.Util;
using System.Text.RegularExpressions;

namespace RAGEShaderManager.Configuration
{
    #region OS Version Enum
    public enum OSVersion : int
    {
        OS_WINDOWS2000,
        OS_XP,
        OS_VISTA,
        OS_WINDOWS7,
        OS_INVALID
    }
    #endregion

    #region ShaderPlatformConfig
    public struct ShaderPlatformConfig
    {
        public string   PlatformName;
        public Dictionary<string, PlatformConfigEntry> PlatformConfigs;
    }
    #endregion

    #region ShaderPlatformConfigEntry
    public class PlatformConfigEntry
    {
        public string   ConfigName;
        public string   CompileToolName;
        public Dictionary<string, string> ArgumentList;

        public PlatformConfigEntry()
        {
        }

        public bool HasArgument(string argument)
        {
            if (argument.StartsWith("%") && argument.EndsWith("%"))
            {
                if (ArgumentList.ContainsKey(argument.ToUpper()))
                    return true;
                else
                    return false;
            }

            return false;
        }
    }
    #endregion

    #region ShaderPathConfig
    public struct ShaderPathConfig
    {
        public string   PathName;
        public string   Path;
    }
    #endregion

    #region ToolConfig
    public struct ShaderToolConfig
    {
        public string   ToolName;
        public string   Path;
        public string   Arguments;
    }
    #endregion

    public class ShaderConfigurations
    {
        #region Singleton Instance
        private static ShaderConfigurations _instance;

        public static ShaderConfigurations Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new ShaderConfigurations();

                return _instance;
            }
        }
        #endregion

        #region Variables
        private Dictionary<string, ShaderPlatformConfig>    ShaderPlatforms;
        private Dictionary<string, ShaderToolConfig>        ShaderTools;
        private Dictionary<string, ShaderPathConfig>        ShaderPaths;

        public string ShaderSourceDir = "";
        public string BaseIncludePath = "";
        public string ShaderOutputDir = "";
        public string ShaderBuildOutput = "";
        public string WorkingDirectory = "";
        public string TemplateDirectory = "";

        public Dictionary<string, string> GlobalArguments = new Dictionary<string, string>();

        public bool ConfigLoaded = false;
        #endregion

        #region Constructor
        public ShaderConfigurations()
        {
            ShaderPlatforms = new Dictionary<string, ShaderPlatformConfig>();
            ShaderTools = new Dictionary<string, ShaderToolConfig>();
            ShaderPaths = new Dictionary<string, ShaderPathConfig>();
        }
        #endregion

        #region Config XML Loading
        public void LoadConfigurationFile(string filename)
        {
            XmlNode parentNode = XmlUtil.LoadXmlFile(filename);

            XmlNode shaderPathNodes = XmlUtil.GetChildNode(parentNode, "ShaderPaths");
            XmlNode shaderToolNodes = XmlUtil.GetChildNode(parentNode, "ShaderTools");
            XmlNode shaderPlatformNodes = XmlUtil.GetChildNode(parentNode, "ShaderPlatforms");

            if (shaderPathNodes == null || shaderToolNodes == null || shaderPlatformNodes == null)
                throw new ArgumentException("XML settings file is invalid.");

            ShaderPlatforms.Clear();
            ShaderTools.Clear();
            ShaderPaths.Clear();

            LoadShaderPaths(shaderPathNodes);
            LoadShaderPlatforms(shaderPlatformNodes);
            LoadShaderTools(shaderToolNodes);

            ConfigLoaded = true;
        }

        private void LoadShaderPaths(XmlNode shaderPathNodes)
        {
            XmlNode node;

            for (int i = 0; i < shaderPathNodes.ChildNodes.Count; i++)
            {
                ShaderPathConfig pathConfig = new ShaderPathConfig();
                node = shaderPathNodes.ChildNodes[i];

                pathConfig.Path = XmlUtil.GetAttributeValue(node, "Path");
                pathConfig.PathName = XmlUtil.GetAttributeValue(node, "Name");

                // Resolve the environment variables in the path if there are any.
                pathConfig.Path = Environment.ExpandEnvironmentVariables(pathConfig.Path);

                if (pathConfig.PathName.ToUpper() == "SHADERSOURCE")
                {
                    ShaderSourceDir = pathConfig.Path;
                    GlobalArguments.Add("%SHADERSOURCE%", ShaderSourceDir);
                }

                if (pathConfig.PathName.ToUpper() == "BASEINCLUDEPATH")
                {
                    BaseIncludePath = pathConfig.Path;
                    GlobalArguments.Add("%BASEINCLUDEPATH%", BaseIncludePath);
                }

                if (pathConfig.PathName.ToUpper() == "TEMPLATEDIR")
                {
                    TemplateDirectory = pathConfig.Path;
                    GlobalArguments.Add("%TEMPLATEDIR%", TemplateDirectory);
                }
                
                if (pathConfig.PathName.ToUpper() == "SHADERPATH")
                {
                    ShaderOutputDir = pathConfig.Path;
                    GlobalArguments.Add("%SHADERPATH%", ShaderOutputDir);
                }

                if (pathConfig.PathName.ToUpper() == "SHADERBUILDOUTPUT")
                {
                    ShaderBuildOutput = pathConfig.Path;
                    GlobalArguments.Add("%SHADERBUILDOUTPUT%", ShaderBuildOutput);
                }

                if (pathConfig.PathName.ToUpper() == "WORKINGDIR")
                {
                    WorkingDirectory = pathConfig.Path;
                    GlobalArguments.Add("%WORKINGDIR%", WorkingDirectory);
                }

                ShaderPaths.Add(pathConfig.PathName, pathConfig);
            }
        }

        private void LoadShaderTools(XmlNode toolNodes)
        {
            XmlNode node;

            for (int i = 0; i < toolNodes.ChildNodes.Count; i++)
            {
                ShaderToolConfig toolConfig = new ShaderToolConfig();
                node = toolNodes.ChildNodes[i];

                toolConfig.Arguments = XmlUtil.GetAttributeValue(node, "Arguments");
                toolConfig.Path = XmlUtil.GetAttributeValue(node, "Path");
                toolConfig.ToolName = XmlUtil.GetAttributeValue(node, "Name");

                // Resolve the environment variables in the path if there are any.
                toolConfig.Path = Environment.ExpandEnvironmentVariables(toolConfig.Path);

                if (ShaderTools.ContainsKey(toolConfig.ToolName))
                {
                    ConsoleOutput.PrintWarning(String.Format("Duplicate tool definitions. \"{0}\" already exists.", toolConfig.ToolName));
                    continue;
                }

                ShaderTools.Add(toolConfig.ToolName, toolConfig);
            }
        }

        private void LoadShaderPlatforms(XmlNode platformNodes)
        {
            XmlNode platformNode;
            XmlNode platformConfigNode;

            for (int i = 0; i < platformNodes.ChildNodes.Count; i++)
            {
                ShaderPlatformConfig platform = new ShaderPlatformConfig();
                platformNode = platformNodes.ChildNodes[i];

                platform.PlatformName = XmlUtil.GetAttributeValue(platformNode, "Name");
                platform.PlatformConfigs = new Dictionary<string, PlatformConfigEntry>();

                if (ShaderPlatforms.ContainsKey(platform.PlatformName))
                {
                    ConsoleOutput.PrintWarning(String.Format("Duplicate platform definitions. \"{0}\" already exists.", platform.PlatformName));
                    continue;
                }

                for (int j = 0; j < platformNode.ChildNodes.Count; j++)
                {
                    PlatformConfigEntry entry = new PlatformConfigEntry();
                    entry.ArgumentList = new Dictionary<string, string>();
                    platformConfigNode = platformNode.ChildNodes[j];

                    string attributeName = "";

                    for (int att = 0; att < platformConfigNode.Attributes.Count; att++ )
                    {
                        attributeName = String.Format("%{0}%", platformConfigNode.Attributes[att].Name.ToUpper());
                        entry.ArgumentList.Add(attributeName, platformConfigNode.Attributes[att].Value);
                    }

                    entry.ConfigName = XmlUtil.GetAttributeValue(platformConfigNode, "Name");
                    entry.CompileToolName = XmlUtil.GetAttributeValue(platformConfigNode, "CompileTool");

                    if (platform.PlatformConfigs.ContainsKey(entry.ConfigName))
                    {
                        ConsoleOutput.PrintWarning(String.Format("Duplicate platform configuration definitions. \"{0}\" already exists.", entry.ConfigName));
                        continue;
                    }

                    platform.PlatformConfigs.Add(entry.ConfigName, entry);
                }

                ShaderPlatforms.Add(platform.PlatformName, platform);
            }
        }
        #endregion

        #region Preload.list Loading
        public void LoadPreloadListFile(string filename, ref List<string> preloadList)
        {
            StreamReader preloadReader = new StreamReader(filename);
            string line;

            while(!preloadReader.EndOfStream)
            {
                line = preloadReader.ReadLine();

                if (!string.IsNullOrEmpty(line))
                {
                    string resolvedPath = FileUtil.ResolveFullPath(ShaderSourceDir, line);
                    preloadList.Add(resolvedPath);
                }
            }
        }

        public void LoadShaderListFromShaderDirectoryRec(ref List<string> shaderList, String subDir)
        {
            if (Directory.Exists(subDir))
            {
                DirectoryInfo dirInfo = new DirectoryInfo(subDir);
                FileInfo[] fiInfo = dirInfo.GetFiles("*.fx");

                foreach (FileInfo fi in fiInfo)
                {
                    string filename = Path.GetFileName(fi.Name);
                    shaderList.Add(Path.Combine(subDir, filename));
                }

                DirectoryInfo[] diInfo = dirInfo.GetDirectories();
                foreach (DirectoryInfo di in diInfo)
                {
                    LoadShaderListFromShaderDirectoryRec(ref shaderList, di.FullName);
                }
            }
        }

        public void LoadShaderListFromShaderDirectory(ref List<string> shaderList)
        {
            if (Directory.Exists(ShaderSourceDir))
            {
                DirectoryInfo dirInfo = new DirectoryInfo(ShaderSourceDir);
                FileInfo[] fiInfo = dirInfo.GetFiles("*.fx");

                foreach (FileInfo fi in fiInfo)
                {
                    string filename = Path.GetFileName(fi.Name);
                    shaderList.Add(Path.Combine(ShaderSourceDir, filename));
                }

                DirectoryInfo[] diInfo = dirInfo.GetDirectories();
                foreach (DirectoryInfo di in diInfo)
                {
                    if (di.FullName != this.ShaderBuildOutput && !di.FullName.Contains("VS_Project"))
                    {
                        LoadShaderListFromShaderDirectoryRec(ref shaderList, di.FullName);
                    }
                }
            }
        }
        #endregion

        #region Get Platform/Tool Configs
        public bool GetPlatform(string platformName, ref ShaderPlatformConfig config)
        {
            if (ShaderPlatforms.ContainsKey(platformName))
            {
                config = ShaderPlatforms[platformName];
                return true;
            }
            return false;
        }

        public bool GetPlatformConfig(string platformName, string configName, ref Dictionary<string, PlatformConfigEntry> entry)
        {
            if (ShaderPlatforms.ContainsKey(platformName))
            {
                if (configName.ToUpper() == "ALL")
                {
                    entry = ShaderPlatforms[platformName].PlatformConfigs;
                    return true;
                }
                else
                {
                    if (ShaderPlatforms[platformName].PlatformConfigs.ContainsKey(configName))
                    {
                        entry.Add(configName, ShaderPlatforms[platformName].PlatformConfigs[configName]);
                        return true;
                    }
                }
            }
            return false;
        }

        public bool GetToolConfig(string toolName, ref ShaderToolConfig config)
        {
            if (ShaderTools.ContainsKey(toolName))
            {
                config = ShaderTools[toolName];
                return true;
            }
            return false;
        }
        #endregion

        #region OS Version Checking
        public void CheckPlatformConfigOSVersion(ref Dictionary<string, PlatformConfigEntry> entries, OSVersion currentVersion)
        {
            List<string> removeConfigs = new List<string>();

            foreach (KeyValuePair<string, PlatformConfigEntry> kvp in entries)
            {
                PlatformConfigEntry entry = kvp.Value;

                if (entry.HasArgument("%MINOSVERSION%"))
                {
                    OSVersion minVersion = ConvertStringOSVersion(entry.ArgumentList["%MINOSVERSION%"]);

                    if (minVersion == OSVersion.OS_INVALID || currentVersion < minVersion)
                        removeConfigs.Add(kvp.Key);
                }
            }

            foreach (string removeConfig in removeConfigs)
            {
                if (entries.ContainsKey(removeConfig))
                {
                    entries.Remove(removeConfig);
                    ConsoleOutput.PrintWarning(string.Format("Removing {0} config because you current OS version does not support it.", removeConfig));
                }
            }
        }

        public OSVersion CheckForMaximumOSVersionNeeded(ref Dictionary<string, PlatformConfigEntry> entries)
        {
            OSVersion maxVersion = OSVersion.OS_XP;

            foreach (KeyValuePair<string, PlatformConfigEntry> kvp in entries)
            {
                PlatformConfigEntry entry = kvp.Value;

                if (entry.HasArgument("%MINOSVERSION%"))
                {
                    OSVersion minVersion = ConvertStringOSVersion(entry.ArgumentList["%MINOSVERSION%"]);

                    if (minVersion > maxVersion)
                        maxVersion = minVersion;
                }
            }

            return maxVersion;
        }

        public OSVersion ConvertStringOSVersion(string version)
        {
            if (version.ToUpper() == "2000")
                return OSVersion.OS_WINDOWS2000;
            else if (version.ToUpper() == "XP")
                return OSVersion.OS_XP;
            else if (version.ToUpper() == "VISTA")
                return OSVersion.OS_VISTA;
            else if (version.ToUpper() == "7")
                return OSVersion.OS_WINDOWS7;

            return OSVersion.OS_INVALID;
        }
        #endregion

        #region Registry Checking
        public string GetRegistryConfigFilename()
        {
            RegistryKey rootKey = Registry.CurrentUser.OpenSubKey("Software\\Rockstar Games\\Rage\\rageShaderManager");

            // If there is no key then return an empty string so we can either throw an error or
            // fall back on a specified config filename.
            if (rootKey == null)
                return "";

            string configFilename = (string)rootKey.GetValue("ConfigFilename");

            if (!File.Exists(configFilename))
                return "";

            rootKey.Close();

            return configFilename;
        }

        public void SetRegistryConfigFilename(string configFilename)
        {
            RegistryKey rootKey = Registry.CurrentUser.OpenSubKey("Software\\Rockstar Games\\Rage\\rageShaderManager", true);

            if (rootKey == null)
                rootKey = Registry.CurrentUser.CreateSubKey("Software\\Rockstar Games\\Rage\\rageShaderManager");

            if (rootKey != null)
                rootKey.SetValue("ConfigFilename", configFilename);

            rootKey.Close();
        }

        public bool GetRegistryOption(string option, ref int intVal)
        {
            try
            {
                RegistryKey rootKey = Registry.CurrentUser.OpenSubKey("Software\\Rockstar Games\\Rage\\rageShaderManager", true);

                if (rootKey == null)
                    return false;

                string[] keys = rootKey.GetValueNames();
                bool foundOption = false;

                // Make sure the registry value we are checking exists.
                foreach (string key in keys)
                {
                    if (key.ToLower() == option.ToLower())
                    {
                        foundOption = true;
                        break;
                    }
                }

                // If it doesn't exist, just bail out now.
                if (!foundOption)
                    return false;

                // Get the type of key so we can properly cast it.
                RegistryValueKind regType = rootKey.GetValueKind(option);

                if (regType == RegistryValueKind.DWord)
                    intVal = (int)rootKey.GetValue(option);
                else
                {
                    intVal = 0;
                    return false;
                }

                // Close that shit up!
                rootKey.Close();
            }
            catch (System.Exception ex)
            {
                ConsoleOutput.PrintWarning("Could not access registry for override options.");
                ConsoleOutput.PrintWarning(ex.Message);

                return false;
            }

            return true;
        }

        public bool GetRegistryOption(string option)
        {
            bool keyValue = false;

            try
            {
                RegistryKey rootKey = Registry.CurrentUser.OpenSubKey("Software\\Rockstar Games\\Rage\\rageShaderManager", true);

                if (rootKey == null)
                    return false;

                string[] keys = rootKey.GetValueNames();
                bool foundOption = false;

                // Make sure the registry value we are checking exists.
                foreach (string key in keys)
                {
                    if (key.ToLower() == option.ToLower())
                    {
                        foundOption = true;
                        break;
                    }
                }

                // If it doesn't exist, just bail out now.
                if (!foundOption)
                    return false;

                // Get the type of key so we can properly cast it.
                RegistryValueKind regType = rootKey.GetValueKind(option);

                if (regType == RegistryValueKind.DWord)
                    keyValue = ((int)rootKey.GetValue(option) == 1 ? true : false);

                // Close that shit up!
                rootKey.Close();
            }
            catch (System.Exception ex)
            {
                ConsoleOutput.PrintWarning("Could not access registry for override options.");
                ConsoleOutput.PrintWarning(ex.Message);

                return false;
            }

            return keyValue;
        }
        #endregion
    }
}
