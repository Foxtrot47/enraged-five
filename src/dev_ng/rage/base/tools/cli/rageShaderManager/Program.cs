using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Text.RegularExpressions;

using RAGEShaderManager.Build;
using RAGEShaderManager.Configuration;
using RAGEShaderManager.Dependencies;

namespace RAGEShaderManager
{
    static class Program
    {
        [STAThread]
        static int Main(string[] args)
        {
            string toolPath = Application.ExecutablePath;
            OSVersion currentOSVersion = OSVersion.OS_INVALID;
            OSVersion maximumOSVersion = OSVersion.OS_INVALID;

            Arguments parsedArgs = new Arguments(args);

            // Required argument values.
            string platform         = parsedArgs["platform"];
            string platformConfig   = parsedArgs["platformConfig"];
            string shader           = parsedArgs["shader"];
            string action           = parsedArgs["action"];
            string configFilename   = parsedArgs["config"];

            // Optional argument values.
            bool buildFromDirectory     = parsedArgs["buildFromDirectory"] != null ? true : false;
            bool useXGEBuild            = parsedArgs["incredibuild"] != null ? true : false;
            bool useSNDBSBuild          = parsedArgs["sndbs"] != null ? true : false;

            string ignoreShadersPatternArg = parsedArgs["ignoreShadersPattern"];
            if (!string.IsNullOrEmpty(ignoreShadersPatternArg))
            {
                ConsoleOutput.PrintOutput(String.Format("Shader skipping pattern: {0}", ignoreShadersPatternArg));
            }

            string XGEXMLFilename = Path.Combine(System.IO.Path.GetTempPath(),"XGEShaderBuild.xml");
            if (parsedArgs["incredibuild"] != null && parsedArgs["incredibuild"].ToLower() != "true")
                XGEXMLFilename = parsedArgs["incredibuild"];

            string SNDBSFilename = Path.Combine(System.IO.Path.GetTempPath(), "SNDBSShaderBuild.bat");
            if (parsedArgs["sndbs"] != null && parsedArgs["sndbs"].ToLower() != "true")
                SNDBSFilename = parsedArgs["sndbs"];

            bool openMonitor = parsedArgs["openmonitor"] != null ? true : false;
            bool useIDEMonitor = parsedArgs["useIDEMonitor"] != null ? true : false;
            
            bool debugBuild = parsedArgs["debugBuild"] != null ? true : false;
            bool debugDependencies  = parsedArgs["debugDep"] != null ? true : false;
            bool noDependencies     = parsedArgs["noDependencies"] != null ? true : false;

            bool useSilentPreprocessor = parsedArgs["useSilentPreprocessor"] != null ? true : false;

            string maxCPUString = parsedArgs["maxCPU"];
            int maxCPU = 0;

            if (maxCPUString != null)
                int.TryParse(maxCPUString, out maxCPU);

            // Override values.
            string workingDirectory = parsedArgs["workingDir"];
            string shaderPath       = parsedArgs["shaderPath"];
            string shaderSource     = parsedArgs["shaderSource"];
            string shaderOutput     = parsedArgs["shaderOutput"];
            string addIncludePath     = parsedArgs["addIncludePath"];

            // Registry key values
            string configRegKey     = parsedArgs["regConfig"];

            // Override the timeout limit on Incredibuild.
            // Set in minutes so we don't have a huge time limit number in the commandline
            string timeLimit = parsedArgs["XGETimeout"];
            if (!string.IsNullOrEmpty(timeLimit))
            {
                int IBTimeoutLimit = 0;

                if (int.TryParse(timeLimit, out IBTimeoutLimit))
                {
                    XGEBuilder.Instance.TimeLimit = IBTimeoutLimit;
                }
            }
            else
            {
                // Just default to 0 so no limit is set and uses the local Incredibuild setting.
                XGEBuilder.Instance.TimeLimit = 0;
            }

            // Only use 1 distributed build system.
            if (useSNDBSBuild && useXGEBuild)
            {
                ConsoleOutput.PrintError("Attempting to use SN-DBS and Incredibuild. Please choose one or the other");
                return 1;
            }

            // Variable to store if we are building or rebuilding.
            bool cleanOnBuild = false;
            bool rebuild = false;
            bool clean = false;
            bool createDependencySupportFile = false;

            // If we have a config file path we want to set, do it now and exit out after.
            // This should really only be run once if needed but I figured this could go into an install.bat or BuildButton.bat startup script
            // depending on the project you are working on.
            if (!string.IsNullOrEmpty(configRegKey))
            {
                if (File.Exists(configRegKey))
                {
                    ShaderConfigurations.Instance.SetRegistryConfigFilename(configRegKey);
                    ConsoleOutput.PrintOutput(string.Format("Configuration File Added to Registry: {0}", configRegKey));
                }
                else
                {
                    ConsoleOutput.PrintError(string.Format("Invalid config filename: {0}\nCannot add it to the registry.", configRegKey));
                    return 1;
                }
            }

            if (!string.IsNullOrEmpty(configFilename))
                configFilename = Path.GetFullPath(configFilename);

            // Load the configuration file if one is specified and it exists, otherwise try loading the default one.
            if (!string.IsNullOrEmpty(configFilename) && File.Exists(configFilename))
            {
                ShaderConfigurations.Instance.LoadConfigurationFile(configFilename);
            }
            else
            {
                // If we have no config file specified then search the registry for one.
                configFilename = ShaderConfigurations.Instance.GetRegistryConfigFilename();

                // Make sure the registry key has a valid path to a file.
                if (string.IsNullOrEmpty(configFilename))
                {
                    // config filename not given or found - try to figure out a default
                    string projname = Environment.ExpandEnvironmentVariables("%RSG_PROJNAME%");
                    //string projbranch = Environment.ExpandEnvironmentVariables("%RSG_PROJBRANCH%");   // TODO: shaders must know about branches!

                    configFilename = Path.Combine("x:\\", projname);
                    configFilename = Path.Combine(configFilename, "bin");
                    configFilename = Path.Combine(configFilename, projname + "shaders.xml");

                    if (!File.Exists(configFilename))
                    {
                        string configError = "No configuration file was specified or could not be found.\nThere are three ways this can be achieved:\n\n";
                        configError += "1.) See if "+configFilename+" is valid for your project\n";
                        configError += "2.) Use the -regConfig=<config filename> commandline argument to write a config filename to the registry.\n";
                        configError += "3.) Use the -config=<config filename> commandline argument to specify a config file.";

                        ConsoleOutput.PrintError(configError);
                        return 1;
                    }
                }

                ShaderConfigurations.Instance.LoadConfigurationFile(configFilename);
            }

            if (!string.IsNullOrEmpty(ShaderConfigurations.Instance.BaseIncludePath))
                addIncludePath = ShaderConfigurations.Instance.BaseIncludePath;

            if (debugBuild)
				ConsoleOutput.PrintOutput(string.Format("Using Config File: {0}", configFilename));

            // Check to see if we want to override any of the global shader paths (added to override certain folders usually when building embedded shaders).
            if (!string.IsNullOrEmpty(workingDirectory))
            {
                ShaderConfigurations.Instance.WorkingDirectory = workingDirectory;
                ShaderConfigurations.Instance.GlobalArguments["%WORKINGDIRECTORY%"] = workingDirectory;
            }
            if (!string.IsNullOrEmpty(shaderPath))
            {
                ShaderConfigurations.Instance.ShaderOutputDir = shaderPath;
                ShaderConfigurations.Instance.GlobalArguments["%SHADERPATH%"] = shaderPath;
            }
            if (!string.IsNullOrEmpty(shaderSource))
            {
                ShaderConfigurations.Instance.ShaderSourceDir = shaderSource;
                ShaderConfigurations.Instance.GlobalArguments["%SHADERSOURCE%"] = shaderSource;
            }
            if (!string.IsNullOrEmpty(shaderOutput))
            {
                ShaderConfigurations.Instance.ShaderBuildOutput = shaderOutput;
                ShaderConfigurations.Instance.GlobalArguments["%SHADERBUILDOUTPUT%"] = shaderOutput;
            }


            // If we aren't building from the preload list and the shader name is empty, bail out.
            if (string.IsNullOrEmpty(shader) && !useXGEBuild && !useSNDBSBuild && !buildFromDirectory)
            {
                ConsoleOutput.PrintError("No shader file, incredibuild task or preload.list file was specified to build.");
                return 1;
            }

            // If we are building through incredibuild make sure we aren't also specifying a shader or a preload list.
            if (useXGEBuild && !string.IsNullOrEmpty(shader))
            {
                ConsoleOutput.PrintError("A shader or preload.list was specified when incredibuild was specified also. Please choose only one.");
                return 1;
            }

            if (string.IsNullOrEmpty(action))
            {
                ConsoleOutput.PrintWarning("No action specified, using \"cleanbuild\".");
                action = "CLEANBUILD";
            }

            // Check to see if we want to force a rebuild or just a regular build.
            rebuild = action.ToUpper() == "REBUILD" ? true : false;
            clean = action.ToUpper() == "CLEAN" ? true : false; // may also want to set rebuild?
            cleanOnBuild = action.ToUpper() == "CLEANBUILD" ? true : false; // may also want to set rebuild?
            createDependencySupportFile = action.ToUpper() == "BUILDDEPENDENCY" ? true : false; // may also want to set rebuild?

            // Make sure the build action types are valid.
            if (rebuild == false && clean == false && cleanOnBuild == false && createDependencySupportFile == false)
            {
                ConsoleOutput.PrintError("Unspecified action type. Actions available are 'rebuild', 'cleanbuild', 'clean' or 'createDependencySupportFile'");
                return 1;
            }

            if ((useXGEBuild ||useSNDBSBuild) && createDependencySupportFile)
            {
                ConsoleOutput.PrintError("XGE/SN-DBS is not compatible with dependency build");
                return 1;
            }

            // Set whether or not we want to show the arguments that get passed to the tool.
            ShaderBuilder.Instance.DebugShowToolArguments = debugBuild;
            // If the option is false from the commandline, check the registry next.
            if (!debugBuild)
                ShaderBuilder.Instance.DebugShowToolArguments = ShaderConfigurations.Instance.GetRegistryOption("DumpCommandlines");

            // If the option is false from the commandline, check the registry next.
            if (!debugDependencies)
                debugDependencies = ShaderConfigurations.Instance.GetRegistryOption("DumpDependencies");

            // By default the openMonitor flag is false so Incredibuild doesn't open the window. This can be overridden in the registry.
            if (!openMonitor)
                openMonitor = ShaderConfigurations.Instance.GetRegistryOption("OpenMonitor");

            // Set the working directory based on the working directory set in the XML config file.
            try
            {
                Directory.SetCurrentDirectory(ShaderConfigurations.Instance.WorkingDirectory);
            }
            catch (UnauthorizedAccessException)
            {
                ConsoleOutput.PrintError(String.Format("Could not get access to set working directory to: {0}", ShaderConfigurations.Instance.WorkingDirectory));
            }
            catch (FileNotFoundException)
            {
                ConsoleOutput.PrintError(String.Format("Could not find directory: {0}", ShaderConfigurations.Instance.WorkingDirectory));
            }

            // Get the configuration for the given platform.
            // Bail out if we can't find the platform the arguments are referencing.
            ShaderPlatformConfig config = new ShaderPlatformConfig();
            if (ShaderConfigurations.Instance.GetPlatform(platform, ref config))
            {
                if (!useXGEBuild || !useSNDBSBuild)
                    ConsoleOutput.PrintOutput(String.Format("Setting platform to: {0}", platform));
            }
            else
            {
                ConsoleOutput.PrintError(String.Format("Could not find platform \"{0}\" in settings file.\r\nPlease make sure you typed the platform properly.", platform));
                return 1;
            }

            OperatingSystem os = Environment.OSVersion;
            Version vs = os.Version;

            // Check the users OS version so we can exclude them from building certain shader models.
            if (os.Platform == PlatformID.Win32NT)
            {
                switch (vs.Major)
                {
                    case 5:
                        if (vs.Minor == 0)
                            currentOSVersion = OSVersion.OS_WINDOWS2000;
                        else
                            currentOSVersion = OSVersion.OS_XP;
                        break;
                    case 6:
                        if (vs.Minor == 0)
                            currentOSVersion = OSVersion.OS_VISTA;
                        else
                            currentOSVersion = OSVersion.OS_WINDOWS7;
                        break;
                    default:
                        currentOSVersion = OSVersion.OS_INVALID;
                        break;
                }
            }
            
            if (currentOSVersion == OSVersion.OS_INVALID)
            {
                ConsoleOutput.PrintError("Windows version is not supported by this tool.");
                return 1;
            }

            Dictionary<string, PlatformConfigEntry> platformConfigEntries = new Dictionary<string,PlatformConfigEntry>();

            // If we don't specify a platformConfig then build all the platform configurations available for the chosen platform.
            // When building an embedded shader, always build all the configurations you can.
            if (string.IsNullOrEmpty(platformConfig))
                platformConfig = "ALL";

            // If we do specify a platform configuration make sure it's available in the XML file.
            if (!ShaderConfigurations.Instance.GetPlatformConfig(platform, platformConfig, ref platformConfigEntries))
            {
                ConsoleOutput.PrintError(String.Format("Could not find platform configuration \"{0}\" in settings file.\r\nPlease make sure you typed the configuration name properly.", platform));
                return 1;
            }

            // Check to see if any of the platform configurations are not allowed to build based on the users OS version.
            ShaderConfigurations.Instance.CheckPlatformConfigOSVersion(ref platformConfigEntries, currentOSVersion);

            // Because we are using Incredibuild, other people might not be able to build out our shader version.
            // XGE has a way to specify allowing only certain people with high enough OS versions to build our content, so
            // check to see if we need to do this.
            //maximumOSVersion = ShaderConfigurations.Instance.CheckForMaximumOSVersionNeeded(ref platformConfigEntries);

            // Change this to use the hosts computers OS version as the maximum version so we use the current OS and above to build.
            maximumOSVersion = currentOSVersion;

            // Because we could have removed configurations because of OS limitations, check to see if we even have any left to build.
            if (platformConfigEntries.Count == 0)
            {
                ConsoleOutput.PrintError("There are no available configurations to build.\r\nMake sure you have some specified and that they are available for your current OS.");
                return 1;
            }

            List<string> shaderList = new List<string>();

            // Build up a path to the source shader if we aren't building from the preload.list file.
            if( useXGEBuild || useSNDBSBuild || buildFromDirectory )
            {
                ShaderConfigurations.Instance.LoadShaderListFromShaderDirectory(ref shaderList);
            }
            else
            {
                string shaderName;
                // If we pass a fully pathed shader to the tool then take its path and don't build up our own. T.L.
                if (!File.Exists(shader))
                    shaderName = Path.Combine(ShaderConfigurations.Instance.ShaderSourceDir, shader);
                else
                    shaderName = shader;

                shaderList.Add(shaderName);
            }

            Dictionary<string, DateTime> dependencyList = new Dictionary<string, DateTime>();
            int errorCode = 0;

            if (useXGEBuild || useSNDBSBuild || !noDependencies || createDependencySupportFile)
            {
                ShaderDependencies.Instance.InitDependencyList();

                // Build dependency tree
                for (int i = 0; i < shaderList.Count; i++)
                {
                    // Go through all the configurations we have specified to build so we can build output shader names, gather dependencies, etc.
                    int callDepth = 0;
                    ShaderDependencies.Instance.BuildDependencyTree(shaderList[i], null, addIncludePath, ref callDepth, 128, platform);
                }

                if (debugDependencies)
                {
                    for (int i = 0; i < shaderList.Count; i++)
                    {
                        dependencyList.Clear();

                        string shaderName = shaderList[i];
                        if (ShaderDependencies.Instance.BuildDependencies(shaderName, addIncludePath, ref dependencyList))
                        {
                            ConsoleOutput.PrintOutput(shaderName);
                            foreach (KeyValuePair<string, DateTime> depend in dependencyList)
                            {
                                ConsoleOutput.PrintOutput("   " + depend.Key);
                            }
                        }
                    }
                }
            }

            // Build lists of:
            //  1. regex patterns for skipping shaders
            //  2. shaders to prioritize with regard to build order
            //  3. shaders that should only be built locally (if doing a distributed build)
            Dictionary<string, Regex> configRegExIgnores = new Dictionary<string, Regex>();
            Dictionary<string, List<string>> configFilePriority = new Dictionary<string, List<string>>();
            Dictionary<string, List<string>> configForceLocal = new Dictionary<string, List<string>>();
            foreach (KeyValuePair<string, PlatformConfigEntry> kvp in platformConfigEntries)
            {
                string platformConfigName = kvp.Key;
                PlatformConfigEntry platformConfigEntry = kvp.Value;

                // If there's a shader ignore pattern on the command line, then use it. Otherwise use the one in the config file for this platformConfig (if there is one).
                if (!string.IsNullOrEmpty(ignoreShadersPatternArg) || platformConfigEntry.HasArgument("%IGNORESHADERSPATTERN%"))
                {
                    string ignorePattern = string.IsNullOrEmpty(ignoreShadersPatternArg) ? platformConfigEntry.ArgumentList["%IGNORESHADERSPATTERN%"] : ignoreShadersPatternArg;
                    Regex ignoreRegEx = new Regex(ignorePattern);
                    configRegExIgnores.Add(platformConfigName, ignoreRegEx);
                }

                // Get the file build priorities for this config
                if (platformConfigEntry.HasArgument("%BUILDPRIORITY%"))
                {
                    string prioritizedFiles = platformConfigEntry.ArgumentList["%BUILDPRIORITY%"];
                    List<string> filePriorityList = new List<string>(prioritizedFiles.ToLower().Split(';'));
                    configFilePriority.Add(platformConfigName,filePriorityList);
                }

                // Get the list of files that should only be compiled on the local machine
                if (platformConfigEntry.HasArgument("%BUILDLOCAL%"))
                {
                    string buildLocalFiles = platformConfigEntry.ArgumentList["%BUILDLOCAL%"];
                    List<string> forceLocalFileList = new List<string>(buildLocalFiles.ToLower().Split(';'));
                    configForceLocal.Add(platformConfigName,forceLocalFileList);
                }
            }

            if (useXGEBuild || useSNDBSBuild)
            {
                // Let the user know we are gathering all the information to build the shaders because it might take a couple seconds.
                if (useXGEBuild)
                {
                    ConsoleOutput.PrintOutput(string.Format("Building Incredibuild XML File: {0}\nPlease Wait...\n", XGEXMLFilename));
                }
                else if (useSNDBSBuild)
                {
                    ConsoleOutput.PrintOutput(string.Format("Building SN-DBS script file: {0}\nPlease Wait...\n", SNDBSFilename));
                }

                // Gather a list of the configuration names because each configuration will have its own 'TaskGroup' in the XGE build.
                List<string> configNames = new List<string>();
                bool singleCore = false;

                foreach (KeyValuePair<string, PlatformConfigEntry> kvp in platformConfigEntries)
                {
                    if (kvp.Value.HasArgument("%SINGLECORE%"))
                        singleCore = true;

                    configNames.Add(kvp.Key);
                }

                string extraParams = "";

                if (debugBuild)
                    extraParams = "-debugBuild";

                if (useIDEMonitor)
                    extraParams += " -useIDEMonitor";

                if (useSilentPreprocessor)
                    extraParams += " -useSilentPreprocessor";


                if (useXGEBuild)
                {
                    // Initialize the XGE build. This will create the basic information for the XML file.
                    XGEBuilder.Instance.InitializeBuild(ref configNames, configFilename, toolPath, extraParams, action, singleCore);
                }
                else if (useSNDBSBuild)
                {
                    SNDBSBuilder.Instance.InitializeBuild(SNDBSFilename, configFilename, toolPath);
                }

                // Add XGE tasks
                // Go through all the configurations we have specified to build so we can build output shader names, gather dependencies, etc.
                foreach (KeyValuePair<string, PlatformConfigEntry> kvp in platformConfigEntries)
                {
                    PlatformConfigEntry platformConfigEntry = kvp.Value;
                    string platformConfigName = kvp.Key;

                    // Re-order the file list based on the priority list that was passed in
                    List<string> filePriorityList, sortedShaderList;
                    if (configFilePriority.TryGetValue(platformConfigName, out filePriorityList))
                    {
                        ConsoleOutput.PrintOutput(String.Format("Re-ordering shader files based on priority list..."));
                        sortedShaderList = new List<string>();
                        List<string> unsortedShaderList = new List<string>(shaderList);

                        // Move the shaders over starting at highest priority
                        for (int priorityIndex = 0; priorityIndex < filePriorityList.Count; priorityIndex++)
                        {
                            for (int shaderIndex = 0; shaderIndex < unsortedShaderList.Count; )
                            {
                                string shaderNameNoPath = Path.GetFileName(unsortedShaderList[shaderIndex]).ToLower();
                                if (shaderNameNoPath == filePriorityList[priorityIndex])
                                {
                                    sortedShaderList.Add(unsortedShaderList[shaderIndex]);
                                    unsortedShaderList.RemoveAt(shaderIndex);
                                }
                                else
                                {
                                    shaderIndex++;
                                }
                            }
                        }

                        // Now add whatever non-priority shader names that are left over
                        foreach (string shaderFile in unsortedShaderList )
                        {
                            sortedShaderList.Add(shaderFile);
                        }
                    }
                    else
                    {
                        sortedShaderList = shaderList;
                    }

                    for (int i = 0; i < sortedShaderList.Count; i++)
                    {
                        dependencyList.Clear();

                        string shaderName = sortedShaderList[i];
                        if (ShaderDependencies.Instance.BuildDependencies(shaderName, addIncludePath, ref dependencyList))
                        {
                            // Build up a path to the final output file so we can use it for dependencies.
                            // Uses some of the global variables specified in the config XML file.
                            string shaderNameNoExt = Path.GetFileNameWithoutExtension(shaderName);
                            string outputShader = Path.Combine(ShaderConfigurations.Instance.ShaderOutputDir, platformConfigEntry.ArgumentList["%OUTPUTDIR%"]);
                            outputShader = Path.Combine(outputShader, shaderNameNoExt + "." + platformConfigEntry.ArgumentList["%SHADEREXT%"]);

                            string intermediateFolder = Path.Combine(ShaderConfigurations.Instance.ShaderBuildOutput, platformConfigEntry.ArgumentList["%OUTPUTDIR%"]);
                            string sourceFolder = Path.Combine(ShaderConfigurations.Instance.ShaderOutputDir, platformConfigEntry.ArgumentList["%OUTPUTDIR%"] + "_source");
                            string extraOutputFiles;

                            string hlslFile = intermediateFolder + "\\" + shaderNameNoExt + ".hlsl";
                            string iFile = intermediateFolder + "\\" + shaderNameNoExt + ".i";
                            string shaderSourceFile = sourceFolder + "\\" + shaderNameNoExt + ".hlsl";

                            string shaderNameNoPath = Path.GetFileName(shaderName).ToLower();

                            // Check this shader against the ignore-pattern for this platformConfig
                            Regex ignoreRegEx;
                            if (configRegExIgnores.TryGetValue(platformConfigName, out ignoreRegEx) && ignoreRegEx.IsMatch(shaderName))
                            {
                                if ( debugBuild )
                                    ConsoleOutput.PrintOutput(String.Format("Skipping {0} due to IgnoreShadersPattern pattern.", shaderName));

                                continue;
                            }

                            // Check if this shader is on the force-local list
                            List<string> forceLocalFileList;
                            bool forceLocal = (configForceLocal.TryGetValue(platformConfigName, out forceLocalFileList) && forceLocalFileList.Contains(shaderNameNoPath))
								|| (platformConfigEntry.HasArgument("%FORCELOCALBUILD%") && platformConfigEntry.ArgumentList["%FORCELOCALBUILD%"].ToUpper() == "TRUE");
                            
                            // Hacky way to change up how intermediate files are thrown into the dependency chain.
                            // Would like to add this to the .XML file in the future so we can just check the config file to see if we have intermediate files to check against.

                            // PS3 has different intermediate files so make sure to change up the dependencies.
                            if (platform.ToUpper().Contains("PSN"))
                            {
                                hlslFile = intermediateFolder + "\\" + shaderNameNoExt + ".cg";
                                extraOutputFiles = hlslFile + ";" + iFile;
                            }
                            // Max only has 1 intermediate file because it just uses FXC as a method to compile and error check the preprocessed .fx shader files.
                            else if (platform.ToUpper().Contains("MAX"))
                                extraOutputFiles = intermediateFolder + "\\" + shaderNameNoExt + ".o";
                            else
                                extraOutputFiles = hlslFile + ";" + iFile;

                            // If this configuration is going to generate a source file then put it in the dependency list.
                            if (platformConfigEntry.HasArgument("%GENERATESOURCE%"))
                            {
                                bool generateSource = platformConfigEntry.ArgumentList["%GENERATESOURCE%"] == "true" ? true : false;
                                if (generateSource)
                                    extraOutputFiles = extraOutputFiles + ";" + shaderSourceFile;
                            }

                            if (useXGEBuild)
                            {
                                // Get the list of other .fx and .fxh files that are used as dependencies for the shader we want to build.
                                XGEBuilder.Instance.AddShaderEntry(platform, platformConfigName, shaderName, outputShader, extraOutputFiles, ref dependencyList, forceLocal);
                            }
                            else if (useSNDBSBuild)
                            {
                                string shaderIntermediateDir = Path.Combine(intermediateFolder, shaderNameNoExt);
                                shaderIntermediateDir += "\\*.*";

                                SNDBSBuilder.Instance.AddShaderEntry(platform, platformConfigName, shaderName, outputShader, extraOutputFiles, ref dependencyList, forceLocal, shaderIntermediateDir);
                            }
                        }
                    }   
                }

				// Add the task for generating material preset templates (introduced in RDR3)
                XGEBuilder.Instance.AddMaterialPresetTemplateGeneratorEntry();

                if (useXGEBuild)
                {
                    // Execute the actual XGE build. Use our 'rebuild' commandline argument and pass it to the XGE console.
                    errorCode = XGEBuilder.Instance.ExecuteBuild(rebuild, maximumOSVersion, openMonitor, platform, clean, XGEXMLFilename, useIDEMonitor);
                }
                else
                {
                    errorCode = SNDBSBuilder.Instance.ExecuteBuild(SNDBSFilename, platform);
                }
            }
            else
            {
                foreach (KeyValuePair<string, PlatformConfigEntry> kvp in platformConfigEntries)
                {
                    PlatformConfigEntry platformConfigEntry = kvp.Value;
                    string platformConfigName = kvp.Key;

                    ConsoleOutput.PrintOutput(String.Format("Setting platform config: {0}", platformConfigName));

                    string dependencyPath = "";
                    if (createDependencySupportFile)
                    { // Setup path
                        ConsoleOutput.PrintOutput("Scanning dependencies");

                        dependencyPath = Path.Combine(ShaderConfigurations.Instance.ShaderBuildOutput, "dependency");

                        if (!Directory.Exists(dependencyPath))
                            Directory.CreateDirectory(dependencyPath);

                        //ShaderDependencies.Instance.DumpDependencyTree();
                    }

                    for (int i = 0; i < shaderList.Count; i++)
                    {
                        // Build up a path to the outputted shader and check dependencies against that file. 
                        string shaderName = shaderList[i];
                        string shaderNameNoExt = Path.GetFileNameWithoutExtension(shaderName);
                        string newShaderPath = Path.Combine(Path.GetDirectoryName(shaderName), Path.GetFileName(shaderName));
                        string outputShader = Path.Combine(ShaderConfigurations.Instance.ShaderOutputDir, platformConfigEntry.ArgumentList["%OUTPUTDIR%"]);
                        outputShader = Path.Combine(outputShader, shaderNameNoExt + "." + platformConfigEntry.ArgumentList["%SHADEREXT%"]);

                        // Check this shader against the ignore-pattern for this platformConfig
                        Regex ignoreRegEx;
                        if (configRegExIgnores.TryGetValue(platformConfigName, out ignoreRegEx) && ignoreRegEx.IsMatch(shaderName))
                        {
                            if (debugBuild)
                                ConsoleOutput.PrintOutput(String.Format("Skipping {0} due to IgnoreShadersPattern pattern.", shaderName));

                            continue;
                        }

                        // If we aren't rebuilding this shader, then check it's dependencies and build if needed.
                        // If we are rebuilding this shader, just force the shader to build.
                        if (createDependencySupportFile)
                        {
                            dependencyList.Clear();
                            if (ShaderDependencies.Instance.BuildDependencies(shaderName, addIncludePath, ref dependencyList))
                            {
                                string dependencyFileName = String.Concat(shaderNameNoExt, ".d");
                                string dependencyFilePath = Path.Combine(dependencyPath, dependencyFileName);

                                DateTime shaderDependencyDate = ShaderDependencies.Instance.GetLatestModifiedFileDateTime(ref dependencyList);
                                if (debugBuild)
                                    ConsoleOutput.PrintOutput(String.Format("Building dependency for {0} at {1} for {2}", newShaderPath, dependencyFilePath, shaderDependencyDate.ToString()));

                                if (File.GetLastWriteTime(dependencyFilePath) != shaderDependencyDate)
                                {
                                    if (!File.Exists(dependencyFilePath))
                                    {
                                        StreamWriter writer = new StreamWriter(dependencyFilePath);
                                        writer.Write(shaderDependencyDate.ToString());
                                        writer.Close();
                                    }

                                    File.SetLastWriteTime(dependencyFilePath, shaderDependencyDate);
                                }
                            }

                        }
                        else
                        {
                            if (!rebuild)
                            {
                                if (!noDependencies)
                                {
                                    // Get the files that the shader specified depends on.
                                    ConsoleOutput.PrintOutput(String.Format("Checking dependencies for: {0}", newShaderPath));
                                    ShaderDependencies.Instance.BuildDependencies(newShaderPath, addIncludePath, ref dependencyList);
                                    ShaderDependencies.Instance.AppendExtraDependencies(newShaderPath, platform, platformConfigEntry, ref dependencyList);
                                }

                                // Check to see if we need to build the shader based on the dependency check.
                                // noDependencies is usually set if we are using incredibuild because it has its own dependency checking.
                                if (noDependencies || ShaderDependencies.Instance.CheckDependency(outputShader, ref dependencyList, true)) // debugDependencies
                                {
                                    if (cleanOnBuild || clean)
                                    {
                                        if (File.Exists(outputShader))
                                        {
                                            ConsoleOutput.PrintOutput("Deleting " + outputShader);
                                            try
                                            {
                                                File.Delete(outputShader);
                                            }
                                            catch (System.Exception /*e*/)
                                            {
                                                ConsoleOutput.PrintError("Failed to delete " + outputShader);
                                            }
                                        }
                                    }

                                    if (cleanOnBuild)
                                    {
                                        ConsoleOutput.PrintOutput(String.Format("Building: {0}", newShaderPath));
                                        errorCode = ShaderBuilder.Instance.BuildShader(newShaderPath, platformConfigEntry, useSilentPreprocessor);

                                        if (errorCode != 0)
                                            break;
                                    }
                                }
                            }
                            else
                            {
                                // Force a rebuild of the shader
                                ConsoleOutput.PrintOutput(String.Format("Rebuilding: {0}", newShaderPath));
                                errorCode = ShaderBuilder.Instance.BuildShader(newShaderPath, platformConfigEntry, useSilentPreprocessor);

                                if (errorCode != 0)
                                    break;
                            }
                        }
                    }
                }

                if (createDependencySupportFile)
                {
                    if (errorCode != 0)
                        ConsoleOutput.PrintError("Dependency Scan Failed");
                    else
                        ConsoleOutput.PrintOutput("Dependency Scan Completed");
                }
                else
                {
                    if (errorCode != 0)
                        ConsoleOutput.PrintError("Shader Build Failed");
                    else
                        ConsoleOutput.PrintOutput("Shader Build Completed");
                }
            }

            return errorCode;
        }
    }
}
