using System;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Xml;

namespace xlspgen
{
    class XlspUtil
    {
        public static string GetRequiredAttribute(XmlElement el, string attrName)
        {
            return GetAttribute(el, attrName, true);
        }

        public static string GetOptionalAttribute(XmlElement el, string attrName)
        {
            return GetAttribute(el, attrName, false);
        }

        public static string GetAttribute(XmlElement el, string attrName, bool required)
        {
            XmlNode n = el.Attributes.GetNamedItem(attrName);

            if ((null == n) || string.IsNullOrEmpty(n.Value))
            {
                if (required)
                {
                    System.Console.WriteLine(String.Format("Error: Required attribute '{0}' not specified", attrName));
                    throw new ArgumentNullException();
                }
                return "";
            }

            return n.Value;
        }
    }

    class XlspProxy
    {
        public string m_Name = "";
        public string m_Env = "partnernet";
        public string m_Ip = "";
        public string m_Port = "";

        public bool Reset(XmlElement el)
        {
            m_Name = XlspUtil.GetRequiredAttribute(el, "name");
            m_Env = XlspUtil.GetRequiredAttribute(el, "env").ToLower();
            m_Ip = XlspUtil.GetRequiredAttribute(el, "ip");
            m_Port = XlspUtil.GetRequiredAttribute(el, "port");

            if (("partnernet" != m_Env) &&
               ("certnet" != m_Env) &&
               ("productionnet" != m_Env))
            {
                System.Console.WriteLine(String.Format("Error: Invalid env value for XLSP proxy.  Legal values are partnernet, certnet, and productionnet (case-insensitive)."));
                return false;
            }

            return true;
        }
    }

    class XlspSgInternetNic
    {
        public string m_Ip = "";
        public string m_Mask = "";
        public string m_Gateway = "";
        public string m_Mac = "";

        public bool Reset(XmlElement el)
        {
            m_Ip = XlspUtil.GetRequiredAttribute(el, "ip");
            m_Mask = XlspUtil.GetRequiredAttribute(el, "mask");
            m_Gateway = XlspUtil.GetRequiredAttribute(el, "gateway");
            m_Mac = XlspUtil.GetRequiredAttribute(el, "nic");
            return true;
        }
    }

    class XlspSgDatacenterNic
    {
        public string m_Id = "";
        public string m_Ip = "";
        public string m_Mask = "";
        public string m_Gateway = "";
        public string m_Mac = "";
        public string m_NatIpStart = "";
        public string m_NatIpEnd = "";
        public int m_NumNatIps = 0;

        public bool Reset(XmlElement el)
        {
            m_Id = XlspUtil.GetOptionalAttribute(el, "id");
            if (string.IsNullOrEmpty(m_Id))
            {
                m_Id = "1";
            }

            m_Ip = XlspUtil.GetRequiredAttribute(el, "ip");
            m_Mask = XlspUtil.GetRequiredAttribute(el, "mask");
            m_Gateway = XlspUtil.GetRequiredAttribute(el, "gateway");
            m_Mac = XlspUtil.GetRequiredAttribute(el, "nic");
            m_NatIpStart = XlspUtil.GetRequiredAttribute(el, "natIpStart");
            m_NatIpEnd = XlspUtil.GetRequiredAttribute(el, "natIpEnd");

            IPAddress start = IPAddress.Parse(m_NatIpStart);
            IPAddress end = IPAddress.Parse(m_NatIpEnd);

            for (int i = 0; i < 3; i++)
            {
                if (start.GetAddressBytes()[i] != end.GetAddressBytes()[i])
                {
                    System.Console.WriteLine(String.Format("Error: Only last octet in IP range start ({0}) and end ({1}) are allowed to differ", m_NatIpEnd, m_NatIpStart));
                    return false;
                }
            }

            int startOctet = start.GetAddressBytes()[3];
            int endOctet = end.GetAddressBytes()[3];

            if (endOctet < startOctet)
            {
                System.Console.WriteLine(String.Format("Error: End of NAT IP range ({0}) is lower than start ({1})", m_NatIpEnd, m_NatIpStart));
                return false;
            }

            m_NumNatIps = endOctet - startOctet + 1;

            int MAX_NAT_IPS = 65;

            if (m_NumNatIps > MAX_NAT_IPS)
            {
                System.Console.WriteLine(String.Format("Error: NAT IP range ({0} to {1}) is too larger; can only include up to {2} IPs", 
                                                       m_NatIpEnd, m_NatIpStart, MAX_NAT_IPS));
                return false;
            }

            return true;        
        }
    }

    class XlspTitle
    {
        public string m_TitleId = "0xFFFFFFFF";
        public string m_UserData = "";

        public bool Reset(XmlElement el)
        {
            m_TitleId = XlspUtil.GetRequiredAttribute(el, "titleId");
            m_UserData = XlspUtil.GetOptionalAttribute(el, "userData");
            return true;
        }
    }

    class XlspServer
    {
        public string m_Id = "";
        public string m_Ip = "";
        public string m_Port = "";

        public bool Reset(XmlElement el)
        {
            m_Id = XlspUtil.GetRequiredAttribute(el, "id");
            m_Ip = XlspUtil.GetRequiredAttribute(el, "ip");
            m_Port = XlspUtil.GetRequiredAttribute(el, "port");
            return true;
        }
    }

    class XlspTitleServers
    {
        public Hashtable m_TitleByTitleId;
        public Hashtable m_ServerById;

        public bool Reset(string filename)
        {
            m_TitleByTitleId = new Hashtable();
            m_ServerById = new Hashtable();

            FileInfo finfo = new FileInfo(filename);

            XmlDocument doc = new XmlDocument();
            string uri = "file://" + finfo.FullName;
            doc.Load(uri);

            XmlElement docEl = doc.DocumentElement;
            if (null == docEl)
            {
                System.Console.WriteLine(String.Format("Error: File does not contain XlspTitleServers element"));
                return false;
            }

            XmlElement titlesEl = doc.DocumentElement["Titles"];
            if (null == titlesEl)
            {
                System.Console.WriteLine(String.Format("Error: File does not contain Titles element"));
                return false;
            }

            XmlElement titleEl = (XmlElement)titlesEl.FirstChild;
            for (; null != titleEl; titleEl = (XmlElement)titleEl.NextSibling)
            {
                XlspTitle title = new XlspTitle();
                if (!title.Reset(titleEl))
                {
                    System.Console.WriteLine(String.Format("Error: Error reading Title element"));
                    return false;
                }

                if (m_TitleByTitleId.Contains(title.m_TitleId))
                {
                    System.Console.WriteLine(String.Format("Error: Duplicate entry for titleId {0}", title.m_TitleId));
                    return false;
                }

                m_TitleByTitleId.Add(title.m_TitleId, title);
            }

            XmlElement serversEl = doc.DocumentElement["Servers"];
            if (null == serversEl)
            {
                System.Console.WriteLine(String.Format("Error: File does not contain Servers element"));
                return false;
            }

            XmlElement serverEl = (XmlElement)serversEl.FirstChild;
            for (; null != serverEl; serverEl = (XmlElement)serverEl.NextSibling)
            {
                XlspServer server = new XlspServer();
                if (!server.Reset(serverEl))
                {
                    System.Console.WriteLine(String.Format("Error: Error reading Server element"));
                    return false;
                }

                if (m_ServerById.Contains(server.m_Id))
                {
                    System.Console.WriteLine(String.Format("Error: Duplicate entry for server ID {0}", server.m_Id));
                    return false;
                }

                m_ServerById.Add(server.m_Id, server);
            }

            return true;
        }
    }

    class XlspSg
    {
        public string m_Name = "";
        public XlspSgInternetNic m_InternetNic;
        public XlspSgDatacenterNic m_DatacenterNic;
        public XlspTitleServers m_TitleServers;

        public bool Reset(XmlElement el, string defaultTsFilename)
        {
            m_InternetNic = new XlspSgInternetNic();
            m_DatacenterNic = new XlspSgDatacenterNic();
            m_TitleServers = new XlspTitleServers();

            m_Name = XlspUtil.GetRequiredAttribute(el, "name");

            if (!m_InternetNic.Reset(el["InternetNic"]))
            {
                System.Console.WriteLine(String.Format("Error: Failed to read InternetNic for SG '{0}'", m_Name));
                return false;
            }

            if (!m_DatacenterNic.Reset(el["DatacenterNic"]))
            {
                System.Console.WriteLine(String.Format("Error: Failed to read InternetNic for SG '{0}'", m_Name));
                return false;
            }

            string tsFilename = XlspUtil.GetOptionalAttribute(el, "tsFilename");
            if (string.IsNullOrEmpty(tsFilename))
            {
                tsFilename = defaultTsFilename;
            }

            if (!m_TitleServers.Reset(tsFilename))
            {
                System.Console.WriteLine(String.Format("Error: Failed to read titleservers for SG '{0}' from file {1}", m_Name, tsFilename ));
                return false;
            }

            return true;
        }
    }

    class XlspCluster
    {
        string m_Name = "<UNKNOWN>";
        string m_SiteId = "0x5454080A";
        string m_SgLoadBalancer = "";

        Hashtable m_TitlesByFilename;
        XlspProxy m_Proxy;
        Hashtable m_SgByName;

        StreamWriter m_StreamWriter;

        //Reads an XlspCluster from an XML file.
        public bool Reset(string filename)
        {
            System.Console.WriteLine(String.Format("Reading XLSP cluster definition from {0}", filename));

            m_Name = "";
            m_SiteId = "0x5454080A";
            m_SgLoadBalancer = "";
            m_TitlesByFilename = new Hashtable();
            m_Proxy = new XlspProxy();
            m_SgByName = new Hashtable(); ;

            XmlDocument doc = new XmlDocument();
            string uri = "file://" + filename;
            doc.Load(uri);

            XmlElement docElem = doc.DocumentElement;
            if (null == docElem)
            {
                System.Console.WriteLine(String.Format("Error: File does not contain XlspCluster element"));
                return false;
            }

            //Read cluster attributes
            m_Name = XlspUtil.GetRequiredAttribute(docElem, "name");

            string siteId = XlspUtil.GetOptionalAttribute(docElem, "siteId");
            if (string.IsNullOrEmpty(siteId))
            {
                m_SiteId = siteId;
            }

            string tsFilename = XlspUtil.GetRequiredAttribute(docElem, "tsFilename");

            m_SgLoadBalancer = XlspUtil.GetOptionalAttribute(docElem, "sgLoadBalancer");


            //Read proxy (required)
            XmlElement proxyElem = docElem["Proxy"];
            if (null == proxyElem)
            {
                System.Console.WriteLine(String.Format("Error: File does not contain required Proxy element"));
                return false;
            }
            if (!m_Proxy.Reset(proxyElem))
            {
                System.Console.WriteLine(String.Format("Error: Error reading XLSP proxy definition"));
                return false;
            }


            //Read list of SGs           
            XmlElement sgsElem = docElem["Sgs"];
            if (null != sgsElem)
            {
                XmlElement el = (XmlElement)sgsElem.FirstChild;
                for (; null != el; el = (XmlElement)el.NextSibling)
                {
                    XlspSg sg = new XlspSg();
                    if(!sg.Reset(el, tsFilename))
                    {
                        System.Console.WriteLine(String.Format("Error: Error reading SG definition"));
                        return false;
                    }

                    if (m_SgByName.Contains(sg.m_Name))
                    {
                        System.Console.WriteLine(String.Format("Error: Duplicate SG '{0}'", sg.m_Name));
                        return false;
                    }

                    m_SgByName.Add(sg.m_Name, sg);
                }
            }

            System.Console.WriteLine(String.Format("Successfully read XLSP cluster definition."));

            return true;
        }

        public XlspTitle ReadTitleXml(string filename)
        {
            System.Console.WriteLine(String.Format("Reading XLSP title definition from {0}", filename));

            XmlDocument doc = new XmlDocument();
            string uri = "file://" + filename;
            doc.Load(uri);

            XmlElement titleElem = doc.DocumentElement;
            if (null == titleElem)
            {
                System.Console.WriteLine(String.Format("Error: File does not contain XlspTitle element"));
                return null;
            }

            XlspTitle title = new XlspTitle();
            title.Reset(titleElem);

            return title;
        }

        void WriteLine(string text)
        {
            string s = text;
            m_StreamWriter.WriteLine(s);
        }

        public bool GenerateIniFiles(string outpath)
        {
            string dirname = outpath + string.Format("\\{0}", this.m_Name);
            DirectoryInfo dinfo = new DirectoryInfo(dirname);

            if (dinfo.Exists)
            {
                //Delete the directory
                string[] fileEntries = Directory.GetFiles(dinfo.FullName);
                foreach (string fileName in fileEntries)
                {
                    File.Delete(fileName);
                }

                string[] subdirEntries = Directory.GetDirectories(dinfo.FullName);
                foreach (string subdir in subdirEntries)
                {
                    string[] subfileEntries = Directory.GetFiles(subdir);
                    foreach (string fileName in subfileEntries)
                    {
                        File.Delete(fileName);
                    }
                    Directory.Delete(subdir);
                }

                Directory.Delete(dinfo.FullName);
            }

            Directory.CreateDirectory(dinfo.FullName);

            //Generate the ini files for all XLSP boxes in the cluster
            if (!GenerateProxyIniFile(dinfo.FullName))
            {
                System.Console.WriteLine(String.Format("Error: Failed to generate XLSP proxy config file"));
                return false;
            }

            if (!GenerateSgIniFiles(dinfo.FullName))
            {
                System.Console.WriteLine(String.Format("Error: Failed to generate XLSP SG config files"));
                return false;
            }

            return true;
        }

        public bool GenerateProxyIniFile(string outpath)
        {
            //Create a directory using the machine name
            string dirname = outpath + string.Format("\\{0}", m_Proxy.m_Name);
            DirectoryInfo dinfo = new DirectoryInfo(dirname);
            Directory.CreateDirectory(dinfo.FullName);
            
            //Open the file for writing
            string filename = dirname + "\\lsphttpd.ini";
            FileInfo finfo = new FileInfo(filename);

            if (finfo.Exists && finfo.IsReadOnly)
            {
                System.Console.WriteLine(String.Format("Error: {0} already exists and is read-only", finfo.FullName));
                return false;
            }

            m_StreamWriter = File.CreateText(finfo.FullName);
            //m_StreamWriter.NewLine = "\n";
            
            //Header
            WriteLine("; XLSP Proxy config file generated by XLSPGEN");
            WriteLine("");

            //[WebSg] section
            string envUrl;
            if (m_Proxy.m_Env == "partnernet")
            {
                envUrl = "websvc.part.xboxlive.com";
            }
            else if (m_Proxy.m_Env == "certnet")
            {
                envUrl = "131.107.60.4";
            }
            else if (m_Proxy.m_Env == "productionnet")
            {
                envUrl = "websvc.xboxlive.com";
            }
            else
            {
                System.Console.WriteLine(String.Format("Error: Invalid env '{0}'", m_Proxy.m_Env));
                return false;
            }

            WriteLine("[WebSg]");
            WriteLine(string.Format("HostName={0}", envUrl));
            WriteLine("");

            //[Bindings] section           
            WriteLine("[Bindings]");
            WriteLine(string.Format("Ip={0}", m_Proxy.m_Ip));
            WriteLine(string.Format("Port={0}", m_Proxy.m_Port));
            WriteLine("");

            //[Allow] section
            WriteLine("[Allow]");
            IDictionaryEnumerator iter = m_SgByName.GetEnumerator();
            int i = 1;
            while (iter.MoveNext())
            {
                WriteLine(string.Format("Ip{0}={1}", i, ((XlspSg)iter.Value).m_DatacenterNic.m_Ip));
                ++i;
            }

            m_StreamWriter.Close();

            return true;
        }

        public bool GenerateSgIniFiles(string outpath)
        {
            //We write an sgconfig.ini for each SG.  We'll probably want to create a directory 
            //for each, so the user doesn't have to rename all the files.            
            IDictionaryEnumerator iter = m_SgByName.GetEnumerator();
            while (iter.MoveNext())
            {
                XlspSg sg = (XlspSg)iter.Value;

                if (!GenerateSgIniFile(sg, outpath))
                {
                    System.Console.WriteLine(String.Format("Error: Error writing sgconfig.ini for SG '{0}'", sg.m_Name));
                    return false;
                }
            }

            return true;
        }

        public bool GenerateSgIniFile(XlspSg sg, string outpath)
        {
            //Create a directory using the machine name
            string dirname = outpath + string.Format("\\{0}", sg.m_Name);
            DirectoryInfo dinfo = new DirectoryInfo(dirname);
            Directory.CreateDirectory(dinfo.FullName);

            //Open the ini file.
            string filename = dirname + "\\sgconfig.ini";
            FileInfo finfo = new FileInfo(filename);

            if (finfo.Exists && finfo.IsReadOnly)
            {
                System.Console.WriteLine(String.Format("Error: {0} already exists and is read-only", finfo.FullName));
                return false;
            }

            m_StreamWriter = File.CreateText(finfo.FullName);

            //Header
            WriteLine("; XLSP Security Gateway config file generated by XLSPGEN");
            WriteLine("");

            //LspProxy
            WriteLine("LspProxy");
            WriteLine("{");
            WriteLine(string.Format("    Ip            {0}", m_Proxy.m_Ip));
            WriteLine(string.Format("    Port          {0}", m_Proxy.m_Port));
            WriteLine(string.Format("    SiteId        {0}", m_SiteId));
            WriteLine("}");
            WriteLine("");

            //Advertisement
            WriteLine("AdvertiseOnLive   1");

            if (!string.IsNullOrEmpty(m_SgLoadBalancer))
            {
                WriteLine("; Note: A load balancer sits in front of SGs, so its IP is advertised instead of the Internet NIC's");
                WriteLine(string.Format("AdvertiseIp       {0}", m_SgLoadBalancer));
            }
            else
            {
                WriteLine(string.Format("AdvertiseIp       {0}", sg.m_InternetNic.m_Ip));
            }

            //Compute the client limit
            WriteLine(string.Format("ClientLimit       {0}", sg.m_DatacenterNic.m_NumNatIps * 1000));
            WriteLine(string.Format("ClientPortLimit   {0}", 64));
            WriteLine("");

            //NetworkInterface (Internet)
            WriteLine("NetworkInterface");
            WriteLine("{");
            WriteLine("    Type          Internet");
            WriteLine(string.Format("    Ip            {0}", sg.m_InternetNic.m_Ip));
            WriteLine(string.Format("    IpMask        {0}", sg.m_InternetNic.m_Mask));
            WriteLine(string.Format("    IpGateway     {0}", sg.m_InternetNic.m_Gateway));
            WriteLine(string.Format("    Nic           {0}", sg.m_InternetNic.m_Mac));
            WriteLine("}");
            WriteLine("");

            //NetworkInterface (Datacenter)
            WriteLine("NetworkInterface");
            WriteLine("{");
            WriteLine("    Type          Datacenter");
            WriteLine(string.Format("    Id            {0}", sg.m_DatacenterNic.m_Id));
            WriteLine(string.Format("    IpMask        {0}", sg.m_DatacenterNic.m_Mask));
            WriteLine(string.Format("    IpGateway     {0}", sg.m_DatacenterNic.m_Gateway));
            WriteLine(string.Format("    Nic           {0}", sg.m_DatacenterNic.m_Mac));
            WriteLine(string.Format("    Ip            {0}", sg.m_DatacenterNic.m_NatIpStart));
            WriteLine(string.Format("    IpEnd         {0}", sg.m_DatacenterNic.m_NatIpEnd));
            WriteLine("}");
            WriteLine("");

            //Service
            WriteLine("Service");
            WriteLine("{");
            WriteLine(string.Format("    Id            {0}", m_SiteId));
            WriteLine(string.Format("    Name          ROCKSTAR"));
            WriteLine("}");
            WriteLine("");

            //Titles
            IDictionaryEnumerator iter = sg.m_TitleServers.m_TitleByTitleId.GetEnumerator();
            while (iter.MoveNext())
            {
                XlspTitle title = (XlspTitle)iter.Value;

                WriteLine("Title");
                WriteLine("{");
                WriteLine(string.Format("    TitleId       {0}", title.m_TitleId));
                WriteLine(string.Format("    UserData      \"{0}\"", title.m_UserData));
                WriteLine("}");
                WriteLine("");
            }

            //Servers
            iter = sg.m_TitleServers.m_ServerById.GetEnumerator();
            while (iter.MoveNext())
            {
                XlspServer server = (XlspServer)iter.Value;

                WriteLine("Server");
                WriteLine("{");
                WriteLine("    Service       ROCKSTAR");
                WriteLine(string.Format("    Id            {0}", server.m_Id));
                WriteLine("    Address");
                WriteLine("    {");
                WriteLine(string.Format("        InterfaceId    {0}", sg.m_DatacenterNic.m_Id));
                WriteLine(string.Format("        Ip             {0}", server.m_Ip));
                WriteLine(string.Format("        Port           {0}", server.m_Port));
                WriteLine("    }");
                WriteLine("}");
                WriteLine("");
            }

            //Event
            WriteLine("Event");
            WriteLine("{");
            WriteLine("    Id             default");
            WriteLine(string.Format("    IntervalSecs   1"));
            WriteLine("}");
            WriteLine("");

            m_StreamWriter.Close();

            return true;
        }
    }
 }
