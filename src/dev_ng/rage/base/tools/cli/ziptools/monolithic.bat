@echo off
setlocal
setlocal enabledelayedexpansion
if "%1"=="" echo %0 ps3 or %0 xenon
if "%1"=="" exit/b

cd /d t:\rdr2\game
del files_%1_layer0.txt
del files_%1_layer0_cache.txt
del files_%1_layer1.txt

echo Constructing response file layer 0 . . .
for %%f in (act.zip camera.zip content.zip fonts.zip mtlshaders.zip textures.zip tune.zip animationRes_%1.zip fragments*_%1.zip grassres*_%1.zip navres_%1.zip speedtreeres_%1.zip) do (
	unzip -lqq %%f | egrep -v "(\.hash\.log|\.inst|\.supercachemarker|\.masterList|terrainBnd.*txt|/$)" | cut -c29- >> files_%1_layer0.txt
	set FFF0=!FFF0! %%f
)

echo Constructing response file layer 0 cache . . .
for %%f in (terrain*_%1.zip) do (
	unzip -lqq %%f | egrep -v "(\.hash\.log|\.inst|\.supercachemarker|\.masterList|terrainBnd.*txt|/$)" | cut -c29- >> files_%1_layer0_cache.txt
	set FFF0_CACHE=!FFF0_CACHE! %%f
)

echo Constructing response file for layer 1 . . .
for %%f in (audio_%1.zip cutscene_%1.zip dustres_%1.zip mapres_%1.zip naturalmotion_%1.zip shaders_%1.zip waterres_%1.zip) do (
	unzip -lqq %%f | egrep -v "(\.hash\.log|\.inst|\.supercachemarker|\.masterList|terrainBnd.*txt|/$)" | cut -c29- >> files_%1_layer1.txt
	set FFF1=!FFF1! %%f
)

set FFF1=%FFF1% -mount /redemption/territory_swAll/

for %%f in (redemption\territory_swAll\*_%1.zip) do (
	unzip -lqq %%f | egrep -v "(\.hash\.log|\.inst|\.supercachemarker|\.masterList|terrainBnd.*txt|/$)" | cut -c29- | sed "s,^,redemption/territory_swAll/," >> files_%1_layer1.txt
	set FFF1=!FFF1! %%f
)

wc -l files_%1_layer*.txt
echo Building rpfs . . .

if %1==ps3 (
	makerpf T:\rdr2\PS3_GAME\USRDIR\rdr2_layer0.rpf files_%1_layer0.txt -temp c:\tempzip %FFF0%
	makerpf T:\rdr2\PS3_GAME\USRDIR\rdr2_layer0_cache.rpf files_%1_layer0_cache.txt -temp c:\tempzip %FFF0_CACHE%
	makerpf T:\rdr2\PS3_GAME\USRDIR\rdr2_layer1.rpf files_%1_layer1.txt -temp c:\tempzip %FFF1%
) else (
	makerpf rdr2_%1_layer0.rpf files_%1_layer0.txt -temp c:\tempzip %FFF0%
	makerpf rdr2_%1_layer0_cache.rpf files_%1_layer0_cache.txt -temp c:\tempzip %FFF0_CACHE%
	makerpf rdr2_%1_layer1.rpf files_%1_layer1.txt -temp c:\tempzip %FFF1%
)
