// 
// ziptools/makerpf.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "data/aes.h"
#include "file/direntry.h"
#include "file/packfile.h"
#include "file/stream.h"
#include "file/asset.h"		// for ASSET.FileName
#include "math/amath.h"
#include "system/endian.h"
#include "system/magicnumber.h"
#include "system/param.h"
#include "system/timer.h"
#include "zlib/zlib.h"
#include "diag/output.h"

#if !__TOOL
#error "I ought to be a tool!"
#endif

using namespace rage;

PARAM(noencryption,"Disable header encryption (not recommended, only for testing)");
PARAM(minimizepadding, "Reduce all padding to 256 bytes to fit more into in-memory archives");
PARAM(verbose, "Spew more about progress");
PARAM(mount, "Change internal zipfile mount point (can be specified more than once)");
XPARAM(aeskey);

static fiPackEntry *s_Entries;

#define SPEW	(!__OPTIMIZED)

const u32 ZIP_ENTRY_MAGIC = 0x02014b50;
const u32 ZIP_CENTRAL_MAGIC = 0x06054b50;

bool getnextfileinzip(char *dest,size_t ASSERT_ONLY(destSize),u32 &rpftime,fiStream *S,bool iszip) {
	if (!iszip) {
		bool result = fgetline(dest,destSize,S) != 0;
		rpftime = Rpf::ConvertFileTimeToRpfTime(fiDeviceLocal::GetInstance().GetFileTime(dest));
		return result;
	}

	int magic;
	while (S->ReadInt(&magic,1) && magic == ZIP_ENTRY_MAGIC) {
		char junk[32];
		char extra[256], comment[256];
		u16 filenameLen, extraLen, cmtLen, method;
		u32 compLen, uncompLen;
		int relOffs;
		S->Read(junk,6);
		S->ReadShort(&method,1);
		Rpf::MsDosTime time;
		Rpf::MsDosDate date;
		S->ReadShort((u16*)&time,1);
		S->ReadShort((u16*)&date,1);
		rpftime = Rpf::ZipTimeToRpfTime(time,date);
		S->Read(junk,4);
		S->ReadInt(&compLen,1);
		S->ReadInt(&uncompLen,1);
		S->ReadShort(&filenameLen,1);
		S->ReadShort(&extraLen,1);
		S->ReadShort(&cmtLen,1);
		S->Read(junk,2 + 2 + 4);
		S->ReadInt(&relOffs,1);
		Assert(filenameLen < destSize-1);
		Assert(extraLen < sizeof(extra));
		Assert(cmtLen < sizeof(comment));
		S->Read(dest,filenameLen); 
		bool isDir = dest[filenameLen-1] == '/';
		dest[filenameLen++] = 0;
		S->Read(extra,extraLen); // if (extraLen) Displayf("extraLen=%d",extraLen);
		S->Read(comment,cmtLen); // if (cmtLen) Displayf("cmtLen=%d",cmtLen);
		if (!isDir)
			return true;
	}
		return false;
}

static void add_garbage(void *dest,unsigned start,unsigned stop) 
{
	static u8 crap[16] = 
	{ 0xbe, 0x2a, 0xd7, 0x12, 0xed, 0x02, 0xf3, 0x53,
	  0xb9, 0x7e, 0xd4, 0xac, 0x8c, 0x7f, 0xfe, 0x7b };
	Assert(start <= stop);
	Assert(stop - start < 16);
	u8 *d = (u8*) dest;
	while (start < stop) {
		d[start] = crap[start & 15];
		++start;
	}
}

int main(int argc,char **argv) {

	if (argc < 3) {
		Displayf("usage: %s destfile responseFileOrZip [-keepnameheap] [-temp tempfilename] [-minimizepadding] [-maxsize maxsizeinKB] [file1.zip file2.zip [-mount dir] file3.zip...]", argv[0]);
		return 1;
	}

	sysParam::Init(argc, argv);

	u64 smallPadding = 8; // PARAM_minimizepadding.Get() ? 256 : 8;
	u64  largePadding = PARAM_minimizepadding.Get() ? 8 : 2048;		// was 128k
	int bulkOffsetShift = fiBulkOffsetShift;
	u64 resourcePadding = 256 << bulkOffsetShift;			// we could support a scale shift of up to 4 with this padding, but other alignment only goes to 2k.
	u64 largePaddingThreshold = (128 * 1024);
	bool verbose = PARAM_verbose.Get();

	// find any zipfiles anywhere on the command line (so that globbing is convenient)
	const char *zipfiles[256];
	int zipcount = 0;
	for (int i=1; i<argc; i++) {
		const char *ext = strrchr(argv[i],'.');
		if (ext && !stricmp(ext,".zip"))
			zipfiles[zipcount++] = argv[i];
		else if (!strcmp(argv[i],"-mount"))
			zipfiles[zipcount++] = argv[++i];
	}

	const char fakeMount[] = "/";
	const char *curMount = fakeMount;
	for (int i=0; i<zipcount; i++) {
		if (zipfiles[i][0]=='/')
			curMount = zipfiles[i];
		else  {
			fiPackfile *pf = rage_new fiPackfile;
			if (!pf->Init(zipfiles[i]))
				Quitf("Unable to load zipfile '%s'",zipfiles[i]);
			if (!pf->MountAs(curMount))
				Quitf("Unable to mount zipfile '%s'",zipfiles[i]);
		}
	}

	fiSafeStream S(fiStream::Open(argv[2]));
	if (!S) {
		Errorf("Unable to open response file %s",argv[2]);
		return 1;
	}

	// See if it's a zip file
	S->Seek(S->Size() - 22);
	u32 magic = 0;
	int respFileStart = 0;
	S->ReadInt(&magic,1);
	if (magic == ZIP_CENTRAL_MAGIC) {
		int junk[4];
		S->ReadInt(junk,sizeof(junk)/sizeof(int));
		respFileStart = junk[3];
	}
	S->Seek(respFileStart);

	DirEntry *root = NULL;
	char filename[256], fakename[256];
	u64 totalSize = 0, totalCompSize = 0;
	u64 compSize = sizeof(fiPackHeader) + sizeof(fiPackEntry);					// start with the rpf header and the root directory entry size.
	u64 estimatedPaddingSize = 0;			// only used with -maxsize option
	u64 estimatedWriteOffset = 0;			// only used with -maxsize option

	// First pass -- compute directory size.
	u32 timestamp;
	while (getnextfileinzip(filename,sizeof(filename),timestamp,S,magic == ZIP_CENTRAL_MAGIC)) {
		if (filename[0] == ';')	// comment character
			continue;
		if (filename[1] == ':')	{ // ignore drive letters
			Warningf("Ignoring name with drive letter: %s",filename);
			continue;
		}
		compSize += DirEntry::Insert(&root,filename[0]=='.'&&strchr("/\\",filename[1])?filename+2:filename,0,0,0,timestamp,NULL);
	}
	delete root;
	root = NULL;

	// Round to 2k
	u64 sectorMask = 2047;
	compSize = (compSize + sectorMask) & ~sectorMask;
	u64 allocatedDirectorySize = compSize;

	/// Displayf("Directory size is %uk",u32(compSize>>10));

	// Reset response file
	S->Seek(respFileStart);

	// let the AES decoder use the output path to determine which key to use.
	if (!PARAM_aeskey.Get())
		PARAM_aeskey.Set(argv[1]);

	fiSafeStream O(fiStream::Create(argv[1]));
	if (!O) {
		Errorf("%s: Cannot create!",argv[1]);
		return 1;
	}
	Displayf("Building archive '%s'...",argv[1]);
	// Seek to correct spot now in case the archive ends up being empty...
	O->Seek64(compSize);

	static char zeroes[128 * 1024];

	while (getnextfileinzip(filename,sizeof(filename),timestamp,S,magic == ZIP_CENTRAL_MAGIC)) {
		if (filename[0] == ';')	// comment character
			continue;
		if (filename[1] == ':')	{ // ignore drive letters
			Warningf("Ignoring name with drive letter: %s",filename);
			continue;
		}

		formatf(fakename,"%s%s",fakeMount,filename);
		if (!zipcount && fiDeviceLocal::GetInstance().GetAttributes(filename) & FILE_ATTRIBUTE_DIRECTORY)
			continue;
		fiStream *thisFile = fiStream::Open(zipcount? fakename : filename);
		if (!thisFile) {
			Errorf("Unable to open archive file %s",filename);
			continue;
		}
		
		u32 thisSize = thisFile->Size();
		if (!thisSize || thisSize >= (1U << 30)) {		// ignore huge files that are probably other archives!  (Also we can't stream a file larger than a gigabyte anyway)
			Warningf("Ignoring empty or huge file %s",filename);
			thisFile->Close();
			continue;
		}
		u8 *src = new u8[thisSize];
		thisFile->Read(src,thisSize);
		thisFile->Close();

		const char *prettyName = strrchr(filename,'\\');
		if (!prettyName)
			prettyName = strrchr(filename,'/');
		if (!prettyName)
			prettyName = filename;
		else
			++prettyName;

		u32 destSize;
		u8* dest;

		bool isResource = false;
		bool isOldResource = false;
		datResourceFileHeader hdr;

		if (thisSize >= sizeof(datResourceFileHeader)) {
			memcpy(&hdr,src,sizeof(hdr));
			isResource = 
				hdr.Magic == datResourceFileHeader::c_MAGIC_PS3 || sysEndian::Swap(hdr.Magic) == datResourceFileHeader::c_MAGIC_PS3 ||
				hdr.Magic == datResourceFileHeader::c_MAGIC_OTHER || sysEndian::Swap(hdr.Magic) == datResourceFileHeader::c_MAGIC_OTHER;
			isOldResource =
				hdr.Magic == datResourceFileHeader::c_MAGIC_PS3_OLD || sysEndian::Swap(hdr.Magic) == datResourceFileHeader::c_MAGIC_PS3_OLD ||
				hdr.Magic == datResourceFileHeader::c_MAGIC_OTHER_OLD || sysEndian::Swap(hdr.Magic) == datResourceFileHeader::c_MAGIC_OTHER_OLD;
			isResource |= isOldResource;

			if (isResource) {
				if (hdr.Magic != datResourceFileHeader::c_MAGIC_PS3 && hdr.Magic != datResourceFileHeader::c_MAGIC_OTHER &&
					hdr.Magic != datResourceFileHeader::c_MAGIC_PS3_OLD && hdr.Magic != datResourceFileHeader::c_MAGIC_OTHER_OLD) {
					hdr.Magic = sysEndian::Swap(hdr.Magic);
					hdr.Version = sysEndian::Swap(hdr.Version);
					(u32&)hdr.Info.h0 = sysEndian::Swap((u32&)hdr.Info.h0);	// sigh
					(u32&)hdr.Info.h1 = sysEndian::Swap((u32&)hdr.Info.h1);	// sigh				
				}
				if (isOldResource)
					hdr.Info.h1.IsNewResource = false;
			}
		}

		bool isAudio = (strchr(prettyName, '.') == NULL);
		bool isScript = !isAudio && !strcasecmp(strrchr(prettyName, '.'), ".sco");
		bool isStrTbl = !isAudio && !strcasecmp(strrchr(prettyName, '.'), ".strtbl");
		bool isCutBin = !isAudio && !strcasecmp(strrchr(prettyName, '.'), ".cutbin");

		totalSize += thisSize;
		if (!isResource && !isAudio && !isScript && !isStrTbl && !isCutBin) {
			destSize = thisSize * 2;
			dest = new u8[destSize];
			if (verbose)
				Printf("Compressing '%s'...",prettyName);

			z_stream c_stream;
			memset(&c_stream,0,sizeof(c_stream));

			int compressionQuality = Z_BEST_COMPRESSION;
			if (deflateInit2(&c_stream, compressionQuality,Z_DEFLATED,-MAX_WBITS,9,Z_DEFAULT_STRATEGY) < 0)
				Quitf("Error in deflateInit");

			c_stream.next_in = (Bytef*)src;
			c_stream.avail_in = (uInt) thisSize;
			c_stream.next_out = dest;
			c_stream.avail_out = (uInt) destSize;
			if (deflate(&c_stream, Z_FINISH) < 0)
				Quitf("Error in deflate");
			if (c_stream.avail_in)
				Quitf("deflate didn't consume all input?");
			deflateEnd(&c_stream);

			destSize = (u32) (destSize - c_stream.avail_out);
			if (destSize < thisSize) {
				if (verbose)
					Displayf("done (%.0f%%)",(float(thisSize-destSize)*100.0f)/thisSize);
				delete[] src;
			}
			else {
				destSize = thisSize;
				if (verbose)
					Displayf("done (stored)");
				delete[] dest;
				dest = src;
			}
		}
		else {
			if (verbose)
				Displayf("Storing %s '%s'...",isResource?"resource":isAudio?"audio file":isScript?"script file":isStrTbl?"stringtable":isCutBin?"cutscene xml":"generic",prettyName);
			dest = src;
			destSize = thisSize;
		}


		// Estimate the size of the final output data (including padding and directory entries) - required when using the -maxsize parameter
		// Don't need to include the directory size any longer, since we always assume worst-case in the header anyway.

		// estimate padding of output data
		u64 alignSize;
		if (isResource)
		{
			alignSize = (thisSize < largePaddingThreshold) ? smallPadding : largePadding;
			alignSize = Max(alignSize, resourcePadding);
		}
		else
			alignSize = (destSize < largePaddingThreshold) ? smallPadding : largePadding;
		u64 slop = alignSize - (estimatedWriteOffset & (alignSize - 1));
		if (slop != alignSize) {
			estimatedWriteOffset += slop;
			estimatedPaddingSize += slop;
		}

		totalCompSize += destSize;
		estimatedWriteOffset += destSize;

		u64 newCompSize = (compSize + alignSize) & ~(alignSize-1);
		if (newCompSize != compSize) {
			O->Write(zeroes,int(newCompSize - compSize));
			compSize = newCompSize;
		}
		if (O->Write(dest,destSize) != (int)destSize) {
			Errorf("Short write on temp file");
			return 1;
		}
#if SPEW
		Displayf("Wrote %s to offset 0x%" I64FMT "x, %u bytes",prettyName,compSize,destSize);
#endif
		// there are 31 bits (23 for resources) reserved, so make sure the actual size fits.
		Assert(compSize < (1ULL << (31 + bulkOffsetShift)));
		DirEntry::Insert(&root,filename[0]=='.'&&strchr("/\\",filename[1])?filename+2:filename,(u32)(compSize >> bulkOffsetShift),destSize,thisSize,timestamp,isResource?&hdr:NULL);
		delete[] dest;
		compSize += destSize;
	}

	u64 fileSizeMask = 2047;
	u64 newCompSize = (compSize + fileSizeMask) & ~fileSizeMask;
	O->Write(zeroes,int(newCompSize - compSize));
	compSize = newCompSize;

	Assertf(O->Tell64() == compSize,"padding discrepancy, %I64x != %I64x",O->Tell64(),compSize);
	char pp1[64], pp2[64];
	Displayf("%s total bytes read, %s bytes compressed, %.0f%% compression.",prettyprinter(pp1,sizeof(pp1),totalSize),prettyprinter(pp2,sizeof(pp2),totalCompSize),(float(totalSize-totalCompSize)*100.0f)/totalSize);

	// build name heap
	int entryCount = 0, nameHeapSize = 0;
	DirEntry::ComputeNameHeapSize(root,entryCount,nameHeapSize);
	++entryCount;		// include root directory
	
	unsigned entrySize = (entryCount * sizeof(fiPackEntry) + 15) & ~15;
	fiPackEntry *entries = (fiPackEntry*) new char[entrySize];

	unsigned metaSize = (entryCount) * sizeof(fiPackMetaData) + nameHeapSize + 2;
	metaSize = (metaSize + 15) & ~15;
	fiPackMetaData *meta = (fiPackMetaData*) new char [metaSize];

	// Make sure there's 2k worth of zeroes at the end so padding to a sector boundary is easy
	char *nameHeap = (char*)(meta + entryCount);
	int entryOffset = 1, heapOffset = 2;
	strcpy(nameHeap,"/");
	// Build root directory.
	memset(&entries[0],0,sizeof(entries[0]));
	entries[0].u.directory.m_DirectoryIndex = 1;
	entries[0].u.directory.m_IsDir = 1;
	entries[0].u.directory.m_DirectoryCount = DirEntry::BuildTree(root,entries,meta,nameHeap,entryOffset,heapOffset);

	meta[0].m_FileTime = 0;
	meta[0].m_NameOffset = 0;

	Assert(entryCount == entryOffset);
	delete root;

	// be utterly paranoid -- make sure there's no known data.
	// no idea if this is overkill or not.
	add_garbage(entries,entryCount * sizeof(fiPackEntry),entrySize);
	add_garbage(meta,entryCount * sizeof(fiPackMetaData) + nameHeapSize + 2,metaSize);

	nameHeapSize = heapOffset;

	// Compute total header size now
	u64 exactWriteOffset = sizeof(fiPackHeader) + entryCount*sizeof(fiPackEntry);
	exactWriteOffset = (exactWriteOffset + 15) & ~15;		// so encryption catches everything

	if (exactWriteOffset > allocatedDirectorySize)
		Quitf("This shouldn't happen, initial estimation was too conservative by %u bytes",exactWriteOffset - allocatedDirectorySize);

	// Build the output archive header

	bool encrypted = !PARAM_noencryption.Get();

	// Write the header
	fiPackHeader hdr;
	memset(&hdr,0,sizeof(hdr));
	hdr.m_Magic = MAKE_MAGIC_NUMBER('R','P','F','6');
	hdr.m_EntryCount = entryCount;
	hdr.m_MetaDataOffset = (u32)(compSize >> 3);

	AES aes;
	if (encrypted)
		hdr.m_Encrypted = aes.GetKeyId();

	// Convert headers to big-endian if necessary.
	Rpf::Convert_LE(&hdr.m_Magic,1);
	Rpf::Convert_BE(&hdr.m_EntryCount,3);
	Rpf::Convert_BE(&entries->m_NameHash,entryCount*5);
	Rpf::Convert_BE(&meta->m_NameOffset,entryCount*2);

	if (encrypted) {
		aes.Encrypt(&entries->m_NameHash,entrySize);
		aes.Encrypt(&meta->m_NameOffset,metaSize);
	}

	O->Seek64(0);
	O->Write(&hdr.m_Magic,sizeof(hdr));
	O->Write(&entries->m_NameHash,entrySize);
	O->Seek64(compSize);
	O->Write(&meta->m_NameOffset,metaSize);

	Displayf("%s total bytes written to RPF6 output archive.",prettyprinter(pp1,sizeof(pp1),compSize));

	return 0;
}
