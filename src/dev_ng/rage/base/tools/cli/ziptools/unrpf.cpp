// 
// ziptools/unrpf.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 
#include "file/asset.h"
#include "file/packfile.h"
#include "string/stringhash.h"

#include <direct.h>
#include "system/xtl.h"
#include <CommCtrl.h>
#include "system/nelem.h"

using namespace rage;

int (*Process)(const fiPackEntry &pe,const char *srcname);
bool ShowSize, ShowRscVersion, Quietly;
char OutputDir[256];
static fiPackfile *s_Pf;
static const fiPackEntry *s_Entries;
static fiHandle s_Handle;

int CopyFileFromArchive(const fiPackEntry &pe,const char *srcName)
{
	int result = 1;
	u32 size;
	u8 *buffer = s_Pf->ExtractFileToMemory(&pe - s_Entries,size);
	char outName[256];
	formatf(outName,"%s/%s",OutputDir,srcName);
	ASSET.CreateLeadingPath(outName);
	fiStream *S = ASSET.Create(outName,"");
	if (S)
	{
		S->Write(buffer, size);
		S->Close();
		result = 0;
	}
	else
	{
		Errorf("Unable to create '%s'",outName);
		result = 1;
	}
	delete[] buffer;
	return result;
}

int ViewFileInArchive(const fiPackEntry &pe,const char *srcname)
{
	if (ShowSize)
		Printf("%8u %8u ",pe.GetConsumedSize(),pe.GetUncompressedSize());
	if (ShowRscVersion)
	{
		if (pe.GetVersion())
			Printf("%3d(%02x) ",pe.GetVersion(),pe.GetVersion());
		else
			Printf("---(--) ");
	}

	Printf("%s\n",srcname);
	return 0;
}


static HWND s_List;
static const char *s_NameHeap;
static int s_NameShift;
const int NumColumns = 5;

inline u32 InMemorySize(const fiPackEntry &pe) { return pe.GetVersion()? pe.u.resource.m_Info.GetPhysicalSize() + pe.u.resource.m_Info.GetVirtualSize() : pe.GetUncompressedSize(); }

int AddToGui(const fiPackEntry &pe,const char *srcname)
{
	u32 uCount = SendMessage(s_List,LVM_GETITEMCOUNT,0,0);
	char buf[64];
	LVITEM itemData = {0};
	itemData.mask = LVIF_TEXT | LVIF_PARAM;
	itemData.iItem = uCount; 
	itemData.iSubItem = 0; 
	itemData.pszText = (LPSTR)(s_NameHeap + (pe.m_NameOffset << s_NameShift));
	itemData.cchTextMax = 0;
	itemData.iImage = NULL;
	itemData.lParam = (LPARAM)&pe;
	ListView_InsertItem(s_List,&itemData);
	// Subitems only support text, not param data
	ListView_SetItemText(s_List,uCount,1,formatf(buf,"%8u",pe.GetConsumedSize()));
	ListView_SetItemText(s_List,uCount,2,formatf(buf,"%8u",InMemorySize(pe)));
	ListView_SetItemText(s_List,uCount,3,formatf(buf,"%3d",pe.GetVersion()));
	ListView_SetItemText(s_List,uCount,4,(LPSTR)srcname);

	return 0;
}


static void Normalize(char *dest)
{
	while (*dest)
	{
		if (*dest == '\\')
			*dest = '/';
		else if (*dest >= 'A' && *dest <= 'Z')
			*dest += 32;
		++dest;
	}
}

// We support simplified globs that allow for up to one * anywhere in the glob string.
bool GlobMatch(const char *path,const char *glob)
{
	if (!strcmp(glob,"*"))
		return true;

	char pathTemp[256];
	safecpy(pathTemp,path);
	Normalize(pathTemp);
	size_t pathLen = strlen(pathTemp);

	const char *star = strchr(glob,'*');
	size_t globLen = strlen(glob) - 1;
	if (!star)
		return !strcmp(pathTemp,glob);
	else if (star == glob)	// At start
	{
		return globLen <= pathLen && !strncmp(path + pathLen - globLen,star+1,globLen);
	}
	else if (star[1] == 0)	// At end
	{
		return globLen <= pathLen && !strncmp(path,glob,globLen);
	}
	else	// Somewhere in middle
	{
		size_t before = star - glob;
		size_t after = glob + globLen - star;
		return before <= pathLen && !strncmp(path,glob,before) &&
			after <= pathLen && !strncmp(path + pathLen - after,star+1,after);
	}
}


int Recurse(u32 dirIndex,const char *match,const char *parentPath)
{
	u32 dirBase = s_Entries[dirIndex].u.directory.m_DirectoryIndex;
	u32 dirCount = s_Entries[dirIndex].u.directory.m_DirectoryCount;
	int result = 0;
	for (u32 i=dirBase; dirCount; i++,dirCount--) {
		char tempPath[256];
		safecpy(tempPath,parentPath);
		if (*tempPath)
			safecat(tempPath,"/");
		safecat(tempPath,s_NameHeap + (s_Entries[i].m_NameOffset << s_NameShift));

		if (s_Entries[i].IsDir())
			Recurse(i,match,tempPath);
		else {
			if (GlobMatch(tempPath,match))
				result += Process(s_Entries[i],tempPath);
		}
	}
	return result;
}

int orders[NumColumns] = { 1, 1, 1, 1, 1 };

int CALLBACK CompareFunc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort)
{
	const fiPackEntry *p1 = (fiPackEntry*) lParam1;
	const fiPackEntry *p2 = (fiPackEntry*) lParam2;
	switch (lParamSort) {
		case 0: return orders[lParamSort] * strcmp(s_NameHeap + (p1->m_NameOffset << s_NameShift),s_NameHeap + (p2->m_NameOffset << s_NameShift));
		case 1: return orders[lParamSort] * (p1->GetConsumedSize() - p2->GetConsumedSize());
		case 2: return orders[lParamSort] * (InMemorySize(*p1) - InMemorySize(*p2));
		case 3: return orders[lParamSort] * (p1->GetVersion() - p2->GetVersion());
		case 4:
			{
				char txtA[256], txtB[256];
				ListView_GetItemText(s_List,lParam1,4,txtA,sizeof(txtA));
				ListView_GetItemText(s_List,lParam2,4,txtB,sizeof(txtB));
				return orders[lParamSort] * strcmp(txtA, txtB);
			}
		default: return 0;
	}
}

LRESULT CALLBACK WindowProc(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
	if (msg == WM_NOTIFY) {
		NMHDR *hdr = (NMHDR*)lParam;
		if (hdr->code == LVN_COLUMNCLICK) {
			NMLISTVIEW *nm = (NMLISTVIEW*)lParam;
			if (nm->iSubItem == 4)
				ListView_SortItemsEx(s_List,CompareFunc,nm->iSubItem);
			else
				ListView_SortItems(s_List,CompareFunc,nm->iSubItem);
			orders[nm->iSubItem] = -orders[nm->iSubItem];		// toggle column sort order on each click
		}
		else if (hdr->code == NM_DBLCLK) {	// extract current file
			NMITEMACTIVATE *nm = (NMITEMACTIVATE*)lParam;
			LVITEM itemData = {0};
			itemData.mask = LVIF_PARAM;
			itemData.iItem = nm->iItem; 
			ListView_GetItem(s_List,&itemData);
			char path[256];
			char temp[1024];
			ListView_GetItemText(s_List,nm->iItem,4,path,sizeof(path));
			if (CopyFileFromArchive(*(fiPackEntry*)itemData.lParam,path))
				MessageBox(hwnd,path,"Error extracting file!",MB_ICONERROR|MB_OK);
			else
				MessageBox(hwnd,formatf(temp,"Extracted file %s to directory %s",path,OutputDir),"Complete.",MB_OK);
		}
		return 0;
	}
	else if (msg == WM_SIZE) {
		MoveWindow(s_List,0,0,LOWORD(lParam),HIWORD(lParam),true);
		return 0;
	}
	else if (msg == WM_CLOSE) {
		PostQuitMessage(0);
		return 0;
	}
	else
		return DefWindowProc(hwnd,msg,wParam,lParam);
}

int GuiLoop(const char *archive)
{
	fiPackfile pf;
	if (!pf.Init(archive,true,fiPackfile::CACHE_NONE)) 
	{
		MessageBox(NULL,"Cannot open file.",archive,MB_ICONERROR|MB_OK);
		return 1;
	}
	pf.SetRelativePath("");
	s_Pf = &pf;
	s_NameHeap = pf.GetNameHeap();
	s_Entries = pf.GetEntries();
	s_Handle = pf.GetPackfileHandle();
	s_NameShift = pf.GetNameShift();

	//InitCommonControls();

	WNDCLASSEX wc;
	memset(&wc,0,sizeof(wc));
	wc.cbSize        = sizeof(WNDCLASSEX);
	wc.style         = CS_DBLCLKS;
	wc.lpfnWndProc   = WindowProc;
	wc.hIcon         = LoadIcon(NULL, IDI_APPLICATION);
	wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)(COLOR_WINDOW+1);
	wc.lpszClassName = "UnRpf";
	wc.hIconSm       = LoadIcon(NULL, IDI_APPLICATION);
	RegisterClassEx(&wc);

	HWND hwnd = CreateWindowEx(WS_EX_CLIENTEDGE,
		"UnRpf",
		archive,
		WS_OVERLAPPEDWINDOW|WS_VISIBLE,
		CW_USEDEFAULT,CW_USEDEFAULT,
		600,400,
		NULL,NULL,GetModuleHandle(NULL),NULL);

	// Borrowed code from framework/tools/src/GTA_tools/libwin...
	RECT rectDim;
	GetClientRect(hwnd,&rectDim);

	s_List = CreateWindowEx(0,
		WC_LISTVIEW, 
		"", 
		LVS_EDITLABELS|WS_HSCROLL|WS_VSCROLL|WS_CLIPSIBLINGS|WS_CLIPCHILDREN|LVS_REPORT|LVS_SHOWSELALWAYS|WS_VISIBLE|WS_CHILD,
		rectDim.left,rectDim.top,
		rectDim.right,rectDim.bottom,
		hwnd,
		NULL,
		GetModuleHandle(NULL),
		NULL);

	static const char *columns[] = { "Name","Compressed","Uncompressed","Version","Full Path" };
	static u32 widths[] = { 180, 100, 100, 70, 200 };
	for (int i=0; i<NELEM(columns); i++) {
		LVCOLUMN colData;
		colData.mask = LVCF_FMT|LVCF_TEXT|LVCF_WIDTH; 
		colData.fmt = i&&i<4? LVCFMT_RIGHT : LVCFMT_LEFT; 
		colData.cx = widths[i]; 
		colData.pszText = (LPSTR) columns[i]; 
		colData.cchTextMax = strlen(colData.pszText); 
		SendMessage(s_List,LVM_INSERTCOLUMN,i,(LPARAM)&colData);
	}

	Process = AddToGui;
	Recurse(0,"*","");

	MSG msg;
	while (GetMessage(&msg,NULL,0,0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return 0;
}

int main(int argc,char **argv) 
{
	if (argc < 2) 
	{
		Displayf("Usage: unrpf rpffile (runs in GUI mode)");
		Displayf("       unrpf [command] [-o outputdir] zipfile [file1 file2...]");
		Displayf("Command is: -v[sv] - Display directory info (with sizes and/or rsc version)");
		Displayf("            -x[q] - Extract files (quietly)");
		Displayf("Simplified wildcards work; at most one * may appear in each one.");
		return 1;
	}

	_getcwd(OutputDir, sizeof(OutputDir));

	// Process command
	if (argv[1][0]=='-' && argv[1][1]=='v') 
	{
		Process = ViewFileInArchive;
		ShowSize = strchr(argv[1]+2,'s') != 0;
		ShowRscVersion = strchr(argv[1]+2,'v') != 0;
	}
	else if (argv[1][0]=='-' && argv[1][1]=='x')
	{
		Process = CopyFileFromArchive;
		if (argv[1][2] == 'q')
			Quietly = true;
	}
	else
		return GuiLoop(argv[1]);

	// Process output directory if specified
	if (!strcmp(argv[2],"-o"))
	{
		safecpy(OutputDir, argv[3]);
		argv+=2;
		argc-=2;
	}

	fiPackfile pf;
	if (!pf.Init(argv[2],true,fiPackfile::CACHE_NONE)) 
	{
		Errorf("Unable to open '%s'",argv[2]);
		return 1;
	}
	pf.SetRelativePath("");
	s_Pf = &pf;
	s_NameHeap = pf.GetNameHeap();
	s_Entries = pf.GetEntries();
	s_Handle = pf.GetPackfileHandle();
	s_NameShift = pf.GetNameShift();

	int errors = 0;

	if (argc > 3) 
	{
		for (int i=3; i<argc; i++) 
		{
			Normalize(argv[i]);
			errors += Recurse(0,argv[i],"");
		}
	}
	else
	{
		errors += Recurse(0,"*","");
	}

	if (errors)
		Errorf("%d errors during operations.",errors);
	return errors? 1 : 0;
}
