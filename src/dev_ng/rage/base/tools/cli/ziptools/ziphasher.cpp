// 
// /ziphasher.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "file/stream.h"
#include "string/stringhash.h"

#include <stdlib.h>

using namespace rage;

const u32 ZIP_ENTRY_MAGIC = 0x02014b50;
const u32 ZIP_CENTRAL_MAGIC = 0x06054b50;

bool getnextfileinzip(char *dest,size_t ASSERT_ONLY(destSize),fiStream *S) {
	int magic;
	while (S->ReadInt(&magic,1) && magic == ZIP_ENTRY_MAGIC) {
		char junk[32];
		char extra[256], comment[256];
		u16 filenameLen, extraLen, cmtLen, method;
		u32 compLen, uncompLen;
		int relOffs;
		S->Read(junk,6);
		S->ReadShort(&method,1);
		S->Read(junk,8);
		S->ReadInt(&compLen,1);
		S->ReadInt(&uncompLen,1);
		S->ReadShort(&filenameLen,1);
		S->ReadShort(&extraLen,1);
		S->ReadShort(&cmtLen,1);
		S->Read(junk,2 + 2 + 4);
		S->ReadInt(&relOffs,1);
		Assert(filenameLen < destSize-1);
		Assert(extraLen < sizeof(extra));
		Assert(cmtLen < sizeof(comment));
		S->Read(dest,filenameLen); 
		bool isDir = dest[filenameLen-1] == '/';
		dest[filenameLen++] = 0;
		S->Read(extra,extraLen); // if (extraLen) Displayf("extraLen=%d",extraLen);
		S->Read(comment,cmtLen); // if (cmtLen) Displayf("cmtLen=%d",cmtLen);
		if (!isDir)
			return true;
	}
	return false;
}


int main(int argc,char **argv)
{
	u32 hashMatch = 0;
	--argc;
	++argv;
	int result = 1;
	if (!strchr(argv[0],'.')) {
		hashMatch = (u32) strtoul(strncmp(argv[0],"hash_",5) ? argv[0] : argv[0] + 5,NULL,16);
		Displayf("Searching for hash code %x...",hashMatch);
		--argc;
		++argv;
	}
	for (;argc; --argc,++argv) {
		fiStream *S = fiStream::Open(argv[0]);
		if (S) {
			S->Seek(S->Size() - 22);
			u32 magic = 0;
			S->ReadInt(&magic,1);
			if (magic == ZIP_CENTRAL_MAGIC) {
				int junk[4];
				S->ReadInt(junk,sizeof(junk)/sizeof(int));
				S->Seek(junk[3]);	// offset of central dir
				char name[256];
				while (getnextfileinzip(name,sizeof(name),S)) {
					const char *slash = strrchr(name,'/');
					if (slash)
						++slash;
					else
						slash = name;
					u32 thisHash = atStringHash(slash);
					if (!hashMatch || hashMatch == thisHash) {
						Displayf("%08x %s %s",thisHash,argv[0],name);
						result = 0;
					}
				}
			}
			S->Close();
		}
	}
	return result;
}