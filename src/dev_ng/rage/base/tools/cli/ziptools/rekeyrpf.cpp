// 
// ziptools/rekeyrpf.cpp 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#include "data/aes.h"
#include "file/direntry.h"
#include "file/packfile.h"
#include "file/stream.h"
#include "file/asset.h"		// for ASSET.FileName
#include "math/amath.h"
#include "system/endian.h"
#include "system/magicnumber.h"
#include "system/param.h"
#include "system/timer.h"
#include "zlib/zlib.h"
#include "diag/output.h"

#if !__TOOL
#error "I ought to be a tool!"
#endif

using namespace rage;

struct fiPackHeaderOld {
	u32 m_Magic;			// 'RPF0'
	u32 m_TotalSize;		// sizeof(fiPackEntry)*m_EntryCount+m_NameHeapSize, rounded up to 2k boundary.
	u32 m_EntryCount;		// number of fiPackEntry structures
	u32 m_WastedSpace;		// when a packfile is patched, this tracks how much space is getting wasted.
	u32 m_Encrypted;		// nonzero if it's AES-encrypted.  Key is project-specific.
	char padding[2048-20];	// align to sector boundary
};

void convert_rpf(const char *filename) {
	fiStream *S = fiStream::Open(filename,false);
	if (!S) {
		Errorf("File '%s' cannot be opened for read/write access",filename);
		return;
	}
	fiPackHeaderOld h;
	const u32 RPF4 = MAKE_MAGIC_NUMBER('R','P','F','4');
	S->ReadInt((int*)&h,sizeof(h)/4);
	if (h.m_Magic != RPF4)
		Errorf("File '%s' isn't in RPF4 format.",filename);
	else if (!(h.m_Encrypted >> 28))
		Warningf("File '%s' is already in new format.",filename);
	else {
		AES aes_dec(h.m_Encrypted);
		AES aes_enc(h.m_Encrypted);
		char *buffer = rage_new char[h.m_TotalSize];
		S->Read(buffer,h.m_TotalSize);
		aes_dec.Decrypt(buffer,h.m_TotalSize);
		S->Seek(0);
		h.m_Encrypted &= AES_KEY_ID_MASK;
		S->WriteInt((int*)&h,sizeof(h)/4);
		aes_enc.Encrypt(buffer,h.m_TotalSize);
		S->Write(buffer,h.m_TotalSize);
		Displayf("File '%s' converted to new format.",filename);
	}
	S->Close();
}

int main(int argc,char **argv) 
{
	sysParam::Init(argc, argv);

	if (argc == 1) {
		char buf[256];
		while (gets(buf)) {
			const char *ext = strrchr(buf,'.');
			if (ext && !strcmp(ext,".rpf"))
				convert_rpf(buf);
			else
				Displayf("Ignoring %s...",buf);
		}
	}
	else for (int i=1; i<argc; i++)
		convert_rpf(argv[i]);
}
