// 
// ziptools/testcomp.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "file/compress.h"

#include "file/stream.h"
#include "system/timer.h"

using namespace rage;

#if !__TOOL
#error "Must be a TOOL build"
#endif

int main(int argc,char **argv) {
	if (argc < 2)
		return 1;

	fiStream *S = fiStream::Open(argv[1]);
	if (!S)
		return 1;
	int size = S->Size();
	u8 *data = new u8[size];
	if (S->Read(data,size) != size)
		return 1;
	S->Close();

	u8 *compressed = new u8[size+1024];
	sysTimer T;
	fiCompress(compressed,size+1024,data,size,15,true);
	Displayf("Operation complete in %5.2f seconds",T.GetTime());
}
