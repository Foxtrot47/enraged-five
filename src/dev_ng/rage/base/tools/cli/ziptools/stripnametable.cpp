// 
// /stripnametable.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 
#include "file/packfile.h"
#include "system/magicnumber.h"

using namespace rage;

int main(int argc,char **argv)
{
	const fiDevice &dev = fiDeviceLocal::GetInstance();
	if (argc != 2)
	{
		Displayf("usage: stripnametable filename.rpf");
		return 1;
	}
	fiHandle handle = dev.Open(argv[1],false);
	if (!fiIsValidHandle(handle))
	{
		Errorf("Unable to open '%s'",argv[1]);
		return 1;
	}
	fiPackHeader hdr;
	dev.Read(handle,&hdr,sizeof(hdr));
	Rpf::Convert_LE(&hdr.m_Magic,1);
	Rpf::Convert_BE(&hdr.m_EntryCount,3);
	if (hdr.m_Magic != MAKE_MAGIC_NUMBER('R','P','F','6'))
	{
		Errorf("File '%s' is not RPF6",argv[1]);
		dev.Close(handle);
		return 1;
	}
	if (hdr.m_MetaDataOffset == 0)
	{
		Warningf("File '%s' already had name heap removed.",argv[1]);
		dev.Close(handle);
		return 0;
	}

	dev.Seek64(handle,(u64)hdr.m_MetaDataOffset << 3,seekSet);
	dev.SetEndOfFile(handle);

	dev.Seek(handle,0,seekSet);
	hdr.m_MetaDataOffset = 0;
	Rpf::Convert_LE(&hdr.m_Magic,1);
	Rpf::Convert_BE(&hdr.m_EntryCount,3);
	dev.Write(handle,&hdr,sizeof(hdr));
	dev.Close(handle);
	Displayf("Removed nameheap from '%s'",argv[1]);
	return 0;
}