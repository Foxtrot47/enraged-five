#include <stdio.h>
#include <string.h>
#include <direct.h>

int main(int argc,char **argv)
{
	if (argc != 2) {
		fprintf(stderr,"usage: nmkdir dirname\n");
		fprintf(stderr,"creates all intermediate directories if necessary.\n");
		fprintf(stderr,"should work around XP DFS buf.\n");
		return 1;
	}
	
	char buf[1024];
	char buf2[1024];
	memset(buf2,0,sizeof(buf2));
	strcpy(buf,argv[1]);
	while (strchr(buf,'/'))
		*strchr(buf,'/') = '\\';

	char *scan = buf;
	while (scan = strchr(scan,'\\')) {
		memcpy(buf2,buf,scan-buf);
		_mkdir(buf2);
		++scan;
	}
	_mkdir(buf);
	return 0;
}

