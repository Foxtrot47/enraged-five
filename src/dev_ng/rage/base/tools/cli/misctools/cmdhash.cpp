// 
// /hashstring.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#include <cstdio>

#include "system/main.h"
#include "system/param.h"
#include "string/stringhash.h"
using namespace rage;

PARAM(literal, "Use atLiteralStringHash (case-sensitive)");
PARAM(hex, "Display hex output");
PARAM(name, "String name to hash");
PARAM(sign, "Hash value should be signed (not unsigned)");

int Main()
{
	bool hex = PARAM_hex.Get();	
	bool literal = PARAM_literal.Get();
	bool sign = PARAM_sign.Get();

	const char* name = NULL;
	PARAM_name.Get(name);

	if (!name)
	{
		puts("Usage: cmdhash.exe -name <name> [options]\n");
		puts("Options:");
		puts(" - hex      Display hex output instead of decimal");
		puts(" - literal  Use case-sensitive hash");
		puts(" - sign     Hash value should be signed (not unsigned)");
		return -1;
	}

	if (hex)
		printf("%X", (literal ? atLiteralStringHash(name) : atStringHash(name)));
	else
	{
		if (sign)
			printf("%d", (literal ? atLiteralStringHash(name) : atStringHash(name)));
		else
			printf("%u", (literal ? atLiteralStringHash(name) : atStringHash(name)));
	}		
	
	return 0;
}
