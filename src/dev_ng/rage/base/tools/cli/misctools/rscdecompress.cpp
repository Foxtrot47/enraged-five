// 
// /rscdecompress.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "data/resourceheader.h"
#include "file/limits.h"
#include "file/stream.h"
#include "system/endian.h"
#include "system/param.h"

#include "system/xtl.h"
#include "paging/xcompress_6995.h"

#include <stdio.h>

using namespace rage;

#pragma comment(lib,"xcompress6995.lib")

static inline void Swapper(size_t &x) {
	size_t s = (x>>24) | (x<<24) | ((x>>8)&0xFF00) | ((x<<8)&0xFF0000);
	x = s;
}

static inline size_t to_ps3(bool ps3,size_t value) {
	return ps3?
		(value * 43) >> 5
		: value;
}

int main(int argc,char **argv) {
	sysParam::Init(argc, argv);

	for (int i=1; i<argc; i++) {
		Printf("Decompressing %s\n", argv[i]);
		fiStream *S = fiStream::Open(argv[i]);
		Assertf(S,"Unable to open file %s for reading",argv[i]);
		if (S) {
			const char *ext = strrchr(argv[i],'.');
			const char *platform = "?????";
			bool swapped = true;
			if (ext && (ext[1]=='w'||ext[1]=='W')) {
				platform = "win32";
				swapped = false;
				AssertMsg(0, "win32 not supported");
			}
			else if (ext && (ext[1]=='x'||ext[1]=='X')) {
				platform = "xenon";
			}
			else if (ext && (ext[1]=='c'||ext[1]=='C')) {
				platform = "ps3";
				AssertMsg(0, "ps3 not supported");
			}
			datResourceFileHeader hdr;
			size_t compressedSize = S->Size() - sizeof(datResourceFileHeader);
			if (AssertVerify(S->Read(&hdr,sizeof(hdr)) == sizeof(hdr))) {
				if (swapped) {
					Swapper(hdr.Magic);
					Swapper((size_t&)hdr.Version);
					Swapper((size_t&)hdr.Info);
				}
				bool ps3 = hdr.Magic == datResourceFileHeader::c_MAGIC_PS3;
				Assertf(hdr.Magic == datResourceFileHeader::c_MAGIC_OTHER || ps3, "%s is not a valid resource file", argv[1]);
				if (hdr.Magic == datResourceFileHeader::c_MAGIC_OTHER || ps3) {
					char* compressedData = rage_new char[compressedSize];
					Assert(compressedData);
					AssertVerify(S->Read(compressedData,compressedSize)==int(compressedSize));

					size_t magic = *(size_t*)compressedData;
					if (swapped)
						Swapper(magic);
					AssertMsg(magic != XCOMPRESS_FILE_IDENTIFIER_LZXNATIVE,"Old-format Xcompress asset found, rebuild");
					if (magic != (XCOMPRESS_FILE_IDENTIFIER_LZXNATIVE + XCOMPRESS_LZXNATIVE_VERSION_MINOR) &&
						magic != (XCOMPRESS_FILE_IDENTIFIER_LZXNATIVE + 1)) // hack for compatible versions
						Quitf("file '%s' not in XCompress format", argv[i]);

					size_t finalSize = *((size_t*)compressedData+1);
					if (swapped)
						Swapper(finalSize);
					Assert(finalSize == compressedSize - 2*sizeof(u32));

					size_t uncompressedSize = hdr.Info.GetVirtualSize() + to_ps3(ps3,hdr.Info.GetPhysicalSize());
					char* uncompressedData = rage_new char[uncompressedSize];
					Assert(uncompressedData);

					if (hdr.Info.old.IsCompressed)
					{
						XMEMDECOMPRESSION_CONTEXT decompressionContext = NULL;
						XMEMCODEC_PARAMETERS_LZX codecParams = { 0, 128*1024, 512*1024 };
						if(FAILED(XMemCreateDecompressionContext(XMEMCODEC_LZX, (VOID*)&codecParams, XMEMCOMPRESS_STREAM, &decompressionContext)))
							Quitf("XMemCreateDecompressionContext failed");

						SIZE_T inSize = compressedSize - 2*sizeof(u32);
						SIZE_T outSize = uncompressedSize;
						if (FAILED(XMemDecompress(decompressionContext,uncompressedData,&outSize,compressedData+2*sizeof(u32),inSize)))
							Quitf("XMemDecompress failed.");

						XMemDestroyDecompressionContext(decompressionContext);
					}
					else
					{
						Assert(compressedSize == uncompressedSize);
						memcpy(uncompressedData,compressedData,compressedSize);
					}

					delete [] compressedData;

					if (swapped) {
						Swapper(hdr.Magic);
						Swapper((size_t&)hdr.Version);
						Swapper((size_t&)hdr.Info);
					}

					char newName[RAGE_MAX_PATH];
					formatf(newName, RAGE_MAX_PATH, "%s.uncompressed", argv[i]);
					fiStream *T = fiStream::Create(newName);
					Assertf(T,"Unable to open file %s for writing",newName);

					T->Write(&hdr,sizeof(hdr));
					AssertVerify(T->Write(uncompressedData,uncompressedSize)==int(uncompressedSize));
					T->Close();
					delete [] uncompressedData;
				}
			}
			S->Close();
		}
	}
}
