#include <stdio.h>
#include <string.h>
#include <stdlib.h>

bool parseTag(FILE *f,char *dest,size_t destSize) {
	for (;;) {
		int ch = getc(f);
		if (ch == EOF)
			return false;
		if (ch == '<') {
			*dest++ = ch;
			while ((ch = getc(f)) != '>') {
				if (destSize-- > 3)
					*dest++ = ch;
			}
			*dest++ = ch;
			*dest++ = 0;
			return true;
		}
	} 
}

int main(int argc,char **argv)
{
	const char *toolsroot = getenv("RS_TOOLSROOT");
	if (!toolsroot) {
		fprintf(stderr,"No RS_TOOLSROOT defined.\n");
		return 2;
	}
	char localName[256];
	sprintf(localName,"%s\\etc\\local.xml",toolsroot);
	FILE *localFile = fopen(localName,"rt");
	if (!localFile) {
		fprintf(stderr,"Unable to open %s.\n",localName);
		return 2;
	}
	for (int i=1; i<argc; i++)
		strupr(argv[i]);
	
	char targetBranch[64];
	char lineBuf[1024];
	bool in_target_branch = false;
	int enabled = 0;
	while (parseTag(localFile,lineBuf,sizeof(lineBuf))) {
		// puts(lineBuf);
		// <branches default="dev">
		// <branch name="dev">
		if (strstr(lineBuf,"branches default")) {
			sprintf(targetBranch,"<branch name=%s",lineBuf+18);
			// printf("looking for %s\n",targetBranch);
		}
		else if (strstr(lineBuf,"branch name")) {
			in_target_branch = !strcmp(lineBuf,targetBranch);
		}
		else if (in_target_branch && strstr(lineBuf,"enabled")) {
			if (argc == 1)
				puts(lineBuf);
			else if (strstr(lineBuf,"true")) {
				strupr(lineBuf);
				for (int i=1; i<argc; i++)
					if (strstr(lineBuf,argv[i]))
						enabled = 1;
			}
		}
	}
	return enabled;
}
