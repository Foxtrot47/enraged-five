//
// misctools/cik_isadisasm.cpp
//
// Copyright (C) 2013-2014 Rockstar Games.  All Rights Reserved.
//

#include <assert.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <algorithm>
#include <string>
#include <vector>

typedef signed   char       s8;
typedef unsigned char       u8;
typedef signed   short      s16;
typedef unsigned short      u16;
typedef signed   int        s32;
typedef unsigned int        u32;
typedef signed   __int64    s64;
typedef unsigned __int64    u64;
#define SIZETFMT "I64"


static bool g_Endpgm = false;
static u64 g_Address = 0;


template<unsigned X> struct FloorLog2
{
	enum { val = FloorLog2<(X>>1)>::val + 1 };
};

template<> struct FloorLog2<1>
{
	enum { val = 0 };
};


static u32 CountBits(u32 x)
{
	x = (x&0x55555555) + ((x>>1) &0x55555555);
	x = (x&0x33333333) + ((x>>2) &0x33333333);
	x = (x&0x0f0f0f0f) + ((x>>4) &0x0f0f0f0f);
	x = (x&0x00ff00ff) + ((x>>8) &0x00ff00ff);
	x = (x&0x0000ffff) + ((x>>16)&0x0000ffff);
	return x;
}


#define FIELD(NAME, MASK)                                                      \
	enum                                                                       \
	{                                                                          \
		NAME##_MASK         = MASK,                                            \
		NAME##_SHIFT        = FloorLog2<(MASK)&-(MASK)>::val,                  \
	}

#define GET_FIELD(DWORD, NAME)                                                 \
	(((DWORD)&NAME##_MASK)>>NAME##_SHIFT)

#define DUMP_FIELD(DWORD, NAME)                                                \
	out->Printf("  " #NAME " 0x%x\n", GET_FIELD((DWORD), NAME))


////////////////////////////////////////////////////////////////////////////////
// OutputColumns
////////////////////////////////////////////////////////////////////////////////
class OutputColumns
{
public:

	OutputColumns(const u32 *dataBegin, size_t numDwords)
		: m_data(dataBegin)
		, m_dataEnd(dataBegin+numDwords)
		, m_newline(true)
		, m_printedAddr(false)
	{
		assert(numDwords <= 2);
	}

	~OutputColumns()
	{
		if (m_data < m_dataEnd)
		{
			PrintAddr();
			Printf("\n");
		}
	}

	void Printf(const char *fmt, ...)
	{
		if (m_newline)
		{
			PrintAddr();
			const size_t numDwords = m_dataEnd - m_data;
			while (m_data < m_dataEnd)
			{
				printf("0x%08x ", *m_data++);
			}
			assert(numDwords <= 2);
			//                        "0x00000000 0x00000000 |"
			static const char str[] = "                      |";
			//                         0         1         2
			//                         01234567890123456789012
			static unsigned strlut[] = {0, 11, 22};
			printf("%s", str+strlut[numDwords]);
		}

		char buf[1024];
		va_list args;
		va_start(args, fmt);
		vsnprintf(buf, sizeof(buf), fmt, args);
		va_end(args);
		printf("%s", buf);
		size_t len = strlen(buf);
		m_newline = (len>0 && buf[len-1]=='\n');
	}

private:

	const u32 *m_data;
	const u32 *m_dataEnd;
	bool m_newline;
	bool m_printedAddr;

	void PrintAddr()
	{
		if (!m_printedAddr)
		{
			assert(m_data < m_dataEnd);
			printf("0x%05" SIZETFMT "x ", g_Address);
			g_Address += (size_t)m_dataEnd - (size_t)m_data;
			m_printedAddr = true;
		}
		else
		{
			printf("        ");
		}
	}
};

////////////////////////////////////////////////////////////////////////////////
// Sdst
////////////////////////////////////////////////////////////////////////////////
#define Sdst(sdst) SdstHelper(sdst).c_str()
static std::string SdstHelper(u32 sdst)
{
	char buf[16];
	if (sdst <= 103)
	{
		sprintf(buf, "SGPR%u", sdst);
	}
	else if (112 <= sdst && sdst <= 123)
	{
		sprintf(buf, "TTMP%u", sdst-112);
	}
	else switch (sdst)
	{
		case 106: return std::string("VCC_LO");
		case 107: return std::string("VCC_HI");
		case 108: return std::string("TBA_LO");
		case 109: return std::string("TBA_HI");
		case 110: return std::string("TMA_LO");
		case 111: return std::string("TMA_HI");
		case 124: return std::string("M0");
		case 126: return std::string("EXEC_LO");
		case 127: return std::string("EXEC_HI");
		default:  sprintf(buf, "???(%u)", sdst);
	}
	return std::string(buf);
}

////////////////////////////////////////////////////////////////////////////////
// Ssrc
////////////////////////////////////////////////////////////////////////////////
#define Ssrc(ssrc, k) SsrcHelper((ssrc),(k)).c_str()
static std::string SsrcHelper(u32 ssrc, u32 k)
{
	char buf[16];
	if (ssrc <= 103)
	{
		sprintf(buf, "SGPR%u", ssrc);
	}
	else if (112 <= ssrc && ssrc <= 123)
	{
		sprintf(buf, "TTMP%u", ssrc-112);
	}
	else if (129 <= ssrc && ssrc <= 192)
	{
		sprintf(buf, "%u", ssrc-128);
	}
	else if (193 <= ssrc && ssrc <= 208)
	{
		sprintf(buf, "-%u", ssrc-192);
	}
	else if (ssrc == 255)
	{
		sprintf(buf, "0x%08x", k);
	}
	else switch (ssrc)
	{
		case 104: return std::string("FLAT_SCRATCH_LO");
		case 105: return std::string("FLAT_SCRATCH_HI");
		case 106: return std::string("VCC_LO");
		case 107: return std::string("VCC_HI");
		case 108: return std::string("TBA_LO");
		case 109: return std::string("TBA_HI");
		case 110: return std::string("TMA_LO");
		case 111: return std::string("TMA_HI");
		case 124: return std::string("M0");
		case 126: return std::string("EXEC_LO");
		case 127: return std::string("EXEC_HI");
		case 128: return std::string("0");
		case 240: return std::string("0.5");
		case 241: return std::string("-0.5");
		case 242: return std::string("1.0");
		case 243: return std::string("-1.0");
		case 244: return std::string("2.0");
		case 245: return std::string("-2.0");
		case 246: return std::string("4.0");
		case 247: return std::string("-4.0");
		case 251: return std::string("VCCZ");
		case 252: return std::string("EXECZ");
		case 253: return std::string("SCC");
		case 254: return std::string("LDS_DIRECT");
		default:  sprintf(buf, "???(%u)", ssrc);
	}
	return std::string(buf);
}

////////////////////////////////////////////////////////////////////////////////
// VSsrc
////////////////////////////////////////////////////////////////////////////////
#define VSsrc(src,k) VSsrcHelper((src),(k)).c_str()
static std::string VSsrcHelper(u32 src, u32 k)
{
	if (src < 256)
	{
		return SsrcHelper(src, k);
	}
	else
	{
		char buf[16];
		sprintf(buf, "VGPR%u", src-256);
		return std::string(buf);
	}
}

////////////////////////////////////////////////////////////////////////////////
// Insn
////////////////////////////////////////////////////////////////////////////////
class Insn
{
public:
	virtual ~Insn() {}

	virtual unsigned GetNumDwords(const u32 *insn) = 0;

	virtual void Dump(OutputColumns *out, const u32 *insn) = 0;

protected:

	Insn(u32 dw0EncodingMask, u32 dw0EncodingValue);
};

#define INSN_CTOR(TYPE)                                                        \
	TYPE()                                                                     \
		: Insn(DW0_ENCODING_MASK, DW0_ENCODING_VALUE)                          \
	{                                                                          \
	}

////////////////////////////////////////////////////////////////////////////////
// Sop2Insn
////////////////////////////////////////////////////////////////////////////////
class Sop2Insn : public Insn
{
public:

	INSN_CTOR(Sop2Insn)

	unsigned GetNumDwords(const u32 *insn)
	{
		const u32 ssrc0 = GET_FIELD(*insn, DW0_SSRC0);
		const u32 ssrc1 = GET_FIELD(*insn, DW0_SSRC1);
		return 1 + (ssrc0==255 || ssrc1==255);
	}

	void Dump(OutputColumns *out, const u32 *insn)
	{
		out->Printf("SOP2   | ");
		const u32 dw0 = insn[0];
		const u32 dw1 = insn[1];
		const u32 ssrc0 = GET_FIELD(dw0, DW0_SSRC0);
		const u32 ssrc1 = GET_FIELD(dw0, DW0_SSRC1);
		const u32 sdst  = GET_FIELD(dw0, DW0_SDST);
		const u32 op    = GET_FIELD(dw0, DW0_OP);
		switch (op)
		{
			case 0:     out->Printf("S_ADD_U32                  %s, %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 1:     out->Printf("S_SUB_U32                  %s, %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 2:     out->Printf("S_ADD_I32                  %s, %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 3:     out->Printf("S_SUB_I32                  %s, %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 4:     out->Printf("S_ADDC_U32                 %s, %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 5:     out->Printf("S_SUBB_U32                 %s, %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 6:     out->Printf("S_MIN_I32                  %s, %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 7:     out->Printf("S_MIN_U32                  %s, %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 8:     out->Printf("S_MAX_I32                  %s, %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 9:     out->Printf("S_MAX_U32                  %s, %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 10:    out->Printf("S_CSELECT_B32              %s, %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 11:    out->Printf("S_CSELECT_B64              %s, %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 14:    out->Printf("S_AND_B32                  %s, %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 15:    out->Printf("S_AND_B64                  %s, %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 16:    out->Printf("S_OR_B32                   %s, %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 17:    out->Printf("S_OR_B64                   %s, %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 18:    out->Printf("S_XOR_B32                  %s, %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 19:    out->Printf("S_XOR_B64                  %s, %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 20:    out->Printf("S_ANDN2_B32                %s, %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 21:    out->Printf("S_ANDN2_B64                %s, %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 22:    out->Printf("S_ORN2_B32                 %s, %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 23:    out->Printf("S_ORN2_B64                 %s, %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 24:    out->Printf("S_NAND_B32                 %s, %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 25:    out->Printf("S_NAND_B64                 %s, %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 26:    out->Printf("S_NOR_B32                  %s, %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 27:    out->Printf("S_NOR_B64                  %s, %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 28:    out->Printf("S_XNOR_B32                 %s, %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 29:    out->Printf("S_XNOR_B64                 %s, %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 30:    out->Printf("S_LSHL_B32                 %s, %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 31:    out->Printf("S_LSHL_B64                 %s, %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 32:    out->Printf("S_LSHR_B32                 %s, %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 33:    out->Printf("S_LSHR_B64                 %s, %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 34:    out->Printf("S_ASHR_I32                 %s, %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 35:    out->Printf("S_ASHR_I64                 %s, %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 36:    out->Printf("S_BFM_B32                  %s, %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 37:    out->Printf("S_BFM_B64                  %s, %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 38:    out->Printf("S_MUL_I32                  %s, %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 39:    out->Printf("S_BFE_U32                  %s, %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 40:    out->Printf("S_BFE_I32                  %s, %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 41:    out->Printf("S_BFE_U64                  %s, %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 42:    out->Printf("S_BFE_I64                  %s, %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 43:    out->Printf("S_CBRANCH_G_FORK           %s, %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 44:    out->Printf("S_ABSDIFF_I32              %s, %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			default:    out->Printf("[op=%u ???]                %s, %s, %s\n", op, Sdst(sdst), Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
		}
	}

private:

	FIELD(DW0_SSRC0,        0x000000ff);
	FIELD(DW0_SSRC1,        0x0000ff00);
	FIELD(DW0_SDST,         0x007f0000);
	FIELD(DW0_OP,           0x3f800000);
	FIELD(DW0_ENCODING,     0xc0000000);
	enum{DW0_ENCODING_VALUE=0x80000000};
};

////////////////////////////////////////////////////////////////////////////////
// SopkInsn
////////////////////////////////////////////////////////////////////////////////
class SopkInsn : public Insn
{
public:

	INSN_CTOR(SopkInsn)

	unsigned GetNumDwords(const u32 *insn)
	{
		return 1;
	}

	void Dump(OutputColumns *out, const u32 *insn)
	{
		out->Printf("SOPK   | ");
		const u32 dw0 = insn[0];
		const u32 simm16 = GET_FIELD(dw0, DW0_SIMM16);
		const u32 sdst   = GET_FIELD(dw0, DW0_SDST);
		const u32 op     = GET_FIELD(dw0, DW0_OP);
		switch (op)
		{
			case 0:     out->Printf("S_MOVK_I32                 %s, %i\n", Sdst(sdst), (s16)simm16); break;
			case 2:     out->Printf("S_CMOVK_I32                %s, %i\n", Sdst(sdst), (s16)simm16); break;
			case 3:     out->Printf("S_CMPK_EQ_I32              %s, %i\n", Sdst(sdst), (s16)simm16); break;
			case 4:     out->Printf("S_CMPK_LG_I32              %s, %i\n", Sdst(sdst), (s16)simm16); break;
			case 5:     out->Printf("S_CMPK_GT_I32              %s, %i\n", Sdst(sdst), (s16)simm16); break;
			case 6:     out->Printf("S_CMPK_GE_I32              %s, %i\n", Sdst(sdst), (s16)simm16); break;
			case 7:     out->Printf("S_CMPK_LT_I32              %s, %i\n", Sdst(sdst), (s16)simm16); break;
			case 8:     out->Printf("S_CMPK_LE_I32              %s, %i\n", Sdst(sdst), (s16)simm16); break;
			case 9:     out->Printf("S_CMPK_EQ_U32              %s, %u\n", Sdst(sdst), (u16)simm16); break;
			case 10:    out->Printf("S_CMPK_LG_U32              %s, %u\n", Sdst(sdst), (u16)simm16); break;
			case 11:    out->Printf("S_CMPK_GT_U32              %s, %u\n", Sdst(sdst), (u16)simm16); break;
			case 12:    out->Printf("S_CMPK_GE_U32              %s, %u\n", Sdst(sdst), (u16)simm16); break;
			case 13:    out->Printf("S_CMPK_LT_U32              %s, %u\n", Sdst(sdst), (u16)simm16); break;
			case 14:    out->Printf("S_CMPK_LE_U32              %s, %u\n", Sdst(sdst), (u16)simm16); break;
			case 15:    out->Printf("S_ADDK_I32                 %s, %i\n", Sdst(sdst), (s16)simm16); break;
			case 16:    out->Printf("S_MULK_I32                 %s, %i\n", Sdst(sdst), (s16)simm16); break;
			case 17:    out->Printf("S_CBRANCH_I_FORK           %s, %i\n", Sdst(sdst), (s16)simm16); break;
			case 18:    out->Printf("S_GETREG_B32               %s, size=%u offset=%u hwRegId=%u\n", Sdst(sdst), (simm16&0x1f)+1, (simm16>>5)&0x15, (simm16>>10)&0x3f); break;
			case 19:    out->Printf("S_SETREG_B32               %s, size=%u offset=%u hwRegId=%u\n", Sdst(sdst), (simm16&0x1f)+1, (simm16>>5)&0x15, (simm16>>10)&0x3f); break;
			case 21:    out->Printf("S_SETREG_IMM32_B32         %s, size=%u offset=%u hwRegId=%u\n", Sdst(sdst), (simm16&0x1f)+1, (simm16>>5)&0x15, (simm16>>10)&0x3f); break;
			default:    out->Printf("[op=%u ???]                %s, 0x%04x\n", op, Sdst(sdst), simm16); break;
		}
	}

private:

	FIELD(DW0_SIMM16,       0x0000ffff);
	FIELD(DW0_SDST,         0x007f0000);
	FIELD(DW0_OP,           0x0f800000);
	FIELD(DW0_ENCODING,     0xf0000000);
	enum{DW0_ENCODING_VALUE=0xb0000000};
};

////////////////////////////////////////////////////////////////////////////////
// Sop1Insn
////////////////////////////////////////////////////////////////////////////////
class Sop1Insn : public Insn
{
public:

	INSN_CTOR(Sop1Insn)

	unsigned GetNumDwords(const u32 *insn)
	{
		const u32 ssrc0 = GET_FIELD(*insn, DW0_SSRC0);
		return 1 + (ssrc0==255);
	}

	void Dump(OutputColumns *out, const u32 *insn)
	{
		out->Printf("SOP1   | ");
		const u32 dw0 = insn[0];
		const u32 dw1 = insn[1];
		const u32 ssrc0 = GET_FIELD(dw0, DW0_SSRC0);
		const u32 op    = GET_FIELD(dw0, DW0_OP);
		const u32 sdst  = GET_FIELD(dw0, DW0_SDST);
		switch (op)
		{
			case 3:     out->Printf("S_MOV_B32                  %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1)); break;
			case 4:     out->Printf("S_MOV_B64                  %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1)); break;
			case 5:     out->Printf("S_CMOV_B32                 %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1)); break;
			case 6:     out->Printf("S_CMOV_B64                 %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1)); break;
			case 7:     out->Printf("S_NOT_B32                  %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1)); break;
			case 8:     out->Printf("S_NOT_B64                  %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1)); break;
			case 9:     out->Printf("S_WQM_B32                  %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1)); break;
			case 10:    out->Printf("S_WQM_B64                  %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1)); break;
			case 11:    out->Printf("S_BREV_B32                 %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1)); break;
			case 12:    out->Printf("S_BREV_B64                 %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1)); break;
			case 13:    out->Printf("S_BCNT0_I32_B32            %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1)); break;
			case 14:    out->Printf("S_BCNT0_I32_B64            %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1)); break;
			case 15:    out->Printf("S_BCNT1_I32_B32            %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1)); break;
			case 16:    out->Printf("S_BCNT1_I32_B64            %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1)); break;
			case 17:    out->Printf("S_FF0_I32_B32              %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1)); break;
			case 18:    out->Printf("S_FF0_I32_B64              %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1)); break;
			case 19:    out->Printf("S_FF1_I32_B32              %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1)); break;
			case 20:    out->Printf("S_FF1_I32_B64              %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1)); break;
			case 21:    out->Printf("S_FLBIT_I32_B32            %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1)); break;
			case 22:    out->Printf("S_FLBIT_I32_B64            %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1)); break;
			case 23:    out->Printf("S_FLBIT_I32                %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1)); break;
			case 24:    out->Printf("S_FLBIT_I32_I64            %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1)); break;
			case 25:    out->Printf("S_SEXT_I32_I8              %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1)); break;
			case 26:    out->Printf("S_SEXT_I32_I16             %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1)); break;
			case 27:    out->Printf("S_BITSET0_B32              %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1)); break;
			case 28:    out->Printf("S_BITSET0_B64              %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1)); break;
			case 29:    out->Printf("S_BITSET1_B32              %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1)); break;
			case 30:    out->Printf("S_BITSET1_B64              %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1)); break;
			case 31:    out->Printf("S_GETPC_B64                %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1)); break;
			case 32:    out->Printf("S_SETPC_B64                %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1)); g_Endpgm=true; break;
			case 33:    out->Printf("S_SWAPPC_B64               %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1)); break;
			case 34:    out->Printf("S_RFE_B64                  %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1)); break;
			case 36:    out->Printf("S_AND_SAVEEXEC_B64         %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1)); break;
			case 37:    out->Printf("S_OR_SAVEEXEC_B64          %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1)); break;
			case 38:    out->Printf("S_XOR_SAVEEXEC_B64         %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1)); break;
			case 39:    out->Printf("S_ANDN2_SAVEEXEC_B64       %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1)); break;
			case 40:    out->Printf("S_ORN2_SAVEEXEC_B64        %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1)); break;
			case 41:    out->Printf("S_NAND_SAVEEXEC_B64        %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1)); break;
			case 42:    out->Printf("S_NOR_SAVEEXEC_B64         %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1)); break;
			case 43:    out->Printf("S_XNOR_SAVEEXEC_B64        %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1)); break;
			case 44:    out->Printf("S_QUADMASK_B32             %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1)); break;
			case 45:    out->Printf("S_QUADMASK_B64             %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1)); break;
			case 46:    out->Printf("S_MOVRELS_B32              %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1)); break;
			case 47:    out->Printf("S_MOVRELS_B64              %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1)); break;
			case 48:    out->Printf("S_MOVRELD_B32              %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1)); break;
			case 49:    out->Printf("S_MOVRELD_B64              %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1)); break;
			case 50:    out->Printf("S_CBRANCH_JOIN             %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1)); break;
			case 52:    out->Printf("S_ABS_I32                  %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1)); break;
			case 53:    out->Printf("S_MOV_FED_B32              %s, %s\n", Sdst(sdst), Ssrc(ssrc0,dw1)); break;
			default:    out->Printf("[op=%u ???]                %s, %s\n", op, Sdst(sdst), Ssrc(ssrc0,dw1)); break;
		}
	}

private:

	FIELD(DW0_SSRC0,        0x000000ff);
	FIELD(DW0_OP,           0x0000ff00);
	FIELD(DW0_SDST,         0x007f0000);
	FIELD(DW0_ENCODING,     0xff800000);
	enum{DW0_ENCODING_VALUE=0xbe800000};
};

////////////////////////////////////////////////////////////////////////////////
// SopcInsn
////////////////////////////////////////////////////////////////////////////////
class SopcInsn : public Insn
{
public:

	INSN_CTOR(SopcInsn)

	unsigned GetNumDwords(const u32 *insn)
	{
		const u32 ssrc0 = GET_FIELD(*insn, DW0_SSRC0);
		const u32 ssrc1 = GET_FIELD(*insn, DW0_SSRC1);
		return 1 + (ssrc0==255 || ssrc1==255);
	}

	void Dump(OutputColumns *out, const u32 *insn)
	{
		out->Printf("SOPC   | ");
		const u32 dw0 = insn[0];
		const u32 dw1 = insn[1];
		const u32 ssrc0 = GET_FIELD(dw0, DW0_SSRC0);
		const u32 ssrc1 = GET_FIELD(dw0, DW0_SSRC1);
		const u32 op    = GET_FIELD(dw0, DW0_OP);
		switch (op)
		{
			case 0:     out->Printf("S_CMP_EQ_I32               %s, %s\n", Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 1:     out->Printf("S_CMP_LG_I32               %s, %s\n", Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 2:     out->Printf("S_CMP_GT_I32               %s, %s\n", Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 3:     out->Printf("S_CMP_GE_I32               %s, %s\n", Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 4:     out->Printf("S_CMP_LT_I32               %s, %s\n", Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 5:     out->Printf("S_CMP_LE_I32               %s, %s\n", Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 6:     out->Printf("S_CMP_EQ_U32               %s, %s\n", Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 7:     out->Printf("S_CMP_LG_U32               %s, %s\n", Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 8:     out->Printf("S_CMP_GT_U32               %s, %s\n", Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 9:     out->Printf("S_CMP_GE_U32               %s, %s\n", Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 10:    out->Printf("S_CMP_LT_U32               %s, %s\n", Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 11:    out->Printf("S_CMP_LE_U32               %s, %s\n", Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 12:    out->Printf("S_BITCMP0_B32              %s, %s\n", Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 13:    out->Printf("S_BITCMP1_B32              %s, %s\n", Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 14:    out->Printf("S_BITCMP0_B64              %s, %s\n", Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 15:    out->Printf("S_BITCMP1_B64              %s, %s\n", Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			case 16:    out->Printf("S_SETVSKIP                 %s, %s\n", Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
			default:    out->Printf("[op=%u ???]                %s, %s, %s\n", op, Ssrc(ssrc0,dw1), Ssrc(ssrc1,dw1)); break;
		}
	}

private:

	FIELD(DW0_SSRC0,        0x000000ff);
	FIELD(DW0_SSRC1,        0x0000ff00);
	FIELD(DW0_OP,           0x007f0000);
	FIELD(DW0_ENCODING,     0xff800000);
	enum{DW0_ENCODING_VALUE=0xbf000000};
};

////////////////////////////////////////////////////////////////////////////////
// SoppInsn
////////////////////////////////////////////////////////////////////////////////
class SoppInsn : public Insn
{
public:

	INSN_CTOR(SoppInsn)

	unsigned GetNumDwords(const u32 *insn)
	{
		return 1;
	}

	void Dump(OutputColumns *out, const u32 *insn)
	{
		out->Printf("SOPP   | ");
		const u32 dw0 = insn[0];
		const u32 simm16 = GET_FIELD(dw0, DW0_SIMM16);
		const u32 op     = GET_FIELD(dw0, DW0_OP);
		switch (op)
		{
			case 0:     out->Printf("S_NOP (x%u)\n", (simm16&7)+1); break;
			case 1:     out->Printf("S_ENDPGM\n"); g_Endpgm=true; break;
			case 2:     out->Printf("S_BRANCH                   0x%05" SIZETFMT "x\n", g_Address+((s64)(s16)simm16)*4); break;
			case 4:     out->Printf("S_CBRANCH_SCC0             0x%05" SIZETFMT "x\n", g_Address+((s64)(s16)simm16)*4); break;
			case 5:     out->Printf("S_CBRANCH_SCC1             0x%05" SIZETFMT "x\n", g_Address+((s64)(s16)simm16)*4); break;
			case 6:     out->Printf("S_CBRANCH_VCCZ             0x%05" SIZETFMT "x\n", g_Address+((s64)(s16)simm16)*4); break;
			case 7:     out->Printf("S_CBRANCH_VCCNZ            0x%05" SIZETFMT "x\n", g_Address+((s64)(s16)simm16)*4); break;
			case 8:     out->Printf("S_CBRANCH_EXECZ            0x%05" SIZETFMT "x\n", g_Address+((s64)(s16)simm16)*4); break;
			case 9:     out->Printf("S_CBRANCH_EXECNZ           0x%05" SIZETFMT "x\n", g_Address+((s64)(s16)simm16)*4); break;
			case 10:    out->Printf("S_BARRIER\n"); break;
			case 11:    out->Printf("S_SETKILL                  %u\n", simm16&1); break;
			case 12:    out->Printf("S_WAITCNT                  vmcount=%u, export/mem-write-data count=%u, LGKM_cnt=%u\n", simm16&15, (simm16>>4)&7, (simm16>>8)&31); break;
			case 13:    out->Printf("S_SETHALT                  %s\n", (simm16&1)?"halt":"resume"); break;
			case 14:    out->Printf("S_SLEEP                    %u\n", (simm16&3)*64); break;
			case 15:    out->Printf("S_SETPRIO                  %u\n", simm16); break;
			case 16:    out->Printf("S_SENDMSG                  0x%04x\n", simm16); break;
			case 17:    out->Printf("S_SENDMSGHALT              0x%04x\n", simm16); break;
			case 18:    out->Printf("S_TRAP                     0x02x\n", simm16); break;
			case 19:    out->Printf("S_ICACHE_INV\n"); break;
			case 20:    out->Printf("S_INCPERFLEVEL             %u\n", simm16&15); break;
			case 21:    out->Printf("S_DECPERFLEVEL             %u\n", simm16&15); break;
			case 22:    out->Printf("S_TTRACEDATA\n"); break;
			case 23:    out->Printf("S_CBRANCH_CDBGSYS          0x%05" SIZETFMT "x\n", g_Address+((s64)(s16)simm16)*4); break;
			case 24:    out->Printf("S_CBRANCH_CDBGUSER         0x%05" SIZETFMT "x\n", g_Address+((s64)(s16)simm16)*4); break;
			case 25:    out->Printf("S_CBRANCH_CDBGSYS_OR_USER  0x%05" SIZETFMT "x\n", g_Address+((s64)(s16)simm16)*4); break;
			case 26:    out->Printf("S_CBRANCH_CDBGSYS_AND_USER 0x%05" SIZETFMT "x\n", g_Address+((s64)(s16)simm16)*4); break;
			default:    out->Printf("[op=%u ???]                0x%04x\n", op, simm16); break;
		}
	}

private:

	FIELD(DW0_SIMM16,       0x0000ffff);
	FIELD(DW0_OP,           0x007f0000);
	FIELD(DW0_ENCODING,     0xff800000);
	enum{DW0_ENCODING_VALUE=0xbf800000};
};

////////////////////////////////////////////////////////////////////////////////
// SmrdInsn
////////////////////////////////////////////////////////////////////////////////
class SmrdInsn : public Insn
{
public:

	INSN_CTOR(SmrdInsn)

	unsigned GetNumDwords(const u32 *insn)
	{
		const u32 dw0 = insn[0];
		const u32 offset = GET_FIELD(dw0, DW0_OFFSET);
		const u32 imm    = GET_FIELD(dw0, DW0_IMM);
		return 1 + (imm==0 && offset==255);
	}

	void Dump(OutputColumns *out, const u32 *insn)
	{
		out->Printf("SMRD   | ");

		const u32 dw0 = insn[0];
		const u32 dw1 = insn[1];
		const u32 offset = GET_FIELD(dw0, DW0_OFFSET);
		const u32 imm    = GET_FIELD(dw0, DW0_IMM);
		const u32 sbase  = GET_FIELD(dw0, DW0_SBASE);
		const u32 sdst   = GET_FIELD(dw0, DW0_SDST);
		const u32 op     = GET_FIELD(dw0, DW0_OP);

		// S_LOAD_* use 2 sgprs, and S_BUFFER_LOAD_* use 4 sgprs.
		const u32 numSbaseSgprs = (op<=4) ? 2 : 4;

		char addr[128];
		if (imm)
		{
			sprintf(addr, "SGPR[%u:%u] + 0x%x", sbase*2, sbase*2+numSbaseSgprs-1, offset*4);
		}
		else
		{
			sprintf(addr, "SGPR[%u:%u] + %s", sbase*2, sbase*2+numSbaseSgprs-1, Ssrc(imm,dw1));
		}

		switch (op)
		{
			case 0:     out->Printf("S_LOAD_DWORD               %s, %s\n", Sdst(sdst), addr); break;
			case 1:     out->Printf("S_LOAD_DWORDX2             %s, %s\n", Sdst(sdst), addr); break;
			case 2:     out->Printf("S_LOAD_DWORDX4             %s, %s\n", Sdst(sdst), addr); break;
			case 3:     out->Printf("S_LOAD_DWORDX8             %s, %s\n", Sdst(sdst), addr); break;
			case 4:     out->Printf("S_LOAD_DWORDX16            %s, %s\n", Sdst(sdst), addr); break;
			case 8:     out->Printf("S_BUFFER_LOAD_DWORD        %s, %s\n", Sdst(sdst), addr); break;
			case 9:     out->Printf("S_BUFFER_LOAD_DWORDX2      %s, %s\n", Sdst(sdst), addr); break;
			case 10:    out->Printf("S_BUFFER_LOAD_DWORDX4      %s, %s\n", Sdst(sdst), addr); break;
			case 11:    out->Printf("S_BUFFER_LOAD_DWORDX8      %s, %s\n", Sdst(sdst), addr); break;
			case 12:    out->Printf("S_BUFFER_LOAD_DWORDX16     %s, %s\n", Sdst(sdst), addr); break;
			case 29:    out->Printf("S_DCACHE_INV_VOL\n"); break;
			case 30:    out->Printf("S_MEMTIME                  %s\n", Sdst(sdst)); break;
			case 31:    out->Printf("S_DCACHE_INV\n"); break;
			default:    out->Printf("[op=%u ???]                %s, %s\n", op, Sdst(sdst), addr); break;
		}
	}

private:

	FIELD(DW0_OFFSET,       0x000000ff);
	FIELD(DW0_IMM,          0x00000100);
	FIELD(DW0_SBASE,        0x00007e00);
	FIELD(DW0_SDST,         0x003f8000);
	FIELD(DW0_OP,           0x07c00000);
	FIELD(DW0_ENCODING,     0xf8000000);
	enum{DW0_ENCODING_VALUE=0xc0000000};
};

////////////////////////////////////////////////////////////////////////////////
// Vop2Insn
////////////////////////////////////////////////////////////////////////////////
class Vop2Insn : public Insn
{
public:

	INSN_CTOR(Vop2Insn)

	unsigned GetNumDwords(const u32 *insn)
	{
		const u32 dw0  = *insn;
		const u32 src0 = GET_FIELD(dw0, DW0_SRC0);
		const u32 op   = GET_FIELD(dw0, DW0_OP);
		return 1 + ((src0==255) || (op==32/*V_MADMK_F32*/) || (op==33/*V_MADAK_F32*/));
	}

	void Dump(OutputColumns *out, const u32 *insn)
	{
		out->Printf("VOP2   | ");

		const u32   dw0   = insn[0];
		const u32   dw1   = insn[1];
		const float dw1f  = ((float*)insn)[1];
		const u32   src0  = GET_FIELD(dw0, DW0_SRC0);
		const u32   vsrc1 = GET_FIELD(dw0, DW0_VSRC1);
		const u32   vdst  = GET_FIELD(dw0, DW0_VDST);
		const u32   op    = GET_FIELD(dw0, DW0_OP);
		switch (op)
		{
			case 0:     out->Printf("V_CNDMASK_B32              VGPR%u, %s, VGPR%u\n", vdst, VSsrc(src0,dw1), vsrc1); break;
			case 1:     out->Printf("V_READLANE_B32             VGPR%u, %s, VGPR%u\n", vdst, VSsrc(src0,dw1), vsrc1); break;
			case 2:     out->Printf("V_WRITELANE_B32            VGPR%u, %s, VGPR%u\n", vdst, VSsrc(src0,dw1), vsrc1); break;
			case 3:     out->Printf("V_ADD_F32                  VGPR%u, %s, VGPR%u\n", vdst, VSsrc(src0,dw1), vsrc1); break;
			case 4:     out->Printf("V_SUB_F32                  VGPR%u, %s, VGPR%u\n", vdst, VSsrc(src0,dw1), vsrc1); break;
			case 5:     out->Printf("V_SUBREV_F32               VGPR%u, %s, VGPR%u\n", vdst, VSsrc(src0,dw1), vsrc1); break;
			case 6:     out->Printf("V_MAC_LEGACY_F32           VGPR%u, %s, VGPR%u\n", vdst, VSsrc(src0,dw1), vsrc1); break;
			case 7:     out->Printf("V_MUL_LEGACY_F32           VGPR%u, %s, VGPR%u\n", vdst, VSsrc(src0,dw1), vsrc1); break;
			case 8:     out->Printf("V_MUL_F32                  VGPR%u, %s, VGPR%u\n", vdst, VSsrc(src0,dw1), vsrc1); break;
			case 9:     out->Printf("V_MUL_I32_I24              VGPR%u, %s, VGPR%u\n", vdst, VSsrc(src0,dw1), vsrc1); break;
			case 10:    out->Printf("V_MUL_HI_I32_I24           VGPR%u, %s, VGPR%u\n", vdst, VSsrc(src0,dw1), vsrc1); break;
			case 11:    out->Printf("V_MUL_U32_U24              VGPR%u, %s, VGPR%u\n", vdst, VSsrc(src0,dw1), vsrc1); break;
			case 12:    out->Printf("V_MUL_HI_U32_U24           VGPR%u, %s, VGPR%u\n", vdst, VSsrc(src0,dw1), vsrc1); break;
			case 13:    out->Printf("V_MIN_LEGACY_F32           VGPR%u, %s, VGPR%u\n", vdst, VSsrc(src0,dw1), vsrc1); break;
			case 14:    out->Printf("V_MAX_LEGACY_F32           VGPR%u, %s, VGPR%u\n", vdst, VSsrc(src0,dw1), vsrc1); break;
			case 15:    out->Printf("V_MIN_F32                  VGPR%u, %s, VGPR%u\n", vdst, VSsrc(src0,dw1), vsrc1); break;
			case 16:    out->Printf("V_MAX_F32                  VGPR%u, %s, VGPR%u\n", vdst, VSsrc(src0,dw1), vsrc1); break;
			case 17:    out->Printf("V_MIN_I32                  VGPR%u, %s, VGPR%u\n", vdst, VSsrc(src0,dw1), vsrc1); break;
			case 18:    out->Printf("V_MAX_I32                  VGPR%u, %s, VGPR%u\n", vdst, VSsrc(src0,dw1), vsrc1); break;
			case 19:    out->Printf("V_MIN_U32                  VGPR%u, %s, VGPR%u\n", vdst, VSsrc(src0,dw1), vsrc1); break;
			case 20:    out->Printf("V_MAX_U32                  VGPR%u, %s, VGPR%u\n", vdst, VSsrc(src0,dw1), vsrc1); break;
			case 21:    out->Printf("V_LSHR_B32                 VGPR%u, %s, VGPR%u\n", vdst, VSsrc(src0,dw1), vsrc1); break;
			case 22:    out->Printf("V_LSHRREV_B32              VGPR%u, %s, VGPR%u\n", vdst, VSsrc(src0,dw1), vsrc1); break;
			case 23:    out->Printf("V_ASHR_I32                 VGPR%u, %s, VGPR%u\n", vdst, VSsrc(src0,dw1), vsrc1); break;
			case 24:    out->Printf("V_ASHRREV_I32              VGPR%u, %s, VGPR%u\n", vdst, VSsrc(src0,dw1), vsrc1); break;
			case 25:    out->Printf("V_LSHL_B32                 VGPR%u, %s, VGPR%u\n", vdst, VSsrc(src0,dw1), vsrc1); break;
			case 26:    out->Printf("V_LSHLREV_B32              VGPR%u, %s, VGPR%u\n", vdst, VSsrc(src0,dw1), vsrc1); break;
			case 27:    out->Printf("V_AND_B32                  VGPR%u, %s, VGPR%u\n", vdst, VSsrc(src0,dw1), vsrc1); break;
			case 28:    out->Printf("V_OR_B32                   VGPR%u, %s, VGPR%u\n", vdst, VSsrc(src0,dw1), vsrc1); break;
			case 29:    out->Printf("V_XOR_B32                  VGPR%u, %s, VGPR%u\n", vdst, VSsrc(src0,dw1), vsrc1); break;
			case 30:    out->Printf("V_BFM_B32                  VGPR%u, %s, VGPR%u\n", vdst, VSsrc(src0,dw1), vsrc1); break;
			case 31:    out->Printf("V_MAC_F32                  VGPR%u, %s, VGPR%u\n", vdst, VSsrc(src0,dw1), vsrc1); break;
			case 32:    out->Printf("V_MADMK_F32                VGPR%u, %s, VGPR%u, %f\n", vdst, VSsrc(src0,dw1), vsrc1, dw1f); break;
			case 33:    out->Printf("V_MADAK_F32                VGPR%u, %s, VGPR%u, %f\n", vdst, VSsrc(src0,dw1), vsrc1, dw1f); break;
			case 34:    out->Printf("V_BCNT_U32_B32             VGPR%u, %s, VGPR%u\n", vdst, VSsrc(src0,dw1), vsrc1); break;
			case 35:    out->Printf("V_MBCNT_LO_U32_B32         VGPR%u, %s, VGPR%u\n", vdst, VSsrc(src0,dw1), vsrc1); break;
			case 36:    out->Printf("V_MBCNT_HI_U32_B32         VGPR%u, %s, VGPR%u\n", vdst, VSsrc(src0,dw1), vsrc1); break;
			case 37:    out->Printf("V_ADD_I32                  VGPR%u, %s, VGPR%u\n", vdst, VSsrc(src0,dw1), vsrc1); break;
			case 38:    out->Printf("V_SUB_I32                  VGPR%u, %s, VGPR%u\n", vdst, VSsrc(src0,dw1), vsrc1); break;
			case 39:    out->Printf("V_SUBREV_I32               VGPR%u, %s, VGPR%u\n", vdst, VSsrc(src0,dw1), vsrc1); break;
			case 40:    out->Printf("V_ADDC_U32                 VGPR%u, %s, VGPR%u\n", vdst, VSsrc(src0,dw1), vsrc1); break;
			case 41:    out->Printf("V_SUBB_U32                 VGPR%u, %s, VGPR%u\n", vdst, VSsrc(src0,dw1), vsrc1); break;
			case 42:    out->Printf("V_SUBBREV_U32              VGPR%u, %s, VGPR%u\n", vdst, VSsrc(src0,dw1), vsrc1); break;
			case 43:    out->Printf("V_LDEXP_F32                VGPR%u, %s, VGPR%u\n", vdst, VSsrc(src0,dw1), vsrc1); break;
			case 44:    out->Printf("V_CVT_PKACCUM_U8_F32       VGPR%u, %s, VGPR%u\n", vdst, VSsrc(src0,dw1), vsrc1); break;
			case 45:    out->Printf("V_CVT_PKNORM_I16_F32       VGPR%u, %s, VGPR%u\n", vdst, VSsrc(src0,dw1), vsrc1); break;
			case 46:    out->Printf("V_CVT_PKNORM_U16_F32       VGPR%u, %s, VGPR%u\n", vdst, VSsrc(src0,dw1), vsrc1); break;
			case 47:    out->Printf("V_CVT_PKRTZ_F16_F32        VGPR%u, %s, VGPR%u\n", vdst, VSsrc(src0,dw1), vsrc1); break;
			case 48:    out->Printf("V_CVT_PK_U16_U32           VGPR%u, %s, VGPR%u\n", vdst, VSsrc(src0,dw1), vsrc1); break;
			case 49:    out->Printf("V_CVT_PK_I16_I32           VGPR%u, %s, VGPR%u\n", vdst, VSsrc(src0,dw1), vsrc1); break;
			default:    out->Printf("[op=%u ???]                VGPR%u, %s, VGPR%u\n", op, vdst, VSsrc(src0,dw1), vsrc1); break;
		}
	}

private:

	FIELD(DW0_SRC0,         0x000001ff);
	FIELD(DW0_VSRC1,        0x0001fe00);
	FIELD(DW0_VDST,         0x01fe0000);
	FIELD(DW0_OP,           0x7e000000);
	FIELD(DW0_ENCODING,     0x80000000);
	enum{DW0_ENCODING_VALUE=0x00000000};
};

////////////////////////////////////////////////////////////////////////////////
// Vop1Insn
////////////////////////////////////////////////////////////////////////////////
class Vop1Insn : public Insn
{
public:

	INSN_CTOR(Vop1Insn)

	unsigned GetNumDwords(const u32 *insn)
	{
		const u32 src0 = GET_FIELD(*insn, DW0_SRC0);
		return 1 + (src0==255);
	}

	void Dump(OutputColumns *out, const u32 *insn)
	{
		out->Printf("VOP1   | ");

		const u32 dw0 = insn[0];
		const u32 dw1 = insn[1];
		const u32 src0  = GET_FIELD(dw0, DW0_SRC0);
		const u32 vdst  = GET_FIELD(dw0, DW0_VDST);
		const u32 op    = GET_FIELD(dw0, DW0_OP);
		switch (op)
		{
			case 0:     out->Printf("V_NOP\n"); break;
			case 1:     out->Printf("V_MOV_B32                  VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 2:     out->Printf("V_READFIRSTLANE_B32        VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 3:     out->Printf("V_CVT_I32_F64              VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 4:     out->Printf("V_CVT_F64_I32              VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 5:     out->Printf("V_CVT_F32_I32              VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 6:     out->Printf("V_CVT_F32_U32              VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 7:     out->Printf("V_CVT_U32_F32              VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 8:     out->Printf("V_CVT_I32_F32              VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 9:     out->Printf("V_MOV_FED_B32              VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 10:    out->Printf("V_CVT_F16_F32              VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 11:    out->Printf("V_CVT_F32_F16              VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 12:    out->Printf("V_CVT_RPI_I32_F32          VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 13:    out->Printf("V_CVT_FLR_I32_F32          VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 14:    out->Printf("V_CVT_OFF_F32_I4           VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 15:    out->Printf("V_CVT_F32_F64              VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 16:    out->Printf("V_CVT_F64_F32              VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 17:    out->Printf("V_CVT_F32_UBYTE0           VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 18:    out->Printf("V_CVT_F32_UBYTE1           VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 19:    out->Printf("V_CVT_F32_UBYTE2           VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 20:    out->Printf("V_CVT_F32_UBYTE3           VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 21:    out->Printf("V_CVT_U32_F64              VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 22:    out->Printf("V_CVT_F64_U32              VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 32:    out->Printf("V_FRACT_F32                VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 33:    out->Printf("V_TRUNC_F32                VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 34:    out->Printf("V_CEIL_F32                 VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 35:    out->Printf("V_RNDNE_F32                VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 36:    out->Printf("V_FLOOR_F32                VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 37:    out->Printf("V_EXP_F32                  VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 38:    out->Printf("V_LOG_CLAMP_F32            VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 39:    out->Printf("V_LOG_F32                  VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 40:    out->Printf("V_RCP_CLAMP_F32            VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 41:    out->Printf("V_RCP_LEGACY_F32           VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 42:    out->Printf("V_RCP_F32                  VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 43:    out->Printf("V_RCP_IFLAG_F32            VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 44:    out->Printf("V_RSQ_CLAMP_F32            VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 45:    out->Printf("V_RSQ_LEGACY_F32           VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 46:    out->Printf("V_RSQ_F32                  VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 47:    out->Printf("V_RCP_F64                  VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 48:    out->Printf("V_RCP_CLAMP_F64            VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 49:    out->Printf("V_RSQ_F64                  VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 50:    out->Printf("V_RSQ_CLAMP_F64            VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 51:    out->Printf("V_SQRT_F32                 VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 52:    out->Printf("V_SQRT_F64                 VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 53:    out->Printf("V_SIN_F32                  VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 54:    out->Printf("V_COS_F32                  VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 55:    out->Printf("V_NOT_B32                  VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 56:    out->Printf("V_BFREV_B32                VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 57:    out->Printf("V_FFBH_U32                 VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 58:    out->Printf("V_FFBL_B32                 VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 59:    out->Printf("V_FFBH_I32                 VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 60:    out->Printf("V_FREXP_EXP_I32_F64        VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 61:    out->Printf("V_FREXP_MANT_F64           VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 62:    out->Printf("V_FRACT_F64                VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 63:    out->Printf("V_FREXP_EXP_I32_F32        VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 64:    out->Printf("V_FREXP_MANT_F32           VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 65:    out->Printf("V_CLREXCP                  VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 66:    out->Printf("V_MOVRELD_B32              VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 67:    out->Printf("V_MOVRELS_B32              VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			case 68:    out->Printf("V_MOVRELSD_B32             VGPR%u, %s\n", vdst, VSsrc(src0,dw1)); break;
			default:    out->Printf("[op=%u ???]                VGPR%u, %s\n", op, vdst, VSsrc(src0,dw1)); break;
		}
	}

private:

	FIELD(DW0_SRC0,         0x000001ff);
	FIELD(DW0_OP,           0x0001fe00);
	FIELD(DW0_VDST,         0x01fe0000);
	FIELD(DW0_ENCODING,     0xfe000000);
	enum{DW0_ENCODING_VALUE=0x7e000000};
};

////////////////////////////////////////////////////////////////////////////////
// VopcMnemonic
////////////////////////////////////////////////////////////////////////////////
static const char *VopcMnemonic(u32 op)
{
	bool unknown = false;
	static char fullInsnBuf[] = "                          ";
	const char *fullInsn = fullInsnBuf;
	if (op < 0x80)
	{
		static const char *const insnLut[8] =
		{
			"CMP",
			"CMPX",
			"CMP",
			"CMPX",
			"CMPS",
			"CMPSX",
			"CMPS",
			"CMPSX",
		};
		static const char *const operatorLut[16] =
		{
			"F",
			"LT",
			"EQ",
			"LE",
			"GT",
			"LG",
			"GE",
			"O",
			"U",
			"NGE",
			"NLG",
			"NGT",
			"NLE",
			"NEQ",
			"NLT",
			"TRU",
		};
		static const char *const typeLut[8] =
		{
			"F32",
			"F32",
			"F64",
			"F64",
			"F32",
			"F32",
			"F64",
			"F64",
		};
		size_t len = sprintf(fullInsnBuf, "V_%s_%s_%s", insnLut[op>>4], operatorLut[op&15], typeLut[op>>4]);
		assert(len < sizeof(fullInsnBuf)-1);
		fullInsnBuf[len] = ' ';
	}
	else
	{
		static const char *const insnLut[8] =
		{
			"CMP",
			"CMPX",
			"CMP",
			"CMPX",
			"CMP",
			"CMPX",
			"CMP",
			"CMPX",
		};
		static const char *const operatorLut[16] =
		{
			"F",
			"LT",
			"EQ",
			"LE",
			"GT",
			"LG",
			"GE",
			"TRU",
		};
		static const char *const typeLut[8] =
		{
			"I32",
			"I32",
			"I64",
			"I64",
			"U32",
			"U32",
			"U64",
			"U64",
		};
		if ((op&8) == 0)
		{
			size_t len = sprintf(fullInsnBuf, "V_%s_%s_%s", insnLut[(op-0x80)>>4], operatorLut[op&7], typeLut[(op-0x80)>>4]);
			assert(len < sizeof(fullInsnBuf)-1);
			fullInsnBuf[len] = ' ';
		}
		else
		{
			switch (op)
			{
				case 0x88: fullInsn = "V_CMP_CLASS_F32           "; break;
				case 0x98: fullInsn = "V_CMPX_CLASS_F32          "; break;
				case 0xa8: fullInsn = "V_CMP_CLASS_F64           "; break;
				case 0xb8: fullInsn = "V_CMPX_CLASS_F64          "; break;
				default:
					unknown = true;
			}
		}
	}
	return unknown ? NULL : fullInsn;
}

////////////////////////////////////////////////////////////////////////////////
// VopcInsn
////////////////////////////////////////////////////////////////////////////////
class VopcInsn : public Insn
{
public:

	INSN_CTOR(VopcInsn)

	unsigned GetNumDwords(const u32 *insn)
	{
		const u32 src0 = GET_FIELD(*insn, DW0_SRC0);
		return 1 + (src0==255);
	}

	void Dump(OutputColumns *out, const u32 *insn)
	{
		out->Printf("VOPC   | ");

		const u32 dw0 = insn[0];
		const u32 src0  = GET_FIELD(dw0, DW0_SRC0);
		const u32 vsrc1 = GET_FIELD(dw0, DW0_VSRC1);
		const u32 op    = GET_FIELD(dw0, DW0_OP);

		const char *const mnemonic = VopcMnemonic(op);

		if (mnemonic)
		{
			out->Printf("%s %s, VGPR%u\n", mnemonic, VSsrc(src0,insn[1]), vsrc1);
		}
		else
		{
			DUMP_FIELD(dw0, DW0_SRC0);
			DUMP_FIELD(dw0, DW0_VSRC1);
			DUMP_FIELD(dw0, DW0_OP);
		}
	}

private:

	FIELD(DW0_SRC0,         0x000001ff);
	FIELD(DW0_VSRC1,        0x0001fe00);
	FIELD(DW0_OP,           0x01fe0000);
	FIELD(DW0_ENCODING,     0xfe000000);
	enum{DW0_ENCODING_VALUE=0x7c000000};
};

////////////////////////////////////////////////////////////////////////////////
// Vop3aInsn
////////////////////////////////////////////////////////////////////////////////
class Vop3aInsn : public Insn
{
public:

	INSN_CTOR(Vop3aInsn)

	unsigned GetNumDwords(const u32 *insn)
	{
		return 2;
	}

	void Dump(OutputColumns *out, const u32 *insn)
	{
		out->Printf("VOP3a  | ");

		const u32 dw0 = insn[0];
		const u32 dw1 = insn[1];
		const u32 vdst  = GET_FIELD(dw0, DW0_VDST);
		const u32 abs   = GET_FIELD(dw0, DW0_ABS);
		const u32 clamp = GET_FIELD(dw0, DW0_CLAMP);
		const u32 op    = GET_FIELD(dw0, DW0_OP);
		const u32 src0  = GET_FIELD(dw1, DW1_SRC0);
		const u32 src1  = GET_FIELD(dw1, DW1_SRC1);
		const u32 src2  = GET_FIELD(dw1, DW1_SRC2);
		const u32 omod  = GET_FIELD(dw1, DW1_OMOD);
		const u32 neg   = GET_FIELD(dw1, DW1_NEG);

		const char *const neg0 = (neg&1) ? "-":"";
		const char *const neg1 = (neg&2) ? "-":"";
		const char *const neg2 = (neg&4) ? "-":"";

		char args[128];
		sprintf(args, "VGPR%u, %s%s, %s%s, %s%s (abs=%u, clamp=%u, omod=%u)", vdst, neg0, VSsrc(src0,0), neg1, VSsrc(src1,0), neg2, VSsrc(src2,0), abs, clamp, omod);

		if (op < 256)
		{
			const char *const mnemonic = VopcMnemonic(op);
			if (mnemonic)
			{
				out->Printf("%s %s\n", mnemonic, args);
			}
			else
			{
				out->Printf("compare\n");
				DUMP_FIELD(insn[0], DW0_VDST);
				DUMP_FIELD(insn[0], DW0_ABS);
				DUMP_FIELD(insn[0], DW0_CLAMP);
				DUMP_FIELD(insn[0], DW0_RESERVED);
				DUMP_FIELD(insn[0], DW0_OP);
				DUMP_FIELD(insn[1], DW1_SRC0);
				DUMP_FIELD(insn[1], DW1_SRC1);
				DUMP_FIELD(insn[1], DW1_SRC2);
				DUMP_FIELD(insn[1], DW1_OMOD);
				DUMP_FIELD(insn[1], DW1_NEG);
			}
		}
		else
		{
			switch (op)
			{
				case 256:   out->Printf("V_CNDMASK_B32              %s\n", args); break;
				case 257:   out->Printf("V_READLANE_B32             %s\n", args); break;
				case 258:   out->Printf("V_WRITELANE_B32            %s\n", args); break;
				case 259:   out->Printf("V_ADD_F32                  %s\n", args); break;
				case 260:   out->Printf("V_SUB_F32                  %s\n", args); break;
				case 261:   out->Printf("V_SUBREV_F32               %s\n", args); break;
				case 262:   out->Printf("V_MAC_LEGACY_F32           %s\n", args); break;
				case 263:   out->Printf("V_MUL_LEGACY_F32           %s\n", args); break;
				case 264:   out->Printf("V_MUL_F32                  %s\n", args); break;
				case 265:   out->Printf("V_MUL_I32_I24              %s\n", args); break;
				case 266:   out->Printf("V_MUL_HI_I32_I24           %s\n", args); break;
				case 267:   out->Printf("V_MUL_U32_U24              %s\n", args); break;
				case 268:   out->Printf("V_MUL_HI_U32_U24           %s\n", args); break;
				case 269:   out->Printf("V_MIN_LEGACY_F32           %s\n", args); break;
				case 270:   out->Printf("V_MAX_LEGACY_F32           %s\n", args); break;
				case 271:   out->Printf("V_MIN_F32                  %s\n", args); break;
				case 272:   out->Printf("V_MAX_F32                  %s\n", args); break;
				case 273:   out->Printf("V_MIN_I32                  %s\n", args); break;
				case 274:   out->Printf("V_MAX_I32                  %s\n", args); break;
				case 275:   out->Printf("V_MIN_U32                  %s\n", args); break;
				case 276:   out->Printf("V_MAX_U32                  %s\n", args); break;
				case 277:   out->Printf("V_LSHR_B32                 %s\n", args); break;
				case 278:   out->Printf("V_LSHRREV_B32              %s\n", args); break;
				case 279:   out->Printf("V_ASHR_I32                 %s\n", args); break;
				case 280:   out->Printf("V_ASHRREV_I32              %s\n", args); break;
				case 281:   out->Printf("V_LSHL_B32                 %s\n", args); break;
				case 282:   out->Printf("V_LSHLREV_B32              %s\n", args); break;
				case 283:   out->Printf("V_AND_B32                  %s\n", args); break;
				case 284:   out->Printf("V_OR_B32                   %s\n", args); break;
				case 285:   out->Printf("V_XOR_B32                  %s\n", args); break;
				case 286:   out->Printf("V_BFM_B32                  %s\n", args); break;
				case 287:   out->Printf("V_MAC_F32                  %s\n", args); break;
				case 288:   out->Printf("V_MADMK_F32                %s\n", args); break;
				case 289:   out->Printf("V_MADAK_F32                %s\n", args); break;
				case 290:   out->Printf("V_BCNT_U32_B32             %s\n", args); break;
				case 291:   out->Printf("V_MBCNT_LO_U32_B32         %s\n", args); break;
				case 292:   out->Printf("V_MBCNT_HI_U32_B32         %s\n", args); break;
				case 299:   out->Printf("V_LDEXP_F32                %s\n", args); break;
				case 300:   out->Printf("V_CVT_PKACCUM_U8_F32       %s\n", args); break;
				case 301:   out->Printf("V_CVT_PKNORM_I16_F32       %s\n", args); break;
				case 302:   out->Printf("V_CVT_PKNORM_U16_F32       %s\n", args); break;
				case 303:   out->Printf("V_CVT_PKRTZ_F16_F32        %s\n", args); break;
				case 304:   out->Printf("V_CVT_PK_U16_U32           %s\n", args); break;
				case 305:   out->Printf("V_CVT_PK_I16_I32           %s\n", args); break;
				case 320:   out->Printf("V_MAD_LEGACY_F32           %s\n", args); break;
				case 321:   out->Printf("V_MAD_F32                  %s\n", args); break;
				case 322:   out->Printf("V_MAD_I32_I24              %s\n", args); break;
				case 323:   out->Printf("V_MAD_U32_U24              %s\n", args); break;
				case 324:   out->Printf("V_CUBEID_F32               %s\n", args); break;
				case 325:   out->Printf("V_CUBESC_F32               %s\n", args); break;
				case 326:   out->Printf("V_CUBETC_F32               %s\n", args); break;
				case 327:   out->Printf("V_CUBEMA_F32               %s\n", args); break;
				case 328:   out->Printf("V_BFE_U32                  %s\n", args); break;
				case 329:   out->Printf("V_BFE_I32                  %s\n", args); break;
				case 330:   out->Printf("V_BFI_B32                  %s\n", args); break;
				case 331:   out->Printf("V_FMA_F32                  %s\n", args); break;
				case 332:   out->Printf("V_FMA_F64                  %s\n", args); break;
				case 333:   out->Printf("V_LERP_U8                  %s\n", args); break;
				case 334:   out->Printf("V_ALIGNBIT_B32             %s\n", args); break;
				case 335:   out->Printf("V_ALIGNBYTE_B32            %s\n", args); break;
				case 336:   out->Printf("V_MULLIT_F32               %s\n", args); break;
				case 337:   out->Printf("V_MIN3_F32                 %s\n", args); break;
				case 338:   out->Printf("V_MIN3_I32                 %s\n", args); break;
				case 339:   out->Printf("V_MIN3_U32                 %s\n", args); break;
				case 340:   out->Printf("V_MAX3_F32                 %s\n", args); break;
				case 341:   out->Printf("V_MAX3_I32                 %s\n", args); break;
				case 342:   out->Printf("V_MAX3_U32                 %s\n", args); break;
				case 343:   out->Printf("V_MED3_F32                 %s\n", args); break;
				case 344:   out->Printf("V_MED3_I32                 %s\n", args); break;
				case 345:   out->Printf("V_MED3_U32                 %s\n", args); break;
				case 346:   out->Printf("V_SAD_U8                   %s\n", args); break;
				case 347:   out->Printf("V_SAD_HI_U8                %s\n", args); break;
				case 348:   out->Printf("V_SAD_U16                  %s\n", args); break;
				case 349:   out->Printf("V_SAD_U32                  %s\n", args); break;
				case 350:   out->Printf("V_CVT_PK_U8_F32            %s\n", args); break;
				case 351:   out->Printf("V_DIV_FIXUP_F32            %s\n", args); break;
				case 352:   out->Printf("V_DIV_FIXUP_F64            %s\n", args); break;
				case 353:   out->Printf("V_LSHL_B64                 %s\n", args); break;
				case 354:   out->Printf("V_LSHR_B64                 %s\n", args); break;
				case 355:   out->Printf("V_ASHR_I64                 %s\n", args); break;
				case 356:   out->Printf("V_ADD_F64                  %s\n", args); break;
				case 357:   out->Printf("V_MUL_F64                  %s\n", args); break;
				case 358:   out->Printf("V_MIN_F64                  %s\n", args); break;
				case 359:   out->Printf("V_MAX_F64                  %s\n", args); break;
				case 360:   out->Printf("V_LDEXP_F64                %s\n", args); break;
				case 361:   out->Printf("V_MUL_LO_U32               %s\n", args); break;
				case 362:   out->Printf("V_MUL_HI_U32               %s\n", args); break;
				case 363:   out->Printf("V_MUL_LO_I32               %s\n", args); break;
				case 364:   out->Printf("V_MUL_HI_I32               %s\n", args); break;
				case 367:   out->Printf("V_DIV_FMAS_F32             %s\n", args); break;
				case 368:   out->Printf("V_DIV_FMAS_F64             %s\n", args); break;
				case 369:   out->Printf("V_MSAD_U8                  %s\n", args); break;
				case 370:   out->Printf("V_QSAD_U8                  %s\n", args); break;
				case 371:   out->Printf("V_MQSAD_U8                 %s\n", args); break;
				case 372:   out->Printf("V_TRIG_PREOP_F64           %s\n", args); break;
				case 384:   out->Printf("V_NOP                      %s\n", args); break;
				case 385:   out->Printf("V_MOV_B32                  %s\n", args); break;
				case 386:   out->Printf("V_READFIRSTLANE_B32        %s\n", args); break;
				case 387:   out->Printf("V_CVT_I32_F64              %s\n", args); break;
				case 388:   out->Printf("V_CVT_F64_I32              %s\n", args); break;
				case 389:   out->Printf("V_CVT_F32_I32              %s\n", args); break;
				case 390:   out->Printf("V_CVT_F32_U32              %s\n", args); break;
				case 391:   out->Printf("V_CVT_U32_F32              %s\n", args); break;
				case 392:   out->Printf("V_CVT_I32_F32              %s\n", args); break;
				case 393:   out->Printf("V_MOV_FED_B32              %s\n", args); break;
				case 394:   out->Printf("V_CVT_F16_F32              %s\n", args); break;
				case 395:   out->Printf("V_CVT_F32_F16              %s\n", args); break;
				case 396:   out->Printf("V_CVT_RPI_I32_F32          %s\n", args); break;
				case 397:   out->Printf("V_CVT_FLR_I32_F32          %s\n", args); break;
				case 398:   out->Printf("V_CVT_OFF_F32_I4           %s\n", args); break;
				case 399:   out->Printf("V_CVT_F32_F64              %s\n", args); break;
				case 400:   out->Printf("V_CVT_F64_F32              %s\n", args); break;
				case 401:   out->Printf("V_CVT_F32_UBYTE0           %s\n", args); break;
				case 402:   out->Printf("V_CVT_F32_UBYTE1           %s\n", args); break;
				case 403:   out->Printf("V_CVT_F32_UBYTE2           %s\n", args); break;
				case 404:   out->Printf("V_CVT_F32_UBYTE3           %s\n", args); break;
				case 405:   out->Printf("V_CVT_U32_F64              %s\n", args); break;
				case 406:   out->Printf("V_CVT_F64_U32              %s\n", args); break;
				case 416:   out->Printf("V_FRACT_F32                %s\n", args); break;
				case 417:   out->Printf("V_TRUNC_F32                %s\n", args); break;
				case 418:   out->Printf("V_CEIL_F32                 %s\n", args); break;
				case 419:   out->Printf("V_RNDNE_F32                %s\n", args); break;
				case 420:   out->Printf("V_FLOOR_F32                %s\n", args); break;
				case 421:   out->Printf("V_EXP_F32                  %s\n", args); break;
				case 422:   out->Printf("V_LOG_CLAMP_F32            %s\n", args); break;
				case 423:   out->Printf("V_LOG_F32                  %s\n", args); break;
				case 424:   out->Printf("V_RCP_CLAMP_F32            %s\n", args); break;
				case 425:   out->Printf("V_RCP_LEGACY_F32           %s\n", args); break;
				case 426:   out->Printf("V_RCP_F32                  %s\n", args); break;
				case 427:   out->Printf("V_RCP_IFLAG_F32            %s\n", args); break;
				case 428:   out->Printf("V_RSQ_CLAMP_F32            %s\n", args); break;
				case 429:   out->Printf("V_RSQ_LEGACY_F32           %s\n", args); break;
				case 430:   out->Printf("V_RSQ_F32                  %s\n", args); break;
				case 431:   out->Printf("V_RCP_F64                  %s\n", args); break;
				case 432:   out->Printf("V_RCP_CLAMP_F64            %s\n", args); break;
				case 433:   out->Printf("V_RSQ_F64                  %s\n", args); break;
				case 434:   out->Printf("V_RSQ_CLAMP_F64            %s\n", args); break;
				case 435:   out->Printf("V_SQRT_F32                 %s\n", args); break;
				case 436:   out->Printf("V_SQRT_F64                 %s\n", args); break;
				case 437:   out->Printf("V_SIN_F32                  %s\n", args); break;
				case 438:   out->Printf("V_COS_F32                  %s\n", args); break;
				case 439:   out->Printf("V_NOT_B32                  %s\n", args); break;
				case 440:   out->Printf("V_BFREV_B32                %s\n", args); break;
				case 441:   out->Printf("V_FFBH_U32                 %s\n", args); break;
				case 442:   out->Printf("V_FFBL_B32                 %s\n", args); break;
				case 443:   out->Printf("V_FFBH_I32                 %s\n", args); break;
				case 444:   out->Printf("V_FREXP_EXP_I32_F64        %s\n", args); break;
				case 445:   out->Printf("V_FREXP_MANT_F64           %s\n", args); break;
				case 446:   out->Printf("V_FRACT_F64                %s\n", args); break;
				case 447:   out->Printf("V_FREXP_EXP_I32_F32        %s\n", args); break;
				case 448:   out->Printf("V_FREXP_MANT_F32           %s\n", args); break;
				case 449:   out->Printf("V_CLREXCP                  %s\n", args); break;
				case 450:   out->Printf("V_MOVRELD_B32              %s\n", args); break;
				case 451:   out->Printf("V_MOVRELS_B32              %s\n", args); break;
				case 452:   out->Printf("V_MOVRELSD_B32             %s\n", args); break;
				default:    out->Printf("[op=%u ???]                %s\n", op, args); break;
			}
		}
	}

private:

	FIELD(DW0_VDST,         0x000000ff);
	FIELD(DW0_ABS,          0x00000700);
	FIELD(DW0_CLAMP,        0x00000800);
	FIELD(DW0_RESERVED,     0x0001f000);
	FIELD(DW0_OP,           0x03fe0000);
	FIELD(DW0_ENCODING,     0xfc000000);
	enum{DW0_ENCODING_VALUE=0xd0000000};

	FIELD(DW1_SRC0,         0x000001ff);
	FIELD(DW1_SRC1,         0x0003fe00);
	FIELD(DW1_SRC2,         0x07fc0000);
	FIELD(DW1_OMOD,         0x18000000);
	FIELD(DW1_NEG,          0xe0000000);
};

////////////////////////////////////////////////////////////////////////////////
// Vop3bInsn
////////////////////////////////////////////////////////////////////////////////
class Vop3bInsn : public Insn
{
public:

	INSN_CTOR(Vop3bInsn)

	unsigned GetNumDwords(const u32 *insn)
	{
		return 2;
	}

	void Dump(OutputColumns *out, const u32 *insn)
	{
		out->Printf("VOP3b\n");
		DUMP_FIELD(insn[0], DW0_VDST);
		DUMP_FIELD(insn[0], DW0_SDST);
		DUMP_FIELD(insn[0], DW0_RESERVED);
		DUMP_FIELD(insn[0], DW0_OP);
		DUMP_FIELD(insn[1], DW1_SRC0);
		DUMP_FIELD(insn[1], DW1_SRC1);
		DUMP_FIELD(insn[1], DW1_SRC2);
		DUMP_FIELD(insn[1], DW1_OMOD);
		DUMP_FIELD(insn[1], DW1_NEG);
	}

private:

	FIELD(DW0_VDST,         0x000000ff);
	FIELD(DW0_SDST,         0x00007f00);
	FIELD(DW0_RESERVED,     0x00018000);
	FIELD(DW0_OP,           0x03fe0000);
	FIELD(DW0_ENCODING,     0xfc000000);
	enum{DW0_ENCODING_VALUE=0xd0000000};

	FIELD(DW1_SRC0,         0x000001ff);
	FIELD(DW1_SRC1,         0x0003fe00);
	FIELD(DW1_SRC2,         0x07fc0000);
	FIELD(DW1_OMOD,         0x18000000);
	FIELD(DW1_NEG,          0xe0000000);
};

////////////////////////////////////////////////////////////////////////////////
// VintrpInsn
////////////////////////////////////////////////////////////////////////////////
class VintrpInsn : public Insn
{
public:

	INSN_CTOR(VintrpInsn)

	unsigned GetNumDwords(const u32 *insn)
	{
		return 1;
	}

	void Dump(OutputColumns *out, const u32 *insn)
	{
		out->Printf("VINTRP | ");

		const u32 dw0 = insn[0];
		const u32 vsrc     = GET_FIELD(dw0, DW0_VSRC);
		const u32 attrchan = GET_FIELD(dw0, DW0_ATTRCHAN);
		const u32 attr     = GET_FIELD(dw0, DW0_ATTR);
		const u32 vdst     = GET_FIELD(dw0, DW0_VDST);
		const u32 op       = GET_FIELD(dw0, DW0_OP);
		switch (op)
		{
			case 0:     out->Printf("V_INTERP_P1_F32            VGPR%u, ATTR%u[%u], VGPR%u\n", vdst, attr, attrchan, vsrc); break;
			case 1:     out->Printf("V_INTERP_P2_F32            VGPR%u, ATTR%u[%u], VGPR%u\n", vdst, attr, attrchan, vsrc); break;
			case 2:     out->Printf("V_INTERP_MOV_F32           VGPR%u, ATTR%u[%u], VGPR%u\n", vdst, attr, attrchan, vsrc); break;
			default:    out->Printf("[op=%u ???]                VGPR%u, ATTR%u[%u], VGPR%u\n", op, vdst, attr, attrchan, vsrc); break;
		}
	}

private:

	FIELD(DW0_VSRC,         0x000000ff);
	FIELD(DW0_ATTRCHAN,     0x00000300);
	FIELD(DW0_ATTR,         0x0000fc00);
	FIELD(DW0_OP,           0x00030000);
	FIELD(DW0_VDST,         0x03fc0000);
	FIELD(DW0_ENCODING,     0xfc000000);
	enum{DW0_ENCODING_VALUE=0xc8000000};
};

////////////////////////////////////////////////////////////////////////////////
// DsInsn
////////////////////////////////////////////////////////////////////////////////
class DsInsn : public Insn
{
public:

	INSN_CTOR(DsInsn)

	unsigned GetNumDwords(const u32 *insn)
	{
		return 2;
	}

	void Dump(OutputColumns *out, const u32 *insn)
	{
		out->Printf("DS     | ");

		const u32 dw0 = insn[0];
		const u32 dw1 = insn[1];
		const u32 offset0  = GET_FIELD(dw0, DW0_OFFSET0);
		const u32 offset1  = GET_FIELD(dw0, DW0_OFFSET1);
		const u32 reserved = GET_FIELD(dw0, DW0_RESERVED);
		const u32 gds      = GET_FIELD(dw0, DW0_GDS);
		const u32 op       = GET_FIELD(dw0, DW0_OP);
		const u32 addr     = GET_FIELD(dw1, DW1_ADDR);
		const u32 data0    = GET_FIELD(dw1, DW1_DATA0);
		const u32 data1    = GET_FIELD(dw1, DW1_DATA1);
		const u32 vdst     = GET_FIELD(dw1, DW1_VDST);

		const char *dsStr = gds ? "GDS" : "LDS";

		switch (op)
		{
			case 53: out->Printf("DS_SWIZZLE_B32             VGPR%u, 0x%02x%02x\n", vdst, offset1, offset0); break;
			case 54: out->Printf("DS_READ_B32                VGPR%u, %s[0x%03x]\n", vdst, dsStr, addr); break;
			case 55: out->Printf("DS_READ2_B32               VGPR[%u:%u], %s[0x%03x], %s[0x%03x]\n", vdst, vdst+1, dsStr, addr+offset0*4, dsStr, addr+offset1*4); break;

			default:
				DUMP_FIELD(insn[0], DW0_OFFSET0);
				DUMP_FIELD(insn[0], DW0_OFFSET1);
				DUMP_FIELD(insn[0], DW0_RESERVED);
				DUMP_FIELD(insn[0], DW0_GDS);
				DUMP_FIELD(insn[0], DW0_OP);
				DUMP_FIELD(insn[1], DW1_ADDR);
				DUMP_FIELD(insn[1], DW1_DATA0);
				DUMP_FIELD(insn[1], DW1_DATA1);
				DUMP_FIELD(insn[1], DW1_VDST);
				break;
		}
	}

private:

	FIELD(DW0_OFFSET0,      0x000000ff);
	FIELD(DW0_OFFSET1,      0x0000ff00);
	FIELD(DW0_RESERVED,     0x00010000);
	FIELD(DW0_GDS,          0x00020000);
	FIELD(DW0_OP,           0x03fc0000);
	FIELD(DW0_ENCODING,     0xfc000000);
	enum{DW0_ENCODING_VALUE=0xd8000000};

	FIELD(DW1_ADDR,         0x000000ff);
	FIELD(DW1_DATA0,        0x0000ff00);
	FIELD(DW1_DATA1,        0x00ff0000);
	FIELD(DW1_VDST,         0xff000000);
};

////////////////////////////////////////////////////////////////////////////////
// MubufInsn
////////////////////////////////////////////////////////////////////////////////
class MubufInsn : public Insn
{
public:

	INSN_CTOR(MubufInsn)

	unsigned GetNumDwords(const u32 *insn)
	{
		return 2;
	}

	void Dump(OutputColumns *out, const u32 *insn)
	{
		out->Printf("MUBUF  | ");

		const u32 dw0 = insn[0];
		const u32 dw1 = insn[1];
		const u32 offset  = GET_FIELD(dw0, DW0_OFFSET);
		const u32 offen   = GET_FIELD(dw0, DW0_OFFEN);
		const u32 idxen   = GET_FIELD(dw0, DW0_IDXEN);
		const u32 glc     = GET_FIELD(dw0, DW0_GLC);
		const u32 addr64  = GET_FIELD(dw0, DW0_ADDR64);
		const u32 lds     = GET_FIELD(dw0, DW0_LDS);
		const u32 op      = GET_FIELD(dw0, DW0_OP);
		const u32 vaddr   = GET_FIELD(dw1, DW1_VADDR);
		const u32 vdata   = GET_FIELD(dw1, DW1_VDATA);
		const u32 srsrc   = GET_FIELD(dw1, DW1_SRSRC) << 2;
		const u32 slc     = GET_FIELD(dw1, DW1_SLC);
		const u32 tfe     = GET_FIELD(dw1, DW1_TFE);
		const u32 soffset = GET_FIELD(dw1, DW1_SOFFSET);

		char addr[64];
		if (lds)
		{
			sprintf(addr, "0x%x", offset);
		}
		else
		{
			if (offen)
			{
				sprintf(addr, "SGPR[%u:%u]+%s+VGPR%u", srsrc, srsrc+3, Ssrc(soffset,0), vaddr);
			}
			else if (idxen)
			{
				sprintf(addr, "SGPR[%u:%u]+%s+0x%x [idx=VGPR%u]", srsrc, srsrc+3, Ssrc(soffset,0), offset, vaddr);
			}
			else
			{
				sprintf(addr, "SGPR[%u:%u]+%s+0x%x", srsrc, srsrc+3, Ssrc(soffset,0), offset);
			}
		}

		char flags[64];
		sprintf(flags, "(glc=%u, slc=%u, tfe=%u)", glc, slc, tfe);

		switch (op)
		{
			case 0:     out->Printf("BUFFER_LOAD_FORMAT_X       VGPR%u, %s, %s\n", vdata, addr, flags); break;
			case 1:     out->Printf("BUFFER_LOAD_FORMAT_XY      VGPR[%u:%u], %s, %s\n", vdata, vdata+1, addr, flags); break;
			case 2:     out->Printf("BUFFER_LOAD_FORMAT_XYZ     VGPR[%u:%u], %s, %s\n", vdata, vdata+2, addr, flags); break;
			case 3:     out->Printf("BUFFER_LOAD_FORMAT_XYZW    VGPR[%u:%u], %s, %s\n", vdata, vdata+3, addr, flags); break;
			case 4:     out->Printf("BUFFER_STORE_FORMAT_X      VGPR%u, %s, %s\n", vdata, addr, flags); break;
			case 5:     out->Printf("BUFFER_STORE_FORMAT_XY     VGPR[%u:%u], %s, %s\n", vdata, vdata+1, addr, flags); break;
			case 6:     out->Printf("BUFFER_STORE_FORMAT_XYZ    VGPR[%u:%u], %s, %s\n", vdata, vdata+2, addr, flags); break;
			case 7:     out->Printf("BUFFER_STORE_FORMAT_XYZW   VGPR[%u:%u], %s, %s\n", vdata, vdata+3, addr, flags); break;
			case 8:     out->Printf("BUFFER_LOAD_UBYTE          VGPR%u, %s, %s\n", vdata, addr, flags); break;
			case 9:     out->Printf("BUFFER_LOAD_SBYTE          VGPR%u, %s, %s\n", vdata, addr, flags); break;
			case 10:    out->Printf("BUFFER_LOAD_USHORT         VGPR%u, %s, %s\n", vdata, addr, flags); break;
			case 11:    out->Printf("BUFFER_LOAD_SSHORT         VGPR%u, %s, %s\n", vdata, addr, flags); break;
			case 12:    out->Printf("BUFFER_LOAD_DWORD          VGPR%u, %s, %s\n", vdata, addr, flags); break;
			case 13:    out->Printf("BUFFER_LOAD_DWORDX2        VGPR%u, %s, %s\n", vdata, addr, flags); break;
			case 14:    out->Printf("BUFFER_LOAD_DWORDX4        VGPR%u, %s, %s\n", vdata, addr, flags); break;
			case 24:    out->Printf("BUFFER_STORE_BYTE          VGPR%u, %s, %s\n", vdata, addr, flags); break;
			case 26:    out->Printf("BUFFER_STORE_SHORT         VGPR%u, %s, %s\n", vdata, addr, flags); break;
			case 28:    out->Printf("BUFFER_STORE_DWORD         VGPR%u, %s, %s\n", vdata, addr, flags); break;
			case 29:    out->Printf("BUFFER_STORE_DWORDX2       VGPR%u, %s, %s\n", vdata, addr, flags); break;
			case 30:    out->Printf("BUFFER_STORE_DWORDX4       VGPR%u, %s, %s\n", vdata, addr, flags); break;
			case 48:    out->Printf("BUFFER_ATOMIC_SWAP         VGPR%u, %s, %s\n", vdata, addr, flags); break;
			case 49:    out->Printf("BUFFER_ATOMIC_CMPSWAP      VGPR%u, %s, %s\n", vdata, addr, flags); break;
			case 50:    out->Printf("BUFFER_ATOMIC_ADD          VGPR%u, %s, %s\n", vdata, addr, flags); break;
			case 51:    out->Printf("BUFFER_ATOMIC_SUB          VGPR%u, %s, %s\n", vdata, addr, flags); break;
			case 52:    out->Printf("BUFFER_ATOMIC_RSUB         VGPR%u, %s, %s\n", vdata, addr, flags); break;
			case 53:    out->Printf("BUFFER_ATOMIC_SMIN         VGPR%u, %s, %s\n", vdata, addr, flags); break;
			case 54:    out->Printf("BUFFER_ATOMIC_UMIN         VGPR%u, %s, %s\n", vdata, addr, flags); break;
			case 55:    out->Printf("BUFFER_ATOMIC_SMAX         VGPR%u, %s, %s\n", vdata, addr, flags); break;
			case 56:    out->Printf("BUFFER_ATOMIC_UMAX         VGPR%u, %s, %s\n", vdata, addr, flags); break;
			case 57:    out->Printf("BUFFER_ATOMIC_AND          VGPR%u, %s, %s\n", vdata, addr, flags); break;
			case 58:    out->Printf("BUFFER_ATOMIC_OR           VGPR%u, %s, %s\n", vdata, addr, flags); break;
			case 59:    out->Printf("BUFFER_ATOMIC_XOR          VGPR%u, %s, %s\n", vdata, addr, flags); break;
			case 60:    out->Printf("BUFFER_ATOMIC_INC          VGPR%u, %s, %s\n", vdata, addr, flags); break;
			case 61:    out->Printf("BUFFER_ATOMIC_DEC          VGPR%u, %s, %s\n", vdata, addr, flags); break;
			case 62:    out->Printf("BUFFER_ATOMIC_FCMPSWAP     VGPR%u, %s, %s\n", vdata, addr, flags); break;
			case 63:    out->Printf("BUFFER_ATOMIC_FMIN         VGPR%u, %s, %s\n", vdata, addr, flags); break;
			case 64:    out->Printf("BUFFER_ATOMIC_FMAX         VGPR%u, %s, %s\n", vdata, addr, flags); break;
			case 80:    out->Printf("BUFFER_ATOMIC_SWAP_X2      VGPR%u, %s, %s\n", vdata, addr, flags); break;
			case 81:    out->Printf("BUFFER_ATOMIC_CMPSWAP_X2   VGPR%u, %s, %s\n", vdata, addr, flags); break;
			case 82:    out->Printf("BUFFER_ATOMIC_ADD_X2       VGPR%u, %s, %s\n", vdata, addr, flags); break;
			case 83:    out->Printf("BUFFER_ATOMIC_SUB_X2       VGPR%u, %s, %s\n", vdata, addr, flags); break;
			case 84:    out->Printf("BUFFER_ATOMIC_RSUB_X2      VGPR%u, %s, %s\n", vdata, addr, flags); break;
			case 85:    out->Printf("BUFFER_ATOMIC_SMIN_X2      VGPR%u, %s, %s\n", vdata, addr, flags); break;
			case 86:    out->Printf("BUFFER_ATOMIC_UMIN_X2      VGPR%u, %s, %s\n", vdata, addr, flags); break;
			case 87:    out->Printf("BUFFER_ATOMIC_SMAX_X2      VGPR%u, %s, %s\n", vdata, addr, flags); break;
			case 88:    out->Printf("BUFFER_ATOMIC_UMAX_X2      VGPR%u, %s, %s\n", vdata, addr, flags); break;
			case 89:    out->Printf("BUFFER_ATOMIC_AND_X2       VGPR%u, %s, %s\n", vdata, addr, flags); break;
			case 90:    out->Printf("BUFFER_ATOMIC_OR_X2        VGPR%u, %s, %s\n", vdata, addr, flags); break;
			case 91:    out->Printf("BUFFER_ATOMIC_XOR_X2       VGPR%u, %s, %s\n", vdata, addr, flags); break;
			case 92:    out->Printf("BUFFER_ATOMIC_INC_X2       VGPR%u, %s, %s\n", vdata, addr, flags); break;
			case 93:    out->Printf("BUFFER_ATOMIC_DEC_X2       VGPR%u, %s, %s\n", vdata, addr, flags); break;
			case 94:    out->Printf("BUFFER_ATOMIC_FCMPSWAP_X2  VGPR%u, %s, %s\n", vdata, addr, flags); break;
			case 95:    out->Printf("BUFFER_ATOMIC_FMIN_X2      VGPR%u, %s, %s\n", vdata, addr, flags); break;
			case 96:    out->Printf("BUFFER_ATOMIC_FMAX_X2      VGPR%u, %s, %s\n", vdata, addr, flags); break;
			case 112:   out->Printf("BUFFER_WBINVL1_SC          VGPR%u, %s, %s\n", vdata, addr, flags); break;
			case 113:   out->Printf("BUFFER_WBINVL1             VGPR%u, %s, %s\n", vdata, addr, flags); break;
			default:    out->Printf("[op=%u ???]                VGPR%u, %s, %s\n", op, vdata, addr, flags); break;
		}
	}

private:

	FIELD(DW0_OFFSET,       0x00000fff);
	FIELD(DW0_OFFEN,        0x00001000);
	FIELD(DW0_IDXEN,        0x00002000);
	FIELD(DW0_GLC,          0x00004000);
	FIELD(DW0_ADDR64,       0x00008000);
	FIELD(DW0_LDS,          0x00010000);
	FIELD(DW0_RESERVED0,    0x00020000);
	FIELD(DW0_OP,           0x01fc0000);
	FIELD(DW0_RESERVED1,    0x02000000);
	FIELD(DW0_ENCODING,     0xfc000000);
	enum{DW0_ENCODING_VALUE=0xe0000000};

	FIELD(DW1_VADDR,        0x000000ff);
	FIELD(DW1_VDATA,        0x0000ff00);
	FIELD(DW1_SRSRC,        0x001f0000);
	FIELD(DW1_RESERVED,     0x00200000);
	FIELD(DW1_SLC,          0x00400000);
	FIELD(DW1_TFE,          0x00800000);
	FIELD(DW1_SOFFSET,      0xff000000);
};

////////////////////////////////////////////////////////////////////////////////
// MtbufInsn
////////////////////////////////////////////////////////////////////////////////
class MtbufInsn : public Insn
{
public:

	INSN_CTOR(MtbufInsn)

	unsigned GetNumDwords(const u32 *insn)
	{
		return 2;
	}

	void Dump(OutputColumns *out, const u32 *insn)
	{
		out->Printf("MTBUF  | ");

		const u32 dw0 = insn[0];
		const u32 dw1 = insn[1];
		const u32 offset  = GET_FIELD(dw0, DW0_OFFSET);
		const u32 offen   = GET_FIELD(dw0, DW0_OFFEN);
		const u32 idxen   = GET_FIELD(dw0, DW0_IDXEN);
		const u32 glc     = GET_FIELD(dw0, DW0_GLC);
		const u32 addr64  = GET_FIELD(dw0, DW0_ADDR64);
		const u32 op      = GET_FIELD(dw0, DW0_OP);
		const u32 dfmt    = GET_FIELD(dw0, DW0_DFMT);
		const u32 nfmt    = GET_FIELD(dw0, DW0_NFMT);
		const u32 vaddr   = GET_FIELD(dw1, DW1_VADDR);
		const u32 vdata   = GET_FIELD(dw1, DW1_VDATA);
		const u32 srsrc   = GET_FIELD(dw1, DW1_SRSRC) << 2;
		const u32 slc     = GET_FIELD(dw1, DW1_SLC);
		const u32 tfe     = GET_FIELD(dw1, DW1_TFE);
		const u32 soffset = GET_FIELD(dw1, DW1_SOFFSET);

		char addr[64];
		if (offen)
		{
			sprintf(addr, "SGPR[%u:%u]+%s+VGPR%u", srsrc, srsrc+3, Ssrc(soffset,0), vaddr);
		}
		else if (idxen)
		{
			sprintf(addr, "SGPR[%u:%u]+%s+0x%x [idx=VGPR%u]", srsrc, srsrc+3, Ssrc(soffset,0), offset, vaddr);
		}
		else
		{
			sprintf(addr, "SGPR[%u:%u]+%s+0x%x", srsrc, srsrc+3, Ssrc(soffset,0), offset);
		}

		static const char *const dfmtlut[] =
		{
			"invalid",      "8",            "16",           "8_8",
			"32",           "16_16",        "10_11_11",     "11_11_10",
			"10_10_10_2",   "2_10_10_10",   "8_8_8_8",      "32_32",
			"16_16_16_16",  "32_32_32",     "32_32_32_32",  "reserved",
		};
		static const char *const nfmtlut[] =
		{
			"unorm",        "snorm",        "uscaled",      "sscaled",
			"uint",         "sint",         "snorm_nz",     "float",
			"reserved",     "srgb",         "ubnorm",       "ubnorm_nz",
			"ubint",        "ubscaled",     "reserved",     "reserved",
		};

		char fmtFlags[64];
		sprintf(fmtFlags, "%s %s (addr64=%u, glc=%u, slc=%u, tfe=%u)", dfmtlut[dfmt], nfmtlut[nfmt], addr64, glc, slc, tfe);

		switch (op)
		{
			case 0:     out->Printf("TBUFFER_LOAD_FORMAT_X      VGPR%u, %s %s\n", vdata, addr, fmtFlags); break;
			case 1:     out->Printf("TBUFFER_LOAD_FORMAT_XY     VGPR[%u:%u], %s %s\n", vdata, vdata+1, addr, fmtFlags); break;
			case 2:     out->Printf("TBUFFER_LOAD_FORMAT_XYZ    VGPR[%u:%u], %s %s\n", vdata, vdata+2, addr, fmtFlags); break;
			case 3:     out->Printf("TBUFFER_LOAD_FORMAT_XYZW   VGPR[%u:%u], %s %s\n", vdata, vdata+3, addr, fmtFlags); break;
			case 4:     out->Printf("TBUFFER_STORE_FORMAT_X     VGPR%u, %s %s\n", vdata, addr, fmtFlags); break;
			case 5:     out->Printf("TBUFFER_STORE_FORMAT_XY    VGPR[%u:%u], %s %s\n", vdata, vdata+1, addr, fmtFlags); break;
			case 6:     out->Printf("TBUFFER_STORE_FORMAT_XYZ   VGPR[%u:%u], %s %s\n", vdata, vdata+2, addr, fmtFlags); break;
			case 7:     out->Printf("TBUFFER_STORE_FORMAT_XYZW  VGPR[%u:%u], %s %s\n", vdata, vdata+3, addr, fmtFlags); break;
		}
	}

private:

	FIELD(DW0_OFFSET,       0x00000fff);
	FIELD(DW0_OFFEN,        0x00001000);
	FIELD(DW0_IDXEN,        0x00002000);
	FIELD(DW0_GLC,          0x00004000);
	FIELD(DW0_ADDR64,       0x00008000);
	FIELD(DW0_OP,           0x00070000);
	FIELD(DW0_DFMT,         0x00780000);
	FIELD(DW0_NFMT,         0x03800000);
	FIELD(DW0_ENCODING,     0xfc000000);
	enum{DW0_ENCODING_VALUE=0xe8000000};

	FIELD(DW1_VADDR,        0x000000ff);
	FIELD(DW1_VDATA,        0x0000ff00);
	FIELD(DW1_SRSRC,        0x001f0000);
	FIELD(DW1_RESERVED,     0x00200000);
	FIELD(DW1_SLC,          0x00400000);
	FIELD(DW1_TFE,          0x00800000);
	FIELD(DW1_SOFFSET,      0xff000000);
};

////////////////////////////////////////////////////////////////////////////////
// MimgInsn
////////////////////////////////////////////////////////////////////////////////
class MimgInsn : public Insn
{
public:

	INSN_CTOR(MimgInsn)

	unsigned GetNumDwords(const u32 *insn)
	{
		return 2;
	}

	void Dump(OutputColumns *out, const u32 *insn)
	{
		out->Printf("MIMG   | ");

		const u32 dw0 = insn[0];
		const u32 dw1 = insn[1];
		const u32 dmask = GET_FIELD(dw0, DW0_DMASK);
		const u32 unorm = GET_FIELD(dw0, DW0_UNORM);
		const u32 glc   = GET_FIELD(dw0, DW0_GLC);
		const u32 da    = GET_FIELD(dw0, DW0_DA);
		const u32 r128  = GET_FIELD(dw0, DW0_R128);
		const u32 tfe   = GET_FIELD(dw0, DW0_TFE);
		const u32 lwe   = GET_FIELD(dw0, DW0_LWE);
		const u32 op    = GET_FIELD(dw0, DW0_OP);
		const u32 slc   = GET_FIELD(dw0, DW0_SLC);
		const u32 vaddr = GET_FIELD(dw1, DW1_VADDR);
		const u32 vdata = GET_FIELD(dw1, DW1_VDATA);
		const u32 srsrc = GET_FIELD(dw1, DW1_SRSRC)*4;
		const u32 ssamp = GET_FIELD(dw1, DW1_SSAMP)*4;

		char dst[32];
		unsigned numDsts = CountBits(dmask);
		if (numDsts == 1)
		{
			sprintf(dst, "VGPR%u", vdata);
		}
		else
		{
			sprintf(dst, "VGPR[%u:%u]", vdata, vdata+numDsts-1);
		}

		char args[128];
		sprintf(args, "%s, rsrc=SGPR[%u:%u] samp=SGPR[%u:%u] (dmask=0x%x, unorm=%u, glc=%u, da=%u, r128=%u, tfe=%u, lwe=%u, slc=%u)", dst, srsrc, srsrc+7, ssamp, ssamp+3, dmask, unorm, glc, da, r128, tfe, lwe, slc);

		switch (op)
		{
			case 0:     out->Printf("IMAGE_LOAD                 %s\n", args); break;
			case 1:     out->Printf("IMAGE_LOAD_MIP             %s\n", args); break;
			case 2:     out->Printf("IMAGE_LOAD_PCK             %s\n", args); break;
			case 3:     out->Printf("IMAGE_LOAD_PCK_SGN         %s\n", args); break;
			case 4:     out->Printf("IMAGE_LOAD_MIP_PCK         %s\n", args); break;
			case 5:     out->Printf("IMAGE_LOAD_MIP_PCK_SGN     %s\n", args); break;
			case 8:     out->Printf("IMAGE_STORE                %s\n", args); break;
			case 9:     out->Printf("IMAGE_STORE_MIP            %s\n", args); break;
			case 10:    out->Printf("IMAGE_STORE_PCK            %s\n", args); break;
			case 11:    out->Printf("IMAGE_STORE_MIP_PCK        %s\n", args); break;
			case 14:    out->Printf("IMAGE_GET_RESINFO          %s\n", args); break;
			case 15:    out->Printf("IMAGE_ATOMIC_SWAP          %s\n", args); break;
			case 16:    out->Printf("IMAGE_ATOMIC_CMPSWAP       %s\n", args); break;
			case 17:    out->Printf("IMAGE_ATOMIC_ADD           %s\n", args); break;
			case 18:    out->Printf("IMAGE_ATOMIC_SUB           %s\n", args); break;
			case 19:    out->Printf("IMAGE_ATOMIC_RSUB          %s\n", args); break;
			case 20:    out->Printf("IMAGE_ATOMIC_SMIN          %s\n", args); break;
			case 21:    out->Printf("IMAGE_ATOMIC_UMIN          %s\n", args); break;
			case 22:    out->Printf("IMAGE_ATOMIC_SMAX          %s\n", args); break;
			case 23:    out->Printf("IMAGE_ATOMIC_UMAX          %s\n", args); break;
			case 24:    out->Printf("IMAGE_ATOMIC_AND           %s\n", args); break;
			case 25:    out->Printf("IMAGE_ATOMIC_OR            %s\n", args); break;
			case 26:    out->Printf("IMAGE_ATOMIC_XOR           %s\n", args); break;
			case 27:    out->Printf("IMAGE_ATOMIC_INC           %s\n", args); break;
			case 28:    out->Printf("IMAGE_ATOMIC_DEC           %s\n", args); break;
			case 29:    out->Printf("IMAGE_ATOMIC_FCMPSWAP      %s\n", args); break;
			case 30:    out->Printf("IMAGE_ATOMIC_FMIN          %s\n", args); break;
			case 31:    out->Printf("IMAGE_ATOMIC_FMAX          %s\n", args); break;
			case 32:    out->Printf("IMAGE_SAMPLE               %s\n", args); break;
			case 33:    out->Printf("IMAGE_SAMPLE_CL            %s\n", args); break;
			case 34:    out->Printf("IMAGE_SAMPLE_D             %s\n", args); break;
			case 35:    out->Printf("IMAGE_SAMPLE_D_CL          %s\n", args); break;
			case 36:    out->Printf("IMAGE_SAMPLE_L             %s\n", args); break;
			case 37:    out->Printf("IMAGE_SAMPLE_B             %s\n", args); break;
			case 38:    out->Printf("IMAGE_SAMPLE_B_CL          %s\n", args); break;
			case 39:    out->Printf("IMAGE_SAMPLE_LZ            %s\n", args); break;
			case 40:    out->Printf("IMAGE_SAMPLE_C             %s\n", args); break;
			case 41:    out->Printf("IMAGE_SAMPLE_C_CL          %s\n", args); break;
			case 42:    out->Printf("IMAGE_SAMPLE_C_D           %s\n", args); break;
			case 43:    out->Printf("IMAGE_SAMPLE_C_D_CL        %s\n", args); break;
			case 44:    out->Printf("IMAGE_SAMPLE_C_L           %s\n", args); break;
			case 45:    out->Printf("IMAGE_SAMPLE_C_B           %s\n", args); break;
			case 46:    out->Printf("IMAGE_SAMPLE_C_B_CL        %s\n", args); break;
			case 47:    out->Printf("IMAGE_SAMPLE_C_LZ          %s\n", args); break;
			case 48:    out->Printf("IMAGE_SAMPLE_O             %s\n", args); break;
			case 49:    out->Printf("IMAGE_SAMPLE_CL_O          %s\n", args); break;
			case 50:    out->Printf("IMAGE_SAMPLE_D_O           %s\n", args); break;
			case 51:    out->Printf("IMAGE_SAMPLE_D_CL_O        %s\n", args); break;
			case 52:    out->Printf("IMAGE_SAMPLE_L_O           %s\n", args); break;
			case 53:    out->Printf("IMAGE_SAMPLE_B_O           %s\n", args); break;
			case 54:    out->Printf("IMAGE_SAMPLE_B_CL_O        %s\n", args); break;
			case 55:    out->Printf("IMAGE_SAMPLE_LZ_O          %s\n", args); break;
			case 56:    out->Printf("IMAGE_SAMPLE_C_O           %s\n", args); break;
			case 57:    out->Printf("IMAGE_SAMPLE_C_CL_O        %s\n", args); break;
			case 58:    out->Printf("IMAGE_SAMPLE_C_D_O         %s\n", args); break;
			case 59:    out->Printf("IMAGE_SAMPLE_C_D_CL_O      %s\n", args); break;
			case 60:    out->Printf("IMAGE_SAMPLE_C_L_O         %s\n", args); break;
			case 61:    out->Printf("IMAGE_SAMPLE_C_B_O         %s\n", args); break;
			case 62:    out->Printf("IMAGE_SAMPLE_C_B_CL_O      %s\n", args); break;
			case 63:    out->Printf("IMAGE_SAMPLE_C_LZ_O        %s\n", args); break;
			case 64:    out->Printf("IMAGE_GATHER4              %s\n", args); break;
			case 65:    out->Printf("IMAGE_GATHER4_CL           %s\n", args); break;
			case 66:    out->Printf("IMAGE_GATHER4_L            %s\n", args); break;
			case 67:    out->Printf("IMAGE_GATHER4_B            %s\n", args); break;
			case 68:    out->Printf("IMAGE_GATHER4_B_CL         %s\n", args); break;
			case 69:    out->Printf("IMAGE_GATHER4_LZ           %s\n", args); break;
			case 70:    out->Printf("IMAGE_GATHER4_C            %s\n", args); break;
			case 71:    out->Printf("IMAGE_GATHER4_C_CL         %s\n", args); break;
			case 76:    out->Printf("IMAGE_GATHER4_C_L          %s\n", args); break;
			case 77:    out->Printf("IMAGE_GATHER4_C_B          %s\n", args); break;
			case 78:    out->Printf("IMAGE_GATHER4_C_B_CL       %s\n", args); break;
			case 79:    out->Printf("IMAGE_GATHER4_C_LZ         %s\n", args); break;
			case 80:    out->Printf("IMAGE_GATHER4_O            %s\n", args); break;
			case 81:    out->Printf("IMAGE_GATHER4_CL_O         %s\n", args); break;
			case 84:    out->Printf("IMAGE_GATHER4_L_O          %s\n", args); break;
			case 85:    out->Printf("IMAGE_GATHER4_B_O          %s\n", args); break;
			case 86:    out->Printf("IMAGE_GATHER4_B_CL_O       %s\n", args); break;
			case 87:    out->Printf("IMAGE_GATHER4_LZ_O         %s\n", args); break;
			case 88:    out->Printf("IMAGE_GATHER4_C_O          %s\n", args); break;
			case 89:    out->Printf("IMAGE_GATHER4_C_CL_O       %s\n", args); break;
			case 92:    out->Printf("IMAGE_GATHER4_C_L_O        %s\n", args); break;
			case 93:    out->Printf("IMAGE_GATHER4_C_B_O        %s\n", args); break;
			case 94:    out->Printf("IMAGE_GATHER4_C_B_CL_O     %s\n", args); break;
			case 95:    out->Printf("IMAGE_GATHER4_C_LZ_O       %s\n", args); break;
			case 96:    out->Printf("IMAGE_GET_LOD              %s\n", args); break;
			case 104:   out->Printf("IMAGE_SAMPLE_CD            %s\n", args); break;
			case 105:   out->Printf("IMAGE_SAMPLE_CD_CL         %s\n", args); break;
			case 106:   out->Printf("IMAGE_SAMPLE_C_CD          %s\n", args); break;
			case 107:   out->Printf("IMAGE_SAMPLE_C_CD_CL       %s\n", args); break;
			case 108:   out->Printf("IMAGE_SAMPLE_CD_O          %s\n", args); break;
			case 109:   out->Printf("IMAGE_SAMPLE_CD_CL_O       %s\n", args); break;
			case 110:   out->Printf("IMAGE_SAMPLE_C_CD_O        %s\n", args); break;
			case 111:   out->Printf("IMAGE_SAMPLE_C_CD_CL_O     %s\n", args); break;
			default:    out->Printf("[op=%u ???]                %s\n", op, args); break;
		}
	}

private:

	FIELD(DW0_RESERVED,     0x000000ff);
	FIELD(DW0_DMASK,        0x00000f00);
	FIELD(DW0_UNORM,        0x00001000);
	FIELD(DW0_GLC,          0x00002000);
	FIELD(DW0_DA,           0x00004000);
	FIELD(DW0_R128,         0x00008000);
	FIELD(DW0_TFE,          0x00010000);
	FIELD(DW0_LWE,          0x00020000);
	FIELD(DW0_OP,           0x01fc0000);
	FIELD(DW0_SLC,          0x02000000);
	FIELD(DW0_ENCODING,     0xfc000000);
	enum{DW0_ENCODING_VALUE=0xf0000000};

	FIELD(DW1_VADDR,        0x000000ff);
	FIELD(DW1_VDATA,        0x0000ff00);
	FIELD(DW1_SRSRC,        0x001f0000);
	FIELD(DW1_SSAMP,        0x03e00000);
	FIELD(DW1_RESERVED,     0xfc000000);
};

////////////////////////////////////////////////////////////////////////////////
// ExpInsn
////////////////////////////////////////////////////////////////////////////////
class ExpInsn : public Insn
{
public:

	INSN_CTOR(ExpInsn)

	unsigned GetNumDwords(const u32 *insn)
	{
		return 2;
	}

	void Dump(OutputColumns *out, const u32 *insn)
	{
		out->Printf("EXP    | ");

		const u32 dw0 = insn[0];
		const u32 dw1 = insn[1];
		const u32 en      = GET_FIELD(dw0, DW0_EN);
		const u32 tgt     = GET_FIELD(dw0, DW0_TGT);
		const u32 compr   = GET_FIELD(dw0, DW0_COMPR);
		const u32 done    = GET_FIELD(dw0, DW0_DONE);
		const u32 vm      = GET_FIELD(dw0, DW0_VM);
		const u32 vscr[4] = {GET_FIELD(dw1, DW1_VSRC0), GET_FIELD(dw1, DW1_VSRC1), GET_FIELD(dw1, DW1_VSRC2), GET_FIELD(dw1, DW1_VSRC3)};

		char tgtStr[16];
		if (tgt<8)                      sprintf(tgtStr, "MRT%u", tgt);
		else if (tgt==8)                sprintf(tgtStr, "MRTZ");
		else if (tgt==9)                sprintf(tgtStr, "MRTZ");
		else if (12<=tgt && tgt<=15)    sprintf(tgtStr, "POS%u", tgt-12);
		else if (32<=tgt && tgt<=63)    sprintf(tgtStr, "PARAM%u", tgt-32);
		else                            sprintf(tgtStr, "???(0x%x)", tgt);

		out->Printf("EXPORT                     %s", tgtStr);
		for (unsigned i=0; i<(compr?2:4); ++i)
		{
			if ((en & (1<<i)) != 0)
			{
				out->Printf(", VGPR%u", vscr[i]);
			}
		}

		out->Printf(" (compr=%u, done=%u, vm=%u)\n", compr, done, vm);
	}

private:

	FIELD(DW0_EN,           0x0000000f);
	FIELD(DW0_TGT,          0x000003f0);
	FIELD(DW0_COMPR,        0x00000400);
	FIELD(DW0_DONE,         0x00000800);
	FIELD(DW0_VM,           0x00001000);
	FIELD(DW0_RESERVED,     0x03ffe000);
	FIELD(DW0_ENCODING,     0xfc000000);
	enum{DW0_ENCODING_VALUE=0xf8000000};

	FIELD(DW1_VSRC0,        0x000000ff);
	FIELD(DW1_VSRC1,        0x0000ff00);
	FIELD(DW1_VSRC2,        0x00ff0000);
	FIELD(DW1_VSRC3,        0xff000000);
};

////////////////////////////////////////////////////////////////////////////////
// FlatInsn
////////////////////////////////////////////////////////////////////////////////
class FlatInsn : public Insn
{
public:

	INSN_CTOR(FlatInsn)

	unsigned GetNumDwords(const u32 *insn)
	{
		return 2;
	}

	void Dump(OutputColumns *out, const u32 *insn)
	{
		out->Printf("FLAT   | ");

		const u32 dw0 = insn[0];
		const u32 dw1 = insn[1];
		const u32 glc     = GET_FIELD(dw0, DW0_GLC);
		const u32 slc     = GET_FIELD(dw0, DW0_SLC);
		const u32 op      = GET_FIELD(dw0, DW0_OP);
		const u32 addr    = GET_FIELD(dw1, DW1_ADDR);
		const u32 data    = GET_FIELD(dw1, DW1_DATA);
		const u32 tfe     = GET_FIELD(dw1, DW1_TFE);
		const u32 vdst    = GET_FIELD(dw1, DW1_VDST);

		switch (op)
		{
			case 8:     out->Printf("FLAT_LOAD_UBYTE            VGPR%u, VGPR%u, VGPR%u (tfe=%u)\n", vdst, addr, data, tfe);
			case 9:     out->Printf("FLAT_LOAD_SBYTE            VGPR%u, VGPR%u, VGPR%u (tfe=%u)\n", vdst, addr, data, tfe);
			case 10:    out->Printf("FLAT_LOAD_USHORT           VGPR%u, VGPR%u, VGPR%u (tfe=%u)\n", vdst, addr, data, tfe);
			case 11:    out->Printf("FLAT_LOAD_SSHORT           VGPR%u, VGPR%u, VGPR%u (tfe=%u)\n", vdst, addr, data, tfe);
			case 12:    out->Printf("FLAT_LOAD_DWORD            VGPR%u, VGPR%u, VGPR%u (tfe=%u)\n", vdst, addr, data, tfe);
			case 13:    out->Printf("FLAT_LOAD_DWORDX2          VGPR%u, VGPR%u, VGPR%u (tfe=%u)\n", vdst, addr, data, tfe);
			case 14:    out->Printf("FLAT_LOAD_DWORDX4          VGPR%u, VGPR%u, VGPR%u (tfe=%u)\n", vdst, addr, data, tfe);
			case 15:    out->Printf("FLAT_LOAD_DWORDX3          VGPR%u, VGPR%u, VGPR%u (tfe=%u)\n", vdst, addr, data, tfe);
			case 24:    out->Printf("FLAT_STORE_BYTE            VGPR%u, VGPR%u, VGPR%u (tfe=%u)\n", vdst, addr, data, tfe);
			case 26:    out->Printf("FLAT_STORE_SHORT           VGPR%u, VGPR%u, VGPR%u (tfe=%u)\n", vdst, addr, data, tfe);
			case 28:    out->Printf("FLAT_STORE_DWORD           VGPR%u, VGPR%u, VGPR%u (tfe=%u)\n", vdst, addr, data, tfe);
			case 29:    out->Printf("FLAT_STORE_DWORDX2         VGPR%u, VGPR%u, VGPR%u (tfe=%u)\n", vdst, addr, data, tfe);
			case 30:    out->Printf("FLAT_STORE_DWORDX4         VGPR%u, VGPR%u, VGPR%u (tfe=%u)\n", vdst, addr, data, tfe);
			case 31:    out->Printf("FLAT_STORE_DWORDX3         VGPR%u, VGPR%u, VGPR%u (tfe=%u)\n", vdst, addr, data, tfe);
			case 48:    out->Printf("FLAT_ATOMIC_SWAP           VGPR%u, VGPR%u, VGPR%u (tfe=%u)\n", vdst, addr, data, tfe);
			case 49:    out->Printf("FLAT_ATOMIC_CMPSWAP        VGPR%u, VGPR%u, VGPR%u (tfe=%u)\n", vdst, addr, data, tfe);
			case 50:    out->Printf("FLAT_ATOMIC_ADD            VGPR%u, VGPR%u, VGPR%u (tfe=%u)\n", vdst, addr, data, tfe);
			case 51:    out->Printf("FLAT_ATOMIC_SUB            VGPR%u, VGPR%u, VGPR%u (tfe=%u)\n", vdst, addr, data, tfe);
			case 53:    out->Printf("FLAT_ATOMIC_SMIN           VGPR%u, VGPR%u, VGPR%u (tfe=%u)\n", vdst, addr, data, tfe);
			case 54:    out->Printf("FLAT_ATOMIC_UMIN           VGPR%u, VGPR%u, VGPR%u (tfe=%u)\n", vdst, addr, data, tfe);
			case 55:    out->Printf("FLAT_ATOMIC_SMAX           VGPR%u, VGPR%u, VGPR%u (tfe=%u)\n", vdst, addr, data, tfe);
			case 56:    out->Printf("FLAT_ATOMIC_UMAX           VGPR%u, VGPR%u, VGPR%u (tfe=%u)\n", vdst, addr, data, tfe);
			case 57:    out->Printf("FLAT_ATOMIC_AND            VGPR%u, VGPR%u, VGPR%u (tfe=%u)\n", vdst, addr, data, tfe);
			case 58:    out->Printf("FLAT_ATOMIC_OR             VGPR%u, VGPR%u, VGPR%u (tfe=%u)\n", vdst, addr, data, tfe);
			case 59:    out->Printf("FLAT_ATOMIC_XOR            VGPR%u, VGPR%u, VGPR%u (tfe=%u)\n", vdst, addr, data, tfe);
			case 60:    out->Printf("FLAT_ATOMIC_INC            VGPR%u, VGPR%u, VGPR%u (tfe=%u)\n", vdst, addr, data, tfe);
			case 61:    out->Printf("FLAT_ATOMIC_DEC            VGPR%u, VGPR%u, VGPR%u (tfe=%u)\n", vdst, addr, data, tfe);
			case 62:    out->Printf("FLAT_ATOMIC_FCMPSWAP       VGPR%u, VGPR%u, VGPR%u (tfe=%u)\n", vdst, addr, data, tfe);
			case 63:    out->Printf("FLAT_ATOMIC_FMIN           VGPR%u, VGPR%u, VGPR%u (tfe=%u)\n", vdst, addr, data, tfe);
			case 64:    out->Printf("FLAT_ATOMIC_FMAX           VGPR%u, VGPR%u, VGPR%u (tfe=%u)\n", vdst, addr, data, tfe);
			case 80:    out->Printf("FLAT_ATOMIC_SWAP_X2        VGPR%u, VGPR%u, VGPR%u (tfe=%u)\n", vdst, addr, data, tfe);
			case 81:    out->Printf("FLAT_ATOMIC_CMPSWAP_X2     VGPR%u, VGPR%u, VGPR%u (tfe=%u)\n", vdst, addr, data, tfe);
			case 82:    out->Printf("FLAT_ATOMIC_ADD_X2         VGPR%u, VGPR%u, VGPR%u (tfe=%u)\n", vdst, addr, data, tfe);
			case 83:    out->Printf("FLAT_ATOMIC_SUB_X2         VGPR%u, VGPR%u, VGPR%u (tfe=%u)\n", vdst, addr, data, tfe);
			case 85:    out->Printf("FLAT_ATOMIC_SMIN_X2        VGPR%u, VGPR%u, VGPR%u (tfe=%u)\n", vdst, addr, data, tfe);
			case 86:    out->Printf("FLAT_ATOMIC_UMIN_X2        VGPR%u, VGPR%u, VGPR%u (tfe=%u)\n", vdst, addr, data, tfe);
			case 87:    out->Printf("FLAT_ATOMIC_SMAX_X2        VGPR%u, VGPR%u, VGPR%u (tfe=%u)\n", vdst, addr, data, tfe);
			case 88:    out->Printf("FLAT_ATOMIC_UMAX_X2        VGPR%u, VGPR%u, VGPR%u (tfe=%u)\n", vdst, addr, data, tfe);
			case 89:    out->Printf("FLAT_ATOMIC_AND_X2         VGPR%u, VGPR%u, VGPR%u (tfe=%u)\n", vdst, addr, data, tfe);
			case 90:    out->Printf("FLAT_ATOMIC_OR_X2          VGPR%u, VGPR%u, VGPR%u (tfe=%u)\n", vdst, addr, data, tfe);
			case 91:    out->Printf("FLAT_ATOMIC_XOR_X2         VGPR%u, VGPR%u, VGPR%u (tfe=%u)\n", vdst, addr, data, tfe);
			case 92:    out->Printf("FLAT_ATOMIC_INC_X2         VGPR%u, VGPR%u, VGPR%u (tfe=%u)\n", vdst, addr, data, tfe);
			case 93:    out->Printf("FLAT_ATOMIC_DEC_X2         VGPR%u, VGPR%u, VGPR%u (tfe=%u)\n", vdst, addr, data, tfe);
			case 94:    out->Printf("FLAT_ATOMIC_FCMPSWAP_X2    VGPR%u, VGPR%u, VGPR%u (tfe=%u)\n", vdst, addr, data, tfe);
			case 95:    out->Printf("FLAT_ATOMIC_FMIN_X2        VGPR%u, VGPR%u, VGPR%u (tfe=%u)\n", vdst, addr, data, tfe);
			case 96:    out->Printf("FLAT_ATOMIC_FMAX_X2        VGPR%u, VGPR%u, VGPR%u (tfe=%u)\n", vdst, addr, data, tfe);
			default:    out->Printf("[op=%u ???]                VGPR%u, VGPR%u, VGPR%u (tfe=%u)\n", op, vdst, addr, data, tfe);
		}
	}

private:

	FIELD(DW0_GLC,          0x00010000);
	FIELD(DW0_SLC,          0x00020000);
	FIELD(DW0_OP,           0x01fc0000);
	FIELD(DW0_ENCODING,     0xfc000000);
	enum{DW0_ENCODING_VALUE=0xdc000000};

	FIELD(DW1_ADDR,         0x000000ff);
	FIELD(DW1_DATA,         0x0000ff00);
	FIELD(DW1_TFE,          0x00800000);
	FIELD(DW1_VDST,         0xff000000);
};

////////////////////////////////////////////////////////////////////////////////
// InsnDecoder
////////////////////////////////////////////////////////////////////////////////
class InsnDecoder
{
public:

	static void RegisterInsnType(u32 dw0EncodingMask, u32 dw0EncodingValue, Insn *insn)
	{
		Decoder d;
		d.m_dw0EncodingMask  = dw0EncodingMask;
		d.m_dw0EncodingValue = dw0EncodingValue;
		d.m_insn             = insn;
		sm_decoders.push_back(d);
		std::sort(sm_decoders.begin(), sm_decoders.end());
	}

	static const u32 *Dump(const u32 *insn)
	{
		Insn *const type = SelectInsnType(insn);
		if (!type)
		{
			OutputColumns out(insn, 1);
			out.Printf("???\n");
			return insn+1;
		}
		else
		{
			const unsigned numDwords = type->GetNumDwords(insn);
			OutputColumns out(insn, numDwords);
			type->Dump(&out, insn);
			return insn+numDwords;
		}
	}

private:

	struct Decoder
	{
		u32     m_dw0EncodingMask;
		u32     m_dw0EncodingValue;
		Insn   *m_insn;

		// Sort order is to check the masks with most bits first
		bool operator<(const Decoder &other) const
		{
			return m_dw0EncodingMask > other.m_dw0EncodingMask;
		}
	};

	static std::vector<Decoder> sm_decoders;

	static Insn *SelectInsnType(const u32 *insn)
	{
		const u32 dw0 = *insn;
		for (std::vector<Decoder>::const_iterator d=sm_decoders.begin(); d!=sm_decoders.end(); ++d)
		{
			if ((dw0 & d->m_dw0EncodingMask) == d->m_dw0EncodingValue)
			{
				return d->m_insn;
			}
		}
		return NULL;
	}
};

std::vector<InsnDecoder::Decoder> InsnDecoder::sm_decoders;

Insn::Insn(u32 dw0EncodingMask, u32 dw0EncodingValue)
{
	InsnDecoder::RegisterInsnType(dw0EncodingMask, dw0EncodingValue, this);
}

#define REGISTER_INSN_DECODER(TYPE) TYPE g_registerInsnDecoder_##TYPE

REGISTER_INSN_DECODER(Sop2Insn);
REGISTER_INSN_DECODER(SopkInsn);
REGISTER_INSN_DECODER(Sop1Insn);
REGISTER_INSN_DECODER(SopcInsn);
REGISTER_INSN_DECODER(SoppInsn);
REGISTER_INSN_DECODER(SmrdInsn);
REGISTER_INSN_DECODER(Vop2Insn);
REGISTER_INSN_DECODER(Vop1Insn);
REGISTER_INSN_DECODER(VopcInsn);
REGISTER_INSN_DECODER(Vop3aInsn);
REGISTER_INSN_DECODER(Vop3bInsn);
REGISTER_INSN_DECODER(VintrpInsn);
REGISTER_INSN_DECODER(DsInsn);
REGISTER_INSN_DECODER(MubufInsn);
REGISTER_INSN_DECODER(MtbufInsn);
REGISTER_INSN_DECODER(MimgInsn);
REGISTER_INSN_DECODER(ExpInsn);

#undef REGISTER_INSN_DECODER

////////////////////////////////////////////////////////////////////////////////
// Usage
////////////////////////////////////////////////////////////////////////////////
static void Usage(int exitCode)
{
	FILE *const out = exitCode ? stderr : stdout;
	fprintf(out, "cik_isadisasm [options...] <filename>\n");
	fprintf(out, "options:\n");
	fprintf(out, "  --help        display this information\n");
	fprintf(out, "  -a<address>   start address display from specified value\n");
	fprintf(out, "  -e            stop after a S_ENDPGM\n");
	fprintf(out, "  -j<offset>    jump to offset in file\n");
	fprintf(out, "  -N<size>      process N bytes\n");
	exit(exitCode);
}

////////////////////////////////////////////////////////////////////////////////
// GetNum
////////////////////////////////////////////////////////////////////////////////
static size_t GetNum(const char *str)
{
	size_t tmp = 0xdecafbaddecafbad;
	const char *fmt = "%" SIZETFMT "u";
	if (str[0] == '0')
	{
		if (str[1] == 'x')
		{
			fmt = "0x%" SIZETFMT "x";
		}
		else if (str[1] == 'X')
		{
			fmt = "0X%" SIZETFMT "X";
		}
	}
	if (sscanf(str, fmt, &tmp) != 1)
	{
		Usage(1);
	}
	return tmp;
}

////////////////////////////////////////////////////////////////////////////////
// main
////////////////////////////////////////////////////////////////////////////////
int main(int argc, char **argv)
{
	// Parse command line args
	bool stopEnd = false;
	size_t offset = 0;
	size_t size   = ~offset;
	const char *filename = NULL;
	for (int i=1; i<argc; ++i)
	{
		const char *const arg = argv[i];
		if (arg[0] == '-')
		{
			if (strcmp(arg, "--help") == 0)
			{
				Usage(0);
			}
			else if (arg[1] == 'a')
			{
				g_Address = GetNum(arg+2);
				continue;
			}
			else if (arg[1] == 'e')
			{
				stopEnd = true;
				continue;
			}
			else if (arg[1] == 'j')
			{
				offset = GetNum(arg+2);
				continue;
			}
			else if (arg[1] == 'N')
			{
				size = GetNum(arg+2);
				continue;
			}
			else
			{
				Usage(1);
			}
		}
		if (filename)
		{
			Usage(1);
		}
		filename = arg;
	}
	if (!filename)
	{
		Usage(1);
	}

	// Open file
	FILE *file = fopen(filename, "rb");
	if (!file)
	{
		fprintf(stderr, "unable to open file \"%s\".\n", filename);
		exit(1);
	}
	_fseeki64(file, 0, SEEK_END);
	size_t fileSize = _ftelli64(file);
	offset = offset < fileSize ? offset : fileSize;
	size = (offset+size) < fileSize ? size : (fileSize-offset);
	u32 *buf = new u32[(size+3)/4];
	const u32 *const bufEnd = buf + (size/4);
	_fseeki64(file, offset, SEEK_SET);
	fread(buf, size, 1, file);
	fclose(file);

	// Disassemble
	const u32 *insn = buf;
	while (insn < bufEnd)
	{
		insn = InsnDecoder::Dump(insn);
		if (stopEnd && g_Endpgm)
		{
			break;
		}
	}

	delete[] buf;

	return 0;
}
