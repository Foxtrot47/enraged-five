#include <stdio.h>

void main(int argc,char **argv) {
	for (int i=1; i<argc; i++) {
		FILE *f = fopen(argv[i],"r+b");
		if (!f) {
			fprintf(stderr,"cannot open %s\n",argv[i]);
			continue;
		}
		fseek(f,0,SEEK_END);
		int size = ftell(f);
		char *buffer = new char[size + 1];
		fseek(f,0,SEEK_SET);
		fread(buffer,1,size,f);
		if (buffer[size-2] != '\r') {
			printf("file '%s' is missing newline\n",argv[i]);
			fflush(f);
			fseek(f,size,SEEK_SET);
			fputc('\r',f);
			fputc('\n',f);
		}
		fclose(f);
		delete[] buffer;
	}
}
