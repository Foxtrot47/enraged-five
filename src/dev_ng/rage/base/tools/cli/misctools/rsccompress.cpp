// 
// /rscdecompress.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "data/resourceheader.h"
#include "file/limits.h"
#include "file/stream.h"
#include "system/endian.h"
#include "system/param.h"

#include "system/xtl.h"
#include "paging/xcompress_7776.h"

#include <stdio.h>

using namespace rage;

#pragma comment(lib,"xcompress7776.lib")

static inline void Swapper(size_t &x) {
	size_t s = (x>>24) | (x<<24) | ((x>>8)&0xFF00) | ((x<<8)&0xFF0000);
	x = s;
}

static inline size_t to_ps3(bool ps3,size_t value) {
	return ps3?
		(value * 43) >> 5
		: value;
}

int main(int argc,char **argv) {
	sysParam::Init(argc, argv);

	for (int i=1; i<argc; i++) {
		char newName[RAGE_MAX_PATH];
		formatf(newName, RAGE_MAX_PATH, "%s.uncompressed", argv[i]);
		Printf("Compressing %s\n", newName);
		fiStream *S = fiStream::Open(newName);
		Assertf(S,"Unable to open file %s for reading",newName);
		if (S) {
			const char *ext = strrchr(argv[i],'.');
			const char *platform = "?????";
			bool swapped = true;
			if (ext && (ext[1]=='w'||ext[1]=='W')) {
				platform = "win32";
				swapped = false;
				AssertMsg(0, "win32 not supported");
			}
			else if (ext && (ext[1]=='x'||ext[1]=='X')) {
				platform = "xenon";
			}
			else if (ext && (ext[1]=='c'||ext[1]=='C')) {
				platform = "ps3";
				AssertMsg(0, "ps3 not supported");
			}
			datResourceFileHeader hdr;
			size_t uncompressedSize = S->Size() - sizeof(datResourceFileHeader);
			if (AssertVerify(S->Read(&hdr,sizeof(hdr)) == sizeof(hdr))) {
				if (swapped) {
					Swapper(hdr.Magic);
					Swapper((size_t&)hdr.Version);
					Swapper((size_t&)hdr.Info);
				}
				bool ps3 = hdr.Magic == datResourceFileHeader::c_MAGIC_PS3;
				Assertf(hdr.Magic == datResourceFileHeader::c_MAGIC_OTHER || ps3, "%s is not a valid resource file", argv[1]);
				if (hdr.Magic == datResourceFileHeader::c_MAGIC_OTHER || ps3) {
					Assert(uncompressedSize == hdr.Info.GetVirtualSize() + to_ps3(ps3,hdr.Info.GetPhysicalSize()));

					char* uncompressedData = rage_new char[uncompressedSize];
					Assert(uncompressedData);
					AssertVerify(S->Read(uncompressedData,uncompressedSize)==int(uncompressedSize));

					size_t compressedSize = uncompressedSize * 2;
					if (compressedSize < 4096)
						compressedSize = 4096;	// guarantee a certain minimum working size in case the file is very small.
					char* compressedData = rage_new char[compressedSize];
					Assert(compressedData);

					size_t finalSize = uncompressedSize;

					XMEMCOMPRESSION_CONTEXT compressionContext = NULL;
					XMEMCODEC_PARAMETERS_LZX codecParams = { 0, 128*1024, 512*1024 };
					if(FAILED(XMemCreateCompressionContext(XMEMCODEC_LZX, (VOID*)&codecParams, XMEMCOMPRESS_STREAM, &compressionContext)))
						Quitf("XMemCreateCompressionContext failed");

					SIZE_T inSize = uncompressedSize;
					SIZE_T outSize = compressedSize - 2*sizeof(u32);
					if (FAILED(XMemCompress(compressionContext,compressedData+2*sizeof(u32),&outSize,uncompressedData,inSize)))
						Quitf("XMemCompress failed.");

					XMemDestroyCompressionContext(compressionContext);

					compressedSize = outSize + 2*sizeof(u32);
					finalSize = outSize;

					delete [] uncompressedData;

					size_t magic = XCOMPRESS_FILE_IDENTIFIER_LZXNATIVE + XCOMPRESS_LZXNATIVE_VERSION_MINOR;
					if (swapped)
						Swapper(magic);
					*(size_t*)compressedData = magic;

					if (swapped)
						Swapper(finalSize);
					*((size_t*)compressedData+1) = finalSize;

					if (swapped) {
						Swapper(hdr.Magic);
						Swapper((size_t&)hdr.Version);
						Swapper((size_t&)hdr.Info);
					}

					char newName[RAGE_MAX_PATH];
					formatf(newName, RAGE_MAX_PATH, "%s.recompressed", argv[i]);
					fiStream *T = fiStream::Create(newName);
					Assertf(T,"Unable to open file %s for writing",newName);

					T->Write(&hdr,sizeof(hdr));
					AssertVerify(T->Write(compressedData,compressedSize)==int(compressedSize));
					T->Close();
					delete [] compressedData;
				}
			}
			S->Close();
		}
	}
}
