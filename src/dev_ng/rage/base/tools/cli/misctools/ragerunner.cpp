#include "system/main.h"

#include "atl/string.h"
#include "file/stream.h"
#include "file/token.h"
#include "string/string.h"
#include "system/param.h"

#pragma warning(disable: 4668)
#ifndef STRICT
#define STRICT
#endif

#include <windows.h>
#include <commctrl.h>
#pragma warning(error: 4668)
#pragma comment(lib,"comctl32.lib")

const char szAppName[] = "RageRunner";

using namespace rage;

const int MaxArgs = 256;
char *Args[MaxArgs];
int ArgCount = 0;


static void ClearArgs() {
	while (ArgCount)
		StringFree(Args[--ArgCount]);
}


static void AddArg(const char *arg) {
	if (ArgCount < MaxArgs)
		Args[ArgCount++] = StringDuplicate(arg);
}


static void AddDashedArg(const char *arg) {
	if (!*arg)
		return;
	Args[ArgCount] = new char[strlen(arg)+2];
	Args[ArgCount][0] = '-';
	strcpy(Args[ArgCount]+1,arg);
	++ArgCount;
}


static void ParseArgsFromFile(const char *filename) {
	fiStream *S = fiStream::Open(filename);
	if (S) {
		ClearArgs();
		char buffer[256];
		fiTokenizer T(filename, S);
		while (T.GetToken(buffer,sizeof(buffer)))
			AddArg(buffer);
		S->Close();
	}
}

static const char *Option(const char *opt) {
	// Remove the leading dash if it's there
	if (opt[0] == '-')
		++opt;
	return StringDuplicate(opt);
}

class Control {
public:
	Control(const char *windowText);
	static void Add(Control *newControl);
	static void ParseArgs(int argc,char **argv);
	virtual unsigned int ParseArg(const char *arg,const char *value) = 0;
	virtual void SaveArgs() = 0;
	virtual int AddTemplate(char *&tmp,int row,int col,int colWidth) = 0;
	virtual void Init(HWND /*hDlg*/) { }
	virtual void Command(HWND /*hDlg*/) { }
	virtual const char *GetTooltip() { return ""; }

	static int Run(const char *title,const char *fontName,int fontSize,int colWidth,int numCols,int numRows);
	static const char *GetExtraFlags() { return sm_ExtraFlags; }
protected:
	static void AddTemplateInternal(char *&tmp,int row,int col,int colWidth,WORD id,WORD atom,const char *text,DWORD style,DWORD exStyle);
	static BOOL CALLBACK DialogProc(HWND hDlg,UINT msg,WPARAM wParam,LPARAM lParam);
	const char *m_Text;
	WORD m_Id, m_TipId;
	static const int MaxControls = 256;
	static int sm_ControlCount;
	static WORD sm_Id;
	static Control *sm_Controls[MaxControls];
	static char sm_ExtraFlags[];
	enum { BUTTON=0x80, EDIT, STATIC, LISTBOX, SCROLLBAR, COMBOBOX };
	enum { EXTRAFLAGS = 10000 };
};

Control::Control(const char *windowText) : m_Text(StringDuplicate(windowText)), m_Id(0) {
}

int Control::sm_ControlCount;
WORD Control::sm_Id = 100;
Control* Control::sm_Controls[MaxControls];
char Control::sm_ExtraFlags[1024];

void Control::Add(Control *newControl) {
	Assert(sm_ControlCount < MaxControls);
	sm_Controls[sm_ControlCount++] = newControl;
}

void Control::ParseArgs(int argc,char **argv) {
	unsigned int matchcount = 0;
	for (int i=0; i<argc; i++) {
		if(!matchcount) {
			if (argv[i][0] == '-') {
				for (int j=0; j<sm_ControlCount; j++) {
					matchcount = max(sm_Controls[j]->ParseArg(argv[i]+1,i+1<argc&&argv[i+1][0]!='-'? argv[i+1] : ""), matchcount);
				}
			}
		}
		if(!matchcount) {
			if (sm_ExtraFlags[0])
				strcat(sm_ExtraFlags," ");
			strcat(sm_ExtraFlags,argv[i]);
		}
		else
			matchcount--;
	}
}


void Control::AddTemplateInternal(char *&tmp,int row,int col,int colWidth,WORD id,WORD atom,const char *text,DWORD style,DWORD exStyle) {
	tmp = (char*)(((u32)tmp + 3) & ~3);
	DLGITEMTEMPLATE *dit = (DLGITEMTEMPLATE*) tmp;
	dit->x = (short)(col * 4) + 4;
	dit->y = (short)(row * 12);
	dit->cx = (short)(colWidth * 4);
	dit->cy = (atom==COMBOBOX)? 64 : 12;
	dit->style = WS_CHILD | WS_VISIBLE | style;
	dit->dwExtendedStyle = exStyle;
	dit->id = id;
	LPWORD lpw = (LPWORD) (dit+1);
	*lpw++ = 0xFFFF;
	*lpw++ = (WORD) atom;
	u16 *wText = (u16*)lpw;
	do {
		*wText++ = *text;
	} while (*text++);
	*wText++ = 0;
	tmp = (char*)wText;
}


BOOL CALLBACK Control::DialogProc(HWND hDlg,UINT msg,WPARAM wParam,LPARAM /*lParam*/) {
	switch (msg) {
		case WM_INITDIALOG: {
			HWND hTip = CreateWindow(TOOLTIPS_CLASS,NULL,TTS_ALWAYSTIP,CW_USEDEFAULT,CW_USEDEFAULT,CW_USEDEFAULT,CW_USEDEFAULT,
				hDlg,0,GetModuleHandle(0),NULL); 
			for (int i=0; i<sm_ControlCount; i++) {
				sm_Controls[i]->Init(hDlg);
				TOOLINFO ti;
				ZeroMemory(&ti,sizeof(ti));
				ti.cbSize	= sizeof(TOOLINFO); 
				ti.uFlags	= TTF_IDISHWND | TTF_SUBCLASS ; 
				ti.hwnd		= GetDlgItem(hDlg,sm_Controls[i]->m_TipId); 
				ti.uId		= (UINT)ti.hwnd; 
				ti.lpszText = (LPSTR) sm_Controls[i]->GetTooltip();
				SendMessage(hTip,TTM_SETMAXTIPWIDTH,0,(LPARAM)(INT) 400); 
				SendMessage(hTip,TTM_ADDTOOL,0,(LPARAM) (LPTOOLINFO) &ti); 
			}
			if (sm_ExtraFlags[0])
				::SetDlgItemText(hDlg,EXTRAFLAGS,sm_ExtraFlags);
			return TRUE;
		}
		case WM_COMMAND: {
			if (wParam == IDOK || wParam == IDCANCEL) {
				if (wParam == IDOK) {
					ClearArgs();
					for (int i=0; i<sm_ControlCount; i++)
						sm_Controls[i]->SaveArgs();
					GetDlgItemText(hDlg,EXTRAFLAGS,sm_ExtraFlags,sizeof(sm_ExtraFlags));
				}
				EndDialog(hDlg,wParam == IDOK);
			}
			else {
				for (int i=0; i<sm_ControlCount; i++)
					if (sm_Controls[i]->m_Id == LOWORD(wParam))
						sm_Controls[i]->Command(hDlg);
			}
			return TRUE;
		}
		default:
			return FALSE;
	}
}


int Control::Run(const char *title,const char *fontName,int fontSize,int colWidth,int numCols,int numRows) {
	InitCommonControls();

	// See ms-help://MS.VSCC.2003/MS.MSDNQTR.2003APR.1033/winui/winui/windowsuserinterface/windowing/dialogboxes/usingdialogboxes.htm
	// for this crazy bullshit.
	DLGTEMPLATE *dlgTemplate = (DLGTEMPLATE*) new char[256 * 1024];
	dlgTemplate->dwExtendedStyle = WS_EX_TOPMOST;
	dlgTemplate->style = WS_POPUP | WS_BORDER | WS_SYSMENU | DS_CENTER | DS_MODALFRAME | WS_CAPTION | DS_SETFONT;
	dlgTemplate->x = dlgTemplate->y = 0;
	dlgTemplate->cx = (short)((colWidth+2) * numCols * 4);
	int minRows = (sm_ControlCount +  numCols - 1) / numCols;
	if (numRows < minRows)
		numRows = minRows;
	dlgTemplate->cy = (short)(numRows * 12) + 24;
	LPWORD lpw = (LPWORD)(dlgTemplate+1);
	*lpw++ = 0;	// no menu
	*lpw++ = 0;	// predefined dialog box class
	u16 *wTitle = (u16*)lpw;
	do {
		*wTitle++ = *title;
	} while (*title++);
	*wTitle++ = (u16) fontSize;
	do {
		*wTitle++ = *fontName;
	} while (*fontName++);
	char *ptr = (char*)wTitle;
	int templateCount = 0;
	int row = 0, col = 0;
	for (int i=0; i<sm_ControlCount; i++) {
		templateCount += sm_Controls[i]->AddTemplate(ptr,row,col,colWidth);
		 if (++row == numRows) {
			 col += colWidth + 2;
			 row = 0;
		 }
	}
	const int indent = 11;
	AddTemplateInternal(ptr,numRows,0,indent,0,STATIC,"Extra flags:",SS_CENTER | SS_NOPREFIX,0);
	AddTemplateInternal(ptr,numRows,indent,(colWidth+1) * numCols - indent,EXTRAFLAGS,EDIT,"",WS_BORDER | ES_AUTOHSCROLL,0);
	AddTemplateInternal(ptr,numRows+1,(colWidth+2) * (numCols-1),colWidth,IDOK,BUTTON,"Launch",BS_DEFPUSHBUTTON,0);
	dlgTemplate->cdit = (WORD) (templateCount+3);
	return DialogBoxIndirect(GetModuleHandle(NULL), dlgTemplate, NULL, Control::DialogProc);
}


class CheckBox: public Control {
public:
	CheckBox(const char *desc,const char *onvalue,const char *offvalue) : Control(desc), m_OnValue(Option(onvalue)), m_OffValue(Option(offvalue)), m_Value(false) { }
	unsigned int ParseArg(const char *arg,const char * /*value*/) {
		if (!stricmp(arg,m_OnValue))
			m_Value = 1;
		else if (!stricmp(arg,m_OffValue))
			m_Value = 0;
		else
			return 0;
		return 1;
	}
	void SaveArgs() {
		AddDashedArg(m_Value? m_OnValue : m_OffValue);
	}
	int AddTemplate(char *&tmp,int row,int col,int colWidth) {
		AddTemplateInternal(tmp,row,col,colWidth,m_TipId = m_Id = sm_Id++,BUTTON,m_Text,BS_AUTOCHECKBOX,0);
		return 1;
	}
	void Init(HWND hDlg) {
		SendMessage(GetDlgItem(hDlg,m_Id),BM_SETCHECK,m_Value,0);
	}
	void Command(HWND hDlg) {
		m_Value = SendMessage(GetDlgItem(hDlg,m_Id),BM_GETCHECK,0,0) != 0;
	}
	const char *GetTooltip() { return *m_OnValue? m_OnValue : m_OffValue; }
private:
	const char *m_OnValue, *m_OffValue;
	bool m_Value;
};


class StaticText: public Control {
public:
	StaticText(const char *text) : Control(text) { }
	unsigned int ParseArg(const char*,const char*) { return 0; }
	void SaveArgs() { }
	int AddTemplate(char *&tmp,int row,int col,int colWidth) {
		AddTemplateInternal(tmp,row,col,colWidth,m_TipId = m_Id = 99,STATIC,m_Text,SS_CENTER | SS_NOPREFIX,0);
		return 1;
	}
};



class EditBox: public Control {
public:
	EditBox(const char *desc,const char *optvalue,int editWidth) : Control(desc), m_OptValue(Option(optvalue)), m_String(StringDuplicate("")), m_EditWidth(editWidth) { }
	unsigned int ParseArg(const char *arg,const char *value) {
		if (!stricmp(arg,m_OptValue)) {
			StringFree(m_String);
			m_String = StringDuplicate(value);
			return m_String[0] ? 2 : 1;
		}
		else
			return 0;
	}
	void SaveArgs() {
		if (*m_String) {
			AddDashedArg(m_OptValue);
			AddArg(m_String);
		}
	}
	int AddTemplate(char *&tmp,int row,int col,int colWidth) {
		AddTemplateInternal(tmp,row,col,colWidth-m_EditWidth,m_TipId = sm_Id++,STATIC,m_Text,0,0);
		AddTemplateInternal(tmp,row,col+colWidth-m_EditWidth,m_EditWidth,m_Id = sm_Id++,EDIT,m_String,WS_BORDER | ES_AUTOHSCROLL,0);
		return 2;
	}
	void Command(HWND hDlg) {
		char buffer[256];
		GetDlgItemText(hDlg,m_Id,buffer,sizeof(buffer));
		StringFree(m_String);
		m_String = StringDuplicate(buffer);
	}
	const char *GetTooltip() { return m_OptValue; }
protected:
	const char *m_OptValue;
	const char *m_String;
	int m_EditWidth;
};

class ComboBox: public EditBox {
public:
	ComboBox(const char *desc,const char *optvalue,const char *items,int editWidth) : EditBox(desc,optvalue,editWidth), m_Items(StringDuplicate(items)) { }
	unsigned int ParseArg(const char *arg,const char *value) {
		if (!stricmp(arg,m_OptValue)) {
			if(!value[0])
				return 1;
			bool match = false;
			char items[256], *ip = items;
			strcpy(items,m_Items);
			while ((ip = strtok(ip,";")) != 0) {
				if(!stricmp(ip,value))
				{
					match = true;
					StringFree(m_String);
					m_String = StringDuplicate(value);
					break;
				}
				ip = NULL;
			}
			return match ? 2 : 1;
		}
		else
			return 0;
	}
	int AddTemplate(char *&tmp,int row,int col,int colWidth) {
		AddTemplateInternal(tmp,row,col,colWidth-m_EditWidth,m_TipId = sm_Id++,STATIC,m_Text,0,0);
		AddTemplateInternal(tmp,row,col+colWidth-m_EditWidth,m_EditWidth,m_Id = sm_Id++,COMBOBOX,m_String,CBS_DROPDOWN | WS_VSCROLL | WS_BORDER | CBS_AUTOHSCROLL,0);
		return 2;
	}
	void Init(HWND hDlg) {
		char items[256], *ip = items;
		strcpy(items,m_Items);
		while ((ip = strtok(ip,";")) != 0) {
			SendMessage(GetDlgItem(hDlg,m_Id),CB_ADDSTRING,0,(long)ip);
			ip = NULL;
		}
		SetDlgItemText(hDlg,m_Id,m_String);
	}
	void Command(HWND hDlg) {
		char buffer[256];
		GetDlgItemText(hDlg,m_Id,buffer,sizeof(buffer));
		StringFree(m_String);
		m_String = StringDuplicate(buffer);
	}
private:
	const char *m_Items;
};



static void ExitBox(const char *fmt,...) {
	char buffer[1024];
	va_list args;
	va_start(args,fmt);
	vsprintf(buffer,fmt,args);
	strcat(buffer,
		"\nResponse file will be created if it doesn't exist.  It contains initial state for args and will contain final state on exit.\n"
		"The .ini file format is documented in http://agedocs/cgi-bin/wiki.pl?RageRunner" ".\n"
		"\nWould you like to open a browser window for the documentation now?");
	if (MessageBox(NULL,buffer,szAppName,MB_YESNO | MB_DEFBUTTON2 | MB_ICONQUESTION) == IDYES)
		ShellExecute(NULL,"open","http://agedocs/cgi-bin/wiki.pl?RageRunner",0,0,SW_SHOWNORMAL);
}

int Main() {
	int argc = sysParam::GetArgCount();
	char **argv = sysParam::GetArgArray();

	if (argc != 3) {
		ExitBox("Usage: ragerunner <ini-name> <responsefile-name>");
		return 2;
	}

	fiSafeStream S(fiStream::Open(argv[1]));

	if (S) {
		const char *windowTitle = szAppName;		// Default dialog box title
		const char *fontName = "Tahoma";
		int fontSize = 10;
		int colWidth = 30;
		int numCols = 1;
		int numRows = 1;
		char buffer[512];
		fiTokenizer *T = new fiTokenizer(argv[1], S);
		int includeDepth = 0;
		const int maxDepth = 4;
		fiTokenizer *includes[maxDepth];
		for (;;) {
			while (T->GetToken(buffer,sizeof(buffer))) {
				if (!stricmp(buffer,"INCLUDE")) {
					if (includeDepth == maxDepth) {
						ExitBox("Too many nested includes");
						return 2;
					}
					T->GetToken(buffer,sizeof(buffer));
					fiStream *newS = fiStream::Open(buffer);
					if (!newS) {
						ExitBox("Unable to locate include file '%s'",buffer);
						return 2;
					}
					includes[includeDepth++] = T;
					T = new fiTokenizer(buffer, newS);
				}
				else if (!stricmp(buffer,"TITLE")) {
					T->GetToken(buffer,sizeof(buffer));
					windowTitle = StringDuplicate(buffer);
				}
				else if (!stricmp(buffer,"FONT")) {
					T->GetToken(buffer,sizeof(buffer));
					fontName = StringDuplicate(buffer);
					fontSize = T->GetInt();
				}
				else if (!stricmp(buffer,"SIZE")) {
					colWidth = T->GetInt();
					numCols = T->GetInt();
					numRows = T->GetInt();
				}
				else if (!stricmp(buffer,"BEGINDEFAULTS")) {
					ClearArgs();
					for(;;) {
						if (!T->GetToken(buffer,sizeof(buffer)) || !stricmp(buffer,"ENDDEFAULTS"))
							break;
						AddArg(buffer);
					}
				}
				else if (!stricmp(buffer,"CHECKBOX")) {
					T->GetToken(buffer,sizeof(buffer));
					char onvalue[32], offvalue[32];
					T->GetToken(onvalue,sizeof(onvalue));
					T->GetToken(offvalue,sizeof(offvalue));
					Control::Add(new CheckBox(buffer,onvalue,offvalue));
				}
				else if (!stricmp(buffer,"EDITBOX")) {
					T->GetToken(buffer,sizeof(buffer));
					char optname[32];
					T->GetToken(optname,sizeof(optname));
					Control::Add(new EditBox(buffer,optname,T->GetInt()));
				}
				else if (!stricmp(buffer,"COMBOBOX")) {
					T->GetToken(buffer,sizeof(buffer));
					char optname[32], items[256];
					T->GetToken(optname,sizeof(optname));
					T->GetToken(items,sizeof(items));
					Control::Add(new ComboBox(buffer,optname,items,T->GetInt()));
				}
				else if (!stricmp(buffer,"TEXT")) {
					T->GetToken(buffer,sizeof(buffer));
					Control::Add(new StaticText(buffer));
				}
				else {
					ExitBox("Unknown command '%s' on line '%d'",buffer,T->line);
					return 2;
				}
			}
			delete T;
			if (includeDepth)
				T = includes[--includeDepth];
			else
				break;
		}

		ParseArgsFromFile(argv[2]);

		Control::ParseArgs(ArgCount,Args);

		int result = Control::Run(windowTitle,fontName,fontSize,colWidth,numCols,numRows);
		if (result == 1) {	// Ok
			fiStream *S = fiStream::Create(argv[2]);
			if (S) {
				for (int i=0; i<ArgCount; i++)
					fprintf(S,strchr(Args[i],' ')?"\"%s\"\r\n" : "%s\r\n",Args[i]);
				fprintf(S,"%s\r\n",Control::GetExtraFlags());
				S->Close();
			}
			return 0;
		}
		else
			return 1;
	}
	else {
		ExitBox("Unable to open ini file '%s'",argv[1]);
		return 2;
	}
}
