#define _CRT_SECURE_NO_DEPRECATE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <direct.h>
#include <time.h>

#define STRICT
#include <windows.h>


void print_item(int color,const char *tag,const char *file) {
	HANDLE h = GetStdHandle(STD_OUTPUT_HANDLE);
	int sl = strlen(tag);
	static char spaces[] = "                                             ";
	SetConsoleTextAttribute(h,color);
	printf("%s",tag);
	fflush(stdout);
	SetConsoleTextAttribute(h,7);
	const int margin = 26;
	if (sl < margin)
		printf("%*.*s",margin-sl,margin-sl,spaces);
	printf("%s\n",file);
	fflush(stdout);
}

void print_help(char* argv0)
{
	printf("Usage: %s [-full] [-local] [-backup] [-help] [<filename>]\n", argv0);
	printf("   -full - Lists status of every file, including those checked out by others\n");
	printf("   -local - Lists status of every file, with respect to the current workspace\n");
	printf("   -backup - Creates a .zip archive of all locally checked out files\n");
	printf("   -help - Prints this message\n");
	printf("   <filename> - Prints the status for the specified file (wildcards allowed)\n");
}

int main(int argc,char **argv)
{
	char buf[512];
	char cwd[512];
	bool full = false;
	char cmdline[512];
	bool filespec = false;
	bool backup = false;
	strcpy(cmdline,"p4 fstat");
	
	if (argc <= 1)
	{
		print_help(argv[0]);
		return 1;
	}

	for (int i=1; i<argc; i++) {
		if (!strcmp(argv[i], "-help"))
		{
			print_help(argv[0]);
			return 1;
		}
		else if (!strcmp(argv[i],"-full"))
			full = true;
		else if (!strcmp(argv[i],"-local"))
			strcat(cmdline," *");
		else if (!strcmp(argv[i],"-backup"))
			backup = true;
		else if (argv[i][0] != '-') {
			strcat(cmdline," .../");
			strcat(cmdline,argv[i]);
			filespec = true;
		}
	}

	if (!filespec)
		strcat(cmdline," ...");

	FILE *p = _popen(cmdline,"rt");
	if (!p) {
		fprintf(stderr,"p4status - cannot find p4 exe\n");
		return 1;
	}

	_getcwd(cwd, sizeof(cwd));
	int cwdLen = strlen(cwd) + 1;

	char currentFile[512];
	int state = 0;
	char author[128];
	char action[128];
	int headRev = 0;

	FILE *response = NULL;
	if (backup) {
		response = fopen("p4backup.rsp","w");
		backup = false;	// will become true again if we have files
	}

	while (fgets(buf,sizeof(buf),p)) {
		int sl = strlen(buf);
		while (sl-- && buf[sl]=='\r' || buf[sl]=='\n')
			buf[sl] = '\0';

		if (sl < 4) {
			continue;
		}

		if (!strncmp(buf+4,"clientFile ",10)) {
			strcpy(currentFile,buf+4+11+cwdLen);
			state = 1;
		}
		else if (!strncmp(buf+4,"headRev ",8)) {
			headRev = atoi(buf+4+8);
		}
		else if (!strncmp(buf+4,"haveRev ",8)) {
			int haveRev = atoi(buf+4+8);
			if (haveRev < headRev)
				print_item(6+8,"needs-sync:local",currentFile);
		}
		else if (!strncmp(buf+4,"action ",7)) {
			sprintf(action,"%s:local",buf+4+7);
			print_item(7,action,currentFile);
			state = 2;
			if (response && (!strcmp(buf+4+7,"edit") || !strcmp(buf+4+7,"add"))) {
				fprintf(response,"%s\n",currentFile);
				backup = true;
			}
			
		}
		else if (!strncmp(buf+4,"... otherOpen",13)) {
			strcpy(author,strchr(buf+4+13,32)+1);
		}
		else if (!strncmp(buf+4,"... otherAction",15)) {
			sprintf(action,"%s:%s",strchr(buf+4+15,32)+1,author);
			if (state == 2 || full)
				print_item(5+8,action,currentFile);
		}
	}

	_pclose(p);

	if (response) {
		fclose(response);

		if (backup) {
			struct tm *newtime;
		       time_t now;

		        time( &now );
		        newtime = localtime( &now); // C4996

			const char *homedrive = getenv("HOMEDRIVE");
			const char *homepath = getenv("HOMEPATH");
			const char *override = getenv("P4STATUSDIR");
			if (!homedrive) homedrive = ".";
			if (!homepath) homepath = "\\";

			const char *studio = getenv("ROCKSTAR_STUDIO");
			if (studio && !strcmp(studio,"rockstarsandiego.com")) {
				homedrive = "N:\\RSGSAN\\USER\\";
				homepath = getenv("USERNAME");
			}
			if (override) {
				homedrive = override;
				homepath = "";
			}

			char zipcmd[256];
			sprintf(zipcmd,"zip -@ \"%s%s\\p4backup_%04d_%02d_%02d_%02d.%02d.%02d.zip\" < p4backup.rsp",
				homedrive, homepath,
				newtime->tm_year + 1900, newtime->tm_mon+1, newtime->tm_mday,
				newtime->tm_hour, newtime->tm_min, newtime->tm_sec);
			puts(zipcmd);
			system(zipcmd);
			system("del p4backup.rsp");
		}
	}
}
