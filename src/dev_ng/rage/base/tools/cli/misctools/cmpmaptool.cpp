// 
// /cmpmaptool.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "system/main.h"
#include "system/param.h"
#include "system/stack.h"

FPARAM(1, cmpmapName, "Name of the .cmpmap file");
PARAM(addr, "Find the symbol at a given address");

using namespace rage;

int Main()
{
	const char* cmpmapName = NULL;
	PARAM_cmpmapName.Get(cmpmapName);

	if(!cmpmapName)
	{
		Displayf("Usage: %s <cmpmapname> -addr <addr>", sysParam::GetProgramName());
		return 1;
	}

	sysStack::ShutdownClass(); // we don't want info for this program

	sysStack::InitClass(cmpmapName);

	const char* addrStr = NULL;
	u32 addr = 0;
	if (PARAM_addr.Get(addrStr))
	{
		addr = strtoul(addrStr, NULL, 0);
		char symbol[1024];
		int offset = sysStack::ParseMapFileSymbol(symbol, 1024, addr, false);
		Displayf("Addr is %s+%x", symbol, offset);
	}

	return 0;
}