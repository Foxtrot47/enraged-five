// 
// Solution to test different memory configuration changes
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// Eric J Anderson
// 

#include <ctime>
#include <list>
#include <iostream>
#include <cstdio>
using namespace std;

#include "math/simplemath.h"
#include "system/bestfitallocator.h"
#include "system/poolallocator.h"
using namespace rage;

// Defines
#define ITERATIONS (1 * 1024 * 1024)
#define ACTION_RATIO 1 // This number represents the value of "malloc" in the malloc:free ratio

#define CAPACITY (10 << 20)
#define SIZE (32767)
#define MAX_SIZE (512)
#define MIN_SIZE (1)

const size_t g_align[] = {4, 8, 16, 32, 64};
typedef std::list<void*> pointer_list;
pointer_list g_pointers;

enum {MALLOC, FREE};
size_t g_debug_action[2] = {0};

size_t GetRandomSize()
{
	const size_t value = MIN_SIZE + (rand() % (MAX_SIZE - MIN_SIZE));
	return AlignPow2(value, 4);
}

size_t GetRandomAlignment()
{
	const size_t pos = rand() % (sizeof(g_align) / sizeof(size_t));
	Assert(pos >= 0 && pos < 5);
	return g_align[pos];
}

bool GetRandomAction()
{
	return (rand() % (ACTION_RATIO + 1)) > 0;
}

char GetRandomChar()
{
	return (rand() % 135) + 40;
}

void* GetRandomUsedPtr()
{
	if (g_pointers.empty())
		return NULL;

	//Displayf("Count = %d", g_pointers.size());
	const size_t pos = rand() % g_pointers.size();
	pointer_list::iterator it = g_pointers.begin();
	for (size_t i = 0; i < pos; ++i)
		it++;

	return *it;
}

void PrintInfo()
{
	Displayf("Iterations: %d", ITERATIONS);
	Displayf("Malloc/Free: %d", ACTION_RATIO);
	Displayf("Capacity: %d", CAPACITY);
	Displayf("Max Size: %d", MAX_SIZE);
	Displayf("Min Size: %d", MIN_SIZE);
	Displayf("");

	Displayf("Min Free Block Size: %d", sysMemPoolAllocator::s_minFreeBlockSize);
	Displayf("Max Size: %d", sysMemPoolAllocator::s_maxCapacity);
	Displayf("");
}

void* AlignSize16(void* ptr) { return (void*) ((((size_t) ptr) + 0xF) & ~0xF); }

void main()
{
	PrintInfo();

	srand ((u32) time(NULL));

	char* const memory = rage_new char[CAPACITY];
	void* const aligned = AlignSize16(memory);

	/*sysMemPoolAllocator* pAllocator = rage_new sysMemPoolAllocator();
	sysMemPoolAllocator::PoolWrapper<void>* pPool = rage_new sysMemPoolAllocator::PoolWrapper<void>(pAllocator);
	pAllocator->Init(aligned, CAPACITY, SIZE);*/
	sysMemBestFitAllocator* pAllocator = rage_new sysMemBestFitAllocator();
	pAllocator->Init(memory, CAPACITY);
	pAllocator->SanityCheck();

	char ch = 1;

	for (int i = 0; i < ITERATIONS; ++i)
	{
		bool should_malloc = GetRandomAction();

		if (should_malloc)
		{
			// Malloc
			g_debug_action[MALLOC]++;

			const size_t bytes = GetRandomSize();
			const size_t align = GetRandomAlignment();

			void* ptr = pAllocator->Allocate(bytes, align);
			if (ptr && pAllocator->IsValidPointer(ptr))
			{
				//Displayf("Allocate: %p, Size: %d", ptr, pAllocator->GetSize(ptr));
				sysMemSet(ptr, ch++, bytes);
				g_pointers.push_back(ptr);
				pAllocator->SanityCheck();
			}

			
			/*bool status = pPool->IsValidPtr(ptr);
			Displayf("Status = %d", status);
			status = pPool->IsValidPtr((void*) 0xEEEEEEEE);
			Displayf("Status = %d", status);*/
		}
		else
		{
			// Free
			g_debug_action[FREE]++;

			void* ptr = GetRandomUsedPtr();
			if (ptr)
			{
				//Displayf("Free: %p, Size: %d", ptr, pAllocator->GetSize(ptr));
				g_pointers.remove(ptr);
				pAllocator->Free(ptr);			
				pAllocator->SanityCheck();
			}
		}

		printf("\b\b\b\b\b\b\b\b%d", i);
	}

	pAllocator->SanityCheck();

	printf("\b\b\b\b\b\b\b\b");
	Displayf("Total: %d, Used: %d, Free: %d", pAllocator->GetHeapSize(), pAllocator->GetMemoryUsed(), pAllocator->GetMemoryAvailable());
	//Displayf("Pool Size: %d, Pool Count: %d", pAllocator->GetSize(), pAllocator->GetCount());
	pAllocator->SanityCheck();

	pAllocator->Reset();
	pAllocator->SanityCheck();

	Displayf("Action: %d / %d", g_debug_action[0], g_debug_action[1]);

	// Clean-up
	//delete pPool;
	delete pAllocator;
	delete [] memory;
}
