// 
// /hashstring.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#include "string/stringhash.h"
#include "system/main.h"
#include "system/param.h"

#include <cstdio>

using namespace rage;
using namespace std;

PARAM(literal, "Use atLiteralStringHash (case-sensitive)");
PARAM(hex, "Output hexidecimal instead of decimal");

int Main()
{
	bool literal = PARAM_literal.Get();
	bool hex = PARAM_hex.Get();
	char s[256];
	while(fscanf(stdin, "%s", s))
	{
		u32 hash = literal ? atLiteralStringHash(s) : atStringHash(s);
		printf(hex ? "%x\n" : "%u\n", hash);
	}
	return 0;
}
