#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define STRICT
#include <windows.h>
#pragma comment(lib,"user32.lib")

typedef unsigned u32;

u32 atStringHash( const char *string, const u32 initValue  = 0)
{
	// This is the one-at-a-time hash from this page:
	// http://burtleburtle.net/bob/hash/doobs.html
	// (borrowed from /soft/swat/src/swcore/string2key.cpp)

	bool quotes = (*string == '\"');
	u32 key = initValue;

	if (quotes) 
		string++;

	while (*string && (!quotes || *string != '\"'))
	{
		char character = *string++;
		if (character >= 'A' && character <= 'Z') 
			character += 'a'-'A';
		else if (character == '\\')
			character = '/';

		key += character;
		key += (key << 10);	//lint !e701
		key ^= (key >> 6);	//lint !e702
	}
	key += (key << 3);	//lint !e701
	key ^= (key >> 11);	//lint !e702
	key += (key << 15);	//lint !e701

	// The original swat code did several tests at this point to make
	// sure that the resulting value was representable as a valid
	// floating-point number (not a negative zero, or a NaN or Inf)
	// and also reserved a nonzero value to indicate a bad key.
	// We don't do this here for generality but the caller could
	// obviously add such checks again in higher-level code.
	return key;
}

int fgetline(FILE *f,char *dest,int destSize) {
	int ch;
	int stored = 0;
	while ((ch = fgetc(f)) != EOF) {
		if (ch >= 32 && destSize > 1) {
			*dest++ = ch;
			--destSize;
			++stored;
		}
		if (ch == 10)
			break;
	}
	*dest = 0;
	return stored;
}



int main(int argc,char **argv)
{
	bool stripext = false;

	if (argc != 2) {
		fprintf(stderr,"usage: scrhasher string-to-hash\n");
		return 2;
	}

	unsigned hash = atStringHash(argv[1]);
	char buf[256];
	sprintf(buf,"SCR_REGISTER_SECURE(%s, 0x%08x, ::scripting::%s)",
		argv[1], hash, argv[1]);

	OpenClipboard(NULL);
	EmptyClipboard();
	HANDLE newHandle = GlobalAlloc(GMEM_MOVEABLE,strlen(buf)+1);
	char *newS = (char*) GlobalLock(newHandle);
	strcpy(newS, buf);
	GlobalUnlock(newHandle);
	SetClipboardData(CF_TEXT,newHandle);
	CloseClipboard();

	puts(buf);
	return 0;
}
