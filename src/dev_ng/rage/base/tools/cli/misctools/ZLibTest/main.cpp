
#define _CRT_SECURE_NO_DEPRECATE
#define MY_ZCALLOC

#include "system/new.h"
#include "zlib/zutil.h"

//#define Z_PREFIX
//#define ZLIB_WINAPI

#include <cassert>
#include <chrono>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <vector>

#include "zlib/lz4.h"
#include "zlib/zlib.h"

using namespace std;
using namespace std::chrono;

string g_path;
const size_t g_size = (256 << 20);
const size_t g_worst = LZ4_compressBound(g_size);

char* g_raw = nullptr;
char* g_uncompressed = nullptr;
char* g_compressed = nullptr;

extern "C" void* rage_malloc(size_t siz) 
{
	return rage_new char[siz];
}

extern "C" void rage_free(void *ptr) 
{
	delete[](char*) ptr;
}

void ReadAllBytes(char const* filename, char* data)
{
	ifstream ifs(filename, ios::binary | ios::ate);

	if (ifs.good())
	{
		ifstream::pos_type pos = ifs.tellg();

		ifs.seekg(0, ios::beg);
		ifs.read(data, pos);
	}
	else
	{
		cout << "BAD FILE: " << filename << endl;
	}
}

void Init()
{
	high_resolution_clock::time_point start = high_resolution_clock::now();

	g_raw = new char[g_size];
	g_uncompressed = new char[g_size];
	g_compressed = new char[g_worst];

	high_resolution_clock::time_point stop = high_resolution_clock::now();
	milliseconds ms = duration_cast<milliseconds>(stop - start);
	cout << "New: " << ms.count() << " ms" << endl;

	// Random
	//start = high_resolution_clock::now();

	/*for (size_t i = 0; i < g_size; ++i)
	g_raw[i] = rand() % 256;*/

	//ms = duration_cast<milliseconds>(high_resolution_clock::now() - start);
	//cout << "Randomize: " << ms.count() << " ms" << endl;
}

void Read(const char* path)
{
	memset(g_raw, 0, g_size);

	g_path = path;
	ReadAllBytes(path, g_raw);
}

void Shutdown()
{
	high_resolution_clock::time_point start = high_resolution_clock::now();

	delete[] g_raw;
	delete[] g_uncompressed;
	delete[] g_compressed;

	milliseconds ms = duration_cast<milliseconds>(high_resolution_clock::now() - start);
	cout << "Delete: " << ms.count() << " ms" << endl;
}

void Clear()
{
	memcpy(g_uncompressed, g_raw, g_size);
	memset(g_compressed, 0, g_worst);
}

void Compress_LZ4(const size_t bytes, size_t& size, size_t& time)
{
	Clear();

	//const clock_t start = clock();
	high_resolution_clock::time_point start = high_resolution_clock::now();

	size = LZ4_compress(g_uncompressed, g_compressed, bytes);

	//const double duration = (clock() - start) / static_cast<double>(CLOCKS_PER_SEC);
	milliseconds ms = duration_cast<milliseconds>(high_resolution_clock::now() - start);	
	double ratio = static_cast<double>(bytes - size) / static_cast<double>(bytes);
	double percent = ratio * 100.0;
	time += (size_t) ms.count();

	cout << "File=" << g_path.c_str() << ", Input=" << bytes << ", Output=" << size << ", Ratio=" << setprecision(2) << percent << "%, Elapsed=" << ms.count() << " ms" << endl;
}

size_t Decompress_LZ4(char* input, char* output, const size_t size)
{
	return LZ4_decompress_fast(input, output, size);
}

void Compress_ZLib(const size_t bytes, size_t& size, size_t& time)
{
	Clear();

	//const clock_t start = clock();
	high_resolution_clock::time_point start = high_resolution_clock::now();
	
	z_stream stream;
	stream.next_in = (Bytef*) g_uncompressed;
	stream.avail_in = (uInt)bytes;	
	stream.next_out = (Bytef*) g_compressed;
	stream.avail_out = (uInt) g_worst;
	
	stream.zalloc = 0;
	stream.zfree = 0;
	stream.opaque = (voidpf) 0;

	int err = deflateInit(&stream, Z_BEST_SPEED);
	if (err != Z_OK)
		return;// err;

	err = deflate(&stream, Z_FINISH);
	if (err != Z_STREAM_END)
	{
		deflateEnd(&stream);
		return;// err == Z_OK ? Z_BUF_ERROR : err;
	}

	size = stream.total_out;

	milliseconds ms = duration_cast<milliseconds>(high_resolution_clock::now() - start);	
	double ratio = static_cast<double>(bytes - size) / static_cast<double>(bytes);
	double percent = ratio * 100.0;
	time += (size_t) ms.count();

	cout << "File=" << g_path.c_str() << ", Input=" << bytes << ", Output=" << size << ", Ratio=" << setprecision(2) << percent << "%, Elapsed=" << ms.count() << " ms" << endl;
}

//void LZ4_HC(const size_t bytes)
//{
//	Clear();
//
//	//const clock_t start = clock();
//	high_resolution_clock::time_point start = high_resolution_clock::now();
//
//	int size = LZ4_compressHC(g_source, g_dest, bytes);
//
//	//const double duration = (clock() - start) / static_cast<double>(CLOCKS_PER_SEC);
//	milliseconds ms = duration_cast<milliseconds>(high_resolution_clock::now() - start);
//
//	double ratio = static_cast<double>(bytes - size) / static_cast<double>(bytes);
//	double percent = ratio * 100.0;
//	cout << "File=" << g_path.c_str() << ", Input=" << bytes << ", Output=" << size << ", Ratio=" << setprecision(2) << percent << "%, Elapsed=" << ms.count() << " ms" << endl;
//}

void main(int argc, char* argv[])
{
	srand(time_t(NULL));

	Init();

	const size_t loopCount = 1;// 5;
	cout << "Loop Count = " << loopCount << endl;

	const size_t inputCount = 1;// 22;
	cout << "Input Count = " << inputCount << endl;

	const size_t blockSize = 4 << 20;
	cout << "Block Size = " << blockSize << endl;
	cout << endl;

	// Read
	Read("D:\\uncompressed\\replay_01.dat");
	memcpy(g_uncompressed, g_raw, g_size);
	memset(g_compressed, 0, g_worst);

	// Compress
	size_t compressedSize = LZ4_compress(g_uncompressed, g_compressed, blockSize);
	memset(g_uncompressed, 0, g_size);

	size_t result = LZ4_decompress_fast(g_compressed, g_uncompressed, blockSize);
	cout << "Sizes Equal: " << (result == compressedSize) << endl;

	int value = memcmp(g_uncompressed, g_raw, result);
	cout << "Raw == Source: " << (bool) (value == 0) << endl;

	// LZ4
	/*{
		size_t total = 0;
		size_t ms = 0;
		cout << "LZ4" << endl;

		for (int i = 0; i < loopCount; ++i)
		{
			for (size_t j = 0; j < inputCount; ++j)
			{
				char path[64] = { 0 };
				sprintf(path, "D:\\uncompressed\\replay_%02d.dat", j + 1);
				Read(path);

				size_t size = 0;
				Compress_LZ4(blockSize, size, ms);
				total += size;
			}
		}		

		double ratio = static_cast<double>((loopCount * blockSize * inputCount) - total) / static_cast<double>(loopCount * blockSize * inputCount);
		double percent = ratio * 100.0;
		cout << "Input=" << (loopCount * blockSize * inputCount) << ", Output=" << total << ", Avg Ratio=" << setprecision(2) << percent << "%, Avg Elapsed=" << setprecision(2) << ((double)ms / (inputCount * loopCount)) << " ms" << endl;
		cout << endl;
	}

	// ZLib
	{
		size_t total = 0;
		size_t ms = 0;
		cout << "ZLib" << endl;

		for (int i = 0; i < loopCount; ++i)
		{
		for (size_t j = 0; j < inputCount; ++j)
		{
		char path[64] = { 0 };
		sprintf(path, "D:\\uncompressed\\replay_%02d.dat", j + 1);
		Read(path);

		size_t size = 0;
		Compress_ZLib(blockSize, size, ms);
		total += size;
		}
		}

		double ratio = static_cast<double>((loopCount * blockSize * inputCount) - total) / static_cast<double>(loopCount * blockSize * inputCount);
		double percent = ratio * 100.0;
		cout << "Input=" << (loopCount * blockSize * inputCount) << ", Output=" << total << ", Avg Ratio=" << setprecision(2) << percent << "%, Avg Elapsed=" << setprecision(2) << ((double)ms / (inputCount * loopCount)) << " ms" << endl;
		cout << endl;
	}*/

	// LZ4 HC
	/*cout << "LZ4 HC" << endl;
	for (int i = 1; i <= 22; ++i)
	{
		char path[64] = { 0 };
		sprintf(path, "D:\\uncompressed\\replay_%02d.dat", i);
		Read(path);

		LZ4_HC(4 << 20);
	}
	cout << endl;*/

	Shutdown();
}

