// 
// /compressmap.cpp 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 
#include "file/device.h"
#include "file/stream.h"
#include "system/param.h"
#include "system/main.h"
#include "system/xtl.h"
#include <dbghelp.h>
#include <string.h>
#include <stdlib.h>

using namespace rage;

#if __64BIT
#define strtoul _strtoui64
#endif

struct Symbol 
{
	size_t Addr;
	char *Name;

	static int compare(const void *a,const void *b) {
		size_t addrA = ((Symbol*)a)->Addr;
		size_t addrB = ((Symbol*)b)->Addr;
		if (addrA > addrB)
			return 1;
		else if (addrA < addrB)
			return -1;
		else
			return 0;	// this shouldn't happen
	}
};

void StripSpaces(char* buf)
{
	int wasAlpha = 0;
	char* dst = buf;
	while (*buf)
	{
		char c = *buf++;
		if (c != ' ' || (wasAlpha && isalnum(*buf)))
		{
			*dst++ = c;						
			wasAlpha = isalnum(c);
		}
		if (c == ',')
		{
			// want a space after every comma
			if (dst == buf)
			{
				memmove(buf+1,buf,strlen(buf)+1);
				++buf;
			}
			*dst++ = ' ';
			wasAlpha = false;
		}
	}
	*dst = 0;
}

void Strip(char* buf, const char* match)
{
	size_t len = strlen(match);
	int wasAlpha = 0;
	while (*buf)
	{
		if (!wasAlpha)
		{
			if (strncmp(buf, match, len) == 0)
				strcpy(buf, buf + len);
			else
				++buf;
		}
		else
		{
			wasAlpha = isalnum(*buf++);
		}
	}
}

void Replace(char* buf, const char* match, const char* replace)
{
	size_t len = strlen(match);
	size_t repllen = strlen(replace);
	int wasAlpha = 0;
	while (*buf)
	{
		if (!wasAlpha && strncmp(buf, match, len) == 0)
		{
			strcpy(buf + repllen, buf + len);
			memcpy(buf, replace, repllen);
			buf += repllen - 1;
		}
		wasAlpha = isalnum(*buf++);
	}
}

bool CompileMap_Win32(const char *mapName,const char *cmpName, bool durango)
{
	fiStream *S = fiStream::Open(mapName,fiDeviceLocal::GetInstance());
	if (!S)
	{
		Errorf("Unable to locate map file '%s'",mapName);
		return false;
	}
	// Displayf("Parsing symbols from [%s]",fileName);

	S = fiStream::PreLoad(S);

	// check if this is a xenon map file by looking for references to xboxkrnl.exe
	bool xenon = false;
	char buffer[4096];
	while (fgetline(buffer,sizeof(buffer),S) && !xenon) {
		if (strstr(buffer, "xboxkrnl.exe")) {
			xenon = true;
			break;
		}
	}

	HINSTANCE dbghelp = 0;
	if (xenon)
	{
		// Load the Xenon dbghelp.dll to unmangle the symbols from the map file
		char dbghelppath[256];
		ExpandEnvironmentStrings("%XEDK%\\bin\\win32\\dbghelp.dll", dbghelppath, sizeof(dbghelppath));
		dbghelp = LoadLibrary(dbghelppath);
		if (!dbghelp)
		{
			Quitf("Error loading XEDK dbghelp.dll. Check your XEDK env var and ensure you have bin\\win32\\dbghelp.dll");
			return false;
		}
	}
	else
	{
		// standard Win32
		dbghelp = LoadLibrary("dbghelp.dll");
		if (!dbghelp)
		{
			Quitf("Error loading Win32 dbghelp.dll. Check your PATH and ensure you have dbghelp.dll (usually in Windows/System32)");
			return false;
		}
	}
	typedef DWORD (WINAPI *tUnDecorateSymbolName)(
		PCSTR   DecoratedName,
		PSTR    UnDecoratedName,
		DWORD   UndecoratedLength,
		DWORD   Flags);
	tUnDecorateSymbolName UnDecorateSymbolName = 
		(tUnDecorateSymbolName)GetProcAddress(dbghelp, "UnDecorateSymbolName");	

	Symbol* symbols = 0;
	ptrdiff_t symbolCount = 0;
	int heapSize = 0;
	char* symbolNameHeap = 0;

	ptrdiff_t preferred_load_addr = 0;

	ptrdiff_t unrelocated_main_addr = 0;

	for (int pass=1; pass<=2; pass++) {
		S->Seek(0);
		ptrdiff_t symCount = 0;
		heapSize = 0;
		bool seen_load_addr = false;
		bool seen_publics = false;
		char decoded[4096];

		decoded[sizeof(decoded) - 1] = '\0';

		while (fgetline(buffer,sizeof(buffer),S)) {
			if (!seen_publics) {

				if(!seen_load_addr)
				{
					const char* searchFor = "Preferred load address is ";
					const char* found = strstr(buffer, searchFor);
					if(found)
					{
						found += strlen(searchFor);
						preferred_load_addr = strtoul(found, NULL, 16);
						seen_load_addr = true;
					}
				}

				if (strstr(buffer,"Publics by Value"))
					seen_publics = true;
				continue;
			}
			// _0001:00000000       __setargv                  00401000 f   setargv.obj
			if (buffer[1] == '0' && buffer[5] == ':' && buffer[15] == ' ') {
				if (!strstr(buffer, " f "))
					continue;
				char *name = buffer+21;
				char *addr = name;
				while (*addr && *addr != 32)
					++addr;
				*addr = 0;
				while (addr[1]==32)
					++addr;

				UnDecorateSymbolName(name, decoded, sizeof(decoded), 
					UNDNAME_NO_ALLOCATION_LANGUAGE|UNDNAME_NO_ACCESS_SPECIFIERS|UNDNAME_NO_MEMBER_TYPE|
					UNDNAME_NO_FUNCTION_RETURNS|UNDNAME_NO_THROW_SIGNATURES);
				StripSpaces(decoded);
				// shorten the symbol:
				Strip(decoded, "class ");
				Strip(decoded, "struct ");
				Strip(decoded, "enum ");
				Strip(decoded,"__ptr64");
				// if it's a rage:: symbol then strip any additional rage::'s
				if (strncmp(decoded, "rage::", 6) == 0)
					Strip(decoded+6, "rage::");
				// use short rage type names
				Replace(decoded, "unsigned int", "u32");
				Replace(decoded, "int", "u32");
				Replace(decoded, "unsigned short", "u16");
				Replace(decoded, "short", "s16");
				Replace(decoded, "unsigned char", "u8");
				size_t len = strlen(decoded);

				if (pass == 2 && symbols)
				{
					char* c = symbolNameHeap + heapSize;
					*c = '\0';

					strcat(c, decoded);

					// Name is offset into symbol file.
					symbols[symCount].Name = (char*)(c - symbolNameHeap + sizeof(symbolCount) + sizeof(preferred_load_addr)
						+ (durango ? sizeof(unrelocated_main_addr) : 0) + symbolCount * sizeof(Symbol));
					symbols[symCount].Addr = strtoul(addr+1,NULL,16);

					if (unrelocated_main_addr == 0 && len == 4 && strcmp(decoded, "main") == 0) {
						unrelocated_main_addr = symbols[symCount].Addr;
					}
				}

				++symCount;
				heapSize += (unsigned)len + 1;
			}
		}
		if (pass == 1) {
			symbolCount = symCount;
			symbols = rage_new Symbol[symCount];
			symbolNameHeap = rage_new char[heapSize];
		}
	}
	S->Close();

	qsort(symbols,symbolCount,sizeof(Symbol),Symbol::compare);

	S = fiStream::Create(cmpName,fiDeviceLocal::GetInstance());
	if (!S)
	{
		Errorf("Unable to create compiled map\n");
		delete [] symbols;
		delete [] symbolNameHeap;
		return false;
	}

	S->Write(&symbolCount, sizeof(symbolCount));
	S->Write(&preferred_load_addr, sizeof(preferred_load_addr));
	if (durango) {
		S->Write(&unrelocated_main_addr, sizeof(unrelocated_main_addr));
	}
	S->Write(symbols, (int)(symbolCount * sizeof (Symbol)));
	S->Write(symbolNameHeap, heapSize);

	S->Close();

	// Displayf("Wrote %d symbols, heap %d, total %d",symbolCount,heapSize,sizeof(symbolCount) + heapSize + symbolCount * sizeof(Symbol));

	delete [] symbols;
	delete [] symbolNameHeap;
	FreeLibrary(dbghelp);

	return true;
}


bool CompileMap_PS3(const char *mapName,const char *cmpName)
{
	fiStream *S = fiStream::Open(mapName,fiDeviceLocal::GetInstance());
	if (!S)
	{
		Errorf("Unable to locate map file '%s'",mapName);
		return false;
	}

	S = fiStream::PreLoad(S);

	// Displayf("Parsing symbols from [%s]",fileName);

	Symbol* symbols = 0;
	int symbolCount = 0;
	int heapSize = 0;
	char* symbolNameHeap = 0;

	char* spusymbols[256];
	struct spuGuid {char guid[16]; unsigned ea;};
	spuGuid spuguids[256];
	unsigned numspuimages = 0;
	int curspuimage = 0;

	for (int pass=1; pass<=2; pass++) {
		S->Seek(0);
		int symCount = 0;
		heapSize = 0;
		char buffer[256];

		while (fgetline(buffer,sizeof(buffer),S)) {

			// 000000000001f5d0 T .cellGcmBeginFrame
			// 01234567890123456789
			const char* sectionStr = strchr(buffer, ' ');
			sectionStr = sectionStr ? sectionStr+1 : buffer;
			if (buffer[0] == '0' && (sectionStr[0] == 'T' || sectionStr[0] == 't' || sectionStr[0] == 'D'))
			{
				u32 addr = (u32)strtoul(buffer,NULL,16);

				const char *symSizeStr = strchr(sectionStr, ' ');
				if (!symSizeStr) continue;
				++symSizeStr;

				// discard symbols with 0 size, as these can cause clashes (eg. _binary_foobar_end).
				const unsigned symSize = (u32)strtoul(symSizeStr,NULL,16);
				if (!symSize) continue;

				const char *name = strchr(symSizeStr, ' ');
				if (!name) continue;
				++name;

				const size_t len = strlen(name);

				if (strncmp(name, "_binary_", 8) != 0)
				{
					if (*sectionStr == 'D') continue;
				}

				else
				{
					struct TypeSuffix
					{
						const char *type;
						const char *suffix;
					};
					static const TypeSuffix typeSuffix[] =
					{
						{"dspfx", "_dspfx_elf_start"    },
						{"frag",  "_frag_frag_bin_start"},
						{"frag",  "_frag_bin_start"     },
						{"job",   "_job_bin_start"      },
						{"task",  "_task_elf_start"     },
					};
					bool isSpuElf = false;
					unsigned type;
					for (type=0; type<sizeof(typeSuffix)/sizeof(*typeSuffix); ++type)
					{
						const size_t suffixLen = strlen(typeSuffix[type].suffix);
						if (len > suffixLen && strcmp(name + len - suffixLen, typeSuffix[type].suffix) == 0)
						{
							isSpuElf = true;
							break;
						}
					}
					if (isSpuElf)
					{
						if (pass == 1)
						{
							const char* basename = strrchr(mapName, '\\'); // get the basename
							if (!basename) basename = mapName;
							const char* config = strchr(basename, '_');
							int configlen = (int)(strchr(mapName, '.') - config);
							if (strncmp("_snc", config+configlen-4, 4) == 0)
								configlen -= 4;

							char filename[512] = "c:\\spu_debug\\";
							const char *const beginElfName = strchr(name+strlen("_binary_"), '_')+1;
							const char *const endElfName = name + len - strlen(typeSuffix[type].suffix);
							strncat(filename, beginElfName, endElfName-beginElfName);
							strncat(filename, config, configlen);
							strcat (filename, ".");
							strcat (filename, typeSuffix[type].type);
							char guidfname[512];
							formatf(guidfname, "%s.guid", filename);
							strcat(filename, ".cmp");

							spusymbols[numspuimages] = 0;
							memset(&spuguids[numspuimages], 0, sizeof(spuGuid));
							fiStream *S = fiStream::Open(guidfname, fiDeviceLocal::GetInstance());
							if (S)
							{
								S->Read(&spuguids[numspuimages].guid, 16);
								S->Close();
								spuguids[numspuimages].ea = addr;
								S = fiStream::Open(filename, fiDeviceLocal::GetInstance());
								if (S)
								{
									int size = S->Size();
									char* spu = rage_new char[size];
									spusymbols[numspuimages] = spu;
									S->Read(spu, size);
									S->Close();
									int spuSymbolCount = *(int*)spu;
									symCount += spuSymbolCount;
									heapSize += size - 12 - spuSymbolCount * sizeof(Symbol);
								}
								else
								{
									Warningf("Unable to open spu compressed symbols file \"%s\".", filename);
								}
							}
							else
							{
								Warningf("Unable to open spu guid file \"%s\".", guidfname);
							}
							++numspuimages;
						}

						// pass == 2
						else
						{
							if (spusymbols[curspuimage])
							{
								int spuSymbolCount = *(int*)spusymbols[curspuimage];
								Symbol* spusym = (Symbol*)(spusymbols[curspuimage] + 8);
								Symbol* spuend = spusym + spuSymbolCount;
								for(; spusym != spuend; ++spusym)
								{
									char* c = symbolNameHeap + heapSize;
									*c = '\0';
									const char* name = spusymbols[curspuimage] + (size_t)spusym->Name;
									strcat(c, name);
									symbols[symCount].Name = (char*)(c - symbolNameHeap + sizeof(symbolCount) + sizeof(int) + symbolCount * sizeof(Symbol));
									symbols[symCount].Addr = addr + spusym->Addr;
									++symCount;
									heapSize += (int)strlen(c) + 1;
								}
								delete [] spusymbols[curspuimage];
							}
							++curspuimage;
						}
					}
				}

				if (pass == 2 && symbols)
				{
					char* c = symbolNameHeap + heapSize;
					*c = '\0';

					strcat(c, name);

					// Name is offset into symbol file.
					symbols[symCount].Name = (char*)(c - symbolNameHeap + sizeof(symbolCount) + sizeof(int) + symbolCount * sizeof(Symbol));
					symbols[symCount].Addr = addr;
				}

				++symCount;
				heapSize += (unsigned)len + 1;
			}
		}
		if (pass == 1) {
			symbolCount = symCount;
			symbols = rage_new Symbol[symCount];
			symbolNameHeap = rage_new char[heapSize];
		}
	}
	S->Close();


	qsort(symbols,symbolCount,sizeof(Symbol),Symbol::compare);

	S = fiStream::Create(cmpName,fiDeviceLocal::GetInstance());
	if (!S)
	{
		Errorf("Unable to create compiled map\n");
		delete [] symbols;
		delete [] symbolNameHeap;
		return false;
	}

	// At least for now, this isn't used for PS3. We still write it to the file,
	// mostly so that cmpmaptool has a chance to work without knowing which platform
	// the map is for.
	int preferred_load_addr = 0;

	S->WriteInt(&symbolCount, 1);
	S->WriteInt(&preferred_load_addr, 1);
	S->WriteInt((int*)symbols, 2 * symbolCount);
	S->Write(symbolNameHeap, heapSize);
	S->Write(spuguids, sizeof(spuGuid) * numspuimages);
	S->WriteInt(&numspuimages, 1);

	S->Close();

	// Displayf("Wrote %d symbols, heap %d, total %d",symbolCount,heapSize,sizeof(symbolCount) + heapSize + symbolCount * sizeof(Symbol));

	delete [] symbols;
	delete [] symbolNameHeap;

	return true;
}


bool CompileMap_PS4(const char *mapName,const char *cmpName)
{
	fiStream *S = fiStream::Open(mapName,fiDeviceLocal::GetInstance());
	if (!S)
	{
		Errorf("Unable to locate map file '%s'",mapName);
		return false;
	}

	S = fiStream::PreLoad(S);

	Symbol* symbols = 0;
	ptrdiff_t symbolCount = 0;
	int heapSize = 0;
	char* symbolNameHeap = 0;
	ptrdiff_t unrelocated_main_addr = 0;

	for (int pass=1; pass<=2; pass++) 
	{
		S->Seek(0);
		int symCount = 0;
		heapSize = 0;
		char buffer[256];

		while (fgetline(buffer,sizeof(buffer),S)) 
		{

			// Address  Size     Align Out     In      File    Symbol
			// =================================================================
			if (buffer[0] == '0')
			{
				const char *symSizeStr = strchr(buffer, ' ');
				symSizeStr = symSizeStr ? symSizeStr+1 : buffer;
				u32 addr = (u32)strtoul(buffer,NULL,16);

				// discard symbols with 0 size, as these can cause clashes (eg. _binary_foobar_end).
				const unsigned symSize = (u32)strtoul(symSizeStr,NULL,16);
				if (!symSize) continue;

				// get alignout str
				const char *alignOutStr = strchr(symSizeStr, ' ');
				while (alignOutStr[0] == ' ')
					alignOutStr++;

				// align out in orbis.map, skip any non-zero
				u32 alignOut = (u32)strtoul(alignOutStr,NULL,16);
				if (alignOut > 0)
					continue;

				// get name str
				const char *name = strchr(alignOutStr, ' ');

				// skip the leading space
				if (!name) continue;
				++name;

				// trim leading spaces
				while (name[0] == ' ')
					name++;

				const size_t len = strlen(name);
				if (pass == 2 && symbols)
				{
					char* c = symbolNameHeap + heapSize;
					*c = '\0';

					strcat(c, name);

					// Name is offset into symbol file.
					symbols[symCount].Name = (char*)(c - symbolNameHeap + sizeof(symbolCount) + sizeof(ptrdiff_t)*2 + symbolCount * sizeof(Symbol));
					symbols[symCount].Addr = addr;

					if (unrelocated_main_addr == 0 && len == 4 && strcmp(name, "main") == 0)
					{
						unrelocated_main_addr = symbols[symCount].Addr;
					}
				}

				++symCount;
				heapSize += (unsigned)len + 1;
			}
		}
		if (pass == 1) {
			symbolCount = symCount;
			symbols = rage_new Symbol[symCount];
			symbolNameHeap = rage_new char[heapSize];
		}
	}
	S->Close();


	qsort(symbols,symbolCount,sizeof(Symbol),Symbol::compare);

	S = fiStream::Create(cmpName,fiDeviceLocal::GetInstance());
	if (!S)
	{
		Errorf("Unable to create compiled map\n");
		delete [] symbols;
		delete [] symbolNameHeap;
		return false;
	}

	// At least for now, this isn't used for PS3. We still write it to the file,
	// mostly so that cmpmaptool has a chance to work without knowing which platform
	// the map is for.
	ptrdiff_t preferred_load_addr = 0;

	S->Write(&symbolCount, sizeof(symbolCount));
	S->Write(&preferred_load_addr, sizeof(preferred_load_addr));
	S->Write(&unrelocated_main_addr, sizeof(unrelocated_main_addr));
	S->Write(symbols, (int)(symbolCount * sizeof (Symbol)));
	S->Write(symbolNameHeap, heapSize);

	S->Close();

	// Displayf("Wrote %d symbols, heap %d, total %d",symbolCount,heapSize,sizeof(symbolCount) + heapSize + symbolCount * sizeof(Symbol));

	delete [] symbols;
	delete [] symbolNameHeap;

	return true;
}


PARAM(durango, "Compile .map as a Durango compressmap.");
PARAM(orbis,   "Compile .map as an Orbis compressmap.");
int Main()
{
	if (sysParam::GetArgCount() != 2 && sysParam::GetArgCount() != 3 && sysParam::GetArgCount() != 4)
		Quitf("Usage: compressmap whatever.map|whatever.sym [compressedmap.cmp] [-durango|-orbis]");

	const char *mapName = sysParam::GetArg(1);
	const char *ext = strrchr(mapName,'.');
	if (!ext || (stricmp(ext,".sym") && stricmp(ext,".map")))
		Quitf("Input name must end in .sym or .map");

	const char *cmpName;
	char cmpNameBuf[512];
	if (sysParam::GetArgCount() == 3)
		cmpName = sysParam::GetArg(2);
	else
	{
		strcpy(cmpNameBuf, sysParam::GetArg(1));
		strcpy(strrchr(cmpNameBuf,'.'),".cmp");
		cmpName = cmpNameBuf;
	}

	bool result;
	if (PARAM_orbis.Get())
	{
		result = CompileMap_PS4(mapName, cmpName);
	}
	else if (stricmp(ext,".sym") == 0)
	{
		result = CompileMap_PS3(mapName, cmpName);
	}
	else
	{
		const bool durango = !!PARAM_durango.Get();
		result = CompileMap_Win32(mapName, cmpName, durango);
	}

	return !result;
}
