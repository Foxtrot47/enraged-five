#include <windows.h>
#include <stdlib.h>
#include <stdio.h>

int main(int argc,char **argv) {
	char *cmdLine = GetCommandLine();
	cmdLine = strchr(cmdLine,' ');
	if (cmdLine) {
		DWORD now = timeGetTime();
		puts(cmdLine);
		system(cmdLine);
		now = timeGetTime() - now;
		printf("**** %u milliseconds\n",now);
	}
	return 0;
}
