#define STRICT
#include <windows.h>
#include <stdio.h>

#pragma comment(lib,"user32.lib")

void main()
{
	OpenClipboard(NULL);
	HANDLE h = GetClipboardData(CF_TEXT);
	if (h) {
		char *s = (char*) GlobalLock(h);
		puts(s);
		GlobalUnlock(h);	

		EmptyClipboard();

		char *copyS = new char[strlen(s) + 1];
		char *match;
		strcpy(copyS, s);
		while (match = strchr(copyS,10))
			*match = 32;
		while (match = strchr(copyS,13))
			*match = 32;

		HANDLE newHandle = GlobalAlloc(GMEM_MOVEABLE,strlen(copyS) + 1);

		char *newS = (char*) GlobalLock(newHandle);
		strcpy(newS, copyS);
		GlobalUnlock(newHandle);

		SetClipboardData(CF_TEXT,newHandle);
	}
	else
		puts("Nothing in clipboard.");

	CloseClipboard();
}
