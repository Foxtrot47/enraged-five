#define _WIN32_WINNT 0x0400
#define STRICT
#include <windows.h>
#include <stdio.h>

void main(int argc,char **argv)
{
	if (argc < 2)
	{
		puts("usage: watcher directory-name");
		return;
	}

	const DWORD bufferSize = 16384;
	FILE_NOTIFY_INFORMATION *buffer = (FILE_NOTIFY_INFORMATION*)
		new char[bufferSize];
	DWORD outSize;
	
	
	HANDLE hDir = CreateFile(argv[1],
		GENERIC_READ,
		FILE_SHARE_DELETE | FILE_SHARE_READ | FILE_SHARE_WRITE,
		NULL,
		OPEN_EXISTING,
		FILE_FLAG_BACKUP_SEMANTICS, // necessary to open a directory
		NULL);

	if (hDir == INVALID_HANDLE_VALUE)
		puts("error opening directory");

	while (ReadDirectoryChangesW(hDir,buffer,bufferSize,TRUE,
		FILE_NOTIFY_CHANGE_FILE_NAME |
		FILE_NOTIFY_CHANGE_DIR_NAME |
		FILE_NOTIFY_CHANGE_ATTRIBUTES |
		FILE_NOTIFY_CHANGE_SIZE |
		FILE_NOTIFY_CHANGE_LAST_WRITE |
		FILE_NOTIFY_CHANGE_CREATION,
		&outSize,
		NULL, NULL)) 
	{
		if (!outSize)
		{
			puts("empty return?");
			continue;
		}

		FILE_NOTIFY_INFORMATION *i = buffer;
		for (;;)
		{
			static const char *actions[] =
			{
				"",
				"added",
				"removed",
				"modified",
				"old name was",
				"new name is"
			};
			if (i->Action < 1 || i->Action > 5)
				puts("unknown action, ignored?");
			else
			{
				char aname[1024];
				// printf("%d bytes in name\n",i->FileNameLength);
				int j;
				int len = i->FileNameLength>>1;
				if (len > sizeof(aname)-1)
					len = sizeof(aname)-1;
				for (j=0; j<len; j++)
					aname[j] = i->FileName[j];
				aname[j] = 0;
				
				printf("%s [%s]\n",actions[i->Action],aname);
			}
			
			if (!i->NextEntryOffset)
				break;
			i = (FILE_NOTIFY_INFORMATION*)((char*)i + i->NextEntryOffset);
		}
	}
	puts("RDW failed.");

	CloseHandle(hDir);	
}
