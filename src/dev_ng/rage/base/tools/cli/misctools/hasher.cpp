#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define STRICT
#include <windows.h>
#pragma comment(lib,"user32.lib")

typedef unsigned u32;

u32 atStringHash( const char *string, const u32 initValue  = 0)
{
	// This is the one-at-a-time hash from this page:
	// http://burtleburtle.net/bob/hash/doobs.html
	// (borrowed from /soft/swat/src/swcore/string2key.cpp)

	bool quotes = (*string == '\"');
	u32 key = initValue;

	if (quotes) 
		string++;

	while (*string && (!quotes || *string != '\"'))
	{
		char character = *string++;
		if (character >= 'A' && character <= 'Z') 
			character += 'a'-'A';
		else if (character == '\\')
			character = '/';

		key += character;
		key += (key << 10);	//lint !e701
		key ^= (key >> 6);	//lint !e702
	}
	key += (key << 3);	//lint !e701
	key ^= (key >> 11);	//lint !e702
	key += (key << 15);	//lint !e701

	// The original swat code did several tests at this point to make
	// sure that the resulting value was representable as a valid
	// floating-point number (not a negative zero, or a NaN or Inf)
	// and also reserved a nonzero value to indicate a bad key.
	// We don't do this here for generality but the caller could
	// obviously add such checks again in higher-level code.
	return key;
}


u32 atLiteralStringHash( const char *string, const u32 initValue  = 0)
{
	// This is the one-at-a-time hash from this page:
	// http://burtleburtle.net/bob/hash/doobs.html
	// (borrowed from /soft/swat/src/swcore/string2key.cpp)
	u32 key = initValue;

	while (*string)
	{
		char character = *string++;
		key += character;
		key += (key << 10);	//lint !e701
		key ^= (key >> 6);	//lint !e702
	}
	key += (key << 3);	//lint !e701
	key ^= (key >> 11);	//lint !e702
	key += (key << 15);	//lint !e701
	return key;
}

int fgetline(FILE *f,char *dest,int destSize) {
	int ch;
	int stored = 0;
	while ((ch = fgetc(f)) != EOF) {
		if (ch >= 32 && destSize > 1) {
			*dest++ = ch;
			--destSize;
			++stored;
		}
		if (ch == 10)
			break;
	}
	*dest = 0;
	return stored;
}



int main(int argc,char **argv)
{
	bool stripext = false;
	bool literalHash = false;

	while (--argc && **++argv=='-') {
		if (!strcmp(*argv,"-stripext")) {
			stripext = true;
		}
		else if (!strcmp(*argv,"-literal")) {
			literalHash = true;
		}
		else {
			fprintf(stderr,"unknown option '%s'\n",*argv);
			return 2;
		}
	}

	if (argc != 1 && argc != 2) {
		fprintf(stderr,"usage: hasher [-stripext] [-literal] filename [hashcode]\n"
			"filename is a list of names, one per line (extensions optionally stripped)\n"
			"hashcode is a hex hash code that we'll try to match\n");
		return 2;
	}

	FILE *f = fopen(argv[0],"r");
	if (!f) {
		if (literalHash)
		{
			printf("\"%s\" -> 0x%x\n",argv[0],atLiteralStringHash(argv[0]));
		}
		else
		{
			printf("\"%s\" -> 0x%x\n",argv[0],atStringHash(argv[0]));
		}
		fprintf(stderr,"cannot open '%s'\n",argv[0]);

		OpenClipboard(NULL);
		EmptyClipboard();
		char buf[256];
		if (literalHash)
		{
			sprintf(buf,"ATLITERALSTRINGHASH(\"%s\",0x%x)",
				argv[0],atLiteralStringHash(argv[0]));
		}
		else
		{
			sprintf(buf,"ATSTRINGHASH(\"%s\",0x%x)",
				argv[0],atStringHash(argv[0]));
		}
		HANDLE newHandle = GlobalAlloc(GMEM_MOVEABLE,strlen(buf)+1);
		char *newS = (char*) GlobalLock(newHandle);
		strcpy(newS, buf);
		GlobalUnlock(newHandle);
		SetClipboardData(CF_TEXT,newHandle);
		CloseClipboard();

		return 2;
	}
	char line[256];
	u32 hashMatch = 0;
	if (argc == 2)
		hashMatch = strtoul(argv[1],NULL,16);
	int result = 1;
	while (fgetline(f,line,sizeof(line)) || !feof(f)) {
		if (stripext && strrchr(line,'.'))
			*strrchr(line,'.') = 0;
		u32 thisHash = 0;
		if (literalHash) {
			thisHash = atLiteralStringHash(line);
		}
		else {
			thisHash = atStringHash(line);
		}
		if (!hashMatch || hashMatch == thisHash)
			printf("\"%s\" -> %x\n",line,thisHash);
		if (hashMatch == thisHash)
			result = 0;
	}
	fclose(f);
	return result;
}
