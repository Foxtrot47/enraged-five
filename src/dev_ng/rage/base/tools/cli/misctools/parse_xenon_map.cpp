#define _CRT_SECURE_NO_DEPRECATE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void main(int argc,char **argv) {
	if (argc != 2) {
		puts("usage: parse_xenon_map inputfile.map");
		return;
	}
	FILE *f = fopen(argv[1],"r");
	if (!f) {
		printf("cannot open '%s'\n",argv[1]);
		return;
	}
	char buf[256];
	unsigned sum = 0;
	while (fgets(buf,sizeof(buf),f)) {
		if (strstr(buf,".rdata")) {
			unsigned rdata;
			sscanf(buf,"%*x:%*x %x",&rdata);
			printf("Read-only data:   %10dk\n",(rdata+1023)>>10);
			sum += rdata;
		}
		else if (strstr(buf,".pdata")) {
			unsigned pdata;
			sscanf(buf,"%*x:%*x %x",&pdata);
			printf("Program(?) data:  %10dk\n",(pdata+1023)>>10);
			sum += pdata;
		}
		else if (strstr(buf,".text")) {
			unsigned text;
			sscanf(buf,"%*x:%*x %x",&text);
			printf("Program code:     %10dk\n",(text+1023)>>10);
			sum += text;
		}
		else if (strstr(buf,".data")) {
			unsigned data;
			sscanf(buf,"%*x:%*x %x",&data);
			printf("Program data:     %10dk\n",(data+1023)>>10);
			sum += data;
		}
		else if (strstr(buf,".bss")) {
			unsigned bss;
			sscanf(buf,"%*x:%*x %x",&bss);
			printf("Zeroed data:      %10dk\n",(bss+1023)>>10);
			sum += bss;
		}
		else if (strstr(buf,"Publics by Value"))
			break;
	}
	fclose(f);
	printf("Total accumulated:%10dk\n",(sum+1023)>>10);
}
