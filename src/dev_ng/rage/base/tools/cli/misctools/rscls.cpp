// 
// /rscls.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "paging/rscbuilder.h"
#include "file/stream.h"
#include "system/endian.h"
#include "string/string.h"

#include <stdio.h>

using namespace rage;

void* operator new(size_t c,size_t /*a*/) { return operator new(c); }
void* operator new[](size_t c,size_t /*a*/) { return operator new[](c); }

static inline void Swapper(size_t &x) {
	size_t s = (x>>24) | (x<<24) | ((x>>8)&0xFF00) | ((x<<8)&0xFF0000);
	x = s;
}

void GenerateChunkInfo(char* chunkInfo, int bufSize, const datResourceChunk* chunks, int chunkCount, size_t* maxSize = 0)
{
	chunkInfo[0] = 0;
	for(int i=0; i<chunkCount;)
	{
		size_t size = chunks[i].Size;
		if (maxSize && size > *maxSize)
			*maxSize = size;
		int count = 0;
		for(; i < chunkCount && chunks[i].Size == size; ++i)
			++count;
		if (i > 1 && count == 1 && size <= 65536) // don't bother printing tail chunks
			break;
		char buf[16];
		if (count > 1)
			formatf(buf, "%ix%ik", count, (size+1023)/1024);
		else
			formatf(buf, "%ik", (size+1023)/1024);
		if (!chunkInfo[0])
			safecpy(chunkInfo, "(", bufSize);
		else
			safecat(chunkInfo, " ", bufSize);
		safecat(chunkInfo, buf, bufSize);
	}
	if (chunkInfo[0])
		safecat(chunkInfo, ")", bufSize);
}

int main(int argc,char **argv) {
	bool csv = false;
	const char *label = NULL;
	if (argc>2 && !strcmp(argv[1],"-csv")) {
		++argv;
		--argc;
		label = argv[1];
		++argv;
		--argc;
		csv = true;
	}

	if (!csv)
		printf("Pltfm  Version  Virtual(csize) Physical(csize) %%Orig  Name\n");
	u64 rawTotal = 0, compTotal = 0;
	u32 count = 0, biggestVirtualChunkSize = 0, biggestPhysicalChunkSize = 0;
	for (int i=1; i<argc; i++) {
		fiStream *S = fiStream::Open(argv[i]);
		if (S) {
			const char *ext = strrchr(argv[i],'.');
			const char *platform = "?????";
			bool swapped = true;
			// Assume PC resource sizes (do this every time in case we get a mix of extensions)
			g_rscVirtualLeafSize = g_rscVirtualLeafSize_WIN32; 
			g_rscPhysicalLeafSize = g_rscPhysicalLeafSize_WIN32;
			if (ext && (ext[1]=='w'||ext[1]=='W')) {
				platform = "win32";
				swapped = false;
			}
			else if (ext && (ext[1]=='y'||ext[1]=='y')) {
				platform = "win64";
				swapped = false;
			}
			else if (ext && (ext[1]=='x'||ext[1]=='X')) {
				platform = "xenon";
			}
			else if (ext && (ext[1]=='c'||ext[1]=='C')) {
				platform = "ps3";
				// ...and change them if it's a PS3 file. 
				g_rscVirtualLeafSize = g_rscVirtualLeafSize_PS3; 
				g_rscPhysicalLeafSize = g_rscPhysicalLeafSize_PS3;
			}
			datResourceFileHeader hdr;
			size_t compressedSize = S->Size() - sizeof(datResourceFileHeader);
			if (S->Read(&hdr,sizeof(hdr)) == sizeof(hdr)) {
				if (swapped) {
					Swapper(hdr.Magic);
					Swapper((size_t&)hdr.Version);
					Swapper((size_t&)hdr.Info.Virtual);
					Swapper((size_t&)hdr.Info.Physical);
				}

				if (hdr.Magic == datResourceFileHeader::c_MAGIC) {
					datResourceMap map;
					pgRscBuilder::GenerateMap(hdr.Info, map);
					char virtualChunkInfo[1024];
					char physicalChunkInfo[1024];
					size_t virtualChunkSize = 0;
					size_t physicalChunkSize = 0;
					GenerateChunkInfo(virtualChunkInfo, sizeof(virtualChunkInfo), &map.Chunks[0], map.VirtualCount, &virtualChunkSize);
					GenerateChunkInfo(physicalChunkInfo, sizeof(physicalChunkInfo), &map.Chunks[map.VirtualCount], map.PhysicalCount, &physicalChunkSize);
					if (csv)
					{
						printf("%s,%u\n",label,hdr.Info.GetVirtualSize() + hdr.Info.GetPhysicalSize());
					}
					else 
					{
						printf("%5s %08x %8u%s %8u%s  %3u   %s\n",
							platform, hdr.Version,
							hdr.Info.GetVirtualSize(),virtualChunkInfo,
							hdr.Info.GetPhysicalSize(),physicalChunkInfo,
							(compressedSize*100)/(hdr.Info.GetVirtualSize()+hdr.Info.GetPhysicalSize()),argv[i]);
					}
					++count;
					rawTotal += hdr.Info.GetVirtualSize() + hdr.Info.GetPhysicalSize();
					compTotal += compressedSize;
					if (biggestVirtualChunkSize < virtualChunkSize)
						biggestVirtualChunkSize = virtualChunkSize;
					if (biggestPhysicalChunkSize < physicalChunkSize)
						biggestPhysicalChunkSize = physicalChunkSize;
				}	
			}
			S->Close();
		}
	}
	if (!csv)
		printf("\n%14u  file%s listed\n",count,count==1?"":"s");
	if (rawTotal && compTotal && !csv) {
		char buf1[64], buf2[64];
		printf("%14uk biggest virtual chunk size\n%14uk biggest physical chunk size\n",
			biggestVirtualChunkSize>>10,biggestPhysicalChunkSize>>10);
		printf("%14s  bytes total compressed\n%14s  total uncompressed\n%14u%% of orig size.\n",
			prettyprinter(buf1,sizeof(buf1),compTotal),prettyprinter(buf2,sizeof(buf2),rawTotal),
			(u32)(compTotal/(rawTotal/100)));
	}
}
