// 
// /tracker.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
//

#include "system/main.h"

#include "diag/tracker_local.h"
#include "bank/tree.h"
#include "file/stream.h"
#include "file/tcpip.h"
#include "atl/array.h"
#include "atl/map.h"

#include <stdlib.h>
#include <string.h>

#include "system/stack.h"
#include "system/param.h"

struct thread 
{
	rage::u32 tid;
	rage::diagTrackerLocal *tracker;
	void Init(rage::u32 a,rage::diagTrackerLocal *b) 
	{
		tid = a;
		tracker = b;
	}
};

//////////////////////////////////////////////////////////////////////////
// Implementation for callstackEntry - class for doing the callstack tracking (if enabled in the log)
bool specialStartTag = false;
const char* specialTag = NULL;

struct callstackEntry
{
	callstackEntry() : m_funcName(NULL), m_address(0), m_next(NULL), m_child(NULL), m_parent(NULL)
	{	
		m_size = 0;
		m_owned_allocs = 0;
	}

	const char *	m_funcName;
	rage::u32		m_address;
	size_t			m_size;
	int				m_owned_allocs;

	callstackEntry*	m_next;
	callstackEntry*	m_child;
	callstackEntry*	m_parent;

	static std::map<long,callstackEntry*>	sm_allocs;
	static bool								sm_enabled;

	static void InitClass(const char *filename)
	{
		sm_enabled = true;
		rage::sysStack::InitClass(filename);
#if __OPTIMIZED
		Warningf("Symbol map parsing doesn't seem to currently work property in non-debug builds.");
#endif
	}

	void Validate()
	{
		callstackEntry* cur = m_child;
		if(cur)
		{
			size_t tally = 0;
			while(cur)
			{
				tally+=cur->m_size;
				cur = cur->m_next;
			}
			Assert(tally==m_size);
		}

		if(m_child)
			m_child->Validate();

		if(m_next)
			m_next->Validate();
	}

	callstackEntry* GetAddress(rage::u32 address)
	{
		rage::u32 addr = address;
		char symName[256] = "unknown";
		rage::u32 offset = rage::sysStack::ParseMapFileSymbol(symName,sizeof(symName), addr, false);
		address = addr - offset; // get the real address of this function

		callstackEntry* cur = this;
		while(cur->m_next)
		{
			if(cur->m_address==address)
				break;
			cur = cur->m_next;
		}

		if (cur->m_address && cur->m_address!=address)
		{
			cur->m_next = new callstackEntry;
			cur = cur->m_next;
			cur->m_parent = m_parent;
		}

		cur->m_address = address;
		cur->m_funcName = symName?symName:"<unknown symbol>";

		return cur;
	}

	callstackEntry* GetChildAddress(rage::u32 address)
	{
		if(!m_child)
		{
			m_child = new callstackEntry;
			m_child->m_parent = this;
		}
		return m_child->GetAddress(address);
	}

	callstackEntry* Sort() // returns new head
	{
		//Validate();
		if(m_child)
			m_child = m_child->Sort();

		if(m_next)
		{
			m_next = m_next->Sort();
			if(m_size < m_next->m_size)
			{
				callstackEntry* tmp = m_next;
				m_next = m_next->m_next;
				tmp->m_next = this;
				return tmp->Sort();
			}
		}
		//Validate();
		return this;
	}

	void Print(int depth = 1)
	{
		if(m_size)
		{
			char sizetext[32];
			const float K = 1024;
			const float MEG = 1024*K;
			if(m_size>K)
				sprintf(sizetext,"%.2f %s",m_size>MEG?float(m_size/MEG):float(m_size/K), m_size>MEG?"MB":"KB");
			else
				sprintf(sizetext,"%u B", m_size);
			Displayf("%*s 0x%x (%s) - %s",depth, "*", m_address, sizetext, m_funcName);
			if(m_child)
				m_child->Print(depth+1);
		}

		if(m_next)
			m_next->Print(depth);
	}

};

std::map<long,callstackEntry*> callstackEntry::sm_allocs;
bool callstackEntry::sm_enabled = false;

void RemoveAllocFromStoredStack(long ptr, size_t siz)
{
	callstackEntry* e = callstackEntry::sm_allocs[ptr];
	if(e==NULL)
		return; // not an address that we previously tallied

	Assert(e->m_owned_allocs);
	e->m_owned_allocs--;
// 	if(!e->m_owned_allocs)
// 	{
// 		Assert(siz==e->m_size);
// 	}

	while(e)
	{
		e->m_size -= siz;
		e = e->m_parent;
	}

	callstackEntry::sm_allocs.erase(ptr);
}

static callstackEntry mystack;

void StoreAllocStack(char* stopper, long ptr, size_t siz)
{
	if(!callstackEntry::sm_enabled)
		return;

	// check for the stack info
	rage::u32 addresses[256];
	int numaddressess = 0;
	for(int i=0;i<256;i++)
	{
		addresses[i] = stopper? (rage::u32) strtoul(stopper+1,&stopper,10) : 0;
		if(addresses[i]==0)
			break;
		numaddressess++;
	}

	rage::u32 base_of_callstack = addresses[numaddressess-1];
	callstackEntry *cur = mystack.GetAddress(base_of_callstack);
	cur->m_size += siz;

	for(int i=numaddressess-2;i>=0;i--)
	{
		// down through the list
		cur = cur->GetChildAddress(addresses[i]);
		cur->m_size += siz;

		if(!i)
		{
			// store the ptr
			callstackEntry::sm_allocs[ptr] = cur;
			cur->m_owned_allocs++;
		}
	}
}
// end callstackEntry implementation
//////////////////////////////////////////////////////////////////////////

enum diagDumpFilterEnum { 
	DUMP_ALL,
	GAME_ONLY,
	RSC_ONLY,
	VIRT_ONLY,
	PHYS_ONLY,
	XTL_ONLY
};

namespace {
	diagDumpFilterEnum DumpFilter;
}

bool ProcessFile( const char* filename, rage::atFixedArray<thread,256> &trackers, bool &didReport )
{
	rage::fiStream *input = rage::fiStream::Open( filename );
	if ( !input ) 
	{		
		return false;
	}

	std::map<long, long> allocations;

	// this filter assumes this: types[5] = { "GameVirt", "RscVirt", "GamePhys", "RscPhys", "XTL" };
	bool filterLookup[5];
	// Game Virt
	filterLookup[0] = (DumpFilter==DUMP_ALL || DumpFilter==GAME_ONLY || DumpFilter==VIRT_ONLY);
	// Rsc Virt
	filterLookup[1] = (DumpFilter==DUMP_ALL || DumpFilter==RSC_ONLY || DumpFilter==VIRT_ONLY);
	// Game Phys
	filterLookup[2] = (DumpFilter==DUMP_ALL || DumpFilter==GAME_ONLY || DumpFilter==PHYS_ONLY);
	// Rsc Phys
	filterLookup[3] = (DumpFilter==DUMP_ALL || DumpFilter==RSC_ONLY || DumpFilter==PHYS_ONLY);
	// XTL
	filterLookup[4] = (DumpFilter==DUMP_ALL || DumpFilter==XTL_ONLY);

	char buffer[2048];
	bool ignoreData = specialStartTag;
while (rage::fgetline(buffer,sizeof(buffer),input)) 
	{
		rage::u32 tid = strtoul(buffer, NULL, 10);
		const char *cmd = strchr(buffer,';');

		if (!cmd)
			continue;
		++cmd;


		if(ignoreData)
		{
			if(cmd[0]!='P') // push, pop, or platform acceptable here
				continue;
		}

		int i;
		for (i=0; i<trackers.GetCount(); i++)
		{
			if (trackers[i].tid == tid)
			{
				break;
			}
		}

		if (i == trackers.GetCount()) 
		{
			char buf[64];
			sprintf(buf,"Tracker for thread %x",tid);
			trackers.Append().Init(tid,new rage::diagTrackerLocal(buf));
		}
		rage::diagTracker* tracker = trackers[i].tracker;		

		if (cmd[0]=='P' && cmd[1]=='u') 
		{
			if(ignoreData)
			{
				if(!strcmp(specialTag,cmd+3))
					ignoreData=false;
			}

			tracker->Push(cmd+3);
		}
		else if (cmd[0]=='P' && cmd[1]=='o') 
		{
			tracker->Pop();
		}
		else if (cmd[0]=='T' && cmd[1]=='a') 
		{
			char *stopper;
			long ptr = strtoul(cmd+3,&stopper,16);
			size_t siz = stopper? (size_t) strtoul(stopper+1,&stopper,10) : 0;
			int type = stopper? (int) strtoul(stopper+1,&stopper,10) : 0;

			FastAssert(type>=0 && type<=4);
			if(filterLookup[type])
				tracker->Tally((void*)ptr,siz,type);

			allocations[ptr] = ptr;

			if(stopper && stopper[0]==';')
			{
				StoreAllocStack(stopper, ptr, siz);
			}
		}
		else if (cmd[0]=='U' && cmd[1]=='n') 
		{
			char *stopper;
			long ptr = strtoul(cmd+3,&stopper,16);
			size_t siz = stopper? atoi(stopper+1) : 0;
			if(!tracker->UnTally((void*)ptr,siz))
				for (int j=0; j<trackers.GetCount(); j++)
					if (trackers[j].tracker->UnTally((void*)ptr,siz))
						break;

			RemoveAllocFromStoredStack(ptr, siz);

			allocations.erase( ptr );
		}
		else if (cmd[0]=='R' && cmd[1]=='e') 
		{
			didReport = true;

			if ( cmd[3] )
			{
				char *stopper = strchr( (char *)(cmd+3), ';' );
				if ( stopper )
				{
					char name[64];
					memset( name, 0, sizeof(name) );
					strncpy( name, cmd+3, stopper - (cmd+3) );

					Displayf( "[[Report for thread %u: %s]]", tid, name );
					tracker->Report( name );
				}
				else
				{
					Displayf( "[[Report for thread %u: %s]]", tid, cmd+3 );
					tracker->Report( cmd+3 );
				}
			}
			else
			{
				Displayf( "[[Report for thread %u]]", tid );
				tracker->Report( 0 );
			}			
		}
		else if ( cmd[0]=='I' && cmd[1]=='n' )
		{
			char *stopper;
			long ptr = strtoul(cmd+3,&stopper,16);

			// if we don't have the allocation from a Tally already, this must be information from an XTL allocation
			if ( allocations[ptr] == NULL )
			{
				size_t siz = stopper? atoi(stopper+1) : 0;

				tracker->Tally( (void*)ptr, siz, 4 );

				allocations[ptr] = ptr;
			}
		}
		else if ( (cmd[0]=='C' && cmd[1]=='E') || (cmd[0]=='T' && cmd[1]=='E') 
			|| (cmd[0]=='U' && cmd[1]=='E') || (cmd[0]=='M' && cmd[1]=='E') ) 
		{
			// do nothing
		}
		else if ( cmd[0]=='P' && cmd[1]=='l' )
		{
			if ( cmd[3] )
			{
				char *stopper = strchr( (char *)(cmd+3), ';' );
				if ( stopper )
				{
					char platform[32];
					memset( platform, 0, sizeof(platform) );
					strncpy( platform, cmd+3, stopper - (cmd+3) );

					tracker->SetTargetPlatform( platform );
				}
			}
		}
		else
		{
			Warningf("Ignored [%s]",buffer);
		}
	}

	input->Close();

	return true;
}

PARAM(f,"[tracker] filter");
PARAM(s,"[tracker] symbol table");
PARAM(p,"[tracker] preload");
PARAM(preload,"[tracker] preload");
PARAM(t,"[tracker] start tag");
PARAM(k,"[tracker] stack view in kilobytes");

int Main() 
{
	int argc = rage::sysParam::GetArgCount();
	char **argv = rage::sysParam::GetArgArray();

	if ( argc < 2 ) 
	{
		fprintf( stderr, "Usage: tracker filename [-s=<symfile>] [-f=<filter>] [-preload] [-t=<start tag>] [-k]\n" );
		//fprintf(stderr,"Use tcpip:9050:listen to wait on port 9050.");
		return 1;
	}

	rage::fiDeviceTcpIp::InitClass(argc, argv);

	bool multiFile = false;
	char part1[128];
	char part2[128];
	
	char* plus = strstr( argv[1], "+" );
	if ( plus )
	{
		multiFile = true;
		rage::safecpy( part1, argv[1], (plus - argv[1]) + 1 );
		rage::safecpy( part2, plus+1, 128 );
	}

	bool callstack = false;

	// parse command line switches
	for(int i=2;i<argc;i++) // i==0:exename ; i==1:inputfile
	{
		if(!strncmp(argv[i],"-s=",3))
		{
			callstack = true;
			callstackEntry::InitClass(&argv[i][3]);
		}
		if(!strncmp(argv[i],"-t=",3))
		{
			specialStartTag = true;
			specialTag = argv[i]+3;
		}
		if(!strncmp(argv[i],"-f=",3))
		{
			char filter = argv[i][3];
			switch(filter)
			{
			case 'g':
				DumpFilter = GAME_ONLY;
				break;
			case 'r':
				DumpFilter = RSC_ONLY;
				break;
			case 'v':
				DumpFilter = VIRT_ONLY;
				break;
			case 'p':
				DumpFilter = PHYS_ONLY;
				break;
			case 'x':
				DumpFilter = XTL_ONLY;
				break;
			default:
				Warningf("Unrecognized filter: %s",filter);
			}
		}	
	}

	rage::atFixedArray<thread,256> trackers;
	bool didReport = false;
	int currentFileIndex = 0;

	do 
	{
		char filename[128];
		if ( multiFile )
		{
			sprintf( filename, "%s%04d%s", part1, currentFileIndex, part2 );
		}
		else
		{
			rage::safecpy( filename, argv[1], 128 );
		}

		++currentFileIndex;

		if ( !ProcessFile( filename, trackers, didReport ) )
		{			
			if ( currentFileIndex == 1 )
			{
				// failed on the first file
				fprintf( stderr, "tracker: cannot open '%s'", filename );
				return 1;
			}
			else
			{
				// must have found the last multiFile
				break;
			}
		}
	} 
	while( multiFile );

	rage::fiDeviceTcpIp::ShutdownClass();

	if(callstack)
	{
		mystack.Sort();
		mystack.Print();
		didReport = true;
	}

	if (!didReport) 
	{
		// rage::Displayf("***** At end of program: ******");
		for (int i=0; i<trackers.GetCount(); i++) 
		{
			// rage::Displayf("[[Thread %u]]",trackers[i].tid);
			trackers[i].tracker->Report(0,0);
			// delete trackers[i].tracker;
		}
	}

	trackers.Reset();

	return 0;
}
