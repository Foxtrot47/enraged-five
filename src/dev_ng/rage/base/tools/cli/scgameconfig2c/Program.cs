﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Diagnostics;
using DebugOutput = scgameconfig2c_2010.ConsoleTraceDebugOutput;


namespace scgameconfig2c_2010
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            ConsoleTraceDebugOutput.Init();

            string myAppName = Process.GetCurrentProcess().ProcessName;
            DebugOutput.WriteLine("Strarting {0}", myAppName);

            ProgramCmdConfigs.Instance.LoadCommandArgs(args);

            //See if we specificed the file
            string configFileName = "";
            if (ProgramCmdConfigs.Instance.Get("file", ref configFileName))
            {
                ScsGameConfigObject.Instance.LoadConfigFromFile(configFileName);
            }
            
            if (ProgramCmdConfigs.Instance.Get("nogui"))
            {
                //Skip the displaying the ui and do whatever the GO button is.
                string leaderboards_enum_file = "leaderboards_enum.sch";
                if (ProgramCmdConfigs.Instance.Get("leaderboards_enum", ref leaderboards_enum_file))
                {
                    Script_LeaderboardEnumWriter lbenumwriter = new Script_LeaderboardEnumWriter();
                    lbenumwriter.OutputFileName = leaderboards_enum_file;
                    lbenumwriter.DoWriteToFile();
                }
            }
            else
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Form1());
            }   
        }
    }

    
}
