﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace scgameconfig2c_2010
{
    class ConsoleTraceDebugOutput
    {       
        class MyTraceListener : TraceListener
        {
            public override void Write(string s)
            {
                Console.Write(s);
            }

            public override void WriteLine(string s)
            {
                Console.WriteLine(s);
            }
        }

        static public void Init()
        {
            Debug.Listeners.Add(new MyTraceListener());
        }

        static public void WriteLine(string format, params object[] args)
        {
            Trace.WriteLine(String.Format(format, args));
        }
    }
}
