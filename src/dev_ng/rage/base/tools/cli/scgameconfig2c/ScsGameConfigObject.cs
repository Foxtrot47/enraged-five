﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Xml.Serialization;

using DebugOutput = scgameconfig2c_2010.ConsoleTraceDebugOutput;

using GameConfig.Model.Core;
using GameConfig.Model;

namespace scgameconfig2c_2010
{
    class ScsGameConfigObject
    {
        public readonly static ScsGameConfigObject Instance = new ScsGameConfigObject();

        /// <summary>
        ///  This is used to add the [XmlAnyAttribute()] and [XmlAnyElement()] attributes to the
        ///  AllAttributes and AllElements properties on ModelBase, respectively. If these attributes
        ///  are added in code (compile time) then XmlUnknownAttribute and XmlUnknownElement exceptions
        ///  are never thrown during load, making it impossible to output debug messages for those
        ///  errors.
        /// </summary>
        private static XmlAttributeOverrides GetSerializationOverrides()
        {
            XmlAttributes anyAttAttributes = new XmlAttributes();
            XmlAnyAttributeAttribute anyAtt = new XmlAnyAttributeAttribute();
            anyAttAttributes.XmlAnyAttribute = anyAtt;
            XmlAttributes anyElemAttributes = new XmlAttributes();
            XmlAnyElementAttribute anyElem = new XmlAnyElementAttribute();
            anyElemAttributes.XmlAnyElements.Add(anyElem);
            XmlAttributeOverrides overrides = new XmlAttributeOverrides();
            overrides.Add(typeof(ModelBase), "AllAttributes", anyAttAttributes);
            overrides.Add(typeof(ModelBase), "AllElements", anyElemAttributes);
            return overrides;
        }

        public void LoadConfigFromFile(string fileName)
        {
            using (StreamReader reader = new StreamReader(fileName))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(ScsGameConfiguration), GetSerializationOverrides());
                m_config = serializer.Deserialize(reader) as ScsGameConfiguration;

                DebugOutput.WriteLine("Successfully loading config from " + fileName);
                DebugOutput.WriteLine("Inputs: " + m_config.LeaderboardsConfiguration.Inputs.Count);
                DebugOutput.WriteLine("Leaderboards: " + m_config.LeaderboardsConfiguration.Leaderboards.Count);
                DebugOutput.WriteLine("Categories: " + m_config.LeaderboardsConfiguration.Categories.Count);
            }
        }

        public ScsGameConfiguration GetConfig()
        {
            return m_config;
        }

        public Input GetInputById(int inputId)
        {
            foreach (Input inpt in m_config.LeaderboardsConfiguration.Inputs)
            {
                int id = Convert.ToInt32(inpt.Id);
                if (id == inputId)
                {
                    return inpt;
                }
            }

            return null;
        }

        public Column GetColumnByIdOnLeaderboard(Leaderboard lb, int id)
        {
            foreach (Column col in lb.Columns)
            {
                int colId = Convert.ToInt32(col.Id);
                if (colId == id)
                {
                    return col;
                }
            }

            return null;
        }

        public Input[] GetInputsOnLeaderboard(Leaderboard lb)
        {
            List<Input> inputList = new List<Input>();
            foreach(Column col in lb.Columns)
            {
                if (col.Aggregation.GameInput != null)
                {
                    int inputID = Convert.ToInt32(col.Aggregation.GameInput.InputId);
                    if (inputID > 0)
                    {
                        //Find the input with this ID.
                        Input lbInput = GetInputById(inputID);
                        if (lbInput != null )
                        {
                            inputList.Add(lbInput);
                        }
                    }
                }
            }

            return inputList.ToArray();
        }

        private ScsGameConfiguration m_config;
    }
}
