﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using DebugOutput = scgameconfig2c_2010.ConsoleTraceDebugOutput;
using GameConfig.Model;

namespace scgameconfig2c_2010
{
    class Script_LeaderboardEnumWriter : ScriptFileWriter
    {
        //
        //Specific instance stuff
        //
        public string GetLBEnumName(Leaderboard lb)
        {
            return String.Format("LEADERBOARD_{0}", lb.Name);
        }
        public void WriteLeaderboardEnum()
        {
            //Get the current config
            ScsGameConfiguration config = ScsGameConfigObject.Instance.GetConfig();

            //Output the enum
            //             ENUM LEADERBOARDS_ENUM
            // 	                LEADERBOARD_<NAME> = <ID>,
            // 	                ...
            // 	                LEADERBOARDS_COUNT = <COUNT>,
            //                  LEADERBOARDS_INVALID = 0
            //             ENDENUM

            WriteLineTabbed("ENUM LEADERBOARDS_ENUM");
            {
                PushTabLevel();
                foreach (Leaderboard lb in config.LeaderboardsConfiguration.Leaderboards)
                {
                    WriteLineTabbed("{0} = {1},", GetLBEnumName(lb), lb.Id);
                }
                WriteLineTabbed("");
                WriteLineTabbed("LEADEBOARDS_COUNT = {0},", config.LeaderboardsConfiguration.Leaderboards.Count);
                WriteLineTabbed("LEADERBOARDS_INVALID = 0");
                PopTabLevel();
            }
            WriteLineTabbed("ENDENUM //LEADERBOARDS_ENUM");
        }

        public string GetInputEnumString(Input inpt)
        {
            return String.Format("LB_INPUT_{0}", inpt.Name);
        }

        public void WriteInputsEnum()
        {
        // ENUM LEADERBOARDS_INPUTS
        // 	    LB_INPUT_<NAME> = <ID> // <TYPE>
        // ENDENUM
            //Get the current config
            ScsGameConfiguration config = ScsGameConfigObject.Instance.GetConfig();

            WriteLineTabbed("ENUM LEADERBOARD_INPUTS");
            {
                PushTabLevel();
                foreach (Input inpt in config.LeaderboardsConfiguration.Inputs)
                {
                    WriteLineTabbed("{0} = {1}, // {2}", GetInputEnumString(inpt), inpt.Id, inpt.DataType);
                }
                WriteLineTabbed("");
                WriteLineTabbed("LB_INPUT_COUNT = {0},", config.LeaderboardsConfiguration.Inputs.Count);
                WriteLineTabbed("LB_INPUT_INVALID = 0");

                PopTabLevel();
            }
            WriteLineTabbed("ENDENUM //LEADERBOARD_INPUTS");

        }

        public string GetCategoryEnumString(Category ctgy)
        {
            return String.Format("LB_CATEGORY_{0}", ctgy.Name.ToUpper());
        }

        public void WriteCategoriesEnum()
        {
            //Get the current config
            ScsGameConfiguration config = ScsGameConfigObject.Instance.GetConfig();

            /*
             ENUM LEADERBOARDS_CATEGORIES
	            LB_CATEGORY_<CATEGORY> = HASH("<CATEGORY>"),
	            LEADERBOARDS_CATEGORIES_COUNT = <COUNT>,
	            LEADERBOARDS_CATEGORIES_INVALID = 0
            ENDENUM
             */

            WriteLineTabbed("ENUM LEADERBOARDS_CATEGORIES");
            {
                PushTabLevel();
                foreach (Category ctgy in config.LeaderboardsConfiguration.Categories)
                {
                    WriteLineTabbed("{0} = HASH(\"{1}\"),", GetCategoryEnumString(ctgy), ctgy.Name);
                }
                WriteLineTabbed("");
                WriteLineTabbed("LB_CATEGORY_COUNT = {0},", config.LeaderboardsConfiguration.Categories.Count);
                WriteLineTabbed("LB_CATEGORY_INVALID = 0");
                PopTabLevel();
            }
            WriteLineTabbed("ENDENUM //LEADERBOARDS_CATEGORIES");


        }

        public void WriteCommentedOutSummary()
        {
            //Get the current config
            ScsGameConfiguration config = ScsGameConfigObject.Instance.GetConfig();

            //Name: <LB Name>
            //ID: <ID> - Enum value
            //Num inputs: <number>
            //Input list:
            //  <inputEnum>
            //Num columns: <number>
            //Column Info:
            //  <ColName> ( <AGG_TYPE> ) : 

            WriteLineTabbed("/*");
            
            foreach (Leaderboard lb in config.LeaderboardsConfiguration.Leaderboards)
            {
                WriteLineTabbed("-----------------------------------------------------");
                WriteLineTabbed("Name: {0}", lb.Name);
                WriteLineTabbed("ID: {0} - {1}", lb.Id, GetLBEnumName(lb));

                //Inputs
                Input[] inputList = ScsGameConfigObject.Instance.GetInputsOnLeaderboard(lb);
                WriteLineTabbed("Inputs: {0}", inputList.Length);
                {
                    PushTabLevel();
                    foreach (Input inpt in inputList)
                    {
                        WriteLineTabbed("{0} ({1})", GetInputEnumString(inpt), inpt.DataType);
                    }
                    PopTabLevel();
                }

                //Columns
                WriteLineTabbed("Columns: {0}", lb.Columns.Count);
                {
                    PushTabLevel();
                    foreach (Column col in lb.Columns)
                    {
                        string inputString = "";
                        if (col.Aggregation.GameInput != null)
                        {
                            Input inpt = ScsGameConfigObject.Instance.GetInputById(Convert.ToInt32(col.Aggregation.GameInput.InputId));
                            if (inpt != null)
                            {
                                inputString = String.Format("InputId: {0}", GetInputEnumString(inpt));
                            }
                        }
                        else if (col.Aggregation.ColumnInput != null)
                        {
                            StringBuilder strBldr = new StringBuilder();
                            strBldr.Append("Column Inputs: ");
                            foreach (ColumnInput colInpt in col.Aggregation.ColumnInput)
                            {
                                //Get the name of the columns it refers to
                                int colInputId = Convert.ToInt32(colInpt.ColumnId);
                                Column theInputColumn = ScsGameConfigObject.Instance.GetColumnByIdOnLeaderboard(lb, colInputId);
                                if (theInputColumn != null)
                                {
                                    strBldr.AppendFormat("{0} ", theInputColumn.Name);
                                }
                            }
                            inputString = strBldr.ToString();
                        }

                        WriteLineTabbed("{0} ( AGG_{1} ) - {2}", col.Name, col.Aggregation.Type, inputString);
                    }
                    PopTabLevel();
                }

                //Category Groups
                WriteLineTabbed("Instances: {0}", lb.CategoryPermutations.Count);
                {
                    PushTabLevel();
                    {
                        foreach (CategoryPermutation perm in lb.CategoryPermutations)
                        {
                            WriteLineTabbed(perm.Value);
                        }
                    }
                    PopTabLevel();
                }

                WriteLineTabbed("");
            }

            WriteLineTabbed("*/");
        }

        public override void DoWriteToFile()
        {
            //Create the file writer
            CreateStreamForWriting();

            //Hack bit to get us by for the transistion
            WriteLineTabbed("//REMOVE ME: Support for older functionality");
            WriteLineTabbed("ENUM LEADERBOARDS_TYPES_ENUM");
            {
                PushTabLevel();
                WriteLineTabbed("LEADERBOARD_TYPE_GROUP");
                PopTabLevel();
            }
            WriteLineTabbed("ENDENUM");
            WriteLineTabbed("");

            WriteLeaderboardEnum();
            WriteLineTabbed("");

            WriteInputsEnum();
            WriteLineTabbed("");

            WriteCategoriesEnum();
            WriteLineTabbed("\n");

            WriteCommentedOutSummary();
            WriteLineTabbed("");

            CloseStream();
        }
    }
}
