﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;
using System.Text.RegularExpressions;

namespace scgameconfig2c_2010
{
    class ProgramCmdConfigs
    {
        public readonly static ProgramCmdConfigs Instance = new ProgramCmdConfigs();

        private StringDictionary m_parameters;

        public bool Get(string argName)
        {
            return m_parameters.ContainsKey(argName);
        }

        public bool Get(string argName, ref string value)
        {
            if (m_parameters.ContainsKey(argName))
            {
                //Only set the value to something if it's not just 'true'
                if (!m_parameters[argName].Equals("true"))
                { 
                    value = m_parameters[argName];
                }
                
                return true;
            }

            return false;
        }

        public void LoadCommandArgs(string[] args)
        {
            m_parameters = new StringDictionary();

            Regex Spliter = new Regex(@"^-{1,2}|=",
                RegexOptions.IgnoreCase | RegexOptions.Compiled);


            Regex Remover = new Regex(@"^['""]?(.*?)['""]?$",
                RegexOptions.IgnoreCase | RegexOptions.Compiled);

            string Parameter = null;
            string[] Parts;

            // Valid parameters forms:
            // {-,--}param{ ,=}((",')value(",'))
            // Examples: 
            // -param1 value1 -param2 -param3="Test-:-work"  -param4=This/Path/works
            foreach (string Txt in args)
            {
                // Look for new parameters (-,--) and a
                // possible enclosed value (=)
                Parts = Spliter.Split(Txt, 3);

                switch (Parts.Length)
                {
                    // Found a value (for the last parameter 
                    // found (space separator))
                    case 1:
                        if (Parameter != null)
                        {
                            if (!m_parameters.ContainsKey(Parameter))
                            {
                                Parts[0] =
                                    Remover.Replace(Parts[0], "$1");

                                m_parameters.Add(Parameter, Parts[0]);
                            }
                            Parameter = null;
                        }
                        // else Error: no parameter waiting for a value (skipped)
                        break;

                    // Found just a parameter
                    case 2:
                        // The last parameter is still waiting. 
                        // With no value, set it to true.
                        if (Parameter != null)
                        {
                            if (!m_parameters.ContainsKey(Parameter))
                                m_parameters.Add(Parameter, "true");
                        }
                        Parameter = Parts[1];
                        break;

                    // Parameter with enclosed value
                    case 3:
                        // The last parameter is still waiting. 
                        // With no value, set it to true.
                        if (Parameter != null)
                        {
                            if (!m_parameters.ContainsKey(Parameter))
                                m_parameters.Add(Parameter, "true");
                        }

                        Parameter = Parts[1];

                        // Remove possible enclosing characters (",')
                        if (!m_parameters.ContainsKey(Parameter))
                        {
                            Parts[2] = Remover.Replace(Parts[2], "$1");
                            m_parameters.Add(Parameter, Parts[2]);
                        }

                        Parameter = null;
                        break;
                }
            }

            // In case a parameter is still waiting
            if (Parameter != null)
            {
                if (!m_parameters.ContainsKey(Parameter))
                    m_parameters.Add(Parameter, "true");
            }
        }
    }
}
