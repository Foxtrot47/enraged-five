rem *** Used to create a Python exe 

rem ***** check out the output files from perforce
p4 edit %RS_TOOLSROOT%\bin\coding\python\*.*

rem ***** get rid of all the old files in the build folder
rd /S /Q build dist

rem ***** create the exe
%RS_TOOLSPYTHON% setup.py py2exe

if ERRORLEVEL 1 goto :EOF

rem ***** Get unchanged stuff out of the changelist
p4 revert -a %RS_TOOLSROOT%\bin\coding\python\*.*
