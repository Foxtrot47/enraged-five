from distutils.core import setup
import py2exe
import os

setup(
	console=['stripobjects.py'],
	options={
		"py2exe": {
            "ascii":True,
			"excludes":["email", "email.Utils"],
            "dist_dir":os.getenv("RS_TOOLSROOT") +"\\bin\\coding\\python",
			}
		},
    zipfile="stripobjects.zip"
)
