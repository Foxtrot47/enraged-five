import os, sys, subprocess
import optparse

opt = optparse.OptionParser(usage="Usage: %prog [options] inputfile.vcproj outputDir")
opt.add_option("-v", "--verbose", dest="verbose", action="store_true", default=False, help="Print lots of info about what's going on")

(options, outargs) = opt.parse_args(sys.argv[1:])

if len(outargs) < 2:
    opt.print_help()
    sys.exit(1)

projfile = outargs[0]
projdir = os.path.split(projfile)[0]
intdir = outargs[1]
intdir = os.path.join(projdir, intdir)
keepsyms = os.path.join(projdir, 'debuginfo.txt')
try: keepsyms = [x.strip() for x in file(keepsyms)]
except: keepsyms = []

if (options.verbose):
    print 'stripping object files for %s in %s' % (projfile, intdir)
#print keepsyms

numObjFiles = 0
strippedToUnstripped = 0
unstrippedToStripped = 0
alreadyStripped = 0
alreadyUnstripped = 0
outOfDateStripped = 0
outOfDateUnstripped = 0
neededToStrip = 0

for obj in os.listdir(intdir):
    if '.debug.' in obj or '.nodebug.' in obj:
        pass
    elif obj.endswith('.obj'):
        numObjFiles += 1
        output = intdir + '/' + obj
        st = os.stat(output)

        # theory of operation: if a file is supposed to have debug info,
        # there will be an .obj and .nodebug.obj file.  If the file is
        # not supposed to have debug info, there will be an .obj and
        # .debug.obj file.  if the .obj file is newer than either of
        # these target files, they are deleted and recreated.
        
        withdebug = intdir + '/' + obj.replace(".obj", ".debug.obj")
        withdebugexists = False
        try:       
            if st.st_mtime > os.path.getmtime(withdebug):
                if (options.verbose):
                    print "Unstripped backup file %s is out of date compared to %s, deleting" % (withdebug, obj)
                os.remove(withdebug)
                outOfDateUnstripped += 1
            else:
                withdebugexists = True
        except os.error:
            pass
            
        stripped = intdir + '/' + obj.replace(".obj", ".nodebug.obj")
        strippedexists = False
        try:
            if st.st_mtime > os.path.getmtime(stripped):
                if (options.verbose):
                    print "Stripped backup file %s is out of date compared to %s, deleting" % (stripped, obj)
                os.remove(stripped)
                outOfDateStripped += 1
            else:
                strippedexists = True
        except os.error:
            pass

        if os.path.splitext(obj.lower())[0] in keepsyms:
            if options.verbose:
                print "Preserving symbols for %s" % (obj)
            # don't strip this file
            if withdebugexists:
                if options.verbose:
                    print "  Found an unstripped backup of %s - current file must be stripped" % (withdebug)
                    print "  Renaming %s as stripped backup %s, and renaming unstripped %s as %s" % (output, stripped, withdebug, output)
                # current .obj file is stripped, replace it with the unstripped version
                os.rename(output, stripped)
                os.rename(withdebug, output)
                strippedToUnstripped += 1
            else:
                if options.verbose:
                    print "  No unstripped backup exists. File must already be unstripped"
                alreadyUnstripped += 1
        else:
            if options.verbose:
                print "Removing symbols for %s" % (obj)
            # do strip this file
            if strippedexists:
                # current .obj file is UNstripped, replace it with the stripped version
                if options.verbose:
                    print "  Found a stripped backup of %s, current file must be unstripped" % (stripped)
                    print "  Renaming %s as unstripped backup %s, and renaming stripped %s as %s" % (output, withdebug, stripped, output)
                os.rename(output, withdebug)
                os.rename(stripped, output)
                unstrippedToStripped += 1
            elif not withdebugexists:
                if options.verbose:
                    print "  No stripped or unstripped backup exists. Renaming %s as %s and stripping the file" % (output, withdebug)
                os.rename(output, withdebug)
                cmd = 'ppu-lv2-strip -g "%s" -o "%s"' % (withdebug, output)
                if options.verbose:
                    print "  Executing: %s" % (cmd)
                #cmd = 'vsibin -sd "%s" -o "%s"' % (withdebug, output)
                #print cmd
                subprocess.call(cmd)
                os.utime(output, (st.st_atime, st.st_mtime))
                neededToStrip += 1
            else:
                if options.verbose:
                    print "  Unstripped backup exists and stripped backup does not - file must already be stripped"
                alreadyStripped += 1
                
if options.verbose:
    print "----------------------------------------------------"
    print "Object files: %d" % (numObjFiles)
    print "Out of date backups deleted: %d stripped, %d unstripped" % (outOfDateStripped, outOfDateUnstripped)
    print "Unstripped files preserved: %d, converted from backup: %d" % (alreadyUnstripped, strippedToUnstripped)
    print "Stripped files preserved: %d, converted from backup: %d, generated: %d" % (alreadyStripped, unstrippedToStripped, neededToStrip)
    