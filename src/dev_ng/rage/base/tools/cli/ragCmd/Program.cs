﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Text;
using System.Collections.ObjectModel;
using System.Net;
using System.Net.Sockets;

using ragCore;
using ragWidgets;

using RSG.Base.Logging;
using RSG.Base.Net;

using ragClient;
using RSG.Base.Logging.Universal;
using System.IO;

//System to communicate with rag widgets from the command line over the rag proxy
//For any largish system you should be using the bank protocol directly (InterpretString)
//but for one or two commands it might be quicker to use PassToConsoleWidget as there is
//much less start up cost associated.
//Greg 26/8/2011

/*
WidgetText enterbug = (WidgetText)client.BM.FindFirstWidgetFromPath("\\Debug\\Bugstar\\Get Bug by Number\\Enter Bug #");
enterbug.String = "15779";

WidgetButton fetchbug = (WidgetButton)client.BM.FindFirstWidgetFromPath("\\Debug\\Bugstar\\Get Bug by Number\\Fetch");
fetchbug.Activate();

WidgetButton movetobug = (WidgetButton)client.BM.FindFirstWidgetFromPath("\\Debug\\Bugstar\\Bugs\\Warp to Bug");
movetobug.Activate();
*/

namespace ragCmd
{
    class Program
    {
        static void Main(string[] args)
        {
            LogFactory.Initialize(Path.Combine(LogFactory.GetLogDirectory(), "RagCmd"), false);
            LogFactory.CreateApplicationConsoleLogTarget();
            IUniversalLog m_log = LogFactory.ApplicationLog;

            // This should be configurable via the command line.
            bool ConsoleOnly = true;

            m_log.Message("Running in {0} mode.", (ConsoleOnly ? "console only" : "full connection"));
            Client.ConsoleOnly = ConsoleOnly;
            Client.SetupConnection();

            // Check whether we were provided a file containing the list of commands to run.
            if (args.Length > 0)
            {
                m_log.Message("Processing commands from '{0}'.", args[args.Length - 1]);
                string[] lines = System.IO.File.ReadAllLines(args[args.Length - 1]);

                foreach (string line in lines)
                {
                    m_log.Message("Executing {0}", line);
                    if (ConsoleOnly)
                        PassToConsoleAsWidget(line);
                    else
                        InterpretString(line);
                }
            }
            else
            {
                while (true)
                {
                    string line = Console.ReadLine();

                    if (line == "x")
                    {
                        break;
                    }
                    else
                    {
                        if (ConsoleOnly)
                            PassToConsoleAsWidget(line);
                        else
                            InterpretString(line);
                    }
                }
            }

            Client.CloseConnections();
            LogFactory.FlushApplicationLog();
            LogFactory.ApplicationShutdown();
        }

        static void InterpretString( string line )
        {
            string[] tokens = line.Split(new char[] {','});

            if (tokens.Length < 1)
                return;

            Widget rawwidget = (Widget)Client.BM.FindFirstWidgetFromPath(tokens[0]);

            if (rawwidget == null)
            {
                Console.WriteLine("can't find widget #{0}", tokens[0]);
            }
            else
            {
                string[] target = new string[tokens.Length - 1];
                Array.Copy(tokens, 1, target, 0, tokens.Length - 1);

                string RetVal = rawwidget.ProcessCommand(target);
                Console.WriteLine(RetVal);
            }
        }

        static void PassToConsoleAsWidget(string line)
        {
            string RetVal = Client.Context.Console.SendCommand(line);

            Console.WriteLine(RetVal);
        }
    }
}
