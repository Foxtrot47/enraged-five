
$global:GameSocket = $null
$global:listen = $null

function Start-ConsoleComm()
{
	if ( $global:listen -eq $null )
	{
     		$global:listen = new-object System.Net.Sockets.TcpListener("0.0.0.0", 5050) 
	}
	$global:listen.Start()

	while( $global:listen.Pending() )
	{}

	write-host "Listener is now ready " 
	$global:GameSocket = $global:listen.AcceptSocket();

	if($global:GameSocket -eq $null) 
	{ 

		$listen.Stop()
		return; 
	} 

	write-host "Connected.  Run End-ConsoleCommand when finished to cleanup.`n" 


	$global:listen.Stop()
}


function End-ConsoleComm()
{
	## Close the streams 
	$global:GameSocket.Close() 
	$global:GameSocket= $null
}

$GameBuffer = new-object System.Byte[] 2048

function Send-ConsoleComm()
{
	if ( $global:GameSocket -eq $null )
	{
		Start-ConsoleComm
		if ( $global:GameSocket -eq $null )
		{
			write-host "Error Couldn't connect"
			return $false	
		}
	}

	$args | %{ $command += $_.ToString() + "," }
 	if ( $args.Length -eq 0 )
 	{
		return
	}
	 # use lower case for everything 
	$command = $command.ToLower()  
	
   # Write their command to the remote host      
   $sent = $global:GameSocket.Send( [text.Encoding]::Ascii.GetBytes( $command ) )
   if ( $sent -ne $command.Length )
   {
		write-host " Couldn't send all the command accross"
   }
   
   ## Allow data to buffer for a bit 
   start-sleep -m 50 
   
   # Every command must return a value so wait until it does
   while( $global:GameSocket.Available -eq 0 )
   {
	  start-sleep -m 50 
   }
   
   # TODO : find out if the game has stopped
   $result = ""
   
   while( $global:GameSocket.Available -ne 0 )
   {
		$GameBufferSize = $GameSocket.Receive($GameBuffer )
		if ( $GameBufferSize -eq 0 )
		{
			write-error "Lost Connection with Host Game "
			End-ConsoleComm
			return
		}
    
		$res =  [Text.encoding]::Ascii.getString($GameBuffer) 
		$result += $res.SubString( 0, $GameBufferSize )
    }
    
    if ( $result -like "*Error*" )
    {
		write-error $result
	}
	return $result
}
function Convert-Args()
{
	 $args | %{ $text += $_.ToString() + "," }
	 return $text
}
set-alias ec End-ConsoleComm
set-alias scc Send-ConsoleComm  
set-alias ~ Send-ConsoleComm
