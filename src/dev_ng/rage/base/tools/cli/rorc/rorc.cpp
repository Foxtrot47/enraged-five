// 
// rorc/rorc.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/*
	rorc stands for "Rage Offline Resource Compiler".  An offline resource compiler
	generates resources for a game without actually running that game.  In particular,
	it is capable of generating resources for a different target platform.  At this
	level the code is very similar, because all of our target platforms (now) support
	the same basic type sizes and structure padding.

	The major difference is that when generating for Xenon or PS3 from a PC, we have
	to byte-swap the data before saving it out.  We do this by creating DeclareStruct
	member functions for every class, and let templates provide some syntactic sugar
	to make everything as painless as possible (and also aggressively check for any
	unexpected gaps in structures that could indicate newly added data that somebody
	forgot to add to their DeclareStruct function).  Note that DeclareStruct is *only*
	invoked by the resource compiler, and doesn't even need to exist on the target
	platform at all.

	Bitfields are usable cross-platform, as long as you use the macros in data/bitfield.h.
*/

#define SIMPLE_HEAP_SIZE	(248*1024)

#include "system/main.h"
#include "system/param.h"
#include "system/platform.h"


#if !__RESOURCECOMPILER
#error "Only Rsc platform will will work properly."
#endif

#include "offlinersc/offlinersc.h"
#include "cranimation/animation.h"
#include "cranimation/animdictionary.h"
#include "grblendshapes/manager.h"
#include "grcore/texture.h"
#include "grcore/image.h"
#include "grcore/texturedefault.h"
#include "grcore/texturexenonproxy.h"
#include "grcore/texturegcm.h"
#include "rmcore/drawable.h"
#include "grmodel/modelfactory.h"
#include "parser/manager.h"
#include "phbound/bound.h"
#include "phcore/materialmgrimpl.h"

#include "file/asset.h"
#include "file/token.h"

#include "paging/dictionary.h"
#include "paging/rscbuilder.h"

using namespace rage;

PARAM(stripTexturePath,"[rorc] strips the path from the texture name when adding it to the texture dictionary" );
PARAM(dumpVersionNumbers,"[rorc] outputs the current version numbers of supported assets in text file" );
PARAM(blendtargets,"[rorc] generate blendtarget resources for any drawables (instead of normal drawables)" );
PARAM(compactanim,"[rorc] generate compact animation resource, rather than just packed");
PARAM(basenamekeys, "[rorc] use base names (without relative paths) for animation dictionary keys");
PARAM(swfsharetex,"[rorc] Look for a .sharetex file next to each swf file for texture sharing");
PARAM(swffontref,"[rorc] Use another .swf file as a font reference file." );

XPARAM(path);


int Main() {
	INIT_PARSER;

	const char* versFile;
	if ( PARAM_dumpVersionNumbers.Get(versFile) )		// create a file storing the version numbers of all the resources
	{
		VersionFile vers( versFile);
		vers.Output( "drawable", rmcDrawable::RORC_VERSION );
		vers.Output( "anim", crAnimation::RORC_VERSION );
		vers.Output( "textures", grcTextureDefault::RORC_VERSION );
		vers.Output( "bound", phBound::RORC_VERSION );
		SHUTDOWN_PARSER;
		return 0;
	}

	if (sysParam::GetArgCount() < 3)
		Quitf("Usage:\nrorc textureList.txt outputFile.xeck [-path=xxx]\n- or -\nrorc [entity.type|file.anim] outputFile.xeck [-path=xxx]");

	const char *inputExt = strrchr(sysParam::GetArg(1),'.');
	if (!inputExt)
		Quitf("Input file must have extension so we can determine file type.");
	bool needGraphics = !stricmp(inputExt,".type") || !stricmp(inputExt,".swf") || !stricmp(inputExt,".txt") || !stricmp(inputExt,".textures") || !stricmp(inputExt,".dds");
	bool needShaders = !stricmp(inputExt,".type");
	OffLineResourceCreator	resourcer(needGraphics, needShaders);

	if (resourcer.HasExtension( "txt") || resourcer.HasExtension( "textures")) {
	// Is the input filetype a list of textures?
		fiStream *S = ASSET.Open( resourcer.GetInput(),"",true,true);
		if (S) {
			S->Close();
			bool stripPath = PARAM_stripTexturePath.Get();

			while ( resourcer.Next() )
			{
				pgDictionary<grcTexture> *textureDictionary = grmShaderGroup::CreateTextureDictionary(resourcer.GetInput(),stripPath);
				resourcer.Resource( textureDictionary, "#td" ,grcTextureDefault::RORC_VERSION);
			}
		}
	}
	// Is the input file an rmcDrawable?
	else if ( resourcer.HasExtension( "type")) { 
		
		if (PARAM_blendtargets.Get())
		{
			while ( resourcer.Next() )
			{
				sysMemStartTemp();
				rmcDrawable *drawable = rage_new rmcDrawable;
				drawable->Load( resourcer.GetInput() );
				sysMemEndTemp();

				grbTargetManager *pTargetManager = rage_new grbTargetManager;

				// Load the target manager
				ValidateResult(pTargetManager->Load(resourcer.GetInput(),drawable));

				sysMemStartTemp();
				pgRscBuilder::Delete(drawable);
				sysMemEndTemp();

				resourcer.Resource(pTargetManager, "#btm");
			}
		}
		else
		{
			while ( resourcer.Next() )
			{
				rmcDrawable *drawable = rage_new rmcDrawable;
	
				// Load the drawable
				ValidateResult(drawable->Load( resourcer.GetInput()));
	
				resourcer.Resource( drawable, "#dr" );
			}
		}
		
	}
	// ...or is it an animation?
	else if (resourcer.HasExtension( "anim")) 
	{
		while ( resourcer.Next() )
		{
			crAnimation *anim = rage_new crAnimation;
			// Load the animation
			ValidateResult(anim->Load(resourcer.GetInput(), NULL, true, PARAM_compactanim.Get()));

			resourcer.Resource( anim, "#an" );
		}
	}
	// ...or is it a list of animations to be converted into an animation dictionary?
	else if(resourcer.HasExtension("animlist"))
	{
		const bool baseNameKeys = PARAM_basenamekeys.Get();

		crAnimLoaderBasic loader(PARAM_compactanim.Get());

		while ( resourcer.Next() )
		{
			crAnimDictionary *dictionary = rage_new crAnimDictionary;
			dictionary->AddRef();	// A resource should be created with a ref count of 1
			// Load the dictionary
			ValidateResult(dictionary->LoadAnimations(resourcer.GetInput(), &loader, baseNameKeys));

			resourcer.Resource( dictionary, "#adt" );
		}
	}
	// ... okay, maybe it's a bound?
	else if ( resourcer.HasExtension( "bnd") ) 
	{
		// Lots of mysterious physics setup
		phMaterialMgrImpl<phMaterial>::Create();
		ASSET.PushFolder("$/physics/materials");
		MATERIALMGR.Load();
		ASSET.PopFolder();

		while ( resourcer.Next() )
		{
			// Load the bound
			phBound *bound = phBound::Load( resourcer.GetInput() );
			ValidateResult(bound );

			resourcer.Resource( bound, "#bd" );
		}
		// Manager cleanup
		MATERIALMGR.Destroy();
	}
	// ... Jeez. An individual texture maybe?
	else if ( resourcer.HasExtension( "dds") ) 
	{
		while ( resourcer.Next() )
		{
			// Load the texture
			grcTexture *texture = grcTextureFactory::GetInstance().Create( resourcer.GetInput() );

			resourcer.Resource( texture, "#tx" );
		}
	}
	else
		Quitf("Unable to open input texture list or type file '%s'", resourcer.GetInput());

	SHUTDOWN_PARSER;
	return resourcer.HasErrors();
}
