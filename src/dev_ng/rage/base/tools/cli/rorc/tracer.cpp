// 
// /tracer.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 
#include "file/packfile.h"
#include "file/stream.h"
#include "system/main.h"
#include "system/param.h"

#include <vector>
#include <algorithm>

using namespace rage;

PARAM(tracezip,"Name of an archive containing all of the traces.");
PARAM(virtual,"Tally only virtual memory.");
PARAM(physical,"Tally only physical memory.");
PARAM(leafSize,"Set base leaf size; large value can simulate existing bucket functionality.");

struct allocation {
	size_t size, align, bucketIndex;
};

struct bucket {
	size_t size, capacity;
};

struct CompareAllocation {
	bool operator()( const allocation& a, const allocation& b ) const
	{
		return a.size > b.size;	// sort larger size first
	}
};

int g_Traces;
u64 g_TotalCapacity, g_TotalFragmentation;

static void analyze_trace(std::string name,std::vector<allocation> &trace,const size_t leafSize) {
	// sort by largest first
	std::sort(trace.begin(), trace.end(), CompareAllocation());
	std::vector<bucket> buckets;
	size_t bucketsByHeight[32];
	memset(bucketsByHeight, 0, sizeof(bucketsByHeight));
	for (size_t i=0; i<trace.size(); i++) {
		size_t bestIdx = ~0U, bestRemain = ~0U;
		// can stop early if a bucket was filled exactly (could include some epsilon)
		for (size_t j=0; j<buckets.size() && bestRemain; j++) {
			// compute size of this bucket after adding this trace, accounting for any required alignment
			size_t slop = (((buckets[j].size + (trace[i].align-1)) & ~(trace[i].align-1))) - buckets[j].size;
			size_t newSize = buckets[j].size + slop + trace[i].size;
			// stop now if we overflowed the bucket
			if (newSize > buckets[j].capacity)
				continue;
			size_t thisRemain = (buckets[j].capacity - newSize) + slop;
			// if this leaves the least amount of free space, remember
			if (thisRemain < bestRemain) {
				bestIdx = j;
				bestRemain = thisRemain;
			}
		}
		if (bestIdx == ~0U) {
			bestIdx = buckets.size();
			bucket newBucket;
			newBucket.size = 0;
			newBucket.capacity = leafSize;
			int height = 0;
			while (newBucket.capacity < trace[i].size) {
				++height;
				newBucket.capacity <<= 1;
			}
			buckets.push_back(newBucket);
			bucketsByHeight[height]++;
			while (bucketsByHeight[height] == 2) {
				bucketsByHeight[height] = 0;
				++height;
				bucketsByHeight[height]++;
			}
		}
		trace[i].bucketIndex = bestIdx;
		buckets[bestIdx].size = ((buckets[bestIdx].size + (trace[i].align-1)) & ~(trace[i].align-1)) + trace[i].size;
		Assert(buckets[bestIdx].size <= buckets[bestIdx].capacity);
	}
	size_t internalLoss = 0, totalCapacity = 0;
	for (size_t j=0; j<buckets.size(); j++) {
		internalLoss += buckets[j].capacity - buckets[j].size;
		totalCapacity += buckets[j].capacity;
	}

	Displayf("%s consumed %u buckets, %u total, %u (%u%%) internal fragmentation",name.c_str(),buckets.size(),
		totalCapacity, internalLoss, (internalLoss * 100) / totalCapacity);
	/* Printf("   ");
	for (int i=0; i<24; i++)
		Printf("%d,",bucketsByHeight[i]);
	Displayf(""); */
	++g_Traces;
	g_TotalCapacity += totalCapacity;
	g_TotalFragmentation += internalLoss;
}

int Main() {
	const char *filename = "t:\\xtraces.zip";
	PARAM_tracezip.Get(filename);

	fiPackfile packfile;
	if (!packfile.Init(filename))
		Quitf("Cannot open archive '%s'",filename);
	packfile.ClearRelativeOffset();

	fiFindData data;
	fiHandle h = packfile.FindFileBegin("",data);

	size_t leafSize = 256;
	PARAM_leafSize.Get(leafSize);
	int mask = (1<<0) | (1<<2);	// both virtual and physical for xenon
	if (PARAM_virtual.Get())
		mask = (1<<0);
	else if (PARAM_physical.Get())
		mask = (1<<2);
	// const int mask = (1<<0);	// must pick one or the other for PS3

	size_t biggest = 0, totalInitial = 0;
	if (fiIsValidHandle(h)) {
		do {
			fiStream *S = fiStream::Open(data.m_Name, packfile);
			Assert(S);
			if (S) {
				// Displayf("trace [%s]",data.m_Name);
				std::vector <allocation> trace;
				char linebuf[64];
				size_t accum = 0;
				while (fgetline(linebuf,sizeof(linebuf),S)) {
					if (linebuf[0]=='A') {
						const char *pSize = linebuf+2;
						const char *pAlign = strchr(pSize,',') + 1;
						const char *pHeap = strchr(pAlign,',') + 1;
						allocation temp;
						int heap = atoi(pHeap);
						Assert(heap==0 || heap==2);
						temp.size = atoi(pSize);
						temp.align = atoi(pAlign);
						temp.size = (temp.size + temp.align-1) & ~(temp.align-1);
						temp.bucketIndex = 0;
						if ((1 << heap) & mask)
							trace.push_back(temp);
						accum += temp.size;	// track accumulated size regardless of memory type
					}
					// ignore frees for now
				}
				S->Close();
				// Displayf("%d allocations in %s, %u bytes total",trace.size(),data.m_Name,accum);
				if (accum >= 8192 && trace.size()) {
					if (trace[0].size > biggest)
						biggest = trace[0].size;
					totalInitial += trace[0].size;
					analyze_trace(data.m_Name,trace,leafSize);
				}
			}
		} while (packfile.FindFileNext(h,data));
		packfile.FindFileEnd(h);
	}

	Displayf("**** %d traces analyzed.  %I64uM bytes total, %I64uM bytes (%I64u%%) fragmented.",
		g_Traces,g_TotalCapacity>>20,g_TotalFragmentation>>20,g_TotalFragmentation/(g_TotalCapacity/100));
	Displayf("**** Biggest initial allocation was %u bytes, average is %u.",biggest,g_Traces?totalInitial/g_Traces:0);

	return 0;
}
