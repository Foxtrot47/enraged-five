if [%1]==[] %0 rscbeta_2008
ECHO Make sure Win32 %1 build is up-to-date.
PAUSE
win32_%1\rorc_win32_%1.exe entity.type resourceTest.wdr -path %RAGE_ASSET_ROOT%\sample_rmcore\drawable -shaderpath=%RAGE_ASSET_ROOT% -platform=pc
win32_%1\rorc_win32_%1.exe entity.type resourceTest.xdr -path %RAGE_ASSET_ROOT%\sample_rmcore\drawable -shaderpath=%RAGE_ASSET_ROOT% -platform=xenon
win32_%1\rorc_win32_%1.exe entity.type resourceTest.cdr -path %RAGE_ASSET_ROOT%\sample_rmcore\drawable -shaderpath=%RAGE_ASSET_ROOT% -platform=ps3

win32_%1\rorc_win32_%1.exe rage_male.type resourceTest.wdr -path %RAGE_ASSET_ROOT%\rage_male -shaderpath=%RAGE_ASSET_ROOT% -platform=pc -autotexdict
win32_%1\rorc_win32_%1.exe rage_male.type resourceTest.xdr -path %RAGE_ASSET_ROOT%\rage_male -shaderpath=%RAGE_ASSET_ROOT% -platform=xenon -autotexdict
win32_%1\rorc_win32_%1.exe rage_male.type resourceTest.cdr -path %RAGE_ASSET_ROOT%\rage_male -shaderpath=%RAGE_ASSET_ROOT% -platform=ps3 -autotexdict

win32_%1\rorc_win32_%1.exe entity.textures resourceTest.wtd -path %RAGE_ASSET_ROOT%\sample_rmcore\drawable -shaderpath=%RAGE_ASSET_ROOT% -platform=pc
win32_%1\rorc_win32_%1.exe entity.textures resourceTest.xtd -path %RAGE_ASSET_ROOT%\sample_rmcore\drawable -shaderpath=%RAGE_ASSET_ROOT% -platform=xenon
win32_%1\rorc_win32_%1.exe entity.textures resourceTest.ctd -path %RAGE_ASSET_ROOT%\sample_rmcore\drawable -shaderpath=%RAGE_ASSET_ROOT% -platform=ps3

win32_%1\rorc_win32_%1.exe entity.type entity.cdr -path %RAGE_ASSET_ROOT%\sample_creature\imtest_north -shaderpath=%RAGE_ASSET_ROOT% -platform=ps3 -autotexdict -vertexopt
win32_%1\rorc_win32_%1.exe entity.type entity_edge.cdr -path %RAGE_ASSET_ROOT%\sample_creature\imtest_north -shaderpath=%RAGE_ASSET_ROOT% -platform=ps3 -autotexdict -vertexopt -edgegeom

win32_%1\rorc_win32_%1.exe entity.type entity.cbtm -path %RAGE_ASSET_ROOT%\sample_creature\imtest_north -shaderpath=%RAGE_ASSET_ROOT% -platform=ps3 -autotexdict -blendtargets -vertexopt
win32_%1\rorc_win32_%1.exe entity.type entity_edge.cbtm -path %RAGE_ASSET_ROOT%\sample_creature\imtest_north -shaderpath=%RAGE_ASSET_ROOT% -platform=ps3 -autotexdict -blendtargets -vertexopt -edgegeom

win32_%1\rorc_win32_%1.exe entity.textures %RAGE_ASSET_ROOT%\fragments\stagecoach.wtd -path %RAGE_ASSET_ROOT%\fragments\stagecoach -platform=pc
win32_%1\rorc_win32_%1.exe entity.textures %RAGE_ASSET_ROOT%\fragments\stagecoach.xtd -path %RAGE_ASSET_ROOT%\fragments\stagecoach -platform=xenon
win32_%1\rorc_win32_%1.exe entity.textures %RAGE_ASSET_ROOT%\fragments\stagecoach.ctd -path %RAGE_ASSET_ROOT%\fragments\stagecoach -platform=ps3

win32_%1\rorc_win32_%1.exe entity.textures %RAGE_ASSET_ROOT%\fragments\env_sheet.wtd -path %RAGE_ASSET_ROOT%\fragments\env_sheet -platform=pc
win32_%1\rorc_win32_%1.exe entity.textures %RAGE_ASSET_ROOT%\fragments\env_sheet.xtd -path %RAGE_ASSET_ROOT%\fragments\env_sheet -platform=xenon
win32_%1\rorc_win32_%1.exe entity.textures %RAGE_ASSET_ROOT%\fragments\env_sheet.ctd -path %RAGE_ASSET_ROOT%\fragments\env_sheet -platform=ps3

GOTO skip
win32_%1\rorc_win32_%1.exe textureList2.txt textureList2.wtd -path %RAGE_ASSET_ROOT%\sample_grcore\sample_texdict -platform=pc
win32_%1\rorc_win32_%1.exe textureList2.txt textureList2.xtd -path %RAGE_ASSET_ROOT%\sample_grcore\sample_texdict -platform=xenon
win32_%1\rorc_win32_%1.exe textureList2.txt textureList2Int.xtd -path %RAGE_ASSET_ROOT%\sample_grcore\sample_texdict -platform=xenon -interleavemips
win32_%1\rorc_win32_%1.exe textureList2.txt textureList2.ctd -path %RAGE_ASSET_ROOT%\sample_grcore\sample_texdict -platform=ps3

GOTO skip
ECHO Testing tdViewer application now.  Windows version:
ECHO (Click on the textures to view bigger versions)
$(RS_TOOLSROOT)\bin\tdviewer %RAGE_ASSET_ROOT%\sample_grcore\sample_texdict\textureList2.wtd
$(RS_TOOLSROOT)\bin\tdviewer %RAGE_ASSET_ROOT%\sample_rmcore\drawable\resourceTestNormal.wtd
$(RS_TOOLSROOT)\bin\tdviewer %RAGE_ASSET_ROOT%\sample_rmcore\drawable\resourceTestProxy.wtd
ECHO Testing tdViewer application now.  Xenon version:
ECHO (Make sure the textures look the same between Windows, Xenon, and PS3)
ECHO (and that none look like garbage)
$(RS_TOOLSROOT)\bin\tdviewer %RAGE_ASSET_ROOT%\sample_grcore\sample_texdict\textureList2.xtd
$(RS_TOOLSROOT)\bin\tdviewer %RAGE_ASSET_ROOT%\sample_rmcore\drawable\resourceTestNormal.xtd
$(RS_TOOLSROOT)\bin\tdviewer %RAGE_ASSET_ROOT%\sample_rmcore\drawable\resourceTestProxy.xtd
ECHO Testing tdViewer application now.  PS3 version:
$(RS_TOOLSROOT)\bin\tdviewer %RAGE_ASSET_ROOT%\sample_grcore\sample_texdict\textureList2.ctd
$(RS_TOOLSROOT)\bin\tdviewer %RAGE_ASSET_ROOT%\sample_rmcore\drawable\resourceTestNormal.ctd
$(RS_TOOLSROOT)\bin\tdviewer %RAGE_ASSET_ROOT%\sample_rmcore\drawable\resourceTestProxy.ctd
:skip

ECHO Now run sample_drawable with -rscLoad on Win32, Xenon, and PS3.
