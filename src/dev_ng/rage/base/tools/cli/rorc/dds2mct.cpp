// 
// dds2mct.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#define USE_STOCK_ALLOCATOR

#include "grconvert/mctconvert.h"
#include "system/param.h"
#include "system/main.h"
#include "string/string.h"

#include <stdlib.h>
#include <string.h>

using namespace rage;

int Main() {
	if (sysParam::GetArgCount() == 1) {
		rage::Displayf("usage: dds2mct [-quality x] srcfile.dds [srcfile.dds...]");
		return 1;
	}
	int quality = 80;
	int total = 0;
	for (int i=1; i<sysParam::GetArgCount(); i++) {
		if (!strcmp(sysParam::GetArg(i),"-quality")) {
			quality = atoi(sysParam::GetArg(++i));
			rage::Displayf("Quality now %d",quality);
		}
		else {
			char mct[256];
			strcpy(mct,sysParam::GetArg(i));
			strcpy(strrchr(mct,'.'),".mct");
			total += rage::MctConvert(sysParam::GetArg(i),mct,quality);
		}
	}
	char ddsBuf[64], mctBuf[64];
	Displayf("%d files successfully converted, %sK to %sK",
		total,
		prettyprinter(ddsBuf,sizeof(ddsBuf),g_TotalDds>>10),
		prettyprinter(mctBuf,sizeof(mctBuf),g_TotalMct>>10));
	return 0;
}

