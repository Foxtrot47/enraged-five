#ifndef RORC_UTILS_H
#define RORC_UTILS_H

#include "system/param.h"
#include "file/limits.h"
#include "string/string.h"

PARAM(shaderpath,"Shader library path (only necessary for .type files, assets/tune/db or lib is assumed))");
PARAM(shaderlibpath,"Explicit shader library path");
PARAM(shaderdbpath,"Explicit shader database path");
XPARAM(path);

/* Set up the shader search path as follows.
	If -shaderlibpath or -shaderdbpath are explicitly specified, use them.
	Otherwise, if -shaderpath is specified, look under $shaderpath/tune/shaders/lib and $shaderpath/tune/shaders/db
	Otherwise, if -path is specified, look under $path/tune/shaders/lib and $path/tune/shaders/db
	*/
static void SetupDeferredLoad()
{
	using namespace rage;
	const char *shaderpath = "t:/rage/assets";
	char shaderLibPath[RAGE_MAX_PATH];
	char shaderDbPath[RAGE_MAX_PATH];
	if(!PARAM_shaderpath.Get(shaderpath))
		PARAM_path.Get(shaderpath);
	formatf(shaderLibPath,sizeof(shaderLibPath),"%s/%s", shaderpath, "tune/shaders/lib");
	formatf(shaderDbPath,sizeof(shaderDbPath),"%s/%s", shaderpath, "tune/shaders/db");
	if (PARAM_shaderdbpath.Get(shaderpath))
		safecpy(shaderDbPath,shaderpath,sizeof(shaderDbPath));
	if (PARAM_shaderlibpath.Get(shaderpath))
		safecpy(shaderLibPath,shaderpath,sizeof(shaderLibPath));
	// Tell shader groups to only remember the parameters and not actually create the shaders
	// yet.  This is necessary for offline conversion.  They get created in the resource
	// constructor instead.
	grmShaderGroup::SetDeferredLoad(true,shaderLibPath,shaderDbPath);

}

#endif
