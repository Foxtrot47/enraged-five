REM Annoying fragile thing -- we link with the debug libraries to
REM disambiguate them from the "real" native PC d3d9 libraries.
for %%I in (%TESTERS%) do (
	sed "s,setargv.obj,setargv.obj \&quot;$(XEDK)/lib/win32/vs2005/xgraphics.lib\&quot; \&quot;$(XEDK)/lib/win32/vs2005/d3d9.lib\&quot; \&quot;$(XEDK)/lib/win32/vs2005/xcompress.lib\&quot;,g" %%I_2005.vcproj > tmp
	move tmp %%I_2005.vcproj
)
