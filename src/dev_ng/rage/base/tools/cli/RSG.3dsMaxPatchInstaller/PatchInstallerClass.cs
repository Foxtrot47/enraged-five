﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.IO;
using System.Reflection;

namespace RSG
{
    [RunInstaller(true)]
    public partial class MaxPatchInstaller : System.Configuration.Install.Installer
    {
        public MaxPatchInstaller()
        {
            InitializeComponent();
        }

        #region Public Methods
        public static void CopyStream(Stream input, Stream output)
        {  
            byte[] buffer = new byte[8192];

            int bytesRead;
            while ((bytesRead = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                output.Write(buffer, 0, bytesRead);
            }
        }

        public override void Install(System.Collections.IDictionary stateSaver)
        {
            base.Install(stateSaver);

            Assembly assembly;
            assembly = Assembly.GetExecutingAssembly();

           
            string targetDir = Context.Parameters["DP_TargetDir"].ToString();

            stateSaver.Add("TargetDir", targetDir);

            string mxsDotNetDestFile = targetDir + "/stdplugs/mxsdotnet.dlx";
            string maxConfigDestFile = targetDir + "/3dsmax.exe.config";

            // begin debug
            string[] names = assembly.GetManifestResourceNames();
            StreamWriter sw = new StreamWriter("C:\\install_dump.txt", false);
            foreach (string k in names)
            {
                sw.WriteLine(k);
            }

            sw.Flush();
            sw.Close(); 
            // end debug

            try
            {
                using (Stream input = assembly.GetManifestResourceStream("RSG.Files.overrides.3dsmax.exe.config"))
                using (Stream output = File.Create(maxConfigDestFile))
                {
                    input.CopyTo(output);
                }

                using (Stream input = assembly.GetManifestResourceStream("RSG.Files.overrides.mxsdotNet.dlx"))
                using (Stream output = File.Create(mxsDotNetDestFile))
                {                 
                    input.CopyTo(output);
                }
            }
            catch (Exception e)
            {
                throw new InstallException(e.Message);
            }
        }

        public override void Uninstall(System.Collections.IDictionary savedState)
        {
           
           base.Uninstall(savedState);

           Assembly assembly;
           assembly = Assembly.GetExecutingAssembly();

           string targetDir = savedState["TargetDir"].ToString();

           string mxsDotNetDestFile = targetDir + "/stdplugs/mxsdotnet.dlx";
           string maxConfigDestFile = targetDir + "/3dsmax.exe.config";

           try
           {
               using (Stream input = assembly.GetManifestResourceStream("RSG.Files.native_files.3dsmax.exe.config"))
               using (Stream output = File.Create(maxConfigDestFile))
               {
                   input.CopyTo(output);
               }

               using (Stream input = assembly.GetManifestResourceStream("RSG.Files.native_files.mxsdotnet.dlx"))
               using (Stream output = File.Create(mxsDotNetDestFile))
               {
                    input.CopyTo(output);
               }
           }
           catch (Exception e)
           {
               throw new InstallException(e.Message);
           }
 
        }
        #endregion // Public Methods

        #region Private Methods
        private void ReplaceFile( string src, string dest )
        {
            System.IO.FileInfo fileInfo = new System.IO.FileInfo(dest);
            fileInfo.IsReadOnly = false;
            
            if (File.Exists(src) == true)
                File.Copy(src, dest, true);
           
        }
        #endregion // Private Methods
    }
}

