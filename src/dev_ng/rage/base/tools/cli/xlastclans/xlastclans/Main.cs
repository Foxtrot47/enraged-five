using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.IO;
using System.Xml;
using System.Globalization;

namespace xlastclans
{
    class MainClass
    {
        //Used to copy debug output to console output.
        class MyTraceListener : TraceListener
        {
            public override void Write(string s)
            {
                Console.Write(s);
            }

            public override void WriteLine(string s)
            {
                Console.WriteLine(s);
            }
        }

        private static void Usage()
        {
            string myname = Process.GetCurrentProcess().ProcessName;

            Trace.WriteLine(String.Format("\nUsage: {0} [options] <inname> <outname>", myname));
            Trace.WriteLine("\n<inname> specifies the input file name");
            Trace.WriteLine("\n<outname> specifies the output file name");
        }

        enum PropertyType
        {
            INT32,
            INT64,
            STRING
        }

        class Property
        {
            public string Name;
            public uint Id;
            public int DataSize;
            public uint StringId;

            public Property(string name, uint id, PropertyType type, uint stringId)
            {
                Name = name;
                if(PropertyType.INT32 == type)
                {
                    Id = (uint) id + 0x10000000;
                    DataSize = 4;
                }
                else if(PropertyType.INT64 == type)
                {
                    Id = (uint) id + 0x20000000;
                    DataSize = 8;
                }
                else if(PropertyType.STRING == type)
                {
                    Id = (uint) id + 0x40000000;
                    DataSize = 0;
                }
                StringId = stringId;
            }
        }

        class Table
        {
            public string Name;
            public uint Id;
            public Property RatingProperty;
            public List<Property> Columns = new List<Property>();
            public uint StringId;

            public Table(string name,
                        uint id,
                        Property ratingProp,
                        uint stringId)
            {
                Name = name;
                Id = id;
                RatingProperty = ratingProp;
                StringId = stringId;
            }

            public void AddColumn(Property property)
            {
                Columns.Add(property);
            }
        }

        static void Main(string[] args)
        {
            try
            {
                Debug.Listeners.Add(new MyTraceListener());

                if (args.Length == 0)
                {
                    MainClass.Usage();
                    return;
                }

                string in_fname = null;
                string out_fname = null;
                
                for (int i = 0; i < args.Length; ++i)
                {
                    if(null == in_fname)
                    {
                        in_fname = args[i];
                    }
                    else if(null == out_fname)
                    {
                        out_fname = args[i];
                    }
                }

                if (null == in_fname || null == out_fname)
                {
                    MainClass.Usage();
                    return;
                }

                //Does the input file exist?
                FileInfo in_finfo = new FileInfo(in_fname);

                //Open the xml file
                XmlDocument doc = new XmlDocument();

                Trace.WriteLine("Adding clan tables to " + in_finfo.FullName + "...");

                try
                {
                    string uri = "file://" + in_finfo.FullName;
                    doc.Load(uri);
                }
                catch (System.Exception e)
                {
                    string msg = string.Format("Error loading XML file {0}: {1}",
                                                in_fname,
                                                e.Message);

                    throw(new Exception(msg));
                }

                XmlElement proj = doc.DocumentElement["GameConfigProject"];
                if(null == proj)
                {
                    throw new Exception("No GameConfigProject defined in " + in_fname);
                }

                XmlElement statsViewsEl = proj["StatsViews"];
                if(null == statsViewsEl)
                {
                    throw new Exception("No leader boards defined in " + in_fname);
                }

                XmlElement propertiesEl = proj["Properties"];
                if(null == propertiesEl)
                {
                    throw new Exception("No properties defined in " + in_fname);
                }

                XmlElement stringsEl = proj["LocalizedStrings"];
                if(null == stringsEl)
                {
                    throw new Exception("No strings defined in " + in_fname);
                }

                uint nextViewId = 1;//int.Parse(statsViewsEl.Attributes["nextViewId"].Value);
                uint nextPropertyId = 1;//int.Parse(propertiesEl.Attributes["nextId"].Value);
                uint nextStringId = 1;//int.Parse(stringsEl.Attributes["nextId"].Value);

                //Compute ids to use for views, properties, and strings.

                Dictionary<uint, uint> duplicates = new Dictionary<uint,uint>();

                foreach(XmlNode n in statsViewsEl.ChildNodes)
                {
                    if(n.Name != "StatsView")
                    {
                        continue;
                    }

                    uint id = uint.Parse(n.Attributes["id"].Value);

                    if(duplicates.ContainsKey(id))
                    {
                        throw new Exception("Duplicate StatsView id:" + id);
                    }

                    duplicates.Add(id, id);

                    if(id >= nextViewId)
                    {
                        nextViewId = id + 1;
                    }
                }

                duplicates.Clear();

                foreach(XmlNode n in propertiesEl.ChildNodes)
                {
                    if(n.Name != "Property")
                    {
                        continue;
                    }

                    string idStr = n.Attributes["id"].Value;
                    uint id = uint.Parse(idStr.Substring(2), NumberStyles.HexNumber);

                    if(duplicates.ContainsKey(id))
                    {
                        throw new Exception(string.Format("Duplicate Property id:0x{0:X8}", id));
                    }

                    duplicates.Add(id, id);

                    //0x00008000 is reserved for system-defined properties.
                    id &= ~0xFFFF8000;
                    if(id >= nextPropertyId)
                    {
                        nextPropertyId = id + 1;
                    }

                    if(nextPropertyId >= 0x8000)
                    {
                        throw new Exception("Error generating propery id");
                    }
                }

                duplicates.Clear();

                foreach(XmlNode n in stringsEl.ChildNodes)
                {
                    if(n.Name != "LocalizedString")
                    {
                        continue;
                    }

                    uint id = uint.Parse(n.Attributes["id"].Value);

                    if(duplicates.ContainsKey(id))
                    {
                        throw new Exception("Duplicate String id:" + id);
                    }

                    duplicates.Add(id, id);

                    if(id >= 32500)
                    {
                        //This is probably a system-defined string.
                    }
                    else if(id >= nextStringId)
                    {
                        nextStringId = id + 1;
                    }

                    if(nextPropertyId >= 32500)
                    {
                        throw new Exception("Error generating string id");
                    }
                }

                Dictionary<string, Table> tables = new Dictionary<string,Table>();
                Dictionary<string, uint> stringIds = new Dictionary<string,uint>();
                Dictionary<string, Property> properties = new Dictionary<string,Property>();

                //Add clan strings to the string table
                string[] newStrings =
                {
                    "_ClanOwners",
                    "_ClanMembers",
                    "_ClanMemberId",
                    "_ClanId",
                    "_ClanInviteExpiry",
                    "_ClanNames",
                    "_ClanNameChar",
                    "_ClanTimeJoined",
                };

                for(int i = 0; i < newStrings.Length; ++i)
                {
                    stringIds.Add(newStrings[i], nextStringId++);
                }

                //Clan id property - used as the indexing field in each of
                //the clan leaderboards.
                properties.Add("_ClanId", new Property("_ClanId",
                                                        nextPropertyId++,
                                                        PropertyType.INT64,
                                                        stringIds["_ClanId"]));

                //Clan owners table.
                Table table = new Table("_ClanOwners",
                                        nextViewId++,
                                        properties["_ClanId"],
                                        stringIds["_ClanOwners"]);

                for(int i = 0; i < 63; ++i)
                {
                    string name = string.Format("_ClanMemberId{0:X2}", i);
                    Property property = new Property(name,
                                                    nextPropertyId++,
                                                    PropertyType.INT64,
                                                    stringIds["_ClanMemberId"]);
                    table.AddColumn(property);
                }

                tables.Add(table.Name, table);

                //Invite expiry table.
                table = new Table("_ClanInviteExpiry",
                                    nextViewId++,
                                    properties["_ClanId"],
                                    stringIds["_ClanInviteExpiry"]);

                for(int i = 0; i < 63; ++i)
                {
                    string name = string.Format("_ClanInviteExpiry{0:X2}", i);
                    Property property = new Property(name,
                                                    nextPropertyId++,
                                                    PropertyType.INT64,
                                                    stringIds["_ClanInviteExpiry"]);
                    table.AddColumn(property);
                }

                tables.Add(table.Name, table);

                //Clan names table.
                table = new Table("_ClanNames",
                                    nextViewId++,
                                    properties["_ClanId"],
                                    stringIds["_ClanNames"]);

                for(int i = 0; i < 63; ++i)
                {
                    string name = string.Format("_ClanNameChar{0:X2}", i);
                    Property property = new Property(name,
                                                    nextPropertyId++,
                                                    PropertyType.INT32,
                                                    stringIds["_ClanNameChar"]);
                    table.AddColumn(property);
                }

                tables.Add(table.Name, table);

                //Clan members table.
                table = new Table("_ClanMembers",
                                    nextViewId++,
                                    properties["_ClanId"],
                                    stringIds["_ClanMembers"]);

                table.AddColumn(new Property("_ClanTimeJoined",
                                                nextPropertyId++,
                                                PropertyType.INT64,
                                                stringIds["_ClanTimeJoined"]));

                tables.Add(table.Name, table);

                //Add the column properties to the properties collection.
                foreach(Table t in tables.Values)
                {
                    foreach(Property p in t.Columns)
                    {
                        properties.Add(p.Name, p);
                    }
                }

                //Check for prior existence of clan tables.
                XmlNode node = statsViewsEl.FirstChild;
                XmlNode next = node.NextSibling;

                for(; null != node; node = next)
                {
                    next = next.NextSibling;

                    string name = node.Attributes["friendlyName"].Value;

                    if(tables.ContainsKey(name))
                    {
                        throw(new Exception(in_fname + " already contains clan tables"));

                        //Trace.WriteLine(string.Format("Removing leaderboard {0}", name));
                        //statsViewsEl.RemoveChild(node);
                    }
                }

                //Check for prior existence of clan properties.
                node = propertiesEl.FirstChild;
                next = node.NextSibling;

                for(; null != node; node = next)
                {
                    next = next.NextSibling;

                    string name = node.Attributes["friendlyName"].Value;
                    if(properties.ContainsKey(name))
                    {
                        throw(new Exception(in_fname + " already contains clan tables"));

                        //Trace.WriteLine(string.Format("Removing property {0}", name));
                        //propertiesEl.RemoveChild(node);
                    }
                }

                //Check for prior existence of clan strings.
                node = stringsEl.FirstChild;
                next = node.NextSibling;

                for(; null != node; node = next)
                {
                    next = next.NextSibling;

                    if(node.Name != "LocalizedString")
                    {
                        continue;
                    }

                    string name = node.Attributes["friendlyName"].Value;
                    if(stringIds.ContainsKey(name))
                    {
                        throw(new Exception(in_fname + " already contains clan tables"));

                        //Trace.WriteLine(string.Format("Removing string {0}", name));
                        //stringsEl.RemoveChild(node);
                    }
                }

                //Add clan strings to the XML doc
                foreach(KeyValuePair<string, uint> kvp in stringIds)
                {
                    XmlDocumentFragment frag = doc.CreateDocumentFragment();

                    string fmt = 
			            "<LocalizedString clsid=\"{{F88D7D87-B2C0-452D-977D-297D70400AFF}}\" id=\"{0}\" friendlyName=\"{1}\" xmlns=\"{2}\">"
				        + "<Translation locale=\"de-DE\">{1}</Translation>"
				        + "<Translation locale=\"en-US\">{1}</Translation>"
				        + "<Translation locale=\"es-ES\">{1}</Translation>"
				        + "<Translation locale=\"fr-FR\">{1}</Translation>"
				        + "<Translation locale=\"it-IT\">{1}</Translation>"
				        + "<Translation locale=\"ja-JP\">{1}</Translation>"
			            + "</LocalizedString>";

                    string innerXml = string.Format(fmt, kvp.Value, kvp.Key, proj.NamespaceURI);

                    frag.InnerXml = innerXml;

                    Trace.WriteLine("Adding string " + kvp.Key);

                    stringsEl.AppendChild(frag);
                }

                //Add clan properties to the XML doc
                foreach(KeyValuePair<string, Property> kvp in properties)
                {
                    XmlDocumentFragment frag = doc.CreateDocumentFragment();

                    string fmt = 
			            "<Property clsid=\"{{43CE4FA7-6DE9-4F37-A929-49BFE63CBAAA}}\" id=\"0x{0:X8}\" stringId=\"{1}\" dataSize=\"{2}\" friendlyName=\"{3}\" xmlns=\"{4}\"/>";

                    string innerXml =
                        string.Format(fmt, kvp.Value.Id, kvp.Value.StringId, kvp.Value.DataSize, kvp.Key, proj.NamespaceURI);

                    frag.InnerXml = innerXml;

                    Trace.WriteLine("Adding property " + kvp.Key);

                    propertiesEl.AppendChild(frag);
                }

                //Add clan tables to the XML doc
                foreach(KeyValuePair<string, Table> kvp in tables)
                {
                    Trace.WriteLine("Adding table " + kvp.Key);

                    WriteTable(statsViewsEl, kvp.Value);
                }

                //Update next ids for strings, properties, tables.
                statsViewsEl.Attributes["nextViewId"].Value = nextViewId.ToString();
                propertiesEl.Attributes["nextId"].Value = nextPropertyId.ToString();
                stringsEl.Attributes["nextId"].Value = nextStringId.ToString();

                Trace.WriteLine("Writing to " + out_fname + "...");

                XmlWriterSettings xmlSettings = new XmlWriterSettings();
                xmlSettings.Indent = true;
                xmlSettings.IndentChars = "\t";
                xmlSettings.Encoding = Encoding.Unicode;
                XmlWriter writer = XmlWriter.Create(out_fname, xmlSettings);

                doc.Save(writer);

                writer.Flush();

                //doc.Save(out_fname);

                Trace.WriteLine("Succeeded");
            }
            catch (System.Exception e)
            {
                Trace.WriteLine(e.Message);
            }
        }

        private static void WriteTable(XmlElement parent, Table table)
        {
            XmlElement view = CreateElement(parent, "StatsView");
            XmlElement cols = CreateElement(parent, "Columns");

            WriteTableHeader(view, cols, table.Name, table.Id, table.StringId, table.RatingProperty);

            int ordinal = 3;
            int attrId = 1;

            foreach(Property p in table.Columns)
            {
                WriteField(cols, p, ordinal, attrId);
                ++ordinal;
                ++attrId;
            }

            view.AppendChild(cols);

            parent.AppendChild(view);
        }

        private static void WriteTableHeader(XmlElement view,
                                            XmlElement cols,
                                            string name,
                                            uint id,
                                            uint stringId,
                                            Property ratingProperty)
        {
            XmlAttribute attr = CreateAttribute(view, "clsid");
            attr.Value = "{6B341F2D-7A8A-43BC-AABF-949D6B9F28C7}";
            view.Attributes.Append(attr);

            attr = CreateAttribute(view, "id");
            attr.Value = id.ToString();
            view.Attributes.Append(attr);

            attr = CreateAttribute(view, "stringId");
            attr.Value = stringId.ToString();
            view.Attributes.Append(attr);

            attr = CreateAttribute(view, "resetType");
            attr.Value = "Never";
            view.Attributes.Append(attr);

            attr = CreateAttribute(view, "friendlyName");
            attr.Value = name;
            view.Attributes.Append(attr);

            attr = CreateAttribute(view, "entryExpiration");
            attr.Value = "0";
            view.Attributes.Append(attr);

            attr = CreateAttribute(view, "hidden");
            attr.Value = "true";
            view.Attributes.Append(attr);

            attr = CreateAttribute(view, "arbitrated");
            attr.Value = "false";
            view.Attributes.Append(attr);

            attr = CreateAttribute(view, "onlineOnly");
            attr.Value = "false";
            view.Attributes.Append(attr);

            attr = CreateAttribute(view, "topEntries");
            attr.Value = "4294967295";
            view.Attributes.Append(attr);

            attr = CreateAttribute(view, "maxAttachments");
            attr.Value = "0";
            view.Attributes.Append(attr);

            attr = CreateAttribute(view, "viewType");
            attr.Value = "Leaderboard";
            view.Attributes.Append(attr);

            Property rank = new Property("Rank", 0x8001, PropertyType.INT32, 32518);
            Property gamerName = new Property("Gamer Name", 0x8002, PropertyType.STRING, 32516);

            WriteField(cols, rank, 0, 65535);
            WriteField(cols, gamerName, 1, 65533);
            WriteField(cols, ratingProperty, 2, 65534);
        }

        private static void WriteField(XmlElement parent,
                                        Property prop,
                                        int ordinal,
                                        int attrId)
        {
            XmlElement fldEl = CreateElement(parent, "Field");

            XmlAttribute attr = CreateAttribute(fldEl, "ordinal");
            attr.Value = ordinal.ToString();
            fldEl.Attributes.Append(attr);

            attr = CreateAttribute(fldEl, "stringId");
            attr.Value = prop.StringId.ToString();
            fldEl.Attributes.Append(attr);

            attr = CreateAttribute(fldEl, "hidden");
            attr.Value = "false";
            fldEl.Attributes.Append(attr);

            attr = CreateAttribute(fldEl, "friendlyName");
            attr.Value = prop.Name;
            fldEl.Attributes.Append(attr);

            attr = CreateAttribute(fldEl, "attributeId");
            attr.Value = attrId.ToString();
            fldEl.Attributes.Append(attr);

            XmlElement propEl = CreateElement(fldEl, "Property");

            attr = CreateAttribute(propEl, "id");
            attr.Value = string.Format("0x{0:X8}", prop.Id);
            propEl.Attributes.Append(attr);

            XmlElement aggEl = CreateElement(propEl, "Aggregation");

            attr = CreateAttribute(aggEl, "type");
            attr.Value = "Last";
            aggEl.Attributes.Append(attr);

            propEl.AppendChild(aggEl);
            fldEl.AppendChild(propEl);
            parent.AppendChild(fldEl);
        }

        static XmlElement CreateElement(XmlElement parent, string name)
        {
            return parent.OwnerDocument.CreateElement(name, parent.NamespaceURI);
        }

        static XmlAttribute CreateAttribute(XmlElement parent, string name)
        {
            return parent.OwnerDocument.CreateAttribute(name);
        }
    }
}
