using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;

using RSG.Base;
using RSG.Base.Command;
using RSG.Base.SCM;

namespace AssemblyVersionUpdater
{
    class Program
    {
        static int GetVersion( out string version )
        {
            version = null;

            string rageRootFolder = null;
            try
            {
                rageRootFolder = Environment.GetEnvironmentVariable( "RAGE_ROOT_FOLDER" );
                if ( String.IsNullOrEmpty( rageRootFolder) )
                {
                    Console.WriteLine( "Invalid RAGE_ROOT_FOLDER environment variable." );
                    return 1;
                }
            }
            catch ( System.Exception e )
            {
                Console.WriteLine( e.Message );
                return 1;
            }

            string versionFilename = Path.Combine( rageRootFolder, @"build\version.h" );
            try
            {
                if ( !File.Exists( versionFilename ) )
                {
                    Console.WriteLine( "Unable to locate '{0}'.", versionFilename );
                    return 1;
                }
            }
            catch ( System.Exception e )
            {
                Console.WriteLine( e.Message );
                return 1;
            }

            List<string> lines = new List<string>();

            TextReader reader = null;
            try
            {
                reader = new StreamReader( versionFilename );

                string line = reader.ReadLine();
                while ( line != null )
                {
                    lines.Add( line );

                    line = reader.ReadLine();
                }
            }
            catch ( System.Exception e )
            {
                Console.WriteLine( e.Message );
                return 1;
            }
            finally
            {
                if ( reader != null )
                {
                    reader.Close();
                }
            }

            string releaseNumber = string.Empty;
            string majorVersion = string.Empty;
            string minorVersion = string.Empty;

            foreach ( string line in lines )
            {
                string[] split = line.Split( new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries );
                if ( (split.Length >= 3) && (split[0] == "#define") )
                {
                    switch ( split[1] )
                    {
                        case "RAGE_RELEASE":
                            {
                                releaseNumber = split[2];
                            }
                            break;
                        case "RAGE_MAJOR_VERSION":
                            {
                                majorVersion = split[2];
                            }
                            break;
                        case "RAGE_MINOR_VERSION":
                            {
                                minorVersion = split[2];
                            }
                            break;
                    }
                }
            }

            if ( !String.IsNullOrEmpty( releaseNumber ) && !String.IsNullOrEmpty( majorVersion ) && !String.IsNullOrEmpty( minorVersion ) )
            {
                version = String.Format( "{0}.{1}.{2}.*", majorVersion, minorVersion, releaseNumber );
                return 0;
            }

            Console.WriteLine( "Missing one or more of the following: RAGE_RELEASE, RAGE_MAJOR_VERSION and/or RAGE_MINOR_VERSION." );
            return 1;
        }

        static int WriteAssemblyInfoFile( string filename, string version, string company )
        {
            // Look for the current version
            try
            {
                if ( File.Exists( filename ) )
                {
                    List<string> lines = new List<string>();

                    TextReader reader = null;
                    try
                    {
                        reader = new StreamReader( filename );

                        string line = reader.ReadLine();
                        while ( line != null )
                        {
                            lines.Add( line );

                            line = reader.ReadLine();
                        }
                    }
                    catch ( System.Exception e )
                    {
                        Console.WriteLine( e.Message );
                        return 1;
                    }
                    finally
                    {
                        if ( reader != null )
                        {
                            reader.Close();
                        }
                    }

                    string currentAssemblyVersion = string.Empty;
                    string currentAssemblyCompany = string.Empty;

                    string assemblyVersionSearchText = " AssemblyVersion( \"";
                    string assemblyCompanySearchText = " AssemblyCompany( \"";
                    foreach ( string line in lines )
                    {
                        int start = line.IndexOf( assemblyVersionSearchText );
                        if ( start != -1 )
                        {
                            start += assemblyVersionSearchText.Length;

                            int end = line.IndexOf( "\"", start + 1 );
                            if ( end != -1 )
                            {
                                currentAssemblyVersion = line.Substring( start, end - start );
                            }

                            continue;
                        }

                        start = line.IndexOf( assemblyCompanySearchText );
                        if ( start != -1 )
                        {
                            start += assemblyCompanySearchText.Length;

                            int end = line.IndexOf( "\"", start + 1 );
                            if ( end != -1 )
                            {
                                currentAssemblyCompany = line.Substring( start, end - start );
                            }
                        }
                    }

                    if ( (currentAssemblyVersion == version) && (currentAssemblyCompany == company) )
                    {
                        Console.WriteLine( "'{0}' is up to date.", filename );
                        return 0;
                    }
                }
            }
            catch ( System.Exception e )
            {
                Console.WriteLine( e.Message );
                return 1;
            }

            rageStatus status;
            ragePerforce.CheckOutFile( filename, "Assembly Version Update", out status );
            if ( !status.Success() )
            {
                Console.WriteLine( status.ErrorString );
                return 1;
            }

            TextWriter writer = null;
            try
            {
                writer = new StreamWriter( filename, false );

                writer.WriteLine( "using System.Reflection;" );
                writer.WriteLine( "using System.Runtime.CompilerServices;" );
                writer.WriteLine( "using System.Runtime.InteropServices;" );
                writer.WriteLine( string.Empty );
                writer.WriteLine( "/******************************************************************************" );
                writer.WriteLine( "* DO NOT EDIT THIS FILE.  IT IS AUTO-GENERATED." );
                writer.WriteLine( "* " );
                writer.WriteLine( "* To use in your project:" );
                writer.WriteLine( "*     1) Add this file to your project.  If you are using a common file, such as" );
                writer.WriteLine( "*        rage/build/CommonAssemblyInfo.cs, then add the file as a Link.  This" );
                writer.WriteLine( "*        option can be found on the Add Existing Item dialog in the Add button's" );
                writer.WriteLine( "*        drop down menu." );
                writer.WriteLine( "*     2) Edit your project's AssemblyInfo.cs file, commenting out the Assembly*" );
                writer.WriteLine( "*        items you see in this file." );
                writer.WriteLine( "******************************************************************************/" );
                writer.WriteLine( string.Empty );
                writer.WriteLine( "// General Information about an assembly is controlled through the following " );
                writer.WriteLine( "// set of attributes. Change these attribute values to modify the information" );
                writer.WriteLine( "// associated with an assembly." );
                writer.WriteLine( String.Format( "[assembly: AssemblyCompany( \"{0}\" )]", company ) );
                writer.WriteLine( String.Format( "[assembly: AssemblyCopyright( \"Copyright � {0} {1}\" )]", company, DateTime.Now.Year ) );
                writer.WriteLine( string.Empty );
                writer.WriteLine( "// Version information for an assembly consists of the following four values:" );
                writer.WriteLine( "//" );
                writer.WriteLine( "//      Major Version" );
                writer.WriteLine( "//      Minor Version " );
                writer.WriteLine( "//      Build Number" );
                writer.WriteLine( "//      Revision" );
                writer.WriteLine( "//" );
                writer.WriteLine( "// You can specify all the values or you can default the Revision and Build Numbers " );
                writer.WriteLine( "// by using the '*' as shown below:" );
                writer.WriteLine( String.Format( "[assembly: AssemblyVersion( \"{0}\" )]", version ) );
                writer.WriteLine( string.Empty );
            }
            catch ( System.Exception e )
            {
                Console.WriteLine( e.Message );
                return 1;
            }
            finally
            {
                if ( writer != null )
                {
                    writer.Close();
                }
            }

            Console.WriteLine( "'{0}' Saved.", filename );
            return 0;
        }

        static int Main( string[] args )
        {
            rageCommandLineParser parser = new rageCommandLineParser();
            rageCommandLineItem filenameItem = parser.AddItem(
                new rageCommandLineItem( string.Empty, 1, true, "filename", "Full path to the AssemblyInfo.cs file." ) );
            rageCommandLineItem companyNameItem = parser.AddItem(
                new rageCommandLineItem( "company", 1, false, "[company]", "The name of the company.  Default: Rockstar San Diego." ) );

            if ( !parser.Parse( args ) )
            {
                Console.WriteLine( parser.Error );
                return 1;
            }

            string version;
            int result = GetVersion( out version );
            if ( result != 0 )
            {
                return result;
            }

            string filename = Path.GetFullPath( filenameItem.Value as string );
            string companyName = companyNameItem.WasSet ? companyNameItem.Value as string : "Rockstar Games";
            return WriteAssemblyInfoFile( filename, version, companyName );
        }
    }
}
