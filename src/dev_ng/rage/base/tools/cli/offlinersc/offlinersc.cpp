// 
// offlinersc.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "system/param.h"
#include "grcore/texturedefault.h"
#include "grcore/texturexenonproxy.h"
#include "grcore/texturegcm.h"
#include "rmcore/drawable.h"
#include "grmodel/modelfactory.h"
#include "grmodel/shaderfx.h"
#include "system/platform.h"
#include "file/asset.h"
#include "file/token.h"
#include "grcore/image.h"

#include "system/param.h"
#include "file/limits.h"
#include "string/string.h"
#include "grmodel/shadergroup.h"

#include "paging/dictionary.h"
#include "paging/rscbuilder.h"
#include "diag/output.h"
#include "offlinersc/offlinersc.h"
#include "cranimation/animation.h"
#include "diag/xmllog.h"
#include <iostream>

using namespace rage;

PARAM( path,		  "[offlinersc] Directory containing source and destination files");
PARAM( folder,		  "[offlinersc] Batchable subfolder to allow large batchsizes");
PARAM(maxheapsize,"[offlinersc] sets the heap size to the given amount in megabytes ( default is 32 meg so should only be necessary in level based games )");

PARAM( vchunksize,  "[offlinersc] Set maximum  virtual  allocation size, in kilobytes");
PARAM( pchunksize,  "[offlinersc] Set maximum physical allocation size, in kilobytes");
PARAM( chunksize,  "[offlinersc] Set maximum physical allocation size, in kilobytes");
PARAM( platform,   "[offlinersc] Target platform (default pc; can be xenon or psn)");
PARAM( autotexdict,"[offlinersc] Enable automatic texture dictionary generation for drawables");
PARAM( texdict,"[offlinersc] Texture dictionary to use with this asset");
PARAM( version,"[offlinersc] Append a version number to the resourced asset for easier versioning");
PARAM( msbuild,"[offlinersc] Uses msbuild complaint error and warning messages");
PARAM( disallowErrors, "[offlinersc] will not save resource if any errors occur during building" );
PARAM( packuv,"[offlinersc] pack the model UV co-ordinates (unpacked by default)" );

PARAM(shaderpath,"[offlinersc] Shader library path (only necessary for .type files, assets/tune/db or lib is assumed))");
PARAM(shaderlibpath,"[offlinersc] Explicit shader library path");
PARAM(shaderdbpath,"[offlinersc] Explicit shader database path");
PARAM(shaderlibs,"[offlinersc] Explicit shader library path");
PARAM(shaderdb,"[offlinersc] Explicit shader database path");
PARAM(verbose,"[offlinersc] displays info all the assets it's converting, can be 'quiet', 'normal', 'verbose'");
PARAM(quiet,"[offlinersc] only displays error messages and warnings");

PARAM(logDetails,"[offlinersc] adds details of resource to log file ");


//------------------------------------------------

VersionFile::VersionFile( const char* name) 
	: m_log( rage_new XmlFileLog( name, "xml" ) )
{}
VersionFile::~VersionFile() 
{ 
	delete m_log; 
}
void VersionFile::Output( const char* name, int version )
{
	std::string	tagName = std::string( name ) + "Version";
	if ( m_log )
	{
		m_log->Write( tagName.c_str(), version );
	}
}


//---------------------------------------------------

static pgDictionary<grcTexture>* SetupReferenceTextureDictionary( const char* inputList )
{
	pgDictionary<grcTexture> *textureDictionary = NULL;

	// Is the input filetype a list of textures?
	fiStream *S = ASSET.Open(inputList,"txt",true,true);
	if (!S)
		S = ASSET.Open(inputList,"textures",true,true);
	if (S) {
		char buf[256];
		int count = 0;
		fiTokenizer T(inputList,S);
		while (T.GetToken(buf,sizeof(buf)))			// get the number of textures
			count++;

		if ( count )
		{
			textureDictionary = rage_new pgDictionary<grcTexture>( count);
			S->Seek(0);
			fiTokenizer T(inputList,S);

			while (T.GetToken(buf,sizeof(buf))) 
			{
				if (!textureDictionary->AddEntry(buf, rage_new grcTextureString( buf )))
				{
					Errorf("Texture '%s' collided with something already in this dictionary!", buf );
				}
			}
		}
		S->Close();	
	}
	return textureDictionary;
}

static bool s_SetupGraphics;

OffLineResourceCreator::OffLineResourceCreator(  bool SetupGraphics, bool PreloadShaders ) 
		: m_referenceTextureDictionary(0), m_assetCount(0)
{
	grcDevice::InitSingleton();

	const char *path;
	if (PARAM_path.Get(path))
		ASSET.SetPath(path);

	if ( PARAM_msbuild.Get() )
	{
		// setup for msbuild style output 
		diagOutput::UseVSFormatOutput();
		diagOutput::DisablePopUpQuits();
		diagOutput::DisablePopUpErrors();
	}
	if ( PARAM_quiet.Get() )
	{
		diagOutput::SetOutputMask( diagOutput::OUTPUT_WARNINGS | diagOutput::OUTPUT_ERRORS );
	}

	// Set a chunk size for the resource; this guarantees that any allocation will never cross
	// a multiple of this size, allowing you to do a gather-scatter read into multiple non-contiguous
	// buffers all of chunkSize bytes each.
	m_vchunkSize = 0;
	m_pchunkSize = 0;

	PARAM_vchunksize.Get(m_vchunkSize);
	PARAM_pchunksize.Get(m_pchunkSize);
	if ( PARAM_chunksize.Get(m_vchunkSize) )
	{
		m_pchunkSize  = m_vchunkSize;
	}
	m_vchunkSize <<= 10;
	m_pchunkSize <<= 10;

	const char *platform = "pc";
	if (!PARAM_platform.Get(platform)) 
	{
		// try to detect platform from output name
		const char *output = sysParam::GetArg(2);
		output = strrchr(output,'.');
		if (output) 
		{
			if (output[1]=='x'||output[1]=='X') 
			{
				/// Displayf("Autodetect platform as Xenon");
				platform = "xenon";
			}
			else if (output[1]=='c'||output[1]=='C') 
			{
				/// Displayf("Autodetect platform as PS3");
				platform = "ps3";
			}
		}
	}
	g_sysPlatform = sysGetPlatform(platform);
#if __RESOURCECOMPILER
	g_ByteSwap = sysGetByteSwap(g_sysPlatform);
#endif

	m_buildHeapSize = 32;
	PARAM_maxheapsize.Get(m_buildHeapSize);
	m_buildHeapSize *= 1024 * 1024;

	s_SetupGraphics = SetupGraphics;

	if ( SetupGraphics )
	{
#if __RESOURCECOMPILER
		// Create a texture factory appropriate for the target platform.
		if (g_sysPlatform == platform::XENON) {
			grcTextureFactoryXenonProxy::CreatePagedTextureFactory();
		}
		else if (g_sysPlatform == platform::PS3) {
			grcTextureFactoryGCM::CreatePagedTextureFactory();
		}
		else 
#endif
		{
			grcTextureFactoryDefault::CreatePagedTextureFactory();
		}

		const char *shaderpath = "t:/rage/assets";
		char shaderLibPath[RAGE_MAX_PATH];
		char shaderDbPath[RAGE_MAX_PATH];
		if(!PARAM_shaderpath.Get(shaderpath))
			PARAM_path.Get(shaderpath);
		formatf(shaderLibPath,sizeof(shaderLibPath),"%s/%s", shaderpath, "tune/shaders/lib");
		formatf(shaderDbPath,sizeof(shaderDbPath),"%s/%s", shaderpath, "tune/shaders/db");
		if (PARAM_shaderdbpath.Get(shaderpath))
			safecpy(shaderDbPath,shaderpath,sizeof(shaderDbPath));
		if (PARAM_shaderdb.Get(shaderpath))
			safecpy(shaderDbPath,shaderpath,sizeof(shaderDbPath));
		if (PARAM_shaderlibpath.Get(shaderpath))
			safecpy(shaderLibPath,shaderpath,sizeof(shaderLibPath));
		if (PARAM_shaderlibs.Get(shaderpath))
			safecpy(shaderLibPath,shaderpath,sizeof(shaderLibPath));

		grmModelFactory::CreateStandardModelFactory();
		grmShaderFactory::CreateStandardShaderFactory();
	
		if (PreloadShaders)
		{
			char* tok = strtok(shaderLibPath, ";");
			while (tok)
			{
				grcEffect::Preload(tok);
				tok = strtok(NULL, ";");
			}
		}

		if (PreloadShaders)
		{
			char* tok = strtok(shaderDbPath, ";");
			while (tok)
			{
				grcMaterialLibrary *matLib = grcMaterialLibrary::Preload(tok);
				if (matLib)
					matLib->grcMaterialLibrary::Push();
				tok = strtok(NULL, ";");
			}
		}

		// Make sure normal packing is enabled when targetting Xenon.
		grmModelFactory::GetVertexConfigurator()->SetPackNormals(g_sysPlatform != platform::WIN32PC);

		if (PARAM_packuv.Get())
		{
			// Make sure uv packing is enabled when targetting Xenon.
			grmModelFactory::GetVertexConfigurator()->SetPackTexCoords(g_sysPlatform != platform::WIN32PC);
		}

		// Verify it actually compiles!
		// pgDictionary<fragType>::SetCurrent(pgDictionary<fragType>::GetCurrent());

		// Tell shader groups to create texture dictionaries automatically if desired.
		// In this case, the textures will live in the same resource as the drawable
		// instead of being in a separate texture dictionary.
		grmShaderGroup::SetAutoTexDict(PARAM_autotexdict.Get());

	}
	
	// Initialize animation system
	crAnimation::InitClass();

	if ( PARAM_texdict.Get() )
	{
		const char* name;
		PARAM_texdict.Get( name );
		m_referenceTextureDictionary = SetupReferenceTextureDictionary( name);
		pgDictionary<grcTexture>::SetCurrent( m_referenceTextureDictionary );
	}

	m_arg1 =  sysParam::GetArg(1);
	m_arg2 =  sysParam::GetArg(2);
	
	m_inputList.reserve(32);
	m_rscName.reserve(32);
	m_folders.reserve(32);

	GetFNames( &m_arg1[0], m_inputList );
	GetFNames(  &m_arg2[0], m_rscName );
	
	const char* folder;
	if ( PARAM_folder.Get(folder) )
	{	
		GetFNames( const_cast<char*>(folder), m_folders );
		Assert( m_inputList.size() == m_folders.size()  ||  m_folders.size() == 1 );
		ASSET.PushFolder( m_folders[ 0] );		// go to the first folder 
	}
	Assert( m_inputList.size() == m_rscName.size() );

	m_pass = 0;
}

OffLineResourceCreator::~OffLineResourceCreator() 
{ 
	if ( PARAM_verbose.Get())
	{
		Displayf("Largest virtual allocation was %u bytes, largest physical was %u bytes.",
			g_LargestVirtualAllocation, g_LargestPhysicalAllocation);

		Debugf1( 2,"-- Operation completed in %f seconds. -- ",m_elapsed.GetTime());
	}
	
	CleanUp();
}

void OffLineResourceCreator::GetFNames( char* m_inputListList, std::vector<char*>& v  )
{
	char* m_endPtr = m_inputListList;
	v.push_back( m_endPtr );
	m_endPtr = strchr( m_endPtr, ';' );
	while ( m_endPtr )
	{
		*m_endPtr = '\0';
		m_endPtr++;
		v.push_back( m_endPtr );
		m_endPtr = strchr( m_endPtr, ';' );
	}

	
}
		
bool OffLineResourceCreator::Next()
{
	bool result = m_assetCount < m_inputList.size();

	if ( result )
	{
		ResetErrors();

		// Make sure the texture free list (used for xenon texture interleaving) is always
		// cleared between resource heaps.
		if (grcTextureFactory::HasInstance())
			grcTextureFactory::GetInstance().ClearFreeList();

		pgRscBuilder::SetBuildName(GetRscName());
		pgRscBuilder::BeginBuild( m_pass? true : false, m_buildHeapSize, m_vchunkSize, m_pchunkSize);
		if ( m_assetCount >0 && m_folders.size() && m_pass == 0)
		{
			ASSET.PushFolder( m_folders.size() == 1 ? m_folders[0] : m_folders[ m_assetCount] );
		}
		if ( PARAM_verbose.Get() )
		{
			Debugf1( 2, "%s\t=>\t%s", GetInput(), GetRscName() );
		}
	}
	return result;
}
void OffLineResourceCreator::ResetErrors() 
{ 
	m_currentErrorCount = diagOutput::GetErrorCount(); 
}
bool OffLineResourceCreator::HasErrors() const 
{ 
	return !( !PARAM_disallowErrors.Get() || m_currentErrorCount == diagOutput::GetErrorCount());
}
bool OffLineResourceCreator::HasExtension( const char* ext ) const
{
	return ASSET.Exists( GetInput(),ext );
}
void OffLineResourceCreator::CleanUp()
{
	// Shutdown animation system
	crAnimation::ShutdownClass();

	if (s_SetupGraphics) {
		if (!g_ByteSwap && m_referenceTextureDictionary)
		{
			m_referenceTextureDictionary->Release();
		}
		// Unload all of the materials
		while (grcMaterialLibrary *matLib = static_cast<grcMaterialLibrary*>(grcMaterialLibrary::GetCurrent())) {
			grcMaterialLibrary::GetCurrent()->Pop();
			delete matLib;
		}

		grmShader::ShutdownClass();

		delete &grmShaderFactory::GetInstance();
		delete &grmModelFactory::GetInstance();
		delete &grcTextureFactory::GetInstance();
	}
}

void OffLineResourceCreator::LogDetails( int versionNo , const char* type )
{
	if ( PARAM_logDetails.Get() )
	{
		sysMemStartTemp();
		size_t pSize;
		size_t vSize;
		size_t tSize;
		pgRscBuilder::GetSize( vSize, pSize );
		tSize = vSize + pSize;
		char ext[] = "#.desc";
		ext[0] = g_sysPlatform;
		{
			XmlFileLog log( GetRscName(), ext);

			log.GroupStart(  "Resource" );
				log.Write( "Name", ASSET.FileName( GetRscName() ) );
				log.Write( "Path", GetRscName() );
				log.Write( "VirtualSize", vSize);
				log.Write( "PhysicalSize", pSize);
				log.Write( "TotalSize", tSize);
				log.Write( "Type", type);
				log.Write( "Version", versionNo);
			log.GroupEnd( "Resource" );
		}
		
		sysMemEndTemp();
	}
}

