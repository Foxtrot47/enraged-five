// 
// offlinersc/offlinersc.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef RAGE_OFFLINERSC_H
#define RAGE_OFFLINERSC_H

#include <vector>
#include "atl/ptr.h"
#include "atl/string.h"
#include "system/timer.h"

#if !__WIN32PC || !__PAGING
#error "Only Win32 {Debug/Release} will work properly."
#endif

namespace rage
{
	template<class T> class pgDictionary;
	class grcTexture;

	// PURPOSE
	//		Helper class to make resourcing easier and faster
	//		
	//	When offline resourcing objects you just need to construct this object at the top of the main function
	//	It will set up rage for you. Then call the next function to get the next object on the inputlist
	//	and just call resource passing in your object and it's extension.
	//
	class OffLineResourceCreator
	{
		static const int DefaultMemorySize = 16 * 1024 * 1024 ;
	public:
		OffLineResourceCreator( bool SetupGraphics = true, bool PreloadShaders = true );
		virtual ~OffLineResourceCreator();


		// PURPOSE
		//		Iterates over the input list and sets up the resouring
		//		
		bool Next();


		
		// PURPOSE
		//		Resources the object
		//	
		template<class T>
		void Resource( T* ptr , const char* ext, int VersionNo =  T::RORC_VERSION )
		{
			if ( !ptr )
			{
				Errorf("Resource does not exist.");
			}
			if ( ptr && !HasErrors() && m_pass == pgRscBuilder::LAST_PASS)
			{
				pgRscBuilder::ReadyBuild(ptr);
				datTypeStruct Structure;
				ptr->DeclareStruct( Structure );
				if ( !HasErrors() )  // any resource errors
				{
					pgRscBuilder::SaveBuild( GetRscName(),ext, VersionNo );
					LogDetails( VersionNo, &ext[1] );//typeid( T).name() );
				}
			}

			// Safe to delete the object on its first pass because we don't byteswap or rebase pointers.
			if ( !m_pass )
			{
				// okay, still not a very good idea because we forget the max allocation size.
				// delete ptr;
			}

			pgRscBuilder::EndBuild(ptr);

			if ( m_pass == pgRscBuilder::LAST_PASS)
			{
				m_pass = 0;
				m_assetCount++;

				if ( m_folders.size() )	// pop folder if any set
				{
					ASSET.PopFolder();
				}
			}
			else
			{
				m_pass = 1;
			}
		}
		// PURPOSE
		//		Resets the error count usually done before manually loading a resource
		//
		void ResetErrors();

		// PURPOSE
		//		Returns true if error has occurred since last reset.
		//		Command line params determine the strictness of the error checking
		//
		bool HasErrors() const; 

		// PURPOSE
		//		Returns true if file is of the given type
		//
		bool HasExtension( const char* ext ) const;

		
		const char*			GetInput() const { return m_inputList[m_assetCount]; }
		const char*			GetRscName() const { return m_rscName[m_assetCount]; }
		int					GetVirtualChunkSize() const		{ return m_vchunkSize; }
		int					GetPhysicalChunkSize() const	{ return m_pchunkSize; }
		int					GetHeapSize() const				{ return m_buildHeapSize; }
		
		void LogDetails( int versionNo , const char* type );
	private:
		void CleanUp();
		void GetFNames( char* m_inputListList, std::vector<char*>& v  );

		size_t							m_assetCount;
		std::vector<char *>				m_inputList;
		std::vector<char *>				m_rscName; 
		std::vector<char *>				m_folders; 
		atString						m_arg1;
		atString						m_arg2;

		sysTimer						m_elapsed;

		int								m_vchunkSize;
		int								m_pchunkSize;
		int								m_buildHeapSize;
		pgDictionary<grcTexture>* 		m_referenceTextureDictionary;

		int								m_currentErrorCount;

		int								m_pass;
	};

	// fwd reference
	class XmlFileLog;

	// PURPOSE
	//		Outs the version of a given resource type in a certain format
	//		It currently uses an xml format which works well with msbuild.
	//
	class VersionFile
	{
		XmlFileLog*		m_log;
	public:
		VersionFile( const char* name="versions");
		~VersionFile();	

		void Output( const char* name, int version );	
	};

};

// Simple macro which error-checks key steps of the process, even on Release builds.
#define ValidateResult(x)	if (!x) Quitf("%s failed, aborting.",#x); else;


#endif
