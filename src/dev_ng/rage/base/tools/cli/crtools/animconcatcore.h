// 
// crtools/animconcatcore.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef CRTOOLS_ANIMCONCATCORE_H
#define CRTOOLS_ANIMCONCATCORE_H

#include "atl/array.h"

namespace rage
{

class crAnimation;
class crSkeletonData;


namespace crAnimConcatCore
{

// PURPOSE: Animation section descriptor
struct AnimationSection
{	
	AnimationSection();

	const crAnimation* m_SrcAnim;
	float m_Start;
	float m_End;
	float m_Overlap;
};

// PURPOSE: Advanced animation concatenation
// NOTE: Sense of overlap and lead in is reversed in advanced version
// Negative overlaps (and lead in) produce gaps between animations (and at start)
// Positive overlaps cause blending while both animations play
extern bool ConcatAnimations(const atArray<AnimationSection>& animSections, crAnimation& destAnim, float sampleRate=1.f/30.f, bool loop=false, float leadIn=0.f, bool superset=false, bool absoluteMover=false, const crSkeletonData* skelData=NULL);

// PURPOSE: Simple animation concatenation
// NOTE: Sense of overlap and lead in/out is reversed compared to advanced version
// All overlaps, lead in/out are positive (but they all introduce gaps)
extern bool ConcatAnimations(const atArray<const crAnimation*>& srcAnims, crAnimation& destAnim, float overlap, float sampleRate=1.f/30.f, bool loop=false, float leadIn=0.f, float leadOut=0.f, bool superset=false, bool absoluteMover=false, bool crop=false, const crSkeletonData* skelData=NULL);

// PURPOSE: Very simple animation concatenation
// NOTE: Sense of overlap and lead in/out is reversed compared to advanced version
// All overlaps, lead in/out are positive (but they all introduce gaps)
extern bool ConcatAnimations(const crAnimation& srcAnim0, const crAnimation& srcAnim1, crAnimation& destAnim, float overlap=1.f/30.f, float sampleRate=1.f/30.f, bool loop=false, float leadIn=0.f, float leadOut=0.f, bool superset=false, bool absoluteMover=false, bool crop=false, const crSkeletonData* skelData=NULL);


}; // namespace crAnimConcatCore

}; // namespace rage

#endif // CRTOOLS_ANIMCONCATCORE_H
