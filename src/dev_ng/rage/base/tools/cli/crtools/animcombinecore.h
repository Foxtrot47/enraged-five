// 
// crtools/animcombinecore.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 


#ifndef CRTOOLS_ANIMCOMBINECORE_H
#define CRTOOLS_ANIMCOMBINECORE_H

#include "atl/array.h"

namespace rage
{

class crAnimation;

namespace crAnimCombine
{

// PURPOSE: Combine operation
enum eCombineOperation
{
	kCombineOperationNone,
	kCombineOperationAdd,
	kCombineOperationSubtract,
	kCombineOperationBlend,
	kCombineOperationMerge,
};

// PURPOSE: Combine alignment type
enum eCombineAlignment
{
	kCombineAlignmentNone,
	kCombineAlignmentVariableTime,
	kCombineAlignmentVariablePhase,
	kCombineAlignmentFixedTime,
	kCombineAlignmentFixedFrame,
	kCombineAlignmentFixedPhase
};

// PURPOSE: Combine animations
extern bool CombineAnimation(const atArray<const crAnimation*>& srcAnims, crAnimation& destAnim, eCombineOperation op, float weight, eCombineAlignment alignment, float alignmentValue, float sampleRate=1.f/30.f);


}; // namespace crAnimCombineCore

}; // namespace rage

#endif // CRTOOLS_ANIMCOMBINECORE_H


