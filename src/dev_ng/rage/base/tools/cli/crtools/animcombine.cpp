// 
// /animcombine.cpp 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#include "animcombinecore.h"
#include "animcompresscore.h"

#include "cranimation/animation.h"
#include "cranimation/animtolerance.h"
#include "cranimation/frame.h"
#include "file/asset.h"
#include "file/serialize.h"
#include "system/main.h"
#include "system/param.h"

#if !__TOOL
#error "AnimCombine needs to use the default memory allocator. Compile a tool build instead."
#endif

using namespace rage;

PARAM(help, "Get command line help");
PARAM(anims, "Source animations (Example: anim0.anim,anim1.anim)");
PARAM(out, "Output animation (Example: out.anim)");
PARAM(add, "Perform add operation (with optional weight value)");
PARAM(subtract, "Perform subtract operation (with optional weight value)");
PARAM(blend, "Perform blend operation (with optional weight value)");
PARAM(merge, "Perform merge operation (with optional weight value)");
//PARAM(blendn, "Perform blendN operation (with optional weight values)");
PARAM(sync, "Synchronize the source animation timings");
PARAM(align, "Align the source animation phases");
PARAM(time, "Fix the source animation times");
PARAM(frame, "Fix the source animation frames (30 Hz)");
PARAM(phase, "Fix the source animation phases");
PARAM(compression, "Compression tolerance");
PARAM(nocompression, "Skip compression");
PARAM(samplerate, "Rate to use when sampling (Example: 0.033333)");
PARAM(samplefreq, "Sample frequency (Example: 30.0)");
PARAM(rawonly, "Only accept raw input animation, generate error (not warning) on non-raw");


int Main()
{
	// initialization:
	ASSET.SetPath(RAGE_ASSET_ROOT);
	crAnimation::InitClass();

	// process help:
	if(PARAM_help.Get())
	{
		sysParam::Help("");
		return 0;
	}

	// process anim list:
	const int maxAnims = 64;
	const int maxAnimBufSize = 8192;

	int numAnims = 0;
	const char* animFilenames[maxAnims];
	char animBuf[maxAnimBufSize];

	numAnims = PARAM_anims.GetArray(animFilenames, maxAnims, animBuf, maxAnimBufSize);
	if(numAnims <= 0)
	{
		Quitf("ERROR - No source animation(s) specified\n");
	}
	else if(numAnims != 2)
	{
		Quitf("ERROR - Currently exactly 2 animations must be specified\n");
	}

	// process output filename:
	const char* outputFilename = NULL;
	PARAM_out.Get(outputFilename);
	if(!outputFilename)
	{
		Quitf("ERROR - No output filename specified\n");
	}

	// process operation:
	crAnimCombine::eCombineOperation op = crAnimCombine::kCombineOperationNone;
	int numOps = 0;
	float weight = 1.f;

	bool add = PARAM_add.Get();
	if(add)
	{
		PARAM_add.Get(weight);
		numOps++;
		op = crAnimCombine::kCombineOperationAdd;
	}

	bool subtract = PARAM_subtract.Get();
	if(subtract)
	{
		PARAM_subtract.Get(weight);
		numOps++;
		op = crAnimCombine::kCombineOperationSubtract;
	}

    bool blend = PARAM_blend.Get();
	if(blend)
	{
		weight = 0.5f;
		PARAM_blend.Get(weight);
		numOps++;
		op = crAnimCombine::kCombineOperationBlend;
	}

	bool merge = PARAM_merge.Get();
	if(merge)
	{
		PARAM_blend.Get(weight);
		numOps++;
		op = crAnimCombine::kCombineOperationMerge;
	}

	if(numOps > 1)
	{
		Quitf("ERROR - Found; %s%s%s.  Only one of these can be present\n", add?"add ":"", subtract?"subtract ":"", blend?"blend ":"");
	}
	else if(numOps == 0)
	{
		Quitf("ERROR - Failed to find add/subtract/blend.  One of these must be present\n");
	}

	// process alignment:
	crAnimCombine::eCombineAlignment alignment = crAnimCombine::kCombineAlignmentNone;
	int numAlignments = 0;
	float alignmentValue = 0.f;

	bool sync = PARAM_sync.Get();
	if(sync) 
	{
		numAlignments++;
		alignment = crAnimCombine::kCombineAlignmentVariableTime;
	}

	bool align = PARAM_align.Get();
	if(align)
	{
		numAlignments++;
		alignment = crAnimCombine::kCombineAlignmentVariablePhase;
	}

	bool time = PARAM_time.Get();
	if(time)
	{
		PARAM_time.Get(alignmentValue);
		numAlignments++;
		alignment = crAnimCombine::kCombineAlignmentFixedTime;
	}

	bool frame = PARAM_frame.Get();
	if(frame)
	{
		PARAM_frame.Get(alignmentValue);
		numAlignments++;
		alignment = crAnimCombine::kCombineAlignmentFixedFrame;
	}

	bool phase = PARAM_phase.Get();
	if(phase)
	{
		PARAM_phase.Get(alignmentValue);
		numAlignments++;
		alignment = crAnimCombine::kCombineAlignmentFixedPhase;
	}

	// process sample freq/rate:
	float sampleFreq = 30.f;
	PARAM_samplefreq.Get(sampleFreq);
	float sampleRate = 1.f/sampleFreq;
	PARAM_samplerate.Get(sampleRate);

	// load animations:
	atArray<const crAnimation*> srcAnims;
	for(int i=0; i<numAnims; ++i)
	{
		crAnimation* anim = crAnimation::AllocateAndLoad(animFilenames[i]);
		if(!anim)
		{
			Quitf("ERROR - Failed to load animation '%s'", animFilenames[i]);
		}
		else if(!anim->IsRaw())
		{
			if(PARAM_rawonly.Get())
			{
				Quitf("ERROR - Source animation '%s' is not marked as raw, and raw only is specified", animFilenames[i]);
			}
			else
			{
				Warningf("WARNING - Source animation '%s' is not marked as raw, may have already been compressed", animFilenames[i]);
			}
		}
		srcAnims.Grow() = anim;
	}

	crAnimation destAnim;
	crAnimCombine::CombineAnimation(srcAnims, destAnim, op, weight, alignment, alignmentValue, sampleRate);

    if ( PARAM_nocompression.Get() )
    {
        // save output animation:
        if ( !destAnim.Save( outputFilename ) )
        {
            Quitf( "ERROR - Failed to save output file '%s'", outputFilename );
        }
    }
    else
    {
        // process compression:
	    float compression = SMALL_FLOAT;
	    if(PARAM_compression.Get())
	    {
		    compression = -1.f;
		    PARAM_compression.Get(compression);
	    }

	    // compress output animation:
	    crAnimToleranceSimple tolerance;
	    if(compression >= 0.f)
	    {
		    crAnimCompress::ProcessSimpleCompressionSettings(tolerance, compression, compression, compression);
	    }

	    crAnimation outAnim;
	    crAnimCompress::CompressAnimation(destAnim, outAnim, tolerance);

	    // save output animation:
	    if(!outAnim.Save(outputFilename))
	    {
		    Quitf("ERROR - Failed to save output file '%s'", outputFilename);
	    }
    }

	// shutdown:
	for(int i=0; i<srcAnims.GetCount(); ++i)
	{
		if(srcAnims[i])
		{
			delete srcAnims[i];
		}
	}
	srcAnims.Reset();

	crAnimation::ShutdownClass();

	return 0;
}

 

