using namespace rage;

#include "cranimation/animation.h"
#include "crskeleton/jointdata.h"
#include "crskeleton/skeletondata.h"
#include "file/asset.h"
#include "parser/manager.h"
#include "system/main.h"
#include "system/param.h"

PARAM(help, "Get command line help");
PARAM(skel, "Skeleton file (Example: skeleton.skel)");
PARAM(out, "Destination jlimits (Example: out.jlimits)");
PARAM(convert, "Attempt to convert .skel limits to new format");

int Main()
{
	INIT_PARSER;

	// initialization
	ASSET.SetPath(RAGE_ASSET_ROOT);
	crAnimation::InitClass();

	// process help
	if (PARAM_help.Get())
	{
		sysParam::Help("");
		return 0;
	}

	const char* skelFile = NULL;
	PARAM_skel.Get(skelFile);
	if(!skelFile)
	{
		Quitf("ERROR - No skeleton file specified");
	}

	// process output filename:
	const char* outFile;
	PARAM_out.Get(outFile);
	if(!outFile)
	{
		Quitf("ERROR - No output file specified");
	}

	// process convert
	crJointData::eSkelDataInit convert = crJointData::kInitAsEulers;
	if (PARAM_convert.Get())
	{
		convert = crJointData::kInitFromEulers;
	} 

	crJointData jointData;
	jointData.InitFromSkeletonData(skelFile, convert);

	jointData.Save(outFile);

	SHUTDOWN_PARSER;

	return 0;
}