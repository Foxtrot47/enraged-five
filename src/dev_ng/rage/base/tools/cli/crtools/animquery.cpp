// 
// animquery.cpp 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 

#include "cranimation/animation.h"
#include "system/main.h"
#include "system/param.h"

#if !__TOOL
#error "AnimQuery needs to use the default memory allocator. Compile a tool build instead."
#endif

using namespace rage;

PARAM(help, "Get command line help");
PARAM(anim, "Source animation(s) (Example: anim0.anim,anim1.anim ...)");
PARAM(track, "Track to query if it exists");
PARAM(id, "Id to query if it exists");

int Main()
{
	// initialization:
	crAnimation::InitClass();

	// process help:
	if(PARAM_help.Get())
	{
		sysParam::Help("");
		return 0;
	}

	// load animation
	const char* animFilename;
	if(!PARAM_anim.Get(animFilename))
	{
		Quitf("ERROR - No source animation specified\n");
	}

	u8 version;
	crAnimation anim;
	if(!anim.Load(animFilename, &version, false, false, false))
	{
		Quitf("ERROR - failed to load animation '%s'\n", animFilename);
	}

	const char* trackName;
	if(!PARAM_track.Get(trackName))
	{
		Quitf("ERROR - No track specified\n");
	}

	const char* idName;
	if(!PARAM_id.Get(idName))
	{
		Quitf("ERROR - No id specified\n");
	}

	u8 track = (u8)atoi(trackName);
	u16 id = (u16)atoi(idName);
	bool exist = anim.HasTrack(track, id);
	Printf("Track %u with id %u %s\n", track, id, exist ? "exist" : "doesn't exist");

	// shutdown:
	crAnimation::ShutdownClass();

	return exist;
}
