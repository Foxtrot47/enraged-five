// 
// animcompare.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "cranimation/animation.h"
#include "cranimation/frame.h"
#include "cranimation/frameinitializers.h"
#include "crskeleton/skeleton.h"
#include "crskeleton/skeletondata.h"
#include "file/asset.h"
#include "file/serialize.h"
#include "system/main.h"
#include "system/param.h"
#include "vectormath/classes.h"

using namespace rage;

PARAM(help, "Get command line help");
PARAM(skel, "Skeleton (Example: char.skel)");
PARAM(anim, "Source animations (Example: anim0.anim[;start|;end|;time=x|;frame=x|;phase=x|;search;sync],anim1.anim ... )");
PARAM(csv, "Produce output in comma seperated format");
PARAM(best, "Search for the best fit");
PARAM(worst, "Search for the worst fit");
PARAM(samplerate, "Rate to use when searching for best/worst fit (Example: 0.033333)");
PARAM(samplefreq, "Sample frequency (Example: 30.0)");
PARAM(noroot, "Don't compare root bones");
PARAM(threshold, "Threshold to report failure at");
PARAM(bone, "Perform comparison on a single bone (Example: foot_left)");
PARAM(output, "Display comparison values as they are calculated");

crAnimation* ParseAnim(const char* animString, float& time)
{
	// parse the filename:
	char animFilename[1024];

	const char* ch = animString;
	char* chFilename = animFilename;
	do
	{
		*(chFilename++) = *(ch++);
	}
	while(*ch && *ch != ';');
	*chFilename = '\0';

	// parse keyword (if there is one):
	char animKeyword[1024];
	animKeyword[0] = '\0';
	float animValue = 0.f;

	if(*ch == ';')
	{
		ch++;
		char* chKeyword = animKeyword;
		do 
		{
			*(chKeyword++) = *(ch++);
		}
		while(*ch && *ch != '=');
		*chKeyword = '\0';

		// parse value (if present):
		if(*ch == '=')
		{
			animValue = float(atof(++ch));			
		}
	}

	// load the animation:
	crAnimation* anim = crAnimation::AllocateAndLoad(animFilename);
	if(anim == NULL)
	{
		Quitf("ERROR - failed to load animation '%s'\n", animFilename);
	}

	// use keyword:
	time = 0.f;
	if(!stricmp(animKeyword, "start"))
	{
		time = 0.f;
	}
	else if(!stricmp(animKeyword, "end"))
	{
		time = anim->GetDuration();
	}
	else if(!stricmp(animKeyword, "search"))
	{
		time = -1.f;
	}
	else if(!stricmp(animKeyword, "sync"))
	{
		time = -2.f;
	}
	else if(!stricmp(animKeyword, "time"))
	{
		time = Clamp(animValue, 0.f, anim->GetDuration());
	}
	else if(!stricmp(animKeyword, "frame"))
	{
		time = Clamp(anim->Convert30FrameToTime(animValue), 0.f, anim->GetDuration());
	}
	else if(!stricmp(animKeyword, "phase"))
	{
		time = Clamp(anim->ConvertPhaseToTime(animValue), 0.f, anim->GetDuration());
	}
	else if(animKeyword[0])
	{
		Quitf("ERROR - unrecognized keyword '%s'", animKeyword);
	}

	return anim;
}

float ComparePoses(const crSkeleton& srcSkel, const crSkeleton& destSkel, int bone)
{
	if(bone < 0)
	{
		int numBones = srcSkel.GetBoneCount();

		float similarity2 = 0.f;
		for(int i=0; i<numBones; ++i)
		{
			Mat34V srcMtx, dstMtx;
			srcSkel.GetGlobalMtx(i, srcMtx);
			destSkel.GetGlobalMtx(i, dstMtx);
			Vec3V diff = srcMtx.GetCol3() - dstMtx.GetCol3();
			similarity2 += MagSquared(diff).Getf();
		}

		return (similarity2 > 0.f) ? (sqrt(similarity2) / float(numBones)) : 0.f;
	}
	else
	{
		Mat34V srcMtx, dstMtx;
		srcSkel.GetGlobalMtx(bone, srcMtx);
		destSkel.GetGlobalMtx(bone, dstMtx);

		Vec3V diff = srcMtx.GetCol3() - dstMtx.GetCol3();
		float similarity = Mag(diff).Getf();

		return similarity;
	}
}

float CompareAnims(const crAnimation& srcAnim, float& srcTime, const crAnimation& destAnim, float& destTime, const crSkeletonData& skelData, bool best, bool noRoot, float sampleRate, int bone, bool csv, bool output)
{
	crSkeleton* srcSkel = rage_new crSkeleton();
	srcSkel->Init(skelData, NULL);

	crSkeleton* destSkel = rage_new crSkeleton();
	destSkel->Init(skelData, NULL);

	crFrameData* frameData = rage_new crFrameData;
	crFrameDataInitializerBoneAndMover initializer(skelData, false);
	initializer.InitializeFrameData(*frameData);
	if(noRoot)
	{
		frameData->RemoveDof(kTrackBoneTranslation, 0);
		frameData->RemoveDof(kTrackBoneRotation, 0);
	}
	crFrame* frame = rage_new crFrame;
	frame->Init(*frameData);
	frameData->Release();

	float bestSimilarity = best?FLT_MAX:0.f;
	float bestSrcTime = 0.f;
	float bestDestTime = 0.f;

	if(output && csv)
	{
		Printf("SrcTime,DestTime,");
		if(bone >= 0)
		{
			Printf("BoneName,");
		}
		Printf("Similarity\n");
	}

	for(float src=Max(srcTime, 0.f); src <= srcAnim.GetDuration(); src += sampleRate)
	{
		for(float dest=Max(destTime, 0.f); dest <= destAnim.GetDuration(); dest += sampleRate)
		{
			if(srcTime < -1.f)
			{
				src = dest;
			}

			// pose src and dest skeletons:
			srcAnim.CompositeFrame(src, *frame);
			frame->Pose(*srcSkel);
			srcSkel->Update();

			destAnim.CompositeFrame(dest, *frame);
			frame->Pose(*destSkel);
			destSkel->Update();
		
			// compare poses, find best similarity:
			float similarity = ComparePoses(*srcSkel, *destSkel, bone);

			if((best && (similarity < bestSimilarity)) || (!best && (similarity > bestSimilarity)))
			{
				bestSimilarity = similarity;
				bestSrcTime = src;
				bestDestTime = dest;
			}

			if(output)
			{
				Printf(csv?"%f,%f,":"SrcTime %f DestTime %f ", src, dest);
				if(bone >= 0)
				{
					Printf(csv?"%s,":"BoneName %s ", skelData.GetBoneData(bone)->GetName());
				}
				Printf(csv?"%f\n":"Similarity %f\n", similarity);
			}

			if(destTime >= 0.f)
			{
				break;
			}
		}

		if(srcTime < -1.f)
		{
			break;
		}

		if(srcTime >= 0.f)
		{
			break;
		}
	}

	if(srcTime < 0.f)
	{
		srcTime = bestSrcTime;
	}
	if(destTime < 0.f)
	{
		destTime = bestDestTime;
	}
	
	delete srcSkel;
	delete destSkel;
	delete frame;

	return bestSimilarity;
}


int Main()
{
	// initialization:
	ASSET.SetPath(RAGE_ASSET_ROOT);
	crAnimation::InitClass();

	// process help:
	if(PARAM_help.Get())
	{
		sysParam::Help("");
		return 0;
	}

	// process skel:
	crSkeletonData* skelData = NULL;
	if(PARAM_skel.Get())
	{
		const char* skelFile;
		PARAM_skel.Get(skelFile);

		skelData = crSkeletonData::AllocateAndLoad(skelFile);
		if(!skelData)
		{
			Quitf("ERROR - failed to load skeleton file '%s'", skelFile);
		}
	}
	else
	{
		Quitf("ERROR - No skeleton file specified");
	}

	// process fit:
	bool best = true;
	if(PARAM_worst.Get())
	{
		best = false;
		if(PARAM_best.Get())
		{
			Quitf("ERROR - Can't specify best and worst fit together");
		}
	}

	// process sample freq/rate:
	float sampleFreq = 30.f;
	PARAM_samplefreq.Get(sampleFreq);
	float sampleRate = 1.f/sampleFreq;
	PARAM_samplerate.Get(sampleRate);

	// no root:
	bool noRoot = false;
	noRoot = PARAM_noroot.Get();

	// output:
	bool output = false;
	output = PARAM_output.Get();

	// bone:
	int bone = -1;
	const char* bonename;
	if(PARAM_bone.Get(bonename))
	{
		const crBoneData* bd = skelData->FindBoneData(bonename);
		if(!bd)
		{
			Quitf("ERROR - Failed to find bone '%s'", bonename);
		}
		bone = bd->GetIndex();
	}

	// threshold:
	float threshold = -1.f;
	if(PARAM_threshold.Get())
	{
		PARAM_threshold.Get(threshold);
	}

	// process anim list:
	const int maxAnims = 64;
	const int maxAnimBufSize = 8192;
	
	int numAnims = 0;
	const char* animFilenames[maxAnims];
	char animBuf[maxAnimBufSize];

	numAnims = PARAM_anim.GetArray(animFilenames, maxAnims, animBuf, maxAnimBufSize);
	if(numAnims <= 0)
	{
		Quitf("ERROR - No source animations specified\n");
	}
	else if(numAnims <= 1)
	{
		Quitf("ERROR - Insufficient number of source animations specified");
	}

	bool csv = false;
	if(PARAM_csv.Get())
	{
		csv = true;
	}

	// for src animation:
	float srcTime;
    crAnimation* srcAnim = ParseAnim(animFilenames[0], srcTime);

	// for each dest animation:
	int success = 0;
	for(int i=1; i<numAnims; i++)
	{
		float destTime;
		crAnimation* destAnim = ParseAnim(animFilenames[i], destTime);
		Assert(destTime >= -1.f && "TODO - sync only supported on source anim at this time, use search on dest");
			
		float destResult = destTime;
		float srcResult = srcTime;
		float similarity = CompareAnims(*srcAnim, srcResult, *destAnim, destResult, *skelData, best, noRoot, sampleRate, bone, csv, output);

		// print results:
		if(csv)
		{
			Printf("SourceFilename,SourceTime,DestFilename,DestTime,");
			if(bone >= 0)
			{
				Printf("BoneName,");
			}
			Printf("Similarity\n");
		}
		Printf(csv?"%s,":"SourceFilename:\t%s\n", srcAnim->GetName());
		Printf(csv?"%f,":"SourceTime:\t%f\n", srcResult);
		Printf(csv?"%s,":"DestFilename:\t%s\n", destAnim->GetName());
		Printf(csv?"%f,":"DestTime:\t%f\n", destResult);
		if(bone >= 0)
		{
			Printf(csv?"%s,":"BoneName:\t%s\n", skelData->GetBoneData(bone)->GetName());
		}
		Printf(csv?"%f":"Similarity:\t%f\n", similarity);

		Printf("\n");
		
		if(threshold >= 0.f && similarity > threshold)
		{
			success++;
		}

		delete destAnim;
	}

	// shutdown:
	delete srcAnim;
	delete skelData;

	crAnimation::ShutdownClass();

	return success;
}
