// 
// crtools/animoptimizecore.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 


#ifndef CRTOOLS_ANIMOPTIMIZECORE_H
#define CRTOOLS_ANIMOPTIMIZECORE_H

#include "atl/array.h"
#include "atl/string.h"
#include "cranimation/animtolerance.h"
#include "math/random.h"

namespace rage
{

class crSkeletonData;


////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Optimize animation compression settings
// Uses genetic alogithm to search for optimial compression settings
// Optinally distributes work over incredibuild
class crAnimOptimizer
{
protected:

	struct Solution;

public:

	// PURPOSE: Constructor
	crAnimOptimizer(u32 seed=0);

	// PURPOSE: Destructor
	~crAnimOptimizer();

	// PURPOSE: Initializer
	void Init(const crSkeletonData& skelData, u32 maxBlockSize, float compressionRotation, float compressionTranslation, float compressionDefault, crAnimTolerance::eCompressionCost compressionCost, crAnimTolerance::eDecompressionCost decompressionCost, const char* preCompressionRules, const char* postCompressionRules);

	// PURPOSE: Add a compression group
	void AddGroup(const atString& rules, float tolerance, int parentGroup);

	// PURPOSE: Get number of compression groups
	int GetNumGroups() const;

	// PURPOSE: Add a test
	void AddTest(int boneIdx, float limit);

	// PURPOSE: Generate new population via crossover and mutation
	void Generate(int population, float crossover, float mutation, float minimum, float maximum, float step);

	// PURPOSE: Measure fitness of population against set of animations
	bool Measure(const atArray<atString>& animFilenames);

	// PURPOSE: Distribute measurement over incredibuild
	bool Distribute(const atArray<atString>& animFilenames, const char* distrbuteCmdLine, float timeout);

	// PURPOSE: Select fittest
	void Select(float selection);

	// PURPOSE: Output results to file
	bool Results(const char* resultsFile);

	// PURPOSE: Dump results
	void Dump(int solution=-1) const;


protected:

	// PURPOSE: Internal function
	void PrintGroupSolution(const Solution& s, atString& inoutString) const;


	const crSkeletonData* m_SkelData;

	// PURPOSE: A potential solution; compression tolerances (in gene form), plus errors and size - if tested
	struct Solution
	{
		Solution()
			: m_Size(0)
		{
		}

		atArray<float> m_Tolerances;
		atArray<float> m_Errors;

		u32 m_Size;
	};

	atArray<Solution*> m_Solutions;

	// PURPOSE: A compression group
	struct Group
	{
		atArray<atString> m_Rules;
		int m_ParentGroup;
	};

	atArray<Group> m_Groups;

	// PURPOSE: A test (usually an anchor or other chain end bone)
	struct Test
	{
		int m_BoneIdx;
		float m_Limit;
	};

	atArray<Test> m_Tests;

	float m_SampleRate;

	u32 m_MaxBlockSize;

	float m_DefaultRotationTolerance;
	float m_DefaultTranslationTolerance;
	float m_DefaultScaleTolerance;
	float m_DefaultTolerance;

	crAnimTolerance::eCompressionCost m_CompressionCost;
	crAnimTolerance::eDecompressionCost m_DecompressionCost;

	const char* m_PreCompressionRules;
	const char* m_PostCompressionRules;

	mthRandom m_Random;
};

////////////////////////////////////////////////////////////////////////////////

inline int crAnimOptimizer::GetNumGroups() const
{
	return m_Groups.GetCount();
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage


#endif // CRTOOLS_ANIMOPTIMIZECORE_H
