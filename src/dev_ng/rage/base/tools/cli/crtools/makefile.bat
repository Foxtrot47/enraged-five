set ARCHIVE=crtools
set FILES=animcmdlinehelper animdumpcore animcombinecore animcompresscore animoptimizecore animconcatcore
set TESTERS=animdump animedit animcompare animcombine animconcat animcompress animoptimize makeweightset makeproperties makejlimits
set LIBS=%RAGE_CORE_LIBS% %RAGE_GFX_LIBS% 
set LIBS=%LIBS% distribute cranimation 
set XINCLUDE=%RAGE_DIR%\base\tools\test
set XPROJ=%RAGE_DIR%\base\src %RAGE_DIR%\base\tools\test\distribute
