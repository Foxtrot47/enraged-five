// 
// crtools/animoptimize.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 


#include "cranimation/animation.h"
#include "crskeleton/skeletondata.h"
#include "distribute/distributeincredibuild.h"
#include "file/asset.h"
#include "file/stream.h"
#include "file/token.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timer.h"

#include "animoptimizecore.h"

using namespace rage;

PARAM(skel, "Skeleton file");
PARAM(anim, "Animation file");
PARAM(anims, "Animation files");
PARAM(animlist, "Animation list");
PARAM(anchors, "Anchors");
PARAM(groups, "Compression groups");
PARAM(tests, "Tests");
PARAM(results, "Result file");
PARAM(seed, "Seed random number generator");
PARAM(generations, "Generations");
PARAM(population, "Population");
PARAM(distribute, "Distribute");
PARAM(timeout, "Timeout (in seconds)");
PARAM(mutation, "Mutation rate");
PARAM(crossovers, "Crossover rate");
PARAM(selection, "Selection percentage");
PARAM(executable, "Executable path/name");
PARAM(min, "Minimum compression tolerance");
PARAM(max, "Maximum compression tolerance");
PARAM(step, "Compression tolerance step size");
PARAM(maxblocksize, "Maximum block size (Example: 65536)");
PARAM(compressionrotation, "Simple compression tolerance on rotation tracks (Example: 0.0005)");
PARAM(compressiontranslation, "Simple compression tolerance on translation tracks (Example: 0.0005)");
PARAM(compressiondefault, "Simple compression tolerance on all other tracks (Example: 0.001)");
PARAM(compressioncost, "Simple compression cost (Example: 100)");
PARAM(decompressioncost, "Simple compression cost (Example: 40)");
PARAM(precompressionrules, "Complex compression rules applied pre-optimization (Example: rule0;rule1; ... etc,  where a rule is <trackname|#id|*>,<bonename[?*]|#id|*>[+],<max error>)");
PARAM(postcompressionrules, "Complex compression rules applied post-optimization (Example: rule0;rule1; ... etc,  where a rule is <trackname|#id|*>,<bonename[?*]|#id|*>[+],<max error>)");

int Main()
{
	// initialization
	crAnimation::InitClass();	

	// process parameters
	crSkeletonData* skelData = NULL;
	const char* skelFilename = NULL;
	if(PARAM_skel.Get(skelFilename))
	{
		skelData = crSkeletonData::AllocateAndLoad(skelFilename);
		if(!skelData)
		{
			Errorf("Failed to load .skel file '%s'", skelFilename);
		}
	}
	else
	{
		Errorf("No .skel file specified");
	}

	atArray<atString> animFilenames;

	if(PARAM_anim.Get())
	{
		const char* filename = NULL;
		PARAM_anim.Get(filename);
		if(filename)
		{
			animFilenames.Grow() = filename;
		}
		else
		{
			Errorf("No anim filename specified");
		}
	}
	else if(PARAM_anims.Get())
	{
		const int maxFiles = 256;
		const int maxFileBufSize = maxFiles * 64;

		const char* filenames[maxFiles];
		char fileBuf[maxFileBufSize];

		int numFiles = PARAM_anims.GetArray(filenames, maxFiles, fileBuf, maxFileBufSize);
		if(numFiles > 0)
		{
			for(int i=0; i<numFiles; ++i)
			{
				animFilenames.Grow() = filenames[i];
			}
		}
		else
		{
			Errorf("No anim filenames specified");
		}
	}
	else if(PARAM_animlist.Get())
	{
		const char* listFilename = NULL;
		PARAM_animlist.Get(listFilename);

		fiSafeStream f(ASSET.Open(listFilename, "animlist"));
		if(f)
		{
			fiTokenizer T(listFilename, f);	

			const int maxBufSize = RAGE_MAX_PATH;
			char buf[maxBufSize];
			while(T.GetLine(buf, maxBufSize) > 0)
			{
				animFilenames.Grow() = buf;
			}
		}
		else
		{
			Errorf("Failed to open .animlist file '%s'", listFilename);
		}
	}
	else
	{
		Errorf("No anim/anims/animlist specified");
	}
	
	int maxBlockSize = 65536;
	PARAM_maxblocksize.Get(maxBlockSize);

	float compressionRotation = crAnimToleranceSimple::sm_DefaultMaxAbsoluteRotationError;
	PARAM_compressionrotation.Get(compressionRotation);

	float compressionTranslation = crAnimToleranceSimple::sm_DefaultMaxAbsoluteTranslationError;
	PARAM_compressiontranslation.Get(compressionTranslation);

	float compressionDefault = crAnimToleranceSimple::sm_DefaultMaxAbsoluteDefaultError;
	PARAM_compressiondefault.Get(compressionDefault);

	int compressionCost = int(crAnimTolerance::kCompressionCostDefault);
	PARAM_compressioncost.Get(compressionCost);

	int decompressionCost = int(crAnimTolerance::kDecompressionCostDefault);
	PARAM_decompressioncost.Get(decompressionCost);

	const char* preCompressionRules = NULL;
	PARAM_precompressionrules.Get(preCompressionRules);

	const char* postCompressionRules = NULL;
	PARAM_postcompressionrules.Get(postCompressionRules);
	
	u32 seed = u32(sysTimer::GetTicks());
	PARAM_seed.Get(seed);

	crAnimOptimizer optimizer(seed);
	optimizer.Init(*skelData, maxBlockSize, compressionRotation, compressionTranslation, compressionDefault, crAnimTolerance::eCompressionCost(compressionCost), crAnimTolerance::eDecompressionCost(decompressionCost), preCompressionRules, postCompressionRules);

	const char* groups;
	if(PARAM_groups.Get(groups) && groups && groups[0])
	{
		int parents[64];
		int pidx = 0;
		parents[pidx] = -1;

		const char* c = groups;

		atString group;

		while(*c)
		{
			switch(*(c++))
			{
			case '(':
				if(!group.GetLength())
				{
					parents[pidx] = optimizer.GetNumGroups()-1;
					parents[++pidx] = optimizer.GetNumGroups()-1;
				}
				break;
			case ')':
				if(!group.GetLength())
				{
					pidx--;
				}
				break;
			case '\\':
				if(!group.GetLength())
				{
					parents[pidx] = (pidx>0)?parents[pidx-1]:-1;
				}
				break;
			case ';':
				if(!group.GetLength())
				{
					parents[pidx] = optimizer.GetNumGroups()-1;
				}
				break;
			case '=':
				if(group.GetLength() > 0)
				{
					atString number;
					while(*c)
					{
						if((*c >= '0' && *c <= '9') || *c == '.')
						{
							number += *(c++);
						}
						else
						{
							float tolerance = float(atof(number.c_str()));
							optimizer.AddGroup(group, tolerance, parents[pidx]);

							group.Clear();
							break;
						}
					}
				}
				break;
			default:
				group += *(c-1);
				break;
			}
		}
	}

	if(PARAM_tests.Get())
	{
		const int maxTests = 16;
		const int maxTestBufSize = maxTests * 64;

		const char* tests[maxTests];
		char testBuf[maxTestBufSize];

		int numTests = PARAM_tests.GetArray(tests, maxTests, testBuf, maxTestBufSize);
		if(numTests > 0)
		{
			for(int i=0; i<numTests; ++i)
			{
				atString test;

				const char* c = tests[i];
				while(*c)
				{
					switch(*(c++))
					{
					case '=':
						if(test.GetLength() > 0)
						{
							atString number;
							while(*c)
							{
								if((*c >= '0' && *c <= '9') || *c == '.')
								{
									number += *(c++);
								}
								else
								{
								}
							}

							float limit = float(atof(number.c_str()));

							const crBoneData* bd = skelData->FindBoneData(test);
							if(bd)
							{
								optimizer.AddTest(bd->GetIndex(), limit);
							}
							else
							{
							}

							test.Clear();
						}
						break;

					default:
						test += *(c-1);
						break;
					}
				}
			}
		}
	}

	u32 generations = 0;
	PARAM_generations.Get(generations);

	// if in master mode, gather other master properties
	if(generations > 0)
	{
		bool distribute = PARAM_distribute.Get();

		atString distributeCmdLine;
		if(distribute)
		{
			char buf[64];

			const char* executable = "animoptimize.exe";
			PARAM_executable.Get(executable);

			distributeCmdLine = executable;

			distributeCmdLine += " -skel ";
			distributeCmdLine += skelFilename;

			distributeCmdLine += " -maxblocksize ";
			sprintf(buf, "%d", maxBlockSize);
			distributeCmdLine += buf;

			distributeCmdLine += " -compressionrotation ";
			sprintf(buf, "%f", compressionRotation);
			distributeCmdLine += buf;

			distributeCmdLine += " -compressiontranslation ";
			sprintf(buf, "%f", compressionTranslation);
			distributeCmdLine += buf;

			distributeCmdLine += " -compressiondefault ";
			sprintf(buf, "%f", compressionDefault);
			distributeCmdLine += buf;

			distributeCmdLine += " -compressioncost ";
			sprintf(buf, "%d", compressionCost);
			distributeCmdLine += buf;
			
			distributeCmdLine += " -decompressioncost ";
			sprintf(buf, "%d", decompressionCost);
			distributeCmdLine += buf;

			if(preCompressionRules)
			{
				distributeCmdLine += " -precompressionrules ";
				distributeCmdLine += preCompressionRules;
			}
			if(postCompressionRules)
			{
				distributeCmdLine += " -postcompressionrules ";
				distributeCmdLine += postCompressionRules;
			}
		}

		float mutation = 0.1f;
		PARAM_mutation.Get(mutation);

		float crossovers = 0.25f;
		PARAM_crossovers.Get(crossovers);

		float selection = 0.25f;
		PARAM_selection.Get(selection);

		float minimum = crAnimToleranceSimple::sm_DefaultMaxAbsoluteDefaultError/10.f;
		PARAM_min.Get(minimum);

		float maximum = crAnimToleranceSimple::sm_DefaultMaxAbsoluteDefaultError*10.f;
		PARAM_max.Get(maximum);

		float step = crAnimToleranceSimple::sm_DefaultMaxAbsoluteDefaultError/10.f;
		PARAM_step.Get(step);

		int population = 40;
		PARAM_population.Get(population);

		float timeout = -1.f;
		PARAM_timeout.Get(timeout);

		for(u32 i=0; i<generations; ++i)
		{
			Displayf("generation %d", i);
			optimizer.Generate(population, crossovers, mutation, minimum, maximum, step);
			if(distribute)
			{
				optimizer.Distribute(animFilenames, distributeCmdLine.c_str(), timeout);
			}
			else
			{
				optimizer.Measure(animFilenames);
			}
			optimizer.Select(selection);

			optimizer.Dump();
		}

		optimizer.Dump(0);
	}
	else
	{
		optimizer.Measure(animFilenames);
	}

	// output results
	const char* resultsFilename = NULL;
	if(PARAM_results.Get(resultsFilename) && resultsFilename && resultsFilename[0])
	{
		optimizer.Results(resultsFilename);
	}

	delete skelData;

	// shutdown
	crAnimation::ShutdownClass();

	return 0;
}