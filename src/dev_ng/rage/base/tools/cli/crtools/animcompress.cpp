// 
// /animcompress.cpp 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#include "animcompresscore.h"

#include "animcmdlinehelper.h"

#include "cranimation/animation.h"
#include "cranimation/animtolerance.h"
#include "crskeleton/skeletondata.h"
#include "file/asset.h"
#include "file/serialize.h"
#include "system/main.h"
#include "system/param.h"

#if !__TOOL
#error "AnimCompress needs to use the default memory allocator. Compile a tool build instead."
#endif

using namespace rage;

PARAM(help, "Get command line help");
PARAM(anim, "Source animation(s) (Example: anim0.anim,anim1.anim ...)");
PARAM(animlist, "Source animation(s) provided by .animlist file (text file, one animation per line)");
PARAM(skel, "Skeleton file (Example: skeleton.skel)");
PARAM(out, "Destination animation(s) (Example: out0.anim,out1.anim ...)");
PARAM(outlist, "Destination animation(s) provided by .animlist file (text file, one animation per line)");
PARAM(maxblocksize, "Maximum block size (Example: 65536)");
PARAM(compressionrotation, "Simple compression tolerance on rotation tracks (Example: 0.0005)");
PARAM(compressiontranslation, "Simple compression tolerance on translation tracks (Example: 0.0005)");
PARAM(compressionscale, "Simple compression tolerance on scale tracks (Example: 0.0005)");
PARAM(compressiondefault, "Simple compression tolerance on all other tracks (Example: 0.001)");
PARAM(compressioncost, "Simple compression cost (Example: 100)");
PARAM(decompressioncost, "Simple compression cost (Example: 40)");
PARAM(compressionrules, "Complex compression rules (Example: rule0;rule1; ... etc,  where a rule is <trackname|#id|*>,<bonename[?*]|#id|*>[+],<max error>)");
PARAM(rawonly, "Only accept raw input animation, generate error (not warning) on non-raw");
PARAM(compressionmovement, "Enable movement based compression");
PARAM(compressionspeedconstriction, "Movement compression speed constriction factor (Example: 1)");
PARAM(compressionspeedrelaxation, "Movement compression speed relaxation factor (Example: 2)");
PARAM(compressionminspeed, "Movement compression min speed threshold (Example: 0.1)");
PARAM(compressionmaxspeed, "Movement compression max speed threshold (Example: 0.5)");
PARAM(compressionbonechains, "Movement compression bone chains (Example: Foot_L,FootR)");
PARAM(decompress, "Decompress animation to raw");

const int MAX_ANIMS = 256;
const int MAX_ANIM_BUF_SIZE = 65536;

int Main()
{
	// initialization:
	ASSET.SetPath(RAGE_ASSET_ROOT);
	crAnimation::InitClass();

	// process help:
	if(PARAM_help.Get())
	{
		sysParam::Help("");
		return 0;
	}

	// process anim list:
	const int maxAnims = MAX_ANIMS;
	const int maxAnimBufSize = MAX_ANIM_BUF_SIZE;

	int numAnims = 0;
	const char* animFilenames[maxAnims];
	char animBuf[maxAnimBufSize];

	const char* animListFile = NULL;
	if(PARAM_animlist.Get(animListFile))
	{
		numAnims = crAnimCmdLineHelper::ProcessListFile(animListFile, animFilenames, maxAnims, animBuf, maxAnimBufSize);
	}
	else
	{
		numAnims = PARAM_anim.GetArray(animFilenames, maxAnims, animBuf, maxAnimBufSize);
	}
	if(numAnims <= 0)
	{
		Quitf("No source animation(s) specified\n");
	}

	// process out list:
	int numOut = 0;
	const char* outFilenames[maxAnims];
	char outBuf[maxAnimBufSize];

	const char* outListFile = NULL;
	if(PARAM_outlist.Get(outListFile))
	{
		numOut = crAnimCmdLineHelper::ProcessListFile(outListFile, outFilenames, maxAnims, outBuf, maxAnimBufSize);
	}
	else
	{
		numOut = PARAM_out.GetArray(outFilenames, maxAnims, outBuf, maxAnimBufSize);
	}
	if(numOut <= 0)
	{
		Quitf("No destination animation(s) specified\n");
	}
	if(numOut != numAnims)
	{
		Quitf("Number of source animation(s) doesn't match number of destination animation(s)\n");
	}

	// process skeleton:
	crSkeletonData* skelData = NULL;
	const char* skelFilename = NULL;
	PARAM_skel.Get(skelFilename);
	if(skelFilename)
	{
		skelData = crSkeletonData::AllocateAndLoad(skelFilename);
		if(!skelData)
		{
			Quitf("ERROR - Failed to load skeleton file '%s'", skelFilename);
		}
	}

	// process tolerance:
	crAnimToleranceMovement tolerance(1.f, 1.f);

	float maxRotationError = crAnimToleranceSimple::sm_DefaultMaxAbsoluteRotationError;
	float maxTranslationError = crAnimToleranceSimple::sm_DefaultMaxAbsoluteTranslationError;
	float maxScaleError = crAnimToleranceSimple::sm_DefaultMaxAbsoluteScaleError;
	float maxDefaultError = crAnimToleranceSimple::sm_DefaultMaxAbsoluteDefaultError;
	u32 compressionCost = crAnimTolerance::kCompressionCostDefault;
	u32 decompressionCost = crAnimTolerance::kDecompressionCostDefault;

	PARAM_compressionrotation.Get(maxRotationError);
	PARAM_compressiontranslation.Get(maxTranslationError);
	PARAM_compressionscale.Get(maxScaleError);
	PARAM_compressiondefault.Get(maxDefaultError);
	PARAM_compressioncost.Get(compressionCost);
	PARAM_decompressioncost.Get(decompressionCost);

	crAnimCompress::ProcessSimpleCompressionSettings(tolerance, maxRotationError, maxTranslationError, maxScaleError, maxDefaultError, (crAnimTolerance::eDecompressionCost)decompressionCost, (crAnimTolerance::eCompressionCost)compressionCost);

	if(PARAM_compressionrules.Get())
	{
		const char* compressionRules = NULL;
		PARAM_compressionrules.Get(compressionRules);

		if(!crAnimCompress::ProcessComplexCompressionRules(tolerance, compressionRules, skelFilename?skelData:NULL))
		{
			return -1;
		}
	}

	// process max block size:
	u32 maxBlockSize = 65536;
	PARAM_maxblocksize.Get(maxBlockSize);

	// process movement compression:
	if(PARAM_compressionmovement.Get())
	{
		if(skelFilename)
		{
			float speedConstriction = crAnimToleranceMovement::sm_DefaultSpeedConstriction;
			PARAM_compressionspeedconstriction.Get(speedConstriction);

			float speedRelaxation = crAnimToleranceMovement::sm_DefaultSpeedRelaxation;
			PARAM_compressionspeedrelaxation.Get(speedRelaxation);

			float minSpeed = crAnimToleranceMovement::sm_DefaultMinSpeed;
			PARAM_compressionminspeed.Get(minSpeed);

			float maxSpeed = crAnimToleranceMovement::sm_DefaultMaxSpeed;
			PARAM_compressionmaxspeed.Get(maxSpeed);

			crAnimCompress::ProcessMovementCompressionSettings(tolerance, speedConstriction, speedRelaxation, minSpeed, maxSpeed);

			const char* boneChains = NULL;
			if(PARAM_compressionbonechains.Get(boneChains))
			{
				crAnimCompress::ProcessMovementBoneChains(tolerance, boneChains, *skelData);
			}

//			crAnimCompress::CalcMovementCompression(tolerance, srcAnim, *skelData);
		}
		else
		{
			Warningf("WARNING - Movement compression not possible without a valid skeleton file");
		}
	}

	// process animation:
	for(int i=0; i<numAnims; ++i)
	{
		const char* srcAnimFilename = animFilenames[i];
		const char* outAnimFilename = outFilenames[i];

		crAnimation srcAnim;
		if(!srcAnimFilename || !srcAnim.Load(srcAnimFilename))
		{
			Quitf("ERROR - Failed to load animation '%s'", srcAnimFilename);
		}

		if(!srcAnim.IsRaw())
		{
			if(PARAM_rawonly.Get())
			{
				Quitf("ERROR - Source animation '%s' is not marked as raw, and raw only is specified", srcAnimFilename);
			}
			else
			{
				Warningf("WARNING - Source animation '%s' is not marked as raw, may have already been compressed", srcAnimFilename);
			}
		}

		crAnimation outAnim;
		if(PARAM_decompress.Get())
		{
			if(!crAnimCompress::DecompressAnimation(srcAnim, outAnim))
			{
				Errorf("ERROR - Failed to decompress animation '%s'", srcAnimFilename);
				return -1;
			}
		}
		else
		{
			if(!crAnimCompress::CompressAnimation(srcAnim, outAnim, tolerance, maxBlockSize))
			{
				Errorf("ERROR - Failed to compress animation '%s'", srcAnimFilename);
				return -1;
			}
		}

		// save new animation file:
		if(!outAnim.Save(outAnimFilename))
		{
			Errorf("ERROR - Failed to save animation '%s'", outAnimFilename);
			return -1;
		}
	}

	delete skelData;

	// shutdown:
	crAnimation::ShutdownClass();

	return 0;
}

