// 
// animedit.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "file/asset.h"
#include "file/serialize.h"
#include "system/main.h"
#include "system/param.h"
#include "RsULog/ULogger.h"
#include <Rpc.h>

#include "animcmdlinehelper.h"
#include "animeditcore.h"

using namespace rage;

rage::atString GetTempLogFileName( const char* pPrefix );

PARAM(help, "Get command line help");
PARAM(anim, "Source animation(s) (Example: anim0.anim,anim1.anim ...)");
PARAM(animlist, "Source animation(s) provided by .animlist file (text file, one animation per line)");
PARAM(out, "Destination animation(s) (Example: out0.anim,out1.anim ...)");
PARAM(outlist, "Destination animation(s) provided by .animlist file (text file, one animation per line)");
PARAM(op, "Operation(s) (Example: op0;op1;op2;");
PARAM(skel, "Optional skeleton file, required for some operations (Example: skeleton.skel)");
PARAM(rawonly, "Only accept raw input animation, generate error (not warning) on non-raw");
PARAM(ulog, "Universal logging." );

const int MAX_OP_LEN = 64;
const int MAX_ANIMS = 256;
const int MAX_ANIM_BUF_SIZE = 65536;

void Init()
{
	ASSET.SetPath(RAGE_ASSET_ROOT);
	crAnimation::InitClass();

	if(PARAM_ulog.Get())
	{
		ULOGGER.PreExport(GetTempLogFileName("animedit").c_str(), "animedit");
	}
}

void Shutdown()
{
	crAnimation::ShutdownClass();
	ULOGGER.PostExport();
}

int Main()
{
	// initialization:
	Init();

	// process skeleton file:
	const char* skelFilename = NULL;
	const crSkeletonData* skelData = NULL;
	if(PARAM_skel.Get(skelFilename))
	{
		int iVersion=0;
		skelData = crSkeletonData::AllocateAndLoad(skelFilename, &iVersion);
		if(!skelData)
		{
			ULOGGER.SetProgressMessage("ERROR: Failed to load skeleton file '%s'", skelFilename);
			Shutdown();
			Quitf("Failed to load skeleton file '%s'", skelFilename);
		}
	}

	// process anim list:
	const int maxAnims = MAX_ANIMS;
	const int maxAnimBufSize = MAX_ANIM_BUF_SIZE;

	int numAnims = 0;
	const char* animFilenames[maxAnims];
	char animBuf[maxAnimBufSize];

	const char* animListFile = NULL;
	if(PARAM_animlist.Get(animListFile))
	{
		numAnims = crAnimCmdLineHelper::ProcessListFile(animListFile, animFilenames, maxAnims, animBuf, maxAnimBufSize);
	}
	else
	{
		numAnims = PARAM_anim.GetArray(animFilenames, maxAnims, animBuf, maxAnimBufSize);
	}
	if(numAnims <= 0)
	{
		ULOGGER.SetProgressMessage("ERROR: No source animation(s) specified\n");
		Shutdown();
		Quitf("No source animation(s) specified\n");
	}

	// process out list:
	int numOut = 0;
	const char* outFilenames[maxAnims];
	char outBuf[maxAnimBufSize];

	const char* outListFile = NULL;
	if(PARAM_outlist.Get(outListFile))
	{
		numOut = crAnimCmdLineHelper::ProcessListFile(outListFile, outFilenames, maxAnims, outBuf, maxAnimBufSize);
	}
	else
	{
		numOut = PARAM_out.GetArray(outFilenames, maxAnims, outBuf, maxAnimBufSize);
	}
	if(numOut <= 0)
	{
		ULOGGER.SetProgressMessage("ERROR: No destination animation(s) specified\n");
		Shutdown();
		Quitf("No destination animation(s) specified\n");
	}
	if(numOut != numAnims)
	{
		ULOGGER.SetProgressMessage("ERROR: Number of source animation(s) doesn't match number of destination animation(s)\n");
		Shutdown();
		Quitf("Number of source animation(s) doesn't match number of destination animation(s)\n");
	}

	// process operation list:
	const char* opText = NULL;
	PARAM_op.Get(opText);

	crAnimEdit::OpData* ops = NULL;
	if(opText)
	{
		ops = crAnimEdit::ParseOps(opText, skelData);
		if(!ops)
		{
			ULOGGER.SetProgressMessage("ERROR: Failed to parse op list");
			Shutdown();
			Quitf("Failed to parse op list");
		}
	}

	// for each animation:
	for(int i=0; i<numAnims; i++)
	{
		crAnimation* anim = crAnimation::AllocateAndLoad(animFilenames[i]);
		if(!anim)
		{
			ULOGGER.SetProgressMessage("ERROR: failed to load animation '%s'\n", animFilenames[i]);
			Errorf("failed to load animation '%s'\n", animFilenames[i]);
			continue;
		}
		else if(!anim->IsRaw())
		{
			if(PARAM_rawonly.Get())
			{
				ULOGGER.SetProgressMessage("ERROR: Source animation '%s' is not marked as raw, and raw only is specified", animFilenames[i]);
				Shutdown();
				Quitf("ERROR - Source animation '%s' is not marked as raw, and raw only is specified", animFilenames[i]);
			}
			else
			{
				ULOGGER.SetProgressMessage("WARNING: Source animation '%s' is not marked as raw, may have already been compressed", animFilenames[i]);
				Warningf("WARNING - Source animation '%s' is not marked as raw, may have already been compressed", animFilenames[i]);
			}
		}

		// perform operations here:
		if(!crAnimEdit::ExecuteOps(ops, anim, skelData))
		{
			ULOGGER.SetProgressMessage("ERROR: failed to execute ops");
			Errorf("failed to execute ops");
			continue;
		}

		// save animation file:
		if(!anim->Save(outFilenames[i]))
		{
			ULOGGER.SetProgressMessage("ERROR: failed to save animation '%s'\n", outFilenames[i]);
			Errorf("failed to save animation '%s'\n", outFilenames[i]);
			continue;
		}

		delete anim;
	}

	// shutdown:
	delete ops;

	if(skelData)
	{
		delete skelData;
	}

	//Shutdown();

	return 0;
}

// Used by tools, creates a unique filename so we can xge animedit and have unique
// logs.
rage::atString GetTempLogFileName( const char* pPrefix )
{
	UUID uuid; 
	char* uuidAsString; 
	::UuidCreate(&uuid); 
	::UuidToString(&uuid, reinterpret_cast<RPC_CSTR*>(&uuidAsString)); 

	char cToolsRoot[RAGE_MAX_PATH];
	GetEnvironmentVariable("RS_TOOLSROOT",cToolsRoot,RAGE_MAX_PATH );

	char cLogPath[RAGE_MAX_PATH];
	sprintf(cLogPath, "%s\\logs\\cutscene\\process\\%s_%s.ulog", cToolsRoot, pPrefix, uuidAsString);
	
	::RpcStringFree(reinterpret_cast<RPC_CSTR*>(&uuidAsString));

	Displayf(cLogPath);

	return atString(cLogPath);
}