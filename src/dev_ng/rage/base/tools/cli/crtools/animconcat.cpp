// 
// /animconcat.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "animcompresscore.h"
#include "animconcatcore.h"

#include "cranimation/animation.h"
#include "cranimation/animtolerance.h"
#include "cranimation/frame.h"
#include "crskeleton/skeletondata.h"
#include "file/asset.h"
#include "file/serialize.h"
#include "math/simplemath.h"
#include "system/main.h"
#include "system/param.h"

#include "RsULog/ULogger.h"

using namespace rage;

PARAM(help, "Get command line help");
PARAM(anims, "Source animations to be concatenated (Example: anim0.anim,anim1.anim)");
PARAM(out, "Output concatenated animation (Example: out.anim)");
PARAM(nocompression, "Lossless compression only");
PARAM(compression, "Compression tolerance");
PARAM(samplerate, "Rate to use when sampling (Example: 0.033333)");
PARAM(samplefreq, "Sample frequency (Example: 30.0)");
PARAM(overlap, "Overlapping blend duration in seconds (Example: 0.1)");
PARAM(leadin, "Lead in duration in seconds (Example: 0.1)");
PARAM(leadout, "Lead out duration in seconds (Example: 0.1)");
PARAM(superset, "Output animation contains superset of tracks from all input animations");
PARAM(absolutemover, "Treat mover as absolute, don't convert to delta to join animations seamlessly");
PARAM(crop, "Crop previous animation instead of interpolating when overlapping");
PARAM(skel, "Optional .skel file, used to provide identity to absent tracks (Example: skeleton.skel)");
PARAM(rawonly, "Only accept raw input animation, generate error (not warning) on non-raw");
PARAM(ulog, "Universal logging." );

rage::atString GetTempLogFileName( const char* pPrefix );

void Init()
{
	ASSET.SetPath(RAGE_ASSET_ROOT);
	crAnimation::InitClass();

	if(PARAM_ulog.Get())
	{
		ULOGGER.PreExport(GetTempLogFileName("acn").c_str(), "animconcat");
	}
}

void Shutdown()
{
	crAnimation::ShutdownClass();
	ULOGGER.PostExport();
}

int Main(void)
{
	// initialization:
	Init();

	// process anim list:
	const int maxAnims = 64;
	const int maxAnimBufSize = 8192;

	int numAnims = 0;
	const char* animFilenames[maxAnims];
	char animBuf[maxAnimBufSize];

	numAnims = PARAM_anims.GetArray(animFilenames, maxAnims, animBuf, maxAnimBufSize);
	if(numAnims <= 0)
	{
		ULOGGER.SetProgressMessage("ERROR: No source animation(s) specified");
		Shutdown();
		Quitf("ERROR - No source animation(s) specified\n");
	}
	
	// process output filename:
	const char* outputFilename = NULL;
	PARAM_out.Get(outputFilename);
	if(!outputFilename)
	{
		ULOGGER.SetProgressMessage("ERROR: No output filename specified");
		Shutdown();
		Quitf("ERROR - No output filename specified\n");
	}

	// process sample freq/rate:
	float sampleFreq = 30.f;
	PARAM_samplefreq.Get(sampleFreq);
	float sampleRate = 1.f/sampleFreq;
	if(PARAM_samplerate.Get(sampleRate))
	{
		sampleFreq = 1.f/sampleRate;
	}

	// process compression:
	float compression = SMALL_FLOAT;
	if(PARAM_compression.Get())
	{
		compression = -1.f;
		PARAM_compression.Get(compression);
	}

	// process no compression:
	if(PARAM_nocompression.Get())
	{
		compression = 0.f;
	}

	// process overlap:
	float overlap = sampleRate;
	PARAM_overlap.Get(overlap);

	// process lead in:
	float leadIn = 0.f;
	PARAM_leadin.Get(leadIn);

	// process lead out:
	float leadOut = 0.f;
	PARAM_leadout.Get(leadOut);

	// process superset:
	bool superset = PARAM_superset.Get();
	
	// process absolute mover:
	bool absoluteMover = PARAM_absolutemover.Get();

	// process crop:
	bool crop = PARAM_crop.Get();

	// process skeleton:
	crSkeletonData skelData;
	const crSkeletonData* skelDataPtr = NULL;
	const char* skelFilename = NULL;
	PARAM_skel.Get(skelFilename);
	if(skelFilename && skelFilename[0])
	{
		int iVersion=0;
		if(skelData.Load(skelFilename, &iVersion))
		{
			skelDataPtr = &skelData;
		}
		else
		{
			ULOGGER.SetProgressMessage("ERROR: Failed to load skeleton file '%s'", skelFilename);
			Shutdown();
			Quitf("ERROR - Failed to load skeleton file '%s'", skelFilename);
		}
	}

	// load animations:
	atArray<const crAnimation*> srcAnims;
	for(int i=0; i<numAnims; ++i)
	{
		crAnimation* anim = crAnimation::AllocateAndLoad(animFilenames[i]);
		if(!anim)
		{
			ULOGGER.SetProgressMessage("ERROR: Failed to load animation '%s'", animFilenames[i]);
			Shutdown();
			Quitf("ERROR - Failed to load animation '%s'", animFilenames[i]);
		}
		else if(!anim->IsRaw())
		{
			if(PARAM_rawonly.Get())
			{
				ULOGGER.SetProgressMessage("ERROR: Source animation '%s' is not marked as raw, and raw only is specified", animFilenames[i]);
				Shutdown();
				Quitf("ERROR - Source animation '%s' is not marked as raw, and raw only is specified", animFilenames[i]);
			}
			else
			{
				ULOGGER.SetProgressMessage("WARNING: Source animation '%s' is not marked as raw, may have already been compressed", animFilenames[i]);
				Warningf("WARNING - Source animation '%s' is not marked as raw, may have already been compressed", animFilenames[i]);
			}
		}
		srcAnims.Grow() = anim;
	}

	// concatenate animations
	crAnimation destAnim;
	if(!crAnimConcatCore::ConcatAnimations(srcAnims, destAnim, overlap, sampleRate, false, leadIn, leadOut, superset, absoluteMover, crop, skelDataPtr))
	{
		ULOGGER.SetProgressMessage("ERROR: Failed to concatenate animations");
		Shutdown();
		Quitf("ERROR - Failed to concatenate animations");
		
	}

	// compress output animation:
	crAnimToleranceSimple tolerance;
	if(compression >= 0.f)
	{
		crAnimCompress::ProcessSimpleCompressionSettings(tolerance, compression, compression, compression);
	}

	crAnimation outAnim;
	crAnimation* outAnimPtr = &outAnim;
	if(InRange(compression, 0.f, SMALL_FLOAT))
	{
		outAnimPtr = &destAnim;
	}
	else
	{
		crAnimCompress::CompressAnimation(destAnim, outAnim, tolerance);
	}

	// save output animation:
	if(!outAnimPtr->Save(outputFilename))
	{
		ULOGGER.SetProgressMessage("ERROR: Failed to save output file '%s'", outputFilename);
		Shutdown();
		Quitf("ERROR - Failed to save output file '%s'", outputFilename);
		
	}

	// shutdown:
	for(int i=0; i<srcAnims.GetCount(); ++i)
	{
		if(srcAnims[i])
		{
			delete srcAnims[i];
		}
	}
	srcAnims.Reset();

	Shutdown();

	return 0;
}

// Used by tools, creates a unique filename so we can xge animedit and have unique
// logs.
rage::atString GetTempLogFileName( const char* pPrefix )
{
	char cToolsRoot[RAGE_MAX_PATH];
	GetEnvironmentVariable("RS_TOOLSROOT",cToolsRoot,RAGE_MAX_PATH );

	char cLogPath[RAGE_MAX_PATH];
	sprintf(cLogPath, "%s\\logs\\cutscene\\process\\", cToolsRoot);

	char cTempFilename[RAGE_MAX_PATH];
	GetTempFileName(cLogPath, pPrefix, 0, cTempFilename);
	DeleteFileA(cTempFilename);

	return atString(cTempFilename);
}
