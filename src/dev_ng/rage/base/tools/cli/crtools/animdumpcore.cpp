// 
// crtools/animdumpcore.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 


#include "animdumpcore.h"

#include "animcmdlinehelper.h"

#include "cranimation/animation.h"
#include "cranimation/animtrack.h"
#include "cranimation/animstream.h"
#include "cranimation/framedata.h"
#include "crskeleton/skeletondata.h"

namespace rage
{

namespace crAnimDump
{

////////////////////////////////////////////////////////////////////////////////

bool DumpAnimation(const crAnimation& anim, u8 version, bool csv, bool tracks, bool blocks, bool chunks, bool channels, bool values, bool eulers, bool memory, const crSkeletonData* skelData)
{
	if(csv)
	{
		Printf("Filename,Version,Duration,Fps,InternalFrames,FramesPerChunk,MaxBlockSize,PackedMemUse,CompactMemUse,Looped,HasMover,Raw,NumTracks,NumBlocks\n");
	}

	// print basic info:
	Printf(csv?"%s,":"Filename:\t%s\n", anim.GetName());
	Printf(csv?"%d,":"Version:\t%d\n", version);
	Printf(csv?"%f,":"Duration:\t%f\n", anim.GetDuration());
	Printf(csv?"%f,":"Fps:\t\t%f\n", float(Max(1,int(anim.GetNumInternalFrames())-1))/anim.GetDuration());
	Printf(csv?"%d,":"InternalFrames:\t%d\n", anim.GetNumInternalFrames());
	Printf(csv?"%d,":"FramesPerChunk:\t%d\n", anim.GetNumInternalFramesPerChunk());
	Printf(csv?"%d,":"MaxBlockSize:\t%d\n", anim.GetMaxBlockSize());
	Printf(csv?"%d,":"MemUse:\t%d\n", anim.ComputeSize());
	Printf(csv?"%d,":"IsLooped:\t%d\n", anim.IsLooped());
	Printf(csv?"%d,":"HasMover:\t%d\n", anim.HasMoverTracks());
	Printf(csv?"%d,":"Raw:\t\t%d\n", anim.IsRaw());
	Printf(csv?"%d,":"NumTracks:\t%d\n", anim.GetNumTracks());
	Printf(csv?"%d":"NumBlocks:\t%d\n", anim.GetNumBlocks());

	// print more verbose info:
	if(tracks || values)
	{
		Printf("\n");
		if(csv)
		{
			Printf("\nTrackIdx,Track,TrackId,TrackType,TrackName");
			Printf(memory?",TrackMemUse":"");
			if(values)
			{
				Printf(",TrackValues");
			}
			Printf("\n");
		}

		for(u32 i=0; i<anim.GetNumTracks(); i++)
		{
			crAnimTrack* track = anim.GetTrack(i);

			char trackName[255];
			sprintf(trackName, "%s", crAnimTrack::ConvertTrackIndexToName(track->GetTrack()));
			if(skelData)
			{
				switch(track->GetTrack())
				{
				case kTrackBoneTranslation:
				case kTrackBoneRotation:
				case kTrackBoneScale:
					{
						int boneIdx = -1;
						skelData->ConvertBoneIdToIndex(track->GetId(), boneIdx);
						sprintf(&trackName[strlen(trackName)], ":%s", (boneIdx>=0)?skelData->GetBoneData(boneIdx)->GetName():"unknown");
					}
					break;
				}
			}
			Printf(csv?"%d,%d,%d,%d,%s":"Track[%d]:\ttrack %d id %d type %d %s", i, int(track->GetTrack()), int(track->GetId()), int(track->GetType()), (trackName[0]?trackName:"unknown"));

			if(memory)
			{
				Printf(csv?",%d":" (%d bytes)", int(track->ComputeSize()));
			}

			if(values)
			{
				if(!csv)
				{
					Printf("\n");
				}

				for(u32 j=0; j<anim.GetNumInternalFrames(); ++j)
				{
					float t = float(j);

					switch(track->GetType())
					{
					case kFormatTypeFloat:
						{	
							float f;
							track->EvaluateFloat(t, f);
							Printf(csv?",%d,%f":"Value[%d]:\t%f\n", j, f);
						}
						break;

					case kFormatTypeVector3:
						{	
							Vec3V v;
							track->EvaluateVector3(t, v);
							Printf(csv?"%d,%f,%f,%f":"Value[%d]:\t%f %f %f\n", j, v[0], v[1], v[2]);
						}
						break;

					case kFormatTypeQuaternion:
						{	
							QuatV q;
							track->EvaluateQuaternion(t, q);
							if(eulers)
							{
								Vec3V e = QuatVToEulersXYZ(q);
								e *= ScalarVFromF32(RtoD);
								Printf(csv?",%d,%f,%f,%f":"Value[%d]:\t%f %f %f\n", j, e[0], e[1], e[2]);
							}
							else
							{
								Printf(csv?",%d,%f,%f,%f,%f":"Value[%d]:\t%f %f %f %f\n", j, q[0], q[1], q[2], q[3]);
							}
						}
						break;

					default: 
						Printf(csv?"%d,?":"Value[%d]:\t?\n", j);
					}
				}
			}

			if(chunks)
			{
				for(u16 c=0; c<track->GetNumChunks(); ++c)
				{
					const crAnimChunk* chunk = track->GetChunk(c);
					u8 track = chunk->GetTrack();
					u16 id = chunk->GetId();
					Printf(csv?" %d %d %d %d":"\nChunk[%d]:\ttrack %d id %d type %d", c, track, id, chunk->GetType());

					if(memory)
					{
						Printf(csv?" %d":" (%d bytes)", chunk->ComputeSize());
					}

					if(channels)
					{
						for(u16 i=0; i<chunk->GetNumChannels(); ++i)
						{
							Printf("\n");
							const crAnimChannel* channel = chunk->GetChannel(i);
							if(channel)
							{
								u8 channelType = channel->GetType();
								Printf(csv?" %d %d":"Channel[%d]:\ttype %d", i, channelType);
								if(memory)
								{
									Printf(csv?" %d":" (%d bytes)", channel->ComputeSize());
								}
							}
						}
					}
				}
			}
			Printf("\n");
		}
	}

	Printf("\n");

	if(blocks || chunks || channels)
	{
		const u16 numBlocks = u16(anim.GetNumBlocks());
		for(u16 i=0; i<numBlocks; ++i)
		{ 
			Printf("\n");
			if(csv)
			{
				Printf("\nBlockIdx,BlockSize,CompactBlockSize,CompactSlopSize");
				if(chunks || channels)
				{
					Printf(",ChunkAndChannels");
				}
				Printf("\n");
			}

			const crBlockStream* block = anim.GetBlock(i);
			const crBlockStream* compactBlock = anim.GetBlock(i);

			Printf(csv?"%d,%d,%d,%d":"Block[%d]:\tblock size %d compact block size %d compact slop size %d", i, block->m_DataSize, compactBlock->m_CompactSize, compactBlock->m_SlopSize);
		}
	}

	Printf("\n");

	return true;
}

////////////////////////////////////////////////////////////////////////////////


}; // namespace crAnimDumpCore

}; // namespace rage

