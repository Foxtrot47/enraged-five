// 
// crtools/animcompresscore.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "animcompresscore.h"

#include "animcmdlinehelper.h"

#include "cranimation/animation.h"
#include "cranimation/animstream.h"
#include "cranimation/animtrack.h"
#include "cranimation/frame.h"
#include "crmetadata/properties.h"
#include "crmetadata/property.h"
#include "crmetadata/propertyattributes.h"
#include "crskeleton/bonedata.h"
#include "crskeleton/skeletondata.h"
#include "string/string.h"
#include "string/stringutil.h"


namespace rage
{

namespace crAnimCompress
{

////////////////////////////////////////////////////////////////////////////////

bool ProcessSimpleCompressionSettings(crAnimToleranceSimple& tolerance, float maxTranslationError, float maxRotationError, float maxScaleError, float maxDefaultError, crAnimTolerance::eDecompressionCost decompressionCost, crAnimTolerance::eCompressionCost compressionCost, float minFrequency)
{
	tolerance.Init(maxTranslationError, maxRotationError, maxScaleError, maxDefaultError, decompressionCost, compressionCost, minFrequency);

	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool ParseProperty(const char*& text, u32& outHash, bool& outIsFloat, float& outFloat, int& outInt)
{
	char name[256];
	char* namePtr = name;
	*name = '\0';

	while(*text && *text != ',' && *text != '=')
	{
		*(namePtr++) = *(text++);
	}
	*namePtr = '\0';

	if(*text == '=' && name[0] != '\0')
	{
		text++;

		outHash = atHashString(name);

		char value[256];
		char* valuePtr = value;
		*value = '\0';

		outIsFloat = false;
		while(*text && *text != ',')
		{
			outIsFloat |= (*text == '.');
			*(valuePtr++) = *(text++);
		}
		*valuePtr = '\0';

		if(*text == ',')
		{
			text++;
		}

		if(value[0] != '\0')
		{
			if(outIsFloat)
			{
				outFloat = float(atof(value));
			}
			else
			{
				outInt = atoi(value);
			}

			return true;
		}
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

bool AddToleranceRule(crAnimToleranceComplex& tolerance, u8 track, bool trackWildcard, u16 id, bool idWildcard, float maxAbsoluteError, float minFrequency, crAnimTolerance::eDecompressionCost decompressionCost, crAnimTolerance::eCompressionCost compressionCost, const char* propertyText)
{
	u32 ruleIdx;
	if(decompressionCost != crAnimTolerance::kDecompressionCostInvalid || compressionCost != crAnimTolerance::kCompressionCostInvalid)
	{
		ruleIdx = tolerance.AddRule(track, trackWildcard, id, idWildcard, maxAbsoluteError, minFrequency, decompressionCost, compressionCost);
	}
	else if(minFrequency > 0.f)
	{
		ruleIdx = tolerance.AddRule(track, trackWildcard, id, idWildcard, maxAbsoluteError, minFrequency);
	}
	else if(maxAbsoluteError >= 0.f)
	{
		ruleIdx = tolerance.AddRule(track, trackWildcard, id, idWildcard, maxAbsoluteError);
	}
	else 
	{
		ruleIdx = tolerance.AddRule(track, trackWildcard, id, idWildcard);
	}

	const char* propertyTextPtr = propertyText;
	while(propertyTextPtr && *propertyTextPtr)
	{
		u32 hash;
		float f;
		int i;
		bool isFloat;

		if(ParseProperty(propertyTextPtr, hash, isFloat, f, i))
		{
			if(isFloat)
			{
				tolerance.AddProperty(ruleIdx, hash, f);
			}
			else
			{
				tolerance.AddProperty(ruleIdx, hash, i);
			}
		}
		else 
		{
			Errorf("ERROR - Malformed property string '%s'", propertyText);
			return false;
		}
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool ProcessComplexCompressionIdText(crAnimToleranceComplex& tolerance, const char* idText, u8 track, bool trackWildcard, float maxAbsoluteError, float minFrequency, crAnimTolerance::eDecompressionCost decompressionCost, crAnimTolerance::eCompressionCost compressionCost, const char* propertyText, const crSkeletonData* skelData)
{
	const int maxTextLen = 256;
	char idBuf[maxTextLen];
	strcpy(idBuf, idText);

	bool andChildren = (idBuf[strlen(idBuf)-1] == '+');
	if(andChildren)
	{
		switch(track)
		{
		case kTrackBoneTranslation:
		case kTrackBoneRotation:
		case kTrackBoneScale:
			idBuf[strlen(idBuf)-1] = '\0';
			break;

		default:
			Errorf("ERROR - Bad id text '%s' the 'and children' operator only supported on bone tracks", idBuf);
			return false;
		}
	}

	bool idWildcard = (idBuf[0] == '*' && idBuf[1] == '\0');
	if(idWildcard)
	{
		if(!AddToleranceRule(tolerance, track, trackWildcard, 0, idWildcard, maxAbsoluteError, minFrequency, decompressionCost, compressionCost, propertyText)) return false;
	}
	else
	{
		if(idBuf[0] == '#')
		{
			int idAsInt = atoi(idBuf+1);
			if(idAsInt < 0 || idAsInt > 65535)
			{
				Errorf("ERROR - Bad id value '%s'", idBuf);
				return false;
			}
			u16 id = u16(idAsInt);

			if(!AddToleranceRule(tolerance, track, trackWildcard, id, idWildcard, maxAbsoluteError, minFrequency, decompressionCost, compressionCost, propertyText)) return false;
	
			if(andChildren)
			{
				switch(track)
				{
				case kTrackBoneTranslation:
				case kTrackBoneRotation:
				case kTrackBoneScale:
					{
						if(!skelData)
						{
							Errorf("ERROR - Must specify skeleton data, if using bone names or children in complex compression rules");
							return false;
						}

						int boneIdx;
						if(skelData->ConvertBoneIdToIndex(id, boneIdx))
						{
							crBoneDataIterator it(skelData->GetBoneData(boneIdx));
							while(it.GetNext())
							{
								if(!AddToleranceRule(tolerance, track, trackWildcard, it.GetCurrent()->GetBoneId(), idWildcard, maxAbsoluteError, minFrequency, decompressionCost, compressionCost, propertyText)) return false;
							}
						}
						else
						{
							Warningf("ERROR - Unknown bone id '%d', unable to add children", id);
						}
						break;
					}
				}
			}
		}
		else
		{
			switch(track)
			{
			case kTrackBoneTranslation:
			case kTrackBoneRotation:
			case kTrackBoneScale:
				{
					if(!skelData)
					{
						Errorf("ERROR - Must specify skeleton data, if using bone names or children in complex compression rules");
						return false;
					}

					const int numBones = skelData->GetNumBones();
					for(int i=0; i<numBones; ++i)
					{
						const crBoneData* bd = skelData->GetBoneData(i);
						if(!StringWildcardCompare(idBuf, bd->GetName()))
						{
							if(!AddToleranceRule(tolerance, track, trackWildcard, bd->GetBoneId(), idWildcard, maxAbsoluteError, minFrequency, decompressionCost, compressionCost, propertyText)) return false;

							if(andChildren)
							{
								crBoneDataIterator it(bd);
								while(it.GetNext())
								{
									if(!AddToleranceRule(tolerance, track, trackWildcard, it.GetCurrent()->GetBoneId(), idWildcard, maxAbsoluteError, minFrequency, decompressionCost, compressionCost, propertyText)) return false;
								}
							}
						}
					}
				}
				break;

			case kTrackMoverTranslation:
			case kTrackMoverRotation:
			case kTrackMoverScale:
			case kTrackBlendShape:
				// TODO --- provide name -> hash support for these tracks?
			default:
				Errorf("ERROR - Bad id text '%s' non-numeric ids only supported on bone tracks", idBuf);
				return false;
			}
		}
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

void ParseText(const char*& text, char* buf, bool numeric=true, bool commas=false)
{
	*buf = '\0';

	while(*text && *text != ';' && (commas || *text != ',') && (!numeric || ((*text >= '0' && *text <= '9') || *text == '-' || *text == '.')))
	{
		*(buf++) = *(text++);
	}

	*buf = '\0';

	if(*text == ',')
	{
		text++;
	}
}

bool ProcessComplexCompressionRules(crAnimToleranceComplex& tolerance, const char* compressionRules, const crSkeletonData* skelData)
{
	if(compressionRules && compressionRules[0])
	{
		do 
		{
			const int maxTextLen = 256;
			char trackText[maxTextLen];
			char idText[maxTextLen];
			char errorText[maxTextLen];
			char frequencyText[maxTextLen];
			char decompressionCostText[maxTextLen];
			char compressionCostText[maxTextLen];
			char propertyText[maxTextLen];

			ParseText(compressionRules, trackText, false);
			ParseText(compressionRules, idText, false);
			ParseText(compressionRules, errorText);
			ParseText(compressionRules, frequencyText);
			ParseText(compressionRules, decompressionCostText);
			ParseText(compressionRules, compressionCostText);
			ParseText(compressionRules, propertyText, false, true);

			if(*compressionRules)
			{
				compressionRules++;
			}

			float maxAbsoluteError = -1.f;
			if(errorText[0])
			{
				maxAbsoluteError = float(atof(errorText));
				if(maxAbsoluteError < 0.f)
				{
					Errorf("ERROR - Bad maximum absolute error value '%s'", errorText);
					return false;
				}
			}

			float minFrequency = 0.f;
			if(frequencyText[0])
			{
				minFrequency = float(atof(frequencyText));
				if(minFrequency <= 0.f)
				{
					Errorf("ERROR - Bad minimum frequency value '%s'", frequencyText);
					return false;
				}
			}

			crAnimTolerance::eDecompressionCost decompressionCost = crAnimTolerance::kDecompressionCostInvalid;
			if(decompressionCostText[0])
			{
				decompressionCost = crAnimTolerance::eDecompressionCost(atoi(decompressionCostText));
				if(decompressionCost < crAnimTolerance::kDecompressionCostMinimum || decompressionCost > crAnimTolerance::kDecompressionCostMaximum)
				{
					Errorf("ERROR - Bad decompression cost value '%s'", decompressionCostText);
					return false;
				}
			}

			crAnimTolerance::eCompressionCost compressionCost = crAnimTolerance::kCompressionCostInvalid;
			if(decompressionCostText[0])
			{
				compressionCost = crAnimTolerance::eCompressionCost(atoi(compressionCostText));
				if(compressionCost < crAnimTolerance::kCompressionCostMinimum || compressionCost > crAnimTolerance::kCompressionCostMaximum)
				{
					Errorf("ERROR - Bad compression cost value '%s'", compressionCostText);
					return false;
				}
			}

			u8 track = 0;
			bool trackWildcard = (trackText[0] == '*' && trackText[1] == '\0');
			if(!trackWildcard)
			{
				if(!crAnimTrack::ConvertTrackNameToIndex(trackText, track))
				{
					if(trackText[0] =='#')
					{
						int trackAsInt = atoi(trackText+1);
						if(trackAsInt < 0 || trackAsInt > 255)
						{
							Errorf("ERROR - Bad track value '%s'", trackText);
							return false;
						}
						track = u8(trackAsInt);
					}
					else
					{
						Errorf("ERROR - Unknown track name '%s'", trackText);
						return false;
					}
				}
			}

			if(!ProcessComplexCompressionIdText(tolerance, idText, track, trackWildcard, maxAbsoluteError, minFrequency, decompressionCost, compressionCost, propertyText, skelData))
			{
				return false;
			}
		}
		while(*compressionRules);
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool ProcessPropertySimpleCompressionSettings(crAnimToleranceSimple& tolerance, const crProperty& property)
{
	crPropertyAttributeFloatAccessor accessMaxTranslationError(property.GetAttribute("MaxTranslationError"));
	crPropertyAttributeFloatAccessor accessMaxRotationError(property.GetAttribute("MaxRotationError"));
	crPropertyAttributeFloatAccessor accessMaxScaleError(property.GetAttribute("MaxScaleError"));
	crPropertyAttributeFloatAccessor accessMaxDefaultError(property.GetAttribute("MaxDefaultError"));
	crPropertyAttributeIntAccessor accessDecompressionCost(property.GetAttribute("DecompressionCost"));
	crPropertyAttributeIntAccessor accessCompressionCost(property.GetAttribute("CompressionCost"));
	crPropertyAttributeFloatAccessor accessMinFrequency(property.GetAttribute("MinFrequency"));

	float maxTranslationError = crAnimToleranceSimple::sm_DefaultMaxAbsoluteTranslationError;
	if(accessMaxTranslationError.Valid())
	{
		accessMaxTranslationError.Get(maxTranslationError);
		if(maxTranslationError < 0.f)
		{
			Errorf("ERROR - Bad maximum absolute translation error %f", maxTranslationError);
			return false;
		}
	}

	float maxRotationError = crAnimToleranceSimple::sm_DefaultMaxAbsoluteRotationError;
	if(accessMaxRotationError.Valid())
	{
		accessMaxRotationError.Get(maxRotationError);
		if(maxRotationError < 0.f)
		{
			Errorf("ERROR - Bad maximum absolute rotation error %f", maxRotationError);
			return false;
		}
	}

	float maxScaleError = crAnimToleranceSimple::sm_DefaultMaxAbsoluteScaleError;
	if(accessMaxScaleError.Valid())
	{
		accessMaxScaleError.Get(maxScaleError);
		if(maxScaleError < 0.f)
		{
			Errorf("ERROR - Bad maximum absolute scale error %f", maxScaleError);
			return false;
		}
	}

	float maxDefaultError = crAnimToleranceSimple::sm_DefaultMaxAbsoluteDefaultError;
	if(accessMaxDefaultError.Valid())
	{
		accessMaxDefaultError.Get(maxDefaultError);
		if(maxDefaultError < 0.f)
		{
			Errorf("ERROR - Bad maximum absolute default error %f", maxDefaultError);
			return false;
		}
	}

	crAnimTolerance::eDecompressionCost decompressionCost = crAnimTolerance::kDecompressionCostDefault;
	if(accessDecompressionCost.Valid())
	{
		accessDecompressionCost.Get(*reinterpret_cast<int*>(&decompressionCost));
		if(decompressionCost < crAnimTolerance::kDecompressionCostMinimum || decompressionCost > crAnimTolerance::kDecompressionCostMaximum)
		{
			Errorf("ERROR - Bad decompression cost %d", decompressionCost);
			return false;
		}
	}

	crAnimTolerance::eCompressionCost compressionCost = crAnimTolerance::kCompressionCostDefault;
	if(accessCompressionCost.Valid())
	{
		accessCompressionCost.Get(*reinterpret_cast<int*>(&compressionCost));
		if(compressionCost < crAnimTolerance::kCompressionCostMinimum || compressionCost > crAnimTolerance::kCompressionCostMaximum)
		{
			Errorf("ERROR - Bad compression cost %d", compressionCost);
			return false;
		}
	}

	float minFrequency = crAnimToleranceSimple::sm_DefaultMinFrequency;
	if(accessMinFrequency.Valid())
	{
		accessMinFrequency.Get(minFrequency);
		if(minFrequency <= 0.f)
		{
			Errorf("ERROR - Bad minimum frequency %f", minFrequency);
			return false;
		}
	}

	return ProcessSimpleCompressionSettings(tolerance, maxTranslationError, maxRotationError, maxScaleError, maxDefaultError, decompressionCost, compressionCost, minFrequency);
}

////////////////////////////////////////////////////////////////////////////////

bool ProcessPropertyComplexCompressionSettings(crAnimToleranceComplex& tolerance, const crProperty& property)
{
	if(ProcessPropertySimpleCompressionSettings(tolerance, property))
	{
		crSkeletonData skelData;
		const crSkeletonData* skelDataPtr = NULL;

		crPropertyAttributeStringAccessor accessSkelFile(property.GetAttribute("Skeleton"));
		if(accessSkelFile.Valid())
		{
			const char* skelFile = accessSkelFile.GetPtr()->c_str();
			if(skelFile)
			{
				if(skelData.Load(skelFile))
				{
					skelDataPtr = &skelData;
				}
				else
				{
					Warningf("WARNING - failed to load skeleton file '%s'", skelFile);
				}
			}
		}

		crPropertyAttributeStringAccessor accessRules(property.GetAttribute("Rules"));
		if(accessRules.Valid())
		{
			const char* rules = accessRules.GetPtr()->c_str();
			if(rules)
			{
				return ProcessComplexCompressionRules(tolerance, rules, skelDataPtr);
			}
		}
		return true;
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

bool ProcessPropertySimpleCompressionSettings(crAnimToleranceSimple& tolerance, const crProperties& properties)
{
	const crProperty* prop = properties.FindProperty("Compression_DO_NOT_RESOURCE");
	if(prop)
	{
		return ProcessPropertySimpleCompressionSettings(tolerance, *prop);
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

bool ProcessPropertyComplexCompressionSettings(crAnimToleranceComplex& tolerance, const crProperties& properties)
{
	const crProperty* prop = properties.FindProperty("Compression_DO_NOT_RESOURCE");
	if(prop)
	{
		return ProcessPropertyComplexCompressionSettings(tolerance, *prop);
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

bool ProcessMovementCompressionSettings(crAnimToleranceMovement& tolerance, float speedConstriction, float speedRelaxation, float minSpeed, float maxSpeed)
{
	tolerance.SetSpeedRelaxation(speedConstriction, speedRelaxation, minSpeed, maxSpeed);

	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool ProcessMovementBoneChains(crAnimToleranceMovement& tolerance, const char* boneNames, const crSkeletonData& skelData)
{
	if(boneNames)
	{
		while(*boneNames)
		{
			const int maxBoneNameLen = 256;
			char boneName[maxBoneNameLen];
			char* boneNamePtr = boneName;

			while(*boneNames)
			{
				if(*boneNames != ',')
				{
					*(boneNamePtr++) = *(boneNames++);
				}
				else
				{
					boneNames++;
					break;
				}
			}
			*boneNamePtr = '\0';

			const crBoneData* bd = skelData.FindBoneData(boneName);
			if(bd)
			{
				tolerance.AddBoneChain(bd->GetBoneId());
			}
			else
			{
				Warningf("crAnimCompress::ProcessMovementBoneChains - failed to find bone '%s'", boneName);
			}
		}
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool CalcMovementCompression(crAnimToleranceMovement& tolerance, const crAnimation& srcAnim, const crSkeletonData& skelData)
{
	const float sampleRate = srcAnim.GetDuration() / float(srcAnim.GetNumInternalFrames()-1);

	return tolerance.CalcRelaxation(srcAnim, skelData, sampleRate);
}

////////////////////////////////////////////////////////////////////////////////

u32 CompressAnimationFrames(const crAnimation& srcAnim, const atArray<const crFrame*>& frames, crAnimation& destAnim, const crAnimTolerance& tolerance, u32 maxBlockSize, u32 maxNumBlocks)
{
	const u32 numFrames = srcAnim.GetNumInternalFrames();

	// clear out previous attempt
	destAnim.Shutdown();

	// calculate the frames per chunk
	u32 framesPerChunk = u32((numFrames-1)/(maxNumBlocks));

	// round up to next 16 frame value minus 1
	const u32 minFramesPerChunk = 16;
	if(maxNumBlocks == 1 || framesPerChunk >= minFramesPerChunk || u32((numFrames-1)/(maxNumBlocks-1)) >= minFramesPerChunk)
	{
		framesPerChunk = ((framesPerChunk+16)&(~15))-1;
	}
	else
	{
		return false;
	}

	// create animation, compress frames with tolerance and then pack (so block size can be measured)
	destAnim.Create(numFrames, srcAnim.GetDuration(), srcAnim.IsLooped(), srcAnim.HasMoverTracks(), framesPerChunk);
	destAnim.SetProjectFlags(srcAnim.GetProjectFlags());
	destAnim.CreateFromFramesFast(frames, tolerance);

	u32 averageBlockSize = destAnim.ComputeSize() / maxNumBlocks;
	return averageBlockSize <= maxBlockSize;
}

////////////////////////////////////////////////////////////////////////////////

bool CompressAnimation(const crAnimation& srcAnim, crAnimation& destAnim, const crAnimTolerance& tolerance, u32 maxBlockSize, bool exhaustive, bool pack)
{
	// convert source animation into frames:
	const u32 minFramesPerChunk = 16;
	const u32 numFrames = srcAnim.GetNumInternalFrames();
	const u32 maxNumBlocks = Max((numFrames / minFramesPerChunk)-1, 1u);
	atArray<const crFrame*> frames(numFrames, numFrames);

	for(u32 i=0; i<numFrames; i++)
	{
		crFrame* frame = rage_new crFrame;
		frame->InitCreateAnimationDofs(srcAnim);
		frame->Composite(srcAnim, srcAnim.ConvertInternalFrameToTime(float(i)));
		if(!frame->Legal())
			return false;

		frames[i] = frame;
	}

	// create new animation, try a single block first
	u32 numBlocks = 1;
	if(!CompressAnimationFrames(srcAnim, frames, destAnim, tolerance, maxBlockSize, numBlocks))
	{
		exhaustive |= (numFrames <= (minFramesPerChunk*8));  // use exhaustive search if only small number of blocks possible

		if(exhaustive)
		{
			do 
			{
				numBlocks++;
			} 
			while(!CompressAnimationFrames(srcAnim, frames, destAnim, tolerance, maxBlockSize, numBlocks));
		}
		else
		{
			u32 lowBlocks = 1;
			u32 highBlocks = (numFrames / minFramesPerChunk)-1;
			u32 midBlocks = Clamp(u32(numFrames / ((float(numFrames) / float(destAnim.GetMaxBlockSize())) * maxBlockSize)), lowBlocks+1, highBlocks-1);

			while(lowBlocks <= highBlocks)
			{
				if(CompressAnimationFrames(srcAnim, frames, destAnim, tolerance, maxBlockSize, midBlocks))
				{
					numBlocks = midBlocks;
					highBlocks = midBlocks-1;
				}
				else 
				{
					lowBlocks = midBlocks+1;
				}
				midBlocks = (lowBlocks+highBlocks) >> 1;
			}

			// last try may have been a fail, re-compress at last successful value
			if(midBlocks != numBlocks)
			{
				AssertVerify(CompressAnimationFrames(srcAnim, frames, destAnim, tolerance, maxBlockSize, numBlocks));
			}
		}
	}

	if(pack)
	{
		u32 bestNumBlocks = numBlocks;
		u32 bestTotalSize = INT_MAX;
		u32 bestRetries = 2;

		// pack animation
		while(1)
		{
			destAnim.Pack(false, false, false);
			if(destAnim.GetMaxBlockSize() <= maxBlockSize)
			{
				u32 totalSize = 0;
				for(u32 i=0; i<destAnim.GetNumBlocks(); ++i)
				{
					totalSize += destAnim.GetBlock(i)->GetBlockSize();
				}
				if(totalSize < bestTotalSize)
				{
					bestNumBlocks = numBlocks;
					bestTotalSize = totalSize;
				}
				else
				{
					if(!bestRetries--)
					{
						break;
					}
				}
			}

			if(numBlocks >= maxNumBlocks)
			{
				break;
			}
			numBlocks++;
			if(!CompressAnimationFrames(srcAnim, frames, destAnim, tolerance, maxBlockSize, numBlocks))
			{
				break;
			}
		}

		if(numBlocks != bestNumBlocks)
		{
			AssertVerify(CompressAnimationFrames(srcAnim, frames, destAnim, tolerance, maxBlockSize, bestNumBlocks));
			destAnim.Pack(false, false, false);
			Assert(destAnim.GetMaxBlockSize() <= maxBlockSize);
		}
	}

	// shutdown:
	for(u32 i=0; i<numFrames; i++)
	{
		delete frames[i];
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool DecompressAnimation(const crAnimation& srcAnim, crAnimation& destAnim)
{
	// convert source animation into frames
	const u32 numFrames = srcAnim.GetNumInternalFrames();
	atArray<const crFrame*> frames(numFrames, numFrames);

	for(u32 i=0; i<numFrames; i++)
	{
		crFrame* frame = rage_new crFrame;
		frame->InitCreateAnimationDofs(srcAnim);
		frame->Composite(srcAnim, srcAnim.ConvertInternalFrameToTime(float(i)));
		if(!frame->Legal())
			return false;

		frames[i] = frame;
	}

	// convert frames to raw animation
	crAnimToleranceSimple tolerance(crAnimToleranceSimple::sm_DefaultMaxAbsoluteTranslationError, 
		crAnimToleranceSimple::sm_DefaultMaxAbsoluteRotationError, 
		crAnimToleranceSimple::sm_DefaultMaxAbsoluteScaleError, 
		crAnimToleranceSimple::sm_DefaultMaxAbsoluteDefaultError, 
		crAnimTolerance::kDecompressionCostMinimum, 
		crAnimTolerance::kCompressionCostMinimum);

	destAnim.Create(numFrames, srcAnim.GetDuration(), srcAnim.IsLooped(), srcAnim.HasMoverTracks(), numFrames, true);
	destAnim.SetProjectFlags(srcAnim.GetProjectFlags());
	destAnim.CreateFromFramesFast(frames, tolerance);

	// shutdown
	for(u32 i=0; i<numFrames; ++i)
	{
		delete frames[i];
	}
	return true;
}

////////////////////////////////////////////////////////////////////////////////


}; // namespace crAnimCompressCore

}; // namespace rage

