// 
// crtools/animcmdlinehelper.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef CRTOOLS_ANIMCMDLINEHELPER_H
#define CRTOOLS_ANIMCMDLINEHELPER_H

namespace rage
{

namespace crAnimCmdLineHelper
{

// PURPOSE: Process animlist or cliplist file
extern int ProcessListFile(const char* listFile, const char** entries, int maxEntries, char* buf, int maxBufSize);

// PURPOSE: Compare string against string containing wild cards (* and ?)
//extern int StrWildCmp(const char *wild, const char *string);

}; // namespace crAnimCmdLineHelper

}; // namespace rage

#endif // CRTOOLS_ANIMCMDLINEHELPER_H
