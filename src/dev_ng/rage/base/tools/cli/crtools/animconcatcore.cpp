// 
// crtools/animconcatcore.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "animconcatcore.h"

#include "atl/array.h"
#include "cranimation/animation.h"
#include "cranimation/animtolerance.h"
#include "cranimation/frame.h"
#include "vectormath/classes.h"

namespace rage
{

namespace crAnimConcatCore
{

////////////////////////////////////////////////////////////////////////////////

AnimationSection::AnimationSection()
: m_SrcAnim(NULL)
, m_Start(0.f)
, m_End(-1.f)
, m_Overlap(0.f)
{	
}

////////////////////////////////////////////////////////////////////////////////

bool ConcatAnimations(const atArray<AnimationSection>& animSections, crAnimation& destAnim, float sampleRate, bool loop, float leadIn, bool superset, bool absoluteMover, const crSkeletonData* skelData)
{
	Assert(animSections.GetCount() > 0);
	Assert(animSections[0].m_SrcAnim != NULL);
	Assert(leadIn<=0.f && (leadIn==0.f || !loop));

	float sampleFreq = 1.f/sampleRate;

	// TODO --- drive by parameter?
	const u16 moverId = 0;

	// verify source animations:
	const crAnimation& srcAnim0 = *animSections[0].m_SrcAnim;
	const bool hasMoverTracks = srcAnim0.HasMoverTracks();
	const int numAnims = animSections.GetCount();
	for(int i=1; i<numAnims; ++i)
	{
		Assert(animSections[i].m_SrcAnim != NULL);
		if(hasMoverTracks != animSections[i].m_SrcAnim->HasMoverTracks())
		{
			Quitf("ERROR - Animations must be consistent with presence/absence of mover tracks");
		}
	}

	// create destination frames:
	atArray<const crFrame*> destFrames;

	Mat34V moverMtx(V_IDENTITY);
	if(srcAnim0.HasMoverTracks() && !absoluteMover)
	{
		crFrame moverFrame;
		moverFrame.InitCreateMoverDofs();
		srcAnim0.CompositeFrame(0.f, moverFrame);

		moverFrame.GetMoverMatrix(moverId, moverMtx);
	}

	crFrame srcFrame;
	srcFrame.InitCreateAnimationDofs(srcAnim0);
	if(superset)
	{
		for(int a=1; a<numAnims; ++a)
		{
			srcFrame.InitCreateAnimationDofs(*animSections[a].m_SrcAnim, false);
		}
	}

	bool isRaw = true;
	for(int i=0; i<numAnims; ++i)
	{
		const bool first = (i==0);
		const bool last = (i==(numAnims-1)) && !loop;
		const crAnimation& srcAnim = *animSections[i].m_SrcAnim;

		const float duration = ((animSections[i].m_End<0.f)?srcAnim.GetDuration():animSections[i].m_End)-animSections[i].m_Start;
		Assert(duration >= 0.f);

		isRaw = isRaw && srcAnim.IsRaw();
		
		const float overlapPrevious = (i>0)?animSections[i-1].m_Overlap:(loop?animSections[numAnims-1].m_Overlap:0.f);
		const float overlap = animSections[i].m_Overlap;

		if(duration < ((last?0.f:Max(overlap, 0.f)) + Max(overlapPrevious, 0.f)))
		{
			Warningf("WARNING - overlap too large for animation durations, undefined results");
		}

		int animFrames = int(((duration + Max(-overlap, 0.f)) * sampleFreq) + 0.5f) + 1;
		for(int af=first?((leadIn<0.f)?int(leadIn*sampleFreq-0.5f):0):((overlapPrevious>0.f)?(int((overlapPrevious*sampleFreq)+0.5f)+1):1); af<animFrames; ++af)
		{
			float at = animSections[i].m_Start + Max(float(af) / sampleFreq, 0.f);

			crFrame* destFrame = rage_new crFrame;
			destFrame->Init(srcFrame);
			destFrame->Zero();
			if(skelData)
			{
				destFrame->IdentityFromSkel(*skelData);
			}

			if(absoluteMover)
			{
				srcAnim.CompositeFrame(Min(at, (animSections[i].m_End<0.f)?duration:animSections[i].m_End), *destFrame);
			}
			else
			{
				srcAnim.CompositeFrameWithMover(Min(at, (animSections[i].m_End<0.f)?duration:animSections[i].m_End), sampleRate, *destFrame);
			}

			if(overlap>0.f && !last && (duration-at)<=overlap)
			{
				float bt = overlap - (duration-at);
				float bw = Min(bt / overlap, 1.f);

				crFrame blendFrame;
				blendFrame.InitCreateAnimationDofs(srcAnim);
				if(absoluteMover)
				{
					animSections[(i+1)%numAnims].m_SrcAnim->CompositeFrame(bt + animSections[(i+1)%numAnims].m_Start, blendFrame);
				}
				else
				{
					animSections[(i+1)%numAnims].m_SrcAnim->CompositeFrameWithMover(bt + animSections[(i+1)%numAnims].m_Start, sampleRate, blendFrame);
				}

				destFrame->Blend(bw, blendFrame);
			}
			else if(overlap<0.f && !last && (at-duration)>0.f)
			{
				float bt = at-duration;
				float bw = Min(bt / -overlap, 1.f);

				crFrame blendFrame;
				blendFrame.InitCreateAnimationDofs(srcAnim);
				if(absoluteMover)
				{
					animSections[(i+1)%numAnims].m_SrcAnim->CompositeFrame(animSections[(i+1)%numAnims].m_Start, blendFrame);
				}
				else
				{
					animSections[(i+1)%numAnims].m_SrcAnim->CompositeFrameWithMover(animSections[(i+1)%numAnims].m_Start, sampleRate, blendFrame);
				}

				destFrame->Blend(bw, blendFrame);
			}

			Mat34V mtx;
			if(destFrame->GetMoverMatrix(moverId, mtx) && !absoluteMover)
			{
				Transform(moverMtx, moverMtx, mtx);
				destFrame->SetMoverMatrix(moverId, moverMtx);
			}

			if(!destFrame->Legal())
			{
				Quitf("WARNING - illegal values found in source animations, result would be corrupt");
			}
			destFrames.Grow() = destFrame;
		}
	}

	// create destination animation:
	int numFrames = destFrames.GetCount();
	float totalDuration = float(numFrames-1)*sampleRate;

	destAnim.Create(numFrames, totalDuration, false, hasMoverTracks, crAnimation::sm_DefaultFramesPerChunk, isRaw);
	destAnim.CreateFromFramesFast(destFrames, LOSSLESS_ANIM_TOLERANCE);

	// cleanup:
	for(int i=0; i<destFrames.GetCount(); ++i)
	{
		if(destFrames[i])
		{
			delete destFrames[i];
		}
	}
	destFrames.Reset();

	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool ConcatAnimations(const atArray<const crAnimation*>& srcAnims, crAnimation& destAnim, float overlap, float sampleRate, bool loop, float leadIn, float leadOut, bool superset, bool absoluteMover, bool crop, const crSkeletonData* skelData)
{
	const int numAnims = srcAnims.GetCount();

	atArray<AnimationSection> animSections;
	animSections.Resize(numAnims);

	for(int i=0; i<numAnims; ++i)
	{
		animSections[i].m_SrcAnim = srcAnims[i];
		animSections[i].m_Overlap = -overlap;
		animSections[i].m_Start = 0.f;
		if(crop && i<(numAnims-1) && overlap>0.f)
		{
			animSections[i].m_End = srcAnims[i]->GetDuration() - overlap;
		}
		else
		{
			animSections[i].m_End = srcAnims[i]->GetDuration();
		}
	}

	if(!loop)
	{
		animSections[numAnims-1].m_Overlap = -leadOut;
	}

	return crAnimConcatCore::ConcatAnimations(animSections, destAnim, sampleRate, loop, loop?0.f:-leadIn, superset, absoluteMover, skelData);
}

////////////////////////////////////////////////////////////////////////////////

bool ConcatAnimations(const crAnimation& srcAnim0, const crAnimation& srcAnim1, crAnimation& destAnim, float overlap, float sampleRate, bool loop, float leadIn, float leadOut, bool superset, bool absoluteMover, bool crop, const crSkeletonData* skelData)
{
	atArray<const crAnimation*> srcAnims;
	srcAnims.Grow() = &srcAnim0;
	srcAnims.Grow() = &srcAnim1;

	return crAnimConcatCore::ConcatAnimations(srcAnims, destAnim, overlap, sampleRate, loop, leadIn, leadOut, superset, absoluteMover, crop, skelData);
}

////////////////////////////////////////////////////////////////////////////////

}; // namespace crAnimConcatCore

}; // namespace rage
