// 
// crtools/makeproperties.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "crmetadata/properties.h"
#include "crmetadata/property.h"
#include "file/asset.h"
#include "system/main.h"
#include "system/param.h"

using namespace rage;

REQ_PARAM(file, "Properties file (Example: skeleton.properties)");

int Main()
{
	crProperty::InitClass();

	// load properties
	const char* filename = NULL;
	PARAM_file.Get(filename);
	if(!filename)
	{
		Quitf("ERROR - missing properties filename");
	}

	crProperties* properties = NULL;
	if(ASSET.Exists(filename, "properties"))
	{
		properties = crProperties::AllocateAndLoad(filename);
	}
	if(!properties)
	{
		properties = rage_new crProperties();
	}

	// save properties
	properties->Save(filename);

	// cleanup
	delete properties;

	return 0;
}