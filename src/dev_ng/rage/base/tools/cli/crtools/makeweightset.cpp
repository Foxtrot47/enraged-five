// 
// /makeweightset.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "cranimation/animation.h"
#include "cranimation/weightset.h"
#include "crskeleton/skeletondata.h"
#include "file/asset.h"
#include "system/main.h"
#include "system/param.h"

using namespace rage;

PARAM(help, "Get command line help");
PARAM(skel, "Skeleton file (Example: skeleton.skel)");
PARAM(out, "Destination weight set (Example: out.weightset)");
PARAM(in, "Optional input weight set (Example: in.weightset)");
PARAM(weight, "Weight setting instruction(s) (Example: <bonename0|id[x]|idx[x]>[=0.5][*], ...)");
PARAM(verbose, "Display verbose output");

int Main()
{
	// initialization:
	ASSET.SetPath(RAGE_ASSET_ROOT);
	crAnimation::InitClass();

	// process help:
	if(PARAM_help.Get())
	{
		sysParam::Help("");
		return 0;
	}

	// process verbose:
	bool verbose = PARAM_verbose.Get();

	// process skeleton file:
	const char* skelFilename = NULL;
	PARAM_skel.Get(skelFilename);
	if(!skelFilename)
	{
		Quitf("ERROR - No skeleton file specified");
	}

	// process input weight set filename:
	const char* weightSetFilename = NULL;
	PARAM_in.Get(weightSetFilename);

	// process output filename:
	const char* outFilename = NULL;
	PARAM_out.Get(outFilename);
	if(!outFilename)
	{
		Quitf("ERROR - No output file specified");
	}

	// process set instructions:
	const int maxInstructions = 64;
	const int maxInstructionBufSize = 8192;
	
	int numInstructions = 0;
	const char* instructions[maxInstructions];
	char instructionBuf[maxInstructionBufSize];

	numInstructions = PARAM_weight.GetArray(instructions, maxInstructions, instructionBuf, maxInstructionBufSize);
	if(numInstructions <= 0)
	{
		Quitf("ERROR - No weight instructions specified");
	}

	// load skeleton data:
	crSkeletonData* skelData = crSkeletonData::AllocateAndLoad(skelFilename);
	if(!skelData)
	{
		Quitf("ERROR - Failed to load skeleton file '%s'", skelFilename);
	}

	// load/create weight set:
	crWeightSet* weightSet = NULL;
	if(weightSetFilename)
	{
		weightSet = crWeightSet::AllocateAndLoad(weightSetFilename, *skelData);
		if(!weightSet)
		{
			Quitf("ERROR - Failed to load weight set file '%s'", weightSetFilename);
		}
	}
	else
	{
		weightSet = rage_new crWeightSet(*skelData);
	}

	// for each weight instruction:
	for(int i=0; i<numInstructions; ++i)
	{
		char buf[maxInstructionBufSize];
		strcpy(buf, instructions[i]);

		bool andChildren = false;

		char* nameString = &buf[0];
		char* weightString = NULL;

		char* c = &buf[0];
		while(*c != '\0')
		{
			if(*c == '=')
			{
				*c = '\0';
				if(!weightString)
				{
					if(*++c)
					{
						weightString = c;
					}
					continue;
				}
				else
				{
					Quitf("ERROR - Badly formatted weight instruction '%s'", instructions[i]);
				}
			}
			else if(*c == '*')
			{
				*c='\0';
				if(*++c)
				{
					Quitf("ERROR - Badly formatted weight instruction '%s'", instructions[i]);
				}
				andChildren = true;
				continue;
			}
			c++;
		}

		int boneIdx;
		if(strlen(nameString)>5 && nameString[0]=='i' && nameString[1]=='d' && nameString[2]=='x' && nameString[3]=='[' && nameString[strlen(nameString)-1]==']')
		{
			nameString[strlen(nameString)-1] = '\0';

			boneIdx = atoi(&nameString[4]);
			if(boneIdx < 0 || boneIdx >= skelData->GetNumBones())
			{
				Quitf("ERROR - bad/out of range bone index '%s'", &nameString[4]);
			}
		}
		else if(strlen(nameString)>3 && nameString[0]=='i' && nameString[1]=='d' && nameString[2]=='[' && nameString[strlen(nameString)-1]==']')
		{
			nameString[strlen(nameString)-1] = '\0';

			u16 boneId = u16(atoi(&nameString[3]));
			if(!skelData->ConvertBoneIdToIndex(boneId, boneIdx))
			{
				Quitf("ERROR - bad/unknown bone id '%s'", &nameString[3]);
			}
		}
		else
		{
			const crBoneData* bd = skelData->FindBoneData(nameString);
			if(!bd)
			{
				Quitf("ERROR - bad/unknown bone name '%s'", nameString);
			}
			boneIdx = bd->GetIndex();
		}

		float weight = 1.f;
		if(weightString)
		{
			weight = float(atof(weightString));
		}

		if(verbose)
		{
			Printf("setting weight '%f' on bone[%d] '%s'%s\n", weight, boneIdx, skelData->GetBoneData(boneIdx)->GetName(), (andChildren?" and children":""));
		}

		weightSet->SetAnimWeight(boneIdx, weight, andChildren);
	}

	// output weight set file:
	if(!weightSet->Save(outFilename))
	{
		Quitf("ERROR - failed to save weight set file '%s'", outFilename);
	}

	// shutdown:
	delete weightSet;
	delete skelData;

	crAnimation::ShutdownClass();

	return 0;
}
