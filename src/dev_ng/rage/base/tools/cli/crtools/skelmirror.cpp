#include "system/main.h"
#include "system/param.h"

#include "crskeleton/skeletondata.h"

using namespace rage;

PARAM(skel, "Skeleton file");
PARAM(out, "Output skeleton file");
PARAM(type, "Naming convention used for joints. Options are r*sd and r*n");

int Main()
{
	crSkeletonData* skelData = NULL;
	const char* skelFilename = NULL;
	if (PARAM_skel.Get(skelFilename))
	{
		skelData = crSkeletonData::AllocateAndLoad(skelFilename);
		if (!skelData)
		{
			Errorf("Failed ot load .skel file '%s'", skelFilename);
		}
	}
	else 
	{
		Errorf("No .skel file specified");
	}

	const char* outFilename = NULL;
	if (!PARAM_out.Get(outFilename))
	{
		Errorf("No output .skel file specified");
	}

	const char* type = NULL;
	if (PARAM_type.Get(type))
	{
		if (stricmp(type, "r*sd") == 0)
		{
			skelData->GenerateMirrorIndices2();
		}
		else if (stricmp(type, "r*n") == 0)
		{
			skelData->GenerateMirrorIndices();
		}
		else
		{
			Errorf("Unrecognised type '%s'", type);
		}
	}
	else
	{
		// Default to R*N convention
		skelData->GenerateMirrorIndices();
	}

	skelData->Save(outFilename);

	return 0;
}