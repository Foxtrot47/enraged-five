// 
// animdump.cpp 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#include "animdumpcore.h"

#include "cranimation/animation.h"
#include "crskeleton/skeletondata.h"
#include "file/asset.h"
#include "file/serialize.h"
#include "system/main.h"
#include "system/param.h"

#if !__TOOL
#error "AnimDump needs to use the default memory allocator. Compile a tool build instead."
#endif

using namespace rage;

PARAM(help, "Get command line help");
PARAM(anim, "Source animation(s) (Example: anim0.anim,anim1.anim ...)");
PARAM(csv, "Produce output in comma seperated format");
PARAM(tracks, "Display track info");
PARAM(blocks, "Display block info");
PARAM(chunks, "Display chunk info");
PARAM(channels, "Display channel info");
PARAM(values, "Display values contained within tracks");
PARAM(eulers, "Display quaternions as eulers");
PARAM(memory, "Display estimated memory use");
PARAM(skel, "Optional skeleton data file (Example skeleton.skel)");
PARAM(pack, "Pack loaded animation");
PARAM(compact, "Compact loaded animation");

int Main()
{
	// initialization:
	ASSET.SetPath(RAGE_ASSET_ROOT);
	crAnimation::InitClass();

	// process help:
	if(PARAM_help.Get())
	{
		sysParam::Help("");
		return 0;
	}

	// process anim list:
	const int maxAnims = 64;
	const int maxAnimBufSize = 8192;
	
	int numAnims = 0;
	const char* animFilenames[maxAnims];
	char animBuf[maxAnimBufSize];

	numAnims = PARAM_anim.GetArray(animFilenames, maxAnims, animBuf, maxAnimBufSize);
	if(numAnims <= 0)
	{
		Quitf("ERROR - No source animation(s) specified\n");
	}

	// process skeleton data:
	crSkeletonData skelData;
	crSkeletonData* skelDataPtr = NULL;

	const char* skelDataFilename = NULL;
	if(PARAM_skel.Get(skelDataFilename) && skelDataFilename && skelDataFilename[0])
	{
		if(skelData.Load(skelDataFilename, NULL))
		{
			skelDataPtr = &skelData;
		}
		else
		{
			Quitf("ERROR - failed to load skeleton data filename '%s'", skelDataFilename);
		}
	}

	bool tracks = PARAM_tracks.Get();
	bool blocks = PARAM_blocks.Get();
	bool chunks = PARAM_chunks.Get();
	bool channels = PARAM_channels.Get();
	bool values = PARAM_values.Get();
	bool eulers = PARAM_eulers.Get();
	bool csv = PARAM_csv.Get();
	bool memory = PARAM_memory.Get();
	bool pack = PARAM_pack.Get();
	bool compact = PARAM_compact.Get();

	// for each animation:
	for(int i=0; i<numAnims; i++)
	{
		u8 version;
		crAnimation anim;
		if(!anim.Load(animFilenames[i], &version, pack, compact, true))
		{
			Errorf("ERROR - failed to load animation '%s'\n", animFilenames[i]);
			continue;
		}

		crAnimDump::DumpAnimation(anim, version, csv, tracks, blocks, chunks, channels, values, eulers, memory, skelDataPtr);
	}

	// shutdown:
	crAnimation::ShutdownClass();

	return 0;
}
