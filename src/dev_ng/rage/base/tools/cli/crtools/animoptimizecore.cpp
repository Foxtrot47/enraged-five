// 
// crtools/animoptimizecore.cpp 
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "animoptimizecore.h"

#include "cranimation/animation.h"
#include "cranimation/animtolerance.h"
#include "cranimation/animtrack.h"
#include "cranimation/frame.h"
#include "cranimation/frameinitializers.h"
#include "crskeleton/skeleton.h"
#include "crskeleton/skeletondata.h"
#include "crtools/animcompresscore.h"
#include "distribute/distributeincredibuild.h"
#include "file/asset.h"
#include "file/token.h"
#include "math/simplemath.h"
#include "system/exec.h"


namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crAnimOptimizer::crAnimOptimizer(u32 seed)
: m_SampleRate(1.f/30.f)
, m_DefaultRotationTolerance(crAnimToleranceSimple::sm_DefaultMaxAbsoluteRotationError)
, m_DefaultTranslationTolerance(crAnimToleranceSimple::sm_DefaultMaxAbsoluteTranslationError)
, m_DefaultScaleTolerance(crAnimToleranceSimple::sm_DefaultMaxAbsoluteScaleError)
, m_DefaultTolerance(crAnimToleranceSimple::sm_DefaultMaxAbsoluteDefaultError)
, m_CompressionCost(crAnimTolerance::kCompressionCostDefault)
, m_DecompressionCost(crAnimTolerance::kDecompressionCostDefault)
, m_PreCompressionRules(NULL)
, m_PostCompressionRules(NULL)
{
	Displayf("Random number generator seed %d", seed);
	m_Random.SetFullSeed(u64(seed));
}

////////////////////////////////////////////////////////////////////////////////

crAnimOptimizer::~crAnimOptimizer()
{
	const int numSolutions = m_Solutions.GetCount();
	for(int i=0; i<numSolutions; ++i)
	{
		if(m_Solutions[i])
		{
			delete m_Solutions[i];
			m_Solutions[i] = NULL;
		}
	}
	m_Solutions.Reset();
}

////////////////////////////////////////////////////////////////////////////////

void crAnimOptimizer::Init(const crSkeletonData& skelData, u32 maxBlockSize, float compressionRotation, float compressionTranslation, float compressionDefault, crAnimTolerance::eCompressionCost compressionCost, crAnimTolerance::eDecompressionCost decompressionCost, const char* preCompressionRules, const char* postCompressionRules)
{
	m_SkelData = &skelData;
	m_MaxBlockSize = maxBlockSize;
	m_DefaultRotationTolerance = compressionRotation;
	m_DefaultTranslationTolerance = compressionTranslation;
	m_DefaultTolerance = compressionDefault;
	m_CompressionCost = compressionCost;
	m_DecompressionCost = decompressionCost;
	m_PreCompressionRules = preCompressionRules;
	m_PostCompressionRules = postCompressionRules;

	m_Solutions.Reset();
	m_Solutions.Grow() = rage_new Solution;
}

////////////////////////////////////////////////////////////////////////////////

void crAnimOptimizer::AddGroup(const atString& rules, float tolerance, int parentGroup)
{
	Group& g = m_Groups.Grow();

	const char* c = rules.c_str();
	while(*c)
	{
		atString& r = g.m_Rules.Grow();
		while(*c)
		{
			if(*c != ',')
			{
				r += *(c++);
			}
			else
			{
				c++;
				break;
			}
		}
	}

	g.m_ParentGroup = parentGroup;

	float accumulate = 0.f;
	int pg = parentGroup;
	while(pg >= 0)
	{
		accumulate += m_Solutions[0]->m_Tolerances[pg];
		pg = m_Groups[pg].m_ParentGroup;
	}
	tolerance = Max(tolerance-accumulate, 0.f);

	m_Solutions[0]->m_Tolerances.Grow() = tolerance;
}

////////////////////////////////////////////////////////////////////////////////

void crAnimOptimizer::AddTest(int boneIdx, float limit)
{
	Test& t = m_Tests.Grow();
	t.m_BoneIdx = boneIdx;
	t.m_Limit = limit;
}

////////////////////////////////////////////////////////////////////////////////

void crAnimOptimizer::Generate(int population, float crossover, float mutation, float minimum, float maximum, float step)
{
	int numGroups = m_Groups.GetCount();

	int numSolutions = m_Solutions.GetCount();
	for(int i=numSolutions; i<population; ++i)
	{
		m_Solutions.Grow() = rage_new Solution;
		Solution& s = *m_Solutions.back();
		
		int source = m_Random.GetRanged(0, numSolutions-1);
		for(int g=0; g<numGroups; ++g)
		{
			float error = 0.f;
			if(m_Random.GetFloat() < mutation)
			{
				error = ((m_Random.GetFloat()*2.f) - 1.f) * step;
			}

			float tolerance = Max(m_Solutions[source]->m_Tolerances[g] + error, 0.f);

			float accumulate = 0.f;
			int pg = m_Groups[g].m_ParentGroup;
			while(pg >= 0)
			{
				accumulate += s.m_Tolerances[pg];
				pg = m_Groups[pg].m_ParentGroup;
			}

			tolerance = Max(Clamp(tolerance, minimum-accumulate, maximum-accumulate), 0.f);

			s.m_Tolerances.Grow() = tolerance;
			
			if(m_Random.GetFloat() < crossover)
			{
				source = m_Random.GetRanged(0, numSolutions-1);
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

bool crAnimOptimizer::Measure(const atArray<atString>& animFilenames)
{
	crSkeleton srcSkel;
	srcSkel.Init(*m_SkelData, NULL);

	crSkeleton destSkel;
	destSkel.Init(*m_SkelData, NULL);

	crFrameData frameData;
	crFrameDataInitializerBoneAndMover initializer(*m_SkelData, false);
	initializer.InitializeFrameData(frameData);

	bool noRoot = true;
	if(noRoot)
	{
		frameData.RemoveDof(kTrackBoneTranslation, 0);
		frameData.RemoveDof(kTrackBoneRotation, 0);
	}

	crFrame frame(frameData);

	int numSolutions = m_Solutions.GetCount();
	for(int i=0; i<numSolutions; ++i)
	{
		Solution& s = *m_Solutions[i];
		if(!s.m_Size)
		{
			crAnimToleranceComplex tolerance;
			crAnimCompress::ProcessSimpleCompressionSettings(tolerance, m_DefaultTranslationTolerance, m_DefaultRotationTolerance, m_DefaultScaleTolerance, m_DefaultTolerance, m_DecompressionCost, m_CompressionCost);
			if(m_PreCompressionRules)
			{
				crAnimCompress::ProcessComplexCompressionRules(tolerance, m_PreCompressionRules, m_SkelData);
			}

			const int numGroups = m_Groups.GetCount();
			for(int g=0; g<numGroups; ++g)
			{
				const int numRules = m_Groups[g].m_Rules.GetCount();
				for(int r=0; r<numRules; ++r)
				{
					crAnimCompress::ProcessComplexCompressionIdText(tolerance, m_Groups[g].m_Rules[r].c_str(), kTrackBoneRotation, false, s.m_Tolerances[g], 0.f, crAnimTolerance::kDecompressionCostInvalid, crAnimTolerance::kCompressionCostInvalid, NULL, m_SkelData);
				}
			}

			if(m_PostCompressionRules)
			{
				crAnimCompress::ProcessComplexCompressionRules(tolerance, m_PostCompressionRules, m_SkelData);
			}

			const int numTests = m_Tests.GetCount();
			s.m_Errors.Reset();
			s.m_Errors.Resize(numTests);
			for(int t=0; t<numTests; ++t)
			{
				s.m_Errors[t] = 0.f;
			}

			atArray<float> errors2;
			errors2.Resize(numTests);

			const int numAnimations = animFilenames.GetCount();
			for(int a=0; a<numAnimations; ++a)
			{
				crAnimation srcAnim;
				if(!srcAnim.Load(animFilenames[a]))
				{
//					continue;
					return false;
				}

				crAnimation destAnim;
				if(!crAnimCompress::CompressAnimation(srcAnim, destAnim, tolerance, m_MaxBlockSize))
				{
					s.m_Size = INT_MAX;
					return false;
				}

				for(int t=0; t<numTests; ++t)
				{
					errors2[t] = 0.f;
				}

				const int numFrames = int((srcAnim.GetDuration() / m_SampleRate) + 0.5f) + 1;
				int f=0;
				for(; f<numFrames; ++f)
				{
					float time = Clamp(float(f)*m_SampleRate, 0.f, srcAnim.GetDuration());

					// pose src and dest skeletons
					srcAnim.CompositeFrame(time, frame);
					frame.Pose(srcSkel);
					srcSkel.Update();

					destAnim.CompositeFrame(time, frame);
					frame.Pose(destSkel);
					destSkel.Update();

					int t=0;
					for(; t<numTests; ++t)
					{
						Mat34V srcMtx, dstMtx;
						srcSkel.GetGlobalMtx(m_Tests[t].m_BoneIdx, srcMtx);
						destSkel.GetGlobalMtx(m_Tests[t].m_BoneIdx, dstMtx);
						Vec3V diff = dstMtx.GetCol3() - srcMtx.GetCol3();
						float mag2 = MagSquared(diff).Getf();
						errors2[t] = Max(errors2[t], mag2);

						if(mag2 > square(m_Tests[t].m_Limit*0.5f))
						{
							Displayf("FAILED solution %d/%d anim %d/%d frame %d/%d test %d/%d mag > limit %f > %f\n", i, numSolutions, a, numAnimations, f, numFrames, t, numTests, sqrt(mag2), m_Tests[t].m_Limit*0.5f);
							s.m_Size = 0;
							break;
						}
						else
						{

						}
					}
					if(t<numTests)
					{
						break;
					}
				}

				for(int t=0; t<numTests; ++t)
				{
					s.m_Errors[t] = Max(sqrt(errors2[t]), s.m_Errors[t]);
				}

				if(f<numFrames)
				{
					break;
				}

				u32 size = destAnim.ComputeSize();
				s.m_Size += size;
			}
		}
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool crAnimOptimizer::Distribute(const atArray<atString>& animFilenames, const char* distributeCmdLine, float timeout)
{
	DistributeIncredibuild distribute;

	const int numAnimations = animFilenames.GetCount();

	const int maxPacketSize = 256;
	int numPackets = (numAnimations-1) / maxPacketSize + 1;
	int packetSize = numAnimations / numPackets;

#define USE_PACKETS (1)
#if !USE_PACKETS 
	numPackets = 1;
	packetSize = numAnimations;
#endif

	const int tempBufSize = RAGE_MAX_PATH;
	char tempBuf[tempBufSize];  
	tempBuf[0] = '\0';
	if(sysGetEnv("TEMP", tempBuf, tempBufSize))
	{
		strcat(tempBuf, "\\");
	}

	for(int p=0; p<numPackets; ++p)
	{
		char listFile[RAGE_MAX_PATH];
		sprintf(listFile, "%slist_xxx_%d.animlist", tempBuf, p);

		fiSafeStream f = ASSET.Create(listFile, "animlist");
		if(f)
		{
			const int packetStart = packetSize*p;
			const int packetEnd = Min(numAnimations, packetStart+packetSize);
			for(int a=packetStart; a<packetEnd; ++a)
			{
				fprintf(f, "%s\r\n", animFilenames[a].c_str());
			}
		}
	}

	int numSolutions = m_Solutions.GetCount();
	for(int i=0; i<numSolutions; ++i)
	{
		Solution& s = *m_Solutions[i];
		if(!s.m_Size)
		{
			atString cmdLine;
			cmdLine = distributeCmdLine;

			cmdLine += " -tests ";

			const int numTests = m_Tests.GetCount();
			for(int t=0; t<numTests; ++t)
			{
				const crBoneData* bd = m_SkelData->GetBoneData(m_Tests[t].m_BoneIdx);
				if(bd)
				{
					if(t > 0)
					{
						cmdLine += ',';
					}

					char buf[256];
					sprintf(buf, "%s=%.8f", bd->GetName(), m_Tests[t].m_Limit);
					cmdLine += buf;
				}
			}

			cmdLine += " -groups ";
			PrintGroupSolution(s, cmdLine);

			for(int p=0; p<numPackets; ++p)
			{
				atString packetCmdLine;
				packetCmdLine = cmdLine;

				char listFile[RAGE_MAX_PATH];
				sprintf(listFile, "%slist_xxx_%d.animlist", tempBuf, p);

				packetCmdLine += " -animlist ";
				packetCmdLine += listFile;

				char resultsFile[RAGE_MAX_PATH];
				sprintf(resultsFile, "%sresults_xxx_%d_%d.txt", tempBuf, i, p);

				packetCmdLine += " -results ";
				packetCmdLine += resultsFile;

				distribute.Schedule(packetCmdLine.c_str());
			}
		}
	}

	const int maxNumRetries = 8;
	for(int n=0; n<maxNumRetries; ++n)
	{
		if(distribute.Process(timeout))
		{
			for(int i=0; i<numSolutions; ++i)
			{
				Solution& s = *m_Solutions[i];
				if(!s.m_Size)
				{
					for(int p=0; p<numPackets; ++p)
					{
						char resultsFile[RAGE_MAX_PATH];
						sprintf(resultsFile, "%sresults_xxx_%d_%d.txt", tempBuf, i, p);

						fiSafeStream f = ASSET.Open(resultsFile, "txt");
						if(f)
						{
							fiTokenizer T(resultsFile, f);
							int size = T.GetInt();
							if(size > 0)
							{
								s.m_Size += size;
							}
							else
							{
								s.m_Size = 0;
								break;
							}

							const int numTests = m_Tests.GetCount();
							s.m_Errors.Reset();
							s.m_Errors.Resize(numTests);

							const int numErrors = numTests;
							for(int e=0; e<numErrors; ++e)
							{
								s.m_Errors[e] = T.GetFloat();
							}
						}
					}
				}
			}

			return true;		
		}
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

void crAnimOptimizer::Select(float selection)
{
	const int numSolutions = m_Solutions.GetCount();

	for(int n=numSolutions-1; n>0; --n)
	{
		bool swap = false;
		for(int i=0; i<n; ++i)
		{
			if((m_Solutions[i]->m_Size > m_Solutions[i+1]->m_Size || !m_Solutions[i]->m_Size) && m_Solutions[i+1]->m_Size)
			{
				SwapEm(m_Solutions[i], m_Solutions[i+1]);
				swap = true;
			}
		}
		if(!swap)
		{
			break;
		}
	}

	const int numRetain = int(float(numSolutions)*selection+0.5f);
	int r=numSolutions-1;
	for(; r>0; --r)
	{
		if(r<numRetain && m_Solutions[r]->m_Size)
		{
			break;
		}

		delete m_Solutions[r];
		m_Solutions[r] = NULL;
	}
	m_Solutions.Resize(r+1);
}

////////////////////////////////////////////////////////////////////////////////

bool crAnimOptimizer::Results(const char* resultsFile)
{
	fiSafeStream f = ASSET.Create(resultsFile, "txt");
	if(f)
	{
		fiTokenizer T(resultsFile, f);
		T.Put(int(m_Solutions[0]->m_Size));

		const int numErrors = m_Solutions[0]->m_Errors.GetCount();
		for(int e=0; e<numErrors; ++e)
		{
			T.Put(m_Solutions[0]->m_Errors[e]);
		}

		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

void crAnimOptimizer::Dump(int solution) const
{
	if(solution < 0)
	{
		const int numSolutions = m_Solutions.GetCount();
		for(int i=0; i<numSolutions; ++i)
		{
			const Solution& s = *m_Solutions[i];

			Printf("[%4d] %8d", i, s.m_Size);
			
			const int numErrors = s.m_Errors.GetCount();
			for(int e=0; e<numErrors; ++e)
			{
				Printf(" %.6f", s.m_Errors[e]);
			}

			const int numGroups = m_Groups.GetCount();
			for(int g=0; g<numGroups; ++g)
			{
				Printf(" %.8f", s.m_Tolerances[g]);
			}
			Printf("\n");
		}
	}
	else
	{
		const Solution& s = *m_Solutions[solution];

		Printf("size %d\n", s.m_Size);

		const int numTests = m_Tests.GetCount();
		for(int t=0; t<numTests; ++t)
		{
			const crBoneData* bd = m_SkelData->GetBoneData(m_Tests[t].m_BoneIdx);
			float error = (t<s.m_Errors.GetCount())?s.m_Errors[t]:-1.f;
			Printf("error %s %.6f\n", bd?bd->GetName():"NULL", error);
		}

		const int numGroups = m_Groups.GetCount();
		for(int g=0; g<numGroups; ++g)
		{
			float tolerance = 0.f;
			int pg = m_Groups[g].m_ParentGroup;
			while(pg >= 0)
			{
				tolerance += s.m_Tolerances[pg];
				pg = m_Groups[pg].m_ParentGroup;
			}

			tolerance += s.m_Tolerances[g];

			const int numRules = m_Groups[g].m_Rules.GetCount();
			for(int r=0; r<numRules; ++r)
			{
				if(r > 0)
				{
					Printf(",");
				}
				Printf("%s", m_Groups[g].m_Rules[r].c_str());
			}

			Printf(" %.8f", tolerance);
			Printf("\n");
		}

		atString resumeString;
		PrintGroupSolution(s, resumeString);

		Printf("resume string \"%s\"\n", resumeString.c_str());

		// TODO --- prefix and postfix compression strings?
		Printf("compress string \"");
		for(int g=0; g<numGroups; ++g)
		{
			float tolerance = 0.f;
			int pg = m_Groups[g].m_ParentGroup;
			while(pg >= 0)
			{
				tolerance += s.m_Tolerances[pg];
				pg = m_Groups[pg].m_ParentGroup;
			}

			tolerance += s.m_Tolerances[g];

			const int numRules = m_Groups[g].m_Rules.GetCount();
			for(int r=0; r<numRules; ++r)
			{
				Printf("bonerotation,%s,%.8f;", m_Groups[g].m_Rules[r].c_str(), tolerance);
			}
		}
		Printf("\"\n");
	}
}

////////////////////////////////////////////////////////////////////////////////

void crAnimOptimizer::PrintGroupSolution(const Solution& s, atString& inoutString) const
{
	int parents[64];
	int pidx = 0;
	parents[pidx] = -1;

	const int numGroups = m_Groups.GetCount();
	for(int g=0; g<numGroups; ++g)
	{
		const int numRules = m_Groups[g].m_Rules.GetCount();
		for(int r=0; r<numRules; ++r)
		{
			if(r)
			{
				inoutString += ',';
			}
			inoutString += m_Groups[g].m_Rules[r];
		}
		inoutString += '=';

		float tolerance = s.m_Tolerances[g];
		int pg = m_Groups[g].m_ParentGroup;
		while(pg >= 0)
		{
			tolerance += s.m_Tolerances[pg];
			pg = m_Groups[pg].m_ParentGroup;
		}

		char toleranceBuf[64];
		sprintf(toleranceBuf, "%.08f", tolerance);
		inoutString += toleranceBuf;

		if(g<(numGroups-1) && m_Groups[g+1].m_ParentGroup == g)
		{
			inoutString += '(';
			parents[++pidx] = g;
		}
		else
		{
			while(pidx>0)
			{
				if(g<(numGroups-1) && parents[pidx] == m_Groups[g+1].m_ParentGroup)
				{
					break;
				}
				inoutString += ')';
				pidx--;
			}
			if(g<(numGroups-1))
			{
				inoutString += '\\';
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

