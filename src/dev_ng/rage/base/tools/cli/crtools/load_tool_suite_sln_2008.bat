@echo off

setlocal
pushd "%~dp0"

CALL setenv.bat

set BUILD_FOLDER=%RS_BUILDBRANCH%
SET MB_DEPLOY=%RS_TOOLSROOT%\dcc\current\motionbuilder2010\
SET RAGE_DIR=%RS_PROJROOT%\src\dev\rage
SET SCE_PS3_ROOT=X:\usr\local\270_001\cell

ECHO LOAD_SLN ENVIRONMENT
ECHO BUILD_FOLDER: 	%BUILD_FOLDER%
ECHO RAGE_DIR: 		%RAGE_DIR%
ECHO SCE_PS3_ROOT: 	%SCE_PS3_ROOT%
ECHO END LOAD_SLN ENVIRONMENT

start "" "C:\Program Files (x86)\Microsoft Visual Studio 9.0\Common7\IDE\devenv.exe" %cd%\anim_tool_suite_2008.sln
popd
