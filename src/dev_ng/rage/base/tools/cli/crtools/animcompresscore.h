// 
// crtools/animcompresscore.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef CRTOOLS_ANIMCOMPRESSCORE_H
#define CRTOOLS_ANIMCOMPRESSCORE_H

#include "cranimation/animtolerance.h"

namespace rage
{

class crAnimation;
class crProperties;
class crProperty;
class crSkeletonData;


namespace crAnimCompress
{

// PURPOSE: Set up a simple compression tolerance object
extern bool ProcessSimpleCompressionSettings(crAnimToleranceSimple& tolerance, float maxTranslationError=crAnimToleranceSimple::sm_DefaultMaxAbsoluteTranslationError, float maxRotationError=crAnimToleranceSimple::sm_DefaultMaxAbsoluteRotationError, float maxScaleError=crAnimToleranceSimple::sm_DefaultMaxAbsoluteScaleError, float maxDefaultError=crAnimToleranceSimple::sm_DefaultMaxAbsoluteDefaultError, crAnimTolerance::eDecompressionCost decompressionCost=crAnimTolerance::kDecompressionCostDefault, crAnimTolerance::eCompressionCost compressionCost=crAnimTolerance::kCompressionCostDefault, float minFrequency=crAnimToleranceSimple::sm_DefaultMinFrequency);

// PURPOSE: Internal step in complex compression rule processing
// NOTES: See ProcessComplexCompressionRules below for syntax of idText
extern bool ProcessComplexCompressionIdText(crAnimToleranceComplex& tolerance, const char* idText, u8 track, bool trackWildcard, float maxAbsoluteError, float minFrequency, crAnimTolerance::eDecompressionCost decompressionCost, crAnimTolerance::eCompressionCost compressionCost, const char* propertyText, const crSkeletonData* skelData);

// PURPOSE: Set up the rules on a complex compression tolerance object
// PARAMS:
// tolerance - complex compression tolerance object to add rules to
// compressionRules - compression rule string, see notes
// skelData - optional skeleton data (if not supplied, rules can't include bone names or children operator) 
// RETURNS: true - successfully converted compression rules, false - failed to convert (tolerance state undefined)
// NOTES: The compressionRules string takes the following format
// <track text>,<id text>,<compression tolerance>[,<min frequency>]; 
// They are semicolon separated, there is no limit on how many can be specified, 
// order is important (as they are evaluated in order and support things like wild cards).
// <track text> can either be a track type name (ie bonetranslation, moverrotation, blendshape etc),
// or an integer (preceeded by a #) or * for a wild card to catch all track types.
// <id text> can either be a bone name (ie thumb_l) or a partial bone name (ie thumb*, finger_?_?_r),
// or a bone id or other id (preceeded by a #) or a * for a wild card to catch all ids.
// It can be followed by a + (in the case of bone related tracks) to include all child bones in this rule.
// (ie arm*+ would catch the left and right arm, and all bones below it including hands, fingers etc). 
// <compression tolerance> is the compression tolerance to use for track/id combinations caught by this rule
// (must be >= 0.0, typical default value is 0.005).
// <min frequency> is optional minimum sampling frequency in Hz.  If present, must be > 0.0
extern bool ProcessComplexCompressionRules(crAnimToleranceComplex& tolerance, const char* compressionRules, const crSkeletonData* skelData);

// PURPOSE: Process compression settings stored within a property (or properties)
extern bool ProcessPropertySimpleCompressionSettings(crAnimToleranceSimple& tolerance, const crProperty& property);
extern bool ProcessPropertyComplexCompressionSettings(crAnimToleranceComplex& tolerance, const crProperty& property);
extern bool ProcessPropertySimpleCompressionSettings(crAnimToleranceSimple& tolerance, const crProperties& properties);
extern bool ProcessPropertyComplexCompressionSettings(crAnimToleranceComplex& tolerance, const crProperties& properties);

// PURPOSE: Set up movement compression tolerance object
extern bool ProcessMovementCompressionSettings(crAnimToleranceMovement& tolerance, float speedConstriction, float speedRelaxation, float minSpeed, float maxSpeed);

// PURPOSE: Set up bone chains on movement compression tolerance object
// PARAMS:
// tolerance - movement compression tolerance object to add bone chains to
// boneNames - list of comma separated bone names of the anchor bones for the new bone chains
// skelData - skeleton data reference
// RETURNS: true - successful, false - failed to add bone chain(s)
extern bool ProcessMovementBoneChains(crAnimToleranceMovement& tolerance, const char* boneNames, const crSkeletonData& skelData);


// PURPOSE: Calculate movement compression tolerances
// PARAMS:
// tolerance - movement compression tolerance object to configure
// srcAnim - source animation to use to calibrate tolerances
// skelData - skeleton data reference
// RETURNS: true - successful, false - failed to calculate tolerances
// NOTES: Uses settings, bone chains and analysis of source animation to
// determine variable compression tolerances based on character movement
extern bool CalcMovementCompression(crAnimToleranceMovement& tolerance, const crAnimation& srcAnim, const crSkeletonData& skelData);

// PURPOSE: Takes source animation and re-compresses it, using settings provided.
// RETURNS: true - successfully re-compressed animation, false - failed to re-compress (destAnim state undefined)
extern bool CompressAnimation(const crAnimation& srcAnim, crAnimation& destAnim, const crAnimTolerance& tolerance, u32 maxBlockSize=CR_DEFAULT_MAX_BLOCK_SIZE, bool exhaustive=false, bool pack=true);


// PURPOSE: Takes source animation and de-compresses to raw animation
// RETURNS: true - successfully de-compressed animation, false - failed to de-compress (destAnim state undefined)
extern bool DecompressAnimation(const crAnimation& srcAnim, crAnimation& destAnim);

}; // namespace crAnimCompressCore

}; // namespace rage

#endif // CRTOOLS_ANIMCOMPRESSCORE_H
