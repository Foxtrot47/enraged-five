// 
// crtools/animcmdlinehelper.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "animcmdlinehelper.h"

#include "file/asset.h"
#include "file/stream.h"
#include "file/token.h"

namespace rage
{

namespace crAnimCmdLineHelper
{

////////////////////////////////////////////////////////////////////////////////

int ProcessListFile(const char* listFile, const char** entries, int maxEntries, char* buf, int maxBufSize)
{
	Assert(entries);
	Assert(maxEntries > 0);
	Assert(buf);
	Assert(maxBufSize > 0);

	int numEntries = 0;
	char* p = buf;

	fiSafeStream f(ASSET.Open(listFile, ""));
	if(f)
	{
		fiTokenizer T(listFile, f);	

		char lineBuf[RAGE_MAX_PATH];
		while(T.GetLine(lineBuf, RAGE_MAX_PATH) > 0)
		{
			if(numEntries == maxEntries)
			{
				Warningf("crAnimCmdLineHelper::ProcessListFile - too many entries (%d) in list file '%s'", numEntries, listFile);
				return numEntries;
			}

			entries[numEntries] = p;

			p = safecpy(p, lineBuf, maxBufSize-(p-buf));
			p += strlen(lineBuf)+1;

			if(maxBufSize-(p-buf) > 0)
			{
				numEntries++;
			}
			else
			{
				Warningf("crAnimCmdLineHelper::ProcessListFile - entries too large (%d) in list file '%s'", maxBufSize, listFile);
				return numEntries;
			}
		}

		return numEntries;
	}
	else
	{
		Errorf("crAnimCmdLineHelper::ProcessListFile - failed to open list file '%s'", listFile);
		return 0;
	}
}

////////////////////////////////////////////////////////////////////////////////

/*
int StrWildCmp(const char *wild, const char *string)
{
	const char *cp = NULL, *mp = NULL;

	while ((*string) && (*wild != '*')) 
	{
		if ((*wild != *string) && (*wild != '?')) 
		{
			return 0;
		}
		wild++;
		string++;
	}

	while (*string) 
	{
		if (*wild == '*') 
		{
			if (!*++wild) 
			{
				return 1;
			}
			mp = wild;
			cp = string+1;
		} 
		else if ((*wild == *string) || (*wild == '?')) 
		{
			wild++;
			string++;
		} 
		else 
		{
			wild = mp;
			string = cp++;
		}
	}

	while (*wild == '*') 
	{
		wild++;
	}
	return !*wild;
}
*/
////////////////////////////////////////////////////////////////////////////////

}; // namespace crAnimCmdLineHelper

}; // namespace rage


