// 
// animeditcore.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef CRTOOLS_ANIMEDITCORE_H
#define CRTOOLS_ANIMEDITCORE_H

#include "vectormath/vec3v.h"
#include "vectormath/quatv.h"
#include "cranimation/animation.h"
#include "cranimation/animtolerance.h"
#include "cranimation/animtrack.h"
#include "cranimation/frame.h"
#include "crskeleton/skeletondata.h"

namespace rage
{

class crAnimation;
class crSkeletonData;


namespace crAnimEdit
{

struct OpData
{
	OpData() : m_NextOp(NULL) {}

	~OpData() 
	{
		if ( m_NextOp )
		{
			delete m_NextOp;
			m_NextOp = NULL;
		}
	}

	enum
	{
		kFormatFloat,
		kFormatQuaternion,
		kFormatEuler,
		kFormatVector3,
	};

	enum
	{
		kTypeCompress,
		kTypeFps,
		kTypeDuration,
		kTypeStart,
		kTypeEnd,
		kTypeCrop,
		kTypeSet,
		kTypeMove,
		kTypeCopy,
		kTypeAdd,
		kTypeRemove,
		kTypeMover,
		kTypeNormalize,
		kTypeBoneId,
		kTypeMirror,
		kTypeLooped,
		// OFFSET?
	};

	int m_Type;
	int m_Format;
	float m_Float;
	float m_Float2;
	bool m_Bool;
	Vec3V m_Vector3;
	QuatV m_Quaternion;
	int m_Int;
	u8 m_Track;
	u16 m_Id;
	u8 m_SourceTrack;
	u16 m_SourceId;
	OpData* m_NextOp;
};


extern OpData* ParseOps(const char* opText, const crSkeletonData* skelData);
extern bool ExecuteOps(OpData* ops, crAnimation*& anim, const crSkeletonData* skelData);

}; // namespace crAnimEdit

}; // namespace rage

#endif