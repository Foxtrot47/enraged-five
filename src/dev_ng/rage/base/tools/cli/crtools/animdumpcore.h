// 
// crtools/animdumpcore.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 


#ifndef CRTOOLS_ANIMDUMPCORE_H
#define CRTOOLS_ANIMDUMPCORE_H


namespace rage
{

class crAnimation;
class crSkeletonData;

namespace crAnimDump
{

// PURPOSE:
extern bool DumpAnimation(const crAnimation& anim, u8 version, bool csv, bool tracks, bool blocks, bool chunks, bool channels, bool values, bool eulers, bool memory, const crSkeletonData* skelData);


}; // namespace crAnimDumpCore

}; // namespace rage

#endif // CRTOOLS_ANIMDUMPCORE_H


