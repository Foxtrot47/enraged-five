// 
// crtools/animcombinecore.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "animcombinecore.h"

#include "cranimation/animation.h"
#include "cranimation/animtolerance.h"
#include "cranimation/frame.h"


namespace rage
{

namespace crAnimCombine
{

////////////////////////////////////////////////////////////////////////////////

bool CombineAnimation(const atArray<const crAnimation*>& srcAnims, crAnimation& destAnim, eCombineOperation op, float weight, eCombineAlignment alignment, float alignmentValue, float sampleRate)
{
    // destination can only be raw if all source animations are raw
    bool isRaw=true;
    for(int i=0; i<srcAnims.GetCount(); ++i)
    {
		isRaw &= srcAnims[i]->IsRaw();
    }

	// create destination animation:
	float duration = srcAnims[0]->GetDuration();
	int numFrames = int((duration / sampleRate)+0.5f) + 1;
	destAnim.Create(numFrames, duration, srcAnims[0]->IsLooped(), srcAnims[0]->HasMoverTracks(), crAnimation::sm_DefaultFramesPerChunk, isRaw);
	destAnim.SetProjectFlags(srcAnims[0]->GetProjectFlags());

	// sample frames:
	atArray<const crFrame*> destFrames;
	destFrames.Reserve(numFrames);
	for(int i=0; i<numFrames; ++i)
	{
		// TEMP - support multiple animations in a loop here
		int j = 1;

		// calculate times
		float destTime = Clamp(float(i)*sampleRate, 0.f, duration);
		float srcTime = 0.f;
		switch(alignment)
		{
		case kCombineAlignmentVariableTime:
			srcTime = destTime;
			break;

		case kCombineAlignmentVariablePhase:
			srcTime = srcAnims[j]->ConvertPhaseToTime(srcAnims[0]->ConvertTimeToPhase(destTime));
			break;

		case kCombineAlignmentFixedTime:
			srcTime = alignmentValue;
			break;

		case kCombineAlignmentFixedFrame:
			srcTime = srcAnims[j]->Convert30FrameToTime(alignmentValue);
			break;

		case kCombineAlignmentFixedPhase:
			srcTime = srcAnims[j]->ConvertPhaseToTime(alignmentValue);
			break;

		default:
			break;
		}

		// composite frames
		crFrame* destFrame = rage_new crFrame;
		destFrame->InitCreateAnimationDofs(*srcAnims[0]);
		destFrame->Composite(*srcAnims[0], destTime);

		crFrame srcFrame;
		srcFrame.InitCreateAnimationDofs(*srcAnims[j]);
		srcFrame.Composite(*srcAnims[j], srcTime);

		// perform operation
		switch(op)
		{
		case kCombineOperationAdd:
			destFrame->Add(weight, srcFrame);
			break;

		case kCombineOperationSubtract:
			destFrame->Subtract(weight, srcFrame);
			break;

		case kCombineOperationBlend:
			destFrame->Blend(weight, srcFrame);
			break;

		case kCombineOperationMerge:
			destFrame->Init(srcFrame, false);
			destFrame->Composite(*srcAnims[0], destTime);
			destFrame->Merge(srcFrame);
			break;

		default:
			break;
		}

		destFrames.Grow() = destFrame;
	}

	// convert frames to raw animation
	crAnimToleranceSimple tolerance(crAnimToleranceSimple::sm_DefaultMaxAbsoluteTranslationError, 
		crAnimToleranceSimple::sm_DefaultMaxAbsoluteRotationError, 
		crAnimToleranceSimple::sm_DefaultMaxAbsoluteScaleError, 
		crAnimToleranceSimple::sm_DefaultMaxAbsoluteDefaultError, 
		crAnimTolerance::kDecompressionCostMinimum, 
		crAnimTolerance::kCompressionCostMinimum);

	destAnim.CreateFromFramesFast(destFrames, tolerance);

	// shutdown:
	for(int i=0; i<destFrames.GetCount(); ++i)
	{
		if(destFrames[i])
		{
			delete destFrames[i];
		}
	}

	return true;
}


////////////////////////////////////////////////////////////////////////////////

}; // namespace crAnimCombineCore

}; // namespace rage

