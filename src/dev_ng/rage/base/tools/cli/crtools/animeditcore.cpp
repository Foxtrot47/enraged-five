// 
// animeditcore.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "file/asset.h"
#include "file/serialize.h"
#include "animeditcore.h"
#include "RsULog/ULogger.h"

namespace rage
{

namespace crAnimEdit
{

void EatWhitespace(const char*& opText, bool noEof=false)
{
	while(opText && *opText && (*opText == ' ' || *opText == '\t'))
	{
		opText++;
	}
	if(!noEof && !*opText)
	{
		opText = NULL;
	}
}

bool ParseChar(const char*& opText, char ch, bool optional=false)
{
	EatWhitespace(opText, optional);
	if(opText)
	{
		if(*opText == ch)
		{
			opText++;
			return true;
		}
	}

	if(!optional)
	{
		ULOGGER.SetProgressMessage("ERROR: ParseChar - failed to find '%c'", ch);
		Errorf("ParseChar - failed to find '%c'", ch);
		opText = NULL;
	}
	return false;
}

bool ParseInt(const char*& opText, int& n)
{
	const int bufSize = 64;
	char buf[bufSize];

	EatWhitespace(opText);
	if(opText)
	{
		int i;
		for(i=0; i<bufSize; i++)
		{
			if((*opText >= '0' && *opText <= '9') || (!i && *opText == '-'))
			{
				buf[i] = *opText;
				opText++;
			}
			else
			{
				break;
			}
		}
		if(i<bufSize)
		{
			buf[i] = '\0';
			if(*buf)
			{
				n = atoi(buf);
				return true;
			}
		}
	}

	ULOGGER.SetProgressMessage("ERROR: ParseInt - failed to parse int");
	Errorf("ParseInt - failed to parse int");
	opText = NULL;
	return false;
}

bool ParseFloat(const char*& opText, float& f)
{
	const int bufSize = 64;
	char buf[bufSize];

	EatWhitespace(opText);
	if(opText)
	{
		int i;
		for(i=0; i<bufSize; i++)
		{
			if((*opText >= '0' && *opText <= '9') || (*opText == '-') || (*opText == '+') || (*opText == '.') || (i>0 && (*opText == 'e' || *opText == 'E')))
			{
				buf[i] = *opText;
				opText++;
			}
			else
			{
				break;
			}
		}

		if(i<bufSize)
		{
			buf[i] = '\0';
			if(*buf)
			{
				f = static_cast<float>(atof(buf));
				return true;
			}
		}

	}

	ULOGGER.SetProgressMessage("ERROR: ParseFloat - failed to parse float");
	Errorf("ParseFloat - failed to parse float");
	opText = NULL;
	return false;
}

bool ParseVector3(const char*& opText, Vec3V_InOut v)
{
	for(int i=0; i<3; ++i)
	{
		if(!ParseFloat(opText, v[i]))
		{
			return false;
		}

		if(i<2 && !ParseChar(opText, ','))
		{
			return false;
		}
	}

	return true;
}

bool ParseQuaternion(const char*& opText, QuatV_InOut q)
{
	float f[4];
	for(int i=0; i<4; ++i)
	{
		if(!ParseFloat(opText, f[i]))
		{
			return false;
		}

		if(i<3 && !ParseChar(opText, ','))
		{
			return false;
		}
	}

	q = QuatV(f[0], f[1], f[2], f[3]);
	q = Normalize(q);

	return true;
}

bool ParseTrack(const char*& opText, u8& track)
{
	int trackAsInt;
	if(ParseInt(opText, trackAsInt))
	{
		track = static_cast<u8>(trackAsInt);
		return true;
	}
	return false;
}

bool ParseId(const char*& opText, u16& id)
{
	int idAsInt;
	if(ParseInt(opText, idAsInt))
	{
		id = static_cast<u16>(idAsInt);
		return true;
	}
	return false;
}

bool ParseTrackAndId(const char*& opText, u8& track, u16& id)
{
	if(ParseChar(opText, '['))
	{
		if(ParseTrack(opText, track))
		{
			if(ParseChar(opText, ','))
			{
				if(ParseId(opText, id))
				{
					ParseChar(opText, ']');
					return true;
				}
			}
		}
	}
	return false;
}

bool ParseKeyword(const char*& opText, char* buf, int bufSize)
{
	EatWhitespace(opText);
	if(opText)
	{
		int i;
		for(i=0; i<bufSize; i++)
		{
			if((*opText >= 'a' && *opText <= 'z') || (*opText >= 'A' && *opText <= 'Z') || (*opText == '_') || (i>0 && (*opText >= '0' && *opText <= '9')))
			{
				buf[i] = *opText;
				opText++;
			}
			else
			{
				break;
			}
		}
		if(i<bufSize)
		{
			buf[i] = '\0';
			if(*buf)
			{
				return true;
			}
		}
	}

	ULOGGER.SetProgressMessage("ERROR: ParseKeyword - failed to parse keyword");
	Errorf("ParseKeyword - failed to parse keyword");
	opText = NULL;
	return false;
}

bool ParseBool(const char*& opText, bool& b)
{
	EatWhitespace(opText);
	if(opText)
	{
		if(*opText == '0')
		{
			b = false;
		}
		else if(*opText == '1')
		{
			b = true;
		}
		else 
		{
			const int bufSize = 64;
			char buf[bufSize];

			if(ParseKeyword(opText, buf, bufSize))
			{
				if(!stricmp("false", buf))
				{
					b = false;
				}
				else if(!stricmp("true", buf))
				{
					b = true;
				}
				else
				{
					Errorf("ERROR: ParseBool - failed to recognise bool");
					opText = false;
					return false;
				}
				return true;
			}
			return false;
		}
		opText++;
		if(!opText)
		{
			opText = NULL;
		}
		return true;
	}

	return false;
}

bool ParseFormat(const char*& opText, int& format)
{
	const int bufSize = 64;
	char buf[bufSize];

	if(ParseKeyword(opText, buf, bufSize))
	{
		if(!stricmp("float", buf))
		{
			format = OpData::kFormatFloat;
		}
		else if(!stricmp("quaternion", buf))
		{
			format = OpData::kFormatQuaternion;
		}
		else if(!stricmp("vector3", buf))
		{
			format = OpData::kFormatVector3;
		}
		else if (!stricmp("euler", buf))
		{
			format = OpData::kFormatEuler;
		}
		else
		{
			ULOGGER.SetProgressMessage("ERROR: ParseFormat - failed to recognise format");
			Errorf("ParseFormat - failed to recognise format");
			opText = NULL;
			return false;
		}
		return true;
	}

	return false;
}

bool ParseType(const char*& opText, int& type)
{
	const int bufSize = 64;
	char buf[bufSize];

	if(ParseKeyword(opText, buf, bufSize))
	{
		if(!stricmp("compress", buf))
		{
			type = OpData::kTypeCompress;
		}
		else if(!stricmp("fps", buf))
		{
			type = OpData::kTypeFps;
		}
		else if(!stricmp("duration", buf))
		{
			type = OpData::kTypeDuration;
		}
		else if(!stricmp("start", buf))
		{
			type = OpData::kTypeStart;
		}
		else if(!stricmp("end", buf))
		{
			type = OpData::kTypeEnd;
		}
		else if(!stricmp("crop", buf))
		{
			type = OpData::kTypeCrop;
		}
		else if(!stricmp("set", buf))
		{
			type = OpData::kTypeSet;
		}
		else if(!stricmp("move", buf))
		{
			type = OpData::kTypeMove;
		}
		else if(!stricmp("copy", buf))
		{
			type = OpData::kTypeCopy;
		}
		else if(!stricmp("add", buf))
		{
			type = OpData::kTypeAdd;
		}
		else if(!stricmp("remove", buf))
		{
			type = OpData::kTypeRemove;
		}
		else if(!stricmp("mover", buf))
		{
			type = OpData::kTypeMover;
		}
		else if(!stricmp("normalize", buf))
		{
			type = OpData::kTypeNormalize;
		}
		else if(!stricmp("boneid", buf))
		{
			type = OpData::kTypeBoneId;
		}
		else if(!stricmp("mirror", buf))
		{
			type = OpData::kTypeMirror;
		}
		else if (!stricmp("looped", buf))
		{
			type = OpData::kTypeLooped;
		}
		else
		{
			Errorf("ERROR: ParseType - failed to recognise type");
			opText = NULL;
			return false;
		}
		return true;
	}

	return false;
}

OpData* ParseOp(const char*& opText, const crSkeletonData* skelData)
{
	OpData* op = rage_new OpData;

	if(ParseType(opText, op->m_Type))
	{
		switch(op->m_Type)
		{
		case OpData::kTypeCompress:
		case OpData::kTypeFps:
		case OpData::kTypeDuration:
		case OpData::kTypeStart:
		case OpData::kTypeEnd:
		case OpData::kTypeCrop:
			{
				bool optional = false;
				if(op->m_Type == OpData::kTypeCompress)
				{
					optional = true;
					op->m_Float = -1.f;
				}

				bool second = false;
				if(op->m_Type == OpData::kTypeCrop)
				{
					second = true;
				}

				if(ParseChar(opText, '=', optional))
				{
					ParseFloat(opText, op->m_Float);
					if(second && ParseChar(opText, ',', false))
					{
						ParseFloat(opText, op->m_Float2);
					}
				}

			}
			break;

		case OpData::kTypeSet:
		case OpData::kTypeMove:
		case OpData::kTypeCopy:
		case OpData::kTypeAdd:
		case OpData::kTypeRemove:
			{
				if(ParseTrackAndId(opText, op->m_Track, op->m_Id))
				{
					if(op->m_Type != OpData::kTypeRemove)
					{
						if(ParseChar(opText, '='))
						{
							switch(op->m_Type)
							{
							case OpData::kTypeMove:
							case OpData::kTypeCopy:
								{
									ParseTrackAndId(opText, op->m_SourceTrack, op->m_SourceId);
								}
								break;

							case OpData::kTypeSet:
							case OpData::kTypeAdd:
								{
									if(ParseFormat(opText, op->m_Format))
									{
										op->m_Vector3 = Vec3V(V_ZERO);
										op->m_Quaternion = QuatV(V_IDENTITY);
										op->m_Float = 0.f;
										op->m_Int = 0;

										int boneIdx;
										if(skelData && skelData->ConvertBoneIdToIndex(op->m_Id, boneIdx))
										{
											switch(op->m_Track)
											{
											case kTrackBoneTranslation:
												if(op->m_Format == OpData::kFormatVector3)
												{
													const crBoneData* bd = skelData->GetBoneData(boneIdx);
													if(bd)
													{
														op->m_Vector3 = bd->GetDefaultTranslation();
													}
												}
												break;

											case kTrackBoneRotation:
												if(op->m_Format == OpData::kFormatQuaternion || op->m_Format == OpData::kFormatEuler)
												{
													const crBoneData* bd = skelData->GetBoneData(boneIdx);
													if(bd)
													{
														op->m_Quaternion = bd->GetDefaultRotation();
													}
												}
												break;

												//													case kTrackBoneScale:

											default:
												break;
											}
										}

										if(ParseChar(opText, '(', true))
										{
											switch(op->m_Format)
											{
											case OpData::kFormatVector3:
											case OpData::kFormatEuler:
												{
													Vec3V v;
													if(ParseVector3(opText, v))
													{
														switch(op->m_Format)
														{
														case OpData::kFormatVector3:
															op->m_Vector3 = v;
															break;

														case OpData::kFormatEuler:
															op->m_Quaternion = QuatVFromEulersXYZ(v);
															break;

														default:
															break;
														}
													}
												}
												break;

											case OpData::kFormatFloat:
												ParseFloat(opText, op->m_Float);
												break;

											case OpData::kFormatQuaternion:
												ParseQuaternion(opText, op->m_Quaternion);
												break;

											default:
												break;
											}

											ParseChar(opText, ')');
										}
									}
								}
								break;
							}
						}
					}
				}
			}
			break;

		case OpData::kTypeMover:
		case OpData::kTypeNormalize:
		case OpData::kTypeBoneId:
		case OpData::kTypeLooped:
			{
				if(ParseChar(opText, '='))
				{
					ParseBool(opText, op->m_Bool);
				}
			}
			break;
		}

		if(opText)
		{
			EatWhitespace(opText);
			if(opText)
			{
				if(ParseChar(opText, ';'))
				{
					EatWhitespace(opText);
					return op;
				}

				ULOGGER.SetProgressMessage("ERROR: ParseOp - unable to advance to next op");
				Errorf("ParseOp - unable to advance to next op");
				opText = NULL;
			}
			else
			{
				return op;
			}
		}
	}

	delete op;
	return NULL;
}

OpData* ParseOps(const char* opText, const crSkeletonData* skelData)
{
	OpData* head = NULL;
	OpData* tail = NULL;
	do 
	{
		OpData* op = ParseOp(opText, skelData);
		if(op)
		{
			if(!head)
			{
				head = op;
			}
			else
			{
				tail->m_NextOp = op;
			}
			tail = op;
		}
		else 
		{
			delete head;
			return NULL;
		}
	} 
	while(opText);

	return head;
}

bool ExecuteOp(OpData* op, crAnimation*& anim, const crSkeletonData* skelData)
{
	switch(op->m_Type)
	{
	case OpData::kTypeFps:
		{
			float duration = float(anim->GetNumInternalFrames() - 1) / op->m_Float;
			anim->SetDuration(duration);
		}
		break;

	case OpData::kTypeDuration:
		{
			anim->SetDuration(op->m_Float);
		}
		break;

	case OpData::kTypeCompress:
		{
			crAnimation* newAnim = rage_new crAnimation;
			newAnim->Create(
				anim->GetNumInternalFrames(),
				anim->GetDuration(),
				anim->IsLooped(),
				anim->HasMoverTracks(),
				crAnimation::sm_DefaultFramesPerChunk,  // Switch to anim->GetNumInternalFramesPerChunk() and test
				false );

			crAnimToleranceSimple tolerance(
				(op->m_Float>=0.f)?op->m_Float:crAnimToleranceSimple::sm_DefaultMaxAbsoluteTranslationError,
				(op->m_Float>=0.f)?op->m_Float:crAnimToleranceSimple::sm_DefaultMaxAbsoluteRotationError,
				(op->m_Float>=0.f)?op->m_Float:crAnimToleranceSimple::sm_DefaultMaxAbsoluteScaleError,
				(op->m_Float>=0.f)?op->m_Float:crAnimToleranceSimple::sm_DefaultMaxAbsoluteDefaultError,
				crAnimTolerance::kDecompressionCostMaximum, crAnimTolerance::kCompressionCostMaximum);

			const int numFrames = anim->GetNumInternalFrames();
			atArray<QuatV> aq(numFrames,numFrames);
			atArray<Vec3V> av(numFrames,numFrames);
			atArray<float> af(numFrames,numFrames);
			atArray<int> ai(numFrames,numFrames);

			for(u32 i=0; i<anim->GetNumTracks(); i++)
			{
				crAnimTrack* track = anim->GetTrack(i);
				crAnimTrack* newTrack = rage_new crAnimTrack(track->GetTrack(), track->GetId());

				switch(track->GetType())
				{
				case kFormatTypeQuaternion:
					for(int j=0; j<numFrames; j++)
						track->EvaluateQuaternion(float(j), aq[j]);
					newTrack->CreateQuaternion(aq, tolerance);
					break;
				case kFormatTypeVector3:
					for(int j=0; j<numFrames; j++)
						track->EvaluateVector3(float(j), av[j]);
					newTrack->CreateVector3(av, tolerance);
					break;
				case kFormatTypeFloat:
					for(int j=0; j<numFrames; j++)
						track->EvaluateFloat(float(j), af[j]);
					newTrack->CreateFloat(af, tolerance);
					break;
				default:
					Assert(0);
				}

				if(!newAnim->CreateTrack(newTrack))
				{
					delete newTrack;
					delete newAnim;
					return false;
				}
			}

			delete anim;
			anim = newAnim;
		}
		break;

	case OpData::kTypeStart:
	case OpData::kTypeEnd:
	case OpData::kTypeCrop:
		{
			float start = 0.f;
			float end = anim->GetDuration();

			switch(op->m_Type)
			{
			case OpData::kTypeStart:
				start = Clamp(op->m_Float, start, end);
				break;
			case OpData::kTypeEnd:
				end = Clamp(op->m_Float, start, end);
				break;
			case OpData::kTypeCrop:
				start = Clamp(op->m_Float, start, end);
				end = Clamp(op->m_Float2, start, end);
				break;
			}

			const float duration = end - start;
			const u32 numFrames = 1 + u32(0.5f + duration * float(anim->GetNumInternalFrames()-1) / anim->GetDuration());
			const float startFrame = start * float(anim->GetNumInternalFrames()-1) / anim->GetDuration();

			crAnimation* newAnim = rage_new crAnimation;
			newAnim->Create(
				numFrames,
				duration,
				anim->IsLooped(),
				anim->HasMoverTracks(),
				crAnimation::sm_DefaultFramesPerChunk,  // why doesn't crop work if it is anim->GetNumInternalFramesPerChunk()?
				anim->IsRaw() );

			atArray<QuatV> aq(numFrames,numFrames);
			atArray<Vec3V> av(numFrames,numFrames);
			atArray<float> af(numFrames,numFrames);
			atArray<int> ai(numFrames,numFrames);

			for(u32 i=0; i<anim->GetNumTracks(); i++)
			{
				crAnimTrack* track = anim->GetTrack(i);
				crAnimTrack* newTrack = rage_new crAnimTrack(track->GetTrack(), track->GetId());

				switch(track->GetType())
				{
				case kFormatTypeQuaternion:
					for(u32 j=0; j<numFrames; j++)
						track->EvaluateQuaternion(Min<float>(float(j)+startFrame, float(anim->GetNumInternalFrames())), aq[j]);
					newTrack->CreateQuaternion(aq, LOSSLESS_ANIM_TOLERANCE);
					break;
				case kFormatTypeVector3:
					for(u32 j=0; j<numFrames; j++)
						track->EvaluateVector3(Min<float>(float(j)+startFrame, float(anim->GetNumInternalFrames())), av[j]);
					newTrack->CreateVector3(av, LOSSLESS_ANIM_TOLERANCE);
					break;
				case kFormatTypeFloat:
					for(u32 j=0; j<numFrames; j++)
						track->EvaluateFloat(Min<float>(float(j)+startFrame, float(anim->GetNumInternalFrames())), af[j]);
					newTrack->CreateFloat(af, LOSSLESS_ANIM_TOLERANCE);
					break;
				default:
					Assert(0);
				}

				if(!newAnim->CreateTrack(newTrack))
				{
					delete newTrack;
					delete newAnim;
					return false;
				}
			}

			delete anim;
			anim = newAnim;
		}
		break;

	case OpData::kTypeMirror:
		{
			if(!skelData)
			{
				ULOGGER.SetProgressMessage("ERROR: Skeleton required for mirror operation");
				Quitf("Skeleton required for mirror operation");
			}

			const u32 numFrames = anim->GetNumInternalFrames();

			atArray<crFrame> frames;
			frames.Resize(numFrames);

			atArray<const crFrame*> framePtrs;
			framePtrs.Resize(numFrames);

			for(u32 i=0; i<numFrames; ++i)
			{
				float time = anim->GetDuration() * float(i)/float(numFrames-1);

				frames[i].InitCreateAnimationDofs(*anim);
				frames[i].Composite(*anim, time);

				Assert(skelData);
				frames[i].Mirror(*skelData);

				framePtrs[i] = &frames[i];
			}

			crAnimation* newAnim = rage_new crAnimation;
			newAnim->Create(
				anim->GetNumInternalFrames(),
				anim->GetDuration(),
				anim->IsLooped(),
				anim->HasMoverTracks(),
				crAnimation::sm_DefaultFramesPerChunk,  // Switch to anim->GetNumInternalFramesPerChunk() and test
				anim->IsRaw() );

			newAnim->CreateFromFramesFast(framePtrs, LOSSLESS_ANIM_TOLERANCE);

			delete anim;
			anim = newAnim;
		}
		break;

	case OpData::kTypeLooped:
		{
			anim->SetLooped(op->m_Bool);
		}
		break;

	case OpData::kTypeMover:
	case OpData::kTypeNormalize:
	case OpData::kTypeBoneId:
		ULOGGER.SetProgressMessage("ERROR: ExecuteOp - type not yet implemented");
		Errorf("ExecuteOp - type not yet implemented");
		break;

	case OpData::kTypeRemove:
	case OpData::kTypeSet:
	case OpData::kTypeAdd:
	case OpData::kTypeMove:
	case OpData::kTypeCopy:
		{
			crAnimation* newAnim = rage_new crAnimation;
			newAnim->Create(
				anim->GetNumInternalFrames(),
				anim->GetDuration(),
				anim->IsLooped(),
				anim->HasMoverTracks(),
				anim->GetNumInternalFramesPerChunk(), 
				anim->IsRaw() );

			for(u32 i=0; i<anim->GetNumTracks(); i++)
			{
				crAnimTrack* track = anim->GetTrack(i);
				Assert(track->GetNumInternalFramesPerChunk() == anim->GetNumInternalFramesPerChunk());

				u8 trk = (op->m_Type==OpData::kTypeMove||op->m_Type==OpData::kTypeCopy)?op->m_SourceTrack:op->m_Track;
				u16 id = (op->m_Type==OpData::kTypeMove||op->m_Type==OpData::kTypeCopy)?op->m_SourceId:op->m_Id;

				if(track->TestTrackAndId(trk, id))
				{
					switch(op->m_Type)
					{
					case OpData::kTypeAdd:
						ULOGGER.SetProgressMessage("WARNING: Attempting to add track %d id %d, but already present... overwriting", int(trk), int(id));
						Warningf("Attempting to add track %d id %d, but already present... overwriting", int(trk), int(id));
						// break deliberately omitted
					case OpData::kTypeSet:
					case OpData::kTypeRemove:
						break;

					case OpData::kTypeCopy:
						{
							crAnimTrack* newTrack = track->Clone();
							newAnim->CreateTrack(newTrack);
						}
						// break deliberately omitted
					case OpData::kTypeMove:
						{
							u32 numFrames = anim->GetNumInternalFrames();

							crAnimTrack* newTrack = rage_new crAnimTrack(track->GetTrack(), track->GetId());
							switch(track->GetType())
							{
							case kFormatTypeQuaternion:
								{
									atArray<QuatV> aq(numFrames, numFrames);
									for(u32 j=0; j<numFrames; j++)
										track->EvaluateQuaternion(float(j), aq[j]);
									newTrack->CreateQuaternion(aq, LOSSLESS_ANIM_TOLERANCE, anim->GetNumInternalFramesPerChunk());
								}
								break;
							case kFormatTypeVector3:
								{
									atArray<Vec3V> av(numFrames, numFrames);
									for(u32 j=0; j<numFrames; j++)
										track->EvaluateVector3(float(j), av[j]);
									newTrack->CreateVector3(av, LOSSLESS_ANIM_TOLERANCE, anim->GetNumInternalFramesPerChunk());
								}
								break;
							case kFormatTypeFloat:
								{
									atArray<float> af(numFrames, numFrames);
									for(u32 j=0; j<numFrames; j++)
										track->EvaluateFloat(float(j), af[j]);
									newTrack->CreateFloat(af, LOSSLESS_ANIM_TOLERANCE, anim->GetNumInternalFramesPerChunk());
								}
								break;
							default:
								Assert(0);
							}
							newAnim->CreateTrack(newTrack);
						}
						break;
					}
				}
				else
				{
					crAnimTrack* newTrack = track->Clone();
					newAnim->CreateTrack(newTrack);
				}
			}

			switch(op->m_Type)
			{
			case OpData::kTypeAdd:
			case OpData::kTypeSet:
				{
					const u32 numFrames = anim->GetNumInternalFrames();

					crAnimTrack* newTrack = rage_new crAnimTrack(op->m_Track, op->m_Id);
					switch(op->m_Format)
					{
					case OpData::kFormatQuaternion:
					case OpData::kFormatEuler:
						{
							atArray<QuatV> aq(numFrames, numFrames);
							for(u32 j=0; j<numFrames; j++)
								aq[j] = op->m_Quaternion;
							newTrack->CreateQuaternion(aq, LOSSLESS_ANIM_TOLERANCE, anim->GetNumInternalFramesPerChunk());
						}
						break;
					case OpData::kFormatVector3:
						{
							atArray<Vec3V> av(numFrames, numFrames);
							for(u32 j=0; j<numFrames; j++)
								av[j] = op->m_Vector3;
							newTrack->CreateVector3(av, LOSSLESS_ANIM_TOLERANCE, anim->GetNumInternalFramesPerChunk());
						}
						break;
					case OpData::kFormatFloat:
						{
							atArray<float> af(numFrames, numFrames);
							for(u32 j=0; j<numFrames; j++)
								af[j] = op->m_Float;
							newTrack->CreateFloat(af, LOSSLESS_ANIM_TOLERANCE, anim->GetNumInternalFramesPerChunk());
						}
						break;

					default:
						Assert(0);
					}
					newAnim->CreateTrack(newTrack);
				}
				break;

			default:
				break;
			}

			delete anim;
			anim = newAnim;
		}
		break;
	}

	return true;
}

bool ExecuteOps(OpData* ops, crAnimation*& anim, const crSkeletonData* skelData)
{
	while(ops && ExecuteOp(ops, anim, skelData))
	{
		ops = ops->m_NextOp;		
	}

	return ops == NULL;
}

}; // namespace crAnimEdit

}; // namespace rage
