import os
import os.path
import sys
import re

if len(sys.argv) != 3:
    print "Usage: " + os.path.basename(sys.argv[0]) + " [input name] [output name]"
    sys.exit(1)

class Mask(object):
    def __init__(self, name=None, mirrored=None, index=0, rawName=None):
        self.name = name
        self.mirrored = mirrored
        self.index = index
        self.rawName = rawName
        
maskList = list()
orderFileName = sys.argv[1]
try:
    orderFile = open(orderFileName, "r")
except:
    print "Error: Could not read order file: " + orderFileName
    sys.exit(1)
    
for line in orderFile.readlines():
    line = line.lower()
    line = line.lstrip("\t ")
    line = line.rstrip("\n\t ")
    pattern = re.compile(r"^([a-z0-9_]+?)(_mirrored|_original)_wm(\d+)(.*)")
    match = pattern.match(line)
    if match == None or match.groups()[3] != '':
        print "Error: Invalid channel name: " + line
        sys.exit(1)
    else:
        maskList.append(Mask(match.groups()[0], match.groups()[1] == "_mirrored", int(match.groups()[2]), line))

orderFile.close()

def MaskCompare(x, y):
    if x.mirrored and not y.mirrored:
        return 1
    elif not x.mirrored and y.mirrored:
        return -1
    elif x.index > y.index:
        return 1
    elif x.name < y.name:
        return -1
    else:
        return 1
maskList.sort(MaskCompare)

textureList = list()
for mask in maskList:
    if mask.mirrored:
        found = False
        for searchMask in maskList:
            if searchMask.name == mask.name and not searchMask.mirrored:
                found = True
                break
        if found == False:
            print "Error: Cannot find channel: " + mask.name
            sys.exit(1)
    else:
        textureFileName = mask.name + ".tif"
        found = False
        for searchTextureFileName in textureList:
            if searchTextureFileName == textureFileName:
                found = True
                break
        if found == False:
            if os.path.exists(textureFileName):
                textureList.append(textureFileName)
            else:
                print "Error: Cannot open mask texture: " + textureFileName
                sys.exit(1)

numTextures = len(textureList)
if numTextures % 4 == 1:
    print "Warning: You have wasted an entire packed texture for a single mask"
    
numPackedTextures = (numTextures + 3) / 4
kBlankTextureFileName = "??ANM_BLANK??"
while numPackedTextures != (len(textureList) / 4):
    textureList.append(kBlankTextureFileName)
print "Info: Creating " + str(numPackedTextures) + " packed textures"

kChannels = ['r', 'g', 'b', 'a']
for i in range(0, numPackedTextures):
    packSystemCommand = "rageTextureConvert -f=A4R4G4B4 -mergeformat=\""

    channelCount = 0
    for j in range(0, 4):
        if textureList[i*4+j] != kBlankTextureFileName:
            packSystemCommand += str(channelCount) + ":b~" + kChannels[j] + ","
            channelCount += 1
    if channelCount == 0:
        continue

    packSystemCommand = packSystemCommand.rstrip(',')
    packSystemCommand += "\" -mergetexs="

    for j in range(0, 4):
        if textureList[i*4+j] != kBlankTextureFileName:
            packSystemCommand += textureList[i*4+j] + ","

    packSystemCommand = packSystemCommand.rstrip(',')
    packSystemCommand += " -out=maskMap_0" + str(i + 1) + ".dds"

    print packSystemCommand
    if os.system(packSystemCommand) != 0:
        sys.exit(1)

remapOrderFileName = sys.argv[2]
try:
    remapOrderFile = open(remapOrderFileName, "w")
    for mask in maskList:
        remapOrderFile.write(mask.rawName + "\n")
    remapOrderFile.close()
except:
    print "Error: Could not write remap file: " + remapOrderFileName
    sys.exit(1)