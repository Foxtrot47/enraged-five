// 
// /meshinfo.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "parser/manager.h"
#include "system/main.h"
#include "system/param.h"
#include "mesh/mesh.h"
#include "mesh/serialize.h"

FPARAM(1, in, "File to get info for");
PARAM(ranges, "Print info about data ranges");
PARAM(materials, "Print info about materials");

using namespace rage;

int Main()
{
	INIT_PARSER;

	const char* in = NULL;
	PARAM_in.Get(in);
	Assert(in);

	mshMesh* mesh = rage_new mshMesh;
	if ( SerializeFromFile(in, *mesh, "mesh") == false ) 
	{
		Errorf("Can't load mesh file %s", in);
		return 1;
	}

	Printf("Materials: %d\n", mesh->GetMtlCount());
	Printf("Triangles: %d\n", mesh->GetTriangleCount());
	Printf("Verts: %d\n", mesh->GetVertexCount());
	Printf("Skinned: %s\n", mesh->IsSkinned() ? "true" : "false");
	Printf("Matrices: %d\n", mesh->GetMatrixCount());
	Printf("BlindDataChannels: %d\n", mesh->GetNumFloatBlindChannels());
	Printf("BlendTargetCount: %d\n", mesh->GetBlendTargetCount());

	if (PARAM_materials.Get())
	{
		Printf("Materials:\n");
		for(int i = 0; i < mesh->GetMtlCount(); i++)
		{
			Printf("  Material %d:\n", i);
			mshMaterial& mtl = mesh->GetMtl(i);
			Printf("    Name: %s\n", mtl.Name.m_String);
			Printf("    Primitives: %d\n", mtl.Prim.GetCount());
			Printf("    Verts: %d\n", mtl.GetVertexCount());
			Printf("    Channels: %d\n", mtl.GetChannelCount());
			Printf("    TexureSets: %d\n", mtl.TexSetCount);
			Printf("	TanBiCount: %d\n", mtl.TanBiSetCount);
			Printf("	Priority: %d\n", mtl.Priority);
		}
	}

	if (PARAM_ranges.Get() && mesh->GetMtlCount() > 0)
	{
		Printf("Ranges:\n");
		for(int i = 0; i < mesh->GetMtlCount(); i++)
		{
			Printf("  Material %d:\n", i);

			mshMaterial& mtl = mesh->GetMtl(i);

			mshVertex min = mtl.GetVertex(0);
			mshVertex max = mtl.GetVertex(0);

			for(int j = 0; j < mtl.GetVertexCount(); j++)
			{
				const mshVertex& vert = mtl.GetVertex(j);

				min.Pos.Min(min.Pos, vert.Pos);
				min.Cpv.Min(min.Cpv, vert.Cpv);
				min.Cpv2.Min(min.Cpv2, vert.Cpv2);
				min.Cpv3.Min(min.Cpv3, vert.Cpv3);
				min.Nrm.Min(min.Nrm, vert.Nrm);
				for(int tc = 0; tc < mtl.TexSetCount; ++tc)
				{
					min.Tex[tc].x = Min(min.Tex[tc].x, vert.Tex[tc].x);
					min.Tex[tc].y = Min(min.Tex[tc].y, vert.Tex[tc].y);
				}
				for(int tbc = 0; tbc < mtl.TanBiSetCount; ++tbc)
				{
					min.TanBi[tbc].T.Min(min.TanBi[tbc].T, vert.TanBi[tbc].T);
					min.TanBi[tbc].B.Min(min.TanBi[tbc].B, vert.TanBi[tbc].B);
				}

				max.Pos.Max(max.Pos, vert.Pos);
				max.Cpv.Max(max.Cpv, vert.Cpv);
				max.Cpv2.Max(max.Cpv2, vert.Cpv2);
				max.Cpv3.Max(max.Cpv3, vert.Cpv3);
				max.Nrm.Max(max.Nrm, vert.Nrm);
				for(int tc = 0; tc < mtl.TexSetCount; ++tc)
				{
					max.Tex[tc].x = Max(max.Tex[tc].x, vert.Tex[tc].x);
					max.Tex[tc].y = Max(max.Tex[tc].y, vert.Tex[tc].y);
				}
				for(int tbc = 0; tbc < mtl.TanBiSetCount; ++tbc)
				{
					max.TanBi[tbc].T.Max(max.TanBi[tbc].T, vert.TanBi[tbc].T);
					max.TanBi[tbc].B.Max(max.TanBi[tbc].B, vert.TanBi[tbc].B);
				}
			}

			Printf("    Pos: ( %f %f %f ), ( %f %f %f )\n", min.Pos.x, min.Pos.y, min.Pos.z, max.Pos.x, max.Pos.y, max.Pos.z);
			Printf("    Nrm: ( %f %f %f ), ( %f %f %f )\n", min.Nrm.x, min.Nrm.y, min.Nrm.z, max.Nrm.x, max.Nrm.y, max.Nrm.z);
			Printf("    Cpv: ( %f %f %f %f ), ( %f %f %f %f )\n", min.Cpv.x, min.Cpv.y, min.Cpv.z, min.Cpv.w, max.Cpv.x, max.Cpv.y, max.Cpv.z, max.Cpv.w);
			Printf("    Cpv2: ( %f %f %f %f ), ( %f %f %f %f )\n", min.Cpv2.x, min.Cpv2.y, min.Cpv2.z, min.Cpv2.w, max.Cpv2.x, max.Cpv2.y, max.Cpv2.z, max.Cpv2.w);
			Printf("    Cpv3: ( %f %f %f %f ), ( %f %f %f %f )\n", min.Cpv3.x, min.Cpv3.y, min.Cpv3.z, min.Cpv3.w, max.Cpv3.x, max.Cpv3.y, max.Cpv3.z, max.Cpv3.w);
			for(int tc = 0; tc < mtl.TexSetCount; ++tc)
			{
				Printf("    UV%d: ( %f %f ), ( %f %f )\n", tc, min.Tex[tc].x, min.Tex[tc].y, max.Tex[tc].x, max.Tex[tc].y);
			}
			for(int tbc = 0; tbc < mtl.TanBiSetCount; ++tbc)
			{
				Printf("    Tangent%d: ( %f %f %f ), ( %f %f %f )\n", tbc, min.TanBi[tbc].T.x, min.TanBi[tbc].T.y, min.TanBi[tbc].T.z, max.TanBi[tbc].T.x, max.TanBi[tbc].T.y, max.TanBi[tbc].T.z);
				Printf("    Binormal%d: ( %f %f %f ), ( %f %f %f )\n", tbc, min.TanBi[tbc].B.x, min.TanBi[tbc].B.y, min.TanBi[tbc].B.z, max.TanBi[tbc].B.x, max.TanBi[tbc].B.y, max.TanBi[tbc].B.z);
			}

		}
	}

	delete mesh;

	SHUTDOWN_PARSER;

	return 0;
}
