// 
// meshtools/meshtodirectx.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "system/main.h"

#include "atl/array.h"
#include "file/asset.h"
#include "grcore/config.h"
#include "grcore/device.h"
#include "mesh/mesh.h"
#include "mesh/serialize.h"
#include "system/param.h"

#if !__D3D
#error "This tester only compiles as a Direct3D application."
#else
#include "system/xtl.h"
#include <d3d9.h>
#include <d3dx9mesh.h>
#if !__OPTIMIZED
#pragma comment(lib,"d3dx9d.lib")
#pragma comment(lib,"d3d9.lib")
#else
#pragma comment(lib,"d3dx9.lib")
#pragma comment(lib,"d3d9.lib")
#endif
#endif


using namespace rage;

void Help() {
	Displayf("meshtodirectx -input inputPath -output outputPath [-ascii]");
}

#if __D3D	
namespace rage
{
IDirect3DDevice9* CreateNULLRefDevice()
{
	HRESULT hr;
	IDirect3D9* pD3D = Direct3DCreate9( D3D_SDK_VERSION );
	if( NULL == pD3D )
		return NULL;

	D3DDISPLAYMODE Mode;
	pD3D->GetAdapterDisplayMode(0, &Mode);

	D3DPRESENT_PARAMETERS pp;
	ZeroMemory( &pp, sizeof(D3DPRESENT_PARAMETERS) ); 
	pp.BackBufferWidth  = 1;
	pp.BackBufferHeight = 1;
	pp.BackBufferFormat = Mode.Format;
	pp.BackBufferCount  = 1;
	pp.SwapEffect       = D3DSWAPEFFECT_COPY;
	pp.Windowed         = TRUE;

	IDirect3DDevice9* pd3dDevice;
	hr = pD3D->CreateDevice( D3DADAPTER_DEFAULT, D3DDEVTYPE_NULLREF, GetConsoleWindow(), 
		D3DCREATE_HARDWARE_VERTEXPROCESSING, &pp, &pd3dDevice );
	//SAFE_RELEASE( pD3D );
	if( FAILED(hr) || pd3dDevice == NULL )
		return NULL;

	return pd3dDevice;
}

LPD3DXMESH CreateD3DXMesh(const mshMaterial& mtl,grcDeviceHandle* device)
{
	LPD3DXMESH theMesh;

	// create the mesh that we'll fill with copied data:
	DWORD options=D3DXMESH_MANAGED|D3DXMESH_32BIT;

	// Vertex declaration
	D3DVERTEXELEMENT9 VERTEX_DECL[] =
	{
		{ 0,  0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT,  D3DDECLUSAGE_POSITION, 0}, 
		{ 0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT,  D3DDECLUSAGE_NORMAL,   0}, 
		{ 0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT,  D3DDECLUSAGE_TEXCOORD, 0},
		D3DDECL_END()
	};

	// Vertex format
	struct VERTEX
	{
		D3DXVECTOR3 m_Position;
		D3DXVECTOR3 m_Normal;
		D3DXVECTOR2 m_Texcoord;
	};

	// count the # of dengenerate triangles:
	int numDegenerates=0;
	for (int i=0;i<mtl.Prim.GetCount();i++)
	{
		const mshPrimitive& prim=mtl.Prim[i];
		if (prim.Type==mshTRISTRIP || prim.Type==mshTRISTRIP2)
		{
			int primIndex=0;
			for (int j=0;j<prim.Idx.GetCount()-2;j++)
			{
				int index1=prim.Idx[primIndex];
				int index2=prim.Idx[primIndex+1];
				int index3=prim.Idx[primIndex+2];
				if (index1==index2 || index2==index3 || index1==index3)
				{
					numDegenerates++;
				}
				primIndex++;
			}
		}
	}

	D3DXCreateMesh(mtl.GetTriangleCount()-numDegenerates,mtl.GetVertexCount(),options,VERTEX_DECL,device,&theMesh);

	// copy the vertex buffer:
	VERTEX* dataTo;
	theMesh->LockVertexBuffer(0x0,(LPVOID*)&dataTo);
	int vertCount=mtl.GetVertexCount();
	for (int i=0;i<vertCount;i++)
	{
		dataTo[i].m_Position.x=mtl.GetPos(i).x;
		dataTo[i].m_Position.y=mtl.GetPos(i).y;
		dataTo[i].m_Position.z=mtl.GetPos(i).z;

		Vector3 nrm=mtl.GetNormal(i);
		nrm.Normalize();

		// HACK: get rid of zero length normals:
		if (nrm.Mag2()<0.9f)
			nrm.Set(0.0f,1.0f,0.0f);
			
		dataTo[i].m_Normal.x=nrm.x;
		dataTo[i].m_Normal.y=nrm.y;
		dataTo[i].m_Normal.z=nrm.z;

		dataTo[i].m_Texcoord.x=mtl.GetTexCoord(i,0).x;
		dataTo[i].m_Texcoord.y=mtl.GetTexCoord(i,0).y;
	}
	theMesh->UnlockVertexBuffer();

	u32* dataToIdx;

	// copy the index buffer:
	theMesh->LockIndexBuffer(0x0,(LPVOID*)&dataToIdx);
	int index=0;
	for (int i=0;i<mtl.Prim.GetCount();i++)
	{
		const mshPrimitive& prim=mtl.Prim[i];
		if (prim.Type==mshTRISTRIP || prim.Type==mshTRISTRIP2)
		{

			bool switchOrder=prim.Type==mshTRISTRIP;
			int primIndex=0;
			for (int j=0;j<prim.Idx.GetCount()-2;j++)
			{
				int index1=prim.Idx[primIndex];
				int index2=prim.Idx[primIndex+1];
				int index3=prim.Idx[primIndex+2];
				if (index1==index2 || index2==index3 || index1==index3)
				{
					// skip degenerate triangles!
				}
				else if (switchOrder)
				{
					dataToIdx[index++]=index1;
					dataToIdx[index++]=index2;
					dataToIdx[index++]=index3;
				}
				else 
				{
					dataToIdx[index++]=index3;
					dataToIdx[index++]=index2;
					dataToIdx[index++]=index1;
				}
				switchOrder=!switchOrder;
				primIndex++;
			}
		}
		else
		{
			Errorf("Primitive Type is not supported!");
			return NULL;
		}
		
	}

	//Assert(index/3==mtl.GetTriangleCount());
	theMesh->UnlockIndexBuffer();

	return theMesh;
}
} // namespace rage


PARAM(input, "the path of the  .mesh input file.");
PARAM(output,"the path of the .x output file.");
PARAM(ascii,".x file is exported as an ascii file.");


int Main() 
{
	// create a NULL D3D device:
	IDirect3DDevice9* device=CreateNULLRefDevice();
	if (device==NULL)
		Quitf("creating NULL D3D device failed!");
	//GRCDEVICE.SetCurrent(device);

	const char* inputPath;
	const char* outputPath;

	PARAM_input.Get(inputPath);
	PARAM_output.Get(outputPath);
	if (!inputPath || !outputPath)
	{
		Help();
		return -1;
	}

	mshMesh &mesh = *rage_new mshMesh;
	bool success = SerializeFromFile(inputPath,mesh,"mesh");
	if (!success)
	{
		Quitf( "Couldn't load file '%s%s'!", ASSET.GetPath(), inputPath );
	}

	Displayf("creating meshes from geometries...");

	// setup attributes (basically per polygon material index):
	D3DXMATERIAL* materials=rage_new D3DXMATERIAL[mesh.GetMtlCount()];

	// added the geometries as meshes:
	LPD3DXMESH* geomMeshes=Alloca(LPD3DXMESH,mesh.GetMtlCount());
	LPD3DXMESH newMesh=NULL;
	for (int i=0;i<mesh.GetMtlCount();i++)
	{
		geomMeshes[i]=CreateD3DXMesh(mesh.GetMtl(i),device);

		int triCount=geomMeshes[i]->GetNumFaces();
		DWORD* attributeArray=rage_new DWORD[triCount];
		for (int j=0;j<triCount;j++)
			attributeArray[j]=i;
		
		materials[i].MatD3D.Ambient.r=materials[i].MatD3D.Ambient.g=materials[i].MatD3D.Ambient.b=0.0f;
		materials[i].MatD3D.Diffuse.r=materials[i].MatD3D.Diffuse.g=materials[i].MatD3D.Diffuse.b=1.0f;
		materials[i].MatD3D.Emissive.r=materials[i].MatD3D.Emissive.g=materials[i].MatD3D.Emissive.b=0.0f;
		materials[i].MatD3D.Specular.r=materials[i].MatD3D.Specular.g=materials[i].MatD3D.Specular.b=0.0f;
		materials[i].MatD3D.Power=0.0f;
		materials[i].pTextureFilename="";

/*
		DWORD* attributeData;
		geomMeshes[i]->LockAttributeBuffer(0x0,&attributeData);
		memcpy(attributeData,attributeArray,sizeof(DWORD)*triCount);
		geomMeshes[i]->UnlockAttributeBuffer();
		delete [] attributeArray;
*/	
	}


	Displayf("combining meshes...");
	LPD3DXMESH combinedMesh;
	
	D3DXConcatenateMeshes(geomMeshes,mesh.GetMtlCount(),D3DXMESH_32BIT,NULL,NULL,NULL,device,&combinedMesh);
	DWORD* aAdjacency = rage_new DWORD[combinedMesh->GetNumFaces() * 3]; 
	combinedMesh->GenerateAdjacency(1e-6f,aAdjacency);
	DWORD* aAdjacencyOut = rage_new DWORD[combinedMesh->GetNumFaces() * 3]; 
	newMesh=NULL;
	D3DXCleanMesh(D3DXCLEAN_BOWTIES,combinedMesh,aAdjacency,&newMesh,aAdjacencyOut,NULL);
	if (newMesh!=NULL)
	{
		combinedMesh=newMesh;
	}
	else
		Quitf("D3DXCleanMesh() failed!");

	Displayf("saving the DirectX .x file '%s'...",outputPath);
	HRESULT result=D3DXSaveMeshToX(outputPath,combinedMesh,NULL,materials,NULL,mesh.GetMtlCount(),PARAM_ascii.Get()?D3DXF_FILEFORMAT_TEXT:D3DXF_FILEFORMAT_BINARY);
	if (result!=S_OK)
		Quitf("Couldn't save to '%s'",outputPath);

	return 0;
}

#endif
