#include "system/main.h"

#include "mesh/mesh.h"
#include "mesh/serialize.h"
#include "file/asset.h"
#include "system/param.h"

using namespace rage;

void help() {
	Displayf("process [-tristrip] [-tangents] [-merge] [-ascii | -binary] file1 file2...");
}

void analyze(const mshMesh &mesh) {
	int cache_misses = 0, cache_hits = 0;
	int total_indices = 0;
	int total_primitives = 0;
	for (int i=0; i<mesh.GetMtlCount(); i++) {
		const mshMaterial &mtl = mesh.GetMtl(i);
		int *used = rage_new int[mtl.GetVertexCount()];
		memset(used, 0, mtl.GetVertexCount() * sizeof(int));
		total_primitives += mtl.Prim.GetCount();
		for (int j=0; j<mtl.Prim.GetCount(); j++) {
			total_indices += mtl.Prim[j].Idx.GetCount();
			const mshPrimitive &prim = mtl.Prim[j];
			for (int k=0; k<prim.Idx.GetCount(); k++) {
				int idx = prim.Idx[k];
				if (used[idx])
					++cache_hits;
				else {
					used[idx]++;
					++cache_misses;
				}
			}
		}
		delete [] used;
	}
	printf("PTC hit rate: %d%%; avg strip length %5.2f\n",
		(cache_hits * 100) / (cache_hits + cache_misses),
		(float)total_indices / (float) total_primitives);
}

int Main() {
	char** argv = sysParam::GetArgArray();
	int argc = sysParam::GetArgCount();

	bool tristrip = false, clean = false, tobinary = false, toascii = true, tangents = false, merge = false, vopt = false, reorder = false;

	if (argc == 1) {
		help();
		return 1;
	}

	for (int i=1; i<argc; i++) {
		if (!strcmp(argv[i],"-tristrip"))
			tristrip = true;
		else if (!strcmp(argv[i],"-clean"))
			clean = true;
		else if (!strcmp(argv[i],"-ascii")) {
			toascii = true;
			tobinary = false;
		}
		else if (!strcmp(argv[i],"-binary")) {
			tobinary = true;
			toascii = false;
		}
		else if (!strcmp(argv[i],"-tangents")) {
			tangents = true;
		}
		else if (!strcmp(argv[i],"-merge")) {
			merge = true;
		}
		else if (!strcmp(argv[i],"-reorder")) {
			reorder = true;
		}
		else if (!strcmp(argv[i],"-vopt")) {
			vopt = true;
		}
		else if (clean || tristrip || tangents || merge || tobinary || toascii) {
			mshMesh MESH;
			if (SerializeFromFile(argv[i],MESH,"mesh")) {
#if 0		// example code, show how to modify data
				for (int m=0; m<MESH.GetMtlCount(); m++) {
					mshMaterial &mtl = MESH.GetMtl(m);
					for (int j=0; j<mtl.Verts.GetCount(); j++)
						mtl.Verts[j].Cpv.Set(1,1,1,1);
				}
#endif
				analyze(MESH);
				if (clean) {
					Printf("Cleaning '%s'...",argv[i]);
					MESH.CleanModel();
				}
				if (tangents) {
					Displayf("Computing tangents for '%s'...",argv[i]);
					MESH.ComputeTanBi(0);
				}
				else if (vopt) {
					Displayf("Vertex Cache Optimizing '%s'...",argv[i]);
					MESH.Triangulate();
					MESH.CacheOptimize();
				}
				else if (tristrip) {
					Displayf("Tristripping '%s'...",argv[i]);
					MESH.Triangulate();
					MESH.Tristrip();
				}
				if (merge) {
					Printf("Merging duplicate materials from '%s', from %d...",argv[i],MESH.GetMtlCount());
					MESH.MergeSameMaterials();
					Displayf("to %d.",MESH.GetMtlCount());
				}
				if (reorder) {
					Printf("Reordering vertices in '%s'...",argv[i]);
					MESH.ReorderVertices();
				}
				if (toascii) {
					Displayf("Saving '%s' to ascii",argv[i]);
					SerializeToAsciiFile(argv[i],MESH);
				}
				else if (tobinary) {
					Displayf("Saving '%s' to binary",argv[i]);
					SerializeToBinaryFile(argv[i],MESH);
				}
			}
			else
				Errorf("Unable to load '%s'",argv[i]);
		}
		else {
			Errorf("Nothing to do!");
			help();
		}
	}

	return 0;
}
