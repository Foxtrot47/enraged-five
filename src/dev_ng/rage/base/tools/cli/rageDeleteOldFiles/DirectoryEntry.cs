using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

namespace rageDeleteOldFiles
{
	class DirectoryEntry
	{
		public DirectoryEntry(string strPathAndFilename)
		{
			m_strPathAndFilename = strPathAndFilename;
			if(File.Exists(strPathAndFilename))
			{
				// File
				FileInfo obFile = new FileInfo(strPathAndFilename);
				m_obDateModified = obFile.LastWriteTime;
				m_iSizeInBytes = (UInt64)obFile.Length;
			}
			else if (Directory.Exists(strPathAndFilename))
			{
				// Directory
				DirectoryInfo obDirectory = new DirectoryInfo(strPathAndFilename);
				m_obDateModified = DateTime.MinValue;
				m_iSizeInBytes = 0;
				foreach (FileInfo obFile in obDirectory.GetFiles("*", SearchOption.AllDirectories))
				{
					m_iSizeInBytes += (UInt64)obFile.Length;
					if (m_obDateModified < obFile.LastWriteTime)
					{
						m_obDateModified = obFile.LastWriteTime;
					}
				}
			}
		}

		private string m_strPathAndFilename;
		public string PathAndFilename
		{
			get
			{
				return m_strPathAndFilename;
			}
		}

		private DateTime m_obDateModified;
		public DateTime DateModified
		{
			get
			{
				return m_obDateModified;
			}
		}

		private UInt64 m_iSizeInBytes;
		public UInt64 SizeInBytes
		{
			get
			{
				return m_iSizeInBytes;
			}
		}
	}
}
