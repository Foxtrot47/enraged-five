using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using RSG.Base.Command;
using RSG.Base.IO;

namespace rageDeleteOldFiles
{
	class Program
	{
		static void Usage()
		{
			Console.WriteLine("rageDeleteOldFiles -folder <path to prune> -size <max size folder is allowed to become in mb>");
			Console.WriteLine("	e.g. rageDeleteOldFiles -folder N:/RSGSAN/MC4/assets -size 2048");
			Console.WriteLine("	     Deletes all files and folders until N:/RSGSAN/MC4/assets is under 2GB, starting with the oldest");
		}

		static int Main(string[] args)
		{
			// Parse args
			string strFolder = null;
			UInt64 iSizeInBytes = UInt64.MaxValue;
			for (int i = 0; i < args.Length; i++)
			{
				if (args[i] == "-folder")
				{
					strFolder = args[++i];
				}
				else if (args[i] == "-size")
				{
					string strSizeInMB = args[++i];
					UInt64 iSizeInMB = UInt64.Parse(strSizeInMB);
					iSizeInBytes = iSizeInMB * 1024 * 1024;
				}
				else
				{
					Usage();
					return -1;
				}
			}

			if (strFolder == null)
			{
				Usage();
				return -1;
			}

			// Output args
			Console.WriteLine("Pruning " + strFolder + " to " + iSizeInBytes + "bytes ("+ (((iSizeInBytes/1024)/1024)/1024) +"GB, "+ ((iSizeInBytes/1024)/1024) + "MB,  "+ (iSizeInBytes/1024) +"k)");

			// Get folder info
			List<DirectoryEntry> obDirectoryEntries = new List<DirectoryEntry>();

			// Add files
			foreach(string strFilename in Directory.GetFiles(strFolder))
			{
				DirectoryEntry obDirectoryEntry = new DirectoryEntry(strFilename);

				// Add it to the list at the correct location
				bool bAdded = false;
				for(int i=0; i<obDirectoryEntries.Count; i++)
				{
					if(obDirectoryEntry.DateModified > obDirectoryEntries[i].DateModified)
					{
						bAdded = true;
						obDirectoryEntries.Insert(i, obDirectoryEntry);
						break;
					}
				}
				if (!bAdded)
				{
					obDirectoryEntries.Add(obDirectoryEntry);
				}
			}

			// Add folders
			foreach (string strFilename in Directory.GetDirectories(strFolder))
			{
				DirectoryEntry obDirectoryEntry = new DirectoryEntry(strFilename);

				// Add it to the list at the correct location
				bool bAdded = false;
				for (int i = 0; i < obDirectoryEntries.Count; i++)
				{
					if (obDirectoryEntry.DateModified > obDirectoryEntries[i].DateModified)
					{
						bAdded = true;
						obDirectoryEntries.Insert(i, obDirectoryEntry);
						break;
					}
				}
				if (!bAdded)
				{
					obDirectoryEntries.Add(obDirectoryEntry);
				}
			}

			// obDirectoryEntries now contains list of all the files and folders, sorted so that the oldest is last and newest first
			// Get size
			UInt64 iCurrentFolderSizeInBytes = 0;
			foreach (DirectoryEntry obDirectoryEntry in obDirectoryEntries)
			{
				iCurrentFolderSizeInBytes += obDirectoryEntry.SizeInBytes;
			}

			rageStatus obStatus;
			while (iCurrentFolderSizeInBytes > iSizeInBytes)
			{
				Console.WriteLine("Deleting " + obDirectoryEntries[obDirectoryEntries.Count - 1].PathAndFilename);
				if (File.Exists(obDirectoryEntries[obDirectoryEntries.Count - 1].PathAndFilename))
				{
					rageFileUtilities.DeleteLocalFile(obDirectoryEntries[obDirectoryEntries.Count - 1].PathAndFilename, out obStatus);
				}
				else
				{
					rageFileUtilities.DeleteLocalFolder(obDirectoryEntries[obDirectoryEntries.Count - 1].PathAndFilename, out obStatus);
				}
				obDirectoryEntries.RemoveAt(obDirectoryEntries.Count - 1);
				iCurrentFolderSizeInBytes = 0;
				foreach (DirectoryEntry obDirectoryEntry in obDirectoryEntries)
				{
					iCurrentFolderSizeInBytes += obDirectoryEntry.SizeInBytes;
				}
			}

			// Get folder size

			return 0;
		}
	}
}
