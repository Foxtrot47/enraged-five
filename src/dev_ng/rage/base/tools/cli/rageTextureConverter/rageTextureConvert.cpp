// 
// rageTextureConvert/rageTextureConvert.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
//

#define SIMPLE_HEAP_SIZE		(280 * 1024)

#include "atl/string.h"
#include "file/asset.h"
#include "file/token.h"
#include "file/device.h"
#include "math/simplemath.h"
#include "system/main.h"
#include "system/param.h"
#include "string/string.h"

#include "rageAssetLog/rageAssetLog.h"
#include "ragetexturelib/rageTextureConvert.h"

#include <assert.h>
#include <fstream>
#include <vector>
#include <string>
#include <list>
#include <map>

using namespace rage;
using namespace std;

PARAM(help,						"[rageTextureConvert] Display usage information.");

PARAM(in,						"[rageTextureConvert] Specifies the input texture path");
PARAM(out,						"[rageTextureConvert] Specifies the output texture path");
PARAM(format,					"[rageTextureConvert] Specifies the output texture format (e.g. DXT1, DXT3, DXT5, ARGB).");
PARAM(atlasx,					"[rageTextureConvert] specifies the amount of atlas elements across the texture.");
PARAM(atlasy,					"[rageTextureConvert] specifies the amount of atlas elements vertically");
PARAM(clipthreshold,			"[rageTextureConvert] threshold to clip the output to a certain alpha value ( 0 - 1).");
PARAM(miplevels,				"[rageTextureConvert] No of mip map levels to create (If this value is zero, a complete mipmap chain is created.  This also the default.)");
PARAM(makearray,				"[rageTextureConvert] specifies a file for generating a texture array");
PARAM(outarray,					"[rageTextureConvert] Specifies an output filename for exported 2D atlas");
PARAM(log,						"[rageTextureConvert] Turns texture export logging on default is off");
PARAM(outputRelevantClipArea,	"[rageTextureConvert] Adds extra program output which describes the non-transparent region of the texture.");
PARAM(particleClipArea,			"[rageTextureConvert] Adds extra program output which describes the non-transparent region of the texture.");

namespace 
{
	struct Settings
	{
		string							inputFilename;
		string							outputFilename;
		string							format;		
		atArray<string>					arrayTexPaths;
		int								arrayTexWidth;
		int								ax;
		int								ay;
		float							clipthreshold;
		TCConvertOptions				tcOpts;
		atArray<string>					expTexPaths;
		std::string						outarray;
		bool							outputRelevantClipArea;
		bool							clipAreaVerbose;

		Settings()
		{

			format = "none";
			ax = 1;
			ay = 1;
			clipthreshold = FLT_MIN;
			clipAreaVerbose = false;
			outputRelevantClipArea = false;
		}

		void GetDependencies(atArray<string> & dependencies)
		{
			if (inputFilename.length()>0)
			{
				dependencies.Grow() = inputFilename;
			}

			for (int i=0; i<expTexPaths.GetCount(); i++)
				dependencies.Grow() = expTexPaths[i];
		}
	};

	//-----------------------------------------------------------------------------

	void PrintHelp()
	{
		sysParam::Help("=rageTextureConvert");
	}
	
	//-----------------------------------------------------------------------------

	void FlipPathSlashes(string& str)
	{
		for(string::iterator it = str.begin(); it != str.end(); ++it)
			if( (*it) == '\\' ) 
				(*it) = '/';
	}

	//-----------------------------------------------------------------------------

	int parseCommandLine(Settings &settings)
	{
		// Make sure one of the input flags exists
		if (!(PARAM_in.Get() || 
			PARAM_makearray.Get()))
		{
			Errorf("One of the following must be specified: -in, -makearray");
			return 1;
		}

		if (!(PARAM_out.Get())) {
			Errorf("One of the following must be specified: -out");
			return 1;
		}

		const char* arrayDescriptor;
		if(PARAM_makearray.Get(arrayDescriptor))
		{
			fiStream *S = ASSET.Open( arrayDescriptor, "txt" );
			if ( !S )
			{
				return false;
			}
			fiTokenizer T( arrayDescriptor, S );
			char arrayPath[512];
			ASSET.RemoveNameFromPath( arrayPath,512, arrayDescriptor);
			int numLayers = T.MatchInt( "numlayers" );
			settings.arrayTexWidth = T.MatchInt( "width" );
			string	apath = string(arrayPath)+string("\\");

			for (int i = 0; i < numLayers; i++) {
				char fname[512];
				char bumpname[512];

				T.GetToken( fname, sizeof(fname) );
				T.GetToken( bumpname, sizeof(bumpname) );
				
				settings.arrayTexPaths.Grow() = apath+string(fname);
				settings.arrayTexPaths.Grow() = apath+string(bumpname);
			}
			settings.format="R5G6B5";
		}
	
		//Input / Output Texture Paths
		if(PARAM_in.Get())
		{
			const char* inputFile = NULL;
			PARAM_in.Get(inputFile);
			if(strlen(inputFile) != 0)
			{
				settings.inputFilename = inputFile;
			}
			else
			{
				Errorf("-in command line agument used, but no file path was specified.");
				return 1;
			}
		}

		if(PARAM_out.Get())
		{
			const char* outputFile = NULL;
			PARAM_out.Get(outputFile);
			if(strlen(outputFile) != 0)
			{
				settings.outputFilename = outputFile;

				//Make sure the path exists for the destination file
				ASSET.CreateLeadingPath(outputFile);
			}
			else
			{
				Errorf("-out command line argument used, but no file path was specified.");
				return 1;
			}
		}

		const char* format = NULL;
		PARAM_format.Get(format);
		if ( NULL != format && strlen(format) > 0 ) 
			settings.format = format;
		else
			settings.format = "none";
		PARAM_miplevels.Get(settings.tcOpts.MipLevels);
		PARAM_atlasx.Get(settings.ax);
		PARAM_atlasy.Get(settings.ay);
		PARAM_clipthreshold.Get( settings.clipthreshold);

		if(PARAM_outputRelevantClipArea.Get() || PARAM_particleClipArea.Get())
		{
			settings.outputRelevantClipArea = true;
			settings.clipAreaVerbose = PARAM_particleClipArea.Get();
		}

		const char* outarray=0;
		if ( PARAM_outarray.Get(outarray))
		{
			settings.outarray=outarray;
		}

		return 0;
	}
	
	//-----------------------------------------------------------------------------

} //namespace

//-----------------------------------------------------------------------------

int ProcessAndConvertTexture(Settings& rtcSettings, TextureConvert&	rageTextureConvert)
{
	{
		if(!rageTextureConvert.loadTexture(rtcSettings.inputFilename.c_str()))
		{
			Errorf("Could not load texture \"%s\"", rtcSettings.inputFilename.c_str());
			return 1;
		}
	}

	D3DSURFACE_DESC desc;
	rageTextureConvert.getDescription(desc);

	{
		if( (rtcSettings.tcOpts.Width <= 0) && (rtcSettings.tcOpts.Height <= 0) )
		{
			//No resize specification was set on the command line, so just set the output size to 
			//the same as the input texture size
			rtcSettings.tcOpts.Height = desc.Height;
			rtcSettings.tcOpts.Width = desc.Width;
		}
		else if( (rtcSettings.tcOpts.Height != (int)desc.Height) || (rtcSettings.tcOpts.Width != (int)desc.Width) )
		{
			//An absolute size specification was set, so resize accordingly
			Displayf("Absolute Resize Output Width : %d", rtcSettings.tcOpts.Width);
			Displayf("Absolute Resize Output Height : %d", rtcSettings.tcOpts.Height);

			rageTextureConvert.resizeTexture( rtcSettings.tcOpts.Width, rtcSettings.tcOpts.Height );
		}
	}

	if ( rtcSettings.format == "GUESS_COLOR" )
	{
		TextureConvert::AlphaCheckResult acr = rageTextureConvert.hasAlpha(true, 0.04f);
		if ( (acr == TextureConvert::RTC_HASONEBITALPHA) || (acr == TextureConvert::RTC_NOALPHA) )
		{
			rtcSettings.tcOpts.Format = D3DFMT_DXT1;
			Displayf("Format : DXT1");
		}
		else
		{
			rtcSettings.tcOpts.Format = D3DFMT_DXT5;
			Displayf("Format : DXT5");
		}
	}
	else
	{
		Displayf("Format : %s ", rtcSettings.format.c_str() );
	}

	// Calculate how much of the texture is actually used, and output a clip region that
	// represents a minimal bound of the non-transparent body.
	if (rtcSettings.outputRelevantClipArea)
	{
		rageTextureConvert.calculateRelevantClipArea(rageTextureConvert.GetTexture(), rtcSettings.ax, rtcSettings.ay, rtcSettings.clipAreaVerbose, rtcSettings.clipthreshold, rtcSettings.outputFilename.c_str());
		rageTextureConvert.saveTextureWithoutCompression(rtcSettings.outputFilename.c_str(), rtcSettings.tcOpts);
	}

	//Generate the mip-maps
	if(!rageTextureConvert.buildAndProcessMips(rtcSettings.tcOpts))
	{
		Errorf("An error occured while processing the texture mip-maps, conversion aborted.");
	}

#if USE_COMPRESSINATOR
	//If we are writing DXT5 or DXT1 textures the conversion and compression will be handled by the ATI compressonator in the call
	//to save the texture
	if( (rtcSettings.tcOpts.Format != D3DFMT_DXT5) && (rtcSettings.tcOpts.Format != D3DFMT_DXT1) )
	{
		TextureConvert::AlphaCheckResult alphaCheckRes = rageTextureConvert.hasAlpha();
		Displayf("TextureAlpha : %d", ( (alphaCheckRes == TextureConvert::RTC_HASMULTIBITALPHA) || (alphaCheckRes == TextureConvert::RTC_HASONEBITALPHA) ) ? 1 : 0);

		if (!rageTextureConvert.convertToFormat(rtcSettings.tcOpts))
		{
			Errorf("Failed to convert to format \"%s\"", rtcSettings.format);
			return 1;
		}
	}
#else

	TextureConvert::AlphaCheckResult alphaCheckRes = rageTextureConvert.hasAlpha();
	Displayf("TextureAlpha : %d", ( (alphaCheckRes == TextureConvert::RTC_HASMULTIBITALPHA) || (alphaCheckRes == TextureConvert::RTC_HASONEBITALPHA) ) ? 1 : 0);

	if (!rageTextureConvert.convertToFormat(rtcSettings.tcOpts))
	{
		Errorf("Failed to convert to format \"%s\"", rtcSettings.format);
		return 1;
	}

#endif //USE_COMPRESSINATOR

	if (!rtcSettings.outputFilename.empty() && !rtcSettings.outputRelevantClipArea)
	{
		//Make sure the destination file isn't read-only
		u32 fAttr = ASSET.GetAttributes(rtcSettings.outputFilename.c_str(), "dds");
		if( (fAttr != FILE_ATTRIBUTE_INVALID) && ( fAttr & FILE_ATTRIBUTE_READONLY ) )
		{
			Errorf("Destination file \"%s\" is read-only, write texture.", rtcSettings.outputFilename);
			return 1;
		}

		//Make sure the directory we are exporting to exists.
		ASSET.CreateLeadingPath(rtcSettings.outputFilename.c_str());

		//Export the texture
		if (!rageTextureConvert.saveTexture(rtcSettings.outputFilename.c_str(), rtcSettings.tcOpts))
		{
			Errorf("Failed to write texture \"%s\"", rtcSettings.outputFilename);
			return 1;
		}

	}
	return 0;
}

//-----------------------------------------------------------------------------
int ProcessAndConvertArrayTexture(Settings& rtcSettings, TextureConvert&	rageTextureConvert)
{
	if(!rageTextureConvert.loadArrayTextures(rtcSettings.arrayTexPaths, rtcSettings.arrayTexPaths.GetCount(), rtcSettings.arrayTexWidth))
	{
		Errorf("Failed to load all cube-map textures, exectution aborted.");
		return 1;
	}

	LPDIRECT3DVOLUMETEXTURE9 pvolTexture = rageTextureConvert.GetVolumeTexture();
	D3DVOLUME_DESC desc;
	pvolTexture->GetLevelDesc(0, &desc);

	if( (rtcSettings.tcOpts.Width <= 0) && (rtcSettings.tcOpts.Height <= 0) )
	{
		rtcSettings.tcOpts.Height = desc.Height;
		rtcSettings.tcOpts.Width = desc.Width;
	}

	u32 fAttr = ASSET.GetAttributes(rtcSettings.outputFilename.c_str(), "dds");
	if( (fAttr != FILE_ATTRIBUTE_INVALID) && ( fAttr & FILE_ATTRIBUTE_READONLY ) )
	{
		Errorf("Destination file \"%s\" is read-only, write texture.", rtcSettings.outputFilename);
		return 1;
	}

	//Make sure the directory we are exporting to exists.
	ASSET.CreateLeadingPath(rtcSettings.outputFilename.c_str());

	//Export the texture
	if (!rageTextureConvert.saveVolumeTexture(rtcSettings.outputFilename.c_str(), rtcSettings.tcOpts))
	{
		Errorf("Failed to write texture \"%s\"", rtcSettings.outputFilename);
		return 1;
	}
	std::string atname = rtcSettings.outputFilename;

	if ( rtcSettings.outarray == "")
	{
		atname = atname.substr(0,atname.length()-4);
		atname+="_atlas.dds";
	}
	else
	{
		atname = rtcSettings.outarray;
	}
	

	if (!rageTextureConvert.saveTexture(atname.c_str(), rtcSettings.tcOpts))
	{
		Errorf("Failed to write texture \"%s\"", atname);
		return 1;
	}

	return 0;
}

//-----------------------------------------------------------------------------

int Main(void)
{
	std::map<std::string, std::string>	convTextureTable;

	int retVal = 0;

	if( sysParam::GetArgCount() == 1)
	{
		PrintHelp();

		return 0;
	}

	if(PARAM_help.Get())
	{
		PrintHelp();

		return 0;
	}

	Settings baseSettings;
	retVal = parseCommandLine(baseSettings);
	if(retVal != 0)
	{
		return retVal;
	}

	std::vector<Settings> conversionSettings;

	{
		conversionSettings.push_back(baseSettings);
	}

	int nSettings = conversionSettings.size();
	for(int setIdx=0; setIdx<nSettings; setIdx++)
	{
		Settings& convSet = conversionSettings[setIdx];

		//Check to make sure we aren't converting textures of the same name from different directories
		const char* textureFileName = ASSET.FileName(convSet.inputFilename.c_str());
		std::map<std::string, std::string>::iterator ctIt = convTextureTable.find(textureFileName);
		if(ctIt != convTextureTable.end())
		{
			//A texture with this filename has already been converted, so error and don't convert the file otherwise
			//the texture that was converted first will be overwritten with the new texture
			Errorf("The file \"%s\", has the same filename as the tetxure \"%s\", this texture will not be converted to avoid overwriting the already converted file.",
				ctIt->second.c_str(), convSet.inputFilename.c_str());
			continue;
		}
		else
		{
			//Add the texture to the table of converted textures
			convTextureTable[textureFileName] = convSet.inputFilename;
		}

		rageAssetLog * log = NULL;
		bool exportTexture = true;
		if (PARAM_log.Get())
		{
			atArray<std::string> dependencies;
			convSet.GetDependencies(dependencies);

			// This loads the time stamps for the log file for comparison
			log = rage_new rageAssetLog(convSet.outputFilename.c_str());
			log->Init(dependencies);

			// Checks to see if we need to export the texture
			exportTexture = log->IsNew();

			// check to see if DDS exists
			if(!exportTexture)
			{
				if(!fiDeviceLocal::GetInstance().GetFileTime(convSet.outputFilename.c_str()))
				{
					exportTexture = true;
				}
			}

		}

		if (exportTexture)
		{
			Displayf("Converting texture \"%s\"", convSet.inputFilename.c_str());

			if (log)
			{
				log->Start();
			}

			{
				TextureConvert	rageTextureConvert;

				if(strcmp(convSet.format.c_str(), "none")==0)
				{
					//No format was specified on the command line so default to DXT5
					convSet.format = "DXT5";
				}

				std::map<std::string, D3DFORMAT>::iterator mapit;
				mapit = TextureConvert::sD3DFormats.find(convSet.format);
				if (mapit == TextureConvert::sD3DFormats.end())
				{
					Errorf("Unknown texture format \"%s\"", convSet.format.c_str());
					delete log;
					log = NULL;
					return 1;
				}
				convSet.tcOpts.Format = mapit->second;

				//If the RTC format string matches one of the normal map formats
				//then set the normal map flag so the rest of the conversion pipeline
				//knows the texture data represents a normal map.  Also, textures that 
				//are known to be normal maps should never be gamma adjusted on load
				//so mark them as such.
				if( (convSet.format == "COMP_NRM_DXT1") ||
					(convSet.format == "COMP_NRM_PD_DXT1") ||
					(convSet.format == "COMP_NRM_DXT5") ||
					(convSet.format == "COMP_NRM_DISP_DXT5") ||
					(convSet.format == "COMP_NRM_PD_DXT5") )
				{
					convSet.tcOpts.TextureIsNormalMap = true;
					convSet.tcOpts.SetSRGBGammaInDDSHeader = false;
				}

				// Height maps shouldn't be gamma adjusted.
				if (convSet.format == "L8")  
				{
					convSet.tcOpts.SetSRGBGammaInDDSHeader = false;
				}

				rageTextureConvert.DisableTIFFWarnings();

				if(convSet.arrayTexPaths.GetCount())
					retVal = ProcessAndConvertArrayTexture(convSet, rageTextureConvert);
				else
					retVal = ProcessAndConvertTexture(convSet, rageTextureConvert);
			}
		}

		// Destroy this log object
		if (log)
		{
			//If the retVal is anything other than 0, then something has gone wrong and 
			//we need to make sure the FAILURE marker is written out into the log file.
			if(retVal)
			{
				log->SetLogSuccessFlag(false);
			}

			log->Stop();

			delete log;
			log = NULL;
		}
	}

	return retVal;
}

