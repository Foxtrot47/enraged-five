<BaseTextureOptions template="BaseTextureOptions" base="TextureOptions" name="blackgrad_16b" uuid="283e8bf4-65db-11db-aeab-000874f4bc42">
  <CalcHDRScaleAndBias value="True" />
  <ExposureTextures uuid="283e8bf5-65db-11db-aeab-000874f4bc42" />
  <PC type="PlatformOptions" base="" template="BaseTextureOptions/PlatformOptions">
    <Format>DXT1</Format>
  </PC>
  <Xbox360 type="PlatformOptions" base="" template="BaseTextureOptions/PlatformOptions">
    <Format>DXT1</Format>
  </Xbox360>
  <PS3 type="PlatformOptions" base="" template="BaseTextureOptions/PlatformOptions">
    <Format>DXT1</Format>
  </PS3>
</BaseTextureOptions>