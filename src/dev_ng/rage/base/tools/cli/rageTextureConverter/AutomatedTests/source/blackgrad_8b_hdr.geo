<BaseTextureOptions template="BaseTextureOptions" base="TextureOptions" name="blackgrad_8b_hdr" uuid="2cafcda6-65db-11db-aeab-000874f4bc42">
  <CalcHDRScaleAndBias value="True" />
  <ExposureTextures uuid="2cafcda7-65db-11db-aeab-000874f4bc42" />
  <PC type="PlatformOptions" base="" template="BaseTextureOptions/PlatformOptions">
    <Format>DXT1</Format>
  </PC>
  <Xbox360 type="PlatformOptions" base="" template="BaseTextureOptions/PlatformOptions">
    <Format>DXT1</Format>
  </Xbox360>
  <PS3 type="PlatformOptions" base="" template="BaseTextureOptions/PlatformOptions">
    <Format>DXT1</Format>
  </PS3>
</BaseTextureOptions>