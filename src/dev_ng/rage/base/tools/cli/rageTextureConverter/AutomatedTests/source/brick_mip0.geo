<BaseTextureOptions template="BaseTextureOptions" base="TextureOptions" name="brick_mip0" uuid="3e8cbcc4-768e-11db-a34f-00137233912b">
  <MipTextures uuid="3e8cbcc5-768e-11db-a34f-00137233912b">
    <Item>%RAGE_ROOT_FOLDER%\base\tools\rageTextureConverter\AutomatedTests\source\brick_mip1.tif</Item>
    <Item>%RAGE_ROOT_FOLDER%\base\tools\rageTextureConverter\AutomatedTests\source\brick_mip2.tif</Item>
    <Item>%RAGE_ROOT_FOLDER%\base\tools\rageTextureConverter\AutomatedTests\source\brick_mip3.tif</Item>
  </MipTextures>
  <ExposureTextures uuid="3e8cbcc6-768e-11db-a34f-00137233912b" />
  <PC type="PlatformOptions" base="" template="BaseTextureOptions/PlatformOptions">
    <Format>DXT1</Format>
  </PC>
  <Xbox360 type="PlatformOptions" base="" template="BaseTextureOptions/PlatformOptions">
    <Format>DXT1</Format>
  </Xbox360>
  <PS3 type="PlatformOptions" base="" template="BaseTextureOptions/PlatformOptions">
    <Format>DXT1</Format>
  </PS3>
</BaseTextureOptions>
