<BaseTextureOptions template="BaseTextureOptions" base="TextureOptions" name="wallCubeMap_pX" uuid="c1a1cb64-7a61-11db-a34f-00137233912b">
  <MipTextures uuid="c1a1cb65-7a61-11db-a34f-00137233912b" />
  <ExposureTextures uuid="c1a1cb66-7a61-11db-a34f-00137233912b" />
  <CubeMapPX>%RAGE_ROOT_FOLDER%\base\tools\rageTextureConverter\AutomatedTests\source\wallCubeMap_pX.tif</CubeMapPX>
  <CubeMapNX>%RAGE_ROOT_FOLDER%\base\tools\rageTextureConverter\AutomatedTests\source\wallCubeMap_nX.tif</CubeMapNX>
  <CubeMapPY>%RAGE_ROOT_FOLDER%\base\tools\rageTextureConverter\AutomatedTests\source\wallCubeMap_pY.tif</CubeMapPY>
  <CubeMapNY>%RAGE_ROOT_FOLDER%\base\tools\rageTextureConverter\AutomatedTests\source\wallCubeMap_nY.tif</CubeMapNY>
  <CubeMapPZ>%RAGE_ROOT_FOLDER%\base\tools\rageTextureConverter\AutomatedTests\source\wallCubeMap_pZ.tif</CubeMapPZ>
  <CubeMapNZ>%RAGE_ROOT_FOLDER%\base\tools\rageTextureConverter\AutomatedTests\source\wallCubeMap_nZ.tif</CubeMapNZ>
  <PC type="PlatformOptions" base="" template="BaseTextureOptions/PlatformOptions">
    <Format>DXT1</Format>
  </PC>
  <Xbox360 type="PlatformOptions" base="" template="BaseTextureOptions/PlatformOptions">
    <Format>DXT1</Format>
  </Xbox360>
  <PS3 type="PlatformOptions" base="" template="BaseTextureOptions/PlatformOptions">
    <Format>DXT1</Format>
  </PS3>
</BaseTextureOptions>