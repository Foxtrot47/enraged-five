// -----------------------------------------------------------------------------
// $Workfile: TexutureConvert.h  $
//   Creator: Clemens Pecinovsky
// Copyright: 2005 Rockstar Vienna
// $Revision$
//   $Author$
//     $Date$
// -----------------------------------------------------------------------------
// Description: 
// -----------------------------------------------------------------------------

#ifndef RSV_TEXTURE_CONVERT_H
#define RSV_TEXTURE_CONVERT_H

#include <d3dx9.h>
#include <WinDef.h>

#include <map>
#include <string>

#include <IL/il.h>
#include <IL/ilu.h>

namespace Neo
{

	//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

struct TCConvertOptions
{
	int			Width;
	int			Height;
	int			MipLevels;
	float		HDROffR, HDROffG, HDROffB, HDROffA;
	float		HDRExpR, HDRExpG, HDRExpB, HDRExpA;
	D3DFORMAT	Format;
	DWORD		mipFilter;

	TCConvertOptions()
	{
		Width = Height = -1;
		MipLevels = 0;
		HDROffR = HDROffG = HDROffB = HDROffA = 0.0f;
		HDRExpR = HDRExpG = HDRExpB = HDRExpA = 1.0f;
		Format = D3DFMT_DXT5;
		mipFilter = D3DX_FILTER_BOX;
	}
	TCConvertOptions(int w, int h, D3DFORMAT f)
		: Width(w)
		, Height(h)
		, Format(f)
	{
		MipLevels = 0;
		HDROffR = HDROffG = HDROffB = HDROffA = 0.0f;
		HDRExpR = HDRExpG = HDRExpB = HDRExpA = 1.0f;
	}
};

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

class TextureConvert
{
public:
    TextureConvert();
    virtual ~TextureConvert();
    
	// Load the texture, whatever format it is, whatever input format I read in, convert it to a DirectX format
    bool loadTexture(LPCTSTR pSrcFile);

	// Save as a directX file
    bool saveTexture(LPCTSTR pDestFile, TCConvertOptions& tcOpt);
    void releaseTexture();
    bool convertABGRF32ToRGBE();
    bool convertABGRF32ToRGBEWithExponentFromFile(LPCTSTR alphaName);
    bool convertABGRF32ToDXT5RGBE();
    bool convertRGBEToABGRF32();
	bool convertABGRF32ToARGB();
	bool crop(float uMin, float vMin, float uMax, float vMax, bool cropPow2RoundingUp, ILenum filter, TCConvertOptions* tcOpt, float scale);
	bool oneToOneCrop(float uMin, float vMin, float uMax, float vMax, TCConvertOptions* tcOpt);
	bool scale(TCConvertOptions* tcOpt, float scale, bool nearestFilter);
    bool loadAlphaFromL8(LPCTSTR alphaName);

    bool convertToFormat(TCConvertOptions& tcOpt);

    bool removeAlphaFromA8R8G8B8();
    bool convertAlphaFromA8R8G8B8IntoLuminance(int width, int height);
    void getDescription(D3DSURFACE_DESC &desc);
	int getWidth();
	int getHeight();
	D3DFORMAT getFormat();
	bool computeHDRExponentOffset(float &outExpR, float &outExpG, float &outExpB,
								  float &outOffR, float &outOffG, float &outOffB);
	bool gammaCorrect();
	bool adjustExposure(float exposureVal);
	bool hasAlpha();

	void initDevil(int numImages = 1);
	void shutdownDevil();
	void setCurrDevilImage(int image);

	bool convertTextureToDevilImage();

	bool convertDevilImageToTexture();

	typedef std::map<std::string, D3DFORMAT> FORMAT_MAP;
    static FORMAT_MAP sD3DFormats;

	class Convertor 
	{
	public:
		virtual void Start( const char* ) {}
		virtual void Convert( const float* src, float* des , TCConvertOptions* pOpts ) = 0;
		virtual void End() {}

		// settings for convertor
		virtual int MipFilter() { return D3DX_FILTER_BOX; }
	};


	typedef Convertor PixelConvertor;	


	static std::map<std::string,  PixelConvertor* > sConversionProcesses;
	typedef std::map<std::string, PixelConvertor*>::iterator	ProcessIterator;

	bool convert( PixelConvertor* convertor, TCConvertOptions* pOpts );

    //void testConversion();

	// Array of image IDs
	ILuint* m_DevilImageIDs;
	int	m_NumDevilImageIDs;

private:



    bool createNULLRefDevice();
    void releaseNULLRefDevice();

	bool	convertTo32Bit();

    void reduceExponentFromRGB(float redf, float greenf, float bluef, 
        int alphaExp,int &red, int &green, int &blue);

    void calculateRGBE(float redf, float greenf, float bluef, 
        int &red, int &green, int &blue, int &alpha);

    void calculateRGBfromRGBE(int red, int green, int blue, int alpha,
        float &redf, float &greenf, float &bluef);

    bool convertToFormatInternal(LPDIRECT3DTEXTURE9 &oldTexture,
								 TCConvertOptions& tcOpt, 
								 LPDIRECT3DTEXTURE9 *newTexture);

    int getExponent(float brightness);

    static void initD3DFormats();


    LPDIRECT3D9             mPD3D;
    LPDIRECT3DDEVICE9       mPD3DDevice;
	
	float					mInputGamma;
    LPDIRECT3DTEXTURE9      mPTexture;
};

} // namespace Neo

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//Temporary, copied from grCore/dds.h to avoid having to bring in 
//all of the other rage graphics and core libraries

struct DDPIXELFORMAT {
		unsigned int dwSize;			// Size of structure. This member must be set to 32.
		unsigned int dwFlags;			// Flags to indicate valid fields. Uncompressed formats will usually use DDPF_RGB to indicate an RGB format, while compressed formats will use DDPF_FOURCC with a four-character code.
		unsigned int dwFourCC;			// This is the four-character code for compressed formats. dwFlags should include DDPF_FOURCC in this case. For DXTn compression, this is set to "DXT1", "DXT2", "DXT3", "DXT4", or "DXT5".
		unsigned int dwRGBBitCount;		// For RGB formats, this is the total number of bits in the format. dwFlags should include DDPF_RGB in this case. This value is usually 16, 24, or 32. For A8R8G8B8, this value would be 32.
		unsigned int dwRBitMask;
		unsigned int dwGBitMask;
		unsigned int dwBBitMask;		// For RGB formats, these three fields contain the masks for the red, green, and blue channels. For A8R8G8B8, these values would be 0x00ff0000, 0x0000ff00, and 0x000000ff respectively.
		unsigned int dwRGBAlphaBitMask; // For RGB formats, this contains the mask for the alpha channel, if any. dwFlags should include DDPF_ALPHAPIXELS in this case. For A8R8G8B8, this value would be 0xff000000.
	};

struct DDCAPS2 {
		unsigned int dwCaps1;
		unsigned int dwCaps2;
		unsigned int Reserved[2];
	};

struct DDSURFACEDESC2 {
		unsigned int dwSize;				// Size of structure. This member must be set to 124.
		unsigned int dwFlags;				// Flags to indicate valid fields. Always include DDSD_CAPS, DDSD_PIXELFORMAT, DDSD_WIDTH, DDSD_HEIGHT.
		unsigned int dwHeight;				// Height of the main image in pixels
		unsigned int dwWidth;				// Width of the main image in pixels
		unsigned int dwPitchOrLinearSize;	// For uncompressed formats, this is the number of bytes per scan line (DWORD> aligned) for the main image. dwFlags should include DDSD_PITCH in this case. For compressed formats, this is the total number of bytes for the main image. dwFlags should be include DDSD_LINEARSIZE in this case.
		unsigned int dwDepth;				// For volume textures, this is the depth of the volume. dwFlags should include DDSD_DEPTH in this case.
		unsigned int dwMipMapCount;			// For items with mipmap levels, this is the total number of levels in the mipmap chain of the main image. dwFlags should include DDSD_MIPMAPCOUNT in this case.
		unsigned int dwReserved1[5];		//	Unused	
		float fColorExp[3];					//	Color scale (pre-calculated exponent) (R,G,B) for HDR values
		float fColorOfs[3];					//	Color offset (R,G,B) for HDR values
		DDPIXELFORMAT ddpfPixelFormat;		// 32-byte value that specifies the pixel format structure.
		DDCAPS2 ddsCaps;					// 16-byte value that specifies the capabilities structure.
		unsigned int dwReserved2;			// 	Unused
	};

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

#endif // RSV_TEXTURE_CONVERT_H
