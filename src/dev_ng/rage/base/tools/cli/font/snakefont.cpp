/*
	Build with: cl snakefont.c user32.lib gdi32.lib

	This program is a hack.  Treat it as such.

	Suggested usage:

	snakefont Impact 32 1.2 255,255,255 0,0,0

	First parameter is the font name
	Second parameter is the cell size; size * 8 is the image size.
	Third parameter is the scale factor applied to the font size to 
	get it to fill the cell size better.
	Fourth parameter is the RGB value for the foreground color.
	Fifth parameter is the RGB value for the background color.

	Once the font is how you like it, make the "window" active
	and hit Alt-PrintScreen to copy it to the clipboard, then paste
	it to pbrush.exe (or your favorite paint program).
	
	Press ^C in the console window to shut it down.

	Hint: If you have Windows 98 or later installed, go to Display
	Properties / Effects and check "Smooth edges of screen fonts"
	to get cheesy antialiasing for free.
*/

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#pragma comment(lib,"user32.lib")
#pragma comment(lib,"gdi32.lib")

static char *Name;
static int Size;
static float Scale;
static int Back = 0x0, Fore = 0xFFFFFF;

int ParseColor(char *str) {
    int r, g, b;
    r = atoi(str);

    str = strchr(str,',') + 1;
    g = atoi(str);

    str = strchr(str,',') + 1;
    b = atoi(str);

    return (int)(r) | ((int)(g) << 8) | ((int)(b) << 16);
}


static void DumpFont(HDC hdc) {
		LOGFONT lf;
		HFONT font;
		HFONT old;
		int i, j;
		memset(&lf,0,sizeof(lf));
		strcpy(lf.lfFaceName,Name);
        lf.lfHeight = Size * Scale;
		font = CreateFontIndirect(&lf);

        SetTextColor(hdc,Fore);
        SetBkColor(hdc,Back);
		old = (HFONT)SelectObject(hdc,font);

		for (i=32; i<128; i++) {
			char buf = i;
			int x = (i & 15) * Size + 1;
			int y = ((i - 32) >> 4) * Size;
			TextOut(hdc,x,y,&buf,1);
		}	

		SelectObject(hdc,old);
}


LRESULT CALLBACK windproc(HWND hwnd,UINT msg,WPARAM wparam,LPARAM lparam) {
	if (msg == WM_PAINT) {
		PAINTSTRUCT ps;
		BeginPaint(hwnd,&ps);
		DumpFont(ps.hdc);
		EndPaint(hwnd,&ps);
		return 0L;
	}
	else
		return DefWindowProc(hwnd,msg,wparam,lparam);
}

void main(int argc, char **argv) {
	WNDCLASS wc;
	HWND CFS;
    HBRUSH br;
	char buf[256];

	Name = argv[1];
	Size = atoi(argv[2]);
    if (argc > 3)
        Scale = atof(argv[3]);
    if (argc > 4)
        Fore = ParseColor(argv[4]);
    if (argc > 5)
        Back = ParseColor(argv[5]);
    br = CreateSolidBrush(Back);
    
	memset(&wc,0,sizeof(wc));
    wc.style = 0; 
	wc.lpfnWndProc = windproc; 
	wc.hbrBackground = br;
	wc.lpszClassName = "FontSnaker"; 

	RegisterClass(&wc);

	CFS = CreateWindow(wc.lpszClassName,
		"Cheesy Font Snaker!",
		WS_POPUP,
		0,0,Size * 16,Size * 8,
		0,0,0,0);
	ShowWindow(CFS,SW_SHOWDEFAULT);
	UpdateWindow(CFS);
	{
		MSG msg;
        while (GetMessage(&msg,CFS,0,0)) {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
	}
}
