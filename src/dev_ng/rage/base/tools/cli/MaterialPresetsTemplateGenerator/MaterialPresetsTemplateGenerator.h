#include "forceinclude.h"
#include "configParser/configParser.h"
#include "configParser/configGameView.h"

#include "grcore/effect.h"

using namespace rage;

class MaterialPresetsTemplateGenerator
{
public:
	MaterialPresetsTemplateGenerator(const char* shaderPath, const char* shaderDbPath, const char* outputPath);

	void Initialize(bool processAllTemplates, bool processSurfaceTemplates);
	bool GenerateTemplateForShader(const char* shaderName);

private:
	static bool ProcessTemplateInstanceData(grcInstanceData& instanceData, u32 hashcode, void* data);
	static bool ProcessSurfaceInstanceData(grcInstanceData& instanceData, u32 hashcode, void* data);

	const char* m_ShaderPath;
	const char* m_ShaderDbPath;
	const char* m_OutputPath;

	static const configParser::ConfigGameView ms_gv;
};