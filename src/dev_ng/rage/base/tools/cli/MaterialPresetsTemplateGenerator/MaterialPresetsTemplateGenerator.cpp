// MaterialPresetsTemplateGenerator.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "MaterialPresetsTemplateGenerator.h"

#include "grcore/texturedefault.h"
#include "grmodel/modelfactory.h"
#include "grmodel/shader.h"

#include "parser/macros.h"
#include "parser/manager.h"
#include "atl/array.h"
#include "atl/string.h"
#include "string/stringutil.h"

#include "MaterialPresets/MaterialPreset.h"
#include "MaterialPresets/MaterialPresetManager.h"

using namespace rage;

const configParser::ConfigGameView MaterialPresetsTemplateGenerator::ms_gv;

MaterialPresetsTemplateGenerator::MaterialPresetsTemplateGenerator(const char* shaderPath, const char* shaderDbPath, const char* outputPath)
{
	m_ShaderPath = shaderPath;
	m_ShaderDbPath = shaderDbPath;
	m_OutputPath = outputPath;
}

void MaterialPresetsTemplateGenerator::Initialize(bool processAllTemplates, bool processSurfaceTemplates)
{
	TCHAR buildDir[RAGE_MAX_PATH];
	ms_gv.GetBuildDir(&(buildDir[0]), RAGE_MAX_PATH);
	atString masterPresetPath(buildDir);
	masterPresetPath += atString("/common/shaders/db");
	MaterialPresetManager::GetCurrent().SetPresetPath(masterPresetPath);
	pgDictionaryBase::SetListOwnerThread(g_CurrentThreadId);

	PARSER.Settings().SetFlag(parSettings::USE_TEMPORARY_HEAP, true);

	grcTextureFactoryDefault::CreatePagedTextureFactory();
	grmModelFactory::CreateStandardModelFactory();
	grmShaderFactory::CreateStandardShaderFactory();
	grmShaderFactory::GetInstance().PreloadShaders(m_ShaderPath);
	grcMaterialLibrary::SetCurrent(grcMaterialLibrary::Preload(m_ShaderDbPath));

	if (processAllTemplates)
	{
		grcMaterialLibrary::GetCurrent()->ForAll(MaterialPresetsTemplateGenerator::ProcessTemplateInstanceData, (void*) m_OutputPath);

		MaterialPresetManager::GetCurrent().SetPresetPath(atString(m_OutputPath));
		MaterialPresetManager::GetCurrent().LoadPresets();

		// Legacy stuff that might not really get used but gives us the option.
		if (processSurfaceTemplates)
			grcMaterialLibrary::GetCurrent()->ForAll(MaterialPresetsTemplateGenerator::ProcessSurfaceInstanceData, (void*) m_OutputPath);
	}
}

bool MaterialPresetsTemplateGenerator::GenerateTemplateForShader(const char* shaderName)
{
	char fullShaderName[256];

	if (ASSET.FindExtensionInPath(shaderName) == NULL)
		formatf(fullShaderName, "%s.sps", shaderName);
	else
		safecpy(fullShaderName, shaderName, sizeof(fullShaderName));

	bool result = false;
	grcInstanceData* pInstanceData = grcEffect::LookupMaterial(atStringHash(fullShaderName));
	if (pInstanceData)
	{
		result = ProcessTemplateInstanceData(*pInstanceData, NULL, (void*) m_OutputPath);
	}
	else
	{
		Errorf("Could not retrieve instance data for: %s", fullShaderName);
	}

	return result;
}

bool MaterialPresetsTemplateGenerator::ProcessTemplateInstanceData(grcInstanceData& instanceData, u32 hashcode, void* data)
{
	const char* outputPath = (const char*) data;

	if(!&instanceData)
	{
		Errorf("Failed to load an sps file. Make sure all files exist in %s that are given from the preload file.", outputPath);
		return false;
	}

	const char* materialName = instanceData.GetMaterialName();

	ePresetLevel presetLevel = kTemplate;

	//HACK:  Pseudo-hack; all surface-level presets are in sub-directories of the build\dev\common\data.
	if ( stristr(materialName, "/") != NULL || stristr(materialName, "\\") != NULL )
		return true;  //Do not process any surface materials.

	char materialFileName[256];
	fiAssetManager::RemoveExtensionFromPath(&(materialFileName[0]), 256, materialName);

	//grcEffect& effect = instanceData.GetMaterial()? instanceData.GetMaterial()->GetBasis() : instanceData.GetBasis();
	grcEffect& effect = instanceData.GetBasis();

	const char* parent = NULL;
	if ( presetLevel == kSurface )
	{
		//Set the parent.
		parent = effect.GetEffectName();
	}

	MaterialPreset preset(parent, effect.GetEffectName(), materialFileName, presetLevel);

	int numVariables = effect.GetInstancedVariableCount();

	preset.SetDrawBucket(instanceData.DrawBucket);
	preset.SetDrawBucketMask(instanceData.DrawBucketMask);
	preset.SetIsInstanced(instanceData.IsInstanced);

	for(int varIndex = 0; varIndex < numVariables; ++varIndex)
	{
		grmVariableInfo pVarInfo;
		effect.GetInstancedVariableInfo(varIndex, pVarInfo);
		grcEffectVar handle = effect.LookupVar(pVarInfo.m_Name);

		atString variableName(pVarInfo.m_Name);
		atString uiName(pVarInfo.m_UiName);
		atString uiHint(pVarInfo.m_UiHint);

		if (pVarInfo.m_Type == grcEffect::VT_FLOAT)
		{
			float value;
			effect.GetVar(instanceData, handle, value);
			FloatType newFloat(uiName, variableName, varIndex, false, value);
			newFloat.SetMinValue(pVarInfo.m_UiMin);
			newFloat.SetMaxValue(pVarInfo.m_UiMax);
			newFloat.SetStep(pVarInfo.m_UiStep);
			preset.AddFloat(newFloat);
		}
		else if (pVarInfo.m_Type == grcEffect::VT_INT)
		{
			float floatValue;
			effect.GetVar(instanceData, handle, floatValue);
			int value = (int) floatValue;  //TODO:  Investigate whether this is valid?
			IntType newInt(uiName, variableName, varIndex, false, value);
			preset.AddInt(newInt);
		} 
		else if (pVarInfo.m_Type == grcEffect::VT_TEXTURE)
		{
			grcTexture *value;
			effect.GetVar(instanceData, handle, value);

			eTextureMapType textureMapType = kTypeDiffuse;
			if ( stristr(variableName, "Normal") != NULL || stristr(variableName, "Bump") != NULL )
				textureMapType = kTypeNormal;
			else if ( stristr(variableName, "Specular") != NULL || stristr(variableName, "SpecTex") != NULL)
				textureMapType = kTypeSpecular;
			else if ( stristr(variableName, "TintPalette") != NULL )
				textureMapType = kTypeTintPalette;
			else if ( stristr(variableName, "Detail") != NULL || stristr(uiHint, "detailmap") != NULL)
				textureMapType = kTypeDetail;

			TexMapType newTexMap(uiName, variableName, varIndex, false, textureMapType);  
			newTexMap.SetTextureTemplate(atString(pVarInfo.m_TCPTemplate));
			newTexMap.SetTextureTemplateRelative(atString(pVarInfo.m_TCPTemplateRelative));
			newTexMap.SetTextureTemplateOverride((pVarInfo.m_TCPAllowOverride == 1 ? true : false));
			newTexMap.SetTextureFormat(atString(pVarInfo.m_TextureOutputFormats));
			preset.AddTexMap(newTexMap);
		} 
		else if (pVarInfo.m_Type == grcEffect::VT_VECTOR2)
		{
			Vector2 value;
			effect.GetVar(instanceData, handle, value);
			Vector2Type newVector2(uiName, variableName, varIndex, false, value);
			newVector2.SetMinValue(pVarInfo.m_UiMin);
			newVector2.SetMaxValue(pVarInfo.m_UiMax);
			newVector2.SetStep(pVarInfo.m_UiStep);
			preset.AddVector2(newVector2);
		} 
		else if (pVarInfo.m_Type == grcEffect::VT_VECTOR3)
		{
			Vector3 value;
			effect.GetVar(instanceData, handle, value);
			Vector3Type newVector3(uiName, variableName, varIndex, false, value);
			newVector3.SetMinValue(pVarInfo.m_UiMin);
			newVector3.SetMaxValue(pVarInfo.m_UiMax);
			newVector3.SetStep(pVarInfo.m_UiStep);
			preset.AddVector3(newVector3);
		} 
		else if (pVarInfo.m_Type == grcEffect::VT_VECTOR4)
		{
			Vector4 value;
			effect.GetVar(instanceData, handle, value);
			Vector4Type newVector4(uiName, variableName, varIndex, false, value);
			newVector4.SetMinValue(pVarInfo.m_UiMin);
			newVector4.SetMaxValue(pVarInfo.m_UiMax);
			newVector4.SetStep(pVarInfo.m_UiStep);
			preset.AddVector4(newVector4);
		} 
	}

	atString outputFilename(outputPath);
	if(outputFilename[outputFilename.length()-1]!='\\' && outputFilename[outputFilename.length()-1]!='/')
		outputFilename += "/";
	outputFilename += materialFileName;
	if ( presetLevel == kTemplate )
		outputFilename += MaterialPresetManager::m_TemplateFileExtension;
	else if ( presetLevel == kSurface )
		outputFilename += MaterialPresetManager::m_SurfaceFileExtension;

	ASSET.CreateLeadingPath(outputFilename.c_str());
	PARSER.SaveObject(outputFilename, "", &preset, parManager::XML);
	return true;
}

bool MaterialPresetsTemplateGenerator::ProcessSurfaceInstanceData(grcInstanceData& instanceData, u32 hashcode, void* data)
{
	const char* outputPath = (const char*) data;
	const char* materialName = instanceData.GetMaterialName();

	ePresetLevel presetLevel = kSurface;
	if ( stristr(materialName, "/") == NULL && stristr(materialName, "\\") == NULL )
		return true;  //Do not process any template-level objects.

	char materialFileName[RAGE_MAX_PATH];
	fiAssetManager::RemoveExtensionFromPath(&(materialFileName[0]), RAGE_MAX_PATH, materialName);

	grcEffect& effect = instanceData.GetBasis();

	atString shaderName;
	atString parent("");
	if ( presetLevel == kSurface )
	{
		shaderName = effect.GetEffectName();

		//Set the parent.
		parent += effect.GetEffectName();
		parent += MaterialPresetManager::m_TemplateFileExtension;
	}

	MaterialPreset preset(parent, shaderName, materialFileName, presetLevel);

	int numVariables = effect.GetInstancedVariableCount();

	preset.SetDrawBucket(instanceData.DrawBucket);
	preset.SetDrawBucketMask(instanceData.DrawBucketMask);
	preset.SetIsInstanced(instanceData.IsInstanced);

	for(int varIndex = 0; varIndex < numVariables; ++varIndex)
	{
		grmVariableInfo pVarInfo;
		effect.GetInstancedVariableInfo(varIndex, pVarInfo);
		grcEffectVar handle = effect.LookupVar(pVarInfo.m_Name);

		atString variableName(pVarInfo.m_Name);
		atString uiName(pVarInfo.m_UiName);
		atString uiHint(pVarInfo.m_UiHint);

		if (pVarInfo.m_Type == grcEffect::VT_FLOAT)
		{
			float value;
			effect.GetVar(instanceData, handle, value);
			FloatType newFloat(uiName, variableName, varIndex, false, value);
			preset.AddFloat(newFloat);
		}
		else if (pVarInfo.m_Type == grcEffect::VT_INT)
		{
			float floatValue;
			effect.GetVar(instanceData, handle, floatValue);
			int value = (int) floatValue;  //TODO:  Investigate whether this is valid?
			IntType newInt(uiName, variableName, varIndex, false, value);
			preset.AddInt(newInt);
		} 
		else if (pVarInfo.m_Type == grcEffect::VT_TEXTURE)
		{
			grcTexture *value;
			effect.GetVar(instanceData, handle, value);
			eTextureMapType textureMapType = kTypeDiffuse;
			if ( stristr(variableName, "Normal") != NULL || stristr(variableName, "Bump") != NULL )
				textureMapType = kTypeNormal;
			else if ( stristr(variableName, "Specular") != NULL || stristr(variableName, "SpecTex") != NULL)
				textureMapType = kTypeSpecular;
			else if ( stristr(variableName, "TintPalette") != NULL )
				textureMapType = kTypeTintPalette;
			else if ( stristr(variableName, "Detail") != NULL || stristr(uiHint, "detailmap") != NULL)
				textureMapType = kTypeDetail;

			TexMapType newTexMap(uiName, variableName, varIndex, false, textureMapType);  
			preset.AddTexMap(newTexMap);
		} 
		else if (pVarInfo.m_Type == grcEffect::VT_VECTOR2)
		{
			Vector2 value;
			effect.GetVar(instanceData, handle, value);
			Vector2Type newVector2(uiName, variableName, varIndex, false, value);
			preset.AddVector2(newVector2);
		} 
		else if (pVarInfo.m_Type == grcEffect::VT_VECTOR3)
		{
			Vector3 value;
			effect.GetVar(instanceData, handle, value);
			Vector3Type newVector3(uiName, variableName, varIndex, false, value);
			preset.AddVector3(newVector3);
		} 
		else if (pVarInfo.m_Type == grcEffect::VT_VECTOR4)
		{
			Vector4 value;
			effect.GetVar(instanceData, handle, value);
			Vector4Type newVector4(uiName, variableName, varIndex, false, value);
			preset.AddVector4(newVector4);
		} 
	}

	//Fix up all of the MaterialPresets surfaces compared against their templates.
	atString templatePresetPath(outputPath);
	templatePresetPath += parent;
	const MaterialPreset& parentPreset = *(MaterialPresetManager::GetCurrent().LoadPreset(templatePresetPath));

	//Iterate through each of the variables; determine set the locked variable if they have been overridden.
	atString outputFilename(outputPath);
	outputFilename += materialFileName;
	if ( presetLevel == kTemplate )
		outputFilename += MaterialPresetManager::m_TemplateFileExtension;
	else if ( presetLevel == kSurface )
		outputFilename += MaterialPresetManager::m_SurfaceFileExtension;
	ASSET.CreateLeadingPath(outputFilename.c_str());

	fiStream* presetFile = ASSET.Create(outputFilename.c_str(), "");
	fprintf(presetFile, "<?xml version=\"1.0\" encoding=\"UTF-8\"?> \n");
	fprintf(presetFile, "<MaterialPreset>\n");
	fprintf(presetFile, "\t<Parent>%s</Parent>\n", templatePresetPath);
	fprintf(presetFile, "\t<ShaderName>%s</ShaderName>\n", preset.GetShaderName());
	fprintf(presetFile, "\t<FriendlyName>%s</FriendlyName>\n", preset.GetFriendlyName());
	
	bool lockNonTextureVariables = true;

	//Texture Maps
	atArray<TexMapType> texMapArray;
	for(int texMapIndex = 0; texMapIndex < preset.GetTexMaps().GetCount(); ++texMapIndex)
	{
		const TexMapType& texMap = preset.GetTexMaps()[texMapIndex];
		const TexMapType* parentTexMapType = parentPreset.GetTexMap(texMap.GetIndex());

		if ( (texMap.IsLocked() != parentTexMapType->IsLocked())
			|| stricmp(texMap.GetValue().c_str(), parentTexMapType->GetValue().c_str()) != 0 )
		{	
			texMapArray.PushAndGrow(texMap);
		}
	}

	if ( texMapArray.GetCount() > 0 )
	{
		fprintf(presetFile, "\t<TexMaps>\n");

		for(int texMapIndex = 0; texMapIndex < texMapArray.GetCount(); ++texMapIndex)
		{
			const TexMapType& texMap = texMapArray[texMapIndex];

			fprintf(presetFile, "\t\t<Item>\n");
			fprintf(presetFile, "\t\t\t<Name>%s</Name>\n", texMap.GetName());
			fprintf(presetFile, "\t\t\t<VariableName>%s</VariableName>\n", texMap.GetVariableName());
			fprintf(presetFile, "\t\t\t<Locked value=\"true\"/>\n"); //Locked because this surface has overridden the value.
			fprintf(presetFile, "\t\t\t<Index value=\"%d\"/>\n", texMap.GetIndex()); 
			fprintf(presetFile, "\t\t\t<TexMapType>%s</TexMapType>\n", eTextureMapType_Strings[texMap.GetTextureMapType()]);
			fprintf(presetFile, "\t\t\t<Value>%s</Value>\n", texMap.GetValue());
			fprintf(presetFile, "\t\t</Item>\n");
		}

		fprintf(presetFile, "\t</TexMaps>\n");
	}

	//Vector 4
	atArray<Vector4Type> vector4Array;
	for(int vectorIndex = 0; vectorIndex < preset.GetVector4s().GetCount(); ++vectorIndex)
	{
		const Vector4Type& vector4Type = preset.GetVector4s()[vectorIndex];
		const Vector4Type* parentVector4Type = parentPreset.GetVector4(vector4Type.GetIndex());

		if ( lockNonTextureVariables || (vector4Type.IsLocked() != parentVector4Type->IsLocked()) ||
			vector4Type.GetValue().IsEqual(parentVector4Type->GetValue()) == false )
		{
			vector4Array.PushAndGrow(vector4Type);
		}
	}

	if ( vector4Array.GetCount() > 0 )
	{
		fprintf(presetFile, "\t<Vec4s>\n");

		for(int vectorIndex = 0; vectorIndex < vector4Array.GetCount(); ++vectorIndex)
		{
			const Vector4Type& vector4Type = vector4Array[vectorIndex];
			fprintf(presetFile, "\t\t<Item>\n");
			fprintf(presetFile, "\t\t\t<Name>%s</Name>\n", vector4Type.GetName());
			fprintf(presetFile, "\t\t\t<VariableName>%s</VariableName>\n", vector4Type.GetVariableName());
			fprintf(presetFile, "\t\t\t<Locked value=\"true\"/>\n"); //Locked because this surface has overridden the value.
			fprintf(presetFile, "\t\t\t<Index value=\"%d\"/>\n", vector4Type.GetIndex()); 
			fprintf(presetFile, "\t\t\t<Value x=\"%f\" y=\"%f\" z=\"%f\" w=\"%f\"/>\n", vector4Type.GetValue().GetX(), vector4Type.GetValue().GetY(), vector4Type.GetValue().GetZ(), vector4Type.GetValue().GetW());
			fprintf(presetFile, "\t\t</Item>\n");
		}

		fprintf(presetFile, "\t</Vec4s>\n");
	}

	//Vector 3
	atArray<Vector3Type> vector3Array;
	
	for(int vectorIndex = 0; vectorIndex < preset.GetVector3s().GetCount(); ++vectorIndex)
	{
		const Vector3Type& vector3Type = preset.GetVector3s()[vectorIndex];
		const Vector3Type* parentVector3Type = parentPreset.GetVector3(vector3Type.GetIndex());

		if ( lockNonTextureVariables || (vector3Type.IsLocked() != parentVector3Type->IsLocked()) ||
			vector3Type.GetValue().IsEqual(parentVector3Type->GetValue()) == false )
		{
			vector3Array.PushAndGrow(vector3Type);
		}
	}
	if ( vector3Array.GetCount() > 0 )
	{
		fprintf(presetFile, "\t<Vec3s>\n");

		for(int vectorIndex = 0; vectorIndex < vector3Array.GetCount(); ++vectorIndex)
		{
			const Vector3Type& vector3Type = vector3Array[vectorIndex];
			fprintf(presetFile, "\t\t<Item>\n");
			fprintf(presetFile, "\t\t\t<Name>%s</Name>\n", vector3Type.GetName());
			fprintf(presetFile, "\t\t\t<VariableName>%s</VariableName>\n", vector3Type.GetVariableName());
			fprintf(presetFile, "\t\t\t<Locked value=\"true\"/>\n"); //Locked because this surface has overridden the value.
			fprintf(presetFile, "\t\t\t<Index value=\"%d\"/>\n", vector3Type.GetIndex()); 
			fprintf(presetFile, "\t\t\t<Value x=\"%f\" y=\"%f\" z=\"%f\"/>\n", vector3Type.GetValue().GetX(), vector3Type.GetValue().GetY(), vector3Type.GetValue().GetZ());
			fprintf(presetFile, "\t\t</Item>\n");
		}

		fprintf(presetFile, "\t</Vec3s>\n");
	}

	//Vector 2
	atArray<Vector2Type> vector2Array;
	for(int vectorIndex = 0; vectorIndex < preset.GetVector2s().GetCount(); ++vectorIndex)
	{
		const Vector2Type& vector2Type = preset.GetVector2s()[vectorIndex];
		const Vector2Type* parentVector2Type = parentPreset.GetVector2(vector2Type.GetIndex());

		if ( lockNonTextureVariables || (vector2Type.IsLocked() != parentVector2Type->IsLocked()) ||
			vector2Type.GetValue().IsEqual(parentVector2Type->GetValue()) == false )
		{
			vector2Array.PushAndGrow(vector2Type);
		}
	}

	if ( vector2Array.GetCount() > 0 )
	{
		fprintf(presetFile, "\t<Vec2s>\n");

		for(int vectorIndex = 0; vectorIndex < vector2Array.GetCount(); ++vectorIndex)
		{
			const Vector2Type& vector2Type = vector2Array[vectorIndex];
			fprintf(presetFile, "\t\t<Item>\n");
			fprintf(presetFile, "\t\t\t<Name>%s</Name>\n", vector2Type.GetName());
			fprintf(presetFile, "\t\t\t<VariableName>%s</VariableName>\n", vector2Type.GetVariableName());
			fprintf(presetFile, "\t\t\t<Locked value=\"true\"/>\n"); //Locked because this surface has overridden the value.
			fprintf(presetFile, "\t\t\t<Index value=\"%d\"/>\n", vector2Type.GetIndex()); 
			fprintf(presetFile, "\t\t\t<Value x=\"%f\" y=\"%f\"/>\n", vector2Type.GetValue().x, vector2Type.GetValue().y);
			fprintf(presetFile, "\t\t</Item>\n");
		}

		fprintf(presetFile, "\t</Vec2s>\n");
	}

	//Floats 
	atArray<FloatType> floatArray;
	for(int floatIndex = 0; floatIndex < preset.GetFloats().GetCount(); ++floatIndex)
	{
		const FloatType& floatType = preset.GetFloats()[floatIndex];
		const FloatType* parentFloatType = parentPreset.GetFloat(floatType.GetIndex());

		if ( lockNonTextureVariables || (floatType.IsLocked() != parentFloatType->IsLocked()) ||
			floatType.GetValue() != parentFloatType->GetValue() )
		{
			floatArray.PushAndGrow(floatType);
		}
	}

	if ( floatArray.GetCount() > 0 )
	{
		fprintf(presetFile, "\t<Floats>\n");

		for(int floatIndex = 0; floatIndex < floatArray.GetCount(); ++floatIndex)
		{
			const FloatType& floatType = floatArray[floatIndex];
			fprintf(presetFile, "\t\t<Item>\n");
			fprintf(presetFile, "\t\t\t<Name>%s</Name>\n", floatType.GetName());
			fprintf(presetFile, "\t\t\t<VariableName>%s</VariableName>\n", floatType.GetVariableName());
			fprintf(presetFile, "\t\t\t<Locked value=\"true\"/>\n"); //Locked because this surface has overridden the value.
			fprintf(presetFile, "\t\t\t<Index value=\"%d\"/>\n", floatType.GetIndex()); 
			fprintf(presetFile, "\t\t\t<Value value=\"%f\"/>\n", floatType.GetValue());
			fprintf(presetFile, "\t\t</Item>\n");
		}

		fprintf(presetFile, "\t</Floats>\n");
	}

	//Integers
	atArray<IntType> intArray;
	for(int intIndex = 0; intIndex < preset.GetInts().GetCount(); ++intIndex)
	{
		const IntType& intType = preset.GetInts()[intIndex];
		const IntType* parentIntType = parentPreset.GetInt(intType.GetIndex());

		if ( lockNonTextureVariables || (intType.IsLocked() != parentIntType->IsLocked()) ||
			intType.GetValue() != parentIntType->GetValue() )
		{
			intArray.PushAndGrow(intType);
		}
	}

	if ( intArray.GetCount() > 0 )
	{
		fprintf(presetFile, "\t<Ints>\n");

		for(int intIndex = 0; intIndex < intArray.GetCount(); ++intIndex)
		{
			const IntType& intType = intArray[intIndex];
			fprintf(presetFile, "\t\t<Item>\n");
			fprintf(presetFile, "\t\t\t<Name>%s</Name>\n", intType.GetName());
			fprintf(presetFile, "\t\t\t<VariableName>%s</VariableName>\n", intType.GetVariableName());
			fprintf(presetFile, "\t\t\t<Locked value=\"true\"/>\n"); //Locked because this surface has overridden the value.
			fprintf(presetFile, "\t\t\t<Index value=\"%d\"/>\n", intType.GetIndex()); 
			fprintf(presetFile, "\t\t\t<Value value=\"%f\"/>\n", intType.GetValue());
			fprintf(presetFile, "\t\t</Item>\n");
		}

		fprintf(presetFile, "\t</Ints>\n");
	}

	//Do not need to write out any properties on surface objects.

	fprintf(presetFile, "</MaterialPreset>\n");

	presetFile->Close();
	return true;
}