
#include "stdafx.h"

#define USE_STOCK_ALLOCATOR	1

#include "system/param.h"

#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <conio.h>

// RAGE headers
#include "system/xtl.h"
#include "system/main.h"
#include "paging/dictionary.h"
#include "parser/manager.h"
#include "grmodel/model.h"

#include "MaterialPresetsTemplateGenerator.h"

#include "MaterialPresets/MaterialPresetManager.h"

using namespace rage;

extern __THREAD int RAGE_LOG_DISABLE;

PARAM(shaderpath, "Shader path." );
PARAM(shaderdbpath, "Shader database path.");
PARAM(outputpath, "Preset output path.");
PARAM(shadername, "Shader to generate template for.");
PARAM(surfacePresets, "Generate surface presets.");

int Main()
{
	RAGE_LOG_DISABLE = 1;

	const char* shaderPath;
	const char* shaderDbPath;
	const char* outputPath;
	const char* shaderName = NULL;

	bool generateSurfacePresets = false;

	if (!PARAM_shaderpath.Get(shaderPath))
	{
		Errorf("Missing a required -shaderpath argument.");
		return -1;
	}

	if (!PARAM_shaderdbpath.Get(shaderDbPath))
	{
		Errorf("Missing a required -shaderdbpath variable.");
		return -1;
	}

	if (!PARAM_outputpath.Get(outputPath))
	{
		Errorf("Missing a required -outputpath variable.");
		return -1;
	}
	else
	{
		if (PARAM_surfacePresets.Get())
		{
			generateSurfacePresets = true;
		}
	}

	PARAM_shadername.Get(shaderName);

	MaterialPresetsTemplateGenerator templateGenerator(shaderPath, shaderDbPath, outputPath);

	if (shaderName == NULL)
	{
		templateGenerator.Initialize(true, generateSurfacePresets);
		return 0;
	}

	if (shaderName)
	{
		templateGenerator.Initialize(false, generateSurfacePresets);
		return templateGenerator.GenerateTemplateForShader(shaderName) ? 0 : 1;
	}

	return 0;
}

