// TextureExport.cpp : Defines the entry point for the console application.
//

// Local Headers
#include "utility.h"

// RageCore Headers
#include "file/asset.h"
#include "parser/manager.h"
#include "system/param.h"
using namespace rage;

// libtiff headers
#include "libtiff/tiff.h" // for compression constants

// stdlib Headers
//#include <tchar.h>
//#include <cstdlib>

#ifndef MAX_PATH
#define MAX_PATH (255)
#endif // MAX_PATH

PARAM( force, "Force texture export; otherwise timestamp checking done." );
PARAM( input, "Input texture(s) filename(s); in rexMax-syntax." );
PARAM( output, "Output texture filename (use extensions '.dds' or '.tif')." );

int 
_tmain( int argc, _TCHAR* argv[] )
{
	diagChannel::SetOutput( true );
	sysParam::Init( argc, argv );
	
	int exit_code = 0;

	const char* pInputFilenames = NULL;
	const char* pOutputFilename = NULL;
	
	if ( !PARAM_input.Get( pInputFilenames ) )
	{
		fprintf( stderr, "Invalid input file(s); use '-input <filename>' to specify. Aborting." );
		return ( 1 );
	}

	if ( !PARAM_output.Get( pOutputFilename ) )
	{
		fprintf( stderr, "Invalid output file; use '-output <filename>' to specify. Aborting." );
		return ( 1 );
	}

	if ( PARAM_force.Get() )
		textureExportUtility::SetForceTextureExport( true );
	else
		textureExportUtility::SetForceTextureExport( false );

	const char* pExtension = ASSET.FindExtensionInPath( pOutputFilename );
	if ( 0 == stricmp( pExtension, ".dds" ) )
	{
		if ( !textureExportUtility::ExportTexture( pInputFilenames, pOutputFilename, false, false, true ) )
			exit_code = 1;
	}
	else if ( ( 0 == stricmp( pExtension, ".tif" ) ) || ( 0 == stricmp( pExtension, ".tiff" ) ) )
	{
		if ( !textureExportUtility::ExportTextureAsTiff( pInputFilenames, pOutputFilename, COMPRESSION_LZW ) )
			exit_code = 1;
	}
	
	return ( exit_code );
}

