
#include "utility.h"

// DevIL headers
#include "devil/il.h"
#include "devil/ilu.h"

// libtiff headers
#include "libtiff/tiff.h"
#include "libtiff/tiffio.h"

// RAGE headers
#include "atl/string.h"
#include "file/asset.h"
#include "file/device.h"

#define BOOL int
#define TRUE 1
#define FALSE 0

using namespace rage;

static ILuint s_DevilConvertImageNames[2];
static bool s_DevilBegun = false;
static bool ForceTextureExport(false);

static inline bool isPow2(size_t x) { return (x & (x - 1)) == 0; }

void 
textureExportUtility::SetForceTextureExport(bool on)
{
	ForceTextureExport = on;
}

bool 
textureExportUtility::GetForceTextureExport( )
{
	return (ForceTextureExport);
}

bool 
textureExportUtility::TextureExportNeeded(const atString& inputFileName,const atString& outputFileName)
{
	if ( ForceTextureExport )
		return true;
	
	const fiDevice& device = fiDeviceLocal::GetInstance();
	u64 TimeOut = device.GetFileTime(outputFileName);

	if(TimeOut == 0)
		return true;

	char seperators[] = "+>";
	char buffer[8192];

	strcpy(buffer,inputFileName);
	char* pToken = strtok(buffer,seperators);

	while(pToken)
	{
		u64 TimeIn = device.GetFileTime(pToken);

		if(TimeIn == 0)
			return false;

		if(TimeIn > TimeOut)
			return true;

		pToken = strtok(NULL,seperators);
	}

	return false;
}

void 
textureExportUtility::BeginDevil()
{
	Assert(s_DevilBegun==false);

	ilInit();
	iluInit();
	s_DevilBegun=true;
	// We need to bind images in order to clean up their memory later
	ilGenImages(2, s_DevilConvertImageNames);
	ilBindImage(s_DevilConvertImageNames[0]);
	ilEnable(IL_CONV_PAL);
}

void 
textureExportUtility::EndDevil()
{
	Assert(s_DevilBegun==true);

	// Unbind this image & delete it
	ilBindImage(0);
	ilDeleteImages(2, s_DevilConvertImageNames) ;
	s_DevilBegun=false;
	ilShutDown();
}

bool 
textureExportUtility::LoadDevilImage(const char* fileName)
{
	bool exit_code = true;

	const char *plus = strchr(fileName,'+');
	const char *bracket = strchr(fileName,'>');

	if (bracket)
	{
		char buffer[8192];
		strcpy(buffer,fileName);

		char* p_name = strtok(buffer,">");

		if(p_name)
		{
			ilBindImage(s_DevilConvertImageNames[0]);
			if (ilLoadImage((ILstring) p_name)==FALSE)
			{
				exit_code = false;
				Errorf( "Texture conversion warning. Failed to load diffuse map '%s'", p_name );
				return exit_code;
			}

			if (ilConvertImage(IL_RGBA,IL_UNSIGNED_BYTE) == FALSE)
			{
				exit_code = false;
				Errorf( "Texture conversion warning. Unable to convert diffuse map '%s' to RGBA", p_name );
				return exit_code;
			}

			ILuint diffuseWidth = (ILuint) ilGetInteger(IL_IMAGE_WIDTH);
			ILuint diffuseHeight = (ILuint) ilGetInteger(IL_IMAGE_HEIGHT);

			ILubyte *diffuseData = ilGetData();
			diffuseWidth = (ILuint) ilGetInteger(IL_IMAGE_WIDTH);
			diffuseHeight = (ILuint) ilGetInteger(IL_IMAGE_HEIGHT);
			ILint diffuseFormat = ilGetInteger(IL_IMAGE_FORMAT);
			ILuint diffuseDepth = (ILuint) ilGetInteger(IL_IMAGE_DEPTH);
			ILuint count = diffuseWidth * diffuseHeight;
			while (count--)
			{
				diffuseData[0] = diffuseData[1];
				diffuseData[1] = 0;
				diffuseData[2] = 0;
				diffuseData[3] = 0xff;
				diffuseData += 4;
			}

			p_name = strtok(NULL,">");
			s32 MapCount = 1;

			while(p_name)
			{
				bool changedMainSize = false;

				ilBindImage(s_DevilConvertImageNames[1]);
				if (ilLoadImage((ILstring) p_name)==FALSE)
				{
					ilBindImage(s_DevilConvertImageNames[0]);
					exit_code = false;
					Errorf("Texture conversion warning.  Failed to load opacity map '%s'", p_name);
					return exit_code;
				}
				if (ilConvertImage(IL_RGBA,IL_UNSIGNED_BYTE) == FALSE)
				{
					exit_code = false;
					Errorf("Texture conversion warning. Unable to convert opacity map '%s' to RGBA", p_name);
					return exit_code;
				}

				ILuint opacityWidth = (ILuint) ilGetInteger(IL_IMAGE_WIDTH);
				ILuint opacityHeight = (ILuint) ilGetInteger(IL_IMAGE_HEIGHT);
				ILuint opacityDepth = (ILuint) ilGetInteger(IL_IMAGE_DEPTH);
				ILint opacityFormat = ilGetInteger(IL_IMAGE_FORMAT);

				if (opacityWidth != diffuseWidth || opacityHeight != diffuseHeight)
				{
					if(opacityWidth > diffuseWidth)
					{
						diffuseWidth = opacityWidth;
						changedMainSize = true;
					}
					if(opacityHeight > diffuseHeight)
					{
						diffuseHeight = opacityHeight;
						changedMainSize = true;
					}

					iluScale(diffuseWidth,diffuseHeight,opacityDepth);
				}

				ILubyte *opacityData = ilGetData();
				ilBindImage(s_DevilConvertImageNames[0]);

				// Check to make sure they're compatible
				if (changedMainSize)
				{
					iluImageParameter(ILU_FILTER,ILU_NEAREST);
					iluScale(diffuseWidth,diffuseHeight,diffuseDepth);
				}
				diffuseData = ilGetData();

				if (opacityFormat != IL_RGBA || diffuseFormat != IL_RGBA)
				{
					exit_code = false;
					Errorf("Texture conversion warning. Devil fn 'ilConvertImage' failed.");
					return exit_code;
				}

				// Merge alpha channel from opacity map into alpha channel of diffuse map
				ILuint count = diffuseWidth * diffuseHeight;
				while (count--)
				{
					diffuseData[MapCount] = opacityData[1];
					diffuseData += 4;
					opacityData += 4;
				}

				p_name = strtok(NULL,">");
				MapCount++;

				if(MapCount == 4)
					break;
			}
		}
	}
	else if (plus)
	{
		char diffuse[512], opacity[512];
		safecpy(diffuse, fileName, plus - fileName + 1);
		safecpy(opacity, plus + 1, sizeof(opacity));

		// Bind diffuse map
		ilBindImage(s_DevilConvertImageNames[0]);
		if (ilLoadImage((ILstring) diffuse)==FALSE)
		{
			exit_code = false;
			Errorf("Texture conversion warning. Failed to load diffuse map '%s'", diffuse);
			return exit_code;
		}

		if (ilConvertImage(IL_RGBA,IL_UNSIGNED_BYTE) == FALSE)
		{
			exit_code = false;
			Errorf("Texture conversion warning. Unable to convert diffuse map '%s' to RGBA", diffuse);
			return exit_code;
		}
		ILuint diffuseWidth = (ILuint) ilGetInteger(IL_IMAGE_WIDTH);
		ILuint diffuseHeight = (ILuint) ilGetInteger(IL_IMAGE_HEIGHT);

		iluImageParameter(ILU_FILTER,ILU_SCALE_LANCZOS3);
		iluScale(diffuseWidth,diffuseHeight,ilGetInteger(IL_IMAGE_DEPTH));

		ILubyte *diffuseData = ilGetData();
		diffuseWidth = (ILuint) ilGetInteger(IL_IMAGE_WIDTH);
		diffuseHeight = (ILuint) ilGetInteger(IL_IMAGE_HEIGHT);
		ILint diffuseFormat = ilGetInteger(IL_IMAGE_FORMAT);

		// Bind opacity map
		ilBindImage(s_DevilConvertImageNames[1]);
		if (ilLoadImage((ILstring) opacity)==FALSE)
		{
			ilBindImage(s_DevilConvertImageNames[0]);
			exit_code = false;
			Errorf("Texture conversion warning.  Failed to load opacity map '%s'", opacity);
			return exit_code;
		}
		if (ilConvertImage(IL_RGBA,IL_UNSIGNED_BYTE) == FALSE)
		{
			exit_code = false;
			Errorf("Texture conversion warning. Unable to convert opacity map '%s' to RGBA", opacity);
			return exit_code;
		}

		ILuint opacityWidth = (ILuint) ilGetInteger(IL_IMAGE_WIDTH);
		ILuint opacityHeight = (ILuint) ilGetInteger(IL_IMAGE_HEIGHT);
		ILuint opacityDepth = (ILuint) ilGetInteger(IL_IMAGE_DEPTH);
		ILint opacityFormat = ilGetInteger(IL_IMAGE_FORMAT);

		// Check to make sure they're compatible
		if (opacityWidth != diffuseWidth || opacityHeight != diffuseHeight)
		{
			iluImageParameter(ILU_FILTER,ILU_NEAREST);
			iluScale(diffuseWidth,diffuseHeight,opacityDepth);
		}

		ILubyte *opacityData = ilGetData();
		ilBindImage(s_DevilConvertImageNames[0]);

		if (opacityFormat != IL_RGBA || diffuseFormat != IL_RGBA)
		{
			exit_code = false;
			Errorf("Texture conversion warning. Devil fn 'ilConvertImage' failed.");
			return exit_code;
		}

		// Merge alpha channel from opacity map into alpha channel of diffuse map
		ILuint count = diffuseWidth * diffuseHeight;
		while (count--)
		{
			// TODO: Could trivially support premultiplied alpha here
			// diffuseData[0] = diffuseData[0];
			// diffuseData[1] = diffuseData[1];
			// diffuseData[2] = diffuseData[2];
			// TODO: This doesn't seem right; I'm taking green out of the opacity map
			// and feeding it into diffuse alpha because the opacity map itself doesn't
			// seem to have transparency.  Arguably I should be using 5/8 green, 2/8 red,
			// and 1/8 blue or some other YUV intensity approximation.
			diffuseData[3] = opacityData[1];
			diffuseData += 4;
			opacityData += 4;
		}

		return exit_code;
	}
	else
	{
		if(ilLoadImage((ILstring) (const char *)fileName))
		{
			if (ilConvertImage(IL_RGBA,IL_UNSIGNED_BYTE) == FALSE)
			{
				exit_code = false;
				Errorf("Texture conversion warning. Unable to convert diffuse map '%s' to RGBA", fileName);
				return exit_code;
			}

			ILuint diffuseWidth = (ILuint) ilGetInteger(IL_IMAGE_WIDTH);
			ILuint diffuseHeight = (ILuint) ilGetInteger(IL_IMAGE_HEIGHT);

			iluScale(diffuseWidth,diffuseHeight,ilGetInteger(IL_IMAGE_DEPTH));
			return exit_code;
		}
		else
		{
			exit_code = false;
			Errorf("Texture conversion warning. Failed to load image '%s'", fileName);
			return exit_code;
		}
	}

	return exit_code;
}

bool 
textureExportUtility::SaveDevilImage(const char* outputFileName)
{
	bool exit_code = true;

	ilSetInteger(IL_DXTC_FORMAT, IL_DXT_NO_COMP);					// Set the dxt format
	ilEnable(IL_FILE_OVERWRITE);							// Set this to overwrite existing file
	iluImageParameter(ILU_FILTER, ILU_NEAREST);	
	
	ILboolean bSaveRes = ilSaveImage((ILstring) (const char *)outputFileName);	// USE THIS VERSION OF SAVE!  other versions don't
	if(!bSaveRes)
	{
		exit_code = false;
		Errorf("Texture conversion warning. Failed to write converted texture '%s'", outputFileName);
		return exit_code;
	}
	else
	{
		Displayf( "Texture '%s' written.", outputFileName );
	}

	return exit_code;
}

//
// Use libtiff to write out a TIFF file; DevIL TIFF compression doesn't work.  *sigh*.
// This does not respect the rexTextureShift parameter for scaling; that parameter
// is needing to get removed anyway.
//
bool 
textureExportUtility::SaveDevilImageAsTiff( const char* outputFileName, int tiff_compression )
{
	bool exit_code = true;

	// DevIL image is already in RGBA format; byte-per-channel from the 
	// LoadDevilImage call that loaded the source texture from disk.
	const int samplesPerPixel = 4;
	ILuint width = ilGetInteger( IL_IMAGE_WIDTH );
	ILuint height = ilGetInteger( IL_IMAGE_HEIGHT );
	ILubyte* pImageData = ilGetData( );
	if ( !pImageData )
	{
		exit_code = false;
		Errorf( "Failed to read DevIL image data; no data returned.  %s not saved.",
			outputFileName );
		return (exit_code);
	}

	TIFF* pTif = TIFFOpen( outputFileName, "w" );
	if ( pTif )
	{
		TIFFSetField( pTif, TIFFTAG_COMPRESSION, tiff_compression );
		TIFFSetField( pTif, TIFFTAG_IMAGEWIDTH, width );
		TIFFSetField( pTif, TIFFTAG_IMAGELENGTH, height );
		TIFFSetField( pTif, TIFFTAG_SAMPLESPERPIXEL, samplesPerPixel );
		TIFFSetField( pTif, TIFFTAG_BITSPERSAMPLE, 8 );
		TIFFSetField( pTif, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG );
		TIFFSetField( pTif, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_RGB );
		
		// http://old.nabble.com/TIFFWriteEncodedStrip-LZW-compression-failing-for-certain-input-td20703401.html
		//TIFFSetField( pTif, TIFFTAG_PREDICTOR, PREDICTOR_HORIZONTAL ); 

		ILinfo info;
		iluGetImageInfo( &info );
		switch ( info.Origin )
		{
		case IL_ORIGIN_LOWER_LEFT:
			TIFFSetField( pTif, TIFFTAG_ORIENTATION, ORIENTATION_BOTLEFT );
			break;
		case IL_ORIGIN_UPPER_LEFT:
			TIFFSetField( pTif, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT );
			break;
		}
		if ( 0 == TIFFWriteEncodedStrip( pTif, 0, pImageData, width * height * samplesPerPixel ) )
		{
			exit_code = false;
			Errorf( "Failed to write TIFF image data for: %s.", outputFileName );
		}
		TIFFClose( pTif );
	}
	else
	{
		exit_code = false;
		Errorf( "Failed to create TIFF image for: %s.", outputFileName );
	}

	return exit_code;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
bool 
textureExportUtility::ExportTexture(const char* inputFileName, const char* outputFileName, bool hasAlpha, bool isBumpMap, bool isRaw)
{
	bool exit_code = true;
	char seperators[] = "+>";
	char Input[8192];
	char buffer[8192];
	bool isAlpha = true;
	bool isFirst = true;

	Input[0] = '\0';

	if(strstr(inputFileName,">"))
		isAlpha = false;

	strcpy(buffer,inputFileName);
	char* pTextureName = strtok(buffer,seperators);
	atString lastTexturename;

	while(pTextureName)
	{
		bool Found = false;
		char FoundPath[8192];

		lastTexturename = pTextureName;

		if(ASSET.Exists(pTextureName,""))
		{
			strcpy(FoundPath,pTextureName);
			Found = true;
		}
		
		if(Found)
		{
			if(!isFirst)
			{
				if(isAlpha)
					strcat(Input,"+");
				else
					strcat(Input,">");
			}

			isFirst = false;

			strcat(Input,FoundPath);
		}

		pTextureName = strtok(NULL,seperators);
	}

	if(!TextureExportNeeded(atString(Input),atString(outputFileName)))
		return exit_code;

	if(strlen(Input)==0)
	{
		exit_code = false;
		Errorf("Input texture not found: %s.", lastTexturename.c_str());
		return exit_code;
	}

	BeginDevil();

	try
	{
		// Now convert
		exit_code = ( LoadDevilImage(Input) );
		if (exit_code)
		{
			//First perform some basic validation on the image size
			ILuint width = ilGetInteger(IL_IMAGE_WIDTH);
			ILuint height = ilGetInteger(IL_IMAGE_HEIGHT);

			//(1) Make sure the texture width and height are a power of 2
			if(!isPow2((int)width))
			{
				exit_code = false;
				Errorf("Texture conversion error. The image '%s' has a width of %d which is not a power of 2, texture will not be converted.",
					(const char*)Input, width);
			}
			if(!isPow2((int)height))
			{
				exit_code = false;
				Errorf("Texture conversion error. The image '%s' has a height of %d which is not a power of 2, texture will not be converted.",
					(const char*)Input, height);
			}
						
			if(width < 4 || height < 4)
			{
				Errorf("Height or width of texture is less than 4 pixels.  This is not supported: %s.", inputFileName);
				return exit_code;
			}
			
			if (exit_code)
			{
				if(isRaw)
				{
					exit_code = SaveDevilImage(outputFileName);
				}
				else
				{
					exit_code = false;
					Errorf( "Nothing to save." );
				}
			}
		}
	}
	catch(...)
	{
		//there are some exceptions being triggered by the devil library
		//this catches them so the export doesnt crash...
		//...obviously there should be some handling for this but I'd
		//rather the exporter didnt fall over because of it
		exit_code = false;
		Errorf("Texture conversion warning. The image '%s' has caused an exception in devil.",(const char*)Input);
	}

	EndDevil();

	return exit_code;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
bool
textureExportUtility::ExportTextureAsTiff( const char* inputFileName, const char* outputFileName, int compression )
{
	bool exit_code = true;
	char seperators[] = "+>";
	char Input[8192];
	char buffer[8192];
	bool isAlpha = true;
	bool isFirst = true;
	
	Input[0] = '\0';

	if(strstr(inputFileName,">"))
		isAlpha = false;

	strcpy(buffer,inputFileName);
	char* pTextureName = strtok(buffer,seperators);
	atString lastTexturename;

	while(pTextureName)
	{
		bool Found = false;
		char FoundPath[8192];

		lastTexturename = pTextureName;

		if(ASSET.Exists(pTextureName,""))
		{
			strcpy(FoundPath,pTextureName);
			Found = true;
		}

		if(Found)
		{
			if(!isFirst)
			{
				if(isAlpha)
					strcat(Input,"+");
				else
					strcat(Input,">");
			}

			isFirst = false;

			strcat(Input,FoundPath);
		}

		pTextureName = strtok(NULL,seperators);
	}

	if (!TextureExportNeeded(atString(Input),atString(outputFileName)))
		return exit_code;

	if(strlen(Input)==0)
	{
		exit_code = false;
		Errorf("Input texture not found: %s.", lastTexturename.c_str());
		return exit_code;
	}

	BeginDevil();

	try
	{
		// Now convert
		exit_code = LoadDevilImage(Input);
		if (exit_code)
		{
			//First perform some basic validation on the image size
			ILuint width = ilGetInteger(IL_IMAGE_WIDTH);
			ILuint height = ilGetInteger(IL_IMAGE_HEIGHT);

			//(1) Make sure the texture width and height are a power of 2
			if(!isPow2((int)width))
			{
				exit_code = false;
				Errorf("Texture conversion error. The image '%s' has a width of %d which is not a power of 2, texture will not be converted.",
					(const char*)Input, width);
			}
			if(!isPow2((int)height))
			{
				exit_code = false;
				Errorf("Texture conversion error. The image '%s' has a height of %d which is not a power of 2, texture will not be converted.",
					(const char*)Input, height);
			}

			if(width < 4 || height < 4)
			{
				Errorf("Height or width of texture is less than 4 pixels.  This is not supported: %s.", inputFileName);
				return exit_code;
			}

			iluScale(width,height,ilGetInteger(IL_IMAGE_DEPTH));
			
			if (exit_code)
			{
				exit_code = SaveDevilImageAsTiff(outputFileName, compression);
			}
		}
	}
	catch(...)
	{
		//there are some exceptions being triggered by the devil library
		//this catches them so the export doesnt crash...
		//...obviously there should be some handling for this but I'd
		//rather the exporter didnt fall over because of it

		exit_code = false;
		Errorf("Texture conversion warning. The image '%s' has caused an exception in devil.",(const char*)Input);
	}

	EndDevil();

	return exit_code;
}


