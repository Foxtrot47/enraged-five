#ifndef __TEXTUREEXPORT_UTILITY_H__
#define __TEXTUREEXPORT_UTILITY_H__

#include "atl/string.h"

/* PURPOSE:
	collection of utility functions for commomly used tasks in max
*/
namespace textureExportUtility
{
	void SetForceTextureExport(bool on);
	bool GetForceTextureExport( );

	bool TextureExportNeeded(const ::rage::atString& inputFileName,
		const ::rage::atString& outputFileName);

	void BeginDevil();
	void EndDevil();

	bool LoadDevilImage(const char* fileName);
	
	bool SaveDevilImage(const char* outputFileName );
	bool SaveDevilImageAsTiff(const char* outputFileName, int tiff_compression );
	
	bool ExportTexture(const char* inputFileName, const char* outputFileName, bool hasAlpha, bool isBumpMap, bool isRaw);

	bool ExportTextureAsTiff( const char* inputFileName, const char* outputFileName, int compression );
	
} // textureExportUtility namespace

#endif //__TEXTUREEXPORT_UTILITY_H__
