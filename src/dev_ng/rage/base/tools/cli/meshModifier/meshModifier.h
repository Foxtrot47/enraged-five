// 
// meshModifier/meshModifier.h 
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved. 
// 
#ifndef _MESHMODIFIER_H
#define	_MESHMODIFIER_H

#include "vector/matrix34.h"
#include "vector/quaternion.h"

namespace rage {

class mshMesh;


enum texAddressMode
{
	eTexAddress_wrap		= 0,
	eTexAddress_mirror		= 1,
	eTexAddress_clamp		= 2,
	eTexAddress_border		= 3,
	eTexAddress_clamptoedge	= 4,
	eNumTexAddress_modes    = 5
};

class uvCroppingObject {

private:
	int		m_materialID;
	int		m_uvChannelID;

	texAddressMode	addr_mode_u;
	texAddressMode	addr_mode_v;

	Vector2	m_src_min_uv;
	Vector2 m_src_max_uv;

	bool	m_isCrossingTileEdge_u;
	bool	m_isCrossingTileEdge_v;

	Vector2	m_scale_uv;
	Vector2 m_offset_uv;

	Vector2	m_dst_min_uv;
	Vector2 m_dst_max_uv;
};


class meshModifier {
public:
	// wrappers to manipulate mesh data
	static	int			GetMeshNumMaterials( const mshMesh &m); 
	static	int			GetMeshNumUVChannels( const mshMesh &m, const int materialID );

	static	bool		SaveMeshFile( const char* meshFileName, mshMesh* mesh, const bool IsSavingBinaryFormat );
	static	mshMesh*	LoadMeshFile( const char* meshFileName );

	static	void		GetMeshMaterialUVExtents( const mshMesh &m, Vector2 &min_uv, Vector2 &max_uv, const int materialID, const int uvChannel );

	static	bool		SetMeshMaterialCPVData( mshMesh &m, const int materialID, const Vector4 &color,
												bool maskRed   = false,
												bool maskGreen = false,
												bool maskBlue  = false,
												bool maskAlpha = false );

	static	bool		OptimizeMeshMaterialUVs( mshMesh &m, const int materialID, const int uvChannel, texAddressMode /* address_mode_u */, texAddressMode /* address_mode_v  */,
												Vector2 dstExtents_min, Vector2 dstExtents_max);

	static	bool		IsCrossingTileEdgeBoundary( float min_u, float max_u, texAddressMode texaddrmode = eTexAddress_wrap );

	static	bool		ScaleOffsetMeshMaterialUVs( mshMesh &m, const int materialID, const int uvChannel, Vector2 &scale_uv, Vector2 &offset_uv, const bool mask_u, const bool mask_v  );

	static	void		SetVerbose( bool isVerbose )	{sm_isVerbose = isVerbose;}
	static	bool		IsVerbose( void )				{return sm_isVerbose;}

private:
	static	bool		sm_isVerbose;

};



}


#endif
