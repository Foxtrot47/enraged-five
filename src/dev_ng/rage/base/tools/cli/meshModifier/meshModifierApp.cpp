// 
// meshModifier/meshModifierApp.cpp 
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved. 
// 


#include "system/main.h"

#include "atl/array.h"
#include "file/asset.h"
#include "file/device.h"
#include "file/token.h"
#include "grcore/texture.h"
#include "grcore/texturedefault.h"
#include "grcore/texturexenonproxy.h"
#include "grmodel/modelfactory.h"
#include "grmodel/geometry.h"
#include "paging/dictionary.h"
#include "phbound/bound.h"
#include "phcore/materialmgrimpl.h"
#include "rmcore/drawable.h"
#include "rmcore/typefileparser.h"
#include "system/param.h"
#include "system/timer.h"
#include "vector/matrix34.h"
#include "vector/quaternion.h"
#include "zlib/zlib.h"

#include "mesh/mesh.h"
#include "mesh/serialize.h"

#include <vector>

#include "meshModifier.h"

using namespace rage;



REQ_PARAM( input,	"source meshfile" );
REQ_PARAM( output,	"name of the output meshfile" );

PARAM( optimizeuntiledmaterial,	"specify material id uv set to renormalized if it isn't tiled in 'u' and/or 'v' dimension");
PARAM( texcoordset,				"texture coordinate set to modify in specified material");
PARAM( optimizeoutput,			"outputfile to store optimization information");
PARAM( forceoutputmesh,			"create output meshfile even if optimization did nothing.");
PARAM( urange,					"Output range for U values (default is -urange=0,1)");
PARAM( vrange,					"Output range for V values (default is -vrange=0,1)");

PARAM( addr_mode_u,			"texture addressing mode for u channel (default is 'eTexAddress_wrap') [ eTexAddress_wrap ][ eTexAddress_mirror ][ eTexAddress_clamp ][ eTexAddress_border ][ eTexAddress_clamptoedge ] ");
PARAM( addr_mode_v,			"texture addressing mode for v channel (default is 'eTexAddress_wrap') [ eTexAddress_wrap ][ eTexAddress_mirror ][ eTexAddress_clamp ][ eTexAddress_border ][ eTexAddress_clamptoedge ] ");

PARAM( asciimode,			"saves mesh file to ascii format if this is specified, (default is binary)");
PARAM( verbose,				"umm.. the tool is verbose");

PARAM( multiprocess,		"allows for a file to be specified that will allow batching of configs -- format = MinU MinV MaxU MaxV MatIndex UvSet");

PARAM( file, "[ignore] unused");

const char *input	= "unoptimized.mesh";
const char *output	= "optimized.mesh";
const char *optimizeoutput= "modifierOutput.txt";

const char *addr_mode_str_u = "eTexAddress_wrap";
const char *addr_mode_str_v = "eTexAddress_wrap";

// match enum order of 'texAddressMode'
char *addr_mode_strings[]={
	"eTexAddress_wrap",
	"eTexAddress_mirror",
	"eTexAddress_clamp"	,
	"eTexAddress_border",	
	"eTexAddress_clamptoedge"
	};

struct MultiprocessOptions
{
	float uMin, vMin, uMax, vMax;
	int materialID;
	int uvSet;
};

//if( PARAM_x.GetArray(newPos,3)==3)

texAddressMode	GetTexAddressEnumFromString( const char *str )
{
	int num_modes = (int)eNumTexAddress_modes;

	for(int i=0; i < num_modes; i++ )
	{
		if( 0 == strcmp( str, addr_mode_strings[i]) )
		{
			return (texAddressMode)i;
		}
	}

	// Default if no match found.
	return eTexAddress_wrap;
}

char * GetStringFromAddressEnum( texAddressMode eMode )
{
	return addr_mode_strings[ (int)eMode ];
}


int Main()
{
	bool	isSavingAscii			= PARAM_asciimode.Get();
	int		materialIDtoOptimize	= -1;
	int		texcoordset				= 0;
	bool	forceoutputmesh			= false;

	texAddressMode	addr_mode_u = eTexAddress_wrap;
	texAddressMode	addr_mode_v = eTexAddress_wrap;

	forceoutputmesh = PARAM_forceoutputmesh.Get();

	PARAM_texcoordset.Get(texcoordset);
	PARAM_optimizeuntiledmaterial.Get(materialIDtoOptimize);
	PARAM_optimizeoutput.Get(optimizeoutput);
	PARAM_input.Get(input);
	PARAM_output.Get(output);

	meshModifier::SetVerbose( PARAM_verbose.Get() );

	if( PARAM_addr_mode_u.Get() )
	{
		PARAM_addr_mode_u.Get(addr_mode_str_u);
		addr_mode_u = GetTexAddressEnumFromString( addr_mode_str_u );
	}

	if( PARAM_addr_mode_v.Get() )
	{
		PARAM_addr_mode_v.Get(addr_mode_str_v);
		addr_mode_v = GetTexAddressEnumFromString( addr_mode_str_v );
	}


	mshMesh	*pMesh = NULL;

	pMesh = meshModifier::LoadMeshFile( input );

	//verify everything loaded ok and we have all the data we need.
	Assert( pMesh );

	Vector2 srcExtents_min;
	Vector2 srcExtents_max;

	Vector2 dstExtents_min;
	Vector2 dstExtents_max;
	dstExtents_min.Set(0.0f);
	dstExtents_max.Set(1.0f);

	Vector2 norm_uv_min_output;
	Vector2 norm_uv_max_output;

	if (PARAM_urange.Get())
	{
		float f[2];
		PARAM_urange.GetArray(f, 2);
		dstExtents_min.x = f[0];
		dstExtents_max.x = f[1];
	}

	if (PARAM_vrange.Get())
	{
		float f[2];
		PARAM_vrange.GetArray(f, 2);
		dstExtents_min.y = f[0];
		dstExtents_max.y = f[1];
	}

	bool didOptimizeUVs = false;
	bool isCrossingTile_u = false;
	bool isCrossingTile_v = false;
	
	const char* multiprocessName;
	std::vector<MultiprocessOptions> multiProcs;
	if (PARAM_multiprocess.Get(multiprocessName))
	{
		fiSafeStream S(ASSET.Open(multiprocessName, "txt"));
		if (S)
		{
			fiTokenizer tok(multiprocessName, S);
			while(!tok.EndOfFile())
			{
				MultiprocessOptions opts;
				opts.uMin = tok.GetFloat();
				opts.vMin = tok.GetFloat();
				opts.uMax = tok.GetFloat();
				opts.vMax = tok.GetFloat();
				opts.materialID = tok.GetInt();
				opts.uvSet = tok.GetInt();
				multiProcs.push_back(opts);
			}
		}

		size_t count = multiProcs.size();
		for(size_t i = 0; i < count; i++)
		{
			MultiprocessOptions &opts = multiProcs[i];
			dstExtents_min.x = opts.uMin;
			dstExtents_max.x = opts.uMax;
			dstExtents_min.y = opts.vMin;
			dstExtents_max.y = opts.vMax;
			materialIDtoOptimize = opts.materialID;
			texcoordset = opts.uvSet;

			Assert( materialIDtoOptimize >= 0 );
			Assert( materialIDtoOptimize < meshModifier::GetMeshNumMaterials( *pMesh ) );

			meshModifier::GetMeshMaterialUVExtents( *pMesh, srcExtents_min, srcExtents_max, materialIDtoOptimize, texcoordset );

			// test extents to see if they cross a tiling edge boundary.
			isCrossingTile_u = meshModifier::IsCrossingTileEdgeBoundary( srcExtents_min.x, srcExtents_max.x );
			isCrossingTile_v = meshModifier::IsCrossingTileEdgeBoundary( srcExtents_min.y, srcExtents_max.y );

			didOptimizeUVs &= meshModifier::OptimizeMeshMaterialUVs( *pMesh, materialIDtoOptimize, texcoordset, addr_mode_u, addr_mode_v, dstExtents_min, dstExtents_max );

			Displayf("Process %d of %d", i, count );
			Displayf("Material %d ", materialIDtoOptimize );
			Displayf("texcoordset %d ", texcoordset );
			Displayf("texAddress_u %s ", GetStringFromAddressEnum(addr_mode_u) );
			Displayf("texAddress_v %s ", GetStringFromAddressEnum(addr_mode_v) );
			Displayf("optimized %s ", didOptimizeUVs ? "true" : "false");
			Displayf("isCrossingTile_u %s ", isCrossingTile_u ? "true" : "false");
			Displayf("isCrossingTile_v %s ", isCrossingTile_v ? "true" : "false");
			Displayf("src_min_uv %f %f ", srcExtents_min.x, srcExtents_min.y );
			Displayf("src_max_uv %f %f ", srcExtents_max.x, srcExtents_max.y );
		}
	}
	else
	{
		Assert( materialIDtoOptimize >= 0 );
		Assert( materialIDtoOptimize < meshModifier::GetMeshNumMaterials( *pMesh ) );

		meshModifier::GetMeshMaterialUVExtents( *pMesh, srcExtents_min, srcExtents_max, materialIDtoOptimize, texcoordset );

		// test extents to see if they cross a tiling edge boundary.
		isCrossingTile_u = meshModifier::IsCrossingTileEdgeBoundary( srcExtents_min.x, srcExtents_max.x );
		isCrossingTile_v = meshModifier::IsCrossingTileEdgeBoundary( srcExtents_min.y, srcExtents_max.y );

		didOptimizeUVs = meshModifier::OptimizeMeshMaterialUVs( *pMesh, materialIDtoOptimize, texcoordset, addr_mode_u, addr_mode_v, dstExtents_min, dstExtents_max );
		Displayf("Material %d ", materialIDtoOptimize );
		Displayf("texcoordset %d ", texcoordset );
		Displayf("texAddress_u %s ", GetStringFromAddressEnum(addr_mode_u) );
		Displayf("texAddress_v %s ", GetStringFromAddressEnum(addr_mode_v) );
		Displayf("optimized %s ", didOptimizeUVs ? "true" : "false");
		Displayf("isCrossingTile_u %s ", isCrossingTile_u ? "true" : "false");
		Displayf("isCrossingTile_v %s ", isCrossingTile_v ? "true" : "false");
		Displayf("src_min_uv %f %f ", srcExtents_min.x, srcExtents_min.y );
		Displayf("src_max_uv %f %f ", srcExtents_max.x, srcExtents_max.y );
	}

	// write results to output file.
	fiStream *pStream = ASSET.Create( optimizeoutput,"txt");

	// Verify the master list file was created(opened)
	if (!pStream)
	{
		Quitf("Unable to create '%s/%s.txt",ASSET.GetPath(), optimizeoutput);
	}

	fprintf(pStream,"Material %d \n", materialIDtoOptimize );
	fprintf(pStream,"texcoordset %d \n", texcoordset );

	fprintf(pStream,"texAddress_u %s \n", GetStringFromAddressEnum(addr_mode_u) );
	fprintf(pStream,"texAddress_v %s \n", GetStringFromAddressEnum(addr_mode_v) );

	fprintf(pStream,"optimized %s \n", didOptimizeUVs ? "true" : "false");
	fprintf(pStream,"isCrossingTile_u %s \n", isCrossingTile_u ? "true" : "false");
	fprintf(pStream,"isCrossingTile_v %s \n", isCrossingTile_v ? "true" : "false");
	fprintf(pStream,"src_min_uv %f %f \n", srcExtents_min.x, srcExtents_min.y );
	fprintf(pStream,"src_max_uv %f %f \n", srcExtents_max.x, srcExtents_max.y );

	pStream->Close();

	// save output mesh file.
	if( forceoutputmesh || didOptimizeUVs )
	{
		meshModifier::SaveMeshFile( output, pMesh, !isSavingAscii );
	}

	delete pMesh;

	return 0;
}
