// 
// meshModifier/meshModifier.cpp 
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved. 
// 
#include "meshModifier.h"

#include "mesh/mesh.h"
#include "mesh/serialize.h"
#include "file/asset.h"

#include "atl/array.h"

#include "file/device.h"
#include "file/token.h"
#include "grcore/texture.h"
#include "grcore/texturedefault.h"
#include "grmodel/modelfactory.h"
#include "grmodel/geometry.h"

#include "phcore/materialmgrimpl.h"
#include "rmcore/drawable.h"
#include "rmcore/typefileparser.h"
#include "system/param.h"
#include "system/timer.h"
#include "vector/matrix34.h"
#include "vector/quaternion.h"
#include "zlib/zlib.h"


using namespace rage;


bool meshModifier::sm_isVerbose;


int	meshModifier::GetMeshNumMaterials( const mshMesh &m)
{
	return m.GetMtlCount();
}

int meshModifier::GetMeshNumUVChannels( const mshMesh &m, const int materialID )
{
	return m.GetMtl( materialID ).TexSetCount;
}

// returns the min/max extents of a material's particular uv channel.
void	meshModifier::GetMeshMaterialUVExtents( const mshMesh &m, Vector2 &min_uv, Vector2 &max_uv, const int materialID, const int uvChannel )
{
	Assert( materialID >= 0 );

	// initialize extents.
	min_uv.x = FLT_MAX;
	min_uv.y = FLT_MAX;
	max_uv.x = -FLT_MAX;
	max_uv.y = -FLT_MAX;

	int vertCount = m.GetMtl(materialID).GetVertexCount();

	for( int vertIdx = 0; vertIdx < vertCount; vertIdx++ )
	{
		Vector2	uvData = m.GetMtl(materialID).GetTexCoord( vertIdx, uvChannel );

		min_uv.x = Min( min_uv.x, uvData.x );
		min_uv.y = Min( min_uv.y, uvData.y );

		max_uv.x = Max( max_uv.x, uvData.x );
		max_uv.y = Max( max_uv.y, uvData.y );
	}
}

bool meshModifier::SetMeshMaterialCPVData( mshMesh &m, const int materialID, const Vector4 &color,  bool maskRed,  bool maskGreen,  bool maskBlue,  bool maskAlpha )
{
	if( materialID > m.GetMtlCount() ) return false;

	for (int j=0; j< m.GetMtl( materialID ).GetVertexCount() ; j++)
	{

		Vector4	output = m.GetMtl( materialID ).GetCpv( j );

		if( !maskRed ) output.x = color.x;

		if( !maskGreen ) output.y = color.y;

		if( !maskBlue ) output.z = color.z;

		if( !maskAlpha ) output.w = color.w;

		m.GetMtl( materialID ).SetCpv( j, output );
	}

	return true;
}

bool	meshModifier::SaveMeshFile( const char* meshFileName, mshMesh* mesh, const bool IsSavingBinaryFormat )
{
	if( IsSavingBinaryFormat)
	{
		if(sm_isVerbose) Displayf("Saving '%s' to binary", meshFileName );

		SerializeToBinaryFile( meshFileName , *mesh );
	}
	else
	{
		if(sm_isVerbose) Displayf("Saving '%s' to ascii", meshFileName);

		SerializeToAsciiFile( meshFileName , *mesh );
	}

	return true;
}



mshMesh*	meshModifier::LoadMeshFile( const char* meshFileName )
{
	mshMesh* srcMesh = rage_new mshMesh;

	if ( SerializeFromFile(meshFileName, *srcMesh, "mesh") == false ) 
	{
		Errorf("Can't load mesh file %s", meshFileName);
		delete srcMesh;
		return NULL;
	}

	return srcMesh;
}

bool meshModifier::OptimizeMeshMaterialUVs( mshMesh &m, const int materialID, const int uvChannel, texAddressMode /* address_mode_u */, texAddressMode /* address_mode_v  */,
										   Vector2 dstExtents_min, Vector2 dstExtents_max)
{
	// get un-normalized extents first
	Vector2	srcExtents_min;
	Vector2 srcExtents_max;

	GetMeshMaterialUVExtents( m, srcExtents_min, srcExtents_max, materialID, uvChannel );

	if( sm_isVerbose )
	{
		Displayf(" [ materialID=%d ] [uvChannel=%d ] [min %f %f ] [ max %f %f ]", materialID, uvChannel, srcExtents_min.x, srcExtents_min.y,
			srcExtents_max.x, srcExtents_max.y );
	}

	// test extents to see if they cross a tiling edge boundary.
	bool isCrossingTile_u = IsCrossingTileEdgeBoundary( srcExtents_min.x, srcExtents_max.x );
	bool isCrossingTile_v = IsCrossingTileEdgeBoundary( srcExtents_min.y, srcExtents_max.y );

	// Don't remap if there is no range to remap from
	if (srcExtents_max.x == srcExtents_min.x)
	{
		isCrossingTile_u = true;
	}
	if (srcExtents_max.y == srcExtents_min.y)
	{
		isCrossingTile_v = true;
	}

	if( isCrossingTile_u && isCrossingTile_v ) return false;// nothing changed - early out!

	// Intialize to identity transform, that way if one/both of the uv extents is crossing a tile and shouldn't be renormalized,
	//	we can ensure other parts of code that blindly apply transforms to both components dont really change anything.
	Vector2	scale_uv;
	Vector2	offset_uv;

	scale_uv.Set(1.0f);
	offset_uv.Set(0.0f);

	// compute scale/offset to remap them in each dimension if it isnt crossing a tile boundary.
	// The remapping below is going to be t_out = t_in * scale + offset, so we need scale and offset
	// from srcExtents and dstExtents

	//  (t_in - src_min)
	//  ----------------   * (dst_max - dst_min) + dst_max = t_out
	// (src_max - src_min)

	//  t_in * (dstRange / srcRange) - (src_min) * (dstRange / srcRange) + dst_min = t_out

	// scale = dstRange / srcRange
	// offset = dst_min - src_min * scale

	Vector2 srcRange, dstRange;
	srcRange.Subtract(srcExtents_max, srcExtents_min);
	dstRange.Subtract(dstExtents_max, dstExtents_min);

	if( !isCrossingTile_u )
	{
		scale_uv.x = dstRange.x / srcRange.x;
		offset_uv.x = dstExtents_min.x - srcExtents_min.x * scale_uv.x;
	}

	if( !isCrossingTile_v )
	{
		scale_uv.y = dstRange.y / srcRange.y;
		offset_uv.y = dstExtents_min.y - srcExtents_min.y * scale_uv.y;
	}

	// apply offset, then scale to bring uv's into 0-1 range as long as its not tiled (spanning more than 1.0 in a dimension)
	meshModifier::ScaleOffsetMeshMaterialUVs( m, materialID, uvChannel, scale_uv, offset_uv, !isCrossingTile_u, !isCrossingTile_v );

	return true;
}



bool	meshModifier::ScaleOffsetMeshMaterialUVs( mshMesh &m, const int materialID, const int uvChannel, Vector2 &scale_uv, Vector2 &offset_uv, const bool modify_u, const bool modify_v  )
{
	if( (!modify_u) && (!modify_v)) return true;// no-op

	// verify the uvChannel we want to modify exists first!
	Assert( m.GetMtl( materialID ).TexSetCount > uvChannel );

	// verify material index is also valid!
	Assert( materialID < m.GetMtlCount() );

	// Just simply run through all uv's, apply offset then scale and write back
	int	vertCount = m.GetMtl(materialID).GetVertexCount();

	for( int vertIdx = 0; vertIdx < vertCount; vertIdx++ )
	{
		Vector2	uvData = m.GetMtl(materialID).GetTexCoord(vertIdx, uvChannel );

		// modify unmasked channel portions
		if( modify_u )
		{
			uvData.x *= scale_uv.x;
			uvData.x += offset_uv.x;
//			uvData.x = Lerp(Range(uvData.x, inMin, inMax), outMin, outMax);
		}

		if(modify_v)
		{
			uvData.y *= scale_uv.y;
			uvData.y += offset_uv.y;
		}

		// Writeback
		m.GetMtl(materialID).SetTexCoord( vertIdx, uvChannel, uvData );
	}

	return true;
}

bool meshModifier::IsCrossingTileEdgeBoundary( float min_u, float max_u, texAddressMode /* texaddrmode */ )
{
	Assert( min_u <= max_u );

	// NOTE: This code is all written assuming 'D3DTADDRESS_WRAP' mapping.
	// if you're using something else, its more likely u+v extents will return 'true'.

	// TODO: handle stuff like mirrored tiling.

	// 1st test if we're crossing the origin, in 'D3DTADDRESS_WRAP' mode something like -.5 to .5 means it crosses the entire texture.
	// if you were doing mirrored mode then -.5 to .5 really only uses 0->.5 in normalized texture space - see why supporting multiple mapping modes
	// complicates this code a ton? you'll have to pass in a mapping mode enumeration and a lot more testing if you want to handle those other cases correctly.
	//
	if( (min_u < 0.f) && (max_u > 0.f) )
	{
		// have to tag it as crossing if we're in 'D3DTADDRESS_WRAP' mode
		return true;
	}

	// Test if we are crossing any other tile edge boundaries.
	if( max_u > 0.f )// extending into positive coordinates???
	{
		Assert( min_u >= 0.f );// 'this assert should not trigger since we handled above case of crossing '0'.

		// slide 'min_u' so it ends up in the 0<->1 range so we can normalize it easier. (e.g. a min value of 1.5 would become .5)

		// values are positive so the behavior of 'floor' is... 1.5 is .5 so...
		float whole = floorf( min_u );

		min_u -= whole;
		max_u -= whole;

		//float frac_min = min_u - floorf( min_u );

		if( max_u > 1.f )
		{
			// crossing/cutting right edge
			// something like min = .25 and max = 1.3
			return true;
		}
		else
		{
			// gradient doesnt cross a tile edge
			// something like min = .1 and max = 1.0
			return false;
		}
	}
	else
	{
		// max_u is <= 0.f
		Assert( min_u <= 0.f );

		// fastest test to see if its 'tiled'
		if ( fabsf(min_u-max_u) > 1.f ) return true;// crossing/snapped across entire dimension (do not use >= test)

		// crossing less than the entire space, do fraction/whole tests now.

		// take the max value and slide it into the -1 to 0 range.
		float diff = ceilf( max_u ); // -1.7 becomes -1, now negate and add to min+max to slide it over.

		min_u += diff;
		max_u += diff;

		if( min_u < -1.f ) return true; // cutting tile edge.

		// values are always negative, so we use ceil so ->  ( -2.8 becomes -2 )
		float min_u_whole = ceilf( min_u );// .7 becomes 0
		float max_u_whole = ceilf( max_u );// .9 becomes 0

		float min_fraction = min_u - min_u_whole;// .7
		float max_fraction = max_u - max_u_whole;// .9

		// handle case where fractions match, either clamped to same coordinate or 'tiled'
		if( max_fraction == min_fraction )
		{
			if( min_u_whole != max_u_whole )
			{
				// tiled/crossing at least once in some direction since whole values mismatch.
				return true; // treat perfectly tiled/snapped mapping as crossing tile boundary.
			}
			else if( min_u_whole == max_u_whole)
			{
				Assert( min_u == max_u );
				return false;// same mapping coordinate so its not tiled.
			}
		}
		else
		{
			if( min_u_whole != max_u_whole )
			{
				// cutting AD or BC
				return true;
			}

			// Fractions are not equal and whole values are the same
			// ->interior mapped, not crossing tile edges.
			return false;
		}
	}

	// unreachable case???
	return false;
}
