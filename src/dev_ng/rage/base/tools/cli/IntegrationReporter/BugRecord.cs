﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace IntegrationReporter
{
    public class BugRecord
    {
        [XmlAttribute]
        public int BugNumber;

        [XmlArrayItem("Changelist")]
        public List<int> ChangelistNumbers;

        [XmlAttribute]
        public string User;

        [XmlAttribute]
        public string Description;

        public BugRecord()
        {
            ChangelistNumbers = new List<int>();
            BugNumber = -1;
        }

        public BugRecord(int bugNumber)
        {
            ChangelistNumbers = new List<int>();
            BugNumber = bugNumber;
        }
    }
}
