﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace IntegrationReporter
{
    public class DiffRecord
    {
        [XmlAttribute]
        public string SourceFile;

        [XmlAttribute]
        public string DestFile;

        public DiffRecord()
        {
            SourceFile = null;
            DestFile = null;
        }

        public DiffRecord(string sourceFile, string destFile)
        {
            SourceFile = sourceFile;
            DestFile = destFile;
        }
    }
}
