﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace IntegrationReporter
{
    public class ChangelistRecord
    {
        [XmlAttribute]
        public int ChangelistNumber;

        [XmlAttribute]
        public string User;

        [XmlAttribute]
        public string Description;

        [XmlAttribute]
        public string BugNumber;

        [XmlArrayItem("File")]
        public List<FileRecord> Files;

        public ChangelistRecord()
        {
            ChangelistNumber = -1;
            User = null;
            Description = null;
            Files = null;
            BugNumber = null;
        }

        public ChangelistRecord(int change, string user, string description)
        {
            ChangelistNumber = change;
            User = user;
            Description = description;
            BugNumber = null;
            Files = new List<FileRecord>();
        }
    }
}
