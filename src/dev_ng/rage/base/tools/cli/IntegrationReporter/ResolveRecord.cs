﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace IntegrationReporter
{
    [Serializable]
    public enum ResolveResult
    {
        UNKNOWN,
        RESOLVED,
        SKIPPED,
        HAS_CONFLICTS,
    };

    [Serializable]
    public enum ChangeType
    {
        YOURS,
        THEIRS,
        BOTH,
        CONFLICT
    };

    public class ResolveChanges : IXmlSerializable
    {
        [XmlAttribute]
        public int Yours;
        
        [XmlAttribute]
        public int Theirs;

        [XmlAttribute]
        public int Both;

        [XmlAttribute]
        public int Conflict;

        public ResolveChanges()
        {
            Yours = -1;
            Theirs = -1;
            Both = -1;
            Conflict = -1;
        }

        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            XmlSerializer serializer = new XmlSerializer(this.GetType());
            ResolveChanges changes = (ResolveChanges)serializer.Deserialize(reader);

            this.Yours = changes.Yours;
            this.Theirs = changes.Theirs;
            this.Both = changes.Both;
            this.Conflict = changes.Conflict;
        }

        public void WriteXml(XmlWriter writer)
        {
            if ( Yours != -1 && Theirs != -1 && Both != -1 && Conflict != -1 )
            {
                writer.WriteAttributeString("Yours", Yours.ToString());
                writer.WriteAttributeString("Theirs", Theirs.ToString());
                writer.WriteAttributeString("Both", Both.ToString());
                writer.WriteAttributeString("Conflict", Conflict.ToString());
            }
        }
    }

    [Serializable]
    public class ResolveRecord
    {
        [XmlAttribute("TargetFile")]
        public string TargetFilename { get; set; }

        [XmlAttribute("SourceFile")]
        public string SourceFilename { get; set; }

        [XmlAttribute("ResolveStatus")]
        public ResolveResult ResolveStatus { get; set; }

        [XmlAttribute("StartRevision")]
        public int StartRevision;

        [XmlAttribute("EndRevision")]
        public int EndRevision;

        [XmlArrayItem("Changelist")]
        public List<int> Changelists;

        [XmlElement("ResolveChanges")]
        public ResolveChanges Changes;

        public bool HasConflicts 
        {
            get 
            { 
                return Changes.Conflict > 0;
            } 
        }

        public ResolveRecord()
        {
            TargetFilename = null;
            SourceFilename = null;

            ResolveStatus = ResolveResult.UNKNOWN;
            StartRevision = -1;
            EndRevision = -1;

            Changes = new ResolveChanges();
            Changelists = new List<int>();
        }

        public ResolveRecord(string sourceFilename)
        {
            TargetFilename = sourceFilename;
            SourceFilename = null;

            ResolveStatus = ResolveResult.UNKNOWN;
            StartRevision = -1;
            EndRevision = -1;

            Changes = new ResolveChanges();
            Changelists = new List<int>();
        }

        public ResolveRecord(string targetFilename, string sourceTarget)
        {
            TargetFilename = targetFilename;
            SourceFilename = sourceTarget;

            ResolveStatus = ResolveResult.UNKNOWN;
            StartRevision = -1;
            EndRevision = -1;

            Changes = new ResolveChanges();
            Changelists = new List<int>();
        }
    }
}
