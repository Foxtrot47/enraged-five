﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace IntegrationReporter
{
    [Serializable]
    public class IntegrationReport
    {
        [XmlAttribute]
        public string BranchName;

        [XmlAttribute]
        public string Workspace;

        [XmlAttribute]
        public string Port;

        [XmlAttribute]
        public string User;

        [XmlAttribute]
        public string Time;

        [XmlAttribute]
        public int LastChangelist;

        public List<ResolveRecord> ResolveRecords;
        public List<ChangelistRecord> ChangelistRecords;
        public List<BugRecord> BugRecords;
        public List<DiffRecord> DiffRecords;

        public IntegrationReport()
        {
            ResolveRecords = new List<ResolveRecord>();
            ChangelistRecords = new List<ChangelistRecord>();
            BugRecords = new List<BugRecord>();
            DiffRecords = new List<DiffRecord>();
        }
    }
}
