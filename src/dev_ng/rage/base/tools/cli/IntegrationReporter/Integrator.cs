﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

using RSG.SourceControl.Perforce;
using P4API;

namespace IntegrationReporter
{
    public enum ResolveType
    {
        SAFE_RESOLVE,
        AUTO_RESOLVE,
        THEIRS_RESOLVE,
        YOURS_RESOLVE
    }

    public class Integrator
    {
        private P4 m_P4;
        private string m_Branch;
        private string m_Description;
        private string m_ExtraDescription;
        private List<string> m_Sources;
        private string m_OriginalDestination;
        private List<string> m_Destinations;
        private P4PendingChangelist m_Changelist;
        private int m_LastChangelist;
        private IntegrationReport m_Report;
        private List<string> m_SubmissionErrors;
        private string m_Label;

        public bool CruiseControlFormatted;
        public bool Submit;
        public bool SubmitSuccess;
        public bool Revert;
        public bool ReverseBranchMapping;
        public ResolveType ResolveMode;

        public Integrator(string branch, string source, string destination, string description, string label)
        {
            CruiseControlFormatted = false;
            Submit = false;
            SubmitSuccess = false;
            Revert = true;
            ResolveMode = ResolveType.SAFE_RESOLVE;

            m_P4 = new P4();
            m_Report = new IntegrationReport();
            m_Branch = branch;
            m_Sources = new List<string>();
            m_Destinations = new List<string>();
            m_ExtraDescription = description;
            m_SubmissionErrors = new List<string>();
            m_Label = label;

            if (String.IsNullOrWhiteSpace(source) == false)
            {
                if (source.EndsWith("...") == false)
                {
                    if (source.EndsWith("\\") || source.EndsWith("/"))
                        source += "...";
                    else
                        source += "\\...";
                }

                P4RecordSet recordSet = m_P4.Run("where", source);
                if (recordSet.Records.Length == 0)
                {
                    LogError("Unable to determine location of " + source + " from p4 where command.");
                }
                else
                {
                    foreach (P4Record record in recordSet.Records)
                    {
                        string sourceRecord = record.Fields["depotFile"];
                        bool unmapped = record.Fields.ContainsKey("unmap");
                        if (unmapped == false)
                        {
                            //Ignore any negations in the branch specification.
                            m_Sources.Add(sourceRecord);
                        }
                    }
                }
            }
        
            if ( destination.EndsWith("...") == false )
            {
                if (destination.EndsWith("\\") || destination.EndsWith("/"))
                    destination += "...";
                else
                    destination += "\\...";

                m_OriginalDestination = destination;
                P4RecordSet recordSet = m_P4.Run("where", destination);
                if (recordSet.Records.Length == 0)
                {
                    LogError("Unable to determine location of " + destination + " from p4 where command.");
                }
                else
                {
                    foreach (P4Record record in recordSet.Records)
                    {
                        string destinationRecord = record.Fields["depotFile"];
                        bool unmapped = record.Fields.ContainsKey("unmap");
                        if (unmapped == false)
                        {
                            //Ignore any negations in the branch specification.
                            m_Destinations.Add(destinationRecord);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// A branch is bi-directional, but we need to know which direction in the branch mapping 
        /// to use in the p4 integrate command.
        /// </summary>
        /// <returns>
        /// -1 if the branch mapping needs to be reversed.
        /// 0 if there was an error.
        /// 1 if the branch mapping needs not to be changed.
        /// </returns>
        private int FindBranchDirection()
        {
            P4RecordSet branchMappingsSet = m_P4.Run("branch", "-o", m_Branch);
            foreach (P4Record record in branchMappingsSet.Records)
            {
                string[] views = record.ArrayFields["View"];
                foreach (string view in views)
                {
                    string[] paths = view.Split(' ');
                    string sourceMapping = paths[0].ToLower();
                    string destinationMapping = paths[1].ToLower();

                    foreach (string destination in m_Destinations)
                    {
                        string destLower = destination.ToLower();
                        string trimmedSourceMap = sourceMapping.Replace("...", "");
                        string trimmedDestMap = destination.Replace("...", "");
                        
                        if (String.Compare(sourceMapping, destLower) == 0)
                        {
                            return -1;
                        }
                        else if (String.Compare(destinationMapping, destLower) == 0)
                        {
                            return 1;
                        }
                        else if (destLower.Contains(trimmedSourceMap) == true)
                        {
                            return -1;
                        }
                        else if (destLower.Contains(trimmedDestMap) == true)
                        {
                            return 1;
                        }
                    }
                }
            }

            return 0;
        }

        public bool Generate(out IntegrationReport report)
        {
            bool result = true;
            report = null;

            //Get the initial statistics for the integration report.
            P4RecordSet lastChangeRecord = m_P4.Run("changes", "-m", "1", "-s", "submitted");
            m_LastChangelist = Convert.ToInt32(lastChangeRecord.Records[0].Fields["change"]);

            if (Submit == true)
            {
                m_Description = "Integration Update from " + m_LastChangelist + ".\n";
            }
            else
            {
                m_Description = "Integration Report from " + m_LastChangelist + ".\n";
            }

            if (m_Branch != null)
            {
                m_Description += "Branch: " + m_Branch + "\n";
            }
            else if (m_Sources.Count > 0 && m_Destinations.Count > 0)
            {
                m_Description += m_Sources[0] + " to " + m_Destinations[0] + ".";
            }

            if (m_Label != null)
            {
                m_Description += "Label: " + m_Label + "\n";
            }

            if (Submit == true)
            {
                m_Description += "Buddy: None.\n";
            }

            if (m_Description != null)
            {
                m_Description += m_ExtraDescription;
            }

            m_P4.Connect();
            m_Changelist = m_P4.CreatePendingChangelist(m_Description);

            P4RecordSet recordset = null;

            //Determine first if a branch specification is being used.  
            if ( m_Branch != null && m_Destinations.Count > 0) 
            {
                try
                {
                    int mappingDirection = FindBranchDirection();
                    bool reverseMapping = ReverseBranchMapping;  //If the ReverseBranchMapping is set to true, use that setting as that takes precedence.
                    if (ReverseBranchMapping == false)
                    {
                        if (mappingDirection == 0)
                        {
                            LogError("Unable to determine branch mapping direction.");
                            return false;
                        }
                        else if (mappingDirection == 1)
                        {
                            //Forward direction.
                            reverseMapping = false;
                        }
                        else if (mappingDirection == -1)
                        {
                            //Reverse direction.
                            reverseMapping = true;
                        }
                    }

                    //Ensure that deletes are accepted.
                    //Ensure that baseless merges are maintained.
                    foreach (string rawDestination in m_Destinations)
                    {
                        string destination = GetLabelledFileSpec(rawDestination);

                        if (reverseMapping == true)
                            recordset = m_P4.Run("integrate", "-c", m_Changelist.Number.ToString(), "-d", "-i", "-b", m_Branch, "-r", destination);
                        else
                            recordset = m_P4.Run("integrate", "-c", m_Changelist.Number.ToString(), "-d", "-i", "-b", m_Branch, destination);

                        if (recordset.HasErrors() == true)
                        {
                            foreach (string error in recordset.Errors)
                            {
                                LogError(error);
                            }

                            result = false;
                        }
                    }
                }
                catch (P4API.Exceptions.RunException e)
                {
                    LogError("An error has occurred trying to integrate.");
                    LogError(e.Message);
                    return false;
                }

            }
            else if (m_Sources.Count > 0 && m_Destinations.Count > 0)
            {
                //Use a file specification.  Assume that only one source and one destination has been specified.
                try
                {
                    string source = GetLabelledFileSpec(m_Sources[0]);
                    string destination = GetLabelledFileSpec(m_Destinations[0]);

                    recordset = m_P4.Run("integrate", "-c", m_Changelist.Number.ToString(), "-d", "-i", source, destination);
                    if (recordset.HasWarnings() == true)
                    {
                        foreach (string warning in recordset.Warnings)
                        {
                            LogWarning(warning);
                        }
                    }

                    if (recordset.HasErrors() == true)
                    {
                        foreach (string error in recordset.Errors)
                        {
                            LogError(error);
                        }

                        result = false;
                    }
                }
                catch(P4API.Exceptions.RunException e)
                {
                    LogError("An error has occurred trying to integrate.");
                    LogError(e.Message);
                    return false;
                }
            }

            m_Report.BranchName = m_Branch;
            m_Report.Port = m_P4.Port;
            m_Report.User = m_P4.User;
            m_Report.Workspace = m_P4.Client;
            m_Report.Time = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
            m_Report.LastChangelist = m_LastChangelist;

            //Accept Safe - If eithers yours or theirs is different from the base, and the changes are in common, accept that revision.
            //If both are different from base, skip this file.
            P4RecordSet safeResolveSet;
            try
            {
                string resolveModeArgument;
                switch (ResolveMode)
                {
                    case ResolveType.AUTO_RESOLVE:
                        resolveModeArgument = "-am";
                        break;

                    case ResolveType.THEIRS_RESOLVE:
                        resolveModeArgument = "-at";
                        break;

                    case ResolveType.YOURS_RESOLVE:
                        resolveModeArgument = "-ay";
                        break;

                    default:
                        resolveModeArgument = "-as";
                        break;
                }

                m_Report.ResolveRecords = new List<ResolveRecord>();
                foreach (string destination in m_Destinations)
                {
                    safeResolveSet = m_P4.Run("resolve", resolveModeArgument, destination);

                    List<ResolveRecord> resolveRecords;
                    if (ParseResolveRecordSet(safeResolveSet, out resolveRecords) == false)
                    {
                        LogError("Unable to parse resolve records.");
                        return false;
                    }

                    m_Report.ResolveRecords.AddRange(resolveRecords);
                }

                //For every file, determine what changelist it belongs to.
                if (ParseChangelists(m_Report.ResolveRecords) == false)
                {
                    LogError("Unable to parse changelist records.");
                    return false;
                }

                if (ParseBugFixes(m_Report.ChangelistRecords) == false)
                {
                    LogError("Unable to parse bug fix records.");
                    return false;
                }

            }
            catch (P4API.Exceptions.RunException e)
            {
                LogError("An error has occurred trying to integrate.");
                LogError(e.Message);
                return false;
            }

            if (Submit == true)
            {
                try
                {
                    //NOTE:  If you're going to accept yours or theirs, there will be no
                    //resolve records since the resolve is already decided.
                    if (m_Report.ResolveRecords.Count != 0 
                        || ResolveMode == ResolveType.THEIRS_RESOLVE 
                        || ResolveMode == ResolveType.YOURS_RESOLVE)
                    {
                        P4UnParsedRecordSet recordSet = m_Changelist.Submit();

                        if (recordSet.Errors.Length > 0)
                        {
                            SubmitSuccess = false;
                            foreach (string error in recordSet.Errors)
                            {
                                LogError(error);
                                m_SubmissionErrors.Add(error);
                            }

                            //Revert and delete the changelist.
                            if (Revert == true)
                            {
                                m_Changelist.Revert();
                                m_Changelist.Delete();
                            }
                        }
                        else
                        {
                            SubmitSuccess = true;
                        }
                    }
                    else
                    {
                        SubmitSuccess = true;

                        //Revert and delete this changelist -- it should have no files in it.
                        m_Changelist.Revert();
                        m_Changelist.Delete();
                    }
                }
                catch (P4API.Exceptions.RunUnParsedException e)
                {
                    SubmitSuccess = false;

                    //An exception has occurred with the submission.  
                    LogError("An error has occurred trying to submit the integration in Changelist " + m_Changelist.Number + ".  The changelist will be reverted.");
                    LogError(e.Message);

                    //Format the message into seperate lines for us to better process output.
                    foreach (string error in e.Message.Split('\n'))
                    {
                        m_SubmissionErrors.Add(error);
                    }

                    if (Revert == true)
                    {
                        m_Changelist.Revert();
                        m_Changelist.Delete();
                    }
                }

                //After this integration, we assume that there will be no differences.
                if (m_Branch != null && m_Destinations.Count > 0)
                {
                    //Perform a diff.
                    foreach (string rawDestination in m_Destinations)
                    {
                        string destination = GetLabelledFileSpec(rawDestination);

                        P4RecordSet diffRecordSet = m_P4.Run("diff2", "-q", "-t", "-b", m_Branch, destination);

                        List<DiffRecord> diffRecords = new List<DiffRecord>();
                        ParseDiffRecords(diffRecordSet, out diffRecords);
                        m_Report.DiffRecords.AddRange(diffRecords);
                    }
                }
                else if (m_Sources.Count > 0 && m_Destinations.Count > 0)
                {
                    P4RecordSet diffRecordSet = m_P4.Run("diff2", "-q", "-t", "-b", m_Sources[0] + "#head", m_Destinations[0]);
                    List<DiffRecord> diffRecords = new List<DiffRecord>();
                    ParseDiffRecords(diffRecordSet, out diffRecords);
                    m_Report.DiffRecords.AddRange(diffRecords);
                }
            }
            else
            {
                //Revert and delete the changelist.
                if (Revert == true)
                {
                    m_Changelist.Revert();
                    m_Changelist.Delete();
                }
            }

            report = m_Report;
            return result;
        }

        /// <summary>
        /// Appends the labelled syntax to the file path.
        /// </summary>
        /// <param name="inputFile"></param>
        /// <returns></returns>
        private string GetLabelledFileSpec(string inputFile)
        {
            string fileSpec = inputFile;
            if (m_Label != null)
            {
                fileSpec += "@" + m_Label;
            }

            return fileSpec;
        }

        /// <summary>
        /// Gets the number of conflicts in the resolution records.
        /// </summary>
        /// <returns></returns>
        public int GetConflictCount()
        {
            int conflictCount = 0;
            foreach (ResolveRecord resolveRecord in m_Report.ResolveRecords)
            {
                if (resolveRecord.HasConflicts)
                {
                    conflictCount++;
                }
            }

            return conflictCount;
        }

        public void PrintReport(bool printConflictList)        
        {
            int conflictCount = GetConflictCount();
            int fileCount = m_Report.ResolveRecords.Count;

            LogMessage("Summary:");
            LogMessage("Integrated from " + m_OriginalDestination + " using branch specification " + m_Branch);
            LogMessage("Number of Conflicts: " + conflictCount);
            LogMessage("Total Files: " + fileCount);

            if (printConflictList && conflictCount > 0)
            {
                LogMessage("Conflicted Files: ");
                foreach (ResolveRecord resolveRecord in m_Report.ResolveRecords)
                {
                    if (resolveRecord.HasConflicts)
                    {
                        LogMessage("\t" + resolveRecord.TargetFilename + " (" + resolveRecord.Changes.Conflict + ")" );
                    }
                }
            }

            if (Submit == true)
            {
                if (fileCount == 0)
                {
                    LogMessage("No files were submitted.  There were no changes.");
                }
                else
                {
                    if (SubmitSuccess == true)
                    {
                        LogMessage("Changelist " + m_Changelist.Number + " was submitted.");
                    }
                    else
                    {
                        LogMessage("No changelist was submitted due to errors:");

                        foreach(string submissionError in m_SubmissionErrors)
                        {
                            LogMessage("\t" + submissionError);
                        }
                    }
                    LogMessage("Remaining Differences Between Branches: " + m_Report.DiffRecords.Count);

                    int recordIndex = 0;
                    int recordMax = 5;
                    foreach (DiffRecord diffRecord in m_Report.DiffRecords)
                    {
                        LogMessage(diffRecord.SourceFile + " ... " + diffRecord.DestFile);

                        recordIndex++;
                        if (recordIndex >= recordMax)
                        {
                            break;
                        }
                    }

                    if (recordIndex == recordMax)
                    {
                        LogMessage("List has been truncated.  See the diff report for a complete list.");
                    }
                }
            }
        }

        private bool IsIntegrated(string filePath, List<ResolveRecord> resolveRecords)
        {
            bool inChangelist = false;
            foreach (ResolveRecord resolveRecord in resolveRecords)
            {
                if (String.Compare(resolveRecord.SourceFilename, filePath) == 0)
                {
                    inChangelist = true;
                    break;
                }
            }

            return inChangelist;
        }

        private bool ParseResolveRecordSet(P4RecordSet recordSet, out List<ResolveRecord> resolveRecords)
        {
            //  Standard output on a resolve looks like:
            //
            //  X:\rdr3\tools\etc\content\maps.xml - merging //depot/gta5/tools/etc/content/maps.xml#176,#180
            //  Diff chunks: 8 yours + 0 theirs + 0 both + 2 conflicting
            //  //SANW-KWEINBERG1-RDR3/rdr3/tools/etc/content/maps.xml - resolve skipped.
            //
            Regex statusRexEx = new Regex("[Diff chunks:|Non-text diff:] ([0-9]+) yours \\+ ([0-9]+) theirs \\+ ([0-9]+) both \\+ ([0-9]+) conflicting", RegexOptions.Compiled | RegexOptions.IgnoreCase);

            const string mergeToken = " - merging ";
            const string vsToken = " - vs";
            const string diffChunksToken = "Diff chunks: ";
            const string nonTextDiffToken = "Non-text diff: ";

            resolveRecords = new List<ResolveRecord>();

            //Increment by three because the output is structured on three lines for each file.
            int numRecords = recordSet.Messages.Length;
            for (int recordSetIndex = 0; recordSetIndex < numRecords; recordSetIndex += 3)
            {
                string info = recordSet.Messages[recordSetIndex];
                string status = recordSet.Messages[recordSetIndex + 1];
                string action = recordSet.Messages[recordSetIndex + 2];

                //Parse the information line.
                string[] tokens = new string[] { mergeToken, vsToken };
                string[] actionSplit = info.Split(tokens, StringSplitOptions.RemoveEmptyEntries);

                if (actionSplit.Length != 2)
                {
                    LogError("Unexpected line encountered while parsing: " + info + ".");
                    return false;
                }

                string sourceFilename = actionSplit[0];

                tokens = new string[] { "#" };
                string[] destFilenameSplit = actionSplit[1].Split(tokens, StringSplitOptions.RemoveEmptyEntries);

                string destFilename = destFilenameSplit[0].Trim();
                string startRevisionStr = destFilenameSplit[1];
                startRevisionStr = startRevisionStr.Replace(",", "");

                string endRevisionStr = null;
                if (destFilenameSplit.Length > 2)
                {
                    endRevisionStr = destFilenameSplit[2];
                }

                //Parse the resolution information.
                if (status.Contains(diffChunksToken) == false && status.Contains(nonTextDiffToken) == false)
                {
                    LogError("Unable to diff status from parse record set: " + status);
                    return false;
                }

                Match statusMatch = statusRexEx.Match(status);
                if (statusMatch.Groups.Count != 5)
                {
                    LogError("Matching string \"" + status + "\" did not accumulate the expected number matches to determine the amount of the types of changes.");
                    return false;
                }

                ResolveRecord record = new ResolveRecord(sourceFilename, destFilename);
                record.Changes.Yours= Convert.ToInt32(statusMatch.Groups[1].Value);
                record.Changes.Theirs = Convert.ToInt32(statusMatch.Groups[2].Value);
                record.Changes.Both = Convert.ToInt32(statusMatch.Groups[3].Value);
                record.Changes.Conflict = Convert.ToInt32(statusMatch.Groups[4].Value);
                record.StartRevision = Convert.ToInt32(startRevisionStr);
                record.EndRevision = Convert.ToInt32(endRevisionStr);

                if (record.HasConflicts == true)
                {
                    record.ResolveStatus = ResolveResult.HAS_CONFLICTS;
                }
                else
                {
                    record.ResolveStatus = ResolveResult.RESOLVED;
                }

                P4RecordSet revisionRecordSet;
                if (record.EndRevision != 0)
                {
                    revisionRecordSet = m_P4.Run("changes", record.SourceFilename + "#" + record.StartRevision + ",#" + record.EndRevision);
                }
                else
                {
                    revisionRecordSet = m_P4.Run("changes", "-m 1", record.SourceFilename + "#" + record.StartRevision);
                }


                foreach(P4Record revisionRecord in revisionRecordSet) 
                {
                    string changelist = revisionRecord.Fields["change"];
                    int changelistNumber = Convert.ToInt32(changelist);
                    record.Changelists.Add(changelistNumber);
                }

                resolveRecords.Add(record);
            }

            return true;
        }

        private bool ParseChangelists(List<ResolveRecord> resolveRecords)
        {
            Dictionary<int, ChangelistRecord> processedChangelists = new Dictionary<int, ChangelistRecord>();
            foreach (ResolveRecord resolveRecord in resolveRecords)
            {
                string fileRevision = resolveRecord.SourceFilename + "#" + resolveRecord.StartRevision;
                if (resolveRecord.EndRevision == 0)
                {
                    fileRevision += "," + resolveRecord.StartRevision;
                }
                else if (resolveRecord.EndRevision != 0)
                {
                    fileRevision += "," + resolveRecord.EndRevision;
                }

                P4RecordSet changeRecordSet = m_P4.Run("changes", "-l", fileRevision);
                foreach (P4Record record in changeRecordSet.Records)
                {
                    int change = Convert.ToInt32(record.Fields["change"]);
                    string user = record.Fields["user"];
                    string description = record.Fields["desc"];

                    if (processedChangelists.ContainsKey(change) == false)
                    {
                        ChangelistRecord changelistRecord = new ChangelistRecord(change, user, description);

                        //Acquire the description of the changelist to get a full list of files.
                        //
                        P4RecordSet changeDescriptionRecordSet = m_P4.Run("describe", "-s", change.ToString());

                        foreach (P4Record changeDescriptionRecord in changeDescriptionRecordSet)
                        {
                            string[] depotFiles = changeDescriptionRecord.ArrayFields["depotFile"];
                            string[] revisions = changeDescriptionRecord.ArrayFields["rev"];
                            string[] actions = changeDescriptionRecord.ArrayFields["action"];

                            //Validate that the files cited in the changelist are actually part of the integration.
                            //The branch specification can filter some of these files out, or they are in a separate
                            //sub-directory.
                            //
                            for (int index = 0; index < depotFiles.Length; index++)
                            {
                                bool isIntegrated = IsIntegrated(depotFiles[index], resolveRecords);
                                string action = actions[index];

                                FileRecord fileRecord = new FileRecord(depotFiles[index], Convert.ToInt32(revisions[index]), isIntegrated, action);

                                changelistRecord.Files.Add(fileRecord);
                            }

                        }

                        processedChangelists.Add(change, changelistRecord);
                        m_Report.ChangelistRecords.Add(changelistRecord);
                    }
                }
            }

            return true;
        }

        private bool ParseBugFixes(List<ChangelistRecord> changelistRecords)
        {
            for(int changelistIndex = 0; changelistIndex < changelistRecords.Count; ++changelistIndex) 
            {
                ChangelistRecord changelistRecord = changelistRecords[changelistIndex];
                Match bugstarNotes = Regex.Match(changelistRecord.Description, "(B\\*|B\\* |bugstar:|bug |bug)(\\d+)");

                if (bugstarNotes.Groups.Count == 3)
                {
                    //There will be three tokens if there's bugstar number here.
                    //0: The full description.
                    //1: The bug marker.
                    //2: The bug number.

                    int bugNumber;
                    if (Int32.TryParse(bugstarNotes.Groups[2].Value, out bugNumber) == true)
                    {
                        //Parse the string to ensure it's a number, but save the string value
                        //so the XmlSerializer can take advantage of not printing out null values.
                        //
                        changelistRecord.BugNumber = bugstarNotes.Groups[2].Value;

                        int changelistBugNumber = Convert.ToInt32(changelistRecord.BugNumber);
                        bool found = false;
                        foreach(BugRecord bugRecord in m_Report.BugRecords)
                        {
                            if (bugRecord.BugNumber == changelistBugNumber)
                            {
                                bugRecord.ChangelistNumbers.Add(changelistRecord.ChangelistNumber);
                                found = true;
                                break;
                            }
                        }

                        if (found == false)
                        {
                            BugRecord newBugRecord = new BugRecord(changelistBugNumber);
                            newBugRecord.ChangelistNumbers.Add(changelistRecord.ChangelistNumber);
                            m_Report.BugRecords.Add(newBugRecord);
                        }
                    }
                }
            }

            return true;
        }

        public bool ParseDiffRecords(P4RecordSet recordSet, out List<DiffRecord> diffRecords)
        {
            diffRecords = new List<DiffRecord>();
            foreach (P4Record record in recordSet)
            {
                DiffRecord diffRecord = new DiffRecord(record.Fields["depotFile"], record.Fields["depotFile2"]);
                diffRecords.Add(diffRecord);
            }

            return true;
        }

        public void LogMessage(string output)
        {
            if (CruiseControlFormatted == true)
            {
                Console.WriteLine("INFO_MSG: " + output);
            }
            else
            {
                Console.WriteLine(output);
            }
        }

        public void LogError(string output)
        {
            Console.WriteLine("Error: " + output);
        }

        public void LogWarning(string output)
        {
            Console.WriteLine("Warning: " + output);
        }
    }
}
