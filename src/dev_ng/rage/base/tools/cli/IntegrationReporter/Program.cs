﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml.Xsl;
using RSG.Base;

namespace IntegrationReporter
{
    class Program
    {
        static string m_BranchArgument = "branch";
        static string m_SourceArgument = "source";
        static string m_DestinationArgument = "destination";

        static string m_XslDirectoryArgument = "xslpath";
        static string m_IntermediateFileArgument = "intermediatefile";
        static string m_OutputPathArgument = "outputpath";

        static string m_CruiseControlArgument = "cc";
        static string m_SubmitArgument = "submit";  //Submit the integration.
        static string m_LabelArgument = "label";
        static string m_NoRevertArgument = "norevert";

        static string m_SafeResolveArgument = "saferesolve";
        static string m_AutoResolveArgument = "autoresolve";
        static string m_AcceptTheirsArgument = "accepttheirs";
        static string m_AcceptYoursArgument = "acceptyours";

        //Forcefully sets the branch to be reversed.  This is necessary since we inline certain branches within the same folder.
        //(e.g. X:\rdr3\src\dev_migrate and X:\rdr3\src\dev exist under the same directory of X:\rdr3, which we would be specified if
        //we wanted to integrate all directories under the dev_migrate label.
        static string m_ReverseArgument = "reverse"; 

        static string m_HelpArgument = "help";

        static string m_DescriptionArgument = "description";    //Description of the changelist.
        
        static int Main(string[] args)
        {
            CommandLineParser parser = new CommandLineParser(args);

            string branch = parser.Parameters[m_BranchArgument];
            string source = parser.Parameters[m_SourceArgument];
            string destination = parser.Parameters[m_DestinationArgument];
            string description = parser.Parameters[m_DescriptionArgument];
            string xslDirectory = parser.Parameters[m_XslDirectoryArgument];
            string intermediateXMLFile = parser.Parameters[m_IntermediateFileArgument];
            string outputDirectory = parser.Parameters[m_OutputPathArgument];
            bool submit = Convert.ToBoolean(parser.Parameters[m_SubmitArgument]);
            bool reverse = Convert.ToBoolean(parser.Parameters[m_ReverseArgument]);
            bool ccFormatted = Convert.ToBoolean(parser.Parameters[m_CruiseControlArgument]);
            bool helpPrompt = Convert.ToBoolean(parser.Parameters[m_HelpArgument]);
            bool norevert = Convert.ToBoolean(parser.Parameters[m_NoRevertArgument]);
            string label = parser.Parameters[m_LabelArgument];
            bool safeResolve = Convert.ToBoolean(parser.Parameters[m_SafeResolveArgument]);
            bool autoResolve = Convert.ToBoolean(parser.Parameters[m_AutoResolveArgument]);
            bool acceptYours = Convert.ToBoolean(parser.Parameters[m_AcceptYoursArgument]);
            bool acceptTheirs = Convert.ToBoolean(parser.Parameters[m_AcceptTheirsArgument]);

            bool cliParseResult = true;
            if (String.IsNullOrWhiteSpace(branch) == true && String.IsNullOrWhiteSpace(source) == true)
            {
                LogError("No branch or source arguments were found.  Specify a Perforce branch specification (-branch) or a source file location (-source).");
                cliParseResult = false;
            }

            if (String.IsNullOrWhiteSpace(source) == false)
            {
                if (Directory.Exists(source) == false)
                {
                    LogError("The source directory " + source + " does not exist.");
                    cliParseResult = false;
                }
            }

            if (String.IsNullOrWhiteSpace(destination) == true)
            {
                LogError("Null or empty " + m_DestinationArgument + " argument was provided.  Specify a directory to integrate from.");
                cliParseResult = false;
            }

            if (Directory.Exists(destination) == false)
            {
                LogError("The destination directory " + destination + " does not exist.");
                cliParseResult = false;
            }

            if (String.IsNullOrWhiteSpace(xslDirectory) == true)
            {
                LogError("Null or empty " + xslDirectory + " argument was provided.  Specify a directory containing the XSL and CCS documents for generating reports.");
                cliParseResult = false;
            }

            if (Directory.Exists(xslDirectory) == false)
            {
                LogError("The XSL directory " + xslDirectory + " does not exist.");
                cliParseResult = false;
            }

            if (String.IsNullOrWhiteSpace(outputDirectory) == true)
            {
                LogError("Null or empty " + m_OutputPathArgument + " argument was provided.  Specify an output HTML file path.");
                cliParseResult = false;
            }

            int resolveArgCount = 0;
            if (safeResolve == true) resolveArgCount++;
            if (autoResolve == true) resolveArgCount++;
            if (acceptYours == true) resolveArgCount++;
            if (acceptTheirs == true) resolveArgCount++;

            if (resolveArgCount > 1)
            {
                LogError("Multiple resolve types have been instantiated.  Please select only one (automatic, safe).");
                cliParseResult = false;
            }

            if (cliParseResult == false || helpPrompt == true)
            {
                LogError("Usage: [-branch <branch_specification>|-source <source integration path>] -destination <target integration path> -xslFile <xsl_file> -cssfile <css_file> -outputfile <output HTML file> [-description <addition changelist description>] [-submit] [-label <name>] [-autoresolve|-saferesolve]");
                return -1;
            }

            Integrator integrator = new Integrator(branch, source, destination, description, label);
            integrator.CruiseControlFormatted = ccFormatted;
            integrator.Submit = submit;
            integrator.ReverseBranchMapping = reverse;
            integrator.Revert = !norevert;

            if (autoResolve) integrator.ResolveMode = ResolveType.AUTO_RESOLVE;
            else if (safeResolve) integrator.ResolveMode = ResolveType.AUTO_RESOLVE;
            else if (acceptTheirs) integrator.ResolveMode = ResolveType.THEIRS_RESOLVE;
            else if (acceptYours) integrator.ResolveMode = ResolveType.YOURS_RESOLVE;

            IntegrationReport report;
            if (integrator.Generate(out report) == false)
            {
                LogError("Unable to properly run the integrator.");
                return -1;
            }

            if (Save(intermediateXMLFile, report) == false)
            {
                LogError("Unable to save integration report XML file.");
                return -1;
            }
            
            string[] xslFiles = Directory.GetFiles(xslDirectory, "*.xsl");
            foreach (string xslFile in xslFiles)
            {
                string outputFile = Path.Combine(outputDirectory, Path.GetFileNameWithoutExtension(xslFile));
                outputFile += ".html";
                if (Transform(xslFile, intermediateXMLFile, outputFile) == false)
                {
                    LogError("Unable to transform HTML from XSL document " + xslFile + ".");
                    return -1;
                }
            }

            integrator.PrintReport(true);

            if (integrator.Submit == true)
            {
                //Return whether the submission was successful.
                return integrator.SubmitSuccess ? 0 : -1;
            }
            else if (integrator.GetConflictCount() > 0)
            {
                //Return whether a conflict has been encountered.
                return -1;
            }

            return 0;
        }

        static bool Save(string filePath, IntegrationReport report)
        {
            try
            {
                string directory = Path.GetDirectoryName(filePath);
                if (Directory.Exists(directory) == false)
                {
                    Directory.CreateDirectory(directory);
                }

                XmlSerializer serializer = new XmlSerializer(report.GetType());
                StreamWriter writer = new StreamWriter(filePath);
                serializer.Serialize(writer, report);
                writer.Close();
            }
            catch (Exception e)
            {
                LogError(e.Message);
                return false;
            }

            return true;
        }

        static bool Transform(string xslPath, string xmlPath, string htmlPath)
        {
            try
            {
                string directory = Path.GetDirectoryName(htmlPath);
                if (Directory.Exists(directory) == false)
                {
                    Directory.CreateDirectory(directory);
                }

                XslCompiledTransform transform = new XslCompiledTransform();
                transform.Load(xslPath);
                transform.Transform(xmlPath, htmlPath);
                return true;
            }
            catch (Exception e)
            {
                LogError(e.Message);
                return false;
            }
        }

        static void LogError(string output)
        {
            Console.WriteLine("Error: " + output);
        }
    }
}
