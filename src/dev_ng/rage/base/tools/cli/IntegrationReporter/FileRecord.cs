﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace IntegrationReporter
{
    [Serializable]
    public class FileRecord
    {
        [XmlAttribute]
        public string Name;

        [XmlAttribute]
        public int Revision;

        [XmlAttribute]
        public bool Integrated;

        [XmlAttribute]
        public string Action;

        public FileRecord()
        {
            Name = null;
            Revision = -1;
            Integrated = false;
            Action = null;
        }

        public FileRecord(string name, int revision, bool integrated, string action)
        {
            Name = name;
            Revision = revision;
            Integrated = integrated;
            Action = action;
        }
    }
}
