// A quick coop between David Etherton (from the past with a single cpp file which did the same trick for MP3), 
// David Muir (who set up the updated version of this), and Klaas who only added some per file shenanigans. 
// 09/09/2013

// Things to deal with
// - Endianness

#include "data/aes.h"
#include "data/base64.h"
#include "file/packfile.h"
#include "system/endian.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#if !HACK_GTA4
#error Not set up for any non-HACK_GTA4 project.
#endif // !HACK_GTA

#define SWAP( val ) ( ( val << 24 ) | ( ( val << 8 ) & 0x00FF0000 ) | ( ( val >> 8 ) & 0x0000FF00 ) | ( val >> 24 ) )

using namespace rage;

// Can't change the code, so we extend
namespace rage {
	extern bool rijndael_setup(const unsigned char *key, int keylen, int num_rounds, symmetric_key *skey);
}

class AESPlus : public AES
{
public:
	AESPlus(unsigned key = 0 ) : AES(key) {}
	void SetKey(unsigned key_id, const unsigned char keysequence[32]) {
		rijndael_setup(keysequence,32,0,&key);
		key.rijndael.extra_rounds = key_id >> 28;
	}
};
 
bool ProcessPackfile(u8* buf, const char* file, u32 dest_key_id, unsigned char* dest_key, const char* dest_name)
{
	fiPackHeader* hdr = (fiPackHeader*)buf;
	if (hdr->m_Magic != '7FPR' && hdr->m_Magic != 'RPF7') {
		fprintf(stderr,"file '%s' is not in RPF7 format\n",file);
		return false;
	}

	unsigned source_key = 0;
	const char *source_name = 0;

	bool swapped = (hdr->m_Magic == '7FPR');
	switch (hdr->m_Encrypted) {
	case AES_KEY_ID_GTA5_PS3:
	case SWAP( AES_KEY_ID_GTA5_PS3 ): 
		swapped = true;
		source_key = AES_KEY_ID_GTA5_PS3; 
		source_name = "ps3"; 
		break;
	case AES_KEY_ID_GTA5_360: 
	case SWAP( AES_KEY_ID_GTA5_360 ): 
		swapped = true;
		source_key = AES_KEY_ID_GTA5_360; 
		source_name = "xbox360"; 
		break;
	case AES_KEY_ID_GTA5_PC: 
		source_key = AES_KEY_ID_GTA5_PC; 
		source_name = "pc"; 
		break;
	case AES_KEY_ID_GTA5_AVX:
		source_key = AES_KEY_ID_GTA5_AVX;
		source_name = "durango";
		break;
	default: 
		fprintf(stderr,"unknown encryption key id %08x\n", hdr->m_Encrypted); 
		return (EXIT_FAILURE); 
		break;
	}

	printf("archive '%s' encoded with key %s\n",file,source_name);

	u32 entryCount = swapped ? SWAP( hdr->m_EntryCount ) : hdr->m_EntryCount;
	u32 nameHeapSize = swapped ? SWAP( hdr->m_NameHeapSize ) : hdr->m_NameHeapSize;
	// The MSB will be set if data is compressed with XCompress instead of Zlib.
	nameHeapSize &= 0xFFFFFFF; 
	u32 encrypted = swapped ? SWAP( hdr->m_Encrypted ) : hdr->m_Encrypted;

	unsigned int totalSize = (sizeof(fiPackEntry) * entryCount) + nameHeapSize;
	u8* buffer = buf + sizeof(fiPackHeader);
	u8* nameheap = buffer + (sizeof(fiPackEntry) * entryCount);
	AES aes(source_key);
	aes.Decrypt(buffer,totalSize);

	AESPlus aes2(AES_KEY_ID_DEFAULT);
    hdr->m_Encrypted = swapped ? SWAP(dest_key_id) : dest_key_id;
	aes2.SetKey(dest_key_id, dest_key);

	// Reencrypt individual files
	fiPackEntry* entries = (fiPackEntry*) buffer;
	for (u32 i = 0; i < entryCount; ++i)
	{
		fiPackEntry pe = entries[i];

		if (swapped)
		{
			u64 &dw0 = *(u64*)&pe;
			u32 &w2 = pe.u.file.m_UncompressedSize;
			u32 &w3 = pe.u.file.m_Encrypted;
			dw0 = sysEndian::Swap(dw0);
			w2 = sysEndian::Swap(w2);
			w3 = sysEndian::Swap(w3);
		}

		const char* name = (const char*)(nameheap + pe.m_NameOffset);
        printf("processing entry '%s'\n", name);

        bool rekey = false;
        rekey |= pe.IsFile() && pe.u.file.m_Encrypted;
        rekey |= pe.IsResource();

        const char* ext = name + strlen(name) - 4;
        bool isRpf = !stricmp(ext, ".rpf");
        rekey |= pe.IsFile() && isRpf;

		if (rekey)
		{
			printf("Reencrypting %s %s\n", pe.IsResource() ? "resource" : "file", name);

			size_t size = pe.GetConsumedSize();
			u8* filebuffer = buf + pe.GetFileOffset();

            if (isRpf)
			{
                if (!ProcessPackfile(filebuffer, name, dest_key_id, dest_key, name))
                    return false;
			}
			else
			{
				aes.Decrypt(filebuffer, size);
				aes2.Encrypt(filebuffer,size);
			}
		}
	}

	aes2.Encrypt(buffer, totalSize);
	printf("re-keyed archive '%s' to %s\n",file,dest_name);
}

int main(int argc,char **argv)
{
	u32 override_key_id = 0;
	static unsigned char override_key[32];

	const char *dest_id_override = 0;

	if (argc == 6 && !stricmp(argv[2], "-id") && strlen(argv[3]) == 8 && !stricmp(argv[4], "-key"))
	{
		override_key_id = strtoul(argv[3], NULL, 16);
		printf("Override key %08X\n", override_key_id);

		FILE* f = fopen(argv[5], "rb");
		if (!f) {
			fprintf(stderr,"cannot open file '%s'\n",argv[1]);
			return (EXIT_FAILURE);
		}
		fread(override_key, 1, 32, f);
		fclose(f);
	}
	else if (argc == 2 && argc == 3) {
		// no problem 	
	}
	else
	{
		fprintf(stderr,"usage: \n\t%s archive.rpf [default|ps3|xbox360|pc|durango]\n\t\tLast parameter causes rpf to be re-keyed with that key.\n",argv[0]);
		fprintf(stderr,"\t%s archive.rpf -id <id> -key <key> \n\t\tId sets the key identifier, and the key is a binary file.\n",argv[0]);
		return (EXIT_FAILURE);
	}

    char keyBase64[128] = {0};
    unsigned charsUsed = 0;
    datBase64::Encode(override_key, sizeof(override_key), keyBase64, sizeof(keyBase64), &charsUsed);
    printf("base64 key: %s\n", keyBase64);

	if (argc == 3 || argc  == 6)
	{
		FILE *f = fopen(argv[1],"r+b");
		if (!f) {
			fprintf(stderr,"cannot open file '%s'\n",argv[1]);
			return (EXIT_FAILURE);
		}

		fseek(f, 0, SEEK_END);
		u32 fileSize = ftell(f);
		rewind(f); 

        u8* buf = new u8[fileSize];
        fread(buf, fileSize, 1, f);

		u32 dest_key = override_key_id;
        const char* dest_name = NULL;

		if (argc == 3)
		{
			if (!strcmp(argv[2],"ps3"))
				dest_key = AES_KEY_ID_GTA5_PS3, dest_name = "ps3";
			else if (!strcmp(argv[2],"xbox360"))
				dest_key = AES_KEY_ID_GTA5_360, dest_name = "xbox360";
			else if (!strcmp(argv[2],"pc"))
				dest_key = AES_KEY_ID_GTA5_PC, dest_name = "pc";
			else if (!strcmp(argv[2],"durango"))
				dest_key = AES_KEY_ID_GTA5_AVX, dest_name = "durango";
			else {
				fprintf(stderr,"unknown dest key '%s'\n",argv[2]);
				return 1;
			}
		}

		if (override_key_id > 0)
		{
			dest_key = override_key_id;
			dest_name = "override";
		}
 
#if 1
        if (!ProcessPackfile(buf, argv[1], dest_key, override_key, dest_name))
            return EXIT_FAILURE;

		fseek(f,0,SEEK_SET);
		fwrite(buf,1,fileSize,f);
		printf("re-keyed archive '%s' to %s\n",argv[1],dest_name);

		fclose(f);
#else

		fiPackHeader hdr;
		fread(&hdr,sizeof(hdr),1,f);
		if (hdr.m_Magic != '7FPR' && hdr.m_Magic != 'RPF7') {
			fprintf(stderr,"file '%s' is not in RPF7 format\n",argv[1]);
			return (EXIT_FAILURE);
		}

		unsigned source_key = 0, dest_key = 0;
		const char *source_name = 0, *dest_name = 0;

		bool swapped = (hdr.m_Magic == '7FPR');
		switch (hdr.m_Encrypted) {
		case AES_KEY_ID_GTA5_PS3:
		case SWAP( AES_KEY_ID_GTA5_PS3 ): 
			swapped = true;
			source_key = AES_KEY_ID_GTA5_PS3; 
			source_name = "ps3"; 
			break;
		case AES_KEY_ID_GTA5_360: 
		case SWAP( AES_KEY_ID_GTA5_360 ): 
			swapped = true;
			source_key = AES_KEY_ID_GTA5_360; 
			source_name = "xbox360"; 
			break;
		case AES_KEY_ID_GTA5_PC: 
			source_key = AES_KEY_ID_GTA5_PC; 
			source_name = "pc"; 
			break;
		case AES_KEY_ID_GTA5_AVX:
			source_key = AES_KEY_ID_GTA5_AVX;
			source_name = "durango";
			break;
		default: 
			fprintf(stderr,"unknown encryption key id %08x\n", hdr.m_Encrypted); 
			return (EXIT_FAILURE); 
			break;
		}

		printf("archive '%s' encoded with key %s\n",argv[1],source_name);

		u32 entryCount = swapped ? SWAP( hdr.m_EntryCount ) : hdr.m_EntryCount;
		u32 nameHeapSize = swapped ? SWAP( hdr.m_NameHeapSize ) : hdr.m_NameHeapSize;
		// The MSB will be set if data is compressed with XCompress instead of Zlib.
		nameHeapSize &= 0xFFFFFFF; 
		u32 encrypted = swapped ? SWAP( hdr.m_Encrypted ) : hdr.m_Encrypted;

		unsigned int totalSize = (sizeof(fiPackEntry) * entryCount) + nameHeapSize;
		char *buffer = new char[totalSize];
		char *nameheap = buffer + (sizeof(fiPackEntry) * entryCount);
		fread(buffer,1,totalSize,f);
		AES aes(source_key);
		aes.Decrypt(buffer,totalSize);

		dest_key = override_key_id;

		if (argc == 3)
		{
			if (!strcmp(argv[2],"ps3"))
				dest_key = AES_KEY_ID_GTA5_PS3, dest_name = "ps3", hdr.m_Encrypted = SWAP( AES_KEY_ID_GTA5_PS3 );
			else if (!strcmp(argv[2],"xbox360"))
				dest_key = AES_KEY_ID_GTA5_360, dest_name = "xbox360", hdr.m_Encrypted = SWAP( AES_KEY_ID_GTA5_360 );
			else if (!strcmp(argv[2],"pc"))
				dest_key = AES_KEY_ID_GTA5_PC, dest_name = "pc", hdr.m_Encrypted = AES_KEY_ID_GTA5_PC;
			else if (!strcmp(argv[2],"durango"))
				dest_key = AES_KEY_ID_GTA5_AVX, dest_name = "durango", hdr.m_Encrypted = AES_KEY_ID_GTA5_AVX;
			else {
				fprintf(stderr,"unknown dest key '%s'\n",argv[2]);
				return 1;
			}
		}

		AESPlus aes2;

		if (override_key_id > 0)
		{
			dest_key = override_key_id;
			dest_name = "override";
			hdr.m_Encrypted = swapped ? SWAP(override_key_id) : override_key_id;
			aes2.SetKey(override_key_id, override_key);
		}

		// Reencrypt individual files
		fiPackEntry* entries = (fiPackEntry*) buffer;
		for (u32 i = 0; i < entryCount; ++i)
		{
			fiPackEntry pe = entries[i];

			if (swapped)
			{
				u64 &dw0 = *(u64*)&pe;
				u32 &w2 = pe.u.file.m_UncompressedSize;
				u32 &w3 = pe.u.file.m_Encrypted;
				dw0 = sysEndian::Swap(dw0);
				w2 = sysEndian::Swap(w2);
				w3 = sysEndian::Swap(w3);
			}

			const char* name = nameheap + pe.m_NameOffset;

			if (pe.IsFile() && pe.u.file.m_Encrypted)
			{
				printf("Reencrypting file %s\n",name);

				size_t size = pe.GetConsumedSize();
				char* filebuffer = new char[size];

				fseek(f, pe.GetFileOffset() + totalSize, SEEK_SET);
				fread(filebuffer, 1, size, f);
				aes.Decrypt(filebuffer, size);
				aes2.Encrypt(filebuffer,size);
				fseek(f, pe.GetFileOffset() + totalSize, SEEK_SET);

				fwrite(filebuffer,1,totalSize,f);
				delete [] filebuffer;
			}
			else if (pe.IsResource())
			{
				printf("Reencrypting resource %s\n",name);
				// Going to assume all the resources in this are encrypted
				size_t size = pe.GetConsumedSize();
				char* filebuffer = new char[size];

				fseek(f, pe.GetFileOffset() + totalSize, SEEK_SET);
				fread(filebuffer, 1, size, f);
				aes.Decrypt(filebuffer, size);
				aes2.Encrypt(filebuffer,size);
				fseek(f, pe.GetFileOffset() + totalSize, SEEK_SET);

				fwrite(filebuffer,1,totalSize,f);
				delete [] filebuffer;
			}
		}

		fseek(f,0,SEEK_SET);
		fwrite(&hdr,sizeof(hdr),1,f);

		aes2.Encrypt(buffer,totalSize);
		fwrite(buffer,1,totalSize,f);
		printf("re-keyed archive '%s' to %s\n",argv[1],dest_name);

		fclose(f);
#endif
	}

	return (EXIT_SUCCESS);
}
