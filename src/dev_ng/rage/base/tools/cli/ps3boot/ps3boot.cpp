// 
// /ps3boot.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#include "file/tcpip.h"

#if __WIN32PC

#include "file/stream.h"
#include "file/winsock.h"
#include "system/endian.h"
#include "system/xtl.h"

#define TCPIP fiDeviceTcpIp::GetInstance()
#define LOCAL fiDeviceLocal::GetInstance()

using namespace rage;

namespace rage {
	extern void (*g_PrintString)(const char *);
}

int main(int argc,const char **argv)
{
	// We intentionally bypass a lot of standard startup code, so do this explicitly.
	fiDeviceTcpIp::InitClass();

	if (argc < 2) {
		Displayf("usage: ps3boot [-t ipaddr] filename.self [args...]");
		return 1;
	}

	--argc;
	++argv;

	const char *targetName = getenv("PS3BOOT");
	if (!strcmp(argv[0],"-t")) {
		if (argc>1) {
			targetName = argv[1];
			argv += 2;
			argc -= 2;
		}
		else {
			Errorf("-t switch missing parameter");
			return 1;
		}
	}
	else if (!targetName) {
		Errorf("-t switch not specified and PS3BOOT environment variable not set.");
		return 1;
	}

	const char *exeName = argv[0];
	++argv;
	--argc;

	const char *ext = strrchr(exeName,'.');
	if (!ext || strcmp(ext,".self")) {
		Errorf("Executable extension should be .self.",ext);
		return 1;
	}

	fiStream *S = fiStream::Open(exeName);
	if (!S) {
		Errorf("Cannot read executable '%s'",exeName);
		return 1;
	}

	fiHandle control = TCPIP.Connect(targetName, 9099);
	if (!fiIsValidHandle(control)) {
		Errorf("Cannot connect to '%s'",targetName);
		S->Close();
		return 1;
	}

	char lastArg[64];
	struct sockaddr_in sin;
	int namelen = sizeof(sin);
	if (getsockname((int) control,(sockaddr*)&sin,&namelen) == 0)
		formatf(lastArg,sizeof(lastArg),"-ps3boot=%d.%d.%d.%d",
			sin.sin_addr.S_un.S_un_b.s_b1,sin.sin_addr.S_un.S_un_b.s_b2,
			sin.sin_addr.S_un.S_un_b.s_b3,sin.sin_addr.S_un.S_un_b.s_b4);
	else
		Warningf("Cannot figure out my IP?");

	int size = S->Size();

	u32 sendVersion = sysEndian::Swap((u32)0);
	int sendSize = sysEndian::Swap(size);

	TCPIP.SafeWrite(control,&sendVersion,sizeof(sendVersion));
	TCPIP.SafeWrite(control,&sendSize,sizeof(sendSize));

	char buffer[16384];
	int thisRead;
	while (size && (thisRead = S->Read(buffer,size<sizeof(buffer)?size:sizeof(buffer))) > 0) {
		TCPIP.SafeWrite(control,buffer,thisRead);
		size -= thisRead;
	}
	S->Close();
	if (size) {
		TCPIP.Close(control);
		Errorf("Failed to copy executable over network.");
		return 1;
	}

	int sendArgc = sysEndian::Swap(argc+1);
	TCPIP.SafeWrite(control,&sendArgc,sizeof(sendArgc));
	for (int i=0; i<argc; i++) {
		int len = strlen(argv[i]) + 1;
		int sendLen = sysEndian::Swap(len);
		TCPIP.SafeWrite(control,&sendLen,sizeof(sendLen));
		TCPIP.SafeWrite(control,argv[i],len);
	}
	int len = strlen(lastArg) + 1;
	int sendLen = sysEndian::Swap(len);
	TCPIP.SafeWrite(control,&sendLen,sizeof(sendLen));
	TCPIP.SafeWrite(control,lastArg,len);

	TCPIP.Close(control);
	Displayf("Launched '%s'...",exeName);

	// Wait for TTY output connection.
	fiHandle listener = TCPIP.Listen(9098,1);
	control = TCPIP.Pickup(listener);
	TCPIP.Close(listener);

	Displayf("[reading tty output]");

	char tty[512];
	int count;
	while ((count = TCPIP.Read(control,tty,sizeof(tty)-1)) > 0) {
		tty[count] = '\0';
		g_PrintString(tty);
	}
	TCPIP.Close(control);
	Displayf("[remote process closed tty socket]");

	return 0;
}

#else	// !__WIN32PC

#define DISABLE_RFS
#include "system/main.h"

#include "grcore/device.h"
#include "grcore/im.h"
#include "grcore/setup.h"
#include "string/string.h"

#include <netex/libnetctl.h>
#include <sysutil/sysutil_sysparam.h>
#include <sysutil/sysutil_common.h>

#define HDDGAME

#ifdef HDDGAME
#include <sysutil/sysutil_hddgame.h>		// not in standard SDK!
#else
#include <sysutil/sysutil_gamedata.h>
#endif
#include <sys/process.h>

using namespace rage;

static sys_memory_container_t memory_container;

#define GAMEDATA_MEMORY_CONTAINER_SIZE		(3 * 1024 * 1024)
#define GAMEDATADIRNAME		"RAGE10000"
#define GAMEDATA_SYSTEMVERSION	"01.0000"

#define SAMPLE_PARAMSFO_TITLE		"RAGE PS3 BOOTLOADER"
#define SAMPLE_PARAMSFO_TITLEID		GAMEDATADIRNAME
#define SAMPLE_PARAMSFO_VERSION		"01.00"
#define SAMPLE_PARAMSFO_PARENTALLEV		(3)
#define SAMPLE_PARAMSFO_ATTRIBUTE		CELL_GAMEDATA_ATTR_NORMAL

#ifdef HDDGAME

void cb_data_status_hddgame( CellHddGameCBResult *result, CellHddGameStatGet *get, CellHddGameStatSet *set )
{
	printf( "cb_data_status_hddgame() start\n");
	//dumpCellHddGameStatGet(get);

	//memcpy(&sHddGameStatGet, get, sizeof(CellHddGameStatGet)) ;

	if (!get->isNewData) {
#if 0
		printf("Data %s is already exist.\n", get->contentInfoPath );

		int WORK_SIZEKB = SAMPLE_WORK_SIZEKB;
		int NEED_SIZEKB = 0;
		NEED_SIZEKB = get->hddFreeSizeKB - WORK_SIZEKB ;

		if (NEED_SIZEKB < 0) {
			result->errNeedSizeKB = NEED_SIZEKB;
			result->result = CELL_HDDGAME_CBRESULT_ERR_NOSPACE;
			printf("HDD size check error. needs %d KB disc space more.\n", result->errNeedSizeKB * -1 );
		} else {
			result->result = CELL_HDDGAME_CBRESULT_OK_CANCEL;	
			result->reserved = NULL;							
		}
#endif
		result->result = CELL_HDDGAME_CBRESULT_OK_CANCEL;	
		result->reserved = NULL;							
	}
	else {
		printf("Data %s is not exist yet.\n", get->contentInfoPath );

		result->result = CELL_HDDGAME_CBRESULT_ERR_NODATA;
		result->reserved = NULL;						
	}

	set->setParam = NULL;			
	set->reserved = NULL;			

	printf( "result->result=[%d]\n", result->result );
	printf( "cb_data_status_hddgame() end\n");

	return;
}
#else

void cb_data_status_gamedata( CellGameDataCBResult *result, CellGameDataStatGet *get, CellGameDataStatSet *set )
{
	printf( "cb_data_status_gamedata() start\n");
	// dumpCellGameDataStatGet(get);

	// memcpy(&sGameDataStatGet, get, sizeof(CellGameDataStatGet)) ;

	if (!get->isNewData) {
#if 0
		int CURRENT_SIZEKB = 0;
		int MINIMUM_SIZEKB = 0;

		printf("Data %s is already exist.\n", get->contentInfoPath );

		CURRENT_SIZEKB = get->hddFreeSizeKB;

		MINIMUM_SIZEKB = CURRENT_SIZEKB;

		int gameDataSizeKB = 0 ;
		int ret = cellGameDataGetSizeKB( &gameDataSizeKB );
		if(ret < 0){
			printf("cellGameDataGetSizeKB Error: 0x%08x\n", ret);
		} else {
			printf("cellGameDataGetSizeKB Ok: GameDataSizeKB = [%d]\n", gameDataSizeKB);
		}

		if (MINIMUM_SIZEKB < 0) {
			result->errNeedSizeKB = MINIMUM_SIZEKB;
			result->result = CELL_GAMEDATA_CBRESULT_ERR_NOSPACE;
			printf("HDD size check error. needs %d KB disc space more.\n", result->errNeedSizeKB * -1 );
			return;
		}
#endif
	}
	else {
		int NEW_SIZEKB = 0;
		int NEED_SIZEKB = 0;

		printf("Data %s is not exist yet.\n", get->contentInfoPath );

		/* CellFsStat status;
		const char *fileList[] = {
			GAMEDATA_1,
			GAMEDATA_2,
			ICON0_PNG,
			ICON1_PAM,
			PIC1_PNG
		};
		int cnt = sizeof(fileList)/sizeof(char *);
		int i = 0 ;
		for(i=0 ; i < cnt ; i++){
			snprintf(filePath_I, CELL_FS_MAX_FS_PATH_LENGTH, "%s/%s",  fileList[i]);
			ret = cellFsStat(filePath_I, &status);
			if(ret == CELL_FS_SUCCEEDED){
				NEW_SIZEKB += ( (status.st_size + 1023) / 1024 ) ;
			}
		} */
		printf("file size = %d\n", NEW_SIZEKB) ;

		strcpy( get->getParam.title,			SAMPLE_PARAMSFO_TITLE );
		strcpy( get->getParam.titleId,			SAMPLE_PARAMSFO_TITLEID );
		strcpy( get->getParam.dataVersion,		SAMPLE_PARAMSFO_VERSION );

		get->getParam.parentalLevel = SAMPLE_PARAMSFO_PARENTALLEV;
		get->getParam.attribute     = SAMPLE_PARAMSFO_ATTRIBUTE;

		NEW_SIZEKB += get->sysSizeKB;

		NEED_SIZEKB = get->hddFreeSizeKB - NEW_SIZEKB;

		printf("hddFreeSizeKB=[%d] NEW_SIZEKB=[%d] NEED_SIZEKB=[%d]\n", get->hddFreeSizeKB, NEW_SIZEKB, NEED_SIZEKB);

		if (NEED_SIZEKB < 0) {
			result->errNeedSizeKB = NEED_SIZEKB;
			result->result = CELL_GAMEDATA_CBRESULT_ERR_NOSPACE;
			printf("HDD size check error. needs %d KB disc space more.\n", result->errNeedSizeKB * -1 );
			return;
		}
	}

	set->setParam = &get->getParam;
	set->reserved = NULL;

	result->result = CELL_GAMEDATA_CBRESULT_OK;
	result->reserved = NULL;

	printf( "result->result=[%d]\n", result->result );
	printf( "cb_data_status_gamedata() end\n");

	return;
}
#endif

static void GameData(void*)
{
	if( sys_memory_container_create( &memory_container, GAMEDATA_MEMORY_CONTAINER_SIZE) != 0 ) {
		printf("failed create container\n");
		return ;
	}
	printf("memory_container = %d\n", memory_container);

	int ret = 
#ifdef HDDGAME
		cellHddGameCheck( CELL_HDDGAME_VERSION_CURRENT,
#else
		cellGameDataCheckCreate2( CELL_GAMEDATA_VERSION_CURRENT,
#endif
		GAMEDATADIRNAME,
#ifdef HDDGAME
		CELL_HDDGAME_ERRDIALOG_ALWAYS,
		cb_data_status_hddgame,
#else
		CELL_GAMEDATA_ERRDIALOG_ALWAYS,
		cb_data_status_gamedata,
#endif
		memory_container
		);

	printf("cellGameDataCheckCreate2() : 0x%x\n", ret);

	sys_memory_container_destroy( memory_container );

#if 0
	if(ret == CELL_GAMEDATA_RET_OK) {
		saveSampleData();

		if(is_setSystemVersion){
			ret = cellGameDataSetSystemVer( GAMEDATA_SYSTEMVERSION );
			if(ret){
				printf("cellGameDataSetSystemVer Error: 0x%08x\n", ret);
			} else {
				printf("cellGameDataSetSystemVer Ok\n");
			}
		}
	}
#endif
}

namespace rage {
	extern char g_BootServerStatusString[64];
	extern void BootServer(void *);
}

int Main()
{
	union CellNetCtlInfo info;
	memset(&info,0,sizeof(info));

	sysIpcThreadId thr = sysIpcCreateThread(GameData,NULL,sysIpcMinThreadStackSize,PRIO_NORMAL,"GameData");
	sysIpcWaitThreadExit(thr);

	sysIpcCreateThread(BootServer,NULL,sysIpcMinThreadStackSize,PRIO_NORMAL,"BootServer");

	grcSetup setup;
	setup.Init("", "Rage PS3 Bootloader");
	setup.BeginGfx(false);
	setup.CreateDefaultFactories();
	Displayf("enter loop");
	int basex = 100, basey = 100;
	int corner = 0;

	char version[64];
	formatf(version,sizeof(version),"Version 1.03 for SDK %x.%03x",
		CELL_SDK_VERSION>>12,CELL_SDK_VERSION&4095);

	const char *banners[4] = {
		"RAGE PS3 Bootloader \\",
		"RAGE PS3 Bootloader |",
		"RAGE PS3 Bootloader /",
		"RAGE PS3 Bootloader -",
	};

	do {
		setup.BeginUpdate();
			// do update-related stuff
			if (corner % 1000 == 0)
				cellNetCtlGetInfo(CELL_NET_CTL_INFO_IP_ADDRESS, &info);
		setup.EndUpdate();
		setup.BeginDraw();
			++corner;
			if (corner == 1000)
				basex = GRCDEVICE.GetWidth() - 400;
			else if (corner == 2000)
				basey = GRCDEVICE.GetHeight() - 200;
			else if (corner == 3000)
				basex = 100;
			else if (corner == 4000) {
				basey = 100;
				corner = 0;
			}
			// do rendering here
			grcDraw2dText(basex,basey,banners[corner&3]);
			grcDraw2dText(basex,basey+20,version);
			grcDraw2dText(basex,basey+40,info.ip_address);
			grcDraw2dText(basex,basey+60,g_BootServerStatusString);
		setup.EndDraw();
	} while (!setup.WantExit());
	
	setup.DestroyFactories();
	setup.EndGfx();
	setup.Shutdown();
	return 0;
}

#endif	// !__WIN32PC
