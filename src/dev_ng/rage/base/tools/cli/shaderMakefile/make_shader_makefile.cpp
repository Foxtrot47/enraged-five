// this could probably be two lines of perl.  I suck.
#define _CRT_SECURE_NO_DEPRECATE
#include <stdio.h>
#include <string.h>

int main(int argc,char **argv) 
{
	if (argc != 4) {
		printf("usage: %s preload.list output.mk shaderpath\n",argv[0]);
		printf("takes all shaders in preload.list and adds results to output.mk\n");
		return 1;
	}
	
	FILE *preload = fopen(argv[1],"r");
	if (!preload) {
		printf("cannot open '%s'\n",argv[1]);
		return 1;
	}
	FILE *makefile = fopen(argv[2],"w");
	if (!makefile) {
		printf("cannot create '%s'\n",argv[2]);
		fclose(preload);
		return 1;
	}

	fprintf(makefile,"SHELL = $(WINDIR)\\system32\\cmd.exe\n\n");

	fprintf(makefile,"ifeq 	($(PLATFORM),psn)\n");
	fprintf(makefile,"SHADERDIR = psn\n");
	fprintf(makefile,"SHADEREXT = cgx\n");
	fprintf(makefile,"else\n");
	fprintf(makefile,"ifeq 	($(PLATFORM),xenon)\n");
	fprintf(makefile,"SHADERDIR = fxl_final\n");
	fprintf(makefile,"SHADEREXT = fxc\n");
	fprintf(makefile,"else\n");
	fprintf(makefile,"SHADERDIR	= win32_30\n");
	fprintf(makefile,"SHADERDIR_30ATI9 = win32_30_atidx9\n");
	fprintf(makefile,"SHADERDIR_30ATI10 = win32_30_atidx10\n");
	fprintf(makefile,"SHADERDIR_30NV9 = win32_30_nvdx9\n");
	fprintf(makefile,"SHADERDIR_30NV10 = win32_30_nvdx10\n");
	fprintf(makefile,"SHADERDIR_40ATI10 = win32_40_atidx10\n");
	fprintf(makefile,"SHADERDIR_40NV10 = win32_40_nvdx10\n");
	fprintf(makefile,"SHADEREXT = fxc\n");
	fprintf(makefile,"endif\n");
	fprintf(makefile,"endif\n\n");

	if (strchr(argv[3],'$')) {
		const char *start = strchr(argv[3],'$');
		if (strchr(start,')')) {
			const char *stop = strchr(start,')') + 1;
			char varTemp[256];
			int len = stop-start;
			memcpy(varTemp,start,len);
			varTemp[len] = 0;
			fprintf(makefile,"ifeq    (%s,)\n",varTemp);
			fprintf(makefile,"badEnv:\n");
			fprintf(makefile,"\t@echo $%s env variable is not defined.\n",varTemp);
			fprintf(makefile,"\t@exit 1\n");
			fprintf(makefile,"endif\n\n");
		}
	}
	fprintf(makefile,"RAGE_DIRECTORY = $(subst \\,/,$(RAGE_DIR))\n\n");
	fprintf(makefile,"MAKEDEP_PATH = $(subst \\,/,$(RS_TOOLSROOT)\\bin\\coding\\makedep.exe)\n\n");
	fprintf(makefile,"MAKESHADER_PATH = $(subst \\,/,$(RS_TOOLSROOT)\\script\\coding\\shaders\\makeshader.bat)\n\n");
	fprintf(makefile,"SHADERPATH = $(subst \\,/,%s)\n\n",argv[3]);
	fprintf(makefile,"SHADERPATHDOS = $(subst /,\\,$(SHADERPATH))\n\n",argv[3]);
	fprintf(makefile,"all: alltargets\n\n");
	fprintf(makefile,
		"clean:\n"
		"\tif exist $(subst /,\\,$(SHADERPATH)$(SHADERDIR)/*.$(SHADEREXT)) del $(subst /,\\,$(SHADERPATH)$(SHADERDIR)/*.$(SHADEREXT))\n"
		"\tif exist $(SHADERDIR)\\*.d del $(SHADERDIR)\\*.d\n"
		"\tif exist $(subst /,\\,$(SHADERPATH)$(SHADERDIR_30ATI9)/*.$(SHADEREXT)) del $(subst /,\\,$(SHADERPATH)$(SHADERDIR_30ATI9)/*.$(SHADEREXT))\n"
		"\tif exist $(SHADERDIR_30ATI9)\\*.d del $(SHADERDIR_30ATI9)\\*.d\n"
		"\tif exist $(subst /,\\,$(SHADERPATH)$(SHADERDIR_30ATI10)/*.$(SHADEREXT)) del $(subst /,\\,$(SHADERPATH)$(SHADERDIR_30ATI10)/*.$(SHADEREXT))\n"
		"\tif exist $(SHADERDIR_30ATI10)\\*.d del $(SHADERDIR_30ATI10)\\*.d\n"
		"\tif exist $(subst /,\\,$(SHADERPATH)$(SHADERDIR_30NV9)/*.$(SHADEREXT)) del $(subst /,\\,$(SHADERPATH)$(SHADERDIR_30NV9)/*.$(SHADEREXT))\n"
		"\tif exist $(SHADERDIR_30NV9)\\*.d del $(SHADERDIR_30NV9)\\*.d\n"
		"\tif exist $(subst /,\\,$(SHADERPATH)$(SHADERDIR_30NV10)/*.$(SHADEREXT)) del $(subst /,\\,$(SHADERPATH)$(SHADERDIR_30NV10)/*.$(SHADEREXT))\n"
		"\tif exist $(SHADERDIR_30NV10)\\*.d del $(SHADERDIR_30NV10)\\*.d\n"
		"\tif exist $(subst /,\\,$(SHADERPATH)$(SHADERDIR_40ATI10)/*.$(SHADEREXT)) del $(subst /,\\,$(SHADERPATH)$(SHADERDIR_40ATI10)/*.$(SHADEREXT))\n"
		"\tif exist $(SHADERDIR_40ATI10)\\*.d del $(SHADERDIR_40ATI10)\\*.d\n"
		"\tif exist $(subst /,\\,$(SHADERPATH)$(SHADERDIR_40NV10)/*.$(SHADEREXT)) del $(subst /,\\,$(SHADERPATH)$(SHADERDIR_40NV10)/*.$(SHADEREXT))\n"
		"\tif exist $(SHADERDIR_40NV10)\\*.d del $(SHADERDIR_40NV10)\\*.d\n"
		"\n");
	fprintf(makefile,"# FLAGS = -useATGCompiler\n");
	fprintf(makefile,"FLAGS = -noPerformanceDump -quiet\n\n");

	char buf[256];
	while (fgets(buf,sizeof(buf),preload)) {
		strtok(buf,"\r\n");
		if (!buf[0] || buf[0] == ';')
			continue;
		char *ext = strrchr(buf,'.');
		if (ext) {
			*ext = 0;
			fprintf(makefile,"TARGETS += $(SHADERPATH)$(SHADERDIR)/%s.$(SHADEREXT)\n\n",buf,buf);
			fprintf(makefile,"-include $(SHADERDIR)/%s.d\n\n",buf);
			fprintf(makefile,"$(SHADERPATH)$(SHADERDIR)/%s.$(SHADEREXT): %s.fx\n",buf,buf);
			fprintf(makefile,"\tif not exist $(SHADERDIR)\\* mkdir $(SHADERDIR)\n");
			fprintf(makefile,"\t$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\\base\\src $(SHADERPATH)$(SHADERDIR)/%s.$(SHADEREXT) %s.fx $(SHADERDIR)/%s.d\n",buf,buf,buf);
			fprintf(makefile,"\tcall $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) %s.fx\n",buf);
			fprintf(makefile,"\n");

			fprintf(makefile, "ifeq ($(PLATFORM),win32)\n");
			
			// win32_30_ati9
			fprintf(makefile,"TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/%s.$(SHADEREXT)\n\n",buf,buf);
			fprintf(makefile,"-include $(SHADERDIR_30ATI9)/%s.d\n\n",buf);
			fprintf(makefile,"$(SHADERPATH)$(SHADERDIR_30ATI9)/%s.$(SHADEREXT): %s.fx\n",buf,buf);
			fprintf(makefile,"\tif not exist $(SHADERDIR_30ATI9)\\* mkdir $(SHADERDIR_30ATI9)\n");
			fprintf(makefile,"\t$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\\base\\src $(SHADERPATH)$(SHADERDIR_30ATI9)/%s.$(SHADEREXT) %s.fx $(SHADERDIR_30ATI9)/%s.d\n",buf,buf,buf);
			fprintf(makefile,"\tcall $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) %s.fx\n",buf);
			fprintf(makefile,"\n");

			// win32_30_ati10
			fprintf(makefile,"TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/%s.$(SHADEREXT)\n\n",buf,buf);
			fprintf(makefile,"-include $(SHADERDIR_30ATI10)/%s.d\n\n",buf);
			fprintf(makefile,"$(SHADERPATH)$(SHADERDIR_30ATI10)/%s.$(SHADEREXT): %s.fx\n",buf,buf);
			fprintf(makefile,"\tif not exist $(SHADERDIR_30ATI10)\\* mkdir $(SHADERDIR_30ATI10)\n");
			fprintf(makefile,"\tif not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\n");
			fprintf(makefile,"\t$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\\base\\src $(SHADERPATH)$(SHADERDIR_30ATI10)/%s.$(SHADEREXT) %s.fx $(SHADERDIR_30ATI10)/%s.d\n",buf,buf,buf);
			fprintf(makefile,"\tif exist %%windir%%\\system32\\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) %s.fx\n",buf);
			fprintf(makefile,"\tif not exist %%windir%%\\system32\\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/%s.$(SHADEREXT)\n",buf);
			fprintf(makefile,"\n");

			// win32_30_nv9
			fprintf(makefile,"TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/%s.$(SHADEREXT)\n\n",buf,buf);
			fprintf(makefile,"-include $(SHADERDIR_30NV9)/%s.d\n\n",buf);
			fprintf(makefile,"$(SHADERPATH)$(SHADERDIR_30NV9)/%s.$(SHADEREXT): %s.fx\n",buf,buf);
			fprintf(makefile,"\tif not exist $(SHADERDIR_30NV9)\\* mkdir $(SHADERDIR_30NV9)\n");
			fprintf(makefile,"\t$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\\base\\src $(SHADERPATH)$(SHADERDIR_30NV9)/%s.$(SHADEREXT) %s.fx $(SHADERDIR_30NV9)/%s.d\n",buf,buf,buf);
			fprintf(makefile,"\tcall $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) %s.fx\n",buf);
			fprintf(makefile,"\n");

			// win32_30_nv10
			fprintf(makefile,"TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/%s.$(SHADEREXT)\n\n",buf,buf);
			fprintf(makefile,"-include $(SHADERDIR_30NV10)/%s.d\n\n",buf);
			fprintf(makefile,"$(SHADERPATH)$(SHADERDIR_30NV10)/%s.$(SHADEREXT): %s.fx\n",buf,buf);
			fprintf(makefile,"\tif not exist $(SHADERDIR_30NV10)\\* mkdir $(SHADERDIR_30NV10)\n");
			fprintf(makefile,"\tif not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)\n");
			fprintf(makefile,"\t$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\\base\\src $(SHADERPATH)$(SHADERDIR_30NV10)/%s.$(SHADEREXT) %s.fx $(SHADERDIR_30NV10)/%s.d\n",buf,buf,buf);
			fprintf(makefile,"\tif exist %%windir%%\\system32\\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) %s.fx\n",buf);
			fprintf(makefile,"\tif not exist %%windir%%\\system32\\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/%s.$(SHADEREXT)\n",buf);
			fprintf(makefile,"\n");

			// win32_40_ati10
			fprintf(makefile,"TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/%s.$(SHADEREXT)\n\n",buf,buf);
			fprintf(makefile,"-include $(SHADERDIR_40ATI10)/%s.d\n\n",buf);
			fprintf(makefile,"$(SHADERPATH)$(SHADERDIR_40ATI10)/%s.$(SHADEREXT): %s.fx\n",buf,buf);
			fprintf(makefile,"\tif not exist $(SHADERDIR_40ATI10)\\* mkdir $(SHADERDIR_40ATI10)\n");
			fprintf(makefile,"\tif not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\n");
			fprintf(makefile,"\t$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\\base\\src $(SHADERPATH)$(SHADERDIR_40ATI10)/%s.$(SHADEREXT) %s.fx $(SHADERDIR_40ATI10)/%s.d\n",buf,buf,buf);
			fprintf(makefile,"\tif exist %%windir%%\\system32\\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) %s.fx\n",buf);
			fprintf(makefile,"\tif not exist %%windir%%\\system32\\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/%s.$(SHADEREXT)\n",buf);
			fprintf(makefile,"\n");

			// win32_40_nv10
			fprintf(makefile,"TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/%s.$(SHADEREXT)\n\n",buf,buf);
			fprintf(makefile,"-include $(SHADERDIR_40NV10)/%s.d\n\n",buf);
			fprintf(makefile,"$(SHADERPATH)$(SHADERDIR_40NV10)/%s.$(SHADEREXT): %s.fx\n",buf,buf);
			fprintf(makefile,"\tif not exist $(SHADERDIR_40NV10)\\* mkdir $(SHADERDIR_40NV10)\n");
			fprintf(makefile,"\tif not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)\n");
			fprintf(makefile,"\t$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\\base\\src $(SHADERPATH)$(SHADERDIR_40NV10)/%s.$(SHADEREXT) %s.fx $(SHADERDIR_40NV10)/%s.d\n",buf,buf,buf);
			fprintf(makefile,"\tif exist %%windir%%\\system32\\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) %s.fx\n",buf);
			fprintf(makefile,"\tif not exist %%windir%%\\system32\\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/%s.$(SHADEREXT)\n",buf);
			fprintf(makefile,"\n");

			fprintf(makefile, "endif\n\n");
		}
	}

	fprintf(makefile,"TARGETS += $(SHADERPATH)preload.list\n\n");
	fprintf(makefile,"$(SHADERPATH)preload.list: preload.list\n");
	fprintf(makefile,"\tif not exist $(SHADERPATHDOS)* mkdir $(SHADERPATHDOS)\n");
	fprintf(makefile,"\tif exist $(SHADERPATHDOS)preload.list del /f $(SHADERPATHDOS)preload.list\n");
	fprintf(makefile,"\ttype preload.list > $(SHADERPATHDOS)preload.list\n\n");

	fprintf(makefile,"alltargets: $(TARGETS)\n");
	fprintf(makefile,"\techo DONE > $(SHADERDIR).dummy\n",buf);

	fclose(makefile);
	fclose(preload);

	return 0;
}
