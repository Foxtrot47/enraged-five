using System;
using System.Xml;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Diagnostics;

namespace xlast2c
{
    class Presence : Schema
    {
        string m_Name;
        uint m_Id;
        LocalizedString m_LString;
        uint m_StringId;

        public uint GetId()
        {
            return m_Id;
        }

        public override string GetName()
        {
            return "Presence_" + m_Name.Replace(" ", "");
        }

        public string GetTemplateString(string locale)
        {
            return m_LString[locale];
        }

        public void Reset(XmlElement mode,
            PropertyCollection props,
            LocalizedStringCollection lstrings)
        {
            m_Id = Property.ParseUInt(mode.GetAttribute("contextValue"));;
            m_StringId = Property.ParseUInt(mode.GetAttribute("stringId"));

            m_Name = mode.GetAttribute("friendlyName");

            m_LString = lstrings[m_StringId];

            //Find occurrences of {c#} and {p#} where # is a hex or decimal
            //number.  For instances of {c#} this number represents the
            //context id, and for instances of {p#} this number represents
            //the property id.

            //When the regexp is parsed the # part of {c#} and {p#} will be
            //tagged with "id".
            Regex rex = new Regex("\\{[cp](?<id>(0[xX][0-9a-fA-F]+)|([0-9]+))\\}");

            //FIXME (KB) - handle other localizations.
            MatchCollection mc = rex.Matches(m_LString["en-US"]);

            for(int i = 0; i < mc.Count; ++i)
            {
                //Get the value of the "id" tag.
                string itemId = mc[i].Result("${id}");

                if(null != props[itemId])
                {
                    this.AddMember(props[itemId]);
                }
            }

            Trace.WriteLine("  " + m_Name);
        }

        public override string GetEnum()
        {
            string str = base.GetEnum();

            if(!string.IsNullOrEmpty(str)){ str += "\n"; }

            //Id of the presence schema
            str += "    enum\n";
            str += "    {\n";
            str += String.Format("        PRESENCE_ID = {0}\n", this.GetName().ToUpper());
            str += "    };";

            return str;
        }

        public uint Id
        {
            get{return m_Id;}
        }
    }

    //PURPOSE
    //  Collection of presence schemas.
    class PresenceCollection
    {
        List<Presence> m_PresenceSchemas;

        public void Reset(XmlDocument doc,
            PropertyCollection props,
            LocalizedStringCollection lstrings)
        {
            Trace.WriteLine("Collecting presence schemas...");

            XmlElement proj = doc.DocumentElement["GameConfigProject"];
            XmlElement presence = null != proj ? proj["Presence"] : null;
            XmlElement mel = null != presence ? (XmlElement) presence.FirstChild : null;

            //Build the presence schemas

            m_PresenceSchemas = new List<Presence>();

            for(; null != mel; mel = (XmlElement) mel.NextSibling)
            {
                Presence pschema = new Presence();

                pschema.Reset(mel, props, lstrings);

                m_PresenceSchemas.Add(pschema);
            }
        }

        public IEnumerator<Presence> GetEnumerator()
        {
            return (null != m_PresenceSchemas) ? m_PresenceSchemas.GetEnumerator() : (IEnumerator<Presence>) null;
        }

        //PURPOSE
        //  Generates (in string form) the C++ class representation
        //  of all presence schemas.
        public virtual string ToCString()
        {
            string str = "";

            //Presence id enum
            str += "enum PresenceId\n";
            str += "{\n";
            foreach (Presence pres in m_PresenceSchemas)
            {
                str += String.Format("    {0} = {1},\n",
                                    pres.GetName().ToUpper(),
                                    pres.Id);
            }
            str += "};\n";

            int i = 0;
            foreach (Presence pres in m_PresenceSchemas)
            {
                str = str + pres.ToCString();

                if (i < m_PresenceSchemas.Count - 1)
                {
                    str += "\n\n";
                }
                ++i;
            }

            return str;
        }

        //PURPOSE
        //  Generates (in string form) the C++ class representation
        //  of all presence schemas.
        public virtual string ToCUtilString()
        {
            string str = "";

            //Presence id enum
            str += "enum PresenceIds\n";
            str += "{\n";

            foreach (Presence pres in m_PresenceSchemas)
            {
                str += String.Format("    {0} = {1},\n",
                                    pres.GetName().ToUpper(),
                                    pres.Id);
            }
            str += String.Format("    PRESENCE_COUNT = {0},\n", m_PresenceSchemas.Count);
            str += "};\n";

            str += " \n";

            //Presence id enum
            str += "class PresenceUtil\n";
            str += "{\n";
            str += "public:\n";
            str += "    PresenceUtil() {}\n";

            str += "    \n";
            str += "    static bool GetIsValid(int presenceid) \n";
            str += "    {\n";
            str += "        bool ret = false;\n";
            str += "        switch(presenceid)\n";
            str += "        {\n";
            foreach (Presence pres in m_PresenceSchemas)
            {
                str += String.Format("            case {0}:\n", pres.GetName().ToUpper());
                str += "                ret = true;\n";
            }
            str += "        }\n";
            str += "        return ret;\n";
            str += "    }\n";

            str += "    \n";
            str += "    static const char* GetLabel(int presenceid) \n";
            str += "    {\n";
            str += "        const char* ret = \"UNKNOWN_LABEL\";\n";
            str += "        switch(presenceid)\n";
            str += "        {\n";
            foreach (Presence pres in m_PresenceSchemas)
            {
                str += String.Format("            case {0}:", pres.GetName().ToUpper());
                str += String.Format(" ret = \"{0}\"; break;\n", pres.GetName().ToUpper());
            }
            str += "        }\n";
            str += "        return ret;\n";
            str += "    }\n";

            str += "    \n";
            str += "    static int GetId(int presenceid) \n";
            str += "    {\n";
            str += "        int ret = 0;\n";
            str += "        switch(presenceid)\n";
            str += "        {\n";
            foreach (Presence pres in m_PresenceSchemas)
            {
                str += String.Format("            case {0}:", pres.GetName().ToUpper());
                str += String.Format(" ret = {0}; break;\n", pres.Id);
            }
            str += "        }\n";
            str += "        return ret;\n";
            str += "    }\n";

            str += "    \n";
            str += "    static int GetCountOfFields(int presenceid) \n";
            str += "    {\n";
            str += "        int ret = 0;\n";
            str += "        switch(presenceid)\n";
            str += "        {\n";
            foreach (Presence pres in m_PresenceSchemas)
            {
                str += String.Format("            case {0}:", pres.GetName().ToUpper());
                str += String.Format(" ret = {0}; break;\n", pres.GetFieldCount());
            }
            str += "        }\n";
            str += "        return ret;\n";
            str += "    }\n";


            str += "    \n";
            str += "    static int GetFieldIdFromFieldIndex(int presenceid, int fieldIndex) \n";
            str += "    {\n";
            str += "        int fieldId = -1;\n";
            str += "        switch(presenceid)\n";
            str += "        {\n";
            foreach (Presence pres in m_PresenceSchemas)
            {
                str += String.Format("            case {0}:\n", pres.GetName().ToUpper());
                str += "            {\n";
                int fldCount = pres.GetFieldCount();
                if (fldCount > 0)
                {
                    str += "                switch(fieldIndex)\n";
                    str += "                {\n";
                    for (int i = 0; i < fldCount; ++i)
                    {
                        Property prop = pres.GetField(i);
                        str += String.Format("                  case {0}: fieldId = {1}; break;\n", i, prop.Id);
                    }
                    str += "                  default: break;\n";
                    str += "                }\n";
                }
                str += "            }\n";
                str += "            break;\n";
            }
            str += "        }\n";
            str += "        return fieldId;\n";
            str += "    }\n";

            //////////////////////////////////////

            str += "\n\tstatic const char * GetFieldStrFromFieldId(int fieldId, int fieldIndex)\n";
            str += "\t{\n";
            str += "\t\tswitch(fieldId)\n";
            str += "\t\t{\n";
            foreach (Presence pres in m_PresenceSchemas)
            {
                int fldCount = pres.GetFieldCount();
                for (int i = 0; i < fldCount; ++i)
                {
                    Property prop = pres.GetField(i);
                    str += String.Format("\t\t\tcase {0}: // {1}\n", prop.Id, prop.Name);
                    str += "\t\t\t{\n";
                    if (prop.NumValues() > 0)
                    {
                        str += "\t\t\t\tswitch(fieldIndex)\n";
                        str += "\t\t\t\t{\n";

                        IDictionaryEnumerator e = prop.GetValues();
                        while (e.MoveNext())
                        {
                            Property.PropertyValue v = e.Value as Property.PropertyValue;
                            str += String.Format("\t\t\t\t\tcase {0}: // {1}\n", v.GetValue(), e.Key);
                            str += "\t\t\t\t\t{\n";
                            str += String.Format("\t\t\t\t\t\treturn \"{0}\";\n", e.Key);
                            str += "\t\t\t\t\t}\n";
                        }
                        str += "\t\t\t\t\tdefault: \n";
                        str += "\t\t\t\t\t{\n";
                        str += "\t\t\t\t\t\tgnetWarning(\"Field ID at specified index does not exist\");\n";
                        str += "\t\t\t\t\t\treturn \"\";\n";
                        str += "\t\t\t\t\t}\n";
                        str += "\t\t\t\t}\n";
                    }
                    str += "\t\t\t\tbreak;\n";
                    str += "\t\t\t}\n";
                }
            }
            str += "\t\t}\n";
            str += "\t\tgnetWarning(\"Field ID does not exist\");\n";
            str += "\t\treturn \"\";\n";
            str += "\t}\n";

            ////////////////////////////////

            str += "    \n";
            str += "    static int GetFieldIndexFromFieldId(int presenceid, int fieldId) \n";
            str += "    {\n";
            str += "        int fieldIndex = -1;\n";
            str += "        switch(presenceid)\n";
            str += "        {\n";
            foreach (Presence pres in m_PresenceSchemas)
            {
                str += String.Format("            case {0}:\n", pres.GetName().ToUpper());
                str += "            {\n";

                int fldCount = pres.GetFieldCount();
                
                if (fldCount > 0)
                {
                    str += "                switch(fieldId)\n";
                    str += "                {\n";
                    for (int i = 0; i < fldCount; ++i)
                    {
                        Property prop = pres.GetField(i);
                        str += String.Format("                  case {0}: fieldIndex = {1}; break;\n", prop.Id, i);
                    }
                    str += "                  default: break;\n";
                    str += "                }\n";
                }
                str += "            }\n";
                str += "            break;\n";
            }
            str += "        }\n";
            str += "        return fieldIndex;\n";
            str += "    }\n";

            str += "    \n";
            str += "};\n";

            return str;
        }

        //PURPOSE
        //  Converts the presences to a string representation
        //  of a script enum.
        public virtual string ToEnumString()
        {
            string str = "";

            str += "//PURPOSE\n";
            str += "//   Enum with all presence ids.\n";

            str += "ENUM PRESENCE_ENUM\n";

            foreach (Presence pres in m_PresenceSchemas)
            {
                str += String.Format("    {0} = {1},\n", pres.GetName().ToUpper(), pres.Id);
            }

            str += String.Format("    PRESENCE_COUNT = {0}\n", m_PresenceSchemas.Count);

            //close the enum
            str += "ENDENUM\n\n";

            return str;
        }
    }
}
