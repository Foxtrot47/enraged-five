using System;
using System.Xml;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace xlast2c
{
    //PURPOSE
    //  Collection of XLAST achievements.
    //  Each achievement is internally contained in a Property object.
    class Achievements
    {
        public class Achievement
        {
            public Property m_Prop = null;
            public int m_Index = -1;

            public uint titleStringId;

            public uint descriptionStringId;

            public uint unachievedStringId;

            public bool showUnachieved;

            public uint type;

            public uint cred;

            public uint imageId;
        };

        //Table of achievements
        SortedDictionary<uint, Achievement> m_Achievements;

        public void Reset(XmlDocument doc)
        {
            Trace.WriteLine("Collecting achievements...");

            //Traverse to the "Achievements" section.

            XmlElement proj = doc.DocumentElement["GameConfigProject"];
            XmlElement items = null != proj ? proj["Achievements"] : null;
            XmlElement el = null != items ? (XmlElement) items.FirstChild : null;

            m_Achievements = new SortedDictionary<uint, Achievement>();

            // Achievement ids start at 1 to account for ps3 platinum trophy
            for(int i = 1; null != el; el = (XmlElement) el.NextSibling, ++i)
            {
                Achievement ach = new Achievement();

                ach.m_Prop = new Property();
                ach.m_Prop.Reset(el);
                ach.m_Index = i;

                ach.titleStringId = uint.Parse(el.Attributes.GetNamedItem("titleStringId").Value);
                ach.descriptionStringId = uint.Parse(el.Attributes.GetNamedItem("descriptionStringId").Value);
                ach.unachievedStringId = uint.Parse(el.Attributes.GetNamedItem("unachievedStringId").Value);
                ach.showUnachieved = el.Attributes.GetNamedItem("showUnachieved").Value == "true";

                string strType = el.Attributes.GetNamedItem("achievementType").Value;
                ach.type = 0;
                if(strType == "Checkpoint")
                {
                    ach.type = 1;
                }
                else if(strType == "Completion")
                {
                    ach.type = 2;
                }
                else if (strType == "Event")
                {
                    ach.type = 3;
                }
                else if (strType == "Leveling")
                {
                    ach.type = 4;
                }
                else if (strType == "Other")
                {
                    ach.type = 5;
                }
                else if (strType == "Tournament")
                {
                    ach.type = 6;
                }
                else if (strType == "Unlock")
                {
                    ach.type = 7;
                }
                else
                {
                    string error = "Unknown achievement type: " + strType;
                    Debug.Assert(false, error);
                }

                ach.cred = uint.Parse(el.Attributes.GetNamedItem("cred").Value);
                ach.imageId = uint.Parse(el.Attributes.GetNamedItem("imageId").Value);

                m_Achievements.Add(ach.m_Prop.Id, ach);

                Trace.WriteLine("  " + ach.m_Prop.Name);
            }
        }

        public int GetCount()
        {
            return m_Achievements.Count;
        }

        public IEnumerator GetEnumerator()
        {
            return (null != m_Achievements)
                    ? m_Achievements.Values.GetEnumerator()
                    : (IEnumerator) null;
        }

        //PURPOSE
        //  Converts the achievements to a string representation
        //  of a C enum.
        public virtual string ToCString()
        {
            string str = "";

            str += "class Achievements\n{\n";
            str += "public:\n";


            //Enum for the count
            str += "    enum\n    {\n";

            str += "#if RSG_NP\n";
            str += String.Format("        COUNT = {0}\n", m_Achievements.Count+1);
            str += "#else\n";
            str += String.Format("        COUNT = {0}\n", m_Achievements.Count);
            str += "#endif //RSG_NP\n";

            str += "    };\n";


            //First do the PSN version of the achievements
            str += "    enum\n    {\n";
            str += "#if RSG_NP\n";
            str += "        PLATINUM = 0,\n";
            str += "#endif //RSG_NP\n";

            SortedDictionary<uint, Achievement>.Enumerator entries =
                m_Achievements.GetEnumerator();

            while (entries.MoveNext())
            {
                Property prop = entries.Current.Value.m_Prop;
                string name = prop.Name.Replace(" ", "_").ToUpper();

                str += String.Format("        {0} = {1},\n", name, prop.Id);
            }

            //close the enum
            str += "    };\n\n"; 

            
            //
            //Create functions to map to and from achievement id and index
            //

            //Make the functions if there are achievements
            if(m_Achievements.Count > 0)
            {
                //
                //IdToIndex - converts an achievement id to an index from
                //zero to the number of achievements minus one.  Note that
                //achievement ids are not necessarily sequential.
                //
                str += "    static int IdToIndex(const int achId)\n";
                str += "    {\n";
                str += "        switch(achId)\n";
                str += "        {\n";

                str += "#if RSG_NP \n";
                str += "        case PLATINUM: return 0;\n";
                str += "#endif\n";

                entries = m_Achievements.GetEnumerator();
                while (entries.MoveNext())
                {
                    Achievement ach = entries.Current.Value;
                    string name = ach.m_Prop.Name.Replace(" ", "_").ToUpper();

                    str += String.Format("        case {0}: return {1};\n", name, ach.m_Index);
                }
                str += "        }\n";
                str += "        return -1;\n";
                str += "    }\n";

                str += "\n";

                //
                //IndexToId - converts an achievement index to an id.
                //
                str += "    static int IndexToId(const int index)\n";
                str += "    {\n";
                str += "        switch(index)\n";
                str += "        {\n";

                str += "#if RSG_NP \n";
                str += "        case 0: return PLATINUM;\n";
                str += "#endif\n";

                entries = m_Achievements.GetEnumerator();
                while (entries.MoveNext())
                {
                    Achievement ach = entries.Current.Value;
                    string name = ach.m_Prop.Name.Replace(" ", "_").ToUpper();

                    str += String.Format("        case {0}: return {1};\n", ach.m_Index, name);
                }
                str += "        }\n";
                str += "        return -1;\n";
                str += "    }\n";

                // steam Id to char
                str += " \n";
                str += "#if __STEAM_BUILD\n";
                str += "	static const char* IdToChar(const int achId)\n";
                str += "	{\n";
                str += "		switch(achId)\n";
                str += "		{\n";

                entries = m_Achievements.GetEnumerator();
                while (entries.MoveNext())
                {
                    Achievement ach = entries.Current.Value;
                    string name = ach.m_Prop.Name.Replace(" ", "_").ToUpper();

                    str += String.Format("   \t     case {0}: return \"{1}\";\n", name, name);
                }

                str += "		}\n";
                str += "		return \"\";\n";
                str += "}\n";
                str += "#endif\n";
            }
            else //Make a empty function with no parameters
            {
                str += "    static int IdToIndex(const int /*achId*/)\n";
                str += "    {\n";
                str += "        return -1;\n";
                str += "    }\n\n";
                str += "    static int IndexToId(const int /*achId*/)\n";
                str += "    {\n";
                str += "        return -1;\n";
                str += "    }\n";
                str += "    static const char* IdToChar(const int /*achId*/)\n";
                str += "    {\n";
                str += "        return \"\";\n";
                str += "    }\n";
            }

            //Close the class Achievements
            str += "};\n";

            return str;
        }

		public string StringToHash(string str)
		{
			string output = "0";
			System.Diagnostics.ProcessStartInfo psi = new System.Diagnostics.ProcessStartInfo(@"X:\payne\src\dev\rage\tools\base\exes\cmdhash.exe");
			psi.RedirectStandardOutput = true;
			psi.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
			psi.UseShellExecute = false;
			psi.Arguments = "-name " + str + " -hex";
			System.Diagnostics.Process listFiles;
			listFiles = System.Diagnostics.Process.Start(psi);
			System.IO.StreamReader myOutput = listFiles.StandardOutput;
			listFiles.WaitForExit(2000);
			if (listFiles.HasExited)
			{
				output = "0x" + myOutput.ReadToEnd();
			}

			return output;
		}

		//PURPOSE
		//  Converts the achievements to a string representation
		//  of a C enum.
		public virtual string ToCEnumString()
		{
			string str = "";

			str += "class AchievementHashes\n{\n";
			str += "public:\n";

			//
			//Create functions to map to and from achievement id to hash, and back
			//

			//Make the functions if there are achievements
			if (m_Achievements.Count > 0)
			{
				//
				//IdToIndex - converts an achievement id to an index from
				//zero to the number of achievements minus one.  Note that
				//achievement ids are not necessarily sequential.
				//
				str += "    static int IdToHash(const int achId)\n";
				str += "    {\n";
				str += "        switch(achId)\n";
				str += "        {\n";

                str += "#if RSG_NP \n";
				str += String.Format("        case Achievements::PLATINUM: return {0};\n", StringToHash("PLATINUM"));
				str += "#endif\n";

				SortedDictionary<uint, Achievement>.Enumerator entries = m_Achievements.GetEnumerator();
				while (entries.MoveNext())
				{
					Achievement ach = entries.Current.Value;
					string name = ach.m_Prop.Name.Replace(" ", "_").ToUpper();

					str += String.Format("        case Achievements::{0}: return {1};\n", name, StringToHash(name));
				}
				str += "        }\n";
				str += "        return -1;\n";
				str += "    }\n";

				str += "\n";

				//
				//IndexToId - converts an achievement index to an id.
				//
				str += "    static int HashToId(const int index)\n";
				str += "    {\n";
				str += "        switch(index)\n";
				str += "        {\n";

                str += "#if RSG_NP \n";
				str += String.Format("        case {0}: return Achievements::PLATINUM;\n", StringToHash("PLATINUM"));
				str += "#endif\n";

				entries = m_Achievements.GetEnumerator();
				while (entries.MoveNext())
				{
					Achievement ach = entries.Current.Value;
					string name = ach.m_Prop.Name.Replace(" ", "_").ToUpper();

					str += String.Format("        case {0}: return Achievements::{1};\n", StringToHash(name), name);
				}
				str += "        }\n";
				str += "        return -1;\n";
				str += "    }\n";
			}
			else //Make a empty function with no parameters
			{
				str += "    static int IdToHash(const int /*achId*/)\n";
				str += "    {\n";
				str += "        return -1;\n";
				str += "    }\n\n";
				str += "    static int HashToId(const int /*achId*/)\n";
				str += "    {\n";
				str += "        return -1;\n";
				str += "    }\n";
			}

			//Close the class Achievements
			str += "};\n";

			return str;
		}

        //PURPOSE
        //  Converts the achievements to a string representation
        //  of a script enum.
        public virtual string ToEnumString()
        {
            string str = ""
                +"    \n"
                + "USING \"commands_misc.sch\""
                + "    \n"
                + "    \n";
            str += "//PURPOSE\n";
            str += "//   Enum with all achievements ids.\n"; 
            str += "ENUM ACHIEVEMENT_ENUM\n";
            str += "// REMEMBER THAT IN PS3 THERE IS ONE EXTRA ACHIEVEMENT\n";
            str += "// #IF RSG_NP\n";
            str += "//        PLATINUM = 0,\n";
            str += "// #ENDIF //RSG_NP\n";

            SortedDictionary<uint, Achievement>.Enumerator entries = m_Achievements.GetEnumerator();

            while (entries.MoveNext())
            {
                Property prop = entries.Current.Value.m_Prop;
                string name = prop.Name.Replace(" ", "_").ToUpper();

                str += String.Format("        {0} = {1},\n", name, prop.Id);
            }

            str += String.Format("    ACHIEVEMENT_COUNT = {0}\n", m_Achievements.Count+1);

            //close the enum
            str += "ENDENUM\n\n";

            // Write out the GET_ACHIEVEMENT_ENUM_BY_NAME script function
            str += "\t//PURPOSE\n"
                + "\t//   Returns the achievement enum given the name.\n";

            str += "\tFUNC ACHIEVEMENT_ENUM GET_ACHIEVEMENT_ENUM_BY_NAME_HASH(INT nameHash)\n";

            
            entries = m_Achievements.GetEnumerator();
            int counter = 0;
            while (entries.MoveNext())
            {
                Property prop = entries.Current.Value.m_Prop;
                string name = prop.Name.Replace(" ", "_");
                string EnumName = prop.Name.Replace(" ", "_").ToUpper();
                

                string IfSTring = "ELIF";
                if (counter == 0)
                {
                    IfSTring = "IF";
                }
                counter++;

                str += String.Format("\t\t{2} GET_HASH_KEY(\"{0}\") = nameHash\n\t\t\tRETURN {1}\n", name, EnumName, IfSTring);
            }

            str = str
                + "\t\tENDIF\n"
                + "\n"
                + "\t\tRETURN ACHIEVEMENT_COUNT\n";

            str += "\tENDFUNC\n"
            + "\n";

            // Hash the input string
            str += "\t//PURPOSE\n"
                + "\t//   Hash the given string for getting the achievement.\n";

            str += "\tFUNC ACHIEVEMENT_ENUM GET_ACHIEVEMENT_ENUM_BY_NAME_STRING(STRING lbName)\n";

            str += "\n"
                + "\t\tINT nameHash = GET_HASH_KEY(lbName)\n";
            str += "\t\tRETURN GET_ACHIEVEMENT_ENUM_BY_NAME_HASH(nameHash)\n";
            str += "\tENDFUNC\n";

            return str;
        }

        //PURPOSE
        //  Converts the achievements to a string representation
        //  of a C enum.
        public virtual string ToRageScriptString()
        {
            string str = "";

            str += "ENUM ACHIEVEMENTS\n";

            str += String.Format("    ACHIEVEMENT_NUM_ACHIEVEMENTS = {0},\n", m_Achievements.Count);

            SortedDictionary<uint, Achievement>.Enumerator entries=
                m_Achievements.GetEnumerator();

            for(int i = 0; i < m_Achievements.Count; ++i)
            {
                entries.MoveNext();

                Property prop = entries.Current.Value.m_Prop;

                string name = prop.Name.Replace(" ", "_").ToUpper();

                str += String.Format("    ACHIEVEMENT_{0} = {1}", name, prop.Id);
                if (i < m_Achievements.Count - 1)
                {
                    str += ",\n";
                }
                else
                {
                    str += "\n";
                }
            }

            str += "ENDENUM\n";

            return str;
        }
    }
}
