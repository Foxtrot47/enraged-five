using System;
using System.Xml;
using System.Collections;
using System.Diagnostics;

namespace xlast2c
{
    //PURPOSE
    //  Contains the properties that define a matching filter schema.
    class MatchingFilter : Schema
    {
        //Name of the filter
        string m_Name;

        uint m_FilterId;

        class Condition
        {
            public Property m_LeftOp;
            public Property m_RightOp;
            public string m_Op;
        };

        Condition[] m_Conditions;
        int m_NumConditions = 0;

        //PURPOSE
        //  Returns the C++ class name.
        public override string GetName()
        {
            return "MatchingFilter_" + m_Name.Replace(" ", "");
        }

        //PURPOSE
        //  Extracts the properties that define a matching filter.
        public void Reset(XmlElement query,
                        PropertyCollection props,
                        PropertyCollection constants)
        {
            m_Name = query.Attributes.GetNamedItem("friendlyName").Value;

            m_FilterId = Property.ParseUInt(query.Attributes.GetNamedItem("id").Value);

            XmlElement condition = (XmlElement) query["Filters"].FirstChild;

            m_NumConditions = 0;

            for(XmlElement c = condition; null != c; c = (XmlElement) c.NextSibling)
            {
                ++m_NumConditions;
            }

            //Add 1 to accommodate game mode.
            m_NumConditions += 1;

            m_Conditions = new Condition[m_NumConditions];

            m_NumConditions = 0;

            for(; null != condition; condition = (XmlElement) condition.NextSibling)
            {
                string leftStr = condition.Attributes.GetNamedItem("left").Value;
                string rightStr = condition.Attributes.GetNamedItem("right").Value;

                Property leftProp = props[leftStr];
                Property rightProp = props[rightStr];

                if(null == rightProp)
                {
                    rightProp = constants[rightStr];
                }

                if(null == leftProp)
                {
                    Debug.Fail("Invalid condition operand:" + leftStr + " in matching filter.",
                                "Check that GAME_TYPE and/or constants are not included in your filter conditions");
                }

                if(null == rightProp)
                {
                    Debug.Fail("Invalid condition operand:" + rightStr + " in matching filter.",
                                "Check that GAME_TYPE and/or constants are not included in your filter conditions");
                }

                if(null == leftProp || null == rightProp)
                {
                    continue;
                }

                if(DataTypes.XUSER_DATA_TYPE_INT32 != leftProp.Type)
                {
                    Debug.Fail(string.Format("Unhandled data type:{0} for:{0}",
                                            leftProp.TypeStr,
                                            leftProp.Name));
                }

                if(DataTypes.XUSER_DATA_TYPE_INT32 != rightProp.Type)
                {
                    Debug.Fail(string.Format("Unhandled data type:{0} for:{0}",
                                            rightProp.TypeStr,
                                            rightProp.Name));
                }

                m_Conditions[m_NumConditions] = new Condition();
                m_Conditions[m_NumConditions].m_LeftOp = leftProp;
                m_Conditions[m_NumConditions].m_RightOp = rightProp;

                m_Conditions[m_NumConditions].m_Op =
                    condition.Attributes.GetNamedItem("op").Value;

                ++m_NumConditions;

                this.AddMember(props[rightStr]);
            }

            Trace.WriteLine("  " + m_Name);
        }

        public override string GetEnum()
        {
            string str = base.GetEnum();
            if(!string.IsNullOrEmpty(str))
            {
                str = str.TrimEnd("\n".ToCharArray()) + "\n";
            }

            str += String.Format("    enum\n    {{\n");
            str += String.Format("        FILTER_ID = {0},\n", m_FilterId);
            str += String.Format("        CONDITION_COUNT = {0},\n", m_NumConditions);
            str += "    };";

            return str;
        }

        public override string GetAccessors()
        {
            string str = base.GetAccessors();

            str += "    typedef MatchingAttributes MatchingAttributesType;\n";

            //GetAttributeIdForCondition()
            str += "    //Returns the id of the session attribute that will be filtered\n";
            str += "    //against by the operation at the given condition index.\n";
            str += "    //This represents the id of the attribute advertised by the\n";
            str += "    //session host and corresponds to the left hand side of the filter\n";
            str += "    //operation.\n";
            str += "    static FieldId GetAttributeIdForCondition(const unsigned conditionIndex)\n";
            str += "    {\n";
            str += "        switch(conditionIndex)\n";
            str += "        {\n";
            for(int i = 0; i < m_NumConditions; ++i)
            {
                str += String.Format("        case {0}: ", i);
                str += String.Format("return {0};\n", m_Conditions[i].m_LeftOp.GetFieldIdName());
            }
            str += "        }\n";
            str += "        return FieldId(-1);\n";
            str += "    }\n";

            //GetAttributeIndexForCondition()
            str += "    //Returns the index of the session attribute that will be filtered\n";
            str += "    //against by the operation at the given condition index.\n";
            str += "    //This represents the index of the attribute advertised by the\n";
            str += "    //session host and corresponds to the left hand side of the filter\n";
            str += "    //operation.\n";
            str += "    static int GetAttributeIndexForCondition(const unsigned conditionIndex)\n";
            str += "    {\n";
            str += "        switch(conditionIndex)\n";
            str += "        {\n";
            for(int i = 0; i < m_NumConditions; ++i)
            {
                str += String.Format("        case {0}: ", i);
                str += String.Format("return MatchingAttributes::GetFieldIndexFromFieldId({0});\n", m_Conditions[i].m_LeftOp.GetFieldIdName());
            }
            str += "        }\n";
            str += "        return -1;\n";
            str += "    }\n";

            //GetParamIdForCondition()
            str += "    //Returns the id of the filter parameter for the given condition.\n";
            str += "    //This represents the right hand side of the filter operation.\n";
            str += "    static FieldId GetParamIdForCondition(const unsigned conditionIndex)\n";
            str += "    {\n";
            str += "        switch(conditionIndex)\n";
            str += "        {\n";
            for(int i = 0; i < m_NumConditions; ++i)
            {
                str += String.Format("        case {0}: ", i);
                str += String.Format("return {0};\n", m_Conditions[i].m_RightOp.GetFieldIdName());
            }
            str += "        }\n";
            str += "        return FieldId(-1);\n";
            str += "    }\n";

            //GetParamIndexForCondition()
            str += "    //Returns the index of the filter parameter for the given condition.\n";
            str += "    //This represents the right hand side of the filter operation.\n";
            str += "    static int GetParamIndexForCondition(const unsigned conditionIndex)\n";
            str += "    {\n";
            str += "        switch(conditionIndex)\n";
            str += "        {\n";
            for(int i = 0; i < m_NumConditions; ++i)
            {
                str += String.Format("        case {0}: ", i);
                str += String.Format("return GetFieldIndexFromFieldId({0});\n", m_Conditions[i].m_RightOp.GetFieldIdName());
            }
            str += "        }\n";
            str += "        return -1;\n";
            str += "    }\n";

            //GetOperator()
            str += "    static unsigned GetOperator(const unsigned conditionIndex)\n";
            str += "    {\n";
            str += "        switch(conditionIndex)\n";
            str += "        {\n";
            for(int i = 0; i < m_NumConditions; ++i)
            {
                str += String.Format("        case {0}: ", i);
                str += String.Format("return \'{0}\';\n", m_Conditions[i].m_Op);
            }
            str += "        }\n";
            str += "        return 0;\n";
            str += "    }\n";

            //GetFilterName()
            str += "    static const char* GetFilterName()\n";
            str += "    {\n";
            str += "        return \"" + m_Name.Replace(" ", "") + "\";\n";
            str += "    }\n";

            return str;
        }
    }

    //PURPOSE
    //  A collection of match query schemas.
    class MatchingFilterCollection
    {
        //List of match queries.
        ArrayList m_Filters;

        //PURPOSE
        //  Collects match queries from the "Queries" section of an xlast
        //  document.
        public void Reset(XmlDocument doc, PropertyCollection props)
        {
            XmlElement proj = doc.DocumentElement["GameConfigProject"];
            XmlElement matchmaking = null != proj ? proj["Matchmaking"] : null;
            XmlElement queries = null != matchmaking ? matchmaking["Queries"] : null;
            XmlElement constants = null != matchmaking ? matchmaking["Constants"] : null;
            XmlElement qel = null != queries ? (XmlElement) queries.FirstChild : null;

            //Get the constants
            PropertyCollection consts = new PropertyCollection(xlast2c.eLeaderboardtypes.LEADERBOARD_TYPE_PLAYER);

            //For now don't support constants.  There's almost always
            //another way to achieve the same matchmaking behavior without
            //using constants.  If they ever become absolutely necessary
            //then we can add them.
            /*Trace.WriteLine("Collecting matchmaking constants...");
            XmlElement cel = null != constants ? (XmlElement) constants.FirstChild : null;
            for(; null != cel; cel = (XmlElement) cel.NextSibling)
            {
                Property prop = new Property();
                prop.Reset(cel);
                consts.AddProperty(prop);
            }*/

            //Build the queries

            Trace.WriteLine("Collecting matchmaking queries...");

            m_Filters = new ArrayList();

            for(; null != qel; qel = (XmlElement) qel.NextSibling)
            {
                MatchingFilter filter = new MatchingFilter();

                filter.Reset(qel, props, consts);

                m_Filters.Add(filter);
            }
        }

        //PURPOSE
        //  Generates (in string form) the C++ class representation
        //  of all match queries.
        public virtual string ToCString()
        {
            string str = "";

            for(int i = 0; i < m_Filters.Count; ++i)
            {
                str = str + ((MatchingFilter) m_Filters[i]).ToCString();

                if(i < m_Filters.Count - 1)
                {
                    str += "\n\n";
                }
            }

            return str;
        }

        public virtual string ToRageScriptString()
        {
            string str = "";

            for(int i = 0; i < m_Filters.Count; ++i)
            {
                str = str + ((MatchingFilter) m_Filters[i]).ToRageScriptString();

                if(i < m_Filters.Count - 1)
                {
                    str += "\n\n";
                }
            }

            return str;
        }
    }
}
