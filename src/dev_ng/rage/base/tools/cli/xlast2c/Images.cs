using System;
using System.Xml;
using System.Collections;
using System.Diagnostics;

namespace xlast2c
{
    //PURPOSE
    //  Collection of XLAST images
    //  Each images is internally contained in a Property object.
    class Images
    {
        public class Image
        {
            public uint id;
            public string name;
            public string Path;
        }

        //Table of id/image pairs
        Hashtable m_htImages;

        public void Reset(XmlDocument doc)
        {
            Trace.WriteLine("Collecting Images...");

            //Traverse to the "GamerPictures" section.

            XmlElement proj = doc.DocumentElement["GameConfigProject"];
            XmlElement items = null != proj ? proj["Images"] : null;
            XmlElement el = null != items ? (XmlElement)items.FirstChild : null;

            m_htImages = new Hashtable();

            for (; null != el; el = (XmlElement)el.NextSibling)
            {
                Image image = new Image();
                image.id = uint.Parse(el.Attributes.GetNamedItem("id").Value);
                image.name = el.Attributes.GetNamedItem("friendlyName").Value;
                image.Path = el.FirstChild.InnerText;

                m_htImages.Add(image.id, image);

                Trace.WriteLine("  " + image.Path);
            }
        }

        //PURPOSE
        //  Returns Image by name.
        public Image this[string name]
        {
            get
            {
                IDictionaryEnumerator entries = m_htImages.GetEnumerator();
                entries.Reset();
                while (entries.MoveNext())
                {
                    Image image = (Image)entries.Value;
                    if (image.name == name)
                    {
                        return image;
                    }
                }

                return null;
            }
        }

        //PURPOSE
        //  Indexes Images by integer id.
        public Image this[uint id]
        {
            get
            {
                return (Image)m_htImages[id];
            }
        }
    }
}
