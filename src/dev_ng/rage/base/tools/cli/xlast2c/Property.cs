using System;
using System.Xml;
using System.Globalization;
using System.Collections;
using System.Diagnostics;

namespace xlast2c
{
    //PURPOSE
    //  Container for XLAST property data.
    class Property
    {
        public class PropertyValue
        {
            uint m_Value;
            uint m_StringId;

            public PropertyValue(uint value, uint stringId)
            {
                m_Value = value;
                m_StringId = stringId;
            }

            public uint GetValue() { return m_Value; }
            public uint GetStringId() { return m_StringId; }
        }

        //Property name
        string m_Name;
        //Property id
        uint m_Id;
        //Type code of property
        uint m_Type;
        //C type of property (u32, float, etc.)
        string m_TypeStr;

        //Contains key/value pairs for the possible values of this property.
        Hashtable m_Values;

        //If the property is a constant this member contains its value.
        object m_ConstantValue;

        //True if the property value is a constant.
        bool m_IsConstant = false;

        bool m_bIsUsedInLB = false;

        public static bool Filter(Property prop)
        {
            return null != prop
                    && "GAME_TYPE" != prop.Name
                    && "GAMERNAME" != prop.Name
                    && "GAMERZONE" != prop.Name
                    && "GAMERCOUNTRY" != prop.Name
                    && "LANGUAGE" != prop.Name
                    && "GAMERRATING" != prop.Name
                    && "GAMERMU" != prop.Name
                    && "GAMERSIGMA" != prop.Name
                    && "GAMERPUID" != prop.Name
                    && "AFFILIATEVALUE" != prop.Name
                    && "GAMERHOSTNAME" != prop.Name
                    && "PLATFORMTYPE" != prop.Name
                    && "PLATFORMLOCK" != prop.Name;
        }

        //PURPOSE
        //  Parses a string into an int.
        //  This is here because the standard C# string parsing
        //  functions can't parse strings that could be either
        //  hex or decimal, and they don't parse the "0x" in hex
        //  strings.
        public static uint ParseUInt(string strId)
        {
            uint id;

            if(strId.Length > 1 &&
                '0' == strId[0] &&
                ('x' == strId[1] || 'X' == strId[1]))
            {
                id = uint.Parse(strId.Substring(2), NumberStyles.HexNumber);
            }
            else
            {
                id = uint.Parse(strId);
            }

            return id;
        }

        //PURPOSE
        //  Extracts the name and id from an property element
        //  as well as any enumeration of values for the property.
        public virtual void Reset(XmlElement propEl)
        {
            XmlAttribute attr =
                (XmlAttribute) propEl.Attributes.GetNamedItem("id");

            uint id = Property.ParseUInt(attr.Value);

            if("Constant" == propEl.Name)
            {
                string name = propEl.Attributes.GetNamedItem("name").Value;

                this.Reset(name, id);

                //The value
                uint val = Property.ParseUInt(propEl.Attributes.GetNamedItem("value").Value);

                m_ConstantValue = val;
                m_IsConstant = true;
            }
            else
            {
                string name = propEl.Attributes.GetNamedItem("friendlyName").Value;

                this.Reset(name, id);

                //Collect allowable values for this property.

                XmlNodeList valNodes = propEl.GetElementsByTagName("ContextValue");

                //Special case for the GameModes context
                if(0 == valNodes.Count)
                {
                    valNodes = propEl.GetElementsByTagName("GameMode");
                }

                IEnumerator it = valNodes.GetEnumerator();

                while(it.MoveNext())
                {
                    XmlElement valueEl = (XmlElement)it.Current;

                    //The key
                    string valName =
                        valueEl.Attributes.GetNamedItem("friendlyName").Value;

                    //The value
                    uint val = Property.ParseUInt(valueEl.Attributes.GetNamedItem("value").Value);

                    //The string id
                    uint stringId = Property.ParseUInt(valueEl.Attributes.GetNamedItem("stringId").Value);

                    this.AddValue(valName, val, stringId);

                    Trace.WriteLine("  " + valName);
                }
            }

            Trace.WriteLine("  " + m_Name);
        }

        public virtual void Reset(Property prop)
        {
            m_Name = prop.m_Name;
            m_Id = prop.m_Id;
            m_Type = prop.m_Type;
            m_TypeStr = prop.m_TypeStr;
            m_bIsUsedInLB = prop.m_bIsUsedInLB;
        }

        public virtual void Reset(Property prop, string name)
        {
            m_Name = name;
            m_Id = prop.m_Id;
            m_Type = prop.m_Type;
            m_TypeStr = prop.m_TypeStr;
            m_bIsUsedInLB = prop.m_bIsUsedInLB;
        }

        public virtual void Reset(string name, uint id)
        {
            m_Name = name;
            m_Id = id;
            m_Type = DataTypes.GetTypeFromId(id);
            if(DataTypes.XUSER_DATA_TYPE_CONTEXT == m_Type)
            {
                m_Type = DataTypes.XUSER_DATA_TYPE_INT32;
            }
            m_TypeStr = DataTypes.GetTypeStrFromType(m_Type);
        }

        public bool HasValues()
        {
            return (null != m_Values);
        }

        public void AddValue(string name, uint val, uint stringId)
        {
            if(null == m_Values)
            {
                m_Values = new Hashtable();
            }

            m_Values.Add(name, new PropertyValue(val, stringId));
        }

        public int NumValues()
        {
            return m_Values.Count;
        }

        //PURPOSE
        //  Returns an enumerator that can be used to enumerate the key/value
        //  pairs.
        public IDictionaryEnumerator GetValues()
        {
            return (null != m_Values) ? m_Values.GetEnumerator() : null;
        }

        public string Name
        {
            get
            {
                return m_Name;
            }
        }

        public uint Id
        {
            get
            {
                return m_Id;
            }
        }

        public uint Type
        {
            get
            {
                return m_Type;
            }
        }

        public string TypeStr
        {
            get
            {
                return m_TypeStr;
            }
        }

        public bool IsConstant
        {
            get
            {
                return m_IsConstant;
            }
        }

        public bool IsUsedInLeaderboard
        {
            get
            {
                return m_bIsUsedInLB;
            }

            set
            {
                m_bIsUsedInLB = value;
            }
        }

        public object ConstantValue
        {
            get
            {
                return m_ConstantValue;
            }
        }

        public virtual string GetFieldName()
        {
            return this.Name.Replace(" ", "_").ToUpper();
        }

        public virtual string GetFieldIdName()
        {
            return "FIELD_ID_" + this.GetFieldName();
        }

        public virtual string GetColumnIdName()
        {
            return "COLUMN_ID_" + this.GetFieldName();
        }
    }

    //PURPOSE
    //  Collection of properties.
    class PropertyCollection
    {
        public class ContextValue
        {
            public ContextValue(string pName, uint pValue)
            {
                sName = pName;
                uValue = pValue;
            }

            public string GetName() { return sName; }
            public uint GetValue() { return uValue; }

            string sName;
            uint uValue;
        }

        public class Context
        {
            public void AddContextValue(ContextValue pNewValue)
            {
                m_ContextValues.Add(pNewValue);
            }

            public string Name
            {
                get { return sName; }
                set { sName = value;  }
            }

            public ArrayList ContextValues
            {
                get { return m_ContextValues;  }
            }

            string sName;
            ArrayList m_ContextValues = new ArrayList();
        }

        //Table of name/property pairs
        Hashtable m_PropsById = new Hashtable();
        Hashtable m_PropsByName = new Hashtable();
        ArrayList m_Contexts = new ArrayList();

        //This collection is for what type of leaderboards
        eLeaderboardtypes m_LeaderboardType;

        public PropertyCollection(eLeaderboardtypes leaderboardType)
        {
            m_LeaderboardType = leaderboardType;
        }

        public void AddProperty(Property prop)
        {
            Debug.Assert(Property.Filter(prop));

            m_PropsById.Add(prop.Id, prop);
            m_PropsByName.Add(prop.Name, prop);
        }

        //PURPOSE
        //  Extracts properties from the "Properties" section of the xml file.
        public void Reset(XmlDocument doc)
        {
            Trace.WriteLine("Collecting properties...");

            //Traverse to the "Properties" section.

            XmlElement proj = doc.DocumentElement["GameConfigProject"];
            XmlElement items = null != proj ? proj["Properties"] : null;
            XmlElement el = null != items ? (XmlElement) items.FirstChild : null;

            Property prop;

            m_PropsById = new Hashtable();
            m_PropsByName = new Hashtable();
            m_Contexts = new ArrayList();

            //Iterate over the elements in "Properties" and create
            //a Property object for each.
            for(; null != el; el = (XmlElement) el.NextSibling)
            {
                prop = new Property();

                prop.Reset(el);

                if(Property.Filter(prop))
                {
                    this.AddProperty(prop);
                }
            }

            Trace.WriteLine("Collecting contexts...");

            //Traverse to the "Contexts" section.

            items = proj["Contexts"];
            el = (XmlElement) items.FirstChild;

            //Iterate over the elements in "Contexts" and create
            //a property object for each.
            for(; null != el; el = (XmlElement) el.NextSibling)
            {
                Context cContext = new Context();
                prop = new Property();

                prop.Reset(el);

                if(Property.Filter(prop))
                {
                    this.AddProperty(prop);

                    if (prop.HasValues())
                    {
                        IDictionaryEnumerator iEnumerator = prop.GetValues();

                        while (iEnumerator.MoveNext())
                        {
                            Property.PropertyValue pContextValue = (Property.PropertyValue)iEnumerator.Value;

                            cContext.Name = prop.Name;
                            cContext.AddContextValue(new ContextValue((string)iEnumerator.Key, pContextValue.GetValue()));
                        }
                    }

                    m_Contexts.Add(cContext);
                }
            }

            //Because "GameModes" is on the same level in the xml hierarchy
            //as "Contexts" we need this hack to include GameModes in the
            //collection of contexts.
            el = (XmlElement) proj["GameModes"];

            el = (XmlElement) el.Clone();

            //The "GameModes" element doesn't contain id and friendlyName
            //attributes.  Because the Property object expects these we
            //artificially add them.
            el.SetAttribute("id", String.Format("0x{0:x8}", DataTypes.X_CONTEXT_GAME_MODE_ID));
            el.SetAttribute("friendlyName", "GAME_MODE");

            prop = new Property();

            prop.Reset(el);

            this.AddProperty(prop);
        }

        public Property FindByName(string name)
        {
            return (Property) m_PropsByName[name];
        }

        //PURPOSE
        //  Indexes properties by integer id.
        public Property this[uint id]
        {
            get
            {
                return (Property) m_PropsById[id];
            }
        }

        //PURPOSE
        //  Indexes properties by string id.
        //  The string is converted to integer.
        public Property this[string strId]
        {
            get
            {
                uint id = Property.ParseUInt(strId);

                return (Property) m_PropsById[id];
            }
        }

        string GetFieldNameAccessor()
        {
            string str = "";

            IDictionaryEnumerator props = m_PropsById.GetEnumerator();

            str += "    static const char* GetFieldNameFromFieldId(const FieldId fieldId)\n";
            str += "    {\n";
            str += "        switch((int)fieldId)\n";
            str += "        {\n";

            while(props.MoveNext())
            {
                Property prop = (Property) props.Value;
                string fldIdName = prop.GetFieldIdName();

                str += String.Format("        case {0}: ", fldIdName);
                str += String.Format("return \"{0}\";\n", fldIdName);
            }

            str += "        }\n";

            str += "        return \"*** UNKNOWN FIELD ***\";\n";
            str += "    }";

            return str;
        }

        string GetFieldTypeAccessor()
        {
            string str = "";

            IDictionaryEnumerator props = m_PropsById.GetEnumerator();

            str += "    static FieldType GetFieldTypeFromFieldId(const FieldId fieldId)\n";
            str += "    {\n";
            str += "        switch((int)fieldId)\n";
            str += "        {\n";

            while(props.MoveNext())
            {
                Property prop = (Property) props.Value;

                string fldIdName = prop.GetFieldIdName();
                string fldType = DataTypes.GetFieldTypeStrFromType(prop.Type);

                str += String.Format("        case {0}: ", fldIdName);
                str += String.Format("return {0};\n", fldType);
            }

            str += "        }\n";

            str += String.Format("        return {0};\n",
                                DataTypes.GetFieldTypeStrFromType(DataTypes.XUSER_DATA_TYPE_INVALID));
            str += "    }";

            return str;
        }

        string GetScriptFieldTypeAccessor()
        {
            string str = "";

            IDictionaryEnumerator props = m_PropsById.GetEnumerator();

            str += "    \n";
            str += "    \n";
            str += "    //PURPOSE\n";
            str += "    //   This returns the field type given a field id.\n";

            bool clanLeaderboard = (m_LeaderboardType == eLeaderboardtypes.LEADERBOARD_TYPE_CLAN);

            if (clanLeaderboard)
                str += "    FUNC CLAN_LBCOLUMN_TYPES GetClanFieldTypeFromFieldId(CLAN_LBCOLUMNIDS fieldId)\n";
            else
                str += "    FUNC LBCOLUMN_TYPES GetFieldTypeFromFieldId(LBCOLUMNIDS fieldId)\n";

            str += "        SWITCH fieldId\n";

            while (props.MoveNext())
            {
                Property prop = (Property)props.Value;
                if (prop.IsUsedInLeaderboard)
                {
                    string fldIdName = prop.GetColumnIdName();
                    string fldType = DataTypes.GetColumnTypeStrFromType(prop.Type, m_LeaderboardType);

                    if (clanLeaderboard)
                        str += String.Format("                case CLAN_LB{0} ", fldIdName);
                    else
                        str += String.Format("                case LB{0} ", fldIdName);

                    str += String.Format("RETURN {0}\n", fldType);
                } 
            }

            str += String.Format("                DEFAULT ");

            str += String.Format("BREAK\n");

            str += "        ENDSWITCH\n";

            str += "        RETURN ";
            if (clanLeaderboard) str += "CLAN_";
            str += "LBCOLUMNTYPE_INVALID\n";

            str += "    ENDFUNC";
            str += "    \n";

            return str;
        }

        public virtual string FieldSetEnumToCString()
        {
            string str = "";
            IDictionaryEnumerator props = m_PropsById.GetEnumerator();
            str += "    //   Enum with all field types used in leaderboards \n";
            str += "\tenum LeaderboardFieldSet\n\t{\n";

            int count = 0;
            props.Reset();
            while (props.MoveNext())
            {
                Property prop = (Property)props.Value;
                if (prop.IsUsedInLeaderboard)
                {
                    string fldIdName = prop.GetColumnIdName();
                    str += String.Format("\t\t{0}, \t\t\t//{1}\n", fldIdName, DataTypes.GetColumnTypeStrFromType(prop.Type, m_LeaderboardType));
                    count++;
                }
            }

            str += String.Format("\n\t\tLEADERBOARD_FIELDSET_COUNT = {0}\n", count);
            str += "\t};\n";

            return str;
        }

        public virtual string ToCString()
        {
            string str = "";

            str = String.Format("class SchemaBase\n{{\n");

            str += "public:\n";

            str += String.Format("    enum FieldType\n    {{\n");
            str += String.Format("        FIELDTYPE_INVALID = {0},\n", unchecked((int) DataTypes.XUSER_DATA_TYPE_INVALID));
            str += String.Format("        FIELDTYPE_INT32 = {0},\n", unchecked((int) DataTypes.XUSER_DATA_TYPE_INT32));
            str += String.Format("        FIELDTYPE_INT64 = {0},\n", unchecked((int) DataTypes.XUSER_DATA_TYPE_INT64));
            str += String.Format("        FIELDTYPE_FLOAT = {0},\n", unchecked((int) DataTypes.XUSER_DATA_TYPE_FLOAT));
            str += String.Format("        FIELDTYPE_DOUBLE = {0},\n", unchecked((int) DataTypes.XUSER_DATA_TYPE_DOUBLE));
            str += "    };\n";

            IDictionaryEnumerator props = m_PropsById.GetEnumerator();

            str += String.Format("    enum FieldId\n    {{\n");

            while(props.MoveNext())
            {
                Property prop = (Property) props.Value;
                string fldIdName = prop.GetFieldIdName();

                str += String.Format("        {0} = 0x{1:x8},\n",
                                    fldIdName,
                                    prop.Id);
            }

            str += "    };\n";

            foreach (Context iCurrContext in m_Contexts)
            {
                string sEnumName = iCurrContext.Name.Replace(" ", "_");

                str += String.Format("    enum {0}\n    {{\n", sEnumName);

                foreach (ContextValue iCurrValue in iCurrContext.ContextValues)
                {
                    string sEnumValueName = iCurrValue.GetName().Replace(" ", "_");

                    str += String.Format("        {0} = {1},\n", sEnumValueName, iCurrValue.GetValue());
                }

                str += "    };\n";
            }

            //string fldNameAccessor = this.GetFieldNameAccessor();
            //if(!string.IsNullOrEmpty(fldNameAccessor))
            //{
            //    str += fldNameAccessor.TrimEnd("\n".ToCharArray()) + "\n";
            //}

            string fldTypeAccessor = this.GetFieldTypeAccessor();
            if(!string.IsNullOrEmpty(fldTypeAccessor))
            {
                str += fldTypeAccessor.TrimEnd("\n".ToCharArray()) + "\n";
            }

            str += "};\n";

            /*while(props.MoveNext())
            {
                Property prop = (Property) props.Value;

                IDictionaryEnumerator values = prop.GetValues();

                if(null != values)
                {
                    str += String.Format("class Context_{0}\n", prop.Name.Replace(" ", "_"));
                    str += "{\n";

                    str += "public:\n";
                    str += "    static int GetId(const char* label)\n";
                    str += "    {\n";

                    while(values.MoveNext())
                    {
                        str += String.Format("        if(0 == ::strcmp(label, \"{0}\")) return {1};\n", (string) values.Key, (uint) values.Value);
                    }

                    str += "        return -1;\n";
                    str += "    }\n";
                    str += "};\n";
                }
            }*/

            return str;
        }

        public string ToPresenceEnumString()
        {
            string str = "";

            str += "    \n";
            str += "//PURPOSE\n";
            str += "//   Enums of context values\n";

            foreach (Context iCurrContext in m_Contexts)
            {
                string sEnumName = iCurrContext.Name.Replace(" ", "_").ToUpper();

                str += String.Format("ENUM CONTEX_{0}\n", sEnumName);

                foreach (ContextValue iCurrValue in iCurrContext.ContextValues)
                {
                    string sEnumValueName = iCurrValue.GetName().Replace(" ", "_").ToUpper();

                    str += String.Format("    CONTEX_{0} = {1},\n", sEnumValueName, iCurrValue.GetValue());
                }

                str += String.Format("    CONTEX_{0}_COUNT = {1}\n", sEnumName, iCurrContext.ContextValues.Count);

                str += "ENDENUM\n";
                str += "    \n";
            }

            return str;
        }

        public virtual string ToEnumString()
        {
            string str = "";

            str += "    \n";
            str += "    \n";
            str += "    //PURPOSE\n";

            bool clanLeaderboard = (m_LeaderboardType == xlast2c.eLeaderboardtypes.LEADERBOARD_TYPE_CLAN);

            if (clanLeaderboard)
            {
                str += "    //   Enum with all leaderbaord column types.\n";
                str += String.Format("    ENUM CLAN_LBCOLUMN_TYPES\n");
                str += String.Format("        CLAN_LBCOLUMNTYPE_INVALID = {0},\n", unchecked((int)DataTypes.XUSER_DATA_TYPE_INVALID));
                str += String.Format("        CLAN_LBCOLUMNTYPE_INT32 = {0},\n", unchecked((int)DataTypes.XUSER_DATA_TYPE_INT32));
                str += String.Format("        CLAN_LBCOLUMNTYPE_INT64 = {0},\n", unchecked((int)DataTypes.XUSER_DATA_TYPE_INT64));
                str += String.Format("        CLAN_LBCOLUMNTYPE_FLOAT = {0},\n", unchecked((int)DataTypes.XUSER_DATA_TYPE_FLOAT));
                str += String.Format("        CLAN_LBCOLUMNTYPE_DOUBLE = {0}\n", unchecked((int)DataTypes.XUSER_DATA_TYPE_DOUBLE));
                str += "    ENDENUM\n";
            }
            else
            {
                str += "    //   Enum with all leaderbaord column types.\n";
                str += String.Format("    ENUM LBCOLUMN_TYPES\n");
                str += String.Format("        LBCOLUMNTYPE_INVALID = {0},\n", unchecked((int)DataTypes.XUSER_DATA_TYPE_INVALID));
                str += String.Format("        LBCOLUMNTYPE_INT32 = {0},\n", unchecked((int)DataTypes.XUSER_DATA_TYPE_INT32));
                str += String.Format("        LBCOLUMNTYPE_INT64 = {0},\n", unchecked((int)DataTypes.XUSER_DATA_TYPE_INT64));
                str += String.Format("        LBCOLUMNTYPE_FLOAT = {0},\n", unchecked((int)DataTypes.XUSER_DATA_TYPE_FLOAT));
                str += String.Format("        LBCOLUMNTYPE_DOUBLE = {0}\n", unchecked((int)DataTypes.XUSER_DATA_TYPE_DOUBLE));
                str += "    ENDENUM\n";
            }

            IDictionaryEnumerator props = m_PropsById.GetEnumerator();
            int count = 0;
            //
            //  Enum with each property used in leaderboards.  Helpful in script for managing updates
            //
            str += "    \n";
            str += "    \n";
            str += "    //PURPOSE\n";

            if (clanLeaderboard)
            {
                str += "    //   Enum with all field types used in clan leaderboards \n";
                str += String.Format("    ENUM CLAN_LB_FIELDSET\n");
            }
            else
            {
                str += "    //   Enum with all field types used in leaderboards \n";
                str += String.Format("    ENUM LB_FIELDSET\n");
            }

            while (props.MoveNext())
            {
                Property prop = (Property)props.Value;
                if (prop.IsUsedInLeaderboard)
                {
                    string fldIdName = prop.GetColumnIdName();

                    if (clanLeaderboard)
                        str += String.Format("        CLAN_{0}, //\t{1}\n", fldIdName, DataTypes.GetColumnTypeStrFromType(prop.Type, m_LeaderboardType));
                    else
                        str += String.Format("        {0}, //\t{1}\n", fldIdName, DataTypes.GetColumnTypeStrFromType(prop.Type, m_LeaderboardType));

                    count++;
                }
            }

            if (clanLeaderboard)
                str += String.Format("        CLAN_LB_FIELDSET_COUNT = {0}\n", count);
            else
                str += String.Format("             LB_FIELDSET_COUNT = {0}\n", count);
            str += "    ENDENUM\n";

            str += "    \n";
            str += "    \n";
            str += "    //PURPOSE\n";

            if (clanLeaderboard)
            {
                str += "    //   Enum with all leaderboard column ids.\n";
                str += String.Format("    ENUM CLAN_LBCOLUMNIDS\n");
            }
            else
            {
                str += "    //   Enum with all leaderboard column ids.\n";
                str += String.Format("    ENUM LBCOLUMNIDS\n");
            }

            //
            //  ENUM of columns used with associated column IDs 
            //
            count = 0;
            props.Reset();
            while (props.MoveNext())
            {
                Property prop = (Property)props.Value;
                if (prop.IsUsedInLeaderboard)
                {
                    string fldIdName = prop.GetColumnIdName();

                    if (clanLeaderboard)
                        str += String.Format("        CLAN_LB{0} = {1},\n", fldIdName, prop.Id);
                    else
                        str += String.Format("        LB{0} = {1},\n", fldIdName, prop.Id);

                    count++; 
                }     
            }

            if (clanLeaderboard)
            {
                str += String.Format("        CLAN_LBCOLUMNIDS_COUNT = {0}\n", count);
            }
            else
            {
                str += String.Format("        LBCOLUMNIDS_COUNT = {0}\n", count);
            }

            str += "    ENDENUM\n";

            string fldTypeAccessor = this.GetScriptFieldTypeAccessor();
            if (!string.IsNullOrEmpty(fldTypeAccessor))
            {
                str += fldTypeAccessor.TrimEnd("\n".ToCharArray()) + "\n";
            }

            return str;
        }
    }
}
