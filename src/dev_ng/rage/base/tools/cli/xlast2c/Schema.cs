using System;
using System.Collections;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace xlast2c
{
    //PURPOSE
    //  Base class for all schemas.
    //  Schemas are used for generating C++ class representations
    //  of XLAST schemas.
    abstract class Schema
    {
        //Collection of properties used by this schema.
        private ArrayList m_Members = new ArrayList();

        //Set the to true/false to generate/not generate
        //setter and getter accessors when GetAccessors() is called.
        protected bool m_GenerateSetters = true;
        protected bool m_GenerateGetters = true;

        public const int X_CONTEXT_GAME_TYPE = 0x0000800A;

        protected virtual bool InheritSchemaBase()
        {
            return true;
        }

        public void AddMember(Property prop)
        {
            if(null != prop)
            {
                Debug.Assert(Property.Filter(prop));

                if(!m_Members.Contains(prop))
                {
                    m_Members.Add(prop);
                }
            }
        }

        //PURPOSE
        //  Returns the C identifier of this schema.
        public abstract string GetName();

        public Property GetField(int index)
        {
            return (Property) m_Members[index];
        }

        public int GetFieldCount()
        {
            return m_Members.Count;
        }

        public int GetFieldIndex(Property prop)
        {
            return m_Members.IndexOf(prop);
        }

        //PURPOSE
        //  Returns a string containing an enumeration of field ids for
        //  the schema.
        protected virtual string GetFieldIdEnum()
        {
            return "";
        }

        //PURPOSE
        //  Returns a string containing, for each property,
        //  a C enumeration of the possible values for that property.
        public virtual string GetCValuesEnum()
        {
            string str = "";

            int fldCount = this.GetFieldCount();

            if(fldCount > 0)
            {
                for(int i = 0; i < fldCount; ++i)
                {
                    Property prop = this.GetField(i);

                    if(!prop.HasValues())
                    {
                        continue;
                    }

                    IDictionaryEnumerator values = prop.GetValues();

                    if(null == values)
                    {
                        continue;
                    }

                    string enumName = prop.Name.Replace(" ", "");

                    str += String.Format("    enum {0}\n    {{\n", enumName);

                    while(values.MoveNext())
                    {
                        string valName =
                            prop.Name.Replace(" ", "_").ToUpper() + "_" +
                            ((string) values.Key).Replace(" ", "_").ToUpper();

                        Property.PropertyValue propertyValue = (Property.PropertyValue)values.Value;
                        uint val = propertyValue.GetValue();

                        str += String.Format("        {0} = {1},\n", valName, val);
                    }

                    str += "    };\n";
                }

                if(!string.IsNullOrEmpty(str))
                {
                    //Remove the trailing newlines
                    str = str.TrimEnd("\n".ToCharArray()) + "\n";
                }
            }

            return str;
        }

        //PURPOSE
        //  Returns a string containing, for each property,
        //  a script enumeration of the possible values for that property.
        public virtual string GetScriptValuesEnum()
        {
            return "";
        }

        public virtual string GetRageScriptValuesEnum()
        {
            string str = "";

            int fldCount = this.GetFieldCount();

            if(fldCount > 0)
            {
                for(int i = 0; i < fldCount; ++i)
                {
                    Property prop = this.GetField(i);

                    if(!prop.HasValues())
                    {
                        continue;
                    }

                    IDictionaryEnumerator values = prop.GetValues();

                    if(null == values)
                    {
                        continue;
                    }

                    string prefix = this.GetName().Replace(" ", "").ToUpper() + "_";

                    string enumName = prefix + prop.Name.Replace(" ", "_").ToUpper();

                    if (i > 0)
                    {
                        str += "\n";
                    }

                    str += String.Format("ENUM {0}\n", enumName);

                    str += String.Format("    {0}_NUM_VALUES = {1},\n",
                                        enumName,
                                        prop.NumValues());

                    for(int j = 0; values.MoveNext(); ++j)
                    {
                        string valName =
                            prefix +
                            prop.Name.Replace(" ", "_").ToUpper() + "_" +
                            ((string) values.Key).Replace(" ", "_").ToUpper();

                        Property.PropertyValue propertyValue = (Property.PropertyValue)values.Value;
                        uint val = propertyValue.GetValue();

                        if (j > 0)
                        {
                            str += ",\n";
                        }

                        str += String.Format("    {0} = {1}", valName, val);
                    }

                    str += "\nENDENUM\n";
                }

                if(!string.IsNullOrEmpty(str))
                {
                    //Remove the trailing newlines
                    str = str.TrimEnd("\n".ToCharArray());
                }
            }

            return str;
        }

        //PURPOSE
        //  Returns a string representation of C initialization code
        //  that is typically placed inside a constructor.
        public virtual string GetInitializerList()
        {
            string str = "";

            int fldCount = this.GetFieldCount();

            str +=
                String.Format("        *(rage::u32*) m_fieldCount = {0};\n",
                m_Members.Count);

            str += String.Format("        *(rage::u64*) m_fieldMask = 0;\n");

            for(int i = 0; i < fldCount; ++i)
            {
                Property prop = this.GetField(i);

                //if(X_CONTEXT_GAME_TYPE != prop.Id)
                {
                    string memberName = prop.Name.Replace(" ", "");

                    str += String.Format("        *(rage::u32*) m_{0}_id = 0x{1:x8};\n",
                        memberName,
                        prop.Id);
                }
            }

            return str;
        }

        //PURPOSE
        //  Returns a string containing all the enumerations for this
        //  schema.  Can be overridden by subclasses.
        public virtual string GetEnum()
        {
            string fldCountEnum = "    enum\n    {\n";
            fldCountEnum += String.Format("        FIELD_COUNT = {0},\n", m_Members.Count);
            fldCountEnum += "    };\n";

            string s = this.GetFieldIdEnum();
            if(!string.IsNullOrEmpty(s))
            {
                s = s.TrimEnd("\n".ToCharArray()) + "\n";
            }

            return fldCountEnum + s + this.GetCValuesEnum();
        }

        //PURPOSE
        //  Returns a string containing all the enumerations for this
        //  schema.  Can be overridden by subclasses.
        public virtual string GetScriptEnum()
        {
            return this.GetScriptValuesEnum();
        }

        protected virtual string GetFieldIndexAccessor()
        {
            string str = "";

            int fldCount = this.GetFieldCount();

            if(fldCount > 0)
            {
                str += "    static int GetFieldIndexFromFieldId(const FieldId fieldId)\n";
                str += "    {\n";
                str += "        switch((int)fieldId)\n";
                str += "        {\n";

                for(int i = 0; i < fldCount; ++i)
                {
                    Property prop = this.GetField(i);
                    str += String.Format("        case {0}: ", prop.GetFieldIdName());
                    str += String.Format("return {0};\n", i);
                }

                str += "        }\n";

                str += "        return -1;\n";
                str += "    }";
            }

            return str;
        }

        protected virtual string GetFieldIdAccessor()
        {
            string str = "";

            int fldCount = this.GetFieldCount();

            if(fldCount > 0)
            {
                str += "    static FieldId GetFieldIdFromFieldIndex(const int fieldIndex)\n";
                str += "    {\n";
                str += "        switch(fieldIndex)\n";
                str += "        {\n";

                for(int i = 0; i < fldCount; ++i)
                {
                    Property prop = this.GetField(i);
                    str += String.Format("        case {0}: ", i);
                    str += String.Format("return {0};\n", prop.GetFieldIdName());
                }

                str += "        }\n";

                str += "        return FieldId(-1);\n";
                str += "    }";
            }

            return str;
        }

        protected virtual string GetFieldNameAccessor()
        {
            string str = "";

            int fldCount = this.GetFieldCount();

            if(fldCount > 0)
            {
                str += "    static const char* GetFieldNameFromFieldIndex(const int fieldIndex)\n";
                str += "    {\n";
                str += "        switch(fieldIndex)\n";
                str += "        {\n";

                for(int i = 0; i < fldCount; ++i)
                {
                    Property prop = this.GetField(i);
                    str += String.Format("        case {0}: ", i);
                    str += String.Format("return \"{0}\";\n", prop.GetFieldName());
                }

                str += "        }\n";

                str += "        return NULL;\n";
                str += "    }";
            }

            return str;
        }

        protected virtual string GetFieldIdArrayAccessor()
        {
            string str = "";

            int fldCount = this.GetFieldCount();

            if(fldCount > 0)
            {
                str += "    static void GetFieldIds(unsigned (&fldIds)[FIELD_COUNT])\n";
                str += "    {\n";

                for(int i = 0; i < fldCount; ++i)
                {
                    Property prop = this.GetField(i);
                    str += String.Format("        fldIds[{0}] = {1};\n",
                                        i,
                                        prop.GetFieldIdName());
                }

                str += "    }";
            }

            return str;
        }

        protected virtual string GetFieldNameArrayAccessor()
        {
            string str = "";

            int fldCount = this.GetFieldCount();

            if(fldCount > 0)
            {
                str += "    static void GetFieldNames(const char* (&fldNames)[FIELD_COUNT])\n";
                str += "    {\n";

                for(int i = 0; i < fldCount; ++i)
                {
                    Property prop = this.GetField(i);
                    str += String.Format("        fldNames[{0}] = GetFieldNameFromFieldIndex({1});\n",
                                        i,
                                        i);
                }

                str += "    }";
            }

            return str;
        }

        protected virtual string GetFieldTypeAccessor()
        {
            string str = "";

            int fldCount = this.GetFieldCount();

            if(fldCount > 0)
            {
                str += "    static FieldType GetFieldTypeFromFieldIndex(const int fieldIndex)\n";
                str += "    {\n";
                str += "        switch(fieldIndex)\n";
                str += "        {\n";

                for(int i = 0; i < fldCount; ++i)
                {
                    Property prop = this.GetField(i);
                    string fldType = DataTypes.GetFieldTypeStrFromType(prop.Type);
                    str += String.Format("        case {0}: ", i);
                    str += String.Format("return {0};\n", fldType);
                }

                str += "        }\n";

                str += String.Format("        return {0};\n",
                                    DataTypes.GetFieldTypeStrFromType(DataTypes.XUSER_DATA_TYPE_INVALID));
                str += "    }";
            }

            return str;
        }

        protected virtual string GetFieldSizeAccessor()
        {
            string str = "";

            int fldCount = this.GetFieldCount();

            if(fldCount > 0)
            {
                str += "    static unsigned GetSizeofFieldFromFieldIndex(const int fieldIndex)\n";
                str += "    {\n";
                str += "        switch(fieldIndex)\n";
                str += "        {\n";

                for(int i = 0; i < fldCount; ++i)
                {
                    Property prop = this.GetField(i);
                    string memberName = prop.Name.Replace(" ", "");

                    str += String.Format("        case {0}: ", i);
                    str += String.Format("return (unsigned) sizeof({0}); //m_{1}\n",
                                        prop.TypeStr,
                                        memberName);
                }

                str += "        }\n";

                str += "        return 0;\n";
                str += "    }";
            }

            return str;
        }

        //PURPOSE
        //  Returns a string representation of C++ accessors (Set/Get methods)
        //  for the properties of this schema.
        public virtual string GetAccessors()
        {
            string str = "";

            int fldCount = this.GetFieldCount();

            if(fldCount > 0)
            {
                str += "    bool IsFieldNil(const int fieldIndex) const\n";
                str += "    {\n";
                str += "        return 0 != (*((const rage::u64*) m_fieldMask) & (rage::u64(1) << fieldIndex));\n";
                str += "    }\n";

                str += "    void ClearField(const int fieldIndex)\n";
                str += "    {\n";
                str += "        *((rage::u64*) m_fieldMask) &= ~(rage::u64(1) << fieldIndex);\n";
                str += "    }\n";

                str += "    void ClearAllFields()\n";
                str += "    {\n";
                str += "        *((rage::u64*) m_fieldMask) = rage::u64(0);\n";
                str += "    }\n";

                str += "    bool SetFieldData(const int fieldIndex, const void* data, const unsigned sizeofData)\n";
                str += "    {\n";
                str += "        switch(fieldIndex)\n";
                str += "        {\n";

                for(int i = 0; i < fldCount; ++i)
                {
                    Property prop = this.GetField(i);
                    string memberName = prop.Name.Replace(" ", "");

                    str += String.Format("        case {0}:\n", i);
                    str += String.Format("        if(sizeof(m_{0})==sizeofData)\n", memberName);
                    str += String.Format("        {{\n");
                    str += String.Format("            *({1}*) m_{0} = *(const {1}*) data;\n", memberName, prop.TypeStr);
                    str += String.Format("            *((rage::u64*) m_fieldMask) |= (rage::u64(1)<<{0});\n", i);
                    str += String.Format("            return true;\n");
                    str += String.Format("        }}\n");
                    str += String.Format("        break;\n");
                }

                str += "        }\n";
                str += "        return false;\n";
                str += "    }\n";

                str += "    const void* GetFieldData(const int fieldIndex) const\n";
                str += "    {\n";
                str += "        switch(fieldIndex)\n";
                str += "        {\n";

                for(int i = 0; i < fldCount; ++i)
                {
                    Property prop = this.GetField(i);
                    string memberName = prop.Name.Replace(" ", "");

                    str += String.Format("        case {0}: ", i);
                    str += String.Format("return (*((rage::u64*) m_fieldMask) & (rage::u64(1)<<{0})) ? m_{1} : 0;\n",
                                        i,
                                        memberName);
                }

                str += "        }\n";
                str += "        return 0;\n";
                str += "    }\n";

                for(int i = 0; i < fldCount; ++i)
                {
                    Property prop = this.GetField(i);

                    //if(X_CONTEXT_GAME_TYPE != prop.Id)
                    {
                        string memberName = prop.Name.Replace(" ", "");

                        string fieldMaskSet =
                            String.Format("*((rage::u64*) m_fieldMask) |= (rage::u64(1)<<{0})", i);
                        string fieldMaskClear =
                            String.Format("*((rage::u64*) m_fieldMask) &= ~(rage::u64(1)<<{0})", i);
					    string fieldMaskIsSet =
						    String.Format("(*((rage::u64*) m_fieldMask) & (rage::u64(1)<<{0})) != 0", i);

                        if(m_GenerateSetters)
                        {
                            str += String.Format("    void Set{0}(const {1} val) {{ *({1}*) m_{0} = val; {2}; }}\n",
                                memberName,
                                prop.TypeStr,
                                fieldMaskSet);
                        }

                        if(m_GenerateGetters)
                        {
                            str += String.Format("    {0} Get{1}() const {{ return *({0}*) m_{1}; }}\n",
                                prop.TypeStr,
                                memberName);
                        }

                        str += String.Format("    void Clear{0}() {{{1}; }}\n",
                            memberName,
                            fieldMaskClear);

					    str += String.Format("    bool IsSet{0}() const {{ return {1}; }}\n",
						    memberName,
						    fieldMaskIsSet);				
				    }
                }
            }

            return str;
        }

        //PURPOSE
        //  Returns a string representation of C++ data field
        //  declarations for the properties of this schema.
        public virtual string GetFieldDeclarations()
        {
            string str = "";

            int fldCount = this.GetFieldCount();

            str = "    rage::u8 m_fieldCount[sizeof(rage::u32)];\n";

            str += "    rage::u8 m_fieldMask[sizeof(rage::u64)];\n";

            for(int i = 0; i < fldCount; ++i)
            {
                Property prop = this.GetField(i);

                //if(X_CONTEXT_GAME_TYPE != prop.Id)
                {
                    string memberName = prop.Name.Replace(" ", "");

                    str += String.Format("    rage::u8 m_{0}_id[sizeof(rage::u32)];\n", memberName);
                    //str += String.Format("    rage::u8 m_{0}_type[sizeof(rage::u8)];\n", memberName);
                    str += String.Format("    rage::u8 m_{0}[sizeof({1})];\n",
                        memberName,
                        prop.TypeStr);
                }
            }

            return str;
        }

        public virtual string GetInnerClasses()
        {
            return "";
        }

        //PURPOSE
        //  Generates a string representation of this schema as a C++ class.
        public virtual string ToCString()
        {
            string className = this.GetName();

            string str;

            if (this.InheritSchemaBase())
            {
                //SchemaBase is generated in Property.cs
                str = String.Format("class {0} : public SchemaBase\n{{\n", className);
            }
            else
            {
                str = String.Format("class {0}\n{{\n", className);
            }

            str += "public:\n";

            string enums = this.GetEnum();
            if (!string.IsNullOrEmpty(enums))
            {
                str += enums.TrimEnd("\n".ToCharArray()) + "\n";
            }

            //Inner classes
            string innerClasses = this.GetInnerClasses();
            if (null == innerClasses) { innerClasses = ""; }
            if (!string.IsNullOrEmpty(innerClasses))
            {
                Regex r = new Regex(".*\n");
                MatchCollection mc = r.Matches(innerClasses + "\n");

                foreach (Match match in mc)
                {
                    string line = match.ToString();
                    str += "    " + line;
                }

                str += "\n";
            }

            //Constructor

            str += String.Format("    {0}()\n", className);

            str += "    {\n";

            str += this.GetInitializerList();

            str += "    }\n";

            //Accessors

            string accessors = this.GetAccessors();
            if (!string.IsNullOrEmpty(accessors))
            {
                str += accessors.TrimEnd("\n".ToCharArray()) + "\n";
            }

            string fldIndexAccessor = this.GetFieldIndexAccessor();
            if (!string.IsNullOrEmpty(fldIndexAccessor))
            {
                str += fldIndexAccessor.TrimEnd("\n".ToCharArray()) + "\n";
            }

            string fldIdAccessor = this.GetFieldIdAccessor();
            if (!string.IsNullOrEmpty(fldIdAccessor))
            {
                str += fldIdAccessor.TrimEnd("\n".ToCharArray()) + "\n";
            }

            if (className == MatchingAttributes.GetNameStatic())
            {
                string fldNameAccessor = this.GetFieldNameAccessor();
                if (!string.IsNullOrEmpty(fldNameAccessor))
                {
                    str += fldNameAccessor.TrimEnd("\n".ToCharArray()) + "\n";
                }
            }

            string fldIdArrayAccessor = this.GetFieldIdArrayAccessor();
            if (!string.IsNullOrEmpty(fldIdArrayAccessor))
            {
                str += fldIdArrayAccessor.TrimEnd("\n".ToCharArray()) + "\n";
            }

            if (className == MatchingAttributes.GetNameStatic())
            {
                string fldNameArrayAccessor = this.GetFieldNameArrayAccessor();
                if (!string.IsNullOrEmpty(fldNameArrayAccessor))
                {
                    str += fldNameArrayAccessor.TrimEnd("\n".ToCharArray()) + "\n";
                }
            }

            string fldTypeAccessor = this.GetFieldTypeAccessor();
            if (!string.IsNullOrEmpty(fldTypeAccessor))
            {
                str += fldTypeAccessor.TrimEnd("\n".ToCharArray()) + "\n";
            }

            string fldSizeAccessor = this.GetFieldSizeAccessor();
            if (!string.IsNullOrEmpty(fldSizeAccessor))
            {
                str += fldSizeAccessor.TrimEnd("\n".ToCharArray()) + "\n";
            }

            //Member declarations

            str += "private:\n";

            str += this.GetFieldDeclarations();

            str += "};";

            return str;
        }

        //PURPOSE
        //  Generates a string representation of this schema as a C++ class.
        public virtual string ToEnumString()
        {
            string str = "";

            string enums = this.GetScriptEnum();
            if (!string.IsNullOrEmpty(enums))
            {
                str += enums.TrimEnd("\n".ToCharArray()) + "\n";
            }

            return str;
        }

        public virtual string ToRageScriptString()
        {
            return this.GetRageScriptValuesEnum();
        }
    }
}
