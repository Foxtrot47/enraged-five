using System;
using System.Xml;
using System.Collections;
using System.Diagnostics;

namespace xlast2c
{
    //PURPOSE
    //  Container for XLAST localized strings.
    //  Each localized string contains a collection of strings
    //  where each string is a translation into a different locale.
    class LocalizedString
    {
        //Locale/string pairs.  Strings are indexed by locale.
        Hashtable m_htStrings;
        //Name of string.
        public string m_Name;

        //PURPOSE
        //  Extracts a collection of string translations from a
        //  localized string element.
        public void Reset(XmlElement localizedString)
        {
            m_Name = localizedString.Attributes.GetNamedItem("friendlyName").Value;

            m_htStrings = new Hashtable();

            XmlElement xlate = (XmlElement) localizedString.FirstChild;

            Trace.WriteLine("  " + m_Name);

            //Iterate over the translations.
            for(; null != xlate; xlate = (XmlElement) xlate.NextSibling)
            {
                //Skip children not named "Translation"
                if(xlate.Name != "Translation")
                {
                    continue;
                }

                string locale = xlate.Attributes.GetNamedItem("locale").Value;

                m_htStrings[locale] = xlate.InnerText;

                Trace.WriteLine(String.Format("    {0} : \"{1}\"",
                    locale,
                    xlate.InnerText));
            }
        }

        //PURPOSE
        //  Indexes translations by locale.
        public string this[string locale]
        {
            get
            {
                return (string) m_htStrings[locale];
            }
        }
    }

    //PURPOSE
    //  Collection of localized strings.
    class LocalizedStringCollection
    {
        //Localize strings indexed by id.
        //Each value in the hash table is an instance of LocalizedString.
        Hashtable m_htTranslations;
        ArrayList m_aLocales;

        //PURPOSE
        //  Collects localized strings from the "LocalizedStrings"
        //  section of an xlast document.
        public void Reset(XmlDocument doc)
        {
            Trace.WriteLine("Collecting strings...");

            //Traverse to the "LocalizedStrings" section.
            XmlElement proj = doc.DocumentElement["GameConfigProject"];
            XmlElement localizedStrings = proj["LocalizedStrings"];
            XmlElement lsel = (XmlElement) localizedStrings.FirstChild;

            // make a list of the locales
            m_aLocales = new ArrayList();
            for (; null != lsel; lsel = (XmlElement)lsel.NextSibling)
            {
                //Skip children not named "SupportedLocale"
                if (lsel.Name != "SupportedLocale")
                {
                    continue;
                }

                string locale = lsel.Attributes.GetNamedItem("locale").Value;

                m_aLocales.Add(locale);
            }
            
            m_htTranslations = new Hashtable();

            //Iterate over the localized strings.
            lsel = (XmlElement)localizedStrings.FirstChild;
            for(; null != lsel; lsel = (XmlElement) lsel.NextSibling)
            {
                if(lsel.Name != "LocalizedString")
                {
                    continue;
                }

                XmlAttribute attr =
                    (XmlAttribute) lsel.Attributes.GetNamedItem("id");

                uint id = Property.ParseUInt(attr.Value);

                LocalizedString ls = new LocalizedString();

                ls.Reset(lsel);

                m_htTranslations[id] = ls;
            }
        }

        public void Append(XmlDocument doc)
        {
            Trace.WriteLine("Collecting additional appended strings...");

            //Traverse to the "LocalizedStrings" section.
            XmlElement proj = doc.DocumentElement["GameConfigProject"];
            XmlElement localizedStrings = proj["LocalizedStrings"];
            XmlElement lsel = (XmlElement)localizedStrings.FirstChild;

            //Iterate over the localized strings.
            lsel = (XmlElement)localizedStrings.FirstChild;
            for (; null != lsel; lsel = (XmlElement)lsel.NextSibling)
            {
                if (lsel.Name != "LocalizedString")
                {
                    continue;
                }

                XmlAttribute attr =
                    (XmlAttribute)lsel.Attributes.GetNamedItem("id");

                uint id = Property.ParseUInt(attr.Value);

                while(m_htTranslations.Contains(id))
                {
                    Trace.WriteLine(String.Format("Loc string hash collision detected for id {0} - {1}", id, ((XmlAttribute)lsel.Attributes.GetNamedItem("friendlyName")).Value));
                    id = id * 1000;
                }
                    
                LocalizedString ls = new LocalizedString();

                ls.Reset(lsel);

                m_htTranslations[id] = ls;               
            }
        }

         //PURPOSE
        //  Indexes localized strings by id.
        public LocalizedString this[uint id]
        {
            get
            {
                return (LocalizedString) m_htTranslations[id];
            }
        }

        //PURPOSE
        //  Retrieves the id of a named string.
        public bool GetNamedStringId(string name, out uint id)
        {
            id = 0;
            IDictionaryEnumerator entries = m_htTranslations.GetEnumerator();
            entries.Reset();
            while (entries.MoveNext())
            {
                LocalizedString str = (LocalizedString)entries.Value;
                if (str.m_Name == name)
                {
                    id = (uint)entries.Key;
                    return true;
                }
            }

            return false;
        }

        public IDictionaryEnumerator GetValues()
        {
            return (null != m_htTranslations) ? m_htTranslations.GetEnumerator() : null;
        }

        public IEnumerator GetLocales()
        {
            return (null != m_aLocales) ? m_aLocales.GetEnumerator() : null;
        }
    }
}
