using System;
using System.Xml;
using System.Collections;
using System.Diagnostics;

namespace xlast2c
{
    class StatsRecord : Schema
    {
        Leaderboard m_Lb;

        const int RANK_ID = 0x10008001;
        const int GAMER_NAME_ID = 0x40008002;

        //PURPOSE
        //  Returns the C++ class name.
        public override string GetName()
        {
            return "UpdateRecord";
        }

        //PURPOSE
        //  Extracts the properties that define the columns of a leaderboard.
        public void Reset(Leaderboard lb, XmlElement statsView, PropertyCollection props)
        {
            m_Lb = lb;

            XmlElement field = (XmlElement) statsView["Columns"].FirstChild;

            for(; null != field; field = (XmlElement) field.NextSibling)
            {
                XmlAttribute attr;

                XmlElement el = (XmlElement) field["Property"];

                if(null == el)
                {
                    el = (XmlElement) field["Context"];
                }

                attr = (XmlAttribute) el.Attributes.GetNamedItem("id");

                if(null != props[attr.Value])
                {
                    if(props[attr.Value].Id != RANK_ID &&
                        props[attr.Value].Id != GAMER_NAME_ID)
                    {
                        this.AddMember(props[attr.Value]);
                    }
                }
            }

            Trace.WriteLine("  " + m_Lb.GetName());
        }

        public override string GetEnum()
        {
            string str = base.GetEnum();

            return str;
        }

        public override string GetAccessors()
        {
            string str = base.GetAccessors() + "\n";

            //GetFieldIdFromColumnId()
            str += "    static FieldId GetFieldIdFromColumnId(const ColumnId columnId)\n";
            str += "    {\n";
            str += "        switch(columnId)\n";
            str += "        {\n";
            int colIdx = 0;
            foreach(Leaderboard.Column column in m_Lb.Columns)
            {
                str += String.Format("        case {0}: ", column.m_LbProp.GetColumnIdName());
                str += String.Format("return {0};\n", column.m_UpdateProp.GetFieldIdName());
                ++colIdx;
            }
            str += "        }\n";
            str += "        return FieldId(-1);\n";
            str += "    }\n";

            return str;
        }
    }

    /*//PURPOSE
    //  A collection of leaderboards.
    class StatsRecordCollection
    {
        //List of stats records.
        ArrayList m_StatsRecords;

        //PURPOSE
        //  Collects stats records from the "StatsViews" section of an xlast
        //  document.
        public void Reset(XmlDocument doc, PropertyCollection props)
        {
            Trace.WriteLine("Collecting stats record definitions...");

            XmlElement proj = doc.DocumentElement["GameConfigProject"];
            XmlElement statsViews = null != proj ? proj["StatsViews"] : null;
            XmlElement el = null != statsViews ? (XmlElement) statsViews.FirstChild : null;

            //Build the record defs

            m_StatsRecords = new ArrayList();

            for(; null != el; el = (XmlElement) el.NextSibling)
            {
                StatsRecord sr = new StatsRecord();

                sr.Reset(el, props);

                m_StatsRecords.Add(sr);
            }
        }

        //PURPOSE
        //  Generates (in string form) the C++ class representation
        //  of all leaderboards.
        public virtual string ToCString()
        {
            string str = "";

            for(int i = 0; i < m_StatsRecords.Count; ++i)
            {
                str = str + ((StatsRecord) m_StatsRecords[i]).ToCString();

                if(i < m_StatsRecords.Count - 1)
                {
                    str += "\n\n";
                }
            }

            return str;
        }
    }*/
}
