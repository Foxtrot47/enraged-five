XLAST 2 C
=========

Usage: xlast2c <filename>

Where <filename> is the name of an XLAST file.

xlast2c generates C++ classes (schemas) from XML data contained in a XLAST
file.

The C++ classes are written to a file called *.schema.h where * is the name of
the XLAST file.

For example, if the name of the XLAST file is Foo.xlast then the name of the
schema file will be Foo.schema.h.