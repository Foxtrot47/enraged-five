using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Text.RegularExpressions;

namespace xlast2c
{
    class SteamWriter
    {
        string m_SteamVdfPath;
        Achievements m_Achievements;
        LocalizedStringCollection m_Strings;
        Images m_Images;

        Dictionary<string, LocalizedString> SteamMaps;
        List<string> SteamLanguages;

        public SteamWriter(string steamVdfPath, Achievements ach, LocalizedStringCollection loc, Images im)
        {
            m_Achievements = ach;
            m_Strings = loc;
            m_Images = im;
            m_SteamVdfPath = steamVdfPath;

            SteamLanguages = new List<string>();
            SteamMaps = new Dictionary<string, LocalizedString>();
        }

        private void AddLocMap(string steamKey, string steamLoc)
        {
            IDictionaryEnumerator i = m_Strings.GetValues();
            bool bFoundLoc = false;

            while (i.MoveNext() && !bFoundLoc)
            {
                LocalizedString v = (LocalizedString)i.Value;
                IEnumerator locales = m_Strings.GetLocales();
                while (locales.MoveNext() && !bFoundLoc)
                {
                    string ls = v[(string)locales.Current];
                    if (String.Equals(ls,steamLoc, StringComparison.OrdinalIgnoreCase))
                    {
                        SteamMaps.Add(steamKey, v);
                        bFoundLoc = true;
                    }
                }
            }

            if (!bFoundLoc)
            {
                Console.WriteLine("ERROR: KEY COULD NOT BE FOUND: " + steamKey);
                Console.ReadLine();
            }
        }

        private bool ParseSteamVdf()
        {
            if (!File.Exists(m_SteamVdfPath))
            {
                throw new FileNotFoundException(m_SteamVdfPath);
            }

            string[] Lines = File.ReadAllLines(m_SteamVdfPath);

            string curLanguage = "";
            foreach (string str in Lines)
            {
                MatchCollection kvpStr = GetKvp(str);
                if (kvpStr.Count > 0)
                {
                    foreach (Match m in kvpStr)
                    {
                        string[] s = ClearTabs(m.Value).Split(new string[] { "\"\"" }, StringSplitOptions.None);
                        if (curLanguage.ToLower() == "english")
                        {
                            AddLocMap(RemoveQuotes(s[0]), RemoveQuotes(s[1]));
                        }
                    }
                }
                else
                {
                    string objStr = GetObject(str);
                    if (!String.IsNullOrEmpty(objStr))
                    {
                        // Steam VDF object names
                        if (objStr.ToLower() == "lang" || objStr.ToLower() == "tokens")
                        {
                            continue;
                        }
                        else
                        {
                            SteamLanguages.Add(objStr);
                            curLanguage = objStr;
                        }
                    }
                }
            }

            return true;
        }

        private string GetObject(string str)
        {
            Match m = Regex.Match(str, "(\").*(\")");
            string val = m.Value;
            return ClearTabs(RemoveQuotes(val));
        }

        private MatchCollection GetKvp(string str)
        {
            MatchCollection m = Regex.Matches(str, ".*(\").*(\").*(\").*(\")");
            return m;
        }

        private string ClearTabs(string str)
        {
            return str.Replace("\t", "");
        }

        private string RemoveQuotes(string str)
        {
            return str.Replace("\"", "");
        }

        public void SteamExport()
        {
            if (!ParseSteamVdf())
            {
                // ruh oh...
            }

            List<String> Output = new List<String>();
            Output.Add("\"lang\"");
            Output.Add("{");

            foreach (string locale in SteamLanguages)
            {
                Output.Add(String.Format("\t\"{0}\"", locale));
                Output.Add("\t{");
                    Output.Add("\t\t\"Tokens\"");
                    Output.Add("\t\t{");
                    foreach(KeyValuePair<string,LocalizedString> kvp in SteamMaps)
                    {
                        string key = kvp.Key;
                        string lang = "";
                        switch(locale)
                        {
                            case "english":
                                lang = "en-US";
                                break;
                            case "german":
                                lang = "de-DE";
                                break;
                            case "french":
                                lang = "fr-FR";
                                break;
                            case "italian":
                                lang = "it-IT";
                                break;
                            case "korean":
                                lang = "ko-KR";
                                break;
                            case "spanish":
                                lang = "es-ES";
                                break;
                            case "tchinese":
                                lang = "zh-CHT";
                                break;
                            case "schinese":
                                lang = "zh-CN";
                                break;
                            case "russian":
                                lang = "ru-RU";
                                break;
                            case "japanese":
                                lang = "ja-JP";
                                break;
                            case "polish":
                                lang = "pl-PL";
                                break;
                            case "brazilian":
                                lang = "pt-PT";
                                break;
                            default:
                                lang = "en-US";
                                break;
                        }
                        string locStr = kvp.Value[lang].Replace("\"", "'");
                        Output.Add(String.Format("\t\t\t\"{0}\"\t\"{1}\"", key, locStr));
                    }
                    Output.Add("\t\t}");
                Output.Add("\t}");
            }

            Output.Add("}");

            string p = m_SteamVdfPath.Replace(".vdf", "_result.vdf");
            File.WriteAllLines(p, Output.ToArray());
        }
    }
}
