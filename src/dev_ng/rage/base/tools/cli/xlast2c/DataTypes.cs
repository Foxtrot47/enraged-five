namespace xlast2c
{
    //PURPOSE
    //  This is a collection of type identifiers as defined
    //  by Xenon Live (see xonline.h).
    class DataTypes
    {
        public const uint XUSER_DATA_TYPE_INVALID   = ~0u;
        public const uint XUSER_DATA_TYPE_CONTEXT   = 0;
        public const uint XUSER_DATA_TYPE_INT32     = 1;
        public const uint XUSER_DATA_TYPE_INT64     = 2;
        public const uint XUSER_DATA_TYPE_DOUBLE    = 3;
        public const uint XUSER_DATA_TYPE_UNICODE   = 4;
        public const uint XUSER_DATA_TYPE_FLOAT     = 5;
        public const uint XUSER_DATA_TYPE_BINARY    = 6;
        public const uint XUSER_DATA_TYPE_DATETIME  = 7;

        //public const uint X_CONTEXT_GAME_TYPE_ID    = 0x0000800A;
        public const uint X_CONTEXT_GAME_MODE_ID    = 0x0000800B;

        public const uint X_PROPERTY_TYPE_MASK      = 0xF0000000;
        public const uint X_PROPERTY_SCOPE_MASK     = 0x00008000;
        public const uint X_PROPERTY_ID_MASK        = 0x00007FFF;


        //PURPOSE
        //  Extracts the type id from the item id.
        public static uint GetTypeFromId(uint itemId)
        {
            return (uint) (itemId & X_PROPERTY_TYPE_MASK) >> 28;
        }

        public static uint MakePropertyId(uint type, uint id)
        {
            return ((type << 28) & X_PROPERTY_TYPE_MASK) | (id & X_PROPERTY_ID_MASK);
        }

        //PURPOSE
        //  Translates a type id into a type string.
        public static string GetTypeStrFromType(uint type)
        {
            string typeStr;

            switch(type)
            {
                case XUSER_DATA_TYPE_CONTEXT:
                    typeStr = "rage::u32";
                    break;
                case XUSER_DATA_TYPE_INT32:
                    typeStr = "rage::u32";
                    break;
                case XUSER_DATA_TYPE_INT64:
                    typeStr = "rage::u64";
                    break;
                case XUSER_DATA_TYPE_DOUBLE:
                    typeStr = "double";
                    break;
                case XUSER_DATA_TYPE_FLOAT:
                    typeStr = "float";
                    break;
                default:
                    typeStr = "**UNKNOWN_TYPE**";
                    break;
            }

            return typeStr;
        }

        public static string GetFieldTypeStrFromType(uint type)
        {
            string typeStr;

            switch (type)
            {
                case XUSER_DATA_TYPE_CONTEXT:
                    typeStr = "FIELDTYPE_INT32";
                    break;
                case XUSER_DATA_TYPE_INT32:
                    typeStr = "FIELDTYPE_INT32";
                    break;
                case XUSER_DATA_TYPE_INT64:
                    typeStr = "FIELDTYPE_INT64";
                    break;
                case XUSER_DATA_TYPE_DOUBLE:
                    typeStr = "FIELDTYPE_DOUBLE";
                    break;
                case XUSER_DATA_TYPE_FLOAT:
                    typeStr = "FIELDTYPE_FLOAT";
                    break;
                default:
                    typeStr = "FIELDTYPE_INVALID";
                    break;
            }

            return typeStr;
        }

        public static string GetColumnTypeStrFromType(uint type, eLeaderboardtypes leaderboardType)
        {
            string typeStr = "";

            if (leaderboardType == eLeaderboardtypes.LEADERBOARD_TYPE_CLAN)
            {
                typeStr += "CLAN_";
            }

            switch (type)
            {
                case XUSER_DATA_TYPE_CONTEXT:
                    typeStr += "LBCOLUMNTYPE_INT32";
                    break;
                case XUSER_DATA_TYPE_INT32:
                    typeStr += "LBCOLUMNTYPE_INT32";
                    break;
                case XUSER_DATA_TYPE_INT64:
                    typeStr += "LBCOLUMNTYPE_INT64";
                    break;
                case XUSER_DATA_TYPE_DOUBLE:
                    typeStr += "LBCOLUMNTYPE_DOUBLE";
                    break;
                case XUSER_DATA_TYPE_FLOAT:
                    typeStr += "LBCOLUMNTYPE_FLOAT";
                    break;
                default:
                    typeStr += "LBCOLUMNTYPE_INVALID";
                    break;
            }

            return typeStr;
        }
    }
}
