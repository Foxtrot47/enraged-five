using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

using System.Drawing;
using System.Drawing.Drawing2D;

namespace xlast2c
{
    public class EndianBinaryWriter : BinaryWriter
    {
        public EndianBinaryWriter(Stream stream)
            : base(stream)
        {

        }
        public override void Write(float value)
        {
            byte[] b = BitConverter.GetBytes(value);
            Array.Reverse(b);
            Write(b);
        }
        public override void Write(double value)
        {
            byte[] b = BitConverter.GetBytes(value);
            Array.Reverse(b);
            Write(b);
        }
        public override void Write(int value)
        {
            byte[] b = BitConverter.GetBytes(value);
            Array.Reverse(b);
            Write(b);
        }
        public override void Write(uint value)
        {
            byte[] b = BitConverter.GetBytes(value);
            Array.Reverse(b);
            Write(b);
        }
        public override void Write(short value)
        {
            byte[] b = BitConverter.GetBytes(value);
            Array.Reverse(b);
            Write(b);
        }
        public override void Write(ushort value)
        {
            byte[] b = BitConverter.GetBytes(value);
            Array.Reverse(b);
            Write(b);
        }
        public override void Write(long value)
        {
            byte[] b = BitConverter.GetBytes(value);
            Array.Reverse(b);
            Write(b);
        }
        public override void Write(ulong value)
        {
            byte[] b = BitConverter.GetBytes(value);
            Array.Reverse(b);
            Write(b);
        }
    }
    
    class PcMetadataWriter
    {
        const uint PC_METADATA_VERSION = 1;

        static uint atFinalizeHash(uint partialHashValue)
        {
            uint key = partialHashValue;
            key += (key << 3);	//lint !e701
            key ^= (key >> 11);	//lint !e702
            key += (key << 15);	//lint !e701
            return key;
        }

        static uint atDataHash(byte[] data, uint size, uint initValue)
        {
            // This is the one-at-a-time hash from this page:
            // http://burtleburtle.net/bob/hash/doobs.html
            // (borrowed from /soft/swat/src/swcore/string2key.cpp)

            uint key = initValue;
            for (uint count = 0; count < size; count++)
            {
                key += data[count];
                key += (key << 10);	//lint !e701
                key ^= (key >> 6);	//lint !e702
            }

            return atFinalizeHash(key);
        }

        static uint atStringHash(string s, uint initValue)
        {
            byte[] bytes = System.Text.Encoding.ASCII.GetBytes(s.ToLower());
            return PcMetadataWriter.atDataHash(bytes, (uint)bytes.Length, initValue);
        }

        public class Database
        {
            public class Table
            {
                enum IndexType
                {
                    NO_INDEX = 0,
                    ARRAY_INDEX = 1,
                    HASHTABLE_INDEX = 2,
                    BTREE_INDEX = 3,
                };

                public abstract class Record
                {
                    uint m_Key;

                    public Record(uint key)
                    {
                        m_Key = key;
                    }

                    public uint GetKey()
                    {
                        return m_Key;
                    }

                    abstract public uint GetSize();

                    abstract public void Write(BinaryWriter bw);
                }

                bool m_Analyzed;
                IndexType m_IndexType;
                uint m_MinKey;
                uint m_MaxKey;
                uint m_KeySize;
                uint m_MinRecordSize;
                uint m_MaxRecordSize;
                uint m_OffsetSize;
                uint m_NumRecords;
                uint m_TotalMetaDataSize;
                uint m_TotalIndexSize;
                uint m_TotalDataSize;
                uint m_TotalSize;

                string m_Name;
                uint m_Id;
                SortedDictionary<uint, Record> m_Records;

                public Table(string tableName)
                {
                    m_Name = tableName;
                    m_Id = PcMetadataWriter.atStringHash(m_Name.ToLower(), 0);

                    m_Analyzed = false;
                    m_IndexType = IndexType.NO_INDEX;
                    m_MinKey = 0;
                    m_MaxKey = 0;
                    m_KeySize = 0;
                    m_MinRecordSize = 0;
                    m_MaxRecordSize = 0;
                    m_OffsetSize = 0;
                    m_NumRecords = 0;
                    m_TotalMetaDataSize = 36;   // TODO: NS - would be better to have this more generically calculated
                    m_TotalIndexSize = 0;
                    m_TotalDataSize = 0;
                    m_TotalSize = 0;

                    m_Records = new SortedDictionary<uint, Record>();
                }

                public uint GetId()
                {
                    return m_Id;
                }

                public uint GetNumRecords()
                {
                    return (uint)m_Records.Count;
                }

                public bool DoesRecordExist(uint key)
                {
                    Record ignore;
                    return m_Records.TryGetValue(key, out ignore);
                }

                public void AddRecord(Record r)
                {
                    Debug.Assert(m_Analyzed == false);

                    Debug.Assert(DoesRecordExist(r.GetKey()) == false);
                    m_Records[r.GetKey()] = r;
                }

                public Record this[uint id]
                {
                    get
                    {
                        return (Record)m_Records[id];
                    }
                }

                public IDictionaryEnumerator GetRecords()
                {
                    return m_Records.GetEnumerator();
                }

                public uint GetSize()
                {
                    if (m_Analyzed == false)
                    {
                        Analyze();
                    }

                    return m_TotalSize;
                }

                public void Analyze()
                {
                    Debug.Assert(m_Analyzed == false);

                    // analyze the table to determine the most efficient type of disk-based
                    // data structure to use for the given data

                    // first check if an index is needed
                    //    an index is not needed if:
                    //    1. the keys are sequential, and the records are fixed length, and
                    //       the total data size is not significantly more than the index would be

                    // if an index is needed, then we choose either a disk-based array with
                    // implicit keys (more efficient), or a disk-based hashtable with stored keys
                    //    disk-based array requirements:
                    //    1. the keys are sequential (although the lowest key can start at anything,
                    //       and the data is analyzed to determine if filling in gaps with dummy
                    //       records would be better than falling back to a hash table index)

                    IDictionaryEnumerator records = GetRecords();
                    records.Reset();

                    bool sequential = true;
                    m_MinRecordSize = 0;
                    m_MaxRecordSize = 0;
                    m_TotalIndexSize = 0;
                    m_TotalDataSize = 0;
                    m_MinKey = 0;
                    m_MaxKey = 0;
                    if (records.MoveNext())
                    {
                        Database.Table.Record r = (Database.Table.Record)records.Value;
                        m_MinKey = r.GetKey();
                        m_MinRecordSize = r.GetSize();
                        m_MaxRecordSize = m_MinRecordSize;
                    }

                    uint sequence = m_MinKey;
                    records.Reset();
                    while (records.MoveNext())
                    {
                        Database.Table.Record r = (Database.Table.Record)records.Value;
                        if (sequence != r.GetKey())
                        {
                            sequential = false;
                        }

                        m_TotalDataSize += r.GetSize();

                        uint recordSize = r.GetSize();

                        m_MinRecordSize = Math.Min(m_MinRecordSize, recordSize);
                        m_MaxRecordSize = Math.Max(m_MaxRecordSize, recordSize);

                        m_MaxKey = Math.Max(m_MaxKey, r.GetKey());

                        sequence++;
                    }

                    bool fixedSizedRecords = m_MinRecordSize == m_MaxRecordSize;

                    m_NumRecords = GetNumRecords();

                    m_IndexType = sequential ? IndexType.ARRAY_INDEX : IndexType.HASHTABLE_INDEX;
                    m_KeySize = 0;
                    m_OffsetSize = 0;

                    if (m_TotalDataSize <= byte.MaxValue)
                    {
                        m_OffsetSize = 1;
                    }
                    else if (m_TotalDataSize <= ushort.MaxValue)
                    {
                        m_OffsetSize = 2;
                    }
                    else
                    {
                        m_OffsetSize = 4;
                    }

                    if ((m_MaxKey - m_MinKey + 1) <= byte.MaxValue)
                    {
                        m_KeySize = 1;
                    }
                    else if ((m_MaxKey - m_MinKey + 1) <= ushort.MaxValue)
                    {
                        m_KeySize = 2;
                    }
                    else
                    {
                        m_KeySize = 4;
                    }

                    if (m_IndexType == IndexType.ARRAY_INDEX)
                    {
                        m_KeySize = 0;    // the keys will implicitly be the index of the disk-based array
                    }

                    // calculate the total size in bytes of the index
                    m_TotalIndexSize = (m_OffsetSize * m_NumRecords) + (m_KeySize * m_NumRecords);

                    // determine if it would be more efficient to fill in gaps with dummy records to
                    // make it sequential than to leave it non-sequential and store the keys. For example,
                    // if keys 1-39 and 41-60 exist, but key 40 is missing, then we can add a dummy
                    // record at key = 40 and use a sequential data structure instead of a hashtable)

                    ulong numDummyRecords = (m_MaxKey - m_MinKey + 1) - m_NumRecords;

                    if (sequential == false)
                    {
                        ulong newIndexSize = m_OffsetSize * (m_NumRecords + numDummyRecords);

                        // switch to an array index if it would be smaller than a hashtable index
                        // or even if it would slightly larger since lookup times would benefit significantly
                        if (newIndexSize <= (m_TotalIndexSize * 2.0))
                        {
                            m_IndexType = IndexType.ARRAY_INDEX;
                            m_KeySize = 0;
                            m_NumRecords += (uint)numDummyRecords;
                            m_TotalIndexSize = (uint)newIndexSize;
                            sequential = true;
                        }
                    }

                    // determine if it would be better to eliminate the index altogether
                    if (sequential && fixedSizedRecords && (numDummyRecords == 0))// && (m_TotalDataSize <= (m_TotalIndexSize * 2)))
                    {
                        m_IndexType = IndexType.NO_INDEX;
                        m_KeySize = 0;            // no keys to store (the keys and offsets are calculated directly from the fixed size records)
                        m_TotalIndexSize = 0;
                    }

                    m_TotalSize = m_TotalMetaDataSize + m_TotalIndexSize + m_TotalDataSize;
                    m_Analyzed = true;
                }

                public void WriteMetaData(BinaryWriter bw)
                {
                    // write the table's meta data
                    // Note: if you change the amount of metadata written,
                    // you must change m_TotalMetaDataSize in the contructor
                    bw.Write((byte)m_IndexType);        // 1
                    bw.Write((byte)m_KeySize);          // 2
                    bw.Write((byte)m_OffsetSize);       // 3
                    bw.Write((byte)0 /*reserved*/);     // 4
                    bw.Write((uint)m_MinRecordSize);    // 8
                    bw.Write((uint)m_MaxRecordSize);    // 12
                    bw.Write((uint)m_MinKey);           // 16
                    bw.Write((uint)m_MaxKey);           // 20
                    bw.Write((uint)m_NumRecords);       // 24
                    bw.Write((uint)m_NumRecords - GetNumRecords());       // 28
                    bw.Write((uint)m_TotalIndexSize);   // 32
                    bw.Write((uint)m_TotalDataSize);    // 36

                    Trace.WriteLine("Writing Table '" + m_Name + "'");
                    Trace.WriteLine((m_IndexType == IndexType.NO_INDEX) ? "\tNo index" : (m_IndexType == IndexType.ARRAY_INDEX) ? "\tArray index" : "\tHash table index");
                    Trace.WriteLine("\tMinimum Key: " + m_MinKey);
                    Trace.WriteLine("\tMaximum Key: " + m_MaxKey);
                    Trace.WriteLine("\tKey Size: " + m_KeySize);
                    Trace.WriteLine((m_MinRecordSize == m_MaxRecordSize) ? "\tFixed size records of length " + m_MinRecordSize + " bytes" : "\tVariable length records");
                    Trace.WriteLine("\tOffset size: " + m_OffsetSize);
                    Trace.WriteLine("\tMinimum Record Size: " + m_MinRecordSize );
                    Trace.WriteLine("\tMaximum Record Size: " + m_MaxRecordSize );
                    Trace.WriteLine("\tNumber of Records: " + m_NumRecords);
                    Trace.WriteLine("\tNumber of Dummy Records: " + (m_NumRecords - GetNumRecords()));
                    Trace.WriteLine("\tTotal Index Size: " + m_TotalIndexSize);
                    Trace.WriteLine("\tTotal Data Size: " + m_TotalDataSize);
                    Trace.WriteLine("\tTotal Size: " + m_TotalSize);
                }

                public void WriteIndex(BinaryWriter bw)
                {
                    IDictionaryEnumerator records = GetRecords();

                    // write the table's index
                    if (m_IndexType == IndexType.ARRAY_INDEX)
                    {
                        Debug.Assert(m_KeySize == 0);

                        records.Reset();

                        uint prevRealKey = m_MinKey;
                        uint curKey = m_MinKey;
                        uint offset = 0;

                        while (records.MoveNext())
                        {
                            Database.Table.Record r = (Database.Table.Record)records.Value;

                            // if we need to insert dummy records to make it sequential, duplicate the current record so that 
                            // record sizes can still be calculated by taking the difference between consecutive offsets

                            uint numRecords = r.GetKey() - prevRealKey;

                            prevRealKey = r.GetKey();
                            if (prevRealKey == m_MinKey)
                            {
                                numRecords = 1;
                            }

                            while (numRecords > 0)
                            {
                                if (numRecords == 1)
                                {
                                    offset += r.GetSize();
                                }

                                // write offset
                                if (m_OffsetSize == 1)
                                {
                                    Debug.Assert(r.GetSize() <= byte.MaxValue);
                                    bw.Write((byte)offset);
                                }
                                else if (m_OffsetSize == 2)
                                {
                                    Debug.Assert(r.GetSize() <= ushort.MaxValue);
                                    bw.Write((ushort)offset);
                                }
                                else
                                {
                                    Debug.Assert(r.GetSize() <= uint.MaxValue);
                                    bw.Write((uint)offset);
                                }

                                curKey++;
                                numRecords--;
                            }
                        }
                    }
                    else if (m_IndexType == IndexType.HASHTABLE_INDEX)
                    {
                        Debug.Assert(m_KeySize > 0);

                        records.Reset();

                        uint offset = 0;

                        while (records.MoveNext())
                        {
                            Database.Table.Record r = (Database.Table.Record)records.Value;

                            offset += r.GetSize();

                            uint key = r.GetKey() - m_MinKey;                            

                            // write key
                            if (m_KeySize == 1)
                            {
                                Debug.Assert(key <= byte.MaxValue);
                                bw.Write((byte)key);
                            }
                            else if (m_KeySize == 2)
                            {
                                Debug.Assert(key <= ushort.MaxValue);
                                bw.Write((ushort)key);
                            }
                            else
                            {
                                Debug.Assert(key <= uint.MaxValue);
                                bw.Write((uint)key);
                            }

                            // write offset
                            if (m_OffsetSize == 1)
                            {
                                Debug.Assert(r.GetSize() <= byte.MaxValue);
                                bw.Write((byte)offset);
                            }
                            else if (m_OffsetSize == 2)
                            {
                                Debug.Assert(r.GetSize() <= ushort.MaxValue);
                                bw.Write((ushort)offset);
                            }
                            else
                            {
                                Debug.Assert(r.GetSize() <= uint.MaxValue);
                                bw.Write((uint)offset);
                            }
                        }
                    }
                }

                public void WriteData(BinaryWriter bw)
                {
                    // if we're using a hashtable index or if records are variable length,
                    // then we don't need to add dummy records to the data partition
                    // (dummy records for variable length tables will implicitly be 0 length,
                    // and therefore don't require any storage)
                    uint numDummyRecords = m_NumRecords - GetNumRecords();
                    bool fixedSizeRecords = m_MinRecordSize == m_MaxRecordSize;

                    if ((m_IndexType == IndexType.HASHTABLE_INDEX) || (m_IndexType == IndexType.ARRAY_INDEX) || (fixedSizeRecords == false) || (numDummyRecords == 0))
                    {
                        IDictionaryEnumerator records = GetRecords();
                        records.Reset();
                        while (records.MoveNext())
                        {
                            Database.Table.Record r = (Database.Table.Record)records.Value;
                            r.Write(bw);
                        }
                    }
                    else
                    {
                        // TODO: NS - this isn't needed because we no longer add dummy records to the data partition.

                        // the analysis stage has indicated that it will be more efficient to pad
                        // the data with with dummy records in order to make the data structure
                        // sequential, so we have more work to do.

                        uint prevRealKey = m_MinKey;
                        uint curKey = m_MinKey;

                        IDictionaryEnumerator records = GetRecords();
                        records.Reset();
                        while (records.MoveNext())
                        {
                            Database.Table.Record r = (Database.Table.Record)records.Value;

                            uint numRecords = r.GetKey() - prevRealKey;

                            prevRealKey = r.GetKey();
                            if (prevRealKey == m_MinKey)
                            {
                                numRecords = 1;
                            }

                            numRecords--;

                            while (numRecords > 0)
                            {
                                // write a dummy record
                                byte[] dummyBytes = new byte[m_MinRecordSize];
                                bw.Write(dummyBytes);

                                curKey++;
                                numRecords--;
                            }

                            r.Write(bw);
                            curKey++;
                        }
                    }
                }

                public void Write(BinaryWriter bw)
                {
                    if (m_Analyzed == false)
                    {
                        Analyze();
                    }

                    WriteMetaData(bw);
                    WriteIndex(bw);
                    WriteData(bw);
                }
            }

            public class TableRecord : Database.Table.Record
            {
                Table m_Table;

                public TableRecord(uint key, Table table)
                    : base(key)
                {
                    m_Table = table;
                }

                public Table GetTable()
                {
                    return m_Table;
                }

                public override uint GetSize()
                {
                    return m_Table.GetSize();
                }

                public override void Write(BinaryWriter bw)
                {
                    m_Table.Write(bw);
                }
            }

            Table m_Tables;

            public Database()
            {
                m_Tables = new Table("Database_TOC");
            }

            public uint GetNumTables()
            {
                return (uint)m_Tables.GetNumRecords();
            }

            public bool DoesTableExist(string tableName)
            {
                uint id = PcMetadataWriter.atStringHash(tableName.ToLower(), 0);
                return m_Tables.DoesRecordExist(id);
            }

            public void AddTable(Table t)
            {
                m_Tables.AddRecord(new TableRecord(t.GetId(), t));
            }

            public Table this[uint id]
            {
                get
                {
                    TableRecord t = (TableRecord)m_Tables[id];
                    return t.GetTable();
                }
            }

            public IDictionaryEnumerator GetTables()
            {
                return m_Tables.GetRecords();
            }

            public void Write(BinaryWriter bw)
            {
                bw.Write((uint)PC_METADATA_VERSION);
                bw.Write(System.Text.Encoding.ASCII.GetBytes("META"));

                string timestamp = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss");
                bw.Write(System.Text.Encoding.ASCII.GetBytes(timestamp));
                m_Tables.Write(bw);
            }
        }

        Achievements m_Achievements;
        LocalizedStringCollection m_Strings;
        Images m_Images;
        PresenceCollection m_Presences;
        Database m_Database;
        String m_DirectoryPath;


        public PcMetadataWriter(Achievements ach, LocalizedStringCollection loc, Images im, PresenceCollection pres, string directoryPath)
        {
            m_Achievements = ach;
            m_Strings = loc;
            m_Images = im;
            m_Presences = pres;
            m_DirectoryPath = directoryPath;
        }

        private string GetString(uint id, string langid)
        {
            string result = m_Strings[id][langid];
            if (result == null)
                result = "(unknown)";
            return result;
        }

        private string GetString(string name, string langid)
        {
            uint id = 0;
            bool exists = m_Strings.GetNamedStringId(name, out id);
            if (exists)
            {
                return GetString(id, langid);           
            }
            return "(unknown)";
        }

        private string GetStringFromFriendly(string sid, string langid)
        {
            IDictionaryEnumerator i = m_Strings.GetValues();
            while (i.MoveNext())
            {
                if (((LocalizedString)i.Value).m_Name == sid)
                    return GetString((uint)i.Key, langid);
            }
            return sid;
        }

        public class AchievementMetaData : Database.Table.Record
        {
            byte m_Value;
            byte m_Flags;

            public AchievementMetaData(uint key, byte value, byte flags)
                : base(key)
            {
                m_Value = value;
                m_Flags = flags;
            }

            public override uint GetSize()
            {
                return sizeof(byte) + sizeof(byte);
            }

            public override void Write(BinaryWriter bw)
            {
                bw.Write(m_Value);
                bw.Write(m_Flags);
            }
        }

        private void CreateAchievementMetaDataTable(Database db)
        {
            Database.Table t = new Database.Table("Achievement_Metadata");

            IEnumerator entries = m_Achievements.GetEnumerator();
            entries.Reset();
            while (entries.MoveNext())
            {
                Achievements.Achievement a = (Achievements.Achievement)entries.Current;
                Debug.Assert(a.cred <= byte.MaxValue);

                byte flags = 0;

                if (a.showUnachieved)
                {
                    flags |= 0x01;
                }
                Debug.Assert(a.type <= 8);
                flags |= (byte)(a.type << 1);
                Debug.Assert((flags & 0xF0) == 0);

                // Note: currently only the lowest 4 flag bits are used.
                // The remaining 4 bits can be used for other purposes.

                AchievementMetaData r = new AchievementMetaData(a.m_Prop.Id, (byte)a.cred, flags);

                t.AddRecord(r);
            }

            db.AddTable(t);
        }

        public class ImageRecord : Database.Table.Record
        {
            byte[] m_ImageData;

            public ImageRecord(uint key, byte[] imageData)
                : base(key)
            {
                m_ImageData = imageData;
            }

            public override uint GetSize()
            {
                return (uint)m_ImageData.Length;
            }

            public override void Write(BinaryWriter bw)
            {
                bw.Write(m_ImageData);
            }
        }

        private ImageRecord GetImageRecord(uint recordId, uint imageId)
        {
            ImageRecord result = null;
            Images.Image image = m_Images[imageId];
            Debug.Assert(image != null, "Image id " + imageId + " does not exist");
            string path = "";

            if (image != null)
            {
                Image icon = null;
                try
                {
                    
                    path = m_DirectoryPath + "\\pc\\" + image.Path;
                    icon = Image.FromFile(path);
                }
                catch
                {
                    Trace.WriteLine("No PC-specific version found in .\\pc\\" + image.Path);
                    try
                    {
                        path = m_DirectoryPath+ "\\" + image.Path;
                        icon = Image.FromFile(path);
                    }
                    catch
                    {
                        Trace.WriteLine("...image file not found: " + image.Path + "?");
                    }
                }

                int destWidth = 240, destHeight = 240;

                if (icon != null)
                {
                    //bool convert = (icon.Width != destWidth) || (icon.Height != destHeight);
                    bool convert = false;
                    if (convert == false)
                    {
                        // if conversion is not necessary, then copy the original file's bytes
                        // directly to avoid re-saving the image using C#'s library. It is
                        // assumed that the artist saved the file in an optimized format.
                        // Also, with lossy formats such as jpeg, re-saving causes generation loss.
                        FileInfo finfo = new FileInfo(path);
                        if (finfo.Exists)
                        {
                            byte[] bytes = File.ReadAllBytes(finfo.FullName);
                            result = new ImageRecord(recordId, bytes);
                        }
                    }
                    else
                    {
                        try
                        {
                            Bitmap bitmap = new Bitmap(destWidth, destHeight);
                            Graphics g = Graphics.FromImage(bitmap);
                            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                            g.DrawImage(icon, 0, 0, destWidth, destHeight);
                            g.Dispose();
                            icon = bitmap;

                            MemoryStream ms = new MemoryStream();
                            icon.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                            byte[] bytes = ms.GetBuffer();
                            result = new ImageRecord(recordId, bytes);
                            ms.Close();
                        }
                        catch
                        {
                            Trace.WriteLine("...bad image format for " + image.Path + "?");
                        }
                    }
                }
            }

            return result;
        }

        private void CreateAchievementImageTable(Database db)
        {
            Database.Table t = new Database.Table("Achievement_Images");

            IEnumerator entries = m_Achievements.GetEnumerator();
            entries.Reset();
            while (entries.MoveNext())
            {
                Achievements.Achievement a = (Achievements.Achievement)entries.Current;
                ImageRecord r = GetImageRecord(a.m_Prop.Id, a.imageId);
                if(r != null)
                {
                    t.AddRecord(r);
                }
            }

            db.AddTable(t);
        }

        public class UTF8StringRecord : Database.Table.Record
        {
            string m_String;

            public UTF8StringRecord(uint key, string str)
                : base(key)
            {
                m_String = str;
            }

            public override uint GetSize()
            {
                return (uint)System.Text.Encoding.UTF8.GetByteCount(m_String);
            }

            public override void Write(BinaryWriter bw)
            {
                // convert to a byte array first to avoid using BinaryWriter::Write(string)
                // which outputs strings with prepended length bytes
                byte[] bytes = System.Text.Encoding.UTF8.GetBytes(m_String);
                bw.Write(bytes);
            }
        }

        private void CreateAchievementStringTable(Database db, string locale)
        {
            Database.Table t = new Database.Table("Achievement_Strings_" + locale);

            IEnumerator entries = m_Achievements.GetEnumerator();
            entries.Reset();
            while (entries.MoveNext())
            {
                Achievements.Achievement a = (Achievements.Achievement)entries.Current;
                uint key = a.m_Prop.Id;

                string title = GetString(a.titleStringId, locale);
                string description = GetString(a.descriptionStringId, locale);
                string unachievedString = GetString(a.unachievedStringId, locale);

                t.AddRecord(new UTF8StringRecord((key * 3) - 2, title));
                t.AddRecord(new UTF8StringRecord((key * 3) - 1, description));
                t.AddRecord(new UTF8StringRecord((key * 3) - 0, unachievedString));
            }

            db.AddTable(t);
        }

        private void CreateAchievementsDatabase(Database db)
        {
            if ((m_Achievements.GetEnumerator() == null) ||
                (m_Achievements.GetCount() == 0))
            {
                return;
            }

            CreateAchievementMetaDataTable(db);

            CreateAchievementImageTable(db);

            IEnumerator locales = m_Strings.GetLocales();
            while (locales.MoveNext())
            {
                string locale = (string)locales.Current;
                CreateAchievementStringTable(db, locale);
            }
        }

        public void WriteDatabase(string fname)
        {
            FileInfo finfo = new FileInfo(fname);
            if (finfo.Exists && finfo.IsReadOnly)
            {
                Trace.WriteLine(String.Format("\n{0} is read-only", finfo.FullName));
            }
            else
            {
                FileStream stream = File.Create(finfo.FullName);

                Trace.WriteLine(String.Format("Writing to {0}...", finfo.FullName));

                EndianBinaryWriter bw = new EndianBinaryWriter(stream);

                m_Database.Write(bw);

                bw.Close();
            }
        }

        private void CreateTitleStringTable(Database db, string locale)
        {
            Database.Table t = new Database.Table("Title_Strings_" + locale);
            
            string titleName = GetString("X_STRINGID_TITLENAME", locale);
            uint id = PcMetadataWriter.atStringHash("TitleName", 0);

            t.AddRecord(new UTF8StringRecord(id, titleName));

            db.AddTable(t);
        }

        private void AddNamedImageToTable(Database.Table t, string imageName, string recordName)
        {
            Images.Image image = m_Images[imageName];
            if (image != null)
            {
                uint recordId = PcMetadataWriter.atStringHash(recordName, 0);
                ImageRecord r = GetImageRecord(recordId, image.id);
                if (r != null)
                {
                    t.AddRecord(r);
                }
            }
        }

        private void CreateTitleImageTable(Database db)
        {
            Database.Table t = new Database.Table("Title_Images");

            AddNamedImageToTable(t, "X_IMAGEID_GAME", "TitleImage");
            AddNamedImageToTable(t, "X_IMAGEID_GAME_MARKETPLACE", "TitleImage_Marketplace");
            AddNamedImageToTable(t, "X_IMAGEID_GAME_BKGND", "TitleImage_Background");
            AddNamedImageToTable(t, "X_IMAGEID_GAME_BOXART", "TitleImage_BoxArt");

            db.AddTable(t);
        }

        public void CreateTitleMetadataTable(Database db)
        {
            CreateTitleImageTable(db);

            IEnumerator locales = m_Strings.GetLocales();
            while (locales.MoveNext())
            {
                string locale = (string)locales.Current;
                CreateTitleStringTable(db, locale);
            }
        }

        public class RichPresenceTemplateRecord : UTF8StringRecord
        {
            public RichPresenceTemplateRecord(uint key, string str)
                : base(key, str)
            {
            }
        }

        private void CreateRichPresenceContextStringTable(Property prop, Database db, string locale)
        {
            if (!prop.HasValues())
            {
                return;
            }

            IDictionaryEnumerator values = prop.GetValues();

            if (null == values)
            {
                return;
            }

            // if this context was already encountered, then we already have a table for it
            string tableName = "Rich_Presence_Context_" + prop.Id + "_Strings_" + locale;
            if (db.DoesTableExist(tableName))
            {
                return;
            }

            Database.Table t = new Database.Table(tableName);

            while (values.MoveNext())
            {
                Property.PropertyValue propertyValue = (Property.PropertyValue)values.Value;
                uint val = propertyValue.GetValue();
                uint stringId = propertyValue.GetStringId();
                string str = GetString(stringId, locale);
                t.AddRecord(new UTF8StringRecord(val, str));
            }

            db.AddTable(t);
        }

        private void CreateRichPresenceTemplateTable(Database db, string locale)
        {
            Database.Table t = new Database.Table("Rich_Presence_Templates_" + locale);

            IEnumerator presences = m_Presences.GetEnumerator();
            while (presences.MoveNext())
            {
                Presence presence = (Presence)presences.Current;
                string template = presence.GetTemplateString(locale);
                t.AddRecord(new RichPresenceTemplateRecord(presence.GetId(), template));

                int fldCount = presence.GetFieldCount();

                if(fldCount > 0)
                {
                    for(int i = 0; i < fldCount; ++i)
                    {
                        Property prop = presence.GetField(i);

                        // constant property values are not allowed in a rich presence template
                        Debug.Assert(prop.IsConstant == false);

                        CreateRichPresenceContextStringTable(prop, db, locale);
                    }
                }
            }

            db.AddTable(t);
        }

        public class RichPresenceContextDefaultValueRecord : Database.Table.Record
        {
            uint m_Value;

            public RichPresenceContextDefaultValueRecord(uint key, uint value)
                : base(key)
            {
                m_Value = value;
            }

            public override uint GetSize()
            {
                return sizeof(uint);
            }

            public override void Write(BinaryWriter bw)
            {
                bw.Write(m_Value);
            }
        }

        private void CreateRichPresenceContextDefaultValuesTable(Database db)
        {
            Database.Table t = new Database.Table("Rich_Presence_Context_Default_Values");

            IEnumerator presences = m_Presences.GetEnumerator();
            while (presences.MoveNext())
            {
                Presence presence = (Presence)presences.Current;

                int fldCount = presence.GetFieldCount();

                if(fldCount > 0)
                {
                    for(int i = 0; i < fldCount; ++i)
                    {
                        Property prop = presence.GetField(i);

                        if (!prop.HasValues())
                        {
                            continue;
                        }

                        IDictionaryEnumerator values = prop.GetValues();

                        if (null == values)
                        {
                            continue;
                        }

                        if(t.DoesRecordExist(prop.Id))
                        {
                            continue;
                        }

                        // TODO: NS - add support for default context values if needed.
                        // For now I'm assuming games always set the value of each context
                        // before using a rich presence template.

                        uint defaultValue = 0; // prop.GetDefaultValue()
                        t.AddRecord(new RichPresenceContextDefaultValueRecord(prop.Id, defaultValue));
                    }
                }
            }

            db.AddTable(t);
        }

        public class RichPresencePropertyRecord : Database.Table.Record
        {
            uint m_Value;

            public RichPresencePropertyRecord(uint key, uint value)
                : base(key)
            {
                m_Value = value;
            }

            public override uint GetSize()
            {
                return sizeof(uint);
            }

            public override void Write(BinaryWriter bw)
            {
                bw.Write(m_Value);
            }
        }

        private void CreateRichPresencePropertiesTable(Database db)
        {
            Database.Table t = new Database.Table("Rich_Presence_Properties");

            IEnumerator presences = m_Presences.GetEnumerator();
            while (presences.MoveNext())
            {
                Presence presence = (Presence)presences.Current;

                int fldCount = presence.GetFieldCount();

                if (fldCount > 0)
                {
                    for (int i = 0; i < fldCount; ++i)
                    {
                        Property prop = presence.GetField(i);

                        if (prop.HasValues())
                        {
                            continue;
                        }

                        IDictionaryEnumerator values = prop.GetValues();

                        if (null != values)
                        {
                            continue;
                        }

                        if (t.DoesRecordExist(prop.Id))
                        {
                            continue;
                        }

                        uint type = prop.Type;

                        const uint XUSER_DATA_TYPE_INT32     = 1;
                        const uint XUSER_DATA_TYPE_INT64     = 2;
                        const uint XUSER_DATA_TYPE_DOUBLE    = 3;
                        const uint XUSER_DATA_TYPE_FLOAT     = 5;

                        switch (type)
                        {
                            case XUSER_DATA_TYPE_INT32:
                                break;
                            case XUSER_DATA_TYPE_INT64:
                                break;
                            case XUSER_DATA_TYPE_DOUBLE:
                                break;
                            case XUSER_DATA_TYPE_FLOAT:
                                break;
                            default:
                                Debug.Assert(false, "Property has an unknown type");
                                break;
                        }

                        t.AddRecord(new RichPresencePropertyRecord(prop.Id, prop.Type));
                    }
                }
            }

            db.AddTable(t);
        }

        public void CreateRichPresenceDatabase(Database db)
        {
            IEnumerator locales = m_Strings.GetLocales();
            while (locales.MoveNext())
            {
                string locale = (string)locales.Current;
                
                CreateRichPresenceTemplateTable(db, locale);                
            }

            CreateRichPresenceContextDefaultValuesTable(db);
            CreateRichPresencePropertiesTable(db);
        }

        public void CreateDatabase(string fname)
        {
            m_Database = new Database();

            CreateTitleMetadataTable(m_Database);

            CreateAchievementsDatabase(m_Database);

            CreateRichPresenceDatabase(m_Database);

            WriteDatabase(fname);
        }
    }
}
