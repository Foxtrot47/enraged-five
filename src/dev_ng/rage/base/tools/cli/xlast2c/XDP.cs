﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace xlast2c
{
    // Current implementation of this class is pretty sketch and outputs CSV files
    // that fit the formate required for the XDP Service Configuration documents.
    // When the actual XDP site is available most, if not all of this code is going in the garbage.
    class XDPWriter
    {
        Achievements m_Achievements;
        Images m_Images;
        LocalizedStringCollection m_Strings;
        PresenceCollection m_Presences;
        PropertyCollection m_Collection;

        public XDPWriter(Achievements ach, LocalizedStringCollection loc, Images im, PresenceCollection p, PropertyCollection c)
        {
            m_Achievements = ach;
            m_Strings = loc;
            m_Images = im;
            m_Presences = p;
            m_Collection = c;
        }

        public bool XboxOneAchievementTool(string xdkPath, string packagePath, string xb1TitleId, string xb1ServiceConfig)
        {
            if (!File.Exists(xdkPath))
            {
                Console.Error.WriteLine(String.Format("XDK Path: \"{0}\" did not exist.", xdkPath));
                return false;
            }

            if (!File.Exists(packagePath))
            {
                Console.Error.WriteLine(String.Format("Package Path: \"{0}\" did not exist.", packagePath));
                return false;
            }

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = "cmd.EXE";
            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.Arguments = String.Format("{0} XDK /c", xdkPath);

            Process p = Process.Start(startInfo);
            p.Close();

            return true;
        }
    }
}
