using System;
using System.Xml;
using System.Collections;
using System.Diagnostics;
using System.Collections.Generic;

namespace xlast2c
{

    //PURPOSE: Enum with all leaderboard types.
    enum eLeaderboardtypes
    {
        LEADERBOARD_TYPE_INVALID,
        LEADERBOARD_TYPE_PLAYER,
        LEADERBOARD_TYPE_CLAN
    };

    class Leaderboard : Schema
    {
        //Name of the leaderboard
        string m_Name;

        //Id of the leaderboard
        uint m_LeaderboardId;

        StatsRecord m_UpdateRecord;

        const uint RANK_FIELD_ID           = 0x10008001;
        const uint GAMER_NAME_FIELD_ID     = 0x40008002;

        public const uint X_SKILL_RANKED_LEADERBOARD_PREFIX     = 0xFFFF0000;
        public const uint X_SKILL_STD_LEADERBOARD_PREFIX        = 0xFFFE0000;
        public const uint X_RATING_COLUMN_ID                    = 0x0000FFFE;
        public const uint X_COLUMN_ID_SKILL                     = 0x0000003D;

        bool m_IsHidden         = false;
        bool m_IsArbitrated     = false;
        bool m_IsResetNever     = true;
        bool m_IsResetWeekly    = false;
        bool m_IsResetMonthly   = false;
        bool m_IsResetBimonthly = false;
        bool m_IsResetAnnually  = false;

        public class Column
        {
            public uint m_Id;
            public string m_Name;
            public string m_AggregationMethod;
            public Property m_UpdateProp;
            public Property m_LbProp;

            public bool IsRatingColumn()
            {
                return X_RATING_COLUMN_ID == m_Id;
            }
        }

        List<Column> m_Columns;

        public Leaderboard()
        {
            //Don't generate setter accessors
            m_GenerateSetters = false;
        }

        public bool IsHidden{get{return m_IsHidden;}}
        public bool IsArbitrated{get{return m_IsArbitrated;}}
        public bool IsResetNever{get{return m_IsResetNever;}}
        public bool IsResetWeekly{get{return m_IsResetWeekly;}}
        public bool IsResetMonthly{get{return m_IsResetMonthly;}}
        public bool IsResetBimonthly{get{return m_IsResetBimonthly;}}
        public bool IsResetAnnually{get{return m_IsResetAnnually;}}

        public List<Column> Columns{get{return m_Columns;}}

        public bool IsSkill
        {
            get
            {
                return X_SKILL_RANKED_LEADERBOARD_PREFIX == (m_LeaderboardId & X_SKILL_RANKED_LEADERBOARD_PREFIX)
                       || X_SKILL_STD_LEADERBOARD_PREFIX == (m_LeaderboardId & X_SKILL_STD_LEADERBOARD_PREFIX);
            }
        }

        public bool IsRankedSkill
        {
            get
            {
                return X_SKILL_RANKED_LEADERBOARD_PREFIX == (m_LeaderboardId & X_SKILL_RANKED_LEADERBOARD_PREFIX);
            }
        }

        protected internal void AddColumn(uint id,
                                            string name,
                                            string aggMethod,
                                            Property updateProp,
                                            Property lbProp)
        {
            Column col = new Column();
            col.m_Id = id;
            col.m_Name = name.Replace(" ", "_");
            col.m_AggregationMethod = "AGG_" + aggMethod.ToUpper();
            col.m_UpdateProp = updateProp;
            col.m_LbProp = lbProp;

            m_Columns.Add(col);
        }

        public string RawName{get{return m_Name;}}

        public uint LeaderboardId{get{return m_LeaderboardId;}}

        //PURPOSE
        //  Returns the C++ class name.
        public override string GetName()
        {
            return (this.IsSkill ? "Skill" : "") + "Leaderboard_" + m_Name.Replace(" ", "");
        }

        public void Reset(string name, uint leaderboardId)
        {
            m_Name = name;
            m_LeaderboardId = leaderboardId;
            m_Columns = new List<Column>();

            m_IsHidden         = false;
            m_IsArbitrated     = false;
            m_IsResetNever     = true;
            m_IsResetWeekly    = false;
            m_IsResetMonthly   = false;
            m_IsResetBimonthly = false;
            m_IsResetAnnually  = false;

            m_UpdateRecord = null;
        }

        //PURPOSE
        //  Extracts the properties that define the columns of a leaderboard.
        public void Reset(XmlElement statsView, PropertyCollection props)
        {
            string name = statsView.Attributes.GetNamedItem("friendlyName").Value;
            string strLbId = statsView.Attributes.GetNamedItem("id").Value;

            this.Reset(name, uint.Parse(strLbId));

            XmlNode hidden = statsView.Attributes.GetNamedItem("hidden");
            XmlNode arbitrated = statsView.Attributes.GetNamedItem("arbitrated");
            XmlNode resetType = statsView.Attributes.GetNamedItem("resetType");

            m_IsHidden          = (null != hidden) ? hidden.Value.ToLower() == "true" : false;
            m_IsArbitrated      = (null != arbitrated) ? arbitrated.Value.ToLower() == "true" : false;

            if(null != resetType)
            {
                m_IsResetNever      = resetType.Value.ToLower() == "never";
                m_IsResetWeekly     = resetType.Value.ToLower() == "weekly";
                m_IsResetMonthly    = resetType.Value.ToLower() == "monthly";
                m_IsResetBimonthly  = resetType.Value.ToLower() == "bimonthly";
                m_IsResetAnnually   = resetType.Value.ToLower() == "annually";
            }
            else
            {
                m_IsResetNever = true;
                m_IsResetWeekly = m_IsResetMonthly = m_IsResetBimonthly = m_IsResetAnnually = false;
            }

            XmlElement field = (XmlElement) statsView["Columns"].FirstChild;

            for(; null != field; field = (XmlElement) field.NextSibling)
            {
                XmlAttribute attr;

                XmlElement property = (XmlElement) field["Property"];

                if(null != property)
                {
                    attr = (XmlAttribute) property.Attributes.GetNamedItem("id");
                }
                else
                {
                    XmlElement ctx = (XmlElement) field["Context"];

                    attr = (XmlAttribute) ctx.Attributes.GetNamedItem("id");
                }

                Property updateProp = props[attr.Value];

                if(null != updateProp)
                {
                    //Rank and gamer name columns and do not appear as actual
                    //columns in a leaderboard query.  They appear instead as
                    //ancillary data in the structure returned from the query.
                    if(updateProp.Id != RANK_FIELD_ID && updateProp.Id != GAMER_NAME_FIELD_ID)
                    {
                        string colName = field.Attributes.GetNamedItem("friendlyName").Value;
                        string colIdStr = field.Attributes.GetNamedItem("attributeId").Value;
                        uint colId = Property.ParseUInt(colIdStr);
                        string colAggMethod = field["Property"]["Aggregation"].Attributes.GetNamedItem("type").Value;

                        //Mark that this property is referenced in a LB
                        updateProp.IsUsedInLeaderboard = true;

                        //We create a unique instance of the property
                        //because it is possible to have leaderboards
                        //with multiple columns containing the same property,
                        //e.g. if each uses a different aggregation type.
                        //This is the field id that is used to read values
                        //from the schema, as opposed to the column id which
                        //is used to request columns from the stats service.
                        Property lbProp = new Property();

                        this.AddColumn(colId, colName, colAggMethod, updateProp, lbProp);

                        lbProp.Reset(m_Columns[m_Columns.Count - 1].m_Name, updateProp.Id);
                        this.AddMember(lbProp);
                        
                    }
                }
            }

            if(!this.IsSkill)
            {
                m_UpdateRecord = new StatsRecord();
                m_UpdateRecord.Reset(this, statsView, props);
            }

            Trace.WriteLine("  " + m_Name);
        }

        public override string GetInitializerList()
        {
            return "";
        }

        public override string GetEnum()
        {
            string str = "";
            
            //str += base.GetEnum();

            str += String.Format("    static const unsigned LEADERBOARD_ID = 0x{0:x8};\n", m_LeaderboardId);
            str += String.Format("    static const unsigned INPUT_COUNT = {0};\n", (null != m_UpdateRecord) ? m_UpdateRecord.GetFieldCount() : 0);
            str += String.Format("    static const unsigned COLUMN_COUNT = {0};\n", m_Columns.Count);

            str += "    enum ColumnId\n    {\n";

            for(int i = 0; i < m_Columns.Count; ++i)
            {
                str += String.Format("        {0} = 0x{1:x8},\n",
                    m_Columns[i].m_LbProp.GetColumnIdName(),
                    m_Columns[i].m_Id);

                if(X_RATING_COLUMN_ID == m_Columns[i].m_Id
                    || (this.IsSkill && X_COLUMN_ID_SKILL == m_Columns[i].m_Id))
                {
                    str += String.Format("        RATING_COLUMN_ID = {0},\n",
                                        m_Columns[i].m_LbProp.GetColumnIdName());
                }
            }

            str += "    };\n";

            str += "    enum\n";
            str += "    {\n";
            str += String.Format("        IS_SKILL             = {0}\n", this.IsSkill ? "true," : "false,");
            str += String.Format("        IS_HIDDEN            = {0}\n", IsHidden ? "true," : "false,");
            str += String.Format("        IS_ARBITRATED        = {0}\n", IsArbitrated ? "true," : "false,");
            str += String.Format("        IS_RESET_NEVER       = {0}\n", IsResetNever ? "true," : "false,");
            str += String.Format("        IS_RESET_WEEKLY      = {0}\n", IsResetWeekly ? "true," : "false,");
            str += String.Format("        IS_RESET_MONTHLY     = {0}\n", IsResetMonthly ? "true," : "false,");
            str += String.Format("        IS_RESET_BIMONTHLY   = {0}\n", IsResetBimonthly ? "true," : "false,");
            str += String.Format("        IS_RESET_ANNUALLY    = {0}\n", IsResetAnnually ? "true," : "false,");
            str += "    };\n";

            return str;
        }

        protected override string GetFieldIndexAccessor()
        {
            //Leaderboards don't use fields.  They use columns.
            //Leaderboard updates use fields.  The distinction is that
            //a single update field can affect the values of multiple columns.
            return "";
        }

        protected override string GetFieldIdAccessor()
        {
            //Leaderboards don't use fields.
            return "";
        }

        protected override string GetFieldIdArrayAccessor()
        {
            //Leaderboards don't use fields.
            return "";
        }

        protected override string GetFieldTypeAccessor()
        {
            //Leaderboards don't use fields.
            return "";
        }

        protected override string GetFieldSizeAccessor()
        {
            //Leaderboards don't use fields.
            return "";
        }

        public override string GetAccessors()
        {
            string str = "";

            //GetInputIds()
            str += "    static const unsigned* GetInputIds()\n";
            str += "    {\n";
            if(null != m_UpdateRecord)
            {
                str += "        static const unsigned s_InputIds[] = {";
                for(int i = 0; i < m_UpdateRecord.GetFieldCount(); ++i)
                {
                    Property prop = m_UpdateRecord.GetField(i);
                    str += String.Format("{0},", prop.GetFieldIdName());
                }
                str += "};\n";
                str += "        return s_InputIds;\n";
            }
            else
            {
                if(this.IsSkill)
                {
                    str += "        //This is a skill leaderboard, thus no inputs\n";
                }
                str += "        return 0;\n";
            }
            str += "    }\n";

            //GetColumnIds()
            str += "    static const unsigned* GetColumnIds()\n";
            str += "    {\n";
            str += "        static const unsigned s_ColumnIds[] = {";
            for(int i = 0; i < m_Columns.Count; ++i)
            {
                str += String.Format("{0},", m_Columns[i].m_LbProp.GetColumnIdName());
            }
            str += "};\n";
            str += "        return s_ColumnIds;\n";
            str += "    }\n";

            //GetColumnInputIds()
            str += "    static const unsigned* GetColumnInputIds()\n";
            str += "    {\n";
            if(null != m_UpdateRecord)
            {
                str += "        static const unsigned s_ColumnInputIds[] = {";
                for(int i = 0; i < m_Columns.Count; ++i)
                {
                    str += String.Format("{0},", m_Columns[i].m_UpdateProp.GetFieldIdName());
                }
                str += "};\n";
                str += "        return s_ColumnInputIds;\n";
            }
            else
            {
                if(this.IsSkill)
                {
                    str += "        //This is a skill leaderboard, thus no inputs\n";
                }
                str += "        return 0;\n";
            }
            str += "    }\n";

            //str += base.GetAccessors();

            //GetLeaderboardName()
            str += "    static const char* GetLeaderboardName()\n";
            str += "    {\n";
            str += String.Format("        return \"{0}\";\n", this.GetName());
            str += "    }\n";

            return str;
        }

        public override string GetFieldDeclarations()
        {
            //Leaderboards don't use fields.
            return "";
        }

        public override string GetInnerClasses()
        {
            string str = base.GetInnerClasses();

            /*if(!this.IsSkill)
            {
                str += "//Used to write updates to the leaderboard\n"
                        + m_UpdateRecord.ToCString();
            }*/

            return str;
        }
    }

    //PURPOSE
    //  A collection of leaderboards.
    class LeaderboardCollection
    {
        //List of leaderboards.
        ArrayList m_Leaderboards;

        //This collection is for what type of leaderboards
        eLeaderboardtypes m_LeaderboardType;

        public LeaderboardCollection(eLeaderboardtypes leaderboardType)
        {
            m_LeaderboardType = leaderboardType;
        }

        //PURPOSE
        //  Access to leaderboards. Data will only be valid after Reset() is called.
        public ArrayList GetLeaderboards() { return m_Leaderboards; }

        //PURPOSE
        //  Collects leaderboards from the "StatsViews" section of an xlast
        //  document.
        public void Reset(XmlDocument doc, PropertyCollection props)
        {
            Trace.WriteLine("Collecting leaderboards...");

            XmlElement proj = doc.DocumentElement["GameConfigProject"];
            XmlElement statsViews = null != proj ? proj["StatsViews"] : null;
            XmlElement el = null != statsViews ? (XmlElement) statsViews.FirstChild : null;

            //Build the leaderboards

            m_Leaderboards = new ArrayList();

            for(; null != el; el = (XmlElement) el.NextSibling)
            {
                Leaderboard lb = new Leaderboard();

                lb.Reset(el, props);

                m_Leaderboards.Add(lb);
            }

            //Add skill leaderboards

            Property gmProp = props.FindByName("GAME_MODE");

            IDictionaryEnumerator values = gmProp.GetValues();

            Property skillSkillProp = new Property();
            Property skillGamesPlayedProp = new Property();
            Property skillMuProp = new Property();
            Property skillSigmaProp = new Property();

            //From xbox.h
            uint X_STATS_COLUMN_SKILL_SKILL        = 61;
            uint X_STATS_COLUMN_SKILL_GAMESPLAYED  = 62;
            uint X_STATS_COLUMN_SKILL_MU           = 63;
            uint X_STATS_COLUMN_SKILL_SIGMA        = 64;

            uint FIELD_ID_SKILL_SKILL         = DataTypes.MakePropertyId(DataTypes.XUSER_DATA_TYPE_INT64, 1);
            uint FIELD_ID_SKILL_GAMESPLAYED   = DataTypes.MakePropertyId(DataTypes.XUSER_DATA_TYPE_INT64, 2);
            uint FIELD_ID_SKILL_MU            = DataTypes.MakePropertyId(DataTypes.XUSER_DATA_TYPE_DOUBLE, 3);
            uint FIELD_ID_SKILL_SIGMA         = DataTypes.MakePropertyId(DataTypes.XUSER_DATA_TYPE_DOUBLE, 4);

            skillSkillProp.Reset("Skill", FIELD_ID_SKILL_SKILL);
            skillGamesPlayedProp.Reset("GamesPlayed", FIELD_ID_SKILL_GAMESPLAYED);
            skillMuProp.Reset("Mu", FIELD_ID_SKILL_MU);
            skillSigmaProp.Reset("Sigma", FIELD_ID_SKILL_SIGMA);

            while(values != null && values.MoveNext())
            {
                //string name = ((string) values.Key).Replace(" ", "_").ToUpper();
                string name = (string) values.Key;
                Property.PropertyValue propertyValue = (Property.PropertyValue)values.Value;
                uint id = propertyValue.GetValue();
                uint rankedId = id | Leaderboard.X_SKILL_RANKED_LEADERBOARD_PREFIX;
                uint stdId = id | Leaderboard.X_SKILL_STD_LEADERBOARD_PREFIX;

                Leaderboard rankedLb = new Leaderboard();
                Leaderboard stdLb = new Leaderboard();
                rankedLb.Reset(name + "Ranked", rankedId);
                stdLb.Reset(name + "Standard", stdId);

                rankedLb.AddMember(skillSkillProp);
                rankedLb.AddColumn(X_STATS_COLUMN_SKILL_SKILL, "SKILL", "Last", skillSkillProp, skillSkillProp);
                rankedLb.AddMember(skillGamesPlayedProp);
                rankedLb.AddColumn(X_STATS_COLUMN_SKILL_GAMESPLAYED, "GAMESPLAYED", "Sum", skillGamesPlayedProp, skillGamesPlayedProp);
                rankedLb.AddMember(skillMuProp);
                rankedLb.AddColumn(X_STATS_COLUMN_SKILL_MU, "MU", "Last", skillMuProp, skillMuProp);
                rankedLb.AddMember(skillSigmaProp);
                rankedLb.AddColumn(X_STATS_COLUMN_SKILL_SIGMA, "SIGMA", "Last", skillSigmaProp, skillSigmaProp);

                stdLb.AddMember(skillSkillProp);
                stdLb.AddColumn(X_STATS_COLUMN_SKILL_SKILL, "SKILL", "Last", skillSkillProp, skillSkillProp);
                stdLb.AddMember(skillGamesPlayedProp);
                stdLb.AddColumn(X_STATS_COLUMN_SKILL_GAMESPLAYED, "GAMESPLAYED", "Sum", skillGamesPlayedProp, skillGamesPlayedProp);
                stdLb.AddMember(skillMuProp);
                stdLb.AddColumn(X_STATS_COLUMN_SKILL_MU, "MU", "Last", skillMuProp, skillMuProp);
                stdLb.AddMember(skillSigmaProp);
                stdLb.AddColumn(X_STATS_COLUMN_SKILL_SIGMA, "SIGMA", "Last", skillSigmaProp, skillSigmaProp);

                m_Leaderboards.Add(rankedLb);
                m_Leaderboards.Add(stdLb);
            }
        }

        //PURPOSE
        //  Generates (in string form) the C++ class representation
        //  of all leaderboards.
        public virtual string ToCString()
        {
            string str = "";

            for (int i = 0; i < m_Leaderboards.Count; ++i)
            {
                str = str + ((Leaderboard)m_Leaderboards[i]).ToCString();

                if (i < m_Leaderboards.Count - 1)
                {
                    str += "\n\n";
                }
            }

            str = str
                + "\nstruct LeaderboardInfo\n"
                + "{\n"
                + "    LeaderboardInfo()\n"
                + "    : LeaderBoardId(-1)\n"
                + "    , NumInputs(0)\n"
                + "    , InputIds(0)\n"
                + "    , NumColumns(0)\n"
                + "    , ColumnIds(0)\n"
                + "    , ColumnInputIds(0)\n"
                + "    {}\n"
                + "    int GetColumnIndex(const unsigned columnId) const\n"
                + "    {\n"
                + "        for(int i = 0; i < (int)NumColumns; ++i)\n"
                + "        {\n"
                + "            if(columnId == ColumnIds[i]){return i;}\n"
                + "        }\n"
                + "        return -1;\n"
                + "    }\n"
                + "    int LeaderBoardId;\n"
                + "    unsigned NumInputs;\n"
                + "    const unsigned* InputIds;\n"
                + "    unsigned NumColumns;\n"
                + "    const unsigned* ColumnIds;\n"
                + "    const unsigned* ColumnInputIds;\n"
                + "    static bool GetLeaderboardInfo(const int lbId, LeaderboardInfo* lbInfo)\n"
                + "    {\n"
                + "        switch(lbId)\n"
                + "        {\n";
            for (int i = 0; i < m_Leaderboards.Count; ++i)
            {
                Leaderboard lb = (Leaderboard)m_Leaderboards[i];
                str = str
                    + "        case " + lb.GetName() + "::LEADERBOARD_ID:\n"
                    + "            lbInfo->LeaderBoardId = " + lb.GetName() + "::LEADERBOARD_ID;\n"
                    + "            lbInfo->NumInputs = " + lb.GetName() + "::INPUT_COUNT;\n"
                    + "            lbInfo->InputIds = " + lb.GetName() + "::GetInputIds();\n"
                    + "            lbInfo->NumColumns = " + lb.GetName() + "::COLUMN_COUNT;\n"
                    + "            lbInfo->ColumnIds = " + lb.GetName() + "::GetColumnIds();\n"
                    + "            lbInfo->ColumnInputIds = " + lb.GetName() + "::GetColumnInputIds();\n"
                    + "            return true;\n";
            }
            str = str
                + "        }\n"
                + "        return false;\n"
                + "    }\n"
                + "};\n";

            return str;
        }

        //PURPOSE
        //  Generates (in string form) the C++ class representation
        //  of all leaderboard schemas.
        public virtual string ToCUtilString()
        {
            string str = "";

            str = str
                + "\nclass LeaderboardUtil\n"
                + "{\n"
                + "public:\n"
                + "      \n";
                
                /*
                + "    \n"
                + "    enum LeaderboardType\n"
                + "    {\n";
            str += String.Format("        LEADERBOARD_TYPE_INVALID = 0,\n");
            str += String.Format("        LEADERBOARD_TYPE_PLAYER,\n");
            str += String.Format("        LEADERBOARD_TYPE_CLAN\n")
                + "    };\n"
                 */
                

            str += "    enum LeaderboardIds\n"
                + "    {\n";
            int countOfSkill = 0;
            for (int i = 0; i < m_Leaderboards.Count; ++i)
            {
                Leaderboard lb = (Leaderboard)m_Leaderboards[i];

                if (lb.IsSkill)
                {
                    countOfSkill++;
                    continue;
                }

                string name = lb.GetName().Replace(" ", "_").ToUpper();
                str += String.Format("        {0} = {1},\n", name, lb.LeaderboardId);
            }
            str = str
                + "        LEADERBOARDS_COUNT = " + ((int)m_Leaderboards.Count - countOfSkill) + "\n"
                + "    };\n"
                + "    \n"
                + "    \n"
                + "    static unsigned GetLeaderboardCount() { return LEADERBOARDS_COUNT; }\n"
                + "    \n"
                + "    \n"
                + "    static const char* GetLeaderboardName(const int lbId)\n"
                + "    {\n"
                + "        switch(lbId)\n"
                + "        {\n";
            for (int i = 0; i < m_Leaderboards.Count; ++i)
            {
                Leaderboard lb = (Leaderboard)m_Leaderboards[i];

                if (lb.IsSkill)
                {
                    continue;
                }

                string name = lb.GetName().Replace(" ", "_").ToUpper();
                str += String.Format("        case {0}: return \"{1}\";\n", name, lb.GetName());
            }
            str = str
                + "        }\n"
                + "        Assert(0);\n"
                + "        return \"UNKNOWN\";\n"
                + "    }\n"
                + "    \n"
                + "    \n"
                + "    static int IndexToId(const int index)\n"
                + "    {\n"
                + "        switch(index)\n"
                + "        {\n";

            for (int i = 0; i < m_Leaderboards.Count; ++i)
            {
                Leaderboard lb = (Leaderboard)m_Leaderboards[i];

                if (lb.IsSkill)
                {
                    continue;
                }

                string name = lb.GetName().Replace(" ", "_").ToUpper();
                str += String.Format("        case {0}: return {1};\n", i, name);
            }
            str = str
                + "        }\n"
                + "        Assert(0);\n"
                + "        return 0;\n"
                + "    }\n"
                + "    \n"
                + "    \n"
                + "    static bool GetIsValid(const int lbId)\n"
                + "    {\n"
                + "        switch(lbId)\n"
                + "        {\n";
                for (int i = 0; i < m_Leaderboards.Count; ++i)
                {
                    Leaderboard lb = (Leaderboard)m_Leaderboards[i];

                    if (lb.IsSkill)
                    {
                        continue;
                    }

                    string name = lb.GetName().Replace(" ", "_").ToUpper();
                    str += String.Format("        case {0}: return true;\n", name, lb.GetName());
                }
                str = str
                + "        }\n"
                + "        return false;\n"
                + "    }\n"
                + "};\n";

            return str;
        }

        //PURPOSE
        //  Generates (in string form) the C++ class representation
        //  of all leaderboards.
        public virtual string ToEnumString()
        {
            string str = "";

            bool clanLeaderboard = (m_LeaderboardType == xlast2c.eLeaderboardtypes.LEADERBOARD_TYPE_CLAN);


            str = str
                + "    \n"
                + "USING \"commands_misc.sch\""
                + "    \n"
                + "    \n";

            if (!clanLeaderboard)
            {
                str = str

                    + "    \n"
                    + "    //PURPOSE\n"
                    + "    //   Enum with all leaderboard types.\n"
                    + "    ENUM LEADERBOARDS_TYPES_ENUM\n";
                str += String.Format("        LEADERBOARD_TYPE_INVALID = 0,\n");
                str += String.Format("        LEADERBOARD_TYPE_PLAYER,\n");
                str += String.Format("        LEADERBOARD_TYPE_GROUP,\n");
                str += String.Format("        LEADERBOARD_TYPE_GROUP_MEMBER\n");
                str += "    ENDENUM\n";
                str += "    \n";
            }

            str += "    \n"
                + "    //PURPOSE\n";

            str += "    //   Enum with all leaderboard ids.\n";
            str += "    ENUM ";
            if (clanLeaderboard) str += "CLAN_";
            str += "LEADERBOARDS_ENUM\n";


            int countofskill = 0;
            for (int i = 0; i < m_Leaderboards.Count; ++i)
            {
                Leaderboard lb = (Leaderboard)m_Leaderboards[i];

                if (lb.IsSkill)
                {
                    countofskill++;
                    continue;
                }
                string name = lb.GetName().Replace(" ", "_").ToUpper();

                if (clanLeaderboard)
                    str += String.Format("        CLAN_{0} = {1},\n", name, lb.LeaderboardId);
                else
                    str += String.Format("        {0} = {1},\n", name, lb.LeaderboardId);
            }

            str += "        ";
            if (clanLeaderboard) str += "CLAN_";
            str += String.Format("LEADERBOARDS_COUNT = {0},\n", m_Leaderboards.Count - countofskill);

            str += "        ";
            if (clanLeaderboard) str += "CLAN_";
            str += String.Format("LEADERBOARDS_INVALID = 0\n", m_Leaderboards.Count - countofskill);

            str += "    ENDENUM\n"
                + "    \n"
                + "    \n"
                + "\t//PURPOSE\n";

            str += "\t//   Returns the leaderboard name.\n";

            if (clanLeaderboard)
                str += "\tFUNC STRING GET_CLAN_LEADERBOARD_NAME(CLAN_LEADERBOARDS_ENUM lbId)\n";
            else
                str += "\tFUNC STRING GET_LEADERBOARD_NAME(LEADERBOARDS_ENUM lbId)\n";

            str += "    \n"
                + "\t\tSWITCH lbId\n";

            for (int i = 0; i < m_Leaderboards.Count; ++i)
            {
                Leaderboard lb = (Leaderboard)m_Leaderboards[i];

                if (lb.IsSkill)
                {
                    continue;
                }

                string name = lb.GetName().Replace(" ", "_").ToUpper();

                if (clanLeaderboard)
                    str += String.Format("\t\t\tCASE CLAN_{0} RETURN \"Clan_{1}\"\n", name, lb.GetName());
                else
                    str += String.Format("\t\t\tCASE {0} RETURN \"{1}\"\n", name, lb.GetName());
            }
            str = str
                + "\t\tENDSWITCH\n"
                + "\n"
                 + "\t\tRETURN \"UNKNOWN\"\n"
                + "\tENDFUNC\n"
                + "\n"

                +"    \n"
                + "    \n"
                + "\t//PURPOSE\n";
            str += "\t//   Returns the leaderboard Id by index.\n";

            if (clanLeaderboard)
                str += "\tFUNC INT GET_CLAN_LEADERBOARD_ID_BY_INDEX(INT index)\n";
            else
                str += "\tFUNC INT GET_LEADERBOARD_ID_BY_INDEX(INT index)\n";

            str += "    \n"
                + "\t\tSWITCH index\n";

            for (int i = 0; i < m_Leaderboards.Count; ++i)
            {
                Leaderboard lb = (Leaderboard)m_Leaderboards[i];

                if (lb.IsSkill)
                {
                    continue;
                }

                string name = lb.GetName().Replace(" ", "_").ToUpper();

                if (clanLeaderboard)
                    str += String.Format("\t\t\tCASE {0} RETURN CLAN_{1}\n", i, name);
                else
                    str += String.Format("\t\t\tCASE {0} RETURN {1}\n", i, name);
            }
            str = str
                + "\t\tENDSWITCH\n"
                + "\n"

                + "\t\tRETURN ";
                if (clanLeaderboard) str += "CLAN_";
                str += String.Format("LEADERBOARDS_INVALID\n", m_Leaderboards.Count - countofskill);

                str = str
                + "\tENDFUNC\n"
                + "\n";

            //
            str += "\t//PURPOSE\n"
                + "\t//   Returns the leaderboard enum given the game.\n";

            if (clanLeaderboard)
                str += "\tFUNC LEADERBOARDS_ENUM GET_CLAN_LEADERBOARD_ENUM_BY_NAME(STRING lbName)\n";
            else
                str += "\tFUNC LEADERBOARDS_ENUM GET_LEADERBOARD_ENUM_BY_NAME(STRING lbName)\n";

            str += "\n"
                + "\t\tINT hashedLbName =  GET_HASH_KEY(lbName)\n";
            for (int i = 0; i < m_Leaderboards.Count; ++i)
            {
                Leaderboard lb = (Leaderboard)m_Leaderboards[i];

                if (lb.IsSkill)
                {
                    continue;
                }

                string name = lb.GetName().Replace(" ", "_").ToUpper();
                if (clanLeaderboard)
                    name = "CLAN_" + name;

                string IfSTring = "ELIF";
                if (i == 0)
                {
                    IfSTring = "IF";
                }
                if (clanLeaderboard)
                    str += String.Format("\t\t{2} GET_HASH_KEY(\"Clan_{0}\") = hashedLbName\n\t\t\tRETURN {1}\n", lb.GetName(), name, IfSTring);
                else
                    str += String.Format("\t\t{2} GET_HASH_KEY(\"{0}\") = hashedLbName\n\t\t\tRETURN {1}\n", lb.GetName(), name, IfSTring);
            }
            str = str
                + "\t\tENDIF\n"
                + "\n"
                + "\t\tRETURN ";
                
                if (clanLeaderboard) str += "CLAN_";
                str += String.Format("LEADERBOARDS_INVALID\n", m_Leaderboards.Count - countofskill);
                
                str += "\tENDFUNC\n"
                + "\n";

            return str;
        }

        public string ToSummary()
        {
            string str = "\n\n\n/*";
            str += "\n";
            for (int i = 0; i < m_Leaderboards.Count; ++i)
            {
                Leaderboard lb = (Leaderboard)m_Leaderboards[i];

                if (lb.IsSkill)
                {
                    continue;
                }

                str += "---------------------------------------------------------\n";
                str += "Name: " + lb.GetName().Replace(" ", "_").ToUpper() + "\n"
                    + "ID: " + lb.LeaderboardId + "\n"
                    + "Num Cols: " + lb.Columns.Count + "\n"
                    + "Column Info: \n";

                for (int iCol = 0; iCol < lb.Columns.Count; iCol++)
                {
                    Leaderboard.Column col = lb.Columns[iCol];
                    string colName = "";
                    if (col.IsRatingColumn())
                    {
                        colName = "*";
                    }
                    colName += col.m_Name;
                    str += "    " + colName + "( " + col.m_AggregationMethod + " ) : " +
                                col.m_UpdateProp.GetColumnIdName() + " - " + DataTypes.GetColumnTypeStrFromType(col.m_UpdateProp.Type, m_LeaderboardType) + "\n";
                }

                str += "\n\n";
            }

            str += "*/";

            return str;
        }
    }
}
