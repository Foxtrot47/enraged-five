using System;
using System.Xml;
using System.Collections;
using System.Diagnostics;

namespace xlast2c
{
    //PURPOSE
    //  Contains the properties that define a matching attributes schema.
    class MatchingAttributes : Schema
    {
        private ArrayList m_GameModes = new ArrayList();

        static public string GetNameStatic()
        {
            return "MatchingAttributes";
        }

        //PURPOSE
        //  Returns the C++ class name.
        public override string GetName()
        {
            return GetNameStatic();
        }

        //PURPOSE
        //  Extracts the properties that define a matching attributes schema.
        public void Reset(XmlDocument doc, PropertyCollection props)
        {
            Trace.WriteLine("Building matching attributes schema...");

            XmlElement proj = doc.DocumentElement["GameConfigProject"];
            XmlElement matchmaking = null != proj ? proj["Matchmaking"] : null;
            XmlElement schema = null != matchmaking ? matchmaking["Schema"] : null;
            XmlElement sch_item = null != schema ? (XmlElement) schema.FirstChild : null;

            //Build the matching schema

            for(; null != sch_item; sch_item = (XmlElement) sch_item.NextSibling)
            {
                XmlAttribute attr =
                    (XmlAttribute) sch_item.Attributes.GetNamedItem("id");

                Property prop = props[attr.Value];

                if(null != prop && DataTypes.X_CONTEXT_GAME_MODE_ID == prop.Id)
                {
                    m_GameModes.Add(prop);
                }
                else
                {
                    this.AddMember(prop);
                }
            }
        }

        public override string GetCValuesEnum()
        {
            string str = base.GetCValuesEnum();

            for (int i = 0; i < m_GameModes.Count; ++i)
            {
                Property prop = (Property)m_GameModes[i];

                if (!prop.HasValues())
                {
                    continue;
                }

                IDictionaryEnumerator values = prop.GetValues();

                if (null == values)
                {
                    continue;
                }

                string enumName = prop.Name.Replace(" ", "");

                str += String.Format("    enum {0}\n    {{\n", enumName);

                while (values.MoveNext())
                {
                    string valName =
                        prop.Name.Replace(" ", "_").ToUpper() + "_" +
                        ((string)values.Key).Replace(" ", "_").ToUpper();

                    Property.PropertyValue propertyValue = (Property.PropertyValue)values.Value;

                    uint val = propertyValue.GetValue();

                    str += String.Format("        {0} = {1},\n", valName, val);
                }

                str += "    };\n";
            }

            return str;
        }


        public override string GetScriptValuesEnum()
        {
            string str = base.GetScriptValuesEnum();

            for (int i = 0; i < m_GameModes.Count; ++i)
            {
                Property prop = (Property)m_GameModes[i];

                if (!prop.HasValues())
                {
                    continue;
                }

                IDictionaryEnumerator values = prop.GetValues();

                if (null == values)
                {
                    continue;
                }

                string enumName = prop.Name.Replace(" ", "");
                enumName = enumName.Replace("_", "");

                str += "//PURPOSE\n";
                str += "//   Enum with all gamemode ids.\n";
                str += String.Format("ENUM " + enumName + "_ENUM\n");

                int count = 0;
                while (values.MoveNext())
                {
                    Property.PropertyValue propertyValue = (Property.PropertyValue)values.Value;

                    uint val = propertyValue.GetValue();

                    string valName = prop.Name.Replace(" ", "").ToUpper();
                    valName = valName.Replace("_", "") +"_" + val;

                    str += String.Format("        {0} = {1},\n", valName, val);

                    count++;
                }

                str += String.Format("        " + enumName + "_COUNT = {0}\n", count);

                str += "ENDENUM\n";
            }

            return str;
        }
    }
}
