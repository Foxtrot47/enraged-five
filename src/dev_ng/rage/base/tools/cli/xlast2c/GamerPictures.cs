using System;
using System.Xml;
using System.Collections;
using System.Diagnostics;

namespace xlast2c
{
    //PURPOSE
    //  Collection of XLAST gamer pictures.
    //  Each gamer picture is internally contained in a Property object.
    class GamerPictures
    {
        //Table of name/picture pairs
        Hashtable m_htGamerPictures;

        public void Reset(XmlDocument doc)
        {
            Trace.WriteLine("Collecting Gamerpictures...");

            //Traverse to the "GamerPictures" section.

            XmlElement proj = doc.DocumentElement["GameConfigProject"];
            XmlElement items = null != proj ? proj["GamerPictures"] : null;
            XmlElement el = null != items ? (XmlElement) items.FirstChild : null;

            m_htGamerPictures = new Hashtable();

            for(; null != el; el = (XmlElement) el.NextSibling)
            {
                Property prop = new Property();

                prop.Reset(el);

                m_htGamerPictures.Add(prop.Id, prop);

                Trace.WriteLine("  " + prop.Name);
            }
        }

        //PURPOSE
        //  Indexes GamerPictures by integer id.
        public Property this[int id]
        {
            get
            {
                return (Property)m_htGamerPictures[id];
            }
        }

        //PURPOSE
        //  Indexes GamerPictures by string id.
        //  The string is converted to integer.
        public Property this[string strId]
        {
            get
            {
                uint id = Property.ParseUInt(strId);

                return (Property) m_htGamerPictures[id];
            }
        }

        //PURPOSE
        //  Converts the GamerPictures to a string representation
        //  of a C enum.
        public virtual string ToCString()
        {
            string str = "";

            str += "class GamerPictures\n{\n";
            str += "public:\n";

            str += "    enum\n    {\n";
            str += String.Format("        COUNT = {0},\n", m_htGamerPictures.Count);
            str += "    };\n";

            if(m_htGamerPictures.Count > 0)
            {
                str += "    enum\n    {\n";

                IDictionaryEnumerator entries = m_htGamerPictures.GetEnumerator();

                while (entries.MoveNext())
                {
                    Property prop = (Property)entries.Value;

                    string name = prop.Name.Replace(" ", "_").ToUpper();

                    str += String.Format("        {0} = {1},\n", name, prop.Id);
                }

                str += "    };\n";
            }
            str += "};\n";

            return str;
        }

        //PURPOSE
        //  Converts the GamerPictures to a string representation
        //  of a C enum.
        public virtual string ToRageScriptString()
        {
            string str = "";

            str += "ENUM GAMERPICTURES\n";

            str += String.Format("    GAMERPICTURE_NUM_GAMERPICTURES = {0},\n", m_htGamerPictures.Count);

            IDictionaryEnumerator entries = m_htGamerPictures.GetEnumerator();

            for (int i = 0; i < m_htGamerPictures.Count; ++i)
            {
                entries.MoveNext();

                Property prop = (Property)entries.Value;

                string name = prop.Name.Replace(" ", "_").ToUpper();

                str += String.Format("    GAMERPICTURE_{0} = {1}", name, prop.Id);
                if (i < m_htGamerPictures.Count - 1)
                {
                    str += ",\n";
                }
                else
                {
                    str += "\n";
                }
            }

            str += "ENDENUM\n";

            return str;
        }
    }
}
