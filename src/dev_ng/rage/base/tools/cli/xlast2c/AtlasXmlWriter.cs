using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Xml;

namespace xlast2c
{
    enum UsedForRating
    {
        YES,
        NO
    };

    enum AtlasType
    {
        INVALID,        
        Byte,
        Int,
        Int64,
        Float,
        Short,
        String
    };

    class AtlasTypeConverter
    {
        static public string ToStr(AtlasType type)
        {
            switch (type)
            {
                case AtlasType.Byte: return "byte";
                case AtlasType.Float: return "float";
                case AtlasType.Int: return "int";
                case AtlasType.Int64: return "int64";
                case AtlasType.Short: return "short";
                case AtlasType.String: return "string";

                case AtlasType.INVALID:
                default:
                    return "INVALID";
            }
        }

        static public AtlasType FromStr(string s)
        {
            foreach (AtlasType t in Enum.GetValues(typeof(AtlasType)))
            {
                if (s == AtlasTypeConverter.ToStr(t)) return t;
            }

            return AtlasType.INVALID;
        }

        //NOTE: Atlas cannot store unsigned values, so apps will need to cast as necessary.
        static public AtlasType FromCType(string ctype)
        {
            if (ctype == "double") return AtlasType.Float;
            if (ctype == "float") return AtlasType.Float;

            if (ctype == "rage::i8") return AtlasType.Byte;
            if (ctype == "rage::u8") return AtlasType.Byte;

            if (ctype == "rage::i16") return AtlasType.Short;
            if (ctype == "rage::u16") return AtlasType.Short;

            if (ctype == "rage::i32") return AtlasType.Int;
            if (ctype == "rage::u32") return AtlasType.Int;

            if (AtlasXmlWriter.m_NoInt64)
            {
                if (ctype == "rage::i64") return AtlasType.Int;
                if (ctype == "rage::u64") return AtlasType.Int;
            }
            else
            {
                if (ctype == "rage::i64") return AtlasType.Int64;
                if (ctype == "rage::u64") return AtlasType.Int64;
            }

            return AtlasType.INVALID;
        }
    }

    class AtlasKey
    {
        int m_Id = 0;
        string m_Name = "";
        AtlasType m_Type = AtlasType.INVALID;
        string m_Desc = "";

        //Ctor used when reading from XML
        public AtlasKey(XmlElement el)
        {
            m_Id = int.Parse(el.GetAttribute("id"));
            m_Name = el.GetAttribute("name");
            m_Type = AtlasTypeConverter.FromStr(el.GetAttribute("type"));
            m_Desc = el.GetAttribute("desc");
        }

        //Ctor used when creating manually
        public AtlasKey(string name,
                        AtlasType type,
                        string lbName,
                        string friendlyName)
        {
            m_Id = 0; //0 is unknown; the ID will be assigned as the key is added to our list
            m_Name = name;
            m_Type = type;
            m_Desc = String.Format("{0}[{1}]", lbName, friendlyName);
        }

        public int GetId() { return m_Id; }
        public void SetId(int id) { m_Id = id; }
        public string GetName() { return m_Name; }
        public AtlasType GetAtlasType() { return m_Type; }
        public string GetDesc() { return m_Desc; }
    }

    class AtlasStat
    {
        public enum StatCategory
        {
            Static,
            Player
        };

        int m_Id = 0;
        string m_Name = "";
        AtlasType m_Type = AtlasType.INVALID;
        StatCategory m_Category = StatCategory.Player;
        string m_Desc = "";
        string m_Value = null;

        //Ctor used when reading from XML
        public AtlasStat(XmlElement el)
        {
            m_Id = int.Parse(el.GetAttribute("id"));
            m_Name = el.GetAttribute("name");
            m_Type = AtlasTypeConverter.FromStr(el.GetAttribute("type"));
            m_Category = (el.GetAttribute("category") == "Player") ? StatCategory.Player : StatCategory.Static;
            m_Desc = el.GetAttribute("desc");
            m_Value = el.GetAttribute("value");
        }

        //Ctor used when creating manually
        public AtlasStat(string name,
                         AtlasType type,
                         string lbName,
                         string friendlyName,
                         UsedForRating usedForRating)
        {
            m_Id = 0;  //0 is unknown; the ID will be assigned as the key is added to our list
            m_Name = name;
            m_Type = type;
            m_Category = StatCategory.Player; //We have no static stats right now
            m_Desc = String.Format("{0}[{1}]{2}",
                                   lbName,
                                   friendlyName,
                                   (usedForRating == UsedForRating.YES) ? "(RATING)" : "");
        }

        public int GetId() { return m_Id; }
        public void SetId(int id) { m_Id = id; }
        public string GetName() { return m_Name; }
        public AtlasType GetAtlasType() { return m_Type; }
        public StatCategory GetCategory() { return m_Category; }
        public string GetDesc() { return m_Desc; }
        public string GetValue() { return m_Value; }
    }

    class AtlasRule
    {
        int m_Id = 0;
        string m_Name = "";
        string m_Operation = "";
        string m_Desc = "";
        ArrayList m_KeyInputs = new ArrayList();
        ArrayList m_StatInputs = new ArrayList();
        string m_StatOutput = "";

        //Ctor used when reading from XML
        public AtlasRule(XmlElement el)
        {
            m_Id = int.Parse(el.GetAttribute("id"));
            m_Name = el.GetAttribute("name");
            m_Operation = el.GetAttribute("operation");
            m_Desc = el.GetAttribute("desc");

            for (XmlNode node = el.FirstChild; null != node; node = node.NextSibling)
            {
                XmlElement child = node as XmlElement;

                if (child.Name == "InputKey")
                {
                    m_KeyInputs.Add(child.GetAttribute("name"));
                }
                else if (child.Name == "InputStat")
                {
                    m_StatInputs.Add(child.GetAttribute("name"));
                }
                else if (child.Name == "OutputStat")
                {
                    m_StatOutput = child.GetAttribute("name");
                }
            }
        }

        //Ctor used when creating manually
        public AtlasRule(string name,
                         string op,
                         ArrayList keyInputs,
                         ArrayList statInputs,
                         string statOutput)
        {
            m_Id = 0;
            m_Name = name;
            m_Operation = op;
            m_KeyInputs = keyInputs;
            m_StatInputs = statInputs;
            m_StatOutput = statOutput;
        }

        public int GetId() { return m_Id; }
        public void SetId(int id) { m_Id = id; }
        public string GetName() { return m_Name; }
        public string GetOperation() { return m_Operation; }
        public string GetDesc() { return m_Desc; }
        public ArrayList GetKeyInputs() { return m_KeyInputs; }
        public ArrayList GetStatInputs() { return m_StatInputs; }
        public string GetStatOutput() { return m_StatOutput; }
    }

    //PURPOSE
    //  Writes an XML file that can be imported by Gamespy's Atlas stats & arbitration system.
    class AtlasXmlWriter
    {
        SortedList<string, AtlasKey> m_Keys;
        SortedList<string, AtlasStat> m_Stats;
        SortedList<string, AtlasRule> m_Rules;
        int m_HighestKeyId;
        int m_HighestStatId;
        int m_HighestRuleId;

        string m_PrevAtlasXml;
        int m_NumPrevKeys;
        int m_NumPrevStats;
        int m_NumPrevRules;

        int m_NumDeletedPadStats = 0;
        int m_NumPagePadStats = 0;
        int m_NumStaticStats = 0;

        StreamWriter m_StreamWriter;
        int m_IndentLevel = 0;
        uint m_MajorVersion = 0;
        uint m_MinorVersion = 0;
        public static bool m_NoInt64 = false;

        public bool ProcessPrevAtlasXml(string filename)
        {
            m_PrevAtlasXml = filename;
            m_NumPrevKeys = 0;
            m_NumPrevStats = 0;
            m_NumPrevRules = 0;

            if (filename.Length < 1)
            {
                //No previous version
                return true;
            }

            //Open the XML file
            FileInfo fileInfo = new FileInfo(filename);

            if (!fileInfo.Exists)
            {
                Console.WriteLine(String.Format("\n{0} does not exist", filename));
                return false;
            }

            XmlDocument doc = new XmlDocument();
            string uri = "file://" + fileInfo.FullName;
            Console.WriteLine(String.Format("Opening {0}...", uri));
            doc.Load(uri);

            Console.WriteLine(String.Format("\nLoaded previous Atlas XML {0}, parsing...", filename));

            //Parse the keys, stats, and rules
            XmlElement ruleset = doc.DocumentElement["Ruleset"];
            XmlNode node = null != ruleset ? ruleset.FirstChild : null;

            for (; null != node; node = node.NextSibling)
            {
                XmlElement el = node as XmlElement;

                if (el != null)
                {
                    if (el.Name == "Key")
                    {
                        AddKey(new AtlasKey(el)); 
                    }
                    else if (el.Name == "Stat")
                    {
                        AddStat(new AtlasStat(el));
                    }
                    else if (el.Name == "Rule")
                    {
                        AddRule(new AtlasRule(el));
                    }
                }
            }

            //It's possible that some stats in this version were removed (ex. GTA4 v9), 
            //which will cause stats to be placed on the wrong pages when imported back
            //into Atlas.  To avoid this, we must add pad stats to make the IDs sequential.
            SortedList<int, AtlasStat> sortedStats = new SortedList<int, AtlasStat>();
            foreach(KeyValuePair<string, AtlasStat> kvp in m_Stats)
            {
                sortedStats.Add(kvp.Value.GetId(), kvp.Value);
            }

            m_NumDeletedPadStats = 0;

            for (int i = 1; i < m_HighestStatId; i++)
            {
                if (!sortedStats.ContainsKey(i))
                {
                    AtlasStat stat = new AtlasStat(String.Format("STAT_DELETED_PADDING_{0}", i), AtlasType.Byte, "NONE", "Deleted Stat Padding", UsedForRating.NO);
                    stat.SetId(i);
                    AddStat(stat);
                    ++m_NumDeletedPadStats;
                }
            }

            m_NumPrevKeys = m_Keys.Count;
            m_NumPrevStats = m_Stats.Count;
            m_NumPrevRules = m_Rules.Count;

            return true;
        }

        public void Reset(uint toolMajorVersion,
                          uint toolMinorVersion,
                          string prevAtlasXml,
                          LeaderboardCollection leaderboards,
                          bool noInt64)
        {
            m_MajorVersion = toolMajorVersion;
            m_MinorVersion = toolMinorVersion;
            m_NoInt64 = noInt64;
            
            //Reset state
            m_Keys = new SortedList<string, AtlasKey>();
            m_Stats = new SortedList<string, AtlasStat>();
            m_Rules = new SortedList<string, AtlasRule>();
            m_HighestKeyId = 0;
            m_HighestStatId = 0;
            m_HighestRuleId = 0;
            m_NumDeletedPadStats = 0;
            m_NumPagePadStats = 0;

            //If we have a prevAtlasXml, the new one must include all its keys and stats, so add those now.
            if (!ProcessPrevAtlasXml(prevAtlasXml))
            {
                Console.WriteLine(String.Format("\nError processing previous Atlas XML file"));
                return;
            }

            //Add special keys
            string versionStr = string.Format("XLAST2C_VERSION_{0}_{1}", m_MajorVersion, m_MinorVersion);
            AddKey(new AtlasKey(versionStr, AtlasType.Int, "NONE", "Version of XLAST2C that generated Atlas XML"));
            AddKey(new AtlasKey("ARENA_FFA_PLACE", AtlasType.String, "NONE", "ARENA_FFA_PLACE"));

            //Main loop
            ArrayList lbArray = leaderboards.GetLeaderboards();

            int i = 0;
            while(i < lbArray.Count)
            {
                const int ATLAS_STATS_PER_PAGE = 1000;

                //If starting a new page, add special stats that must be present in each
                //Sake table for reading.
                int numPlayerStats = m_Stats.Count - m_NumStaticStats;

                if (0 == (numPlayerStats % ATLAS_STATS_PER_PAGE))
                {
                    int page = (numPlayerStats / ATLAS_STATS_PER_PAGE) + 1;
                    AddSimpleReplaceRule("rs_GamerHandle", String.Format("rs_GamerHandle_p{0}", page), "NONE", "Gamer Handle", AtlasType.String, UsedForRating.NO);
                    AddSimpleReplaceRule("rs_GamerName", String.Format("rs_GamerName_p{0}", page), "NONE", "Gamer Name", AtlasType.String, UsedForRating.NO);
                }

                Leaderboard lb = (Leaderboard)lbArray[i];

                //We have to keep all the stats for a leaderboard on the same page, so if
                //there isn't enough room left on this page we need to pad it out.
                int numRemaining = ATLAS_STATS_PER_PAGE - (numPlayerStats % ATLAS_STATS_PER_PAGE);

                if (numRemaining < GetNumStatsRequiredByLb(lb))
                {
                    for (int j = 0; j < numRemaining; j++)
                    {
                        AddStat(new AtlasStat(String.Format("STAT_PAGE_PADDING_{0}", m_HighestStatId + 1), AtlasType.Byte, "NONE", "Page Padding", UsedForRating.NO));
                        ++m_NumPagePadStats;
                    }

                    continue;
                }
                else
                {
                    //Add the stats for this leaderboard
                    AddLeaderboard(lb);
                    ++i;
                }                
            }
        }

        public int GetNumStatsRequiredByLb(Leaderboard lb)
        {
            int num = 1 + lb.Columns.Count; //EXISTS field, and one stat per column

            if(lb.IsSkill)
            {
                num += 2; //Rating, and num games played
            }

            return num;
        }

        void AddLeaderboard(Leaderboard lb)
        {
            //All leaderboards have an EXISTS field.  This lets us know if the player has written stats
            //for that leaderboard yet.
            AddExistsStat(lb);

            //Non-skill leaderboards write a stat for each of their columns.
            if (!lb.IsSkill)
            {
                for (int i = 0; i < lb.Columns.Count; i++)
                {
                    Leaderboard.Column c = lb.Columns[i];

                    ArrayList keyInputs = new ArrayList();
                    ArrayList statInputs = new ArrayList();

                    //Add key for field used to update the stat.
                    string keyName = ComposeKeyName(String.Format("{0:x8}_{1:x8}", lb.LeaderboardId, c.m_UpdateProp.Id));
                    AddKey(new AtlasKey(keyName, AtlasTypeConverter.FromCType(c.m_UpdateProp.TypeStr), lb.GetName(), c.m_UpdateProp.GetFieldName()));
                    keyInputs.Add(keyName);

                    //Create the stat from the column.
                    string statName = ComposeStatName(String.Format("{0:x8}_{1:x8}", lb.LeaderboardId, c.m_Id));
                    AddStat(new AtlasStat(statName,
                                          AtlasTypeConverter.FromCType(c.m_LbProp.TypeStr),
                                          lb.GetName(),
                                          c.m_LbProp.GetFieldName(),
                                          c.IsRatingColumn() ? UsedForRating.YES : UsedForRating.NO));

                    //If this is not a Replace operation, then we add the stat to the inputs.
                    string ruleOp = GetAtlasRuleOp(c.m_AggregationMethod);
                    if (ruleOp != "Replace")
                    {
                        statInputs.Add(statName);
                    }

                    //Add the rule.
                    AddRule(new AtlasRule(ComposeRuleName(String.Format("{0:x8}_{1:x8}", lb.LeaderboardId, c.m_Id)),
                                          ruleOp,
                                          keyInputs,
                                          statInputs,
                                          statName));
                }
            }

            //Skill leaderboards only write stats necessary to compute skill rating.
            else
            {
                //Add rating column
                string ratingStatName = ComposeStatName(String.Format("{0:x8}_0000003d", lb.LeaderboardId));
                AddStat(new AtlasStat(ratingStatName, AtlasType.Int, lb.GetName(), "Rating", UsedForRating.YES));

                //Add num games played stat
                string numPlayedStatName = String.Format("NUM_GAMES_PLAYED_{0:x8}", lb.LeaderboardId);
                AddStat(new AtlasStat(numPlayedStatName, AtlasType.Int, lb.GetName(), "NUM_GAMES_PLAYED", UsedForRating.NO));

                //Add keys for triggering the team and FFA ELO rules.
                string teamTriggerKeyName = ComposeKeyName(String.Format("{0:x8}_ELO_TEAM", lb.LeaderboardId));
                AddKey(new AtlasKey(teamTriggerKeyName, AtlasType.Byte, lb.GetName(), "Elo Team"));

                string ffaTriggerKeyName = ComposeKeyName(String.Format("{0:x8}_ELO_FFA", lb.LeaderboardId));
                AddKey(new AtlasKey(ffaTriggerKeyName, AtlasType.Byte, lb.GetName(), "Elo FFA"));

                //Add rules to increment NUM_GAMES_PLAYED. These must be added before ELO rule so they run first.
                {
                    ArrayList keyInputs = new ArrayList();
                    keyInputs.Add(teamTriggerKeyName);

                    ArrayList statInputs = new ArrayList();
                    statInputs.Add(numPlayedStatName);

                    AddRule(new AtlasRule(String.Format("NUM_GAMES_PLAYED_{0:x8}_ELO_TEAM", lb.LeaderboardId),
                                          "Increment",
                                          keyInputs,
                                          statInputs,
                                          numPlayedStatName));
                }

                {
                    ArrayList keyInputs = new ArrayList();
                    keyInputs.Add(ffaTriggerKeyName);

                    ArrayList statInputs = new ArrayList();
                    statInputs.Add(numPlayedStatName);

                    AddRule(new AtlasRule(String.Format("NUM_GAMES_PLAYED_{0:x8}_ELO_FFA", lb.LeaderboardId),
                                          "Increment",
                                          keyInputs,
                                          statInputs,
                                          numPlayedStatName));
                }

                //Add team ELO rule
                {
                    ArrayList keyInputs = new ArrayList();
                    keyInputs.Add(teamTriggerKeyName);

                    ArrayList statInputs = new ArrayList();
                    statInputs.Add(ratingStatName);
                    statInputs.Add(numPlayedStatName);

                    AddRule(new AtlasRule(ComposeRuleName(String.Format("{0:x8}_0000003d_ELO_TEAM", lb.LeaderboardId)),
                                          "StandardELO",
                                          keyInputs,
                                          statInputs,
                                          ratingStatName));
                }

                //Add FFA ELO rule
                {
                    ArrayList keyInputs = new ArrayList();
                    keyInputs.Add(ffaTriggerKeyName);
                    keyInputs.Add("ARENA_FFA_PLACE");

                    ArrayList statInputs = new ArrayList();
                    statInputs.Add(ratingStatName);
                    statInputs.Add(numPlayedStatName);

                    AddRule(new AtlasRule(ComposeRuleName(String.Format("{0:x8}_0000003d_ELO_FFA", lb.LeaderboardId)),
                                          "StandardELO",
                                          keyInputs,
                                          statInputs,
                                          ratingStatName));
                }
            }
        }

        public void WriteKey(AtlasKey key)
        {
            WriteLine(String.Format("<Key name=\"{0}\" id=\"{1}\" type=\"{2}\" desc=\"{3}\" />",
                                    key.GetName(),
                                    key.GetId(),
                                    AtlasTypeConverter.ToStr(key.GetAtlasType()),
                                    key.GetDesc()));
        }

        public void WriteStat(AtlasStat stat)
        {
            if (stat.GetCategory() == AtlasStat.StatCategory.Player)
            {
                WriteLine(String.Format("<Stat name=\"{0}\" id=\"{1}\" category=\"{2}\" type=\"{3}\" desc=\"{4}\" />",
                                        stat.GetName(),
                                        stat.GetId(),
                                        "Player",
                                        AtlasTypeConverter.ToStr(stat.GetAtlasType()),
                                        stat.GetDesc()));
            }
            else
            {
                WriteLine(String.Format("<Stat name=\"{0}\" id=\"{1}\" category=\"{2}\" type=\"{3}\" desc=\"{4}\" value=\"{5}\"/>",
                                        stat.GetName(),
                                        stat.GetId(),
                                        "Static",
                                        AtlasTypeConverter.ToStr(stat.GetAtlasType()),
                                        stat.GetDesc(),
                                        stat.GetValue()));
            }
        }

        public void WriteFile(string filename,
                              string gamespyGameName,
                              string atlasVersion)
        {
            //Open the file for writing.
            FileInfo finfo = new FileInfo(filename);

            if (finfo.Exists && finfo.IsReadOnly)
            {
                Console.WriteLine(String.Format("\nCould not write Atlas XML: {0} is read-only", finfo.FullName));
                return;
            }

            m_StreamWriter = File.CreateText(finfo.FullName);
            m_StreamWriter.NewLine = "\n";

            System.Console.WriteLine(String.Format("Writing Atlas XML to {0}...", finfo.FullName));

            //------------------------------------------------------------------
            //Write header
            //------------------------------------------------------------------
            m_IndentLevel = 0;
            WriteLine("<?xml version=\"1.0\" ?>");
            WriteLine("<Atlas>");
            ++m_IndentLevel;

            WriteLine("<!--RULESET DEFINITION-->");
            WriteLine(String.Format("<Ruleset gameName=\"{0}\" version=\"{1}\">", gamespyGameName, atlasVersion));
            WriteLine("");
            ++m_IndentLevel;

            //------------------------------------------------------------------
            //Write keys section
            //------------------------------------------------------------------
            WriteLine("<!--KEYS-->");

            //Sort them by ID, which is how GS sorts them
            SortedList<int, AtlasKey> sortedKeys = new SortedList<int, AtlasKey>();
            foreach(KeyValuePair<string, AtlasKey> kvp in m_Keys)
            {
                sortedKeys.Add(kvp.Value.GetId(), kvp.Value);
            }

            foreach (KeyValuePair<int, AtlasKey> kvp in sortedKeys)
            {
                WriteKey(kvp.Value);
            }
            WriteLine("");

            //------------------------------------------------------------------
            //Write stats section
            //------------------------------------------------------------------
            WriteLine("<!--STATS-->");
            //SortedList<int, AtlasStat> sortedStats = new SortedList<int, AtlasStat>();
            //foreach (KeyValuePair<string, AtlasStat> kvp in m_Stats)
            //{
            //    sortedStats.Add(kvp.Value.GetId(), kvp.Value);
            //}

            //int lastId = 0;
            //foreach (KeyValuePair<int, AtlasStat> kvp in sortedStats)
            foreach (KeyValuePair<string, AtlasStat> kvp in m_Stats)
            {
                //if(kvp.Key != (lastId+1))
                //{
                //    Trace.WriteLine("BULLSHIT");
                //}

                WriteStat(kvp.Value);

                //lastId = kvp.Key;
            }
            WriteLine("");

            //------------------------------------------------------------------
            //Write rules section
            //------------------------------------------------------------------
            WriteLine("<!--RULES-->");

            //First, get them sorted by ID, which is how GS outputs them.
            SortedList<int, AtlasRule> sortedRules = new SortedList<int, AtlasRule>();
            foreach (KeyValuePair<string, AtlasRule> kvp in m_Rules)
            {
                sortedRules.Add(kvp.Value.GetId(), kvp.Value);
            }

            foreach (KeyValuePair<int, AtlasRule> kvp in sortedRules)
            {
                AtlasRule rule = kvp.Value;

                WriteLine(String.Format("<Rule name=\"{0}\" id=\"{1}\" operation=\"{2}\" desc=\"\">", rule.GetName(), rule.GetId(), rule.GetOperation()));
                ++m_IndentLevel;

                for (int j = 0; j < rule.GetKeyInputs().Count; j++)
                {
                    string keyName = (string)rule.GetKeyInputs()[j];
                    
                    if (IsKeyRegistered(keyName))
                    {
                        WriteLine(String.Format("<InputKey name=\"{0}\" />", keyName));
                    }
                    else
                    {
                        Console.WriteLine("Key {0} referenced by rule {1} is not registered; XML will fail Atlas import", keyName, rule.GetName());
                    }
                }

                for (int j = 0; j < rule.GetStatInputs().Count; j++)
                {
                    string statName = (string)rule.GetStatInputs()[j];

                    if (IsStatRegistered(statName))
                    {
                        WriteLine(String.Format("<InputStat name=\"{0}\" />", statName));
                    }
                    else
                    {
                        Console.WriteLine("Stat {0} referenced by rule {1} is not registered; XML will fail Atlas import", statName, rule.GetName());
                    }
                }
                
                if (IsStatRegistered(rule.GetStatOutput()))
                {
                    WriteLine(String.Format("<OutputStat name=\"{0}\" />", rule.GetStatOutput()));
                }
                else
                {
                    Console.WriteLine("Output stat {0} referenced by rule {1} is not registered; XML will fail Atlas import", rule.GetStatOutput(), rule.GetName());
                }                

                --m_IndentLevel;
                WriteLine("</Rule>");
            }
            WriteLine("");

            //------------------------------------------------------------------
            //Write footer            
            //------------------------------------------------------------------
            --m_IndentLevel;
            WriteLine("</Ruleset>");
            --m_IndentLevel;
            WriteLine("</Atlas>");

            //Write summary comment
            WriteLine(String.Format("<!-- GENERATED WITH XLAST2C V{0}.{1} -->", m_MajorVersion, m_MinorVersion));
            WriteLine(string.Format("<!-- {0} keys, {1} stats ({2} page padding, {3} static), {4} rules, {5} SAKE fields -->",
                                    m_Keys.Count,
                                    m_Stats.Count,
                                    m_NumPagePadStats,
                                    m_NumStaticStats,
                                    m_Rules.Count,
                                    GetNumSakeFields()));
            if (m_PrevAtlasXml.Length > 0)
            {
                WriteLine(string.Format("<!-- This file extended file {0}, which had {1} keys, {2} stats, and {3} rules -->",
                                        m_PrevAtlasXml,
                                        m_NumPrevKeys,
                                        m_NumPrevStats,
                                        m_NumPrevRules));
                WriteLine(string.Format("<!-- Padding stats were addded for {0} ID gaps in {1} -->",
                                        m_NumDeletedPadStats,
                                        m_PrevAtlasXml));
            }

            m_StreamWriter.Close();
        }

        bool IsKeyRegistered(string name)
        {
            return (m_Keys != null) && m_Keys.ContainsKey(name);
        }

        bool IsStatRegistered(string name)
        {
            return (m_Stats != null) && m_Stats.ContainsKey(name);
        }

        void AddKey(AtlasKey v)
        {
            AtlasKey cur;

            if (m_Keys.TryGetValue(v.GetName(), out cur))
            {
                v.SetId(cur.GetId());
                m_Keys.Remove(cur.GetName());
            }

            if (v.GetId() > m_HighestKeyId)
            {
                m_HighestKeyId = v.GetId();
            }
            else if (v.GetId() == 0)
            {
                v.SetId(++m_HighestKeyId);
            }

            m_Keys.Add(v.GetName(), v); //Sorted by name to match GS's XML output
        }

        void AddStat(AtlasStat v)
        {
            AtlasStat cur;

            if (m_Stats.TryGetValue(v.GetName(), out cur))
            {
                v.SetId(cur.GetId());
                m_Stats.Remove(cur.GetName());
            }

            if (v.GetId() > m_HighestStatId)
            {
                m_HighestStatId = v.GetId();
            }
            else if (v.GetId() == 0)
            {
                v.SetId(++m_HighestStatId);
            }

            if(v.GetCategory() == AtlasStat.StatCategory.Static)
            {
                ++m_NumStaticStats;
            }

            m_Stats.Add(v.GetName(), v); //Sorted by name to match GS's XML output
        }

        void AddRule(AtlasRule v)
        {
            AtlasRule cur;

            if (m_Rules.TryGetValue(v.GetName(), out cur))
            {
                v.SetId(cur.GetId());
                m_Rules.Remove(cur.GetName());
            }

            if (v.GetId() > m_HighestRuleId)
            {
                m_HighestRuleId = v.GetId();
            }
            else if (v.GetId() == 0)
            {
                v.SetId(++m_HighestRuleId);
            }

            m_Rules.Add(v.GetName(), v);
        }

        int GetNumSakeFields()
        {
            //One sake field will be created for every stat. There are also the 
            //two fixed stats (ownerid, recordid) that every table has.
            return m_Stats.Count + 2;
        }

        string ComposeKeyName(string name)
        {
            return "KEY_" + name;
        }

        string ComposeStatName(string name)
        {
            return "STAT_" + name;
        }

        string ComposeRuleName(string name)
        {
            return "RULE_" + name;
        }

        void AddSimpleReplaceRule(string shortKeyName,
                                  string shortStatName,
                                  string lbName,
                                  string friendlyName,
                                  AtlasType type,
                                  UsedForRating usedForRating)
        {
            string keyName = ComposeKeyName(shortKeyName);
            string statName = ComposeStatName(shortStatName);

            AddKey(new AtlasKey(keyName, type, lbName, friendlyName));
            AddStat(new AtlasStat(statName, type, lbName, friendlyName, usedForRating));

            ArrayList keyInputs = new ArrayList();
            keyInputs.Add(keyName);

            ArrayList statInputs = new ArrayList();

            AddRule(new AtlasRule(ComposeRuleName(shortStatName),
                                  "Replace",
                                  keyInputs,
                                  statInputs,
                                  statName));
        }

        void AddSimpleReplaceRule(string name,
                                  string lbName,
                                  string friendlyName,
                                  AtlasType type,
                                  UsedForRating usedForRating)
        {
            AddSimpleReplaceRule(name, name, lbName, friendlyName, type, usedForRating);
        }

        string GetExistsKeyName(Leaderboard lb)
        {
            return ComposeKeyName(string.Format("{0:x8}_EXISTS", lb.LeaderboardId));
        }

        string GetExistsStatName(Leaderboard lb)
        {
            return ComposeStatName(string.Format("{0:x8}_EXISTS", lb.LeaderboardId));
        }

        void AddExistsStat(Leaderboard lb)
        {
            AddSimpleReplaceRule(string.Format("{0:x8}_EXISTS", lb.LeaderboardId), lb.GetName(), "Exists", AtlasType.Byte, UsedForRating.NO);
        }

        string GetAtlasRuleOp(string aggregationMethod)
        {
            if (aggregationMethod == "AGG_LAST") return "Replace";
            if (aggregationMethod == "AGG_SUM") return "Add";
            if (aggregationMethod == "AGG_MAX") return "Maximum";
            if (aggregationMethod == "AGG_MIN") return "Minimum";

            return "UNKNOWN";
        }
       
        void WriteLine(string text)
        {
            string s = "";

            for (int i = 0; i < m_IndentLevel; i++)
            {
                s += "\t";
            }

            s += text;

            m_StreamWriter.WriteLine(s);
        }
    }
}
