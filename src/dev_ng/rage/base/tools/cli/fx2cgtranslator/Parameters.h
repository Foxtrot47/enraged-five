
#ifndef _PARAMETERS_H_
#define _PARAMETERS_H_

#include "grcore/effect.h"
#include "file/stream.h"
#include "system/platform.h"
extern int line;

const char* OrbisMapSuffix();
extern void WriteString(rage::fiStream &S,const char *s);
extern void gram_error(const char *s);
extern char g_shaderPlatform;

enum ModifierType
{
	MODIFIER_SHARED			= (1<<0),
	MODIFIER_UNIFORM		= (1<<1),
	MODIFIER_ROW_MAJOR		= (1<<2),
	MODIFIER_COLUMN_MAJOR	= (1<<3),
	MODIFIER_CONST			= (1<<4),
	MODIFIER_STATIC			= (1<<5),
	MODIFIER_GROUPSHARED	= (1<<6),
};

enum InterpolatorType
{
	INTERPOLATOR_DEFAULT,
	INTERPOLATOR_NOPERSPECTIVE,
	INTERPOLATOR_CENTROID,
	INTERPOLATOR_SAMPLE,
	INTERPOLATOR_NOINTERPOLATION,
};

static const char *InterpolatorString(int it)	{
	switch(it)
	{
		case INTERPOLATOR_DEFAULT:
			return "";
		case INTERPOLATOR_CENTROID:
			return "centroid";
		case INTERPOLATOR_SAMPLE:
			return "sample";
		case INTERPOLATOR_NOINTERPOLATION:
			return g_shaderPlatform == rage::platform::ORBIS ? "nointerp" : "nointerpolation";
		case INTERPOLATOR_NOPERSPECTIVE:
			return g_shaderPlatform == rage::platform::ORBIS ? "nopersp" : "noperspective";
		default:
			Assertf(0,"Unknown interpolator: %u",it);
			return "";
	}
}


struct Param_t {
	Param_t(const char *scope,const char *type,const char *name,const char *sem) : Scope(scope), Type(type), Name(name), Semantic(sem), Interpolator(INTERPOLATOR_DEFAULT), Next(0), Line(line) {	}
	const char *Scope, *Type, *Name, *Semantic;
	InterpolatorType Interpolator;
	Param_t *Next;
	int Line;

	virtual void WriteOut(rage::fiStream* streamout)
	{
		if (Interpolator) 
		{
			fprintf(streamout,"%s ",InterpolatorString(Interpolator));
		}
		if (g_shaderPlatform == rage::platform::ORBIS && !strncmp(Type,"sampler",7))
		{
			fprintf(streamout,"%s SamplerState %s,%s Texture%s %s%s /* Orbis hack */",Scope,Name,Scope,Type+7,Name,OrbisMapSuffix());
		}
		else
		{
			fprintf(streamout,"%s %s %s", Scope, Type, Name);
			if (Semantic) fprintf(streamout," : %s",Semantic);
		}
	}
};

enum BaseType_t { BT_BOOL, BT_INT, BT_FLOAT, BT_SAMPLER, BT_TEXTURE, BT_STRING, BT_STRUCT, BT_SAMPLERSTATE,
	BT_RW_TEXTURE, BT_RO_BUFFER, BT_RW_BUFFER, BT_VOID };

struct Annotation_t {
	const char *Name;
	enum AnnoType { AT_INT, AT_FLOAT, AT_STRING, AT_INVALID } Type;
	union {
		int Int;
		float Float;
		const char *String;
	};
	Annotation_t *Next;
	
	void Save(rage::fiStream &S);
};

struct Type_t {
	Type_t(const char *name,BaseType_t bt,int r,int c,rage::grcEffect::VarType v,Annotation_t::AnnoType t) : Name(name), BaseType(bt), Rows(r), Cols(c), VarType(v), AnnoType(t) { }
	Type_t(const Type_t &parent,const char *suffix) : BaseType(parent.BaseType), Rows(parent.Rows), Cols(parent.Cols), VarType(parent.VarType), AnnoType(parent.AnnoType) { 
		Name = new char[strlen(parent.Name) + strlen(suffix) + 1];
		strcpy((char*)Name,parent.Name);
		strcat((char*)Name,suffix);
	}
	bool IsMsaaTexture() const {
		return BaseType == BT_TEXTURE && Cols;
	}
	bool IsComparisonSampler() const {
		return BaseType == BT_SAMPLER && Cols;
	}

	const char *Name;
	BaseType_t BaseType;
	int Rows, Cols;
	rage::grcEffect::VarType VarType;
	Annotation_t::AnnoType AnnoType;
};

struct Expr_t {
	enum ExprType { ET_INT, ET_FLOAT, ET_STRING, ET_LIST, ET_SAMPLER };
	Expr_t(ExprType t) : Type(t) { }
	ExprType GetType() const { return Type; }
	virtual int AsInt() const { gram_error("Trying to convert something that isn't an int?"); return 0; }
	virtual float AsFloat() const { gram_error("Trying to convert something that isn't a float?"); return 0.0f; }
	virtual const char* AsString() const { gram_error("Trying to convert something that isn't a string?"); return NULL; }
	virtual int GetSize() const { return 1; }
	virtual void Save(rage::fiStream &S) const {
		Assert(Type==ET_INT||Type==ET_FLOAT);
		if (Type==ET_INT) {
			int i = AsInt();
			S.WriteInt(&i,1);
			// printf("(int)%d\n",i);
		}
		else {
			float f = AsFloat();
			S.WriteFloat(&f,1);
			// printf("(float)%f\n",f);
		}
	}
	ExprType Type;
};

struct ConstantBuffer_t {
	ConstantBuffer_t(const char *name, const char *regsemantic, int regIndex, bool bGlobal);

	void Save(rage::fiStream &S);
	void Debug();

	static ConstantBuffer_t *GetCurrentBuffer() { return pCurrentBuffer; }
	static ConstantBuffer_t *Lookup(const char *name);

	static ConstantBuffer_t *pCurrentBuffer;

	const char *Name;
	const char *RegSemantic;
	unsigned int NameHash;
	short	Regs[6];
	unsigned int Size;
	unsigned int RegCount;
	bool isGlobal;
	unsigned char Usage;
	const char *AssigningProgram;
};

enum RegisterEnum
{
	REG_UNKNOWN, REG_INTERNAL, REG_BOOLEAN, REG_CONSTANT_BUFFER = REG_BOOLEAN,	// it's only one of those available
	REG_BUFFER_OFFSET, REG_SAMPLER, REG_TEXTURE, REG_UNORDERED_VIEW, REG_COUNT,
};

struct Parameter_t {
	Parameter_t(bool isStatic, bool isConst,
		const char *major, const char *name, Type_t *type, int arraySize,
		const char *semantic, const char *regsemantic,
		Annotation_t *annotations, Expr_t *initValue,
		RegisterEnum regEnum, int regIndex);
	
	static Parameter_t *Lookup(const char *name);
		
	void Save(rage::fiStream &S);
	void Debug();
	
	const char *Name;
	Type_t *Type;
	int ArraySize;
	const char *Semantic, *RegSemantic;
	RegisterEnum Regroup;
	int Reg;
	int ConstantBufferByteOffset;
	ConstantBuffer_t *pParentBuffer;
	Annotation_t *Annotations;
	Expr_t *InitValue;
	bool Used;
	unsigned char Usage;
	bool isNoStrip;
	const char *AssigningProgram;
};

#endif
