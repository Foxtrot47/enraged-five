﻿/* SCE CONFIDENTIAL
ORBIS Programmer Tool Runtime Library Release 00.820.020
* Copyright (C) 2012 Sony Computer Entertainment Inc.
* All Rights Reserved.
*/

#ifndef _SCE_PSSL_TYPES_H_
#define _SCE_PSSL_TYPES_H_

#if !defined(DOXYGEN_IGNORE)

#if !defined(SHACC_PLATFORM_HPP)
	#include "host_stdint.h"
#endif // !defined(SHACC_PLATFORM_HPP)

namespace sce
{
namespace Shader
{
namespace Binary
{
typedef enum PsslType
{
	kTypeFloat1,
	kTypeFloat2,
	kTypeFloat3,
	kTypeFloat4,
	kTypeHalf1,
	kTypeHalf2,
	kTypeHalf3,
	kTypeHalf4,
	kTypeInt1,
	kTypeInt2,
	kTypeInt3,
	kTypeInt4,
	kTypeUint1,
	kTypeUint2,
	kTypeUint3,
	kTypeUint4,
	kTypeFloat1x1,
	kTypeFloat2x1,
	kTypeFloat3x1,
	kTypeFloat4x1,
	kTypeFloat1x2,
	kTypeFloat2x2,
	kTypeFloat3x2,
	kTypeFloat4x2,
	kTypeFloat1x3,
	kTypeFloat2x3,
	kTypeFloat3x3,
	kTypeFloat4x3,
	kTypeFloat1x4,
	kTypeFloat2x4,
	kTypeFloat3x4,
	kTypeFloat4x4,
	kTypeHalf1x1,
	kTypeHalf2x1,
	kTypeHalf3x1,
	kTypeHalf4x1,
	kTypeHalf1x2,
	kTypeHalf2x2,
	kTypeHalf3x2,
	kTypeHalf4x2,
	kTypeHalf1x3,
	kTypeHalf2x3,
	kTypeHalf3x3,
	kTypeHalf4x3,
	kTypeHalf1x4,
	kTypeHalf2x4,
	kTypeHalf3x4,
	kTypeHalf4x4,
	kTypeInt1x1,
	kTypeInt2x1,
	kTypeInt3x1,
	kTypeInt4x1,
	kTypeInt1x2,
	kTypeInt2x2,
	kTypeInt3x2,
	kTypeInt4x2,
	kTypeInt1x3,
	kTypeInt2x3,
	kTypeInt3x3,
	kTypeInt4x3,
	kTypeInt1x4,
	kTypeInt2x4,
	kTypeInt3x4,
	kTypeInt4x4,
	kTypeUint1x1,
	kTypeUint2x1,
	kTypeUint3x1,
	kTypeUint4x1,
	kTypeUint1x2,
	kTypeUint2x2,
	kTypeUint3x2,
	kTypeUint4x2,
	kTypeUint1x3,
	kTypeUint2x3,
	kTypeUint3x3,
	kTypeUint4x3,
	kTypeUint1x4,
	kTypeUint2x4,
	kTypeUint3x4,
	kTypeUint4x4,
	kTypePoint,
	kTypeLine,
	kTypeTriangle,
	kTypeAdjacentline,
	kTypeAdjacenttriangle,
	kTypePatch,
	kTypeStructure,
	kTypeTypeEnd 
} PsslType;

typedef enum PsslSemantic
{
	kSemanticPosition,
	kSemanticNormal,
	kSemanticColor,
	kSemanticBinormal,
	kSemanticTangent,
	kSemanticTexcoord0,
	kSemanticTexcoord1,
	kSemanticTexcoord2,
	kSemanticTexcoord3,
	kSemanticTexcoord4,
	kSemanticTexcoord5,
	kSemanticTexcoord6,
	kSemanticTexcoord7,
	kSemanticTexcoord8,
	kSemanticTexcoord9,
	kSemanticTexcoordEnd,
	kSemanticImplicit,
	kSemanticNonreferencable,
	kSemanticClip,
	kSemanticFog,
	kSemanticPointsize,
	kSemanticFragcoord,
	kSemanticTarget0,
	kSemanticTarget1,
	kSemanticTarget2,
	kSemanticTarget3,
	kSemanticTarget4,
	kSemanticTarget5,
	kSemanticTarget6,
	kSemanticTarget7,
	kSemanticTarget8,
	kSemanticTarget9,
	kSemanticTarget10,
	kSemanticTarget11,
	kSemanticDepth,
	kSemanticLastcg,
	kSemanticUserDefined,
	kSemanticSClipDistance,
	kSemanticSCullDistance,
	kSemanticSCoverage,
	kSemanticSDepthOutput,
	kSemanticSDispatchthreadId,
	kSemanticSDomainLocation,
	kSemanticSGroupId,
	kSemanticSGroupIndex,
	kSemanticSGroupThreadId,
	kSemanticSPosition,
	kSemanticSVertexId,
	kSemanticSInstanceId,
	kSemanticSSampleIndex,
	kSemanticSPrimitiveId,
	kSemanticSGsinstanceId,
	kSemanticSOutputControlPointId,
	kSemanticSFrontFace,
	kSemanticSRenderTargetIndex,
	kSemanticSViewportIndex,
	kSemanticSTargetOutput,
	kSemanticSEdgeTessFactor,
	kSemanticSInsideTessFactor,
	kSemanticSemanticEnd 
} PsslSemantic;

typedef enum PsslFragmentInterpType
{
	kFragmentInterpTypeSampleNone,
	kFragmentInterpTypeSampleLinear,
	kFragmentInterpTypeSamplePoint,
	kFragmentInterpTypeFragmentInterpTypeLast 
} PsslFragmentInterpType;

typedef enum PsslBufferType
{
	kBufferTypeDataBuffer,
	kBufferTypeTexture1d,
	kBufferTypeTexture2d,
	kBufferTypeTexture3d,
	kBufferTypeTextureCube,
	kBufferTypeTexture1dArray,
	kBufferTypeTexture2dArray,
	kBufferTypeTextureCubeArray,
	kBufferTypeMsTexture2d,
	kBufferTypeMsTexture2dArray,
	kBufferTypeRegularBuffer,
	kBufferTypeByteBuffer,
	kBufferTypeRwDataBuffer,
	kBufferTypeRwTexture1d,
	kBufferTypeRwTexture2d,
	kBufferTypeRwTexture3d,
	kBufferTypeRwTexture1dArray,
	kBufferTypeRwTexture2dArray,
	kBufferTypeRwRegularBuffer,
	kBufferTypeRwByteBuffer,
	kBufferTypeAppendBuffer,
	kBufferTypeConsumeBuffer,
	kBufferTypeConstantBuffer,
	kBufferTypeTextureBuffer,
	kBufferTypePointBuffer,
	kBufferTypeLineBuffer,
	kBufferTypeTriangleBuffer,
	kBufferTypeBufferTypeLast 
} PsslBufferType;

typedef enum PsslInternalBufferType
{
	kInternalBufferTypeUav,
	kInternalBufferTypeSrv,
	kInternalBufferTypeLds,
	kInternalBufferTypeGds,
	kInternalBufferTypeCbuffer,
	kInternalBufferTypeTextureSampler,
	kInternalBufferTypeInternal,
	kInternalBufferTypeInternalBufferTypeLast 
} PsslInternalBufferType;

typedef enum PsslShaderType
{
	kShaderTypeVsShader,
	kShaderTypeFsShader,
	kShaderTypeCsShader,
	kShaderTypeGsShader,
	kShaderTypeHsShader,
	kShaderTypeDsShader,
	kShaderTypeShaderTypeLast 
} PsslShaderType;

typedef enum PsslCodeType
{
	kCodeTypeIl,
	kCodeTypeIsa,
	kCodeTypeCodeTypeLast 
} PsslCodeType;

typedef enum PsslStatus
{
	kStatusFail = -1,
	kStatusOk   =  0,
	kStatusStatusLast 
} PsslStatus;

typedef enum PsslComponentMask
{
	kComponentMask,
	kComponentMaskX,
	kComponentMaskY,
	kComponentMaskXy,
	kComponentMaskZ,
	kComponentMaskXZ,
	kComponentMaskYz,
	kComponentMaskXyz,
	kComponentMaskW,
	kComponentMaskXW,
	kComponentMaskYW,
	kComponentMaskXyW,
	kComponentMaskZw,
	kComponentMaskXZw,
	kComponentMaskYzw,
	kComponentMaskXyzw,
	kComponentMaskComponentMaskLast 
} PsslComponentMask;

typedef enum PsslGsIoType
{
	kGsIoTypeGsTri,
	kGsIoTypeGsLine,
	kGsIoTypeGsPoint,
	kGsIoTypeGsAdjTri,
	kGsIoTypeGsAdjLine,
	kGsIoTypeGsIoTypeLast 
} PsslGsIoType;

typedef enum PsslHsTopologyType
{
	kHsTopologyTypeHsPoint,
	kHsTopologyTypeHsLine,
	kHsTopologyTypeHsCwtri,
	kHsTopologyTypeHsCcwtri,
	kHsTopologyTypeHsTopologyTypeLast 
} PsslHsTopologyType; 

typedef enum PsslHsPartitioningType
{
	kHsPartitioningTypeHsInteger,
	kHsPartitioningTypeHsPowerOf2,
	kHsPartitioningTypeHsOddFactorial,
	kHsPartitioningTypeHsEvenFactorial,
	kHsPartitioningTypeHsPartitioningTypeLast 
} PsslHsPartitioningType;

typedef enum PsslHsDsPatchType
{
	kHsDsPatchTypeHsDsIsoline,
	kHsDsPatchTypeHsDsTri,
	kHsDsPatchTypeHsDsQuad,
	kHsDsPatchTypeHsDsPatchTypeLast 
} PsslHsDsPatchType; 

typedef enum PsslVertexVariant
{
	kVertexVariantVertex, 
	kVertexVariantExport, 
	kVertexVariantLocal, 
	kVertexVariantLast 
} PsslVertexVariant;

typedef enum PsslDomainVariant
{
	kDomainVariantVertex, 
	kDomainVariantExport, 
	kDomainVariantLast 
} PsslDomainVariant;


// return the element count from type
// return 0 for error
static inline uint32_t getPsslTypeElementCount( PsslType type )
{
	switch( type )
	{
	// vector types
	case kTypeFloat1:
	case kTypeInt1:
	case kTypeHalf1:
	case kTypeUint1:
		return 1;
	case kTypeFloat2:
	case kTypeInt2:
	case kTypeHalf2:
	case kTypeUint2:
		return 2;
	case kTypeFloat3:
	case kTypeInt3:
	case kTypeHalf3:
	case kTypeUint3:
		return 3;
	case kTypeFloat4:
	case kTypeInt4:
	case kTypeHalf4:
	case kTypeUint4:
		return 4;
	// matrix types
	case kTypeFloat1x1:
	case kTypeInt1x1:
	case kTypeHalf1x1:
	case kTypeUint1x1:
		return 1;
	case kTypeFloat2x1:
	case kTypeInt2x1:
	case kTypeHalf2x1:
	case kTypeUint2x1:
		return 2;
	case kTypeFloat3x1:
	case kTypeInt3x1:
	case kTypeHalf3x1:
	case kTypeUint3x1:
		return 3;
	case kTypeFloat4x1:
	case kTypeInt4x1:
	case kTypeHalf4x1:
	case kTypeUint4x1:
		return 4;
	case kTypeFloat1x2:
	case kTypeInt1x2:
	case kTypeHalf1x2:
	case kTypeUint1x2:
		return 2;
	case kTypeFloat2x2:
	case kTypeInt2x2:
	case kTypeHalf2x2:
	case kTypeUint2x2:
		return 4;
	case kTypeFloat3x2:
	case kTypeInt3x2:
	case kTypeHalf3x2:
	case kTypeUint3x2:
		return 6;
	case kTypeFloat4x2:
	case kTypeInt4x2:
	case kTypeHalf4x2:
	case kTypeUint4x2:
		return 8;
	case kTypeFloat1x3:
	case kTypeInt1x3:
	case kTypeHalf1x3:
	case kTypeUint1x3:
		return 3;
	case kTypeFloat2x3:
	case kTypeInt2x3:
	case kTypeHalf2x3:
	case kTypeUint2x3:
		return 6;
	case kTypeFloat3x3:
	case kTypeInt3x3:
	case kTypeHalf3x3:
	case kTypeUint3x3:
		return 9;
	case kTypeFloat4x3:
	case kTypeInt4x3:
	case kTypeHalf4x3:
	case kTypeUint4x3:
		return 12;
	case kTypeFloat1x4:
	case kTypeInt1x4:
	case kTypeHalf1x4:
	case kTypeUint1x4:
		return 4;
	case kTypeFloat2x4:
	case kTypeInt2x4:
	case kTypeHalf2x4:
	case kTypeUint2x4:
		return 8;
	case kTypeFloat3x4:
	case kTypeInt3x4:
	case kTypeHalf3x4:
	case kTypeUint3x4:
		return 12;
	case kTypeFloat4x4:
	case kTypeInt4x4:
	case kTypeHalf4x4:
	case kTypeUint4x4:
		return 16;
	default:
		return 0;
	}
}

static inline const char * getPsslTypeString( PsslType type )
{
	switch( type )
	{
	case kTypeFloat1:           return "kTypeFloat1"; 
	case kTypeFloat2:           return "kTypeFloat2"; 
	case kTypeFloat3:           return "kTypeFloat3"; 
	case kTypeFloat4:           return "kTypeFloat4"; 
	case kTypeHalf1:            return "kTypeHalf1"; 
	case kTypeHalf2:            return "kTypeHalf2"; 
	case kTypeHalf3:            return "kTypeHalf3"; 
	case kTypeHalf4:            return "kTypeHalf4"; 
	case kTypeInt1:             return "kTypeInt1"; 
	case kTypeInt2:             return "kTypeInt2"; 
	case kTypeInt3:             return "kTypeInt3"; 
	case kTypeInt4:             return "kTypeInt4"; 
	case kTypeUint1:            return "kTypeUint1"; 
	case kTypeUint2:            return "kTypeUint2"; 
	case kTypeUint3:            return "kTypeUint3"; 
	case kTypeUint4:            return "kTypeUint4"; 
	case kTypeFloat1x1:         return "kTypeFloat1x1"; 
	case kTypeFloat2x1:         return "kTypeFloat2x1"; 
	case kTypeFloat3x1:         return "kTypeFloat3x1"; 
	case kTypeFloat4x1:         return "kTypeFloat4x1"; 
	case kTypeFloat1x2:         return "kTypeFloat1x2"; 
	case kTypeFloat2x2:         return "kTypeFloat2x2"; 
	case kTypeFloat3x2:         return "kTypeFloat3x2"; 
	case kTypeFloat4x2:         return "kTypeFloat4x2"; 
	case kTypeFloat1x3:         return "kTypeFloat1x3"; 
	case kTypeFloat2x3:         return "kTypeFloat2x3"; 
	case kTypeFloat3x3:         return "kTypeFloat3x3"; 
	case kTypeFloat4x3:         return "kTypeFloat4x3"; 
	case kTypeFloat1x4:         return "kTypeFloat1x4"; 
	case kTypeFloat2x4:         return "kTypeFloat2x4"; 
	case kTypeFloat3x4:         return "kTypeFloat3x4"; 
	case kTypeFloat4x4:         return "kTypeFloat4x4"; 
	case kTypeHalf1x1:          return "kTypeHalf1x1"; 
	case kTypeHalf2x1:          return "kTypeHalf2x1"; 
	case kTypeHalf3x1:          return "kTypeHalf3x1"; 
	case kTypeHalf4x1:          return "kTypeHalf4x1"; 
	case kTypeHalf1x2:          return "kTypeHalf1x2"; 
	case kTypeHalf2x2:          return "kTypeHalf2x2"; 
	case kTypeHalf3x2:          return "kTypeHalf3x2"; 
	case kTypeHalf4x2:          return "kTypeHalf4x2"; 
	case kTypeHalf1x3:          return "kTypeHalf1x3"; 
	case kTypeHalf2x3:          return "kTypeHalf2x3"; 
	case kTypeHalf3x3:          return "kTypeHalf3x3"; 
	case kTypeHalf4x3:          return "kTypeHalf4x3"; 
	case kTypeHalf1x4:          return "kTypeHalf1x4"; 
	case kTypeHalf2x4:          return "kTypeHalf2x4"; 
	case kTypeHalf3x4:          return "kTypeHalf3x4"; 
	case kTypeHalf4x4:          return "kTypeHalf4x4"; 
	case kTypeInt1x1:           return "kTypeInt1x1"; 
	case kTypeInt2x1:           return "kTypeInt2x1"; 
	case kTypeInt3x1:           return "kTypeInt3x1"; 
	case kTypeInt4x1:           return "kTypeInt4x1"; 
	case kTypeInt1x2:           return "kTypeInt1x2"; 
	case kTypeInt2x2:           return "kTypeInt2x2"; 
	case kTypeInt3x2:           return "kTypeInt3x2"; 
	case kTypeInt4x2:           return "kTypeInt4x2"; 
	case kTypeInt1x3:           return "kTypeInt1x3"; 
	case kTypeInt2x3:           return "kTypeInt2x3"; 
	case kTypeInt3x3:           return "kTypeInt3x3"; 
	case kTypeInt4x3:           return "kTypeInt4x3"; 
	case kTypeInt1x4:           return "kTypeInt1x4"; 
	case kTypeInt2x4:           return "kTypeInt2x4"; 
	case kTypeInt3x4:           return "kTypeInt3x4"; 
	case kTypeInt4x4:           return "kTypeInt4x4"; 
	case kTypeUint1x1:          return "kTypeUint1x1"; 
	case kTypeUint2x1:          return "kTypeUint2x1"; 
	case kTypeUint3x1:          return "kTypeUint3x1"; 
	case kTypeUint4x1:          return "kTypeUint4x1"; 
	case kTypeUint1x2:          return "kTypeUint1x2"; 
	case kTypeUint2x2:          return "kTypeUint2x2"; 
	case kTypeUint3x2:          return "kTypeUint3x2"; 
	case kTypeUint4x2:          return "kTypeUint4x2"; 
	case kTypeUint1x3:          return "kTypeUint1x3"; 
	case kTypeUint2x3:          return "kTypeUint2x3"; 
	case kTypeUint3x3:          return "kTypeUint3x3"; 
	case kTypeUint4x3:          return "kTypeUint4x3"; 
	case kTypeUint1x4:          return "kTypeUint1x4"; 
	case kTypeUint2x4:          return "kTypeUint2x4"; 
	case kTypeUint3x4:          return "kTypeUint3x4"; 
	case kTypeUint4x4:          return "kTypeUint4x4"; 
	case kTypePoint:            return "kTypePoint"; 
	case kTypeLine:             return "kTypeLine"; 
	case kTypeTriangle:         return "kTypeTriangle"; 
	case kTypeAdjacentline:     return "kTypeAdjacentline"; 
	case kTypeAdjacenttriangle: return "kTypeAdjacenttriangle"; 
	case kTypePatch:            return "kTypePatch"; 
	case kTypeStructure:        return "kTypeStructure";	
	default: return "(unknown)";
	}
}

static inline const char * getPsslSemanticString( PsslSemantic semantic )
{
	switch( semantic )
	{
	case kSemanticPosition:                  return "kSemanticPosition"; 
	case kSemanticNormal:                    return "kSemanticNormal"; 
	case kSemanticColor:                     return "kSemanticColor"; 
	case kSemanticBinormal:                  return "kSemanticBinormal"; 
	case kSemanticTangent:                   return "kSemanticTangent"; 
	case kSemanticTexcoord0:                 return "kSemanticTexcoord0"; 
	case kSemanticTexcoord1:                 return "kSemanticTexcoord1"; 
	case kSemanticTexcoord2:                 return "kSemanticTexcoord2"; 
	case kSemanticTexcoord3:                 return "kSemanticTexcoord3"; 
	case kSemanticTexcoord4:                 return "kSemanticTexcoord4"; 
	case kSemanticTexcoord5:                 return "kSemanticTexcoord5"; 
	case kSemanticTexcoord6:                 return "kSemanticTexcoord6"; 
	case kSemanticTexcoord7:                 return "kSemanticTexcoord7"; 
	case kSemanticTexcoord8:                 return "kSemanticTexcoord8"; 
	case kSemanticTexcoord9:                 return "kSemanticTexcoord9"; 
	case kSemanticTexcoordEnd:               return "kSemanticTexcoordEnd"; 
	case kSemanticImplicit:                  return "kSemanticImplicit"; 
	case kSemanticNonreferencable:           return "kSemanticNonreferencable"; 
	case kSemanticClip:                      return "kSemanticClip"; 
	case kSemanticFog:                       return "kSemanticFog"; 
	case kSemanticPointsize:                 return "kSemanticPointsize"; 
	case kSemanticFragcoord:                 return "kSemanticFragcoord"; 
	case kSemanticTarget0:                   return "kSemanticTarget0"; 
	case kSemanticTarget1:                   return "kSemanticTarget1"; 
	case kSemanticTarget2:                   return "kSemanticTarget2"; 
	case kSemanticTarget3:                   return "kSemanticTarget3"; 
	case kSemanticTarget4:                   return "kSemanticTarget4"; 
	case kSemanticTarget5:                   return "kSemanticTarget5"; 
	case kSemanticTarget6:                   return "kSemanticTarget6"; 
	case kSemanticTarget7:                   return "kSemanticTarget7"; 
	case kSemanticTarget8:                   return "kSemanticTarget8"; 
	case kSemanticTarget9:                   return "kSemanticTarget9"; 
	case kSemanticTarget10:                  return "kSemanticTarget10"; 
	case kSemanticTarget11:                  return "kSemanticTarget11"; 
	case kSemanticDepth:                     return "kSemanticDepth"; 
	case kSemanticLastcg:                    return "kSemanticLastcg"; 
	case kSemanticUserDefined:               return "kSemanticUserDefined"; 
	case kSemanticSClipDistance:             return "kSemanticSClipDistance"; 
	case kSemanticSCullDistance:             return "kSemanticSCullDistance"; 
	case kSemanticSCoverage:                 return "kSemanticSCoverage"; 
	case kSemanticSDepthOutput:              return "kSemanticSDepthOutput"; 
	case kSemanticSDispatchthreadId:         return "kSemanticSDispatchthreadId"; 
	case kSemanticSDomainLocation:           return "kSemanticSDomainLocation"; 
	case kSemanticSGroupId:                  return "kSemanticSGroupId"; 
	case kSemanticSGroupIndex:               return "kSemanticSGroupIndex"; 
	case kSemanticSGroupThreadId:            return "kSemanticSGroupThreadId"; 
	case kSemanticSPosition:                 return "kSemanticSPosition"; 
	case kSemanticSVertexId:                 return "kSemanticSVertexId"; 
	case kSemanticSInstanceId:               return "kSemanticSInstanceId"; 
	case kSemanticSSampleIndex:              return "kSemanticSSampleIndex"; 
	case kSemanticSPrimitiveId:              return "kSemanticSPrimitiveId"; 
	case kSemanticSGsinstanceId:             return "kSemanticSGsinstanceId"; 
	case kSemanticSOutputControlPointId:     return "kSemanticSOutputControlPointId"; 
	case kSemanticSFrontFace:                return "kSemanticSFrontFace"; 
	case kSemanticSRenderTargetIndex:        return "kSemanticSRenderTargetIndex"; 
	case kSemanticSViewportIndex:            return "kSemanticSViewportIndex"; 
	case kSemanticSTargetOutput:             return "kSemanticSTargetOutput"; 
	case kSemanticSEdgeTessFactor:           return "kSemanticSEdgeTessFactor"; 
	case kSemanticSInsideTessFactor:         return "kSemanticSInsideTessFactor";
	default: return "(unknown)";
	}
}

static inline const char *getPsslFragmentInterpTypeString(PsslFragmentInterpType type)
{
	switch (type)
	{
	case (kFragmentInterpTypeSampleNone):   return "kFragmentInterpTypeSampleNone"; 
	case (kFragmentInterpTypeSampleLinear): return "kFragmentInterpTypeSampleLinear"; 
	case (kFragmentInterpTypeSamplePoint):  return "kFragmentInterpTypeSamplePoint"; 
	default: return "(unknown)";
	}
}

static inline const char *getPsslBufferTypeString(PsslBufferType type)
{
	switch (type)
	{
	case (kBufferTypeDataBuffer):       return "kBufferTypeDataBuffer"; 
	case (kBufferTypeTexture1d):        return "kBufferTypeTexture1d"; 
	case (kBufferTypeTexture2d):        return "kBufferTypeTexture2d"; 
	case (kBufferTypeTexture3d):        return "kBufferTypeTexture3d"; 
	case (kBufferTypeTextureCube):      return "kBufferTypeTextureCube"; 
	case (kBufferTypeTexture1dArray):   return "kBufferTypeTexture1dArray"; 
	case (kBufferTypeTexture2dArray):   return "kBufferTypeTexture2dArray"; 
	case (kBufferTypeTextureCubeArray): return "kBufferTypeTextureCubeArray"; 
	case (kBufferTypeMsTexture2d):      return "kBufferTypeMsTexture2d"; 
	case (kBufferTypeMsTexture2dArray): return "kBufferTypeMsTexture2dArray"; 
	case (kBufferTypeRegularBuffer):    return "kBufferTypeRegularBuffer"; 
	case (kBufferTypeByteBuffer):       return "kBufferTypeByteBuffer"; 
	case (kBufferTypeRwDataBuffer):     return "kBufferTypeRwDataBuffer"; 
	case (kBufferTypeRwTexture1d):      return "kBufferTypeRwTexture1d"; 
	case (kBufferTypeRwTexture2d):      return "kBufferTypeRwTexture2d"; 
	case (kBufferTypeRwTexture3d):      return "kBufferTypeRwTexture3d"; 
	case (kBufferTypeRwTexture1dArray): return "kBufferTypeRwTexture1dArray"; 
	case (kBufferTypeRwTexture2dArray): return "kBufferTypeRwTexture2dArray"; 
	case (kBufferTypeRwRegularBuffer):  return "kBufferTypeRwRegularBuffer"; 
	case (kBufferTypeRwByteBuffer):     return "kBufferTypeRwByteBuffer"; 
	case (kBufferTypeAppendBuffer):     return "kBufferTypeAppendBuffer"; 
	case (kBufferTypeConsumeBuffer):    return "kBufferTypeConsumeBuffer"; 
	case (kBufferTypeConstantBuffer):   return "kBufferTypeConstantBuffer"; 
	case (kBufferTypeTextureBuffer):    return "kBufferTypeTextureBuffer"; 
	case (kBufferTypePointBuffer):      return "kBufferTypePointBuffer"; 
	case (kBufferTypeLineBuffer):       return "kBufferTypeLineBuffer"; 
	case (kBufferTypeTriangleBuffer):   return "kBufferTypeTriangleBuffer"; 
	default: return "(unknown)";
	}
}

static inline const char *getPsslInternalBufferTypeString(PsslInternalBufferType type)
{
	switch (type)
	{
	case (kInternalBufferTypeUav):             return "kInternalBufferTypeUav"; 
	case (kInternalBufferTypeSrv):             return "kInternalBufferTypeSrv"; 
	case (kInternalBufferTypeLds):             return "kInternalBufferTypeLds"; 
	case (kInternalBufferTypeGds):             return "kInternalBufferTypeGds"; 
	case (kInternalBufferTypeCbuffer):         return "kInternalBufferTypeCbuffer"; 
	case (kInternalBufferTypeTextureSampler):  return "kInternalBufferTypeTextureSampler"; 
	case (kInternalBufferTypeInternal):        return "kInternalBufferTypeInternal"; 
	default: return "(unknown)";
	}
}

static inline const char *getPsslShaderTypeString(PsslShaderType type)
{
	switch (type)
	{
	case (kShaderTypeVsShader): return "kShaderTypeVsShader"; 
	case (kShaderTypeFsShader): return "kShaderTypeFsShader"; 
	case (kShaderTypeCsShader): return "kShaderTypeCsShader"; 
	case (kShaderTypeGsShader): return "kShaderTypeGsShader"; 
	case (kShaderTypeHsShader): return "kShaderTypeHsShader"; 
	case (kShaderTypeDsShader): return "kShaderTypeDsShader"; 
	default: return "(unknown)";
	}
}

static inline const char *getPsslCodeTypeString(PsslCodeType type)
{
	switch (type)
	{
	case (kCodeTypeIl):  return "kCodeTypeIl"; 
	case (kCodeTypeIsa): return "kCodeTypeIsa"; 
	default: return "(unknown)";
	}
}

static inline const char *getPsslErrorCodeString(PsslStatus type)
{
	switch (type)
	{
	case (kStatusOk):   return "kStatusOk"; 
	case (kStatusFail): return "kStatusFail"; 
	default: return "(unknown)";
	}
}

static inline const char *getPsslComponentMaskString(PsslComponentMask type)
{
	switch (type)
	{
	case (kComponentMask):     return "kComponentMask"; 
	case (kComponentMaskX):    return "kComponentMaskX"; 
	case (kComponentMaskY):    return "kComponentMaskY"; 
	case (kComponentMaskXy):   return "kComponentMaskXy"; 
	case (kComponentMaskZ):    return "kComponentMaskZ"; 
	case (kComponentMaskXZ):   return "kComponentMaskXZ"; 
	case (kComponentMaskYz):   return "kComponentMaskYz"; 
	case (kComponentMaskXyz):  return "kComponentMaskXyz"; 
	case (kComponentMaskW):    return "kComponentMaskW"; 
	case (kComponentMaskXW):   return "kComponentMaskXW"; 
	case (kComponentMaskYW):   return "kComponentMaskYW"; 
	case (kComponentMaskXyW):  return "kComponentMaskXyW"; 
	case (kComponentMaskZw):   return "kComponentMaskZw"; 
	case (kComponentMaskXZw):  return "kComponentMaskXZw"; 
	case (kComponentMaskYzw):  return "kComponentMaskYzw"; 
	case (kComponentMaskXyzw): return "kComponentMaskXyzw"; 
	default: return "(unknown)";
	}
}

static inline const char *getPsslGsIoString(PsslGsIoType type)
{
	switch (type)
	{
	case (kGsIoTypeGsTri):      return "kGsIoTypeGsTri"; 
	case (kGsIoTypeGsLine):     return "kGsIoTypeGsLine"; 
	case (kGsIoTypeGsPoint):    return "kGsIoTypeGsPoint"; 
	case (kGsIoTypeGsAdjTri):   return "kGsIoTypeGsAdjTri"; 
	case (kGsIoTypeGsAdjLine):  return "kGsIoTypeGsAdjLine"; 
	default: return "(unknown)";
	}
}
 
static inline const char *getPsslHsTopologyString(PsslHsTopologyType type)
{
	switch (type)
	{
	case (kHsTopologyTypeHsPoint):  return "kHsTopologyTypeHsPoint"; 
	case (kHsTopologyTypeHsLine):   return "kHsTopologyTypeHsLine"; 
	case (kHsTopologyTypeHsCwtri):  return "kHsTopologyTypeHsCwtri"; 
	case (kHsTopologyTypeHsCcwtri): return "kHsTopologyTypeHsCcwtri"; 
	default: return "(unknown)";
	}
}

static inline const char *getPsslHsPartitioningString(PsslHsPartitioningType type)
{
	switch (type)
	{
	case (kHsPartitioningTypeHsInteger):       return "kHsPartitioningTypeHsInteger"; 
	case (kHsPartitioningTypeHsPowerOf2):      return "kHsPartitioningTypeHsPowerOf2"; 
	case (kHsPartitioningTypeHsOddFactorial):  return "kHsPartitioningTypeHsOddFactorial"; 
	case (kHsPartitioningTypeHsEvenFactorial): return "kHsPartitioningTypeHsEvenFactorial"; 
	default: return "(unknown)";
	}
}

static inline const char *getPsslHsDsPatchString(PsslHsDsPatchType type)
{
	switch (type)
	{
	case (kHsDsPatchTypeHsDsIsoline): return "kHsDsPatchTypeHsDsIsoline"; 
	case (kHsDsPatchTypeHsDsTri):     return "kHsDsPatchTypeHsDsTri"; 
	case (kHsDsPatchTypeHsDsQuad):    return "kHsDsPatchTypeHsDsQuad"; 
	default: return "(unknown)";
	}
}

static inline const char *getPsslVertexVariantString(PsslVertexVariant type) 
{ 
	switch (type) 
	{ 
	case (kVertexVariantVertex): return "kVertexVariantVertex"; break; 
	case (kVertexVariantExport): return "kVertexVariantExport"; break; 
	case (kVertexVariantLocal):  return "kVertexVariantLocal";  break; 
	default: return "(unknown)"; 
	} 
}

static inline const char *getPsslDomainVariantString(PsslDomainVariant type) 
{ 
	switch (type) 
	{ 
	case (kDomainVariantVertex): return "kDomainVariantVertex"; break; 
	case (kDomainVariantExport): return "kDomainVariantExport"; break; 
	default: return "(unknown)";
	} 
}

} // namespace Binary
} // namespace Shader
} // namespace sce

#endif // !defined(DOXYGEN_IGNORE)

#endif // _SCE_PSSL_TYPES_H_

