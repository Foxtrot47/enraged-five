#ifndef _EFFECTDATA_H_
#define _EFFECTDATA_H_

#include "atl/array.h"
#include "atl/map.h"
#include "atl/string.h"
#include "string/stringhash.h"
#include "Parameters.h"


struct StateTemp_t 
{
	StateTemp_t(int l,int s,bool isFloat) : LexVal(l), State(s), IsFloat(isFloat) { }
	int LexVal;
	int State;
	bool IsFloat;
};


class GlobalEffectData
{
public:
	static int											s_ShaderVersion;

	static rage::atMap<rage::ConstString,StateTemp_t*>	s_SamplerStates;
	static rage::atMap<rage::ConstString,StateTemp_t*>	s_RenderStates;
	static rage::atArray<Parameter_t*>					s_Locals;
	static rage::atArray<Parameter_t*>					s_Globals;
	static rage::atArray<Parameter_t*>					s_Constants;
	static rage::atArray<ConstantBuffer_t*>				s_BufferLocals;
	static rage::atArray<ConstantBuffer_t*>				s_BufferGlobals;
	static rage::atMap<rage::ConstString,Parameter_t*>	s_UnassignGlobalParamsToCBuffer;
	static rage::atMap<rage::ConstString,Parameter_t*>	s_UnassignParamToCBuffer;
	
	static void SetupAndWriteDx10Constants(void* poShader, unsigned int usage, const char* pszProgramName, rage::fiStream* effectStream);
};

#endif

