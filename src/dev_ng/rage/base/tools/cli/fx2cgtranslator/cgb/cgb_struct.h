/*   SCE CONFIDENTIAL                                       */
/*   $PSLibId$ */
/*   Copyright (C) 2007 Sony Computer Entertainment Inc.    */
/*   All Rights Reserved.                                   */

#ifndef __CELL_CGB_STRUCT_H__
#define __CELL_CGB_STRUCT_H__

#if defined(WIN32) || defined(LINUX)
#else
#include <sys/types.h>
#endif

typedef struct CellCgbVertexProgramConfiguration{
	uint16_t	instructionSlot;
	uint16_t	instructionCount;
	uint16_t	attributeInputMask;
	uint8_t		registerCount;
	uint8_t		clipmask;
}CellCgbVertexProgramConfiguration;

typedef struct CellCgbFragmentProgramConfiguration{
	uint32_t	offset;
	uint32_t	attributeInputMask;
	uint16_t	texCoordsInputMask;
	uint16_t	texCoords2D;
	uint16_t	texCoordsCentroid;
	uint32_t	fragmentControl;
	uint8_t		registerCount;
}CellCgbFragmentProgramConfiguration;

#endif // __CELL_CGB_STRUCT_H__
