// 
// fx2cgtranslator/cgpacker.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "cgpacker.h"

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include "system/new.h"		// <new>

// #define dprintf(x) printf x
#define dprintf(x)

uint32_t swap(uint32_t input) 
{
	return (input>>24) | ((input>>8) & 0xFF00) | ((input<<8) & 0xFF0000) | (input << 24);
}

/* static void SwapData(uint32_t *data,size_t wordCount) 
{
	while (wordCount--)
	{
		*data = swap(*data);
		++data;
	}
} */

CgBinaryHeader* CgBinaryHeader::Load(const char *filename) 
{
	FILE *f = fopen(filename,"rb");
	if (!f)
		return NULL;

	fseek(f, 0, SEEK_END);
	size_t size = ftell(f);
	fseek(f, 0, SEEK_SET);

	char *storage = new char[size];

	fread(storage, 1, size, f);

	fclose(f);

	CgBinaryHeader *hdr = (CgBinaryHeader*) storage;

	dprintf(("cg size %u bytes, %u symbols in program, code size %u bytes\n",swap(hdr->totalSize),swap(hdr->symbolCount),swap(hdr->microcodeSize)));
	dprintf(("  sym table at %u, header at %u, ucode at %u\n",swap(hdr->symbolOffset),swap(hdr->microcodeHeaderOffset),swap(hdr->microcodeOffset)));

	return hdr;
}

int CgBinaryHeader::GetSize() const
{
	return swap(totalSize);
}

bool CgBinaryHeader::Save(const char *filename) const
{
	FILE *f = fopen(filename,"wb");
	if (!f)
		return false;

	fwrite(this, swap(totalSize), 1, f);

	fclose(f);

	return true;
}


char *mystrdup(const char *src) 
{
	size_t l = strlen(src) + 1;
	char *result = new char[l+1];
	memcpy(result, src, l+1);
	return result;
}

CgSymbol::CgSymbol()
{
	memset(this, 0, sizeof(*this));
}

CgSymbol::~CgSymbol() 
{
	delete name;
	delete value;
	delete [] patches;
	delete semantic;
}

void CgSymbol::Init(const CgBinaryHeader &header,const CgSymbolData &data)
{
	char *base = (char*)(&header);
	type = swap(data.type);
	binding = swap(data.binding);
	variability = swap(data.variability);
	resourceIndex = swap(data.resourceIndex);
	name = data.nameOffset? mystrdup(base + swap(data.nameOffset)) : NULL;
	value = data.valueOffset? new CgFloat4(*(CgFloat4*)(base + swap(data.valueOffset))) : NULL;
	if (data.patchOffset)
	{
		uint32_t *src = (uint32_t*) (base + swap(data.patchOffset));
		patches = new uint32_t[swap(*src) + 1];
		patches[0] = swap(*src);
		for (uint32_t i=0; i<patches[0]; i++)
			patches[i+1] = swap(src[i+1]);
	}
	else
		patches = NULL;
	semantic = data.semanticOffset? mystrdup(base + swap(data.semanticOffset)) : NULL;
	direction = swap(data.direction);
	parameter = swap(data.parameter);
	usedFlag = swap(data.usedFlag);
	unknown = swap(data.unknown);
}

const char *CgSymbolData::GetName(const CgBinaryHeader & hdr)
{
	return (char*)&hdr + swap(nameOffset);
}

bool CgSymbolData::IsNecessary(const CgBinaryHeader & hdr)
{
	char *name = (char*)&hdr + swap(nameOffset);

	// MODIFY THIS TEST TO SUIT YOUR NEEDS.
	// symbol needs to be used and not varying and not our huge magic bone array
	// bool is_fragment_program = hdr.profile == 0x5c1b0000;
	bool necessary = (usedFlag /*|| is_fragment_program*/) && strncmp(name,"gBoneMtx",8) && strncmp(name,"gAllGlobals",11) && swap(variability) != 4101;
	// printf("sym %s profile %x used %d necessary %d\n",name,hdr.profile,usedFlag,necessary);
	return necessary;
}

CgProgram::CgProgram(const CgBinaryHeader &hdr)
{
	// it appears that the header is always present just before the ucode
	profile = swap(hdr.profile);
	version = swap(hdr.version);

	// delete all unreferenced symbols now.
	symbolCount = 0;
	uint32_t sc = swap(hdr.symbolCount);
	CgSymbolData *sd = (CgSymbolData*) ((char*)&hdr + swap(hdr.symbolOffset));
	for (uint32_t i=0; i<sc; i++) 
	{
		// Is this the first element in an array (and are we a fragment program)
		const char *name = sd[i].GetName(hdr);
		const char *bracket = strchr(name,'[');
		if (profile == 0x1b5c && bracket && !strcmp(bracket,"[0]"))
		{
			int nameLen = strlen(name) - 3;
			uint32_t j, k;
			for (j=i+1; j<sc && strncmp(name,sd[j].GetName(hdr),nameLen) == 0; j++)
				;
			// figure out if any array elements are used
			int anyUsedFlag = 0;
			uint32_t startI = i;
			// if there was a "header" entry with no suffix, make sure it participates in the testing.
			if (i > 0 && strncmp(name,sd[i-1].GetName(hdr),nameLen) == 0)
				--startI;
			for (k=startI; k<j; k++)
				anyUsedFlag |= sd[k].usedFlag;
			// if the header entry wasn't used before but is now, make sure we account for it.
			if (anyUsedFlag && i != startI && !sd[startI].usedFlag)
				++symbolCount;
			// and mark them all used if so
			for (k=startI; k<j; k++)
				sd[k].usedFlag = anyUsedFlag;
		}

		if (sd[i].IsNecessary(hdr)) 
		{
			++symbolCount;
		}
	}

	// now allocate enough space for whatever is left
	symbols = new CgSymbol[symbolCount];
	symbolCount = 0;
	for (uint32_t i=0; i<sc; i++)
	{
		if (sd[i].IsNecessary(hdr))
		{
			CgSymbol &s = symbols[symbolCount++];
			s.Init(hdr,sd[i]);
			dprintf(("%s (%s) (%u) %p %s\n",s.name,
				s.variability==4101?"varying":s.variability==4102?"uniform":s.variability==4103?"constant":"????",
				s.type,
				s.value,s.semantic));
		}
	}
	dprintf(("%u symbols after copy\n",symbolCount));

	microcodeHeaderSize = swap(hdr.microcodeOffset) - swap(hdr.microcodeHeaderOffset);
	microcodeHeader = new char[microcodeHeaderSize];
	assert(hdr.microcodeHeaderOffset);
	memcpy(microcodeHeader, (char*)&hdr + swap(hdr.microcodeHeaderOffset), microcodeHeaderSize);

	microcodeSize = swap(hdr.microcodeSize);
	microcode = new char[microcodeSize];
	memcpy(microcode, (char*)&hdr + swap(hdr.microcodeOffset), microcodeSize);
}


CgBinaryHeader* CgProgram::Convert()
{
	const uint32_t maxSize = 65536;
	char *result = new char[maxSize];
	CgBinaryHeader &hdr = *(CgBinaryHeader*) result;
	uint32_t offset = sizeof(CgBinaryHeader);

	hdr.profile = swap(profile);
	hdr.version = swap(version);
	hdr.symbolCount = swap(symbolCount);
	hdr.symbolOffset = swap(offset);

	CgSymbolData *sd = (CgSymbolData*)(result + offset);
	offset += symbolCount * sizeof(CgSymbolData);

	for (uint32_t i=0; i<symbolCount; i++)
	{
		sd[i].type = swap(symbols[i].type);
		sd[i].binding = swap(symbols[i].binding);
		sd[i].variability = swap(symbols[i].variability);
		sd[i].resourceIndex = swap(symbols[i].resourceIndex);
		if (symbols[i].name)
		{
			sd[i].nameOffset = swap(offset);
			strcpy(result + offset, symbols[i].name);
			offset += strlen(symbols[i].name) + 1;
		}
		else
			sd[i].nameOffset = 0;
		sd[i].valueOffset = 0;	// filled in later
		sd[i].patchOffset = 0;	// filled in later
		if (symbols[i].semantic)
		{
			sd[i].semanticOffset = swap(offset);
			strcpy(result + offset, symbols[i].semantic);
			offset += strlen(symbols[i].semantic) + 1;
		}
		else
			sd[i].semanticOffset = 0;
		sd[i].direction = swap(symbols[i].direction);
		sd[i].parameter = swap(symbols[i].parameter);
		sd[i].usedFlag = swap(symbols[i].usedFlag);
		sd[i].unknown = swap(symbols[i].unknown);
	}
	// Align offset to uint32_t boundary for patch tables, then copy in patch tables.
	while (offset & 3)
		result[offset++] = '\0';
	for (uint32_t i=0; i<symbolCount; i++)
	{
		if (symbols[i].patches)
		{
			sd[i].patchOffset = swap(offset);
			uint32_t *dest = (uint32_t*)(result + offset);
			*dest++ = swap(symbols[i].patches[0]);
			offset += sizeof(uint32_t);
			for (uint32_t j=0; j<symbols[i].patches[0]; j++, offset += sizeof(uint32_t)) 
				*dest++ = swap(symbols[i].patches[j+1]);
		}
	}
	// Next do default values (aligned to 16 bytes)
	while (offset & 15)
		result[offset++] = '\0';
	for (uint32_t i=0; i<symbolCount; i++)
	{
		if (symbols[i].value)
		{
			sd[i].valueOffset = swap(offset);
			memcpy(result + offset, symbols[i].value, sizeof(CgFloat4));
			offset += sizeof(CgFloat4);
		}
	}

	// Finally do the microcode header, then the microcode itself
	hdr.microcodeHeaderOffset = swap(offset);
	memcpy(result + offset, microcodeHeader, microcodeHeaderSize);
	offset += microcodeHeaderSize;

	hdr.microcodeOffset = swap(offset);
	memcpy(result + offset, microcode, microcodeSize);
	hdr.microcodeSize = swap(microcodeSize);
	offset += microcodeSize;

	hdr.totalSize = swap(offset);
	assert(offset < maxSize);

	return &hdr;
}

CgProgram::~CgProgram()
{
	delete[] (char*) microcode;
	delete[] (char*) microcodeHeader;
	delete[] symbols;
}
