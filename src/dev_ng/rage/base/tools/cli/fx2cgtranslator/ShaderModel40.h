#ifndef _SHADERMODEL_40_H
#define _SHADERMODEL_40_H

#include "ShaderModelCommon.h"
#include "Parameters.h"
#include "system/platform.h"



class ShaderModel40
{
public:

	ShaderModel40();
	~ShaderModel40();

	unsigned int	AddGeometryProgram(const char* entry, const char* cgc_flags, const char* target);

	void			BeginGeometryProgram();
	void			EndGeometryProgram();
	void			SaveGeometryProgram(unsigned char index);

	void			SetEffectProperties(const char* dirname, rage::fiStream* effect_stream)	{ mEffectDirName = dirname; mEffectStream = effect_stream; }

	unsigned char	GetGeometryProgramCount() const					{ return mGeometryProgramCount; }
	GeometryProgram	GetGeometryProgram(unsigned char index) const	{ return mGeometryPrograms[index]; }


protected:
	
	GeometryProgram		mGeometryPrograms[kMaxProgramCount];
	char*				mGeometryProgramTarget[kMaxProgramCount];
	unsigned char		mGeometryProgramCount;

	const char			*mEffectDirName;
	rage::fiStream		*mEffectStream;
};

#endif