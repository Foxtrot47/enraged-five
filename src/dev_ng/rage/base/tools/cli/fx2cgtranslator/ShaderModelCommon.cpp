#include "fxccompile.h"
#include "ShaderModelCommon.h"
#include "ShaderModel40.h"
#include "ShaderModel50.h"

#include "effectdata.h"
#include "grammar.h"
#include "atl\map.h"
#include "string/string.h"
#include "system/platform.h"
#include <direct.h>
#include <process.h>

#define PC_ALLOW_OLD_FXC 1
#define PC_USE_WINDOWS8SDK 0

ShaderModel40 sShaderModel40;
ShaderModel50 sShaderModel50;

bool g_bUseWindow8SDK = false;

void* GetShaderModel40()
{
	return &sShaderModel40;
}

void* GetShaderModel50()
{
	return &sShaderModel50;
}

D3D10_SODeclaration::Stream::Stream() : 
										mSemanticIndex(0), 
										mStartComponent(0), 
										mComponentCount(0), 
										mOutputSlot(0)
{
	mSemanticName[0] = '\0';;
}
D3D10_SODeclaration::Stream::~Stream()
{

}
D3D10_SODeclaration::~D3D10_SODeclaration()
{
	mStreamCount = 0;
}
D3D10_SODeclaration::D3D10_SODeclaration() 
{
	mStreamCount = 0;
}

D3D10_SODeclaration::D3D10_SODeclaration(const D3D10_SODeclaration& other) 
{
	mStreamCount = kMaxStreamCount;
	ResetStreams();
	for(int i = 0; i < other.mStreamCount; ++i)
	{
		InsertStream(other.mStreams[i]);
	}
}
D3D10_SODeclaration::D3D10_SODeclaration(const char* flags)
{
	mStreamCount  = 0;
	//"0:POSITION.xyz; 1:NORMAL.xyz; 2:TIMER.x; 3:TYPE.x"
	int currentInFlags = 0;
	while (flags[currentInFlags] != '\0')
	{
		char tmpDecl[kMaxSemanticNameLength];
		int currentInTmpDecl = 0;
		while (flags[currentInFlags] != ';' && flags[currentInFlags] != '\0')
		{
			tmpDecl[currentInTmpDecl++] = flags[currentInFlags];
			currentInFlags++;
		}
		tmpDecl[currentInTmpDecl] = '\0';
		int streamindex = AddStream();
		char *pSizeStr = strchr(tmpDecl, '.');
		*pSizeStr = '\0';
		pSizeStr++;
		strncpy(mStreams[streamindex].mSemanticName, tmpDecl, kMaxSemanticNameLength);
		mStreams[streamindex].mOutputSlot = 0;		// TODO: find how we can use and define it
		mStreams[streamindex].mSemanticIndex = 0;	// TODO: find how we can use and define it
		mStreams[streamindex].mStartComponent = 0;
		mStreams[streamindex].mComponentCount = static_cast<unsigned char>(strlen(pSizeStr));
		if (flags[currentInFlags] != '\0')
			currentInFlags++;

	}
}

D3D10_SODeclaration& D3D10_SODeclaration::operator = (const D3D10_SODeclaration& other)
{
	ResetStreams();
	for(int i = 0; i < other.mStreamCount; ++i)
	{
		InsertStream(other.mStreams[i]);
	}
	return *this;
}

bool D3D10_SODeclaration::operator == (const D3D10_SODeclaration& other) const
{
	if (mStreamCount != other.mStreamCount)
		return false;
	for(int i = 0; i < mStreamCount; ++i)
	{
		const Stream& str0 = mStreams[i];
		const Stream& str1 = other.mStreams[i];

		bool isEqual = strncmp(str0.mSemanticName, str1.mSemanticName, kMaxSemanticNameLength) == 0 &&
			str0.mSemanticIndex == str1.mSemanticIndex &&
			str0.mStartComponent == str1.mStartComponent &&
			str0.mComponentCount == str1.mComponentCount &&
			str0.mOutputSlot == str1.mOutputSlot;
		if (!isEqual) return false;
	}
	return true;
}
void D3D10_SODeclaration::ResetStreams()
{
	Assert(mStreamCount <= kMaxStreamCount);
	//clear the current streams...
	for(int i = 0; i < mStreamCount; ++i)
	{
		mStreams[i].mSemanticName[0] = '\0';
		mStreams[i].mSemanticIndex = static_cast<unsigned char>(kInvalidValue);
		mStreams[i].mStartComponent = static_cast<unsigned char>(kInvalidValue);
		mStreams[i].mComponentCount = static_cast<unsigned char>(kInvalidValue);
		mStreams[i].mOutputSlot = static_cast<unsigned char>(kInvalidValue);
	}
	mStreamCount = 0;
}
int D3D10_SODeclaration::AddStream()
{
	Assert(mStreamCount < kMaxStreamCount);
	int index = mStreamCount++;
	mStreams[index].mSemanticName[0] = '\0';
	mStreams[index].mSemanticIndex = static_cast<unsigned char>(kInvalidValue);
	mStreams[index].mStartComponent = static_cast<unsigned char>(kInvalidValue);
	mStreams[index].mComponentCount = static_cast<unsigned char>(kInvalidValue);
	mStreams[index].mOutputSlot = static_cast<unsigned char>(kInvalidValue);

	return index;
}
int D3D10_SODeclaration::InsertStream(const Stream& str)
{
	Assert(mStreamCount < kMaxStreamCount);
	int index = mStreamCount++;
	strncpy(mStreams[index].mSemanticName, str.mSemanticName, kMaxSemanticNameLength);
	mStreams[index].mSemanticIndex = str.mSemanticIndex;
	mStreams[index].mStartComponent = str.mStartComponent;
	mStreams[index].mComponentCount = str.mComponentCount;
	mStreams[index].mOutputSlot = str.mOutputSlot;

	return index;
}
void D3D10_SODeclaration::WriteStreamOut(rage::fiStream* effectstream)
{
	effectstream->PutCh(mStreamCount);

	for(unsigned int i = 0; i < mStreamCount; ++i)
	{
		int currentNameLen = strlen(mStreams[i].mSemanticName) + 1;
		effectstream->PutCh(static_cast<unsigned char>(currentNameLen));
		effectstream->Write(mStreams[i].mSemanticName, currentNameLen);
		effectstream->PutCh(mStreams[i].mSemanticIndex);
		effectstream->PutCh(mStreams[i].mStartComponent);
		effectstream->PutCh(mStreams[i].mComponentCount);
		effectstream->PutCh(mStreams[i].mOutputSlot);
	}
}

#include "errno.h"

static bool CheckFileExists(const char* path)
{
	rage::fiStream *const stream = rage::fiStream::Open( path, true );
	if (stream)
	{
		stream->Close();
		return true;
	}
	return false;
}

void PCCompileProgram(const char* entry, const char* target, bool backwardsCompatible)
{
	char dirname[256];
	strcpy(dirname,cg_name);
	if (strchr(dirname,'\\') != 0) 
	{
		*strchr(dirname, '\\') = 0;
		_mkdir(dirname);
		strcpy(dirname, cg_name); 
	} 
	*strchr(dirname,'.') = '\0';
	_mkdir(dirname);

	if (rage::g_sysPlatform != rage::platform::WIN32PC) 
	{
		gram_error("this function is only valid for PC builds."); 
	}
#if	PC_USE_WINDOWS8SDK
	const char *WindowsSdk = getenv("WindowsSdkDir");
#endif // PC_USE_WINDOWS8SDK
	const char *DxSdk = getenv("DXSDK_DIR");
	if (DxSdk == NULL)
	{
		DxSdk = "c:\\dxsdk\\"; 
	} 
	
//	char fxc[512]; 
	const char* options = debugInfo ? "/Gfp /Od /Zi /Zpr" : "/Zpr"; 

	char argv0[1024], argv0quoted[1024], argv0_old[1024], argv2[128], argv3[128], argv4[128], argv5[128], arg6[128];

#if	PC_USE_WINDOWS8SDK
	rage::formatf(argv0,sizeof(argv0),"%s\\bin\\x86\\fxc.exe" ,WindowsSdk); // Windows 8 SDK
#endif // PC_USE_WINDOWS8SDK
	rage::formatf(argv0_old,sizeof(argv0_old),"%s\\Utilities\\bin\\x86\\fxc.exe" ,DxSdk); // June 2010 SDK
	rage::formatf(argv0quoted, sizeof(argv0quoted), "\"%s\"", argv0_old);
	rage::formatf(argv2,sizeof(argv2),"/T%s",target);
	rage::formatf(argv3,sizeof(argv3),"/E%s",entry);
	rage::formatf(argv4,sizeof(argv4),"/Fo%s\\%s",dirname,entry);
	rage::formatf(argv5,sizeof(argv5),"/DShaderTarget=%d",target[0]=='v'?0:1);
	rage::formatf(arg6,sizeof(arg6),"/Fc%s\\%s.asm",dirname,entry);

	char fullArgs[1024];

	// Enable output of the .asm files on PC.
	bool outputAsm = true;
	
	//domain and hull shader cannot be compiled with the backwards compatible mode.
	const char *const compatibilityFlag = backwardsCompatible ? "/Gec" : "";

#if	PC_USE_WINDOWS8SDK
	g_bUseWindow8SDK = CheckFileExists(argv0);
#endif // PC_USE_WINDOWS8SDK
	rage::formatf(fullArgs, sizeof(fullArgs), "%s /nologo %s %s %s %s %s %s %s %s",
		PC_ALLOW_OLD_FXC && !g_bUseWindow8SDK ? argv0quoted : argv0,
		argv2, argv3, argv4, argv5, compatibilityFlag, options, cg_name, outputAsm ? arg6 : ""); 
	if (!QueueCommand(fullArgs)) // ExecuteCommand(fullArgs))
	{
		gram_errorf("Unable to compile %s under target %s", entry, target);
		gram_errorf(fullArgs);
		gram_exit(1);
	}
}

int RegisterGeometryProgram(const char *target, const char *entry, const char *cgc_flags)
{
	//sShaderModel50.AddGeometryProgram(entry, cgc_flags);
	return sShaderModel40.AddGeometryProgram(entry, cgc_flags, target);
}

int CompileGeometryProgram(const char *target, const char *entry, const char *cgc_flags)
{
	if (!quiet) gram_warningf("******* Compiling GeometryProgram *******");
	if (!quiet) gram_warningf("\t target(%s) entry(%s) cgc_flags(%s)", target, entry, cgc_flags);
	const int index = RegisterGeometryProgram(target,entry,cgc_flags);
	if (cg_stream)
	{
		cg_stream->Close();
		cg_stream = NULL;
	}

	PCCompileProgram(entry, target, true);
	cg_stream = rage::fiStream::Open(cg_name, false);
	cg_stream->Seek(cg_stream->Size());
	if (!quiet) gram_warningf("*****************************************");
	return index;
}

int RegisterHullProgram(const char *entry, const char *cgc_flags)
{
	return sShaderModel50.AddHullProgram(entry, cgc_flags);
}

int CompileHullProgram(const char *target, const char *entry, const char *cgc_flags)
{
	if (!quiet) gram_warningf("******* Compiling HullProgram *******");
	if (!quiet) gram_warningf("\t target(%s) entry(%s) cgc_flags(%s)", target, entry, cgc_flags);
	const int index = RegisterHullProgram(entry,cgc_flags);
	
	if (cg_stream)
	{
		cg_stream->Close();
		cg_stream = NULL;
	}

	PCCompileProgram(entry, target, true);
	cg_stream = rage::fiStream::Open(cg_name, false);
	cg_stream->Seek(cg_stream->Size());
	if (!quiet) gram_warningf("*****************************************");
	return index;
}

int RegisterDomainProgram(const char *entry, const char *cgc_flags)
{
	return sShaderModel50.AddDomainProgram(entry, cgc_flags);
}

int CompileDomainProgram(const char *target, const char *entry, const char *cgc_flags)
{
	if (!quiet) gram_warningf("******* Compiling DomainProgram *******");
	if (!quiet) gram_warningf("\t target(%s) entry(%s) cgc_flags(%s)", target, entry, cgc_flags);
	const int index = RegisterDomainProgram(entry, cgc_flags);
	
	if (cg_stream)
	{
		cg_stream->Close();
		cg_stream = NULL;
	}

	PCCompileProgram(entry, target, true);
	cg_stream = rage::fiStream::Open(cg_name, false);
	cg_stream->Seek(cg_stream->Size());
	if (!quiet) gram_warningf("*****************************************");
	return index;
}

int RegisterComputeProgram(const char *entry, const char *cgc_flags)
{
	return sShaderModel50.AddComputeProgram(entry, cgc_flags);
}

int CompileComputeProgram(const char *target, const char *entry, const char *cgc_flags)
{
	if (!quiet) gram_warningf("******* Compiling ComputeProgram *******");
	if (!quiet) gram_warningf("\t target(%s) entry(%s) cgc_flags(%s)", target, entry, cgc_flags);
	const int index = RegisterComputeProgram(entry, cgc_flags);

	if (cg_stream)
	{
		cg_stream->Close();
		cg_stream = NULL;
	}

	PCCompileProgram(entry, target, true);
	cg_stream = rage::fiStream::Open(cg_name, false);
	cg_stream->Seek(cg_stream->Size());
	if (!quiet) gram_warningf("*****************************************");
	return index;
}

int SaveGeometryPrograms(const char* dirname, rage::fiStream* effect_stream)
{
	sShaderModel40.SetEffectProperties(dirname, effect_stream);
	effect_stream->PutCh(sShaderModel40.GetGeometryProgramCount());
	for (unsigned char i = 0; i < sShaderModel40.GetGeometryProgramCount(); ++i) 
	{
		sShaderModel40.BeginGeometryProgram();
		sShaderModel40.SaveGeometryProgram(i);
		sShaderModel40.EndGeometryProgram();
	} 

	if (bVerbose)
	{
		// 
		printf("================== Global Buffers ====================\n");
		for (int uIndex=0; uIndex<GlobalEffectData::s_BufferGlobals.GetCount(); ++uIndex)
			printf("%s - Offset %d Size %d\n", GlobalEffectData::s_BufferGlobals[uIndex]->Name, GlobalEffectData::s_BufferGlobals[uIndex]->Regs[rage::GS_TYPE], GlobalEffectData::s_BufferGlobals[uIndex]->Size);

		printf("==================== Local Buffers ====================\n");
		for (int uIndex=0; uIndex<GlobalEffectData::s_BufferLocals.GetCount(); ++uIndex)
			printf("%s - Offset %d Size %d\n", GlobalEffectData::s_BufferLocals[uIndex]->Name, GlobalEffectData::s_BufferLocals[uIndex]->Regs[rage::GS_TYPE], GlobalEffectData::s_BufferLocals[uIndex]->Size);

		printf("==================== Global Variables ====================\n");
		for (int uIndex=0; uIndex<GlobalEffectData::s_Globals.GetCount(); ++uIndex)
			printf("%s - Buffer %s Offset %d Size %d\n", GlobalEffectData::s_Globals[uIndex]->Name, (GlobalEffectData::s_Globals[uIndex]->pParentBuffer != NULL) ? GlobalEffectData::s_Globals[uIndex]->pParentBuffer->Name : "None", GlobalEffectData::s_Globals[uIndex]->ConstantBufferByteOffset, GlobalEffectData::s_Globals[uIndex]->ArraySize);

		printf("==================== Local Variables ====================\n");
		for (int uIndex=0; uIndex<GlobalEffectData::s_Locals.GetCount(); uIndex++)
			printf("%s - Buffer %s Offset %d Size %d\n", GlobalEffectData::s_Locals[uIndex]->Name, (GlobalEffectData::s_Locals[uIndex]->pParentBuffer != NULL) ? GlobalEffectData::s_Locals[uIndex]->pParentBuffer->Name : "None", GlobalEffectData::s_Locals[uIndex]->ConstantBufferByteOffset, GlobalEffectData::s_Locals[uIndex]->ArraySize);

		for (rage::s32 uIndex = 0; uIndex < GlobalEffectData::s_UnassignParamToCBuffer.GetNumSlots(); ++uIndex)
		{
			const rage::atMapEntry<rage::ConstString,Parameter_t*>* poEntry = GlobalEffectData::s_UnassignParamToCBuffer.GetEntry(uIndex);
			if (poEntry != NULL)
			{
				printf("Register '%s' does not have a constant buffer assigned to it\n",poEntry->key);
			}
		}
	}

	for (rage::s32 uIndex = 0; uIndex < GlobalEffectData::s_UnassignGlobalParamsToCBuffer.GetNumSlots(); ++uIndex)
	{
		const rage::atMapEntry<rage::ConstString,Parameter_t*>* poEntry = GlobalEffectData::s_UnassignGlobalParamsToCBuffer.GetEntry(uIndex);
		if (poEntry != NULL)
		{
			gram_warningf("Global Register '%s' does not have a constant buffer assigned to it",poEntry->key);
		}
	}
	return 0;
}

int SaveHullPrograms(const char* dirname, rage::fiStream* effect_stream)
{
	sShaderModel50.SetEffectProperties(dirname, effect_stream);
	effect_stream->PutCh(sShaderModel50.GetHullProgramCount());
	for (unsigned char i = 0; i < sShaderModel50.GetHullProgramCount(); ++i) 
	{
		sShaderModel50.BeginHullProgram();
		sShaderModel50.SaveHullProgram(i);
		sShaderModel50.EndHullProgram();
	} 

	if (bVerbose)
	{
		// 
		printf("================== Global Buffers ====================\n");
		for (int uIndex=0; uIndex<GlobalEffectData::s_BufferGlobals.GetCount(); ++uIndex)
			printf("%s - Offset %d Size %d\n", GlobalEffectData::s_BufferGlobals[uIndex]->Name, GlobalEffectData::s_BufferGlobals[uIndex]->Regs[rage::HS_TYPE], GlobalEffectData::s_BufferGlobals[uIndex]->Size);

		printf("==================== Local Buffers ====================\n");
		for (int uIndex=0; uIndex<GlobalEffectData::s_BufferLocals.GetCount(); ++uIndex)
			printf("%s - Offset %d Size %d\n", GlobalEffectData::s_BufferLocals[uIndex]->Name, GlobalEffectData::s_BufferLocals[uIndex]->Regs[rage::HS_TYPE], GlobalEffectData::s_BufferLocals[uIndex]->Size);

		printf("==================== Global Variables ====================\n");
		for (int uIndex=0; uIndex<GlobalEffectData::s_Globals.GetCount(); ++uIndex)
			printf("%s - Buffer %s Offset %d Size %d\n", GlobalEffectData::s_Globals[uIndex]->Name, (GlobalEffectData::s_Globals[uIndex]->pParentBuffer != NULL) ? GlobalEffectData::s_Globals[uIndex]->pParentBuffer->Name : "None", GlobalEffectData::s_Globals[uIndex]->ConstantBufferByteOffset, GlobalEffectData::s_Globals[uIndex]->ArraySize);

		printf("==================== Local Variables ====================\n");
		for (int uIndex=0; uIndex<GlobalEffectData::s_Locals.GetCount(); uIndex++)
			printf("%s - Buffer %s Offset %d Size %d\n", GlobalEffectData::s_Locals[uIndex]->Name, (GlobalEffectData::s_Locals[uIndex]->pParentBuffer != NULL) ? GlobalEffectData::s_Locals[uIndex]->pParentBuffer->Name : "None", GlobalEffectData::s_Locals[uIndex]->ConstantBufferByteOffset, GlobalEffectData::s_Locals[uIndex]->ArraySize);

		for (rage::s32 uIndex = 0; uIndex < GlobalEffectData::s_UnassignParamToCBuffer.GetNumSlots(); ++uIndex)
		{
			const rage::atMapEntry<rage::ConstString,Parameter_t*>* poEntry = GlobalEffectData::s_UnassignParamToCBuffer.GetEntry(uIndex);
			if (poEntry != NULL)
			{
				printf("Register '%s' does not have a constant buffer assigned to it\n",poEntry->key);
			}
		}
	}

	for (rage::s32 uIndex = 0; uIndex < GlobalEffectData::s_UnassignGlobalParamsToCBuffer.GetNumSlots(); ++uIndex)
	{
		const rage::atMapEntry<rage::ConstString,Parameter_t*>* poEntry = GlobalEffectData::s_UnassignGlobalParamsToCBuffer.GetEntry(uIndex);
		if (poEntry != NULL)
		{
			gram_warningf("Global Register '%s' does not have a constant buffer assigned to it",poEntry->key);
		}
	}
	return 0;
}

int SaveDomainPrograms(const char* dirname, rage::fiStream* effect_stream)
{
	sShaderModel50.SetEffectProperties(dirname, effect_stream);
	effect_stream->PutCh(sShaderModel50.GetDomainProgramCount());
	for (unsigned char i = 0; i < sShaderModel50.GetDomainProgramCount(); ++i) 
	{
		sShaderModel50.BeginDomainProgram();
		sShaderModel50.SaveDomainProgram(i);
		sShaderModel50.EndDomainProgram();
	} 

	if (bVerbose)
	{
		// 
		printf("================== Global Buffers ====================\n");
		for (int uIndex=0; uIndex<GlobalEffectData::s_BufferGlobals.GetCount(); ++uIndex)
			printf("%s - Offset %d Size %d\n", GlobalEffectData::s_BufferGlobals[uIndex]->Name, GlobalEffectData::s_BufferGlobals[uIndex]->Regs[rage::DS_TYPE], GlobalEffectData::s_BufferGlobals[uIndex]->Size);

		printf("==================== Local Buffers ====================\n");
		for (int uIndex=0; uIndex<GlobalEffectData::s_BufferLocals.GetCount(); ++uIndex)
			printf("%s - Offset %d Size %d\n", GlobalEffectData::s_BufferLocals[uIndex]->Name, GlobalEffectData::s_BufferLocals[uIndex]->Regs[rage::DS_TYPE], GlobalEffectData::s_BufferLocals[uIndex]->Size);

		printf("==================== Global Variables ====================\n");
		for (int uIndex=0; uIndex<GlobalEffectData::s_Globals.GetCount(); ++uIndex)
			printf("%s - Buffer %s Offset %d Size %d\n", GlobalEffectData::s_Globals[uIndex]->Name, (GlobalEffectData::s_Globals[uIndex]->pParentBuffer != NULL) ? GlobalEffectData::s_Globals[uIndex]->pParentBuffer->Name : "None", GlobalEffectData::s_Globals[uIndex]->ConstantBufferByteOffset, GlobalEffectData::s_Globals[uIndex]->ArraySize);

		printf("==================== Local Variables ====================\n");
		for (int uIndex=0; uIndex<GlobalEffectData::s_Locals.GetCount(); uIndex++)
			printf("%s - Buffer %s Offset %d Size %d\n", GlobalEffectData::s_Locals[uIndex]->Name, (GlobalEffectData::s_Locals[uIndex]->pParentBuffer != NULL) ? GlobalEffectData::s_Locals[uIndex]->pParentBuffer->Name : "None", GlobalEffectData::s_Locals[uIndex]->ConstantBufferByteOffset, GlobalEffectData::s_Locals[uIndex]->ArraySize);

		for (rage::s32 uIndex = 0; uIndex < GlobalEffectData::s_UnassignParamToCBuffer.GetNumSlots(); ++uIndex)
		{
			const rage::atMapEntry<rage::ConstString,Parameter_t*>* poEntry = GlobalEffectData::s_UnassignParamToCBuffer.GetEntry(uIndex);
			if (poEntry != NULL)
			{
				printf("Register '%s' does not have a constant buffer assigned to it\n",poEntry->key);
			}
		}
	}

	for (rage::s32 uIndex = 0; uIndex < GlobalEffectData::s_UnassignGlobalParamsToCBuffer.GetNumSlots(); ++uIndex)
	{
		const rage::atMapEntry<rage::ConstString,Parameter_t*>* poEntry = GlobalEffectData::s_UnassignGlobalParamsToCBuffer.GetEntry(uIndex);
		if (poEntry != NULL)
		{
			gram_warningf("Global Register '%s' does not have a constant buffer assigned to it",poEntry->key);
		}
	}
	return 0;
}

int SaveComputePrograms(const char* dirname, rage::fiStream* effect_stream)
{
	sShaderModel50.SetEffectProperties(dirname, effect_stream);
	effect_stream->PutCh(sShaderModel50.GetComputeProgramCount());
	for (unsigned char i = 0; i < sShaderModel50.GetComputeProgramCount(); ++i) 
	{
		sShaderModel50.BeginComputeProgram();
		sShaderModel50.SaveComputeProgram(i);
		sShaderModel50.EndComputeProgram();
	} 

	if (bVerbose)
	{
		// 
		printf("================== Global Buffers ====================\n");
		for (int uIndex=0; uIndex<GlobalEffectData::s_BufferGlobals.GetCount(); ++uIndex)
			printf("%s - Offset %d Size %d\n", GlobalEffectData::s_BufferGlobals[uIndex]->Name, GlobalEffectData::s_BufferGlobals[uIndex]->Regs[rage::CS_TYPE], GlobalEffectData::s_BufferGlobals[uIndex]->Size);

		printf("==================== Local Buffers ====================\n");
		for (int uIndex=0; uIndex<GlobalEffectData::s_BufferLocals.GetCount(); ++uIndex)
			printf("%s - Offset %d Size %d\n", GlobalEffectData::s_BufferLocals[uIndex]->Name, GlobalEffectData::s_BufferLocals[uIndex]->Regs[rage::CS_TYPE], GlobalEffectData::s_BufferLocals[uIndex]->Size);

		printf("==================== Global Variables ====================\n");
		for (int uIndex=0; uIndex<GlobalEffectData::s_Globals.GetCount(); ++uIndex)
			printf("%s - Buffer %s Offset %d Size %d\n", GlobalEffectData::s_Globals[uIndex]->Name, (GlobalEffectData::s_Globals[uIndex]->pParentBuffer != NULL) ? GlobalEffectData::s_Globals[uIndex]->pParentBuffer->Name : "None", GlobalEffectData::s_Globals[uIndex]->ConstantBufferByteOffset, GlobalEffectData::s_Globals[uIndex]->ArraySize);

		printf("==================== Local Variables ====================\n");
		for (int uIndex=0; uIndex<GlobalEffectData::s_Locals.GetCount(); uIndex++)
			printf("%s - Buffer %s Offset %d Size %d\n", GlobalEffectData::s_Locals[uIndex]->Name, (GlobalEffectData::s_Locals[uIndex]->pParentBuffer != NULL) ? GlobalEffectData::s_Locals[uIndex]->pParentBuffer->Name : "None", GlobalEffectData::s_Locals[uIndex]->ConstantBufferByteOffset, GlobalEffectData::s_Locals[uIndex]->ArraySize);

		for (rage::s32 uIndex = 0; uIndex < GlobalEffectData::s_UnassignParamToCBuffer.GetNumSlots(); ++uIndex)
		{
			const rage::atMapEntry<rage::ConstString,Parameter_t*>* poEntry = GlobalEffectData::s_UnassignParamToCBuffer.GetEntry(uIndex);
			if (poEntry != NULL)
			{
				printf("Register '%s' does not have a constant buffer assigned to it\n",poEntry->key);
			}
		}
	}

	for (rage::s32 uIndex = 0; uIndex < GlobalEffectData::s_UnassignGlobalParamsToCBuffer.GetNumSlots(); ++uIndex)
	{
		const rage::atMapEntry<rage::ConstString,Parameter_t*>* poEntry = GlobalEffectData::s_UnassignGlobalParamsToCBuffer.GetEntry(uIndex);
		if (poEntry != NULL)
		{
			gram_warningf("Global Register '%s' does not have a constant buffer assigned to it",poEntry->key);
		}
	}
	return 0;
}
