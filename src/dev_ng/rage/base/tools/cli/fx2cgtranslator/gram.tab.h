typedef union {
	int ival;
	float fval;
	const char *sval;
	Symbol_t *sym;
	bool boolval;
	Leadin_t *leadin;
	Type_t *type;
	Expr_t *expr;
	ExprList_t *elist;
	Annotation_t *anno;
	Technique_t *tech;
	Pass_t *pass;
	State_t *state;
	RenderState_t *rstate;
	Param_t *param;
	StateTemp_t *statetemp;
} YYSTYPE;
#define	SHARED	258
#define	UNIFORM	259
#define	TECHNIQUE	260
#define	PASS	261
#define	STRUCT	262
#define	STATIC	263
#define	CONST	264
#define	OUT	265
#define	INOUT	266
#define	SAMPLER_STATE	267
#define	COLUMN_MAJOR	268
#define	ROW_MAJOR_	269
#define	COMPILE	270
#define	_NULL_	271
#define	TRUE	272
#define	FALSE	273
#define	VOID	274
#define	VERTEXSHADER	275
#define	PIXELSHADER	276
#define	NOINTERPOLATION	277
#define	PROPERTY	278
#define	NEWSYM	279
#define	STRUCTNAME	280
#define	FUNCNAME	281
#define	VARNAME	282
#define	INVALID	283
#define	SEMANTIC	284
#define	BODY	285
#define	TYPENAME	286
#define	SAMPLERSTATE_STATE	287
#define	SAMPLERSTATE_VALUE	288
#define	RENDERSTATE	289
#define	RENDERVALUE	290
#define	TECHNAME	291
#define	PASSNAME	292
#define	FLOATLIT	293
#define	INTLIT	294
#define	STRLIT	295
#define	PASSTHROUGH	296
#define	CBUFFER	297
#define	COMPILESHADER	298
#define	CONSTRUCTGSWITHSO	299
#define	INSIDE_CENTROID	300
#define	INSIDE_SAMPLE	301
#define	GEOMETRYSHADER	302
#define	GROUPSHARED	303
#define	SETGEOMETRYSHADER	304
#define	SETPIXELSHADER	305
#define	SETVERTEXSHADER	306
#define	SETBLENDSTATE	307
#define	SETDEPTHSTENCILSTATE	308
#define	SETRASTERIZERSTATE	309
#define	TECHNIQUE10	310
#define	INPUTPATCH	311
#define	OUTPUTPATCH	312
#define	SETCOMPUTESHADER	313
#define	SETDOMAINSHADER	314
#define	SETHULLSHADER	315
#define	TECHNIQUE11	316
#define	DX10TEXTURESAMPLERTOKEN	317
#define	DECLARE_POSITION	318
#define	DECLARE_POSITION_CLIPPLANES	319


extern YYSTYPE gram_lval;
