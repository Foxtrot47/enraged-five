@echo off

setlocal
pushd "%~dp0"

CALL setenv.bat

set BUILD_FOLDER=%RS_BUILDBRANCH%
set RAGE_DIR=%CD%\..\..\..\..\..\rage
set SCE_PS3_ROOT=X:/ps3sdk/dev/usr/local/430_001/cell

ECHO LOAD_SLN ENVIRONMENT
ECHO BUILD_FOLDER: 	%BUILD_FOLDER%
ECHO RAGE_DIR: 		%RAGE_DIR%
ECHO SCE_PS3_ROOT: 	%SCE_PS3_ROOT%
ECHO END LOAD_SLN ENVIRONMENT

start "" %cd%\fx2cg_2008.sln

popd
