#ifndef _GRAMMAR_H_
#define _GRAMMAR_H_

#include "file/stream.h"
#include "file/asset.h"

extern void WriteString(rage::fiStream &S,const char *s);
extern void gram_errorf(const char*,...);
extern void gram_warningf(const char*,...);
extern void gram_exit(unsigned int code);

extern bool				debugInfo;
extern bool				ps4razor;
extern bool				bVerbose;
extern bool				quiet;
extern char				cg_name[256];
extern rage::fiStream	*cg_stream;
extern rage::fiStream	*gram_stream;
extern rage::fiStream	*effect_stream;

extern char				s_CurrentFile[256];
extern int				line;

#if RSG_PC
extern bool				bNVStereo;
#endif
#endif
