#include "fxccompile.h"

#include <stdio.h>

#pragma warning(disable: 4668)
#include <windows.h>
#pragma warning(error: 4668)

#include "atl/string.h"
#include "atl/queue.h"
#include "system/ipc.h"
#include "system/taskheader.h"
#include "system/task.h"

// This code was roughly taken from this link:
// http://msdn.microsoft.com/en-us/library/bb509710(v=VS.85).aspx#using_the_effect-compiler_tool_in_a_subprocess
//
// Compiling shaders using a sub-process needs to handle clearing the stdout and stderr buffers properly otherwise the tool can hang indefinitely.
// We saw problems on GTA when this sort of thing happened so hopefully this will fix the problems going forward.

int ExecuteCommand(const char* commandline, bool stripStdErr)
{
	DWORD dwWaitResult = 0;

	HANDLE hReadOutPipe_Read = NULL;
	HANDLE hReadErrorPipe_Read = NULL;

	HANDLE hReadOutPipe = NULL;
	HANDLE hReadErrorPipe = NULL;

	PROCESS_INFORMATION piProcInfo;
	STARTUPINFO siStartInfo;
	SECURITY_ATTRIBUTES saAttr;

	const DWORD BUFSIZE = 256 * 1024;
	BYTE buff[BUFSIZE];

	saAttr.nLength = sizeof(SECURITY_ATTRIBUTES);
	saAttr.bInheritHandle = TRUE;
	saAttr.lpSecurityDescriptor = NULL;

	if (!CreatePipe(&hReadErrorPipe_Read, &hReadErrorPipe, &saAttr, 0))
		return 1;
	if (!SetHandleInformation(hReadErrorPipe_Read, HANDLE_FLAG_INHERIT, 0))
		return 1;

	if (!CreatePipe(&hReadOutPipe_Read, &hReadOutPipe, &saAttr, 0))
		return 1;
	if (!SetHandleInformation(hReadOutPipe_Read, HANDLE_FLAG_INHERIT, 0))
		return 1;

	ZeroMemory(&piProcInfo, sizeof(piProcInfo));
	ZeroMemory(&siStartInfo, sizeof(siStartInfo));

	siStartInfo.cb = sizeof(STARTUPINFO);
	siStartInfo.hStdError = hReadErrorPipe;
	siStartInfo.hStdOutput = hReadOutPipe;
	siStartInfo.dwFlags |= STARTF_USESTDHANDLES;

	BOOL bSuccess = CreateProcessA(	NULL,
		(LPSTR)commandline,
		NULL,
		NULL,
		TRUE,
		0,
		NULL,
		NULL,
		&siStartInfo,
		&piProcInfo);

	if (!bSuccess)
	{
		fprintf(stderr, "Could not start process: %s", commandline);
		return 1;
	}

	HANDLE WaitHandles[] = {
		piProcInfo.hProcess, hReadOutPipe, hReadErrorPipe
	};

	const int ciMaxAttempts = 8;
	int iAttempts = 0;
	while (iAttempts < ciMaxAttempts)
	{
		DWORD dwBytesRead, dwBytesAvailable;

		dwWaitResult = WaitForMultipleObjects(3, WaitHandles, false, 8000);

		while( PeekNamedPipe(hReadOutPipe_Read, NULL, 0, NULL, &dwBytesAvailable, NULL) && dwBytesAvailable )
		{
			ReadFile(hReadOutPipe_Read, buff, BUFSIZE-1, &dwBytesRead, 0);
			buff[dwBytesRead] = '\0';

			printf("%s", buff);
		}

		if( !stripStdErr )
		{
			while( PeekNamedPipe(hReadErrorPipe_Read, NULL, 0, NULL, &dwBytesAvailable, NULL) && dwBytesAvailable )
			{
				ReadFile(hReadErrorPipe_Read, buff, BUFSIZE-1, &dwBytesRead, 0);
				buff[dwBytesRead] = '\0';

				printf("%s", buff);
			}
		}

		if(dwWaitResult == WAIT_OBJECT_0)
			break;

		if (dwWaitResult == WAIT_TIMEOUT)
			iAttempts++;
	}

	if (dwWaitResult == WAIT_TIMEOUT)
	{
		printf("Warning - Process timed out.\n");
		//return 1;
	}

	DWORD dwExitCode;

	GetExitCodeProcess(piProcInfo.hProcess, &dwExitCode);
	if (dwExitCode != 0 && !stripStdErr)
	{
		printf("Error - Exit Code %d %s\n", dwExitCode, dwExitCode != 1? "- check for bad DirectX install (prior to June 2010) on that machine; look in IB waterfall display to find it" : "", dwExitCode);
	}

	return dwExitCode;
}

void DisableEnvVar(const char* envVar)
{
	SetEnvironmentVariable(envVar,"");
}

struct ExecuteState
{
	PROCESS_INFORMATION piProcInfo;
	HANDLE WaitHandles[3];
	HANDLE hReadOutPipe_Read;
	HANDLE hReadErrorPipe_Read;
	bool inUse;
	
	ExecuteState()
	{
		inUse = false;
	}
};

#define MAX_PROGRAMS 2048
rage::atFixedArray<rage::atString, MAX_PROGRAMS> m_CommandQueue;

bool QueueCommand(const char* commandline)
{
	m_CommandQueue.Push(rage::atString(commandline));
	return true;
}

void JobUpdate(rage::sysTaskParameters& taskParams)
{
	if (!ExecuteAsyncCommand((char*)taskParams.Input.Data, *(int*)taskParams.Output.Data))
	{
		printf("error: unable to execute %s\n", (char*)taskParams.Input.Data);
		*(int*)taskParams.Output.Data = -1;
		return;
	}
	DWORD dwExitCode = WaitOnExecute(*(int*)taskParams.Output.Data, false);
	if (dwExitCode)
	{
		printf("error: unable to execute %d\n", *(int*)taskParams.Output.Data);
		//break;
	}
	*(int*)taskParams.Output.Data = dwExitCode;
}

int WaitOnQueue(bool /*stripStdErr*/)
{
	rage::sysTaskHandle aTaskHandles[MAX_PROGRAMS];
	rage::atFixedArray<rage::atString, MAX_PROGRAMS> m_JobsQueue;
	rage::atFixedArray<int, MAX_PROGRAMS> m_JobsResults;

	rage::sysTaskManager::InitClass();

	int numThreads = rage::sysTaskManager::GetNumThreads();

	// SN-DBS support. The number of threads is specified in the tool.ini file so use this in order
	// for SN-DBS to properly handle the number of threads being spawned.
	char numThreadEnvVar[512];
	if (GetEnvironmentVariable("OMP_NUM_THREADS", numThreadEnvVar, sizeof(numThreadEnvVar)))
	{
		int iNumThreadsEnvVar = atoi(numThreadEnvVar);
		if (iNumThreadsEnvVar)
			numThreads = iNumThreadsEnvVar;

		printf("SN-DBS: number of threads set by environment variable %d\n", numThreads);
	}

	int iSchedulerIndex = rage::sysTaskManager::AddScheduler("Compile", 64 * 1024, 4096, rage::PRIO_IDLE, ((rage::u64)1 << (rage::u64)numThreads) - 1, 0);

	for(int loop = 0; loop < m_CommandQueue.GetCount(); loop++)
	{
		const rage::atString &strCommand = m_CommandQueue[loop];
		rage::sysTaskParameters taskParams;
		taskParams.Input.Data = (void*)strCommand.c_str();
		taskParams.Input.Size = sizeof(void*);

		m_JobsResults.Push(loop);
		taskParams.Output.Data = &m_JobsResults[loop];
		taskParams.Output.Size = sizeof(m_JobsResults[loop]);

		rage::sysTaskHandle& task = aTaskHandles[loop];		
		task = rage::sysTaskManager::Create(TASK_INTERFACE(JobUpdate), taskParams, iSchedulerIndex);
	}

	if (m_CommandQueue.GetCount() > 0)
	{
		rage::sysTaskManager::WaitMultiple(m_CommandQueue.GetCount(), aTaskHandles);
	}

	DWORD dwFinalExitCode = 0;
	
	for(int loop = 0; loop < m_CommandQueue.GetCount(); loop++)
	{
		DWORD dwExitCode = m_JobsResults[loop];
		dwFinalExitCode |= dwExitCode;
		if (dwExitCode)
		{
			printf("error: unable to execute %s\n", m_CommandQueue[loop].c_str());
			//break;
		}
	}
	return (dwFinalExitCode != 0) ? false : true;
}

#define MAX_EXECUTING	MAX_PROGRAMS
ExecuteState stateArray[MAX_EXECUTING];

bool ExecuteAsyncCommand(const char* commandline, int stateId)
{
	HANDLE hReadOutPipe = NULL;
	HANDLE hReadErrorPipe = NULL;

	STARTUPINFO siStartInfo;
	SECURITY_ATTRIBUTES saAttr;

	saAttr.nLength = sizeof(SECURITY_ATTRIBUTES);
	saAttr.bInheritHandle = TRUE;
	saAttr.lpSecurityDescriptor = NULL;
	
	if (!CreatePipe(&stateArray[stateId].hReadErrorPipe_Read, &hReadErrorPipe, &saAttr, 0))
		return false;
	if (!SetHandleInformation(stateArray[stateId].hReadErrorPipe_Read, HANDLE_FLAG_INHERIT, 0))
		return false;

	if (!CreatePipe(&stateArray[stateId].hReadOutPipe_Read, &hReadOutPipe, &saAttr, 0))
		return false;
	if (!SetHandleInformation(stateArray[stateId].hReadOutPipe_Read, HANDLE_FLAG_INHERIT, 0))
		return false;

	ZeroMemory(&stateArray[stateId].piProcInfo, sizeof(stateArray[stateId].piProcInfo));
	ZeroMemory(&siStartInfo, sizeof(siStartInfo));

	siStartInfo.cb = sizeof(STARTUPINFO);
	siStartInfo.hStdError = hReadErrorPipe;
	siStartInfo.hStdOutput = hReadOutPipe;
	siStartInfo.dwFlags |= STARTF_USESTDHANDLES;

	BOOL bSuccess = CreateProcessA(	NULL,
		(LPSTR)commandline,
		NULL,
		NULL,
		TRUE,
		0,
		NULL,
		NULL,
		&siStartInfo,
		&stateArray[stateId].piProcInfo);

	if (!bSuccess)
	{
		fprintf(stderr, "Could not start process: %s", commandline);
		return false;
	}

	stateArray[stateId].WaitHandles[0] = stateArray[stateId].piProcInfo.hProcess;
	stateArray[stateId].WaitHandles[1] = hReadOutPipe;
	stateArray[stateId].WaitHandles[2] = hReadErrorPipe;
	
	return true;
}

int WaitOnExecute(int stateId, bool stripStdErr)
{
	const DWORD BUFSIZE = 256 * 1024;
	BYTE buff[BUFSIZE];

	DWORD dwWaitResult = 0;

	const int ciMaxAttempts = 8;
	int iAttempts = 0;
	while (iAttempts < ciMaxAttempts)
	{
		DWORD dwBytesRead, dwBytesAvailable;

		dwWaitResult = WaitForMultipleObjects(3, stateArray[stateId].WaitHandles, false, 8000);

		while( PeekNamedPipe(stateArray[stateId].hReadOutPipe_Read, NULL, 0, NULL, &dwBytesAvailable, NULL) && dwBytesAvailable )
		{
			ReadFile(stateArray[stateId].hReadOutPipe_Read, buff, BUFSIZE-1, &dwBytesRead, 0);
			buff[dwBytesRead] = '\0';

			printf("%s", buff);
		}

		if( !stripStdErr )
		{
			while( PeekNamedPipe(stateArray[stateId].hReadErrorPipe_Read, NULL, 0, NULL, &dwBytesAvailable, NULL) && dwBytesAvailable )
			{
				ReadFile(stateArray[stateId].hReadErrorPipe_Read, buff, BUFSIZE-1, &dwBytesRead, 0);
				buff[dwBytesRead] = '\0';

				printf("%s", buff);
			}
		}

		if(dwWaitResult == WAIT_OBJECT_0)
			break;

		if (dwWaitResult == WAIT_TIMEOUT)
			iAttempts++;
	}

	if (dwWaitResult == WAIT_TIMEOUT)
	{
		printf("Warning - Process timed out.\n");
		//return 1;
	}

	DWORD dwExitCode;

	GetExitCodeProcess(stateArray[stateId].piProcInfo.hProcess, &dwExitCode);
	if (dwExitCode != 0 && !stripStdErr)
	{
		printf("Error - Exit Code %d %s\n", dwExitCode, dwExitCode != 1? "- check for bad DirectX install (prior to June 2010) on that machine; look in IB waterfall display to find it" : "", dwExitCode);
	}

	stateArray[stateId].inUse = false;
	return dwExitCode;
}
