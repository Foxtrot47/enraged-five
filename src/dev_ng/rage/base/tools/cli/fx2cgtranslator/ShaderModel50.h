#ifndef _SHADERMODEL_50_H_
#define _SHADERMODEL_50_H_

#include "ShaderModelCommon.h"

class ShaderModel50
{
public:
	ShaderModel50();
	~ShaderModel50();

	unsigned int	AddComputeProgram(const char* entry, const char* cgc_flags);
	unsigned int	AddDomainProgram(const char* entry, const char* cgc_flags);
	unsigned int	AddGeometryProgram(const char* entry, const char* cgc_flags);
	unsigned int	AddHullProgram(const char* entry, const char* cgc_flags);

	void			SaveComputeProgram(unsigned char index);
	void			SaveDomainProgram(unsigned char index);
	void			SaveGeometryProgram(unsigned char index);
	void			SaveHullProgram(unsigned char index);

	void			BeginComputeProgram();
	void			EndComputeProgram();
	void			BeginDomainProgram();
	void			EndDomainProgram();
	void			BeginGeometryProgram();
	void			EndGeometryProgram();
	void			BeginHullProgram();
	void			EndHullProgram();



	void			SetEffectProperties(const char* dirname, rage::fiStream* effect_stream)	{ mEffectDirName = dirname; mEffectStream = effect_stream; }

	unsigned char	GetComputeProgramCount() const					{ return mComputeProgramCount; }
	ComputeProgram	GetComputeProgram(unsigned char index) const	{ return mComputePrograms[index]; }
	unsigned char	GetDomainProgramCount() const					{ return mDomainProgramCount; }
	DomainProgram	GetDomainProgram(unsigned char index) const		{ return mDomainPrograms[index]; }
	unsigned char	GetGeometryProgramCount() const					{ return mGeometryProgramCount; }
	GeometryProgram	GetGeometryProgram(unsigned char index) const	{ return mGeometryPrograms[index]; }
	unsigned char	GetHullProgramCount() const						{ return mHullProgramCount; }
	HullProgram		GetHullProgram(unsigned char index) const		{ return mHullPrograms[index]; }

protected:

	ComputeProgram		mComputePrograms[kMaxProgramCount];
	DomainProgram		mDomainPrograms[kMaxProgramCount];
	GeometryProgram		mGeometryPrograms[kMaxProgramCount];
	HullProgram			mHullPrograms[kMaxProgramCount];

	unsigned char		mComputeProgramCount;
	unsigned char		mDomainProgramCount;
	unsigned char		mGeometryProgramCount;
	unsigned char		mHullProgramCount;

	const char			*mEffectDirName;
	rage::fiStream		*mEffectStream;
};


#endif
