#include "Shadermodel40.h"

#include "grammar.h"
#include "effectdata.h"

extern void GetShaderStructure(void* pvCode, unsigned int uCodeSize, void** poShader);
extern void ReleaseShader(void* poShader);

bool SetupData(void* poShader, ConstantBuffer_t *poConstantBuffer, unsigned int usage, const char* pszProgramName);
bool SetupData(void* poShader, Parameter_t *poParameter, unsigned int usage, const char* pszProgramName);

ShaderModel40::ShaderModel40() : mGeometryProgramCount(0), mEffectDirName(0), mEffectStream(0)
{
	AddGeometryProgram(NULL, "","gs_4_0");
}
ShaderModel40::~ShaderModel40()
{
	//should we delete the dublicated entry points
}
void ShaderModel40::BeginGeometryProgram()
{

}
void ShaderModel40::EndGeometryProgram()
{

}

void ShaderModel40::SaveGeometryProgram(unsigned char index)
{
	Assert(index < mGeometryProgramCount);
	Assert(mEffectStream != 0);
	Assert(mEffectDirName != 0);

	GeometryProgram gsProgram = mGeometryPrograms[index];
	if (gsProgram.mName == NULL) 
	{
		unsigned int zero = 0;
		mEffectStream->PutCh(5);			// currentNameLen
		mEffectStream->Write("NULL",5);		// Name
		mEffectStream->PutCh(0);			// constantCount
		if (GlobalEffectData::s_ShaderVersion >= 40) 
			mEffectStream->PutCh(0);			// constantBufferCount
		mEffectStream->PutCh(0);			// streamout entry count
		mEffectStream->WriteInt(&zero,1);	// ucodeSize

		return;
	}

	char ucodeName[256];
	Assert(strlen(mEffectDirName) < 256);
	strcpy(ucodeName, mEffectDirName);
	strcat(ucodeName,"/");
	strcat(ucodeName, gsProgram.mName);
	const char *currentName = gsProgram.mName;
	int currentNameLen = strlen(currentName) + 1;

	rage::fiStream *ucodeFile = rage::fiStream::Open(ucodeName);
	Assert(ucodeFile);
	rage::u32 ucodeSize;
	ucodeSize = ucodeFile->Size();
	char *ucode = new char[ucodeSize];
	ucodeFile->Read(ucode, ucodeSize);
	ucodeFile->Close();


	mEffectStream->PutCh(static_cast<unsigned char>(currentNameLen));
	mEffectStream->Write(currentName,currentNameLen);

	void *poShader;
	GetShaderStructure(ucode, ucodeSize, &poShader);
	if (poShader == NULL)
	{
		gram_errorf("Invalid GS shader version used must be version 4.0");
	}
	
	GlobalEffectData::SetupAndWriteDx10Constants(poShader, rage::USAGE_GEOMETRYPROGRAM, currentName, mEffectStream);

	gsProgram.mDx10StreamOut.WriteStreamOut(mEffectStream);

	mEffectStream->WriteInt(&ucodeSize,1);
	mEffectStream->Write(ucode, ucodeSize);

	if (ucodeSize > 0)
	{	// Write the shader model version number (geometry shaders available on dx10+)

		const char* target = mGeometryProgramTarget[index];
		AssertMsg(target[3] >= '4' && target[3] <= '5', "Unsupported shader model");
		AssertMsg(target[5] >= '0' && target[5] <= '9', "Unrecognised shader model string");
		unsigned char major = target[3] - '0';
		unsigned char minor = target[5] - '0';

		mEffectStream->WriteByte(&major,1);
		mEffectStream->WriteByte(&minor,1);
		
		//printf("writing %s, sm: %d, %d\n", currentName, major, minor);
	}

	ReleaseShader(poShader);
	delete[] ucode;
}
unsigned int ShaderModel40::AddGeometryProgram(const char* entry, const char* cgc_flags, const char* target)
{
	D3D10_SODeclaration tmpDecl = D3D10_SODeclaration(cgc_flags);
	for (unsigned int i=1; i < mGeometryProgramCount; ++i)
	{
		const GeometryProgram& pGeometryProgram = mGeometryPrograms[i];
		if (strcmp(entry, pGeometryProgram.mName) == 0) 
		{
			if (pGeometryProgram.mDx10StreamOut != tmpDecl) 
			{
				gram_errorf("geometry entry point '%s' compiled with a different Stream Out  = '%s' before, now '%s'\n", entry, pGeometryProgram.mDx10StreamOut.AsString(), tmpDecl.AsString());
			}
			return i;
		}
	}
	if (mGeometryProgramCount == kMaxProgramCount)
	{
		gram_errorf("Maximum Number Of Geometry Programs is Reached, exiting...");
	}
	mGeometryPrograms[mGeometryProgramCount].mDx10StreamOut = tmpDecl;
	mGeometryPrograms[mGeometryProgramCount].mName = rage::StringDuplicate(entry);
	mGeometryProgramTarget[mGeometryProgramCount] = rage::StringDuplicate(target);
	
	return mGeometryProgramCount++;
}