#include "system/d3d11.h"
#include "file/stream.h"
#include <d3d10shader.h>
#include "string/stringhash.h"
#include "ShaderModel40.h"
#include "grcore/effect.h"

#if !__OPTIMIZED
#define D3D_DEBUG_INFO
#endif // !__OPTIMIZED

//#define __SAL_H_FULL_VER 140050727
//#pragma warning(disable:4668)
#include <d3d11.h>
//#pragma warning(error:4668)

#include <D3Dcompiler.h>
#include <D3D11shader.h>

#pragma comment(lib,"dxguid.lib")
// Statically linking against this shit breaks IB shader builds.
// #pragma comment(lib,"d3d11.lib")
// #pragma comment(lib,"d3dx11.lib")
// #pragma comment(lib,"d3dcompiler.lib")

#include "grcore/effect.h"
#include "grammar.h"

extern bool g_bUseWindow8SDK;

void GetShaderStructure(void* pvCode, unsigned int uCodeSize, void** poShader)
{
	ID3D11ShaderReflection *pShader;
	//ID3D10ShaderReflection1 *pShader;

	//call to D3DReflect results in E_NOINTERFACE on Win7
	HMODULE hCompiler = NULL;
	if (g_bUseWindow8SDK)
	{
		hCompiler = LoadLibrary("D3DCOMPILER_46.DLL");	// Windows 8 SDK
		if (hCompiler == NULL)
		{
			poShader = NULL;
			gram_errorf("Failed to load D3DCOMPILER_46.DLL - Please reinstall the Windows 8 SDK");
			return;
		}
	}
	else
	{
		hCompiler = LoadLibrary("D3DCOMPILER_43.DLL");	// Dated March 2010, might require June 2010 SDK
		if (hCompiler == NULL)
		{
			poShader = NULL;
			gram_errorf("Failed to load D3DCOMPILER_43.DLL - Please reinstall the Windows 8 SDK and June 2010 DirectX SDK");
			return;
		}
	}

	typedef HRESULT (WINAPI *D3DReflect_t)(LPCVOID pSrcData, SIZE_T SrcDataSize, REFIID pInterface, void **ppReflector); 
	static D3DReflect_t D3DReflect;
	if (!D3DReflect) {
		D3DReflect = (D3DReflect_t) GetProcAddress(hCompiler, "D3DReflect");
		if (!D3DReflect)
		{
			gram_warningf("Failed to get D3D11 Reflection Shader Interface");
			poShader = NULL;
			return;
		}
	}

	const HRESULT hr = D3DReflect(pvCode, uCodeSize, IID_ID3D11ShaderReflection, (void**)&pShader);
	if (hr == S_OK)
	{
		*poShader = pShader;
	}
	else
	{
		gram_errorf("D3DReflect call failed (error %x, might be a 3_0 shader, or maybe reinstall the June 2010 DirectX SDK or later)",hr);
		*poShader = NULL;
	}
}

static bool IsConstantExported( const D3D_SHADER_INPUT_TYPE type )	{
	return	type == D3D10_SIT_TEXTURE					|| type == D3D_SIT_STRUCTURED ||
			type == D3D11_SIT_UAV_RWTYPED				|| type == D3D11_SIT_UAV_RWSTRUCTURED ||
			type == D3D11_SIT_UAV_RWBYTEADDRESS			|| type == D3D11_SIT_UAV_APPEND_STRUCTURED ||
			type == D3D11_SIT_UAV_CONSUME_STRUCTURED	|| type == D3D11_SIT_UAV_RWSTRUCTURED_WITH_COUNTER;
}

unsigned int GetConstantCount(void *poShader)
{
	ID3D11ShaderReflection *pShader = (ID3D11ShaderReflection*)poShader;
	if (pShader == NULL)
		return 0;

	D3D11_SHADER_DESC desc;
	unsigned int uCount = 0;
	pShader->GetDesc(&desc);

	for (unsigned int uConstBuf = 0; uConstBuf < desc.ConstantBuffers; uConstBuf++)
	{
		ID3D11ShaderReflectionConstantBuffer* poConstBuffer = pShader->GetConstantBufferByIndex(uConstBuf);
		D3D11_SHADER_BUFFER_DESC oDesc;
		poConstBuffer->GetDesc(&oDesc);
		uCount += oDesc.Variables;
	}

	for (unsigned int uResources = 0; uResources < desc.BoundResources; uResources++)
	{
		D3D11_SHADER_INPUT_BIND_DESC oDesc;
		HRESULT hr = pShader->GetResourceBindingDesc(uResources, &oDesc);
		if (SUCCEEDED(hr) && IsConstantExported(oDesc.Type))
		{
			++uCount;
		}
	}
	return uCount;
}

void WriteConstant(void* poShader, rage::fiStream *S, unsigned int uIndex)
{
	ID3D11ShaderReflection *pShader = (ID3D11ShaderReflection*)poShader;
	if (pShader == NULL)
		return;

	D3D11_SHADER_DESC desc;
	unsigned int uCount = 0;
	pShader->GetDesc(&desc);

	for (unsigned int uConstBuf = 0; uConstBuf < desc.ConstantBuffers; uConstBuf++)
	{
		ID3D11ShaderReflectionConstantBuffer* poConstBuffer = pShader->GetConstantBufferByIndex(uConstBuf);
		D3D11_SHADER_BUFFER_DESC oDesc;
		poConstBuffer->GetDesc(&oDesc);
		for (unsigned int uVariables = 0; uVariables < oDesc.Variables; uVariables++)
		{
			if (uIndex == uCount)
			{
				ID3D11ShaderReflectionVariable* poVariable = poConstBuffer->GetVariableByIndex(uVariables);
				D3D11_SHADER_VARIABLE_DESC oVarDesc;
				poVariable->GetDesc(&oVarDesc);
				WriteString(*S, oVarDesc.Name);
				return;
			}
			uCount++;
		}
	}

	for (unsigned int uResources = 0; uResources < desc.BoundResources; uResources++)
	{
		D3D11_SHADER_INPUT_BIND_DESC oDesc;
		HRESULT hr = pShader->GetResourceBindingDesc(uResources, &oDesc);
		if (SUCCEEDED(hr) && IsConstantExported(oDesc.Type))
		{
			if (uIndex == uCount)
			{
				WriteString(*S, oDesc.Name);
				return;
			}
			uCount++;
		}
	}
}

void ReleaseShader(void* poShader)
{
	ID3D11ShaderReflection *pShader = (ID3D11ShaderReflection*)poShader;
	if (pShader == NULL)
		return;

	pShader->Release();
}


static bool IsResourceTexture(const D3D11_SHADER_INPUT_BIND_DESC &ResDesc, const Parameter_t &Parameter)
{
	return (Parameter.Type->VarType == rage::grcEffect::VT_TEXTURE) &&
		((ResDesc.Type == D3D10_SIT_TEXTURE) || (ResDesc.Type == D3D10_SIT_SAMPLER));
}

static bool IsResourceStructuredBuffer(const D3D11_SHADER_INPUT_BIND_DESC &ResDesc, const Parameter_t &Parameter)
{
	return (Parameter.Type->VarType == rage::grcEffect::VT_STRUCTUREDBUFFER) &&
		(ResDesc.Type == D3D11_SIT_STRUCTURED);
}
static bool IsResourceUnordered(const D3D11_SHADER_INPUT_BIND_DESC &ResDesc, const Parameter_t &Parameter)
{
	return
		(Parameter.Type->VarType == rage::grcEffect::VT_UAV_TEXTURE		&& ResDesc.Type == D3D11_SIT_UAV_RWTYPED) ||
		(Parameter.Type->VarType == rage::grcEffect::VT_UAV_STRUCTURED	&& (
			ResDesc.Type == D3D11_SIT_UAV_RWSTRUCTURED				||
			ResDesc.Type == D3D11_SIT_UAV_APPEND_STRUCTURED			||
			ResDesc.Type == D3D11_SIT_UAV_CONSUME_STRUCTURED		||
			ResDesc.Type == D3D11_SIT_UAV_RWSTRUCTURED_WITH_COUNTER ));
}

rage::ShaderType GetShaderTypeFromUsage(unsigned int usage)
{
	switch (usage)
	{
	case rage::USAGE_VERTEXPROGRAM:	return rage::VS_TYPE; break;
	case rage::USAGE_FRAGMENTPROGRAM:	return rage::PS_TYPE; break;
	case rage::USAGE_GEOMETRYPROGRAM:	return rage::GS_TYPE; break;
	case rage::USAGE_HULLPROGRAM:		return rage::HS_TYPE; break;
	case rage::USAGE_DOMAINPROGRAM:	return rage::DS_TYPE; break;
	case rage::USAGE_COMPUTEPROGRAM:	return rage::CS_TYPE; break;
	}
	gram_errorf("Constant buffer usage to shader type conversion fails");
	return rage::NONE_TYPE;
}
bool SetupData(void* poShader, ConstantBuffer_t *poConstantBuffer, unsigned int usage, const char* pszProgramName)
{
	ID3D11ShaderReflection *pShader = (ID3D11ShaderReflection*)poShader;
	if (poConstantBuffer->Name == NULL)
	{
		gram_warningf("Constant Buffer Name for Program %s is NULL", pszProgramName);
		return false;
	}

	if (pShader == NULL)
	{
		gram_warningf("Shader Reflection Interface is NULL");
		return false;
	}

	D3D11_SHADER_DESC desc;
	pShader->GetDesc(&desc);

	for (unsigned int uConstBuf = 0; uConstBuf < desc.ConstantBuffers; uConstBuf++)
	{
		ID3D11ShaderReflectionConstantBuffer* poConstBuffer = pShader->GetConstantBufferByIndex(uConstBuf);
		D3D11_SHADER_BUFFER_DESC oDesc;
		poConstBuffer->GetDesc(&oDesc);
		if (!strcmp(oDesc.Name, poConstantBuffer->Name))
		{
			poConstantBuffer->RegCount = oDesc.Variables;
			poConstantBuffer->Size = oDesc.Size;
			poConstantBuffer->Usage |= usage;

			bool bFoundBindPosition = false;
			for (unsigned int uBind = 0; uBind < desc.BoundResources; uBind++)
			{
				D3D11_SHADER_INPUT_BIND_DESC oBindDesc;
				if (pShader->GetResourceBindingDesc(uBind, &oBindDesc) == S_OK)
				{
					if (!strcmp(oDesc.Name, oBindDesc.Name))
					{
						unsigned short uRegister = static_cast<unsigned short>(oBindDesc.BindPoint);
						if (oBindDesc.Type == D3D10_SIT_CBUFFER)
						{
							//if (poConstantBuffer->AssigningProgram != NULL && poConstantBuffer->Regs[GetShaderTypeFromUsage(usage)] != -1 && poConstantBuffer->Regs[GetShaderTypeFromUsage(usage)] != uRegister )
							//{
							//	gram_warningf("constant Buffer (%s)is used at a different bind position in another program", poConstantBuffer->Name);
							//	gram_warningf("Was -- program: %s reg: %d", poConstantBuffer->AssigningProgram, poConstantBuffer->Regs[GetShaderTypeFromUsage(usage)] );
							//	gram_warningf("Now -- program: %s reg: %d", pszProgramName, uRegister );
							//}
							poConstantBuffer->Regs[GetShaderTypeFromUsage(usage)] = uRegister;
							poConstantBuffer->AssigningProgram = pszProgramName;
						}
						bFoundBindPosition = true;
						break;
					}
				}
				else
				{
					gram_errorf("Unexpected parsing of bind data for constant buffer %s", poConstantBuffer->Name);
					return false;
				}
			}
			if (!bFoundBindPosition)
			{
				gram_errorf("Failed to locate bind position for constant buffer %s", poConstantBuffer->Name);
				return false;
			}
			return true;
		} 
	}
	//gram_warningf("Failed to locate bind position for constant buffer %s in program %s", poConstantBuffer->Name, pszProgramName);
	return false;
}

bool SetupData(void* poShader, Parameter_t *poParameter, unsigned int usage, const char* pszProgramName)
{
	ID3D11ShaderReflection *pShader = (ID3D11ShaderReflection*)poShader;
	if (pShader == NULL)
	{
		gram_warningf("Shader Reflection Interface is NULL");
		return false;
	}

	D3D11_SHADER_DESC desc;
	pShader->GetDesc(&desc);

	for (unsigned int uConstBuf = 0; uConstBuf < desc.ConstantBuffers; uConstBuf++)
	{
		ID3D11ShaderReflectionConstantBuffer* poConstBuffer = pShader->GetConstantBufferByIndex(uConstBuf);
		D3D11_SHADER_BUFFER_DESC oDesc;
		poConstBuffer->GetDesc(&oDesc);
		for (unsigned int uVariables = 0; uVariables < oDesc.Variables; uVariables++)
		{
			ID3D11ShaderReflectionVariable* poVariable = poConstBuffer->GetVariableByIndex(uVariables);
			D3D11_SHADER_VARIABLE_DESC oVarDesc;
			poVariable->GetDesc(&oVarDesc);
			if (!strcmp(oVarDesc.Name, poParameter->Name))
			{
				if (poParameter->ConstantBufferByteOffset == 0xFFFF)
				{
					poParameter->ConstantBufferByteOffset = oVarDesc.StartOffset;
					poParameter->AssigningProgram = pszProgramName;
				}
				else if ( poParameter->ConstantBufferByteOffset != (int)oVarDesc.StartOffset)
				{
					gram_errorf("%s is placed at different bind positions %d %d", poParameter->Name, poParameter->ConstantBufferByteOffset, oVarDesc.StartOffset);
				}
				
				poParameter->Usage |= usage;//

				//poParameter->ConstantBufferNameHash = rage::atStringHash(oDesc.Name);
				poParameter->pParentBuffer = ConstantBuffer_t::Lookup(oDesc.Name);
				if (poParameter->pParentBuffer == NULL)
				{
					gram_errorf("Parameter %s did not hook up to constant buffer properly", poParameter->Name);
				}

				if(oVarDesc.uFlags & D3D_SVF_USED || poParameter->isNoStrip)
				{
					poParameter->Used = true;
				}				
				return true;
			}
		}
	}

	bool bFound = desc.BoundResources ? false : true;
	for (unsigned int uResourceIndex = 0; uResourceIndex < desc.BoundResources; uResourceIndex++)
	{
		D3D11_SHADER_INPUT_BIND_DESC oResDesc;
		pShader->GetResourceBindingDesc(uResourceIndex, &oResDesc);
		if ((!strcmp(oResDesc.Name, poParameter->Name)) &&
			( IsResourceTexture(oResDesc, *poParameter) || IsResourceStructuredBuffer(oResDesc, *poParameter) || IsResourceUnordered(oResDesc,*poParameter) ))
		{			
			if (oResDesc.Type == D3D10_SIT_TEXTURE)
			{
				if (poParameter->Reg != (int)oResDesc.BindPoint)
				{
					if (poParameter->AssigningProgram != NULL)
					{
						gram_warningf("%s is placed at different bind positions %s - %d %s - %d", poParameter->Name, poParameter->AssigningProgram, poParameter->Reg, pszProgramName, oResDesc.BindPoint);
					}
					poParameter->Reg = oResDesc.BindPoint;					
				}
				if (poParameter->ConstantBufferByteOffset == 0xFFFF) //Texture2D can be used as a parameter in shadermodel 50
				{
					poParameter->ConstantBufferByteOffset = oResDesc.BindPoint;
				}
				else if ( poParameter->ConstantBufferByteOffset != (int)oResDesc.BindPoint)
				{
					gram_warningf("%s is placed at different bind positions %d %d", poParameter->Name, poParameter->ConstantBufferByteOffset, oResDesc.BindPoint);
				}
				poParameter->AssigningProgram = pszProgramName;
			}
			else if (oResDesc.Type == D3D10_SIT_SAMPLER)
			{
				if (poParameter->ConstantBufferByteOffset == 0xFFFF)
				{
					poParameter->ConstantBufferByteOffset = oResDesc.BindPoint;
				}
				else if ( poParameter->ConstantBufferByteOffset != (int)oResDesc.BindPoint)
				{
					gram_warningf("%s is placed at different bind positions %d %d", poParameter->Name, poParameter->ConstantBufferByteOffset, oResDesc.BindPoint);
				}
			}
			else //if (oResDesc.Type == D3D11_SIT_STRUCTURED || oResDesc.Type == D3D11_SIT_UAV_RWSTRUCTURED || oResDesc.Type == D3D11_SIT_UAV_RWTYPED)
			{
				if (poParameter->Reg != (int)oResDesc.BindPoint)
				{
					if (poParameter->AssigningProgram != NULL)
					{
						gram_warningf("%s is placed at different bind positions %s - %d %s - %d", poParameter->Name, poParameter->AssigningProgram, poParameter->Reg, pszProgramName, oResDesc.BindPoint);
					}
					poParameter->Reg = oResDesc.BindPoint;					
				}
				if (poParameter->ConstantBufferByteOffset == 0xFFFF)
				{
					poParameter->ConstantBufferByteOffset = oResDesc.BindPoint;
				}
				else if ( poParameter->ConstantBufferByteOffset != (int)oResDesc.BindPoint)
				{
					gram_warningf("%s is placed at different bind positions %d %d", poParameter->Name, poParameter->ConstantBufferByteOffset, oResDesc.BindPoint);
				}
				poParameter->AssigningProgram = pszProgramName;
			}
			//poParameter->ConstantBufferNameHash = 0;
			poParameter->pParentBuffer = NULL;
			poParameter->Used = true;
			poParameter->Usage |= usage;
			bFound = true;
		}
	}	

	return bFound;
}

void OutputBindingDescription(D3D11_SIGNATURE_PARAMETER_DESC oDesc)
{
	gram_warningf("%d - %s", oDesc.SemanticIndex, oDesc.SemanticName);
}

void OutputBindings(ID3D11ShaderReflection* poOutputShader)
{
	D3D11_SHADER_DESC OutputDesc;
	poOutputShader->GetDesc(&OutputDesc);

	for (unsigned int uIndex = 0; uIndex < OutputDesc.OutputParameters; uIndex++)
	{
		D3D11_SIGNATURE_PARAMETER_DESC OutputBindDesc;
		poOutputShader->GetOutputParameterDesc(uIndex, &OutputBindDesc);

		OutputBindingDescription(OutputBindDesc);
	}
}

void InputBindings(ID3D11ShaderReflection* poInputShader)
{
	D3D11_SHADER_DESC InputDesc;
	poInputShader->GetDesc(&InputDesc);

	for (unsigned int uIndex = 0; uIndex < InputDesc.InputParameters; uIndex++)
	{
		D3D11_SIGNATURE_PARAMETER_DESC InputBindDesc;
		poInputShader->GetInputParameterDesc(uIndex, &InputBindDesc);

		OutputBindingDescription(InputBindDesc);
	}
}


namespace unique
{
	static const D3D_NAME vertexOutputs[] =
	{
		D3D_NAME_CULL_DISTANCE	// can't leave the static array empty
	};
	static const D3D_NAME fragmentInputs[] =
	{
		D3D10_NAME_IS_FRONT_FACE,
		D3D10_NAME_SAMPLE_INDEX,
		D3D10_NAME_COVERAGE
	};

	template<int N>
	bool match(const D3D_NAME target, const D3D_NAME (&exList)[N])
	{
		for(int i=0; i<N; ++i)	{
			if (exList[i] == target)
				return true;
		}
		return false;
	}
	
	bool matchVertexOutputs(const D3D11_SIGNATURE_PARAMETER_DESC &desc)	
	{
		return match( desc.SystemValueType, vertexOutputs );
	}

	bool matchFragmentInputs(const D3D11_SIGNATURE_PARAMETER_DESC &desc)
	{
		return match( desc.SystemValueType, fragmentInputs );
	}
}


bool CompareBindings(void* pvOutput, void* pvInput)
{
	ID3D11ShaderReflection *poOutputShader = (ID3D11ShaderReflection*)pvOutput;
	if (poOutputShader == NULL)
	{
		gram_warningf("Shader Reflection Interface is NULL");
		return false;
	}

	ID3D11ShaderReflection *poInputShader = (ID3D11ShaderReflection*)pvInput;
	if (poInputShader == NULL)
	{
		gram_warningf("Shader Reflection Interface is NULL");
		return false;
	}


	D3D11_SHADER_DESC OutputDesc;
	poOutputShader->GetDesc(&OutputDesc);

	D3D11_SHADER_DESC InputDesc;
	poInputShader->GetDesc(&InputDesc);

	int nVertexOutputs=0, nFragmentInputs=0;
	unsigned iIndex=0;

	for (iIndex=0; iIndex<OutputDesc.OutputParameters; iIndex++)
	{
		D3D11_SIGNATURE_PARAMETER_DESC OutputBindDesc;
		poOutputShader->GetOutputParameterDesc(iIndex, &OutputBindDesc);
		nVertexOutputs += !unique::matchVertexOutputs( OutputBindDesc );
	}

	for (iIndex=0; iIndex<InputDesc.InputParameters; iIndex++)
	{
		D3D11_SIGNATURE_PARAMETER_DESC InputBindDesc;
		poInputShader->GetInputParameterDesc(iIndex, &InputBindDesc);
		nFragmentInputs += !unique::matchFragmentInputs( InputBindDesc );
	}

	if (nFragmentInputs > nVertexOutputs)
	{
		gram_warningf("Mismatching number of parameters");
		gram_warningf("\nOutput Bindings");
		OutputBindings(poOutputShader);
		gram_warningf("\nInput Bindings");
		InputBindings(poInputShader);
		return false;
	}

	int iOutputOffset = 0;
	int iInputOffset = 0; 
	for (iIndex = 0; iIndex < (int)(InputDesc.InputParameters - iInputOffset); iIndex++)
	{
		D3D11_SIGNATURE_PARAMETER_DESC InputBindDesc;
		poInputShader->GetInputParameterDesc(iIndex + iInputOffset, &InputBindDesc);

		if (unique::matchFragmentInputs(InputBindDesc))
		{
			iOutputOffset--;
			continue;
		}

		Assert( iIndex + iOutputOffset < OutputDesc.OutputParameters );
		
		D3D11_SIGNATURE_PARAMETER_DESC OutputBindDesc;
		poOutputShader->GetOutputParameterDesc(iIndex + iOutputOffset, &OutputBindDesc);

		if (unique::matchVertexOutputs(OutputBindDesc))
		{
			iInputOffset--;
			continue;
		}

		if (OutputBindDesc.SystemValueType != D3D10_NAME_UNDEFINED) 
		{
			// Skip I think
			if ((InputBindDesc.SystemValueType == D3D10_NAME_UNDEFINED) /*|| (OutputBindDesc.SystemValueType == D3D10_NAME_VIEWPORT_ARRAY_INDEX)*/)
			{
				iInputOffset--;
				continue;
			}
		}

		// Compare components
		bool bMatches = true;
		if (InputBindDesc.ComponentType != OutputBindDesc.ComponentType)
			bMatches = false;

		/*	
		if (InputBindDesc.ReadWriteMask != OutputBindDesc.ReadWriteMask)
			bMatches = false;

		if (InputBindDesc.Mask != OutputBindDesc.Mask)
			bMatches = false;
		*/

		if (InputBindDesc.Register != OutputBindDesc.Register)
		{
			gram_warningf("Bind Parameter Registers Mismatch Output %s%d - %d Input %s%d - %d", 
				OutputBindDesc.SemanticName, OutputBindDesc.SemanticIndex, OutputBindDesc.Register,
				InputBindDesc.SemanticName, InputBindDesc.SemanticIndex, InputBindDesc.Register);
			bMatches = false;
		}

		if (InputBindDesc.SemanticIndex != OutputBindDesc.SemanticIndex)
			bMatches = false;

		if (InputBindDesc.Stream != OutputBindDesc.Stream)
			bMatches = false;

		if (InputBindDesc.SystemValueType != OutputBindDesc.SystemValueType)
			bMatches = false;

		if (strcmp(InputBindDesc.SemanticName, OutputBindDesc.SemanticName) || !bMatches)
		{
			gram_warningf("Bind Parameter Index %d Mismatches Output %s Input %s", iIndex, OutputBindDesc.SemanticName, InputBindDesc.SemanticName);

			gram_warningf("\nOutput Bindings");
			OutputBindings(poOutputShader);
			gram_warningf("\nInput Bindings");
			InputBindings(poInputShader);

			return false;
		}
	}
	return true;
}

