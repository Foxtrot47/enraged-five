// 
// fx2cgtranslator/fx2cg.cpp 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
//
// Documentation can be found at https://rage.rockstargames.com/index.php?title=Fx2cg_Tips

#include "file/stream.h"
#include "system/param.h"
#include "system/platform.h"
#include "system/timer.h"
#include "effectdata.h"

extern int gram_parse();
extern int gram_debug;
extern rage::fiStream *gram_stream;
extern const char *out_name;
extern char cg_name[256];
extern bool useRandomizer;
extern int randomizerMaxIteration;
extern bool noPerformanceDump;
extern bool quiet;
extern bool debugInfo;
extern bool debugParams;
extern bool ps4razor;
extern bool bVerbose;
extern bool bValidateLinkage;
extern bool bEnableThreading;
extern char g_shaderPlatform;
extern bool bEnableCompression;
#if RSG_PC
extern bool bNVStereo;
#endif
PARAM(platform,"Target platform (default pc; can be xenon or psn)");
PARAM(gram_debug,"Enable grammar debugging");
PARAM(useRandomizer,"Enable use of the randomized compiler for fragment shader on psn (warning : rather long...)");
PARAM(randomizerMaxIteration,"Indicate the number of iteration over shader randomization (default : 10)");
PARAM(quiet,"Disable some annoying warnings.");
PARAM(noPerformanceDump,"Disable performance dump (num cycle/ALU count) (mainly to avoid a problem with incredibuild/psn builds)");
PARAM(shadermodel,"Shader Version (30; can be 30/40/41 or 50)");
PARAM(debugInfo, "Generate shader debug information for Win32 PC");
PARAM(ps4razor, "Force generation of shader debug files for use with PS4 Razor GPU captures");
PARAM(outputParamData, "Output to console window data about parameters");
PARAM(verbose, "Display Lots of Information about compiled shader");
PARAM(novalidation, "Disable Validation of Shader Linkage");
PARAM(nothreading, "Disable threaded builds");
PARAM(noCompression, "Disable shader platform specific binary compression if available");
#if RSG_PC
PARAM(nvStereo, "Indicates that we are compiling nv stereo shaders");
#endif
// #pragma comment(lib,"/usr/local/cell/host-win32/Cg/lib/libcgc.lib")

using namespace rage;

int main(int argc,char **argv)
{
	// sysTimer T;
	sysParam::Init(argc,argv);

#if 0 // Display Command line arguments
	for (int iIndex = 0; iIndex < argc; iIndex++)
	{
		Displayf("%s ", argv[iIndex]);
	}
#endif

	// Get target platform (default to ps3 to avoid breaking existing scripts)
	const char *platform = "ps3";
	PARAM_platform.Get(platform);
	g_sysPlatform = g_shaderPlatform = sysGetPlatform(platform);

	// Durango and Orbis aren't fully-fledged separate platforms yet because they borrow x64 resources.
	// But the shader compiler needs to know the difference.  We pretend the system platform is still
	// PC so things like byte swapping are done correctly.
	if (!stricmp(platform,"durango")) {
		g_sysPlatform = platform::WIN32PC;
		g_shaderPlatform = platform::DURANGO;
		GlobalEffectData::s_ShaderVersion = 50;
	}
	else if (!stricmp(platform,"orbis")) {
		g_sysPlatform = platform::WIN32PC;
		g_shaderPlatform = platform::ORBIS;
		GlobalEffectData::s_ShaderVersion = 50;
	}

	if (PARAM_gram_debug.Get())
		gram_debug = 1;

	if (PARAM_useRandomizer.Get())
		useRandomizer = true;
		
	if (PARAM_noPerformanceDump.Get())
		noPerformanceDump = true;

	if (PARAM_randomizerMaxIteration.Get())
		PARAM_randomizerMaxIteration.Get(randomizerMaxIteration);

	if (PARAM_quiet.Get())
		quiet = true;

	if (PARAM_debugInfo.Get())
 		debugInfo = true;

	if (PARAM_ps4razor.Get())
 		ps4razor = true;

	if (PARAM_outputParamData.Get())
		debugParams = true;

	if (PARAM_verbose.Get())
		bVerbose = true;

	if (PARAM_novalidation.Get())
		bValidateLinkage = false;

	if (PARAM_nothreading.Get())
		bEnableThreading = false;

	if (PARAM_noCompression.Get())
		bEnableCompression = false;

#if RSG_PC
	if (PARAM_nvStereo.Get())
 		bNVStereo = true;
#endif

	PARAM_shadermodel.Get(GlobalEffectData::s_ShaderVersion);

		
	// Set up platform-specific output file.
	// char ext[5] = "._gx";
	// ext[1] = g_sysPlatform;

	const char *cg_ext = (g_sysPlatform==platform::PS3 || g_sysPlatform==platform::PSP2)? ".cg" : ".hlsl";

	gram_stream = sysParam::GetArgCount()>2? rage::fiStream::Open(sysParam::GetArg(1)) : NULL;
	out_name = sysParam::GetArg(2);

	int result = 1;
	if (gram_stream) {
		strcpy(cg_name,sysParam::GetArg(1));
		if (!strrchr(cg_name,'.'))
			strcat(cg_name,cg_ext);
		else
			strcpy(strrchr(cg_name,'.'),cg_ext);
		result = gram_parse();

		gram_stream->Close();
		
	}
	else
	{
		fprintf(stderr, "Failed to Open %s", sysParam::GetArg(1));
	}

	if (result == 0)
	{
		Displayf("Completed %s\n",cg_name);
	}
	else
	{
		fprintf(stderr, "Failed to compile %s", cg_name);
	}
	return result;
}
