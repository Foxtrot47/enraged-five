#ifndef _FXC_COMPILE_H
#define _FXC_COMPILE_H

#ifndef _WIN32_WINNT		// Allow use of features specific to Windows XP or later.                   
#define _WIN32_WINNT 0x0501	// Change this to the appropriate value to target other versions of Windows.
#endif

int ExecuteCommand(const char* commandline, bool stripStdErr = false);
void DisableEnvVar(const char* envVar);

bool QueueCommand(const char* commandline);
bool ExecuteAsyncCommand(const char* commandline, int stateId);

int WaitOnQueue(bool stripStdErr);
int WaitOnExecute(int stateId, bool stripStdErr);

#endif