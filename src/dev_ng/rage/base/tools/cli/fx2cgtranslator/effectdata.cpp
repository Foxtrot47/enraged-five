#include "effectdata.h"
#include "grammar.h"

extern bool SetupData(void* poShader, ConstantBuffer_t *poConstantBuffer, unsigned int usage, const char* pszProgramName);
extern bool SetupData(void* poShader, Parameter_t *poParameter, unsigned int usage, const char* pszProgramName);
extern rage::ShaderType GetShaderTypeFromUsage(unsigned int usage);
extern unsigned int GetConstantCount(void *poShader);
extern void WriteConstant(void* poShader, rage::fiStream *S, unsigned int uIndex);


int											GlobalEffectData::s_ShaderVersion = 30;
rage::atMap<rage::ConstString,StateTemp_t*>	GlobalEffectData::s_SamplerStates;
rage::atMap<rage::ConstString,StateTemp_t*>	GlobalEffectData::s_RenderStates;
rage::atArray<Parameter_t*>					GlobalEffectData::s_Locals;
rage::atArray<Parameter_t*>					GlobalEffectData::s_Globals;
rage::atArray<Parameter_t*>					GlobalEffectData::s_Constants;
rage::atArray<ConstantBuffer_t*>			GlobalEffectData::s_BufferLocals;
rage::atArray<ConstantBuffer_t*>			GlobalEffectData::s_BufferGlobals;
rage::atMap<rage::ConstString,Parameter_t*>	GlobalEffectData::s_UnassignGlobalParamsToCBuffer;
rage::atMap<rage::ConstString,Parameter_t*>	GlobalEffectData::s_UnassignParamToCBuffer;


void GlobalEffectData::SetupAndWriteDx10Constants(void* poShader, unsigned int usage, const char* pszProgramName, rage::fiStream* effectStream)
{
	if (s_ShaderVersion < 40)
	{
		gram_errorf("cannot have constant buffers for Dx9 Shaders");
	}
	unsigned char cbcount = 0;
	ConstantBuffer_t* constantBuffers[14];
	memset(constantBuffers, 0, 14*sizeof(ConstantBuffer_t*));
	
	for (int uIndex=0; uIndex < GlobalEffectData::s_BufferGlobals.GetCount(); ++uIndex)
	{
		if (SetupData(poShader, GlobalEffectData::s_BufferGlobals[uIndex], usage, pszProgramName))
		{
			constantBuffers[cbcount] = GlobalEffectData::s_BufferGlobals[uIndex];
			cbcount++;
		}
	}
	for (int uIndex=0; uIndex < GlobalEffectData::s_BufferLocals.GetCount(); ++uIndex)
	{
		if (SetupData(poShader, GlobalEffectData::s_BufferLocals[uIndex], usage, pszProgramName))
		{
			constantBuffers[cbcount] = GlobalEffectData::s_BufferLocals[uIndex];
			cbcount++;
		}
	}
	rage::u8 constantCount = static_cast<rage::u8>(GetConstantCount(poShader));
	effectStream->PutCh(constantCount);

	for (rage::u32 uIndex = 0; uIndex < constantCount; uIndex++)
	{
		WriteConstant(poShader, effectStream, uIndex);
	}

	effectStream->PutCh(cbcount);
	for(int i = 0; i < cbcount; i++)
	{
		WriteString(*effectStream, constantBuffers[i]->Name);
		effectStream->WriteShort(&(constantBuffers[i]->Regs[GetShaderTypeFromUsage(usage)]), 1);
	}

	for (int uIndex=0; uIndex < GlobalEffectData::s_Globals.GetCount(); ++uIndex)
	{
		SetupData(poShader, GlobalEffectData::s_Globals[uIndex], usage, pszProgramName);
		if (GlobalEffectData::s_Globals[uIndex]->pParentBuffer == GlobalEffectData::s_BufferLocals[0])
		{
			GlobalEffectData::s_UnassignGlobalParamsToCBuffer.Delete(GlobalEffectData::s_Globals[uIndex]->Name);
			GlobalEffectData::s_UnassignGlobalParamsToCBuffer[rage::ConstString(GlobalEffectData::s_Globals[uIndex]->Name)] = GlobalEffectData::s_Globals[uIndex];
		}
	}

	for (int uIndex=0; uIndex < GlobalEffectData::s_Locals.GetCount(); ++uIndex)
	{
		SetupData(poShader, GlobalEffectData::s_Locals[uIndex], usage, pszProgramName);
		if (GlobalEffectData::s_Locals[uIndex]->pParentBuffer == GlobalEffectData::s_BufferLocals[0])
		{
			GlobalEffectData::s_UnassignParamToCBuffer.Delete(GlobalEffectData::s_Locals[uIndex]->Name);
			GlobalEffectData::s_UnassignParamToCBuffer[rage::ConstString(GlobalEffectData::s_Locals[uIndex]->Name)] = GlobalEffectData::s_Locals[uIndex];
		}
	}
}