top: bottom

include ..\..\..\..\rage\build\Makefile.template

.PHONY: FORCE
TARGET = fx2cg
ELF = $(INTDIR)/$(TARGET)_$(INTDIR).ppu.elf
SELF = $(INTDIR)/$(TARGET)_$(INTDIR).ppu.self

bottom: $(SELF)

$(SELF): $(ELF)
	make_fself $(ELF) $(SELF)

LIBS += ..\..\..\..\rage\base\src\vcproj\RageCore\$(INTDIR)\RageCore.lib

..\..\..\..\rage\base\src\vcproj\RageCore\$(INTDIR)\RageCore.lib: FORCE
	$(MAKE) -C ..\..\..\..\rage\base\src\vcproj\RageCore -f RageCore.mk

LIBS += ..\..\..\..\rage\stlport\STLport-5.0RC5\src/$(INTDIR)/stlport.lib

..\..\..\..\rage\stlport\STLport-5.0RC5\src/$(INTDIR)/stlport.lib: FORCE
	$(MAKE) -C ..\..\..\..\rage\stlport\STLport-5.0RC5\src -f stlport.mk

INCLUDES = -I ..\..\..\..\rage\stlport\STLport-5.0RC5\stlport -I ..\..\..\..\rage\base\src

$(INTDIR)\fx2cg.obj: fx2cg.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(ELF): $(LIBS) $(INTDIR)\fx2cg.obj
	ppu-lv2-gcc -o $(ELF) $(INTDIR)\fx2cg.obj -Wl,--start-group $(LIBS) -Wl,--end-group $(LLIBS)
