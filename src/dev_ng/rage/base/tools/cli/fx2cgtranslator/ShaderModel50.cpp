#include "ShaderModel50.h"

#include "grammar.h"
#include "effectdata.h"

extern void GetShaderStructure(void* pvCode, unsigned int uCodeSize, void** poShader);
extern void ReleaseShader(void* poShader); 
extern bool SetupData(void* poShader, ConstantBuffer_t *poConstantBuffer, unsigned int usage, const char* pszProgramName);
extern bool SetupData(void* poShader, Parameter_t *poParameter, unsigned int usage, const char* pszProgramName);


ShaderModel50::ShaderModel50() : mComputeProgramCount(0), mDomainProgramCount(0), mGeometryProgramCount(0), mHullProgramCount(0), mEffectDirName(0), mEffectStream(0)
{
	AddComputeProgram(NULL, "");
	AddDomainProgram(NULL, "");
	AddGeometryProgram(NULL, "");
	AddHullProgram(NULL, "");
}
ShaderModel50::~ShaderModel50() 
{
	//should we delete the dublicated entry points
}
void ShaderModel50::BeginHullProgram()
{

}
void ShaderModel50::EndHullProgram()
{

}
void ShaderModel50::BeginDomainProgram()
{

}
void ShaderModel50::EndDomainProgram()
{

}
void ShaderModel50::BeginComputeProgram()
{

}
void ShaderModel50::EndComputeProgram()
{

}
void ShaderModel50::BeginGeometryProgram()
{

}
void ShaderModel50::EndGeometryProgram()
{

}
unsigned int ShaderModel50::AddComputeProgram(const char* entry, const char* cgc_flags)
{
	(void*) cgc_flags;
	for (unsigned int i=1; i < mComputeProgramCount; ++i)
	{
		const ComputeProgram& pComputeProgram = mComputePrograms[i];
		if (strcmp(entry, pComputeProgram.mName) == 0) 
		{
			return i;
		}
	}
	if (mComputeProgramCount == kMaxProgramCount)
	{
		gram_errorf("Maximum Number Of Compute Programs is Reached, exiting...");
	}
	mComputePrograms[mComputeProgramCount].mName = rage::StringDuplicate(entry);

	return mComputeProgramCount++;
}
unsigned int ShaderModel50::AddDomainProgram(const char* entry, const char* cgc_flags)
{
	(void*) cgc_flags;
	for (unsigned int i=1; i < mDomainProgramCount; ++i)
	{
		const DomainProgram& pDomainProgram = mDomainPrograms[i];
		if (strcmp(entry, pDomainProgram.mName) == 0) 
		{
			return i;
		}
	}
	if (mDomainProgramCount == kMaxProgramCount)
	{
		gram_errorf("Maximum Number Of Domain Programs is Reached, exiting...");
	}
	mDomainPrograms[mDomainProgramCount].mName = rage::StringDuplicate(entry);

	return mDomainProgramCount++;
}
unsigned int ShaderModel50::AddGeometryProgram(const char* entry, const char* cgc_flags)
{
	(void *) entry;
	(void*) cgc_flags;
	AssertMsg(false, "not implemented yet, use Shdermodel40 version");
	return 0;
	//D3D11_SODeclaration tmpDecl = D3D11_SODeclaration(cgc_flags);
	//for (unsigned int i=1; i < mGeometryProgramCount; ++i)
	//{
	//	const GeometryProgram& pGeometryProgram = mGeometryPrograms[i];
	//	if (strcmp(entry, pGeometryProgram.mName) == 0) 
	//	{
	//		if (pGeometryProgram.mDx11StreamOut != tmpDecl) 
	//		{
	//			gram_errorf("Geometry entry point '%s' compiled with a different Stream Out  = '%s' before, now '%s'\n", entry, pGeometryProgram.mDx11StreamOut.AsString(), tmpDecl.AsString());
	//		}
	//		return i;
	//	}
	//}
	//if (mGeometryProgramCount == kMaxProgramCount)
	//{
	//	gram_errorf("Maximum Number Of Geometry Programs is Reached, exiting...");
	//}
	//mGeometryPrograms[mGeometryProgramCount].mDx11StreamOut = tmpDecl;
	//mGeometryPrograms[mGeometryProgramCount].mName = rage::StringDuplicate(entry);

	//return mGeometryProgramCount++;
}
unsigned int ShaderModel50::AddHullProgram(const char* entry, const char* cgc_flags)
{
	(void*) cgc_flags;
	for (unsigned int i=1; i < mHullProgramCount; ++i)
	{
		const HullProgram& pHullProgram = mHullPrograms[i];
		if (strcmp(entry, pHullProgram.mName) == 0) 
		{
			return i;
		}
	}
	if (mHullProgramCount == kMaxProgramCount)
	{
		gram_errorf("Maximum Number Of Hull Programs is Reached, exiting...");
	}
	mHullPrograms[mHullProgramCount].mName = rage::StringDuplicate(entry);

	return mHullProgramCount++;
}



void ShaderModel50::SaveComputeProgram(unsigned char index)
{
	Assert(index < mComputeProgramCount);
	Assert(mEffectStream != 0);
	Assert(mEffectDirName != 0);

	ComputeProgram csProgram = mComputePrograms[index];
	if (csProgram.mName == NULL) 
	{
		unsigned int zero = 0;
		mEffectStream->PutCh(5);			// currentNameLen
		mEffectStream->Write("NULL",5);		// Name
		mEffectStream->PutCh(0);			// constantCount
		if (GlobalEffectData::s_ShaderVersion >= 40) 
			mEffectStream->PutCh(0);			// constantBufferCount
		mEffectStream->WriteInt(&zero,1);	// ucodeSize

		return;
	}

	char ucodeName[256];
	strcpy(ucodeName, mEffectDirName);
	strcat(ucodeName,"/");
	strcat(ucodeName, csProgram.mName);
	const char *currentName = csProgram.mName;
	int currentNameLen = strlen(currentName) + 1;

	rage::fiStream *ucodeFile = rage::fiStream::Open(ucodeName);
	Assert(ucodeFile);
	rage::u32 ucodeSize;
	ucodeSize = ucodeFile->Size();
	char *ucode = new char[ucodeSize];
	ucodeFile->Read(ucode, ucodeSize);
	ucodeFile->Close();


	mEffectStream->PutCh(static_cast<unsigned char>(currentNameLen));
	mEffectStream->Write(currentName,currentNameLen);

	void *poShader = NULL;
	GetShaderStructure(ucode, ucodeSize, &poShader);
	if (poShader == NULL)
	{
		gram_errorf("Invalid CS shader version used must be version 5.0");
	}

	GlobalEffectData::SetupAndWriteDx10Constants(poShader, rage::USAGE_COMPUTEPROGRAM, currentName, mEffectStream);

	mEffectStream->WriteInt(&ucodeSize,1);
	mEffectStream->Write(ucode, ucodeSize);

	ReleaseShader(poShader);
	delete[] ucode;
}
void ShaderModel50::SaveDomainProgram(unsigned char index)
{
	Assert(index < mDomainProgramCount);
	Assert(mEffectStream != 0);
	Assert(mEffectDirName != 0);

	DomainProgram dsProgram = mDomainPrograms[index];
	if (dsProgram.mName == NULL) 
	{
		unsigned int zero = 0;
		mEffectStream->PutCh(5);			// currentNameLen
		mEffectStream->Write("NULL",5);		// Name
		mEffectStream->PutCh(0);			// constantCount
		if (GlobalEffectData::s_ShaderVersion >= 40) 
			mEffectStream->PutCh(0);			// constantBufferCount
		mEffectStream->WriteInt(&zero,1);	// ucodeSize

		return;
	}

	char ucodeName[256];
	strcpy(ucodeName, mEffectDirName);
	strcat(ucodeName,"/");
	strcat(ucodeName, dsProgram.mName);
	const char *currentName = dsProgram.mName;
	int currentNameLen = strlen(currentName) + 1;

	rage::fiStream *ucodeFile = rage::fiStream::Open(ucodeName);
	Assert(ucodeFile);
	rage::u32 ucodeSize;
	ucodeSize = ucodeFile->Size();
	char *ucode = new char[ucodeSize];
	ucodeFile->Read(ucode, ucodeSize);
	ucodeFile->Close();


	mEffectStream->PutCh(static_cast<unsigned char>(currentNameLen));
	mEffectStream->Write(currentName,currentNameLen);

	void *poShader;
	GetShaderStructure(ucode, ucodeSize, &poShader);
	if (poShader == NULL)
	{
		gram_errorf("Invalid DS shader version used must be version 5.0");
	}

	
	GlobalEffectData::SetupAndWriteDx10Constants(poShader, rage::USAGE_DOMAINPROGRAM, currentName, mEffectStream);
	
	mEffectStream->WriteInt(&ucodeSize,1);
	mEffectStream->Write(ucode, ucodeSize);

	ReleaseShader(poShader);
	delete[] ucode;
}
void ShaderModel50::SaveGeometryProgram(unsigned char index)
{
	(void) index;
	AssertMsg(false, "not implemented yet, use Shadermodel40 version");
	//Assert(index < mGeometryProgramCount);
	//Assert(mEffectStream != 0);
	//Assert(mEffectDirName != 0);

	//GeometryProgram gsProgram = mGeometryPrograms[index];
	//if (gsProgram.mName == NULL) 
	//{
	//	unsigned int zero = 0;
	//	mEffectStream->PutCh(5);			// currentNameLen
	//	mEffectStream->Write("NULL",5);		// Name
	//	mEffectStream->PutCh(0);			// constantCount
	//	if (GlobalEffectData::s_ShaderVersion >= 40) 
	//		mEffectStream->PutCh(0);			// constantBufferCount
	//	mEffectStream->PutCh(0);			// streamout entry count
	//	mEffectStream->WriteInt(&zero,1);	// ucodeSize

	//	return;
	//}

	//char ucodeName[256];
	//strcpy(ucodeName, mEffectDirName);
	//strcat(ucodeName,"/");
	//strcat(ucodeName, gsProgram.mName);
	//const char *currentName = gsProgram.mName;
	//int currentNameLen = strlen(currentName) + 1;

	//rage::fiStream *ucodeFile = rage::fiStream::Open(ucodeName);
	//Assert(ucodeFile);
	//rage::u32 ucodeSize;
	//ucodeSize = ucodeFile->Size();
	//char *ucode = new char[ucodeSize];
	//ucodeFile->Read(ucode, ucodeSize);
	//ucodeFile->Close();


	//mEffectStream->PutCh(static_cast<unsigned char>(currentNameLen));
	//mEffectStream->Write(currentName,currentNameLen);

	//void *poShader;
	//GetShaderStructure(ucode, ucodeSize, &poShader);
	//if (poShader == NULL)
	//{
	//	gram_errorf("Invalid GS shader version used must be version 4.0");
	//}
	//rage::u8 constantCount = static_cast<rage::u8>(GetConstantCount(poShader));
	//mEffectStream->PutCh(constantCount);

	//for (rage::u32 uIndex = 0; uIndex < constantCount; uIndex++)
	//{
	//	WriteConstant(poShader, mEffectStream, uIndex);
	//}

	//for (int uIndex=0; uIndex < GlobalEffectData::s_BufferGlobals.GetCount(); ++uIndex)
	//	SetupData(poShader, GlobalEffectData::s_BufferGlobals[uIndex], rage::USAGE_GEOMETRYPROGRAM, currentName);

	//for (int uIndex=0; uIndex < GlobalEffectData::s_BufferLocals.GetCount(); ++uIndex)
	//	SetupData(poShader, GlobalEffectData::s_BufferLocals[uIndex], rage::USAGE_GEOMETRYPROGRAM, currentName);

	//for (int uIndex=0; uIndex < GlobalEffectData::s_Globals.GetCount(); ++uIndex)
	//{
	//	SetupData(poShader, GlobalEffectData::s_Globals[uIndex], rage::USAGE_GEOMETRYPROGRAM, currentName);
	//	if (GlobalEffectData::s_Globals[uIndex]->pParentBuffer == GlobalEffectData::s_BufferLocals[0])
	//	{
	//		GlobalEffectData::s_UnassignGlobalParamsToCBuffer.Delete(GlobalEffectData::s_Globals[uIndex]->Name);
	//		GlobalEffectData::s_UnassignGlobalParamsToCBuffer[rage::ConstString(GlobalEffectData::s_Globals[uIndex]->Name)] = GlobalEffectData::s_Globals[uIndex];
	//	}
	//}

	//for (int uIndex=0; uIndex < GlobalEffectData::s_Locals.GetCount(); ++uIndex)
	//{
	//	SetupData(poShader, GlobalEffectData::s_Locals[uIndex], rage::USAGE_GEOMETRYPROGRAM, currentName);
	//	if (GlobalEffectData::s_Locals[uIndex]->pParentBuffer == GlobalEffectData::s_BufferLocals[0])
	//	{
	//		GlobalEffectData::s_UnassignParamToCBuffer.Delete(GlobalEffectData::s_Locals[uIndex]->Name);
	//		GlobalEffectData::s_UnassignParamToCBuffer[rage::ConstString(GlobalEffectData::s_Locals[uIndex]->Name)] = GlobalEffectData::s_Locals[uIndex];
	//	}
	//}

	//gsProgram.mDx11StreamOut.WriteStreamOut(mEffectStream);

	//mEffectStream->WriteInt(&ucodeSize,1);
	//mEffectStream->Write(ucode, ucodeSize);

	//ReleaseShader(poShader);
	//delete[] ucode;
}
void ShaderModel50::SaveHullProgram(unsigned char index)
{
	Assert(index < mHullProgramCount);
	Assert(mEffectStream != 0);
	Assert(mEffectDirName != 0);

	HullProgram hsProgram = mHullPrograms[index];
	if (hsProgram.mName == NULL) 
	{
		unsigned int zero = 0;
		mEffectStream->PutCh(5);			// currentNameLen
		mEffectStream->Write("NULL",5);		// Name
		mEffectStream->PutCh(0);			// constantCount
		if (GlobalEffectData::s_ShaderVersion >= 40) 
			mEffectStream->PutCh(0);			// constantBufferCount
		mEffectStream->WriteInt(&zero,1);	// ucodeSize
		return;
	}
	char ucodeName[256];
	strcpy(ucodeName, mEffectDirName);
	strcat(ucodeName,"/");
	strcat(ucodeName, hsProgram.mName);
	const char *currentName = hsProgram.mName;
	int currentNameLen = strlen(currentName) + 1;
	
	rage::fiStream *ucodeFile = rage::fiStream::Open(ucodeName);
	Assert(ucodeFile);
	rage::u32 ucodeSize;
	ucodeSize = ucodeFile->Size();
	char *ucode = new char[ucodeSize];
	ucodeFile->Read(ucode, ucodeSize);
	ucodeFile->Close();


	mEffectStream->PutCh(static_cast<unsigned char>(currentNameLen));
	mEffectStream->Write(currentName,currentNameLen);

	void *poShader;
	GetShaderStructure(ucode, ucodeSize, &poShader);
	if (poShader == NULL)
	{
		gram_errorf("Invalid HS shader version used must be version 5.0");
	}
	
	GlobalEffectData::SetupAndWriteDx10Constants(poShader, rage::USAGE_HULLPROGRAM, currentName, mEffectStream);

	//hsProgram.mDx11StreamOut.WriteStreamOut(mEffectStream);

	mEffectStream->WriteInt(&ucodeSize,1);
	mEffectStream->Write(ucode, ucodeSize);

	ReleaseShader(poShader);
	delete[] ucode;
}