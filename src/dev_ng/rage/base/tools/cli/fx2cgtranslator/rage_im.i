




























































































	










uniform float4x4 gWorld : World;
uniform float4x4 gWorldInverse : WorldInverse;
uniform float4x4 gWorldView : WorldView;
uniform float4x4 gWorldViewInverse : WorldViewInverse;
uniform float4x4 gWorldViewProj : WorldViewProjection;
uniform float4x4 gWorldInverseTranspose : WorldInverseTranspose;
uniform float4x4 gViewInverse : ViewInverse;
uniform float4x3 gTextureMtx0 : TextureMatrix;
uniform float	gbOffsetEnable : OffsetEnable = 0.0f;








uniform float4 gLightPosDir[3] : Position
<
	string Object = "PointDirLight";
	string Space = "World";
> = { {1403.0f, 1441.0f, 1690.0f, 0.0f}, {0.0, 0.0, 0.0, 0.0}, {0.0, 0.0, 0.0, 0.0} };

uniform float4 gLightColor[3] : Diffuse
<
    string UIName = "Diffuse Light Color";
    string Object = "LightPos";
> = { {1.0f, 1.0f, 1.0f, 1.0f}, {0.0, 0.0, 0.0, 0.0}, {0.0, 0.0, 0.0, 0.0} };

uniform float gLightType[3] : LightType
<
	string UIName = "The type of each light source";
> = { (float) 1, (float) 1, (float) 1 };

uniform int gLightCount : LightCount
<
	string UIName = "Active Light Count";
> = 3;






uniform float4 gLightAmbient : Ambient
<
    string UIWidget = "Ambient Light Color";
    string Space = "material";
> = {0.0f, 0.0f, 0.0f, 1.0f};



struct rageVertexInput {
	
    float3 pos			: POSITION;
	float4 diffuse		: COLOR0;
    float2 texCoord0	: TEXCOORD0;
    float2 texCoord1	: TEXCOORD1;
    float3 normal		: NORMAL;
};

struct rageVertexOffset {
	float3 pos			: POSITION1;
	float4 diffuse		: COLOR1;
};

struct rageVertexInputBump {
	
    float3 pos			: POSITION;
	float4 diffuse		: COLOR0;
    float2 texCoord0	: TEXCOORD0;
    float2 texCoord1	: TEXCOORD1;
    float3 normal		: NORMAL;
	float4 tangent		: TANGENT0;
};

struct rageVertexOffsetBump {
    float3 pos			: POSITION1;
	float4 diffuse		: COLOR1;
    float3 normal		: NORMAL1;
	float4 tangent		: TANGENT1;
};

struct rageVertexOutputPassThrough {
	
    float4 pos			: POSITION;
    float4 diffuse		: COLOR0;
    float2 texCoord0	: TEXCOORD0;
	float2 texCoord1	: TEXCOORD1;
	float3 normal		: COLOR1;
};

struct rageLightOutput {
	float4 lightPosDir;				
};




rageVertexOutputPassThrough VS_ragePassThrough(rageVertexInput IN)
{
	rageVertexOutputPassThrough OUT;
	OUT.pos = mul(float4(IN.pos, 1.0), gWorldViewProj);
    OUT.diffuse = IN.diffuse;
    OUT.texCoord0 = IN.texCoord0;
    OUT.texCoord1 = IN.texCoord1;
    OUT.normal = mul(float4(IN.normal, 1.0), gWorldInverseTranspose).xyz;
    return OUT;
}



rageVertexOutputPassThrough VS_ragePassThroughNoXform(rageVertexInput IN)
{
	rageVertexOutputPassThrough OUT;
	OUT.pos = float4(IN.pos, 1.0);
    OUT.diffuse = IN.diffuse;
    OUT.texCoord0 = IN.texCoord0;
    OUT.texCoord1 = IN.texCoord1;
    OUT.normal = float3(0.0, 0.0, -1.0);	
    return OUT;
}



rageLightOutput rageComputeLightData(float3 inWorldPos, int idx)
{
	rageLightOutput OUT;


	if ( (int) gLightType[idx] == 0 ) {
		OUT.lightPosDir.xyz = (gLightPosDir[idx].xyz - inWorldPos.xyz);
		OUT.lightPosDir.w = length(OUT.lightPosDir.xyz);
		if ( OUT.lightPosDir.w != 0.0 ) {
			OUT.lightPosDir.xyz /= OUT.lightPosDir.w;
		}
	}
	else 

	{
		
		
		OUT.lightPosDir.xyz = -gLightPosDir[idx].xyz;
		OUT.lightPosDir.w = 0.0;
	}

	return OUT;
}


float4 rageGetLitFactors( float NdotL, float NdotH, float power )
{
	float4 OUT;

	OUT.y = max(NdotL, 0.0);
	if ( OUT.y <= 0 )
		OUT.z = 0.0;
	else
		OUT.z = pow((max(NdotH, 0.0)),power);
	OUT.xw = 1.0;






	return OUT;
}


float3 rageComputeBinormal( float3 normal, float4 tangent )
{
	float3 OUT;
	OUT = cross((float3)tangent.xyz, normal);
	OUT *= tangent.w;	

	return OUT;
}






	







uniform const float4x3 gBoneMtx[55] : WorldMatrixArray;




struct rageSkinVertexInput {
	
    float3 pos            : POSITION0;
	float4 weight		  : BLENDWEIGHT;
	float4 blendindices	  : BLENDINDICES;
    float2 texCoord0      : TEXCOORD0;
    float2 texCoord1      : TEXCOORD1;
    float3 normal		  : NORMAL;
	float4 diffuse		  : COLOR0;
};

struct rageSkinVertexOffset {
    float3 pos            : POSITION1;
};

struct rageSkinVertexInputBump {
	
    float3 pos            : POSITION0;
	float4 weight		  : BLENDWEIGHT;
	float4 blendindices	  : BLENDINDICES;
    float2 texCoord0      : TEXCOORD0;
    float2 texCoord1      : TEXCOORD1;
    float3 normal		  : NORMAL0;
	float4 tangent		  : TANGENT0;
	float4 diffuse		  : COLOR0;
};

struct rageSkinVertexOffsetBump {
	
    float3 pos            : POSITION1;
    float3 normal		  : NORMAL1;
	float4 tangent		  : TANGENT1;
};







float4x3 ComputeSkinMtx(int4 indicies, float4 weights)
{
	
	float4x3 skinMtx = gBoneMtx[indicies.z] * weights.x;
	skinMtx += gBoneMtx[indicies.y] * weights.y;
	skinMtx += gBoneMtx[indicies.x] * weights.z;
	skinMtx += gBoneMtx[indicies.w] * weights.w;
	return skinMtx;
}

















	





	



	
	
	
		
	


	





		
		
		
		
	

















	





	










	
	











	
	
	
	



	







	






	float specularFactor : Specular
	<
		string UIName = "Specular Falloff";
		float UIMin = 0.0;
		float UIMax = 10000.0;
		float UIStep = 0.1;
	> = 100.0;

	float specularColorFactor : SpecularColor
	<
		string UIName = "Specular Intensity";
		float UIMin = 0.0;
		float UIMax = 10000.0;
		float UIStep = 0.1;
	> = 1.0;

	

































































float Depth_DivideWTerm=1.0;





















































	






	




struct vertexOutput {
    float4 pos						: POSITION;
    float2 texCoord     : TEXCOORD0;
	float3 worldNormal				: TEXCOORD1;



	float3 worldEyePos				: TEXCOORD3;




	float4 color0					: COLOR0;
};


struct vertexOutputUnlit {
    float4 pos						: POSITION;
    float2 texCoord		: TEXCOORD0;
	float4 color0					: COLOR0;



};



struct vertexOutputDepth {
    float4 pos		        : POSITION;
    float2 texCoord         : TEXCOORD0;
	float3 depthColor		: COLOR0;
	float4 color0			: COLOR1;
};


void rtsProcessVertLighting(
					float3 localPos, 
					float3 localNormal, 
	


					float4 vertColor,
					float4x3 worldMtx, 
					float4x3 worldInvTranspose,
					out float3 worldEyePos,
					out float3 worldNormal,
	



					out float4 color0 )
{
	float3 pos = mul(float4(localPos,1), worldMtx);

	
	worldEyePos = gViewInverse[3].xyz - pos;

	
	worldNormal = normalize(mul(float3(localNormal), (float3x3)worldInvTranspose));;











    color0 = vertColor;

}

float4 rtsComputePixelColor( float4 baseColor,
							float3 worldNormal,
							float2 diffuseTexCoord,
							sampler2D diffuseSampler,
	



	



	


	





							float3 worldEyePos
	


						)
{


	float4 diffuseColor = tex2D(diffuseSampler, diffuseTexCoord);
    






	float3 N = worldNormal;



    float3 E = normalize(worldEyePos);



	


		float4 specularColor = specularColorFactor;
	
	
	float specStrength = specularColor.w * specularFactor;















    










    
    
	
	float4 light;
	float3 color = float3(0,0,0);

	
	float3 specular = float3(0,0,0);








	


	float3 viewDir;
	viewDir = gViewInverse[3].xyz - worldEyePos;
	for (int i = 0; i < 3; ++i) {
		rageLightOutput lightData = rageComputeLightData(viewDir, i);
	    float3 L = lightData.lightPosDir.xyz;

		



		float falloff = 1.0 - min((lightData.lightPosDir.w / gLightPosDir[i].w), 1.0);
		float lightIntensity = gLightColor[i].w;
		if ( lightIntensity < 0.f ) {
			falloff *= -lightIntensity;		
		}
		else {
			falloff = min(falloff * lightIntensity, 1.0);
		}



	    float3 H = normalize(L + E);
	    light = rageGetLitFactors(dot(N,L), dot(N,H), specStrength);
		specular += gLightColor[i].rgb * light.z * falloff;





		
		color += gLightColor[i].rgb * light.y * falloff;
	}









    color += gLightAmbient;


    specular *= specularColor.xyz;

	
	
	
	
	
    
    float4 outColor =diffuseColor;





















	
	
	outColor *= float4(color,1.0);


	
	outColor *= baseColor;


    


    
    outColor += float4(specular,0.);


	outColor.a *= gLightAmbient.w;
	return outColor;
}

float4 rtsComputeNormalColor( float4 baseColor,
							float3 worldNormal,
							float2 diffuseTexCoord,
							sampler2D diffuseSampler
	





						)
{
    






	float3 N = worldNormal;

	
	N = (N * 0.5) + 0.5;
	
	float alpha = (tex2D(diffuseSampler, diffuseTexCoord).a) * baseColor.a;

	return float4(N, alpha);
}
	







vertexOutput VS_Transform(rageVertexInputBump IN, rageVertexOffsetBump INOFFSET, uniform float bEnableOffsets)
{
    vertexOutput OUT;
    
    float3 inPos = IN.pos;
    float3 inNrm = IN.normal;
    float4 inCpv = IN.diffuse;





    if( bEnableOffsets != 0.0f )
	{
		inPos += INOFFSET.pos;
		inNrm += INOFFSET.normal;
		inCpv += INOFFSET.diffuse;



	}

	rtsProcessVertLighting( inPos, 
							inNrm, 



							inCpv, 
							(float4x3) gWorld, 
							(float4x3) gWorldInverseTranspose,
							OUT.worldEyePos,
							OUT.worldNormal,




							OUT.color0 
						);

    
    OUT.pos =  mul(float4(inPos,1), gWorldViewProj);
    
    
    OUT.texCoord.xy = IN.texCoord0.xy;
















	return OUT;
}

vertexOutputUnlit VS_TransformUnlit(rageVertexInput IN, rageVertexOffsetBump INOFFSET, uniform float bEnableOffsets)
{
    vertexOutputUnlit OUT;
   
    float3 inPos = IN.pos;
    float4 inCpv = IN.diffuse;

    if( bEnableOffsets != 0.0f )
	{
		inPos += INOFFSET.pos;
		inCpv += INOFFSET.diffuse;
	}
	
    
    OUT.pos =  mul(float4(inPos,1), gWorldViewProj);
    
    OUT.texCoord.xy = IN.texCoord0.xy;
















    OUT.color0 = inCpv;
    return OUT;
}

vertexOutputDepth VS_TransformDepth(rageVertexInput IN, rageVertexOffset INOFFSET, uniform float bEnableOffsets)
{
   vertexOutputDepth OUT;
   
	float3 inPos = IN.pos;
    float4 inCpv = IN.diffuse;

    if( bEnableOffsets != 0.0f )
	{
		inPos += INOFFSET.pos;
		inCpv += INOFFSET.diffuse;
	}
	
   
   OUT.pos =  mul(float4(inPos,1), gWorldViewProj);
   OUT.texCoord = IN.texCoord0;
   float  depth=(OUT.pos.z/Depth_DivideWTerm);
   OUT.depthColor = float3(depth,0.0,0.0);
   OUT.color0 = inCpv;
        
    return OUT;
}

vertexOutputDepth smd_VS_TransformDepth(rageVertexInput IN, rageVertexOffset INOFFSET, uniform float bEnableOffsets)
{
   vertexOutputDepth OUT;

	float3 inPos = IN.pos;
    float4 inCpv = IN.diffuse;

    if( bEnableOffsets != 0.0f )
	{
		inPos += INOFFSET.pos;
		inCpv += INOFFSET.diffuse;
	}

   
   
   OUT.texCoord = IN.texCoord0;
   OUT.color0 = IN.diffuse;
	
	OUT.pos =  mul(float4(inPos,1), gWorldViewProj);
   float4 A = OUT.pos;
   
   float depth = A.z/A.w;
   OUT.depthColor.x =  A.z;
   OUT.depthColor.y =  A.w;
   OUT.depthColor.z =  depth;
   OUT.color0	= inCpv;
   
   return OUT;
}







vertexOutput VS_TransformSkin(rageSkinVertexInputBump IN, rageSkinVertexOffsetBump INOFFSET, uniform float bEnableOffsets) 
{
    vertexOutput OUT;
    
    float3 inPos = IN.pos;
    float3 inNrm = IN.normal;





    if( bEnableOffsets != 0.0f )
	{
		inPos += INOFFSET.pos;
		inNrm += INOFFSET.normal;



	}
	
	int4 IndexVector = D3DCOLORtoUBYTE4(IN.blendindices);

	float4x3 boneMtx = ComputeSkinMtx( IndexVector, IN.weight );




	rtsProcessVertLighting( inPos,
							inNrm,






							 float4(1,1,1,1),

							boneMtx, 
							boneMtx,
							OUT.worldEyePos,
							OUT.worldNormal,




							OUT.color0
						);

    
	

	float3 pos = mul(float4(inPos,1), boneMtx);




    OUT.pos =  mul(float4(pos,1), gWorldViewProj);
    
    OUT.texCoord.xy = IN.texCoord0.xy;
















	return OUT;
}

vertexOutputUnlit VS_TransformSkinUnlit(rageSkinVertexInput IN, rageSkinVertexOffset INOFFSET, uniform float bEnableOffsets) 
{
    vertexOutputUnlit OUT;
    
    float3 inPos = IN.pos;

    if( bEnableOffsets != 0.0f )
	{
		inPos += INOFFSET.pos;
	}
	
	int4 IndexVector = D3DCOLORtoUBYTE4(IN.blendindices);
	float4x3 boneMtx = ComputeSkinMtx( IndexVector, IN.weight );
	
	float3 pos = mul(float4(inPos,1), boneMtx);

    
    OUT.pos =  mul(float4(pos,1), gWorldViewProj);
    
    OUT.texCoord.xy = IN.texCoord0.xy;
















    OUT.color0 =  float4(1,1,1,1);
    return OUT;
}

vertexOutputDepth VS_TransformSkinDepth(rageSkinVertexInput IN, rageSkinVertexOffset INOFFSET, uniform float bEnableOffsets) 
{
    vertexOutputDepth OUT;
    
    float3 inPos = IN.pos;

    if( bEnableOffsets != 0.0f )
	{
		inPos += INOFFSET.pos;
	}
	
	int4 IndexVector = D3DCOLORtoUBYTE4(IN.blendindices);
	float4x3 boneMtx = ComputeSkinMtx( IndexVector, IN.weight );
	
	float3 pos = mul(float4(inPos,1), boneMtx);

    
    OUT.pos =  mul(float4(pos,1), gWorldViewProj);
    OUT.texCoord = IN.texCoord0;
    
    float  depth=(OUT.pos.z/Depth_DivideWTerm);
    
    OUT.depthColor = float3(depth,0.0,0.0);
    OUT.color0 =  float4(1,1,1,1);
        
    return OUT;    
}

vertexOutputDepth smd_VS_TransformSkinDepth(rageSkinVertexInput IN, rageSkinVertexOffset INOFFSET, uniform float bEnableOffsets) 
{
    vertexOutputDepth OUT;
    float3 inPos = IN.pos;

    if( bEnableOffsets != 0.0f )
	{
		inPos += INOFFSET.pos;
	}
    
	int4 IndexVector = D3DCOLORtoUBYTE4(IN.blendindices);
	float4x3 boneMtx = ComputeSkinMtx( IndexVector, IN.weight );
	
	float3 pos = mul(float4(inPos,1), boneMtx);

    
    OUT.texCoord = IN.texCoord0;
    OUT.color0 =  float4(1,1,1,1);
    
    OUT.pos =  mul(float4(pos,1), gWorldViewProj);
	float4 A = OUT.pos;
    float depth = A.z/A.w;
    
    OUT.depthColor.x =  A.z;
    OUT.depthColor.y =  A.w;
    OUT.depthColor.z =  depth;
    
    OUT.color0 = IN.diffuse;
        
    return OUT;    
}








sampler2D TextureSampler : DiffuseTex <
	string UIName="Diffuse Texture";



> = sampler_state {
    AddressU  = WRAP;        
    AddressV  = WRAP;
    AddressW  = WRAP;
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
    MAGFILTER = LINEAR;
    MIPMAPLODBIAS = 0.0;
};




























































float4 PS_Textured( vertexOutput IN): COLOR
{
	return rtsComputePixelColor( IN.color0, 
								IN.worldNormal,
								IN.texCoord.xy,
								TextureSampler, 

















								IN.worldEyePos



							);
}

float4 PS_TexturedUnlit( vertexOutputUnlit IN ) : COLOR
{
	float4 color = IN.color0;
    
    
    float4 outColor =  tex2D(TextureSampler, IN.texCoord.xy);

















	outColor.a *= gLightAmbient.w;
	return outColor * color;
}

float4 PS_PaintNormals( vertexOutput IN ) : COLOR
{
	return rtsComputeNormalColor( IN.color0, 
								IN.worldNormal,
								IN.texCoord.xy,
								TextureSampler 






							);
}

float4 PS_DrawDepth( vertexOutputDepth IN ) : COLOR
{
	

	if((tex2D(TextureSampler, IN.texCoord).w*IN.color0.w)<1)
		clip(-1);	

	
	return float4(IN.depthColor.x,0.0,0.0,1.0);
}

float4 smd_PS_DrawDepth( vertexOutputDepth IN ) : COLOR
{

	if((tex2D(TextureSampler, IN.texCoord).w*IN.color0.w)<0.6f)
		clip(-1);	

	return float4((IN.depthColor.x/IN.depthColor.y),0.f, 0.f, 0.0);
	
}




	
	
	
	technique draw
	{
		pass p0 
		{        
			AlphaBlendEnable = true;
			AlphaTestEnable = true;
			VertexShader = compile vs_3_0 VS_Transform();
			PixelShader  = compile ps_3_0 PS_Textured();
		}
	}
	technique drawskinned
	{
		pass p0 
		{        
			AlphaRef = 100;
			AlphaBlendEnable = true;
			AlphaTestEnable = true;
			VertexShader = compile vs_3_0 VS_TransformSkin();
			PixelShader  = compile ps_3_0 PS_Textured();
		}
	}

	
	
	
	technique depth_draw
	{
		pass p0 
		{        
			AlphaBlendEnable = false;
			AlphaTestEnable = false;
			VertexShader = compile vs_3_0 VS_TransformDepth();
			PixelShader  = compile ps_3_0 PS_DrawDepth();
			
			
		}
	}
	technique depth_drawskinned
	{
		pass p0 
		{        
			AlphaBlendEnable = false;
			AlphaTestEnable = false;
			VertexShader = compile vs_3_0 VS_TransformSkinDepth();
			PixelShader  = compile ps_3_0 PS_DrawDepth();
			
			

		}
	}
	technique smd_draw
	{
		pass p0 
		{        
			
			
			
			AlphaBlendEnable = false;
			AlphaTestEnable = false;
			VertexShader = compile vs_3_0 smd_VS_TransformDepth();
			PixelShader  = compile ps_3_0 smd_PS_DrawDepth();
		}
	}
	technique smd_drawskinned
	{
		pass p0 
		{        
			
			
			
			AlphaBlendEnable = false;
			AlphaTestEnable = false;
			VertexShader = compile vs_3_0 smd_VS_TransformSkinDepth();
			PixelShader  = compile ps_3_0 smd_PS_DrawDepth();
		}
	}
	
	
	
	
	technique unlit_draw
	{
		pass p0 
		{        
			VertexShader = compile vs_3_0 VS_TransformUnlit();
			PixelShader  = compile ps_3_0 PS_TexturedUnlit();
		}
	}
	technique unlit_drawskinned
	{
		pass p0 
		{        
			VertexShader = compile vs_3_0 VS_TransformSkinUnlit();
			PixelShader  = compile ps_3_0 PS_TexturedUnlit();
		}
	}
	
	
	
	
	technique normals_draw
	{
		pass p0 
		{        
			VertexShader = compile vs_3_0 VS_Transform();
			PixelShader  = compile ps_3_0 PS_PaintNormals();
		}
	}
	technique normals_drawskinned
	{
		pass p0 
		{        
			AlphaRef = 100;
			VertexShader = compile vs_3_0 VS_TransformSkin();
			PixelShader  = compile ps_3_0 PS_PaintNormals();
		}
	}






struct rageVertexBlit {
	
	float4 pos                  : POSITION;
	float4 diffuse              : COLOR0;
	float2 texCoord0			: TEXCOORD0;
};

rageVertexBlit VS_Blit(rageVertexBlit IN) {
	return IN;
}


float4 PS_Blit(rageVertexBlit IN) : COLOR {
	float4 texel = tex2D(TextureSampler, IN.texCoord0);
	return texel * IN.diffuse;	
}


technique drawblit
{
        pass p0
        {
        VertexShader = compile vs_3_0 VS_Blit();
        PixelShader  = compile ps_3_0 PS_Blit();
        }
}
