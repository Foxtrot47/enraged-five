#ifndef _SHADERMODEL_COMMON_H_
#define _SHADERMODEL_COMMON_H_

#include "file/stream.h"
#include "Parameters.h"

enum 
{
	kMaxProgramCount = 256,
};


//Hull Shader Param
struct HSParam_t : public Param_t
{
	HSParam_t(int modifier, const char* PathcType, const char* datatype, int datacount, const char* name) :	Param_t(NULL, datatype, name, NULL),
		mModifier(modifier),
		mPatchType(PathcType), 
		mDataType(datatype), 
		mDataCount(datacount),
		mName(name) 
	{ ; }

	const char* GetPatchType()	const { return mPatchType; } 
	virtual void WriteOut(rage::fiStream* streamout)
	{
		if (mModifier == 0)
		{
			fprintf(streamout," const %s<%s, %d> %s", GetPatchType(), mDataType, mDataCount, mName);
		}
		else
		{
			fprintf(streamout," %s<%s, %d> %s", GetPatchType(), mDataType, mDataCount, mName);
		}
		if (Semantic) fprintf(streamout," : %s",Semantic);
	}

	int				mModifier;
	const char*		mPatchType;
	const char*		mDataType;
	int				mDataCount;
	const char*		mName;
};


struct GSInParam_t : public Param_t
{
	GSInParam_t(const char* primtype, const char* datatype, const char* name, unsigned int count) : 
			Param_t(NULL, datatype, name, NULL), 
			mPrimitiveType(static_cast<const char*>(primtype)), 
			mDataType(datatype), 
			mName(name), 
			mNumElements(count) 
	{ ; }

	const char* GetPrimitiveType()	const { return mPrimitiveType; }//PrimitiveTypeNames[static_cast<unsigned int>(mPrimitiveType)]; }
	virtual void WriteOut(rage::fiStream* streamout)
	{
		fprintf(streamout," %s %s %s[%d]", GetPrimitiveType(), mDataType, mName, mNumElements);
		if (Semantic) fprintf(streamout," : %s",Semantic);
	}

	const char*		mPrimitiveType;
	const char*		mDataType;
	const char*		mName;
	unsigned int	mNumElements;
};
struct GSOutParam_t : public Param_t
{
	GSOutParam_t(const char* streamtype, const char* datatype, const char* name) :
			Param_t(NULL, datatype, name, NULL),
			mStreamType(static_cast<const char*>(streamtype)), 
			mDataType(datatype), 
			mName(name) 
	{ ; }

	const char* GetStreamType()	const { return mStreamType; } //StreamTypeNames[static_cast<unsigned int>(mStreamType)]; }
	virtual void WriteOut(rage::fiStream* streamout)
	{
		fprintf(streamout," inout %s<%s> %s", GetStreamType(), mDataType, mName);
		if (Semantic) fprintf(streamout," : %s",Semantic);
	}

	const char*		mStreamType;
	const char*		mDataType;
	const char*		mName;
};

//D3D10_STREAM_OUTPUT_DECLARATION_ENTRY pDecl[] =
//{
//	// semantic name, semantic index, start component, component count, output slot
//	{ "SV_POSITION", 0, 0, 4, 0 },   // output all components of position
//	{ "TEXCOORD0", 0, 0, 3, 0 },     // output the first 3 of the normal
//	{ "TEXCOORD1", 0, 0, 2, 0 },     // output the first 2 texture coordinates
//};
//
//� [ Buffer: ] Semantic[ SemanticIndex ] [ .Mask ]; [ ... ; ] ... [ ... ;]�
//GeometryShader pGSwSO = ConstructGSWithSO(pGSComp, "0:Position.xy; 1:Position.zw; 2:Color.xy", "3:Texcoord.xyzw; 3:$SKIP.x;", NULL, NULL, 1);

//typedef struct D3D11_SO_DECLARATION_ENTRY {
//	UINT Stream;
//	LPCSTR SemanticName;
//	UINT SemanticIndex;
//	BYTE StartComponent;
//	BYTE ComponentCount;
//	BYTE OutputSlot;
//} D3D11_SO_DECLARATION_ENTRY;
//
//typedef struct D3D10_SO_DECLARATION_ENTRY {
//	LPCSTR SemanticName;
//	UINT SemanticIndex;
//	BYTE StartComponent;
//	BYTE ComponentCount;
//	BYTE OutputSlot;
//} D3D10_SO_DECLARATION_ENTRY;

class D3D10_SODeclaration
{
public:
	enum {
		kMaxStreamCount = 64,
		kMaxSemanticNameLength = 16,
		kInvalidValue = 0xFF,
	};
	struct Stream
	{
		char			mSemanticName[kMaxSemanticNameLength];	//	Type of output element. Possible values: "POSITION", "NORMAL", or "TEXCOORD0". 
		unsigned char	mSemanticIndex;							//	Output element's zero-based index. Should be used if, for example, you have more than one texture coordinate stored in each vertex. 
		unsigned char	mStartComponent;						//	Which component of the entry to begin writing out to. Valid values are 0 ~ 3. For example, if you only wish to output to the y and z components of a position, then StartComponent should be 1 and ComponentCount should be 2. 
		unsigned char	mComponentCount;						//	The number of components of the entry to write out to. Valid values are 1 ~ 4. For example, if you only wish to output to the y and z components of a position, then StartComponent should be 1 and ComponentCount should be 2. 
		unsigned char	mOutputSlot;							//	The output slot that contains the vertex buffer that contains this output entry

		Stream();
		~Stream();
	};
	D3D10_SODeclaration();
	D3D10_SODeclaration(const D3D10_SODeclaration& other);
	D3D10_SODeclaration(const char* flags);
	~D3D10_SODeclaration();

	int AddStream();
	int InsertStream(const Stream& str);
	void ResetStreams();

	void WriteStreamOut(rage::fiStream* effectstream);
	const char* AsString()								const	{ return "Stream Out Decl"; }
	bool operator != (const D3D10_SODeclaration& other)	const	{ return !(*this == other); }
	bool operator == (const D3D10_SODeclaration& other)	const;

	D3D10_SODeclaration& operator = (const D3D10_SODeclaration& other);
protected:

	Stream			mStreams[kMaxStreamCount];
	unsigned char	mStreamCount;

};
typedef D3D10_SODeclaration D3D11_SODeclaration;
struct GeometryProgram
{
	const char			*mName;
	D3D10_SODeclaration	mDx10StreamOut;	
	//D3D11_SODeclaration	mDx11StreamOut;	
};
struct ComputeProgram
{
	const char			*mName;
};
struct DomainProgram
{
	const char			*mName;
};
struct HullProgram
{
	const char			*mName;
};


extern void SaveProgram(rage::fiStream* effect_stream);
extern void PCCompileProgram(const char* entry, const char* target, bool backwardsCompatible);

extern int RegisterGeometryProgram(const char *target, const char *entry, const char *cgc_flags);
//extern int CompileGeometryProgram(const char *target, const char *entry, const char *cgc_flags);
extern int SaveGeometryPrograms(const char* dirname, rage::fiStream* effect_stream);

//extern int CompileDomainProgram(const char *target, const char *entry, const char *cgc_flags);
extern int RegisterDomainProgram(const char *entry, const char *cgc_flags);
extern int SaveDomainPrograms(const char* dirname, rage::fiStream* effect_stream);

extern int RegisterHullProgram(const char *entry, const char *cgc_flags);
//extern int CompileHullProgram(const char *target, const char *entry, const char *cgc_flags);
extern int SaveHullPrograms(const char* dirname, rage::fiStream* effect_stream);

extern int RegisterComputeProgram(const char *entry, const char *cgc_flags);
//extern int CompileComputeProgram(const char *target, const char *entry, const char *cgc_flags);
extern int SaveComputePrograms(const char* dirname, rage::fiStream* effect_stream);

extern void* GetShaderModel40();
extern void* GetShaderModel50();

#endif