// 
// fx2cgtranslator/cgpacker.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef CGPACKER_H
#define CGPACKER_H

// #include <stdint.h>		// sigh, not in .net 2005
typedef unsigned uint32_t;
typedef int int32_t;
typedef unsigned short uint16_t;
typedef unsigned char uint8_t;

struct CgProgram;

// This is first in the file.  Big-endian by default.
struct CgBinaryHeader 
{
	uint32_t profile;
	uint32_t version;
	uint32_t totalSize;
	uint32_t symbolCount;
	uint32_t symbolOffset;
	uint32_t microcodeHeaderOffset;
	uint32_t microcodeSize;
	uint32_t microcodeOffset;

	static CgBinaryHeader* Load(const char* filename);
	bool Save(const char *filename) const;
	int GetSize() const;
};

struct CgSymbolData
{
	uint32_t type;				// The type of the symbol (float3, etc.)
	uint32_t binding;			// What resource the symbol is bound to
	uint32_t variability;		// Varying / uniform / constant
	int32_t resourceIndex;		// Constant index / texunit index
	uint32_t nameOffset;		// Offset to name string
	uint32_t valueOffset;		// Offset to default value
	uint32_t patchOffset;		// Offset to FP patch data
	uint32_t semanticOffset;	// Offset to semantic name
	uint32_t direction;			// In / out / inout
	int32_t parameter;			// Index in function parameter list
	int32_t usedFlag;			// Boolean value indicating whether the symbol is used
	int32_t unknown;

	bool IsNecessary(const CgBinaryHeader &hdr);
	const char *GetName(const CgBinaryHeader &hdr);
};

struct CgPatchHeader
{
	uint32_t	count;
	uint32_t	address[1];
};

struct CgVertexMicrocodeHeader
{
	uint32_t	instructionCount;
	uint32_t	instructionSlot;          // 0x1ea0
	uint32_t	registerCount;
	uint32_t	attributeFlags;           // inputs that are used
	uint32_t	resultMask;               // written to register 0x1ff4
	uint32_t	unknown1;
};

struct CgFragmentMicrocodeHeader
{
	uint32_t	instructionCount;
	uint32_t	attributeFlags;
	uint32_t	unknown1;
	uint16_t	texcoordUsedMask;
	uint16_t	texcoord2DMask;
	uint16_t	texcoordCentroidMask;
	uint8_t		registerCount;
	uint8_t		halfPrecisionFlag;
	uint8_t		depthOutputFlag;
	uint8_t		killFlag;
	uint8_t		unknown5;
	uint8_t		unknown6;
	uint32_t	unknown7;
	uint32_t	unknown8;
};

struct CgFloat4 {
	uint32_t x, y, z, w;	// don't use floats so that we don't get NaN during non-byte-swapped copy
};

struct CgSymbol
{
	uint32_t type;				// The type of the symbol (float3, etc.)
	uint32_t binding;			// What resource the symbol is bound to
	uint32_t variability;		// Varying / uniform / constant
	int32_t resourceIndex;		// Constant index / texunit index
	char *name;
	CgFloat4 *value;			// how to get the size?
	uint32_t *patches;			// null, or [0] is count of patch offsets, followed by patches
	char *semantic;
	uint32_t direction;			// In / out / inout
	int32_t parameter;			// Index in function parameter list
	int32_t usedFlag;			// Boolean value indicating whether the symbol is used
	int32_t unknown;

	void Init(const CgBinaryHeader &header,const CgSymbolData &data);
	CgSymbol();
	~CgSymbol();
};

struct CgProgram
{
	uint32_t profile;
	uint32_t version;
	uint32_t symbolCount;
	CgSymbol *symbols;
	void *microcodeHeader;
	uint32_t microcodeHeaderSize;
	void *microcode;
	uint32_t microcodeSize;

	CgProgram(const CgBinaryHeader &header);
	~CgProgram();

	CgBinaryHeader* Convert();
};

#endif
