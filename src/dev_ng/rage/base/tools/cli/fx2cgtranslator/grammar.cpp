#include "grammar.h"
#include "string/string.h"
#include "system/bootmgr.h"
#include <stdlib.h>

char			s_CurrentFile[256];
int				line = 1;

bool			debugInfo = false;
bool			ps4razor = false;
bool			bVerbose = false;
bool			quiet = false;
char			cg_name[256];
rage::fiStream	*cg_stream;
rage::fiStream	*gram_stream;
rage::fiStream	*effect_stream;
#if RSG_PC
bool			bNVStereo = false;
#endif

void gram_exit(unsigned int code)
{
	//system("pause");
	exit(code);
}
void gram_error(const char *s)
{
	// If debugging fx2cg, hit a hardcoded breakpoint here to make things
	// easier, otherwise the console window closes before you can read what went
	// wrong.
	if (rage::sysBootManager::IsDebuggerPresent())
		__debugbreak();

	printf("%s(%d) : error X1111: %s\n",s_CurrentFile,line,s);
	if (cg_stream) {
		cg_stream->Close();
		cg_stream = NULL;
	}
	if (gram_stream) {
		gram_stream->Close();
		gram_stream = NULL;
	}
	if (effect_stream) {
		effect_stream->Close();
		_unlink(effect_stream->GetName());
		effect_stream = NULL;
	}
	gram_exit(1);
}

void WriteString(rage::fiStream &S,const char *s) 
{
	Assert(s);
	int len = strlen(s) + 1;
	S.PutCh(static_cast<unsigned char>(len));
	S.Write(s,len);
}

void gram_errorf(const char *fmt,...)
{
	char buffer[512];
	va_list args;
	va_start(args,fmt);
	rage::vformatf(buffer,sizeof(buffer),fmt,args);
	va_end(args);
	gram_error(buffer);
}

void gram_warningf(const char *fmt,...)
{
	va_list args;
	va_start(args,fmt);
	printf("%s(%d) : fx2cg: ",s_CurrentFile,line);
	vprintf(fmt,args);
	putchar('\n');
	va_end(args);
}
