// 
// /xmltorbf.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "parser/manager.h"
#include "parser/tree.h"
#include "system/main.h"
#include "system/param.h"

using namespace rage;

int Main()
{

	if (sysParam::GetArgCount() < 3) {
		printf("Usage: %s <infile> <outfile>\n", sysParam::GetProgramName());
		return 1;
	}

	const char* inFileName = sysParam::GetArg(1);
	const char* outFileName = sysParam::GetArg(2);

	INIT_PARSER;

	parTree* tree = PARSER.LoadTree(inFileName, "");
	PARSER.SaveTree(outFileName, "", tree, parManager::BINARY);
	delete tree;

	SHUTDOWN_PARSER;

	return 0;
}
