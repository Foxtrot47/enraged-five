//-----------------------------------------------------------------------------
// xbCat.cs
//
// This file is the main source file for the C# version of xbCat.
//
// Portions of this file originated in the xbWatson samble code which
// is covered by the following copyright:
//
// Xbox Advanced Technology Group.
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------

using System;
using XDevkit;
using System.Threading;

//-------------------------------------------------------------------------
// Name: xbCat
// Desc: The xbCat class is the main application class for
//       C# xbCat. It connects to the default Xbox, waits
//       for notifications, and then handles those notifications.
//-------------------------------------------------------------------------
public class xbCat
{
    // xManager gives you access to one or more devkits, including letting
    // you add to the list of available consoles.
    private XDevkit.XboxManagerClass xManager;
    private XDevkit.XboxConsole xboxConsole;
    private XDevkit.IXboxDebugTarget xDebugTarget;

    System.Threading.Semaphore exitSemaphore;

    string consoleName;
    string xexName;
    string cmdLine;
    string mediaDir;

    bool exitedCleanly;
    bool reboot;
    bool coldReboot;

    int timeout;

    System.Text.RegularExpressions.Regex matchExpression;

    // Are we connected to an Xbox devkit?
    bool fConnected;

    [STAThread]
    static int Main(string[] args)
    {
        try
        {
            xbCat xboxxbCat = new xbCat(args);
            xboxxbCat.InitDM();
            return xboxxbCat.exitedCleanly ? 0 : 2;
        }
        catch (Exception exp)
        {
            Console.Write("Could not connect to Xbox\n " + exp);
            return 1;
        }
    }

    void Usage(string arg)
    {
        if (arg != null)
            Console.WriteLine("Invalid option " + arg);
        Console.WriteLine("");
        Console.WriteLine("Copy the TTY output from a console to stdout, optionally rebooting and launching an executable");
        Console.WriteLine("");
        Console.WriteLine("usage:");
        Console.WriteLine("  xbcat [options] [executable [args]]");
        Console.WriteLine("");
        Console.WriteLine("        /X:target  IP address or name of target console");
        Console.WriteLine("        /R         Title reboot (fast)");
        Console.WriteLine("        /C         Cold reboot (slow)");
        Console.WriteLine("        /T:seconds Timeout before exiting with an error code");
        Console.WriteLine("        /M:regex   Regex to match before exiting successfully");
        Console.WriteLine("        /?         Display this message");
        Environment.Exit(1);
    }

    void ProcessArgs(string[] args)
    {
        for (int i = 0; i < args.Length; i++)
        {
            if (args[i][0] != '/')
            {
                if (xexName == null)
                    xexName = args[i];
                else if (cmdLine == null)
                    cmdLine = args[i];
                else
                    Usage(args[i]);

                continue;
            }

            switch (char.ToUpper(args[i][1]))
            {
                case 'X':
                    if (args[i][2] != ':')
                        Usage(args[i]);
                    consoleName = args[i].Substring(3);
                    break;

                case 'R':
                    reboot = true;
                    break;

                case 'C':
                    coldReboot = true;
                    break;

                case 'T':
                    if (args[i][2] != ':' || !int.TryParse(args[i].Substring(3), out timeout))
                        Usage(args[i]);
                    break;

                case 'M':
                    if (args[i][2] != ':')
                        Usage(args[i]);
                    try
                    {
                        matchExpression = new System.Text.RegularExpressions.Regex(args[i].Substring(3));
                    }
                    catch (ArgumentException ex)
                    {
                        Console.WriteLine("Unable to parse regular expression: " + ex.Message + "\n");
                        Usage(args[i]);
                    }
                    break;

                case '?':
                    Usage(null);
                    break;

                default:
                    Usage(args[i]);
                    break;
            }
        }

        if (xexName != null)
        {
            int index = xexName.LastIndexOfAny("\\/".ToCharArray());
            if (index != -1)
                mediaDir = xexName.Substring(0, index);
        }
    }
    
    static void TimedOut(object exitSemaphore)
    {
        Console.Write("xbCat: Timeout expired\n");
        ((System.Threading.Semaphore)exitSemaphore).Release();
    }


    //---------------------------------------------------------------------
    // Name: Constructor
    //---------------------------------------------------------------------
    public xbCat(string[] args)
    {
        exitSemaphore = new System.Threading.Semaphore(0, 1);
        fConnected = false;
        exitedCleanly = false;
        reboot = coldReboot = false;
        timeout = 0;

        ProcessArgs(args);

        xManager = new XboxManagerClass();
        // Open the default Xbox. This will fail if there is no default
        // Xbox or if it is unreachable for any reason.
        xboxConsole = xManager.OpenConsole(consoleName != null ? consoleName : xManager.DefaultConsole);

        if (reboot || coldReboot || xexName != null)
            xboxConsole.Reboot(xexName, mediaDir, cmdLine, XboxRebootFlags.Wait | (coldReboot ? XboxRebootFlags.Cold : XboxRebootFlags.Title));

        if (coldReboot)
            Thread.Sleep(30000);

        if (timeout > 0)
            new System.Threading.Timer(TimedOut, exitSemaphore, timeout * 1000, 0);
    }


    //---------------------------------------------------------------------
    // Name: InitDM
    // Desc: Request notifications from the Xbox devkit
    //---------------------------------------------------------------------
    public void InitDM()
    {
        // Get the Xbox we are talking to
        try 
        {
            // Hook up Notification Channel - tell the Xbox devkit to send
            // notifications to us. The set of notifications is documented under
            // DmNotify in the help file.
            xboxConsole.OnStdNotify += new XboxEvents_OnStdNotifyEventHandler( xboxConsole_OnStdNotify );
            xDebugTarget = xboxConsole.DebugTarget;
            xDebugTarget.ConnectAsDebugger( null, XboxDebugConnectFlags.Force );
            bool notStopped;
            xDebugTarget.Go(out notStopped);
        }
        catch(Exception e)
        {
            // Display an error message if connecting fails. Translating
            // error codes to text is not currently supported.
            xboxConsole.OnStdNotify -= new XboxEvents_OnStdNotifyEventHandler( xboxConsole_OnStdNotify );
            Console.Write( "Could not connect to Xbox\n " + e );
            
        }

        exitSemaphore.WaitOne();
    }


    //---------------------------------------------------------------------
    // Name: xboxConsole_OnStdNotify
    // Desc: Callback for DmNotify style notifications
    //---------------------------------------------------------------------
    private void xboxConsole_OnStdNotify( XboxDebugEventType eventCode, IXboxEventInfo eventInformation )
    {
        bool fStopped = eventInformation.Info.IsThreadStopped == 0 ? false : true;
        bool NotStopped, Exception = true;
        switch( eventCode )
        {
                // Handler for DM_EXEC changes, mainly for detecting reboots
            case XDevkit.XboxDebugEventType.ExecStateChange:
                if ( eventInformation.Info.ExecState != XDevkit.XboxExecutionState.Rebooting )
                    xDebugTarget.StopOn( XboxStopOnFlags.OnStackTrace, true );
                if( !fConnected )
                {
                    fConnected = true;
                    Console.Write ( "xbCat: Connection to Xbox successful\n" );
                }
                else if( eventInformation.Info.ExecState == XDevkit.XboxExecutionState.Rebooting )
                    Console.Write("xbCat; Xbox is restarting\n");
                break;

                // Handler for DM_DEBUGSTR notifications, to display debug
                // print text.
            case XDevkit.XboxDebugEventType.DebugString:
                Console.Write(eventInformation.Info.Message);
                if( fStopped )
                {
                    eventInformation.Info.Thread.Continue( Exception );
                    xDebugTarget.Go( out NotStopped );
                    exitSemaphore.Release();
                }
                if (matchExpression != null && matchExpression.IsMatch(eventInformation.Info.Message))
                {
                    exitedCleanly = true;
                    exitSemaphore.Release();
                }
                break;

                // Handler for asserts triggered on the Xbox
            case XDevkit.XboxDebugEventType.AssertionFailed:
                Console.Write("Assertion failed: \r\n");
                Console.Write(eventInformation.Info.Message);

                if( fStopped )
                {
                    exitSemaphore.Release();
                }

                break;

                // Handler for fatal errors - RIPs in the Xbox OS
            case XDevkit.XboxDebugEventType.RIP:
                Console.Write("RIP : " + eventInformation.Info.Message);
                if( fStopped )
                {
                    exitSemaphore.Release();
                }
                break;

                // Handler for a breakpoint - DM_BREAK
            case XDevkit.XboxDebugEventType.ExecutionBreak:
            {
                Console.Write("Break: " + eventInformation.Info.Address + " " +
                    eventInformation.Info.Thread.ThreadId + "\n" );
                exitSemaphore.Release();
                break;
            }

                // Handler for a data breakpoint - DM_DATABREAK
            case XDevkit.XboxDebugEventType.DataBreak:
            {
                Console.Write("Databreak : " + eventInformation.Info.Address + " " +
                    eventInformation.Info.Thread.ThreadId + " " +
                    eventInformation.Info.GetType() + " " +
                    eventInformation.Info.Address );
                exitSemaphore.Release();
                break;
            }

                // Handler for an exception (access violation, etc.) - DM_EXCEPTION
            case XDevkit.XboxDebugEventType.Exception:

                if (eventInformation.Info.Flags == XDevkit.XboxExceptionFlags.FirstChance)
                {
                    if( eventInformation.Info.Code != 0x406D1388 ) // Ignore the setthreadname exception
                    {
                        Console.Write("First Chance Exception : " + eventInformation.Info.Thread.ThreadId +
                                " " + eventInformation.Info.Code + " " +
                                eventInformation.Info.Address + " " +
                                eventInformation.Info.Flags + "\n");

                        // At this point we could check for more what type of exception was
                        // reported and display more information.
                    }
                }
                else
                {
                    Console.Write("Exception : " + eventInformation.Info.Thread.ThreadId +
                            " " + eventInformation.Info.Code + " " +
                            eventInformation.Info.Address + " " +
                            eventInformation.Info.Flags + "\n");

                }

                if (fStopped)
                {
                    exitSemaphore.Release();
                } 
                break;

                // Handle all those notification types that don't take special handling.
            default:
                Console.Write(eventInformation.Info.Message);
                break;
                    
        }
    }
}
