
from xml.dom.minidom import *
from basetype import *

import atHash

class StructSpec(MemberSpec):
    def __init__(self, *args, **kwargs):
        self.addGroupWidget = None
        self.type = None
        super(StructSpec, self).__init__(*args, **kwargs)
        
    def CheckAttributes(self):
        self.WikiPage = "Struct_PSC_tag"
        MemberSpec.CheckAttributes(self)
        self.RequiredAttribute("type", safeVal="")
        self.OptionalAttribute("addGroupWidget", "true")



class PointerSpec(StructSpec):
    def __init__(self, *args, **kwargs):
        self.policy = None
        self.onUnknownType = None
        self.toString = None
        self.fromString = None
        self.userHandlesNull = None
        super(PointerSpec, self).__init__(*args, **kwargs)

    def CheckAttributes(self):
        self.WikiPage = "Pointer_PSC_tag"
       
        MemberSpec.CheckAttributes(self)
        self.RequiredAttribute("type", safeVal="")
        self.OptionalAttribute("addGroupWidget", "true")
        self.RequiredAttribute("policy", ("owner", "simple_owner", "external_named", "link", "lazylink"), safeVal="owner")

        if self.policy == "owner":
            self.OptionalAttribute("onUnknownType")

        if self.policy == "external_named":
            self.RequiredAttribute("toString")
            self.RequiredAttribute("fromString")
            self.OptionalAttribute("userHandlesNull", "false")

class AutoregSpec(GenericSpec):
#    ClassesToRegister = {}
#    RegisterAllInFile = False
    
    def __init__(self, domNode, parentObj):
        self.allInFile = "true"
        GenericSpec.__init__(self, domNode, parentObj)
#        typeString = ""
#        for node in domNode.childNodes:
#            if node.nodeType == xml.dom.Node.TEXT_NODE:
#                typeString += node.data
#        self.TypeList = typeString.split()
#        for i in self.TypeList:
#            AutoregSpec.ClassesToRegister[i] = 0 # add to the dict

    def CheckAttributes(self):
        self.WikiPage = "Autoregister_PSC_tag"
        self.OptionalAttribute("allInFile", "true", ("true", "false"))
#        if self.allInFile == "true":
#            AutoregSpec.RegisterAllInFile = True;

class RawTextSpec(GenericSpec):
    def __init__(self, domNode, parentObj):
        GenericSpec.__init__(self, domNode, parentObj)
        textString = ""
        for node in domNode.childNodes:
            if node.nodeType == xml.dom.Node.TEXT_NODE or node.nodeType == xml.dom.Node.CDATA_SECTION_NODE:
                if (node.data.strip() != ''): # if the node isn't all whitespace
                    textString += node.data
                    textString += "\n"
        self.TextData = textString
    
class CIncludeSpec(RawTextSpec):
    def __init__(self, domNode, parentObj):
        RawTextSpec.__init__(self, domNode, parentObj)
        self.Includes = set([x.strip() for x in self.TextData.split("\n")])
        try:
            self.Includes.remove("")
        except:
            pass

class HIncludeSpec(RawTextSpec):
    def __init__(self, domNode, parentObj):
        RawTextSpec.__init__(self, domNode, parentObj)
        self.Includes = set([x.strip() for x in self.TextData.split("\n")])
        try:
            self.Includes.remove("")
        except:
            pass

class HInsertSpec(RawTextSpec):
    def __init__(self, domNode, parentObj):
        RawTextSpec.__init__(self, domNode, parentObj)
        
class StructDefSpec(GenericSpec):
    def __init__(self, domNode, parentObj):

        self.type = None
        self.version = None
        self.onPreLoad = None
        self.onPostLoad = None
        self.onPreSave = None
        self.onPostSave = None
        self.base = None
        self.usedi = None
        self.onPreAddWidgets = None
        self.onPostAddWidgets = None
        self.constructible = None
        self.onPreSet = None
        self.onPostSet = None
        self.autoregister = None
        self.template = None
        self.cond = None
        self.rsc = None
        self.preserveNames = None
        self.generate = None
        self.name = None
        self.layoutHint = None
        self.simple = None
        self.platform = None
    
        self.StructdefID = GetCurrStructID()
        if self.StructdefID < 0:
            self.StructdefID = 0
        self.StructdefID += 1

        structdefName = ""
        if domNode.attributes.has_key("type"):
            structdefName = domNode.getAttribute("type")
        
        # fix my typo
        if domNode.attributes.has_key("constructable"):
            domNode.setAttribute("constructible", domNode.getAttribute("constructable"))
            domNode.removeAttribute("constructable")
            
        SetStructNameAndID(structdefName, self.StructdefID)
        SetMemberNameAndID("", 0)

        GenericSpec.__init__(self, domNode, parentObj)
        self.GenStruct = None
        self.GenBuildNum = None
        self.Children = BuildSpecObjects(domNode, self)
        self.BuildNumber = 0
        
        if self.name:
            self.MangledCppType = MangleCppName(self.type)
            self.ParserName = self.name
        else:
            self.MangledCppType = MangleCppName(self.type)
            self.ParserName = self.MangledCppType
            
            
        # need to strip any leading namespace qualifiers, or else we get problems with code like:
        # ::rage::parStructureStaticData ::rage::fiDeviceInstaller::InitData::parser_Data = {
        # (cpp parses the whole line as a single qualified name)
        self.CppType = self.type
        if self.type[:2] == "::":
            self.CppType = self.type[2:]
            
        self.NameHash = atHash.atLiteralStringHash(self.ParserName)
        
        # check that all children are valid nodes
        for kid in self.Children:
            if not isinstance(kid, MemberSpec) and not isinstance(kid, AssertSpec) and not isinstance(kid, PadSpec) and not isinstance(kid, HInsertSpec):
                CodeGenError("Found a node that can't be inside a <structdef>: <%s>" % kid.DomNode.nodeName, wikiPage=self.WikiPage)
        
        # check that no two children have the same name or namehash
        for i in range(len(self.Children)):
            for j in range(i+1, len(self.Children)):
                if isinstance(self.Children[i], MemberSpec) and isinstance(self.Children[j], MemberSpec):
                    if self.Children[i].name == self.Children[j].name:
                        CodeGenError("Two children of structdef %s have the same name (%s)" % (self.name, self.Children[j].name))
                    if self.Children[i].NameHash == self.Children[j].NameHash:
                        CodeGenError("Two children of structdef %s have the same hashvalues for their names (%s and %s)" % (self.name, self.Children[i].name, self.Children[j].name))


    def SetStructdefErrorInfo(self):
        if self.type:
            SetStructNameAndID(self.type, self.StructdefID)
        SetMemberNameAndID("", -1)

    def UnsetStructdefErrorInfo(self):
        SetStructNameAndID("", -1)
        SetMemberNameAndID("", -1)

    def CheckAttributes(self):
        self.WikiPage = "Structdef_PSC_tag"
        self.SetStructdefErrorInfo()
        GenericSpec.CheckAttributes(self)
        self.RequiredAttribute("type", safeVal="")        
        self.OptionalAttribute("version", "0")
        self.OptionalAttribute("onPreLoad")
        self.OptionalAttribute("onPostLoad")
        self.OptionalAttribute("onPreSave")
        self.OptionalAttribute("onPostSave")
        self.OptionalAttribute("base")
        self.OptionalAttribute("usedi", "false")
        self.OptionalAttribute("onPreAddWidgets")
        self.OptionalAttribute("onPostAddWidgets")
        self.OptionalAttribute("constructible", "true", ["true", "false"])
        self.OptionalAttribute("onPreSet")
        self.OptionalAttribute("onPostSet")
        self.OptionalAttribute("autoregister", "true", ["true", "false"])
        self.OptionalAttribute("template", "false", ["true", "false"])
        self.OptionalAttribute("cond", "1")
        self.OptionalAttribute("rsc", "false", ["true", "false"])
        self.OptionalAttribute("preserveNames", "false", ["true", "false"])
        self.OptionalAttribute("layoutHint")
        self.OptionalAttribute("simple", "false", ["true", "false"])
        self.OptionalAttribute("platform")
        
        default = None
        if GenericSpec.CurrDocumentSpec:
            default = GenericSpec.CurrDocumentSpec.generate
        self.OptionalAttribute("generate", default)
        if self.generate:
            self.generate = self.generate.lower()
                
        self.OptionalAttribute("name")

    def ForEachMember(self, func):
        self.SetStructdefErrorInfo()
        memberNum = 0
        for i in self.Children:
            if isinstance(i, MemberSpec):
                i.SetErrorInfo()
                func(i)
                i.UnsetErrorInfo()
                memberNum += 1

    def IterMembers(self):
        memberNum = 0
        for i in self.Children:
            if isinstance(i, MemberSpec):
                i.SetErrorInfo()
                yield i
                i.UnsetErrorInfo()
                memberNum += 1
       

 
