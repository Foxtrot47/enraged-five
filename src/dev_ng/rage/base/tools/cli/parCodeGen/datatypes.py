
import hashlib
import re
import sys
import time
import xml.dom
import xml.dom.minidom
from xml.sax.saxutils import *

import atHash

from basetype import *
from structtype import *
from inttypes import *
from floattypes import *
from vectortypes import *
from arraytype import *
from stringtypes import *
from enumtype import *
from matrixtypes import *
from maptype import *

class DocumentSpec(GenericSpec):
    def __init__(self, domNode, generatedDoc):
        self.generate = None
        self.scriptFile = None
    
        # reset the const table for each document
        ResetConstTable()
    
        GenericSpec.__init__(self, domNode, False) # passing a null parent
        GenericSpec.CurrDocumentSpec = self
        self.Children = BuildSpecObjects(domNode, False) # children's parent refs. shouldn't point back to the doc.
        GenericSpec.CurrDocumentSpec = None
        for i in self.Children:
            i.document = self # but we do need them to get to the document object somehow
        if generatedDoc != None:
            for i in [x for x in self.Children if isinstance(x, StructDefSpec)]:
                i.GenStruct = None
                i.GenBuildNum = 0
                structName = i.type
                # search for a struct and a buildnumber in generatedDoc with the same name
                for j in generatedDoc.documentElement.childNodes:
                    if j.nodeName == "structdef" and j.getAttribute("type") == structName:
                        i.GenStruct = j
                    if j.nodeName == "buildnumber" and j.getAttribute("type") == structName:
                        i.GenBuildNum = int(j.getAttribute("value"))
                        

    def CheckAttributes(self):
        self.WikiPage = "Structdef_PSC_tag"
        self.OptionalAttribute("generate", "")
        self.OptionalAttribute("scriptFile", "")
        
    def NeedsCDefinitions(self):
        if not self.Children:
            return False
        for i in self.Children:
            if isinstance(i, StructDefSpec) or isinstance(i, EnumDefSpec):
                if "class" in i.generate:
                    return True
        return False
       
    def NeedsSchDefinitions(self):
        return self.scriptFile
        
    def ComputeGUID(self):
        xml = self.DomNode.toxml()
        hasher = hashlib.md5()
        hasher.update(xml)
        self.GUID = hasher.hexdigest()
        

class StringkeySpec(GenericSpec):
    def __init__(self, domNode, parentObj):
        self.name = None
        self.string = None
        super(StringkeySpec, self).__init__(*args, **kwargs)
        self.Key = atHash.atStringHash(self.string)

    def CheckAttributes(self):
        self.WikiPage = "Stringkey_PSC_tag"
        self.RequiredAttribute("name", safeVal="")
        self.RequiredAttribute("string", safeVal="")

class ConstSpec(GenericSpec):
    def __init__(self, *args, **kwargs):
        self.name = None
        self.value = None
        super(ConstSpec, self).__init__(*args, **kwargs)
        
    def CheckAttributes(self):
        self.WikiPage = "Const_PSC_tag"
        self.RequiredAttribute("name", safeVal="")
        self.RequiredAttribute("value", safeVal="")
        try:
            int(self.value, 0)
        except:
            CodeGenError("In constant '%s', couldn't convert string value '%s' into an integer literal" % (self.name, self.value), wikiPage=self.WikiPage)
        AddKnownConstant(self.name, self.value)

class BoolSpec(MemberSpec):
    def __init__(self, *args, **kwargs):
        self.onWidgetChanged = None
        self.init = None
        super(BoolSpec, self).__init__(*args, **kwargs)

    def CheckAttributes(self):
        self.WikiPage = "Bool_PSC_tag"
        MemberSpec.CheckAttributes(self)
        self.OptionalAttribute("onWidgetChanged")
        self.OptionalAttribute("init", "false")

class BoolVSpec(BoolSpec):
    pass

AddType(MapSpec,        "map",      "")
AddType(AssertSpec,     "assert",   "")
AddType(ReportSpec,     "report",   "")
AddType(ArraySpec,      "array",    "")
AddType(FloatSpec,      "float",    "")
AddType(AngleSpec,      "float",    "angle")
AddType(Float16Spec,    "Float16",  "")
AddType(DoubleSpec,     "double",   "")
AddType(IntSpec,        "int",      "")
AddType(IntSpec,        "s32",      "")
AddType(U32Spec,        "u32",      "")
AddType(SizeTSpec,      "size_t",   "")
AddType(PtrdiffTSpec,   "ptrdiff_t","")
AddType(Color32Spec,    "Color32",  "")
AddType(Color32Spec,    "u32",      "color")
AddType(CharSpec,       "char",     "")
AddType(CharSpec,       "s8",       "")
AddType(U8Spec,         "u8",       "")
AddType(ShortSpec,      "short",    "")
AddType(ShortSpec,      "s16",      "")
AddType(U16Spec,        "u16",      "")
AddType(S64Spec,        "s64",      "")
AddType(U64Spec,        "u64",      "")
AddType(StringSpec,     "string",   "")
AddType(StructSpec,     "struct",   "")
AddType(StructSpec,     "class",    "")
AddType(PointerSpec,    "pointer",  "")
AddType(StructDefSpec,  "structdef", "")
AddType(Vector2Spec,    "Vector2",  "")
AddType(Vector3Spec,    "Vector3",  "")
AddType(Vector3ColorSpec, "Vector3", "color")
AddType(Vector4Spec,    "Vector4",  "")
AddType(Matrix34Spec,   "Matrix34", "")
AddType(Matrix44Spec,   "Matrix44", "")
AddType(Vec2VSpec,      "Vec2V", "")
AddType(Vec3VSpec,      "Vec3V", "")
AddType(Vec3VColorSpec, "Vec3V", "color")
AddType(Vec4VSpec,      "Vec4V", "")
AddType(ScalarVSpec,    "ScalarV", "")
AddType(VecBoolVSpec,   "VecBoolV", "")
AddType(Mat33VSpec,     "Mat33V", "")
AddType(Mat34VSpec,     "Mat34V", "")
AddType(Mat44VSpec,     "Mat44V", "")
AddType(DocumentSpec,   "document", "")
AddType(DocumentSpec,   "rageStructureDictionary", "")
AddType(DocumentSpec,   "ParserSchema", "")
AddType(BoolSpec,       "bool",     "")
AddType(BoolVSpec,      "BoolV",    "")
AddType(EnumDefSpec,    "enumdef",  "")
AddType(EnumValSpec,    "enumval",  "")
AddType(EnumSpec,       "enum",     "")
AddType(BitsetSpec,     "bitset",   "")
AddType(AutoregSpec,    "autoregister", "")
AddType(StringkeySpec,  "stringkey", "")
AddType(HIncludeSpec,   "hinclude", "")
AddType(HInsertSpec,    "hinsert", "")
AddType(CIncludeSpec,   "cinclude", "")
AddType(PadSpec,        "pad", "")
AddType(ConstSpec,      "const", "")
