using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

// C++ Comment Extractor/Transformer
// removes non-comments from a file. Turns comments into an XMLey style document:
//
//  // Stuff
//   // goes here
//
//  /* <structdef type="foo">
//     </structdef>
//  */
//
//Becomes:
//
// 
// <structdef type="foo">
// </structdef>
// 
//

namespace ExtractXmlComments
{
    class Program
    {
        static void Main(string[] args)
        {
            foreach(string filename in args)
            {
                System.IO.StreamReader f = new System.IO.StreamReader(filename);
                string chars = f.ReadToEnd();

                f.Close();

                string outChars = ExtractAll(chars);

                System.Console.Write(outChars);
            }
        }

        // Removes everything but comments from the chars string, returns the new stripped string
        static public string ExtractComments(string chars)
        {
            int numChars = chars.Length;

            StringBuilder outStr = new StringBuilder(numChars);

            bool inBlockComment = false;
            int i = 1;
            while (i < numChars)
            {
                if (inBlockComment)
                {
                    if (chars[i] == '*' && chars[i+1] == '/') 
                    {
                        i++;
                        inBlockComment = false; // just saw the end of a block comment
                    }
                    else
                    {
                        outStr.Append(chars[i]); // still in comment, add char to buffer
                    }
                }
                else
                {
                    if (chars[i-1] == '/' && chars[i] == '/') // if starting a new single line comment
                    {
                        i++;
                        while(i < numChars && chars[i] != '\n')
                        {
                            outStr.Append(chars[i]); // add everything up to \n
                            i++;
                        }
                    }
                    else if (chars[i-1] == '/' && chars[i] == '*')
                    {
                        inBlockComment = true;
                    }
                    else if (chars[i] == '\n')
                    {
                        outStr.Append('\n'); // add non-comment newlines to keep the line count the same
                    }
                }
                i++;
            }
            return outStr.ToString();
        }

        public class XmlMetadataBlock
        {
            public XmlMetadataBlock(string tag)
            {
                BeginTag = "<" + tag;
                EndTag = "</" + tag + ">";
            }

            // True if the tail of 'chars' (up to the ith character) matches the EndTag
            public bool EndsMatch(string chars, int i)
            {
                string subStr = chars.Substring(Math.Max(0, i-EndTag.Length+1), EndTag.Length);
                return subStr == EndTag;
            }

            public bool StartsMatch(string chars, int i)
            {
                string subStr = chars.Substring(i, Math.Min(BeginTag.Length, chars.Length - i));
                return subStr == BeginTag;
            }

            public string BeginTag;
            public string EndTag;
        };

        static public List<XmlMetadataBlock> GetMetadataBlockList()
        {
            List<XmlMetadataBlock> list = new List<XmlMetadataBlock>();
            list.Add(new XmlMetadataBlock("structdef"));
            list.Add(new XmlMetadataBlock("buildnumber"));
            list.Add(new XmlMetadataBlock("enumdef"));
            list.Add(new XmlMetadataBlock("autoregister"));
            return list;
        }

        // Removes everything but metadata for the parser
        static public string ExtractXmlMetadata(string chars)
        {
            StringBuilder outStr = new StringBuilder(chars.Length);

            List<XmlMetadataBlock> xmlBlocks = GetMetadataBlockList();
            int strLen = chars.Length;
            int i = 0;

            int inWhichBlock = -1;
            bool maybeInALeaf = false;

            while(i < strLen)
            {
                if (inWhichBlock >= 0)
                {
                    outStr.Append(chars[i]);
                    if (chars[i] == '<')
                    {
                        // another start tag, so we can't be reading a leaf node.
                        maybeInALeaf = false;
                    }
                    if (chars[i] == '>')
                    {
                        if (maybeInALeaf && chars.Substring(i - 1, 2) == "/>")
                        {
                            inWhichBlock = -1;
                        }
                        if (xmlBlocks[inWhichBlock].EndsMatch(chars, i))
                        {
                            inWhichBlock = -1;
                        }
                    }
                }
                else
                {
                    if (chars[i] == '<')
                    {
                        for(int which = 0; which < xmlBlocks.Count; which++)
                        {
                            if (xmlBlocks[which].StartsMatch(chars, i))
                            {
                                inWhichBlock = which;
                                outStr.Append(chars[i]);
                                maybeInALeaf = true;
                                break;
                            }
                        }
                    }
                    if (chars[i] == '\n')
                    {
                        outStr.Append('\n');
                    }
                }
                i++;
            }

            return outStr.ToString();
        }

        static public string ExtractAll(string chars)
        {
            return ExtractXmlMetadata(ExtractComments(chars));
        }
    }
}
