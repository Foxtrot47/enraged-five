
# This file contains some C++ support routines that can come in handy for any templates that are generating code or header files
# based on the parser metadata
from __future__ import with_statement

from templatesupport import *
from basetype import *
from structtype import *
from inttypes import *
from floattypes import *
from vectortypes import *
from arraytype import *
from stringtypes import *
from enumtype import *
from matrixtypes import *
from datatypes import *
from stat import *
from maptype import *

########################################################
## GetPlatformCPPString
##
## Given a platform attribute, returns a preprocessor string for that platform, suitable for #if-ing
########################################################

''' use cases:
pc
    -> RSG_PC
ps3
    -> RSG_PS3
ps3|xbox
    -> RSG_XENON
ps3|tool
    -> (RSG_PS3 | (RSG_RSC | RSG_TOOL | __EXPORTER | __EDITOR))
pc|xbox|ps3|win32|win64
    -> (RSG_PC | RSG_XENON | RSG_PS3 | RSG_WIN32 | RSG_WIN64)
tool|!pc
    -> ( (tool) | !RSG_PC)
console|tool
    -> (__CONSOLE | (...tool...))
ps3|psn|xenon|console
    -> 
'''

        
def GetPlatformCPPString(platform):
    # input is a | seperated union of possibly negated platform types. Split this, reform it into a union of RSG_PLATNAME symbols
    inComps = platform.split("|")
    outComps = []
    for plat in inComps:
        plat = plat.strip()
        negate = False
        outComp = ""
        if plat[0] == '!':
            negate = True
            plat = plat[1:]
        if plat == "pc":
            outComp = "RSG_PC"
        elif plat == "console":
            outComp = "__CONSOLE"
        elif plat == "tool":
            outComp = "( RSG_RSC || RSG_TOOL || __EXPORTER || __EDITOR )"
        elif plat in ("win32pc", "x86", "win32"):
            outComp = "(RSG_PC && RSG_CPU_X86)"
        elif plat in ("win64pc", "x64", "win64"):
            outComp = "(RSG_PC && RSG_CPU_X64)"
        elif plat in ("xenon", "xbox360"):
            outComp = "RSG_XENON"
        elif plat in ("ps3", "psn"):
            outComp = "RSG_PS3"
        elif plat in ("durango"):
            outComp = "RSG_DURANGO"
        elif plat in ("orbis"):
            outComp = "RSG_ORBIS"
        elif plat in ("prospero"):
            outComp = "RSG_PROSPERO"
        elif plat in ("scarlett"):
            outComp = "RSG_SCARLETT"
        else:
            CodeGenError('Couldn\'t understand platform name "' + plat + '"')
        if negate:
            outComp = "!" + outComp
        outComps.append(outComp)
        
    if len(outComps) == 0:
        return ""
    elif len(outComps) == 1:
        return outComps[0]
    else:
        return "(" + " || ".join(outComps) + ")"

def WriteStartPoundIfPlatform(obj):
    if obj.platform:
        obj.WriteString('''\n#if %(:platString)s // (platform="%(platform)s")''', platString = GetPlatformCPPString(obj.platform))
        
def WriteEndPoundIfPlatform(obj):
    if obj.platform:
        obj.WriteString('''\n#endif // %(:platString)s (platform="%(platform)s")''', platString = GetPlatformCPPString(obj.platform))

## Use Python's 'with' statement for an RAII way to conditionally include the #if (platformstring) ... #endif pairs if necessary
## For example:
## def WriteSomeMemberData(self):
##     with WritePoundIfPlatform(self):
##         self.WriteString("blah blah blah")
class WritePoundIfPlatform(object):
    def __init__(self, member):
        self.member = member
    
    def __enter__(self):
        self.startPos = GenericSpec.OutFile.tell()
        WriteStartPoundIfPlatform(self.member)
        self.endPos = GenericSpec.OutFile.tell()
        return self
    
    def __exit__(self, type, value, traceback):
        if self.member.platform:
            if GenericSpec.OutFile.tell() == self.endPos:
                # we didn't actually write anything, so don't write the #if either
                GenericSpec.OutFile.seek(self.startPos)
            else:
                # need a #endif
                WriteEndPoundIfPlatform(self.member)
        return False

@AddMethod(StructDefSpec)
def ForEachMember_IfPlatform(self, func):
    for x in self.IterMembers():
        with WritePoundIfPlatform(x):
            func(x)

########################################################
## GetCType and GetSplitCType
##
## Given a member spec, GetCType gets the C++ type of the member,
## and GetSplitCType gets the type as a (left,right) pair such that you can declare a variable
## as "<left> m_Name <right>;"
##
########################################################

@AddMethod(MemberSpec)
def GetCType(self, **kw):
    return "unknownType"

@AddMethod(MemberSpec)  
def GetSplitCType(self, **kw):
    return (self.GetCType(**kw), "")
    
@AddMethod(StructSpec)
def GetCType(self, **kw):
    return self.type

@AddMethod(PointerSpec)
def GetCType(self, **kw):
    if self.policy == 'lazylink':
        return "::rage::parLazyLink< " + self.type + " > "
    return self.type + "*"

@AddMethod(MapSpec)
def GetCType(self, **kw):
    if self.type == 'atMap':
        return '::rage::atMap< ' + self.KeySpec.GetCType(**kw) + ", " + self.ChildSpec.GetCType(**kw) + " > "
    elif self.type == 'atBinaryMap':
        return '::rage::atBinaryMap< ' + self.ChildSpec.GetCType(**kw) + ", " + self.KeySpec.GetCType(**kw) + " > "
    return "NULL"
        
@AddMethod(ArraySpec)
def GetCType(self, **kw):
    if self.type == 'member':
        (preType, postType) = self.ChildSpec.GetSplitCType(**kw)
        return preType + '[' + self.size + ']' + postType
    elif self.type == 'atRangeArray':
        return '::rage::atRangeArray< ' + self.ChildSpec.GetCType(**kw) + ", " + self.size + " > "
    elif self.type == 'atFixedArray':
        return '::rage::atFixedArray< ' + self.ChildSpec.GetCType(**kw) + ", " + self.size + " > "
    elif self.type == 'atArray':
        return '::rage::atArray< ' + self.ChildSpec.GetCType(**kw) + ", 0, ::rage::u" + self.indexBits + "> "
    elif self.type == 'virtual':
        return 'void'
    return "void*"

@AddMethod(ArraySpec)    
def GetSplitCType(self, **kw):
    if self.type == 'member':
        (preType, postType) = self.ChildSpec.GetSplitCType(**kw)
        return (preType, '[' + self.size + ']' + postType)
    elif self.type == 'pointer':
        (preType, postType) = self.ChildSpec.GetSplitCType(**kw)
        if len(postType) > 0:
            postType = ""
            preType = preType + '*' * postType.count('[') # if we have a pointer to a fixed length array, convert it into a pointer to a pointer
        return (preType + "*", postType)
    else:
        return (self.GetCType(**kw), "")
    
@AddMethod(StringSpec)
def GetCType(self, **kw):
    if self.type == "member":
        return "char[" + self.size + "]"
    elif self.type == "ConstString":
        return "::rage::ConstString"
    elif self.type == "atString":
        return "::rage::atString"
    elif self.type == "wide_member":
        return "::rage::char16[" + self.size + "]"
    elif self.type == "wide_pointer":
        return "const ::rage::char16*"
    elif self.type == "atWideString":
        return "::rage::atWideString"
    elif self.type == "atPartialHashValue":
        return "::rage::u32"
    elif self.type in StringSpec.HashTypes or self.NamespaceType != None:
        # all the other hash type names happen to match the rage names
        return "::rage::" + self.type
    return "const char*"

@AddMethod(StringSpec)
def GetSplitCType(self, **kw):
    if self.type == 'member':
        return ("char", '[' + self.size + ']')
    elif self.type == "wide_member":
        return ("::rage::char16", "[" + self.size + "]")
    else:
        return (self.GetCType(**kw), "")

@AddMethod(EnumSpec)        
def GetCType(self, **kw):
    if self.size == "8":
        return "::rage::u8"
    elif self.size == "16":
        return "::rage::u16"
    elif kw.get("useNaturalEnumType", False):
        return self.EnumDefType
    else:
        return "::rage::u32"
    
@AddMethod(BitsetSpec)
def GetCType(self, **kw):
    if self.StorageType == "fixed32":
        return "::rage::atFixedBitSet< %s, ::rage::u32 >" % (self.numBits)
    elif self.StorageType == "fixed16":
        return "::rage::atFixedBitSet< %s, ::rage::u16 >" % (self.numBits)
    elif self.StorageType == "fixed8":
        return "::rage::atFixedBitSet< %s, ::rage::u8 >" % (self.numBits)
    elif self.StorageType == "atBitSet":
        return "::rage::atBitSet"
    else:
        return self.StorageType
    
CreateSimpleGetterFunctions("GetCType", {
    IntSpec:        "int",
    U32Spec:        "::rage::u32",
    Color32Spec:    "::rage::Color32",
    CharSpec:       "::rage::s8",
    U8Spec:         "::rage::u8",
    ShortSpec:      "::rage::s16",
    U16Spec:        "::rage::u16",
    S64Spec:        "::rage::s64",
    U64Spec:        "::rage::u64",
    Float16Spec:    "::rage::Float16",
    FloatSpec:      "float",
    DoubleSpec:     "double",
    ScalarVSpec:    "::rage::ScalarV",
    Vector2Spec:    "::rage::Vector2",
    Vector3Spec:    "::rage::Vector3",
    Vector4Spec:    "::rage::Vector4",
    Vec2VSpec:      "::rage::Vec2V",
    Vec3VSpec:      "::rage::Vec3V",
    Vec4VSpec:      "::rage::Vec4V",
    Matrix34Spec:   "::rage::Matrix34",
    Matrix44Spec:   "::rage::Matrix44",
    Mat33VSpec:     "::rage::Mat33V",
    Mat34VSpec:     "::rage::Mat34V",
    Mat44VSpec:     "::rage::Mat44V",
    BoolSpec:       "bool",
    BoolVSpec:      "::rage::BoolV",
    VecBoolVSpec:   "::rage::VecBoolV",
    SizeTSpec:      "size_t",
    PtrdiffTSpec:   "ptrdiff_t",
    }
)

########################################################
## WriteCommonCHeader 
########################################################

# This also sets self.IncludeGuardName
@AddMethod(DocumentSpec)
def WriteCommonCHeader(self, fileName, sourceFileName, useIncludeGuard=False, usage=""):
    GenericSpec.Depth = 0
    justFileName = os.path.split(fileName)[1]
    justDirName = os.path.split(os.path.split(os.path.abspath(fileName))[0])[1]
    self.IncludeGuardName = justDirName.upper().replace(".", "_") + "_" + justFileName.upper().replace(".", "_")
    yearStr = time.strftime("%Y", time.localtime())
    self.WriteString('''//
// %(:dirName)s/%(:fileName)s
//
// Copyright (C) 1999-%(:thisYear)s Rockstar Games. All Rights Reserved.
//
''', dirName=justDirName,
    fileName=justFileName,
    thisYear=yearStr)

    if useIncludeGuard:
        self.WriteString('''
#ifndef %(IncludeGuardName)s
#define %(IncludeGuardName)s
''')


    exeName = sys.argv[0]
    zipName = sys.argv[0].replace(".exe", ".zip")
    # use the .zip file instead if it exists
    if os.path.exists(zipName):
        exeName = zipName
        
    argv0date = time.strftime("%a, %d %b %Y %H:%M:%S", time.localtime(os.stat(exeName)[ST_MTIME]))
        
    self.WriteString('''
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
//////
//////      This file was generated by parCodeGen
//////        Built on: %(:generated)s
//////        Built by: %(:argv0path)s (last modified: %(:argv0date)s)
//////      
//////      DO NOT HAND EDIT IT AND DO NOT COMMIT! CHANGES WILL BE OVERWRITTEN
//////
//////      This file is built from the XML descriptions inside
//////        %(:baseFileName)s 
//////      %(:usage)s
//////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////

''',
    dirName=justDirName,
    baseFileName=sourceFileName,
    usage=usage,
    generated=time.strftime("%a, %d %b %Y %H:%M:%S (%Z)", time.localtime()),
    argv0path=os.path.abspath(exeName),
    argv0date=argv0date
    )

