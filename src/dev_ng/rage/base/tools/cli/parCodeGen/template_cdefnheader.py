from __future__ import with_statement

# This template file contains all of the code specific to generating C definition header documents from parser metadata.
# The general pattern here is to inject new member functions into the existing class hierarchy 
# and the new member functions will be used to generate the code.
#
# Most code below will use the @AddMethod decorator like so:
# @AddMethod(ClassName)
# def NewMethod(self): 
#   # do stuff
#
# That adds a ClassName.NewMethod member function
# @AddMethod is defined in templatesupport.py

from templatesupport import *
from template_cpp_support import *
from basetype import *
from structtype import *
from inttypes import *
from floattypes import *
from vectortypes import *
from arraytype import *
from stringtypes import *
from enumtype import *
from matrixtypes import *
from datatypes import *

#####################################
## Document
#####################################

@AddMethod(DocumentSpec)
def WriteCDefnHeader(self, fileName, sourceFileName):
    global IncludeSet
    GenericSpec.Depth = 0
    self.WriteCommonCHeader(fileName, sourceFileName, useIncludeGuard=True, usage="and should be added to the project")

    includeSet = set()
    includeSet.add('"parser/macros.h"')
        
    for i in self.Children:
        includeSet |= i.BuildIncludeSet()
        
    includeList = list(includeSet)
    includeList.sort()
    
    for i in includeList:
        self.WriteString('''
#include %(:i)s''', i=i)

    self.WriteString('''
    
''')

    
    forwardRefs = set()
    for i in self.Children:
        forwardRefs |= i.BuildForwardRefSet()
    forwardRefList = list(forwardRefs)
    for i in range(len(forwardRefList)):
        oldStr = forwardRefList[i]
        if oldStr.startswith("::"):
            forwardRefList[i] = oldStr[2:]
    forwardRefList.sort()
    
    indent = 0
    currNs = []
    if len(forwardRefList) > 0:
        self.WriteString('''#if __WIN32
# pragma warning(push)
# pragma warning(disable: 4099) // First seen using struct, now using class
#endif

''')

    for i in forwardRefList:
        toks = i.split("::")
        if len(toks) > 1:
            newNs = toks[:-1]
            commonPrefix = len(currNs)
            
            # pop off namespaces from currNs until we find a common prefix between currNs and newNs
            while len(currNs) > len(newNs) or currNs != newNs[:len(currNs)]:
                indent -= 1
                self.WriteString("\t" * indent + "}\n") # pop ns
                currNs.pop()
                
            # now push namespaces until we currNs equals newNs
            while currNs != newNs:
                oldLen = len(currNs)
                newToks = currNs[:]
                newToks.append(newNs[oldLen])
                currNs = newToks[:]
                self.WriteString("\t" * indent + "namespace %s {\n" % (newNs[oldLen]))
                indent += 1

        self.WriteString("\t" * indent + "class %s;\n" % (toks[-1]))

    # finally pop any unclosed namespaces at the end
    for i in range(len(currNs)):
        indent -= 1
        self.WriteString("\t" * indent + "} // namespace %s\n" % (currNs[-(i+1)])) # pop ns
    
    if len(forwardRefList) > 0:
        self.WriteString('''
#if __WIN32
# pragma warning(pop)
#endif
''')

    for i in self.Children:
        i.WriteCDefnHeader()
        
        
    self.WriteString('''
    
PAR_CHECK_OUT_OF_DATE_HEADER(%(:includeGuardStripped)s, id_%(GUID)s)
    
#endif
''', includeGuardStripped=self.IncludeGuardName[:-2]) # remove the _H from the include guard

#####################################
## Build Include Set
#####################################

@AddMethod(GenericSpec)
def BuildIncludeSet(self):
    return set()

CreateSimpleGetterFunctions("BuildIncludeSet", {
    Vector2Spec: set(['"vector/vector2.h"']),
    Vector3Spec: set(['"vector/vector3.h"']),
    Vector4Spec: set(['"vector/vector4.h"']),
    Vec2VSpec: set(['"vectormath/vec2v.h"']),
    Vec3VSpec: set(['"vectormath/vec3v.h"']),
    Vec4VSpec: set(['"vectormath/vec4v.h"']),
    Matrix34Spec: set(['"vector/matrix34.h"']),
    Matrix44Spec: set(['"vector/matrix44.h"']),
    Mat33VSpec: set(['"vectormath/mat33v.h"']),
    Mat34VSpec: set(['"vectormath/mat34v.h"']),
    Mat44VSpec: set(['"vectormath/mat44v.h"']),
    BoolVSpec: set(['"vectormath/boolv.h"']),
    ScalarVSpec: set(['"vectormath/scalarv.h"']),
    VecBoolVSpec: set(['"vectormath/vecboolv.h"']),
    Color32Spec: set(['"vector/color32.h"']),
    BitsetSpec: set(['"atl/bitset.h"']),
    Float16Spec: set(['"math/float16.h"']),
    })
    
@AddMethod(StructDefSpec)
def BuildIncludeSet(self):
    self.SetStructdefErrorInfo()
    s = set()
    if self.rsc == "true":
        s.add('"data/resource.h"')
    s.add('"data/base.h"')
    for i in self.IterMembers():
        s |= i.BuildIncludeSet()
    return s
    
@AddMethod(ArraySpec)
def BuildIncludeSet(self):
    s = set()
    if self.type in ('atArray', 'atRangeArray', 'atFixedArray'):
        s.add('"atl/array.h"')
    return s | self.ChildSpec.BuildIncludeSet()
    
@AddMethod(MapSpec)
def BuildIncludeSet(self):
    s = set()
    if self.type in ('atMap'):
        s.add('"atl/map.h"')
    elif self.type in ('atBinaryMap'):
        s.add('"atl/binmap.h"')
    return s | self.ChildSpec.BuildIncludeSet() | self.KeySpec.BuildIncludeSet()
    
@AddMethod(StringSpec)
def BuildIncludeSet(self):
    s = set()
    if self.type in ('wide_pointer', 'wide_member', 'atWideString'):
        s.add('"string/unicode.h"')
    if self.type == 'atString':
        s.add('"atl/string.h"')
    elif self.type == 'ConstString':
        s.add('"string/string.h"')
    elif self.type == 'atWideString':
        s.add('"atl/wstring.h"')
    elif self.type in StringSpec.HashTypes:
        s.add('"atl/hashstring.h"')
    return s
    
@AddMethod(EnumDefSpec)
def BuildIncludeSet(self):
    s = set()
    if "bitset" in self.generate:
        s.add('"atl/bitset.h"')
    return s
    
@AddMethod(HIncludeSpec)
def BuildIncludeSet(self):
    return self.Includes
    
#####################################
## Build Include Set
#####################################

@AddMethod(GenericSpec)
def BuildForwardRefSet(self):
    return set()
    
@AddMethod(StructDefSpec)
def BuildForwardRefSet(self):
    self.SetStructdefErrorInfo()
    s = set()
    for i in self.IterMembers():
        s |= i.BuildForwardRefSet()
    return s
    
@AddMethod(PointerSpec)
def BuildForwardRefSet(self):
    s = set()
    s.add(self.type)
    return s
    
@AddMethod(ArraySpec)
def BuildForwardRefSet(self):
    s = set()
    if self.type == 'pointer' and isinstance(self.ChildSpec, StructSpec):
        s.add(self.ChildSpec.type)
    elif self.type == 'virtual':
        s.add("::rage::parVirtualReadData");
    return s | self.ChildSpec.BuildForwardRefSet()

@AddMethod(MapSpec)
def BuildForwardRefSet(self):
    return self.ChildSpec.BuildForwardRefSet() | self.KeySpec.BuildForwardRefSet()
    
#####################################
## Structures and Members
#####################################

MaxTypeLen = 0

@AddMethod(GenericSpec)
def WriteCDefnHeader(self):
    pass

@AddMethod(StructDefSpec)
def WriteCDefnHeader(self):
    self.SetStructdefErrorInfo()
    global MaxTypeLen
    if self.simple == "true":
        baseString = ""
    else:
        baseString = ": public ::rage::datBase"
    if self.base:
        baseString = " : public " + self.base
        
    self.WriteString('''
        
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/// class %(CppType)s
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
''')

    # add namespace decls
    nsToks = self.CppType.split("::")
    for i in nsToks[:-1]:
        self.WriteString('\nnamespace %s {' % i)
        GenericSpec.Depth += 1

    # I could just add #ifs everywhere, but this keeps the header files prettier
    if self.cond != "1":
        self.WriteString('''
#if %(cond)s
''')    
    WriteStartPoundIfPlatform(self)

    self.WriteString('''
    
class %(:localName)s %(:baseString)s
{''', baseString=baseString, localName=nsToks[-1])

    if self.constructible == "false":
        self.WriteString('''
protected:
    %(:localName)s();                 // Protected default constructor
public:''', localName=nsToks[-1])
    else:
        self.WriteString('''
public:
    %(:localName)s();                 // Default constructor''', localName=nsToks[-1])
    
    if self.simple == "true":
        self.WriteString('''
    ~%(:localName)s();
    ''', localName=nsToks[-1])
    else:
        self.WriteString('''
    virtual ~%(:localName)s();        // Virtual destructor
    ''', localName=nsToks[-1])
        
    if self.rsc == "true":
        self.WriteString('''
    %(:localName)s(::rage::datResource& rsc);
    DECLARE_PLACE(%(:localName)s);
    ''', localName=nsToks[-1])
        
    MaxTypeLen = 0
    self.ForEachMember(lambda x: x.MeasureTypeLength())
    self.ForEachMember_IfPlatform(lambda x: x.WriteCDefnHeader())
    
    for x in self.Children:
        if isinstance(x, HInsertSpec):
            x.WriteCDefnHeader()
    
    parsable = "PAR_PARSABLE"
    if self.simple == "true":
        parsable = "PAR_SIMPLE_PARSABLE"
    
    self.WriteString('''
    
    %(:parsable)s;
};
''', parsable=parsable)

    WriteEndPoundIfPlatform(self)

    # I could just add #ifs everywhere, but this keeps the header files prettier
    if self.cond != "1":
        self.WriteString('''
#endif // %(cond)s
''')

    for i in range(len(nsToks)-1, 0, -1):
        GenericSpec.Depth -= 1
        self.WriteString("\n} // namespace %s" % nsToks[i-1])
        
@AddMethod(MemberSpec)
def MeasureTypeLength(self):
    global MaxTypeLen
    (preType, postType) = self.GetSplitCType(useNaturalEnumType=True)
    l = len(preType)
    if l > MaxTypeLen:
        MaxTypeLen = l
        
@AddMethod(MemberSpec)
def WriteCDefnHeader(self):
    global MaxTypeLen
    (preType, postType) = self.GetSplitCType(useNaturalEnumType=True)
    desc = ""
    if self.description != "":
        desc = "\t\t// " + self.description
    self.WriteString('''
    %(:cType)s''' + " " * (MaxTypeLen - len(preType)) + '''        %(CppName)s%(:postType)s;%(:desc)s''', cType=preType, postType=postType, desc=desc);
    
@AddMethod(ArraySpec)
def WriteCDefnHeader(self):
    global MaxTypeLen
    if self.type == 'virtual':
        if not self.onVirtualRead.startswith("::"):
            self.WriteString('''
    bool %(onVirtualRead)s(void* data, ::rage::parVirtualReadData& cbData); // To be implemented in some other .cpp file''')
        return
    super(ArraySpec, self).WriteCDefnHeader() # call the base class 
    self.CheckWriteSizeVar(self.CppName)

@AddMethod(GenericSpec)
def CheckWriteSizeVar(self, codeName):
    pass
    
@AddMethod(ArraySpec)
def CheckWriteSizeVar(self, codeName):
    if self.type == 'pointer' and self.sizeVar != None:
        # need to declare the size variable too
        self.WriteString('''
    ::rage::u32''' + " " * (MaxTypeLen - len("::rage::u32")) + '''        %(sizeVar)s; // Index variable for the %(:cVarName)s array''', cVarName=codeName);
    self.ChildSpec.CheckWriteSizeVar(codeName)
    
@AddMethod(HInsertSpec)
def WriteCDefnHeader(self):
    self.WriteString('''
    
    ///////// BEGIN HINSERT CODE
%(:userData)s
    ///////// END HINSERT CODE''', userData=self.TextData.strip('\r\n'))
    
######################################
## Enums
######################################

@AddMethod(EnumDefSpec)
def WriteCDefnHeader(self):
    self.WriteString('''

/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/// enum %(CppType)s
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
''')
    WriteStartPoundIfPlatform(self)
    nsToks = self.CppType.split("::")
    for i in nsToks[:-1]:
        self.WriteString('\nnamespace %s {' % i)
        GenericSpec.Depth += 1
    self.WriteString('''
enum %(:localType)s {''', localType=nsToks[-1])
    maxNameLen = 0
    hasMinMaxValues = True
    maxValueLen = 0
    maxValue = 0
    minValue = 0
    for i in self.Children:
        if len(i.name) > maxNameLen:
            maxNameLen = len(i.name)
        try:
            ii = int(i.value)
            maxValue = max(ii, maxValue)
            minValue = min(ii, minValue)
        except ValueError:
            # couldn't convert i.value to an int, must be a string, don't compute a max value
            hasMinMaxValues = False
        if len(str(i.value)) > maxValueLen:
            maxValueLen = len(str(i.value))
            
    for i in self.Children:
        i.WriteCDefnString(maxNameLen, maxValueLen)
    self.WriteString('''
};

///////// Special values
const int %(:localType)s_NUM_ENUMS = %(:numEnums)d;
''', numEnums=len(self.Children), localType=nsToks[-1])

    if hasMinMaxValues:
        self.WriteString('''
const int %(:localType)s_MAX_VALUE = %(:maxValue)d;
const int %(:localType)s_MIN_VALUE = %(:minValue)d;
''', localType=nsToks[-1], maxValue=maxValue, minValue=minValue);
    else:
        self.WriteString('''
// (can't compute %(:localType)s_MAX_VALUE or _MIN_VALUE because of the named enum values)
''', localType=nsToks[-1])

    # output typedef for bitset use (union of bools and indexable bitset)
    if self.generate and "bitset" in self.generate:
        self.WriteString('''\n
typedef rage::atFixedBitSet<%(:localType)s_MAX_VALUE + 1, rage::u32> %(:localType)sBitSetData;
typedef union _%(:localType)sBitSet {
private:
private:
    struct {\n''', localType=nsToks[-1])

        self.WriteString("#if !__BE\n")
        for i in self.Children:
            self.WriteString("       bool %s : 1;\n" % (i.name))
        self.WriteString("#else // __BE\n")
        totalEnums = len(self.Children)
        totalBits = (totalEnums + 31) & 0x7fffffe0
        for i in range(totalBits):
            endianIndex = (i&0x7fffffe0) | ((i&0x1f)^0x1f)
            if endianIndex < totalEnums:
                self.WriteString("       bool %s : 1; // %d\n" % (self.Children[endianIndex].name, endianIndex))
            else:
                self.WriteString("       bool _pad%d : 1;\n" % (endianIndex))
        self.WriteString("#endif // __BE\n")

        self.WriteString('''    } bools;
    rage::u32 storage[(((%(:localType)s_MAX_VALUE + 1)+(31))>>5)];
public:
    %(:localType)sBitSetData & BitSet()              { return *reinterpret_cast<%(:localType)sBitSetData *>(&storage[0]); }
    const %(:localType)sBitSetData & BitSet() const  { return *reinterpret_cast<const %(:localType)sBitSetData *>(&storage[0]); }

    _%(:localType)sBitSet() { 
#if !__SPU
        if (!::rage::datResource_sm_Current) 
#endif
            BitSet().Reset();
    }
    _%(:localType)sBitSet(::rage::datResource&)					{}
    _%(:localType)sBitSet(const _%(:localType)sBitSet & s)		{ BitSet() = s.BitSet();  }
    _%(:localType)sBitSet& operator=(const _%(:localType)sBitSet& s)	{ BitSet() = s.BitSet(); return *this; }
} %(:localType)sBitSet;''', localType=nsToks[-1])

    for i in range(len(nsToks)-1, 0, -1):
        GenericSpec.Depth -= 1
        self.WriteString("\n} // namespace %s" % (nsToks[i-1]))
    WriteEndPoundIfPlatform(self)

@AddMethod(EnumValSpec)
def WriteCDefnString(self, maxNameLen, maxValueLen):
    if self.description:
        self.WriteString("\n\t%(name)s" + " " * (maxNameLen - len(self.name)) + " = %(value)s, " + " " * (maxValueLen - len(str(self.value))) + " // %(description)s")
    else:
        self.WriteString("\n\t%(name)s" + " " * (maxNameLen - len(self.name)) + " = %(value)s, ")
    
    