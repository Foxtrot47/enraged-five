from basetype import *

class IntSpec(DataMemberSpec):
    StdMin = "INT_MIN/2"
    StdMax = "INT_MAX/2"
    
    def __init__(self, *args, **kwargs):
        self.step = None
        self.init = None
        super(IntSpec, self).__init__(*args, **kwargs)

    def CheckAttributes(self):
        self.WikiPage = "Int_PSC_tag"
        DataMemberSpec.CheckAttributes(self)
        self.OptionalAttribute("init", "0")
        self.OptionalAttribute("step", "1")


class U32Spec(IntSpec):
    StdMin = "0"
    StdMax = "UINT_MAX"

class SizeTSpec(IntSpec):
    StdMin = "0"
    StdMax = "SIZE_MAX"
    
class PtrdiffTSpec(IntSpec):
	pass
   
class Color32Spec(U32Spec):
    pass

class CharSpec(IntSpec):
    StdMin = "CHAR_MIN"
    StdMax = "CHAR_MAX"

class U8Spec(IntSpec):
    StdMin = "0"
    StdMax = "UCHAR_MAX"

class ShortSpec(IntSpec):
    StdMin = "SHRT_MIN"
    StdMax = "SHRT_MAX"

class U16Spec(IntSpec):
    StdMin = "0"
    StdMax = "USHRT_MAX"

class S64Spec(IntSpec):
    StdMin = "LONG_MIN"
    StdMax = "LONG_MAX"
    
class U64Spec(IntSpec):
    StdMin = "0"
    StdMax = "ULONG_MAX"
    