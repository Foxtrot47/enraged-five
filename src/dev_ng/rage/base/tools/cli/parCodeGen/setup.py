from distutils.core import setup
import py2exe
import os

import sys
print sys.path

setup(
	console=['parCodeGen.py'],
	options={
		"py2exe": {
			"excludes":["email", "email.Utils", "email.utils", "_scproxy"],
            "dll_excludes": [ "mswsock.dll", "powrprof.dll" ], # http://stackoverflow.com/questions/1979486/py2exe-win32api-pyc-importerror-dll-load-failed
            "dist_dir":os.getenv("RS_TOOLSROOT") + "\\bin\\coding\\python"
			}
		},
    zipfile="parCodeGen.zip"
)
