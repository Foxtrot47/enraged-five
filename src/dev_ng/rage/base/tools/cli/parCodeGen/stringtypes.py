import sys, os.path
from xml.dom.minidom import *
from basetype import *

class StringSpec(MemberSpec):
    def __init__(self, *args, **kwargs):
        self.type = None
        self.onWidgetChanged = None
        self.size = None
        self.init = None
        super(StringSpec, self).__init__(*args, **kwargs)

    
    Namespaces = {} # Map from int to {HashStringName, HashValueName} dict
    CoreStringTypes = ["member", "pointer", "ConstString", "atString", "wide_member", "wide_pointer", "atWideString"]
    HashTypes = ["atHashString", "atNonFinalHashString", "atHashValue", "atHashWithStringDev", "atHashWithStringBank", "atHashWithStringNotFinal", "atFinalHashString", "atPartialHashValue"]
    AllStringTypes = [] # built in LoadHashTypes
    
    @staticmethod
    def LoadHashTypes(): # no 'self', this is a static method
        AllStringTypes = StringSpec.CoreStringTypes + StringSpec.HashTypes
        # the namespaces.txt file gives the list of hashes
        # Check two spots for it. The place the script was launched from or RS_TOOLSROOT\bin\coding\python (these should be the same 
        # except when running the uncompiled parCodeGen.py)
        fname = os.path.join(os.path.dirname(sys.argv[0]), "namespaces.txt")
        if not os.path.exists(fname):
            fname = os.path.join(os.path.expandvars("${RS_TOOLSROOT}\\bin\\coding\\python"), "namespaces.txt")
        try:
            f = file(fname)
        except:
            CodeGenWarning("Couldn't find or open file '%s'" % (fname))
        else:
            for line in f:
                ls = line.strip()
                if ls == "" or ls.startswith("#"):
                    continue
                (nsIndex, hashStringName, hashValueName) = ls.split()
                StringSpec.Namespaces[int(nsIndex)] = {'HashStringName': hashStringName, 'HashValueName': hashValueName}
                AllStringTypes.append(hashStringName)
                AllStringTypes.append(hashValueName)
    
    def CheckAttributes(self):
        self.WikiPage = "String_PSC_tag"
        MemberSpec.CheckAttributes(self)
            
        self.RequiredAttribute("type", StringSpec.AllStringTypes, safeVal="pointer")
        if self.type == "atHashString" or self.type == "atNonFinalHashString":
            self.type = "atHashWithStringNotFinal"
        self.OptionalAttribute("onWidgetChanged")
        if (self.type == "member" or self.type == "wide_member"):
            self.RequiredAttribute("size")
        else:
            self.OptionalAttribute("size", "0")
        if self.size:
            ValidateIntLiteralOrKnownConst(self.size)
        self.OptionalAttribute("init", "")
        
        self.NamespaceIndex = None
        self.NamespaceType = None
        for (nsIndex, ns) in StringSpec.Namespaces.iteritems():
            if self.type == ns['HashStringName']:
                self.NamespaceIndex = nsIndex
                self.NamespaceType = "string"
            elif self.type == ns['HashValueName']:
                self.NamespaceIndex = nsIndex
                self.NamespaceType = "value"
                
StringSpec.LoadHashTypes()
