# C++ Comment Extractor/Transformer
# removes non-comments from a file. Extracts certain XML tags from within comments
# also looks for PAR_STRINGKEY(x) and turns it into <stringkey>x</stringkey>
#
#   // Stuff
#   // goes here
#
#   /* <structdef type="foo">
#      </structdef>
#   */
#
# Becomes:
#
#
#  <structdef type="foo">
#  </structdef>
#

import sys
import re
import os
import StringIO

import atHash

StringKeyRE = re.compile(r"PAR_STRINGKEY\s*\(\s*(\w+)\s*\)")
AtStringHashStartRE = re.compile(r'''
    ATSTRINGHASH \s* \( \s*''', re.VERBOSE)
    
AtStringHashRE = re.compile(r'''
    ATSTRINGHASH  \s* \(  \s*  # ATSTRINGHASH(
    "(([^"\\]|\\.)*)"           # matches a C string (groups 1, 2)
    \s* , \s*                  # ,
    (-?(0x|0X)? [0-9a-fA-F]+)u? # matches a numeric literal (groups 3,4)
    \s* \)                     # end of function
    ''', re.VERBOSE)

def PrintError(filename, linenum, message):
    sys.stderr.write("%s (%d): Error: %s\n" % (filename, linenum, message))
    
def extractComments(chars, filename, onlyFromMarkedBlocks=False):
    global StringKeyRE
    numChars = len(chars)

    out = StringIO.StringIO()

    inBlockComment = False
    i = 1
    linenum = 1
    while i < numChars:
        if inBlockComment:
            if chars[i] == "*" and chars[i+1] == "/":
                i += 1
                inBlockComment = False
            else:
                if chars[i] == '\n':
                    linenum += 1
                out.write(chars[i])
        else:
            if chars[i-1] == "/" and chars[i] == "/" and not onlyFromMarkedBlocks:
                i += 1
                while i < numChars and chars[i] != "\n":
                    out.write(chars[i])
                    i += 1
                out.write("\n")
                linenum += 1
            elif chars[i-1] == "/" and chars[i] == "*":
                if not onlyFromMarkedBlocks or chars[i+1:i+4] == "XML":
                    inBlockComment = True
            elif chars[i] == "\n":
                out.write("\n")
                linenum += 1
            elif chars[i] == 'P':
                match = StringKeyRE.match(chars, i)
                if match:
                    m = match.group(1)
                    out.write("<stringkey name='PAR_sk_%s' string='%s'/>" % (m, m))
            elif chars[i] == 'A':
                match = AtStringHashStartRE.match(chars, i)
                if match:
                    match = AtStringHashRE.match(chars, i)
                    if match:
                        string = match.group(1)
                        value = int(match.group(3), 0) # base = 0, allow hex or decimal
                        trueValue = atStringHash(string)
                        # just validate this one, don't write anything
                        if trueValue != value:
                            PrintError(filename, linenum, "Validation failed for ATSTRINGHASH(\"%s\", 0x%x). Value should be 0x%x" % (string, value, trueValue))
                    else:
                        pass
                        # PrintError(filename, linenum, "Couldn't parse ATSTRINGHASH macro args: %s" % chars[i:i+30])
        i += 1
        
    outChars = out.getvalue()
    out.close()

    return outChars

# remove everything but data within certain xml blocks
def extractXMLMetadata(chars):
    out = StringIO.StringIO()

    # Format: start tag name, end tag name, can be leaf
    parserXmlBlocks = [
        ["<structdef",   "</structdef>",        False],
        ["<buildnumber", "</buildnumber>",      True],
        ["<enumdef",     "</enumdef>",          False],
        ["<autoregister", "</autoregister>",    True],
        ["<stringkey", "</stringkey>", True],
        ["<import", "</import>", True],
        ["<const name=", "</const>", True],
        ["<const value=", "</const>", True],
        ]

    strLen = len(chars)
    i = 0
    inStructure = False
    inBuildnumber = False
    inEnumdef = False

    inWhich = -1    
    while i < strLen:
        if inWhich >= 0:
            out.write(chars[i])
            if chars[i] == ">":
                if chars.endswith(parserXmlBlocks[inWhich][1], 0, i+1):
                    inWhich = -1
                if parserXmlBlocks[inWhich][2] and chars.endswith("/>", 0, i+1):
                    inWhich = -1
        else:
            if chars[i] == "<":
                for (which, block) in enumerate(parserXmlBlocks):
                    if chars.startswith(block[0], i):
                        inWhich = which;
                        out.write(chars[i])
                        break
            if chars[i] == "\n":
                out.write("\n")
        i += 1

    outChars = out.getvalue()
    out.close()

    return outChars        

def extractAll(chars, filename, onlyFromMarkedBlocks=False):
    return extractXMLMetadata(extractComments(chars, filename, onlyFromMarkedBlocks))

def main(args):
    for i in args:
        f = file(i, "r")

        chars = f.read()
        f.close()        

        outChars = extractAll(chars, i)

        print outChars

if __name__ == '__main__':
    main(sys.argv[1:])
