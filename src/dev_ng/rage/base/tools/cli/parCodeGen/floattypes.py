from basetype import *

class FloatSpec(DataMemberSpec):
    StdMin = "-FLT_MAX/1000.0f"
    StdMax = "+FLT_MAX/1000.0f"
    
    def __init__(self, *args, **kwargs):
        self.step = None
        self.init = None
        self.highPrecision = None
        super(FloatSpec, self).__init__(*args, **kwargs)
        
    def CheckAttributes(self):
        self.WikiPage = "Float_PSC_tag"
        DataMemberSpec.CheckAttributes(self)
        self.OptionalAttribute("step", "0.1f")
        self.OptionalAttribute("init", "0.0f")
        self.OptionalAttribute("highPrecision", "false")
    
        

class AngleSpec(FloatSpec):
    StdMin = "0.0f"
    StdMax = "2.0f * PI"

    def CheckAttributes(self):
        FloatSpec.CheckAttributes(self)
        self.OptionalAttribute("step", "0.01f")

class ScalarVSpec(FloatSpec):
    pass

class Float16Spec(FloatSpec):
    pass

class DoubleSpec(FloatSpec):
    pass
   