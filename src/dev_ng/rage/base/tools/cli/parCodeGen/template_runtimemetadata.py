from __future__ import with_statement

import math

from templatesupport import *
from template_cpp_support import *
from basetype import *
from structtype import *
from inttypes import *
from floattypes import *
from vectortypes import *
from arraytype import *
from stringtypes import *
from enumtype import *
from matrixtypes import *
from datatypes import *

import atHash

########################################################################################################
########################################################################################################
##
## The main write functions.
##
########################################################################################################
########################################################################################################

####################################################
## Top level document
####################################################

@AddMethod(DocumentSpec)
def WriteCCode(self, fileName, sourceFileName):
    GenericSpec.Depth = 0
    self.WriteCommonCHeader(fileName, sourceFileName, useIncludeGuard=True, usage="and should be #included near the top of the cpp file.")
    self.WriteString('''
#if !__SPU

// RAGE_QA: NO_NAMESPACE_RAGE
// DOM-IGNORE-BEGIN

''')


    includes = set()
    includes.add("atl/creator.h")
    includes.add("atl/delegate.h")
    includes.add("parser/codegenutils.h")
    includes.add("<limits.h>")
    includes.add("<float.h>")
    
    for i in self.Children:
        i.AddIncludes(includes)
        
    includeList = list(includes)
    includeList = sorted(includeList, key=lambda x: x.replace('<', '{')) # swap < with { for comparison because { is higher in ascii - so system includes show up at the end of the list as per rage standard
    
    for i in includeList:
        if i[0] == '<':
            self.WriteString('#include %s\n' % i)
        else:
            self.WriteString('#include "%s"\n' % i)

    self.WriteString('''
namespace rage
{
    struct parMemberSimpleData;
}

#if __XENON
#pragma optimize("g", on)
#endif

#ifdef __SNC__
// Add limits to inlining to reduce code size for this run-once code.
#pragma control %%push autoinlinesize=32
#pragma control %%push inlinesize=32
#pragma control %%push inlinemaxsize=32
#endif

#ifndef SIZE_MAX
#define SIZE_MAX UINT_MAX
#endif

''')
    for i in self.Children:
        i.WriteCCode()

    self.WriteAutoregistrationCode()
    self.WriteStringkeys()
    
    self.WriteString('''

#ifdef __SNC__
#pragma control %%pop inlinemaxsize
#pragma control %%pop inlinesize
#pragma control %%pop autoinlinesize
#endif
        
#if __XENON
#pragma optimize("", on)
#endif
        
// DOM-IGNORE-END

#endif // !__SPU

#endif
''')


@AddMethod(GenericSpec)
def WriteCCode(self):
    pass

    
####################################################
## Find necessary #includes
####################################################

@AddMethod(GenericSpec)
def AddIncludes(self, includeSet):
    pass
    
@AddMethod(EnumDefSpec)
def AddIncludes(self, includeSet):
    includeSet.add("parser/memberenumdata.h")
    
@AddMethod(StructDefSpec)
def AddIncludes(self, includeSet):
    self.SetStructdefErrorInfo()
    self.ForEachMember(lambda x: includeSet.update(x.AddMemberIncludes()))

CreateSimpleGetterFunctions("AddMemberIncludes", {
    BoolSpec:    set(("parser/memberdata.h", "parser/membersimpledata.h")),
    IntSpec:    set(("parser/memberdata.h", "parser/membersimpledata.h")),
    FloatSpec:  set(("parser/memberdata.h", "parser/membersimpledata.h")),
    Float16Spec:  set(("parser/memberdata.h", "parser/membersimpledata.h", "math/float16.h")),
    VectorSpec: set(("parser/memberdata.h", "parser/membervectordata.h")),
    MatrixSpec: set(("parser/memberdata.h", "parser/membermatrixdata.h")),
    StructSpec: set(("parser/memberdata.h", "parser/memberstructdata.h")),
    EnumSpec:   set(("parser/memberdata.h", "parser/memberenumdata.h")),
    StringSpec: set(("parser/memberdata.h", "parser/memberstringdata.h")),
})

@AddMethod(ArraySpec)
def AddMemberIncludes(self):
    kidIncls = self.ChildSpec.AddMemberIncludes()
    return kidIncls | set(("parser/memberdata.h", "parser/memberarraydata.h"))
    
@AddMethod(MapSpec)
def AddMemberIncludes(self):
    kidIncls = self.ChildSpec.AddMemberIncludes()
    kidIncls = kidIncls | self.KeySpec.AddMemberIncludes()
    return kidIncls | set(("parser/memberdata.h", "parser/membermapdata.h"))
    
####################################################
## Each structure
####################################################
@AddMethod(StructDefSpec)    
def WriteCCode(self):
    self.SetStructdefErrorInfo()
    
    self.WriteString('''
////////////////////////////////////////////////////////////////////
/// %(ParserName)s
////////////////////////////////////////////////////////////////////
#if %(cond)s
''')
    WriteStartPoundIfPlatform(self)
    # Check to see if there was a <struct> in the generated code. If so, compare build numbers.
    if self.version != '0':
        if self.GenStruct == None:
            self.BuildNumber = 0
        elif self.DomNode.toxml() == self.GenStruct.toxml():
            # build number is unchanged
            self.BuildNumber = self.GenBuildNum
        else:
            # if major version number changed, then reset build number to 0
            if (self.DomNode.getAttribute("version") != self.GenStruct.getAttribute("version")):
                self.BuildNumber = 0
                print "Resetting the build number of", self.type, "to 0"
            else:
                # bump the build number
                self.BuildNumber = self.GenBuildNum + 1
                print "Bumping the build number on", self.type, "to", self.BuildNumber

        self.WriteString('''
/*XML
%(:xmlCode)s
<buildnumber type="%(CppType)s" value="%(:buildNum)d"/>
*/
''', xmlCode=self.DomNode.toxml(), buildNum=self.BuildNumber)

    self.WriteString('''
CompileTimeAssert(sizeof(%(CppType)s) != 0xFFFFFFFF); // If this fails - there was a <structdef type="%(CppType)s"> but no class %(CppType)s
''');

    for x in self.IterMembers():
        with WritePoundIfPlatform(x):
            x.WriteMemberData()
        x.WriteString("\n")

    templateDecl = ""
    if (self.template == 'true'):
        templateDecl = "template<>\n"

    self.WriteString('''
%(:templateDecl)s
void* const parser_%(MangledCppType)s__Members[] = { // If there is an error here: Does the class %(CppType)s have the PAR_PARSABLE or PAR_SIMPLE_PARSABLE macro?
'''
    , templateDecl=templateDecl)

    self.ForEachMember_IfPlatform(lambda x: x.WriteString("\n\t&%(:parMemName)s,", parMemName=x.FindParMemberName()))
   
    self.WriteString('''
    NULL
};
    
// If there is a compile error on a line below, <structdef type="%(CppType)s"> had a member that doesn't exist in the C++
%(:templateDecl)s
::rage::u32 const %(CppType)s::parser_MemberOffsets[] = {''', templateDecl=templateDecl);
    for x in self.IterMembers():
        with WritePoundIfPlatform(x):
            x.WriteString("\n\t")
            x.WriteAddress(self.CppType)
            x.WriteString(",")
    self.WriteString('''
    0
};

''')

    useNames = "PARSER_ALL_METADATA_HAS_NAMES"
    if self.preserveNames == "true":
        useNames = "1"
    self.WriteString('''
#if %(:useNames)s
const char* const parser_%(MangledCppType)s__MemberNames[] = {''', useNames=useNames);
    self.ForEachMember_IfPlatform(lambda x: x.WriteString('''\n\t"%(ParserName)s",'''))

    # now need to gather additional strings
    extraNames = set()
    self.ForEachMember(lambda x: x.FindExtraMemberNames(extraNames))
    if len(extraNames) > 0:
        self.WriteString('''
    // Extra names (these don't correspond to entries in the tables above - they're for nested members or other strings we need)''')
        for name in extraNames:
            self.WriteString('''\n\t"%s",''' % name)
    self.WriteString('''
    NULL,
};
const char* const parser_%(MangledCppType)s__StructureName = "%(ParserName)s";
#else
const char* const * const parser_%(MangledCppType)s__MemberNames = NULL;
const char* const parser_%(MangledCppType)s__StructureName = NULL;
#endif

%(:templateDecl)s
::rage::parStructureStaticData %(CppType)s::parser_Data = {
    0x%(NameHash)08x, // "%(ParserName)s"
    parser_%(MangledCppType)s__StructureName,
    NULL, // m_Structure
    parser_%(MangledCppType)s__Members,
    parser_MemberOffsets,
    parser_%(MangledCppType)s__MemberNames,
    %(CppType)s::parser_IsInheritable(),
    %(preserveNames)s,
};

%(:templateDecl)s
void %(CppType)s::parser_Register() {
    ::rage::u32 majorVersion = %(version)s;
    if (%(CppType)s::parser_Data.m_Structure)
    {
#if __DEV
        ::rage::parCguWarnOnMultipleRegistration(%(CppType)s::parser_Data);
#endif
        return;
    }
''', templateDecl=templateDecl)
    structureFactory = "parCguStructure::CreateStructure"
    if self.usedi == "true":
        structureFactory = "parCguStructure::CreateDiStructure"

    self.WriteString('''
    ::rage::parStructure* newStructure = ::rage::%(:strFactory)s(sizeof(%(CppType)s));
    %(CppType)s::parser_Data.m_Structure = newStructure;''', strFactory=structureFactory)
    
    if self.constructible != "false":
        self.WriteString('''
    ::rage::parCguStructure::FactoryFunction factoryFn(& ::rage::atPtrCreator< void, %(CppType)s > );
    ::rage::parCguStructure::PlacementFunction placementFn(& ::rage::atPlacePtrCreator< void*, %(CppType)s > );
    ::rage::parCguStructure::DestructorFunction destructorFn(& ::rage::parCguDestructor< %(CppType)s > );
    newStructure = ::rage::parCguStructure::SetFactories(newStructure, factoryFn, placementFn, destructorFn);
''')
        
    self.WriteString('''
    newStructure = ::rage::parCguStructure::SetGetStructureCB(newStructure, &::rage::parCguGetStructureCB< %(CppType)s >);
''')

    # count up the delegates, to see if we need to write the del. block and to see if we need to create an instance pointer
    delegateList = ['onPreLoad', 'onPostLoad', 'onPreSave', 'onPostSave', 'onPreSet', 'onPostSet', 'onPreAddWidgets', 'onPostAddWidgets']
    delegateCount = 0;
    for fn in delegateList:
        if getattr(self, fn):
            delegateCount += 1

    if len(self.Children) > 0 or delegateCount > 0:
        self.WriteString('''

    %(CppType)s* instance; instance = NULL;
''')

    self.ForEachMember_IfPlatform(lambda x: x.WriteSizeCheck("instance->" + x.CppName))
    
    if self.generate and "psochecks" in self.generate:
        lastMember = None
        self.WriteString('''
    // Additional checks for structdefs that go into PSO files''')
        
        startOffset = 4
        hasVptr = True
        if self.simple == "true":
            startOffset = 0
            hasVptr = False
        for i in self.Children:
            if isinstance(i, PadSpec):
                startOffset += int(i.bytes)
            elif isinstance(i, MemberSpec):
                break
        startOffsetWithMaxPadding = ((startOffset + 15) / 16) * 16
    
        for i in self.IterMembers():
            if lastMember:
                with WritePoundIfPlatform(lastMember):
                    with WritePoundIfPlatform(i):
                        self.WriteString('''
    CompileTimeAssert( (''')
                        lastMember.WriteAddress(self.CppType)
                        lastMember.WriteString(") < (")
                        i.WriteAddress(self.CppType)
                        self.WriteString(''') ); // If this fails, it means %s and %s aren't in the right order in the <structdef>''' % (lastMember.CppName, i.CppName))
            else:
                # first member - make sure if there is no base class it starts at 0x0 or 0x4
                if not self.base:
                    with WritePoundIfPlatform(i):
                        if hasVptr:
                            self.WriteString('''
    CompileTimeAssert( (''')
                            i.WriteAddress(self.CppType)
                            self.WriteString(''') >= 0x4); // If this fails, you might not have any virtual functions. If you don't, add simple="true" to your <structdef>''')
                            self.WriteString('''
    CompileTimeAssert( (''')
                            i.WriteAddress(self.CppType)
                            self.WriteString(''') <= 0x%(:paddedOffset)x); // If this fails, what you claim is the first member var actually isn't.''', paddedOffset=startOffsetWithMaxPadding)
                        else:
                            self.WriteString('''
    CompileTimeAssert( (''')
                            i.WriteAddress(self.CppType)
                            self.WriteString(''') == 0x%(:startOffset)x ); // If this fails, what you claim is the first member var actually isn't.''', startOffset=startOffset)
            lastMember = i
            
        self.WriteString('''
    // End additional PSO checks
''')
    
    self.ForEachMember_IfPlatform(lambda x: x.ResolvePointers())
    self.ForEachMember_IfPlatform(lambda x: x.AddMemberCallbacks())
    self.ForEachMember_IfPlatform(lambda x: x.WriteExtraAttributes())

    if self.base:
        self.WriteString('''

    // If this fails, base class "%(base)s" isn't inheritable (maybe it uses PAR_SIMPLE_PARSABLE?)
    parAssertf(%(base)s::parser_IsInheritable() == %(CppType)s::parser_IsInheritable(), "Class %%s inherits from %%s - either both must use PAR_PARSABLE, or both must use PAR_SIMPLE_PARSABLE", "%(CppType)s", "%(base)s");

    const %(CppType)s* offsetInstance = reinterpret_cast< const %(CppType)s* >(0x10); // can't be null or compiler will optimize out the next line
    const int baseClassOffset = static_cast<int>(reinterpret_cast<intptr_t>(static_cast< const %(base)s* >(offsetInstance)) - 0x10);
    newStructure = ::rage::parCguStructure::SetBaseClass(newStructure, ::rage::parCguGetRequiredStructure< %(base)s >(__DEV ? "%(base)s" : NULL, __DEV ? "%(CppType)s" : NULL), baseClassOffset);

    majorVersion += ::rage::parCguStructure::GetMajorVersion(newStructure);
''')
    else:
        pass
        #self.WriteString('''\n    newStructure = ::rage::parCguStructure::SetBaseClass(newStructure, NULL, 0);''')

    if delegateCount > 0:
        self.WriteString('''\n    newStructure = ::rage::parCguStructure::BeginAddingInitialDelegates(newStructure, %(:delCount)d);''', delCount=delegateCount)

        self.AddCallbackSetter('onPreLoad', 'PreLoad', 'void', '::rage::parTreeNode*')
        self.AddCallbackSetter('onPostLoad', 'PostLoad', 'void', '')
        self.AddCallbackSetter('onPreSave', 'PreSave', 'void', '')
        self.AddCallbackSetter('onPostSave', 'PostSave', 'void', '::rage::parTreeNode*')
        self.AddCallbackSetter('onPreSet', 'PreSet', 'void', '::rage::parTreeNode*, ::rage::parMember*, void*')
        self.AddCallbackSetter('onPostSet', 'PostSet', 'void', '::rage::parTreeNode*, ::rage::parMember*, void*')
        self.WriteString("\n#if __BANK")
        self.AddCallbackSetter('onPreAddWidgets', 'PreAddWidget', 'void', '::rage::bkBank&')
        self.AddCallbackSetter('onPostAddWidgets', 'PostAddWidget', 'void', '::rage::bkBank&')
        self.WriteString("\n#endif")

        self.WriteString('''\n    newStructure = ::rage::parCguStructure::EndAddingInitialDelegates(newStructure);''')                
    
    self.WriteString('''
    newStructure = ::rage::parCguStructure::BuildStructureFromStaticData(newStructure, %(CppType)s::parser_Data, majorVersion, %(:buildNumber)d);
''', buildNumber=self.BuildNumber)
    
    # add extra metadata
    if len(self.ExtraAttrs) > 0:
        self.WriteString('''
    newStructure = ::rage::parCguStructure::CreateExtraAttributes(newStructure);
''')
        for (key, value) in self.ExtraAttrs.iteritems():
            self.WriteString('''
    newStructure = ::rage::parCguStructure::AddExtraAttribute(newStructure, "%(:key)s", "%(:value)s");''', key=key, value=value)
        self.WriteString('''
    newStructure = ::rage::parCguStructure::SortExtraAttributes(newStructure);
''')
          

    self.WriteString('''        
    ::rage::parCguRegisterStructure(newStructure);
}
''')

# This is WIP resourcing code. Needs to be finished up
    # if self.rsc:
        # self.WriteString('''
# %(CppType)s::%(CppType)s(::rage::parStructure& str)   // Resource constructor for parser-based resource placement
# ''')
        # seperator = ":"
        # if self.base:
            # self.WriteString('''
# : %(base)(str)''')
            # seperator = ","
        # self.WriteString('''
# {
# }
# ''')

    WriteEndPoundIfPlatform(self)
    self.WriteString('''
#endif // %(cond)s
''')

    self.UnsetStructdefErrorInfo()        

@AddMethod(StructDefSpec)
def AddCallbackSetter(self, attr, parStructFn, retType, args):
    self.SetStructdefErrorInfo()
    attrVal = getattr(self, attr)
    if attrVal:
        argList = args.split(',')
        numArgs = len(argList)
        # special case, if the string was ''
        if args == '':
            numArgs = 0
            
        if attrVal[:2] == "::":
            # global static function
            # ::rage::atDelegate<void (::rage::parTreeNode*)>(&MyOnLoad)
            self.WriteString('''
newStructure = ::rage::parCguStructure::AddDelegate(newStructure, "%(:parStructFn)s", ::rage::atDelegate< %(:retType)s (%(:args)s)>(&%(:funcName)s));
    ''', parStructFn=parStructFn, retType=retType, args=args, funcName=attrVal)
        else:
            # member of the class we're operating on
            # ::rage::atDelegate<void (::rage::parTreeNode*)>(instance, &MyType::MyOnLoad)
            self.WriteString('''
newStructure = ::rage::parCguStructure::AddDelegate(newStructure, "%(:parStructFn)s",  ::rage::atDelegate< %(:retType)s (%(:args)s)>(instance, &%(CppType)s::%(:funcName)s));
    ''', parStructFn=parStructFn, retType=retType, args=args, funcName=attrVal)

####################################################
## All the stringkeys in the document need to be in one place
####################################################
@AddMethod(DocumentSpec)
def WriteStringkeys(self):
    # first make a map of string -> stringkey
    strkeys = {}
    for node in self.Children:
        if isinstance(node, StringkeySpec):
            strkeys[node.string] = node
    # now check that no two strings hash to the same value
    keyset = {}
    for (key, node) in strkeys.iteritems():
        if node.Key in keyset:
            CodeGenWarning("Two PAR_STRINGKEYs have the same key value: \"%s\" and \"%s\"" % (keyset[node.Key].string, node.string))
        keyset[node.Key] = node
        
    if len(strkeys) == 0:
        return
    self.WriteString('''
    
///////////////////////////////////////////
// String Keys
///////////////////////////////////////////''')

    for (key, node) in strkeys.iteritems():
        self.WriteString('''
const int %(:name)s = 0x%(:key)x;''',
                         key=node.Key,
                         name=node.Name)                        
                            

####################################################
## All the autoregistration blocks in the document need to be in one place
####################################################
@AddMethod(DocumentSpec)
def WriteAutoregistrationCode(self):
    classesToRegister = []

    for node in self.Children:
        if isinstance(node, StructDefSpec):
            if node.autoregister != "false":
                classesToRegister.append(node); 
                
    if len(classesToRegister) == 0:
        return
    self.WriteString('''
///////////////////////////////////////////
// Autoregistration
///////////////////////////////////////////''')

    counter = 0        
    for node in classesToRegister:
        counter += 1
        with WritePoundIfPlatform(node):
            if isinstance(node, StructDefSpec):
                condition = node.cond
                if condition != "1":
                    self.WriteString('''
    #if %(:cond)s''', cond=condition)
                self.WriteString('''
    ::rage::parCguAutoRegistrationNode autoreg_%(:name)s(&%(:type)s::parser_Register);''',
                             type=node.CppType,
                             name=node.MangledCppType)
                if condition != "1":
                    self.WriteString('''
        #endif // %(:cond)s''', cond=condition)
    # reset the autoreg class for the next file
    AutoregSpec.ClassesToRegister = {}
    AutoregSpec.RegisterAllInFile = False
    
####################################################
## Write checks to make sure the constant definitions are correct
####################################################
@AddMethod(ConstSpec)
def WriteCCode(self):
    self.WriteString('''
CompileTimeAssert(%(name)s == %(value)s); // If this fails, make sure that the <const name="%(name)s" ...> has the correct value''');

####################################################
## Special purpose enum/bitset code
####################################################

@AddMethod(EnumSpec)
def GetEnumDataName(self):
    if self.EnumDefType:
        return "parser_" + MangleCppName(self.EnumDefType) + "_Data"
    return "::rage::parEnumData::Empty"
        
@AddMethod(EnumSpec)
def GetEnumSubtype(self):
    subtypeMap = {
        "8": "SUBTYPE_8BIT",
        "16": "SUBTYPE_16BIT",
        "32": "SUBTYPE_32BIT",
        }

    return "::rage::parMemberEnumSubType::" + subtypeMap[self.size]    

@AddMethod(BitsetSpec)    
def GetEnumSubtype(self):
    subtypeMap = {
        "fixed": "SUBTYPE_32BIT_FIXED",
        "fixed8": "SUBTYPE_8BIT_FIXED",
        "fixed16": "SUBTYPE_16BIT_FIXED",
        "fixed32": "SUBTYPE_32BIT_FIXED",
        "atBitSet": "SUBTYPE_ATBITSET",
        "generated": "SUBTYPE_32BIT_FIXED",
        }

    return "::rage::parMemberBitsetSubType::" + subtypeMap[self.type]   

@AddMethod(EnumDefSpec)
def WriteCCode(self):
    with WritePoundIfPlatform(self):
        useNames = "PARSER_ALL_METADATA_HAS_NAMES"
        preserveNameFlag = ""
        if self.preserveNames == "true":
            useNames = "1"
            preserveNameFlag = "(1 << ::rage::parEnumFlags::ENUM_ALWAYS_HAS_NAMES) |"
        # write a table of strings
        self.WriteString('''
#if %(:useNames)s
const char* parser_%(ParserName)s_Strings[] = {''', useNames=useNames)
        for i in self.Children:
            i.WriteNameString()
        self.WriteString('''
NULL
};
#else
const char* * parser_%(ParserName)s_Strings = NULL;
#endif
''')
        # write a table of values
        self.WriteString('''
::rage::parEnumListEntry parser_%(ParserName)s_Values[] = {''')
        for i in self.Children:
            i.WriteValueString()
        self.WriteString('''
{0, -1}
};
    ''');
    
        # write the object that ties it all together
        self.WriteString('''
const unsigned int parser_%(ParserName)s_Count = %(:numStrings)d;
    
::rage::parEnumData parser_%(ParserName)s_Data = {
    parser_%(ParserName)s_Values,
    parser_%(ParserName)s_Strings,
    %(:numStrings)d,
    (1 << ::rage::parEnumFlags::ENUM_STATIC) |
#if %(:useNames)s
    (1 << ::rage::parEnumFlags::ENUM_HAS_NAMES) |
#endif
    %(:preserveNameFlag)s
    0,
    0x%(NameHash)x // "%(ParserName)s"
};
''', numStrings=len(self.Children), useNames=useNames, preserveNameFlag=preserveNameFlag)   

@AddMethod(EnumValSpec)   
def WriteNameString(self):
    self.WriteString('''\n\t"%(name)s",''')

@AddMethod(EnumValSpec)   
def WriteValueString(self):
    self.WriteString('''\n\t{0x%(:namehash)08x, (int)%(value)s}, // "%(name)s" ''', namehash=atHash.atLiteralStringHash(self.name))
    
########################################################################################################
########################################################################################################
##
## XML -> parser metadata type maps
##
########################################################################################################
########################################################################################################

####################################################
## GetParMemberType
## 
## For each Spec object, which parMember subclass does it correspond to
####################################################
CreateSimpleGetterFunctions("GetParMemberType", {
    MemberSpec: "unknownType",
    StructSpec: "parMemberStruct",
    IntSpec:    "parMemberSimple",
    FloatSpec:  "parMemberSimple",
    VectorSpec: "parMemberVector",
    ArraySpec:  "parMemberArray",
    MapSpec:    "parMemberMap",
    StringSpec: "parMemberString",
    EnumSpec:   "parMemberEnum",
    BitsetSpec: "parMemberEnum",
    MatrixSpec: "parMemberMatrix",
    BoolSpec:   "parMemberSimple",
    }
)
    
####################################################
## GetParMemberTypeEnum
##
## For each Spec object, which parMember::Type enum does it correspond to
####################################################
CreateSimpleGetterFunctions("GetParMemberTypeEnum", {
    MemberSpec: "unknownEnum",
    StructSpec: "TYPE_STRUCT",
    IntSpec:    "TYPE_INT",
    U32Spec:    "TYPE_UINT",
    CharSpec:   "TYPE_CHAR",
    U8Spec:     "TYPE_UCHAR",
    ShortSpec:  "TYPE_SHORT",
    U16Spec:    "TYPE_USHORT",
    S64Spec:    "TYPE_INT64",
    U64Spec:    "TYPE_UINT64",
    FloatSpec:  "TYPE_FLOAT",
    Float16Spec:  "TYPE_FLOAT16",
    DoubleSpec:     "TYPE_DOUBLE",
    Vector2Spec:    "TYPE_VECTOR2",
    Vector3Spec:    "TYPE_VECTOR3",
    Vector4Spec:    "TYPE_VECTOR4",
    Vec2VSpec:      "TYPE_VEC2V",
    Vec3VSpec:      "TYPE_VEC3V",
    Vec4VSpec:      "TYPE_VEC4V",
    ArraySpec:      "TYPE_ARRAY",
    MapSpec:        "TYPE_MAP",
    StringSpec:     "TYPE_STRING",
    EnumSpec:       "TYPE_ENUM",
    BitsetSpec:     "TYPE_BITSET",
    Matrix34Spec:   "TYPE_MATRIX34",
    Matrix44Spec:   "TYPE_MATRIX44",
    Mat33VSpec:     "TYPE_MAT33V",
    Mat34VSpec:     "TYPE_MAT34V",
    Mat44VSpec:     "TYPE_MAT44V",
    BoolSpec:       "TYPE_BOOL",
    BoolVSpec:      "TYPE_BOOLV",
    ScalarVSpec:    "TYPE_SCALARV",
    VecBoolVSpec:   "TYPE_VECBOOLV",
    SizeTSpec:      "TYPE_SIZET",
    PtrdiffTSpec:   "TYPE_PTRDIFFT",
    }
)

####################################################
## GetSubtype
##
## Normally 0, but overridden for certain types - this doesn't always get called either, often a subtype is explicitly
## passed in during WriteStandardMemberData
####################################################
CreateSimpleGetterFunctions("GetSubtype", {
    DataMemberSpec: "0",
    Color32Spec:    "SUBTYPE_COLOR",
    AngleSpec:      "SUBTYPE_ANGLE",
    Vector3ColorSpec:   "SUBTYPE_COLOR",
    Vec3VColorSpec: "SUBTYPE_COLOR",
    }
)

########################################################################################################
########################################################################################################
##
## Output code for MEMBER DATA below
##
########################################################################################################
########################################################################################################

####################################################
## WriteMemberData
##
## Writes out the definition for the parMember*::Data subclasses that 
## define each member.
## Most of these functions call WriteStandardMemberData to write the common data
####################################################

@AddMethod(DataMemberSpec)
def WriteMemberData(self):
    st = self.GetSubtype()
    if st != "0":
        st = "::rage::parMemberSimpleSubType::" + st
    self.WriteStandardMemberData(subtype=st)
    self.WriteString('''    %(init)s,
#if __BANK
    "%(description)s", (float)%(:min)s, (float)%(:max)s, (float)%(:step)s,
#endif
};''', min=GetCppFloat(self.min), max=GetCppFloat(self.max), step=GetCppFloat(self.step))

@AddMethod(FloatSpec)
def WriteMemberData(self):
    st = self.GetSubtype()
    if st != "0":
        st = "::rage::parMemberSimpleSubType::" + st
    self.WriteStandardMemberData(subtype=st)
    self.WriteString('''    %(:init)s,
#if __BANK
    "%(description)s", (float)%(:min)s, (float)%(:max)s, (float)%(:step)s,
#endif
};''', init=GetCppFloat(self.init), min=GetCppFloat(self.min), max=GetCppFloat(self.max), step=GetCppFloat(self.step))

@AddMethod(StructSpec)
def WriteMemberData(self):
    flags = []
    if self.addGroupWidget != "false":
        flags.append("ADD_GROUP_WIDGET")
    self.WriteStandardMemberData(subtype="::rage::parMemberStructSubType::SUBTYPE_STRUCTURE", flags=flags)
    self.WriteString('''#if __BANK
    "%(description)s", 
#endif
\tNULL,
};''')

@AddMethod(PointerSpec)
def WriteMemberData(self):
    flags = []
    subtype = "SUBTYPE_SIMPLE_POINTER"
    if self.addGroupWidget != "false":
        flags.append("ADD_GROUP_WIDGET")

    if (self.policy == "owner"):
        subtype = "SUBTYPE_POINTER"
    elif (self.policy == "simple_owner"):
        subtype = "SUBTYPE_SIMPLE_POINTER"
    elif (self.policy == "external_named"):
        subtype = "SUBTYPE_EXTERNAL_NAMED_POINTER"
        if (self.userHandlesNull != "false"):
            subtype = "SUBTYPE_EXTERNAL_NAMED_POINTER_USERNULL"
    elif (self.policy == "link"):
        subtype = "SUBTYPE_LINK_IMMEDIATE"
    elif (self.policy == "lazylink"):
        subtype = "SUBTYPE_LINK_LAZY"
    self.WriteStandardMemberData(flags=flags, subtype="::rage::parMemberStructSubType::" + subtype)
    self.WriteString('''#if __BANK
    "%(description)s", 
#endif
NULL,''')

    if self.policy == "external_named":
        self.WriteString('''
    reinterpret_cast< void*(*)(const char*) >(%(fromString)s),
    reinterpret_cast< const char*(*)(const void*) >(%(toString)s),''')
    else:
        self.WriteString('''
    NULL,
    NULL,''')

    if (self.policy == "owner" and self.onUnknownType):
        self.WriteString("\n\treinterpret_cast< void*(*)(::rage::parTreeNode*) >(%(onUnknownType)s),")
    else:
        self.WriteString("\n\tNULL,")

    self.WriteString('''                     
};''')

@AddMethod(MapSpec)
def WriteMemberData(self):
    self.ChildSpec.WriteMemberData()
    self.KeySpec.WriteMemberData()
    
    flags = []

    if self.addGroupWidget != "false":
        flags.append("ADD_GROUP_WIDGET")
    
    subtypeMap = {
        'atMap': "SUBTYPE_ATMAP",
        'atBinaryMap': "SUBTYPE_ATBINARYMAP"
        }
    
    subtype = subtypeMap[self.type]
    
    if self.type == "atMap":
        createIteratorDelegate = "&" + self.FindParMemberName() + "_iteratorDel"
        self.WriteString('''::rage::parMemberMapData::CreateIteratorDelegate %(:parMemName)s_iteratorDel(&::rage::parMemberMapIterators::CreateIteratorAtMap< %(:kType)s, %(:dType)s > );
''',
            kType = self.KeySpec.GetCType(),
            dType = self.ChildSpec.GetCType(),
            parMemName = self.FindParMemberName(),
        )
        
        createInterfaceDelegate = "&" + self.FindParMemberName() + "_interfaceDel"
        self.WriteString('''::rage::parMemberMapData::CreateInterfaceDelegate %(:parMemName)s_interfaceDel(&::rage::parMemberMapInterfaces::CreateInterfaceAtMap< %(:kType)s, %(:dType)s > );
''',
            kType = self.KeySpec.GetCType(),
            dType = self.ChildSpec.GetCType(),
            parMemName = self.FindParMemberName(),
        )
        
    elif self.type == "atBinaryMap":
        createIteratorDelegate = "&" + self.FindParMemberName() + "_iteratorDel"
        self.WriteString('''::rage::parMemberMapData::CreateIteratorDelegate %(:parMemName)s_iteratorDel(&::rage::parMemberMapIterators::CreateIteratorAtBinaryMap );
''',
            kType = self.KeySpec.GetCType(),
            dType = self.ChildSpec.GetCType(),
            parMemName = self.FindParMemberName(),
        )
        
        createInterfaceDelegate = "&" + self.FindParMemberName() + "_interfaceDel"
        self.WriteString('''::rage::parMemberMapData::CreateInterfaceDelegate %(:parMemName)s_interfaceDel(&::rage::parMemberMapInterfaces::CreateInterfaceAtBinaryMap );
''',
            kType = self.KeySpec.GetCType(),
            dType = self.ChildSpec.GetCType(),
            parMemName = self.FindParMemberName(),
        )
        
    else:
        insertKeyDelegate = "NULL"
        postInsertDelegate = "NULL"
        createIteratorDelegate = "NULL"
        resetDelegate = "NULL"
    
    self.WriteStandardMemberData(flags=flags, subtype="::rage::parMemberMapSubType::" + subtype)   
    self.WriteString(
    '''    %(:createIteratorDelegate)s,
    %(:createInterfaceDelegate)s,
    &%(:key)s,
    &%(:data)s,
};''',
    createIteratorDelegate = createIteratorDelegate,
    createInterfaceDelegate = createInterfaceDelegate,
    key = self.KeySpec.FindParMemberName(),
    data = self.ChildSpec.FindParMemberName()
    )

@AddMethod(ArraySpec)
def WriteMemberData(self):  
    self.ChildSpec.WriteMemberData()
    
    flags = []
    typeFlags = []
    
    if self.addGroupWidget != "false":
        flags.append("ADD_GROUP_WIDGET")
        
    if self.heap == "physical":
        typeFlags.append("FLAG_ALLOCATE_IN_PHYSICAL_MEMORY")
        
    if self.align:
        alignVal = int(self.align)
        alignPower = 0
        if alignVal > 0:
            alignPower = int(math.log(alignVal, 2))
            if not (0 <= alignPower < 16):
                CodeGenError("Invalid alignment power %d" % alignPower);
        if alignPower & 0x1:
            flags.append("ARRAY_ALIGN_BIT1")
        if alignPower & 0x2:
            flags.append("ARRAY_ALIGN_BIT2")
        if alignPower & 0x4:
            flags.append("ARRAY_ALIGN_BIT4")
        if alignPower & 0x8:
            flags.append("ARRAY_ALIGN_BIT8")
        
    subtype = "0"

    subtypeMap = {
        'atFixedArray': "SUBTYPE_ATFIXEDARRAY",
        'atRangeArray': "SUBTYPE_ATRANGEARRAY",
        'pointer': "SUBTYPE_POINTER",
        'member': "SUBTYPE_MEMBER",
        'virtual': "SUBTYPE_VIRTUAL"
        }

    indexBitsMap = {
        '16': "SUBTYPE_ATARRAY",
        '32': "SUBTYPE_ATARRAY_32BIT_IDX",
        }

    if self.type == "atArray":
        subtype = indexBitsMap[self.indexBits]
    elif self.type == "pointer" and self.sizeVar:
        subtype = "SUBTYPE_POINTER_WITH_COUNT"
    else:
        subtype = subtypeMap[self.type]

    ctype = self.ChildSpec.GetCType()
    if self.type == "atArray":
        self.WriteString("::rage::parMemberArrayData::ResizeArrayDelegate %(:parMemName)s_resizeDel(&::rage::parMemberArrayResizers::ResizeAtArray< %(:cType)s, ::rage::u%(indexBits)s > );", cType=ctype, parMemName=self.FindParMemberName())
        resizeOrVirtualDelegate = "&" + self.FindParMemberName() + "_resizeDel"
    elif self.type == "pointer":
        self.WriteString("::rage::parMemberArrayData::ResizeArrayDelegate %(:parMemName)s_resizeDel(&::rage::parMemberArrayResizers::ResizeRawArray< %(:cType)s > );", cType=ctype, parMemName=self.FindParMemberName())
        resizeOrVirtualDelegate = "&" + self.FindParMemberName() + "_resizeDel"
    else:
        resizeOrVirtualDelegate = "NULL"
        
		
    self.WriteStandardMemberData(flags=flags, subtype="::rage::parMemberArraySubType::" + subtype, typeFlags=typeFlags)     
    # element size
    self.WriteString('''
    sizeof(%(:cType)s),
    {%(size)s},
    &%(:kidParMemName)s,
	%(:resizeOrVirtualDelegate)s,
	};''',
                    cType=ctype,
                    kidParMemName=self.ChildSpec.FindParMemberName(),
                    resizeOrVirtualDelegate = resizeOrVirtualDelegate)

@AddMethod(BoolSpec)
def WriteMemberData(self):
    self.WriteStandardMemberData()
    self.WriteString('''
    (float)%(init)s,
#if __BANK
    "%(description)s", 0.0f, 0.0f, 0.0f
#endif
};''')

@AddMethod(EnumSpec)
def WriteMemberData(self):
    self.WriteString('''
extern ::rage::parEnumData %(:enumDataName)s;
''', enumDataName=self.GetEnumDataName())

    self.WriteStandardMemberData(subtype=self.GetEnumSubtype()) # note not using the standard GetSubtype here.
    self.WriteString('''    %(init)s,
    &%(:enumDataName)s,
    0,
#if __BANK
    "%(description)s",
#endif
};''', enumDataName=self.GetEnumDataName())

@AddMethod(BitsetSpec)
def WriteMemberData(self):
    if self.EnumDefType:
        self.WriteString('''
extern ::rage::parEnumData %(:enumDataName)s;
''', enumDataName=self.GetEnumDataName())

    self.WriteStandardMemberData(subtype=self.GetEnumSubtype()) # note not using the standard GetSubtype here.
    self.WriteString('''    %(init)s,
    &%(:enumDataName)s,
    (::rage::u16)%(:numBits)s,
#if __BANK
    "%(description)s",
#endif
};''', enumDataName=self.GetEnumDataName(), numBits=self.numBits)

@AddMethod(MatrixSpec)
def WriteMemberData(self):
    typeFlags = []
    if self.highPrecision == "true":
        typeFlags = ["FLAG_HIGH_PRECISION"]
    self.WriteStandardMemberData(typeFlags=typeFlags)
    init = ", ".join(GetCppFloatList(self.init))
    self.WriteString('''
    %(:init)s, 
#if __BANK
    "%(description)s", (float)%(:min)s, (float)%(:max)s, (float)%(:step)s,
#endif
};''', init=init, min=GetCppFloat(self.min), max=GetCppFloat(self.max), step=GetCppFloat(self.step))

@AddMethod(StringSpec)
def WriteMemberData(self):
    flags = []
    subtype = "0"
    customTypeFlagStr = None
    
    subtypeMap = {
        "member":       "::rage::parMemberStringSubType::SUBTYPE_MEMBER",
        "pointer":      "::rage::parMemberStringSubType::SUBTYPE_POINTER",
        "ConstString":  "::rage::parMemberStringSubType::SUBTYPE_CONST_STRING",
        "atString":     "::rage::parMemberStringSubType::SUBTYPE_ATSTRING",
        "wide_member":  "::rage::parMemberStringSubType::SUBTYPE_WIDE_MEMBER",
        "wide_pointer": "::rage::parMemberStringSubType::SUBTYPE_WIDE_POINTER",
        "atWideString": "::rage::parMemberStringSubType::SUBTYPE_ATWIDESTRING",
        "atHashValue":  "::rage::parMemberStringSubType::SUBTYPE_ATHASHVALUE",
        "atHashWithStringDev": "(__DEV ? ::rage::parMemberStringSubType::SUBTYPE_ATNONFINALHASHSTRING : ::rage::parMemberStringSubType::SUBTYPE_ATHASHVALUE)",
        "atHashWithStringBank": "(__BANK ? ::rage::parMemberStringSubType::SUBTYPE_ATNONFINALHASHSTRING : ::rage::parMemberStringSubType::SUBTYPE_ATHASHVALUE)",
        "atHashWithStringNotFinal": "(!__FINAL ? ::rage::parMemberStringSubType::SUBTYPE_ATNONFINALHASHSTRING : ::rage::parMemberStringSubType::SUBTYPE_ATHASHVALUE)",
        "atFinalHashString":    "::rage::parMemberStringSubType::SUBTYPE_ATFINALHASHSTRING",
        "atPartialHashValue":   "::rage::parMemberStringSubType::SUBTYPE_ATPARTIALHASHVALUE",
    }
    
    subtype = subtypeMap.get(self.type, "0")
    
    if subtype == "0":
        if self.NamespaceType != None:
            customTypeFlagStr = "0x%x" % self.NamespaceIndex
            if self.NamespaceType == "string":
                subtype = "::rage::parMemberStringSubType::SUBTYPE_ATNSHASHSTRING";
            elif self.NamespaceType == "value":
                subtype = "::rage::parMemberStringSubType::SUBTYPE_ATNSHASHVALUE";
    
    self.WriteStandardMemberData(flags=flags, subtype=subtype, customTypeFlagStr = customTypeFlagStr)
    self.WriteString('''    %(size)s,
#if __BANK
    "%(description)s",
#endif
};''')

@AddMethod(VectorSpec)
def WriteMemberData(self, subtype="0"):
    typeFlags = []
    if self.highPrecision == "true":
        typeFlags = ["FLAG_HIGH_PRECISION"]
    st = self.GetSubtype()
    if st != "0":
        st = "::rage::parMemberVectorSubType::" + st
    self.WriteStandardMemberData(subtype=st, typeFlags=typeFlags)
    init = ", ".join(GetCppFloatList(self.init))
    self.WriteString('''
    %(:init)s, 
#if __BANK
    "%(description)s", (float)%(:min)s, (float)%(:max)s, (float)%(:step)s,
#endif
};''', init=init, min=GetCppFloat(self.min), max=GetCppFloat(self.max), step=GetCppFloat(self.step))

####################################################
## WriteStandardMemberData
##
## Writes out the definition for the common parMember::Data (not type specific data -
## that's done in WriteMemberData)
####################################################

@AddMethod(MemberSpec)
def WriteStandardMemberData(self, flags=[], type=None, subtype="0", noVisitFlags=[], typeFlags=[], customTypeFlagStr = None):
    self.SetErrorInfo()
    if type == None:
        type = self.GetParMemberTypeEnum()
    # always add FLAG_STATIC so we don't delete these objects            
    localFlags = flags[:] + ["STATIC"]
    localNoVisitFlags = noVisitFlags[:]
    if self.hideWidgets == "true":
        localNoVisitFlags.append("VISIT_BANK")
    if self.noInit == "true":
        localFlags.append("NO_INIT")

    hideMap = {
        "file" : "VISIT_FILE",
        "net" : "VISIT_NET",
        "script" : "VISIT_SCRIPT",
        "tool" : "VISIT_TOOL",
        "bank" : "VISIT_BANK"
        }

    if self.hideFrom:
        hideRequests = [x.strip() for x in self.hideFrom.split(',')]
        for i in hideRequests:
            if hideMap.has_key(i):
                localNoVisitFlags.append(hideMap[i])
            else:
                CodeGenWarning("'%s' isn't allowed in a hideFrom= attribute. Allowed values are: %s" % (i, ", ".join(hideMap.keys())), wikiPage=self.WikiPage)

    flagStr = " |\n\t".join(["(1 << ::rage::parMemberFlags::MEMBER_%s)" % (x) for x in localFlags])

    visitFlagStr = '0xFFFF'
    if len(localNoVisitFlags) > 0:
        visitFlagStr = "0xFFFF & ~(" + " |\n\t".join(["(1 << ::rage::parMemberVisitorFlags::%s)" % (x) for x in localNoVisitFlags]) + ")"

    typeFlagStr = '0'
    if customTypeFlagStr != None:
        typeFlagStr = customTypeFlagStr
    elif len(typeFlags) > 0:
        typeFlagStr = " |\n\t".join(["(1 << ::rage::%sData::%s)" % (self.GetParMemberType(), x) for x in typeFlags])

    # write name, offset, flags, type, subtype
    self.WriteString('''
::rage::%(:parMemType)sData %(:parMemName)s = {
    0x%(:namehash)08x, // "%(ParserName)s"
    0,
    ::rage::parMemberType::%(:typeEnum)s,
    %(:subtype)s,
    %(:flags)s,
    %(:visitFlags)s,
    %(:typeFlags)s,
    NULL,
''', parMemType=self.GetParMemberType(), parMemName=self.FindParMemberName(), flags=flagStr,
                     visitFlags=visitFlagStr, typeEnum=type, subtype=subtype,
                     namehash=self.NameHash, typeFlags=typeFlagStr)
    self.UnsetErrorInfo()

        
@AddMethod(FloatSpec)
def WriteStandardMemberData(self, **kwargs):
    typeFlags = []
    if self.highPrecision == "true":
        typeFlags = ["FLAG_HIGH_PRECISION"]
    DataMemberSpec.WriteStandardMemberData(self, typeFlags=typeFlags, **kwargs)

@AddMethod(DoubleSpec)
def WriteStandardMemberData(self, **kwargs):
    typeFlags = ["FLAG_HIGH_PRECISION"]
    DataMemberSpec.WriteStandardMemberData(self, typeFlags=typeFlags, **kwargs)

    
########################################################################################################
########################################################################################################
##
## Output code for REGISTRATION FUNCTION below
##
########################################################################################################
########################################################################################################

####################################################
## AddMemberCallbacks
##   used for any type that needs to add member callbacks to the output (e.g. onWidgetChanged)
####################################################

@AddMethod(MemberSpec)
def AddMemberCallbacks(self):
    pass # by default don't write any callbacks

@AddMethodToClasses(DataMemberSpec, MapSpec, ArraySpec, BoolSpec, EnumSpec, MatrixSpec, StringSpec, VectorSpec)
def AddMemberCallbacks(self):
    self.WriteStandardMemberCallbacks()

####################################################
## WriteStandardMemberCallbacks
##   writes the code to add callbacks for a member
####################################################
    
@AddMethod(MemberSpec)
def WriteStandardMemberCallbacks(self):
    if self.onWidgetChanged:
        self.WriteString('''
#if __BANK
    %(:parMemName)s.m_WidgetCb = rage_new ::rage::datCallback(MFA(%(:parType)s::%(onWidgetChanged)s), (instance+1));
#endif
''', parMemName=self.FindParMemberName(), parType=self.FindParStructureType())
    
@AddMethod(ArraySpec)
def WriteStandardMemberCallbacks(self):
    self.ChildSpec.WriteStandardMemberCallbacks()

@AddMethod(MapSpec)
def WriteStandardMemberCallbacks(self):
    self.ChildSpec.WriteStandardMemberCallbacks()
    self.KeySpec.WriteStandardMemberCallbacks()
    
####################################################
## ResolvePointers
##   In some places we have to set pointers from one member data to another. We do that in the registration function where we can get the 
##   addresses of all of the member data objects
####################################################

@AddMethod(MemberSpec)
def ResolvePointers(self):
    pass
        
@AddMethod(StructSpec)
def ResolvePointers(self):
    # m_StructurePtr points to the appropriate parStructure
    self.WriteString('''
    %(:member)s.m_StructurePtr = ::rage::parCguGetRequiredStructure< %(type)s >(__DEV ? "%(type)s" : NULL, __DEV ? "%(:structureType)s" : NULL);''',
                     structureType=self.FindParStructureType(), member=self.FindParMemberName())
                     
@AddMethod(PointerSpec)
def ResolvePointers(self):
    if self.policy in ["owner", "simple_owner", "link", "lazylink"]:
        StructSpec.ResolvePointers(self)
    else:
        # external pointers don't need resolution
        pass
        
@AddMethod(ArraySpec)
def ResolvePointers(self):
    # this isn't really pointer resolution but its a place where we want to do runtime checks instead of compile-time ones.
    if self.type == 'pointer' and self.sizeVar:
        self.WriteString('''
    if (sizeof(instance->%(sizeVar)s) == 1)
    {
        %(:parMemName)s.m_Subtype = ::rage::parMemberArraySubType::SUBTYPE_POINTER_WITH_COUNT_8BIT_IDX;
    }
    else if (sizeof(instance->%(sizeVar)s) == 2)
    {
        %(:parMemName)s.m_Subtype = ::rage::parMemberArraySubType::SUBTYPE_POINTER_WITH_COUNT_16BIT_IDX;
    }
    // Default is 32 bit index
    %(:parMemName)s.m_CountMemberOffset = (::rage::u32)(reinterpret_cast<char*>(&(instance->%(sizeVar)s)) - reinterpret_cast<char*>(instance));
''', parMemName=self.FindParMemberName())

    if self.type == 'virtual':
        if self.onVirtualRead[:2] == "::":
            self.WriteString('''
    %(:parMemName)s.m_VirtualReadCallback = rage_new ::rage::parMemberArrayData::VirtualReadDelegate(&%(onVirtualRead)s);
    ''', parMemName=self.FindParMemberName())
        else:
            self.WriteString('''
    %(:parMemName)s.m_VirtualReadCallback = rage_new ::rage::parMemberArrayData::VirtualReadDelegate(instance, &%(:parentType)s::%(onVirtualRead)s);
    ''', parentType=self.FindParStructureType(), parMemName=self.FindParMemberName())

    # make sure to call the child function, in case it needs pointer resolution.
    self.ChildSpec.ResolvePointers()

@AddMethod(MapSpec)
def ResolvePointers(self):
    # make sure to call the child function, in case it needs pointer resolution.
    self.ChildSpec.ResolvePointers()
    self.KeySpec.ResolvePointers()

####################################################
## WriteExtraAttributes
##   Extra attributes can be attached to any member, and need to be set up at runtime in the registration function
####################################################
@AddMethod(MemberSpec)
def WriteExtraAttributes(self):
    if len(self.ExtraAttrs):
        for (key, value) in self.ExtraAttrs.iteritems():
            self.WriteString('''
    ::rage::parCguStructure::AddOneExtraAttribute(&(%(:parMemName)s.m_ExtraAttributes), "%(:key)s", "%(:value)s");''',
                             parMemName=self.FindParMemberName(),
                             key=key,
                             value=value)

@AddMethod(StringSpec)
def WriteExtraAttributes(self):
    MemberSpec.WriteExtraAttributes(self)
    if self.init != None and self.init != "":
        oldExtraAttrs = self.ExtraAttrs

        if self.type in ("atHashWithStringNotFinal", "atHashValue"):
            if self.type == "atHashWithStringNotFinal":
                self.WriteString('''
    NOTFINAL_ONLY(::rage::atHashWithStringNotFinal %(:parMemName)s_initString("%(:value)s")); // Add this string to the atHashString map''',
                     parMemName=self.FindParMemberName(),
                     value=self.init);
    
            self.WriteString('''
    ::rage::parCguStructure::AddOneExtraAttribute(&(%(:parMemName)s.m_ExtraAttributes), "initHashValue", (int)0x%(:hashvalue)08x); /* "%(:value)s" */''',
                             parMemName=self.FindParMemberName(),
                             value=self.init,
                             hashvalue=atHash.atStringHash(self.init))
        else:
            self.ExtraAttrs = {"initValue": self.init} 
            MemberSpec.WriteExtraAttributes(self)

        self.ExtraAttrs = oldExtraAttrs
    
@AddMethod(ArraySpec)
def WriteExtraAttributes(self):
    MemberSpec.WriteExtraAttributes(self)
    self.ChildSpec.WriteExtraAttributes() # write extra attrs for the children too

@AddMethod(MapSpec)
def WriteExtraAttributes(self):
    MemberSpec.WriteExtraAttributes(self)
    self.ChildSpec.WriteExtraAttributes() # write extra attrs for the children too
    self.KeySpec.WriteExtraAttributes() # write extra attrs for the children too
    
####################################################
## Misc functions
####################################################

@AddMethod(MemberSpec)
def FindParMemberName(self):
    if not self.Parent.Parent: # if the parent is a structdef
        return "parser_" + self.Parent.ParserName + "_" + self.ParserName
    else:
        return self.Parent.FindParMemberName() + "_" + self.ParserName

@AddMethod(MemberSpec)
def WriteAddress(self, structType):
    self.WriteString('''OffsetOf(%(:structType)s, %(CppName)s)''', structType=structType)

@AddMethod(ArraySpec)
def WriteAddress(self, structType):
    if self.type == 'virtual':
        self.WriteString('''0xDEADBEEF% /* %(CppName)s is a virtual array member - do not use this offset */''')
    else:
        MemberSpec.WriteAddress(self, structType)

@AddMethod(MemberSpec)
def WriteSizeCheck(self, memName): # for adding a compile-time size check for each member
     self.WriteString("\n    CompileTimeAssert(sizeof(%(:memName)s) == sizeof(%(:size)s)); // If this fails, check the declaration of \"%(CppName)s\" in structdef type=\"%(:type)s\"",
                      memName=memName, size=self.GetCType(), type=self.FindParStructureType())
                      
@AddMethod(ArraySpec)
def WriteSizeCheck(self, memName):
    if self.type == 'virtual':
        pass # no size checks for virtual members
    else:
        MemberSpec.WriteSizeCheck(self, memName)
        self.ChildSpec.WriteSizeCheck(memName + "[0]")

@AddMethod(MapSpec)
def WriteSizeCheck(self, memName):
    MemberSpec.WriteSizeCheck(self, memName)
    if (self.type == 'atBinaryMap'):
        self.ChildSpec.WriteSizeCheck("(*(" + memName + ".GetItem(0)))")
        self.KeySpec.WriteSizeCheck("(*(" + memName + ".GetKey(0)))")
    elif (self.type == 'atMap'):
        self.ChildSpec.WriteSizeCheck(memName + ".GetEntry(0)->data");
        self.KeySpec.WriteSizeCheck(memName + ".GetEntry(0)->key");
        
@AddMethod(MemberSpec)
def FindExtraMemberNames(self, extraNames):
    pass
    
@AddMethod(ArraySpec)
def FindExtraMemberNames(self, extraNames):
    if self.ChildSpec.name:
        extraNames.add( self.ChildSpec.name )
    self.ChildSpec.FindExtraMemberNames(extraNames)

@AddMethod(MapSpec)
def FindExtraMemberNames(self, extraNames):
    if self.ChildSpec.name:
        extraNames.add( self.ChildSpec.name )
    self.ChildSpec.FindExtraMemberNames(extraNames)
    if self.KeySpec.name:
        extraNames.add( self.KeySpec.name )
    self.KeySpec.FindExtraMemberNames(extraNames)
    
        
