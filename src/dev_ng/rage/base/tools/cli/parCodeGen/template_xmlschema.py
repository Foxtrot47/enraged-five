
# This template file contains all of the code specific to generating XML Schema documents from parser metadata.
# The general pattern here is to inject new member functions into the existing class hierarchy 
# and the new member functions will be used to generate the code.
#
# Most code below will use the @AddMethod decorator like so:
# @AddMethod(ClassName)
# def NewMethod(self): 
#   # do stuff
#
# That adds a ClassName.NewMethod member function
# @AddMethod is defined in templatesupport.py

from templatesupport import *
from basetype import *
from structtype import *
from inttypes import *
from floattypes import *
from vectortypes import *
from arraytype import *
from stringtypes import *
from enumtype import *
from matrixtypes import *
from datatypes import *


##############################################
## GenericSpec
##############################################

@AddMethod(GenericSpec)
def WriteSchemaCode(self):
    pass

@AddMethod(GenericSpec)
def WriteSchematronCode(self, useDi):
    pass
   
###############################################
## DocumentSpec
###############################################

@AddMethod(DocumentSpec)
def WriteSchemaCode(self):
    GenericSpec.Depth = 0
    self.WriteString(
'''<?xml version="1.0" encoding="utf-8" ?>
<xsd:schema
    xmlns:xsd="http://www.w3.org/2001/XMLSchema"
    xmlns:sch="http://www.ascc.net/xml/schematron"
    xmlns:par="http://www.rockstargames.com/RageParserSchema">
    
    <xsd:import
        namespace="http://www.rockstargames.com/RageParserSchema"
        schemaLocation="file://C:/ragesoft/rage/base/src/parser/parsertypes.xsd"
    />

    <xsd:annotation>
        <xsd:appinfo>
            <sch:title>Schematron Validation</sch:title>
        </xsd:appinfo>
    </xsd:annotation>''')
        
    GenericSpec.Depth += 1
    for i in self.Children:
        i.WriteSchemaCode()
    GenericSpec.Depth -= 1
    self.WriteString(
'''
</xsd:schema>''')

###############################################
## StructDefSpec
###############################################

@AddMethod(StructDefSpec)
def WriteSchemaCode(self):
    self.SetStructdefErrorInfo()
    self.WriteString('''

<!-- ****************    Schema for %(ParserName)s    ***************  -->
''')
        
    asserts = [x for x in self.Children if isinstance(x, AssertSpec)]
    limits = [x for x in self.Children if x.HasSpecialLimits()]

    usesSchematron = True
    if len(asserts) > 0 or len(limits) > 0 or self.usedi == "true":
        usesSchematron = True
    
    if usesSchematron:
        self.WriteString('''
<xsd:annotation>
    <xsd:appinfo>
        <sch:pattern id="%(ParserName)s_requiredDataTest" name="Required Data Test">
            <sch:rule context="%(ParserName)s">''')
        GenericSpec.Depth += 5
        for i in self.Children:
            i.WriteSchematronCode(self.usedi == "true")
        GenericSpec.Depth -= 5
        self.WriteString(
'''
            </sch:rule>
        </sch:pattern>''')

        if len(asserts) > 0:
            self.WriteString(
'''
        <sch:pattern id="%(ParserName)s_dataRelationships" name="Data Relationships">
            <sch:rule context="%(ParserName)s">''')
            GenericSpec.Depth += 5
            for i in asserts:
                i.WriteSchematronAssertion()
            GenericSpec.Depth -= 5
            self.WriteString(
'''
            </sch:rule>
        </sch:pattern>''')

        if len(limits) > 0:
            self.WriteString(
'''
        <sch:pattern id="%(ParserName)s_limitCheck" name="Data Range Checking">
            <sch:rule context="%(ParserName)s">''')
            GenericSpec.Depth += 5
            for i in limits:
                i.WriteSchematronLimitTest()
            GenericSpec.Depth -= 5
            self.WriteString(
'''
            </sch:rule>
        </sch:pattern>''')
        self.WriteString(
'''        
    </xsd:appinfo>
</xsd:annotation>
''')

    minOccurs = 1
    if self.usedi == "true":
        minOccurs = 0
    base="par:Structure"
    if self.base:
        base=self.base
    self.WriteString(
'''
<xsd:complexType name="%(ParserName)s">
    <xsd:complexContent>
        <xsd:extension base="%(:base)s">
            <xsd:sequence minOccurs="0">''', base=base)

    GenericSpec.Depth += 5
    for i in self.Children:
        i.WriteSchemaCode()
    GenericSpec.Depth -= 5

    self.WriteString(
'''
            </xsd:sequence>
        </xsd:extension>
    </xsd:complexContent>
</xsd:complexType>
''')
        
    if (self.constructible == "true"):
        self.WriteString(
'''

<xsd:element name="%(ParserName)s" type="%(ParserName)s"/>
''')
    self.UnsetStructdefErrorInfo();

###############################################
## EnumDefSpec
###############################################
@AddMethod(EnumDefSpec)
def WriteSchemaCode(self):
    self.WriteString('''
    
<!-- ****************    Schema for %(ParserName)s    ***************  -->
    
<xsd:simpleType name="%(ParserName)s">
    <xsd:restriction base="xsd:string">''')
    for i in self.Children:
        i.WriteSchemaCode()
    self.WriteString('''
    </xsd:restriction>
</xsd:simpleType>
''')

###############################################
## EnumValSpec
###############################################
@AddMethod(EnumValSpec)
def WriteSchemaCode(self):
    self.WriteString('''
        <xsd:enumeration value="%(name)s"/>''')
    
###############################################
## MemberSpec
###############################################

@AddMethod(MemberSpec)
def WriteSchemaCode(self):
    self.WriteString('\n<xsd:element name="%(ParserName)s" type="%(:schemaName)s"/>',
                     schemaName=self.GetSchemaName())

@AddMethod(MemberSpec)
def WriteSchematronCode(self, useDi):
    if useDi:
        self.WriteString('\n<sch:assert test="@parent or %(ParserName)s">%(ParserName)s is required if there is no parent</sch:assert>')
    else:
        self.WriteString('\n<sch:assert test="count(%(ParserName)s) = 1">Exactly one &lt;%(ParserName)s&gt; must exist</sch:assert>')
        
###############################################
## AssertSpec and ReportSpec
###############################################

@AddMethod(AssertSpec)
def WriteSchematronAssertion(self):
    if self.message == "":
        self.WriteString('\n<sch:assert test=%(test)s>%(test)s</sch:assert>')
    else:
        self.WriteString('\n<sch:assert test=%(test)s>%(test)s: %(message)s</sch:assert>')

@AddMethod(ReportSpec)
def WriteSchematronAssertion(self):
    if self.message == "":
        self.WriteString('\n<sch:report test=%(test)s>%(test)s</sch:report>')
    else:
        self.WriteString('\n<sch:report test=%(test)s>%(test)s: %(message)s</sch:report>')

###############################################
## PointerSpec
###############################################

@AddMethod(PointerSpec)
def WriteSchemaCode(self):
    self.WriteString('''
<xsd:element name="%(ParserName)s">
    <xsd:complexType>
        <xsd:simpleContent>
            <xsd:extension base="%(type)s">
                <xsd:attribute name="type" use="required"/>
            </xsd:extension>
        </xsd:simpleContent>
    </xsd:complexType>
</xsd:element>''')
        
###############################################
## ArraySpec
###############################################

@AddMethod(ArraySpec)
def WriteSchemaCode(self):
    # type of schema to output depends on contents.
    # could check # of children here

    minOccurs = "0"
    maxOccurs = "unbounded"
    if self.size:
        minOccurs = self.size
        maxOccurs = self.size
    
    if isinstance(self.ChildSpec, ArraySpec) or self.IsArrayOfStructures():
        self.WriteString('''
<xsd:element name="%(ParserName)s">
    <xsd:complexType>
        <xsd:sequence minOccurs="%(:minOccurs)s" maxOccurs="%(:maxOccurs)s">''',
                         minOccurs=minOccurs,
                         maxOccurs=maxOccurs)
        GenericSpec.Depth += 3
        self.ChildSpec.WriteSchemaCode()
        GenericSpec.Depth -= 3
        self.WriteString('''
        </xsd:sequence>
    </xsd:complexType>
</xsd:element>''')
    else:
        MemberSpec.WriteSchemaCode(self)

###############################################
## MapSpec
###############################################

@AddMethod(MapSpec)
def WriteSchemaCode(self):
    # type of schema to output depends on contents.
    # could check # of children here

    minOccurs = "0"
    maxOccurs = "unbounded"
    
    otherExtns = ""
    if isinstance(self.ChildSpec, PointerSpec):
        otherExtns = '''<xsd:attribute name="type" use="required"/>'''
    
    self.WriteString('''
<xsd:element name="%(ParserName)s">
    <xsd:complexType>
        <xsd:sequence minOccurs="%(:minOccurs)s" maxOccurs="%(:maxOccurs)s">
            <xsd:element name="%(:childName)s">
                <xsd:complexType>
                    <xsd:simpleContent>
                        <xsd:extension base="%(:childType)s">
                            <xsd:attribute name="key" use="required"/>
                            %(:otherExtns)s
                        </xsd:extension>
                    </xsd:simpleContent>
                </xsd:complexType>
            </xsd:element>
        </xsd:sequence>
    </xsd:complexType>
</xsd:element>''',
                         minOccurs=minOccurs,
                         maxOccurs=maxOccurs,
                         childName=self.ChildSpec.name,
                         childType=self.ChildSpec.GetSchemaName(),
                         otherExtns=otherExtns
                         )
    
###############################################
## WriteSchematronLimitTest functions:
###############################################

@AddMethod(MemberSpec)        
def WriteStandardMemberLimitTest(self, components):
    for i in components:
        if self.min != self.StdMin:
            if self.max != self.StdMax:
                self.WriteString('''\n<sch:assert test="%(ParserName)s/@%(:compname)s &gt;= %(:min)s and %(ParserName)s/@%(:compname)s &lt;= %(:max)s">%(:compname)s must be between %(:min)s and %(:max)s</sch:assert>''', compname=i,
                min=GetXmlFloat(self.min), max=GetXmlFloat(self.max))
            else:
                self.WriteString('''\n<sch:assert test="%(ParserName)s/@%(:compname)s &gt;= %(:min)s">%(:compname)s must be &gt;= %(:min)s</sch:assert>''', compname=i, min=GetXmlFloat(self.min))
        elif self.max != self.StdMax:
            self.WriteString('''\n<sch:assert test="%(ParserName)s/@%(:compname)s &lt;= %(:max)s">%(:compname)s must be &lt;= %(:max)s</sch:assert>''', compname=i, max=GetXmlFloat(self.max))
    self.UnsetErrorInfo()

@AddMethod(DataMemberSpec)
def WriteSchematronLimitTest(self): self.WriteStandardMemberLimitTest(("value",))
    
@AddMethodToClasses(Vector2Spec, Vec2VSpec)
def WriteSchematronLimitTest(self): self.WriteStandardMemberLimitTest(("x", "y"))

@AddMethodToClasses(Vector3Spec, Vec3VSpec)
def WriteSchematronLimitTest(self): self.WriteStandardMemberLimitTest(("x", "y", "z"))

@AddMethodToClasses(Vector4Spec, Vec4VSpec)
def WriteSchematronLimitTest(self): self.WriteStandardMemberLimitTest(("x", "y", "z", "w"))
    
###############################################
## GetSchemaName functions:
###############################################
    
@AddMethod(ArraySpec)
def GetSchemaName(self):
    # choose array type based on child type
    name = self.ChildSpec.DomNode.nodeName
    if name == "string":
        return "par:StringArray"
    elif name == "float":
        return "par:FloatArray"
    elif name in ("int", "s32", "u32", "Color32"):
        return "par:IntArray"
    elif name in ("char", "s8", "u8"):
        return "par:CharArray"
    elif name in ("short", "s16", "u16"):
        return "par:ShortArray"
    elif name == "bool":
        return "par:BoolArray"
    elif name == "Vector2" or name == "Vec2V":
        return "par:Vector2Array"
    elif name == "Vector3" or name == "Vec3V":
        return "par:Vector3Array"
    elif name == "Vector4" or name == "Vec4V":
        return "par:Vector4Array"
    elif name in ["BoolV", "ScalarV", "VecBoolV"]:
        return "<!-- Error: No array type defined yet for %s -->" % name
    return self.ChildSpec.type

@AddMethod(MapSpec)
def GetSchemaName(self):
    return self.ChildSpec.type

CreateSimpleGetterFunctions("GetSchemaName", {
    BoolSpec: "par:Bool",
    EnumSpec: "par:Enum",
    FloatSpec: "par:Float",
    AngleSpec: "par:AngleFloat",
    IntSpec: "par:Int",
    Matrix33Spec: "par:Matrix33",
    Matrix34Spec: "par:Matrix34",
    Matrix44Spec: "par:Matrix44",
    StringSpec: "par:String",
    Vector2Spec: "par:Vector2",
    Vector3Spec: "par:Vector3",
    Vector4Spec: "par:Vector4",
    Vec2VSpec: "par:Vector2",
    Vec3VSpec: "par:Vector3",
    Vec4VSpec: "par:Vector4",
    VecBoolVSpec: "par:VecBoolV",
    }
)

@AddMethod(StructSpec) 
def GetSchemaName(self):
    genName = self.type
    if genName.startswith("::"):
        genName = genName[2:]
    genName = genName.replace("::", "__")

    return genName

@AddMethod(EnumSpec)
def GetSchemaName(self):
    genName = self.type
    if genName.startswith("::"):
        genName = genName[2:]
    genName = genName.replace("::", "__")

    return genName
