
import sys
from xml.dom.minidom import *
from structtype import *
from basetype import *

class ArraySpec(MemberSpec):
    def __init__(self, domNode, parentObj):
        self.type = None
        self.size = None
        self.sizeVar = None
        self.indexBits = None
        self.onVirtualRead = None
        self.addGroupWidget = None
        self.align = None
        self.heap = None
    
        MemberSpec.__init__(self, domNode, parentObj)
        
        # make sure all the children have a name="Item" tag. name is normally treated as a required attribute, but its not required in this context,
        # so its easier just to set it here than mess around with telling when it's allowed or not
        for node in domNode.childNodes:
            if node.nodeType == xml.dom.Node.ELEMENT_NODE:
                node.setAttribute("name", "Item")
                
        self.Children = BuildSpecObjects(domNode, self)

        if len(self.Children) != 1:
            print "Error: Array \"%s\" must have exactly one child." % (self.CppName)
            sys.exit(1)
        self.ChildSpec = self.Children[0];
        if not isinstance(self.ChildSpec, MemberSpec) and not isinstance(self.ChildSpec, AssertSpec) and not isinstance(self.ChildSpec, PadSpec):
            CodeGenError("Found a node that can't be inside an <array>: <%s>." % self.ChildSpec.DomNode.nodeName, wikiPage=self.WikiPage)
    
    def CheckAttributes(self):
        self.WikiPage="Array_PSC_tag"
        MemberSpec.CheckAttributes(self)
        self.RequiredAttribute("type", ["atArray", "atFixedArray", "atRangeArray", "pointer", "member", "virtual"], safeVal="atArray")

        # some types require a size attribute
        if self.type in ["member", "atRangeArray", "atFixedArray"]:
            self.RequiredAttribute("size", safeVal="0")
        else:
            self.OptionalAttribute("size", "0")
            
        if self.size:
            ValidateIntLiteralOrKnownConst(self.size)

        if self.type in ["pointer", "atArray"]:
            self.OptionalAttribute("align", "0", ["0", "1", "2", "4", "8", "16", "32", "64", "128", "256", "512", "1024", "2048", "4096", "8192", "16384", "32768"]);
            self.OptionalAttribute("heap", "virtual", ["virtual", "physical"])
            
        if self.type == "pointer":
            self.OptionalAttribute("sizeVar")
            if not self.sizeVar and not self.size:
                CodeGenError("<array> element with type=\"pointer\" needs to have a size= or sizeVar= attribute.", wikiPage=self.WikiPage)

        if self.type == "atArray":
            self.OptionalAttribute("indexBits", "16", ["16", "32"])
            
        # virtual arrays need a callback specified
        if self.type == 'virtual':
            self.RequiredAttribute('onVirtualRead', safeVal="")

        self.OptionalAttribute("addGroupWidget", "true")
    
    # If this is a multidimensional array, checks the ultimate structure stored in the array
    def IsArrayOfStructures(self):
        spec = self.FindDataSpec()
        if isinstance(spec, StructSpec):
            return True
        if isinstance(spec, PointerSpec) and spec.policy in ["owner", "simple_owner"]:
            return True
        return False

    def DepthOfArrays(self, depth=1):
        if isinstance(self.ChildSpec, ArraySpec):
            return self.ChildSpec.DepthOfArrays(depth+1)
        return depth

    def FindDataSpec(self):
        if isinstance(self.ChildSpec, ArraySpec):
            return self.ChildSpec.FindDataSpec()
        return self.ChildSpec
