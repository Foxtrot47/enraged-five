
import re
import sys
import xml.dom
import atHash
from xml.sax.saxutils import *

SpecTypeMap = {}

CurrFileName = ""
CurrLineNumber = -1
CurrStructdefID = -1
CurrStructdefName = ""
CurrMemberID = 0
CurrMemberName = ""
CurrEnumdefID = -1
CurrEnumdefName = ""

FoundErrors = 0

def FormatErrorMessage(errType, errorStr, addToFile=True, wikiPage=""):
    global CurrFileName, CurrLineNumber, CurrStructdefID, CurrStructdefName, CurrMemberID, CurrMemberName, FoundErrors
    linenumber = CurrLineNumber
    if linenumber < 0:
        linenumber = 0
        
    seeAlsoStr = ""
    if wikiPage:
        seeAlsoStr = " See https://devstar.rockstargames.com/wiki/index.php/%s" % (wikiPage)

    currStruct = ""
    if CurrStructdefID > 0:
        if CurrStructdefName == "":
            currStruct = "in structdef %d" % (CurrStructdefID)
        else:
            currStruct = "in structdef '%s'" % (CurrStructdefName)

    currMember = ""
    if CurrMemberID > 0:
        if CurrMemberName == "":
            currMember = ", member %d" % (CurrMemberID)
        else:
            currMember = ", member '%s'" % (CurrMemberName)
            
    if CurrEnumdefID > 0:
        if CurrEnumdefName == "":
            currStruct = "in enumdef %d" % (CurrEnumdefID)
        else:
            currStruct = "in enumdef '%s'" % (CurrEnumdefName)

    errLoc = currStruct + currMember
    if errLoc != "":
        errLoc += ": "

    sys.stderr.write("%s (%d): %s: %s%s%s\n" % (CurrFileName, CurrLineNumber, errType, errLoc, errorStr, seeAlsoStr))
    if addToFile:
        GenericSpec.OutFile.write("""

///////////////////////////////////////////////
CompileTimeAssert(0 && \"Fix the %s below\");
// %s (%d):
// %s%s
///////////////////////////////////////////////

"""    % (errType, CurrFileName, CurrLineNumber, errLoc, errorStr));
        FoundErrors += 1

def SetEnumdefNameAndID(name, id):
    global CurrEnumdefName, CurrEnumdefID
    global CurrStructdefName, CurrStructdefID
    global CurrMemberName, CurrMemberID
    CurrEnumdefName = name
    CurrEnumdefID = id
    CurrStructdefName = ""
    CurrStructdefID = -1
    CurrMemberName = ""
    CurrMemberID = -1
        
def SetStructNameAndID(name, id):
    global CurrStructdefName, CurrStructdefID
    global CurrEnumdefName, CurrEnumdefID
    CurrStructdefName = name
    CurrStructdefID = id
    CurrEnumdefName = ""
    CurrEnumdefID = -1

def SetMemberNameAndID(name, id):
    global CurrMemberName, CurrMemberID
    global CurrEnumdefName, CurrEnumdefID
    CurrMemberName = name
    CurrMemberID = id
    CurrEnumdefName = ""
    CurrEnumdefID = -1

def GetCurrStructID():
    global CurrStructdefID
    return CurrStructdefID

def GetCurrMemberID():
    global CurrMemberID
    return CurrMemberID
            
def CodeGenFatalError(errorStr, **kw):
    CodeGenError(errorStr, **kw)
    sys.exit(1)
    
def CodeGenError(errorStr, **kw):
    FormatErrorMessage("error", errorStr, **kw)

def CodeGenWarning(warnStr, **kw):
    FormatErrorMessage("warning", warnStr, **kw)
	
def CodeGenInfo(infoStr, **kw):
	FormatErrorMessage("info", infoStr, False, **kw)

def MangleCppName(name):
    if name == None:
        return ""
    newName = name
    if newName.startswith("::"):
        newName = newName[2:]
    newName = newName.replace("::", "__")
    newName = newName.replace("<", "_t_")
    newName = newName.replace(">", "")
    return newName

# Adds a type to the SpecTypeMap (for creation from XML)
# The coreType is the name in the tag, e.g. "<float>"
# the subType is the name on a type= attribute, e.g. type="angle"
# (subtype can be '' if the type attribute isn't used this way)
def AddType(className, coreType, subType):
    if not SpecTypeMap.has_key(coreType):
        SpecTypeMap[coreType] = {}
    SpecTypeMap[coreType][subType] = className

# Converts XML data into one of the Spec objects using the SpecTypeMap to map names (and
# optional type= attributes) to python object types.
def BuildSpecObjects(domNode, parent):
    # select just elements (ignore all text, comments, etc)
    elements = [x for x in domNode.childNodes if x.nodeType == xml.dom.Node.ELEMENT_NODE]
    specs = []
    for elem in elements:
        typeName = elem.tagName
        subTypeName = ""
        try:
            subTypeName = elem.getAttribute('type')
        except KeyError:
            # no type= attribute
            pass

        # from the type, find the map of subtypes
        subTypeMap = {}
        try:
            subTypeMap = SpecTypeMap[typeName]
        except KeyError:
            CodeGenError("Don't know what to do with element named '%s'." % (typeName), wikiPage="Category:ParserSchema")

        # from the subtype, find the type of python object to create
        # if using the type= attribute didn't work try again with
        # empty string
        specClass = False
        try:
            specClass = subTypeMap[subTypeName]
        except KeyError:
            # try again without type
            try:
                specClass = subTypeMap['']
            except:
                CodeGenError("Don't know what to do with an element named '%s' with type '%s'" % (typeName, subTypeName), wikiPage="Category:ParserSchema")

        # create a new instance of the class in specClass
        if specClass:
            newObj = specClass(elem, parent)
            specs.append(newObj)
        else:
            CodeGenError("Don't know what to do with an element named '%s' with type '%s'." % (typeName, subTypeName), wikiPage="Category:ParserSchema")

    return specs

IsFloatRE = re.compile(r"^[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?$")
IsIntRE = re.compile(r"^[-+]?[0-9]+$")

def IsBareFloat(str):
    return IsFloatRE.match(str)
    
# XML floats do not end in 'f', strip it if it exists
def GetXmlFloat(str):
    if str[-1] == "f" and IsBareFloat(str[:-1]):
        return str[:-1]
    return str
    
# C++ floats should end in 'f', add it if it doesn't exist
def GetCppFloat(str):
    if IsBareFloat(str):
        if IsIntRE.match(str):
            return str + ".f"
        return str + "f"
    return str

def GetCppFloatList(str):
    return [GetCppFloat(x.strip()) for x in str.split(",")]


# Code for managing a table of known constants
ConstTable = {}

def AddKnownConstant(name, val):
    ConstTable[name] = val

def HasKnownConstant(str):
    return ConstTable.has_key(str)
    
def ResetConstTable():
    ConstTable = {}

def IsIntLiteral(str):
    try:
        int(str, 0)
        return True
    except:
        return False

def ValidateIntLiteralOrKnownConst(str):
    if not IsIntLiteral(str) and not HasKnownConstant(str):
        CodeGenInfo("'%s' is not an integer literal and not declared as a <const>. This may cause problems for offline processing" % str)
        AddKnownConstant(str, None) # add it to the table so we only see this message once

        
# General convention for member variables: 
# self.foo comes from an xml attribute foo="bar"
# self.Foo (uppercase) is a computed or internal value
class GenericSpec(object):
    OutFile = sys.stdout
    Depth = 0
    CurrDocumentSpec = None
    
    def __init__(self, domNode, parentObj):
        self.DomNode = domNode
        
        if domNode:
            attrList = [domNode.attributes.item(x) for x in range(domNode.attributes.length)]
        else:
            attrList = []
            
        self.WikiPage = None
            
        self.Parent = parentObj
        self.Attrs = dict([(x.name,x.nodeValue) for x in attrList]) # a list of all of the attributes
        self.ExtraAttrs = self.Attrs.copy()                           # a list of the extra attributes, not counting the required / optional ones. This should just be ones that start with 'x'.
        self.CheckAttributes()
        self.CheckForUnusedAttrs()
    
    # Derived classes should override this, and call RequiredAttribute and OptionalAttribute to list all of the attributes they will need
    # Any attributes they use also need to be previously declared in the __init__ function. For example to add an 'outputType' attribute
    # def __init__(self, domNode, parentObj):
    #   self.outputType = "ascii" # declare it here
    #   BaseType.__init__(self, domNode, parentObj)
    # def CheckAttributes(self):
    #   self.OptionalAttribute("outputType", defval="ascii", options=["ascii", "utf8", "utf16"]); # define it here
    #   BaseType.CheckAttributes(self);
    def CheckAttributes(self):
        pass

    def RequiredAttribute(self, attr, options=None, safeVal=None):
        attrVal = self.ExtraAttrs.get(attr, safeVal)
            
        if not hasattr(self, attr):
            CodeGenError("XML Attribute %s hasn't been declared for type %s." % (attr, self.__class__.__name__), wikiPage=self.WikiPage)
        elif not self.ExtraAttrs.has_key(attr):
            CodeGenError("Node '%s' missing required attribute '%s'." % (self.DomNode.nodeName, attr), wikiPage=self.WikiPage)
        elif options and not self.ExtraAttrs[attr] in options:
            CodeGenError("Node '%s', attribute '%s' should have one of the following values: '%s'." % (self.DomNode.nodeName, attr, "', '".join(options)), wikiPage=self.WikiPage)
            attrVal = safeVal
            
        setattr(self, attr, attrVal)
        if self.ExtraAttrs.has_key(attr):
            del(self.ExtraAttrs[attr])

    def OptionalAttribute(self, attr, defval=None, options=None):
        if not hasattr(self, attr):
            CodeGenError("XML Attribute %s hasn't been declared for type %s." % (attr, self.__class__.__name__), wikiPage=self.WikiPage)
        # if the attribute is not present, or if its present but has previously been overridden, assign a new value
        if options and self.ExtraAttrs.has_key(attr) and self.ExtraAttrs[attr] not in options:
            CodeGenError("Node '%s', optional attribute '%s' should have one of the following values: '%s'." % (self.DomNode.nodeName, attr, "', '".join(options)), wikiPage=self.WikiPage)
            
        attrVal = self.ExtraAttrs.get(attr, defval)
            
        setattr(self, attr, attrVal)
        if self.ExtraAttrs.has_key(attr):
            del(self.ExtraAttrs[attr])

    def CheckForUnusedAttrs(self):
        keysCpy = self.ExtraAttrs.keys()[:]
        for i in keysCpy:
            if i[0] != 'x' and i[0:2] != "ui": # attributes that start with 'x' or "ui" are extra
                CodeGenWarning("Attribute '%s' isn't being used in node '%s'" % (i, self.DomNode.nodeName), wikiPage=self.WikiPage)
            if i[0] != 'x':
                del(self.ExtraAttrs[i])
        
    def HasSpecialLimits(self):
        return 0

    # WriteString does some special argument handling for more convenient template substitution
    # %(foo)s will write member var 'foo' as a string (and remember the convention is lower case = straight from XML, upper case = derived properties)
    # %(:foo)s will write the keyword argument foo=bar that was passed into the call as a string
    def WriteString(self, str, **kwds):
        tempAttrs = self.__dict__.copy()
        for (key, value) in kwds.items():
            tempAttrs[":" + key] = value
        # for debugging depth leaks
        # newStr = ("\n" + "/*gs%d*/" % GenericSpec.Depth + "    " * GenericSpec.Depth).join(str.split("\n"))
        newStr = ("\n" + "    " * GenericSpec.Depth).join(str.split("\n"))
        GenericSpec.OutFile.write(newStr % tempAttrs)

    def FindParStructureType(self):
        return self.FindStructdefSpec().CppType

    def FindStructdefSpec(self):
        if self.Parent:
            return self.Parent.FindStructdefSpec()
        else:
            return self
    

class PadSpec(GenericSpec):
    def __init__(self, domNode, parentObj):
        self.bytes = ""
        self.platform = ""
        super(PadSpec, self).__init__(domNode, parentObj)
    
    def CheckAttributes(self):
        self.WikiPage = "Pad_PSC_tag"
        GenericSpec.CheckAttributes(self)
        self.RequiredAttribute("bytes", safeVal="0")
        self.OptionalAttribute("platform", "")
        
        if self.Parent.generate and "class" in self.Parent.generate:
            CodeGenError("<pad> can't be used to add padding to generated classes, only to declare padding for existing C++ classes")
    
class AssertSpec(GenericSpec):
    def __init__(self, *args, **kwargs):
        self.test = ""
        self.message = ""
        super(AssertSpec, self).__init__(*args, **kwargs)
        
    def CheckAttributes(self):
        self.WikiPage = "Assert_PSC_tag"
        GenericSpec.CheckAttributes(self)
        self.RequiredAttribute("test", safeVal="")
        self.OptionalAttribute("message", "")

        testString = self.test
        testString = re.sub(r"(?<!\w)m_(\w+)", r"\1", self.test)
        self.test = quoteattr(testString)

        self.message = escape(self.message)

class ReportSpec(AssertSpec):
    pass

startsWithXmlRE = re.compile(r"\A(X|x)(M|m)(L|l)")
validXmlNameRE = re.compile(r"\A[A-Za-z:_][A-Za-z:0-9._-]*\Z")

def IsValidXmlElementName(s):
    global validXmlNameRE, startsWithXmlRE
    if startsWithXmlRE.match(s):
        return False
    if validXmlNameRE.match(s):
        return True
    return False

def MangleName(origName):
    if origName == None:
        return ""
    if origName.startswith("m_"):
        s = origName[2:]
        if IsValidXmlElementName(s):
            return s
        else:
            CodeGenInfo("Couldn't strip the 'm_' from '%s' - doing so would create an invalid XML name" % origName)
    if IsValidXmlElementName(origName):
        return origName
    # OK don't do any real mangling for now - just error about it.
    CodeGenError("Can't use '%s' as a name, because it's not a valid XML element name (can't begin with 'xml', can't start with [0-9.-], and only punctuation allowed is [:._-]" % origName)
    return ""
	
class MemberSpec(GenericSpec):
    def __init__(self, domNode, parentObj):
        self.MemberID = GetCurrMemberID()
        self.MemberID += 1
        name = ""
        if domNode and domNode.attributes.has_key("name"):
            name = domNode.getAttribute("name")
        SetMemberNameAndID(name, self.MemberID)
        if name == "" and not isinstance(parentObj, MemberSpec): # only way members can be unnamed is if they are part of another member
            CodeGenError("Missing attribute \"name\"")
        
        self.name = None
        self.description = None
        self.hideWidgets = None
        self.noInit = None
        self.hideFrom = None
        self.onWidgetChanged = None # only some subclasses actually use this, but its a bunch of them, so just declare it here
        self.platform = None
        self.parName = None
        
        self.CppName = None
        self.NameHash = 0
        
        super(MemberSpec, self).__init__(domNode, parentObj)
        
        if self.parName:
            self.CppName = self.name
            if IsValidXmlElementName(self.parName):
                self.ParserName = self.parName
            else:
                CodeGenError("Can't use '%s' as a name, because it's not a valid XML element name (can't begin with 'xml', can't start with [0-9.-], and only punctuation allowed is [:._-]" % self.parName)
                self.ParserName = ""
        else:
            self.CppName = self.name
            self.ParserName = MangleName(self.name)
        self.NameHash = atHash.atLiteralStringHash(self.ParserName)
        
    def SetErrorInfo(self):
        SetMemberNameAndID(self.name, self.MemberID)

    def UnsetErrorInfo(self):
        SetMemberNameAndID("", -1)

    def CheckAttributes(self):
        self.SetErrorInfo()
        GenericSpec.CheckAttributes(self)
        self.RequiredAttribute("name", safeVal="") # name may be implicit, for array members and map values, but treat it as required. Those nodes can add a name attr to the dom before processing.
        self.SetErrorInfo()
        self.OptionalAttribute("description", "")
        self.OptionalAttribute("hideWidgets", "false")
        self.OptionalAttribute("noInit", "false")
        self.OptionalAttribute("hideFrom")
        self.OptionalAttribute("platform")
        self.OptionalAttribute("parName")
        
        if isinstance(self.Parent, MemberSpec) and self.platform:
            CodeGenError("Can't add a platform attribute to a nested member", wikiPage=self.WikiPage)

    def HasStandardMemberCallbacks(self):
        return self.onWidgetChanged != None


class DataMemberSpec(MemberSpec):
    StdMin = "-FLT_MAX/1000.0f"
    StdMax = "+FLT_MAX/1000.0f"

    def __init__(self, *args, **kwargs):
        self.min = None
        self.max = None
        self.onWidgetChanged = None
        self.type = None
    
        super(DataMemberSpec, self).__init__(*args, **kwargs)
        
    def CheckAttributes(self):
        MemberSpec.CheckAttributes(self)
        self.OptionalAttribute("min", self.StdMin)
        self.OptionalAttribute("max", self.StdMax)
        self.OptionalAttribute("onWidgetChanged")
        self.OptionalAttribute("type")

    def HasSpecialLimits(self):
        return (self.min != self.StdMin or self.max != self.StdMax)


