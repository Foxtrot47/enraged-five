from basetype import *

from atHash import *

class EnumSpec(MemberSpec):
    def __init__(self, *args, **kwargs):
        self.init = None
        self.type = None
        self.onWidgetChanged = None
        self.size = None
        super(EnumSpec, self).__init__(*args, **kwargs)
        
    def CheckAttributes(self):
        self.WikiPage = "Enum_PSC_tag"
        MemberSpec.CheckAttributes(self)
        self.OptionalAttribute("init", "0")
        self.RequiredAttribute("type", safeVal="")
        self.OptionalAttribute("onWidgetChanged")
        self.OptionalAttribute("size", "32", ["8", "16", "32"])
    
        self.EnumDefType = self.type

            
class BitsetSpec(EnumSpec):
    def __init__(self, *args, **kwargs):
        self.values = None
        self.numBits = None
        super(BitsetSpec, self).__init__(*args, **kwargs)

    def CheckAttributes(self):
        self.WikiPage = "Bitset_PSC_tag"
        MemberSpec.CheckAttributes(self)
        self.OptionalAttribute("init", "0")
        self.RequiredAttribute("type", ["fixed", "fixed32", "fixed16", "fixed8", "atBitSet", "generated"], safeVal="fixed")
        self.OptionalAttribute("values")
        self.OptionalAttribute("onWidgetChanged")
        self.OptionalAttribute("numBits")
        
        # all the base class (enum) code expects 'type' to refer to
        # the possible enum values, but for bitsets type should be the type of bitset, and 'values'
        # should be where it gets the bit values from. 
        # so use more clear names internally
        self.StorageType = self.type
        self.EnumDefType = self.values
        
        if self.type == 'generated':
            if not self.values:
                CodeGenError('In bitset %s, a values="" attribute is required when using type="generated"' % self.name, wikiPage=self.WikiPage)
            if self.numBits:
                CodeGenError('In bitset %s, numBits attribute is not allowed when using type="generated"' % self.name, wikiPage=self.WikiPage)
            self.StorageType = self.values + "BitSet"
            self.numBits = self.values + "_MAX_VALUE + 1"
        
        if self.StorageType == 'fixed':
            self.StorageType = "fixed32"
        
        if self.numBits == "use_values":
            if not self.values:
                CodeGenError('In bitset %s, numBits="use_values" but there was no values attributes' % self.name, wikiPage=self.WikiPage)
            self.numBits = self.values + "_MAX_VALUE + 1"
        
        if self.type == "atBitSet":
            self.numBits = "0"
            
        if not self.numBits:
            numBitsDefaultMap = {
                "fixed32": "32",
                "fixed16": "16",
                "fixed8": "8",
            }
            self.numBits = numBitsDefaultMap[self.StorageType]

            
    def GetNumBits(self):
        return self.numBits
            
        

class EnumValSpec(GenericSpec):
    def __init__(self, *args, **kwargs):
        self.name = None
        self.value = None
        self.description = None
        self.hideFrom = None
        super(EnumValSpec, self).__init__(*args, **kwargs)

    def CheckAttributes(self):
        self.WikiPage = "Enumval_PSC_tag"
        GenericSpec.CheckAttributes(self)
        self.RequiredAttribute("name", safeVal="")
        self.OptionalAttribute("value")
        self.OptionalAttribute("description", "")
        self.OptionalAttribute("hideFrom")

        

class EnumDefSpec(GenericSpec):
    def __init__(self, domNode, parentObj):
        self.type = None
        self.generate = None
        self.values = None
        self.platform = None
        self.preserveNames = None
        self.scriptName = None
        self.autoregister = None
    
        enumdefName = ""
        if domNode.attributes.has_key("type"):
            enumdefName = domNode.getAttribute("type")
            
        SetEnumdefNameAndID(enumdefName, 1)
        
        GenericSpec.__init__(self, domNode, parentObj)
        
        self.Children = BuildSpecObjects(domNode, self)

        if self.generate and "bitset" in self.generate and "class" not in self.generate and GenericSpec.CurrDocumentSpec.generate:
            self.generate = self.generate + " " + GenericSpec.CurrDocumentSpec.generate
            if "class" not in self.generate:
                CodeGenError("<enumdef> element \"" + i.name + "\" missing \"class\" from generate parameters (trying to generate bitset)", wikiPage=self.WikiPage)

        # set the values for any enum that doesn't have one set explicitly
        enumVal = -1 # start at -1 so the first unnamed value gets incremented to '0'
        if self.generate and "bitset" in self.generate:
            if self.values != "incr":
                CodeGenError("When using generate=\"bitset\", only values=\"incr\" is allowed", wikiPage=self.WikiPage)
            else:
                # if used as index into a bitset then all the values must be contiguous
                for i in self.Children:
                    enumVal += 1
                    if i.value:
                        if (int(i.value) != enumVal):
                            CodeGenError("<enumdef> element \"" + i.name + "\" has an out of order value (needs to be in order for generate=\"bitset\")", wikiPage=self.WikiPage)
                    else:
                        i.value = str(enumVal)
        else:
            lastNamedEnum = None # None, or the value of the last enum that had an explicit value, iff the value was a string not an integer
            for i in self.Children:
                if i.value:
                    try:
                        enumVal = int(i.value)
                        lastNamedEnum = None
                    except ValueError:
                        # means the value wasn't an int, probably a string that names some other enum, so we need to build an enum expression instead (ugh)
                        enumVal = 0
                        lastNamedEnum = i.value
                else:
                    if self.values == "incr":
                        enumVal += 1
                        if lastNamedEnum:
                            i.value = "(%s + %d)" % (lastNamedEnum, enumVal) # complex case... value = MYENUM + 3
                        else:
                            i.value = str(enumVal)                          # simple case value = 3
                    elif self.values == "hash":
                        i.value = "0x%08x" % atStringHash(i.name)
                    elif self.values == "literalhash":
                        i.value = "0x%08x" % atLiteralStringHash(i.name)
                    elif self.values == "atHash16":
                        i.value = "0x%04x" % atHash16(i.name)
                    elif self.values == "atHash16U":
                        i.value = "0x%04x" % atHash16U(i.name)
                        
        SetEnumdefNameAndID("", -1)

    def CheckAttributes(self):
        self.WikiPage = "Enumdef_PSC_tag"
        GenericSpec.CheckAttributes(self)
        self.RequiredAttribute("type", safeVal="")
        self.OptionalAttribute("generate", GenericSpec.CurrDocumentSpec.generate)
        self.OptionalAttribute("values", "incr", ["incr", "hash", "literalhash", "atHash16", "atHash16U"])
        self.OptionalAttribute("platform")
        self.OptionalAttribute("preserveNames", "false", ["true", "false"])
        self.OptionalAttribute("scriptName", "")
        self.OptionalAttribute("autoregister", "true", ["true", "false"])

        self.ParserName = MangleCppName(self.type)
        self.CppType = self.type 
        self.NameHash = atLiteralStringHash(self.ParserName)
