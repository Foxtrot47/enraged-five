
# Support functions for output templates

def AddMethod(className, methodName=None):
    '''
    A decorator for adding a method to another class. Use like so:
    @AddMethod(MyClass, newMethod)
    def foo() ...
    
    And then you can do this:
    x = MyClass()
    x.newMethod()  # calls foo()
    
    If the new method name is omitted we use the same name as the function. So:
    @AddMethod(MyClass)
    def foo() ...
    
    will add a MyClass.foo() method that calls foo()
    '''
    def RealDecorator(fn):
        if methodName:
            setattr(className, methodName, fn)
        else:
            setattr(className, fn.__name__, fn)
        return fn
    return RealDecorator
    
def AddMethodToClasses(*classNames):
    '''
    A decorator for adding a method to a bunch of classes. Use like so:
    @AddMethod(MyClass, MyOtherClass, MyThirdClass)
    def foo() ...
    
    will add a MyClass.foo(), MyOtherClass.foo(), and MyThirdClass.foo() that all call foo()
    '''
    def RealDecorator(fn):
        for className in classNames:
            setattr(className, fn.__name__, fn)
        return fn
    return RealDecorator
    
    
def CreateSimpleGetterFunctions(methodName, table):
    for (cls, retval) in table.iteritems():
        def DummyFn(v): # this function is only necessary to introduce a new environment for closure - otherwise if we just used 'retval' all the generated functions point to the same value (which ends up as the last list element)
            def Getter(self, **kw):
                return v
            setattr(cls, methodName, Getter)
        DummyFn(retval)
