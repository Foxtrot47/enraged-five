from basetype import *

class MatrixSpec(DataMemberSpec):
    def __init__(self, *args, **kwargs):
        self.step = None
        self.init = None
        self.highPrecision = None
        super(MatrixSpec, self).__init__(*args, **kwargs)

    def CheckAttributes(self):
        self.WikiPage = "Matrix_PSC_tag"
        DataMemberSpec.CheckAttributes(self)
        self.OptionalAttribute("step", "0.1f")
        self.OptionalAttribute("init", "1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f")
        self.OptionalAttribute("highPrecision", "false")

class Matrix33Spec(MatrixSpec):
    StdMin = "-FLT_MAX/1000.0f"
    StdMax = "+FLT_MAX/1000.0f"

    def CheckAttributes(self):
        MatrixSpec.CheckAttributes(self)
        if self.init:
            # make sure there are 16 elements in the init string, adjusted for 4 rows of 3
            initStr = self.init
            elems = initStr.split(",")
            if len(elems) == 1:
                # copy to all members
                elems = [elems[0], elems[0], elems[0], "0.0f", elems[0], elems[0], elems[0], "0.0f", elems[0], elems[0], elems[0], "0.0f", "0.0f", "0.0f", "0.0f", "0.0f"]            
            else:
                # insert zeroes in the 4th column because our storage structure is set up for a 4x4 matrix
                if len(elems) <= 9:
                    if len(elems) > 3:
                        elems.insert(3,"0.0f")
                    if len(elems) > 7:
                        elems.insert(7,"0.0f")
                while(len(elems) < 16):
                    elems.append("0.0f")
            self.init = ",".join(elems)

class Matrix34Spec(MatrixSpec):
    StdMin = "-FLT_MAX/1000.0f"
    StdMax = "+FLT_MAX/1000.0f"

    def CheckAttributes(self):
        MatrixSpec.CheckAttributes(self)
        if self.init:
            # make sure there are 16 elements in the init string, adjusted for 4 rows of 3
            initStr = self.init
            elems = initStr.split(",")
            if len(elems) == 1:
                # copy to all members
                elems = [elems[0], elems[0], elems[0], "0.0f", elems[0], elems[0], elems[0], "0.0f", elems[0], elems[0], elems[0], "0.0f", elems[0], elems[0], elems[0], "0.0f"]            
            else:
                # insert zeroes in the 4th column because our storage structure is set up for a 4x4 matrix
                if len(elems) <= 12:
                    if len(elems) > 3:
                        elems.insert(3,"0.0f")
                    if len(elems) > 7:
                        elems.insert(7,"0.0f")
                    if len(elems) > 11:
                        elems.insert(11,"0.0f")					
                while(len(elems) < 16):
                    elems.append("0.0f")
            self.init = ",".join(elems)

class Matrix44Spec(MatrixSpec):
    StdMin = "-FLT_MAX/1000.0f"
    StdMax = "+FLT_MAX/1000.0f"
    
    def CheckAttributes(self):
        MatrixSpec.CheckAttributes(self)
        if self.init:
            # make sure there are 16 elements in the init string
            initStr = self.init
            elems = initStr.split(",")
            if len(elems) == 1:
                # copy to all members
                elems = [elems[0], elems[0], elems[0], elems[0], elems[0], elems[0], elems[0], elems[0], elems[0], elems[0], elems[0], elems[0], elems[0], elems[0], elems[0], elems[0]]            
            else:                
                while(len(elems) < 16):
                    elems.append("0.0f")
            self.init = ",".join(elems)

class Mat33VSpec(Matrix33Spec):
    pass
        
class Mat34VSpec(Matrix34Spec):
    pass
    
class Mat44VSpec(Matrix44Spec):
    pass

