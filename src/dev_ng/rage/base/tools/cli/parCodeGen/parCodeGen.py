#!/usr/bin/python


import xml.dom.minidom
import filecmp
import os
import os.path
import optparse
import subprocess
from stat import *
import StringIO
import sys
import tempfile
import time
import win32file, win32api, win32con
import xml.parsers.expat

from datatypes import *
import basetype
import extractComments
import template_xmlschema
import template_cdefnheader
import template_cdefncode
import template_runtimemetadata
import template_schfile

StructDictTemplateStart = """
<rageStructureDictionary>
    <!-- parCodeGen will modify all the structdef elements and anything inside those elements -->
    <!-- It will not modify other elements, so project specific data can go in here as well -->
"""

StructDictTemplateEnd = """
</rageStructureDictionary>
"""

StructDictTemplate = StructDictTemplateStart + StructDictTemplateEnd

# quick helper fn from http://stackoverflow.com/questions/5574702/how-to-print-to-stderr-in-python
def print_err(*args):
    sys.stderr.write(' '.join(map(str,args)) + '\n')

def PerforceCheckout(filename):
    cwd = os.getcwd()
    os.chdir(os.path.dirname(filename))
    command = "p4 edit " + filename
    print command
    result = subprocess.call(command) == 0
    os.chdir(cwd)
    return result
    
def PerforceAdd(filename):
    cwd = os.getcwd()
    os.chdir(os.path.dirname(filename))
    command = "p4 add " + filename
    print command
    result = subprocess.call(command) == 0
    os.chdir(cwd)
    return result
    
def CreateTempfile(basename):
    (dir, file) = os.path.split(basename)
    (newFileFd, newFileName) = tempfile.mkstemp(prefix=file, dir=dir, text=True)
    return (os.fdopen(newFileFd, "w"), newFileName)

def PrintRenameError(oldName, newName):
    print_err( "error: While renaming %s to %s\n" % (oldName, newName) )
    err = win32api.GetLastError()
    print_err( "error: Windows Error: %d %s\n" % (err, win32api.FormatMessage(err)))
    print_err( "error: Is %s checked out in perforce?" % newName)
    
def AtomicRenameCore(oldName, newName):
    try:
        MOVEFILE_WRITE_THROUGH = 0x8
        win32api.MoveFileEx(oldName, newName, win32con.MOVEFILE_REPLACE_EXISTING | MOVEFILE_WRITE_THROUGH)
#        REPLACEFILE_IGNORE_ACL_ERRORS = 0x4
#        win32file.ReplaceFile(newName, oldName, None, win32file.REPLACEFILE_WRITE_THROUGH | win32file.REPLACEFILE_IGNORE_MERGE_ERRORS | REPLACEFILE_IGNORE_ACL_ERRORS)
    except Exception, e:
        print_err(e)
        PrintRenameError(oldName, newName)
        return False
    return True
        
def AtomicRename(oldName, newName):
    if not os.path.exists(newName) or not AreFilesIdentical(oldName, newName):
        for i in xrange(1, 10):
            if AtomicRenameCore(oldName, newName):
                return
            print_err("Attempt %d failed... waiting 2s and trying again" % (i))
            time.sleep(2)

def AreFilesIdentical(file1, file2):
    return filecmp.cmp(file1, file2, False)
        
g_UseAtomicRenaming = True
        
def StartOutputFile(basename, usePerforce=False):
    if g_UseAtomicRenaming:# or usePerforce:
        return CreateTempfile(basename)
    else:
        return (file(basename, "w"), basename)
        
def EndOutputFile(fileobj, outname, basename, usePerforce=False):
    fileobj.close()
    #if usePerforce:
        #fileExists = os.path.exists(basename)
        #if fileExists and not AreFilesIdentical(outname, basename):
            #if not PerforceCheckout(basename):
                #print_err("Failed to checkout %s" % (basename))
                #sys.exit(1)
        #AtomicRename(outname, basename)
        #if not fileExists:
            #if not PerforceAdd(basename):
                #print_err("Failed to add %s" % (basename))
                #sys.exit(1)
    if g_UseAtomicRenaming:
        AtomicRename(outname, basename)
        
def fileTime(name):
    st = {}
    try:
        st = os.stat(name)
    except:
        return 0.0
    return st[ST_MTIME]

class ImportError(IOError):
    pass
    
def ProcessImportStatements(docRoot):
    nodeList = docRoot.getElementsByTagName("import")
    for nodeNum in range(len(nodeList)):
        node = nodeList[nodeNum]
        importDocName = node.getAttribute("href")
        importDocName = os.path.expandvars(importDocName)
        if os.path.exists(importDocName):
            importDocFile = open(importDocName)
            importDoc = xml.dom.minidom.parseString(importDocFile.read())

            # recurse and process any imports
            oldDir = os.getcwd()
            os.chdir(os.path.dirname(os.path.abspath(importDocName)))
            ProcessImportStatements(importDoc) 
            os.chdir(oldDir)

            schemas = importDoc.getElementsByTagName("ParserSchema")
            if len(schemas) != 1:
                raise ImportError("Bad import file - expected one <ParserSchema> block")
            for impnode in schemas[0].childNodes:
                node.parentNode.insertBefore(impnode.cloneNode(True), node)
            node.parentNode.removeChild(node)
        else:
            raise ImportError("Couldn't import file " + importDocName)
    
def FindXmlBlocks(filename, verbose=False, validate=False, metadataFileName=None, onlyExtractFromMarkedBlocks=False):
    inputFile = open(filename)
    inputData = inputFile.read()
    
    isPsc = False
        
    if os.path.splitext(filename)[1] == ".psc":
        fullXmlString = inputData
        isPsc = True
    else:
        # join the blocks, wrap them in a top level tag so we can read them as an xml file
        fullXmlString = "<ParserSchema>" + extractComments.extractAll(inputData, filename, onlyExtractFromMarkedBlocks) + "</ParserSchema>\n"

    try:
        doc = xml.dom.minidom.parseString(fullXmlString)
        # find the first element node (skip comments)
        docRoot = None
        for node in doc.childNodes:
            if node.nodeType == xml.dom.Node.ELEMENT_NODE:
                docRoot = node
                break
        
        # if we're supposed to save out the metadata and NOT validate, save before processing the imports
        if metadataFileName:
            (tempFile, tempfilename) = CreateTempfile(metadataFileName)
            # add a comment about how this is autogenerated code
            comment = doc.createComment("This file was autogenerated by parCodeGen. DO NOT HAND EDIT IT!")
            doc.insertBefore(comment, doc.childNodes[0])
            doc.writexml(tempFile)
            doc.removeChild(comment)
            tempFile.close()
            
            # I tried to find something clever that would strip out the whitespace from the XML doc before writing it, and failed
            # (the general suggestions were to use another XML processor - too much work)
            (outFile, outfilename) = StartOutputFile(metadataFileName)
            tempFile = file(tempfilename, "r")
            counter = 0
            for line in tempFile:
                if line.strip():
                    # line isn't blank
                    counter = 0
                    outFile.write(line)
                else:
                    counter += 1
                    if counter < 2: # allow /some/ consecutive blank lines, not too many
                        outFile.write(line)
            tempFile.close()
            os.unlink(tempfilename)

            EndOutputFile(outFile, outfilename, metadataFileName)
            
        
        # check for an <import> statement and process it here
        ProcessImportStatements(docRoot)

                
        kids = [x for x in docRoot.childNodes if x.nodeType != xml.dom.Node.TEXT_NODE]

        if validate:
            validationName = "C:\\__parCodeGen_temp_metadata__.xml"

            # save this out _after_ processing the imports, so we have all the info we need to validate with
            (outFile, outfilename) = StartOutputFile(validationName)
            doc.writexml(outFile)
            EndOutputFile(outFile, outfilename, validationName)

            print "validating " + filename
            exitCode = os.system('T:\\rage\\tools\\Modules\\base\\rage\\exes\\rageCheckXml.exe ' +
                      '--validate T:\\rage\\tools\\schemas\\parsermetadata.xsd ' +
                      validationName)
            print "done"
            if exitCode == 0:
                print filename + " is valid"
            else:
                print_err( filename, "(0): error: validation failed")
    
        if verbose and len(kids) == 0 and not isPsc:
            print filename, "(0): info: No XML metadata (<structdef>, <enumdef>, etc tags) found"
        return doc
    except xml.parsers.expat.ExpatError, exc:
        print_err( filename, "(%d)" % exc.lineno, ": error", exc.args[0] )
        raise exc


def main(args):
    global g_UseAtomicRenaming
    
    opt = optparse.OptionParser(usage="Usage: %prog [options] inputfile [otherInput]*")
    opt.add_option("-s", "--schema", dest="schemaName", action="store", default="", help="Sends schema output to FILE.xsd", metavar="FILE")
    opt.add_option("-c", "--ccode", dest="ccodeName", action="store", default="", help="Sends C++ code to FILE.h", metavar="FILE")
    opt.add_option("--noschema", dest="outputSchema", action="store_false", default=True, help="Suppress XML Schema output")
    opt.add_option("--nocode", dest="outputCode", action="store_false", default=True, help="Suppress C++ code output")
    opt.add_option("--validate", dest="validate", action="store_true", default=False, help="Validate the XML metadata before generating code")
    opt.add_option("--savemetadata", dest="saveMetadata", action="store", default="", help="Saves the metadata from the input file to a PSC file in the DIR directory", metavar="DIR")
    opt.add_option("--structdict", dest="structDict", action="store", default=None, help="Writes the processed structdefs to the listed structdef dictionary file", metavar="FILE")
    opt.add_option("--schemadict", dest="schemaDict", action="store", default=None, help="Writes the generated XSD schemas to the listed combined schema file", metavar="FILE")
    opt.add_option("--configfile", dest="configFile", action="store", default=None, help="Sets the location of a config file used to read other command line options", metavar="FILE")
    opt.add_option("--noconfig", dest="noConfig", action="store_true", default=False, help="Prevents reading from a config file")
    opt.add_option("--outputdir", dest="outputDir", action="store", default="", help="specifies an output directory to save all .xsd and .h files to")
    opt.add_option("--inputdir", dest="inputDir", action="store", default="", help="specifies an input directory to read all inputted files from")
    opt.add_option("--notempfiles", dest="noTempFiles", action="store_true", default=False, help="Don't create temp files, write directly to the outputs")
    opt.add_option("-d", "--depends", dest="depends", action="append", default=[], help="specifies an additional dependency file to check before modifying the output file")

#   This line will always do a rebuild on modified files
#    opt.add_option("-r", "--rebuild", dest="rebuild", action="store_true", default=True, help="Regenerate all files (regardless of modification time)")
#   This line will only rebuild files when necessary
    opt.add_option("-r", "--rebuild", dest="rebuild", action="store_true", default=False, help="Regenerate all files (regardless of modification time)")

    (options, outargs) = opt.parse_args(args[1:])
    
    if options.noTempFiles:
        g_UseAtomicRenaming = False

    # look for a config file. If one exists, use the options specified in there    
    if not options.noConfig:
        configFilePaths = (r"..\..\bin", r"..\..\..\base\bin")
        configFileName = "parcodegen_config.txt"
        configFileNameList = [os.path.join(x, configFileName) for x in configFilePaths]
        if options.configFile:
            configFileNameList = [options.configFile]
        
        for path in configFileNameList:
            if os.path.exists(path):
                f = open(path)
                tokens = f.read().split()
                f.close();
                # read the options, but ignore all the positional params
                print "Reading config file", path
                # passing in values=options should use the prev. command line args as default values
                (options, ignoredOutArgs) = opt.parse_args(tokens, values=options)
                break
        
    if len(outargs) < 1:
        opt.print_help()
        sys.exit(1)

    if len(outargs) > 1 and (options.schemaName != "" or options.ccodeName != ""):
        opt.print_help()
        sys.exit(1)

    inputDocuments = []

    filesExist = False
    allFilesUpToDate = True
    
    for fileName in outargs:
        try:
            if options.inputDir != "":
                fileName = options.inputDir + fileName;

            inputFileName = os.path.abspath(fileName)

            if os.path.splitext(inputFileName)[1] == "":
                newFileName = inputFileName + ".psc"
                if os.path.exists(newFileName):
                    inputFileName = newFileName
                else:
                    if os.path.exists(inputFileName + ".cpp"):
                        print_err("Using .cpp files for input is no longer supported, move all the schema data into %s.psc" % (inputFileName))
                        sys.exit(1)
                    else:
                        print_err( "Couldn't find %s.psc for input" % (inputFileName) )
                    continue
            elif inputFileName.endswith(".cpp"):
                if not options.saveMetadata: # .cpp is OK if you're extracting metadata from it
                    print_err("Using .cpp files for input is no longer supported, move all the schema data into %s.psc" % (os.path.splitext(inputFileName)[0]))
                    sys.exit(1)
                    continue
            elif not os.path.exists(inputFileName):
                print_err("Couldn't find %s for input" % (inputFileName))
                continue

            filesExist = True
            
            #noExtName = os.path.splitext(os.path.abspath(inputFileName))[0]
            basename = os.path.splitext(os.path.basename(inputFileName))[0]

            if options.outputDir != "":
                basename = options.outputDir + basename;
            else:
                basename = os.path.splitext(inputFileName)[0]

            schemaName = options.schemaName
            ccodeName = options.ccodeName
        
            if schemaName == "":
                schemaName = basename + "_parser.xsd"
            if ccodeName == "":
                ccodeName = basename + "_parser.h"

            metadataname = None
            if options.saveMetadata != "":
                metadataname = os.path.join(options.saveMetadata, os.path.split(basename + ".psc")[1])

            # check if any of the code is newer than the file with the <structdef>
            # if so, regenerate the code
            if hasattr(sys, "frozen"):
                # we've been frozen with py2exe
                codeDir = os.path.split(sys.path[0])[0]
                # codeFiles = os.listdir(codeDir)
                codeFiles = [codeDir + "\\parCodeGen.zip", codeDir + "\\parCodeGen.exe"]
            else:
                codeDir = sys.path[0]
                codeFiles = [x for x in os.listdir(codeDir) if os.path.splitext(x)[1] == ".py"]
            codeTime = max([fileTime(os.path.join(codeDir, x)) for x in codeFiles])
           
            origTime = max(codeTime, fileTime(inputFileName))
            
            if len(options.depends) > 0:
                origTime = max(origTime, max([fileTime(x) for x in options.depends]))
            
            # only process the file if the file (or anything in the parCodeGen directory) is newer than the outputs.
            processFile = False
            if options.rebuild:
                processFile = True
            else:
                if options.outputSchema and fileTime(schemaName) < origTime:
                    processFile = True
                if options.outputCode and fileTime(ccodeName) < origTime:
                    processFile = True

            if not processFile:
                continue;

            allFilesUpToDate = False
            
            domDoc = FindXmlBlocks(inputFileName, True, options.validate, metadataname)
            inputDocuments.append(domDoc)
            
            generatedDoc = None
            if (options.outputCode and os.path.exists(ccodeName)):
                generatedDoc = FindXmlBlocks(ccodeName, onlyExtractFromMarkedBlocks=True)

            # capture any output we're supposed to put into the generated _parser.h file later
            stringstream = StringIO.StringIO()
            GenericSpec.OutFile = stringstream
                
            # turn the xml tree into a set of datatype objects
            basetype.CurrFileName = inputFileName
            basetype.CurrLineNumber = 0
            nodes = DocumentSpec(domDoc.documentElement, generatedDoc)
            
            print "parCodeGen:", inputFileName

            if options.outputCode:
                outputfilename = None
                (GenericSpec.OutFile, outputfilename) = StartOutputFile(ccodeName)
                
                GenericSpec.OutFile.write(stringstream.getvalue())
                stringstream.close()
                
                nodes.WriteCCode(ccodeName, inputFileName)
                EndOutputFile(GenericSpec.OutFile, outputfilename, ccodeName)
                
                # check to see if we need to write out a .h and .cpp file as well
                if nodes.NeedsCDefinitions():
                    nodes.ComputeGUID()
                
                    cdefnHeaderFileName = ccodeName.replace("_parser.h", ".h")
                    (GenericSpec.OutFile, outputfilename) = StartOutputFile(cdefnHeaderFileName)
                    nodes.WriteCDefnHeader(cdefnHeaderFileName, inputFileName)
                    EndOutputFile(GenericSpec.OutFile, outputfilename, cdefnHeaderFileName)
                    
                    cdefnCodeFileName = ccodeName.replace("_parser.h", ".cpp")
                    (GenericSpec.OutFile, outputfilename) = StartOutputFile(cdefnCodeFileName)
                    nodes.WriteCDefnCode(cdefnCodeFileName, inputFileName, cdefnHeaderFileName, ccodeName)
                    EndOutputFile(GenericSpec.OutFile, outputfilename, cdefnCodeFileName)
                
            if options.outputSchema:
                (GenericSpec.OutFile, outputfilename) = StartOutputFile(schemaName)
                nodes.WriteSchemaCode()
                EndOutputFile(GenericSpec.OutFile, outputfilename, schemaName)
                
            if nodes.NeedsSchDefinitions():
                schName = os.path.join(os.environ['RS_CODEBRANCH'], 'game/script_headers', nodes.scriptFile)
                print "Exporting", schName
                (GenericSpec.OutFile, outputfilename) = StartOutputFile(schName, True)
                nodes.WriteSch(schName, inputFileName)
                EndOutputFile(GenericSpec.OutFile, outputfilename, schName, True)
                
        except ImportError,e:
            print_err(inputFileName, "error:", e)
        except xml.parsers.expat.ExpatError:
            pass
            

    if options.structDict and len(inputDocuments) > 0:
        # read the existing struct dict
        # for each structdef in domDoc, search for the structdef in the dictionary, replace it if it's there.

        structureDict = None
        try:
            structureDict = xml.dom.minidom.parse(options.structDict)
        except IOError:
            global StructDictTemplate
            structureDict = xml.dom.minidom.parseString(StructDictTemplate)
        except xml.parsers.expat.ExpatError, e:
            structureDict = None
            print_err("%s (%d) error: couldn't parse XML in structure dictionary" % (options.structDict, e.lineno))
            print_err("%s (%d) error: %s" % (options.structDict, e.lineno, xml.parsers.expat.ErrorString(e.code)))

        if structureDict:    
            childMap = {}
            for node in structureDict.documentElement.childNodes:
                if isinstance(node, xml.dom.minidom.Element) and node.tagName == "structdef":
                    childMap[node.getAttribute("type")] = node

            for document in inputDocuments:
                for newNode in document.documentElement.childNodes:
                    if isinstance(newNode, xml.dom.minidom.Element) and newNode.tagName == "structdef":
                        if childMap.has_key(newNode.getAttribute("type")):
                            structureDict.documentElement.replaceChild(newNode, childMap[newNode.getAttribute("type")])
                        else:
                            structureDict.documentElement.appendChild(newNode.cloneNode(True))
                            structureDict.documentElement.appendChild(structureDict.createTextNode("\n\n"))
            (outFile, outfilename) = StartOutputFile(options.structDict)
            structureDict.writexml(outFile)
            AtomicRename(outFile, outfilename, options.structDict)

    if filesExist and allFilesUpToDate:
        print "parCodeGen: All files up to date"
            
    if basetype.FoundErrors > 0:
        return 1
    return 0

if __name__ == '__main__':
    code = main(sys.argv)
    if code != 0:
        sys.exit(code)
    
