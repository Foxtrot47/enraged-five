
import sys
from xml.dom.minidom import *
from structtype import *
from basetype import *
from inttypes import *
from stringtypes import *

class MapSpec(MemberSpec):
    def __init__(self, domNode, parentObj):
        self.type = None
        self.key = None
        self.addGroupWidget = None
        self.enumKeyType = None
        self.enumKeySize = "32"
                
        MemberSpec.__init__(self, domNode, parentObj)
        
        # make sure all the children have a name="Item" tag. name is normally treated as a required attribute, but its not required in this context,
        # so its easier just to set it here than mess around with telling when it's allowed or not
        for node in domNode.childNodes:
            if node.nodeType == xml.dom.Node.ELEMENT_NODE:
                node.setAttribute("name", "Item")
                
        self.Children = BuildSpecObjects(domNode, self)
        if len(self.Children) != 1:
            print "Error: Map \"%s\" must have exactly one child. Has %d" % (self.CppName, len(self.Children))
            sys.exit(1)
        self.ChildSpec = self.Children[0];
        if not isinstance(self.ChildSpec, MemberSpec) and not isinstance(self.ChildSpec, AssertSpec) and not isinstance(self.ChildSpec, PadSpec):
            CodeGenError("Found a node that can't be inside a <map>: <%s>" % self.ChildSpec.DomNode.nodeName, wikiPage=self.WikiPage)
            
        # make a little DOM we can parse for the key
        doc = Document()
        parentElt = doc.createElement("map")
        doc.documentElement = parentElt
        
        keyElem = None
        
        if self.key in ('s8', 'u8', 's16', 'u16', 's32', 'u32', 'size_t', 'ptrdiff_t', 'u64', 's64'):
            keyElem = doc.createElement(self.key)
        elif self.key in StringSpec.HashTypes:
            keyElem = doc.createElement('string')
            keyElem.setAttribute("type", self.key)
        elif self.key == 'enum':
            if not self.enumKeyType:
                CodeGenError("enumKeyType attribute is required when using type=\"enum\"", wikiPage=self.WikiPage)
            else:
                keyElem = doc.createElement('enum')
                keyElem.setAttribute("type", self.enumKeyType)
                keyElem.setAttribute("size", self.enumKeySize)
        
        keyElem.setAttribute("name", "Key")
        
        parentElt.appendChild(keyElem)
        
        keys = BuildSpecObjects(parentElt, self)
        if len(keys) != 1:
            print "Processing error, somehow got wrong number of kids for map key node"
            sys.exit(1)
        self.KeySpec = keys[0]
        
    def CheckAttributes(self):
        self.WikiPage = "Map_PSC_tag"
        MemberSpec.CheckAttributes(self)
        self.RequiredAttribute("type", ["atMap", "atBinaryMap"], safeVal="atMap")
        self.RequiredAttribute("key", ["s8", "u8", "s16", "u16", "s32", "u32", "size_t", "ptrdiff_t", "u64", "s64", "enum"] + StringSpec.HashTypes, safeVal="s32")
        self.OptionalAttribute("addGroupWidget", "true")
        self.OptionalAttribute("enumKeyType", None)
        self.OptionalAttribute("enumKeySize", "32", ["8", "16", "32"])
    
