from __future__ import with_statement

# This template file contains all of the code specific to generating C definition code documents from parser metadata.
# The general pattern here is to inject new member functions into the existing class hierarchy 
# and the new member functions will be used to generate the code.
#
# Most code below will use the @AddMethod decorator like so:
# @AddMethod(ClassName)
# def NewMethod(self): 
#   # do stuff
#
# That adds a ClassName.NewMethod member function
# @AddMethod is defined in templatesupport.py

from templatesupport import *
from template_cpp_support import *
from basetype import *
from structtype import *
from inttypes import *
from floattypes import *
from vectortypes import *
from arraytype import *
from stringtypes import *
from enumtype import *
from matrixtypes import *
from datatypes import *

import atHash

@AddMethod(GenericSpec)
def WriteCDefnCode(self): pass

#####################################
## Document
#####################################

@AddMethod(DocumentSpec)
def WriteCDefnCode(self, fileName, sourceFileName, cdefnHeaderFileName, parserFileName):
    try:
        GenericSpec.Depth = 0
        self.WriteCommonCHeader(fileName, sourceFileName, useIncludeGuard=False, usage="and should be added to the project")

        self.WriteString('''
#include "%(:cdefnHeader)s"
#include "%(:parserHeader)s"
    ''', cdefnHeader=os.path.split(cdefnHeaderFileName)[1], parserHeader=os.path.split(parserFileName)[1])

        includeSet = set()
        for i in self.Children:
            includeSet |= i.BuildCIncludeSet()
            
        includeList = list(includeSet)
        includeList.sort()
        
        for i in includeList:
            self.WriteString('''
#include %(:i)s''', i=i)


        for i in self.Children:
            i.WriteCDefnCode()
            
        self.WriteString('''
        
PAR_HEADER_GUID(%(:includeGuardStripped)s, id_%(GUID)s);
''', includeGuardStripped=self.IncludeGuardName[:-4])
    except:
        CodeGenError("in file %s" % (sourceFileName))
        raise

@AddMethod(GenericSpec)
def BuildCIncludeSet(self):
    return set()
    
@AddMethod(CIncludeSpec)
def BuildCIncludeSet(self):
    return self.Includes
    
#####################################
## Structures
#####################################

@AddMethod(StructDefSpec)
def WriteCDefnCode(self):
    self.SetStructdefErrorInfo()
    self.WriteString('''
    
//////////////////////////////////////////////
// class %(CppType)s
//////////////////////////////////////////////
    ''')
    if self.cond != "1":
        self.WriteString('''
#if %(cond)s''')
    with WritePoundIfPlatform(self):

        self.WriteConstructor()
        self.WriteDestructor()
    
        if self.rsc == "true":
            self.WriteResourceConstructor()
            self.WriteString('''
IMPLEMENT_PARSER_PLACE(%(CppType)s);
''')
    
    if self.cond != "1":
        self.WriteString('''
#endif // %(cond)s''')

#####################################
## Construction
#####################################

CtorArrayDepth = 0

@AddMethod(StructDefSpec)
def WriteConstructor(self):
    self.SetStructdefErrorInfo()
    global CtorArrayDepth
    CtorArrayDepth = 0

    nsToks = self.CppType.split("::")
    self.WriteString('''\n%(CppType)s::%(:localName)s()''', localName = nsToks[-1])
    # write initializer list
    sepChar = ":"
    for i in self.IterMembers():
        with WritePoundIfPlatform(i):
            if i.WriteCDefnCtorInit(sepChar):
                sepChar = ","
    self.WriteString('''\n{''')
    self.ForEachMember_IfPlatform(lambda x: x.WriteCDefnCtor())
    self.WriteString('''
}
''')

@AddMethod(StructDefSpec)
def WriteResourceConstructor(self):
    self.SetStructdefErrorInfo()
    nsToks = self.CppType.split("::")
    self.WriteString('''\n%(CppType)s::%(:localName)s(::rage::datResource& rsc)''', localName = nsToks[-1])
    # write initializer list
    sepChar = ":"
    if self.base != None:
        self.WriteString('''\n: %(base)s(rsc)''')
        sepChar = ","
    for i in self.IterMembers():
        with WritePoundIfPlatform(i):
            if i.WriteCDefnRscCtorInit(sepChar):
                sepChar = ","
    self.WriteString('''\n{''')
    self.ForEachMember_IfPlatform(lambda x: x.WriteCDefnRscCtor())
    self.WriteString('''
}
''')

#####################################
## Initialization List code
##
## These all take a seperator character which is ':' for the first thing we write out and ',' for subsequent things:
## Foo::Foo()
## : m_A()
## , m_B()
## , m_C()
#####################################

@AddMethod(GenericSpec)
def WriteCDefnCtorInit(self, sepChar):
    return False
    
@AddMethod(DataMemberSpec)
def WriteCDefnCtorInit(self, sepChar):
    self.WriteString("\n%(:sepChar)s %(CppName)s(%(init)s)", sepChar=sepChar)
    return True

@AddMethod(BoolSpec)
def WriteCDefnCtorInit(self, sepChar):
    self.WriteString("\n%(:sepChar)s %(CppName)s(%(init)s)", sepChar=sepChar)
    return True

@AddMethod(FloatSpec)
def WriteCDefnCtorInit(self, sepChar):
    self.WriteString("\n%(:sepChar)s %(CppName)s(%(:init)s)", sepChar=sepChar, init=GetCppFloat(self.init))
    return True
    
@AddMethod(EnumSpec)
def WriteCDefnCtorInit(self, sepChar):
    self.WriteString("\n%(:sepChar)s %(CppName)s((%(EnumDefType)s)%(init)s)", sepChar=sepChar)
    return True
    
@AddMethod(PointerSpec)
def WriteCDefnCtorInit(self, sepChar):
    if self.policy not in ('lazylink'): # some policies mean the pointer has a built in c'tor
        self.WriteString("\n%(:sepChar)s %(CppName)s(NULL)", sepChar=sepChar)
        return True
    return False
    
@AddMethodToClasses(Vector2Spec, Vec2VSpec)
def WriteCDefnCtorInit(self, sepChar):
    (x,y,z,w) = GetCppFloatList(self.init)
    self.WriteString("\n%(:sepChar)s %(CppName)s(%(:x)s, %(:y)s)", sepChar=sepChar, x=x, y=y)
    return True

@AddMethodToClasses(Vector3Spec, Vec3VSpec)
def WriteCDefnCtorInit(self, sepChar):
    (x,y,z,w) = GetCppFloatList(self.init)
    self.WriteString("\n%(:sepChar)s %(CppName)s(%(:x)s, %(:y)s, %(:z)s)", sepChar=sepChar, x=x, y=y, z=z)
    return True

@AddMethodToClasses(Vector4Spec, Vec4VSpec)
def WriteCDefnCtorInit(self, sepChar):
    (x,y,z,w) = GetCppFloatList(self.init)
    self.WriteString("\n%(:sepChar)s %(CppName)s(%(:x)s, %(:y)s, %(:z)s, %(:w)s)", sepChar=sepChar, x=x, y=y, z=z, w=w)
    return True

@AddMethod(VecBoolVSpec)    
def WriteCDefnCtorInit(self, sepChar):
    self.WriteString("\n%(:sepChar)s %(CppName)s(::rage::V_F_F_F_F) /* VecBoolV init values are currently unsupported */", sepChar=sepChar)
    return True
    
@AddMethod(BitsetSpec)
def WriteCDefnCtorInit(self, sepChar):
    if self.StorageType != "atBitSet" and self.type != "generated":
        self.WriteString("\n%(:sepChar)s %(CppName)s(false)", sepChar=sepChar)
        return True
    return False
    
@AddMethodToClasses(ScalarVSpec)
def WriteCDefnCtorInit(self, sepChar):
    return False
    
   
@AddMethod(Matrix34Spec)
def WriteCDefnCtorInit(self, sepChar):
    elts = GetCppFloatList(self.init)
    self.WriteString('''
%(:sepChar)s %(CppName)s(
    %(:ax)s,\t%(:ay)s,\t%(:az)s,
    %(:bx)s,\t%(:by)s,\t%(:bz)s,
    %(:cx)s,\t%(:cy)s,\t%(:cz)s,
    %(:dx)s,\t%(:dy)s,\t%(:dz)s
    )''', sepChar=sepChar, 
    ax=elts[0], ay=elts[1], az=elts[2],
    bx=elts[4], by=elts[5], bz=elts[6],
    cx=elts[8], cy=elts[9], cz=elts[10],
    dx=elts[12], dy=elts[13], dz=elts[14])
    return True
    
@AddMethod(Matrix44Spec)
def WriteCDefnCtorInit(self, sepChar):
    elts = GetCppFloatList(self.init)
    self.WriteString('''
%(:sepChar)s %(CppName)s(
    %(:ax)s,\t%(:ay)s,\t%(:az)s,\t%(:aw)s,
    %(:bx)s,\t%(:by)s,\t%(:bz)s,\t%(:bw)s,
    %(:cx)s,\t%(:cy)s,\t%(:cz)s,\t%(:cw)s,
    %(:dx)s,\t%(:dy)s,\t%(:dz)s,\t%(:dw)s
    )''', sepChar=sepChar, 
    ax=elts[0], ay=elts[1], az=elts[2], aw=elts[3],
    bx=elts[4], by=elts[5], bz=elts[6], bw=elts[7],
    cx=elts[8], cy=elts[9], cz=elts[10], cw=elts[8],
    dx=elts[12], dy=elts[13], dz=elts[14], dw=elts[15])
    return True
    
@AddMethod(Mat33VSpec)
def WriteCDefnCtorInit(self, sepChar):
    elts = GetCppFloatList(self.init)
    self.WriteString('''
%(:sepChar)s %(CppName)s(::rage::V_ROW_MAJOR,
    %(:ax)s,\t%(:ay)s,\t%(:az)s,
    %(:bx)s,\t%(:by)s,\t%(:bz)s,
    %(:cx)s,\t%(:cy)s,\t%(:cz)s
    )''', sepChar=sepChar, 
    ax=elts[0], ay=elts[1], az=elts[2],
    bx=elts[4], by=elts[5], bz=elts[6],
    cx=elts[8], cy=elts[9], cz=elts[10])
    return True
    
@AddMethod(Mat34VSpec)
def WriteCDefnCtorInit(self, sepChar):
    elts = GetCppFloatList(self.init)
    self.WriteString('''
%(:sepChar)s %(CppName)s(::rage::V_ROW_MAJOR,
        %(:ax)s,\t%(:ay)s,\t%(:az)s,%(:aw)s,
        %(:bx)s,\t%(:by)s,\t%(:bz)s,%(:bw)s,
        %(:cx)s,\t%(:cy)s,\t%(:cz)s,%(:cw)s
    )''', sepChar=sepChar, 
    ax=elts[0], ay=elts[1], az=elts[2], aw=elts[3],
    bx=elts[4], by=elts[5], bz=elts[6], bw=elts[7],
    cx=elts[8], cy=elts[9], cz=elts[10], cw=elts[11],
    )
    return True
    
@AddMethod(Mat44VSpec)
def WriteCDefnCtorInit(self, sepChar):
    elts = GetCppFloatList(self.init)
    self.WriteString('''
%(:sepChar)s %(CppName)s(::rage::V_ROW_MAJOR,
    %(:ax)s,\t%(:ay)s,\t%(:az)s,\t%(:aw)s,
    %(:bx)s,\t%(:by)s,\t%(:bz)s,\t%(:bw)s,
    %(:cx)s,\t%(:cy)s,\t%(:cz)s,\t%(:cw)s,
    %(:dx)s,\t%(:dy)s,\t%(:dz)s,\t%(:dw)s
    )''', sepChar=sepChar, 
    ax=elts[0], ay=elts[1], az=elts[2], aw=elts[3],
    bx=elts[4], by=elts[5], bz=elts[6], bw=elts[7],
    cx=elts[8], cy=elts[9], cz=elts[10], cw=elts[8],
    dx=elts[12], dy=elts[13], dz=elts[14], dw=elts[15])
    return True

@AddMethod(StringSpec)
def WriteCDefnCtorInit(self, sepChar):
    if self.type in ("pointer", "wide_pointer"):
        if self.init != "":
            copyMethod = "::rage::StringDuplicate"
            if self.type == "wide_pointer":
                self.WriteString("\n%(:sepChar)s %(CppName)s(::rage::WideStringDuplicate(_C16(\"%(init)s\")))", sepChar=sepChar, copyMethod=copyMethod)
            else:
                self.WriteString("\n%(:sepChar)s %(CppName)s(::rage::StringDuplicate(\"%(init)s\"))", sepChar=sepChar, copyMethod=copyMethod)
        else:
            self.WriteString("\n%(:sepChar)s %(CppName)s(NULL)", sepChar=sepChar)
        return True
    elif self.type in ("atString", "ConstString", "atWideString") and self.init != "":
        self.WriteString("\n%(:sepChar)s %(CppName)s(\"%(init)s\")", sepChar=sepChar)
        return True
    return False
    
@AddMethod(ArraySpec)
def WriteCDefnCtorInit(self, sepChar):
    if self.type == "pointer":
        self.WriteString("\n%(:sepChar)s %(CppName)s(NULL)", sepChar=sepChar)
        if self.sizeVar != None:
            self.WriteString("\n, %(sizeVar)s(0)")
        return True
    return False

#####################################
## Constructor Body Code
## 
## Generally all construction can happen using intializer lists as above. The two cases where it can't are
## for member-strings and member-arrays. For member-strings we init the first char to NULL - for member-arrays we need
## to do essentially all the work we did up in the initilizer list but use assignment operators instead.
#####################################
    
@AddMethod(GenericSpec)
def WriteCDefnCtor(self): pass

# Init member strings to empty strings (see below for implementation)
@AddMethod(StringSpec)
def WriteCDefnCtor(self):
    self.WriteArrayEltInit(self.CppName)
    
# if we have a member array, call into the array initialization code
@AddMethod(ArraySpec)
def WriteCDefnCtor(self):
    if self.type in ("atRangeArray", "member"):
        if not self.ChildSpec.NeedsConstruction():
            return
    
        self.WriteString('''
    //// Initialize members of %(CppName)s''')
        self.WriteArrayEltInit(self.CppName)

@AddMethod(ScalarVSpec)
def WriteCDefnCtor(self):
    self.WriteString('''
    %(CppName)s.Setf(%(:init)s);''', init=GetCppFloat(self.init));
    
        
#####################################
## Array Element Initialization Code
#####################################      

@AddMethod(ArraySpec)
def WriteArrayEltInit(self, varname):
    global CtorArrayDepth
    
    if self.type == 'virtual':
        return # no initialization necessary
    
    CtorArrayDepth += 1
    if self.type == 'atRangeArray':
        self.WriteString('''
    for(int index%(:depth)s = 0; index%(:depth)s < %(:varname)s.GetMaxCount(); index%(:depth)s++) 
    {''', depth=CtorArrayDepth, varname=varname)
    elif self.type in ('atArray', 'atFixedArray'):
        self.WriteString('''
    for(int index%(:depth)s = 0; index%(:depth)s < %(size)s; index%(:depth)s++) 
    {''', depth=CtorArrayDepth)
    else:
        self.WriteString('''
    for(size_t index%(:depth)s = 0; index%(:depth)s < %(size)s; index%(:depth)s++) 
    {''', depth=CtorArrayDepth)

    GenericSpec.Depth += 1
    self.ChildSpec.WriteArrayEltInit(varname + "[index" + str(CtorArrayDepth) + "]")
    GenericSpec.Depth -= 1
    self.WriteString('''
    }''')
    CtorArrayDepth -= 1
    
@AddMethod(GenericSpec)
def WriteArrayEltInit(self, varname):
    pass
    
@AddMethod(PointerSpec)
def WriteArrayEltInit(self, varname):
    if self.policy not in ('lazylink'): #some policies don't need initting
        self.WriteString('''
    %(:varname)s = NULL;''', varname=varname);
    
@AddMethod(BoolSpec)
def WriteArrayEltInit(self, varname):
    self.WriteString('''
    %(:varname)s = %(init)s;''', varname=varname);

@AddMethod(DataMemberSpec)
def WriteArrayEltInit(self, varname):
    self.WriteString('''
    %(:varname)s = %(init)s;''', varname=varname);
    
@AddMethod(EnumSpec)
def WriteArrayEltInit(self, varname):
    self.WriteString('''
    %(:varname)s = (%(EnumDefType)s)%(init)s;''', varname=varname);

@AddMethod(BitsetSpec)
def WriteArrayEltInit(self, varname):
    if self.StorageType != "atBitSet":
        self.WriteString("\n    %(:varname)s.Reset()", varname=varname)
     
    
@AddMethod(StringSpec)
def WriteArrayEltInit(self, varname):
    if self.type == "member":
        if self.init != "":
            self.WriteString('''
    ::rage::safecpy(%(:cVarName)s, "%(init)s");''', cVarName=varname)
        else:
            self.WriteString('''
    %(:cVarName)s[0] = '\\0';''', cVarName=varname)
    elif self.type == "wide_member":
        if self.init != "":
            self.WriteString('''
    wcsncpy(%(:cVarName)s, _C16("%(init)s"), %(size)s);
    %(:cVarName)s[(%(size)s)-1] = ::rage::char16(0);''', cVarName=varname)
        else:
            self.WriteString('''
    %(:cVarName)s[0] = 0;''', cVarName=varname)
    elif self.type == "atHashValue" and self.init != "":
        self.WriteString('''
    %(:cVarName)s.SetHash(0x%(:hash)x); // "%(init)s"''', cVarName=varname, hash=atHash.atStringHash(self.init))
    elif self.type == "atPartialHashValue":
        if self.init == "":
            self.WriteString('''
    %(:cVarName)s = 0;''', cVarName=varname)
        else:
            self.WriteString('''
    static ::rage::u32 s_%(:cVarName)s_init = atPartialStringHash("%(init)s");
    %(:cVarName)s = s_%(:cVarName)s_init;''', cVarName=varname)
    elif self.type in StringSpec.HashTypes and self.init != "":
        self.WriteString('''
    static ::rage::%(type)s s_%(:cVarName)s_init("%(init)s"); // 0x%(:hash)x
    %(:cVarName)s = s_%(:cVarName)s_init;''', cVarName=varname, hash=atHash.atStringHash(self.init))

@AddMethod(Vector2Spec)
def WriteArrayEltInit(self, varname):
    (x,y,z,w) = GetCppFloatList(self.init)
    self.WriteString('''
    %(:varname)s.Set(%(:x)s, %(:y)s);''', varname=varname, x=x, y=y)
    
@AddMethod(Vector3Spec)
def WriteArrayEltInit(self, varname):
    (x,y,z,w) = GetCppFloatList(self.init)
    self.WriteString('''
    %(:varname)s.Set(%(:x)s, %(:y)s, %(:z)s);''', varname=varname, x=x, y=y, z=z)

@AddMethod(Vector4Spec)
def WriteArrayEltInit(self, varname):
    (x,y,z,w) = GetCppFloatList(self.init)
    self.WriteString('''
    %(:varname)s.Set(%(:x)s, %(:y)s, %(:z)s, %(:w)s);''', varname=varname, x=x, y=y, z=z, w=w)
        
@AddMethod(Vec2VSpec)
def WriteArrayEltInit(self, varname):
    (x,y,z,w) = GetCppFloatList(self.init)
    self.WriteString('''
    %(:varname)s = ::rage::Vec2V(%(:x)s, %(:y)s);''', varname=varname, x=x, y=y)
    
@AddMethod(Vec3VSpec)
def WriteArrayEltInit(self, varname):
    (x,y,z,w) = GetCppFloatList(self.init)
    self.WriteString('''
    %(:varname)s = ::rage::Vec3V(%(:x)s, %(:y)s, %(:z)s);''', varname=varname, x=x, y=y, z=z)

@AddMethod(Vec4VSpec)
def WriteArrayEltInit(self, varname):
    (x,y,z,w) = GetCppFloatList(self.init)
    self.WriteString('''
    %(:varname)s = ::rage::Vec4V(%(:x)s, %(:y)s, %(:z)s, %(:w)s);''', varname=varname, x=x, y=y, z=z, w=w)
        
@AddMethod(VecBoolVSpec)
def WriteArrayEltInit(self, varname):
    self.WriteString('''
    %(:varname)s = ::rage::VecBoolV(::rage::V_F_F_F_F); /* VecBoolV init values are currently unsupported */''', varname=varname)
        
@AddMethod(Matrix34Spec)
def WriteArrayEltInit(self, varname):
    elts = GetCppFloatList(self.init)
    self.WriteString('''
    %(:cVarName)s.Set(
        %(:ax)s,\t%(:ay)s,\t%(:az)s,
        %(:bx)s,\t%(:by)s,\t%(:bz)s,
        %(:cx)s,\t%(:cy)s,\t%(:cz)s,
        %(:dx)s,\t%(:dy)s,\t%(:dz)s
    );''', cVarName=varname, 
    ax=elts[0], ay=elts[1], az=elts[2],
    bx=elts[4], by=elts[5], bz=elts[6],
    cx=elts[8], cy=elts[9], cz=elts[10],
    dx=elts[12], dy=elts[13], dz=elts[14])
    
@AddMethod(Matrix44Spec)
def WriteArrayEltInit(self, varname):
    elts = GetCppFloatList(self.init)
    self.WriteString('''
    %(:cVarName)s.Set(
        %(:ax)s,\t%(:ay)s,\t%(:az)s,\t%(:aw)s,
        %(:bx)s,\t%(:by)s,\t%(:bz)s,\t%(:bw)s,
        %(:cx)s,\t%(:cy)s,\t%(:cz)s,\t%(:cw)s,
        %(:dx)s,\t%(:dy)s,\t%(:dz)s,\t%(:dw)s
    );''', cVarName=varname, 
    ax=elts[0], ay=elts[1], az=elts[2], aw=elts[3],
    bx=elts[4], by=elts[5], bz=elts[6], bw=elts[7],
    cx=elts[8], cy=elts[9], cz=elts[10], cw=elts[8],
    dx=elts[12], dy=elts[13], dz=elts[14], dw=elts[15])
    
@AddMethod(Mat33VSpec)
def WriteArrayEltInit(self, varname):
    elts = GetCppFloatList(self.init)
    self.WriteString('''
    %(:cVarName)s = ::rage::Mat33V(::rage::V_ROW_MAJOR,
        %(:ax)s,\t%(:ay)s,\t%(:az)s,
        %(:bx)s,\t%(:by)s,\t%(:bz)s,
        %(:cx)s,\t%(:cy)s,\t%(:cz)s
    );''', cVarName=varname, 
    ax=elts[0], ay=elts[1], az=elts[2],
    bx=elts[4], by=elts[5], bz=elts[6],
    cx=elts[8], cy=elts[9], cz=elts[10])
    
@AddMethod(Mat34VSpec)
def WriteArrayEltInit(self, varname):
    elts = GetCppFloatList(self.init)
    self.WriteString('''
    %(:cVarName)s = ::rage::Mat34V(::rage::V_ROW_MAJOR,
        %(:ax)s,\t%(:ay)s,\t%(:az)s,%(:aw)s,
        %(:bx)s,\t%(:by)s,\t%(:bz)s,%(:bw)s,
        %(:cx)s,\t%(:cy)s,\t%(:cz)s,%(:cw)s
    );''', cVarName=varname, 
    ax=elts[0], ay=elts[1], az=elts[2], aw=elts[3],
    bx=elts[4], by=elts[5], bz=elts[6], bw=elts[7],
    cx=elts[8], cy=elts[9], cz=elts[10], cw=elts[11],
    )
    
@AddMethod(Mat44VSpec)
def WriteArrayEltInit(self, varname):
    elts = GetCppFloatList(self.init)
    self.WriteString('''
    %(:cVarName)s = ::rage::Mat44V(::rage::V_ROW_MAJOR,
        %(:ax)s,\t%(:ay)s,\t%(:az)s,\t%(:aw)s,
        %(:bx)s,\t%(:by)s,\t%(:bz)s,\t%(:bw)s,
        %(:cx)s,\t%(:cy)s,\t%(:cz)s,\t%(:cw)s,
        %(:dx)s,\t%(:dy)s,\t%(:dz)s,\t%(:dw)s
    );''', cVarName=varname, 
    ax=elts[0], ay=elts[1], az=elts[2], aw=elts[3],
    bx=elts[4], by=elts[5], bz=elts[6], bw=elts[7],
    cx=elts[8], cy=elts[9], cz=elts[10], cw=elts[8],
    dx=elts[12], dy=elts[13], dz=elts[14], dw=elts[15])
        
#####################################
## NeedsConstruction()
##   - true if we have something that doesn't get default constructed by the compiler already
#####################################
        
CreateSimpleGetterFunctions("NeedsConstruction", {
    GenericSpec: False,
    DataMemberSpec: True,
    BoolSpec: True,
    EnumSpec: True,
    })
    
@AddMethod(PointerSpec)
def NeedsConstruction(self):
    return self.policy not in ('lazylink')

@AddMethod(StringSpec)
def NeedsConstruction(self):
    return self.type in ('pointer', 'wide_pointer', 'member', 'wide_member', 'atPartialHashValue')
    
@AddMethod(ArraySpec)
def NeedsConstruction(self):
    if self.type in ('member', 'atRangeArray'):
        return self.ChildSpec.NeedsConstruction()
    return False
    
@AddMethod(BitsetSpec)
def NeedsConstruction(self):
    return self.StorageType not in ('atBitSet') and self.type != "generated"
    
        
#####################################
## Resource Initialization List code
##
## These all take a seperator character which is ':' for the first thing we write out and ',' for subsequent things:
## Foo::Foo(::rage::datResource& rsc)
## : m_A(rsc)
## , m_B(rsc)
## , m_C(rsc)
#####################################

@AddMethod(GenericSpec)
def WriteCDefnRscCtorInit(self, sepChar):
    return False
    
@AddMethod(BitsetSpec)
def WriteCDefnRscCtorInit(self, sepChar):
    self.WriteString("\n%(:sepChar)s %(CppName)s(rsc)", sepChar=sepChar)
    return True
    
@AddMethod(ArraySpec)
def WriteCDefnRscCtorInit(self, sepChar):
    if self.type in ('atArray', 'atRangeArray', 'atFixedArray'):
        self.WriteString("\n%(:sepChar)s %(CppName)s(rsc, true)", sepChar=sepChar)
        return True
    return False

@AddMethod(StringSpec)
def WriteCDefnRscCtorInit(self, sepChar):
    if self.type in ('ConstString', 'atString', 'atWideString') or self.type in StringSpec.HashTypes:
        self.WriteString("\n%(:sepChar)s %(CppName)s(rsc)", sepChar=sepChar)
        return True
    return False
    
@AddMethod(StructSpec)
def WriteCDefnRscCtorInit(self):
    self.WriteString("\n%(:sepChar)s %(CppName)s(rsc)", sepChar=sepChar)
    return True

#####################################
## Resource Constructor body code
## 
## for things that couldn't get handled in the initialization list
##
#####################################
    
@AddMethod(GenericSpec)
def WriteCDefnRscCtor(self):
    pass
    
@AddMethod(ArraySpec)
def WriteCDefnRscCtor(self):
    if self.type in ('member', 'pointer'):
        self.WriteString("CompileTimeAssert(0); // RUSS NEEDS TO FINISH THIS CODE")

@AddMethod(PointerSpec)
def WriteCDefnRscCtor(self):
    if self.policy in ('owner', 'simple_owner'):
        self.WriteString('''rsc.Place(%(CppName)s);''')
    else:
        self.WriteString("CompileTimeAssert(0); // RUSS NEEDS TO FINISH THIS CODE")

    
#####################################
## Destruction
#####################################

@AddMethod(StructDefSpec)
def WriteDestructor(self):
    self.SetStructdefErrorInfo()
    nsToks = self.CppType.split("::")
    self.WriteString('''
%(CppType)s::~%(:localName)s()
{''', localName=nsToks[-1])
    for mem in self.IterMembers():
        if mem.NeedsDestruction():
            with WritePoundIfPlatform(mem):
                mem.WriteCDefnDtor(mem.CppName)
    self.WriteString('''
}
''')

@AddMethod(GenericSpec)
def WriteCDefnDtor(self, varname): pass

@AddMethod(PointerSpec)
def WriteCDefnDtor(self, varname):
    if self.policy in ("owner", "simple_owner"):
        self.WriteString('''
    delete %(:cVarName)s;''', cVarName=varname)

@AddMethod(ArraySpec)
def WriteCDefnDtor(self, varname):
    global CtorArrayDepth
    
    if self.type == 'virtual':
        return # no destruction necessary
    
    if self.ChildSpec.NeedsDestruction():
        if CtorArrayDepth == 0:
            self.WriteString('''

    // Deleting %(:varname)s's contents''', varname=varname)
        CtorArrayDepth += 1
        
        # different kinds of end condition per array type
        endCondition = ""
        indexType = "size_t"
        if self.type == "member":
            endCondition = '''index%(:depth)s < %(size)s'''
        elif self.type == "atRangeArray":
            endCondition = '''index%(:depth)s < (size_t)%(:varname)s.GetMaxCount()'''
            indexType = "int"
        elif self.type in ("atArray", "atFixedArray"):
            endCondition = '''index%(:depth)s < %(:varname)s.GetCount()'''
            indexType = "int"
        elif self.type == "pointer":
            if self.sizeVar != None:
                endCondition = '''%(:varname)s && index%(:depth)s < %(sizeVar)s'''
            else:
                endCondition = '''%(:varname)s && index%(:depth)s < %(size)s'''

        self.WriteString('''
    for(%(:indexType)s index%(:depth)s = 0; ''' + endCondition + '''; index%(:depth)s++) 
    {''', indexType=indexType, depth=CtorArrayDepth, varname=varname)

        GenericSpec.Depth += 1
        self.ChildSpec.WriteCDefnDtor(varname + "[index" + str(CtorArrayDepth) + "]")
        GenericSpec.Depth -= 1
 
        self.WriteString('''
    }''')
        CtorArrayDepth -= 1

    if self.type == 'pointer':
        self.WriteString('''
    delete [] %(:cVarName)s;''', cVarName=varname)
    
@AddMethod(StringSpec)
def WriteCDefnDtor(self, varname):
    if self.type in ('pointer', 'wide_pointer'):
        self.WriteString('''
    delete [] %(:cVarName)s;''', cVarName=varname)

@AddMethod(MapSpec)
def WriteCDefnDtor(self, varname):
    if self.type == 'atMap':
        self.WriteString('''
    {
        %(:cTypeDef)s::Iterator atMapIterator = %(CppName)s.CreateIterator();
        while(!atMapIterator.AtEnd())
        {
            delete atMapIterator.GetData();
            atMapIterator.Next();
        }
    }''', cTypeDef=self.GetCType())
    elif self.type == 'atBinaryMap':
        self.WriteString('''
    {
        ::rage::atArray< %(:cTypeDef)s::DataPair>& dataArray = %(CppName)s.GetRawDataArray();
        int count = dataArray.GetCount();
        for(int i = 0; i < count; i++)
        {
            delete dataArray[i].data;
        }
    }''', cTypeDef=self.GetCType())
    
    
CreateSimpleGetterFunctions("NeedsDestruction", {
    GenericSpec: False,
    })
    
@AddMethod(MapSpec)
def NeedsDestruction(self):
    if self.type in ('atMap', 'atBinaryMap'):
        if self.KeySpec.NeedsDestruction():
            CodeGenError("atMap key value %s says it needs to be destructed." % (self.KeySpec.GetCType()))
        return self.ChildSpec.NeedsDestruction()
    return False
    
@AddMethod(PointerSpec)
def NeedsDestruction(self):
    return self.policy in ('owner', 'simple_owner')
    
@AddMethod(ArraySpec)
def NeedsDestruction(self):
    if self.type == 'pointer':
        return True
    if self.type == 'virtual':
        return False
    return self.ChildSpec.NeedsDestruction()
    
@AddMethod(StringSpec)
def NeedsDestruction(self):
    if self.type in ('pointer', 'wide_pointer'):
        return True

        
