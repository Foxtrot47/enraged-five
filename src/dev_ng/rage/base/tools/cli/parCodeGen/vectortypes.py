from basetype import *

class VectorSpec(DataMemberSpec):
    def __init__(self, *args, **kwargs):
        self.step = None
        self.init = None
        self.highPrecision = None
        super(VectorSpec, self).__init__(*args, **kwargs)

    def CheckAttributes(self):
        self.WikiPage = "Vector_PSC_members"
        DataMemberSpec.CheckAttributes(self)
        self.OptionalAttribute("step", "0.1f")
        self.OptionalAttribute("init", "0.0f, 0.0f, 0.0f, 0.0f")
        self.OptionalAttribute("highPrecision", "false")
        if self.init:
            # make sure there are 4 elements in the init string
            initStr = self.init
            elems = initStr.split(",")
            if len(elems) == 1:
                # copy to all members
                elems = [elems[0], elems[0], elems[0], elems[0]]
            else:
                while(len(elems) < 4):
                    elems.append("0.0f")
            self.init = ",".join(elems)
            
class VecBoolVSpec(VectorSpec):
    def CheckAttributes(self):
        VectorSpec.CheckAttributes(self)
        self.init = "0.0f, 0.0f, 0.0f, 0.0f /* Init values for VecBoolV are currently unsupported */"
        

class Vector3Spec(VectorSpec):
    StdMin = "-FLT_MAX/1000.0f"
    StdMax = "+FLT_MAX/1000.0f"


class Vector3ColorSpec(Vector3Spec):
    StdMin = "0.0f"
    StdMax = "1.0f"

    def CheckAttributes(self):
        Vector3Spec.CheckAttributes(self)
        self.OptionalAttribute("step", "0.01f")

class Vector4Spec(VectorSpec):
    StdMin = "-FLT_MAX/1000.0f"
    StdMax = "+FLT_MAX/1000.0f"

class Vector2Spec(VectorSpec):
    StdMin = "-FLT_MAX/1000.0f"
    StdMax = "+FLT_MAX/1000.0f"


class Vec3VSpec(VectorSpec):
    StdMin = "-FLT_MAX/1000.0f"
    StdMax = "+FLT_MAX/1000.0f"


class Vec3VColorSpec(Vec3VSpec):
    StdMin = "0.0f"
    StdMax = "1.0f"

    def CheckAttributes(self):
        Vec3VSpec.CheckAttributes(self)
        self.OptionalAttribute("step", "0.01f")

class Vec4VSpec(VectorSpec):
    StdMin = "-FLT_MAX/1000.0f"
    StdMax = "+FLT_MAX/1000.0f"

class Vec2VSpec(VectorSpec):
    StdMin = "-FLT_MAX/1000.0f"
    StdMax = "+FLT_MAX/1000.0f"
    