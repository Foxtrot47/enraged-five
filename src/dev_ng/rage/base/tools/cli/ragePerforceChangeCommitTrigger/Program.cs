// Run "corflags /32bit+ ragePerforceChangeCommitTrigger.exe" or else it will choke on 64bit systems.
// There ought to be a way to fix the project to handle it, but this way does work.
using System;   
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;

using RSG.Base;
using RSG.Base.IO;

namespace ragePerforceChangeCommitTrigger
{
	class Program
	{
        // Default values for the configuration - this will be overridden
        // by the config file.
        static List<string> recipientAddresses = new List<string>();
        static List<string> subjectTags = new List<string>();
        static Dictionary<Regex, XmlNode> regexToSetting = new Dictionary<Regex, XmlNode>();
        static string cellTag;
        static string linkTag;
        static string bgColor = "#DDDDFF";
        static string fgColor = "";
        static string cssFile = "I:\\rage\\triggers\\styles-rage.css";    // Default, unless overridden.
        static string cssClass = "commit";
        static string codeReviewTag;
        static string p4UserName;
        static XmlDocument obConfig;



        static void ApplySettings(XmlNode settingsNode, String settingName)
        {
            XmlNodeList settingsList = settingsNode.ChildNodes;

            foreach (XmlNode setting in settingsList)
            {
                if (setting.Name.Equals("cssFile", StringComparison.CurrentCultureIgnoreCase))
                {
                    // Override the background color?
                    cssFile = setting.InnerXml;
                }
                if (setting.Name.Equals("cssClass", StringComparison.CurrentCultureIgnoreCase))
                {
                    // Override the background color?
                    cssClass = setting.InnerXml;
                }
                if (setting.Name.Equals("bgcolor", StringComparison.CurrentCultureIgnoreCase))
                {
                    // Override the background color?
                    bgColor = setting.InnerXml;
                }
                else if (setting.Name.Equals("fgcolor", StringComparison.CurrentCultureIgnoreCase))
                {
                    // Override the background color?
                    fgColor = setting.InnerXml;
                }
                else if (setting.Name.Equals("addrecipient", StringComparison.CurrentCultureIgnoreCase))
                {
                    // Add a new recipient?
                    string address = setting.InnerXml;

                    // Avoid duplicates.
                    if (!recipientAddresses.Contains(address))
                    {
                        recipientAddresses.Add(address);
                    }
                }
                else if (setting.Name.Equals("setrecipient", StringComparison.CurrentCultureIgnoreCase))
                {
                    // Override all existing recipients?
                    recipientAddresses.Clear();
                    recipientAddresses.Add(setting.InnerXml);
                }
                else if (setting.Name.Equals("addsubject", StringComparison.CurrentCultureIgnoreCase))
                {
                    string subjectTag = setting.InnerXml;

                    // Avoid duplicates.
                    if (!subjectTags.Contains(subjectTag))
                    {
                        subjectTags.Add(subjectTag);
                    }
                }
                else if (setting.Name.Equals("codereview", StringComparison.CurrentCultureIgnoreCase))
                {
                    XmlNodeList reviewInfo = setting.ChildNodes;

                    // Is this user required to do a code review?
                    XmlNode includeInfo = setting.SelectSingleNode("includegroup");

                    if (includeInfo != null)
                    {
                        // Check for the include group first. If the user is not in it, a code
                        // review is not required.
                        if (!IsUserInGroup(p4UserName, includeInfo.InnerText))
                        {
                            // Not in include list - user is exempt from code review.
                            continue;
                        }
                    }

                    XmlNode excludeInfo = setting.SelectSingleNode("excludegroup");

                    if (excludeInfo != null)
                    {
                        // Is the user is in the exclusion list, they're exempt.
                        if (IsUserInGroup(p4UserName, excludeInfo.InnerText))
                        {
                            // Not in include list - user is exempt from code review.
                            continue;
                        }
                    }

                    // Code review is required.
                    XmlNode tagName = setting.SelectSingleNode("tag");

                    if (tagName == null)
                    {
                        Console.WriteLine("codereview parameter for setting " + settingName + " does not have a tag");
                        System.Environment.Exit(1);
                        return;
                    }

                    codeReviewTag = tagName.InnerText;
                }
            }
        }

        static bool IsUserInGroup(String userName, String groupName)
        {
            XmlNodeList obGroups = obConfig.GetElementsByTagName("groups");

            if (obGroups == null || obGroups.Count < 1)
            {
                Console.WriteLine("The config file is missing the group definition");
                System.Environment.Exit(1);
            }

            XmlNode groupNode = obGroups[0].SelectSingleNode("group[attribute::name='" + groupName + "']");

            if (groupNode == null)
            {
                Console.WriteLine("Cannot find group named " + groupName);
                System.Environment.Exit(1);
            }

            XmlNodeList userList = groupNode.ChildNodes;

            foreach (XmlNode obUser in userList)
            {
                if (obUser.Name.Equals("user", StringComparison.CurrentCultureIgnoreCase))
                {
                    if (userName.Equals(obUser.InnerText, StringComparison.CurrentCultureIgnoreCase))
                    {
                        return true;
                    }
                }
            }

            return false;
        }


		static int Main(string[] args)
		{
            if (args.Length < 7)
            {
                Console.WriteLine("Usage: ragePerforceChangeCommitTrigger content|commit|changeform %changelist%|%changeform% %serverport% %user% weburl location configfile");
                Console.WriteLine("location is either sandiego or north currently.");
                return 1;
            }
			/* Console.WriteLine("ragePerforceChangeCommitTrigger args : ");
			foreach (string arg in args)
			{
				Console.WriteLine(arg);
			} */
            string strOperation = args[0];

            if (args[0] != "content" && args[0] != "commit" && args[0] != "changeform") {
                Console.WriteLine("First arg must be 'content' or 'commit' or 'changeform'.");
                return 1;
            }
            bool validateFiles = (args[0] == "content");
            bool sendEmail = (args[0] == "commit");
            bool modifyChangeForm = (args[0] == "changeform");
			string strChangeSet = args[1];
            string triggerUserName = "readonly";
            string smtpServer = "";

            // This is rsgsanpfp01.
            string p4WebServer = args[4]; // "http://rsgsanP4Web.rockstar.t2.corp:8080/";
            string location = args[5];

            // Read the config file.
            string configFilePath = args[6];

            obConfig = new XmlDocument();
            try
            {
                obConfig.Load(configFilePath);
            }
            catch (Exception e)
            {
                Console.WriteLine("Cannot read P4 trigger config file '" + configFilePath + "': " + e.Message);
                return 1;
            }

            // Parse basic settings, like weburl and location.
            XmlNodeList obSetup = obConfig.GetElementsByTagName("setup");

            if (obSetup.Count > 0)
            {
                // If we stumble over the setup node, parse it.
                XmlNode branch = obSetup[0];
                {
                    XmlNode locationInfo = branch.SelectSingleNode("location");

                    if (locationInfo != null)
                    {
                        location = locationInfo.InnerText;
                    }

                    XmlNode webUrlInfo = branch.SelectSingleNode("weburl");

                    if (webUrlInfo != null)
                    {
                        p4WebServer = webUrlInfo.InnerText;
                    }

                    XmlNode triggerUserInfo = branch.SelectSingleNode("triggeruser");

                    if (triggerUserInfo != null)
                    {
                        triggerUserName = triggerUserInfo.InnerText;
                    }

                    XmlNode smtpServerInfo = branch.SelectSingleNode("smtpserver");

                    if (smtpServerInfo != null)
                    {
                        smtpServer = smtpServerInfo.InnerText;
                    }
                }
            }


            XmlNodeList obSettingsList = obConfig.GetElementsByTagName("settings");

            if (obSettingsList.Count < 1)
            {
                Console.WriteLine("Config file doesn't have a settings list.");
                return 1;
            }

            XmlNode obSettings = obSettingsList[0];

            // Parse the config file.
            XmlNodeList obBranchlistList = obConfig.GetElementsByTagName("branchlist");

            if (obBranchlistList.Count < 1)
            {
                Console.WriteLine("Config file doesn't have a branch list.");
                return 1;
            }

            XmlNodeList obBranchList = obBranchlistList[0].ChildNodes;

            // Verify the branch list and read the config at the same time.
            foreach (XmlNode branch in obBranchList)
            {
                if (branch.Name.Equals("branch", StringComparison.CurrentCultureIgnoreCase))
                {
                    if (branch.SelectSingleNode("prefix") == null)
                    {
                        Console.WriteLine("Branch doesn't have a prefix tag");
                        return 1;
                    }

                    if (branch.SelectSingleNode("setting") == null)
                    {
                        Console.WriteLine("Branch doesn't have a setting tag");
                        return 1;
                    }
                }

                if (branch.Name.Equals("description", StringComparison.CurrentCultureIgnoreCase))
                {
                    XmlNode descriptionInfo = branch.SelectSingleNode("regex");
                    XmlNode settingInfo = branch.SelectSingleNode("setting");
                    
                    if (descriptionInfo == null)
                    {
                        Console.WriteLine("Description definition doesn't have a regex tag");
                        return 1;
                    }

                    if (settingInfo == null)
                    {
                        Console.WriteLine("Description definition doesn't have a setting tag");
                        return 1;
                    }

                    Regex regex = new Regex(descriptionInfo.InnerXml);
                    XmlNode settingsNode = obSettings.SelectSingleNode("setting[attribute::name='" + settingInfo.InnerText + "']");

                    if (settingsNode == null)
                    {
                        Console.WriteLine("Description definition " + descriptionInfo.InnerXml + " refers to unknown setting " + settingInfo.InnerText);
                        return 1;
                    }

                    regexToSetting[regex] = settingsNode;
                }
            }

            // Find the default settings.
            XmlNode defaultSettings = obSettings.SelectSingleNode("setting[attribute::name='default']");

            if (defaultSettings != null)
            {
                ApplySettings(defaultSettings, "default");
            }

            string p4CommonArgs = "-p " + args[2] + " -u " + triggerUserName + " ";

            // Acquire a ticket if this is not the read-only account.
            if (triggerUserName != "readonly")
            {
                Process loginProcess = new Process();
                // Redirect the output stream of the child process.
                loginProcess.StartInfo.UseShellExecute = false;
                loginProcess.StartInfo.RedirectStandardInput = true;
                loginProcess.StartInfo.RedirectStandardOutput = true;
                loginProcess.StartInfo.RedirectStandardError = true;
                loginProcess.StartInfo.FileName = "p4.exe";
                loginProcess.StartInfo.Arguments = p4CommonArgs + "login"; // note that the connection properties are hardcoded here

                // DateTime timer = DateTime.Now;

                //Console.WriteLine("Running: " + loginProcess.StartInfo.FileName + " " + loginProcess.StartInfo.Arguments);

                loginProcess.Start();
                // Do not wait for the child process to exit before
                // reading to the end of its redirected stream.
                // describerProcess.WaitForExit();
                // Read the output stream first and then wait.
                loginProcess.StandardInput.WriteLine("");
                //Console.WriteLine("StandardOutput: " + loginProcess.StandardOutput.ReadToEnd());
                //Console.WriteLine("StandardError: " + loginProcess.StandardError.ReadToEnd());
                loginProcess.WaitForExit();
            }


            if (modifyChangeForm)
            {
                return ModifyChangeForm(p4CommonArgs, strChangeSet);
            }



			// Get a description of the change
            // Start the child process.
            // ms-help://MS.VSCC.v80/MS.MSDN.v80/MS.NETDEVFX.v20.en/cpref6/html/P_System_Diagnostics_Process_StandardOutput.htm
            Process describerProcess = new Process();
            // Redirect the output stream of the child process.
            describerProcess.StartInfo.UseShellExecute = false;
            describerProcess.StartInfo.RedirectStandardOutput = true;
            describerProcess.StartInfo.RedirectStandardError = true;
            describerProcess.StartInfo.FileName = "p4.exe";
            describerProcess.StartInfo.Arguments = p4CommonArgs + "describe -s " + strChangeSet; // note that the connection properties are hardcoded here

            // DateTime timer = DateTime.Now;

            describerProcess.Start();
            // Do not wait for the child process to exit before
            // reading to the end of its redirected stream.
            // describerProcess.WaitForExit();
            // Read the output stream first and then wait.
            string describeOutput = describerProcess.StandardOutput.ReadToEnd();
            string describeError = describerProcess.StandardError.ReadToEnd();
            describerProcess.WaitForExit();
            
            // Console.WriteLine("p4 describe in " + (DateTime.Now - timer).TotalSeconds);

			// Make the results pretty using HTML
			List<string> obAStrHTMLDescription = new List<string>();
			List<string> obAStrAffectedFiles = new List<string>();
			List<string> obAStrSections = new List<string>();
            string[] obAStrDescription = describeOutput.Split(new String[] { "\r\n" }, StringSplitOptions.None);

            // We expect at least two lines here, or else something went wrong.
            if (obAStrDescription.Length < 2)
            {
                Console.WriteLine("Something went wrong when grabbing the changeset info via " + describerProcess.StartInfo.Arguments);

                if (obAStrDescription.Length > 0)
                {
                    Console.WriteLine(obAStrDescription[0]);
                }

                Console.WriteLine(describeError);

                return 1;
            }


            // timer = DateTime.Now;
            // Find the sender (in the first line)
            // Change xxxx by user@client
            string sender = "ragecommit@rockstarsandiego.com";
            string senderFullName = "Rage Commit";

            string firstLine = obAStrDescription[0];
            string [] tokens = firstLine.Split(' ');
            if (tokens.Length >= 4 && tokens[3].Contains("@"))
            {
                string [] moreTokens = tokens[3].Split('@');

                Process senderProcess = new Process();
                // Redirect the output stream of the child process.
                senderProcess.StartInfo.UseShellExecute = false;
                senderProcess.StartInfo.RedirectStandardOutput = true;
                senderProcess.StartInfo.FileName = "p4.exe";
                senderProcess.StartInfo.Arguments = p4CommonArgs + "user -o " + moreTokens[0];

                p4UserName = moreTokens[0];

                senderProcess.Start();
                string senderOutput = senderProcess.StandardOutput.ReadToEnd();
                senderProcess.WaitForExit();

                string [] obAStrUser = senderOutput.Split(new String[] { "\r\n" }, StringSplitOptions.None);
                foreach (string strItem in obAStrUser)
                {
                    if (strItem.StartsWith("Email:"))
                    {
                        string [] stillMoreTokens = strItem.Split('\t');
                        sender = stillMoreTokens[1];
                    }

                    if (strItem.StartsWith("FullName:"))
                    {
                        string[] stillMoreTokens = strItem.Split('\t');
                        senderFullName = stillMoreTokens[1];
                    }
                }
            }

            // Create a friendly name for the sender.
            string senderDisplayName = senderFullName + " (" + rageEmailUtilities.GetStudioNameFromEmail(sender) + ")";

            // Console.WriteLine("find sender in " + (DateTime.Now - timer).TotalSeconds);

            // Apply all settings first.
            bool bLogFiles = false;
            foreach (string origStrDescriptionLine in obAStrDescription)
            {
                string strDescriptionLine = origStrDescriptionLine.Clone() as string;

                // Check all description regexes.
                foreach (KeyValuePair<Regex, XmlNode> kvp in regexToSetting)
                {
                    if (kvp.Key.Match(strDescriptionLine).Success)
                    {
                        ApplySettings(kvp.Value, kvp.Value.Attributes.GetNamedItem("name").Name);
                    }
                }

                if (bLogFiles && (strDescriptionLine.Length > 1))
                {
                    int iFilenameStart = 4;
                    int iFilenameEnd = strDescriptionLine.LastIndexOf('#');
                    if (iFilenameEnd == -1) iFilenameEnd = strDescriptionLine.Length;
                    string filename = strDescriptionLine.Substring(iFilenameStart, iFilenameEnd - iFilenameStart);


                    string strLowercased = strDescriptionLine.ToLower();

                    // Find out which branch applies here.
                    foreach (XmlNode branch in obBranchList)
                    {
                        if (branch.Name.Equals("branch", StringComparison.CurrentCultureIgnoreCase))
                        {
                            XmlNode prefixInfo = branch.SelectSingleNode("prefix");
                            String branchTest = "... " + prefixInfo.InnerText;

                            if (strLowercased.StartsWith(branchTest))
                            {
                                // This test applies. Use the settings.
                                XmlNode settingsInfo = branch.SelectSingleNode("setting");

                                // Find that steting.
                                XmlNode settingsNode = obSettings.SelectSingleNode("setting[attribute::name='" + settingsInfo.InnerText + "']");

                                if (settingsNode == null)
                                {
                                    Console.WriteLine("Cannot find setting " + settingsInfo.InnerText);
                                    return 1;
                                }

                                ApplySettings(settingsNode, settingsInfo.InnerText);

                                // If this branch is marked "final", stop processing
                                // any further rules.
                                if (branch.Attributes.GetNamedItem("final") != null)
                                {
                                    break;
                                }
                            }
                        }
                    }
                }

                if (strDescriptionLine == "Affected files ...")
                {
                    bLogFiles = true;
                }
            }

            cellTag = "";

            if (bgColor.Length > 0)
            {
                cellTag += " bgcolor=" + bgColor;
            }

            if (cssClass.Length > 0)
            {
                cellTag += " class=\"" + cssClass + "\"";
            }

            linkTag = "";

            if (fgColor.Length > 0)
            {
                linkTag = "style=\"color:" + fgColor + "\"";
            }

    		// Open a cell
            obAStrHTMLDescription.Add("<tr valign=TOP><td align=left" + cellTag + ">");

            if (fgColor.Length > 0)
            {
                obAStrHTMLDescription.Add("<font color=" + fgColor + ">");
            }

			// Add description
			bLogFiles = false;
            string linePrefix = "<b>";
            string lineSuffix = "</b>";
            int lineCount = 0;

            // timer = DateTime.Now;
            Regex rdr2ArtToolFixTest = new Regex("Art\\s+Tools?\\s+fix", RegexOptions.IgnoreCase);

            bool bCodeReviewRequired = (codeReviewTag != null && codeReviewTag.Length > 0);
            bool bCodeReviewDone = false;

            int exitCode = 0;
			foreach (string origStrDescriptionLine in obAStrDescription)
			{
                string strDescriptionLine = origStrDescriptionLine.Clone() as string;

                if (CheckForIntegratedChangesets(strDescriptionLine, ref obAStrHTMLDescription, p4CommonArgs, p4WebServer, strChangeSet))
                {
                    // An integration doesn't require a code review.
                    bCodeReviewDone = true;
                    continue;
                }

                // Check all description regexes.
                foreach (KeyValuePair<Regex, XmlNode> kvp in regexToSetting)
                {
                    if (kvp.Key.Match(strDescriptionLine).Success)
                    {
                        strDescriptionLine = "\t<B>" + strDescriptionLine.Substring(1) + "</b>";
                    }
                }

                // change < and > into &lt; and &gt; unless they're part of
                // <code>, </code>, <i>, </i>, <b>, </b>, etc.
                strDescriptionLine = Regex.Replace(strDescriptionLine, @"<(?!/?(A(\s+HREF.*)?|CODE|PRE|I|B|TT|LIST|LI|SUB|SUP|FONT[^>]*|EM|STRONG)>)", "&lt;", RegexOptions.IgnoreCase);
                strDescriptionLine = Regex.Replace(strDescriptionLine, @"(?<!</?(A(\s+HREF.*)?|CODE|PRE|I|B|TT|LIST|LI|SUB|SUP|FONT[^>]*|EM|STRONG))>", "&gt;", RegexOptions.IgnoreCase);

                // Replace B*NNNNN with <a href="bugstar:NNNN">B*NNNNN</a>
                // Parens are used in the source string to force the matches to be in separate groups, and $2 references to the digits only in the replacement part.
                strDescriptionLine = Regex.Replace(strDescriptionLine, "(B\\*|B\\* |bugstar:)(\\d+)", "<a href=\"bugstar:$2\">Bug $2</a>" );

				if (strDescriptionLine == "Affected files ...")
				{
					// New thing, so table it
					obAStrSections.Add(strDescriptionLine.Replace(" ...", ""));

					// Close a cell
					obAStrHTMLDescription.Add("</pre>");
                    if (fgColor.Length > 0)
                    {
                        obAStrHTMLDescription.Add("</font>");
			    	}
					
					obAStrHTMLDescription.Add("</td></tr>");

					// Fill in the cell 

// If keeping the table, try this.
/*
					obAStrHTMLDescription.Add("<tr valign=TOP><td align=left bgcolor=#CCFFCC>");
					obAStrHTMLDescription.Add("<a name=\"" + strDescriptionLine.Replace(" ...", "").Replace(" ", "_") + "\"><b>");
					obAStrHTMLDescription.Add(strDescriptionLine +"</b></td></tr>");
*/

// If killing the table, try this.

					obAStrHTMLDescription.Add("</table><br>");
					obAStrHTMLDescription.Add("<a name=\"" + strDescriptionLine.Replace(" ...", "").Replace(" ", "_") + "\"><b>");
					obAStrHTMLDescription.Add(strDescriptionLine +"</b><br><br>");
        			obAStrHTMLDescription.Add("<table border=1 cellspacing=1 cellpadding=10 width=100%>");


					// Open a cell
					obAStrHTMLDescription.Add("<tr valign=TOP><td align=left" + cellTag + ">");

                    if (fgColor.Length > 0)
                    {
                        obAStrHTMLDescription.Add("<font color=" + fgColor + ">");
                    }

                    obAStrHTMLDescription.Add("<pre>");

                    bLogFiles = true;
				}
				else
				{
                    if (sendEmail && strDescriptionLine.Contains("Automatically generated by cruise control"))
                        sendEmail = false;

                    if (!bLogFiles)
                    {
                        obAStrHTMLDescription.Add(linePrefix + strDescriptionLine + lineSuffix);
                        obAStrHTMLDescription.Add("<br>");

                        if (validateFiles && (strDescriptionLine.Contains("DO NOT SUBMIT") || strDescriptionLine.Contains("DO NOT COMMIT")))
                        {
                            Console.WriteLine("Description contains DO NOT SUBMIT marker, did you mean to submit this?");
                            exitCode = 1;
                        }

                        if (bCodeReviewRequired && !bCodeReviewDone && strDescriptionLine.Trim().StartsWith(codeReviewTag, StringComparison.CurrentCultureIgnoreCase))
                        {
                            bCodeReviewDone = true;
                        }
                    }
					if (bLogFiles && (strDescriptionLine.Length > 1))
					{
                        int iFilenameStart = 4;
						int iFilenameEnd = strDescriptionLine.LastIndexOf('#');
						if(iFilenameEnd == -1) iFilenameEnd = strDescriptionLine.Length;
                        string filename = strDescriptionLine.Substring(iFilenameStart, iFilenameEnd - iFilenameStart);
                        bool thirdParty = filename.Contains("3rdParty") || filename.Contains("3rdparty") || filename.Contains("/cell/");

                        if (++lineCount == 100)
                            obAStrHTMLDescription.Add(linePrefix + "(list truncated after 100 lines)" + lineSuffix);

                        if (lineCount < 100)
                            obAStrAffectedFiles.Add(filename);

                        if (validateFiles && !thirdParty && (filename.Contains("/MSSCCPRJ.SCC") || filename.EndsWith(".suo") || filename.EndsWith(".ncb") ||
                            filename.EndsWith("_parser.h") || filename.EndsWith("_parser.xsd") ||
                            (filename.Contains("/embedded_") && (filename.EndsWith("_win32_30.h") || filename.EndsWith("_fxl_final.h") || filename.EndsWith("_psn.h") || filename.EndsWith("_dcl.h"))) ||
                            filename.Contains(".user") || filename.EndsWith(".ilk") || filename.EndsWith(".exp"))
                            )
                        {
                            if (strDescriptionLine.Contains(" add"))
                            {
                                Console.WriteLine("Invalid filename for add: " + filename);
                                exitCode = 1;
                            }
                        }

                        if (validateFiles && filename.EndsWith(".vcproj") && !thirdParty)
                        {
                            exitCode = Math.Max(exitCode, ValidateVcproj(p4CommonArgs, filename, strChangeSet));
                        }


                        // only check certain files, and only add or edit, to avoid slowing down large integrates.
                        if (validateFiles && !thirdParty && (filename.EndsWith(".c") || filename.EndsWith(".cpp") || filename.EndsWith(".inl") ||
                                ( filename.EndsWith(".cs") && filename.ToLower().Contains("rageperforcechangecommittrigger") == false ) || 
                                filename.EndsWith(".h") || filename.EndsWith(".fx") || filename.EndsWith(".fxh") || filename.EndsWith(".sc") || filename.EndsWith(".sch"))
                            && (strDescriptionLine.Contains(" add") || strDescriptionLine.Contains(" edit")))
                        {
                            // p4 print filename@=%changelist%
                            Process printerProcess = new Process();
                            // Redirect the output stream of the child process.
                            printerProcess.StartInfo.UseShellExecute = false;
                            printerProcess.StartInfo.RedirectStandardOutput = true;
                            printerProcess.StartInfo.RedirectStandardError = true;
                            printerProcess.StartInfo.FileName = "p4.exe";
                            printerProcess.StartInfo.Arguments = p4CommonArgs + "print \"" + filename + "\"@=" + strChangeSet;

                            printerProcess.Start();
                            string printerOutput = printerProcess.StandardOutput.ReadToEnd();
                            string printerError = printerProcess.StandardError.ReadToEnd();
                            printerProcess.WaitForExit();

                            if (printerError.Length != 0)
                            {
                                Console.WriteLine("Error when validating file - executing " + printerProcess.StartInfo.Arguments);
                                Console.WriteLine(printerError);
                                return 1;
                            }

                            if (printerOutput.Length != 0)
                            {
                                string[] printerLines = printerOutput.Split(new String[] { "\r\n" }, StringSplitOptions.None);

                                int line = -1;   // skip the header line p4 print displays
                                foreach (string printerLine in printerLines)
                                {
                                    ++line;
                                    if (printerLine.Length == 0)
                                        continue;

                                    /* int col = 1;
                                    for (int i = 0; i < printerLine.Length; i++)
                                    {
                                        if (printerLine[i] < 1 || printerLine[i] > 126)
                                        {
                                            Console.WriteLine("File contains NON-ASCII character '" + printerLine[i] + "' (line " + line + ", col " + col + "): " + filename);
                                            ++col;
                                            exitCode = 1;
                                        }
                                        else if (printerLine[i] == 9)
                                            col = (((col - 1) + 4) & ~3) + 1; // four space tabs
                                        else
                                            ++col;
                                    } */

                                    if (printerLine.Contains("DO NOT SUBMIT") || printerLine.Contains("DO NOT COMMIT"))
                                    {
                                        Console.WriteLine("File contains DO NOT SUBMIT/COMMIT string (line " + line + "): " + filename);
                                        exitCode = 1;
                                    }
                                    if (printerLine[0] != '/' && (printerLine.Contains("pragma") && printerLine.Contains("optimize") && printerLine.Contains("off")) && !printerLine.Contains("PRAGMA-OPTIMIZE-ALLOW"))
                                    {
                                        Console.WriteLine("File contains pragma optimize off (line " + line + "): " + filename);
                                        Console.WriteLine("(Use magic keyword PRAGMA-OPTIMIZE-ALLOW on the same line to allow this anyway)");
                                        exitCode = 1;
                                    }
                                    if (printerLine[0] != '/' && printerLine.Contains("ragma") && printerLine.Contains("O=0") && !printerLine.Contains("PRAGMA-OPTIMIZE-ALLOW"))
                                    {
                                        Console.WriteLine("File contains pragma control (O=0) (line " + line + "): " + filename);
                                        Console.WriteLine("(Use magic keyword PRAGMA-OPTIMIZE-ALLOW on the same line to allow this anyway)");
                                        exitCode = 1;
                                    }
                                    if (printerLine[0] != '/' && printerLine.StartsWith("OPTIMISATIONS_OFF") && !printerLine.Contains("PRAGMA-OPTIMIZE-ALLOW"))
                                    {
                                        Console.WriteLine("File contains OPTIMISATIONS_OFF directive (line " + line + "): " + filename);
                                        Console.WriteLine("(Use magic keyword PRAGMA-OPTIMIZE-ALLOW on the same line to allow this anyway)");
                                        exitCode = 1;
                                    }
                                    if (printerLine[0] != '/' && printerLine.StartsWith("OPTIMIZATIONS_OFF") && !printerLine.Contains("PRAGMA-OPTIMIZE-ALLOW"))
                                    {
                                        Console.WriteLine("File contains OPTIMIZATIONS_OFF directive (line " + line + "): " + filename);
                                        Console.WriteLine("(Use magic keyword PRAGMA-OPTIMIZE-ALLOW on the same line to allow this anyway)");
                                        exitCode = 1;
                                    }
                                    if (printerLine[0] != '/' && printerLine.Contains("PRAGMA_OPTIMIZE_OFF") && !printerLine.Contains("PRAGMA-OPTIMIZE-ALLOW"))
                                    {
                                        Console.WriteLine("File contains PRAGMA_OPTIMIZATION_OFF directive (line " + line + "): " + filename);
                                        Console.WriteLine("(Use magic keyword PRAGMA-OPTIMIZE-ALLOW on the same line to allow this anyway)");
                                        exitCode = 1;
                                    }
                                }
                            }
                        }

                        if (lineCount < 100)
                            obAStrHTMLDescription.Add(linePrefix + "<a href=\"" + p4WebServer + "@md=d&cd=//&c=0lc@" + filename + "?ac=22\" " + linkTag + ">" + strDescriptionLine.Substring(4) + "</a>" + lineSuffix);
                    }
                    linePrefix = "";
                    lineSuffix = "";
				}
			}

            if (bCodeReviewRequired && !bCodeReviewDone)
            {
                Console.WriteLine("Please do a code review before comming! Write '" + codeReviewTag + "' with the name of the reviewer IN A SEPARATE LINE in your description.");
                return 1;
            }

            // Console.WriteLine("parse description in " + (DateTime.Now - timer).TotalSeconds);

            if (exitCode != 0)
            {
                return exitCode;
            }

			// Close a cell
            obAStrHTMLDescription.Add("</pre>");
            if (fgColor.Length > 0)
            {
                obAStrHTMLDescription.Add("</font>");
			}
			
            obAStrHTMLDescription.Add("</td></tr>");

			// Create header
			List<string> astrHeader = new List<string>();
			astrHeader.Add("<html><head><title>Change " + strChangeSet + "</title>");

            // Add the style sheet template.
            StreamReader reader;    
            try
            {
                reader = new StreamReader(cssFile);
            }
            catch (Exception e)
            {
                Console.WriteLine("Cannot read P4 trigger css file '" + cssFile+ "': " + e.Message);
                return 1;
            }

            if (reader == null)
            {
                Console.WriteLine("Cannot read P4 trigger css file '" + cssFile + "'");
                return 1;
            }

            string cssLine;

            while ((cssLine = reader.ReadLine()) != null)
            {
                astrHeader.Add(cssLine);
            }

            reader.Close();

            astrHeader.Add("</head><body>");

			astrHeader.Add("<h1>Change Set " + ChangeSetHTMLLink(strChangeSet, p4WebServer) + "</h1>");
/*
			foreach (String strSection in obAStrSections) 
			{
				astrHeader.Add("[<a href=\"#"+ strSection.Replace(" ", "_") +"\">"+ strSection +"</a>]");
			}
*/
			astrHeader.Add("<table border=1 cellspacing=1 cellpadding=10 width=100%>");

			// Insert header
			obAStrHTMLDescription.InsertRange(0, astrHeader);

			// Write footer
			obAStrHTMLDescription.Add("</table>\n");
			obAStrHTMLDescription.Add("</body>\n");

            // timer = DateTime.Now;
			// Generate subject line
			String strSubject = "submit: [" + strChangeSet + "] ";
			foreach (String strAffectedFile in obAStrAffectedFiles)
			{
                String thisFile = " " + rageFileUtilities.GetFilenameFromFilePath(rageFileUtilities.GetLocationFromPath(strAffectedFile));
                if (!strSubject.Contains(thisFile))
                    strSubject += thisFile;

                if (strSubject.Length > 128)
                {
                    strSubject += " ...";
                    break;
                }
			}
            // Console.WriteLine("generate subject line in " + (DateTime.Now - timer).TotalSeconds);

            // Ignore it if we have no clue.
            if (recipientAddresses.Count == 0)
                sendEmail = false;

            string strPrefix = "";

            foreach (string subjectTag in subjectTags)
            {
                strPrefix += subjectTag + " ";
            }

            strSubject = strPrefix + strSubject;
            string recipients = "";

            foreach (string recipient in recipientAddresses)
            {
                recipients += recipient + " ";
            }

            // DEBUG CODE, remove before submit:
            /////strSubject += recipients;
            // For debugging, uncomment this line with YOUR email address in it.
            /////// recipients = "etherton@rockstarsandiego.com";
            /////// recipients = "mkrehan@rockstarsandiego.com";
            /////// recipients = "rschaaf@rockstarsandiego.com";

			// Email it to people
            // timer = DateTime.Now;
            if (sendEmail) {
                if (smtpServer != "")
                {
                    rageEmailUtilities.SmtpServer = smtpServer;
                    rageEmailUtilities.UseSmtpAuth = false;
                }


                rageEmailUtilities.SendEmail(recipients, sender, strSubject, senderDisplayName, obAStrHTMLDescription);
            // Console.WriteLine("send email in " + (DateTime.Now - timer).TotalSeconds);
            }

            // Success!
            return 0;
		}


        static string[] GetChangesetDesc(string changeset, String p4CommonArgs)
        {
            Process getChangesetProcess = new Process();

            getChangesetProcess.StartInfo.UseShellExecute = false;
            getChangesetProcess.StartInfo.RedirectStandardOutput = true;
            getChangesetProcess.StartInfo.FileName = "p4.exe";
            getChangesetProcess.StartInfo.Arguments = p4CommonArgs + "describe -s " + changeset;

            getChangesetProcess.Start();
            String getChangesetOutput = getChangesetProcess.StandardOutput.ReadToEnd();
            getChangesetProcess.WaitForExit();

            return getChangesetOutput.Split(new String[] { "\r\n" }, StringSplitOptions.None);
        }


        static bool CheckForIntegratedChangesets(String line, ref List<string> oOutput, String p4CommonArgs, String p4WebServer, String strChangeset)
        {
            Regex IntegrationRegexTest = new Regex("(\\*\\*\\s*INTEGRATED? (\\d+)\\s*\\*\\*)|(^\\s*(\\d\\d\\d\\d\\d+)\\s*$)", RegexOptions.IgnoreCase);
            Regex IntegrationMarkerStart = new Regex("^\\s*==== INTEGRATED ");
            Regex IntegrationMarkerEnd = new Regex("^\\s*================");

            Match testStart = IntegrationMarkerStart.Match(line);

            if (testStart.Success)
            {
                oOutput.Add("<b>" + line + "</b><br><blockquote><font color=#666666>");
                return true;
            }

            Match testEnd = IntegrationMarkerEnd.Match(line);

            if (testEnd.Success)
            {
                oOutput.Add("</font></blockquote>");
                return true;
            }

            Match result = IntegrationRegexTest.Match(line);

            if (result.Success && result.Groups.Count > 2)
            {
                String integChangeset = result.Groups[2].ToString();

                if (integChangeset == "")
                {
                    integChangeset = result.Groups[4].ToString();
                }

                Process integChangesetProcess = new Process();

                integChangesetProcess.StartInfo.UseShellExecute = false;
                integChangesetProcess.StartInfo.RedirectStandardOutput = true;
                integChangesetProcess.StartInfo.FileName = "p4.exe";
                integChangesetProcess.StartInfo.Arguments = p4CommonArgs + "describe -s " + integChangeset;

                integChangesetProcess.Start();
                String integChangesetOutput = integChangesetProcess.StandardOutput.ReadToEnd();
                integChangesetProcess.WaitForExit();

                string[] oIntegDescription = integChangesetOutput.Split(new String[] { "\r\n" }, StringSplitOptions.None);

                oOutput.Add("<b>INTEGRATED CHANGESET " + ChangeSetHTMLLink(integChangeset, p4WebServer) + "</b><br>");
                oOutput.Add("<br><blockquote><u><font color=#666666>");

                bool firstline = true;

                foreach (string strDescriptionLine in oIntegDescription)
                {
                    if (strDescriptionLine == "Affected files ...")
                    {
                        break;
                    }

                    oOutput.Add(strDescriptionLine + "<br>");

                    if (firstline)
                    {
                        oOutput.Add("</u>");
                        firstline = false;
                    }
                }

                oOutput.Add("</font></blockquote><br>");
            }

            return false;
        }

        static void SendErrorDebuggingMail(string errorString)
        {
            string recipients = "mkrehan@rockstarsandiego.com";
            string sender = "ragecommit@rockstarsandiego.com";
            string strSubject = "RAGE Perforce Trigger ERROR";
            string[] obAStrHTMLDescription = new string[1];
            obAStrHTMLDescription[0] = errorString;
            rageEmailUtilities.SendEmail(recipients, sender, strSubject, obAStrHTMLDescription);
        }

        static String ChangeSetHTMLLink(String strChangeSet, String p4WebServer)
        {
            return "<a href=\"" + p4WebServer + "@md=d&cd=//&c=7FM@/" + strChangeSet + "?ac=10\" " + linkTag + ">" + strChangeSet + "</a>";
        }

        static int ModifyChangeForm(String p4CommonArgs, string formFile)
        {
            // Read the form.
            List<string> oForm = new List<string>();
            bool handlingDescription = false;
            bool modifiedForm = true;
            Regex descStartRegex = new Regex("^\\s*Description:", RegexOptions.IgnoreCase);
            Regex IntegrationRegexTest = new Regex("(\\*\\*\\s*INTEGRATED? (\\d+)\\s*\\*\\*)|(^\\s*(\\d\\d\\d\\d\\d+)\\s*$)", RegexOptions.IgnoreCase);

            try
            {
                StreamReader sr = new StreamReader(formFile);
                String line;

                while ((line = sr.ReadLine()) != null)
                {
                    // Are we parsing the description right now?
                    if (handlingDescription)
                    {
                        // Yes - replace stuff as necessary.
                        Match result = IntegrationRegexTest.Match(line);

                        if (result.Success && result.Groups.Count > 2)
                        {
                            String integChangeset = result.Groups[2].ToString();

                            if (integChangeset == "")
                            {
                                integChangeset = result.Groups[4].ToString();
                            }

                            oForm.Add("\t==== INTEGRATED " + integChangeset + " ====");

                            // Grab the actual description.
                            string[] originalChangesetDesc = GetChangesetDesc(integChangeset, p4CommonArgs);

                            foreach (String descLine in originalChangesetDesc)
                            {
                                // Don't display that crap about affected files.
                                if (descLine.StartsWith("Affected files"))
                                {
                                    break;
                                }

                                oForm.Add("\t" + descLine.TrimStart(null));
                            }

                            oForm.Add("\t===========================");
                            modifiedForm = true;
                            continue;
                        }
                    }
                    else
                    {
                        // We haven't hit the description line yet.
                        Match result = descStartRegex.Match(line);

                        if (result.Success)
                        {
                            // Now we have.
                            handlingDescription = true;
                        }
                    }

                    oForm.Add(line);
                }

                sr.Close();
            }
            catch(Exception e)
            {
                string error = "Could not open change form file: " + formFile + "\r\n";
                error += "Exception: " + e.Message;

                SendErrorDebuggingMail(error);
                return 0;
            }

            if (!handlingDescription)
            {
                string error = "Could not find a description in change form file: " + formFile + "\r\n";

                SendErrorDebuggingMail(error);
                return 0;
            }

            if (modifiedForm)
            {
                // Now write the changed version of the file.
                try
                {
                    StreamWriter sw = new StreamWriter(formFile);

                    foreach (string line in oForm)
                    {
                        sw.WriteLine(line);
                    }

                    sw.Close();
                }
                catch (Exception e)
                {
                    string error = "Could not write change form file: " + formFile + "\r\n";
                    error += "Exception: " + e.Message;

                    SendErrorDebuggingMail(error);
                    return 0;
                }
            }



            return 0;
        }

        static int ValidateVcproj(String p4CommonArgs, String filename, String strChangeSet)
        {
            int exitCode = 0;
             // p4 print filename@=%changelist%
            Process printerProcess = new Process();
            // Redirect the output stream of the child process.
            printerProcess.StartInfo.UseShellExecute = false;
            printerProcess.StartInfo.RedirectStandardOutput = true;
            printerProcess.StartInfo.RedirectStandardError = true;
            printerProcess.StartInfo.FileName = "p4.exe";
            printerProcess.StartInfo.Arguments = p4CommonArgs + "print \"" + filename + "\"@=" + strChangeSet;

            printerProcess.Start();
            string printerOutput = printerProcess.StandardOutput.ReadToEnd();
            string printerError = printerProcess.StandardError.ReadToEnd();
            printerProcess.WaitForExit();

            if (printerError.Length != 0)
            {
                Console.WriteLine("Error when validating file - executing " + printerProcess.StartInfo.Arguments);
                Console.WriteLine(printerError);
                return 1;
            }

            Regex fileStartRegEx = new Regex("\\s*<File\\s*", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            Regex fileEndRegEx = new Regex("\\s*</File>\\s*", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            Regex fileRegEx = new Regex("\\s*RelativePath=\"([^\"]*)\"", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            Regex nameRegEx = new Regex("\\s*Name=\"([^\"]*)\"", RegexOptions.Compiled | RegexOptions.IgnoreCase);

            if (printerOutput.Length != 0)
            {
                string[] printerLines = printerOutput.Split(new String[] { "\r\n" }, StringSplitOptions.None);

                int line = -1;   // skip the header line p4 print displays
                int file = 0;
                int tool = 0;
                String optimisedFile = "";
                String toolName = "";
                foreach (string printerLine in printerLines)
                {
                    ++line;
                    if (printerLine.Length == 0)
                        continue;

                    // Don't do any additional checking if we detect a SPURS project.
                    if (printerLine.Contains("SN_TARGET_PS3_SPU"))
                        break;

                    if (fileStartRegEx.Match(printerLine).Success)
                    {
                        file++;
                    }
                    else if (fileEndRegEx.Match(printerLine).Success)
                    {
                        optimisedFile = "";
                        file--;
                    }

                    if (file > 0) {
                        Match result = fileRegEx.Match(printerLine);
                        if (result.Success && result.Groups.Count > 1)
                        {
                            optimisedFile = result.Groups[1].ToString();
                        }

                        if (printerLine.Contains("<Tool"))
                            tool++;

                        if (tool > 0)
                        {
                            if (printerLine.Contains("/>"))
                            {
                                tool--;
                                toolName = "";
                            }
                            else
                            {
                                Match nameResult = nameRegEx.Match(printerLine);
                                if (nameResult.Success && nameResult.Groups.Count > 1)
                                {
                                    toolName = nameResult.Groups[1].ToString();
                                }

                                // weak
                                if ((toolName == "VCCLX360CompilerTool" && printerLine.Contains("Optimization=\"")) ||
                                    (toolName == "VCCLCompilerTool" && printerLine.Contains("AdditionalOptions=\"")))
                                {
                                    Console.WriteLine("File " + filename + " contains optimization options (line " + line + "): " + optimisedFile);
                                    exitCode = 1;
                                }
                            }
                        } 
                    }
                }
            }
            return exitCode;  
        }
    }
}
