using System.Xml.Serialization;

namespace ThoughtWorks.CruiseControl.CCTrayLib.Configuration
{
	public class BuildTransitionNotification
	{
		[XmlAttribute(AttributeName="showBalloon")]
		public bool ShowBalloon = true;

        [XmlAttribute( AttributeName = "showBalloonOnRogueBuild" )]
        public bool ShowBalloonOnRogueBuild = true;

        [XmlAttribute( AttributeName = "rogueBuildSeconds" )]
        public int RogueBuildMinutes = 2 * 60;

        [XmlAttribute( AttributeName = "rogueBuildNotificationIntervalMinutes" )]
        public int RogueBuildNotificationIntervalMinutes = 5;

		[XmlElement(ElementName = "Sound")]
		public AudioFiles AudioFiles = new AudioFiles();

		[XmlElement(ElementName = "BalloonMessages")]
		public BalloonMessages BalloonMessages = new BalloonMessages();

        [XmlAttribute(AttributeName = "minimumNotificationLevel")]
        public NotifyInfoFlags MinimumNotificationLevel = NotifyInfoFlags.Info;
	}
}
