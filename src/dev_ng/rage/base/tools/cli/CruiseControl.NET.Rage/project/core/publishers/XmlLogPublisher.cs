using System.IO;
using System.Xml;
using Exortech.NetReflector;
using ThoughtWorks.CruiseControl.Core.Util;
using ThoughtWorks.CruiseControl.Remote;

namespace ThoughtWorks.CruiseControl.Core.Publishers
{
    [ReflectorType("xmllogger")]
    public class XmlLogPublisher : ITask
    {
		public static readonly string DEFAULT_LOG_SUBDIRECTORY = "buildlogs";

        [ReflectorProperty("logDir", Required = false)] 
		public string ConfiguredLogDirectory;

        [ReflectorProperty("postScript", Required = false)]
        public string PostScript = "";

        [ReflectorProperty("postArgs", Required = false)]
        public string PostArgs = "";

		// This is only public because of a nasty hack which I (MR) put in the code. To be made private later...
		public string LogDirectory(string artifactDirectory)
		{
			if (StringUtil.IsBlank(ConfiguredLogDirectory))
			{
				return Path.Combine(artifactDirectory, DEFAULT_LOG_SUBDIRECTORY);
			}
			else if (Path.IsPathRooted(ConfiguredLogDirectory))
			{
				return ConfiguredLogDirectory;
			}
			else
			{
				return Path.Combine(artifactDirectory, ConfiguredLogDirectory);
			}
		}

		public void Run(IIntegrationResult result)
        {
         
            // only deal with known integration status
            if (result.Status == IntegrationStatus.Unknown)
                return;

            using (XmlIntegrationResultWriter integrationWriter = new XmlIntegrationResultWriter(CreateWriter(LogDirectory(result.ArtifactDirectory), GetFilename(result))))
            {
				integrationWriter.Formatting = Formatting.Indented;
				integrationWriter.Write(result);
            }
          
            result.BuildLogDirectory = LogDirectory(result.ArtifactDirectory);

            // Rockstar Customisation DW : 14-03-2011
            RunPostScript(result.BuildLogDirectory);
        }

        // Rockstar customisation - DW : 14-03-2011
        // runs a sript after the log file has been saved
        public void RunPostScript(string artifactDir)
        {
            if ( PostScript.Length > 0 )
            {
                ProcessExecutor processExecutor = new ProcessExecutor();
                ProcessInfo info = new ProcessInfo(PostScript, PostArgs, artifactDir);
                info.TimeOut = 360000;
                ProcessResult result = processExecutor.Execute(info);
            }
        }   

        private TextWriter CreateWriter(string dirname, string filename)
        {
            // create directory if necessary
            if (!Directory.Exists(dirname))
                Directory.CreateDirectory(dirname);

            string path = Path.Combine(dirname, filename);

			// create XmlWriter using UTF8 encoding
			return new StreamWriter(path);
        }

        private string GetFilename(IIntegrationResult result)
        {
            return Util.StringUtil.RemoveInvalidCharactersFromFileName(new LogFile(result).Filename);
        }
    }
}