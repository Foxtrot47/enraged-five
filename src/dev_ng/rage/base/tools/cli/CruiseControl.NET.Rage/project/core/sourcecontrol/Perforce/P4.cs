using System;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Text;
using Exortech.NetReflector;
using ThoughtWorks.CruiseControl.Core.Util;
using System.Collections.Generic;

namespace ThoughtWorks.CruiseControl.Core.Sourcecontrol.Perforce
{
    public class ViewSpecification
    {
        public ViewSpecification( string perforcePath, string clientPath, string label )
        {
            this.PerforcePath = perforcePath;
            this.ClientPath = clientPath;
            this.Label = label;
        }

        public string PerforcePath = null;
        public string ClientPath = null;
        public string Label = null;

        public override string ToString()
        {
            StringBuilder s = new StringBuilder();
            s.Append( this.PerforcePath );

            if ( !String.IsNullOrEmpty( this.Label ) )
            {
                s.Append( '@' );
                s.Append( this.Label );
            }

            if ( !String.IsNullOrEmpty( this.ClientPath ) )
            {
                s.Append( ' ' );
                s.Append( this.ClientPath );
            }

            return s.ToString();
        }
    }

	[ReflectorType("p4")]
	public class P4 : ISourceControl
	{
		private readonly IP4Purger p4Purger;
		internal static readonly string COMMAND_DATE_FORMAT = "yyyy/MM/dd:HH:mm:ss";

		protected readonly ProcessExecutor processExecutor;
		private readonly IP4Initializer p4Initializer;
        protected readonly IP4ProcessInfoCreator processInfoCreator;

        private Dictionary<string, string> LatestChangeOnLabelMap = null;

		public P4()
		{
			processExecutor = new ProcessExecutor();
			processInfoCreator = new P4ConfigProcessInfoCreator();
			p4Initializer = new ProcessP4Initializer(processExecutor, processInfoCreator);
			p4Purger = new ProcessP4Purger(processExecutor, processInfoCreator);
		}

		public P4(ProcessExecutor processExecutor, IP4Initializer initializer, IP4Purger p4Purger, IP4ProcessInfoCreator processInfoCreator)
		{
			this.processExecutor = processExecutor;
			p4Initializer = initializer;
			this.processInfoCreator = processInfoCreator;
			this.p4Purger = p4Purger;
		}

		[ReflectorProperty("executable", Required=false)]
		public string Executable = "p4";

		[ReflectorProperty("view")]
		public string View;

		[ReflectorProperty("client", Required=false)]
		public string Client = string.Empty;

		[ReflectorProperty("user", Required=false)]
		public string User = string.Empty;

		[ReflectorProperty("password", Required=false)]
		public string Password = string.Empty;

		[ReflectorProperty("port", Required=false)]
		public string Port = string.Empty;

		[ReflectorProperty("workingDirectory", Required=false)]
		public string WorkingDirectory = string.Empty;

		[ReflectorProperty("applyLabel", Required = false)]
		public bool ApplyLabel = false;

		[ReflectorProperty("autoGetSource", Required = false)]
		public bool AutoGetSource = true;

		[ReflectorProperty("forceSync", Required = false)]
		public bool ForceSync = false;

		[ReflectorProperty(@"p4WebURLFormat", Required=false)]
		public string P4WebURLFormat;

		[ReflectorProperty("timeZoneOffset", Required=false)]
		public double TimeZoneOffset = 0;

		private string BuildModificationsCommandArguments(DateTime from, DateTime to)
		{
			return string.Format("changes -s submitted {0}", GenerateRevisionsForView(from, to));
		}

        private string BuildLatestChangeOnLabelCommandArguments(string path, string label)
        {
            return string.Format("changes -s submitted -m 1 {0}@{1}", path, label);
        }

        private string GetLatestChangeOnLabel(string path, string label)
        {
            ProcessInfo process = processInfoCreator.CreateProcessInfo(this, BuildLatestChangeOnLabelCommandArguments(path, label));
            string[] words = Execute(process).Trim().Split(" ".ToCharArray(), 4);
            if (words.Length == 4 && !String.IsNullOrEmpty(words[2]))
                return words[2];
            else
                return label;
        }

        private Dictionary<string, string> CreateLatestChangeOnLabelMap()
        {
            Dictionary<string, string> map = new Dictionary<string, string>();

            ViewSpecification[] views = this.ViewForSpecifications;
            foreach (ViewSpecification view in views)
            {
                if (!String.IsNullOrEmpty(view.Label))
                {
                    map[view.PerforcePath + "@" + view.Label] = GetLatestChangeOnLabel(view.PerforcePath, view.Label);
                }
            }

            return map;
        }

        private string LookupLatestChangeOnLabel(string path, string label)
        {
            if (LatestChangeOnLabelMap != null)
            {
                string latestChangeOnLabel;
                if (LatestChangeOnLabelMap.TryGetValue(path + '@' + label, out latestChangeOnLabel))
                {
                    return latestChangeOnLabel;
                }
            }
            return label;
        }

		protected string GenerateRevisionsForView(DateTime from, DateTime to)
		{
			StringBuilder args = new StringBuilder();

            ViewSpecification[] views = this.ViewForSpecifications;
            foreach ( ViewSpecification view in views )
            {
                if ( args.Length > 0 )
                {
                    args.Append( ' ' );
                }

                args.Append( view.PerforcePath );

                if ( from == DateTime.MinValue )
                {
                    if ( !String.IsNullOrEmpty( view.Label ) )
                    {
                        args.Append("@" + LookupLatestChangeOnLabel(view.PerforcePath, view.Label));
                    }
                    else
                    {
                        args.Append( "@" + FormatDate( to ) );
                    }
                }
                else
                {
                    if ( !String.IsNullOrEmpty( view.Label ) )
                    {
                        args.Append(string.Format("@{0},@{1}", FormatDate(from), LookupLatestChangeOnLabel(view.PerforcePath, view.Label)));
                    }
                    else
                    {
                        args.Append( string.Format( "@{0},@{1}", FormatDate( from ), FormatDate( to ) ) );
                    }                    
                }
            }

			return args.ToString();
		}

		private string FormatDate(DateTime date)
		{
			DateTime offsetDate = date.AddHours(TimeZoneOffset);
			return offsetDate.ToString(COMMAND_DATE_FORMAT, CultureInfo.InvariantCulture);
		}

		public virtual ProcessInfo CreateChangeListProcess(DateTime from, DateTime to)
		{
			return processInfoCreator.CreateProcessInfo(this, BuildModificationsCommandArguments(from, to));
		}

		public virtual ProcessInfo CreateDescribeProcess(string changes)
		{
			if (changes.Length == 0)
				throw new Exception("Empty changes list found - this should not happen");

			foreach (char c in changes)
			{
				if (! (Char.IsDigit(c) || c == ' '))
					throw new CruiseControlException("Invalid changes list encountered");
			}

			return processInfoCreator.CreateProcessInfo(this, "describe -s " + changes);
		}

		public Modification[] GetModifications(IIntegrationResult from, IIntegrationResult to)
		{
            this.LatestChangeOnLabelMap = CreateLatestChangeOnLabelMap();
			P4HistoryParser parser = new P4HistoryParser();
            ProcessInfo process = CreateChangeListProcess(from.StartTime, to.StartTime);
            this.LatestChangeOnLabelMap = null;

            try
            {
                string processResult = Execute(process);
                String changes = parser.ParseChanges(processResult);
                if (changes.Length == 0)
                {
                    return new Modification[0];
                }
                else
                {
                    process = CreateDescribeProcess(changes);
                    Modification[] mods = parser.Parse(new StringReader(Execute(process)), from.StartTime, to.StartTime);
                    if (!StringUtil.IsBlank(P4WebURLFormat))
                    {
                        foreach (Modification mod in mods)
                        {
                            mod.Url = string.Format(P4WebURLFormat, mod.ChangeNumber);
                        }
                    }
                    FillIssueUrl(mods);
                    return mods;
                }
            }
            catch (CruiseControlException ex)
            {
                //KRW - 5/7/2012 - Added to handle the external studios' consistent barrage of
                //failed Perforce commands when polling the server.  Studios will frequently see
                //WSAECONNRESET errors, which will fail the build and causes confusion about its state.
                return new Modification[0];
            }
		}

		/// <summary>
		/// Labelling in Perforce requires 2 activities. First you create a 'label specification' which is the name of the label, and what
		/// part of the source repository it is associated with. Secondly you actually populate the label with files and associated
		/// revisions by performing a 'label sync'. We take the versioned file set as being the versions that are currently 
		/// checked out on the client (In theory this could be refined by using the timeStamp, but it would be better
		/// to wait until CCNet has proper support for atomic-commit change groups, and use that instead)
		/// </summary>
		public void LabelSourceControl(IIntegrationResult result)
		{
			if (ApplyLabel && result.Succeeded)
			{
				if (result.Label == null || result.Label.Length == 0)
					throw new ApplicationException("Internal Exception - Invalid (null or empty) label passed");

				try
				{
					int.Parse(result.Label);
					throw new CruiseControlException("Perforce cannot handle purely numeric labels - you must use a label prefix for your project");
				}
				catch (FormatException)
				{}
				ProcessInfo process = CreateLabelSpecificationProcess(result.Label);

				string processOutput = Execute(process);
				if (containsErrors(processOutput))
				{
					Log.Error(string.Format("Perforce labelling failed:\r\n\t process was : {0} \r\n\t output from process was: {1}", process.ToString(), processOutput));
					return;
				}

				process = CreateLabelSyncProcess(result.Label);
				processOutput = Execute(process);
				if (containsErrors(processOutput))
				{
					Log.Error(string.Format("Perforce labeling failed:\r\n\t process was : {0} \r\n\t output from process was: {1}", process.ToString(), processOutput));
					return;
				}
			}
		}

		private bool containsErrors(string processOutput)
		{
			return processOutput.IndexOf("error:") > -1;
		}

		private ProcessInfo CreateLabelSpecificationProcess(string label)
		{
			ProcessInfo processInfo = processInfoCreator.CreateProcessInfo(this, "label -i");
			processInfo.StandardInputContent = string.Format("Label:	{0}\n\nDescription:\n	Created by CCNet\n\nOptions:	unlocked\n\nView:\n{1}", label, ViewForSpecificationsAsNewlineSeparatedString);
			return processInfo;
		}

        public virtual ViewSpecification[] ViewForSpecifications
        {
            get
            {
                ArrayList views = new ArrayList();

                string[] viewLines = this.View.Split( ',' );
                foreach ( string viewLine in viewLines )
                {                   
                    string perforcePath = null;
                    string clientPath = null;
                    string label = null;

                    int indexOf = viewLine.IndexOf( ':' );
                    if ( indexOf != -1 )
                    {
                        perforcePath = viewLine.Substring( 0, indexOf );
                        clientPath = viewLine.Substring( indexOf + 1 );
                    }
                    else
                    {
                        perforcePath = viewLine;
                        clientPath = viewLine;

                        indexOf = clientPath.IndexOf( "//" );
                        if ( indexOf != -1 )
                        {
                            clientPath = clientPath.Insert( indexOf + 2, "$(client)/" );
                        }
                        else
                        {
                            // uh-oh
                        }
                    }

                    indexOf = perforcePath.IndexOf( '@' );
                    if ( indexOf != -1 )
                    {
                        label = perforcePath.Substring( indexOf + 1 );
                        perforcePath = perforcePath.Substring( 0, indexOf );

                        indexOf = clientPath.IndexOf( '@' );
                        if ( indexOf != -1 )
                        {
                            clientPath = clientPath.Substring( 0, indexOf );
                        }
                    }

                    if ( !String.IsNullOrEmpty( perforcePath ) )
                    {
                        views.Add( new ViewSpecification( perforcePath, clientPath, label ) );
                    }                   
                }

                return (ViewSpecification[])views.ToArray( typeof( ViewSpecification ) );
            }
        }

        private string ViewForSpecificationsAsNewlineSeparatedString
        {
            get
            {
                StringBuilder s = new StringBuilder();

                ViewSpecification[] views = this.ViewForSpecifications;
                foreach ( ViewSpecification view in views )
                {
                    s.Append( " " );

                    string viewLine = view.ToString();
                    if ( !String.IsNullOrEmpty( this.Client ) )
                    {
                        s.Append( viewLine.Replace( "$(client)", this.Client ) );
                    }
                    else
                    {
                        s.Append( viewLine );
                    }

                    s.Append( "\n" );
                }

                return s.ToString();
            }
        }

		public string ViewForDisplay
		{
			get 
            {
                StringBuilder s = new StringBuilder();

                ViewSpecification[] views = this.ViewForSpecifications;
                foreach ( ViewSpecification view in views )
                {
                    s.Append( " " );

                    string viewLine = view.ToString();
                    if ( !String.IsNullOrEmpty( this.Client ) )
                    {
                        s.Append( viewLine.Replace( "$(client)", this.Client ) );
                    }
                    else
                    {
                        s.Append( viewLine );
                    }
                    
                    s.Append( Environment.NewLine );
                }

                return s.ToString(); 
            }
		}

		private ProcessInfo CreateLabelSyncProcess(string label)
		{
			return processInfoCreator.CreateProcessInfo(this, "labelsync -l " + label);
		}

		public void GetSource(IIntegrationResult result)
		{
            result.BuildProgressInformation.SignalStartRunTask("Getting source from Perforce");

			if ( this.AutoGetSource )
			{
                ProcessInfo info = processInfoCreator.CreateProcessInfo( this, CreateSyncCommandLine( result.StartTime ) );
                Log.Info( string.Format( "Getting source from Perforce: {0} {1}", info.FileName, info.Arguments ) );
                Execute( info );
            }
		}

        protected internal virtual string CreateSyncCommandLine( DateTime to )
        {
            StringBuilder s = new StringBuilder( "sync" );

            if ( this.ForceSync )
            {
                s.Append( " -f" );
            }

            ViewSpecification[] views = this.ViewForSpecifications;
            foreach ( ViewSpecification view in views )
            {
                s.Append( ' ' );
                s.Append( view.PerforcePath );

                s.Append( '@' );
                if ( !String.IsNullOrEmpty( view.Label ) )
                {
                    s.Append( view.Label );
                }
                else
                {
                    s.Append( FormatDate( to ) );
                }
            }

            return s.ToString();
        }

		protected virtual string Execute(ProcessInfo p)
		{
			Log.Debug("Perforce plugin - running:" + p.ToString());
			ProcessResult result = processExecutor.Execute(p);
			return result.StandardOutput.Trim() + Environment.NewLine + result.StandardError.Trim();
		}

		public void Initialize(IProject project)
		{
			if (StringUtil.IsBlank(WorkingDirectory))
			{
				p4Initializer.Initialize(this, project.Name, project.WorkingDirectory);
			}
			else
			{
				p4Initializer.Initialize(this, project.Name, WorkingDirectory);
			}
		}

		public void Purge(IProject project)
		{
			if (StringUtil.IsBlank(WorkingDirectory))
			{
				p4Purger.Purge(this, project.WorkingDirectory);
			}
			else
			{
				p4Purger.Purge(this, WorkingDirectory);
			}
		}

        [ReflectorProperty("issueUrlBuilder", InstanceTypeKey = "type", Required = false)]
        public IModificationUrlBuilder IssueUrlBuilder;


        private void FillIssueUrl(Modification[] modifications)
        {
            if (IssueUrlBuilder != null)
            {
                IssueUrlBuilder.SetupModification(modifications);
            }
        }
	}

    [ReflectorType("p4Incremental")]
    public class P4Incremental : P4
    {
        // I'm afraid I haven't yet been able to incorporate the viewSpecifications label stuff yet - Rage - I'd appreciate if you can do this here.
		protected internal override string CreateSyncCommandLine(DateTime modificationsToDate)
        {
            string commandline = "sync";
            if (ForceSync)
            {
                commandline += " -f";
            }
            commandline += " " + GenerateRevisionsForView(DateTime.MinValue, modificationsToDate);

            return commandline;
        }

        // Issue a harmless p4 command to see if the perforce is working.
        public bool IsConnected()
        {
            ProcessInfo info = processInfoCreator.CreateProcessInfo(this, "login -s");
            Log.Info(string.Format("Rockstar P4 customisation - Testing connection to Perforce: {0} {1}", info.FileName, info.Arguments));
            ProcessResult result = processExecutor.Execute(info);
            if (result.ExitCode != ProcessResult.SUCCESSFUL_EXIT_CODE)
            {
                Log.Error(string.Format("Rockstar P4 customisation - Not connected to Perforce: {0} {1} ExitCode {2}", info.FileName, info.Arguments, result.ExitCode));
                return false;
            }
            return true;
        }
    }
}


