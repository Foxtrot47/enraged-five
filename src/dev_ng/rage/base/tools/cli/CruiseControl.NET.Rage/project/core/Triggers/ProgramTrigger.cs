﻿using System;
using System.IO;
using Exortech.NetReflector;
using ThoughtWorks.CruiseControl.Core.Util;
using ThoughtWorks.CruiseControl.Remote;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace ThoughtWorks.CruiseControl.Core.Triggers
{
    [ReflectorType("programTrigger")]
    public class ProgramTrigger : ITrigger
    {
        public const string DefaultServerUri = RemoteCruiseServer.DefaultUri;
        private const int DefaultIntervalSeconds = 5;

        private readonly ICruiseManagerFactory managerFactory;

        public ProgramTrigger()
            : this(new RemoteCruiseManagerFactory())
        { }

        public ProgramTrigger(ICruiseManagerFactory managerFactory)
        {
            this.managerFactory = managerFactory;
        }

        [ReflectorProperty("triggerStatus", Required = false)]
        public IntegrationStatus TriggerStatus = IntegrationStatus.Success;

        [ReflectorProperty("trigger", InstanceTypeKey = "type", Required = false)]
        public ITrigger InnerTrigger = NewIntervalTrigger();

        [ReflectorProperty("executable", Required = true)]
        public string Executable = null;

        [ReflectorProperty("arguments", Required = false)]
        public string Arguments = null;

        [ReflectorProperty("workingDirectory", Required = false)]
        public string WorkingDirectory = null;

        [ReflectorProperty("exitCodes", Required = false)]
        public string ExitCodes = null;

        public void IntegrationCompleted()
        {
            InnerTrigger.IntegrationCompleted();
        }

        public DateTime NextBuild
        {
            get
            {
                return InnerTrigger.NextBuild;
            }
        }

        public IntegrationRequest Fire()
        {
            IntegrationRequest request = InnerTrigger.Fire();
            if (request == null) 
                return null;

            InnerTrigger.IntegrationCompleted(); // reset inner trigger (timer)

            if (File.Exists(Executable) == false)
            {
                Log.Error("Unable to find " + Executable + ".");
                return null;
            }

            if (Directory.Exists(WorkingDirectory) == false)
            {
                Log.Error("Unable to find directory " + WorkingDirectory + ".");
                return null;
            }

            Process process = new Process();
            process.StartInfo.FileName = Executable;

            if ( Arguments != null )
            {
                process.StartInfo.Arguments = Arguments;
            }

            if (WorkingDirectory != null)
            {
                process.StartInfo.WorkingDirectory = WorkingDirectory;
            }

            List<int> validExitCodes = new List<int>();
            if (ExitCodes != null)
            {
                string[] strExitCodes = ExitCodes.Split(',');
                foreach (string exitCode in strExitCodes)
                {
                    int result = 0;
                    if (Int32.TryParse(exitCode,out result) == true)
                    {
                        validExitCodes.Add(result);
                    }
                }
            }
            else
            {
                validExitCodes.Add(0); // 0 is the default exit code to trigger.
            }
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardError = true;

            Log.Debug("Starting program trigger: " + Executable + " with the arguments " + Arguments + " from the directory " + WorkingDirectory + ".");
            process.Start();
            process.WaitForExit();

            while(process.StandardOutput.EndOfStream == false)
            {
                string line = process.StandardOutput.ReadLine();
                Console.WriteLine(line);
            }

            while (process.StandardError.EndOfStream == false)
            {
                string line = process.StandardError.ReadLine();
                Console.WriteLine(line);
            }

            Log.Debug("Program trigger exited with " + process.ExitCode + ".");
            bool success = false;
            foreach( int code in validExitCodes )
            {
                if (code == process.ExitCode)
                {
                    success = true;
                    break;
                }
            }

            //if (currentStatus.LastBuildDate > lastStatus.LastBuildDate && currentStatus.BuildStatus == TriggerStatus)
            if ( success ) 
            {
                return request;
            }

            return null;
        }

        private static ITrigger NewIntervalTrigger()
        {
            IntervalTrigger trigger = new IntervalTrigger();
            trigger.IntervalSeconds = DefaultIntervalSeconds;
            trigger.BuildCondition = BuildCondition.ForceBuild;
            return trigger;
        }
    }
}
