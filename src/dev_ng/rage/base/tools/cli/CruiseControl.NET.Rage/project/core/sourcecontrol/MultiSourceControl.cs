using System.Collections;
using Exortech.NetReflector;
using ThoughtWorks.CruiseControl.Core.Util;
using ThoughtWorks.CruiseControl.Core.Sourcecontrol.Perforce;

namespace ThoughtWorks.CruiseControl.Core.Sourcecontrol
{
	[ReflectorType("multi")]
	public class MultiSourceControl : ISourceControl
	{
		private ISourceControl[] _sourceControls;

		[ReflectorProperty("requireChangesFromAll", Required=false)]
		public bool RequireChangesFromAll = false;

		[ReflectorArray("sourceControls", Required=true)]
		public ISourceControl[] SourceControls 
		{
			get 
			{
				if (_sourceControls == null)
					_sourceControls = new ISourceControl[0];

				return _sourceControls;
			}

			set { _sourceControls = value; }
		}

		public virtual Modification[] GetModifications(IIntegrationResult from, IIntegrationResult to)
		{
			ArrayList modifications = new ArrayList();
			foreach (ISourceControl sourceControl in SourceControls)
			{
				Modification[] mods = sourceControl.GetModifications(from, to);
				if (mods != null && mods.Length > 0)
				{
					modifications.AddRange(mods);
				}
				else if (RequireChangesFromAll)
				{
					modifications.Clear();
					break;
				}
			}

			return (Modification[]) modifications.ToArray(typeof(Modification));
		}

		public void LabelSourceControl(IIntegrationResult result)
		{
			foreach (ISourceControl sourceControl in SourceControls)
			{
				sourceControl.LabelSourceControl(result);
			}
		}

		public void GetSource(IIntegrationResult result) 
		{
			foreach (ISourceControl sourceControl in SourceControls)
			{
				sourceControl.GetSource(result);
			}
		}

		public void Initialize(IProject project)
		{
		}

		public void Purge(IProject project)
		{
		}
	}

    //--------------------------------------------------------------------------------------
    // Custom Multi Source Control Class for Rockstar. - derek.ward@rockstarnorth.com - 2009
    [ReflectorType("incremental_multi")]
    public class MultiSourceControlIncremental : MultiSourceControl
    {
        [ReflectorProperty("ChangelistSkipThreshold", Required = true)]
        public int ChangelistSkipThreshold;

        public enum PreTest { None = 0, Each, All };
        [ReflectorProperty("PreTestConnection", Required = false)]
        public PreTest PreTestConnection = PreTest.All;

        // Returns false if a sourcecontrol is definately not connected.
        public bool PreCheck(ISourceControl sourceControl)
        {
            if (sourceControl is P4Incremental)
            {
                P4Incremental p4Inc = new P4Incremental();
                p4Inc = sourceControl as P4Incremental;
                if (p4Inc == null)
                {
                    throw new System.Exception("P4Incremental cast failed.");
                }
                return p4Inc.IsConnected();
            }
            return true;
        }

        // If we are interested find out if any source controls are disconnected...
        public bool CheckAllConnections()
        {
            if (PreTestConnection == PreTest.All)
            {
                foreach (ISourceControl sourceControl in SourceControls)
                {
                    if (!PreCheck(sourceControl))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public override Modification[] GetModifications(IIntegrationResult from, IIntegrationResult to)
        {
            Log.Info("*****************************************************");
            Log.Info("***** ROCKSTAR INCREMENTAL MULTI SOURCE CONTROL *****");
            Log.Info("*****************************************************");

            Log.Info("\nfrom.StartTime \n" + from.StartTime.ToString());
            System.DateTime originalFromTime = from.StartTime;
            from.StartTime = from.StartTime.AddSeconds(1.0); // add a second so we don't get the mods that we picked up last time.
            Log.Info("\nfrom.StartTime second added \n" + from.StartTime.ToString());

            ArrayList modifications = new ArrayList();
            if (CheckAllConnections())
            {                
                foreach (ISourceControl sourceControl in SourceControls)
                {
                    if (PreTestConnection != PreTest.Each || PreCheck(sourceControl))
                    {
                        Modification[] mods = sourceControl.GetModifications(from, to);

                        // correct for timezones.
                        foreach (Modification mod in mods)
                        {
                            // Dear cruise control please put common things like TimeZoneOffset into ISourceControl otherwise major hackery is required...
                            if (sourceControl is P4Incremental)
                            {
                                P4Incremental p4Inc = new P4Incremental();
                                p4Inc = sourceControl as P4Incremental;
                                if (p4Inc == null)
                                {
                                    throw new System.Exception("P4Incremental cast failed.");
                                }

                                if (p4Inc.TimeZoneOffset != 0)
                                {
                                    mod.ModifiedTime = mod.ModifiedTime.AddHours(-p4Inc.TimeZoneOffset);
                                    Log.Info("\nModification " + mod.FileName + " really occurred at " + mod.ModifiedTime + "\n");
                                }
                            }
                            else
                            {
                                throw new System.Exception("not expecting other classes of source control to this custom modification");
                            }
                        }

                        if (mods != null && mods.Length > 0)
                        {
                            modifications.AddRange(mods);
                        }
                        else if (RequireChangesFromAll)
                        {
                            modifications.Clear();
                            break;
                        }
                    }
                }
            }

            // ***********************************************************
            // OK now comes the 'incremental' customisation for Rockstar.
            if (modifications.Count > 0)
            {
                // We sort all the mods by date.
                modifications.Sort(); // IComparable is implemented for Modification class to sort by date already... whoopeee!

                // We take a copy of the first and last because they may be getting deleted.
                Modification first = modifications[0] as Modification;
                Modification last = modifications[modifications.Count-1] as Modification;

                Log.Info("\nWe are " + (last.ModifiedTime-first.ModifiedTime).ToString() + " behind the head.\n");

                // Continued to use arraylists as the original code uses them, probably better generics could be chosen though.
                ArrayList modificationsOriginal     = new ArrayList(modifications);
                ArrayList modificationsPending      = new ArrayList(modifications);
                ArrayList changelistNumbersAccepted = new ArrayList();
                ArrayList changelistNumbersQueued   = new ArrayList();

                // We transform the modifications into only the earliest mods.
                foreach (Modification mod in modificationsOriginal)
                {
                    int compareResult = mod.CompareTo(first);

                    if (compareResult > 0)
                    {
                        if (changelistNumbersQueued.Count == 0 || mod.ChangeNumber != (int)changelistNumbersQueued[changelistNumbersQueued.Count - 1])
                            changelistNumbersQueued.Add(mod.ChangeNumber);
                        
                        modifications.Remove(mod);
                    }
                    else if (compareResult == 0)
                    {
                        modificationsPending.Remove(mod);

                        Log.Info("\nModification has been accepted\n\n" + mod.ToString() + "\n\n");
                        if (changelistNumbersAccepted.Count == 0 || mod.ChangeNumber != (int)changelistNumbersAccepted[changelistNumbersAccepted.Count - 1])
                            changelistNumbersAccepted.Add(mod.ChangeNumber);
                    }
                    else if (compareResult < 0)
                    {
                        throw new System.Exception("Incremental Multi Source Control - modification array not sorted!");
                    }
                    else
                    {
                        throw new System.Exception("Incremental Multi Source Control - bad IComparable Result");
                    }
                }

                // Display stats on the changelists in consideration.
                int totalChangelists = changelistNumbersAccepted.Count + changelistNumbersQueued.Count;
                Log.Info("\n" + totalChangelists + " changelist(s) in queue. (Skips when >" + ChangelistSkipThreshold + ")\n");
                
                foreach (int change in changelistNumbersAccepted)
                {
                    Log.Info("Changelist Processing : " + change);
                }

                foreach (int change in changelistNumbersQueued)
                {
                    Log.Info("Changelist Queued     : " + change);
                }

                // -1 for ChangelistSkipThreshold means NO skipping
                //  0 would mean that you always skip to the head.
                if (totalChangelists > ChangelistSkipThreshold && 
                    ChangelistSkipThreshold >= 0) 
                {
                    Log.Warning("\nSkipping to Head\n");
                    modifications = modificationsOriginal;  // get everything.                    
                    to.RealStartTime = to.StartTime;
                    to.StartTime = last.ModifiedTime;       // We modify the to.StartTime to be the date and time of these mods.

                    // Inform the integration result of the modifiactions that are pending                
                    to.ModificationsPending = new Modification[0];
                }
                else
                {
                    to.RealStartTime = to.StartTime;
                    to.StartTime = first.ModifiedTime; // We modify the to.StartTime to be the date and time of these mods.

                    // Inform the integration result of the modifiactions that are pending                
                    to.ModificationsPending = (Modification[])modificationsPending.ToArray(typeof(Modification));
                }

                Log.Info("\nModifications pending " + to.ModificationsPending.Length.ToString());

                Log.Info("\nto.StartTime will clamp us to these mods ONLY \n" + to.StartTime.ToString());
            }

            // Undo the from time modification as it could ( and has! ) interfere with the behaviour of the integration.
            from.StartTime = originalFromTime;
            Log.Info("\nfrom.StartTime second removed \n" + from.StartTime.ToString());

            // END the 'incremental' customisation for Rockstar.
            // ***********************************************************

            return (Modification[])modifications.ToArray(typeof(Modification));
        }
    }
}
