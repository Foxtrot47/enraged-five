using System.IO;
using System.Xml.Serialization;
using Exortech.NetReflector;
using ThoughtWorks.CruiseControl.Core.Util;

namespace ThoughtWorks.CruiseControl.Core.Tasks
{
    [ReflectorType("modificationWriter")]
    public class ModificationWriterTask : ITask
    {
        private readonly IFileSystem fileSystem;

        public ModificationWriterTask()
            : this(new SystemIoFileSystem())
        { }

        public ModificationWriterTask(IFileSystem fileSystem)
        {
            this.fileSystem = fileSystem;
        }

        /// <summary>
        /// Description used for the visualisation of the buildstage, if left empty the process name will be shown
        /// </summary>
        [ReflectorProperty("description", Required = false)]
        public string Description = string.Empty;
        
        public void Run(IIntegrationResult result)
        {
            result.BuildProgressInformation.SignalStartRunTask(Description != string.Empty ? Description : "Writing Modifications");

            XmlSerializer serializer = new XmlSerializer(typeof(Modification[]));
            StringWriter writer = new Utf8StringWriter();
            serializer.Serialize(writer, result.Modifications);

            string filename = ModificationFile(result);
            fileSystem.EnsureFolderExists(filename);
            fileSystem.Save(filename, writer.ToString());

            //Rockstar Customisation - after saving mods file - write pending changes             
            RunPending(result);

            // also run post script - this is so we can work with the updated mod files as they are updated.
            RunPostScript(result);
        }

        //=============================================================================
        // Write modifications pending - Rockstar Customisation
        public void RunPending(IIntegrationResult result)
        {
            Log.Info("PendingFilename " + PendingFilename);
            if (PendingFilename.Length > 0)
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Modification[]));
                StringWriter writer = new Utf8StringWriter();
                serializer.Serialize(writer, result.ModificationsPending);

                string filename = PendingModificationFile(result);
                Log.Info("Writing ModificationsPending " + result.ModificationsPending.Length.ToString() + " " + filename);
                fileSystem.EnsureFolderExists(filename);
                fileSystem.Save(filename, writer.ToString());
            }
        }

        // Rockstar customisation - DW : 14-03-2011
        // runs a sript after the log file has been saved
        public void RunPostScript(IIntegrationResult result)
        {           
            if ( PostScript.Length > 0 )
            {
                string artifactDir = LogDirectory(result.ArtifactDirectory);
                if ( Directory.Exists(artifactDir) == false)
                {
                    Directory.CreateDirectory(artifactDir);
                }

                ProcessExecutor processExecutor = new ProcessExecutor();
                ProcessInfo info = new ProcessInfo(PostScript, PostArgs, artifactDir);
                info.TimeOut = 360000;
                ProcessResult res = processExecutor.Execute(info);
            }
        }

        public string LogDirectory(string artifactDirectory)
        {
            if (StringUtil.IsBlank(ConfiguredLogDirectory))
            {
                return Path.Combine(artifactDirectory, DEFAULT_LOG_SUBDIRECTORY);
            }
            else if (Path.IsPathRooted(ConfiguredLogDirectory))
            {
                return ConfiguredLogDirectory;
            }
            else
            {
                return Path.Combine(artifactDirectory, ConfiguredLogDirectory);
            }
        }

        private string ModificationFile(IIntegrationResult result)
        {
        	if (!AppendTimeStamp)
        		return Path.Combine(result.BaseFromArtifactsDirectory(OutputPath), Filename);

        	FileInfo fi = new FileInfo(Filename);
        	string dummy = Filename.Remove(Filename.Length - fi.Extension.Length, fi.Extension.Length);
        	string newFileName = string.Format("{0}_{1}{2}", dummy, result.StartTime.ToString("yyyyMMddHHmmssfff"),
        	                                   fi.Extension);

        	return Path.Combine(result.BaseFromArtifactsDirectory(OutputPath), newFileName);
        }

        private string PendingModificationFile(IIntegrationResult result)
        {
            if (PendingFilename.Length == 0)
                return "";

            if (!AppendTimeStamp)
                return Path.Combine(result.BaseFromArtifactsDirectory(OutputPath), PendingFilename);

            FileInfo fi = new FileInfo(Filename);
            string dummy = Filename.Remove(Filename.Length - fi.Extension.Length, fi.Extension.Length);
            string newFileName = string.Format("{0}_{1}{2}", dummy, result.StartTime.ToString("yyyyMMddHHmmssfff"),
                                               fi.Extension);

            return Path.Combine(result.BaseFromArtifactsDirectory(OutputPath), newFileName);
        }

    	/// <summary>
        /// The fileName to use to store the modifications
        /// </summary>
        [ReflectorProperty("filename", Required = false)]
        public string Filename = "modifications.xml";

        /// <summary>
        /// The fileName to use to store the modifications that are pending - if left blank not used.
        /// </summary>
        [ReflectorProperty("pendingFilename", Required = false)]
        public string PendingFilename = "";

        /// <summary>
        /// Path of the file
        /// </summary>
        [ReflectorProperty("path", Required = false)]
        public string OutputPath;

        /// <summary>
        /// Append the buildStartdate to the filename, and so prevent overwriting the file with other builds
        /// To be used in conjunction with the ModificationReaderTask <see cref="ModificationReaderTask"/> if set to true
        /// </summary>
        [ReflectorProperty("appendTimeStamp", Required = false)]
        public bool AppendTimeStamp;

        public static readonly string DEFAULT_LOG_SUBDIRECTORY = "buildlogs";

        [ReflectorProperty("logDir", Required = false)]
        public string ConfiguredLogDirectory;

        /// <summary>
        /// Script to run after the modifications have been published
        /// </summary>
        [ReflectorProperty("postScript", Required = false)]
        public string PostScript = "";

        /// <summary>
        /// Script args to run with after the modifications have been published
        /// </summary>
        [ReflectorProperty("postArgs", Required = false)]
        public string PostArgs = "";
    }
}
