using System;
using System.Collections;
using System.Collections.Specialized;
using ThoughtWorks.CruiseControl.CCTrayLib.Configuration;
using ThoughtWorks.CruiseControl.CCTrayLib.Monitoring;
using ThoughtWorks.CruiseControl.Remote;

namespace ThoughtWorks.CruiseControl.CCTrayLib.Presentation
{
	public class BuildTransitionSoundPlayer
	{
		private readonly IAudioPlayer audioPlayer;
		private readonly IDictionary soundFileLookup = new HybridDictionary();

		public BuildTransitionSoundPlayer(IProjectMonitor monitor, IAudioPlayer audioPlayer, AudioFiles configuration)
		{
			this.audioPlayer = audioPlayer;

			if (configuration != null)
			{
				soundFileLookup[BuildTransition.Broken] = configuration.BrokenBuildSound;
				soundFileLookup[BuildTransition.Fixed] = configuration.FixedBuildSound;
				soundFileLookup[BuildTransition.StillFailing] = configuration.StillFailingBuildSound;
				soundFileLookup[BuildTransition.StillSuccessful] = configuration.StillSuccessfulBuildSound;
                soundFileLookup[BuildTransition.RogueBuild] = configuration.RogueBuildSound;
			}

			monitor.BuildOccurred += new MonitorBuildOccurredEventHandler(Monitor_BuildOccurred);
            monitor.RogueBuild += new MessageEventHandler(Monitor_RogueBuild);
		}

		private void Monitor_BuildOccurred(object sender, MonitorBuildOccurredEventArgs e)
		{
			string soundFileToPlay = ChooseSoundFile(e.BuildTransition);

			if (soundFileToPlay == null || soundFileToPlay.Length == 0)
				return;
				
			audioPlayer.Play(soundFileToPlay);
		}

        private void Monitor_RogueBuild( Message msg )
        {
            string soundFileToPlay = ChooseSoundFile( BuildTransition.RogueBuild );

            if ( soundFileToPlay == null || soundFileToPlay.Length == 0 )
                return;

            audioPlayer.Play( soundFileToPlay );
        }

		private string ChooseSoundFile(BuildTransition transition)
		{
			return (string) soundFileLookup[transition];
		}
	}
}
