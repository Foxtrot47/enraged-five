#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc,char **argv) {
	if (argc != 4 && argc != 5 && argc != 6) {
		fprintf(stderr,"usage: embedfile <binaryFileName> <outputName> <symbolName> [platformName] [-append]\n");
		return 1;
	}

	int append = 0;
	char *platform = NULL;

	if(argc == 5)
	{
		if(strcmp(argv[4], "-append") == 0)
		{
			append = 1;
		}
		else
		{
			platform = argv[4];
		}
	}
	else if(argc == 6)
	{
		append = 1;
		platform = argv[4];
	}

	FILE *f = fopen(argv[1],"rb");
	if (!f) {
		fprintf(stderr,"embedfile: cannot open input file '%s'\n",argv[1]);
		return 1;
	}
	FILE *o = fopen(argv[2], (append == 1) ? "at" : "wt");
	if (!o) {
		fclose(f);
		fprintf(stderr,"embedfile: cannot create output file '%s'\n",argv[2]);
		return 1;
	}
	const char *sym = argv[3];

	int ch;
	int count = 16;

	if(append == 0)
	{
		fprintf(o,"// Automatically generated by embedfile, do not edit.\n\n");
		fprintf(o,"#include \"file/embedded.h\"\n\n");
	}

	if (platform)
		fprintf(o,"#if %s\n\n",platform);
	fprintf(o,"static unsigned char %s_data[] = {\n",sym);
	while ((ch = fgetc(f)) != EOF) {
		fprintf(o,"%d,",ch);
		if (--count == 0) {
			fprintf(o,"\n");
			count = 16;
		}
	}
	fprintf(o,"\n};\n\n");
	fprintf(o,"rage::fiEmbeddedFile %s(\"%s\",%s_data,sizeof(%s_data));\n\n",sym,argv[1],sym,sym);
	if (platform)
		fprintf(o,"#endif // %s\n",platform);
	fclose(o);
	fclose(f);
	return 0;
}
