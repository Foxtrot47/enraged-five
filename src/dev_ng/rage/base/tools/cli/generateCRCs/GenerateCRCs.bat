@echo off

:: Config
set versionfile=X:\gta5\titleupdate\dev\common\data\version.txt
set functionfile=FunctionList.txt

:: US SKU
set sku=us
set skuId=1
call :process_sku

:: EU SKU
set sku=eu
set skuId=2
call :process_sku

:: JP SKU
set sku=jp
set skuId=3
call :process_sku

:: Combine files
echo Combining data into bonusTunables.json...
copy /b crcs_us.json + crcs_eu.json + crcs_jp.json bonusTunables.json
del crcs_us.json
del crcs_eu.json
del crcs_jp.json
echo Done!

:: End
goto :end

:: Per-sku subroutine
:process_sku
set prefix=X:\gta5\titleupdate\dev\default_%sku%
"%RS_TOOLSROOT%\bin\GenerateCRCs\GenerateCRCs.exe" -elf "%prefix%.elf" -sym "%prefix%.sym" -out "crcs_%sku%.json" -funcs "%functionfile%" -version "%versionfile%" -sku %skuId% -verbose
exit /b

:end
pause
