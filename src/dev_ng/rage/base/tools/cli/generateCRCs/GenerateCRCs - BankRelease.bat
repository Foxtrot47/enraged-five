@echo off

:: Config
set versionfile=X:\gta5\titleupdate\dev\common\data\version.txt
set functionfile=FunctionList.txt
set prefix=X:\gta5\titleupdate\dev\game_psn_bankrelease_snc

"%RS_TOOLSROOT%\bin\GenerateCRCs\GenerateCRCs.exe" -elf "%prefix%.elf" -sym "%prefix%.sym" -out "crcs_bankrelease.json" -funcs "%functionfile%" -version "%versionfile%" -sku 0 -verbose

echo Done!
pause
