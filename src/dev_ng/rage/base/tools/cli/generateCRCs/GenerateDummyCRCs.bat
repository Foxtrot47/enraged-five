@echo off

:: Dummy version
"%RS_TOOLSROOT%\bin\GenerateCRCs\GenerateCRCs.exe" -dummyCRCs 677 172 -out bonusTunables_DummyVersion.json -verbose

:: US SKU
"%RS_TOOLSROOT%\bin\GenerateCRCs\GenerateCRCs.exe" -dummyCRCs 682 50 -sku 1 -out bonusTunables_US.json -addrRange 0x20000 0x15337A4 -noActionFlags -verbose

:: EU SKU
"%RS_TOOLSROOT%\bin\GenerateCRCs\GenerateCRCs.exe" -dummyCRCs 682 50 -sku 2 -out bonusTunables_EU.json -addrRange 0x20000 0x15337A4 -noActionFlags -verbose

:: JP SKU
"%RS_TOOLSROOT%\bin\GenerateCRCs\GenerateCRCs.exe" -dummyCRCs 682 50 -sku 3 -out bonusTunables_JP.json -addrRange 0x20000 0x15337A4 -noActionFlags -verbose

echo Done!
pause
