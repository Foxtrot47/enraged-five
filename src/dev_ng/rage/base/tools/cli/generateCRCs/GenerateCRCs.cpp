//-------------------------------------------------------------------------------------------------
// GenerateCRCs.cpp 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
//-------------------------------------------------------------------------------------------------
#include <stdio.h>
#include <windows.h>
#include <time.h>
#include <string>
#include <vector>
#include <map>

typedef unsigned __int64 u64;
typedef signed __int64 s64;
typedef unsigned int u32;
typedef signed int s32;
typedef unsigned short u16;
typedef signed short s16;
typedef unsigned char u8;
typedef signed char s8;

static const u32 s_xorValue = 0xb7ac4b1c;

static const char* s_versionNumberHeader = "[VERSION_NUMBER]";
static const u8 s_elfIdent[] = {0x7F, 0x45, 0x4C, 0x46}; // ".ELF"

//-------------------------------------------------------------------------------------------------

struct Symbol
{
	size_t offset;
	size_t len;
	const char* pSymbol;

	struct Compare
	{
		bool operator()(const char* l, const char* r) const
		{
			return strcmp(l, r) < 0;
		}
	};
};
typedef std::map<const char*, Symbol, Symbol::Compare> SymbolMap;
typedef SymbolMap::iterator SymbolMapIt;
typedef SymbolMap::const_iterator SymbolMapConstIt;

enum ElfFormat
{
	ELF_Unknown,
	ELF_PS3,
	ELF_PS4
};

enum ELFIdent
{
	EI_MAG0,
	EI_MAG1,
	EI_MAG2,
	EI_MAG3,
	EI_CLASS,		// 1 = 32-bit, 2 = 64-bit
	EI_DATA,		// 1 = little endian, 2 = big endian
	EI_VERSION,		// Expect 1
	EI_OSABI,
	EI_ABIVERSION,
};

#pragma pack(push, 1)
struct ELFHeader
{
	u8 e_ident[16];
	u16 e_type;
	u16 e_machine;
	u32 e_version;
	u64 e_entry;
	u64 e_phoff;
	u64 e_shoff;
	u32 e_flags;
	u16 e_ehsize;
	u16 e_phentsize;
	u16 e_phnum;
	u16 e_shentsize;
	u16 e_shnum;
	u16 e_shstrndx;
};

struct ELFProgramEntry
{
	u32 p_type;
	u32 p_flags;
	u64 p_offset;
	u64 p_vaddr;
	u64 p_paddr;
	u64 p_filesz;
	u64 p_memsz;
	u64 p_align;
};
#pragma pack(pop)

struct ELFFile
{
	ELFHeader header;
	ELFProgramEntry* pProgramEntries;
	size_t numProgramEntries; // Equivalent to header.e_phnum
};

struct FunctionEntry
{
	FunctionEntry() : actionFlags(0), offset(0), len(0), crc(0) {}

	std::string funcName;
	u32 actionFlags;
	size_t offset;
	u32 len;
	u32 crc;
};

//-------------------------------------------------------------------------------------------------

static inline u16 swapEndian(u16 n)
{
	return ((n&0x00ff) << 8) | ((n&0xff00) >> 8);
}

static inline u32 swapEndian(u32 n)
{
	return ((n&0x000000ff) << 24) | ((n&0x0000ff00) << 8) | ((n&0x00ff0000) >> 8) | ((n&0xff000000) >> 24);
}

static inline u64 swapEndian(u64 n)
{
	return ((n&0x00000000000000ff) << 56) | ((n&0x000000000000ff00) << 40) | ((n&0x0000000000ff0000) << 24) | ((n&0x00000000ff000000) << 8) |
		((n&0x000000ff00000000) >> 8) | ((n&0x0000ff0000000000) >> 24) | ((n&0x00ff000000000000) >> 40) | ((n&0xff00000000000000) >> 56);
}

static inline u32 GenRandU32()
{
	// RAND_MAX is 15-bits, so do 3 rand()s of 11 bits, 10 bits and 11 bits
	u32 ret = (rand() & 0x7ff);
	ret |= ((rand() & 0x3ff) << 11);
	ret |= ((rand() & 0x7ff) << 21);
	return ret;
}

//-------------------------------------------------------------------------------------------------

// This must match the CRC function in InfoState.cpp in game code!
u32 crcRange(const u8* pBuff, const u8* pEnd)
{
	// Based on the Jenkins hash from http://en.wikipedia.org/wiki/Jenkins_hash_function, with different initial value
	// This happens to be the same as the hash we use with atDataHash, except not restricted to 4-byte blocks.
	u32 hash = 0x04c11db7;
	while(pBuff != pEnd)
	{
		hash += *pBuff++;
		hash += (hash << 10);
		hash ^= (hash >> 6);
	}
	hash += (hash << 3);
	hash ^= (hash >> 11);
	hash += (hash << 15);
	return hash;
}

//-------------------------------------------------------------------------------------------------

void displayUsage()
{
	printf(
		"GenerateCRCs: Generate CRCs for a list of PS3 functions, in JSON format.\n"
		"\n"
		"For usage instructions see:\n"
		"https://hub.rockstargames.com/display/RSGSECGAME/GenerateCRCs\n");
}

//-------------------------------------------------------------------------------------------------

size_t ReadHex(char*& r_pBuff, bool& r_error)
{
	char* pBuff = r_pBuff;
	if(pBuff[0] == '0' && pBuff[1] == 'x')
		pBuff += 2;

	// Assume 64-bit number to start with
	size_t len = 0;
	size_t shift = 60;
	size_t ret = 0;
	for(;;)
	{
		// Get this digit
		size_t digit;
		if(*pBuff >= '0' && *pBuff <= '9')
			digit = *pBuff - '0';
		else if(*pBuff >= 'a' && *pBuff <= 'f')
			digit = *pBuff - 'a' + 10;
		else if(*pBuff >= 'A' && *pBuff <= 'F')
			digit = *pBuff - 'A' + 10;
		else
		{
			if(*pBuff != 0 && *pBuff != ' ')
			{
				r_error = true;
				return 0;
			}
			break;
		}

		// Add to total
		ret |= (digit << shift);
		shift -= 4;
		++pBuff;
		++len;

		// Hex number too long
		if(len > 16)
		{
			r_error = true;
			return 0;
		}
	}

	// Adjust number by actual length
	r_error = false;
	r_pBuff = pBuff;
	return ret >> (16-len)*4;
}

//-------------------------------------------------------------------------------------------------

char* readFile(const char* filename, size_t* p_size=NULL)
{
	// Open file
	FILE* pFile = fopen(filename, "rb");
	if(!pFile)
		return 0;

	// Read file into memory
	fseek(pFile, 0, SEEK_END);
	size_t fileSize = (size_t)ftell(pFile);
	fseek(pFile, 0, SEEK_SET);
	char* pBuff = new char[fileSize+1];
	fread(pBuff, 1, fileSize, pFile);
	pBuff[fileSize] = 0;
	fclose(pFile);
	if(p_size)
		*p_size = fileSize;
	return pBuff;
}

//-------------------------------------------------------------------------------------------------

bool parseVersionFile(const char* filename, u32& r_version)
{
	// Read file into memory
	char* pBuff = readFile(filename);
	if(!pBuff)
	{
		printf("Failed to open file '%s'\n", filename);
		return false;
	}

	// Scan for "[VERSION_NUMBER]"
	char* pVersionNum = pBuff;
	while(*pVersionNum)
	{
		// Skip comment lines
		if(*pVersionNum == '#')
		{
			while(*pVersionNum != 0 && *pVersionNum != '\n')
				++pVersionNum;
		}
		else if(strnicmp(pVersionNum, s_versionNumberHeader, strlen(s_versionNumberHeader)) == 0)
			break;

		while(*pVersionNum == '\r' || *pVersionNum == '\n')
			++pVersionNum;
	}
	if(*pVersionNum == 0)
	{
		printf("Could not find '%s' in '%s'\n", s_versionNumberHeader, filename);
		delete[] pBuff;
		return false;
	}
	pVersionNum += strlen(s_versionNumberHeader);

	// Read the next line
	while(*pVersionNum != 0 && *pVersionNum <= ' ')
		++pVersionNum;
	if(!pVersionNum)
	{
		printf("Unexpected end of file in '%s'\n", filename);
		delete[] pBuff;
		return false;
	}
	char* pEnd = pVersionNum;
	while(*pEnd >= '0' && *pEnd <= '9')
		++pEnd;
	*pEnd = 0;
	r_version = (u32)atoi(pVersionNum);
	delete[] pBuff;
	return true;
}

//-------------------------------------------------------------------------------------------------

bool parseFunctionFile(const char* filename, std::vector<FunctionEntry>& r_functions)
{
	// Read file into memory
	char* pBuff = readFile(filename);
	if(!pBuff)
	{
		printf("Failed to open file '%s'\n", filename);
		return false;
	}

	// Process lines
	char* pBuffStart = pBuff;
	while(*pBuff != 0)
	{
		// Scan to end of line or comment
		char* pLineStart = pBuff;
		while(*pBuff != 0 && *pBuff != '\n' && *pBuff != '#')
			++pBuff;
		std::string line(pLineStart, pBuff);
		if(*pBuff == '#')
		{
			while(*pBuff != 0 && *pBuff != '\n')
				++pBuff;
		}
		while(*pBuff == '\r' || *pBuff == '\n')
			++pBuff;

		// Trim trailing whitespace
		while(!line.empty() && (line.back() == ' ' || line.back() == '\r' || line.back() == '\t'))
			line.erase(line.end()-1);

		// Blank line?
		if(line.empty())
			continue;

		// Take off action flags
		FunctionEntry entry;
		for(size_t i=line.length()-1; i>0; --i)
		{
			char ch = line[i];
			if(ch == ' ' || ch == '\t')
			{
				entry.actionFlags = (u32)atoi(line.c_str()+i+1);
				line.erase(line.begin()+i, line.end());
				break;
			}
		}

		// Trim trailing whitespace
		while(!line.empty() && (line.back() == ' ' || line.back() == '\r' || line.back() == '\t'))
			line.erase(line.end()-1);

		// Add entry to list
		entry.funcName = line;
		r_functions.push_back(entry);
	}

	delete[] pBuffStart;
	return true;
}

//-------------------------------------------------------------------------------------------------

bool parseElfFile(const char* filename, ELFFile& r_fileHeader, FILE*& r_pFile, ElfFormat fmt)
{
	// Open file
	FILE* pFile = fopen(filename, "rb");
	if(!pFile)
	{
		printf("Failed to open file '%s'\n", filename);
		return false;
	}

	// Read and verify header
	ELFHeader header;
	if(fread(&header, 1, sizeof(header), pFile) != sizeof(header))
	{
		printf("'%s' appears to be corrupt - too short\n", filename);
		fclose(pFile);
		return false;
	}
	if(memcmp(header.e_ident, s_elfIdent, sizeof(s_elfIdent)) != 0)
	{
		printf("'%s' is not a valid ELF file (Invalid ident).\n", filename);
		fclose(pFile);
		return false;
	}
	if(header.e_ident[EI_CLASS] != 2)
	{
		printf("'%s' is not a valid ELF file (Not 64-bit).\n", filename);
		fclose(pFile);
		return false;
	}
	if(header.e_ident[EI_DATA] != 1 && header.e_ident[EI_DATA] != 2)
	{
		printf("'%s' is not a valid ELF file (Invalid endian).\n", filename);
		fclose(pFile);
		return false;
	}
	bool bigEndian = header.e_ident[EI_DATA] == 2;
	if(header.e_ident[EI_VERSION] != 1)
	{
		printf("'%s' is not a valid ELF file (Invalid version).\n", filename);
		fclose(pFile);
		return false;
	}
	if(fmt == ELF_PS3 && header.e_ident[EI_OSABI] != 0x66)
	{
		printf("'%s' is not a PS3 ELF file (Invalid OS ABI).\n", filename);
		fclose(pFile);
		return false;
	}
	else if(fmt == ELF_PS4 && header.e_ident[EI_OSABI] != 0x09)
	{
		printf("'%s' is not a PS4 ELF file (Invalid OS ABI).\n", filename);
		fclose(pFile);
		return false;
	}

	// Endian swap header if required
	if(bigEndian)
	{
		header.e_type = swapEndian(header.e_type);
		header.e_machine = swapEndian(header.e_machine);
		header.e_version = swapEndian(header.e_version);
		header.e_entry = swapEndian(header.e_entry);
		header.e_phoff = swapEndian(header.e_phoff);
		header.e_shoff = swapEndian(header.e_shoff);
		header.e_flags = swapEndian(header.e_flags);
		header.e_ehsize = swapEndian(header.e_ehsize);
		header.e_phentsize = swapEndian(header.e_phentsize);
		header.e_phnum = swapEndian(header.e_phnum);
		header.e_shentsize = swapEndian(header.e_shentsize);
		header.e_shnum = swapEndian(header.e_shnum);
		header.e_shstrndx = swapEndian(header.e_shstrndx);
	}

	// Check sizes
	if(header.e_ehsize != sizeof(ELFHeader))
	{
		printf("'%s' is not a valid ELF file (Invalid e_ehsize).\n", filename);
		fclose(pFile);
		return false;
	}
	else if(header.e_phentsize != sizeof(ELFProgramEntry))
	{
		printf("'%s' is not a valid ELF file (Invalid e_phentsize).\n", filename);
		fclose(pFile);
		return false;
	}

	// PS3 machine / platform checks:
	if(fmt == ELF_PS3)
	{
		if(header.e_type != 0x0002 || header.e_machine != 0x0015 || header.e_version != 0x00000001)
		{
			printf("'%s' is not a PS3 ELF file\n", filename);
			fclose(pFile);
			return false;
		}
	}

	// PS4 machine / platform checks:
	else if(fmt == ELF_PS4)
	{
		if(header.e_type != 0xfe10 || header.e_machine != 0x003e || header.e_version != 0x00000001)
		{
			printf("'%s' is not a PS4 ELF file\n", filename);
			fclose(pFile);
			return false;
		}
	}

	// Read program header entries
	fseek(pFile, (long)header.e_phoff, SEEK_SET);
	ELFProgramEntry* pProgramEntries = new ELFProgramEntry[header.e_phnum];
	if(fread(pProgramEntries, 1, sizeof(ELFProgramEntry)*header.e_phnum, pFile) !=
		sizeof(ELFProgramEntry)*header.e_phnum)
	{
		printf("'%s' appears to be corrupt - too short\n", filename);
		delete[] pProgramEntries;
		fclose(pFile);
		return false;
	}
	if(bigEndian)
	{
		for(size_t i=0; i<header.e_phnum; ++i)
		{
			pProgramEntries[i].p_type = swapEndian(pProgramEntries[i].p_type);
			pProgramEntries[i].p_flags = swapEndian(pProgramEntries[i].p_flags);
			pProgramEntries[i].p_offset = swapEndian(pProgramEntries[i].p_offset);
			pProgramEntries[i].p_vaddr = swapEndian(pProgramEntries[i].p_vaddr);
			pProgramEntries[i].p_paddr = swapEndian(pProgramEntries[i].p_paddr);
			pProgramEntries[i].p_filesz = swapEndian(pProgramEntries[i].p_filesz);
			pProgramEntries[i].p_memsz = swapEndian(pProgramEntries[i].p_memsz);
			pProgramEntries[i].p_align = swapEndian(pProgramEntries[i].p_align);
		}
	}

	r_fileHeader.header = header;
	r_fileHeader.pProgramEntries = pProgramEntries;
	r_fileHeader.numProgramEntries = header.e_phnum;
	r_pFile = pFile;
	return true;
}

//-------------------------------------------------------------------------------------------------

bool parseSymbolFile(const char* filename, char*& r_pFileBuffer, SymbolMap& r_symbols)
{
	// Read file into memory
	char* pBuff = readFile(filename);
	if(!pBuff)
	{
		printf("Failed to open file '%s'\n", filename);
		return false;
	}

	// Process file
	r_pFileBuffer = pBuff;
	bool error = false;
	Symbol sym;
	while(*pBuff != 0)
	{
		// Read offset
		sym.offset = ReadHex(pBuff, error);
		if(error)
			break;

		// Skip symbol type (1 character)
		while(*pBuff == ' ')
			++pBuff;
		if(*pBuff != 0)
			++pBuff;
		while(*pBuff == ' ')
			++pBuff;

		// Read length
		sym.len = ReadHex(pBuff, error);
		if(error)
			break;

		// Read symbol name
		while(*pBuff == ' ')
			++pBuff;
		sym.pSymbol = pBuff;

		// Null terminate symbol name
		while(*pBuff >= ' ')
			++pBuff;
		if(*pBuff != 0)
			*pBuff++ = 0;

		// Skip whitespace
		while(*pBuff == '\r' || *pBuff == '\n')
			++pBuff;

		r_symbols[sym.pSymbol] = sym;
	}
	if(error)
	{
		printf("Failed to parse symbol file '%s' - is it a valid PS3 .sym file?\n", filename);
		delete[] r_pFileBuffer;
		r_pFileBuffer = 0;
		r_symbols.clear();
		return false;
	}
	return true;
}

//-------------------------------------------------------------------------------------------------

bool parseMapFile(const char* filename, char*& r_pFileBuffer, SymbolMap& r_symbols)
{
	// Reset
	r_pFileBuffer = 0;
	r_symbols.clear();

	// Read file into memory
	char* pBuff = readFile(filename);
	if(!pBuff)
	{
		printf("Failed to open file '%s'\n", filename);
		return false;
	}

	// Check header (We're not very forgiving...)
	if(strncmp(pBuff, "Address  Size     Align Out     In      File    Symbol", 54) != 0)
	{
		printf("Failed to parse map file '%s' - First line was not what was expected.\n", filename);
		delete[] pBuff;
		return false;
	}
	r_pFileBuffer = pBuff;

	// Skip to first line of actual data
	bool error = false;
	for(;;)
	{
		if(*pBuff != ' ')
		{
			char* pPrev = pBuff;
			ReadHex(pBuff, error);
			if(!error)
			{
				pBuff = pPrev;
				break;
			}
		}
		while(*pBuff != '\r' && *pBuff != '\n')
			++pBuff;
		while(*pBuff == '\r' || *pBuff == '\n')
			++pBuff;
	}

	// Process symbols
	Symbol sym;
	const size_t symbolOffset = 48; // Column index of the symbol name in the .map
	while(*pBuff != 0)
	{
		// Read offset
		char* pLineStart = pBuff;
		sym.offset = ReadHex(pBuff, error);
		if(error)
			break;

		// Skip whitespace
		while(*pBuff <= ' ' && *pBuff != 0)
			++pBuff;

		// Read length
		sym.len = ReadHex(pBuff, error);
		if(error)
			break;

		// Skip to symbol offset
		error = false;
		for(size_t ofs=pBuff-pLineStart; ofs<symbolOffset; ++ofs)
		{
			if(pLineStart[ofs] == '\r' || pLineStart[ofs] == '\n')
			{
				error = true;
				break;
			}
		}
		if(!error)
		{
			// Is there a symbol name on this line?
			pBuff = pLineStart + symbolOffset;
			if(pBuff[0] > ' ' && pBuff[-1] <= ' ')
			{
				// Read symbol name
				sym.pSymbol = pBuff;

				// Null terminate symbol name
				while(*pBuff >= ' ')
					++pBuff;
				if(*pBuff != 0)
					*pBuff++ = 0;

				r_symbols[sym.pSymbol] = sym;
			}
		}

		// Skip to next line
		while(*pBuff != '\r' && *pBuff != '\n')
			++pBuff;
		while(*pBuff == '\r' || *pBuff == '\n')
			++pBuff;
	}
	if(error)
	{
		printf("Failed to parse map file '%s'\n", filename);
		delete[] r_pFileBuffer;
		r_pFileBuffer = 0;
		r_symbols.clear();
		return false;
	}
	return true;
}

//-------------------------------------------------------------------------------------------------

bool resolveFunctionNames(std::vector<FunctionEntry>& r_functionList, const SymbolMap& symbols)
{
	// Process each function in the list
	for(size_t i=0; i<r_functionList.size(); ++i)
	{
		// Does this look like an address & length?
		FunctionEntry& func = r_functionList[i];
		bool error;
		char* pBuff = (char*) func.funcName.c_str();
		size_t hexVal = ReadHex(pBuff, error);
		if(!error)
		{
			// Check address is valid
			if(hexVal > 0xffffffff)
			{
				printf("Invalid address: 0x%p. Address must be 32-bit.\n", (void*)hexVal);
				return false;
			}
			func.offset = (u32)hexVal;

			// Eat whitespace
			while(*pBuff <= ' ' && *pBuff != 0)
				++pBuff;

			// Parse length
			hexVal = ReadHex(pBuff, error);
			if(error)
			{
				printf("Invalid address & length or symbol name: \"%s\".\n", func.funcName.c_str());
				return false;
			}
			else if(hexVal > 0xffffffff)
			{
				printf("Invalid length: %s. Length must be 32-bit.\n", (void*)hexVal);
				return false;
			}
			func.len = (u32)hexVal;
		}
		else if(symbols.empty())
		{
			printf("Symbol \"%s\" referenced in function list file, but no symbol file was specified with -sym.\n",
				func.funcName.c_str());
			return false;
		}
		else
		{
			// Lookup function in the sym file
			SymbolMapConstIt it = symbols.find(func.funcName.c_str());
			if(it == symbols.end())
			{
				printf("Could not find symbol \"%s\" in the symbol file.\n", func.funcName.c_str());
				return false;
			}
			const Symbol& sym = it->second;
			if(sym.len == 0)
			{
				printf("Symbol \"%s\" has a length of 0.\n", func.funcName.c_str());
				return false;
			}
			func.offset = sym.offset;
			func.len = (u32)sym.len;
		}
	}

	return true;
}

//-------------------------------------------------------------------------------------------------

bool generateOutputFile(const char* filename, const std::vector<FunctionEntry>& functionList,
	u32 versionID)
{
	FILE* pFile = fopen(filename, "wb");
	if(!pFile)
	{
		printf("Failed to open output file '%s'\n", filename);
		return false;
	}
	for(size_t i=0; i<functionList.size(); ++i)
	{
		// NB: Order here is specified by CTunables::LoadMemoryChecks() in game code
		u32 params[5];
		params[0] = versionID ^ functionList[i].crc ^ s_xorValue;
		params[1] = ((u32)functionList[i].offset) ^ functionList[i].crc ^ s_xorValue;
		params[2] = functionList[i].len ^ functionList[i].crc ^ s_xorValue;
		params[3] = functionList[i].crc;
		params[4] = functionList[i].actionFlags ^ functionList[i].crc ^ s_xorValue;
		fprintf(pFile, "[%d,%d,%d,%d,%d],", params[0], params[1], params[2], params[3], params[4]);
	}
	fclose(pFile);
	return true;
}

bool generateDummyOutputFile(const char* filename, u32 versionID, u32 numCRCs, size_t minRange,
	size_t maxRange, bool verbose, bool noActionFlags)
{
	FILE* pFile = fopen(filename, "wb");
	if(!pFile)
	{
		printf("Failed to open output file '%s'\n", filename);
		return false;
	}
	srand((unsigned)time(NULL));
	for(u32 i=0; i<numCRCs; ++i)
	{
		u32 crc = GenRandU32();
		u32 offset = GenRandU32() & 0x23fffff;	// Roughly what the max code address is
		u32 len = GenRandU32() & 0xffff;		// 64KB max len
		u32 actionFlags = GenRandU32() & 0xff;	// 8-bit random flags

		// Clamp range?
		if(minRange != 0 || maxRange != 0)
		{
			offset = (u32)minRange + (GenRandU32() % (u32)(maxRange - minRange - 256));
			len = GenRandU32() & 0xff;		// 255 byte max len
		}

		// Round down to 4 byte boundary
		offset &= 0xfffffff0;

		// No flags?
		if(noActionFlags)
			actionFlags = 0;

		// NB: Order here is specified by CTunables::LoadMemoryChecks() in game code
		u32 params[5];
		params[0] = versionID ^ crc ^ s_xorValue;
		params[1] = offset ^ crc ^ s_xorValue;
		params[2] = len ^ crc ^ s_xorValue;
		params[3] = crc;
		params[4] = actionFlags ^ crc ^ s_xorValue;
		fprintf(pFile, "[%d,%d,%d,%d,%d],", params[0], params[1], params[2], params[3], params[4]);

		if(verbose)
		{
			u32 xor = crc ^ s_xorValue;
			printf("Dummy CRC %d:\n", i);
			printf("Version: 0x%08x (Obfuscated: %d)\n", versionID, versionID ^ xor);
			printf("Offset:  0x%08x (Obfuscated: %d)\n", offset, offset ^ xor);
			printf("Length:  0x%08x (Obfuscated: %d)\n", len, len ^ xor);
			printf("CRC:     0x%08x (Obfuscated: %d)\n", crc, crc);
			printf("Flags:   0x%08x (Obfuscated: %d)\n", actionFlags, actionFlags ^ xor);
			printf("\n");
		}
	}
	fclose(pFile);
	return true;
}

//-------------------------------------------------------------------------------------------------

int doCustomCheck(char** argv)
{
	int checkType = atoi(argv[0]);
	if(checkType < 0 || checkType > 0xff)
	{
		printf("Invalid check type, %d. Valid range is [0..255]\n", checkType);
		return 1;
	}

	int version = atoi(argv[1]);
	if(version < 0 || version > 0xffff)
	{
		printf("Invalid version, %d. Valid range is [0..65535]\n", version);
		return 2;
	}

	int sku = atoi(argv[2]);
	if(sku < 0 || sku > 0xffff)
	{
		printf("Invalid SKU ID, %d. Valid range is [0..65535]\n", sku);
		return 3;
	}

	bool error;
	size_t address = ReadHex(argv[3], error);
	if(error)
	{
		printf("Invalid address, %s\n", argv[3]);
		return 4;
	}
	else if(address > 0xffffffff)
	{
		printf("Invalid address, %p. Must be 32-bit.\n", address);
		return 5;
	}

	size_t length = ReadHex(argv[4], error);
	if(error)
	{
		printf("Invalid length, %s.\n", argv[3]);
		return 6;
	}
	else if(length > 0xffffffff)
	{
		printf("Invalid length, %p. Must be 32-bit.\n", length);
		return 7;
	}

	size_t valueSizeT = ReadHex(argv[5], error);
	if(error)
	{
		printf("Invalid value, %s.\n", argv[3]);
		return 8;
	}
	else if(valueSizeT > 0xffffffff)
	{
		printf("Invalid value, %p. Must be 32-bit.\n", valueSizeT);
		return 9;
	}
	u32 value = (u32)valueSizeT;

	int actionFlags = atoi(argv[6]);

	u32 versionID = (checkType << 24) | (sku << 16) | version;

	// NB: Order here is specified by CTunables::LoadMemoryChecks() in game code
	u32 params[5];
	u32 xor = value ^ s_xorValue;
	params[0] = versionID ^ xor;
	params[1] = (u32)address ^ xor;
	params[2] = (u32)length ^ xor;
	params[3] = value;
	params[4] = actionFlags ^ xor;

	printf("Version: 0x%08x (Obfuscated: %d)\n", versionID, params[0]);
	printf("Offset:  0x%08x (Obfuscated: %d)\n", (u32)address, params[1]);
	printf("Length:  0x%08x (Obfuscated: %d)\n", (u32)length, params[2]);
	printf("CRC:     0x%08x (Obfuscated: %d)\n", value, params[3]);
	printf("Flags:   0x%08x (Obfuscated: %d)\n", actionFlags, params[4]);
	printf("\n");
	printf("JSON data for tunables:\n");
	printf("[%d,%d,%d,%d,%d]\n", params[0], params[1], params[2], params[3], params[4]);
	return 0;
}

//-------------------------------------------------------------------------------------------------

int doFileCRC(const char* filename)
{
	// Read file into memory
	size_t fileSize;
	u8* pBuff = (u8*)readFile(filename, &fileSize);
	if(!pBuff)
	{
		printf("Failed to open file '%s'\n", filename);
		return false;
	}

	// CRC it and display CRC
	u32 crc = crcRange(pBuff, pBuff+fileSize);
	printf("CRC for \"%s\" is:\n0x%08x\n", filename, crc);

	// Cleanup and return
	delete[] pBuff;
	return 0;
}

//-------------------------------------------------------------------------------------------------
// -elf eboot.elf -out out.json -funcs FunctionList.txt -version X:\gta5\titleupdate\TU_Sub_13_0\common\data\version.txt -sku 2 -verbose
// -elf game_orbis_beta.elf -map game_orbis_beta.map -relativeto "rage::CommonMain(int, char**)" -out out.json -funcs FunctionList.txt -version X:\gta5\titleupdate\dev_ng\common\data\version.txt -sku 5 -verbose
// -customcheck 2 704 0 0x03162D98 0x20 0x00 32
// -customcheck 1 704 0 0x03162D98 0x04 0xdbf1bc87 32
// -customcheck 0 591 5 0x03162D98 0x10 0xdbf1bc87 1 relative

int main(int argc, char** argv)
{
	// Get arguments from command line
	std::string elfFile, symFile, mapFile, relativeSym, outFile, funcsFile, versionFile;
	u32 sku = 0;
	ElfFormat elfFormat = ELF_Unknown;
	bool gotSku = false;
	bool verbose = false;
	bool doDummyCRC = false;
	bool noActionFlags = false;
	u32 dummyVersionNum = 0;
	u32 dummyCRCCount = 0;
	size_t minRange = 0, maxRange = 0;
	for(int i=1; i<argc; ++i)
	{
		if((stricmp(argv[i], "-elf") == 0 || stricmp(argv[i], "-ps3elf") == 0) && i != argc-1)
		{
			elfFile = argv[i+1];
			elfFormat = ELF_PS3;
			++i;
		}
		if(stricmp(argv[i], "-ps4elf") == 0 && i != argc-1)
		{
			elfFile = argv[i+1];
			elfFormat = ELF_PS4;
			++i;
		}
		else if(stricmp(argv[i], "-sym") == 0 && i != argc-1)
		{
			symFile = argv[i+1];
			++i;
		}
		else if(stricmp(argv[i], "-map") == 0 && i != argc-1)
		{
			mapFile = argv[i+1];
			++i;
		}
		else if(stricmp(argv[i], "-relativeto") == 0 && i != argc-1)
		{
			relativeSym = argv[i+1];
			++i;
		}
		else if(stricmp(argv[i], "-out") == 0 && i != argc-1)
		{
			outFile = argv[i+1];
			++i;
		}
		else if(stricmp(argv[i], "-funcs") == 0 && i != argc-1)
		{
			funcsFile = argv[i+1];
			++i;
		}
		else if(stricmp(argv[i], "-version") == 0 && i != argc-1)
		{
			versionFile = argv[i+1];
			++i;
		}
		else if(stricmp(argv[i], "-sku") == 0 && i != argc-1)
		{
			sku = atoi(argv[i+1]);
			gotSku = true;
			++i;
		}
		else if(stricmp(argv[i], "-verbose") == 0)
		{
			verbose = true;
		}
		else if(stricmp(argv[i], "-dummyCRCs") == 0 && argc-i >= 2)
		{
			dummyVersionNum = atoi(argv[i+1]);
			dummyCRCCount = atoi(argv[i+2]);
			doDummyCRC = true;
			i += 2;
		}
		else if(stricmp(argv[i], "-addrRange") == 0 && argc-i >= 2)
		{
			char* buff = argv[i+1];
			bool error;
			minRange = ReadHex(buff, error);
			if(!error)
			{
				buff = argv[i+2];
				maxRange = ReadHex(buff, error);
			}
			if(error)
			{
				printf("Invalid address range - not a hex number\n");
				displayUsage();
				return -1;
			}
			i += 2;
		}
		else if(stricmp(argv[i], "-noActionFlags") == 0)
		{
			noActionFlags = true;
		}
		else if(stricmp(argv[i], "-customcheck") == 0 && argc-i >= 7)
		{
			return doCustomCheck(argv+i+1);
		}
		else if(stricmp(argv[i], "-crc") == 0 && i != argc-1)
		{
			return doFileCRC(argv[i+1]);
		}
		else
		{
			printf("Unknown option or missing parameter for option '%s'\n", argv[i]);
		}
	}
	if(outFile.empty())
	{
		printf("Missing output filename\n");
		displayUsage();
		return -1;
	}

	if(doDummyCRC)
	{
		if(minRange != 0 || maxRange != 0)
		{
			if(minRange >= maxRange || maxRange-minRange < 256)
			{
				printf("Invalid address range - Min must be < max, and range must be >= 256 bytes\n");
				displayUsage();
				return -1;
			}
		}
		printf("Generating %d dummy CRCs using version number %d\n", dummyCRCCount, dummyVersionNum);
	}
	else if(elfFile.empty())
	{
		printf("Missing elf filename\n");
		displayUsage();
		return -1;
	}
	else if(funcsFile.empty())
	{
		printf("Missing functions filename\n");
		displayUsage();
		return -1;
	}
	else if(versionFile.empty())
	{
		printf("Missing version filename\n");
		displayUsage();
		return -1;
	}
	else if(!gotSku)
	{
		printf("Missing SKU identifier\n");
		displayUsage();
		return -1;
	}
	else if(sku > 0xffff)
	{
		printf("Invalid SKU, %d. SKU must be 0..65535\n", sku);
		return 1;
	}

	// Process game version file and generate 32-bit version & SKU
	u32 version;
	if(doDummyCRC)
	{
		version = dummyVersionNum;
	}
	else if(!parseVersionFile(versionFile.c_str(), version))
	{
		return 2;
	}
	if(version > 0xffff)
	{
		printf("Invalid game version, %d. Version must be 0..65535\n", version);
		return 3;
	}
	u32 packedVersion = (sku << 16) | version;
	printf("Game version %d, SKU ID %d = version code 0x%08x\n", version, sku, packedVersion);

	// Process function list file
	std::vector<FunctionEntry> functionList;
	if(!doDummyCRC)
	{
		functionList.reserve(1024);
		if(!parseFunctionFile(funcsFile.c_str(), functionList))
		{
			return 4;
		}
		if(verbose)
		{
			printf("Parsed %u functions from \"%s\"\n", functionList.size(), funcsFile.c_str());
		}

		// Process elf file
		ELFFile elfFileHeader;
		FILE* pElfFile = 0;
		if(!parseElfFile(elfFile.c_str(), elfFileHeader, pElfFile, elfFormat))
		{
			return 5;
		}

		// Process sym / map file
		char* pSymFileBuffer;
		SymbolMap symbols;
		if(!symFile.empty() && !parseSymbolFile(symFile.c_str(), pSymFileBuffer, symbols))
		{
			return 6;
		}
		else if(!mapFile.empty() && !parseMapFile(mapFile.c_str(), pSymFileBuffer, symbols))
		{
			return 6;
		}
		if(verbose && !symFile.empty())
		{
			printf("Parsed %u symbols from \"%s\"\n", symbols.size(), symFile.c_str());
		}
		else if(verbose && !mapFile.empty())
		{
			printf("Parsed %u symbols from \"%s\"\n", symbols.size(), mapFile.c_str());
		}

		// Resolve function addresses
		if(!resolveFunctionNames(functionList, symbols))
		{
			return 7;
		}

		// Do we need to apply a relative offset?
		size_t relativeOffset = 0;
		if(!relativeSym.empty())
		{
			// Lookup symbol
			SymbolMapConstIt it = symbols.find(relativeSym.c_str());
			if(it == symbols.end())
			{
				printf("Could not find relative-to symbol \"%s\" in the symbol file.\n", relativeSym.c_str());
				return 11;
			}
			relativeOffset = it->second.offset;
		}

		// Process each function in the list
		bool error = false;
		u8* pBuffer = 0;
		size_t bufferSize = 0;
		for(size_t i=0; i<functionList.size(); ++i)
		{
			// Find which elf section this symbol is in
			FunctionEntry& func = functionList[i];
			const ELFProgramEntry* pSection = 0;
			for(size_t j=0; j<elfFileHeader.numProgramEntries; ++j)
			{
				const ELFProgramEntry& pe = elfFileHeader.pProgramEntries[j];
				if(func.offset >= pe.p_vaddr && func.offset <= pe.p_vaddr+pe.p_filesz)
				{
					pSection = &pe;
					break;
				}
			}
			if(!pSection)
			{
				printf("Could not find appropriate elf section for symbol \"%s\".\n",
					func.funcName.c_str());
				error = true;
				break;
			}

			// Seek to the symbol
			size_t fileOffset = func.offset - pSection->p_vaddr + pSection->p_offset;
			fseek(pElfFile, (long)fileOffset, SEEK_SET);

			// Read it into memory
			if(func.len > bufferSize)
			{
				delete[] pBuffer;
				bufferSize = func.len*2;
				pBuffer = new u8[bufferSize];
			}
			if(fread(pBuffer, 1, func.len, pElfFile) != func.len)
			{
				printf("Could not read from elf file for symbol \"%s\".\n", func.funcName.c_str());
				error = true;
				break;
			}

			// CRC it
			func.crc = crcRange(pBuffer, pBuffer+func.len);

			// Apply relative offset and make sure offset is 32-bit
			s64 adjustedOffset = (s64)(func.offset - relativeOffset);
			if(abs(adjustedOffset) > 0x7fffffff)
			{
				printf("Symbol \"%s\" has a relative offset of 0x%08I64x after relative offset applied, which is too large.\n",
					adjustedOffset, func.funcName.c_str());
				error = true;
				break;
			}
			func.offset = (u32)(s32)adjustedOffset;

			// Verbose output?
			if(verbose)
			{
				u32 xor = func.crc ^ s_xorValue;
				printf("%s:\n", func.funcName.c_str());
				printf("Version: 0x%08x (Obfuscated: %d)\n", packedVersion, packedVersion ^ xor);
				if(!relativeSym.empty())
				{
					printf("Offset:  0x%08x (Absolute 0x%p, ObfuscatedOfs: %d)\n", (u32)func.offset,
						((s32)func.offset)+relativeOffset, ((u32)func.offset) ^ xor);
				}
				else
					printf("Offset:  0x%08x (Obfuscated: %d)\n", (u32)func.offset, ((u32)func.offset) ^ xor);
				printf("Length:  0x%08x (Obfuscated: %d)\n", func.len, func.len ^ xor);
				printf("CRC:     0x%08x (Obfuscated: %d)\n", func.crc, func.crc);
				printf("Flags:   0x%08x (Obfuscated: %d)\n", func.actionFlags, func.actionFlags ^ xor);
				printf("\n");
			}
		}

		// Close elf file
		fclose(pElfFile);
		if(error)
		{
			// Don't bother cleaning up memory allocs, the OS will do it faster
			return 8;
		}
	}

	// Generate output
	if(doDummyCRC)
	{
		if(!generateDummyOutputFile(outFile.c_str(), packedVersion, dummyCRCCount, minRange,
			maxRange, verbose, noActionFlags))
		{
			return 9;
		}
		printf("Generated dummy CRCs for %d functions.\n", dummyCRCCount);
	}
	else
	{
		if(!generateOutputFile(outFile.c_str(), functionList, packedVersion))
		{
			return 10;
		}
		printf("Generated CRCs for %d functions.\n", functionList.size());
	}

	// Don't bother cleaning up memory allocs, the OS will do it faster
	return 0;
}

//-------------------------------------------------------------------------------------------------
