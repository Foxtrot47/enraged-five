@echo off

:: Config
set versionfile=X:\gta5\titleupdate\dev\common\data\version.txt
set functionfile=FunctionList.txt
set prefix=X:\gta5\titleupdate\dev\game_psn_beta_snc

"%RS_TOOLSROOT%\bin\GenerateCRCs\GenerateCRCs.exe" -elf "%prefix%.elf" -sym "%prefix%.sym" -out "crcs_beta.json" -funcs "%functionfile%" -version "%versionfile%" -sku 0 -verbose

echo Done!
pause
