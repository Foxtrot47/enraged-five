// 
// xorc/visitorresource.cpp
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

using namespace rage;

#include "visitorresource.h"

#include "atl/array_struct.h"
#include "parser/manager.h"
#include "vector/vector2.h"
#include "vector/vector3.h"
#include "vector/vector4.h"
#include "vector/matrix34.h"
#include "vector/matrix44.h"

void SwapVector3(Vector3& d)
{
	datSwapper(d.x);
	datSwapper(d.y);
	datSwapper(d.z);
}

void SwapVector4(Vector4& d)
{
	datSwapper(d.x);
	datSwapper(d.y);
	datSwapper(d.z);
	datSwapper(d.w);
}

void xorcVisitorResource::VisitStructure(void* dataPtr, parStructure& metadata)
{
	parNonGenericVisitor::VisitStructure(dataPtr, metadata);

	if (metadata.GetFlags().IsSet(parStructure::HAS_VIRTUALS))
	{
		// Overwrite the vptr with the hash of the structure name

		// treat ptrRef as an array of u32*s, overwrite its first element (which is where the vtbl ptr goes)
		((u32*)dataPtr)[0] = metadata.GetNameHash();
		datSwapper(((u32*)dataPtr)[0]);
	}
}

void xorcVisitorResource::Vector2Member( Vector2& d, parMemberVector& )
{
	datSwapper(d.x);
	datSwapper(d.y);
}

void xorcVisitorResource::Vector3Member( Vector3& d, parMemberVector& )
{
	SwapVector3(d);
}

void xorcVisitorResource::Vector4Member( Vector4& d, parMemberVector& )
{
	SwapVector4(d);
}

void xorcVisitorResource::Matrix34Member( Matrix34& d, parMemberMatrix& )
{
	SwapVector3(d.a);
	SwapVector3(d.b);
	SwapVector3(d.c);
	SwapVector3(d.d);
}

void xorcVisitorResource::Matrix44Member( Matrix44& d, parMemberMatrix& )
{
	SwapVector4(d.a);
	SwapVector4(d.b);
	SwapVector4(d.c);
	SwapVector4(d.d);
}

void xorcVisitorResource::EnumMember(int& , parMemberEnum& metadata)
{
	switch(metadata.GetSubtype())
	{
	case parMemberEnumSubType::SUBTYPE_32BIT:
		datSwapper(metadata.GetMemberFromStruct<u32>(m_ContainingStructureAddress));
		break;
	case parMemberEnumSubType::SUBTYPE_16BIT:
		datSwapper(metadata.GetMemberFromStruct<u16>(m_ContainingStructureAddress));
		break;
	case parMemberEnumSubType::SUBTYPE_8BIT:
		datSwapper(metadata.GetMemberFromStruct<u8>(m_ContainingStructureAddress));
		break;
	}
}


void xorcVisitorResource::PointerStringMember( char*& d, parMemberString& )
{
	datSwapper(d);
}

void xorcVisitorResource::ConstStringMember( ConstString& d, parMemberString& )
{
	datTypeStruct s;
	d.DeclareStruct(s);
}

void xorcVisitorResource::AtStringMember( atString& d, parMemberString& )
{
	datTypeStruct s;
	d.DeclareStruct(s);
}

void xorcVisitorResource::WidePointerStringMember( char16*& str, parMemberString& )
{
	if (str)
	{
		char16* s = str;
		while(*s != 0)
		{
			datSwapper(*s);
			s++;
		}
	}
	datSwapper((char*&)str);
}

void xorcVisitorResource::AtWideStringMember( atWideString&, parMemberString& )
{
	Errorf("atWideString resourcing is not supported");
}

void xorcVisitorResource::AtHashStringMember(atHashString& str, parMemberString& )
{
	// HACK because there's no good way to get the m_hash member
	datSwapper(reinterpret_cast<u32&>(str));
}

void xorcVisitorResource::ExternalPointerMember( void*& , parMemberStruct& )
{
	Errorf("External pointer resourcing is not supported");
}


void xorcVisitorResource::EndArrayMember( void* , void* , u32, parMemberArray& metadata )
{
	// we may need to relocate some pointers
	switch(metadata.GetSubtype())
	{
	case parMemberArraySubType::SUBTYPE_ATARRAY:
		{
			atArray<char,0,u16>& arr = metadata.GetMemberFromStruct<atArray<char,0,u16> >(m_ContainingStructureAddress);
			arr.DatSwapAllButContents();	
		}
		break;
	case parMemberArraySubType::SUBTYPE_ATARRAY_32BIT_IDX:
		{
			atArray<char,0,u32>& arr = metadata.GetMemberFromStruct<atArray<char,0,u32> >(m_ContainingStructureAddress);
			arr.DatSwapAllButContents();
		}
		break;
	case parMemberArraySubType::SUBTYPE_POINTER:
	case parMemberArraySubType::SUBTYPE_POINTER_WITH_COUNT:
	case parMemberArraySubType::SUBTYPE_POINTER_WITH_COUNT_8BIT_IDX:
	case parMemberArraySubType::SUBTYPE_POINTER_WITH_COUNT_16BIT_IDX:
		datSwapper(metadata.GetMemberFromStruct<char*>(m_ContainingStructureAddress));
		break;
	case parMemberArraySubType::SUBTYPE_VIRTUAL:
		Errorf("virtual array resourcing is not supported");
		break;
	case parMemberArraySubType::SUBTYPE_ATFIXEDARRAY:
		{
			int* countAddr = metadata.GetFixedArrayCountAddress(m_ContainingStructureAddress);
			datSwapper(*countAddr);
		}
		break;
	case parMemberArraySubType::SUBTYPE_ATRANGEARRAY:
	case parMemberArraySubType::SUBTYPE_MEMBER:
		// nothing to do here - these types don't have pointers or counts
		break;
	}
}

void xorcVisitorResource::EndPointerMember( void*& ptrRef, parMemberStruct& metadata)
{
	datSwapper(ptrRef);
}

//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
#if __DEV && !__64BIT
void xorcVisitorPlaceRsc::VisitStructure(void* dataPtr, parStructure& metadata)
{
	metadata.PlaceRsc(dataPtr, *m_Rsc);
	parNonGenericVisitor::VisitStructure(dataPtr, metadata);
}


bool xorcVisitorPlaceRsc::BeginPointerMember( void*& ptrRef, parMemberStruct& metadata )
{
	m_Rsc->Fixup(ptrRef);

	// Need to peek at the pointed to object and register it with the external structure manager, so that GetConcreteStructure (called
	// by the base class visitor) can return the right thing
	if (ptrRef)
	{
		if (metadata.CanDerive())
		{
			u32* hashPtr = reinterpret_cast<u32*>(ptrRef);
			PARSER.GetExternalStructureManager().AddInstance(ptrRef, PARSER.FindStructure(*hashPtr));
		}
		else
		{
			PARSER.GetExternalStructureManager().AddInstance(ptrRef, metadata.GetBaseStructure());
		}
	}

	return true;
}


bool xorcVisitorPlaceRsc::BeginArrayMember( void* /*ptrToMember*/, void* /*arrayContents*/, u32 /*numElements*/, parMemberArray& metadata)
{
	switch(metadata.GetSubtype())
	{
	case parMemberArraySubType::SUBTYPE_ATARRAY:
		{
			atArray<char,0,u16>& arr = metadata.GetMemberFromStruct<atArray<char,0,u16> >(m_ContainingStructureAddress);
			atArray<char,0,u16>::Place(&arr, *m_Rsc);
		}
		break;
	case parMemberArraySubType::SUBTYPE_ATARRAY_32BIT_IDX:
		{
			atArray<char,0,u32>& arr = metadata.GetMemberFromStruct<atArray<char,0,u32> >(m_ContainingStructureAddress);
			atArray<char,0,u32>::Place(&arr, *m_Rsc);
		}
		break;
	case parMemberArraySubType::SUBTYPE_POINTER:
	case parMemberArraySubType::SUBTYPE_POINTER_WITH_COUNT:
	case parMemberArraySubType::SUBTYPE_POINTER_WITH_COUNT_8BIT_IDX:
	case parMemberArraySubType::SUBTYPE_POINTER_WITH_COUNT_16BIT_IDX:
		m_Rsc->Fixup(metadata.GetMemberFromStruct<char*>(m_ContainingStructureAddress));
		break;
	case parMemberArraySubType::SUBTYPE_VIRTUAL:
		Errorf("virtual array resourcing is not supported");
		break;
	case parMemberArraySubType::SUBTYPE_ATFIXEDARRAY:
	case parMemberArraySubType::SUBTYPE_ATRANGEARRAY:
	case parMemberArraySubType::SUBTYPE_MEMBER:
		// nothing to do here - these types don't have pointers
		break;
	}

	// Check what the contents of the array are - don't need to recurse into POD arrays
	switch(metadata.GetPrototypeMember()->GetType())
	{
	case parMemberType::TYPE_BOOL:
	case parMemberType::TYPE_CHAR:
	case parMemberType::TYPE_ENUM:
	case parMemberType::TYPE_FLOAT:
	case parMemberType::TYPE_INT:
	case parMemberType::TYPE_MATRIX34:
	case parMemberType::TYPE_MATRIX44:
	case parMemberType::TYPE_SHORT:
	case parMemberType::TYPE_UCHAR:
	case parMemberType::TYPE_UINT:
	case parMemberType::TYPE_USHORT:
	case parMemberType::TYPE_VECTOR2:
	case parMemberType::TYPE_VECTOR3:
	case parMemberType::TYPE_VECTOR4:
		return false;
	default:
		break;
	}

	return true;
}

void xorcVisitorPlaceRsc::PointerStringMember(char*& ptrRef, parMemberString&)
{
	m_Rsc->Fixup(ptrRef);
}

void xorcVisitorPlaceRsc::ConstStringMember(ConstString& str, parMemberString& )
{
	ConstString::Place(&str, *m_Rsc);
}

void xorcVisitorPlaceRsc::AtStringMember(atString& str, parMemberString& )
{
	atString::Place(&str, *m_Rsc);
}

void xorcVisitorPlaceRsc::WidePointerStringMember(char16*& ptrRef, parMemberString& )
{
	m_Rsc->Fixup(ptrRef);
}

void xorcVisitorPlaceRsc::AtWideStringMember(atWideString&, parMemberString& )
{
	parErrorf("Not implemented!");
}
#endif