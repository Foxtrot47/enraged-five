// 
// /xorc.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 
#define SIMPLE_HEAP_SIZE	(248*1024)


#include "system/main.h"
#include "system/param.h"
#include "system/platform.h"

#if !__RESOURCECOMPILER
#error "Only Rsc platform will will work properly."
#endif

#include "offlinersc/offlinersc.h"
#include "parser/manager.h"
#include "parser/psoschemabuilder.h"
#include "parser/visitorutils.h"

#include "file/asset.h"
#include "file/token.h"

#include "paging/dictionary.h"
#include "paging/rscbuilder.h"

#include "visitorresource.h"

using namespace rage;

REQ_PARAM(meta, "Location of the parser metadata file");

PARAM(test, "Load the resource and write the result to the specified file");

parStructure* g_TopLevelStructureMetadata = NULL;

class ResourceableBase : public pgBase
{
public:
	void DeclareStruct(datTypeStruct&)
	{
		xorcVisitorResource swapper;
		swapper.VisitToplevelStructure(this, *g_TopLevelStructureMetadata);
	}
};

int GenerateResource(parTree* treeData) 
{
	const char* extn = "#xr";
	const int version = 1;

	// OLRC is hardcoded to read argv[1] and argv[2] for the input and output names - so be careful when adding new arguments
	OffLineResourceCreator	resourcer(false, false);

	// TODO: We should really check to make sure that the class we want to resource derives from pgBase - maybe pgBase should be parsable?
	// But at any rate pgBase contains some data so we can't just go around casting arbitrary data as a pgBase*.

	while ( resourcer.Next() )
	{
		// For each pass in the resourcer, make a copy of the structure we loaded in
		// Create an instance of that structure
		void* sourceData = g_TopLevelStructureMetadata->Create();
		PARSER.LoadFromStructure(treeData->GetRoot(), *g_TopLevelStructureMetadata, sourceData);

		resourcer.Resource((ResourceableBase*)sourceData, extn, version);
	}

	const char* testOutName = NULL;
	if (PARAM_test.Get(testOutName))
	{
		pgStreamer::InitClass();

		datResourceMap map;
		datResourceInfo hdr;
		void* resourceData = pgRscBuilder::LoadBuild(sysParam::GetArg(2),extn,version,map,hdr);
		//		IsPgBase(t);
		datResource rsc(map,sysParam::GetArg(2));
		xorcVisitorPlaceRsc placer(rsc);

		bool oldFlag = PARSER.Settings().SetFlag(parSettings::INITIALIZE_NEW_DATA, false);
		placer.VisitStructure(resourceData, *g_TopLevelStructureMetadata);
		PARSER.Settings().SetFlag(parSettings::INITIALIZE_NEW_DATA, oldFlag);

		pgRscBuilder::Cleanup(map);		

		// The resource is loaded, now save it out as an XML
		PARSER.SaveFromStructure(testOutName, "meta", *g_TopLevelStructureMetadata, resourceData);

		delete resourceData;

		pgStreamer::ShutdownClass();
	}

	return resourcer.HasErrors();
}

int Main() {
#if PARSER_USES_META_METADATA
	INIT_PARSER;

	if (sysParam::GetArgCount() < 3)
		Quitf("Usage:\n xorc input.xml output.#xr (or output.pso) -meta=metadata.psm");

	// Load definitions for the objects we'll be creating
	const char* metafile = NULL;
	PARAM_meta.Get(metafile);

	PARSER.LoadStructureMetadata(metafile);

	// Load the tree - so we can read the first element to see what to create
	parTree* treeData = PARSER.LoadTree(sysParam::GetArg(1), "");

	// Find the parStructure metadata for the root of the tree
	g_TopLevelStructureMetadata = PARSER.FindStructure(treeData->GetRoot()->GetElement().GetName());


	int status = 1;

	if (!stricmp(ASSET.FindExtensionInPath(sysParam::GetArg(2)), ".pso"))
	{
		void* sourceData = g_TopLevelStructureMetadata->Create();

		PARSER.LoadFromStructure(treeData->GetRoot(), *g_TopLevelStructureMetadata, sourceData);

		bool result = psoSaveFromStructure("C:\\test.pso", *g_TopLevelStructureMetadata, sourceData);
		status = result ? 0 : 1;

		g_TopLevelStructureMetadata->Destroy(sourceData);
		delete sourceData;
	}
	else
	{
		status = GenerateResource(treeData);
	}

	delete treeData;

	SHUTDOWN_PARSER;

	return status;

#else
	Quitf("External metadata (and external metadata resourcing) only works in DEV builds");
	return 1;
#endif
}