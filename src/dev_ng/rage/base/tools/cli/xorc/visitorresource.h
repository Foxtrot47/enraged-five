// 
// xorc/visitorresource.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef XORC_VISITORRESOURCE_H
#define XORC_VISITORRESOURCE_H

#include "data/struct.h"
#include "parser/visitorutils.h"

namespace rage
{
	class xorcVisitorResource : public parNonGenericVisitor
	{
		virtual void VisitStructure(void* dataPtr, parStructure& metadata);

		virtual void BoolMember		(bool& d,	parMemberSimple& ) {datSwapper(d);}
		virtual void CharMember		(s8& d,		parMemberSimple& ) {datSwapper(d);}
		virtual void UCharMember	(u8& d,		parMemberSimple& ) {datSwapper(d);}
		virtual void ShortMember	(s16& d,		parMemberSimple& ) {datSwapper(d);}
		virtual void UShortMember	(u16& d,		parMemberSimple& ) {datSwapper(d);}
		virtual void IntMember		(s32& d,		parMemberSimple& ) {datSwapper(d);}
		virtual void UIntMember		(u32& d,		parMemberSimple& ) {datSwapper(d);}
		virtual void FloatMember	(float& d,	parMemberSimple& ) {datSwapper(d);}

		virtual void Vector2Member	(Vector2& d,		parMemberVector& );
		virtual void Vector3Member	(Vector3& d,		parMemberVector& );
		virtual void Vector4Member	(Vector4& d,	parMemberVector& );

		virtual void Matrix34Member	(Matrix34& d,		parMemberMatrix& );
		virtual void Matrix44Member	(Matrix44& d,	parMemberMatrix& );

		virtual void EnumMember(int& , parMemberEnum& );

		virtual void PointerStringMember(char*& , parMemberString& );
		virtual void ConstStringMember		(ConstString& , parMemberString& );
		virtual void AtStringMember			(atString& ,	parMemberString& );

		virtual void WidePointerStringMember(char16*& , parMemberString& );
		virtual void AtWideStringMember		(atWideString& , parMemberString& );

		virtual void AtHashStringMember(atHashString&, parMemberString& );

		virtual void ExternalPointerMember	(void*& ,		parMemberStruct& );

		virtual void EndArrayMember(void* ptrToMember, void* arrayContents, u32 numElements, parMemberArray& metadata);

		virtual void EndPointerMember(void*& ptrRef, parMemberStruct& metadata);
	};
#if __DEV && !__64BIT
	class xorcVisitorPlaceRsc : public parNonGenericVisitor
	{
	public:
		xorcVisitorPlaceRsc(datResource& rsc) : m_Rsc(&rsc) {
			atFixedBitSet32 newTypeMask;
			newTypeMask.Set(parMemberType::TYPE_ARRAY);
			newTypeMask.Set(parMemberType::TYPE_STRUCT);
			newTypeMask.Set(parMemberType::TYPE_STRING);

			SetMemberTypeMask(newTypeMask);
		}

		virtual void VisitStructure(void* dataPtr, parStructure& metadata);

		virtual bool BeginPointerMember(void*& ptrRef, parMemberStruct& metadata);
		virtual bool BeginArrayMember(void* , void* , u32 , parMemberArray&);

		virtual void PointerStringMember(char*& , parMemberString& );
		virtual void ConstStringMember		(ConstString& , parMemberString& );
		virtual void AtStringMember			(atString& ,	parMemberString& );

		virtual void WidePointerStringMember(char16*& , parMemberString& );
		virtual void AtWideStringMember		(atWideString& , parMemberString& );

		datResource* m_Rsc;
	};
#endif
}

#endif // XORC_VISITORRESOURCE_H