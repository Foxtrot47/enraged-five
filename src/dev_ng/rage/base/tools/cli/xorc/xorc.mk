top: bottom

include ..\..\..\..\rage\build\Makefile.template

.PHONY: FORCE
TARGET = xorc
ELF = $(INTDIR)/$(TARGET)_$(INTDIR).ppu.elf
SELF = $(INTDIR)/$(TARGET)_$(INTDIR).ppu.self

bottom: $(SELF)

$(SELF): $(ELF)
	make_fself $(ELF) $(SELF)

LIBS += ..\..\..\..\..\rage\base\src\vcproj\RageGraphics\$(INTDIR)\RageGraphics.lib

..\..\..\..\..\rage\base\src\vcproj\RageGraphics\$(INTDIR)\RageGraphics.lib: FORCE
	$(MAKE) -C ..\..\..\..\..\rage\base\src\vcproj\RageGraphics -f RageGraphics.mk

LIBS += ..\..\..\..\..\rage\base\src\vcproj\RageCore\$(INTDIR)\RageCore.lib

..\..\..\..\..\rage\base\src\vcproj\RageCore\$(INTDIR)\RageCore.lib: FORCE
	$(MAKE) -C ..\..\..\..\..\rage\base\src\vcproj\RageCore -f RageCore.mk

LIBS += ..\..\..\..\..\rage\stlport\STLport-5.0RC5\src/$(INTDIR)/stlport.lib

..\..\..\..\..\rage\stlport\STLport-5.0RC5\src/$(INTDIR)/stlport.lib: FORCE
	$(MAKE) -C ..\..\..\..\..\rage\stlport\STLport-5.0RC5\src -f stlport.mk

LIBS += ..\..\..\..\..\rage/base/src\cranimation/$(INTDIR)/cranimation.lib

..\..\..\..\..\rage/base/src\cranimation/$(INTDIR)/cranimation.lib: FORCE
	$(MAKE) -C ..\..\..\..\..\rage/base/src\cranimation -f cranimation.mk

LIBS += ..\offlinersc/$(INTDIR)/offlinersc.lib

..\offlinersc/$(INTDIR)/offlinersc.lib: FORCE
	$(MAKE) -C ..\offlinersc -f offlinersc.mk

INCLUDES = -I ..\..\..\..\rage\stlport\STLport-5.0RC5\stlport -I ..\..\..\..\..\rage/base/src -I ..\..\..\..\..\rage\stlport\STLport-5.0RC5\src -I ..\..\..\..\..\rage\base\tools\cli -I ..\..\..\..\..\rage\base\tools\dcc\libs -I ..\..\..\..\..\rage\base\tools\libs

$(INTDIR)\xorc.obj: xorc.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(ELF): $(LIBS) $(INTDIR)\xorc.obj
	ppu-lv2-gcc -o $(ELF) $(INTDIR)\xorc.obj -Wl,--start-group $(LIBS) -Wl,--end-group $(LLIBS)
