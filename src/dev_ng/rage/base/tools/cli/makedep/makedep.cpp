#define _CRT_SECURE_NO_DEPRECATE
#include <string.h>
#include <stdio.h>


char includePaths[16][128];
int includeCount = 1;
FILE *depfile;

int seenCount;
char *seen[4096];	// lame

void recurse(const char *name,const char *parentname)
{
	FILE *f = NULL;
	char fullname[128];
	/// printf("recurse(%s,%s)\n",name,parentname);
	if (parentname && !strchr(name,'/') && strrchr(parentname,'/')) {
		strcpy(fullname,parentname);
		strcpy(strrchr(fullname,'/')+1,name);
		/// printf("checking for '%s'...\n",fullname);
		f = fopen(fullname, "r");
	}
	if (!f) {
		for (int i=0; i<includeCount; i++) {
			strcpy(fullname,includePaths[i]);
			strcat(fullname,name);
			/// printf("checking for '%s'...\n",fullname);
			f = fopen(fullname, "r");
			if (f)
				break;
		}
	}
	if (!f) {
		fprintf(stderr,"makedep - '%s' (from %s) not found\n",name,parentname);
		return;
	}

	for (int i=0; i<seenCount; i++) {
		if (!strcmp(seen[i],fullname)) {
			// printf("duplicate %s\n",fullname);
			fclose(f);
			return;
		}
	}

	fprintf(depfile,"\t%s\t\\\n",fullname);
	seen[seenCount++] = _strdup(fullname);

	char linebuf[512];
	while (fgets(linebuf, sizeof(linebuf), f)) {
		char *lp = linebuf;
		while (*lp==32 || *lp==9)
			++lp;
		if (*lp == '#') {
			++lp;
			while (*lp==32 || *lp==9)
				++lp;
			if (!strncmp(lp,"include",7)) {
				lp += 7;
				while (*lp==32 || *lp==9)
					++lp;
				if (*lp == '"') {
					char *q2 = strchr(++lp,'"');
					if (q2) {
						*q2 = 0;
						while (strchr(lp,'\\'))
							*strchr(lp,'\\') = '/';
						recurse(lp,fullname);
					}
				}
			}
		}
	}
	fclose(f);
}

int makedep(const char *outputName,const char *sourceName,const char *depName) 
{
	depfile = fopen(depName, "w");
	if (depfile) {
		fprintf(depfile, "%s: \\\n",outputName);
		recurse(sourceName,NULL);
		fprintf(depfile,"\n");

		for (int i=0; i<seenCount; i++)
			fprintf(depfile,"%s:\n\n",seen[i]);

		fclose(depfile);
		return 0;
	}
	else {
		printf("makedep: unable to create output depenency file '%s'\n",depName);
		return 1;
	}
}


int main(int argc,char **argv) 
{
	if (argc < 4) {
		printf("usage: makedep [-I path ...] outputName inputName depName\n");
		printf("Determines all dependencies of inputName and writes it to a .d at depName\n");
		printf("The target dependency in the .d file is specified by outputName.\n");
		return 1;
	}
	while (argc > 2 && !strcmp(argv[1],"-I")) {
		strcpy(includePaths[includeCount],argv[2]);
		while (strchr(includePaths[includeCount],'\\'))
			*strchr(includePaths[includeCount],'\\') = '/';
		strcat(includePaths[includeCount],"/");
		includeCount++;
		argc -= 2;
		argv += 2;
	}

	return makedep(argv[1],argv[2],argv[3]);
}
