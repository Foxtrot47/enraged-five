// 
// psoprinter.cpp
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#include "psoprinter.h"

#if !__NO_OUTPUT

#include "math/float16.h"
#include "parser/psofaketypes.h"
#include "parser/psofile.h"
#include "parser/memberarraydata.h"
#include "parser/memberenumdata.h"
#include "parser/membermapdata.h"
#include "parser/memberstructdata.h"
#include "system/nelem.h"

#include "vector/matrix34.h"
#include "vector/matrix44.h"
#include "vector/vector2.h"
#include "vector/vector3.h"
#include "vector/vector4.h"
#include "vectormath/mat33v.h"
#include "vectormath/mat34v.h"
#include "vectormath/mat44v.h"
#include "vectormath/scalarv.h"
#include "vectormath/vec2v.h"
#include "vectormath/vec3v.h"
#include "vectormath/vec4v.h"
#include "vectormath/vecboolv.h"

#include <algorithm>

using namespace rage;

extern parEnumData parser_rage__parMember__Type_Data;

char g_indents[] = "                                                                                                                                                                                                                                                                                                                                                                    ";
int g_indentLevel = 0;

void Indent()
{
	Printf(g_indents + (sizeof(g_indents) - 1 - 2*g_indentLevel));
}

#define InDisplayf Indent(); Displayf

#if __DEV || __RESOURCECOMPILER || __TOOL
const char* GetString(const u32 hash)
{
	return atLiteralHashString::TryGetString(hash);
}
const char* GetString(atLiteralHashValue hash)
{
	return GetString(hash.GetHash());
}
#else
const char* GetString(const u32)
{
	return NULL;
}
const char* GetString(atLiteralHashValue)
{
	return NULL;
}
#endif

void PrintStructureMembers(psoStruct& s);

EXTERN_PARSER_ENUM(rage__parMember__Type);
EXTERN_PARSER_ENUM(rage__parMemberArray__Subtype);
EXTERN_PARSER_ENUM(rage__parMemberEnum__Subtype);
EXTERN_PARSER_ENUM(rage__parMemberMap__Subtype);
EXTERN_PARSER_ENUM(rage__parMemberStruct__Subtype);
EXTERN_PARSER_ENUM(rage__parMemberString__Subtype);

void GetNameString(const u32 hash, char* outBuf, int bufSize)
{
	const char* name = GetString(hash);
	if (name)
	{
		safecpy(outBuf, name, bufSize);
	}
	else
	{
		formatf(outBuf, bufSize, "0x%08x", hash);
	}
}	

void PrintPsoSchemaCatalog(rage::psoFile& /*file*/, rage::psoSchemaCatalog cat)
{
	Indent(); Displayf("PSCH Schema Section:");

	g_indentLevel++;


	int schemaIdx = 0;
	for(psoSchemaCatalog::StructureSchemaIterator iter = cat.BeginSchemas(); iter != cat.EndSchemas(); ++iter)
	{
		const psoStructureSchema& schStructure = *iter;
		char structName[128];
		GetNameString(schStructure.GetNameHash().GetHash(), structName, NELEM(structName));

		InDisplayf("%4d: /-----------------------------------------------------------------------------------------------", schemaIdx);
		schemaIdx++;
		InDisplayf("      | %s           Size: %d (0x%x), Flags: %02x, Vers: %d, Sig: 0x%08x", structName, 
			schStructure.GetSize(), schStructure.GetSize(), schStructure.GetFlags().GetRawBlock(0), schStructure.GetVersion().GetMajor(), schStructure.GetSignature());
		InDisplayf("      |-----------------------------------------------------------------------------------------------");
		InDisplayf("      | #  |             NAME              |    TYPE            |      OFFSET    |      DATA           ");

		for(int memIdx = 0; memIdx < schStructure.GetNumMembers(); memIdx++)
		{
			psoMemberSchema mem = schStructure.GetMemberByIndex(memIdx);
			psoType memType = mem.GetType();

			atLiteralHashValue hash = mem.GetNameHash();
			char memName[128];
			char offsetStr[32];
			if (!psoConstants::IsReserved(hash))
			{
				GetNameString(hash.GetHash(), memName, NELEM(memName));
				formatf(offsetStr, "%6d %08x", mem.GetOffset(), mem.GetOffset());
			}
			else
			{
				safecpy(memName, "");
				offsetStr[0] = '\0';
			}

			const char* type = memType.GetName();

			char extraData[128];
			extraData[0] = '\0';

			if (memType.IsEnum())
			{
				char refName[128];
				GetNameString(mem.GetReferentHash().GetHash(), refName, NELEM(refName));
				formatf(extraData, " Values: %s", refName);
			}
			else if (memType.IsBitset())
			{
				char refName[128];
				GetNameString(mem.GetBitsetEnumHash().GetHash(), refName, NELEM(refName));
				formatf(extraData, " Count: %d, Enum: %s", mem.GetBitsetCount(), refName);
			}
			else if (memType == psoType::TYPE_STRUCT)
			{
				char refName[128];
				GetNameString(mem.GetReferentHash().GetHash(), refName, NELEM(refName));
				formatf(extraData, " Struct: %s", refName);
			}
			else if (memType == psoType::TYPE_STRING_MEMBER || memType == psoType::TYPE_WIDE_STRING_MEMBER)
			{
				// What about string pointer types here? Do they use the count field?
				formatf(extraData, " Count: %d", mem.GetStringCount());
			}
			else if (memType.IsArray())
			{
				u8 alignPwr = mem.GetArrayAlignmentPower();
				u32 align = alignPwr ? 1 << alignPwr : 0;

				if (memType == psoType::TYPE_ARRAY_MEMBER ||
					memType == psoType::TYPE_ARRAY_POINTER32 ||
					memType == psoType::TYPE_ARRAY_POINTER64)
				{
					formatf(extraData, " Elt#: %d, Count: %d, Align %d", mem.GetArrayElementSchemaIndex(), mem.GetFixedArrayCount(), align);
				}
				else
				{
					formatf(extraData, " Elt#: %d, Align %d", mem.GetArrayElementSchemaIndex(), align);
				}
			}
			else if (memType.IsMap())
			{
				formatf(extraData, " Key#: %d, Value#: %d", mem.GetKeySchemaIndex(), mem.GetValueSchemaIndex());
			}

			InDisplayf("      |%3d |%30s |%19s |%15s |%s",memIdx, memName, type, offsetStr, extraData);
		}
		InDisplayf("      \\-----------------------------------------------------------------------------------------------", schemaIdx);
		InDisplayf("");

	}


	schemaIdx = 0;
	for(psoSchemaCatalog::EnumSchemaIterator iter = cat.BeginEnums(); iter != cat.EndEnums(); ++iter)
	{
		const psoEnumSchema& schEnum = *iter;
		char structName[128];
		GetNameString(schEnum.GetNameHash().GetHash(), structName, NELEM(structName));

		InDisplayf("%4d:  /-------------------------------------------------------", schemaIdx);
		schemaIdx++;
		InDisplayf("      | Enum %s    Sig: 0x%08x", structName, schEnum.GetSignature());
		InDisplayf("      |-------------------------------------------------------");
		InDisplayf("      | # |           NAME           |   HASH   |    VALUE   |");

		for(int enumIdx = 0; enumIdx < schEnum.GetNumEnums(); enumIdx++)
		{
			const char* nameStr = GetString(schEnum.GetMemberNameHash(enumIdx).GetHash());
			if (!nameStr) 
			{
				nameStr = "";
			}
			InDisplayf("      |%3d|%26s| %08x |%12d|",enumIdx, nameStr, schEnum.GetMemberNameHash(enumIdx).GetHash(), schEnum.GetMemberValue(enumIdx));
		}
		InDisplayf("       \\-------------------------------------------------------", schemaIdx);
		InDisplayf("");
	}
	g_indentLevel--;
}

void PrintPsoStructureMap(psoFile& file)
{
	InDisplayf("Structure Map:");
	g_indentLevel++;

	psoResourceData* resource = file.internal_GetResourceData();
	InDisplayf("Root Object Id - StructArray %d, Offset %d", 
		resource->m_RootObjectId.GetTableIndex(),
		resource->m_RootObjectId.GetArrayOffset());

	char* storage = NULL;
	storage = file.internal_GetStorage();

	InDisplayf("     /-------------------------------------------------------------------------------------------------");
	InDisplayf("     | Structure Map     Root object @(%d,0x%x)", resource->m_RootObjectId.GetTableIndex(), resource->m_RootObjectId.GetArrayOffset(), resource->m_RootObjectId.GetArrayOffset());
	InDisplayf("     |-------------------------------------------------------------------------------------------------");
	InDisplayf("     |  IDX  |   HASH   |               NAME                 |      ADDRESS     |  OFFSET  |    SIZE    ");

	for(u32 saIndex = 0; saIndex < resource->m_NumStructArrayEntries; saIndex++)
	{
		psoRscStructArrayTableData& str = resource->m_StructArrayTable[saIndex];

		u32 offset = storage ? (u32)(str.m_Data.GetPtr() - storage) : 0;

		if (psoConstants::IsParMemberType(str.m_NameHash))
		{
			psoType type((u8)str.m_NameHash.GetHash());


			InDisplayf("     | %5d | POD 0x%02x | %34s | %016p | %08x | %08x",
				saIndex, 
				str.m_NameHash.GetHash(), type.GetName(),
				str.m_Data.GetPtr(),
				offset, 
				str.m_Size);
		}
		else if (psoConstants::IsAnonymous(str.m_NameHash))
		{
			InDisplayf("     | %5d | %08x |                           <<anon>> | %016p | %08x | %08x",
				saIndex, 
				str.m_NameHash.GetHash(), 
				str.m_Data.GetPtr(),
				offset,
				str.m_Size);
		}
		else
		{
			InDisplayf("     | %5d | %08x | %34s | %016p | %08x | %08x",
				saIndex, 
				str.m_NameHash.GetHash(), atLiteralHashString::TryGetString(str.m_NameHash.GetHash()),
				str.m_Data.GetPtr(),
				offset,
				str.m_Size);
		}
	}

	InDisplayf("     \\-------------------------------------------------------------------------------------------------");

	g_indentLevel--;

}

void PrintMember(psoStruct& str, psoMember& m, bool firstIndent = true)
{
	if (firstIndent)
	{
		Indent();
	}
	Printf("0x%08x (%s) : %s = ", 
		m.GetSchema().GetNameHash().GetHash(), 
		GetString(m.GetSchema().GetNameHash().GetHash()),
		m.GetType().GetName()
		);

	char buf[RAGE_MAX_PATH];

	switch(m.GetType().GetEnum())
	{
	case psoType::TYPE_BOOL:	
		Displayf("%s", m.GetDataAs<bool>() ? "true" : "false"); break;
	case psoType::TYPE_BOOLV:	
		Displayf("%s", m.GetDataAs<BoolV>().Getb() ? "true" : "false"); break;
	case psoType::TYPE_VECBOOLV: 
		{
			VecBoolV vb = m.GetDataAs<VecBoolV>();
			Displayf("%s,%s,%s,%s", 
				vb.GetX().Getb() ? "true" : "false", 
				vb.GetY().Getb() ? "true" : "false", 
				vb.GetZ().Getb() ? "true" : "false", 
				vb.GetW().Getb() ? "true" : "false" 
				);
		}
		break;
	case psoType::TYPE_STRUCT:
		{
			psoStruct subStruct = m.GetSubStructure();
			Displayf("");
			g_indentLevel++;
			PrintPsoStructure(subStruct);
			g_indentLevel--;
		}
		break;
	case psoType::TYPE_POINTER32:
	case psoType::TYPE_POINTER64:
		{
			psoStructId id;
			if (m.GetType() == psoType::TYPE_POINTER32)
			{
				id = m.GetDataAs<psoFake32::CharPtr>().m_StructId;
			}
			else
			{
				id = m.GetDataAs<psoFake64::CharPtr>().m_StructId;
			}

			psoStruct subStruct = m.GetSubStructure();
			if (subStruct.IsNull())
			{
				Displayf("NULL");
			}
			else if (subStruct.IsValid())
			{
				Displayf(" @(%d,0x%x)", id.GetTableIndex(), id.GetArrayOffset());
				g_indentLevel++;
				PrintPsoStructure(subStruct);
				g_indentLevel--;
			}
			else
			{
				Displayf("INVALID");
			}
		}
		break;
	case psoType::TYPE_S8:
		Displayf("%d", m.GetDataAs<char>()); break;
	case psoType::TYPE_U8:
		Displayf("%u", m.GetDataAs<u8>()); break;
	case psoType::TYPE_S16:
		Displayf("%d", m.GetDataAs<s16>()); break;
	case psoType::TYPE_U16:
		Displayf("%u", m.GetDataAs<u16>()); break;
	case psoType::TYPE_S32:
		Displayf("%d", m.GetDataAs<int>()); break;
	case psoType::TYPE_U32:
		Displayf("%u", m.GetDataAs<u32>()); break;
	case psoType::TYPE_S64:
		Displayf("%d" I64FMT, m.GetDataAs<s64>()); break;
	case psoType::TYPE_U64:
		Displayf("%d" I64FMT, m.GetDataAs<ptrdiff_t>()); break;
	case psoType::TYPE_FLOAT:
	case psoType::TYPE_SCALARV:
		Displayf("%f", m.GetDataAs<float>()); break;
	case psoType::TYPE_FLOAT16:
		Displayf("%f", m.GetDataAs<Float16>().GetFloat32_FromFloat16()); break;
	case psoType::TYPE_VEC2:
	case psoType::TYPE_VEC2V:
		{
			struct v2f { float x, y; };
			v2f v = m.GetDataAs<v2f>();
			Displayf("%f, %f", v.x, v.y);
		}
		break;
	case psoType::TYPE_VEC3:
	case psoType::TYPE_VEC3V:
		{
			struct v3f { float x, y, z; };
			v3f v = m.GetDataAs<v3f>();
			Displayf("%f, %f, %f", v.x, v.y, v.z);
		}
		break;
	case psoType::TYPE_VEC4V:
		{
			Vec4V v = m.GetDataAs<Vec4V>();
			Displayf("%f, %f, %f, %f", v.GetXf(), v.GetYf(), v.GetZf(), v.GetWf());
		}
		break;
	case psoType::TYPE_MAT33V:
		{
			Mat33V mat = m.GetDataAs<Mat33V>();
			Displayf("[ c0=[%f, %f, %f], c1=[%f, %f, %f], c2=[%f, %f, %f] ]",
				mat.GetCol0().GetXf(), mat.GetCol0().GetYf(), mat.GetCol0().GetZf(),
				mat.GetCol1().GetXf(), mat.GetCol1().GetYf(), mat.GetCol1().GetZf(),
				mat.GetCol2().GetXf(), mat.GetCol2().GetYf(), mat.GetCol2().GetZf()
				);
		}
	case psoType::TYPE_MAT34V:
		{
			Mat34V mat = m.GetDataAs<Mat34V>();
			Displayf("[ c0=[%f, %f, %f], c1=[%f, %f, %f], c2=[%f, %f, %f], c3=[%f, %f, %f] ]",
				mat.GetCol0().GetXf(), mat.GetCol0().GetYf(), mat.GetCol0().GetZf(),
				mat.GetCol1().GetXf(), mat.GetCol1().GetYf(), mat.GetCol1().GetZf(),
				mat.GetCol2().GetXf(), mat.GetCol2().GetYf(), mat.GetCol2().GetZf(),
				mat.GetCol3().GetXf(), mat.GetCol3().GetYf(), mat.GetCol3().GetZf()
				);
		}
	case psoType::TYPE_MAT43:
		{
			float* data = reinterpret_cast<float*>(m.GetRawDataPtr());
			Displayf("[ c0=[%f, %f, %f, %f], c1=[%f, %f, %f, %f], c2=[%f, %f, %f, %f] ]",
				data[0], data[1], data[2], data[3],
				data[4], data[5], data[6], data[7],
				data[8], data[9], data[10], data[11]);
		}
	case psoType::TYPE_MAT44V:
		{
			Mat44V mat = m.GetDataAs<Mat44V>();
			Displayf("[ c0=[%f, %f, %f, %f], c1=[%f, %f, %f, %f], c2=[%f, %f, %f, %f], c3=[%f, %f, %f, %f] ]",
				mat.GetCol0().GetXf(), mat.GetCol0().GetYf(), mat.GetCol0().GetZf(), mat.GetCol0().GetWf(),
				mat.GetCol1().GetXf(), mat.GetCol1().GetYf(), mat.GetCol1().GetZf(), mat.GetCol1().GetWf(),
				mat.GetCol2().GetXf(), mat.GetCol2().GetYf(), mat.GetCol2().GetZf(), mat.GetCol2().GetWf(),
				mat.GetCol3().GetXf(), mat.GetCol3().GetYf(), mat.GetCol3().GetZf(), mat.GetCol3().GetWf()
				);
		}
	case psoType::TYPE_STRING_MEMBER:
		Displayf("\"%s\"", &m.GetDataAs<char>());
		break;
	case psoType::TYPE_STRING_POINTER32:
		{
			psoFake32::CharPtr ptr = m.GetDataAs<psoFake32::CharPtr>();
			Displayf("@(%d,0x%x) \"%s\"", ptr.m_StructId.GetTableIndex(), ptr.m_StructId.GetArrayOffset(), m.GetFile().GetInstanceDataAs<char*>(ptr.m_StructId));
		}
		break;
	case psoType::TYPE_STRING_POINTER64:
		{
			psoFake64::CharPtr ptr = m.GetDataAs<psoFake64::CharPtr>();
			Displayf("@(%d,0x%x) \"%s\"", ptr.m_StructId.GetTableIndex(), ptr.m_StructId.GetArrayOffset(), m.GetFile().GetInstanceDataAs<char*>(ptr.m_StructId));
		}
		break;
	case psoType::TYPE_ATSTRING32:
		{
			psoFake32::AtString ats = m.GetDataAs<psoFake32::AtString>();
			Displayf("@(%d,0x%x) \"%s\"", ats.m_Data.m_StructId.GetTableIndex(), ats.m_Data.m_StructId.GetArrayOffset(), m.GetFile().GetInstanceDataAs<char*>(ats.m_Data.m_StructId));
		}
		break;
	case psoType::TYPE_ATSTRING64:
		{
			psoFake64::AtString ats = m.GetDataAs<psoFake64::AtString>();
			Displayf("@(%d,0x%x) \"%s\"", ats.m_Data.m_StructId.GetTableIndex(), ats.m_Data.m_StructId.GetArrayOffset(), m.GetFile().GetInstanceDataAs<char*>(ats.m_Data.m_StructId));
		}
		break;
	case psoType::TYPE_WIDE_STRING_MEMBER:
		{
			WideToUtf8(buf, &m.GetDataAs<char16>(), NELEM(buf));
			Displayf("\"%s\"", buf);
		}
		break;
	case psoType::TYPE_WIDE_STRING_POINTER32:
		{
			psoFake32::CharPtr ptr = m.GetDataAs<psoFake32::CharPtr>();
			WideToUtf8(buf, m.GetFile().GetInstanceDataAs<char16*>(ptr.m_StructId), NELEM(buf));
			Displayf("@(%d,0x%x) \"%s\"", ptr.m_StructId.GetTableIndex(), ptr.m_StructId.GetArrayOffset(), buf);
		}
		break;
	case psoType::TYPE_WIDE_STRING_POINTER64:
		{
			psoFake64::CharPtr ptr = m.GetDataAs<psoFake64::CharPtr>();
			WideToUtf8(buf, m.GetFile().GetInstanceDataAs<char16*>(ptr.m_StructId), NELEM(buf));
			Displayf("@(%d,0x%x) \"%s\"", ptr.m_StructId.GetTableIndex(), ptr.m_StructId.GetArrayOffset(), buf);
		}
		break;
	case psoType::TYPE_ATWIDESTRING32:
		{
			psoFake32::AtString ats = m.GetDataAs<psoFake32::AtString>();
			WideToUtf8(buf, m.GetFile().GetInstanceDataAs<char16*>(ats.m_Data.m_StructId), NELEM(buf));
			Displayf("@(%d,0x%x) \"%s\"", ats.m_Data.m_StructId.GetTableIndex(), ats.m_Data.m_StructId.GetArrayOffset(), buf);
		}
		break;
	case psoType::TYPE_ATWIDESTRING64:
		{
			psoFake64::AtString ats = m.GetDataAs<psoFake64::AtString>();
			WideToUtf8(buf, m.GetFile().GetInstanceDataAs<char16*>(ats.m_Data.m_StructId), NELEM(buf));
			Displayf("@(%d,0x%x) \"%s\"", ats.m_Data.m_StructId.GetTableIndex(), ats.m_Data.m_StructId.GetArrayOffset(), buf);
		}
		break;
	case psoType::TYPE_STRINGHASH:
		{
			u32 hash = m.GetDataAs<u32>();
			const char* stringData = atFinalHashString::TryGetString(hash);
			if (!stringData)
			{
				stringData = atNonFinalHashString::TryGetString(hash);
			}
			if (stringData)
			{
				Displayf("0x%08x \"%s\"", hash, stringData);
			}
			else
			{
				Displayf("0x%08x", hash);
			}
		}
		break;
	case psoType::TYPE_PARTIALSTRINGHASH:
		Displayf("0x%08x", m.GetDataAs<u32>());
		break;
	case psoType::TYPE_LITERALSTRINGHASH:
		{
			u32 hash = m.GetDataAs<u32>();
			const char* stringData = atLiteralHashString::TryGetString(hash);
			if (stringData)
			{
				Displayf("0x%08x \"%s\"", hash, stringData);
			}
			else
			{
				Displayf("0x%08x", hash);
			}
		}
		break;
	case psoType::TYPE_ARRAY_MEMBER:
	case psoType::TYPE_ATARRAY32:
	case psoType::TYPE_ATARRAY64:
	case psoType::TYPE_ATFIXEDARRAY:
	case psoType::TYPE_ATARRAY32_32BITIDX:
	case psoType::TYPE_ATARRAY64_32BITIDX:
	case psoType::TYPE_ARRAY_POINTER32:
	case psoType::TYPE_ARRAY_POINTER32_WITH_COUNT:
	case psoType::TYPE_ARRAY_POINTER64:
	case psoType::TYPE_ARRAY_POINTER64_WITH_COUNT:
		{
			psoMemberArrayInterface arr(str, m);
			// TODO: special output for arrays of PODs
			size_t count = arr.GetArrayCount();

			Displayf("[ %d ]", count);

			g_indentLevel++;
			for(size_t i = 0; i < count; i++)
			{
				Indent();
				Printf("%d: ", i);
				psoMember arrElt = arr.GetElement(i);
				PrintMember(str, arrElt, false);
			}
			g_indentLevel--;
		}
		break;
	case psoType::TYPE_ENUM8:
	case psoType::TYPE_ENUM16:
	case psoType::TYPE_ENUM32:
		{
			int enumVal = 0;
			switch(m.GetType().GetEnum())
			{
			case psoType::TYPE_ENUM32:
				enumVal = m.GetDataAs<int>(); break;
			case psoType::TYPE_ENUM16:
				enumVal = m.GetDataAs<s16>(); break;
			case psoType::TYPE_ENUM8:
				enumVal = m.GetDataAs<s8>(); break;
			default:
				break;
			}

			psoEnumSchema schemaEnum = m.GetReferentEnum();
			atLiteralHashValue hashName;
			if (schemaEnum.NameFromValue(enumVal, hashName))
			{
				Displayf("%d ( 0x%08x (%s) )", enumVal, hashName.GetHash(), GetString(hashName));
			}
			else
			{
				Displayf("%d", enumVal);
			}
		}
		break;
	case psoType::TYPE_BITSET8:
	case psoType::TYPE_BITSET16:
	case psoType::TYPE_BITSET32:
	case psoType::TYPE_ATBITSET32:
	case psoType::TYPE_ATBITSET64:
		{
			psoMemberBitsetInterface memBits(str, m);
			for(size_t i = 0; i < memBits.GetBitCount(); i++)
			{
				if (memBits.GetBit(i))
				{
					atLiteralHashValue hash = memBits.GetBitName(i);
					const char* name = atLiteralHashString::TryGetString(hash.GetHash());
					if (name)
					{
						Printf("%s ", name);
					}
					else
					{
						Printf("#%d ", i);
					}
				}
			}
		}
		break;
	case psoType::TYPE_ATBINARYMAP32:
	case psoType::TYPE_ATBINARYMAP64:
		{
			psoMemberSchema keySchema = str.GetSchema().GetMemberByIndex(m.GetSchema().GetKeySchemaIndex());
			psoMemberSchema valueSchema = str.GetSchema().GetMemberByIndex(m.GetSchema().GetValueSchemaIndex());

			Displayf("%s -> %s", keySchema.GetType().GetName(), valueSchema.GetType().GetName());

			psoMemberMapInterface mapMember(str, m);

			size_t count = mapMember.GetMapCount();

			g_indentLevel++;
			for(size_t i = 0; i < count; i++)
			{
				Indent();
				Displayf("%d: ", i);

				g_indentLevel++;
				psoStruct keyValueStruct = mapMember.GetKeyValueStruct(i);
				PrintStructureMembers(keyValueStruct);
				g_indentLevel--;
			}
			g_indentLevel--;
		}
		break;
	case psoType::TYPE_STRUCTID:
		Displayf("StructID(?!) (%d,0x%x)", m.GetDataAs<psoStructId>().GetTableIndex(), m.GetDataAs<psoStructId>().GetArrayOffset());
		break;
	case psoType::TYPE_INVALID:
		Displayf("INVALID(?!)");
		break;
	default:
		Displayf("UNKNOWN TYPE(?!) %d", m.GetType().GetRaw());
		break;
	}
}

void PrintStructureMembers(psoStruct& s)
{
	for(int i = 0; i < s.GetSchema().GetNumMembers(); i++)
	{
		psoMember member = s.GetMemberByIndex(i);
		if (member.IsValid() && !psoConstants::IsReserved(member.GetSchema().GetNameHash())) // has to be a named member
		{
			PrintMember(s, member);
		}
	}
}

void PrintPsoStructure(psoStruct& s)
{
	InDisplayf("Instance of 0x%08x (%s) at address 0x%16x", s.GetSchema().GetNameHash().GetHash(), GetString(s.GetSchema().GetNameHash()), s.GetInstanceData());
	InDisplayf("{");

	g_indentLevel++;
	PrintStructureMembers(s);
	g_indentLevel--;

	InDisplayf("}");
}

void PrintPsoStructureTree(psoFile& f)
{
	psoStruct root = f.GetRootInstance();
	if (!root.IsNull())
	{
		PrintPsoStructure(root);
	}
}

void PrintPsoDescription(psoFile& f)
{
	InDisplayf("PSO File:");
	
	g_indentLevel++;

	PrintPsoSchemaCatalog(f, f.GetSchemaCatalog());
	PrintPsoStructureMap(f);
	PrintPsoStructureTree(f);

	g_indentLevel--;
}

#if __DEV || __TOOL
const char* GetHashName(atLiteralHashValue hash, char* buf)
{
	const char* name = atLiteralHashString::TryGetString(hash.GetHash());
	if (!name)
	{
		formatf(buf, 32, "hash:0x%08x", hash.GetHash());
		name = buf;
	}
	return name;
}
#else
const char* GetHashName(atLiteralHashValue , char* )
{
	return NULL;
}
#endif


#if 0 // psoTODO: Bring this back

void PrintPsoStructAsXml(psoStruct& s, const char* nameStr, const char* typeStr, const char* keyStr);

template<typename _Type>
void PrintPodArrayToXml( const char* name, const char* arrayType, const char* formatStr, int count, psoMemberArrayInterface &arr ) 
{
	InDisplayf("<%s content=\"%s\">", name, arrayType);
	g_indentLevel++;
	for(int i = 0; i < count; i++)
	{
		psoMember arrElt = arr.GetElement(i);
		InDisplayf(formatStr, arrElt.GetDataAs<_Type>());
	}
	g_indentLevel--;
	InDisplayf("</%s>", name);
}

void PrintVecArrayToXml( const char* name, const char* arrayType, int eltsPerVector, int count, psoMemberArrayInterface &arr ) 
{
	InDisplayf("<%s content=\"%s\">", name, arrayType);
	g_indentLevel++;
	for(int i = 0; i < count; i++)
	{
		psoMember arrElt = arr.GetElement(i);
		float* floats = &arrElt.GetDataAs<float>();
		Indent();
		for(int i = 0; i < eltsPerVector; i++)
		{
			Printf("%f\t", floats[i]);
		}
		Displayf("");
	}
	g_indentLevel--;
	InDisplayf("</%s>", name);
}

void PrintPsoMemberAsXml(psoStruct& str, psoMember& m, const char* overrideName = NULL, psoMember* keyMember = NULL)
{
	using namespace parMemberType;

	char namebuf[32], typebuf[32];
	char keyStr[64];

	keyStr[0] = '\0';
	if (keyMember)
	{
		switch (keyMember->GetType())
		{
		case TYPE_CHAR:		formatf(keyStr, " key=\"%d\"", keyMember->GetDataAs<char>()); break;
		case TYPE_UCHAR:	formatf(keyStr, " key=\"%u\"", keyMember->GetDataAs<u8>()); break;
		case TYPE_SHORT:	formatf(keyStr, " key=\"%d\"", keyMember->GetDataAs<s16>()); break;
		case TYPE_USHORT:	formatf(keyStr, " key=\"%u\"", keyMember->GetDataAs<u16>()); break;
		case TYPE_INT:		formatf(keyStr, " key=\"%d\"", keyMember->GetDataAs<s32>()); break;
		case TYPE_UINT:		formatf(keyStr, " key=\"%u\"", keyMember->GetDataAs<u32>()); break;
		case TYPE_STRING:
			{
				switch((parMemberStringSubType::Enum)keyMember->GetSubType())
				{
				case parMemberStringSubType::SUBTYPE_ATNONFINALHASHSTRING:
					formatf(keyStr, " key=\"%s\"", keyMember->GetDataAs<atHashString>().GetCStr());
					break;
				default:
					break;
				}
			}
			break;
		default:
			break;
		}
	}

	const char* name = overrideName ? overrideName : GetHashName(m.GetSchema().GetNameHash(), namebuf);

	switch(m.GetType())
	{
	case TYPE_BOOL:
		InDisplayf("<%s%s value=\"%s\" />", name, keyStr, m.GetDataAs<bool>() ? "true" : "false"); break;
	case TYPE_BOOLV:
		InDisplayf("<%s%s value=\"%s\" />", name, keyStr, m.GetDataAs<BoolV>().Getb() ? "true" : "false"); break;
	case TYPE_CHAR:
		InDisplayf("<%s%s value=\"%d\" />", name, keyStr, m.GetDataAs<char>()); break;
	case TYPE_UCHAR:
		InDisplayf("<%s%s value=\"%u\" />", name, keyStr, m.GetDataAs<u8>()); break;
	case TYPE_SHORT:
		InDisplayf("<%s%s value=\"%d\" />", name, keyStr, m.GetDataAs<s16>()); break;
	case TYPE_USHORT:
		InDisplayf("<%s%s value=\"%u\" />", name, keyStr, m.GetDataAs<u16>()); break;
	case TYPE_INT:
		InDisplayf("<%s%s value=\"%d\" />", name, keyStr, m.GetDataAs<int>()); break;
	case TYPE_UINT:
		InDisplayf("<%s%s value=\"%u\" />", name, keyStr, m.GetDataAs<u32>()); break;
	case TYPE_PTRDIFFT:
		InDisplayf("<%s%s value=\"%d\" />", name, keyStr, m.GetDataAs<ptrdiff_t>()); break;
	case TYPE_SIZET:
		InDisplayf("<%s%s value=\"%u\" />", name, keyStr, m.GetDataAs<size_t>()); break;
	case TYPE_FLOAT:
	case TYPE_SCALARV:
		InDisplayf("<%s%s value=\"%f\" />", name, keyStr, m.GetDataAs<float>()); break;
	case TYPE_FLOAT16:
		InDisplayf("<%s%s value=\"%f\" />", name, keyStr, m.GetDataAs<Float16>().GetFloat32_FromFloat16()); break;
	case TYPE_VEC2V:
	case TYPE_VECTOR2:
		{
			Vector2 v = m.GetDataAs<Vector2>();
			InDisplayf("<%s%s x=\"%f\" y=\"%f\" />", name, keyStr, v.x, v.y);
		}
		break;
	case TYPE_VEC3V:
	case TYPE_VECTOR3:
		{
			Vector3 v = m.GetDataAs<Vector3>();
			InDisplayf("<%s%s x=\"%f\" y=\"%f\" z=\"%f\" />", name, keyStr, v.x, v.y, v.z);
		}
		break;
	case TYPE_VEC4V:
	case TYPE_VECTOR4:
		{
			Vector4 v = m.GetDataAs<Vector4>();
			InDisplayf("<%s%s x=\"%f\" y=\"%f\" z=\"%f\" w=\"%f\" />", name, keyStr, v.x, v.y, v.z, v.w);
		}
		break;
	case TYPE_STRUCT:
		{
			psoStruct subStruct = m.GetSubStructure();
			if (subStruct.IsNull())
			{
				InDisplayf("<%s%s type=\"NULL\"/>", name, keyStr);
			}
			else if (subStruct.IsValid())
			{
				const char* typestr = NULL;
				if ((parMemberStructSubType::Enum)m.GetSubType() == parMemberStructSubType::SUBTYPE_POINTER)
				{
					typestr = GetHashName(subStruct.GetSchema().GetNameHash(), typebuf);
				}
				PrintPsoStructAsXml(subStruct, name, typestr, keyStr);
			}
			else
			{
				InDisplayf("<!-- %s INVALID -->", name);
			}
		}
		break;
	case TYPE_STRING:
		{
			const char* strData = NULL;
			char hashBuf[32];

			switch((parMemberStringSubType::Enum)m.GetSubType())
			{
			case parMemberStringSubType::SUBTYPE_MEMBER:
				strData = &m.GetDataAs<char>();
				break;
			case parMemberStringSubType::SUBTYPE_CONST_STRING:
			case parMemberStringSubType::SUBTYPE_POINTER:
				{
					psoStructId id = m.GetDataAs<psoStructId>();
					strData = m.GetFile().GetInstanceDataAs<char*>(id);
				}
				break;
			case parMemberStringSubType::SUBTYPE_ATNONFINALHASHSTRING:
				{
					atHashString& s = m.GetDataAs<atHashString>();
					strData = s.GetCStr();
					if (!strData && s.GetHash() != 0)
					{
						formatf(hashBuf, "hash:0x%x", s.GetHash());
					}
				}
				break;
			case parMemberStringSubType::SUBTYPE_ATHASHVALUE:
				{
					atHashValue& s = m.GetDataAs<atHashValue>();
					if (s.GetHash() != 0)
					{
						formatf(hashBuf, "hash:0x%x", s.GetHash());
					}
				}
				break;
			case parMemberStringSubType::SUBTYPE_ATSTRING:
				{
					psoFakeAtString& fakeString = m.GetDataAs<psoFakeAtString>();
					psoStructId id = fakeString.m_Data.m_StructId;
					strData = m.GetFile().GetInstanceDataAs<char*>(id);
				}
				break;
			default:
				InDisplayf("<!-- %s STRING TYPE NOT HANDLED -->", name);
			}
			
			if (strData && strData[0])
			{
				InDisplayf("<%s%s>%s</%s>", name, keyStr, strData, name);
			}
			else
			{
				InDisplayf("<%s />", name);
			}
		}
		break;
	case TYPE_ARRAY:
		{
			psoMemberArrayInterface arr(str, m);
			int count = arr.GetArrayCount();

			if (count == 0)
			{
				InDisplayf("<%s%s />", name, keyStr);
			}
			else
			{
				switch(arr.GetArrayElementSchema().GetType())
				{
				case parMemberType::TYPE_CHAR:
					PrintPodArrayToXml<s8>(name, "char_array", "%d ", count, arr);
					break;
				case parMemberType::TYPE_UCHAR:
					PrintPodArrayToXml<u8>(name, "char_array", "%u ", count, arr);
					break;
				case parMemberType::TYPE_SHORT:
					PrintPodArrayToXml<s16>(name, "short_array", "%d ", count, arr);
					break;
				case parMemberType::TYPE_USHORT:
					PrintPodArrayToXml<u16>(name, "short_array", "%u ", count, arr);
					break;
				case parMemberType::TYPE_INT:
					PrintPodArrayToXml<s32>(name, "int_array", "%d ", count, arr);
					break;
				case parMemberType::TYPE_UINT:
					PrintPodArrayToXml<u32>(name, "int_array", "%u ", count, arr);
					break;
				case parMemberType::TYPE_FLOAT:
					PrintPodArrayToXml<float>(name, "float_array", "%f\t", count, arr);
					break;
				case parMemberType::TYPE_VEC2V:
					PrintVecArrayToXml(name, "vec2v_array", 2, count, arr);
					break;
				case parMemberType::TYPE_VECTOR2:
					PrintVecArrayToXml(name, "vector2_array", 2, count, arr);
					break;
				case parMemberType::TYPE_VEC3V:
				case parMemberType::TYPE_VECTOR3:
					PrintVecArrayToXml(name, "vector3_array", 3, count, arr);
					break;
				case parMemberType::TYPE_VEC4V:
				case parMemberType::TYPE_VECTOR4:
					PrintVecArrayToXml(name, "vector4_array", 4, count, arr);
					break;

				default:
					InDisplayf("<%s%s>", name, keyStr);
					g_indentLevel++;
					for(int i = 0; i < count; i++)
					{
						psoMember arrElt = arr.GetElement(i);
						PrintPsoMemberAsXml(str, arrElt, "Item");
					}
					g_indentLevel--;
					InDisplayf("</%s>", name);
				}
			}
		}

		break;
	case TYPE_ENUM:
		{
			int enumVal = 0;
			switch((parMemberEnumSubType::Enum)m.GetSubType())
			{
			case parMemberEnumSubType::SUBTYPE_32BIT:
				enumVal = m.GetDataAs<int>(); break;
			case parMemberEnumSubType::SUBTYPE_16BIT:
				enumVal = m.GetDataAs<s16>(); break;
			case parMemberEnumSubType::SUBTYPE_8BIT:
				enumVal = m.GetDataAs<s8>(); break;
			}

			psoEnumSchema schemaEnum = m.GetReferentEnum();
			atLiteralHashValue hashName;
			if (schemaEnum.NameFromValue(enumVal, hashName) && GetString(hashName))
			{
				InDisplayf("<%s%s>%s</%s>", name, keyStr, GetString(hashName), name);
			}
			else
			{
				InDisplayf("<%s%s value=\"%d\" />", name, keyStr, enumVal);
			}
		}
		break;
	case TYPE_MAP:
		{
			psoMemberMapInterface map(str, m);
			int count = map.GetMapCount();

			if (count == 0)
			{
				InDisplayf("<%s%s />", name, keyStr);
			}
			else
			{
				InDisplayf("<%s%s>", name, keyStr);
				g_indentLevel++;

				for(int i = 0; i < count; i++)
				{
					psoMember keyMember = map.GetKey(i);
					psoMember valueMember = map.GetValue(i);
					PrintPsoMemberAsXml(str, valueMember, NULL, &keyMember);
				}

				g_indentLevel--;
				InDisplayf("</%s>", name);
			}
		}
		break;
	case TYPE_BITSET:
	case TYPE_MATRIX34:
	case TYPE_MATRIX44:
	case TYPE_MAT33V:
	case TYPE_MAT34V:
	case TYPE_MAT44V:
	case TYPE_VECBOOLV:
		InDisplayf("<!-- %s not handled -->", name);
		break;
	case INVALID_TYPE:
		break;
	}
}

void PrintPsoStructAsXml(psoStruct& s, const char* nameStr, const char* typeStr, const char* keyStr)
{
	int numMembers = s.GetSchema().GetNumMembers();

	char nameBuf[32];

	if (!nameStr)
	{
		nameStr = GetHashName(s.GetSchema().GetNameHash(), nameBuf);		
	}

	if (typeStr)
	{
		InDisplayf("<%s%s type=\"%s\">", nameStr, keyStr, typeStr);
	}
	else
	{
		InDisplayf("<%s%s>", nameStr, keyStr);
	}

	g_indentLevel++;

	for(int i = 0; i < numMembers; i++)
	{
		psoMember m = s.GetMemberByIndex(i);
		if (!m.IsValid() || psoConstants::IsReserved(m.GetSchema().GetNameHash()))
		{
			continue;
		}
		PrintPsoMemberAsXml(s, m);
	}

	g_indentLevel--;

	InDisplayf("</%s>", nameStr);

}

void PrintPsoAsXml(psoFile& f)
{
	psoStruct s = f.GetRootInstance();
	if (!s.IsNull())
	{
		PrintPsoStructAsXml(s, NULL, NULL, "");
	}
}

#endif // 0

#endif // !__NO_OUTPUT
