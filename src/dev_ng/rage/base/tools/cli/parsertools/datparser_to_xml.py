import re
import sys
import os

def main(infilename, outfilename):
    infile = file(infilename)
    outfile = file(outfilename, "w")

    groups = []

    idt = 0

    linenum = 0

    for i in infile:
        linenum += 1
        tokens = i.split()

        if linenum == 1 and tokens[0] != "type:":
            print "All cammach tuning files must begin with \"type: a\""
            sys.exit(1)

        if tokens[0] == "type:":
            continue
        if '{' in tokens:            
            groups.append(tokens[0])
            print >>outfile, "\t" * idt + "<" + tokens[0] + ">"
            idt += 1
        elif '}' in tokens:
            idt -= 1
            oldname = groups.pop()
            print >>outfile, "\t" * idt + "</" + oldname + ">"
        elif len(tokens) == 4:
            print >>outfile, "\t" * idt + '<' + tokens[0] + ' x="' + tokens[1] + '" y="' + tokens[2] + '" z="' + tokens[3] + '"/>'
        elif len(tokens) == 2:
            print >>outfile, "\t" * idt + '<' + tokens[0] + ' value="' + tokens[1] + '"/>'
        else:
            print "Don't know what to do with line ", i

if __name__ == '__main__':
    main(sys.argv[1], sys.argv[2])