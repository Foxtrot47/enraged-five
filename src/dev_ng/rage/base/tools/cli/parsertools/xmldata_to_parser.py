import sys, os, amara, types

xmlInput = file(sys.argv[1]).read()

outString = ""

doc = amara.parse(xmlInput)

node = doc.childNodes[0]

def translateAttr(a):
	global outString
	tagName = a.localName
	if a.localName[0:2] == "m_":
		tagName = a.localName[2:]
	if a.type == "string":
		outString += "<%s>%s</%s>" % (tagName, a.value, tagName)
	elif a.type == "bool":
		if int(a.value) == 0:
			outString += "<%s value=\"false\"/>" % (tagName)
		else:
			outString += "<%s value=\"true\"/>" % (tagName)
	elif a.type == "float" or a.type == "int":
		outString += "<%s value=\"%s\"/>" % (tagName, a.value)
	else:
		print "Unknown attribute type", a.type

arrayNameMap = {
	"VehicleLocationSet": "Locations",
	"VehicleTransitionSet": "Transitions"
}

def translateNode(node, name):
	global outString
	outString += "<" + name + ">";

	if "name" in node.xml_properties:
		outString += "<Name>%s</Name>" % (node.name)
	
	if "attributes_" in node.xml_properties:
		attrs = node.attributes_
		for i in attrs.xml_child_elements.values():
			translateAttr(i)

	if "contents" in node.xml_properties:
		contents = node.contents
		arrayName = arrayNameMap.get(node.localName, "Contents")
		outString += "<%s>" % (arrayName);
		for i in contents.xml_children:
			if type(i) is not types.UnicodeType:
				translateNode(i, "Item")
		outString += "</%s>" %( arrayName);

	outString += "</" + name + ">";

translateNode(node, node.localName)

xmlOut = amara.parse(outString)

file(sys.argv[2], "w").write(xmlOut.xml(indent=u"yes"))

