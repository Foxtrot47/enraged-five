// 
// parsertool.cpp
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#include "psoprinter.h"

#include "data/aes.h"
#include "parser/manager.h"
#include "parser/psofile.h"
#include "parser/psoparserbuilder.h"
#include "parser/psorscbuilder.h"
#include "parser/psorscparserbuilder.h"
#include "parser/structdefs.h"
#include "parser/tree.h"
#include "parser/visitorutils.h"
#include "data/rson.h"
#include "system/main.h"
#include "system/param.h"
#include "system/platform.h"
#include "system/task.h"
#include "system/timer.h"
#include "zlib/zlib.h"

using namespace rage;

PARAM(unsafe, "[parsertool] Use the unsafe XML reader");
PARAM(savexml, "[parsertool] Save the input to an XML file");
PARAM(saverbf, "[parsertool] Save the input to an RBF file");
PARAM(savepso, "[parsertool] Save the input to a PSO file (RSC format)");
PARAM(savepsoiff, "[parsertool] Save the input to a PSO file (IFF format)");
PARAM(rawxml, "[parsertool] Treat the input as raw XML, don't do any special processing on it");
PARAM(timeout, "[parsertool] Do loading in a child thread, and kill the thread if it doesn't terminate after <timeout> seconds");
PARAM(extstructs, "[parsertool] Load the specified external structure definitions");
PARAM(pscdir, "[parsertool] Loads the structdefs from the specified directory (and subdirs)");
PARAM(psoinfo, "[parsertool] Print out a description of the PSO file");
PARAM(psotoxml, "[parsertool] Print out a description of the PSO file in XML format");
PARAM(stringsxml, "[parsertool] Registers all the names in the specified XML file, so when we convert from PSO to XML we can to reverse name lookups");
PARAM(stringsdir, "[parsertool] Registers all the strings in hashstring_*.txt from the specified directory");
PARAM(sort, "[parsertool] Sorts sibling elements of an XML file, for easier comparisons");
PARAM(comparelayouts, "[parsertool] Compare the layouts of the structures in .psc files vs. the ones in .#esd, for all structures used in the input file");
PARAM(platform, "[parsertool] The platform to use when comparing structure layouts");
PARAM(verbose, "[parsertool] Print more verbose output");
PARAM(checksum, "[parsertool] Include checksum in generated PSO files");
PARAM(decrypt, "[parsertool] Decrypt the input file first, if you specify -decrypt=filename, save the decrypted data to the file too. - use -aeskey to specify the key. You can also use -aeskey=gta5save");
FPARAM(1, infile, "Input file name");

namespace rage
{
	XPARAM(aeskey);
}

parSettings g_Settings;
const char* g_InFileName;
volatile bool g_TaskComplete;
parTree* g_TaskOutput;

// Taken from GTA's savegame_data.cpp
static unsigned char g_savegame_aes_key_GTAV_PC[32] = {
	0x46,  0xed,  0x8d,  0x3f,  0x94,  0x35,  0xe4,  0xec,
	0x12,  0x2c,  0xb2,  0xe2,  0xaf,  0x97,  0xc5,  0x7e,
	0x4c,  0x5a,  0x8c,  0x30,  0x92,  0xc7,  0x84,  0x4e,
	0x11,  0xc6,  0x86,  0xff,  0x41,  0xdf,  0x41,  0x0f
};

static unsigned char g_savegame_aes_key_GTAV_PS3_ORBIS[32] = {
	0x16,  0x85,  0xff,  0xa3,  0x8d,  0x01,  0x0f,  0x0d,
	0xfe,  0x66,  0x1c,  0xf9,  0xb5,  0x57,  0x2c,  0x50,
	0x0d,  0x80,  0x26,  0x48,  0xdb,  0x37,  0xb9,  0xed,
	0x0f,  0x48,  0xc5,  0x73,  0x42,  0xc0,  0x22,  0xf5
};

static unsigned char g_savegame_aes_key_GTAV_XENON_DUR[32] = {
	0x66,  0xc0,  0xd6,  0x9e,  0xce,  0x49,  0xca,  0x45,
	0x76,  0x22,  0xb5,  0x85,  0x8f,  0x29,  0xac,  0xb0,
	0x3c,  0xbf,  0xfb,  0x0b,  0x76,  0x14,  0x37,  0x23,
	0xa1,  0xc2,  0x63,  0xa6,  0x2a,  0xe9,  0x68,  0xec
};

void DecryptBuffer(void* data, size_t size)
{
	AES* aes = NULL;

	// Check for a resource header
	u8* udata = reinterpret_cast<u8*>(data);
	u32 magic = (udata[0] << 24) | (udata[1] << 16) | (udata[2] << 8) | (udata[3]);
	if (magic == datResourceFileHeader::c_MAGIC || magic == sysEndian::Swap(datResourceFileHeader::c_MAGIC))
	{
		// Skip the resource header
		data = udata + 16;
		size -= 16;
	}

	const char* aeskey = NULL;
	if (PARAM_aeskey.Get(aeskey))
	{
		if (!strcmp(aeskey, "gta5save"))
		{
			unsigned char* theKey = NULL;
			switch(g_sysPlatform)
			{
			case platform::WIN32PC:
			case platform::WIN64PC:
				theKey = g_savegame_aes_key_GTAV_PC;
				break;
			case platform::PS3:
			case platform::ORBIS:
				theKey = g_savegame_aes_key_GTAV_PS3_ORBIS;
				break;
			case platform::XENON:
			case platform::DURANGO:
				theKey = g_savegame_aes_key_GTAV_XENON_DUR;
				break;
			default:
				Errorf("Unknown platform '%c' for gta5save decryption", g_sysPlatform);
				return;
			}
			aes = rage_new AES(theKey);
		}
		else
		{
			aes = rage_new AES();
		}
	}
	else
	{
		Errorf("-aeskey is required if you are decrypting a file");
	}

	aes->Decrypt(data, (u32)size);

	delete aes;
}

void LoadTreeThread(sysTaskParameters& /*params*/)
{
	g_TaskOutput = PARSER.LoadTree(g_InFileName, "", &g_Settings);
	g_TaskComplete = true;
}


parTree* DoTreeLoad(const char* infilename, parSettings& settings, float 
#if !__PS3
					timeout
#endif
					)
{
	g_Settings = settings;
	g_InFileName = infilename; // could pass these in via taskparams but this is easier
	g_TaskComplete = false;
	g_TaskOutput = NULL;

	// load in a thread
	sysTaskParameters taskParams;
	sysMemSet(&taskParams,0x00,sizeof(taskParams));

#if !__PS3
	if (timeout > 0.0f) 
	{
		sysTaskHandle childTask = sysTaskManager::Create(TASK_INTERFACE(LoadTreeThread), taskParams);
		int sleepUntil = (int)(timeout * 1000.0f); // ms
		int totalSleep = 0; // ms
		int sleepIncrement = 50; // ms
		while(!sysTaskManager::Poll(childTask) && totalSleep < sleepUntil)
		{
			sysIpcSleep(sleepIncrement);
			totalSleep += sleepIncrement;
		}
		if (!g_TaskComplete)
		{
			Errorf("Load task appears hung. Killing.");
			exit(0);
		}
	}
	else
#endif
	{
		LoadTreeThread(taskParams);
	}

	return g_TaskOutput;
}

void RegisterStrings(parTreeNode* node)
{
	atLiteralHashString s(node->GetElement().GetName());
	const char* attrVal = NULL;
	if (node->FindValueFromPath("@type", attrVal)) 
	{
		atLiteralHashString s(attrVal);
	}



	for(parTreeNode::ChildNodeIterator iter  = node->BeginChildren(); iter != node->EndChildren(); ++iter)
	{
		RegisterStrings(*iter);
	}
}

int SkipWhitespace(fiStream* s)
{
	while(true)
	{
		int c = s->FastGetCh();
		if (c < 0)
		{
			return -1;
		}
		if (!Rson::IsSpace((char)c))
		{
			return c;
		}
	}
}

int SkipToken(fiStream* s)
{
	while(true)
	{
		int c = s->FastGetCh();
		if (c < 0)
		{
			return -1;
		}
		if (Rson::IsSpace((char)c))
		{
			return c;
		}
	}
}

int ReadJsonString(fiStream* s, char* dest, int destLen)
{
	int numWritten = 0;
	bool done = false;
	while(!done && numWritten < destLen-1)
	{
		int c = s->FastGetCh();
		switch(c)
		{
		case '"':
			done = true;
			break; // all done
		case '\\':
			c = s->FastGetCh();
			switch(c)
			{
			case '"': *dest = '"'; dest++; numWritten++; break;
			case 'n': *dest = '\n'; dest++; numWritten++; 	break;
			case 't': *dest = '\t'; dest++; numWritten++;  break;
			case '\\': *dest = '\\'; dest++; numWritten++;  break;
			case 'u':
				{
					char ucodeStr[4];
					s->Read(ucodeStr, 4);
					u32 ucode = strtoul(ucodeStr, NULL, 16);
					char utf8[3];
					int bytes = WideToUtf8Char((u16)ucode, utf8);
					for(int i = 0; i < bytes; i++)
					{
						*dest = utf8[i];
						dest++;
						numWritten++;
						if (numWritten == destLen-1) 
						{
							break;
						}
					}
				}
			}
			break;
		default:
			*dest = (char)c;
			dest++;
			numWritten++;
			break;
		}
	}
	*dest = '\0';
	return numWritten;
}

template<typename Type>
int ParseStringTxtFile(fiStream* stream)
{
	int numStrings = 0;
	while(true)
	{
		int wsEnd = SkipWhitespace(stream);
		if (wsEnd < 0) { break; }
		int tokEnd = SkipToken(stream);
		if (tokEnd < 0) { break; }
		wsEnd = SkipWhitespace(stream);
		if (wsEnd < 0) { break; }
		char TheString[RAGE_MAX_PATH * 4]; // lots of space, just in case
		ReadJsonString(stream, TheString, RAGE_MAX_PATH * 4);
		Type hashInst(TheString);
		numStrings++;
	}
	return numStrings;
};

void RegisterStringsFromDir(const char* dirName)
{
	sysTimer timer;

	char fileName[RAGE_MAX_PATH];

	int numStrings = 0;

	formatf(fileName, "%s\\hashstrings_nonfinal.txt", dirName);
	if (fiStream* s = ASSET.Open(fileName, ""))
	{
		numStrings += ParseStringTxtFile<atHashString>(s);
		s->Close();
	}

	formatf(fileName, "%s\\hashstrings_final.txt", dirName);
	if (fiStream* s = ASSET.Open(fileName, ""))
	{
		numStrings += ParseStringTxtFile<atFinalHashString>(s);
		s->Close();
	}


	formatf(fileName, "%s\\hashstrings_literal.txt", dirName);
	if (fiStream* s = ASSET.Open(fileName, ""))
	{
		numStrings += ParseStringTxtFile<atLiteralHashString>(s);
		s->Close();
	}

	Displayf("Loaded %d strings in %fs", numStrings, timer.GetTime());
}

void SortTree(parTreeNode* node)
{
	// These are stable sorts, so it's legit to do more than one in a row
	node->SortChildrenByAttribute("key");	// Secondary sort
	node->SortChildrenByName();				// Primary sort

	for(parTreeNode::ChildNodeIterator iter = node->BeginChildren(); iter != node->EndChildren(); ++iter)
	{
		SortTree(*iter);
	}
}

int Main()
{
#if !__PS3
	sysTaskManager::AddScheduler("defSched",65535,32768,PRIO_LOWEST,~0,0);
#endif

	bool needsInputFile = true;

	const char* typesToCompare = NULL;
	if (PARAM_comparelayouts.Get(typesToCompare))
	{
		if (!PARAM_extstructs.Get() || !PARAM_pscdir.Get() || !PARAM_platform.Get())
		{
			Errorf("-comparelayouts requires -extstructs, -pscdir, and -platform");
#if !__FINAL
			sysParam::Help();
#endif
			exit(1);
		}
		needsInputFile = false;
	}
	else
	{
		if (PARAM_extstructs.Get() && PARAM_pscdir.Get())
		{
			Errorf("You can only use -extstructs and -pscdir at the same time when using -comparelayouts");
#if !__FINAL
			sysParam::Help();
#endif
			exit(1);
		}
	}

	if (PARAM_decrypt.Get() && !PARAM_aeskey.Get())
	{
		Errorf("-decrypt requires -aeskey (and probably -platform)");
		exit(1);
	}

	const char* inFileName = NULL;
	if (!PARAM_infile.Get(inFileName) && !typesToCompare) {
#if !__FINAL
		sysParam::Help();
#endif
		exit(1);
	}


#if __WIN32PC
	const char* platformName = NULL;
	if (PARAM_platform.Get(platformName))
	{
		g_sysPlatform = sysGetPlatform(platformName);
	}
#endif

	const char* inFileNameOrig = inFileName;

	size_t fileSize = 0;
	char* fileBuffer = NULL;
	char fileBuffName[RAGE_MAX_PATH];

	fiStream* inStream = NULL;
	if (needsInputFile)
	{
		if (inFileName) 
		{
			inStream = ASSET.Open(inFileName, "");
		}
		if (inStream)
		{
			fileSize = inStream->Size();
			fileBuffer = rage_new char[fileSize];
			inStream->Read(fileBuffer, (int)fileSize);
			inStream->Close();
			fiDevice::MakeMemoryFileName(fileBuffName, RAGE_MAX_PATH, fileBuffer, fileSize, false, inFileNameOrig);
			inFileName = &fileBuffName[0];
		}
		else
		{
			Errorf("Couldn't open input file %s", inFileName);
			exit(1);
		}
	}

	const char* decryptName = NULL;

	if (PARAM_decrypt.Get(decryptName))
	{
		if (inStream)
		{
			DecryptBuffer(fileBuffer, fileSize);

			if (decryptName)
			{
				fiStream* outStream = ASSET.Create(decryptName, "");
				if (outStream)
				{
					outStream->Write(fileBuffer, (int)fileSize);
					outStream->Close();
				}
			}

			fiDevice::MakeMemoryFileName(fileBuffName, RAGE_MAX_PATH, fileBuffer, fileSize, false, inFileNameOrig);
			inFileName = &fileBuffName[0];
		}
	}



	datResourceFileHeader* header = reinterpret_cast<datResourceFileHeader*>(fileBuffer);
	if (header && header->Magic == sysEndian::Swap(datResourceFileHeader::c_MAGIC))
	{
		sysEndian::SwapMe(header->Magic);
		sysEndian::SwapMe(header->Version);
		sysEndian::SwapMe(*reinterpret_cast<u32*>(&header->Info.Virtual));
		sysEndian::SwapMe(*reinterpret_cast<u32*>(&header->Info.Physical));
	}
	if (header && header->Magic == datResourceFileHeader::c_MAGIC)
	{
		Assertf(header->Info.GetPhysicalSize() == 0, "What's this file doing with a physical chunk?");
		Assertf(g_sysPlatform != platform::XENON, "Only zlib decompression is currently supported");
		size_t decompressedDataSize = header->Info.GetVirtualSize();
		char* decompressionBuffer = rage_aligned_new(16) char[sizeof(datResourceFileHeader) + decompressedDataSize];
		sysMemCpy(decompressionBuffer, fileBuffer, sizeof(datResourceFileHeader));

		z_stream c_stream;
		memset(&c_stream,0,sizeof(c_stream));
		if (inflateInit2(&c_stream,-MAX_WBITS) < 0)
			Quitf("Error in inflateInit");
		c_stream.next_in = (u8*)fileBuffer + sizeof(datResourceFileHeader);
		c_stream.avail_in = (uInt) fileSize - sizeof(datResourceFileHeader);
		c_stream.next_out = (u8*)decompressionBuffer + sizeof(datResourceFileHeader);
		c_stream.avail_out = (uInt)decompressedDataSize;
		int err = inflate(&c_stream, Z_FINISH);
		if (err < 0)
			Quitf("Error extracting zip data %d %s", err, c_stream.msg);
		inflateEnd(&c_stream);

		delete fileBuffer;
		fileBuffer = decompressionBuffer;
		fileSize = decompressedDataSize + sizeof(datResourceFileHeader);

		fiDevice::MakeMemoryFileName(fileBuffName, RAGE_MAX_PATH, fileBuffer, fileSize, false, inFileNameOrig);
		inFileName = &fileBuffName[0];
	}


	INIT_PARSER;
	pgStreamer::InitClass();

	parSettings settings = parSettings::sm_StandardSettings;

	if (PARAM_rawxml.Get()) {
		settings.SetFlag(parSettings::NEVER_SORT_ATTR_LIST, true);
		settings.SetFlag(parSettings::PROCESS_SPECIAL_ATTRS, false);
	}
	else
	{
		settings.SetFlag(parSettings::NEVER_SORT_ATTR_LIST, false);
		settings.SetFlag(parSettings::PROCESS_SPECIAL_ATTRS, true);
	}

	settings.SetFlag(parSettings::READ_SAFE_BUT_SLOW, !PARAM_unsafe.Get());

	parManager* externalPscMgr = NULL;
	parManager* externalEsdMgr = NULL;
	parManager* originalMgr = parManager::sm_Instance;

	const char* stringsXmlName = NULL;
	if (PARAM_stringsxml.Get(stringsXmlName))
	{
		parTree* strings = PARSER.LoadTree(stringsXmlName, "");

		RegisterStrings(strings->GetRoot());

		delete strings;
	}

	const char* stringsDirName = NULL;
	if (PARAM_stringsdir.Get(stringsDirName))
	{
		RegisterStringsFromDir(stringsDirName);
	}

#if PARSER_USES_EXTERNAL_STRUCTURE_DEFNS
	const char* pscDir = NULL;
	if (PARAM_pscdir.Get(pscDir))
	{
		externalPscMgr = rage_new parManager;
		externalPscMgr->Initialize(parSettings::sm_StandardSettings, false);
		parManager::sm_Instance = externalPscMgr;

		sysTimer t;
		Displayf("Loading parser schemas from %s", pscDir);
		parAddReflectionClassesToManager(*externalPscMgr);
		PARSER.GetExternalStructureManager().LoadStructdefs(pscDir);
		Displayf("Loaded structdefs in %fs", t.GetTime());
	}

	const char* psmFile = NULL;
	if (PARAM_extstructs.Get(psmFile))
	{
		externalEsdMgr = rage_new parManager;
		externalEsdMgr->Initialize(parSettings::sm_StandardSettings, false);
		parManager::sm_Instance = externalEsdMgr;

		sysTimer t;
		Displayf("Loading external structure definitions from %s", psmFile);
		parAddReflectionClassesToManager(*externalEsdMgr);
		PARSER.GetExternalStructureManager().LoadExternalStructureDefns(psmFile);
		Displayf("Loaded parStructures in %fs", t.GetTime());
	}
#endif

	Assert(PARAM_comparelayouts.Get() || !externalEsdMgr || !externalPscMgr);

	float timeout = -1.0f;
	PARAM_timeout.Get(timeout);

	if (typesToCompare)
	{
		atString typesToCompareStr(typesToCompare);

		atArray<atString> typeNameArray;
		typesToCompareStr.Split(typeNameArray, ",", true);

		for(int i = 0; i < typeNameArray.GetCount(); i++)
		{
#if __DEV || __TOOL
			parStructure* esdStructure = externalEsdMgr->FindStructure(typeNameArray[i].c_str());
			if (esdStructure)
			{
				parCompareMemoryLayouts(NULL, esdStructure, externalEsdMgr, "extstruct file (runtime)", externalPscMgr, "PSC files", PARAM_verbose.Get());
			}
			else
			{
				Errorf("Couldn't find a type named '%s' in the structure dictionary %s", typeNameArray[i].c_str(), psmFile);
			}
#endif
		}

		parManager::sm_Instance = originalMgr;

		if (externalPscMgr)
		{
			externalPscMgr->Uninitialize();
			delete externalPscMgr;
		}
		if (externalEsdMgr)
		{
			externalEsdMgr->Uninitialize();
			delete externalEsdMgr;
		}

		SHUTDOWN_PARSER;

		delete fileBuffer;
		return 0;
	}

	/////////////////////////////////////////////////////////////////
	// Load in the file - in any format. Try and create an instance of whatever was in the file too

	parPtrToStructure objectData = NULL;
	psoFile* psofile =  NULL;
	parStructure* structure = NULL;
	parTree* tree = NULL;
	void* inPlaceBuffer = NULL;

	fiStream* inputFile = ASSET.Open(inFileName, "");
	if (!inputFile)
	{
		Errorf("Couldn't open input file %s", inFileName);
		exit(1);
	}
	u32 magic = 0;
	inputFile->ReadInt(&magic, 1);
	inputFile->Close();
	magic = NtoB(magic);

	if (magic == psoConstants::PSIN_MAGIC_NUMBER ||
		magic == psoConstants::PMAP_MAGIC_NUMBER || 
		magic == psoConstants::PSCH_MAGIC_NUMBER || 
		magic == psoConstants::STRS_MAGIC_NUMBER ||
		magic == datResourceFileHeader::c_MAGIC ||
		magic == sysEndian::Swap(datResourceFileHeader::c_MAGIC))
	{
		if (magic == datResourceFileHeader::c_MAGIC || magic == sysEndian::Swap(datResourceFileHeader::c_MAGIC))
		{
			Displayf("Loading RSC PSO file from %s", inFileName);

			datResourceMap map;
			datResourceFileHeader* header = reinterpret_cast<datResourceFileHeader*>(fileBuffer);
			header->Info.GenerateMap(map);
			char* startOfResource = fileBuffer + sizeof(datResourceFileHeader);
			map.VirtualBase = startOfResource;
			for(int i = 0; i < map.VirtualCount; i++)
			{
				map.Chunks[i].DestAddr = startOfResource + (ptrdiff_t)((char*)map.Chunks[i].SrcAddr - datResourceFileHeader::c_FIXED_VIRTUAL_BASE);
			}

			datResource rsc(map,inFileName);
			psoResourceData* psoRsc = reinterpret_cast<psoResourceData*>( map.GetVirtualBase() );
			if (psoRsc) 
			{
				psoRsc->Place(psoRsc, rsc);
				psoRsc->PostPlace();
			}

			psofile = psoInitFromResource(psoRsc, inFileName);
		}
		else
		{
			Displayf("Loading IFF PSO file from %s", inFileName);

			psofile = psoLoadFile(inFileName);
		}

		if (psofile)
		{
			psoStruct s = psofile->GetRootInstance();

			if (externalPscMgr || externalEsdMgr)
			{
				structure = PARSER.FindStructure(s.GetSchema().GetNameHash().GetHash());

				if (structure)
				{
					psoLoadInPlaceResult res = psoLoadObjectInPlace(*psofile, PSO_HEAP_CURRENT, PSO_HEAP_CURRENT);
					objectData = res.m_RootObject;
					inPlaceBuffer = res.m_InplaceBuffer;
				}
			}
		}
	}
	else
	{
		Displayf("Loading tree from %s", inFileName);
		tree = DoTreeLoad(inFileName, settings, timeout);

		if (tree && (externalPscMgr || externalEsdMgr))
		{
			structure = PARSER.FindStructure(tree->GetRoot()->GetElement().GetName());
			if (structure)
			{
				objectData = structure->Create();
				PARSER.LoadFromStructure(tree->GetRoot(), *structure, objectData);
			}
			else
			{
				Errorf("Couldn't find a structdef for the root level node <%s> in file %s", tree->GetRoot()->GetElement().GetName(), inFileName);
			}
		}

		if (tree && PARAM_sort.Get())
		{
			Displayf("Sorting XML tree");
			SortTree(tree->GetRoot());
		}
	}

	if (PARAM_comparelayouts.Get() && objectData && structure)
	{
#if __DEV || __TOOL
		parCompareMemoryLayouts(objectData, structure, externalEsdMgr, "extstruct file (runtime)", externalPscMgr, "PSC files", PARAM_verbose.Get());
#endif
	}

#if !__NO_OUTPUT
	if (PARAM_psoinfo.Get() && psofile)
	{
		PrintPsoDescription(*psofile);
	}

	//if (PARAM_psotoxml.Get() && psofile)
	//{
	//	PrintPsoAsXml(*psofile);
	//}
#endif

	const char* outfile = NULL;
	if (PARAM_savexml.Get(outfile) && outfile && *outfile)
	{

		if (structure)
		{
			Displayf("Saving OBJECTS to XML file %s", outfile);
			PARSER.SaveFromStructure(outfile, "", *structure, objectData, parManager::XML);
		}
		else if (tree)
		{
			Displayf("Saving TREE to XML file %s", outfile);
			PARSER.SaveTree(outfile, "", tree, parManager::XML);
		}
	}
	
	outfile = NULL;
	if (PARAM_saverbf.Get(outfile) && outfile && *outfile)
	{
		if (structure)
		{
			Displayf("Saving OBJECTS to RBF file %s", outfile);
			PARSER.SaveFromStructure(outfile, "", *structure, objectData, parManager::BINARY);
		}
		else if (tree)
		{
			Displayf("Saving TREE to RBF file %s", outfile);
			PARSER.SaveTree(outfile, "", tree, parManager::BINARY);
		}
	}

	outfile = NULL;
	if (PARAM_savepso.Get(outfile) && outfile && *outfile)
	{
		if (structure)
		{
#if __RESOURCECOMPILER
			Displayf("Saving OBJECTS to PSO RSC file %s", outfile);
			psoRscSaveFromStructure(outfile, *structure, objectData, PARAM_checksum.Get());
#else	
			Errorf("Can't save to PSO RSC file unless this is a RESOURCECOMPILER build");
#endif
		}
	}

	outfile = NULL;
	if (PARAM_savepsoiff.Get(outfile) && outfile && *outfile)
	{
		if (structure)
		{
			Displayf("Saving OBJECTS to PSO IFF file %s", outfile);
			psoSaveFromStructure(outfile, *structure, objectData, PARAM_checksum.Get());
		}
	}

	delete tree;

	if (inPlaceBuffer)
	{
		delete [] inPlaceBuffer;
	}
	else if (structure && objectData)
	{
		structure->Destroy(objectData);
		delete objectData;
	}

	delete psofile;

	parManager::sm_Instance = originalMgr;

	if (externalPscMgr)
	{
		externalPscMgr->Uninitialize();
		delete externalPscMgr;
	}
	if (externalEsdMgr)
	{
		externalEsdMgr->Uninitialize();
		delete externalEsdMgr;
	}

	delete fileBuffer;

	SHUTDOWN_PARSER;

	return 0;
}
