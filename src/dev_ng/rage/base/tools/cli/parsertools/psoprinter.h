// 
// psoprinter.h
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PARSERTOOLS_PSOPRINTER_H
#define PARSERTOOLS_PSOPRINTER_H

namespace rage
{
	class psoFile;
	class psoSchemaCatalog;
	class psoStruct;
	class psoStructureMap;
}

#if !__NO_OUTPUT

void PrintPsoStructure(rage::psoStruct& str);

void PrintPsoSchemaCatalog(rage::psoFile& file, rage::psoSchemaCatalog cat);
void PrintPsoStructureMap(rage::psoStructureMap& map);
void PrintPsoStructureTree(rage::psoFile& f);

// Prints everything
void PrintPsoDescription(rage::psoFile& f);

void PrintPsoAsXml(rage::psoFile& f);

#endif // !__NO_OUTPUT

#endif
