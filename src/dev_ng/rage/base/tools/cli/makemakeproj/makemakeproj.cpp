#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int fgetline(FILE *f,char *dest,int destSize) {
	int ch;
	int stored = 0;
	while ((ch = fgetc(f)) != EOF) {
		if (ch >= 32 && destSize > 1) {
			*dest++ = ch;
			--destSize;
			++stored;
		}
		if (ch == 10)
			break;
	}
	*dest = 0;
	return stored;
}

struct { const char *projConfig, *platform, *config, *shaderTarget; } configs[] = {
	{ "BankRelease|Win32","win32","bankrelease", "win32_30" },
	{ "BankRelease|Xbox 360","xenon","bankrelease", "fxl_final" },
	{ "SN PS3 SNC BankRelease|Win32","psn","bankrelease", "psn" },
	{ "Beta|Win32","win32","beta", "win32_30" },
	{ "Beta|x64","win64","beta", "win32_30" },
	{ "Beta|Xbox 360","xenon","beta", "fxl_final" },
	{ "SN PS3 SNC Beta|Win32","psn","beta", "psn" },
	{ "RscBeta|Win32","win32","rscbeta", "win32_30" },
	{ "ToolBeta|Win32","win32","toolbeta", "win32_30" },
	{ "ToolBeta|x64","win64","toolbeta", "win32_30" },
	{ "Debug|Win32","win32","debug", "win32_30" },
	{ "Debug|x64","win64","debug", "win32_30" },
	{ "Debug|Xbox 360","xenon","debug", "fxl_final" },
	{ "SN PS3 SNC Debug|Win32","psn","debug", "psn" },
	{ "RscDebug|Win32","win32","rscdebug", "win32_30" },
	{ "ToolDebug|Win32","win32","tooldebug", "win32_30" },
	{ "ToolDebug|x64","win64","tooldebug", "win32_30" },
	{ "PreRelease|Win32","win32","prerelease", "win32_30" },
	{ "PreRelease|x64","win64","prerelease", "win32_30" },
	{ "PreRelease|Xbox 360","xenon","prerelease", "fxl_final" },
	{ "SN PS3 SNC PreRelease|Win32","psn","prerelease", "psn" },
	{ "ToolPreRelease|Win32","win32","toolprerelease", "win32_30" },
	{ "ToolPreRelease|x64","win64","toolprerelease", "win32_30" },
	{ "Profile|Win32","win32","profile", "win32_30" },
	{ "Profile|x64","win64","profile", "win32_30" },
	{ "Profile|Xbox 360","xenon","profile", "fxl_final" },
	{ "SN PS3 SNC Profile|Win32","psn","profile", "psn" },
	{ "ToolProfile|Win32","win32","toolprofile", "win32_30" },
	{ "ToolProfile|x64","win64","toolprofile", "win32_30" },
	{ "Final|Win32","win32","final", "win32_30" },
	{ "Final|x64","win64","final", "win32_30" },
	{ "Final|Xbox 360","xenon","final", "fxl_final" },
	{ "SN PS3 SNC Final|Win32","psn","final", "psn" },
	{ "ToolFinal|Win32","win32","toolfinal", "win32_30" },
	{ "ToolFinal|x64","win64","toolfinal", "win32_30" },
	{ "Release|Win32","win32","release", "win32_30" },
	{ "Release|x64","win64","release", "win32_30" },
	{ "Release|Xbox 360","xenon","release", "fxl_final" },
	{ "RscRelease|Win32","win32","rscrelease", "win32_30" },
	{ "SN PS3 SNC Release|Win32","psn","release", "psn" },
	{ "ToolRelease|Win32","win32","toolrelease", "win32_30" },
	{ "ToolRelease|x64","win64","toolrelease", "win32_30" }
};

#define NELEM(x)	(sizeof(x)/sizeof(x[0]))

enum VersionNumber
{
	kVS2005,
	kVS2008,
};

int main(int argc,char **argv)
{
	if (argc < 2) 
	{
		printf("usage: makemakeproj basename filelist\n");
		printf("expects basename.guid to contain project guid.\n");
		printf("expects filelist to contain list of files in project\n");
		printf("creates basename.vcproj\n");
		return 1;
	}

	const char *basename = argv[1];
	const char* versionArg = argv[3];
	VersionNumber version = kVS2005;
	char projname[256];
	if( !versionArg || _stricmp(versionArg, "vs2005") == 0)
	{
		version = kVS2005;
		sprintf(projname,"%s_2005.vcproj",basename);
	}
	else
	{
		version = kVS2008;
		sprintf(projname,"%s_2008.vcproj",basename);
	}

	char guidname[256];
	sprintf(guidname,"%s.guid",basename);

	const char *filelist = argv[2];

	FILE *projfile = fopen(projname,"w");
	if (!projfile) {
		fprintf(stderr,"unable to create '%s'\n",projname);
		return 1;
	}
	FILE *guidfile = fopen(guidname,"r");
	if (!guidfile) {
		char cmd[256];
		const char *RS_TOOLSROOT = getenv("RS_TOOLSROOT");
		if (!RS_TOOLSROOT)
		{
			fprintf(stderr, "RS_TOOLSROOT environment variable is not set!  Unable to find uuidgen program.\n");
			return -1;
		}
		sprintf(cmd,"%s\\bin\\coding\\uuidgen -o%s",RS_TOOLSROOT,guidname);
		if (system(cmd) == 0) {
			sprintf(cmd,"p4 add %s",guidname);
			system(cmd);
			guidfile = fopen(guidname,"r");
		}
		else {
			fprintf(stderr,"unable to bootstrap '%s' for you\n",guidfile);
			return 1;
		}
		
	}
	if (!guidfile) {
		fprintf(stderr,"still unable to read guid even after creating one?\n");
		return 1;
	}

	char guid[64];
	fgetline(guidfile,guid,sizeof(guid));
	fclose(guidfile);
	_strupr(guid);

	char* versionNumber = NULL;
	if(version == kVS2005)
	{
		//Default or version == kVS2005

		versionNumber = "8.00";

		fprintf(projfile,
			"<?xml version=\"1.0\" encoding=\"Windows-1252\"?>\n"
			"<VisualStudioProject\n"
			"\tProjectType=\"Visual C++\"\n"
			"\tVersion=\"%s\"\n"
			"\tName=\"%s\"\n"
			"\tProjectGUID=\"{%s}\"\n"
			"\tKeyword=\"MakeFileProj\"\n"
			"\t>\n"
			"\t<Platforms>\n"
			"\t\t<Platform\n"
			"\t\t\tName=\"Win32\"\n"
			"\t\t/>\n"
			"\t\t<Platform\n"
			"\t\t\tName=\"x64\"\n"
			"\t\t/>\n"
			"\t\t<Platform\n"
			"\t\t\tName=\"Xbox 360\"\n"
			"\t\t/>\n"
			"\t</Platforms>\n"
			"\t<ToolFiles>\n"
			"\t</ToolFiles>\n"
			"\t<Configurations>\n",
			versionNumber,
			basename,
			guid);
	}
	else if(version == kVS2008)
	{
		versionNumber = "9.00";

		fprintf(projfile,
			"<?xml version=\"1.0\" encoding=\"Windows-1252\"?>\n"
			"<VisualStudioProject\n"
			"\tProjectType=\"Visual C++\"\n"
			"\tVersion=\"%s\"\n"
			"\tName=\"%s\"\n"
			"\tProjectGUID=\"{%s}\"\n"
			"\tKeyword=\"MakeFileProj\"\n"
			"\tTargetFrameworkVersion=\"131072\"\n"		//New line with 2008 projects.
			"\t>\n"
			"\t<Platforms>\n"
			"\t\t<Platform\n"
			"\t\t\tName=\"Win32\"\n"
			"\t\t/>\n"
			"\t\t<Platform\n"
			"\t\t\tName=\"x64\"\n"
			"\t\t/>\n"
			"\t\t<Platform\n"
			"\t\t\tName=\"Xbox 360\"\n"
			"\t\t/>\n"
			"\t</Platforms>\n"
			"\t<ToolFiles>\n"
			"\t</ToolFiles>\n"
			"\t<Configurations>\n",
			versionNumber,
			basename,
			guid);
	}

// -j = number of jobs to run at once
// -s = silent, don't ech commands before running them (reduces tty spew)
#define COMMON	"-j $(NUMBER_OF_PROCESSORS) -s"

	for (int i=0; i<NELEM(configs); i++) {
		const char *projConfig = configs[i].projConfig;
		const char *platform = configs[i].platform;
		const char *config = configs[i].config;
		const char *shaderTarget = configs[i].shaderTarget;
		
		fprintf(projfile,
			"\t\t<Configuration\n"
			"\t\t\tName=\"%s\"\n"
			"\t\t\tOutputDirectory=\"%s_%s\"\n"
			"\t\t\tIntermediateDirectory=\"%s_%s\"\n"
			"\t\t\tConfigurationType=\"0\"\n"
			"\t\t\t>\n"
			"\t\t\t<Tool\n"
			"\t\t\t\tName=\"VCNMakeTool\"\n"
			"\t\t\t\tBuildCommandLine=\"$(RS_TOOLSROOT)\\bin\\rage\\make " COMMON " -f %s.mk PLATFORM=%s CONFIG=%s RAGE_ASSET_ROOT=$(RAGE_ASSET_ROOT) RAGE_DIR=$(RAGE_DIR) RS_TOOLSROOT=$(RS_TOOLSROOT) all\"\n"
			"\t\t\t\tReBuildCommandLine=\"$(RS_TOOLSROOT)\\bin\\rage\\make " COMMON " -f %s.mk PLATFORM=%s CONFIG=%s clean&#x0D;&#x0A;$(RS_TOOLSROOT)\\bin\\rage\\make " COMMON " -f %s.mk PLATFORM=%s CONFIG=%s RAGE_ASSET_ROOT=$(RAGE_ASSET_ROOT) RAGE_DIR=$(RAGE_DIR) RS_TOOLSROOT=$(RS_TOOLSROOT) all\"\n"
			"\t\t\t\tCleanCommandLine=\"$(RS_TOOLSROOT)\\bin\\rage\\make " COMMON " -f %s.mk PLATFORM=%s CONFIG=%s clean\"\n"
			"\t\t\t\tOutput=\"%s.dummy\"\n"
			"\t\t\t/>\n"
			"\t\t</Configuration>\n",
				projConfig,
				platform,config,
				platform,config,
				basename,platform,config,
				basename,platform,config,
				basename,platform,config,
				basename,platform,config,
				shaderTarget);
			
	}

	fprintf(projfile,
		"\t</Configurations>\n"
		"\t<References>\n"
		"\t</References>\n"
		"\t<Files>\n"
		);

	FILE *files = fopen(filelist,"r");
	if (!files) {
		fprintf(stderr,"unable to open file list '%s'\n",filelist);
		return 1;
	}
	char file[128];
	while (fgetline(files,file,sizeof(file))) {
		if (file[0] == 0 || file[0] == ';')
			continue;

		int sl = strlen(file);
		if (sl && file[sl-1] == '{') {
			file[sl-2] = '\0';
			fprintf(projfile,
				"\t\t<Filter\n"
				"\t\t\tName=\"%s\"\n"
				"\t\t\tFilter=\"\">\n",file);
		}
		else if (file[0] == '}')
			fprintf(projfile,"\t\t</Filter>\n");
		else
			fprintf(projfile,
				"\t\t<File\n"
				"\t\t\tRelativePath=\"%s\"\n"
				"\t\t\t>\n"
				"\t\t</File>\n",file);
	}
	fclose(files);

	fprintf(projfile,
		"\t</Files>\n"
		"\t<Globals>\n"
		"\t</Globals>\n"
		"</VisualStudioProject>\n");
	fclose(projfile);

	return 0;
}
