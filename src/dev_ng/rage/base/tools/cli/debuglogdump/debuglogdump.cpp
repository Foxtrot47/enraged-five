// 
// /debuglogdump.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "system/main.h"

#include "diag/debuglog.h"
#include "system/param.h"
#include "system/stack.h"

//Disabled because it is causing compiler errors otherwise.  
//Enable __DEBUGLOG locally to get a working version of this application.
//
/*
#if !__DEBUGLOG
#error You must have __DEBUGLOG enabled to build debuglogdump
#endif
*/


using namespace rage;

PARAM(debuglogstart, "[debuglogdump] Byte offset to start dumping the debuglog from.  Can be approximate.  Will dump from first log entry starting after this offset.");
PARAM(debuglogstop, "[debuglogdump] Byte offset to stop dumping the debuglog from.  Can be approximate.  Will dump until first log entry starting after this offset.");
PARAM(mapfile, "[debuglogdump] Location of .cmpmap file to use for callstack dumps, if unspecified you will not get any callstacks.");

int Main()
{
	sysStack::ShutdownClass();

	const char* mapfile = NULL;
	PARAM_mapfile.Get(mapfile);
	if (mapfile && *mapfile != '\0')
		sysStack::InitClass(mapfile);

	int start = 0, stop = INT_MAX;
	PARAM_debuglogstart.Get(start);
	PARAM_debuglogstop.Get(stop);

	diagDebugLogInit(true, true);
	diagDebugLogDump(start, stop, mapfile && *mapfile != '\0');
	diagDebugLogShutdown();

	if (mapfile && *mapfile != '\0')
		sysStack::ShutdownClass();
	sysStack::InitClass(sysParam::GetArg(0));

	return 0;
}