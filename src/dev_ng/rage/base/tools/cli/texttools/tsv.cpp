// tsv.cpp

#if __WIN32

#include "tsv.h"

#include "file/asset.h"

#include <stdarg.h>
#include <stdlib.h>

#define VERBOSE 0

namespace rage {

static void _TPrintf(const rage::char16 *_tstring)
{
	USES_CONVERSION;
	if (!_tstring)
		Printf("[null]");
	else if (hasWides(_tstring))
		Printf("[unprintable]");
	else
		Printf(W2A(_tstring));
}

// This strips out most of the extra quotes and things that MS Excel adds
// to your strings if you use quotes or commas or linefeeds in your string
static void Requote(rage::char16 *string)
{
	int len;
	bool keepDoublequotes=false;

	// if surrounded by a pair of triple doublequotes, replace them with a pair of doublequotes

	len=wcslen(string);
	if (!wcsncmp(_C16("\"\"\""),string,3) && !wcsncmp(_C16("\"\"\""),&string[len-3],3))
	{
		wcsncpy(&string[1],&string[3],len-4);
		string[len-4]=0;

		keepDoublequotes=true;
	}

	// replace all ""word"" with "word"

	len=wcslen(string);
	for (int i=0;i<len-2;i++)
	{
		if (wcsncmp(_C16("\"\""),&string[i],2))
			continue;

		for (int j=i+2;j<len-2;j++)
		{
			if (!wcsncmp(_C16("\"\""),&string[j],2))
			{
				wcsncpy(&string[i],&string[i+1],len-1-i);	// remove first of the first pair of ""
				string[len-1]=0;

				wcsncpy(&string[j],&string[j+1],len-2-j);	// remove second of the second pair of ""
				string[len-2]=0;

				len-=2;
				i=j;										// continue where we left off

				break;
			}
		}
	}

	// if surrounded by doublequotes, remove them

	if (!keepDoublequotes && (string[0]==_T('\"') && string[len-1]==_T('\"')))
	{
		wcsncpy(string,&string[1],len-2);
		string[len-2]=0;
	}
}

// This looks for escape sequences (currently just \b) and replaces them
// with the one-character equivalent.  It does not attempt to reclaim any
// leftover bytes at the end of the string.
static void ParseEscapes(rage::char16 *str)
{
	rage::char16 *from=str;
	rage::char16 *to=str;

	if (*from == '\0')
		return;

	do
	{
		if (from[0]==_T('\\') && from[1]==_T('b'))
		{
			*to++=_T('\b');
			*from++;
		}
		else
			*to++=*from;
	}
	while (*++from);

	*to=_T('\0');
}

int ParseTSVLine(const char *src,int count,int size,...) {
	va_list args;
	va_start(args,size);
	int parsed = 0;
	while (parsed < count) {
		int stored = 0;
		char *dest = va_arg(args,char*);
		while (*src && *src != 13 && *src != '\t') {
			if (stored < size-1)
				dest[stored++] = *src;
			++src;
		}
		++parsed;
		dest[stored] = 0;
		if (*src == '\t')
			++src;
		else
			break;
	}
	return parsed;
}


void parTsvFile::Kill() {
	if (m_Table) {
		int count = m_ColumnCount * m_RowCount;
		int i;
		for (i=0; i<m_ColumnCount; i++)
			delete [] m_Columns[i];
		for (i=0; i<count; i++)
			delete [] m_Table[i];
		delete [] m_Table;
		m_Table = 0;

#if __UNICODE
		for (i=0; i<count; i++)
			delete [] m_TableAscii[i];
		delete [] m_TableAscii;
		m_TableAscii = 0;
#endif
	}
}

extern int fgetwsc(char *,int,fiStream*);

int fgetws(rage::char16 *dest,int maxSize,fiStream *S) {
	int ch1, ch2, stored = 0;
	--maxSize;
	while (maxSize>0)
	{
		if ((ch1=S->GetCh())==-1)
			break;
		if ((ch2=S->GetCh())==-1)
			break;
		int ch=(ch2<<8) + ch1;
//		printf("ch=%d\n",ch);
		*dest++ = rage::char16(ch);
		maxSize--;
		stored++;
		if (ch == 13)
			break;
	}
	*dest = 0;
	return stored;
}


bool parTsvFile::Load(const char *filename,const char *extension) {
	fiStream *S = ASSET.Open(filename,extension);
	int i;

	if (!S)
		return false;

#if __UNICODE
	// first two characters are some kind of identifier
	rage::char16 junk[4];
	fgetws(junk,2,S);

	if (junk[0]!=65279)
		Quitf("File '%s' is not a UNICODE file",filename);
#endif

	// Read header line.
	rage::char16 buf[4096];
	fgetws(buf,sizeof(buf),S);

//	rage::char16 message[1024];
//	swprintf(message,_T("Line='%s'\n"),buf);
//	_TPrintf(message);

#ifndef _UNICODE
	if (buf[0]==-1 && buf[1]==-2)
		Quitf("File '%s' is in UNICODE format.  Must compile application with UNICODE enabled.",filename);
#endif

	if (wcschr(buf,'#')) *wcschr(buf,'#') = 0;
	rage::char16 *p = buf;

#ifndef _UNICODE
	if (buf[0]==-1 && buf[1]==-2)
		Quitf("File is in UNICODE format.  Must compile application with UNICODE enabled.");
#endif

//	_tprintf(_T("sizeof(rage::char16)=%d\n"),sizeof(rage::char16));

//	for (i=0;i<ret;i++)
//		_tprintf(_T("character %d='%c' (%d)\n"),i,buf[i],buf[i]);

	m_ColumnCount = 0;
	while (*p && m_ColumnCount < MaxColumns) {
//		_tprintf(_T("character='%c' (%d)\n"),*p,*p);

		while (*p && *p<32)
			++p;
		rage::char16 *start = p;
		while (*p >= 32 && *p != '\t')
			++p;
		if (*p)
			*p++ = 0;
		if (start != p)
			m_Columns[m_ColumnCount++] = WideStringDuplicate(start);
//		if (start != p)
//			_tprintf(_T("column header %d='%s'  length=%d  (%c,%c)\n"),m_ColumnCount-1,start,wcslen(start),start[0],start[wcslen(start)-1]);
	}

//	for (int i=0;i<m_ColumnCount;i++)
//		_tprintf(_T("column %d='%s'\n"),i,m_Columns[i]);

	// Count up number of lines in file.
	m_RowCount = 0;
	while (fgetws(buf,sizeof(buf),S))
		m_RowCount++;

	// Rewind file, skip header again
	S->Seek(0);
	fgetws(buf,sizeof(buf),S);

	m_Table = rage_new rage::char16*[m_ColumnCount * m_RowCount];
	memset(m_Table,0,sizeof(rage::char16*) * m_ColumnCount * m_RowCount);

	m_RowCount = 0;

	while (fgetws(buf,sizeof(buf),S))
	{
#if VERBOSE
		rage::char16 message[1024];
		swprintf(message,_T("Line: %s\n"),buf);
//		_TPrintf(message);
#endif

		if (wcschr(buf,'#')) *wcschr(buf,'#') = 0;
		p = buf;
		int count = 0;
		while (*p && *p==10 || *p<32)		// skip leading <CR>
			++p;
		while (*p && count < m_ColumnCount)
		{
			// skip leading control characters
			while (*p && *p!=10 && *p<32 && *p!='\t')
				++p;

			// mark the start of this cell
			rage::char16 *start = p;

			// go to the end of this cell and NULL terminate it
			while (*p==10 || *p >= 32 && *p != '\t')
				++p;
			if (*p)
				*p++ = 0;

			// record the cell
			if (start != p) {
				m_Table[m_RowCount * m_ColumnCount + count] = WideStringDuplicate(start);

				Requote(m_Table[m_RowCount*m_ColumnCount+count]);
				ParseEscapes(m_Table[m_RowCount*m_ColumnCount+count]);

#if VERBOSE
				rage::char16 buffer[1024];
				swprintf(buffer,_T("m_RowCount=%d  count=%d  string='%s'\n"),m_RowCount,count,m_Table[m_RowCount*m_ColumnCount+count]);
				_TPrintf(buffer);
#endif

				++count;
			}
		}
		++m_RowCount;
	}

	S->Close();

#if __UNICODE
	m_TableAscii = rage_new char*[m_ColumnCount * m_RowCount];
	memset(m_TableAscii,0,sizeof(char*)*m_ColumnCount*m_RowCount);

	for (i=0;i<m_ColumnCount*m_RowCount;i++)
	{
		if (!m_Table[i])
			continue;

		int length=wcslen(m_Table[i]);
		m_TableAscii[i]=rage_new char[length+1];
		for (int j=0;j<length+1;j++)
		{
			if (m_Table[i][j]>255)
				m_TableAscii[i][j]='?';
			else
				m_TableAscii[i][j]=char(m_Table[i][j]&0xff);
		}

//		if (m_TableAscii[i])
//			printf("m_TableAscii[%d]='%s'\n",i,m_TableAscii[i]);
	}
#endif

//	Print();

	return true;
}

void parTsvFile::Print() const {
	Displayf("%d x %d table",m_RowCount,m_ColumnCount);
	int i, j;
	for (i=0; i<m_ColumnCount; i++)
	{
		_TPrintf(m_Columns[i]);
		_TPrintf(_C16(","));
	}
	Printf("\n");
	for (j=0; j<m_RowCount; j++) {
		for (i=0; i<m_ColumnCount; i++)
		{
			_TPrintf(GetString(j,i));
			_TPrintf(_C16(","));
		}
		Printf("\n");
	}
}


int parTsvFile::GetInt(int row,int col) const {
	return wcstol(GetString(row,col), NULL, 10);
}


/**
float parTsvFile::GetFloat(int row,int col) const {
	return (float) _ttof(GetString(row,col));
}
**/

int parTsvFile::GetColumn(const char *match, bool required) const {
	int i;
	USES_CONVERSION;
	const rage::char16 *_tmatch=A2W(match);
//	_tprintf(_T("_tmatch='%s'\n"),_tmatch);
	for (i=0; i<m_ColumnCount; i++)
		if (!_wcsicmp(_tmatch,m_Columns[i]))
			return i;
	if(required)
		Quitf("Missing column '%s'",match);
	Warningf("Missing column '%s'",match);
	return -1;
}


int parTsvFile::GetRow(int keyColumn,const char *match) const {
	int i;
	USES_CONVERSION;
	const rage::char16 *_tmatch=A2W(match);
	for (i=0; i<m_RowCount; i++) {
		const rage::char16 *s = GetString(i,keyColumn);
		if (s && !_wcsicmp(s,_tmatch))
			return i;
	}
	Quitf("Column %d never had match for '%s'",keyColumn,match);
	return -1;
}

int parTsvFile::GetRowSafe(int keyColumn,const char *match) const {
	int i;
	USES_CONVERSION;
	const rage::char16 *_tmatch=A2W(match);
	for (i=0; i<m_RowCount; i++) {
		const rage::char16 *s = GetString(i,keyColumn);
		if (s && !_wcsicmp(s,_tmatch))
			return i;
	}
	//Quitf("Column %d never had match for '%s'",keyColumn,match);
	return -1;
}


/**
void parTsvFile::SetString(int row,int col,const char *string) {
	int cell = row * m_ColumnCount + col;
	delete m_Table[cell];
	m_Table[cell] = StringDuplicate(string);
}


void parTsvFile::SetInt(int row,int col,int val) {
	char buf[16];
	sprintf(buf,"%d",val);
	SetString(row,col,buf);
}


void parTsvFile::SetFloat(int row,int col,float val) {
	char buf[16];
	sprintf(buf,"%.4f",val);
	SetString(row,col,buf);
}
**/
} // namespace rage


#endif
