// stringeditor.h

#ifndef TEXTTOOLS_STRINGEDITOR_H
#define TEXTTOOLS_STRINGEDITOR_H

#include "editstring.h"
#include "tsv.h"

#include "atl/array.h"
#include "atl/map.h"
#include "data/base.h"
#include "text/language.h"

namespace rage {

class bkBank;
class txtStringTable;


// txtStringEditor //

class txtStringEditor : public datBase {

public:

	txtStringEditor();
	~txtStringEditor();

	// Load .txt stringtable, optionally filtering out any entries that don't match the given platform string.
	// Stringtable entries with no platform value entered are assumed to be for all platforms.
	bool Load(const char *filename, const char* platform=0, const char* region=0, const int flagmask=0x7fffffff);
#if __DEV
	bool Export(const char *basePathname);
#endif
	void Print() const;

	bool IsValid();

	txtEditString *GetCurrent();

	void AddWidgets(bkBank &bank);

	int GetNumStringEntries();

	typedef atMap<atString,txtStringEntry*> Map;
	Map& GetHashTable()				{return mStringEntries;};

private:

	txtStringTable *Create(txtLanguage::eLanguage language);
	bool AddString(const char *identifier,txtEditString *string);
	bool DoLoad(const char *filename, const char* platform, const char* region, const int flagmask);

	static void CopyData(txtEditString& source,class txtStringData& destination);

public:

	parTsvFile mFile; // this needs to be persistent since ::DoLoad it to allocate memory which export then uses
	txtEditString *mEditStringHead;
	Map mStringEntries;
	txtStringEntry *mStringEntryHead;

	atArray<const char*> mIdentifierList;

	// Loading //

	int mLinesParsed;

	// Editing //

	int mCurrentString;
	int mNumStringEntries;
	int mNumEditStrings;
};

} // namespace rage
#endif
