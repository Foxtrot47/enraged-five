// tsv.h

// Tab-separated values table, based on Etherton's parse/csv.h.
// TSVs support UNICODE.  For example, Excel does NOT support UNICODE
// in CSV files, so we have to use UNICODE-encoded text files instead.
// Default file extension is .txt, which is easily recognized by
// Wordpad, Notepad, Excel, Word, and a million other applications.

#ifndef TEXT_TSV_H
#define TEXT_TSV_H

#if __WIN32

#include "text/config.h"			// for _UNICODE
#include <tchar.h>
namespace rage {

int ParseTSVLine(const char *src,int count,int size,...);
//int ParseTSVLine(const rage::char16 *src,int count,int size,...);

class parTsvFile {
public:
	parTsvFile() : m_Table(0), m_ColumnCount(0), m_RowCount(0) { }
	~parTsvFile() { Kill(); }

	bool Load(const char *filename,const char *extension="txt");
	bool Save(const char *,const char *);
	void Kill();
	void Print() const;

	int GetColumnCount() const { return m_ColumnCount; }
	int GetRowCount() const { return m_RowCount; }

	int GetRow(int keyColumn,const char *match) const;

	// Same as above, but won't quitf if there's no match (returns -1)
	int GetRowSafe(int keyColumn,const char *match) const;

	int GetColumn(const char *columnName, bool required=false) const;

#if __UNICODE
	const char *GetStringAscii(int row,int col) const { return m_TableAscii[row * m_ColumnCount + col]; }
#else
	const char *GetStringAscii(int row,int col) const { return m_Table[row * m_ColumnCount + col]; }
#endif

//	const char *GetString(int row,int col) const { return m_Table[row * m_ColumnCount + col]; }
//	const rage::char16 *GetStringUnicode(int row,int col) const { return m_Table[row * m_ColumnCount + col]; }
	const char16 *GetString(int row,int col) const { return m_Table[row * m_ColumnCount + col]; }

	int GetInt(int row,int col) const;
//	float GetFloat(int row,int col) const;

//	void SetString(int row,int col,const char *string);
//	void SetInt(int row,int col,int val);
//	void SetFloat(int row,int col,float val);

private:
	enum { MaxColumns = 16 };
	char16 *m_Columns[MaxColumns];
	char16 **m_Table;

#if __UNICODE
	char **m_TableAscii;
#endif

	int m_ColumnCount, m_RowCount;
};
} // namespace rage

#endif

#endif
