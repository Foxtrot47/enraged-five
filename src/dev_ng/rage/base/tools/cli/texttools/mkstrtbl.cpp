// mkstrtbl.c

// NOTE: This should be complied in a "ToolBeta Win32" configuration

#include "texttools/stringeditor.h"
#include "system/param.h"
#include <direct.h>

#include "system/main.h"

namespace rage
{
PARAM(in,"the file that we read from");
PARAM(out,"the file that we write to");
PARAM(platform,"platform string, e.g. ps2 or xbox--only export entries with a matching or blank value in the 'platform' column");
PARAM(region,"region string, e.g. ntsc or pal--only export entries with a matching or blank value in the 'region' column");
PARAM(flagmask,"output flag bitmask (decimal)--only export entries with a matching flag");
PARAM(nopause,"don't wait for a keypress when export finishes--useful for batching");
PARAM(help,"print help message");
PARAM(h,"print help message");
}

using namespace rage;

int Main()
{
	// Parse Arguments //

	const char *inFile=NULL;
	const char *outFile=NULL;
	const char *platform=NULL;
	PARAM_platform.Get(platform);

	PARAM_in.Get(inFile);
	PARAM_out.Get(outFile);
	
	const char *region=NULL;
	PARAM_region.Get(region);
	int flagmask=0x7fffffff;
	PARAM_flagmask.Get(flagmask);

	if (PARAM_help.Get() || PARAM_h.Get() || !inFile || !outFile)
		Quitf("Usage: %s -in whatever.txt -out whatever.strtbl [-nopause] [-platform platformName] [-region regionName] [-flagmask flagmask]",sysParam::GetProgramName());

	Displayf("inFile=%s.txt  outFile=%s.strtbl",inFile,outFile);

	// Load TSV file //

	txtStringEditor *stringEditor=rage_new txtStringEditor();
	if (!stringEditor->Load(inFile, platform, region, flagmask))
		Quitf("Failed to load stringtable '%s'",inFile);

	// Export StringTable //

#if __DEV
	stringEditor->Export(outFile);
#endif

	delete stringEditor;

	// Pause //

//#if __WIN32PC
//	if (!PARAM_nopause.Get())
//	{
//		chdir("C:\\");
//		system("pause");
//	}
//#endif

	return 0;
}

