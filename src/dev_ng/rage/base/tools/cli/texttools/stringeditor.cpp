// stringeditor.cpp

#include "stringeditor.h"

#include "editstring.h"
#include "tsv.h"

#include "atl/string.h"
#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "file/asset.h"
#include "text/fonttex.h"
#include "text/stringtable.h"
#include <locale.h>
#include <mbctype.h>

namespace rage {
// txtStringEditor //

txtStringEditor::txtStringEditor()
{
	mEditStringHead=NULL;
	mStringEntryHead=NULL;

	mCurrentString=NULL;
	mNumStringEntries=0;
	mNumEditStrings=0;

	mIdentifierList.Reset();
}

txtStringEditor::~txtStringEditor()
{
 	txtStringEntry* cur = mStringEntryHead;
	txtStringEntry* next = NULL;
	mStringEntryHead = NULL;
 	while(cur != NULL)
 	{
 		next = cur->mNext;
 		delete cur;
 		cur = next;
 	}

	txtEditString *edCur=mEditStringHead;
	txtEditString *edNext=NULL;
	mEditStringHead = NULL;
	while (edCur)
	{
		edNext = edCur->mNext;
		delete edCur;
		edCur = edNext;
	}

	mStringEntries.Kill();
}

bool txtStringEditor::Load(const char *filename, const char* platform/*=0*/, const char* region/*=0*/, const int flagmask/*=0x7fffffff*/)
{
	bool success=false;

	mLinesParsed=-1;

	__try {
		success=DoLoad(filename, platform, region, flagmask);
	}
	__except(1) {
		success=false;

		if (mLinesParsed==-1)
			Errorf("Error loading CSV file");
		else
			Errorf("Parse error around line %d.  %d entries read successfully.",mLinesParsed+1,mNumEditStrings);
		// nothing
	}

	return success;
}

bool txtStringEditor::DoLoad(const char *filename, const char* platform, const char* region, const int flagmask)
{
	mLinesParsed=-1;

	mIdentifierList.Reset();

	Map::Iterator iterator=mStringEntries.CreateIterator();
	for (iterator.Start();!iterator.AtEnd();iterator.Next())
	{
		delete iterator.GetData();
	}
	mStringEntries.Kill();

	if (!ASSET.Exists(filename,"txt"))
		return false;

	if (!mFile.Load(filename,"txt"))
		return false;

	int columnIdentifier=mFile.GetColumn("identifier");
	int columnFont=mFile.GetColumn("font", true);
	int columnLanguage=mFile.GetColumn("language", true);
	int columnString=mFile.GetColumn("string", true);
	int columnOffsetX=mFile.GetColumn("offset x");
	int columnOffsetY=mFile.GetColumn("offset y");
	int columnScaleX=mFile.GetColumn("scale x");
	int columnScaleY=mFile.GetColumn("scale y");
	int columnFlags=mFile.GetColumn("flags");
	int columnForceUppercase=mFile.GetColumn("uppercase");
	int columnPlatform=-1;
	if(platform)
	{
		columnPlatform=mFile.GetColumn("platform");
		if(columnPlatform<0)
		{
			Errorf("Platform specific loading was requested but %s.txt file has no 'platform' column!", filename);
			return false;
		}
	}
	int columnRegion=-1;
	if(region)
	{
		columnRegion=mFile.GetColumn("region");
		if(columnRegion<0)
		{
			Errorf("Region specific loading was requested but %s.txt file has no 'region' column!", filename);
			return false;
		}
	}

	int numRows=mFile.GetRowCount();

	mNumEditStrings=0;

	for (int row=0;row<numRows;row++)
	{
		mLinesParsed=row;

		const char *identifier=mFile.GetStringAscii(row,columnIdentifier);
		const char *font=mFile.GetStringAscii(row,columnFont);
		const char *language=mFile.GetStringAscii(row,columnLanguage);

		const rage::char16 *string=mFile.GetString(row,columnString);
		if (!string)
			string=_C16("");

		if (!identifier || !language || !font)
			continue;

		if(columnPlatform != -1)
		{
			const char *platString = mFile.GetStringAscii(row,columnPlatform);
			if(platString && platString[0])		// blank platform string should NOT be skipped, otherwise a matching string is required:
			{
				if(stricmp(platString, platform))
					continue;
			}
		}
		if(columnRegion != -1)
		{
			const char *regionString = mFile.GetStringAscii(row,columnRegion);
			if(regionString && regionString[0])		// blank region string should NOT be skipped, otherwise a matching string is required:
			{
				if(stricmp(regionString, region))
					continue;
			}
		}

		const char *c;

		const int flags=columnFlags!=-1 ? ((c=mFile.GetStringAscii(row,columnFlags))!=NULL)?atoi(c):1 : 1;
		if ((flagmask & flags) == 0)
			continue;	// flag mask does not match

		// setup the locale, so character toupper conversion works correctly
		int iLang = txtLanguage::Get(language);
		if (iLang>=0)
			setlocale( LC_CTYPE,  txtLanguage::mNames[iLang] );
		_setmbcp(_MB_CP_LOCALE );

		if ( (columnForceUppercase!=-1) && ((c=mFile.GetStringAscii(row,columnForceUppercase))!=NULL) && (atoi(c)>0) )
		{
			// force the stringtable text to be uppercase (even if the string is not in the source file)
			CompileTimeAssert(sizeof(char16) == sizeof(wchar_t));
			wchar_t * wc = reinterpret_cast<wchar_t*>(const_cast<char16*>(string));
			while(*wc)
				*wc++ = towupper(*wc);
		}

		txtEditString *newString=rage_new txtEditString(font,language,string);

		// all the extra nonsense is to prevent missing cells in table from causing NULL derefs
		newString->mOffsetX=columnOffsetX!=-1 ? ((c=mFile.GetStringAscii(row,columnOffsetX))!=NULL?(signed char)atoi(c):(signed char)0) : (signed char)0;
		newString->mOffsetY=columnOffsetY!=-1 ? ((c=mFile.GetStringAscii(row,columnOffsetY))!=NULL?(signed char)atoi(c):(signed char)0) : (signed char)0;
		newString->mScale.x=columnScaleX!=-1 ? ((c=mFile.GetStringAscii(row,columnScaleX))!=NULL?float(atof(c)):1.f) : 1.f;
		newString->mScale.y=columnScaleY!=-1 ? ((c=mFile.GetStringAscii(row,columnScaleY))!=NULL?float(atof(c)):1.f) : 1.f;
		newString->mFlags=(unsigned short)flags;

		// without all the extra nonsense
//		newString->mOffsetX=(signed char)atoi(mFile.GetStringAscii(row,columnOffsetX));
//		newString->mOffsetY=(signed char)atoi(mFile.GetStringAscii(row,columnOffsetY));
//		newString->mScale.x=float(atof(mFile.GetStringAscii(row,columnScaleX)));
//		newString->mScale.y=float(atof(mFile.GetStringAscii(row,columnScaleY)));
//		newString->mFlags=(unsigned short)atoi(mFile.GetStringAscii(row,columnFlags));

		if (!AddString(identifier,newString))
		{
			// duplicate!
 			delete newString;
 			continue;
		}
/**
		USES_CONVERSION;
		Displayf(
			"scale=%0.2f %0.2f  offset=%d %d  flags=0x%04X  identifier='%s'    font='%s'    string='%s'",
			newString->mScale.x,newString->mScale.y,
			newString->mOffsetX,newString->mOffsetY,
			int(newString->mFlags),
			identifier,
			font,
			hasWides(string)?"[unprintable]":W2A(string)
		);
**/

		mNumEditStrings++;
	}

	if (mNumStringEntries==0 && mNumEditStrings==0)
	{
		Errorf("No string entries found");
		return false;
	}

	Displayf("Loaded %d string entries containing %d edit strings",mNumStringEntries,mNumEditStrings);

//	Print();

	return true;
}

static int compareFunction(const void *arg1,const void *arg2)
{
	return _stricmp(*(const char**)arg1,*(const char**)arg2);
}

#if __DEV
bool txtStringEditor::Export(const char *basePathname)
{
	// Create String Tables //

	txtStringTable *stringTables[txtLanguage::kLanguageCount];
	int offsets[txtLanguage::kLanguageCount];

	for (int language=0;language<txtLanguage::GetCount();language++)
	{
		int stringCount=0;
		Map::Iterator iterator=mStringEntries.CreateIterator();
		for (iterator.Start();!iterator.AtEnd();iterator.Next())
		{
			txtStringEntry *stringEntry=iterator.GetData();
			if (stringEntry->mStrings[language])
				stringCount++;
		}

		// only save stringtables for languages with strings
		if (!stringCount)
		{
			stringTables[language]=NULL;
			continue;
		}

		stringTables[language]=Create((txtLanguage::eLanguage)language);
	}

	// Create Stringtables file //

	char pathname[256];
	ASSET.FullPath(pathname,256,basePathname,"strtbl");
	fiStream *stream=fiStream::Create(pathname);

	if (!stream)
		Quitf("Failed to create file '%s'.  Make sure the folder exists, or that the file is not Read-only.",pathname);

	Displayf("Saving stringtable '%s'",pathname);

	int numLanguages=txtLanguage::GetCount();
	stream->WriteInt(&numLanguages,1);

	// set all offsets to 0 for the first pass, we'll fill them in correctly later

	for (int language=0;language<txtLanguage::GetCount();language++)
	{
		offsets[language]=0;
		stream->WriteInt(&offsets[language],1);
	}

	// Save Version number //

	unsigned int version=0x0200;
	stream->WriteInt(&version,1);

	// Sort list of labels //

	int numIdentifiers=mIdentifierList.GetCount();
	const char **refs=rage_new const char*[numIdentifiers];
	int j=0;
	for (int i=0;i<numIdentifiers;i++)
		refs[j++]=mIdentifierList[i];

	qsort(refs,numIdentifiers,4,compareFunction);

	// Save list of labels //

	stream->WriteInt(&numIdentifiers,1);
	for (j=0;j<numIdentifiers;j++)
	{
		int length=strlen(refs[j]);
		stream->WriteInt(&length,1);
		stream->Write((const char*)refs[j],length+1);
	}
	delete []refs;

	// Save Stringtables //

	for (int language=0;language<txtLanguage::GetCount();language++)
	{
		offsets[language]=stream->Tell();
		if (stringTables[language])
			stringTables[language]->Save(*stream);
	}

	// Write directory of stringtable file //

	stream->Seek(0);
	stream->WriteInt(&numLanguages,1);
	for (int language=0;language<txtLanguage::GetCount();language++)
		stream->WriteInt(&offsets[language],1);

	stream->Close();


	for (int language=0;language<txtLanguage::GetCount();language++)
	{
		if(stringTables[language])
		{
			stringTables[language]->Kill();
			delete stringTables[language];
		}
	}

	return true;
}
#endif

void txtStringEditor::CopyData(txtEditString& source,txtStringData& destination)
{
//	destination.mFont=txtGetFont(source.mFont);			// HACK - get this right

	destination.m_Hash=source.mHash;
	destination.m_Scale=source.mScale;
	destination.m_OffsetX=source.mOffsetX;
	destination.m_OffsetY=source.mOffsetY;
	destination.m_Flags=source.mFlags;

	destination.m_String=rage_new rage::char16[wcslen(source.GetString())+1];
	wcscpy(destination.m_String,source.GetString());

	destination.m_Font=(txtFontTex*)(int(StringDuplicate(source.mFont))|0x1);
//	destination.mFont=txtGetFont(source.mFont);
}

txtStringTable *txtStringEditor::Create(txtLanguage::eLanguage language)
{
//	Warningf("txtStringEditor::CreateStringTable() not fully implemented");

	int stringTableCount=0;
	Map::Iterator iterator=mStringEntries.CreateIterator();
	for (iterator.Start();!iterator.AtEnd();iterator.Next())
	{
		stringTableCount++;
	}

//	Displayf("Creating string table with %d strings in %s",stringTableCount,txtLanguage::mNames[language]);

	txtStringTable *stringTable;
	stringTable=rage_new txtStringTable();
	stringTable->Init();

	int stringNum=0;
	iterator=mStringEntries.CreateIterator();
	for (iterator.Start();!iterator.AtEnd();iterator.Next())
	{
		txtStringEntry *stringEntry=iterator.GetData();
		txtStringData *stringData=rage_new txtStringData;
		if (stringEntry->mStrings[language])
		{
			CopyData(*stringEntry->mStrings[language],*stringData);
		}
		else if (stringEntry->mStrings[txtLanguage::kEn])
		{
			Warningf("No %s translation for '%s'.  Using English Translation",txtLanguage::mNames[language],iterator.GetKey());
			CopyData(*stringEntry->mStrings[txtLanguage::kEn],*stringData);
		}
		else
		{
			Errorf("No English or %s translation for '%s'.",txtLanguage::mNames[language],iterator.GetKey());
			const rage::char16 *message=_C16("X");
			stringData->m_String=rage_new rage::char16[wcslen(message)+1];
			wcscpy(stringData->m_String,message);
		}
		stringData->m_Hash=txtStringTable::Hash(iterator.GetKey());

		// look for collisions
		if(!stringTable->Add(stringData)) // return false if already exists
		{
			// let's see if it's a "bad collision"
			txtStringData*tmpdata = stringTable->Get(iterator.GetKey());
			if(strcmp(tmpdata->GetStringUTF8(),stringData->GetStringUTF8()))
				Warningf("Hashtable collision for '%s'.  This is bad - grab a programmer!",iterator.GetKey());
			else
				Errorf("Duplicate entry (discarded):\n  language='%s'\n  identifier='%s'",txtLanguage::mNames[language],iterator.GetKey()); // seems you can get here if you capitalize them differently
			delete stringData;
			continue;
		}
		stringNum++;
	}

	return stringTable;
}

bool txtStringEditor::AddString(const char *identifier,txtEditString *string)
{
	int language=txtLanguage::Get(string->mLanguage);

	if (language<0)
	{
		Warningf("unrecognized language: '%s' for key '%s' with value '%s'",string->mLanguage, identifier, string->GetCharString());
		return false;
	}

	txtStringEntry **entryPtr=mStringEntries.Access(identifier);
	txtStringEntry *entry = entryPtr ? *entryPtr : NULL;

	if (!entry)
	{
		entry=rage_new txtStringEntry;
		mStringEntries.Insert(atString(identifier),entry);
		entry->mNext=mStringEntryHead;
		mStringEntryHead=entry;
		mNumStringEntries++;
		
		if (mIdentifierList.GetCount()+1>=mIdentifierList.GetCapacity())
		{
			// increase capacity by one:
			mIdentifierList.Grow();
			// decrease count by one:
			mIdentifierList.Pop();
		}
		mIdentifierList.Insert(0)=identifier;
	}

	if (entry->mStrings[language])
	{
		Errorf("Duplicate entry (discarded):\n  language='%s'\n  identifier='%s'",txtLanguage::mNames[language],identifier);
		return false;
	}

	entry->mStrings[language]=string;

	string->mNext=NULL;

	txtEditString *last=mEditStringHead;
	if (!last)
	{
		mEditStringHead=string;
		return true;
	}

	while (last->mNext)
		last=last->mNext;

	last->mNext=string;

	return true;
}

void txtStringEditor::Print() const
{
	Displayf("--- Edit Strings ---");
	for (const txtEditString *string=mEditStringHead;string;string=string->mNext)
	{
		string->Print();
	}

	Displayf("--- String Entries ---");
	for (const txtStringEntry *entry=mStringEntryHead;entry;entry=entry->mNext)
	{
		entry->Print();
	}

	Displayf("--- Identifiers ---");
	for (int i=0;i<mIdentifierList.GetCount();i++)
		Displayf("%s",mIdentifierList[i]);

	Displayf(" ");
}

bool txtStringEditor::IsValid()
{
	return mStringEntries.GetNumUsed()>0 ? true : false;
}

int txtStringEditor::GetNumStringEntries()
{
	int numStringEntries=0;

	Map::Iterator iterator=mStringEntries.CreateIterator();
	for (iterator.Start();!iterator.AtEnd();iterator.Next())
	{
		numStringEntries++;
	}

	return numStringEntries;
}

txtEditString *txtStringEditor::GetCurrent()
{
	if (mCurrentString>=mNumStringEntries)
		return NULL;

	txtEditString *current=mEditStringHead;
	for (int i=0;i<mCurrentString;i++)
		current=current->mNext;

	return current;
}
} // namespace rage

