// editstring.cpp

#include "texttools/editstring.h"
#include "bank/bank.h"

namespace rage {
// txtStringEntry //

txtStringEntry::txtStringEntry()
{
	for (int i=0;i<txtLanguage::kLanguageCount;i++)
		mStrings[i]=NULL;

	mNext=NULL;
}

void txtStringEntry::Print() const
{
	for (int i=0;i<txtLanguage::kLanguageCount;i++)
		if (mStrings[i])
			Displayf("%s: %s",txtLanguage::mNames[i],mStrings[i]->GetCharString());
}

// txtEditString //

txtEditString::txtEditString()
{
	mFont=StringDuplicate("unknown");
	mLanguage=StringDuplicate("en");
	mString=WideStringDuplicate(_C16("X"));
	mCharString=StringDuplicate("X");

	mScale.Set(1,1);
	mHash=0;
	mOffsetX=mOffsetY=0;
	mFlags=0x000;
}

txtEditString::~txtEditString()
{
	delete [] mString;
	delete [] mCharString;
	delete [] mLanguage;
	delete [] mFont;
}

txtEditString::txtEditString(const char *font,const char *language,const char16 *string)
{
	USES_CONVERSION;

	mFont=StringDuplicate(font);
	mLanguage=StringDuplicate(language);
	mString=WideStringDuplicate(string);
	mCharString=StringDuplicate(W2A(string));

	for (char *chr=mLanguage;*chr;chr++)
		*chr=char(tolower(*chr));

	mScale.Set(1,1);
	mOffsetX=mOffsetY=0;
	mFlags=0x000;
}

void txtEditString::SetString(const char16 *string)
{
	USES_CONVERSION;

	delete [] mString;
	mString=WideStringDuplicate(string);

	delete [] mCharString;
	mCharString=StringDuplicate(W2A(string));
}

char16 *txtEditString::GetString() const
{
	return mString;
}

char *txtEditString::GetCharString() const
{
	return mCharString;
}

void txtEditString::Print() const
{
	USES_CONVERSION;
	const char *string=hasWides(mString)?"[unprintable]":W2A(mString);
	Displayf("font='%s'  language='%s'  string='%s'",mFont,mLanguage,string);
}

#if __BANK
void txtEditString::AddWidgets(bkBank &bank)
{
	USES_CONVERSION;
	char *string=W2A(mString);

	bank.AddText("Font",mFont,strlen(mFont),true);
	bank.AddText("Language",mLanguage,strlen(mFont),true);
	bank.AddText("String",string,strlen(string),true);
	bank.AddSlider("OffsetX",&mOffsetX,-127,127,1);
	bank.AddSlider("OffsetY",&mOffsetY,-127,127,1);
	bank.AddSlider("Flags",&mFlags,0,65535,1);
	bank.AddSlider("Scale",&mScale,0.01f,10.f,0.01f);
}
#endif
} // namespace rage

