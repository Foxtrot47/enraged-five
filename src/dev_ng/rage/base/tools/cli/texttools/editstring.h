// editstring.h

#ifndef TEXTTOOLS_EDITSTRING_H
#define TEXTTOOLS_EDITSTRING_H

#include "text/language.h"
#include "vector/vector2.h"


namespace rage {

class bkBank;
class txtStringEditor;

class txtEditString  
{
	friend txtStringEditor;

public:

	txtEditString();
	txtEditString(const char *font,const char *language,const rage::char16 *string);
	virtual ~txtEditString();

	void operator=(txtEditString&);

	void Print() const;

	void AddWidgets(bkBank &bank);

	void SetString(const rage::char16 *string);
	rage::char16 *GetString() const;
	char *GetCharString() const;

public:
	txtEditString *mNext;			// singly-linked list
	char *mFont;
	char *mLanguage;
	Vector2 mScale;
	signed char mOffsetX,mOffsetY;
	unsigned short mFlags;
	unsigned int mHash;

private:
	rage::char16 *mString;
	char *mCharString;
};

class txtStringEntry {

public:

	txtStringEntry();

	void Print() const;

	txtEditString *mStrings[txtLanguage::kLanguageCount];

	txtStringEntry *mNext;
};

} // namespace rage

#endif
