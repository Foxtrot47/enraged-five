#include "system/main.h"
#include "system/param.h"
#include "file/stream.h"
#include "diag/seh.h"
#include "data/aes.h"
#include "data/base64.h"

using namespace rage;

PARAM(decrypt, "Decrypt instead of encrypt");
PARAM(key, "Base64 encoded encryption key to use");
PARAM(inputfile, "The input file to encrypt/decrypt");
PARAM(outputfile, "The output file to write the encrypted/decrypted input to");


int Main()
{
	fiStream* inputStream = NULL;
	fiStream* outputStream = NULL;

	rtry
	{
		const char* inputFilename;
		rcheck(PARAM_inputfile.Get(inputFilename), catchall, Errorf("Missing -inputfile"));

		const char* outputFilename;
		rcheck(PARAM_outputfile.Get(outputFilename), catchall, Errorf("Missing -outputfile"));

		const char* encodedKey;
		rcheck(PARAM_key.Get(encodedKey), catchall, Errorf("Missing -key"));

		u8 key[32];
		unsigned keyLen = 0;
		rverify(datBase64::Decode(encodedKey, sizeof(key), key, &keyLen), catchall, Errorf("Failed to decode key"));
		rverify(keyLen == 32, catchall, Errorf("Key is not 32 bytes (%d)", keyLen));

		inputStream = fiStream::Open(inputFilename, true);
		outputStream = fiStream::Create(outputFilename);

		rverify(inputStream != NULL, catchall, Errorf("Failed to open input stream"));
		rverify(outputStream != NULL, catchall, Errorf("Failed to open output stream"));

		int size = inputStream->Size();
		rverify(size > 0, catchall, Errorf("Input file is empty"));
		
		u8* buffer = rage_new u8[size];

		rverify(inputStream->Read(buffer, size) == size, catchall, Errorf("Failed to read entire input stream"));
		
		AES aes(key);
		bool success = PARAM_decrypt.Get() ? aes.Decrypt(buffer, size) : aes.Encrypt(buffer, size);
		rverify(success, catchall, Errorf("Failed to encrypt/decrypt input"));

		rverify(outputStream->Write(buffer, size) == size, catchall, Errorf("Failed to write entire output stream"));
	
		inputStream->Close();
		outputStream->Flush();
		outputStream->Close();

		delete[] buffer;
		
		return 0;
	}
	rcatchall
	{
		if (inputStream != NULL)
		{
			inputStream->Close();
			inputStream = NULL;
		}

		if (outputStream != NULL)
		{
			outputStream->Close();
			outputStream = NULL;
		}

		return -1;
	}
}
