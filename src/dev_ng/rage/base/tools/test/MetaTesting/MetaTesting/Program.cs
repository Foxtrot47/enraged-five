﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Metadata.Model;
using RSG.Metadata.Data;
using RSG.Metadata.Parser;
using RSG.Base.Configuration;

namespace MetaTesting
{
    class Program
    {
        static void Main(string[] args)
        {
           
            StructureDictionary sd = new StructureDictionary();
            IConfig conf = ConfigFactory.CreateConfig();
            IBranch branch = conf.Project.DefaultBranch;

            sd.Load(branch, "x:/gta5/src/dev/game/Peds/rendering/");

            Structure s = sd["CPedPropInfo"];
            MetaFile mf = new MetaFile(s);

            ITunable compData = RSG.Metadata.Util.Tunable.FindFirstStuctureNamed("aAnchors", mf.Members);
            
            IMember member = RSG.Metadata.Util.Member.FindStructMemberByName(mf.Definition.Members, "aAnchors");

            (compData as ArrayTunable).Insert(0, new StructureTunable(member as StructMember, null));

            //(compData as ArrayTunable).Add(new StructureTunable(member as StructMember));

            mf.Serialise("X:/gta5/test/afterInsert.meta");

            /*
            IMember member = RSG.Metadata.Util.Member.FindStructMemberByName(mf.Definition.Members, "availComp");
            ITunable cunt = RSG.Metadata.Util.Tunable.FindFirstStuctureNamed("availComp", mf.Members);
            UInt8Tunable test = new UInt8Tunable(new UInt8Member());
            test.Value = 255;
            (cunt as ArrayTunable).Add(test);
            (cunt as ArrayTunable).Add(test);

            mf.Serialise("X:/gta5/assets/peds/meta/s_f_ranger_01.meta");
             * */
            /*
            StructureDictionary sd = new StructureDictionary();
            sd.Load("X:/gta5/build/dev/metadata/");



            MetaFile mf = new MetaFile("c:/Z_Z_MetaTest.meta", sd);

            ITunable test = RSG.Metadata.Util.Tunable.FindFirstStuctureNamed("aComponentData3", mf.Members);
            //Structure cpvstruct = sd["CPVComponentData"];

            IMember member = RSG.Metadata.Util.Member.FindStructMemberByName(mf.Definition.Members, "aComponentData3");


            (test as ArrayTunable).Insert(1, new StructureTunable(member as StructMember));
            //(test as ArrayTunable).Add(new StructureTunable(member as StructMember));
           
            

            mf.Serialise("c:/asdfasdj.meta");
             */
        }
    }
}
