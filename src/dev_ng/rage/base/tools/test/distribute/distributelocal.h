// 
// distribute/distributelocal.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef DISTRIBUTE_DISTRIBUTELOCAL_H 
#define DISTRIBUTE_DISTRIBUTELOCAL_H 

#include "distribute.h"

namespace rage 
{

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Distribute execution locally (invoking executables in parallel)
class DistributeLocal : public Distribute
{
public:

	// PURPOSE: Constructor
	DistributeLocal();

	// PURPOSE: Destructor
	virtual ~DistributeLocal();

	// PURPOSE: Implement schedule
	virtual void Schedule(const char* cmdline);

	// PURPOSE: Implement process
	virtual bool Process(float timeout=-1.f);

protected:


};

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // DISTRIBUTE_DISTRIBUTELOCAL_H 
