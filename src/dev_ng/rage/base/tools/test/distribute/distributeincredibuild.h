// 
// distribute/distributeincredibuild.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef DISTRIBUTE_DISTRIBUTEINCREDIBUILD_H 
#define DISTRIBUTE_DISTRIBUTEINCREDIBUILD_H 

#include "distribute.h"

namespace rage 
{

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Distribute execution via incredibuild
class DistributeIncredibuild : public Distribute
{
public:

	// PURPOSE: Constructor
	DistributeIncredibuild();

	// PURPOSE: Destructor
	virtual ~DistributeIncredibuild();

	// PURPOSE: Implement process
	virtual bool Process(float timeout=-1.f);

protected:

	u32 m_Magic;
	u32 m_Stamp;
};

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // DISTRIBUTE_DISTRIBUTEINCREDIBUILD_H 
