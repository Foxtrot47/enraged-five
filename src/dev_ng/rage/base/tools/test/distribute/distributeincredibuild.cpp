// 
// distribute/distributeincredibuild.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "distributeincredibuild.h"

#include "file/asset.h"
#include "file/stream.h"
#include "math/random.h"
#include "system/exec.h"
#include "system/timemgr.h"


namespace rage 
{

////////////////////////////////////////////////////////////////////////////////

DistributeIncredibuild::DistributeIncredibuild()
: m_Magic(0)
, m_Stamp(0)
{
	g_DrawRand.SetFullSeed(u64(sysTimer::GetTicks()));
	m_Magic = u32(g_DrawRand.GetInt());
}

////////////////////////////////////////////////////////////////////////////////

DistributeIncredibuild::~DistributeIncredibuild()
{
}

////////////////////////////////////////////////////////////////////////////////

bool DistributeIncredibuild::Process(float timeout)
{
	char magicBuf[16];
	sprintf(magicBuf, "%x", m_Magic);

	char stampBuf[16];
	sprintf(stampBuf, "%x", m_Stamp);
	m_Stamp++;

	atString batchFile;

	const int tempBufSize = RAGE_MAX_PATH;
	char tempBuf[tempBufSize];
	if(sysGetEnv("TEMP", tempBuf, tempBufSize))
	{
		batchFile = tempBuf;
		batchFile += '\\';
	}

	batchFile += "distribute";
	batchFile += '_';
	batchFile += magicBuf;
	batchFile += '_';
	batchFile += stampBuf;
		
	atString groupName;
	groupName = "distribute";
	groupName += stampBuf;

	fiStream* f = ASSET.Create(batchFile.c_str(), "bat");
	if(f)
	{
		fprintf(f, "@echo off\r\n");
		const int numPending = m_Pending.GetCount();
		for(int i=0; i<numPending; ++i)
		{
			fprintf(f, "xgsubmit.exe /group=%s /command ", groupName.c_str());
			fprintf(f, m_Pending[i].c_str());
			fprintf(f, "\r\n");
		}

		fprintf(f, "xgwait.exe /silent /group=%s", groupName.c_str());
		f->Close();

		atString cmdLine;
		if(timeout > 0.f)
		{
			char timeoutBuf[64];
			sprintf(timeoutBuf, "%d", int(timeout));

			cmdLine = "rageExecuteCommand.exe -timeOutInSeconds ";
			cmdLine += timeoutBuf;
			cmdLine += " # ";
		}
		cmdLine += "xgconsole.exe /avoidlocal=on /command=";
		cmdLine += batchFile;
		cmdLine += ".bat";

		int ret = sysExec(cmdLine.c_str());
		if(ret >= 0)
		{
			return true;
		}
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
