// 
// distribute/distribute.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef DISTRIBUTE_DISTRIBUTE_H 
#define DISTRIBUTE_DISTRIBUTE_H 

#include "atl/array.h"
#include "atl/string.h"

namespace rage 
{

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Distribute execution 
class Distribute
{
public:

	// PURPOSE: Constructor
	Distribute();

	// PURPOSE: Destructor
	virtual ~Distribute();

	// PURPOSE: Schedule a command line to be executed
	// NOTES: Multiple schedule calls can be made, 
	// but nothing is executed until process is called
	virtual void Schedule(const char* cmdline);

	// PURPOSE: Process all pending scheduled command lines
	virtual bool Process(float timeout=-1.f) = 0;

	// PURPOSE: Clear all pending scheduled command lines
	virtual void Clear();

protected:

	atArray<atString> m_Pending;
};

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // DISTRIBUTE_DISTRIBUTE_H 
