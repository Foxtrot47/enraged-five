﻿//---------------------------------------------------------------------------------------------
// <copyright file="AntlrErrorHandlerWrapper.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Antlr
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using Antlr4.Runtime;
    using RSG.Base.Language;

    /// <summary>
    /// ANTLR error handler wrapper; transforms ANTLRs error handling class into a
    /// shared IErrorHandler implementation.
    /// </summary>
    public class AntlrErrorHandlerWrapper : BaseErrorListener
    {
        #region Member Data
        /// <summary>
        /// Error handler object.
        /// </summary>
        private IErrorHandler _handler;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="handler"></param>
        public AntlrErrorHandlerWrapper(IErrorHandler handler)
        {
            if (null == handler)
                throw (new ArgumentNullException("handler"));
            this._handler = handler;
        }
        #endregion // Constructor(s)

        #region ANTLR BaseErrorListener Methods
        /// <summary>
        /// ANTLR BaseErrorListener handler.
        /// </summary>
        /// <param name="recognizer"></param>
        /// <param name="offendingSymbol"></param>
        /// <param name="line"></param>
        /// <param name="charPositionInLine"></param>
        /// <param name="msg"></param>
        /// <param name="e"></param>
        public override void SyntaxError(IRecognizer recognizer, IToken offendingSymbol, 
            int line, int charPositionInLine, String msg, RecognitionException e)
        {
            // Transform this ANTLR SyntaxError call into a call into the IErrorHandler
            // we were passed during construction.
            Debug.Assert(null != this._handler, "IErrorHandler is null!");
            if (null == this._handler)
                throw (new NotSupportedException("IErrorHandler is null!"));

            // Find offending line.
            CommonTokenStream tokens = (CommonTokenStream)recognizer.InputStream;
            String fullInputLine = tokens.TokenSource.InputStream.ToString();
            String[] inputLines = fullInputLine.Split('\n');
            String errorLine = inputLines[line - 1];

            ISymbol symbol = new AntlrSymbol(offendingSymbol);
            IPosition position = new AntlrPosition(offendingSymbol);

            AntlrSyntaxError syntaxError = new AntlrSyntaxError(errorLine, symbol,
                position, msg);
            this._handler.SyntaxError(syntaxError);
        }
        #endregion // ANTLR BaseErrorListener Methods
    }

} // RSG.Interop.Antlr namespace
