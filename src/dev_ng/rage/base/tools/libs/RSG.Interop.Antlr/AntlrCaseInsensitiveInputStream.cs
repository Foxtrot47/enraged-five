﻿//---------------------------------------------------------------------------------------------
// <copyright file="BooleanTerm.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Antlr
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Antlr4.Runtime;

    /// <summary>
    /// 
    /// </summary>
    /// <see cref="https://gist.github.com/sharwell/9424666"/>
    /// 
    public class AntlrCaseInsensitiveInputStream : AntlrInputStream
    {
        #region Member Data
        protected Char[] _lookaheadData;
        #endregion // Member Data

        #region Constructor(s)
        public AntlrCaseInsensitiveInputStream(String input)
            : base()
        { }

        /// <summary>
        /// 
        /// </summary>
        public AntlrCaseInsensitiveInputStream()
            : base()
        {
            _lookaheadData = 
        }

        //
        //
        // Exceptions:
        //   System.IO.IOException:
        public AntlrInputStream(Stream input);
        //
        // Summary:
        //     Copy data in string to a local char array
        public AntlrInputStream(string input);
        //
        //
        // Exceptions:
        //   System.IO.IOException:
        public AntlrInputStream(TextReader r);
        //
        // Summary:
        //     This is the preferred constructor for strings as no data is copied
        public AntlrInputStream(char[] data, int numberOfActualCharsInArray);
        //
        //
        // Exceptions:
        //   System.IO.IOException:
        public AntlrInputStream(Stream input, int initialSize);
        //
        //
        // Exceptions:
        //   System.IO.IOException:
        public AntlrInputStream(TextReader r, int initialSize);
        //
        //
        // Exceptions:
        //   System.IO.IOException:
        public AntlrInputStream(Stream input, int initialSize, int readChunkSize);
        //
        //
        // Exceptions:
        //   System.IO.IOException:
        public AntlrInputStream(TextReader r, int initialSize, int readChunkSize);
        #endregion // Constructor(s)
    }

} // RSG.Interop.Antlr namespace
