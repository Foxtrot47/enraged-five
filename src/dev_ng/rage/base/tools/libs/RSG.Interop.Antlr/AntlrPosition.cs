﻿//---------------------------------------------------------------------------------------------
// <copyright file="AntlrPosition.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Antlr
{
    using Antlr4.Runtime;
    using RSG.Base.Language;

    /// <summary>
    /// ANTLR position implementation; construct these from a 
    /// <see cref="Antlr4.Runtime.ParserRuleContext"/>.
    /// </summary>
    public class AntlrPosition : IPosition
    {
        #region Properties
        /// <summary>
        /// Start line number.
        /// </summary>
        public uint StartLine 
        { 
            get; 
            private set; 
        }

        /// <summary>
        /// Start column number.
        /// </summary>
        public uint StartColumn 
        { 
            get; 
            private set; 
        }

        /// <summary>
        /// End line number.
        /// </summary>
        public uint EndLine 
        { 
            get; 
            private set; 
        }

        /// <summary>
        /// End column number.
        /// </summary>
        public uint EndColumn 
        { 
            get; 
            private set; 
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="token"></param>
        public AntlrPosition(IToken token)
        {
            this.StartLine = (uint)token.Line;
            this.StartColumn = (uint)token.StartIndex;
            this.EndLine = (uint)token.Line;
            this.EndColumn = (uint)token.StopIndex;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="context"></param>
        public AntlrPosition(ParserRuleContext context)
        {
            this.StartLine = (uint)context.Start.Line;
            this.StartColumn = (uint)context.Start.StartIndex;
            this.EndLine = (uint)context.Stop.Line;
            this.EndColumn = (uint)context.Stop.StopIndex;
        }

        /// <summary>
        /// Copy constructor.
        /// </summary>
        /// <param name="position"></param>
        public AntlrPosition(IPosition position)
        {
            this.StartLine = position.StartLine;
            this.StartColumn = position.StartColumn;
            this.EndLine = position.EndLine;
            this.EndColumn = position.EndColumn;
        }
        #endregion // Constructor(s)
    }

} // RSG.Interop.Antlr namespace
