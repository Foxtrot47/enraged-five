﻿//---------------------------------------------------------------------------------------------
// <copyright file="AntlrSyntaxError.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Antlr
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using RSG.Base.Language;

    /// <summary>
    /// 
    /// </summary>
    public class AntlrSyntaxError : ISyntaxError
    {
        #region Properties
        /// <summary>
        /// Input stream line.
        /// </summary>
        public String InputLine { get; private set; }

        /// <summary>
        /// Current input symbol.
        /// </summary>
        public ISymbol Symbol { get; private set; }

        /// <summary>
        /// Position of syntax error (column within current line).
        /// </summary>
        /// <see cref="ISyntaxError.InputLine"/>
        public IPosition Position { get; private set; }

        /// <summary>
        /// Syntax error message.
        /// </summary>
        public String Message { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="inputLine"></param>
        /// <param name="sym"></param>
        /// <param name="pos"></param>
        /// <param name="msg"></param>
        public AntlrSyntaxError(String inputLine, ISymbol sym, IPosition pos, String msg)
        {
            this.InputLine = inputLine;
            this.Symbol = sym;
            this.Position = pos;
            this.Message = msg;
        }
        #endregion // Constructor(s)
    }

} // RSG.Interop.Antlr namespace
