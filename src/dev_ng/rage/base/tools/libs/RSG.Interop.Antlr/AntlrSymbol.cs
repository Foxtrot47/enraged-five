﻿//---------------------------------------------------------------------------------------------
// <copyright file="AntlrSymbol.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Antlr
{
    using System;
    using Antlr4.Runtime;
    using RSG.Base.Language;

    /// <summary>
    /// 
    /// </summary>
    public class AntlrSymbol : ISymbol
    {
        #region Properties
        /// <summary>
        /// Symbol text.
        /// </summary>
        public String Name
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="token"></param>
        public AntlrSymbol(IToken token)
        {
            this.Name = token.Text;
        }
        #endregion // Constructor(s)
    }

} // RSG.Interop.Antlr namespace
