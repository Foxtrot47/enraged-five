using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

using RSG.Base.Command;

namespace RageBuildTasks
{
    public class VisualStudioProjectParser
    {
        public VisualStudioProjectParser() { }

        private static Regex m_environmentVariableRegex = new Regex("\\x24\\x28(?<Variable>[^\\x29]+)\\x29", RegexOptions.Compiled);
        private const string m_VCProjExtension = ".VCPROJ";
        private const string m_CSProjExtension = ".CSPROJ";
        private const string m_VDProjExtension = ".VDPROJ";

        // PURPOSE: Determines the type of project (via extension) and determines the name of the startup project in the solution.
        // PARAMS: solution - The path of the solution to be parsed.
        // project - This can be null -- an override if a particular project needs to be found.
        // projectType - Returns the project type, in string form for now.
        // projectFilename - The path to the project that should be parsed.
        // obStatus - Success or failure context object.
        public static void DetermineProjectTypeAndFilename(string solution, string project, out string projectType, out string projectFilename, out rageStatus obStatus)
        {
            projectFilename = string.Empty;
            projectType = string.Empty;

            obStatus = new rageStatus();

            TextReader reader = null;
            try
            {
                reader = new StreamReader(solution);

                string solutionName = Path.GetFileNameWithoutExtension(solution);

                string line = reader.ReadLine();
                while (line != null)
                {
                    if (line.StartsWith("Project"))
                    {
                        string[] split = line.Trim().Split(new char[] { ',' });
                        if (split.Length >= 2)
                        {
                            // Try to find the project specified, or a project with the same name as the solution
                            if ((String.IsNullOrEmpty(project) && split[0].Contains(solutionName)) ||
                                (!String.IsNullOrEmpty(project) && split[0].Contains(project)))
                            {
                                projectFilename = split[1].Trim();
                                projectFilename = projectFilename.Trim(new char[] { '"' });
                                break;
                            }
                        }
                    }

                    line = reader.ReadLine();
                }

                reader.Close();
            }
            catch (Exception e)
            {
                if (reader != null)
                {
                    reader.Close();
                    reader = null;
                }

                obStatus = new rageStatus(e.Message);
                return;
            }

            if (String.IsNullOrEmpty(projectFilename) == false)
            {
                projectFilename = Path.GetFullPath(Path.Combine(Path.GetDirectoryName(solution), projectFilename));
                string extension = Path.GetExtension(projectFilename);

                if (!String.IsNullOrEmpty(extension))
                {
                    projectType = extension.ToUpper();
                }

                if (!File.Exists(projectFilename))
                {
                    obStatus = new rageStatus(String.Format("Unable to determine the output file for '{0}'.", solution));
                }
                else
                {
                    obStatus = new rageStatus();
                }
            }
            else
            {
                obStatus = new rageStatus();
            }
        }
        // PURPOSE: Parses the given project to determine if a configuration exists.
        // PARAMS: projectFilename - The project to be parsed.
        // projectType - the type of project to be parsed.  This is redundant for now.
        // configuration - The name of the configuration to search for.
        // RETURNS: True is the specified configuration exists.
        public static bool HasConfiguration(string projectFilename, string projectType, string configuration, out rageStatus obStatus)
        {
            if (String.Compare(projectType, m_VCProjExtension, true) == 0)
            {
                return VCProjHasConfiguration(projectFilename, configuration, out obStatus);
            }
            else if (String.Compare(projectType, m_CSProjExtension, true) == 0)
            {
                return CSProjHasConfiguration(projectFilename, configuration, out obStatus);
            }
            else if (String.Compare(projectType, m_VDProjExtension, true) == 0)
            {
                return VDProjHasConfiguration(projectFilename, configuration, out obStatus);
            }
            else
            {
                obStatus = new rageStatus( String.Format("{0} is an unknown project type!  Can not parse project {1} for configuration existence.", projectType, projectFilename) );
                return false;
            }
        }

        private static bool VCProjHasConfiguration(string projectFilename, string configuration, out rageStatus obStatus)
        {
            string outputFilename = null;
            string outputDir = null;
            string projectName = null;
            TextReader reader = null;
            try
            {
                reader = new StreamReader(projectFilename);

                string line = reader.ReadLine();
                bool inConfigurations = false;
                bool inConfiguration = false;
                bool inOurConfiguration = false;
                while (line != null)
                {
                    if (!inConfigurations)
                    {
                        if (line.Contains("Configurations"))
                        {
                            inConfigurations = true;
                        }
                        else if ((projectName == null) && line.Contains("Name="))
                        {
                            // the first Name= we run across is the project name
                            string[] split = line.Trim().Split(new char[] { '"' }, StringSplitOptions.RemoveEmptyEntries);
                            if (split.Length >= 2)
                            {
                                projectName = split[1];
                            }
                        }
                    }
                    else if (!inConfiguration)
                    {
                        if (line.Contains("/Configurations"))
                        {
                            inConfigurations = false;

                            if ((outputFilename != null) && (projectName != null) && (outputDir != null))
                            {
                                // we're done
                                break;
                            }
                        }
                        else if (line.Contains("Configuration"))
                        {
                            inConfiguration = true;
                        }
                    }
                    else
                    {
                        if (line.Contains("</Configuration>"))
                        {
                            inConfiguration = false;
                            inOurConfiguration = false;

                            if ((outputFilename != null) && (projectName != null) && (outputDir != null))
                            {
                                // we're done
                                break;
                            }
                        }
                        else if (!inOurConfiguration)
                        {
                            if (line.Contains(String.Format("Name=\"{0}\"", configuration)))
                            {
                                obStatus = new rageStatus();
                                return true;
                            }
                        }
                    }

                    line = reader.ReadLine();
                }

                reader.Close();
            }
            catch (Exception e)
            {
                if (reader != null)
                {
                    reader.Close();
                    reader = null;
                }

                obStatus = new rageStatus(e.Message);
                return false;
            }

            obStatus = new rageStatus(String.Format("Unable to find configuration {0} for project {1}.", configuration, projectFilename));
            return false;
        }

        private static bool CSProjHasConfiguration(string projectFilename, string configuration, out rageStatus obStatus)
        {
            obStatus = new rageStatus();

            // do a little remapping
            string strConfig = configuration.Replace( "Mixed Platforms", "AnyCPU" );

            TextReader reader = null;
            try
            {
                reader = new StreamReader( projectFilename );

                string line = reader.ReadLine();
                bool inDefaultPropertyGroup = false;
                bool inPropertyGroup = false;
                bool foundConfiguration = false;
                bool foundPlatform = false;

                while ( line != null )
                {
                    if ( !inDefaultPropertyGroup && !inPropertyGroup )
                    {
                        if ( line.Contains( "<PropertyGroup" ) )
                        {
                            if ( line.Contains( "Condition" )
                                && (line.Contains( strConfig ) || line.Contains( configuration.Replace( " ", string.Empty ) )) )
                            {
                                return true;
                            }
                            else
                            {
                                inDefaultPropertyGroup = true;
                            }
                        }
                    }
                    else if ( inDefaultPropertyGroup )
                    {
                        if ( line.Contains( "</PropertyGroup>" ) )
                        {
                            inDefaultPropertyGroup = false;
                        }
                        else if ( line.Contains( "Configuration" ) )
                        {
                            string[] split = line.Trim().Split( new char[] { '<', '>' }, StringSplitOptions.RemoveEmptyEntries );
                            if ( split.Length >= 2 )
                            {
                                if ( split[1].ToUpper().Contains( configuration ) )
                                {
                                    foundConfiguration = true;
                                }
                            }
                        }
                        else if ( line.Contains( "Platform" ) )
                        {
                            string[] split = line.Trim().Split( new char[] { '<', '>' }, StringSplitOptions.RemoveEmptyEntries );
                            if ( split.Length >= 2 )
                            {
                                if ( split[1].ToUpper().Contains( configuration ) )
                                {
                                    foundPlatform = true;
                                }
                            }
                        }

                        if(foundConfiguration == true && foundPlatform == true)
                        {
                            return true;
                        }
                    }
                    else if ( inPropertyGroup )
                    {
                        if ( line.Contains( "</PropertyGroup>" ) )
                        {
                            inDefaultPropertyGroup = false;
                            foundPlatform = false;
                            foundConfiguration = false;
                        }
                    }

                    line = reader.ReadLine();
                }

                reader.Close();
            }
            catch ( Exception e )
            {
                if ( reader != null )
                {
                    reader.Close();
                    reader = null;
                }

                obStatus = new rageStatus( e.Message );
                return false;
            }

            obStatus = new rageStatus(String.Format("Unable to find configuration {0} for project {1}.", configuration, projectFilename));
            return false;
        }

        private static bool VDProjHasConfiguration(string projectFilename, string configuration, out rageStatus obStatus)
        {
            obStatus = new rageStatus();

            // do a little remapping
            string strConfig = configuration.Replace( "Mixed Platforms", "AnyCPU" );

            TextReader reader = null;
            try
            {
                reader = new StreamReader( projectFilename );

                Stack<string> sections = new Stack<string>();
                string possibleSectionName = null;

                bool inConfigurations = false;
                bool inOurConfiguration = false;

                string line = reader.ReadLine();
                while ( line != null )
                {
                    if ( line.Trim().StartsWith( "\"" ) && !line.Contains( " = " ) )
                    {
                        possibleSectionName = line.Trim().Trim( new char[] { '"' } );
                    }
                    else if ( (line.Trim() == "{") && (possibleSectionName != null) )
                    {
                        sections.Push( possibleSectionName );

                        if ( !inConfigurations && (possibleSectionName == "Configurations") )
                        {
                            inConfigurations = true;
                        }
                        else if ( inConfigurations && !inOurConfiguration && (possibleSectionName == configuration) )
                        {
                            return true;
                        }

                        possibleSectionName = null;
                    }
                    else if ( (line.Trim() == "}") && (possibleSectionName == null) )
                    {
                        string section = sections.Pop();

                        if ( section == configuration )
                        {
                            inOurConfiguration = false;
                        }
                        else if ( section == "Configurations" )
                        {
                            inConfigurations = false;
                        }
                    }

                    line = reader.ReadLine();
                }

                reader.Close();
            }
            catch ( Exception e )
            {
                if ( reader != null )
                {
                    reader.Close();
                    reader = null;
                }

                obStatus = new rageStatus( e.Message );
                return false;
            }

            obStatus = new rageStatus(String.Format("Unable to find configuration {0} for project {1}.", configuration, projectFilename));
            return false;
         }

        public static string GetOutputFilename(string solution, string projectFilename, string configuration, ref string outputFilenameExtension, out rageStatus obStatus)
        {
            string outputFilename = null;
            string outputDir = null;
            string projectName = null;
            TextReader reader = null;
            try
            {
                reader = new StreamReader(projectFilename);

                string line = reader.ReadLine();
                bool inConfigurations = false;
                bool inConfiguration = false;
                bool inOurConfiguration = false;
                bool inTool = false;
                bool inOurTool = false;
                while (line != null)
                {
                    if (!inConfigurations)
                    {
                        if (line.Contains("Configurations"))
                        {
                            inConfigurations = true;
                        }
                        else if ((projectName == null) && line.Contains("Name="))
                        {
                            // the first Name= we run across is the project name
                            string[] split = line.Trim().Split(new char[] { '"' }, StringSplitOptions.RemoveEmptyEntries);
                            if (split.Length >= 2)
                            {
                                projectName = split[1];
                            }
                        }
                    }
                    else if (!inConfiguration)
                    {
                        if (line.Contains("/Configurations"))
                        {
                            inConfigurations = false;

                            if ((outputFilename != null) && (projectName != null) && (outputDir != null))
                            {
                                // we're done
                                break;
                            }
                        }
                        else if (line.Contains("Configuration"))
                        {
                            inConfiguration = true;
                        }
                    }
                    else
                    {
                        if (line.Contains("</Configuration>"))
                        {
                            inConfiguration = false;
                            inOurConfiguration = false;

                            if ((outputFilename != null) && (projectName != null) && (outputDir != null))
                            {
                                // we're done
                                break;
                            }
                        }
                        else if (!inOurConfiguration && !inTool && !inOurTool)
                        {
                            if (line.Contains(String.Format("Name=\"{0}\"", configuration)))
                            {
                                inOurConfiguration = true;
                            }
                        }
                        else if (!inTool)
                        {
                            if (line.Contains("<Tool"))
                            {
                                inTool = true;
                            }
                            else if (line.Contains("OutputDirectory="))
                            {
                                string[] split = line.Trim().Split(new char[] { '"' }, StringSplitOptions.RemoveEmptyEntries);
                                if (split.Length >= 2)
                                {
                                    outputDir = split[1];
                                }
                            }
                            else if (line.Contains("ConfigurationType="))
                            {
                                string[] split = line.Trim().Split(new char[] { '"' }, StringSplitOptions.RemoveEmptyEntries);
                                if (split.Length >= 2)
                                {
                                    string configType = split[1];
                                    switch (configType)
                                    {
                                        case "1":
                                            outputFilenameExtension = ".exe";
                                            break;
                                        case "2":
                                            outputFilenameExtension = ".dll";
                                            break;
                                        case "3":
                                            outputFilenameExtension = ".lib";
                                            break;
                                        case "4":
                                            outputFilenameExtension = ".lib";
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }
                        }
                        else if (!inOurTool)
                        {
                            if (line.Contains("Name="))
                            {
                                string[] split = line.Trim().Split(new char[] { '"' }, StringSplitOptions.RemoveEmptyEntries);
                                if (split.Length >= 2)
                                {
                                    if ((!configuration.Contains("360") && (split[1] == "VCLinkerTool"))
                                        || (split[1] == "VCX360ImageTool"))
                                    {
                                        inOurTool = true;
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (line.Contains("</Tool>"))
                            {
                                inTool = false;
                                inOurTool = false;
                            }
                            else if (line.Contains("OutputFile=") ||
                                (configuration.Contains("360") && line.Contains("OutputFileName=")))
                            {
                                string[] split = line.Trim().Split(new char[] { '"' }, StringSplitOptions.RemoveEmptyEntries);
                                if (split.Length >= 2)
                                {
                                    outputFilename = split[1];
                                }
                            }
                        }
                    }

                    line = reader.ReadLine();
                }

                reader.Close();
            }
            catch (Exception e)
            {
                if (reader != null)
                {
                    reader.Close();
                    reader = null;
                }

                obStatus = new rageStatus(e.Message);

                return string.Empty;
            }

            if (outputFilename == null)
            {
                if ((outputDir != null) && (projectFilename != null))
                {
                    outputFilename = Path.Combine(outputDir, Path.GetFileNameWithoutExtension(projectFilename));

                    if (outputFilenameExtension != null)
                    {
                        outputFilename = outputFilename + outputFilenameExtension;
                    }
                    else
                    {
                        outputFilename = outputFilename + ".exe";    // I hope it's an exe.
                    }
                }
                else
                {
                    obStatus = new rageStatus(String.Format("Unable to determine the output file name for {0}.", BuildLogTitle(solution, projectFilename, configuration, null)));
                    return string.Empty;
                }
            }

            if (projectName == null)
            {
                obStatus = new rageStatus(String.Format("Unable to determine the output project name for {0}.", BuildLogTitle(solution, projectFilename, configuration, null)));
                return string.Empty;
            }

            if (outputDir == null)
            {
                obStatus = new rageStatus(String.Format("Unable to determine the output directory for {0}.", BuildLogTitle(solution, projectFilename, configuration, null)));
                return string.Empty;
            }

            outputDir = ResolveEnvironmentVariables(outputDir, outputDir, projectName, projectFilename, solution, configuration, out obStatus);
            if (!obStatus.Success())
            {
                return string.Empty;
            }

            outputFilename = ResolveEnvironmentVariables(outputFilename, outputDir, projectName, projectFilename, solution, configuration, out obStatus);
            if (!obStatus.Success())
            {
                return string.Empty;
            }

            outputFilename = outputFilename.Replace('/', '\\');
            outputFilename = outputFilename.Replace("\\\\", "\\");
            outputFilename = outputFilename.Replace("&quot;", "");

            if (outputFilename.Contains("$(ConfigurationName)"))
            {
                string currentDirectory = Directory.GetCurrentDirectory();
                Directory.SetCurrentDirectory(Path.GetDirectoryName(projectFilename));

                string tempFilename = outputFilename.Replace("$(ConfigurationName)", configuration.Replace('|', '_'));
                if (!File.Exists(tempFilename))
                {
                    int indexOf = configuration.IndexOf('|');
                    tempFilename = outputFilename.Replace("$(ConfigurationName)", configuration.Substring(0, indexOf));
                    if (!File.Exists(tempFilename))
                    {
                        tempFilename = outputFilename.Replace("$(ConfigurationName)", configuration.Substring(indexOf + 1));
                    }
                }

                outputFilename = tempFilename;
                Directory.SetCurrentDirectory(currentDirectory);
            }

            if (outputFilename.Contains("$(PlatformName)"))
            {
                outputFilename = outputFilename.Replace("$(PlatformName)", GetPlatformName(configuration));
            }

            outputFilename = Path.GetFullPath(Path.Combine(Path.GetDirectoryName(projectFilename), outputFilename));

            if (configuration.ToUpper().Contains("PS3"))
            {
                // for PS3, deploy the fake-signed elf, if it exists
                string selfFilename = Path.ChangeExtension(outputFilename, ".self");
                if (File.Exists(selfFilename))
                {
                    outputFilename = selfFilename;
                }
            }

            return outputFilename;
        }

        protected static string BuildLogTitle(string part1, string part2, string part3, string part4)
        {
            StringBuilder strLogTitle = new StringBuilder(part1);

            if (!String.IsNullOrEmpty(part2))
            {
                strLogTitle.Append(" - ");
                strLogTitle.Append(part2);
            }

            if (!String.IsNullOrEmpty(part3))
            {
                strLogTitle.Append(" - ");
                strLogTitle.Append(part3);
            }

            if (!String.IsNullOrEmpty(part4))
            {
                strLogTitle.Append(" - ");
                strLogTitle.Append(part4);
            }

            return strLogTitle.ToString();
        }


        private static string ResolveEnvironmentVariables(string text, string outputDir, string projectName, string projectFilename, string solution, string configuration, out rageStatus obStatus)
        {
            obStatus = new rageStatus();

            string rtnText = text;

            Match m = m_environmentVariableRegex.Match(rtnText);
            while (m.Success)
            {
                string variable = m.Result("${Variable}");
                switch (variable)
                {
                    case "OutDir":
                        rtnText = rtnText.Replace("$(OutDir)", outputDir);
                        break;
                    case "ProjectName":
                        rtnText = rtnText.Replace("$(ProjectName)", projectName);
                        break;
                    case "ProjectDir":
                        rtnText = rtnText.Replace("$(ProjectDir)", Path.GetDirectoryName(projectFilename) + Path.DirectorySeparatorChar);
                        break;
                    case "SolutionDir":
                        rtnText = rtnText.Replace("$(SolutionDir)", Path.GetDirectoryName(solution) + Path.DirectorySeparatorChar);
                        break;
                    case "PlatformName":
                        rtnText = rtnText.Replace("$(PlatformName)", GetPlatformName(configuration));
                        break;
                    case "ConfigurationName":
                        // we'll handle this in the calling function
                        break;
                    default:
                        {
                            string environmentVariable = Environment.GetEnvironmentVariable(variable);
                            if (!String.IsNullOrEmpty(environmentVariable))
                            {
                                rtnText = rtnText.Replace("$(" + variable + ")", environmentVariable + Path.DirectorySeparatorChar);
                            }
                            else
                            {
                                obStatus = new rageStatus(String.Format("Unable to resolve environment variable '{0}'.", variable));

                                return string.Empty;
                            }
                        }
                        break;
                }

                m = m.NextMatch();
            }
            return rtnText;
        }

        public static string GetPlatformName(string configuration)
        {
            return configuration.Substring(configuration.IndexOf('|') + 1);
        }
    }
}
