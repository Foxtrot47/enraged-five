﻿using System;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Base.SCM;

namespace RageBuildTasks
{
    public class GeneralInfo : RageTaskBase
    {
        #region Properties
        public string Port
        {
            get
            {
                return ragePerforce.Port;
            }
            set
            {
                if (value != null)
                {
                    ragePerforce.Port = value;
                }
            }
        }

        public string Client
        {
            get
            {
                return ragePerforce.WorkspaceName;
            }
            set
            {
                if (value != null)
                {
                    ragePerforce.WorkspaceName = value;
                }
            }
        }

        public string User
        {
            get
            {
                return ragePerforce.UserName;
            }
            set
            {
                if (value != null)
                {
                    ragePerforce.UserName = value;
                }
            }
        }
        #endregion

        #region Overrides

        public override bool Execute()
        {
            return true;
        }

        #endregion
    }
}
