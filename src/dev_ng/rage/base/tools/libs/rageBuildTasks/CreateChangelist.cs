﻿using System;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Base.SCM;
using RSG.Base.Command;

namespace RageBuildTasks
{
    public class CreateChangelist : RageTaskBase
    {
        private string m_Description;

        #region Properties
        public string Description
        {
            get { return m_Description;  }
            set { m_Description = value; }
        }
        #endregion

        #region Overrides

        public override bool Execute()
        {
            rageStatus obStatus;
            ragePerforce.CreateChangelist(null, this.Description, out obStatus);

            if (obStatus.Success() == false)
            {
                return false;
            }

            return true;
        }

        #endregion
    }
}
