using System;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using System.Collections.Generic;
using System.IO;
using System.Text;

using RSG.Base.SCM;

namespace RageBuildTasks
{
    public class BuildEmailsConfigFile : RagePerforceTaskBase
    {
        #region variables
        private string m_filename;
        private string m_groupUsers;
        private string m_excludeUsers;
        #endregion

        #region Properties
        /// <summary>
        /// The name of the xml file to save.
        /// </summary>
        [Required]
        public string Filename
        {
            get
            {
                return m_filename;
            }
            set
            {
                m_filename = value;
            }
        }

        /// <summary>
        /// A semi-colon separated list of groups and their users in the following format:
        /// group1=user1,user2,user3;group2=user4,user5
        /// </summary>
        public string GroupUsers
        {
            get
            {
                return m_groupUsers;
            }
            set
            {
                m_groupUsers = value;
            }
        }

        /// <summary>
        /// A semi-colon separated list of users to exclude from the file.
        /// </summary>
        public string ExcludeUsers
        {
            get
            {
                return m_excludeUsers;
            }
            set
            {
                m_excludeUsers = value;
            }
        }

        private string m_northPerforcePort;
        private string m_northPerforceUser;
        private string m_northPerforceClient;

        public string NorthPort
        {
            get { return m_northPerforcePort; }
            set { m_northPerforcePort = value;}
        }

        public string NorthUser
        {
            get { return m_northPerforceUser; }
            set { m_northPerforceUser = value; }
        }

        public string NorthClient
        {
            get { return m_northPerforceClient; }
            set { m_northPerforceClient = value; }
        }
        #endregion

        #region Overrides
        public override bool Execute()
        {
            if ( String.IsNullOrEmpty( this.Filename ) )
            {
                this.Log.LogError( "No Email Config filename was specified." );
                return false;
            }

            // make sure we have the full path
            this.Filename = Path.GetFullPath( this.Filename );

            ragePerforce.EchoToConsole = false;

            ragePerforce.ragePerforceUser[] obUsers = ragePerforce.GetUsers();
            if ( obUsers == null )
            {
                this.Log.LogError( "There was an error retrieving the list of Perforce users." );
                return false;
            }

            List<ragePerforce.ragePerforceUser> usersList = new List<ragePerforce.ragePerforceUser>();
            usersList.AddRange(obUsers);
            
            //TODO:  There must be a better way to handle a list of ports, clients and users to generate
            //the e-mail list from.  Refactoring this code would be nice.
            //
            if ( String.IsNullOrEmpty(NorthPort) == false 
                && String.IsNullOrEmpty(NorthClient) == false
                && String.IsNullOrEmpty(NorthUser) == false )
            {
                //Generate the list of users based on Rockstar North.
                ragePerforce.Port = NorthPort;
                ragePerforce.WorkspaceName = NorthClient;
                ragePerforce.UserName = NorthUser;

                ragePerforce.ragePerforceUser[] obNorthUsers = ragePerforce.GetUsers();
                if (obNorthUsers == null)
                {
                    //Do not catalog an error just yet.
                    return true;
                }

                usersList.AddRange(obNorthUsers);
            }

            if (WriteEmailConfigFile(usersList.ToArray()) == false)
                return false;

            return true;
        }
        #endregion

        private bool WriteEmailConfigFile(ragePerforce.ragePerforceUser[] obUsers)
        {
            Dictionary<string, string> usersInGroups = new Dictionary<string, string>();
            if (!String.IsNullOrEmpty(this.GroupUsers))
            {
                string[] strGroupUsers = this.GroupUsers.Split(new char[] { Path.PathSeparator }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string strGroupUser in strGroupUsers)
                {
                    string[] strGroupUsersSplit = strGroupUser.Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries);
                    if (strGroupUsersSplit.Length == 2)
                    {
                        string strGroup = strGroupUsersSplit[0].Trim();

                        string[] strUsers = strGroupUsersSplit[1].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string strUser in strUsers)
                        {
                            usersInGroups[strUser.Trim().ToUpper()] = strGroup;
                        }
                    }
                }
            }

            Dictionary<string, string> excludedUsers = new Dictionary<string, string>();
            if (!String.IsNullOrEmpty(this.ExcludeUsers))
            {
                string[] strUsernames = this.ExcludeUsers.Split(new char[] { Path.PathSeparator }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string strUsername in strUsernames)
                {
                    excludedUsers[strUsername.Trim().ToUpper()] = strUsername.Trim();
                }
            }

            this.Log.LogMessage(MessageImportance.High, "Creating {0}", this.Filename);

            TextWriter obWriter = null;
            try
            {
                obWriter = new StreamWriter(this.Filename);

                List<string> activeUsers = new List<string>();
                foreach (ragePerforce.ragePerforceUser obUser in obUsers)
                {
                    if (!excludedUsers.ContainsKey(obUser.Username.ToUpper()))
                    {
                        string strGroup;
                        if (usersInGroups.TryGetValue(obUser.Username.ToUpper(), out strGroup))
                        {
                            obWriter.WriteLine("<user name=\"{0}\" group=\"{1}\" address=\"{2}\" />", obUser.Username, strGroup, obUser.EmailAddress);
                        }
                        else
                        {
                            bool found = false;
                            foreach (string user in activeUsers)
                            {
                                if (String.Compare(user, obUser.Username, true) == 0)
                                {
                                    found = true;
                                    break;
                                }
                            }

                            if (found == false)
                            {
                                activeUsers.Add(obUser.Username);
                                obWriter.WriteLine("<user name=\"{0}\" address=\"{1}\" />", obUser.Username, obUser.EmailAddress);
                            }
                        }
                    }
                }

                obWriter.Close();
            }
            catch (Exception e)
            {
                if (obWriter != null)
                {
                    obWriter.Close();
                }

                this.Log.LogError(e.Message);
                return false;
            }

            return true;
        }
    }
}
