using System;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using System.Collections.Generic;
using System.IO;
using System.Text;

using RSG.Base.Command;
using RSG.Base.IO;

namespace RageBuildTasks
{
    public class DeleteOldFolders : Task
    {
        #region Variables
        private string m_sourceDirectroy;
        private string m_searchPattern;
        private int m_numberOfFoldersToKeep;
        #endregion

        #region Properties
        /// <summary>
        /// The directory containing the folders we want deleted.
        /// </summary>
        [Required]
        public string SourceDirectory
        {
            get
            {
                return m_sourceDirectroy;
            }
            set
            {
                m_sourceDirectroy = value;
            }
        }

        /// <summary>
        /// The name or pattern of folders to be deleted.
        /// </summary>
        public string SearchPattern
        {
            get
            {
                return m_searchPattern;
            }
            set
            {
                m_searchPattern = value;
            }
        }

        /// <summary>
        /// The number of folder want to keep.  If this is 0, then all folders matching the search pattern will be deleted.
        /// </summary>
        public int NumberOfFoldersToKeep
        {
            get
            {
                return m_numberOfFoldersToKeep;
            }
            set
            {
                m_numberOfFoldersToKeep = value;
            }
        }
        #endregion

        #region Overrides
        public override bool Execute()
        {
            if ( String.IsNullOrEmpty( this.SourceDirectory ) )
            {
                this.Log.LogError( "SourceDirectory was not specified." );
                return false;
            }

            // Get the full path, just in case
            this.SourceDirectory = Path.GetFullPath( this.SourceDirectory );

            List<string> directories = new List<string>();

            try
            {
                if ( String.IsNullOrEmpty( this.SearchPattern ) )
                {
                    directories.AddRange( Directory.GetDirectories( this.SourceDirectory ) );
                }
                else
                {
                    directories.AddRange( Directory.GetDirectories( this.SourceDirectory, this.SearchPattern ) );
                }
            }
            catch ( Exception e )
            {
                this.Log.LogWarning( e.Message );
                return true;
            }

            directories.Sort( CompareFolderTimestamps );

            if ( this.NumberOfFoldersToKeep <= 0 )
            {
                this.Log.LogMessage( MessageImportance.High, "Deleting all folders in {0}{1}", this.SourceDirectory,
                    !String.IsNullOrEmpty( this.SearchPattern ) ? " matching pattern " + this.SearchPattern : string.Empty );
                foreach ( string directory in directories )
                {
                    rageStatus obStatus;
                    rageFileUtilities.DeleteLocalFolder( directory, out obStatus );
                    if ( !obStatus.Success() )
                    {
                        this.Log.LogWarning( obStatus.ErrorString );
                    }
                    else
                    {
                        this.Log.LogMessage( MessageImportance.Normal, "Deleted {0}", directory );
                    }
                }
            }
            else
            {
                int numToDelete = directories.Count - this.NumberOfFoldersToKeep;
                if ( numToDelete <= 0 )
                {
                    this.Log.LogMessage( MessageImportance.High, "Less than {0} directories in {1}{2}.  Nothing to delete.", 
                        this.NumberOfFoldersToKeep, this.SourceDirectory,
                        !String.IsNullOrEmpty( this.SearchPattern ) ? " matching pattern " + this.SearchPattern : string.Empty );
                }
                else
                {
                    this.Log.LogMessage( MessageImportance.High, "Deleting the last {0} folders in {1}{2}", numToDelete, this.SourceDirectory,
                        !String.IsNullOrEmpty( this.SearchPattern ) ? " matching pattern " + this.SearchPattern : string.Empty );

                    for ( int i = 0; i < numToDelete; ++i )
                    {
                        rageStatus obStatus;
                        rageFileUtilities.DeleteLocalFolder( directories[i], out obStatus );
                        if ( !obStatus.Success() )
                        {
                            this.Log.LogWarning( obStatus.ErrorString );
                        }
                        else
                        {
                            this.Log.LogMessage( MessageImportance.Normal, "Deleted {0}", directories[i] );
                        }
                    }
                }
            }

            return true;
        }
        #endregion

        #region Private Functions
        private static int CompareFolderTimestamps( string x, string y )
        {
            if ( x == null )
            {
                if ( y == null )
                {
                    // If x is null and y is null, they're
                    // equal. 
                    return 0;
                }
                else
                {
                    // If x is null and y is not null, y
                    // is greater. 
                    return -1;
                }
            }
            else
            {
                // If x is not null...
                //
                if ( y == null )
                // ...and y is null, x is greater.
                {
                    return 1;
                }
                else
                {
                    // ...and y is not null, compare the 
                    // time of the folders.
                    //
                    DateTime dtX = Directory.GetLastWriteTime( x );
                    DateTime dtY = Directory.GetLastWriteTime( y );

                    // sort them with ordinary DateTime comparison.
                    //
                    return dtX.CompareTo( dtY );
                }
            }
        }
        #endregion
    }
}
