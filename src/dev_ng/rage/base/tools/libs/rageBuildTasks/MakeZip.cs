using System;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;

using RSG.Base;
using RSG.Base.Command;
using RSG.Base.IO;

namespace RageBuildTasks
{
    public class MakeZip : RageTaskBase
    {
        #region Variables
        private string m_zipFilename;
        private string m_sourceDirectory;
        private string m_workingDirectory;
        private string m_excludeFileExtensions;
        private string m_deleteDirectoryPattern;
        #endregion

        #region Properties
        /// <summary>
        /// The path and file name of the zip archive to create.  If the path is relative, it is assumed to be
        /// relative to the source directory.
        /// </summary>
        [Required]
        public string ZipFilename
        {
            get
            {
                return m_zipFilename;
            }
            set
            {
                m_zipFilename = value;
            }
        }

        /// <summary>
        /// The path of the directory from which to create the archive.  This is relative to the <see cref="WorkingDirectory"/>
        /// if that has been specified.  The paths in the archive will be relative to this directory.
        /// </summary>
        [Required]
        public string SourceDirectory
        {
            get
            {
                return m_sourceDirectory;
            }
            set
            {
                m_sourceDirectory = value;
            }
        }

        /// <summary>
        /// The directory to start zip.exe from.  This must be specified if <see cref="SourceDirectory"/> is intended to be
        /// a relative path for everything in the archive.
        /// </summary>
        public string WorkingDirectory
        {
            get
            {
                return m_workingDirectory;
            }
            set
            {
                m_workingDirectory = value;
            }
        }

        /// <summary>
        /// An optional list of file extensions to exclude from the archive.
        /// </summary>
        public string ExcludeFileExtensions
        {
            get
            {
                return m_excludeFileExtensions;
            }
            set
            {
                m_excludeFileExtensions = value;
            }
        }

        /// <summary>
        /// An optional pattern, send to <see cref="Directory.Directories"/>, used when the destination disc
        /// is full and we need to delete the oldest matching directory.  The pattern will always be used on
        /// the parent directory of <see cref="ZipFilename"/>'s directory.
        /// </summary>
        public string DeleteDirectoryPattern
        {
            get
            {
                return m_deleteDirectoryPattern;
            }
            set
            {
                m_deleteDirectoryPattern = value;
            }
        }
        #endregion

        #region Overrides
        public override bool Execute()
        {
            if ( String.IsNullOrEmpty( this.ZipFilename ) || String.IsNullOrEmpty( this.SourceDirectory ) )
            {
                return false;
            }

            // make sure we have full paths for the zip filename and the working directory
            if ( !String.IsNullOrEmpty( this.WorkingDirectory ) && !Path.IsPathRooted( this.WorkingDirectory ) )
            {
                this.WorkingDirectory = Path.GetFullPath( this.WorkingDirectory );
            }

            if ( !Path.IsPathRooted( this.ZipFilename ) )
            {
                this.ZipFilename = Path.GetFullPath( Path.Combine( this.SourceDirectory, this.ZipFilename ) );
            }

            string strZipLog;
            rageStatus obStatus;
            BuildZip( out strZipLog, out obStatus );
            if ( !obStatus.Success() )
            {
                return false;
            }

            // clean up
            if ( !ShouldKeepBuildLog() && !String.IsNullOrEmpty( strZipLog ) && File.Exists( strZipLog ) )
            {
                File.Delete( strZipLog );
            }

            return true;
        }
        #endregion

        #region Private Functions
        protected void BuildZip( out string strZipLog, out rageStatus obStatus )
        {
            List<string> astrErrorsAsText = new List<string>();
            List<string> astrLogAsHtml = new List<string>();

            string strZipFilename = this.ZipFilename;

            // if we're saving to a network path, let's build to a temporary then copy it up.
            if ( strZipFilename.StartsWith( "//" ) )
            {
                strZipFilename = Path.Combine( Environment.GetEnvironmentVariable( "TEMP" ), Path.GetFileName( strZipFilename ) );
            }

            // make sure our destination exists
            rageFileUtilities.MakeDir( Path.GetDirectoryName( strZipFilename ) );

            string strDosCommand = string.Empty;
            while ( true )
            {
                astrErrorsAsText.Clear();
                astrLogAsHtml.Clear();

                // remove the old zip, so we don't add it to
                rageFileUtilities.DeleteLocalFile( strZipFilename, out obStatus );
                if ( !obStatus.Success() )
                {
                    this.Log.LogError( obStatus.ErrorString );
                    astrErrorsAsText.Add( obStatus.ErrorString );
                    break;
                }

                // Zip the tools from perforce up to the temp folder
                rageExecuteCommand obCommand = new rageExecuteCommand();
                obCommand.Command = "zip.exe";

                obCommand.Arguments = String.Format( "-r -S \"{0}\" \"{1}\"", strZipFilename, this.SourceDirectory );
                if ( !String.IsNullOrEmpty( this.ExcludeFileExtensions ) )
                {
                    obCommand.Arguments = String.Format( "{0} -x {1}", obCommand.Arguments, this.ExcludeFileExtensions );
                }

                if ( !String.IsNullOrEmpty( this.WorkingDirectory ) )
                {
                    obCommand.WorkingDirectory = this.WorkingDirectory;
                }

                obCommand.LogFilename = Path.Combine( Environment.GetEnvironmentVariable( "TEMP" ),
                    BuildLogFileName(GetMsBuildEnvironmentVariable("CCNetProject"), Path.GetFileNameWithoutExtension(this.ZipFilename), "zip", null, null));
                obCommand.UseBusySpinner = false;
                obCommand.RemoveLog = true;

                strDosCommand = obCommand.GetDosCommand();
                this.Log.LogMessage( MessageImportance.High, strDosCommand );

                obStatus = new rageStatus();
                obCommand.Execute( out obStatus );

                astrLogAsHtml.Add( obCommand.GetLogAsHtmlString() );

                if ( !obStatus.Success() )
                {
                    bool noSpaceAvailable = false;
                    foreach ( string strLogLine in obCommand.GetLogAsArray() )
                    {
                        if ( !noSpaceAvailable && strLogLine.Contains( "zip I/O error: No space left on device" ) )
                        {
                            noSpaceAvailable = true;
                            this.Log.LogMessage( MessageImportance.Normal, strLogLine );
                        }
                        else if ( strLogLine.Contains( " error: " ) )
                        {
                            this.Log.LogError( strLogLine );
                            astrErrorsAsText.Add( strLogLine );
                        }
                        else if ( strLogLine.Contains( " warning: " ) )
                        {
                            this.Log.LogWarning( strLogLine );
                        }
                        else
                        {
                            this.Log.LogMessage( MessageImportance.Low, strLogLine );
                        }
                    }

                    if ( noSpaceAvailable )
                    {
                        if ( !String.IsNullOrEmpty( this.DeleteDirectoryPattern ) )
                        {
                            // delete an old set of zips and retry
                            MakeSpace( strZipFilename, out obStatus );
                            if ( !obStatus.Success() )
                            {
                                break;
                            }
                        }
                        else
                        {
                            string strLogText = "The volume is full.";

                            this.Log.LogError( strLogText );
                            astrErrorsAsText.Add( strLogText );
                            obStatus = new rageStatus( strLogText );
                            break;
                        }
                    }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }

            // now copy to the final destination, if needed
            if ( (strZipFilename != this.ZipFilename) && File.Exists( strZipFilename ) )
            {
                this.Log.LogMessage( MessageImportance.Normal, "Copying {0} to {1}", strZipFilename, this.ZipFilename );

                rageStatus obFileStatus;

                while ( true )
                {
                    rageFileUtilities.CopyFile( strZipFilename, this.ZipFilename, out obFileStatus );
                    if ( !obStatus.Success() )
                    {
                        if ( obStatus.ErrorString.ToUpper().Contains( "NO SPACE ON DISK" )
                            || obStatus.ErrorString.ToUpper().Contains( "NOT ENOUGH SPACE ON DISK" ) )
                        {
                            if ( !String.IsNullOrEmpty( this.DeleteDirectoryPattern ) )
                            {
                                // delete an old set of zips and retry
                                MakeSpace( strZipFilename, out obStatus );
                                if ( !obStatus.Success() )
                                {
                                    astrErrorsAsText.Add( obStatus.ErrorString );
                                    break;
                                }
                            }
                            else
                            {
                                string strLogText = "The volume is full.";

                                this.Log.LogError( strLogText );
                                astrErrorsAsText.Add( strLogText );
                                obStatus = new rageStatus( strLogText );
                                break;
                            }
                        }
                    }
                    else
                    {
                        break;
                    }
                }

                // delete the temporary
                rageFileUtilities.DeleteLocalFile( strZipFilename, out obFileStatus );
            }

            if ( !String.IsNullOrEmpty( this.LogDirectory ) )
            {
                strZipLog = Path.Combine( this.LogDirectory,
                    BuildLogFileName(GetMsBuildEnvironmentVariable("CCNetProject"), Path.GetFileNameWithoutExtension(this.ZipFilename), "zip", null, null));
            }
            else
            {
                strZipLog = Path.Combine( Environment.GetEnvironmentVariable( "TEMP" ),
                    BuildLogFileName(GetMsBuildEnvironmentVariable("CCNetProject"), Path.GetFileNameWithoutExtension(this.ZipFilename), "zip", null, null));
            }

            if ( astrErrorsAsText.Count == 0 )
            {
                this.Log.LogMessage( MessageImportance.High, "Zip Complete." );
            }
            else
            {
                this.Log.LogMessage( MessageImportance.High, "{0} errors found.  Unable to create Zip Log.", astrErrorsAsText.Count );
            }
        }

        protected void MakeSpace( string strZipFilename, out rageStatus obStatus )
        {
            // delete an old set of zips and retry
            string directory = Path.GetDirectoryName( strZipFilename );
            string parentDirectory = Path.GetFullPath( Path.Combine( directory, ".." ) );

            string[] folders = Directory.GetDirectories( parentDirectory, this.DeleteDirectoryPattern );
            List<string> folderList = new List<string>( folders );
            folderList.Sort();

            if ( folderList.Count > 0 )
            {
                this.Log.LogWarning( "The volume is full.  Deleting old archive {0}", folderList[0] );
                obStatus = new rageStatus();

                rageFileUtilities.DeleteLocalFolder( folderList[0], out obStatus );

                if ( !obStatus.Success() )
                {
                    this.Log.LogWarning( obStatus.ErrorString );
                    obStatus = new rageStatus();
                }
            }
            else
            {
                string strLogText = "The volume is full.  Could not locate any archives to delete.";

                this.Log.LogError( strLogText );
                obStatus = new rageStatus( strLogText );
            }
        }
        #endregion
    }
}
