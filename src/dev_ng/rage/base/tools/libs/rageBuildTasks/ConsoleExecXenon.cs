using System;
using System.Diagnostics;
using XDevkit;
using System.Threading;

using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;

namespace RageBuildTasks
{
    //-------------------------------------------------------------------------
    // Name: DebugManager
    // Desc: The DebugManager class is the main application class for
    //       C# xbWatson. It connects to the default Xbox, waits
    //       for notifications, and then handles those notifications.
    //-------------------------------------------------------------------------
    public class ConsoleExecXenon : Task
    {
        // m_xManager gives you access to one or more devkits, including letting7
        // you add to the list of available consoles.
        private XDevkit.XboxManagerClass m_xManager;
        private XDevkit.XboxConsole m_xboxConsole;
        private XDevkit.IXboxDebugTarget m_xDebugTarget;

        private String m_cmdLine;
        private String m_mediaDirectory = "";
        private String m_xboxTarget = "";

        // Are we connected to an Xbox devkit?
        private bool m_fConnected;
        private bool m_exited;
        //private bool rebootFinished;
        private bool m_ignoreErrors;
        private bool m_failOnError = false;
        private bool m_coldReboot = false;
        private int m_timeoutMS = 30000; //milliseconds

        public bool TaskFinished
        {
            get { return m_exited; }
        }

        //---------------------------------------------------------------------
        // Name: Constructor
        //---------------------------------------------------------------------
        public ConsoleExecXenon()
        {
            m_fConnected = false;
            m_exited = false;
            m_xManager = new XboxManagerClass();
            // Open the default Xbox. This will fail if there is no default
            // Xbox or if it is unreachable for any reason.
            m_xboxConsole = m_xManager.OpenConsole( m_xboxTarget == "" ? m_xManager.DefaultConsole : m_xboxTarget );
            //rebootFinished = false;
            m_ignoreErrors = false;
        }

        //---------------------------------------------------------------------
        // Name: InitDM
        // Desc: Request notifications from the Xbox devkit
        //---------------------------------------------------------------------
        public void InitDM()
        {
            // Get the Xbox we are talking to
            try
            {
                m_exited = false;
                m_ignoreErrors = false;

                ProcessStartInfo psInfo = new ProcessStartInfo( Environment.GetEnvironmentVariable( "XEDK" ) + "\\bin\\win32\\xbreboot.exe",
                    (!String.IsNullOrEmpty( this.Target ) ? "/x " + this.Target + " " : string.Empty) + this.XbrebootCommandLine );
                psInfo.CreateNoWindow = true;
                psInfo.UseShellExecute = false;

                Process p = Process.Start( psInfo );
                if ( p != null )
                {
                    while ( !p.HasExited )
                    {
                        // do nothing
                    }
                }

                // Hook up Notification Channel - tell the Xbox devkit to send
                // notifications to us. The set of notifications is documented under
                // DmNotify in the help file.
                m_xboxConsole.OnStdNotify += new XboxEvents_OnStdNotifyEventHandler( xboxConsole_OnStdNotify );
                m_xDebugTarget = m_xboxConsole.DebugTarget;
                m_xDebugTarget.ConnectAsDebugger( null, XboxDebugConnectFlags.Force );
            }
            catch ( Exception e )
            {
                // Display an error message if connecting fails. Translating
                // error codes to text is not currently supported.
                m_xboxConsole.OnStdNotify -= new XboxEvents_OnStdNotifyEventHandler( xboxConsole_OnStdNotify );
                this.Log.LogMessage( MessageImportance.High, "Could not connect to Xbox\n " + e );
            }
        }


        //---------------------------------------------------------------------
        // Name: xboxConsole_OnStdNotify
        // Desc: Callback for DmNotify style notifications
        //---------------------------------------------------------------------
        private void xboxConsole_OnStdNotify( XboxDebugEventType eventCode, IXboxEventInfo eventInformation )
        {
            bool fStopped = eventInformation.Info.IsThreadStopped == 0 ? false : true;
            bool notStopped, hasException = true;

            switch ( eventCode )
            {
                // Handler for DM_EXEC changes, mainly for detecting reboots
                case XDevkit.XboxDebugEventType.ExecStateChange:
                    {
                        this.Log.LogMessage( MessageImportance.Low, "Code" + eventInformation.Info.ExecState );

                        if ( !m_fConnected )
                        {
                            m_fConnected = true;
                            this.Log.LogMessage( MessageImportance.Low, "xbWatson: Connection to Xbox successful\n" );
                            break;
                        }
                        else if ( eventInformation.Info.ExecState == XDevkit.XboxExecutionState.Rebooting )
                        {
                            this.Log.LogMessage( MessageImportance.Low, "xbWatson; Xbox is restarting\n" );
                            m_exited = true;
                        }
                        else
                        {
                            this.Log.LogMessage( MessageImportance.Low, "it's doing something\n" );

                            if ( (eventInformation.Info.ExecState != XboxExecutionState.PendingTitle)
                                && (eventInformation.Info.ExecState != XboxExecutionState.Running) )
                            {
                                m_exited = true;
                            }
                        }

                        if ( m_exited && (eventInformation.Info.ExecState == XboxExecutionState.RebootingTitle) )
                        {
                            this.Log.LogMessage( MessageImportance.Low, "xbWatson; Xbox has restarted\n" );
                        }
                    }
                    break;

                // Handler for DM_DEBUGSTR notifications, to display debug print text.
                case XDevkit.XboxDebugEventType.DebugString:
                    {
                        string[] messages = eventInformation.Info.Message.Split( "\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                        foreach ( string str in messages )
                        {
                            if ( !this.FailOnError && str.Contains( "***  TESTER IGNORE REST OF ERRORS ***" ) )
                            {
                                m_ignoreErrors = true;
                                this.Log.LogMessage( MessageImportance.High, "Ignoring all future errors as test is complete" );

                                m_exited = true;
                            }

                            if (str.Contains("** THE GAME HAS CRASHED **") == true)
                            {
                                this.Log.LogMessage(MessageImportance.High, "The game has crashed.  Exiting...");
                                m_exited = true;
                                break;
                            }

                            if ( str != "\n" )
                            {
                                bool bLogFatalError = false;
                                bool bLogError = false;
                                bool bLogWarning = false;

                                string strUpper = str.ToUpper();
                                strUpper = strUpper.Trim();

                                if ( strUpper.Contains( "): FATAL ERROR : " ) || strUpper.StartsWith( "FATAL ERROR: " ) )
                                {
                                    bLogFatalError = true;
                                }
                                if ( strUpper.Contains( "): ERROR : " ) || strUpper.StartsWith( "ERROR: " ) )
                                {
                                    bLogError = true;
                                }
                                if ( strUpper.Contains( "): WARNING : " ) || strUpper.StartsWith( "WARNING: " ) )
                                {
                                    bLogWarning = true;
                                }

                                if ( !m_ignoreErrors )
                                {
                                    if ( bLogFatalError || bLogError )
                                    {
                                        this.Log.LogError( str );
                                    }
                                    else if ( bLogWarning )
                                    {
                                        this.Log.LogWarning( str );
                                    }
                                    else
                                    {
                                        this.Log.LogMessage( MessageImportance.Normal, str );
                                    }

                                    if ( bLogFatalError && !this.XbrebootCommandLine.Contains( "-noquits" ) )
                                    {
                                        m_exited = true;
                                        break;
                                    }
                                }
                                else
                                {
                                    if ( bLogFatalError || bLogError )
                                    {
                                        this.Log.LogWarning( "Error treated as warning: {0}", str );
                                    }
                                    else if ( bLogWarning )
                                    {
                                        this.Log.LogWarning( str );
                                    }
                                    else
                                    {
                                        this.Log.LogMessage( MessageImportance.Normal, str );
                                    }
                                }
                            }
                        }

                        if ( fStopped )
                        {
                            eventInformation.Info.Thread.Continue( hasException );
                            m_xDebugTarget.Go( out notStopped );
                        }
                    }
                    break;

                // Handler for asserts triggered on the Xbox
                case XDevkit.XboxDebugEventType.AssertionFailed:
                    {
                        if ( !m_ignoreErrors )
                        {
                            this.Log.LogError( "Assertion failed: {0}", eventInformation.Info.Message );
                        }
                        else
                        {
                            this.Log.LogWarning( "Errors treated as warnings: Assertion failed: {0}", eventInformation.Info.Message );
                        }

                        m_exited = true;
                    }
                    break;

                // Handler for fatal errors - RIPs in the Xbox OS
                case XDevkit.XboxDebugEventType.RIP:
                    {
                        if ( !m_ignoreErrors )
                        {
                            this.Log.LogError( "RIP : " + eventInformation.Info.Message );
                        }
                        else
                        {
                            this.Log.LogWarning( "Errors treated as warnings: RIP : {0}", eventInformation.Info.Message );
                        }

                        m_exited = fStopped;
                    }
                    break;

                // Handler for a breakpoint - DM_BREAK
                case XDevkit.XboxDebugEventType.ExecutionBreak:
                    {
                        if ( !m_ignoreErrors )
                        {
                            this.Log.LogError( "Break: {0} {1}", 
                                eventInformation.Info.Address, eventInformation.Info.Thread.ThreadId );
                        }
                        else
                        {
                            this.Log.LogWarning( "Errors treated as warnings: Break: {0} {1}", 
                                eventInformation.Info.Address, eventInformation.Info.Thread.ThreadId );
                        }
                    }
                    break;

                // Handler for a data breakpoint - DM_DATABREAK
                case XDevkit.XboxDebugEventType.DataBreak:
                    {
                        if ( !m_ignoreErrors )
                        {
                            this.Log.LogError( "Databreak : {0} {1} {2} {3}",  
                                eventInformation.Info.Address, eventInformation.Info.Thread.ThreadId,
                                eventInformation.Info.GetType(), eventInformation.Info.Address );
                        }
                        else
                        {
                            this.Log.LogWarning( "Errors treated as warnings: Databreak : {0} {1} {2} {3}",
                                eventInformation.Info.Address, eventInformation.Info.Thread.ThreadId,
                                eventInformation.Info.GetType(), eventInformation.Info.Address );
                        }

                        m_exited = true;
                    }
                    break;

                // Handler for an exception (access violation, etc.) - DM_EXCEPTION
                case XDevkit.XboxDebugEventType.Exception:
                    {
                        if ( eventInformation.Info.Flags == XDevkit.XboxExceptionFlags.FirstChance )
                        {
                            /*  Log.LogError("First Chance Exception : Thread Id " + eventInformation.Info.Thread.ThreadId +
                                      " Code " + eventInformation.Info.Code + " Address " +
                                      eventInformation.Info.Address + " Flags " +
                                      eventInformation.Info.Flags + "\n");
                              */
                            // At this point we could check for more what type of exception was
                            // reported and display more information.
                        }
                        else
                        {
                            if ( !m_ignoreErrors )
                            {
                                this.Log.LogError( "Exception : {0} {1} {2} {3}", 
                                    eventInformation.Info.Thread.ThreadId, eventInformation.Info.Code, 
                                    eventInformation.Info.Address, eventInformation.Info.Flags );
                            }
                            else
                            {
                                this.Log.LogWarning( "Errors treated as warnings: Exception : {0} {1} {2} {3}", 
                                    eventInformation.Info.Thread.ThreadId, eventInformation.Info.Code, 
                                    eventInformation.Info.Address, eventInformation.Info.Flags );
                            }

                            m_exited = true;

                        }
                    }
                    break;

                // Handle all those notification types that don't take special handling.
                default:
                    {
                        this.Log.LogMessage( MessageImportance.Low, eventInformation.Info.Message + " event type " + eventCode );
                    }
                    break;
            }
        }

        [Required]
        public string XbrebootCommandLine
        {
            get
            {
                return m_cmdLine;
            }
            set
            {
                m_cmdLine = value;
            }
        }

        public string MediaDirectory
        {
            get
            {
                return m_mediaDirectory;
            }
            set
            {
                m_mediaDirectory = value;
            }
        }

        public string Target
        {
            get
            {
                return m_xboxTarget;
            }
            set
            {
                m_xboxTarget = value;

                if ( m_xboxTarget.StartsWith( "/x " ) )
                {
                    m_xboxTarget = m_xboxTarget.Substring( 2 ).Trim();
                }
            }
        }

        public bool FailOnError
        {
            get
            {
                return m_failOnError;
            }
            set
            {
                m_failOnError = value;
            }
        }

        public bool ColdReboot
        {
            get
            {
                return m_coldReboot;
            }
            set
            {
                m_coldReboot = value;
            }
        }

        public int TimeoutMS
        {
            get 
            {
                return m_timeoutMS; 
            }
            set
            {
                m_timeoutMS = value;
            }
        }

        public override bool Execute()
        {
            InitDM();
            this.Log.LogMessage( "Console - " + m_xboxConsole.Name );
            this.Log.LogMessage( "cmd - xbreboot " + this.XbrebootCommandLine );

            // reboot to new app
            //m_xboxConsole.Reboot(null, null, Exec, XboxRebootFlags.W);

            // could call script to reboot to dashboard
            while ( !this.TaskFinished )
            {
                Thread.Sleep( 100 );
            }
            m_xboxConsole.OnStdNotify -= new XboxEvents_OnStdNotifyEventHandler( xboxConsole_OnStdNotify );


            this.Log.LogMessage( MessageImportance.Low, "Waiting while rebooting " );
            m_xboxConsole.Reboot( null, null, null, m_coldReboot ? XboxRebootFlags.Cold : XboxRebootFlags.Title );

            Thread.Sleep(TimeoutMS);

            this.Log.LogMessage( MessageImportance.Low, "Done " );

            if ( this.FailOnError )
            {
                return !this.Log.HasLoggedErrors;
            }
            else
            {
                if ( !m_ignoreErrors )
                {
                    this.Log.LogError( "Text not found:  '***  TESTER IGNORE REST OF ERRORS ***'.  The test is incomplete." );
                }

                return m_ignoreErrors;
            }
        }
    }
}