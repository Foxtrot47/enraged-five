using System;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using System.Collections.Generic;
using System.IO;
using System.Text;

using RSG.Base;
using RSG.Base.Command;
using RSG.Base.IO;

namespace RageBuildTasks
{
	public class ExecuteCommand : Task
	{
		#region Variables
		private rageExecuteCommand m_obCommand = new rageExecuteCommand();
		private string m_strEmailProblemsTo = "";
		private bool m_bIgnoreExitCode = false;
		private bool m_bLogErrorsWithMSBuild = true;
        private string m_additionalLogFilename = null;
		#endregion

		#region Properties
		/// <summary>
		/// Required String parameter.  The command to run. This can be a system command, such as attrib, or an executable, such as program.exe.
		/// </summary>
		[Required]
		public string Command
		{
			get
			{
				return m_obCommand.Command;
			}
			set
			{
                string command = value;
                if ( !String.IsNullOrEmpty( command ) )
                {
                    // strip off quotes
                    if ( command.StartsWith( "\"" ) && command.EndsWith( "\"" ) )
                    {
                        command = command.Substring( 1, command.Length - 2 );
                    }
                }

                m_obCommand.Command = command;
			}
		}

		/// <summary>
		/// Optional string conatining the command arguments.
		/// </summary>
		public string Arguments
		{
			get
			{
				return m_obCommand.Arguments;
			}
			set
			{
				m_obCommand.Arguments = value;
			}
		}

		/// <summary>
		/// Optional Int32 output read-only parameter.  Specifies the exit code provided by the executed command.
		/// </summary>
		public Int32 ExitCode
		{
			get
			{
				return m_obCommand.ExitCode;
			}
		}

		/// <summary>
		/// Optional Boolean parameter.  If true, the task ignores the exit code provided by the executed command. Otherwise, the task returns false if the executed command returns a non-zero exit code.
		/// </summary>
		public bool IgnoreExitCode
		{
			get
			{
				return m_bIgnoreExitCode;
			}
			set
			{
				m_bIgnoreExitCode = value;
			}
		}

		/// <summary>
		/// Optional Boolean parameter.  If true, the errors and wornings from the command are logged as MSBuild errors and warnings.
		/// </summary>
		public bool LogErrorsWithMSBuild
		{
			get
			{
				return m_bLogErrorsWithMSBuild;
			}
			set
			{
				m_bLogErrorsWithMSBuild = value;
			}
		}

		/// <summary>
		/// The location to log output too
		/// </summary>
		public string LogFilename
		{
			get
			{
				return m_obCommand.LogFilename;
			}
			set
			{
				m_obCommand.LogFilename = value;
			}
		}

		/// <summary>
		/// The working directory
		/// </summary>
		public string WorkingDirectory
		{
			get
			{
				return m_obCommand.WorkingDirectory;
			}
			set
			{
				m_obCommand.WorkingDirectory = value;
			}
		}

		/// <summary>
		/// Email address or addresses to send any problems too
		/// </summary>
		public string EmailProblemsTo
		{
			get
			{
				return m_strEmailProblemsTo;
			}
			set
			{
				m_strEmailProblemsTo = value;
			}
		}

		/// <summary>
		/// Optional float parameter.  Specifies the amount of time, in seconds, after which the task executable is terminated. 
		/// </summary>
		public float TimeOutInSeconds
		{
			get
			{
				return m_obCommand.TimeOutInSeconds;
			}
			set
			{
				m_obCommand.TimeOutInSeconds = value;
			}
		}

		/// <summary>
		/// Optional float parameter.  Specifies the amount of time, in seconds, after which if there has been no text ouput the task executable is terminated. 
		/// </summary>
		public float TimeOutWithoutOutputInSeconds
		{
			get
			{
				return m_obCommand.TimeOutWithoutOutputInSeconds;
			}
			set
			{
				m_obCommand.TimeOutWithoutOutputInSeconds = value;
			}
		}

		/// <summary>
		/// String parameter.  The priority of the spawned task
		/// </summary>
		public string Priority
		{
			set
			{
				m_obCommand.SetPriority(value);
			}
		}

        /// <summary>
        /// Sets the path and filename of an additional log that the executing process might save.  At the end of execution,
        /// the contents of this file will be parsed for Errors and Warnings, then added to the output.
        /// </summary>
        public string AdditionalLogFilename
        {
            get
            {
                return m_additionalLogFilename;
            }
            set
            {
                m_additionalLogFilename = value;
            }
        }
		#endregion

		#region Overrides
		public override bool Execute()
		{
			// Do it
			rageStatus obStatus;
			m_obCommand.UseBusySpinner = false;
			m_obCommand.EchoToConsole = false;
			m_obCommand.UpdateLogFileInRealTime = false;
            if (m_obCommand.LogFilename == "")
			{
				m_obCommand.RemoveLog = true;
			}
			else
			{
				m_obCommand.RemoveLog = false;
			}

            this.Log.LogMessage( MessageImportance.High, String.Format( "{0} {1}", m_obCommand.Command, m_obCommand.Arguments ) );

			m_obCommand.Execute(out obStatus);

            ParseAndOutputLines( m_obCommand.GetLogAsArray() );

            bool bAnyErrors = false;
            if ( !String.IsNullOrEmpty( this.AdditionalLogFilename ) && File.Exists( this.AdditionalLogFilename ) )
            {
                TextReader reader = null;
                try
                {
                    reader = new StreamReader( this.AdditionalLogFilename );

                    List<string> lines = new List<string>();
                    string line = reader.ReadLine();
                    while ( line != null )
                    {
                        lines.Add( line );
                        line = reader.ReadLine();
                    }                    

                    bAnyErrors = ParseAndOutputLines( lines );
                }
                catch ( Exception ex )
                {
                    this.Log.LogError( ex.ToString() );
                }
                finally
                {
                    if ( reader != null )
                    {
                        reader.Close();
                    }
                }
            }

			if ( m_obCommand.LogFilename != "" )
			{
				if (obStatus.Success())
				{
					m_obCommand.HtmlLog.LogEvent("<font color=#00FF00>Exit code : " + m_obCommand.ExitCode + "</font>");
				}
				else
				{
					m_obCommand.HtmlLog.LogEvent("<font color=#FF0000>Exit code : " + m_obCommand.ExitCode + "</font>");
					m_obCommand.HtmlLog.LogEvent("<font color=#FF0000>Exit status : " + obStatus.ErrorString + "</font>");
				}
				m_obCommand.UpdateLogFileInRealTime = true;
			}

			if ( (!obStatus.Success() || bAnyErrors) && (m_strEmailProblemsTo != "") )
			{
				// Email the log
                Log.LogMessage( MessageImportance.High, String.Format( "Emailing {0}", m_strEmailProblemsTo ) );
				rageEmailUtilities.SendEmail(m_strEmailProblemsTo, rageFileUtilities.GetFilenameFromFilePath(m_obCommand.Command) + "@rockstarsandiego.com", m_obCommand.Command + " " + m_obCommand.Arguments, m_obCommand.LogFilename);
			}
			
            if ( m_bIgnoreExitCode )
			{
				return true;
			}
            
            return obStatus.Success() && !bAnyErrors;
		}
		#endregion

        #region Private Functions
        private bool ParseAndOutputLines( List<string> lines )
        {
            bool bAnyErrors = false;

            foreach ( string line in lines )
            {
                bool bLogError = false;
                bool bLogFatalError = false;
                bool bLogWarning = false;
                if ( m_bLogErrorsWithMSBuild )
                {
                    string strBuildLogLineUpper = line.ToUpper();
                    strBuildLogLineUpper = strBuildLogLineUpper.Trim();

                    if ( strBuildLogLineUpper.Contains( "): FATAL ERROR : " ) || strBuildLogLineUpper.StartsWith( "FATAL ERROR: " ) )
                    {
                        bLogFatalError = true;
                    }
                    else if ( strBuildLogLineUpper.Contains( "): ERROR : " ) || strBuildLogLineUpper.StartsWith( "ERROR: " ) )
                    {
                        bLogError = true;
                    }
                    else if ( strBuildLogLineUpper.Contains( "): WARNING : " ) || strBuildLogLineUpper.StartsWith( "WARNING: " ) )
                    {
                        bLogWarning = true;
                    }
                }

                if ( bLogError || bLogFatalError )
                {
                    this.Log.LogError( String.Format( "{0}: {1}", Path.GetFileName( this.Command ), line ) );

                    bAnyErrors = true;
                }
                else if ( bLogWarning )
                {
                    this.Log.LogWarning( String.Format( "{0}: {1}", Path.GetFileName( this.Command ), line ) );
                }
                else
                {
                    this.Log.LogMessage( MessageImportance.Normal, line );
                }
            }

            return bAnyErrors;
        }
        #endregion
    }
}
