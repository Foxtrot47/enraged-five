using System;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using System.Collections.Generic;
using System.IO;
using System.Text;

using RSG.Base.Command;

namespace RageBuildTasks
{
	public class IncrediBuildInit : Task
	{
		#region Variables
		private rageIncrediBuild m_obIncrediBuild = null;
		#endregion

		#region Properties
		/// <summary>
		/// Optional Int32 output read-only parameter.  Specifies the exit code provided by the executed command.
		/// </summary>
		[Output]
		public string GroupID
		{
			get
			{
				Console.WriteLine("Returning rageIncrediBuild with GUID " + m_obIncrediBuild.GroupUID);
				return m_obIncrediBuild.GroupUID;
			}
		}
		#endregion

		#region Overrides
		public override bool Execute()
		{
			// Don't actually need to do anything!  It is all in the contructor of rageIncrediBuild
			Console.WriteLine("Creating rageIncrediBuild");
			m_obIncrediBuild = new rageIncrediBuild();
			Console.WriteLine("Created rageIncrediBuild with GUID " + m_obIncrediBuild.GroupUID);
			m_obIncrediBuild.Init();
			Console.WriteLine("rageIncrediBuild Inited");
			return true;
		}
		#endregion
	}
}
