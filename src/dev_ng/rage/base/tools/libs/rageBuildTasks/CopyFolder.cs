using System;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using System.Collections.Generic;
using System.IO;
using System.Text;

using RSG.Base.Command;
using RSG.Base.IO;

namespace RageBuildTasks
{
    public class CopyFolder : Task
    {
        #region Variables
        private string m_sourceDirectory;
        private string m_destinationDirectory;
        #endregion

        #region Properties
        /// <summary>
        /// The directory to copy from.
        /// </summary>
        [Required]
        public string SourceDirectory
        {
            get
            {
                return m_sourceDirectory;
            }
            set
            {
                m_sourceDirectory = value;
            }
        }

        /// <summary>
        /// The directory to copy to.  This can be relative to the <see cref="SourceDirectory"/>.
        /// </summary>
        [Required]
        public string DestinationDirectory
        {
            get
            {
                return m_destinationDirectory;
            }
            set
            {
                m_destinationDirectory = value;
            }
        }
        #endregion

        #region Overrides
        public override bool Execute()
        {
            if ( String.IsNullOrEmpty( this.SourceDirectory ) )
            {
                this.Log.LogError( "SourceDirectory not specified." );
                return false;
            }

            if ( !Path.IsPathRooted( this.SourceDirectory ) )
            {
                this.SourceDirectory = Path.GetFullPath( this.SourceDirectory );
            }

            if ( String.IsNullOrEmpty( this.DestinationDirectory ) )
            {
                this.Log.LogError( "DestinationDirectory not specified." );
                return false;
            }

            if ( !Path.IsPathRooted( this.DestinationDirectory ) )
            {
                this.DestinationDirectory = Path.GetFullPath( Path.Combine( this.SourceDirectory, this.DestinationDirectory ) );
            }

            if ( !Directory.Exists( this.SourceDirectory ) )
            {
                this.Log.LogError( "SourceDirectory {0} does not exist.", this.SourceDirectory );
                return false;
            }

            string[] srcFilenames = Directory.GetFiles( this.SourceDirectory );
            if ( srcFilenames.Length == 0 )
            {
                this.Log.LogError( "No files in {0} to copy to {1}", this.SourceDirectory, this.DestinationDirectory );
                return false;
            }

            rageStatus obStatus;

            if ( Directory.Exists( this.DestinationDirectory ) )
            {
                string[] destFilenames = Directory.GetFiles( this.DestinationDirectory );
                if ( destFilenames.Length > 0 )
                {
                    this.Log.LogMessage( MessageImportance.High, "Deleting {0}", this.DestinationDirectory );

                    rageFileUtilities.DeleteLocalFolder( this.DestinationDirectory, out obStatus );
                    if ( !obStatus.Success() )
                    {
                        this.Log.LogWarning( obStatus.ErrorString );
                    }
                    else
                    {
                        foreach ( string filename in destFilenames )
                        {
                            this.Log.LogMessage( MessageImportance.Low, "Deleted {0}", filename );
                        }

                        this.Log.LogMessage( MessageImportance.High, "Deleted {0}", this.DestinationDirectory );
                    }
                }
            }

            this.Log.LogMessage( MessageImportance.High, "Copying {0} to {1}", this.SourceDirectory, this.DestinationDirectory );
            
            do
            {                
                rageFileUtilities.CopyLocalFolder( this.SourceDirectory, this.DestinationDirectory, out obStatus );
                if ( !obStatus.Success() )
                {
                    this.Log.LogWarning( "{0}  Trying again.", obStatus.ErrorString );
                }
            } while ( !obStatus.Success() );

            foreach ( string filename in srcFilenames )
            {
                this.Log.LogMessage( MessageImportance.Low, "Copied {0} to {1}", 
                    filename, Path.Combine( this.DestinationDirectory, Path.GetFileName( filename ) ) );
            }

            this.Log.LogMessage( MessageImportance.High, "Copied {0} to {1}", this.SourceDirectory, this.DestinationDirectory );

            return true;
        }
        #endregion
    }
}
