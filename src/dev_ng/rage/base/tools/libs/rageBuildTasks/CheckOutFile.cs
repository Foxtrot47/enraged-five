using System;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using System.Collections.Generic;
using System.IO;
using System.Text;

using RSG.Base.Command;
using RSG.Base.SCM;

namespace RageBuildTasks
{
    public class CheckOutFile : RagePerforceTaskBase
    {
        #region Variables
        private string m_filename;
        private string m_description;
        #endregion

        #region Properties

        /// <summary>
        /// The file to be checked out of Perforce.
        /// </summary>
        [Required]
        public string Filename
        {
            get
            {
                return m_filename;
            }
            set
            {
                m_filename = value;
            }
        }

        public string Description
        {
            get
            {
                return m_description;
            }
            set
            {
                m_description = value;
            }
        }

        public bool DisableLogging = false;
        #endregion

        #region Overrides
        public bool Execute(string client, string port, string user, string filename )
        {
            this.Client = client;
            this.Port = port;
            this.User = user;
            this.Filename = filename;
            this.DisableLogging = true;

            return Execute();
        }

        public override bool Execute()
        {
            if (String.IsNullOrEmpty(this.Filename))
            {
                this.Log.LogError("File was not specified for the Check Out File command.");
                return false;
            }

            if (DisableLogging == false)
            {
                this.Log.LogMessage(MessageImportance.High, "Checking Out " + this.Filename + " From Perforce...");
            }

            ragePerforce.ragePerforceClientspec clientSpec = ragePerforce.GetClientspec(this.Client, this.Port);
            string perforcePath = clientSpec.ConvertToPerforcePath(this.Filename);
            if (perforcePath == null)
            {
                this.Log.LogMessage("Unable to convert " + this.Filename + " into a Perforce path.");
                return false;
            }

            rageStatus obStatus;
            ragePerforce.CheckOutFile(perforcePath, this.Description, out obStatus);
            if (!obStatus.Success())
            {
                //If a file has already been checked out, then the p4 edit command will fail.
                //Verify that the file has not been checked out, and if it still hasn't, fail the task.
                //
                string localPath = this.Filename;
                FileAttributes attributes = File.GetAttributes(localPath);
                bool isReadOnly = ((attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly);

                if (isReadOnly == true)
                {
                    this.Log.LogError(obStatus.ErrorString);
                    return false;
                }
            }

            return true;
        }
        #endregion
    }
}
