using System;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using System.Collections.Generic;
using System.IO;
using System.Text;

using RSG.Base.Command;
using RSG.Base.SCM;

namespace RageBuildTasks
{
    public class SubmitPerforce : RagePerforceTaskBase
    {
        #region Variables
        private string m_revertUnchangedFiles;
        private string m_description;
        private bool m_integrate = false;
        private string m_integrationScript = null;
        #endregion

        #region Properties
        /// <summary>
        /// The description of the changelist.
        /// </summary>
        public string RevertUnchangedFiles
        {
            get
            {
                return m_revertUnchangedFiles;
            }
            set
            {
                m_revertUnchangedFiles = value;
            }
        }

        /// <summary>
        /// Description of the changelist.
        /// </summary>
        public string Description
        {
            get 
            {
                return m_description;
            }
            set
            {
                m_description = value;
            }
        }

        /// <summary>
        /// Integrates the changelist to the release branch.
        /// </summary>
        public bool Integrate
        {
            get { return m_integrate; }
            set { m_integrate = value; }
        }

        public string IntegrationScript
        {
            get { return m_integrationScript; }
            set { m_integrationScript = value; }
        }
        #endregion

        #region Overrides
        public override bool Execute()
        {
            ragePerforce.EchoToConsole = true;

            bool bRevertUnchangedFiles = false;
            if(String.IsNullOrEmpty(RevertUnchangedFiles) == false)
                bRevertUnchangedFiles = Boolean.Parse(RevertUnchangedFiles);

            this.Log.LogMessage( MessageImportance.High, "Submitting Changelist to Perforce..." );

            rageStatus obStatus;
            int submittedChangelistNumber = ragePerforce.SubmitChangelist(bRevertUnchangedFiles, m_description, out obStatus);
            if ( !obStatus.Success() || submittedChangelistNumber == -1)
            {
                //The submission may fail if there are two files to submit.  Do not fail the build in this case.
                this.Log.LogWarning( obStatus.ErrorString );
                return true;
            }

            if (m_integrate == true)
            {
                rageExecuteCommand integrateCommand = new rageExecuteCommand();

                if (Path.GetExtension(IntegrationScript) == ".rb")
                {
                    integrateCommand.Command = "ruby";
                    integrateCommand.Arguments = IntegrationScript + " " + submittedChangelistNumber.ToString() + " --autorevertunchanged --autosubmit";
                }
                else
                {
                    integrateCommand.Command = IntegrationScript;
                    integrateCommand.Arguments = submittedChangelistNumber.ToString() + " --autorevertunchanged --autosubmit";
                }
                
                integrateCommand.UpdateLogAfterEverySoManyLines = 500;
                integrateCommand.LogToHtmlFile = false;
                integrateCommand.RemoveLog = true;
                integrateCommand.UseBusySpinner = false;
                integrateCommand.EchoToConsole = true;

                Console.WriteLine("Integrating changelist " + submittedChangelistNumber + " to release branch.");
                Console.WriteLine(integrateCommand.GetDosCommand());
                integrateCommand.Execute(out obStatus);
            }

            return true;
        }
        #endregion
    }
}
