using System;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.Win32;

using RSG.Base.Command;

namespace RageBuildTasks
{
    public class InstallSyntaxEditor : RageTaskBase
    {
        #region Variables
        private string m_installersLocation;

        private static Regex s_regex = new Regex( "\\AMSI\\s*\\x28(.)+\\x29\\s*\\x28([a-zA-Z_0-9:]+)\\x29\\s*\\x5B(?<Time>[0-9:]+)\\x5D\\s*:\\s*(?<Text>(.)*)", RegexOptions.Compiled );

        static string sm_licenseKey = "VKG69-LCELC(=31LL)OU?;T#5IJD"; // "SED40-HFAEL-303EL-JQ64Q-6KCD";
        static string sm_licenseSeed = "eNcRyPtEdYo";
        #endregion

        #region Properties
        /// <summary>
        /// The folder where the MSI installers are located.
        /// </summary>
        [Required]
        public string InstallersLocation
        {
            get
            {
                return m_installersLocation;
            }
            set
            {
                m_installersLocation = value;
            }
        }
        #endregion

        #region Overrides
        public override bool Execute()
        {
            if ( String.IsNullOrEmpty( this.InstallersLocation ) )
            {
                this.Log.LogError( "InstallersLocation is not defined." );
                return false;
            }

            if ( !Directory.Exists( this.InstallersLocation ) )
            {
                this.Log.LogError( "The InstallersLocation '{0}' does not exist.", this.InstallersLocation );
                return false;
            }

            Version obSyntaxEditorVersion = GetRequiredVersion();
            if ( obSyntaxEditorVersion == null )
            {
                return false;
            }

            Version obCurrentSyntaxEditorVersion = GetInstalledVersion();
            if ( (obCurrentSyntaxEditorVersion != null) && (obSyntaxEditorVersion == obCurrentSyntaxEditorVersion) )
            {
                this.Log.LogMessage( MessageImportance.Normal, "The required version of ActiproSoftware Syntax Editor, {0}, is already installed.", obSyntaxEditorVersion.ToString() );
                return true;
            }

            if ( (obCurrentSyntaxEditorVersion != null) && !ExecuteInstaller( obCurrentSyntaxEditorVersion, false ) )
            {
                return false;
            }

            if ( !ExecuteInstaller( obSyntaxEditorVersion, true ) )
            {
                return false;
            }

            return true;            
        }
        #endregion

        #region Private Functions
        private Version GetRequiredVersion()
        {
            string strBaseDir = GetMsBuildEnvironmentVariable( "baseDir" );
            if ( String.IsNullOrEmpty( strBaseDir ) )
            {
                strBaseDir = Environment.GetEnvironmentVariable( "RAGE_ROOT_FOLDER" );
                if ( String.IsNullOrEmpty( strBaseDir ) )
                {
                    this.Log.LogError( "baseDir is not defined." );
                    return null;
                }
            }

            string strLicenseFilename = Path.Combine( strBaseDir, "base\\tools\\ui\\rag\\rag\\Properties\\licenses.licx" );
            if ( !File.Exists( strLicenseFilename ) )
            {
                strLicenseFilename = Path.Combine( strBaseDir, "base\\tools\\ui\\rag\\rag\\licenses.licx" );
                if ( !File.Exists( strLicenseFilename ) )
                {
                    this.Log.LogError( "Could not find '{0}'.", strLicenseFilename );
                    return null;
                }
            }

            string strSyntaxEditorVersion = string.Empty;
            TextReader reader = null;
            try
            {
                reader = new StreamReader( strLicenseFilename );

                string strLine = reader.ReadLine();
                while ( strLine != null )
                {
                    if ( strLine.StartsWith( "ActiproSoftware.SyntaxEditor.SyntaxEditor" ) )
                    {
                        string[] strLineParts = strLine.Split( new char[] { ',' } );
                        if ( strLineParts.Length >= 3 )
                        {
                            string[] strVersionParts = strLineParts[2].Split( new char[] { '=' } );
                            if ( strVersionParts.Length >= 2 )
                            {
                                strSyntaxEditorVersion = strVersionParts[1];
                                break;
                            }
                        }
                    }

                    strLine = reader.ReadLine();
                }
            }
            catch ( Exception e )
            {
                this.Log.LogError( e.Message );
            }
            finally
            {
                if ( reader != null )
                {
                    reader.Close();
                }
            }

            if ( String.IsNullOrEmpty( strSyntaxEditorVersion ) )
            {
                this.Log.LogError( "Unable to determine the SyntaxEditor version number." );
                return null;
            }

            if ( strSyntaxEditorVersion.Split( new char[] { '.' } ).Length < 4 )
            {
                strSyntaxEditorVersion += ".0";
            }
            
            return new Version( strSyntaxEditorVersion );
        }

        private Version GetInstalledVersion()
        {
            try
            {
                RegistryKey key = Registry.LocalMachine.OpenSubKey( "SOFTWARE" );
                if ( key != null )
                {
                    key = key.OpenSubKey( "Microsoft" );
                    if ( key != null )
                    {
                        key = key.OpenSubKey( ".NETFramework" );
                        if ( key != null )
                        {
                            key = key.OpenSubKey( "AssemblyFolders" );
                        }
                    }
                }

                if ( key != null )
                {
                    string strPattern = "ActiproSoftware.SyntaxEditor.Net20.v";

                    string[] subKeyNames = key.GetSubKeyNames();
                    foreach ( string subKeyName in subKeyNames )
                    {
                        if ( subKeyName.StartsWith( strPattern ) )
                        {
                            string strVersion = subKeyName.Substring( strPattern.Length );
                            return new Version( strVersion + ".0" );
                        }
                    }
                }
            }
            catch
            {
            	
            }

            return null;
        }

        private bool ExecuteInstaller( Version obSyntaxEditorVersion, bool bInstall )
        {
            string strSyntaxEditorInstaller = Path.Combine( this.InstallersLocation, String.Format( "SyntaxEditor-Full-{0}.msi", obSyntaxEditorVersion.ToString() ) );
            if ( !File.Exists( strSyntaxEditorInstaller ) )
            {
                this.Log.LogError( "The required SyntaxEditor installer '{0}' does not exist.", strSyntaxEditorInstaller );
                return false;
            }

            rageExecuteCommand obCommand = new rageExecuteCommand();
            obCommand.Command = "msiexec";

            StringBuilder obArguments = new StringBuilder();
            obArguments.Append( bInstall ?  " /i \"" : " /x \"" );
            obArguments.Append( strSyntaxEditorInstaller );
            obArguments.Append( "\"" );

            string strTempLog = Path.Combine( Environment.GetEnvironmentVariable( "TEMP" ), bInstall ? "install_syntax_editor.txt" : "uninstall_syntax_editor.txt" );
            obArguments.Append( " /l*v \"" );
            obArguments.Append( strTempLog );
            obArguments.Append( "\" /qn" );

            obCommand.Arguments = obArguments.ToString();
            obCommand.LogFilename = Path.Combine( Environment.GetEnvironmentVariable( "TEMP" ), bInstall ? "install_syntax_editor.html" : "uninstall_syntax_editor.html" );
            obCommand.RemoveLog = true;
            obCommand.TimeOutInSeconds = 5 * 60.0f;
            obCommand.TimeOutWithoutOutputInSeconds = 5 * 60.0f;
            obCommand.UpdateLogFileInRealTime = false;
            obCommand.UseBusySpinner = false;
            obCommand.WorkingDirectory = @"c:\";

            this.Log.LogMessage( MessageImportance.Normal, obCommand.GetDosCommand() );

            rageStatus obStatus = null;
            obCommand.Execute( out obStatus );            
    
            string strTitle = BuildLogTitle(
                String.Format( "SyntaxEditor {0} Log for {1}", bInstall ? "Installation" : "Uninstallation", 
                Path.GetFileName( strSyntaxEditorInstaller ) ), null, null, null );

            List<string> astrSummaryAsText = new List<string>();
            astrSummaryAsText.Add( String.Format( "Integration Project:{0}", GetMsBuildEnvironmentVariable( "CCNetProject" ) ) );
            astrSummaryAsText.Add( String.Format( "Code Grab:{0} on {1}",
                GetMsBuildEnvironmentVariable( "CCNetBuildTime" ), GetMsBuildEnvironmentVariable( "CCNetBuildDate" ) ) );
            astrSummaryAsText.Add( "Code Tag:Latest" );

            astrSummaryAsText.Add( String.Format( "Time:{0}", System.DateTime.Now.ToLongTimeString() ) );
            astrSummaryAsText.Add( String.Format( "Date:{0}", System.DateTime.Now.ToLongDateString() ) );
            astrSummaryAsText.Add( String.Format( "Computer:{0}", Environment.GetEnvironmentVariable( "COMPUTERNAME" ) ) );
            astrSummaryAsText.Add( String.Format( "User:{0}", Environment.GetEnvironmentVariable( "USERNAME" ) ) );
            astrSummaryAsText.Add( String.Format( "Command Line:{0}", obCommand.GetDosCommand() ) );

            int iReturnCode = 0;

            List<string> astrErrorsAsText = new List<string>();

            TextReader reader = null;
            string strTime = string.Empty;
            try
            {
                reader = new StreamReader( strTempLog );

                string strLine = reader.ReadLine();
                while ( strLine != null )
                {
                    string strLogText = strLine;

                    Match m = s_regex.Match( strLine );
                    if ( m.Success )
                    {
                        strTime = m.Result( "${Time}" );
                        if ( !String.IsNullOrEmpty( strTime ) )
                        {
                            string[] strTimeParts = strTime.Split( new char[] { ':' } );
                            if ( strTimeParts.Length >= 3 )
                            {
                                int[] iTimeParts = new int[strTimeParts.Length];
                                for ( int i = 0; i < strTimeParts.Length; ++i )
                                {
                                    try
                                    {
                                        iTimeParts[i] = int.Parse( strTimeParts[i] );
                                    }
                                    catch 
                                    {
                                    	iTimeParts[i] = 0;
                                    }
                                }

                                bool bAM = true;
                                if ( iTimeParts[0] > 12 )
                                {
                                    iTimeParts[0] -= 12;
                                    bAM = false;
                                }

                                strTime = String.Format( "{0}/{1}/{2} {3}:{4}:{5} {6}", DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Year,
                                    iTimeParts[0], iTimeParts[1], iTimeParts[2], bAM ? "AM" : "PM" );
                            }
                        }

                        strLogText = m.Result( "${Text}" );                        
                    }

                    strLogText = strLogText.Replace( "\\n", "<br>" ).Replace( "\n", "<br>" );

                    string strSearchText = " MainEngineThread is returning ";
                    int indexOf = strLine.IndexOf( strSearchText );
                    if ( indexOf != -1 )
                    {
                        string strReturnCode = strLine.Substring( indexOf + strSearchText.Length );
                        try
                        {
                            iReturnCode = int.Parse( strReturnCode );

                            bool bIsWarning = false;
                            bool bIsError = true;
                            switch ( iReturnCode )
                            {
                                case 0:
                                    bIsWarning = false;
                                    bIsError = false;
                                    break;
                                case 123:
                                    bIsError = false;
                                    strLine += ".  The product is already installed.";
                                    break;
                                case 1601:
                                    strLine += ".  The Windows Installer service could not be accessed. Contact your support personnel to verify that the Windows Installer service is properly registered.";
                                    break;
                                case 1602:
                                    strLine += ".  User cancel installation.";
                                    break;
                                case 1603:
                                    strLine += ".  Fatal error during installation.";
                                    break;
                                case 1604:
                                    strLine += ".  Installation suspended, incomplete.";
                                    break;
                                case 1605:
                                    strLine += ".  This action is only valid for products that are currently installed.";
                                    bIsError = false;
                                    break;
                                case 1606:
                                    strLine += ".  Feature ID not registered.";
                                    break;
                                case 1607:
                                    strLine += ".  Component ID not registered.";
                                    break;
                                case 1608:
                                    strLine += ".  Unknown property.";
                                    break;
                                case 1609:
                                    strLine += ".  Handle is in an invalid state.";
                                    break;
                                case 1610:
                                    strLine += ".  The configuration data for this product is corrupt. Contact your support personnel.";
                                    break;
                                case 1611:
                                    strLine += ".  Component qualifier not present.";
                                    break;
                                case 1612:
                                    strLine += ".  The installation source for this product is not available. Verify that the source exists and that you can access it.";
                                    break;
                                case 1613:
                                    strLine += ".  This installation package cannot be installed by the Windows Installer service. You must install a Windows service pack that contains a newer version of the Windows Installer service.";
                                    break;
                                case 1614:
                                    strLine += ".  Product is uninstalled.";
                                    bIsWarning = bInstall ? false : true;
                                    bIsError = bInstall ? true : false;
                                    break;
                                case 1615:
                                    strLine += ".  SQL query syntax invalid or unsupported.";
                                    break;
                                case 1616:
                                    strLine += ".  Record field does not exist.";
                                    break;
                                case 1618:
                                    strLine += ".  Another installation is already in progress. Complete that installation before proceeding with this install.";
                                    break;
                                case 1619:
                                    strLine += ".  This installation package could not be opened. Verify that the package exists and that you can access it, or contact the application vendor to verify that this is a valid Windows Installer package.";
                                    break;
                                case 1620:
                                    strLine += ".  This installation package could not be opened. Contact the application vendor to verify that this is a valid Windows Installer package.";
                                    break;
                                case 1621:
                                    strLine += ".  There was an error starting the Windows Installer service user interface. Contact your support personnel.";
                                    break;
                                case 1622:
                                    strLine += ".  Error opening installation log file. Verify that the specified log file location exists and is writable.";
                                    break;
                                case 1623:
                                    strLine += ".  This language of this installation package is not supported by your system.";
                                    break;
                                case 1624:
                                    strLine += ".  Error applying transforms. Verify that the specified transform paths are valid.";
                                    break;
                                case 1625:
                                    strLine += ".  This installation is forbidden by system policy. Contact your system administrator.";
                                    break;
                                case 1626:
                                    strLine += ".  Function could not be executed.";
                                    break;
                                case 1627:
                                    strLine += ".  Function failed during execution.";
                                    break;
                                case 1628:
                                    strLine += ".  Invalid or unknown table specified.";
                                    break;
                                case 1629:
                                    strLine += ".  Data supplied is of wrong type.";
                                    break;
                                case 1630:
                                    strLine += ".  Data of this type is not supported.";
                                    break;
                                case 1631:
                                    strLine += ".  The Windows Installer service failed to start. Contact your support personnel.";
                                    break;
                                case 1632:
                                    strLine += ".  The temp folder is either full or inaccessible. Verify that the temp folder exists and that you can write to it.";
                                    break;
                                case 1633:
                                    strLine += ".  This installation package is not supported on this platform. Contact your application vendor.";
                                    break;
                                case 1634:
                                    strLine += ".  Component not used on this machine.";
                                    break;
                                case 1635:
                                    strLine += ".  This patch package could not be opened. Verify that the patch package exists and that you can access it, or contact the application vendor to verify that this is a valid Windows Installer patch package.";
                                    break;
                                case 1636:
                                    strLine += ".  This patch package could not be opened. Contact the application vendor to verify that this is a valid Windows Installer patch package.";
                                    break;
                                case 1637:
                                    strLine += ".  This patch package cannot be processed by the Windows Installer service. You must install a Windows service pack that contains a newer version of the Windows Installer service.";
                                    break;
                                case 1638:
                                    strLine += ".  Another version of this product is already installed. Installation of this version cannot continue. To configure or remove the existing version of this product, use Add/Remove Programs on the Control Panel.";
                                    break;
                                case 1639:
                                    strLine += ".  Invalid command line argument. Consult the Windows Installer SDK for detailed command line help.";
                                    break;
                                case 3010:
                                    bIsWarning = false;
                                    bIsError = false;
                                    strLine += ".  A restart is required to complete the install. This does not include installs where the ForceReboot action is run. Note that this error will not be available until future version of the installer.";
                                    break;
                                default:
                                    strLine += ".  No description available.";
                                    break;
                            }

                            if ( bIsWarning )
                            {
                                this.Log.LogWarning( strLine );
                            }
                            else if ( bIsError )
                            {
                                this.Log.LogError( strLine );
                                astrErrorsAsText.Add( strLine );
                            }
                            else
                            {
                                this.Log.LogMessage( MessageImportance.Normal, strLine );
                            }
                        }
                        catch
                        {
                            this.Log.LogError( "Unable to parse the error code from '{0}'", strLine );
                        }
                    }
                    else
                    {
                        this.Log.LogMessage( MessageImportance.Normal, strLine );
                    }

                    strLine = reader.ReadLine();
                }
            }
            catch (System.Exception e)
            {
                this.Log.LogError( "Unable to open the log file '{0}': {1}", strTempLog, e.Message );
            }
            finally
            {
                if ( reader != null )
                {
                    reader.Close();
                }                
            }

            try
            {
                if ( File.Exists( strTempLog ) )
                {
                    File.Delete( strTempLog );
                }
            }
            catch
            {

            }

            if ( astrErrorsAsText.Count > 0 )
            {
                this.Log.LogError( "Unable to install SyntaxEditor.  Exit Code: {0}", iReturnCode );
                return false;
            }

            if ( bInstall )
            {
                return SetRegistry();
            }

            return true;
        }

        private bool SetRegistry()
        {
            try
            {
                string strLicenseKey = PatheticCrypto( sm_licenseKey, sm_licenseSeed );

                Registry.SetValue( @"HKEY_LOCAL_MACHINE\SOFTWARE\Actipro Software\SyntaxEditor\WindowsForms\4.0", "InstallDate",
                    DateTime.Now.ToShortDateString(), RegistryValueKind.String );
                Registry.SetValue( @"HKEY_LOCAL_MACHINE\SOFTWARE\Actipro Software\SyntaxEditor\WindowsForms\4.0", "Licensee",
                    "Rockstar San Diego", RegistryValueKind.String );
                Registry.SetValue( @"HKEY_LOCAL_MACHINE\SOFTWARE\Actipro Software\SyntaxEditor\WindowsForms\4.0", "LicenseKey",
                    strLicenseKey, RegistryValueKind.String );
                Registry.SetValue( @"HKEY_LOCAL_MACHINE\SOFTWARE\Actipro Software\SyntaxEditor\WindowsForms\4.0", "ResellerUrl",
                    "https://www.ActiproSoftware.com/Purchase/DotNet/Order.aspx", RegistryValueKind.String );
                Registry.SetValue( @"HKEY_LOCAL_MACHINE\SOFTWARE\Actipro Software\SyntaxEditor\WindowsForms\4.0", "UserName",
                    "Adam Dickinson", RegistryValueKind.String );
                Registry.SetValue( @"HKEY_LOCAL_MACHINE\SOFTWARE\Actipro Software\SyntaxEditor\WindowsForms\4.0", "LicenseType",
                    "Full Release", RegistryValueKind.String );
            }
            catch (System.Exception e)
            {
                this.Log.LogError( e.Message );
                return false;
            }

            return true;
        }

        private static string PatheticCrypto( string sourceString, string seed )
        {
            int index = 0;
            string result = "";
            int length = sourceString.Length;
            int seedLen = seed.Length;

            for ( int x = 0; x < length; x++ )
            {
                result += (char)(sourceString[x] ^ (seed[index++] & 15));
                index %= seedLen;
            }

            return result;
        }
        #endregion
    }
}
