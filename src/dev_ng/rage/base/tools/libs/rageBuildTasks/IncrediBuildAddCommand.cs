using System;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using System.Collections.Generic;
using System.IO;
using System.Text;

using RSG.Base.Command;

namespace RageBuildTasks
{
	public class IncrediBuildAddCommand : Task
	{
		#region Variables
		private rageExecuteCommand m_obCommand = new rageExecuteCommand();
		private string m_strEmailProblemsTo = "";
		private bool m_bIgnoreExitCode = false;
		private string m_strIncredibuildGUID;
		private string m_strOutputFilenames = "";
        private bool m_bAllowRemote = true;
		#endregion

		#region Properties
		/// <summary>
		/// Required String parameter.  The command to run. This can be a system command, such as attrib, or an executable, such as program.exe.
		/// </summary>
		[Required]
		public string Command
		{
			get
			{
				return m_obCommand.Command;
			}
			set
			{
				m_obCommand.Command = value;
			}
		}

		/// <summary>
		/// Optional string conatining the command arguments.
		/// </summary>
		public string Arguments
		{
			get
			{
				return m_obCommand.Arguments;
			}
			set
			{
				m_obCommand.Arguments = value;
			}
		}

		/// <summary>
		/// Optional Int32 output read-only parameter.  Specifies the exit code provided by the executed command.
		/// </summary>
		public Int32 ExitCode
		{
			get
			{
				return m_obCommand.ExitCode;
			}
		}

		/// <summary>
		/// Optional Boolean parameter.  If true, the task ignores the exit code provided by the executed command. Otherwise, the task returns false if the executed command returns a non-zero exit code.
		/// </summary>
		public bool IgnoreExitCode
		{
			get
			{
				return m_bIgnoreExitCode;
			}
			set
			{
				m_bIgnoreExitCode = value;
			}
		}

		/// <summary>
		/// The location to log output too
		/// </summary>
		public string LogFilename
		{
			get
			{
				return m_obCommand.LogFilename;
			}
			set
			{
				m_obCommand.LogFilename = value;
			}
		}

		/// <summary>
		/// The working directory
		/// </summary>
		public string WorkingDirectory
		{
			get
			{
				return m_obCommand.WorkingDirectory;
			}
			set
			{
				m_obCommand.WorkingDirectory = value;
			}
		}

		/// <summary>
		/// Email address or addresses to send any problems too
		/// </summary>
		public string EmailProblemsTo
		{
			get
			{
				return m_strEmailProblemsTo;
			}
			set
			{
				m_strEmailProblemsTo = value;
			}
		}

		/// <summary>
		/// Optional float parameter.  Specifies the amount of time, in seconds, after which the task executable is terminated. 
		/// </summary>
		public float TimeOutInSeconds
		{
			get
			{
				return m_obCommand.TimeOutInSeconds;
			}
			set
			{
				m_obCommand.TimeOutInSeconds = value;
			}
		}

		/// <summary>
		/// Optional float parameter.  Specifies the amount of time, in seconds, after which if there has been no text ouput the task executable is terminated. 
		/// </summary>
		public float TimeOutWithoutOutputInSeconds
		{
			get
			{
				return m_obCommand.TimeOutWithoutOutputInSeconds;
			}
			set
			{
				m_obCommand.TimeOutWithoutOutputInSeconds = value;
			}
		}

		/// <summary>
		/// String parameter.  The priority of the spawned task
		/// </summary>
		public string Priority
		{
			set
			{
				m_obCommand.SetPriority(value);
			}
		}

		/// <summary>
		/// String parameter.  The GUID of the Incredibuild queue
		/// </summary>
		public string IncrediBuildGroupID
		{
			get
			{
				return m_strIncredibuildGUID;
			}
			set
			{
				m_strIncredibuildGUID = value;
			}
		}

		/// <summary>
        /// If the command can be executed on remote machines (default = true)
        /// Optional
        /// </summary>
        public bool AllowRemote
        {
            get
            {
                return m_bAllowRemote;
            }
            set
            {
                m_bAllowRemote = value;
            }
        }

		/// <summary>
		/// The files that will be touched by this process
		/// Optional
		/// </summary>
		public string OutputFilenames
		{
			get
			{
				return m_strOutputFilenames;
			}
			set
			{
				m_strOutputFilenames = value;
			}
		}

		#endregion

		#region Overrides
		public override bool Execute()
		{
			// Don't actually execute the command now, instead queue it up in IncrediBuild
			if ((IncrediBuildGroupID == null) || (IncrediBuildGroupID == ""))
			{
				// Not got a valid IncrediBuildGroupID so bail
				this.Log.LogError("Not given a valid IncrediBuildGroupID.");
				return false;
			}

			rageIncrediBuild obIncrediBuild = new rageIncrediBuild(IncrediBuildGroupID);
			obIncrediBuild.AddCommand(m_obCommand, EmailProblemsTo, IgnoreExitCode,
                m_strOutputFilenames.Split( ";,".ToCharArray(), StringSplitOptions.RemoveEmptyEntries ), m_bAllowRemote );
			return true;
		}
		#endregion
	}
}
