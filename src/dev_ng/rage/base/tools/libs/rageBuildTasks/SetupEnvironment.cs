using System;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using System.Collections.Generic;
using System.IO;
using System.Text;

using RSG.Base;

namespace RageBuildTasks
{
    public class SetupEnvironment : RagePerforceTaskBase
    {
        #region Variables
        private bool m_skipDefaults;
        private string m_environmentVariables;
        #endregion

        #region Properties

        /// <summary>
        /// By default, we set RAGE_ROOT_FOLDER, RAGE_DIR, MAX9_DIR, and MAYA_DEV_PATH_6_0.  Set this to true to not do that.
        /// </summary>
        public bool SkipDefaults
        {
            get
            {
                return m_skipDefaults;
            }
            set
            {
                m_skipDefaults = value;
            }
        }

        /// <summary>
        /// The environment variables and their values in the following format:
        /// key1=value1;key2=value2
        /// </summary>
        public string EnvironmentVariables
        {
            get
            {
                return m_environmentVariables;
            }
            set
            {
                m_environmentVariables = value;
            }
        }
        #endregion

        #region Overrides
        public override bool Execute()
        {
            if (!this.SkipDefaults)
            {
                string strSourceCodeRoot = Environment.GetEnvironmentVariable("RAGE_DIR");
                if( String.IsNullOrEmpty(strSourceCodeRoot) == true)
                {
                    // Oh dear, something bad happened
                    this.Log.LogError("Unable to find \"RAGE_DIR\" environment variable.  The project framework may not be installed! Exiting task...");
                    return false;
                }

                strSourceCodeRoot = strSourceCodeRoot.Replace('/', '\\');

                //Temporary work around for MAX plugins that have been set to build directly to the 3dsmax installation plugin folder
                //re-point the MAX9_DIR environment variable to point to the 3dsmax9 folder underneath the code line root
                Environment.SetEnvironmentVariable("MAX9_DIR", String.Format("{0}\\tools\\max\\3dsmax9", strSourceCodeRoot));
                this.Log.LogMessage(MessageImportance.Normal, "SetEnvironmentVariable MAX9_DIR={0}",
                    Environment.GetEnvironmentVariable("MAX9_DIR"));

                Environment.SetEnvironmentVariable("MAYA_DEV_PATH_6_0", "C:\\Program Files\\Alias\\Maya8.0");
                this.Log.LogMessage(MessageImportance.Normal, "SetEnvironmentVariable MAYA_DEV_PATH_6_0={0}",
                    Environment.GetEnvironmentVariable("MAYA_DEV_PATH_6_0"));
            }

            if ( !String.IsNullOrEmpty( this.EnvironmentVariables ) )
            {
                string[] keyValueSplit = this.EnvironmentVariables.Split( new char[] { Path.PathSeparator }, StringSplitOptions.RemoveEmptyEntries );
                foreach ( string keyValue in keyValueSplit )
                {
                    string[] split = keyValue.Split( new char[] { '=' } );

                    string key = null;
                    string value = null;

                    if ( split.Length > 2 )
                    {
                        key = split[0];

                        StringBuilder strValue = new StringBuilder( split[1] );
                        for ( int i = 2; i < split.Length; ++i )
                        {
                            strValue.Append( '=' );
                            strValue.Append( split[i] );
                        }

                        value = strValue.ToString();
                    }
                    else if ( split.Length == 2 )
                    {
                        key = split[0];
                        value = split[1];
                    }

                    if ( (key != null) && (value != null ) )
                    {
                        Environment.SetEnvironmentVariable( key, value );
                        this.Log.LogMessage( MessageImportance.Normal, "SetEnvironmentVariable {0}={1}",
                            key, Environment.GetEnvironmentVariable( key ) );
                    }
                    else
                    {
                        this.Log.LogError( "Could not parse environment variable {0}", keyValue );
                    }
                }
            }

            return true;
        }
        #endregion
    }
}
