using System;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using System.Collections.Generic;
using System.IO;
using System.Text;

using RSG.Base.Command;
using RSG.Base.IO;

namespace RageBuildTasks
{
    public class ArchiveLogs : MakeZip
    {
        #region Variables
        private int m_logNumberThreshhold = -1;
        #endregion

        #region Properties
        public int LogNumberThreshhold
        {
            get
            {
                return m_logNumberThreshhold;
            }
            set
            {
                m_logNumberThreshhold = value;
            }
        }
        #endregion

        #region Overrides
        public override bool Execute()
        {
            if ( String.IsNullOrEmpty( this.LogDirectory ) )
            {
                this.Log.LogError( "The Log Directory was not set." );
                return false;
            }

            int threshhold = (this.LogNumberThreshhold != -1) ? this.LogNumberThreshhold : 1000;

            string[] directories = Directory.GetDirectories( this.LogDirectory );
            if ( directories.Length >= threshhold )
            {
                if ( base.Execute() )
                {
                    this.Log.LogMessage( MessageImportance.High, "Deleting the last {0} logs.", threshhold );

                    for ( int i = 0; i < threshhold; ++i )
                    {
                        rageStatus obStatus;
                        rageFileUtilities.DeleteLocalFolder( directories[i], out obStatus );
                        if ( !obStatus.Success() )
                        {
                            this.Log.LogWarning( obStatus.ErrorString );
                        }
                        else
                        {
                            this.Log.LogMessage( MessageImportance.Normal, "Deleted {0}", directories[i] );
                        }
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                this.Log.LogMessage( MessageImportance.High, "Less than {0} log directories.  No archive is needed at this time.", threshhold );
            }

            return true;
        }
        #endregion
    }
}
