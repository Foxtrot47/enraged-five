﻿using System;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Xml;

using RSG.Base.SCM;
using RSG.Base.Command;
using System.Reflection;
using Microsoft.Win32;

namespace RageBuildTasks
{
    /// <summary>
    /// An MSBuild Task to build a Visual Studio solution given a partial path and name of a solution
    /// </summary>
    public class RevertChangelist : RagePerforceTaskBase
    {
        public override bool Execute()
        {
            rageStatus obStatus = new rageStatus();
            ragePerforce.RevertChangelist(out obStatus);
            if (obStatus.Success() == false)
            {
                return false;
            }

			return true;
        }
    }
}

