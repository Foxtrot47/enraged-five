using System;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

using RSG.Base;
using RSG.Base.Command;
using RSG.Base.IO;
using RSG.Base.Logging;

namespace RageBuildTasks
{
    /// <summary>
    /// A MSBuild Task to copy the binary output files that are the result of a compiled solution to a specified location.
    /// </summary>
    public class CopySolutionBinaries : RageTaskBase
    {
        #region Delegates
        private delegate string GetPrimaryFilenameDelegate( string projectFilename, out rageStatus obStatus );
        private delegate void GetFilenamesToCopyDelegate( string primaryFilename, List<string> srcFiles, List<string> destFiles, out rageStatus obStatus );
        #endregion

        #region Variables
        private string m_solution;
        private string m_project;
        private string m_configuration;
        private bool m_buildContinuedOnError;

        private string m_renameDestFile;
        private string m_copyDestFolder;
        private bool m_copyDebugFiles;

        private Dictionary<string, GetPrimaryFilenameDelegate> m_projectTypeToGetPrimaryFilenameDictionary = new Dictionary<string,GetPrimaryFilenameDelegate>();
        private Dictionary<string, GetFilenamesToCopyDelegate> m_projectTypeToGetFilenamesToCopyDictionary = new Dictionary<string,GetFilenamesToCopyDelegate>();
        #endregion

        #region Properties
        /// <summary>
        /// The solution to build.  At minimum, the format must be that of the old tools.txt:  suite\samples\tools\rageViewer\rageViewer.
        /// If needed, "_2005.sln" or ".sln" will be appended.  It's best if the path root is specified, but should also work
        /// if the path is relative to the current directory (or the CruiseControl config's WorkingDirectory).
        /// </summary>
        [Required]
        public string Solution
        {
            get
            {
                return m_solution;
            }
            set
            {
                m_solution = value;
            }
        }

        /// <summary>
        /// The project to compile, if any
        /// </summary>
        public string Project
        {
            get
            {
                return m_project;
            }
            set
            {
                m_project = value;
            }
        }

        /// <summary>
        /// The build configuration.
        /// </summary>
        [Required]
        public string Configuration
        {
            get
            {
                return m_configuration;
            }
            set
            {
                m_configuration = value;
            }
        }

		/// <summary>
		/// The build platform.
		/// </summary>
		public string PlatformName
		{
			get
			{
				return m_configuration.Substring(m_configuration.IndexOf('|') + 1);
			}
		}

		/// <summary>
        /// Whether the build continued on error.  Affects how we report errors.
        /// </summary>
        public bool BuildContinuedOnError
        {
            get
            {
                return m_buildContinuedOnError;
            }
            set
            {
                m_buildContinuedOnError = value;
            }
        }

        /// <summary>
        /// The name of the file (without path and extension) to rename the "primary output" file and its map file, if any.
        /// The primary file is the executable or library named as the target of the compile.  Supplemental libraries are not renamed.
        /// </summary>
        public string RenameDestFile
        {
            get
            {
                return m_renameDestFile;
            }
            set
            {
                m_renameDestFile = value;
            }
        }

        /// <summary>
        /// The full path of the folder to which to copy the binaries.
        /// </summary>
        [Required]
        public string CopyDestFolder
        {
            get
            {
                return m_copyDestFolder;
            }
            set
            {
                m_copyDestFolder = value;
            }
        }

        public bool CopyDebugFiles
        {
            get
            {
                return m_copyDebugFiles;
            }
            set
            {
                m_copyDebugFiles = value;
            }
        }
        #endregion

        #region Overrides

        private const string m_VCProjExtension = ".VCPROJ";
        private const string m_CSProjExtension = ".CSPROJ";
        private const string m_VDProjExtension = ".VDPROJ";

        public override bool Execute()
        {
            if ( String.IsNullOrEmpty( this.Solution ) )
            {
                this.Log.LogWarning("CopyBinaries: Solution is not specified.");
                return false;
            }

            m_projectTypeToGetPrimaryFilenameDictionary.Add(m_VCProjExtension, new GetPrimaryFilenameDelegate(GetVCprojPrimaryFilename));
            m_projectTypeToGetPrimaryFilenameDictionary.Add(m_CSProjExtension, new GetPrimaryFilenameDelegate(GetCSprojPrimaryFilename));
            m_projectTypeToGetPrimaryFilenameDictionary.Add(m_VDProjExtension, new GetPrimaryFilenameDelegate(GetVDprojPrimaryFilename));

            m_projectTypeToGetFilenamesToCopyDictionary.Add(m_VCProjExtension, new GetFilenamesToCopyDelegate(GetVCprojFilenamesToCopy));
            m_projectTypeToGetFilenamesToCopyDictionary.Add(m_CSProjExtension, new GetFilenamesToCopyDelegate(GetCSprojFilenamesToCopy));
            m_projectTypeToGetFilenamesToCopyDictionary.Add(m_VDProjExtension, new GetFilenamesToCopyDelegate(GetVDprojFilenamesToCopy));

            // transform this.Solution into the actual file name, if needed
            string strSolution = this.Solution;
            SolutionVersion version;
            if ( !GetSolutionFilename( ref strSolution, out version ) )
            {
                this.Log.LogWarning( "CopyBinaries: Ignoring {0} as the sln file does not exist.", strSolution );
                return true;
            }

            this.Solution = strSolution;

            if ( String.IsNullOrEmpty( this.Configuration ) )
            {
                this.Log.LogWarning( "CopyBinaries: Ignoring {0} as the no configuration was specified.", this.Solution );
                return true;
            }

            // copy the files to the destination
            if ( !String.IsNullOrEmpty( this.CopyDestFolder ) )
            {
                rageStatus obStatus = new rageStatus();
                CopyFiles( out obStatus );
                if ( !obStatus.Success() )
                {
                    this.Log.LogError(obStatus.ErrorString);
                    //TODO:  Have this return false.  Copying binaries will become important for us to making actual releases
                    //out of this build.
                    return true;
                }
            }

            return true;
        }
        #endregion

        #region Private Functions
        /// <summary>
        /// Copies the binaries for the specified solution.
        /// </summary>
        /// <param name="strBuildLog">The verbose log that was created.  If !ShouldKeepBuildLog(), you must delete it yourself when done.</param>
        /// <param name="obStatus">The success of failure of the copy.</param>
        private void CopyFiles( out rageStatus obStatus )
        {
            this.Log.LogMessage( MessageImportance.High, BuildLogTitle( 
                String.Format( "Copying Binaries for {0}", Path.GetFileName( this.Solution ) ), this.Project, this.Configuration, null ) );

            string strHtmlLogFilename = BuildLogFileName(GetMsBuildEnvironmentVariable("CCNetProject"), Path.GetFileNameWithoutExtension(this.Solution), this.Project, this.Configuration, "copy");
            strHtmlLogFilename = Path.Combine( Environment.GetEnvironmentVariable( "TEMP" ), strHtmlLogFilename );

            string strTitle = BuildLogTitle( String.Format( "Copy Binaries Log for {0}", Path.GetFileName( this.Solution ) ),
                this.Project, this.Configuration, null );

            rageHtmlLog obHtmlLog = new rageHtmlLog( strHtmlLogFilename, strTitle );
            obHtmlLog.UpdateFileInRealTime = false;

            List<string> astrErrorsAsText = new List<string>();

            string projectType, projectFilename;
            VisualStudioProjectParser.DetermineProjectTypeAndFilename( this.Solution, this.Project, out projectType, out projectFilename, out obStatus );
            if ( !obStatus.Success() )
            {
                return;
            }

            // get the primary executable (or library)
            string primaryFilename = GetPrimaryFilename( projectType, projectFilename, out obStatus );
            if ( obStatus.Success() )
            {
                if ( File.Exists( primaryFilename ) )
                {
                    List<string> srcFiles, destFiles;
                    GetFilenamesToCopy( projectType, primaryFilename, out srcFiles, out destFiles, out obStatus );
                    if ( obStatus.Success() )
                    {
                        if ( srcFiles.Count == destFiles.Count )
                        {
                            if ( srcFiles.Count == 0 )
                            {
                                string strLogText = String.Format( "No source binaries found to copy for {0}.", Path.GetFileName( this.Solution ) );

                                obHtmlLog.LogEvent( String.Format( "Error: {0}", strLogText ) );
                                astrErrorsAsText.Add( strLogText );
                                this.Log.LogError( strLogText );
                            }
                            else
                            {
                                for ( int i = 0; i < srcFiles.Count; ++i )
                                {
                                    rageFileUtilities.CopyFile( srcFiles[i], destFiles[i], true, out obStatus );
                                    if ( obStatus.Success() )
                                    {
                                        string strLogText = String.Format( "Copied {0} to {1}", srcFiles[i], destFiles[i] );

                                        obHtmlLog.LogEvent( strLogText );
                                        this.Log.LogMessage( MessageImportance.Normal, strLogText );
                                    }
                                    else
                                    {
                                        string strLogText = String.Format( obStatus.ErrorString );

                                        obHtmlLog.LogEvent( String.Format( "Error: {0}", strLogText ) );
                                        astrErrorsAsText.Add( strLogText );
                                        this.Log.LogError( strLogText );
                                    }
                                }
                            }
                        }
                        else
                        {
                            string strLogText = "The source and destination file lists have different lengths.";
                            
                            obHtmlLog.LogEvent( String.Format( "Error: {0}", strLogText ) );
                            astrErrorsAsText.Add( strLogText );
                            this.Log.LogError( strLogText );
                        }
                    }
                    else
                    {
                        obHtmlLog.LogEvent( String.Format( "Error: {0}", obStatus.ErrorString ) );
                        astrErrorsAsText.Add( obStatus.ErrorString );
                        this.Log.LogError( obStatus.ErrorString );
                    }
                }
                else
                {
                    if ( this.BuildContinuedOnError )
                    {
                        string strLogText = null;
                        if ( !String.IsNullOrEmpty( this.Project ) )
                        {
                            strLogText = String.Format( "Ignoring {0} - {1} because {2} does not exist.", 
                                Path.GetFileName( this.Solution ), this.Project, primaryFilename );
                        }
                        else
                        {
                            strLogText = String.Format( "Ignoring {0} because {1} does not exist.",
                                Path.GetFileName( this.Solution ), primaryFilename );
                        }

                        obHtmlLog.LogEvent( String.Format( "Warning: {0}", strLogText ) );
                        this.Log.LogWarning( strLogText );
                    }
                    else
                    {
                        string strLogText = null;
                        if ( !String.IsNullOrEmpty( this.Project ) )
                        {
                            strLogText = String.Format( "Could not find {0}.  {1} - {2} may have failed to compile.",
                                primaryFilename, Path.GetFileName( this.Solution ), this.Project, primaryFilename );
                        }
                        else
                        {
                            strLogText = String.Format( "Could not find {0}.  {1} may have failed to compile.",
                                primaryFilename, Path.GetFileName( this.Solution ) );
                        }

                        obHtmlLog.LogEvent( String.Format( "Error: {0}", strLogText ) );
                        astrErrorsAsText.Add( strLogText );
                        this.Log.LogError( strLogText );
                    }
                }
            }
            else
            {
                obHtmlLog.LogEvent( String.Format( "Error: {0}", obStatus.ErrorString ) );
                astrErrorsAsText.Add( obStatus.ErrorString );
                this.Log.LogError( obStatus.ErrorString );
            }

            if ( astrErrorsAsText.Count > 0 )
            {
                obStatus = new rageStatus( String.Format( "Error(s) copying binaries for {0}.",
                    BuildLogTitle( Path.GetFileName( this.Solution ), this.Project, this.Configuration, null ) ) );

                this.Log.LogMessage( MessageImportance.High, obStatus.ErrorString );
            }
            else
            {
                obStatus = new rageStatus();
                this.Log.LogMessage( MessageImportance.High, "Copy Binaries Complete." );
            }
        }

        /// <summary>
        /// Determines the full path and file a name of the primary output file.
        /// </summary>
        /// <param name="projectType"></param>
        /// <param name="projectFilename"></param>
        /// <param name="obStatus">The success or failure of the operation</param>
        /// <returns>The primary output filename.</returns>
        private string GetPrimaryFilename( string projectType, string projectFilename, out rageStatus obStatus )
        {
            GetPrimaryFilenameDelegate del;
            if ( !m_projectTypeToGetPrimaryFilenameDictionary.TryGetValue( projectType, out del ) )
            {
                obStatus = new rageStatus( String.Format( "The project type '{0}' is not supported.", projectType ) );
                return null;
            }

            return del.Invoke( projectFilename, out obStatus );
        }

        /// <summary>
        /// Parses a .vcproj file to determine the primary filename.
        /// </summary>
        /// <param name="projectFilename">The full path and file name of the project to parse.</param>
        /// <param name="obStatus">The success or failure of the operation</param>
        /// <returns>The primary output filename.</returns>
        private string GetVCprojPrimaryFilename( string projectFilename, out rageStatus obStatus )
        {
            string outputFilename = null;
            string outputFilenameExtension = null;
          
            obStatus = new rageStatus();

            // for PS3, we need to do a little conversion to find the item in the project file
            string strConfig = this.Configuration;
            if ( strConfig.ToUpper().Contains( "PS3" ) )
            {
                string[] split = strConfig.Split( new char[] { '|' } );
                if ( split.Length == 2 )
                {
                    StringBuilder ps3Config = new StringBuilder();

                    if ( split[0].ToUpper().Contains( "PS3" ) )
                    {
                        ps3Config.Append( "SN PS3 " );

                        if ( split[0].ToUpper().Contains( "SNC" ) )
                        {
                            ps3Config.Append( "SNC " );
                        }

                        ps3Config.Append( split[1] );
                        ps3Config.Append( "|Win32" );
                    }
                    else
                    {
                        ps3Config.Append( "SN PS3 " );

                        if ( split[1].ToUpper().Contains( "SNC" ) )
                        {
                            ps3Config.Append( "SNC " );
                        }

                        ps3Config.Append( split[0] );
                        ps3Config.Append( "|Win32" );
                    }

                    strConfig = ps3Config.ToString();
                }
            }
            else if ( this.Configuration.ToUpper().Contains( "RSC" ) || this.Configuration.ToUpper().Contains( "TOOL" ) )
            {
                string[] split = strConfig.Split( new char[] { '|' } );
                if ( split.Length == 2 )
                {
                    StringBuilder rscConfig = new StringBuilder();

                    if ( (split[0].ToUpper() == "RSC" ) || (split[0].ToUpper() == "TOOL" ) )
                    {
                        rscConfig.Append( split[0] );
                        rscConfig.Append( split[1] );
                        rscConfig.Append( "|Win32" );

                        strConfig = rscConfig.ToString();
                    }
                    else if ( (split[1].ToUpper() == "RSC") || (split[1].ToUpper() == "TOOL") )
                    {
                        rscConfig.Append( split[1] );
                        rscConfig.Append( split[0] );
                        rscConfig.Append( "|Win32" );

                        strConfig = rscConfig.ToString();
                    }                    
                }
            }
            else
            {
                // do a little remapping
                strConfig = this.Configuration.Replace( "Mixed Platforms", "Win32" );
            }

            outputFilename = VisualStudioProjectParser.GetOutputFilename(this.Solution, projectFilename, strConfig, ref outputFilenameExtension, out obStatus);

            return outputFilename;
        }

        /// <summary>
        /// Parses a .csproj file to determine the primary filename.
        /// </summary>
        /// <param name="projectFilename">The full path and file name of the project to parse.</param>
        /// <param name="obStatus">The success or failure of the operation</param>
        /// <returns>The primary output filename.</returns>
        private string GetCSprojPrimaryFilename( string projectFilename, out rageStatus obStatus )
        {
            string outputFilename = null;
            string outputFileExtension = null;
            string outputDir = null;

            obStatus = new rageStatus();

            // do a little remapping
            string strConfig = this.Configuration.Replace( "Mixed Platforms", "AnyCPU" );

            TextReader reader = null;
            try
            {
                reader = new StreamReader( projectFilename );

                string line = reader.ReadLine();
                bool inDefaultPropertyGroup = false;
                bool inPropertyGroup = false;
                while ( line != null )
                {
                    if ( !inDefaultPropertyGroup && !inPropertyGroup )
                    {
                        if ( line.Contains( "<PropertyGroup" ) )
                        {
                            if ( line.Contains( "Condition" )
                                && (line.Contains( strConfig ) || line.Contains( this.Configuration.Replace( " ", string.Empty ) )) )
                            {
                                inPropertyGroup = true;
                            }
                            else
                            {
                                inDefaultPropertyGroup = true;
                            }
                        }
                    }
                    else if ( inDefaultPropertyGroup )
                    {
                        if ( line.Contains( "</PropertyGroup>" ) )
                        {
                            inDefaultPropertyGroup = false;

                            if ( (outputFilename != null) && (outputFileExtension != null) && (outputDir != null) )
                            {
                                // we're done
                                break;
                            }
                        }
                        else if ( line.Contains( "OutputType" ) )
                        {
                            string[] split = line.Trim().Split( new char[] { '<', '>' }, StringSplitOptions.RemoveEmptyEntries );
                            if ( split.Length >= 2 )
                            {
                                if ( split[1].ToUpper().Contains( "EXE" ) )
                                {
                                    outputFileExtension = ".exe";
                                }
                                else if ( split[1].ToUpper().Contains( "LIBRARY" ) )
                                {
                                    outputFileExtension = ".dll";
                                }
                            }
                        }
                        else if ( line.Contains( "AssemblyName" ) )
                        {
                            string[] split = line.Trim().Split( new char[] { '<', '>' }, StringSplitOptions.RemoveEmptyEntries );
                            if ( split.Length >= 2 )
                            {
                                outputFilename = split[1];
                            }
                        }
                    }
                    else if ( inPropertyGroup )
                    {
                        if ( line.Contains( "</PropertyGroup>" ) )
                        {
                            inDefaultPropertyGroup = false;

                            if ( (outputFilename != null) && (outputFileExtension != null) && (outputDir != null) )
                            {
                                // we're done
                                break;
                            }
                        }
                        else if ( line.Contains( "OutputPath" ) )
                        {
                            string[] split = line.Trim().Split( new char[] { '<', '>' }, StringSplitOptions.RemoveEmptyEntries );
                            if ( split.Length >= 2 )
                            {
                                outputDir = split[1];
                            }
                        }
                    }

                    line = reader.ReadLine();
                }

                reader.Close();
            }
            catch ( Exception e )
            {
                if ( reader != null )
                {
                    reader.Close();
                    reader = null;
                }

                obStatus = new rageStatus( e.Message );
                return string.Empty;
            }

            if ( outputFilename == null )
            {
                obStatus = new rageStatus( String.Format( "Unable to determine the output file name for {0}.", BuildLogTitle( this.Solution, this.Project, this.Configuration, null ) ) );
                return string.Empty;
            }

            if ( outputFileExtension == null )
            {
                obStatus = new rageStatus( String.Format( "Unable to determine the output file type for {0}.", BuildLogTitle( this.Solution, this.Project, this.Configuration, null ) ) );
                return string.Empty;
            }
            
            if ( outputDir == null )
            {
                obStatus = new rageStatus( String.Format( "Unable to determine the output directory for {0}.", BuildLogTitle( this.Solution, this.Project, this.Configuration, null ) ) );
                return string.Empty;
            }

            string executableFilename = Path.GetFullPath( Path.Combine( Path.GetDirectoryName( projectFilename ), outputDir ) );

            return Path.Combine( executableFilename, outputFilename + outputFileExtension );
        }

        /// <summary>
        /// Parses a .vdproj file to determine the primary filename.
        /// </summary>
        /// <param name="projectFilename">The full path and file name of the project to parse.</param>
        /// <param name="obStatus">The success or failure of the operation</param>
        /// <returns>The primary output filename.</returns>
        private string GetVDprojPrimaryFilename( string projectFilename, out rageStatus obStatus )
        {
            string outputFilename = null;

            obStatus = new rageStatus();

            // do a little remapping
            string strConfig = this.Configuration.Replace( "Mixed Platforms", "AnyCPU" );

            TextReader reader = null;
            try
            {
                reader = new StreamReader( projectFilename );

                Stack<string> sections = new Stack<string>();
                string possibleSectionName = null;

                bool inConfigurations = false;
                bool inOurConfiguration = false;

                string line = reader.ReadLine();
                while ( line != null )
                {
                    if ( line.Trim().StartsWith( "\"" ) && !line.Contains( " = " ) )
                    {
                        possibleSectionName = line.Trim().Trim( new char[] { '"' } );
                    }
                    else if ( (line.Trim() == "{") && (possibleSectionName != null) )
                    {
                        sections.Push( possibleSectionName );

                        if ( !inConfigurations && (possibleSectionName == "Configurations") )
                        {
                            inConfigurations = true;
                        }
                        else if ( inConfigurations && !inOurConfiguration && (possibleSectionName == this.Configuration) )
                        {
                            inOurConfiguration = true;
                        }

                        possibleSectionName = null;
                    }
                    else if ( (line.Trim() == "}") && (possibleSectionName == null) )
                    {
                        string section = sections.Pop();

                        if ( section == this.Configuration )
                        {
                            inOurConfiguration = false;
                        }
                        else if ( section == "Configurations" )
                        {
                            inConfigurations = false;
                        }
                    }
                    else
                    {
                        if ( inOurConfiguration )
                        {
                            if ( line.Trim().StartsWith( "\"OutputFilename\"" ) )
                            {
                                string[] split = line.Split( new char[]{ '"', '=', ':' }, StringSplitOptions.RemoveEmptyEntries );
                                if ( split.Length > 0 )
                                {
                                    outputFilename = split[split.Length - 1];
                                    break;
                                }
                            }
                        }
                    }

                    line = reader.ReadLine();
                }

                reader.Close();
            }
            catch ( Exception e )
            {
                if ( reader != null )
                {
                    reader.Close();
                    reader = null;
                }

                obStatus = new rageStatus( e.Message );
                return string.Empty;
            }

            if ( outputFilename == null )
            {
                obStatus = new rageStatus( String.Format( "Unable to determine the output file name for {0}.", BuildLogTitle( this.Solution, this.Project, this.Configuration, null ) ) );
                return string.Empty;
            }

            return Path.GetFullPath( Path.Combine( Path.GetDirectoryName( projectFilename ), outputFilename ) );
        }

        private void GetFilenamesToCopy( string projectType, string primaryFilename, 
            out List<string> srcFiles, out List<string> destFiles, out rageStatus obStatus )
        {
            srcFiles = new List<string>();
            destFiles = new List<string>();

            GetFilenamesToCopyDelegate del;
            if ( !m_projectTypeToGetFilenamesToCopyDictionary.TryGetValue( projectType, out del ) )
            {
                obStatus = new rageStatus( String.Format( "The project type '{0}' is not supported.", projectType ) );
                return;
            }

            del.Invoke( primaryFilename, srcFiles, destFiles, out obStatus );
        }

        private void GetVCprojFilenamesToCopy( string primaryFilename, List<string> srcFiles, List<string> destFiles, out rageStatus obStatus )
        {
            srcFiles.Add( primaryFilename );

            string mapFilename = string.Empty;
            if ( this.CopyDebugFiles )
            {
                // look for a map file
                mapFilename = Path.ChangeExtension( primaryFilename, ".cmp" );
                if ( File.Exists( mapFilename ) )
                {
                    srcFiles.Add( mapFilename );
                }
                else
                {
                    mapFilename = Path.ChangeExtension( primaryFilename, ".cmpmap" );
                    if ( File.Exists( mapFilename ) )
                    {
                        srcFiles.Add( mapFilename );
                    }
                    else
                    {
                        mapFilename = Path.ChangeExtension( primaryFilename, ".map" );
                        if ( File.Exists( mapFilename ) )
                        {
                            srcFiles.Add( mapFilename );
                        }
                    }
                }
            }

            // look for *.dlm, *.mll, *.dll, and *.pyd
            if ( !this.Configuration.ToUpper().Contains( "360" ) && !this.Configuration.ToUpper().Contains( "PS3" ) )
            {                
                if ( this.Configuration.Contains( "Max9" ) || this.Configuration.Contains( "Max8" ) )
                {
                    string[] dlmFilenames = Directory.GetFiles( Path.GetDirectoryName( primaryFilename ), "*.dlm" );
                    if ( dlmFilenames.Length > 0 )
                    {
                        foreach ( string dlmFilename in dlmFilenames )
                        {
                            if ( dlmFilename.ToUpper() != dlmFilename.ToUpper() )
                            {
                                srcFiles.Add( dlmFilename );
                            }
                        }
                    }
                }
                else if ( this.Configuration.Contains( "7.0Release" ) || this.Configuration.Contains( "8.0Release" )
                    || this.Configuration.Contains( "9.0Release" ) || this.Configuration.Contains( "2008Release" ) )
                {
                    string[] mllFilenames = Directory.GetFiles( Path.GetDirectoryName( primaryFilename ), "*.mll" );
                    if ( mllFilenames.Length > 0 )
                    {
                        foreach ( string mllFilename in mllFilenames )
                        {
                            if ( mllFilename.ToUpper() != primaryFilename.ToUpper() )
                            {
                                srcFiles.Add( mllFilename );
                            }
                        }
                    }
                }
                else
                {
                    string[] dllFilenames = Directory.GetFiles( Path.GetDirectoryName( primaryFilename ), "*.dll" );
                    if ( dllFilenames.Length > 0 )
                    {
                        foreach ( string dllFilename in dllFilenames )
                        {
                            if ( dllFilename.ToUpper() != primaryFilename.ToUpper() )
                            {
                                srcFiles.Add( dllFilename );
                            }
                        }
                    }

                    //Look for any python extensions
                    string[] pydFilenames = Directory.GetFiles( Path.GetDirectoryName( primaryFilename ), "*.pyd" );
                    if ( pydFilenames.Length > 0 )
                    {
                        foreach ( string pydFilename in pydFilenames )
                        {
                            if ( pydFilename.ToUpper() != primaryFilename.ToUpper() )
                            {
                                srcFiles.Add( pydFilename );
                            }
                        }
                    }
                }
            }

            foreach ( string srcFile in srcFiles )
            {
                string destFile = Path.GetFileName( srcFile );
                if ( !String.IsNullOrEmpty( this.RenameDestFile )
                    && ((srcFile == primaryFilename) || (srcFile == mapFilename)) )
                {
                    destFile = this.RenameDestFile + Path.GetExtension( srcFile );
                }

                destFile = Path.GetFullPath( Path.Combine( this.CopyDestFolder, destFile ) );
                destFiles.Add( destFile );
            }

            obStatus = new rageStatus();
        }

        private void GetCSprojFilenamesToCopy( string primaryFilename, List<string> srcFiles, List<string> destFiles, out rageStatus obStatus )
        {
            srcFiles.Add( primaryFilename );

            // look for *.dll and *.pdb
            if ( !this.Configuration.ToUpper().Contains( "360" ) && !this.Configuration.ToUpper().Contains( "PS3" ) )
            {
                string[] dllFilenames = Directory.GetFiles( Path.GetDirectoryName( primaryFilename ), "*.dll" );
                if ( dllFilenames.Length > 0 )
                {
                    foreach ( string dllFilename in dllFilenames )
                    {
                        if ( dllFilename.ToUpper() != primaryFilename.ToUpper() )
                        {
                            srcFiles.Add( dllFilename );
                        }
                    }
                }

                string configFilename = primaryFilename + ".config";
                if ( File.Exists( configFilename ) )
                {
                    srcFiles.Add( configFilename );
                }

                if ( this.CopyDebugFiles )
                {
                    string[] pdbFilenames = Directory.GetFiles( Path.GetDirectoryName( primaryFilename ), "*.pdb" );
                    if ( pdbFilenames.Length > 0 )
                    {
                        foreach ( string pdbFilename in pdbFilenames )
                        {
                            srcFiles.Add( pdbFilename );
                        }
                    }
                }
            }

            foreach ( string srcFile in srcFiles )
            {
                string destFile = Path.GetFileName( srcFile );
                if ( !String.IsNullOrEmpty( this.RenameDestFile ) && (srcFile == primaryFilename) )
                {
                    destFile = this.RenameDestFile + Path.GetExtension( srcFile );
                }

                destFile = Path.GetFullPath( Path.Combine( this.CopyDestFolder, destFile ) );
                destFiles.Add( destFile );
            }

            obStatus = new rageStatus();
        }

        private void GetVDprojFilenamesToCopy( string primaryFilename, List<string> srcFiles, List<string> destFiles, out rageStatus obStatus )
        {
            srcFiles.Add( primaryFilename );

            string destFile = Path.GetFileName( srcFiles[0] );
            if ( !String.IsNullOrEmpty( this.RenameDestFile ) )
            {
                destFile = this.RenameDestFile + Path.GetExtension( srcFiles[0] );
            }

            destFile = Path.GetFullPath( Path.Combine( this.CopyDestFolder, destFile ) );
            destFiles.Add( destFile );

            obStatus = new rageStatus();
        }
    
        #endregion
    }
}
