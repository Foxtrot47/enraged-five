using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Timers;

using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;

using ps3tmapiInterface;

namespace MyTasks
{
    public class ConsoleExecPS3 : Task
    {
        public ConsoleExecPS3()
        {
            m_target.ConnectError += new MessageEventHandler( target_ConnectError );
            m_target.ConnectInfo += new MessageEventHandler( target_ConnectInfo );
            m_target.ConnectStatus += new ConnectStatusEventHandler( target_ConnectStatus );
            m_target.DebugEvent += new DebugEventEventHandler( target_DebugEvent );
            m_target.OutputTTY = true;
            m_target.TTY += new MessageEventHandler( target_TTY );
            m_target.UnhandledCrash += new MessageEventHandler( target_UnhandledCrash );
            m_target.UnitStatus += new UnitStatusEventHandler( target_UnitStatus );
        }

        #region Variables
        private string m_cmdLine = string.Empty;
        private string m_mediaDirectory = string.Empty;
        private string m_ps3TargetAddress = string.Empty;
        private bool m_waitForConnection = true;
        private bool m_failOnError = false;
        private bool m_ignoreErrors = false;
        private bool m_alreadyReset = false;
        private bool m_hasExited = false;
        private bool m_waitingForReset = false;

        private Process m_ps3runProcess = null;
        private PS3Target m_target = new PS3Target();
        #endregion

        #region Properties
        [Required]
        public string PS3RunCommandLine
        {
            get 
            { 
                return m_cmdLine; 
            }
            set 
            { 
                m_cmdLine = value; 

                if ( !m_cmdLine.Contains( "-w " ) )
                {
                    m_cmdLine = "-w " + m_cmdLine;
                }
            }
        }
        public string MediaDirectory
        {
            get 
            { 
                return m_mediaDirectory; 
            }
            set 
            { 
                m_mediaDirectory = value; 
            }
        }
        public string Target
        {
            get 
            { 
                return m_ps3TargetAddress; 
            }
            set 
            { 
                m_ps3TargetAddress = value; 

                if ( m_ps3TargetAddress.StartsWith( "-t " ) )
                {
                    m_ps3TargetAddress = m_ps3TargetAddress.Substring( 2 ).Trim();
                }
            }
        }
        public bool FailOnError
        {
            get 
            { 
                return m_failOnError; 
            }
            set 
            { 
                m_failOnError = value; 
            }
        }
        #endregion

        #region Task Overrides
        public override bool Execute()
        {
            if ( StartPS3RunProcess() )
            {
                this.Log.LogMessage( "Console - " + this.Target );
                this.Log.LogMessage( "cmd - " + m_ps3runProcess.StartInfo.FileName + " " + m_ps3runProcess.StartInfo.Arguments );

                int retryCount = 20;
                m_waitForConnection = true;
                while ( m_waitForConnection )
                {
                    if ( !m_target.Connect( this.Target ) )
                    {
                        Thread.Sleep( 1000 );
                    }

                    --retryCount;
                    if ( retryCount == 0 )
                    {
                        break;
                    }                    
                }

                m_target.Update();

                while ( m_target.IsConnected && !m_hasExited && !m_ps3runProcess.HasExited )
                {
                    m_target.Update();
                }

                this.Log.LogMessage( MessageImportance.Normal, 
                    String.Format( "Done With Test (TargetConnected={0}, HasExited={1}, PS3RunExited={2}, WaitingForReset={3}",
                    m_target.IsConnected, m_hasExited, m_ps3runProcess.HasExited, m_waitingForReset ) );
                
                this.Log.LogMessage( MessageImportance.Normal, "Waiting while rebooting" );

                if ( m_ps3runProcess.HasExited )
                {
                    // just to make sure
                    ResetSystrayRfs();
                }

                // wait for reset, if any
                while ( m_target.IsConnected && (!m_hasExited || m_waitingForReset) )
                {
                    m_target.Update();
                }

                m_target.Dispose();
                m_target = null;

                if ( !m_ps3runProcess.HasExited )
                {
                    this.Log.LogMessage( MessageImportance.Normal, "Ps3run has not exited.  Killing it." );
                    m_ps3runProcess.Kill();
                }
            }            

            if ( this.FailOnError )
            {
                return !this.Log.HasLoggedErrors;
            }
            else
            {
                if ( !m_ignoreErrors )
                {
                    this.Log.LogError( "Text not found:  '***  TESTER IGNORE REST OF ERRORS ***'.  The test is incomplete." );
                }

                return m_ignoreErrors;
            }
        }
        #endregion

        #region Private Functions
        private bool StartPS3RunProcess()
        {
            ProcessStartInfo psInfo = new ProcessStartInfo( "ps3run", "-w " +
                (!String.IsNullOrEmpty( this.Target ) ? "-t " + this.Target + " " : string.Empty) + this.PS3RunCommandLine );
            psInfo.CreateNoWindow = true;
            psInfo.UseShellExecute = false;

            m_ps3runProcess = Process.Start( psInfo );

            if ( m_ps3runProcess == null )
            {
                this.Log.LogError( "Could not start process 'ps3run'" );
                return false;
            }

            return true;
        }

        private void ResetSystrayRfs()
        {
            if ( m_alreadyReset == false )
            {
                m_alreadyReset = true;

                m_target.Reset();

                m_hasExited = true;
            }
        }
        #endregion

        #region PS3Target Event Handlers
        private void target_ConnectError( object sender, MessageEventArgs e )
        {
            if ( !m_ignoreErrors )
            {
                this.Log.LogError( e.Message );
            }
            else
            {
                this.Log.LogWarning( "Error treated as warning: {0}", e.Message );
            }
        }

        private void target_ConnectInfo( object sender, MessageEventArgs e )
        {
            this.Log.LogMessage( MessageImportance.Low, e.Message );
        }

        private void target_ConnectStatus( object sender, ConnectStatusEventArgs e )
        {
            if ( e.Status == ConnectStatus.NOT_CONNECTED )
            {
                if ( !m_waitForConnection )
                {
                    if ( !m_ignoreErrors && !m_alreadyReset )
                    {
                        this.Log.LogError( e.AsString() );
                    }

                    if ( m_waitingForReset )
                    {
                        m_waitingForReset = false;
                        m_hasExited = true;
                    }
                }
                else
                {
                    this.Log.LogMessage( MessageImportance.Normal, e.AsString() );
                }
            }
            else if ( e.Status == ConnectStatus.CONNECTING )
            {
                m_waitForConnection = true;
                this.Log.LogMessage( MessageImportance.Low, e.AsString() );
            }
            else if ( e.Status == ConnectStatus.CONNECTED )
            {
                m_waitForConnection = false;
                this.Log.LogMessage( MessageImportance.Low, e.AsString() );
            }
            else
            {
                this.Log.LogMessage( MessageImportance.Low, e.AsString() );
            }
        }

        private void target_DebugEvent( object sender, DebugEventEventArgs e )
        {
            switch ( e.Event )
            {
                case DebugEvent.PPU_THREAD_CREATE:
                case DebugEvent.PPU_THREAD_EXIT:
                    this.Log.LogMessage( MessageImportance.Low, e.AsString() );
                    break;
                case DebugEvent.PPU_EXP_ALIGNMENT:
                case DebugEvent.PPU_EXP_DABR_MATCH:
                case DebugEvent.PPU_EXP_DATA_HTAB_MISS:
                case DebugEvent.PPU_EXP_DATA_SLB_MISS:
                case DebugEvent.PPU_EXP_FLOAT:
                case DebugEvent.PPU_EXP_ILL_INST:
                case DebugEvent.PPU_EXP_PREV_INT:
                case DebugEvent.PPU_EXP_TEXT_HTAB_MISS:
                case DebugEvent.PPU_EXP_TEXT_SLB_MISS:
                case DebugEvent.PPU_EXP_TRAP:
                    if ( !m_ignoreErrors )
                    {
                        this.Log.LogError( e.AsString() );
                    }
                    else
                    {
                        this.Log.LogWarning( "Error treated as warning: {0}", e.AsString() );
                    }
                    break;
                default:
                    break;
            }
        }

        private void target_TTY( object sender, MessageEventArgs e )
        {
            if ( !this.FailOnError && e.Message.Contains( "***  TESTER IGNORE REST OF ERRORS ***" ) )
            {
                this.Log.LogMessage( MessageImportance.High, "Ignoring all future errors as test is complete" );
                m_ignoreErrors = true;
                ResetSystrayRfs();
                return;
            }

            bool bLogFatalError = false;
            bool bLogError = false;
            bool bLogWarning = false;

            string strUpper = e.Message.ToUpper();
            strUpper = strUpper.Trim();

            if ( strUpper.Contains( "): FATAL ERROR : " ) || strUpper.StartsWith( "FATAL ERROR: " ) )
            {
                bLogFatalError = true;
            }
            if ( strUpper.Contains( "): ERROR : " ) || strUpper.StartsWith( "ERROR: " ) )
            {
                bLogError = true;
            }
            if ( strUpper.Contains( "): WARNING : " ) || strUpper.StartsWith( "WARNING: " ) )
            {
                bLogWarning = true;
            }

            if ( !m_ignoreErrors )
            {
                if ( bLogFatalError || bLogError )
                {
                    this.Log.LogError( e.Message );
                }
                else if ( bLogWarning )
                {
                    this.Log.LogWarning( e.Message );
                }
                else
                {
                    this.Log.LogMessage( MessageImportance.Normal, e.Message );
                }

                if ( bLogFatalError && !this.PS3RunCommandLine.Contains( "-noquits" ) )
                {
                    ResetSystrayRfs();
                }
            }
            else
            {
                if ( bLogFatalError || bLogError )
                {
                    this.Log.LogWarning( "Error treated as warning: {0}", e.Message );
                }
                else if ( bLogWarning )
                {
                    this.Log.LogWarning( e.Message );
                }
                else
                {
                    this.Log.LogMessage( MessageImportance.Normal, e.Message );
                }
            }
        }

        private void target_UnhandledCrash( object sender, MessageEventArgs e )
        {
            if ( !m_ignoreErrors )
            {
                this.Log.LogError( e.Message );
            }
            else
            {
                this.Log.LogWarning( "Error treated as warning: {0}", e.Message );
            }
            
            ResetSystrayRfs();
        }

        private void target_UnitStatus( object sender, UnitStatusEventArgs e )
        {
            if ( e.Status == UnitStatus.NOT_CONNECTED )
            {
                this.Log.LogMessage( MessageImportance.High, e.AsString() );

                if ( !m_ignoreErrors && !m_alreadyReset )
                {
                    this.Log.LogError( "Lost Connection to Target" );
                    Thread.Sleep( 1000 );
                }                

                if ( m_waitingForReset )
                {
                    m_waitingForReset = false;
                    m_hasExited = true;
                }
            }
            else if ( e.Status == UnitStatus.RESETTING )
            {
                this.Log.LogMessage( MessageImportance.Low, e.AsString() );

                m_waitingForReset = true;
                m_alreadyReset = true;
            }
            else if ( e.Status == UnitStatus.RESET )
            {
                this.Log.LogMessage( MessageImportance.Low, e.AsString() );

                if ( m_waitingForReset )
                {
                    m_waitingForReset = false;
                    m_hasExited = true;
                }
            }
            else
            {
                this.Log.LogMessage( MessageImportance.Low, e.AsString() );
            }
        }
        #endregion
    }
}
