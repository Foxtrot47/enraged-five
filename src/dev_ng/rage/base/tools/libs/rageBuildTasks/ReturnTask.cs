using System;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Xml;

using RSG.Base.SCM;
using RSG.Base.Command;
using System.Reflection;
using Microsoft.Win32;

namespace RageBuildTasks
{
    /// <summary>
    /// An MSBuild Task to build a Visual Studio solution given a partial path and name of a solution
    /// </summary>
    public class ReturnTask : RageTaskBase
    {
        #region Variables
		private bool m_success;
        #endregion

        #region Properties
        /// <summary>
        /// A dummy task that MS Build runs to return true/false so calling processes such as Cruise Control
		/// can properly be notified of a success or failure.
        /// </summary>
        [Required]
        public bool Success
        {
            get 
            {
                return m_success;
            }
            set
            {
                m_success = value;
            }
        }
        #endregion

        #region Overrides
        public override bool Execute()
        {
			return m_success;
        }
        #endregion

    }
}
