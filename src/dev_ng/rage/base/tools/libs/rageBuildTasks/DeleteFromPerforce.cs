using System;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using System.Collections.Generic;
using System.IO;
using System.Text;

using RSG.Base.Command;
using RSG.Base.SCM;

namespace RageBuildTasks
{
    public class DeleteFromPerforce : RagePerforceTaskBase
    {
        #region Enums
        enum DeleteFileResult
        {
            Deleted,
            Failed,
            Skipped
        }
        #endregion

        #region Variables
        private ITaskItem[] m_filenames = null;
        private string m_localDirectory = null;
        private bool m_recurseDirectory = true;
        #endregion

        #region Properties
        public ITaskItem[] Filenames
        {
            get
            {
                return m_filenames;
            }
            set
            {
                m_filenames = value;
            }
        }

        /// <summary>
        /// A directory to delete
        /// </summary>
        public string LocalDirectory
        {
            get
            {
                return m_localDirectory;
            }
            set
            {
                m_localDirectory = value;
            }
        }

        /// <summary>
        /// Whether or not to recurse on that directory.  Default is true.
        /// </summary>
        public bool Recurse
        {
            get
            {
                return m_recurseDirectory;
            }
            set
            {
                m_recurseDirectory = value;
            }
        }
        #endregion

        #region Overrides
        public override bool Execute()
        {
            List<string> filenameList = new List<string>();

            if ( this.Filenames != null )
            {
                foreach ( ITaskItem item in this.Filenames )
                {
                    filenameList.Add( item.ToString() );
                }
            }

            if ( !String.IsNullOrEmpty( this.LocalDirectory ) )
            {
                try
                {
                    string[] files = Directory.GetFiles( this.LocalDirectory, "*.*",
                        this.Recurse ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly );
                    if ( (files != null) && (files.Length > 0) )
                    {
                        filenameList.AddRange( files );
                    }
                }
                catch ( System.Exception e )
                {
                    this.Log.LogWarning( "Directory.GetFiles exception: {0}.", e.Message );
                }
            }

            int iDeleted = 0;
            int iFailed = 0;
            int iSkipped = 0;

            filenameList.Sort();
            bool bSuccess = DeleteFiles( filenameList, ref iDeleted, ref iFailed, ref iSkipped );

            this.Log.LogMessage( MessageImportance.High, "{0} Deleted    {1} Failed    {2} Skipped.", iDeleted, iFailed, iSkipped );
            return bSuccess;
        }
        #endregion

        #region Private Functions
        private bool DeleteFiles( List<string> filenames, ref int iDeleted, ref int iFailed, ref int iSkipped )
        {
            foreach ( string filename in filenames )
            {
                switch ( DeleteFile( filename ) )
                {
                    case DeleteFileResult.Deleted:
                        ++iDeleted;
                        break;
                    case DeleteFileResult.Failed:
                        ++iFailed;
                        break;
                    case DeleteFileResult.Skipped:
                        ++iSkipped;
                        break;
                }
            }

            return iFailed == 0;
        }


        private DeleteFileResult DeleteFile( string filename )
        {
            rageStatus obStatus;

            try
            {
                if ( !ragePerforce.FileExists( filename, out obStatus ) )
                {
                    this.Log.LogMessage( MessageImportance.Normal, "Skipping '{0}' because it no longer exists in Perforce.", filename );
                    return DeleteFileResult.Skipped;
                }

                if ( !obStatus.Success() )
                {
                    this.Log.LogWarning( "!FileExists problem:  {0}: '{1}'.", obStatus.ErrorString, filename );
                }
            }
            catch ( System.Exception e )
            {
                this.Log.LogWarning( "!FileExists exception: {0}: '{1}'.", e.Message, filename );
            }

            try
            {
                ragePerforce.DeleteFile( filename, out obStatus );
                if ( obStatus.Success() )
                {
                    this.Log.LogMessage( MessageImportance.Normal, "Deleted '{0}'.", filename );
                    return DeleteFileResult.Deleted;
                }
                else
                {
                    this.Log.LogError( "Unable to delete '{0}': '{1}'.", filename, obStatus.ErrorString );
                    return DeleteFileResult.Failed;
                }
            }
            catch ( System.Exception e )
            {
                this.Log.LogError( "DeleteFile exception: {0}: '{1}'.", e.Message, filename );
                return DeleteFileResult.Failed;
            }
        }
        #endregion
    }
}
