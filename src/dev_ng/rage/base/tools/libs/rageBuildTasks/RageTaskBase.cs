using System;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

using RSG.Base;
using RSG.Base.Command;
using RSG.Base.IO;

namespace RageBuildTasks
{
    public abstract class RageTaskBase : Task
    {
        public enum SolutionVersion
        {
            kNoVersion,
            k2005Solution,
            k2008Solution,
            k2010Solution,
        };

        public RageTaskBase()
        {
            string strCmdLine = Environment.CommandLine;
            int start = strCmdLine.IndexOf( "/p:" );
            if ( start != -1 )
            {
                start += 3;

                int end = strCmdLine.IndexOf( "/p:", start );
                if ( end == -1 )
                {
                    end = strCmdLine.IndexOf( "/l:", start );
                    if ( end == -1 )
                    {
                        end = strCmdLine.Length;
                    }
                }

                --end;
                string strProperties = strCmdLine.Substring( start, end - start );

                string[] strPropertyPairs = strProperties.Split( new char[] { Path.PathSeparator } );
                foreach ( string strPropertyPair in strPropertyPairs )
                {
                    string[] strKeyValue = strPropertyPair.Split( new char[] { '=' } );
                    if ( strKeyValue.Length >= 2 )
                    {
                        string strKey = strKeyValue[0];

                        string strValue = strKeyValue[1];
                        for ( int i = 2; i < strKeyValue.Length; ++i )
                        {
                            strValue = String.Format( "{0}={1}", strValue, strKeyValue[i] );
                        }

                        m_msBuildEnvironmentVariables[strKey] = strValue;
                    }
                }

            }
        }

        #region Variables
        private string m_logDirectory;

        private Dictionary<string, string> m_msBuildEnvironmentVariables = new Dictionary<string, string>();
        #endregion

        #region Properties

        /// <summary>
        /// When specified, log(s) for the task will be created in addition to the MSBuild output.  This will
        /// always be verbose so that the MSBuild verbosity can be lowered without a way to see the full reports.
        /// </summary>
        public string LogDirectory
        {
            get
            {
                return m_logDirectory;
            }
            set
            {
                m_logDirectory = value;

                // make sure our directory exists
                if ( !String.IsNullOrEmpty( m_logDirectory ) )
                {
                    rageFileUtilities.MakeDir( m_logDirectory );
                }
            }
        }

        #endregion

        #region Protected Functions
        protected string GetMsBuildEnvironmentVariable( string strKey )
        {
            string strValue;
            if ( m_msBuildEnvironmentVariables.TryGetValue( strKey, out strValue ) )
            {
                return strValue;
            }

            return string.Empty;
        }

        /// <summary>
        /// If the <see cref="LogDirectory"/> was specified, returns true.  Otherwise, false.
        /// </summary>
        /// <returns><c>true</c> if the <see cref="LogDirectory"/> was specified, otherwise <c>false</c>.</returns>
        protected bool ShouldKeepBuildLog()
        {
            return !String.IsNullOrEmpty( this.LogDirectory );
        }

        static string m_SolutionSuffix = ".sln";
        static string m_2005SolutionSuffix = "_2005" + m_SolutionSuffix;
        static string m_2008SolutionSuffix = "_2008" + m_SolutionSuffix;
        static string m_2010SolutionSuffix = "_2010" + m_SolutionSuffix;

        /// <summary>
        /// Given a partial path and solution name, determines the actual filename
        /// </summary>
        /// <param name="solution">The partial path and solution name.</param>
        /// <returns>true if the solution exists and is returned by reference, otherwise false</returns>
        protected bool GetSolutionFilename( ref string solution, out SolutionVersion versionType )
        {
            solution = Path.GetFullPath( solution );
            versionType = SolutionVersion.kNoVersion;

            //If the solution has an extension already
            if (String.Compare(Path.GetExtension(solution), m_SolutionSuffix, true) == 0)
            {
                if (solution.EndsWith(m_2005SolutionSuffix) == true)
                    versionType = SolutionVersion.k2005Solution;
                else if (solution.EndsWith(m_2008SolutionSuffix) == true)
                    versionType = SolutionVersion.k2008Solution;
                else if (solution.EndsWith(m_2010SolutionSuffix) == true)
                    versionType = SolutionVersion.k2010Solution;

                if (versionType != SolutionVersion.kNoVersion)
                    return true;
            }

            string rawSolutionName = Path.ChangeExtension(solution, null);

            //Find 2010 projects first, then 2008 projects, then 2005, then any nondescript one.
            string solution2010 = rawSolutionName + m_2010SolutionSuffix;
            if (File.Exists(solution2010))
            {
                solution = solution2010;
                versionType = SolutionVersion.k2010Solution;
                return true;
            }

            string solution2008 = rawSolutionName + m_2008SolutionSuffix;
            if (File.Exists(solution2008))
            {
                solution = solution2008;
                versionType = SolutionVersion.k2008Solution;
                return true;
            }

            string solution2005 = rawSolutionName + m_2005SolutionSuffix;
            if (File.Exists(solution2005))
            {
                solution = solution2005;
                versionType = SolutionVersion.k2005Solution;
                return true;
            }

            string solutionNoVersion = rawSolutionName + m_SolutionSuffix;
            if (File.Exists(solutionNoVersion))
            {
                solution = solutionNoVersion;

                //Resort to reading the first line of the solution file.
                StreamReader sr = File.OpenText(solution);
                const int numLinesToCheck = 2;  //Some solutions have newlines at the top.  Check the first three lines for a signature.
                for (int lineIndex = 0; lineIndex < numLinesToCheck; ++lineIndex)
                {
                    if (sr.EndOfStream == false)
                    {
                        string line = sr.ReadLine();

                        if (String.IsNullOrEmpty(line) == false)
                        {
                            if (line == "Microsoft Visual Studio Solution File, Format Version 9.00")
                                versionType = SolutionVersion.k2005Solution;
                            else if (line == "Microsoft Visual Studio Solution File, Format Version 10.00")
                                versionType = SolutionVersion.k2008Solution;
                            else if (line == "Microsoft Visual Studio Solution File, Format Version 11.00")
                                versionType = SolutionVersion.k2010Solution;
                        }
                    }
                }

                sr.Close();

                if ( versionType == SolutionVersion.kNoVersion )
                    return false;

                return true;
            }

            //Unable to find a correct solution!
            return false;
        }

        static string m_ProjectSuffix = ".vcproj";
        static string m_ProjectXSuffix = ".vcxproj";
        static string m_2005ProjectSuffix = "_2005" + m_ProjectSuffix;
        static string m_2008ProjectSuffix = "_2008" + m_ProjectSuffix;
        static string m_2010ProjectSuffix = "_2010" + m_ProjectXSuffix;


        private bool GetProjectRelativePath(string solutionPath, string projectName, out string relativePath)
        {
            relativePath = null;

            if (File.Exists(solutionPath) == false)
            {
                return false;
            }

            string loweredProjectName = projectName.ToLower();
            StreamReader inStream = File.OpenText(solutionPath);
            string inString = inStream.ReadLine();
            while(inString != null)
            {
                if (inString.ToLower().Contains(loweredProjectName) == true)
                {
                    //The following line is in the format:
                    //Project("{8BC9CEB8-8B4A-11D0-8D11-00A0C91BC942}") = "MetadataEditor", "..\MetadataEditor\MetadataEditor_2005.vcproj", "{4F18D2F2-925E-4788-ADDD-4B45404D0352}"
                    //
                    string[] splitByComma = inString.Split(',');
                    if ( splitByComma.Length > 1 )
                    {
                        relativePath = splitByComma[1];
                        relativePath = relativePath.Trim();
                        relativePath = relativePath.Trim('"');
                        return true;
                    }
                }
                inString = inStream.ReadLine();
            }

            return false;
        }

        /// <summary>
        /// Given a partial project path, determines the actual filename of the project.
        /// Use the version of the solution to assume which project is used, based on versions.
        /// </summary>
        protected bool GetProjectFilename(string solutionPath, string projectName, out string outProjectPath)
        {
            outProjectPath = null;

            SolutionVersion version;
            if (GetSolutionFilename(ref solutionPath, out version) == false)
            {
                return false;
            }

            if (String.IsNullOrEmpty(projectName) == true)
            {
                return true;
            }

            string basePath = Path.GetDirectoryName(solutionPath);
            string relativePathToProject;
            if (GetProjectRelativePath(solutionPath, projectName, out relativePathToProject) == false)
            {
                return false;
            }

            string projectPath = Path.Combine(basePath, relativePathToProject);
        
            if ( File.Exists(projectPath) == true )
            {
                outProjectPath = projectPath;
                return true;
            }

            return false;
        }

        /// <summary>
        /// Given the solution of a particular Visual Studio type, return the path to the Visual Studio executable.
        /// </summary>
        /// <param name="solution">The partial path and solution name.</param>
        /// <returns>string name of the Visual Studio executable; nil if it is of an indeterminable version.</returns>
        protected string GetDevEnvExecutable(string solution)
        {
            SolutionVersion version;
            if (GetSolutionFilename(ref solution, out version) == false)
                return null;

            string devEnvPath = null;
            if ( version == SolutionVersion.k2005Solution )
            {
                devEnvPath = "C:\\Program Files\\Microsoft Visual Studio 8\\Common7\\ide\\devenv.com";
                if (File.Exists(devEnvPath) == false)
                    devEnvPath = "C:\\Program Files (x86)\\Microsoft Visual Studio 8\\Common7\\ide\\devenv.com";
            }
            else if ( version == SolutionVersion.k2008Solution )
            {
                devEnvPath = "C:\\Program Files\\Microsoft Visual Studio 9.0\\Common7\\ide\\devenv.com";
                if (File.Exists(devEnvPath) == false)
                    devEnvPath = "C:\\Program Files (x86)\\Microsoft Visual Studio 9.0\\Common7\\ide\\devenv.com";
            }
            else if ( version == SolutionVersion.k2010Solution
                    || version == SolutionVersion.kNoVersion )
            {
                devEnvPath = "C:\\Program Files\\Microsoft Visual Studio 10.0\\Common7\\ide\\devenv.com";
                if (File.Exists(devEnvPath) == false)
                    devEnvPath = "C:\\Program Files (x86)\\Microsoft Visual Studio 10.0\\Common7\\ide\\devenv.com";
            }

            if (File.Exists(devEnvPath) == false)
            {
                this.Log.LogError(String.Format("Unable to find " + devEnvPath + "!  Aborting task..."));
                return null;
            }

            return devEnvPath;
        }

        /// <summary>
        /// Concatenates the given non-null and non-empty string parts into a Log Title string.  
        /// </summary>
        /// <param name="part1"></param>
        /// <param name="part2"></param>
        /// <param name="part3"></param>
        /// <param name="part4"></param>
        /// <returns>The log title.</returns>
        protected string BuildLogTitle( string part1, string part2, string part3, string part4 )
        {
            StringBuilder strLogTitle = new StringBuilder( part1 );

            if ( !String.IsNullOrEmpty( part2 ) )
            {
                strLogTitle.Append( " - " );
                strLogTitle.Append( part2 );
            }

            if ( !String.IsNullOrEmpty( part3 ) )
            {
                strLogTitle.Append( " - " );
                strLogTitle.Append( part3 );
            }

            if ( !String.IsNullOrEmpty( part4 ) )
            {
                strLogTitle.Append( " - " );
                strLogTitle.Append( part4 );
            }

            return strLogTitle.ToString();
        }

        /// <summary>
        /// Concatenates the given non-null and non-empty string parts into a log html filename, prefixed with a time-based filename.
        /// The resulting string is cleaned of illegal filename characters before returning.
        /// </summary>
        /// <param name="part1"></param>
        /// <param name="part2"></param>
        /// <param name="part3"></param>
        /// <param name="part4"></param>
        /// <param name="part5"></param>
        /// <returns>The log filename.</returns>
        protected string BuildLogFileName( string part1, string part2, string part3, string part4, string part5 )
        {
            StringBuilder strLogFileName = new StringBuilder( rageFileUtilities.GenerateTimeBasedFilename() );

            if ( !String.IsNullOrEmpty( part1 ) )
            {
                strLogFileName.Append( '_' );
                strLogFileName.Append( part1.Replace( ' ', '_' ) );
            }

            if ( !String.IsNullOrEmpty( part2 ) )
            {
                strLogFileName.Append( '_' );
                strLogFileName.Append( part2.Replace( ' ', '_' ) );
            }

            if ( !String.IsNullOrEmpty( part3 ) )
            {
                strLogFileName.Append( '_' );
                strLogFileName.Append( part3.Replace( ' ', '_' ) );
            }

            if ( !String.IsNullOrEmpty( part4 ) )
            {
                strLogFileName.Append( '_' );
                strLogFileName.Append( part4.Replace( ' ', '_' ) );
            }

            if ( !String.IsNullOrEmpty( part5 ) )
            {
                strLogFileName.Append( '_' );
                strLogFileName.Append( part5.Replace( ' ', '_' ) );
            }

            strLogFileName.Append( ".html" );

            return rageFileUtilities.CleanStringOfBadCharacters( strLogFileName.ToString() );
        }
        #endregion
    }
}
