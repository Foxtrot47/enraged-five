using System;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using System.Collections.Generic;
using System.IO;
using System.Text;

using RSG.Base;
using RSG.Base.Command;
using RSG.Base.IO;
using RSG.Base.Logging;

namespace RageBuildTasks
{
    public class CleanDebugFiles : RageTaskBase
    {
        #region Variables
        private string m_folder;
        #endregion

        #region Properties
        /// <summary>
        /// The folder to clean recursively
        /// </summary>
        [Required]
        public string Folder
        {
            get
            {
                return m_folder;
            }
            set
            {
                m_folder = value;
            }
        }
        #endregion

        public override bool Execute()
        {
            if ( String.IsNullOrEmpty( this.Folder ) )
            {
                return false;
            }

            this.Folder = Path.GetFullPath( this.Folder );

            rageStatus obStatus;

            DeleteDebugFiles( out obStatus );
            if ( !obStatus.Success() )
            {
                return false;
            }

            return true;
        }

        #region Private Functions
        private void DeleteDebugFiles( out rageStatus obStatus )
        {
            this.Log.LogMessage( MessageImportance.High, BuildLogTitle(
                String.Format( "Deleting debug binaries in '{0}'.", this.Folder ), null, null, null ) );

            string[] astrDirParts = this.Folder.Split( new char[] { '\\', '/' }, StringSplitOptions.RemoveEmptyEntries );
            string strHtmlLogFilename = BuildLogFileName(GetMsBuildEnvironmentVariable("CCNetProject"), astrDirParts[astrDirParts.Length - 1], "delete", null, null);
            strHtmlLogFilename = Path.Combine( Environment.GetEnvironmentVariable( "TEMP" ), strHtmlLogFilename );

            string strTitle = BuildLogTitle( String.Format( "Clean Debug Files Log for '{0}'", this.Folder ),
                null, null, null );

            rageHtmlLog obHtmlLog = new rageHtmlLog( strHtmlLogFilename, strTitle );
            obHtmlLog.UpdateFileInRealTime = false;

            List<string> astrErrorsAsText = new List<string>();

            DeleteDebugFiles( "*.map", astrErrorsAsText );
            DeleteDebugFiles( "*.cmpmap", astrErrorsAsText );
            DeleteDebugFiles( "*.cmp", astrErrorsAsText );
            DeleteDebugFiles( "*.pdb", astrErrorsAsText );

            if ( astrErrorsAsText.Count > 0 )
            {
                obStatus = new rageStatus( String.Format( "Error(s) deleting debug files from {0}.", this.Folder ) );
                this.Log.LogError( obStatus.ErrorString );
            }
            else
            {
                obStatus = new rageStatus();
                this.Log.LogMessage( MessageImportance.High, "Clean Debug Files Successful." );
            }
        }

        private void DeleteDebugFiles( string strSearchPattern, List<string> astrErrorsAsText )
        {
            string[] astrFilenames = Directory.GetFiles( this.Folder, strSearchPattern, SearchOption.AllDirectories );
            foreach ( string strFilename in astrFilenames )
            {
                // Only delete if the file is writable.  If the file is read-only, it is probably checked into Perforce.
                FileAttributes attributes = File.GetAttributes( strFilename );
                if ( (attributes & FileAttributes.ReadOnly) != FileAttributes.ReadOnly )
                {
                    rageStatus obStatus;

                    rageFileUtilities.DeleteLocalFile( strFilename, out obStatus );
                    if ( obStatus.Success() )
                    {
                        string strLogText = String.Format( "Deleted {0}.", strFilename );
                        this.Log.LogMessage( MessageImportance.Normal, strLogText );
                    }
                    else
                    {
                        astrErrorsAsText.Add( obStatus.ErrorString );
                        this.Log.LogError( obStatus.ErrorString );
                    }
                }
            }
        }
        #endregion
    }
}
