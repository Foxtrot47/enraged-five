using System;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Xml;

using RSG.Base;
using RSG.Base.Command;
using System.Reflection;
using Microsoft.Win32;

namespace RageBuildTasks
{
    /// <summary>
    /// An MSBuild Task to build a Visual Studio solution given a partial path and name of a solution
    /// </summary>
    public class BuildSolution : RageTaskBase
    {
        #region Variables
        private string m_solution;
        private string m_project;
        private string m_configuration;
        private bool m_compileWithDevenv = false;
        private bool m_noVSI = false;
        private bool m_clean = false;
        private bool m_rebuild = false;
        private bool m_disabled = false;
        private string[] m_checkOutFiles;
        private static bool m_success = true;
        #endregion

        #region Properties
        /// <summary>
        /// The solution to build.  At minimum, the format must be that of the old tools.txt:  
        /// c:\soft\rage\suite\samples\tools\rageViewer\rageViewer.  If needed, "_2005.sln" or ".sln" will be appended.
        /// It's best if the path root is specified, but should also work if the path is relative to the current 
        /// directory (or the CruiseControl config's WorkingDirectory).
        /// </summary>
        [Required]
        public string Solution
        {
            get
            {
                return m_solution;
            }
            set
            {
                m_solution = value;
            }
        }
        
        /// <summary>
        /// The project to compile, if any.
        /// </summary>
        public string Project
        {
            get
            {
                return m_project;
            }
            set
            {
                m_project = value;
            }
        }

        /// <summary>
        /// The build configuration.
        /// </summary>
        [Required]
        public string Configuration
        {
            get
            {
                return m_configuration;
            }
            set
            {
                m_configuration = value;
            }
        }

        /// <summary>
        /// Whether or not we can use the appropriate Distributed Build System to compile the solution.
        /// </summary>
        public bool CompileWithDevenv
        {
            get
            {
                return m_compileWithDevenv;
            }
            set
            {
                m_compileWithDevenv = value;
            }
        }

        public bool NoVSI
        {
            get
            {
                return m_noVSI;
            }
            set
            {
                m_noVSI = value;
            }
        }

        public bool Clean
        {
            get
            {
                return m_clean;
            }
            set
            {
                m_clean = value;
            }
        }

        public bool Rebuild
        {
            get
            {
                return m_rebuild;
            }
            set
            {
                m_rebuild = value;
            }
        }

        public bool Disabled
        {
            get 
            {
                return m_disabled;
            }
            set
            {
                m_disabled = value;
            }
        }

        [Output]
        public string BuildSuccess
        {
            get 
            {
                if (m_success == true)
                    return "true";
                else
                    return "false";
            }
            set
            {
                if (String.Compare(value, "true", true) == 0)
                    m_success = true;
                else
                    m_success = false;
            }
        }

        public string[] CheckOutFiles
        {
            get { return m_checkOutFiles; }
            set { m_checkOutFiles = value; }
        }

        public bool IsSuccessful
        {
            get
            {
                return m_success;
            }
            set
            {
                m_success = value;
            }
        }
        #endregion

        #region Overrides
        public override bool Execute()
        {
            if (this.Disabled == true)
            {
                this.Log.LogWarning("Solution {0}|{1} is disabled.", this.Solution, this.Configuration);
                return true;
            }

            if ( String.IsNullOrEmpty( this.Solution ) )
            {
                IsSuccessful = false;
                return false;
            }

            if ( this.CheckOutFiles != null )
            {
                GeneralInfo generalInfo = new GeneralInfo();

                if ( this.CheckOutFiles.Length == 1 )
                {
                    this.CheckOutFiles = this.CheckOutFiles[0].Split(';');
                }

                foreach (string file in this.CheckOutFiles)
                {
                    CheckOutFile checkOutFileTask = new CheckOutFile();
                    checkOutFileTask.Execute(generalInfo.Client, generalInfo.Port, generalInfo.User, file);
                }
            }

            // transform this.Solution into the actual file name, if needed
            string strSolution = this.Solution;
            SolutionVersion version;
            if ( !GetSolutionFilename( ref strSolution, out version ) )
            {
                if ( version == SolutionVersion.kNoVersion )
                    this.Log.LogError("BuildSolution: Visual Studio solution for {0} could not be determined.", strSolution);
                else
                    this.Log.LogError( "BuildSolution: {0} does not exist.", strSolution );

                IsSuccessful = false;
                return true;
            }

            this.Solution = strSolution;

            if ( String.IsNullOrEmpty( this.Configuration ) )
            {
                this.Log.LogError( "BuildRageSolution: No configuration was specified for {0}.", this.Solution );
                IsSuccessful = false;
                return true;
            }

            rageStatus obStatus;

            string[] configurationList = this.Configuration.Split(";".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            foreach(string configuration in configurationList)
            {
                DateTime startTime = DateTime.Now;

                // build the solution
                BuildProject(configuration, out obStatus);
                if (!obStatus.Success())
                {
                    //Set the IsSuccessful property in the MS Build as false.  Later on, MS Build will notify
                    //Cruise Control on success/failure using this variable.
                    //
                    IsSuccessful = false;

                    return true;
                }

                DateTime endTime = DateTime.Now;
                TimeSpan difference = endTime - startTime;

                this.Log.LogMessage("Time Taken: " + difference.ToString());
            }

            return true;
        }
        #endregion

        #region Private Functions

        /// <summary>
        /// Acquires the command and arguments to call the devenv processes.
        /// </summary>
        private void GetDevEnvBuildCommand(string configuration, out string strCmd, out StringBuilder strArgs)
        {
             // Create command for the normal Visual Studio environment.
            strArgs = new StringBuilder();
            strCmd = GetDevEnvExecutable(this.Solution);

            strArgs.Append("\"");
            strArgs.Append(this.Solution);

            if ( this.Clean )
            {
                strArgs.Append( "\" /clean \"" );
            }
            else if (this.Rebuild == true)
            {
                strArgs.Append("\" /rebuild \"");
            }
            else
            {
                strArgs.Append("\" /build \"");
            }

            strArgs.Append(configuration);
            strArgs.Append("\"");

            if (!String.IsNullOrEmpty(this.Project))
            {
                strArgs.Append(" /Project \"");
                strArgs.Append(this.Project);
                strArgs.Append("\"");
            }
        }

        /// <summary>
        /// Builds the project with the given parameters.
        /// </summary>
        /// <param name="strBuildLog">The verbose log that was created.  if !ShouldKeepBuildLog(), you must delete it yourself when done.</param>
        /// <param name="obStatus">The success of failure of the build.</param>
        private void BuildProject(string configuration, out rageStatus obStatus )
        {
            Environment.SetEnvironmentVariable( "XBECOPY_SUPPRESS_COPY", "1" );

            // Construct build command
            string strCmd = string.Empty;
            StringBuilder strArgs = new StringBuilder();
            bool bUseIncrediBuild = !this.CompileWithDevenv && !configuration.ToUpper().Contains( "PS3" );
            bool bUseVsi = !this.NoVSI && configuration.ToUpper().Contains("PS3");
            bool bVsiUseDbs = !this.CompileWithDevenv && bUseVsi;

            if ( bUseIncrediBuild )
            {
                string incrediBuildPath = "C:\\Program Files\\Xoreax\\IncrediBuild\\BuildConsole.exe";
                if ( File.Exists(incrediBuildPath) == false )
                    incrediBuildPath = "C:\\Program Files (x86)\\Xoreax\\IncrediBuild\\BuildConsole.exe";

                if ( File.Exists(incrediBuildPath) == false)
                {
                    this.Log.LogError(String.Format("Unable to find IncrediBuild application.  Switching to Visual Studio."));
                    GetDevEnvBuildCommand(configuration, out strCmd, out strArgs);
                }
                else
                {
                    strCmd = "\"" + incrediBuildPath + "\"";

                    strArgs.Append("/SHOWAGENT ");

                    if (this.Rebuild == true)
                        strArgs.Append("/rebuild ");
                    else if (this.Clean == true)
                        strArgs.Append("/clean ");
                    else
                        strArgs.Append("/build ");

                    strArgs.Append("/cfg=\"");
                    strArgs.Append(configuration);
                    strArgs.Append("\"");

                    if (!String.IsNullOrEmpty(this.Project))
                    {
                        string projectName = null;
                        if (GetProjectFilename(this.Solution, this.Project, out projectName) == false)
                            projectName = this.Project;
                        else
                        {
                            //IncrediBuild is expecting only the name of the project as it is referenced
                            //in the solution.  By the standard, this requires the name to be like "projectName_2008".
                            projectName = Path.GetFileNameWithoutExtension(projectName);
                        }

                        strArgs.Append(" /prj=\"");
                        strArgs.Append(projectName);
                        strArgs.Append("\"");
                    }

                    strArgs.Append(" \"");
                    strArgs.Append(this.Solution);
                    strArgs.Append("\"");
                }
            }
            else if ( bUseVsi )
            {
                string vsiCommand = "C:\\Program Files\\SN Systems\\Common\\VSI\\bin\\vsibuild.exe";
                if (File.Exists(vsiCommand) == false)
                     vsiCommand = "C:\\Program Files (x86)\\SN Systems\\Common\\VSI\\bin\\vsibuild.exe";

                 if (File.Exists(vsiCommand) == false)
                {
                    string errorMessage = "Unable to find SN Systems vsibuild.exe application.  Unable to continue.";
                    this.Log.LogError(errorMessage);
                    obStatus = new rageStatus(errorMessage);
                    return;
                }

                strCmd = "\"" + vsiCommand + "\"";

                strArgs.Append( "\"" );
                strArgs.Append( this.Solution );
                strArgs.Append( "\" \"" );
                strArgs.Append(configuration);
                strArgs.Append( "\"" );

                if ( !String.IsNullOrEmpty( this.Project ) )
                {
                    strArgs.Append( " /project \"" );
                    strArgs.Append( this.Project );
                    strArgs.Append( "\"" );
                }

                if ( bVsiUseDbs )
                {
                    bool bVsiUseIncredibuild = false;

                    // Try to determine which one is better
                    try
                    {
                        RegistryKey keyValue = (RegistryKey) Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\VisualStudio\8.0\Addins\VSIAddIn.Connect8", "AboutBoxDetails", null);
                        if (keyValue != null)
                        {
                            string valString = keyValue.ToString();
                            int indexOf = valString.IndexOf( "Version" );
                            if ( indexOf != -1 )
                            {
                                string verString = valString.Substring( indexOf + 8 );
                                
                                indexOf = 0;
                                while ( (indexOf < verString.Length) && !Char.IsWhiteSpace( verString[indexOf] ) )
                                {
                                    ++indexOf;
                                }

                                if ( indexOf < verString.Length )
                                {
                                    verString = verString.Substring( 0, indexOf );

                                    string[] verStringSplit = verString.Split( new char[] { '.' } );
                                    if ( verStringSplit.Length >= 3 )
                                    {
                                        int major = int.Parse( verStringSplit[0] );
                                        int majorRevision = int.Parse( verStringSplit[1] );
                                        int minor = int.Parse( verStringSplit[2] );
                                        if ( (major > 1) 
                                            || ((major == 1) && (majorRevision > 8))
                                            || ((major == 1) && (majorRevision == 8) && (minor > 3)))
                                        {
                                            bVsiUseIncredibuild = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch 
                    {

                    }

                    if ( bVsiUseIncredibuild )
                    {
                        strArgs.Append( " /incredi" );
                    }
                    else
                    {
                        strArgs.Append( " /sn-dbs" );
                    }
                }
            }
            else
            {
                GetDevEnvBuildCommand(configuration, out strCmd, out strArgs);
            }

            if ( String.IsNullOrEmpty(strCmd) == true)
            {
                string errorMessage = "Resulting command for BuildSolution task is empty or null.  Unable to continue...";
                this.Log.LogError(errorMessage);
                obStatus = new rageStatus(errorMessage);
                return;
            }

            // Build it            
            rageExecuteCommand obCommand = new rageExecuteCommand();
            obCommand.Command = strCmd;
            obCommand.Arguments = strArgs.ToString();
            obCommand.TimeOutInSeconds = 2.0f * 60.0f * 60.0f;		// Time out after two hours
            obCommand.TimeOutWithoutOutputInSeconds = 15.0f * 60.0f;		// Time out after three minutes
            obCommand.UpdateLogFileInRealTime = false;
            obCommand.LogFilename = Path.Combine( Environment.GetEnvironmentVariable( "TEMP" ),
                BuildLogFileName(GetMsBuildEnvironmentVariable("CCNetProject"), Path.GetFileNameWithoutExtension(this.Solution), this.Project, configuration, null));

            obCommand.EchoToConsole = false;
            obCommand.RemoveLog = true;
            obCommand.UseBusySpinner = false;

            this.Log.LogMessage( MessageImportance.High, obCommand.GetDosCommand() );

            obCommand.Execute( out obStatus );

            // Ok, build complete, did it work?
            obStatus = new rageStatus();

            // Look back through log to find conclusion
            List<string> astrBuildLogAsText = obCommand.GetLogAsArray();
            SolutionOutput slnOutput = SolutionOutput.Parse(this.Solution, configuration, astrBuildLogAsText);

            string errorString = "";
            string warningString = "";
            bool hasErrors = false;
            bool hasWarnings = false;
            const int numExtraLines = 1;
            for (int lineIndex = 0; lineIndex < slnOutput.OutputLines.Count; ++lineIndex)
            {
                OutputLine outLine = slnOutput.OutputLines[lineIndex];
                if (outLine.Logged == true)
                    continue;

                switch (outLine.Type)
                {
                    case OutputLine.OutputType.Error:
                    case OutputLine.OutputType.WarningTreatedAsError:
                    case OutputLine.OutputType.WarningsTreatedAsErrors:
                        {
                            if (outLine.Text.Contains("Failed to connect to Primary Coordinator") == true)
                                continue;

                            errorString += outLine.Text + "\n";
                            for (int nextLineIndex = 0; nextLineIndex < numExtraLines; ++nextLineIndex)
                            {
                                int arrayIndex = nextLineIndex + lineIndex + 1;
                                if (arrayIndex < slnOutput.OutputLines.Count)
                                {
                                    OutputLine nextLine = slnOutput.OutputLines[arrayIndex];
                                    errorString += nextLine.Text + "\n";
                                    nextLine.Logged = true;
                                }
                            }

                            hasErrors = true;
                            outLine.Logged = true;
                        }
                        break;
                    case OutputLine.OutputType.Warning:
                        {
                            warningString += outLine.Text + "\n";
                            for (int nextLineIndex = 0; nextLineIndex < numExtraLines; ++nextLineIndex)
                            {
                                int arrayIndex = nextLineIndex + lineIndex + 1;
                                if (arrayIndex < slnOutput.OutputLines.Count)
                                {
                                    OutputLine nextLine = slnOutput.OutputLines[arrayIndex];
                                    warningString += nextLine.Text + "\n";
                                    nextLine.Logged = true;
                                }
                            }

                            hasWarnings = true;
                            outLine.Logged = true;
                        }
                        break;
                    case OutputLine.OutputType.Normal:
                        {
                            this.Log.LogMessage(MessageImportance.Normal, outLine.Text);
                            outLine.Logged = true;
                        }
                        break;
                }
            }

            string solutionHeader = null;
            if (String.IsNullOrEmpty(this.Project) == false)
            {
                solutionHeader = String.Format("Solution: {0}, Project: {1}, Configuration: {2}:", this.Solution, this.Project, configuration);
            }
            else
            {
                solutionHeader = String.Format("Solution: {0}, Configuration: {1}:", this.Solution, configuration);
            }

            if ( hasErrors == true )
            {
                this.Log.LogError(solutionHeader);
                this.Log.LogError(errorString);
            }
            else if ( slnOutput.FailCount != 0 )
            {
                // Make sure we print out an error, if one occurred even though we may not have found an error string.
                this.Log.LogError(obStatus.ErrorString);
            }

            if (hasWarnings == true)
            {
                this.Log.LogWarning(solutionHeader);
                this.Log.LogWarning(warningString);
            }

            if (slnOutput.FailCount != 0 || hasErrors == true)
            {
                obStatus = new rageStatus(String.Format("{0}, {1}: Error occurred during build.", Path.GetFileName(this.Solution), configuration));
                if ( (slnOutput.SucceedCount == -1) && (slnOutput.FailCount == -1) && (slnOutput.SkipCount == -1) )
                {
                    obStatus = new rageStatus(
                        String.Format("{0}, {1}: The build ended unexpectedly.", Path.GetFileName(this.Solution), configuration));
                }
            }
 
            this.Log.LogMessage( MessageImportance.High, "{0} Succeeded    {1} Failed    {2} Skipped.",
                slnOutput.SucceedCount, slnOutput.FailCount, slnOutput.SkipCount );

            Environment.SetEnvironmentVariable( "XBECOPY_SUPPRESS_COPY", "0" );
        }
        #endregion
    }

    class OutputLine
    {
        public OutputLine()
        {

        }

        public OutputLine( OutputType outputType, string filename, int lineNumber, string text,
            bool processTimedOut, int projectID )
        {
            m_outputType = outputType;
            m_filename = filename;
            m_lineNumber = lineNumber;
            m_text = text;
            m_processTimedOut = processTimedOut;
            m_projectID = projectID;
            m_logged = false;
        }

        public enum OutputType
        {
            Normal,
            Warning,
            Error,
            WarningTreatedAsError,
            WarningsTreatedAsErrors
        }

        #region Variables
        private OutputType m_outputType = OutputType.Normal;
        private string m_filename = null;
        private int m_lineNumber = -1;
        private string m_text = null;
        private bool m_processTimedOut = false;
        private int m_projectID = -1;
        private bool m_logged;

        private static Regex sm_fileLineRegex = new Regex( "\\s*(?<File>[a-zA-Z_0-9\\x2F\\x2E\\x3A\\x5C\\x20]*)\\x28(?<lineNum>[0-9]+)\\x29\\s*:\\s*(?<Text>(.)*)", RegexOptions.Compiled );
        #endregion

        #region Properties
        public OutputType Type
        {
            get
            {
                return m_outputType;
            }
            set
            {
                m_outputType = value;
            }
        }

        public string Filename
        {
            get
            {
                return m_filename;
            }
            set
            {
                m_filename = value;
            }
        }

        public int LineNumber
        {
            get
            {
                return m_lineNumber;
            }
            set
            {
                m_lineNumber = value;
            }
        }

        public string Text
        {
            get
            {
                return m_text;
            }
            set
            {
                m_text = value;
            }
        }

        public bool Logged
        {
            get
            {
                return m_logged;
            }
            set
            {
                m_logged = value;
            }
        }

        public bool ProcessTimedOut
        {
            get
            {
                return m_processTimedOut;
            }
            set
            {
                m_processTimedOut = value;
            }
        }

        public int ProjectID
        {
            get
            {
                return m_projectID;
            }
            set
            {
                m_projectID = value;
            }
        }
        #endregion

        #region Public Functions
        public static OutputLine Parse( string strLogLine )
        {
            string strUppercaseLogLine = strLogLine.ToUpper();

            OutputType outputType = OutputType.Normal;
            string filename = "";
            int lineNumber = -1;
            bool processTimedOut = false;
            int projID = -1;

            int indexOf = strLogLine.IndexOf( ">" );
            if ( (indexOf != -1) && (indexOf < 4) )
            {
                if (int.TryParse(strLogLine.Substring(0, indexOf), out projID) == true)
                    projID--;
                else //unsuccessful parsing
                    return null;

            }

            if ( !strUppercaseLogLine.Contains( " ERROR(S) " ) && !strUppercaseLogLine.Contains( "WARNING(S)" ) )
            {
                if ( strUppercaseLogLine.Contains( "WARNINGS BEING TREATED AS ERRORS" ) 
                    || strUppercaseLogLine.Contains( "WARNING TREATED AS ERROR" ) 
                    || strUppercaseLogLine.Contains( "WARNING AS ERROR" ) )
                {
                    outputType = OutputType.WarningsTreatedAsErrors;
                }
                else if ( (strUppercaseLogLine.IndexOf( " WARNING " ) != -1) || (strUppercaseLogLine.IndexOf( "): WARNING:" ) != -1)
                    || strUppercaseLogLine.StartsWith( "WARNING " ) || strUppercaseLogLine.StartsWith( "WARNING: " ) )
                {
                    if ( !strUppercaseLogLine.Contains( "XBECOPY_SUPPRESS_COPY" ) )
                    {
                        outputType = OutputType.Warning;
                    }
                }
                else if ( (strUppercaseLogLine.IndexOf( " ERROR " ) != -1) || (strUppercaseLogLine.IndexOf( "): ERROR:" ) != -1)
                    || strUppercaseLogLine.StartsWith( "ERROR " ) || strUppercaseLogLine.StartsWith( "ERROR: " )
                    || strUppercaseLogLine.StartsWith( "FAILED " )
                    || (strUppercaseLogLine.IndexOf( "FATAL ERROR: " ) != -1) )
                {
                    outputType = OutputType.Error;
                }
                else if ( strUppercaseLogLine.IndexOf( "PROCESS TIMED OUT." ) != -1 )
                {
                    outputType = OutputType.Error;
                    processTimedOut = true;
                }
                else if ( strUppercaseLogLine.IndexOf( "PROJECT CONFIGURATION NOT FOUND: " ) != -1 )
                {
                    outputType = OutputType.Error;
                }
                else if ( strUppercaseLogLine.Contains( "FAILED TO INITIATE BUILD" ) )
                {
                    outputType = OutputType.Error;
                }
                else if ( strUppercaseLogLine.Contains( "ERROR WAITING FOR DBSBUILD" ) )
                {
                    outputType = OutputType.Error;
                }

                Match match = sm_fileLineRegex.Match( strLogLine );
                if ( match.Success )
                {
                    filename = match.Result( "${File}" );

                    string lineNumStr = match.Result( "${lineNum}" );
                    try
                    {
                        lineNumber = Convert.ToInt32( lineNumStr ) - 1;
                    }
                    catch
                    {
                    	
                    }
                }
            }

            return new OutputLine( outputType, filename, lineNumber, strLogLine, processTimedOut, projID );
        }
        #endregion
    }

    class SolutionOutput
    {
        public SolutionOutput()
        {

        }

        public SolutionOutput( string name, string config )
        {
            m_name = name;
            m_config = config;
        }

        #region Variables
        private string m_name = null;
        private string m_config = null;
        private int m_numSucceeded = -1;
        private int m_numFailed = -1;
        private int m_numUpToDate = -1;
        private int m_numSkipped = -1;
        List<OutputLine> m_outputLines = new List<OutputLine>();
        #endregion

        #region Properties
        public string Name
        {
            get
            {
                return m_name;
            }
            set
            {
                m_name = value;
            }
        }

        public string Config
        {
            get
            {
                return m_config;
            }
            set
            {
                m_config = value;
            }        
        }

        public int SucceedCount
        {
            get
            {
                return m_numSucceeded;
            }
            set
            {
                m_numSucceeded = value;
            }
        }

        public int FailCount
        {
            get
            {
                return m_numFailed;
            }
            set
            {
                m_numFailed = value;
            }
        }

        public int UpToDateCount
        {
            get
            {
                return m_numUpToDate;
            }
            set
            {
                m_numUpToDate = value;
            }
        }

        public int SkipCount
        {
            get
            {
                return m_numSkipped;
            }
            set
            {
                m_numSkipped = value;
            }
        }

        public List<OutputLine> OutputLines
        {
            get
            {
                return m_outputLines;
            }
            set
            {
                m_outputLines = value;
            }
        }

        #endregion

        #region Public Functions
        public static SolutionOutput Parse( string name, string config, List<string> strLogLines )
        {
            SolutionOutput slnOutput = new SolutionOutput( name, config );

            foreach ( string strLogLine in strLogLines )
            {
                OutputLine outLine = OutputLine.Parse( strLogLine );
                if ( outLine != null )
                {
                    string strUppercaseText = outLine.Text.ToUpper();

                    if ( (strUppercaseText.Contains( "SUCCEEDED" ) && strUppercaseText.Contains( "FAILED" )
                        && strUppercaseText.Contains( "SKIPPED" )) 
                        || strUppercaseText.Contains( "---------------------- DONE ----------------------" ) )
                    {
                        string[] parts = strUppercaseText.Split( new char[] { ' ' } );
                        for ( int i = 1; i < parts.Length; ++i )
                        {                            
                            string part = parts[i];

                            if ( part.StartsWith( "SUCCEEDED" ) )
                            {
                                if (int.TryParse(parts[i - 1], out slnOutput.m_numSucceeded) == false)
                                    slnOutput.SucceedCount = -1;
                            }
                            else if ( part.StartsWith( "FAILED" ) )
                            {
                                if (int.TryParse(parts[i - 1], out slnOutput.m_numFailed) == false)
                                    slnOutput.FailCount = -1;
                            }
                            else if ( part.StartsWith( "UP-TO-DATE" ) )
                            {
                                if ( parts[i - 1] == "OR" )
                                {
                                    slnOutput.UpToDateCount = slnOutput.SucceedCount;
                                }
                                else
                                {
                                    if (int.TryParse(parts[i - 1], out slnOutput.m_numUpToDate) == false)
                                        slnOutput.UpToDateCount = -1;
                                }
                            }
                            else if ( part.StartsWith( "SKIPPED" ) )
                            {
                                if (int.TryParse(parts[i - 1], out slnOutput.m_numSkipped) == false)
                                    slnOutput.SkipCount = -1;
                            }
                            else if ( part.StartsWith( "DONE" ) )
                            {
                                slnOutput.SucceedCount = 0;
                                slnOutput.FailCount = 0;
                                slnOutput.UpToDateCount = 0;
                                slnOutput.SkipCount = 0;
                            }
                        }
                    }

                    slnOutput.OutputLines.Add( outLine );
                }
            }

            return slnOutput;
        }
        #endregion
    }
}
