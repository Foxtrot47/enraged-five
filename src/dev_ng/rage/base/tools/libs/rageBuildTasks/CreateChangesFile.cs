using System;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using System.Collections.Generic;
using System.IO;
using System.Text;

using RSG.Base;
using RSG.Base.Command;
using RSG.Base.IO;

namespace RageBuildTasks
{
    /// <summary>
    /// Creates an html file that points (or tries to point) to the CI Build log file.
    /// </summary>
    public class CreateChangesFile : RageTaskBase
    {
        #region Variables
        private string m_filename;
        private string m_webUrlPrefix;
        private string m_webUrlPostfix;
        #endregion

        #region Properties
        /// <summary>
        /// The name of the changes file to save.
        /// </summary>
        [Required]
        public string Filename
        {
            get
            {
                return m_filename;
            }
            set
            {
                m_filename = value;
            }
        }

        /// <summary>
        /// An optional parameter to point the file to the CI Build web location of the log file.
        /// </summary>
        public string WebUrlPrefix
        {
            get
            {
                return m_webUrlPrefix;
            }
            set
            {
                m_webUrlPrefix = value;
            }
        }

        /// <summary>
        /// An optional parameter to point the file to the CI Build web location of the log file.
        /// </summary>
        public string WebUrlPostfix
        {
            get
            {
                return m_webUrlPostfix;
            }
            set
            {
                m_webUrlPostfix = value;
            }
        }
        #endregion

        #region Overrides
        public override bool Execute()
        {
            if ( String.IsNullOrEmpty( this.Filename ) )
            {
                this.Log.LogError( "No filename was specified." );
                return false;
            }

            string strChangesLog;
            rageStatus obStatus;
            WriteChangesFile( out strChangesLog, out obStatus );
            if ( !obStatus.Success() )
            {
                return false;
            }

            // clean up
            if ( !ShouldKeepBuildLog() && !String.IsNullOrEmpty( strChangesLog )
                && File.Exists( strChangesLog ) )
            {
                File.Delete( strChangesLog );
            }
            
            return true;
        }
        #endregion

        #region Private Functions
        private void WriteChangesFile( out string strChangesLog, out rageStatus obStatus )
        {
            if ( !String.IsNullOrEmpty( this.LogDirectory ) )
            {
                strChangesLog = Path.Combine( this.LogDirectory,
                    BuildLogFileName(GetMsBuildEnvironmentVariable("CCNetProject"), Path.GetFileNameWithoutExtension(this.Filename), null, null, null));
            }
            else
            {
                strChangesLog = Path.Combine( Environment.GetEnvironmentVariable( "TEMP" ),
                    BuildLogFileName(GetMsBuildEnvironmentVariable("CCNetProject"), Path.GetFileNameWithoutExtension(this.Filename), null, null, null));
            }

            // Write a log file that'll be useful later
            StreamWriter swGrabLogWriter = File.CreateText( strChangesLog );

            // Write header
            swGrabLogWriter.WriteLine( "<html><head><title>Information About This Build Of Tools</title></head><body bgcolor=#FFFFEE>" );
            swGrabLogWriter.WriteLine( "<h1>Information About This Build Of Tools</h1>" );
            swGrabLogWriter.WriteLine( "<table border=0 cellspacing=1 cellpadding=1>" );
            swGrabLogWriter.WriteLine(
                String.Format( "<tr valign=TOP><td align=left bgcolor=#EEEEEE>Time Of Code Grab</td><td align=left bgcolor=#EEFFEE>{0} on {1}</td></tr>",
                GetMsBuildEnvironmentVariable( "CCNetBuildTime" ), GetMsBuildEnvironmentVariable( "CCNetBuildDate" ) ) );
            swGrabLogWriter.WriteLine(
                String.Format( "<tr valign=TOP><td align=left bgcolor=#EEEEEE>Computer Name</td><td align=left bgcolor=#EEFFEE>{0}</td></tr>",
                Environment.GetEnvironmentVariable( "COMPUTERNAME" ) ) );
            swGrabLogWriter.WriteLine(
                String.Format( "<tr valign=TOP><td align=left bgcolor=#EEEEEE>User Name</td><td align=left bgcolor=#EEFFEE>{0}</td></tr>",
                Environment.GetEnvironmentVariable( "USERNAME" ) ) );
            swGrabLogWriter.WriteLine( "</table>" );
            swGrabLogWriter.WriteLine( "<p>" );
            swGrabLogWriter.WriteLine( "The following changes made it in...." );
            swGrabLogWriter.WriteLine( "<p>" );

            swGrabLogWriter.Write( "See the CI Build Log for {0} ", GetMsBuildEnvironmentVariable( "CCNetLabel" ) );
            string[] astrCIBuildLogFilenames = BuildCIBuildLogFilenames();
            for ( int i = 0; i < astrCIBuildLogFilenames.Length; ++i )
            {
                if ( i > 0 )
                {
                    swGrabLogWriter.Write( " or " );
                }

                swGrabLogWriter.Write( "<a href=\"{0}\">here</a>", astrCIBuildLogFilenames[i] );
            }
            swGrabLogWriter.WriteLine( '.' );

            swGrabLogWriter.Close();

            // make a copy in the tools folder so that it gets added to the zips later
            rageFileUtilities.DeleteLocalFile( this.Filename, out obStatus );
            rageFileUtilities.CopyFile( strChangesLog, this.Filename, out obStatus );
        }

        private string[] BuildCIBuildLogFilenames()
        {
            List<string> astrCIBuildLogFilenames = new List<string>();

            string strCIBuildDate = GetMsBuildEnvironmentVariable( "CCNetBuildDate" );
            string strCIBuildTime = GetMsBuildEnvironmentVariable( "CCNetBuildTime" );
            string strCIBuildLogName = String.Format( "log{0}{1}",
                strCIBuildDate.Replace( "-", string.Empty ), strCIBuildTime.Replace( ":", string.Empty ) );

            if ( String.IsNullOrEmpty( this.WebUrlPrefix ) )
            {
                string strCIBuildLogPath = GetMsBuildEnvironmentVariable( "CCNetArtifactDirectory" );
                strCIBuildLogPath = Path.Combine( strCIBuildLogPath, "buildlogs" );

                astrCIBuildLogFilenames.Add( Path.Combine( strCIBuildLogPath,
                    String.Format( "{0}.xml", strCIBuildLogName ) ) );
                astrCIBuildLogFilenames.Add( Path.Combine( strCIBuildLogPath, 
                    String.Format( "{0}Lbuild.{1}.xml", strCIBuildLogName, GetMsBuildEnvironmentVariable( "CCNetLabel" ) ) ) );
            }
            else
            {
                string webUrlPrefix = this.WebUrlPrefix.TrimEnd( new char[] { '/' } );
                string webUrlPostfix = !String.IsNullOrEmpty( this.WebUrlPostfix ) 
                    ? this.WebUrlPostfix.TrimStart( new char[] { '/' } ) : string.Empty;

                astrCIBuildLogFilenames.Add( String.Format( "{0}/{1}/{2}", webUrlPrefix,
                    String.Format( "{0}.xml", strCIBuildLogName ), webUrlPostfix ) );
                astrCIBuildLogFilenames.Add( String.Format( "{0}/{1}/{2}", webUrlPrefix,
                    String.Format( "{0}Lbuild.{1}.xml", strCIBuildLogName, GetMsBuildEnvironmentVariable( "CCNetLabel" ) ),
                    webUrlPostfix ) );
            }

            return astrCIBuildLogFilenames.ToArray();
        }
        #endregion
    }
}
