using System;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using System.Collections.Generic;
using System.IO;
using System.Text;

using RSG.Base.Command;

namespace RageBuildTasks
{
	public class IncrediBuildDoIt : Task
	{
		#region Variables
		private rageIncrediBuild m_obIncrediBuild = null;
		private int m_iExitCode;
		private bool m_bIgnoreExitCode = false;
        public bool m_ShowInterface = true;
		#endregion

		#region Properties

        /// <summary>
        /// Shows the interface of the Incredibuild task.
        /// </summary>
        public bool ShowInterface
        {
            get
            {
                return m_ShowInterface;
            }
            set
            {
                m_ShowInterface = value;
            }
        }

		/// <summary>
		/// Required String parameter.  The GUID of the Task to Do.
		/// </summary>
		[Required]
		public string GroupID
		{
			get
			{
				return m_obIncrediBuild.GroupUID;
			}
			set
			{
				m_obIncrediBuild = new rageIncrediBuild(value);
			}
		}

		/// <summary>
		/// Optional Int32 output read-only parameter.  Specifies the exit code provided by the executed command.
		/// </summary>
		[Output]
		public Int32 ExitCode
		{
			get
			{
				return m_iExitCode;
			}
		}

		/// <summary>
		/// Optional Boolean parameter.  If true, the task ignores the exit code provided by the executed command. Otherwise, the task returns false if the executed command returns a non-zero exit code.
		/// </summary>
		public bool IgnoreExitCode
		{
			get
			{
				return m_bIgnoreExitCode;
			}
			set
			{
				m_bIgnoreExitCode = value;
			}
		}

		#endregion

		#region Overrides
		public override bool Execute()
		{
            // Do it
			rageStatus obStatus;
            m_iExitCode = m_obIncrediBuild.DoIt(m_ShowInterface, out obStatus);

            List<string> logAsArray = null;

            string outputFilename = Path.GetFullPath( Environment.GetEnvironmentVariable( "TEMP" ) + "/rageIncrediBuild_" + this.GroupID + ".out.txt" );
            if ( File.Exists( outputFilename ) )
            {
                TextReader reader = null;
                try
                {
                    reader = new StreamReader( outputFilename );
                    logAsArray = new List<string>();

                    string line = reader.ReadLine();
                    while ( line != null )
                    {
                        logAsArray.Add( line );

                        line = reader.ReadLine();
                    }
                }
                catch (System.Exception ex)
                {
                    this.Log.LogError( "Unable to parse log file '{0}':  {1}", outputFilename, ex.Message );

                    logAsArray = null;
                }
                finally
                {
                    if ( reader != null )
                    {
                        reader.Close();
                    }
                }
            }

            if ( logAsArray == null )
            {
                logAsArray = m_obIncrediBuild.LogAsArray;
            }

            if ( logAsArray != null )
            {
                IBSolutionOutput ibSlnOutput = IBSolutionOutput.Parse( this.GroupID, outputFilename, logAsArray );
                if ( ibSlnOutput != null )
                {
                    foreach ( IBOutputLine outLine in ibSlnOutput.OutputLines )
                    {
                        switch ( outLine.Type )
                        {
                            case IBOutputLine.OutputType.Error:
                            case IBOutputLine.OutputType.WarningTreatedAsError:
                            case IBOutputLine.OutputType.WarningsTreatedAsErrors:
                                {
                                    if ( outLine.Owner != null )
                                    {
                                        this.Log.LogError( String.Format( "{0}: {1}", outLine.Owner.Name, outLine.Text ) );
                                    }
                                    else
                                    {
                                        this.Log.LogError( outLine.Text );
                                    }
                                }
                                break;
                            case IBOutputLine.OutputType.Normal:
                                {
                                    this.Log.LogMessage( MessageImportance.Normal, outLine.Text );
                                }
                                break;
                            case IBOutputLine.OutputType.Warning:
                                {
                                    if ( outLine.Owner != null )
                                    {
                                        this.Log.LogWarning( String.Format( "{0}: {1}", outLine.Owner.Name, outLine.Text ) );
                                    }
                                    else
                                    {
                                        this.Log.LogWarning( outLine.Text );
                                    }
                                }
                                break;
                        }
                    }
                }
            }

			if ((!IgnoreExitCode) && (!obStatus.Success()))
			{
				this.Log.LogError(obStatus.ErrorString);
				return false;
			}
			return true;
		}
		#endregion
	}

    class IBOutputLine
    {
        public IBOutputLine()
        {

        }

        public IBOutputLine( OutputType outputType, string text, bool isIncredibuildError, bool processTimedOut )
        {
            m_outputType = outputType;
            m_text = text;
            m_isIncrediBuildError = isIncredibuildError;
            m_processTimedOut = processTimedOut;
        }

        public enum OutputType
        {
            Normal,
            Warning,
            Error,
            WarningTreatedAsError,
            WarningsTreatedAsErrors
        }

        #region Variables
        private IBProjectOutput m_owner = null;
        private OutputType m_outputType = OutputType.Normal;
        private string m_text = null;
        private bool m_isIncrediBuildError = false;
        private bool m_processTimedOut = false;
        #endregion

        #region Properties
        public IBProjectOutput Owner
        {
            get
            {
                return m_owner;
            }
            set
            {
                m_owner = value;
            }
        }

        public OutputType Type
        {
            get
            {
                return m_outputType;
            }
            set
            {
                m_outputType = value;
            }
        }

        public string Text
        {
            get
            {
                return m_text;
            }
            set
            {
                m_text = value;
            }
        }

        public bool IsIncredibuildError
        {
            get
            {
                return m_isIncrediBuildError;
            }
            set
            {
                m_isIncrediBuildError = value;
            }
        }

        public bool ProcessTimedOut
        {
            get
            {
                return m_processTimedOut;
            }
            set
            {
                m_processTimedOut = value;
            }
        }
        #endregion

        #region Public Functions
        public static IBOutputLine Parse( string strLogLine )
        {
            string strUppercaseLogLine = strLogLine.ToUpper();

            OutputType outputType = OutputType.Normal;
            bool isIncrediBuildError = false;
            bool processTimedOut = false;

            if ( !strUppercaseLogLine.Contains( " ERROR(S) " ) && !strUppercaseLogLine.Contains( "WARNING(S)" ) )
            {
                if ( strUppercaseLogLine.Contains( "WARNINGS BEING TREATED AS ERRORS" ) )
                {
                    outputType = OutputType.WarningsTreatedAsErrors;
                }
                else if ( strUppercaseLogLine.Contains( "WARNING TREATED AS ERROR" ) )
                {
                    outputType = OutputType.WarningTreatedAsError;
                }
                else if ( (strUppercaseLogLine.IndexOf( " WARNING " ) != -1) || (strUppercaseLogLine.IndexOf( "): WARNING:" ) != -1)
                    || strUppercaseLogLine.StartsWith( "WARNING " ) || strUppercaseLogLine.StartsWith( "WARNING: " ) )
                {
                    outputType = OutputType.Warning;
                }
                else if ( (strUppercaseLogLine.IndexOf( " ERROR " ) != -1) || (strUppercaseLogLine.IndexOf( "): ERROR:" ) != -1)
                    || strUppercaseLogLine.StartsWith( "ERROR " ) || strUppercaseLogLine.StartsWith( "ERROR: " )
                    || (strUppercaseLogLine.IndexOf( " FAILED " ) != -1) || strUppercaseLogLine.StartsWith( "FAILED " )
                    || (strUppercaseLogLine.IndexOf( "FATAL ERROR: " ) != -1) )
                {
                    outputType = OutputType.Error;
                }
                else if ( (strUppercaseLogLine.IndexOf("FAILED TO CONNECT TO PRIMARY COORDINATOR") != -1 ) )
                {
                    outputType = OutputType.Warning;
                }
                else if ( strUppercaseLogLine.IndexOf( "PROCESS TIMED OUT." ) != -1 )
                {
                    outputType = OutputType.Error;
                    processTimedOut = true;
                }
                else if ( strUppercaseLogLine.Contains( "FAILED TO INITIATE BUILD" ) )
                {
                    outputType = OutputType.Error;
                    isIncrediBuildError = true;
                }
            }

            return new IBOutputLine( outputType, strLogLine, isIncrediBuildError, processTimedOut );
        }
        #endregion
    }

    class IBProjectOutput
    {
        public IBProjectOutput()
        {

        }

        public IBProjectOutput( string name )
        {
            m_name = name;
        }

        #region Variables
        private string m_name = null;
        private int m_numWarnings = -1;
        private int m_numErrors = -1;
        private List<IBOutputLine> m_outputLines = new List<IBOutputLine>();

        private bool m_treatWarningsAsErrors = false;
        private bool m_treatWarningAsError = false;

        private const string c_prefix = "--------------------PROJECT: ";
        #endregion

        #region Properties
        public string Name
        {
            get
            {
                return m_name;
            }
            set
            {
                m_name = value;
            }
        }

        public List<IBOutputLine> OutputLines
        {
            get
            {
                return m_outputLines;
            }
            set
            {
                m_outputLines = value;
            }
        }

        public List<IBOutputLine> WarningOutputLines
        {
            get
            {
                List<IBOutputLine> warningOutputLines = new List<IBOutputLine>();
                foreach ( IBOutputLine outLine in m_outputLines )
                {
                    if ( outLine.Type == IBOutputLine.OutputType.Warning )
                    {
                        warningOutputLines.Add( outLine );
                    }
                }

                return warningOutputLines;
            }
        }

        public List<IBOutputLine> ErrorOutputLines
        {
            get
            {
                List<IBOutputLine> errorOutputLines = new List<IBOutputLine>();
                foreach ( IBOutputLine outLine in m_outputLines )
                {
                    if ( (outLine.Type != IBOutputLine.OutputType.Normal) && (outLine.Type != IBOutputLine.OutputType.Warning) )
                    {
                        errorOutputLines.Add( outLine );
                    }
                }

                return errorOutputLines;
            }
        }

        public int WarningsCount
        {
            get
            {
                return m_numWarnings;
            }
            set
            {
                m_numWarnings = value;
            }
        }

        public int ErrorsCount
        {
            get
            {
                return m_numErrors;
            }
            set
            {
                m_numErrors = value;
            }
        }

        public bool HasIncredibuildErrors
        {
            get
            {
                foreach ( IBOutputLine line in m_outputLines )
                {
                    if ( line.IsIncredibuildError )
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        public bool HasProcessTimedOutErrors
        {
            get
            {
                foreach ( IBOutputLine line in m_outputLines )
                {
                    if ( line.ProcessTimedOut )
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        public bool TreatWarningsAsErrors
        {
            get
            {
                return m_treatWarningsAsErrors;
            }
            set
            {
                m_treatWarningsAsErrors = value;
            }
        }

        public bool TreatWarningAsError
        {
            get
            {
                return m_treatWarningAsError;
            }
            set
            {
                m_treatWarningAsError = value;
            }
        }
        #endregion

        #region Public Functions
        public static IBProjectOutput Parse( string strLogLine )
        {
            string strUppercaseLogLine = strLogLine.ToUpper();
            if ( strUppercaseLogLine.StartsWith( c_prefix ) )
            {
                return new IBProjectOutput( strLogLine );
            }

            return null;
        }
        #endregion
    }

    class IBSolutionOutput
    {
        public IBSolutionOutput()
        {

        }

        public IBSolutionOutput( string name, string logFilename )
        {
            m_name = name;
            m_logFilename = logFilename;
        }

        #region Variables
        private string m_name = null;
        private string m_logFilename = null;
        private int m_numSucceeded = -1;
        private int m_numFailed = -1;
        private int m_numUpToDate = -1;
        private int m_numSkipped = -1;
        List<IBOutputLine> m_outputLines = new List<IBOutputLine>();
        List<IBProjectOutput> m_projectOutputs = new List<IBProjectOutput>();
        #endregion

        #region Properties
        public string Name
        {
            get
            {
                return m_name;
            }
            set
            {
                m_name = value;
            }
        }

        public string LogFilename
        {
            get
            {
                return m_logFilename;
            }
            set
            {
                m_logFilename = value;
            }
        }

        public int SucceedCount
        {
            get
            {
                return m_numSucceeded;
            }
            set
            {
                m_numSucceeded = value;
            }
        }

        public int FailCount
        {
            get
            {
                return m_numFailed;
            }
            set
            {
                m_numFailed = value;
            }
        }

        public int UpToDateCount
        {
            get
            {
                return m_numUpToDate;
            }
            set
            {
                m_numUpToDate = value;
            }
        }

        public int SkipCount
        {
            get
            {
                return m_numSkipped;
            }
            set
            {
                m_numSkipped = value;
            }
        }

        public bool HasIncredibuildErrors
        {
            get
            {
                foreach ( IBProjectOutput proj in m_projectOutputs )
                {
                    if ( proj.HasIncredibuildErrors )
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        public bool HasProcessTimedOutErrors
        {
            get
            {
                foreach ( IBProjectOutput proj in m_projectOutputs )
                {
                    if ( proj.HasProcessTimedOutErrors )
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        public List<IBOutputLine> OutputLines
        {
            get
            {
                return m_outputLines;
            }
            set
            {
                m_outputLines = value;
            }
        }

        public List<IBProjectOutput> ProjectOutputs
        {
            get
            {
                return m_projectOutputs;
            }
            set
            {
                m_projectOutputs = value;
            }
        }
        #endregion

        #region Public Functions
        public static IBSolutionOutput Parse( string name, string logFilename, List<string> strLogLines )
        {
            IBSolutionOutput slnOutput = new IBSolutionOutput( name, logFilename );

            IBProjectOutput currentProjOutput = null;
            foreach ( string strLogLine in strLogLines )
            {
                IBProjectOutput projOutput = IBProjectOutput.Parse( strLogLine );
                if ( projOutput != null )
                {
                    slnOutput.ProjectOutputs.Add( projOutput );
                    currentProjOutput = projOutput;

                    IBOutputLine output = new IBOutputLine( IBOutputLine.OutputType.Normal, strLogLine, false, false );

                    currentProjOutput.OutputLines.Add( output );
                    slnOutput.OutputLines.Add( output );

                    continue;
                }

                IBOutputLine outLine = IBOutputLine.Parse( strLogLine );
                if ( outLine != null )
                {
                    string strUppercaseText = outLine.Text.ToUpper();
                    if ( currentProjOutput != null )
                    {
                        switch ( outLine.Type )
                        {
                            case IBOutputLine.OutputType.Warning:
                                {
                                    if ( currentProjOutput.TreatWarningsAsErrors )
                                    {
                                        outLine.Type = IBOutputLine.OutputType.Error;
                                    }
                                    else if ( currentProjOutput.TreatWarningAsError )
                                    {
                                        currentProjOutput.TreatWarningAsError = false;
                                    }
                                }
                                break;
                            case IBOutputLine.OutputType.WarningsTreatedAsErrors:
                                {
                                    currentProjOutput.TreatWarningsAsErrors = true;
                                }
                                break;
                            case IBOutputLine.OutputType.WarningTreatedAsError:
                                {
                                    currentProjOutput.TreatWarningAsError = true;
                                }
                                break;
                            default:
                                break;
                        }

                        outLine.Owner = currentProjOutput;
                        currentProjOutput.OutputLines.Add( outLine );
                    }

                    if ( strUppercaseText.Contains( "SUCCEEDED" ) && strUppercaseText.Contains( "FAILED" )
                        && strUppercaseText.Contains( "SKIPPED" ) )
                    {
                        currentProjOutput = null;

                        string[] parts = strUppercaseText.Split( new char[] { ' ' } );
                        for ( int i = 1; i < parts.Length; ++i )
                        {
                            string part = parts[i];

                            if ( part.StartsWith( "SUCCEEDED" ) )
                            {
                                try
                                {
                                    slnOutput.SucceedCount = int.Parse( parts[i - 1] );
                                }
                                catch
                                {

                                }
                            }
                            else if ( part.StartsWith( "FAILED" ) )
                            {
                                try
                                {
                                    slnOutput.FailCount = int.Parse( parts[i - 1] );
                                }
                                catch
                                {

                                }
                            }
                            else if ( part.StartsWith( "UP-TO-DATE" ) )
                            {
                                if ( parts[i - 1] == "OR" )
                                {
                                    slnOutput.UpToDateCount = slnOutput.SucceedCount;
                                }
                                else
                                {
                                    try
                                    {
                                        slnOutput.UpToDateCount = int.Parse( parts[i - 1] );
                                    }
                                    catch
                                    {

                                    }
                                }
                            }
                            else if ( part.StartsWith( "SKIPPED" ) )
                            {
                                try
                                {
                                    slnOutput.SkipCount = int.Parse( parts[i - 1] );
                                }
                                catch
                                {

                                }
                            }
                        }
                    }

                    slnOutput.OutputLines.Add( outLine );
                }
            }

            return slnOutput;
        }
        #endregion
    }
}
