using System;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using System.Collections.Generic;
using System.IO;
using System.Text;

using RSG.Base.Command;
using RSG.Base.SCM;

namespace RageBuildTasks
{
    public class AddToPerforce : RagePerforceTaskBase
    {
        #region Enums
        enum AddFileResult
        {
            Added,
            Failed,
            Skipped
        }
        #endregion

        #region Variables
        private ITaskItem[] m_filenames = null;
        private string m_localDirectory = null;
        private bool m_recurseDirectory = true;
        private ITaskItem[] m_filePatterns = null;
        #endregion

        #region Properties
        /// <summary>
        /// A list of files to add
        /// </summary>
        public ITaskItem[] Filenames
        {
            get
            {
                return m_filenames;
            }
        }

        /// <summary>
        /// A directory to add
        /// </summary>
        public string LocalDirectory
        {
            get
            {
                return m_localDirectory;
            }
            set
            {
                m_localDirectory = value;
            }
        }
        
        /// <summary>
        /// Whether or not to recurse on that directory.  Default is true.
        /// </summary>
        public bool Recurse
        {
            get
            {
                return m_recurseDirectory;
            }
            set
            {
                m_recurseDirectory = value;
            }
        }

        /// <summary>
        /// File patterns to match when searching the directory.  Default is "*.*"
        /// </summary>
        public ITaskItem[] FilePatterns
        {
            get
            {
                return m_filePatterns;
            }
            set
            {
                m_filePatterns = value;
            }
        }
        #endregion

        #region Overrides
        public override bool Execute()
        {
            List<string> filenameList = new List<string>();

            if ( this.Filenames != null )
            {
                foreach ( ITaskItem item in this.Filenames )
                {
                    filenameList.Add( item.ToString() );
                }
            }
            
            if ( !String.IsNullOrEmpty( this.LocalDirectory ) )
            {
                if ( this.FilePatterns != null )
                {
                    foreach ( ITaskItem item in this.FilePatterns )
                    {
                        try
                        {
                            string[] files = Directory.GetFiles( this.LocalDirectory, item.ToString(),
                                this.Recurse ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly );
                            if ( (files != null) && (files.Length > 0) )
                            {
                                filenameList.AddRange( files );
                            }
                        }
                        catch (System.Exception e)
                        {
                            this.Log.LogWarning( "Directory.GetFiles exception: {0}: Pattern '{1}'.", e.Message, item.ToString() );
                        }
                    }
                }
                else
                {
                    try
                    {
                        string[] files = Directory.GetFiles( this.LocalDirectory, "*.*",
                            this.Recurse ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly );
                        if ( (files != null) && (files.Length > 0) )
                        {
                            filenameList.AddRange( files );
                        }
                    }
                    catch ( System.Exception e )
                    {
                        this.Log.LogWarning( "Directory.GetFiles exception: {0}: Pattern '*.*'.", e.Message );
                    }
                }
            }

            int iAdded = 0;
            int iFailed = 0;
            int iSkipped = 0;

            filenameList.Sort();
            bool bSuccess = AddFiles( filenameList, ref iAdded, ref iFailed, ref iSkipped );

            this.Log.LogMessage( MessageImportance.High, "{0} Added    {1} Failed    {2} Skipped.", iAdded, iFailed, iSkipped );
            return bSuccess;
        }
        #endregion

        #region Private Functions
        private bool AddFiles( List<string> filenames, ref int iAdded, ref int iFailed, ref int iSkipped )
        {
            foreach ( string filename in filenames )
            {
                switch ( AddFile( filename ) )
                {
                    case AddFileResult.Added:
                        ++iAdded;
                        break;
                    case AddFileResult.Failed:
                        ++iFailed;
                        break;
                    case AddFileResult.Skipped:
                        ++iSkipped;
                        break;
                }
            }

            return iFailed == 0;
        }


        private AddFileResult AddFile( string filename )
        {
            rageStatus obStatus;

            try
            {
                if ( ragePerforce.FileExists( filename, out obStatus ) )
                {
                    this.Log.LogMessage( MessageImportance.Normal, "Skipping '{0}' because it already exists in Perforce.", filename );
                    return AddFileResult.Skipped;
                }

                if ( !obStatus.Success() )
                {
                    this.Log.LogWarning( "FileExists problem:  {0}: '{1}'.", obStatus.ErrorString, filename );
                }
            }
            catch ( System.Exception e )
            {
                this.Log.LogWarning( "FileExists exception: {0}: '{1}'.", e.Message, filename );
            }

            try
            {
                ragePerforce.AddFile( filename, false, out obStatus );
                if ( obStatus.Success() )
                {
                    this.Log.LogMessage( MessageImportance.Normal, "Added '{0}'.", filename );
                    return AddFileResult.Added;
                }
                else
                {
                    this.Log.LogError( "Unable to add '{0}': '{1}'.", filename, obStatus.ErrorString );
                    return AddFileResult.Failed;
                }
            }
            catch ( System.Exception e )
            {
                this.Log.LogError( "AddFile exception: {0}: '{1}'.", e.Message, filename );
                return AddFileResult.Failed;
            }
        }
        #endregion
    }
}
