﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Interop.AutodeskMotionBuilder
{

    /// <summary>
    /// Autodesk MotionBuilder Exception class.
    /// </summary>
    public class AutodeskMotionBuilderException : Exception
    {
        #region Constructor(s)
        public AutodeskMotionBuilderException()
            : base()
        {
        }

        public AutodeskMotionBuilderException(String message)
            : base(message)
        {
        }

        public AutodeskMotionBuilderException(String message, Exception inner)
            : base(message, inner)
        {
        }

        public AutodeskMotionBuilderException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context)
            : base(info, context)
        {
        }
        #endregion // Constructor(s)
    }

} // RSG.Interop.AutodeskMotionBuilder namespace
