﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;
using RSG.Base.IO;

namespace RSG.Interop.AutodeskMotionBuilder
{

    /// <summary>
    /// Static class giving information about installed versions of Autodesk
    /// MotionBuilder.
    /// </summary>
    public sealed class AutodeskMotionBuilder
    {
        #region Constants
        private static readonly String MB_EXE = "motionbuilder.exe";

        /// <summary>
        /// Additional plugin INI header (typo *is* deliberate).
        /// </summary>
        private static readonly String ADDITIONAL_PLUGIN_SECTION = "AdditionnalPluginPath";
        private static readonly String ADDITIONAL_PLUGIN_ENTRY = "PlugInPath";
        private static readonly String ADDITIONAL_PLUGIN_COUNT = "Count";
        private static readonly String PYTHON_STARTUP_SECTION = "Python";
        private static readonly String PYTHON_STARTUP_ENTRY = "PythonStartup";
        private static readonly String ADDITIONAL_ROCKSTAR_SECTION = "Rockstar";
        private static readonly String ADDITIONAL_ROCKSTAR_ASSEMBLY_ENTRY = "AssemblyPath";
        #endregion // Constants

        #region Controller Methods
        /// <summary>
        /// Determine whether any version of MotionBuilder is installed.
        /// </summary>
        /// <returns></returns>
        public static bool IsInstalled()
        {
            bool result = true;
            IEnumerable<MotionBuilderVersion> versions =
                Enum.GetValues(typeof(MotionBuilderVersion)).Cast<MotionBuilderVersion>();
            foreach (MotionBuilderVersion version in versions)
            {
                if (MotionBuilderVersion.None == version)
                    continue; // Skip.
                result |= (IsVersionInstalled(version, true) || IsVersionInstalled(version, false));
            }

            return (result);
        }

        /// <summary>
        /// Determine whether a particular version of MotionBuilder is installed.
        /// </summary>
        /// <param name="version"></param>
        /// <param name="is64bit"></param>
        /// <returns></returns>
        public static bool IsVersionInstalled(MotionBuilderVersion version, bool is64bit)
        {
            RegistryKey rootKey;
            if (is64bit)
                rootKey = RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine, RegistryView.Registry64);
            else
                rootKey = RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine, RegistryView.Registry32);

            using (RegistryKey versionKey = rootKey.OpenSubKey(version.RegistryKey(is64bit)))
            {
                if (null == versionKey)
                    return (false);

                String installPath = (String)versionKey.GetValue("InstallPath");
                return (!String.IsNullOrWhiteSpace(installPath));           
            }
        }

        /// <summary>
        /// Get installation path for a particular version of MotionBuilder.
        /// </summary>
        /// <param name="version"></param>
        /// <param name="is64bit"></param>
        /// <returns></returns>
        public static String GetInstallPath(MotionBuilderVersion version, bool is64bit)
        {
            Debug.Assert(IsVersionInstalled(version, is64bit));
            if (!IsVersionInstalled(version, is64bit))
                throw (new AutodeskMotionBuilderException("MotionBuilder version not installed."));

            RegistryKey rootKey;
            if (is64bit)
                rootKey = RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine, RegistryView.Registry64);
            else
                rootKey = RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine, RegistryView.Registry32);

            using (RegistryKey versionKey = rootKey.OpenSubKey(version.RegistryKey(is64bit)))
            {
                String installPath = (String)versionKey.GetValue("InstallPath");
                if (!String.IsNullOrWhiteSpace(installPath))
                    return (installPath);
                else
                    throw (new AutodeskMotionBuilderException("Failed to read MotionBuilder install path."));
            }
        }

        /// <summary>
        /// Get executable path for a particular version of 3dsmax.
        /// </summary>
        /// <param name="version"></param>
        /// <param name="is64bit"></param>
        /// <returns></returns>
        public static String GetExecutablePath(MotionBuilderVersion version, bool is64bit)
        {
            Debug.Assert(IsVersionInstalled(version, is64bit));
            if (!IsVersionInstalled(version, is64bit))
                throw (new AutodeskMotionBuilderException("MotionBuilder version not installed."));

            String installdir = GetInstallPath(version, is64bit);
            if (is64bit)
                return (Path.Combine(installdir, "bin", "x64", MB_EXE));
            else
                return (Path.Combine(installdir, "bin", MB_EXE));
        }

        /// <summary>
        /// Return information on all running MotionBuilder instances.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<MotionBuilderInstance> GetRunningInstances()
        {
            return (MotionBuilderInstance.GetRunningInstances());
        }

        /// <summary>
        /// Return MotionBuilder configuration directory.
        /// </summary>
        /// <param name="version">MotionBuilder version.</param>
        /// <param name="is64bit"></param>
        /// <returns></returns>
        public static String GetConfigDirectory(MotionBuilderVersion version, bool is64bit)
        {
            String versionDir = version.UserConfigPath(is64bit);
            String configDirectory = Path.Combine(versionDir, "config");

            return (configDirectory);
        }

        /// <summary>
        /// Return MotionBuilder plugin configuration filename.
        /// </summary>
        /// <param name="version">MotionBuilder version.</param>
        /// <param name="is64bit"></param>
        /// <returns></returns>
        public static String GetPluginConfigFilename(MotionBuilderVersion version, bool is64bit)
        {
            String configDirectory = GetConfigDirectory(version, is64bit);
            return (Path.Combine(configDirectory, Environment.MachineName + ".Application.txt"));
        }

        /// <summary>
        /// Return MotionBuilder additional plugin paths.
        /// </summary>
        /// <param name="version">MotionBuilder version.</param>
        /// <param name="is64bit"></param>
        /// <returns></returns>
        public static IEnumerable<String> GetAdditionalPluginPaths(MotionBuilderVersion version, bool is64bit)
        {
            String configFilename = GetPluginConfigFilename(version, is64bit);
            List<String> pluginPaths = new List<String>();
            try
            {
                if (File.Exists(configFilename))
                {
                    IniFile iniFile = IniFile.Load(configFilename);

                    String countString = iniFile.GetValue(ADDITIONAL_PLUGIN_COUNT, ADDITIONAL_PLUGIN_SECTION);
                    int count = int.Parse(countString);

                    if (count == 0)
                        return pluginPaths;

                    IEnumerable<String> paths = iniFile.GetKeys(ADDITIONAL_PLUGIN_SECTION);
                    foreach (String key in paths)
                    {
                        if (!key.StartsWith(ADDITIONAL_PLUGIN_ENTRY))
                            continue;
                        pluginPaths.Add(iniFile.GetValue(key, ADDITIONAL_PLUGIN_SECTION));
                    }
                }
            }
            catch (Exception ex)
            {
                throw (new AutodeskMotionBuilderException("Failed to get plugin paths.", ex));
            }

            return (pluginPaths);
        }

        /// <summary>
        /// Set MotionBuilder python paths.
        /// </summary>
        /// <param name="version">MotionBuilder version.</param>
        /// <param name="is64bit"></param>
        /// <param name="pythonPath"></param>
        public static void SetPythonStartupPath(MotionBuilderVersion version, bool is64bit, String pythonPath)
        {
            String configFilename = GetPluginConfigFilename(version, is64bit);
            try
            {
                if (File.Exists(configFilename))
                {
                    IniFile iniFile = IniFile.Load(configFilename);

                    iniFile.SetValue(PYTHON_STARTUP_ENTRY, PYTHON_STARTUP_SECTION,
                            pythonPath);
                    iniFile.Save(configFilename, IniFileSaveOption.SpacesBetweenKeysAndValues);
                }
            }
            catch (Exception ex)
            {
                throw (new AutodeskMotionBuilderException("Failed to set plugin python paths.", ex));
            }
        }

        /// <summary>
        /// Get MotionBuilder python paths.
        /// </summary>
        /// <param name="version">MotionBuilder version.</param>
        /// <param name="is64bit"></param>
        /// <returns></returns>
        public static String GetPythonStartupPath(MotionBuilderVersion version, bool is64bit)
        {
            String configFilename = GetPluginConfigFilename(version, is64bit);
            try
            {
                if (File.Exists(configFilename))
                {
                    IniFile iniFile = IniFile.Load(configFilename);

                    return iniFile.GetValue(PYTHON_STARTUP_ENTRY, PYTHON_STARTUP_SECTION);
                }
            }
            catch (Exception ex)
            {
                throw (new AutodeskMotionBuilderException("Failed to get plugin python paths.", ex));
            }
            return "";
        }

        /// <summary>
        /// Set MotionBuilder additional plugin paths.
        /// </summary>
        /// <param name="version">MotionBuilder version.</param>
        /// <param name="is64bit"></param>
        /// <param name="paths"></param>
        public static void SetAdditionalPluginPaths(MotionBuilderVersion version, bool is64bit, IEnumerable<String> paths)
        {
            String configFilename = GetPluginConfigFilename(version, is64bit);
            try
            {
                if (File.Exists(configFilename))
                {
                    IniFile iniFile = IniFile.Load(configFilename);
                    int count = 0;

                    // Set new paths.
                    foreach (String path in paths)
                    {
                        String key = String.Format("{0}{1}", ADDITIONAL_PLUGIN_ENTRY, count++);
                        iniFile.SetValue(key, ADDITIONAL_PLUGIN_SECTION, path);
                    }
                    iniFile.SetValue(ADDITIONAL_PLUGIN_COUNT, ADDITIONAL_PLUGIN_SECTION, 
                        paths.Count().ToString());
                    iniFile.Save(configFilename, IniFileSaveOption.SpacesBetweenKeysAndValues);
                }
            }
            catch (Exception ex)
            {
                throw (new AutodeskMotionBuilderException("Failed to set plugin paths.", ex));
            }
        }

        /// <summary>
        /// Set MotionBuilder python paths.
        /// </summary>
        /// <param name="version">MotionBuilder version.</param>
        /// <param name="is64bit"></param>
        /// <param name="assemblyPath"></param>
        public static void SetAdditionalRockstarPluginPath(MotionBuilderVersion version, bool is64bit, String assemblyPath)
        {
            String configFilename = GetPluginConfigFilename(version, is64bit);
            try
            {
                if (File.Exists(configFilename))
                {
                    IniFile iniFile = IniFile.Load(configFilename);

                    iniFile.SetValue(ADDITIONAL_ROCKSTAR_ASSEMBLY_ENTRY, ADDITIONAL_ROCKSTAR_SECTION,
                            assemblyPath);
                    iniFile.Save(configFilename, IniFileSaveOption.SpacesBetweenKeysAndValues);
                }
            }
            catch (Exception ex)
            {
                throw (new AutodeskMotionBuilderException("Failed to set plugin rockstar paths.", ex));
            }
        }
        #endregion // Controller Methods
    }

} // RSG.Interop.AutodeskMotionBuilder namespace
