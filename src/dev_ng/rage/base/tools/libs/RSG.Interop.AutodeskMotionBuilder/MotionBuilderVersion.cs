﻿using System;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using RSG.Base.Attributes;

namespace RSG.Interop.AutodeskMotionBuilder
{

    /// <summary>
    /// Autodesk MotionBuilder version enumeration.
    /// </summary>
    [DataContract]
    public enum MotionBuilderVersion
    {
        [EnumMember]
        None = 0,

        [EnumMember]
        [FieldDisplayName("MotionBuilder 2010")]
        [RegistryKey(@"Software\Autodesk\MotionBuilder\2010", true)]
        [RegistryKey(@"Software\Autodesk\MotionBuilder\2010", false)]
        [UserConfigPath(Environment.SpecialFolder.LocalApplicationData, @"Autodesk\MB2010", true)]
        [UserConfigPath(Environment.SpecialFolder.LocalApplicationData, @"Autodesk\MB2010", false)]
        [ToolPath("motionbuilder2010")]
        AutodeskMotionBuilder2010 = 2010,

        [EnumMember]
        [FieldDisplayName("MotionBuilder 2012")]
        [RegistryKey(@"Software\Autodesk\MotionBuilder\2012", true)]
        [RegistryKey(@"Software\Autodesk\MotionBuilder\2012", false)]
        [UserConfigPath(Environment.SpecialFolder.MyDocuments, @"MB\2012-x64", true)]
        [UserConfigPath(Environment.SpecialFolder.MyDocuments, @"MB\2012", false)]
        [ToolPath("motionbuilder2012")]
        AutodeskMotionBuilder2012 = 2012,

        [EnumMember]
        [FieldDisplayName("MotionBuilder 2013")]
        [RegistryKey(@"Software\Autodesk\MotionBuilder\2013", true)]
        [RegistryKey(@"Software\Autodesk\MotionBuilder\2013", false)]
        [UserConfigPath(Environment.SpecialFolder.MyDocuments, @"MB\2013-x64", true)]
        [UserConfigPath(Environment.SpecialFolder.MyDocuments, @"MB\2013", false)]
        [ToolPath("motionbuilder2013")]
        AutodeskMotionBuilder2013 = 2013,

        [EnumMember]
        [FieldDisplayName("MotionBuilder 2014")]
        [RegistryKey(@"Software\Autodesk\MotionBuilder\2014", true)]
        [RegistryKey(@"Software\Autodesk\MotionBuilder\2014", false)]
        [UserConfigPath(Environment.SpecialFolder.MyDocuments, @"MB\2014-x64", true)]
        [UserConfigPath(Environment.SpecialFolder.MyDocuments, @"MB\2014", false)]
        [ToolPath("motionbuilder2014")]
        AutodeskMotionBuilder2014 = 2014,
    }

    /// <summary>
    /// MotionBuilderVersion enumeration utilities and extension methods.
    /// </summary>
    public static class MotionBuilderVersionExtensions
    {
        /// <summary>
        /// Return MotionBuilder friendly-name.
        /// </summary>
        /// <param name="version"></param>
        /// <returns></returns>
        public static String GetDisplayName(this MotionBuilderVersion version)
        {
            Type type = version.GetType();

            FieldInfo fi = type.GetField(version.ToString());
            FieldDisplayNameAttribute[] attributes =
                fi.GetCustomAttributes(typeof(FieldDisplayNameAttribute), false) as FieldDisplayNameAttribute[];

            FieldDisplayNameAttribute attribute = attributes.FirstOrDefault();
            if (null != attribute)
                return (attribute.DisplayName);
            else
                throw (new ArgumentException("Motionbuilder version has no FieldDisplayName defined."));
        }

        /// <summary>
        /// Return MotionBuilder version Registry Key.
        /// </summary>
        /// <param name="version"></param>
        /// <param name="is64bit"></param>
        /// <returns></returns>
        public static String RegistryKey(this MotionBuilderVersion version, bool is64bit)
        {
            Type type = version.GetType();

            FieldInfo fi = type.GetField(version.ToString());
            RegistryKeyAttribute[] attributes =
                fi.GetCustomAttributes(typeof(RegistryKeyAttribute), false) as RegistryKeyAttribute[];

            RegistryKeyAttribute attribute = attributes.Where(a => a.Is64Bit == is64bit).FirstOrDefault();
            if (null != attribute)
                return (attribute.Key);
            else
                throw (new ArgumentException("MotionBuilder version has no Registry Key defined."));
        }

        /// <summary>
        /// Return MotionBuilder user-config path.
        /// </summary>
        /// <param name="version"></param>
        /// <param name="is64bit"></param>
        /// <returns></returns>
        public static String UserConfigPath(this MotionBuilderVersion version, bool is64bit)
        {
            Type type = version.GetType();

            FieldInfo fi = type.GetField(version.ToString());
            UserConfigPathAttribute[] attributes =
                fi.GetCustomAttributes(typeof(UserConfigPathAttribute), false) as UserConfigPathAttribute[];

            UserConfigPathAttribute attribute = attributes.Where(a => a.Is64Bit == is64bit).FirstOrDefault();
            if (null != attribute)
                return (attribute.GetFullPath());
            else
                throw (new ArgumentException("MotionBuilder version has no User Config Path defined."));
        }

        /// <summary>
        /// Return 3dsmax tool path.
        /// </summary>
        /// <param name="version"></param>
        /// <returns></returns>
        public static String ToolPath(this MotionBuilderVersion version)
        {
            Type type = version.GetType();

            FieldInfo fi = type.GetField(version.ToString());
            ToolPathAttribute[] attributes =
                fi.GetCustomAttributes(typeof(ToolPathAttribute), false) as ToolPathAttribute[];

            ToolPathAttribute attribute = attributes.FirstOrDefault();
            if (null != attribute)
                return (attribute.Path);
            else
                throw (new ArgumentException("MotionBuilder version has no Tool Path defined."));
        }
    }

} // RSG.Interop.AutodeskMotionBuilder namespace
