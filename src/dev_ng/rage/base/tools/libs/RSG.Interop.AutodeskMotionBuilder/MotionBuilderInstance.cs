﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace RSG.Interop.AutodeskMotionBuilder
{

    /// <summary>
    /// Autodesk MotionBuilder instance class; representing a running instance 
    /// on the local machine.
    /// </summary>
    public sealed class MotionBuilderInstance : IComparable<MotionBuilderInstance>
    {
        #region Constants
        /// <summary>
        /// Autodesk MotionBuilder process executable name.
        /// </summary>
        private const String MOTIONBUILDER_PROCESS_EXE = "motionbuilder";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// MotionBuilder instance process object.
        /// </summary>
        public Process Process
        {
            get;
            private set;
        }

        /// <summary>
        /// MotionBuilder version.
        /// </summary>
        public MotionBuilderVersion Version
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor from a local PID (process identifier).
        /// </summary>
        /// <param name="pid"></param>
        public MotionBuilderInstance(int pid)
        {
            Process p = Process.GetProcessById(pid);
            if (MOTIONBUILDER_PROCESS_EXE.Equals(p.MainModule.ModuleName))
                this.Process = p;
            else
                throw new NotSupportedException("Not valid Autodesk MotionBuilder process module.");
        }

        /// <summary>
        /// Constructor from a Process object.
        /// </summary>
        /// <param name="p"></param>
        private MotionBuilderInstance(Process p)
        {
            this.Process = p;
        }
        #endregion // Constructor(s)

        #region Object Overrides
        /// <summary>
        /// MotionBuilderInstance equality operator based on Process ID.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(Object obj)
        {
            if (!(obj is MotionBuilderInstance))
                return (false);

            return ((obj as MotionBuilderInstance).Process.Id.Equals(this.Process.Id));
        }

        /// <summary>
        /// MotionBuilderInstance get hash code override.
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return (this.Process.Id.GetHashCode());
        }
        #endregion // Object Overrides

        #region IComparable<MotionBuilderInstance> Interface
        /// <summary>
        /// Compare against another MotionBuilderInstance object.
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public int CompareTo(MotionBuilderInstance o)
        {
            return (this.Process.Id.CompareTo(o.Process.Id));
        }
        #endregion // IComparable<MaxInstance> Interface

        #region Static Controller Methods
        /// <summary>
        /// Return array of all running MotionBuilder instances.
        /// </summary>
        /// <returns></returns>
        public static MotionBuilderInstance[] GetRunningInstances()
        {
            List<MotionBuilderInstance> instances = new List<MotionBuilderInstance>();
            Process[] processes = Process.GetProcessesByName(MOTIONBUILDER_PROCESS_EXE);
            foreach (Process process in processes)
            {
                instances.Add(new MotionBuilderInstance(process));
            }
            instances.Sort();
            return (instances.ToArray());
        }
        #endregion // Static Controller Methods
    }

} // RSG.Interop.AutodeskMotionBuilder namespace
