// 
// parser/psoschema.cpp
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#include "mpsoschema.h"

#include "parser/psofile.h"
#include "parser/psoschema.h"

namespace RSG
{
namespace ManagedRage
{

MPsoStructureSchema^ MPsoStructureSchema::CreateFromUnmanaged( rage::psoSchemaCatalog& cat, rage::psoStructureSchema& sch )
{
	MPsoStructureSchema^ ret = gcnew MPsoStructureSchema;
	ret->m_NameHash = MAtLiteralHashValue(sch.GetNameHash());
	ret->m_BaseSignature = sch.ComputeBaseSignature();
	ret->m_Signature = cat.ComputeSignature(sch, false);
	ret->m_Size = sch.GetSize();
	ret->m_Version = sch.GetVersion().GetMajor();
	ret->m_Flags = sch.GetFlags().GetRawBlock(0);

	int numMembers = sch.GetNumMembers();
	for(int i = 0; i < numMembers; i++)
	{
		rage::psoMemberSchema mem = sch.GetMemberByIndex(i);

		MPsoMemberSchema^ mgdMem = nullptr;
		MPsoType type = MPsoType::FromNative(mem.GetType());

		if (type.Enum == ePsoType::TYPE_STRUCT)
		{
			MPsoStructureMemberSchema^ mgdStruct = gcnew MPsoStructureMemberSchema;
			mgdStruct->m_ReferentHash = MAtLiteralHashValue(mem.GetReferentHash());
			mgdMem = mgdStruct;
		}
		else if (type.IsEnum)
		{
			MPsoEnumMemberSchema^ mgdEnum = gcnew MPsoEnumMemberSchema;
			mgdEnum->m_ReferentHash = MAtLiteralHashValue(mem.GetReferentHash());
			mgdMem = mgdEnum;
		}
		else if (type.IsArray)
		{
			MPsoArrayMemberSchema^ mgdArray = gcnew MPsoArrayMemberSchema;
			mgdArray->m_ElementSchemaIndex = mem.GetArrayElementSchemaIndex();
			mgdArray->m_AlignmentPower = mem.GetArrayAlignmentPower();
			if (type.IsDirectContainer)
			{
				mgdArray->m_FixedArrayCount = mem.GetFixedArrayCount();
			}
			else if (type.IsArrayWithExternalCount)
			{
				mgdArray->m_CountMemberIndex = mem.GetArrayCounterSchemaIndex();
			}
			mgdMem = mgdArray;
		}
		else if (type.IsBitset)
		{
			MPsoBitsetMemberSchema^ mgdBitset = gcnew MPsoBitsetMemberSchema;
			mgdBitset->m_EnumSchemaHash = MAtLiteralHashValue(mem.GetBitsetEnumHash());
			mgdMem = mgdBitset;
		}
		else if (type.IsMap)
		{
			MPsoMapMemberSchema^ mgdMap = gcnew MPsoMapMemberSchema;
			mgdMap->m_KeySchemaIndex = mem.GetKeySchemaIndex();
			mgdMap->m_ValueSchemaIndex = mem.GetValueSchemaIndex();
			mgdMem = mgdMap;
		}
		else
		{
			mgdMem = gcnew MPsoMemberSchema;
		}
		mgdMem->m_NameHash = MAtLiteralHashValue(mem.GetNameHash());
		mgdMem->m_Offset = mem.GetOffset();
		mgdMem->m_Type = type;

		ret->m_Members.Add(mgdMem);
	}

	return ret;
}

MPsoMemberSchema^ MPsoStructureSchema::GetMemberByName( MAtLiteralHashValue hash )
{
	for(int i = 0; i < m_Members.Count; i++)
	{
		if (m_Members[i]->NameHash == hash)
		{
			return m_Members[i];
		}
	}
	return nullptr;
}

bool MPsoMemberSchema::IsAnonymous()
{
	return rage::psoConstants::IsAnonymous(m_NameHash.ToRage());
}

MPsoEnumSchema^ MPsoEnumSchema::CreateFromUnmanaged( rage::psoEnumSchema& sch )
{
	MPsoEnumSchema^ ret = gcnew MPsoEnumSchema;
	ret->m_NameHash = MAtLiteralHashValue(sch.GetNameHash());
	ret->m_Signature = sch.ComputeBaseSignature();

	for(int i = 0; i < sch.GetNumEnums(); i++)
	{
		MAtLiteralHashValue hash(sch.GetMemberNameHash(i));
		int val = sch.GetMemberValue(i);
		ret->m_NameFromValueMap[val] = hash;
		ret->m_ValueFromNameMap[hash] = val;
	}

	return ret;
}

MPsoSchemaCatalog^ MPsoSchemaCatalog::CreateFromUnmanaged(rage::psoFile& pso)
{
	MPsoSchemaCatalog^ ret = gcnew MPsoSchemaCatalog;

	rage::psoSchemaCatalog rageCat = pso.GetSchemaCatalog();

	for(rage::psoSchemaCatalog::StructureSchemaIterator iter = rageCat.BeginSchemas(); iter != rageCat.EndSchemas(); ++iter)
	{
		rage::psoStructureSchema sch = *iter;
		MPsoStructureSchema^ mgdSchema = MPsoStructureSchema::CreateFromUnmanaged(rageCat, sch);
		ret->m_StructureSchemas.Add(mgdSchema->NameHash, mgdSchema);
		ret->m_Schemas.Add(mgdSchema->NameHash, mgdSchema);
	}

	for(rage::psoSchemaCatalog::EnumSchemaIterator iter = rageCat.BeginEnums(); iter != rageCat.EndEnums(); ++iter)
	{
		rage::psoEnumSchema sch = *iter;
		MPsoEnumSchema^ mgdSchema = MPsoEnumSchema::CreateFromUnmanaged(sch);
		ret->m_EnumSchemas.Add(mgdSchema->NameHash, mgdSchema);
		ret->m_Schemas.Add(mgdSchema->NameHash, mgdSchema);
	}

	// Now "link" all the references
	for each (MPsoStructureSchema^ schema in ret->m_StructureSchemas.Values)
	{
		for each (MPsoMemberSchema^ member in schema->Members)
		{
			if (MPsoStructureMemberSchema^ memStruct = dynamic_cast<MPsoStructureMemberSchema^>(member))
			{
				if (memStruct->m_ReferentHash.Hash != 0)
				{
					ret->StructureSchemas->TryGetValue(memStruct->m_ReferentHash, memStruct->m_Referent);
				}
			}
			else if (MPsoEnumMemberSchema^ memEnum = dynamic_cast<MPsoEnumMemberSchema^>(member))
			{
				memEnum->m_Referent = ret->EnumSchemas[memEnum->ReferentHash];
			}
			else if (MPsoArrayMemberSchema^ memArray = dynamic_cast<MPsoArrayMemberSchema^>(member))
			{
				memArray->m_ElementSchema = schema->Members[memArray->ElementSchemaIndex];
			}
			else if (MPsoBitsetMemberSchema^ memBitset = dynamic_cast<MPsoBitsetMemberSchema^>(member))
			{
				if (memBitset->EnumSchemaHash.IsNotNull)
				{
					memBitset->m_EnumSchema = ret->EnumSchemas[memBitset->EnumSchemaHash];
				}
			}
			else if (MPsoMapMemberSchema^ memMap = dynamic_cast<MPsoMapMemberSchema^>(member))
			{
				memMap->m_KeySchema = schema->Members[memMap->m_KeySchemaIndex];
				memMap->m_ValueSchema = schema->Members[memMap->m_ValueSchemaIndex];
			}
		}
	}

	return ret;
}


} // ManagedRage
} // RSG