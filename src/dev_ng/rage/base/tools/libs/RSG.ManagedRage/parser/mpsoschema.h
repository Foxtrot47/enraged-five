// 
// parser\psoschema.h 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#ifndef MANAGEDRAGE_PARSER_PSOSCHEMA_H
#define MANAGEDRAGE_PARSER_PSOSCHEMA_H

#include <cstdlib>

#include "mpsotypes.h"

// Managed includes
#include "../core/Types.h"
#include "../atl/hashstring.h"

// Unmanaged includes
#include "parser/psotypes.h"

#using <System.dll>
using namespace System::Runtime::InteropServices;
using namespace System::Collections::Generic;

namespace rage
{
	class psoEnumSchema;
	class psoFile;
	class psoMemberSchema;
	class psoSchemaCatalog;
	class psoStructureSchema;
}

namespace RSG
{
	namespace ManagedRage
	{
		ref class MPsoStructureSchema;
		ref class MPsoEnumSchema;

		public ref class MPsoMemberSchema
		{
		public:
			MPsoMemberSchema() : m_Offset(0) {}

			property MAtLiteralHashValue NameHash { MAtLiteralHashValue get() { return m_NameHash; } }
			property MPsoType Type { MPsoType get() { return m_Type; } }
			property ptrdiff_t Offset { ptrdiff_t get() { return m_Offset; } }

			bool IsAnonymous();

		internal:
			MAtLiteralHashValue m_NameHash;
			MPsoType m_Type;
			ptrdiff_t m_Offset;
		};

		public ref class MPsoStructureMemberSchema : public MPsoMemberSchema
		{
		public:
			property MAtLiteralHashValue ReferentHash { MAtLiteralHashValue get() { return m_ReferentHash; } }
			property MPsoStructureSchema^ Referent { MPsoStructureSchema^ get() { return m_Referent; } }

		internal:
			MAtLiteralHashValue m_ReferentHash;
			MPsoStructureSchema^ m_Referent;
		};

		public ref class MPsoEnumMemberSchema : public MPsoMemberSchema
		{
		public:
			property MAtLiteralHashValue ReferentHash { MAtLiteralHashValue get() { return m_ReferentHash; } }
			property MPsoEnumSchema^ Referent { MPsoEnumSchema^ get() { return m_Referent; } }

		internal:
			MAtLiteralHashValue m_ReferentHash;
			MPsoEnumSchema^ m_Referent;
		};

		public ref class MPsoArrayMemberSchema : public MPsoMemberSchema
		{
		public:
			MPsoArrayMemberSchema() : m_ElementSchemaIndex(0), m_AlignmentPower(0), m_FixedArrayCount(-1), m_CountMemberIndex(-1) {}

			// All arrays define this, what is it an array _of_
			property u32 ElementSchemaIndex { u32 get() { return m_ElementSchemaIndex; } }
			property MPsoMemberSchema^ ElementSchema { MPsoMemberSchema^ get() { return m_ElementSchema; } }

			property u32 AlignmentPower { u32 get() { return m_AlignmentPower; } }
			property u32 Alignment { u32 get() { return 1 << m_AlignmentPower; } } 

			// Fixed sized arrays define this - for non-fixed it's -1
			property int FixedArrayCount { int get() { return m_FixedArrayCount; } }

			// Pointer_with_count arrays define this, for others it's -1. Note this is an offset in bytes from the start of the structure
			property int CountMemberOffset { int get() { return m_CountMemberIndex; } }

		internal:
			u32 m_ElementSchemaIndex, m_AlignmentPower;
			int m_FixedArrayCount, m_CountMemberIndex;
			MPsoMemberSchema^ m_ElementSchema;
		};

		public ref class MPsoBitsetMemberSchema : public MPsoMemberSchema
		{
		public:
			MPsoBitsetMemberSchema() {}

			property MAtLiteralHashValue EnumSchemaHash { MAtLiteralHashValue get() { return m_EnumSchemaHash; } }
			property MPsoEnumSchema^ EnumSchema { MPsoEnumSchema^ get() { return m_EnumSchema; } }

		internal:
			MAtLiteralHashValue m_EnumSchemaHash;
			MPsoEnumSchema^ m_EnumSchema;
		};

		public ref class MPsoMapMemberSchema : public MPsoMemberSchema
		{
		public:
			MPsoMapMemberSchema() : m_KeySchemaIndex(-1), m_ValueSchemaIndex(-1) {}

			property int KeySchemaIndex { int get() { return m_KeySchemaIndex; } }
			property int ValueSchemaIndex { int get() { return m_ValueSchemaIndex; } }
			property MPsoMemberSchema^ KeySchema { MPsoMemberSchema^ get() { return m_KeySchema; } }
			property MPsoMemberSchema^ ValueSchema { MPsoMemberSchema^ get() { return m_ValueSchema; } }

		internal:
			int m_KeySchemaIndex, m_ValueSchemaIndex;
			MPsoMemberSchema^ m_KeySchema;
			MPsoMemberSchema^ m_ValueSchema;
		};

		public ref class MPsoSchema abstract
		{
		public:
			MPsoSchema() {}

			property MAtLiteralHashValue NameHash { MAtLiteralHashValue get() { return m_NameHash; }}

		internal:
			MAtLiteralHashValue m_NameHash;
		};

		public ref class MPsoStructureSchema : public MPsoSchema
		{
		public:
			MPsoStructureSchema() : m_BaseSignature(0), m_Signature(0), m_Size(0), m_Version(0), m_Flags(0) {}

			property u32 BaseSignature { u32 get() { return m_BaseSignature; } }
			property u32 Signature { u32 get() { return m_Signature; } }
			property size_t Size { size_t get() { return m_Size; } }
			property u32 Version { u32 get() { return m_Version; } }
			property u8 Flags { u8 get() { return m_Flags; } }

			property IList<MPsoMemberSchema^>^ Members { IList<MPsoMemberSchema^>^ get() { return %m_Members; } }
			MPsoMemberSchema^ GetMemberByName(MAtLiteralHashValue hash);

		internal:
			static MPsoStructureSchema^ CreateFromUnmanaged(rage::psoSchemaCatalog& cat, rage::psoStructureSchema& sch);

			u32 m_BaseSignature, m_Signature, m_Version;
			size_t m_Size;
			u8 m_Flags;
			MAtLiteralHashValue m_Name;

			List<MPsoMemberSchema^> m_Members;
		};

		public ref class MPsoEnumSchema : public MPsoSchema
		{
		public:
			MPsoEnumSchema() : m_Signature(0) {} 

			property u32 Signature { u32 get() { return m_Signature;} }

			property IDictionary<int, MAtLiteralHashValue>^ NameFromValue { IDictionary<int, MAtLiteralHashValue>^ get() { return %m_NameFromValueMap; } }
			property IDictionary<MAtLiteralHashValue, int>^ ValueFromName { IDictionary<MAtLiteralHashValue, int>^ get() { return %m_ValueFromNameMap; } }

		internal:
			static MPsoEnumSchema^ CreateFromUnmanaged(rage::psoEnumSchema& sch);

			Dictionary<int, MAtLiteralHashValue> m_NameFromValueMap;
			Dictionary<MAtLiteralHashValue, int> m_ValueFromNameMap;

			u32 m_Signature;
		};

		public ref class MPsoSchemaCatalog
		{
		public:
			property IDictionary<MAtLiteralHashValue, MPsoSchema^>^ Schemas { 
				IDictionary<MAtLiteralHashValue, MPsoSchema^>^ get() { return %m_Schemas; } 
			}

			property IDictionary<MAtLiteralHashValue, MPsoStructureSchema^>^ StructureSchemas
			{
				IDictionary<MAtLiteralHashValue, MPsoStructureSchema^>^ get() { return %m_StructureSchemas; }
			}

			property IDictionary<MAtLiteralHashValue, MPsoEnumSchema^>^ EnumSchemas
			{
				IDictionary<MAtLiteralHashValue, MPsoEnumSchema^>^ get() { return %m_EnumSchemas;}
			}

		internal:
			Dictionary<MAtLiteralHashValue, MPsoSchema^> m_Schemas;
			Dictionary<MAtLiteralHashValue, MPsoStructureSchema^> m_StructureSchemas;
			Dictionary<MAtLiteralHashValue, MPsoEnumSchema^> m_EnumSchemas;

			static MPsoSchemaCatalog^ CreateFromUnmanaged(rage::psoFile& file);
		};
	}
}

#endif // MANAGEDRAGE_PARSER_PSOSCHEMA_H