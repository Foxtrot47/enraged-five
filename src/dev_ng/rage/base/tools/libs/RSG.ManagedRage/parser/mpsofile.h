// 
// parser/psofile.h 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#ifndef MANAGEDRAGE_PARSER_PSOFILE_H
#define MANAGEDRAGE_PARSER_PSOFILE_H

#using <System.dll>
using namespace System::Collections::Generic;

#include "../core/Types.h"

#include "mpsodata.h"
#include "mpsoschema.h"

// Forward declarations
namespace rage
{
	class psoFile;
}

namespace RSG
{

namespace ManagedRage
{
	public ref class MPsoStructArray
	{
	public:
		MPsoStructArray(u32 typeHash, u32 size);
		// Info that comes direct from the file
		property u32 TypeHash { u32 get() { return m_TypeHash; } }
		property u32 Size { u32 get() { return m_Size; } }
		// TODO: Do we need an accessor for the pointer to the data?

		// "Derived" info
		property System::String^ Name { System::String^ get(); }
		property bool IsArrayOfStructures { bool get() { return m_Schema != nullptr; } }
		property MPsoStructureSchema^ Schema { MPsoStructureSchema^ get() { return m_Schema; } }
		property MPsoType PodType { MPsoType get() { return m_PodType; } }
		property ptrdiff_t Count { ptrdiff_t get() { return m_Count; } }
	internal:
		u32 m_TypeHash;
		size_t m_Size;
		MPsoStructureSchema^ m_Schema;
		int m_Count;
		MPsoType m_PodType;
	};

	public ref class MPsoStructureMap
	{
	public:
		property IList<MPsoStructArray^>^ StructArrays 
		{
			IList<MPsoStructArray^>^ get() { return %m_StructArrays; }
		}

	internal:
		List<MPsoStructArray^> m_StructArrays;
	};

	public ref class MPsoFile
	{
	public:
		MPsoFile();
		~MPsoFile();
		!MPsoFile();

		static MPsoFile^ Load(System::String^ filename);

		static MPsoFile^ Load(array<System::Byte>^ data);


		MPsoStruct^ GetInstance(MPsoStructId id);

		property MPsoStruct^ RootInstance
		{
			MPsoStruct^ get() { return m_Root; }
		}

		property MPsoSchemaCatalog^ SchemaCatalog 
		{ 
			MPsoSchemaCatalog^ get() {return m_SchemaCatalog;}
		}

		property MPsoStructureMap^ StructureMap 
		{
			MPsoStructureMap^ get() { return %m_StructureMap;}
		}

		bool Save(System::String^ filename);

		bool Save(array<System::Byte>^ data, size_t dataSize);
	internal:
		void PopulateStructureMap();

		static MPsoFile^ CreateFromUnmanaged( rage::psoFile* newfile );

		::rage::psoFile*		m_UnmanagedFile;
		MPsoStruct^				m_Root;
		MPsoSchemaCatalog^		m_SchemaCatalog;
		MPsoStructureMap		m_StructureMap;
	};

}

}

#endif // MANAGEDRAGE_PARSER_PSOFILE_H
