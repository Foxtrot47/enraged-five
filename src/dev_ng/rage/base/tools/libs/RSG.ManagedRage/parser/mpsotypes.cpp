// 
// parser/psoschema.cpp
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

// RSG.ManagedRage includes
#include "mpsotypes.h"
#include "../MarshallingUtils.h"

// Rage includes
#include "parser/psofile.h"
#include "parser/psoschema.h"


namespace RSG
{
namespace ManagedRage
{
	System::String^ MPsoType::ToString()
	{
		return CharArrayToSystemString(GetNative().GetName());
	}

} // ManagedRage
} // RSG