// 
// parser\psodata.cpp
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#include "mpsodata.h"

// managed includes
#include "mpsofile.h"
#include "mpsoschema.h"
#include "../MarshallingUtils.h"

// rage includes
#include "parser/psodata.h"
#include "parser/psoschema.h"

namespace RSG
{
namespace ManagedRage
{

bool MPsoMember::IsValid()
{
	return m_UnmanagedMember->IsValid();
}

generic <typename T> where T : value struct
T MPsoMember::GetDataAs()
{
	// http://www.codeproject.com/Articles/25896/Reading-Unmanaged-Data-Into-Structures
	T value;
	pin_ptr<T> dst = &value;
	memcpy((void*)dst, (void*)m_UnmanagedMember->GetRawDataPtr(), sizeof(T));
	return value;
}

generic <typename T> where T : value struct
void MPsoMember::SetData(T val)
{
	memcpy((void*)m_UnmanagedMember->GetRawDataPtr(), (void*)&val, sizeof(T));
}

MPsoMember^ MPsoMember::CreateFromUnmanaged( MPsoFile^ file, MPsoStruct^ parent, MPsoMemberSchema^ schema, rage::psoMember* unmanaged )
{
	MPsoMember^ ret = nullptr;

	switch(schema->m_Type.Enum)
	{
	case ePsoType::TYPE_STRUCT:
	case ePsoType::TYPE_POINTER32:
	case ePsoType::TYPE_POINTER64:
		{
			MPsoStructMember^ structMem = gcnew MPsoStructMember;
			ret = structMem;
		}
		break;
	case ePsoType::TYPE_ARRAY_MEMBER:
	case ePsoType::TYPE_ARRAY_POINTER32:
	case ePsoType::TYPE_ARRAY_POINTER64:
	case ePsoType::TYPE_ARRAY_POINTER32_WITH_COUNT:
	case ePsoType::TYPE_ARRAY_POINTER64_WITH_COUNT:
	case ePsoType::TYPE_ATARRAY32:
	case ePsoType::TYPE_ATARRAY64:
	case ePsoType::TYPE_ATARRAY32_32BITIDX:
	case ePsoType::TYPE_ATARRAY64_32BITIDX:
		{
			MPsoArrayMember^ arrayMem = gcnew MPsoArrayMember;
			ret = arrayMem;
		}
		break;
	default:
		ret = gcnew MPsoMember;
	}
	ret->m_File = file;
	ret->m_Schema = schema;
	ret->m_Struct = parent;
	ret->m_UnmanagedMember = unmanaged;

	return ret;
}

System::String^ MPsoMember::GetStringData()
{
	if (Schema->Type.IsNarrowString)
	{
		return CharArrayToSystemString(m_UnmanagedMember->GetStringData());
	}
	else
	{
		return CharArrayToSystemString((wchar_t*)m_UnmanagedMember->GetWideStringData());
	}
}

void MPsoMember::SetStringData(System::String^ value)
{
	if (Schema->Type.IsNarrowString)
	{
		char* str = (char*)(void*)Marshal::StringToHGlobalAnsi(value);

		m_UnmanagedMember->SetStringData(str);
	}
}

System::String^ MPsoMember::ToString()
{
	return System::String::Format("{0} @ 0x{1:x8}", Schema->NameHash, (size_t)m_UnmanagedMember->GetRawDataPtr());
}

MPsoMember::!MPsoMember()
{
	delete m_UnmanagedMember;
}

MPsoStruct^ MPsoStructMember::SubStructure::get() 
{
	rage::psoStruct* substruct = new rage::psoStruct;
	*substruct = m_UnmanagedMember->GetSubStructure();
	if (substruct->IsValid() && !substruct->IsNull())
	{
		u32 schemaHash = substruct->GetSchema().GetNameHash().GetHash();
		return MPsoStruct::CreateFromUnmanaged(m_File, 
			m_File->SchemaCatalog->StructureSchemas[MAtLiteralHashValue(schemaHash)],
			substruct
			);
	}
	else
	{
		delete substruct;
		return nullptr;
	}
}

int MPsoArrayMember::Count::get()
{
	rage::psoMemberArrayInterface arrayInterface(*(m_Struct->m_UnmanagedStruct), *m_UnmanagedMember);
	return arrayInterface.GetArrayCount();
}

MPsoMemberSchema^ MPsoArrayMember::ElementSchema::get()
{
	return safe_cast<MPsoArrayMemberSchema^>(m_Schema)->ElementSchema;
}

MPsoMember^ MPsoArrayMember::default::get(int i)
{
	rage::psoMemberArrayInterface arrayInterface(*(m_Struct->m_UnmanagedStruct), *m_UnmanagedMember);

	rage::psoMember member = arrayInterface.GetElement(i);
	if (member.IsValid())
	{
		rage::psoMember* heapMember = new rage::psoMember(member.GetSchema(), member.GetRawDataPtr(), m_File->m_UnmanagedFile);
		return MPsoMember::CreateFromUnmanaged(m_File, m_Struct, ElementSchema, heapMember);
	}
	else
	{
		return nullptr;
	}
}


bool MPsoArrayMember::Enumerator::MoveNext()
{
	m_Index++;
	if (m_Index < m_Collection->Count)
	{
		m_Current = m_Collection[m_Index];
		return true;
	}
	else
	{
		return false;
	}
}

int MPsoMapMember::Count::get()
{
	rage::psoMemberMapInterface mapInterface(*(m_Struct->m_UnmanagedStruct), *m_UnmanagedMember);
	return mapInterface.GetMapCount();
}

MPsoMemberSchema^ MPsoMapMember::KeySchema::get()
{
	return safe_cast<MPsoMapMemberSchema^>(m_Schema)->KeySchema;
}

MPsoMemberSchema^ MPsoMapMember::ValueSchema::get()
{
	return safe_cast<MPsoMapMemberSchema^>(m_Schema)->ValueSchema;
}

MPsoMapMember::KeyVal MPsoMapMember::default::get(int i)
{
	rage::psoMemberMapInterface mapInterface(*(m_Struct->m_UnmanagedStruct), *(m_UnmanagedMember));

	rage::psoMember keyMember = mapInterface.GetKey(i);
	rage::psoMember valueMember = mapInterface.GetValue(i);
	if (keyMember.IsValid() && valueMember.IsValid())
	{
		rage::psoMember* heapKey = new rage::psoMember(keyMember.GetSchema(), keyMember.GetRawDataPtr(), m_File->m_UnmanagedFile);
		rage::psoMember* heapValue = new rage::psoMember(valueMember.GetSchema(), valueMember.GetRawDataPtr(), m_File->m_UnmanagedFile);

		return KeyVal(
			MPsoMember::CreateFromUnmanaged(m_File, m_Struct, KeySchema, heapKey),
			MPsoMember::CreateFromUnmanaged(m_File, m_Struct, ValueSchema, heapValue));
	}
	return KeyVal(nullptr, nullptr);
}

bool MPsoMapMember::Enumerator::MoveNext()
{
	m_Index++;
	if (m_Index < m_Collection->Count)
	{
		m_Current = (*m_Collection)[m_Index];
		return (m_Current.Key != nullptr && m_Current.Value != nullptr);
	}
	return false;
}

MPsoStruct^ MPsoStruct::CreateFromUnmanaged( MPsoFile^ file, MPsoStructureSchema^ schema, rage::psoStruct* unmanaged )
{
	MPsoStruct^ ret = gcnew MPsoStruct;
	ret->m_File = file;
	ret->m_Schema = schema;
	ret->m_UnmanagedStruct = unmanaged;
	return ret;
}

MPsoStruct::~MPsoStruct()
{
	// release managed resources
	this->!MPsoStruct();
}

MPsoStruct::!MPsoStruct()
{
	// release unmanaged resources
	delete m_UnmanagedStruct;
}

MPsoMember^ MPsoStruct::GetMemberByIndex( int index )
{
	rage::psoMember stackMember = m_UnmanagedStruct->GetMemberByIndex(index);
	MPsoMemberSchema^ memSchema = m_Schema->Members[index];
	if (stackMember.IsValid() && memSchema)
	{
		rage::psoMember* heapMember = new rage::psoMember(stackMember.GetSchema(), stackMember.GetRawDataPtr(), m_File->m_UnmanagedFile);
		return MPsoMember::CreateFromUnmanaged(m_File, this, memSchema, heapMember);
	}
	else
	{
		return nullptr;
	}
}

MPsoMember^ MPsoStruct::GetMemberByName( MAtLiteralHashValue hash )
{
	rage::psoMember stackMember = m_UnmanagedStruct->GetMemberByName(hash.ToRage());
	MPsoMemberSchema^ memSchema = m_Schema->GetMemberByName(hash);
	if (stackMember.IsValid() && memSchema)
	{
		rage::psoMember* heapMember = new rage::psoMember(stackMember.GetSchema(), stackMember.GetRawDataPtr(), m_File->m_UnmanagedFile);
		return MPsoMember::CreateFromUnmanaged(m_File, this, memSchema, heapMember);
	}
	else
	{
		return nullptr;
	}
}

bool MPsoStruct::Enumerator::MoveNext()
{
	m_Current = nullptr;
	IList<MPsoMemberSchema^>^ memberList = m_Collection->Schema->Members;
	do 
	{
		m_Index++;
		if (m_Index >= memberList->Count)
		{
			return false;
		}

		MPsoMemberSchema^ mem = (*memberList)[m_Index];
		if (!mem->IsAnonymous())
		{
			m_Current = m_Collection->GetMemberByIndex(m_Index);
			return true;
		}
	} while (m_Current == nullptr);
	return false;
}


System::String^ MPsoStruct::ToString()
{
	return System::String::Format("{0} @ 0x{1:x8}", m_Schema->NameHash, (size_t)m_UnmanagedStruct->GetInstanceData());
}

}
}
