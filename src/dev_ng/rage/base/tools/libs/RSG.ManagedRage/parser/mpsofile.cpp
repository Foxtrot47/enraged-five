// 
// parser\psofile.cpp 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#include "mpsofile.h"

#include "parser/psoconsts.h"
#include "parser/psofile.h"

// RageCore headers
#include "file/device.h"
#include "file/limits.h"

using namespace System::Runtime::InteropServices;

namespace RSG
{
namespace ManagedRage
{

MPsoStructArray::MPsoStructArray( u32 typeHash, u32 size ) 
	: m_TypeHash(typeHash)
	, m_Size(size)
{
	if (rage::psoConstants::IsPsoType(typeHash))
	{
		rage::psoType native = rage::psoType::ConvertOldStructArrayTypeToPsoType(typeHash, false);
		m_PodType = MPsoType::FromNative(native);
	}
}

System::String^ MPsoStructArray::Name::get() 
{
	if (rage::psoConstants::IsPsoType(TypeHash))
	{
		return PodType.ToString();
	}
	else if (rage::psoConstants::IsAnonymous(TypeHash))
	{
		return System::String::Format("Anon {0}", TypeHash);
	}
	else 
	{
		MAtLiteralHashValue hash(TypeHash);
		return hash.ToString();
	}
}

MPsoFile^ MPsoFile::Load( System::String^ filename )
{
	char* str = (char*)(void*)Marshal::StringToHGlobalAnsi(filename);

	rage::psoFile* newfile = rage::psoLoadFile(str, rage::PSOLOAD_NO_PREP);
	MPsoFile^ ret = CreateFromUnmanaged(newfile);

	Marshal::FreeHGlobal((System::IntPtr)str);

	return ret;
}

MPsoFile^ MPsoFile::Load(array<System::Byte>^ data)
{
	int length = data->Length;
	pin_ptr<unsigned char> pinnedPtr = &data[0];
	char* nativeCopy = new char[length];
	memcpy(nativeCopy, pinnedPtr, length);

	rage::psoFile* newfile = rage::psoLoadFileFromBuffer(nativeCopy, length, "", rage::PSOLOAD_NO_PREP, true);
	MPsoFile^ ret = CreateFromUnmanaged(newfile);

	return ret;
}

bool MPsoFile::Save( System::String^ filename )
{
	char* str = (char*)(void*)Marshal::StringToHGlobalAnsi(filename);

	return m_UnmanagedFile->SaveFile(str);
}

bool MPsoFile::Save(array<System::Byte>^ data, size_t dataSize)
{
	char* nativeBuf = new char[dataSize];
	memset(nativeBuf, 'p', dataSize);
	char memname[::rage::RAGE_MAX_PATH];
	::rage::fiDevice::MakeMemoryFileName(&memname[0], ::rage::RAGE_MAX_PATH, nativeBuf, dataSize, false, "");
	bool result = m_UnmanagedFile->SaveFile(&memname[0]);

	pin_ptr<unsigned char> pinnedPtr = &data[0];
	memcpy(pinnedPtr, nativeBuf, dataSize);

	delete nativeBuf;
	return true;
}

MPsoFile::MPsoFile() 
	: m_UnmanagedFile(NULL)
	, m_SchemaCatalog(nullptr)
{
}

MPsoFile::~MPsoFile()
{
	// Delete managed resources
	delete m_SchemaCatalog;

	this->!MPsoFile();
}

MPsoFile::!MPsoFile()
{
	// Delete unmanaged resources
	delete m_UnmanagedFile;
}


void MPsoFile::PopulateStructureMap()
{
	rage::psoFile* rageFile = m_UnmanagedFile;

	for(u32 i = 0; i < rageFile->GetNumStructArrays(); i++)
	{
		rage::psoRscStructArrayTableData& rageData = rageFile->GetStructArrayEntry(i);

		MPsoStructArray^ s = gcnew MPsoStructArray(rageData.m_NameHash.GetHash(), rageData.m_Size);

		if (rage::psoConstants::IsPsoType(rageData.m_NameHash))
		{
			rage::psoType psotype = (rage::psoType::Type)rageData.m_NameHash.GetHash(); 
			s->m_PodType = MPsoType::FromNative(psotype);
			u32 eltSize = psotype.GetSize();

			// Handle 0s?
			s->m_Count = eltSize > 0 ? s->m_Size / eltSize : -1;
		}
		else
		{
			s->m_Schema = m_SchemaCatalog->StructureSchemas[MAtLiteralHashValue(rageData.m_NameHash)];
			s->m_Count = s->m_Size / s->m_Schema->Size;
		}

		m_StructureMap.m_StructArrays.Add(s);
	}
}

MPsoStruct^ MPsoFile::GetInstance( MPsoStructId id )
{
	rage::psoStruct* str = new rage::psoStruct;
	*str = m_UnmanagedFile->GetInstance(rage::psoStructId::Create(id.TableIndex, id.TableOffset));
	if (str->IsValid() && !str->IsNull())
	{
		MAtLiteralHashValue schemaNameHash(str->GetSchema().GetNameHash());
		return MPsoStruct::CreateFromUnmanaged(this, SchemaCatalog->StructureSchemas[schemaNameHash], str);
	}
	else
	{
		delete str;
		return nullptr;
	}
}

MPsoFile^ MPsoFile::CreateFromUnmanaged( rage::psoFile* newfile )
{
	if (newfile)
	{
		MPsoFile^ ret = gcnew MPsoFile;
		ret->m_UnmanagedFile = newfile;
		ret->m_SchemaCatalog = MPsoSchemaCatalog::CreateFromUnmanaged(*newfile);

		ret->PopulateStructureMap();

		rage::psoStruct* rootStr = new rage::psoStruct;
		*rootStr = newfile->GetRootInstance();
		MAtLiteralHashValue rootTypeName(rootStr->GetSchema().GetNameHash());
		ret->m_Root = MPsoStruct::CreateFromUnmanaged(ret, ret->m_SchemaCatalog->StructureSchemas[rootTypeName], rootStr);
		return ret;
	}
	return nullptr;
}


}
}

