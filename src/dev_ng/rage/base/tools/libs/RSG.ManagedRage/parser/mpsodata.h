// 
// parser\psodata.h 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#ifndef MANAGEDRAGE_PARSER_PSODATA_H
#define MANAGEDRAGE_PARSER_PSODATA_H

#using <System.dll>
using namespace System::Runtime::InteropServices;
using namespace System::Collections::Generic;

#include "../core/Types.h"
#include "../atl/hashstring.h"

namespace rage
{
	class psoStruct;
	class psoMember;
}

namespace RSG
{

namespace ManagedRage
{
	ref class MPsoFile;
	ref class MPsoMemberSchema;
	ref class MPsoStruct;
	ref class MPsoStructureSchema;

	public value struct MPsoStructId
	{
	public:
		MPsoStructId(u32 index, u32 offset) : TableIndex(index), TableOffset(offset) {}
		initonly u32 TableIndex;
		initonly u32 TableOffset;
	};

	public ref class MPsoMember
	{
	public:
		MPsoMember() : m_UnmanagedMember(NULL) {}
		virtual ~MPsoMember() { this->!MPsoMember(); }
		!MPsoMember();

		bool IsValid();

		property MPsoMemberSchema^ Schema { MPsoMemberSchema^ get() { return m_Schema; } }

		generic <typename T> where T : value struct
		T GetDataAs();

		generic <typename T> where T : value struct
		void SetData(T val);

		System::String^ GetStringData(); // for string types only

		void SetStringData(System::String^ value);

		virtual System::String^ ToString() override;

	internal:
		static MPsoMember^ CreateFromUnmanaged(MPsoFile^ file, MPsoStruct^ parent, MPsoMemberSchema^ schema, rage::psoMember* unmanaged);

		rage::psoMember* m_UnmanagedMember;
		MPsoMemberSchema^ m_Schema;
		MPsoStruct^ m_Struct;
		MPsoFile^ m_File; // See MPsoStruct::m_File for why this is here
	};

	public ref class MPsoStructMember : public MPsoMember
	{
	public:
		property MPsoStruct^ SubStructure { MPsoStruct^ get(); }
	};

	public ref class MPsoArrayMember : public MPsoMember, public IEnumerable<MPsoMember^>
	{
	public:
		property MPsoMemberSchema^ ElementSchema { MPsoMemberSchema^ get(); }

		property MPsoMember^ default[int] { MPsoMember^ get(int i); }
		property int Count { int get(); }

		virtual IEnumerator<MPsoMember^>^ GetEnumerator() { return gcnew Enumerator(this); }
		virtual System::Collections::IEnumerator^ GetEnumeratorBase() = System::Collections::IEnumerable::GetEnumerator
		{
			return GetEnumerator();
		}

	private:
		ref class Enumerator : public IEnumerator<MPsoMember^>
		{
		public:
			~Enumerator() {}

			Enumerator(MPsoArrayMember^ coll) : m_Index(-1), m_Collection(coll) {}
			property MPsoMember^ Current { virtual MPsoMember^ get() { return m_Current; } }
			virtual bool MoveNext();
			virtual void Reset() { m_Index = -1; m_Current = nullptr; }

			property Object^ CurrentBase { virtual Object^ get() sealed = System::Collections::IEnumerator::Current::get { return Current; } }

		private:
			int m_Index;
			MPsoArrayMember^ m_Collection;
			MPsoMember^ m_Current;
		};
	};

	// A PSO map looks like an array of key,value pairs
	public ref class MPsoMapMember : public MPsoMember, public IEnumerable<KeyValuePair<MPsoMember^, MPsoMember^> >
	{
	public:
		typedef KeyValuePair<MPsoMember^, MPsoMember^> KeyVal;

		property MPsoMemberSchema^ KeySchema { MPsoMemberSchema^ get(); }
		property MPsoMemberSchema^ ValueSchema { MPsoMemberSchema^ get(); }

		property KeyVal default[int] { KeyVal get(int i); }
		property int Count { int get(); }

		virtual IEnumerator<KeyVal>^ GetEnumerator() { return gcnew Enumerator(this); }
		virtual System::Collections::IEnumerator^ GetEnumeratorBase() = System::Collections::IEnumerable::GetEnumerator { return GetEnumerator(); }

	private:
		ref class Enumerator : public IEnumerator<KeyVal>
		{
		public:
			~Enumerator() {}
			Enumerator(MPsoMapMember^ coll) : m_Index(-1), m_Collection(coll) {}
			property KeyVal Current { virtual KeyVal get() { return m_Current; } }
			virtual bool MoveNext();
			virtual void Reset() { m_Index = -1; m_Current = KeyVal(nullptr, nullptr);}

			property Object^ CurrentBase { virtual Object^ get() sealed = System::Collections::IEnumerator::Current::get { return Current; } }

		private:
			int m_Index;
			MPsoMapMember^ m_Collection;
			KeyVal m_Current;
		};
	};

	public ref class MPsoStruct : public IEnumerable<MPsoMember^>
	{
	public:
		MPsoStruct() : m_UnmanagedStruct(NULL) {}
		~MPsoStruct();
		!MPsoStruct();

		property MPsoStructureSchema^ Schema { MPsoStructureSchema^ get() { return m_Schema; } }
		MPsoMember^ GetMemberByIndex(int index);
		MPsoMember^ GetMemberByName(MAtLiteralHashValue hash);

		property MPsoMember^ default[MAtLiteralHashValue] { MPsoMember^ get(MAtLiteralHashValue hash) {return GetMemberByName(hash); } }

		// NOTE! This enumerator is a little different from the one in the schema. This will only enumerate NAMED members of the structure
		// (which is probably what you mean by iterating over the members. But the schema has some anon members that won't get enumerated.
		// they are just for supporting data though)
		virtual IEnumerator<MPsoMember^>^ GetEnumerator() { return gcnew Enumerator(this); }
		virtual System::Collections::IEnumerator^ GetEnumeratorBase() = System::Collections::IEnumerable::GetEnumerator { return GetEnumerator(); }

		virtual System::String^ ToString() override;

	internal:
		static MPsoStruct^ CreateFromUnmanaged(MPsoFile^ file, MPsoStructureSchema^ schema, rage::psoStruct* unmanaged);

		rage::psoStruct* m_UnmanagedStruct;
		MPsoStructureSchema^ m_Schema;
		// This class keeps a file handle b/c we can't free the file and keep this alive, 
		// since m_UnmanagedStruct points into memory ultimately owned by the MPsoFile
		MPsoFile^ m_File; 

	private:
		ref class Enumerator : public IEnumerator<MPsoMember^>
		{
		public:
			~Enumerator() {}
			Enumerator(MPsoStruct^ coll) : m_Index(-1), m_Collection(coll) {}
			property MPsoMember^ Current { virtual MPsoMember^ get() { return m_Current; } }
			virtual bool MoveNext();
			virtual void Reset() { m_Index = -1; m_Current = nullptr; }

			property Object^ CurrentBase { virtual Object^ get() sealed = System::Collections::IEnumerator::Current::get { return Current; } }
		private:
			int m_Index;
			MPsoStruct^ m_Collection;
			MPsoMember^ m_Current;
		};
	};

}

}

#endif // MANAGEDRAGE_PARSER_PSODATA_H
