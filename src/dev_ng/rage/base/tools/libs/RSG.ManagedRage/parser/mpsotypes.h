// 
// parser\psoschema.h 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#ifndef MANAGEDRAGE_PARSER_PSOTYPES_H
#define MANAGEDRAGE_PARSER_PSOTYPES_H

#include <cstdlib>

// Managed includes
#include "../core/Types.h"
#include "../atl/hashstring.h"

// Unmanaged includes
#include "parser/psotypes.h"

#using <System.dll>
using namespace System::Runtime::InteropServices;
using namespace System::Collections::Generic;

namespace RSG
{
	namespace ManagedRage
	{
		public enum class ePsoType
		{
			// Core types
			TYPE_INVALID	= rage::psoType::TYPE_INVALID,
			TYPE_BOOL		= rage::psoType::TYPE_BOOL,
			TYPE_BOOLV		= rage::psoType::TYPE_BOOLV,
			TYPE_VECBOOLV	= rage::psoType::TYPE_VECBOOLV,
			TYPE_STRUCTID	= rage::psoType::TYPE_STRUCTID,
			TYPE_STRUCT		= rage::psoType::TYPE_STRUCT,
			TYPE_POINTER32	= rage::psoType::TYPE_POINTER32,
			TYPE_POINTER64	= rage::psoType::TYPE_POINTER64,

			TYPE_S8			= rage::psoType::TYPE_S8,
			TYPE_U8			= rage::psoType::TYPE_U8,
			TYPE_S16		= rage::psoType::TYPE_S16,
			TYPE_U16		= rage::psoType::TYPE_U16,
			TYPE_S32		= rage::psoType::TYPE_S32,
			TYPE_U32		= rage::psoType::TYPE_U32,
			TYPE_S64		= rage::psoType::TYPE_S64,
			TYPE_U64		= rage::psoType::TYPE_U64,

			TYPE_FLOAT16	= rage::psoType::TYPE_FLOAT16,
			TYPE_FLOAT		= rage::psoType::TYPE_FLOAT,
			TYPE_SCALARV	= rage::psoType::TYPE_SCALARV,

			TYPE_VEC2		= rage::psoType::TYPE_VEC2,
			TYPE_VEC2V		= rage::psoType::TYPE_VEC2V,
			TYPE_VEC3		= rage::psoType::TYPE_VEC3,
			TYPE_VEC3V		= rage::psoType::TYPE_VEC3V,
			TYPE_VEC4V		= rage::psoType::TYPE_VEC4V,
			TYPE_MAT33V		= rage::psoType::TYPE_MAT33V,
			TYPE_MAT34V		= rage::psoType::TYPE_MAT34V,
			TYPE_MAT43		= rage::psoType::TYPE_MAT43,
			TYPE_MAT44V		= rage::psoType::TYPE_MAT44V,

			TYPE_STRING_MEMBER			= rage::psoType::TYPE_STRING_MEMBER,
			TYPE_STRING_POINTER32		= rage::psoType::TYPE_STRING_POINTER32,
			TYPE_STRING_POINTER64		= rage::psoType::TYPE_STRING_POINTER64,
			TYPE_ATSTRING32				= rage::psoType::TYPE_ATSTRING32,
			TYPE_ATSTRING64				= rage::psoType::TYPE_ATSTRING64,
			TYPE_WIDE_STRING_MEMBER		= rage::psoType::TYPE_WIDE_STRING_MEMBER,
			TYPE_WIDE_STRING_POINTER32	= rage::psoType::TYPE_WIDE_STRING_POINTER32,
			TYPE_WIDE_STRING_POINTER64	= rage::psoType::TYPE_WIDE_STRING_POINTER64,
			TYPE_ATWIDESTRING32			= rage::psoType::TYPE_ATWIDESTRING32,
			TYPE_ATWIDESTRING64			= rage::psoType::TYPE_ATWIDESTRING64,
			TYPE_STRINGHASH				= rage::psoType::TYPE_STRINGHASH,

			TYPE_ARRAY_MEMBER					= rage::psoType::TYPE_ARRAY_MEMBER,
			TYPE_ATARRAY32						= rage::psoType::TYPE_ATARRAY32,
			TYPE_ATARRAY64						= rage::psoType::TYPE_ATARRAY64,
			TYPE_ATFIXEDARRAY					= rage::psoType::TYPE_ATFIXEDARRAY,
			TYPE_ATARRAY32_32BITIDX				= rage::psoType::TYPE_ATARRAY32_32BITIDX,
			TYPE_ATARRAY64_32BITIDX				= rage::psoType::TYPE_ATARRAY64_32BITIDX,
			TYPE_ARRAY_POINTER32				= rage::psoType::TYPE_ARRAY_POINTER32,
			TYPE_ARRAY_POINTER64				= rage::psoType::TYPE_ARRAY_POINTER64,
			TYPE_ARRAY_POINTER32_WITH_COUNT		= rage::psoType::TYPE_ARRAY_POINTER32_WITH_COUNT,
			TYPE_ARRAY_POINTER64_WITH_COUNT		= rage::psoType::TYPE_ARRAY_POINTER64_WITH_COUNT,

			TYPE_ENUM8		= rage::psoType::TYPE_ENUM8,
			TYPE_ENUM16		= rage::psoType::TYPE_ENUM16,
			TYPE_ENUM32		= rage::psoType::TYPE_ENUM32,
			TYPE_BITSET8	= rage::psoType::TYPE_BITSET8,
			TYPE_BITSET16	= rage::psoType::TYPE_BITSET16,
			TYPE_BITSET32	= rage::psoType::TYPE_BITSET32,
			TYPE_ATBITSET32 = rage::psoType::TYPE_ATBITSET32,
			TYPE_ATBITSET64 = rage::psoType::TYPE_ATBITSET64,

			TYPE_ATBINARYMAP32 = rage::psoType::TYPE_ATBINARYMAP32,
			TYPE_ATBINARYMAP64 = rage::psoType::TYPE_ATBINARYMAP64,
		};


		public value struct MPsoType
		{
		public:
			MPsoType(ePsoType t) : m_Type(t) {}

			property size_t Size { size_t get() { return GetNative().GetSize();} }
			property size_t Align { size_t get() { return GetNative().GetAlign(); } } 

			property bool IsValid { bool get() { return GetNative().IsValid(); } }

			property bool IsPod		{ bool get() { return GetNative().IsPod(); } }
			property bool IsBitset	{ bool get() { return GetNative().IsBitset(); } }
			property bool IsMap		{ bool get() { return GetNative().IsMap(); } }
			property bool IsString	{ bool get() { return GetNative().IsString(); } }
			property bool IsArray	{ bool get() { return GetNative().IsArray(); } }
			property bool IsEnum	{ bool get() { return GetNative().IsEnum(); } }

			property bool HasSubStructure	{ bool get() { return GetNative().HasSubStructure(); } }
			property bool IsMapKey			{ bool get() { return GetNative().IsMapKey(); } }
			property bool IsNative			{ bool get() { return GetNative().IsNative(); } }
			property bool IsMemberType		{ bool get() { return GetNative().IsMemberType(); } }
			property bool IsDirectContainer	{ bool get() { return GetNative().IsDirectContainer(); } }
			property bool IsStructArrayType	{ bool get() { return GetNative().IsStructArrayType(); } }
			
			property bool IsNarrowString	{ bool get() { return GetNative().IsNarrowString(); } }
			property bool IsWideString		{ bool get() { return GetNative().IsWideString(); } }

			property bool IsCArray					{ bool get() { return GetNative().IsCArray(); } }
			property bool IsArrayWithExternalCount	{ bool get() { return GetNative().IsArrayWithExternalCount(); } }

			virtual System::String^ ToString() override;

			property ePsoType Enum { ePsoType get() { return m_Type; } } 
			static MPsoType FromNative(rage::psoType t) { return MPsoType(static_cast<ePsoType>(t.GetEnum())); }
		protected:
			rage::psoType GetNative() { return rage::psoType(static_cast<rage::psoType::Type>(m_Type)); }
			ePsoType m_Type;
		};

	}
}

#endif // MANAGEDRAGE_PARSER_PSOTYPES_H