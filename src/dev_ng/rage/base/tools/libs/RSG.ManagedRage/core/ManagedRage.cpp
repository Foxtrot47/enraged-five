//
// File: ManagedRageFile.h
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Main class library source file
//

// Managed includes
#include "ManagedRage.h"

// Rage includes
#include "parser/manager.h"
#include "system/param.h"

#using <mscorlib.dll>

// For MessageBoxA function used in RageCore
#pragma comment(lib, "user32.lib")

int Main()
{
	return (0);
}

namespace RSG
{

namespace ManagedRage
{

	// Now need to bloody initialise sysParam.
	void cRage::Init()
	{
		::rage::sysParam::Init(0, NULL);

		INIT_PARSER;
	}

} // ManagedRage namespace

} // RSG namespace
