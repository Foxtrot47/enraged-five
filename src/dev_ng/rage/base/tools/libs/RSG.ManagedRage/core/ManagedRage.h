#ifndef MANAGEDRAGE_H
#define MANAGEDRAGE_H

//
// File: ManagedRage.h
// Author: David Muir <david.muir@rockstarnorth.com>
// Description:
//

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#pragma managed
#endif

namespace RSG
{

namespace ManagedRage
{

	/**
	 * @brief Global Rage properties
	 */	
	public ref class cRage
	{
	public:
		static void Init();

		/** @brief Linked Rage version string */
		static System::String^ Version = RAGE_RELEASE_STRING;
		/** @brief Linked Rage release integer */
		static const int Release = RAGE_RELEASE;

		// Platform Definitions
		static const bool PLATFORM_CONSOLE = (bool)(__CONSOLE);
		static const bool PLATFORM_PS3	   = (bool)(__PS3);
		static const bool PLATFORM_PPU     = (bool)(__PPU);
		static const bool PLATFORM_SPU     = (bool)(__SPU);

		static const bool PLATFORM_WIN32   = (bool)(__WIN32);
		static const bool PLATFORM_OSX     = (bool)(__OSX);
		static const bool PLATFORM_POSIX   = (bool)(__POSIX);
		static const bool PLATFORM_XENON   = (bool)(__XENON);
		static const bool PLATFORM_LINUX   = (bool)(__LINUX);
		static const bool PLATFORM_WIN32PC = (bool)(__WIN32PC);

	};

} // ManagedRage namespace

} // RSN namespace

#endif // MANAGEDRAGE_H

// ManagedRage.h
