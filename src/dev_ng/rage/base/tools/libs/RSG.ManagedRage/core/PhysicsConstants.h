#pragma once

#include "phbullet/CollisionMargin.h"

namespace RSG
{
	namespace ManagedRage
	{
		public ref class PhysicsConstants
		{
		public:
			static property float ConvexDistanceMargin
			{
				float get() { return CONVEX_DISTANCE_MARGIN; }
			}
			static property float ConcaveDistanceMargin 
			{
				float get() { return CONCAVE_DISTANCE_MARGIN; }
			}
		};
	}
}
