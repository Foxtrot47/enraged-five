#ifndef MANAGEDRAGE_TYPES_H
#define MANAGEDRAGE_TYPES_H

//
// File: Types.h
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Common types
//

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

namespace RSG
{

namespace ManagedRage
{

// Signed Integer Datatypes
typedef __int8  s8;
typedef __int16 s16;
typedef __int32 s32;
typedef __int64 s64;

// Unsigned Integer Datatypes
typedef unsigned __int8  u8;
typedef unsigned __int16 u16;
typedef unsigned __int32 u32;
typedef unsigned __int64 u64;


} // ManagedRage namespace

} // RSN namespace

#endif // MANAGEDRAGE_TYPES_H

// End of file
