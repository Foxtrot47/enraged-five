#ifndef MANAGEDRAGE_ASCII_TOKENIZER_H
#define MANAGEDRAGE_ASCII_TOKENIZER_H

//
// File: Token.h
// Author: Luke Openshaw <luke.openshaw@rockstarnorth.com>
// Description: Managed wrapper around the rage tokenizer
//

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma managed
#pragma once
#endif

#using <System.dll>

#include "file/token.h"
//#include "../vector.h"
#include "basetoken.h"

namespace RSG
{

namespace ManagedRage
{

namespace File
{
	///
	///
	///
	public ref class AsciiTokenizer : BaseTokenizer
	{
	internal:
		#pragma region Con/Destructors
		///
		/// Use TokenUtility.OpenTokenizedFile and TokenUtility.CreateTokenizedFile to instatiate these.
		///
		AsciiTokenizer();
		
		///
		///
		///
		~AsciiTokenizer();
		#pragma endregion
	public:
		#pragma region Public Methods
		///
		///
		///
		virtual void Save() override;

		///
		///
		///
		virtual bool IsBinary() override { return false; }

		///
		///
		///
		virtual System::String^ Filename();

		///
		///
		///
		virtual System::Boolean^ PutDelimiter( System::String^ src ) override;

	internal:
		///
		///
		///
		virtual void Init( System::String^ filename, System::Boolean create ) override;
		
	private:
		::rage::fiAsciiTokenizer* m_Tokenizer;
	};
}

}

}

#endif // MANAGEDRAGE_ASCII_TOKENIZER_H