// Managed includes
#include "asciitoken.h"
#include "../core/ManagedRage.h"

#include "file/asset.h"
#include "vector/vector2.h"
#include "vector/vector3.h"
#include "vector/vector4.h"
#include "system/param.h"

namespace RSG
{

namespace ManagedRage
{

namespace File
{


#pragma region Con/Destructors
///
///
///
AsciiTokenizer::AsciiTokenizer()
	: BaseTokenizer()
{
	this->m_Tokenizer = new ::rage::fiAsciiTokenizer();
	this->m_BaseTokenizer = this->m_Tokenizer;
}
		
///
///
///
AsciiTokenizer::~AsciiTokenizer()
{
	if (m_TuningStream)
		m_TuningStream->Close();

	if(m_Tokenizer)
		delete m_Tokenizer;
}
#pragma endregion

#pragma region Public Methods
///
///
///
void AsciiTokenizer::Init( System::String^ filename, System::Boolean create )
{
	BaseTokenizer::Init();
	System::String^ basename = System::IO::Path::Combine(System::IO::Path::GetDirectoryName(filename), System::IO::Path::GetFileNameWithoutExtension(filename));
	System::String^ extension = System::IO::Path::GetExtension(filename)->Replace(".", "");

	char* strFilename = (char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(basename);
	char* strExtension = (char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(extension);
	
	if( create )
	{
		m_TuningStream = rage::ASSET.Create( strFilename, strExtension );
	}
	else
	{
		m_TuningStream = rage::ASSET.Open( strFilename, strExtension, true, true );
	}
	m_Tokenizer->Init( strFilename, m_TuningStream );

	System::Runtime::InteropServices::Marshal::FreeHGlobal((System::IntPtr)strFilename);
	System::Runtime::InteropServices::Marshal::FreeHGlobal((System::IntPtr)strExtension);
}

void AsciiTokenizer::Save()
{
	m_TuningStream->Close();
}

System::String^ AsciiTokenizer::Filename()
{
	if(m_Tokenizer)
		return gcnew System::String(m_Tokenizer->filename);

	return nullptr;
}

///
///
///
System::Boolean^ AsciiTokenizer::PutDelimiter( System::String^ src )
{
	char* str = (char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(src);
	System::Boolean^ val = this->m_BaseTokenizer->PutDelimiter( str );
	System::Runtime::InteropServices::Marshal::FreeHGlobal((System::IntPtr)str);
	return ( val );
}

#pragma endregion 

} // File

} // ManagedRage

} // RSG