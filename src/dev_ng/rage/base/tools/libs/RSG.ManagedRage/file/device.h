#ifndef MANAGEDRAGE_DEVICE_H
#define MANAGEDRAGE_DEVICE_H

//
// File: cDevice.h
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of cDevice.h class
//

// ManagedRage Library Header Files
#include "../Types.h"

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma managed
#endif

namespace RSG
{

namespace ManagedRage
{

namespace file
{

public enum class etSeekWhence sealed
{ 
	kSeekSet, //!< Position is relative to start of file
	kSeekCur, //!< Position is relative to current file position (can be negative)
	kSeekEnd  //!< Position is relative to end of file. Reliable operation is only guaranteed if pos is zero or negative
};

public ref class tFindData sealed
{
public:
	System::String^		sName;			//!< Name of the file
	u64					nSize;			//!< Size of the file
	System::DateTime^	LastWriteTime;	//!< Last time the file was written
	u32					Attributes;		//!< Bitfield using the FILE_ATTRIBUTE_... values
};

public ref class cFileAttributes sealed
{
public:
	static const u32 READONLY            = 0x00000001;
	static const u32 HIDDEN              = 0x00000002;
	static const u32 SYSTEM              = 0x00000004;
	static const u32 DIRECTORY           = 0x00000010;
	static const u32 ARCHIVE			 = 0x00000020;
	static const u32 NORMAL              = 0x00000080;
	static const u32 TEMPORARY           = 0x00000100;
	static const u32 SPARSE_FILE         = 0x00000200;
	static const u32 REPARSE_POINT       = 0x00000400;
	static const u32 COMPRESSED          = 0x00000800;
	static const u32 OFFLINE             = 0x00001000;
	static const u32 NOT_CONTENT_INDEXED = 0x00002000;
	static const u32 INVALID			 = ~0U;
};

} // file namespace

} // ManagedRage namespace

} // RSN namespace

#endif // MANAGEDRAGE_DEVICE_H

// cDevice.h
