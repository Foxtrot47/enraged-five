
// RAGE headers
#include "file/packfile.h"
using namespace System::Diagnostics;

namespace RSG
{
namespace ManagedRage
{

ref class PackEntry;

public ref class Packfile
{
public:
	enum class etPlatform
	{
		Unknown,
		Win32,
		Xenon,
		PS3,
		Win64,
		Durango,
		Orbis
	};

	Packfile( );
	~Packfile( );

	bool Load( System::String^ filename );
	bool Load( System::IO::Stream^ stream );
	bool Close( );
	bool IsInitialised( ) { return m_pPackfile->IsInitialized(); }
	
	PackEntry^ FindEntry( System::String^ filename);
	PackEntry^ FindEntry( System::String^ filename, bool ignoreCase );
	array<unsigned char>^ Extract( System::String^ filename );

	property System::UInt32 EncryptionKey
	{
		System::UInt32 get() 
		{ 
			if (nullptr != m_pPackfile)
				return (m_pPackfile->GetEncryptionKey()); 
			else
				return (0);
		}
	}
	property array<PackEntry^>^ Entries
	{
		array<PackEntry^>^ get() { return m_entries; }
	}
	property etPlatform Platform 
	{
		etPlatform get() { return m_ePlatform; }
	}
	property size_t VirtualLeafSize
	{
		size_t get() { return m_virtualLeafSize; }
	}
	property size_t PhysicalLeafSize
	{
		size_t get() { return m_physicalLeafSize; }
	}
private:
	void Initialise( );
	void ProcessEntry( const ::rage::fiPackEntry* pEntry, const ::rage::fiPackEntry* pEntries, System::Collections::Generic::List<PackEntry^>^ entries, System::String^ prefix, int index );
	void SetupLeafSizes( );

	::rage::fiPackfile*			m_pPackfile;
	etPlatform			m_ePlatform;
	size_t						m_virtualLeafSize;
	size_t						m_physicalLeafSize;
	int							m_NameShift;
	array<PackEntry^>^			m_entries;
};

public ref class PackEntry
{
public:
	PackEntry( Packfile^ parent, const char* nameheap, int nameShift, const ::rage::fiPackEntry* pEntry, System::String^ path );
	~PackEntry( );

	property System::String^ Name
	{
		System::String^ get() { return m_sName; }
	} 
	property System::String^ Path
	{
		System::String^ get() { return m_sPath; }
	}
	property bool IsFile
	{
		bool get() { return ( m_pEntry->IsFile() ); }
	}
	property bool IsDirectory
	{
		bool get() { return ( m_pEntry->IsDir() ); }
	}
	property bool IsCompressed
	{
		bool get() { return ( m_pEntry->IsCompressed() ); }
	}
	property bool IsResource
	{
		bool get() { return ( m_pEntry->IsResource() ); }
	}
	property unsigned int ConsumedSize
	{
		unsigned int get() { return ( m_pEntry->m_ConsumedSize ); }
	}
	property unsigned int Index
	{
		unsigned int get() { return ( m_index ); }
		void set(unsigned int index) { m_index = index; }
	}
	property unsigned int UncompressedSize
	{
		unsigned int get() 
		{
			if ( IsResource )
			{
				// Determine the uncompressed size from the entry datResourceInfo 
				// structure; using leaf sizes to determine the actual size.
				return ( this->VirtualSize + this->PhysicalSize );
			}
			else if ( IsFile )
				return ( m_pEntry->u.file.m_UncompressedSize );
			else
				return ( (unsigned int)-1 );
		}
	}
	property unsigned int FileOffset
	{
		unsigned int get() { return ( m_offset ); }
	}

	// Resource-only parameters.
	property int Version
	{
		int get() 
		{
			if ( IsResource )
				return ( m_pEntry->u.resource.m_Info.GetVersion() );
			else
				return ( 0 );
		}
	}
	property size_t VirtualSize
	{
		size_t get()
		{
			if (IsResource)
				return ( m_pEntry->u.resource.m_Info.Virtual.GetSize( (::rage::u32)(m_parent->VirtualLeafSize) ) );
			else
				return ( 0 );
		}
	}
	property size_t VirtualChunkSize
	{
		size_t get()
		{
			if (IsResource)
				return m_parent->VirtualLeafSize << m_pEntry->u.resource.m_Info.Virtual.LeafShift;
			else
				return ( 0 );
		}
	}
	property size_t VirtualChunkCount
	{
		size_t get()
		{
			if (IsResource)
				return (m_pEntry->u.resource.m_Info.GetVirtualChunkCount());
			else
				return ( 0 );
		}
	}
	property size_t PhysicalSize
	{
		size_t get()
		{
			if (IsResource)
				return (m_pEntry->u.resource.m_Info.Physical.GetSize( (::rage::u32)m_parent->PhysicalLeafSize ) );
			else
				return ( 0 );
		}
	}
	property size_t PhysicalChunkSize
	{
		size_t get()
		{
			if (IsResource)
				return m_parent->PhysicalLeafSize << m_pEntry->u.resource.m_Info.Physical.LeafShift;
			else
				return ( 0 );
		}
	}
	property size_t PhysicalChunkCount
	{
		size_t get()
		{
			if (IsResource)
				return (m_pEntry->u.resource.m_Info.GetPhysicalChunkCount());
			else
				return ( 0 );
		}
	}
		
private:
	System::String^				m_sName;
	System::String^				m_sPath;
	const ::rage::fiPackEntry*	m_pEntry;
	Packfile^					m_parent;
	int							m_index;
	unsigned int				m_offset; // adjusted byte offset in RPF.
};

public ref struct PackfileException : public System::Exception
{
};

} // ManagedRage namespace
} // RSG namespace
