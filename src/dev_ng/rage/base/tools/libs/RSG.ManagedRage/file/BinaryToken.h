#ifndef MANAGEDRAGE_BINARY_TOKENIZER_H
#define MANAGEDRAGE_BINARY_TOKENIZER_H

//
// File: BinaryToken.h
// Author: Luke Openshaw <luke.openshaw@rockstarnorth.com>
// Description: Managed wrapper around the rage tokenizer
//

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma managed
#pragma once
#endif

#using <System.dll>

//#include "file/token.h"
#include "../vector.h"
#include "basetoken.h"

namespace RSG
{

namespace ManagedRage
{

namespace File
{
	///
	///
	///
	public ref class BinTokenizer : BaseTokenizer
	{
	internal:
		#pragma region Con/Destructors
		///
		/// Use TokenUtility.OpenTokenizedFile and TokenUtility.CreateTokenizedFile to instatiate these.
		///
		BinTokenizer();
		
		///
		///
		///
		~BinTokenizer();
		#pragma endregion
	public:
		#pragma region Public Methods
		///
		///
		///
		virtual void Save() override;

		///
		///
		///
		virtual bool IsBinary() override { return true; }

		///
		///
		///
		virtual void GetDelimiter( System::String^ de ) override { }

		///
		///
		///
		virtual System::Boolean^ PutDelimiter( System::String^ src ) override { return true; }

	internal:
		///
		///
		///
		virtual void Init( System::String^ filename, System::Boolean create ) override;

		#pragma endregion
	private:
		::rage::fiBinTokenizer* m_Tokenizer;
	};
}

}

}

#endif // MANAGEDRAGE_BINARY_TOKENIZER_H