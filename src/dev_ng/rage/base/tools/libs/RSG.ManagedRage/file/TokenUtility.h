#ifndef MANAGEDRAGE_TOKEN_UTLITY_H
#define MANAGEDRAGE_TOKEN_UTLITY_H

//
// File: Token.h
// Author: Luke Openshaw <luke.openshaw@rockstarnorth.com>
// Description: Managed wrapper around the rage tokenizer
//

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma managed
#pragma once
#endif

#using <System.dll>

#include "BaseToken.h"
#include "AsciiToken.h"
#include "BinaryToken.h"

namespace RSG
{

namespace ManagedRage
{

namespace File
{
	public enum class TokenizerType
	{
		Ascii,
		Binary
	};
	///
	///
	///
	public ref class TokenUtility abstract sealed
	{
	public:
		
		///
		///
		///
		static BaseTokenizer^ OpenTokenizedFile(System::String^ filename);

		///
		///
		///
		static BaseTokenizer^ CreateTokenizedFile(System::String^ filename, TokenizerType type);
	};
}

}

}

#endif // MANAGEDRAGE_TOKEN_UTLITY_H