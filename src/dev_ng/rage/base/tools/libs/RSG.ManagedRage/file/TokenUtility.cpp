#include "TokenUtility.h"
#include "file/token.h"
#include "file/asset.h"

namespace RSG
{

namespace ManagedRage
{

namespace File
{

BaseTokenizer^ TokenUtility::OpenTokenizedFile(System::String^ filename)
{
	BaseTokenizer^ tok;

	System::String^ basename = System::IO::Path::Combine(System::IO::Path::GetDirectoryName(filename), System::IO::Path::GetFileNameWithoutExtension(filename));
	System::String^ extension = System::IO::Path::GetExtension(filename)->Replace(".", "");
	
	char* strFilename = (char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(basename);
	char* strExtension = (char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(extension);
	
	// Have a wee peek at the first byte to see if it's a binary or ascii file
	::rage::fiStream* stream = rage::ASSET.Open( strFilename, strExtension, true, true );
	int c = stream->GetCh();	
	if(c == 26)
	{
		tok = gcnew BinTokenizer();
	}
	else
	{
		tok = gcnew AsciiTokenizer();
	}
	stream->Close();

	tok->Init(filename, false);

	System::Runtime::InteropServices::Marshal::FreeHGlobal((System::IntPtr)strFilename);
	System::Runtime::InteropServices::Marshal::FreeHGlobal((System::IntPtr)strExtension);

	return tok;
}

BaseTokenizer^ TokenUtility::CreateTokenizedFile(System::String^ filename, TokenizerType type)
{
	BaseTokenizer^ tok;
	if(type == TokenizerType::Binary)
	{
		tok = gcnew BinTokenizer();
	}
	else
	{
		tok = gcnew AsciiTokenizer();
	}

	tok->Init(filename, true);
	return tok;
}

}

}

}