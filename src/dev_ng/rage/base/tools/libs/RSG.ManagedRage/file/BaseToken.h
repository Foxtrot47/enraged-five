#ifndef MANAGEDRAGE_BASE_TOKENIZER_H
#define MANAGEDRAGE_BASE_TOKENIZER_H

//
// File: TokenBase.h
// Author: Luke Openshaw <luke.openshaw@rockstarnorth.com>
// Description: Managed wrapper around the rage tokenizer
//

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma managed
#pragma once
#endif

#using <System.dll>

#include "file/token.h"
#include "../vector.h"

namespace RSG
{

namespace ManagedRage
{

namespace File
{
	///
	///
	///
	public ref class BaseTokenizer abstract
	{
	public:
		#pragma region Con/Destructors
		///
		///
		///
		BaseTokenizer();
		
		///
		///
		///
		~BaseTokenizer();
		#pragma endregion

		#pragma region Public Methods

		///
		///
		///
		virtual void Init( );

		

		///
		///
		///
		virtual void Save() abstract;

		///
		///
		///
		virtual bool IsBinary() abstract;

		// read functions
		virtual System::Int32								GetByte( const bool error )				{ return System::Int32( this->m_BaseTokenizer->GetByte( error ) ); }
		virtual System::Int32								GetShort( const bool error )				{ return System::Int32( this->m_BaseTokenizer->GetShort( error ) ); }
		virtual System::Int32								GetInt( const bool error )					{ return System::Int32( this->m_BaseTokenizer->GetInt( error ) ); }
		virtual System::Int32								GetHexU32( const bool error )				{ return System::Int32( static_cast<rage::fiAsciiTokenizer*>(this->m_BaseTokenizer)->GetHexU32( error ) ); }
		virtual System::Single								GetFloat( const bool error )				{ return ( this->m_BaseTokenizer->GetFloat( error ) ); }

		virtual Math::Vector2^								GetVector2( System::Boolean^ error ); 
		virtual Math::Vector3^								GetVector3( System::Boolean^ error ); 
		virtual Math::Vector4^								GetVector4( System::Boolean^ error ); 
		virtual void										GetDelimiter(System::String^ de); 

		virtual System::Int32								MatchInt( System::String^ match ); 
		virtual System::Single								MatchFloat( System::String^ match ); 
		virtual Math::Vector2^								MatchVector2( System::String^ match ); 
		virtual Math::Vector3^								MatchVector3( System::String^ match ); 
		virtual Math::Vector4^								MatchVector4( System::String^ match ); 

		virtual System::Int32								MatchIInt( System::String^ match ); 
		virtual System::Single								MatchIFloat( System::String^ match ); 
		virtual Math::Vector2^								MatchIVector2( System::String^ match ); 
		virtual Math::Vector3^								MatchIVector3( System::String^ match ); 
		virtual Math::Vector4^								MatchIVector4( System::String^ match ); 

		// Formatting functions
		virtual void										StartBlock()							{ this->m_BaseTokenizer->StartBlock(); }
		virtual void										EndBlock() 								{ this->m_BaseTokenizer->EndBlock(); }
		virtual void										StartLine() 							{ this->m_BaseTokenizer->StartLine(); }
		virtual void										EndLine() 								{ this->m_BaseTokenizer->EndLine(); }
		virtual void										Indent( System::Int32^ i ) 				{ this->m_BaseTokenizer->Indent( (int)i ); }

		// Write functions
		virtual System::Boolean^							Put( System::String^ src, System::Int32^ tabs ); 
		virtual System::Boolean^							PutDelimiter( System::String^ src )=0;
		virtual System::Boolean^							PutByte( System::Int32 i )				{ return this->m_BaseTokenizer->PutByte( (int)i ); }
		virtual System::Boolean^							PutShort( System::Int32 i )				{ return this->m_BaseTokenizer->PutShort( (int)i ); }
		virtual System::Boolean^							Put( System::Int32 i )					{ return this->m_BaseTokenizer->Put( (int)i ); }
		virtual System::Boolean^							Put( System::UInt32 i )					{ return this->m_BaseTokenizer->Put( (int)i ); }
		virtual System::Boolean^							Put( System::Single i )					{ return this->m_BaseTokenizer->Put( (float)i ); }
		virtual System::Boolean^							Put( Math::Vector2^ v ); 
		virtual System::Boolean^							Put( Math::Vector3^ v ); 
		virtual System::Boolean^							Put( Math::Vector4^ v ); 

		//System::Boolean^							Put(const ScalarV& v);
		//System::Boolean^							PutV( ClipAnimation::Vector2^ v);
		//System::Boolean^							PutV( ClipAnimation::Vector3^ v );
		//System::Boolean^							PutV( ClipAnimation::Vector4^ v );
		//System::Boolean^							Put(const Mat33V& v);
		//System::Boolean^							Put(const Mat34V& v);

		// Base functions
		virtual System::Int32								GetToken( System::String^% dest, System::Int32 maxLen );
		virtual void										MatchToken(System::String^ match);
		virtual System::Boolean								CheckToken( System::String^ check, System::Boolean^ consume );
		virtual System::Boolean								CheckIToken( System::String^ check, System::Boolean^ consume );
		virtual System::String^								GetName();
		virtual System::String^								GetFilename();
		virtual void										IgnoreToken();
		#pragma endregion
		::rage::fiStream* m_TuningStream;
	internal:
		///
		///
		///
		virtual void Init( System::String^ filename, System::Boolean create ) abstract;
	protected:
		::rage::fiBaseTokenizer* m_BaseTokenizer;
		

		static System::Boolean m_Initialised = false;
	};

}

}

}

#endif // MANAGEDRAGE_BASE_TOKENIZER_H