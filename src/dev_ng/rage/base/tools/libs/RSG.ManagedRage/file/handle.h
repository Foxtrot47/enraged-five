#ifndef MANAGEDRAGE_HANDLE_H
#define MANAGEDRAGE_HANDLE_H

//
// File: tHandle.h
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Managed wrapper around the rage::fiHandle type
//

#if defined(_MSC_VER)
#pragma once
#endif

// Rage Engine Header Files
#include "file/handle.h"

#if defined(_MSC_VER)
#pragma managed
#endif

namespace RSG
{

namespace ManagedRage
{

namespace file
{

public ref class tHandle
{
public:
	static tHandle^ tHandleInvalid = gcnew tHandle();

	/**
	 * @brief Convert from a Rage ::fiHandle to a ManagedRage::file::tHandle
	 */
	static tHandle^ FromRageHandle(::fiHandle hHandle);

	tHandle()
		: m_hHandle(System::IntPtr::Zero)
	{
	}

	tHandle(System::IntPtr hHandle)
		: m_hHandle(hHandle)
	{
	}

	System::IntPtr^ RetHandle()
	{
		return m_hHandle;
	}

	::fiHandle ToRageHandle()
	{
		return (m_hHandle.ToPointer());
	}

	bool IsValid()
	{
		return (m_hHandle.ToInt32() > -1);
	}

protected:
	System::IntPtr m_hHandle;
};

} // file namespace

} // ManagedRage namespace

} // RSN namespace

#endif // MANAGEDRAGE_HANDLE_H

// tHandle.h
