//
// File: tHandle.cpp
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of tHandle.cpp class
//

#if defined(_MSC_VER)
#pragma once
#endif

#include "handle.h"

#if defined(_MSC_VER)
#pragma managed
#endif

namespace RSG
{

namespace ManagedRage
{

namespace file
{

tHandle^ 
tHandle::FromRageHandle(::fiHandle hHandle)
{
	return gcnew tHandle(static_cast<System::IntPtr>(hHandle));
}

} // file namespace

} // ManagedRage namespace

} // RSN namespace

// tHandle.cpp
