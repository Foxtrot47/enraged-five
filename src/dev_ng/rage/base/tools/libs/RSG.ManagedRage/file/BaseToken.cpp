#include "basetoken.h"
#include "../core/ManagedRage.h"
#include "vector/vector2.h"
#include "vector/vector3.h"
#include "vector/vector4.h"

namespace RSG
{

namespace ManagedRage
{

namespace File
{

#pragma region Con/Destructors
///
///
///
BaseTokenizer::BaseTokenizer()
{

}
		
///
///
///
BaseTokenizer::~BaseTokenizer()
{
	
}
#pragma endregion

#pragma region Public Functions
#pragma unmanaged
// Unmanaged marshalling functions,  refactor and move.

///
///
///
void NGetVector3(rage::fiBaseTokenizer* tokenizer, float* out, bool error)
{
	rage::Vector3 vec;
	tokenizer->GetVector( vec, error );
	out[0] = vec[0];
	out[1] = vec[1];
	out[2] = vec[2];
}

///
///
///
void NGetVector4(rage::fiBaseTokenizer* tokenizer, float* out, bool error)
{
	rage::Vector4 vec;
	tokenizer->GetVector( vec, error );
	out[0] = vec[0];
	out[1] = vec[1];
	out[2] = vec[2];
	out[3] = vec[3];
}

///
///
///
bool NPutVec2( rage::fiBaseTokenizer* tokenizer, float* in )
{
	rage::Vector2 vec;
	vec[0] = in[0];
	vec[1] = in[1];
	return ( tokenizer->Put( vec ) );
}

///
///
///
bool NPutVec3( rage::fiBaseTokenizer* tokenizer, float* in )
{
	rage::Vector3 vec;
	vec[0] = in[0];
	vec[1] = in[1];
	vec[2] = in[2];
	return ( tokenizer->Put( vec ) );
}

///
///
///
bool NPutVec4( rage::fiBaseTokenizer* tokenizer, float* in )
{
	rage::Vector4 vec;
	vec[0] = in[0];
	vec[1] = in[1];
	vec[2] = in[2];
	vec[3] = in[3];
	return ( tokenizer->Put( vec ) );
}

///
///
///
void NMatchVector2( rage::fiBaseTokenizer* tokenizer, char* str, float* out )
{
	rage::Vector2 vec; 
	tokenizer->MatchVector( str, vec );
	out[0] = vec[0];
	out[1] = vec[1];
}

///
///
///
void NMatchVector3( rage::fiBaseTokenizer* tokenizer, char* str, float* out )
{
	rage::Vector3 vec; 
	tokenizer->MatchVector( str, vec );
	out[0] = vec[0];
	out[1] = vec[1];
	out[2] = vec[2];
}

///
///
///
void NMatchVector4( rage::fiBaseTokenizer* tokenizer, char* str, float* out )
{
	rage::Vector4 vec; 
	tokenizer->MatchVector( str, vec );
	out[0] = vec[0];
	out[1] = vec[1];
	out[2] = vec[2];
	out[3] = vec[3];
}

///
///
///
void NMatchIVector2( rage::fiBaseTokenizer* tokenizer, char* str, float* out )
{
	rage::Vector2 vec; 
	tokenizer->MatchIVector( str, vec );
	out[0] = vec[0];
	out[1] = vec[1];
}

///
///
///
void NMatchIVector3( rage::fiBaseTokenizer* tokenizer, char* str, float* out )
{
	rage::Vector3 vec; 
	tokenizer->MatchIVector( str, vec );
	out[0] = vec[0];
	out[1] = vec[1];
	out[2] = vec[2];
}

///
///
///
void NMatchIVector4( rage::fiBaseTokenizer* tokenizer, char* str, float* out )
{
	rage::Vector4 vec; 
	tokenizer->MatchIVector( str, vec );
	out[0] = vec[0];
	out[1] = vec[1];
	out[2] = vec[2];
	out[3] = vec[3];
}
#pragma managed


///
///
///
void BaseTokenizer::Init()
{
	if(!m_Initialised)
	{
		RSG::ManagedRage::cRage::Init();
		m_Initialised = true;
	}
}

///
///
///
Math::Vector2^	BaseTokenizer::GetVector2(  System::Boolean^ error )
{
	rage::Vector2 vec;
	this->m_BaseTokenizer->GetVector( vec, (bool)error );
	return ( gcnew Math::Vector2( vec[0], vec[1] ) ); 
}

///
///
///
Math::Vector3^	BaseTokenizer::GetVector3( System::Boolean^ error )
{
	float vec[3];
	NGetVector3(this->m_BaseTokenizer, vec, (bool)error);

	return ( gcnew Math::Vector3( vec[0], vec[1], vec[2] ) ); 
}


///
///
///
Math::Vector4^	BaseTokenizer::GetVector4( System::Boolean^ error )
{
	float vec[4];
	NGetVector4(this->m_BaseTokenizer, vec, (bool)error);

	return ( gcnew Math::Vector4( vec[0], vec[1], vec[2], vec[3] ) ); 
}

///
///
///
void BaseTokenizer::GetDelimiter(System::String^ de)
{
	this->MatchToken(de);
}

///
///
///
System::Int32 BaseTokenizer::MatchInt( System::String^ match )
{
	char* str = (char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(match);
	System::Int32 val = System::Int32(this->m_BaseTokenizer->MatchInt(str));
	System::Runtime::InteropServices::Marshal::FreeHGlobal((System::IntPtr)str);

	return (val);
}

///
///
///
System::Single BaseTokenizer::MatchFloat( System::String^ match )
{
	char* str = (char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(match);
	System::Single val = System::Single(this->m_BaseTokenizer->MatchFloat(str));
	System::Runtime::InteropServices::Marshal::FreeHGlobal((System::IntPtr)str);

	return (val);
}

///
///
///
Math::Vector2^ BaseTokenizer::MatchVector2( System::String^ match )
{
	char* str = (char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(match);
	float vec[2];
	NMatchVector2( this->m_BaseTokenizer, str, vec );
	Math::Vector2^ val = gcnew Math::Vector2( vec[0], vec[1] );
	System::Runtime::InteropServices::Marshal::FreeHGlobal((System::IntPtr)str);

	return (val);
}

///
///
///
Math::Vector3^ BaseTokenizer::MatchVector3( System::String^ match )
{
	char* str = (char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(match);
	float vec[3];
	NMatchVector2( this->m_BaseTokenizer, str, vec );
	Math::Vector3^ val = gcnew Math::Vector3( vec[0], vec[1], vec[2] );
	System::Runtime::InteropServices::Marshal::FreeHGlobal((System::IntPtr)str);

	return (val);
}

///
///
///
Math::Vector4^ BaseTokenizer::MatchVector4( System::String^ match )
{
	char* str = (char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(match);
	float vec[4];
	NMatchVector2( this->m_BaseTokenizer, str, vec );
	Math::Vector4^ val = gcnew Math::Vector4( vec[0], vec[1], vec[2], vec[3] );
	System::Runtime::InteropServices::Marshal::FreeHGlobal((System::IntPtr)str);

	return (val);
}

///
///
///
System::Int32 BaseTokenizer::MatchIInt( System::String^ match )
{
	char* str = (char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(match);
	System::Int32 val = System::Int32(this->m_BaseTokenizer->MatchIInt(str));
	System::Runtime::InteropServices::Marshal::FreeHGlobal((System::IntPtr)str);

	return (val);
}

///
///
///
System::Single BaseTokenizer::MatchIFloat( System::String^ match )
{
	char* str = (char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(match);
	System::Single val = System::Single(this->m_BaseTokenizer->MatchIFloat(str));
	System::Runtime::InteropServices::Marshal::FreeHGlobal((System::IntPtr)str);

	return (val);
}

///
///
///
Math::Vector2^ BaseTokenizer::MatchIVector2( System::String^ match )
{
	char* str = (char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(match);
	float vec[2];
	NMatchIVector2( this->m_BaseTokenizer, str, vec );
	Math::Vector2^ val = gcnew Math::Vector2( vec[0], vec[1] );
	System::Runtime::InteropServices::Marshal::FreeHGlobal((System::IntPtr)str);

	return (val);
}

///
///
///
Math::Vector3^ BaseTokenizer::MatchIVector3( System::String^ match )
{
	char* str = (char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(match);
	float vec[3];
	NMatchIVector3( this->m_BaseTokenizer, str, vec );
	Math::Vector3^ val = gcnew Math::Vector3( vec[0], vec[1], vec[2] );
	System::Runtime::InteropServices::Marshal::FreeHGlobal((System::IntPtr)str);

	return (val);
}

///
///
///
Math::Vector4^ BaseTokenizer::MatchIVector4( System::String^ match )
{
	char* str = (char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(match);
	float vec[4];
	NMatchIVector4( this->m_BaseTokenizer, str, vec );
	Math::Vector4^ val = gcnew Math::Vector4( vec[0], vec[1], vec[2], vec[3] );
	System::Runtime::InteropServices::Marshal::FreeHGlobal((System::IntPtr)str);

	return (val);
}

///
///
///
System::Boolean^ BaseTokenizer::Put( System::String^ src, System::Int32^ tabs )
{
	char* str = (char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(src);
	System::Boolean^ val = this->m_BaseTokenizer->Put( str, (int)tabs );
	System::Runtime::InteropServices::Marshal::FreeHGlobal((System::IntPtr)str);
	return ( val );
}

///
///
///
System::Boolean^ BaseTokenizer::Put( Math::Vector2^ v)
{
	float vec[2];
	vec[0] = (float)v->x;
	vec[1] = (float)v->y;

	return ( NPutVec2( this->m_BaseTokenizer, vec ) );
}

///
///
///
System::Boolean^ BaseTokenizer::Put( Math::Vector3^ v )
{
	float vec[3];
	vec[0] = (float)v->x;
	vec[1] = (float)v->y;
	vec[2] = (float)v->z;

	return ( NPutVec3( this->m_BaseTokenizer, vec ) );
}

///
///
///
System::Boolean^ BaseTokenizer::Put( Math::Vector4^ v )
{
	float vec[4];
	vec[0] = (float)v->x;
	vec[1] = (float)v->y;
	vec[2] = (float)v->z;
	vec[3] = (float)v->w;

	return ( NPutVec4( this->m_BaseTokenizer, vec ) );
}

///
///
///
System::Int32 BaseTokenizer::GetToken( System::String^% dest, System::Int32 maxLen )
{
	char tok[512];
	System::Int32 ret = this->m_BaseTokenizer->GetToken( tok, (int)maxLen );
	dest = gcnew System::String(tok);
	return ( ret );
}

///
///
///
void BaseTokenizer::MatchToken(System::String^ match)
{
	char* str = (char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(match);
	this->m_BaseTokenizer->MatchToken(str);
	System::Runtime::InteropServices::Marshal::FreeHGlobal((System::IntPtr)str);
}

///
///
///
System::Boolean BaseTokenizer::CheckToken( System::String^ check, System::Boolean^ consume)
{
	char* str = (char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(check);
	System::Boolean val = this->m_BaseTokenizer->CheckToken(str, (bool)consume);
	System::Runtime::InteropServices::Marshal::FreeHGlobal((System::IntPtr)str);

	return ( val );
}

System::Boolean BaseTokenizer::CheckIToken( System::String^ check, System::Boolean^ consume )
{
	char* str = (char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(check);
	System::Boolean val = this->m_BaseTokenizer->CheckIToken(str, (bool)consume);
	System::Runtime::InteropServices::Marshal::FreeHGlobal((System::IntPtr)str);

	return ( val );
}

///
///
///
System::String^ BaseTokenizer::GetName()
{
	return ( gcnew System::String( this->m_BaseTokenizer->GetName() ) );
}

///
///
///
System::String^ BaseTokenizer::GetFilename()
{
	return ( gcnew System::String( this->m_BaseTokenizer->filename ) );
}

///
///
///
void BaseTokenizer::IgnoreToken()
{
	System::String^ buff = gcnew System::String("");
	this->GetToken(buff, 1);
}
#pragma endregion






}

}

}
