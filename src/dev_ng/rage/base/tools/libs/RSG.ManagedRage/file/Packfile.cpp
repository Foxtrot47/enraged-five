
#include "Packfile.h"
#include "../MarshallingUtils.h"

// RAGE headers
#include "data/resourceheader.h"
#include "file/device.h"
#include "file/packfile.h"

using namespace System::Diagnostics;

namespace RSG
{
namespace ManagedRage
{

Packfile::Packfile( )
	: m_pPackfile( NULL )
{
}

Packfile::~Packfile( )
{
	if ( m_pPackfile )
		Close( );
}

bool
Packfile::Load( System::IO::Stream^ stream )
{	
	using namespace System::IO;

	// Ensure the RPF system doesn't output TTY.
	bool init = false;
	bool outputEnabled = ::rage::diagChannel::GetOutput();
	::rage::diagChannel::SetOutput(false);

	try
	{
		pin_ptr<const char> ansiFilename = "MEMORYSTREAM";

		// Allocate memory stream to read into and initialise from that
		// after its location is pinned.
		array<System::Byte>^ data = gcnew array<System::Byte>(stream->Length);
		pin_ptr<unsigned char> pData = &data[0];
		char* pActualData = (char*)pData;

		m_pPackfile = new ::rage::fiPackfile( );
		init = m_pPackfile->Init( ansiFilename, true, ::rage::fiPackfile::CACHE_MEMORY, pActualData );
		if ( init )
		{
			Initialise();
		}
		delete[] ansiFilename;
		SetupLeafSizes( );
	}
	catch (...)
	{
		throw (gcnew PackfileException());
	}
	finally
	{
		::rage::diagChannel::SetOutput(outputEnabled);
	}

	return ( init );
}

bool 
Packfile::Load( System::String^ filename )
{
	using namespace System::IO;

	// Ensure the RPF system doesn't output TTY.
	bool init = false;
	bool outputEnabled = ::rage::diagChannel::GetOutput();
	::rage::diagChannel::SetOutput(false);
	try
	{
		pin_ptr<const wchar_t> wsFilename = PtrToStringChars( filename );
		pin_ptr<const char> ansiFilename = ManagedRage::WideCharArrayToCharArray( filename, wsFilename );

		m_pPackfile = new ::rage::fiPackfile( );
		init = m_pPackfile->Init( ansiFilename, true, ::rage::fiPackfile::CACHE_NONE );
		if ( init )
		{
			Initialise();
		}
		delete[] ansiFilename;
		SetupLeafSizes( );
	}
	catch (...)
	{
		throw (gcnew PackfileException());
	}
	finally
	{
		::rage::diagChannel::SetOutput(outputEnabled);
	}

	return ( init );
}

bool 
Packfile::Close( )
{
	if ( m_pPackfile )
	{
		m_pPackfile->Shutdown( );
		delete m_pPackfile;
		m_pPackfile = NULL;
	}
	return ( true );
}

PackEntry^ 
Packfile::FindEntry( System::String^ filename )
{
	return FindEntry(filename, true);
}

PackEntry^ 
Packfile::FindEntry( System::String^ filename, bool ignoreCase )
{
	if (this->Entries == nullptr)
		return ( nullptr );

	for each (PackEntry^ entry in this->Entries)
	{
		System::String^ entryFilename;
		if ( System::String::IsNullOrWhiteSpace( entry->Path ) )
			entryFilename = entry->Name;
		else
			entryFilename = System::String::Format( "{0}/{1}", entry->Path, entry->Name );
		entryFilename = entryFilename->Replace('\\', '/');
		if (ignoreCase)
		{
			if ( filename->Equals( entryFilename, System::StringComparison::OrdinalIgnoreCase ) )
				return ( entry );
		}
		else
		{
			if ( filename == entryFilename )
				return ( entry );
		}
	}
	return ( nullptr );
}

array<System::Byte>^ 
Packfile::Extract( System::String^ filename )
{
	using namespace System::Runtime::InteropServices;
	char* ansiFilename = (char*)(void*)Marshal::StringToHGlobalAnsi( filename );
	
	unsigned int hFile = m_pPackfile->GetEntryIndex( ansiFilename );
	if ( -1 == hFile )
	{
		Marshal::FreeHGlobal( (System::IntPtr)ansiFilename );
		return ( nullptr );
	}

	unsigned int size = 0;
	unsigned char* pData = m_pPackfile->ExtractFileToMemory( (unsigned int)hFile, size );
	array<System::Byte>^ data = gcnew array<System::Byte>( size );
	
	System::Runtime::InteropServices::Marshal::Copy( 
		System::IntPtr((void*)pData), data, 0, size );
	delete[] pData;
	Marshal::FreeHGlobal( (System::IntPtr)ansiFilename );

	return ( data );
}

void
Packfile::Initialise( )
{
	m_pPackfile->SetRelativePath( "" );
	System::Collections::Generic::List<PackEntry^>^ entries = 
		gcnew System::Collections::Generic::List<PackEntry^>();
	const ::rage::fiPackEntry* pEntries = m_pPackfile->GetEntries( );

	m_NameShift = m_pPackfile->GetNameShift();

	// Flatten entry structure; gets correct directory names.
	ProcessEntry( &pEntries[0], pEntries, entries, "", 0 );

	// Allocate and assign.
	m_entries = entries->ToArray( );		

	// Loop through our entries to initialise our platform and leaf sizes.
	for ( unsigned int i = 0; i < (unsigned int) m_entries->Length; ++i )
	{
		// Detect platform; don't have public access to fiPackfile header.			
		if ( ( etPlatform::Unknown == m_ePlatform ) && ( m_entries[i]->IsResource ) )
		{
			if ( m_entries[i]->Name->Contains(".w") )
				m_ePlatform = etPlatform::Win32;
			else if ( m_entries[i]->Name->Contains(".x") )
				m_ePlatform = etPlatform::Xenon;
			else if ( m_entries[i]->Name->Contains(".c") )
				m_ePlatform = etPlatform::PS3;
			else if ( m_entries[i]->Name->Contains(".y") )
				m_ePlatform = etPlatform::Win64;
			else if ( m_entries[i]->Name->Contains(".d") )
				m_ePlatform = etPlatform::Durango;
			else if ( m_entries[i]->Name->Contains(".o") )
				m_ePlatform = etPlatform::Orbis;
			break;
		}
	}
}

void
Packfile::ProcessEntry( const ::rage::fiPackEntry* pEntry, const ::rage::fiPackEntry* pEntries, System::Collections::Generic::List<PackEntry^>^ entries, System::String^ prefix, int index )
{
	const char* pNameheap = m_pPackfile->GetNameHeap( );

	if ( pEntry->IsDir() )
	{
		System::String^ name = gcnew System::String( pNameheap + (pEntry->m_NameOffset << m_NameShift) );
		int indexEnd = pEntry->u.directory.m_DirectoryIndex + pEntry->u.directory.m_DirectoryCount;
		for ( int i = pEntry->u.directory.m_DirectoryIndex; i < indexEnd; ++i )
		{
			ProcessEntry( &pEntries[i], pEntries, entries, System::IO::Path::Combine( prefix, name ), i );
		}
	}
	else if ( pEntry->IsFile() || pEntry->IsResource() )
	{
		PackEntry^ packEntry = gcnew PackEntry( this, pNameheap, m_NameShift, pEntry, prefix );
		packEntry->Index = index;
		entries->Add( packEntry );
	}
}

void
Packfile::SetupLeafSizes( )
{
	m_virtualLeafSize = ::rage::g_rscVirtualLeafSize_WIN32;
	m_physicalLeafSize = ::rage::g_rscPhysicalLeafSize_WIN32;

	// Platform specialisations.
	switch ( m_ePlatform )
	{
	case etPlatform::PS3:
		m_virtualLeafSize = ::rage::g_rscVirtualLeafSize_PS3;
		m_physicalLeafSize = ::rage::g_rscPhysicalLeafSize_PS3;
		break;
	default:
		break;
	}
}

PackEntry::PackEntry( Packfile^ parent, const char* nameheap, int nameShift, const ::rage::fiPackEntry* pEntry, System::String^ path )
	: m_pEntry( pEntry )
	, m_parent( parent )
{
	Assert( NULL != pEntry );
	m_sName = gcnew System::String( nameheap + (pEntry->m_NameOffset << nameShift) );
	m_sPath = path;
	if ( !IsDirectory )
		m_offset = pEntry->m_FileOffset << ::rage::fiPackEntry::FileOffset_Shift;
	else
		m_offset = 0;
}

PackEntry::~PackEntry( )
{

}

} // ManagedRage namespace
} // RSG namespace
