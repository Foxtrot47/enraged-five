#include "BinaryToken.h"

// Managed includes
#include "asciitoken.h"
#include "../core/ManagedRage.h"

#include "file/asset.h"
#include "vector/vector2.h"
#include "vector/vector3.h"
#include "vector/vector4.h"
#include "system/param.h"

namespace RSG
{

namespace ManagedRage
{

namespace File
{

#pragma region Con/Destructors
///
///
///
BinTokenizer::BinTokenizer()
	: BaseTokenizer()
{
	this->m_Tokenizer = new ::rage::fiBinTokenizer();
	this->m_BaseTokenizer = this->m_Tokenizer;
}
		
///
///
///
BinTokenizer::~BinTokenizer()
{
	if(m_Tokenizer)
		delete m_Tokenizer;
}
#pragma endregion

///
///
///
void BinTokenizer::Init( System::String^ filename, System::Boolean create )
{
	BaseTokenizer::Init();
	System::String^ basename = System::IO::Path::Combine(System::IO::Path::GetDirectoryName(filename), System::IO::Path::GetFileNameWithoutExtension(filename));
	System::String^ extension = System::IO::Path::GetExtension(filename)->Replace(".", "");

	char* strFilename = (char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(filename);
	char* strExtension = (char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(extension);
	
	if( create )
	{
		m_TuningStream = rage::ASSET.Create( strFilename, strExtension );
		m_TuningStream->PutCh(26);
	}
	else
	{
		m_TuningStream = rage::ASSET.Open( strFilename, strExtension, true, true );
		int c = m_TuningStream->GetCh();
	}

	m_Tokenizer->Init( strFilename, m_TuningStream );

	System::Runtime::InteropServices::Marshal::FreeHGlobal((System::IntPtr)strFilename);
	System::Runtime::InteropServices::Marshal::FreeHGlobal((System::IntPtr)strExtension);
}

///
///
///
void BinTokenizer::Save()
{
	m_TuningStream->Close();
}

}

}

}

