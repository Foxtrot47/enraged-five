//
// File: stringhash.cpp
// Description:
//
// Author: David Muir <david.muir@rockstarnorth.com>
// Date: 30 January 2009
//

#include "mstringhash.h"

#using <System.dll>
using namespace System::Runtime::InteropServices;

// ManagedRage headers
#include "../MarshallingUtils.h"

// RageCore headers
#include "string/stringhash.h"

namespace RSG
{

namespace ManagedRage
{

unsigned int 
StringHashUtil::atStringHash( System::String^ string, unsigned int init )
{
	char* buffer = (char*)(void*)Marshal::StringToHGlobalAnsi(string);
	System::UInt32 hash = (::rage::atStringHash(buffer, init));
	Marshal::FreeHGlobal(System::IntPtr(buffer));

	return (hash);
}

unsigned int 
StringHashUtil::atPartialStringHash( System::String^ string, unsigned int init )
{
	char* buffer = (char*)(void*)Marshal::StringToHGlobalAnsi(string);
	System::UInt32 hash = (::rage::atPartialStringHash(buffer, init));
	Marshal::FreeHGlobal(System::IntPtr(buffer));

	return (hash);
}

} // ManagedRage namespace

} // RSG namespace

// stringhash.cpp
