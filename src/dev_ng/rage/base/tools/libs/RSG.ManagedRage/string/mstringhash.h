//
// File: mstringhash.h
// Description:
//
// Author: David Muir <david.muir@rockstarnorth.com>
// Date: 30 January 2009
//

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#pragma managed
#endif

namespace RSG
{

namespace ManagedRage
{

public ref class StringHashUtil
{
public:
	// See ::rage::atStringHash function documentation
	static unsigned int atStringHash( System::String^ string, unsigned int initValue );

	// See ::rage::atPartialStringHash function documentation
	static unsigned int atPartialStringHash( System::String^ string, unsigned int initValue );

};

} // ManagedRage namespace

} // RSG namespace

// mstringhash.h
