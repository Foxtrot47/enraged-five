// 
// data\aes.cpp 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#include "maes.h"

// Managed includes
#include "../core/Types.h"

// Unmanaged includes
#include "data/aes.h"

namespace RSG
{
namespace ManagedRage
{
	bool Aes::Encrypt( array<System::Byte>^ data )
	{
		int length = data->Length;
		if (length)
		{
			pin_ptr<unsigned char> pinData = &data[0];
			unsigned char* nativeData = pinData;
			// This unmanaged type can't be created on the managed stack, so I have to 'new' it.
			rage::AES* aes = new rage::AES((int)m_Key);
			bool status = aes->Encrypt(nativeData, length);
			delete aes;
			return status;
		}
		return false;
	}

	bool Aes::Decrypt( array<System::Byte>^ data )
	{
		int length = data->Length;
		if (length)
		{
			pin_ptr<unsigned char> pinData = &data[0];
			unsigned char* nativeData = pinData;
			// This unmanaged type can't be created on the managed stack, so I have to 'new' it.
			rage::AES* aes = new rage::AES((int)m_Key);
			bool status = aes->Decrypt(nativeData, length);
			delete aes;
			return status;
		}
		return false;
	}

}
}

