// 
// data\maes.h 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#ifndef MANAGEDRAGE_DATA_AES_H
#define MANAGEDRAGE_DATA_AES_H

namespace RSG
{
	namespace ManagedRage
	{
		public enum class AesKey
		{
			// Add more from aes.h as necessary
			AES_KEY_ID_GTA5_PC = 0x0FFFFFF9,
			AES_KEY_ID_GTA5_PS3	= 0x0FFFFFF8,
			AES_KEY_ID_GTA5_360	= 0x0FFFFFF7,
			AES_KEY_ID_GTA5_AVX	= 0x0FFFFFF6,
			AES_KEY_ID_GTA5SAVE_PC = 0x0FFFFFF5,
			AES_KEY_ID_GTA5SAVE_PS3	= 0x0FFFFFF4,
			AES_KEY_ID_GTA5SAVE_360	= 0x0FFFFFF3,
			AES_KEY_ID_RAGE	= 0x0FFFFF00,
		};

		public ref class Aes
		{
		public:
			Aes(AesKey key) : m_Key(key) {}

			bool Encrypt(array<System::Byte>^ data);
			bool Decrypt(array<System::Byte>^ data);

		private:
			AesKey m_Key;
		};
	}
}

#endif // MANAGEDRAGE_DATA_AES_H
