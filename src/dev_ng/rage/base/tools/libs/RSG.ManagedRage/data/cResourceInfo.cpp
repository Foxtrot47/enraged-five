//
// File: cResourceInfo.cpp
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Managed wrapper around the rage::datResourceInfo class
//              See Rage documentation for usage.
//

#include "cResourceInfo.h"

using namespace System::Diagnostics;

namespace RSG
{

namespace ManagedRage
{

// data Subsystem namespace
namespace data
{

cResourceInfo::cResourceInfo( )
	: m_pResourceInfo( NULL )
	, m_pResourceInfoOld( NULL )
{
}

cResourceInfo::cResourceInfo(const ::rage::datResourceInfo* pResourceInfo)
	: m_pResourceInfo( pResourceInfo )
	, m_pResourceInfoOld( NULL )
{
}

cResourceInfo::cResourceInfo(const ::rage::datResourceInfoOld* pResourceInfo)
	: m_pResourceInfo( NULL )
	, m_pResourceInfoOld( pResourceInfo )
{
}

cResourceInfo::~cResourceInfo()
{
}

size_t 
cResourceInfo::GetVirtualSize()
{
	Assertf( m_pResourceInfo || m_pResourceInfoOld, "Uninitialised cResourceInfo object." );
	if ( !( m_pResourceInfo || m_pResourceInfoOld ) )
		return ( (size_t)-1 );

	if ( m_pResourceInfo )
		return ( 0 );
	else
		return ( m_pResourceInfoOld->GetVirtualSize() );
}

size_t 
cResourceInfo::GetVirtualChunkSize()
{
	Assertf( m_pResourceInfo || m_pResourceInfoOld, "Uninitialised cResourceInfo object." );
	if ( !( m_pResourceInfo || m_pResourceInfoOld ) )
		return ( (size_t)-1 );

	if ( m_pResourceInfo )
		return ( 0 );
	else
		return ( m_pResourceInfoOld->GetVirtualChunkSize() );
}

size_t 
cResourceInfo::GetPhysicalSize()
{
	Assertf( m_pResourceInfo || m_pResourceInfoOld, "Uninitialised cResourceInfo object." );
	if ( !( m_pResourceInfo || m_pResourceInfoOld ) )
		return ( (size_t)-1 );

	if ( m_pResourceInfo )
		return ( m_pResourceInfo->GetPhysicalSize() );
	else
		return ( m_pResourceInfoOld->GetPhysicalSize() );
}

size_t 
cResourceInfo::GetPhysicalChunkSize()
{
	Assertf( m_pResourceInfo || m_pResourceInfoOld, "Uninitialised cResourceInfo object." );
	if ( !( m_pResourceInfo || m_pResourceInfoOld ) )
		return ( (size_t)-1 );

	if ( m_pResourceInfo )
		return ( 0 );
	else
		return ( m_pResourceInfoOld->GetPhysicalChunkSize() );
}

size_t 
cResourceInfo::GetFullVirtualChunkCount()
{
	Assertf( m_pResourceInfo || m_pResourceInfoOld, "Uninitialised cResourceInfo object." );
	if ( !( m_pResourceInfo || m_pResourceInfoOld ) )
		return ( (size_t)-1 );

	if ( m_pResourceInfo )
		return ( 0 );
	else
		return ( m_pResourceInfoOld->GetFullVirtualChunkCount() );
}

size_t 
cResourceInfo::GetVirtualRemainder()
{
	Assertf( m_pResourceInfo || m_pResourceInfoOld, "Uninitialised cResourceInfo object." );
	if ( !( m_pResourceInfo || m_pResourceInfoOld ) )
		return ( (size_t)-1 );

	if ( m_pResourceInfo )
		return ( 0 );
	else
		return ( m_pResourceInfoOld->GetVirtualRemainder() );
}

size_t 
cResourceInfo::GetVirtualChunkCount()
{
	Assertf( m_pResourceInfo || m_pResourceInfoOld, "Uninitialised cResourceInfo object." );
	if ( !( m_pResourceInfo || m_pResourceInfoOld ) )
		return ( (size_t)-1 );

	if ( m_pResourceInfo )
		return ( 0 );
	else
		return ( m_pResourceInfoOld->GetVirtualChunkCount() );
}

size_t 
cResourceInfo::GetFullPhysicalChunkCount()
{
	Assertf( m_pResourceInfo || m_pResourceInfoOld, "Uninitialised cResourceInfo object." );
	if ( !( m_pResourceInfo || m_pResourceInfoOld ) )
		return ( (size_t)-1 );

	if ( m_pResourceInfo )
		return ( 0 );
	else
		return ( m_pResourceInfoOld->GetFullPhysicalChunkCount() );
}

size_t 
cResourceInfo::GetPhysicalRemainder()
{
	Assertf( m_pResourceInfo || m_pResourceInfoOld, "Uninitialised cResourceInfo object." );
	if ( !( m_pResourceInfo || m_pResourceInfoOld ) )
		return ( (size_t)-1 );

	if ( m_pResourceInfo )
		return ( 0 );
	else
		return ( m_pResourceInfoOld->GetPhysicalRemainder() );
}

size_t 
cResourceInfo::GetPhysicalChunkCount()
{
	Assertf( m_pResourceInfo || m_pResourceInfoOld, "Uninitialised cResourceInfo object." );
	if ( !( m_pResourceInfo || m_pResourceInfoOld ) )
		return ( (size_t)-1 );

	if ( m_pResourceInfo )
		return ( 0 );
	else
		return ( m_pResourceInfoOld->GetPhysicalChunkCount() );
}

} // data namespace

} // ManagedRage namespace

} // RSN namespace
