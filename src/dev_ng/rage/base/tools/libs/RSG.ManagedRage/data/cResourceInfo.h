#ifndef MANAGEDRAGE_RESOURCEINFO_H
#define MANAGEDRAGE_RESOURCEINFO_H

//
// File: cResourceInfo.h
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Managed wrapper around the rage::datResourceInfo class
//              See Rage documentation for usage.
//

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma unmanaged
#pragma once
#endif

// Rage Engine Header Files
#include "data/resourceheader.h"

// ManagedRage Library Header Files
#include "../Types.h"

#if defined(_MSC_VER)
#pragma managed
#endif

namespace RSG
{

namespace ManagedRage
{

// data Subsystem namespace
namespace data
{

/**
 * @brief ResourceInfo class
 */
public ref class cResourceInfo sealed : System::IDisposable
{
public:
	cResourceInfo( );
	cResourceInfo( const ::rage::datResourceInfo* pResourceInfo );
	cResourceInfo( const ::rage::datResourceInfoOld* pResourceInfo );
	~cResourceInfo();

	size_t GetVirtualSize();
	size_t GetVirtualChunkSize();
	size_t GetPhysicalSize();
	size_t GetPhysicalChunkSize();
	size_t GetFullVirtualChunkCount();
	size_t GetVirtualRemainder();
	size_t GetVirtualChunkCount();
	size_t GetFullPhysicalChunkCount();
	size_t GetPhysicalRemainder();
	size_t GetPhysicalChunkCount();

private:
	const ::rage::datResourceInfo*		m_pResourceInfo;
	const ::rage::datResourceInfoOld*	m_pResourceInfoOld;
};

} // data namespace

} // ManagedRage namespace

} // RSN namespace

#endif // MANAGEDRAGE_RESOURCEINFO_H

