//
// File: MPropertyAttribute.cpp
// Author: Luke Openshaw <luke.openshaw@rockstarnorth.com>
// Description:  Managed wrapper for property attribute class
//

#include "MPropertyAttribute.h"
#include "crMetadata/propertyattributes.h"
#include "clipxml/clipxml.h"

namespace RSG
{
	namespace ManagedRage
	{
		namespace ClipAnimation
		{
			
			#pragma region MPropertyAttribute
			MPropertyAttribute::MPropertyAttribute(rage::crPropertyAttribute* unmanagedAttribute)
			{
				this->m_Name = gcnew System::String(unmanagedAttribute->GetName());
				this->m_Type = gcnew System::Int32(unmanagedAttribute->GetType());
			}
			MPropertyAttribute::MPropertyAttribute(rage::PropertyAttributeXML* unmanagedAttribute)
			{
				this->m_Name = gcnew System::String(unmanagedAttribute->GetName());
				this->m_Type = gcnew System::Int32(unmanagedAttribute->GetType());
			}
			#pragma endregion // structors 
			
			const rage::crPropertyAttribute* MPropertyAttribute::GenerateUnmanagedAttribute()
			{
				if(m_UnmanagedAttribute)
				{
					char* str = (char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(this->m_Name);
					m_UnmanagedAttribute->SetName(str);
					System::Runtime::InteropServices::Marshal::FreeHGlobal((System::IntPtr)str);
				}
				return m_UnmanagedAttribute;
			}

			const rage::PropertyAttributeXML* MPropertyAttribute::GenerateUnmanagedAttributeXml()
			{
				if(m_UnmanagedAttributeXml)
				{
					char* str = (char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(this->m_Name);
					m_UnmanagedAttributeXml->SetName(str);
					System::Runtime::InteropServices::Marshal::FreeHGlobal((System::IntPtr)str);
				}
				return m_UnmanagedAttributeXml;
			}
			
			#pragma region MPropertyAttributeFloat
			MPropertyAttributeFloat::MPropertyAttributeFloat(rage::crPropertyAttributeFloat* unmanagedAttribute)
				: MPropertyAttribute(unmanagedAttribute)
			{
				this->m_Float = unmanagedAttribute->GetFloat();
			}

			MPropertyAttributeFloat::MPropertyAttributeFloat(rage::PropertyAttributeFloatXML* unmanagedAttribute)
				: MPropertyAttribute(unmanagedAttribute)
			{
				this->m_Float = unmanagedAttribute->GetFloat();
			}
		
			const rage::crPropertyAttribute* MPropertyAttributeFloat::GenerateUnmanagedAttribute()
			{
				m_UnmanagedAttribute = new rage::crPropertyAttributeFloat();
				MPropertyAttribute::GenerateUnmanagedAttribute();
				
				((rage::crPropertyAttributeFloat*)m_UnmanagedAttribute)->GetFloat() = (float)this->m_Float;

				return m_UnmanagedAttribute;
			}

			const rage::PropertyAttributeXML* MPropertyAttributeFloat::GenerateUnmanagedAttributeXml()
			{
				m_UnmanagedAttributeXml = new rage::PropertyAttributeFloatXML();
				MPropertyAttribute::GenerateUnmanagedAttributeXml();

				((rage::PropertyAttributeFloatXML*)m_UnmanagedAttributeXml)->GetFloat() = (float)this->m_Float;

				return m_UnmanagedAttributeXml;
			}
			#pragma endregion

			#pragma region MPropertyAttributeInt
			MPropertyAttributeInt::MPropertyAttributeInt(rage::crPropertyAttributeInt* unmanagedAttribute)
				: MPropertyAttribute(unmanagedAttribute)
			{
				this->m_Int = gcnew System::Int32(unmanagedAttribute->GetInt());
			}

			MPropertyAttributeInt::MPropertyAttributeInt(rage::PropertyAttributeIntXML* unmanagedAttribute)
				: MPropertyAttribute(unmanagedAttribute)
			{
				this->m_Int = gcnew System::Int32(unmanagedAttribute->GetInt());
			}
		
			const rage::crPropertyAttribute* MPropertyAttributeInt::GenerateUnmanagedAttribute()
			{
				m_UnmanagedAttribute = new rage::crPropertyAttributeInt();
				MPropertyAttribute::GenerateUnmanagedAttribute();

				((rage::crPropertyAttributeInt*)m_UnmanagedAttribute)->GetInt() = (int)this->m_Int;
				
				return m_UnmanagedAttribute;
			}

			const rage::PropertyAttributeXML* MPropertyAttributeInt::GenerateUnmanagedAttributeXml()
			{
				m_UnmanagedAttributeXml = new rage::PropertyAttributeIntXML();
				MPropertyAttribute::GenerateUnmanagedAttributeXml();

				((rage::PropertyAttributeIntXML*)m_UnmanagedAttributeXml)->GetInt() = (int)this->m_Int;

				return m_UnmanagedAttributeXml;
			}
			#pragma endregion

			#pragma region MPropertyAttributeBool
			MPropertyAttributeBool::MPropertyAttributeBool(rage::crPropertyAttributeBool* unmanagedAttribute)
				: MPropertyAttribute(unmanagedAttribute)
			{
				this->m_Bool = unmanagedAttribute->GetBool();
			}

			MPropertyAttributeBool::MPropertyAttributeBool(rage::PropertyAttributeBoolXML* unmanagedAttribute)
				: MPropertyAttribute(unmanagedAttribute)
			{
				this->m_Bool = unmanagedAttribute->GetBool();
			}
		
			const rage::crPropertyAttribute* MPropertyAttributeBool::GenerateUnmanagedAttribute()
			{
				m_UnmanagedAttribute = new rage::crPropertyAttributeBool();
				MPropertyAttribute::GenerateUnmanagedAttribute();
				
				((rage::crPropertyAttributeBool*)m_UnmanagedAttribute)->GetBool() = System::Convert::ToBoolean(this->m_Bool);
				
				return m_UnmanagedAttribute;
			}

			const rage::PropertyAttributeXML* MPropertyAttributeBool::GenerateUnmanagedAttributeXml()
			{
				m_UnmanagedAttributeXml = new rage::PropertyAttributeBoolXML();
				MPropertyAttribute::GenerateUnmanagedAttributeXml();

				((rage::PropertyAttributeBoolXML*)m_UnmanagedAttributeXml)->GetBool() = System::Convert::ToBoolean(this->m_Bool);

				return m_UnmanagedAttributeXml;
			}
			#pragma endregion

			#pragma region MPropertyAttributeString
			MPropertyAttributeString::MPropertyAttributeString(rage::crPropertyAttributeString* unmanagedAttribute)
				: MPropertyAttribute(unmanagedAttribute)
			{
				this->m_String = gcnew System::String(unmanagedAttribute->GetString().c_str());
			}

			MPropertyAttributeString::MPropertyAttributeString(rage::PropertyAttributeStringXML* unmanagedAttribute)
				: MPropertyAttribute(unmanagedAttribute)
			{
				this->m_String = gcnew System::String(unmanagedAttribute->GetString().c_str());
			}
		
			const rage::crPropertyAttribute* MPropertyAttributeString::GenerateUnmanagedAttribute()
			{
				m_UnmanagedAttribute = new rage::crPropertyAttributeString();
				MPropertyAttribute::GenerateUnmanagedAttribute();

				char* str = (char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(this->m_String);
				((rage::crPropertyAttributeString*)m_UnmanagedAttribute)->GetString() = rage::atString(str);
				System::Runtime::InteropServices::Marshal::FreeHGlobal((System::IntPtr)str);

				return m_UnmanagedAttribute;
			}

			const rage::PropertyAttributeXML* MPropertyAttributeString::GenerateUnmanagedAttributeXml()
			{
				m_UnmanagedAttributeXml = new rage::PropertyAttributeStringXML();
				MPropertyAttribute::GenerateUnmanagedAttributeXml();

				char* str = (char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(this->m_String);
				((rage::PropertyAttributeStringXML*)m_UnmanagedAttributeXml)->GetString() = rage::atString(str);
				System::Runtime::InteropServices::Marshal::FreeHGlobal((System::IntPtr)str);

				return m_UnmanagedAttributeXml;
			}
			#pragma endregion

			#pragma region MPropertyAttributeBitSet
			MPropertyAttributeBitSet::MPropertyAttributeBitSet(rage::crPropertyAttributeBitSet* unmanagedAttribute)
				: MPropertyAttribute(unmanagedAttribute)
			{
				//this->m_BitSet = gcnew System::Int32(unmanagedAttribute->GetBitSet());
			}
		
			const rage::crPropertyAttribute* MPropertyAttributeBitSet::GenerateUnmanagedAttribute()
			{
				rage::crPropertyAttributeBitSet* unmanagedAttribute = new rage::crPropertyAttributeBitSet();
				MPropertyAttribute::GenerateUnmanagedAttribute();

				return unmanagedAttribute;
			}
			#pragma endregion

			/*float test( rage::crPropertyAttributeVector3* unmanagedAttribute )
			{
				float luke = (unmanagedAttribute->GetVector3())[0];
				return luke;
			}*/

			#pragma region MPropertyAttributeVector3
			MPropertyAttributeVector3::MPropertyAttributeVector3(rage::crPropertyAttributeVector3* unmanagedAttribute)
				: MPropertyAttribute(unmanagedAttribute)
			{
				this->m_Vector3 = gcnew Math::Vector3( (unmanagedAttribute->GetVector3())[0],(unmanagedAttribute->GetVector3())[1],(unmanagedAttribute->GetVector3())[2] );
			}

			MPropertyAttributeVector3::MPropertyAttributeVector3(rage::PropertyAttributeVector3XML* unmanagedAttribute)
				: MPropertyAttribute(unmanagedAttribute)
			{
				this->m_Vector3 = gcnew Math::Vector3( (unmanagedAttribute->GetVector3())[0],(unmanagedAttribute->GetVector3())[1],(unmanagedAttribute->GetVector3())[2] );
			}
		
			const rage::crPropertyAttribute* MPropertyAttributeVector3::GenerateUnmanagedAttribute()
			{
				m_UnmanagedAttribute = new rage::crPropertyAttributeVector3();
				((rage::crPropertyAttributeVector3*)m_UnmanagedAttribute)->GetVector3()[0] = (float)this->m_Vector3->x;
				((rage::crPropertyAttributeVector3*)m_UnmanagedAttribute)->GetVector3()[1] = (float)this->m_Vector3->y;
				((rage::crPropertyAttributeVector3*)m_UnmanagedAttribute)->GetVector3()[2] = (float)this->m_Vector3->z;
				MPropertyAttribute::GenerateUnmanagedAttribute();

				return m_UnmanagedAttribute;
			}

			const rage::PropertyAttributeXML* MPropertyAttributeVector3::GenerateUnmanagedAttributeXml()
			{
				m_UnmanagedAttributeXml = new rage::PropertyAttributeVector3XML();
				((rage::PropertyAttributeVector3XML*)m_UnmanagedAttributeXml)->GetVector3()[0] = (float)this->m_Vector3->x;
				((rage::PropertyAttributeVector3XML*)m_UnmanagedAttributeXml)->GetVector3()[1] = (float)this->m_Vector3->y;
				((rage::PropertyAttributeVector3XML*)m_UnmanagedAttributeXml)->GetVector3()[2] = (float)this->m_Vector3->z;
				MPropertyAttribute::GenerateUnmanagedAttributeXml();

				return m_UnmanagedAttributeXml;
			}
			#pragma endregion

			#pragma region MPropertyAttributeVector4
			MPropertyAttributeVector4::MPropertyAttributeVector4(rage::crPropertyAttributeVector4* unmanagedAttribute)
				: MPropertyAttribute(unmanagedAttribute)
			{
				this->m_Vector4 = gcnew Math::Vector4( (unmanagedAttribute->GetVector4())[0],(unmanagedAttribute->GetVector4())[1],(unmanagedAttribute->GetVector4())[2],(unmanagedAttribute->GetVector4())[3] );
			}
		
			const rage::crPropertyAttribute* MPropertyAttributeVector4::GenerateUnmanagedAttribute()
			{
				m_UnmanagedAttribute = new rage::crPropertyAttributeVector4();
				((rage::crPropertyAttributeVector4*)m_UnmanagedAttribute)->GetVector4()[0] = (float)this->m_Vector4->x;
				((rage::crPropertyAttributeVector4*)m_UnmanagedAttribute)->GetVector4()[1] = (float)this->m_Vector4->y;
				((rage::crPropertyAttributeVector4*)m_UnmanagedAttribute)->GetVector4()[2] = (float)this->m_Vector4->z;
				((rage::crPropertyAttributeVector4*)m_UnmanagedAttribute)->GetVector4()[3] = (float)this->m_Vector4->w;
				MPropertyAttribute::GenerateUnmanagedAttribute();

				return m_UnmanagedAttribute;
			}
			#pragma endregion

			#pragma region MPropertyAttributeQuaternion
			MPropertyAttributeQuaternion::MPropertyAttributeQuaternion(rage::crPropertyAttributeQuaternion* unmanagedAttribute)
				: MPropertyAttribute(unmanagedAttribute)
			{
				this->m_Vector4 = gcnew Math::Vector4( (unmanagedAttribute->GetQuaternion())[0],(unmanagedAttribute->GetQuaternion())[1],(unmanagedAttribute->GetQuaternion())[2],(unmanagedAttribute->GetQuaternion())[3] );
			}

			MPropertyAttributeQuaternion::MPropertyAttributeQuaternion(rage::PropertyAttributeQuaternionXML* unmanagedAttribute)
				: MPropertyAttribute(unmanagedAttribute)
			{
				this->m_Vector4 = gcnew Math::Vector4( (unmanagedAttribute->GetQuaternion())[0],(unmanagedAttribute->GetQuaternion())[1],(unmanagedAttribute->GetQuaternion())[2],(unmanagedAttribute->GetQuaternion())[3] );
			}
		
			const rage::crPropertyAttribute* MPropertyAttributeQuaternion::GenerateUnmanagedAttribute()
			{
				m_UnmanagedAttribute = new rage::crPropertyAttributeQuaternion();
				((rage::crPropertyAttributeQuaternion*)m_UnmanagedAttribute)->GetQuaternion()[0] = (float)this->m_Vector4->x;
				((rage::crPropertyAttributeQuaternion*)m_UnmanagedAttribute)->GetQuaternion()[1] = (float)this->m_Vector4->y;
				((rage::crPropertyAttributeQuaternion*)m_UnmanagedAttribute)->GetQuaternion()[2] = (float)this->m_Vector4->z;
				((rage::crPropertyAttributeQuaternion*)m_UnmanagedAttribute)->GetQuaternion()[3] = (float)this->m_Vector4->w;
				MPropertyAttribute::GenerateUnmanagedAttribute();

				return m_UnmanagedAttribute;
			}

			const rage::PropertyAttributeXML* MPropertyAttributeQuaternion::GenerateUnmanagedAttributeXml()
			{
				m_UnmanagedAttributeXml = new rage::PropertyAttributeQuaternionXML();
				((rage::PropertyAttributeQuaternionXML*)m_UnmanagedAttributeXml)->GetQuaternion()[0] = (float)this->m_Vector4->x;
				((rage::PropertyAttributeQuaternionXML*)m_UnmanagedAttributeXml)->GetQuaternion()[1] = (float)this->m_Vector4->y;
				((rage::PropertyAttributeQuaternionXML*)m_UnmanagedAttributeXml)->GetQuaternion()[2] = (float)this->m_Vector4->z;
				((rage::PropertyAttributeQuaternionXML*)m_UnmanagedAttributeXml)->GetQuaternion()[3] = (float)this->m_Vector4->w;
				MPropertyAttribute::GenerateUnmanagedAttributeXml();

				return m_UnmanagedAttributeXml;
			}
			#pragma endregion

			#pragma region MPropertyAttributeMatrix34
			MPropertyAttributeMatrix34::MPropertyAttributeMatrix34(rage::crPropertyAttributeMatrix34* unmanagedAttribute)
				: MPropertyAttribute(unmanagedAttribute)
			{
			}
		
			const rage::crPropertyAttribute* MPropertyAttributeMatrix34::GenerateUnmanagedAttribute()
			{
				rage::crPropertyAttributeMatrix34* unmanagedAttribute = new rage::crPropertyAttributeMatrix34();

				return unmanagedAttribute;
			}
			#pragma endregion

			#pragma region MPropertyAttributeSituation
			MPropertyAttributeSituation::MPropertyAttributeSituation(rage::crPropertyAttributeSituation* unmanagedAttribute)
				: MPropertyAttribute(unmanagedAttribute)
			{
			}
		
			const rage::crPropertyAttribute* MPropertyAttributeSituation::GenerateUnmanagedAttribute()
			{
				rage::crPropertyAttributeSituation* unmanagedAttribute = new rage::crPropertyAttributeSituation();

				return unmanagedAttribute;
			}
			#pragma endregion

			#pragma region MPropertyAttributeData
			MPropertyAttributeData::MPropertyAttributeData(rage::crPropertyAttributeData* unmanagedAttribute)
				: MPropertyAttribute(unmanagedAttribute)
			{
				
			}
		
			const rage::crPropertyAttribute* MPropertyAttributeData::GenerateUnmanagedAttribute()
			{
				rage::crPropertyAttributeData* unmanagedAttribute = new rage::crPropertyAttributeData();
				MPropertyAttribute::GenerateUnmanagedAttribute();

				return unmanagedAttribute;
			}
			#pragma endregion

			#pragma region MPropertyAttributeHashString
			MPropertyAttributeHashString::MPropertyAttributeHashString(rage::crPropertyAttributeHashString* unmanagedAttribute)
				: MPropertyAttribute(unmanagedAttribute)
			{
				this->m_Hash = gcnew System::UInt32(unmanagedAttribute->GetHashString().GetHash());
				this->m_HashString = gcnew System::String(unmanagedAttribute->GetHashString().GetCStr());
			}
		
			const rage::crPropertyAttribute* MPropertyAttributeHashString::GenerateUnmanagedAttribute()
			{
				m_UnmanagedAttribute = new rage::crPropertyAttributeHashString();
				MPropertyAttribute::GenerateUnmanagedAttribute();
				
				char* str = (char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(this->m_HashString);
				((rage::crPropertyAttributeHashString*)m_UnmanagedAttribute)->GetHashString().SetFromString(str);
				System::Runtime::InteropServices::Marshal::FreeHGlobal((System::IntPtr)str);

				return m_UnmanagedAttribute;
			}
			#pragma endregion
		}
	}
}