//
// File: MTag.cpp
// Author: Luke Openshaw <luke.openshaw@rockstarnorth.com>
// Description: Managed wrapper for crTag type
//

#include "MTag.h"
#include "crMetadata/tag.h"
#include "clipxml/clipxml.h"

namespace RSG
{
	namespace ManagedRage
	{
		namespace ClipAnimation
		{
			#pragma region structors
			MTag::MTag(rage::crTag* unmanagedTag)
			{
				m_Name = gcnew System::String(unmanagedTag->GetName());
				m_Start = unmanagedTag->GetStart();
				m_End = unmanagedTag->GetEnd();
				m_Property = gcnew MProperty(&unmanagedTag->GetProperty());
			}

			MTag::MTag(rage::TagXML* unmanagedTag)
			{
				m_Name = gcnew System::String(unmanagedTag->GetProperty().GetName());
				m_Start = unmanagedTag->GetStart();
				m_End = unmanagedTag->GetEnd();
				m_Property = gcnew MProperty(&unmanagedTag->GetProperty());
			}
			
			MTag::MTag(System::String^ name, System::Single^ start, System::Single^ end, MProperty^ prop)
			{
				m_Name = name;
				m_Start = start;
				m_End = end;
				m_Property = prop;
			}
			
			MTag::~MTag()
			{
				
			}
			#pragma endregion // structors 

			const rage::crTag* MTag::GenerateUnmanagedTag()
			{
				rage::crTag* unmanagedTag = new rage::crTag();
				unmanagedTag->SetStartEnd(System::Convert::ToSingle(m_Start), System::Convert::ToSingle(m_End));
				unmanagedTag->GetProperty() = *(m_Property->GenerateUnmanagedProperty());

				return unmanagedTag;
			}

			const rage::TagXML* MTag::GenerateUnmanagedTagXml()
			{
				rage::TagXML* unmanagedTag = new rage::TagXML();
				unmanagedTag->SetStart(System::Convert::ToSingle(m_Start));
				unmanagedTag->SetEnd(System::Convert::ToSingle(m_End));
				unmanagedTag->GetProperty() = *(m_Property->GenerateUnmanagedPropertyXml());

				return unmanagedTag;
			}
		}
	}
}