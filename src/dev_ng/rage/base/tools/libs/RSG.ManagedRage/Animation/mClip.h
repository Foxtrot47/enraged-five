#ifndef MANAGEDRAGE_CLIP_H
#define MANAGEDRAGE_CLIP_H

//
// File: MClip.h
// Author: Luke Openshaw <luke.openshaw@rockstarnorth.com>
// Description: Managed wrapper for animation clip class
//

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma managed
#pragma once
#endif

#include "MProperty.h"
#include "MTag.h"
#include "clipxml/clipxml.h"

#using <System.dll>

// forward declarations
namespace rage
{
class crProperties;
class crTags;
class crTag;
class crMetadataExtractor;
}


using namespace System::Collections::Generic;

namespace RSG
{

	namespace ManagedRage
	{
		namespace ClipAnimation
		{
			[event_source(managed)]
			public ref class MClip
			{
			public:
				enum class ClipType
				{
					Normal,
					XML,
				};

				#pragma region structors
				MClip(ClipType type);
				~MClip();
				#pragma endregion // structors

				#pragma region Public Functions
				static void			Init();
				System::Boolean^	Load(System::String^ filename);
				//System::Boolean^	Load(System::IO::Stream^ filestream);
				void				Unload();
				System::Boolean^	Save();
				void				RemoveTag(MTag^ tag);
				void				AddTag(MTag^ tag);
				System::Boolean^	GetHasLoaded()	{ return m_HasLoaded; }

				System::String^ GetFilename()		{ return m_Filename; }
				List<MProperty^>^ GetProperties()	{ return m_Properties; }
				List<MTag^>^ GetTags()				{ return m_Tags; }
				System::Single^ GetDuration()		{ return m_Duration; }

				MProperty^ FindProperty(System::String^ propertyName);
				#pragma endregion // Public Functions

				event System::EventHandler^ OnLoad;
			private:
				#pragma region Private Functions
				void FlushManagedPropertiesAndTags();
				void FlushUnmanagedPropertiesAndTags();
				void UpdateClipFromManagedConstruct();
				void PopulatePropertiesAndTags();
				#pragma endregion // Private Functions

				::rage::crMetadataExtractor*		m_UnmanagedExtractor;
				::rage::crProperties*				m_UnmanagedProperties;
				::rage::crTags*						m_UnmanagedTags;
				::rage::crPropertyAttributeFloat*	m_UnmanagedDurationAttribute;

				List<MProperty^>^					m_Properties;
				List<MTag^>^						m_Tags;

				System::Single^						m_Duration;
				System::Boolean^					m_HasLoaded;
				System::String^						m_Filename;

				static System::Boolean					m_Initialised = false;

				::rage::ClipXML*					m_ClipObject;
				ClipType							m_ClipType;
			};
		}

	}

}

#endif // MANAGEDRAGE_CLIP_H