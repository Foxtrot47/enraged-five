#ifndef MANAGEDRAGE_CLIPTAG_H
#define MANAGEDRAGE_CLIPTAG_H

//
// File: MTag.h
// Author: Luke Openshaw <luke.openshaw@rockstarnorth.com>
// Description: Managed wrapper for crTag type
//

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma managed
#pragma once
#endif

#include "MProperty.h"

using namespace System::Collections::Generic;
//using namespace System;

namespace rage
{
	class crTag;
	class TagXML;
}

namespace RSG
{
	namespace ManagedRage
	{
		namespace ClipAnimation
		{
			public ref class MTag
			{
			public:
				#pragma region structors
				MTag(rage::crTag* unmanagedTag);
				MTag(rage::TagXML* unmanagedTag);
				MTag(System::String^ name, System::Single^ start, System::Single^ end, MProperty^ prop);
				~MTag();
				#pragma endregion // structors

				#pragma region Public Functions
				const rage::crTag* GenerateUnmanagedTag();
				const rage::TagXML* GenerateUnmanagedTagXml();

				System::String^ GetName() { return m_Name; }
				System::Single^ GetStart() { return m_Start; }
				System::Single^ GetEnd() { return m_End; }

				void SetStart(System::Single^ start) { m_Start = start; }
				void SetEnd(System::Single^ end) { m_End = end; }

				MProperty^ GetProperty() { return m_Property; }
				#pragma endregion // Public Functions

			private:
				System::String^		m_Name;
				System::Single^		m_Start;
				System::Single^		m_End;

				MProperty^			m_Property;
			};
		}
	}
}

#endif // MANAGEDRAGE_CLIPTAG_H