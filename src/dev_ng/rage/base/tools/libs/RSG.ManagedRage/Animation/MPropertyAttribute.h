#ifndef MANAGEDRAGE_CLIPATTRIBUTE_H
#define MANAGEDRAGE_CLIPATTRIBUTE_H

#include "../vector.h"

//
// File: MPropertyAttribute.h
// Author: Luke Openshaw <luke.openshaw@rockstarnorth.com>
// Description: Managed wrapper for property attribute class
//

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma managed
#pragma once
#endif

namespace rage
{
class crPropertyAttribute;
class crPropertyAttributeFloat;
class crPropertyAttributeInt;
class crPropertyAttributeBool;
class crPropertyAttributeString;
class crPropertyAttributeBitSet;
class crPropertyAttributeVector3;
class crPropertyAttributeVector4;
class crPropertyAttributeQuaternion;
class crPropertyAttributeMatrix34;
class crPropertyAttributeSituation;
class crPropertyAttributeData;
class crPropertyAttributeHashString;

class PropertyAttributeXML;
class PropertyAttributeStringXML;
class PropertyAttributeIntXML;
class PropertyAttributeFloatXML;
class PropertyAttributeBoolXML;
class PropertyAttributeQuaternionXML;
class PropertyAttributeVector3XML;
}

namespace RSG
{
	namespace ManagedRage
	{
		namespace ClipAnimation
		{
			public ref class MPropertyAttribute abstract
			{
			public:
				// LPXO:  This sucks Munsons dick.  The only way round it is to factor the enum out of RAGE and use some pre-compiler
				//			"magic".  For now we duplicate the enum and regret about it later.
				enum class eType
				{
					kTypeNone,

					kTypeFloat,
					kTypeInt,
					kTypeBool,
					kTypeString,
					kTypeBitSet,
					kTypeVector3,
					kTypeVector4,
					kTypeQuaternion,
					kTypeMatrix34,
					kTypeSituation,
					kTypeData,
					kTypeHashString,
					// RESERVED - for more Rage specific property attribute types only

					// this must follow the list of built in property attribute types
					kTypeNum,

					// custom property attribute types *must* have a unique id >= kPropertyAttributeTypeCustom
					kTypeCustom = 0x80,
				};


				MPropertyAttribute(rage::crPropertyAttribute* unmanagedAttribute);
				MPropertyAttribute(rage::PropertyAttributeXML* unmanagedAttribute);
				MPropertyAttribute(System::String^ name) { m_Name = name; }
				virtual const rage::crPropertyAttribute* GenerateUnmanagedAttribute();
				virtual const rage::PropertyAttributeXML* GenerateUnmanagedAttributeXml();
				
				System::String^ GetName() { return m_Name; }
				System::Int32^ GetType() new { return m_Type; }

			protected:
				System::Int32^ m_Type;
				System::String^ m_Name;
				rage::crPropertyAttribute* m_UnmanagedAttribute;
				rage::PropertyAttributeXML* m_UnmanagedAttributeXml;
			};

			public ref class MPropertyAttributeFloat : MPropertyAttribute
			{
			public:
				MPropertyAttributeFloat(rage::crPropertyAttributeFloat* unmanagedAttribute);
				MPropertyAttributeFloat(rage::PropertyAttributeFloatXML* unmanagedAttribute);
				MPropertyAttributeFloat(System::String^ name, System::Single^ value) : MPropertyAttribute(name) { m_Float = value; m_Type = (int)eType::kTypeFloat; }
				virtual const rage::crPropertyAttribute* GenerateUnmanagedAttribute() override;
				virtual const rage::PropertyAttributeXML* GenerateUnmanagedAttributeXml() override;

				System::Single^ GetFloat() { return m_Float; }
				void SetFloat(System::Single^ value) { m_Float = value; }
			private:
				System::Single^ m_Float;
			};

			public ref class MPropertyAttributeInt : MPropertyAttribute
			{
			public:
				MPropertyAttributeInt(rage::crPropertyAttributeInt* unmanagedAttribute);
				MPropertyAttributeInt(rage::PropertyAttributeIntXML* unmanagedAttribute);
				MPropertyAttributeInt(System::String^ name, System::Int32^ value) : MPropertyAttribute(name) { m_Int = value; m_Type = (int)eType::kTypeInt; }
				virtual const rage::crPropertyAttribute* GenerateUnmanagedAttribute() override;
				virtual const rage::PropertyAttributeXML* GenerateUnmanagedAttributeXml() override;

				System::Int32^ GetInt() { return m_Int; }
				void SetInt(System::Int32^ value) { m_Int = value; }
			private:
				System::Int32^ m_Int;
			};

			public ref class MPropertyAttributeBool : MPropertyAttribute
			{
			public:
				MPropertyAttributeBool(rage::crPropertyAttributeBool* unmanagedAttribute);
				MPropertyAttributeBool(rage::PropertyAttributeBoolXML* unmanagedAttribute);
				MPropertyAttributeBool(System::String^ name, System::Boolean^ value) : MPropertyAttribute(name) { m_Bool = value; m_Type = (int)eType::kTypeBool; }
				virtual const rage::crPropertyAttribute* GenerateUnmanagedAttribute() override;
				virtual const rage::PropertyAttributeXML* GenerateUnmanagedAttributeXml() override;

				System::Boolean^ GetBool() { return m_Bool; }
				void SetBool(System::Boolean^ value) { m_Bool = value; }
			private:
				System::Boolean^ m_Bool;
			};

			public ref class MPropertyAttributeString : MPropertyAttribute
			{
			public:
				MPropertyAttributeString(rage::crPropertyAttributeString* unmanagedAttribute);
				MPropertyAttributeString(rage::PropertyAttributeStringXML* unmanagedAttribute);
				MPropertyAttributeString(System::String^ name, System::String^ value) : MPropertyAttribute(name) { m_String = value; m_Type = (int)eType::kTypeString; }
				virtual const rage::crPropertyAttribute* GenerateUnmanagedAttribute() override;
				virtual const rage::PropertyAttributeXML* GenerateUnmanagedAttributeXml() override;

				System::String^ GetString() { return m_String; }
				void SetString(System::String^ value) { m_String = value; }
			private:
				System::String^ m_String;
			};

			public ref class MPropertyAttributeBitSet : MPropertyAttribute
			{
			public:
				MPropertyAttributeBitSet(rage::crPropertyAttributeBitSet* unmanagedAttribute);
				virtual const rage::crPropertyAttribute* GenerateUnmanagedAttribute() override;

				System::Int32^ GetBitSet() { return m_BitSet; }
			private:
				System::Int32^ m_BitSet;
			};

			public ref class MPropertyAttributeVector3 : MPropertyAttribute
			{
			public:
				MPropertyAttributeVector3(rage::crPropertyAttributeVector3* unmanagedAttribute);
				MPropertyAttributeVector3(rage::PropertyAttributeVector3XML* unmanagedAttribute);
				virtual const rage::crPropertyAttribute* GenerateUnmanagedAttribute() override;
				virtual const rage::PropertyAttributeXML* GenerateUnmanagedAttributeXml() override;
			
				Math::Vector3^ GetVector3() { return m_Vector3; }
			private:
				Math::Vector3^ m_Vector3;
			};

			public ref class MPropertyAttributeVector4 : MPropertyAttribute
			{
			public:
				MPropertyAttributeVector4(rage::crPropertyAttributeVector4* unmanagedAttribute);
				virtual const rage::crPropertyAttribute* GenerateUnmanagedAttribute() override;

				Math::Vector4^ GetVector4() { return m_Vector4; }
			private:
				Math::Vector4^ m_Vector4;
				
			};

			public ref class MPropertyAttributeQuaternion : MPropertyAttribute
			{
			public:
				MPropertyAttributeQuaternion(rage::crPropertyAttributeQuaternion* unmanagedAttribute);
				MPropertyAttributeQuaternion(rage::PropertyAttributeQuaternionXML* unmanagedAttribute);
				virtual const rage::crPropertyAttribute* GenerateUnmanagedAttribute() override;
				virtual const rage::PropertyAttributeXML* GenerateUnmanagedAttributeXml() override;

				Math::Vector4^ GetVector4() { return m_Vector4; }
			private:
				Math::Vector4^ m_Vector4;
				
			};

			public ref class MPropertyAttributeMatrix34 : MPropertyAttribute
			{
			public:
				MPropertyAttributeMatrix34(rage::crPropertyAttributeMatrix34* unmanagedAttribute);
				virtual const rage::crPropertyAttribute* GenerateUnmanagedAttribute() override;

			private:
				
			};

			public ref class MPropertyAttributeSituation : MPropertyAttribute
			{
			public:
				MPropertyAttributeSituation(rage::crPropertyAttributeSituation* unmanagedAttribute);
				virtual const rage::crPropertyAttribute* GenerateUnmanagedAttribute() override;

			private:
				
			};

			public ref class MPropertyAttributeData : MPropertyAttribute
			{
			public:
				MPropertyAttributeData(rage::crPropertyAttributeData* unmanagedAttribute);
				virtual const rage::crPropertyAttribute* GenerateUnmanagedAttribute() override;

			private:
				
			};

			public ref class MPropertyAttributeHashString : MPropertyAttribute
			{
			public:
				MPropertyAttributeHashString(rage::crPropertyAttributeHashString* unmanagedAttribute);
				MPropertyAttributeHashString(System::String^ name, System::UInt32^ value) : MPropertyAttribute(name) { m_Hash = value; m_Type = (int)eType::kTypeHashString; }
				virtual const rage::crPropertyAttribute* GenerateUnmanagedAttribute() override;

				System::String^ GetHashString() { return m_HashString; }
				void SetHashString(System::String^ value) { m_HashString = value; }
			private:
				System::UInt32^ m_Hash;
				System::String^ m_HashString;
			};
		}
	}
}

#endif // MANAGEDRAGE_CLIPATTRIBUTE_H
