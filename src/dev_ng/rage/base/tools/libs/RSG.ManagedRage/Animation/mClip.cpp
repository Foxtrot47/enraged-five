//
// File: MClip.h
// Author: Luke Openshaw <luke.openshaw@rockstarnorth.com>
// Description: Managed wrapper for animation clip class
//

// Managed includes
#include "MClip.h"
#include "../core/ManagedRage.h"

// Rage includes
#include "crMetadata/properties.h"
#include "crMetadata/tags.h"
#include "crMetadata/tag.h"
#include "crMetadata/extractor.h"
#include "crMetadata/propertyattributes.h"
#include "file/stream.h"

namespace RSG
{
	namespace ManagedRage
	{
		namespace ClipAnimation
		{
			#pragma region structors
			MClip::MClip(ClipType type) : m_ClipObject(NULL)
			{
				m_ClipType = type;

				if(m_ClipType == ClipType::Normal)
				{
					m_UnmanagedProperties = new rage::crProperties();
					m_UnmanagedTags = new rage::crTags();
					m_UnmanagedExtractor = new rage::crMetadataExtractor();
					m_UnmanagedDurationAttribute = new rage::crPropertyAttributeFloat();
					m_UnmanagedDurationAttribute->SetName("Duration");

					m_UnmanagedExtractor->Add(MakeFunctor(*m_UnmanagedDurationAttribute, &::rage::crPropertyAttributeFloat::Serialize), "Duration:", "DurationEnd");
					m_UnmanagedExtractor->Add(MakeFunctor(*m_UnmanagedProperties, &::rage::crProperties::Serialize), "EmbeddedProperties:\x01", "EmbeddedPropertiesEnd");
					m_UnmanagedExtractor->Add(MakeFunctor(*m_UnmanagedTags, &::rage::crTags::Serialize), "EmbeddedTags:\x01", "EmbeddedTagsEnd");
				}
				else if(m_ClipType == ClipType::XML)
				{
					m_ClipObject = new rage::ClipXML();
				}

				m_Properties = gcnew List<MProperty^>();
				m_Tags = gcnew List<MTag^>();
				m_Duration = 0.0f;
				m_HasLoaded = false;
			}

			MClip::~MClip()
			{
				if(m_Properties)
					delete m_Properties;

				if(m_Tags)
					delete m_Tags;
				
				if(m_UnmanagedExtractor)
					delete m_UnmanagedExtractor;

				if(m_ClipObject)
					delete m_ClipObject;
			}
			#pragma endregion // structors

			#pragma region Public Functions
			void MClip::Init()
			{
				if(!m_Initialised)
				{
					rage::crTag::InitClass();
					RSG::ManagedRage::cRage::Init();
					m_Initialised = true;
				}
			}

			void MClip::RemoveTag(MTag^ tag)
			{
				m_Tags->Remove(tag);
			}

			void MClip::AddTag(MTag^ tag)
			{
				m_Tags->Add(tag);
			}

			System::Boolean^ MClip::Load(System::String^ filename)
			{
				m_Filename = filename;

				if(m_ClipType == ClipType::Normal)
				{
					FlushUnmanagedPropertiesAndTags();

					char* str = (char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(filename);
					if(m_UnmanagedExtractor->Load(str))
					{
						System::Runtime::InteropServices::Marshal::FreeHGlobal((System::IntPtr)str);

						PopulatePropertiesAndTags();
						m_HasLoaded = true;
						OnLoad(this, gcnew System::EventArgs());
					}
					else
					{
						m_HasLoaded = false;
					}
				}
				else if(m_ClipType == ClipType::XML)
				{
					char* str = (char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(filename);

					if(m_ClipObject->Load(str))
					{
						System::Runtime::InteropServices::Marshal::FreeHGlobal((System::IntPtr)str);

						PopulatePropertiesAndTags();
						m_HasLoaded = true;
					}
					else
					{
						m_HasLoaded = false;
					}
				}

				return m_HasLoaded;
			}
			/*
			System::Boolean^ MClip::Load(System::IO::Stream^ filestream)
			{
				FlushUnmanagedPropertiesAndTags();
				rage::fiStream* stream = new rage::fiStream();
				filestream->Seek(0, System::IO::SeekOrigin::Begin);
				//cli::array<System::Byte,1>^ mbytes = gcnew cli::array<System::Byte,1>(filestream->Length);
				//int wrtiecount = filestream->Read(mbytes, 0, filestream->Length);
				for(int i=0; i<filestream->Length; i++)
				{
					System::Int32 b = filestream->ReadByte();
					//System::Int32 byte = System::Convert::ToByte(filestream->ReadByte());
					//stream->WriteByte(, 1);
				}
				//stream->WriteByte(mbyte, 1);
				if(m_UnmanagedExtractor->Load(filestream))
				{
					System::Runtime::InteropServices::Marshal::FreeHGlobal((System::IntPtr)str);

					PopulatePropertiesAndTags();
					m_HasLoaded = true;
					OnLoad(this, gcnew System::EventArgs());
				}
				else
				{
					m_HasLoaded = false;
				}

				return m_HasLoaded;
			}
			*/
			void MClip::Unload()
			{
				FlushManagedPropertiesAndTags();
				FlushUnmanagedPropertiesAndTags();
				m_HasLoaded = false;
			}

			System::Boolean^ MClip::Save()
			{
				bool res = false;

				char* str = (char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(m_Filename);
				UpdateClipFromManagedConstruct();

				if(m_ClipType == ClipType::Normal)
				{
					res = m_UnmanagedExtractor->Save(str);
				}
				else if(m_ClipType == ClipType::XML)
				{
					res = m_ClipObject->Save(str);
				}

				System::Runtime::InteropServices::Marshal::FreeHGlobal((System::IntPtr)str);

				return res;
			}

			MProperty^ MClip::FindProperty(System::String^ propertyName)
			{
				for(int i=0; i<m_Properties->Count; i++)
				{
					if( m_Properties[i]->GetName() == propertyName )
					{
						return m_Properties[i];
					}
				}
				return nullptr;
			}
			#pragma endregion

			#pragma region Private Functions
			void MClip::PopulatePropertiesAndTags()
			{
				FlushManagedPropertiesAndTags();

				if(m_ClipType == ClipType::Normal)
				{
					for(int i=0; i<m_UnmanagedProperties->GetNumProperties(); i++)
					{
						rage::crProperty* unmanagedProp = m_UnmanagedProperties->FindPropertyByIndex(i);
						MProperty^ prop = gcnew MProperty(unmanagedProp);

						m_Properties->Add(prop);
					}

					for(int i=0; i<m_UnmanagedTags->GetNumTags(); i++)
					{
						rage::crTag* unmanagedTag = m_UnmanagedTags->GetTag(i);
						MTag^ tag = gcnew MTag(unmanagedTag);

						m_Tags->Add(tag);
					}
					if(m_UnmanagedDurationAttribute)
					{
						m_Duration = m_UnmanagedDurationAttribute->GetFloat();
					}
					else
					{
						m_Duration = 0.0f;
					}
				}
				else if(m_ClipType == ClipType::XML)
				{
					for(int i=0; i<m_ClipObject->GetNumProperties(); i++)
					{
						rage::PropertyXML* unmanagedProp = m_ClipObject->FindPropertyByIndex(i);
						MProperty^ prop = gcnew MProperty(unmanagedProp);

						m_Properties->Add(prop);
					}

					for(int i=0; i<m_ClipObject->GetTags().GetCount(); ++i)
					{
						rage::TagXML* unmanagedTag = m_ClipObject->GetTags()[i];
						MTag^ tag = gcnew MTag(unmanagedTag);

						m_Tags->Add(tag);
					}
					m_Duration = m_ClipObject->GetDuration();
				}
			}

			void MClip::FlushManagedPropertiesAndTags()
			{
				m_Tags->Clear();
				m_Properties->Clear();
			}

			void MClip::FlushUnmanagedPropertiesAndTags()
			{
				if(m_ClipType == ClipType::Normal)
				{
					m_UnmanagedTags->RemoveAllTags();
					m_UnmanagedProperties->RemoveAllProperties();
				}
			}

			void MClip::UpdateClipFromManagedConstruct()
			{
				FlushUnmanagedPropertiesAndTags();

				if(m_ClipType == ClipType::Normal)
				{
					for(int i=0; i<m_Tags->Count; i++)
					{
						m_UnmanagedTags->AddTag(*(m_Tags[i]->GenerateUnmanagedTag()));
					}

					for(int i=0; i<m_Properties->Count; i++)
					{
						m_UnmanagedProperties->AddProperty(*(m_Properties[i]->GenerateUnmanagedProperty()));
					}
				}
				else if(m_ClipType == ClipType::XML)
				{
					m_ClipObject->ClearTags();
					m_ClipObject->ClearProperties();

					for(int i=0; i<m_Tags->Count; i++)
					{
						m_ClipObject->AddUniqueTag((rage::TagXML*)m_Tags[i]->GenerateUnmanagedTagXml());
					}

					for(int i=0; i<m_Properties->Count; i++)
					{
						m_ClipObject->AddProperty((rage::PropertyXML*)m_Properties[i]->GenerateUnmanagedPropertyXml());
					}
				}
			}
			#pragma endregion

			#pragma region Events

			#pragma endregion
		}
	}
}