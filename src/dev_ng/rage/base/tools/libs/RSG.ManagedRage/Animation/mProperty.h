#ifndef MANAGEDRAGE_CLIPPROPERTY_H
#define MANAGEDRAGE_CLIPPROPERTY_H

//
// File: MProperty.h
// Author: Luke Openshaw <luke.openshaw@rockstarnorth.com>
// Description: Managed wrapper for clip property class
//

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma managed
#pragma once
#endif

#include "MPropertyAttribute.h"
#include "atl/array.h"

namespace rage
{
class crProperty;
class PropertyXML;
}

using namespace System::Collections::Generic;
namespace RSG
{
	namespace ManagedRage
	{
		namespace ClipAnimation
		{
			public ref class MProperty
			{
			public:
				#pragma region structors
				MProperty(rage::crProperty* unmanagedProperty);
				MProperty(rage::PropertyXML* unmanagedProperty);
				MProperty(System::String^ name, List<MPropertyAttribute^>^ attributes);
				~MProperty();
				#pragma endregion // structors

				#pragma region Public Functions
				const rage::crProperty* GenerateUnmanagedProperty();
				const rage::PropertyXML* GenerateUnmanagedPropertyXml();
				List<MPropertyAttribute^>^ GetPropertyAttributes() { return m_Attributes; }
				System::String^ GetName() { return m_Name; }
				#pragma endregion // Public Functions

				#pragma region Private Functions
				void PopulateAttributes(::rage::crProperty* unmanagedProperty);
				void PopulateAttributes(rage::PropertyXML* unmanagedProperty);
				#pragma endregion

			private:
				List<MPropertyAttribute^>^ m_Attributes;  
				System::String^ m_Name;
			};
		}

	}

}

#endif // MANAGEDRAGE_CLIPPROPERTY_H