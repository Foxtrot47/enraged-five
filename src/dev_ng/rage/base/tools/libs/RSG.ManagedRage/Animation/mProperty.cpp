//
// File: MProperty.cpp
// Author: Luke Openshaw <luke.openshaw@rockstarnorth.com>
// Description: Managed wrapper for clip property class
//

#include "MProperty.h"
#include "MPropertyAttribute.h"
#include "crMetadata/property.h"
#include "crMetadata/propertyattributes.h"
#include "clipxml/clipxml.h"

namespace RSG
{
	namespace ManagedRage
	{
		namespace ClipAnimation
		{
			#pragma region structors
			MProperty::MProperty(::rage::crProperty* unmanagedProperty)
			{
				m_Attributes = gcnew List<MPropertyAttribute^>();
				m_Name = gcnew System::String(unmanagedProperty->GetName());
				
				PopulateAttributes(unmanagedProperty);
			}

			MProperty::MProperty(rage::PropertyXML* unmanagedProperty)
			{
				m_Attributes = gcnew List<MPropertyAttribute^>();
				m_Name = gcnew System::String(unmanagedProperty->GetName());

				PopulateAttributes(unmanagedProperty);
			}

			MProperty::MProperty(System::String^ name, List<MPropertyAttribute^>^ attributes)
			{
				m_Attributes = attributes;
				m_Name = name;
			}

			MProperty::~MProperty()
			{
				
			}
			#pragma endregion

			#pragma region Public Functions
			const rage::crProperty* MProperty::GenerateUnmanagedProperty()
			{
				rage::crProperty* unmanagedProperty = new rage::crProperty();

				char* str = (char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(m_Name);
				unmanagedProperty->SetName(str);
				System::Runtime::InteropServices::Marshal::FreeHGlobal((System::IntPtr)str);

				for(int i=0; i<m_Attributes->Count; i++)
				{
					const rage::crPropertyAttribute* unmanagedAttribute = m_Attributes[i]->GenerateUnmanagedAttribute();
					if(unmanagedAttribute)
						unmanagedProperty->AddAttribute(*unmanagedAttribute);
				}

				return unmanagedProperty;
			}
			const rage::PropertyXML* MProperty::GenerateUnmanagedPropertyXml()
			{
				rage::PropertyXML* unmanagedProperty = new rage::PropertyXML();

				char* str = (char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(m_Name);
				unmanagedProperty->SetName(str);
				System::Runtime::InteropServices::Marshal::FreeHGlobal((System::IntPtr)str);

				for(int i=0; i<m_Attributes->Count; i++)
				{
					const rage::PropertyAttributeXML* unmanagedAttribute = m_Attributes[i]->GenerateUnmanagedAttributeXml();
					if(unmanagedAttribute)
						unmanagedProperty->AddAttribute((rage::PropertyAttributeXML*)unmanagedAttribute);
				}


				return unmanagedProperty;
			}
			#pragma endregion

			#pragma region Private Functions
			void MProperty::PopulateAttributes(rage::crProperty* unmanagedProperty)
			{
				
				for(int i=0; i<unmanagedProperty->GetNumAttributes(); i++)
				{
					MPropertyAttribute^ attr;
					rage::crPropertyAttribute* unmanagedAttribute = unmanagedProperty->GetAttributeByIndex(i);
					
					switch (unmanagedAttribute->GetType())
					{
					case rage::crPropertyAttribute::kTypeInt:
						attr =  gcnew MPropertyAttributeInt(static_cast<rage::crPropertyAttributeInt *>(unmanagedAttribute));
						break;
					case rage::crPropertyAttribute::kTypeFloat:
						attr =  gcnew MPropertyAttributeFloat(static_cast<rage::crPropertyAttributeFloat *>(unmanagedAttribute));
						break;
					case rage::crPropertyAttribute::kTypeBool:
						attr =  gcnew MPropertyAttributeBool(static_cast<rage::crPropertyAttributeBool *>(unmanagedAttribute));
						break;
					case rage::crPropertyAttribute::kTypeString:
						attr =  gcnew MPropertyAttributeString(static_cast<rage::crPropertyAttributeString *>(unmanagedAttribute));
						break;
					case rage::crPropertyAttribute::kTypeBitSet:
						attr =  gcnew MPropertyAttributeBitSet(static_cast<rage::crPropertyAttributeBitSet *>(unmanagedAttribute));
						break;
					
					case rage::crPropertyAttribute::kTypeVector3:
						attr =  gcnew MPropertyAttributeVector3(static_cast<rage::crPropertyAttributeVector3 *>(unmanagedAttribute));
						break;
					case rage::crPropertyAttribute::kTypeVector4:
						attr =  gcnew MPropertyAttributeVector4(static_cast<rage::crPropertyAttributeVector4 *>(unmanagedAttribute));
						break;
					case rage::crPropertyAttribute::kTypeQuaternion:
						attr =  gcnew MPropertyAttributeQuaternion(static_cast<rage::crPropertyAttributeQuaternion *>(unmanagedAttribute));
						break;
					case rage::crPropertyAttribute::kTypeMatrix34:
						attr =  gcnew MPropertyAttributeMatrix34(static_cast<rage::crPropertyAttributeMatrix34 *>(unmanagedAttribute));
						break;
					case rage::crPropertyAttribute::kTypeSituation:
						attr =  gcnew MPropertyAttributeSituation(static_cast<rage::crPropertyAttributeSituation *>(unmanagedAttribute));
						break;
					case rage::crPropertyAttribute::kTypeData:
						attr =  gcnew MPropertyAttributeData(static_cast<rage::crPropertyAttributeData *>(unmanagedAttribute));
						break;
					case rage::crPropertyAttribute::kTypeHashString:
						attr =  gcnew MPropertyAttributeHashString(static_cast<rage::crPropertyAttributeHashString *>(unmanagedAttribute));
						break;
					default:
						// Throw exception
						break;
					}
					if(attr)//&& unmanagedAttribute->GetType() != rage::crPropertyAttribute::kTypeVector3)
						m_Attributes->Add(attr);
				}
			}
			void MProperty::PopulateAttributes(rage::PropertyXML* unmanagedProperty)
			{

				for(int i=0; i<unmanagedProperty->GetNumAttributes(); i++)
				{
					MPropertyAttribute^ attr;
					rage::PropertyAttributeXML* unmanagedAttribute = unmanagedProperty->GetAttributeByIndex(i);

					switch (unmanagedAttribute->GetType())
					{
					case rage::PropertyAttributeXML::kTypeInt:
						attr =  gcnew MPropertyAttributeInt(static_cast<rage::PropertyAttributeIntXML *>(unmanagedAttribute));
						break;
					case rage::PropertyAttributeXML::kTypeFloat:
						attr =  gcnew MPropertyAttributeFloat(static_cast<rage::PropertyAttributeFloatXML *>(unmanagedAttribute));
						break;
					case rage::PropertyAttributeXML::kTypeBool:
						attr =  gcnew MPropertyAttributeBool(static_cast<rage::PropertyAttributeBoolXML *>(unmanagedAttribute));
						break;
					case rage::PropertyAttributeXML::kTypeString:
						attr =  gcnew MPropertyAttributeString(static_cast<rage::PropertyAttributeStringXML *>(unmanagedAttribute));
						break;
					case rage::PropertyAttributeXML::kTypeVector3:
						attr =  gcnew MPropertyAttributeVector3(static_cast<rage::PropertyAttributeVector3XML *>(unmanagedAttribute));
						break;
					case rage::PropertyAttributeXML::kTypeQuaternion:
						attr =  gcnew MPropertyAttributeQuaternion(static_cast<rage::PropertyAttributeQuaternionXML *>(unmanagedAttribute));
						break;
					default:
						// Throw exception
						break;
					}
					if(attr)
						m_Attributes->Add(attr);
				}
			}
			#pragma endregion
		}
	}
}