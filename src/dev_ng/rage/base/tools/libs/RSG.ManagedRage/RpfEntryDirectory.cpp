
#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma managed
#endif

#include "RpfEntryDirectory.h"
#include "MarshallingUtils.h"

using namespace System;
using namespace System::Diagnostics;
using namespace System::IO;
using namespace ::rage;


namespace RSG
{

namespace ManagedRage
{

namespace file
{

	RpfEntry::RpfEntry( ::rage::fiPackfile* pEntry )
		: m_pEntry( NULL )
	{
		m_pEntry = new ::rage::fiPackEntry();
	}

	RpfEntry::~RpfEntry( )
	{
		if ( m_pEntry )
			delete m_pEntry;
		m_pEntry = NULL;
	}

	RpfEntryDirectory::RpfEntryDirectory( System::String^ filename )
	{	
		pin_ptr<const wchar_t> wsString = PtrToStringChars(filename);
		pin_ptr<const char> sANSI = ManagedRage::WideCharArrayToCharArray(filename, wsString);

		try
		{
			FileStream^ fp = File::Open( filename, FileMode::Open, FileAccess::Read );
			array<unsigned char^>^ magic = gcnew array<unsigned char^>( 4 );
			fp->Read( magic, 0, 4 );
		}
		catch (...)
		{

		}
		finally 
		{
			fp.Close();
		}
	}

	RpfEntryDirectory::~RpfEntryDirectory( )
	{

	}

} // file namespace

} // ManagedRage namespace

} // RSG namespace

