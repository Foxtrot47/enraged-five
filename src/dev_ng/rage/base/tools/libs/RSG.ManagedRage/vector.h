#ifndef MANAGEDRAGE_VECTOR_H
#define MANAGEDRAGE_VECTOR_H


namespace RSG
{
	namespace ManagedRage
	{
		namespace Math
		{
			public ref class Vector2
			{
			public:
				Vector2( ) { x = 0.0f; y = 0.0f; }
				Vector2( System::Single xx, System::Single yy) { x = xx; y = yy; }
				Vector2( Vector2 %v) { x = v.x; y = v.y; }

				System::Single x;
				System::Single y;
			};

			public ref class Vector3
			{
			public:
				Vector3( ) { x = 0.0f; y = 0.0f; z = 0.0f; }
				Vector3( System::Single xx, System::Single yy, System::Single zz ) { x = xx; y = yy; z = zz; }
				Vector3( Vector3 %v) { x = v.x; y = v.y; z = v.z; }

				System::Single x;
				System::Single y;
				System::Single z;
			};

			public ref class Vector4
			{
			public:
				Vector4( ) { x = 0.0f; y = 0.0f; z = 0.0f; w = 0.0f; }
				Vector4( System::Single xx, System::Single yy, System::Single zz, System::Single ww ) { x = xx; y = yy; z = zz; w = ww; }
				Vector4( Vector4 %v) { x = v.x; y = v.y; z = v.z; w = v.w; }

				System::Single x;
				System::Single y;
				System::Single z;
				System::Single w;
			};
		}
	}
}
#endif // MANAGEDRAGE_VECTOR_H