#ifndef RPFENTRYDIRECTORY_H__
#define RPFENTRYDIRECTORY_H__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma managed
#pragma once
#endif

#include "file/packfile.h"

namespace RSG
{

namespace ManagedRage
{

namespace file
{

	// Managed wrapper around ::rage::fiPackEntry.
	ref class RpfEntry
	{
	public:
		RpfEntry( ::rage::fiPackfile* pEntry );
		~RpfEntry( );
	private:
		::rage::fiPackEntry* m_pEntry;
	};

	// Managed wrapper around the RPF entry directory.
	ref class RpfEntryDirectory
	{
	public:
		RpfEntryDirectory( System::String^ filename );
		~RpfEntryDirectory( );

		int NumEntries( ) { return m_Entries->Length() };
		RpfEntry^ GetEntry( int index ) 
		{ 
			System::Diagnostics::Debug::Assert( index >= 0 );
			System::Diagnostics::Debug::Assert( index < m_Entries->Length );
			return m_Entries[index]; 
		}

	private:
		array<RpfEntry^>^ m_Entries;
	};

} // file namespace

} // ManagedRage namespace

} // RSG namespace

#endif // RPFENTRYDIRECTORY_H__
