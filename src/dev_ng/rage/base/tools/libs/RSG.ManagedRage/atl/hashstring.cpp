// 
// atl/mhashstring.h 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

// Managed includes
#include "hashstring.h"

// Rage includes
#include "atl/hashstring.h"

using namespace System::Runtime::InteropServices;
using namespace RSG::ManagedRage;

System::String^ MAtNonFinalHashString::TryGetString(u32 hash)
{
	const char* str = rage::atNonFinalHashString::TryGetString(hash);
	return str ? gcnew System::String(str) : nullptr;
}

System::String^ MAtFinalHashString::TryGetString(u32 hash)
{
	const char* str = rage::atFinalHashString::TryGetString(hash);
	return str ? gcnew System::String(str) : nullptr;
}


System::String^ MAtLiteralHashString::TryGetString(u32 hash)
{
	const char* str = rage::atLiteralHashString::TryGetString(hash);
	return str ? gcnew System::String(str) : nullptr;
}

void MAtLiteralHashString::Add(System::String^ str)
{
	char* cstr = (char*)(void*)Marshal::StringToHGlobalAnsi(str);
	rage::atLiteralHashString hs(cstr);
	Marshal::FreeHGlobal((System::IntPtr)cstr);
}

MAtLiteralHashValue::MAtLiteralHashValue( const rage::atLiteralHashValue& h )
 : Hash(h.GetHash()) 
{
}

rage::atLiteralHashValue MAtLiteralHashValue::ToRage()
{
	return rage::atLiteralHashValue(Hash);
}

System::String^ MAtLiteralHashValue::ToString()
{
	System::String^ str = MAtLiteralHashString::TryGetString(Hash);
	if (str == nullptr)
	{
		return System::String::Format("(0x{0:x8})", Hash);
	}
	else
	{
		return str;
	}
}
