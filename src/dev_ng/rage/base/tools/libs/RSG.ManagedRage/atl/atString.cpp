//
// File: atString.cpp
// Description:
//
// Author: David Muir <david.muir@rockstarnorth.com>
// Date: 30 January 2009
//

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#pragma managed
#endif

#using <System.dll>
using namespace System::Runtime::InteropServices;

// ManagedRage headers
#include "../MarshallingUtils.h"

// RageCore headers
#include "atl/string.h"
#include "atl/map.h"

namespace RSG
{

namespace ManagedRage
{

	public ref class StringUtil
	{
	public:
		// See ::rage::atHash function documentation
		static System::UInt32^ atHash( System::String^ string )
		{
			char* buffer = (char*)(void*)Marshal::StringToHGlobalAnsi(string);
			System::UInt32 hash = (::rage::atHash(buffer));
			Marshal::FreeHGlobal(System::IntPtr(buffer));

			return (hash);
		}

		// See ::rage::atHash16 function documentation
		static System::UInt16 atHash16( System::String^ string )
		{
			char* buffer = (char*)(void*)Marshal::StringToHGlobalAnsi(string);
			System::UInt16 hash = (::rage::atHash16(buffer));
			Marshal::FreeHGlobal(System::IntPtr(buffer));

			return (hash);
		}

		// See ::rage::atHashU16 function documentation
		static System::UInt16 atHash16U( System::String^ string )
		{
			char* buffer = (char*)(void*)Marshal::StringToHGlobalAnsi(string);
			System::UInt16 hash = (::rage::atHash16U(buffer));
			Marshal::FreeHGlobal(System::IntPtr(buffer));

			return (hash);
		}
	};	

} // ManagedRage namespace

} // RSN namespace

// atString.cpp
