// 
// atl/mhashstring.h 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#ifndef MANAGEDRAGE_ATL_HASHSTRING
#define MANAGEDRAGE_ATL_HASHSTRING

#include "../core/Types.h"
#include "atl/hashstring.h"

namespace RSG
{
namespace ManagedRage
{

public value struct MAtLiteralHashValue
{
public:
	explicit MAtLiteralHashValue(const rage::atLiteralHashValue& h);
	explicit MAtLiteralHashValue(u32 h) : Hash(h) {}
	initonly u32 Hash;

	bool operator==(MAtLiteralHashValue other)
	{
		return Hash == other.Hash;
	}

	property bool IsNull { bool get() { return Hash == 0; } }
	property bool IsNotNull { bool get() { return Hash != 0; } }

	rage::atLiteralHashValue ToRage();

	virtual System::String^ ToString() override;

};

// I don't really need the classes below yet, just the string lookup fns

public value struct MAtFinalHashString
{
public:
	static System::String^ TryGetString(u32 hash);
};

public value struct MAtNonFinalHashString
{
public:
	static System::String^ TryGetString(u32 hash);
};

public value struct MAtLiteralHashString
{
public:
	static System::String^ TryGetString(u32 hash);
	static void Add(System::String^ str);
};


} // namespace ManagedRage
} // namespace RSG

#endif // MANAGEDRAGE_ATL_HASHSTRING