﻿//---------------------------------------------------------------------------------------------
// <copyright file="XgeXmlInterfaceTest.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2012-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Incredibuild.UnitTest
{
    using System;
    using System.IO;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using RSG.Interop.Incredibuild;
    using XGE = RSG.Interop.Incredibuild.XGE;

    /// <summary>
    /// 
    /// </summary>
    [TestClass]
    public class XgeXmlInterfaceTest
    {
        private static readonly String FILENAME = "x:/test.xml";

        [TestInitialize]
        public void Setup()
        {
            System.Diagnostics.Debug.Listeners.Clear();
        }

        [TestMethod]
        [ExpectedException(typeof(XGE.ConfigurationException))]
        public void TestEmptyProject()
        {
            XGE.Project proj = new XGE.Project("Unit Tests");

            // Prior to setting the environment; validate its currently invalid.
            proj.Write(FILENAME);
        }

        [TestMethod]
        public void TestValidProject()
        {
            XGE.Project proj = new XGE.Project("Unit Tests");
            
            XGE.Environment env_360 = new XGE.Environment("env_360");
            XGE.Environment env_PS3 = new XGE.Environment("env_PS3");
            env_360.Variables.Add("BinVar", Path.Combine(Environment.GetEnvironmentVariable("RS_TOOLSROOT"), "bin"));
            env_PS3.Variables.Add("BinVar", Path.Combine(Environment.GetEnvironmentVariable("RS_TOOLSROOT"), "bin"));
            XGE.Tool tool_360 = new XGE.Tool("test360", "ragebuilder_0378.exe", "-version");
            XGE.Tool tool_PS3 = new XGE.Tool("testPS3", "ragebuilder_0378.exe", "-version");
            env_360.Tools.Add(tool_360);
            env_PS3.Tools.Add(tool_PS3);

            Assert.AreEqual(1, env_360.Tools.Count);
            Assert.AreEqual(1, env_360.Variables.Count);
            Assert.AreEqual(1, env_PS3.Tools.Count);
            Assert.AreEqual(1, env_PS3.Variables.Count);

            XGE.TaskJob task_360 = new XGE.TaskJob("task_360", String.Empty);
            XGE.TaskJob task_PS3 = new XGE.TaskJob("task_ps3", String.Empty);
            XGE.TaskGroup group_360 = new XGE.TaskGroup("group_360",
                new XGE.ITask[] { task_360, task_360 }, env_360, tool_360);
            XGE.TaskGroup group_PS3 = new XGE.TaskGroup("group_PS3",
                new XGE.ITask[] { task_PS3, task_PS3 }, env_PS3, tool_PS3);
            proj.Tasks.Add(group_360);
            proj.Tasks.Add(group_PS3);
            proj.DefaultEnvironment = env_360;
            proj.Write(FILENAME);

            //XGE.XGE.Start(FILENAME);
        }
    }

} // RSG.Interop.Incredibuild.UnitTest namespace
