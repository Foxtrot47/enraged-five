﻿//---------------------------------------------------------------------------------------------
// <copyright file="IncredibuildStateTest.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2012-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Incredibuild.UnitTest
{
    using System;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using RSG.Interop.Incredibuild;

    /// <summary>
    /// Unit tests for the Incredibuild class.
    /// </summary>
    [TestClass]
    public class InredibuildStateTest
    {
        [TestMethod]
        public void Test()
        {
            Assert.IsTrue(Incredibuild.IsInstalled);
            Assert.IsTrue(Incredibuild.InstallDirectory.Length > 0);
            Assert.IsTrue(System.IO.Directory.Exists(Incredibuild.InstallDirectory));
            Assert.IsTrue(Incredibuild.Version.StartsWith("4."));
        }
    }

} // RSG.Interop.Incredibuild.UnitTest namespace
