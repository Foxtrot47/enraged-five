﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.IO;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Text.RegularExpressions;
using RSG.Base.Logging;
using RSG.Base.Configuration.Automation;
using RSG.Base.Configuration.Bugstar;
using RSG.Base.Configuration.Export;
using RSG.Base.Configuration.Export.Implementation;
using RSG.Base.Configuration.Ports;
using RSG.Base.Configuration.Portal;
using RSG.Base.Configuration.Reports;
using RSG.Base.Configuration.ROS;
using RSG.Base.Configuration.Implementation;
using RSG.Base.Tasks;

// *** DHM FIX ME: trying to get rid of the P4 dependency ***
using RSG.SourceControl.Perforce;
using RSG.Base.Configuration.Rag;

namespace RSG.Base.Configuration
{

    /// <summary>
    /// Factory class to create our various configuration objects.
    /// </summary>
    public static class ConfigFactory
    {
        #region Constants
        /// <summary>
        /// Log context string.
        /// </summary>
        private static readonly String LOG_CTX = "LogFactory";
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        static ConfigFactory()
        {
            ExceptionFormatter.RegisterExceptionFormatter<ConfigurationException>(ConfigurationExceptionFormatter);
            ExceptionFormatter.RegisterExceptionFormatter<ConfigurationVersionException>(ConfigurationVersionExceptionFormatter);
        }
        #endregion // Constructor(s)

        #region Static Controller Methods
        /// <summary>
        /// Create core tools/project configuration data.
        /// </summary>
        /// <returns></returns>
        public static IConfig CreateConfig()
        {
            Config.Log.ProfileCtx(LOG_CTX, "Creating configuration object.");
            IConfig config = new Config(String.Empty);
            Config.Log.ProfileEnd();
            return (config);
        }

        /// <summary>
        /// Create core tools/project configuration data; attempting to initialise
        /// main project to a particular DLC project.
        /// </summary>
        /// <param name="dlcName">DLC project key (e.g. "dlc_w_ar_heavyrifle")</param>
        /// <returns>Configuration object</returns>
        public static IConfig CreateConfig(String dlcName)
        {
            Config.Log.ProfileCtx(LOG_CTX, "Creating configuration object.");
            IConfig config = new Config(dlcName);
            Config.Log.ProfileEnd();
            return (config);
        }

        /// <summary>
        /// Create writable core tools/project configuration data.  ONLY TO BE
        /// USED BY THE INSTALLER.
        /// </summary>
        /// <returns></returns>
        public static Installer.IWritableConfig CreateWritableConfig()
        {
            Config.Log.ProfileCtx(LOG_CTX, "Creating *writable*-configuration object.");
            Installer.IWritableConfig config = new Config(String.Empty);
            Config.Log.ProfileEnd();
            return (config);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<IProjectSummary> CreateProjectSummary()
        {
            using (ProfileContext ctx = new ProfileContext(Config.Log,
                "Creating Project Summary objects."))
            {
                String toolsRoot = System.Environment.GetEnvironmentVariable("RS_TOOLSROOT");
                String filename = Config.GetConfigFilename(toolsRoot);
                try
                {
                    List<IProjectSummary> projects = new List<IProjectSummary>();
                    XDocument xmlDoc = XDocument.Load(filename);
                    foreach (XElement xmlProjectElem in
                        xmlDoc.XPathSelectElements("/config/projects/project"))
                    {
                        projects.Add(new ProjectSummary(xmlProjectElem));
                    }

                    foreach (XElement xmlProjectElem in
                        xmlDoc.XPathSelectElements("/config/projects/dlc"))
                    {
                        projects.Add(new ProjectSummary(xmlProjectElem));
                    }

                    return (projects);
                }
                catch (Exception ex)
                {
                    throw new ConfigurationException("Failed to read project summary information.", ex)
                        {
                            Filename = filename
                        };
                }
            }
        }

        /// <summary>
        /// Create application settings configuration data (for project).
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        public static IDictionary<String, IApplicationSettings> CreateApplicationSettings(IProject project)
        {
            Config.Log.ProfileCtx(LOG_CTX, "Creating application settings object: {0}.", project.FriendlyName);
            IDictionary<String, IApplicationSettings> settings = LoadApplicationSettings(project);
            Config.Log.ProfileEnd();
            return (settings);
        }

        /// <summary>
        /// Create export configuration data.
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        public static IExportConfig CreateExportConfig(IProject project, ExportConfigType types)
        {
            Config.Log.ProfileCtx(LOG_CTX, "Creating export config object: {0}.", types);
            IExportConfig exportConfig = ExportConfig.Load(project, types);
            Config.Log.ProfileEnd();
            return (exportConfig);
        }

        /// <summary>
        /// Create report configuration data.
        /// </summary>
        /// <param name="config"></param>
        /// <returns></returns>
        [Obsolete("Use CreateReportConfig IProject variant instead.", true)]
        public static IReportsConfig CreateReportConfig(IConfig config)
        {
            return (CreateReportConfig(config.Project));
        }

        /// <summary>
        /// Create report configuration data.
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        public static IReportsConfig CreateReportConfig(IProject project)
        {
            Config.Log.ProfileCtx(LOG_CTX, "Creating report config object: {0}.", project.FriendlyName);
            IReportsConfig reports = new ReportsConfig(project);
            Config.Log.ProfileEnd();
            return (reports);
        }

        /// <summary>
        /// Create ROS (Rockstar Online Services) configuration data.
        /// </summary>
        /// <returns></returns>
        [Obsolete("Use CreateROSConfig(project) instead.")]
        public static IROSConfig CreateROSConfig()
        {
            IConfig config = CreateConfig();
            return (CreateROSConfig(config.Project));
        }

        /// <summary>
        /// Create ROS (Rockstar Online Services) configuration data.
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        public static IROSConfig CreateROSConfig(IProject project)
        {
            Config.Log.ProfileCtx(LOG_CTX, "Creating ROS config object: {0}.", project.FriendlyName);
            IROSConfig ros = new ROSConfig(project);
            Config.Log.ProfileEnd();
            return (ros);
        }

        /// <summary>
        /// Create Automation Core Services configuration data.
        /// </summary>
        /// <returns></returns>
        [Obsolete("Use 'CreateAutomationServicesConfig(project)' instead.")]
        public static IAutomationCoreServices CreateAutomationConfig()
        {
            IConfig config = CreateConfig();
            return (CreateAutomationConfig(config));
        }

        /// <summary>
        /// Create Automation Core Services configuration data for project.
        /// </summary>
        /// <param name="config"></param>
        /// <returns></returns>
        [Obsolete("Use 'CreateAutomationServicesConfig(project)' instead.")]
        public static IAutomationCoreServices CreateAutomationConfig(IConfig config)
        {
            return (CreateAutomationConfig(config.Project));
        }

        /// <summary>
        /// Create Automation Core Services configuration data for project.
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        [Obsolete("Use 'CreateAutomationServicesConfig(project)' instead.")]
        public static IAutomationCoreServices CreateAutomationConfig(IProject project)
        {
            Config.Log.ProfileCtx(LOG_CTX, "Creating Automation Services config object: {0}.", project.FriendlyName);
            IAutomationCoreServices automationConfig = new AutomationCoreServices(project);
            Config.Log.ProfileEnd();
            return (automationConfig);
        }

        /// <summary>
        /// Create Automation Service configuration data for project.
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        public static IAutomationServicesConfig CreateAutomationServicesConfig(IProject project)
        {
            using (ProfileContext ctx = new ProfileContext(Config.Log, LOG_CTX,
                "Creating Automation Services Configuration data: {0}.", project.FriendlyName))
            {
                return (AutomationServicesConfig.Load(project));
            }
        }

        /// <summary>
        /// Create Portal configuration data for project.
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        public static IPortalConfig CreatePortalConfig(IProject project, String projectPath = null)
        {
            using (ProfileContext ctx = new ProfileContext(Config.Log, LOG_CTX,
                "Creating Portal Configuration data: {0}.", project.FriendlyName))
            {
                return (PortalConfig.Load(project, projectPath));
            }
        }

        /// <summary>
        /// Create Bugstar configuration data.
        /// </summary>
        /// <returns></returns>
        [Obsolete("Use 'CreateBugstarConfig(project)' instead.")]
        public static IBugstarConfig CreateBugstarConfig()
        {
            IConfig config = CreateConfig();
            return (CreateBugstarConfig(config.Project));
        }

        /// <summary>
        /// Create Bugstar configuration data.
        /// </summary>
        /// <returns></returns>
        /// Note: you should prefer the CreateBugstarConfig(project) variant which
        /// gives you a project-specific Bugstar configuration object.
        /// 
        public static IBugstarConfig CreateBugstarConfig(IConfig config)
        {
            return (CreateBugstarConfig(config.Project));
        }

        /// <summary>
        /// Create Bugstar configuration data.
        /// </summary>
        /// <returns></returns>
        public static IBugstarConfig CreateBugstarConfig(IProject project)
        {
            Config.Log.ProfileCtx(LOG_CTX, "Creating Bugstar config object: {0}.", project.FriendlyName);
            IBugstarConfig config = new BugstarConfig(project);
            Config.Log.ProfileEnd();
            return (config);
        }

        /// <summary>
        /// Create installer configuration data.
        /// </summary>
        /// <returns></returns>
        public static Installer.IInstallerConfig CreateInstallerConfig(IProject project)
        {
            Installer.IInstallerConfig config = null;
            using (ProfileContext ctx = new ProfileContext(Config.Log, LOG_CTX, 
                "Creating Installer config object."))
            {
                config = new Installer.InstallerConfig(project);
            }
            return (config);
        }

        /// <summary>
        /// </summary>
        /// <param name="project">Project to create the port configuration for.</param>
        /// <returns>Port configuration object.</returns>
        public static IPortConfig CreatePortConfig(IProject project)
        {
            Config.Log.ProfileCtx(LOG_CTX, "Creating port config object: {0}.", project.FriendlyName);
            IPortConfig config = new PortConfig(project);
            Config.Log.ProfileEnd();
            return config;
        }

        /// <summary>
        /// Creates rag configuration data.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="project"></param>
        /// <returns></returns>
        public static IRagConfig CreateRagConfig(IProject project)
        {
            using (new ProfileContext(Config.Log, LOG_CTX, "Creating rag config object: {0}.",
                project.FriendlyName))
            {
                return (new RagConfig(Config.Log, project));
            }
        }

        /// Create environment object.
        /// </summary>
        /// <returns></returns>
        public static IEnvironment CreateEnvironment()
        {
            return (new Environment());
        }
        #endregion // Static Controller Methods

        #region Private Methods
        /// <summary>
        /// Load application settings for a project.
        /// </summary>
        /// <param name="project"></param>
        private static IDictionary<String, IApplicationSettings> LoadApplicationSettings(IProject project)
        {
            String filename = Path.Combine(project.Config.ToolsConfig, "applications.xml");
            IDictionary<String, IApplicationSettings> settings = new Dictionary<String, IApplicationSettings>();
            if (!File.Exists(filename))
            {
                return (settings);
            }

            XDocument xmlDoc = XDocument.Load(filename);
            IEnumerable<XElement> xmlApplicationElems = xmlDoc.XPathSelectElements("/Applications/Application");
            foreach (XElement xmlApplicationElem in xmlApplicationElems)
            {
                IApplicationSettings appSettings = new ApplicationSettings(xmlApplicationElem);
                settings.Add(appSettings.Name, appSettings);
            }
            return (settings);
        }
        #endregion // Private Methods

        #region Exception Formatters
        /// <summary>
        /// Custom formatter for configuration exceptions.
        /// </summary>
        /// <param name="ex">Exception to format.</param>
        /// <returns></returns>
        private static IEnumerable<String> ConfigurationExceptionFormatter(Exception ex)
        {
            ConfigurationException configException = ex as ConfigurationException;
            if (configException == null)
            {
                Debug.Fail("Invalid ConfigurationException.");
                throw new ArgumentException("ex");
            }

            ICollection<String> information = new List<String>();
            if (!String.IsNullOrEmpty(configException.Filename))
            {
                information.Add(String.Format("Filename: '{0}'.", configException.Filename));
            }
            return information;
        }

        /// <summary>
        /// Custom formatter for configuration exceptions.
        /// </summary>
        /// <param name="ex">Exception to format.</param>
        /// <returns></returns>
        private static IEnumerable<String> ConfigurationVersionExceptionFormatter(Exception ex)
        {
            ConfigurationVersionException versionException = ex as ConfigurationVersionException;
            if (versionException == null)
            {
                Debug.Fail("Invalid ConfigurationVersionException.");
                throw new ArgumentException("ex");
            }

            ICollection<String> information = new List<String>();
            if (!String.IsNullOrEmpty(versionException.Filename))
            {
                information.Add(String.Format("Filename: '{0}'.", versionException.Filename));
            }
            information.Add(String.Format("ExpectedVersion: '{0}'.", versionException.ExpectedVersion));
            information.Add(String.Format("ActualVersion: '{0}'.", versionException.ActualVersion));
            return information;
        }
        #endregion // Exception Formatters
    } // ConfigFactory

} // RSG.Base.Configuration namespace
