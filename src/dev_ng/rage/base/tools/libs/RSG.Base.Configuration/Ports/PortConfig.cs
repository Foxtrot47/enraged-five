﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;

namespace RSG.Base.Configuration.Ports
{
    /// <summary>
    /// 
    /// </summary>
    internal class PortConfig : IPortConfig
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="Filename"/> property.
        /// </summary>
        private readonly String _filename;

        /// <summary>
        /// Private field for the <see cref="Ports"/> property;
        /// </summary>
        private readonly IDictionary<String, int> _ports;
        #endregion // Fields

        #region Constructor(s)
        /// <summary>
        /// Creates a new instance of the <see cref="PortConfig"/> class.
        /// </summary>
        /// <param name="project"></param>
        public PortConfig(IProject project)
        {
            _filename = Path.Combine(project.Config.ToolsConfig, "ports.xml");
            _ports = new Dictionary<String, int>();

            // Make sure the file exists.
            if (!File.Exists(_filename))
            {
                Debug.Fail(String.Format("Global port configuration file '{0}' does not exist.", _filename));
                throw new ConfigurationException("Global port configuration file does not exist.")
                    {
                        Filename = this.Filename
                    };
            }
            Parse(_filename);

            // Check if the user has a local override file.
            String localFilename = Path.Combine(project.Config.ToolsConfig, "ports.local.xml");
            if (File.Exists(localFilename))
            {
                Parse(localFilename);
            }
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// Port configuration XML file.
        /// </summary>
        public String Filename
        {
            get { return _filename; }
        }

        /// <summary>
        /// Mapping of identifiers to ports.
        /// </summary>
        /// <todo>Change this to an IReadOnlyDictionary when we upgrade to 4.5.</todo>
        public IDictionary<String, int> Ports
        {
            get { return _ports; }
        }
        #endregion // Properties

        #region Serialisation Code
        /// <summary>
        /// Parses the passed in filename populating the list of available ports.
        /// </summary>
        /// <param name="filename">File to parse.</param>
        private void Parse(String filename)
        {
            // Load the document and extract all ports.
            XDocument doc = XDocument.Load(filename);

            IEnumerable<XElement> portElems = doc.XPathSelectElements("/Ports/Port");
            foreach (XElement portElem in portElems)
            {
                XAttribute idAtt = portElem.Attribute("id");
                XElement valueElem = portElem.Element("Value");

                if (idAtt != null && valueElem != null)
                {
                    int port;
                    if (Int32.TryParse(valueElem.Value, out port))
                    {
                        _ports[idAtt.Value] = port;
                    }
                    else
                    {
                        Config.Log.Warning(
                            "Unable to parse the port for the '{0}' key. '{1}' is not a valid UInt32 value.",
                            idAtt.Value,
                            valueElem.Value);
                    }
                }
                else
                {
                    Config.Log.Warning("Port element is missing a key or value sub-element.");
                }
            }
        }
        #endregion // Serialisation Code
    } // PortConfig
}
