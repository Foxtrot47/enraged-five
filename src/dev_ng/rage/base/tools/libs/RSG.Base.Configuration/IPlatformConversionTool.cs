﻿using System;
using System.Xml.Linq;

namespace RSG.Base.Configuration
{

    /// <summary>
    /// Platform conversion tool interface abstraction.
    /// </summary>
    public interface IPlatformConversionTool : ISerialisable
    {
        #region Properties
        /// <summary>
        /// Associated platform for this conversion tool.
        /// </summary>
        RSG.Platform.Platform Platform { get; }

        /// <summary>
        /// Associated branch for this conversion tool.
        /// </summary>
        IBranch Branch { get; }

        /// <summary>
        /// Absolute path to conversion tool executable (or script).
        /// </summary>
        String ExecutablePath { get; }

        /// <summary>
        /// Platform and tool global options (independent of what is being converted).
        /// </summary>
        String Arguments { get; }
        #endregion // Properties
    }

} // RSG.Base.Configuration namespace
