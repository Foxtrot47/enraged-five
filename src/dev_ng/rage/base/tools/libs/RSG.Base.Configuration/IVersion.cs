﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Configuration
{
    /// <summary>
    /// Tools version interface.
    /// </summary>
    public interface IToolsVersion
    {
        #region Properties
        /// <summary>
        /// Version XML configuration filename.
        /// </summary>
        String Filename { get; }

        /// <summary>
        /// Version of the tools the user has locally.
        /// </summary>
        ILabelledVersion LocalVersion { get; }

        /// <summary>
        /// Version of the base tools (minimum tools version the user needs to have installed).
        /// </summary>
        ILabelledVersion BaseVersion { get; }

        /// <summary>
        /// Version of the tools the user last installed.
        /// </summary>
        ILabelledVersion InstalledVersion { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Reloads the data from file.
        /// </summary>
        void Reload();

        /// <summary>
        /// Returns whether the version of the tools that the user last installed is valid.
        /// </summary>
        /// <returns></returns>
        bool IsValid();
        #endregion // Methods
    } // IVersion

    /// <summary>
    /// Version interface.
    /// </summary>
    public interface IVersion : IComparable<IVersion>
    {
        /// <summary>
        /// Major version.
        /// </summary>
        uint Major { get; }

        /// <summary>
        /// Minor version.
        /// </summary>
        uint Minor { get; }

        /// <summary>
        /// Revision number.
        /// </summary>
        uint Revision { get; }
    } // IVersionNumber

    /// <summary>
    /// 
    /// </summary>
    public interface ILabelledVersion : IVersion
    {
        /// <summary>
        /// Name of the label.
        /// </summary>
        String LabelName { get; }
    } // ILabelledVersion
}
