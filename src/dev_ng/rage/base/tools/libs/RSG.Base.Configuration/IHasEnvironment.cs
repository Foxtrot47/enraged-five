﻿using System;

namespace RSG.Base.Configuration
{

    /// <summary>
    /// IHasEnvironment specifies that the object has an associated Environment
    /// object.
    /// </summary>
    public interface IHasEnvironment
    {
        #region Properties
        /// <summary>
        /// Encapsulated Environment object.
        /// </summary>
        IEnvironment Environment { get; }
        #endregion // Properties
    }

} // RSG.Base.Configuration namespace
