﻿using System;
using System.IO;
using RSG.Base.Logging;
using RSG.SourceControl.Perforce;
using RSG.SourceControl.Perforce.Util;
using P4API;
using System.Text.RegularExpressions;

namespace RSG.Base.Configuration
{

    /// <summary>
    /// 
    /// </summary>
    public enum PerforceLabelState
    {
        Unknown,
        Labelled,
        Ahead,
        Behind,
        Mismash,
    }

    /// <summary>
    /// 
    /// </summary>
    public static class PerforceLabelStateUtils
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <param name="label"></param>
        /// <returns></returns>
        public static PerforceLabelState GetLabelStatus(P4 p4, String path, String label)
        {
            String havePath = Path.Combine(path, "...#have");
            String labelPath = Path.Combine(path, String.Format("...@{0}", label));

            P4RecordSet rset = p4.Run("diff2", false, "-q", havePath, labelPath);
            if ((null == rset) || rset.HasErrors())
            {
                Log.Log__Error("Error during Perforce label status determination.");
                p4.LogP4RecordSetMessages(rset);
                return (PerforceLabelState.Unknown);
            }
            else
            {
                int filesAhead = 0, filesBehind = 0;
                PerforceLabelState state = PerforceLabelState.Labelled;
                foreach (P4Record record in rset)
                {
                    String status = RecordParser.GetStringField(record, "status");
                    if ((0 == String.Compare(status, "content")) ||
                        (0 == String.Compare(status, "types")))
                    {
                        int haveRev = RecordParser.GetIntField(record, "rev");
                        int labelRev = RecordParser.GetIntField(record, "rev2");

                        if (haveRev > labelRev)
                            ++filesAhead;
                        else if (labelRev > haveRev)
                            ++filesBehind;
                    }
                }
                if ((0 == filesAhead) && (0 == filesBehind))
                    state = PerforceLabelState.Labelled;
                else if ((0 == filesAhead) && (filesBehind > 0))
                    state = PerforceLabelState.Behind;
                else if ((0 == filesBehind) && (filesAhead > 0))
                    state = PerforceLabelState.Ahead;
                else
                    state = PerforceLabelState.Mismash;
                return (state);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="incrementalLabelName"></param>
        /// <param name="configLabelVersion"></param>
        /// <returns></returns>
        public static PerforceLabelState GetQuickLabelStatus(P4 p4, uint configMajorVersion, uint configMinorVersion, String incrementalLabelName, String currentLabelName)
        {
            PerforceLabelState state = PerforceLabelState.Unknown;

            RSG.SourceControl.Perforce.Label latestLabel = RSG.SourceControl.Perforce.Label.GetCurrentToolsVersionLabel(p4, incrementalLabelName, currentLabelName);
            if (latestLabel != null)
            {
                // Convert the label versions
                uint major, minor;
                if (RSG.SourceControl.Perforce.Label.GetToolsLabelVersion(latestLabel.Name, incrementalLabelName, out major, out minor))
                {
                    if (configMajorVersion == major)
                    {
                        if (configMinorVersion < minor)
                        {
                            state = PerforceLabelState.Behind;
                        }
                        else if (configMinorVersion > minor)
                        {
                            // Not sure if this can ever happen
                            state = PerforceLabelState.Ahead;
                        }
                        else // if (configMinorVersion == minor)
                        {
                            state = PerforceLabelState.Labelled;
                        }
                    }
                    else if (configMajorVersion < major)
                    {
                        state = PerforceLabelState.Behind;
                    }
                    else // if (configMajorVersion > major)
                    {
                        // Not sure if this can ever happen
                        state = PerforceLabelState.Ahead;
                    }
                }
            }

            return state;
        }
    }

} // RSG.Base.Configuration
