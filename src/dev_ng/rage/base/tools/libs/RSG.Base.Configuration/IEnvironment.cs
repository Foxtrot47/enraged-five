﻿using System;
using System.Collections.Generic;

namespace RSG.Base.Configuration
{

    /// <summary>
    /// Bracket-type used for variable additions.
    /// </summary>
    /// Note: please use $(var) for substitions; want to migrate the texture
    /// pipeline to that at some point.
    /// 
    public enum BracketType
    {
        Default,        // $(var)-style; default as per Ruby environment.

        [Obsolete("Only used in texture pipeline.")]
        AlternateCurly, // Texture-pipeline support; e.g. ${RS_ASSETS}.
    }
    
    /// <summary>
    /// Environment interface to allow variable string substitution.
    /// </summary>
    public interface IEnvironment : IEnumerable<KeyValuePair<String, String>>
    {
        #region Properties
        /// <summary>
        /// Indexer based on environment variable name.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        String this[String key] { get; }

        /// <summary>
        /// Current depth of environment stack (minimum 1).
        /// </summary>
        int Depth { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Add a new value into the environment.
        /// </summary>
        /// <param name="variable"></param>
        /// <param name="value"></param>
        void Add(String variable, String value);

        /// <summary>
        /// Add a new value into the environment; using specified bracket type.
        /// </summary>
        /// <param name="variable"></param>
        /// <param name="value"></param>
        /// <param name="bracket"></param>
        /// This method is only required for the Texture Pipeline's use of
        /// ${RS_ASSETS}.  *** It should not be used anywhere else. ***
        /// 
        void Add(String variable, String value, BracketType bracket);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="variable"></param>
        /// <returns></returns>
        String Lookup(String variable);

        /// <summary>
        /// Substitute a string with any environment variables.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        String Subst(String value);

        /// <summary>
        /// Reverse substitution; absolute path to environment variables.  Attempts
        /// to match longest path first.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        String ReverseSubst(String value);

        /// <summary>
        /// Determines whether there are substitutions required.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        bool RequiresSubst(String value);

        /// <summary>
        /// Resets the entire environment.
        /// </summary>
        void Reset();

        /// <summary>
        /// Clear the top-level environment of settings.
        /// </summary>
        void Clear();

        /// <summary>
        /// Push a new context.
        /// </summary>
        void Push();

        /// <summary>
        /// Pop last context.
        /// </summary>
        bool Pop();
        #endregion // Methods
    }

} // RSG.Base.Configuration namespace
