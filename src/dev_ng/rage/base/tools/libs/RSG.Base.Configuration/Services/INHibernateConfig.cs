﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Configuration.Services
{
    /// <summary>
    /// NHibernate configuration interface.
    /// </summary>
    public interface INHibernateConfig
    {
        /// <summary>
        /// Name of the database to connect to.
        /// </summary>
        String DatabaseName { get; }

        /// <summary>
        /// Location that the database resides in.
        /// </summary>
        String DatabaseLocation { get; }

        /// <summary>
        /// Username to use to connect to the database.
        /// </summary>
        String DatabaseUsername { get; }

        /// <summary>
        /// Password to use to connect to the database.
        /// </summary>
        String DatabasePassword { get; }

        /// <summary>
        /// How long SQL commands can run on the db before timing out.  Set to 0 for an infinite timeout.
        /// </summary>
        uint CommandTimeout { get; }

        /// <summary>
        /// Flag indicating whether we wish to run the server in debug mode.
        /// This enables some additional logging.
        /// </summary>
        bool DebugMode { get; }

        /// <summary>
        /// Whether the server should only allow read-only operations on the database.
        /// </summary>
        bool ReadOnly { get; }

        /// <summary>
        /// Hibernate dialect
        /// </summary>
        String HibernateDialect { get; }

        /// <summary>
        /// Database driver
        /// </summary>
        String DatabaseDriver { get; }

        /// <summary>
        /// Adds a prefix to the table so we can have multiple running
        /// </summary>
        String DatabaseTablePrefix { get; }
    } // INHibernateConfig
}
