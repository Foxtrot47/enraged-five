﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Base.Configuration.Services;

namespace RSG.Base.Configuration.Services
{
    /// <summary>
    /// Encapsulates all information relating to a single statistics server.
    /// </summary>
    public interface IServer : IServiceHostConfig, INHibernateConfig
    {
        #region Properties
        /// <summary>
        /// Friendly name for the server.
        /// </summary>
        String Name { get; }
        #endregion
    } // IServer
}
