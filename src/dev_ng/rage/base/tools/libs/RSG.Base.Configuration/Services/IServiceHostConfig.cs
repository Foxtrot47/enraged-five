﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Configuration.Services
{
    /// <summary>
    /// Interface for config data related to services.
    /// </summary>
    public interface IServiceHostConfig
    {
        /// <summary>
        /// Address to use to access the server.
        /// </summary>
        String Address { get; }

        /// <summary>
        /// Port to host the web pages and HTTP wcf endpoints on.
        /// </summary>
        uint HttpPort { get; }

        /// <summary>
        /// Port to host the web pages and HTTPS wcf endpoints on.
        /// </summary>
        uint HttpsPort { get; }

        /// <summary>
        /// Port to host the TCP wcf endpoints on.
        /// </summary>
        uint TcpPort { get; }

        /// <summary>
        /// Whether the server is running inside of IIS.
        /// </summary>
        bool UsingIIS { get; }
    } // IServiceHostConfig
}
