﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace RSG.Base.Configuration.Services
{
    public class Server : IServer
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="Name" /> property.
        /// </summary>
        private readonly String _name;

        /// <summary>
        /// Private field for the <see cref="Address" /> property.
        /// </summary>
        private readonly String _address;

        /// <summary>
        /// Private field for the <see cref="HttpPort" /> property.
        /// </summary>
        private readonly uint _httpPort;

        /// <summary>
        /// Private field for the <see cref="HttpsPort"/> property.
        /// </summary>
        private readonly uint _httpsPort;

        /// <summary>
        /// Private field for the <see cref="TcpPort" /> property.
        /// </summary>
        private readonly uint _tcpPort;

        /// <summary>
        /// Private field for the <see cref="UsingIIS" /> property.
        /// </summary>
        private readonly bool _usingIIS;

        /// <summary>
        /// Private field for the <see cref="DatabaseName" /> property.
        /// </summary>
        private readonly String _databaseName;

        /// <summary>
        /// Private field for the <see cref="DatabaseLocation" /> property.
        /// </summary>
        private readonly String _databaseLocation;

        /// <summary>
        /// Private field for the <see cref="DatabaseUsername" /> property.
        /// </summary>
        private readonly String _databaseUsername;

        /// <summary>
        /// Private field for the <see cref="DatabaePassword" /> property.
        /// </summary>
        private readonly String _databasePassword;

        /// <summary>
        /// Private field for the <see cref="CommandTimeout" /> property.
        /// </summary>
        private readonly uint _commandTimeout;

        /// <summary>
        /// Private field for the <see cref="DebugMode" /> property.
        /// </summary>
        private readonly bool _debugMode;

        /// <summary>
        /// Private field for the <see cref="ReadOnly" /> property.
        /// </summary>
        private readonly bool _readOnly;

        /// <summary>
        /// Private field for the <see cref="HibernateDialect" /> property.
        /// </summary>
        private readonly String _hibernateDialect;

        /// <summary>
        /// Private field for the <see cref="DatabaseDriver" /> property.
        /// </summary>
        private readonly String _databaseDriver;

        /// <summary>
        /// Private field for the <see cref="DatabaseTablePrefix" /> property.
        /// </summary>
        private readonly String _databaseTablePrefix;
        #endregion // Fields

        #region Constructor(s)
        /// <summary>
        /// Default
        /// </summary>
        /// <param name="elem"></param>
        public Server(XElement elem)
        {
            _name = GetAttributeValue<String>(elem, "name");

            // IServiceHostConfig properties.
            _address = GetAttributeValue<String>(elem, "address");
            _httpPort = GetAttributeValue<uint>(elem, "http_port");
            _httpsPort = GetAttributeValue<uint>(elem, "https_port");
            _tcpPort = GetAttributeValue<uint>(elem, "tcp_port");
            _usingIIS = GetAttributeValue<bool>(elem, "using_iis");

            // INHibernateConfig properties.
            _databaseName = GetAttributeValue<String>(elem, "db_name");
            _databaseLocation = GetAttributeValue<String>(elem, "db_location");
            _databaseUsername = GetAttributeValue<String>(elem, "db_username");
            _databasePassword = GetAttributeValue<String>(elem, "db_password");
            _debugMode = GetAttributeValue<bool>(elem, "debug_mode");
            _readOnly = GetAttributeValue<bool>(elem, "read_only");
            _commandTimeout = GetAttributeValue<uint>(elem, "command_timeout");
            _hibernateDialect = GetAttributeValue<String>(elem, "dialect");
            _databaseDriver = GetAttributeValue<String>(elem, "driver");
            _databaseTablePrefix = GetAttributeValue<String>(elem, "db_table_prefix");
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// Friendly name for the server.
        /// </summary>
        public String Name
        {
            get { return _name; }
        }

        /// <summary>
        /// Address to use to access the server.
        /// </summary>
        public String Address
        {
            get { return _address; }
        }

        /// <summary>
        /// Port to host the web pages and HTTP wcf endpoints on.
        /// </summary>
        public uint HttpPort
        {
            get { return _httpPort; }
        }

        /// <summary>
        /// Port to host the TCP wcf endpoints on.
        /// </summary>
        public uint TcpPort
        {
            get { return _tcpPort; }
        }

        /// <summary>
        /// Port to host the web pages and HTTPS wcf endpoints on.
        /// </summary>
        public uint HttpsPort
        {
            get { return _httpsPort; }
        }

        /// <summary>
        /// Whether the server is running inside of IIS.
        /// </summary>
        public bool UsingIIS
        {
            get { return _usingIIS; }
        }

        /// <summary>
        /// Name of the database to connect to.
        /// </summary>
        public String DatabaseName
        {
            get { return _databaseName; }
        }

        /// <summary>
        /// Location that the database resides in.
        /// </summary>
        public String DatabaseLocation
        {
            get { return _databaseLocation; }
        }

        /// <summary>
        /// Username to use to connect to the database.
        /// </summary>
        public String DatabaseUsername
        {
            get { return _databaseUsername; }
        }

        /// <summary>
        /// Password to use to connect to the database.
        /// </summary>
        public String DatabasePassword
        {
            get { return _databasePassword; }
        }

        /// <summary>
        /// How long SQL commands can run on the db before timing out.  Set to 0 for an infinite timeout.
        /// </summary>
        public uint CommandTimeout
        {
            get { return _commandTimeout; }
        }

        /// <summary>
        /// Flag indicating whether we wish to run the server in debug mode.
        /// This enables some additional logging.
        /// </summary>
        public bool DebugMode
        {
            get { return _debugMode; }
        }

        /// <summary>
        /// Whether the server should only allow read-only operations on the database.
        /// </summary>
        public bool ReadOnly
        {
            get { return _readOnly; }
        }

        /// <summary>
        /// Hibernate dialect
        /// </summary>
        public String HibernateDialect
        {
            get { return _hibernateDialect; }
        }

        /// <summary>
        /// Database driver
        /// </summary>
        public String DatabaseDriver
        {
            get { return _databaseDriver; }
        }

        /// <summary>
        /// Adds a prefix to the table so we can have multiple running
        /// </summary>
        public String DatabaseTablePrefix
        {
            get { return _databaseTablePrefix; }
        }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Read attribute of a particular type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="elem"></param>
        /// <param name="attr"></param>
        protected T GetAttributeValue<T>(XElement elem, String attr)
        {
            if (elem.Attribute(attr) == null)
            {
                throw new ArgumentNullException(String.Format("Unable to read the '{0}' attribute under the '{1}' element.", attr, elem.Name));
            }

            return (T)Convert.ChangeType(elem.Attribute(attr).Value, typeof(T));
        }
        #endregion // Methods
    } // Server
}
