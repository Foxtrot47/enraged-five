﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Configuration.Installer
{

    /// <summary>
    /// 
    /// </summary>
    public interface IApplicationInstall
    {
        #region Properties
        /// <summary>
        /// Application name.
        /// </summary>
        String Name { get; }

        /// <summary>
        /// Version setup for project (used by installer).
        /// </summary>
        IDictionary<String, bool> Versions { get; }
        #endregion // Properties
    }

} // RSG.Base.Configuration.Installer namespace
