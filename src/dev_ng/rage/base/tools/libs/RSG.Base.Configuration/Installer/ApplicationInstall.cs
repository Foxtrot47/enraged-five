﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;

namespace RSG.Base.Configuration.Installer
{

    /// <summary>
    /// 
    /// </summary>
    internal class ApplicationInstall : IApplicationInstall
    {
        #region Properties
        /// <summary>
        /// Application name.
        /// </summary>
        public String Name 
        { 
            get;
            private set;
        }

        /// <summary>
        /// Version setup for project (used by installer).
        /// </summary>
        public IDictionary<String, bool> Versions 
        { 
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor; from XML element in configuration data.
        /// </summary>
        /// <param name="xmlElem"></param>
        public ApplicationInstall(XElement xmlElem)
        {
            Debug.Assert(xmlElem.Name.LocalName.Equals("Application"),
                "Unsupported XML element for application settings.");
            if (!xmlElem.Name.LocalName.Equals("Application"))
                throw new ArgumentException("Unsupported XML element for application settings.");
            
            this.Versions = new Dictionary<String, bool>();

            XElement xmlNameElem = xmlElem.Element("Name");
            if (null != xmlNameElem)
                this.Name = xmlNameElem.Value;

            IEnumerable<XElement> xmlVersionsElem = xmlElem.XPathSelectElements("Version");
            foreach (XElement xmlVersionElem in xmlVersionsElem)
            {
                XElement xmlIdElem = xmlVersionElem.Element("Id");
                XElement xmlInstallElem = xmlVersionElem.Element("Install");
                if (null != xmlIdElem && null != xmlInstallElem)
                {
                    bool install = false;
                    if (bool.TryParse(xmlInstallElem.Value, out install))
                    {
                        this.Versions.Add(xmlIdElem.Value, install);
                    }
                }
            }
        }
        #endregion // Constructor(s)
    }

} // RSG.Base.Configuration.Installer namespace
