﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Configuration.Installer
{

    /// <summary>
    /// Installer configuration data.
    /// </summary>
    public interface IInstallerConfig
    {
        /// <summary>
        /// Applications to install information.
        /// </summary>
        IEnumerable<IApplicationInstall> Installs { get; }

        /// <summary>
        /// Installer runlines.
        /// </summary>
        IEnumerable<IRunline> Runlines { get; }
    }

} // RSG.Base.Configuration.Installer namespace
