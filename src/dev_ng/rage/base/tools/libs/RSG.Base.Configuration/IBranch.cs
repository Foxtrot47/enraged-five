﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;
using RSG.Base.Xml;

namespace RSG.Base.Configuration
{

    /// <summary>
    /// Branch encapsulation; representing a project branch defining a set of
    /// paths and targets.
    /// </summary>
    public interface IBranch : 
        IHasEnvironment,
        IEnumerable<ITarget>
    {
        #region Properties
        /// <summary>
        /// Project container for this Branch object.
        /// </summary>
        IProject Project { get; }

        /// <summary>
        /// Branch name ("dev", "release" etc).
        /// </summary>
        String Name { get; }

        /// <summary>
        /// Branch locked state (e.g. flagged after branch is released or locked for future edits).
        /// </summary>
        bool Locked { get; }

        /// <summary>
        /// Whether this is the default branch for the Project.
        /// </summary>
        bool IsDefault { get; set; }
        
        /// <summary>
        /// Art root directory path.
        /// </summary>
        String Art { get; }

        /// <summary>
        /// Anim root directory path.
        /// </summary>
        String Anim { get; }

        /// <summary>
        /// Assets root directory path.
        /// </summary>
        String Assets { get; }

        /// <summary>
        /// Root content-tree filename for this branch.
        /// </summary>
        String Content { get; }

        /// <summary>
        /// Export root directory path.
        /// </summary>
        String Export { get; }

        /// <summary>
        /// Processed root directory path.
        /// </summary>
        String Processed { get; }

        /// <summary>
        /// Metadata root directory path.
        /// </summary>
        String Metadata { get; }

        /// <summary>
        /// Definitions root directory path.
        /// </summary>
        String Definitions { get; }

        /// <summary>
        /// Audio root directory path.
        /// </summary>
        String Audio { get; }

        /// <summary>
        /// Build root directory path.
        /// </summary>
        String Build { get; }

        /// <summary>
        /// Common root directory path.
        /// </summary>
        String Common { get; }

        /// <summary>
        /// Shaders root directory path.
        /// </summary>
        String Shaders { get; }

        /// <summary>
        /// Code root directory path.
        /// </summary>
        String Code { get; }

        /// <summary>
        /// RAGE Code root directory path.
        /// </summary>
        String RageCode { get; }

        /// <summary>
        /// Preview root directory path.
        /// </summary>
        String Preview { get; }

        /// <summary>
        /// Script root directory path.
        /// </summary>
        String Script { get; }

        /// <summary>
        /// Fragment tuning root directory path.
        /// </summary>
        String FragmentTune { get; }

        /// <summary>
        /// Text export directory path.
        /// </summary>
        String Text { get; }

        /// <summary>
        /// Targets for this Branch.
        /// </summary>
        IDictionary<RSG.Platform.Platform, ITarget> Targets { get; }

        /// <summary>
        /// Platform conversion tools per-platform (e.g. Ragebuilder).
        /// </summary>
        IDictionary<RSG.Platform.Platform, IPlatformConversionTool> PlatformConversionTools { get; }

        /// <summary>
        /// Branch build label (optional).
        /// </summary>
        Label BuildLabel { get; }

        /// <summary>
        /// Common arguments for Ragebuilder, specified in project.xml
        /// </summary>
        string RageBuilderCommonArgs { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Return whether the passed in filename is from a particular branch's
        /// prefix directory.
        /// </summary>
        /// <param name="prefix"></param>
        /// <param name="filename"></param>
        /// <returns></returns>
        /// E.g.
        ///   branch.IsRootedPath(branch.Export, @"x:\file\asset1.txt")
        ///   
        bool IsRootedPath(String prefix, String filename);

        /// <summary>
        /// Import settings into an IEnvironment object.
        /// </summary>
        /// <param name="environment"></param>
        void Import(IEnvironment environment);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlElem"></param>
        void ParseLocal(XElement xmlElem);

        /// <summary>
        /// Initialise Branch DLC targets.
        /// </summary>
        void SetupBranchDLCTargets();

        /// <summary>
        /// Determine the Perforce state for the branch build directory; whether 
        /// user is on the label etc.
        /// </summary>
        PerforceLabelState GetLocalBuildState();

        /// <summary>
        /// PostInitialisation flush, for $(core) based keys handling.
        /// </summary>
        void PostEnvironmentInit();

        /// <summary>
        /// Retrieve the Pipeline Options specified for the given key and item 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="optionName"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        T GetPipelineOption<T>(string key, string optionName,  T defaultValue);
        #endregion // Methods
    }

} // RSG.Base.Configuration namespace
