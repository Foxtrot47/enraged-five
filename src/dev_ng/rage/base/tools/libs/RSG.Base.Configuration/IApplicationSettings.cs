﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Configuration
{

    /// <summary>
    /// Application settings interface.
    /// </summary>
    public interface IApplicationSettings
    {
        #region Properties
        /// <summary>
        /// Application name.
        /// </summary>
        String Name { get; }

        /// <summary>
        /// Version setup for project (used by installer).
        /// </summary>
        IDictionary<String, bool> Versions { get; }

        /// <summary>
        /// Application configuration data container.
        /// </summary>
        IDictionary<String, Object> Parameters { get; }
        #endregion // Properties
    }

} // RSG.Base.Configuration namespace
