﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.IO;
using System.Xml.Linq;
using RSG.Base.Logging;

namespace RSG.Base.Configuration
{
    /// <summary>
    /// 
    /// </summary>
    internal class ToolsVersion : IToolsVersion
    {
        #region Constants
        internal static readonly String ELEM_VERSION = "version";

        internal static readonly String ATTR_LABEL = "label";
        internal static readonly String ATTR_BASELABEL = "baseLabel";
        #endregion // Constants

        #region Fields
        /// <summary>
        /// Private field for the <see cref="Filename"/> property.
        /// </summary>
        private readonly String _filename;
        #endregion // Fields

        #region Properties
        /// <summary>
        /// XML version configuration file.
        /// </summary>
        public String Filename
        {
            get { return _filename; }
        }

        /// <summary>
        /// Version of the tools the user is on.
        /// </summary>
        public ILabelledVersion LocalVersion { get; private set; }

        /// <summary>
        /// Version of the base tools (minimum tools version the user needs to have installed).
        /// </summary>
        public ILabelledVersion BaseVersion { get; private set; }

        /// <summary>
        /// Version of the tools the user last installed.
        /// </summary>
        public ILabelledVersion InstalledVersion { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public ToolsVersion(IConfig config)
        {
            _filename = Path.Combine(config.ToolsConfig, "version.xml");
        }
        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        public void Reload()
        {
            if (!File.Exists(this.Filename))
            {
                Debug.Fail(String.Format("Version config XML '{0}' does not exist.", this.Filename));
                throw new ConfigurationException("Version config XML file does not exist.")
                    {
                        Filename = this.Filename
                    };
            }

            XDocument versionDoc = XDocument.Load(this.Filename,
                LoadOptions.SetLineInfo | LoadOptions.SetBaseUri);
            ParseVersionConfig(versionDoc);
        }

        /// <summary>
        /// Returns whether the version of the tools that the user last installed is valid.
        /// </summary>
        /// <returns></returns>
        public bool IsValid()
        {
            return (InstalledVersion.CompareTo(BaseVersion) >= 0);
        }
        #endregion // Public Methods

        #region Private Methods
        /// <summary>
        /// Parse version configuration file.
        /// </summary>
        /// <param name="xmlDoc"></param>
        private void ParseVersionConfig(XDocument xmlDoc)
        {
            foreach (XAttribute xmlConfigAttr in xmlDoc.Root.Attributes())
            {
                if (String.Equals(xmlConfigAttr.Name.LocalName, ATTR_BASELABEL))
                {
                    this.BaseVersion = new LabelledVersion(xmlConfigAttr.Value);

                    //TODO: This is a temporary hack for the installed version.  The installer needs to
                    // add the version to the local.xml file before we can extract it and make use of it.
                    this.InstalledVersion = new LabelledVersion(xmlConfigAttr.Value);
                }
                else if (String.Equals(xmlConfigAttr.Name.LocalName, ATTR_LABEL))
                {
                    this.LocalVersion = new LabelledVersion(xmlConfigAttr.Value);
                }
                else
                {
#if DEBUG
                    Config.Log.Debug(xmlDoc, "Unknown config attribute: '{0}'.",
                        xmlConfigAttr.Name.LocalName);
#endif
                }
            }
        }
        #endregion // Private Methods
    } // ToolsVersion


    /// <summary>
    /// Representation of a version.
    /// </summary>
    public class Version : IVersion
    {
        #region Properties
        /// <summary>
        /// Major version.
        /// </summary>
        public uint Major { get; set; }

        /// <summary>
        /// Minor version.
        /// </summary>
        public uint Minor { get; set; }

        /// <summary>
        /// Revision number.
        /// </summary>
        public uint Revision { get; set; }
        #endregion // Properties

        #region IComparable<IVersion> Implementation
        /// <summary>
        /// Compares the current object with another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>A value that indicates the relative order of the objects being compared.
        ///     The return value has the following meanings: Value Meaning Less than zero
        ///     This object is less than the other parameter.Zero This object is equal to
        ///     other. Greater than zero This object is greater than other.</returns>
        public int CompareTo(IVersion other)
        {
            if (Major == other.Major)
            {
                if (Minor == other.Minor)
                {
                    return Revision.CompareTo(other.Revision);
                }
                else
                {
                    return Minor.CompareTo(other.Minor);
                }
            }
            else
            {
                return Major.CompareTo(other.Major);
            }
        }
        #endregion // IComparable<MapDefinition> Implementation

        #region Comparable Implementation
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int CompareTo(object obj)
        {
            if (obj == null)
            {
                return 1;
            }
            
            IToolsVersion other = obj as IToolsVersion;
            Debug.Assert(other != null, "Object is not of type IVersion");
            if (other == null)
            {
                throw new ArgumentException("Object is not of type IVersion");
            }
            
            return CompareTo(other);
        }
        #endregion // Comparable Implementation

        #region Object Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return (String.Format("Version {0}.{1}.{2}", this.Major, this.Minor, this.Revision));
        }
        #endregion // Object Overrides
    } // Version


    /// <summary>
    /// Version that is based off of a p4 label.
    /// </summary>
    public class LabelledVersion : Version, ILabelledVersion
    {
        #region Properties
        /// <summary>
        /// Name of the label.
        /// </summary>
        public String LabelName { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public LabelledVersion(String labelName)
            : base()
        {
            LabelName = labelName;

            // Create the regex that should match the label.
            // Matches the number at the end of a label in the Major.Minor.Revision format.
            Regex labelVersionRegex =
                new Regex(@"(?<major>\d+)\.?(?<minor>\d?)\.?(?<revision>\d?)$");
            Match match = labelVersionRegex.Match(labelName);

            // Major version should always be present
            if (!match.Success || !match.Groups["major"].Success)
            {
                Debug.Assert(false, "Major revision not present.");
                throw new ArgumentException(String.Format("Label name '{0}' doesn't contain the major version number.", labelName));
            }
            Major = UInt32.Parse(match.Groups["major"].Value);

            // Minor version is optional
            if (match.Groups["minor"].Success && !String.IsNullOrEmpty(match.Groups["minor"].Value))
            {
                Minor = UInt32.Parse(match.Groups["minor"].Value);

                if (match.Groups["revision"].Success && !String.IsNullOrEmpty(match.Groups["revision"].Value))
                {
                    Revision = UInt32.Parse(match.Groups["revision"].Value);
                }
            }
        }
        #endregion // Constructor(s)
    } // LabelledVersion
}
