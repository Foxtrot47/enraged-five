﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Configuration
{

    /// <summary>
    /// Force flags configuration data.
    /// </summary>
    public interface IForceFlags
    {
        /// <summary>
        /// Tells tools whether all data content is new (e.g. "New DLC" for maps).
        /// </summary>
        bool AllNewContent { get; }
    }

} // RSG.Base.Configuration namespace
