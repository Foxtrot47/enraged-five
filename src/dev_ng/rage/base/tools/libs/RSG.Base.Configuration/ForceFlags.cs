﻿using System;
using System.Linq;
using System.Xml.Linq;

namespace RSG.Base.Configuration
{

    /// <summary>
    /// Force flags configuration data.
    /// </summary>
    internal class ForceFlags : IForceFlags
    {
        #region Properties
        /// <summary>
        /// Tells tools whether all data content is new (e.g. "New DLC" for maps).
        /// </summary>
        public bool AllNewContent 
        { 
            get; 
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor (for compatibility where XML data doesn't exist).
        /// </summary>
        /// <param name="project"></param>
        public ForceFlags(IProject project)
        {
            this.AllNewContent = false;
        }

        /// <summary>
        /// Constructor (overriding map force flag).
        /// </summary>
        /// <param name="project"></param>
        /// <param name="allNewContent"></param>
        public ForceFlags(IProject project, bool allNewContent)
            : this(project)
        {
            this.AllNewContent = allNewContent;
        }

        /// <summary>
        /// Constructor (from XML data).
        /// </summary>
        /// <param name="project"></param>
        /// <param name="xmlElem"></param>
        public ForceFlags(IProject project, XElement xmlElem)
            : this(project)
        {
            XElement xmlAllNewContent = xmlElem.Element("AllNewContent");
            if (null != xmlAllNewContent)
            {
                bool allNewContent = false;
                if (bool.TryParse(xmlAllNewContent.Value, out allNewContent))
                    this.AllNewContent = allNewContent;
            }
        }
        #endregion // Constructor(s)
    }

} // RSG.Base.Configuration namespace
