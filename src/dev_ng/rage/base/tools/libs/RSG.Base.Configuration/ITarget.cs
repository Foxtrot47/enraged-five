﻿using System;

namespace RSG.Base.Configuration
{

    /// <summary>
    /// Target abstraction; representing a target platform defining the 
    /// architecture and build directory path.
    /// </summary>
    public interface ITarget : IHasEnvironment
    {
        #region Properties
        /// <summary>
        /// Associated Branch object.
        /// </summary>
        IBranch Branch { get; }

        /// <summary>
        /// Target builds enabled locally?
        /// </summary>
        bool Enabled { get; set; }

        /// <summary>
        /// Associated platform key.
        /// </summary>
        RSG.Platform.Platform Platform { get; }

        /// <summary>
        /// Path to platform build files.
        /// </summary>
        String ResourcePath { get; }
        
        /// <summary>
        /// Path for target's shaders. 
        /// </summary>
        String ShaderPath { get; }

        /// <summary>
        /// Shader file extension.
        /// </summary>
        String ShaderExtension { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Import settings into an IEnvironment object.
        /// </summary>
        /// <param name="environment"></param>
        void Import(IEnvironment environment);

        /// <summary>
        /// Return Ragebuilder absolute executable path for a particular target.
        /// </summary>
        /// <returns></returns>
        String GetRagebuilderExecutable();

        /// <summary>
        /// Return Ragebuilder XGE conversion arguments for a particular target.
        /// </summary>
        /// <returns></returns>
        String GetRagebuilderConvertXGEArguments();

        /// <summary>
        /// Return Ragebuilder XGE conversion script argument.
        /// </summary>
        /// <returns></returns>
        String GetRagebuilderConvertXGEScript();

        /// <summary>
        /// Return Ragebuilder local conversion arguments for a particular target.
        /// </summary>
        /// <returns></returns>
        String GetRagebuilderConvertLocalArguments();

        /// <summary>
        /// PostInitialisation flush, for $(core) based keys handling.
        /// </summary>
        void PostEnvironmentInit();
        #endregion // Methods
    }

} // RSG.Base.Configuration namespace
