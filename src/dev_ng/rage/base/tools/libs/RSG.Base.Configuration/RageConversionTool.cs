﻿using System;
using System.IO;
using System.Xml.Linq;
using RSG.Base.Logging;

namespace RSG.Base.Configuration
{
    /// <summary>
    /// RAGE/Ragebuilder platform conversion tool.
    /// </summary>
    internal class RageConversionTool : IPlatformConversionTool
    {
        private const string ATTR_PLATFORM = "platform";
        private const string XmlExecutable = "Executable";
        private const string XmlArguments = "Arguments";

        #region Properties
        /// <summary>
        /// Associated platform for this conversion tool.
        /// </summary>
        public Platform.Platform Platform 
        { 
            get;
            private set;
        }

        /// <summary>
        /// Associated branch for this conversion tool.
        /// </summary>
        public IBranch Branch
        {
            get;
            private set;
        }

        /// <summary>
        /// Absolute path to conversion tool executable (or script).
        /// </summary>
        public String ExecutablePath
        {
            get { return Path.GetFullPath(this.Branch.Environment.Subst(m_sExecutablePath)); }
            private set { m_sExecutablePath = value; }
        }
        private String m_sExecutablePath;

        /// <summary>
        /// Platform and tool global options (independent of what is being 
        /// converted).
        /// </summary>
        public String Arguments
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="platform"></param>
        /// <param name="exe"></param>
        /// <param name="args"></param>
        public RageConversionTool(Branch branch, Platform.Platform platform, String exe, String args)
        {
            this.Branch = branch;
            this.Platform = platform;
            this.ExecutablePath = exe;
            this.Arguments = args;
        }

        /// <summary>
        /// Constructor; from XElement.
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="xmlElem"></param>
        internal RageConversionTool(IBranch branch, XElement xmlElem)
        {
            this.Branch = branch;
            ParseRagebuilder(xmlElem);
        }
        #endregion // Constructor(s)

        #region ISerialisable Interface Methods
        /// <summary>
        /// Serialise data to an XML element.
        /// </summary>
        /// <returns></returns>
        public XElement ToXml()
        {
            throw new NotImplementedException();
        }
        #endregion // ISerialisable Interface Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ragebuilderArgs"></param>
        private void ParseRagebuilder(XElement ragebuilderArgs)
        {
            string platform = ragebuilderArgs.Attribute(ATTR_PLATFORM).Value;
            Platform = RSG.Platform.PlatformUtils.PlatformFromString(platform);
            
            XElement executable = ragebuilderArgs.Element(XmlExecutable);
            if (executable == null || string.IsNullOrEmpty(executable.Value))
            {
                throw new ConfigurationException(string.Format("Ragebuilder Arguments: Executable path not found for platform {0}.", platform));
            }
            ExecutablePath = executable.Value;


            XElement arguments = ragebuilderArgs.Element(XmlArguments);
            if (arguments == null)
            {
                throw new ConfigurationException(string.Format("Ragebuilder Arguments: Arguments not found for platform {0}.", platform));
            }
            Arguments = arguments.Value;
        }
        #endregion // Private Methods
    }

} // RSG.Base.Configuration namespace
