﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RSG.Base.Configuration.Bugstar
{

    /// <summary>
    /// Bugstar configuration interface.
    /// </summary>
    public interface IBugstarConfig
    {
        #region Properties
        /// <summary>
        /// Bugstar configuration XML file.
        /// </summary>
        String Filename { get; }

        /// <summary>
        /// Bugstar Project identifier.
        /// </summary>
        uint ProjectId { get; }

        /// <summary>
        /// Bugstar REST service Uri.
        /// </summary>
        Uri RESTService { get; }

        /// <summary>
        /// Bugstar attachment service Uri.
        /// </summary>
        Uri AttachmentService { get; }

        /// <summary>
        /// Bugstar user domain.
        /// </summary>
        String UserDomain { get; }

        /// <summary>
        /// Bugstar external domain (for service accounts, no AD authentication).
        /// </summary>
        String ExternalDomain { get; }

        /// <summary>
        /// Bugstar project levels and their identifiers.
        /// </summary>
        IEnumerable<KeyValuePair<String, uint>> Levels { get; }

        /// <summary>
        /// Read-only account username.
        /// </summary>
        String ReadOnlyUsername { get; }

        /// <summary>
        /// Read-only account password.
        /// </summary>
        String ReadOnlyPassword { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Retrieves the bugstar id for the passed in level name.
        /// </summary>
        /// <param name="levelName"></param>
        /// <returns></returns>
        uint GetLevelId(String levelName);

        /// <summary>
        /// Attempts to retrieve the bugstar id for the passed in level name, returning whether the operation
        /// was successful or not.
        /// </summary>
        /// <param name="levelName"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        bool TryGetLevelId(String levelName, out uint id);
        #endregion // Methods
    }

} // RSG.Base.Configuration.Bugstar namespace
