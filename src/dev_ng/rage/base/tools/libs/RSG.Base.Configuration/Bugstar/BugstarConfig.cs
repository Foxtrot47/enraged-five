﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Xml.Linq;
using System.Xml.XPath;

namespace RSG.Base.Configuration.Bugstar
{
	/// <summary>
	/// Bugstar configuration object.
	/// </summary>
	internal class BugstarConfig : IBugstarConfig
    {
        #region Constants
        private static readonly String EXTERNAL_DOMAIN = String.Empty;
        #endregion // Constants

        #region Fields
        /// <summary>
        /// Private field for the <see cref="Filename"/> property.
        /// </summary>
        private readonly String _filename;
        #endregion // Fields

        #region Properties
        /// <summary>
		/// Bugstar configuration XML filename.
		/// </summary>
        public String Filename
        {
            get { return _filename; }
		}

		/// <summary>
		/// Bugstar Project identifier.
		/// </summary>
		public uint ProjectId 
		{ 
			get; 
			private set;
		}

		/// <summary>
		/// Bugstar REST service Uri.
		/// </summary>
		public Uri RESTService 
		{ 
			get; 
			private set;
		}

		/// <summary>
		/// Bugstar attachment service Uri.
		/// </summary>
		public Uri AttachmentService 
		{ 
			get; 
			private set;
		}

		/// <summary>
		/// Bugstar user domain.
		/// </summary>
		public String UserDomain 
		{ 
			get; 
			private set;
		}

        /// <summary>
        /// Bugstar external domain (for service accounts, no AD authentication).
        /// </summary>
        public String ExternalDomain 
        {
            get { return (EXTERNAL_DOMAIN); } 
        }

		/// <summary>
		/// Bugstar project levels and their identifiers.
		/// </summary>
		public IEnumerable<KeyValuePair<String, uint>> Levels 
		{ 
			get; 
			private set;
		}

		/// <summary>
		/// Read-only account username.
		/// </summary>
		public String ReadOnlyUsername 
		{ 
			get; 
			private set;
		}

		/// <summary>
		/// Read-only account password.
		/// </summary>
		public String ReadOnlyPassword 
		{ 
			get; 
			private set;
		}
		#endregion // Properties

		#region Constructor(s)
		/// <summary>
		/// Constructor.
		/// </summary>
		internal BugstarConfig(IProject project)
		{
            _filename = Path.Combine(project.Config.ToolsConfig, "bugstar.xml");
            if (File.Exists(this.Filename))
            {
                XDocument xmlDoc = XDocument.Load(this.Filename);
                XElement xmlBugstarElem = xmlDoc.XPathSelectElement("/Bugstar");
                Parse(xmlBugstarElem);
            }
            else
            {
                throw (new ConfigurationException("Bugstar configuration data does not exist.",
                    new FileNotFoundException("Bugstar configation data does not exist.", _filename)));
            }
		}

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="config"></param>
        internal BugstarConfig(IConfig config)
        {
            _filename = Path.Combine(config.ToolsConfig, "bugstar.xml");
            if (File.Exists(this.Filename))
            {
                XDocument xmlDoc = XDocument.Load(this.Filename);
                XElement xmlBugstarElem = xmlDoc.XPathSelectElement("/Bugstar");
                Parse(xmlBugstarElem);
            }
            else
            {
                throw new ConfigurationException("Global Bugstar xml configuration does not exist.")
                    {
                        Filename = this.Filename
                    };
            }
        }
		#endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// Retrieves the bugstar id for the passed in level name.
        /// </summary>
        /// <param name="levelName"></param>
        /// <returns></returns>
        public uint GetLevelId(String levelName)
        {
            uint id;
            if (!TryGetLevelId(levelName, out id))
            {
                throw new KeyNotFoundException(String.Format("A level with name '{0}' doesn't exist in the bugstar config.", levelName));
            }

            return id;
        }

        /// <summary>
        /// Attempts to retrieve the bugstar id for the passed in level name, returning whether the operation
        /// was successful or not.
        /// </summary>
        /// <param name="levelName"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool TryGetLevelId(String levelName, out uint id)
        {
            foreach (KeyValuePair<String, uint> pair in Levels)
            {
                if (pair.Key == levelName)
                {
                    id = pair.Value;
                    return true;
                }
            }

            id = 0;
            return false;
        }
        #endregion // Public Methods

		#region Private Methods
        /// <summary>
        /// Parse Bugstar configuration data (in bugstar.xml).
        /// </summary>
        /// <param name="xmlBugstarElem"></param>
        private void Parse(XElement xmlBugstarElem)
        {
            XElement xmlIdElem = xmlBugstarElem.Element("ProjectId");
            uint id = 0;
            if (null != xmlIdElem)
                uint.TryParse(xmlIdElem.Value, out id);
            this.ProjectId = id;

            //XElement xmlServerElem = xmlBugstarElem.Element("Server");
            //XElement xmlPortElem = xmlBugstarElem.Element("Port");
            //String server = String.Empty;
            //int port = 0;
            //if (null != xmlServerElem && null != xmlPortElem)
            //{
            //    UriBuilder ub = new UriBuilder();
            //    int.TryParse(xmlPortElem.Value, out port);
            //    server = xmlServerElem.Value;
            //    ub.Host = server;
            //    ub.Port = port;
            //    ub.Scheme = "https";
            //    this.Server = ub.Uri;
            //}

            XElement xmlServicesElem = xmlBugstarElem.Element("Services");
            XElement xmlRESTServiceElem = xmlServicesElem.Element("RestService");
            if (null != xmlRESTServiceElem)
            {
                //UriBuilder ub = new UriBuilder();
                //ub.Host = server;
                //ub.Port = port;
                //ub.Path = xmlRESTServiceElem.Value;
                //ub.Scheme = "https";
                this.RESTService = new Uri(xmlRESTServiceElem.Value);// ub.Uri;
            }
            XElement xmlAttachmentServiceElem = xmlServicesElem.Element("AttachmentService");
            if (null != xmlAttachmentServiceElem)
            {
                //UriBuilder ub = new UriBuilder();
                //ub.Host = server;
                //ub.Port = port;
                //ub.Path = xmlAttachmentServiceElem.Value;
                //ub.Scheme = "https";
                this.AttachmentService = new Uri(xmlAttachmentServiceElem.Value);// ub.Uri;
            }

            XElement xmlUserDomainElem = xmlBugstarElem.Element("UserDomain");
            if (null != xmlUserDomainElem)
                this.UserDomain = xmlUserDomainElem.Value;

            XElement xmlServiceAccountElem = xmlBugstarElem.Element("ServiceAccount");
            XElement xmlUsernameElem = xmlServiceAccountElem.Element("Username");
            if (null != xmlUsernameElem)
                this.ReadOnlyUsername = xmlUsernameElem.Value;

            XElement xmlPasswordElem = xmlServiceAccountElem.Element("Password");
            if (null != xmlPasswordElem)
                this.ReadOnlyPassword = xmlPasswordElem.Value;

            List<KeyValuePair<String, uint>> levels = new List<KeyValuePair<String, uint>>();
            XElement xmlLevelsElem = xmlBugstarElem.Element("Levels");
            IEnumerable<XElement> xmlLevelElems = xmlLevelsElem.XPathSelectElements("Level");
            foreach (XElement xmlLevelElem in xmlLevelElems)
            {
                XElement xmlLevelName = xmlLevelElem.Element("Name");
                XElement xmlId = xmlLevelElem.Element("LevelId");

                if (null != xmlLevelName && null != xmlId)
                {
                    uint lid = 0;
                    uint.TryParse(xmlId.Value, out lid);
                    levels.Add(new KeyValuePair<String, uint>(xmlLevelName.Value, lid));
                }
            }
            this.Levels = levels;
        }
		#endregion // Private Methods
	}

} // RSG.Base.Configuration.Bugstar namespace
