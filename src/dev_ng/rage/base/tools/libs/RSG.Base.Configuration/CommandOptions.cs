﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.OS;

namespace RSG.Base.Configuration
{

    /// <summary>
    /// Command-line options service; handles parsing common command line 
    /// options that are supported throughout the pipeline.
    /// </summary>
    /// This replaces the RSG.Pipeline.Services.CommandOptions class.
    /// 
    public class CommandOptions
    {
        #region Constants
        private static readonly String OPT_DEBUG = "debug";
        private static readonly String OPT_NOPOPUPS = "nopopups";
        private static readonly String OPT_HELP = "help";
        private static readonly String OPT_PROJECT = "project";
        private static readonly String OPT_BRANCH = "branch";
        private static readonly String OPT_DLC = "dlc";
        private static readonly String OPT_VERBOSE = "verbose";

        private static readonly String MSG_TITLE = "Tools Version Problem";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Config data.
        /// </summary>
        public IConfig Config
        {
            get;
            private set;
        }

        /// <summary>
        /// Active core project.
        /// </summary>
        public IProject CoreProject
        {
            get;
            private set;
        }

        /// <summary>
        /// Active project.
        /// </summary>
        public IProject Project
        {
            get;
            set;
        }

        /// <summary>
        /// Set branch.
        /// </summary>
        public IBranch Branch
        {
            get;
            set;
        }

        /// <summary>
        /// Set when user specifies help option.
        /// </summary>
        public bool ShowHelp
        {
            get;
            private set;
        }

        /// <summary>
        /// Set when user specifies no popups option.
        /// </summary>
        public bool NoPopups
        {
            get;
            set;
        }

        /// <summary>
        /// Enables verbose log output (enables debug log messages in release builds).
        /// </summary>
        public bool Verbose
        {
            get;
            private set;
        }

        /// <summary>
        /// Return option.
        /// </summary>
        /// <param name="opt"></param>
        /// <returns></returns>
        public Object this[String opt]
        {
            get { return this.m_Options[opt]; }
        }

        /// <summary>
        /// Trailing string arguments.
        /// </summary>
        public IEnumerable<String> TrailingArguments
        {
            get { return this.m_Options.TrailingOptions; }
        }

        /// <summary>
        /// Default option arguments.
        /// </summary>
        public static IEnumerable<LongOption> DefaultArguments
        {
            get { return (DEFAULT_ARGS); }
        }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Default option arguments; merged with your own array if your
        /// handling options.
        /// </summary>
        private static LongOption[] DEFAULT_ARGS = new LongOption[] {
            new LongOption(OPT_DEBUG, LongOption.ArgType.None,
                "Start debugger session; dialog prompts for debugger to use."),
            new LongOption(OPT_NOPOPUPS, LongOption.ArgType.None, 
                "Hide all popup dialogs; taking default options."),
            new LongOption(OPT_HELP, LongOption.ArgType.None, 
                "Display help information and exit."),
            new LongOption(OPT_PROJECT, LongOption.ArgType.Required,
                "Project to use (e.g. gta5, rdr3, gta5_liberty."),
            new LongOption(OPT_BRANCH, LongOption.ArgType.Required, 
                "Project branch to use (e.g. dev, release)."),
            new LongOption(OPT_DLC, LongOption.ArgType.Required, 
                "Project DLC to use (e.g. dlc_w_ar_heavyrifle)."),
            new LongOption(OPT_VERBOSE, LongOption.ArgType.None, 
                "Use debug logging.")
        };

        private Getopt m_Options;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor; default command line and options.
        /// </summary>
        [Obsolete("Use the version where you pass through the command line arguments. This won't work with applications that are started as part of an AppDomain in another application.")]
        public CommandOptions()
            : this(System.Environment.GetCommandLineArgs(), new LongOption[]{})
        {
        }

        /// <summary>
        /// Constructor; default command line with custom options.
        /// </summary>
        /// <param name="options"></param>
        [Obsolete("Use the version where you pass through the command line arguments. This won't work with applications that are started as part of an AppDomain in another application.")]
        public CommandOptions(LongOption[] options)
            : this(System.Environment.GetCommandLineArgs(), options)
        {
        }

        /// <summary>
        /// Constructor; command line and custom options specified.
        /// </summary>
        /// <param name="arguments"></param>
        /// <param name="options"></param>
        public CommandOptions(String[] arguments, LongOption[] options)
        {
            Init(arguments, options);
            Setup();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Return whether we contain a specified option.
        /// </summary>
        /// <param name="opt"></param>
        /// <returns></returns>
        public bool ContainsOption(String opt)
        {
            return (this.m_Options.HasOption(opt));
        }

        /// <summary>
        /// Return usage string from our command options parser.
        /// </summary>
        public String GetUsage()
        {
            return (this.m_Options.Usage());
        }

        /// <summary>
        /// Return usage string from our command options parser (with trailing
        /// argument description).
        /// </summary>
        /// <param name="trailingArgName"></param>
        /// <param name="trailingArgDescription"></param>
        /// <returns></returns>
        public String GetUsage(String trailingArgName, String trailingArgDescription)
        {
            return (this.m_Options.Usage(trailingArgName, trailingArgDescription));
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// One off initialisation.
        /// </summary>
        /// <param name="arguments"></param>
        /// <param name="options"></param>
        private void Init(String[] arguments, LongOption[] options)
        {
            List<LongOption> opts = new List<LongOption>();
            opts.AddRange(options);
            opts.AddRange(DEFAULT_ARGS);

            m_Options = new RSG.Base.OS.Getopt(arguments, opts.ToArray());
            this.ShowHelp = this.ContainsOption(OPT_HELP);
            this.NoPopups = this.ContainsOption(OPT_NOPOPUPS);
            this.Verbose = this.ContainsOption(OPT_VERBOSE);
            if (this.ContainsOption(OPT_DEBUG) && !Debugger.IsAttached)
                Debugger.Launch();
            if (this.Verbose)
                LogFactory.SetLogLevel(LogLevel.Debug);
        }

        /// <summary>
        /// Setup; creating configuration object and setting core project etc.
        /// </summary>
        private void Setup()
        {
            try
            {
                String projectKey = this.ContainsOption(OPT_PROJECT) ? (String)this[OPT_PROJECT] : String.Empty;
                String dlcKey = this.ContainsOption(OPT_DLC) ? (String)this[OPT_DLC] : String.Empty;
                this.Config = ConfigFactory.CreateConfig(dlcKey);

                this.CoreProject = this.Config.CoreProject;
                this.Project = this.Config.Project;
                if (this.ContainsOption(OPT_BRANCH))
                {
                    String branchKey = (String)this[OPT_BRANCH];
                    Debug.Assert(this.Config.Project.Branches.ContainsKey(branchKey),
                        "Branch key not found.",
                        "Branch key '{0}' not found; default branch {1} will be used.",
                        branchKey, this.Config.Project.DefaultBranchName);
                    if (this.Config.Project.Branches.ContainsKey(branchKey))
                        this.Branch = this.Config.Project.Branches[branchKey];
                    else
                    {
                        String branchName = this.Config.CoreProject.DefaultBranchName;
                        if (this.Config.Project.Branches.ContainsKey(branchName))
                            this.Branch = this.Config.Project.Branches[branchName];
                        else
                            this.Branch = this.Config.Project.DefaultBranch;
                    }
                }
                else
                {
                    String branchName = this.Config.CoreProject.DefaultBranchName;
                    if (this.Config.Project.Branches.ContainsKey(branchName))
                        this.Branch = this.Config.Project.Branches[branchName];
                    else
                        this.Branch = this.Config.Project.DefaultBranch;
                }
            }
            catch (ConfigurationVersionException ex)
            {
                RSG.Base.Configuration.Config.Log.Exception(ex,
                    "Configuration version error: run tools installer.  Installed version: {0}, expected version: {1}.",
                    ex.ActualVersion, ex.ExpectedVersion);
                if (!this.NoPopups)
                {
                    String msg = String.Format("An important update has been made to the tool chain.  Install version: {0}, expected version: {1}.{2}{2}Sync latest or labelled tools, run {3} and then restart the application.",
                        ex.ActualVersion, ex.ExpectedVersion, System.Environment.NewLine,
                        Path.Combine(System.Environment.GetEnvironmentVariable("RS_TOOLSROOT"), "install.bat"));
                    MessageBox.Show(msg, MSG_TITLE, MessageBoxButton.OK, MessageBoxImage.Error);
                }
                System.Environment.Exit(1);
            }
            catch (ConfigurationException ex)
            {
                RSG.Base.Configuration.Config.Log.ToolException(ex, "Configuration parse error.");
                if (!this.NoPopups)
                {
                    String msg = String.Format("There was an error initialising configuration data.{0}{0}Sync latest or labelled tools and restart the application.",
                        System.Environment.NewLine);
                    MessageBox.Show(msg, MSG_TITLE, MessageBoxButton.OK, MessageBoxImage.Error);
                }
                System.Environment.Exit(1);
            }
        }
        #endregion // Private Methods
    }

} // RSG.Base.Configuration namespace
