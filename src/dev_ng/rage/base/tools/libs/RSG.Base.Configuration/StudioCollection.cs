﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using RSG.Base.Extensions;
using RSG.Base.Net;

namespace RSG.Base.Configuration
{
    /// <summary>
    /// Studio object collection.
    /// </summary>
    [Obsolete("We use read-only enumerable of IStudio in IConfig2.")]
    public class StudioCollection :
        ICollection<IStudio>
    {
        #region Properties
        /// <summary>
        /// The collection of studios that are currently
        /// loaded in this dictionary
        /// </summary>
        public ICollection<IStudio> Studios
        {
            get;
            protected set;
        }
        
        /// <summary>
        /// The studio user is currently residing in.
        /// </summary>
        public IStudio ThisStudio
        {
            get
            {
                if(null == m_ThisStudio)
                    m_ThisStudio = GetThisStudio();

                return m_ThisStudio;
            }
        }
        IStudio m_ThisStudio;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public StudioCollection()
        {
            Studios = new List<IStudio>();
        }
        #endregion // Constructor(s)

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private IStudio GetThisStudio()
        {
            IPHostEntry host;
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (AddressFamily.InterNetwork != ip.AddressFamily)
                    continue;

                // Loop through studio objects finding right studio.
                foreach (IStudio studio in Studios)
                {
                    KeyValuePair<IPAddress, IPAddress> subnetAndMask =
                        SubnetMask.Create(studio.SubnetRange);

                    if (ip.IsInSameSubnet(subnetAndMask.Key, subnetAndMask.Value))
                        return (studio);
                }            
            }
            
            return (null);
        }
        #endregion

        #region IEnumerable<IStudio> Implementation
        /// <summary>
        /// Loops through the studios and returns each one in turn
        /// </summary>
        public IEnumerator<IStudio> GetEnumerator()
        {
            foreach (IStudio studio in this.Studios)
            {
                yield return studio;
            }
        }

        /// <summary>
        /// Returns the Enumerator for this object
        /// </summary>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return (GetEnumerator());
        }
        #endregion // IEnumerable<IStudio> Implementation

        #region ICollection<IStudio> Implementation
        #region ICollection<IStudio> Properties
        /// <summary>
        /// Returns whether the collection is read-only
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Return number of items in the set.
        /// </summary>
        public int Count
        {
            get
            {
                return Studios.Count;
            }
        }
        #endregion // ICollection<IStudio> Properties

        #region ICollection<IStudio> Methods
        /// <summary>
        /// Add a studio to the collection.
        /// </summary>
        /// <param name="item"></param>
        public void Add(IStudio item)
        {
            Studios.Add(item);
        }

        /// <summary>
        /// Removes all studios from the collection.
        /// </summary>
        public void Clear()
        {
            Studios.Clear();
        }

        /// <summary>
        /// Determine whether the collection contains a specific studio
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Contains(IStudio item)
        {
            return Studios.Contains(item);
        }

        /// <summary>
        /// Copy items in the collection to an array, starting at a particular index.
        /// </summary>
        /// <param name="output"></param>
        /// <param name="index"></param>
        public void CopyTo(IStudio[] output, int index)
        {
            Studios.CopyTo(output, index);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        public bool Remove(IStudio item)
        {
            return Studios.Remove(item);
        }
        #endregion // ICollection<IStudio> Methods
        #endregion // ICollection<IStudio> Implementation
    }

} // RSG.Base.Configuration namespace
