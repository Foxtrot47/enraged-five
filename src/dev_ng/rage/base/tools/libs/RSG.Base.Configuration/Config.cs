﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Logging;
using RSG.SourceControl.Perforce;

namespace RSG.Base.Configuration
{

    /// <summary>
    /// Entry Configuration class for system-wide globals for current tools 
    /// branch.  This includes the project context for the current tools branch.
    /// </summary>
    /// The globals just come from the user-environment.  Rather than 
    /// recalculate then here like we used to do.  To switch project tools you 
    /// need to re-run the installer anyways.
    /// 
    internal class Config : 
        IConfig,
        Installer.IWritableConfig
    {
        #region Constants
        internal static readonly int CONFIG_VERSION = 2;
        internal static readonly String ELEM_CONFIG = "config";
        internal static readonly String ELEM_LOCAL = "local";
        internal static readonly String ELEM_PROJECTS = "projects";
        internal static readonly String ELEM_PLATFORMS = "platforms";
        internal static readonly String ELEM_SCM = "scm";
        internal static readonly String ELEM_USERTYPES = "usertypes";
        internal static readonly String ELEM_STUDIOS = "studios";
        internal static readonly String ELEM_LOGGING = "logging";
        internal static readonly String ELEM_INSTALL = "install";
        internal static readonly String ELEM_UNINSTALL = "uninstall";
        internal static readonly String ELEM_VERSION = "version";

        internal static readonly String ATTR_INSTALL_TIME = "installtime";
        internal static readonly String ATTR_VERSION = "version";
        internal static readonly String ATTR_LABEL = "label";
        internal static readonly String ATTR_BASELABEL = "baseLabel";
        internal static readonly String ATTR_USERNAME = "username";
        internal static readonly String ATTR_USER_FLAGS = "flags";
        internal static readonly String ATTR_EMAIL_ADDRESS = "emailaddress";
        internal static readonly String ATTR_SCM_TOOLCHAIN_INTEGRATION = "toolchain_integration";
        #endregion // Constants

        #region Properties
        #region Core Tools Variables
        /// <summary>
        /// Tools root directory.
        /// </summary>
        public String ToolsRoot
        {
            get { return Path.GetFullPath(m_sToolsRoot); }
            private set { m_sToolsRoot = value; }
        }
        private String m_sToolsRoot;

        /// <summary>
        /// Tools root drive letter.
        /// </summary>
        public String ToolsDrive
        {
            get { return m_sToolsDrive; }
            private set { m_sToolsDrive = value; }
        }
        private String m_sToolsDrive;

        /// <summary>
        /// Tools binary directory.
        /// </summary>
        public String ToolsBin
        {
            get { return Path.GetFullPath(m_sToolsBin); }
            private set { m_sToolsBin = value; }
        }
        private String m_sToolsBin;

        /// <summary>
        /// Path to Tools Installer.
        /// </summary>
        public String ToolsInstaller
        {
            get { return Path.GetFullPath(m_sToolsInstaller); }
            private set { m_sToolsInstaller = value; }
        }
        private String m_sToolsInstaller;

        /// <summary>
        /// Tools library files directory.
        /// </summary>
        public String ToolsLib
        {
            get { return Path.GetFullPath(m_sToolsLib); }
            private set { m_sToolsLib = value; }
        }
        private String m_sToolsLib;

        /// <summary>
        /// Tools Ruby interpreter absolute path.
        /// </summary>
        public String ToolsRuby
        {
            get { return Path.GetFullPath(m_sToolsRuby); }
            private set {m_sToolsRuby = value; }
        }
        private String m_sToolsRuby;

        /// <summary>
        /// Tools IronRuby interpreter absolute path.
        /// </summary>
        public String ToolsIronRuby
        {
            get { return Path.GetFullPath(m_sToolsIronRuby); }
            private set {m_sToolsIronRuby = value; }
        }
        private String m_sToolsIronRuby;

        /// <summary>
        /// Tools temporary files absolute path.
        /// </summary>
        public String ToolsTemp
        {
            get { return Path.GetFullPath(m_sToolsTemp); }
            private set { m_sToolsTemp = value; }
        }
        private String m_sToolsTemp;

        /// <summary>
        /// Tools temporary files absolute path.
        /// </summary>
        public String ToolsLogs
        {
            get { return Path.GetFullPath(m_sToolsLogs); }
            private set { m_sToolsLogs = value; }
        }
        private String m_sToolsLogs;

        /// <summary>
        /// Tools configuration data absolute path.
        /// </summary>
        public String ToolsConfig
        {
            get { return Path.GetFullPath(m_sToolsConfig); }
            private set { m_sToolsConfig = value; }
        }
        private String m_sToolsConfig;
        #endregion // Core Tools Variables

        /// <summary>
        /// XML configuration filename.
        /// </summary>
        public String Filename
        {
            get { return _filename; }
        }
        private String _filename;

        /// <summary>
        /// XML configuration file version.
        /// </summary>
        [Obsolete("Use 'BaseToolsVersion'/'ToolsVersion' properties instead.")]
        public int ConfigVersion
        {
            get;
            private set;
        }

        /// <summary>
        /// Tools version information.
        /// </summary>
        public IToolsVersion Version
        {
            get;
            private set;
        }

        /// <summary>
        /// Tools Perforce label state.
        /// </summary>
        public PerforceLabelState ToolsPerforceState
        {
            get;
            private set;
        }

        /// <summary>
        /// Active project object (can be DLC project).
        /// </summary>
        public IProject Project
        {
            get;
            private set;
        }

        /// <summary>
        /// Core project (never DLC).
        /// </summary>
        public IProject CoreProject
        {
            get;
            private set;
        }

        /// <summary>
        /// Episodic projects; piggybacking these tools.
        /// </summary>
        public IDictionary<String, IProject> DLCProjects
        {
            get;
            private set;
        }

        /// <summary>
        /// Available user types collection.
        /// </summary>
        public ICollection<IUsertype> Usertypes
        {
            get;
            private set;
        }

        /// <summary>
        /// Available studio collection.
        /// </summary>
        public StudioCollection Studios
        {
            get;
            private set;
        }

        #region User Local Data (Set by Installer)
        /// <summary>
        /// Last install time.
        /// </summary>
        public DateTime InstallTime
        {
            get;
            set;
        }

        /// <summary>
        /// User's username.
        /// </summary>
        public String Username
        {
            get;
            set;
        }

        /// <summary>
        /// User's email address.
        /// </summary>
        public String EmailAddress
        {
            get;
            set;
        }

        /// <summary>
        /// User's type flags.
        /// </summary>
        public UInt32 Usertype
        {
            get;
            set;
        }

        /// <summary>
        /// Version control enabled flag.
        /// </summary>
        public bool VersionControlEnabled 
        { 
            get;
            set;
        }

        /// <summary>
        /// User version control settings.
        /// </summary>
        public IEnumerable<IVersionControlSettings> VersionControlSettings 
        { 
            get;
            set;
        }
        #endregion // User Local Data (Set by Installer)

        /// <summary>
        /// Main configuration environment.
        /// </summary>
        public IEnvironment Environment
        {
            get;
            private set;
        }

        /// <summary>
        /// Property to determine whether internal project XML version check
        /// happens (its sometimes required to be switched off).
        /// </summary>
        private bool SkipVersionCheck
        {
            get;
            set;
        }
        #endregion // Properties

        #region Static Properties
        /// <summary>
        /// Configuration loading/saving log.
        /// </summary>
        internal static RSG.Base.Logging.Universal.IUniversalLog Log
        {
            get;
            private set;
        }
        #endregion // Static Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="dlcKey"></param>
        /// <param name="skip_version_check"></param>
        /// 
        public Config(String dlcKey, bool skip_version_check = false)
        {
            this.SkipVersionCheck = skip_version_check;
            if (this.SkipVersionCheck)
                Log.Message("Skipping configuration version check.");

            this.ToolsRoot = System.Environment.GetEnvironmentVariable("RS_TOOLSROOT");
            if (!Path.IsPathRooted(this.ToolsRoot))
            {
                Debug.Fail(String.Format("RS_TOOLSROOT ({0}) has no drive specified.  Misconfigured tools.", this.ToolsRoot));
                throw new ConfigurationException(String.Format("RS_TOOLSROOT ({0}) has no drive specified.  Misconfigured tools.", this.ToolsRoot))
                    {
                        Filename = this.Filename
                    };
            }

            // Setup basic properties.
            _filename = GetConfigFilename(this.ToolsRoot);
            this.ToolsDrive = Path.GetPathRoot(this.ToolsRoot);
            this.ToolsBin = GetEnvVarOrDefault("RS_TOOLSBIN", Path.Combine(this.ToolsRoot, "bin"));
            this.ToolsInstaller = Path.Combine(this.ToolsRoot, "install.bat");
            this.ToolsConfig = GetEnvVarOrDefault("RS_TOOLSCONFIG", Path.Combine(this.ToolsRoot, "etc"));
            this.ToolsLib = GetEnvVarOrDefault("RS_TOOLSLIB", Path.Combine(this.ToolsRoot, "lib"));
            this.ToolsRuby = GetEnvVarOrDefault("RS_TOOLSRUBY", Path.Combine(this.ToolsBin, "ruby", "bin", "ruby.exe"));
            this.ToolsIronRuby = GetEnvVarOrDefault("RS_TOOLSIR", Path.Combine(this.ToolsBin, "ironruby", "bin", "ir64.exe"));
            this.ToolsTemp = GetEnvVarOrDefault("RS_TOOLSTEMP", Path.Combine(this.ToolsRoot, "tmp"));
            this.ToolsLogs = Path.Combine(this.ToolsRoot, "logs");
            this.Version = new ToolsVersion(this);
            this.Environment = new Environment(this);

            // Load file.
            this.Reload(dlcKey);

            this.PostEnvironmentInit();
        }
        
        /// <summary>
        /// Static constructor; initialises the configuration log.
        /// </summary>
        static Config()
        {
            LogFactory.Initialize();
            Log = LogFactory.CreateUniversalLog("RSG.Base.Configuration");
        }
        #endregion // Constructor(s)

        #region Static Controller Methods
        /// <summary>
        /// Return global tools configuration filename.
        /// </summary>
        /// <param name="toolsRoot"></param>
        /// <returns></returns>
        public static String GetConfigFilename(String toolsRoot)
        {
            return (Path.Combine(toolsRoot, "config.xml"));
        }
        #endregion // Static Controller Methods

        #region Controller Methods
        /// <summary>
        /// Import settings into an IEnvironment object.
        /// </summary>
        /// <param name="environment"></param>
        public void Import(IEnvironment environment)
        {
            environment.Add("toolsroot", this.ToolsRoot);
            environment.Add("toolsbin", this.ToolsBin);
            environment.Add("toolsconfig", this.ToolsConfig);
            environment.Add("toolslib", this.ToolsLib);
            environment.Add("toolstemp", this.ToolsTemp);
        }

        /// <summary>
        /// Switch configuration to a keyed DLC project (null or "" to default).
        /// </summary>
        /// <param name="dlcKey"></param>
        public void SwitchDLC(String dlcKey)
        {
            // Initialise a DLC project as the main project.
            if (!String.IsNullOrWhiteSpace(dlcKey))
            {
                if (this.DLCProjects.ContainsKey(dlcKey))
                {
                    Log.Message("Setting DLC project '{0}' as our context.", dlcKey);
                    this.Project = this.DLCProjects[dlcKey];
                }
                else
                {
                    Log.Error("Failed to set DLC project '{0}' context.", dlcKey);
                    this.Project = this.CoreProject;
                }
            }
            else
            {
                Log.Message("Clearing DLC project context to core project.");
                this.Project = this.CoreProject;
            }
        }

        /// <summary>
        /// Reload all the configuration data; and all projects.
        /// </summary>
        /// <param name="dlcKey"></param>
        public void Reload(String dlcKey)
        {
            this.DLCProjects = new Dictionary<String, IProject>();
            this.Usertypes = new List<IUsertype>();
            this.Studios = new StudioCollection();
            this.VersionControlSettings = new List<IVersionControlSettings>();

            if (!File.Exists(this.Filename))
            {
                Debug.Fail(String.Format("Config XML '{0}' does not exist.", this.Filename));
                throw new ConfigurationException("Config XML does not exist.")
                    {
                        Filename = this.Filename
                    };
            }
            
            // Parse Studio.
            ParseStudioFile(Config.Log);
            ParseConfig(this.Filename, false);

            String localXml = Path.Combine(this.ToolsRoot, "local.xml");
            if (File.Exists(localXml))
            {
                ParseConfig(localXml, true);
            }

            SwitchDLC(dlcKey);

            // Needs to happen after the main config file is parsed.
            Version.Reload();
        }

        /// <summary>
        /// Determine the Perforce state for the tools directory; whether user
        /// is on the label etc.
        /// </summary>
        /// <param name="quickCheck">True indicates that it only checks the latest version
        /// against that in config.xml. False indicates that a full p4 diff is performed.</param>
        /// <returns></returns>
        public PerforceLabelState GetToolsPerforceLabelState(bool quickCheck = false)
        {
            PerforceLabelState state = PerforceLabelState.Unknown;
            using (P4 p4 = this.Project.SCMConnect())
            {
                if (quickCheck)
                {
                    state = PerforceLabelStateUtils.GetQuickLabelStatus(p4, this.Version.LocalVersion.Major, this.Version.LocalVersion.Minor,
                        this.Project.Labels[Label.ToolsIncremental], this.Project.Labels[Label.ToolsCurrent]);
                }
                else
                {
                    state = PerforceLabelStateUtils.GetLabelStatus(p4, this.ToolsRoot,
                        this.Project.Labels[Label.ToolsCurrent]);
                }
            }

            return (state);
        }

        /// <summary>
        /// Returns all projects (core + dlc)
        /// </summary>
        /// <returns></returns>
        public IDictionary<String, IProject> AllProjects()
        {
            IDictionary<String, IProject> allProjects = new Dictionary<String, IProject>();
            
            // Add the core project
            allProjects.Add("core", CoreProject);
            
            // Add all the DLC projects
            foreach (KeyValuePair<string, IProject> dlcProject in DLCProjects)
            {
                allProjects.Add(dlcProject);
            }
            
            return allProjects;
        }

        /// <summary>
        /// Save local settings (used by WritableConfig and keeps serialisation and
        /// deserialisation code in one place).
        /// </summary>
        /// <returns></returns>
        public bool SaveLocal()
        {
            String filename = Path.Combine(this.ToolsRoot, "local.xml");

            try
            {
                String filenameBackup = Path.ChangeExtension(filename, ".xml.bak");
                if (File.Exists(filename) && !File.Exists(filenameBackup))
                    File.Copy(filename, filenameBackup);

                XElement scmElem = new XElement("scm",
                    new XAttribute("toolchain_integration", this.VersionControlEnabled));
                if (this.VersionControlEnabled)
                {
                    scmElem.Add(
                        new XElement("build", 
                            new XAttribute("workspace", this.VersionControlSettings.First().Workspace),
                            new XAttribute("username", this.VersionControlSettings.First().Username),
                            new XAttribute("server", this.VersionControlSettings.First().Server)
                        ), // build
                        new XElement("rage", 
                            new XAttribute("workspace", this.VersionControlSettings.First().Workspace),
                            new XAttribute("username", this.VersionControlSettings.First().Username),
                            new XAttribute("server", this.VersionControlSettings.First().Server)
                        ), // rage
                        new XElement("tools", 
                            new XAttribute("workspace", this.VersionControlSettings.First().Workspace),
                            new XAttribute("username", this.VersionControlSettings.First().Username),
                            new XAttribute("server", this.VersionControlSettings.First().Server)
                        ) // tools
                    );
                }

                XDocument xmlDoc = new XDocument(
                    new XDeclaration("1.0", "UTF-8", "yes"),
                    new XElement("local",
#warning DHM FIX ME: obsolete
                        new XComment("Tools Migration: old XAttribute-format but still read by RSG.Base.Configuration."),
                        new XAttribute("version", this.ConfigVersion),
                        new XAttribute("installtime", this.InstallTime.Ticks),
                        new XElement("user",
                            new XAttribute("username", this.Username),
                            new XAttribute("emailaddress", this.EmailAddress),
                            new XAttribute("flags", this.Usertype)
                        ), // user
#warning DHM FIX ME: should be per-project... see Project.cs for new implementation.  To help global-tools migration.
                        scmElem, // scm
#warning DHM FIX ME: end obsolete format
                    
                        // New XElement-format.
                        new XComment("Tools Migration: new XElement-format."),
                        new XElement("ConfigFileVersion", CONFIG_VERSION),
                        new XElement("InstallTime", this.InstallTime.Ticks),
                        new XElement("LocalVersion", this.Version.LocalVersion.LabelName),
                        new XElement("InstalledVersion", this.Version.InstalledVersion.LabelName),
                        new XElement("VersionControlEnabled", new XAttribute("value", this.VersionControlEnabled)),
                        new XElement("User",
                            new XElement("Username", this.Username),
                            new XElement("EmailAddress", this.EmailAddress),
                            new XElement("Usertype", this.Usertype)
                        ), // User
                        new XElement("Applications",
                            new XElement("Application",
                                new XElement("Name", "Xoreax XGE"),
                                new XElement("Enabled", new XAttribute("value", true))
                            )
#warning DHM FIX ME: 3dsmax and MotionBuilder toolset.
                        ) // Applications
                    )
                );
                xmlDoc.Save(filename);

                bool result = true;
                result &= ((Installer.IWritableProject)this.Project).SaveLocal();
                return (true);
            }
            catch (Exception ex)
            {
                throw new ConfigurationException("Failed to save local config.", ex)
                    {
                        Filename = filename
                    };
            }
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        private void PostEnvironmentInit()
        {
            this.Environment.Clear();
            this.Import(this.Environment);
            this.Project.PostEnvironmentInit();
        }

        /// <summary>
        /// Parse top-level configuration data elements.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="local"></param>
        private void ParseConfig(String filename, bool local)
        {
            XDocument xmlDoc = XDocument.Load(filename,
                LoadOptions.SetLineInfo | LoadOptions.SetBaseUri);

            ParseConfigAttributes(xmlDoc, filename, local);
            String root = local ? ELEM_LOCAL : ELEM_CONFIG;
            foreach (XElement xmlProjectElem in
                xmlDoc.XPathSelectElements(String.Format("{0}/projects/project", root)))
            {
                ParseProject(xmlProjectElem, local);
            }
            if (!local)
            {
                Log.Message("Loading {0} project configuration data.", this.Project.FriendlyName);
            }

            // DLC Configuration Data only in non-local XML.
            if (!local)
            {
                IEnumerable<XElement> xmlDLCProjectElems =
                    xmlDoc.XPathSelectElements(String.Format("{0}/projects/dlc", root));
                foreach (XElement xmlDlcProjectElem in xmlDLCProjectElems)
                {
                    ParseDLCProject(xmlDlcProjectElem, this.CoreProject);
                }
                Log.Message("{0} DLC Project Configurations: {1} of {2} available.",
                    this.Project.FriendlyName, this.DLCProjects.Count, xmlDLCProjectElems.Count());
            }

            foreach (XElement xmlUserTypeElem in
                xmlDoc.XPathSelectElements(String.Format("{0}/usertypes/usertype", root)))
            {
                ParseUserType(xmlUserTypeElem, local);
            }
            foreach (XElement xmlScmElem in xmlDoc.XPathSelectElements(String.Format("{0}/scm", root)))
            {
                ParseScm(xmlScmElem, local);
            }

            if (local)
            {
                XElement xmlUserElem = xmlDoc.XPathSelectElement(String.Format("{0}/user", root));
                if (null != xmlUserElem)
                {
                    ParseUserAttributes(xmlUserElem);
                }
                XElement xmlSCMElem = xmlDoc.XPathSelectElement(String.Format("{0}/scm", root));
                if (null != xmlSCMElem)
                {
                    ParseSCMSettings(xmlSCMElem);
                }
#warning DHM TODO: parse logging
            }
        }
        
        /// <summary>
        /// Parse top-level configuration data attributes.
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="filename"></param>
        /// <param name="local"></param>
        private void ParseConfigAttributes(XDocument xmlDoc, String filename, bool local)
        {
            foreach (XAttribute xmlConfigAttr in xmlDoc.Root.Attributes())
            {
                if (0 == String.Compare(xmlConfigAttr.Name.LocalName, ATTR_VERSION))
                {
                    int version = int.Parse(xmlConfigAttr.Value);
                    if (local)
                    {
                        // Version check.
                        if ((!this.SkipVersionCheck) && (this.ConfigVersion != version))
                        {
                            String message = String.Format(
                                "Current version: {0}, Expected version: {1}. You need to run the Tools Installer.",
                                version, this.ConfigVersion);
#if DEBUG
                            Debug.Assert(version != this.ConfigVersion, message);
#endif // DEBUG
                            Log.Error(message);
                            throw new ConfigurationVersionException(version, this.ConfigVersion)
                                {
                                    Filename = this.Filename
                                };
                        }
                    }
                    else
                    {
                        this.ConfigVersion = version;
                    }
                }
                else if ((0 == String.Compare(xmlConfigAttr.Name.LocalName, ATTR_INSTALL_TIME)) && local)
                {
                    try
                    {
                        long ticks = 0;
                        if (long.TryParse(xmlConfigAttr.Value, out ticks))
                        {
                            this.InstallTime = new DateTime(ticks);
                        }
                        else
                        {
                            //Backward compatible - Used to store date/time in the format designated by the system (e.g. MM/DD/YYYY)
                            this.InstallTime = DateTime.Parse(xmlConfigAttr.Value);
                        }
                    }
                    catch
                    {
                        // We really don't care if the InstallTime wasn't parsed correctly.
                        Config.Log.Warning(xmlDoc, "Failed to parse install time: '{0}'.",
                            xmlConfigAttr.Value);
                        this.InstallTime = DateTime.MinValue;
                    }
                }
                else
                {
#if DEBUG
                    Config.Log.Debug(xmlDoc, "Unknown config attribute: '{0}'.",
                        xmlConfigAttr.Name.LocalName);
#endif
                }
            }
        }
        
        /// <summary>
        /// Parse project element.
        /// </summary>
        /// <param name="xmlElem"></param>
        /// <param name="local"></param>
        private void ParseProject(XElement xmlElem, bool local)
        {
            if (null == this.Project)
            {
                this.CoreProject = new Project(this, xmlElem);
                this.Project = this.CoreProject; // default to CoreProject.
            }
            else if (local)
            {
                // Nothing; local 'enabled' flag is no longer supported.
            }
            else
            {
                Debug.Fail("Project already exists but local flag not set.  Internal error.");
                throw new ConfigurationException("Project already exists but local flag not set.  Internal error.")
                    {
                        Filename = this.Filename
                    };
            }
        }

        /// <summary>
        /// Parse DLC project element.
        /// </summary>
        /// <param name="xmlElem"></param>
        /// <param name="coreProject"></param>
        private void ParseDLCProject(XElement xmlElem, IProject coreProject)
        {
            // Create the DLC project object.
            if (null == this.CoreProject)
            {
                Debug.Fail("No core project defined.");
                throw new ConfigurationException("Attempting to load DLC with no core project.")
                    {
                        Filename = this.Filename
                    };
            }

            try
            {
                IProject dlcProject = new Project(this, xmlElem, this.CoreProject);
                this.DLCProjects.Add(dlcProject.Name, dlcProject);
            }
            catch (DLCConfigurationException)
            {
                // Ignore; DLC are currently optional projects.
            }
        }

        /// <summary>
        /// Parse usertype element.
        /// </summary>
        /// <param name="xmlElem"></param>
        /// <param name="local"></param>
        private void ParseUserType(XElement xmlElem, bool local)
        {
            if (!local)
            {
                this.Usertypes.Add(new Usertype(xmlElem));
            }
            else
            {
                Log.Error("Unexpected usertype defined in local configuration data.  Ignored.");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlElem"></param>
        /// <param name="local"></param>
        private void ParseScm(XElement xmlElem, bool local)
        {
            if (!local)
            {
#warning TODO: Parse Perforce data from config.xml.
                this.VersionControlEnabled = true;
            }
            else
            {
                foreach (XAttribute xmlScmAttr in xmlElem.Attributes("toolchain_integration"))
                {
                    this.VersionControlEnabled = bool.Parse(xmlScmAttr.Value.Trim());
                }

#warning TODO: Parse Perforce data from local configuration data.
            }
        }

        /// <summary>
        /// Parse top-level Studio metadata file.
        /// </summary>
        /// <param name="log"></param>
        private void ParseStudioFile(ILog log)
        {
            // Studio data is now in a separate file.
            this.Studios = new StudioCollection();
            String studioConfigFilename = Path.Combine(this.ToolsConfig, "globals", "studios.meta");
            if (File.Exists(studioConfigFilename))
            {
                XDocument xmlStudioDoc = XDocument.Load(studioConfigFilename);
                foreach (XElement xmlStudiosElem in
                    xmlStudioDoc.XPathSelectElements("/StudioFile/Studios/Item"))
                {
                    this.Studios.Add(new Studio(xmlStudiosElem));
                }
            }
        }
                        
        /// <summary>
        /// Return environment variable value or a default value String.
        /// </summary>
        /// <param name="envvar"></param>
        /// <param name="defaultVal"></param>
        /// <returns></returns>
        private String GetEnvVarOrDefault(String envvar, String defaultVal)
        {
            String envvarVal = System.Environment.GetEnvironmentVariable(envvar);
            if (String.IsNullOrEmpty(envvarVal))
                return (defaultVal);
            else
                return (envvarVal);
        }

        /// <summary>
        /// Parse user-XML attributes.
        /// </summary>
        /// <param name="xmlElem"></param>
        private void ParseUserAttributes(XElement xmlElem)
        {
            foreach (XAttribute xmlUserAttr in xmlElem.Attributes())
            {
                if (0 == String.Compare(ATTR_USERNAME, xmlUserAttr.Name.LocalName))
                {
                    this.Username = xmlUserAttr.Value.Trim();
                }
                else if (0 == String.Compare(ATTR_EMAIL_ADDRESS, xmlUserAttr.Name.LocalName))
                {
                    this.EmailAddress = xmlUserAttr.Value.Trim();
                }
                else if (0 == String.Compare(ATTR_USER_FLAGS, xmlUserAttr.Name.LocalName))
                {
                    uint result = 0;
                    if (uint.TryParse(xmlUserAttr.Value.Trim(), out result))
                        this.Usertype = result;
                }
                else
                {
                    RSG.Base.Configuration.Config.Log.Warning(xmlElem,
                        "Unknown user attribute: '{0}'.",
                        xmlUserAttr.Name.LocalName);
                }
            }
        }

        /// <summary>
        /// Parse SCM elements.
        /// </summary>
        /// <param name="xmlElem"></param>
        private void ParseSCMSettings(XElement xmlElem)
        {
            // Parse SCM attributes.
            foreach (XAttribute xmlSCMAttr in xmlElem.Attributes())
            {
                if (0 == String.Compare(ATTR_SCM_TOOLCHAIN_INTEGRATION, xmlSCMAttr.Name.LocalName))
                {
                    bool scm_integration = true;
                    if (bool.TryParse(xmlSCMAttr.Value.Trim(), out scm_integration))
                        this.VersionControlEnabled = scm_integration;
                }
                else
                {
                    Config.Log.Warning(xmlElem, "Unknown SCM attribute: '{0}'.", 
                        xmlSCMAttr.Name.LocalName);
                }
            }

#warning DHM FIX ME: this needs cleaned up in the config - 3 different settings that are never used.  Add root path attribute?
            // Parse SCM settings.
            IEnumerable<XElement> xmlSCMElems = xmlElem.XPathSelectElements("*");
            foreach (XElement xmlSCMElem in xmlSCMElems)
            {
                IVersionControlSettings settings = new VersionControlSettings(xmlSCMElem);
                (this.VersionControlSettings as ICollection<IVersionControlSettings>).Add(settings);
            }
        }
        #endregion // Private Methods
    }

} // RSG.Base.Configuration namespace
