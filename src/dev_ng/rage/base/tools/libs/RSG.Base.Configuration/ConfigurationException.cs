﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Configuration
{
    /// <summary>
    /// Tools Configuration Exception class.
    /// </summary>
    public class ConfigurationException : Exception
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="Filename"/> property.
        /// </summary>
        private String _filename;
        #endregion // Fields

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        protected ConfigurationException()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="message"></param>
        public ConfigurationException(String message)
            : base(message)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="message"></param>
        /// <param name="inner"></param>
        public ConfigurationException(String message, Exception inner)
            : base(message, inner)
        {
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// File that generated the exception.
        /// </summary>
        public String Filename
        {
            get { return _filename; }
            set { _filename = value; }
        }
        #endregion // Properties
    }
} // RSG.Base.Configuration namespace
