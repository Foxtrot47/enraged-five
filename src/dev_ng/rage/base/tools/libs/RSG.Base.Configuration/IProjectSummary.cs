﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Configuration
{

    /// <summary>
    /// Project summary interface; for reading summary information about
    /// available projects when the configuration data may not be available.
    /// </summary>
    public interface IProjectSummary : IHasEnvironment
    {
        /// <summary>
        /// Project name.
        /// </summary>
        /// <seealso cref="IProject.Name" />
        /// <seealso cref="IProject2.Name" />
        String Name { get; }

        /// <summary>
        /// Project friendly UI name.
        /// </summary>
        /// <seealso cref="IProject.FriendlyName" />
        /// <seealso cref="IProject2.FriendlyName" />
        String FriendlyName { get; }

        /// <summary>
        /// Project root-directory (absolute path).
        /// </summary>
        /// <seealso cref="IProject.Root" />
        /// <seealso cref="IProject2.Root" />
        String Root { get; }

        /// <summary>
        /// Project configuration filename.
        /// </summary>
        /// <seealso cref="IProject.Filename" />
        /// <seealso cref="IProject2.Filename" />
        String Filename { get; }

        /// <summary>
        /// Encapsulated Environment object.
        /// </summary>
        IEnvironment Environment { get; }
    }

} // RSG.Base.Configuration namespace
