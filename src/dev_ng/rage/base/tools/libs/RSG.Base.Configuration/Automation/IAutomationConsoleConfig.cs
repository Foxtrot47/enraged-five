﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Configuration.Automation
{

    /// <summary>
    /// Automation Service Console configuration data.
    /// </summary>
    public interface IAutomationConsoleConfig
    {
        /// <summary>
        /// Branch this service is associated with.
        /// </summary>
        String BranchName { get; }

        /// <summary>
        /// Automation Console argument string.
        /// </summary>
        String Arguments { get; }
    }

} // RSG.Base.Configuration.Automation namespace
