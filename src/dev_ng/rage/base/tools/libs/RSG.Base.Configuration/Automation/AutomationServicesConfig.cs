﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Xml.Linq;

namespace RSG.Base.Configuration.Automation
{
    
    /// <summary>
    /// 
    /// </summary>
    internal class AutomationServicesConfig : IAutomationServicesConfig
    {
        #region Properties
        /// <summary>
        /// Automation services configuration details.
        /// </summary>
        public IEnumerable<IAutomationServiceConfig> Services { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="portal"></param>
        /// <param name="services"></param>
        public AutomationServicesConfig(IEnumerable<IAutomationServiceConfig> services)
        {
            this.Services = services;
        }
        #endregion // Constructor(s)

        #region Methods
        /// <summary>
        /// Load Automation Service configuration for a project.
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        public static IAutomationServicesConfig Load(IProject project)
        {
            String filename = Path.Combine(project.Config.ToolsConfig, "automation",
                "AutomationServices.xml");
            return (Load(project, filename));
        }

        /// <summary>
        /// Load Automation Service configuration from a file.
        /// </summary>
        /// <param name="project"></param>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static IAutomationServicesConfig Load(IProject project, String filename)
        {
            XDocument xmlDoc = XDocument.Load(filename);
            IEnumerable<XElement> xmlServiceElems = xmlDoc.Root.Element("Services").Elements("Service");
            IEnumerable<IAutomationServiceConfig> serviceConfigs = xmlServiceElems.Select(x => AutomationServiceConfig.Load(project, x));
            
            return (new AutomationServicesConfig(serviceConfigs));
        }

        /// <summary>
        /// Return all the parameters for all studios and services.
        /// </summary>
        /// <returns>Enumeration of the parameters.</returns>
        public IEnumerable<IAutomationServiceConfig> GetAllServices()
        {
            return (this.Services);
        }

        /// <summary>
        /// Return all service parameters for a particular service type.
        /// </summary>
        /// <param name="serviceName">Service friendly-name.</param>
        /// <returns>Enumeration of the parameters.</returns>
        public IEnumerable<IAutomationServiceConfig> GetAllServices(String serviceType)
        {
            IEnumerable<IAutomationServiceConfig> parameters =
                this.Services.Where(p => String.Equals(p.ServiceType, serviceType, StringComparison.OrdinalIgnoreCase));
            return (parameters);
        }

        /// <summary>
        /// Return all service parameters for a particular studio.
        /// </summary>
        /// <param name="studio">Service studio filter.</param>
        /// <returns></returns>
        public IEnumerable<IAutomationServiceConfig> GetAllServices(IStudio studio)
        {
            IEnumerable<IAutomationServiceConfig> parameters =
                this.Services.Where(p => p.Studio.Equals(studio));
            return (parameters);
        }

        /// <summary>
        /// Return a service config for a particular server host.
        /// </summary>
        /// <param name="serverHost">Server host filter.</param>
        /// <returns></returns>
        public IAutomationServiceConfig GetService(String serverHost)
        {
            IAutomationServiceConfig serviceConfig =
                    this.Services.Where(p => p.ServerHost.Host.Equals(serverHost, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
            return (serviceConfig);
        }

        /// <summary>
        /// Return a service view config for a particular web alias.
        /// </summary>
        /// <param name="webAlias">Web alias for filter.</param>
        /// <returns></returns>
        public IAutomationServiceViewConfig GetServiceView(String webAlias)
        {
            IAutomationServiceViewConfig serviceViewConfig = 
                    this.Services.SelectMany(s => s.Views.Where(
                                                    v => v.WebAlias.Equals(webAlias, StringComparison.OrdinalIgnoreCase)
                                                )
                                            ).FirstOrDefault();
            return (serviceViewConfig);
        }
        #endregion // Methods
    }

}// RSG.Base.Configuration.Automation namespace
