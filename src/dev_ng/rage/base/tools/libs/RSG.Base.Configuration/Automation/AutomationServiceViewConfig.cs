﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Xml.Linq;

namespace RSG.Base.Configuration.Automation
{
    /// <summary>
    /// Automation Service SubView Configuration object.
    /// </summary>
    internal class AutomationServiceViewConfig : IAutomationServiceViewConfig
    {
        #region Properties
        /// <summary>
        /// Reference to parent Service
        /// </summary>
        public IAutomationServiceConfig Service 
        { 
            get;
            private set;
        }

        /// <summary>
        /// Name of The View
        /// </summary>
        public String Name
        {
            get;
            private set;
        }

        /// <summary>
        /// Web Alias for Portal.
        /// </summary>
        public String WebAlias
        {
            get;
            private set;
        }

        /// <summary>
        /// View Mode 
        /// </summary>
        public ViewMode ViewMode
        {
            get;
            private set;
        }

        /// <summary>
        /// Anonymous Use Access
        /// </summary>
        public Boolean AllowAnonymous
        {
            get;
            private set;
        }

        /// <summary>
        /// Property For Filtering The IJobs of the SubView.
        /// </summary>
        public String FilterKey
        {
            get;
            private set;
        }

        /// <summary>
        /// Value Of the Above Property For Filtering
        /// </summary>
        public String FilterValue
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Static Controller Methods
        /// <summary>
        /// Load Automation Service SubView configuration from an XML element.
        /// </summary>
        /// <param name="project"></param>
        /// <param name="xmlElem"></param>
        /// <returns></returns>
        public static AutomationServiceViewConfig Load(IProject project, IAutomationServiceConfig service, XElement xmlElem)
        {
            Debug.Assert(xmlElem.Name.LocalName.Equals("View"),
                "Invalid Service XML element.");
            if (!xmlElem.Name.LocalName.Equals("View"))
                throw (new ArgumentException("Invalid Service XML element.", "xmlElem"));

            AutomationServiceViewConfig config = new AutomationServiceViewConfig();
            config.Service = service;
            config.Name = xmlElem.Element("Name").Value;
            config.WebAlias = xmlElem.Element("WebAlias").Value;

            ViewMode viewMode = ViewMode.Default;
            if (xmlElem.Element("ViewMode") != null)
            {
                if (Enum.TryParse<ViewMode>(xmlElem.Element("ViewMode").Value, out viewMode))
                    config.ViewMode = viewMode;
            }

            if (xmlElem.Element("AllowAnonymous") != null)
            {
                config.AllowAnonymous = Boolean.Parse(xmlElem.Element("AllowAnonymous").Value);
            }

            if (xmlElem.Element("FilterKey") != null)
            {
                config.FilterKey = xmlElem.Element("FilterKey").Value;
            }
            if (xmlElem.Element("FilterValue") != null)
            {
                config.FilterValue = xmlElem.Element("FilterValue").Value;
            }
            
            return (config);
        }
        #endregion // Static Controller Methods
    }

} // RSG.Base.Configuration.Automation namespace
