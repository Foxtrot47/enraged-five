﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Configuration.Automation
{
    /// <summary>
    /// View mode to give a hint to automation views on how to display a service.
    /// </summary>
    public enum ViewMode
    {   
        /// <summary>
        /// Present the jobs as a simple list.
        /// </summary>
        JobList,
        
        /// <summary>
        /// Present only the summary for each matrix group and the status of the last processed changelist.
        /// </summary>
        JobMatrixSummary,

        /// <summary>
        /// Present the jobs as a matrix grouped-by changelist (i.e. Codebuilder) served from local file
        /// </summary>
        JobMatrixStatic,

        /// <summary>
        /// Present the jobs as a matrix grouped-by changelist (i.e. Codebuilder) as plain html
        /// </summary>
        JobMatrixLight,

        /// <summary>
        /// Present the jobs as a matrix grouped-by changelist (i.e. Codebuilder).
        /// </summary>
        JobMatrix,
                
        /// <summary>
        /// Default view-mode.
        /// </summary>
        Default = JobList,
    }

} // RSG.Base.Configuration.Automation namespace
