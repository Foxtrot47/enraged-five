﻿using System;

namespace RSG.Base.Configuration.Automation
{

    /// <summary>
    /// Automation service parameter interface; for interfacing with an
    /// automation system.
    /// </summary>
    [Obsolete("Use IAutomationServiceConfig instead.")]
    public interface IAutomationServiceParameters
    {
        #region Properties
        /// <summary>
        /// The studio the service relates to.
        /// </summary>
        IStudio Studio { get; }
        
        /// <summary>
        /// Configuration data filename.
        /// </summary>
        String Filename { get; }

        /// <summary>
        /// Automation service friendly name (for UI display).
        /// </summary>
        String FriendlyName { get; }

        /// <summary>
        /// Flag to determine whether the service is visible in the Workbench.
        /// </summary>
        bool WorkbenchVisible { get; }
        
        /// <summary>
        /// Automation Service (IAutomationService)
        /// </summary>
        Uri AutomationService { get; }

        /// <summary>
        /// Automation Admin Service (IAutomationAdminService)
        /// </summary>
        Uri AutomationAdminService { get; }

        /// <summary>
        /// File Transfer Service (IFileTransferService)
        /// </summary>
        Uri FileTransferService { get; }
        #endregion // Properties
    }

} // RSG.Base.Configuration.Automation namespace
