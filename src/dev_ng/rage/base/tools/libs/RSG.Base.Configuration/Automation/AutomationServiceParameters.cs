﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.IO;
using System.Xml.Linq;
using RSG.Base.Configuration;

namespace RSG.Base.Configuration.Automation
{

    /// <summary>
    /// 
    /// </summary>
    [Obsolete("Use IAutomationServiceConfig instead.")]
    public class AutomationServiceParameters : IAutomationServiceParameters
    {
        #region Constants
        private static readonly String PARAM_FRIENDLY_NAME = "Friendly Name";
        private static readonly String PARAM_WORKBENCH_VISIBLE = "Workbench Visible";
        private static readonly String PARAM_AUTOMATION_SERVICE = "Automation Service";
        private static readonly String PARAM_AUTOMATION_ADMIN_SERVICE = "Automation Admin Service";
        private static readonly String PARAM_FILE_TRANSFER_SERVICE = "File Transfer Service";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// The studio. 
        /// </summary>
        public IStudio Studio { get; private set; }

        /// <summary>
        /// Configuration data filename.
        /// </summary>
        public String Filename { get; private set; }

        /// <summary>
        /// Automation service friendly name (for UI display).
        /// </summary>
        public String FriendlyName { get; private set; }

        /// <summary>
        /// Flag to determine whether the service is visible in the Workbench.
        /// </summary>
        public bool WorkbenchVisible { get; private set; }

        /// <summary>
        /// Automation Service (IAutomationService)
        /// </summary>
        public Uri AutomationService { get; private set; }

        /// <summary>
        /// Automation Admin Service (IAutomationAdminService)
        /// </summary>
        public Uri AutomationAdminService { get; private set; }

        /// <summary>
        /// File Transfer Service (IFileTransferService)
        /// </summary>
        public Uri FileTransferService { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor; loading parameter data from XElement.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="xmlParamsElem"></param>
        public AutomationServiceParameters(StudioCollection studios, String filename, XElement xmlParamsElem)
        {
            this.Filename = filename;
            Load(studios, xmlParamsElem);
        }
        #endregion // Constructor(s)

        #region Private Methods
        /// <summary>
        /// Load configuration parameters from XElement.
        /// </summary>
        /// <param name="xmlParamsElem"></param>
        private void Load(StudioCollection studios, XElement xmlParamsElem)
        {
            // Initialise Studio from 'name' attribute.
            XAttribute xmlStudioNameAttr = xmlParamsElem.Attribute("name");
            this.Studio = studios.Where(s => 0 == String.Compare(s.Name, xmlStudioNameAttr.Value)).FirstOrDefault();
            Debug.Assert(null != this.Studio, String.Format("Failed to find studio key: '{0}'.", xmlStudioNameAttr.Name));
            
            IDictionary<String, Object> parameters = new Dictionary<String, Object>();
            RSG.Base.Xml.Parameters.Load(xmlParamsElem, ref parameters);
            SetParameters(parameters);
        }

        /// <summary>
        /// Reload parameters.
        /// </summary>
        /// <param name="parameters">Parameters.</param>
        internal void SetParameters(IDictionary<String, Object> parameters)
        {
            this.FriendlyName = parameters.ContainsKey(PARAM_FRIENDLY_NAME) ?
                (String)parameters[PARAM_FRIENDLY_NAME] : String.Empty;
            this.WorkbenchVisible = parameters.ContainsKey(PARAM_WORKBENCH_VISIBLE) ?
                (bool)parameters[PARAM_WORKBENCH_VISIBLE] : false;
            this.AutomationService = new Uri(ReadAndValidateStringParameter(
                parameters, PARAM_AUTOMATION_SERVICE));
            this.AutomationAdminService = new Uri(ReadAndValidateStringParameter(
                parameters, PARAM_AUTOMATION_ADMIN_SERVICE));
            this.FileTransferService = new Uri(ReadAndValidateStringParameter(
                parameters, PARAM_FILE_TRANSFER_SERVICE));
        }

        /// <summary>
        /// Read and validate string parameter.
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        private String ReadAndValidateStringParameter(IDictionary<String, Object> parameters, String param)
        {
            if (!parameters.ContainsKey(param))
            {
                Debug.Fail("Parameter not found: {0}", param);
                throw new ConfigurationException(String.Format("Parameter not found: {0}", param))
                    {
                        Filename = this.Filename
                    };
            }

            return ((String)parameters[param]);
        }
        #endregion // Private Methods
    }

} // RSG.Base.Configuration.Automation namespace
