﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Configuration.Automation
{

    /// <summary>
    /// Automation Service SubView Configuration interface.
    /// </summary>
    public interface IAutomationServiceViewConfig
    {
        #region Properties
        /// <summary>
        /// Reference to parent Service
        /// </summary>
        IAutomationServiceConfig Service { get; }

        /// <summary>
        /// Name of The View
        /// </summary>
        String Name
        {
            get;
        }

        /// <summary>
        /// Web Alias for Portal.
        /// </summary>
        String WebAlias
        {
            get;
        }

        /// <summary>
        /// Anonymous User Access
        /// </summary>
        Boolean AllowAnonymous
        {
            get;
        }

        /// <summary>
        /// View Mode 
        /// </summary>
        ViewMode ViewMode
        {
            get;
        }

        /// <summary>
        /// Property For Filtering The IJobs of the SubView.
        /// </summary>
        String FilterKey
        {
            get;
        }

        /// <summary>
        /// Value Of the Above Property For Filtering
        /// </summary>
        String FilterValue
        {
            get;
        }
        #endregion // Properties

    }

} // RSG.Base.Configuration.Automation namespace
