﻿using System;
using System.Collections.Generic;

namespace RSG.Base.Configuration.Automation
{

    /// <summary>
    /// Automation Core Services Configuration Data.
    /// </summary>
    [Obsolete("Use IAutomationServiceConfig instead.")]
    public interface IAutomationCoreServices
    {
        #region Methods
        /// <summary>
        /// Reload configuration data.
        /// </summary>
        void Reload();

        /// <summary>
        /// Return a service parameters for the current users studio.
        /// </summary>
        /// <param name="service">Service friendly-name.</param>
        /// <returns>The service parameters.</returns>
        IAutomationServiceParameters GetServiceParameters(String service);

        /// <summary>
        /// Return a service parameters for a particular studio (or null).
        /// </summary>
        /// <param name="service">Service friendly-name.</param>
        /// <param name="studio">Service studio filter.</param>
        /// <returns></returns>
        IAutomationServiceParameters GetServiceParameters(String service, IStudio studio);

        /// <summary>
        /// Return all service parameters for all studios.
        /// </summary>
        /// <returns>Enumeration of the parameters.</returns>
        IEnumerable<IAutomationServiceParameters> GetAllParameters();

        /// <summary>
        /// Return all service parameters for a particular service name.
        /// </summary>
        /// <param name="serviceName">Service friendly-name.</param>
        /// <returns>Enumeration of the parameters.</returns>
        IEnumerable<IAutomationServiceParameters> GetAllParameters(String serviceName);

        /// <summary>
        /// Return all service parameters for a particular studio.
        /// </summary>
        /// <param name="studio">Service studio filter.</param>
        /// <returns></returns>
        IEnumerable<IAutomationServiceParameters> GetAllParameters(IStudio studio);
        #endregion // Methods
    }

} // RSG.Base.Configuration.Automation namespace
