﻿using System;
using System.Linq;
using System.Xml.Linq;

namespace RSG.Base.Configuration.Automation
{

    /// <summary>
    /// Automation Service Console configuration data.
    /// </summary>
    internal class AutomationConsoleConfig : IAutomationConsoleConfig
    {
        #region Properties
        /// <summary>
        /// Branch this service is associated with.
        /// </summary>
        public String BranchName
        {
            private set;
            get;
        }

        /// <summary>
        /// Console arguments.
        /// </summary>
        public String Arguments
        {
            get;
            private set;
        }
        #endregion // Properties
        
        #region Static Controller Methods
        /// <summary>
        /// Load Console configuration data from XML element.
        /// </summary>
        /// <param name="xmlConsoleElem"></param>
        /// <returns></returns>
        public static IAutomationConsoleConfig Load(XElement xmlConsoleElem)
        {
            AutomationConsoleConfig config = new AutomationConsoleConfig();
            if (null != xmlConsoleElem.Element("Branch"))
                config.BranchName = xmlConsoleElem.Element("Branch").Value;
            if (null != xmlConsoleElem.Element("Arguments"))
                config.Arguments = xmlConsoleElem.Element("Arguments").Value;
        
            return (config);
        }

        /// <summary>
        /// Create empty Console configuration data.
        /// </summary>
        /// <returns></returns>
        public static IAutomationConsoleConfig CreateEmpty()
        {
            return (new AutomationConsoleConfig());
        }
        #endregion // Static Controller Methods
    }

} // RSG.Base.Configuration.Automation namespace
