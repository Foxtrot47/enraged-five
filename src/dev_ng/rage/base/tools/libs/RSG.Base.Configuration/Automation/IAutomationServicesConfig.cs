﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Configuration.Automation
{
    /// <summary>
    /// 
    /// </summary>
    public interface IAutomationServicesConfig
    {
        #region Properties
        /// <summary>
        /// Automation services configuration details.
        /// </summary>
        IEnumerable<IAutomationServiceConfig> Services { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Return all the parameters for all studios and services.
        /// </summary>
        /// <returns>Enumeration of the parameters.</returns>
        IEnumerable<IAutomationServiceConfig> GetAllServices();

        /// <summary>
        /// Return all service parameters for a particular service type.
        /// </summary>
        /// <param name="serviceName">Service friendly-name.</param>
        /// <returns>Enumeration of the parameters.</returns>
        IEnumerable<IAutomationServiceConfig> GetAllServices(String serviceType);

        /// <summary>
        /// Return all service parameters for a particular studio.
        /// </summary>
        /// <param name="studio">Service studio filter.</param>
        /// <returns></returns>
        IEnumerable<IAutomationServiceConfig> GetAllServices(IStudio studio);

        /// <summary>
        /// Return a service config for a particular server host.
        /// </summary>
        /// <param name="serverHost">Server host filter.</param>
        /// <returns></returns>
        IAutomationServiceConfig GetService(String serverHost);

        /// <summary>
        /// Return a service view config for a particular web alias.
        /// </summary>
        /// <param name="webAlias">Web alias for filter.</param>
        /// <returns></returns>
        IAutomationServiceViewConfig GetServiceView(String webAlias);
        #endregion // Methods
    }

} // RSG.Base.Configuration.Automation namespace
