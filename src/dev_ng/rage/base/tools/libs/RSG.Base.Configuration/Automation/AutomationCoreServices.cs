﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Logging;

namespace RSG.Base.Configuration.Automation
{

    /// <summary>
    /// Automation configuration data for a project.
    /// </summary>
    [Obsolete("Use IAutomationServiceConfig instead.")]
    internal class AutomationCoreServices : IAutomationCoreServices
    {
        #region Member Data
        private IProject m_Project;
        private ICollection<IAutomationServiceParameters> m_Services;
        #endregion // Member Data
        
        #region Constructor(s)
        /// <summary>
        /// Constructor; specifying a IProject object.
        /// </summary>
        /// <param name="config"></param>
        public AutomationCoreServices(IProject project)
        {
            if (null == project)
                throw (new ArgumentNullException("project"));

            this.m_Project = project;
            this.m_Services = new List<IAutomationServiceParameters>();
            Reload();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Return a service parameters for the current users studio (or null).
        /// </summary>
        /// <param name="service">Service friendly-name.</param>
        /// <returns>The service parameters.</returns>
        public IAutomationServiceParameters GetServiceParameters(String serviceName)
        {
            IStudio studio = this.m_Project.Config.Studios.ThisStudio;
            IEnumerable<IAutomationServiceParameters> parameters =
                this.m_Services.Where(p => p.Studio.Equals(studio) && 0 == String.Compare(p.FriendlyName, serviceName, true));
            
            return (parameters.FirstOrDefault());
        }

        /// <summary>
        /// Return a service parameters for a particular studio (or null).
        /// </summary>
        /// <param name="service">Service friendly-name.</param>
        /// <param name="studio">Service studio filter.</param>
        /// <returns></returns>
        public IAutomationServiceParameters GetServiceParameters(String serviceName, IStudio studio)
        {
            IEnumerable<IAutomationServiceParameters> parameters =
                this.m_Services.Where(p => p.Studio.Equals(studio) && 0 == String.Compare(p.FriendlyName, serviceName, true));

            return (parameters.FirstOrDefault());
        }


        /// <summary>
        /// Return all the parameters for all studios and services.
        /// </summary>
        /// <returns>Enumeration of the parameters.</returns>
        public IEnumerable<IAutomationServiceParameters> GetAllParameters()
        {
            return (this.m_Services);
        }

        /// <summary>
        /// Return all service parameters for a particular service name.
        /// </summary>
        /// <param name="serviceName">Service friendly-name.</param>
        /// <returns>Enumeration of the parameters.</returns>
        public IEnumerable<IAutomationServiceParameters> GetAllParameters(String serviceName)
        {
            IEnumerable<IAutomationServiceParameters> parameters =
                this.m_Services.Where(p => 0 == String.Compare(p.FriendlyName, serviceName, true));
            return (parameters);
        }

        /// <summary>
        /// Return all service parameters for a particular studio.
        /// </summary>
        /// <param name="studio">Service studio filter.</param>
        /// <returns></returns>
        public IEnumerable<IAutomationServiceParameters> GetAllParameters(IStudio studio)
        {
            IEnumerable<IAutomationServiceParameters> parameters =
                this.m_Services.Where(p => p.Studio.Equals(studio));
            return (parameters);
        }
        
        /// <summary>
        /// Reload configuration data.
        /// </summary>
        public void Reload()
        {
            // Get the tools root environment variable
            String toolsRoot = System.Environment.GetEnvironmentVariable("RS_TOOLSROOT");
            Debug.Assert(Path.IsPathRooted(toolsRoot), String.Format("RS_TOOLSROOT ({0}) has no drive specified.  Misconfigured tools.", toolsRoot));
            if (!Path.IsPathRooted(toolsRoot))
            {
                throw new ConfigurationException(String.Format("RS_TOOLSROOT ({0}) has no drive specified.  Misconfigured tools.", toolsRoot));
            }

            m_Services.Clear();
            String automationDirectory = Path.Combine(toolsRoot, "etc", "automation");

            if (!Directory.Exists(automationDirectory))
            {
                throw new ConfigurationException(String.Format("The automation directory ({0}) was not found.", automationDirectory));
            }

            String[] files = Directory.GetFiles(automationDirectory, "*.xml");
            foreach(string configFile in files)
            {
                try
                {
                    XDocument xmlDoc = XDocument.Load(configFile, 
                        LoadOptions.SetBaseUri | LoadOptions.SetLineInfo);
                    
                    IEnumerable<XElement> xmlStudioElems = xmlDoc.Root.XPathSelectElements("/Automation/Studio");

                    if (xmlStudioElems.Count() > 0)
                    {
                        foreach (XElement xmlStudioElem in xmlStudioElems)
                        {
                            IAutomationServiceParameters serviceParameters = new AutomationServiceParameters(
                                this.m_Project.Config.Studios, configFile, xmlStudioElem);
                            this.m_Services.Add(serviceParameters);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Config.Log.ToolException(ex, "Failed to load automation service configuration: {0}.",
                        configFile);
                }
                
            }
        }
        #endregion // Controller Methods
    }

} // RSG.Base.Configuration.Automation namespace
