﻿using System;
using System.Collections.Generic;

namespace RSG.Base.Configuration.Automation
{

    /// <summary>
    /// Automation Service Configuration interface.
    /// </summary>
    public interface IAutomationServiceConfig
    {
        #region Properties
        /// <summary>
        /// Service type name.
        /// </summary>
        String ServiceType
        {
            get;
        }

        /// <summary>
        /// Service studio location.
        /// </summary>
        IStudio Studio
        {
            get;
        }

        /// <summary>
        /// Service friendly-name.
        /// </summary>
        String FriendlyName
        {
            get;
        }

        /// <summary>
        /// Whether the service is visible to users.
        /// </summary>
        bool Visible
        {
            get;
        }

        /// <summary>
        /// Server Host to connect.
        /// </summary>
        Uri ServerHost
        {
            get;
        }

        /// <summary>
        /// Http Host to connect.
        /// </summary>
        Uri WebHost
        {
            get;
        }

        /// <summary>
        /// Automation Service Views
        /// </summary>
        IEnumerable<IAutomationServiceViewConfig> Views
        {
            get;
        }

        /// <summary>
        /// Automation Console configuration data.
        /// </summary>
        IAutomationConsoleConfig Console
        {
            get;
        }
        #endregion // Properties

    }

} // RSG.Base.Configuration.Automation namespace
