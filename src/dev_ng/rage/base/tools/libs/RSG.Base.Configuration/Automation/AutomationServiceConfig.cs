﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace RSG.Base.Configuration.Automation
{

    /// <summary>
    /// Automation Service configuration object.
    /// </summary>
    internal class AutomationServiceConfig : IAutomationServiceConfig
    {
        #region Properties
        /// <summary>
        /// Service type name.
        /// </summary>
        public String ServiceType
        {
            get;
            private set;
        }

        /// <summary>
        /// Service studio location.
        /// </summary>
        public IStudio Studio
        {
            get;
            private set;
        }

        /// <summary>
        /// Service friendly-name.
        /// </summary>
        public String FriendlyName
        {
            get;
            private set;
        }

        /// <summary>
        /// Whether the service is visible to users.
        /// </summary>
        public bool Visible
        {
            get;
            private set;
        }

        /// <summary>
        /// Server Host to connect.
        /// </summary>
        public Uri ServerHost
        {
            get;
            private set;
        }

        /// <summary>
        /// Http Host to connect.
        /// </summary>
        public Uri WebHost
        {
            get;
            private set;
        }

        /// <summary>
        /// Automation Service Views
        /// </summary>
        public IEnumerable<IAutomationServiceViewConfig> Views
        {
            get;
            private set;
        }

        /// <summary>
        /// Console configuration data.
        /// </summary>
        public IAutomationConsoleConfig Console
        {
            get;
            private set;
        }
        #endregion // Properties
        
        #region Static Controller Methods
        /// <summary>
        /// Load Automation Service configuration from an XML element.
        /// </summary>
        /// <param name="project"></param>
        /// <param name="xmlElem"></param>
        /// <returns></returns>
        public static AutomationServiceConfig Load(IProject project, XElement xmlElem)
        {
            Debug.Assert(xmlElem.Name.LocalName.Equals("Service"),
                "Invalid Service XML element.");
            if (!xmlElem.Name.LocalName.Equals("Service"))
                throw (new ArgumentException("Invalid Service XML element.", "xmlElem"));

            AutomationServiceConfig config = new AutomationServiceConfig();
            config.ServiceType = xmlElem.Element("Type").Value;
            config.FriendlyName = xmlElem.Element("FriendlyName").Value;
            config.ServerHost = new Uri(xmlElem.Element("ServerHost").Value);
            config.WebHost = new Uri(xmlElem.Element("WebHost").Value);
            bool visible = true;
            if (bool.TryParse(xmlElem.Element("Visible").Value, out visible))
                config.Visible = visible;
            String studio = xmlElem.Element("Studio").Value;
            config.Studio = project.Config.Studios.Studios.Where(s => s.Name.Equals(studio)).FirstOrDefault();

            // View Elements
            IEnumerable<XElement> xmlViewElems = xmlElem.Element("Views").Elements("View");
            config.Views = xmlViewElems.Select(x => AutomationServiceViewConfig.Load(project, config, x));
            
            // Console Element Conf
            if (xmlElem.Element("Console") != null)
            {
                XElement xmlConsoleElem = xmlElem.Element("Console");
                config.Console = AutomationConsoleConfig.Load(xmlConsoleElem);
            }
            else
            {
                config.Console = AutomationConsoleConfig.CreateEmpty();
            }
            return (config);
        }
        #endregion // Static Controller Methods
    }

} // RSG.Base.Configuration.Automation namespace
