﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RSG.Base.Configuration
{
    /// <summary>
    /// Base configuration class that contains common functions that all config base
    /// classes may need (such as attribute parsing methods).
    /// </summary>
    internal abstract class ConfigBase
    {
        #region Methods
        /// <summary>
        /// Read attribute of a particular type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="elem"></param>
        /// <param name="attr"></param>
        protected T ReadAttribute<T>(XElement elem, String attr)
        {
            if (elem.Attribute(attr) == null)
            {
                throw new ConfigurationException(String.Format("Unable to read the '{0}' attribute under the '{1}' element.", attr, elem.Name));
            }

            return (T)Convert.ChangeType(elem.Attribute(attr).Value, typeof(T));
        }
        #endregion // Methods
    } // ConfigBase
}
