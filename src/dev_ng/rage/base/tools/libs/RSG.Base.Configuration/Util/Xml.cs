﻿using System;
using System.Linq;
using System.Xml.Linq;

namespace RSG.Base.Configuration.Util
{

    /// <summary>
    /// XML utility methods.
    /// </summary>
    internal static class Xml
    {
        /// <summary>
        /// Read an attribute value from an XElement.
        /// </summary>
        /// <param name="xmlElem"></param>
        /// <param name="attr"></param>
        /// <param name="?"></param>
        public static void GetAttrValue(XElement xmlElem, String attr, ref String value)
        {
            XAttribute xmlAttr = xmlElem.Attribute(attr);
            if (null != xmlAttr && !String.IsNullOrEmpty(xmlAttr.Value))
                value = xmlAttr.Value.Trim();
        }
    }

} // RSG.Base.Configuration.Util namespace
