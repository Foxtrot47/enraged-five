﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Logging;

namespace RSG.Base.Configuration.Util
{
    /// <summary>
    /// Helper methods for 3ds max version checks.
    /// </summary>
    public static class Max
    {
        /// <summary>
        /// Checks whether a config object can be created.
        /// </summary>
        /// <param name="logExceptions">Whether to write out exceptions to the log.</param>
        /// <returns>Message informing the user whats gone wrong or null if everything is fine.</returns>
        public static String ConfigVersionCheck(bool logExceptions = true)
        {
            String message = null;

            try
            {
                ConfigFactory.CreateConfig();
            }
            catch (ConfigurationVersionException ex)
            {
                message = String.Format("Configuration version error: run tools installer.  Installed version: {0}, expected version: {1}.", ex.ActualVersion, ex.ExpectedVersion);
                if (logExceptions)
                {
                    Log.Log__Exception(ex, message);
                }
            }
            catch (ConfigurationException ex)
            {
                message = String.Format("Configuration parsing error: {0}", ex.Message);
                if (logExceptions)
                {
                    Log.Log__Exception(ex, message);
                }
            }

            return message;
        }
    } // Max
}
