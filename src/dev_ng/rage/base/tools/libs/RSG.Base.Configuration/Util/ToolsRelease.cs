﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.IO;
using System.Xml.Linq;
using RSG.Base.Logging;
using RSG.SourceControl.Perforce;
using P4API;
using System.Windows;
using System.Text.RegularExpressions;

namespace RSG.Base.Configuration.Util
{
    /// <summary>
    /// Functionality used when creating/updating tools label releases.
    /// </summary>
    public static class ToolsRelease
    {
        #region Public Methods
        /// <summary>
        /// Creates a new tools version label, and label syncs both it and the tools testing label.
        /// </summary>
        public static bool CreateNextToolsLabel(IConfig config, P4 p4)
        {
            // Check that the tools testing owner is set to the current p4 user.
            RSG.SourceControl.Perforce.Label testingLabel = RSG.SourceControl.Perforce.Label.GetLabel(p4, config.Project.Labels[Label.ToolsTesting]);
            if (testingLabel.Owner != p4.User)
            {
                MessageBox.Show("Unable to continue as you are not the current owner of the tools testing label " +
                    String.Format("'{0}'.\nCurrent Owner: '{1}'.\n\n", testingLabel.Name, testingLabel.Owner) +
                    "Please set yourself to be the label's owner and then re-run this script.",
                    "Label Creation", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return false;
            }

            // Check that the tools testing label isn't identical to the previously released (current) label.
            RSG.SourceControl.Perforce.Label latestVersionLabel = GetLatestToolsLabel(p4, config.Project.Labels[Label.ToolsIncremental], false);
            RSG.SourceControl.Perforce.Label currentLabel = RSG.SourceControl.Perforce.Label.GetLabel(p4, config.Project.Labels[Label.ToolsCurrent]);
            if (latestVersionLabel != null && !currentLabel.Matches(latestVersionLabel, p4))
            {
                MessageBoxResult result =
                    MessageBox.Show("The current label doesn't match the latest version label indicating that this script has been run more than" +
                    " once since the last 'current' label was created.\nDo you wish to continue?  " +
                    "Clicking 'Yes' will create a new version label.  Alternatively if you only wish to update the current 'testing' label click 'No' " +
                    "and run the update_testing_label.bat batch file.",
                    "Label Creation", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.No)
                {
                    return false;
                }
            }

            // Create the new label.
            RSG.SourceControl.Perforce.Label newVersionLabel =
                CreateNextToolsLabel(p4, config.Project.Labels[Label.ToolsIncremental], config.Project.Labels[Label.ToolsCurrent]);

            Debug.Assert(newVersionLabel != null, "Unable to continue as new version label wasn't created.");
            if (newVersionLabel == null)
            {
                // Inform the user that we can't continue.
                MessageBox.Show("Unable to continue as the script failed to create next label version.",
                    "Label Creation", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return false;
            }

            // Label sync the tools testing and new version labels.
            P4ExceptionLevels previousLevel = p4.ExceptionLevel;

            try
            {
                p4.ExceptionLevel = P4ExceptionLevels.ExceptionOnBothErrorsAndWarnings;
                p4.Run("labelsync", true, "-l", testingLabel.Name);
                p4.Run("labelsync", true, "-l", newVersionLabel.Name);
            }
            catch (System.Exception ex)
            {
                Log.Log__Exception(ex, "P4 exception while running 'labelsync' command.");
                MessageBox.Show(String.Format("The perforce 'labelsync' operation has failed while syncing either the '{0}' or '{1}' label.\n", testingLabel.Name, newVersionLabel.Name) +
                    "This is ok as long as you run the update_testing_label.bat batch file once this script finishes running.",
                    "Label Creation", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            finally
            {
                p4.ExceptionLevel = previousLevel;
            }

            // All went well
            return true;
        }

        /// <summary>
        /// Creates a new tools patch label, and label syncs both it and the tools patch testing labels.
        /// </summary>
        public static bool CreateNextPatchLabel(IConfig config, P4 p4)
        {
            // Check that the tools testing patch owner is set to the current p4 user.
            RSG.SourceControl.Perforce.Label patchTestingLabel = RSG.SourceControl.Perforce.Label.GetLabel(p4, config.Project.Labels[Label.ToolsPatchTesting]);
            if (patchTestingLabel.Owner != p4.User)
            {
                MessageBox.Show("Unable to continue as you are not the current owner of the tools patch testing label " +
                    String.Format("'{0}'.\nCurrent Owner: '{1}'.\n\n", patchTestingLabel.Name, patchTestingLabel.Owner) +
                    "Please set yourself to be the label's owner and then re-run this script.",
                    "Point Release Label Creation", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return false;
            }
            
            // Get the current version label.
            RSG.SourceControl.Perforce.Label currentVersionLabel =
                RSG.SourceControl.Perforce.Label.GetCurrentToolsVersionLabel(p4, config.Project.Labels[Label.ToolsIncremental], config.Project.Labels[Label.ToolsCurrent]);
            if (currentVersionLabel == null)
            {
                // Inform the user that we can't continue.
                MessageBox.Show("Unable to determine what the current label version is.  Do any major labels exist?",
                    "Point Release Label Creation", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return false;
            }

            // Create the next label version.
            RSG.SourceControl.Perforce.Label patchVersionLabel =
                CreateNextPatchToolsLabel(p4, currentVersionLabel.Name, config.Project.Labels[Label.ToolsIncremental]);

            Debug.Assert(patchVersionLabel != null, "Unable to continue as new patch version label wasn't created.");
            if (patchVersionLabel == null)
            {
                // Inform the user that we can't continue.
                MessageBox.Show("Unable to continue as the script failed to create next patch label version.",
                    "Point Release Label Creation", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return false;
            }

            // Label sync the tools testing and new version labels.
            P4ExceptionLevels previousLevel = p4.ExceptionLevel;

            try
            {
                p4.ExceptionLevel = P4ExceptionLevels.ExceptionOnBothErrorsAndWarnings;
                p4.Run("labelsync", true, "-l", patchTestingLabel.Name);
                p4.Run("labelsync", true, "-l", patchVersionLabel.Name);
            }
            catch (System.Exception ex)
            {
                Log.Log__Exception(ex, "P4 exception while running 'labelsync' command.");
                MessageBox.Show(String.Format("The perforce 'labelsync' operation has failed while syncing either the '{0}' or '{1}' label.\n", patchTestingLabel.Name, patchVersionLabel.Name) +
                    "This is ok as long as you run the update_testing_label.bat batch file once this script finishes running.",
                    "Point Release Label Creation", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            finally
            {
                p4.ExceptionLevel = previousLevel;
            }

            // All went well
            return true;
        }

        /// <summary>
        /// Label syncs the testing and latest tools label.
        /// </summary>
        public static bool UpdateCurrentTestingLabel(IConfig config, P4 p4)
        {
            // Check that the latest version label exists.
            RSG.SourceControl.Perforce.Label latestVersionLabel = GetLatestToolsLabel(p4, config.Project.Labels[Label.ToolsIncremental], true);
            if (latestVersionLabel == null)
            {
                MessageBox.Show("Latest version label doesn't exists.  Have you run the create_new_testing_label.bat batch file since the last time the current label was updated?",
                       "Updating Test Label", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return false;
            }

            // Check whether the current label matches the testing label?
            RSG.SourceControl.Perforce.Label testingLabel = RSG.SourceControl.Perforce.Label.GetLabel(p4, config.Project.Labels[Label.ToolsTesting]);
            RSG.SourceControl.Perforce.Label currentLabel = RSG.SourceControl.Perforce.Label.GetLabel(p4, config.Project.Labels[Label.ToolsCurrent]);
            if (currentLabel.Matches(testingLabel, p4))
            {
                MessageBox.Show("Label not updated as the current label matches the testing label.  Have you run the create_new_testing_label.bat batch file since the last time the current label was updated?",
                    "Updating Test Label", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return false;
            }

            // Check that the tools testing owner is set to the current p4 user.
            if (testingLabel.Owner != p4.User)
            {
                MessageBox.Show("Unable to continue as you are not the current owner of the tools testing label " +
                    String.Format("'{0}'.\nCurrent Owner: '{1}'.\n\n", testingLabel.Name, testingLabel.Owner) +
                    "Please set yourself to be the label's owner and then re-run this script.",
                    "Updating Test Label", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return false;
            }

            // Check that the latest version label owner is set to the current p4 user.
            if (latestVersionLabel.Owner != p4.User)
            {
                MessageBox.Show("Unable to continue as you are not the current owner of the latest version label " +
                    String.Format("'{0}'.\nCurrent Owner: '{1}'.\n\n", latestVersionLabel.Name, latestVersionLabel.Owner) +
                    "Please set yourself to be the label's owner and then re-run this script.",
                    "Updating Test Label", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return false;
            }

            // Label sync the tools testing and new version labels.
            P4ExceptionLevels previousLevel = p4.ExceptionLevel;

            try
            {
                p4.ExceptionLevel = P4ExceptionLevels.NoExceptionOnErrors;
                p4.Run("labelsync", true, "-l", testingLabel.Name);
                p4.Run("labelsync", true, "-l", latestVersionLabel.Name);
            }
            catch (System.Exception ex)
            {
                Log.Log__Exception(ex, "P4 exception while running 'labelsync' command.");
                MessageBox.Show(String.Format("The perforce 'labelsync' operation has failed while syncing either the '{0}' or '{1}' label.\n", config.Project.Labels[Label.ToolsTesting], latestVersionLabel.Name) +
                    "This could indicate a problem with perforce.  Please try rerunning this script.",
                    "Updating Test Label", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return false;
            }
            finally
            {
                p4.ExceptionLevel = previousLevel;
            }

            return true;
        }

        /// <summary>
        /// Label syncs the patch testing and latest patch label.
        /// </summary>
        public static bool UpdateCurrentPatchTestingLabel(IConfig config, P4 p4)
        {
            // Get the current version label.
            RSG.SourceControl.Perforce.Label currentVersionLabel =
                RSG.SourceControl.Perforce.Label.GetCurrentToolsVersionLabel(p4, config.Project.Labels[Label.ToolsIncremental], config.Project.Labels[Label.ToolsCurrent]);
            if (currentVersionLabel == null)
            {
                // Inform the user that we can't continue.
                MessageBox.Show("Unable to determine what the current label version is.  Do any major labels exist?",
                    "Updating Point Release Test Label", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return false;
            }

            // Get the tools patch testing label and the patch version label.
            RSG.SourceControl.Perforce.Label patchTestingLabel = RSG.SourceControl.Perforce.Label.GetLabel(p4, config.Project.Labels[Label.ToolsPatchTesting]);
            RSG.SourceControl.Perforce.Label currentLabel = RSG.SourceControl.Perforce.Label.GetLabel(p4, config.Project.Labels[Label.ToolsCurrent]);
            RSG.SourceControl.Perforce.Label patchVersionLabel = GetPatchVersionLabel(p4, currentVersionLabel.Name, config.Project.Labels[Label.ToolsIncremental]);

            // Check whether the current label matches the testing label?
            if (currentLabel.Matches(patchTestingLabel, p4))
            {
                MessageBox.Show("Label not updated as the current label matches the patch testing label.  Have you run the create_new_testing_label.bat batch file since the last time the current label was updated?",
                    "Updating Point Release Test Label", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return false;
            }

            // Check that the tools testing owner is set to the current p4 user.
            if (patchTestingLabel.Owner != p4.User)
            {
                MessageBox.Show("Unable to continue as you are not the current owner of the tools patch testing label " +
                    String.Format("'{0}'.\nCurrent Owner: '{1}'.\n\n", patchTestingLabel.Name, patchTestingLabel.Owner) +
                    "Please set yourself to be the label's owner and then re-run this script.",
                    "Updating Point Release Test Label", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return false;
            }

            // Check that the patch version label owner is set to the current p4 user.
            if (patchVersionLabel.Owner != p4.User)
            {
                MessageBox.Show("Unable to continue as you are not the current owner of the patch version label " +
                    String.Format("'{0}'.\nCurrent Owner: '{1}'.\n\n", patchVersionLabel.Name, patchVersionLabel.Owner) +
                    "Please set yourself to be the label's owner and then re-run this script.",
                    "Updating Point Release Test Label", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return false;
            }

            // Label sync the tools testing and new version labels.
            P4ExceptionLevels previousLevel = p4.ExceptionLevel;

            try
            {
                p4.ExceptionLevel = P4ExceptionLevels.NoExceptionOnErrors;
                p4.Run("labelsync", true, "-l", patchTestingLabel.Name);
                p4.Run("labelsync", true, "-l", patchVersionLabel.Name);
            }
            catch (System.Exception ex)
            {
                Log.Log__Exception(ex, "P4 exception while running 'labelsync' command.");
                MessageBox.Show(String.Format("The perforce 'labelsync' operation has failed while syncing either the '{0}' or '{1}' label.\n", patchTestingLabel.Name, patchVersionLabel.Name) +
                    "This could indicate a problem with perforce.  Please try rerunning this script.",
                    "Updating Test Label", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return false;
            }
            finally
            {
                p4.ExceptionLevel = previousLevel;
            }

            return true;
        }

        /// <summary>
        /// Sets the current tools label to the specified tools version.
        /// </summary>
        public static bool ReleaseToolsLabel(IConfig config, P4 p4, float labelVersion)
        {
            // Check that the local user is the owner of the current label.
            RSG.SourceControl.Perforce.Label currentLabel = RSG.SourceControl.Perforce.Label.GetLabel(p4, config.Project.Labels[Label.ToolsCurrent]);
            if (currentLabel.Owner != p4.User)
            {
                MessageBox.Show("Unable to continue as you are not the current owner of the tools current label " +
                    String.Format("'{0}'.\nCurrent Owner: '{1}'.\n\n", currentLabel.Name, currentLabel.Owner) +
                    "Please set yourself to be the label's owner and then re-run this script.",
                    "Releasing Label", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return false;
            }

            // Check that the requested version label exists.
            RSG.SourceControl.Perforce.Label versionLabel = GetToolsVersionLabel(p4, config.Project.Labels[Label.ToolsIncremental], labelVersion);
            if (versionLabel == null)
            {
                MessageBoxResult result =
                    MessageBox.Show("A tools label for version '{0}' doesn't exist.  Please make sure that the " +
                    "label exists before running the release_tools_label.bat script again.",
                    "Releasing Label", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return false;
            }

            // Update the version.xml file with the new label version.
            bool p4submit = true;
            if (!UpdateToolsLabelInConfig(config, versionLabel, true, p4submit))
            {
                MessageBox.Show("Unable to continue as the script failed to patch the new build version into the version.xml file.\n" +
                    "Please make sure that perforce isn't down and that you have the right permissions to edit the file before " +
                    "running the release_tools_label.bat script again.",
                    "Releasing Label", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return false;
            }

            // Make the current tools label equivalent to the version label.
            try
            {
                // Add the latest version.xml file to the version label.
                p4.Run("tag", true, "-l", versionLabel.Name, config.Version.Filename);

                // Then label sync the current label to the versioned label.
                // e.g.
                //p4 labelsync -l %CURRENT_LABEL% %PERFORCE_ROOT%/tools/...@%VERSION_LABEL%
                foreach (String view in currentLabel.View)
                {
                    p4.Run("labelsync", true, "-l", currentLabel.Name, String.Format("{0}@{1}", view, versionLabel.Name));
                }
            }
            catch (System.Exception ex)
            {
                Log.Log__Exception(ex, "P4 exception while running 'tag' or 'labelsync' command.");
                MessageBox.Show(String.Format("The perforce 'labelsync' operation has failed while syncing the '{0}' label to '{1}'.\n", currentLabel.Name, versionLabel.Name) +
                    "This means that the release didn't succeed.  Please make sure that perforce isn't currently down.",
                    "Releasing Label", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return false;
            }

            // Success!
            return true;
        }
        #endregion // Public Methods

        #region Private Methods
        /// <summary>
        /// Creates the next tools label.
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="incrementalLabelName"></param>
        private static RSG.SourceControl.Perforce.Label CreateNextToolsLabel(P4 p4, String incrementalLabelName, String templateLabelName)
        {
            Debug.Assert(p4.IsValidConnection(true, true));
            RSG.SourceControl.Perforce.Label label = null;

            try
            {
                uint currentMaxLabel = 0;

                // Check to see what the current highest label number is (if any exist).
                P4RecordSet labelRecords = p4.Run("labels", true, "-e", String.Format("{0}*", incrementalLabelName));
                foreach (P4Record labelRecord in labelRecords)
                {
                    String name = labelRecord["label"];

                    uint majorVersion, minorVersion;
                    if (RSG.SourceControl.Perforce.Label.GetToolsLabelVersion(name, incrementalLabelName, out majorVersion, out minorVersion))
                    {
                        currentMaxLabel = System.Math.Max(currentMaxLabel, majorVersion);
                    }
                }

                // Determine what the next label should be called and create it.
                String nextLabelName = String.Format("{0}{1}", incrementalLabelName, currentMaxLabel + 1);
                label = RSG.SourceControl.Perforce.Label.Create(p4, nextLabelName, templateLabelName);
            }
            catch (P4API.Exceptions.P4APIExceptions ex)
            {
                Log.Log__Exception(ex, "Perforce exception occurred while creating the next tools label.");
            }

            return label;
        }

        /// <summary>
        /// Create the next tools patch label.
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="releasedLabelName"></param>
        /// <returns></returns>
        private static RSG.SourceControl.Perforce.Label CreateNextPatchToolsLabel(P4 p4, String releasedLabelName, String incrementalLabelName)
        {
            Debug.Assert(p4.IsValidConnection(true, true));
            RSG.SourceControl.Perforce.Label label = null;

            try
            {
                uint majorVersion, minorVersion;
                if (RSG.SourceControl.Perforce.Label.GetToolsLabelVersion(releasedLabelName, incrementalLabelName, out majorVersion, out minorVersion))
                {
                    // Determine what the next label should be called and create it.
                    String nextLabelName = String.Format("{0}{1}.{2}", incrementalLabelName, majorVersion, minorVersion + 1);
                    label = RSG.SourceControl.Perforce.Label.Create(p4, nextLabelName, releasedLabelName);
                }
            }
            catch (P4API.Exceptions.P4APIExceptions ex)
            {
                Log.Log__Exception(ex, "Perforce exception occurred while creating the next tools patch label.");
            }
            
            return label;
        }

        /// <summary>
        /// Updates the tools version in the config file, optionally submitted the changes to p4.
        /// </summary>
        /// <param name="config">Config to update.</param>
        /// <param name="label">Label to update the config to.</param>
        /// <param name="p4edit">Whether to check the files out of perforce.</param>
        /// <param name="p4submit">Whether to submit the files to perforce.</param>
        private static bool UpdateToolsLabelInConfig(IConfig config, RSG.SourceControl.Perforce.Label label, bool p4edit = false, bool p4submit = false)
        {
            if (!File.Exists(config.Version.Filename))
            {
                Debug.Fail(String.Format("Version XML '{0}' does not exist.", config.Version.Filename));
                throw new ConfigurationException("Version XML file does not exist.")
                    {
                        Filename = config.Version.Filename
                    };
            }

            // Load up the file and make the required changes
            XDocument doc = XDocument.Load(config.Version.Filename);
            XElement versionElem = doc.Element(Config.ELEM_VERSION);
            if (versionElem == null)
            {
                Debug.Fail(String.Format("Version XML '{0}' does not have a valid root element.", config.Version.Filename));
                throw new ConfigurationException("Invalid root element.")
                    {
                        Filename = config.Version.Filename
                    };
            }

            versionElem.SetAttributeValue(Config.ATTR_LABEL, label.Name);

            // Check whether we should be checking out the files from p4.
            int changelistNumber = 0;
            using (P4 p4 = config.Project.SCMConnect())
            {
                if (p4edit)
                {
                    try
                    {
                        String changelistDescription = String.Format("Updating current tools label to {0}", label.Name);
                        P4API.P4PendingChangelist changelist = p4.CreatePendingChangelist(changelistDescription);
                        changelistNumber = changelist.Number;

                        p4.Run("sync", config.Version.Filename);
                        p4.Run("edit", "-c", changelistNumber.ToString(), config.Version.Filename);
                    }
                    catch (P4API.Exceptions.P4APIExceptions ex)
                    {
                        Log.Log__Exception(ex, "Unhandled Perforce exception while trying to edit the '{0}' config XML file.", config.Version.Filename);
                        return false;
                    }
                }

                // Save the changes to the config file and update the locally cached tools label.
                doc.Save(config.Version.Filename);

                // Check whether we should be submitting the files to p4.
                if (p4submit && changelistNumber != 0)
                {
                    try
                    {
                        // Submit the changelist reverting any unchanged files in the process
                        p4.Run("submit", "-c", changelistNumber.ToString(), "-f", "revertunchanged");
                    }
                    catch (P4API.Exceptions.P4APIExceptions ex)
                    {
                        Log.Log__Exception(ex, "Unhandled Perforce exception while trying to submit the changelist {0}.", changelistNumber);
                        return false;
                    }
                }
            }            

            return true;
        }

        /// <summary>
        /// Retrieves the latest versioned tools label.
        /// </summary>
        private static RSG.SourceControl.Perforce.Label GetLatestToolsLabel(P4 p4, String incrementalLabelName, bool majorOnly)
        {
            Debug.Assert(p4.IsValidConnection(true, true));
            uint currentMaxMajorLabel = 0;
            uint currentMaxMinorLabel = 0;
            String labelName = null;

            // Check to see what the current highest label number is (if any exist).
            P4RecordSet labelRecords = p4.Run("labels", true, "-e", String.Format("{0}*", incrementalLabelName));
            foreach (P4Record labelRecord in labelRecords)
            {
                String name = labelRecord["label"];
                uint major, minor;
                if (RSG.SourceControl.Perforce.Label.GetToolsLabelVersion(name, incrementalLabelName, out major, out minor))
                {
                    if (major == currentMaxMajorLabel)
                    {
                        if ((majorOnly && minor == 0) || (!majorOnly && minor > currentMaxMinorLabel))
                        {
                            currentMaxMajorLabel = major;
                            currentMaxMinorLabel = minor;
                            labelName = name;
                        }
                    }
                    else if (major > currentMaxMajorLabel)
                    {
                        if ((majorOnly && minor == 0) || !majorOnly)
                        {
                            currentMaxMajorLabel = major;
                            currentMaxMinorLabel = minor;
                            labelName = name;
                        }
                    }
                }
            }

            // Return the appropriate label.
            return (labelName == null ? null : RSG.SourceControl.Perforce.Label.GetLabel(p4, labelName));
        }

        /// <summary>
        /// Retrieves the latest patch version label.  It uses the second most recent 
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="incrementalLabelName"></param>
        /// <returns></returns>
        private static RSG.SourceControl.Perforce.Label GetPatchVersionLabel(P4 p4, String releasedLabelName, String incrementalLabelName)
        {
            Debug.Assert(p4.IsValidConnection(true, true));
            RSG.SourceControl.Perforce.Label label = null;

            try
            {
                uint majorVersion, minorVersion;
                if (RSG.SourceControl.Perforce.Label.GetToolsLabelVersion(releasedLabelName, incrementalLabelName, out majorVersion, out minorVersion))
                {
                    // Determine what the next label should be called and create it.
                    String patchVersionLabelName = String.Format("{0}{1}.{2}", incrementalLabelName, majorVersion, minorVersion + 1);
                    label = RSG.SourceControl.Perforce.Label.GetLabel(p4, patchVersionLabelName);
                }
            }
            catch (P4API.Exceptions.P4APIExceptions ex)
            {
                Log.Log__Exception(ex, "Perforce exception occurred while creating the next tools patch label.");
            }

            return label;

        }

        /// <summary>
        /// Retrieves a particular tools version label.
        /// </summary>
        private static RSG.SourceControl.Perforce.Label GetToolsVersionLabel(P4 p4, String incrementalLabelName, float version)
        {
            Debug.Assert(p4.IsValidConnection(true, true));
            String labelName = null;

            // Check to see what the current highest label number is (if any exist).
            P4RecordSet labelRecords = p4.Run("labels", true, "-e", String.Format("{0}{1:0.#}", incrementalLabelName, version), "-m", "1");
            if (labelRecords.Records.Any())
            {
                P4Record labelRecord = labelRecords.Records.First();
                labelName = labelRecord["label"];
            }

            // Return the appropriate label.
            return (labelName == null ? null : RSG.SourceControl.Perforce.Label.GetLabel(p4, labelName));
        }
        #endregion // Private Methods
    } // ToolsRelease
}
