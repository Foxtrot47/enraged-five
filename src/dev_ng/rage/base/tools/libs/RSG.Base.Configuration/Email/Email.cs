﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Base.Windows;

namespace RSG.Base.Configuration.Email
{
    /// <summary>
    /// Email utility 
    /// - config driven
    /// - this class provides for a simple email (config driven) interface for sending emails that can be easily
    /// triggered from a script.
    /// </summary>
    public static class Email
    {
        /// <summary>
        /// Send an email (with attachments).
        /// </summary>
        public static void SendEmail(IConfig config, String[] recipients, String subject, String body, IEnumerable<Attachment> attachments)
        {
            MailMessage mail = new MailMessage();
            // These are used if there is an exception
            String recipientsStr = "";
            String attachmentsStr = "";
            try
            {
                foreach (String recipient in recipients)
                {
                    mail.To.Add(new MailAddress(recipient));
                    recipientsStr += recipient + "\n";
                }

                if (attachments != null)
                {
                    mail.Attachments.AddRange(attachments);
                    attachmentsStr = String.Join("\n", attachments.Select(item => item.Name));
                }

                mail.Body = body;
                mail.Subject = subject;
                mail.From = new MailAddress(config.EmailAddress);

                // Need to use the studio specific exchange server to send the email.
                IStudio thisStudio = config.Studios.ThisStudio;

                String server = thisStudio.ExchangeServer;
#warning MET: Hard coded SMTP port, should it be encoded in config file?
                int port = 25;
                SmtpClient client = new SmtpClient(server, port);
                client.Send(mail);
            }
            catch (SmtpException)
            {
                String message = String.Format(
                    "E-mail failed to send, either the server is down or a firewall issue.  You can send it manually:\n\nRecipient(s) were:\n{0}\nBody was:\n{1}\n\nAttachments were:\n{2}",
                    recipientsStr, body, attachmentsStr);
                System.Windows.MessageBox.Show(message, "Error sending e-mail!");
            }
            catch (Exception ex)
            {
                ExceptionStackTraceDlg dlg = new ExceptionStackTraceDlg(ex,
                    "RSGEDI Tools", "*tools@rockstarnorth.com");
                dlg.ShowDialog();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static void SendEmail(IConfig config, String[] recipients, String subject, String body, String[] attachments)
        {
            SendEmail(config, recipients, subject, body, attachments.Select(item => new Attachment(item)));
        }
    }

} // RSG.UniversalLog.Util namespace
