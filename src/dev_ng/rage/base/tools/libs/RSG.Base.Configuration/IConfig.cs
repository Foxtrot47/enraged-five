﻿using System;
using System.Collections.Generic;

namespace RSG.Base.Configuration
{

    /// <summary>
    /// Entry Configuration class for system-wide globals for current tools 
    /// branch.  This includes the project context for the current tools branch.
    /// </summary>
    /// The globals just come from the user-environment.  Rather than 
    /// recalculate then here like we used to do.  To switch project tools you 
    /// need to re-run the installer anyways.
    public interface IConfig : 
        IHasEnvironment
    {
        #region Properties
        #region Core Tools Variables
        /// <summary>
        /// Tools root directory.
        /// </summary>
        String ToolsRoot { get; }

        /// <summary>
        /// Tools root drive letter.
        /// </summary>
        String ToolsDrive { get; }

        /// <summary>
        /// Tools binary directory.
        /// </summary>
        String ToolsBin { get; }

        /// <summary>
        /// Path to Tools Installer.
        /// </summary>
        String ToolsInstaller { get; }

        /// <summary>
        /// Tools library files directory.
        /// </summary>
        String ToolsLib { get; }

        /// <summary>
        /// Tools Ruby interpreter absolute path.
        /// </summary>
        String ToolsRuby { get; }

        /// <summary>
        /// Tools IronRuby interpreter absolute path.
        /// </summary>
        String ToolsIronRuby { get; }

        /// <summary>
        /// Tools temporary files absolute path.
        /// </summary>
        String ToolsTemp { get; }

        /// <summary>
        /// Tools log file directory absolute path.
        /// </summary>
        String ToolsLogs { get; }

        /// <summary>
        /// Tools configuration data absolute path.
        /// </summary>
        String ToolsConfig { get; }
        #endregion // Core Tools Variables

        /// <summary>
        /// XML configuration filename.
        /// </summary>
        String Filename { get; }

        /// <summary>
        /// XML configuration file version.
        /// </summary>
        [Obsolete("Use 'BaseToolsVersion'/'ToolsVersion' properties instead.")]
        int ConfigVersion { get; }

        /// <summary>
        /// Tools version information.
        /// </summary>
        IToolsVersion Version { get; }

        /// <summary>
        /// Active project object (can be DLC project).
        /// </summary>
        IProject Project { get; }

        /// <summary>
        /// Core project (never DLC).
        /// </summary>
        IProject CoreProject { get; }

        /// <summary>
        /// Episodic projects; piggybacking these tools.
        /// </summary>
        IDictionary<String, IProject> DLCProjects { get; }

        /// <summary>
        /// Available user types collection.
        /// </summary>
        ICollection<IUsertype> Usertypes { get; }

        /// <summary>
        /// Available studio collection.
        /// </summary>
        StudioCollection Studios { get; }

        #region User Local Data (Set by Installer)
        /// <summary>
        /// Last install time.
        /// </summary>
        DateTime InstallTime { get; }

        /// <summary>
        /// User's username.
        /// </summary>
        String Username { get; }

        /// <summary>
        /// User's email address.
        /// </summary>
        String EmailAddress { get; }

        /// <summary>
        /// User's type flags.
        /// </summary>
        UInt32 Usertype { get; }

        /// <summary>
        /// Version control enabled flag.
        /// </summary>
        bool VersionControlEnabled { get; }

        /// <summary>
        /// User version control settings.
        /// </summary>
        IEnumerable<IVersionControlSettings> VersionControlSettings { get; }
        #endregion // User Local Data (Set by Installer)
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Import settings into an IEnvironment object.
        /// </summary>
        /// <param name="environment"></param>
        void Import(IEnvironment environment);

        /// <summary>
        /// Switch configuration to a keyed DLC project (null or "" to default).
        /// </summary>
        /// <param name="dlcKey"></param>
        void SwitchDLC(String dlcKey);

        /// <summary>
        /// Reload all of the configuration data.
        /// </summary>
        /// <param name="dlcKey"></param>
        void Reload(String dlcKey);
        
        /// <summary>
        /// Determine the Perforce state for the tools directory; whether user
        /// is on the label etc.
        /// </summary>
        /// <param name="quickCheck">True indicates that it only checks the latest version
        /// against that in config.xml. False indicates that a full p4 diff is performed.</param>
        /// <returns></returns>
        PerforceLabelState GetToolsPerforceLabelState(bool quickCheck = false);

        /// <summary>
        /// All projects : core and dlc
        /// </summary>
        /// <returns></returns>
        IDictionary<String, IProject> AllProjects();

        #endregion // Methods
    }

} // RSG.Base.Configuration namespace
