﻿using System;
using System.Collections.Generic;
using System.Xml;
using RSG.SourceControl.Perforce;

namespace RSG.Base.Configuration
{
    
    /// <summary>
    /// Project encapsulation; representing a project defining a set of
    /// branches and targets.
    /// </summary>
    public interface IProject : 
        IHasEnvironment,
        IEnumerable<IBranch>
    {
        #region Properties
        /// <summary>
        /// Project name.
        /// </summary>
        String Name { get; }

        /// <summary>
        /// Project friendly UI name.
        /// </summary>
        String FriendlyName { get; }

        /// <summary>
        /// Project root directory $(root).
        /// </summary>
        String Root { get; }

        /// <summary>
        /// Project configuration filename.
        /// </summary>
        String Filename { get; }

        /// <summary>
        /// Project cache directory $(cache).
        /// </summary>
        String Cache { get; }

        /// <summary>
        /// Whether this project is episodic.
        /// </summary>
        bool IsDLC { get; }

        /// <summary>
        /// Whether this project has levels.
        /// </summary>
        bool HasLevels { get; }

        /// <summary>
        /// Force flags collection.
        /// </summary>
        IForceFlags ForceFlags { get; }

        /// <summary>
        /// Asset prefixes collection.
        /// </summary>
        IAssetPrefix AssetPrefix { get; }

        /// <summary>
        /// Locally configured default branch name for this project.
        /// </summary>
        String DefaultBranchName { get; }

        /// <summary>
        /// Locally configured default branch for this project.
        /// </summary>
        IBranch DefaultBranch { get; }

        /// <summary>
        /// Collection of default platforms, these can be used when the current default branch
        /// has no active targets in it.
        /// </summary>
        IDictionary<RSG.Platform.Platform, bool> DefaultPlatforms { get; }
        
        /// <summary>
        /// Config object parent.
        /// </summary>
        IConfig Config { get; }

        /// <summary>
        /// Enumerable of Tools-associated mail addresses (for sending logs and bug reports).
        /// </summary>
        IEnumerable<System.Net.Mail.MailAddress> ToolsEmailAddresses { get; }

        /// <summary>
        /// Collection of branches for this project.
        /// </summary>
        IDictionary<String, IBranch> Branches { get; }

        /// <summary>
        /// Dictionary of available labels for this project.
        /// </summary>
        IDictionary<Label, String> Labels { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Import settings into an IEnvironment object.
        /// </summary>
        /// <param name="environment"></param>
        void Import(IEnvironment environment);

        /// <summary>
        /// Reload all of the configuration data.
        /// </summary>
        void Reload();

        /// <summary>
        /// Return Perforce source control object for this project.
        /// </summary>
        /// <returns></returns>
        /// The returned object is owned by the caller; and needs disposing.
        P4 SCMConnect();

        /// <summary>
        /// Return Perforce source control object for this project's replica.
        /// </summary>
        /// <returns></returns>
        /// The returned object is owned by the caller; and needs disposing.
        /// 
        /// Scripts and applications that require readonly Perforce access
        /// that is not guaranteed to be available can use this to avoid
        /// loading on the main Perforce server.
        P4 ReplicaSCMConnect();

        /// <summary>
        /// PostInitialisation flush, for $(core) based keys handling.
        /// </summary>
        void PostEnvironmentInit();
        #endregion // Methods
    }

} // RSG.Base.Configuration namespace
