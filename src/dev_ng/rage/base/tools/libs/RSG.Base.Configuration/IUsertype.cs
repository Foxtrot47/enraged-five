﻿using System;

namespace RSG.Base.Configuration
{

    /// <summary>
    /// Usertype abstraction interface.
    /// </summary>
    public interface IUsertype
    {
        #region Properties
        /// <summary>
        /// Name.
        /// </summary>
        String Name { get; }

        /// <summary>
        /// Friendly name; for UI display.
        /// </summary>
        String FriendlyName { get; }

        /// <summary>
        /// Bit flags for this user.
        /// </summary>
        UInt32 Flags { get; }

        /// <summary>
        /// Pseudo user; unselectable in installer?
        /// </summary>
        bool Pseudo { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Return whether this is an automation user.
        /// </summary>
        /// <returns></returns>
        bool IsAutomationUser();
        #endregion // Methods
    }

} // RSG.Base.Configuration namespace
