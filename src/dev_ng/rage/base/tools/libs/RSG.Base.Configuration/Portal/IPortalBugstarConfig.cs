﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Configuration.Portal
{
    /// <summary>
    /// Portal Bugstar configuration interface.
    /// </summary>
    public interface IPortalBugstarConfig
    {
        #region Properties
        /// <summary>
        /// Project's Bugstar ID
        /// </summary>
        String BugstarID { get; }
        #endregion // Properties
    }

} // RSG.Base.Configuration.Portal namespace