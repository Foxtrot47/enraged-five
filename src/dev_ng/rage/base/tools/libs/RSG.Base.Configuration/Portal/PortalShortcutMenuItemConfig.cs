﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RSG.Base.Configuration.Portal
{

    /// <summary>
    /// Automation Service configuration object.
    /// </summary>
    internal class PortalShortcutMenuItemConfig : IPortalShortcutMenuItemConfig
    {
        #region Properties
        /// <summary>
        /// Shortcut Menu Item Name.
        /// </summary>
        public String Name
        {
            get;
            private set;
        }

        /// <summary>
        /// Shortcut Menu Item Guid.
        /// </summary>
        public Guid Guid
        {
            get;
            private set;
        }
        #endregion // Properties
        
        #region Static Controller Methods
        /// <summary>
        /// Load Portal Shortcut Menu Item configuration from an XML element.
        /// </summary>
        /// <param name="project"></param>
        /// <param name="xmlElem"></param>
        /// <returns></returns>
        public static PortalShortcutMenuItemConfig Load(IProject project, XElement xmlElem)
        {
            Debug.Assert(xmlElem.Name.LocalName.Equals("ShortcutMenuItem"),
                                        "Invalid Service XML element.");
            if (!xmlElem.Name.LocalName.Equals("ShortcutMenuItem"))
                                        throw (new ArgumentException("Invalid Service XML element.", "xmlElem"));

            PortalShortcutMenuItemConfig config = new PortalShortcutMenuItemConfig();
            config.Name = xmlElem.Element("Name").Value;
            config.Guid = new Guid(xmlElem.Element("Guid").Value);
           
            return (config);
        }
        #endregion // Static Controller Methods
    }

} // RSG.Base.Configuration.Portal namespace
