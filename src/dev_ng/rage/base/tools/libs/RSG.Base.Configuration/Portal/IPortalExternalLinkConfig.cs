﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Configuration.Portal
{
    /// <summary>
    /// Portal External Links configuration interface.
    /// </summary>
    public interface IPortalExternalLinkConfig
    {
        #region Properties
        /// <summary>
        /// External Link Name.
        /// </summary>
        String Name
        {
            get;
        }

        /// <summary>
        /// External Link URI.
        /// </summary>
        Uri URI
        {
            get;
        }
        #endregion // Properties
    }

} // RSG.Base.Configuration.Portal namespace