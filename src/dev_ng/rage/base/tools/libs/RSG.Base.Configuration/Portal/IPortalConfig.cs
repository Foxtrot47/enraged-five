﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Configuration.Portal
{
    /// <summary>
    /// Portal Configuration interface.
    /// </summary>
    public interface IPortalConfig
    {
        #region Properties
        /// <summary>
        /// Project's Full Name for the Portal
        /// </summary>
        String Name { get; }

        /// <summary>
        /// Project's Short Name for the Portal
        /// </summary>
        String ShortName { get; }

        /// <summary>
        /// Project's Route Name for the URI
        /// </summary>
        String RouteName { get; }

        /// <summary>
        /// Project's Logo Path (Relative to the Web Server)
        /// </summary>
        String LogoPath { get; }

        /// <summary>
        /// Project's Icon Path (Relative to the Web Server)
        /// </summary>
        String IconPath { get; }

        /// <summary>
        /// Portal Periodical Update Time Interval in Milliseconds
        /// </summary>
        int UpdateInterval { get;}

        /// <summary>
        /// Project's Bugstar Configuration Details
        /// </summary>
        IEnumerable<IPortalBugstarConfig> BugstarConfigs { get; }

        /// <summary>
        /// Portal Statistic Servers configuration details.
        /// </summary>
        IEnumerable<IPortalStatisticServerConfig> StatisticServers { get; }

        /// <summary>
        /// Portal Shortcut Menu Items configuration details.
        /// </summary>
        IEnumerable<IPortalShortcutMenuItemConfig> ShortcutMenuItems { get; }

        /// <summary>
        /// Portal External Links configuration details.
        /// </summary>
        IEnumerable<IPortalExternalLinkConfig> ExternalLinks { get; }
        #endregion // Properties

        #region Public Methods
        /// <summary>
        /// Get the Portal URI (Current Project)
        /// </summary>
        /// <returns></returns>
        Uri GetPortalUri();

        /// <summary>
        /// Get The Portal's Project URI (Current Project)
        /// </summary>
        /// <returns></returns>
        Uri GetPortalProjectUri();

        /// <summary>
        /// Get The Portal's Automation URI (Current Project)
        /// </summary>
        /// <returns></returns>
        Uri GetPortalAutomationUri();

        /// <summary>
        /// Get The Portal's Automation URI For a Specific Server
        /// </summary>
        /// <param name="webAlias"></param>
        /// <returns></returns>
        Uri GetPortalAutomationUri(String webAlias);

        /// <summary>
        /// Get The Portal's Automation URI For a Specific Server
        /// If the JobId is passed then the View's filter is pre-populated
        /// </summary>
        /// <param name="webAlias"></param>
        /// <param name="jobId"></param>
        /// <returns></returns>
        Uri GetPortalAutomationUri(String webAlias, Guid jobId);

        /// <summary>
        /// Get The Portal's Automation Monitor URI For a Specific Server
        /// </summary>
        /// <param name="webAlias"></param>
        /// <returns></returns>
        Uri GetPortalAutomationMonitorUri(String webAlias);

        /// <summary>
        /// Get The Portal's Automation Job Matrix URI For a Specific Server
        /// </summary>
        /// <param name="webAlias"></param>
        /// <returns></returns>
        Uri GetPortalAutomationMatrixUri(String webAlias);

        /// <summary>
        /// Get The Portal's GameBuild Versions URI (Current Project)
        /// </summary>
        /// <returns></returns>
        Uri GetPortalGameBuildVersionsUri();

        /// <summary>
        /// Get The Portal's Tools Versions URI (Current Project)
        /// </summary>
        /// <returns></returns>
        Uri GetPortalToolsVersionsUri();

        /// <summary>
        /// Get The Portal's Ulog URI (Project Independent)
        /// </summary>
        /// <returns></returns>
        Uri GetPortalUlogUri();

        /// <summary>
        /// Get The Portal's Ulog URI for a specific job (Project Independent)
        /// </summary>
        /// <param name="serverHost"></param>
        /// <param name="jobID"></param>
        /// <param name="errorsOnly"></param>
        /// <returns></returns>
        Uri GetPortalUlogUri(String serverHost, Guid jobId, bool errorsOnly = false);
        #endregion // Public Methods
    }
}  // RSG.Base.Configuration.Portal namespace
 