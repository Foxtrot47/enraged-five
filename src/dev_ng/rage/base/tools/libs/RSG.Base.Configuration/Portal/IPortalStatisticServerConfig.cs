﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Configuration.Portal
{
    /// <summary>
    /// Portal Statistic Server configuration interface.
    /// </summary>
    public interface IPortalStatisticServerConfig
    {
        #region Properties
        /// <summary>
        /// Statistic Server Name.
        /// </summary>
        String Name
        {
            get;
        }

        /// <summary>
        /// Statistic Server WebHost.
        /// </summary>
        Uri WebHost
        {
            get;
        }
        #endregion // Properties
    }

} // RSG.Base.Configuration.Portal namespace
