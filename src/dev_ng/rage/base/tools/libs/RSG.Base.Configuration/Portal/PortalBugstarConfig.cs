﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RSG.Base.Configuration.Portal
{
    /// <summary>
    /// Portal Bugstar configuration object.
    /// </summary>
    class PortalBugstarConfig : IPortalBugstarConfig
    {
        #region Properties
        /// <summary>
        /// Project's Bugstar ID
        /// </summary>
        public String BugstarID { get; private set; }
        #endregion // Properties

        #region Static Controller Methods
        /// <summary>
        /// Load Portal Bugstar ID configuration from an XML element.
        /// </summary>
        /// <param name="xmlElem"></param>
        /// <returns></returns>
        public static PortalBugstarConfig Load(IProject project, XElement xmlElem)
        {
            Debug.Assert(xmlElem.Name.LocalName.Equals("BugstarID"),
                                        "Invalid Service XML element.");
            if (!xmlElem.Name.LocalName.Equals("BugstarID"))
                throw (new ArgumentException("Invalid Service XML element.", "xmlElem"));

            PortalBugstarConfig config = new PortalBugstarConfig();
            config.BugstarID = xmlElem.Value;

            return (config);
        }

        #endregion // Static Controller Methods
    }
} // RSG.Base.Configuration.Portal namespace
