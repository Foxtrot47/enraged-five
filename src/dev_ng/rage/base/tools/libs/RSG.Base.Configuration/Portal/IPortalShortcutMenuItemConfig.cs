﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Configuration.Portal
{
    /// <summary>
    /// Portal Shortcut Menu Item configuration interface.
    /// </summary>
    public interface IPortalShortcutMenuItemConfig
    {
        #region Properties
        /// <summary>
        /// Shortcut Menu Item Name.
        /// </summary>
        String Name
        {
            get;
        }

        /// <summary>
        /// Shortcut Menu Item Guid.
        /// </summary>
        Guid Guid
        {
            get;
        }
        #endregion // Properties
    }

} // RSG.Base.Configuration.Portal namespace