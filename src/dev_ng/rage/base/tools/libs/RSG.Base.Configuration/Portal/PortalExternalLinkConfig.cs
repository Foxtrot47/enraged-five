﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RSG.Base.Configuration.Portal
{

    /// <summary>
    /// Portal External Links configuration object.
    /// </summary>
    internal class PortalExternalLinkConfig : IPortalExternalLinkConfig
    {
        #region Properties
        /// <summary>
        /// External Link Name.
        /// </summary>
        public String Name
        {
            get;
            private set;
        }

        /// <summary>
        /// External Link URI.
        /// </summary>
        public Uri URI
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Static Controller Methods
        /// <summary>
        /// Load Portal Shortcut Menu Item configuration from an XML element.
        /// </summary>
        /// <param name="project"></param>
        /// <param name="xmlElem"></param>
        /// <returns></returns>
        public static PortalExternalLinkConfig Load(IProject project, XElement xmlElem)
        {
            Debug.Assert(xmlElem.Name.LocalName.Equals("ExternalLink"),
                                        "Invalid Service XML element.");
            if (!xmlElem.Name.LocalName.Equals("ExternalLink"))
                throw (new ArgumentException("Invalid Service XML element.", "xmlElem"));

            PortalExternalLinkConfig config = new PortalExternalLinkConfig();
            config.Name = xmlElem.Element("Name").Value;
            config.URI = new Uri(xmlElem.Element("URI").Value);

            return (config);
        }
        #endregion // Static Controller Methods
    }

} // RSG.Base.Configuration.Portal namespace
