﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace RSG.Base.Configuration.Portal
{

    /// <summary>
    /// Portal Statistic Server configuration object.
    /// </summary>
    internal class PortalStatisticServerConfig : IPortalStatisticServerConfig
    {
        #region Properties
        /// <summary>
        /// Statistic Server Name.
        /// </summary>
        public String Name
        {
            get;
            private set;
        }

        /// <summary>
        /// Statistic Server WebHost.
        /// </summary>
        public Uri WebHost
        {
            get;
            private set;
        }
        #endregion // Properties
        
        #region Static Controller Methods
        /// <summary>
        /// Load Portal Statistic Server configuration from an XML element.
        /// </summary>
        /// <param name="project"></param>
        /// <param name="xmlElem"></param>
        /// <returns></returns>
        public static PortalStatisticServerConfig Load(IProject project, XElement xmlElem)
        {
            Debug.Assert(xmlElem.Name.LocalName.Equals("StatisticServer"),
                "Invalid Service XML element.");
            if (!xmlElem.Name.LocalName.Equals("StatisticServer"))
                throw (new ArgumentException("Invalid Service XML element.", "xmlElem"));

            PortalStatisticServerConfig config = new PortalStatisticServerConfig();
            config.Name = xmlElem.Element("Name").Value;
            config.WebHost = new Uri(xmlElem.Element("WebHost").Value);
           
            return (config);
        }
        #endregion // Static Controller Methods
    }

} // RSG.Base.Configuration.Portal namespace
