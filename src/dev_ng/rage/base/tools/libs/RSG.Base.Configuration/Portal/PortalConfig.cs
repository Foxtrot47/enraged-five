﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Xml.Linq;
using System.Text.RegularExpressions;

namespace RSG.Base.Configuration.Portal
{
    /// <summary>
    /// Portal Configuration object.
    /// </summary>
    internal class PortalConfig : IPortalConfig
    {
        #region Properties
        /// <summary>
        /// Project's Full Name for the Portal
        /// </summary>
        public String Name { get; private set; }

        /// <summary>
        /// Project's Short Name for the Portal
        /// </summary>
        public String ShortName { get; private set; }

        /// <summary>
        /// Project's Route Name for the URI
        /// </summary>
        public String RouteName
        {
            get { return Regex.Replace(this.ShortName.Replace(@"'", String.Empty), @"[^\w]+", "-"); }
        }

        /// <summary>
        /// Project's Logo Path (Relative to the Web Server)
        /// </summary>
        public String LogoPath { get; private set; }

        /// <summary>
        /// Project's Icon Path (Relative to the Web Server)
        /// </summary>
        public String IconPath { get; private set; }

        /// <summary>
        /// Portal Periodical Update Time Interval in Milliseconds
        /// </summary>
        public int UpdateInterval { get; private set; }

        /// <summary>
        /// Project's Bugstar Configuration Details
        /// </summary>
        public IEnumerable<IPortalBugstarConfig> BugstarConfigs { get; private set; }

        /// <summary>
        /// Portal Statistic Servers configuration details.
        /// </summary>
        public IEnumerable<IPortalStatisticServerConfig> StatisticServers { get; private set; }

        /// <summary>
        /// Portal Shortcut Menu Items configuration details.
        /// </summary>
        public IEnumerable<IPortalShortcutMenuItemConfig> ShortcutMenuItems { get; private set; }

        /// <summary>
        /// Portal External Links configuration details.
        /// </summary>
        public IEnumerable<IPortalExternalLinkConfig> ExternalLinks { get; private set; }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Portal Host
        /// </summary>
        private Uri _host;

        /// <summary>
        /// Portal Project Controller
        /// </summary>
        private String _projectController;

        /// <summary>
        /// Portal Project Route Name
        /// </summary>
        private String _projectRouteName;

        /// <summary>
        /// Portal Project Automation Action
        /// </summary>
        private String _automationAction;

        /// <summary>
        /// Portal Project Automation Monitor Action
        /// </summary>
        private String _automationMonitorAction;

        /// <summary>
        /// Portal Project Automation Job Matrix Action
        /// </summary>
        private String _automationMatrixAction;

        /// <summary>
        /// Portal Game Build Versions Automation Action
        /// </summary>
        private String _gameBuildVersionsAction;

        /// <summary>
        /// Portal Tools Versions Action
        /// </summary>
        private String _toolsVersionsAction;

        /// <summary>
        /// Portal Ulog Action
        /// </summary>
        private String _ulogAction;

        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="statisticServers"></param>
        /// <param name="shortcutMenuItems"></param>
        public PortalConfig(Uri host, String projectController, String projectRouteName, String automationAction,
                            String automationMonitorAction, String automationMatrixAction,
                            String gameBuildVersionsAction, String toolsVersionAction, String ulogAction,
                            String name, String shortName, String logoPath, String iconPath, int updateInterval,
                            IEnumerable<IPortalBugstarConfig> bugstarConfigs,
                            IEnumerable<IPortalStatisticServerConfig> statisticServers,
                            IEnumerable<IPortalShortcutMenuItemConfig> shortcutMenuItems,
                            IEnumerable<IPortalExternalLinkConfig> externalLinks)
        {
            this._host = host;
            this._projectController = projectController;
            this._projectRouteName = projectRouteName;
            this._automationAction = automationAction;
            this._automationMonitorAction = automationMonitorAction;
            this._automationMatrixAction = automationMatrixAction;
            this._gameBuildVersionsAction = gameBuildVersionsAction;
            this._toolsVersionsAction = toolsVersionAction;
            this._ulogAction = ulogAction;

            this.Name = name;
            this.ShortName = shortName;
            this.LogoPath = logoPath;
            this.IconPath = iconPath;
            this.UpdateInterval = updateInterval;

            this.BugstarConfigs = bugstarConfigs;
            this.StatisticServers = statisticServers;
            this.ShortcutMenuItems = shortcutMenuItems;
            this.ExternalLinks = externalLinks;
        }
        #endregion // Constructor(s)

        #region Static Controller Methods
        /// <summary>
        /// Load Portal configuration for a project.
        /// </summary>
        /// <param name="project"></param>
        /// <param name="projectPath">Override the default Project's Path</param>
        /// <returns></returns>
        public static IPortalConfig Load(IProject project, String projectPath = null)
        {
            String fullPath = String.Empty;
            if (projectPath != null)
            {
                fullPath = projectPath;
            }
            else
            {
                fullPath = project.Config.ToolsConfig;
            }
            String filename = Path.Combine(fullPath, "portal.xml");

            return (LoadXML(project, filename));
        }

        /// <summary>
        /// Load Portal configuration from a file.
        /// </summary>
        /// <param name="project"></param>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static IPortalConfig LoadXML(IProject project, String filename)
        {
            XDocument xmlDoc = XDocument.Load(filename);
            
            Uri host = new Uri(xmlDoc.Root.Element("Host").Value);
            String projectController = xmlDoc.Root.Element("ProjectController").Value;
            String projectRouteName = xmlDoc.Root.Element("ProjectRouteName").Value;
            String automationAction = xmlDoc.Root.Element("AutomationAction").Value;
            String automationMonitorAction = xmlDoc.Root.Element("AutomationMonitorAction").Value;
            String automationMatrixAction = xmlDoc.Root.Element("AutomationMatrixAction").Value;
            String gameBuildVersionsAction = xmlDoc.Root.Element("GameBuildVersionsAction").Value;
            String toolsVersionAction = xmlDoc.Root.Element("ToolsVersionsAction").Value;
            String ulogAction = xmlDoc.Root.Element("UlogAction").Value;
            int updateInterval = Int32.Parse(xmlDoc.Root.Element("UpdateInterval").Value);

            String name = xmlDoc.Root.Element("Name").Value;
            String shortName = xmlDoc.Root.Element("ShortName").Value;
            String logoPath = xmlDoc.Root.Element("LogoPath").Value;
            String iconPath = xmlDoc.Root.Element("IconPath").Value;

            IEnumerable<XElement> xmlBugstarConfigElems = xmlDoc.Root.Element("BugstarIDs").Elements("BugstarID");
            IEnumerable<IPortalBugstarConfig> bugstarConfigs =
                                        xmlBugstarConfigElems.Select(x => PortalBugstarConfig.Load(project, x));

            IEnumerable<XElement> xmlStatisticServiceElems =
                                        xmlDoc.Root.Element("StatisticServers").Elements("StatisticServer");
            IEnumerable<IPortalStatisticServerConfig> statisticServices =
                                        xmlStatisticServiceElems.Select(x => PortalStatisticServerConfig.Load(project, x));

            IEnumerable<XElement> xmlShortcutMenuItems = xmlDoc.Root.Element("ShortcutMenuItems").Elements("ShortcutMenuItem");
            IEnumerable<IPortalShortcutMenuItemConfig> shortcutMenuItems =
                                        xmlShortcutMenuItems.Select(x => PortalShortcutMenuItemConfig.Load(project, x));

            IEnumerable<XElement> xmlExternalLinks = xmlDoc.Root.Element("ExternalLinks").Elements("ExternalLink");
            IEnumerable<IPortalExternalLinkConfig> externalLinks =
                                        xmlExternalLinks.Select(x => PortalExternalLinkConfig.Load(project, x));

            return (new PortalConfig(host, projectController, projectRouteName, automationAction, 
                                    automationMonitorAction, automationMatrixAction, gameBuildVersionsAction,
                                    toolsVersionAction, ulogAction, name, shortName, logoPath, iconPath, updateInterval, 
                                    bugstarConfigs, statisticServices, shortcutMenuItems, externalLinks));
        }
        #endregion // Static Controller Methods

        #region Public Methods
        /// <summary>
        /// Get the Portal URI (Current Project)
        /// </summary>
        /// <returns></returns>
        public Uri GetPortalUri() {
            return (this._host);
        }

        /// <summary>
        /// Get The Portal's Project URI (Current Project)
        /// </summary>
        /// <returns></returns>
        public Uri GetPortalProjectUri()
        {
            String webPath = String.Format("{0}/{1}/{2}", this.GetPortalUri(), this._projectController, this._projectRouteName);

            return (new Uri(webPath));
        }

        /// <summary>
        /// Get The Portal's Automation URI (Current Project)
        /// </summary>
        /// <returns></returns>
        public Uri GetPortalAutomationUri()
        {
            String webPath = String.Format("{0}/{1}", this.GetPortalProjectUri(), this._automationAction);
            return (new Uri(webPath));
        }

        /// <summary>
        /// Get The Portal's Automation URI For a Specific Server
        /// </summary>
        /// <param name="webAlias"></param>
        /// <returns></returns>
        public Uri GetPortalAutomationUri(String webAlias)
        {
            String webPath = String.Format("{0}/{1}", this.GetPortalAutomationUri(), webAlias);
            return (new Uri(webPath));
        }

        /// <summary>
        /// Get The Portal's Automation URI For a Specific Server
        /// If the JobId is passed then the View's filter is pre-populated
        /// </summary>
        /// <param name="webAlias"></param>
        /// <param name="jobId"></param>
        /// <returns></returns>
        public Uri GetPortalAutomationUri(String webAlias, Guid jobId)
        {
            String webPath = String.Format("{0}?JobId={1}", this.GetPortalAutomationUri(webAlias), jobId);
            return (new Uri(webPath));
        }

        /// <summary>
        /// Get The Portal's Automation Monitor URI For a Specific Server
        /// </summary>
        /// <param name="webAlias"></param>
        /// <returns></returns>
        public Uri GetPortalAutomationMonitorUri(String webAlias)
        {
            String webPath = String.Format("{0}/{1}/{2}", this.GetPortalProjectUri(), this._automationMonitorAction, webAlias);
            return (new Uri(webPath));
        }

        /// <summary>
        /// Get The Portal's Automation Job Matrix URI For a Specific Server
        /// </summary>
        /// <param name="webAlias"></param>
        /// <returns></returns>
        public Uri GetPortalAutomationMatrixUri(String webAlias)
        {
            String webPath = String.Format("{0}/{1}/{2}", this.GetPortalProjectUri(), this._automationMatrixAction, webAlias);
            return (new Uri(webPath));
        }

        /// <summary>
        /// Get The Portal's GameBuild Versions URI (Current Project)
        /// </summary>
        /// <returns></returns>
        public Uri GetPortalGameBuildVersionsUri()
        {
            String webPath = String.Format("{0}/{1}", this.GetPortalProjectUri(), this._gameBuildVersionsAction);
            return (new Uri(webPath));
        }

        /// <summary>
        /// Get The Portal's Tools Versions URI (Current Project)
        /// </summary>
        /// <returns></returns>
        public Uri GetPortalToolsVersionsUri()
        {
            String webPath = String.Format("{0}/{1}", this.GetPortalProjectUri(), this._toolsVersionsAction);
            return (new Uri(webPath));
        }

        /// <summary>
        /// Get The Portal's Ulog URI (Project Independent)
        /// </summary>
        /// <returns></returns>
        public Uri GetPortalUlogUri()
        {
            String webPath = String.Format("{0}/{1}", this._host, this._ulogAction);
            return (new Uri(webPath));
        }

        /// <summary>
        /// Get The Portal's Ulog URI for a specific job (Project Independent)
        /// </summary>
        /// <param name="ServerHost"></param>
        /// <param name="jobID"></param>
        /// <returns></returns>
        public Uri GetPortalUlogUri(String serverHost, Guid jobId, bool errorsOnly = false)
        {
            String queryString = String.Format("?ServerHost={0}&JobId={1}&ErrorsOnly={2}", serverHost, jobId, errorsOnly);
            return (new Uri(GetPortalUlogUri(), queryString));
        }
        #endregion // Public Methods
    }

}// RSG.Base.Configuration.Portal namespace
