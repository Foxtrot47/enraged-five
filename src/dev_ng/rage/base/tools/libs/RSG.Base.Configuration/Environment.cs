﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using RSG.Base.Extensions;

namespace RSG.Base.Configuration
{

    /// <summary>
    /// Environment class to allow variable string substitution.
    /// </summary>
    internal class Environment : 
        IEnvironment,
        IEnumerable<KeyValuePair<String, String>>
    {
        #region Properties
        /// <summary>
        /// Indexer based on environment variable name.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public String this[String key]
        {
            get 
            {
                Dictionary<String, String> top = this.Variables.Peek();
                return (top[key.ToLower()]); 
            }
        }

        /// <summary>
        /// Current depth of environment stack (minimum 1).
        /// </summary>
        public int Depth
        {
            get
            {
                Debug.Assert(this.Variables.Count >= 1,
                    "Internal error: variable stack is empty!!");
                return (this.Variables.Count);
            }
        }

        /// <summary>
        /// Local dictionary of Name -> Value pairs.
        /// </summary>
        /// This should not be public; use the Add/Remove method for validating
        /// entries.
        private Stack<Dictionary<String, String>> Variables;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public Environment()
        {
            this.Variables = new Stack<Dictionary<String, String>>();
            this.Variables.Push(new Dictionary<String, String>());
        }

        /// <summary>
        /// Constructor; importing environment from configuration object.
        /// </summary>
        /// <param name="config"></param>
        public Environment(IConfig config)
            : this()
        {
            config.Import(this);
        }

        /// <summary>
        /// Constructor; importing environment from project object.
        /// </summary>
        /// <param name="project"></param>
        public Environment(IProject project)
            : this()
        {
            project.Import(this);
        }

        /// <summary>
        /// Constructor; importing environment from branch object.
        /// </summary>
        /// <param name="branch"></param>
        public Environment(IBranch branch)
            : this()
        {
            branch.Import(this);
        }

        /// <summary>
        /// Constructor; importing environment from target object.
        /// </summary>
        /// <param name="target"></param>
        public Environment(ITarget target)
            : this()
        {
            target.Import(this);
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Add a new value into the environment.
        /// </summary>
        /// <param name="variable"></param>
        /// <param name="value"></param>
        public void Add(String variable, String value)
        {
            if (String.IsNullOrEmpty(value))
                Config.Log.Warning("Variable '{0}' being added to Environment is empty.", variable);

            this.Add(variable, value, BracketType.Default);
        }      
        /// <summary>
        /// Add a new value into the environment; using specified bracket type.
        /// </summary>
        /// <param name="variable"></param>
        /// <param name="value"></param>
        /// <param name="bracket"></param>
        /// This method is only required for the Texture Pipeline's use of
        /// ${RS_ASSETS}.  *** It should not be used anywhere else. ***
        /// 
        public void Add(String variable, String value, BracketType bracket = BracketType.Default)
        {
            Debug.Assert(this.Variables != null && this.Variables.Count > 0);
            Debug.Assert(!variable.StartsWith("$("), "Do not use the $() syntax for variables.  Automatically applied.");
            if (variable.StartsWith("$("))
                throw new ArgumentException("Do not use the $() syntax for variables.  Automatically applied.");

            Dictionary<String, String> top = this.Variables.Peek();
            String var = String.Empty;
            switch (bracket)
            {
                case BracketType.AlternateCurly:
                    var = String.Format("${{{0}}}", variable);
                    break;
                case BracketType.Default:
                default:
                    var = String.Format("$({0})", variable);
                    break;
            }
            if (!top.ContainsKey(var))
                top.Add(var, value);
            else
                top[var] = value;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="variable"></param>
        /// <returns></returns>
        public String Lookup(String variable)
        {
            return (this[variable]);
        }

        /// <summary>
        /// Substitute a string with any environment variables.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public String Subst(String value)
        {
            if (String.IsNullOrEmpty(value))
                return (String.Empty);

            String substValue = value;

            bool stillEnvVarToSubst = true;
            while (stillEnvVarToSubst)
            {
                stillEnvVarToSubst = false;
                foreach (var envVar in this.Variables.Peek())
                {
                    if (substValue.Contains(envVar.Key))
                    {
                        substValue = substValue.Replace(envVar.Key, envVar.Value);
                        stillEnvVarToSubst = true;
                    }
                }
            }

            return (substValue);
        }

        /// <summary>
        /// Reverse substitution; absolute path to environment variables.  Attempts
        /// to match longest string-value first (for file and directory names).
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public String ReverseSubst(String value)
        {
            if (String.IsNullOrEmpty(value))
                return (String.Empty);

            String pathValueToReverseSubst = value;

            try
            {
                // Build and sort subst dictionary values by length; we want to match the
                // longest string-value first.  We expand the values to ensure we handle 
                // multiple environment variables and get the right length.
                List<KeyValuePair<String, String>> values = new List<KeyValuePair<String, String>>();
                foreach (KeyValuePair<String, String> kvp in this.Variables.Peek())
                {
                    String substValue = this.Subst(kvp.Value);
                    // Make sure the path is rooted, so we don't include DLC uiname values etc which break with Path.GetFullPath
                    if (Path.IsPathRooted(substValue))
                    {
                        values.Add(new KeyValuePair<String, String>(kvp.Key, Path.GetFullPath(substValue)));
                    }
                }
                values.Sort(
                    delegate(KeyValuePair<String, String> first, KeyValuePair<String, String> second)
                    {
                        // Inverted compare to ensure longest first.
                        return (second.Value.Length.CompareTo(first.Value.Length));
                    }
                );

                pathValueToReverseSubst = Path.GetFullPath(value);
                bool replaced = false;
                do
                {
                    replaced = false;
                    foreach (KeyValuePair<String, String> kvp in values)
                    {
                        if (String.IsNullOrEmpty(kvp.Value))
                            continue; // Ignore empty values previously warned about!

                        if (!(pathValueToReverseSubst.StartsWith(kvp.Value, StringComparison.OrdinalIgnoreCase)))
                            continue;
                        if (kvp.Key == "${RS_ASSETS}") // fuck ${RS_ASSETS}
                            continue;
                        pathValueToReverseSubst = pathValueToReverseSubst.Replace(kvp.Value, kvp.Key, StringComparison.OrdinalIgnoreCase);
                        replaced = true;
                    }

                    if (!replaced)
                        break;
                } while (replaced);
            }
            catch (System.Exception ex)
            {
                Config.Log.Warning("Exception while trying ReverseSubst for path '{0}': {1}", value, ex.Message.ToString());
            }
            return (pathValueToReverseSubst);
        }

        /// <summary>
        /// Determines whether there are substitutions required.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool RequiresSubst(String value)
        {
            if (String.IsNullOrEmpty(value))
                return (false);

            return (value.Contains("$(") || value.Contains("${"));
        }

        /// <summary>
        /// Resets the entire environment.
        /// </summary>
        public void Reset()
        {
            this.Variables = new Stack<Dictionary<String, String>>();
            this.Variables.Push(new Dictionary<String, String>());
        }

        /// <summary>
        /// Clear the top-level environment of settings.
        /// </summary>
        public void Clear()
        {
            Dictionary<String, String> top = this.Variables.Peek();
            top.Clear();
        }

        /// <summary>
        /// Push a new context.
        /// </summary>
        public void Push()
        {
            Debug.Assert(this.Variables != null && this.Variables.Count > 0);
            Dictionary<String, String> top = this.Variables.Peek();
            this.Variables.Push(new Dictionary<String, String>(top));
        }

        /// <summary>
        /// Pop last context.
        /// </summary>
        public bool Pop()
        {
            Debug.Assert(this.Variables != null && this.Variables.Count > 1,
                "Attempting to pop the last environment on the stack.");
            if (1 == this.Variables.Count)
                return (false);

            this.Variables.Pop();
            return (true);
        }
        #endregion // Controller Methods

        #region IEnumerable<KeyValuePair<String, String>> Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerator<KeyValuePair<String, String>> GetEnumerator()
        {
            foreach (KeyValuePair<String, String> kvp in this.Variables.Peek())
                yield return kvp;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return (GetEnumerator());
        }
        #endregion // IEnumerable<KeyValuePair<String, String>> Methods
    }

} // RSG.Base.Configuration namespace
