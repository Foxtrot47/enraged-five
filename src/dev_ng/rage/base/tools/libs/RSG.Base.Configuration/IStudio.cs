﻿using System;
using System.Xml.Linq;

namespace RSG.Base.Configuration
{

    /// <summary>
    /// Studio encapsulation.
    /// </summary>
    public interface IStudio
    {
        #region Properties
        /// <summary>
        /// Studio key name.
        /// </summary>
        String Name { get; }

        /// <summary>
        /// Studio friendly name (for UI display).
        /// </summary>
        String FriendlyName { get; }

        /// <summary>
        /// Network subnet range (e.g. "10.11.16.0/20").
        /// </summary>
        String SubnetRange { get; }

        /// <summary>
        /// Default Perforce server and port for the studio.
        /// </summary>
        String DefaultPerforceServer { get; }

        /// <summary>
        /// Perforce read-only replica server and port for the studio.
        /// </summary>
        String PerforceReplicaServer { get; }

        /// <summary>
        /// Perforce web server.
        /// </summary>
        String PerforceWebServer { get; }

        /// <summary>
        /// Shared network drive resource.
        /// </summary>
        String NetworkDrive { get; }
        
        /// <summary>
        /// Exchange server URI.
        /// </summary>
        String ExchangeServer { get; }

        /// <summary>
        /// Studio domain URI.
        /// </summary>
        String Domain { get; }

        /// <summary>
        /// Studio Active Directory domain (for login authentication).
        /// </summary>
        String ActiveDirectoryDomain { get; }

        /// <summary>
        /// User name of automated builder user.
        /// </summary>
        String BuilderUser { get; }

        /// <summary>
        /// Friendly name of automated builder user.
        /// </summary>
        String BuilderName { get; }

        /// <summary>
        /// Time Zone identifier (based on studio location).
        /// </summary>
        String TimeZoneId { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Return TimeZoneInfo object for this studio.
        /// </summary>
        /// <returns></returns>
        TimeZoneInfo GetTimeZoneInfo();
        #endregion // Methods
    }

} // RSG.Base.Configuration namespace
