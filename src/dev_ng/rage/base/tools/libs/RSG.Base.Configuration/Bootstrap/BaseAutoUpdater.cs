﻿using System.IO;
using System.Diagnostics;
using System;
using System.Collections.Generic;

namespace RSG.Base.Configuration.Bootstrap
{
    /// <summary>
    /// Base updater class.
    /// </summary>
    [Obsolete("Use bootstrap.rb (to be replaced with exe soon) instead.", true)]
    public abstract class BaseAutoUpdater
    {
        #region Private member fields

        private string m_executable;
        private List<string> m_filePaths;
        private string[] m_args;
        private bool m_forceRestart;

        #endregion

        #region Public events

        /// <summary>
        /// Confirm the restart request. Set the forceRestart constructor parameter to true if you are going to ignore
        /// this event.
        /// </summary>
        public event ConfirmationEventHandler ConfirmRestart;

		/// <summary>
        /// Inform subscribers that a restart is about to happen.
        /// </summary>
		public event EventHandler Restarting;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor. Does not add the executable to the list of files to be watched.
        /// </summary>
        /// <param name="pathToExe">Executable's path.</param>
        /// <param name="forceRestart">True if this is a script or other automated task and cannot require user intervention.</param>
        /// <param name="arguments">Command line arguments passed to the application at startup.</param>
        public BaseAutoUpdater(string pathToExe, bool forceRestart, string[] arguments)
            : this(pathToExe, false, forceRestart, arguments)
        {

        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="pathToExe">Executable's path.</param>
        /// <param name="addExecutable">Add the executable to the list of files watched.</param>
        /// <param name="forceRestart">True if this is a script or other automated task and cannot require user intervention.</param>
        /// <param name="arguments">Command line arguments passed to the application at startup.</param>
        public BaseAutoUpdater(string pathToExe, bool addExecutable, bool forceRestart, string[] arguments)
        {
            m_executable = pathToExe;
            m_forceRestart = forceRestart;
            m_filePaths = new List<string>();
            if (addExecutable)
            {
                AddWatchableFile(m_executable);
            }

            m_args = arguments;

            DeleteDummy(pathToExe);
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Add files to the watch list.
        /// </summary>
        /// <param name="filePaths">File paths.</param>
        public void AddWatchableFiles(IEnumerable<string> filePaths)
        {
            foreach (string s in filePaths)
            {
                AddWatchableFile(s);
            }
        }

        /// <summary>
        /// Add a single file to the watch list. If you don't specify a @label, @date or #rev, #head will be prefixed to the file path.
        /// </summary>
        /// <param name="filePath">File path.</param>
        public void AddWatchableFile(string filePath)
        {
            if (!filePath.Contains("@") && !filePath.Contains("#"))
            {
                filePath = filePath + "#head";
            }

            m_filePaths.Add(filePath);
        }

        /// <summary>
        /// Initialise the class.
        /// </summary>
        public virtual void Init()
        {

        }

        /// <summary>
        /// Restart the application.
        /// </summary>
        public void Restart()
        {
            m_filePaths.ForEach(file => DeleteDummy(file));

            if (!String.IsNullOrEmpty(m_filePaths.Find(file => file.IndexOf(m_executable, StringComparison.OrdinalIgnoreCase) == 0)))
            {
                // If we're watching the executable, we can't sync it because the OS locks it. The trick is to move the file, the OS knows
                // the new name of the file and is fooled into allowing us to sync the file.
                Move(m_executable);
            }

            if (Restarting != null)
            {
                Restarting(this, EventArgs.Empty);
            }
			
            OnBeforeRestart();

            Process p = new Process();
            p.StartInfo.Arguments = String.Join(" ", m_args);
            p.StartInfo.FileName = m_executable;
            p.Start();
            System.Environment.Exit(0);
        }

        #endregion

        #region Protected methods

        /// <summary>
        /// Get the files that are to be watched for changes.
        /// </summary>
        /// <returns>The enumerable list of files.</returns>
        protected IEnumerable<string> GetFiles()
        {
            foreach (string file in m_filePaths)
            {
                yield return file;
            }
        }

        /// <summary>
        /// Called when the background worker detects that the file is out of date.
        /// </summary>
        protected void OnRestartRequest()
        {
            bool restart = m_forceRestart;

            if (ConfirmRestart != null)
            {
                ConfirmEventArgs args = new ConfirmEventArgs(m_forceRestart);
                ConfirmRestart(this, args);
                restart = args.Proceed;
            }

            if (restart)
            {
                Restart();
            }
        }

        /// <summary>
        /// Fired just before the program is restarted.
        /// </summary>
        protected virtual void OnBeforeRestart()
        {

        }

        #endregion

        #region Private helper methods

        /// <summary>
        /// Deletes the dummy version of the file.
        /// </summary>
        /// <param name="pathToFile">Path to the file.</param>
        private void DeleteDummy(string pathToFile)
        {
            string pathToDummy = CreateDummyPath(pathToFile);

            try
            {
                File.Delete(pathToDummy);
            }
            catch (IOException)
            {

            }
        }

        private void Move(string pathToFile)
        {
            string pathToDummy = CreateDummyPath(pathToFile);
            File.Move(pathToFile, pathToDummy);
        }

        protected string CreateDummyPath(string pathToFile)
        {
            string pathToDummy = Path.GetFileNameWithoutExtension(pathToFile) + ".old";
            pathToDummy = Path.Combine(Path.GetDirectoryName(pathToFile), pathToDummy);
            return pathToDummy;
        }

        #endregion
    }
}
