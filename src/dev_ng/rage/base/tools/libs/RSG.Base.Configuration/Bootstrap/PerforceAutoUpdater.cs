﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using P4API;
using System.Threading;
using System.Windows;
using RSG.SourceControl.Perforce;

namespace RSG.Base.Configuration.Bootstrap
{
    /// <summary>
    /// Perforce auto-updater class.
    /// </summary>
    [Obsolete("Use bootstrap.rb (to be replaced with exe soon) instead.", true)]
    public class PerforceAutoUpdater : BaseAutoUpdater
    {
        #region Private member fields

        private int m_tickTime;
        private BackgroundWorker m_worker;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="pathToExe">Executable's path.</param>
        /// <param name="addExecutable">Add the executable to the list of files to watch.</param>
        /// <param name="forceRestart">True if this is a script or other automated task and cannot require user intervention.</param>
        /// <param name="arguments">Command line arguments passed to the application at startup.</param>
        /// <param name="tickTime">Tick time (in seconds) that the updater should check for a new version of the file.</param>
        public PerforceAutoUpdater(string pathToExe, bool addExecutable, bool forceRestart, string[] arguments, int tickTime)
            : base(pathToExe, addExecutable, forceRestart, arguments)
        {
            m_tickTime = tickTime;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="pathToExe">Executable's path.</param>
        /// <param name="forceRestart">True if this is a script or other automated task and cannot require user intervention.</param>
        /// <param name="arguments">Command line arguments passed to the application at startup.</param>
        /// <param name="tickTime">Tick time (in seconds) that the updater should check for a new version of the file.</param>
        public PerforceAutoUpdater(string pathToExe, bool forceRestart, string[] arguments, int tickTime)
            : base(pathToExe, forceRestart, arguments)
        {
            m_tickTime = tickTime;
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Start a background task to continually check the status of the files.
        /// </summary>
        public override void Init()
        {
            m_worker = new BackgroundWorker();
            m_worker.DoWork += new DoWorkEventHandler(Worker_DoWork);
            m_worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(Worker_RunWorkerCompleted);
            m_worker.RunWorkerAsync();
        }

        #endregion

        #region Protected overrides

        /// <summary>
        /// Just before the program restarts, force get the latest version of it from Perforce.
        /// </summary>
        protected override void OnBeforeRestart()
        {
            P4 p4 = new P4();
            p4.Connect();

            foreach (string file in GetFiles())
            {
                p4.Run("sync", "-f", file);
            }

            p4.Disconnect();
        }

        #endregion

        #region Worker event handlers

        /// <summary>
        /// Wait until the revision of the given file changes.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private void Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            AutoResetEvent wait = new AutoResetEvent(false);
            P4 p4 = new P4();

            int ticks = m_tickTime * 1000;
            p4.Connect();

            while (!FileHasChanged(p4))
            {
                wait.WaitOne(ticks);
            }

            p4.Disconnect();
        }

        /// <summary>
        /// Signal the owning class that a restart has been requested.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private void Worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            m_worker.DoWork -= Worker_DoWork;
            m_worker.RunWorkerCompleted -= Worker_RunWorkerCompleted;

            OnRestartRequest();
        }

        #endregion

        #region Private helper methods

        /// <summary>
        /// Checks each of the files in turn. If at least one has changed, this method returns true.
        /// </summary>
        /// <param name="p4">P4 connection.</param>
        /// <returns>True if at least one of the watched files has changed.</returns>
        private bool FileHasChanged(P4 p4)
        {
            foreach (string file in GetFiles())
            {
                int haveRevision = 0;
                int headRevision = GetHeadRevision(p4, file, ref haveRevision);
                if (haveRevision != headRevision)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Get the head / have revisions.
        /// </summary>
        /// <param name="p4">Perforce connection.</param>
        /// <param name="fileName">File name.</param>
        /// <param name="haveRevision">The have revision number.</param>
        /// <returns>The head revision number.</returns>
        private int GetHeadRevision(P4 p4, string fileName, ref int haveRevision)
        {
            try
            {
                FileState fileState = new FileState(p4, fileName);
                haveRevision = fileState.HaveRevision;
                return fileState.HeadRevision;
            }
            catch
            {
                haveRevision = 0;
                return 0;
            }
        }

        #endregion
    }
}
