﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Configuration.Bootstrap
{
    /// <summary>
    /// Confirmation event delegate.
    /// </summary>
    /// <param name="sender">Sender.</param>
    /// <param name="e">Confirmation event arguments.</param>
    [Obsolete("Use bootstrap.rb (to be replaced with exe soon) instead.", true)]
    public delegate void ConfirmationEventHandler(object sender, ConfirmEventArgs e);

    /// <summary>
    /// Confirmation event arguments.
    /// </summary>
    [Obsolete("Use bootstrap.rb (to be replaced with exe soon) instead.", true)]
    public class ConfirmEventArgs
    {
        #region Public properties

        /// <summary>
        /// True if the action should proceed.
        /// </summary>
        public bool Proceed { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="defaultValue">Default value.</param>
        public ConfirmEventArgs(bool defaultValue)
        {
            Proceed = defaultValue;
        }

        #endregion
    }
}
