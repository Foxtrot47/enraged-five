﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.IO;
using System.Xml.Linq;
using RSG.Base.Configuration;
using RSG.Base.Windows.Dialogs;
using RSG.Base.Logging;

namespace RSG.Base.Configuration.ROS
{
    /// <summary>
    /// ROS (Rockstar Online Services) Configuration Data implementation.
    /// </summary>
    internal class ROSConfig : ConfigBase, IROSConfig
    {
        #region Constants
        internal const String ELEM_ROS           = "ros";
        internal const String ELEM_AUTH_SERVICES = "authservices";
        internal const String ELEM_GAME_SERVICES = "gameservices";
        internal const String ELEM_GLOBAL_SERVICES = "globalservices";
        internal const String ELEM_SERVICE       = "service";
        internal const String ELEM_PLATFORMS     = "platforms";
        internal const String ELEM_PLATFORM      = "platform";

        internal const String ATTR_ENVIRONMENT   = "environment";
        internal const String ATTR_SERVER        = "server";
        internal const String ATTR_VERSION       = "version";
        internal const String ATTR_TITLE         = "title";
        internal const String ATTR_USESSL        = "usessl";
        internal const String ATTR_USERNAME      = "username";
        internal const String ATTR_PASSWORD      = "password";

        internal const String ATTR_SERVICE_ID    = "id";
        internal const String ATTR_SERVICE_URL   = "relative_url";
        internal const String ATTR_PLATFORM_NAME = "name";
        #endregion // Constants

        #region Fields
        /// <summary>
        /// Private field for the <see cref="Filename"/> property.
        /// </summary>
        private readonly String _filename;
        #endregion // Fields

        #region Properties
        /// <summary>
        /// Associated project.
        /// </summary>
        public IProject Project { get; private set; }

        /// <summary>
        /// XML configuration filename.
        /// </summary>
        public String Filename
        {
            get { return _filename; }
        }
        
        /// <summary>
        /// Local XML configuration filename.
        /// </summary>
        public String LocalFilename { get; private set; }

        /// <summary>
        /// ROS environment we will be connecting to (e.g. dev, cert, prod, etc...).
        /// </summary>
        public String ServerEnvironment { get; protected set; }

        /// <summary>
        /// ROS server address.
        /// </summary>
        public String Server { get; protected set; }

        /// <summary>
        /// Version of the ROS services to use.
        /// </summary>
        public uint Version { get; protected set; }

        /// <summary>
        /// Name of the title to use when querying ROS.
        /// </summary>
        public String GameTitle { get; protected set; }

        /// <summary>
        /// Flag indicating whether we should use SSL when connecting to ROS.
        /// </summary>
        public bool UseSSL { get; protected set; }

        /// <summary>
        /// Username to use to connect to the ROS services.
        /// </summary>
        public String Username { get; protected set; }

        /// <summary>
        /// Password to use to connect to the ROS services.
        /// </summary>
        public String Password { get; protected set; }

        /// <summary>
        /// Collection of authentication services (maps service name to relative urls).
        /// </summary>
        public IDictionary<String, String> AuthServiceUrls { get; protected set; }

        /// <summary>
        /// Collection of game services (maps service name to relative urls).
        /// </summary>
        public IDictionary<String, String> GameServiceUrls { get; protected set; }

        /// <summary>
        /// Collection of global services (maps service name to relative urls).
        /// </summary>
        public IDictionary<String, String> GlobalServiceUrls { get; protected set; }

        /// <summary>
        /// List of platforms that ROS supports for the current title.
        /// TODO: This should really map to our ROSPlatforms enum.
        /// </summary>
        public ICollection<String> Platforms { get; protected set; }

        /// <summary>
        /// Project environment; inheriting from Config.
        /// </summary>
        public IEnvironment Environment { get; protected set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public ROSConfig(IProject project)
        {
            this.Project = project;
            this.Environment = new RSG.Base.Configuration.Environment();
            _filename = Path.Combine(this.Project.Config.ToolsConfig, "ros.xml");

            // Read in all the settings
            Reload();
        }
        #endregion // Constructor(s)
        
        #region Methods
        /// <summary>
        /// Reload all the configuration data.
        /// </summary>
        public void Reload()
        {
            // Get the file that we wish to load and ensure that it exists
            if (!File.Exists(Filename))
            {
                Debug.Fail(String.Format("ROS Xml '{0}' does not exist.", Filename));
                throw new ConfigurationException("ROS XML file does not exist.")
                    {
                        Filename = this.Filename
                    };
            }

            // Reset certain items
            AuthServiceUrls = new Dictionary<String, String>();
            GameServiceUrls = new Dictionary<String, String>();
            GlobalServiceUrls = new Dictionary<String, String>();
            Platforms = new List<String>();

            // Load and parse the ros config file
            XDocument doc = XDocument.Load(Filename);
            ParseConfig(doc, false);

            // Load in any details from the local config
            LocalFilename = Path.Combine(this.Project.Config.ToolsConfig, "local", "ros_login.xml");
            if (File.Exists(LocalFilename))
            {
                XDocument localDoc = XDocument.Load(LocalFilename);
                ParseConfig(localDoc, true);
            }

            // If the user hasn't entered in any username/password details
            if (String.IsNullOrEmpty(Username) || String.IsNullOrEmpty(Password))
            {
                Login();
            }

            // If they are still null (user hit cancel) throw an exception
            if (String.IsNullOrEmpty(Username) || String.IsNullOrEmpty(Password))
            {
                throw new ConfigurationException("Config is invalid as no username/password was found.")
                    {
                        Filename = this.Filename
                    };
            }
        }

        /// <summary>
        /// Allows the user to save out the config to file
        /// </summary>
        /// <param name="local"></param>
        public void Save(bool local)
        {
            Debug.Assert(local, "It's only possible to save out local ROS data.");
            if (local == false)
            {
                throw new ArgumentException("It's only possible to save out local ROS data.");
            }

            // Save out the ros login details
            if (!Directory.Exists(Path.GetDirectoryName(LocalFilename)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(LocalFilename));
            }

            XDocument doc = new XDocument(
                new XElement("ros",
                    new XAttribute("username", Username),
                    new XAttribute("password", Password)));
            doc.Save(LocalFilename);
        }

        /// <summary>
        /// 
        /// </summary>
        public void ClearLoginDetails()
        {
            Username = "";
            Password = "";
        }
        #endregion // Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="local"></param>
        private void ParseConfig(XDocument doc, bool local)
        {
            XElement root = doc.Root;

            ParseMainAttributes(root, local);

            XElement authServices = root.Element(ELEM_AUTH_SERVICES);
            if (authServices != null)
            {
                ParseAuthServices(authServices.Elements(ELEM_SERVICE));
            }

            XElement gameServices = root.Element(ELEM_GAME_SERVICES);
            if (gameServices != null)
            {
                ParseGameServices(gameServices.Elements(ELEM_SERVICE));
            }

            XElement globalServices = root.Element(ELEM_GLOBAL_SERVICES);
            if (globalServices != null)
            {
                ParseGlobalServices(globalServices.Elements(ELEM_SERVICE));
            }
            
            XElement platforms = root.Element(ELEM_PLATFORMS);
            if (platforms != null)
            {
                ParsePlatforms(platforms.Elements(ELEM_PLATFORM));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="elem"></param>
        private void ParseMainAttributes(XElement elem, bool local)
        {
            if (!local)
            {
                ServerEnvironment = ReadAttribute<String>(elem, ATTR_ENVIRONMENT);
                Server = ReadAttribute<String>(elem, ATTR_SERVER);
                GameTitle = ReadAttribute<String>(elem, ATTR_TITLE);
                Environment.Add(ATTR_TITLE, GameTitle);

                Version = ReadAttribute<UInt32>(elem, ATTR_VERSION);
                Environment.Add(ATTR_VERSION, Version.ToString());

                UseSSL = ReadAttribute<bool>(elem, ATTR_USESSL);
            }

            if (local)
            {
                Username = ReadAttribute<String>(elem, ATTR_USERNAME);
                Password = ReadAttribute<String>(elem, ATTR_PASSWORD);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="elements"></param>
        private void ParseAuthServices(IEnumerable<XElement> elements)
        {
            if (elements.Count() > 0)
            {
                // Clear out any services if they already exist
                AuthServiceUrls.Clear();
            }

            // Process all the services that are in the config
            foreach (XElement elem in elements)
            {
                String id = ReadAttribute<String>(elem, ATTR_SERVICE_ID);
                String url = ReadAttribute<String>(elem, ATTR_SERVICE_URL);
                AuthServiceUrls.Add(id, Environment.Subst(url));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="elements"></param>
        private void ParseGameServices(IEnumerable<XElement> elements)
        {
            if (elements.Count() > 0)
            {
                // Clear out any services if they already exist
                GameServiceUrls.Clear();
            }

            // Process all the services that are in the config
            foreach (XElement elem in elements)
            {
                String id = ReadAttribute<String>(elem, ATTR_SERVICE_ID);
                String url = ReadAttribute<String>(elem, ATTR_SERVICE_URL);
                GameServiceUrls.Add(id, Environment.Subst(url));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="elements"></param>
        private void ParseGlobalServices(IEnumerable<XElement> elements)
        {
            if (elements.Count() > 0)
            {
                // Clear out any services if they already exist
                GlobalServiceUrls.Clear();
            }

            // Process all the services that are in the config
            foreach (XElement elem in elements)
            {
                String id = ReadAttribute<String>(elem, ATTR_SERVICE_ID);
                String url = ReadAttribute<String>(elem, ATTR_SERVICE_URL);
                GlobalServiceUrls.Add(id, Environment.Subst(url));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="elements"></param>
        private void ParsePlatforms(IEnumerable<XElement> elements)
        {
            if (elements.Count() > 0)
            {
                // Clear out any platforms if they already exist
                Platforms.Clear();
            }

            // Process all the platforms that are in the config
            foreach (XElement elem in elements)
            {
                String name = ReadAttribute<String>(elem, ATTR_PLATFORM_NAME);
                Platforms.Add(name);
            }
        }

        /// <summary>
        /// Prompts the user to login into ROS
        /// </summary>
        private void Login()
        {
            string title = "ROS Login";
            string message = "Please enter your ros login details below:";

            LoginViewModel vm = new LoginViewModel(title, message);

            LoginWindow loginWindow = new LoginWindow(vm);
            if (loginWindow.ShowDialog() == true)
            {
                Username = vm.Username;
                Password = vm.Password;
            }
            else
            {
                Log.Log__Error("ROS login canceled by user.  We will not be able to connect to the server.");
            }
        }
        #endregion // Private Methods
    } // Config

} // RSG.Base.Configuration.ROS namespace
