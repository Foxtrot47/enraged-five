﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Configuration;

namespace RSG.Base.Configuration.ROS
{
    /// <summary>
    /// 
    /// </summary>
    public interface IROSConfig : IHasEnvironment
    {
        #region Properties
        /// <summary>
        /// Associated project.
        /// </summary>
        IProject Project { get; }

        /// <summary>
        /// ROS environment we will be connecting to (e.g. dev, cert, prod, etc...).
        /// </summary>
        string ServerEnvironment { get; }

        /// <summary>
        /// ROS server address.
        /// </summary>
        string Server { get; }

        /// <summary>
        /// Version of the ROS services to use.
        /// </summary>
        uint Version { get; }

        /// <summary>
        /// Name of the title to use when querying ROS.
        /// </summary>
        string GameTitle { get; }

        /// <summary>
        /// Flag indicating whether we should use SSL when connecting to ROS.
        /// </summary>
        bool UseSSL { get; }

        /// <summary>
        /// Username to use to connect to the ROS services.
        /// </summary>
        string Username { get; }

        /// <summary>
        /// Password to use to connect to the ROS services.
        /// </summary>
        string Password { get; }

        /// <summary>
        /// Collection of authentication services (maps service name to relative urls).
        /// </summary>
        IDictionary<string, string> AuthServiceUrls { get; }

        /// <summary>
        /// Collection of game services (maps service name to relative urls).
        /// </summary>
        IDictionary<string, string> GameServiceUrls { get; }

        /// <summary>
        /// Collection of global services (maps service name to relative urls).
        /// </summary>
        IDictionary<String, String> GlobalServiceUrls { get; }

        /// <summary>
        /// List of platforms that ROS supports for the current title.
        /// </summary>
        ICollection<string> Platforms { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Reload all of the configuration data.
        /// </summary>
        void Reload();

        /// <summary>
        /// Allows the user to save out the config to file
        /// </summary>
        /// <param name="local"></param>
        void Save(bool local);

        /// <summary>
        /// 
        /// </summary>
        void ClearLoginDetails();
        #endregion // Methods
    } // IConfig
}
