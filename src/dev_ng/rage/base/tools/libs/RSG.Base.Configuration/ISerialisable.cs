﻿using System;
using System.Xml.Linq;

namespace RSG.Base.Configuration
{

    /// <summary>
    /// 
    /// </summary>
    public interface ISerialisable
    {
        /// <summary>
        /// Serialise data object to XML element.
        /// </summary>
        /// <returns></returns>
        XElement ToXml();
    }

} // RSG.Base.Configuration namespace
