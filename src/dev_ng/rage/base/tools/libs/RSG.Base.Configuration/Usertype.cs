﻿using System;
using System.Diagnostics;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Logging;

namespace RSG.Base.Configuration
{

    /// <summary>
    /// Usertype abstraction.
    /// </summary>
    internal class Usertype : IUsertype
    {
        #region Constants
        internal static readonly String ELEM_USERTYPE = "usertype";

        internal static readonly String ATTR_NAME = "name";
        internal static readonly String ATTR_FRIENDLY = "uiname";
        internal static readonly String ATTR_FLAGS = "flags";
        internal static readonly String ATTR_PSEUDO = "pseudo";

        internal static readonly uint AUTOMATION_USER_FLAG = 0x0010;
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Name.
        /// </summary>
        public String Name
        {
            get;
            private set;
        }
        
        /// <summary>
        /// Friendly name; for UI display.
        /// </summary>
        public String FriendlyName
        {
            get;
            private set;
        }

        /// <summary>
        /// Bit flags for this user.
        /// </summary>
        public UInt32 Flags
        {
            get;
            private set;
        }

        /// <summary>
        /// Pseudo user; unselectable in installer?
        /// </summary>
        public bool Pseudo
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor; from XElement.
        /// </summary>
        /// <param name="xmlElem"></param>
        public Usertype(XElement xmlElem)
        {
            ParseUsertypeAttributes(xmlElem);
        }

        /// <summary>
        /// Constructor; taking string arguments for properties.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="friendly"></param>
        /// <param name="flags"></param>
        /// <param name="pseudo"></param>
        public Usertype(String name, String friendly, UInt32 flags, bool pseudo)
        {
            this.Name = name;
            this.FriendlyName = friendly;
            this.Flags = flags;
            this.Pseudo = pseudo;
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Return whether this is an automation user.
        /// </summary>
        /// <returns></returns>
        public bool IsAutomationUser()
        {
#warning DHM FIX ME: shitty hack for now until config update
            return ((this.Flags & AUTOMATION_USER_FLAG) > 0);
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Parse usertype attributes from an XElement.
        /// </summary>
        /// <param name="xmlElem"></param>
        private void ParseUsertypeAttributes(XElement xmlElem)
        {
            foreach (XAttribute xmlAttr in xmlElem.Attributes())
            {
                if (0 == String.Compare(ATTR_NAME, xmlAttr.Name.LocalName))
                {
                    this.Name = xmlAttr.Value.Trim();
                }
                else if (0 == String.Compare(ATTR_FRIENDLY, xmlAttr.Name.LocalName))
                {
                    this.FriendlyName = xmlAttr.Value.Trim();
                }
                else if (0 == String.Compare(ATTR_FLAGS, xmlAttr.Name.LocalName))
                {
                    if (xmlAttr.Value.StartsWith("0x") || xmlAttr.Value.StartsWith("0X"))
                    {
                        this.Flags = UInt32.Parse(xmlAttr.Value.Substring(2),
                            System.Globalization.NumberStyles.HexNumber);
                    }
                    else
                    {
                        this.Flags = UInt32.Parse(xmlAttr.Value);
                    }
                }
                else if (0 == String.Compare(ATTR_PSEUDO, xmlAttr.Name.LocalName))
                {
                    this.Pseudo = bool.Parse(xmlAttr.Value);
                }
                else
                {
                    Config.Log.Warning(xmlElem, "Unknown attribute on usertype element: '{0}'.",
                        xmlAttr.Name.LocalName);
                }
            }
        }
        #endregion // Private Methods
    }

} // RSG.Base.Configuration
