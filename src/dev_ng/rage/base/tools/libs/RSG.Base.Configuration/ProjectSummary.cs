﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace RSG.Base.Configuration
{

    /// <summary>
    /// Project summary interface; for reading summary information about
    /// available projects when the configuration data may not be available.
    /// </summary>
    internal class ProjectSummary : 
        IProjectSummary
    {
        #region Properties
        /// <summary>
        /// Project name.
        /// </summary>
        /// <seealso cref="IProject.Name" />
        /// <seealso cref="IProject2.Name" />
        public String Name 
        {
            get { return (m_sName); }
            private set { m_sName = value; }
        }
        private String m_sName;

        /// <summary>
        /// Project friendly UI name.
        /// </summary>
        /// <seealso cref="IProject.FriendlyName" />
        /// <seealso cref="IProject2.FriendlyName" />
        public String FriendlyName
        {
            get { return (m_sFriendlyName); }
            private set { m_sFriendlyName = value; }
        }
        private String m_sFriendlyName;

        /// <summary>
        /// Project root-directory (absolute path).
        /// </summary>
        /// <seealso cref="IProject.Root" />
        /// <seealso cref="IProject2.Root" />
        public String Root
        {
            get { return (this.Environment.Subst(m_sRoot)); }
            private set { m_sRoot = value; }
        }
        private String m_sRoot;

        /// <summary>
        /// Project configuration filename.
        /// </summary>
        /// <seealso cref="IProject.Filename" />
        /// <seealso cref="IProject2.Filename" />
        public String Filename
        {
            get { return (this.Environment.Subst(m_sFilename)); }
            private set { m_sFilename = value; }
        }
        private String m_sFilename;

        /// <summary>
        /// Project summary environment (not complete).
        /// </summary>
        public IEnvironment Environment
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor; from XElement.
        /// </summary>
        /// <param name="xmlElem"></param>
        public ProjectSummary(XElement xmlElem)
        {
            Util.Xml.GetAttrValue(xmlElem, "name", ref this.m_sName);
            Util.Xml.GetAttrValue(xmlElem, "uiname", ref this.m_sFriendlyName);
            Util.Xml.GetAttrValue(xmlElem, "root", ref this.m_sRoot);
            Util.Xml.GetAttrValue(xmlElem, "config", ref this.m_sFilename);
            this.Environment = new Environment();
            this.Environment.Add("name", this.Name);
            this.Environment.Add("root", this.Root);
        }
        #endregion // Constructor(s)
    }

} // RSG.Base.Configuration namespace
