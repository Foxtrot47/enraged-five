﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Configuration
{

    /// <summary>
    /// Version control settings interface.
    /// </summary>
    public interface IVersionControlSettings
    {
        #region Properties
        /// <summary>
        /// Version control provider server connection.
        /// </summary>
        String Server { get; }

        /// <summary>
        /// Version control provider username.
        /// </summary>
        String Username { get; }

        /// <summary>
        /// Version control provider workspace/client (if applicable).
        /// </summary>
        String Workspace { get; }
        #endregion // Properties
    }

} // RSG.Base.Configuration namespace
