﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Logging;
using RSG.Platform;
using RSG.SourceControl.Perforce;

namespace RSG.Base.Configuration
{

    /// <summary>
    /// Project encapsulation; representing a project defining a set of
    /// branches and targets.
    /// </summary>
    internal class Project : 
        IProject,
        Installer.IWritableProject,
        IEnumerable<IBranch>
    {
        #region Constants
        private static readonly String CORE_PROJECT_PREFIX = "core_";

        internal static readonly String ELEM_PROJECT = "project";
        internal static readonly String ELEM_LOCAL = "local";

        internal static readonly String ELEM_BRANCHES = "branches";
        internal static readonly String ELEM_REPORTS = "reports";
        internal static readonly String ELEM_BUGSTAR = "bugstar";
        internal static readonly String ELEM_LABELS = "labels";
        internal static readonly String ELEM_LABEL = "label";
        internal static readonly String ELEM_CONTENT = "content";

        internal static readonly String ATTR_BRANCH_DEFAULT = "default";

        internal static readonly String ATTR_NAME = "name";
        internal static readonly String ATTR_UINAME = "uiname";
        internal static readonly String ATTR_CONFIGFILENAME = "config";
        internal static readonly String ATTR_ROOT = "root";
        internal static readonly String ATTR_CACHE = "cache";
        internal static readonly String ATTR_HASLEVELS = "has_levels";
        internal static readonly String ATTR_LABEL_TYPE = "type";
        internal static readonly String ATTR_LABEL_NAME = "name";
        internal static readonly String ATTR_DEFAULT_BRANCH = "default";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Project name.
        /// </summary>
        public String Name
        {
            get;
            private set;
        }

        /// <summary>
        /// Project friendly UI name.
        /// </summary>
        public String FriendlyName
        {
            get;
            private set;
        }

        /// <summary>
        /// Project root directory $(root).
        /// </summary>
        public String Root
        {
            get { return Path.GetFullPath(this.Environment.Subst(m_sRoot)); }
            private set { m_sRoot = value; }
        }
        private String m_sRoot;

        /// <summary>
        /// Project configuration filename.
        /// </summary>
        public String Filename
        {
            get { return Path.GetFullPath(this.Environment.Subst(m_sFilename)); }
        }
        private String m_sFilename;

        /// <summary>
        /// Project cache directory $(cache).
        /// </summary>
        public String Cache
        {
            get { return Path.GetFullPath(this.Environment.Subst(m_sCache)); }
            private set { m_sCache = value; }
        }
        private String m_sCache;

        /// <summary>
        /// Whether this project is episodic.
        /// </summary>
        public bool IsDLC
        {
            get;
            private set;
        }

        /// <summary>
        /// Whether this project has levels.
        /// </summary>
        public bool HasLevels
        {
            get;
            private set;
        }

        /// <summary>
        /// Force flags collection.
        /// </summary>
        public IForceFlags ForceFlags
        {
            get;
            private set;
        }

        /// <summary>
        /// Asset prefix string (default: empty for non-DLC, project-name for DLC).
        /// </summary>
        public IAssetPrefix AssetPrefix
        {
            get; 
            private set;
        }
        
        /// <summary>
        /// Local default branch name for this project.
        /// </summary>
        public String DefaultBranchName
        {
            get;
            set;
        }

        /// <summary>
        /// Local default branch for this project.
        /// </summary>
        public IBranch DefaultBranch
        {
            get;
            set;
        }

        /// <summary>
        /// Config object parent.
        /// </summary>
        public IConfig Config
        {
            get;
            private set;
        }
        
        /// <summary>
        /// Enumerable of Tools-associated mail addresses (for sending logs and bug reports).
        /// </summary>
        public IEnumerable<System.Net.Mail.MailAddress> ToolsEmailAddresses 
        { 
            get;
            private set;
        }

        /// <summary>
        /// Collection of branches for this project.
        /// </summary>
        public IDictionary<String, IBranch> Branches
        {
            get;
            private set;
        }

        /// <summary>
        /// Collection of default platforms, these can be used when the current default branch
        /// has no active targets in it.
        /// </summary>
        public IDictionary<RSG.Platform.Platform, bool> DefaultPlatforms
        {
            get;
            private set;
        }
        
        /// <summary>
        /// Dictionary of available labels for this project.
        /// </summary>
        public IDictionary<Label, String> Labels 
        { 
            get;
            private set;
        }

        /// <summary>
        /// Application settings; enabled flags etc.
        /// </summary>
        public IDictionary<String, IApplicationSettings> ApplicationSettings 
        { 
            get;
            private set;
        }

        /// <summary>
        /// Project environment; inheriting from Config.
        /// </summary>
        public IEnvironment Environment
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="config">Config object parent.</param>
        /// <param name="name">Project name key</param>
        /// <param name="uiname">Project UI Name</param>
        /// <param name="root">Project root directory</param>
        /// <param name="config_filename"></param>
        public Project(Config config, String name, String uiname, String root, String config_filename)
        {
            Initialise(config);

            this.Name = name;
            this.FriendlyName = uiname;
            this.Root = root;
            m_sFilename = config_filename;
            this.Reload();
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="config"></param>
        /// <param name="xmlElem"></param>
        /// <param name="local"></param>
        internal Project(Config config, XElement xmlElem)
        {
            Initialise(config);

            this.ParseConfigProjectAttributes(xmlElem);
            this.Reload();
        }

        /// <summary>
        /// DLC project constructor.
        /// </summary>
        /// <param name="config"></param>
        /// <param name="xmlElem"></param>
        /// <param name="rootproject"></param>
        internal Project(Config config, XElement xmlElem, IProject parent)
        {
            Initialise(config);
            
            this.IsDLC = true;
            this.ParseConfigProjectAttributes(xmlElem);
            this.Reload();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Import settings into an IEnvironment object.
        /// </summary>
        /// <param name="environment"></param>
        public void Import(IEnvironment environment)
        {
            this.Config.Import(environment);
            this.Import(environment, String.Empty);

            // Import Core-Project variables; if available.  Need to protect
            // against importing environment during initial load.
            if (this.Config.CoreProject is Project)
                ((Project)this.Config.CoreProject).Import(environment, CORE_PROJECT_PREFIX);
        }

        /// <summary>
        /// Reload all project configuration data.
        /// </summary>
        public void Reload()
        {
            this.Branches.Clear();
            this.DefaultPlatforms.Clear();
            this.Environment.Clear();
            this.Config.Import(this.Environment);

            if (!File.Exists(this.Filename))
            {
                if (!this.IsDLC)
                    throw new ConfigurationException("Core project XML file does not exist.")
                        {
                            Filename = this.Filename
                        };
                else
                    throw new DLCConfigurationException("DLC project XML does not exist.")
                        {
                            Filename = this.Filename
                        };
            }

            XDocument xmlDoc = XDocument.Load(this.Filename,
                LoadOptions.SetLineInfo | LoadOptions.SetBaseUri);
            ParseProject(xmlDoc, false);

            String localConfigFilename = Path.Combine(Path.GetDirectoryName(this.Filename), "local.xml");
            if (File.Exists(localConfigFilename))
            {
                XDocument xmlDocLocal = XDocument.Load(localConfigFilename,
                    LoadOptions.SetLineInfo | LoadOptions.SetBaseUri);
                ParseProject(xmlDocLocal, true);
            }

            // Initialise DLC branch targets.
            if (this.IsDLC)
            {
                foreach (IBranch branch in this.Branches.Values)
                    branch.SetupBranchDLCTargets();
            }

            // Clear and re-import a fresh environment.
            this.Environment.Clear();
            this.Import(this.Environment);
        }

        /// <summary>
        /// Save all of the configuration data.
        /// </summary>
        public bool SaveLocal()
        {
            String filename = Path.Combine(this.Config.ToolsConfig, "local.xml");

            try
            {
                String filenameBackup = Path.ChangeExtension(filename, ".xml.bak");
                if (File.Exists(filename) && !File.Exists(filenameBackup))
                    File.Copy(filename, filenameBackup);

                XDocument xmlDoc = new XDocument(
                    new XDeclaration("1.0", "UTF-8", "yes"),
                    new XElement("local",
						//TODO: DHM FIX ME: obsolete format
                        new XComment("Tools Migration: old XAttribute-format but still read by RSG.Base.Configuration."),
                        new XElement("branches",
                            new XAttribute("default", this.DefaultBranchName),
                            this.Branches.Select(branch => 
                                new XElement("branch", 
                                    new XAttribute("name", branch.Key),
                                    new XElement("targets",
                                        branch.Value.Targets.Select(kvp => 
                                            new XElement("target", 
                                                new XAttribute("platform", kvp.Key),
                                                new XAttribute("enabled", kvp.Value.Enabled)
                                            )
                                        )
                                    )
                                )
                            )
                        ),
						//TODO: DHM FIX ME: end obsolete format

                        // New XElement-format.
                        new XComment("Tools Migration: new XElement-format."),
                        new XElement("ConfigFileVersion", RSG.Base.Configuration.Config.CONFIG_VERSION),
                        new XElement("Branches",
                            new XElement("Default", this.DefaultBranchName),
                            this.Branches.Select(branch => 
                                new XElement("Branch", 
                                    new XElement("Name", branch.Key),
                                    new XElement("Targets",
                                        branch.Value.Targets.Select(kvp => 
                                            new XElement("Target", 
                                                new XElement("Platform", kvp.Key),
                                                new XElement("Enabled", kvp.Value.Enabled)
                                            )
                                        )
                                    )
                                )
                            )
                        ),
                        new XElement("DefaultPlatforms",
                            this.DefaultPlatforms.Select(kvp =>
                                new XElement("DefaultPlatform",
                                    new XElement("Platform", kvp.Key),
                                    new XElement("Enabled", kvp.Value)
                                )
                            )
                        )
                    )
                );
                xmlDoc.Save(filename);
                return (true);
            }
            catch (Exception ex)
            {
                throw new ConfigurationException("Failed to save local project config.", ex)
                    {
                        Filename = filename
                    };
            }
        }

        /// <summary>
        /// Return Perforce source control object for this project.
        /// </summary>
        /// <returns></returns>
        /// The returned object is owned by the caller; and needs disposing.
        public P4 SCMConnect()
        {
            if (this.Config.VersionControlEnabled)
            {
                String currentDirectory = Directory.GetCurrentDirectory();
                Directory.SetCurrentDirectory(this.Root);
                P4 p4 = new P4();
                p4.CWD = this.Root;
                p4.CallingProgram = String.Format("{0} [{1}]",
                    System.Environment.CommandLine, "P4.Net API");
                p4.Connect();

                Directory.SetCurrentDirectory(currentDirectory);
                return (p4);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Return Perforce source control object for this project's replica.
        /// </summary>
        /// <returns></returns>
        /// The returned object is owned by the caller; and needs disposing.
        /// 
        /// Scripts and applications that require readonly Perforce access
        /// that is not guaranteed to be available can use this to avoid
        /// loading on the main Perforce server.
        public P4 ReplicaSCMConnect()
        {
            if (this.Config.VersionControlEnabled)
            {
                String currentDirectory = Directory.GetCurrentDirectory();
                Directory.SetCurrentDirectory(this.Root);
                P4 p4 = new P4();
                p4.CWD = this.Root;
                p4.CallingProgram = String.Format("{0} [{1}]",
                    System.Environment.CommandLine, "P4.Net API");
                p4.Port = this.Config.Studios.ThisStudio.PerforceReplicaServer;
                p4.Connect();

                Directory.SetCurrentDirectory(currentDirectory);
                return (p4);
            }
            else
            {
                return null;
            }
        }

        public void PostEnvironmentInit()
        {
            this.Environment.Clear();
            this.Import(this.Environment);
            foreach (var branch in this.Branches.Values)
            {
                branch.PostEnvironmentInit();
            }
        }
        #endregion // Controller Methods

        #region Object Overrides
        /// <summary>
        /// Return simple string representation of this Project.
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return (String.Format("{0}: {1}", this.Name, this.FriendlyName));
        }
        #endregion // Object Overrides

        #region IEnumerable<Branch> Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerator<IBranch> GetEnumerator()
        {
            foreach (IBranch branch in this.Branches.Values)
                yield return branch;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return (GetEnumerator());
        }
        #endregion // IEnumerable<Branch> Methods

        #region Private Methods
        /// <summary>
        /// Initialise some private data.
        /// </summary>
        private void Initialise(IConfig config)
        {
            this.Config = config;
            this.Environment = new Environment();
            this.Branches = new Dictionary<String, IBranch>();
            this.DefaultPlatforms = new Dictionary<RSG.Platform.Platform, bool>();
            this.Labels = new Dictionary<Label, String>();
        }

        /// <summary>
        /// Parse top-level project configuration data elements.
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="local"></param>
        private void ParseProject(XDocument xmlDoc, bool local)
        {
            ParseProjectAttributes(xmlDoc, local);
            String root = local ? ELEM_LOCAL : ELEM_PROJECT;

            // Only parse asset prefix data from the checked in configuration.
            // Do not allow local overrides.
            if (!local)
            {
                XElement xmlForceFlagsElem = xmlDoc.XPathSelectElement(String.Format("/{0}/ForceFlags", root));
                if (null != xmlForceFlagsElem)
                    this.ForceFlags = new ForceFlags(this, xmlForceFlagsElem);
                else
                    this.ForceFlags = new ForceFlags(this);

                XElement xmlAssetPrefixElem = xmlDoc.XPathSelectElement(String.Format("/{0}/AssetPrefix", root));
                if (null != xmlAssetPrefixElem)
                    this.AssetPrefix = new AssetPrefix(this, xmlAssetPrefixElem);
                else
                    this.AssetPrefix = new AssetPrefix(this);

                List<System.Net.Mail.MailAddress> emailAddresses = new List<System.Net.Mail.MailAddress>();
                foreach (XElement xmlEmailAddress in
                    xmlDoc.XPathSelectElements(String.Format("{0}/ToolsEmailAddresses/EmailAddress", root)))
                {
                    String emailAddress = xmlEmailAddress.Element("Address").Value;
                    String displayName = xmlEmailAddress.Element("DisplayName").Value;
                    emailAddresses.Add(new System.Net.Mail.MailAddress(emailAddress, displayName));
                }
                this.ToolsEmailAddresses = emailAddresses.ToArray();
            }

            foreach (XElement xmlBranchElem in
                xmlDoc.XPathSelectElements(String.Format("{0}/branches/branch", root)))
            {
                ParseBranch(xmlBranchElem, local);
            }

            foreach (XElement xmlDefaultPlatformElem in
                xmlDoc.XPathSelectElements(String.Format("{0}/DefaultPlatforms/DefaultPlatform", root)))
            {
                // We currently have no default platforms inside the non-local files.
                if (local)
                {
                    ParseDefaultPlatform(xmlDefaultPlatformElem);
                }
            }

            foreach (XElement xmlLabelElem in
                xmlDoc.XPathSelectElements(String.Format("{0}/labels/label", root)))
            {
                ParseLabel(xmlLabelElem, local);
            }

            SetupDefaultBranch(xmlDoc, local);
        }

        /// <summary>
        /// Parse top-level project data attributes.
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="local"></param>
        private void ParseProjectAttributes(XDocument xmlDoc, bool local)
        {
            foreach (XAttribute xmlProjectAttr in xmlDoc.Root.Attributes())
            {
                if (String.Equals(ATTR_NAME, xmlProjectAttr.Name.LocalName))
                {
                    this.Name = xmlProjectAttr.Value.Trim();
                }
                else if (String.Equals(ATTR_CACHE, xmlProjectAttr.Name.LocalName))
                {
                    this.Cache = xmlProjectAttr.Value.Trim();
                }
                else if (String.Equals(ATTR_HASLEVELS, xmlProjectAttr.Name.LocalName))
                {
                    this.HasLevels = bool.Parse(xmlProjectAttr.Value);
                }
            }
        }

        /// <summary>
        /// Parse project attributes from global configuration XML.
        /// </summary>
        /// <param name="reader"></param>
        private void ParseConfigProjectAttributes(XElement xmlElem)
        {
            foreach (XAttribute xmlProjectAttr in xmlElem.Attributes())
            {
                if (String.Equals(ATTR_NAME, xmlProjectAttr.Name.LocalName))
                {
                    this.Name = xmlProjectAttr.Value.Trim();
                    this.Environment.Add("name", this.Name);
                }
                else if (String.Equals(ATTR_UINAME, xmlProjectAttr.Name.LocalName))
                {
                    this.FriendlyName = xmlProjectAttr.Value.Trim();
                }
                else if (String.Equals(ATTR_ROOT, xmlProjectAttr.Name.LocalName))
                {
                    this.Root = Path.GetFullPath(xmlProjectAttr.Value.Trim());
                    this.Environment.Add("root", this.Root);
                }
                else if (String.Equals(ATTR_CONFIGFILENAME, xmlProjectAttr.Name.LocalName))
                {
                    m_sFilename = this.Environment.Subst(xmlProjectAttr.Value.Trim());
                }
            }
        }

        /// <summary>
        /// Setup default branch properties.
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="local"></param>
        private void SetupDefaultBranch(XDocument xmlDoc, bool local)
        {
            if (local)
            {
                 XElement xmlBranchesElem = xmlDoc.XPathSelectElement(
                    String.Format("{0}/branches", ELEM_LOCAL));
                XAttribute xmlDefaultBranchAttr = xmlBranchesElem.Attributes().Where(
                    attr => (0 == String.Compare(ATTR_BRANCH_DEFAULT, attr.Name.LocalName))).FirstOrDefault();
                if (null != xmlDefaultBranchAttr)
                {
                    string defaultBranchName = xmlDefaultBranchAttr.Value.Trim();
                    if (this.Branches.ContainsKey(defaultBranchName))
                    {
                        this.DefaultBranchName = defaultBranchName;
                        this.DefaultBranch = this.Branches[this.DefaultBranchName];
                    }
                    else
                    {
                        this.DefaultBranchName = String.Empty;
                        this.DefaultBranch = null;
                        RSG.Base.Configuration.Config.Log.Warning(xmlDoc,
                            "Default branch not in list of branches.");
                    }
                }
                else
                {
                    this.DefaultBranchName = String.Empty;
                    this.DefaultBranch = null;
                    RSG.Base.Configuration.Config.Log.Warning(xmlDoc,
                        "Default branch not set if config data.");
                }
            }
            else if (this.DefaultBranch == null)
            {
                XElement xmlBranchesElem = xmlDoc.XPathSelectElement(
                    String.Format("{0}/branches", ELEM_PROJECT));
                XAttribute xmlDefaultBranchAttr = xmlBranchesElem.Attributes().Where(
                    attr => (0 == String.Compare(ATTR_BRANCH_DEFAULT, attr.Name.LocalName))).FirstOrDefault();
                if (null != xmlDefaultBranchAttr)
                {
                    this.DefaultBranchName = xmlDefaultBranchAttr.Value.Trim();
                    this.DefaultBranch = this.Branches[this.DefaultBranchName];
                }
                else
                {
                    this.DefaultBranchName = String.Empty;
                    this.DefaultBranch = null;
                    RSG.Base.Configuration.Config.Log.Warning(xmlDoc,
                        "Default branch not set if config data.");
                }
            }
        }

        /// <summary>
        /// Parse project branch element.
        /// </summary>
        /// <param name="xmlElem"></param>
        /// <param name="local"></param>
        private void ParseBranch(XElement xmlElem, bool local)
        {
            if (local)
            {
                XAttribute attrBranchName = xmlElem.Attributes().Where(
                    attr => 0 == String.Compare(ATTR_NAME, attr.Name.LocalName)).FirstOrDefault();
                if (null != attrBranchName)
                {
                    String branchName = attrBranchName.Value.Trim();
                    if (this.Branches.ContainsKey(branchName))
                    {
                        IBranch branch = this.Branches[branchName];
                        branch.ParseLocal(xmlElem);

                        foreach (ITarget target in branch.Targets.Values)
                        {
                            if (target.Enabled)
                            {
                                this.DefaultPlatforms[target.Platform] = true;
                            }
                        }
                    }
                    else
                    {
                        RSG.Base.Configuration.Config.Log.Warning(
                            "Local branch '{0}' does not exist in core configuration.", branchName);
                    }
                }
                else
                {
                    RSG.Base.Configuration.Config.Log.Error(
                        xmlElem, "Local branch does not have a name assigned.");
                }  
            }
            else
            {
                IBranch branch = new Branch(this, xmlElem);
                this.Branches.Add(branch.Name, branch);
                // Make sure all valid platforms are added to the default platform collection.
                foreach (ITarget target in branch.Targets.Values)
                {
                    if (!this.DefaultPlatforms.ContainsKey(target.Platform))
                    {
                        this.DefaultPlatforms.Add(target.Platform, false);
                    }
                }
            }
        }

        /// <summary>
        /// Parse project default platform element.
        /// </summary>
        /// <param name="xmlElem"></param>
        /// <param name="local"></param>
        private void ParseDefaultPlatform(XElement xmlElem)
        {
            // We don't define new Platforms from the local configuration
            // data; only picking up the Enabled flag.
            XElement xmlPlatformElement = xmlElem.Element("Platform");
            String platform = (xmlPlatformElement != null) ? xmlPlatformElement.Value : String.Empty;
            if (String.IsNullOrEmpty(platform))
            {
                RSG.Base.Configuration.Config.Log.Warning(xmlElem,
                    "Default target has no platform attribute.");
            }
            else
            {
                RSG.Platform.Platform p = PlatformUtils.PlatformFromString(platform);
                // Find target; for platform.
                Debug.Assert(this.DefaultPlatforms.ContainsKey(p),
                    String.Format("Project '{0}' does not define Platform '{1}'.", this.Name, platform));
                if (!this.DefaultPlatforms.ContainsKey(p))
                {
                    RSG.Base.Configuration.Config.Log.Warning("Project '{0}' does not define Platform {1}.",
                        this.Name, platform);
                }
                else
                {
                    XElement xmlEnabledElement = xmlElem.Element("Enabled");
                    bool enabled = xmlEnabledElement != null && bool.Parse(xmlEnabledElement.Value);
                    this.DefaultPlatforms[p] = enabled;
                }
            }
        }

        /// <summary>
        /// Parse report category element.
        /// </summary>
        /// <param name="xmlElem"></param>
        /// <param name="local"></param>
        private void ParseReportCategory(XElement xmlElem, bool local)
        {
            Debug.Assert(!local, "Reports or report categories are not supported in local XML data.");
            if (local)
            {
                RSG.Base.Configuration.Config.Log.Warning(xmlElem,
                    "Reports or report categories are not supported in local XML data.");
                return;
            }
        }

        /// <summary>
        /// Parse project label element.
        /// </summary>
        /// <param name="xmlElem"></param>
        /// <param name="local"></param>
        private void ParseLabel(XElement xmlElem, bool local)
        {
            Debug.Assert(!local, "Label definitions are not supported in local XML data.");
            if (local)
            {
                RSG.Base.Configuration.Config.Log.Warning(xmlElem,
                    "Label definitions are not supported in local XML data.");
                return;
            }

            String labelName = String.Empty;
            String labelType = String.Empty;
            foreach (XAttribute xmlLabelAttr in xmlElem.Attributes())
            {
                if (0 == String.Compare(ATTR_LABEL_NAME, xmlLabelAttr.Name.LocalName))
                {
                    labelName = xmlLabelAttr.Value.Trim();
                }
                else if (0 == String.Compare(ATTR_LABEL_TYPE, xmlLabelAttr.Name.LocalName))
                {
                    labelType = xmlLabelAttr.Value.Trim();
                }
                else
                {
                    RSG.Base.Configuration.Config.Log.Warning(xmlElem,
                        "Unknown label attribute: '{0}'.",
                        xmlLabelAttr.Name.LocalName);
                }
            }

            Label label = LabelUtil.LabelFromConfigurationString(labelType);
            if (!this.Labels.ContainsKey(label) && (Label.None != label))
                this.Labels.Add(label, labelName);
            else if (this.Labels.ContainsKey(label))
                RSG.Base.Configuration.Config.Log.Warning(xmlElem,
                    "Duplicate label key found: '{0}'.  Ignoring.", labelName);
            else
                RSG.Base.Configuration.Config.Log.Warning(xmlElem,
                    "Unknown label key: '{0}'.  Ignoring.", labelName);
        }

        /// <summary>
        /// Import settings implementation; taking prefix to handle "core" settings
        /// and active variants.
        /// </summary>
        /// <param name="environment"></param>
        /// <param name="prefix"></param>
        private void Import(IEnvironment environment, String prefix)
        {
            environment.Add(String.Format("{0}name", prefix), this.Name);
            environment.Add(String.Format("{0}project", prefix), this.Name);
            environment.Add(String.Format("{0}uiname", prefix), this.FriendlyName);

            String rootDir = String.Empty;
            if (this.m_sRoot == null)
            {
                RSG.Base.Configuration.Config.Log.Error(
                    "Project '{0}' is missing a root directory.", this.FriendlyName);
            }
            else
            {
                rootDir = this.m_sRoot.Replace("$(", String.Format("$({0}", prefix));
                environment.Add(String.Format("{0}root", prefix), rootDir);

            }

            String cacheDir = String.Empty;
            if (this.m_sCache == null)
            {
                RSG.Base.Configuration.Config.Log.Error(
                    "Project '{0}' is missing a cache directory.", this.FriendlyName);
            }
            else
            {
                cacheDir = this.m_sCache.Replace("$(", String.Format("$({0}", prefix));
                environment.Add(String.Format("{0}cache", prefix), cacheDir);
            }
        }
        #endregion // Private Methods
    }

} // RSG.Base.Configuration namespace
