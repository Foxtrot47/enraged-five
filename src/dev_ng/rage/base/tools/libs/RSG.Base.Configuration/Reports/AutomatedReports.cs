﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Collections.ObjectModel;
using RSG.Base.Configuration;
using System.IO;

namespace RSG.Base.Configuration.Reports
{
    /// <summary>
    /// 
    /// </summary>
    internal class AutomatedReports : ConfigBase, IAutomatedReports
    {
        #region Constants
        internal const String ELEM_CUTSCENES      = "cutscenes";
        internal const String ELEM_MISSIONS       = "missions";
        internal const String ELEM_PER_CHANGELIST = "per_changelist";
        internal const String ELEM_PER_BUILD      = "per_build";

        internal const String ATTR_BASE_DIR      = "automated_dir";
        internal const String ATTR_STATS_FILE    = "stats_file";
        internal const String ATTR_MODS_FILE     = "modifications_file";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        private IReportsConfig ReportsConfig { get; set; }

        /// <summary>
        /// Directory that all the automated stats get saved to by cruise.
        /// </summary>
        public string BaseDirectory
        {
            get;
            private set;
        }

        /// <summary>
        /// Mission report related config info.
        /// </summary>
        public IAutomatedMissionReports MissionReports
        {
            get;
            private set;
        }

        /// <summary>
        /// Cut scene metrics config info.
        /// </summary>
        public IAutomatedCutsceneReports CutsceneReports
        {
            get;
            private set;
        }

        /// <summary>
        /// Path to the depot file that contains all the stats for per changelist tests
        /// </summary>
        public string PerChangelistStatsFile
        {
            get;
            private set;
        }

        /// <summary>
        /// Depot path to the file containing the modification history for per changelist tests
        /// </summary>
        public string PerChangelistModificationsFile
        {
            get;
            private set;
        }

        /// <summary>
        /// Path to the depot file that contains all the stats for per build tests
        /// </summary>
        public string PerBuildStatsFile
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public IEnvironment Environment
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public AutomatedReports(IReportsConfig reportsConfig)
            : base()
        {
            ReportsConfig = reportsConfig;
            Environment = new RSG.Base.Configuration.Environment();
        }

        /// <summary>
        /// 
        /// </summary>
        public AutomatedReports(IReportsConfig reportsConfig, XElement xmlElem)
            : this(reportsConfig)
        {
            ParseConfig(xmlElem);
        }
        #endregion // Constructor(s)

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlElem"></param>
        private void ParseConfig(XElement xmlElem)
        {
            // Read in the attributes first
            string tempStr = ReadAttribute<String>(xmlElem, ATTR_BASE_DIR);
            BaseDirectory = Path.GetFullPath(ReportsConfig.Config.Project.DefaultBranch.Environment.Subst(tempStr));
            Environment.Add(ATTR_BASE_DIR, BaseDirectory);

            // Create the mission reports object next
            XElement missionsElem = xmlElem.Element(ELEM_MISSIONS);
            if (missionsElem != null)
            {
                MissionReports = new AutomatedMissionReports(this, missionsElem);
            }
            else
            {
                MissionReports = new AutomatedMissionReports(this);
            }

            XElement cutscenesElem = xmlElem.Element(ELEM_CUTSCENES);
            if (cutscenesElem != null)
            {
                CutsceneReports = new AutomatedCutsceneReports(this, cutscenesElem);
            }
            else
            {
                CutsceneReports = new AutomatedCutsceneReports(this);
            }

            // Read in the automated/per build settings next (this needs a bit of cleanup)
            XElement changelistElem = xmlElem.Element(ELEM_PER_CHANGELIST);
            if (changelistElem != null)
            {
                ParseChangelistElement(changelistElem);
            }

            XElement buildElem = xmlElem.Element(ELEM_PER_BUILD);
            if (buildElem != null)
            {
                ParseBuildElement(buildElem);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="elem"></param>
        private void ParseChangelistElement(XElement elem)
        {
            string tempString = ReadAttribute<String>(elem, ATTR_STATS_FILE);
            PerChangelistStatsFile = Path.GetFullPath(Environment.Subst(tempString));

            tempString = ReadAttribute<String>(elem, ATTR_MODS_FILE);
            PerChangelistModificationsFile = Path.GetFullPath(Environment.Subst(tempString));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="elem"></param>
        private void ParseBuildElement(XElement elem)
        {
            string tempString = ReadAttribute<String>(elem, ATTR_STATS_FILE);
            PerBuildStatsFile = Path.GetFullPath(Environment.Subst(tempString));
        }
        #endregion // Private Methods
    } // AutomatedReport
}
