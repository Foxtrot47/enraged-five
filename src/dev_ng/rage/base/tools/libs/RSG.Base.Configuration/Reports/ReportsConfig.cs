﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;

namespace RSG.Base.Configuration.Reports
{
    /// <summary>
    /// Configuration object for all things related to reports.
    /// </summary>
    internal class ReportsConfig : IReportsConfig
    {
        #region Constants
        internal const String ELEM_REPORTS          = "reports";
        internal const String ELEM_CUSTOM           = "custom";
        internal const String ELEM_GROUP            = "group";
        internal const String ELEM_AUTOMATED        = "automated";
        internal const String ELEM_REPORT_GENERATOR = "report_generator";
        #endregion // Constants

        #region Fields
        /// <summary>
        /// Private field for the <see cref="Filename"/> property.
        /// </summary>
        private readonly String _filename;
        #endregion // Fields

        #region Properties
        /// <summary>
        /// XML configuration filename.
        /// </summary>
        public String Filename
        {
            get { return _filename; }
        }

        /// <summary>
        /// Config.
        /// </summary>
        public IConfig Config { get; private set; }

        /// <summary>
        /// Associated project object.
        /// </summary>
        public IProject Project { get; private set; }

        /// <summary>
        /// Custom report groups
        /// </summary>
        public IList<ICustomReportGroup> ReportGroups { get; private set; }

        /// <summary>
        /// Automated report config
        /// </summary>
        public IAutomatedReports AutomatedReports { get; private set; }

        /// <summary>
        /// Report generator reports config.
        /// </summary>
        public IReportGeneratorConfig ReportGeneratorConfig { get; private set; }
        #endregion // Properties

        #region Static Properties
        /// <summary>
        /// Configuration loading/saving log.
        /// </summary>
        internal static IUniversalLog Log { get; private set; }

        /// <summary>
        /// Configuration log file.
        /// </summary>
        private static IUniversalLogTarget LogFile { get; set; }
        #endregion // Static Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public ReportsConfig(IProject project)
        {
            // Read in all the settings
            this.Project = project;
            this.Config = this.Project.Config;
            _filename = Path.Combine(this.Config.ToolsRoot, "etc", "reports.xml");
            Reload();
        }
        
        /// <summary>
        /// Static constructor; initialises the configuration log.
        /// </summary>
        static ReportsConfig()
        {
            LogFactory.Initialize();
            Log = LogFactory.CreateUniversalLog("RSG.Base.Configuration.Reports");
            LogFile = LogFactory.CreateUniversalLogFile(Log);
        }
        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// Reload all of the configuration data.
        /// </summary>
        public void Reload()
        {
            // Get the file that we wish to load and ensure that it exists
            if (!File.Exists(Filename))
            {
                Debug.Fail(String.Format("Reports Xml '{0}' does not exist.", Filename));
                throw new ConfigurationException("Reports XML file does not exist.")
                    {
                        Filename = this.Filename
                    };
            }

            // Load and parse the reports config file
            XDocument doc = XDocument.Load(Filename);
            ParseConfig(doc);
        }
        #endregion // Public Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="doc"></param>
        private void ParseConfig(XDocument doc)
        {
            // Get the root element and ensure its correct
            XElement rootElement = doc.Root;
            if (rootElement.Name != ELEM_REPORTS)
            {
                Debug.Fail(String.Format("Invalid root element ({0}) detected while parsing the reports config file.", rootElement.Name));
                throw new ConfigurationException(String.Format("Invalid root element ({0}) detected while parsing the reports config file.", rootElement.Name))
                    {
                        Filename = this.Filename
                    };
            }

            // Process the root level report groups
            ReportGroups = new List<ICustomReportGroup>();
            foreach (XElement groupElem in rootElement.XPathSelectElements(String.Format("{0}/{1}", ELEM_CUSTOM, ELEM_GROUP)))
            {
                ReportGroups.Add(new CustomReportGroup(groupElem));
            }

            // Process the automated reports config next
            XElement automatedElement = rootElement.Element(ELEM_AUTOMATED);
            if (automatedElement != null)
            {
                AutomatedReports = new AutomatedReports(this, automatedElement);
            }
            else
            {
                AutomatedReports = new AutomatedReports(this);
            }

            XElement reportGenElement = rootElement.Element(ELEM_REPORT_GENERATOR);
            if (reportGenElement != null)
            {
                ReportGeneratorConfig = new ReportGeneratorConfig(this, reportGenElement, this.Config.Project.DefaultBranch.Environment);
            }
        }
        #endregion // Private Methods
    } // ReportConfig
}
