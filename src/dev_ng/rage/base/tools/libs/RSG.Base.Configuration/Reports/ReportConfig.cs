﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using RSG.Base.Configuration.Implementation;

namespace RSG.Base.Configuration.Reports
{
    /// <summary>
    /// 
    /// </summary>
    internal class ReportConfig : IReportConfig
    {
        #region Constants
        internal const String ATTR_NAME = "name";
        internal const String ATTR_CLASS = "class";
        internal const String ATTR_OUTPUT = "output";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Reference to the parent configuration object.
        /// </summary>
        private IReportGeneratorConfig ReportGeneratorConfig { get; set; }

        /// <summary>
        /// A name used to uniquely identify this report config.
        /// </summary>
        public String Name { get; private set; }

        /// <summary>
        /// The name of the report class this report config is for.
        /// </summary>
        public String ClassName { get; private set; }

        /// <summary>
        /// The name of the file that should be saved for this report (can be null).
        /// </summary>
        public String OutputFilename { get; private set; }

        /// <summary>
        /// List of parameters to set up for this report.
        /// </summary>
        public IParameterList Parameters { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public ReportConfig(IReportGeneratorConfig reportGeneratorConfig, XElement xmlElem, IEnvironment environment)
        {
            ReportGeneratorConfig = reportGeneratorConfig;
            ParseReportAttributes(xmlElem);
            Parameters = new ParameterList(xmlElem, environment);
        }
        #endregion // Constructor(s)

        #region Private Methods
        /// <summary>
        /// Parses attributes for this report.
        /// </summary>
        /// <param name="xmlElem"></param>
        private void ParseReportAttributes(XElement xmlElem)
        {
            // Get the attributes
            XAttribute nameAtt = xmlElem.Attribute(ATTR_NAME);
            XAttribute classAtt = xmlElem.Attribute(ATTR_CLASS);
            XAttribute outputAtt = xmlElem.Attribute(ATTR_OUTPUT);

            // Only the name and class are mandatory
            Debug.Assert(nameAtt != null, "Name attribute missing for report config element.");
            Debug.Assert(classAtt != null, "Class attribute missing for report config element.");
            if (nameAtt == null || classAtt == null)
            {
                throw new ConfigurationException("Encountered a report generator report that doesn't have name and/or class attributes set.")
                    {
                        Filename = ReportGeneratorConfig.Filename
                    };
            }

            Name = nameAtt.Value.Trim();
            ClassName = classAtt.Value.Trim();

            // Not all reports have an output (e.g. emailed reports).
            if (outputAtt != null)
            {
                OutputFilename = outputAtt.Value.Trim();
            }
        }
        #endregion // Private Methods
    } // ReportConfig
}
