﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Configuration.Reports
{
    /// <summary>
    /// 
    /// </summary>
    public interface IAutomatedReports
    {
        #region Properties
        /// <summary>
        /// Directory that all the automated stats get saved to by cruise.
        /// </summary>
        string BaseDirectory { get; }

        /// <summary>
        /// Mission report related config info.
        /// </summary>
        IAutomatedMissionReports MissionReports { get; }

        /// <summary>
        /// Cut scene related config info.
        /// </summary>
        IAutomatedCutsceneReports CutsceneReports { get; }

        /// <summary>
        /// Path to the depot file that contains all the stats for per changelist tests
        /// </summary>
        string PerChangelistStatsFile { get; }

        /// <summary>
        /// Depot path to the file containing the modification history for per changelist tests
        /// </summary>
        string PerChangelistModificationsFile { get; }

        /// <summary>
        /// Path to the depot file that contains all the stats for per build tests
        /// </summary>
        string PerBuildStatsFile { get; }

        /// <summary>
        /// Environment for this object.
        /// </summary>
        IEnvironment Environment { get; }
        #endregion // Properties
    } // IAutomatedReport
}
