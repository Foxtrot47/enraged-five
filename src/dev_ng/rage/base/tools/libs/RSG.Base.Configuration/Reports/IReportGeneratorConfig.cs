﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Configuration.Reports
{
    /// <summary>
    /// 
    /// </summary>
    public interface IReportGeneratorConfig
    {
        /// <summary>
        /// 
        /// </summary>
        String Filename { get; }

        /// <summary>
        /// 
        /// </summary>
        IDictionary<string, IReportConfig> Reports { get; }
    } // IReportGeneratorConfig
}
