﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RSG.Base.Configuration.Reports
{
    /// <summary>
    /// Config object for reports that can be run by the report generator.
    /// </summary>
    internal class ReportGeneratorConfig : IReportGeneratorConfig
    {
        #region Constants
        internal const String ELEM_REPORT = "report";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public String Filename
        {
            get { return ReportsConfig.Filename; }
        }

        /// <summary>
        /// Reference to the parent reports config.
        /// </summary>
        public IReportsConfig ReportsConfig { get; private set; }

        /// <summary>
        /// List of report configurations.
        /// </summary>
        public IDictionary<String, IReportConfig> Reports { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public ReportGeneratorConfig(IReportsConfig reportsConfig, XElement xmlElem, IEnvironment environment)
        {
            ReportsConfig = reportsConfig;
            Reports = new Dictionary<string, IReportConfig>();

            foreach (XElement reportElem in xmlElem.Elements(ELEM_REPORT))
            {
                IReportConfig config = new ReportConfig(this, reportElem, environment);
                Reports.Add(config.Name, config);
            }
        }
        #endregion // Constructor(s)
    } // ReportGeneratorConfig
}
