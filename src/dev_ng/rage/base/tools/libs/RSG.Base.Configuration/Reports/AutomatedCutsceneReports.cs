﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using System.IO;

namespace RSG.Base.Configuration.Reports
{
    /// <summary>
    /// 
    /// </summary>
    internal class AutomatedCutsceneReports : ConfigBase, IAutomatedCutsceneReports
    {
        #region Constants

        internal const String ELEM_SCENES = "cutscenes";
        internal const String ATTR_SCENES_DIR = "cutscenes_dir";
        internal const String ATTR_MISSIONS_REGEX = "stats_file_regex";

        #endregion

        #region Properties
        /// <summary>
        /// Reference to the owning automated reports config object.
        /// </summary>
        private IAutomatedReports AutomatedReports
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public string Directory
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public string StatsFileRegexString
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public IEnvironment Environment
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public AutomatedCutsceneReports(IAutomatedReports automatedReports)
        {
            AutomatedReports = automatedReports;
            Environment = new RSG.Base.Configuration.Environment();
        }

        /// <summary>
        /// 
        /// </summary>
        public AutomatedCutsceneReports(IAutomatedReports automatedReports, XElement xmlElem)
            : this(automatedReports)
        {
            ParseConfig(xmlElem);
        }
        #endregion // Constructor(s)

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlElem"></param>
        private void ParseConfig(XElement xmlElem)
        {
            // Read in the attributes first
            Directory = Path.GetFullPath(AutomatedReports.Environment.Subst(ReadAttribute<String>(xmlElem, ATTR_SCENES_DIR)));
            Environment.Add(ATTR_SCENES_DIR, Directory);

            StatsFileRegexString = ReadAttribute<String>(xmlElem, ATTR_MISSIONS_REGEX);
        }
        #endregion // Private Methods
    }
}
