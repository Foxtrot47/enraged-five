﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using RSG.Base.Logging;

namespace RSG.Base.Configuration.Reports
{
    /// <summary>
    /// 
    /// </summary>
    internal class CustomReport : ICustomReport
    {
        #region Constants
        internal const string ATTR_NAME   = "name";
        internal const string ATTR_EXE    = "exe";
        internal const string ATTR_ARGS   = "args";
        internal const string ATTR_OUTPUT = "output";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Name of the custom report group as displayed in the workbench.
        /// </summary>
        public string Name
        {
            get;
            private set;
        }

        /// <summary>
        /// Executable to run when the report is invoked.
        /// </summary>
        public string Executable
        {
            get;
            private set;
        }

        /// <summary>
        /// List of arguments to provide to the executable.
        /// </summary>
        public string Arguments
        {
            get;
            private set;
        }

        /// <summary>
        /// Output file of the executable.
        /// </summary>
        public string Output
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlElem"></param>
        public CustomReport(XElement xmlElem)
        {
            ParseConfigReportGroupAttributes(xmlElem);
        }
        #endregion // Constructor(s)

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlElem"></param>
        private void ParseConfigReportGroupAttributes(XElement xmlElem)
        {
            foreach (XAttribute xmlGroupAttr in xmlElem.Attributes())
            {
                if (0 == String.Compare(ATTR_NAME, xmlGroupAttr.Name.LocalName))
                {
                    Name = xmlGroupAttr.Value.Trim();
                }
                else if (0 == String.Compare(ATTR_EXE, xmlGroupAttr.Name.LocalName))
                {
                    Executable = xmlGroupAttr.Value.Trim();
                }
                else if (0 == String.Compare(ATTR_ARGS, xmlGroupAttr.Name.LocalName))
                {
                    Arguments = xmlGroupAttr.Value.Trim();
                }
                else if (0 == String.Compare(ATTR_OUTPUT, xmlGroupAttr.Name.LocalName))
                {
                    Output = xmlGroupAttr.Value.Trim();
                }
                else
                {
                    ReportsConfig.Log.Warning("Unknown report attribute: '{0}'.", xmlGroupAttr.Name.LocalName);
                }
            }
        }
        #endregion // Private Methods
    } // CustomReport
}
