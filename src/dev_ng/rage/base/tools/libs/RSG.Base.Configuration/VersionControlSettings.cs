﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using RSG.Base.Logging;

namespace RSG.Base.Configuration
{

    /// <summary>
    /// Version control settings implementation.
    /// </summary>
    public class VersionControlSettings : 
        IVersionControlSettings,
        ISerialisable
    {
        #region Constants
        private static readonly String ATTR_SERVER = "server";
        private static readonly String ATTR_USERNAME = "username";
        private static readonly String ATTR_WORKSPACE = "workspace";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Version control provider server connection.
        /// </summary>
        public String Server 
        { 
            get;
            private set;
        }

        /// <summary>
        /// Version control provider username.
        /// </summary>
        public String Username 
        { 
            get;
            private set;
        }

        /// <summary>
        /// Version control provider workspace/client (if applicable).
        /// </summary>
        public String Workspace 
        { 
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor; from XElement.
        /// </summary>
        /// <param name="xmlElem"></param>
        public VersionControlSettings(XElement xmlElem)
        {
            ParseSCMAttributes(xmlElem);
        }

        /// <summary>
        /// Constructor; taking string arguments for properties.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="friendly"></param>
        /// <param name="netmask"></param>
        /// <param name="perforce"></param>
        public VersionControlSettings(String server, String username, String workspace)
        {
            this.Server = server;
            this.Username = username;
            this.Workspace = workspace;
        }
        #endregion // Constructor(s)

        #region ISerialisable Interface Methods
        /// <summary>
        /// Serialise data to an XML element.
        /// </summary>
        /// <returns></returns>
        public XElement ToXml()
        {
            throw new NotImplementedException();
        }
        #endregion // ISerialisable Interface Methods

        #region Private Methods
        /// <summary>
        /// Parse XML.
        /// </summary>
        /// <param name="xmlElem"></param>
        private void ParseSCMAttributes(XElement xmlElem)
        {
            foreach (XAttribute xmlAttr in xmlElem.Attributes())
            {
                if (0 == String.Compare(ATTR_SERVER, xmlAttr.Name.LocalName))
                {
                    this.Server = xmlAttr.Value.Trim();
                }
                else if (0 == String.Compare(ATTR_USERNAME, xmlAttr.Name.LocalName))
                {
                    this.Username = xmlAttr.Value.Trim();
                }
                else if (0 == String.Compare(ATTR_WORKSPACE, xmlAttr.Name.LocalName))
                {
                    this.Workspace = xmlAttr.Value.Trim();
                }
                else
                {
                    Config.Log.Warning(xmlElem, "Unknown attribute on scm element: '{0}'.",
                        xmlAttr.Name.LocalName);
                }
            }
        }
        #endregion // Private Methods
    }

} // RSG.Base.Configuration namespace
