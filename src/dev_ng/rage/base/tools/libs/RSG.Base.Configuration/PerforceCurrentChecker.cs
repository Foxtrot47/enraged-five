﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using P4API;
using RSG.SourceControl.Perforce;

namespace RSG.Base.Configuration
{
    /// <summary>
    /// File event handler.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void PerforceFileEventHandler(object sender, PerforceFileEventArgs e);

    /// <summary>
    /// File event arguments.
    /// </summary>
    public class PerforceFileEventArgs : EventArgs
    {
        public FileState FileState { get; private set; }

        internal PerforceFileEventArgs(FileState state)
        {
            FileState = state;
        }
    }

    /// <summary>
    /// Perforce current checker.
    /// </summary>
    public class PerforceCurrentChecker
    {
        #region Private member fields

        private List<FileState> m_badFiles;

        #endregion

        #region Public properties

        /// <summary>
        /// True if the folder contains out of date files. You can iterate through the out of date files using the GetBadFiles() method.
        /// </summary>
        public bool OutOfDate
        {
            get { return m_badFiles.Count > 0; }
        }

        #endregion

        #region Public events

        /// <summary>
        /// Files not current event handler.
        /// </summary>
        public event EventHandler FilesNotCurrent;

        /// <summary>
        /// Bad file event handler.
        /// </summary>
        public event PerforceFileEventHandler BadFile;

        #endregion

        #region Public methods

        /// <summary>
        /// Enumerate through the collection of bad files.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<FileState> GetBadFiles()
        {
            foreach (var file in m_badFiles)
            {
                yield return file;
            }
        }

        /// <summary>
        /// Check the current level. The number of days specifies how many days are allowed before a file becomes 'state' and is considered out of date.
        /// </summary>
        /// <param name="versions">Number of versions the user can be 'out'.</param>
		/// <returns>True if there are no stale files.</returns>
        public bool Check(int versions)
        {
            IConfig config = ConfigFactory.CreateConfig();
            using (P4 p4 = config.Project.SCMConnect())
            {
				if (!config.Project.Labels.ContainsKey(Label.ToolsIncremental) || String.IsNullOrWhiteSpace(config.Project.Labels[Label.ToolsIncremental]) == true)
	            {
            	    //If the tools incremental label is not declared, then nullify this check. 
        	        //Projects may not have this process set up just yet.
    	            return true;
	            }

                RSG.SourceControl.Perforce.Label latestLabel = RSG.SourceControl.Perforce.Label.GetCurrentToolsVersionLabel(p4,
                                                        config.Project.Labels[Label.ToolsIncremental], config.Project.Labels[Label.ToolsCurrent]);
                if (latestLabel == null)
                {
                    return false;
                }

                String name = latestLabel.Name;
                uint major, minor;
                if (RSG.SourceControl.Perforce.Label.GetToolsLabelVersion(name, config.Project.Labels[Label.ToolsIncremental], out major, out minor))
                {
                    return major > config.Version.LocalVersion.Major - versions;
                }
                else
                {
                    return false;
                }
            }
        }
        
        #endregion

        #region Private helper methods

        /// <summary>
        /// Checks the file to determine if it's out of date.
        /// </summary>
        /// <param name="state">File state.</param>
        /// <param name="p4">P4 connection.</param>
        /// <param name="seconds">Number of seconds.</param>
        /// <returns>True if the file is out of date.</returns>
        private bool IsOutOfDate(FileState state, P4 p4, double seconds)
        {
            FileState fileState = null;
            
            try
            {
                fileState = new FileState(p4, state.DepotFilename + "#" + state.HaveRevision);
            }
            catch
            {

            }

            if (fileState == null)
            {
                return true;
            }
            
            TimeSpan span = state.HeadChangelistTime - fileState.HeadChangelistTime;
            return span.TotalSeconds > seconds;
        }

        #endregion
    }
}
