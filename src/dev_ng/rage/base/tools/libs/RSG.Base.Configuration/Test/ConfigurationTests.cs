﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using NUnit.Framework;
using RSG.Base.Logging;
using RSG.Base.Configuration;

namespace RSG.Base.Configuration.Test
{

    /// <summary>
    /// Configuration data unit tests.
    /// </summary>
    [TestFixture]
    public class ConfigurationTests
    {
        #region Member Data
        /// <summary>
        /// Configuration  object for unit testing.
        /// </summary>
        private IConfig Config { get; set; }
        #endregion // Member Data

        #region NUnit Testcase Setup
        /// <summary>
        /// Unit test initialisation.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            ConsoleLog log = new ConsoleLog();
            Log.Log__Profile("Initialising Tools Configuration Data...");
            this.Config = ConfigFactory.CreateConfig();
            Log.Log__ProfileEnd();
        }
        #endregion // NUnit Testcase Setup

        /// <summary>
        /// Unit tests for the Config object.
        /// </summary>
        [Test]
        public void TestConfig()
        {
            Assert.That(this.Config.Project != null, "Project {0} is invalid.",
                this.Config.Project.Name);
            Assert.That(this.Config.ToolsRoot.Length > 0);
            Assert.AreEqual(System.Environment.GetEnvironmentVariable("RS_TOOLSROOT"),
                this.Config.ToolsRoot);
            Assert.That(Directory.Exists(this.Config.ToolsRoot));
            Assert.That(Directory.Exists(this.Config.ToolsBin));
            Assert.That(Directory.Exists(this.Config.ToolsConfig));
            Assert.That(Directory.Exists(this.Config.ToolsLib));
            Assert.That(Directory.Exists(this.Config.ToolsTemp));
        }

        /// <summary>
        /// Unit tests for the Project object.
        /// </summary>
        [Test]
        public void TestProject()
        {
            TestConfig();
            Assert.That(this.Config.Project.Name.Length > 0, "Project has no name.");
            Assert.AreEqual(this.Config.Project.Name, System.Environment.GetEnvironmentVariable("RS_PROJECT"),
                "Consistency mismatch; RS_PROJECT does not match project name.");
        }

        /// <summary>
        /// Unit tests for the Branch object.
        /// </summary>
        [Test]
        public void TestBranch()
        {
            TestProject();
            Assert.That(this.Config.Project.Branches != null,
                "Project {0} has no branches collection.", this.Config.Project.Name);
            Assert.That(this.Config.Project.Branches.Count > 0,
                "Project {0} has no branches defined.", this.Config.Project.Name);
            Assert.That(this.Config.Project.DefaultBranch ==
                this.Config.Project.Branches[this.Config.Project.DefaultBranchName]);
            foreach (KeyValuePair<String, IBranch> kvp in this.Config.Project.Branches)
            {

            }
        }

        /// <summary>
        /// Unit tests for the Target object.
        /// </summary>
        [Test]
        public void TestTarget()
        {
            TestBranch();
            foreach (KeyValuePair<String, IBranch> bkvp in this.Config.Project.Branches)
            {
                IBranch branch = bkvp.Value;
                Assert.That(branch.Targets.Count > 0, "Branch {0} has no targets.", branch.Name);
                foreach (KeyValuePair<RSG.Platform.Platform, ITarget> tkvp in branch.Targets)
                {

                }
            }
        }
    }

} // RSG.Pipeline.Core.Test namespace
