﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Configuration
{
    /// <summary>
    /// List of parameters
    /// </summary>
    public interface IParameterList : IDictionary<string, object>
    {
    } // IParameterList
}
