﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Xml;

namespace RSG.Base.Configuration
{

    /// <summary>
    /// Application settings class.
    /// </summary>
    internal class ApplicationSettings : IApplicationSettings
    {
        #region Constants
        /// <summary>
        /// Application name attribute.
        /// </summary>
        private static readonly String ATTR_NAME = "name";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Application name.
        /// </summary>
        public String Name 
        { 
            get;
            private set;
        }

        /// <summary>
        /// Version setup for project (used by installer).
        /// </summary>
        public IDictionary<String, bool> Versions 
        { 
            get;
            private set;
        }

        /// <summary>
        /// Application configuration data container.
        /// </summary>
        public IDictionary<String, Object> Parameters 
        {
            get { return m_Parameters; }
            private set { m_Parameters = value; }
        }
        private IDictionary<String, Object> m_Parameters;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="param"></param>
        public ApplicationSettings(String name, IDictionary<String, Object> param)
        {
            this.Name = name;
            this.Versions = new Dictionary<String, bool>();
            this.Parameters = new Dictionary<String, Object>(param);
        }

        /// <summary>
        /// Constructor; from XML element in configuration data.
        /// </summary>
        /// <param name="xmlElem"></param>
        public ApplicationSettings(XElement xmlElem)
        {
            Debug.Assert(xmlElem.Name.LocalName.Equals("Application"),
                "Unsupported XML element for application settings.");
            if (!xmlElem.Name.LocalName.Equals("Application"))
                throw new ArgumentException("Unsupported XML element for application settings.");
            
            this.Versions = new Dictionary<String, bool>();
            this.Parameters = new Dictionary<String, Object>();

            XElement xmlNameElem = xmlElem.Element("Name");
            if (null != xmlNameElem)
                this.Name = xmlNameElem.Value;

            IEnumerable<XElement> xmlVersionsElem = xmlElem.XPathSelectElements("Version");
            foreach (XElement xmlVersionElem in xmlVersionsElem)
            {
                XElement xmlIdElem = xmlVersionElem.Element("Id");
                XElement xmlInstallElem = xmlVersionElem.Element("Install");
                if (null != xmlIdElem && null != xmlInstallElem)
                {
                    bool install = false;
                    if (bool.TryParse(xmlInstallElem.Value, out install))
                    {
                        this.Versions.Add(xmlIdElem.Value, install);
                    }
                }
            }

            XElement xmlParametersElem = xmlElem.Element("Parameters");
            if (null != xmlParametersElem)
                RSG.Base.Xml.Parameters.Load(xmlParametersElem, ref this.m_Parameters);
        }
        #endregion // Constructor(s)
    }

} // RSG.Base.Configuration namespace
