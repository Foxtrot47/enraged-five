﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Configuration
{

    /// <summary>
    /// Runline mode.
    /// </summary>
    public enum RunlineMode
    {
        Install,
        Uninstall,
    }

    /// <summary>
    /// Installer configuration runline interface.
    /// </summary>
    public interface IRunline
    {
        #region Properties
        /// <summary>
        /// Runline installer mode.
        /// </summary>
        RunlineMode Mode { get; }

        /// <summary>
        /// Command line to execute.
        /// </summary>
        String Command { get; }

        /// <summary>
        /// Whether to continue if the runline fails.
        /// </summary>
        bool CanFail { get; }

        /// <summary>
        /// Single run.
        /// </summary>
        bool RunOnce { get; }

        /// <summary>
        /// Always run in the installer (irrespective of version).
        /// </summary>
        bool Always { get; }

        /// <summary>
        /// Users mask for this runline.
        /// </summary>
        uint Users { get; }

        /// <summary>
        /// Version of config this runline was introduced.
        /// </summary>
        int Version { get; }
        #endregion // Properties
    }

} // RSG.Base.Configuration namespace
