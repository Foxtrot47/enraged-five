﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Configuration.Export
{

    /// <summary>
    /// Animation Export configuration data.
    /// </summary>
    public interface IAnimationExportConfig : IExporterConfig
    {
    }

} // RSG.Base.Configuration.Export namespace
