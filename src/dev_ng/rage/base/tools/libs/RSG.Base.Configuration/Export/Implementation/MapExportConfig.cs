﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Configuration.Export;

namespace RSG.Base.Configuration.Export.Implementation
{

    /// <summary>
    /// Map export configuration object.
    /// </summary>
    internal class MapExportConfig :
        BaseExporterConfig,
        IMapExportConfig
    {
        #region Properties
        /// <summary>
        /// Auto-Perforce submit.
        /// </summary>
        public bool AutoSubmit
        {
            get { return (bool)this.GetParameter("Auto Submit", false); }
        }

        /// <summary>
        /// Map Export Report Drawable Limit (count).
        /// </summary>
        public int ReportDrawableLimit
        {
            get { return (int)this.GetParameter("Report Drawable Limit", 40); }
        }

        /// <summary>
        /// Map Export Report TXD Limit (count).
        /// </summary>
        public int ReportTXDLimit
        {
            get { return (int)this.GetParameter("Report TXD Limit", 10); }
        }

        /// <summary>
        /// Map Export Report Bound Drawable Distance Ratio.
        /// </summary>
        public int ReportBoundDrawableDistanceRatio
        {
            get { return (int)this.GetParameter("Report Bound Drawable Distance Ratio", 10); }
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="xmlMapElem"></param>
        private MapExportConfig(XElement xmlMapElem)
            : base(xmlMapElem)
        {
        }
        #endregion // Constructor(s)

        #region Static Controller Methods
        /// <summary>
        /// Load configuration data from XML document.
        /// </summary>
        /// <param name="xmlDoc"></param>
        public static IMapExportConfig Load(XDocument xmlDoc)
        {
            XElement xmlMapElem = xmlDoc.XPathSelectElement("/export/Map/Parameters");
            IMapExportConfig config = new MapExportConfig(xmlMapElem);
            return (config);
        }
        #endregion // Static Controller Methods
    }

} // RSG.Base.Configuration.Export.Implementation namespace
