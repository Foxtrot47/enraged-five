﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;

namespace RSG.Base.Configuration.Export.Implementation
{

    /// <summary>
    /// 
    /// </summary>
    internal class WeaponExportConfig :
        BaseExporterConfig,
        IWeaponExportConfig
    {
        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="xmlParamsElem"></param>
        private WeaponExportConfig(XElement xmlParamsElem)
            : base(xmlParamsElem)
        {
        }
        #endregion // Constructor(s)

        #region Static Controller Methods
        /// <summary>
        /// Load configuration data from XML document.
        /// </summary>
        /// <param name="xmlDoc"></param>
        public static IWeaponExportConfig Load(XDocument xmlDoc)
        {
            XElement xmlParamsElem = xmlDoc.XPathSelectElement("/export/Weapon/Parameters");
            IWeaponExportConfig config = new WeaponExportConfig(xmlParamsElem);
            return (config);
        }
        #endregion // Static Controller Methods
    }

} // RSG.Base.Configuration.Export.Implementation namespace
