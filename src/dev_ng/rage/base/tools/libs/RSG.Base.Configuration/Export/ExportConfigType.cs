﻿using System;
using System.Runtime.Serialization;

namespace RSG.Base.Configuration.Export
{

    /// <summary>
    /// Enumeration of the different exporter configuration datasets we have.
    /// </summary>
    [DataContract]
    [Flags]
    public enum ExportConfigType : uint
    {
        [EnumMember]
        Animation,

        [EnumMember]
        Character,

        [EnumMember]
        Cutscene,

        [EnumMember]
        Map,

        [EnumMember]
        Vehicle,

        [EnumMember]
        Weapon,

        All = 0xFFFFFFFF,
        Default = All,
    }

} // RSG.Base.Configuration.Export namespace
