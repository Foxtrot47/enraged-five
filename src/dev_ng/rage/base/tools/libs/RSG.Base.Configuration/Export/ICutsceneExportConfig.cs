﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Configuration.Export
{
    
    /// <summary>
    /// Cutscene Export configuration data.
    /// </summary>
    public interface ICutsceneExportConfig : IExporterConfig
    {
    }

} // RSG.Base.Configuration.Export namespace
