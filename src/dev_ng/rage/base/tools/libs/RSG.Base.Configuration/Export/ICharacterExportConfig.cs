﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Configuration.Export
{

    /// <summary>
    /// Character Export configuration data.
    /// </summary>
    public interface ICharacterExportConfig : IExporterConfig
    {
    }

} // RSG.Base.Configuration.Export namespace
