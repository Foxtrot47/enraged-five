﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;

namespace RSG.Base.Configuration.Export.Implementation
{

    /// <summary>
    /// 
    /// </summary>
    internal class CutsceneExportConfig :
        BaseExporterConfig,
        ICutsceneExportConfig
    {
        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="xmlParamsElem"></param>
        private CutsceneExportConfig(XElement xmlParamsElem)
            : base(xmlParamsElem)
        {
        }
        #endregion // Constructor(s)

        #region Static Controller Methods
        /// <summary>
        /// Load configuration data from XML document.
        /// </summary>
        /// <param name="xmlDoc"></param>
        public static ICutsceneExportConfig Load(XDocument xmlDoc)
        {
            XElement xmlParamsElem = xmlDoc.XPathSelectElement("/export/Cutscene/Parameters");
            ICutsceneExportConfig config = new CutsceneExportConfig(xmlParamsElem);
            return (config);
        }
        #endregion // Static Controller Methods
    }

} // RSG.Base.Configuration.Export.Implementation namespace
