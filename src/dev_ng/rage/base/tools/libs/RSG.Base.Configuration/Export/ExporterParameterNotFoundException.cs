﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Configuration.Export
{

    /// <summary>
    /// 
    /// </summary>
    public class ExporterParameterNotFoundException : Exception
    {
        #region Constructor(s)
        public ExporterParameterNotFoundException()
            : base()
        {
        }

        public ExporterParameterNotFoundException(String message)
            : base(message)
        {
        }

        public ExporterParameterNotFoundException(String message, Exception inner)
            : base(message, inner)
        {
        }

        public ExporterParameterNotFoundException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context)
            : base(info, context)
        {
        }
        #endregion // Constructor(s)
    }

} // RSG.Base.Configuration.Export namespace
