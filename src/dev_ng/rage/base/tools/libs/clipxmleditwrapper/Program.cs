﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.OS;
using System.Diagnostics;

namespace clipxmleditwrapper
{
    class Program
    {
        private static readonly String OPTION_XML = "xml";
        private static readonly String OPTION_CLIP = "clip";
        private static readonly String OPTION_EXE = "exe";

        static int RunProcess(string strExe, string strArgs)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.Arguments = strArgs;
            startInfo.FileName = strExe;
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.CreateNoWindow = true;
            startInfo.RedirectStandardOutput = true;
            startInfo.UseShellExecute = false;

            Process proc = Process.Start(startInfo);
            String output = proc.StandardOutput.ReadToEnd();
            proc.WaitForExit();

            return proc.ExitCode;
        }

        static void Main(string[] args)
        {
            Getopt options = new Getopt(args, new LongOption[] {
                new LongOption(OPTION_XML, LongOption.ArgType.Optional, 
                    "convert to xml format."),
                new LongOption(OPTION_CLIP, LongOption.ArgType.Optional, 
                    "convert to clip format."),
                new LongOption(OPTION_EXE, LongOption.ArgType.Required, 
                    "clip executable."),
            });

            bool to_xml = options.HasOption(OPTION_XML);
            bool to_clip = options.HasOption(OPTION_CLIP);
            String clip_exe = options[OPTION_EXE] as String;
            string[] trailing = options.TrailingOptions;

            string arguments = String.Empty;
            if (to_xml) arguments += "-toXml ";
            if (to_clip) arguments += "-toClip ";
            arguments += ("-clips " + String.Join(",", trailing));

            RunProcess(clip_exe, arguments);
        }
    }
}
