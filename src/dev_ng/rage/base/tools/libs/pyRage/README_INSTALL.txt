To build, run something like:

C:\Python25\python.exe setup.py bdist_egg

This creates a subdirectory dist\ that contains a python egg.

Note:
	Python modules require Visual Studio 2003 to compile, which also means
	we need to use a custom win32_toolbeta.h that removes the variadic
	macros which aren't supported

