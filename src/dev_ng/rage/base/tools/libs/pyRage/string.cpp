#include "Python.h"

#include "string/stringhash.h"

// Need to define these two somewhere...
namespace rage
{
	int AssertFailed(const char*, const char*, int, char*)
	{
		// do nothing
		return 0;
	}

	int AssertText(const char*, ...)
	{
		// do nothing
		return 0;
	}
}

static PyObject* rage_atStringHash(PyObject* self, PyObject* args)
{
	const char* string;
	unsigned long result;
	if (!PyArg_ParseTuple(args, "s", &string))
	{
		return NULL;
	}
	result = ::rage::atStringHash(string);
	return PyLong_FromUnsignedLong(result);
}


static PyObject* rage_atLiteralStringHash(PyObject* self, PyObject* args)
{
	const char* string;
	unsigned long result;
	if (!PyArg_ParseTuple(args, "s", &string))
	{
		return NULL;
	}
	result = ::rage::atLiteralStringHash(string);
	return PyLong_FromUnsignedLong(result);
}

static PyMethodDef RageStringMethods[] = {
	{"atStringHash", rage_atStringHash, METH_VARARGS, "Return the hash value of a string (lowercasing chars and normalizing slashes)"},
	{"atLiteralStringHash", rage_atLiteralStringHash, METH_VARARGS, "Returns the hash value of the string (with no normalization)"},
	{NULL, NULL, 0, NULL}
};

PyMODINIT_FUNC
initstring(void)
{
	Py_InitModule("string", RageStringMethods);
}

