from setuptools import setup
#from distutils.core import setup
from distutils.extension import Extension



stringModule = Extension(
    'rage.string',
    sources=['string.cpp', '../../../base/src/string/stringhash.cpp'],
    include_dirs=['../../../base/src'],
    extra_compile_args=['/FI..\\..\\..\\base\\tools\\pyRage\\win32_toolbeta.h'],
    define_macros=[('__midl', '0')], # UGLY HACK!!! Needs to be < 501
)

setup(name='rage',
      version='1.0',
      ext_modules=[stringModule],
      packages=['rage'],
	  zip_safe=False
      )
