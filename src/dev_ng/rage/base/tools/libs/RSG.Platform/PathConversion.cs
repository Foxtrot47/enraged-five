﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

using RSG.ManagedRage; // For OBSOLETE function
using RSG.Base.ConfigParser; // For OBSOLETE funciton


namespace RSG.Platform
{

    /// <summary>
    /// Path conversion methods; handling platform path and extension string
    /// conversion.
    /// </summary>
    [Obsolete("Use RSG.Pipeline.Services.Process.PlatformPathConversion instead.")]
    public static class PathConversion
    {
        /// <summary>
        /// Regex to match extensions that start with 'i' and contain 1 or more additional characters which are captured by the 'file_type' group.
        /// </summary>
        [Obsolete]
        private static readonly Regex s_ExtensionRegex = new Regex(@"^i(?'file_type'[A-Za-z0-9]+)$", RegexOptions.IgnoreCase);
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="extension"></param>
        /// <param name="targetInfo"></param>
        /// <returns></returns>
        [Obsolete]
        public static string ConvertIndependentExtensionToPlatform(ref string fileName, ConfigTargetInfo targetInfo)
        {
            string newext;

            // Trim off any preceding '.' characters
            string extension = Path.GetExtension(fileName);
            extension = extension.TrimStart(new char[] { '.' });

            Match match;

            switch (extension)
            {
                case "rpf":
                    newext = "rpf";
                    break;

                case "zip":
                    string fileNameWithoutLastExtension = Path.GetFileNameWithoutExtension(fileName);
                    string secondExtension = Path.GetExtension(fileNameWithoutLastExtension);

                    if (secondExtension != String.Empty)
                    {
                        match = s_ExtensionRegex.Match(extension);
                        if (match.Success)
                        {
                            Group fileTypeGroup = match.Groups["file_type"];

                            switch (targetInfo.Name)
                            {
                                case "independent":
                                    newext = "i" + fileTypeGroup.Value;
                                    break;

                                case "xbox360":
                                    newext = "x" + fileTypeGroup.Value;
                                    break;

                                case "ps3":
                                    newext = "c" + fileTypeGroup.Value;
                                    break;

                                case "win32":
                                    newext = "w" + fileTypeGroup.Value;
                                    break;

                                default:
                                    throw new ArgumentException(String.Format("{0} platform not found", targetInfo.Name));
                            }

                            // Strip off the secondary extension
                            fileName = Path.GetFileNameWithoutExtension(fileName);
                        }
                        else
                        {
                            newext = "rpf";
                        }
                    }
                    else
                    {
                        // ZIP -> rpf conversion
                        newext = "rpf";
                    }
                    break;

                case "ide":
                    newext = "ide";
                    break;

                case "meta":
                    switch (targetInfo.Name)
                    {
                        case "independent":
                            newext = "meta";
                            break;

                        case "xbox360":
                            newext = "xmt";
                            break;

                        case "ps3":
                            newext = "cmt";
                            break;

                        case "win32":
                            newext = "wmt";
                            break;

                        default:
                            throw new ArgumentException(String.Format("{0} platform not found", targetInfo.Name));
                    }
                    break;

                case "sco":
                    switch (targetInfo.Name)
                    {
                        case "independent":
                            newext = "sco";
                            break;

                        case "xbox360":
                            newext = "xsc";
                            break;

                        case "ps3":
                            newext = "csc";
                            break;

                        case "win32":
                            newext = "wsc";
                            break;

                        default:
                            throw new ArgumentException(String.Format("{0} platform not found", targetInfo.Name));
                    }
                    break;

                default:
                    // Attempt to use the regex to convert the extension
                    match = s_ExtensionRegex.Match(extension);
                    if (match.Success)
                    {
                        Group fileTypeGroup = match.Groups["file_type"];

                        switch (targetInfo.Name)
                        {
                            case "independent":
                                newext = "i" + fileTypeGroup.Value;
                                break;

                            case "xbox360":
                                newext = "x" + fileTypeGroup.Value;
                                break;

                            case "ps3":
                                newext = "c" + fileTypeGroup.Value;
                                break;

                            case "win32":
                                newext = "w" + fileTypeGroup.Value;
                                break;

                            default:
                                throw new ArgumentException(String.Format("{0} platform not found", targetInfo.Name));
                        }
                    }
                    else
                    {
                        newext = extension;
                    }
                    break;
            }

            return newext;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="targetInfo"></param>
        /// <param name="exportPath"></param>
        /// <param name="processedPath"></param>
        /// <returns></returns>
        [Obsolete]
        public static string ConvertIndependentFilenameToPlatform(string fileName, ConfigTargetInfo targetInfo, string exportPath, string processedPath)
        {
            string result = Path.GetFullPath(fileName);
            exportPath = Path.GetFullPath(exportPath);
            processedPath = Path.GetFullPath(processedPath);

            // Change the extension
            result = Path.ChangeExtension(result, ConvertIndependentExtensionToPlatform(ref fileName, targetInfo));

            // Check whether the path contains the export or processed directory and replace it with the build path
            if (result.Contains(exportPath))
            {
                result = result.Replace(exportPath, Path.GetFullPath(targetInfo.Directory));
            }
            else if (result.Contains(processedPath))
            {
                result = result.Replace(processedPath, Path.GetFullPath(targetInfo.Directory));
            }

            return Path.GetFullPath(result);
        }
    }
}
