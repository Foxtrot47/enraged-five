﻿// ---------------------------------------------------------------------------------------------
// <copyright file="FileTypeUtils.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2021. All rights reserved.
// </copyright>
// ---------------------------------------------------------------------------------------------

namespace RSG.Platform
{
    using System;
    using System.Collections.Generic;
    using System.Text.RegularExpressions;
    using RSG.Base.Extensions;

    /// <summary>
    /// FileType enumeration utility and extension methods.
    /// </summary>
    public static class FileTypeUtils
    {
        /// <summary>
        /// Attempts to convert an extension to a file type
        /// </summary>
        public static FileType ConvertExtensionToFileType(String extension)
        {
            if (!extensionFileTypeMap_.ContainsKey(extension))
            {
                FileType result = FileType.Unknown;

                foreach (FileType type in Enum.GetValues(typeof(FileType)))
                {
                    Regex reg = type.GetRegexValue();
                    if (reg != null && reg.IsMatch(extension))
                    {
                        result = type;
                    }
                }

                extensionFileTypeMap_.Add(extension, result);
            }

            return extensionFileTypeMap_[extension];
        }

        /// <summary>
        /// FileType enumeration extension method that returns the export
        /// extension String for a particular FileType value.
        /// </summary>
        public static String GetExportExtension(this FileType value) => value.GetPlatformExtension(Platform.Independent);

        /// <summary>
        /// Used to cache results of calls to ConvertExtensionToFileType().
        /// Calls to ConvertExtensionToFileType() should be O(log n) once this is populated.
        /// </summary>
        private static SortedDictionary<String, FileType> extensionFileTypeMap_ = new SortedDictionary<String, FileType>();
    }
}
