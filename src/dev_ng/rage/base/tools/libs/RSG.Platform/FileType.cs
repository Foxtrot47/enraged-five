﻿// ---------------------------------------------------------------------------------------------
// <copyright file="FileType.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2021. All rights reserved.
// </copyright>
// ---------------------------------------------------------------------------------------------

using System.Runtime.Serialization;
using RSG.Base.Attributes;
using RSG.Platform.Attributes;

namespace RSG.Platform
{
    /// <summary>
    /// Type of asset files.
    /// </summary>
    [DataContract]
    public enum FileType
    {
        [EnumMember]
        [Regex(@"^\.?\wbd$")]
        [FileTypeExtension("ibd", "?bd")]
        BoundsDictionary = 0,

        [EnumMember]
        [Regex(@"^\.?\wbn$")]
        [FileTypeExtension("ibn", "?bn")]
        BoundsFile,

        [EnumMember]
        [Regex(@"^\.?\wbs$")]
        [FileTypeExtension("ibs", "?bs")]
        Blendshape,

        [EnumMember]
        [Regex(@"^\.?\wcd$")]
        [FileTypeExtension("icd", "?cd")]
        ClipDictionary,

        [EnumMember]
        [Regex(@"^\.?\wdd$")]
        [FileTypeExtension("idd", "?dd")]
        DrawableDictionary,

        [EnumMember]
        [Regex(@"^\.?\wdr$")]
        [FileTypeExtension("idr", "?dr")]
        Drawable,

        [EnumMember]
        [Regex(@"^\.?\wft$")]
        [FileTypeExtension("ift", "?ft")]
        Fragment,

        [EnumMember]
        [Regex(@"^\.?\wtd$")]
        [FileTypeExtension("itd", "?td")]
        TextureDictionary,

        [EnumMember]
        [Regex(@"^\.?\wld$")]
        [FileTypeExtension("ild", "?ld")]
        ClothDictionary,

        [EnumMember]
        [Regex(@"^\.?\wsd$")]
        [FileTypeExtension("isd", "?sd")]
        BlendshapeDictionary,

        [EnumMember]
        [Regex(@"^\.?\wpt$")]
        [FileTypeExtension("ipt", "?pt")]
        VisualEffectDictionary,

        [EnumMember]
        [Regex(@"^\.?\wed$")]
        [FileTypeExtension("ied", "?ed")]
        ExpressionDictionary,

        [EnumMember]
        [Regex(@"^\.?\wfd$")]
        [FileTypeExtension("ifd", "?fd")]
        FrameFilterDictionary,

        [EnumMember]
        [Regex(@"^\.?(\wmt|meta)$")]
        [FileTypeExtension("meta", "?mt")]
        Metadata,

        [EnumMember]
        [Regex(@"^\.?(\wmf)$")]
        [FileTypeExtension("imf", "?mf")]
        Manifest,

        [EnumMember]
        [Regex(@"^\.pso$")]
        [FileTypeExtension("pso", "?mt")]
        PSO, // PSO is a metadata contained format (i.e. ".pso.meta")

        [EnumMember]
        [Regex(@"^\.?(\wsc|sco)$")]
        [FileTypeExtension("sco", "?sc")]
        Script,

        [EnumMember]
        [Regex(@"^\.?\wtyp$")]
        [FileTypeExtension("ityp", "?typ")]
        ITYP,

        [EnumMember]
        [Regex(@"^\.?\wmap$")]
        [FileTypeExtension("imap", "?map")]
        IMAP,

        [EnumMember]
        [Regex(@"^\.?tcs$")]
        [FileTypeExtension("tcs", null)]
        TexturePipelineSpecification,

        [EnumMember]
        [Regex(@"^\.?tcp$")]
        [FileTypeExtension("tcp", null)]
        TexturePipelineTemplate,

        [EnumMember]
        [Regex(@"^\.?tcl$")]
        [FileTypeExtension("tcl", null)]
        TexturePipelineTemplateLink,

        [EnumMember]
        [Regex(@"^\.?rpf")]
        [FileTypeExtension("rpf", "rpf")]
        RagePackFile,

        [EnumMember]
        [Regex(@"^\.?zip$")]
        [FileTypeExtension("zip", "rpf")]
        ZipArchive,

        [EnumMember]
        [Regex(@"^\.?(cutxml|cut)$")]
        [FileTypeExtension("cutxml", "cut")]
        CutsceneMetadata,

        [EnumMember]
        [Regex(@"^\.?bnd")]
        [FileTypeExtension("bnd", null)]
        ExportBoundFile,

        [EnumMember]
        [Regex(@"^\.?anim")]
        [FileTypeExtension("anim", null)]
        AnimFile,

        [EnumMember]
        [Regex(@"^\.?clip")]
        [FileTypeExtension("clip", null)]
        ClipFile,

        [EnumMember]
        File,

        [EnumMember]
        [Regex(@"^\.?vr")]
        [FileTypeExtension("ivr", "?vr")]
        VehicleRecording,

        [EnumMember]
        [Regex(@"^\.?mpo$")]
        [FileTypeExtension("mpo", null)]
        MaterialObject,

        [EnumMember]
        [Regex(@"^\.?mps$")]
        [FileTypeExtension("mps", null)]
        MaterialSurface,

        [EnumMember]
        [Regex(@"^\.?mpt$")]
        [FileTypeExtension("mpt", null)]
        MaterialTemplate,

        [EnumMember]
        [Regex(@"^\.?mpi$")]
        [FileTypeExtension("mpi", null)]
        MaterialTemplateExport,

        [EnumMember]
        [Regex(@"^\.?imvf")]
        [FileTypeExtension("imvf", "mrf")]
        MotionTree,

        [EnumMember]
        [Regex(@"^\.?\wbvh$")]
        [FileTypeExtension("ibvh", "?bvh")]
        ModelBvhData,

        [EnumMember]
        [Regex(@"^\.?\wbvhd$")]
        [FileTypeExtension("ibvhd", "?bvhd")]
        ModelBvhDictionary,

        [EnumMember]
        Unknown

    } // FileType
} // RSG.Platform namespace
