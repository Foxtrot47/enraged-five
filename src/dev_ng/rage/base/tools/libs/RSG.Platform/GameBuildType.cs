﻿//---------------------------------------------------------------------------------------------
// <copyright file="GameBuildType.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2015. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Platform
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Enumeration to represent game build type (debug, beta etc).
    /// </summary>
    [DataContract]
    public enum GameBuildType
    {
        [EnumMember]
        Beta,

        [EnumMember]
        BankRelease,

        [EnumMember]
        Debug,

        [EnumMember]
        Release,

        [EnumMember]
        Final
    }

} // RSG.Platform namespace
