﻿//---------------------------------------------------------------------------------------------
// <copyright file="GamePlatformStringAttribute.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2011-2015. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Platform
{
    using System;
    using System.ComponentModel;
    using System.Reflection;
    using System.Runtime.Serialization;
    using RSG.Base.Attributes;
    using RSG.Platform.Attributes;

    /*! \mainpage
     *
     * \section intro_sec Purpose
     *
     * The RSG.Platform assembly provides a very basic platform abstraction
     * layer; a core enumeration for all recognised platforms, a core file
     * type enumeration and a few supporting methods.
     * 
     * This is used by the core asset pipeline and many other utilities.
     * 
     */

    /// <summary>
    /// Platform enumeration.
    /// </summary>
    [DataContract]
    [Serializable]
    public enum Platform
    {
        [EnumMember]
        [RagebuilderPlatform("independent")]
        [FriendlyName("Export")]
        [FieldDisplayName("Export")]
        [PlatformExtensionCharacter('i')]
        [PlatformOutputDirectory("independent")]
        [PlatformEndianness(RSG.Base.Endianness.LittleEndian)]
        [PlatformGenerationAndFeatures(PlatformGeneration.None, PlatformFeatures.All)]
        Independent,

        [EnumMember]
        [RagebuilderPlatform("win32")]
        [GamePlatformString("Win32")]
        [RagebuilderTextureTemplate(".win32.tcp")]
        [FriendlyName("Windows x86")]
        [FieldDisplayName("Windows x86")]
        [PlatformExtensionCharacter('w')]
        [PlatformOutputDirectory("pc")]
        [PlatformEndianness(RSG.Base.Endianness.LittleEndian)]
        [Is64BitArchitecture(false)]
        [PlatformGenerationAndFeatures(PlatformGeneration.Seventh, PlatformFeatures.Generation7Default)]
        Win32,

        [EnumMember]
        [RagebuilderPlatform("win64")]
        [GamePlatformString("Win64")]
        [RagebuilderTextureTemplate(".win64.tcp")]
        [FriendlyName("Windows x64")]
        [FieldDisplayName("Windows x64")]
        [PlatformExtensionCharacter('y')]
        [PlatformOutputDirectory("x64")]
        [PlatformEndianness(RSG.Base.Endianness.LittleEndian)]
        [Is64BitArchitecture(true)]
        [CompilerSetting("x64")]
        [PlatformGenerationAndFeatures(PlatformGeneration.Eigth, PlatformFeatures.Generation8Default)]
        Win64,

        [EnumMember]
        [RagebuilderPlatform("ps3")]
        [GamePlatformString("PlayStation3")]
        [RagebuilderTextureTemplate(".ps3.tcp")]
        [FriendlyName("PlayStation 3")]
        [FieldDisplayName("PlayStation 3")]
        [PlatformExtensionCharacter('c')]
        [PlatformOutputDirectory("ps3")]
        [PlatformEndianness(RSG.Base.Endianness.BigEndian)]
        [Is64BitArchitecture(false)]
        [PlatformGenerationAndFeatures(PlatformGeneration.Seventh, PlatformFeatures.Generation7Default)]
        PS3,

        [EnumMember]
        [RagebuilderPlatform("xenon")]
        [GamePlatformString("Xbox360")]
        [RagebuilderTextureTemplate(".xenon.tcp")]
        [FriendlyName("Xbox 360")]
        [FieldDisplayName("Xbox 360")]
        [PlatformExtensionCharacter('x')]
        [PlatformOutputDirectory("xbox360")]
        [PlatformEndianness(RSG.Base.Endianness.BigEndian)]
        [Is64BitArchitecture(false)]
        [PlatformGenerationAndFeatures(PlatformGeneration.Seventh, PlatformFeatures.Generation7Default)]
        Xbox360,

        [EnumMember]
        [RagebuilderPlatform("orbis")]
        [GamePlatformString("PlayStation4")]
        [RagebuilderTextureTemplate(".ps4.tcp")]
        [FriendlyName("PlayStation 4")]
        [FieldDisplayName("PlayStation 4")]
        [PlatformExtensionCharacter('o')]
        [PlatformOutputDirectory("x64")]
        [PlatformEndianness(RSG.Base.Endianness.LittleEndian)]
        [Is64BitArchitecture(true)]
        [CompilerSetting("Orbis")]
        [PlatformGenerationAndFeatures(PlatformGeneration.Eigth, PlatformFeatures.Generation8Default)]
        PS4,

        [EnumMember]
        [RagebuilderPlatform("durango")]
        [GamePlatformString("XboxOne")]
        [RagebuilderTextureTemplate(".xb1.tcp")]
        [FriendlyName("Xbox One")]
        [FieldDisplayName("Xbox One")]
        [PlatformExtensionCharacter('d')]
        [PlatformOutputDirectory("xboxone")]
        [PlatformEndianness(RSG.Base.Endianness.LittleEndian)]
        [Is64BitArchitecture(true)]
        [CompilerSetting("Durango")]
        [PlatformGenerationAndFeatures(PlatformGeneration.Eigth, PlatformFeatures.Generation8Default)]
        XboxOne,

        [EnumMember]
        [RagebuilderPlatform("prospero")]
        [GamePlatformString("Prospero")]
        [RagebuilderTextureTemplate(".ps5.tcp")]
        [FriendlyName("Playstation 5")]
        [FieldDisplayName("Playstation 5")]
        [PlatformExtensionCharacter('p')]
        [PlatformOutputDirectory("ps5")]
        [PlatformEndianness(RSG.Base.Endianness.LittleEndian)]
        [Is64BitArchitecture(true)]
        [CompilerSetting("Prospero")]
        [PlatformGenerationAndFeatures(PlatformGeneration.Ninth, PlatformFeatures.Generation9Default)]
        PS5,

        [EnumMember]
        [RagebuilderPlatform("scarlett")]
        [GamePlatformString("Gaming.Xbox.Scarlett.x64")]
        [RagebuilderTextureTemplate(".xbsx.tcp")]
        [FriendlyName("Xbox Series X")]
        [FieldDisplayName("Xbox Series X")]
        [PlatformExtensionCharacter('z')]
        [PlatformOutputDirectory("xbsx")]
        [PlatformEndianness(RSG.Base.Endianness.LittleEndian)]
        [Is64BitArchitecture(true)]
        [CompilerSetting("Scarlett")]
        [PlatformGenerationAndFeatures(PlatformGeneration.Ninth, PlatformFeatures.Generation9Default)]
        XBSX,

        [EnumMember]
        [RagebuilderPlatform("durango")]
        [GamePlatformString("Gaming.Xbox.XboxOne.x64")]
        [RagebuilderTextureTemplate(".xb1.tcp")]
        [FieldDisplayName("Xbox One GDK")]
        [FriendlyName("Xbox One GDK")]
        [PlatformExtensionCharacter('d')]
        [PlatformOutputDirectory("xboxone")]
        [PlatformEndianness(RSG.Base.Endianness.LittleEndian)]
        [Is64BitArchitecture(true)]
        [CompilerSetting("Durango")]
        [PlatformGenerationAndFeatures(PlatformGeneration.Eigth, PlatformFeatures.Generation8Default)]
        XboxOneGDK,
    }

    /// <summary>
    /// Platform enumeration utility and extension methods.
    /// </summary>
    public static class PlatformUtils
    {

        /// <summary>
        /// Return Ragebuilder Platform String for a Platform enum value.
        /// </summary>
        /// <param name="platform"></param>
        /// <returns></returns>
        public static String PlatformToRagebuilderPlatform(this Platform platform)
        {
            Type type = platform.GetType();

            FieldInfo fi = type.GetField(platform.ToString());
            RagebuilderPlatformAttribute[] attributes =
                fi.GetCustomAttributes(typeof(RagebuilderPlatformAttribute), false) as RagebuilderPlatformAttribute[];

            return (attributes.Length > 0 ? attributes[0].RagebuilderPlatform : null);
        }

        /// <summary>
        /// Return Game Platform String for a Platform enum value.
        /// </summary>
        /// <param name="platform"></param>
        /// <returns></returns>
        public static String PlatformToGamePlatform(this Platform platform)
        {
            Type type = platform.GetType();

            FieldInfo fi = type.GetField(platform.ToString());
            GamePlatformStringAttribute[] attributes =
                fi.GetCustomAttributes(typeof(GamePlatformStringAttribute), false) as GamePlatformStringAttribute[];

            return (attributes.Length > 0 ? attributes[0].GamePlatform : null);
        }

        /// <summary>
        /// Return Ragebuilder Texture Template extension for a Platform enum value.
        /// </summary>
        /// <param name="platform"></param>
        /// <returns></returns>
        public static String PlatformRagebuilderTextureTemplateExtension(this Platform platform)
        {
            Type type = platform.GetType();

            FieldInfo fi = type.GetField(platform.ToString());
            RagebuilderTextureTemplateAttribute[] attributes =
                fi.GetCustomAttributes(typeof(RagebuilderTextureTemplateAttribute), false) as RagebuilderTextureTemplateAttribute[];

            return (attributes.Length > 0 ? attributes[0].TextureTemplateExtension : null);
        }

        /// <summary>
        /// Return Ragebuilder Platform String for a Platform enum value.
        /// </summary>
        /// <param name="platform"></param>
        /// <returns></returns>
        public static String PlatformToCompilerSetting(this Platform platform)
        {
            Type type = platform.GetType();

            FieldInfo fi = type.GetField(platform.ToString());
            CompilerSettingAttribute[] attributes =
                fi.GetCustomAttributes(typeof(CompilerSettingAttribute), false) as CompilerSettingAttribute[];

            return (attributes.Length > 0 ? attributes[0].CompilerSetting : null);
        }

        /// <summary>
        /// Return platform endianness for a Platform enum value.
        /// </summary>
        /// <param name="platform"></param>
        /// <returns></returns>
        public static RSG.Base.Endianness Endianness(this Platform platform)
        {
            Type type = platform.GetType();

            FieldInfo fi = type.GetField(platform.ToString());
            PlatformEndiannessAttribute[] attributes =
                fi.GetCustomAttributes(typeof(PlatformEndiannessAttribute), false) as PlatformEndiannessAttribute[];

            if (attributes.Length > 0)
                return (attributes[0].Endianness);
            else
                throw (new ArgumentException("platform has no endianness defined.", "platform"));
        }

        /// <summary>
        /// Return whether platform is 64-bit architecture.
        /// </summary>
        /// <param name="platform"></param>
        /// <returns></returns>
        public static bool Is64BitArchitecture(this Platform platform)
        {
            Type type = platform.GetType();

            FieldInfo fi = type.GetField(platform.ToString());
            Is64BitArchitectureAttribute[] attributes =
                fi.GetCustomAttributes(typeof(Is64BitArchitectureAttribute), false) as Is64BitArchitectureAttribute[];

            if (attributes.Length > 0)
                return (attributes[0].Is64BitArchitecture);
            else
                throw (new ArgumentException("platform has no architecture defined.", "platform"));
        }

        /// <summary>
        /// Return Platform enum value from a Ragebuilder platform string.
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public static RSG.Platform.Platform RagebuilderPlatformToPlatform(String p)
        {
            RSG.Platform.Platform[] platforms =
                (RSG.Platform.Platform[])Enum.GetValues(typeof(RSG.Platform.Platform));

            Type type = typeof(RSG.Platform.Platform);
            foreach (RSG.Platform.Platform plat in platforms)
            {
                FieldInfo fi = type.GetField(plat.ToString());
                RagebuilderPlatformAttribute[] attributes =
                    fi.GetCustomAttributes(typeof(RagebuilderPlatformAttribute), false) as RagebuilderPlatformAttribute[];

                if (attributes.Length > 0 && attributes[0].RagebuilderPlatform.Equals(p, StringComparison.OrdinalIgnoreCase))
                    return (plat);
            }
            return (Platform.Independent);
        }

        /// <summary>
        /// Return Platform enum value from a game platform string.
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public static RSG.Platform.Platform GamePlatformToPlatform(String p)
        {
            RSG.Platform.Platform[] platforms =
                (RSG.Platform.Platform[])Enum.GetValues(typeof(RSG.Platform.Platform));

            Type type = typeof(RSG.Platform.Platform);
            foreach (RSG.Platform.Platform plat in platforms)
            {
                FieldInfo fi = type.GetField(plat.ToString());
                GamePlatformStringAttribute[] attributes =
                    fi.GetCustomAttributes(typeof(GamePlatformStringAttribute), false) as GamePlatformStringAttribute[];

                if (attributes.Length > 0 && attributes[0].GamePlatform.Equals(p, StringComparison.OrdinalIgnoreCase))
                    return (plat);
            }
            return (Platform.Independent);
        }

        /// <summary>
        /// Return friendly name String for a Platform enum value.
        /// </summary>
        public static String PlatformToFriendlyName(this Platform platform)
        {
            Type type = platform.GetType();

            FieldInfo fi = type.GetField(platform.ToString());
            FriendlyNameAttribute[] attributes =
                fi.GetCustomAttributes(typeof(FriendlyNameAttribute), false) as FriendlyNameAttribute[];

            return (attributes.Length > 0 ? attributes[0].FriendlyName : null);
        }

        /// <summary>
        /// Return Platform Extension Character for a Platform enum value.
        /// </summary>
        /// <param name="platform"></param>
        /// <returns></returns>
        public static Char PlatformRagebuilderCharacter(this Platform platform)
        {
            Type type = platform.GetType();

            FieldInfo fi = type.GetField(platform.ToString());
            PlatformExtensionCharacterAttribute[] attributes =
                fi.GetCustomAttributes(typeof(PlatformExtensionCharacterAttribute), false) as PlatformExtensionCharacterAttribute[];

            return (attributes.Length > 0 ? attributes[0].ExtensionCharacter : Char.MinValue);
        }

        /// <summary>
        /// Return PlatformOutput String for a Platform enum value.
        /// </summary>
        /// <param name="platform"></param>
        /// <returns></returns>
        public static String PlatformToOutputDirectory(this Platform platform)
        {
            Type type = platform.GetType();

            FieldInfo fi = type.GetField(platform.ToString());
            PlatformOutputDirectoryAttribute[] attributes =
                fi.GetCustomAttributes(typeof(PlatformOutputDirectoryAttribute), false) as PlatformOutputDirectoryAttribute[];

            return (attributes.Length > 0 ? attributes[0].PlatformOutputDirectory : null);
        }

        /// <summary>
        /// Return Platform value from String representation.
        /// </summary>
        /// <param name="platform">Platform string.</param>
        /// <returns></returns>
        public static Platform PlatformFromString(String platform)
        {
            RSG.Platform.Platform[] platforms =
                (RSG.Platform.Platform[])Enum.GetValues(typeof(RSG.Platform.Platform));

            foreach (RSG.Platform.Platform p in platforms)
            {
                if (String.Equals(platform, p.ToString(), StringComparison.OrdinalIgnoreCase))
                    return (p);
            }
            return (Platform.Independent);
        }

        /// <summary>
        /// Return Platform value from friendly name string representation.
        /// </summary>
        /// <param name="friendlyName"></param>
        /// <returns></returns>
        public static Platform PlatformFromFriendlyName(String friendlyName)
        {
            foreach (Platform platform in Enum.GetValues(typeof(Platform)))
            {
                if (platform.PlatformToFriendlyName() == friendlyName)
                {
                    return platform;
                }
            }

            return Platform.Independent;
        }

        /// <summary>
        /// Return Platform from an asset filename.
        /// </summary>
        /// <param name="filename">Asset filename</param>
        /// <returns>Platform value</returns>
        public static Platform FilenameToPlatform(String filename)
        {
            String extension = System.IO.Path.GetExtension(filename);
            FileType ft = FileTypeUtils.ConvertExtensionToFileType(extension);
            if (FileType.Unknown == ft)
            {
                // Default; for unknown asset types.
                return (Platform.Independent);
            }
            else
            {
                // Otherwise we determine platform based on extension start
                // character.
                Platform[] platforms = (Platform[])Enum.GetValues(typeof(Platform));
                foreach (Platform platform in platforms)
                {
                    Char platformChar = platform.PlatformRagebuilderCharacter();
                    if (extension[1].Equals(platformChar))
                        return (platform);
                }
            }
            return (Platform.Independent);
        }
    }

} // RSG.Platform namespace
