﻿//---------------------------------------------------------------------------------------------
// <copyright file="PlatformFeature.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Platform
{
    using System;

    /// <summary>
    /// Hardware feature flags.  Way of tagging platforms that can support particular
    /// features giving a finer grain of control over the PlatformGeneration enumeration.
    /// </summary>
    [Flags]
    public enum PlatformFeatures : ulong
    {
        /// <summary>
        /// No hardware features.
        /// </summary>
        None = 0x00000000,

        /// <summary>
        /// All hardware features.
        /// </summary>
        All = ulong.MaxValue,

        /// <summary>
        /// Seventh generation hardware default features.
        /// </summary>
        Generation7Default = All,

        /// <summary>
        /// Eigth generation hardware default features.
        /// </summary>
        Generation8Default = All,

        /// <summary>
        /// Ninth generation hardware default features.
        /// </summary>
        Generation9Default = All,
    }

} // RSG.Platform namespace
