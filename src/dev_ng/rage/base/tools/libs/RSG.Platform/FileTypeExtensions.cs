﻿//---------------------------------------------------------------------------------------------
// <copyright file="FileTypeExtensions.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2012-2021. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Platform
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using RSG.Platform.Attributes;

    /// <summary>
    /// <see cref="RSG.Platform.FileType"/> extension methods.
    /// </summary>
    public static class FileTypeExtensions
    {
        #region Fields
        /// <summary>
        /// Field for caching the <see cref="FileTypeExtensionAttribute"/> attribute values.
        /// Since these are defined by compile-time attributes we can cache them.
        /// </summary>
        private static readonly IDictionary<FileType, FileTypeExtensionAttribute> FileTypeExtensionAttributes =
            new Dictionary<FileType, FileTypeExtensionAttribute>();
        #endregion // Fields

        #region Methods
        /// <summary>
        /// FileType enumeration extension method that returns both the export
        /// and platform extension strings for a particular FileType value.
        /// </summary>
        /// <param name="value">
        /// <see cref="FileType"/> value.
        /// </param>
        /// <param name="exportExt">
        /// Export/independent string extension (including ".").
        /// </param>
        /// <param name="platformExt">
        /// Platform string extension (including ".").
        /// </param>
        public static void GetExtensions(this FileType value, out String exportExt, out String platformExt)
        {
            FileTypeExtensionAttribute attribute;
            if (!FileTypeExtensionAttributes.TryGetValue(value, out attribute))
            {
                Type type = value.GetType();

                System.Reflection.FieldInfo fi = type.GetField(value.ToString());
                FileTypeExtensionAttribute[] attributes = fi.GetCustomAttributes(
                    typeof(FileTypeExtensionAttribute), false) as FileTypeExtensionAttribute[];

                // Update our cache.
                FileTypeExtensionAttributes[value] = attributes.FirstOrDefault();
                attribute = attributes.FirstOrDefault();
            }

            if (null != attribute)
            {
                exportExt = attribute.ExportExtension;
                platformExt = attribute.PlatformExtension;
            }
            else
            {
                exportExt = null;
                platformExt = null;
            }
        }

        /// <summary>
        /// FileType enumeration extension method that returns the platform
        /// extension String for a particular FileType value.
        /// </summary>
        /// <param name="value">
        /// <see cref="FileType"/> value.
        /// </param>
        /// <returns>
        /// <see cref="System.String"/> platform extension (including ".").
        /// </returns>
        public static String GetPlatformExtension(this FileType value)
        {
            // Unknown file type isn't a valid input.
            if (value == FileType.Unknown)
            {
                throw new ArgumentException("Trying to create an extension for the unknown filetype.");
            }

            FileTypeExtensionAttribute attribute;
            lock (FileTypeExtensionAttributes)
            {
                if (!FileTypeExtensionAttributes.TryGetValue(value, out attribute))
                {
                    Type type = value.GetType();

                    System.Reflection.FieldInfo fi = type.GetField(value.ToString());
                    FileTypeExtensionAttribute[] attributes = fi.GetCustomAttributes(
                        typeof(FileTypeExtensionAttribute), false) as FileTypeExtensionAttribute[];

                    // Update our cache.
                    FileTypeExtensionAttributes[value] = attributes.FirstOrDefault();
                    attribute = attributes.FirstOrDefault();
                }
            }

            if (null != attribute)
            {
                return attribute.PlatformExtension;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Return the extension string for a platforms file type.
        /// </summary>
        /// <param name="value">
        /// <see cref="FileType"/> value.
        /// </param>
        /// <param name="platform">
        /// Platform to determine extension.
        /// </param>
        /// <returns>
        /// File extension (including ".").
        /// </returns>
        public static String GetPlatformExtension(this FileType value, Platform platform)
        {
            // Unknown file type isn't a valid input.
            if (value == FileType.Unknown)
            {
                throw new ArgumentException("Trying to create an extension for the unknown filetype.");
            }

            FileTypeExtensionAttribute attribute;
            if (!FileTypeExtensionAttributes.TryGetValue(value, out attribute))
            {
                Type type = value.GetType();

                System.Reflection.FieldInfo fi = type.GetField(value.ToString());
                FileTypeExtensionAttribute[] attributes = fi.GetCustomAttributes(
                    typeof(FileTypeExtensionAttribute), false) as FileTypeExtensionAttribute[];

                // Update our cache.
                FileTypeExtensionAttributes[value] = attributes.FirstOrDefault();
                attribute = attributes.FirstOrDefault();
            }

            if (null != attribute)
            {
                if (platform == Platform.Independent)
                {
                    return attribute.ExportExtension;
                }
                else
                {
                    return String.Format(".{0}", attribute.PlatformExtension.Replace('?', platform.PlatformRagebuilderCharacter()));
                }
            }
            else
            {
                return null;
            }
        }
        #endregion // Methods
    }
} // RSG.Platform namespace
