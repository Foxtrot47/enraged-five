﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Platform.Attributes
{

    /// <summary>
    /// Attribute that specifies the platform is a 64-bit architecture.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = false)]
    class Is64BitArchitectureAttribute : Attribute
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public bool Is64BitArchitecture
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="is64bit"></param>
        public Is64BitArchitectureAttribute(bool is64bit)
        {
            this.Is64BitArchitecture = is64bit;
        }
        #endregion // Constructor(s)
    }

} // RSG.Platform.Attributes 
