﻿using System;

namespace RSG.Platform.Attributes
{

    /// <summary>
    /// Attribute for defining the VisualStudioPlatformAttribute string on an enum value.
    /// </summary>
    public class VisualStudioPlatformAttribute : Attribute
    {
        #region Properties
        /// <summary>
        /// CompilerSetting string assigned to enum value.
        /// </summary>
        public String VisualStudioPlatform
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="platform"></param>
        public VisualStudioPlatformAttribute(String setting)
        {
            this.VisualStudioPlatform = setting;
        }
        #endregion // Constructor(s)
    }

} // RSG.Platform.Attributes namespace
