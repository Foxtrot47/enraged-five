﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Platform.Attributes
{

    /// <summary>
    /// OBSOLETE Attribute class.  DO NOT USE.
    /// </summary>
    [Obsolete("Use IBranch.Targets dictionary instead.")]
    public class PlatformOutputDirectoryAttribute : Attribute
    {
        #region Properties
        /// <summary>
        /// Friendly name for the field.
        /// </summary>
        public String PlatformOutputDirectory
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor; taking friendly name string.
        /// </summary>
        /// <param name="friendly_name"></param>
        public PlatformOutputDirectoryAttribute(String output_name)
        {
            this.PlatformOutputDirectory = output_name;
        }
        #endregion // Constructor(s)
    }

} // RSG.Platform.Attributes namespace
