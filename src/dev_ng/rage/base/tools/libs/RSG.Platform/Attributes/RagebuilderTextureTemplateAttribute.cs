﻿using System;

namespace RSG.Platform.Attributes
{

    /// <summary>
    /// Attribute for defining the Ragebuilder Texture Template extension string.
    /// </summary>
    public class RagebuilderTextureTemplateAttribute : Attribute
    {
        #region Properties
        /// <summary>
        /// Texture template extension (e.g. ".win32.tcp").
        /// </summary>
        public String TextureTemplateExtension
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="platform"></param>
        public RagebuilderTextureTemplateAttribute(String extension)
        {
            this.TextureTemplateExtension = extension;
        }
        #endregion // Constructor(s)
    }

} // RSG.Platform.Attributes namespace
