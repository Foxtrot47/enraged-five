﻿using System;

namespace RSG.Platform.Attributes
{

    /// <summary>
    /// Attribute for defining the CompilerSetting string on an enum value.
    /// </summary>
    public class CompilerSettingAttribute : Attribute
    {
        #region Properties
        /// <summary>
        /// CompilerSetting string assigned to enum value.
        /// </summary>
        public String CompilerSetting
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="platform"></param>
        public CompilerSettingAttribute(String setting)
        {
            this.CompilerSetting = setting;
        }
        #endregion // Constructor(s)
    }

} // RSG.Platform.Attributes namespace
