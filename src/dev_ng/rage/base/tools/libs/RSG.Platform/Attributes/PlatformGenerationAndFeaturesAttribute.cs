﻿//---------------------------------------------------------------------------------------------
// <copyright file="PlatformGenerationAndFeaturesAttribute.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Platform.Attributes
{
    using System;
    
    /// <summary>
    /// Attribute for defining platform hardware generation and features.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field)]
    public class PlatformGenerationAndFeaturesAttribute : Attribute
    {
        #region Properties
        /// <summary>
        /// Platform hardware generation.
        /// </summary>
        public PlatformGeneration HardwareGeneration
        {
            get;
            private set;
        }

        /// <summary>
        /// Platform hardware features.
        /// </summary>
        public PlatformFeatures HardwareFeatures
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="generation"></param>
        /// <param name="features"></param>
        public PlatformGenerationAndFeaturesAttribute(PlatformGeneration generation, PlatformFeatures features)
        {
            this.HardwareGeneration = generation;
            this.HardwareFeatures = features;
        }
        #endregion // Constructor(s)
    }

} // RSG.Platform.Attributes namespace
