﻿using System;

namespace RSG.Platform.Attributes
{

    /// <summary>
    /// Attribute for defining platform endianness (byte-order).
    /// </summary>
    [AttributeUsage(AttributeTargets.Field)]
    public class PlatformEndiannessAttribute : Attribute
    {
        #region Properties
        /// <summary>
        /// Platform endianness.
        /// </summary>
        public RSG.Base.Endianness Endianness
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="endianness"></param>
        public PlatformEndiannessAttribute(RSG.Base.Endianness endianness)
        {
            this.Endianness = endianness;
        }
        #endregion // Constructor(s)
    }

} // RSG.Platform.Attributes namespace
