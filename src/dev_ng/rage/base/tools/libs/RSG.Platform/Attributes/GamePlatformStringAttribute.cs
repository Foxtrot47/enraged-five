﻿//---------------------------------------------------------------------------------------------
// <copyright file="GamePlatformStringAttribute.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2015. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Platform.Attributes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Defines a platform string as used by RAG bank protocol for platform
    /// identification.
    /// </summary>
    /// <see cref="%RAGE_DIR%\base\src\bank\packet.cpp" />
    /// 
    public class GamePlatformStringAttribute : Attribute
    {
        #region Properties
        /// <summary>
        /// Game platform string.
        /// </summary>
        public String GamePlatform
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="platform"></param>
        public GamePlatformStringAttribute(String platform)
        {
            this.GamePlatform = platform;
        }
        #endregion // Constructor(s)
    }

} // RSG.Platform.Attributes namespace
