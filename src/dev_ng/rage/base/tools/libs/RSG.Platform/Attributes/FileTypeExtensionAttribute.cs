﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Platform.Attributes
{

    /// <summary>
    /// Attribute that assigns an export and platform extension to a FileType
    /// enum value.  This is used for encoding the transitions for the pipeline.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, AllowMultiple=false, Inherited=false)]
    public class FileTypeExtensionAttribute : Attribute
    {
        #region Properties
        /// <summary>
        /// Export (independent) extension pattern string assigned to a
        /// FileType enum value.
        /// </summary>
        public String ExportExtension
        {
            get;
            private set;
        }

        /// <summary>
        /// Platform extension pattern string assigned to a FileType enum value.
        /// '?' character will be substituted for platform filename conversion.
        /// </summary>
        public String PlatformExtension
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor; specifying the export and platform extension strings.
        /// </summary>
        /// <param name="exportExt"></param>
        /// <param name="platformExt"></param>
        public FileTypeExtensionAttribute(String exportExt, String platformExt)
        {
            this.ExportExtension = exportExt;
            this.PlatformExtension = platformExt;
        }
        #endregion // Constructor(s)
    }

} // RSG.Platform.Attributes namespace
