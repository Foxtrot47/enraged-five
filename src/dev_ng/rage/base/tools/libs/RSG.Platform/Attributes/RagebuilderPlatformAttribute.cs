﻿using System;

namespace RSG.Platform.Attributes
{

    /// <summary>
    /// Attribute for defining the Ragebuilder Platform string on an enum value.
    /// </summary>
    public class RagebuilderPlatformAttribute : Attribute
    {
        #region Properties
        /// <summary>
        /// Ragebuilder Platform string assigned to enum value.
        /// </summary>
        public String RagebuilderPlatform
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="platform"></param>
        public RagebuilderPlatformAttribute(String platform)
        {
            this.RagebuilderPlatform = platform;
        }
        #endregion // Constructor(s)
    }

} // RSG.Platform.Attributes namespace
