﻿using System;

namespace RSG.Platform.Attributes
{

    /// <summary>
    /// Attribute for defining the Ragebuilder Platform character.
    /// </summary>
    public class PlatformExtensionCharacterAttribute : Attribute
    {
        #region Properties
        /// <summary>
        /// Ragebuilder Platform character assigned to enum value.
        /// </summary>
        public Char ExtensionCharacter
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="character"></param>
        public PlatformExtensionCharacterAttribute(Char character)
        {
            this.ExtensionCharacter = character;
        }
        #endregion // Constructor(s)
    }

} // RSG.Platform.Attributes namespace
