﻿//---------------------------------------------------------------------------------------------
// <copyright file="PlatformGeneration.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Platform
{

    /// <summary>
    /// Hardware generation enumeration.  This could be used as a way of 
    /// marking features to support based on hardware-generations to
    /// support generation transitions.  Alternatively a hardware feature
    /// flag enumeration may be required.
    /// </summary>
    public enum PlatformGeneration
    {
        /// <summary>
        /// Unknown hardware generation.
        /// </summary>
        None,

        /// <summary>
        /// Seventh generation; Xbox 360, PS3 etc.
        /// </summary>
        Seventh,

        /// <summary>
        /// Eigth generation; Xbox One, PS4 etc.
        /// </summary>
        Eigth,

        /// <summary>
        /// Ninth generation; PS5, Xbox Series X etc.
        /// </summary>
        Ninth,
    }

} // RSG.Platform namespace
