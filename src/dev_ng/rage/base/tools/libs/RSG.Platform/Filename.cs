﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace RSG.Platform
{

    /// <summary>
    /// Generic filename and path utility methods.
    /// </summary>
    public static class Filename
    {

        /// <summary>
        /// Return filename basename; stripping off all extensions.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static String GetBasename(String filename)
        {
            String filenameOnly = Path.GetFileName(filename);
            return (filenameOnly.Substring(0, filenameOnly.IndexOf('.')));
        }

        /// <summary>
        /// Return an array of string extensions; to determine file types.
        /// </summary>
        /// <param name="filename">Input filename.</param>
        /// <returns>Array of String extensions.</returns>
        /// E.g.
        ///   PathConversion.GetExtensions(@"x:\path\asset.idr.zip");
        ///   --> [ "zip", "idr" ]
        ///   
        public static String[] GetExtensions(String filename)
        {
            List<String> extensions = new List<String>();
            String p = Path.GetFileNameWithoutExtension(filename);
            String ext = Path.GetExtension(filename);
            while (!String.IsNullOrEmpty(ext))
            {
                extensions.Add(ext);
                ext = Path.GetExtension(p);
                p = Path.GetFileNameWithoutExtension(p);
            }

            return (extensions.ToArray());
        }

        /// <summary>
        /// Return an array of FileType enum values; to determine file types.
        /// </summary>
        /// <param name="extensions">Array of extensions.</param>
        /// <returns></returns>
        public static FileType[] GetFileTypes(IEnumerable<String> extensions)
        {
            IEnumerable<FileType> filetypes = extensions.Select(ext => FileTypeUtils.ConvertExtensionToFileType(ext));
            return (filetypes.ToArray());
        }

        /// <summary>
        /// Return an array of FileType enum values; to determine file types.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static FileType[] GetFileTypes(String filename)
        {
            List<String> extensions = new List<String>(GetExtensions(filename));
            return (GetFileTypes(extensions));
        }

    }

} // RSG.Platform namespace
