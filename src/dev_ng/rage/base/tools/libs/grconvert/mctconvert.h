// 
// grconvert/mctconvert.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 


namespace rage {

struct MctHeader {
	u32 m_Magic;	// 'MCT'
	u32 m_Width;
	u32 m_Height;
	u32 m_LevelCount;
	u32 m_Format;	// D3DFMT_DXT1, _DXT5
	u32 m_DataSize;
	// Data follows
};

extern bool MctConvert(const char *dds,const char *mct,int quality /*0-100*/ );

extern u64 g_TotalDds, g_TotalMct;

}	// namespace rage
