// 
// tools/libs/grconvert/texturexenonproxy.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#if !__64BIT
#include "grcore/texturexenonproxy.h"

#include "diag/tracker.h"
#include "grcore/image.h"
#include "system/memory.h"
#include "system/param.h"

#ifdef DBG
#undef DBG
#endif
#define DBG 0
#define _X86_
#include "system/xtl.h"
#include "xdk.h"
#pragma warning(disable: 4668)
#pragma warning(disable: 4062)
#include "d3d9.h"
#pragma warning(error: 4062)
#pragma warning(error: 4668)
#include "xboxmath.h"
#include "xgraphics.h"

namespace rage {

extern int g_rscDebug;

#if __RESOURCECOMPILER

static inline D3DFORMAT ExpandGPUFormat(D3DFORMAT format)
{
	u32 GPU_format = ((u32)format & D3DFORMAT_TEXTUREFORMAT_MASK) >> D3DFORMAT_TEXTUREFORMAT_SHIFT;

	switch (GPU_format)
	{
	case GPUTEXTUREFORMAT_16_FLOAT          : GPU_format = GPUTEXTUREFORMAT_16_EXPAND         ; break;
	case GPUTEXTUREFORMAT_16_16_FLOAT       : GPU_format = GPUTEXTUREFORMAT_16_16_EXPAND      ; break;
	case GPUTEXTUREFORMAT_16_16_16_16_FLOAT : GPU_format = GPUTEXTUREFORMAT_16_16_16_16_EXPAND; break;
	}

	return (D3DFORMAT)(((u32)format & ~D3DFORMAT_TEXTUREFORMAT_MASK) | (GPU_format << D3DFORMAT_TEXTUREFORMAT_SHIFT));
}

static inline D3DFORMAT UnExpandGPUFormat(D3DFORMAT format)
{
	u32 GPU_format = ((u32)format & D3DFORMAT_TEXTUREFORMAT_MASK) >> D3DFORMAT_TEXTUREFORMAT_SHIFT;

	switch (GPU_format)
	{
	case GPUTEXTUREFORMAT_16_EXPAND          : GPU_format = GPUTEXTUREFORMAT_16_FLOAT         ; break;
	case GPUTEXTUREFORMAT_16_16_EXPAND       : GPU_format = GPUTEXTUREFORMAT_16_16_FLOAT      ; break;
	case GPUTEXTUREFORMAT_16_16_16_16_EXPAND : GPU_format = GPUTEXTUREFORMAT_16_16_16_16_FLOAT; break;
	}

	return (D3DFORMAT)(((u32)format & ~D3DFORMAT_TEXTUREFORMAT_MASK) | (GPU_format << D3DFORMAT_TEXTUREFORMAT_SHIFT));
}

static inline D3DFORMAT ExpandGPUFormat_AS_16_16_16_16(D3DFORMAT format)
{
	u32 GPU_format = ((u32)format & D3DFORMAT_TEXTUREFORMAT_MASK) >> D3DFORMAT_TEXTUREFORMAT_SHIFT;

	switch (GPU_format)
	{
	case GPUTEXTUREFORMAT_8_8_8_8    : GPU_format = GPUTEXTUREFORMAT_8_8_8_8_AS_16_16_16_16   ; break;
	case GPUTEXTUREFORMAT_DXT1       : GPU_format = GPUTEXTUREFORMAT_DXT1_AS_16_16_16_16      ; break;
	case GPUTEXTUREFORMAT_DXT2_3     : GPU_format = GPUTEXTUREFORMAT_DXT2_3_AS_16_16_16_16    ; break;
	case GPUTEXTUREFORMAT_DXT4_5     : GPU_format = GPUTEXTUREFORMAT_DXT4_5_AS_16_16_16_16    ; break;
	case GPUTEXTUREFORMAT_2_10_10_10 : GPU_format = GPUTEXTUREFORMAT_2_10_10_10_AS_16_16_16_16; break;
	case GPUTEXTUREFORMAT_10_11_11   : GPU_format = GPUTEXTUREFORMAT_10_11_11_AS_16_16_16_16  ; break;
	case GPUTEXTUREFORMAT_11_11_10   : GPU_format = GPUTEXTUREFORMAT_11_11_10_AS_16_16_16_16  ; break;
	}

	return (D3DFORMAT)(((u32)format & ~D3DFORMAT_TEXTUREFORMAT_MASK) | (GPU_format << D3DFORMAT_TEXTUREFORMAT_SHIFT));
}

/*static*/ inline D3DFORMAT UnExpandGPUFormat_AS_16_16_16_16(D3DFORMAT format)
{
	u32 GPU_format = ((u32)format & D3DFORMAT_TEXTUREFORMAT_MASK) >> D3DFORMAT_TEXTUREFORMAT_SHIFT;

	switch (GPU_format)
	{
	case GPUTEXTUREFORMAT_8_8_8_8_AS_16_16_16_16    : GPU_format = GPUTEXTUREFORMAT_8_8_8_8   ; break;
	case GPUTEXTUREFORMAT_DXT1_AS_16_16_16_16       : GPU_format = GPUTEXTUREFORMAT_DXT1      ; break;
	case GPUTEXTUREFORMAT_DXT2_3_AS_16_16_16_16     : GPU_format = GPUTEXTUREFORMAT_DXT2_3    ; break;
	case GPUTEXTUREFORMAT_DXT4_5_AS_16_16_16_16     : GPU_format = GPUTEXTUREFORMAT_DXT4_5    ; break;
	case GPUTEXTUREFORMAT_2_10_10_10_AS_16_16_16_16 : GPU_format = GPUTEXTUREFORMAT_2_10_10_10; break;
	case GPUTEXTUREFORMAT_10_11_11_AS_16_16_16_16   : GPU_format = GPUTEXTUREFORMAT_10_11_11  ; break;
	case GPUTEXTUREFORMAT_11_11_10_AS_16_16_16_16   : GPU_format = GPUTEXTUREFORMAT_11_11_10  ; break;
	}

	return (D3DFORMAT)(((u32)format & ~D3DFORMAT_TEXTUREFORMAT_MASK) | (GPU_format << D3DFORMAT_TEXTUREFORMAT_SHIFT));
}

#define GPUSWIZZLE_RED      (GPUSWIZZLE_X<<D3DFORMAT_SWIZZLEX_SHIFT | GPUSWIZZLE_0<<D3DFORMAT_SWIZZLEY_SHIFT | GPUSWIZZLE_0<<D3DFORMAT_SWIZZLEZ_SHIFT | GPUSWIZZLE_1<<D3DFORMAT_SWIZZLEW_SHIFT) // R001 (matches DXGI red formats)
#define GPUSWIZZLE_REDGREEN (GPUSWIZZLE_X<<D3DFORMAT_SWIZZLEX_SHIFT | GPUSWIZZLE_Y<<D3DFORMAT_SWIZZLEY_SHIFT | GPUSWIZZLE_0<<D3DFORMAT_SWIZZLEZ_SHIFT | GPUSWIZZLE_1<<D3DFORMAT_SWIZZLEW_SHIFT) // RG01 (matches DXGI red/green formats)

// these two formats don't exist in d3d9types.h for some reason ..
#define D3DFMT_R8  (_D3DFORMAT)MAKED3DFMT(GPUTEXTUREFORMAT_8, GPUENDIAN_NONE, TRUE, GPUSIGN_ALL_UNSIGNED, GPUNUMFORMAT_FRACTION, GPUSWIZZLE_OOOR)
#define D3DFMT_R16 (_D3DFORMAT)MAKED3DFMT(GPUTEXTUREFORMAT_16, GPUENDIAN_8IN16, TRUE, GPUSIGN_ALL_UNSIGNED, GPUNUMFORMAT_FRACTION, GPUSWIZZLE_OOOR)

// these formats are fixed so that they match DXGI
#define D3DFMT_R8_fixed      (_D3DFORMAT)((D3DFMT_R8      & ~D3DFORMAT_SWIZZLE_MASK) | GPUSWIZZLE_RED)
#define D3DFMT_R16_fixed     (_D3DFORMAT)((D3DFMT_R16     & ~D3DFORMAT_SWIZZLE_MASK) | GPUSWIZZLE_RED)
#define D3DFMT_R16F_fixed    (_D3DFORMAT)((D3DFMT_R16F    & ~D3DFORMAT_SWIZZLE_MASK) | GPUSWIZZLE_RED)
#define D3DFMT_R32F_fixed    (_D3DFORMAT)((D3DFMT_R32F    & ~D3DFORMAT_SWIZZLE_MASK) | GPUSWIZZLE_RED)
#define D3DFMT_CTX1_fixed    (_D3DFORMAT)((D3DFMT_CTX1    & ~D3DFORMAT_SWIZZLE_MASK) | GPUSWIZZLE_REDGREEN)
#define D3DFMT_DXN_fixed     (_D3DFORMAT)((D3DFMT_DXN     & ~D3DFORMAT_SWIZZLE_MASK) | GPUSWIZZLE_REDGREEN)
#define D3DFMT_G8R8_fixed    (_D3DFORMAT)((D3DFMT_G8R8    & ~D3DFORMAT_SWIZZLE_MASK) | GPUSWIZZLE_REDGREEN)
#define D3DFMT_G16R16_fixed  (_D3DFORMAT)((D3DFMT_G16R16  & ~D3DFORMAT_SWIZZLE_MASK) | GPUSWIZZLE_REDGREEN)
#define D3DFMT_G16R16F_fixed (_D3DFORMAT)((D3DFMT_G16R16F & ~D3DFORMAT_SWIZZLE_MASK) | GPUSWIZZLE_REDGREEN)
#define D3DFMT_G32R32F_fixed (_D3DFORMAT)((D3DFMT_G32R32F & ~D3DFORMAT_SWIZZLE_MASK) | GPUSWIZZLE_REDGREEN)

static D3DFORMAT GetD3DFormat(grcImage::Format imgFormat, bool bIsLinear) {
	// NOTE!  There is a copy of this code on the tool side and the game side
	// [tool] -> rage/base/tools/libs/grconvert/texturexenonproxy.cpp
	// [game] -> rage/base/src/grcore/texturexenon.cpp
	// These must be kept in sync.

	D3DFORMAT format = D3DFMT_UNKNOWN;

	switch (imgFormat)
	{
	case grcImage::UNKNOWN                     : format = D3DFMT_UNKNOWN          ; break;
	case grcImage::DXT1                        : format = D3DFMT_DXT1             ; break;
	case grcImage::DXT3                        : format = D3DFMT_DXT3             ; break;
	case grcImage::DXT5                        : format = D3DFMT_DXT5             ; break;
	case grcImage::CTX1                        : format = D3DFMT_CTX1_fixed       ; break;
	case grcImage::DXT3A                       : format = D3DFMT_DXT3A            ; break;
	case grcImage::DXT3A_1111                  : format = D3DFMT_DXT3A_1111       ; break;
	case grcImage::DXT5A                       : format = D3DFMT_DXT5A            ; break;
	case grcImage::DXN                         : format = D3DFMT_DXN_fixed        ; break;
	case grcImage::BC6                         : format = D3DFMT_UNKNOWN          ; break;
	case grcImage::BC7                         : format = D3DFMT_UNKNOWN          ; break;
	case grcImage::A8R8G8B8                    : format = D3DFMT_A8R8G8B8         ; break;
	case grcImage::A8B8G8R8                    : format = D3DFMT_A8B8G8R8         ; break;
	case grcImage::A8                          : format = D3DFMT_A8               ; break;
	case grcImage::L8                          : format = D3DFMT_L8               ; break;
	case grcImage::A8L8                        : format = D3DFMT_A8L8             ; break;
	case grcImage::A4R4G4B4                    : format = D3DFMT_A4R4G4B4         ; break;
	case grcImage::A1R5G5B5                    : format = D3DFMT_A1R5G5B5         ; break;
	case grcImage::R5G6B5                      : format = D3DFMT_R5G6B5           ; break;
	case grcImage::R3G3B2                      : format = D3DFMT_UNKNOWN          ; break; // not supported
	case grcImage::A8R3G3B2                    : format = D3DFMT_UNKNOWN          ; break; // not supported
	case grcImage::A4L4                        : format = D3DFMT_UNKNOWN          ; break; // not supported
	case grcImage::A2R10G10B10                 : format = D3DFMT_A2R10G10B10      ; break;
	case grcImage::A2B10G10R10                 : format = D3DFMT_A2B10G10R10      ; break;
	case grcImage::A16B16G16R16                : format = D3DFMT_A16B16G16R16     ; break;
	case grcImage::G16R16                      : format = D3DFMT_G16R16_fixed     ; break;
	case grcImage::L16                         : format = D3DFMT_L16              ; break;
	case grcImage::A16B16G16R16F               : format = D3DFMT_A16B16G16R16F    ; break;
	case grcImage::G16R16F                     : format = D3DFMT_G16R16F_fixed    ; break;
	case grcImage::R16F                        : format = D3DFMT_R16F_fixed       ; break;
	case grcImage::A32B32G32R32F               : format = D3DFMT_A32B32G32R32F    ; break;
	case grcImage::G32R32F                     : format = D3DFMT_G32R32F_fixed    ; break;
	case grcImage::R32F                        : format = D3DFMT_R32F_fixed       ; break;
	case grcImage::D15S1                       : format = D3DFMT_UNKNOWN          ; break; // not supported
	case grcImage::D24S8                       : format = D3DFMT_D24S8            ; break;
	case grcImage::D24FS8                      : format = D3DFMT_D24FS8           ; break;
	case grcImage::P4                          : format = D3DFMT_UNKNOWN          ; break; // not supported
	case grcImage::P8                          : format = D3DFMT_UNKNOWN          ; break; // not supported
	case grcImage::A8P8                        : format = D3DFMT_UNKNOWN          ; break; // not supported
	case grcImage::R8                          : format = D3DFMT_R8_fixed         ; break;
	case grcImage::R16                         : format = D3DFMT_R16_fixed        ; break;
	case grcImage::G8R8                        : format = D3DFMT_G8R8_fixed       ; break;
	case grcImage::LINA32B32G32R32F_DEPRECATED : format = D3DFMT_LIN_A32B32G32R32F; break;
	case grcImage::LINA8R8G8B8_DEPRECATED      : format = D3DFMT_LIN_A8R8G8B8     ; break;
	case grcImage::LIN8_DEPRECATED             : format = D3DFMT_LIN_L8           ; break;
	case grcImage::RGBE                        : format = D3DFMT_A8R8G8B8         ; break;
	}

	if (format == D3DFMT_UNKNOWN && 0) // cast unsupported formats?
	{
		switch (grcImage::GetFormatBitsPerPixel(imgFormat))
		{
		case   4 : format = D3DFMT_DXT1         ; break;
		case   8 : format = D3DFMT_L8           ; break;
		case  16 : format = D3DFMT_A1R5G5B5     ; break;
		case  32 : format = D3DFMT_A8R8G8B8     ; break;
		case  64 : format = D3DFMT_A16B16G16R16 ; break;
		case 128 : format = D3DFMT_A32B32G32R32F; break;
		}
	}

	if (format != D3DFMT_UNKNOWN)
	{
		if (bIsLinear)
		{
			format = (D3DFORMAT)MAKELINFMT(format);
		}

		if (0) // don't do this here .. don't use float textures if you need them to be filterable (but we still want to have the option to use float textures)
		{
			format = ExpandGPUFormat(format); // expand x16_FLOAT formats to x16_EXPAND, so they will be filterable
		}
	}

	return format;
}

#if __RESOURCECOMPILER || __BANK

static u32 _SetTextureSwizzle(grcTexture::eTextureSwizzle swizzle)
{
	switch (swizzle)
	{
	case grcTexture::TEXTURE_SWIZZLE_R : return GPUSWIZZLE_X;
	case grcTexture::TEXTURE_SWIZZLE_G : return GPUSWIZZLE_Y;
	case grcTexture::TEXTURE_SWIZZLE_B : return GPUSWIZZLE_Z;
	case grcTexture::TEXTURE_SWIZZLE_A : return GPUSWIZZLE_W;
	case grcTexture::TEXTURE_SWIZZLE_0 : return GPUSWIZZLE_0;
	case grcTexture::TEXTURE_SWIZZLE_1 : return GPUSWIZZLE_1;
	}

	return 0;
}

void grcTextureXenonProxy::SetTextureSwizzle(eTextureSwizzle r, eTextureSwizzle g, eTextureSwizzle b, eTextureSwizzle a, bool bApplyToExistingSwizzle)
{
	if (bApplyToExistingSwizzle)
	{
		eTextureSwizzle existing[4];

		GetTextureSwizzle(existing[0], existing[1], existing[2], existing[3]);

		r = ApplyToExistingSwizzle(r, existing);
		g = ApplyToExistingSwizzle(g, existing);
		b = ApplyToExistingSwizzle(b, existing);
		a = ApplyToExistingSwizzle(a, existing);
	}

	m_Texture->Format.SwizzleX = _SetTextureSwizzle(r);
	m_Texture->Format.SwizzleY = _SetTextureSwizzle(g);
	m_Texture->Format.SwizzleZ = _SetTextureSwizzle(b);
	m_Texture->Format.SwizzleW = _SetTextureSwizzle(a);
}

static grcTexture::eTextureSwizzle _GetTextureSwizzle(u32 swizzle)
{
	switch (swizzle)
	{
	case GPUSWIZZLE_X : return grcTexture::TEXTURE_SWIZZLE_R;
	case GPUSWIZZLE_Y : return grcTexture::TEXTURE_SWIZZLE_G;
	case GPUSWIZZLE_Z : return grcTexture::TEXTURE_SWIZZLE_B;
	case GPUSWIZZLE_W : return grcTexture::TEXTURE_SWIZZLE_A;
	case GPUSWIZZLE_0 : return grcTexture::TEXTURE_SWIZZLE_0;
	case GPUSWIZZLE_1 : return grcTexture::TEXTURE_SWIZZLE_1;
	}

	return grcTexture::TEXTURE_SWIZZLE_0;
}

void grcTextureXenonProxy::GetTextureSwizzle(eTextureSwizzle& r, eTextureSwizzle& g, eTextureSwizzle& b, eTextureSwizzle& a) const
{
	r = _GetTextureSwizzle(m_Texture->Format.SwizzleX);
	g = _GetTextureSwizzle(m_Texture->Format.SwizzleY);
	b = _GetTextureSwizzle(m_Texture->Format.SwizzleZ);
	a = _GetTextureSwizzle(m_Texture->Format.SwizzleW);
}

#endif

struct Slot {
	void *Addr;
	int Count;
	int OverflowingCount;   // these are dxt5 pages that cannot be stolen for dxt1 (both top and bottom half of the page has data, so offsetting by 1/2 page, means some data goes over)
	char Name[128-8];
	bool HasDxt5Tail;
};

static atFixedArray<Slot,8192> s_SlotLists[2];

void grcTextureFactoryXenonProxy::ClearFreeList() 
{
	s_SlotLists[0].Reset();
	s_SlotLists[1].Reset();
}

PARAM(nointerleavemips,"[grconvert] Disable Xenon mip interleaving (on by default now)");
PARAM(forcecontiguousmips,"[grconvert] Force contiguous mip chains on Xenon (off by default)");

int grcTextureXenonProxy::AddFreeSlots(bool isDXT1,void *addr,int count, int overflowingCount, const char *filename, bool hasDxt5Tail) 
{
	if (!PARAM_nointerleavemips.Get()) {
	//	Debugf2(g_rscDebug,"interleave: Texture '%s' donates %d DXT%d slots.",filename,count,isDXT1?1:5);
		Slot &s = s_SlotLists[isDXT1].Append();
		s.Addr = addr;
		s.Count = count;
		s.OverflowingCount = overflowingCount;
		safecpy(s.Name, filename, sizeof(s.Name));
		s.HasDxt5Tail = hasDxt5Tail;
	}
	return count;
}

void* grcTextureXenonProxy::FindFreeSlot(bool isDXT1, int count, int overflowingCount, const char *filename, bool tailOnly) 
{
 	int dxt1Back=0;
	int dxt5Back=0;
	int slotCount = s_SlotLists[isDXT1].GetCount();

	if (overflowingCount < count)   // we don't allow requests that don't go at least one past the overflow count (though we could bump the count up, that usually does not help)
	{
		for (int countAdj=0;countAdj<4;countAdj++)  // we want to find the one closest to the right size
		{
			for (int i=0; i<slotCount; i++)		
			{
				Slot &slotSet = s_SlotLists[isDXT1][i];
				if (slotSet.Count == count+countAdj) 
				{
					void *result = slotSet.Addr;

					int slotsLeft = slotSet.Count - count;
				
					if (slotsLeft)			
						dxt1Back = AddFreeSlots(isDXT1,(u8*)result + count*((isDXT1)?8192:16384),slotSet.Count - count,Max(0,slotSet.OverflowingCount-count),slotSet.Name,slotSet.HasDxt5Tail);

					Debugf2(g_rscDebug,"interleave: Texture '%s' borrows %d DXT%d slots from '%s' (gives back %d, wastes %d slots).",filename,count,isDXT1?1:5,slotSet.Name, dxt1Back, slotSet.Count - count - dxt1Back);
					
					s_SlotLists[isDXT1].Delete(i);
					return result;
				}
			}
		}

		// a special case for a count==2 non overflowing dxt1 texture can fit in the miptail of a dxt5.
		if (isDXT1 && count == 2 && overflowingCount==0)
		{
			int slotCount = s_SlotLists[0].GetCount();
			
			for (int testCount=1;testCount<4;testCount++)  // we want to find the one closest to just a tail.
			{
				for (int i=0; i<slotCount; i++)
				{
					Slot &slotSet = s_SlotLists[0][i];	// look for dxt5 candidate
					
					if (slotSet.Count == testCount && slotSet.HasDxt5Tail)  // looking for a dxt5 set with a clean (3 slot) tail
					{
						void *result = (void*)((u8*)slotSet.Addr + 16384*(slotSet.Count-1));	// cut off the tail off and give back the reset

						result = (u8*)result - 4096; // back up slightly into the unused part of the dxt5 miptail, so we get 3 4 k pages

						int slotsLeft = slotSet.Count-1;
												
						if (slotsLeft)				  // give back the ones we did not use above us.
							dxt5Back = AddFreeSlots(false, slotSet.Addr, slotsLeft, 0, slotSet.Name, false);

						Debugf2(g_rscDebug,"interleave: Texture '%s' DXT1 texture steals %d DXT1 slots from '%s' (gives back %, wastes %d slots).",filename, count, slotSet.Name, dxt5Back, slotSet.Count - count - dxt5Back);
						s_SlotLists[0].Delete(i);

						return result;
					}
				}
			}		
		}

		// similarly a dxt5 miptail can fit into a dxt1 slot.
		if (!isDXT1 && count==1 && overflowingCount==0 && tailOnly)	// simple 16x16 tail fits in a a simple 4K page 
		{
			int slotCount = s_SlotLists[1].GetCount();
			
			for (int testCount=1;testCount<4;testCount++)  // we want to find the one closest to just a tail.
			{
				for (int i=0; i<slotCount; i++)
				{
					Slot &slotSet = s_SlotLists[1][i];	// look for dxt1 candidate
					
					if ((slotSet.Count-slotSet.OverflowingCount) == testCount) // need at leave one with full 4 k available
					{
						void *result = (void*)((u8*)slotSet.Addr + 8192*slotSet.OverflowingCount); // skip over the full/overflowed slots so the dxt5 does not mess with them 

						int slotsLeft = slotSet.Count - (1+slotSet.OverflowingCount);
						
						if (slotsLeft)				  // give back the ones we did not use below us.
							dxt1Back = AddFreeSlots(true,(u8*)result+8192,slotsLeft, 0, slotSet.Name, slotSet.HasDxt5Tail);
					
						if (slotSet.OverflowingCount) // give back the ones we skipped.
							dxt1Back = AddFreeSlots(true,slotSet.Addr,slotSet.OverflowingCount, slotSet.OverflowingCount, slotSet.Name, false);

						Debugf2(g_rscDebug,"interleave: Texture '%s' DXT5 texture steals %d DXT1 slots from '%s' (gives back %d, wastes %d slots).",filename,count,slotSet.Name, dxt1Back, slotSet.Count - count - dxt1Back);
						s_SlotLists[1].Delete(i);

						return result;
					}
				}
			}
		}
	}

	// very common single page dxt1 request that could be overflowing, we can steal a dxt5 slot...
	if (isDXT1 && count == 1)
	{
		int slotCount = s_SlotLists[0].GetCount();
		for (int testCount=1;testCount<4;testCount++)  // we want to find the one closest to just a tail.
		{
			for (int i=0; i<slotCount; i++)
			{
				Slot &slotSet = s_SlotLists[0][i];
				if ((slotSet.Count-slotSet.OverflowingCount) == testCount) 
				{
					void *result = (void*)((u8*)slotSet.Addr + 16384*slotSet.OverflowingCount); // skip over the full dxt5 slots (they are using both halves);  				

					int slotsRemaining = slotSet.Count - (1+slotSet.OverflowingCount);
					if (slotsRemaining==0)
					{
						if (slotSet.HasDxt5Tail) // if we used the mip tail, we can give back then adjust position back 4k. and give back a slot
						{
							result = (u8*)result - 4096;

							if (overflowingCount)	// if the new one does not overflow, give back a dxt5, otherwise give back a dxt1
								dxt1Back = AddFreeSlots(true,(u8*)result+8192,1,0,slotSet.Name,false);
							else
								dxt5Back = AddFreeSlots(false,(u8*)result+4096,1,0,slotSet.Name,false);
						}
						else if (overflowingCount==0)  //if used the 32x32 level, And did not OVERFLOW, give back 1 dxt1 page
						{
							dxt1Back = AddFreeSlots(true,(u8*)result+4096,1,0,slotSet.Name,false);
						}	
					}
					else
					{
						// give back the rest of the chain.
						dxt5Back = AddFreeSlots(false,(u8*)result+16384,slotsRemaining,0,slotSet.Name,slotSet.HasDxt5Tail);
					}

					Debugf2(g_rscDebug,"interleave: Texture '%s' DXT1 texture steals %d DXT5 slot(s) from '%s' (gives back %d dxt1 slots, %d dxt5 slots, wastes %d slots).",filename,count,slotSet.Name, dxt1Back, dxt5Back, slotSet.Count - count - dxt1Back - dxt5Back);
				
					s_SlotLists[0].Delete(i);
					return result;
				}
			}
		}
	}


	return NULL;
}

void grcTextureXenonProxy::DisplayFreeSlotMem()
{
	for (int dxt=1;dxt>=0;dxt--)
	{
		for (int count = 0;count<3;count++)
		{
			int freeSlots = 0;
			int slotCount = s_SlotLists[dxt].GetCount();
			for (int i=0; i<slotCount; i++)
			{
				if (s_SlotLists[dxt][i].Count == count+1) 
				{
					freeSlots++;
				}
			}
			Displayf("Unused mipmap interleaving free slots for DXT%d, count = %d: %d (%dk)", dxt?1:5, count+1, freeSlots, freeSlots*8);
		}
	}
}


void grcTextureXenonProxy::Init(const char *pFilename,grcImage *pImage,grcTextureFactory::TextureCreateParams *params)
{
	D3DFORMAT nFormat = GetD3DFormat(pImage->GetFormat(), (params && params->Format == grcTextureFactory::TextureCreateParams::LINEAR) || pImage->IsLinear());

	if(pImage->IsSRGB())
	{
		nFormat = (D3DFORMAT)MAKESRGBFMT(nFormat);
		nFormat = ExpandGPUFormat_AS_16_16_16_16(nFormat);
	}

	m_Name = grcSaveTextureNames ? StringDuplicate(pFilename) : 0;

	u8 imageType = (u8) pImage->GetType();
	Assign(m_LayerCount,pImage->GetLayerCount()-1);
	D3DLOCKED_RECT	rect = { 0, 0 };
	D3DLOCKED_BOX	box = { 0, 0, 0 };

	D3DBaseTexture *texture;
	UINT uBaseSize, uMipSize;
	int levels = pImage->GetExtraMipCount()+1;
	int width = pImage->GetWidth(), height = pImage->GetHeight(), depth = pImage->GetDepth();
	if (imageType == grcImage::CUBE) {
		texture = rage_new D3DCubeTexture;
		Assert("Cube maps must be square" && width == height);
		XGSetCubeTextureHeader(width, levels, 0, (D3DFORMAT)nFormat, D3DPOOL_MANAGED, 
			0, XGHEADER_CONTIGUOUS_MIP_OFFSET, (D3DCubeTexture *) texture, &uBaseSize, &uMipSize);
	}
	else if (imageType == grcImage::VOLUME) {
		texture = rage_new D3DVolumeTexture; 
		XGSetVolumeTextureHeader(width, height, depth, levels, 0, (D3DFORMAT)nFormat, D3DPOOL_MANAGED, 
			0, XGHEADER_CONTIGUOUS_MIP_OFFSET, (D3DVolumeTexture *) texture, &uBaseSize, &uMipSize);
	}
	else { 
		texture = rage_new D3DTexture;
		XGSetTextureHeader(width, height, levels, 0, (D3DFORMAT)nFormat, D3DPOOL_MANAGED, 
			0, XGHEADER_CONTIGUOUS_MIP_OFFSET, 0, (D3DTexture *) texture, &uBaseSize, &uMipSize);
	}
	texture->Common |= D3DCOMMON_CPU_CACHED_MEMORY;
	int scale = levels==1? 16 : levels == 2? (16+4) : (16+4+1);

	// If texture is square and in a DXT format we may be able to interleave it.
	bool contiguous = false;
	bool isAllSlots = false;
	bool canUseSlots = false;
	bool isDXT = grcImage::IsFormatDXTBlockCompressed(pImage->GetFormat());
	bool isDXT1 = (isDXT && grcImage::GetFormatBitsPerPixel(pImage->GetFormat()) == 4); // DXT1, CTX1, DXT3A, DXT3A_1111, DXT5A
	int pageSize = isDXT1? 8192 : 16384;
	int slots = 0;
	int overflowingSlots = 0;  // number of slots that "overflow" (data in top and bottom half pages) when reusing slots, and there for need one more nonoverflowing slot to flow into
	bool isDXT5Tail16 = (!isDXT1 && width<=16 && height==16);	// height must be exactly 16x16, 16x8 uses different 2 4k page. ;(
	bool isDXT5Tail32 = (!isDXT1 && width<=32 && height==32);
	bool hasDxt5Tail = false;

	if (imageType == grcImage::STANDARD && isDXT &&  (width/height<=2 && height/width<=2)) {
		int mipWidth =  width;
		int mipHeight =  height;

		int nonSlotLevels=0;

		slots = 0;
		for (int i=0; i<levels; i++, mipWidth>>=1, mipHeight>>=1)
		{		
			if (mipWidth<=64 && mipHeight<=128)
			{
				slots++;
				
				if (mipHeight>32)					// these have data in the top and bottom half pages
					overflowingSlots++;
				
				if (mipWidth<=16 && mipHeight==16)  // 16x16 and 8x16 dxt5s only use 1 4k page of the 4 allocated
					hasDxt5Tail = !isDXT1;
			}
			else
			{
				nonSlotLevels++;
			}

 			if ((mipWidth<=16 && mipHeight<=64) || ((mipWidth<=32 && mipHeight<=16)))  // 16x64 miptails in one page as do 32x16.
  				break;
		}	
		
		Assert (slots <=4);
		canUseSlots = (nonSlotLevels<=1); 
		isAllSlots  = (nonSlotLevels==0);
	}

	void *pBaseAddr = NULL, *pMipAddr = NULL;

#if __ASSERT
	if (pImage->GetConstructorInfo().m_debugTag != 0 &&
		pImage->GetConstructorInfo().m_debugTag != 'conv')
	{
		const char* tag = (const char*)&pImage->GetConstructorInfo().m_debugTag;

		Assertf(0, "### ERROR (grcTextureXenonProxy::Init): %s constructor tag is %c%c%c%c", pFilename, tag[3], tag[2], tag[1], tag[0]);
	}
#endif // __ASSERT

	bool forceContiguousMips = PARAM_forcecontiguousmips.Get();

	if (pImage->GetConstructorInfo().m_forceContiguousMips)
	{
		forceContiguousMips = true;
		DEV_ONLY(Displayf("forcing contiguous mips for %s", pFilename);)
	}

	if (slots && canUseSlots) {
	
		if (isAllSlots) {
			// look for a list that can hold the whole texture
			pBaseAddr = FindFreeSlot(isDXT1, slots, overflowingSlots, pFilename, isDXT5Tail16);
			
			if (pBaseAddr)
			{
				contiguous = true;
				slots = 0;
			} 
			else if (!forceContiguousMips && slots>1 ) // try splitting it in 2? 
			{
				pMipAddr = FindFreeSlot(isDXT1, slots-1, max(0,overflowingSlots-1), pFilename, isDXT5Tail32);			
				pBaseAddr = FindFreeSlot(isDXT1, 1, overflowingSlots>0?1:0, pFilename, isDXT5Tail16);
				
				if (pMipAddr)		// if we just found a mipAddr, set slots and overflow to what's left for the Base  
				{
					slots = 1;		
					overflowingSlots = overflowingSlots>0?1:0;
				}

				if (pBaseAddr) // if we found a slot for the base, peal off one slot and one overflow slot
				{
					slots--;
					overflowingSlots = max(0,overflowingSlots-1);
				}
			}
		}
		else if (!forceContiguousMips)
		{
			// this is the case where the  level is not slotable, but the mip chain is (usually 128x128)
			pMipAddr = FindFreeSlot(isDXT1, slots, overflowingSlots, pFilename, isDXT5Tail16);
			if (pMipAddr)
				slots = 0;
		}
	}

	SetPhysicalSize(uBaseSize + uMipSize);
	if (!pBaseAddr) {
		int size = uBaseSize;
		if (forceContiguousMips)
		{
			size += uMipSize;
			contiguous = true;
		}
		pBaseAddr = physical_new(size,D3DTEXTURE_ALIGNMENT);
		Debugf2(g_rscDebug,"pBaseAddr = 0x%08x, End = 0x%08x",pBaseAddr,(u8*)pBaseAddr+size);
		// If we were unable to use slots but could provide them, add them to the free list.
		// (we know we couldn't use the slots because pBaseAddr isn't set yet)
		
		if (isAllSlots)	// if we're all in slots, then even the base textures can contribute;
		{
			// just add one. if any over flow this one does.
			AddFreeSlots(isDXT1, (char*)pBaseAddr + (pageSize>>1), 1, (overflowingSlots>0) ? 1 : 0, pFilename, (uMipSize==0)&&hasDxt5Tail);
			overflowingSlots = Max(0,overflowingSlots-1);
			slots--;
		}
	}
	else 
		Debugf2(g_rscDebug,"Reused pBaseAddr = 0x%08x",pBaseAddr);

	if (!pMipAddr) {
		pMipAddr = uMipSize ? (contiguous ? ((char*)pBaseAddr + uBaseSize) : physical_new(uMipSize,D3DTEXTURE_ALIGNMENT)) : NULL;
		// If we weren't contiguous, we had a separate memory allocation and can provide our own slots.
		// Note that for larger textures the slots are at the very end.
		if (!contiguous && uMipSize)
			Debugf2(g_rscDebug,"mipAddr = 0x%08x, End = 0x%08x",pMipAddr,(u8*)pMipAddr+uMipSize);

		if (pMipAddr && slots > 0)
		{
			// just in case we get something wrong in the logic, NEVER donate more pages than you own!
			int maxSlots = (int)uMipSize/((isDXT1)?8192:16384);
			Assertf(maxSlots >= slots, "More slots donated for interleaving than can possibly fix in allocated texture memory!");
			slots = Min(slots,maxSlots);
			AddFreeSlots(isDXT1, (char*)pMipAddr + (uMipSize - (slots * pageSize)) + (pageSize>>1), slots, overflowingSlots, pFilename, hasDxt5Tail);
		}
	}
	else 
		Debugf2(g_rscDebug,"Reused MipAddr = 0x%08x",pMipAddr);

	Debugf2(g_rscDebug,"Allocating %uk+%uk (%uk+%uk actual) for '%s' (%d by %d, %d mips, size %uk) (dontates %d%s slots)",(uBaseSize>>10),(uMipSize>>10),(pBaseAddr)?0:(uBaseSize>>10),(pMipAddr || contiguous)?0:(uMipSize>>10), pFilename, width, height, levels, (pImage->GetSize() * scale) >> 14,slots,isDXT?(isDXT1?" DXT1":" DXT5"):"");

	if (isDXT && forceContiguousMips )
	{
		u32 actualOffset = (u32)pMipAddr - (u32)pBaseAddr;
		if (actualOffset != uBaseSize)
		{
			Errorf("Error! forceContiguousMips is set to true but the base and mip offsets aren't linear on texture %s", pFilename);
		}
	}
	texture->Format.BaseAddress = ((DWORD)pBaseAddr) >> 12;
	texture->Format.MipAddress = ((DWORD)pMipAddr) >> 12;
	m_Texture = texture;


	sysMemStartTemp();
	u8*	pTemp = rage_new u8[uBaseSize + uMipSize];
	grcImage	*pLayer = pImage;
	int			layer = 0;
	bool		bIsPacked = (XGIsPackedTexture(texture) == TRUE);
	while ( pLayer ) {
		grcImage	*pMip = pLayer;
		XGTEXTURE_DESC desc;
		XGGetTextureDesc(texture, 0, &desc);
		for	(int i = 0; i < pLayer->GetExtraMipCount() + 1; i++)
		{
			rect.pBits = (void*)(((i&&texture->Format.MipAddress?texture->Format.MipAddress:texture->Format.BaseAddress)<<12)
				+ XGGetMipLevelOffset(texture,layer,i));
			// Displayf("%u",XGGetMipLevelOffset(texture,0,i));

			XGTEXTURE_DESC mipDesc;
			XGGetTextureDesc(texture, i, &mipDesc);

			if( nFormat & D3DFORMAT_TILED_MASK )
			{
				int srcStride = pMip->GetStride() * (desc.Width / desc.WidthInBlocks);

				switch (grcImage::GetFormatByteSwapSize(pMip->GetFormat()))
				{
				case 1: break;
				case 2: XGEndianSwapMemory(pMip->GetBits(), pMip->GetBits(), XGENDIAN_8IN16, 2, pMip->GetSize()/2); break;
				case 4: XGEndianSwapMemory(pMip->GetBits(), pMip->GetBits(), XGENDIAN_8IN32, 4, pMip->GetSize()/4); break;
				}

				if (imageType == grcImage::VOLUME) {
					XGPOINT3D destPoint = {0,0,0};
					D3DBOX srcBox = {0,0,desc.Width>>i,desc.Height>>i,0,desc.Depth>>i};

					XGTileVolumeTextureLevel(desc.Width,desc.Height,desc.Depth,i,XGGetGpuFormat(UnExpandGPUFormat(desc.Format)),
						bIsPacked? 0 : XGTILE_NONPACKED, box.pBits, &destPoint, pMip->GetBits(),
						srcStride, box.SlicePitch, &srcBox);
				}
				else {
					// Assert(!(desc.Flags & XGTDESC_PACKED));
					// copy to linear 360 device format 
					XGCopySurface(	pTemp, 
									mipDesc.RowPitch, 
									mipDesc.Width, 
									mipDesc.Height, 
									(D3DFORMAT) MAKELINFMT(nFormat),
									NULL, 
									pMip->GetBits(), 
									srcStride, 
									(D3DFORMAT) MAKELINFMT(nFormat), 
									NULL, 
									XGCOMPRESS_NO_DITHERING, 
									0.0f );

					XGTileTextureLevel(desc.Width,desc.Height,i,XGGetGpuFormat(UnExpandGPUFormat(desc.Format)),
						bIsPacked? 0 : XGTILE_NONPACKED,rect.pBits,NULL,pTemp,mipDesc.RowPitch,NULL);
				}

				// MUST BE BYTE-SWAPPED BACK, because grcImage is cached during the first pass of asset conversion ..
				switch (grcImage::GetFormatByteSwapSize(pMip->GetFormat()))
				{
				case 1: break;
				case 2: XGEndianSwapMemory(pMip->GetBits(), pMip->GetBits(), XGENDIAN_8IN16, 2, pMip->GetSize()/2); break;
				case 4: XGEndianSwapMemory(pMip->GetBits(), pMip->GetBits(), XGENDIAN_8IN32, 4, pMip->GetSize()/4); break;
				}
			}

			pMip = pMip->GetNext();
		}

		pLayer = pLayer->GetNextLayer();
		layer++;
	}
	delete [] pTemp;
	sysMemEndTemp();

	m_Width = pImage->GetWidth();
	m_Height = pImage->GetHeight();
	m_MipCount = (u8)(pImage->GetExtraMipCount() + 1);
}


void grcTextureFactoryXenonProxy::CreatePagedTextureFactory() {
	SetInstance(*(rage_new grcTextureFactoryXenonProxy));
}


grcTextureXenonProxy::~grcTextureXenonProxy()
{
	if (m_Texture) {
		physical_delete((void*)(m_Texture->Format.BaseAddress << 12));
		if (m_Texture->Format.MipAddress)
			physical_delete((void*)(m_Texture->Format.MipAddress << 12));
		delete m_Texture;
	}
}

grcTextureXenonProxy::grcTextureXenonProxy(const char *pFilename,grcTextureFactory::TextureCreateParams *params)
{
	grcImage* pImage        = NULL;
	bool      bImageCreated = false;
#if __RESOURCECOMPILER
	void*     pProcessProxy = NULL;
#endif

#if __RESOURCECOMPILER
	sysMemStartTemp();
#endif

	if (strcmp(pFilename, "none") && strcmp(pFilename, "nonresident"))
	{
#if __RESOURCECOMPILER
		grcImage::ImageList images;
		bool result = grcImage::RunCustomLoadFunc( pFilename, images, &pProcessProxy );
		if ( result )
			pImage = images[1];

		if (!result || pImage == NULL) // try loading normally
#endif // __RESOURCECOMPILER
		{
			pImage = grcImage::Load(pFilename);
			bImageCreated = true;
		}
	}
#if __RESOURCECOMPILER
	sysMemEndTemp();
#endif // __RESOURCECOMPILER

	if (pImage)
	{
		const int w = pImage->GetWidth();
		const int h = pImage->GetHeight();

		if ((w|h) & 3)
		{
			Errorf("grcTextureXenonProxy - Texture '%s' - invalid resolution %d by %d", pFilename, w, h);

			if (bImageCreated)
			{
				sysMemStartTemp();
				pImage->Release();
				bImageCreated = false;
				sysMemEndTemp();
			}

			pImage = NULL;
#if __RESOURCECOMPILER
			pProcessProxy = NULL;
#endif
		}
	}

	if (!pImage)
	{
		const u32 texel = strcmp(pFilename, "nonresident") ? 0xFFFFFFFF : 0x40FFFFFF;
		const int dummySize = 4;

		sysMemStartTemp();
		pImage = grcImage::Create(dummySize, dummySize, 1, grcImage::A8R8G8B8, grcImage::STANDARD, 0, 0);
		bImageCreated = true;
		sysMemEndTemp();

		u32 *texels = (u32*) pImage->GetBits();

		for (int i = 0; i < dummySize*dummySize; i++)
		{
			texels[i] = texel;
		}
	}

	Init(pFilename, pImage, params);
#if __RESOURCECOMPILER
	RunCustomProcessFunc(pProcessProxy);
#endif

	if (bImageCreated)
	{
		sysMemStartTemp();
		pImage->Release();
		bImageCreated = false;
		sysMemEndTemp();
	}
}


grcTextureXenonProxy::grcTextureXenonProxy(grcImage *pImage,grcTextureFactory::TextureCreateParams *params) 
{
	const char* name = "image";
#if __BANK || __RESOURCECOMPILER
	name = grcTexture::GetCustomLoadName(name);
#endif // __BANK || __RESOURCECOMPILER
	Init(name,pImage,params);
}




grcTexture	*grcTextureFactoryXenonProxy::Create	(const char *pFilename,grcTextureFactory::TextureCreateParams *params)
{
	char	Buffer[MAX_PATH];

	RAGE_TRACK(Graphics);
	RAGE_TRACK(Texture);

	StringNormalize(Buffer, pFilename, sizeof(Buffer));

	grcTexture	*tex = LookupTextureReference(Buffer);
	if (tex)
		return tex;

	tex = rage_new grcTextureXenonProxy(Buffer,params);
	return(tex);
}



grcTexture	*grcTextureFactoryXenonProxy::Create	(grcImage *pImage,grcTextureFactory::TextureCreateParams *params)
{
	RAGE_TRACK(Graphics);
	RAGE_TRACK(Texture);

	return rage_new grcTextureXenonProxy(pImage,params);
}


inline void datSwapper(DWORD& v) { datSwapper(reinterpret_cast<u32&>(v)); }

void grcTextureXenonProxy::DeclareStruct(datTypeStruct &s) {
	grcTexture::DeclareStruct(s);
	STRUCT_BEGIN(grcTextureXenonProxy);
	
	datSwapper(m_Texture->Common);
	datSwapper(m_Texture->ReferenceCount);
	datSwapper(m_Texture->Fence);
	datSwapper(m_Texture->ReadFence);
	datSwapper(m_Texture->Identifier);
	datSwapper(m_Texture->BaseFlush);
	datSwapper(m_Texture->MipFlush);

	void *base = (void*)(m_Texture->Format.BaseAddress << 12);
	datRelocatePointer(base);
	m_Texture->Format.BaseAddress = (size_t)base >> 12;

	void *mip = (void*)(m_Texture->Format.MipAddress << 12);
	datRelocatePointer(mip);
	m_Texture->Format.MipAddress = (size_t)mip >> 12;

	for (int i=0; i<6; i++)
		datSwapper(m_Texture->Format.dword[i]);

	STRUCT_FIELD_VP(m_Texture);

	STRUCT_FIELD(m_Width);
	STRUCT_FIELD(m_Height);
	STRUCT_FIELD(m_MipCount);
	STRUCT_END();
}



#endif	// __RESOURCECOMPILER

}	// namespace rage
#endif	// !__64BIT
