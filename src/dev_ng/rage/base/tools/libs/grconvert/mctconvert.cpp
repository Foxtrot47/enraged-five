// 
// /mctconvert.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 
#include "mctconvert.h"

#if !__64BIT
#include "grcore/image.h"
#include "system/memory.h"

#ifdef DBG
#undef DBG
#endif
#define DBG 0
#define _X86_
#include "system/xtl.h"
#include "xdk.h"
#pragma warning(disable: 4668)
#pragma warning(disable: 4062)
#include "d3d9.h"
#pragma warning(error: 4062)
#pragma warning(error: 4668)
#include "xboxmath.h"
#include "xgraphics.h"

namespace rage {

u64 g_TotalDds, g_TotalMct;

#if _XDK_VER < 5632
#pragma warning(disable:4100)
#endif

bool MctConvert(const char *dds,const char *mct,int quality) {
	bool result = false;
	grcImage *pImage = grcImage::LoadDDS(dds);

	if (pImage && (pImage->GetFormat()==grcImage::DXT1 || pImage->GetFormat()==grcImage::DXT5) && pImage->GetType()==grcImage::STANDARD) {
		D3DFORMAT nFormat = pImage->GetFormat()==grcImage::DXT1? D3DFMT_DXT1 : D3DFMT_DXT5;

		D3DBaseTexture textureBuf;
		D3DBaseTexture *texture = &textureBuf;
		UINT uBaseSize, uMipSize;
		bool		bIsPacked = (XGIsPackedTexture(texture) == TRUE);
		XGSetTextureHeader(
			pImage->GetWidth(), pImage->GetHeight(), pImage->GetExtraMipCount() + 1, 0, 
			nFormat, D3DPOOL_MANAGED, 
			0, XGHEADER_CONTIGUOUS_MIP_OFFSET, 0, (D3DTexture *) texture, &uBaseSize, &uMipSize);

		Assert(uBaseSize);
		char *pBaseAddr = (char*) sysMemVirtualAllocate(uBaseSize+uMipSize);
		char *pMipAddr = uMipSize? (pBaseAddr + uBaseSize) : NULL;

		texture->Format.BaseAddress = ((DWORD)pBaseAddr) >> 12;
		texture->Format.MipAddress = ((DWORD)pMipAddr) >> 12;

		char *pTemp = new char[uBaseSize + uMipSize];
		grcImage	*pMip = pImage;
		XGTEXTURE_DESC desc;
		XGGetTextureDesc(texture, 0, &desc);
		D3DLOCKED_RECT	rect = { 0, 0 };
		for	(int i = 0; i < pImage->GetExtraMipCount() + 1; i++)
		{
			rect.pBits = (void*)(((i&&texture->Format.MipAddress?texture->Format.MipAddress:texture->Format.BaseAddress)<<12)
				+ XGGetMipLevelOffset(texture,0 /*layer*/,i));

			XGTEXTURE_DESC mipDesc;
			XGGetTextureDesc(texture, i, &mipDesc);

			if (nFormat & D3DFORMAT_TILED_MASK)
			{
				int		srcStride = pMip->GetStride() * (desc.Width / desc.WidthInBlocks);

				XGCopySurface(	pTemp, 
					mipDesc.RowPitch, 
					mipDesc.Width, 
					mipDesc.Height, 
					(D3DFORMAT) MAKELINFMT(nFormat),
					NULL, 
					pMip->GetBits(), 
					srcStride, 
					(D3DFORMAT) MAKELINFMT(nFormat), 
					NULL, 
					XGCOMPRESS_NO_DITHERING, 
					0.0f );

				XGTileTextureLevel(desc.Width,desc.Height,i,XGGetGpuFormat(desc.Format),
					bIsPacked? 0 : XGTILE_NONPACKED,rect.pBits,NULL,pTemp,mipDesc.RowPitch,NULL);
			}

			pMip = pMip->GetNext();
		}

#if _XDK_VER >= 5632
		UINT destBufferSize = pImage->GetSize() * 2;
		char *destBuffer = new char[destBufferSize];

		XGMCT_COMPRESSION_PARAMS mctParams = { (float) quality, 64, 0.0f };
		UINT mctContextSize = XGMCTGetCompressionContextSize(D3DRTYPE_TEXTURE,
			pImage->GetWidth(), pImage->GetHeight(),
			pImage->GetDepth(), pImage->GetExtraMipCount() + 1,
			nFormat,
			NULL, 0, &mctParams);
		char *mctContextBuffer = new char[mctContextSize];
		void *mctContext = XGMCTInitializeCompressionContext(mctContextBuffer, mctContextSize);

		XGMCTCompressTexture(mctContext,
			destBuffer, &destBufferSize,
			NULL, NULL,
			D3DFMT_UNKNOWN,
			texture,
			NULL, 0, &mctParams);

		MctHeader hdr = { 'MCT', pImage->GetWidth(), pImage->GetHeight(), pImage->GetExtraMipCount() + 1,
			nFormat, destBufferSize };
		fiStream *S = fiStream::Create(mct);
		if (S) {
			Displayf("%s (%u) -> %s (%u)",dds,uBaseSize+uMipSize,mct,destBufferSize);
			g_TotalDds += uBaseSize + uMipSize;
			g_TotalMct += destBufferSize;
			S->WriteInt(&hdr.m_Magic,sizeof(hdr)/sizeof(int));
			S->Write(destBuffer,destBufferSize);
			S->Close();
			result = true;
		}
		else
			Errorf("%s: Cannot create (quality %d).",mct,quality);

		delete[] destBuffer;
		delete[] mctContextBuffer;
		XGMCTDestroyCompressionContext(mctContext);
#endif

		delete[] pTemp;
		sysMemVirtualFree(pBaseAddr);
	}
	else
		Errorf("%s: Unsupported format.",dds);

	if (pImage)
		pImage->Release();

	return result;
}

}		// namespace rage
#endif	// __64BIT
