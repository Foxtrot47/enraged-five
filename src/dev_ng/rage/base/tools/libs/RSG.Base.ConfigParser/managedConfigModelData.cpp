//
// filename:	managedConfigModelData.cpp
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		7 January 2010
// description:	Managed C++ wrapper around configParser's ConfigModelData.
//

// --- Includes -----------------------------------------------------------------
#pragma unmanaged
#include "configParser/configParser.h"
#include "configParser/configCoreView.h"
#pragma managed

#include "managedConfigModelData.h"
#include "marshallingUtils.h"

#pragma managed

// --- Implementation -----------------------------------------------------------

namespace RSG
{
namespace Base
{
namespace ConfigParser
{

	ConfigModelData::ConfigModelData( )
		: m_pData( NULL )
	{
		m_pData = new configParser::ConfigModelData;
	}

	ConfigModelData::ConfigModelData( const configParser::ConfigModelData& native_data )
		: m_pData( NULL )
	{
		m_pData = new configParser::ConfigModelData( native_data );
	}

	ConfigModelData::~ConfigModelData( )
	{
		if ( m_pData )
			delete m_pData;
		m_pData = NULL;
	}

	ConfigModelData::DataType 
	ConfigModelData::GetDataType( )
	{
		switch ( m_pData->GetType() )
		{
		case configParser::ConfigModelData::CONFIGMODEL_DATA_BOOL:
			return ( DataType::Boolean );
		case configParser::ConfigModelData::CONFIGMODEL_DATA_STRING:
			return ( DataType::String );
		case configParser::ConfigModelData::CONFIGMODEL_DATA_INT:
			return ( DataType::Integer );
		case configParser::ConfigModelData::CONFIGMODEL_DATA_FLOAT:
			return ( DataType::Double );
		case configParser::ConfigModelData::CONFIGMODEL_DATA_EMPTY:
		default:
			return ( DataType::Empty );
			break;
		}
	}

	bool 
	ConfigModelData::IsEmpty( )
	{
		return ( DataType::Empty == this->GetDataType() );
	}

	System::String^ 
	ConfigModelData::ToString( )
	{
		wchar_t sdata[1024];
		m_pData->ToString( sdata, 1024 );
		return ( gcnew System::String( sdata ) );
	}

	// --- Value Getters ----------------------------------------------------

	void 
	ConfigModelData::GetValue( [System::Runtime::InteropServices::Out] System::String^ %s )
	{
		wchar_t sdata[1024];
		m_pData->GetValue( sdata, 1024 );
		s = gcnew System::String( sdata );
	}

	void 
	ConfigModelData::GetValue( [System::Runtime::InteropServices::Out] bool^ %b )
	{
		bool nb;
		m_pData->GetValue( nb );
		b = gcnew bool( nb );
	}

	void 
	ConfigModelData::GetValue( [System::Runtime::InteropServices::Out] int^ %n )
	{
		int nn;
		m_pData->GetValue( nn );
		n = gcnew int( nn );
	}

	void 
	ConfigModelData::GetValue( [System::Runtime::InteropServices::Out] double^ %f )
	{
		double nf;
		m_pData->GetValue( nf );
		f = gcnew double( nf );
	}

	// --- Child / Iterator Support -----------------------------------------

	bool 
	ConfigModelData::HasChildren( )
	{
		return ( m_pData->HasChildren( ) );
	}

	size_t
	ConfigModelData::ChildCount( )
	{
		return ( m_pData->ChildCount( ) );
	}

	ConfigModelData^ 
	ConfigModelData::operator[]( int index )
	{
		ConfigModelData^ mdata = gcnew ConfigModelData( m_pData[index] );
		return ( mdata );
	}

	System::Collections::IEnumerator^ 
	ConfigModelData::GetEnumerator( )
	{
		return ( gcnew enumerator( this ) );
	}

} // ConfigParser namespace
} // Base namespace
} // RSG namespace
