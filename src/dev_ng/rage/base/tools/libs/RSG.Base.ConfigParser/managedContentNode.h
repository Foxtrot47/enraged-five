#ifndef __MANAGEDCONTENTNODE_H__
#define __MANAGEDCONTENTNODE_H__

//
// File: managedContentNode.h
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: 
//

// --- Defines ------------------------------------------------------------------
#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma managed
#pragma once
#endif

// --- Includes -----------------------------------------------------------------

// configParser headers
#pragma unmanaged
#include "configParser/contentNode.h"
#pragma managed

#include "managedContentNodeFactory.h"

// --- Classes ------------------------------------------------------------------

namespace RSG
{
namespace Base
{
namespace ConfigParser
{

	ref class ContentModel;

	//PURPOSE
	//
	public enum class Platform
	{
		Independent,
		PS3,
		Xenon,
		Win32,
	};

	//PURPOSE
	// Base class for the managed content node classes.
	//
	public ref class ContentNode
	{
	public:
		ContentNode( configParser::ContentNode* pNode, ContentNode^ parent, ContentModel^ model );
		ContentNode( ContentNode^ node );
		virtual ~ContentNode( );

		//-------------------------------------------------------------------
		// Methods
		//-------------------------------------------------------------------

		// DHM WARNING:::!:!:!:!:!:!
		// SUCKS SUCKS SUCKS
		configParser::ContentNode* GetPtr( ) { return m_pNode; }
		
		int				InputIndexOf( ContentNode^ node );
		void			AddInput( ContentNode^ node );
		void			RemoveInput( ContentNode^ node );
		void			RemoveInputAt( int index );

		int				OutputIndexOf( ContentNode^ node );
		void			AddOutput( ContentNode^ node );
		void			RemoveOutput( ContentNode^ node );
		void			RemoveOutputAt( int index );


		//PURPOSE
		// Resolve all input nodes for this node; needs to be done after
		// the full Content tree has been loaded.
		virtual void	ResolveInputNodes( );

		//-------------------------------------------------------------------
		// Properties
		//-------------------------------------------------------------------
		
		[System::ComponentModel::Category("Core")]
		[System::ComponentModel::Description("Content node name.")]
		property System::String^ Name
		{
			System::String^ get()
			{
				return ( gcnew System::String( m_pNode->GetName( ) ) );
			}
		}

		[System::ComponentModel::Category("Core")]
		[System::ComponentModel::Description("Parent node.")]
		[System::ComponentModel::Browsable(false)]
		property ContentNode^ Parent
		{
			ContentNode^ get()
			{
				return ( m_Parent );
			}
		}

		[System::ComponentModel::Category("Core")]
		[System::ComponentModel::Description("Input dependency nodes.")]
		[System::ComponentModel::Browsable(false)]
		property array<ContentNode^>^ Inputs
		{
			array<ContentNode^>^ get()
			{
				return ( this->m_aInputs->ToArray() );
			}
		}

		[System::ComponentModel::Category("Core")]
		[System::ComponentModel::Description("Output nodes.")]
		[System::ComponentModel::Browsable(false)]
		property array<ContentNode^>^ Outputs
		{
			array<ContentNode^>^ get()
			{
				return ( this->m_aOutputs->ToArray() );
			}
		}

		[System::ComponentModel::Category("Source XML")]
		[System::ComponentModel::Description("XML source filename.")]
		property System::String^ XmlFilename
		{
			System::String^ get()
			{
				return ( gcnew System::String( m_pNode->GetSourceXmlFilename( ) ) );
			}
		}

		[System::ComponentModel::Category("Source XML")]
		[System::ComponentModel::Description("XML source file line number.")]
		property int XmlLine
		{
			int get() { return ( m_pNode->GetSourceXmlLine() ); }
		}

		[System::ComponentModel::Category("Source XML")]
		[System::ComponentModel::Description("XML source type identifier.")]
		property System::String^ XmlType
		{
			System::String^ get()
			{
				return ( gcnew System::String( m_pNode->GetType() ) );
			}
		}

	protected:
		void RaiseModelChangedEvent( );

		ContentNode^				m_Parent;	//!< Managed ContentNode parent.
		System::Collections::Generic::List<ContentNode^>^ m_aInputs; //!< Managed ContentNode^ inputs.
		System::Collections::Generic::List<ContentNode^>^ m_aOutputs; //!< Managed ContentNode^ outputs.
	
		configParser::ContentNode*	m_pNode; //!< Native content node object.
		ContentModel^				m_Model; //!< Managed ContentModel reference.
	};

	//PURPOSE
	// EventArg derived class for events that have an associated ContentNode
	// object.
	//
	public ref class ContentNodeEventArgs : public System::EventArgs
	{
	public:
		ContentNodeEventArgs( ContentNode^ node )
			: m_AssociatedContentNode( node )
		{
		}

		//-------------------------------------------------------------------
		// Properties
		//-------------------------------------------------------------------
		[System::ComponentModel::Browsable(false)]
		property ContentNode^ AssociatedContentNode
		{
			ContentNode^ get() { return m_AssociatedContentNode; }
		}

	protected:
		ContentNode^ m_AssociatedContentNode;
	};

} // ConfigParser namespace
} // Base namespace
} // RSG namespace

#endif // __MANAGEDCONTENTNODE_H__
