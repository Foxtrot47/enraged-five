#ifndef __CONFIGPARSER_CONTENTNODEFACTORY_H__
#define __CONFIGPARSER_CONTENTNODEFACTORY_H__

//
// filename:	managedContentNodeFactory.h
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		1 March 2010
// description:	Content-tree node factory class.
//

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

// --- Includes -----------------------------------------------------------------

// configParser headers
#include "configParser/contentNode.h"

// --- Classes ------------------------------------------------------------------

namespace RSG
{
namespace Base
{
namespace ConfigParser
{

// Forward declarations.
ref class ContentNode;
ref class ContentNodeGroup;
ref class ContentNodeMap;
ref class ContentNodeMapZip;
ref class ContentNodeMapProcessedZip;
ref class ContentModel;

//PURPOSE
// The ContentNodeFactory is responsible for creating all managed ContentNode
// objects.  It uses C++ RTTI to determine the actual native ContentNode type
// and constructs the corresponding managed ContentNode type as required.
//
public ref class ContentNodeFactory
{
public:
	ContentNodeFactory( ) { }
	~ContentNodeFactory( ) { }

	static ContentNode^			CreateNode( ContentModel^ model, ContentNode^ parent, configParser::ContentNode* pNode );
	
	// Group Node Construction
	static ContentNodeGroup^	CreateNodeGroup( ContentModel^ model, 
									ContentNode^ parent,
									System::String^ name,
									System::String^ path );

	// Map Node Construction
	static ContentNodeMap^		CreateNodeMap( ContentModel^ model, 
									ContentNode^ parent,
									System::String^ name, 
									System::String^ subtype, 
									System::String^ prefix, 
									bool defs, bool insts );

	static ContentNodeMapZip^	CreateNodeMapZip( ContentModel^ model, ContentNode^ parent,
									System::String^ name, 
									ContentNodeMap^ input );

	static ContentNodeMapProcessedZip^	CreateNodeMapProcessedZip( ContentModel^ model, ContentNode^ parent,
									System::String^ name, 
									ContentNodeMapZip^ input );
};

} // ConfigParser namespace
} // Base namespace
} // RSG namespace

#endif // __CONFIGPARSER_CONTENTNODEFACTORY_H__
