#ifndef __MANAGEDCONTENTNODECORE_H__
#define __MANAGEDCONTENTNODECORE_H__

//
// File: managedContentNodeCore.h
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: 
//

// --- Defines ------------------------------------------------------------------
#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma managed
#pragma once
#endif

// --- Includes -----------------------------------------------------------------

// configParser headers
#pragma unmanaged
#include "configParser/contentNodeCore.h"
#pragma managed

// Local headers
#include "managedContentNode.h"

// --- Classes ------------------------------------------------------------------

namespace RSG
{
namespace Base
{
namespace ConfigParser
{

	//PURPOSE
	// 
	//
	public ref class ContentNodeFile : public ContentNode
	{
	public:
		ContentNodeFile( configParser::ContentNodeFile* pNode, 
			ContentNode^ parent, ContentModel^ model )
			: ContentNode( pNode, parent, model )
		{
		}

		ContentNodeFile( ContentNodeFile^ file )
			: ContentNode( file )
		{
		}

		// Destructor.
		virtual ~ContentNodeFile( ) { }

		//-------------------------------------------------------------------
		// Properties
		//-------------------------------------------------------------------

		[System::ComponentModel::Category("File")]
		[System::ComponentModel::Description("File absolute directory.")]
		property System::String^ Path
		{
			System::String^ get()
			{
				configParser::ContentNodeFile* pFile = CastPtr( );
				wchar_t path[_MAX_PATH];
				pFile->GetRawAbsolutePath( path, _MAX_PATH );
				return ( gcnew System::String( path ) );
			}
		}

		[System::ComponentModel::Category("File")]
		[System::ComponentModel::Description("File raw relative directory.")]
		property System::String^ RawRelativePath
		{
			System::String^ get()
			{
				configParser::ContentNodeFile* pFile = CastPtr( );
				STL_STRING path = pFile->GetRawRelativePath( );
				return ( gcnew System::String( path.c_str() ) );
			}
		}

		[System::ComponentModel::Category("File")]
		[System::ComponentModel::Description("File relative directory.")]
		property System::String^ RelativePath
		{
			System::String^ get()
			{
				configParser::ContentNodeFile* pFile = CastPtr( );
				wchar_t path[_MAX_PATH];
				pFile->GetRelativePath( path, _MAX_PATH );
				return ( gcnew System::String( path ) );
			}
		}

		[System::ComponentModel::Category("File")]
		[System::ComponentModel::Description("File extension.")]
		property System::String^ Extension
		{
			System::String^ get()
			{
				configParser::ContentNodeFile* pFile = CastPtr( );
				return ( gcnew System::String( pFile->GetExtension( ) ) );
			}
			void set( System::String^ ext )
			{
				configParser::ContentNodeFile* pFile = CastPtr( );
				const TCHAR* s = (const TCHAR*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalUni(ext);
				pFile->SetExtension( s );
				System::Runtime::InteropServices::Marshal::FreeHGlobal( (System::IntPtr)(void*)s );
				RaiseModelChangedEvent();
			}
		}

		[System::ComponentModel::Category("File")]
		[System::ComponentModel::Description("File absolute filename.")]
		property System::String^ Filename
		{
			System::String^ get()
			{
				configParser::ContentNodeFile* pFile = CastPtr( );
				wchar_t filename[_MAX_PATH];
				pFile->GetFilename( filename, _MAX_PATH );
				return ( gcnew System::String( filename ) );
			}
		}

	private:
		configParser::ContentNodeFile* CastPtr( )
		{
			configParser::ContentNodeFile* pFile =
				dynamic_cast<configParser::ContentNodeFile*>( m_pNode );
			System::Diagnostics::Debug::Assert( NULL != pFile, "Invalid configParser::ContentNodeFile object." );
			return ( pFile );
		}
	};

	//PURPOSE
	//
	// 
	public ref class ContentNodeTarget : public ContentNodeFile
	{
	public:
		ContentNodeTarget( configParser::ContentNodeTarget* pNode, 
			ContentNode^ parent, ContentModel^ model )
			: ContentNodeFile( pNode, parent, model )
		{
		}

		ContentNodeTarget( ContentNodeTarget^ file )
			: ContentNodeFile( file )
		{
		}

		virtual ~ContentNodeTarget( )
		{
		}

	private:
		configParser::ContentNodeTarget* CastPtr( )
		{
			configParser::ContentNodeTarget* pFile =
				dynamic_cast<configParser::ContentNodeTarget*>( m_pNode );
			System::Diagnostics::Debug::Assert( NULL != pFile, "Invalid configParser::ContentNodeTarget object." );
			return ( pFile );
		}
	};

	//PURPOSE
	//
	//
	public ref class ContentNodeRpf : public ContentNodeTarget
	{
	public:
		ContentNodeRpf( configParser::ContentNodeRpf* pNode, 
			ContentNode^ parent, ContentModel^ model )
			: ContentNodeTarget( pNode, parent, model )
		{
		}


		ContentNodeRpf( ContentNodeRpf^ file )
			: ContentNodeTarget( file )
		{
		}
		virtual ~ContentNodeRpf( )
		{
		}
		
		//-------------------------------------------------------------------
		// Properties
		//-------------------------------------------------------------------
		
		[System::ComponentModel::Category("RPF")]
		[System::ComponentModel::Description("Stack paths during RPF construction.")]
		property bool StackPath
		{
			bool get() 
			{ 
				configParser::ContentNodeRpf* pRpf = CastPtr();
				return pRpf->GetStackPath(); 
			}			
			void set( bool stack ) 
			{ 
				configParser::ContentNodeRpf* pRpf = CastPtr();
				pRpf->SetStackPath( stack ); 
				RaiseModelChangedEvent();
			}
		}

		[System::ComponentModel::Category("RPF")]
		[System::ComponentModel::Description("RPF compression flag.")]
		property bool Compress
		{
			bool get() 
			{ 
				configParser::ContentNodeRpf* pRpf = CastPtr();
				return pRpf->GetCompress(); 
			}
			void set( bool compress ) 
			{ 
				configParser::ContentNodeRpf* pRpf = CastPtr();
				pRpf->SetCompress( compress ); 
				RaiseModelChangedEvent();
			}
		}

	private:
		configParser::ContentNodeRpf* CastPtr( )
		{
			configParser::ContentNodeRpf* pFile =
				dynamic_cast<configParser::ContentNodeRpf*>( m_pNode );
			System::Diagnostics::Debug::Assert( NULL != pFile, "Invalid configParser::ContentNodeRpf object." );
			return ( pFile );
		}
	};

		//PURPOSE
	//
	//
	public ref class ContentNodeZip : public ContentNodeTarget
	{
	public:
		ContentNodeZip( configParser::ContentNodeZip* pNode, 
			ContentNode^ parent, ContentModel^ model )
			: ContentNodeTarget( pNode, parent, model )
		{
		}


		ContentNodeZip( ContentNodeZip^ file )
			: ContentNodeTarget( file )
		{
		}
		virtual ~ContentNodeZip( )
		{
		}
		
		//-------------------------------------------------------------------
		// Properties
		//-------------------------------------------------------------------
		
		[System::ComponentModel::Category("ZIP")]
		[System::ComponentModel::Description("ZIP compression flag.")]
		property bool Compress
		{
			bool get() 
			{ 
				configParser::ContentNodeZip* pZip = CastPtr();
				return pZip->GetCompress(); 
			}
			void set( bool compress ) 
			{ 
				configParser::ContentNodeZip* pZip = CastPtr();
				pZip->SetCompress( compress ); 
				RaiseModelChangedEvent();
			}
		}

	private:
		configParser::ContentNodeZip* CastPtr( )
		{
			configParser::ContentNodeZip* pFile =
				dynamic_cast<configParser::ContentNodeZip*>( m_pNode );
			System::Diagnostics::Debug::Assert( NULL != pFile, "Invalid configParser::ContentNodeZip object." );
			return ( pFile );
		}
	};

} // ConfigParser namespace
} // Base namespace
} // RSG namespace

#endif // __MANAGEDCONTENTNODECORE_H__
