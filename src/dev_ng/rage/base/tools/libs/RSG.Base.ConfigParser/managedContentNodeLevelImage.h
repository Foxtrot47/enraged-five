#ifndef __MANAGEDCONTENTNODELEVELIMAGE_H__
#define __MANAGEDCONTENTNODELEVELIMAGE_H__

//
// File: managedContentNodeLevelImage.h
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: 
//

// --- Defines ------------------------------------------------------------------
#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma managed
#pragma once
#endif

// --- Includes -----------------------------------------------------------------

// configParser headers
#include "configParser/configParser.h"
#include "configParser/contentNodeLevelImage.h"

// --- Classes ------------------------------------------------------------------

namespace RSG
{
namespace Base
{
namespace ConfigParser
{

	//PURPOSE
	// Managed wrapper around the configParser's ContentNodeGroup class.
	//
	// For additional documentation see configParser::ContentNodeGroup
	// documentation.
	//
	public ref class ContentNodeLevelImage : public ContentNode
	{
	public:
		// Default constructor.
		ContentNodeLevelImage( configParser::ContentNodeLevelImage* pNode, 
			ContentNode^ parent, ContentModel^ model )
			: ContentNode( pNode, parent, model )
		{
		}

		// Copy constructor.
		ContentNodeLevelImage( ContentNodeLevelImage^ image )
			: ContentNode( image )
		{
		}

		// Destructor.
		virtual ~ContentNodeLevelImage( ) { }

		//-------------------------------------------------------------------
		// Properties
		//-------------------------------------------------------------------

		[System::ComponentModel::Category("Image")]
		[System::ComponentModel::Description("Level image file.")]
		property System::String^ Filename
		{
			System::String^ get()
			{
				configParser::ContentNodeLevelImage* pImage = CastPtr( );
				wchar_t path[_MAX_PATH];
				pImage->GetImageFilename( path, _MAX_PATH );
				return ( gcnew System::String( path ) );
			}
		}

		[System::ComponentModel::Category("Image")]
		[System::ComponentModel::Description("Raw level image file.")]
		property System::String^ RawFilename
		{
			System::String^ get()
			{
				configParser::ContentNodeLevelImage* pImage = CastPtr( );
				return ( gcnew System::String( pImage->GetRawImageFilename() ) );
			}
		}

		[System::ComponentModel::Category("Image")]
		[System::ComponentModel::Description("Level image minimum x world coordinate.")]
		property float MinX
		{
			float get()
			{
				configParser::ContentNodeLevelImage* pImage = CastPtr();
				return ( pImage->GetImageMinX() );
			}
		}

		[System::ComponentModel::Category("Image")]
		[System::ComponentModel::Description("Level image minimum y world coordinate.")]
		property float MinY
		{
			float get()
			{
				configParser::ContentNodeLevelImage* pImage = CastPtr();
				return ( pImage->GetImageMinY() );
			}
		}

		[System::ComponentModel::Category("Image")]
		[System::ComponentModel::Description("Level image maximum x world coordinate.")]
		property float MaxX
		{
			float get()
			{
				configParser::ContentNodeLevelImage* pImage = CastPtr();
				return ( pImage->GetImageMaxX() );
			}
		}

		[System::ComponentModel::Category("Image")]
		[System::ComponentModel::Description("Level image maximum y world coordinate.")]
		property float MaxY
		{
			float get()
			{
				configParser::ContentNodeLevelImage* pImage = CastPtr();
				return ( pImage->GetImageMaxY() );
			}
		}

	private:		
		configParser::ContentNodeLevelImage* CastPtr( )
		{
			System::Diagnostics::Debug::Assert( m_pNode != NULL, "Invalid native object." );
			configParser::ContentNodeLevelImage* pImage =
				dynamic_cast<configParser::ContentNodeLevelImage*>( m_pNode );
			System::Diagnostics::Debug::Assert( NULL != pImage, "Invalid configParser::ContentNodeLevelImage object." );
			return ( pImage );
		}
	};

} // ConfigParser namespace
} // Base namespace
} // RSG namespace

#endif // __MANAGEDCONTENTNODELEVEL_H__
