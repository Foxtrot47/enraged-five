#ifndef __MANAGEDCONTENTNODELEVEL_H__
#define __MANAGEDCONTENTNODELEVEL_H__

//
// File: managedContentNodeLevel.h
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: 
//

// --- Defines ------------------------------------------------------------------
#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma managed
#pragma once
#endif

// --- Includes -----------------------------------------------------------------

// configParser headers
#include "configParser/configParser.h"
#include "configParser/contentNodeLevel.h"

// Local headers
#include "managedContentNodeGroup.h"
#include "managedContentNodeFactory.h"

// --- Classes ------------------------------------------------------------------

namespace RSG
{
namespace Base
{
namespace ConfigParser
{

	//PURPOSE
	// Managed wrapper around the configParser's ContentNodeGroup class.
	//
	// For additional documentation see configParser::ContentNodeGroup
	// documentation.
	//
	public ref class ContentNodeLevel : public ContentNodeGroup
	{
	public:
		// Default constructor.
		ContentNodeLevel( configParser::ContentNodeLevel* pNode, 
			ContentNode^ parent, ContentModel^ model )
			: ContentNodeGroup( pNode, parent, model )
		{
		}

		// Copy constructor.
		ContentNodeLevel( ContentNodeLevel^ group )
			: ContentNodeGroup( group )
		{
		}

		// Destructor.
		virtual ~ContentNodeLevel( ) { }

		//-------------------------------------------------------------------
		// Properties
		//-------------------------------------------------------------------

		[System::ComponentModel::Category("Level")]
		[System::ComponentModel::Description("Level image file.")]
		property System::String^ ImageFilename
		{
			System::String^ get()
			{
				configParser::ContentNodeLevel* pGroup = CastPtr( );
				wchar_t path[_MAX_PATH];
				pGroup->GetImageFilename( path, _MAX_PATH );
				return ( gcnew System::String( path ) );
			}
		}

		[System::ComponentModel::Category("Level")]
		[System::ComponentModel::Description("Raw level image file.")]
		property System::String^ RawImageFilename
		{
			System::String^ get()
			{
				configParser::ContentNodeLevel* pGroup = CastPtr( );
				return ( gcnew System::String( pGroup->GetRawImageFilename() ) );
			}
		}

	private:		
		configParser::ContentNodeLevel* CastPtr( )
		{
			System::Diagnostics::Debug::Assert( m_pNode != NULL, "Invalid native object." );
			configParser::ContentNodeLevel* pGroup =
				dynamic_cast<configParser::ContentNodeLevel*>( m_pNode );
			System::Diagnostics::Debug::Assert( NULL != pGroup, "Invalid configParser::ContentNodeLevel object." );
			return ( pGroup );
		}
	};

} // ConfigParser namespace
} // Base namespace
} // RSG namespace

#endif // __MANAGEDCONTENTNODELEVEL_H__
