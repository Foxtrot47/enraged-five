#ifndef __MANAGEDCONFIGCOREVIEW_H__
#define __MANAGEDCONFIGCOREVIEW_H__

//
// File: managedConfigCoreView.h
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: 
//

// --- Defines ------------------------------------------------------------------
#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma managed
#pragma once
#endif

// --- Includes -----------------------------------------------------------------
#include "managedConfigModelData.h"

// --- Classes ------------------------------------------------------------------

namespace RSG
{
namespace Base
{
namespace ConfigParser
{

	//PURPOSE
	// Managed wrapper around configParser's configCoreView class.
	//
	// Other managed views should utilise this class but not inherit from it.
	//
	public ref class ConfigCoreView
	{
	public:
		enum class SettingsLocation
		{	
			INDETERMINATE,
			NETWORK,
			GLOBAL,
			PROJECT
		};

		ConfigCoreView( );
		ConfigCoreView( configParser::ConfigCoreView* pView );
		~ConfigCoreView( );

		//--- query Methods -----------------------------------------------------
		bool query( System::String^ expr, [System::Runtime::InteropServices::Out] ConfigModelData^ %data );
		bool query( System::String^ expr, [System::Runtime::InteropServices::Out] System::String^ %data );
		bool query( System::String^ expr, [System::Runtime::InteropServices::Out] bool %data );
		bool query( System::String^ expr, [System::Runtime::InteropServices::Out] int %data );
		bool query( System::String^ expr, [System::Runtime::InteropServices::Out] double %data );

		//--- set Methods -------------------------------------------------------
		bool set( System::String^ expr, System::String^ data );
		bool set( System::String^ expr, System::String^ data, bool flush );
		bool set( System::String^ expr, System::String^ data, bool flush, SettingsLocation loc );
		bool set( System::String^ expr, bool data );
		bool set( System::String^ expr, bool data, bool flush );
		bool set( System::String^ expr, bool data, bool flush, SettingsLocation loc );
		bool set( System::String^ expr, int data );
		bool set( System::String^ expr, int data, bool flush );
		bool set( System::String^ expr, int data, bool flush, SettingsLocation loc );
		bool set( System::String^ expr, double data );
		bool set( System::String^ expr, double data, bool flush );
		bool set( System::String^ expr, double data, bool flush, SettingsLocation loc );

		//--- Config Filename Methods -------------------------------------------
		System::String^ GetNetworkConfigFilename( );
		System::String^ GetGlobalConfigFilename( );
		System::String^ GetGlobalLocalConfigFilename( );
		System::String^ GetProjectConfigFilename( );
		System::String^ GetProjectLocalConfigFilename( );
		array<System::String^>^ GetLogMailAddresses();

		int GetDCCPackageCount( System::String^ package );
		System::String^ GetDCCPackageVersion( System::String^ package, int index );
		System::String^ GetDCCPackageInstallDir( System::String^ package, int index );
		System::String^ GetDCCPackagePlatform( System::String^ package, int index );

	protected:
		configParser::ConfigCoreView* m_pView;
		bool m_ownedPtr;
	};

} // ConfigParser namespace
} // Base namespace
} // RSG namespace

#endif // __MANAGEDCONFIGCOREVIEW_H__
