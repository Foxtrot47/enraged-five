#ifndef __MARSHALLINGUTILS_H__
#define __MARSHALLINGUTILS_H__

//
// File: MarshallingUtils.h
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Data Marshaling Utility functions
//

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma managed
#pragma once
#endif

// Platform SDK Header Files
#include <vcclr.h>

namespace RSG
{
namespace Base
{
namespace ConfigParser
{
	
/**
 * @brief Convert native wide-character array to character array
 */
inline const char* 
WideCharArrayToCharArray(System::String^ sString, const wchar_t* wsString)
{
	size_t convertedChars = 0;
	size_t sizeInBytes = ((sString->Length + 1) * 2);
	errno_t err = 0;
	char* ch = new char[sizeInBytes];

	err = wcstombs_s(&convertedChars, ch, sizeInBytes, wsString, sizeInBytes);
	if (0 != err)
		return NULL;

	return ch;
}

/**
* @brief Convert native character array to System::String^
*/
inline System::String^
CharArrayToSystemString(const char* sString)
{
	return gcnew System::String(sString);
}

} // ConfigParser namespace
} // Base namespace
} // RSG namespace

#endif // __MARSHALLINGUTILS_H__
