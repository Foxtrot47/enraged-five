//
// filename:	managedContentNodeGroup.cpp
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		1 March 2010
// description:	Managed C++ wrapper around configParser's ContentNodeGroup.
//

// --- Includes -----------------------------------------------------------------
#pragma unmanaged
#include "configParser/configParser.h"
#include "configParser/contentNodeGroup.h"
#pragma managed

#include "managedContentNodeGroup.h"
#include "marshallingUtils.h"

#pragma managed

// --- Implementation -----------------------------------------------------------

namespace RSG
{
namespace Base
{
namespace ConfigParser
{

	ContentNodeGroup::ContentNodeGroup( configParser::ContentNodeGroup* pNode, 
										ContentNode^ parent, ContentModel^ model )
		: ContentNode( pNode, parent, model )
	{
		this->m_aChildren = gcnew System::Collections::Generic::List<ContentNode^>( );

		for ( configParser::ContentNode::NodeContainerConstIter it = pNode->BeginChildren();
			  it != pNode->EndChildren( );
			  ++it )
		{
			this->m_aChildren->Add( ContentNodeFactory::CreateNode( m_Model, this, *it ) );
		}
	}
	
	ContentNodeGroup::ContentNodeGroup( ContentNodeGroup^ group )
		: ContentNode( group )
	{
		this->m_aChildren = gcnew System::Collections::Generic::List<ContentNode^>( );
		this->m_aChildren->AddRange(group->Children);
	}

	ContentNodeGroup::~ContentNodeGroup( )
	{
	}

	void
	ContentNodeGroup::ResolveInputNodes( )
	{
		for each ( ContentNode^ child in this->m_aChildren )
		{
			child->ResolveInputNodes( );
		}
		ContentNode::ResolveInputNodes( );
	}

	ContentNode^
	ContentNodeGroup::FindFirst( System::String^ name )
	{
		configParser::ContentNodeGroup* pGroup = CastPtr();
		System::String^ groupName = gcnew System::String( pGroup->GetName() );

		// Base-case, trying to find self?
		if (name->Equals( groupName, System::StringComparison::OrdinalIgnoreCase ) )
			return ( this );

		// Otherwise, iterate into our children.
		for each ( ContentNode^ child in Children )
		{
			if ( dynamic_cast<ContentNodeGroup^>( child ) )
			{
				ContentNodeGroup^ group = dynamic_cast<ContentNodeGroup^>( child );
				ContentNode^ node = group->FindFirst( name );
				if ( nullptr != node )
					return ( node );
			}

			if ( name->Equals( child->Name, System::StringComparison::OrdinalIgnoreCase ) )
				return ( child );
		}

		return ( nullptr );
	}
	
	ContentNode^
	ContentNodeGroup::FindFirst( System::String^ name, System::String^ type )
	{
		configParser::ContentNodeGroup* pGroup = CastPtr();
		System::String^ groupName = gcnew System::String( pGroup->GetName() );
		System::String^ typeName = gcnew System::String( pGroup->GetXmlType() );

		// Base-case, trying to find self?
		if (name->Equals( groupName, System::StringComparison::OrdinalIgnoreCase ) &&
			type->Equals( "" ) )
			return ( this );
		if (name->Equals( groupName, System::StringComparison::OrdinalIgnoreCase ) &&
			type->Equals( typeName ) )
			return ( this );

		// Otherwise, iterate into our children.
		for each ( ContentNode^ child in Children )
		{
			if ( dynamic_cast<ContentNodeGroup^>( child ) )
			{
				ContentNodeGroup^ group = dynamic_cast<ContentNodeGroup^>( child );
				ContentNode^ node = group->FindFirst( name, type );
				if ( nullptr != node )
					return ( node );
			}
			configParser::ContentNode* pNode = child->GetPtr();
			System::String^ childType = gcnew System::String( pNode->GetType() );

			if ( name->Equals( child->Name, System::StringComparison::OrdinalIgnoreCase ) &&
				 type->Equals( "" ) )
				return ( child );
			if ( name->Equals( child->Name, System::StringComparison::OrdinalIgnoreCase ) &&
				 type->Equals( childType ) )
				return ( child );
		}

		return ( nullptr );

	}
	
	System::Collections::Generic::List<ContentNode^>^
	ContentNodeGroup::FindAll( System::String^ type )
	{
		System::Collections::Generic::List<ContentNode^>^ list =
			gcnew System::Collections::Generic::List<ContentNode^>();

		for each ( ContentNode^ child in Children )
		{
			if ( dynamic_cast<ContentNodeGroup^>( child ) )
			{
				ContentNodeGroup^ group = dynamic_cast<ContentNodeGroup^>( child );
				list->AddRange( group->FindAll( type ) );
			}

			if (type->Equals( child->XmlType, System::StringComparison::OrdinalIgnoreCase ) )
				list->Add( child );
		}
		return ( list );
	}

	System::Collections::Generic::List<ContentNode^>^ 
	ContentNodeGroup::FindAll( System::String^ name, System::String^ type )
	{
		System::Collections::Generic::List<ContentNode^>^ list =
			gcnew System::Collections::Generic::List<ContentNode^>();

		for each ( ContentNode^ child in Children )
		{
			if ( dynamic_cast<ContentNodeGroup^>( child ) )
			{
				ContentNodeGroup^ group = dynamic_cast<ContentNodeGroup^>( child );
				list->AddRange( group->FindAll( name, type ) );
			}

			if (type->Equals( child->XmlType, System::StringComparison::OrdinalIgnoreCase ) &&
				name->Equals( child->Name, System::StringComparison::OrdinalIgnoreCase ) )
				list->Add( child );
		}
		return ( list );
	}

	int
	ContentNodeGroup::ChildIndexOf( ContentNode^ node )
	{
		for ( int n = 0; n < m_aChildren->Count; ++n )
		{
			if (node == m_aChildren[n])
				return ( n );
		}
		return ( -1 );
	}

	void
	ContentNodeGroup::AddChild( ContentNode^ node )
	{
		configParser::ContentNodeGroup* pGroup = CastPtr();
		pGroup->AddChild( node->GetPtr() );
		m_aChildren->Add( node );
		RaiseModelChangedEvent( );
	}
	
	void
	ContentNodeGroup::RemoveChild( ContentNode^ node )
	{
		RemoveChildAt( ChildIndexOf( node ) );
	}

	void
	ContentNodeGroup::RemoveChildAt( int index )
	{
		configParser::ContentNodeGroup* pGroup = CastPtr();
		pGroup->RemoveChild( pGroup->BeginChildren() + index );
		m_aChildren->RemoveAt( index );
		RaiseModelChangedEvent( );
	}

} // ConfigParser namespace
} // Base namespace
} // RSG namespace
