//
// filename:	managedConfigCoreView.cpp
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		5 January 2010
// description:	Managed C++ wrapper around configParser's configCoreView.
//

// --- Includes -----------------------------------------------------------------
#pragma unmanaged
#include "configParser/configParser.h"
#include "configParser/configCoreView.h"
#pragma managed

#pragma comment(lib, "ws2_32.lib")

#include "managedConfigCoreView.h"
#include "managedConfigModelData.h"
#include "marshallingUtils.h"

#pragma managed

// --- Implementation -----------------------------------------------------------

namespace RSG
{
namespace Base
{
namespace ConfigParser
{

	ConfigCoreView::ConfigCoreView( )
		: m_pView( NULL ), m_ownedPtr(true)
	{
		m_pView = new configParser::ConfigCoreView;
	}

	ConfigCoreView::ConfigCoreView( configParser::ConfigCoreView* pView )
		: m_pView( pView ), m_ownedPtr(false)
	{
	}

	ConfigCoreView::~ConfigCoreView( )
	{
		if(m_ownedPtr)
			delete m_pView;
	}

	bool 
	ConfigCoreView::query( System::String^ expr, [System::Runtime::InteropServices::Out] ConfigModelData^ %data )
	{
		pin_ptr<const wchar_t> wsExpr = PtrToStringChars( expr );

		configParser::ConfigModelData native_data;
		bool result = ( m_pView->query( wsExpr, native_data ) );

		data = gcnew ConfigModelData( native_data );
		return ( result );
	}

	bool 
	ConfigCoreView::query( System::String^ expr, [System::Runtime::InteropServices::Out] System::String^ %data )
	{
		pin_ptr<const wchar_t> wsExpr = PtrToStringChars( expr );
		wchar_t wsOutput[_MAX_PATH];
		memset( &wsOutput, 0, _MAX_PATH * sizeof( wchar_t ) );

		bool result = ( m_pView->query( wsExpr, wsOutput, _MAX_PATH ) );
		data = gcnew System::String( wsOutput );

		return ( result );
	}

	bool 
	ConfigCoreView::query( System::String^ expr, [System::Runtime::InteropServices::Out] bool %data )
	{
		pin_ptr<const wchar_t> wsExpr = PtrToStringChars( expr );
		bool output;

		bool result = ( m_pView->query( wsExpr, output ) );

		if(result)
			data = output;

		return ( result );
	}

	bool 
	ConfigCoreView::query( System::String^ expr, [System::Runtime::InteropServices::Out] int %data )
	{
		pin_ptr<const wchar_t> wsExpr = PtrToStringChars( expr );
		int output;

		bool result = ( m_pView->query( wsExpr, output ) );
		data = output;

		return ( result );
	}

	bool 
	ConfigCoreView::query( System::String^ expr, [System::Runtime::InteropServices::Out] double %data )
	{
		pin_ptr<const wchar_t> wsExpr = PtrToStringChars( expr );
		double output;

		bool result = ( m_pView->query( wsExpr, output ) );
		data = output;

		return ( result );
	}

	bool 
	ConfigCoreView::set( System::String^ expr, System::String^ data )
	{
		return ( set( expr, data, false, SettingsLocation::INDETERMINATE ) );
	}

	bool 
	ConfigCoreView::set( System::String^ expr, System::String^ data, bool flush )
	{
		return ( set( expr, data, flush, SettingsLocation::INDETERMINATE ) );
	}

	bool 
	ConfigCoreView::set( System::String^ expr, System::String^ data, bool flush, SettingsLocation loc )
	{
		pin_ptr<const wchar_t> wsExpr = PtrToStringChars( expr );
		pin_ptr<const wchar_t> wsData = PtrToStringChars( data );

		bool create = true;

		if(loc == SettingsLocation::INDETERMINATE)
			create = false;

		bool result = ( m_pView->set( wsExpr, wsData, flush, (configParser::ConfigCoreView::SettingsLocation)loc, create ) );
		return ( result );
	}

	bool 
	ConfigCoreView::set( System::String^ expr, bool data )
	{
		return ( set( expr, data, false, SettingsLocation::INDETERMINATE ) );
	}

	bool 
	ConfigCoreView::set( System::String^ expr, bool data, bool flush )
	{
		return ( set( expr, data, flush, SettingsLocation::INDETERMINATE ) );
	}

	bool 
	ConfigCoreView::set( System::String^ expr, bool data, bool flush, SettingsLocation loc )
	{
		pin_ptr<const wchar_t> wsExpr = PtrToStringChars( expr );

		bool create = true;

		if(loc == SettingsLocation::INDETERMINATE)
			create = false;

		bool result = ( m_pView->set( wsExpr, data, flush, (configParser::ConfigCoreView::SettingsLocation)loc, create ) );
		return ( result );
	}

	bool 
	ConfigCoreView::set( System::String^ expr, int data )
	{
		return ( set( expr, data, false, SettingsLocation::INDETERMINATE ) );
	}

	bool 
	ConfigCoreView::set( System::String^ expr, int data, bool flush )
	{
		return ( set( expr, data, flush, SettingsLocation::INDETERMINATE ) );
	}

	bool 
	ConfigCoreView::set( System::String^ expr, int data, bool flush, SettingsLocation loc )
	{
		pin_ptr<const wchar_t> wsExpr = PtrToStringChars( expr );

		bool create = true;

		if(loc == SettingsLocation::INDETERMINATE)
			create = false;

		bool result = (int)( m_pView->set( wsExpr, data, flush, (configParser::ConfigCoreView::SettingsLocation)loc, create ) );
		return ( result );
	}

	bool 
	ConfigCoreView::set( System::String^ expr, double data )
	{
		return ( set( expr, data, false, SettingsLocation::INDETERMINATE ) );
	}

	bool 
	ConfigCoreView::set( System::String^ expr, double data, bool flush )
	{
		return ( set( expr, data, flush, SettingsLocation::INDETERMINATE ) );
	}

	bool 
	ConfigCoreView::set( System::String^ expr, double data, bool flush, SettingsLocation loc )
	{
		pin_ptr<const wchar_t> wsExpr = PtrToStringChars( expr );

		bool create = true;

		if(loc == SettingsLocation::INDETERMINATE)
			create = false;

		bool result = (int)( m_pView->set( wsExpr, data, flush, (configParser::ConfigCoreView::SettingsLocation)loc, create ) );
		return ( result );
	}

	int 
	ConfigCoreView::GetDCCPackageCount( System::String^ package )
	{
		pin_ptr<const wchar_t> wsPackage = PtrToStringChars( package );

		int result = (int)( m_pView->GetDCCPackageCount( wsPackage ) );
		return ( result );
	}

	System::String^
	ConfigCoreView::GetDCCPackageVersion( System::String^ package, int index )
	{
		pin_ptr<const wchar_t> wsPackage = PtrToStringChars( package );
		pin_ptr<configParser::ConfigCoreView::DCCPackage> pDccPackage = 
			new configParser::ConfigCoreView::DCCPackage;
		m_pView->GetDCCPackageDetails( wsPackage, index, *pDccPackage );

		System::String^ version = gcnew System::String( pDccPackage->version );
		delete pDccPackage;
		return ( version );
	}

	System::String^
	ConfigCoreView::GetDCCPackageInstallDir( System::String^ package, int index )
	{
		pin_ptr<const wchar_t> wsPackage = PtrToStringChars( package );
		pin_ptr<configParser::ConfigCoreView::DCCPackage> pDccPackage = 
			new configParser::ConfigCoreView::DCCPackage;
		m_pView->GetDCCPackageDetails( wsPackage, index, *pDccPackage );
		
		System::String^ installdir = gcnew System::String( pDccPackage->installdir );
		delete pDccPackage;
		return ( installdir );
	}

	System::String^
	ConfigCoreView::GetDCCPackagePlatform( System::String^ package, int index )
	{
		pin_ptr<const wchar_t> wsPackage = PtrToStringChars( package );
		pin_ptr<configParser::ConfigCoreView::DCCPackage> pDccPackage = 
			new configParser::ConfigCoreView::DCCPackage;
		m_pView->GetDCCPackageDetails( wsPackage, index, *pDccPackage );

		System::String^ platform = gcnew System::String( pDccPackage->platform );
		delete pDccPackage;
		return ( platform );
	}

	System::String^ 
	ConfigCoreView::GetNetworkConfigFilename( )
	{
		wchar_t wsConfigFilename[_MAX_PATH];
		memset( &wsConfigFilename, _MAX_PATH, sizeof( wchar_t ) );
		m_pView->GetNetworkConfigFilename( wsConfigFilename, _MAX_PATH );
		return ( gcnew System::String( wsConfigFilename ) );
	}

	System::String^ 
	ConfigCoreView::GetGlobalConfigFilename( )
	{
		wchar_t wsConfigFilename[_MAX_PATH];
		memset( &wsConfigFilename, _MAX_PATH, sizeof( wchar_t ) );
		m_pView->GetGlobalConfigFilename( wsConfigFilename, _MAX_PATH );
		return ( gcnew System::String( wsConfigFilename ) );
	}

	System::String^ 
	ConfigCoreView::GetGlobalLocalConfigFilename( )
	{
		wchar_t wsConfigFilename[_MAX_PATH];
		memset( &wsConfigFilename, _MAX_PATH, sizeof( wchar_t ) );
		m_pView->GetGlobalLocalConfigFilename( wsConfigFilename, _MAX_PATH );
		return ( gcnew System::String( wsConfigFilename ) );
	}

	System::String^ 
	ConfigCoreView::GetProjectConfigFilename( )
	{
		wchar_t wsConfigFilename[_MAX_PATH];
		memset( &wsConfigFilename, _MAX_PATH, sizeof( wchar_t ) );
		m_pView->GetProjectConfigFilename( wsConfigFilename, _MAX_PATH );
		return ( gcnew System::String( wsConfigFilename ) );
	}

	System::String^ 
	ConfigCoreView::GetProjectLocalConfigFilename( )
	{
		wchar_t wsConfigFilename[_MAX_PATH];
		memset( &wsConfigFilename, _MAX_PATH, sizeof( wchar_t ) );
		m_pView->GetProjectLocalConfigFilename( wsConfigFilename, _MAX_PATH );
		return ( gcnew System::String( wsConfigFilename ) );
	}

	array<System::String^>^ 
	ConfigCoreView::GetLogMailAddresses()
	{
		std::vector<const TCHAR *> mails;
		m_pView->GetLogMailAddresses( mails );
		array<System::String^>^ strarray = gcnew array<System::String^>((int)mails.size());
		int index = 0;
		for (std::vector<const TCHAR *>::const_iterator mail = mails.begin();
			mail!=mails.end();
			mail++
			)
		{
			strarray[index++] = gcnew System::String( *mail );
			delete (*mail);
		}
		mails.clear();
		return strarray;
	}

} // ConfigParser namespace
} // Base namespace
} // RSG namespace
