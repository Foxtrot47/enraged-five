#ifndef __MANAGEDCONTENTNODEVEHICLES_H__
#define __MANAGEDCONTENTNODEVEHICLES_H__

//
// File: managedContentNodeMaps.h
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: 
//

// --- Defines ------------------------------------------------------------------
#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma managed
#pragma once
#endif

// --- Includes -----------------------------------------------------------------

// configParser headers
#pragma unmanaged
#include "configParser/ContentNodeVehicles.h"
#pragma managed

// Local headers
#include "managedContentNode.h"

// --- Classes ------------------------------------------------------------------

namespace RSG
{
namespace Base
{
namespace ConfigParser
{

	//PURPOSE
	//
	public ref class ContentNodeVehiclesRpf : public ContentNodeRpf
	{
	public:
		ContentNodeVehiclesRpf( configParser::ContentNodeVehiclesRpf* pNode, 
			ContentNode^ parent, ContentModel^ model )
			: ContentNodeRpf( pNode, parent, model )
		{
		}

		ContentNodeVehiclesRpf( ContentNodeVehiclesRpf^ vehicles )
			: ContentNodeRpf( vehicles )
		{
		}

		virtual ~ContentNodeVehiclesRpf( ) { }

		[System::ComponentModel::Category("Vehicles")]
		[System::ComponentModel::Description("Friendly name.")]
		[System::ComponentModel::Browsable(false)]
		property System::String^ FriendlyName
		{
			System::String^ get()
			{
				configParser::ContentNodeVehiclesRpf* pRpf = CastPtr();
				TCHAR name[_MAX_PATH] = {0};
				pRpf->GetFriendlyName( name, _MAX_PATH );
				return ( gcnew System::String( name ) );
			}
		}

	private:
		configParser::ContentNodeVehiclesRpf* CastPtr( )
		{
			configParser::ContentNodeVehiclesRpf* pMap =
				dynamic_cast<configParser::ContentNodeVehiclesRpf*>( m_pNode );
			System::Diagnostics::Debug::Assert( NULL != pMap, "Invalid configParser::ContentNodeVehiclesRpf object." );
			return ( pMap );
		}
	};

	// PURPOSE
	//
	public ref class ContentNodeVehiclesZip : public ContentNodeZip
	{
	public:
		ContentNodeVehiclesZip( configParser::ContentNodeVehiclesZip* pNode, 
			ContentNode^ parent, ContentModel^ model )
			: ContentNodeZip( pNode, parent, model )
		{
		}

		ContentNodeVehiclesZip( ContentNodeVehiclesZip^ vehicles )
			: ContentNodeZip( vehicles )
		{
		}

		virtual ~ContentNodeVehiclesZip( ) { }

		[System::ComponentModel::Category("Vehicles")]
		[System::ComponentModel::Description("Friendly name.")]
		[System::ComponentModel::Browsable(false)]
		property System::String^ FriendlyName
		{
			System::String^ get()
			{
				configParser::ContentNodeVehiclesZip* pRpf = CastPtr();
				TCHAR name[_MAX_PATH] = {0};
				pRpf->GetFriendlyName( name, _MAX_PATH );
				return ( gcnew System::String( name ) );
			}
		}

	private:
		configParser::ContentNodeVehiclesZip* CastPtr( )
		{
			configParser::ContentNodeVehiclesZip* pMap =
				dynamic_cast<configParser::ContentNodeVehiclesZip*>( m_pNode );
			System::Diagnostics::Debug::Assert( NULL != pMap, "Invalid configParser::ContentNodeVehiclesZip object." );
			return ( pMap );
		}
	};

} // ConfigParser namespace
} // Base namespace
} // RSG namespace

#endif // __MANAGEDCONTENTNODEVEHICLES_H__
