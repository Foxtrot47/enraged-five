using System;
using NUnit.Framework;
using RSG.Base.ConfigParser;

namespace RSG.Base.ConfigParser.UnitTests
{

    /// <summary>
    /// 
    /// </summary>
    [TestFixture]
    public class ConfigCoreViewUnitTests
    {
        #region Member Data
        ConfigCoreView m_View;
        #endregion // Member Data

        /// <summary>
        /// Unit tests initialisation method.
        /// </summary>
        [SetUp]
        public void setup()
        {
            m_View = new ConfigCoreView();
        }

        /// <summary>
        /// 
        /// </summary>
        [Test]
        public void query_test()
        {
            // Test single result
            String builddir = String.Empty;
            Assert.IsTrue(m_View.query("//project/branches/branch[@name='dev']/@build", out builddir ));
            Assert.AreEqual(builddir, "$(root)/build/$(branch)");
        
            // Testing child results
            // ...
   
            //
            ConfigModelData temp;
            Assert.IsTrue(m_View.query("//project/@name", out temp));

            // Ensure we can read global network, config and local data.
            int network_ver = 0;
            int config_ver = 0;
            int local_ver = 0;

            Assert.IsTrue(m_View.query("//network/@version", out network_ver));
            Assert.IsTrue(m_View.query("//config/@version", out config_ver));
            Assert.IsTrue(m_View.query("//config_local/@version", out local_ver));
            Assert.AreNotEqual(network_ver, 0);
            Assert.AreNotEqual(config_ver, 0);
            Assert.AreNotEqual(local_ver, 0);
            Assert.AreEqual(network_ver, config_ver);
            Assert.AreEqual(config_ver, local_ver);
        }

        /// <summary>
        /// 
        /// </summary>
        [Test]
        public void iterator_and_child_tests()
        {
            ConfigModelData temp;
            Assert.IsTrue(m_View.query("//project/branches/branch/@name", out temp));
            foreach (ConfigModelData m in temp)
                Assert.AreEqual(0, m.ChildCount());
        }

        /// <summary>
        /// 
        /// </summary>
        [Test]
        public void set_tests()
        {

        }
    }

} // RSG.Base.ConfigParser.UnitTests
