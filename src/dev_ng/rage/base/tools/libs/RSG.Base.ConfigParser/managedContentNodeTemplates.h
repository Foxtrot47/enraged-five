#ifndef __MANAGEDCONTENTNODETENPLATES_H__
#define __MANAGEDCONTENTNODETENPLATES_H__

//
// File: managedContentNodeTemplates.h
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: 
//

// --- Defines ------------------------------------------------------------------
#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma managed
#pragma once
#endif

// --- Includes -----------------------------------------------------------------

// configParser headers
#include "configParser/configParser.h"
#include "configParser/contentNodeTemplates.h"

// Local headers
#include "managedContentNode.h"
#include "managedContentNodeFactory.h"

// --- Classes ------------------------------------------------------------------

namespace RSG
{
namespace Base
{
namespace ConfigParser
{

	//PURPOSE
	// Managed wrapper around the configParser's ContentNodeTemplateFileEach class.
	//
	// For additional documentation see configParser::ContentNodeTemplateFileEach
	// documentation.
	//
	public ref class ContentNodeTemplateFileEach : public ContentNodeGroup
	{
	public:
		// Default constructor.
		ContentNodeTemplateFileEach( configParser::ContentNodeTemplateFileEach* pNode, 
			ContentNode^ parent, ContentModel^ model )
			: ContentNodeGroup( pNode, parent, model )
		{
		}

		// Copy constructor.
		ContentNodeTemplateFileEach( ContentNodeTemplateFileEach^ group )
			: ContentNodeGroup( group )
		{
		}

		// Destructor.
		virtual ~ContentNodeTemplateFileEach( )
		{
		}

		//-------------------------------------------------------------------
		// Methods
		//-------------------------------------------------------------------
		// None

		//-------------------------------------------------------------------
		// Properties
		//-------------------------------------------------------------------

		[System::ComponentModel::Category("Template")]
		[System::ComponentModel::Description("File search filter.")]
		property System::String^ SearchFilter
		{
			System::String^ get()
			{
				configParser::ContentNodeTemplateFileEach* pGroup = CastPtr( );
				STL_STRING filter = pGroup->GetSearchFilter( );
				return ( gcnew System::String( filter.c_str() ) );
			}
		}
	
	private:		
		configParser::ContentNodeTemplateFileEach* CastPtr( )
		{
			System::Diagnostics::Debug::Assert( m_pNode != NULL, "Invalid native object." );
			configParser::ContentNodeTemplateFileEach* pGroup =
				dynamic_cast<configParser::ContentNodeTemplateFileEach*>( m_pNode );
			System::Diagnostics::Debug::Assert( NULL != pGroup, "Invalid configParser::ContentNodeTemplateFileEach object." );
			return ( pGroup );
		}
	};

} // ConfigParser namespace
} // Base namespace
} // RSG namespace

#endif // __MANAGEDCONTENTNODETENPLATES_H__
