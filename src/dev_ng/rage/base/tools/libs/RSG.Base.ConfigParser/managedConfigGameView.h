#ifndef __MANAGEDCONFIGGAMEVIEW_H__
#define __MANAGEDCONFIGGAMEVIEW_H__

//
// File: managedConfigGameView.h
// Author: David Muir <david.muir@rockstarnorth.com>
// Date:: 21 January 2010
// Description: Managed C++ wrapper around configParser's ConfigGameView.
//

// --- Defines ------------------------------------------------------------------
#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma managed
#pragma once
#endif
// --- Includes -----------------------------------------------------------------
#include "managedConfigModelData.h"
#include "managedConfigCoreView.h"
#include "managedContentModel.h"

#pragma unmanaged
#include "configParser\configGameView.h"
#include "configParser\configCoreView.h"
#include "configParser\configUserType.h"
#pragma managed

#include <vcclr.h>

// --- Classes ------------------------------------------------------------------

namespace RSG
{
namespace Base
{
namespace ConfigParser
{

	public ref class ConfigPerforceInfo
	{
	public:
		property System::String^ Name
		{
			System::String^ get()
			{
				return m_name;
			}

			void set(System::String^ value)
			{
				m_name = value;
			}
		}

		property System::String^ Workspace
		{
			System::String^ get()
			{
				return m_workspace;
			}

			void set(System::String^ value)
			{
				m_workspace = value;
			}
		}

		property System::String^ Username
		{
			System::String^ get()
			{
				return m_username;
			}

			void set(System::String^ value)
			{
				m_username = value;
			}
		}

		property System::String^ Server
		{
			System::String^ get()
			{
				return m_server;
			}

			void set(System::String^ value)
			{
				m_server = value;
			}
		}
	private:
		System::String^ m_name;
		System::String^ m_workspace;
		System::String^ m_username;
		System::String^ m_server;
	};

	//PURPOSE
	// Managed wrapper around a specific branch target's information
	//
	public ref class ConfigTargetInfo
	{
	public:

		property System::String^ Name
		{
			System::String^ get()
			{
				return m_name;
			}

			void set(System::String^ value)
			{
				m_name = value;
			}
		}

		property System::String^ Directory
		{
			System::String^ get()
			{
				return m_directory;
			}

			void set(System::String^ value)
			{
				m_directory = value;
			}
		}

		property bool Enabled
		{
			bool get()
			{
				return m_enabled;
			}

			void set(bool value)
			{
				m_enabled = value;
			}
		}

	private:
		System::String^ m_name;
		System::String^ m_directory;
		bool m_enabled;
	};

	//PURPOSE
	// Managed wrapper around a specific branch target's information
	//
	public ref class ConfigRagebuilderInfo
	{
	public:

		property System::String^ Platform
		{
			System::String^ get()
			{
				return m_platform;
			}

			void set(System::String^ value)
			{
				m_platform = value;
			}
		}

		property System::String^ Executable
		{
			System::String^ get()
			{
				return m_executable;
			}

			void set(System::String^ value)
			{
				m_executable = value;
			}
		}

		property System::String^ Options
		{
			System::String^ get()
			{
				return m_options;
			}

			void set(System::String^ value)
			{
				m_options = value;
			}
		}


	private:
		System::String^ m_platform;
		System::String^ m_executable;
		System::String^ m_options;
	};

	//PURPOSE
	// Managed wrapper around a specific user type info
	//
	public ref class ConfigUserTypeInfo
	{
	public:
		property System::String^ Name
		{
			System::String^ get()
			{
				return m_name;
			}

			void set(System::String^ value)
			{
				m_name = value;
			}
		}

		property System::String^ FriendlyName
		{
			System::String^ get()
			{
				return m_friendlyName;
			}

			void set(System::String^ value)
			{
				m_friendlyName = value;
			}
		}

		property unsigned int Mask
		{
			unsigned int get()
			{
				return m_mask;
			}

			void set(unsigned int value)
			{
				m_mask = value;
			}
		}

		property bool IsPseudo
		{
			bool get()
			{
				return m_isPseudo;
			}

			void set(bool value)
			{
				m_isPseudo = value;
			}
		}

	private:
		System::String^ m_name;
		System::String^ m_friendlyName;
		unsigned int m_mask;
		bool m_isPseudo;
	};

	//PURPOSE
	// Managed wrapper around a bugstar level info object
	//
	public ref class BugstarLevelInfo
	{
	public:
		property System::String^ Name
		{
			System::String^ get()
			{
				return m_name;
			}

			void set(System::String^ value)
			{
				m_name = value;
			}
		}

		property unsigned int BugstarId
		{
			unsigned int get()
			{
				return m_bugstarId;
			}

			void set(unsigned int value)
			{
				m_bugstarId = value;
			}
		}

	private:
		System::String^ m_name;
		unsigned int m_bugstarId;
	};

	//PURPOSE
	// Managed wrapper around configParser's ConfigGameView class.
	//
	public ref class ConfigGameView
	{
	public:
		ConfigGameView( );
		~ConfigGameView( );

		ContentModel^ ReloadContentModel( );
		bool Save( );

		#pragma region Tools
		[System::ComponentModel::Category("Tools")]
		[System::ComponentModel::Description("Root tools directory.")]
		property System::String^ ToolsRootDir
		{
			System::String^ get()
			{	
				wchar_t output[_MAX_PATH];
				memset( &output, _MAX_PATH, sizeof( wchar_t ) );
				m_pView->GetToolsRootDir( output, _MAX_PATH );
				return ( System::IO::Path::GetFullPath( gcnew System::String( output ) ) );
			}
		}

		[System::ComponentModel::Category("Tools")]
		[System::ComponentModel::Description("Tools binary directory.")]
		property System::String^ ToolsBinDir
		{
			System::String^ get()
			{
				wchar_t output[_MAX_PATH];
				memset( &output, _MAX_PATH, sizeof( wchar_t ) );
				m_pView->GetToolsBinDir( output, _MAX_PATH );
				return ( System::IO::Path::GetFullPath( gcnew System::String( output ) ) );
			}
		}

		[System::ComponentModel::Category("Tools")]
		[System::ComponentModel::Description("Tools library directory.")]
		property System::String^ ToolsLibDir
		{
			System::String^ get()
			{
				wchar_t output[_MAX_PATH];
				memset( &output, _MAX_PATH, sizeof( wchar_t ) );
				m_pView->GetToolsLibDir( output, _MAX_PATH );
				return ( System::IO::Path::GetFullPath( gcnew System::String( output ) ) );
			}
		}

		[System::ComponentModel::Category("Tools")]
		[System::ComponentModel::Description("Tools configuration directory.")]
		property System::String^ ToolsConfigDir
		{
			System::String^ get()
			{
				wchar_t output[_MAX_PATH];
				memset( &output, _MAX_PATH, sizeof( wchar_t ) );
				m_pView->GetToolsConfigDir( output, _MAX_PATH );
				return ( System::IO::Path::GetFullPath( gcnew System::String( output ) ) );
			}
		}

		[System::ComponentModel::Category("Tools")]
		[System::ComponentModel::Description("Tools Ruby interpreter executable.")]
		property System::String^ ToolsRuby
		{
			System::String^ get()
			{
				wchar_t output[_MAX_PATH];
				memset( &output, _MAX_PATH, sizeof( wchar_t ) );
				m_pView->GetToolsRuby( output, _MAX_PATH );
				return ( System::IO::Path::GetFullPath( gcnew System::String( output ) ) );
			}
		}

		[System::ComponentModel::Category("Tools")]
		[System::ComponentModel::Description("Tools log directory.")]
		property System::String^ ToolsLogDir
		{
			System::String^ get()
			{
				wchar_t output[_MAX_PATH];
				memset( &output, _MAX_PATH, sizeof( wchar_t ) );
				m_pView->GetToolsLogDir( output, _MAX_PATH );
				return ( System::IO::Path::GetFullPath( gcnew System::String( output ) ) );
			}
		}

		[System::ComponentModel::Category("Tools")]
		[System::ComponentModel::Description("Tools temp directory.")]
		property System::String^ ToolsTempDir
		{
			System::String^ get()
			{
				wchar_t output[_MAX_PATH];
				memset( &output, _MAX_PATH, sizeof( wchar_t ) );
				m_pView->GetToolsTempDir( output, _MAX_PATH );
				return ( System::IO::Path::GetFullPath( gcnew System::String( output ) ) );
			}
		}

		[System::ComponentModel::Category("Tools")]
		[System::ComponentModel::Description("Tools cache directory.")]
		property System::String^ ToolsCacheDir
		{
			System::String^ get()
			{
				wchar_t output[_MAX_PATH];
				memset( &output, _MAX_PATH, sizeof( wchar_t ) );
				m_pView->GetCacheDir( output, _MAX_PATH );
				return ( System::IO::Path::GetFullPath( gcnew System::String( output ) ) );
			}
		}
		#pragma endregion

		#pragma region Bugstar
		[System::ComponentModel::Category("Bugstar")]
		[System::ComponentModel::Description("Project's bugstar ID.")]
		property unsigned int BugstarId
		{
			unsigned int get()
			{
				return m_pView->GetBugstarId();
			}
		}

		[System::ComponentModel::Category("Bugstar")]
		[System::ComponentModel::Description("Bugstar server address.")]
		property System::String^ BugstarServer
		{
			System::String^ get()
			{
				wchar_t output[_MAX_PATH];
				memset( &output, 0, _MAX_PATH * sizeof( wchar_t ) );
				m_pView->GetBugstarServer( output, _MAX_PATH );
				return ( gcnew System::String( output ) );
			}
		}

		[System::ComponentModel::Category("Bugstar")]
		[System::ComponentModel::Description("Bugstar server port.")]
		property unsigned int BugstarPort
		{
			unsigned int get()
			{
				return m_pView->GetBugstarPort();
			}
		}

		[System::ComponentModel::Category("Bugstar")]
		[System::ComponentModel::Description("Bugstar rest service.")]
		property System::String^ BugstarRestService
		{
			System::String^ get()
			{
				wchar_t output[_MAX_PATH];
				memset( &output, 0, _MAX_PATH * sizeof( wchar_t ) );
				m_pView->GetBugstarRestService( output, _MAX_PATH );
				return ( gcnew System::String( output ) );
			}
		}

		[System::ComponentModel::Category("Bugstar")]
		[System::ComponentModel::Description("Bugstar attachment service.")]
		property System::String^ BugstarAttachmentService
		{
			System::String^ get()
			{
				wchar_t output[_MAX_PATH];
				memset( &output, 0, _MAX_PATH * sizeof( wchar_t ) );
				m_pView->GetBugstarAttachmentService( output, _MAX_PATH );
				return ( gcnew System::String( output ) );
			}
		}

		[System::ComponentModel::Category("Bugstar")]
		[System::ComponentModel::Description("Bugstar user domain required for login.")]
		property System::String^ BugstarUserDomain
		{
			System::String^ get()
			{
				wchar_t output[_MAX_PATH];
				memset( &output, 0, _MAX_PATH * sizeof( wchar_t ) );
				m_pView->GetBugstarUserDomain( output, _MAX_PATH );
				return ( gcnew System::String( output ) );
			}
		}

		[System::ComponentModel::Category("Bugstar")]
		[System::ComponentModel::Description("Bugstar read-only username.")]
		property System::String^ BugstarReadOnlyUser
		{
			System::String^ get()
			{
				wchar_t output[_MAX_PATH];
				memset( &output, 0, _MAX_PATH * sizeof( wchar_t ) );
				m_pView->GetBugstarReadOnlyUser( output, _MAX_PATH );
				return ( gcnew System::String( output ) );
			}
		}

		[System::ComponentModel::Category("Bugstar")]
		[System::ComponentModel::Description("Bugstar read-only password.")]
		property System::String^ BugstarReadOnlyPassword
		{
			System::String^ get()
			{
				wchar_t output[_MAX_PATH];
				memset( &output, 0, _MAX_PATH * sizeof( wchar_t ) );
				m_pView->GetBugstarReadOnlyPassword( output, _MAX_PATH );
				return ( gcnew System::String( output ) );
			}
		}

		[System::ComponentModel::Category("Bugstar")]
		[System::ComponentModel::Description("Get all the bugstar level information")]
		property System::Collections::Generic::List<BugstarLevelInfo ^> ^ BugstarLevels			
		{
			System::Collections::Generic::List<BugstarLevelInfo ^> ^ get()
			{
				System::Collections::Generic::List<BugstarLevelInfo ^>^ levelInfo = gcnew System::Collections::Generic::List<BugstarLevelInfo ^>();
				size_t size = m_pView->GetNumBugstarLevels();
				wchar_t level[SM_SIZE];

				for(size_t i=0;i<size;i++)
				{
					BugstarLevelInfo^ info = gcnew BugstarLevelInfo();

					m_pView->GetBugstarLevelKey(i, level, SM_SIZE);

					unsigned int id;
					m_pView->GetBugstarLevelId(level, id);
					
					info->Name = gcnew System::String(level);
					info->BugstarId = id;

					levelInfo->Add(info);
				}

				return levelInfo;
			}
		}

		[System::ComponentModel::Category("Bugstar")]
		[System::ComponentModel::Description("Get the id for a level based on the bugstar level data")]
		unsigned int GetBugstarLevelId( System::String ^Level )
		{
			pin_ptr<const TCHAR> unmgStr = PtrToStringChars(Level);

			unsigned int id = 0;
			m_pView->GetBugstarLevelId(unmgStr, id);
			return id;
		}
		#pragma endregion
		
		#pragma region Statistics
		[System::ComponentModel::Category("Statistics")]
		[System::ComponentModel::Description("Statistics server enabled flag.")]
		property bool StatisticsEnabled
		{
			bool get()
			{
				return m_pView->IsStatisticsEnabled();
			}
		}

		[System::ComponentModel::Category("Statistics")]
		[System::ComponentModel::Description("Statistics server address.")]
		property System::String^ StatisticsServer
		{
			System::String^ get()
			{
				wchar_t output[_MAX_PATH];
				memset( &output, 0, _MAX_PATH * sizeof( wchar_t ) );
				m_pView->GetStatisticsServer( output, _MAX_PATH );
				return ( gcnew System::String( output ) );
			}
		}

		[System::ComponentModel::Category("Statistics")]
		[System::ComponentModel::Description("Statistics server port.")]
		property unsigned int StatisticsPort
		{
			unsigned int get()
			{
				return m_pView->GetStatisticsPort();
			}
		}

		[System::ComponentModel::Category("Statistics")]
		[System::ComponentModel::Description("Statistics server flag indicating whether we should use HTTPS.")]
		property bool StatisticsUseHttps
		{
			bool get()
			{
				return m_pView->IsStatisticsUsingHttps();
			}
		}

		[System::ComponentModel::Category("Statistics")]
		[System::ComponentModel::Description("Statistics rest service.")]
		property System::String^ StatisticsRestService
		{
			System::String^ get()
			{
				wchar_t output[_MAX_PATH];
				memset( &output, 0, _MAX_PATH * sizeof( wchar_t ) );
				m_pView->GetStatisticsRestService( output, _MAX_PATH );
				return ( gcnew System::String( output ) );
			}
		}
		#pragma endregion

		#pragma region User
		[System::ComponentModel::Category("User")]
		[System::ComponentModel::Description("User's login username")]
		property System::String^ UserName
		{
			System::String^ get()
			{
				wchar_t output[_MAX_PATH];
				memset( &output, 0, _MAX_PATH * sizeof( wchar_t ) );
				m_pView->GetUserName( output, _MAX_PATH );
				return ( gcnew System::String( output ) );
			}
			
			void set( System::String^ value )
			{
				pin_ptr<const wchar_t> wch = PtrToStringChars( value );
				m_pView->SetUserName( wch );
			}
		}

		[System::ComponentModel::Category("User")]
		[System::ComponentModel::Description("User's email address.")]
		property System::String^ UserEmailAddress
		{
			System::String^ get()
			{
				wchar_t output[_MAX_PATH];
				memset( &output, 0, _MAX_PATH * sizeof( wchar_t ) );
				m_pView->GetUserEmailAddress( output, _MAX_PATH );
				return ( gcnew System::String( output ) );
			}

			void set( System::String^ value )
			{
				pin_ptr<const TCHAR> wch = PtrToStringChars( value );
				m_pView->SetUserEmailAddress( wch );
			}
		}

		[System::ComponentModel::Category("User")]
		[System::ComponentModel::Description("User's flags.")]
		property System::UInt32 UserFlags
		{
			System::UInt32 get()
			{
				unsigned int flags = 0x00000000;
				if(m_pView->GetUserFlags( flags ))
					return ( flags );
				else
					return 0;
			}

			void set( System::UInt32 value )
			{
				m_pView->SetUserFlags( value );
			}
		}

		[System::ComponentModel::Category("User")]
		[System::ComponentModel::Description("Studio")]
		property System::String^ Studio
		{
			System::String^ get()
			{
				wchar_t output[_MAX_PATH];
				memset( &output, 0, _MAX_PATH * sizeof( wchar_t ) );
				m_pView->GetStudio( output, _MAX_PATH );
				return ( gcnew System::String( output ) );
			}
		}

		[System::ComponentModel::Category("User")]
		[System::ComponentModel::Description("User types")]
		property System::Collections::Generic::List<ConfigUserTypeInfo ^> ^ UserTypes			
		{
			System::Collections::Generic::List<ConfigUserTypeInfo ^> ^ get()
			{
				System::Collections::Generic::List<ConfigUserTypeInfo ^>^ userTypes = gcnew System::Collections::Generic::List<ConfigUserTypeInfo ^>();
				size_t size = m_pView->GetNumUserTypes();

				for(size_t i=0;i<size;i++)
				{
					ConfigUserTypeInfo^ info = gcnew ConfigUserTypeInfo();

					configParser::UserType userType;
					m_pView->GetUserType(i,userType);

					info->Name = gcnew System::String(userType.GetName());
					info->FriendlyName = gcnew System::String(userType.GetFriendlyName());
					info->Mask = userType.GetMask();
					info->IsPseudo = userType.GetIsPseudo();

					userTypes->Add(info);
				}

				return userTypes;
			}
		}

		[System::ComponentModel::Category("User")]
		[System::ComponentModel::Description("User pseudo mask")]
		property System::UInt32 ^ UserPseudoMask
		{
			System::UInt32 ^ get()
			{
				return m_pView->GetUserPseudoTypeMask();
			}
		}
		#pragma endregion

		#pragma region Perforce
		[System::ComponentModel::Category("Perforce")]
		[System::ComponentModel::Description("Perforce integration support enabled.")]
		property bool PerforceIntegration
		{
			bool get()
			{
				return ( m_pView->GetPerforceIntegration() );
			}

			void set( bool enable )
			{
				m_pView->SetPerforceIntegration( enable );
			}
		}

		[System::ComponentModel::Category("Perforce")]
		[System::ComponentModel::Description("Perforce info")]
		property System::Collections::Generic::List<ConfigPerforceInfo ^> ^ Perforces			
		{
			System::Collections::Generic::List<ConfigPerforceInfo ^> ^ get()
			{
				System::Collections::Generic::List<ConfigPerforceInfo ^>^ perforces = gcnew System::Collections::Generic::List<ConfigPerforceInfo ^>();
				size_t size = m_pView->GetNumPerforces();

				for(size_t i=0;i<size;i++)
				{
					ConfigPerforceInfo^ info = gcnew ConfigPerforceInfo();

					configParser::PerforceInfo perforceInfo;
					m_pView->GetPerforceInfo(i,perforceInfo);

					info->Name = gcnew System::String(perforceInfo.GetName());
					info->Workspace = gcnew System::String(perforceInfo.GetWorkspace());
					info->Username = gcnew System::String(perforceInfo.GetUsername());
					info->Server = gcnew System::String(perforceInfo.GetServer());

					perforces->Add(info);
				}

				return perforces;
			}

			void set( System::Collections::Generic::List<ConfigPerforceInfo ^> ^ perforces )
			{
				size_t size = perforces->Count;
				m_pView->SetNumPerforces(size);

				for(size_t i = 0; i < size ; i++)
				{
					ConfigPerforceInfo ^ info = perforces[(int)i];
					configParser::PerforceInfo perforceInfo;

					pin_ptr<const TCHAR> pf_name = PtrToStringChars( info->Name );
					pin_ptr<const TCHAR> pf_workspace = PtrToStringChars( info->Workspace );
					pin_ptr<const TCHAR> pf_username = PtrToStringChars( info->Username );
					pin_ptr<const TCHAR> pf_server = PtrToStringChars( info->Server );
					
					perforceInfo.SetName( pf_name );
					perforceInfo.SetWorkspace( pf_workspace );
					perforceInfo.SetUsername( pf_username );
					perforceInfo.SetServer( pf_server );

					m_pView->SetPerforceInfo(i, perforceInfo);
				}
			}
		}

		[System::ComponentModel::Category("Perforce")]
		[System::ComponentModel::Description("Perforce labels")]
		property System::Collections::Generic::Dictionary<System::String ^, System::String ^>^ PerforceLabels
		{
			System::Collections::Generic::Dictionary<System::String ^, System::String ^>^ get()
			{
				System::Collections::Generic::Dictionary<System::String ^, System::String ^>^ labels = 
					gcnew System::Collections::Generic::Dictionary<System::String ^, System::String ^>();
				size_t size = m_pView->GetNumLabels();

				for ( size_t i = 0; i < size; i++ )
				{
					wchar_t wsName[_MAX_PATH], wsType[_MAX_PATH];
					memset( &wsName, 0, _MAX_PATH * sizeof( wchar_t ) );
					memset( &wsType, 0, _MAX_PATH * sizeof( wchar_t ) );
					m_pView->GetLabelNameAt( i, wsName, _MAX_PATH );
					m_pView->GetLabelTypeAt( i, wsType, _MAX_PATH );

					System::String^ name = gcnew System::String( wsName );
					System::String^ type = gcnew System::String( wsType );

					labels->Add( type, name );
				}

				return labels;
			}
		}
		#pragma endregion

		#pragma region Xoreax XGE
		[System::ComponentModel::Category("Applications: Xoreax XGE")]
		[System::ComponentModel::Description("XGE enabled.")]
		property bool UseXoreaxXGE
		{
			bool get()
			{
				return ( m_pView->GetUseXoreaxXGE( ) );
			}

			void set( bool value )
			{
				m_pView->SetUseXoreaxXGE( value );
			}
		}
		#pragma endregion

		#pragma region Application : DCC
		[System::ComponentModel::Category("Applications: DCC")]
		[System::ComponentModel::Description("3dsmax variants")]
		property System::Collections::Generic::List<System::String ^> ^MaxVariants
		{
			System::Collections::Generic::List<System::String ^> ^ get()
			{
				System::Collections::Generic::List<System::String ^>^ maxvariants = gcnew System::Collections::Generic::List<System::String ^>();
				size_t size = m_pView->GetNumMaxVariants();
				wchar_t output[SM_SIZE];

				for(size_t i=0;i<size;i++)
				{
					m_pView->GetMaxVariantName(i,output,SM_SIZE);
					maxvariants->Add(gcnew System::String( output ) );
				}

				return maxvariants;
			}
		}

		[System::ComponentModel::Category("Applications: DCC")]
		[System::ComponentModel::Description("get/set current 3dsmax variant")]
		property unsigned int MaxVariant
		{
			unsigned int get()
			{
				return m_pView->GetMaxVariant();
			}

			void set( unsigned int value )
			{
				m_pView->SetMaxVariant(value);
			}
		}
		#pragma endregion

		#pragma region Logging
		[System::ComponentModel::Category("Logging")]
		[System::ComponentModel::Description("Log to stdout.")]
		property bool LogToStdOut
		{
			bool get()
			{
				return ( m_pView->GetLogToStdOut( ) );
			}

			void set( bool value )
			{
				m_pView->SetLogToStdOut( value );
			}
		}

		[System::ComponentModel::Category("Logging")]
		[System::ComponentModel::Description("Log level.")]
		property int LogLevel
		{
			int get()
			{
				return ( m_pView->GetLogLevel( ) );
			}

			void set( int value )
			{
				m_pView->SetLogLevel( value );
			}
		}

		[System::ComponentModel::Category("Logging")]
		[System::ComponentModel::Description("Log tracing information.")]
		property bool LogTrace
		{
			bool get()
			{
				return ( m_pView->GetLogTraceInfo( ) );
			}

			void set( bool value )
			{
				m_pView->SetLogTraceInfo( value );
			}
		}

		[System::ComponentModel::Category("Logging")]
		[System::ComponentModel::Description("Mail logging errors.")]
		property bool LogMailErrors
		{
			bool get()
			{
				return ( m_pView->GetLogMailErrors( ) );
			}

			void set( bool value )
			{
				m_pView->SetLogMailErrors( value );
			}
		}

		[System::ComponentModel::CategoryAttribute("Logging")]
		[System::ComponentModel::DescriptionAttribute("Mail logging email server hostname.")]
		property System::String^ LogMailServerHost
		{
			System::String^ get()
			{
				wchar_t output[SM_SIZE] = {0};
				m_pView->GetLogMailServerHost( output, SM_SIZE );
				return gcnew System::String( output );
			}
		}

		[System::ComponentModel::CategoryAttribute("Logging")]
		[System::ComponentModel::DescriptionAttribute("Mail logging email server port.")]
		property int LogMailServerPort
		{
			int get()
			{
				return ( m_pView->GetLogMailServerPort() );
			}
		}
		#pragma endregion

		#pragma region Branch
		[System::ComponentModel::Category("Branch")]
		[System::ComponentModel::Description("get all the branch information")]
		property System::Collections::Generic::List<System::String ^> ^Branches
		{
			System::Collections::Generic::List<System::String ^> ^ get()
			{
				System::Collections::Generic::List<System::String ^>^ branches = gcnew System::Collections::Generic::List<System::String ^>();
				size_t size = m_pView->GetNumBranches();
				wchar_t output[SM_SIZE];

				for(size_t i=0;i<size;i++)
				{
					m_pView->GetBranchKey(i,output,SM_SIZE);
					branches->Add(gcnew System::String( output ) );
				}

				return branches;
			}
		}

		[System::ComponentModel::Category("Branch")]
		[System::ComponentModel::Description("get the current branch")]
		property System::String ^Branch
		{
			System::String ^ get()
			{
				wchar_t output[SM_SIZE];
				m_pView->GetBranch(output,SM_SIZE);
				return gcnew System::String( output );
			}

			void set(System::String ^ value)
			{
				pin_ptr<const wchar_t> wch = PtrToStringChars( value );
				m_pView->SetBranch( wch );
			}
		}
		#pragma endregion
		
		#pragma region Targets
		[System::ComponentModel::Category("Target")]
		[System::ComponentModel::Description("get all the targets")]
		System::Collections::Generic::List<ConfigTargetInfo ^> ^Targets( System::String ^Branch )
		{
			System::Collections::Generic::List<ConfigTargetInfo ^>^ targets = gcnew System::Collections::Generic::List<ConfigTargetInfo ^>();
			pin_ptr<const TCHAR> unmgStr = PtrToStringChars(Branch);
			size_t size = m_pView->GetNumTargets((const TCHAR*)unmgStr);
			wchar_t branchName[SM_SIZE];
			wchar_t directory[SM_SIZE];

			for(size_t i=0;i<size;i++)
			{
				ConfigTargetInfo^ targetInfo = gcnew ConfigTargetInfo();

				m_pView->GetTargetKey(i, branchName, SM_SIZE, unmgStr);
				m_pView->GetTargetDir(branchName, directory, SM_SIZE, unmgStr);

				targetInfo->Enabled = m_pView->GetTargetEnabled(branchName,unmgStr);
				targetInfo->Name= gcnew System::String( branchName );
				targetInfo->Directory = gcnew System::String( directory );

				targets->Add(targetInfo);
			}

			return targets;
		}

		[System::ComponentModel::Category("Target")]
		[System::ComponentModel::Description("set all the target information")]
		bool SetTargets( System::String ^Branch, System::Collections::Generic::List<ConfigTargetInfo ^> ^ targets )
		{
			pin_ptr<const TCHAR> unmgStr = PtrToStringChars(Branch);
			m_pView->SetNumTargets(targets->Count,unmgStr);

			for( int i = 0; i < targets->Count ; i++ )
			{
				pin_ptr<const TCHAR> unmgTarget = PtrToStringChars(targets[i]->Name);

				m_pView->SetTargetKey(i,unmgTarget,unmgStr);
				m_pView->SetTargetEnabled(i,targets[i]->Enabled,unmgStr);
			}

			return true;
		}
		#pragma endregion

		#pragma region Rage
		[System::ComponentModel::Category("RagebuilderTarget")]
		[System::ComponentModel::Description("Get all Ragebuilder target information")]
		System::Collections::Generic::List<ConfigRagebuilderInfo ^> ^RagebuilderTargets( System::String ^Branch )
		{
			System::Collections::Generic::List<ConfigRagebuilderInfo ^>^ ragebuilderTargets = gcnew System::Collections::Generic::List<ConfigRagebuilderInfo ^>();
			pin_ptr<const TCHAR> branchName = PtrToStringChars(Branch);
			size_t size = m_pView->GetNumTargets((const TCHAR*)branchName);
			wchar_t platformName[SM_SIZE];
			wchar_t ragebuilderDirectory[SM_SIZE];
			wchar_t ragebuilderOptions[SM_SIZE];

			for(size_t i=0;i<size;i++)
			{
				ConfigRagebuilderInfo^ ragebuilderInfo = gcnew ConfigRagebuilderInfo();

				m_pView->GetTargetKey(i, platformName, SM_SIZE, branchName);

				m_pView->GetTargetRagebuilder(branchName, platformName, ragebuilderDirectory, SM_SIZE);
				m_pView->GetTargetRagebuilderOptions(branchName, platformName, ragebuilderOptions, SM_SIZE);

				ragebuilderInfo->Platform = gcnew System::String( platformName );
				ragebuilderInfo->Executable = gcnew System::String( ragebuilderDirectory );
				ragebuilderInfo->Options = gcnew System::String( ragebuilderOptions );

				ragebuilderTargets->Add(ragebuilderInfo);
			}

			return ragebuilderTargets;
		}
		#pragma endregion

		#pragma region Environment
		[System::ComponentModel::Category("Environment")]
		[System::ComponentModel::Description("Project name.")]
		property System::String^ ProjectName
		{
			System::String^ get()
			{
				wchar_t output[SM_SIZE];
				memset( &output, SM_SIZE, sizeof( wchar_t ) );
				m_pView->GetProjectName( output, SM_SIZE );
				return ( gcnew System::String( output ) );
			}
		}

		[System::ComponentModel::Category("Environment")]
		[System::ComponentModel::Description("Friendly project name.")]
		property System::String^ ProjectUIName
		{
			System::String^ get()
			{
				wchar_t output[SM_SIZE];
				memset( &output, SM_SIZE, sizeof( wchar_t ) );
				m_pView->GetProjectUIName( output, SM_SIZE );
				return ( gcnew System::String( output ) );
			}
		}

		[System::ComponentModel::Category("Environment")]
		[System::ComponentModel::Description("Project root directory.")]
		property System::String^ ProjectRootDir
		{
			System::String^ get()
			{
				wchar_t output[SM_SIZE];
				memset( &output, SM_SIZE, sizeof( wchar_t ) );
				m_pView->GetProjectRootDir( output, SM_SIZE );
				return ( System::IO::Path::GetFullPath( gcnew System::String( output ) ) );
			}
		}
		#pragma endregion

		#pragma region Project
		[System::ComponentModel::Category("Project")]
		[System::ComponentModel::Description("Does the project support levels?")]
		property bool HasLevels
		{
			bool get() { return ( m_pView->HasLevels() ); }
		}

		[System::ComponentModel::Category("Project")]
		[System::ComponentModel::Description("Is this project episodic?")]
		property bool IsEpisodic
		{
			bool get() { return ( m_pView->IsEpisodic() ); }
		}	

		[System::ComponentModel::Category("Project")]
		[System::ComponentModel::Description("Project network directory.")]
		property System::String^ NetworkDir
		{
			System::String^ get()
			{
				wchar_t output[_MAX_PATH];
				memset( &output, _MAX_PATH, sizeof( wchar_t ) );
				m_pView->GetNetworkDir( output, _MAX_PATH );
				return ( System::IO::Path::GetFullPath( gcnew System::String( output ) ) );
			}
		}

		[System::ComponentModel::Category("Project")]
		[System::ComponentModel::Description("Project network texture export directory.")]
		property System::String^ NetworkTextureDir
		{
			System::String^ get()
			{
				wchar_t output[_MAX_PATH];
				memset( &output, _MAX_PATH, sizeof( wchar_t ) );
				m_pView->GetNetworkTextureDir( output, _MAX_PATH );
				return ( System::IO::Path::GetFullPath( gcnew System::String( output ) ) );
			}
		}
	
		[System::ComponentModel::Category("Project")]
		[System::ComponentModel::Description("Project network stream export directory.")]
		property System::String^ NetworkStreamDir
		{
			System::String^ get()
			{
				wchar_t output[_MAX_PATH];
				memset( &output, _MAX_PATH, sizeof( wchar_t ) );
				m_pView->GetNetworkStreamDir( output, _MAX_PATH );
				return ( System::IO::Path::GetFullPath( gcnew System::String( output ) ) );
			}
		}

		[System::ComponentModel::Category("Project")]
		[System::ComponentModel::Description("Project network generic stream export directory.")]
		property System::String^ NetworkGenericStreamDir
		{
			System::String^ get()
			{
				wchar_t output[_MAX_PATH];
				memset( &output, _MAX_PATH, sizeof( wchar_t ) );
				m_pView->GetNetworkGenericStreamDir( output, _MAX_PATH );
				return ( System::IO::Path::GetFullPath( gcnew System::String( output ) ) );
			}
		}

		[System::ComponentModel::Category("Project")]
		[System::ComponentModel::Description("Project build directory.")]
		property System::String^ BuildDir
		{
			System::String^ get()
			{
				wchar_t output[_MAX_PATH];
				memset( &output, _MAX_PATH, sizeof( wchar_t ) );
				m_pView->GetBuildDir( output, _MAX_PATH );
				return ( System::IO::Path::GetFullPath( gcnew System::String( output ) ) );
			}
		}

		[System::ComponentModel::Category("Project")]
		[System::ComponentModel::Description("Project export directory.")]
		property System::String^ ExportDir
		{
			System::String^ get()
			{
				wchar_t output[_MAX_PATH];
				memset( &output, _MAX_PATH, sizeof( wchar_t ) );
				m_pView->GetExportDir( output, _MAX_PATH );
				return ( System::IO::Path::GetFullPath( gcnew System::String( output ) ) );
			}
		}

		[System::ComponentModel::Category("Project")]
		[System::ComponentModel::Description("Project processed directory.")]
		property System::String^ ProcessedDir
		{
			System::String^ get()
			{
				wchar_t output[_MAX_PATH];
				memset( &output, _MAX_PATH, sizeof( wchar_t ) );
				m_pView->GetProcessedDir( output, _MAX_PATH );
				return ( System::IO::Path::GetFullPath( gcnew System::String( output ) ) );
			}
		}

		[System::ComponentModel::Category("Project")]
		[System::ComponentModel::Description("Project metadata directory.")]
		property System::String^ MetadataDir
		{
			System::String^ get()
			{
				wchar_t output[_MAX_PATH];
				memset( &output, _MAX_PATH, sizeof( wchar_t ) );
				m_pView->GetMetadataDir( output, _MAX_PATH );
				return ( System::IO::Path::GetFullPath( gcnew System::String( output ) ) );
			}
		}

		[System::ComponentModel::Category("Project")]
		[System::ComponentModel::Description("Project common directory.")]
		property System::String^ CommonDir
		{
			System::String^ get()
			{
				wchar_t output[_MAX_PATH];
				memset( &output, _MAX_PATH, sizeof( wchar_t ) );
				m_pView->GetCommonDir( output, _MAX_PATH );
				return ( System::IO::Path::GetFullPath( gcnew System::String( output ) ) );
			}
		}

		[System::ComponentModel::Category("Project")]
		[System::ComponentModel::Description("Project shader directory.")]
		property System::String^ ShaderDir
		{
			System::String^ get()
			{
				wchar_t output[_MAX_PATH];
				memset( &output, _MAX_PATH, sizeof( wchar_t ) );
				m_pView->GetShaderDir( output, _MAX_PATH );
				return ( System::IO::Path::GetFullPath( gcnew System::String( output ) ) );
			}
		}

		[System::ComponentModel::Category("Project")]
		[System::ComponentModel::Description("Project code directory.")]
		property System::String^ SourceDir
		{
			System::String^ get()
			{
				wchar_t output[_MAX_PATH];
				memset( &output, _MAX_PATH, sizeof( wchar_t ) );
				m_pView->GetSourceDir( output, _MAX_PATH );
				return ( System::IO::Path::GetFullPath( gcnew System::String( output ) ) );
			}
		}

		[System::ComponentModel::Category("Project")]
		[System::ComponentModel::Description("Project rage code directory.")]
		property System::String^ RageSourceDir
		{
			System::String^ get()
			{
				wchar_t output[_MAX_PATH];
				memset( &output, _MAX_PATH, sizeof( wchar_t ) );
				m_pView->GetRageSourceDir( output, _MAX_PATH );
				return ( System::IO::Path::GetFullPath( gcnew System::String( output ) ) );
			}
		}

		[System::ComponentModel::Category("Project")]
		[System::ComponentModel::Description("Project script directory.")]
		property System::String^ ScriptDir
		{
			System::String^ get()
			{
				wchar_t output[_MAX_PATH];
				memset( &output, _MAX_PATH, sizeof( wchar_t ) );
				m_pView->GetScriptDir( output, _MAX_PATH );
				return ( System::IO::Path::GetFullPath( gcnew System::String( output ) ) );
			}
		}

		[System::ComponentModel::Category("Project")]
		[System::ComponentModel::Description("Project art directory.")]
		property System::String^ ArtDir
		{
			System::String^ get()
			{
				wchar_t output[_MAX_PATH];
				m_pView->GetArtDir( output, _MAX_PATH );
				return ( System::IO::Path::GetFullPath( gcnew System::String( output ) ) );
			}
		}

		[System::ComponentModel::Category("Project")]
		[System::ComponentModel::Description("Project assets directory.")]
		property System::String^ AssetsDir
		{
			System::String^ get()
			{
				wchar_t output[_MAX_PATH];
				m_pView->GetAssetsDir( output, _MAX_PATH );
				return ( System::IO::Path::GetFullPath( gcnew System::String( output ) ) );
			}
		}

		[System::ComponentModel::Category("Project")]
		[System::ComponentModel::Description("Project preview directory.")]
		property System::String^ PreviewDir
		{
			System::String^ get()
			{
				wchar_t output[_MAX_PATH];
				memset( &output, _MAX_PATH, sizeof( wchar_t ) );
				m_pView->GetPreviewDir( output, _MAX_PATH );
				return ( System::IO::Path::GetFullPath( gcnew System::String( output ) ) );
			}
		}

		[System::ComponentModel::Category("Project")]
		[System::ComponentModel::Description("Project audio directory.")]
		property System::String^ AudioDir
		{
			System::String^ get()
			{
				wchar_t output[_MAX_PATH];
				memset( &output, _MAX_PATH, sizeof( wchar_t ) );
				m_pView->GetAudioDir( output, _MAX_PATH );
				return ( System::IO::Path::GetFullPath( gcnew System::String( output ) ) );
			}
		}

		[System::ComponentModel::Category("Project")]
		[System::ComponentModel::Description("Project text directory.")]
		property System::String^ TextDir
		{
			System::String^ get()
			{
				wchar_t output[_MAX_PATH];
				memset( &output, _MAX_PATH, sizeof( wchar_t ) );
				m_pView->GetTextDir( output, _MAX_PATH );
				return ( System::IO::Path::GetFullPath( gcnew System::String( output ) ) );
			}
		}

		[System::ComponentModel::Category("Project")]
		[System::ComponentModel::Description("Project map texture directory name.")]
		property System::String^ MapTexDir
		{
			System::String^ get()
			{
				wchar_t output[_MAX_PATH];
				memset( &output, _MAX_PATH, sizeof( wchar_t ) );
				m_pView->GetMapTexDir( output, _MAX_PATH );
				return ( gcnew System::String( output ) );
			}
		}
		#pragma endregion

		#pragma region Content
		[System::ComponentModel::Category("Content")]
		[System::ComponentModel::Browsable(false)]
		property ContentModel^ Content
		{
			ContentModel^ get()
			{
				return (m_Model);
			}
		}

		[System::ComponentModel::Category("CoreView")]
		[System::ComponentModel::Browsable(false)]
		property RSG::Base::ConfigParser::ConfigCoreView^ ConfigCoreView
		{
			RSG::Base::ConfigParser::ConfigCoreView^ get()
			{
				return m_CoreView;
			}
		}
		#pragma endregion

		#pragma region Reports
		// Return dictionary of Report Category Name to dictionary of Report Name and URL.
		System::Collections::Generic::Dictionary<System::String^, System::Collections::Generic::Dictionary<System::String^, System::String^>^>^ GetReports( )
		{
			System::Collections::Generic::Dictionary<System::String^, System::Collections::Generic::Dictionary<System::String^, System::String^>^>^ reports = 
				gcnew System::Collections::Generic::Dictionary<System::String^, System::Collections::Generic::Dictionary<System::String^, System::String^>^>( );
			wchar_t categoryName[_MAX_PATH];
			wchar_t name[_MAX_PATH];
			wchar_t url[_MAX_PATH];
			memset( &name, _MAX_PATH, sizeof( wchar_t ) );
			memset( &url, _MAX_PATH, sizeof( wchar_t ) );
			
			for ( size_t c = 0; c < m_pView->GetNumReportCategories(); ++c )
			{
				if (m_pView->GetReportCategoryName( c, categoryName, _MAX_PATH ))
				{
					System::Collections::Generic::Dictionary<System::String^, System::String^>^ category = 
						gcnew System::Collections::Generic::Dictionary<System::String^, System::String^>();

					for ( size_t r = 0; r < m_pView->GetReportCategoryNumReports( c ); ++r )
					{
						if (m_pView->GetReportCategoryReportName( c, r, name, _MAX_PATH ) &&
							m_pView->GetReportCategoryReportUrl( c, r, url, _MAX_PATH ))
						{
							category->Add( gcnew System::String( name ), gcnew System::String( url ) );
						}
					}

					reports->Add( gcnew System::String( categoryName ), category );
				}
			}
			return ( reports );
		}
		#pragma endregion

	private:
		configParser::ConfigGameView*			m_pView; 
		ContentModel^							m_Model; // ContentModel object.
		RSG::Base::ConfigParser::ConfigCoreView^	m_CoreView; // ConfigCoreView object.
	};

} // ConfigParser namespace
} // Base namespace
} // RSG namespace

#endif // __MANAGEDCONFIGGAMEVIEW_H__
