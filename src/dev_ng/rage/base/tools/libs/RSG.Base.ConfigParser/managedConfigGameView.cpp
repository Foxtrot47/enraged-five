//
// filename:	managedConfigGameView.cpp
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		21 January 2010
// description:	Managed C++ wrapper around configParser's ConfigGameView.
//

// --- Includes -----------------------------------------------------------------
#pragma unmanaged
#include "configParser/configParser.h"
#include "configParser/configGameView.h"
#pragma managed

#include "managedConfigGameView.h"
#include "managedConfigModelData.h"
#include "marshallingUtils.h"

#pragma managed

// --- Implementation -----------------------------------------------------------

namespace RSG
{
namespace Base
{
namespace ConfigParser
{

ConfigGameView::ConfigGameView( )
	: m_pView( NULL )
{
	m_pView = new configParser::ConfigGameView;
	m_Model = gcnew ContentModel( m_pView->GetContentModel() );
	m_CoreView = gcnew RSG::Base::ConfigParser::ConfigCoreView( &(m_pView->GetConfigCoreView()) );
}

ConfigGameView::~ConfigGameView( )
{
	delete m_pView;
}

ContentModel^ 
ConfigGameView::ReloadContentModel( )
{
	m_pView->ReloadContentModel( );
	return ( m_Model );
}

bool 
ConfigGameView::Save( )
{
	return m_pView->Save();
}

} // ConfigParser namespace
} // Base namespace
} // RSG namespace
