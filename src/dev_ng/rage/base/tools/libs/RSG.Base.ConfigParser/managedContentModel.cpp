//
// filename:	managedContentModel.cpp
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		1 March 2010
// description:	Managed C++ wrapper around configParser's ContentModel.
//

// --- Includes -----------------------------------------------------------------
#pragma unmanaged
#include "configParser/configParser.h"
#include "configParser/contentModel.h"
#pragma managed

#include "managedContentModel.h"
#include "marshallingUtils.h"

#pragma managed

// --- Implementation -----------------------------------------------------------

namespace RSG
{
namespace Base
{
namespace ConfigParser
{

	

} // ConfigParser namespace
} // Base namespace
} // RSG namespace
