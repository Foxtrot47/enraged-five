#ifndef __MANAGEDCONFIGEXPORTVIEW_H__
#define __MANAGEDCONFIGEXPORTVIEW_H__

//
// File: managedConfigExportView.h
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: 
//

// --- Defines ------------------------------------------------------------------
#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma managed
#pragma once
#endif

// --- Includes -----------------------------------------------------------------
#include "managedConfigModelData.h"

// --- Classes ------------------------------------------------------------------

namespace RSG
{
namespace Base
{
namespace ConfigParser
{

	//PURPOSE
	// Managed wrapper around configParser's ConfigExportView class.
	//
	// Other managed views should utilise this class but not inherit from it.
	//
	public ref class ConfigExportView
	{
	public:
		ConfigExportView( );
		~ConfigExportView( );

		//--- query Methods -----------------------------------------------------
		bool query( System::String^ expr, [System::Runtime::InteropServices::Out] ConfigModelData^ %data );
		bool query( System::String^ expr, [System::Runtime::InteropServices::Out] System::String^ %data );
		bool query( System::String^ expr, [System::Runtime::InteropServices::Out] bool %data );
		bool query( System::String^ expr, [System::Runtime::InteropServices::Out] int %data );
		bool query( System::String^ expr, [System::Runtime::InteropServices::Out] double %data );

		//--- Config Filename Methods -------------------------------------------
		// Animation
		System::String^ GetAnimCompressedExportPath( );
		System::String^ GetAnimExportPath( );

		System::String^ GetControlFilePath( );
		System::String^ GetDefaultControlFile( );

		System::String^ GetSkeletonFilePath( );
		System::String^ GetDefaultSkeletonFile( );

		System::String^ GetCompressionFilePath( );
		System::String^ GetDefaultCompressionFile( );

		System::String^ GetBoneTagsFile( );

		System::String^ GetAnimationPostRenderScript( );

		// Effects
		System::String^ GetEffectsCutsceneSectionName( );
		System::String^ GetRmptfxPath( );

		// Map Report Limits
		float GetBoundDrawableDistanceRatio( );
		int GetDrawableCountLimit( );
		int GetTxdCountLimit( );

		// 3DS Max
		bool GetMaxAutoSubmitsEnabled( );

	protected:
		configParser::ConfigExportView* m_pView;
	};

} // ConfigParser namespace
} // Base namespace
} // RSG namespace

#endif // __MANAGEDCONFIGEXPORTVIEW_H__
