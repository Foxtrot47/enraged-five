#ifndef __MANAGEDCONFIGMODELDATA_H__
#define __MANAGEDCONFIGMODELDATA_H__

//
// File: managedConfigModelData.h
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: 
//

// --- Defines ------------------------------------------------------------------
#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma managed
#pragma once
#endif

// --- Includes -----------------------------------------------------------------
// None

// --- Classes ------------------------------------------------------------------

namespace RSG
{
namespace Base
{
namespace ConfigParser
{

	//PURPOSE
	// Managed wrapper around the configParser's ConfigModelData
	// class.
	//
	// For additional documentation see configParser::ConfigModelData
	// documentation.
	//
	public ref class ConfigModelData : public System::Collections::IEnumerable
	{
	public:
		enum class DataType
		{
			Empty,
			Boolean,
			String,
			Integer,
			Double
		};
		
		// Default constructor.
		ConfigModelData( );

		// Copy constructor from native ConfigModelData object.
		ConfigModelData( const configParser::ConfigModelData& native_data );

		// Destructor.
		~ConfigModelData( );

		DataType GetDataType( );

		bool IsEmpty( );

		virtual System::String^ ToString( ) override;

		// --- Value Getters ----------------------------------------------------

		void GetValue( [System::Runtime::InteropServices::Out] System::String^ %s );
		void GetValue( [System::Runtime::InteropServices::Out] bool^ %b );
		void GetValue( [System::Runtime::InteropServices::Out] int^ %n );
		void GetValue( [System::Runtime::InteropServices::Out] double^ %f );

		// --- Child / Iterator Support -----------------------------------------

		bool HasChildren( );
		size_t ChildCount( );
		ConfigModelData^ operator[]( int index );

		// --- IEnumerable Interface --------------------------------------------

#pragma region IEnumerable
		//PURPOSE
		// Enumerator class to support 'foreach' loops through ConfigModelData
		// children.
		ref struct enumerator : System::Collections::IEnumerator
		{
		public:
			enumerator( ConfigModelData^ m )
				: currentIndex( -1 )
			{
				inst = m;
			}

			~enumerator( ) 
			{ 
			}

			virtual bool MoveNext( )
			{
				++currentIndex;
				return ( currentIndex < (int)inst->ChildCount() );
			}

			property System::Object^ Current
			{
				virtual System::Object^ get()
				{
					ConfigModelData^ mdata = inst[currentIndex];
					return ( mdata );
				}
			}

			virtual void Reset( ) 
			{ 
				currentIndex = -1; 
			}

		private:
			ConfigModelData^ inst;
			int currentIndex;
		};
#pragma endregion // IEnumerable

		virtual System::Collections::IEnumerator^ GetEnumerator( );

	private:
		configParser::ConfigModelData* m_pData;
	};

} // ConfigParser namespace
} // Base namespace
} // RSG namespace

#endif // __MANAGEDCONFIGMODELDATA_H__
