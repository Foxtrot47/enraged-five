//
// filename:	managedConfigExportView.cpp
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		5 January 2010
// description:	Managed C++ wrapper around configParser's ConfigExportView.
//

// --- Includes -----------------------------------------------------------------
#pragma unmanaged
#include "configParser/configParser.h"
#include "configParser/configExportView.h"
#pragma managed

#pragma comment(lib, "ws2_32.lib")

#include "managedConfigExportView.h"
#include "marshallingUtils.h"

#pragma managed

// --- Implementation -----------------------------------------------------------

namespace RSG
{
namespace Base
{
namespace ConfigParser
{

	ConfigExportView::ConfigExportView( )
		: m_pView( NULL )
	{
		m_pView = new configParser::ConfigExportView;
	}

	ConfigExportView::~ConfigExportView( )
	{
		delete m_pView;
	}

	bool 
	ConfigExportView::query( System::String^ expr, [System::Runtime::InteropServices::Out] ConfigModelData^ %data )
	{
		pin_ptr<const wchar_t> wsExpr = PtrToStringChars( expr );

		configParser::ConfigModelData native_data;
		bool result = ( m_pView->query( wsExpr, native_data ) );

		data = gcnew ConfigModelData( native_data );
		return ( result );
	}

	bool 
	ConfigExportView::query( System::String^ expr, [System::Runtime::InteropServices::Out] System::String^ %data )
	{
		pin_ptr<const wchar_t> wsExpr = PtrToStringChars( expr );
		wchar_t wsOutput[_MAX_PATH];

		bool result = ( m_pView->query( wsExpr, wsOutput, _MAX_PATH ) );
		data = gcnew System::String( wsOutput );

		return ( result );
	}

	bool 
	ConfigExportView::query( System::String^ expr, [System::Runtime::InteropServices::Out] bool %data )
	{
		pin_ptr<const wchar_t> wsExpr = PtrToStringChars( expr );
		bool output;

		bool result = ( m_pView->query( wsExpr, output ) );
		data = output;

		return ( result );
	}

	bool 
	ConfigExportView::query( System::String^ expr, [System::Runtime::InteropServices::Out] int %data )
	{
		pin_ptr<const wchar_t> wsExpr = PtrToStringChars( expr );
		int output;

		bool result = ( m_pView->query( wsExpr, output ) );
		data = output;

		return ( result );
	}

	bool 
	ConfigExportView::query( System::String^ expr, [System::Runtime::InteropServices::Out] double %data )
	{
		pin_ptr<const wchar_t> wsExpr = PtrToStringChars( expr );
		double output;

		bool result = ( m_pView->query( wsExpr, output ) );
		data = output;

		return ( result );
	}

	System::String^ 
	ConfigExportView::GetAnimCompressedExportPath( )
	{
		wchar_t wsAnimCompressedExportPath[_MAX_PATH];
		m_pView->GetAnimCompressedExportPath( wsAnimCompressedExportPath, _MAX_PATH );
		return ( gcnew System::String( wsAnimCompressedExportPath ) );
	}

	System::String^ 
	ConfigExportView::GetAnimExportPath( )
	{
		wchar_t wsAnimExportPath[_MAX_PATH];
		m_pView->GetAnimExportPath( wsAnimExportPath, _MAX_PATH );
		return ( gcnew System::String( wsAnimExportPath ) );
	}

	System::String^ 
	ConfigExportView::GetControlFilePath( )
	{
		wchar_t wsControlFilePath[_MAX_PATH];
		m_pView->GetControlFilePath( wsControlFilePath, _MAX_PATH );
		return ( gcnew System::String( wsControlFilePath ) );
	}

	System::String^ 
	ConfigExportView::GetDefaultControlFile( )
	{
		wchar_t wsDefaultControlFile[_MAX_PATH];
		m_pView->GetDefaultControlFile( wsDefaultControlFile, _MAX_PATH );
		return ( gcnew System::String( wsDefaultControlFile ) );
	}

	System::String^ 
	ConfigExportView::GetSkeletonFilePath( )
	{
		wchar_t wsSkeletonFilePath[_MAX_PATH];
		m_pView->GetSkeletonFilePath( wsSkeletonFilePath, _MAX_PATH );
		return ( gcnew System::String( wsSkeletonFilePath ) );
	}

	System::String^ 
	ConfigExportView::GetDefaultSkeletonFile( )
	{
		wchar_t wsDefaultSkeletonFile[_MAX_PATH];
		m_pView->GetDefaultSkeletonFile( wsDefaultSkeletonFile, _MAX_PATH );
		return ( gcnew System::String( wsDefaultSkeletonFile ) );
	}
	
	System::String^ 
	ConfigExportView::GetCompressionFilePath( )
	{
		wchar_t wsCompressionFilePath[_MAX_PATH];
		m_pView->GetCompressionFilePath( wsCompressionFilePath, _MAX_PATH );
		return ( gcnew System::String( wsCompressionFilePath ) );
	}

	System::String^ 
	ConfigExportView::GetDefaultCompressionFile( )
	{
		wchar_t wsDefaultCompressionFile[_MAX_PATH];
		m_pView->GetDefaultCompressionFile( wsDefaultCompressionFile, _MAX_PATH );
		return ( gcnew System::String( wsDefaultCompressionFile ) );
	}

	System::String^ 
	ConfigExportView::GetBoneTagsFile( )
	{
		wchar_t wsBoneTagsFile[_MAX_PATH];
		m_pView->GetBoneTagsFile( wsBoneTagsFile, _MAX_PATH );
		return ( gcnew System::String( wsBoneTagsFile ) );
	}

	System::String^ ConfigExportView::GetAnimationPostRenderScript()
	{
		wchar_t wsAnimationPostRenderScriptPath[_MAX_PATH];
		m_pView->GetAnimationPostRenderScript( wsAnimationPostRenderScriptPath, _MAX_PATH );
		return ( gcnew System::String( wsAnimationPostRenderScriptPath ) );
	}

	System::String^ 
	ConfigExportView::GetEffectsCutsceneSectionName( )
	{
		wchar_t wsEffecsCutsceneSectionName[_MAX_PATH];
		m_pView->GetEffectsCutsceneSectionName( wsEffecsCutsceneSectionName, _MAX_PATH );
		return ( gcnew System::String( wsEffecsCutsceneSectionName ) );
	}

	System::String^ 
	ConfigExportView::GetRmptfxPath( )
	{
		wchar_t wsRmptfxPath[_MAX_PATH];
		m_pView->GetRmptfxPath( wsRmptfxPath, _MAX_PATH );
		return ( gcnew System::String( wsRmptfxPath ) );
	}

	int
	ConfigExportView::GetDrawableCountLimit()
	{
		int limit = -1;
		m_pView->GetDrawableCountLimit(limit);
		return limit;
	}

	int
	ConfigExportView::GetTxdCountLimit()
	{
		int limit = -1;
		m_pView->GetTxdCountLimit(limit);
		return limit;
	}

	float
	ConfigExportView::GetBoundDrawableDistanceRatio()
	{
		double ratio = -1.0;
		m_pView->GetBoundDrawableDistanceRatio(ratio);
		return (float) ratio;
	}

	bool ConfigExportView::GetMaxAutoSubmitsEnabled()
	{
		bool enabled = true;
		m_pView->GetMaxAutoSubmitsEnabled(enabled);
		return enabled;
	}

} // ConfigParser namespace
} // Base namespace
} // RSG namespace
