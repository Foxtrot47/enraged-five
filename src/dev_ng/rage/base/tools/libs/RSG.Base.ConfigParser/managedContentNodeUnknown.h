#ifndef __MANAGEDCONTENTNODEUNKNOWN_H__
#define __MANAGEDCONTENTNODEUNKNOWN_H__

//
// File: managedContentNodeUnknown.h
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: 
//

// --- Defines ------------------------------------------------------------------
#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma managed
#pragma once
#endif

// --- Includes -----------------------------------------------------------------

// configParser headers
#pragma unmanaged
#include "configParser/ContentNodeUnknown.h"
#pragma managed

// Local headers
#include "managedContentNode.h"

// --- Classes ------------------------------------------------------------------

namespace RSG
{
namespace Base
{
namespace ConfigParser
{

	//PURPOSE
	// Managed wrapper around the configParser's ContentModel class.
	//
	// For additional documentation see configParser::ContentModel
	// documentation.
	//
	public ref class ContentNodeUnknown : public ContentNode
	{
	public:
		ContentNodeUnknown( configParser::ContentNode* pNode, 
			ContentNode^ parent, ContentModel^ model )
			: ContentNode( pNode, parent, model )
		{
		}

		ContentNodeUnknown( ContentNodeUnknown^ node )
			: ContentNode( node )
		{
		}

		virtual ~ContentNodeUnknown( )
		{
		}
	};

} // ConfigParser namespace
} // Base namespace
} // RSG namespace

#endif // __MANAGEDCONTENTNODEUNKNOWN_H__
