//
// filename:	managedContentNode.cpp
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		1 March 2010
// description:	Managed C++ wrapper around configParser's ContentNode class.
//

// --- Includes -----------------------------------------------------------------
#pragma unmanaged
#include "configParser/configParser.h"
#pragma managed

#include "managedContentNode.h"
#include "managedContentNodeFactory.h"
#include "managedContentModel.h"

// --- Implementation -----------------------------------------------------------

namespace RSG
{
namespace Base
{
namespace ConfigParser
{

	ContentNode::ContentNode( configParser::ContentNode* pNode, ContentNode^ parent, ContentModel^ model )		
		: m_pNode( pNode )
		, m_Parent( parent )
		, m_Model( model )
	{
#if 0
		if ( nullptr != parent )
		{
			System::Diagnostics::Debug::Assert( 
				( parent->GetPtr() == pNode->GetParent() ),
				"Parent reference mismatch.  Internal error." );
		}
#endif
		this->m_aInputs = gcnew System::Collections::Generic::List<ContentNode^>( );
		this->m_aOutputs = gcnew System::Collections::Generic::List<ContentNode^>( );
	}

	ContentNode::ContentNode( ContentNode^ node )
	{
		m_Parent = node->Parent;
		m_pNode = node->m_pNode;
		m_Model = node->m_Model;

		this->m_aInputs = gcnew System::Collections::Generic::List<ContentNode^>( );
		this->m_aInputs->AddRange(node->Inputs);
		this->m_aOutputs = gcnew System::Collections::Generic::List<ContentNode^>( );
		this->m_aOutputs->AddRange(node->Outputs);
	}

	ContentNode::~ContentNode( )	
	{
		m_pNode = NULL;
	}

	void 
	ContentNode::ResolveInputNodes( )
	{
		System::Diagnostics::Debug::Assert( 0 == this->m_aInputs->Count,
			"Looks like inputs have already been resolved." );
		
		// Resolve input by finding the referenced ContentNode.
		ContentNodeGroup^ root = m_Model->Root;

		for ( configParser::ContentNode::NodeContainerConstIter it = m_pNode->BeginInputs();
			  it != m_pNode->EndInputs( );
			  ++it )
		{
			configParser::ContentNode* pInput = (*it);
			System::String^ inputName = gcnew System::String( pInput->GetName() );
			System::String^ inputType = gcnew System::String( pInput->GetType() );
			System::String^ inputGroup = gcnew System::String( m_pNode->GetGroup() );
			
			ContentNode^ groupNode = root->FindFirst( inputGroup, "group" );
			ContentNodeGroup^ group = dynamic_cast<ContentNodeGroup^>( groupNode );
			
			if (nullptr == group)
			{
// DHM FIX ME 2010/03/25 STOP CREATING MANAGED NODES
				this->m_aInputs->Add( ContentNodeFactory::CreateNode( m_Model, this, *it ) );
			}
			else
			{
				ContentNode^ node = group->FindFirst( inputName, inputType );
				this->m_aInputs->Add( node );
				node->AddOutput( this );
			}
		}
	}

	int	
	ContentNode::InputIndexOf( ContentNode^ node )
	{
		for ( int n = 0; n < m_aInputs->Count; ++n )
		{
			if (node == m_aInputs[n])
				return ( n );
		}
		return ( -1 );
	}

	void
	ContentNode::AddInput( ContentNode^ node )
	{
		m_pNode->AddInput( node->m_pNode );
		m_aInputs->Add( node );
		RaiseModelChangedEvent( );
	}

	void 
	ContentNode::RemoveInput( ContentNode^ node )
	{
		RemoveInputAt( InputIndexOf( node ) );
	}

	void
	ContentNode::RemoveInputAt( int index )
	{
		m_pNode->RemoveInput( m_pNode->BeginInputs() + index );
		m_aInputs->RemoveAt( index );
		RaiseModelChangedEvent( );
	}

		int	
	ContentNode::OutputIndexOf( ContentNode^ node )
	{
		for ( int n = 0; n < m_aOutputs->Count; ++n )
		{
			if (node == m_aOutputs[n])
				return ( n );
		}
		return ( -1 );
	}

	void
	ContentNode::AddOutput( ContentNode^ node )
	{
		m_pNode->AddOutput( node->m_pNode );
		m_aOutputs->Add( node );
		RaiseModelChangedEvent( );
	}

	void 
	ContentNode::RemoveOutput( ContentNode^ node )
	{
		RemoveOutputAt( OutputIndexOf( node ) );
	}

	void
	ContentNode::RemoveOutputAt( int index )
	{
		m_pNode->RemoveOutput( m_pNode->BeginOutputs() + index );
		m_aOutputs->RemoveAt( index );
		RaiseModelChangedEvent( );
	}

	void 
	ContentNode::RaiseModelChangedEvent( )
	{
		m_Model->RaiseChangedEvent( );
	}

} // ConfigParser namespace
} // Base namespace
} // RSG namespace
