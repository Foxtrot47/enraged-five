#ifndef __MANAGEDCONTENTMODEL_H__
#define __MANAGEDCONTENTMODEL_H__

//
// File: managedContentModel.h
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: 
//

// --- Defines ------------------------------------------------------------------
#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma managed
#pragma once
#endif

// --- Includes -----------------------------------------------------------------
#include "configParser/contentModel.h"
#include "managedContentNodeGroup.h"

// --- Classes ------------------------------------------------------------------

namespace RSG
{
namespace Base
{
namespace ConfigParser
{

	// Forward declare.
	ref class ContentModel;

	//PURPOSE
	//
	public delegate void ModelChangedHandler( ContentModel^ model );

	//PURPOSE
	// Managed wrapper around the configParser's ContentModel class.
	//
	// For additional documentation see configParser::ContentModel
	// documentation.
	//
	public ref class ContentModel
	{
	public:
		// Event raised when nodes change in the content tree.
		event ModelChangedHandler^ Changed;

		ContentModel( configParser::ContentModel& model )
			: m_pData( &model )
		{
			configParser::ContentNodeGroup& root = m_pData->GetRoot();
			this->m_Root = gcnew ContentNodeGroup( &root, nullptr, this );
			this->m_Root->ResolveInputNodes();
		}

		~ContentModel( )
		{
			m_pData = NULL;
		}

		bool SaveXml( )
		{
			return ( m_pData->SaveXml( ) );
		}

		void RaiseChangedEvent( )
		{
			this->Changed( this );
		}

		// ContentNodeFactory Required Methods
		configParser::ContentModel* GetModelPtr( ) { return ( m_pData ); }

		//-------------------------------------------------------------------
		// Properties
		//-------------------------------------------------------------------

		[System::ComponentModel::Browsable(false)]
		property ContentNodeGroup^ Root
		{
			ContentNodeGroup^ get()
			{
				return ( m_Root );
			}
		}

	private:
		ContentNodeGroup^			m_Root;
		configParser::ContentModel* m_pData;
	};

} // ConfigParser namespace
} // Base namespace
} // RSG namespace

#endif // __MANAGEDCONTENTMODEL_H__
