#ifndef __MANAGEDCONTENTNODEMAPS_H__
#define __MANAGEDCONTENTNODEMAPS_H__

//
// File: managedContentNodeMaps.h
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: 
//

// --- Defines ------------------------------------------------------------------
#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma managed
#pragma once
#endif

// --- Includes -----------------------------------------------------------------

// configParser headers
#pragma unmanaged
#include "configParser/ContentNodeMaps.h"
#pragma managed

// Local headers
#include "managedContentNode.h"

// --- Classes ------------------------------------------------------------------

namespace RSG
{
namespace Base
{
namespace ConfigParser
{

	//PURPOSE
	//
	public ref class ContentNodeMap : public ContentNode
	{
	public:
		ContentNodeMap( configParser::ContentNodeMap* pNode, 
			ContentNode^ parent, ContentModel^ model )
			: ContentNode( pNode, parent, model )
		{
		}
	
		ContentNodeMap( ContentNodeMap^ map )
			: ContentNode( map )
		{
		}

		virtual ~ContentNodeMap( ) { }

		//-------------------------------------------------------------------
		// Properties
		//-------------------------------------------------------------------
		
		[System::ComponentModel::Category("Map")]
		[System::ComponentModel::Description("Source directory.")]
		property System::String^ Path
		{
			System::String^ get()
			{
				configParser::ContentNodeMap* pMap = CastPtr();
				configParser::ContentNodeGroup* pGroup = 
					dynamic_cast<configParser::ContentNodeGroup*>( pMap->GetParent() );
				if ( pGroup )
				{ 
					wchar_t path[_MAX_PATH];
					pGroup->GetRawAbsolutePath( path, _MAX_PATH );
					return ( gcnew System::String( path ) );
				}
				else
				{
					return ( System::String::Empty );
				}
			}
		}

		[System::ComponentModel::Category("Map")]
		[System::ComponentModel::Description("Map unique identifier seed.")]
		[System::ComponentModel::DefaultValue("0")]
		property unsigned int Seed
		{
			unsigned int get()
			{
				configParser::ContentNodeMap* pMap = CastPtr();
				return ( pMap->GetSeed() );
			}
			void set( unsigned int seed )
			{
				configParser::ContentNodeMap* pMap = CastPtr();
				pMap->SetSeed( seed );
			}
		}


		[System::ComponentModel::Category("Map")]
		[System::ComponentModel::Description("Map namespace prefix.")]
		[System::ComponentModel::DefaultValue("")]
		property System::String^ Prefix
		{
			System::String^ get()
			{
				configParser::ContentNodeMap* pMap = CastPtr();
				const STL_STRING& prefix = pMap->GetPrefix( );
				return ( gcnew System::String( prefix.c_str() ) );
			}
			void set( System::String^ prefix )
			{
				configParser::ContentNodeMap* pMap = CastPtr();
				const TCHAR* s = (const TCHAR*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalUni(prefix);
				pMap->SetPrefix( s );
				System::Runtime::InteropServices::Marshal::FreeHGlobal( (System::IntPtr)(void*)s );
				RaiseModelChangedEvent();
			}
		}

		[System::ComponentModel::Category("Map")]
		[System::ComponentModel::Description("Export IDE definitions file.")]
		[System::ComponentModel::DefaultValue(true)]
		property bool ExportDefinitions
		{
			bool get() 
			{ 
				configParser::ContentNodeMap* pMap = CastPtr();
				return ( pMap->GetDefinitions() ); 
			}			
			void set( bool d ) 
			{ 
				configParser::ContentNodeMap* pMap = CastPtr();
				pMap->SetDefinitions( d ); 
				RaiseModelChangedEvent();
			}
		}

		[System::ComponentModel::Category("Map")]
		[System::ComponentModel::Description("Export IPL instances file.")]
		[System::ComponentModel::DefaultValue(true)]
		property bool ExportInstances
		{
			bool get() 
			{ 
				configParser::ContentNodeMap* pMap = CastPtr();
				return ( pMap->GetInstances() ); 
			}
			void set( bool i ) 
			{ 
				configParser::ContentNodeMap* pMap = CastPtr();
				pMap->SetInstances( i ); 
				RaiseModelChangedEvent();
			}
		}

		[System::ComponentModel::Category("Map")]
		[System::ComponentModel::Description("SLOD2 link required.")]
		[System::ComponentModel::DefaultValue(false)]
		property bool SLOD2Link
		{
			bool get() 
			{ 
				configParser::ContentNodeMap* pMap = CastPtr();
				return ( pMap->GetSLOD2Link() ); 
			}
			void set( bool i ) 
			{ 
				configParser::ContentNodeMap* pMap = CastPtr();
				pMap->SetSLOD2Link( i ); 
				RaiseModelChangedEvent();
			}
		}

		[System::ComponentModel::Category("Map")]
		[System::ComponentModel::Description("Export RPF data file.")]
		[System::ComponentModel::DefaultValue(true)]
		property bool ExportData
		{
			bool get() 
			{ 
				configParser::ContentNodeMap* pMap = CastPtr();
				return ( pMap->GetData() ); 
			}
		}

		[System::ComponentModel::Category("Map")]
		[System::ComponentModel::Description("Auto-export enable flag.")]
		[System::ComponentModel::Browsable(true)]
		property bool AutoExport
		{
			bool get() 
			{ 
				configParser::ContentNodeMap* pMap = CastPtr();
				return ( pMap->GetAutoExport() ); 
			}
			void set( bool p ) 
			{ 
				configParser::ContentNodeMap* pMap = CastPtr();
				pMap->SetAutoExport( p ); 
				RaiseModelChangedEvent();
			}
		}

		[System::ComponentModel::Category("Map")]
		[System::ComponentModel::Description("Override pedantic export checks.")]
		[System::ComponentModel::Browsable(false)]
		property bool Pedantic
		{
			bool get() 
			{ 
				configParser::ContentNodeMap* pMap = CastPtr();
				return ( pMap->GetPedantic() ); 
			}
			void set( bool p ) 
			{ 
				configParser::ContentNodeMap* pMap = CastPtr();
				pMap->SetPedantic( p ); 
				RaiseModelChangedEvent();
			}
		}

		enum class MapSubType
		{
			Unknown,
			Props,
			Interior,
			Container,
			ContainerProps,
			ContainerLod,
			ContainerOccl,
			AssetCombine
		};

		[System::ComponentModel::Category("Map")]
		[System::ComponentModel::Description("Map subtype.")]
		property MapSubType MapType
		{
			MapSubType get()
			{
				configParser::ContentNodeMap* pMap = CastPtr();
				return ( (MapSubType)pMap->GetMapType() );
			}
			void set( MapSubType subtype )
			{
				configParser::ContentNodeMap* pMap = CastPtr();
				pMap->SetMapType( (configParser::ContentNodeMap::eMapType)subtype );
			}
		}

	protected:
		virtual configParser::ContentNodeMap* CastPtr( )
		{
			configParser::ContentNodeMap* pMap =
				dynamic_cast<configParser::ContentNodeMap*>( m_pNode );
			System::Diagnostics::Debug::Assert( NULL != pMap, "Invalid configParser::ContentNodeMap object." );
			return ( pMap );
		}
	};

	//PURPOSE
	//
	public ref class ContentNodeMapZip : public ContentNodeZip
	{
	public:
		ContentNodeMapZip( configParser::ContentNodeMapZip* pNode, 
			ContentNode^ parent, ContentModel^ model )
			: ContentNodeZip( pNode, parent, model )
		{
			this->m_aChildren = gcnew System::Collections::Generic::List<ContentNode^>( );

			for ( configParser::ContentNode::NodeContainerConstIter it = pNode->BeginChildren();
				  it != pNode->EndChildren( );
				  ++it )
			{
				this->m_aChildren->Add( ContentNodeFactory::CreateNode( m_Model, this, *it ) );
			}
		}

		ContentNodeMapZip( ContentNodeMapZip^ map )
			: ContentNodeZip( map )
		{
		}

		virtual ~ContentNodeMapZip( ) { }

		[System::ComponentModel::Category("MapZip")]
		[System::ComponentModel::Description("Children.")]
		[System::ComponentModel::Browsable(false)]
		property array<ContentNode^>^ Children
		{
			array<ContentNode^>^ get()
			{
				return ( m_aChildren->ToArray() );
			}
		}

		[System::ComponentModel::Category("MapZip")]
		[System::ComponentModel::Description("Use the new collision pipeline.")]
		[System::ComponentModel::DefaultValue(false)]
		property bool ViaBoundsProcessor
		{
			bool get() 
			{ 
				configParser::ContentNodeMapZip* pMapZip = CastPtr();
				return ( pMapZip->GetViaBoundsProcessor() ); 
			}
		}

	protected:
		System::Collections::Generic::List<ContentNode^>^ m_aChildren;	//!< Managed ContentNode^ children.
	
	private:
		configParser::ContentNodeMapZip* CastPtr( )
		{
			configParser::ContentNodeMapZip* pMap =
				dynamic_cast<configParser::ContentNodeMapZip*>( m_pNode );
			System::Diagnostics::Debug::Assert( NULL != pMap, "Invalid configParser::ContentNodeMapZip object." );
			return ( pMap );
		}
	};

	//PURPOSE
	//
	public ref class ContentNodeMapProcessedZip : public ContentNodeZip
	{
	public:
		ContentNodeMapProcessedZip( configParser::ContentNodeMapProcessedZip* pNode, 
			ContentNode^ parent, ContentModel^ model )
			: ContentNodeZip( pNode, parent, model )
		{
			this->m_aChildren = gcnew System::Collections::Generic::List<ContentNode^>( );

			for ( configParser::ContentNode::NodeContainerConstIter it = pNode->BeginChildren();
				it != pNode->EndChildren( );
				++it )
			{
				this->m_aChildren->Add( ContentNodeFactory::CreateNode( m_Model, this, *it ) );
			}
		}

		ContentNodeMapProcessedZip( ContentNodeMapProcessedZip^ map )
			: ContentNodeZip( map )
		{
		}

		virtual ~ContentNodeMapProcessedZip( ) { }

		[System::ComponentModel::Category("MapProcessedZip")]
		[System::ComponentModel::Description("Children.")]
		[System::ComponentModel::Browsable(false)]
		property array<ContentNode^>^ Children
		{
			array<ContentNode^>^ get()
			{
				return ( m_aChildren->ToArray() );
			}
		}

	protected:
		System::Collections::Generic::List<ContentNode^>^ m_aChildren;	//!< Managed ContentNode^ children.

	private:
		configParser::ContentNodeMapProcessedZip* CastPtr( )
		{
			configParser::ContentNodeMapProcessedZip* pMap =
				dynamic_cast<configParser::ContentNodeMapProcessedZip*>( m_pNode );
			System::Diagnostics::Debug::Assert( NULL != pMap, "Invalid configParser::ContentNodeMapProcessedZip object." );
			return ( pMap );
		}
	};
	
	//
	// This is the asset combine output for maps.
	//
	public ref class ContentNodeMapCombineZip : public ContentNodeZip
	{
	public:
		ContentNodeMapCombineZip( configParser::ContentNodeMapCombineZip* pNode, 
			ContentNode^ parent, ContentModel^ model )
			: ContentNodeZip( pNode, parent, model )
		{
		}

		ContentNodeMapCombineZip( ContentNodeMapCombineZip^ map )
			: ContentNodeZip( map )
		{
		}

		virtual ~ContentNodeMapCombineZip( ) { }

	private:
		configParser::ContentNodeMapCombineZip* CastPtr( )
		{
			configParser::ContentNodeMapCombineZip* pMap =
				dynamic_cast<configParser::ContentNodeMapCombineZip*>( m_pNode );
			System::Diagnostics::Debug::Assert( NULL != pMap, "Invalid configParser::ContentNodeMapCombineZip object." );
			return ( pMap );
		}
	};

} // ConfigParser namespace
} // Base namespace
} // RSG namespace

#endif // __MANAGEDCONTENTNODEMAPS_H__
