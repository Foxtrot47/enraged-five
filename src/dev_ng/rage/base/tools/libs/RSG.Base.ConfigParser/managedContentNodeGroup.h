#ifndef __MANAGEDCONTENTNODEGROUP_H__
#define __MANAGEDCONTENTNODEGROUP_H__

//
// File: managedContentNodeGroup.h
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: 
//

// --- Defines ------------------------------------------------------------------
#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma managed
#pragma once
#endif

// --- Includes -----------------------------------------------------------------

// configParser headers
#include "configParser/configParser.h"
#include "configParser/contentNodeGroup.h"

// Local headers
#include "managedContentNode.h"
#include "managedContentNodeFactory.h"

// --- Classes ------------------------------------------------------------------

namespace RSG
{
namespace Base
{
namespace ConfigParser
{

	//PURPOSE
	// Managed wrapper around the configParser's ContentNodeGroup class.
	//
	// For additional documentation see configParser::ContentNodeGroup
	// documentation.
	//
	public ref class ContentNodeGroup : public ContentNode
	{
	public:
		// Default constructor.
		ContentNodeGroup( configParser::ContentNodeGroup* pNode, 
			ContentNode^ parent, ContentModel^ model );

		// Copy constructor.
		ContentNodeGroup( ContentNodeGroup^ group );

		// Destructor.
		virtual ~ContentNodeGroup( );

		//-------------------------------------------------------------------
		// Methods
		//-------------------------------------------------------------------

		virtual void	ResolveInputNodes( ) override;

		// Find methods
		ContentNode^	FindFirst( System::String^ name );
		ContentNode^	FindFirst( System::String^ name, System::String^ type );
		System::Collections::Generic::List<ContentNode^>^ FindAll( System::String^ type );
		System::Collections::Generic::List<ContentNode^>^ FindAll( System::String^ name, System::String^ type );

		int				ChildIndexOf( ContentNode^ node );
		void			AddChild( ContentNode^ node );
		void			RemoveChild( ContentNode^ node );
		void			RemoveChildAt( int index );

		//-------------------------------------------------------------------
		// Properties
		//-------------------------------------------------------------------

		[System::ComponentModel::Category("Group")]
		[System::ComponentModel::Description("Group relative directory.")]
		property System::String^ RelativePath
		{
			System::String^ get()
			{
				configParser::ContentNodeGroup* pGroup = CastPtr( );
				wchar_t path[_MAX_PATH];
				pGroup->GetRelativePath( path, _MAX_PATH );

				return ( gcnew System::String( path ) );
			}
		}

		[System::ComponentModel::Category("Group")]
		[System::ComponentModel::Description("Group raw relative directory.")]
		property System::String^ RawRelativePath
		{
			System::String^ get()
			{
				configParser::ContentNodeGroup* pGroup = CastPtr( );
				return ( gcnew System::String( pGroup->GetRawRelativePath( ) ) );
			}
		}
		
		[System::ComponentModel::Category("Group")]
		[System::ComponentModel::Description("Group absolute directory.")]
		property System::String^ AbsolutePath
		{
			System::String^ get()
			{
				configParser::ContentNodeGroup* pGroup = CastPtr( );
				wchar_t path[_MAX_PATH];
				pGroup->GetAbsolutePath( path, _MAX_PATH );

				return ( gcnew System::String( path ) );
			}
		}

		[System::ComponentModel::Category("Group")]
		[System::ComponentModel::Description("Group raw absolute directory.")]
		property System::String^ RawAbsolutePath
		{
			System::String^ get()
			{
				configParser::ContentNodeGroup* pGroup = CastPtr( );
				wchar_t path[_MAX_PATH];
				pGroup->GetRawAbsolutePath( path, _MAX_PATH );

				return ( gcnew System::String( path ) );
			}
		}

		[System::ComponentModel::Category("Group")]
		[System::ComponentModel::Description("Children.")]
		[System::ComponentModel::Browsable(false)]
		property array<ContentNode^>^ Children
		{
			array<ContentNode^>^ get()
			{
				return ( m_aChildren->ToArray() );
			}
		}

	protected:
		System::Collections::Generic::List<ContentNode^>^	m_aChildren;	//!< Managed ContentNode^ children.
	
	private:		
		configParser::ContentNodeGroup* CastPtr( )
		{
			System::Diagnostics::Debug::Assert( m_pNode != NULL, "Invalid native object." );
			configParser::ContentNodeGroup* pGroup =
				dynamic_cast<configParser::ContentNodeGroup*>( m_pNode );
			System::Diagnostics::Debug::Assert( NULL != pGroup, "Invalid configParser::ContentNodeGroup object." );
			return ( pGroup );
		}
	};

} // ConfigParser namespace
} // Base namespace
} // RSG namespace

#endif // __MANAGEDCONTENTNODEGROUP_H__
