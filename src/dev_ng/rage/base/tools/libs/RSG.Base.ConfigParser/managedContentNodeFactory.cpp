//
// filename:	managedContentNodeFactory.cpp
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		1 March 2010
// description:	
//

// --- Includes -----------------------------------------------------------------
#include "managedContentNodeFactory.h"
#include "managedContentModel.h"
#include "managedContentNodeCore.h"
#include "managedContentNodeGroup.h"
#include "managedContentNodeLevel.h"
#include "managedContentNodeLevelImage.h"
#include "managedContentNodeMaps.h"
#include "managedContentNodeVehicles.h"
#include "managedContentNodeTemplates.h"
#include "managedContentNodeUnknown.h"
#include "marshallingUtils.h"

#pragma managed

// --- Implementation -----------------------------------------------------------

namespace RSG
{
namespace Base
{
namespace ConfigParser
{

	ContentNode^ 
	ContentNodeFactory::CreateNode( ContentModel^ model, ContentNode^ parent, configParser::ContentNode* pNode )
	{
		if ( dynamic_cast<configParser::ContentNodeMapZip*>( pNode ) )
		{
			return ( gcnew ContentNodeMapZip( dynamic_cast<configParser::ContentNodeMapZip*>( pNode ), parent, model ) );
		}
		else if ( dynamic_cast<configParser::ContentNodeMapProcessedZip*>( pNode ) )
		{
			return ( gcnew ContentNodeMapProcessedZip( dynamic_cast<configParser::ContentNodeMapProcessedZip*>( pNode ), parent, model ) );
		}
		else if ( dynamic_cast<configParser::ContentNodeMapCombineZip*>( pNode ) )
		{
			return ( gcnew ContentNodeMapCombineZip( dynamic_cast<configParser::ContentNodeMapCombineZip*>( pNode ), parent, model ) );
		}
		else if ( dynamic_cast<configParser::ContentNodeVehiclesRpf*>( pNode ) )
		{
			return ( gcnew ContentNodeVehiclesRpf( dynamic_cast<configParser::ContentNodeVehiclesRpf*>( pNode ), parent, model ) );
		}
		else if ( dynamic_cast<configParser::ContentNodeVehiclesZip*>( pNode ) )
		{
			return ( gcnew ContentNodeVehiclesZip( dynamic_cast<configParser::ContentNodeVehiclesZip*>( pNode ), parent, model ) );
		}
		else if ( dynamic_cast<configParser::ContentNodeRpf*>( pNode ) )
		{
			return ( gcnew ContentNodeRpf( dynamic_cast<configParser::ContentNodeRpf*>( pNode ), parent, model ) );
		}
		else if ( dynamic_cast<configParser::ContentNodeFile*>( pNode ) )
		{
			return ( gcnew ContentNodeFile( dynamic_cast<configParser::ContentNodeFile*>( pNode ), parent, model ) );
		}
		else if ( dynamic_cast<configParser::ContentNodeTemplateFileEach*>( pNode ) )
		{
			return ( gcnew ContentNodeTemplateFileEach( dynamic_cast<configParser::ContentNodeTemplateFileEach*>( pNode ), parent, model ) );
		}
		else if ( dynamic_cast<configParser::ContentNodeLevel*>( pNode ) )
		{
			return ( gcnew ContentNodeLevel( dynamic_cast<configParser::ContentNodeLevel*>( pNode ), parent, model ) );
		}
		else if ( dynamic_cast<configParser::ContentNodeLevelImage*>( pNode ) )
		{
			return ( gcnew ContentNodeLevelImage( dynamic_cast<configParser::ContentNodeLevelImage*>( pNode ), parent, model ) );
		}
		else if ( dynamic_cast<configParser::ContentNodeGroup*>( pNode ) )
		{
			return ( gcnew ContentNodeGroup( dynamic_cast<configParser::ContentNodeGroup*>( pNode ), parent, model ) );
		}
		else if ( dynamic_cast<configParser::ContentNodeMap*>( pNode ) )
		{
			return ( gcnew ContentNodeMap( dynamic_cast<configParser::ContentNodeMap*>( pNode ), parent, model ) );
		}
		else
		{
			return ( gcnew ContentNodeUnknown( pNode, parent, model ) );
		}
	}

	ContentNodeGroup^	
	ContentNodeFactory::CreateNodeGroup( ContentModel^ model,
									ContentNode^ parent,
									System::String^ name,
									System::String^ path )
	{
		pin_ptr<const wchar_t> wsName = PtrToStringChars( name );
		pin_ptr<const wchar_t> wsPath = PtrToStringChars( path );

		configUtil::Environment& env = model->GetModelPtr()->GetEnvironment();
		configParser::ContentNodeGroup* pGroup = new 
			configParser::ContentNodeGroup( NULL, &env, wsName, wsPath );
		ContentNodeGroup^ node = gcnew ContentNodeGroup( pGroup, parent, model );
		return (node);
	}

	ContentNodeMap^
	ContentNodeFactory::CreateNodeMap( ContentModel^ model, 
									ContentNode^ parent,
									System::String^ name, 
									System::String^ subtype,
									System::String^ prefix,
									bool defs, 
									bool insts )
	{
		pin_ptr<const wchar_t> wsName = PtrToStringChars( name );
		pin_ptr<const wchar_t> wsSubtype = PtrToStringChars( subtype );
		pin_ptr<const wchar_t> wsPrefix = PtrToStringChars( prefix );

		configUtil::Environment& env = model->GetModelPtr()->GetEnvironment();
		configParser::ContentNodeMap* pMap = new 
			configParser::ContentNodeMap( NULL, &env, wsName, wsSubtype, wsPrefix, defs, insts );
		ContentNodeMap^ node = gcnew ContentNodeMap( pMap, parent, model );
		return (node);
	}
	
	ContentNodeMapZip^
	ContentNodeFactory::CreateNodeMapZip( ContentModel^ model, 
									ContentNode^ parent,
									System::String^ name,
									ContentNodeMap^ input )
	{
		pin_ptr<const wchar_t> wsName = PtrToStringChars( name );
		
		configUtil::Environment& env = model->GetModelPtr()->GetEnvironment();
#pragma warning( push )
#pragma warning( disable: 4482 )
		configParser::ContentNodeMapZip* pMapZip = new
			configParser::ContentNodeMapZip( NULL, &env, wsName, configParser::Platform::kIndependent );
		ContentNodeMapZip^ node = gcnew ContentNodeMapZip( pMapZip, parent, model );
		node->AddInput(input);
		input->AddOutput(node);
		return (node);
#pragma warning( pop )
	}

	ContentNodeMapProcessedZip^
	ContentNodeFactory::CreateNodeMapProcessedZip( ContentModel^ model, 
													ContentNode^ parent,
													System::String^ name,
													ContentNodeMapZip^ input )
	{
		pin_ptr<const wchar_t> wsName = PtrToStringChars( name );

		configUtil::Environment& env = model->GetModelPtr()->GetEnvironment();
#pragma warning( push )
#pragma warning( disable: 4482 )
		configParser::ContentNodeMapProcessedZip* pMapProcessedZip = new
			configParser::ContentNodeMapProcessedZip( NULL, &env, wsName, configParser::Platform::kIndependent );
		ContentNodeMapProcessedZip^ node = gcnew ContentNodeMapProcessedZip( pMapProcessedZip, parent, model );
		node->AddInput(input);
		input->AddOutput(node);
		return (node);
#pragma warning( pop )
	}

} // ConfigParser namespace
} // Base namespace
} // RSG namespace
