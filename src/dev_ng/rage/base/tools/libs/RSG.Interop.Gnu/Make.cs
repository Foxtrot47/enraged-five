﻿//---------------------------------------------------------------------------------------------
// <copyright file="Make.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Gnu
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Text;
    
    /// <summary>
    /// GNU Make interoperability class.
    /// </summary>
    public static class Make
    {
        #region Constants
        /// <summary>
        /// GNU make executable filename.
        /// </summary>
        private const String MAKE_EXE = "make.exe";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Make installed status.
        /// </summary>
        public static bool IsInstalled
        {
            get;
            private set;
        }

        /// <summary>
        /// Make installation directory.
        /// </summary>
        public static String AbsolutePath
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Static constructor.
        /// </summary>
        static Make()
        {
            String toolsroot = Environment.GetEnvironmentVariable("RS_TOOLSROOT");
            AbsolutePath = Path.Combine(toolsroot, "bin", MAKE_EXE);
            IsInstalled = File.Exists(AbsolutePath);
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Start make build for default target.
        /// </summary>
        /// <param name="makefile"></param>
        /// <returns></returns>
        public static bool Start(String makefile)
        {
            return (Start(makefile, String.Empty));
        }

        /// <summary>
        /// Start make build for specific target.
        /// </summary>
        /// <param name="makefile"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public static bool Start(String makefile, String target)
        {
            return (Start(makefile, new String[] { target }));
        }

        /// <summary>
        /// Start make build for specific targets.
        /// </summary>
        /// <param name="makefile"></param>
        /// <param name="targets"></param>
        /// <returns></returns>
        public static bool Start(String makefile, IEnumerable<String> targets)
        {
            Debug.Assert(IsInstalled);
            if (!IsInstalled)
                throw (new NotSupportedException("make is not available on this system."));

            StringBuilder arguments = new StringBuilder();
            arguments.AppendFormat("--makefile={0}", makefile);
            arguments.Append(String.Join(" ", targets));

            Process p = Process.Start(AbsolutePath, arguments.ToString());
            p.WaitForExit();

            return (0 == p.ExitCode);
        }
        #endregion // Controller Methods
    }

} // RSG.Interop.Gnu namespace
