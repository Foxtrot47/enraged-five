﻿//---------------------------------------------------------------------------------------------
// <copyright file="MakefileRule.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Gnu
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// 
    /// </summary>
    public class MakefileRule
    {
        #region Member Data
        private String _target;
        private IEnumerable<String> _dependencies;
        private String _command;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor; specifying the components of a make rule.
        /// </summary>
        /// <param name="target"></param>
        /// <param name="dependencies"></param>
        /// <param name="command"></param>
        public MakefileRule(String target, IEnumerable<String> dependencies, String command)
        {
            this._target = target;
            this._dependencies = dependencies;
            this._command = command;
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        public void Write(System.IO.TextWriter writer)
        {
            writer.WriteLine("{0}: {1}", _target, String.Join(" ", _dependencies));
            writer.WriteLine("\t{0}", _command);
            writer.WriteLine();
        }
        #endregion // Controller Methods
    }

} // RSG.Interop.Gnu namespace
