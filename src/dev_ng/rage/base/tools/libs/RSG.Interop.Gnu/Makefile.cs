﻿//---------------------------------------------------------------------------------------------
// <copyright file="Makefile.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Gnu
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    /// <summary>
    /// GNU Make Makefile interoperability class.
    /// </summary>
    /// This class is used to help create a GNU make makefile set of rules for 
    /// executing ICommandLineToolTask's.
    /// 
    public class Makefile
    {
        #region Member Data
        /// <summary>
        /// Rule storage.
        /// </summary>
        private List<MakefileRule> _rules;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public Makefile()
        {
            this._rules = new List<MakefileRule>();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Save state to disk.
        /// </summary>
        /// <param name="filename"></param>
        public void Save(String filename)
        {
            using (FileStream fs = new FileStream(filename, FileMode.Create, FileAccess.Write))
            using (StreamWriter writer = new StreamWriter(fs))
            {
                foreach (MakefileRule rule in _rules)
                {
                    rule.Write(writer);
                }
            }
        }

        /// <summary>
        /// Add a rule.
        /// </summary>
        /// <param name="rule"></param>
        public void AddRule(MakefileRule rule)
        {
            this._rules.Add(rule);
        }

        /// <summary>
        /// Add a rule.
        /// </summary>
        /// <param name="target"></param>
        /// <param name="dependencies"></param>
        /// <param name="command"></param>
        public void AddRule(String target, IEnumerable<String> dependencies, String command)
        {
            MakefileRule rule = new MakefileRule(target, dependencies, command);
            AddRule(rule);
        }
        #endregion // Controller Methods
    }

} // RSG.Interop.Gnu namespace
