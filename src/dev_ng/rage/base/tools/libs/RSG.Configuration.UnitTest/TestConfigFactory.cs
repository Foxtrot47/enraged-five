﻿//---------------------------------------------------------------------------------------------
// <copyright file="TestConfigFactory.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Configuration.UnitTest
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using RSG.Base.Logging;
    using RSG.Base.Logging.Universal;
    using RSG.Configuration;
    
    /// <summary>
    /// Unit tests for the RSG.Configuration.ConfigFactory static class.
    /// </summary>
    [TestClass]
    public class TestConfigFactory
    {
        #region Member Data
        /// <summary>
        /// Log.
        /// </summary>
        IUniversalLog log;
        #endregion // Member Data

        /// <summary>
        /// Unit test setup method.
        /// </summary>
        [TestInitialize]
        public void TestSetup()
        {
            LogFactory.Initialize();
            this.log = LogFactory.CreateUniversalLog("TestConfigFactory");
        }

        /// <summary>
        /// Test ConfigFactory.CreateConfig.
        /// </summary>
        [TestMethod]
        public void TestCreateConfig()
        {
            IConfig configuration = ConfigFactory.CreateConfig(this.log);
            Assert.IsNotNull(configuration.Project);
            Assert.IsNotNull(configuration.CurrentStudio);
        }

        /// <summary>
        /// Test ConfigFactory.CreateProjectSummary.
        /// </summary>
        [TestMethod]
        public void TestCreateProjectSummary()
        {
            IEnumerable<IProjectSummary> projectSummaries = ConfigFactory.CreateProjectSummary(log);
            Assert.IsTrue(projectSummaries.Any());
        }
    }

} // RSG.Configuration.UnitTest namespace
