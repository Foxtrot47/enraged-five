#ifndef SKELETON_H
#define SKELETON_H

#include "parser/macros.h"
#include "parser/manager.h"

#include "atl/array.h"
#include "atl/string.h"

#include "vector\matrix34.h"

namespace rage
{

	enum AttributeType
	{
		FloatType,
		IntegerType,
		StringType,
		BoolType,
		UndefinedType,
		Point3Type,
		Matrix3Type
	};

// Bone class, This is the actual bone with a name and optional alias
class Bone
{
public:
	Bone();
	virtual ~Bone(){};

	void SetName(const char* name){ m_Name = name; };
	atString GetName(){ return m_Name; };

	void SetAlias(const char* alias){ m_Alias = alias; };
	atString GetAlias(){ return m_Alias; };

	void SetHash(u32 bonehash){ m_Hash = bonehash; } ;
	u32 GetHash(){ return m_Hash; };

private:
	atString m_Name;
	atString m_Alias;
	u32 m_Hash;

	PAR_PARSABLE;
};

// Bone instance class, This contains an instance of the bone along with its transform and which DOFs it is effecting.
class BoneInstance
{
public:
	BoneInstance();
	virtual ~BoneInstance();

	void SetTransform(const Matrix34& transform){ m_Transform = transform; };
	Matrix34& GetTransform(){ return m_Transform; };

	void SetTranslation(bool translation){ m_Translation = translation; };
	bool GetTranslation(){ return m_Translation; };

	void SetRotation(bool rotation){ m_Rotation = rotation; };
	bool GetRotation(){ return m_Rotation; };

	void SetScale(bool scale){ m_Scale = scale; };
	bool GetScale(){ return m_Scale; };

	void AddChild(const BoneInstance& bone){ m_Children.PushAndGrow(bone); };
	u32 GetNumChildren(){ return m_Children.GetCount(); };
	BoneInstance& GetChild(u32 idx){ return m_Children[idx]; };
Bone m_Bone;
private:
	
	Matrix34 m_Transform;
	bool m_Translation;
	bool m_Rotation;
	bool m_Scale;
	atArray< BoneInstance > m_Children;

	PAR_PARSABLE;
};

class BoneList
{
public:
	BoneList(){};
	virtual ~BoneList(){};

	bool Load(const char* filename);
	bool Save(const char* filename);

	atArray< Bone > m_Bones;

	PAR_PARSABLE;
};

// Skeleton class, Contains a hierarchy of bones
class Skeleton
{
public:
	Skeleton();
	virtual ~Skeleton();
	void SetRoot(BoneInstance& bone){ m_Root = bone; };
	BoneInstance* GetRoot(){ return &m_Root; };

	bool Load(const char* filename);
	bool Save(const char* filename);

	bool Skeleton::FindBone(u32 hash, BoneInstance& bone);
	bool FindBoneRecursive(u32 hash, BoneInstance& bone, BoneInstance& foundbone);

//private:
	u32 m_NumberOfBones;
	atString m_Name;
	BoneInstance m_Root;

	PAR_PARSABLE;
};

class AttributeValue
{
public:
	AttributeValue(){};
	virtual ~AttributeValue(){};

	u32 m_Type;
	atString m_Value;

	PAR_PARSABLE;
};

class BoneAttribute
{
public:
	BoneAttribute(){};
	virtual ~BoneAttribute(){};

	void AddAttribute(atHashString hashString, AttributeValue val);

//private:
	atString m_Name;
	u32 m_Hash;
	atMap<atHashString, AttributeValue> m_Attributes;

	PAR_PARSABLE;
};

class BoneAttributeList
{
public:
	BoneAttributeList(){};
	virtual ~BoneAttributeList(){};

	bool Load(const char* filename);
	bool Save(const char* filename);
	void Reset();

	void AddAttribute(BoneAttribute attr);
	void AddRootAttribute(atHashString hashString, AttributeValue val);

	atArray<BoneAttribute> m_Entries;
	atMap<atHashString, AttributeValue> m_RootAttributes;

	PAR_PARSABLE;
};

};

#endif