<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

  <structdef type="rage::Bone" autoregister="true">
	  <string name="m_Name" type="atString"/>
	  <string name="m_Alias" type="atString"/>
    <u32 name="m_Hash"/>
  </structdef>

  <structdef type="rage::BoneInstance" autoregister="true">
    <struct name="m_Bone" type="rage::Bone"/>
	  <Matrix34 name="m_Transform" />
	  <bool name="m_Translation" />
	  <bool name="m_Rotation" />
  	<bool name="m_Scale" />
    <array name="m_Children" type="atArray">
      <struct type="rage::BoneInstance"/>
    </array>
  </structdef>

  <structdef type="rage::Skeleton" autoregister="true">
    <u32 name="m_NumberOfBones"/>
    <string name="m_Name" type="atString"/>
    <struct name="m_Root" type="rage::BoneInstance"/>
  </structdef>

  <structdef type="rage::BoneList" autoregister="true">
    <array name="m_Bones" type="atArray">
      <struct type="rage::Bone"/>
    </array>
  </structdef>

  <structdef type="rage::AttributeValue" autoregister="true">
    <u32 name="m_Type"/>
    <string name="m_Value" type="atString"/>
  </structdef>

  <structdef type="rage::BoneAttribute" autoregister="true">
    <string name="m_Name" type="atString"/>
    <u32 name="m_Hash"/>
    <map name="m_Attributes" key="atHashString" type="atMap">
      <struct type="rage::AttributeValue"/>
    </map>
  </structdef>

  <structdef type="rage::BoneAttributeList" autoregister="true">
    <array name="m_Entries" type="atArray">
      <struct type="rage::BoneAttribute"/>
    </array>
    <map name="m_RootAttributes" key="atHashString" type="atMap">
      <struct type="rage::AttributeValue"/>
    </map>
  </structdef>

</ParserSchema>