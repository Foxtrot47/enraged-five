
#include "system/main.h"
#include "system/param.h"

#include "skeleton/skeleton.h"

int Main()
{
	::rage::sysParam::Init(0, NULL);
	INIT_PARSER;

	rage::Skeleton skel;

	rage::BoneInstance root;
	
	rage::BoneInstance child;
	root.AddChild(child);

	skel.SetRoot(root);

	skel.Save("x:/skeltest.skel");

	rage::Skeleton p;
	p = skel;

	return 0;
}