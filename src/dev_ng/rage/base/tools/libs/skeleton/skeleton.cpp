#include "skeleton.h"

#include "skeleton_parser.h"

namespace rage
{

Bone::Bone() : m_Alias(""),
				m_Name(""),
				m_Hash(0)
{}
//////////////////////////////////////////////////////////////////////////
BoneInstance::BoneInstance() : m_Rotation(false),
								m_Translation(false),
								m_Scale(false),
								m_Transform(Matrix34::IdentityType)
{
}
//////////////////////////////////////////////////////////////////////////
BoneInstance::~BoneInstance()
{
}
//////////////////////////////////////////////////////////////////////////
Skeleton::Skeleton() : m_NumberOfBones(0)
{
}
//////////////////////////////////////////////////////////////////////////
Skeleton::~Skeleton()
{
}
//////////////////////////////////////////////////////////////////////////
bool Skeleton::Load(const char* filename)
{
	if (PARSER.LoadObject(filename, "skel", *this))
	{
		return true;
	}

	return false;
}
//////////////////////////////////////////////////////////////////////////
bool Skeleton::Save(const char* filename)
{
	if (PARSER.SaveObject(filename, "skel", this))
	{
		return true;
	}

	return false;
}
//////////////////////////////////////////////////////////////////////////
bool Skeleton::FindBone(u32 hash, BoneInstance& bone)
{
	return FindBoneRecursive(hash, m_Root, bone);
}
//////////////////////////////////////////////////////////////////////////
bool Skeleton::FindBoneRecursive(u32 hash, BoneInstance& bone, BoneInstance& foundbone)
{
	if(bone.m_Bone.GetHash() == hash) 
	{
		foundbone = bone;
		return true;
	}

	bool result = false;
	for(int i=0; i < bone.GetNumChildren(); ++i)
	{
		result = FindBoneRecursive(hash, bone.GetChild(i), foundbone);
		if(result) break;
	}

	return result;
}
//////////////////////////////////////////////////////////////////////////
bool BoneList::Load(const char* filename)
{
	//if(ASSET.Exists(filename, ""))
	{
		if (PARSER.LoadObject(filename, "bonelist", *this))
		{
			return true;
		}
	}

	return false;
}
//////////////////////////////////////////////////////////////////////////
bool BoneList::Save(const char* filename)
{
	if (PARSER.SaveObject(filename, "bonelist", this))
	{
		return true;
	}

	return false;
}

//////////////////////////////////////////////////////////////////////////
bool BoneAttributeList::Load(const char* filename)
{
	//if(ASSET.Exists(filename, ""))
	{
		if (PARSER.LoadObject(filename, "", *this))
		{
			return true;
		}
	}

	return false;
}
//////////////////////////////////////////////////////////////////////////
void BoneAttributeList::AddRootAttribute(atHashString hashString, AttributeValue val)
{
	m_RootAttributes.Insert(hashString, val);
}
//////////////////////////////////////////////////////////////////////////
bool BoneAttributeList::Save(const char* filename)
{
	if (PARSER.SaveObject(filename, "", this))
	{
		return true;
	}

	return false;
}
//////////////////////////////////////////////////////////////////////////
void BoneAttribute::AddAttribute(atHashString hashString, AttributeValue val)
{
	m_Attributes.Insert(hashString, val);
}
//////////////////////////////////////////////////////////////////////////
void BoneAttributeList::AddAttribute(BoneAttribute attr)
{
	m_Entries.PushAndGrow(attr);
}
//////////////////////////////////////////////////////////////////////////
void BoneAttributeList::Reset()
{
	m_Entries.Reset();
}

};