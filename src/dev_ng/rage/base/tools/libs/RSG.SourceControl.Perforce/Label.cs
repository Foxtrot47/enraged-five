﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using P4API;
using System.IO;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Logging;
using RSG.SourceControl.Perforce.Util;
using System.Text.RegularExpressions;

namespace RSG.SourceControl.Perforce
{
    /// <summary>
    /// Representation of a perforce label.
    /// </summary>
    public class Label
    {
        #region Properties
        /// <summary>
        /// Label name.
        /// </summary>
        public string Name
        {
            get;
            private set;
        }

        /// <summary>
        /// Owner of the label.
        /// </summary>
        public string Owner
        {
            get;
            private set;
        }

        /// <summary>
        /// View of the label.
        /// </summary>
        public string[] View
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Create a new label from a p4 form.
        /// </summary>
        /// <param name="label"></param>
        public Label(P4Form labelRecord)
        {
            InitFromRecord(labelRecord);
        }
        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// Does a diff2 to compare the contents of this label to the other one to determine whether they are the same.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Matches(Label other, P4 p4)
        {
            if (other == null)
            {
                return false;
            }

            // Do some quick checks to start off with that check that the views are the same.
            if (this.View.Length != other.View.Length)
            {
                return false;
            }
            if (this.View.Except(other.View).Any() ||
                other.View.Except(this.View).Any())
            {
                return false;
            }

            // Check whether the files that are labelled are the same
            foreach (string singleView in this.View)
            {
                string thisPath = String.Format("{0}@{1}", singleView, this.Name);
                string otherPath = String.Format("{0}@{1}", singleView, other.Name);

                P4RecordSet rset = p4.Run("diff2", false, "-q", thisPath, otherPath);
                if ((null == rset) || rset.HasErrors())
                {
                    Log.Log__Error("Error during Perforce label status determination.");
                    p4.LogP4RecordSetMessages(rset);
                    return false;
                }
                else
                {
                    foreach (P4Record record in rset)
                    {
                        String status = RecordParser.GetStringField(record, "status");
                        if ((0 == String.Compare(status, "content")) ||
                            (0 == String.Compare(status, "types")))
                        {
                            int thisRev = RecordParser.GetIntField(record, "rev");
                            int otherRev = RecordParser.GetIntField(record, "rev2");

                            if (thisRev != otherRev)
                            {
                                return false;
                            }
                        }
                        else if ((0 == String.Compare(status, "right only")) ||
                            (0 == String.Compare(status, "left only")))
                        {
                            return false;
                        }
                    }
                }
            }

            // If we make it this far then the two labels are the same.
            return true;
        }
        #endregion // Public Methods

        #region Static Controller Methods
        /// <summary>
        /// Retrieves the label with the specified name.
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="labelName"></param>
        /// <returns></returns>
        public static Label GetLabel(P4 p4, string labelName)
        {
            Debug.Assert(p4.IsValidConnection(true, true));
            Label label = null;

            try
            {
                P4Form form = p4.Fetch_Form("label", labelName);
                return new Label(form);
            }
            catch (P4API.Exceptions.P4APIExceptions ex)
            {
                Log.Log__Exception(ex, "Perforce exception while retrieving existing label '{0}'.", labelName);
            }

            return label;
        }

        /// <summary>
        /// Retrieves the current tools version label.
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="incrementalLabelName"></param>
        /// <param name="maxLabelChecks">Maximum number of labels to check against
        /// (should only ever need 2, but default is 5 to be sure).</param>
        /// <returns></returns>
        public static Label GetCurrentToolsVersionLabel(P4 p4, String incrementalLabelName, String currentLabelName, int maxLabelChecks = 5)
        {
            Debug.Assert(p4.IsValidConnection(true, true));

            // Get the current label.
            RSG.SourceControl.Perforce.Label currentLabel = Label.GetLabel(p4, currentLabelName);

            // Check to see what the current highest label number is (if any exist).
            P4RecordSet labelRecords = p4.Run("labels", true, "-e", String.Format("{0}*", incrementalLabelName));

            IOrderedEnumerable<P4Record> orderedRecords =
                labelRecords.Records.OrderByDescending(item =>
                    {
                        String name = item["label"];
                        uint major, minor;
                        GetToolsLabelVersion(name, incrementalLabelName, out major, out minor);
                        uint val = ((major << 4) | minor);
                        return val;
                    });

            // Only try to match the last n number of labels.
            foreach (P4Record labelRecord in orderedRecords.Take(maxLabelChecks))
            {
                // Does this label match the current?
                Label versionLabel = GetLabel(p4, labelRecord["label"]);
                if (versionLabel.Matches(currentLabel, p4))
                {
                    return versionLabel;
                }
            }

            return null;
        }

        /// <summary>
        /// Creates a new label based on another label as a template
        /// </summary>
        /// <param name="labelName"></param>
        /// <param name="templateName"></param>
        public static Label Create(P4 p4, string labelName, string templateName)
        {
            Debug.Assert(p4.IsValidConnection(true, true));
            Label newLabel = null;

            try
            {
                P4Form form = p4.Fetch_Form("label", "-t", templateName, labelName);
                p4.Save_Form(form);
                newLabel = new Label(form);
            }
            catch (P4API.Exceptions.P4APIExceptions ex)
            {
            	Log.Log__Exception(ex, "Perforce exception while creating label '{0}' based on template label '{1}'.", labelName, templateName);
            }

            return newLabel;
        }

        /// <summary>
        /// Parses a versioned label name to extract the major/minor versions.
        /// </summary>
        public static bool GetToolsLabelVersion(String labelName, String incrementalLabelName, out uint majorVersion, out uint minorVersion)
        {
            majorVersion = 0;
            minorVersion = 0;

            // Create the regex that should match the label.
            Regex labelVersionRegex =
                new Regex(String.Format(@"^{0}(?<major>\d+)\.?(?<minor>\d?)$",
                    incrementalLabelName.Replace("[", @"\[").Replace("]", @"\]")));
            Match match = labelVersionRegex.Match(labelName);

            // Major version should always be present
            if (!match.Success || !match.Groups["major"].Success)
            {
                return false;
            }
            majorVersion = UInt32.Parse(match.Groups["major"].Value);

            // Minor version is optional
            if (match.Groups["minor"].Success && !String.IsNullOrEmpty(match.Groups["minor"].Value))
            {
                minorVersion = UInt32.Parse(match.Groups["minor"].Value);
            }
            return true;
        }
        #endregion // Static Controller Methods

        #region Private Method
        /// <summary>
        /// 
        /// </summary>
        /// <param name="labelRecord"></param>
        private void InitFromRecord(P4Form labelRecord)
        {
            Name = labelRecord["Label"];
            Owner = labelRecord["Owner"];
            View = labelRecord.ArrayFields["View"];
        }
        #endregion // Private Method
    } // Label
}
