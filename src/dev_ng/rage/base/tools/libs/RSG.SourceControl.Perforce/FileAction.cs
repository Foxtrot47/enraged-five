﻿using System;
using System.Reflection;
using RSG.SourceControl.Perforce.Attributes;

namespace RSG.SourceControl.Perforce
{

    /// <summary>
    /// Perforce file actions.
    /// </summary>
    /// Ref: http://www.perforce.com/perforce/doc.current/manuals/cmdref/fstat.html
    public enum FileAction
    {
        Unknown,

        [PerforceConstant("add")]
        Add,

        [PerforceConstant("edit")]
        Edit,

        [PerforceConstant("delete")]
        Delete,

        [PerforceConstant("branch")]
        Branch,

        [PerforceConstant("move/add")]
        MoveAndAdd,

        [PerforceConstant("move/delete")]
        MoveAndDelete,

        [PerforceConstant("integrate")]
        Integrate,

        [PerforceConstant("import")]
        Import,

        [PerforceConstant("purge")]
        Purge,

        [PerforceConstant("archive")]
        Archive,
    }

} // RSG.SourceControl.Perforce namespace
