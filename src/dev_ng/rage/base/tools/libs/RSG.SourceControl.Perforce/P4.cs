﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using RSG.Base.Logging;
using Microsoft.Win32;
using RSG.Base.Logging.Universal;
using RSG.Base.Windows.Dialogs;
using System.Text.RegularExpressions;
using RSG.SourceControl.Perforce.Util;
using System.Threading;

namespace RSG.SourceControl.Perforce
{

    /// <summary>
    /// Perforce wrapper class; handling the connection password fun and some
    /// logging wrappers around the Run method.
    /// </summary>
    /// Note: please do not bloat this class.  We will always need access to
    /// the core P4 API for efficiency.  Create additional helper methods in a
    /// helper class if you require (this must not end up like the current
    /// 3dsmax p4.ms script!).
    /// 
    /// For example helper classes see FileState and FileMapping.
    /// <seealso cref="FileState"/>
    /// <seealso cref="FileMapping"/>
    /// 
    public class P4 : P4API.P4Connection
    {
        #region Constants
        /// <summary>
        /// Registry key for Perforce environment settings.
        /// </summary>
        public static readonly String REG_USER_SET = "Software\\perforce\\environment";

        /// <summary>
        /// Regex to parse unparsed recordset for actual submitted CL
        /// - 'Change 4090289 renamed change 4090347 and submitted.'
        /// </summary>
        private static readonly Regex REGEX_SUBMITTED_CHANGELIST = new Regex(@"^Change\s*\d*\s*renamed\s*change\s*(\d*)\s*and\s*submitted\.\s*$");  

        #endregion // Constants

        #region Static Properties
        /// <summary>
        /// Static Perforce log object.
        /// </summary>
        public static IUniversalLog Log
        {
            get;
            private set;
        }

        private static DateTime TicketExpiry 
        { 
            get; 
            set; 
        }
        #endregion // Static Properties

        #region Properties
        
        #endregion // Properties
        #region Constructor(s)
        public P4()
            : base()
        {
            // This ensures we read the errors back from the P4RecordSet
            // rather than getting an exception and potentially blowing the stack.
            this.ExceptionLevel = P4API.P4ExceptionLevels.NoExceptionOnErrors;
        }

        /// <summary>
        /// Static constructor.
        /// </summary>
        static P4()
        {
            LogFactory.Initialize();
            P4.Log = LogFactory.CreateUniversalLog("Perforce");
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Connect to Perforce.
        /// </summary>
        /// This method is overridden to prompt for a password if the user 
        /// doesn't have a valid login session.
        /// 
        public new void Connect()
        {
            bool connected = false;
            int attempts = 0;
            do
            {
                P4.Log.Message("{0} [{1}]: connect", this.Port, this.User);

                P4API.P4ExceptionLevels currentLevel = ExceptionLevel;
                try
                {
                    base.Connect();

                    ExceptionLevel = P4API.P4ExceptionLevels.ExceptionOnBothErrorsAndWarnings;
                    
                    if (TicketExpiry == default(DateTime))
                    {
                        P4API.P4RecordSet res = this.Run("login", "-s");
                        TicketExpiry = DateTime.UtcNow.AddSeconds(int.Parse(res[0].Fields["TicketExpiration"]));
                    }
                    
                    if(TicketExpiry < DateTime.UtcNow)
                    {
                        PerformLogin();
                    }
                   
                    P4.Log.Message("Successfully connected to the Perforce server.");
                    connected = true;
                }
                catch
                {
                    if (TicketExpiry == default(DateTime) || TicketExpiry < DateTime.UtcNow)
                    {
                        PerformLogin();
                    }
                    else
                    {
                        P4.Log.Warning("Failed to connect but ticket is still valid. Is perforce having issues?");
                        Thread.Sleep(30000);
                    }
                }
                finally
                {
                    ExceptionLevel = currentLevel;
                }

                attempts++;
            }
            while (!connected && attempts < 10);
        }

        /// <summary>
        /// 
        /// </summary>
        private void PerformLogin()
        {
            try
            {
                string title = "Perforce Login";
                string message = "Please enter your Perforce login details below:";
                // MS Crasiness:
                // http://stackoverflow.com/questions/6005398/uriformatexception-invalid-uri-invalid-port-specified
                string s = System.IO.Packaging.PackUriHelper.UriSchemePack;
                Uri icon = new Uri("pack://application:,,,/RSG.SourceControl.Perforce;component/Data/p4logo.png");

                LoginViewModel vm = new LoginViewModel(null, title, message, icon);
#warning DHM FIX ME: we should probably use the Config Username here?
                vm.Username = Environment.GetEnvironmentVariable("USERNAME");
                vm.IsUsernameReadOnly = true;

                LoginWindow loginWindow = new LoginWindow(vm);
                if (loginWindow.ShowDialog() == true)
                {
                    P4.Log.Message("Logging into the server with user password...");
                    Login(vm.Password);
                    base.Connect();
                }
                else
                {
                    P4.Log.Error("Perforce login canceled by user.  We will not be connected to the server.");
                }
            }
            catch (P4API.Exceptions.P4APIExceptions ex)
            {
                P4.Log.Exception(ex, "Unhandled exception during Perforce connection ({0} [{1}]).",
                    this.Port, this.User);
            }
        }

        /// <summary>
        /// Uses the passed in password to attempt to connect to perforce
        /// </summary>
        /// <param name="password"></param>
        public void Connect(String password)
        {
            P4.Log.Message("{0} [{1}]: connect", this.Port, this.User);
            try
            {
                base.Connect();
                P4API.P4RecordSet res = this.Run("login", "-s");
            }
            catch
            {
                Login(password);
                base.Connect();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public String Run_Set(String key)
        {
            String value = null;
            RegistryKey rootNative = Registry.CurrentUser.OpenSubKey(REG_USER_SET);

            if (rootNative != null)
                value = (String)rootNative.GetValue(key);

            if (null == value)
            {
                rootNative = Registry.LocalMachine.OpenSubKey(REG_USER_SET);

                if (rootNative != null)
                    value = (String)rootNative.GetValue(key);
            }

            return value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void Run_Set(String key, String value)
        {
            RegistryKey rootNative = Registry.CurrentUser.CreateSubKey(REG_USER_SET);

            rootNative.SetValue(key, value);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public new P4API.P4RecordSet Run(String command, params String[] args )
        {
            StringBuilder arguments = new StringBuilder();
            foreach (String arg in args)
                arguments.AppendFormat("{0} ", arg);

            P4.Log.Message("P4 run: {0}, args: {1}.", command, arguments.ToString());
            return (base.Run(command, args));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <param name="log"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public P4API.P4RecordSet Run(String command, bool log, params String[] args)
        {
            StringBuilder arguments = new StringBuilder();
            foreach (String arg in args)
                arguments.AppendFormat("{0} ", arg);

            if (log)
                P4.Log.Message("P4 run: {0}, args: {1}.", command, arguments.ToString());

            return (base.Run(command, args));
        }
        
        /// <summary>
        /// Log Perforce RecordSet errors, warnings and messages to local log object.
        /// </summary>
        /// <param name="set"></param>
        public void LogP4RecordSetMessages(P4API.P4RecordSet set)
        {
            this.LogP4RecordSetMessages(P4.Log, set);
        }

        /// <summary>
        /// Log Perforce RecordSet errors, warnings and messages to a specified log object.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="set"></param>
        public void LogP4RecordSetMessages(IUniversalLog log, P4API.P4RecordSet set)
        {
            foreach (String msg in set.Errors)
                log.Error(msg);
            foreach (String msg in set.Warnings)
                log.Warning(msg);
            foreach (String msg in set.Messages)
                log.Message(msg);
        }

        /// <summary>
        /// Log Perforce UnParsedRecordSet errors, warnings and messages to local log object.
        /// </summary>
        /// <param name="set"></param>
        public void LogP4RecordSetMessages(P4API.P4UnParsedRecordSet set)
        {
            this.LogP4RecordSetMessages(P4.Log, set);
        }

        /// <summary>
        /// Log Perforce UnParsedRecordSet errors, warnings and messages to specified log object.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="set"></param>
        public void LogP4RecordSetMessages(IUniversalLog log, P4API.P4UnParsedRecordSet set)
        {
            foreach (String msg in set.Errors)
                log.Error(msg);
            foreach (String msg in set.Warnings)
                log.Warning(msg);
            foreach (String msg in set.Messages)
                log.Message(msg);
        }

        /// <summary>
        /// Get submitted changelist
        /// </summary>
        /// <param name="set"></param>
        /// <returns></returns>
        public int GetSubmittedChangelistNumber(P4API.P4UnParsedRecordSet set)
        {
            foreach (String msg in set.Messages)
            {
                Match match = REGEX_SUBMITTED_CHANGELIST.Match(msg);
                if (match.Success)
                {
                    int clNum;

                    if (int.TryParse(match.Groups[1].Value, out clNum))
                    {
                        return clNum;
                    }
                    else
                    {
                        P4.Log.Warning("Expected changlist number in unparsedrecordset string {0}", msg);
                    }
                }
            }
            return -1;
        }

        /// <summary>
        /// - Parses and returns depotFilenames.
        /// - Prints out pretty log output.
        /// - the recordset returned from a p4 run command can use this, is only useful when that recordset sets the 'depotFile' key.
        /// /// </summary>
        /// <param name="command">The p4 command that was run.</param>
        /// <param name="set">The recordset to parse</param>
        /// <param name="verbose">Log a new line for each file in the set</param>
        /// <returns>a list of the depot filenames contained in the recordset</returns>
        public IEnumerable<String> ParseAndLogRecordSetForDepotFilename(string command, P4API.P4RecordSet recordSet, bool verbose = true)
        {
            ISet<String> files = new HashSet<String>();

            foreach (P4API.P4Record record in recordSet)
            {
                String depotFilename = RecordParser.GetStringField(record, "depotFile");
                if (!String.IsNullOrEmpty(depotFilename))
                {
                    files.Add(depotFilename);
                }
            }

            P4.Log.Message("{0} {1} files.", command, files.Count);
            if (verbose)
            {
                foreach (String file in files)
                {
                    P4.Log.Message("{0} : {1} ", command, file);
                }
            }

            return files;
        }

        /// <summary>
        /// Check if a file exists in perforce.
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public bool Exists(String file)
        {
            P4API.P4RecordSet ret = this.Run("fstat", file);
            foreach (P4API.P4Record record in ret.Records)
            {
                if (record.Fields.ContainsKey("headAction") == false 
                    || record.Fields["headAction"].Equals("delete"))
                {
                    return false;
                }
            }
            return (ret.Records.Length > 0);
        }
        #endregion // Controller Methods
    }

} // RSG.SourceControl.Perforce namespace
