﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using RSG.SourceControl.Perforce.Attributes;
using RSG.SourceControl.Perforce.Util;

namespace RSG.SourceControl.Perforce
{
    
    /// <summary>
    /// Uber-fstat record parsing class; providing a model on top of fstat 
    /// records.
    /// </summary>
    /// Ref: http://www.perforce.com/perforce/doc.current/manuals/cmdref/fstat.html
    public class FileState
    {
        #region Properties
        /// <summary>
        /// Local path to file.
        /// </summary>
        public String ClientFilename
        {
            get;
            private set;
        }

        /// <summary>
        /// Depot path to file.
        /// </summary>
        public String DepotFilename
        {
            get;
            private set;
        }

        /// <summary>
        /// Set if mapped client file is synced.
        /// </summary>
        public bool IsMapped
        {
            get;
            private set;
        }
        
        /// <summary>
        /// Set if file is shelved.
        /// </summary>
        public bool IsShelved
        {
            get;
            private set;
        }

        /// <summary>
        /// Set if file is unresolved.
        /// </summary>
        public bool IsUnResolved
        {
            get;
            private set;
        }

        /// <summary>
        /// Set if file is resolved.
        /// </summary>
        public bool IsResolved
        {
            get;
            private set;
        }

        /// <summary>
        /// Action taken at head revision (if in depot).
        /// </summary>
        public FileAction HeadAction
        {
            get;
            private set;
        }

        /// <summary>
        /// Head revision changelist number (if in depot).
        /// </summary>
        public int HeadChange
        {
            get;
            private set;
        }

        /// <summary>
        /// Head revision number (if in depot).
        /// </summary>
        public int HeadRevision
        {
            get;
            private set;
        }

        /// <summary>
        /// Head revision changelist time (if in depot).
        /// </summary>
        public DateTime HeadChangelistTime
        {
            get;
            private set;
        }

        /// <summary>
        /// Head revision modification time (if in depot).
        /// </summary>
        public DateTime HeadModifiedTime
        {
            get;
            private set;
        }

        /// <summary>
        /// Head revision type (if in depot).
        /// </summary>
        public FileType HeadFileType
        {
            get;
            private set;
        }

        /// <summary>
        /// Revision last synced to client (if on client).
        /// </summary>
        public int HaveRevision
        {
            get;
            private set;
        }

        /// <summary>
        /// Open action (if opened on client).
        /// </summary>
        public FileAction OpenAction
        {
            get;
            private set;
        }

        /// <summary>
        /// Open file type (if opened on client).
        /// </summary>
        public FileType OpenFileType
        {
            get;
            private set;
        }

        /// <summary>
        /// Open changelist number (if opened on client).
        /// </summary>
        public uint Changelist
        {
            get;
            private set;
        }

        /// <summary>
        /// Whether the file is open by other users.
        /// </summary>
        public bool OtherOpen
        {
            get;
            private set;
        }

        /// <summary>
        /// Whether the file is locked by other users.
        /// </summary>
        public bool OtherLocked
        {
            get;
            private set;
        }

        /// <summary>
        /// Whether the file is locked by the current user.
        /// </summary>
        public bool Locked
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets an array of string objects containing the names of the other users that
        /// currently have this file checked out.
        /// </summary>
        public string[] OtherOpenUsers
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets an array of string objects containing the names of the other users that
        /// currently have this file locked.
        /// </summary>
        public string[] OtherLockedUsers
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor; from a fstat P4API.P4Record object.
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="fstat"></param>
        public FileState(P4API.P4Connection p4, P4API.P4Record fstat)
        {
            this.OtherOpenUsers = new string[0];
            this.OtherLockedUsers = new string[0];
            InitFromRecord(p4, fstat);
        }

        /// <summary>
        /// Constructor; from a filename.
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="filename"></param>
        public FileState(P4API.P4Connection p4, String filename)
        {
            this.OtherOpenUsers = new string[0];
            this.OtherLockedUsers = new string[0];
            Debug.Assert(p4.IsValidConnection(true, true));
            
            P4API.P4RecordSet recordSet = p4.Run("fstat", filename);
            if (recordSet.Errors.Length > 0)
            {
                this.OpenAction = FileAction.Unknown;
                this.HeadAction = FileAction.Unknown;
            }
            else
            {
                if (recordSet.Records.Length > 0)
                {
                    InitFromRecord(p4, recordSet.Records[0]);
                }
            }
        }
        #endregion // Constructor(s)

        #region Static Controller Methods
        /// <summary>
        /// Batch construction static method.
        /// </summary>
        /// <param name="filenames"></param>
        /// <returns>Array of FileState objects</returns>
        public static FileState[] Create(P4API.P4Connection p4, params String[] filenames)
        {
            if (0 == filenames.Length)
                throw new ArgumentException("No filenames specified.");

            Debug.Assert(p4.IsValidConnection(true, true));
            P4API.P4RecordSet recordSet = p4.Run("fstat", filenames);
            return (Create(p4, recordSet));
        }

        /// <summary>
        /// Batch construction static method.
        /// </summary>
        /// <param name="recordSet"></param>
        /// <returns>Array of FileState objects</returns>
        public static FileState[] Create(P4API.P4Connection p4, P4API.P4RecordSet recordSet)
        {
            List<FileState> fileStates = new List<FileState>();
            foreach (P4API.P4Record record in recordSet)
            {
                fileStates.Add(new FileState(p4, record));
            }
            return (fileStates.ToArray());
        }
        #endregion // Static Controller Methods

        #region Private Methods
        /// <summary>
        /// Initialise from a fstat P4API.P4Record object.
        /// </summary>
        /// <param name="fstat"></param>
        private void InitFromRecord(P4API.P4Connection p4, P4API.P4Record fstat)
        {
            this.ClientFilename = RecordParser.GetStringField(fstat, "clientFile");
            this.DepotFilename = RecordParser.GetStringField(fstat, "depotFile");
            this.IsMapped = RecordParser.IsFlagPresent(fstat, "isMapped");
            this.IsShelved = RecordParser.IsFlagPresent(fstat, "shelved");
            this.IsResolved = RecordParser.IsFlagPresent(fstat, "resolved");
            this.IsUnResolved = RecordParser.IsFlagPresent(fstat, "unresolved");

            String headAction = RecordParser.GetStringField(fstat, "headAction");
            this.HeadAction = PerforceConstantAttribute.GetEnumValueFromString(
                headAction, FileAction.Unknown);

            this.HeadChange = RecordParser.GetIntField(fstat, "headChange");
            this.HeadRevision = RecordParser.GetIntField(fstat, "headRev");
            this.HeadChangelistTime = RecordParser.GetDateTimeField(p4, fstat, "headTime", DateTime.MinValue);
            this.HeadModifiedTime = RecordParser.GetDateTimeField(p4, fstat, "headModTime", DateTime.MinValue);
            this.HeadFileType = new FileType(RecordParser.GetStringField(fstat, "headType"));
            this.HaveRevision = RecordParser.GetIntField(fstat, "haveRev");

            String openAction = RecordParser.GetStringField(fstat, "action");
            this.OpenAction = PerforceConstantAttribute.GetEnumValueFromString(
                openAction, FileAction.Unknown);
            
            this.OpenFileType = new FileType(RecordParser.GetStringField(fstat, "type"));
            this.Changelist = RecordParser.GetChangelistField(fstat, "change");
            this.OtherOpen = RecordParser.IsFlagPresent(fstat, "otherOpen");

            // The Perforce API doesn't give you info about other people 
            // locking the file if the lock is implied by the "+l" modifier.
            // Lets fix that.
            if (this.OtherOpen)
            {
                if (this.HeadFileType.Modifiers.HasFlag(FileModifier.ExclusiveCheckout))
                {
                    this.OtherLocked = true;
                }
                if (fstat.ArrayFields.ContainsKey("otherOpen"))
                    this.OtherOpenUsers = fstat.ArrayFields["otherOpen"].Clone() as string[];
            }
            else
            {
                this.OtherLocked = RecordParser.IsFlagPresent(fstat, "otherLock");
                if (this.OtherLocked && fstat.ArrayFields.ContainsKey("otherLock"))
                    this.OtherOpenUsers = fstat.ArrayFields["otherLock"].Clone() as string[];
            }

            this.Locked = RecordParser.IsFlagPresent(fstat, "ourLock");
        }
        #endregion // Private Methods
    }

} // RSG.SourceControl.Perforce namespace
