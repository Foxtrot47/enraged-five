﻿using System;
using System.Reflection;

namespace RSG.SourceControl.Perforce.Attributes
{ 

    /// <summary>
    /// Attribute that provides a way of tagging enums with String constants
    /// that appear in the Perforce API.
    /// </summary>
    public class PerforceConstantAttribute : Attribute
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public String Constant
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="constant"></param>
        public PerforceConstantAttribute(String constant)
        {
            this.Constant = constant;
        }
        #endregion // Constuctor(s)

        /// <summary>
        /// Return an enum value for the specified String; assuming the enum
        /// values are decorated with the PerforceConstant attribute.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="input"></param>
        /// <returns></returns>
        public static T GetEnumValueFromString<T>(String input, T defaultValue) 
            where T : struct, IConvertible
        {
            if (!typeof(T).IsEnum)
                throw new ArgumentException("Generic 'T' must be an enumerated type.");

            T[] values = (T[])Enum.GetValues(typeof(T));
            foreach (T value in values)
            {
                FieldInfo fi = typeof(T).GetField(value.ToString());
                PerforceConstantAttribute[] attributes =
                    fi.GetCustomAttributes(typeof(PerforceConstantAttribute), false) as PerforceConstantAttribute[];
                if (0 == attributes.Length)
                    continue; // Skip enum value (no attribute)

                String attribute = (attributes.Length > 0 ? attributes[0].Constant : "");
                if (0 == String.Compare(attribute, input))
                    return (value);
            }
            return (defaultValue);
        }

    }

} // RSG.SourceControl.Perforce.Attributes namespace
