﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace RSG.SourceControl.Perforce
{
    /// <summary>
    /// P4Web helper
    /// - provides methods to return the html hyperlinks to p4web served pages.
    /// </summary>
    public static class P4Web
    {
        #region Constants
        /// <summary>
        /// The Web port # used.
        /// </summary>
        private static readonly uint WEB_PORT = 8080;

        /// <summary>
        /// String replacement token
        /// - for identifying the CL number within a string.
        /// </summary>
        public static readonly String CHANGELIST_PREFIX = "CL:";
        #endregion // Constants

        #region Public Methods
        /// <summary>
        /// Replace Changelist number with a p4 web html Changelist link. 
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="cl"></param>
        /// <returns></returns>
        public static String ChangelistHyperLink(P4 p4, uint cl)
        {
            if (p4 != null)
            {
                // Redirect to the web port #.
                String server = p4.Port.Split(':').FirstOrDefault();
                if (server != null)
                {
                    return String.Format("<a title='p4Web' href='http://{1}:{2}/@md=d&amp;cd=@/{0}?ac=10'>{0}</a>", cl, server, WEB_PORT);
                }
            }
            return cl.ToString();
        }

        /// <summary>
        /// Return a string that replaces all changelist numbers ( prefixed by default with 'CL:' ) with a p4 web html Changelist link. 
        /// </summary>
        /// <param name="p4">The p4 object, it may be null.</param>
        /// <param name="sourceString">String to search & replace</param>
        /// <param name="ClPrefix">The optional prefix to a cl number</param>
        /// <returns></returns>
        public static String ChangelistHyperLinks(P4 p4, String sourceString, String ClPrefix = null)
        {
            if (ClPrefix == null)
            {
                ClPrefix = CHANGELIST_PREFIX;
            }

            Regex regex = new Regex(String.Format(@"(.*){0}([\d]+)(.*)", ClPrefix));

            do
            {
                Match match = regex.Match(sourceString);
                if (match.Success && match.Groups.Count == 4)
                {
                    String preface = match.Groups[1].Value;
                    String htmlCl = ChangelistHyperLink(p4, uint.Parse(match.Groups[2].Value));
                    String trailing = match.Groups[3].Value;

                    sourceString = String.Format(@"{0}{1}{2}", preface, htmlCl, trailing);
                    continue;
                }
                break;
            } while (true);

            return sourceString;
        }
        #endregion // Public Methods
    }
}
