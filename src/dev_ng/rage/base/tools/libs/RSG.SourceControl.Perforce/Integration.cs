﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Logging;

namespace RSG.SourceControl.Perforce
{
    /// <summary>
    /// Represents a P4 integration
    /// - WIP
    /// </summary>
    ///         
    [DataContract(Namespace = "")]
    public class Integration
    {
        #region Properties
        /// <summary>
        /// Branch Spec
        /// </summary>
        [DataMember]
        public String BranchSpec { get; set; }

        /// <summary>
        /// Options passed to p4 resolve
        /// </summary>
        [DataMember]
        public String P4ResolveOptions { get; set; }

        /// <summary>
        /// Options passed to p4 resolve for a particular regex match on the DEPOT file paths.
        /// A Regex for which the files it matches are resolved with the p4 resolve option as per the value in the dictionary.
        /// </summary>
        [DataMember]
        public IDictionary<String,String> P4RegexResolveOptions { get; set; }               

        /// <summary>
        /// Integrator Executable filenames to run with integration.
        /// </summary>
        [DataMember]
        public IEnumerable<String> IntegratorExecutables { get; set; }                      
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public Integration()
        {
        }

        /// <summary>
        /// Parameterised constructor
        /// </summary>
        public Integration(XElement elem)
        {
            Deserialise(elem);
        }

        /// <summary>
        /// ParameterisedConstructor
        /// </summary>
        /// <param name="src"></param>
        /// <param name="dst"></param>
        public Integration(String branchSpec, String p4ResolveOptions = "-am", IDictionary<String, String> p4RegexResolveOptions = null, IEnumerable<String> integratorExecutables = null)
        {
            this.BranchSpec = branchSpec;
            this.P4ResolveOptions = p4ResolveOptions;
            this.P4RegexResolveOptions = p4RegexResolveOptions;
            this.IntegratorExecutables = integratorExecutables;
        }
        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// Override ToString
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            StringBuilder result = new StringBuilder(String.Format("{0} ({1})", this.BranchSpec, this.P4ResolveOptions));
            if (this.P4RegexResolveOptions != null && this.P4RegexResolveOptions.Any())
            {
                result.AppendLine(String.Format("{0} : {1} special cases", result, this.P4RegexResolveOptions.Count()));
            }

            foreach (String integratorExecutable in IntegratorExecutables)
            {
                result.AppendLine(String.Format("Integrator Executable : {0}", integratorExecutable));
            }

            return result.ToString();
        }

        /// <summary>
        /// Serialise to XML.
        /// </summary>
        /// <returns></returns>
        public XElement Serialise()
        {
            XElement xmlIntegration = new XElement("Integration",
                new XAttribute("BranchSpec", this.BranchSpec),
                new XAttribute("P4ResolveOptions", this.P4ResolveOptions));

            if (this.P4RegexResolveOptions != null && this.P4RegexResolveOptions.Any())
            {
                XElement xmlRegex = new XElement("P4RegexResolveOptions");
                foreach (KeyValuePair<String,String> kvp in this.P4RegexResolveOptions)
                {
                    String regex = kvp.Key;
                    String resolveOptions = kvp.Value;
                    XElement xmlRegexKvp = new XElement("P4RegexResolveOption", 
                            new XAttribute("Regex", regex),
                            new XAttribute("P4ResolveOption", resolveOptions));
                    xmlRegex.Add(xmlRegexKvp);
                }

                xmlIntegration.Add(xmlRegex);
            }

            if (IntegratorExecutables.Any())
            {
                XElement xmlElemIntegratorExecutables = new XElement("IntegratorExecutables");
                foreach (String integratorExecutable in IntegratorExecutables)
                {
                    xmlElemIntegratorExecutables.Add(new XElement("IntegratorExecutable", integratorExecutable));
                }
                xmlIntegration.Add(xmlElemIntegratorExecutables);
            }

            return xmlIntegration;
        }

        /// <summary>
        /// Deserialise from XML.
        /// </summary>
        /// <param name="">xmlElem</param>
        /// <returns></returns>
        public bool Deserialise(XElement xmlElem)
        {
            try
            {
                this.BranchSpec = xmlElem.Attribute("BranchSpec").Value;
                this.P4ResolveOptions = xmlElem.Attribute("P4ResolveOptions").Value;
                this.P4RegexResolveOptions = new Dictionary<String, String>();
                foreach (XElement xelem in xmlElem.XPathSelectElements("P4RegexResolveOptions/P4RegexResolveOption"))
                {
                    XAttribute regexAttr = xelem.Attributes("Regex").FirstOrDefault();
                    XAttribute resolveAttr = xelem.Attributes("P4ResolveOption").FirstOrDefault();

                    if (regexAttr != default(XAttribute) && resolveAttr != default(XAttribute))
                    {
                        this.P4RegexResolveOptions.Add(regexAttr.Value, resolveAttr.Value);
                    }
                }

                ICollection<String> integratorExecutables = new List<String>();
                foreach (XElement xelem in xmlElem.XPathSelectElements("IntegratorExecutables/IntegratorExecutable"))
                {
                    integratorExecutables.Add(xelem.Value);
                }
                IntegratorExecutables = integratorExecutables;
            }
            catch (Exception ex)
            {
                Log.Log__Exception(ex, "Error deserialising Integration");
                return false;
            }
            return true;
        }
        #endregion // Public Methods
    }
}
