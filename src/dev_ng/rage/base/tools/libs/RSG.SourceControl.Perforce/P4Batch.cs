﻿using System;
using System.Collections.Generic;
using System.Linq;
using P4API;

namespace RSG.SourceControl.Perforce
{

    /// <summary>
    /// P4Batch static class; contains methods to help batch queries to the
    /// Perforce server; seriously use it.  It affects sateillite studios more
    /// as the Perforce server is typically local.
    /// </summary>
    /// Several of the methods also ensure that the depot paths are used; syncing
    /// local paths where they don't already exist locally fails to sync the
    /// files even if they exist in the workspace for example!!
    /// 
    public static class P4Batch
    {

        /// <summary>
        /// Run a Perforce command; ensuring depot paths are used.
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="cmd"></param>
        /// <param name="files"></param>
        /// <returns></returns>
        public static P4RecordSet Run(P4 p4, String cmd, String args, IEnumerable<String> files)
        {
            if (0 == files.Count())
                return (null);

            FileMapping[] fileMappings = FileMapping.Create(p4, files.ToArray());
            String[] fileList = fileMappings.Select(map => map.DepotFilename).ToArray();
            if (String.IsNullOrWhiteSpace(args))
            {
                return (p4.Run(cmd, fileList));
            }
            else
            {
                List<String> fullArgs = new List<String>();
                fullArgs.Add(args);
                fullArgs.AddRange(fileList);
                return (p4.Run(cmd, fullArgs.ToArray()));
            }
        }

        /// <summary>
        /// Run a Perforce command; ensuring depot paths are used.
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="cmd"></param>
        /// <param name="files"></param>
        /// <returns></returns>
        public static P4RecordSet Run(P4 p4, String cmd, String args, params String[] files)
        {
            return (Run(p4, cmd, args, files));
        }

    }

} // RSG.SourceControl.Perforce namespace
