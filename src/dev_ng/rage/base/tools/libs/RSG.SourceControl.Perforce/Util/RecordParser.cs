﻿using System;
using RSG.SourceControl.Perforce;

namespace RSG.SourceControl.Perforce.Util
{

    /// <summary>
    /// Collection of static methods to help parse P4Record structures.
    /// </summary>
    public static class RecordParser
    {
        /// <summary>
        /// Return whether a field is set to "1" in a record.
        /// </summary>
        /// <param name="record"></param>
        /// <param name="field"></param>
        /// <returns></returns>
        public static bool IsFlagSet(P4API.P4Record record, String field)
        {
            return (record.Fields.ContainsKey(field) && (int.Parse(record.Fields[field]) == 1 ? true : false));
        }

        /// <summary>
        /// Return whether a field is present in a record.
        /// </summary>
        /// <param name="record"></param>
        /// <param name="field"></param>
        /// <returns></returns>
        public static bool IsFlagPresent(P4API.P4Record record, String field)
        {
            return (record.Fields.ContainsKey(field));
        }

        /// <summary>
        /// Return integer field.
        /// </summary>
        /// <param name="record"></param>
        /// <param name="field"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static int GetIntField(P4API.P4Record record, String field, int defaultValue = 0)
        {
            if (record.Fields.ContainsKey(field))
            {
                int result;

                if (int.TryParse(record.Fields[field], out result))
                {
                    return result;
                }

                if (record.Fields[field].Equals("none"))
                {
                    return 0;
                }
            }
            return defaultValue;
        }

        /// <summary>
        /// Return changelist field.
        /// <remarks>handles the default changelist</remarks>
        /// </summary>
        /// <param name="record"></param>
        /// <param name="field"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static uint GetChangelistField(P4API.P4Record record, String field, uint defaultValue = 0)
        {
            if (record.Fields.ContainsKey(field))
            {
                uint result;

                if (record.Fields[field] != "default" &&
                    uint.TryParse(record.Fields[field], out result))
                {
                    return result;
                }
            }
            return defaultValue;
        }


        /// <summary>
        /// Return string field.
        /// </summary>
        /// <param name="record"></param>
        /// <param name="field"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static String GetStringField(P4API.P4Record record, String field, String defaultValue = "")
        {
            return (record.Fields.ContainsKey(field) ? record.Fields[field] : defaultValue);
        }

        /// <summary>
        /// Return a DateTime object for a time field.
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="record"></param>
        /// <param name="field"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static DateTime GetDateTimeField(P4API.P4Connection p4, P4API.P4Record record, String field, DateTime defaultValue)
        {
            String datetime = GetStringField(record, field);
            if (String.IsNullOrEmpty(datetime))
                return (defaultValue);
            
            int utc_ticks = int.Parse(datetime);
            return (p4.ConvertDate(utc_ticks));
        }
    }

} // RSG.SourceControl.Perforce.Util namespace
