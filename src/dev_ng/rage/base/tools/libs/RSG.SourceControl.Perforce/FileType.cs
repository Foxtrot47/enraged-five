﻿using System;
using System.Diagnostics;
using System.Reflection;
using RSG.SourceControl.Perforce.Attributes;

namespace RSG.SourceControl.Perforce
{
    /// <summary>
    /// Perforce filetype encapsulation.
    /// </summary>
    /// Ref: http://www.perforce.com/perforce/doc.current/manuals/cmdref/o.ftypes.html
    /// 
    public class FileType
    {
        #region Properties
        /// <summary>
        /// Core Perforce file type.
        /// </summary>
        public FileCoreType Core
        {
            get;
            private set;
        }

        /// <summary>
        /// File type modifiers.
        /// </summary>
        public FileModifier Modifiers
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor; parsing filetype string.
        /// </summary>
        /// <param name="filetype"></param>
        public FileType(String filetype)
        {
            this.Core = FileTypeUtils.FileCoreTypeFromString(filetype);
            this.Modifiers = FileTypeUtils.FileModifiersFromString(filetype);
        }

        /// <summary>
        /// Constructor; specifying components.
        /// </summary>
        /// <param name="core"></param>
        /// <param name="modifiers"></param>
        public FileType(FileCoreType core, FileModifier modifiers = FileModifier.None)
        {
            this.Core = core;
            this.Modifiers = modifiers;
        }
        #endregion // Constructor(s)
    }

    /// <summary>
    /// Core Perforce filetype enumeration.
    /// </summary>
    public enum FileCoreType
    {
        Unknown,

        [PerforceConstant("text")]
        Text,

        [PerforceConstant("binary")]
        Binary,

        [PerforceConstant("symlink")]
        Symlink,
        
        [PerforceConstant("apple")]
        Apple,

        [PerforceConstant("resource")]
        Resource,

        [PerforceConstant("unicode")]
        Unicode,

        [PerforceConstant("utf16")]
        UTF16,
    }

    /// <summary>
    /// Perforce modifier flags.
    /// </summary>
    [Flags]
    public enum FileModifier
    {
        None = 0,

        [PerforceConstant("m")]
        Modtime = 1,

        [PerforceConstant("w")]
        Writable = 2,

        [PerforceConstant("x")]
        Executable = 4,

        [PerforceConstant("k")]
        KeywordExpansion = 8,

        [PerforceConstant("l")]
        ExclusiveCheckout = 16,

        [PerforceConstant("C")]
        ServerStorageCompressedFilePerRevision = 32,

        [PerforceConstant("D")]
        ServerStorageDeltas = 64,

        [PerforceConstant("F")]
        ServerStorageFull = 128,

        [PerforceConstant("S")]
        ServerStorageNumberOfRevisions = 256,

        [PerforceConstant("X")]
        ServerArchiveTriggerForAccess = 512,
    }

    /// <summary>
    /// FileType extension methods.
    /// </summary>
    public static class FileTypeUtils
    {
        #region Constants
        /// <summary>
        /// Filetype string separator.
        /// </summary>
        private static char FILETYPE_SEPARATOR = '+';
        #endregion // Constants

        /// <summary>
        /// From a Perforce filetype string determine the FileCoreType.
        /// </summary>
        /// <param name="filetype"></param>
        /// <returns></returns>
        public static FileCoreType FileCoreTypeFromString(String filetype)
        {
            String[] parts = filetype.Split(new char[] { FILETYPE_SEPARATOR },
                StringSplitOptions.RemoveEmptyEntries);
            if (0 == parts.Length)
                return (FileCoreType.Unknown);

            FileCoreType result = PerforceConstantAttribute.GetEnumValueFromString(parts[0],
                FileCoreType.Unknown);
            return (result);
        }

        /// <summary>
        /// From a Perforce filetype string determine the FileModifier flags.
        /// </summary>
        /// <param name="filetype"></param>
        /// <returns></returns>
        public static FileModifier FileModifiersFromString(String filetype)
        {
            if (-1 == filetype.IndexOf(FILETYPE_SEPARATOR))
                return (FileModifier.None);

            FileModifier[] values = (FileModifier[])Enum.GetValues(typeof(FileModifier));
            FileModifier modifiers = FileModifier.None;
            String[] parts = filetype.Split(new char[] { FILETYPE_SEPARATOR },
                StringSplitOptions.RemoveEmptyEntries);
            if (0 == parts.Length)
                return (FileModifier.None);
            else
            {
                Debug.Assert(parts.Length <= 2);
                Debug.Assert(parts.Length > 0);

                String modifierString = parts[parts.Length-1];
                foreach (FileModifier value in values)
                {
                    FieldInfo fi = typeof(FileModifier).GetField(value.ToString());
                    PerforceConstantAttribute[] attributes =
                        fi.GetCustomAttributes(typeof(PerforceConstantAttribute), false) as PerforceConstantAttribute[];
                    if (0 == attributes.Length)
                        continue; // Skip enum value (no attribute)

                    String attribute = (attributes.Length > 0 ? attributes[0].Constant : "");
                    if (modifierString.Contains(attribute))
                    {
                        modifiers |= value; // Set modifier
                        modifierString = modifierString.Substring(attribute.Length - 1); // Chomp.
                    }
                }
            }
            return (modifiers);
        }
    }
    
} // RSG.SourceControl.Perforce
