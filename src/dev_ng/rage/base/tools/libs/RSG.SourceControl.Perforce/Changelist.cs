﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using RSG.Base.Extensions;
using RSG.SourceControl.Perforce.Attributes;
using RSG.SourceControl.Perforce.Util;

namespace RSG.SourceControl.Perforce
{
    /// <summary>
    /// 
    /// </summary>
    public static class ChangelistExtensions
    {
        /// <summary>
        /// Revert unchanged files before submitting a changelist.
        /// </summary>
        /// <param name="changelist"></param>
        /// <returns></returns>
        public static P4API.P4UnParsedRecordSet SubmitModified(this P4API.P4PendingChangelist changelist, P4 p4)
        {
            P4API.P4RecordSet ret = p4.Run("revert", "-a");
            return (changelist.Submit());
        }
    
    }

    /// <summary>
    /// Changelist state.
    /// </summary>
    public enum ChangelistState
    {
        [PerforceConstant("pending")]
        Pending,

        [PerforceConstant("submitted")]
        Submitted,
        
        [PerforceConstant("shelved")]
        Shelved,

        Unknown,
    }

    /// <summary>
    /// Changelist visibility.
    /// </summary>
    public enum ChangelistVisibility
    {
        [PerforceConstant("public")]
        Public,

        [PerforceConstant("restricted")]
        Restricted,

        Unknown,
    }

    /// <summary>
    /// Uber-changelist describe/changes abstraction class.
    /// </summary>
    public class Changelist
    {
        #region Properties
        /// <summary>
        /// Changelist number.
        /// </summary>
        public uint Number
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime Time
        {
            get;
            private set;
        }

        /// <summary>
        /// Changelist state.
        /// </summary>
        public ChangelistState Status
        {
            get;
            private set;
        }

        /// <summary>
        /// Changelist visibility.
        /// </summary>
        public ChangelistVisibility Visibility
        {
            get;
            private set;
        }

        /// <summary>
        /// Username owner of changelist.
        /// </summary>
        public String Username
        {
            get;
            private set;
        }

        /// <summary>
        /// User's client of the changelist.
        /// </summary>
        public String Client
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public String Path
        {
            get;
            private set;
        }

        /// <summary>
        /// Changelist description.
        /// </summary>
        public String Description
        {
            get;
            private set;
        }

        /// <summary>
        /// Changelist affected files.
        /// </summary>
        public IEnumerable<String> Files
        {
            get;
            private set;
        }

        /// <summary>
        /// Changelist file sizes.
        /// </summary>
        public IEnumerable<UInt32> FileSizes
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor; from a describe P4API.P4Record object.
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="changes"></param>
        public Changelist(P4 p4, P4API.P4Record changes)
        {
            InitFromRecord(p4, changes);
        }

        /// <summary>
        /// Constructor; from a changelist number.
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="changelist"></param>
        public Changelist(P4 p4, int changelist)
        {
            Debug.Assert(p4.IsValidConnection(true, true));

            P4API.P4RecordSet recordSet = p4.Run("describe", false,
                changelist.ToString());
            if (0 == recordSet.Errors.Length)
            {
                InitFromRecord(p4, recordSet.Records[0]);
            }
        }
        #endregion // Constructor(s)

        #region Static Controller Methods
        /// <summary>
        /// Batch construction static method.
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="changelists"></param>
        /// <returns>Array of Changelist objects</returns>
        public static Changelist[] Create(P4 p4, params int[] changelists)
        {
            Debug.Assert(p4.IsValidConnection(true, true));
            P4API.P4RecordSet recordSet = p4.Run("describe", false,
                changelists.Select(cl => cl.ToString()).ToArray());

            return (Create(p4, recordSet));
        }

        /// <summary>
        /// Batch construction static method; from "describe" record set.
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="recordSet"></param>
        /// <returns>Array of Changelist objects</returns>
        public static Changelist[] Create(P4 p4, P4API.P4RecordSet recordSet)
        {
            List<Changelist> changes = new List<Changelist>();
            foreach (P4API.P4Record record in recordSet)
            {
                changes.Add(new Changelist(p4, record));
            }
            return (changes.ToArray());
        }

        /// <summary>
        /// Batch construction static method; from "changes" record set.
        /// Additional queries are performed for file list population.
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="recordSet"></param>
        /// <returns></returns>
        public static Changelist[] CreateFromChangesRecords(P4 p4, P4API.P4RecordSet recordSet)
        {
            IEnumerable<int> changelists = recordSet.Records.Select(r => int.Parse(r.Fields["change"]));
            return (Create(p4, changelists.ToArray()));
        }

        /// <summary>
        /// Batch construction static method; from enumerable record sets.
        /// Additional queries are performed for file list population.
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="recordSets"></param>
        /// <returns></returns>
        public static Changelist[] CreateFromChangesRecords(P4 p4, IEnumerable<P4API.P4RecordSet> recordSets)
        {
            ICollection<int> allChangelists = new List<int>();
            foreach (P4API.P4RecordSet recordSet in recordSets)
            {
                IEnumerable<int> changelists = recordSet.Records.Select(r => int.Parse(r.Fields["change"]));
                allChangelists.AddRange(changelists);
            }
            return (Create(p4, allChangelists.ToArray()));
        }        

        #endregion // Static Controller Methods

        #region Private Methods
        /// <summary>
        /// Initialise from a describe P4Record.
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="describe"></param>
        private void InitFromRecord(P4 p4, P4API.P4Record describe)
        {
            this.Number = RecordParser.GetChangelistField(describe, "change");
            this.Time = RecordParser.GetDateTimeField(p4, describe, "time", DateTime.MinValue);
            this.Username = RecordParser.GetStringField(describe, "user");
            this.Client = RecordParser.GetStringField(describe, "client");
            this.Description = RecordParser.GetStringField(describe, "desc");
            this.Path = RecordParser.GetStringField(describe, "path");

            String status = RecordParser.GetStringField(describe, "status");
            this.Status = PerforceConstantAttribute.GetEnumValueFromString(status, ChangelistState.Unknown);
            String visibility = RecordParser.GetStringField(describe, "changeType");
            this.Visibility = PerforceConstantAttribute.GetEnumValueFromString(visibility, ChangelistVisibility.Unknown);

            List<String> fileList = new List<String>();
            List<UInt32> fileSizeList = new List<UInt32>();
            if (describe.ArrayFields.ContainsKey("depotFile"))
            {
                int depotFiles = describe.ArrayFields["depotFile"].Length;

                for (int i = 0; i < depotFiles; i++)
                {
                    String action = describe.ArrayFields["action"][i];

                    fileList.Add(describe.ArrayFields["depotFile"][i] as String);
                    if (describe.ArrayFields.ContainsKey("fileSize") &&
                        describe.ArrayFields["fileSize"].Length > i)
                    {
                        UInt32 size = 0;
                        UInt32.TryParse(describe.ArrayFields["fileSize"][i], out size);
                        fileSizeList.Add(size);
                    }
                    else
                    {
                        fileSizeList.Add(0);
                    }
                }
            }
            this.Files = fileList;
            this.FileSizes = fileSizeList;
        }
        #endregion // Private Methods
    }

} // RSG.SourceControl.Perforce
