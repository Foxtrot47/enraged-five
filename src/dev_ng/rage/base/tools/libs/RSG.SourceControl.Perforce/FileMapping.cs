﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using RSG.SourceControl.Perforce.Util;

namespace RSG.SourceControl.Perforce
{

    /// <summary>
    /// Uber-where record parsing class; providing a model on top of where.
    /// </summary>
    /// Ref: http://www.perforce.com/perforce/doc.current/manuals/cmdref/where.html
    public class FileMapping
    {
        #region Properties
        /// <summary>
        /// Whether this file is mapped into the local client workspace.
        /// </summary>
        public bool Mapped
        {
            get;
            private set;
        }

        /// <summary>
        /// Local path to file (should be used for local file operations; no
        /// ASCII character expansion).
        /// </summary>
        /// This path should be used with "p4 add" (no force).  Generally this
        /// should not be used with any other Perforce command.
        /// 
        public String LocalFilename
        {
            get;
            private set;
        }

        /// <summary>
        /// Local path to file (but Perforce-syntax with ASCII character 
        /// expansion).
        /// </summary>
        /// This path should be used with "p4 add -f"; or any other Perforce
        /// command.
        /// 
        public String ExpandedLocalFilename
        {
            get;
            private set;
        }

        /// <summary>
        /// Depot path to file.
        /// </summary>
        public String DepotFilename
        {
            get;
            private set;
        }

        /// <summary>
        /// Client path to file.
        /// </summary>
        public String ClientFilename
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor; from a fstat P4API.P4Record object.
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="record"></param>
        public FileMapping(P4API.P4Connection p4, P4API.P4Record record)
        {
            InitFromRecord(p4, record);
        }

        /// <summary>
        /// Constructor; from a filename.
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="filename"></param>
        internal FileMapping(P4API.P4Connection p4, String filename)
        {
            Debug.Assert(p4.IsValidConnection(true, true));

            P4API.P4RecordSet recordSet = p4.Run("where", filename);
            if (0 == recordSet.Errors.Length)
            {
                if (recordSet.Records.Length > 0)
                {
                    InitFromRecord(p4, recordSet.Records[0]);
                }
            }
        }
        #endregion // Constructor(s)
        
        #region Static Controller Methods
        /// <summary>
        /// Batch construction static method.
        /// </summary>
        /// <param name="filenames"></param>
        /// <returns>Array of FileMapping objects</returns>
        public static FileMapping[] Create(P4API.P4Connection p4, params String[] filenames)
        {
            if (null == p4)
                throw (new ArgumentNullException("p4"));
            if (null == filenames)
                throw (new ArgumentNullException("filenames"));
            if (0 == filenames.Length)
                throw new ArgumentException("No filenames specified.");

            Debug.Assert(p4.IsValidConnection(true, true));
            P4API.P4RecordSet recordSet = p4.Run("where", filenames);

            return (Create(p4, recordSet));
        }

        /// <summary>
        /// Batch construction static method.
        /// </summary>
        /// <param name="recordSet"></param>
        /// <returns>Array of FileMapping objects</returns>
        public static FileMapping[] Create(P4API.P4Connection p4, P4API.P4RecordSet recordSet)
        {
            List<FileMapping> fileStates = new List<FileMapping>();
            IEnumerable<IGrouping<String, P4API.P4Record>> mappings = 
                recordSet.Records.GroupBy(r => RecordParser.GetStringField(r, "path"));

            foreach (IGrouping<String, P4API.P4Record> record in mappings)
            {
                // If we have more than one record then we take the mapped
                // record (no "unmap" key).  Otherwise just include it.
                int recordCount = record.Count();
                if (recordCount > 1)
                {
                    // Prefer a mapped entry over an unmapped entry.
                    P4API.P4Record rec = record.FirstOrDefault(r => !RecordParser.IsFlagPresent(r, "unmap"));
                    if (null == rec)
                        rec = record.First(); // But unmapped entry better than none.
 
                    fileStates.Add(new FileMapping(p4, rec));
                }
                else if (1 == recordCount)
                {
                    fileStates.Add(new FileMapping(p4, record.First()));
                }
            }
            return (fileStates.ToArray());
        }
        #endregion // Static Controller Methods

        #region Private Methods
        /// <summary>
        /// Initialise from a where P4API.P4Record object.
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="where"></param>
        private void InitFromRecord(P4API.P4Connection p4, P4API.P4Record where)
        {
            this.Mapped = !RecordParser.IsFlagPresent(where, "unmap");
            this.LocalFilename = RecordParser.GetStringField(where, "path");
            this.ExpandedLocalFilename = AsciiExpansion.Expand(this.LocalFilename);
            this.DepotFilename = RecordParser.GetStringField(where, "depotFile");
            this.ClientFilename = RecordParser.GetStringField(where, "clientFile");
        }
        #endregion // Private Methods
    }

} // RSG.SourceControl.Perforce namespace
