﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Extensions;
using P4API;
using RSG.SourceControl.Perforce.Util;
using RSG.Base.Logging.Universal;

namespace RSG.SourceControl.Perforce.Extensions
{
    public static class P4Extensions
    {
        #region Properties
        /// <summary>
        /// Cache used for interchanges.
        /// - Dictionary is keyed on branchname.
        /// - KVP Key is the LowestCl Number (The lower changelist number that interchanges can use in a revision spec. )
        /// - KVP Value is the last date time a branch was seen to be modified when we set the lower revision limit.
        /// </summary>
        private static Dictionary<String, KeyValuePair<uint, DateTime>> InterchangesCache = new Dictionary<String, KeyValuePair<uint, DateTime>>();
        #endregion // Properties

        #region Public Methods
        /// <summary>
        /// Runs the changes command in chunks
        /// </summary>
        /// <param name="chunkSize"></param>
        /// <param name="paths"></param>
        /// <returns></returns>
        public static IEnumerable<P4API.P4RecordSet> ChangesChunked(this P4 p4, IEnumerable<string> paths, int chunkSize)
        {
            ICollection<P4API.P4RecordSet> recordSets = new List<P4API.P4RecordSet>();
            IEnumerable<IEnumerable<string>> pathChunks = paths.Chunk(chunkSize);

            foreach (IEnumerable<String> pathChunk in pathChunks)
            {
                P4API.P4RecordSet records = p4.Run("changes", pathChunk.ToArray());
                if (records.Records.Count() > 0)
                {
                    recordSets.Add(records);
                }
            }

            // DW: maybe I should flatten the recordsets? - although is it safe to assume we know how to construct a P4API.P4RecordSet object? - I think not.

            return recordSets;
        }

        /// <summary>
        /// Returns true if the branchspec has updated.
        /// - piggybacks the InterchangesCache
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="branchSpecName"></param>
        /// <returns>true if the branchspec is updated, since the last time the cache was updated</returns>
        public static DateTime GetBranchSpecModifiedTime(this P4 p4, String branchSpecName, bool updateCacheModifiedTime)
        {
            // Get the branchspec details ( specifically we are looking for the last updated time )
            P4API.P4RecordSet recordsBranches = p4.Run("branches", "-t", "-e", branchSpecName);
            P4Record rec = recordsBranches.Records.Where(r => r.Fields.ContainsKey("Update")).FirstOrDefault();
            if (rec == default(P4Record))
            {
                throw new Exception(String.Format("Could not derive 'update time' for branch {0}", branchSpecName));
            }

            // This is the actual branchspec modified time
            DateTime modified = RecordParser.GetDateTimeField(p4, rec, "Update", DateTime.MinValue).ToUniversalTime();

            if (!InterchangesCache.ContainsKey(branchSpecName)) 
            {
                // Invalidate cache - don't know about this branchspec in our cache.
                DateTime dateTimeModified = updateCacheModifiedTime ? modified : DateTime.MinValue.ToUniversalTime();
                InterchangesCache[branchSpecName] = new KeyValuePair<uint, DateTime>(0, modified);
            }
            else if (InterchangesCache[branchSpecName].Value != modified)
            {
                // We know about this branchspec and it has been genuinely updated by a user - we have to reset the cache.
                DateTime dateTimeModified = updateCacheModifiedTime ? modified : InterchangesCache[branchSpecName].Value;
                InterchangesCache[branchSpecName] = new KeyValuePair<uint, DateTime>(0, dateTimeModified);
            }

            return modified;
        }

        /// <summary>
        /// Go direct to cache - this does not neccessarily reflect the true modified times of the branchspec
        /// - this is useful to determine if a branchspec has been updated, not in terms of the actual branchspec which can chnage at any time
        /// but retrieve the value from the cache so we can compare that against the actual branchspec modified time.
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="branchSpecName"></param>
        /// <returns></returns>
        public static DateTime GetBranchSpecModifiedTimeFromCache(this P4 p4, String branchSpecName)
        {
            if (InterchangesCache.ContainsKey(branchSpecName))
            {
                return InterchangesCache[branchSpecName].Value;
            }

            return DateTime.MinValue.ToUniversalTime();
        }

        /// <summary>
        /// returns 'p4 interchanges' using a lower CL revision cache to accelerate and reduce this expensive operation.
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="log"></param>
        /// <param name="branchSpecName"></param>
        /// <param name="branchSpecUpdated">set if the branchspec updated.</param>
        /// <param name="updateModifiedTime">set if the branchspec updated.</param>
        /// <returns>records as returned by p4 interchanges</returns>
        public static P4API.P4RecordSet InterchangesCached(this P4 p4, IUniversalLog log, String branchSpecName, bool updateModifiedTime = false)
        {
            try
            {
                // reset cache with branchspec modified time if required.
                DateTime lastModified = p4.GetBranchSpecModifiedTime(branchSpecName, updateModifiedTime);

                // Read cache
                uint currentLowerCLRevision = InterchangesCache[branchSpecName].Key;
                DateTime lastTimeBranchSpecUpdated = InterchangesCache[branchSpecName].Value;

                // Get the last change submitted in the depot, this gives us an upper limit of CLs - knowing this 
                // we can set our lower CL number to the #head thus making the CL revision range as tight as possible.
                // we can also use this to know that there have been no new changes to check for since we last returned 
                // no changes from interchanges.
                P4API.P4RecordSet recordChanges = p4.Run("changes", "-s", "submitted", "-m1");

                // DW: this is very bad and unexplained - yet has been seen to occur maybe about once a day - so haven't caught this in debugger yet.
                // so it seems sometimes no records are returned?!
                IEnumerable<uint> changes = recordChanges.Records.Any() ? recordChanges.Records.Select(r => uint.Parse(r.Fields["change"])) : new List<uint>();
                if (!changes.Any())
                {
                    // best action to take is to ignore the cache completely, yet what remains in the cache should be good so we don't touch it.
                    P4API.P4RecordSet recs = p4.Run("interchanges", "-b", branchSpecName, String.Format("//...@{0},#head", currentLowerCLRevision));
                    return recs;
                }

                uint highestCLNumber = changes.OrderBy(c => c).Last();

                if (currentLowerCLRevision > highestCLNumber)
                {
                    // we know there are no interchanges - we have checked before and we found no results
                    // once changes move on we will start checking from a valid range again.
                    return null;
                }

                // And now this is the expensive operation that we are optimising by specfiying a tight CL revision limit.
                P4API.P4RecordSet records = p4.Run("interchanges", "-b", branchSpecName, String.Format("//...@{0},#head", currentLowerCLRevision));
                uint lowestCLNumber = records.Records.Select(r => uint.Parse(r.Fields["change"])).OrderBy(c => c).FirstOrDefault();

                // nothing returned from interchanges
                if (lowestCLNumber == default(uint))
                {
                    // Set cache to highest CL + 1
                    // *** note +1 <---- *** we have no requirement to search for changes from the highest CL number again because it wasn't returning any results for interchanges.
                    // so we are setting the lowest CL number to be the highest +1 and that can be specifically checked for to save requirement to call interchanges whatsoever 
                    // ( see above where we return null currentLowerCLRevision > highestCLNumber )
                    InterchangesCache[branchSpecName] = new KeyValuePair<uint, DateTime>(highestCLNumber + 1, lastTimeBranchSpecUpdated);
                    return null;
                }

                // Set cache so the lowest CL revision is now stored for future use.
                InterchangesCache[branchSpecName] = new KeyValuePair<uint, DateTime>(lowestCLNumber, lastTimeBranchSpecUpdated);

                // return the p4 interchanges results we desired.
                return records;
            }
            catch (Exception ex)
            {
                log.ToolException(ex, "Something went wrong when running InterchangesCached");
                return null;
            }            
        }
        #endregion // Public Methods
    }
}
