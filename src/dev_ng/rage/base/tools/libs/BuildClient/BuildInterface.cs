﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace AutomatedBuildClient
{
    public partial class BuildInterface : Form
    {
        public BuildInterface()
        {
            InitializeComponent();
        }

        private delegate void InvokeOutputReceived(string output);

        public void OnOutputReceived(string output)
        {
            if (outputTextBox.InvokeRequired == true)
            {
                InvokeOutputReceived invoke = delegate(string invokeOutput)
                {
                    this.OnOutputReceived(invokeOutput);
                };

                object[] args = new object[1];
                args[0] = output;
                outputTextBox.Invoke(invoke, args);
            }
            else
            {
                outputTextBox.AppendText(output);
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
