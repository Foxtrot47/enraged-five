﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using System.Text;
using System.Threading;
using AutomatedBuildShared;

namespace AutomatedBuildClient
{
    class Program
    {
        static BuildInterface m_InterfaceForm = new BuildInterface();
        static bool m_Exiting = false;

        static void ListenThread(object client)
        {
            BuildClient buildClient = (BuildClient)client;
            while (m_Exiting != true)
            {
                if (buildClient.Listen() == false)
                {
                    break;
                }

                if (buildClient.IsConnected() == true)
                {
                    Thread.Sleep(1000);
                }

                if (buildClient.Active == false)
                    buildClient.Disconnect();
            }
        }

        static int Main(string[] args)
        {
            string buildClientsPath = null;
            if (args.Length == 2)
            {
                if ( String.Compare(args[0], "-client", true) == 0 )
                {
                    buildClientsPath = args[1];

                    if (File.Exists(buildClientsPath) == false)
                    {
                        Console.WriteLine("Client definition file: " + buildClientsPath + " could not be found!");
                        return -1;
                    }
                }
                else
                {
                    Console.WriteLine("Unknown argument: " + args[0] + ".");
                    return -1;
                }
            }
            else if (args.Length > 0)
            {
                buildClientsPath = Environment.ExpandEnvironmentVariables("%RS_TOOLSROOT%\\etc\\CruiseControl\\AutomatedBuild\\clients\\clients.xml");

                if (File.Exists(buildClientsPath) == false)
                {
                    Console.WriteLine("Unknown arguments.  Usage: " + Path.GetFileName(Application.ExecutablePath) + " -client <client definition XML path>.");
                    return -1;
                }
            }    

            BuildClient client = BuildClients.GetBuildClient(Environment.MachineName, buildClientsPath);
            if (client == null || client.IsValid() == false)
            {
                //TODO: See about creating a local entry here.  

                Console.WriteLine("Unable to create a build client for this machine!");
                return -1;
            }

            //Register all the tasks.
            BuildTask.RegisterTasks();

            Thread listenThread = new Thread(ListenThread);
            listenThread.Name = "Automated Build Client Listener Thread";
            listenThread.Start(client);

            //Start the user interface to hook into the BuildClient's output.
            
            client.OutputReceived += m_InterfaceForm.OnOutputReceived;
            m_InterfaceForm.ShowDialog();

            //The user interface has been closed; disconnect and exit gracefully.
            client.Close();

            return 0;
        }
    }
}
