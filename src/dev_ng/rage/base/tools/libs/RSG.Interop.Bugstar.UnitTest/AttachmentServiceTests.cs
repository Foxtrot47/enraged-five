﻿//---------------------------------------------------------------------------------------------
// <copyright file="AttachmentServiceTests.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Bugstar.UnitTest
{
    using System;
    using System.IO;
    using System.Linq;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using RSG.Base.Net;
    using RSG.Interop.Bugstar;
    using RSG.Interop.Bugstar.Organisation;

    /// <summary>
    /// Bugstar Attachment service tests.
    /// </summary>
    [TestClass]
    public class AttachmentServiceTests
    {
        /// <summary>
        /// Unit test common initialisation.
        /// </summary>
        [TestInitialize]
        public void Initialise()
        {
            RSG.Base.Logging.LogFactory.Initialize();
        }

        /// <summary>
        /// Test valid .
        /// </summary>
        [TestMethod]
        public void TestAttachmentDownload()
        {
            BugstarConnection connection = new BugstarConnection(
                new Uri("https://rest.bugstar.rockstargames.com:8443/BugstarRestService-1.0/rest"),
                new Uri("https://attachments.bugstar.rockstargames.com:8443/AttachmentService-1.0"));
            Assert.IsFalse(String.IsNullOrEmpty(connection.AttachmentService.ToString()));
            Assert.IsFalse(connection.IsReadOnly);
            Assert.IsFalse(String.IsNullOrEmpty(connection.RESTService.ToString()));

            // Login using service account.
            connection.Login("gamestats", "game5tats", "");
            
            Project[] projects = Project.GetProjects(connection);
            Assert.IsTrue(projects.Length > 0);

            Project activeProject = projects.FirstOrDefault(p => p.Name.Equals("GTA 5 DLC"));
            Assert.IsNotNull(activeProject);

            Bug usefulBug = Bug.GetBugById(activeProject, 1935911);

            foreach (Attachment attachment in usefulBug.AttachmentList)
            {
                using (System.IO.Stream s = attachment.Download())
                {
                    Assert.IsTrue(s.Length > 0);
                }

                String filename = Path.Combine(Environment.ExpandEnvironmentVariables("%TEMP%"), "test.doc");
                Assert.IsTrue(attachment.Download(filename));
                Assert.IsTrue(File.Exists(filename));
            }
        }
    }

} // RSG.Interop.Bugstar.UnitTest namespace
