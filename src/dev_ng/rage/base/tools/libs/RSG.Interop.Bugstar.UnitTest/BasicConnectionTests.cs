﻿//---------------------------------------------------------------------------------------------
// <copyright file="BasicConnectionTests.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Bugstar.UnitTest
{
    using System;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using RSG.Base.Net;
    using RSG.Interop.Bugstar;
    using RSG.Interop.Bugstar.Organisation;

    /// <summary>
    /// Basic Bugstar connection tests.
    /// </summary>
    [TestClass]
    public class BasicConnectionTests
    {
        /// <summary>
        /// Unit test common initialisation.
        /// </summary>
        [TestInitialize]
        public void Initialise()
        {
            RSG.Base.Logging.LogFactory.Initialize();
        }

        /// <summary>
        /// Test valid connection services and simply query using service account.
        /// </summary>
        [TestMethod]
        public void TestValidServiceUrisAndSimplyQuery()
        {
            BugstarConnection connection = new BugstarConnection(
                new Uri("https://rest.bugstar.rockstargames.com:8443/BugstarRestService-1.0/rest"),
                new Uri("https://attachments.bugstar.rockstargames.com:8443/AttachmentService-1.0"));
            Assert.IsFalse(String.IsNullOrEmpty(connection.AttachmentService.ToString()));
            Assert.IsFalse(connection.IsReadOnly);
            Assert.IsFalse(String.IsNullOrEmpty(connection.RESTService.ToString()));

            // Login using service account.
            connection.Login("gamestats", "game5tats", "");

            Project[] projects = Project.GetProjects(connection);
            Assert.IsTrue(projects.Length > 0);
        }

        /// <summary>
        /// Test for invalid login case.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(InvalidLoginException))]
        public void TestInvalidLogin()
        {
            BugstarConnection connection = new BugstarConnection(
                new Uri("https://rest.bugstar.rockstargames.com:8443/BugstarRestService-1.0/rest"),
                new Uri("https://attachments.bugstar.rockstargames.com:8443/AttachmentService-1.0"));
            Assert.IsFalse(String.IsNullOrEmpty(connection.AttachmentService.ToString()));
            Assert.IsFalse(connection.IsReadOnly);
            Assert.IsFalse(String.IsNullOrEmpty(connection.RESTService.ToString()));

            // Login using service account.
            connection.Login("dave", "dave", "dave");
        }

        /// <summary>
        /// Test for invalid service Uris.
        /// </summary>
        [TestMethod]
        // DHM FIX ME: interop assembly should pass up query errors
        //[ExpectedException(BugstarQueryException)]
        public void TestInvalidServiceUris()
        {
            BugstarConnection connection = new BugstarConnection(new Uri("http://dave:8000"), new Uri("http://dave:8001"));
            Assert.IsFalse(String.IsNullOrEmpty(connection.AttachmentService.ToString()));
            Assert.IsFalse(connection.IsReadOnly);
            Assert.IsFalse(String.IsNullOrEmpty(connection.RESTService.ToString()));

            // This should really raise an exception.
            Project[] projects = Project.GetProjects(connection);
            Assert.AreEqual(0, projects.Length);
        }
    }

} // RSG.Interop.Bugstar.UnitTest namespace
