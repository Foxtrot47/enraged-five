#include "MaterialPreset.h"
#include "materialpreset_parser.h"
#include "parser/manager.h"

//NOTE:  This matches the parser's generated array for enumerations.
//This is a hack used to generate the surface presets from the templates.
//
const char* eTextureMapType_Strings[] = {
	"kTypeUnknown",
	"kTypeDiffuse",
	"kTypeNormal",
	"kTypeSpecular",
	"kTypeTintPalette",
	"kTypeDetail",
	NULL
};

TypeBase::TypeBase()
{

}

MaterialPreset::MaterialPreset(const char* parent, const char* shaderName, const char* friendlyName, const ePresetLevel presetLevel) 
: m_Parent(parent), m_ShaderName(shaderName), m_FriendlyName(friendlyName), m_PresetLevel(presetLevel)
{
	
};

const TexMapType* MaterialPreset::GetTexMap(int varIndex) const 
{
	for(int texMapIndex = 0; texMapIndex < m_TexMaps.GetCount(); ++texMapIndex)
	{
		if ( m_TexMaps[texMapIndex].GetIndex() == varIndex ) 
			return &(m_TexMaps[texMapIndex]);
	}

	return NULL;
}

const Vector4Type* MaterialPreset::GetVector4(int varIndex) const 
{
	for(int vectorIndex = 0; vectorIndex < m_Vec4s.GetCount(); ++vectorIndex)
	{
		if ( m_Vec4s[vectorIndex].GetIndex() == varIndex ) 
			return &(m_Vec4s[vectorIndex]);
	}

	return NULL;
}

const Vector3Type* MaterialPreset::GetVector3(int varIndex) const 
{
	for(int vectorIndex = 0; vectorIndex < m_Vec3s.GetCount(); ++vectorIndex)
	{
		if ( m_Vec3s[vectorIndex].GetIndex() == varIndex ) 
			return &(m_Vec3s[vectorIndex]);
	}

	return NULL;
}

const Vector2Type* MaterialPreset::GetVector2(int varIndex) const 
{
	for(int vectorIndex = 0; vectorIndex < m_Vec2s.GetCount(); ++vectorIndex)
	{
		if ( m_Vec2s[vectorIndex].GetIndex() == varIndex ) 
			return &(m_Vec2s[vectorIndex]);
	}

	return NULL;
}

const FloatType* MaterialPreset::GetFloat(int varIndex) const 
{
	for(int floatIndex = 0; floatIndex < m_Floats.GetCount(); ++floatIndex)
	{
		if ( m_Floats[floatIndex].GetIndex() == varIndex ) 
			return &(m_Floats[floatIndex]);
	}

	return NULL;
}

const IntType* MaterialPreset::GetInt(int varIndex) const 
{
	for(int intIndex = 0; intIndex < m_Ints.GetCount(); ++intIndex)
	{
		if ( m_Ints[intIndex].GetIndex() == varIndex ) 
			return &(m_Ints[intIndex]);
	}

	return NULL;
}

bool MaterialPreset::FindTexMap(const atString& name, TexMapType*& texMap, bool findVarName) const
{
	for(int index = 0; index < GetTexMaps().GetCount(); ++index)
	{
		if (findVarName)
		{
			if ( stricmp(m_TexMaps[index].GetVariableName().c_str(), name) == 0)
			{
				texMap = (TexMapType*) &(m_TexMaps[index]);
				return true;
			}
		}
		else
		{
			if ( stricmp(m_TexMaps[index].GetName().c_str(), name) == 0)
			{
				texMap = (TexMapType*) &(m_TexMaps[index]);
				return true;
			}
		}
	}

	return false;
}

bool MaterialPreset::FindVector4(const atString& name, Vector4Type*& vector4Type, bool findVarName) const
{
	for(int index = 0; index < GetVector4s().GetCount(); ++index)
	{
		if (findVarName)
		{
			if ( stricmp(m_Vec4s[index].GetVariableName().c_str(), name) == 0)
			{
				vector4Type = (Vector4Type*) &(m_Vec4s[index]);
				return true;
			}
		}
		else
		{
			if ( stricmp(m_Vec4s[index].GetName().c_str(), name) == 0)
			{
				vector4Type = (Vector4Type*) &(m_Vec4s[index]);
				return true;
			}
		}
	}

	return false;
}

bool MaterialPreset::FindVector3(const atString& name, Vector3Type*& vector3Type, bool findVarName) const
{
	for(int index = 0; index < GetVector3s().GetCount(); ++index)
	{
		if (findVarName)
		{
			if ( stricmp(m_Vec3s[index].GetVariableName().c_str(), name) == 0)
			{
				vector3Type = (Vector3Type*) &(m_Vec3s[index]);
				return true;
			}
		}
		else
		{
			if ( stricmp(m_Vec3s[index].GetName().c_str(), name) == 0)
			{
				vector3Type = (Vector3Type*) &(m_Vec3s[index]);
				return true;
			}
		}
	}

	return false;
}

bool MaterialPreset::FindVector2(const atString& name, Vector2Type*& vector2Type, bool findVarName) const
{
	for(int index = 0; index < GetVector2s().GetCount(); ++index)
	{
		if (findVarName)
		{
			if ( stricmp(m_Vec2s[index].GetVariableName().c_str(), name) == 0)
			{
				vector2Type = (Vector2Type*) &(m_Vec2s[index]);
				return true;
			}
		}
		else
		{
			if ( stricmp(m_Vec2s[index].GetName().c_str(), name) == 0)
			{
				vector2Type = (Vector2Type*) &(m_Vec2s[index]);
				return true;
			}
		}
	}

	return false;
}


bool MaterialPreset::FindFloat(const atString& name, FloatType*& floatType, bool findVarName) const
{
	for(int index = 0; index < GetFloats().GetCount(); ++index)
	{
		if (findVarName)
		{
			if ( stricmp(m_Floats[index].GetVariableName().c_str(), name) == 0)
			{
				floatType = (FloatType*) &(m_Floats[index]);
				return true;
			}
		}
		else
		{
			if ( stricmp(m_Floats[index].GetName().c_str(), name) == 0)
			{
				floatType = (FloatType*) &(m_Floats[index]);
				return true;
			}
		}
	}

	return false;
}

bool MaterialPreset::FindInt(const atString& name, IntType*& intType, bool findVarName) const
{
	for(int index = 0; index < GetInts().GetCount(); ++index)
	{
		if (findVarName)
		{
			if ( stricmp(m_Ints[index].GetVariableName().c_str(), name) == 0)
			{
				intType = (IntType*) &(m_Ints[index]);
				return true;
			}
		}
		else
		{
			if ( stricmp(m_Ints[index].GetName().c_str(), name) == 0)
			{
				intType = (IntType*) &(m_Ints[index]);
				return true;
			}
		}
	}

	return false;
}

bool MaterialPreset::IsAlphaShader() const
{
	if ( m_Properties.m_DrawBucket > 0 || m_Properties.m_ExposeAlphaMap == true)
	{
		return true;
	}

	return false;
}

const TypeBase* MaterialPreset::GetVariable(const atString& name, bool findByVarName)
{
	IntType* pIntTypeBase;
	FloatType* pFloatTypeBase;
	Vector2Type* pVec2Type;
	Vector3Type* pVec3Type;
	Vector4Type* pVec4Type;
	TexMapType* pTexMapType;

	// Based on the name of the variable, try to find the appropriate TypeBase for it.
	if (FindInt(name, pIntTypeBase, findByVarName))
		return (TypeBase*)pIntTypeBase;
	if (FindFloat(name, pFloatTypeBase, findByVarName))
		return (TypeBase*)pFloatTypeBase;
	if (FindVector2(name, pVec2Type, findByVarName))
		return (TypeBase*)pVec2Type;
	if (FindVector3(name, pVec3Type, findByVarName))
		return (TypeBase*)pVec3Type;
	if (FindVector4(name, pVec4Type, findByVarName))
		return (TypeBase*)pVec4Type;
	if (FindTexMap(name, pTexMapType, findByVarName))
		return (TypeBase*)pTexMapType;

	return NULL;
}

const int MaterialPreset::GetNumUnlockedVariables()
{
	int count = 0;
	for (int i = 0; i < m_Variables.GetCount(); i++)
	{
		if (!m_Variables[i]->IsLocked())
			count++;
	}
	return count;
}

void MaterialPreset::CreateVariableArray() 
{
	m_Variables.Resize(GetNumVariables());

	int arrayIndex; 
	for(arrayIndex = 0; arrayIndex < m_TexMaps.GetCount(); ++arrayIndex )
	{
		int index = m_TexMaps[arrayIndex].GetIndex();
		m_Variables.GetElements()[index] = &(m_TexMaps[arrayIndex]);	
	}

	for(arrayIndex = 0; arrayIndex < m_Vec4s.GetCount(); ++arrayIndex )
	{
		int index = m_Vec4s[arrayIndex].GetIndex();
		m_Variables.GetElements()[index] = &(m_Vec4s[arrayIndex]);	
	}

	for(arrayIndex = 0; arrayIndex < m_Vec3s.GetCount(); ++arrayIndex )
	{
		int index = m_Vec3s[arrayIndex].GetIndex();
		m_Variables.GetElements()[index] = &(m_Vec3s[arrayIndex]);	
	}

	for(arrayIndex = 0; arrayIndex < m_Vec2s.GetCount(); ++arrayIndex )
	{
		int index = m_Vec2s[arrayIndex].GetIndex();
		m_Variables.GetElements()[index] = &(m_Vec2s[arrayIndex]);	
	}

	for(arrayIndex = 0; arrayIndex < m_Floats.GetCount(); ++arrayIndex )
	{
		int index = m_Floats[arrayIndex].GetIndex();
		m_Variables.GetElements()[index] = &(m_Floats[arrayIndex]);	
	}

	for(arrayIndex = 0; arrayIndex < m_Ints.GetCount(); ++arrayIndex )
	{
		int index = m_Ints[arrayIndex].GetIndex();
		m_Variables.GetElements()[index] = &(m_Ints[arrayIndex]);	
	}
}

void MaterialPreset::CopyBaseValues(const MaterialPreset& preset)
{
	m_Parent = preset.m_Parent;
	m_ShaderName = preset.m_ShaderName;
	m_FriendlyName = preset.m_FriendlyName;
	m_PresetLevel = preset.m_PresetLevel;
}

void MaterialPreset::CopyProperties(const MaterialPreset& preset)
{
	m_Header.m_CpvMap = preset.m_Header.m_CpvMap;

	m_Properties.m_DrawBucket = preset.m_Properties.m_DrawBucket;
	m_Properties.m_DrawBucketMask = preset.m_Properties.m_DrawBucketMask;
	m_Properties.m_ExposeAlphaMap = preset.m_Properties.m_ExposeAlphaMap;
	m_Properties.m_IsInstanced = preset.m_Properties.m_IsInstanced;
}

bool MaterialPreset::Copy(const MaterialPreset& preset, bool copyAllValues)
{
	// We don't want to stomp out these when doing a merge which uses Copy.
	if (copyAllValues)
	{
		m_Parent = preset.m_Parent;
		m_ShaderName = preset.m_ShaderName;
		m_FriendlyName = preset.m_FriendlyName;
		m_PresetLevel = preset.m_PresetLevel;
	}

	m_Header.m_CpvMap = preset.m_Header.m_CpvMap;

	m_Properties.m_DrawBucket = preset.m_Properties.m_DrawBucket;
	m_Properties.m_DrawBucketMask = preset.m_Properties.m_DrawBucketMask;
	m_Properties.m_ExposeAlphaMap = preset.m_Properties.m_ExposeAlphaMap;
	m_Properties.m_IsInstanced = preset.m_Properties.m_IsInstanced;

	const atArray<TexMapType>& texmaps = preset.GetTexMaps();
	for(int varIndex = 0; varIndex < texmaps.GetCount(); ++varIndex)
	{
		AddTexMap(texmaps[varIndex]);
	}

	const atArray<FloatType>& floats = preset.GetFloats();
	for(int varIndex = 0; varIndex < floats.GetCount(); ++varIndex)
	{
		AddFloat(floats[varIndex]);
	}

	const atArray<IntType>& ints = preset.GetInts();
	for(int varIndex = 0; varIndex < ints.GetCount(); ++varIndex)
	{
		AddInt(ints[varIndex]);
	}

	const atArray<Vector2Type>& vector2s = preset.GetVector2s();
	for(int varIndex = 0; varIndex < vector2s.GetCount(); ++varIndex)
	{
		AddVector2(vector2s[varIndex]);
	}

	const atArray<Vector3Type>& vector3s = preset.GetVector3s();
	for(int varIndex = 0; varIndex < vector3s.GetCount(); ++varIndex)
	{
		AddVector3(vector3s[varIndex]);
	}

	const atArray<Vector4Type>& vector4s = preset.GetVector4s();
	for(int varIndex = 0; varIndex < vector4s.GetCount(); ++varIndex)
	{
		AddVector4(vector4s[varIndex]);
	}

	return true;
}

bool MaterialPreset::Merge(const MaterialPreset& parentPreset)
{
	for (int varIndex = 0; varIndex < parentPreset.GetTexMaps().GetCount(); ++varIndex)
	{
		const TexMapType& parentTexMap = parentPreset.m_TexMaps[varIndex];
		TexMapType* childTexMap = NULL;
		if (FindTexMap(parentTexMap.GetName(), childTexMap))
		{
			if (parentTexMap.IsLocked() == true)
			{
				childTexMap->SetLocked(true);
				childTexMap->SetValue(parentTexMap.GetValue());
			}
		}
		else
		{
			AddTexMap(parentTexMap);
		}
	}

	for (int varIndex = 0; varIndex < parentPreset.GetFloats().GetCount(); ++varIndex)
	{
		const FloatType& parentFloat = parentPreset.m_Floats[varIndex];
		FloatType* childFloat = NULL;
		if (FindFloat(parentFloat.GetName(), childFloat))
		{
			if (parentFloat.IsLocked() == true)
			{
				childFloat->SetLocked(true);
				childFloat->SetValue(parentFloat.GetValue());
			}
		}
		else
		{
			AddFloat(parentFloat);
		}
	}

	for (int varIndex = 0; varIndex < parentPreset.GetInts().GetCount(); ++varIndex)
	{
		const IntType& parentInt = parentPreset.m_Ints[varIndex];
		IntType* childInt = NULL;
		if (FindInt(parentInt.GetName(), childInt))
		{
			if (parentInt.IsLocked() == true)
			{
				childInt->SetLocked(true);
				childInt->SetValue(parentInt.GetValue());
			}
		}
		else
		{
			AddInt(parentInt);
		}
	}

	for (int varIndex = 0; varIndex < parentPreset.GetVector2s().GetCount(); ++varIndex)
	{
		const Vector2Type& parentVec2 = parentPreset.m_Vec2s[varIndex];
		Vector2Type* childVec2 = NULL;
		if (FindVector2(parentVec2.GetName(), childVec2))
		{
			if (parentVec2.IsLocked() == true)
			{
				childVec2->SetLocked(true);
				childVec2->SetValue(parentVec2.GetValue());
			}
		}
		else
		{
			AddVector2(parentVec2);
		}
	}

	for (int varIndex = 0; varIndex < parentPreset.GetVector3s().GetCount(); ++varIndex)
	{
		const Vector3Type& parentVec3 = parentPreset.m_Vec3s[varIndex];
		Vector3Type* childVec3 = NULL;
		if (FindVector3(parentVec3.GetName(), childVec3))
		{
			if (parentVec3.IsLocked() == true)
			{
				childVec3->SetLocked(true);
				childVec3->SetValue(parentVec3.GetValue());
			}
		}
		else
		{
			AddVector3(parentVec3);
		}
	}

	for (int varIndex = 0; varIndex < parentPreset.GetVector4s().GetCount(); ++varIndex)
	{
		const Vector4Type& parentVec4 = parentPreset.m_Vec4s[varIndex];
		Vector4Type* childVec4 = NULL;
		if (FindVector4(parentVec4.GetName(), childVec4))
		{
			if (parentVec4.IsLocked() == true)
			{
				childVec4->SetLocked(true);
				childVec4->SetValue(parentVec4.GetValue());
			}
		}
		else
		{
			AddVector4(parentVec4);
		}
	}

	return true;
}

/*
bool MaterialPreset::Merge(const MaterialPreset& childPreset)
{
	for(int varIndex = 0; varIndex < childPreset.GetTexMaps().GetCount(); ++varIndex)
	{
		const TexMapType& childTexMap = childPreset.m_TexMaps[varIndex];
		TexMapType* parentTexMap = NULL;
		if ( FindTexMap(childTexMap.GetName(), parentTexMap) ) //The variable has been removed if NULL; ignore it.
		{
			if ( parentTexMap->IsLocked() == false )
			{
				parentTexMap->SetLocked(childTexMap.IsLocked());
				parentTexMap->SetValue(childTexMap.GetValue());
			}
		}
	}

	for(int varIndex = 0; varIndex < childPreset.GetFloats().GetCount(); ++varIndex)
	{
		const FloatType& childFloat = childPreset.m_Floats[varIndex];
		FloatType* parentFloat = NULL;
		if ( FindFloat(childFloat.GetName(), parentFloat) ) //The variable has been removed if NULL; ignore it.
		{
			if ( parentFloat->IsLocked()== false )
			{
				parentFloat->SetLocked(childFloat.IsLocked());
				parentFloat->SetValue(childFloat.GetValue());
			}
		}
	}

	for(int varIndex = 0; varIndex < childPreset.GetInts().GetCount(); ++varIndex)
	{
		const IntType& childInt = childPreset.m_Ints[varIndex];
		IntType* parentInt = NULL;
		if ( FindInt(childInt.GetName(), parentInt) ) //The variable has been removed if NULL; ignore it.
		{
			if ( parentInt->IsLocked()== false )
			{
				parentInt->SetLocked(childInt.IsLocked());
				parentInt->SetValue(childInt.GetValue());
			}
		}
	}

	for(int varIndex = 0; varIndex < childPreset.GetVector4s().GetCount(); ++varIndex)
	{
		const Vector4Type& childVector4 = childPreset.m_Vec4s[varIndex];
		Vector4Type* parentVector4 = NULL;
		if ( FindVector4(childVector4.GetName(), parentVector4) ) //The variable has been removed if NULL; ignore it.
		{
			if ( parentVector4->IsLocked()== false )
			{
				parentVector4->SetLocked(childVector4.IsLocked());
				parentVector4->SetValue(childVector4.GetValue());
			}
		}
	}

	for(int varIndex = 0; varIndex < childPreset.GetVector3s().GetCount(); ++varIndex)
	{
		const Vector3Type& childVector3 = childPreset.m_Vec3s[varIndex];
		Vector3Type* parentVector3 = NULL;
		if ( FindVector3(childVector3.GetName(), parentVector3) ) //The variable has been removed if NULL; ignore it.
		{
			if ( parentVector3->IsLocked()== false )
			{
				parentVector3->SetLocked(childVector3.IsLocked());
				parentVector3->SetValue(childVector3.GetValue());
			}
		}
	}

	for(int varIndex = 0; varIndex < childPreset.GetVector2s().GetCount(); ++varIndex)
	{
		const Vector2Type& childVector2 = childPreset.m_Vec2s[varIndex];
		Vector2Type* parentVector2 = NULL;
		if ( FindVector2(childVector2.GetName(), parentVector2) ) //The variable has been removed if NULL; ignore it.
		{
			if ( parentVector2->IsLocked()== false )
			{
				parentVector2->SetLocked(childVector2.IsLocked());
				parentVector2->SetValue(childVector2.GetValue());
			}
		}
	}

	return true;
}
*/

void MaterialPreset::Clear()
{
	m_Parent = NULL;
	m_ShaderName = NULL;
	m_FriendlyName = NULL;
	m_PresetLevel = kMaster;

	m_Header.Clear();

	m_TexMaps.clear();
	m_Vec4s.clear();
	m_Vec3s.clear();
	m_Vec2s.clear();
	m_Floats.clear();
	m_Ints.clear();
}

void MaterialPreset::ClearProperties()
{
	m_Header.Clear();
	
	MaterialProperties emptyProperties;
	m_Properties = emptyProperties;
}

void MaterialPreset::Destroy()
{
	m_Parent = NULL;
	m_ShaderName = NULL;
	m_FriendlyName = NULL;
	m_PresetLevel = kMaster;

	m_Header.Clear();

	m_TexMaps.Reset();
	m_Vec4s.Reset();
	m_Vec3s.Reset();
	m_Vec2s.Reset();
	m_Floats.Reset();
	m_Ints.Reset();

	m_Variables.Reset();
}