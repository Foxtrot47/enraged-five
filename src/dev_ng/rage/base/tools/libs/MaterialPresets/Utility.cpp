#include "Utility.h"

#include "configParser\configGameView.h"

void MaterialPresetUtility::ResolvePath(const char* path, char* resolvedPath, configParser::ConfigGameView* gv)
{
	TCHAR assetPath[_MAX_PATH] = {0};
	gv.GetAssetsDir( assetPath, _MAX_PATH );
	atString newPath(resolvedPath);
	newPath.Replace("$(ASSETS)",assetPath);
	newPath.Trim();
	strcpy(resolvedPath,newPath.c_str());
}