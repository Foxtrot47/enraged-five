
#include "MaterialPresetManager.h"

#include "file/device.h"
#include <io.h>

char* MaterialPresetManager::m_TemplateFileExtension = ".mpt";
char* MaterialPresetManager::m_SurfaceFileExtension = ".mps";
char* MaterialPresetManager::m_ObjectFileExtension = ".mpo";
char* MaterialPresetManager::m_ExportFileExtension = ".mpi";

#define SEPARATOR			'/'
#define SEPARATOR_STRING	"/"
#define OTHER_SEPARATOR	'\\'

MaterialPresetManager m_MaterialPresetManager;

atString MaterialPresetManager::GetErrors()
{
	atString ret = sMaterialPresetManagerErrors;
	sMaterialPresetManagerErrors = "";
	return ret;
}

MaterialPresetManager::MaterialPresetManager() 
{ 
		// These are loaded by default but should be overriden by 1 of 2 ways.
		// 1.) Maxscript calls to update the path based on the configuration DLL.
		// 2.) Any other tool that uses this library will need to resolve the asset directory and update it here.
	char cEnv[RAGE_MAX_PATH];
	if ( sysGetEnv("RS_BUILDBRANCH", cEnv, RAGE_MAX_PATH) == true)
	{
		m_PresetPath = atString(cEnv);
		m_PresetPath += "/common/shaders/db/"; 
	}
	else
		sMaterialPresetManagerErrors += "Environment Variable \"RS_BUILDBRANCH\" can't be found. Please insure you ran the project installer successfully.";

	if ( sysGetEnv("RS_PROJROOT", cEnv, RAGE_MAX_PATH) == true)
	{
		m_MaterialMetadataPath = atString(cEnv);
		m_MaterialMetadataPath += "/assets/metadata/materials";

		m_AssetPath = atString(cEnv);
		m_AssetPath += "/assets";
	}
	else
		sMaterialPresetManagerErrors += "Environment Variable \"RS_PROJROOT\" can't be found. Please insure you ran the project installer successfully.";
}

MaterialPresetManager& MaterialPresetManager::GetCurrent()
{
	if ( m_MaterialPresetManager.IsInitialized() == false )
	{
		m_MaterialPresetManager.Init();
	}

	return m_MaterialPresetManager; 
}

void MaterialPresetManager::Init()
{
	INIT_PARSER;
	m_Initialized = true;
}

void MaterialPresetManager::Shutdown()
{
	m_PresetLibrary.Kill();
	SHUTDOWN_PARSER;
}

static void FixPath(char *dest,int destSize,const char *src, bool trailingSlash = true) {
	safecpy(dest,src,destSize);
	if (dest[0] == 0)
		return;

	int sl = StringLength(dest);
	char *path = dest;

	// Convert to expected slash type
	while (*path) {
		if (*path == OTHER_SEPARATOR)
			*path = SEPARATOR;
		path++;
	}

	// Tack on trailing appropriate slash if necessary
	if (trailingSlash && sl && dest[--sl] != SEPARATOR) {
		dest[++sl] = SEPARATOR;
		dest[++sl] = 0;
	}
}

void MaterialPresetManager::SetPresetPath(const atString& presetPath) 
{
	char fixedPath[RAGE_MAX_PATH];
	FixPath(fixedPath, RAGE_MAX_PATH, presetPath.c_str(), true);

	m_PresetPath = atString(fixedPath);
	m_PresetPath.Lowercase();
}

const char* MaterialPresetManager::GetPresetPath()
{
	return m_PresetPath.c_str();
}

void MaterialPresetManager::SetMetadataPath(const atString& metadataPath)
{
	char fixedPath[RAGE_MAX_PATH];
	FixPath(fixedPath, RAGE_MAX_PATH, metadataPath.c_str(), true);

	m_MaterialMetadataPath = atString(fixedPath);
	m_MaterialMetadataPath.Lowercase();
}

const char* MaterialPresetManager::GetMetadataPath()
{
	return m_MaterialMetadataPath.c_str();
}

void MaterialPresetManager::SetAssetPath(const atString& assetPath)
{
	char fixedPath[RAGE_MAX_PATH];
	FixPath(fixedPath, RAGE_MAX_PATH, assetPath.c_str(), true);

	m_AssetPath = atString(fixedPath);
	m_AssetPath.Lowercase();
}

void MaterialPresetManager::SetCachedTemplateSavePath(char* savePath)
{
	if (savePath)
		m_CachedSavePath = savePath;
}

bool MaterialPresetManager::IsPathInPresetDirectory(const atString& presetPath)
{
	char fixedPath[RAGE_MAX_PATH];
	FixPath(fixedPath, RAGE_MAX_PATH, presetPath.c_str(), false);
	atString presetPathCheck = atString(fixedPath);
	presetPathCheck.Lowercase();

	if (presetPathCheck.StartsWith(m_PresetPath) || presetPathCheck.StartsWith("$(assets)"))
		return true;

	return false;
}

atString MaterialPresetManager::MakeRelativePresetPath(const atString& presetPath)
{
	char fixedPath[RAGE_MAX_PATH];
	FixPath(fixedPath, RAGE_MAX_PATH, presetPath.c_str(), false);

	atString newPresetPath = atString(fixedPath);
	newPresetPath.Lowercase();

	// Check to see if we need to substitute an environment variable.
	if (newPresetPath.StartsWith("$(assets)/metadata/materials/"))
	{
		newPresetPath.Replace(atString("$(assets)/metadata/materials/"), atString(""));
	}
	// Check to see if we already start with an absolute path to the preset directory.
	else if (IsPathInPresetDirectory(newPresetPath))
	{
		newPresetPath.Replace(m_PresetPath, atString(""));
	}

	return newPresetPath;
}

atString MaterialPresetManager::ResolvePresetPath(const atString& presetPath)
{
	atString newPresetPath = presetPath;

	if (newPresetPath.StartsWith("$(assets)"))
	{
		newPresetPath.Replace("$(assets)", m_AssetPath);
	}

	char fixedPath[RAGE_MAX_PATH];
	FixPath(fixedPath, RAGE_MAX_PATH, newPresetPath.c_str(), false);

	return atString(fixedPath);
}

ePresetLevel MaterialPresetManager::DeterminePresetLevel(const atString& presetPath)
{
	ePresetLevel presetLevel = kTemplate;

	const char *ext = strrchr(presetPath.c_str(), '.');
	if (ext)
	{
		if (stricmp(ext, MaterialPresetManager::m_TemplateFileExtension) == 0)
			presetLevel = kTemplate;
		else if (stricmp(ext, MaterialPresetManager::m_SurfaceFileExtension) == 0)
			presetLevel = kSurface;
		else if (stricmp(ext, MaterialPresetManager::m_ObjectFileExtension) == 0)
			presetLevel = kObject;
	}

	return presetLevel;
}

void MaterialPresetManager::GenerateDiffPreset(const MaterialPreset& basePreset, const MaterialPreset& diffPreset, MaterialPreset& newPreset)
{
	const atArray<TexMapType>& texMaps = diffPreset.GetTexMaps();
	for (int i = 0; i < texMaps.GetCount(); i++)
	{
		atString varName = texMaps[i].GetName();
		TexMapType* varType = NULL;
		if (basePreset.FindTexMap(varName, varType))
		{
			if (stricmp(varType->GetValue().c_str(), texMaps[i].GetValue().c_str()) != 0)
				newPreset.AddTexMap(texMaps[i]);
		}
	}

	const atArray<Vector4Type>& vec4s = diffPreset.GetVector4s();
	for (int i = 0; i < vec4s.GetCount(); i++)
	{
		atString varName = vec4s[i].GetName();
		Vector4Type* varType = NULL;
		if (basePreset.FindVector4(varName, varType))
		{
			if (!varType->GetValue().IsEqual(vec4s[i].GetValue()))
				newPreset.AddVector4(vec4s[i]);
		}
	}

	const atArray<Vector3Type>& vec3s = diffPreset.GetVector3s();
	for (int i = 0; i < vec3s.GetCount(); i++)
	{
		atString varName = vec3s[i].GetName();
		Vector3Type* varType = NULL;
		if (basePreset.FindVector3(varName, varType))
		{
			if (!varType->GetValue().IsEqual(vec3s[i].GetValue()))
				newPreset.AddVector3(vec3s[i]);
		}
	}

	const atArray<Vector2Type>& vec2s = diffPreset.GetVector2s();
	for (int i = 0; i < vec2s.GetCount(); i++)
	{
		atString varName = vec2s[i].GetName();
		Vector2Type* varType = NULL;
		if (basePreset.FindVector2(varName, varType))
		{
			if (!varType->GetValue().IsEqual(vec2s[i].GetValue()))
				newPreset.AddVector2(vec2s[i]);
		}
	}

	const atArray<FloatType>& floats = diffPreset.GetFloats();
	for (int i = 0; i < floats.GetCount(); i++)
	{
		atString varName = floats[i].GetName();
		FloatType* varType = NULL;
		if (basePreset.FindFloat(varName, varType))
		{
			if (varType->GetValue() != floats[i].GetValue())
				newPreset.AddFloat(floats[i]);
		}
	}

	const atArray<IntType>& ints = diffPreset.GetInts();
	for (int i = 0; i < ints.GetCount(); i++)
	{
		atString varName = ints[i].GetName();
		IntType* varType = NULL;
		if (basePreset.FindInt(varName, varType))
		{
			if (varType->GetValue() != ints[i].GetValue())
				newPreset.AddInt(ints[i]);
		}
	}
}

void MaterialPresetManager::ReloadPresetLibrary()
{
	atMap<atString, MaterialPreset*>::Iterator iter = m_PresetLibrary.CreateIterator();
	iter.Start();
	while (!iter.AtEnd())
	{
		atString& presetPath = iter.GetKey();
		atString fullPresetPath = atString(m_PresetPath, presetPath);

		// TL TODO
		// Probably want to handle this a bit differently.
		// We are iterating through each preset so there is a lot of doubling up on the reloads. Works for now.
		MergePresetRec(fullPresetPath, true);

		iter.Next();
	}
}

void MaterialPresetManager::ReloadPreset(atString& presetPath)
{
	atString fullPresetPath = atString(m_PresetPath, presetPath);
	MergePresetRec(fullPresetPath, true);
}

void MaterialPresetManager::LoadPresets()
{
	LoadPresets(m_MasterPresets, m_PresetPath);
}

void MaterialPresetManager::LoadPresets( atArray<MaterialPreset*>& presets, const atString& presetPath )
{
	fiFindData data;
	fiHandle handle = fiDeviceLocal::GetInstance().FindFileBegin(presetPath.c_str(), data);
	if ( fiIsValidHandle(handle) ) 
	{
		bool validHandle = true;
		while (validHandle)
		{
			if (data.m_Name[0] != '.') 
			{
				if ((data.m_Attributes & FILE_ATTRIBUTE_DIRECTORY) == FILE_ATTRIBUTE_DIRECTORY)
				{
					atString newPath(presetPath, data.m_Name);
					newPath.Lowercase();

					LoadPresets(presets, newPath);
				}
				else
				{
					if ( MaterialPresetManager::IsMaterialSurface(data.m_Name) || MaterialPresetManager::IsMaterialTemplate(data.m_Name))
					{
						char filePath[RAGE_MAX_PATH];
						sprintf_s(filePath, RAGE_MAX_PATH, "%s/%s", presetPath.c_str(), data.m_Name);
						atString presetFilePath(filePath);
						presetFilePath.Lowercase();

						MaterialPreset* newPreset = LoadPreset(presetFilePath);
						if ( newPreset != NULL )
						{
							presets.PushAndGrow(newPreset);

							atString presetNameKey = atString(data.m_Name);
							presetNameKey.Lowercase();

							m_PresetLibrary.Insert(presetNameKey, newPreset);
						}
					}
				}
			}

			validHandle = fiDeviceLocal::GetInstance().FindFileNext(handle, data);
		}

		fiDeviceLocal::GetInstance().FindFileEnd(handle);
	}
}

MaterialPreset* MaterialPresetManager::LoadPresetNoMerge(const atString& presetFilePath, bool relative)
{
	atString fullPresetPath = presetFilePath;
	
	if (!relative)
		fullPresetPath = atString(m_PresetPath, presetFilePath);

	MaterialPreset* currentPreset = rage_new MaterialPreset();
	PARSER.LoadObject<MaterialPreset>(fullPresetPath.c_str(), "", *currentPreset);
	return currentPreset;
}

MaterialPreset* MaterialPresetManager::LoadPreset(const atString& presetFilePath)
{
	MaterialPreset* preset = rage_new MaterialPreset();
	preset = MergePresetRec(presetFilePath);
	return preset;
}

const MaterialPreset& MaterialPresetManager::GetPreset( const atString& presetFilePath )
{
	atString presetFilePathLower = presetFilePath;
	presetFilePathLower.Lowercase();

	MaterialPreset** preset = m_PresetLibrary.Access(presetFilePathLower.c_str());
	Assert(preset);
	return **(preset);
}

MaterialPreset* MaterialPresetManager::LoadPresetFromLibrary( const atString& presetName )
{
	atString presetNameLower = atString(presetName);
	presetNameLower.Lowercase();

	MaterialPreset** preset = m_PresetLibrary.Access(presetNameLower.c_str());
	if ( preset != NULL )
	{
		return *(m_PresetLibrary.Access(presetNameLower.c_str()));
	}
	else
	{
		atString presetFilePath(m_PresetPath, presetNameLower);
		atString relativePresetPath = MakeRelativePresetPath(presetFilePath);

		if(FileExists(presetFilePath) == false)
		{
			return NULL;
		}

		return LoadPreset(presetFilePath);
	}
}

MaterialPreset* MaterialPresetManager::MergePresetRec(const atString& presetPath, bool reload)
{
	atString relativePresetPath = MakeRelativePresetPath(presetPath);
	atString resolvedPresetPath = ResolvePresetPath(presetPath);
	MaterialPreset* currentPreset = NULL;

	if ( m_PresetLibrary.Access(relativePresetPath.c_str()) == NULL )
	{
		currentPreset = rage_new MaterialPreset();
		PARSER.LoadObject<MaterialPreset>(resolvedPresetPath, "", *currentPreset);
		currentPreset->SetPresetLevel(DeterminePresetLevel(resolvedPresetPath));
	}
	else
	{
		currentPreset = *(m_PresetLibrary.Access(relativePresetPath.c_str()));

		if (reload)
		{
			// Completely kill the memory from the current preset otherwise we'll leak when we reload.
			currentPreset->Destroy();

			PARSER.LoadObject<MaterialPreset>(resolvedPresetPath, "", *currentPreset);
			currentPreset->SetPresetLevel(DeterminePresetLevel(resolvedPresetPath));
		}
	}

	if (currentPreset)
	{
		if (currentPreset->GetParentName() != NULL && strlen(currentPreset->GetParentName()) != 0)
		{
			atString relativeParentPreset = MakeRelativePresetPath(atString(currentPreset->GetParentName()));
			MaterialPreset* parentPreset = NULL;

			if ( m_PresetLibrary.Access(relativeParentPreset.c_str()) == NULL )
			{
				atString parentPresetPath(currentPreset->GetParentName());
				parentPreset = MergePresetRec(parentPresetPath, reload);
			}
			else
			{
				atString parentPresetPath(currentPreset->GetParentName());
				parentPreset = *(m_PresetLibrary.Access(relativeParentPreset.c_str()));

				if (reload)
					parentPreset = MergePresetRec(parentPresetPath, true);
			}

			if ( currentPreset == NULL || parentPreset == NULL )
				return NULL;  //Incomplete preset chain.

			currentPreset->Merge(*parentPreset);
		}

		currentPreset->CreateVariableArray();

		// Don't add the same preset twice.
		if (m_PresetLibrary.Access(relativePresetPath.c_str()) == NULL)
			m_PresetLibrary.Insert(relativePresetPath, currentPreset);
	}

	return currentPreset;
}

bool MaterialPresetManager::SavePreset(const atString& fileName, const MaterialPreset& materialPreset)
{
	INIT_PARSER;
	bool result = PARSER.SaveObject(fileName.c_str(), "", &materialPreset, parManager::XML);
	SHUTDOWN_PARSER;
	return result;
}

bool MaterialPresetManager::FileExists( const char * filename )
{
	_finddata_t fileInfo;
	intptr_t hdl = _findfirst( filename, &fileInfo );
	_findclose( hdl );

	return (hdl != -1);
}

