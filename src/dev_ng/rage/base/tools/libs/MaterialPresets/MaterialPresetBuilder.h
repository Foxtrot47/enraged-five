#include "MaterialPreset.h"

class MaterialPresetBuilder
{
public:
#pragma region Constructors
	MaterialPresetBuilder();
	~MaterialPresetBuilder();
#pragma endregion

#pragma region PublicFunctions
	const MaterialPreset* FlattenPreset();
#pragma endregion

private:
	MaterialPreset* m_MasterPreset;
	MaterialPreset* m_TemplatePreset;
	MaterialPreset* m_SurfacePreset;

	MaterialPreset* m_ObjectPreset;
};