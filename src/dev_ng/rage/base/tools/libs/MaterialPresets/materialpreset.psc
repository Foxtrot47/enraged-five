<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema"
>

  <enumdef type="ePresetLevel">
    <enumval name="kMaster"/>
    <enumval name="kTemplate"/>
    <enumval name="kSurface"/>
    <enumval name="kObject"/>
  </enumdef>
  
	<enumdef type="eTextureMapType">
    <enumval name="kTypeUnknown"/>
		<enumval name="kTypeDiffuse"/>
		<enumval name="kTypeNormal"/>
		<enumval name="kTypeSpecular"/>
    <enumval name="kTypeTintPalette"/>
    <enumval name="kTypeDetail"/>    
  </enumdef>				
	  
	<structdef type="MaterialPreset" autoregister="true">
    <string name="m_Parent" type="atString"/>
		<string name="m_ShaderName" type="atString"/>
		<string name="m_FriendlyName" type="atString"/>
		<struct name="m_Header" type="MaterialHeader" />
		<array name="m_TexMaps" type="atArray">
			<struct type="TexMapType" />
		</array>
		<array name="m_Vec4s" type="atArray">
			<struct type="Vector4Type" />
		</array>
		<array name="m_Vec3s" type="atArray">
			<struct type="Vector3Type" />
		</array>
		<array name="m_Vec2s" type="atArray">
			<struct type="Vector2Type" />
		</array>
		<array name="m_Floats" type="atArray">
			<struct type="FloatType" />
		</array>
		<array name="m_Ints" type="atArray">
			<struct type="IntType" />
		</array>
    <struct name="m_Properties" type="MaterialProperties"/>
	</structdef>
	
	<structdef type="MaterialHeader" autoregister="true">
		<array name="m_CpvMap" type="atArray">
			<struct type="MapChannel"/>
		</array>
	</structdef>
	
	<structdef type="DCCMapChannel" autoregister="true">
		<int name="m_Index" />
		<int name="m_Component" />
	</structdef>
	
	<structdef type="MapChannel" autoregister="true">
		<int name="m_Channel" />
		<struct name="m_X" type="DCCMapChannel" />
		<struct name="m_Y" type="DCCMapChannel" />
		<struct name="m_Z" type="DCCMapChannel" />
		<struct name="m_W" type="DCCMapChannel" />
	</structdef>
	
	<structdef type="TypeBase" autoregister="true">
		<string name="m_Name" type="atString"/>
    		<string name="m_VariableName" type="atString"/>
		<bool name="m_Locked" />
		<int name="m_Index" />
	</structdef>
	
	<structdef type="TexMapType" base="TypeBase" autoregister="true">
		<enum name="m_TexMapType" type="eTextureMapType" />
		<string name="m_Value" type="atString"/>
    		<string name="m_TextureTemplate" type="atString"/>
    		<string name="m_TextureTemplateRelative" type="atString"/>
    		<string name="m_TextureFormat" type="atString"/>
  </structdef>
	
	<structdef type="Vector4Type" base="TypeBase" autoregister="true">
		<Vector4 name="m_Value" />
    		<float name="m_MinValue" />
    		<float name="m_MaxValue" />
    		<float name="m_Step" />
	</structdef>
	
	<structdef type="Vector3Type" base="TypeBase" autoregister="true">
		<Vector3 name="m_Value" />
    		<float name="m_MinValue" />
    		<float name="m_MaxValue" />
    		<float name="m_Step" />
	</structdef>
	
	<structdef type="Vector2Type" base="TypeBase" autoregister="true">
		<Vector2 name="m_Value" />
    		<float name="m_MinValue" />
    		<float name="m_MaxValue" />
    		<float name="m_Step" />
	</structdef>
	
	<structdef type="FloatType" base="TypeBase" autoregister="true">
		<float name="m_Value" />
		<float name="m_MinValue" />
    		<float name="m_MaxValue" />
		<float name="m_Step" />
	</structdef>
	
	<structdef type="IntType" base="TypeBase" autoregister="true">
		<int name="m_Value" />
	</structdef>

  <structdef type="MaterialProperties" autoregister="true">
	<u32 name="m_DrawBucket" init="0" />
	<u32 name="m_DrawBucketMask" init="0"/>
	<bool name="m_IsInstanced" init="false" />
  </structdef>
  
</ParserSchema>