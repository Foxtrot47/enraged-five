//
// File: MaterialPreset.h
// Author: Kevin Weinberg <kevin.weinberg@rockstarsandiego.com>
//		   Luke Openshaw <luke.openshaw@rockstarnorth.com>
// Description: Material Preset base class to describe settings for a shader.
//
#ifndef MATERIAL_PRESET_H
#define MATERIAL_PRESET_H

#include "parser/macros.h"
#include "parser/manager.h"

#include "atl/array.h"
#include "atl/string.h"
#include "vector/vector4.h"
#include "vector/vector3.h"
#include "vector/vector2.h"

using namespace rage;

enum ePresetLevel
{
	kMaster,
	kTemplate,
	kSurface,
	kObject,
};

enum eTextureMapType
{
	kTypeUnknown,
	kTypeDiffuse,
	kTypeNormal,
	kTypeSpecular,
	kTypeTintPalette,
	kTypeDetail
};

//This is a hack that is necessary for the MaterialPresetsTemplateGenerator.
extern const char* eTextureMapType_Strings[]; 

class TypeBase
{
public:
	enum eVariableType
	{
		kUndefined = -1,
		kTextureMap,
		kVector4,
		kVector3,
		kVector2,
		kFloat,
		kInt
	};

	TypeBase();
	TypeBase( atString& name, atString varName, int index, bool locked ) { m_Name = name; m_VariableName = varName, m_Index = index; m_Locked = locked; }
	virtual ~TypeBase() { }

	const atString& GetName() const { return m_Name; }
	const atString& GetVariableName() const { return m_VariableName; }
	int GetIndex() const { return m_Index; }

	virtual eVariableType GetType() const { return kUndefined; } ;

	void SetLocked(const bool& value) { m_Locked = value; }
	bool IsLocked() const { return m_Locked; }

protected:
	atString	m_Name;
	atString	m_VariableName;
	int			m_Index;
	bool		m_Locked;

	PAR_PARSABLE;
};

class TexMapType : public TypeBase
{
public:
	TexMapType() {}

	TexMapType( atString& name, atString& varName, int index, bool locked, eTextureMapType type ) 
		: TypeBase(name, varName, index, locked)
		{ m_TexMapType = type; m_Value = ""; m_TextureTemplate = ""; }

	TexMapType( atString& name, atString& varName, int index, bool locked, atString value, eTextureMapType type ) 
		: TypeBase(name, varName, index, locked)
		{ m_TexMapType = type; m_Value = value; m_TextureTemplate = ""; }

	void SetValue(const atString& value) { m_Value = value; }

	const eTextureMapType GetTextureMapType() const { return m_TexMapType; }
	const atString& GetValue() const { return m_Value; }
	eVariableType GetType() const { return kTextureMap; }

	const atString& GetTextureTemplate() const { return m_TextureTemplate; }
	const atString& GetTextureTemplateRelative() const { return m_TextureTemplateRelative; }
	bool GetTextureTemplateOverride() const { return m_TextureTemplateOverride; }
	const atString& GetTextureFormat() const { return m_TextureFormat; }

	void SetTextureTemplate(const atString& textureTemplate) { m_TextureTemplate = textureTemplate; }
	void SetTextureTemplateRelative(const atString& textureTemplateRelative) { m_TextureTemplateRelative = textureTemplateRelative; }
	void SetTextureTemplateOverride(bool override) { m_TextureTemplateOverride = override; }
	void SetTextureFormat(const atString& textureFormat) { m_TextureFormat = textureFormat; }

private:
	eTextureMapType m_TexMapType;
	atString	m_Value;
	atString	m_TextureTemplate; 
	atString	m_TextureTemplateRelative; 
	bool		m_TextureTemplateOverride;
	atString	m_TextureFormat;

	PAR_PARSABLE;
};

class Vector4Type : public TypeBase
{
public:
	Vector4Type(  ) { }
	Vector4Type( atString& name, atString& varName, int index, bool locked, Vector4::Vector4Ref value ) 
		: TypeBase(name, varName, index, locked)
		{ m_Value = value; m_MinValue = -1; m_MaxValue = -1; m_Step = 0; }

	const Vector4& GetValue() const { return m_Value; }
	eVariableType GetType() const { return kVector4; }

	void SetValue(const Vector4& value) { m_Value = value; }
	void SetMinValue(const float& minValue) { m_MinValue = minValue; }
	void SetMaxValue(const float& maxValue) { m_MaxValue = maxValue; }
	void SetStep(const float& step) { m_Step = step; }

	float GetMinValue() const { return m_MinValue; }
	float GetMaxValue() const { return m_MaxValue; }
	float GetStep() const{ return m_Step; }

private:
	Vector4 m_Value;
	float m_MinValue;
	float m_MaxValue;
	float m_Step;

	PAR_PARSABLE;
};

class Vector3Type : public TypeBase
{
public:
	Vector3Type( ) {  }
	Vector3Type( atString& name, atString& varName, int index, bool locked, Vector3::Vector3Ref value ) 
		: TypeBase(name, varName, index, locked)
		{ m_Value = value; m_MinValue = -1; m_MaxValue = -1; m_Step = 0; }

	void SetValue(const Vector3& value) { m_Value = value; }

	const Vector3& GetValue() const { return m_Value; }
	eVariableType GetType() const { return kVector3; }

	void SetMinValue(const float& minValue) { m_MinValue = minValue; }
	void SetMaxValue(const float& maxValue) { m_MaxValue = maxValue; }
	void SetStep(const float& step) { m_Step = step; }

	float GetMinValue() const { return m_MinValue; }
	float GetMaxValue() const { return m_MaxValue; }
	float GetStep() const{ return m_Step; }

private:
	Vector3 m_Value;
	float m_MinValue;
	float m_MaxValue;
	float m_Step;

	PAR_PARSABLE;
};

class Vector2Type : public TypeBase
{
public:
	Vector2Type( ) {  }
	Vector2Type( atString& name, atString& varName, int index, bool locked, Vector2 value ) 
		: TypeBase(name, varName, index, locked)
		{ m_Value = value; m_MinValue = -1; m_MaxValue = -1; m_Step = 0;  }

	void SetValue(const Vector2& value) { m_Value = value; }

	const Vector2& GetValue() const { return m_Value; }
	eVariableType GetType() const { return kVector2; }

	void SetMinValue(const float& minValue) { m_MinValue = minValue; }
	void SetMaxValue(const float& maxValue) { m_MaxValue = maxValue; }
	void SetStep(const float& step) { m_Step = step; }

	float GetMinValue() const { return m_MinValue; }
	float GetMaxValue() const { return m_MaxValue; }
	float GetStep() const{ return m_Step; }

private:
	Vector2 m_Value;
	float m_MinValue;
	float m_MaxValue;
	float m_Step;

	PAR_PARSABLE;
};

class FloatType : public TypeBase
{
public:
	FloatType( ) { }
	FloatType( atString& name, atString& varName, int index, bool locked, float value ) 
		: TypeBase(name, varName, index, locked)
		{ m_Value = value; m_MinValue = -1; m_MaxValue = -1; m_Step = 0; }

	void SetValue(const float& value) { m_Value = value; }

	const float& GetValue() const { return m_Value; }
	eVariableType GetType() const { return kFloat; }

	void SetMinValue(const float& minValue) { m_MinValue = minValue; }
	void SetMaxValue(const float& maxValue) { m_MaxValue = maxValue; }
	void SetStep(const float& step) { m_Step = step; }

	float GetMinValue() const { return m_MinValue; }
	float GetMaxValue() const { return m_MaxValue; }
	float GetStep() const{ return m_Step; }

private:
	float m_Value;
	float m_MinValue;
	float m_MaxValue;
	float m_Step;

	PAR_PARSABLE;
};

class IntType : public TypeBase
{
public:
	IntType() { }
	IntType( atString& name, atString& varName, int index, bool locked, int value ) 
		: TypeBase(name, varName, index, locked)
		{ m_Value = value; }
	IntType( int value ) { m_Value = value; }

	void SetValue(const int& value) { m_Value = value; }

	const int& GetValue() const { return m_Value; }
	eVariableType GetType() const { return kInt; }
private:
	int m_Value;

	PAR_PARSABLE;
};

struct MaterialProperties
{
public:
	MaterialProperties() { m_DrawBucket = 0; m_DrawBucketMask = 0; m_ExposeAlphaMap = false; m_IsInstanced = false;}

	u32 m_DrawBucket;
	u32 m_DrawBucketMask;
	bool m_ExposeAlphaMap;
	bool m_IsInstanced;

	PAR_PARSABLE;
};


struct DCCMapChannel
{
public:
	DCCMapChannel() {}
	DCCMapChannel(int index, int component) { m_Index = index; m_Component = component; }
private:
	int m_Index;
	int m_Component;

	PAR_PARSABLE;
};

struct MapChannel
{
public:
	MapChannel() {}
private:
	int m_Channel;
	DCCMapChannel m_X;
	DCCMapChannel m_Y;
	DCCMapChannel m_Z;
	DCCMapChannel m_W;
		
	PAR_PARSABLE;
};

class MaterialHeader
{
public:
	MaterialHeader() { };
	virtual ~MaterialHeader() { };

	void Clear() { m_CpvMap.clear(); }
private:
	friend class MaterialPreset;

	atArray<MapChannel> m_CpvMap;

	PAR_PARSABLE;
};

class MaterialPreset
{
public:
	MaterialPreset() : m_PresetLevel(kMaster) { }
	MaterialPreset(const char* /*parent*/, const char* /*shaderName*/, const char* /*friendlyName*/) : m_PresetLevel(kObject) { }
	MaterialPreset(const char* /*parent*/, const char* /*shaderName*/, const char* /*friendlyName*/, const ePresetLevel presetLevel);
	virtual ~MaterialPreset() { };

	MaterialPreset &operator=( const MaterialPreset &that )
	{
		m_Parent = that.m_Parent;
		m_ShaderName = that.m_ShaderName;
		m_FriendlyName = that.m_FriendlyName;
		m_PresetLevel = that.m_PresetLevel;

		m_Header = that.m_Header;

		m_TexMaps = that.m_TexMaps;
		m_Vec4s = that.m_Vec4s;
		m_Vec3s = that.m_Vec3s;
		m_Vec2s = that.m_Vec2s;
		m_Floats = that.m_Floats;
		m_Ints = that.m_Ints;

		m_Variables = that.m_Variables;
		m_Properties = that.m_Properties;

		return *this;
	}

	void AddTexMap(const TexMapType& texMapType) { m_TexMaps.PushAndGrow(texMapType); }
	void AddVector4(const Vector4Type& vector4Type) { m_Vec4s.PushAndGrow(vector4Type); }
	void AddVector3(const Vector3Type& vector3Type) { m_Vec3s.PushAndGrow(vector3Type); }
	void AddVector2(const Vector2Type& vector2Type) { m_Vec2s.PushAndGrow(vector2Type); }
	void AddFloat(const FloatType& floatType) { m_Floats.PushAndGrow(floatType); }
	void AddInt(const IntType& intType) { m_Ints.PushAndGrow(intType); }

	void SetDrawBucket(const u32 drawBucket) { m_Properties.m_DrawBucket = drawBucket; }
	void SetDrawBucketMask(const u32 drawBucketMask) { m_Properties.m_DrawBucketMask = drawBucketMask; }
	void SetExposeAlphaMap(const bool expose) { m_Properties.m_ExposeAlphaMap = expose; }
	void SetIsInstanced(const bool isInstanced) { m_Properties.m_IsInstanced = isInstanced; }
	
	bool IsAlphaShader() const;

	void SetParentName(const char* parent) { m_Parent = parent; }
	void SetShaderName(const char* shaderName) { m_ShaderName = shaderName; }
	void SetFriendlyName(const char* friendlyName) { m_FriendlyName = friendlyName; }
	void SetPresetLevel(ePresetLevel level) { m_PresetLevel = level; }

	const char* GetParentName() const { return m_Parent; }
	const char* GetShaderName() const { return m_ShaderName; }
	const char* GetFriendlyName() const { return m_FriendlyName; }
	const ePresetLevel GetPresetLevel() const { return m_PresetLevel; }

	const atArray<TexMapType>& GetTexMaps() const { return m_TexMaps; }
	const atArray<Vector4Type>& GetVector4s() const { return m_Vec4s; }
	const atArray<Vector3Type>& GetVector3s() const { return m_Vec3s; }
	const atArray<Vector2Type>& GetVector2s() const { return m_Vec2s; }
	const atArray<FloatType>& GetFloats() const { return m_Floats; }
	const atArray<IntType>& GetInts() const { return m_Ints; }

	const TexMapType* GetTexMap(int varIndex) const;
	const Vector4Type* GetVector4(int varIndex) const;
	const Vector3Type* GetVector3(int varIndex) const;
	const Vector2Type* GetVector2(int varIndex) const;
	const FloatType* GetFloat(int varIndex) const;
	const IntType* GetInt(int varIndex) const;
	
	//Creates a flat array containing all variables.
	//Ease of use for instances where the variable are meant to be used
	//in order they were specified in the shader.
	void CreateVariableArray();
	const atArray<TypeBase*>& GetVariableArray() const { return m_Variables; }
	const TypeBase* GetVariable(int index) 
	{  
		if ( index < m_Variables.GetCount() )
			return m_Variables[index]; 

		return NULL;
	}
	const TypeBase* GetVariable(const atString& name, bool findByVarName = false);
	const int GetNumUnlockedVariables();

	//Copies a preset into the existing structure.
	bool Copy(const MaterialPreset& preset, bool copyAllValues);

	// Copies a presets parent, shaderName and friendlyName.
	void CopyBaseValues(const MaterialPreset& preset);
	void CopyProperties(const MaterialPreset& preset);

	//Merges the specified preset with the current one.
	//The values from the specified preset will be added to the existing one.
	bool Merge(const MaterialPreset& preset);

	//Clears the preset.
	void Clear();
	void ClearProperties();

	// Destroy the preset and all the release all the memory.
	void Destroy();

	const int GetNumVariables() const { return m_TexMaps.GetCount() + m_Vec4s.GetCount() + m_Vec3s.GetCount() + m_Vec2s.GetCount() + m_Ints.GetCount() + m_Floats.GetCount(); }

	bool FindTexMap(const atString& name, TexMapType*& texMap, bool findVarName = false) const;
	bool FindVector4(const atString& name, Vector4Type*& vector4Type, bool findVarName = false) const;
	bool FindVector3(const atString& name, Vector3Type*& vector3Type, bool findVarName = false) const;
	bool FindVector2(const atString& name, Vector2Type*& vector2Type, bool findVarName = false) const;
	bool FindFloat(const atString& name, FloatType*& floatType, bool findVarName = false) const;
	bool FindInt(const atString& name, IntType*& intType, bool findVarName = false) const;

private:

	atString m_Parent;
	atString m_ShaderName;
	atString m_FriendlyName;
	ePresetLevel m_PresetLevel;

	MaterialHeader m_Header;

	atArray<TexMapType> m_TexMaps;
	atArray<Vector4Type> m_Vec4s;
	atArray<Vector3Type> m_Vec3s;
	atArray<Vector2Type> m_Vec2s;
	atArray<FloatType>	m_Floats;
	atArray<IntType>	m_Ints;

	atArray<TypeBase*> m_Variables;

	MaterialProperties m_Properties;

	PAR_PARSABLE;
};

#endif //MATERIAL_PRESET_H