#include "atl/array.h"
#include "atl/string.h"
#include "atl/map.h"
#include "system/exec.h"
#include "materialpreset.h"

#pragma once

class MaterialPresetManager
{
public:
	MaterialPresetManager();
	~MaterialPresetManager() { }

	//Loads all presets within m_PresetPath, recursively.
	void LoadPresets();

	atString GetErrors();

	bool IsPathInPresetDirectory(const atString& presetPath);
	atString MakeRelativePresetPath(const atString& presetPath);
	atString ResolvePresetPath(const atString& presetPath);
	ePresetLevel DeterminePresetLevel(const atString& presetPath);

	void GenerateDiffPreset(const MaterialPreset& basePreset, const MaterialPreset& diffPreset, MaterialPreset& newPreset);

	// Sets/gets the preset path.
	void SetPresetPath(const atString& presetPath);
	const char* GetPresetPath();

	void SetMetadataPath(const atString& metadataPath);
	const char* GetMetadataPath();

	void SetAssetPath(const atString& assetPath);

	void GetPresetsForTemplate( atString templatename, atArray<MaterialPreset>& presets );
	void GetPresetsForMaster( atString mastername, atArray<MaterialPreset>& presets );
	void GetPresetsForSurface( atString surfacename, atArray<MaterialPreset>& presets );

	void SetMasterPath( atString masterPath ) { m_MasterPath = masterPath; }
	void SetTemplatePath( atString templatePath ) { m_TemplatePath = templatePath; }
	void SetSurfacePath( atString surfacePath ) { m_SurfacePath = surfacePath; }

	// Store the save path (used for 3dsMax preset saving).
	void SetCachedTemplateSavePath(char* savePath);
	const char* GetCachedTemplateSavePath() { return m_CachedSavePath.c_str(); }

	void Reset();

	void ReloadPresetLibrary();
	void ReloadPreset(atString& presetPath);

	MaterialPreset* LoadPreset( const atString& presetFilePath );
	MaterialPreset* LoadPresetNoMerge(const atString& presetFilePath, bool relative = false);
	const MaterialPreset& GetPreset( const atString& presetFilePath );
	MaterialPreset* LoadPresetFromLibrary( const atString& presetName );
	static bool SavePreset(const atString& fileName, const MaterialPreset& materialPreset);

	static bool IsMaterialTemplate(const char* fileName) { return (strstr(fileName, m_TemplateFileExtension) != NULL); }
	static bool IsMaterialSurface(const char* fileName) { return (strstr(fileName, m_SurfaceFileExtension) != NULL); }
	static bool IsMaterialObject(const char* fileName) { return (strstr(fileName, m_ObjectFileExtension) != NULL); }
	static bool IsMaterialExport(const char* fileName) { return (strstr(fileName, m_ExportFileExtension) != NULL); }
	static bool IsMaterialPreset(const char* fileName) { return (IsMaterialTemplate(fileName) || IsMaterialSurface(fileName) || IsMaterialObject(fileName) || IsMaterialExport(fileName)); }
	
	static MaterialPresetManager& GetCurrent();

	static char* m_TemplateFileExtension;
	static char* m_SurfaceFileExtension;
	static char* m_ObjectFileExtension;
	static char* m_ExportFileExtension;

private:
	void Init();
	bool IsInitialized() const { return m_Initialized; }
	void Shutdown();

	void LoadPresets( atArray<MaterialPreset*>& presets, const atString& presetPath );
	MaterialPreset* MergePresetRec(const atString& presetPath, bool reload = false);
	bool FileExists( const char * filename );

	atString m_MaterialMetadataPath;
	atString m_AssetPath;
	atString sMaterialPresetManagerErrors;

	atMap<atString, MaterialPreset*> m_PresetLibrary;

	atString					m_PresetPath;
	atArray<MaterialPreset*>	m_MasterPresets;
	atArray<MaterialPreset*>	m_TemplatePresets;
	atArray<MaterialPreset*>	m_SurfacePresets;

	atString					m_CachedSavePath;

	atString					m_MasterPath;
	atString					m_TemplatePath;
	atString					m_SurfacePath;

	bool						m_Initialized;
};
