﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

using AutomatedBuildShared;

namespace AutomatedBuild
{
    public enum BuildState
    {
        Idle,
        Active,
        Aborting
    }

    public partial class BuildInterface : Form
    {
        public BuildInterface(bool autoStart)
        {
            InitializeComponent();

            BuildFilePath = null;
            BuildName = null;
            ShelvedChangelistList = new List<int>();

            BuildStart = null;

            executeBuildButton.Enabled = false;
            m_BuildState = BuildState.Idle;

            clientListPanel.VerticalScroll.Enabled = true;
            clientListPanel.HorizontalScroll.Enabled = false;
        }

        private Thread m_BuildThread;
        private BuildState m_BuildState;

        private string m_BuildName;
        private string BuildName
        {
            get { return m_BuildName; }
            set
            {
                m_BuildName = value;
                if (String.IsNullOrEmpty(m_BuildName) == true)
                {
                    buildNameTextLabel.Text = "<none>";
                    m_BuildName = null;
                    executeBuildButton.Enabled = false;
                }
                else
                {
                    buildNameTextLabel.Text = m_BuildName;
                    executeBuildButton.Enabled = true;
                }
            }
        }

        private string m_BuildFilePath;
        private string BuildFilePath
        {
            get { return m_BuildFilePath; }
            set 
            {
                m_BuildFilePath = value;

                if ( String.IsNullOrEmpty(m_BuildFilePath) == true )
                {
                    m_BuildFilePath = null;
                }
                else
                {
                    buildNameTextLabel.Text = m_BuildFilePath;
                }
            }
        }

        private List<int> ShelvedChangelistList;

        //Delegates and Events
        public delegate bool LoadBuildCallback(string buildFile);
        public event LoadBuildCallback BuildLoad;

        public delegate bool StartBuildCallback(string buildFile, bool clean, bool rebuild, List<int> shelvedChangelists, bool retrieveChangelist, bool submitChangelist);
        public event StartBuildCallback BuildStart;

        public delegate void AbortBuildCallback();
        public event AbortBuildCallback BuildAbort;

        private delegate void InvokeOutputReceived(string output);
        private delegate void InvokeUpdate(BuildInfo info);
        private delegate void InvokeEvent();

        private void Update(BuildInfo info)
        {
            BuildName = info.Name;
            currentClientsNumberLabel.Text = info.UsedClients.ToString();
            activeClientNumberLabel.Text = info.ActiveClients.ToString();
            queuedClientsNumberLabel.Text = info.QueuedClients.ToString();
            totalTasksLabelText.Text = info.TotalTasks.ToString();
            tasksCompletedLabelText.Text = info.TasksCompleted.ToString();
            tasksRemainingLabelText.Text = info.TasksRemaining.ToString();
            errorCountLabelText.Text = info.ErrorCount.ToString();
            warningCountLabelText.Text = info.WarningCount.ToString();

            if (clientListPanel.Controls.Count == 0)
            {
                //Create the panels only once.
                foreach (ClientInfo clientInfo in info.ClientInfoList)
                {
                    ClientInfoPanel infoPanel = new ClientInfoPanel(clientInfo);
                    clientListPanel.Controls.Add(infoPanel);
                }
            }
            else
            {
                int index = 0;
                foreach (ClientInfo clientInfo in info.ClientInfoList)
                {
                    ClientInfoPanel clientInfoPanel = (ClientInfoPanel)clientListPanel.Controls[index++];
                    clientInfoPanel.m_ClientInfo = clientInfo;
                    clientInfoPanel.UpdateText();
                }
            }

            retrieveChangelistCheckbox.Checked = info.RetrieveChangelistSetting;
            submitChangelistCheckbox.Checked = info.SubmitChangelistSetting;
        }

        public void Log(string output)
        {
            outputTextBox.AppendText(output);
        }

        public void LogError(string output)
        {
            errorOutputTextBox.AppendText(output);
        }

        public void Reset()
        {
            currentClientsNumberLabel.Text = "0";
            activeClientNumberLabel.Text = "0";
            queuedClientsNumberLabel.Text = "0";

            totalTasksLabelText.Text = "0";
            tasksCompletedLabelText.Text = "0";
            tasksRemainingLabelText.Text = "0";

            errorCountLabelText.Text = "0";
            warningCountLabelText.Text = "0";

            clientListPanel.Controls.Clear();
        }

        public void OnOutputReceived(string output, MessageType type)
        {
            if (outputTextBox.InvokeRequired == true)
            {
                InvokeOutputReceived invoke = delegate(string invokeOutput)
                {
                    this.OnOutputReceived(invokeOutput, type);
                };
  
                object[] args = new object[1];
                args[0] = output;
                this.Invoke(invoke, args);
            }
            else
            {
                if (type != MessageType.Error && type != MessageType.Warning)
                {
                    Log(output);
                }

                if (type == MessageType.TaskNotification || type == MessageType.Error || type == MessageType.Warning)
                {
                    LogError(output);
                }
            }
        }

        public void OnBuildStarted()
        {
            if (outputTextBox.InvokeRequired == true)
            {
                InvokeEvent invoke = delegate()
                {
                    m_BuildState = BuildState.Active;
                    executeBuildButton.Text = "Abort Build";
                    this.outputTextBox.Clear();
                };

                outputTextBox.Invoke(invoke);
            }
            else
            {
                m_BuildState = BuildState.Active;
                executeBuildButton.Text = "Abort Build";
                outputTextBox.Clear();
            }            
        }

        public void OnBuildUpdate(BuildInfo info)
        {
            if (this.InvokeRequired == true)
            {
                InvokeUpdate invoke = delegate(BuildInfo buildInfo)
                {
                    this.Update(buildInfo);
                };
                object[] args = new object[1];
                args[0] = info;
                this.Invoke(invoke, args);
            }
            else
            {
                Update(info);
            }
        }

        public void OnBuildFinished()
        {
            if (this.InvokeRequired == true)
            {
                InvokeEvent invoke = delegate()
                {
                    this.BuildFinishedCallback();
                };

                Invoke(invoke);
            }
            else
            {
                BuildFinishedCallback();
            }
        }

        public void BuildFinishedCallback()
        {
            Reset();

            m_BuildThread = null;
            m_BuildState = BuildState.Idle;
            executeBuildButton.Text = "Execute Build";
            executeBuildButton.Enabled = true;
            cleanCheckBox.Enabled = true;
            rebuildCheckBox.Enabled = true;
            retrieveChangelistCheckbox.Enabled = true;
            shelveChangelistTextBox.Enabled = true;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void loadBuildFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            DialogResult dialogResult = openFileDialog.ShowDialog();

            if ( dialogResult == DialogResult.OK )
            {
                outputTextBox.Clear();
                errorOutputTextBox.Clear();
                clientListPanel.Controls.Clear();

                if ( BuildLoad != null )
                {
                    BuildFilePath = openFileDialog.FileName;
                    BuildLoad(openFileDialog.FileName);
                }
            }
        }

        private void executeBuildButton_Click(object sender, EventArgs e)
        {
            if (m_BuildState == BuildState.Idle)
            {
                Reset();  //Clears the interface.

                executeBuildButton.Text = "Abort Build";
                m_BuildState = BuildState.Active;
                cleanCheckBox.Enabled = false;
                rebuildCheckBox.Enabled = false;
                retrieveChangelistCheckbox.Enabled = false;
                shelveChangelistTextBox.Enabled = false;

                ShelvedChangelistList.Clear();
                if (String.Compare(shelveChangelistTextBox.Text, "") != 0)
                {
                    string[] changelists = shelveChangelistTextBox.Text.Split(',');
                    foreach (string changelist in changelists)
                    {
                        int changelistInt = -1;
                        if (Int32.TryParse(changelist, out changelistInt) == false)
                        {
                            Log("Shelve Changelist \"" + changelist + "\" is not valid. Unable to continue.");
                            return;
                        }

                        ShelvedChangelistList.Add(changelistInt);
                    }
                }

                if (String.IsNullOrEmpty(m_BuildFilePath) == true)
                {
                    Log("Build file path is not valid.  Unable to continue.");
                    return;
                }
                if (File.Exists(m_BuildFilePath) == false)
                {
                    Log("Unable to find build file " + m_BuildFilePath + ".");
                    return;
                }

                if (BuildStart != null)
                {
                    m_BuildThread = new Thread(BuildThread);
                    m_BuildThread.Name = "Automated Build Thread";
                    m_BuildThread.Start();
                }
                else
                {
                    Log("BuildStart event is not defined!  Unable to execute build.");
                    return;
                }
            }
            else if (m_BuildState == BuildState.Active)
            {
                BuildAbort();

                m_BuildThread = null;
                executeBuildButton.Text = "Aborting...";
                executeBuildButton.Enabled = false;
            }
        }

        private void BuildThread()
        {            
            BuildStart(m_BuildFilePath, cleanCheckBox.Checked, rebuildCheckBox.Checked, ShelvedChangelistList, retrieveChangelistCheckbox.Checked, submitChangelistCheckbox.Checked);
        }

        public void Shutdown()
        {
            Close();
        }
    }
}
