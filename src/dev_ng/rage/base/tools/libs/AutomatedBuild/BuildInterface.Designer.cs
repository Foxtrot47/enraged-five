﻿namespace AutomatedBuild
{
    partial class BuildInterface
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadBuildFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outputTextBox = new System.Windows.Forms.RichTextBox();
            this.activeClientsLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.activeClientNumberLabel = new System.Windows.Forms.Label();
            this.queuedClientsNumberLabel = new System.Windows.Forms.Label();
            this.currentClientsNumberLabel = new System.Windows.Forms.Label();
            this.currentClientsLabel = new System.Windows.Forms.Label();
            this.executeBuildButton = new System.Windows.Forms.Button();
            this.buildStatisticsGroupBox = new System.Windows.Forms.GroupBox();
            this.warningCountLabelText = new System.Windows.Forms.Label();
            this.warningCountLabel = new System.Windows.Forms.Label();
            this.errorCountLabelText = new System.Windows.Forms.Label();
            this.errorCountLabel = new System.Windows.Forms.Label();
            this.totalTasksLabelText = new System.Windows.Forms.Label();
            this.totalTasksLabel = new System.Windows.Forms.Label();
            this.tasksCompletedLabelText = new System.Windows.Forms.Label();
            this.tasksCompletedLabel = new System.Windows.Forms.Label();
            this.tasksRemainingLabelText = new System.Windows.Forms.Label();
            this.tasksRemainingLabel = new System.Windows.Forms.Label();
            this.buildNameTextLabel = new System.Windows.Forms.Label();
            this.buildNameLabel = new System.Windows.Forms.Label();
            this.outputTabControl = new System.Windows.Forms.TabControl();
            this.summaryOutputTab = new System.Windows.Forms.TabPage();
            this.errorsOutputTab = new System.Windows.Forms.TabPage();
            this.errorOutputTextBox = new System.Windows.Forms.RichTextBox();
            this.cleanCheckBox = new System.Windows.Forms.CheckBox();
            this.clientListPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.shelveChangelistTextBox = new System.Windows.Forms.TextBox();
            this.shelveChangelistLabel = new System.Windows.Forms.Label();
            this.optionsGroupBox = new System.Windows.Forms.GroupBox();
            this.retrieveChangelistCheckbox = new System.Windows.Forms.CheckBox();
            this.rebuildCheckBox = new System.Windows.Forms.CheckBox();
            this.submitChangelistCheckbox = new System.Windows.Forms.CheckBox();
            this.menuStrip1.SuspendLayout();
            this.buildStatisticsGroupBox.SuspendLayout();
            this.outputTabControl.SuspendLayout();
            this.summaryOutputTab.SuspendLayout();
            this.errorsOutputTab.SuspendLayout();
            this.optionsGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(681, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadBuildFileToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // loadBuildFileToolStripMenuItem
            // 
            this.loadBuildFileToolStripMenuItem.Name = "loadBuildFileToolStripMenuItem";
            this.loadBuildFileToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.loadBuildFileToolStripMenuItem.Text = "Load Build File";
            this.loadBuildFileToolStripMenuItem.Click += new System.EventHandler(this.loadBuildFileToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // outputTextBox
            // 
            this.outputTextBox.Location = new System.Drawing.Point(6, 6);
            this.outputTextBox.Name = "outputTextBox";
            this.outputTextBox.ReadOnly = true;
            this.outputTextBox.Size = new System.Drawing.Size(473, 208);
            this.outputTextBox.TabIndex = 1;
            this.outputTextBox.Text = "";
            // 
            // activeClientsLabel
            // 
            this.activeClientsLabel.AutoSize = true;
            this.activeClientsLabel.Location = new System.Drawing.Point(20, 67);
            this.activeClientsLabel.Name = "activeClientsLabel";
            this.activeClientsLabel.Size = new System.Drawing.Size(34, 13);
            this.activeClientsLabel.TabIndex = 2;
            this.activeClientsLabel.Text = "Total:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Queued:";
            // 
            // activeClientNumberLabel
            // 
            this.activeClientNumberLabel.AutoSize = true;
            this.activeClientNumberLabel.Location = new System.Drawing.Point(70, 67);
            this.activeClientNumberLabel.Name = "activeClientNumberLabel";
            this.activeClientNumberLabel.Size = new System.Drawing.Size(13, 13);
            this.activeClientNumberLabel.TabIndex = 4;
            this.activeClientNumberLabel.Text = "0";
            // 
            // queuedClientsNumberLabel
            // 
            this.queuedClientsNumberLabel.AutoSize = true;
            this.queuedClientsNumberLabel.Location = new System.Drawing.Point(70, 46);
            this.queuedClientsNumberLabel.Name = "queuedClientsNumberLabel";
            this.queuedClientsNumberLabel.Size = new System.Drawing.Size(13, 13);
            this.queuedClientsNumberLabel.TabIndex = 5;
            this.queuedClientsNumberLabel.Text = "0";
            // 
            // currentClientsNumberLabel
            // 
            this.currentClientsNumberLabel.AutoSize = true;
            this.currentClientsNumberLabel.Location = new System.Drawing.Point(70, 25);
            this.currentClientsNumberLabel.Name = "currentClientsNumberLabel";
            this.currentClientsNumberLabel.Size = new System.Drawing.Size(13, 13);
            this.currentClientsNumberLabel.TabIndex = 7;
            this.currentClientsNumberLabel.Text = "0";
            // 
            // currentClientsLabel
            // 
            this.currentClientsLabel.AutoSize = true;
            this.currentClientsLabel.Location = new System.Drawing.Point(14, 25);
            this.currentClientsLabel.Name = "currentClientsLabel";
            this.currentClientsLabel.Size = new System.Drawing.Size(40, 13);
            this.currentClientsLabel.TabIndex = 6;
            this.currentClientsLabel.Text = "Active:";
            // 
            // executeBuildButton
            // 
            this.executeBuildButton.Location = new System.Drawing.Point(367, 402);
            this.executeBuildButton.Name = "executeBuildButton";
            this.executeBuildButton.Size = new System.Drawing.Size(128, 23);
            this.executeBuildButton.TabIndex = 8;
            this.executeBuildButton.Text = "Execute Build";
            this.executeBuildButton.UseVisualStyleBackColor = true;
            this.executeBuildButton.Click += new System.EventHandler(this.executeBuildButton_Click);
            // 
            // buildStatisticsGroupBox
            // 
            this.buildStatisticsGroupBox.Controls.Add(this.warningCountLabelText);
            this.buildStatisticsGroupBox.Controls.Add(this.warningCountLabel);
            this.buildStatisticsGroupBox.Controls.Add(this.errorCountLabelText);
            this.buildStatisticsGroupBox.Controls.Add(this.errorCountLabel);
            this.buildStatisticsGroupBox.Controls.Add(this.totalTasksLabelText);
            this.buildStatisticsGroupBox.Controls.Add(this.totalTasksLabel);
            this.buildStatisticsGroupBox.Controls.Add(this.tasksCompletedLabelText);
            this.buildStatisticsGroupBox.Controls.Add(this.tasksCompletedLabel);
            this.buildStatisticsGroupBox.Controls.Add(this.tasksRemainingLabelText);
            this.buildStatisticsGroupBox.Controls.Add(this.tasksRemainingLabel);
            this.buildStatisticsGroupBox.Controls.Add(this.queuedClientsNumberLabel);
            this.buildStatisticsGroupBox.Controls.Add(this.activeClientsLabel);
            this.buildStatisticsGroupBox.Controls.Add(this.label1);
            this.buildStatisticsGroupBox.Controls.Add(this.activeClientNumberLabel);
            this.buildStatisticsGroupBox.Controls.Add(this.currentClientsNumberLabel);
            this.buildStatisticsGroupBox.Controls.Add(this.currentClientsLabel);
            this.buildStatisticsGroupBox.Location = new System.Drawing.Point(12, 53);
            this.buildStatisticsGroupBox.Name = "buildStatisticsGroupBox";
            this.buildStatisticsGroupBox.Size = new System.Drawing.Size(312, 91);
            this.buildStatisticsGroupBox.TabIndex = 11;
            this.buildStatisticsGroupBox.TabStop = false;
            this.buildStatisticsGroupBox.Text = "Build Statistics";
            // 
            // warningCountLabelText
            // 
            this.warningCountLabelText.AutoSize = true;
            this.warningCountLabelText.Location = new System.Drawing.Point(288, 46);
            this.warningCountLabelText.Name = "warningCountLabelText";
            this.warningCountLabelText.Size = new System.Drawing.Size(13, 13);
            this.warningCountLabelText.TabIndex = 17;
            this.warningCountLabelText.Text = "0";
            // 
            // warningCountLabel
            // 
            this.warningCountLabel.AutoSize = true;
            this.warningCountLabel.Location = new System.Drawing.Point(222, 46);
            this.warningCountLabel.Name = "warningCountLabel";
            this.warningCountLabel.Size = new System.Drawing.Size(55, 13);
            this.warningCountLabel.TabIndex = 16;
            this.warningCountLabel.Text = "Warnings:";
            // 
            // errorCountLabelText
            // 
            this.errorCountLabelText.AutoSize = true;
            this.errorCountLabelText.Location = new System.Drawing.Point(288, 25);
            this.errorCountLabelText.Name = "errorCountLabelText";
            this.errorCountLabelText.Size = new System.Drawing.Size(13, 13);
            this.errorCountLabelText.TabIndex = 15;
            this.errorCountLabelText.Text = "0";
            // 
            // errorCountLabel
            // 
            this.errorCountLabel.AutoSize = true;
            this.errorCountLabel.Location = new System.Drawing.Point(222, 25);
            this.errorCountLabel.Name = "errorCountLabel";
            this.errorCountLabel.Size = new System.Drawing.Size(37, 13);
            this.errorCountLabel.TabIndex = 14;
            this.errorCountLabel.Text = "Errors:";
            // 
            // totalTasksLabelText
            // 
            this.totalTasksLabelText.AutoSize = true;
            this.totalTasksLabelText.Location = new System.Drawing.Point(191, 67);
            this.totalTasksLabelText.Name = "totalTasksLabelText";
            this.totalTasksLabelText.Size = new System.Drawing.Size(13, 13);
            this.totalTasksLabelText.TabIndex = 13;
            this.totalTasksLabelText.Text = "0";
            // 
            // totalTasksLabel
            // 
            this.totalTasksLabel.AutoSize = true;
            this.totalTasksLabel.Location = new System.Drawing.Point(93, 67);
            this.totalTasksLabel.Name = "totalTasksLabel";
            this.totalTasksLabel.Size = new System.Drawing.Size(66, 13);
            this.totalTasksLabel.TabIndex = 12;
            this.totalTasksLabel.Text = "Total Tasks:";
            // 
            // tasksCompletedLabelText
            // 
            this.tasksCompletedLabelText.AutoSize = true;
            this.tasksCompletedLabelText.Location = new System.Drawing.Point(191, 46);
            this.tasksCompletedLabelText.Name = "tasksCompletedLabelText";
            this.tasksCompletedLabelText.Size = new System.Drawing.Size(13, 13);
            this.tasksCompletedLabelText.TabIndex = 11;
            this.tasksCompletedLabelText.Text = "0";
            // 
            // tasksCompletedLabel
            // 
            this.tasksCompletedLabel.AutoSize = true;
            this.tasksCompletedLabel.Location = new System.Drawing.Point(93, 46);
            this.tasksCompletedLabel.Name = "tasksCompletedLabel";
            this.tasksCompletedLabel.Size = new System.Drawing.Size(92, 13);
            this.tasksCompletedLabel.TabIndex = 10;
            this.tasksCompletedLabel.Text = "Tasks Completed:";
            // 
            // tasksRemainingLabelText
            // 
            this.tasksRemainingLabelText.AutoSize = true;
            this.tasksRemainingLabelText.Location = new System.Drawing.Point(191, 25);
            this.tasksRemainingLabelText.Name = "tasksRemainingLabelText";
            this.tasksRemainingLabelText.Size = new System.Drawing.Size(13, 13);
            this.tasksRemainingLabelText.TabIndex = 9;
            this.tasksRemainingLabelText.Text = "0";
            // 
            // tasksRemainingLabel
            // 
            this.tasksRemainingLabel.AutoSize = true;
            this.tasksRemainingLabel.Location = new System.Drawing.Point(93, 25);
            this.tasksRemainingLabel.Name = "tasksRemainingLabel";
            this.tasksRemainingLabel.Size = new System.Drawing.Size(92, 13);
            this.tasksRemainingLabel.TabIndex = 8;
            this.tasksRemainingLabel.Text = "Tasks Remaining:";
            // 
            // buildNameTextLabel
            // 
            this.buildNameTextLabel.AutoSize = true;
            this.buildNameTextLabel.Location = new System.Drawing.Point(82, 33);
            this.buildNameTextLabel.Name = "buildNameTextLabel";
            this.buildNameTextLabel.Size = new System.Drawing.Size(61, 13);
            this.buildNameTextLabel.TabIndex = 13;
            this.buildNameTextLabel.Text = "Build Name";
            // 
            // buildNameLabel
            // 
            this.buildNameLabel.AutoSize = true;
            this.buildNameLabel.Location = new System.Drawing.Point(12, 33);
            this.buildNameLabel.Name = "buildNameLabel";
            this.buildNameLabel.Size = new System.Drawing.Size(64, 13);
            this.buildNameLabel.TabIndex = 12;
            this.buildNameLabel.Text = "Build Name:";
            // 
            // outputTabControl
            // 
            this.outputTabControl.Controls.Add(this.summaryOutputTab);
            this.outputTabControl.Controls.Add(this.errorsOutputTab);
            this.outputTabControl.Location = new System.Drawing.Point(12, 150);
            this.outputTabControl.Name = "outputTabControl";
            this.outputTabControl.SelectedIndex = 0;
            this.outputTabControl.Size = new System.Drawing.Size(490, 246);
            this.outputTabControl.TabIndex = 14;
            // 
            // summaryOutputTab
            // 
            this.summaryOutputTab.Controls.Add(this.outputTextBox);
            this.summaryOutputTab.Location = new System.Drawing.Point(4, 22);
            this.summaryOutputTab.Name = "summaryOutputTab";
            this.summaryOutputTab.Padding = new System.Windows.Forms.Padding(3);
            this.summaryOutputTab.Size = new System.Drawing.Size(482, 220);
            this.summaryOutputTab.TabIndex = 0;
            this.summaryOutputTab.Text = "Summary";
            this.summaryOutputTab.UseVisualStyleBackColor = true;
            // 
            // errorsOutputTab
            // 
            this.errorsOutputTab.Controls.Add(this.errorOutputTextBox);
            this.errorsOutputTab.Location = new System.Drawing.Point(4, 22);
            this.errorsOutputTab.Name = "errorsOutputTab";
            this.errorsOutputTab.Padding = new System.Windows.Forms.Padding(3);
            this.errorsOutputTab.Size = new System.Drawing.Size(482, 220);
            this.errorsOutputTab.TabIndex = 1;
            this.errorsOutputTab.Text = "Errors";
            this.errorsOutputTab.UseVisualStyleBackColor = true;
            // 
            // errorOutputTextBox
            // 
            this.errorOutputTextBox.Location = new System.Drawing.Point(6, 6);
            this.errorOutputTextBox.Name = "errorOutputTextBox";
            this.errorOutputTextBox.ReadOnly = true;
            this.errorOutputTextBox.Size = new System.Drawing.Size(473, 208);
            this.errorOutputTextBox.TabIndex = 15;
            this.errorOutputTextBox.Text = "";
            // 
            // cleanCheckBox
            // 
            this.cleanCheckBox.AutoSize = true;
            this.cleanCheckBox.Location = new System.Drawing.Point(6, 19);
            this.cleanCheckBox.Name = "cleanCheckBox";
            this.cleanCheckBox.Size = new System.Drawing.Size(53, 17);
            this.cleanCheckBox.TabIndex = 15;
            this.cleanCheckBox.Text = "Clean";
            this.cleanCheckBox.UseVisualStyleBackColor = true;
            // 
            // clientListPanel
            // 
            this.clientListPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.clientListPanel.AutoScroll = true;
            this.clientListPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.clientListPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.clientListPanel.Location = new System.Drawing.Point(505, 33);
            this.clientListPanel.Margin = new System.Windows.Forms.Padding(0);
            this.clientListPanel.Name = "clientListPanel";
            this.clientListPanel.Size = new System.Drawing.Size(176, 392);
            this.clientListPanel.TabIndex = 19;
            this.clientListPanel.WrapContents = false;
            // 
            // shelveChangelistTextBox
            // 
            this.shelveChangelistTextBox.Location = new System.Drawing.Point(103, 40);
            this.shelveChangelistTextBox.Name = "shelveChangelistTextBox";
            this.shelveChangelistTextBox.Size = new System.Drawing.Size(62, 20);
            this.shelveChangelistTextBox.TabIndex = 20;
            // 
            // shelveChangelistLabel
            // 
            this.shelveChangelistLabel.AutoSize = true;
            this.shelveChangelistLabel.Location = new System.Drawing.Point(6, 43);
            this.shelveChangelistLabel.Name = "shelveChangelistLabel";
            this.shelveChangelistLabel.Size = new System.Drawing.Size(95, 13);
            this.shelveChangelistLabel.TabIndex = 18;
            this.shelveChangelistLabel.Text = "Shelve Changelist:";
            // 
            // optionsGroupBox
            // 
            this.optionsGroupBox.Controls.Add(this.submitChangelistCheckbox);
            this.optionsGroupBox.Controls.Add(this.retrieveChangelistCheckbox);
            this.optionsGroupBox.Controls.Add(this.rebuildCheckBox);
            this.optionsGroupBox.Controls.Add(this.cleanCheckBox);
            this.optionsGroupBox.Controls.Add(this.shelveChangelistLabel);
            this.optionsGroupBox.Controls.Add(this.shelveChangelistTextBox);
            this.optionsGroupBox.Location = new System.Drawing.Point(330, 53);
            this.optionsGroupBox.Name = "optionsGroupBox";
            this.optionsGroupBox.Size = new System.Drawing.Size(172, 113);
            this.optionsGroupBox.TabIndex = 21;
            this.optionsGroupBox.TabStop = false;
            this.optionsGroupBox.Text = "Options:";
            // 
            // retrieveChangelistCheckbox
            // 
            this.retrieveChangelistCheckbox.AutoSize = true;
            this.retrieveChangelistCheckbox.Location = new System.Drawing.Point(8, 65);
            this.retrieveChangelistCheckbox.Name = "retrieveChangelistCheckbox";
            this.retrieveChangelistCheckbox.Size = new System.Drawing.Size(118, 17);
            this.retrieveChangelistCheckbox.TabIndex = 22;
            this.retrieveChangelistCheckbox.Text = "Retrieve Changelist";
            this.retrieveChangelistCheckbox.UseVisualStyleBackColor = true;
            // 
            // rebuildCheckBox
            // 
            this.rebuildCheckBox.AutoSize = true;
            this.rebuildCheckBox.Location = new System.Drawing.Point(65, 19);
            this.rebuildCheckBox.Name = "rebuildCheckBox";
            this.rebuildCheckBox.Size = new System.Drawing.Size(62, 17);
            this.rebuildCheckBox.TabIndex = 21;
            this.rebuildCheckBox.Text = "Rebuild";
            this.rebuildCheckBox.UseVisualStyleBackColor = true;
            // 
            // submitChangelistCheckbox
            // 
            this.submitChangelistCheckbox.AutoSize = true;
            this.submitChangelistCheckbox.Location = new System.Drawing.Point(6, 88);
            this.submitChangelistCheckbox.Name = "submitChangelistCheckbox";
            this.submitChangelistCheckbox.Size = new System.Drawing.Size(110, 17);
            this.submitChangelistCheckbox.TabIndex = 23;
            this.submitChangelistCheckbox.Text = "Submit Changelist";
            this.submitChangelistCheckbox.UseVisualStyleBackColor = true;
            // 
            // BuildInterface
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(681, 437);
            this.Controls.Add(this.optionsGroupBox);
            this.Controls.Add(this.clientListPanel);
            this.Controls.Add(this.outputTabControl);
            this.Controls.Add(this.buildNameTextLabel);
            this.Controls.Add(this.buildNameLabel);
            this.Controls.Add(this.buildStatisticsGroupBox);
            this.Controls.Add(this.executeBuildButton);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "BuildInterface";
            this.Text = "Automated Build";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.buildStatisticsGroupBox.ResumeLayout(false);
            this.buildStatisticsGroupBox.PerformLayout();
            this.outputTabControl.ResumeLayout(false);
            this.summaryOutputTab.ResumeLayout(false);
            this.errorsOutputTab.ResumeLayout(false);
            this.optionsGroupBox.ResumeLayout(false);
            this.optionsGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.RichTextBox outputTextBox;
        private System.Windows.Forms.Label activeClientsLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label activeClientNumberLabel;
        private System.Windows.Forms.Label queuedClientsNumberLabel;
        private System.Windows.Forms.Label currentClientsNumberLabel;
        private System.Windows.Forms.Label currentClientsLabel;
        private System.Windows.Forms.Button executeBuildButton;
        private System.Windows.Forms.GroupBox buildStatisticsGroupBox;
        private System.Windows.Forms.ToolStripMenuItem loadBuildFileToolStripMenuItem;
        private System.Windows.Forms.Label tasksCompletedLabelText;
        private System.Windows.Forms.Label tasksCompletedLabel;
        private System.Windows.Forms.Label tasksRemainingLabelText;
        private System.Windows.Forms.Label tasksRemainingLabel;
        private System.Windows.Forms.Label buildNameTextLabel;
        private System.Windows.Forms.Label buildNameLabel;
        private System.Windows.Forms.Label totalTasksLabelText;
        private System.Windows.Forms.Label totalTasksLabel;
        private System.Windows.Forms.Label errorCountLabelText;
        private System.Windows.Forms.Label errorCountLabel;
        private System.Windows.Forms.Label warningCountLabelText;
        private System.Windows.Forms.Label warningCountLabel;
        private System.Windows.Forms.TabControl outputTabControl;
        private System.Windows.Forms.TabPage summaryOutputTab;
        private System.Windows.Forms.TabPage errorsOutputTab;
        private System.Windows.Forms.RichTextBox errorOutputTextBox;
        private System.Windows.Forms.CheckBox cleanCheckBox;
        private System.Windows.Forms.FlowLayoutPanel clientListPanel;
        private System.Windows.Forms.TextBox shelveChangelistTextBox;
        private System.Windows.Forms.Label shelveChangelistLabel;
        private System.Windows.Forms.GroupBox optionsGroupBox;
        private System.Windows.Forms.CheckBox rebuildCheckBox;
        private System.Windows.Forms.CheckBox retrieveChangelistCheckbox;
        private System.Windows.Forms.CheckBox submitChangelistCheckbox;
    }
}