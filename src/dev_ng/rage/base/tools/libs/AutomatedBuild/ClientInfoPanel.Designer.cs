﻿namespace AutomatedBuild
{
    partial class ClientInfoPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ClientNameLabel = new System.Windows.Forms.Label();
            this.StatusLabel = new System.Windows.Forms.Label();
            this.TaskTextBox = new System.Windows.Forms.RichTextBox();
            this.TimeLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ClientNameLabel
            // 
            this.ClientNameLabel.AutoSize = true;
            this.ClientNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClientNameLabel.Location = new System.Drawing.Point(3, 5);
            this.ClientNameLabel.Name = "ClientNameLabel";
            this.ClientNameLabel.Size = new System.Drawing.Size(75, 13);
            this.ClientNameLabel.TabIndex = 0;
            this.ClientNameLabel.Text = "Client Name";
            // 
            // StatusLabel
            // 
            this.StatusLabel.AutoSize = true;
            this.StatusLabel.Location = new System.Drawing.Point(4, 23);
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(66, 13);
            this.StatusLabel.TabIndex = 1;
            this.StatusLabel.Text = "Status Label";
            // 
            // TaskTextBox
            // 
            this.TaskTextBox.BackColor = System.Drawing.SystemColors.Control;
            this.TaskTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TaskTextBox.ForeColor = System.Drawing.SystemColors.WindowText;
            this.TaskTextBox.Location = new System.Drawing.Point(4, 40);
            this.TaskTextBox.Name = "TaskTextBox";
            this.TaskTextBox.ReadOnly = true;
            this.TaskTextBox.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.TaskTextBox.Size = new System.Drawing.Size(115, 32);
            this.TaskTextBox.TabIndex = 2;
            this.TaskTextBox.Text = "";
            // 
            // TimeLabel
            // 
            this.TimeLabel.AutoSize = true;
            this.TimeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TimeLabel.Location = new System.Drawing.Point(89, 5);
            this.TimeLabel.Name = "TimeLabel";
            this.TimeLabel.Size = new System.Drawing.Size(49, 13);
            this.TimeLabel.TabIndex = 3;
            this.TimeLabel.Text = "00:00:00";
            // 
            // ClientInfoPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.TimeLabel);
            this.Controls.Add(this.TaskTextBox);
            this.Controls.Add(this.StatusLabel);
            this.Controls.Add(this.ClientNameLabel);
            this.Name = "ClientInfoPanel";
            this.Size = new System.Drawing.Size(138, 72);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label ClientNameLabel;
        private System.Windows.Forms.Label StatusLabel;
        private System.Windows.Forms.RichTextBox TaskTextBox;
        private System.Windows.Forms.Label TimeLabel;
    }
}
