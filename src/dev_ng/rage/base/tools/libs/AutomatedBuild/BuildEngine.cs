﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Xml.Serialization;
using System.Xml.XPath;

using AutomatedBuildShared;
using RSG.SourceControl.Perforce;
using P4API;

namespace AutomatedBuild
{
    /// <summary>
    /// A pairing between a build client and it's associated thread.  Used for initialization
    /// and shutdown in the build engine.
    /// </summary>
    class ActiveClient
    {
        public BuildClient Client;
        public Thread ClientThread;
        
        public ActiveClient(BuildClient client, Thread clientThread)
        {
            Client = client;
            ClientThread = clientThread;
        }
    }

    /// <summary>
    /// Specialized list for active clients for ease of use.
    /// </summary>
    class ActiveClientList : List<ActiveClient>
    {
        public ActiveClientList() { }

        public bool Contains(BuildClient buildClient)
        {
            foreach (ActiveClient activeClient in this)
            {
                if (String.Compare(activeClient.Client.m_Name, buildClient.m_Name) == 0)
                {
                    return true;
                }
            }

            return false;
        }
    }

    /// <summary>
    /// The central processing engine of the Automated Build.  This class will initialize/controls clients, 
    /// assign tasks to the clients and interpret task results into XML and HTML formats.
    /// </summary>
    class BuildEngine
    {
        private BuildClients m_Clients;
        
        private object m_ActiveClientLock;
        private ActiveClientList m_ActiveClients;       //Clients busy with this build.

        private object m_QueuedClientLock;
        private ActiveClientList m_QueuedClients;       //List of clients that are busy with another build.

        private object m_ShutdownClientLock;
        private ActiveClientList m_ShutdownClients;     //Clients that have become disconnected during the build or unable to be initialized.        

        private List<Thread> m_DisonnectionThreads;

        private BuildTaskList m_TaskList;
        private List<ActiveTask> m_DispatchedTasks;     //Tasks that have been dispatched to clients.
        private List<ActiveTask> m_CompletedTasks;      //Tasks that have been dispatched and completed by the clients.
        public List<ActiveTask> m_FailedTasks;          //Failed tasks that have been dispatched and completed by the clients.
        private List<ActiveTask> m_TaskReportList;      //List of tasks to have reports created.

        public BuildFile m_BuildFile;

        public TaskHistoryFile m_TaskHistoryFile;
        public bool m_GenerateHistory;

        public string Label;                        ///A description of the build (generally the name of the build is a human-readable form).
        public int BuildVersion;                  ///An arbitrary integer value that describes the version of the build (or the number of times it's been catalogued and run).
        public string RootReportDirectory;
        public string ReportDirectory;
        public bool OverrideReportDirectory;
        private string ReportXSLDirectory;
        private string HistoryDirectory;
        public string ProjectStateFile;     ///Custom input file from Cruise Control that can be used to determine information about the build.
        public bool Clean;                  
        public bool Rebuild;
        public bool RetrieveChangelist;     ///Retrieves all changelists from build clients.  Placed them in its own changelist.
        public bool SubmitChangelist;       ///Submits the changelist, with contents from all build clients.
        public bool SubmitOnFailure;
        public bool CruiseControlFormatted; ///Formats the output to be interpreted by the Cruise Control builds; outputs text for e-mails.
        public bool EnableDebugMessages;
        public bool EnableStatusUpdates;

        //Debug functionality.
        private static bool LoopBuilds = false; //Debug:  Used for stress testing.

        private P4 m_P4;
        private AutomatedBuildShared.Changelist m_Changelist;    ///The changelist where all build clients dump their contents into.  Only used if RetrieveChangelist is true.
        private bool m_Abort;
        private DateTime m_SyncTime;
        private List<int> m_ShelvedChangelists;

        private DateTime m_StartTime;
        private DateTime m_EndTime;
        private int ErrorCount;
        private int WarningCount;

        //Timeouts
        private int AllQueuedTimeout = 1000 * 5;
        private int AllQueuedCountMax = 12;
        private int AllQueuedCount = 0;

        //User interface callbacks.
        public delegate void OutputReceivedCallback(string output, MessageType type);
        public event OutputReceivedCallback OutputReceived;

        public delegate void BuildEventCallback();
        public delegate void BuildInfoCallback(BuildInfo info);
        public event BuildEventCallback BuildStarted;
        public event BuildInfoCallback BuildUpdate;
        public event BuildEventCallback BuildFinished;

        public BuildEngine(string reportXSLDirectory, string historyDirectory)
        {
            Clean = false;
            Rebuild = false;
            RetrieveChangelist = false;
            SubmitChangelist = false;
            SubmitOnFailure = false;

            CruiseControlFormatted = false;
            Label = null;
            ProjectStateFile = null;

            m_Clients = new BuildClients();
            
            m_ActiveClientLock = new object();
            m_ActiveClients = new ActiveClientList();

            m_QueuedClientLock = new object();
            m_QueuedClients = new ActiveClientList();

            m_ShutdownClientLock = new object();
            m_ShutdownClients = new ActiveClientList();

            m_DisonnectionThreads = new List<Thread>();
            
            m_DispatchedTasks = new List<ActiveTask>();
            m_CompletedTasks = new List<ActiveTask>();
            m_FailedTasks = new List<ActiveTask>();
            m_TaskReportList = new List<ActiveTask>();
            m_TaskList = new BuildTaskList();

            m_Abort = false;

            OverrideReportDirectory = false;
            ReportXSLDirectory = reportXSLDirectory;
            HistoryDirectory = historyDirectory;

            EnableDebugMessages = false;
            EnableStatusUpdates = false;
            
            if (String.IsNullOrWhiteSpace(HistoryDirectory) == false && Directory.Exists(HistoryDirectory) == false)
            {
                Directory.CreateDirectory(HistoryDirectory);
            }

            BuildTask.RegisterTasks();
            BuildFile.OutputReceived += Log;
        }

        public bool Load(string buildFile)
        {
            Initialize();

            if (BuildFile.Load(buildFile, ref m_BuildFile) == false)
            {
                Log("Unable to load build file " + buildFile + "!", MessageType.FatalError);
                return false;
            }

            if (String.IsNullOrEmpty(m_BuildFile.Options.ClientFile) == true)
            {
                Log("Build file does not have a \"ClientFile\" attribute in its BuildOptions.  Unable to continue.", MessageType.FatalError);
                return false;
            }

            if (File.Exists(m_BuildFile.Options.ClientFile) == false)
            {
                Log("Client file " + m_BuildFile.Options.ClientFile + " does not exist.  Unable to start build!", MessageType.FatalError);
                return false;
            }

            try
            {
                //Deserialize the client file.
                XmlSerializer serializer = new XmlSerializer(typeof(BuildClients));
                StreamReader reader = new StreamReader(m_BuildFile.Options.ClientFile);
                m_Clients = (BuildClients)serializer.Deserialize(reader);
                reader.Close();
            }
            catch (Exception)
            {
                Log("Unable to deserialize client file " + m_BuildFile.Options.ClientFile, MessageType.FatalError);
                return false;
            }
            
            //Validate the client list.
            bool valid = true;
            foreach (BuildClient buildClient in m_Clients)
            {
                foreach(BuildClient otherBuildClient in m_Clients)
                {
                    if ( buildClient != otherBuildClient )
                    {
                        if (String.Compare(buildClient.m_MachineName, otherBuildClient.m_MachineName, true) == 0)
                        {
                            Log("Multiple client entries have the same machine name: " + buildClient.m_MachineName + ".  Unable to continue.", MessageType.Error);
                            valid = false;
                        }
                    }
                }
            }

            if (valid == false)
            {
                return false;
            }

            //Determine if the task file requires a history file.
            if (m_BuildFile.Options.Deterministic == true)
            {
                string historyFilePath = Path.Combine(HistoryDirectory, m_BuildFile.BuildName + ".xml");
                if (File.Exists(historyFilePath) == true)
                {
                    m_TaskHistoryFile = new TaskHistoryFile(m_BuildFile.Options.ClientFile, buildFile);
                    if (TaskHistoryFile.Load(historyFilePath, ref m_TaskHistoryFile) == false)
                    {
                        m_GenerateHistory = true;
                        m_TaskHistoryFile = new TaskHistoryFile(m_BuildFile.Options.ClientFile, buildFile);
                    }
                }
                else
                {
                    m_GenerateHistory = true;
                    m_TaskHistoryFile = new TaskHistoryFile(m_BuildFile.Options.ClientFile, buildFile);
                }
            }
            else
            {
                m_GenerateHistory = false;
                m_TaskHistoryFile = null;
            }

            if (Clean == true)
            {
                //Then the interface or command line has specified this build to clean.
                m_BuildFile.Options.Clean = Clean;
            }
            else //if (Clean == false)
            {
                Clean = m_BuildFile.Options.Clean;
            }

            if (Rebuild == true)
            {
                m_BuildFile.Options.Rebuild = Rebuild;
            }
            else
            {
                Rebuild = m_BuildFile.Options.Rebuild;
            }

            //TODO:  These defaults need to be cleaned up; options can be set in the UI and the build file.
            if (RetrieveChangelist == false)
                RetrieveChangelist = m_BuildFile.Options.RetrieveChangelist;

            if (SubmitChangelist == false)
                SubmitChangelist = m_BuildFile.Options.SubmitChangelist;

            if (SubmitOnFailure == false)
                SubmitOnFailure = m_BuildFile.Options.SubmitOnFailure;

            if (String.IsNullOrEmpty(Label) == true)
            {
                //Set the label to be the name within the build file.
                Label = m_BuildFile.BuildName;
            }

            Update(BuildStatus.Loading);
            return true;
        }

        private void Initialize()
        {
            m_Abort = false;
            m_DispatchedTasks.Clear();
            m_FailedTasks.Clear();
            m_CompletedTasks.Clear();
            m_TaskReportList.Clear();
            m_TaskList.Clear();

            lock (m_ActiveClientLock)
            {
                m_ActiveClients.Clear();
            }

            lock (m_QueuedClientLock)
            {
                m_QueuedClients.Clear();
            }

            lock (m_ShutdownClientLock)
            {
                m_ShutdownClients.Clear();
            }

            m_DisonnectionThreads.Clear();
        }

        public bool Start(string buildFile, bool clean, bool rebuild, List<int> shelvedChangelists, bool retrieveChangelist, bool submitChangelist)
        {
            Clean = clean;
            Rebuild = rebuild;
            m_ShelvedChangelists = shelvedChangelists;
            RetrieveChangelist = retrieveChangelist;
            SubmitChangelist = submitChangelist;

            //If we are submitting this changelist, then be sure to have it retrieved.
            if (SubmitChangelist == true)
                RetrieveChangelist = true;

            bool result = false;
            while (LoopBuilds == true && m_Abort == false)
            {
                result = Start(buildFile);
            }

            return Start(buildFile);
        }

        public bool Start(string buildFile)
        {
            if (BuildStarted != null)
            {
                BuildStarted();
            }

            Log("Starting build from file " + buildFile + ".", MessageType.EngineNotification);

            if (Load(buildFile) == false)
                return false;

            if (m_BuildFile.Tasks.Count == 0)
            {
                return true;
            }

            if (RetrieveChangelist == true)
            {
                BuildClient client = BuildClients.GetBuildClient(Environment.MachineName, m_BuildFile.Options.ClientFile);
                if (client != null)
                {
                    //HACK:  Ensure that the game-side Perforce settings are used.
                    client.m_PerforceInfo.Activate();
                    m_P4 = client.m_PerforceInfo.m_P4;
                    m_Changelist = new AutomatedBuildShared.Changelist(client.m_PerforceInfo.m_P4);

                    string changelistDescription = "Automated Build - " + Label;

                    try
                    {
                        if (m_BuildFile.HasValidBuildID())
                        {
                            RequestService.RequestInterfaceSoapClient requestClient = new RequestService.RequestInterfaceSoapClient();
                            RequestService.ArrayOfString cutsceneDescriptions = requestClient.GetCutsceneDescription(m_BuildFile.BuildID);

                            changelistDescription += "\n";
                            foreach (string description in cutsceneDescriptions)
                            {
                                changelistDescription += description + "\n";
                            }
                        }
                    }
                    catch (Exception)
                    {
                        //Could not connect to the database.  
                    }

                    if (m_Changelist.Create(changelistDescription) == true)
                        Log("Created Changelist " + m_Changelist.m_Number + " in Perforce.", MessageType.EngineNotification);
                    else
                    {
                        Log("Unable to create changelist in Perforce.", MessageType.FatalError);
                        return false;
                    }
                }
            }

            //If we have no tasks, then abort early.
            //This happens when the automated processes will run through  exports.
            if (m_BuildFile.Tasks.Count == 0)
            {
                Log("No tasks were found.", MessageType.Normal);
                return true;
            }

            //Initialize the reports directory.
            InitializeReports();

            m_StartTime = DateTime.Now;

            //Connect to clients.
            if (ConnectToClients() == false)
                return false;

            if (m_Abort == true)
            {
                Stop();
                return true;
            }

            //Shelve any changelists.
            ShelveChangelists(m_ShelvedChangelists);

            //Synchronize all active clients.
            if (InitializeClients() == false || m_Abort == true)
            {
                Stop();
                return m_Abort;
            }

            //Propagate any options declared in the engine to any of the tasks,
            //such as cleaning or rebuilding.
            if (InitializeTasks() == false || m_Abort == true)
            {
                Stop();
                return m_Abort;
            }

            //Process all the targets until they are all completed.
            ProcessTasks();

            //Generate the task history after all tasks have been processed.  Do not generate when the build has been aborted.
            if ( m_Abort == false )
                GenerateTaskHistory();
            
            //Disconnect from all clients.
            Stop();

            if (CruiseControlFormatted == true)
            {
                //In Cruise Control builds, supply a summary list of all tasks and their success or failure 
                //for the e-mail to display.
                //
                foreach (ActiveTask activeTask in m_CompletedTasks)
                {
                    if (activeTask.Task.Result.Success == true)
                    {
                        Log(activeTask.Task.ToShortString() + ": Success", MessageType.Normal);
                    }
                    else
                    {
                        Log(activeTask.Task.ToShortString() + ": Failed", MessageType.Normal);
                    }
                }
            }

            //Submit changelist if applicable to the build file.
            bool submitResult = Submit();

            //Declare the amount of time this build was running.
            m_EndTime = DateTime.Now;

            //Generates all reports associated with the tasks.
            GenerateSummaryReport();

            if (submitResult == false)
            {
                //State the build identification for manual repair.
                Log("Unable to submit changelist for build id: " + m_BuildFile.BuildID, MessageType.Normal);
            }

            //Only commit request information back to the database.
            CommitRequestInformation();

            TimeSpan timeElapsed = m_EndTime - m_StartTime;
            Log("Errors: " + ErrorCount + ", Warnings: " + WarningCount + ".", MessageType.Normal);
            Log("Time Elapsed: " + timeElapsed.ToString(), MessageType.Normal);

            return true;
        }

        /// <summary>
        /// Updates an intermediate object to be shuttled to the user interface. 
        /// </summary>
        /// <param name="status"></param>
        public void Update(BuildStatus status)
        {
            BuildInfo info = new BuildInfo();
            info.Status = status;
            foreach(ActiveClient client in m_ActiveClients)
            {
                if ( client.Client.Busy == true)
                {
                    info.UsedClients++;
                }
            }
            
            info.Name = m_BuildFile.BuildName;
            info.ActiveClients = m_ActiveClients.Count;
            info.QueuedClients = m_QueuedClients.Count;

            info.TasksCompleted = m_CompletedTasks.Count;
            info.TasksRemaining = m_TaskList.Count - m_CompletedTasks.Count;
            info.TotalTasks = m_TaskList.Count;

            ErrorCount = 0;
            WarningCount = 0;

            lock (m_DispatchedTasks)  //Used as a lock for both m_CompletedTasks and m_DispatchedTasks
            { 
                foreach (ActiveTask activeTask in m_CompletedTasks)
                {
                    BuildTask task = activeTask.Task;
                    ErrorCount += task.Result.ErrorCount;
                    WarningCount += task.Result.WarningCount;
                }
            }

            info.ErrorCount = ErrorCount;
            info.WarningCount = WarningCount;

            foreach (BuildClient client in m_Clients)
            {
                ClientState clientState = ClientState.Unknown;
                if (m_ActiveClients.Contains(client) == true)
                {
                    clientState = ClientState.Active;
                }
                else if (m_QueuedClients.Contains(client) == true)
                {
                    clientState = ClientState.Queued;
                }
                else
                {
                    clientState = ClientState.Shutdown;
                }

                string taskDescription = null;
                DateTime taskStartTime = DateTime.UtcNow;
                if (client.CurrentTask != null)
                {
                    taskDescription = client.CurrentTask.ToShortString();
                    taskStartTime = client.CurrentTask.StartTime;
                }

                ClientInfo clientInfo = new ClientInfo(client.m_Name, clientState, taskDescription, taskStartTime);
                clientInfo.Update();
                info.ClientInfoList.Add(clientInfo);
            }

            info.RetrieveChangelistSetting = RetrieveChangelist;
            info.SubmitChangelistSetting = SubmitChangelist;

            if (BuildUpdate != null)
            {
                BuildUpdate(info);
            }

            //Publish the current state of this build.
            GenerateStatusReport(info);
        }

        /// <summary>
        /// Disconnects from all the clients; notifies any attached user interfaces.
        /// </summary>
        public void Stop()
        {
            DisconnectClients();

            if (m_Abort == true)
                Log("Build has been aborted.", MessageType.EngineNotification);
            else
                Log("Build has been completed.", MessageType.EngineNotification);

            if (BuildFinished != null)
            {
                BuildFinished();
            }

            EnableDebugMessages = false;

            //Mark the build as complete.
            Update(BuildStatus.Complete);
        }

        /// <summary>
        /// Aborts the current build.
        /// </summary>
        public void Abort()
        {
            m_Abort = true;

            Stop();
        }

        /// <summary>
        /// Connects to all registered clients; initializes them.
        /// </summary>
        /// <returns></returns>
        private bool ConnectToClients()
        {
            Log("Connecting to all clients...", MessageType.Debug);
            foreach(BuildClient client in m_Clients)
            {
                client.Active = true;

                Thread clientThread = new Thread(ActiveClientThread);
                clientThread.Name = "Active Client Thread: " + client.m_MachineName;

                ActiveClient activeClient = new ActiveClient(client, clientThread);
                clientThread.Start(activeClient);
                m_ActiveClients.Add(activeClient);
            }

            Dictionary<BuildClient, bool> connectedStatus = new Dictionary<BuildClient, bool>();
            foreach (BuildClient client in m_Clients)
            {
                connectedStatus.Add(client, false);
            }

            int retryCount = 0;
            int retryLimit = 5;
            bool connectedToAllClients = false;
            while( connectedToAllClients == false && retryCount < retryLimit && m_Abort == false )
            {
                foreach (BuildClient client in m_Clients)
                {
                    lock (client)
                    {
                        connectedStatus[client] = client.IsConnected();

                        //If the client is connected but busy with another client, notify.
                    }
                }

                connectedToAllClients = true;
                foreach (KeyValuePair<BuildClient, bool> pair in connectedStatus)
                {
                    if (pair.Value == false)
                    {
                        connectedToAllClients = false;
                        break;
                    }
                }

                retryCount++;
                Thread.Sleep(1000);
            }

            if (m_Abort == true)
            {
                return true;
            }

            //Determine if some build clients could not be connected to, cite those errors.
            for (int index = 0; index < m_ActiveClients.Count; index++) 
            {
                ActiveClient activeClient = m_ActiveClients[index];
                BuildClient client = activeClient.Client;
                if (client.IsConnected() == false)
                {
                    Log("Unable to establish a connection to " + client.m_MachineName + ".  Ignoring build machine during task processing.", MessageType.EngineNotification);

                    DisconnectClient(activeClient);
                    QueueClient(activeClient);
                    index--;
                }
                else
                {
					//Wait for the client to come back with a status.
                    while (client.IsConnected() && client.CurrentStatus == null && m_Abort != true)
                    {
                        Thread.Sleep(10);
                    }

                    Log("Requesting status for " + client.m_MachineName + ".", MessageType.Debug);
                    if (client.CurrentStatus == null)
                    {
                        //The connection could not be formed.  Immediately shut down the client.
                        ShutdownClient(activeClient);
                        index--;
                    }
                    else if (client.CurrentStatus.State == ClientState.Queued)
                    {
                        //This connection has been queued on the client-side; keep it around until activated.
                        QueueClient(activeClient);
                        index--;
                    }
                    else if (client.CurrentStatus.State == ClientState.Active)
                    {
                        //We're in business
                    }
                }
            }

            if (m_Abort == true)
            {
                return true;
            }

            if (m_ActiveClients.Count == 0 && m_QueuedClients.Count == 0)
            {
                Log("No connections to clients could be established!  Aborting build...", MessageType.FatalError);
                return false;
            }

            Update(BuildStatus.Initializing);
            return true;
        }

        /// <summary>
        /// Separate thread running for a client when it has been queued by this build.
        /// </summary>
        /// <param name="client"></param>
        private void QueueClient(ActiveClient client)
        {
            client.Client.Active = true;

            Thread queuedClientThread = new Thread(QueuedClientThread);
            queuedClientThread.Name = "Queued Client Thread for " + client.Client.m_MachineName + ".";
            queuedClientThread.Start(client);
            client.ClientThread = queuedClientThread;

            lock (m_ActiveClientLock)
            {
                m_ActiveClients.Remove(client);
            }

            lock (m_QueuedClientLock)
            {
                m_QueuedClients.Add(client);
            }
        }

        /// <summary>
        /// Separate thread running for a client when it has been shutdown.
        /// </summary>
        /// <param name="client"></param>
        private void ShutdownClient(ActiveClient client)
        {
            client.Client.Active = true;

            Thread shutdownClientThread = new Thread(ShutdownClientThread);
            shutdownClientThread.Name = "Shutdown Client Thread for " + client.Client.m_MachineName + ".";
            shutdownClientThread.Start(client);
            client.ClientThread = shutdownClientThread;

            if (m_ActiveClients.Contains(client) == true)
            {
                lock (m_ActiveClientLock)
                {
                    m_ActiveClients.Remove(client);
                }
            }

            if (m_QueuedClients.Contains(client) == true)
            {
                lock (m_QueuedClientLock)
                {
                    m_QueuedClients.Remove(client);
                }
            }

            lock (m_ShutdownClientLock)
            {
                m_ShutdownClients.Add(client);
            }
        }

        /// <summary>
        /// Disconnects from the targeted client.  Prior to disconnection, if enabled, the client 
        /// will shelve it's changelist to be unshelved by the build system.
        /// </summary>
        /// <param name="client"></param>
        private void DisconnectClient(ActiveClient client)
        {
            Log("Disconnecting from " + client.Client.m_MachineName + ".", MessageType.EngineNotification);

            bool blockDisconnectTask = false;

            if (blockDisconnectTask == true)
            {
                DisconnectClientThread(client);
            }
            else
            {
                Thread disconnectClientThread = new Thread(DisconnectClientThread);
                disconnectClientThread.Name = "Disconnect Client Changelist: " + client.Client.m_MachineName;
                disconnectClientThread.Start(client);
                m_DisonnectionThreads.Add(disconnectClientThread);
            }
        }

        /// <summary>
        /// Tell the client to shelve its changelist, unshelve it locally.
        /// </summary>
        private void DisconnectClientThread(object input)
        {
            ActiveClient client = (ActiveClient)input;
            
            lock (client.Client)
            {
                bool revertChangelist = true; //By default, revert the changelist.
                ClientStatus status = client.Client.CurrentStatus;
                if (status != null && status.State == ClientState.Active)
                {
                    if (RetrieveChangelist == true && m_Abort == false)
                    {
                        Log("Retrieving changelist from " + client.Client.m_MachineName + ".", MessageType.TaskNotification);

                        ShelveChangelistTask shelveChangelistTask = client.Client.ShelveChanges(m_Changelist.m_Number);
                        if (shelveChangelistTask != null)
                        {
                            if (shelveChangelistTask.Result.Success == false)
                            {
                                LogTaskNotification(client.Client.m_MachineName, "Unable to shelve change " + shelveChangelistTask.ChangelistNumber + " from " + client.Client.m_MachineName + ".", shelveChangelistTask.Result);
                                revertChangelist = false; //Preserve the shelved files on the client machine to be retrieved later.
                            }

                            P4RecordSet shelveRecords = m_P4.Run("unshelve", "-s", shelveChangelistTask.ChangelistNumber.ToString(), "-f", "-c", m_Changelist.m_Number.ToString(), "//...");

                            shelveChangelistTask.Result.LogMessage("Unshelving change " + m_Changelist.m_Number + " from " + client.Client.m_MachineName + ".");
                            foreach (string message in shelveRecords.Messages)
                                shelveChangelistTask.Result.LogMessage(message);

                            foreach (string error in shelveRecords.Errors)
                                shelveChangelistTask.Result.LogError(error);

                            if (shelveRecords.HasErrors() == true)
                            {
                                Log("Unable to unshelve change from " + client.Client.m_MachineName + ".", MessageType.Error);
                                revertChangelist = false; //Preserve the shelved files.
                            }

                            ActiveTask activeTask = new ActiveTask(client.Client, shelveChangelistTask);
                            CreateTaskReport(activeTask);
                        }
                    }
                }

                //Send the disconnection notification for queued, shutdown clients as well.
                client.Client.SendDisconnect(m_BuildFile.BuildName, revertChangelist);
            }

            client.Client.Busy = false;
            client.Client.Active = false;
            client.ClientThread.Join();
        }

        /// <summary>
        /// Disconnects all active, queued and shutdown clients.
        /// </summary>
        /// <returns></returns>
        private bool DisconnectClients()
        {
            lock (m_ActiveClientLock)
            {
                foreach (ActiveClient activeClient in m_ActiveClients)
                {
                    DisconnectClient(activeClient);
                }

                m_ActiveClients.Clear();
            }

            lock (m_QueuedClientLock)
            {
                foreach (ActiveClient queuedClient in m_QueuedClients)
                {
                    DisconnectClient(queuedClient);
                }

                m_QueuedClients.Clear();
            }

            lock (m_ShutdownClientLock)
            {
                foreach (ActiveClient shutdownClient in m_ShutdownClients)
                {
                    DisconnectClient(shutdownClient);
                }

                m_ShutdownClients.Clear();
            }

            //Wait for all clients to be disconnected.
            foreach (Thread disconnectionThread in m_DisonnectionThreads)
            {
                disconnectionThread.Join();
            }

            return true;
        }

        /// <summary>
        /// Disconncets all queued and shutdown clients, and any active clients that are not set as busy.
        /// </summary>
        /// <returns></returns>
        private bool DisconnectInactiveClients()
        {
            for (int activeClientIndex = 0; activeClientIndex < m_ActiveClients.Count; activeClientIndex++)
            {
                ActiveClient activeClient = m_ActiveClients[activeClientIndex];
                if (activeClient.Client.Busy == false)
                {
                    DisconnectClient(activeClient);
                    lock (m_ActiveClientLock)
                    {
                        m_ActiveClients.RemoveAt(activeClientIndex);
                        activeClientIndex--;
                    }
                }
            }

            lock (m_QueuedClientLock)
            {
                foreach (ActiveClient queuedClient in m_QueuedClients)
                {
                    DisconnectClient(queuedClient);
                }

                m_QueuedClients.Clear();
            }

            lock (m_ShutdownClientLock)
            {
                foreach (ActiveClient shutdownClient in m_ShutdownClients)
                {
                    DisconnectClient(shutdownClient);
                }

                m_ShutdownClients.Clear();
            }

            return true;
        }

        private void ThreadInitializeClient(object clientObj)
        {
            BuildClient client = (BuildClient)clientObj;
            if (InitializeClient(client) == false)
            {
                client.Active = false;
                client.Shutdown = true;  //Permanently shut down the client.

                string outputMessage = "Initializing client " + client.m_MachineName + " has failed.  Client has been deactivated.";
                Log(outputMessage, MessageType.Error);
            }
        }

        private bool InitializeClients()
        {
            //Mark the synchronization time.  
            m_SyncTime = DateTime.Now;

            //Localize all initialization tasks before sending them out to the individua clients.
            foreach (BuildTask task in m_BuildFile.InitializationTasks)
            {
                task.Localize();
            }

            List<Thread> initializationThreads = new List<Thread>();
            lock (m_ActiveClients)
            {
                //Send out all configuration tasks for the clients.
                int SimultaneousClients = -1;
                int ClientCount = 0;
                foreach (ActiveClient client in m_ActiveClients)
                {
                    Thread clientInitializeThread = new Thread(ThreadInitializeClient);
                    clientInitializeThread.Name = "Client Initialization Thread: " + client.Client.m_MachineName;
                    clientInitializeThread.Start(client.Client);

                    initializationThreads.Add(clientInitializeThread);

                    if ( SimultaneousClients != -1 && (ClientCount % SimultaneousClients) == 0)
                    {
                        //Wait until this client finishes initializing.
                        clientInitializeThread.Join();
                    }
                    Thread.Sleep(10 * 1000);
                    ClientCount++;
                }
            }

            Update(BuildStatus.Initializing);

            //Once the tasks have been dispatched, wait for all of them to have finished.
            while (true)
            {
                int finishedCount = 0;
                
                foreach (Thread initThread in initializationThreads)
                {
                    if (initThread.IsAlive == false)
                    {
                        finishedCount++;
                    } 
                }

                if (finishedCount == initializationThreads.Count)
                    break;

                Update(BuildStatus.Initializing);
                Thread.Sleep(10);
            }

            //Determine if all clients succeeded.
            if (m_BuildFile.Options.Deterministic == true)
            {
                bool success = true;
                foreach(ActiveClient client in m_ActiveClients)
                {
                    if ( client.Client.Shutdown == true )
                    {
                        //Then fail the build.
                        success = false;
                        Log("Unable to initialize client " + client.Client.m_MachineName + ".", MessageType.FatalError);
                    }
                }

                if (success == false)
                {
                    Log("Unable to initialize all clients.", MessageType.FatalError);
                    return false;
                }
            }


            m_FailedTasks.Clear();  //Any failed tasks here can be ignored; client initialization is not critical to build success.
            return true;
        }

        /// <summary>
        /// Build Engine will shelve its own changelist to be servicable for others to pull down.
        /// 
        /// </summary>
        /// <param name="changelists"></param>
        /// <returns></returns>
        private bool ShelveChangelists(List<int> changelists)
        {
            foreach(int changelist in changelists) 
            {
                P4RecordSet shelveRecord = m_P4.Run("shelve", "-c", changelist.ToString(), "//...") ;
                if (shelveRecord.HasErrors() == true)
                {
                    Log("Changelist " + changelist + " was unable to be shelved.  Aborting build...", MessageType.FatalError);
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Handle the submission procedure.
        /// </summary>
        /// <returns></returns>
        private bool Submit()
        {
            if ( SubmitChangelist == true || RetrieveChangelist == true )
            {
                if (m_P4 == null)
                {
                    Log("Unable to acquire P4 object for Automated Build.", MessageType.FatalError);
                    return false;
                }
            }

            //Once we have acquired all changes from these clients.
            if (SubmitChangelist == true)
            {
                bool buildSuccess = true;
                if ( SubmitOnFailure == false ) 
                {
                    foreach (ActiveTask activeTask in m_CompletedTasks)
                    {
                        if (activeTask.Task.Result.Success == false)
                        {
                            buildSuccess = false;
                            break;
                        }
                    }
                }

                if (buildSuccess == true )
                {
                    P4RecordSet openedFiles = m_P4.Run("opened", "-c", m_Changelist.m_Number.ToString());

                    Log("Submitting Changelist " + m_Changelist.m_Number + "...", MessageType.Debug);
                    SubmissionResult result = m_Changelist.Submit();

                    SubmitChangelistTask submitChangelistTask = new SubmitChangelistTask();
                    submitChangelistTask.ChangelistNumber = m_Changelist.m_Number;

                    //List the files in the changelist originally.
                    //
                    submitChangelistTask.Result.CreateLog("Files in Changelist");
                    foreach (P4Record openedFileRecord in openedFiles)
                    {
                        submitChangelistTask.Result.LogMessage(openedFileRecord["depotFile"] + " " + openedFileRecord["action"]);
                    }

                    submitChangelistTask.Result.CreateLog("Submission Messages");
                    foreach (string message in m_Changelist.m_LastRecordSet.Messages)
                    {
                        submitChangelistTask.Result.LogMessage(message);
                    }

                    if (result == SubmissionResult.Failure)
                    {
                        foreach (string error in m_Changelist.m_LastRecordSet.Errors)
                        {
                            submitChangelistTask.Result.LogError(error);
                        }

                        string message = "Changelist " + m_Changelist.m_Number + " could not be submitted.";
                        Log(message, MessageType.Normal);
                        submitChangelistTask.Result.LogMessage(message);

                        submitChangelistTask.Result.Success = false;
                    }
                    else if (result == SubmissionResult.NoFilesToSubmit)
                    {
                        string message = "Changelist " + m_Changelist.m_Number + " contains no changes from the depot's files.  This change will not be submitted.";
                        Log(message, MessageType.Normal);
                        submitChangelistTask.Result.LogMessage(message);

                        submitChangelistTask.Result.Success = true;
                    }
                    else if (result == SubmissionResult.EmptyChangelist)
                    {
                        string message = "Changelist " + m_Changelist.m_Number + " contains no files.  This change will not be submitted.";
                        Log(message, MessageType.Normal);
                        submitChangelistTask.Result.LogWarning(message);

                        m_Changelist.Delete();
                        submitChangelistTask.Result.Success = true;
                    }
                    else
                    {
                        string message = "Changelist " + m_Changelist.m_SubmittedNumber + " has been submitted.";
                        Log("Changelist " + m_Changelist.m_SubmittedNumber + " has been submitted.", MessageType.Normal);
                        submitChangelistTask.Result.LogWarning(message);

                        submitChangelistTask.Result.Success = true;
                    }

                    ActiveTask activeTask = new ActiveTask(null, submitChangelistTask);
                    CreateTaskReport(activeTask);

                    return submitChangelistTask.Result.Success;
                }
                else
                {
                    Log("Changelist " + m_Changelist.m_Number + " was not submitted because the build had errors.", MessageType.Normal);
                    return false;
                }
            }
            else if ( RetrieveChangelist == true )
            {
                Log("Shelving Changelist " + m_Changelist.m_Number + "...", MessageType.Debug);
                if (m_Changelist.Shelve() == false)
                {
                    Log("Changelist " + m_Changelist.m_Number + " could not be shelved.", MessageType.Normal);
                    return false;
                }
                else
                {
                    Log("Changelist " + m_Changelist.m_Number + " has been shelved.", MessageType.Normal);
                }
            }

            return true;
        }

        /// <summary>
        /// Initializes the client, sending over the default tasks to set up Perforce
        /// and then any subsequent initialization tasks in the build file.
        /// If the client can not be initialized, the client will be disabled.
        /// </summary>
        /// <param name="client"></param>
        /// <returns></returns>
        private bool InitializeClient(BuildClient client)
        {
            if (client.Active == false)
                return false;

            //For each client, submit a PerforceInitialization.
            PerforceInitializationTask perforceInitTask = new PerforceInitializationTask(Label, Environment.MachineName, m_ShelvedChangelists);
            if (DispatchTask(client, perforceInitTask, true) == false)
            {
                Log("Perforce initialization has failed.", MessageType.TaskNotification);
                return false;
            }

            bool success = true;
            foreach (BuildTask buildTask in m_BuildFile.InitializationTasks)
            {
                if (m_Abort == true)
                    break;

                if (BuildTask.InitializationTasks.Contains(buildTask.GetType()) == false)
                {
                    Log("The task " + buildTask.ToString() + " is not an initialization task type.  This task will not be run.", MessageType.Warning);
                    continue;
                }

                if (buildTask.GetType() == typeof(PerforceSynchronization))
                {
                    PerforceSynchronization syncTask = (PerforceSynchronization)buildTask;
                    syncTask.m_SyncTime = m_SyncTime;
                    syncTask.m_BuildLabel = Label;
                    syncTask.m_SourceMachine = Environment.MachineName;

                    if (DispatchTask(client, syncTask, true) == false)
                        success = false;
                }
                else
                {
                    if (DispatchTask(client, buildTask, true) == false)
                        success = false;
                }
            }

            return success;
        }

        /// <summary>
        /// Dispatches the specified task to the active client.
        /// </summary>
        /// <param name="client"></param>
        /// <param name="task"></param>
        /// <param name="blockThread">Determines if the task is executed synchronously or asynchronously.</param>
        /// <returns></returns>
        private bool DispatchTask(BuildClient client, BuildTask task, bool blockThread)
        {
            lock (client)
            {
                client.DispatchTask(task);
            }

            ActiveTask activeTask = new ActiveTask(client, task);
            task.Dispatched = true;
            task.StartTime = DateTime.UtcNow;

            if (BuildTask.InitializationTasks.Contains(activeTask.Task.GetType()) == false)
            {
                m_DispatchedTasks.Add(activeTask);
            }

            LogTaskNotification(activeTask.Client.m_MachineName, activeTask.Task.ToString() + " has been dispatched.", null);

            if (blockThread == true)
            {
                WaitForTask(activeTask);

                return activeTask.Task.Result.Success;
            }
            else
            {
                Thread waitingThread = new Thread(WaitForTask);
                waitingThread.Name = "Task Waiting Thread: " + task.ToString();
                waitingThread.Start(activeTask);

                return true; //Dispatching and thread spawning was successful.  Result from task must be gathered from the thread asynchronously.
            }
        }

        /// <summary>
        /// Asynchronously waits for the specified task's result.
        /// </summary>
        /// <param name="task"></param>
        private void WaitForTask(object task)
        {
            ActiveTask activeTask = (ActiveTask)task;

            activeTask.Client.WaitForTaskResult(ref activeTask.Task);

            //Process the task result
            if (activeTask.Task.Result != null)
            {
                CreateTaskReport(activeTask);

                if (activeTask.Task.Result.Success == true)
                {
                    LogTaskNotification(activeTask.Client.m_MachineName, activeTask.Task.ToString() + " has succeeded.", activeTask.Task.Result);
                }
                else
                {
                    LogTaskNotification(activeTask.Client.m_MachineName, activeTask.Task.ToString() + " has failed.", activeTask.Task.Result);
                    
                    lock (m_DispatchedTasks)
                    {
                        m_FailedTasks.Add(activeTask);
                    }
                }
            }
            else
            {
                LogTaskNotification(activeTask.Client.m_MachineName, "Task result for " + activeTask.Task.ToString() + " could not be determined.  Client has been deactivated.", null);
                activeTask.Client.Active = false;
            }

            //Do not increment the Dispatched or Completed task list if it's an initialization task.
            //Keep in mind that clients can enter a build process mid-way.  Changing these variables will 
            //affect the integrity of the build by aborting early because we have completed the same number of tasks
            //within the build file.
            if (BuildTask.InitializationTasks.Contains(activeTask.Task.GetType()) == false)
            {
                lock (m_DispatchedTasks)
                {
                    m_DispatchedTasks.Remove(activeTask);
                    m_CompletedTasks.Add(activeTask);
                }
            }
        }

        /// <summary>
        /// Waits for all tasks to complete, updating the UI.
        /// </summary>
        private void WaitForAllTasks()
        {
            while (m_DispatchedTasks.Count != 0)
            {
                //We have finished dispatching all tasks to the clients.
                //Release all build clients that are not used. 
                DisconnectInactiveClients();

                Thread.Sleep(1000);
                Update(BuildStatus.Running);
            }

            while (m_CompletedTasks.Count != m_TaskList.Count)
            {
                Thread.Sleep(1000);
                Update(BuildStatus.Running);                
            }
        }

        private BuildTask FindTaskForClient(BuildClient client, bool bForceAcquireTask)
        {
            TaskPriority currentPriority = TaskPriority.TaskNormal;
            BuildTask assignedTask = null;
            int taskIndex = 0;
            foreach (BuildTask task in m_TaskList)
            {
                if (task.Dispatched == true)
                {
                    //Task has already been assigned.
                    taskIndex++;
                    continue;
                }

                TaskPriority priority = client.m_TaskAssignments.GetPriority(task.GetType());

                if (m_BuildFile.Options.Deterministic == true && bForceAcquireTask == false)
                {
                    if (task.TargetClient != null) 
                    {
                        if(task.TargetClient == client)
                        {
                            assignedTask = task;
                            break;
                        }
                    }
                    else 
                    {
                        TaskHistoryItem item = m_TaskHistoryFile.GetTaskHistory(task.HashCRC, client.m_MachineName);
                        if (item != null)
                        {
                            assignedTask = task;
                            break;
                        }
                        else if (m_TaskHistoryFile.Contains(task.HashCRC) == null)
                        {
                            if (priority != TaskPriority.TaskIgnored)
                            {
                                //The item is not part of the history; assign it to this client.
                                assignedTask = task;
                                break;
                            }
                        }
                    }
                }
                else
                {
                    if (priority > currentPriority)
                    {
                        currentPriority = priority;
                        assignedTask = task;
                    }
                    else if (priority == TaskPriority.TaskIgnored)
                    {
                        //Do nothing.
                    }
                    else if (priority == currentPriority && assignedTask == null)
                    {
                        assignedTask = task;
                    }
                }

                taskIndex++;
            }

            //If a task could not be found and the build is deterministic,
            //re-balance the task assignment.  This will eventually redistribute
            //the task load equally across machines.
            if (assignedTask == null && m_BuildFile.Options.Deterministic == true && Clean == false)
            {
                foreach (BuildTask task in m_TaskList)
                {
                    if (task.Dispatched == true)
                    {
                        //Task has already been assigned.
                        continue;
                    }

                    if (m_TaskHistoryFile != null)
                    {
                        //Ensure that these tasks already assigned to another machine.
                        List<TaskHistoryItem> historyItems = m_TaskHistoryFile.GetTaskHistory(task.HashCRC);
                        if (historyItems.Count != 0)
                        {
                            break;
                        }

                        //This task is not part of the task history and needs to be assigned out.
                    }

                    TaskPriority priority = client.m_TaskAssignments.GetPriority(task.GetType());
                    if (priority > currentPriority)
                    {
                        currentPriority = priority;
                        assignedTask = task;

                        m_GenerateHistory = true;
                    }
                    else if (priority == TaskPriority.TaskIgnored)
                    {
                        //Do nothing.
                    }
                    else if (priority == currentPriority && assignedTask == null)
                    {
                        assignedTask = task;

                        m_GenerateHistory = true;
                    }
                }
            }

            return assignedTask;
        }

        private void CreateCleanTasksForClient(BuildTaskList taskList, BuildClient client)
        {
            BuildTaskList clientCleanTasks = new BuildTaskList();
            for (int taskIndex = 0; taskIndex < taskList.Count; taskIndex++)
            {
                BuildTask task = taskList[taskIndex];
                TaskPriority priority = client.m_TaskAssignments.GetPriority(task.GetType());
                if (BuildTask.NormalTasks.Contains(task.GetType()) == true && priority != TaskPriority.TaskIgnored)
                {
                    //If the task is not ignored, then we must try to clean it.
                    BuildTask newTask = (BuildTask)BuildTask.DeepClone(task);
                    newTask.TargetClient = client;
                    clientCleanTasks.Add(newTask);
                }
            }

            m_TaskList.AddRange(clientCleanTasks);
        }

        private bool InitializeTasks()
        {
            foreach(BuildTask task in m_BuildFile.Tasks)
            {
                task.Localize();

                Type type = task.GetType(); 
                while (type != null) 
                {
                    FieldInfo[] myObjectFields = type.GetFields(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance); 
                    foreach (FieldInfo fi in myObjectFields)
                    {
                        if (String.Compare(fi.Name, "Clean", true) == 0)
                            fi.SetValue(task, Clean);
                        else if (String.Compare(fi.Name, "Rebuild", true) == 0)
                            fi.SetValue(task, Rebuild);
                    }

                    type = type.BaseType; 
                }
            }

            m_DispatchedTasks.Clear();
            m_CompletedTasks.Clear();
            m_TaskList.Clear();
            m_TaskReportList.Clear();

            //If this build is not deterministic, in order to be thorough,
            //disperse all clean tasks to all of the build clients.
            if (Clean == true && (m_BuildFile.Options.Deterministic == false || m_TaskHistoryFile == null || m_TaskHistoryFile.ItemCount == 0) )
            {
                BuildTaskList cleanTasks = new BuildTaskList();
                for (int clientIndex = 0; clientIndex < m_ActiveClients.Count; clientIndex++)
                {
                    BuildClient client = m_ActiveClients[clientIndex].Client;
                    CreateCleanTasksForClient(m_BuildFile.Tasks, client);
                }

                m_GenerateHistory = false;
            }
            else
            {
                if (m_TaskHistoryFile == null || m_TaskHistoryFile.ItemCount == 0)
                {
                    foreach (BuildTask task in m_BuildFile.Tasks)
                    {
                        if (BuildTask.NormalTasks.Contains(task.GetType()) == true)
                        {
                            m_TaskList.Add(task);
                        }
                    }
                }
                else
                {
                    //A task history exists -- match up the task history with the actual tasks.
                    //Ensure that duplicate tasks (with the same HashCRC) is maintained.
                    foreach (BuildTask task in m_BuildFile.Tasks)
                    {
                        bool added = false;
                        foreach (TaskHistoryItem item in m_TaskHistoryFile.m_TaskHistory)
                        {
                            if( task.TargetClient != null )
                                continue;

                            if (task.HashCRC == item.CRC)
                            {
                                BuildClient foundClient = m_Clients.Find(item.MachineName);
                                if (foundClient != null)
                                {
                                    task.TargetClient = foundClient;
                                }

                                //A client wasn't found -- this could be a new task or the 
                                //client list has changed.  The task processing should properly
                                //distribute unassigned tasks.

                                m_TaskList.Add(task);
                                added = true;
                                break;
                            }
                        }

                        if (added == false)
                        {
                            //If the task hasn't a previous owner, then let it get a target through the scheduling process
                            //in the next build.
                            m_TaskList.Add(task);
                        }
                    }                    
                }
            }

            Update(BuildStatus.Initializing);
            return true;
        }

        private bool ProcessTasks()
        {
            //Clear any prerequisite tasks before the main processing.
            m_CompletedTasks.Clear();
            m_DispatchedTasks.Clear();

            while (m_CompletedTasks.Count < m_TaskList.Count)
            {
                if (m_Abort == true)
                {
                    break;
                }

                if (m_CompletedTasks.Count + m_DispatchedTasks.Count == m_TaskList.Count)
                {
                    //All tasks have been divvied out. 
                    break;
                }
             
                //In order to prevent any deadlocks, save off the active clients list to a temporary
                //list.  Other threads can potentially edit this list depending on when clients come online.
                List<ActiveClient> activeClients = new List<ActiveClient>();
                lock (m_ActiveClients)
                {
                    foreach (ActiveClient activeClient in m_ActiveClients)
                    {
                        activeClients.Add(activeClient);
                    }
                }

                List<ActiveClient> idleList = new List<ActiveClient>();
                List<ActiveClient> removalList = new List<ActiveClient>();
                foreach (ActiveClient activeClient in activeClients)
                {
                    BuildClient client = activeClient.Client;
                    if (client.IsConnected() == false)
                    {
                        //If we have lost connection to the client, remove it from the list.
                        removalList.Add(activeClient);
                        continue;
                    }
                    else if(client.Busy == false)
                    {
                        //Acquire the best task for this client.
                        BuildTask task = FindTaskForClient(client, false);

                        if (task == null)
                        {
                            //No tasks could be found for this client.
                            idleList.Add(activeClient);
                            continue;
                        }

                        //Assign the client a new task.
                        lock (m_DispatchedTasks)
                        {
                            DispatchTask(client, task, false);
                        }

                        if (m_GenerateHistory == true)
                        {
                            m_TaskHistoryFile.AddTask(task.HashCRC, client.m_MachineName);
                        }

                        //See if all the tasks have been divvied out.  If so, then we don't need to process any longer.
                        if (m_CompletedTasks.Count + m_DispatchedTasks.Count == m_TaskList.Count)
                        {
                            //All tasks have been divvied out. 
                            break;
                        }
                    }
                }

                //Determine if the build has gone into an idle state that will never finish.
                if ( idleList.Count > 0 )
                {                    
                    //If there are no tasks for any clients,
                    //And if there are tasks left to be done.
                    //Then grab the first task to complete.
                    if (idleList.Count == activeClients.Count)
                    {
                        foreach (ActiveClient activeClient in idleList)
                        {
                            BuildClient client = activeClient.Client;
                            BuildTask task = FindTaskForClient(client, true);

                            if (task == null)
                            {
                                //Still no tasks could be given to this client.
                                continue;
                            }

                            lock (m_DispatchedTasks)
                            {
                                DispatchTask(client, task, false);
                            }
                        }

                        //NOTE: If not tasks were outsourced, then we are caught waiting for another build machine to come online.
                        //This solution should be revisited.
                    }
                }

                //Filter out all disconnected clients.
                if (removalList.Count > 0)
                {
                    foreach (ActiveClient removedClient in removalList)
                    {
                        //TODO:  Add this client to the list to continue to retry connecting.
                        //TODO: Create a separate list; spawn threads for connection intermittently.
                        Log("Unable to connect to and configure " + removedClient.Client.m_MachineName + ".  Removing from list of active clients.", MessageType.Normal);
                        if (removedClient.Client.Shutdown == false)
                        {
                            QueueClient(removedClient);
                        }
                        else
                        {
                            ShutdownClient(removedClient);
                        }
                    }
                }

                if (activeClients.Count == 0 && m_QueuedClients.Count == 0)
                {
                    Log("No active or queued clients are found.  All clients have been shutdown.  Aborting...", MessageType.FatalError);
                    break;
                }
                else if (activeClients.Count == 0)
                {
                    if (AllQueuedCount >= AllQueuedCountMax)
                    {
                        Log("All clients are currently queued or unavailable.", MessageType.Normal);
                        AllQueuedCount = 0;
                    }
                     
                    Thread.Sleep(AllQueuedTimeout);
                    AllQueuedCount++;
                }
                else
                {
                    //Reassign the tasks.
                }


                Update(BuildStatus.Running);

                Thread.Sleep(10);
            }

            WaitForAllTasks();
            return true;
        }

        /// <summary>
        /// Separate thread for processing clients that are ready to accept
        /// tasks for the current build.
        /// </summary>
        /// <param name="client"></param>
        public void ActiveClientThread(object client)
        {
            ActiveClient activeClient = (ActiveClient)client;
            BuildClient buildClient = activeClient.Client;

            while (buildClient.Active)
            {
                //Attempt to establish a connection to the client.
                if (buildClient.Connect() == false)
                {
                    Thread.Sleep(100);
                    continue;
                }

                bool isConnected = true;
                while (isConnected == true && buildClient.Active)
                {
                    lock (buildClient)
                    {
                        isConnected = buildClient.IsConnected();
                    }

                    if (isConnected == true)
                    {
                        //Acquire the client's current status.
                        buildClient.UpdateStatus();

                        if (buildClient.CurrentStatus != null
                            && buildClient.CurrentStatus.State == ClientState.Shutdown)
                        {
                            Log(buildClient.m_MachineName + " has received a shutdown notification.", MessageType.Normal);
                            buildClient.Disconnect();
                            ShutdownClient(activeClient);
                        }

                        Thread.Sleep(500);
                    }
                }
            }

            //No longer active, end the connection.
            buildClient.Disconnect();
        }

        /// <summary>
        /// Separate thread for processing clients that have been queued by this build.
        /// </summary>
        /// <param name="client"></param>
        private void QueuedClientThread(object client)
        {
            ActiveClient queuedClient = (ActiveClient)client;

            while (queuedClient.Client.Active)
            {
                //Attempt to establish a connection to the client.
                if (queuedClient.Client.IsConnected() == false)
                {
                    if (queuedClient.Client.Connect() == false)
                    {
                        ShutdownClient(queuedClient);
                        break;
                    }
                }

                //The active client will wait until the status changes.
                queuedClient.Client.UpdateStatus();
                ClientStatus status = queuedClient.Client.CurrentStatus;

                if (status == null || status.State == ClientState.Shutdown)
                {
                    //The client has permanently rejected the client.   
                    //If this happens, the client was closed or crashed.  Or the machine was unresponsive.
                    queuedClient.Client.Disconnect();
                    ShutdownClient(queuedClient);
                    break;
                }
                else if (status.State == ClientState.Active)
                {
                    Log("Received connection from " + queuedClient.Client.m_MachineName + "!  Activating client...", MessageType.Normal);

                    if (InitializeClient(queuedClient.Client) == true)
                    {
                        lock (m_QueuedClientLock)
                        {
                            m_QueuedClients.Remove(queuedClient);
                        }

                        if (Clean == true)
                        {
                            CreateCleanTasksForClient(m_BuildFile.Tasks, queuedClient.Client);
                        }

                        lock (m_ActiveClientLock)
                        {
                            m_ActiveClients.Add(queuedClient);
                        }
                        break;
                    }
                    else
                    {
                        //Add to the shutdown list; the client could not be initialized.
                        Log("Initializing client " + queuedClient.Client.m_MachineName + " has failed.  Client has been deactivated.", MessageType.Warning);
                        queuedClient.Client.Disconnect();
                        ShutdownClient(queuedClient);
                    }
                }

                Thread.Sleep(1000);
            }
        }

        /// <summary>
        /// Separate thread for clients that have been ignored by this build
        /// because of a disconnection, crash or failure to initialize properly.
        /// </summary>
        /// <param name="client"></param>
        private void ShutdownClientThread(object client)
        {
            ActiveClient shutdownClient = (ActiveClient)client;

            while (shutdownClient.Client.Active)
            {
                //Attempt to establish a connection to the client.
                if (shutdownClient.Client.IsConnected() == false)
                {
                    if (shutdownClient.Client.Connect() == false)
                    {
                        Thread.Sleep(1000 * 10);  //Only test the connection every 10 seconds.
                        continue;
                    }

                    //The active client will wait until the status changes.
                    lock (shutdownClient.Client)
                    {
                        shutdownClient.Client.UpdateStatus();
                        ClientStatus status = shutdownClient.Client.CurrentStatus;
                        if (status == null || status.State == ClientState.Shutdown)
                        {
                            shutdownClient.Client.Disconnect();
                            Thread.Sleep(1000 * 10);
                            continue;
                        }
                    }
                }

                if (InitializeClient(shutdownClient.Client) == true)
                {
                    if (Clean == true)
                    {
                        CreateCleanTasksForClient(m_BuildFile.Tasks, shutdownClient.Client);
                    }

                    lock (m_ActiveClientLock)
                    {
                        m_ActiveClients.Add(shutdownClient);
                    }

                    lock (m_ShutdownClientLock)
                    {
                        m_ShutdownClients.Remove(shutdownClient);
                    }
                    break;
                }
                else
                {
                    Log("Initializing client " + shutdownClient.Client.m_MachineName + " has failed.", MessageType.Error);
                }

                //Disconnect the client to free it up for another build to use potentially use.
                shutdownClient.Client.Disconnect();
                Thread.Sleep(1000 * 60);
            }
        }

        protected void GenerateTaskHistory()
        {
            if (m_GenerateHistory == false)
                return;

            //Generate the history every time the build is run.
            //It is a reasonable assumption that all tasks will be succeeding to execute
            //and the clients are online.  
            //TODO:  What happens when the clients are queued by another build?
            //
            string savePath = Path.Combine(HistoryDirectory, m_BuildFile.BuildName + ".xml");
            m_TaskHistoryFile.Save(savePath);
        }

        public void LogTaskNotification(string machineName, string output, TaskResult result)
        {
            string logString = String.Format("[{0}]: {1}", machineName, output);

            if (result == null)  //The task result can be null in cases where tasks are aborted or crash.
            {
                Log(logString, MessageType.TaskNotification);
            }
            else
            {
                Log(logString, MessageType.TaskNotification);

                foreach (TaskLog taskLog in result.Logs)
                {
                    for (int messageIndex = 0; messageIndex < taskLog.Messages.Count; ++messageIndex)
                    {
                        TaskMessage taskMessage = taskLog.Messages[messageIndex];

                        string taskMessageString = String.Format("[{0}]: {1}", machineName, taskMessage.Message);
                        if (taskMessage.MsgType == MessageType.Error)
                        {
                            Log(taskMessageString, MessageType.Error);
                        }
                        else if (taskMessage.MsgType == MessageType.Warning)
                        {
                            Log(taskMessageString, MessageType.Warning);
                        }
                        //Displaying all messages gets rather spammy.  Don't output to the main interface.
                    }
                }
            }
        }

        public void Log(string output, MessageType type)
        {
            if (output.EndsWith("\n") == false)
            {
                output += "\n";
            }

            if (OutputReceived != null)
            {
                if ((type == MessageType.Debug && EnableDebugMessages == true) || type != MessageType.Debug)
                    OutputReceived(output, type);
            }

            if (type == MessageType.Error)
            {
                //NOTE:  Outputting the errors to the console is important for any parent process to intercept.
                string[] split = output.Split("\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                int errorCount = 0;
                int errorLimit = 100;
                foreach (string spitString in split)
                {
                    if (errorCount > errorLimit)
                    {
                        Console.WriteLine("Error: Error list has been truncated.");
                        break;
                    }

                    if (CruiseControlFormatted == false)
                    {
                        string error = "Error: " + spitString;
                        Console.WriteLine(spitString);
                        errorCount++;
                    }
                }
            }
            else if (type == MessageType.Warning)
            {
                //NOTE:  Outputting the warning to the console is important for any parent process to intercept.
                string[] split = output.Split("\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                int warningCount = 0;
                int warningLimit = 100;
                foreach (string splitString in split)
                {
                    if (warningCount > warningLimit)
                    {
                        Console.WriteLine("Warning: Waring list has been truncated.");
                        break;

                    }

                    if (CruiseControlFormatted == false)
                    {
                        string warning = "Warning: " + splitString;
                        Console.WriteLine(warning);
                        warningCount++;
                    }
                }
            }
            else if (type == MessageType.FatalError)
            {
                if (CruiseControlFormatted == true)
                {
                    output = "INFO_MSG: " + output;
                }

                //Print out all fatal errors -- which will get bubbled up to any calling process
                //that will parse the standard output.
                string error = "Error: " + output;
                Console.WriteLine(output);
            }
            else if (type == MessageType.TaskNotification || type == MessageType.EngineNotification)
            {
                Console.WriteLine(output);
            }
            else if (type == MessageType.Debug)
            {
                Console.WriteLine(output);
            }
            else
            {
                if (CruiseControlFormatted == true)
                {
                    output = "INFO_MSG: " + output;
                }

                Console.WriteLine(output);
            }
        }

        /// <summary>
        /// Acquires a build label number.  If the project state file is specified (from Cruise Control),
        /// we will parse that for the id number.  Otherwise, we'll generate our own.
        /// </summary>
        /// <returns></returns>
        private int GetBuildVersion()
        {
            if (String.IsNullOrWhiteSpace(ProjectStateFile) == true || File.Exists(ProjectStateFile) == false)
            {
                string safeBuildName = m_BuildFile.BuildName.Replace(" ", "_");
                string labelMarkerFile = Path.Combine(ReportDirectory, safeBuildName) + ".label";
                int currentLabel = -1;
                if (File.Exists(labelMarkerFile) == true)
                {
                    try
                    {
                        string[] labelFileText = File.ReadAllLines(labelMarkerFile);
                        if (labelFileText.Length > 0)
                        {
                            Int32.TryParse(labelFileText[0], out currentLabel);
                        }

                        currentLabel++;
                    }
                    catch (IOException e)
                    {
                        Log("Unable to read from " + labelMarkerFile + ".\n" + e.Message, MessageType.Warning);
                        currentLabel = 0;
                    }
                }
                else
                {
                    currentLabel = 0;
                }

                //Write the new label.
                try
                {
                    StreamWriter stream = File.CreateText(labelMarkerFile);
                    stream.WriteLine(currentLabel);
                    stream.Close();
                }
                catch (IOException e)
                {
                    Log("Unable to write build marker to " + labelMarkerFile + ".\n" + e.Message, MessageType.Warning);
                }

                return currentLabel;
            }
            else
            {
                StreamReader reader = new StreamReader(ProjectStateFile);
                XPathDocument document = new XPathDocument(reader);
                XPathNavigator navigator = document.CreateNavigator();
                string lastVersionXPathQuery = "/project_state/work_units/work_unit[last()]/@version";
                XPathNodeIterator iter = navigator.Select(lastVersionXPathQuery);
                iter.MoveNext();
                int version = -1;
                Int32.TryParse(iter.Current.Value, out version);
                return version;
            }
        }

        /// <summary>
        /// Creates the report directories; acquired a build version to use as an identifier.
        /// </summary>
        private void InitializeReports()
        {
            RootReportDirectory = ReportDirectory;

            if (OverrideReportDirectory == false || String.IsNullOrWhiteSpace(ReportDirectory) == true)
            {
                //The default folder for reports will be within the tools temporary directory, under the directory
                //corresponding to the name of the build file.
                RootReportDirectory = Path.Combine(Environment.GetEnvironmentVariable("RS_TOOLSROOT"), "tmp\\AutomatedBuild");
                ReportDirectory = Path.Combine(RootReportDirectory, m_BuildFile.BuildName);
            }
            
            try
            {
                if (Directory.Exists(ReportDirectory) == false)
                    Directory.CreateDirectory(ReportDirectory);
            }
            catch (Exception e)
            {
                Log("Unable to create directory " + ReportDirectory + ".  Reports will not be generated.\n" + e.Message, MessageType.FatalError);
            }

            if (CruiseControlFormatted == true)
            {
                BuildVersion = GetBuildVersion();
                RootReportDirectory = ReportDirectory;

                //Create a sub-directory from the ReportDirectory.
                string safeBuildName = m_BuildFile.BuildName.Replace(" ", "_");
                string nextDirectoryLabel = safeBuildName + "_" + BuildVersion.ToString("D5");
                ReportDirectory = Path.Combine(ReportDirectory, nextDirectoryLabel);

                try
                {
                    if (Directory.Exists(ReportDirectory) == false)
                        Directory.CreateDirectory(ReportDirectory);
                }
                catch (Exception e)
                {
                    Log("Unable to create directory " + ReportDirectory + ".  Reports will not be generated.\n" + e.Message, MessageType.FatalError);
                }

                string summaryFilePath = Path.Combine(ReportDirectory, SummaryReport.SummaryReportHtmlFile);
                Log("For a full report summary, please see " + summaryFilePath + ".", MessageType.Normal);
            }

            try
            {
                string[] files = Directory.GetFiles(ReportDirectory);
                if (files.Length > 0)
                {
                    foreach (string file in files)
                    {
                        try
                        {
                            File.Delete(file);
                        }
                        catch (Exception e)
                        {
                            Log("Unable to delete file " + file + ".  This can potentially leave stale report files.\n" + e.Message, MessageType.Warning);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Log("Unable to create directory " + ReportDirectory + ".  Reports will not be generated.\n" + e.Message, MessageType.FatalError);
            }
        }

        /// <summary>
        /// Generates the XML and HTML document containing task output.
        /// </summary>
        /// <param name="activeTask"></param>
        private void CreateTaskReport(ActiveTask activeTask)
        {
            TaskReport taskReport = new TaskReport(activeTask.Client, activeTask.Task, ReportXSLDirectory, ReportDirectory);
            string errorString;
            if (taskReport.Generate(out errorString) == false)
            {
                Log("Unable to generate report for " + activeTask.Task.ToDisplayName() + ".  No information about this task will be written.", MessageType.FatalError);
            }

            m_TaskReportList.Add(activeTask);
        }

        /// <summary>
        /// Generates a front-end interface for all tasks associated with the current build.
        /// </summary>
        private void GenerateSummaryReport()
        {          
            //Generate the main report, summarizing all tasks'.
            SummaryReport summaryReport = new SummaryReport(m_BuildFile.BuildName, BuildVersion, m_EndTime - m_StartTime, m_TaskReportList, ReportXSLDirectory, ReportDirectory);
            if (summaryReport.Generate() == false)
            {
                Log("The summary report for " + m_BuildFile.BuildName + " could not be generated.", MessageType.FatalError);
            }
            else
            {
                if (CruiseControlFormatted == false)
                {
                    Log("View the Summary Report in " + summaryReport.OutputFile + ".", MessageType.Normal);
                }
            }
        }

        /// <summary>
        /// The build report is the current status of this build.  This XML file will be outputted
        /// and then transformed into a web page for easy viewing.
        /// </summary>
        private void GenerateStatusReport(BuildInfo buildInfo)
        {
            //Ignore the intervals (setup, finalization, initialization) where providing status updates will be 
            //misleading or unuseful to users.
            //
            if (EnableStatusUpdates == false)
                return;

            if (String.IsNullOrEmpty(RootReportDirectory) == true)
                return;

            StatusReport statusReport = new StatusReport(m_BuildFile.BuildName, BuildVersion, buildInfo, RootReportDirectory);

            if (statusReport.Generate(m_TaskList) == false)
            {
                Log("The status report  for " + m_BuildFile.BuildName + " could not be generated.", MessageType.Error);
            }
        }

        /// <summary>
        /// Commits all information on the tasks' result to the database.
        /// </summary>
        /// <returns></returns>
        private bool CommitRequestInformation()
        {
            try
            {
                //Take all of the tasks associated
                RequestService.RequestInterfaceSoapClient requestClient = new RequestService.RequestInterfaceSoapClient();
                foreach (ActiveTask activeTask in m_TaskReportList)
                {
                    foreach (int id in activeTask.Task.ID)
                    {
                        if (id != BuildTask.InvalidID)
                        {
                            string taskHTMLPath = Path.Combine(Path.GetFileName(ReportDirectory), activeTask.Task.ToReportName() + ".html");
                            int submittedChangelist = activeTask.Task.Result.Success == true ? m_Changelist.m_SubmittedNumber : -1;

                            bool success = requestClient.CommitCutsceneRequestResult(id, activeTask.Task.Result.Success, taskHTMLPath, submittedChangelist, activeTask.Task.TimeElapsed); 
                            if (success == false)
                            {
                                Log("Unable to commit build result to the database", MessageType.Warning);
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                //No warning or error -- we don't want this to be a crucial part of the process.
                return false;
            }
            return true;
        }


        #region Helper Functions

        public void BuildSerializedXml()
        {   
            BuildClient client = new BuildClient();
            TaskAssignment test = new TaskAssignment();
            test.TaskTypeName = "CSharpSolution";
            test.Ignore = true;
            client.m_TaskAssignments.Add(test);
            m_Clients.Add(client);

            XmlSerializer serializer = new XmlSerializer(typeof(BuildClients));
            StreamWriter writer = new StreamWriter("C:\\output.xml");
            serializer.Serialize(writer, m_Clients);
            writer.Close();

            BuildFile buildFile = new BuildFile();
            buildFile.BuildName = "Test";
            buildFile.BuildID = 2;
            buildFile.Options.Deterministic = true;

            PerforceSynchronization syncTask = new PerforceSynchronization(0, "//depot/...");
            buildFile.InitializationTasks.Add(syncTask);

            StudioMax studioMax = new StudioMax();
            studioMax.Version = StudioMaxVersion.Max2011;
            studioMax.MaxScript = "C:\\maxscript.ms";
            studioMax.MaxFile = "C:\\maxfile.max";
            buildFile.Tasks.Add(studioMax);

            BuildSolution solutionTask = new BuildSolution(@"base\samples\sample_physics\sample_contacts", null, "Beta|Win32");
            solutionTask.CheckOutFiles = new List<string>();
            solutionTask.CheckOutFiles.Add("//depot/...");
            buildFile.Tasks.Add(solutionTask);

            BuildFile.Save("C:\\tasks_new.xml", ref buildFile);
        }

        #endregion
    }
}
