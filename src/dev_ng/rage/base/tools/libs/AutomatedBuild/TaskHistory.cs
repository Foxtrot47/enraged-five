﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace AutomatedBuild
{
    public class TaskHistoryItem
    {
        [XmlAttribute("CRC")]
        public UInt32 CRC;

        [XmlAttribute("MachineName")]
        public string MachineName;

        public TaskHistoryItem()
        {
            CRC = UInt32.MaxValue;
            MachineName = null;
        }

        public TaskHistoryItem(UInt32 crc, string machineName)
        {
            CRC = crc;
            MachineName = machineName;
        }
    }
    
    [Serializable]
    public class TaskHistoryFile
    {
        [XmlAttribute("Version")]
        public int m_Version;
        private static int TASK_HISTORY_VERSION = 1;

        [XmlElement("TaskFilePath")]
        public string m_TaskFilePath;

        [XmlAttribute("TaskFileModifiedDate")]
        public DateTime m_TaskFileModifiedDate;

        [XmlElement("ClientFilePath")]
        public string m_ClientFilePath;

        [XmlAttribute("ClientFileModifiedDate")]
        public DateTime m_ClientFileModifiedDate;

        [XmlAttribute("HistoryGeneratedDate")]
        public DateTime m_HistoryGenerated;

        [XmlArray("TaskHistory")]
        public List<TaskHistoryItem> m_TaskHistory;

        [XmlIgnore]
        public int ItemCount { get { return m_TaskHistory.Count; } }
    
        private static XmlSerializerNamespaces XmlNamespaces;

        public TaskHistoryFile()
        {
            m_Version = TASK_HISTORY_VERSION;
            m_TaskHistory = new List<TaskHistoryItem>();

            m_TaskFilePath = null;
            m_ClientFilePath = null;

            XmlNamespaces = new XmlSerializerNamespaces();
            XmlNamespaces.Add("", ""); 
        }

        public TaskHistoryFile(string clientFilePath, string taskFilePath)
        {
            m_Version = TASK_HISTORY_VERSION;
            m_TaskFilePath = taskFilePath;
            m_ClientFilePath = clientFilePath;

            m_TaskHistory = new List<TaskHistoryItem>();

            XmlNamespaces = new XmlSerializerNamespaces();
            XmlNamespaces.Add("", ""); 
        }

        public void AddTask(UInt32 crc, string machineName)
        {
            TaskHistoryItem taskHistory = new TaskHistoryItem(crc, machineName);
            m_TaskHistory.Add(taskHistory);
        }

        public TaskHistoryItem GetTaskHistory(UInt32 hashCRC, string machineName)
        {
            foreach (TaskHistoryItem item in m_TaskHistory)
            {
                if (item.CRC == hashCRC && String.Compare(item.MachineName, machineName, true) == 0)
                {
                    return item;
                }
            }

            return null;
        }

        public List<TaskHistoryItem> GetTaskHistory(UInt32 hashCRC)
        {
            List<TaskHistoryItem> items = new List<TaskHistoryItem>();
            foreach (TaskHistoryItem item in m_TaskHistory)
            {
                if (item.CRC == hashCRC)
                {
                    items.Add(item);
                }
            }

            return items;
        }

        public TaskHistoryItem Contains(UInt32 hashCRC)
        {
            foreach (TaskHistoryItem item in m_TaskHistory)
            {
                if (item.CRC == hashCRC)
                {
                    return item;
                }
            }

            return null;
        }

        public static bool Load(string loadPath, ref TaskHistoryFile taskFile)
        {
            if (File.Exists(loadPath) == false)
                return false;

            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(TaskHistoryFile));
                StreamReader reader = new StreamReader(loadPath);
                taskFile = (TaskHistoryFile)serializer.Deserialize(reader);
                reader.Close();
            }
            catch (Exception)
            {
                return false;
            }

            if (taskFile.m_Version != TASK_HISTORY_VERSION)
            {
                //Version mismatch; the file is invalidated.
                return false;
            }

            taskFile.m_TaskFileModifiedDate = File.GetLastWriteTimeUtc(taskFile.m_TaskFilePath);
            taskFile.m_ClientFileModifiedDate = File.GetLastWriteTimeUtc(taskFile.m_ClientFilePath);
            DateTime historyModifiedDate = File.GetLastWriteTimeUtc(loadPath);
            if (historyModifiedDate <= taskFile.m_TaskFileModifiedDate)
            {
                //The build file has been changed and the task history is invalidated.
                return false;
            }
            else if (historyModifiedDate <= taskFile.m_ClientFileModifiedDate)
            {
                //The client file has been changed and the task history is invalidated.
                return false;
            }

            return true;
        }

        public bool Save(string savePath)
        {
            //TODO:  There are instances where saving a task history file is not wanted (e.g. cleaning the build).
            //Possibly find a better way to abort the save without a natural error.
            if (m_ClientFilePath == null || m_TaskFilePath == null)
                return false;

            m_HistoryGenerated = DateTime.UtcNow;
            m_TaskFileModifiedDate = File.GetLastWriteTimeUtc(m_TaskFilePath);
            m_ClientFileModifiedDate = File.GetLastWriteTimeUtc(m_ClientFilePath);

            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(TaskHistoryFile));
                StreamWriter writer = new StreamWriter(savePath);
                serializer.Serialize(writer, this, XmlNamespaces);
                writer.Close();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }
    }
}

