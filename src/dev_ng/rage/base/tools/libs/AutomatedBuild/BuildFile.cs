﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

using AutomatedBuildShared;

namespace AutomatedBuild
{
    [Serializable]
    public class InitializationTaskList : BuildTaskList
    {
        public InitializationTaskList() : base() { }
    }

    [Serializable]
    public class BuildTaskList : List<BuildTask>, IXmlSerializable
    {
        private static XmlSerializerNamespaces XmlNamespaces; 

        public BuildTaskList()
        {
            XmlNamespaces = new XmlSerializerNamespaces();
            XmlNamespaces.Add("", ""); 
        }

        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            if (reader.IsEmptyElement)
            {
                this.Clear();
            }
            else
            {
                reader.Read();

                while (true)
                {
                    string typeName = GetType().Name;
                    if (reader.NodeType == XmlNodeType.EndElement && String.Compare(reader.Name, typeName, true) == 0 )
                        break;

                    if (reader.NodeType == XmlNodeType.Comment)
                    {
                        reader.Read();
                        continue;
                    }

                    Type taskType = null;
                    foreach (Type type in BuildTask.RegisteredTasks)
                    {
                        if (String.Compare(type.Name, reader.Name, true) == 0)
                        {
                            taskType = type;
                            break;
                        }
                    }

                    if ( taskType != null )
                    {
                        XmlSerializer serializer = new XmlSerializer(taskType);
                        BuildTask task = (BuildTask) serializer.Deserialize(reader);
                        this.Add(task);

                        //The XML Serialization process will increment the reader to the next spot.
                    }
                    else
                    {
                        BuildFile.Log("Unable to deserialize the task: " + reader.Name + ".  This task will be ignored.", MessageType.Error);
                        reader.Read();
                    }
                }
            }
        }

        public void WriteXml(XmlWriter writer)
        {
            foreach (BuildTask task in this)
            {
                XmlSerializer serializer = new XmlSerializer(task.GetType());
                serializer.Serialize(writer, task, XmlNamespaces);
            }
        }
    }

    public class BuildOptions
    {
        [XmlAttribute("deterministic")]
        public bool Deterministic;  //Scheduling type to replicate task assignments across builds.

        [XmlAttribute("clean")]
        public bool Clean;

        [XmlAttribute("rebuild")]
        public bool Rebuild;

        [XmlAttribute("clientfile")]
        public string ClientFile;

        [XmlAttribute("retrievechangelist")]
        public bool RetrieveChangelist;

        [XmlAttribute("submitchangelist")]
        public bool SubmitChangelist;

        [XmlAttribute("submitonfailure")]
        public bool SubmitOnFailure;

        public BuildOptions()
        {
            Deterministic = false;
            Clean = false;
            ClientFile = null;
            RetrieveChangelist = false;
            SubmitChangelist = false;
            SubmitOnFailure = false;
        }
    }

    [Serializable]
    public class BuildFile : IXmlSerializable
    {
        [XmlAttribute("Name")]
        public string BuildName;

        [XmlAttribute("BuildID")]
        public int BuildID;
        private readonly int InvalidBuildID = -1;

        [XmlAttribute("BuildType")]
        public string BuildType;

        public BuildOptions Options;
        
        public InitializationTaskList InitializationTasks;
        public BuildTaskList Tasks;

        private static XmlSerializerNamespaces XmlNamespaces;

        public delegate void OutputReceivedCallback(string output, MessageType type);
        public static event OutputReceivedCallback OutputReceived = null;

        public BuildFile()
        {
            BuildName = "Unknown";
            BuildID = InvalidBuildID;
            BuildType = null;
            Options = new BuildOptions();
            InitializationTasks = new InitializationTaskList();
            Tasks = new BuildTaskList();
            XmlNamespaces = new XmlSerializerNamespaces();
            XmlNamespaces.Add("", "");
        }

        /// <summary>
        /// Returns whether the build file is part of a build identification.
        /// </summary>
        /// <returns></returns>
        public bool HasValidBuildID()
        {
            return BuildID != InvalidBuildID;
        }

        /// <summary>
        /// Logs to the correct output stream.
        /// </summary>
        /// <param name="output"></param>
        /// <param name="type"></param>
        public static void Log ( string output, MessageType type )
        {
            if ( OutputReceived != null )
                OutputReceived(output, type);
            else
                Console.WriteLine(output);
        }

        /// <summary>
        /// Deserializes the specified build file.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="buildFile"></param>
        /// <returns>true on success deserialization</returns>
        public static bool Load(string filename, ref BuildFile buildFile)
        {
            if (File.Exists(filename) == false)
            {
                Log("Build file " + filename + " does not exist.  Unable to start build!", MessageType.Error);
                return false;
            }

            //Deserialize the build filea.
            StreamReader reader = null;
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(BuildFile));
                reader = new StreamReader(filename);
                buildFile = (BuildFile)serializer.Deserialize(reader);
                reader.Close();

                buildFile.Options.ClientFile = Environment.ExpandEnvironmentVariables(buildFile.Options.ClientFile);
            }
            catch(Exception e)
            {
                buildFile = null;
                Log(e.Message, MessageType.FatalError);
                reader.Close();
                return false;
            }

            return true;
        }

        /// <summary>
        /// Serializes the BuildFile object.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="buildFile"></param>
        /// <returns></returns>
        public static bool Save(string filename, ref BuildFile buildFile)
        {
            try
            {
                StreamWriter writer = new StreamWriter(filename);

               XmlSerializer serializer = new XmlSerializer(typeof(BuildFile));
               serializer.Serialize(writer, buildFile, XmlNamespaces);

               writer.Close(); 
            }
            catch (Exception e)
            {
                Log(e.Message, MessageType.Error);
                return false;
            }

            return true;
        }

        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            BuildName = reader.GetAttribute("Name");
            string buildIDAttribute = reader.GetAttribute("BuildID");
            if (String.IsNullOrWhiteSpace(buildIDAttribute) == true)
            {
                //Non-existent attribute.
                BuildID = InvalidBuildID;
            }
            else if (Int32.TryParse(buildIDAttribute, out BuildID) == false)
            {
                //Invalid number.
                BuildID = InvalidBuildID;
            }

            reader.Read();

            XmlSerializer optionsSerializer = new XmlSerializer(typeof(BuildOptions));
            Options = (BuildOptions)optionsSerializer.Deserialize(reader);

            XmlSerializer initTaskSerializer = new XmlSerializer(typeof(InitializationTaskList));
            InitializationTasks = (InitializationTaskList)initTaskSerializer.Deserialize(reader);

            reader.Read();

            XmlSerializer serializer = new XmlSerializer(typeof(BuildTaskList));
            Tasks = (BuildTaskList) serializer.Deserialize(reader);
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteAttributeString("Name", BuildName);

            XmlSerializer optionsSerializer = new XmlSerializer(typeof(BuildOptions));
            optionsSerializer.Serialize(writer, Options, XmlNamespaces);

            XmlSerializer initTaskSerializer = new XmlSerializer(typeof(InitializationTaskList));
            initTaskSerializer.Serialize(writer, InitializationTasks, XmlNamespaces);

            XmlSerializer serializer = new XmlSerializer(typeof(BuildTaskList));
            serializer.Serialize(writer, Tasks, XmlNamespaces);
        }
    }
}
