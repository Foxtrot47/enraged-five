﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using AutomatedBuildShared;

namespace AutomatedBuild
{
    public partial class ClientInfoPanel : UserControl
    {
        public ClientInfo m_ClientInfo;
        
        public ClientInfoPanel(ClientInfo clientInfo)
        {
            InitializeComponent();

            m_ClientInfo = clientInfo;
            UpdateText();
        }

        public void UpdateText()
        {
            ClientNameLabel.Text = m_ClientInfo.ClientName;
            StatusLabel.Text = m_ClientInfo.State.ToString();
            TaskTextBox.Text = m_ClientInfo.TaskDescription;
            TimeLabel.Text = String.Format("{0:hh\\:mm\\:ss}", m_ClientInfo.TimeElapsed);

            Color backColor = Color.Gray;
            if (m_ClientInfo.State == ClientState.Active)
            {
                backColor = Color.Green;
            }
            else if (m_ClientInfo.State == ClientState.Queued)
            {
                backColor = Color.Blue;
            }
            else
            {
                backColor = Color.Red;
            }

            TimeLabel.BackColor = backColor;
            TaskTextBox.BackColor = backColor;
            
            this.BackColor = backColor;
        }
    }
}
