﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Xsl;

using AutomatedBuildShared;

namespace AutomatedBuild
{
    public class TaskReportInfo
    {
        public TaskResult Result;
        public string MachineName;
        public string TimeElapsed;
        public string TaskDescription;
    }

    public class TaskReport
    {
        private BuildClient Client;
        private BuildTask Task;
        private TimeSpan TimeElapsed;
        private string ReportXSLDirectory;
        private string OutputDirectory;
        private string TaskReportXslFile = "taskreport.xsl";

        public TaskReport()
        {
            Client = null;
            Task = null;
            TimeElapsed = new TimeSpan(0);
            OutputDirectory = null;
            ReportXSLDirectory = null;
        }

        public TaskReport(BuildClient client, BuildTask task, string reportXSLDir, string outputDir)
        {
            Client = client;
            TimeElapsed = new TimeSpan(0, 0, (int) task.TimeElapsed);
            Task = task;
            OutputDirectory = outputDir;
            ReportXSLDirectory = reportXSLDir;
        }

        public bool Generate(out string errorMessage)
        {
            string taskReportXslFile = Path.Combine(ReportXSLDirectory, TaskReportXslFile);
            if (File.Exists(taskReportXslFile) == false)
            {
                errorMessage = "Task report " + taskReportXslFile + " does not exist.";
                return false;
            }

            string taskReportName = Task.ToReportName();

            if (BuildTask.InitializationTasks.Contains(Task.GetType()) == true)
            {
                //If it is an initialization task, then ensure that the machine name is used.
                //Each initialization task is sent to every client.
                taskReportName += "_" + Client.m_MachineName;
            }

            string taskReportXmlSubDirectory = Path.Combine(OutputDirectory, "xml");
            try
            {
                if (Directory.Exists(taskReportXmlSubDirectory) == false)
                    Directory.CreateDirectory(taskReportXmlSubDirectory);
            }
            catch (Exception e)
            {
                errorMessage = "Unable to create directory " + taskReportXmlSubDirectory + ".\n" + e.Message;
                return false;
            }

            string taskReportOutputFile = Path.Combine(taskReportXmlSubDirectory, taskReportName + ".xml");
            string taskReportOutputHtmlFile = Path.Combine(OutputDirectory, taskReportName + ".html");

            try
            {
                Task.FilterReport();

                TaskReportInfo taskReportInfo = new TaskReportInfo();
                taskReportInfo.Result = Task.Result;

                if (Client != null)
                {
                    taskReportInfo.MachineName = Client.m_MachineName;
                }
                else
                {
                    taskReportInfo.MachineName = Environment.MachineName;
                }
                taskReportInfo.TimeElapsed = String.Format("{0:hh\\:mm\\:ss\\.ffff}", TimeElapsed);
                taskReportInfo.TaskDescription = Task.ToString();

                XmlSerializer serializer = new XmlSerializer(typeof(TaskReportInfo));
                StreamWriter writer = new StreamWriter(taskReportOutputFile);
                serializer.Serialize(writer, taskReportInfo);
                writer.Close();
            }
            catch (Exception e)
            {
                errorMessage = "Unable to serialize task report.\n";
                errorMessage += e.Message;
                return false;
            }

            try
            {
                XslCompiledTransform transform = new XslCompiledTransform();
                transform.Load(taskReportXslFile);
                transform.Transform(taskReportOutputFile, taskReportOutputHtmlFile);
            }
            catch (Exception e)
            {
                errorMessage = "Unable to transform HTML using the XSL document" + taskReportXslFile;
                errorMessage += e.Message;
                return false;
            }

            errorMessage = "";
            return true;
        }
    }
}
