﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

using RSG.Base.SCM;

namespace AutomatedBuild
{
    class Program
    {
        static BuildInterface m_InterfaceForm = null;
        static Thread m_BuildEngineThread = null;
        static Thread m_UIThread = null;
        static BuildEngine m_Engine;

        public delegate void InvokeShutdown();

        static readonly string TaskFileArgument = "-taskfile";
        static readonly string ChangelistArgument = "-changelist";
        static readonly string ShelvedChangelistArgument = "-shelvechange";
        static readonly string RetrieveChangelistArgument = "-retrievechange";
        static readonly string SubmitChangelistArgument = "-submit";
        static readonly string LabelArgument = "-label";
        static readonly string ReportDirectoryArgument = "-reportsdir";
        static readonly string NoInterfaceArgument = "-nointerface";
        static readonly string CleanArgument = "-clean";
        static readonly string RebuildArgument = "-rebuild";
        static readonly string CCFormattedArgument = "-cc";
        static readonly string HistoryArgument = "-historydir";
        static readonly string ReportXSLArgument = "-reportxsldir";
        static readonly string StressTestArgument = "-stresstest";
        static readonly string DebugArgument = "-debug";
        static readonly string StatusUpdatesArgument = "-statusupdates";
        static readonly string ProjectStateArgument = "-projectstate";

        static string taskFile = null;
        static int changelistNumber = -1;
        static int shelveChangelist = -1;
        static bool retrieveChangelist = false;
        static bool submitChangelist = false;
        static string label = null;
        static string reportDir = null;
        static bool loadInterface = true;
        static bool clean = false;
        static bool rebuild = false;
        static bool cruiseControlFormatted = false;
        static string historyPath = null;
        static string reportXSLPath = null;
        static bool stressTest = false;
        static bool debug = false;
        static bool statusUpdates = false;
        static string projectStateFile = null;

        static bool m_EngineThreadResult;

        public static void BuildEngineThread()
        {
            if (label != null)
            {
                m_Engine.Label = label;
            }

            List<int> shelvedChangelists = new List<int>();
            if (shelveChangelist != -1)
            {
                shelvedChangelists.Add(shelveChangelist);
            }

            if (reportDir != null)
            {
                m_Engine.ReportDirectory = reportDir;
                m_Engine.OverrideReportDirectory = true;
            }

            m_Engine.EnableDebugMessages = debug;
            m_Engine.EnableStatusUpdates = statusUpdates;
            m_Engine.CruiseControlFormatted = cruiseControlFormatted;
            m_Engine.ProjectStateFile = projectStateFile;
            m_EngineThreadResult = m_Engine.Start(taskFile, clean, rebuild, shelvedChangelists, retrieveChangelist, submitChangelist);

            if (m_InterfaceForm != null)
            {
                InvokeShutdown invokeShutdown = delegate()
                {
                    m_InterfaceForm.Shutdown();
                };

                m_InterfaceForm.Invoke(invokeShutdown, new object[0]);
            }
        }

        public static void UIThread()
        {
            m_InterfaceForm = new BuildInterface(true);

            m_Engine.OutputReceived += m_InterfaceForm.OnOutputReceived;
            m_Engine.BuildStarted += m_InterfaceForm.OnBuildStarted;
            m_Engine.BuildFinished += m_InterfaceForm.OnBuildFinished;
            m_Engine.BuildUpdate += m_InterfaceForm.OnBuildUpdate;

            m_InterfaceForm.BuildStart += m_Engine.Start;
            m_InterfaceForm.BuildAbort += m_Engine.Abort;
            m_InterfaceForm.BuildLoad += m_Engine.Load;

            m_InterfaceForm.ShowDialog();
        }

        [STAThreadAttribute]
        static int Main(string[] args)
        {

            //Set default paths.
            reportXSLPath = Path.Combine(Environment.GetEnvironmentVariable("RS_TOOLSROOT"), "etc\\CruiseControl\\AutomatedBuild\\xsl\\");
            historyPath = Path.Combine(Environment.GetEnvironmentVariable("RS_TOOLSROOT"), "etc\\CruiseControl\\AutomatedBuild\\history\\");

            for (int argumentIndex = 0; argumentIndex < args.Length; argumentIndex++)
            {
                string argument = args[argumentIndex];
                if (String.Compare(argument, TaskFileArgument, true) == 0)
                {
                    taskFile = args[++argumentIndex];
                }
                else if (String.Compare(argument, ChangelistArgument, true) == 0)
                {
                    if (Int32.TryParse(args[++argumentIndex], out changelistNumber) == false)
                    {
                        Console.WriteLine("Unable to parse changelist number argument: " + args[argumentIndex + 1] + ".");
                        return -1;
                    }

                    //NOTE:  To handle instances where Cruise Control is forcibly triggered and there may be no modifications.
                    //
                    if (changelistNumber == 0)
                    {
                        //There have been no modifications.  Grab the latest.
                        changelistNumber = ragePerforce.GetLastSubmittedChangelist();
                    }
                }
                else if (String.Compare(argument, ShelvedChangelistArgument, true) == 0)
                {
                    if (Int32.TryParse(args[++argumentIndex], out shelveChangelist) == false)
                    {
                        Console.WriteLine("Unable to parse changelist number argument: " + args[argumentIndex + 1] + ".");
                        return -1;
                    }
                }
                else if (String.Compare(argument, NoInterfaceArgument, true) == 0)
                {
                    loadInterface = false;
                }
                else if (String.Compare(argument, ReportDirectoryArgument, true) == 0)
                {
                    reportDir = args[++argumentIndex];
                }
                else if (String.Compare(argument, LabelArgument, true) == 0)
                {
                    label = args[++argumentIndex];
                }
                else if (String.Compare(argument, HistoryArgument, true) == 0)
                {
                    historyPath = args[++argumentIndex];
                }
                else if (String.Compare(argument, ReportXSLArgument, true) == 0)
                {
                    reportXSLPath = args[++argumentIndex];
                }
                else if (String.Compare(argument, ProjectStateArgument, true) == 0)
                {
                    projectStateFile = args[++argumentIndex];
                }
                else if (String.Compare(argument, CleanArgument, true) == 0)
                {
                    clean = true;
                }
                else if (String.Compare(argument, RebuildArgument, true) == 0)
                {
                    rebuild = true;
                }
                else if (String.Compare(argument, RetrieveChangelistArgument, true) == 0)
                {
                    retrieveChangelist = true;
                }
                else if (String.Compare(argument, SubmitChangelistArgument, true) == 0)
                {
                    submitChangelist = true;
                }
                else if (String.Compare(argument, CCFormattedArgument, true) == 0)
                {
                    cruiseControlFormatted = true;
                }
                else if (String.Compare(argument, StressTestArgument, true) == 0)
                {
                    stressTest = true;
                }
                else if (String.Compare(argument, DebugArgument, true) == 0)
                {
                    debug = true;
                }
                else if (String.Compare(argument, StatusUpdatesArgument, true) == 0)
                {
                    statusUpdates = true;
                }

            }
            
            m_Engine = new BuildEngine(reportXSLPath, historyPath);

            //m_Engine.BuildSerializedXml();

            if (args.Length == 0)
            {
                m_InterfaceForm = new BuildInterface(false);

                //Start the user interface to hook into the BuildClient's output.
                m_Engine.OutputReceived += m_InterfaceForm.OnOutputReceived;
                m_Engine.BuildStarted += m_InterfaceForm.OnBuildStarted;
                m_Engine.BuildUpdate += m_InterfaceForm.OnBuildUpdate;
                m_Engine.BuildFinished += m_InterfaceForm.OnBuildFinished;

                m_InterfaceForm.BuildStart += m_Engine.Start;
                m_InterfaceForm.BuildAbort += m_Engine.Abort;
                m_InterfaceForm.BuildLoad += m_Engine.Load;
                m_InterfaceForm.ShowDialog();
            }
            else
            {
                int testCount = 0;

                do
                {
                    if (loadInterface == true)
                    {
                        m_UIThread = new Thread(UIThread);
                        m_UIThread.Name = "User Interface Thread";
                        m_UIThread.Start();

                        while (m_InterfaceForm == null || m_InterfaceForm.IsHandleCreated == false)
                            Thread.Sleep(100);
                    }

                    m_BuildEngineThread = new Thread(BuildEngineThread);
                    m_BuildEngineThread.Name = "Build Engine Thread";
                    m_BuildEngineThread.Start();

                    if (m_UIThread != null)
                    {
                        m_UIThread.Join();

                        m_Engine.OutputReceived -= m_InterfaceForm.OnOutputReceived;
                        m_Engine.BuildStarted -= m_InterfaceForm.OnBuildStarted;
                        m_Engine.BuildFinished -= m_InterfaceForm.OnBuildFinished;
                        m_Engine.BuildUpdate -= m_InterfaceForm.OnBuildUpdate;

                        m_InterfaceForm.BuildStart -= m_Engine.Start;
                        m_InterfaceForm.BuildAbort -= m_Engine.Abort;
                        m_InterfaceForm.BuildLoad -= m_Engine.Load;
                    }

                    m_BuildEngineThread.Join();

                    testCount++;
                }
                while (stressTest == true);
            }

            if (m_Engine.m_FailedTasks.Count > 0 || m_EngineThreadResult == false)
                return -1;
            else
                return 0;
        }
    }
}
