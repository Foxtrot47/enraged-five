// 
// /testdevil.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "il.h"
#include "ilu.h"

#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include "ragehdrlib/HdrTextureToolLib.h"
#include "file/token.h"
#include "file/asset.h"
#include "file/stream.h"
#include "system/param.h"

#include "system/simpleallocator.h"
#include "system/externalallocator.h"
#include "system/multiallocator.h"
#include "system/externalheap.h"

#pragma comment(lib, "../devil/libtiff.lib")

#if __WIN32
#pragma warning(disable: 4996)
#endif


// The types of image exports this program can do
enum ExportType
{
	EXPORT_SINGLETEXTURE,
	EXPORT_SINGLEHDR,
	EXPORT_MULTIHDR
};

static const int MAX_STR_LENGTH = 512;
static const int MAX_ARGUMENTS = 20;
static char* IntermediateFilestring = "c:/_HDR_INTERMEDIATE_FILE_";


PARAM(src, "Placeholder for testDevIL cmd line arg parsing");
PARAM(dst, "Placeholder for testDevIL cmd line arg parsing");
PARAM(cropPow2Rounding, "Placeholder for testDevIL cmd line arg parsing");
PARAM(fmt, "Placeholder for testDevIL cmd line arg parsing");
PARAM(width, "Placeholder for testDevIL cmd line arg parsing");
PARAM(height, "Placeholder for testDevIL cmd line arg parsing");
PARAM(mips, "Placeholder for testDevIL cmd line arg parsing");
PARAM(cropUmin, "Placeholder for testDevIL cmd line arg parsing");
PARAM(cropVmin, "Placeholder for testDevIL cmd line arg parsing");
PARAM(cropUmax, "Placeholder for testDevIL cmd line arg parsing");
PARAM(cropVmax, "Placeholder for testDevIL cmd line arg parsing");
PARAM(filter, "Placeholder for testDevIL cmd line arg parsing");


 void DisplayHelp(){
	 printf("USAGE: testdevil -src=SOURCENAME.EXT -dst=DESTNAME.EXT "
		 "-fmt=<AUTO, DXT1, DXT3, DXT5, NORMALMAP, HDR, NONE> -maxsize=### -scale=### "
		 "-cropUmin=### -cropVmin=### -cropUmax=### -cropVmax=### -cropPow2Rounding=<up, down> "
		 "-filter=<nearest, linear, bilinear, box, triangle, bell, bspline, lanczos3, mitchell> "
		 "-hdr=SOURCENAME.HDT"
			"-width=### -height=### -mips=<1, 0>\n\n\tParams in <> "
			"are selection lists.  First entry is default"
			"\n\tParams marked with ### represent integer values\n");
 }
 

////////////////////////////////////////////////////////////////////////////////
//
//	PURPOSE
//		As of 10/18 testDevil is crashing on HDR exports because rage::sysMemAllocator
//	doesn't exist by the time the HDR lib code runs.  This was working before 195
//	and I'm not sure what changed.
//		For now this code will init the allocator.
//
void InitMemory()
{
#define SIMPLE_HEAP_SIZE	(208*1024*1024)
#define SIMPLE_PHYSICAL_SIZE SIMPLE_HEAP_SIZE
#define SIMPLE_PHYSICAL_NODES 8192
	static ::rage::sysMemSimpleAllocator gameAllocator((SIMPLE_HEAP_SIZE),sysMemSimpleAllocator::HEAP_MAIN);
	static ::rage::u8 Workspace[COMPUTE_WORKSPACE_SIZE(SIMPLE_PHYSICAL_NODES)];
	static ::rage::sysMemExternalAllocator physicalAllocator((SIMPLE_PHYSICAL_SIZE),::rage::sysMemPhysicalAllocate((SIMPLE_PHYSICAL_SIZE)),(SIMPLE_PHYSICAL_NODES),Workspace);
	static ::rage::sysMemMultiAllocator theAllocator;
	theAllocator.AddAllocator(gameAllocator);
	theAllocator.AddAllocator(physicalAllocator);
	theAllocator.AddAllocator(physicalAllocator);	// Add physical allocator twice, there's no difference on !__PPU platforms.

#if defined(FIXED_VIRTUAL_RESOURCE_ALLOCATOR) && defined(FIXED_PHYSICAL_RESOURCE_ALLOCATOR)  // the app needs to define these if they want to used fixed resource heaps (selectable by callback in rscbuilder.cpp)
	theAllocator.AddAllocator(FIXED_VIRTUAL_RESOURCE_ALLOCATOR);
	theAllocator.AddAllocator(FIXED_PHYSICAL_RESOURCE_ALLOCATOR);
#endif
	::rage::sysMemAllocator::SetCurrent(theAllocator);
	::rage::sysMemAllocator::SetMaster(theAllocator);
}


////////////////////////////////////////////////////////////////////////////////
//
//	PURPOSE
//		Creates an HDR DDS texture by compositing several input textures.
//
void hdrExporter(char** IntermediateFilename, float* InputExposure, int numExposures, char* OutputFile)
{
	HDRTextureTool tool;
	HDRTextureTool::RunParams params;

	// Early exit
	if ((IntermediateFilename == NULL) || (InputExposure == NULL) || (OutputFile == NULL))
		return;

	params.inputFiles.Reserve(numExposures);
	params.exposures.Reserve(numExposures);
	params.inputFiles.Resize(numExposures);
	params.exposures.Resize(numExposures);

	for (int i = 0; i < numExposures; i++)
	{
		params.inputFiles[i] = IntermediateFilename[i];
		params.exposures[i] = InputExposure[i];
	}

	params.doExport = true;
	params.outputFile = OutputFile;
	tool.Init("", true);
	tool.Run(&params);
	tool.Shutdown();

	params.inputFiles.Reset();
	params.exposures.Reset();
}


////////////////////////////////////////////////////////////////////////////////
//
//	PURPOSE
//		Exports a single texture, saving in HDR DDS format.  A default exposure of
//	0 is used.
//
void hdrExporter(char* filename, float exposure, char* outputFile)
{
	HDRTextureTool tool;
	HDRTextureTool::RunParams params;

	// Early exit
	if ((filename == NULL) || (outputFile == NULL))
		return;

	params.doExport = true;
	params.outputFile = outputFile;

	// Just one texture coming through!
	params.inputFiles.Reserve(1);
	params.exposures.Reserve(1);
	params.inputFiles.Resize(1);
	params.exposures.Resize(1);
	params.inputFiles[0] = filename;
	params.exposures[0] = exposure;

	tool.Init("", true);
	tool.Run(&params);
	tool.Shutdown();

	params.inputFiles.Reset();
	params.exposures.Reset();
}


////////////////////////////////////////////////////////////////////////////////
//
//	PURPOSE
//		The workhorse of this program.  This function exports a single texture
//	performing additional processing as needed, such as resizing.
//
bool ImageExporter(int argc, char **argv)
{
	ILuint max_width = 4096, max_height = 4096;
	ILint exact_width = -1, exact_height = -1;
	ILfloat fScaleFactor = -1.0f;

	ILfloat	fcropUmin= -1.0f;
	ILfloat fcropVmin= -1.0f;
	ILfloat fcropUmax= -1.0f;
	ILfloat fcropVmax= -1.0f;

	ILenum eScaleFilter = ILU_SCALE_LANCZOS3;		// default filter is Lanczos, same as old texgen

	char *src = "test.tif";
	char *dest = "test.dds";
	int format = 0xffffffff;			// Assume alpha (And DDS output format)
	bool mips = true;
	bool cropPow2RoundingUp = true;

	// ---Command line parse code
	for (int i = 1; i < argc; ++i) {
		// Parse the args
		// Ignore '-'
		if ( *argv[i] == '-' )
			argv[i]++;
		if ( !stricmp(argv[i], "?") || !stricmp(argv[i], "help") ) {
			DisplayHelp();
			return true;
		}
		// Find '=' character
		char *c = strchr(argv[i], '=');
		char *restore = NULL;
		if (c)
		{
			restore = c;
			*c = '\0';
		}

		if ( !stricmp(argv[i], "src") )
		{
			src = ++c;
		}
		else if ( !stricmp(argv[i], "dst") )
		{
			dest = ++c;
		}
		else if ( !stricmp(argv[i], "cropPow2Rounding") ) 
		{
			if ( !stricmp(++c, "up") )
			{
				cropPow2RoundingUp = true;
			}
			else if ( !stricmp(c, "down") )
			{
				cropPow2RoundingUp = false;
			}
		}
		else if ( !stricmp(argv[i], "fmt") ) 
		{
			if ( !stricmp(++c, "dxt1") )
			{
				format = IL_DXT1;
			}
			else if ( !stricmp(c, "dxt3") )
			{
				format = IL_DXT3;
			}
			else if ( !stricmp(c, "dxt5") )
			{
				format = IL_DXT5;
			}
			else if ( !stricmp(c, "normalmap") )
			{
				format = IL_DXT_CUSTOM_NORMALMAP;
			}
			else if ( !stricmp(c, "none") )
			{
				format = IL_DXT_NO_COMP;
			}
		}
		else if ( !stricmp(argv[i], "maxsize") ) 
		{
			max_width = max_height = atoi(++c);
		}
		else if ( !stricmp(argv[i], "width") ) 
		{
			exact_width = atoi(++c);
		}
		else if ( !stricmp(argv[i], "height") ) 
		{
			exact_height = atoi(++c);
		}
		else if ( !stricmp(argv[i], "scale") ) 
		{
			fScaleFactor = 1.0f / (float)atoi(++c);
		}			
		else if ( !stricmp(argv[i], "mips") ) 
		{
			mips = atoi(++c) != 0;
		}
		else if ( !stricmp(argv[i], "cropUmin") ) 
		{
			fcropUmin = atof(++c);
		}
		else if ( !stricmp(argv[i], "cropVmin") ) 
		{
			fcropVmin = atof(++c);
		}
		else if ( !stricmp(argv[i], "cropUmax") ) 
		{
			fcropUmax = atof(++c);
		}
		else if ( !stricmp(argv[i], "cropVmax") ) 
		{
			fcropVmax = atof(++c);
		}
		else if (!stricmp(argv[i], "filter"))
		{
			++c;
			if (!stricmp(c, "nearest"))
			{
				eScaleFilter = ILU_NEAREST;
			}
			else if (!stricmp(c, "linear"))
			{
				eScaleFilter = ILU_LINEAR;
			}
			else if (!stricmp(c, "bilinear"))
			{
				eScaleFilter = ILU_BILINEAR;
			}
			else if (!stricmp(c, "box"))
			{
				eScaleFilter = ILU_SCALE_BOX;
			}
			else if (!stricmp(c, "triangle"))
			{
				eScaleFilter = ILU_SCALE_TRIANGLE;
			}
			else if (!stricmp(c, "bell"))
			{
				eScaleFilter = ILU_SCALE_BELL;
			}
			else if (!stricmp(c, "bspline"))
			{
				eScaleFilter = ILU_SCALE_BSPLINE;
			}
			else if (!stricmp(c, "lanczos3"))
			{
				eScaleFilter = ILU_SCALE_LANCZOS3;
			}
			else if (!stricmp(c, "mitchell"))
			{
				eScaleFilter = ILU_SCALE_MITCHELL;
			}
		}

		// This loop punches holes in the argument list.  Kindly replace the killed char.
		if (restore)
		{
			*restore = '=';
		}
	}
	// ---End command line parse code

	// Standardize slashes
	for(char* pcStepper = src; *pcStepper != '\0'; pcStepper++)
	{
		if(*pcStepper == '/') *pcStepper = '\\';
	}
	for(char* pcStepper = dest; *pcStepper != '\0'; pcStepper++)
	{
		if(*pcStepper == '/') *pcStepper = '\\';
	}

	// If the src AND dst files are dds files, just do a raw copy
	char *ext = strrchr(src, '.');
	char *dstext = strrchr(dest, '.');
	if ( ext ) 
	{
		if ( (stricmp(++ext, "dds") == 0) && (stricmp(++dstext, "dds") == 0) &&
			fcropUmin == -1.0f && 
			fcropVmin == -1.0f &&
			fcropUmax == -1.0f &&
			fcropVmax == -1.0f &&
			fScaleFactor == -1.0f
			) 
		{
			char buff[2048];
			char acDirectory[512];
			for(int i=0; i<512; i++) acDirectory[i] = '\0';
			// Build directory structure as needed
			strcpy(acDirectory, dest);
			for(int i=511; i>0; i--) if(acDirectory[i] == '\\') {acDirectory[i] = '\0'; break;}
			sprintf(buff, "mkdir %s 2>NUL:", acDirectory);
			//printf(buff);
			//printf("\n");
			system(buff);

			// Copy file
			sprintf(buff, "copy \"%s\" \"%s\"", src, dest);
			assert(strlen(buff) < sizeof(buff));
			printf("%s\n", buff);
			int iErrorCode = system(buff);
			// printf("iErrorCode = %d\n", iErrorCode);
			if(iErrorCode != 0)
			{
				// Something bad happened
				printf("Given a dds file, I just copy it to the destination without modifing it, but an error occured executing:\n");
				printf(buff);
				printf("\n");
				//return iErrorCode;
				return false;
			}
			return true;
		}
	}

	ilInit();
	iluInit();

	// Enable palette conversion at loading time.
	ilEnable(IL_CONV_PAL);

	// We need to bind images in order to clean up their memory
	ILuint convertImageName;
	ilGenImages(1, &convertImageName);
	ilBindImage(convertImageName);

	iluImageParameter(ILU_FILTER, eScaleFilter);
	
	// If it is a cube map, then just pass it through for now.  This sucks I know.

	// **************  The code that does the "work" ***************
	ILuint val = 0;
	if ( ilLoadImage((ILstring)src) ) 
    {
		// Auto switch format based on alpha content
		if ( format == 0xffffffff ) 
        {
			format = IL_DXT1;
			ILubyte *work = ilGetData();
			ILuint count = (ILuint) ilGetInteger(IL_IMAGE_WIDTH) * ilGetInteger(IL_IMAGE_HEIGHT);
			
			ILuint image_width = ilGetInteger(IL_IMAGE_WIDTH);
			ILuint image_height= ilGetInteger(IL_IMAGE_HEIGHT);

			printf("Image Width = (%d) (%d)\n", image_width, image_height);
			
			val = ilGetInteger(IL_IMAGE_FORMAT);
			printf("Image Format Mode (%d)\n", val);
			if ( (val == IL_RGBA || val == IL_BGRA) && count ) {
				ILubyte refAlpha = work[3];
				for (ILuint i = 0; i < count; ++i) {
					if ( work[3] != refAlpha ) {
						format = IL_DXT5;
						break;
					}
					work += 4;
				}
			}
		}
		// image loaded, get its actual width/height/depth info.
        ILuint image_width = ilGetInteger(IL_IMAGE_WIDTH);
        ILuint image_height= ilGetInteger(IL_IMAGE_HEIGHT);
		ILuint image_depth = ilGetInteger(IL_IMAGE_DEPTH);

		// make sure parameters are all in valid range, otherwise skip the cropping process.
		// test commandlines
		//       -maxsize=128 -fmt=none -src=c:/my_test.tif -dst=c:/my_test_out.dds -cropUmin=0.0 -cropVmin=0.0 -cropUmax=0.5 -cropVmax=0.5 -cropPow2Rounding=up
		//       -maxsize=128 -fmt=none -src=c:/my_test.tif -dst=c:/my_test_out.dds -cropUmin=0.0 -cropVmin=0.0 -cropUmax=0.35 -cropVmax=0.35 -cropPow2Rounding=up
		if(
			( fcropUmin >= 0.f && fcropUmin <= 1.f ) &&
			( fcropVmin >= 0.f && fcropVmin <= 1.f ) &&
			( fcropUmax >= 0.f && fcropUmax <= 1.f ) &&
			( fcropVmax >= 0.f && fcropVmax <= 1.f ) &&
			( fcropUmin <= fcropUmax ) &&
			( fcropVmin <= fcropVmax ) 
			)
		{
			printf("***Cropping Image***");
		
			// Crop the image if that was specified before we do any of the code that imposes maximum size/scaling.

			// convert float uv rectangle cropping parameters to pixel space.
			ILuint cropped_width  = ((ILfloat)image_width  * (fcropUmax-fcropUmin));
			ILuint cropped_height = ((ILfloat)image_height * (fcropVmax-fcropVmin));
			ILuint xoff = (ILfloat)image_width  * fcropUmin;
			ILuint yoff = (ILfloat)image_height * fcropVmin;
			ILuint zoff = 0;

			iluCrop( xoff, yoff, zoff, cropped_width, cropped_height, 0 );// its not a 3d texture and the depth parameter is not the pixel format like i guessed.

			// input textures need to be scaled up now a little if its close to the next pow2 size, or brought down.
			ILfloat original_aspect = image_width / image_height;

			// starting with the source image, we calculate the difference between growing the cropped area back to the current size test

			ILint curr_w = (image_width );
			ILint next_w = (image_width >> 1);

			ILint curr_h = (image_height );
			ILint next_h = (image_height >> 1);

			ILint smallest_w = 32;// dont resize down past this (its tiny)
			ILint smallest_h = 32;// dont resize down past this (its tiny)

			ILuint cropped_to_pow2_width;
			ILuint cropped_to_pow2_height;

			for(;;)
			{
				// We shift down sizes until the cropped amount value is between 'next' and 'curr'
				// then we decide if we round 'down' or 'up'

				// If 'next_w' is still larger than the cropped width, loop.
				if( (cropped_width < curr_w) && (next_w > cropped_width) && (next_w > smallest_w) )
				{
					curr_w = next_w;
					next_w = next_w >> 1;
					continue;
				}

				if( cropPow2RoundingUp &&  (cropped_width!= next_w) )
				{// going to stretch the image up to the next power of 2 (curr_w)
					cropped_to_pow2_width = curr_w;
					break;
				}else{
				// going to scale image down to next power of 2 (next_w)
					cropped_to_pow2_width = next_w;
					break;
				}
			}

			for(;;)
			{
				// We shift down sizes until the cropped amount value is between 'next' and 'curr'
				// then we decide if we round 'down' or 'up'

				// If 'next_w' is still larger than the cropped width, loop.
				if( (cropped_height < curr_h) && (next_h > cropped_height) && (next_h > smallest_h) )
				{
					curr_h = next_h;
					next_h = next_h >> 1;
					continue;
				}

				if( cropPow2RoundingUp && (cropped_height!= next_h) )
				{// going to stretch the image up to the next power of 2 (curr_w)
					cropped_to_pow2_height = curr_h;
					break;
				}else{
					// going to scale image down to next power of 2 (next_w)
					cropped_to_pow2_height = next_h;
					break;
				}
			}

			printf("[rescaling cropped image to %d x %d...]\n",cropped_to_pow2_width, cropped_to_pow2_height );
			iluScale( cropped_to_pow2_width, cropped_to_pow2_height, image_depth);

			// update variables the rest of the code refers to here before we exit.
			// this allows the options -maxwidth and -maxheight to work on the cropped and pow2 adjusted image.
			image_width = cropped_to_pow2_width;
			image_height= cropped_to_pow2_height;
		}

		if (exact_width > 0 && exact_height > 0) 
		{
			if (exact_width > image_width || exact_height > image_height)
			{
				printf("**** HEY! You're scaling an image up here, this probably wasn't what you wanted!\n");
			}
			image_width = exact_width;
			image_height = exact_height;
			printf("[exact rescaling image to %d x %d...]\n",image_width,image_height);
			iluScale(image_width,image_height,image_depth);
		}
		else if(fScaleFactor > 0.0f)
		{
			// They want to scale the image
			image_width = (int)((float)image_width * fScaleFactor);
			image_height = (int)((float)image_height * fScaleFactor);
			printf("[rescaling image to %d x %d...]\n",image_width,image_height);
			iluScale(image_width,image_height,image_depth);
		}
		else if (max_width != 0 && max_height != 0 && (image_width > max_width ||  image_height > max_height)) 
		{
			while (image_width > max_width || image_height > max_height) {
				image_width >>= 1;
				image_height >>= 1;
			}
			printf("[shrinking image to %d x %d...]\n",image_width,image_height);
			iluScale(image_width,image_height,image_depth);
		}

        printf("Image Width = (%d) (%d)\n", image_width, image_height);
		printf("DXTC Format (%d) (%s)\n", format, format==IL_DXT5?"DXT5":format==IL_DXT1?"DXT1":"unknown");

		ilSetInteger(IL_DXTC_FORMAT, format);	// Set the dxt format
		ilEnable(IL_FILE_OVERWRITE);			// Set this to overwrite existing file
		
		if ( mips ) {
			iluBuildMipmaps();						// Generate mip-maps
		}
		ilSaveImage((ILstring)dest);			// USE THIS VERSION OF SAVE!  other versions don't
												// seem to support as many different formats.
	}
	
	// Unbind this image & delete it
	ilBindImage(0);
	ilDeleteImages(1, &convertImageName);	
	return true;
}


////////////////////////////////////////////////////////////////////////////////
//
//	PURPOSE
//		Handles a single HDR texture export.
//
bool SingleHDRExporter(int argc, char **argv)
{
	int argc2 = argc;
	char** argv2;
	char OutputFile[MAX_STR_LENGTH];
	int dstArgIdx = -1;
	char InputExtension[MAX_STR_LENGTH];
	bool success = true;

	InitMemory();

	if (argc >= MAX_ARGUMENTS)
	{
		// If you see this, increase MAX_ARGUMENTS to what argc2 was reported as.
		printf("ERROR: TestDevil cannot handle %u arguments!  Code patch required.\n", argc2);
		return false;
	}

	// Create fake argument list for subprocessing
	argv2 = (char**)malloc(argc2 * sizeof(char*));
	for (int i = 0; i < argc2; i++)
	{
		argv2[i] = (char*)malloc(MAX_STR_LENGTH * sizeof(char));
		argv2[i][0] = '\0';
	}
	InputExtension[0] = '\0';
	OutputFile[0] = '\0';

	// First argument (program name) goes right through
	strcpy(argv2[0], argv[0]);

	// Dice the command arguments, so we save to an intermediate file and then HDR process that file.
	for (int i = 1; i < argc; ++i)
	{
		char buffer[MAX_STR_LENGTH];
		char* argument = buffer;

		// Parse the args
		strcpy(buffer, argv[i]);
		// Ignore '-'
		if (argument[0] == '-')
			argument++;
		// Find '=' character
		char *c = strchr(argument, '=');
		if (c)
		{
			*c = '\0';
			c++;
		}


		// Replace "-fmt=hdr" with "-fmt=none"
		if (!stricmp(argument, "fmt"))
		{
			sprintf(argv2[i], "-fmt=none");
		}
		// Replace "-dst=OUTPUT.EXT" with "-dist=INTEMEDIATE.(SRC.EXT)"
		else if (!stricmp(argument, "dst"))
		{
			strcpy(OutputFile, c);

			// Depending on how the arguments are passed, we may not have the "-src" param yet, so flag this index for post fixup.
			dstArgIdx = i;
		}
		// Find out what extension the source image has, because we need the intermediate output image to use the same one.
		else if (!stricmp(argument, "src"))
		{
			char* extension = strchr(c, '.');
			if (!extension)
			{
				printf("ERROR: Input texture does not have an extension! HDR texture export is doomed for %s\n", c);
				success = false;
			}
			else
			{
				strcpy(InputExtension, extension);
			}

			strcpy(argv2[i], argv[i]);
		}
		else
		{
			// All other arguments get passed through
			strcpy(argv2[i], argv[i]);
		}
	}


	// If everything checks out, then go!
	if (success)
	{
		char IntermediateFilename[MAX_STR_LENGTH];
		char** argv3;

		// ImageExporter is going to junk its input pointers, so clone our argv2 pointers and give it the clones.
		argv3 = (char**)malloc(argc2 * sizeof(char*));
		for (int i = 0; i < argc2; i++)
		{
			argv3[i] = argv2[i];
		}

		sprintf(IntermediateFilename, "%s%s", IntermediateFilestring, InputExtension);

		// Fixup dest
		if (dstArgIdx >= 0)
		{
			sprintf(argv2[dstArgIdx], "-dst=%s", IntermediateFilename);
		}


		// If the input texture is a .DDS file, just copy it.
		if ((InputExtension) && (!stricmp(InputExtension, ".dds")))
		{
			// This function will do it for us.
			strcpy(argv2[dstArgIdx], argv[dstArgIdx]);
			ImageExporter(argc2, argv3);
		}
		else
		{
			// First process the source image (resize, etc.)
			ImageExporter(argc2, argv3);

			// ..then create an HDR texture
			hdrExporter(IntermediateFilename, 0.0f, OutputFile);
		}

		free(argv3);
	}


	// Clean up
	for (int i = 0; i < argc2; i++)
	{
		free(argv2[i]);
		argv2[i] = NULL;
	}
	free(argv2);
	argv2 = NULL;

	return success;
}


////////////////////////////////////////////////////////////////////////////////
//
//	PURPOSE
//		Handles an HDR texture export in which multiple input textures are used
//	in the construction of the final output texture.
//
bool MultiHDRExporter(int argc, char **argv)
{
	int numExposures = 0;
	char HDTfilename[MAX_STR_LENGTH];
	char OutFilename[MAX_STR_LENGTH];
	char** InputFilename = NULL;
	float* InputExposure = NULL;
	char** IntermediateFilename = NULL;
	HDTfilename[0] = '\0';
	OutFilename[0] = '\0';
	int dstIdx = -1;
	int srcIdx = -1;

	InitMemory();

	// The fake arg list
	int argc2 = argc + 1;	// Add one because we need to insert a "-src"
	char** argv2;
	argv2 = (char**)malloc(argc2 * sizeof(char*));
	for (int i = 0; i < argc2; i++)
	{
		argv2[i] = (char*)malloc(MAX_STR_LENGTH * sizeof(char));
		argv2[i][0] = '\0';
	}

	// First argument (program name) goes right through
	strcpy(argv2[0], argv[0]);


	// Parse the command line to collect args and find out the .HDT filename
	for (int i = 1; i < argc; ++i)
	{
		char buffer[MAX_STR_LENGTH];
		char* argument = buffer;

		strcpy(buffer, argv[i]);
		if (argument[0] == '-')
			argument++;
		char *c = strchr(argument, '=');
		if (c)
		{
			*c = '\0';
			c++;
		}

		if (!stricmp(argument, "hdr"))
		{
			// -hdr=INPUT.HDT
			strcpy(HDTfilename, c);
		}
		else if (!stricmp(argument, "fmt"))
		{
			// Replace "-fmt=hdr" with "-fmt=none"
			sprintf(argv2[i], "-fmt=none");
		}
		else if (!stricmp(argument, "dst"))
		{
			// Save the output filename, we'll need it for the HDR step.
			strcpy(OutFilename, c);

			// Depending on how the arguments are passed, we may not have the "-src" param yet, so flag this index for post fixup.
			dstIdx = i;
		}
		else if (!stricmp(argument, "src"))
		{
			// There should never be a -src when doing a multi-HDR export, but in case one leaks in ignore it.
		}
		else
		{
			strcpy(argv2[i], argv[i]);
		}
	}
	srcIdx = argc;



	// Read the HDT
	fiStream* hdtFile = ASSET.Open(HDTfilename, "hdt", false, true);
	if(hdtFile)
	{
		fiTokenizer HDTtokenizer("HDTfiletokenizer", hdtFile);
		numExposures = HDTtokenizer.MatchInt("TextureCount");

		InputFilename = (char**)malloc(numExposures * sizeof(char*));
		InputExposure = (float*)malloc(numExposures * sizeof(float));
		IntermediateFilename = (char**)malloc(numExposures * sizeof(char*));

		for (int i = 0; i < numExposures; i++)
		{
			char token[MAX_STR_LENGTH];
			float exposure = 0;
			char* filename;
			token[0] = 0;

			HDTtokenizer.GetToken(token, MAX_STR_LENGTH);
			InputExposure[i] = HDTtokenizer.GetFloat();

			InputFilename[i] = (char*)malloc(MAX_STR_LENGTH * sizeof(char));
			strcpy(InputFilename[i], token);
		}

		hdtFile->Close();
	}


	// Now grind all the input textures through the regular export, saving to intermediate files
	for (int i = 0; i < numExposures; i++)
	{
		char** argv3 = NULL;


		// We need the source texture's extension so we can clone it for the intermediate file.
		char *extension = strchr(InputFilename[i], '.');
		if (extension == NULL)
		{
			printf("ERROR: Input texture does not have an extension! HDR texture export is doomed for %s (from .HDT %s)\n", InputFilename[i], HDTfilename);
		}

		// Create a unique intermediate name for each input texture
		IntermediateFilename[i] = (char*)malloc(MAX_STR_LENGTH * sizeof(char));
		sprintf(IntermediateFilename[i], "%s%u%s", IntermediateFilestring, i, extension);


		// Fixup src
		if (srcIdx >= 0)
		{
			sprintf(argv2[srcIdx], "-src=%s", InputFilename[i]);
		}

		// Fixup dest
		if (dstIdx >= 0)
		{
			sprintf(argv2[dstIdx], "-dst=%s", IntermediateFilename[i]);
		}


		// ImageExporter is going to junk its input pointers, so clone our argv2 pointers and give it the clones.
		argv3 = (char**)malloc(argc2 * sizeof(char*));
		for (int j = 0; j < argc2; j++)
		{
			argv3[j] = argv2[j];
		}

		// Process the source image (resize, etc.)
		ImageExporter(argc2, argv3);

		free(argv3);
	}


	// Last, create the HDR texture!
	if (numExposures > 0)
	{
		hdrExporter(IntermediateFilename, InputExposure, numExposures, OutFilename);
	}


	// Clean up
	if (InputFilename)
	{
		for (int i = 0; i < numExposures; i++)
		{
			if (InputFilename[i])
				free(InputFilename[i]);
		}
		free(InputFilename);
	}

	if (IntermediateFilename)
	{
		for (int i = 0; i < numExposures; i++)
		{
			if (IntermediateFilename[i])
				free(IntermediateFilename[i]);
		}
		free(IntermediateFilename);
	}

	if (InputExposure)
	{
		free(InputExposure);
	}

	return true;
}


////////////////////////////////////////////////////////////////////////////////
//
//	PURPOSE
//		Determines what kind of export should take place by evaluating the
//	program arguments.
//
ExportType DetermineExportType(int argc, char **argv)
{
	ExportType exportMode = EXPORT_SINGLETEXTURE;
	bool hdr = false;
	bool multi = false;

	// ---Command line parse code
	for (int i = 1; i < argc; ++i)
	{
		char buffer[MAX_STR_LENGTH];
		char* argument = buffer;

		// Parse the args
		strcpy(buffer, argv[i]);

		// Ignore '-'
		if (argument[0] == '-')
			argument++;
		// Find '=' character
		char *c = strchr(argument, '=');
		if (c)
		{
			*c = '\0';
		}


		// "-fmt=hdr" tells us that we're definitely dealing with an HDR export
		if (!stricmp(argument, "fmt"))
		{
			c++;
			if (!stricmp(c, "HDR"))
			{
				hdr = true;
			}
		}
		// "-hdr=..." tells us that there are multiple source images that contribute to this HDR image.
		else if (!stricmp(argument, "hdr"))
		{
			multi = true;
		}
	}


	if (hdr)
	{
		if (multi)
			exportMode = EXPORT_MULTIHDR;
		else
			exportMode = EXPORT_SINGLEHDR;
	}

	return exportMode;
}


////////////////////////////////////////////////////////////////////////////////
//
//	PURPOSE
//		The main function.
//
int main(int argc, char **argv)
{
	ExportType exportMode = DetermineExportType(argc, argv);

//	InitGameHeap();

	sysParam::Init(argc,argv);

	switch (exportMode)
	{
		case EXPORT_SINGLEHDR:
			SingleHDRExporter(argc, argv);
			break;

		case EXPORT_MULTIHDR:
			MultiHDRExporter(argc, argv);
			break;

		case EXPORT_SINGLETEXTURE:
		default:
			ImageExporter(argc, argv);
			break;
	}
}
