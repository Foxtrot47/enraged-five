devIL is an opensource image library.

For documentation or latest version, go here:
	http:\\openil.sourceforge.net

*******************************
CURRENT DEVIL VERSION -> 1.6.7
*******************************

-- GENERAL USAGE NOTES
======================

The code seems to be incomplete and potentially buggy, so beware... :)

Use the iLoadImage(const char *name) & the iSaveImage(const char *name)functions
instead of the other general purpose loaders that accept enums.  The enumerated
versions aren't completely hookedup.  However, the string based load/save routines
seem to be more robust, and they key off of the extension to determine type.

Make sure any tester including this library also manually pulls in the libtiff
library via the #pragma comment(lib, ".........\libtif.lib")  (where ......... is 
relative path to the devil module)  testdevil is a good example of this.

There is an api available to actually access & manipulate the image once it has been
loaded.  Check the documentation on the website for up to date details.

How to convert from tif -> mipmapped, compressed dds in ~10 lines of code:

	#include "il.h"
	#include "ilu.h"		// Needed for mip-map generation
	#pragma comment(lib, "../devil/libtiff.lib")

	
	ilInit();
	iluInit();

	if ( ilLoadImage("test.tif") ) {
		ilSetInteger(IL_DXTC_FORMAT, IL_DXT5);	// Set the dxt format
		ilEnable(IL_FILE_OVERWRITE);			// Set this to overwrite existing file
		iluBuildMipmaps();						// Generate mip-maps
		ilSaveImage("test.dds");
	}	


-- IMPLEMENTATION NOTES
=======================

This was a pain to pull into rage, but it works, and it needs to exist in source form
for debugging purposes in case new bugs pop up.

Since there are a ton of .c & .h files associated with devil, the makefile.bat actually
calls out other batch files to automatically generate the FILES/CODEONLY/HEADONLY env
variables for the il_*.* & ilu_*.* files.  So, if you update devil from the net, you 
will not have to worry about files that fall into those two naming conventions.  However,
you will want to delete any il_* or ilu_* files that should not be included with the project.

devil consists of 3 "layers" - 'il', 'ilu', and 'ilut'  We intentionally drop the 'ilut'
layer since it is primarily for rendering the various images.

Currently, jpg & png formats are disabled since they require external libraries.  We do,
however, have tiff format enabled and we have included the libtiff library, also with
a few minor changes.

I have tried to automate the process of the steps I had to perform when pulling over
the devil source as much as possible.  Hopefully, this will work on future releases.
To take advantage of this, download and extract the latest devil code.  Then, run the
following command:
	copydevilcode.bat <Devil Path (source)> <RAGE Devil Path (destination)>
	Example:
		copydevilcode.bat c:\DevIL-1.6.7\ c:\soft\rage\base\tools\devil

Running copydevilcode should clean all old devil source from rage, copy the required files
from the new source tree back over, make the needed "tweaks" and bugfixes required for
use by rage, and run genproj to make sure it is up to date with the latest files.

If you do update to a newer version of Devil, make sure you update the version number listed
at the top of this document, and please update the various text files used by batchsub.py to
fix any new problems that may have crept into their code..