#if __XENON
#include "file/device.h"
#include "string/string.h"
#include "file/stream.h"
extern "C" {

FILE*  il_fopen(const char *filename, const char *mode)
{
	return (FILE*)rage::fiStream::Open(filename,true);
}

void il_fclose(FILE* handle)
{
	((rage::fiStream*)handle)->Close();
}

int il_fread(void* Buffer, size_t Size, size_t Number, FILE* handle) 
{
	return ((rage::fiStream*)handle)->Read(Buffer, Size*Number);
}

int il_eof(FILE* handle)
{
	return ((rage::fiStream*)handle)->Tell() == ((rage::fiStream*)handle)->Size();
}

int il_ftell(FILE* handle)
{
	return (char)((rage::fiStream*)handle)->Tell();
}

char il_fseek(FILE* handle, int offset, int mode )
{
	mode;
	return (char)((rage::fiStream*)handle)->Seek(offset);
}

char il_fgetc(FILE* handle)
{
	return (char)((rage::fiStream*)handle)->GetCh();
}

}
#endif
