#if __XENON

#include <stdio.h>
FILE* il_fopen(const char *filename, const char *mode);
void il_fclose(FILE* handle);
int il_fread(void* Buffer, size_t Size, size_t Number, FILE* handle);
char il_fgetc(FILE* handle);
int il_eof(FILE* handle);
int il_ftell(FILE* handle);
char il_fseek(FILE* handle, int offset, int mode );

#endif
