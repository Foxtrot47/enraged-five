set ARCHIVE=devil
set ARCHIVE_BASE=devil_base
REM  ** Set any custom files you want to add to this makefile here **
set FILES=xenon_helper il_* ilu_* ilut_*
set CODEONLY=
set HEADONLY=il ilu
set DONT_MERGE_ARCHIVE_INTO_TESTER=anything


set LIBS=%RAGE_CORE_LIBS% %RAGE_GFX_LIBS% devcam devil libtiff ragetexturelib text

set XPROJ=%RAGE_DIR%\base\src %RAGE_DIR%\migrate\src %RAGE_DIR%\suite\src $(RAGE_3RDPARTY)\cli\AMD\ATI_Compress %RAGE_DIR%\base\tools\libs
set WARNINGS=1
set CUSTOM=_rage_readme.txt
