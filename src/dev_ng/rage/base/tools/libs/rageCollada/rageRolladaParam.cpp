// #include <maya/MTransformationMatrix.h>
// #include <maya/MVector.h>
// #include <maya/MQuaternion.h>
// #include <maya/MAngle.h>
#include "parser/manager.h"
#include "parsercore/attribute.h"
#include "rageRolladaParam.h"

using namespace rage;

#ifdef MEMORY_LEAK_DETECTION
int rageRolladaParam::m_NoOfCurrentlyConstructedInstances = 0;
#endif


rageRolladaParam::rageRolladaParam(void)
{
#ifdef MEMORY_LEAK_DETECTION
	m_NoOfCurrentlyConstructedInstances++;
	Displayf("Created an instance of a rageRolladaParam (%p), currently %d active instances\n", this, m_NoOfCurrentlyConstructedInstances);
#endif
}

rageRolladaParam::~rageRolladaParam(void)
{
#ifdef MEMORY_LEAK_DETECTION
	m_NoOfCurrentlyConstructedInstances--;
	Displayf("Deleted an instance of a rageRolladaParam (%p), currently %d active instances\n", this, m_NoOfCurrentlyConstructedInstances);
#endif
}

void rageRolladaParam::CreateFromRolladaNode(const parTreeNode* pobRolladaParamNode)
{
	// No good if I've been given junk
	Assertf(pobRolladaParamNode, "Unable to CreateFromRolladaNode from NULL pointer!");

	// Get name
	const parAttribute* pobNameAttribute = (const_cast<parTreeNode*>(pobRolladaParamNode))->GetElement().FindAttribute("name");
	Assertf(pobNameAttribute, "Unable to find name attribute for RolladaNode");
	m_strName = (const_cast<parAttribute*>(pobNameAttribute))->GetStringValue();
	
	// Get type
	const parAttribute* pobTypeAttribute = (const_cast<parTreeNode*>(pobRolladaParamNode))->GetElement().FindAttribute("type");
	Assertf(pobTypeAttribute, "Unable to find type attribute for RolladaNode");
	m_strType = (const_cast<parAttribute*>(pobTypeAttribute))->GetStringValue();
	
	// Get value
	m_strValue = "";
	if(pobRolladaParamNode->GetData())
	{
		m_strValue = pobRolladaParamNode->GetData();
	}
}

