#pragma once

namespace rage 
{

	class rageColladaVector
	{
	public:
		inline rageColladaVector(void);
		inline rageColladaVector(const float fX, const float fY, const float fZ);
	public:
		inline ~rageColladaVector(void);

		float GetX() const	{	return m_fX;}
		void  SetX(float fX) { m_fX = fX;}
		float GetY() const	{	return m_fY;}
		void  SetY(float fY) { m_fY = fY;}
		float GetZ() const	{	return m_fZ;}
		void  SetZ(float fZ) { m_fZ = fZ;}


	private:
		float	m_fX;
		float	m_fY;
		float	m_fZ;
	};



	// ********************************************************************
	// Inlines
	// ********************************************************************

	inline rageColladaVector::rageColladaVector(void):
	m_fX(0.0f),
		m_fY(0.0f),
		m_fZ(0.0f)
	{
	}

	inline rageColladaVector::rageColladaVector(const float fX, const float fY, const float fZ):
	m_fX(fX),
		m_fY(fY),
		m_fZ(fZ)
	{
	}

	inline rageColladaVector::~rageColladaVector(void)
	{
	}

}
