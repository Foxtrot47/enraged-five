#pragma once
#pragma warning(push)
#pragma warning(disable : 4263)
#pragma warning(disable : 4100)
#include <dae.h>
#include <1.4/dom/domCOLLADA.h>
#pragma warning(pop)

#include "rageCRolladaMaterial.h"

#include <string>
using namespace std;

namespace rage 
{


	class rageColladaMaterial : public rageCRolladaMaterial
	{
	public:
		rageColladaMaterial(void);
		virtual ~rageColladaMaterial(void);

		// Operators
		void CreateFromColladaNode(domMaterial* pobMaterial);

		virtual void OpenGLDraw() const;

	private:

		const domElement* FindFirstNodeOfType(const domElement* pobParentNode, const char* pcTypeOfNode) const;

	#ifdef MEMORY_LEAK_DETECTION
		static int	m_NoOfCurrentlyConstructedInstances;
	#endif

		// Members below here are only used for openGL drawing of materials
		mutable unsigned int	m_OpenGLTextureName;
		mutable char* m_TextureData;
		mutable int m_iTextureWidth;
		mutable int m_iTextureHeight;
		mutable int m_iOpenGLTextureFormat;
		mutable bool m_bAlreadyLoadedTexture;

		string m_strFallBackTexturePath;
	};

}
