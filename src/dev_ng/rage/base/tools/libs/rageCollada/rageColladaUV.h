#pragma once

namespace rage 
{

	class rageColladaUV
	{
	public:
		inline rageColladaUV(void);
		inline rageColladaUV(const float fU, const float fV);
	public:
		inline ~rageColladaUV(void);

		float GetU() const	{	return m_fU;}
		void  SetU(float fU) { m_fU = fU;}
		float GetV() const	{	return m_fV;}
		void  SetV(float fV) { m_fV = fV;}

	private:
		float	m_fU;
		float	m_fV;
	};



	// ********************************************************************
	// Inlines
	// ********************************************************************

	inline rageColladaUV::rageColladaUV(void) :
	m_fU(0.0f),
		m_fV(0.0f)
	{
	}

	inline rageColladaUV::rageColladaUV(const float fU, const float fV) :
	m_fU(fU),
		m_fV(fV)
	{
	}

	inline rageColladaUV::~rageColladaUV(void)
	{
	}

}
