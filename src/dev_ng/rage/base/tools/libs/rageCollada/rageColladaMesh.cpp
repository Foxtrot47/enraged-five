#include "rageColladaMesh.h"
// #include <maya/MColor.h>
// #include <maya/MVector.h>
#include <math.h>

using namespace rage;

#ifdef MEMORY_LEAK_DETECTION
int rageColladaMesh::m_NoOfCurrentlyConstructedInstances = 0;
#endif

rageColladaMesh::rageColladaMesh(void)
{
#ifdef MEMORY_LEAK_DETECTION
	m_NoOfCurrentlyConstructedInstances++;
	Displayf("Created an instance of a rageColladaMesh (%p), currently %d active instances\n", this, m_NoOfCurrentlyConstructedInstances);
#endif
}

rageColladaMesh::~rageColladaMesh(void)
{
	for(unsigned i=0; i<m_AObArrayOfSubMeshes.size(); i++)
	{
		delete m_AObArrayOfSubMeshes[i];
	}
#ifdef MEMORY_LEAK_DETECTION
	m_NoOfCurrentlyConstructedInstances--;
	Displayf("Deleted an instance of a rageColladaMesh (%p), currently %d active instances\n", this, m_NoOfCurrentlyConstructedInstances);
#endif
}

void rageColladaMesh::CreateFromColladaNode(domMesh* pobMeshElement, const rageRolladaScene* pobScene)
{
	//***********************************************************
	// Get name
	//***********************************************************
	m_strName = ((domGeometry*)(pobMeshElement->getXMLParentElement()))->getId();

	//***********************************************************
	// Get polys
	//***********************************************************
	// Sometimes written out as polygons...
	for(unsigned int iPolygonsNo = 0; iPolygonsNo<pobMeshElement->getPolygons_array().getCount(); iPolygonsNo++)
	{
		// Get polygons
		domPolygons* pobPolygons = pobMeshElement->getPolygons_array()[iPolygonsNo];

		// Create Rage SubMesh
		rageColladaSubMesh* pobLocatorSubMesh = new rageColladaSubMesh();
		pobLocatorSubMesh->CreateFromColladaNode(pobPolygons, pobScene);
		m_AObArrayOfSubMeshes.push_back(pobLocatorSubMesh);
	}

	// ...and sometimes written out as polylists
	for(unsigned int iPolygonsNo = 0; iPolygonsNo<pobMeshElement->getPolylist_array().getCount(); iPolygonsNo++)
	{
		// Get polygons
		domPolylist* pobPolygons = pobMeshElement->getPolylist_array()[iPolygonsNo];

		// Create Rage SubMesh
		rageColladaSubMesh* pobLocatorSubMesh = new rageColladaSubMesh();
		pobLocatorSubMesh->CreateFromColladaNode(pobPolygons, pobScene);
		m_AObArrayOfSubMeshes.push_back(pobLocatorSubMesh);
	}
}


void rageColladaMesh::OpenGLDraw() const
{
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glMultMatrixd(m_adMatrixInOpenGLFormat);

	// Go through submeshes, drawing as I go
	vector<rageColladaSubMesh*>::const_iterator it = m_AObArrayOfSubMeshes.begin();
	for(; it != m_AObArrayOfSubMeshes.end(); it++)
	{
		(*it)->OpenGLDraw();
	}

	// Clean up
	glPopMatrix();
}

void rageColladaMesh::SetMatrix(const rageColladaMatrix& obMatrix)
{
	m_obMatrix = obMatrix;
	for(int i=0; i<4; i++)
	{
		for(int j=0; j<4; j++)
		{
			m_adMatrixInOpenGLFormat[(j*4) + i] = obMatrix((i*4) + j);
		}
	}
}


rageColladaBoundingBox	rageColladaMesh::CalcBoundingBox() const
{
	float fMinX = 99999999.9f;
	float fMinY = 99999999.9f;
	float fMinZ = 99999999.9f;
	float fMaxX =-99999999.9f;
	float fMaxY =-99999999.9f;
	float fMaxZ =-99999999.9f;

	// Go through submeshes, looking at bounding boxes as I go
	vector<rageColladaSubMesh*>::const_iterator it = m_AObArrayOfSubMeshes.begin();
	for(; it != m_AObArrayOfSubMeshes.end(); it++)
	{
		rageColladaBoundingBox obMeshBB = (*it)->CalcBoundingBox();

		if(fMinX > obMeshBB.GetMin().GetX()) fMinX = obMeshBB.GetMin().GetX();
		if(fMinY > obMeshBB.GetMin().GetY()) fMinY = obMeshBB.GetMin().GetY();
		if(fMinZ > obMeshBB.GetMin().GetZ()) fMinZ = obMeshBB.GetMin().GetZ();

		if(fMaxX < obMeshBB.GetMax().GetX()) fMaxX = obMeshBB.GetMax().GetX();
		if(fMaxY < obMeshBB.GetMax().GetY()) fMaxY = obMeshBB.GetMax().GetY();
		if(fMaxZ < obMeshBB.GetMax().GetZ()) fMaxZ = obMeshBB.GetMax().GetZ();
	}

	rageColladaBoundingBox obReturnMe;
	obReturnMe.SetMin(rageColladaPoint(fMinX, fMinY, fMinZ));
	obReturnMe.SetMax(rageColladaPoint(fMaxX, fMaxY, fMaxZ));
	return obReturnMe;
}
