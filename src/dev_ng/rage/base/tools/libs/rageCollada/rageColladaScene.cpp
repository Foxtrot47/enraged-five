// #include <maya/MTransformationMatrix.h>
// #include <maya/MVector.h>
// #include <maya/MQuaternion.h>
// #include <maya/MAngle.h>
#include "rageColladaScene.h"

using namespace rage;

#ifdef MEMORY_LEAK_DETECTION
int rageColladaScene::m_NoOfCurrentlyConstructedInstances = 0;
#endif


rageColladaScene::rageColladaScene(void)
{
#ifdef MEMORY_LEAK_DETECTION
	m_NoOfCurrentlyConstructedInstances++;
	Displayf("Created an instance of a rageColladaScene (%p), currently %d active instances\n", this, m_NoOfCurrentlyConstructedInstances);
#endif
}

rageColladaScene::~rageColladaScene(void)
{
	for(unsigned i=0; i<m_AObArrayOfMeshes.size(); i++)
	{
		delete m_AObArrayOfMeshes[i];
	}
	for(unsigned i=0; i<m_AObArrayOfMaterials.size(); i++)
	{
		delete m_AObArrayOfMaterials[i];
	}
	
#ifdef MEMORY_LEAK_DETECTION
	m_NoOfCurrentlyConstructedInstances--;
	Displayf("Deleted an instance of a rageColladaScene (%p), currently %d active instances\n", this, m_NoOfCurrentlyConstructedInstances);
#endif
}

void rageColladaScene::ProcessSceneNode(domNode* pobNode, const rageColladaMatrix& obParentMatrix, const rageRolladaScene* pobScene)
{
	// Displayf("rageColladaScene::ProcessSceneNode %s", (const char*)pobNode->getID());
	// Update matrix for this node
	rageColladaMatrix obCurrentMatrix = obParentMatrix.AsRageMatrix44();

	// Does the node have a matrix?  If so, just use it
	if(pobNode->getMatrix_array().getCount() > 0)
	{
		domFloat4x4& obColladaMatrix = pobNode->getMatrix_array()[0]->getValue();
		rageColladaMatrix obLocalMatrix(obColladaMatrix);
		Matrix44 obCurrentMatrixAsRage44 = obCurrentMatrix.AsRageMatrix44();
		obCurrentMatrixAsRage44.Dot(obLocalMatrix.AsRageMatrix44());
		obCurrentMatrix = rageColladaMatrix(obCurrentMatrixAsRage44);
	}
	else if(pobNode->getContents().getCount() > 0)
	{
		// Ok, there is no baked matrix but there is individual opperations, 
		// so I need to construct a matrix out of its parts
		for(int iOperationNo=0; iOperationNo<(int)pobNode->getContents().getCount(); iOperationNo++)
		{
			daeElement* pobOperationElement = pobNode->getContents()[iOperationNo];
			obCurrentMatrix = obCurrentMatrix.ApplyColladaMatrixOperation(pobOperationElement);
		}
	}

	// Instanced stuff
	const domInstance_node_Array& obNodeArray = pobNode->getInstance_node_array();
	for(int intstanceNo=0; intstanceNo<(int)obNodeArray.getCount(); intstanceNo++)
	{
		// Get the node
		domInstance_node* pobInstance = obNodeArray[intstanceNo];
		xsAnyURI obURIForInstance = pobInstance->getUrl();
		domNode* pobInstanceNode = (domNode*)(domElement*)obURIForInstance.getElement();

		// Recurse
		ProcessSceneNode(pobInstanceNode, obCurrentMatrix, pobScene);
	}

	// obCurrentMatrix.Print();
	// Non-skinned stuff
	const domInstance_geometry_Array& obGeometryArray = pobNode->getInstance_geometry_array();
	for(int intstanceNo=0; intstanceNo<(int)obGeometryArray.getCount(); intstanceNo++)
	{
		// Get the geometry
		domInstance_geometry* pobInstance = obGeometryArray[intstanceNo];
		xsAnyURI obURIForInstance = pobInstance->getUrl();
		domGeometry* pobGeometry = (domGeometry*)(domElement*)obURIForInstance.getElement();

		// Get Mesh
		domMesh* pobMesh = pobGeometry->getMesh();

		// Create Rage Mesh
		rageColladaMesh* pobLocatorMesh = new rageColladaMesh();
		pobLocatorMesh->CreateFromColladaNode(pobMesh, pobScene);
		//Matrix44 obScaleMatrix(obCurrentMatrix.AsRageMatrix44());
		//obScaleMatrix.Dot(obGlobalColladaToRageMatrix.AsRageMatrix44());
		//pobLocatorMesh->SetMatrix(obScaleMatrix);
		pobLocatorMesh->SetMatrix(obCurrentMatrix);
		m_AObArrayOfMeshes.push_back(pobLocatorMesh);

		// Ok, the geometry has its own converter, so I should be able to use that to get a rexObjectGenericMesh version of the mesh
		//daeIntegrationObject* pobIntegrationObj = pobMesh->getIntObject();
		//intMesh* pobIntMesh =(intMesh *)pobIntegrationObj;
		//rexObjectGenericMesh* pobRexGenericMesh = pobIntMesh->getMesh();

		//// Give the rexObjectGenericMesh a better name
		//pobRexGenericMesh->SetMeshName(pobNode->getName());

		//// Give the rexObjectGenericMesh a better matrix
		//// Add it to the scene
		//m_pobRexGenericScene->AddMesh(pobRexGenericMesh, obCurrentMatrix);
	}

	// Skinned stuff
	const domInstance_controller_Array& obControllerArray = pobNode->getInstance_controller_array();
	for(int intstanceNo=0; intstanceNo<(int)obControllerArray.getCount(); intstanceNo++)
	{
		// Get the controller
		domInstance_controller* pobInstance = obControllerArray[intstanceNo];
		xsAnyURI obURIForInstance = pobInstance->getUrl();
		domController* pobController = (domController*)(domElement*)obURIForInstance.getElement();

		// Get the skin
		domSkin* pobSkin = pobController->getSkin();

		// Get the geometry
		xsAnyURI obURIForGeometry = pobSkin->getSource();
		domGeometry* pobGeometry = (domGeometry*)(domElement*)obURIForGeometry.getElement();

		// Get Mesh
		domMesh* pobMesh = pobGeometry->getMesh();

		// Create Rage Mesh
		rageColladaMesh* pobLocatorMesh = new rageColladaMesh();
		pobLocatorMesh->CreateFromColladaNode(pobMesh, pobScene);
		pobLocatorMesh->SetMatrix(obCurrentMatrix);
		m_AObArrayOfMeshes.push_back(pobLocatorMesh);
	}
	
	/*
	// What does this node point to?
	for(int intstanceNo=0; intstanceNo<(int)pobNode->getInstance_array().getCount(); intstanceNo++)
	{
		// Get the instance
		domInstance* pobInstance = pobNode->getInstance_array()[intstanceNo];

		// What does the instance point to?
		xsAnyURI* obURIForInstance = &(pobInstance->getUrl());

		// Get the element that points to
		obURIForInstance->resolveElement();
		domElement* pobElementURIPointsTo = obURIForInstance->getElement();

		// Is it a controller (i.e. skinning information)
		if(!strcmp(pobElementURIPointsTo->getTypeName(), "controller"))
		{
			// Bingo, a controller
			// Ok, ok, I should do something clever here involving skinning, but that sounds
			// hard, so for now, lets just jump to the geometry instead
			// Get the Controller
			domController* pobController = (domController*)pobElementURIPointsTo;

			// Get geometry
			daeIDRef obIDForGeometry = pobController->getTarget();
			obIDForGeometry.resolveElement();
			pobElementURIPointsTo = obIDForGeometry.element;
		}


		// Is it a mesh?
		if(!strcmp(pobElementURIPointsTo->getTypeName(), "geometry"))
		{
			// Bingo, a mesh
			// Get the geometry
			domGeometry* pobGeometry = (domGeometry*)pobElementURIPointsTo;

			// Get Mesh
			domMesh* pobMesh = pobGeometry->getMesh();

			// Create Rage Mesh
			rageColladaMesh* pobLocatorMesh = new rageColladaMesh();
			pobLocatorMesh->CreateFromColladaNode(pobMesh);
			//Matrix44 obScaleMatrix(obCurrentMatrix.AsRageMatrix44());
			//obScaleMatrix.Dot(obGlobalColladaToRageMatrix.AsRageMatrix44());
			//pobLocatorMesh->SetMatrix(obScaleMatrix);
			pobLocatorMesh->SetMatrix(obCurrentMatrix);
			m_AObArrayOfMeshes.push_back(pobLocatorMesh);

			// Ok, the geometry has its own converter, so I should be able to use that to get a rexObjectGenericMesh version of the mesh
			//daeIntegrationObject* pobIntegrationObj = pobMesh->getIntObject();
			//intMesh* pobIntMesh =(intMesh *)pobIntegrationObj;
			//rexObjectGenericMesh* pobRexGenericMesh = pobIntMesh->getMesh();

			//// Give the rexObjectGenericMesh a better name
			//pobRexGenericMesh->SetMeshName(pobNode->getName());

			//// Give the rexObjectGenericMesh a better matrix
			//// Add it to the scene
			//m_pobRexGenericScene->AddMesh(pobRexGenericMesh, obCurrentMatrix);
		}
	}
*/
	// Does the node have any children nodes?
	for(int n=0;n<(int)pobNode->getNode_array().getCount(); n++)
	{
		// Get node
		domNode* pobChildNode = pobNode->getNode_array()[n];

		// Process that node
		ProcessSceneNode(pobChildNode, obCurrentMatrix, pobScene);
	}
}

void rageColladaScene::CreateFromColladaNode(domCOLLADA* pobBaseColladaNode, const rageRolladaScene* pobScene)
{
	domCOLLADA::domScene* pobSceneElement = pobBaseColladaNode->getScene();
	// Get world scale and the UP axis
	float fToMetersScaleFactor = 1.0f;
	domUpAxisType obUpAxis = UPAXISTYPE_Y_UP;

	// Get the element that points to
	// Get asset node
	domAsset* pobAssetNode = pobBaseColladaNode->getAsset();
	if(pobAssetNode)
	{
		// Get unit for scale
		const domAsset::domUnit* pobUnit = pobAssetNode->getUnit();

		if(pobUnit)
		{
			// Get scale
			fToMetersScaleFactor = (float)pobUnit->getMeter();
		}

		// Get up axis
		const domAsset::domUp_axis* pobUpAxis = pobAssetNode->getUp_axis();
		if(pobUpAxis)
		{
			// Get axis
			obUpAxis = pobUpAxis->getValue();
		}
	}

	Displayf("fToMetersScaleFactor = %f\n", fToMetersScaleFactor);
	//if(fToMetersScaleFactor < 0.02)
	//{
	//	fToMetersScaleFactor = 1.0f;
	//}
	// Create global scale matrix
	Matrix44 obScaleMatrix = M44_IDENTITY;
	obScaleMatrix.MakeScaleFull(fToMetersScaleFactor * 100.0f);

	// Next get the up axis
	Matrix44 obRotationMatrix;
	switch(obUpAxis)
	{
		case UPAXISTYPE_X_UP:
		{
			// X up, so rotate everything 90degrees around Z
			obRotationMatrix.MakeRotZ(0.5f * PI);
			break;
		}
		case UPAXISTYPE_Y_UP:
		{
			// Default for Rage, so do nothing
			obRotationMatrix = M44_IDENTITY;
			break;
		}
		case UPAXISTYPE_Z_UP:
		{
			// Z up, so rotate everything 90degrees around X
			obRotationMatrix.MakeRotX(-0.5f * PI);
			break;
		}
		case UPAXISTYPE_COUNT:
			{
				break;
			}
	}

	// Combine to get ColladaToRage matrix
	m_obColladaFileToRageMatrix = obRotationMatrix.Dot(obScaleMatrix);
	// m_obColladaFileToRageMatrix = M44_IDENTITY;

	// Generate OpenGL version of the matrix
	for(int i=0; i<4; i++)
	{
		for(int j=0; j<4; j++)
		{
			m_adColladaFileToRageMatrixInOpenGLFormat[(j*4) + i] = m_obColladaFileToRageMatrix((i*4) + j);
		}
	}

	// Next get the list of materials
	domLibrary_materials_Array& obMaterialLibraryArray = pobBaseColladaNode->getLibrary_materials_array();
	for(unsigned i=0; i<obMaterialLibraryArray.getCount(); i++)
	{
		const domLibrary_materialsRef& pobMaterialLibrary = obMaterialLibraryArray[i];
		const domMaterial_Array& pobMaterialArray = pobMaterialLibrary->getMaterial_array();
		for(unsigned m=0; m<pobMaterialArray.getCount(); m++)
		{
			const domMaterialRef& pobMaterial = pobMaterialArray[m];

			// Create Rage Material
			rageColladaMaterial* pobMaterialMaterial = new rageColladaMaterial();
			pobMaterialMaterial->CreateFromColladaNode(pobMaterial);

			// Add to array
			m_AObArrayOfMaterials.push_back(pobMaterialMaterial);
		}
	}

	// Next get the bounding box
	//if(pobSceneElement->getBoundingbox_array().getCount() > 0)
	//{
	//	m_ObBoundingBoxAsItIsInColladaFile.CreateFromColladaNode(pobSceneElement->getBoundingbox_array()[0]);

	//	// From that, generate an adjusted version
	//	Vector3 obMin(m_ObBoundingBoxAsItIsInColladaFile.GetMin().GetX(), m_ObBoundingBoxAsItIsInColladaFile.GetMin().GetY(), m_ObBoundingBoxAsItIsInColladaFile.GetMin().GetZ());
	//	obGlobalColladaToRageMatrix.AsRageMatrix34().Transform(obMin);
	//	m_ObWorldSpaceBoundingBox.SetMin(rageColladaPoint(obMin.x, obMin.y, obMin.z));

	//	Vector3 obMax(m_ObBoundingBoxAsItIsInColladaFile.GetMax().GetX(), m_ObBoundingBoxAsItIsInColladaFile.GetMax().GetY(), m_ObBoundingBoxAsItIsInColladaFile.GetMax().GetZ());
	//	obGlobalColladaToRageMatrix.AsRageMatrix34().Transform(obMax);
	//	m_ObWorldSpaceBoundingBox.SetMax(rageColladaPoint(obMax.x, obMax.y, obMax.z));
	//}

	// A scene has nodes, so go through them
	domInstanceWithExtra* pobInstanceVisualScene = pobSceneElement->getInstance_visual_scene();
	xsAnyURI& pobVisualSceneUrl = pobInstanceVisualScene->getUrl();
	const domVisual_scene* pobVisualScene = (const domVisual_scene*)(const domElement*)pobVisualSceneUrl.getElement();
	const domNode_Array& obSceneNodes = pobVisualScene->getNode_array();
	for(int n=0;n<(int)obSceneNodes.getCount(); n++)
	{
		// Get node
		domNode* pobNode = obSceneNodes[n];

		// Process that node
		ProcessSceneNode(pobNode, rageColladaMatrix(), pobScene);
	}
}

void rageColladaScene::OpenGLDraw() const
{
	// Draw bounding box
	//glColor3f(1.0f, 0.0f, 0.0f);
	//glPointSize( 5.0f );
	//glBegin(GL_POINTS);
	//	glVertex3f( (float)m_ObWorldSpaceBoundingBox.GetMin().GetX(), (float)m_ObWorldSpaceBoundingBox.GetMin().GetY(), (float)m_ObWorldSpaceBoundingBox.GetMin().GetZ());
	//	glVertex3f( (float)m_ObWorldSpaceBoundingBox.GetMax().GetX(), (float)m_ObWorldSpaceBoundingBox.GetMax().GetY(), (float)m_ObWorldSpaceBoundingBox.GetMax().GetZ());
	//glEnd();

	//glColor3f(0.0f, 1.0f, 0.0f);
	//glPointSize( 5.0f );
	//glBegin(GL_POINTS);
	//	glVertex3f( (float)m_ObBoundingBoxAsItIsInColladaFile.GetMin().GetX(), (float)m_ObBoundingBoxAsItIsInColladaFile.GetMin().GetY(), (float)m_ObBoundingBoxAsItIsInColladaFile.GetMin().GetZ());
	//	glVertex3f( (float)m_ObBoundingBoxAsItIsInColladaFile.GetMax().GetX(), (float)m_ObBoundingBoxAsItIsInColladaFile.GetMax().GetY(), (float)m_ObBoundingBoxAsItIsInColladaFile.GetMax().GetZ());
	//glEnd();

	glPushMatrix();
	glMultMatrixd(m_adColladaFileToRageMatrixInOpenGLFormat);
	// Go through meshes, drawing as I go
	vector<rageColladaMesh*>::const_iterator it = m_AObArrayOfMeshes.begin();
	for(; it != m_AObArrayOfMeshes.end(); it++)
	{
		(*it)->OpenGLDraw();
	}

	// Clean up
	glPopMatrix();
}

rageColladaBoundingBox	rageColladaScene::CalcBoundingBox() const
{
	float fMinX = 99999999.9f;
	float fMinY = 99999999.9f;
	float fMinZ = 99999999.9f;
	float fMaxX =-99999999.9f;
	float fMaxY =-99999999.9f;
	float fMaxZ =-99999999.9f;
	Vector3 obTransformedMin;
	Vector3 obTransformedMax;

	// Go through meshes, looking at bounding boxes as I go
	vector<rageColladaMesh*>::const_iterator it = m_AObArrayOfMeshes.begin();
	for(; it != m_AObArrayOfMeshes.end(); it++)
	{
		rageColladaBoundingBox obMeshBB = (*it)->CalcBoundingBox();

		// Apply the matrix of the mesh to the BB
		Matrix44 obMeshMatrix = (*it)->GetMatrix().AsRageMatrix44();
		obTransformedMin = obMeshMatrix.FullTransform(Vector3(obMeshBB.GetMin().GetX(), obMeshBB.GetMin().GetY(), obMeshBB.GetMin().GetZ()));
		obTransformedMax = obMeshMatrix.FullTransform(Vector3(obMeshBB.GetMax().GetX(), obMeshBB.GetMax().GetY(), obMeshBB.GetMax().GetZ()));

		if(fMinX > obTransformedMin.x) fMinX = obTransformedMin.x;
		if(fMinY > obTransformedMin.y) fMinY = obTransformedMin.y;
		if(fMinZ > obTransformedMin.z) fMinZ = obTransformedMin.z;

		if(fMaxX < obTransformedMax.x) fMaxX = obTransformedMax.x;
		if(fMaxY < obTransformedMax.y) fMaxY = obTransformedMax.y;
		if(fMaxZ < obTransformedMax.z) fMaxZ = obTransformedMax.z;
	}

	rageColladaBoundingBox obReturnMe;
	obReturnMe.SetMin(rageColladaPoint(fMinX, fMinY, fMinZ));
	obReturnMe.SetMax(rageColladaPoint(fMaxX, fMaxY, fMaxZ));
	return obReturnMe;
}

const rageColladaMaterial* rageColladaScene::GetMaterial(const string strMaterialName) const
{
	// Go through materials, looking for the right one
	vector<rageColladaMaterial*>::const_iterator it = m_AObArrayOfMaterials.begin();
	for(; it != m_AObArrayOfMaterials.end(); it++)
	{
		if((*it)->GetName() == strMaterialName)
		{
			return *it;
		}
	}
	return NULL;
}
