// #include <maya/MTransformationMatrix.h>
// #include <maya/MVector.h>
// #include <maya/MQuaternion.h>
// #include <maya/MAngle.h>
#include "parser/manager.h"
#include "parsercore/attribute.h"
#include "rageRolladaMaterialParam.h"

using namespace rage;

#ifdef MEMORY_LEAK_DETECTION
int rageRolladaMaterialParam::m_NoOfCurrentlyConstructedInstances = 0;
#endif


rageRolladaMaterialParam::rageRolladaMaterialParam(void)
{
#ifdef MEMORY_LEAK_DETECTION
	m_NoOfCurrentlyConstructedInstances++;
	Displayf("Created an instance of a rageRolladaMaterialParam (%p), currently %d active instances\n", this, m_NoOfCurrentlyConstructedInstances);
#endif
}

rageRolladaMaterialParam::~rageRolladaMaterialParam(void)
{
#ifdef MEMORY_LEAK_DETECTION
	m_NoOfCurrentlyConstructedInstances--;
	Displayf("Deleted an instance of a rageRolladaMaterialParam (%p), currently %d active instances\n", this, m_NoOfCurrentlyConstructedInstances);
#endif
}

void rageRolladaMaterialParam::CreateFromRolladaNode(const parTreeNode* pobRolladaMaterialParamNode)
{
	// No good if I've been given junk
	Assertf(pobRolladaMaterialParamNode, "Unable to CreateFromRolladaNode from NULL pointer!");

	// Get name
	const parAttribute* pobNameAttribute = (const_cast<parTreeNode*>(pobRolladaMaterialParamNode))->GetElement().FindAttribute("name");
	Assertf(pobNameAttribute, "Unable to find name attribute for RolladaNode");
	m_strName = (const_cast<parAttribute*>(pobNameAttribute))->GetStringValue();
	
	// Get type
	const parAttribute* pobTypeAttribute = (const_cast<parTreeNode*>(pobRolladaMaterialParamNode))->GetElement().FindAttribute("type");
	Assertf(pobTypeAttribute, "Unable to find type attribute for RolladaNode");
	m_strType = (const_cast<parAttribute*>(pobTypeAttribute))->GetStringValue();
	
	// Get value
	m_strValue = pobRolladaMaterialParamNode->GetData();
}

