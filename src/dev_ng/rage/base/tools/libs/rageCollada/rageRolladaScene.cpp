// #include <maya/MTransformationMatrix.h>
// #include <maya/MVector.h>
// #include <maya/MQuaternion.h>
// #include <maya/MAngle.h>
// #include "rexBase/rexTimerLog.h"
#include "parser/manager.h"
#include "rageRolladaScene.h"

using namespace rage;

#ifdef MEMORY_LEAK_DETECTION
int rageRolladaScene::m_NoOfCurrentlyConstructedInstances = 0;
#endif


rageRolladaScene::rageRolladaScene(void)
{
	m_pobRageColladaScene = NULL;
#ifdef MEMORY_LEAK_DETECTION
	m_NoOfCurrentlyConstructedInstances++;
	Displayf("Created an instance of a rageRolladaScene (%p), currently %d active instances\n", this, m_NoOfCurrentlyConstructedInstances);
#endif
}

rageRolladaScene::~rageRolladaScene(void)
{
	for(unsigned i=0; i<m_AObArrayOfNodes.size(); i++)
	{
		delete m_AObArrayOfNodes[i];
	}

	for(unsigned i=0; i<m_AObArrayOfMaterials.size(); i++)
	{
		delete m_AObArrayOfMaterials[i];
	}

	delete m_pobRageColladaScene;
#ifdef MEMORY_LEAK_DETECTION
	m_NoOfCurrentlyConstructedInstances--;
	Displayf("Deleted an instance of a rageRolladaScene (%p), currently %d active instances\n", this, m_NoOfCurrentlyConstructedInstances);
#endif
}

void rageRolladaScene::CreateFromRolladaFile(const char* pcPathAndFilename)
{
	Displayf("Creating Rollada Scene out of %s", pcPathAndFilename);

	// Open file
	INIT_PARSER;
	parTree* pobSceneXMLTree = PARSER.LoadTree(pcPathAndFilename, "");
	parTreeNode* pobSceneRootNode = pobSceneXMLTree->GetRoot();

	// **********************************************************
	// Do the Rollada stuff
	// **********************************************************
	parTreeNode* pobNodesDataNode = pobSceneRootNode->FindChildWithName("nodes");
	// There is nothing to say you HAVE to have nodes node
	if(pobNodesDataNode)
	{
		// Go through all the nodes under the nodes node (Confused?)
		parTreeNode* pobRolladaNodeNode = pobNodesDataNode->FindChildWithName("node");
		while(pobRolladaNodeNode)
		{
			// Get info from node
			rageRolladaNode* pobRolladaNode = new rageRolladaNode();
			pobRolladaNode->CreateFromRolladaNode(pobRolladaNodeNode);
			m_AObArrayOfNodes.push_back(pobRolladaNode);

			// Move onto the next one
			pobRolladaNodeNode = pobNodesDataNode->FindChildWithName("node", pobRolladaNodeNode);
		}
	}

	parTreeNode* pobShadersDataShader = pobSceneRootNode->FindChildWithName("shaders");
	// There is nothing to say you HAVE to have shaders node
	if(pobShadersDataShader)
	{
		// Go through all the shaders under the shaders node
		parTreeNode* pobRolladaShaderNode = pobShadersDataShader->FindChildWithName("shader");
		while(pobRolladaShaderNode)
		{
			// Get info from shader
			rageRolladaMaterial* pobRolladaShader = new rageRolladaMaterial();
			pobRolladaShader->CreateFromRolladaNode(pobRolladaShaderNode);
			m_AObArrayOfMaterials.push_back(pobRolladaShader);

			// Move onto the next one
			pobRolladaShaderNode = pobShadersDataShader->FindChildWithName("shader", pobRolladaShaderNode);
		}
	}

	// **********************************************************
	// Do the Collada stuff
	// **********************************************************
	// Parser asset tag and get the important stuff out
	parTreeNode* pobAssetDataNode = pobSceneRootNode->FindChildWithName("asset");
	Assertf(pobAssetDataNode, "Unable to file collada file node for %s", pcPathAndFilename);
	parTreeNode* pobColladaDataNode = pobAssetDataNode->FindChildWithName("collada_data");
	Assertf(pobColladaDataNode, "Unable to file collada file node for %s", pcPathAndFilename);
	const char* pcColladaFileURI = pobColladaDataNode->GetData();
	Assertf(pcColladaFileURI, "Unable to file collada file URI for %s", pcPathAndFilename);
	// Displayf("pcColladaFileURI = %s\n", pcColladaFileURI);

	// Do it
	CreateFromColladaURI(pcColladaFileURI);

	// Clean up
	pobSceneRootNode = NULL;
	delete pobSceneXMLTree;
}

void rageRolladaScene::CreateFromColladaFile(const char* pcPathAndFilename)
{
	// Displayf("Creating Collada Scene out of %s", pcPathAndFilename);

	// Convert the path and filename into a URI
	// Make sure all my slashes are standard
	int iFilenameLength = (int)strlen(pcPathAndFilename);
	char acFilename[255];
#pragma warning(push)
#pragma warning(disable : 4996)
	strcpy(acFilename, pcPathAndFilename);
#pragma warning(pop)
	for(int i=0; i<iFilenameLength; i++) 
	{
		if(acFilename[i] == '\\')
		{
			acFilename[i] = '/';
		}
	}

	char acFileURI[255];
#pragma warning(push)
#pragma warning(disable : 4996)
	strcpy(acFileURI, "file:///");
	strcat(acFileURI, acFilename);

	// Make sure it is all lower case, otherwise the COLLADA DOM goes crazy
	strlwr(acFileURI);
#pragma warning(pop)

	// Now pass it on to...
	CreateFromColladaURI(acFileURI);
}

void rageRolladaScene::CreateFromColladaURI(const char* pcColladaFileURI)
{
	// **********************************************************
	// Do the Collada stuff
	// **********************************************************
	DAE* pobColladaAPI = new DAE;
//	strlwr(pcColladaFileURI);

	// Tell the user what is happening
	Displayf("Opening %s", pcColladaFileURI);

	//in case there are no argument the main should add a sample file to the arguments
//	rexTimerLog::StartTimer("Loading Collada File");
//	Displayf("collada->load(\"%s\")\n", pcColladaFileURI);
	int res = pobColladaAPI->load(pcColladaFileURI);
	if (res != DAE_OK)
	{
		Assertf(false, "Runtime database failed to initialize from %s : Error %d (%s)\n\n",pcColladaFileURI, res, daeErrorString(res));
		return;
	}
	else
	{
		Displayf("Runtime database initialized from %s.",pcColladaFileURI);
	}
//	rexTimerLog::EndTimer("Loading Collada File");

	// Get scene and convert to Rage
#if __ASSERT
	unsigned int iNumberOfScenes = pobColladaAPI->getDatabase()->getElementCount(NULL,"scene");
	Assertf(iNumberOfScenes > 0, "No scene node found in %s",pcColladaFileURI);
	Assertf(iNumberOfScenes < 2, "More than one scene node (%d) found in %s",iNumberOfScenes,pcColladaFileURI);
#endif

	// Get the scene node
	daeElement *pAsset = NULL;
	res = pobColladaAPI->getDatabase()->getElement(&pAsset,0,NULL,"COLLADA");
	if (res == DAE_OK)
	{
		m_pobRageColladaScene = new rageColladaScene();
//		rexTimerLog::StartTimer("Collada To RageCollada");
		m_pobRageColladaScene->CreateFromColladaNode((domCOLLADA*)pAsset, this);
//		rexTimerLog::EndTimer("Collada To RageCollada");
	}

	// Clean up the collada
	delete pobColladaAPI;
}

void rageRolladaScene::OpenGLDraw() const
{
	if(m_pobRageColladaScene)
	{
		m_pobRageColladaScene->OpenGLDraw();
	}
}

const rageRolladaNode*	rageRolladaScene::GetRolladaNode(const char* pcNodeName) const
{
	vector<rageRolladaNode*>::const_iterator it = GetArrayOfNodes().begin();
	for(; it != GetArrayOfNodes().end(); it++)
	{
		const rageRolladaNode* pobRolladaNode = (*it);
		if(!strcmp(pobRolladaNode->GetName().c_str(), pcNodeName))
		{
			// Bingo
			return pobRolladaNode;
		}
	}
	return NULL;
}


rageColladaBoundingBox	rageRolladaScene::CalcBoundingBox() const
{
	Vector3 obTransformedMin(0.0f,0.0f,0.0f);
	Vector3 obTransformedMax(0.0f,0.0f,0.0f);
	if(GetColladaScene())
	{
		rageColladaBoundingBox obMeshBB = GetColladaScene()->CalcBoundingBox();

		// Apply the matrix of the mesh to the BB
		Matrix44 obMeshMatrix = GetColladaScene()->GetMatrix().AsRageMatrix44();
		obTransformedMin = obMeshMatrix.FullTransform(Vector3(obMeshBB.GetMin().GetX(), obMeshBB.GetMin().GetY(), obMeshBB.GetMin().GetZ()));
		obTransformedMax = obMeshMatrix.FullTransform(Vector3(obMeshBB.GetMax().GetX(), obMeshBB.GetMax().GetY(), obMeshBB.GetMax().GetZ()));
	}

	rageColladaBoundingBox obReturnMe;
	obReturnMe.SetMin(rageColladaPoint(obTransformedMin.x,obTransformedMin.y,obTransformedMin.z));
	obReturnMe.SetMax(rageColladaPoint(obTransformedMax.x,obTransformedMax.y,obTransformedMax.z));
	return obReturnMe;
}

void rageRolladaScene::CreateFromFile(const char* pcPathAndFilename)
{
	// Remove the file extension
	char acPathAndFilename[255]; for(int i=0; i<255; i++) acPathAndFilename[i] = '\0';
	const char* pcExtensionPosition = strrchr(pcPathAndFilename, '.');
	if(pcExtensionPosition)
	{
		strncpy(acPathAndFilename, pcPathAndFilename, (pcExtensionPosition - pcPathAndFilename));
	}
	else
	{
		strcpy(acPathAndFilename, pcPathAndFilename);
	}

	//Displayf("acPathAndFilename = %s", acPathAndFilename);

	// Is there a rollada file version, if so use it
	WIN32_FIND_DATA DirData;
	string strSearchString = (string(acPathAndFilename) + ".rollada");
	// Displayf("strSearchString = %s", strSearchString.c_str());
	HANDLE DirFile = FindFirstFile(strSearchString.c_str(), &DirData);
	if(DirFile != INVALID_HANDLE_VALUE)
	{
		// Bingo!  Found a rollada file
		return CreateFromRolladaFile(strSearchString.c_str());
	}
	else
	{
		// No rollada file, so load the collada version
		string strSearchString = (string(acPathAndFilename) + ".dae");
		return CreateFromColladaFile(strSearchString.c_str());
	}
}

const rageRolladaMaterial* rageRolladaScene::GetMaterial(const string strMaterialName) const
{
	// Go through materials, looking for the right one
	vector<rageRolladaMaterial*>::const_iterator it = m_AObArrayOfMaterials.begin();
	for(; it != m_AObArrayOfMaterials.end(); it++)
	{
		if((*it)->GetName() == strMaterialName)
		{
			return *it;
		}
	}
	return NULL;
}
