#pragma once
// #include "vector/Vector3.h"
// #include <maya/MVector.h>
// #include <maya/MColor.h>
// #include <maya/MIntArray.h>
// #include <maya/MMatrix.h>
#define WIN32_LEAN_AND_MEAN
#pragma warning(push)
#pragma warning(disable: 4668)
#include <windows.h>
#pragma warning(pop)
#include <GL/gl.h>

#pragma warning(push)
#pragma warning(disable : 4263)
#pragma warning(disable : 4100)
#include <dae.h>
#include <1.4/dom/domCOLLADA.h>
#pragma warning(pop)

#include <vector>
using namespace std;

#include "rageColladaColor.h"
#include "rageColladaPoint.h"
#include "rageColladaVector.h"
#include "rageColladaUV.h"

namespace rage {
	class rageColladaVertices
	{
	public:
		rageColladaVertices(void);
	public:
		virtual ~rageColladaVertices(void);

		// Operators
		void CreateFromColladaNode(domVertices* pobVertices);

		// Accessors
		bool	HasVertexPositions()	const {return	(m_AObArrayOfVertexPositions.size() > 0);}
		bool	HasVertexNormals()		const {return	(m_AObArrayOfVertexNormals.size() > 0);}
		bool	HasVertexColours()		const {return	(m_AObArrayOfVertexColours.size() > 0);}
		bool	HasVertexUVs()			const {return	(m_AObArrayOfVertexUVs.size() > 0);}

		const vector<rageColladaPoint>&		GetArrayOfVertexPositions() const {return		m_AObArrayOfVertexPositions;}
		const vector<rageColladaVector>&	GetArrayOfVertexNormals() const {return		m_AObArrayOfVertexNormals;}
		const vector<rageColladaColor>&		GetArrayOfVertexColours() const {return		m_AObArrayOfVertexColours;}
		const vector<rageColladaUV>&		GetArrayOfVertexUVs() const {return		m_AObArrayOfVertexUVs;}

	private:
		vector<rageColladaPoint>		m_AObArrayOfVertexPositions;
		vector<rageColladaVector>		m_AObArrayOfVertexNormals;
		vector<rageColladaColor>		m_AObArrayOfVertexColours;
		vector<rageColladaUV>			m_AObArrayOfVertexUVs;
	};

}
