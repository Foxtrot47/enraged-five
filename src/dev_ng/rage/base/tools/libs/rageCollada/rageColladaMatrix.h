#pragma once
#include "vector/matrix44.h"
#pragma warning(push)
#pragma warning(disable : 4263)
#pragma warning(disable : 4100)
#include <1.4/dom/domTypes.h>
#pragma warning(pop)

namespace rage 
{

	class rageColladaMatrix
	{
	public:
		inline rageColladaMatrix(void);
		inline rageColladaMatrix(const Matrix34& m_obMatrix);
		inline rageColladaMatrix(const Matrix44& m_obMatrix);
		inline rageColladaMatrix(const domFloat4x4& obColladaMatrix);

		inline Matrix44	AsRageMatrix44() const;
		inline Matrix34	AsRageMatrix34() const;

		float operator() (int i) const
		{
			return m_afMatrix[i];
		}
		float& operator() (int i)
		{
			return m_afMatrix[i];
		}

		float operator() (int i, int j) const
		{
			return m_afMatrix[(i*4) + j];
		}
		float& operator() (int i, int j)
		{
			return m_afMatrix[(i*4) + j];
		}

		// Operators
		rageColladaMatrix ApplyColladaMatrixOperation(daeElement* pobOperationElement) const;

	public:
		~rageColladaMatrix(void);

	private:
		float	m_afMatrix[16];
	};


	// ********************************************************************
	// Inlines
	// ********************************************************************
	inline rageColladaMatrix::rageColladaMatrix(void)
	{
		m_afMatrix[0] = 	1.0f;
		m_afMatrix[1] = 	0.0f;
		m_afMatrix[2] = 	0.0f;
		m_afMatrix[3] = 	0.0f;
		m_afMatrix[4] = 	0.0f;
		m_afMatrix[5] =		1.0f;
		m_afMatrix[6] = 	0.0f;
		m_afMatrix[7] = 	0.0f;
		m_afMatrix[8] = 	0.0f;
		m_afMatrix[9] = 	0.0f;
		m_afMatrix[10] =	1.0f;
		m_afMatrix[11] =	0.0f;
		m_afMatrix[12] =	0.0f;
		m_afMatrix[13] =	0.0f;
		m_afMatrix[14] =	0.0f;
		m_afMatrix[15] =	1.0f;
	}

	inline rageColladaMatrix::rageColladaMatrix(const Matrix34& obMatrix)
	{
		m_afMatrix[0] = obMatrix.GetElement(0,0);
		m_afMatrix[1] = obMatrix.GetElement(1,0);
		m_afMatrix[2] = obMatrix.GetElement(2,0);
		m_afMatrix[3] = obMatrix.GetElement(3,0);
		m_afMatrix[4] = obMatrix.GetElement(0,1);
		m_afMatrix[5] = obMatrix.GetElement(1,1);
		m_afMatrix[6] = obMatrix.GetElement(2,1);
		m_afMatrix[7] = obMatrix.GetElement(3,1);
		m_afMatrix[8] = obMatrix.GetElement(0,2);
		m_afMatrix[9] = obMatrix.GetElement(1,2);
		m_afMatrix[10] = obMatrix.GetElement(2,2);
		m_afMatrix[11] = obMatrix.GetElement(3,2);
		m_afMatrix[12] = 0.0f;
		m_afMatrix[13] = 0.0f;
		m_afMatrix[14] = 0.0f;
		m_afMatrix[15] = 0.0f;
	}


	inline rageColladaMatrix::rageColladaMatrix(const Matrix44& obMatrix)
	{
		m_afMatrix[0] = obMatrix.GetFloat(0);
		m_afMatrix[1] = obMatrix.GetFloat(4);
		m_afMatrix[2] = obMatrix.GetFloat(8);
		m_afMatrix[3] = obMatrix.GetFloat(12);
		m_afMatrix[4] = obMatrix.GetFloat(1);
		m_afMatrix[5] = obMatrix.GetFloat(5);
		m_afMatrix[6] = obMatrix.GetFloat(9);
		m_afMatrix[7] = obMatrix.GetFloat(13);
		m_afMatrix[8] = obMatrix.GetFloat(2);
		m_afMatrix[9] = obMatrix.GetFloat(6);
		m_afMatrix[10] = obMatrix.GetFloat(10);
		m_afMatrix[11] = obMatrix.GetFloat(14);
		m_afMatrix[12] = obMatrix.GetFloat(3);
		m_afMatrix[13] = obMatrix.GetFloat(7);
		m_afMatrix[14] = obMatrix.GetFloat(11);
		m_afMatrix[15] = obMatrix.GetFloat(15);
	}

	inline rageColladaMatrix::rageColladaMatrix(const domFloat4x4& obColladaMatrix)
	{
		for(int i=0; i<16; i++)
		{
			m_afMatrix[i] = (float)obColladaMatrix[i];
		}
	}

	inline rageColladaMatrix::~rageColladaMatrix(void)
	{
	}


	inline Matrix44	rageColladaMatrix::AsRageMatrix44() const
	{
		return Matrix44(	m_afMatrix[0], m_afMatrix[4], m_afMatrix[8], m_afMatrix[12],
			m_afMatrix[1], m_afMatrix[5], m_afMatrix[9], m_afMatrix[13],
			m_afMatrix[2], m_afMatrix[6], m_afMatrix[10], m_afMatrix[14],
			m_afMatrix[3], m_afMatrix[7], m_afMatrix[11], m_afMatrix[15]);
	}


	inline Matrix34	rageColladaMatrix::AsRageMatrix34() const
	{
		return Matrix34(	m_afMatrix[0], m_afMatrix[4], m_afMatrix[8],
			m_afMatrix[1], m_afMatrix[5], m_afMatrix[9],
			m_afMatrix[2], m_afMatrix[6], m_afMatrix[10],
			m_afMatrix[3], m_afMatrix[7], m_afMatrix[11]);
	}
}
