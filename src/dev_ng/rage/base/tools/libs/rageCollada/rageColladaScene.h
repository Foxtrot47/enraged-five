#pragma once

#include "rageColladaMesh.h"
#include "rageColladaMaterial.h"

// #include <maya/MMatrix.h>

#pragma warning(push)
#pragma warning(disable : 4263)
#pragma warning(disable : 4100)
#include <dae.h>
#include <1.4/dom/domCOLLADA.h>
#pragma warning(pop)

#include <vector>
using namespace std;
#include "rageColladaBoundingBox.h"
#include "rageColladaMatrix.h"

namespace rage {
	class rageRolladaScene;

	class rageColladaScene
	{
	public:
		rageColladaScene(void);
		~rageColladaScene(void);

		// Accessors
		const vector<rageColladaMesh*>&	GetArrayOfMeshes() const {return m_AObArrayOfMeshes;}
		const rageColladaMaterial* GetMaterial(const string strMaterialName) const;
		rageColladaBoundingBox	CalcBoundingBox() const;
		const rageColladaMatrix&	GetMatrix() const {return m_obColladaFileToRageMatrix;}

		// Operators
		void CreateFromColladaNode(domCOLLADA* pobCOLLADA, const rageRolladaScene* pobScene);
		void OpenGLDraw() const;

	private:
		const vector<rageColladaMaterial*>&	GetArrayOfMaterials() const {return m_AObArrayOfMaterials;}
		void ProcessSceneNode(domNode* pobNode, const rageColladaMatrix& obParentMatrix, const rageRolladaScene* pobScene);
		vector<rageColladaMesh*>	m_AObArrayOfMeshes;
		vector<rageColladaMaterial*>	m_AObArrayOfMaterials;
		rageColladaMatrix			m_obColladaFileToRageMatrix;
		GLdouble					m_adColladaFileToRageMatrixInOpenGLFormat[16];

	#ifdef MEMORY_LEAK_DETECTION
		static int	m_NoOfCurrentlyConstructedInstances;
	#endif
	};
}
