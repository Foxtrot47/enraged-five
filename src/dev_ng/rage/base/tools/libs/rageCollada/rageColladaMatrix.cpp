#include "rageColladaMatrix.h"

#pragma warning(push)
#pragma warning(disable : 4263)
#pragma warning(disable : 4100)
#include <1.4/dom/domTranslate.h>
#include <1.4/dom/domRotate.h>
#include <1.4/dom/domScale.h>
#pragma warning(pop)

using namespace rage;

// Used to apply collada based matrix operations to a rage matrix
rageColladaMatrix rageColladaMatrix::ApplyColladaMatrixOperation(daeElement* pobOperationElement) const
{
	// Matrix44 obMatrix = AsRageMatrix44();
//	printf("pobOperationElement->getTypeName() = %s\n", pobOperationElement->getTypeName());
	rageColladaMatrix obReturnMe = *this;

	if(!strcmp(pobOperationElement->getTypeName(), "translate"))
	{
		// Apply translation to the matirx
		domFloat3& obTranslation = ((domTranslate*)pobOperationElement)->getValue();
		obReturnMe(0,3) += (float)obTranslation[0];
		obReturnMe(1,3) += (float)obTranslation[1];
		obReturnMe(2,3) += (float)obTranslation[2];
		// obMatrix.Translate((float)obTranslation[0], (float)obTranslation[1], (float)obTranslation[2]);
	}
	else if(!strcmp(pobOperationElement->getTypeName(), "rotate"))
	{
		// Apply Rotate to the matirx
		domFloat4& obRotate = ((domRotate*)pobOperationElement)->getValue();
		Matrix34 obRotation34Matrix = M34_IDENTITY;
		obRotation34Matrix.Rotate(Vector3((float)obRotate[0], (float)obRotate[1], (float)obRotate[2]), DtoR * (float)obRotate[3]);
		Matrix44 obRotation44Matrix; obRotation44Matrix.FromMatrix34(obRotation34Matrix);
		obReturnMe = obRotation44Matrix.Dot(obReturnMe.AsRageMatrix44());
	}
	else if(!strcmp(pobOperationElement->getTypeName(), "scale"))
	{
		// Apply scale to the matirx
		domFloat3& obScale = ((domScale*)pobOperationElement)->getValue();
		Matrix44 obScaleMatrix;
		obScaleMatrix.Identity();
		obScaleMatrix.MakeScale((float)obScale[0], (float)obScale[1], (float)obScale[2]);
		obReturnMe = obScaleMatrix.Dot(obReturnMe.AsRageMatrix44());
	}
	return obReturnMe;
}

