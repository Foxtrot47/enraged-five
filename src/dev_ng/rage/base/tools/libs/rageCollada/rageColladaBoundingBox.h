#pragma once
#include "vector/vector3.h"
#pragma warning(push)
#pragma warning(disable : 4263)
#pragma warning(disable : 4100)
#include <1.4/dom/domBox.h>
#pragma warning(pop)
#include "rageColladaPoint.h"

namespace rage 
{

	class rageColladaBoundingBox
	{
	public:
		rageColladaBoundingBox(void) {};
		const rageColladaPoint& GetMin() const	{	return m_obMinPoint;}
		const rageColladaPoint& GetMax() const	{	return m_obMaxPoint;}
		void SetMin(const rageColladaPoint& obMin) {	m_obMinPoint = obMin;}
		void SetMax(const rageColladaPoint& obMax) {	m_obMaxPoint = obMax;}

		// Operators
		void CreateFromColladaNode(domBox* pobBoundingboxElement);

	private:
		rageColladaPoint	m_obMinPoint;
		rageColladaPoint	m_obMaxPoint;
	};
}
