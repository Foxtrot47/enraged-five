#pragma once

#include <string>
using namespace std;

namespace rage 
{
	class rageCRolladaMaterial
	{
	public:
		rageCRolladaMaterial(void) {};
		virtual ~rageCRolladaMaterial(void) {};

		// Accessors
		const string&	GetName() const	{	return m_strName;}
		const string&	GetSimpleTexturePathAndFilename() const	{	return m_strSimpleTexturePathAndFilename;}

		virtual void OpenGLDraw() const = 0;

	protected:

		string			m_strName;
		string			m_strSimpleTexturePathAndFilename;
	};
};

