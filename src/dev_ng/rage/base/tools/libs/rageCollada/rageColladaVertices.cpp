#include "rageColladaVertices.h"
// #include <maya/MColor.h>
// #include <maya/MVector.h>

using namespace rage;

rageColladaVertices::rageColladaVertices(void)
{
}

rageColladaVertices::~rageColladaVertices(void)
{
}

void rageColladaVertices::CreateFromColladaNode(domVertices* pobVertices)
{
	// Get the verts out of Collada and into Rage
	// The vertices tag should have an "input" tag called "POSITION"
	// Get what information is there out of the verts
	for(int i=0;i<(int)pobVertices->getInput_array().getCount(); i++)
	{
		//printf("pobVertices->getInput_array()[%d]->getSemantic() = %s\n", i, pobVertices->getInput_array()[i]->getSemantic());
		//printf("pobVertices->getInput_array()[%d]->getSource()   = %s\n", i, pobVertices->getInput_array()[i]->getSource());
		//printf("pobVertices->getInput_array()[%d]->getValue()   = %s\n", i, pobVertices->getInput_array()[i]->getValue());
		if(!strcmp(pobVertices->getInput_array()[i]->getSemantic(), "POSITION"))
		{
			//***********************************************************
			// Getting Vertex POSITION
			//***********************************************************
			// Found the POSITION tag, so get what it points too
			xsAnyURI* obURIForSourceOfVertexPositions = &(pobVertices->getInput_array()[i]->getSource());

			// Get the source
			domSource* pobSourceOfVertexPositions = (domSource*)(domElement*)obURIForSourceOfVertexPositions->getElement();

			// The first element in the float_array_array of the POSITION source tag is the array of positions
			const domFloat_array* obColladaVertexPositionArray = pobSourceOfVertexPositions->getFloat_array();
			const domListOfFloats& obColladaVertexPositionFloatList = obColladaVertexPositionArray->getValue();
			int iNoOfVertices = obColladaVertexPositionArray->getCount()/3;
			m_AObArrayOfVertexPositions.reserve(iNoOfVertices);
			for(int j=0; j<iNoOfVertices; j++)
			{
				// Extract the position info into something more friendly
				m_AObArrayOfVertexPositions.push_back(rageColladaPoint((float)obColladaVertexPositionFloatList[(j*3) + 0], (float)obColladaVertexPositionFloatList[(j*3) + 1], (float)obColladaVertexPositionFloatList[(j*3) + 2]));
			}
		}
		else if(!strcmp(pobVertices->getInput_array()[i]->getSemantic(), "NORMAL"))
		{
			//***********************************************************
			// Getting Vertex normals
			//***********************************************************
			// Found the NORMAL tag, so get what it points too
			xsAnyURI* obURIForSourceOfVertexNormals = &(pobVertices->getInput_array()[i]->getSource());

			// Get the source
			domSource* pobSourceOfVertexNormals = (domSource*)(domElement*)obURIForSourceOfVertexNormals->getElement();

			// The first element in the float_array_array of the Normal source tag is the array of Normals
			const domFloat_array* obColladaVertexNormalArray = pobSourceOfVertexNormals->getFloat_array();
			const domListOfFloats& obColladaVertexNormalFloatList = obColladaVertexNormalArray->getValue();
			int iNoOfVertices = obColladaVertexNormalArray->getCount()/3;
			m_AObArrayOfVertexNormals.reserve(iNoOfVertices);
			for(int j=0; j<iNoOfVertices; j++)
			{
				// Extract the Normal info into something more friendly
				m_AObArrayOfVertexNormals.push_back(rageColladaVector((float)obColladaVertexNormalFloatList[(j*3) + 0], (float)obColladaVertexNormalFloatList[(j*3) + 1], (float)obColladaVertexNormalFloatList[(j*3) + 2]));
			}
		}
		else if(!strcmp(pobVertices->getInput_array()[i]->getSemantic(), "COLOR"))
		{
			//***********************************************************
			// Getting Vertex colours
			//***********************************************************
			// Found the COLOR tag, so get what it points too
			xsAnyURI* obURIForSourceOfVertexColours = &(pobVertices->getInput_array()[i]->getSource());

			// Get the source
			domSource* pobSourceOfVertexColours = (domSource*)(domElement*)obURIForSourceOfVertexColours->getElement();

			// The first element in the float_array_array of the Colour source tag is the array of Colours
			const domFloat_array* obColladaVertexColourArray = pobSourceOfVertexColours->getFloat_array();
			const domListOfFloats& obColladaVertexColourFloatList = obColladaVertexColourArray->getValue();
			int iNoOfVertices = obColladaVertexColourArray->getCount()/4;
			m_AObArrayOfVertexColours.reserve(iNoOfVertices);
			for(int j=0; j<iNoOfVertices; j++)
			{
				// Extract the Colour info into something more friendly
				m_AObArrayOfVertexColours.push_back(rageColladaColor((float)obColladaVertexColourFloatList[(j*4) + 0], (float)obColladaVertexColourFloatList[(j*4) + 1], (float)obColladaVertexColourFloatList[(j*4) + 2], (float)obColladaVertexColourFloatList[(j*4) + 3]));
			}
		}
	}
}
