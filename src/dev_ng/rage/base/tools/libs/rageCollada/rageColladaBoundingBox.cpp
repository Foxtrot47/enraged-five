#include "rageColladaBoundingBox.h"

using namespace rage;

void rageColladaBoundingBox::CreateFromColladaNode(domBox* pobBoundingboxElement)
{
	// Get half values
	domFloat3& obHalfExtents = pobBoundingboxElement->getHalf_extents()->getValue();

	// Get min
	SetMin(rageColladaPoint((float)-obHalfExtents[0], (float)-obHalfExtents[1], (float)-obHalfExtents[2]));

	// Get Max
	SetMax(rageColladaPoint((float)obHalfExtents[0], (float)obHalfExtents[1], (float)obHalfExtents[2]));
}
