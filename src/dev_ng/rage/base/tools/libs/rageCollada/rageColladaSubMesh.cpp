#include "rageRolladaScene.h"
#include "rageColladaSubMesh.h"
// #include <maya/rageColladaColor.h>
// #include <maya/rageColladaVector.h>
#include <math.h>

using namespace rage;


#ifdef MEMORY_LEAK_DETECTION
int rageColladaSubMesh::m_NoOfCurrentlyConstructedInstances = 0;
#endif

void DrawWireframeEllipsoid(float xSize, float ySize, float zSize, int steps)
{
	int i, j;
	float x,y,z,s;
	if(steps<4) steps=4;
	for(i=1;i<(steps>>1);i++)
	{
		y=ySize*cosf(PI*float(i)/float(steps>>1));
		s=sinf(PI*float(i)/float(steps>>1));
		glBegin(GL_LINE_STRIP);
		for(j=0;j<=steps;j++) {
			x=xSize*s*cosf(2.0f*PI*float(j)/float(steps));
			z=zSize*s*sinf(2.0f*PI*float(j)/float(steps));
			glVertex3f(x,y,z);
		}
		glEnd();
	}
	for(i=0;i<(steps>>1);i++)
	{
		x=xSize*cosf(PI*float(i)/float(steps>>1));
		z=xSize*sinf(PI*float(i)/float(steps>>1));
		glBegin(GL_LINE_STRIP);
		for(j=0;j<=steps;j++) {
			s=sinf(2.0f*PI*float(j)/float(steps));
			y=ySize*cosf(2.0f*PI*float(j)/float(steps));
			glVertex3f(x*s,y,z*s);
		}
		glEnd();
	}
}


rageColladaSubMesh::rageColladaSubMesh(void)
{
	m_bHaveOpenGLDisplayList = false;
	m_pobVertices = NULL;
	m_pobMaterial = NULL;
	m_bDeleteMaterialWithSubMesh = true;
	m_iPolygonVertexNormalInputNo = -1;
	m_iPolygonVertexColourInputNo = -1;
	m_iPolygonVertexTexCoordsInputNo = -1;
	m_iPolygonVertexInputNo = -1;
#ifdef MEMORY_LEAK_DETECTION
	m_NoOfCurrentlyConstructedInstances++;
	Displayf("Created an instance of a rageColladaSubMesh (%p), currently %d active instances\n", this, m_NoOfCurrentlyConstructedInstances);
#endif
}

rageColladaSubMesh::~rageColladaSubMesh(void)
{
	delete m_pobVertices;
	if(m_bDeleteMaterialWithSubMesh)
	{
		delete m_pobMaterial;
	}
	for(unsigned i=0; i<m_AObArrayOfFaces.size(); i++)
	{
		delete[] m_AObArrayOfFaces[i];
		m_AObArrayOfFaces[i] = NULL;
	}
	if(m_bHaveOpenGLDisplayList)
	{
		glDeleteLists(m_uiOpenGLDisplayList, 1);
	}
	m_bHaveOpenGLDisplayList = false;
#ifdef MEMORY_LEAK_DETECTION
	m_NoOfCurrentlyConstructedInstances--;
	Displayf("Deleted an instance of a rageColladaSubMesh (%p), currently %d active instances\n", this, m_NoOfCurrentlyConstructedInstances);
#endif
}

void rageColladaSubMesh::CreateFromColladaNode(domPolygons* pobPolygons, const rageRolladaScene* pobScene)
{
	//***********************************************************
	// Get the material used
	//***********************************************************
	// Convert material name into a URI
	daeURI*	pobDocumentURI = pobPolygons->getDocumentURI();
	// printf("%s\n", *pobDocumentURI);
	const char* pcMaterialName = pobPolygons->getMaterial();
	if(pcMaterialName != NULL)
	{
		m_pobMaterial = NULL;
		if(pobScene)
		{
			// Ok, I have a rollada scene, so look for the material in there
			m_pobMaterial = pobScene->GetMaterial(pcMaterialName);
			m_bDeleteMaterialWithSubMesh = false;
		}
		if(!m_pobMaterial && pobScene->GetColladaScene())
		{
			// Ok, can't find it in the rollada scene, how about the collada scene?
			m_pobMaterial = pobScene->GetColladaScene()->GetMaterial(pcMaterialName);
			m_bDeleteMaterialWithSubMesh = false;
		}
		if(!m_pobMaterial)
		{
			// I have a material, so get it
			char acMaterialNodeURI[1024];
			strcpy(acMaterialNodeURI,	pobDocumentURI->getURI());
			strcat(acMaterialNodeURI,	"#");
			strcat(acMaterialNodeURI,	pcMaterialName);

			xsAnyURI obURIForMaterial; 
			obURIForMaterial.setURI((daeString)acMaterialNodeURI);
			obURIForMaterial.resolveElement();
			domMaterial* pobMaterialElement = (domMaterial*)(domElement*)obURIForMaterial.getElement();
			if(pobMaterialElement)
			{
				rageColladaMaterial* pobMaterial = new rageColladaMaterial();
				pobMaterial->CreateFromColladaNode(pobMaterialElement);
				m_pobMaterial = pobMaterial;
				m_bDeleteMaterialWithSubMesh = true;
			}
		}
	}

	//***********************************************************
	// Get what information I can about the Polygons
	//***********************************************************
	m_iNoOfPolygonVertexInputs = -1;
	const domInputLocalOffset_Array& obInputArray = pobPolygons->getInput_array();
	m_iNoOfPolygonVertexInputs = obInputArray.getCount();
	for(int i=0;i<(int)pobPolygons->getInput_array().getCount(); i++)
	{
		const domInputLocalOffsetRef& obInput = obInputArray[i];
		//printf("pobPolygons->getInput_array()[%d]->getSemantic() = %s\n", i, pobPolygons->getInput_array()[i]->getSemantic());
		//printf("pobPolygons->getInput_array()[%d]->getSource()   = %s\n", i, pobPolygons->getInput_array()[i]->getSource());
		//printf("pobPolygons->getInput_array()[%d]->getValue()   = %s\n", i, pobPolygons->getInput_array()[i]->getValue());
		if(!strcmp(obInput->getSemantic(), "VERTEX"))
		{
			//***********************************************************
			// Getting Polygon Vertices
			//***********************************************************
			m_iPolygonVertexInputNo = i;

			// Get vertices
			xsAnyURI* obURIForSourceOfPolygonVertices = &(obInput->getSource());

			// Get the source
			domVertices* pobVertices = (domVertices*)(domElement*)obURIForSourceOfPolygonVertices->getElement();

			// Create Rage Vertices
			delete m_pobVertices;
			m_pobVertices = new rageColladaVertices();
			m_pobVertices->CreateFromColladaNode(pobVertices);
		}
		else if(!strcmp(obInput->getSemantic(), "NORMAL"))
		{
			//***********************************************************
			// Getting PolygonVertex normals
			//***********************************************************
			m_iPolygonVertexNormalInputNo = i;
			// Found the NORMAL tag, so get what it points too
			xsAnyURI* obURIForSourceOfPolygonVertexNormals = &(obInput->getSource());

			// Get the source
			domSource* pobSourceOfPolygonVertexNormals = (domSource*)(domElement*)obURIForSourceOfPolygonVertexNormals->getElement();

			// The first element in the float_array_array of the Normal source tag is the array of Normals
			const domFloat_array* obColladaPolygonNormalArray = pobSourceOfPolygonVertexNormals->getFloat_array();
			const domListOfFloats& obColladaPolygonNormalFloatList = obColladaPolygonNormalArray->getValue();
			int iNoOfVertices = obColladaPolygonNormalArray->getCount()/3;
			m_AObArrayOfPolygonVertexNormals.reserve(iNoOfVertices);
			for(int j=0; j<iNoOfVertices; j++)
			{
				// Extract the position info into something more friendly
				m_AObArrayOfPolygonVertexNormals.push_back(rageColladaVector((float)obColladaPolygonNormalFloatList[(j*3) + 0], (float)obColladaPolygonNormalFloatList[(j*3) + 1], (float)obColladaPolygonNormalFloatList[(j*3) + 2]));
			}
		}
		else if(!strcmp(obInput->getSemantic(), "TEXCOORD"))
		{
			//***********************************************************
			// Getting PolygonVertex TexCoords
			//***********************************************************
			// Found the TEXCOORD tag, so get what it points too
			m_iPolygonVertexTexCoordsInputNo = i;
			xsAnyURI* obURIForSourceOfPolygonVertexTexCoords = &(obInput->getSource());

			// Get the source
			domSource* pobSourceOfPolygonVertexTexCoords = (domSource*)(domElement*)obURIForSourceOfPolygonVertexTexCoords->getElement();

			// The first element in the float_array_array of the TexCoord source tag is the array of TexCoords
			const domFloat_array* obColladaPolygonTexCoordArray = pobSourceOfPolygonVertexTexCoords->getFloat_array();
			const domListOfFloats& obColladaPolygonTexCoordFloatList = obColladaPolygonTexCoordArray->getValue();
			int iNoOfPolygonVertexTexCoords = obColladaPolygonTexCoordArray->getCount()/2;
			m_AObArrayOfPolygonVertexUVs.reserve(iNoOfPolygonVertexTexCoords);
			for(int j=0; j<iNoOfPolygonVertexTexCoords; j++)
			{
				// Extract the TexCoord info into something more friendly
				m_AObArrayOfPolygonVertexUVs.push_back(rageColladaUV((float)obColladaPolygonTexCoordFloatList[(j*2) + 0], (float)obColladaPolygonTexCoordFloatList[(j*2) + 1]));
			}
		}
		else if(!strcmp(obInput->getSemantic(), "COLOR"))
		{
			//***********************************************************
			// Getting PolygonVertex Colours
			//***********************************************************
			// Found the Colour tag, so get what it points too
			m_iPolygonVertexColourInputNo = i;
			xsAnyURI* obURIForSourceOfPolygonVertexColours = &(obInput->getSource());

			// Get the source
			domSource* pobSourceOfPolygonVertexColours = (domSource*)(domElement*)obURIForSourceOfPolygonVertexColours->getElement();

			// The first element in the float_array_array of the Colour source tag is the array of Colours
			const domFloat_array* obColladaPolygonVertexColourArray = pobSourceOfPolygonVertexColours->getFloat_array();
			const domListOfFloats& obColladaPolygonVertexColourFloatList = obColladaPolygonVertexColourArray->getValue();
			int iNoOfPolygonColours = obColladaPolygonVertexColourArray->getCount()/4;
			m_AObArrayOfPolygonVertexColours.reserve(iNoOfPolygonColours);
			for(int j=0; j<iNoOfPolygonColours; j++)
			{
				// Extract the Colour info into something more friendly
				m_AObArrayOfPolygonVertexColours.push_back(rageColladaColor((float)obColladaPolygonVertexColourFloatList[(j*4) + 0], (float)obColladaPolygonVertexColourFloatList[(j*4) + 1], (float)obColladaPolygonVertexColourFloatList[(j*4) + 2], (float)obColladaPolygonVertexColourFloatList[(j*4) + 3]));
			}
		}
	}
	SetNoOfPolygonVertexInputs(m_iNoOfPolygonVertexInputs);

	// Got all the info I can about the polys, now construct the polys themselves
	//***********************************************************
	// Get Collada Polygons and turn them into Rage Polygons
	//***********************************************************
	// Create the "primitives" (Polygons)
	// For every poly....
	int iNoOfPrimitives = pobPolygons->getP_array().getCount();
	m_AObArrayOfFaces.reserve(iNoOfPrimitives);
	m_AObArrayOfFaceSizes.reserve(iNoOfPrimitives);
	domP_Array& obAArrayOfPrimitives = pobPolygons->getP_array();
	for(int i=0; i<iNoOfPrimitives; i++)
	{
		// Record all the indices into every array
		domListOfUInts& pobAArrayOfIndices = obAArrayOfPrimitives[i]->getValue();

		// For every vertex in the poly...
		int* aiPolyIndices = new int[pobAArrayOfIndices.getCount()];
		for(int v=0; v<(int)pobAArrayOfIndices.getCount(); v++)
		{
			aiPolyIndices[v] = pobAArrayOfIndices[v];
		}
		m_AObArrayOfFaces.push_back(aiPolyIndices);
		m_AObArrayOfFaceSizes.push_back(pobAArrayOfIndices.getCount());
	}

	// Add material
//	m_pobRexGenericMesh->m_Materials.push_back(stFakedUpMaterial);
	//printf("m_pobRexGenericMesh->m_Materials.size() = %d\n", m_pobRexGenericMesh->m_Materials.size());
	//printf("m_pobRexGenericMesh->m_Primitives.size() = %d\n", m_pobRexGenericMesh->m_Primitives.size());
	//printf("m_pobRexGenericMesh->m_Vertices.size() = %d\n", m_pobRexGenericMesh->m_Vertices.size());
}

void rageColladaSubMesh::CreateFromColladaNode(domPolylist* pobPolygons, const rageRolladaScene* pobScene)
{
	//***********************************************************
	// Get the material used
	//***********************************************************
	// Convert material name into a URI
	daeURI*	pobDocumentURI = pobPolygons->getDocumentURI();
	// printf("%s\n", *pobDocumentURI);
	const char* pcMaterialName = pobPolygons->getMaterial();
	if(pcMaterialName != NULL)
	{
		m_pobMaterial = NULL;
		if(pobScene)
		{
			// Ok, I have a rollada scene, so look for the material in there
			m_pobMaterial = pobScene->GetMaterial(pcMaterialName);
			m_bDeleteMaterialWithSubMesh = false;
		}
		if(!m_pobMaterial && pobScene->GetColladaScene())
		{
			// Ok, can't find it in the rollada scene, how about the collada scene?
			m_pobMaterial = pobScene->GetColladaScene()->GetMaterial(pcMaterialName);
			m_bDeleteMaterialWithSubMesh = false;
		}
		if(!m_pobMaterial)
		{
			// I have a material, so get it
			char acMaterialNodeURI[1024];
			strcpy(acMaterialNodeURI,	pobDocumentURI->getURI());
			strcat(acMaterialNodeURI,	"#");
			strcat(acMaterialNodeURI,	pcMaterialName);

			xsAnyURI obURIForMaterial; 
			obURIForMaterial.setURI((daeString)acMaterialNodeURI);
			obURIForMaterial.resolveElement();
			domMaterial* pobMaterialElement = (domMaterial*)(domElement*)obURIForMaterial.getElement();
			if(pobMaterialElement)
			{
				rageColladaMaterial* pobMaterial = new rageColladaMaterial();
				pobMaterial->CreateFromColladaNode(pobMaterialElement);
				m_pobMaterial = pobMaterial;
				m_bDeleteMaterialWithSubMesh = true;
			}
		}
	}

	//***********************************************************
	// Get what information I can about the Polygons
	//***********************************************************
	m_iNoOfPolygonVertexInputs = -1;
	const domInputLocalOffset_Array& obInputArray = pobPolygons->getInput_array();
	m_iNoOfPolygonVertexInputs = obInputArray.getCount();
	for(int i=0;i<(int)pobPolygons->getInput_array().getCount(); i++)
	{
		const domInputLocalOffsetRef& obInput = obInputArray[i];
		//printf("pobPolygons->getInput_array()[%d]->getSemantic() = %s\n", i, pobPolygons->getInput_array()[i]->getSemantic());
		//printf("pobPolygons->getInput_array()[%d]->getSource()   = %s\n", i, pobPolygons->getInput_array()[i]->getSource());
		//printf("pobPolygons->getInput_array()[%d]->getValue()   = %s\n", i, pobPolygons->getInput_array()[i]->getValue());
		if(!strcmp(obInput->getSemantic(), "VERTEX"))
		{
			//***********************************************************
			// Getting Polygon Vertices
			//***********************************************************
			m_iPolygonVertexInputNo = i;

			// Get vertices
			xsAnyURI* obURIForSourceOfPolygonVertices = &(obInput->getSource());

			// Get the source
			domVertices* pobVertices = (domVertices*)(domElement*)obURIForSourceOfPolygonVertices->getElement();

			// Create Rage Vertices
			delete m_pobVertices;
			m_pobVertices = new rageColladaVertices();
			m_pobVertices->CreateFromColladaNode(pobVertices);
		}
		else if(!strcmp(obInput->getSemantic(), "NORMAL"))
		{
			//***********************************************************
			// Getting PolygonVertex normals
			//***********************************************************
			m_iPolygonVertexNormalInputNo = i;
			// Found the NORMAL tag, so get what it points too
			xsAnyURI* obURIForSourceOfPolygonVertexNormals = &(obInput->getSource());

			// Get the source
			domSource* pobSourceOfPolygonVertexNormals = (domSource*)(domElement*)obURIForSourceOfPolygonVertexNormals->getElement();

			// The first element in the float_array_array of the Normal source tag is the array of Normals
			const domFloat_array* obColladaPolygonNormalArray = pobSourceOfPolygonVertexNormals->getFloat_array();
			const domListOfFloats& obColladaPolygonNormalFloatList = obColladaPolygonNormalArray->getValue();
			int iNoOfVertices = obColladaPolygonNormalArray->getCount()/3;
			m_AObArrayOfPolygonVertexNormals.reserve(iNoOfVertices);
			for(int j=0; j<iNoOfVertices; j++)
			{
				// Extract the position info into something more friendly
				m_AObArrayOfPolygonVertexNormals.push_back(rageColladaVector((float)obColladaPolygonNormalFloatList[(j*3) + 0], (float)obColladaPolygonNormalFloatList[(j*3) + 1], (float)obColladaPolygonNormalFloatList[(j*3) + 2]));
			}
		}
		else if(!strcmp(obInput->getSemantic(), "TEXCOORD"))
		{
			//***********************************************************
			// Getting PolygonVertex TexCoords
			//***********************************************************
			// Found the TEXCOORD tag, so get what it points too
			m_iPolygonVertexTexCoordsInputNo = i;
			xsAnyURI* obURIForSourceOfPolygonVertexTexCoords = &(obInput->getSource());

			// Get the source
			domSource* pobSourceOfPolygonVertexTexCoords = (domSource*)(domElement*)obURIForSourceOfPolygonVertexTexCoords->getElement();

			// The first element in the float_array_array of the TexCoord source tag is the array of TexCoords
			const domFloat_array* obColladaPolygonTexCoordArray = pobSourceOfPolygonVertexTexCoords->getFloat_array();
			const domListOfFloats& obColladaPolygonTexCoordFloatList = obColladaPolygonTexCoordArray->getValue();
			int iNoOfPolygonVertexTexCoords = obColladaPolygonTexCoordArray->getCount()/2;
			m_AObArrayOfPolygonVertexUVs.reserve(iNoOfPolygonVertexTexCoords);
			for(int j=0; j<iNoOfPolygonVertexTexCoords; j++)
			{
				// Extract the TexCoord info into something more friendly
				m_AObArrayOfPolygonVertexUVs.push_back(rageColladaUV((float)obColladaPolygonTexCoordFloatList[(j*2) + 0], (float)obColladaPolygonTexCoordFloatList[(j*2) + 1]));
			}
		}
		else if(!strcmp(obInput->getSemantic(), "COLOR"))
		{
			//***********************************************************
			// Getting PolygonVertex Colours
			//***********************************************************
			// Found the Colour tag, so get what it points too
			m_iPolygonVertexColourInputNo = i;
			xsAnyURI* obURIForSourceOfPolygonVertexColours = &(obInput->getSource());

			// Get the source
			domSource* pobSourceOfPolygonVertexColours = (domSource*)(domElement*)obURIForSourceOfPolygonVertexColours->getElement();

			// The first element in the float_array_array of the Colour source tag is the array of Colours
			const domFloat_array* obColladaPolygonVertexColourArray = pobSourceOfPolygonVertexColours->getFloat_array();
			const domListOfFloats& obColladaPolygonVertexColourFloatList = obColladaPolygonVertexColourArray->getValue();
			int iNoOfPolygonColours = obColladaPolygonVertexColourArray->getCount()/4;
			m_AObArrayOfPolygonVertexColours.reserve(iNoOfPolygonColours);
			for(int j=0; j<iNoOfPolygonColours; j++)
			{
				// Extract the Colour info into something more friendly
				m_AObArrayOfPolygonVertexColours.push_back(rageColladaColor((float)obColladaPolygonVertexColourFloatList[(j*4) + 0], (float)obColladaPolygonVertexColourFloatList[(j*4) + 1], (float)obColladaPolygonVertexColourFloatList[(j*4) + 2], (float)obColladaPolygonVertexColourFloatList[(j*4) + 3]));
			}
		}
	}
	SetNoOfPolygonVertexInputs(m_iNoOfPolygonVertexInputs);

	// Got all the info I can about the polys, now construct the polys themselves
	//***********************************************************
	// Get Collada Polygons and turn them into Rage Polygons
	//***********************************************************
	// Create the "primitives" (Polygons)
	// Get number of polys
	int iNoOfPolys = pobPolygons->getCount();

	// Now for each poly, get the verts
	int iPPos = 0;
	for(int p=0; p<iNoOfPolys; p++)
	{
		// Get no of verts in this poly
		int iNoOfVertsInPoly = pobPolygons->getVcount()->getValue()[p];

		// For every vertex in the poly...
		int iNoOfIndicesForPoly = iNoOfVertsInPoly * m_iNoOfPolygonVertexInputs;
		int* aiPolyIndices = new int[iNoOfIndicesForPoly];
//		int iVertNo = 0;
//		for(int iVertNo = 0; iVertNo<(int)iNoOfVertsInPoly; iVertNo++)
		{
			int iIntialPPos = iPPos;
			int iFinalPPos = iIntialPPos + iNoOfIndicesForPoly;
			for(;iPPos < iFinalPPos; iPPos++)
			{
				aiPolyIndices[iPPos - iIntialPPos] = pobPolygons->getP()->getValue()[iPPos];
			}
		}
		m_AObArrayOfFaces.push_back(aiPolyIndices);
		m_AObArrayOfFaceSizes.push_back(iNoOfIndicesForPoly);
	}

	// Add material
	//	m_pobRexGenericMesh->m_Materials.push_back(stFakedUpMaterial);
	//printf("m_pobRexGenericMesh->m_Materials.size() = %d\n", m_pobRexGenericMesh->m_Materials.size());
	//printf("m_pobRexGenericMesh->m_Primitives.size() = %d\n", m_pobRexGenericMesh->m_Primitives.size());
	//printf("m_pobRexGenericMesh->m_Vertices.size() = %d\n", m_pobRexGenericMesh->m_Vertices.size());
}


void rageColladaSubMesh::OpenGLDraw() const
{
//	Displayf("%p rageColladaSubMesh::OpenGLDraw()", this);

	// Get current info
	GLboolean bTexturingOn;
	glGetBooleanv(GL_TEXTURE_2D, &bTexturingOn);
//	bTexturingOn = false;

	// glColor3f(0.5f + 0.5f * ((float)rand()/RAND_MAX), 0.5f + 0.5f * ((float)rand()/RAND_MAX), 0.5f + 0.5f * ((float)rand()/RAND_MAX));

	/******************************************************************************/
	// Begin Texture Experiments
	/******************************************************************************/
	if(bTexturingOn && GetMaterial())
	{
		GetMaterial()->OpenGLDraw();
	}

	/******************************************************************************/
	// End Texture Experiments
	/******************************************************************************/
	if(HaveVertices())
	{
		
		// Draw the mesh using OpenGL
		// I have verts, so draw me!
		// For every poly in the sub mesh
		unsigned iNoOfFaces = GetArrayOfFaces().size();
		for(unsigned p=0; p<iNoOfFaces; p++)
		{
			// Start poly
			glBegin(GL_POLYGON);

			// Create a Rex PrimitiveInfo to hold the poly information
			int iVertsPerFace = GetNumberOfVerticesPerFace(p);
			
			// For every vert in the poly in the sub mesh
			for(int v = 0; v<iVertsPerFace; v++)
			{
				// Normal
				const rageColladaVector* pobNormal = GetFaceVertexNormal(p, v);
				if(pobNormal)
				{
					glNormal3d(pobNormal->GetX(), pobNormal->GetY(), pobNormal->GetZ());
				}

				// Colour
				const rageColladaColor* pobColour = GetFaceVertexColour(p, v);
				if(pobColour)
				{
					// Displayf("Setting colour to (%f %f %f)\n", pobColour->GetR(), pobColour->GetG(), pobColour->GetB());
					glColor3d(pobColour->GetR(), pobColour->GetG(), pobColour->GetB());
				}
				else
				{
					// glColor4f(acDefaultColour[0], acDefaultColour[1], acDefaultColour[2], acDefaultColour[3]);
				}

				// TextureUVs
				const rageColladaUV* pobUV = GetFaceVertexUV(p, v);
				if(pobUV)
				{
					glTexCoord2f(pobUV->GetU(), pobUV->GetV()); 
				}

				// Position
				const rageColladaPoint* pobPosOfVertex = GetVertexPosition(p, v);
				if(pobPosOfVertex)
				{
					glVertex3d(pobPosOfVertex->GetX(),pobPosOfVertex->GetY(),pobPosOfVertex->GetZ());
				}
			}
			// End poly
			glEnd();
		}
	}
}

const rageColladaVector*	rageColladaSubMesh::GetFaceVertexNormal(int iFaceIndex, int iVertexInFaceIndex) const
{
	// Get face
	int* aiFace = m_AObArrayOfFaces[iFaceIndex];

	// Get vertex info for vert iVertexInFaceIndex
	int* piVertexInfoInFace = (aiFace + (iVertexInFaceIndex * GetNoOfPolygonVertexInputs()));

	// Use face vertex or vertex normal?
	if(GetPolygonVertexNormalInputNo() > -1)
	{
		// Use face vertex normal
		// Get normal no field from vertex info
		int* piNormalField = piVertexInfoInFace + m_iPolygonVertexNormalInputNo;

		// Get Normal no
		int iNormalNo = *(piNormalField);

		return &(m_AObArrayOfPolygonVertexNormals[iNormalNo]);
	}
	else if(GetVertices()->HasVertexNormals())
	{
		// Use vertex normal
		// Get vertex no field from vertex info
		int* piVertexField = piVertexInfoInFace + m_iPolygonVertexInputNo;

		// Get vertex no
		int iVertexNo = *(piVertexField);

		return &(m_pobVertices->GetArrayOfVertexNormals()[iVertexNo]);
	}
	return NULL;
}


const rageColladaColor*	rageColladaSubMesh::GetFaceVertexColour(int iFaceIndex, int iVertexInFaceIndex) const
{
	// Get face
	int* aiFace = m_AObArrayOfFaces[iFaceIndex];

	// Get vertex info for vert iVertexInFaceIndex
	int* piVertexInfoInFace = (aiFace + (iVertexInFaceIndex * GetNoOfPolygonVertexInputs()));

	// Use face vertex or vertex Colour?
	if(GetPolygonVertexColourInputNo() > -1)
	{
		// Use face vertex Colour
		// Get Colour no field from vertex info
		int* piColourField = piVertexInfoInFace + m_iPolygonVertexColourInputNo;

		// Get Colour no
		int iColourNo = *(piColourField);

		return &(m_AObArrayOfPolygonVertexColours[iColourNo]);
	}
	else if(GetVertices()->HasVertexColours())
	{
		// Use vertex Colour
		// Get vertex no field from vertex info
		int* piVertexField = piVertexInfoInFace + m_iPolygonVertexInputNo;

		// Get vertex no
		int iVertexNo = *(piVertexField);

		return &(m_pobVertices->GetArrayOfVertexColours()[iVertexNo]);
	}
	return NULL;
}


const rageColladaUV*	rageColladaSubMesh::GetFaceVertexUV(int iFaceIndex, int iVertexInFaceIndex) const
{
	// Get face
	int* aiFace = m_AObArrayOfFaces[iFaceIndex];

	// Get vertex info for vert iVertexInFaceIndex
	int* piVertexInfoInFace = (aiFace + (iVertexInFaceIndex * GetNoOfPolygonVertexInputs()));

	// Use face vertex or vertex UV?
	if(GetPolygonVertexTexCoordsInputNo() > -1)
	{
		// Use face vertex UV
		// Get UV no field from vertex info
		int* piUVField = piVertexInfoInFace + m_iPolygonVertexTexCoordsInputNo;

		// Get UV no
		int iUVNo = *(piUVField);

		return &(m_AObArrayOfPolygonVertexUVs[iUVNo]);
	}
	else if(GetVertices()->HasVertexUVs())
	{
		// Use vertex UV
		// Get vertex no field from vertex info
		int* piVertexField = piVertexInfoInFace + m_iPolygonVertexInputNo;

		// Get vertex no
		int iVertexNo = *(piVertexField);

		return &(m_pobVertices->GetArrayOfVertexUVs()[iVertexNo]);
	}
	return NULL;
}


const rageColladaPoint*	rageColladaSubMesh::GetVertexPosition(int iFaceIndex, int iVertexInFaceIndex) const
{
	// Get face
	int* aiFace = m_AObArrayOfFaces[iFaceIndex];

	// Get vertex info for vert iVertexInFaceIndex
	int* piVertexInfoInFace = (aiFace + (iVertexInFaceIndex * GetNoOfPolygonVertexInputs()));

	// Get vertex no field from vertex info
	int* piVertexField = piVertexInfoInFace + m_iPolygonVertexInputNo;

	// Get vertex no
	int iVertexNo = *(piVertexField);

	return &(m_pobVertices->GetArrayOfVertexPositions()[iVertexNo]);
}

int	rageColladaSubMesh::GetGlobalVertexNumber(int iFaceIndex, int iVertexInFaceIndex) const
{
	// Get face
	int* aiFace = m_AObArrayOfFaces[iFaceIndex];

	// Get vertex info for vert iVertexInFaceIndex
	int* piVertexInfoInFace = (aiFace + (iVertexInFaceIndex * GetNoOfPolygonVertexInputs()));

	// Get vertex no field from vertex info
	int* piVertexField = piVertexInfoInFace + m_iPolygonVertexInputNo;

	// Get vertex no
	int iVertexNo = *(piVertexField);

	return iVertexNo;
}

rageColladaBoundingBox	rageColladaSubMesh::CalcBoundingBox() const
{
	float fMinX = 99999999.9f;
	float fMinY = 99999999.9f;
	float fMinZ = 99999999.9f;
	float fMaxX =-99999999.9f;
	float fMaxY =-99999999.9f;
	float fMaxZ =-99999999.9f;

	// Go through verts, looking at positions as I go
	unsigned iNoOfFaces = GetArrayOfFaces().size();
	for(unsigned p=0; p<iNoOfFaces; p++)
	{
		// Create a Rex PrimitiveInfo to hold the poly information
		int iVertsPerFace = GetNumberOfVerticesPerFace(p);
		
		// For every vert in the poly in the sub mesh
		for(int v = 0; v<iVertsPerFace; v++)
		{
			const rageColladaPoint*	pobVertPos = GetVertexPosition(p, v);

			if(fMinX > pobVertPos->GetX()) fMinX = pobVertPos->GetX();
			if(fMinY > pobVertPos->GetY()) fMinY = pobVertPos->GetY();
			if(fMinZ > pobVertPos->GetZ()) fMinZ = pobVertPos->GetZ();

			if(fMaxX < pobVertPos->GetX()) fMaxX = pobVertPos->GetX();
			if(fMaxY < pobVertPos->GetY()) fMaxY = pobVertPos->GetY();
			if(fMaxZ < pobVertPos->GetZ()) fMaxZ = pobVertPos->GetZ();
		}
	}

	rageColladaBoundingBox obReturnMe;
	obReturnMe.SetMin(rageColladaPoint(fMinX, fMinY, fMinZ));
	obReturnMe.SetMax(rageColladaPoint(fMaxX, fMaxY, fMaxZ));
	return obReturnMe;
}

