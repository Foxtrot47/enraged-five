#pragma once
#include "parser/manager.h"

#include "rageRolladaParam.h"

#include <vector>
using namespace std;

namespace rage {

	class rageRolladaNode
	{
	public:
		rageRolladaNode(void);
		~rageRolladaNode(void);

		// Accessors
		const rageRolladaParam*			GetRolladaParam(const char* pcNodeName) const;
		const string& GetName() const {return m_strName;}

		// Operators
		void CreateFromRolladaNode(const parTreeNode* pobRolladaNode);

	private:
		const vector<rageRolladaParam*>&	GetArrayOfParams() const {return m_AObArrayOfParams;}

		string						m_strName;
		string						m_strId;
		string						m_strType;
		vector<rageRolladaParam*>	m_AObArrayOfParams;

	#ifdef MEMORY_LEAK_DETECTION
		static int	m_NoOfCurrentlyConstructedInstances;
	#endif
	};
}
