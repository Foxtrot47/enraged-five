#pragma once
#include "parser/manager.h"

#include <string>
using namespace std;

namespace rage {

	class rageRolladaParam
	{
	public:
		rageRolladaParam(void);
		~rageRolladaParam(void);

		// Accessors
		const string& GetName() const {return m_strName;}
		const string& GetType() const {return m_strType;}
		const string& GetValue() const {return m_strValue;}

		// Operators
		void CreateFromRolladaNode(const parTreeNode* pobRolladaNode);

	private:
		string	m_strName;
		string	m_strType;
		string	m_strValue;

	#ifdef MEMORY_LEAK_DETECTION
		static int	m_NoOfCurrentlyConstructedInstances;
	#endif
	};
}
