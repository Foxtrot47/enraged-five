// #include <maya/MTransformationMatrix.h>
// #include <maya/MVector.h>
// #include <maya/MQuaternion.h>
// #include <maya/MAngle.h>
#include "parser/manager.h"
#include "parsercore/attribute.h"
#include "rageRolladaNode.h"

using namespace rage;

#ifdef MEMORY_LEAK_DETECTION
int rageRolladaNode::m_NoOfCurrentlyConstructedInstances = 0;
#endif


rageRolladaNode::rageRolladaNode(void)
{
#ifdef MEMORY_LEAK_DETECTION
	m_NoOfCurrentlyConstructedInstances++;
	Displayf("Created an instance of a rageRolladaNode (%p), currently %d active instances\n", this, m_NoOfCurrentlyConstructedInstances);
#endif
}

rageRolladaNode::~rageRolladaNode(void)
{
	for(unsigned i=0; i<m_AObArrayOfParams.size(); i++)
	{
		delete m_AObArrayOfParams[i];
	}
#ifdef MEMORY_LEAK_DETECTION
	m_NoOfCurrentlyConstructedInstances--;
	Displayf("Deleted an instance of a rageRolladaNode (%p), currently %d active instances\n", this, m_NoOfCurrentlyConstructedInstances);
#endif
}

void rageRolladaNode::CreateFromRolladaNode(const parTreeNode* pobRolladaNode)
{
	// No good if I've been given junk
	Assertf(pobRolladaNode, "Unable to CreateFromRolladaNode from NULL pointer!");

	// Get name
	const parAttribute* pobNameAttribute = (const_cast<parTreeNode*>(pobRolladaNode))->GetElement().FindAttribute("name");
	Assertf(pobNameAttribute, "Unable to find name attribute for RolladaNode");
	m_strName = (const_cast<parAttribute*>(pobNameAttribute))->GetStringValue();
	
	// Get id
	const parAttribute* pobIdAttribute = (const_cast<parTreeNode*>(pobRolladaNode))->GetElement().FindAttribute("id");
	Assertf(pobIdAttribute, "Unable to find id attribute for RolladaNode");
	m_strId = (const_cast<parAttribute*>(pobIdAttribute))->GetStringValue();
	
	// Get type
	const parTreeNode* pobTypeDataNode = pobRolladaNode->FindChildWithName("type");
	Assertf(pobTypeDataNode, "Unable to file type node for Rollada node %s", m_strName.c_str());
	m_strType = pobTypeDataNode->GetData();

	// Get parameters
	const parTreeNode* pobAttributesDataNode = pobRolladaNode->FindChildWithName("attributes");
	// There is nothing to say you HAVE to have attributes node
	if(pobAttributesDataNode)
	{
		// Go through all the nodes under the nodes node (Confused?)
		const parTreeNode* pobRolladaParamNode = pobAttributesDataNode->FindChildWithName("param");
		while(pobRolladaParamNode)
		{
			// Get info from node
			rageRolladaParam* pobRolladaParam = new rageRolladaParam();
			pobRolladaParam->CreateFromRolladaNode(pobRolladaParamNode);
			m_AObArrayOfParams.push_back(pobRolladaParam);

			// Move onto the next one
			pobRolladaParamNode = pobAttributesDataNode->FindChildWithName("param", (const_cast<parTreeNode*>(pobRolladaParamNode)));
		}
	}
}


const rageRolladaParam*	rageRolladaNode::GetRolladaParam(const char* pcParamName) const
{
	vector<rageRolladaParam*>::const_iterator it = GetArrayOfParams().begin();
	for(; it != GetArrayOfParams().end(); it++)
	{
		const rageRolladaParam* pobRolladaParam = (*it);
		if(!strcmp(pobRolladaParam->GetName().c_str(), pcParamName))
		{
			// Bingo
			return pobRolladaParam;
		}
	}
	return NULL;
}
