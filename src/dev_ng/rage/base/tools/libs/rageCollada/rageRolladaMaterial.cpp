#define WIN32_LEAN_AND_MEAN
#pragma warning(push)
#pragma warning(disable : 4263)
#pragma warning(disable : 4100)
#include <1.4/dom/domCommon_color_or_texture_type.h>
#pragma warning(pop)

#pragma warning(push)
#pragma warning(disable: 4668)
#include <windows.h>
#pragma warning(pop)
#include <GL/gl.h>
#include <IL/il.h>
#include <IL/ilu.h>
#include "rageRolladaMaterial.h"

using namespace rage;

#ifdef MEMORY_LEAK_DETECTION
int rageRolladaMaterial::m_NoOfCurrentlyConstructedInstances = 0;
#endif

rageRolladaMaterial::rageRolladaMaterial(void)
{
	m_OpenGLTextureName = 0;
	m_TextureData = NULL;
	m_iTextureWidth = 0;
	m_iTextureHeight = 0;
	m_iOpenGLTextureFormat = 0;
	m_bAlreadyLoadedTexture = false;
	m_strFallBackTexturePath = "";

#ifdef MEMORY_LEAK_DETECTION
	m_NoOfCurrentlyConstructedInstances++;
	// Displayf("Created an instance of a rageRolladaMaterial (%p), currently %d active instances\n", this, m_NoOfCurrentlyConstructedInstances);
#endif
}

rageRolladaMaterial::~rageRolladaMaterial(void)
{
	delete m_TextureData;
#ifdef MEMORY_LEAK_DETECTION
	m_NoOfCurrentlyConstructedInstances--;
	// Displayf("Deleted an instance of a rageRolladaMaterial (%p), currently %d active instances\n", this, m_NoOfCurrentlyConstructedInstances);
#endif
}

void rageRolladaMaterial::CreateFromRolladaNode(const parTreeNode* pobRolladaMaterial)
{
	// I do not want to reproduce the mtl loader here, so I could just make a call to the existing one
	// BUT I really don't want the complexity of all that overhead, when 99% of the time I'll never 
	// rage-render or artist-edit the thing.
	// So instead just extract the mtl filename and the first texture name I find

	// No good if I've been given junk
	Assertf(pobRolladaMaterial, "Unable to CreateFromRolladaNode from NULL pointer!");

	// Get name
	const parAttribute* pobNameAttribute = (const_cast<parTreeNode*>(pobRolladaMaterial))->GetElement().FindAttribute("name");
	Assertf(pobNameAttribute, "Unable to find name attribute for RolladaNode");
	m_strName = (const_cast<parAttribute*>(pobNameAttribute))->GetStringValue();

	// Get id
	const parAttribute* pobIdAttribute = (const_cast<parTreeNode*>(pobRolladaMaterial))->GetElement().FindAttribute("id");
	Assertf(pobIdAttribute, "Unable to find id attribute for RolladaNode");
	m_strId = (const_cast<parAttribute*>(pobIdAttribute))->GetStringValue();

	// Get mtl path and file name
	const parTreeNode* pobMtlDataNode = pobRolladaMaterial->FindChildWithName("mtl");
	Assertf(pobMtlDataNode, "Unable to find mtl node for Rollada material %s", m_strName.c_str());
	const parAttribute* pobMtlPathAndFileNameAttribute = (const_cast<parTreeNode*>(pobMtlDataNode))->GetElement().FindAttribute("filename");
	Assertf(pobMtlPathAndFileNameAttribute, "Unable to find filename attribute for Rollada material %s", m_strName.c_str());
	m_strMtlPathAndFilename = (const_cast<parAttribute*>(pobMtlPathAndFileNameAttribute))->GetStringValue();

	// Try and find a texture, any texture will do, just get the first I can find
	const parTreeNode* pobShaderMaterialNode = pobMtlDataNode->FindChildWithName("rage__rageShaderMaterial");
	if(pobShaderMaterialNode)
	{
		const parTreeNode* pobShaderParamsNode = pobShaderMaterialNode->FindChildWithName("apShaderParams");
		if(pobShaderParamsNode)
		{
			// Go through all the Item nodes under here looking for texture params
			const parTreeNode* pobItemNode = pobShaderParamsNode->FindChildWithName("Item");
			while(pobItemNode)
			{
				// Get info from node
				const parAttribute* pobParamTypeAttribute = (const_cast<parTreeNode*>(pobItemNode))->GetElement().FindAttribute("type");
				Assertf(pobParamTypeAttribute, "Unable to find type attribute for Rollada material %s", m_strName.c_str());
				string strParamType = (const_cast<parAttribute*>(pobParamTypeAttribute))->GetStringValue();
				if(strParamType == "rage__rageShaderMaterialParamTexture")
				{
					// Found a texture param
					const parTreeNode* pobDataNode = pobItemNode->FindChildWithName("sData");
					Assertf(pobDataNode, "Unable to find data node for file param in Rollada material %s", m_strName.c_str());
					m_strSimpleTexturePathAndFilename = pobDataNode->GetData();

					// No point going any further, I've got a simple texture
					break;
				}

				// Move onto the next one
				pobItemNode = pobShaderParamsNode->FindChildWithName("Item", (const_cast<parTreeNode*>(pobItemNode)));
			}
		}
	}
}

void rageRolladaMaterial::OpenGLDraw() const
{
	// Displayf("rageRolladaMaterial::Draw() m_bAlreadyLoadedTexture = %d SimpleTexturePathAndFilename = %s", m_bAlreadyLoadedTexture, GetSimpleTexturePathAndFilename().c_str());
	if(m_bAlreadyLoadedTexture)
	{
		glDeleteTextures(1, &m_OpenGLTextureName);
	}

	if(!m_bAlreadyLoadedTexture)
	{
// #define DEBUG_TEXTURES
#ifndef DEBUG_TEXTURES
		// I have nothing to display, so load texture
		m_OpenGLTextureName = 0;
		m_TextureData = NULL;
		m_iTextureWidth = 0;
		m_iTextureHeight = 0;
		m_iOpenGLTextureFormat = 0;

		// Is there a file to load?
		if(GetSimpleTexturePathAndFilename() != "")
		{
			// Annoyingly something somewhere causes dds files to be loaded upside down!
			bool bTextureNeedsFlipping = false;
			// Displayf("Texture Extension = %s", GetSimpleTexturePathAndFilename().substr(GetSimpleTexturePathAndFilename().length() - 3, 3).c_str());
			if(GetSimpleTexturePathAndFilename().substr(GetSimpleTexturePathAndFilename().length() - 4, 4) == ".dds")
			{
				bTextureNeedsFlipping = true;
			}

			// Load the image
			ilInit();
			// Displayf("SimpleTexturePathAndFilename = %s\n", GetSimpleTexturePathAndFilename().c_str());
			// Load it!
			ILboolean bReturnCode = ilLoadImage((ILstring)GetSimpleTexturePathAndFilename().c_str()); 
			if(bReturnCode)
			{
				Displayf("Using %s", GetSimpleTexturePathAndFilename().c_str());
			}
			// Displayf("ilLoadImage bReturnCode = %d\n", bReturnCode);

			// Displayf("m_strFallBackTexturePath = %s", m_strFallBackTexturePath.c_str());
			if(bReturnCode == 0)
			{
				// Failed to load the texture :(
				// So a good fall back is to see if there is a dds version in the same folder as the collada file
				// Get just the filename form the GetSimpleTexturePathAndFilename
				int iPosOfLastSlash = GetSimpleTexturePathAndFilename().rfind('/');
				int iPosOfExtension = GetSimpleTexturePathAndFilename().rfind('.');
				string strFileNamePart = GetSimpleTexturePathAndFilename().substr(iPosOfLastSlash + 1, (iPosOfExtension - iPosOfLastSlash - 1));
				// Displayf("strFileNamePart = %s", strFileNamePart.c_str());

				// Unfortunately it isn't as straight forward as just sticking .dds on the end
				// as various things chop the end of the filename off
				// so instead I need to find a dds in the folder that starts the closest the same as possible
				// Lovely isn't it!
				// So look for files that are similar-ish
				string strBestFilenameYet = "";
				for(int iLengthToLookFor = strFileNamePart.length(); iLengthToLookFor > 0; iLengthToLookFor--)
				{
					WIN32_FIND_DATA DirData;
					string strSearchString = (m_strFallBackTexturePath + strFileNamePart.substr(0, iLengthToLookFor) + ".dds");
					// Displayf("strSearchString = %s", strSearchString.c_str());
					HANDLE DirFile = FindFirstFile(strSearchString.c_str(), &DirData);
					if(DirFile != INVALID_HANDLE_VALUE)
					{
						// Bingo!  Found a close match
						strBestFilenameYet = strSearchString;
						break;
					}
				}

				if(strBestFilenameYet != "")
				{
					// Found a dds that'll do
					bTextureNeedsFlipping = true;
					bReturnCode = ilLoadImage((ILstring)strBestFilenameYet.c_str()); 
					if(bReturnCode)
					{
						Displayf("Failed to find file %s so using %s instead", GetSimpleTexturePathAndFilename().c_str(), strBestFilenameYet.c_str());
					}
				}
				if(bReturnCode == 0)
				{
					Displayf("Failed to find file %s or backup file %s", GetSimpleTexturePathAndFilename().c_str(), strBestFilenameYet.c_str());
				}
			}

			if(bReturnCode)
			{
				// Image loaded, so process
				m_iTextureWidth = ilGetInteger(IL_IMAGE_WIDTH);
				m_iTextureHeight = ilGetInteger(IL_IMAGE_HEIGHT);
				switch(ilGetInteger(IL_IMAGE_FORMAT))
				{
				case IL_RGB: m_iOpenGLTextureFormat = GL_RGB; break;
				case IL_RGBA: m_iOpenGLTextureFormat = GL_RGBA; break;
				default: m_iOpenGLTextureFormat = 0; break;
				}

				// Take a copy of the data, so the file can be dumped
				m_TextureData = new char[m_iTextureWidth * m_iTextureHeight * 3];
				if(bTextureNeedsFlipping)
				{
					// Annoyingly something somewhere causes dds files to be loaded upside down!
					// Displayf("Flipping texture");
					for(int y=0; y<m_iTextureHeight; y++)
					{
						ilCopyPixels(0, y, 0, m_iTextureWidth, 1, 1, IL_RGB, IL_BYTE, &m_TextureData[m_iTextureWidth * (m_iTextureHeight - y - 1) * 3]);
					}
				}
				else
				{
					ilCopyPixels(0, 0, 0, m_iTextureWidth, m_iTextureHeight, 1, IL_RGB, IL_BYTE, m_TextureData);
				}
				m_iOpenGLTextureFormat = GL_RGB;

				// Displayf("Width = %d\n", m_iTextureWidth);
				// Displayf("Height = %d\n", m_iTextureHeight);
			}
		}
#else
//		if(!m_bAlreadyLoadedTexture)
		if(true)
		{
			// Generate a useful debug texture
			static GLubyte checkImage[64][64][4];
			int checkImageHeight = 64;
			int checkImageWidth = 64;
			for (int i = 0; i < checkImageHeight; i++) 
			{
				if(((i%8) == 0) || (i == 1))
				{
					for (int j = 0; j < checkImageWidth; j++) 
					{
						checkImage[i][j][0] = (GLubyte) 200;
						checkImage[i][j][1] = (GLubyte) 255;
						checkImage[i][j][2] = (GLubyte) 200;
						checkImage[i][j][3] = (GLubyte) 255;
					}
				}
				else
				{
					for (int j = 0; j < checkImageWidth; j++) 
					{
						if(((j%8) == 0) || (j == 1))
						{
							checkImage[i][j][0] = (GLubyte) 200;
							checkImage[i][j][1] = (GLubyte) 255;
							checkImage[i][j][2] = (GLubyte) 200;
						}
						else
						{
							checkImage[i][j][0] = (GLubyte) 255;
							checkImage[i][j][1] = (GLubyte) 255;
							checkImage[i][j][2] = (GLubyte) 255;
						}
						checkImage[i][j][3] = (GLubyte) 255;
					}
				}
			}
			m_iTextureWidth = checkImageHeight;
			m_iTextureHeight = checkImageWidth;
			m_iOpenGLTextureFormat = GL_RGBA;
			m_TextureData = (char*)checkImage;
		}
#endif
		m_bAlreadyLoadedTexture = true;
	}

	if(m_bAlreadyLoadedTexture)
	{
		// printf("%p %s %p [%d %d %d] [%d %d %d]\n", this, GetSimpleTexturePathAndFilename().c_str(), m_TextureData, (int)m_TextureData[0], (int)m_TextureData[1], (int)m_TextureData[2], (int)m_TextureData[3], (int)m_TextureData[4], (int)m_TextureData[5]);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		glGenTextures(1, &m_OpenGLTextureName);
		glBindTexture(GL_TEXTURE_2D, m_OpenGLTextureName);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_iTextureWidth, m_iTextureHeight, 0, m_iOpenGLTextureFormat, GL_UNSIGNED_BYTE, m_TextureData);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_REPEAT);
	}
}

