#pragma once

#include "rageRolladaNode.h"
#include "rageRolladaMaterial.h"
#include "rageColladaScene.h"

#include <vector>
using namespace std;

namespace rage {

	class rageRolladaScene
	{
	public:
		rageRolladaScene(void);
		~rageRolladaScene(void);

		// Accessors
		const rageRolladaNode*			GetRolladaNode(const char* pcNodeName) const;
		const rageColladaScene*			GetColladaScene() const {return m_pobRageColladaScene;}
		const rageRolladaMaterial*		GetMaterial(const string strMaterialName) const;

		// Operators
		void CreateFromFile(const char* pcPathAndFilename);
		void CreateFromRolladaFile(const char* pcPathAndFilename);
		void CreateFromColladaFile(const char* pcPathAndFilename);
		void CreateFromColladaURI(const char* pcColladaFileURI);
		void OpenGLDraw() const;
		rageColladaBoundingBox			CalcBoundingBox() const;

	private:

		// Accessors
		const vector<rageRolladaNode*>&		GetArrayOfNodes() const {return m_AObArrayOfNodes;}
		const vector<rageRolladaMaterial*>&	GetArrayOfShaders() const {return m_AObArrayOfMaterials;}

		// Members
		vector<rageRolladaNode*>		m_AObArrayOfNodes;
		vector<rageRolladaMaterial*>	m_AObArrayOfMaterials;
		rageColladaScene*				m_pobRageColladaScene;

	#ifdef MEMORY_LEAK_DETECTION
		static int	m_NoOfCurrentlyConstructedInstances;
	#endif
	};
}
