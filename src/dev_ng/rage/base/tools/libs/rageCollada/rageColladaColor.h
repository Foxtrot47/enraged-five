#pragma once
#include "vector/vector3.h"

namespace rage {

	class rageColladaColor
	{
	public:
		inline rageColladaColor(void);
		inline rageColladaColor(const float fR, const float fG, const float fB);
		inline rageColladaColor(const float fR, const float fG, const float fB, const float fA);
	public:
		inline ~rageColladaColor(void);

		float GetR() const	{	return m_fR;}
		void  SetR(float fR) { m_fR = fR;}
		float GetG() const	{	return m_fG;}
		void  SetG(float fG) { m_fG = fG;}
		float GetB() const	{	return m_fB;}
		void  SetB(float fB) { m_fB = fB;}
		float GetA() const	{	return m_fA;}
		void  SetA(float fA) { m_fA = fA;}


	private:
		float	m_fR;
		float	m_fG;
		float	m_fB;
		float	m_fA;

#ifdef MEMORY_LEAK_DETECTION
		static int	m_NoOfCurrentlyConstructedInstances;
#endif

	};


	// ********************************************************************
	// Inlines
	// ********************************************************************


	inline rageColladaColor::rageColladaColor(void) :
	m_fR(0.5f),
		m_fG(0.5f),
		m_fB(0.5f),
		m_fA(1.0f)
	{
#ifdef MEMORY_LEAK_DETECTION
		m_NoOfCurrentlyConstructedInstances++;
		//	Displayf("Created an instance of a rageColladaColor (%p), currently %d active instances\n", this, m_NoOfCurrentlyConstructedInstances);
#endif
	}

	inline rageColladaColor::rageColladaColor(const float fR, const float fG, const float fB):
	m_fR(fR),
		m_fG(fG),
		m_fB(fB),
		m_fA(1.0f)
	{
#ifdef MEMORY_LEAK_DETECTION
		m_NoOfCurrentlyConstructedInstances++;
		//	Displayf("Created an instance of a rageColladaColor (%p), currently %d active instances\n", this, m_NoOfCurrentlyConstructedInstances);
#endif
	}

	inline rageColladaColor::rageColladaColor(const float fR, const float fG, const float fB, const float fA):
	m_fR(fR),
		m_fG(fG),
		m_fB(fB),
		m_fA(fA)
	{
#ifdef MEMORY_LEAK_DETECTION
		m_NoOfCurrentlyConstructedInstances++;
		//	Displayf("Created an instance of a rageColladaColor (%p), currently %d active instances\n", this, m_NoOfCurrentlyConstructedInstances);
#endif
	}

	inline rageColladaColor::~rageColladaColor(void)
	{
#ifdef MEMORY_LEAK_DETECTION
		m_NoOfCurrentlyConstructedInstances--;
		//	Displayf("Deleted an instance of a rageColladaColor (%p), currently %d active instances\n", this, m_NoOfCurrentlyConstructedInstances);
#endif
	}

}

