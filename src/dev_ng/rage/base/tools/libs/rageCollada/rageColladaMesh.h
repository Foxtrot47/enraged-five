#pragma once
#include "rageColladaSubMesh.h"

// #include <maya/MPoint.h>
// #include <maya/MVector.h>
// #include <maya/MColor.h>
// #include <maya/MIntArray.h>
// #include <maya/MMatrix.h>
#define WIN32_LEAN_AND_MEAN
#pragma warning(push)
#pragma warning(disable: 4668)
#include <windows.h>
#pragma warning(pop)

#include <GL/gl.h>

#pragma warning(push)
#pragma warning(disable : 4263)
#pragma warning(disable : 4100)
#include <dae.h>
#include <1.4/dom/domCOLLADA.h>
#pragma warning(pop)

#include <vector>
using namespace std;

#include "rageColladaBoundingBox.h"
#include "rageColladaMatrix.h"

namespace rage 
{

	class rageRolladaScene;

	class rageColladaMesh
	{
	public:
		rageColladaMesh(void);
	public:
		virtual ~rageColladaMesh(void);

		// Operators
		rageColladaBoundingBox	CalcBoundingBox() const;
		void CreateFromColladaNode(domMesh* pobMeshElement, const rageRolladaScene* pobScene);
		void OpenGLDraw() const;

		// Accessors
		const vector<rageColladaSubMesh*>&	GetArrayOfSubMeshes() const {return m_AObArrayOfSubMeshes;}
		const rageColladaMatrix&			GetMatrix() const {return m_obMatrix;}
		const GLdouble*						GetMatrixInOpenGLFormat() const {return (&(m_adMatrixInOpenGLFormat[0]));}
		const string&						GetName() const {return m_strName;}

		// Modifiers
		void SetMatrix(const rageColladaMatrix& obMatrix);

	private:
		vector<rageColladaSubMesh*>	m_AObArrayOfSubMeshes;
		rageColladaMatrix			m_obMatrix;
		GLdouble					m_adMatrixInOpenGLFormat[16];
		string						m_strName;

	#ifdef MEMORY_LEAK_DETECTION
		static int	m_NoOfCurrentlyConstructedInstances;
	#endif
	};
}
