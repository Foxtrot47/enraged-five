#pragma once
#include "parser/manager.h"

#include <string>
#include <vector>
using namespace std;

namespace rage {

	class rageRolladaMaterialParam
	{
	public:
		rageRolladaMaterialParam(void);
		~rageRolladaMaterialParam(void);

		// Accessors
		const string& GetName() const {return m_strName;}
		const string& GetType() const {return m_strType;}
		const string& GetValue() const {return m_strValue;}
		int			GetNoOfTextures() const {return m_AStrTextureFileURIs.size();}
		const string& GetTextureURI(int iTextureNo) const {return m_AStrTextureFileURIs[iTextureNo];}

		// Operators
		void CreateFromRolladaNode(const parTreeNode* pobRolladaNode);

	private:
		string	m_strName;
		string	m_strType;
		string	m_strValue;
		vector<string>	m_AStrTextureFileURIs;

	#ifdef MEMORY_LEAK_DETECTION
		static int	m_NoOfCurrentlyConstructedInstances;
	#endif
	};
}
