#pragma once
#include "parser/manager.h"

#include "rageCRolladaMaterial.h"

#include <string>
using namespace std;

namespace rage 
{


	class rageRolladaMaterial : public rageCRolladaMaterial
	{
	public:
		rageRolladaMaterial(void);
		virtual ~rageRolladaMaterial(void);

		// Operators
		void CreateFromRolladaNode(const parTreeNode* pobRolladaMaterial);

		// Accessors
		const string&	GetId() const	{	return m_strId;}
		const string&	GetMtlPathAndFilename() const	{	return m_strMtlPathAndFilename;}

		virtual void OpenGLDraw() const;

	private:

		string			m_strId;
		string			m_strMtlPathAndFilename;

	#ifdef MEMORY_LEAK_DETECTION
		static int	m_NoOfCurrentlyConstructedInstances;
	#endif

		// Members below here are only used for openGL drawing of materials
		mutable unsigned int	m_OpenGLTextureName;
		mutable char* m_TextureData;
		mutable int m_iTextureWidth;
		mutable int m_iTextureHeight;
		mutable int m_iOpenGLTextureFormat;
		mutable bool m_bAlreadyLoadedTexture;

		string m_strFallBackTexturePath;
	};

}
