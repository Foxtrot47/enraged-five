#define WIN32_LEAN_AND_MEAN
#pragma warning(push)
#pragma warning(disable : 4263)
#pragma warning(disable : 4100)
#include <1.4/dom/domCommon_color_or_texture_type.h>
#pragma warning(pop)

#pragma warning(push)
#pragma warning(disable: 4668)
#include <windows.h>
#pragma warning(pop)
#include <GL/gl.h>
#include <IL/il.h>
#include <IL/ilu.h>
#include "rageColladaMaterial.h"

using namespace rage;

#ifdef MEMORY_LEAK_DETECTION
int rageColladaMaterial::m_NoOfCurrentlyConstructedInstances = 0;
#endif

rageColladaMaterial::rageColladaMaterial(void)
{
	m_OpenGLTextureName = 0;
	m_TextureData = NULL;
	m_iTextureWidth = 0;
	m_iTextureHeight = 0;
	m_iOpenGLTextureFormat = 0;
	m_bAlreadyLoadedTexture = false;
	m_strFallBackTexturePath = "";

#ifdef MEMORY_LEAK_DETECTION
	m_NoOfCurrentlyConstructedInstances++;
	Displayf("Created an instance of a rageColladaMaterial (%p), currently %d active instances\n", this, m_NoOfCurrentlyConstructedInstances);
#endif
}

rageColladaMaterial::~rageColladaMaterial(void)
{
	delete m_TextureData;
#ifdef MEMORY_LEAK_DETECTION
	m_NoOfCurrentlyConstructedInstances--;
	Displayf("Deleted an instance of a rageColladaMaterial (%p), currently %d active instances\n", this, m_NoOfCurrentlyConstructedInstances);
#endif
}

const domElement* rageColladaMaterial::FindFirstNodeOfType(const domElement* pobParentNode, const char* pcTypeOfNode) const
{
	// Am I what I am looking for
	if(strcmp(pobParentNode->getTypeName(), pcTypeOfNode) == 0)
	{
		return pobParentNode;
	}

	// Look through my contents for nodes of type
	daeTArray< daeSmartRef<daeElement> > obChildArray;
	(const_cast<domElement*> (pobParentNode))->getChildren(obChildArray);
	for(unsigned i=0; i<obChildArray.getCount(); i++)
	{
		daeSmartRef<daeElement> obRefToElement = obChildArray[i];
		const domElement* pobElement = &(*(obRefToElement));

		const domElement* pobFoundNode = FindFirstNodeOfType(pobElement, pcTypeOfNode);
		if(pobFoundNode)
		{
			return pobFoundNode;
		}
	}
	return NULL;
}

void rageColladaMaterial::CreateFromColladaNode(domMaterial* pobMaterialElement)
{
	//***********************************************************
	// Get name
	//***********************************************************
	m_strName = pobMaterialElement->getName();
	m_strFallBackTexturePath = pobMaterialElement->getDocumentURI()->getFilepath();
	m_strFallBackTexturePath = m_strFallBackTexturePath.substr(1, m_strFallBackTexturePath.length());

	//***********************************************************
	// Get simple texture path and filename - All wrong, but works for now
	//***********************************************************
	// Find the texture used, if one is used
	m_strSimpleTexturePathAndFilename = "";
	domInstance_effect* pobEffectInstance = pobMaterialElement->getInstance_effect();
	if(pobEffectInstance)
	{
		xsAnyURI& pobEffectUrl = pobEffectInstance->getUrl();
		const domEffect* pobEffect = (const domEffect*)(const domElement*)pobEffectUrl.getElement();
		if(pobEffect)
		{
			// Got effect, find common profile
			const domFx_profile_abstract_Array& pobProfileArray = pobEffect->getFx_profile_abstract_array();
			for(unsigned i=0; i<pobProfileArray.getCount(); i++)
			{
				domFx_profile_abstractRef pobProfileAbstract = pobProfileArray[i];
				if(strcmp(pobProfileAbstract->getTypeName(), "profile_COMMON") == 0)
				{
					// Bingo, found the COMMON profile, look for file textures
					const domCommon_color_or_texture_type_complexType::domTexture* pobTexture = (const domCommon_color_or_texture_type_complexType::domTexture*)FindFirstNodeOfType(pobProfileAbstract, "texture");
					if(pobTexture)
					{
						char acImageNodeURI[1024];
						strcpy(acImageNodeURI,	pobMaterialElement->getDocumentURI()->getURI());
						strcat(acImageNodeURI,	"#");
						strcat(acImageNodeURI,	pobTexture->getTexture());

						xsAnyURI obURIForImage; 
						obURIForImage.setURI((daeString)acImageNodeURI);
						obURIForImage.resolveElement();
						domImage* pobImage = (domImage*)(domElement*)obURIForImage.getElement();
						if(pobImage)
						{
							// Got an image, now get its file
							domImage::domInit_from* pobFilenameContainer = pobImage->getInit_from();

							xsAnyURI obURIForImageFile = pobFilenameContainer->getValue();
							m_strSimpleTexturePathAndFilename = "";
							if(obURIForImageFile.getAuthority())
							{
								m_strSimpleTexturePathAndFilename = obURIForImageFile.getAuthority();
							}
							m_strSimpleTexturePathAndFilename += obURIForImageFile.getFilepath();
							m_strSimpleTexturePathAndFilename += obURIForImageFile.getFile();

							// Clean the string
							for(unsigned s=0; s<m_strSimpleTexturePathAndFilename.length(); s++)
							{
								if(m_strSimpleTexturePathAndFilename[s] == '|')
								{
									m_strSimpleTexturePathAndFilename[s] = ':';
								}
							}
						}
					}
					if(m_strSimpleTexturePathAndFilename != "") break;
				}
				if(m_strSimpleTexturePathAndFilename != "") break;
			}
		}
	}
}

void rageColladaMaterial::OpenGLDraw() const
{
	// Displayf("rageColladaMaterial::OpenGLDraw() m_bAlreadyLoadedTexture = %d", m_bAlreadyLoadedTexture);
	if(m_bAlreadyLoadedTexture)
	{
		glDeleteTextures(1, &m_OpenGLTextureName);
	}

	if(!m_bAlreadyLoadedTexture)
	{
// #define DEBUG_TEXTURES
#ifndef DEBUG_TEXTURES
		// I have nothing to display, so load texture
		m_OpenGLTextureName = 0;
		m_TextureData = NULL;
		m_iTextureWidth = 0;
		m_iTextureHeight = 0;
		m_iOpenGLTextureFormat = 0;

		// Is there a file to load?
		if(GetSimpleTexturePathAndFilename() != "")
		{
			// Annoyingly something somewhere causes dds files to be loaded upside down!
			bool bTextureNeedsFlipping = false;
			// Displayf("Texture Extension = %s", GetSimpleTexturePathAndFilename().substr(GetSimpleTexturePathAndFilename().length() - 3, 3).c_str());
			if(GetSimpleTexturePathAndFilename().substr(GetSimpleTexturePathAndFilename().length() - 4, 4) == ".dds")
			{
				bTextureNeedsFlipping = true;
			}

			// Load the image
			ilInit();
			// Displayf("SimpleTexturePathAndFilename = %s\n", GetSimpleTexturePathAndFilename().c_str());
			// Load it!
			ILboolean bReturnCode = ilLoadImage((ILstring)GetSimpleTexturePathAndFilename().c_str()); 
			if(bReturnCode)
			{
				Displayf("Using %s", GetSimpleTexturePathAndFilename().c_str());
			}
			// Displayf("ilLoadImage bReturnCode = %d\n", bReturnCode);

			// Displayf("m_strFallBackTexturePath = %s", m_strFallBackTexturePath.c_str());
			if(bReturnCode == 0)
			{
				// Failed to load the texture :(
				// So a good fall back is to see if there is a dds version in the same folder as the collada file
				// Get just the filename form the GetSimpleTexturePathAndFilename
				int iPosOfLastSlash = GetSimpleTexturePathAndFilename().rfind('/');
				int iPosOfExtension = GetSimpleTexturePathAndFilename().rfind('.');
				string strFileNamePart = GetSimpleTexturePathAndFilename().substr(iPosOfLastSlash + 1, (iPosOfExtension - iPosOfLastSlash - 1));
				// Displayf("strFileNamePart = %s", strFileNamePart.c_str());

				// Unfortunately it isn't as straight forward as just sticking .dds on the end
				// as various things chop the end of the filename off
				// so instead I need to find a dds in the folder that starts the closest the same as possible
				// Lovely isn't it!
				// So look for files that are similar-ish
				string strBestFilenameYet = "";
				for(int iLengthToLookFor = strFileNamePart.length(); iLengthToLookFor > 0; iLengthToLookFor--)
				{
					WIN32_FIND_DATA DirData;
					string strSearchString = (m_strFallBackTexturePath + strFileNamePart.substr(0, iLengthToLookFor) + ".dds");
					// Displayf("strSearchString = %s", strSearchString.c_str());
					HANDLE DirFile = FindFirstFile(strSearchString.c_str(), &DirData);
					if(DirFile != INVALID_HANDLE_VALUE)
					{
						// Bingo!  Found a close match
						strBestFilenameYet = strSearchString;
						break;
					}
				}

				if(strBestFilenameYet != "")
				{
					// Found a dds that'll do
					bTextureNeedsFlipping = true;
					bReturnCode = ilLoadImage((ILstring)strBestFilenameYet.c_str()); 
					if(bReturnCode)
					{
						Displayf("Failed to find file %s so using %s instead", GetSimpleTexturePathAndFilename().c_str(), strBestFilenameYet.c_str());
					}
				}
				if(bReturnCode == 0)
				{
					Displayf("Failed to find file %s or backup file %s", GetSimpleTexturePathAndFilename().c_str(), strBestFilenameYet.c_str());
				}
			}

			if(bReturnCode)
			{
				// Image loaded, so process
				m_iTextureWidth = ilGetInteger(IL_IMAGE_WIDTH);
				m_iTextureHeight = ilGetInteger(IL_IMAGE_HEIGHT);
				switch(ilGetInteger(IL_IMAGE_FORMAT))
				{
				case IL_RGB: m_iOpenGLTextureFormat = GL_RGB; break;
				case IL_RGBA: m_iOpenGLTextureFormat = GL_RGBA; break;
				default: m_iOpenGLTextureFormat = 0; break;
				}

				// Take a copy of the data, so the file can be dumped
				m_TextureData = new char[m_iTextureWidth * m_iTextureHeight * 3];
				if(bTextureNeedsFlipping)
				{
					// Annoyingly something somewhere causes dds files to be loaded upside down!
					// Displayf("Flipping texture");
					for(int y=0; y<m_iTextureHeight; y++)
					{
						ilCopyPixels(0, y, 0, m_iTextureWidth, 1, 1, IL_RGB, IL_BYTE, &m_TextureData[m_iTextureWidth * (m_iTextureHeight - y - 1) * 3]);
					}
				}
				else
				{
					ilCopyPixels(0, 0, 0, m_iTextureWidth, m_iTextureHeight, 1, IL_RGB, IL_BYTE, m_TextureData);
				}
				m_iOpenGLTextureFormat = GL_RGB;

				//Displayf("Width = %d\n", m_iTextureWidth);
				//Displayf("Height = %d\n", m_iTextureHeight);
			}
		}
#else
//		if(!m_bAlreadyLoadedTexture)
		if(true)
		{
			// Generate a useful debug texture
			static GLubyte checkImage[64][64][4];
			int checkImageHeight = 64;
			int checkImageWidth = 64;
			for (int i = 0; i < checkImageHeight; i++) 
			{
				if(((i%8) == 0) || (i == 1))
				{
					for (int j = 0; j < checkImageWidth; j++) 
					{
						checkImage[i][j][0] = (GLubyte) 200;
						checkImage[i][j][1] = (GLubyte) 255;
						checkImage[i][j][2] = (GLubyte) 200;
						checkImage[i][j][3] = (GLubyte) 255;
					}
				}
				else
				{
					for (int j = 0; j < checkImageWidth; j++) 
					{
						if(((j%8) == 0) || (j == 1))
						{
							checkImage[i][j][0] = (GLubyte) 200;
							checkImage[i][j][1] = (GLubyte) 255;
							checkImage[i][j][2] = (GLubyte) 200;
						}
						else
						{
							checkImage[i][j][0] = (GLubyte) 255;
							checkImage[i][j][1] = (GLubyte) 255;
							checkImage[i][j][2] = (GLubyte) 255;
						}
						checkImage[i][j][3] = (GLubyte) 255;
					}
				}
			}
			m_iTextureWidth = checkImageHeight;
			m_iTextureHeight = checkImageWidth;
			m_iOpenGLTextureFormat = GL_RGBA;
			m_TextureData = (char*)checkImage;
		}
#endif
		m_bAlreadyLoadedTexture = true;
	}

	if(m_bAlreadyLoadedTexture)
	{
		// printf("%p %s %p [%d %d %d] [%d %d %d]\n", this, GetSimpleTexturePathAndFilename().c_str(), m_TextureData, (int)m_TextureData[0], (int)m_TextureData[1], (int)m_TextureData[2], (int)m_TextureData[3], (int)m_TextureData[4], (int)m_TextureData[5]);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		glGenTextures(1, &m_OpenGLTextureName);
		glBindTexture(GL_TEXTURE_2D, m_OpenGLTextureName);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_iTextureWidth, m_iTextureHeight, 0, m_iOpenGLTextureFormat, GL_UNSIGNED_BYTE, m_TextureData);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_REPEAT);
	}
}

