#pragma once
#include "rageColladaVertices.h"
#include "rageColladaBoundingBox.h"
// #include <maya/MPoint.h>
// #include <maya/MVector.h>
// #include <maya/MColor.h>
// #include <maya/MIntArray.h>
// #include <maya/MMatrix.h>
#define WIN32_LEAN_AND_MEAN
#pragma warning(push)
#pragma warning(disable: 4668)
#include <windows.h>
#pragma warning(pop)
#include <GL/gl.h>


#pragma warning(push)
#pragma warning(disable : 4263)
#pragma warning(disable : 4100)
#include <dae.h>
#include <1.4/dom/domCOLLADA.h>
#pragma warning(pop)
#include <vector>
using namespace std;

#include "rageColladaVector.h"
#include "rageColladaColor.h"
#include "rageColladaUV.h"
#include "rageCRolladaMaterial.h"


namespace rage {

	class rageRolladaScene;

	class rageColladaSubMesh
	{
	public:
		rageColladaSubMesh(void);
	public:
		virtual ~rageColladaSubMesh(void);

		// Accessors
		const rageColladaVector*		GetFaceVertexNormal(int iFaceIndex, int iVertexInFaceIndex) const;
		const rageColladaColor*			GetFaceVertexColour(int iFaceIndex, int iVertexInFaceIndex) const;
		const rageColladaUV*			GetFaceVertexUV(int iFaceIndex, int iVertexInFaceIndex) const;
		const rageColladaPoint*			GetVertexPosition(int iFaceIndex, int iVertexInFaceIndex) const;
		int								GetGlobalVertexNumber(int iFaceIndex, int iVertexInFaceIndex) const;
		int								GetNumberOfVerticesPerFace(int iFaceIndex) const {return (int)(m_fOneOverNoOfPolygonVertexInputs * GetArrayOfFaceSizes()[iFaceIndex]);}

		bool							HaveVertices() const {return (GetPolygonVertexInputNo() > -1);}

		const rageColladaVertices*		GetVertices() const {return m_pobVertices;}

		//const rageColladaVector&		GetPolygonVertexNormal(int iIndex) const {return m_AObArrayOfPolygonVertexNormals[iIndex];}
		//const rageColladaColor&			GetPolygonVertexColour(int iIndex) const {return m_AObArrayOfPolygonVertexColours[iIndex];}
		//const rageColladaUV&			GetPolygonVertexUV(int iIndex) const {return m_AObArrayOfPolygonVertexUVs[iIndex];}
		//const rageColladaVector*		GetPolygonVertexNormalPtr(int iIndex) const {return &(m_AObArrayOfPolygonVertexNormals[iIndex]);}
		//const rageColladaColor*			GetPolygonVertexColourPtr(int iIndex) const {return &(m_AObArrayOfPolygonVertexColours[iIndex]);}
		//const rageColladaUV*			GetPolygonVertexUVPtr(int iIndex) const {return &(m_AObArrayOfPolygonVertexUVs[iIndex]);}
		//const vector<rageColladaVector>& GetArrayOfPolygonVertexNormals() const {return m_AObArrayOfPolygonVertexNormals;}
		//const vector<rageColladaColor>&	GetArrayOfPolygonVertexColours() const {return m_AObArrayOfPolygonVertexColours;}
		//const vector<rageColladaUV>&	GetArrayOfPolygonVertexUVs() const {return m_AObArrayOfPolygonVertexUVs;}
		const vector<unsigned>&			GetArrayOfFaceSizes() const {return m_AObArrayOfFaceSizes;}
		const vector<int*>&				GetArrayOfFaces() const {return m_AObArrayOfFaces;}
		const rageCRolladaMaterial*		GetMaterial() const {return m_pobMaterial;}

		// Operators
		rageColladaBoundingBox			CalcBoundingBox() const;
		void CreateFromColladaNode(domPolygons* pobPolygons, const rageRolladaScene* pobScene);
		void CreateFromColladaNode(domPolylist* pobPolygons, const rageRolladaScene* pobScene);
		void OpenGLDraw() const;

	private:
		int								GetNoOfPolygonVertexInputs() const {return m_iNoOfPolygonVertexInputs;}
		int								GetPolygonVertexInputNo() const {return m_iPolygonVertexInputNo;}
		int								GetPolygonVertexNormalInputNo() const {return m_iPolygonVertexNormalInputNo;}
		int								GetPolygonVertexColourInputNo() const {return m_iPolygonVertexColourInputNo;}
		int								GetPolygonVertexTexCoordsInputNo() const {return m_iPolygonVertexTexCoordsInputNo;}

		void							SetNoOfPolygonVertexInputs(int iNoOfPolygonVertexInputs) { m_iNoOfPolygonVertexInputs = iNoOfPolygonVertexInputs; m_fOneOverNoOfPolygonVertexInputs = 1.0f / iNoOfPolygonVertexInputs; }

		int					m_iPolygonVertexInputNo;
		int					m_iPolygonVertexNormalInputNo;
		int					m_iPolygonVertexColourInputNo;
		int 				m_iPolygonVertexTexCoordsInputNo;
		int					m_iNoOfPolygonVertexInputs;			// Use modifier function to set
		float				m_fOneOverNoOfPolygonVertexInputs;
		rageColladaVertices*			m_pobVertices;
		vector<rageColladaVector>		m_AObArrayOfPolygonVertexNormals;
		vector<rageColladaColor>		m_AObArrayOfPolygonVertexColours;
		vector<rageColladaUV>			m_AObArrayOfPolygonVertexUVs;
		vector<unsigned>	m_AObArrayOfFaceSizes;
		vector<int*>		m_AObArrayOfFaces;

		const rageCRolladaMaterial*			m_pobMaterial;
		bool				m_bDeleteMaterialWithSubMesh;

	#ifdef MEMORY_LEAK_DETECTION
		static int	m_NoOfCurrentlyConstructedInstances;
	#endif

		// Members below here are only used for openGL drawing of materials
		mutable bool				m_bHaveOpenGLDisplayList;
		mutable unsigned int		m_uiOpenGLDisplayList;
	};
}
