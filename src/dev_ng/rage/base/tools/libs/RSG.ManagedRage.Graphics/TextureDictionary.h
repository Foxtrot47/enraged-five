
// RAGE headers
#include "grcore/texture.h"
#include "paging/dictionary.h"
using namespace System::Diagnostics;

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#pragma managed
#endif

namespace RSG
{
namespace ManagedRage
{
namespace Graphics
{

	public ref class TextureDictionary
	{
	public:
		TextureDictionary( System::String^ filename );
		~TextureDictionary( );
	private:
		const int RSC_VERSION;

		System::String^ m_sFilename;
		::rage::pgDictionary<::rage::grcTexture>* m_pTXD;
	};

} // Graphics namespace
} // ManagedRage namespace
} // RSG namespace

