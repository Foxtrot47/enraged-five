
#include "TextureDictionary.h"
#include  "MarshallingUtils.h"

// RAGE headers
#include "file/handle.h"
#include "grcore/setup.h"
#include "paging/rscbuilder.h"
#include "paging/streamer.h"

namespace RSG
{
namespace ManagedRage
{
namespace Graphics
{

TextureDictionary::TextureDictionary( System::String^ filename )
	: RSC_VERSION( ::rage::grcTexture::RORC_VERSION )
	, m_pTXD( NULL )
{
	::rage::sysParam::Init( 0, NULL );
	::rage::grcDevice::InitSingleton( );
	::rage::pgStreamer::InitClass( );

	System::String^ file = System::IO::Path::GetFileNameWithoutExtension( filename );
	System::String^ ext = System::IO::Path::GetExtension( filename );

	unsigned int size;
	pin_ptr<const wchar_t> wsFilename = PtrToStringChars( filename );
	pin_ptr<const char> ansiFilename = WideCharArrayToCharArray( filename, wsFilename );
	pin_ptr<const wchar_t> wsExtension = PtrToStringChars( ext );
	pin_ptr<const char> ansiExtension = WideCharArrayToCharArray( ext, wsExtension );
	
	pin_ptr<unsigned int> pinnedSize = &size;
	pin_ptr<::rage::pgDictionary<::rage::grcTexture>> pinnedTXD = this->m_pTXD; 
	::rage::pgDictionary<::rage::grcTexture>* pTXD = pinnedTXD;
	//::rage::pgStreamer::Handle h = ::rage::pgStreamer::Open( ansiFilename, &size, RSC_VERSION );
	::rage::pgRscBuilder::Load( pTXD, ansiFilename, "#td", RSC_VERSION );
	::rage::pgDictionary<::rage::grcTexture>::SetCurrent( pTXD );

	for ( int n = 0; n < pTXD->GetCount(); ++n )
	{
		::rage::grcTexture* pTexture = pTXD->GetEntry( n );

	}
}

TextureDictionary::~TextureDictionary( )
{
	delete m_pTXD;
	m_pTXD = NULL;
}


} // Graphics namespace
} // ManagedRage namespace
} // RSG namespace