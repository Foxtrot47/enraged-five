
// RAGE headers
#include "grcore/texture.h"
using namespace System::Diagnostics;

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#pragma managed
#endif

namespace RSG
{
namespace ManagedRage
{
namespace Graphics
{

	public ref class Texture
	{
	public:

	private:
		::rage::grcTexture* m_pTexture;
	};

} // Graphics namespace
} // ManagedRage namespace
} // RSG namespace

