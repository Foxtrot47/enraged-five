using System;
using System.Collections;
using System.Text;

namespace rageExpressionParser
{
    public delegate object TokenMaker(string s);

    public struct Token
    {
        public readonly Tokens label;
        public readonly string s;
        public readonly TokenMaker create;

        public Token(Tokens label, string s)
        {
            this.label = label;
            this.s = s;
            this.create = null;
        }

        public Token(Tokens label, string s, TokenMaker create)
            : this(label, s)
        {
            this.create = create;
        }

        public override string ToString()
        {
            return "'" + s + "' (" + label + ")";
        }
    };
}