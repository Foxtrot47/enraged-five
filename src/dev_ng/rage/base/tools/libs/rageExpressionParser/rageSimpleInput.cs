using System;
using System.Collections;

namespace rageExpressionParser
{
    public class SimpleInput : IEnumerator
    {
        private readonly string orig;
        private string val;
        private bool done;

        public SimpleInput(string line)
        {
            this.orig = line;
            this.val = null;
            this.done = false;
        }

        public void Reset() { done = false; }

        public object Current
        {
            get { return val; }
        }

        public bool MoveNext()
        {
            if (!done && val == null)
            {
                val = orig;
                return true;
            }
            else
            {
                done = true;
                val = null;
                return false;
            }
        }
    }
}