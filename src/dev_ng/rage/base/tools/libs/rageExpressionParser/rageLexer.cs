using System;
using System.Collections;
using System.Text.RegularExpressions;

namespace rageExpressionParser
{
    public class Lexer : IEnumerator
    {
        private IEnumerator t;

        public Lexer(IEnumerator input, params Token[] tokens)
        {
            t = input;

            for (int i = 0; i < tokens.Length; ++i)
                t = new Tokenizer(t, tokens[i]);
        }

        public object Current
        {
            get { return t.Current; }
        }

        public bool MoveNext()
        {
            return t.MoveNext();
        }

        public void Reset()
        {
            throw new InvalidOperationException();
        }

        static TokenMaker ignore = new TokenMaker(Tokenizer.ignoretoken);
        public static TokenMaker Ignore
        {
            get { return ignore; }
        }

        public StreamNode ToStream()
        {
            return Lexer.ToStream(this.t);
        }

        static public StreamNode ToStream(IEnumerator input)
        {
            object v = input.Current;

            if (v == null)
                return new StreamNode(null, null);

            PromiseMaker pm = new PromiseMaker(input);
            return new StreamNode(v, pm.Promise);
        }

        class PromiseMaker
        {
            private IEnumerator input;

            public PromiseMaker(IEnumerator input)
            {
                this.input = input;
            }

            private StreamNode MakeGood()
            {
                if (input.MoveNext())
                    return Lexer.ToStream(input);
                else
                    return null;
            }

            public Promise Promise
            {
                get { return new Promise(MakeGood); }
            }
        }

        class Tokenizer : IEnumerator
        {
            private IEnumerator input;
            private string buf;
            private Tokens label;
            private Regex pattern;
            private TokenMaker mktoken;
            private Queue tokens;

            internal static object ignoretoken(string ignored) { return ""; }

            public Tokenizer(IEnumerator input, Token token)
            {
                this.input = input;

                buf = "";
                tokens = new Queue();

                this.label = token.label;
                this.pattern = new Regex("(" + token.s + ")");
                this.mktoken = token.create != null ? token.create : new TokenMaker(this.MakeToken);
            }

            private string[] Split()
            {
                return pattern.Split(buf);
            }

            public object Current
            {
                get { return tokens.Peek(); }
            }

            private void FlushBuffer(Token token)
            {
                string[] parts = Split();

                // separator, if present
                MaybeEnqueue(parts[0]);

                // the lexeme we're after
                if (parts.Length > 1)
                    MaybeEnqueue(mktoken(parts[1]));

                tokens.Enqueue(token);

                buf = "";
            }

            private void AppendToBuffer(object chunk)
            {
                if (chunk != null)
                    buf = buf + (string)chunk;
            }

            private string TokenizeRemainingInput(object chunk)
            {
                AppendToBuffer(chunk);
                ArrayList newtoks = new ArrayList(Split());

                while (newtoks.Count > 2 || newtoks.Count > 0 && chunk != null)
                {
                    MaybeEnqueue(Shift(newtoks));

                    if (newtoks.Count > 0)
                        MaybeEnqueue(mktoken((string)Shift(newtoks)));
                }

                if (chunk == null)
                    return null;
                else
                    return String.Join("", (string[])newtoks.ToArray(typeof(string)));
            }

            private object Shift(ArrayList a)
            {
                object head = a[0];
                a.RemoveAt(0);

                return head;
            }

            private void MaybeEnqueue(object o)
            {
                if (o is string && (string)o == "")
                    return;

                tokens.Enqueue(o);
            }

            public bool MoveNext()
            {
                if (tokens.Count > 0)
                {
                    tokens.Dequeue();
                    if (tokens.Count > 0)
                        return true;
                }

                while (tokens.Count == 0 && buf != null)
                {
                    if (!input.MoveNext())
                        return false;

                    object i = input.Current;

                    if (i is Token)
                    {
                        FlushBuffer((Token)i);
                        break;
                    }

                    buf = TokenizeRemainingInput(i);
                }

                return true;
            }

            public void Reset()
            {
                throw new InvalidOperationException();
            }

            public object MakeToken(string token)
            {
                return new Token(this.label, token);
            }
        }
    }
}