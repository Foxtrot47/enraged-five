using System;
using System.Collections;

namespace rageExpressionParser
{
    public struct ParserState
    {
        public readonly bool Success;
        public readonly ArrayList Values;
        public readonly StreamNode RemainingInput;

        public ParserState(ArrayList values, StreamNode rest)
        {
            this.Values = values;
            this.RemainingInput = rest;
            this.Success = true;
        }

        public ParserState(bool assumedFalse)
        {
            this.Success = false;
            this.Values = null;
            this.RemainingInput = null;
        }

        public override string ToString()
        {
            if (Success)
                //return "[" + String.Join("][", (string[])Values.ToArray(typeof(string))) + "]";
                return ShowTree(Values, "");
            else
                return "FAILED";
        }

        private static string ShowTree(ArrayList tree, string indent)
        {
            System.Text.StringBuilder result = new System.Text.StringBuilder();

            result.Append(indent + "[\n");

            foreach (object o in tree)
            {
                if (o is ArrayList)
                    result.Append(ShowTree((ArrayList)o, indent + "  "));
                else
                    result.Append(indent + "'" + o + "'\n");
            }

            result.Append(indent + "]\n");

            return result.ToString();
        }
    }

    public abstract class Parser
    {
        public abstract ParserState Parse(StreamNode input);

        // for eta-conversion
        public virtual Parser GetParser()
        {
            return this;
        }

        public void Trying(Parser p)
        {
            //Console.WriteLine("trying " + p + "...");
        }

        public ParserState Success(ArrayList values, StreamNode rest)
        {
            ParserState success = new ParserState(values, rest);
            //Console.WriteLine("SUCCESS: " + success.ToString());
            return success;
        }

        public ParserState Failure()
        {
            //Console.WriteLine(this + ": FAILURE");
            return new ParserState(false);
        }

        private string name = null;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public override string ToString()
        {
            if (Name != null)
                return Name;
            else
                return GetType().ToString();
        }
    }

    public class Nothing : Parser
    {
        public override ParserState Parse(StreamNode input)
        {
            return Success(new ArrayList(), input);
        }

        public override string ToString()
        {
            return "Nothing";
        }
    }

    public class EndOfInput : Parser
    {
        public override ParserState Parse(StreamNode input)
        {
            if (input == null || input.Head == null)
                return Success(new ArrayList(), input);
            else
                return Failure();
        }

        public override string ToString()
        {
            return "EndOfInput";
        }
    }

    public class LookFor : Parser
    {
        Tokens lookfor;
        string s;

        public LookFor(Tokens lookfor, string s)
        {
            this.lookfor = lookfor;
            this.s = s;
        }

        public LookFor(Tokens lookfor) : this(lookfor, null) { }

        public override string ToString()
        {
            return "LookFor(" + lookfor + ",'" + s + "')";
        }

        public override ParserState Parse(StreamNode input)
        {
            if (input == null)
                return Failure();

            Token t = (Token)input.Head;

            if (t.label != lookfor)
                return Failure();

            if (s != null && t.s != s)
                return Failure();

            ArrayList values = new ArrayList();
            values.Add(t.s);

            return Success(values, input.Tail);
        }
    }

    public class Concatenate : Parser
    {
        private Parser[] p;

        public Concatenate(params Parser[] p)
        {
            if (p.Length == 0)
                this.p = new Nothing[1];
            else
                this.p = p;
        }

        public override string ToString()
        {
            if (Name == null)
            {
                string[] parsers = new string[p.Length];
                for (int i = 0; i < p.Length; i++)
                    parsers[i] = p[i].ToString();

                return String.Join(" ", parsers);
            }
            else
                return base.ToString();
        }

        public override ParserState Parse(StreamNode input)
        {
            if (p.Length == 1)
                return p[0].Parse(input);

            ArrayList values = new ArrayList();

            foreach (Parser parser in p)
            {
                Trying(parser);
                ParserState state = parser.Parse(input);
                if (!state.Success)
                    return Failure();

                if (state.Values.Count <= 1)
                    values.AddRange(state.Values);
                else
                    values.Add(state.Values);

                input = state.RemainingInput;
            }

            return Success(values, input);
        }
    }

    public class Alternate : Parser
    {
        private Parser[] p;

        public Alternate(params Parser[] p)
        {
            if (p.Length == 0)
                this.p = null;
            else
                this.p = p;
        }

        public override ParserState Parse(StreamNode input)
        {
            if (p == null)
                return Failure();

            foreach (Parser parser in p)
            {
                ParserState state = parser.Parse(input);
                if (state.Success)
                    return state;
            }

            return Failure();
        }
    }

    public class Star : Parser
    {
        protected Parser parser;

        public Star(Parser p)
        {
            ParserStub stub = new ParserStub(new StubToReal(GetParser));

            parser = new Alternate(new Concatenate(p, stub), new Nothing());
        }

        public override ParserState Parse(StreamNode input)
        {
            ParserState state = parser.Parse(input);
            if (state.Success)
                return state;
            else
                return Failure();
        }

        public override Parser GetParser()
        {
            return parser;
        }
    }

    public delegate Parser StubToReal();

    // eta-converted parser
    public class ParserStub : Parser
    {
        StubToReal p;

        public ParserStub(StubToReal p)
        {
            this.p = p;
        }

        public override ParserState Parse(StreamNode input)
        {
            return p().Parse(input);
        }
    }

    public delegate ArrayList ValuesTransform(ArrayList values);
    public class T : Parser
    {
        private Parser p;
        private ValuesTransform map;

        public T(Parser p, ValuesTransform map)
        {
            this.p = p;
            this.map = map;
        }

        public override ParserState Parse(StreamNode input)
        {
            ParserState state = p.Parse(input);

            if (state.Success)
                return Success(map(state.Values), state.RemainingInput);
            else
                return Failure();
        }
    }
}