using System;

namespace rageExpressionParser
{
    public delegate StreamNode Promise();

    public class StreamNode
    {
        private object head;
        private object tail;

        public StreamNode(object head, Promise tail)
        {
            this.head = head;
            this.tail = tail;
        }

        public object Head
        {
            get { return head; }
        }

        public StreamNode Tail
        {
            get
            {
                if (tail is Promise)
                    tail = ((Promise)tail)();

                return (StreamNode)tail;
            }
        }
    }
}