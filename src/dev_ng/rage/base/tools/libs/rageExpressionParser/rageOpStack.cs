using System;
using System.Collections;
using System.IO;
using System.Windows.Forms;

namespace rageExpressionParser
{
    public class OpStack
    {
        public class Const
        {
            public Const(Int32 value)
            {
                Value = value;
            }

            public Const(float value)
            {
                Value = value;
            }

            public object Value;
        }

        public class Add { }
        public class Sub { }
        public class Not { }
        public class Neg { }
        public class Mul { }
        public class Div { }
        public class Mod { }
        public class And { }
        public class Xor { }
        public class Ior { }

        public void Parse(Parser parser, Lexer lexer, Hashtable lookup)
        {
            try
            {
                ParserState state = parser.Parse(lexer.ToStream());
                BuildStack(ref stack, state.Values, lookup);
            }
            catch (Exception)
            {
                // TODO: Improve this exception handling so we don't hide actual exceptions
                MessageBox.Show("Badly formatted expression. Must be of format:\n+= value");
            }
        }

        public Int32 Count
        {
            get { return stack.Count; }
        }
        
        public Object Pop()
        {
            return stack.Pop();
        }

        public object Evaluate()
        {
            ValStack valstack = new ValStack();

            while (0 != Count)
            {
                object o = Pop();

                if (o is OpStack.Const)
                {
                    OpStack.Const constant = o as OpStack.Const;
                    valstack.Push(constant.Value);
                }
                else
                if (o is OpStack.Add)
                {
                    float rval = (float)valstack.Pop();
                    valstack.Top = rval + (float)valstack.Top;
                }
                else
                if (o is OpStack.Div)
                {
                    float rval = (float)valstack.Pop();
                    valstack.Top = rval / (float)valstack.Top;
                }
                else
                if (o is OpStack.Mul)
                {
                    float rval = (float)valstack.Pop();
                    valstack.Top = rval * (float)valstack.Top;
                }
                else
                if (o is OpStack.Sub)
                {
                    float rval = (float)valstack.Pop();
                    valstack.Top = rval - (float)valstack.Top;
                }
            }

            return valstack.Pop();
        }

        private void BuildStack(ref Stack stack, ArrayList tree, Hashtable lookup)
        {
            foreach (object o in tree)
            {
                if (o is ArrayList)
                {
                    BuildStack(ref stack, (ArrayList)o, lookup);
                }
                else
                {
                    if (Ints.ContainsKey(o))
                    {
                        stack.Push(new Const(Convert.ToInt32(o)));
                    }
                    else
                    if (Reals.ContainsKey(o))
                    {
                        stack.Push(new Const(Convert.ToSingle(o)));
                    }
                    else
                    if (Vars.ContainsKey(o))
                    {
                        if (lookup.ContainsKey(o))
                        {
                            stack.Push(new Const(Convert.ToSingle(lookup[(String)o])));
                        }
                    }
                    else
                    if (o.ToString() == "+")
                    {
                        stack.Push(new Add());
                    }
                    else
                    if (o.ToString() == "/")
                    {
                        stack.Push(new Div());
                    }
                    else
                    if (o.ToString() == "*")
                    {
                        stack.Push(new Mul());
                    }
                    else
                    if (o.ToString() == "-")
                    {
                        stack.Push(new Sub());
                    }
                }
            }
        }

        public Hashtable Ints = new Hashtable();
        public Hashtable Reals = new Hashtable();
        public Hashtable Funcs = new Hashtable();
        public Hashtable Vars = new Hashtable();

        private Stack stack = new Stack();
    };

    class ValStack
    {
        public void Push(object o)
        {
            stack.Push(o);
        }

        public object Pop()
        {
            return stack.Pop();
        }

        public object Top
        {
            get { return stack.Peek(); }
            set { stack.Pop(); stack.Push(value); }
        }

        Stack stack = new Stack();
    }
   }