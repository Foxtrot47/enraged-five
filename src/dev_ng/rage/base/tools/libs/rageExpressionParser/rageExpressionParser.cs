using System;
using System.Collections;
using System.IO;
using System.Text.RegularExpressions;

namespace rageExpressionParser
{
    public enum Tokens
    {
        Terminator, Integer, Real, Identifier, Operator, Whitespace,
    };

    class ExpressionParser
    {
        static OpStack opstack = new OpStack();

        static public Object Parse( String expr )
        {
            SimpleInput lexpr = new SimpleInput(expr);

            Lexer l = new Lexer(
              lexpr,
              new Token(Tokens.Terminator, @";\n*|\n+"),
              new Token(Tokens.Real, @"\d+\.\d+\b"),
              new Token(Tokens.Integer, @"\b\d+\b"),
              new Token(Tokens.Identifier, @"[A-Za-z_]\w*"),
              new Token(Tokens.Operator, @"\*\*|[-=+*/()]"),
              new Token(Tokens.Whitespace, @"\s+", Lexer.Ignore));

            ParserStub exprstub = new ParserStub(new StubToReal(GetExpression));
            ParserStub termstub = new ParserStub(new StubToReal(GetTerm));
            ParserStub factstub = new ParserStub(new StubToReal(GetFactor));

            expression = new Alternate(
              new T(new Concatenate(termstub, new LookFor(Tokens.Operator, "+"), exprstub),
                    new ValuesTransform(OpFirst)),
              new T(new Concatenate(termstub, new LookFor(Tokens.Operator, "-"), exprstub),
                    new ValuesTransform(OpFirst)),
              termstub);

            term = new Alternate(
              new T(new Concatenate(factstub, new LookFor(Tokens.Operator, "*"), termstub),
                    new ValuesTransform(OpFirst)),
              new T(new Concatenate(factstub, new LookFor(Tokens.Operator, "/"), termstub),
                    new ValuesTransform(OpFirst)),
              factstub);

            factor = new Alternate(
              new T(
                new Concatenate(
                  new Alternate( 
                    new LookFor(Tokens.Integer),
                    new LookFor(Tokens.Real)),
                  new Alternate(
                    new Concatenate(new LookFor(Tokens.Operator, "("), exprstub, new LookFor(Tokens.Operator, ")")),
                    new Nothing())),
                new ValuesTransform(TagReal)),
              new T(new Concatenate(new LookFor(Tokens.Operator, "("), exprstub, new LookFor(Tokens.Operator, ")")),
                    new ValuesTransform(StripParens)));

            Parser entire =
              new T(new Concatenate(exprstub, new EndOfInput()),
                    new ValuesTransform(OnlyExpression));

            l.MoveNext();

            opstack.Parse( entire, l, new Hashtable() );

            ValStack valstack = new ValStack();

            while (0 != opstack.Count)
            {
                object o = opstack.Pop();

                if (o is OpStack.Const)
                {
                    OpStack.Const constant = o as OpStack.Const;
                    valstack.Push(constant.Value);
                }
                else
                if (o is OpStack.Add)
                {
                    float rval = (float)valstack.Pop();
                    valstack.Top = rval + (float)valstack.Top;
                }
                else
                if (o is OpStack.Div)
                {
                    float rval = (float)valstack.Pop();
                    valstack.Top = rval / (float)valstack.Top;
                }
                else
                if (o is OpStack.Mul)
                {
                    float rval = (float)valstack.Pop();
                    valstack.Top = rval * (float)valstack.Top;
                }
                else
                if (o is OpStack.Sub)
                {
                    float rval = (float)valstack.Pop();
                    valstack.Top = rval - (float)valstack.Top;
                }
            }

            return valstack.Pop();
        }

        static private Parser expression;
        static private Parser GetExpression() { return expression; }

        static private Parser term;
        static private Parser GetTerm() { return term; }

        static private Parser factor;
        static private Parser GetFactor() { return factor; }

        static private ArrayList OnlyExpression(ArrayList values)
        {
            ArrayList result = new ArrayList(1);
            result.Add(values[0]);
            return result;
        }

        static private ArrayList StripParens(ArrayList values)
        {
            ArrayList result = new ArrayList(1);
            result.Add(values[1]);
            return result;
        }

        static private ArrayList OpFirst(ArrayList values)
        {
            ArrayList result = new ArrayList(3);
            result.Add(values[1]);
            result.Add(values[0]);
            result.Add(values[2]);
            return result;
        }

        static private ArrayList TagVarOrFunction(ArrayList values)
        {
            //Console.WriteLine("tag called: values[0] = '" + values[0] + "'; values.Count = " + values.Count);
            ArrayList result = new ArrayList();

            if (values.Count == 1)
            {
                opstack.Vars[values[0]] = null;
                result.Add(values[0]);
            }
            else
            {
                opstack.Funcs[values[0]] = null;
                result.Add(values[0]);
                result.Add(((ArrayList)values[1])[1]);
            }

            return result;
        }

        static private ArrayList TagInteger(ArrayList values)
        {
            ArrayList result = new ArrayList();

            if (values.Count == 1)
            {
                opstack.Ints[values[0]] = null;
                result.Add(values[0]);
            }

            return result;
        }

        static private ArrayList TagReal(ArrayList values)
        {
            ArrayList result = new ArrayList();

            if (values.Count == 1)
            {
                opstack.Reals[values[0]] = null;
                result.Add(values[0]);
            }

            return result;
        }
    }
}