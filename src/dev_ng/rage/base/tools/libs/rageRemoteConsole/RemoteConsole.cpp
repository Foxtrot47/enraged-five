// 
// rageRemoteConsole\RemoteConsole.cpp
// 
// Copyright (C) Rockstar Games.  All Rights Reserved. 
// 

#include "libjson/libjson.h"
#include <float.h>

#include "RemoteConsole.h"

#include "configParser/configPortView.h"
#include "bank/widget.h"

#include "file/asset.h"
#include "file/tcpip.h"
#include "file/stream.h"

#include "system/param.h"

using namespace rage;

char* RemoteConsole::m_LocalHost="127.0.0.1";
int RemoteConsole::m_DefaultPort=5105;  //This corresponds to the RAG's ConsoleProxy object. 
int RemoteConsole::m_ConnectionListPort=10000;
char* RemoteConsole::m_PingMessage="ping";
bool RemoteConsole::m_SwapEndianness = false;
char RemoteConsole::m_DataBuffer[DATA_BUFFER_SIZE];

RemoteConsole::RemoteConsole() : m_Stream(NULL), m_CommandLock(m_CommandLockToken)
{
	// Determine the proxy's hub connection service port from our configuration data.
	configParser::ConfigPortView portview;
	int configPort;
	if (portview.GetPortForId("RAGProxyHubConnectionService", configPort))
	{
		m_ConnectionListPort = configPort;
	}

	m_CommandLock.Exit();
	if ( sysParam::IsInitialized() == false)
		sysParam::Init(0, NULL);
}

RemoteConsole::~RemoteConsole() 
{
	Disconnect();
}

bool RemoteConsole::Connect()
{
	return Connect(m_LocalHost);
}

bool RemoteConsole::Connect(std::string hostname)
{
	// Query the hub connection service for determining the port to use when connecting.
	int proxyPort = GetConsoleProxyPort(hostname.c_str());

	return Connect(hostname, proxyPort);
}

bool RemoteConsole::Connect(std::string hostname, int port)
{
	return Connect(hostname.c_str(), port);
}

bool RemoteConsole::Connect(const char* hostname, int port)
{
	char streamName[RAGE_MAX_PATH];
	formatf(streamName, "tcpip:%d:%s", port, hostname);
	m_Stream = ASSET.Open(streamName, "");
	return (m_Stream != NULL);
}

bool RemoteConsole::IsConnected()
{
	if (m_Stream == NULL)
	{
		return false;
	}
	
	//Don't clog the queue of commands with mere ping commands.
	if ( m_CommandLock.IsLocked() == true )
		return true;

	atString resultStr;
	bool result = SendCommand("rc_ping", resultStr);
	if ( result == false || resultStr.GetLength() == 0 || stricmp(resultStr.c_str(), "rc_ping") != 0)
	{
		return false;
	}

	return true;
}

void RemoteConsole::Disconnect()
{
	if (m_Stream)
	{
		m_Stream->Close();
		m_Stream = NULL;
	}
}

void RemoteConsole::LogMessages(bool enable)
{
	m_LogMessages = enable;
}

void RemoteConsole::SetLogMessageCallback(LogMessageCallback callback)	
{ 
	m_LogMessageCallback = callback; 
}

bool RemoteConsole::SendCommand(const char* command)
{
	if (m_Stream == NULL)
		return false;

	while ( m_CommandLock.IsLocked() ) { } 

	m_CommandLock.Enter();

	if ( m_Stream->Write(command, (int) strlen(command)) == -1 )
	{
		m_CommandLock.Exit();
		return false;
	}

	if ( m_Stream->Write("\n", 1) == -1 )
	{
		m_CommandLock.Exit();
		return false;
	}

	m_Stream->Flush();
	m_CommandLock.Exit();
	return true;
}

bool RemoteConsole::SendCommand(const char* command, atString& result)
{
	if (m_Stream == NULL)
		return false;

	while ( m_CommandLock.IsLocked() ) { } 

	m_CommandLock.Enter();

	if (m_LogMessages && m_LogMessageCallback.IsBound())
	{
		m_LogMessageCallback(command);
	}

	if ( m_Stream->Write(command, (int) strlen(command)) == -1 )
	{
		m_CommandLock.Exit();
		return false;
	}

	if ( m_Stream->Write("\n", 1) == -1 )
	{
		m_CommandLock.Exit();
		return false;
	}
		
	m_Stream->Flush();

	// wait for and return a response
	result.Reset();
	ReadResponse(m_Stream, result);

	m_CommandLock.Exit();
	return true;
}


bool RemoteConsole::SendCommand(const char* command, const int commandSize)
{
	if (m_Stream == NULL)
		return false;

	while ( m_CommandLock.IsLocked() ) { } 

	m_CommandLock.Enter();

	if ( m_Stream->Write(command, commandSize) == -1 )
	{
		m_CommandLock.Exit();
		return false;
	}

	m_Stream->Flush();
	m_CommandLock.Exit();
	return true;
}

void RemoteConsole::ReadResponse(fiStream* stream, atString& result)
{
	const int bufSize = 256;
	char buf[bufSize];
	int bytesRead = 0;
	bool done = false;
	while(!done)
	{
		int c = stream->FastGetCh();
		if (c < 0)
		{
			buf[bytesRead] = '\0';
			result += buf;
			done = true;
		}
		else
		{
			buf[bytesRead] = (char)c;
			bytesRead++;
			if (bytesRead == bufSize-1)
			{
				buf[bufSize-1] = '\0';
				result += buf;
				bytesRead = 0;
			}
			else if (c == 0)
			{
				result += buf;
				done = true;
			}
		}
	}
}

void RemoteConsole::SendSyncCommand()
{
	atString result;
	SendCommand("sync", result);
}

bool RemoteConsole::WidgetExists(const char* widgetName)
{
	char command[MAX_COMMAND_LENGTH];
	formatf(command, "widget_exists \"%s\"", widgetName);
	atString result;
	if ( SendCommand(command, result) == true )
	{
		 if( stricmp(result.c_str(), "true") == 0)
			 return true;
	}

	return false;
}

bool RemoteConsole::WidgetExistsCallback(const char* widgetName, int timeout)
{
	char command[MAX_COMMAND_LENGTH];
	formatf(command, "widget_exists_callback \"%s\" %d", widgetName, timeout);
	atString result;
	if ( SendCommand(command, result) == true )
	{
		if( stricmp(result.c_str(), "true") == 0)
			return true;
	}

	return false;
}

int RemoteConsole::GetWidgetId(const char* widgetName)
{
	char command[MAX_COMMAND_LENGTH];
	formatf(command, "wgid \"%s\"", widgetName);
	if ( SendCommand(command) == true )
	{
		int widgetId = 0;
		if ( m_Stream->ReadInt(&widgetId, 1) > 0 )
		{
			return widgetId;
		}
	}

	return 0;
}

bool RemoteConsole::PressWidgetButton(const char* widgetName)
{
	char command[MAX_COMMAND_LENGTH];
	formatf(command, "widget \"%s\"", widgetName);
	atString result;
	return SendCommand(command, result);
}

bool RemoteConsole::AddUpdates(const char* bankName)
{
	char command[MAX_COMMAND_LENGTH];
	formatf(command, "getupdates \"%s\" on", bankName);
	atString result;
	return SendCommand(command, result);
}

bool RemoteConsole::RemoveUpdates(const char* bankName)
{
	char command[MAX_COMMAND_LENGTH];
	formatf(command, "getupdates \"%s\" off", bankName);
	atString result;
	return SendCommand(command, result);
}

bool RemoteConsole::IsConnectedToGame()
{
	atString result;
	if ( SendCommand("gameconnected", result) == false )
		return false; //Command failed.
	
	if ( stricmp(result, "true") == 0 )
		return true;
	else
		return false;
}

bool RemoteConsole::GetExecutableName(atString& exeName)
{
	if ( SendCommand("getexecutablename", exeName) == false )
		return false;

	return true;
}

bool RemoteConsole::GetPlatform(atString& platform)
{
	if ( SendCommand("getplatform", platform) == false )
		return false;

	return true;
}

bool RemoteConsole::GetBuildConfig(atString& buildConfig)
{
	if ( SendCommand("getbuildconfig", buildConfig) == false )
		return false;

	return true;
}

bool RemoteConsole::GetGameIP(atString& ip)
{
	if ( SendCommand("getgameip", ip) == false )
		return false;

	return true;
}

bool RemoteConsole::SendWidgetCommand(const char* widgetName, const char* args)
{
	atString dummyResult;
	char command[MAX_COMMAND_LENGTH];
	formatf(command, "widget \"%s\" %s", widgetName, args);
	return SendCommand(command, dummyResult);
}


bool RemoteConsole::SendWidgetCommand(const char* widgetName, const char* args, atString& result)
{
	char command[MAX_COMMAND_LENGTH];
	formatf(command, "widget \"%s\" %s", widgetName, args);
	return SendCommand(command, result);
}

bool RemoteConsole::SendVCRCommand(const char* widgetName, VCRCommand command)
{
	char* commandString = NULL;
	switch(command)
	{
		case VCR_PAUSE:
			commandString = "pause";
			break;
		case VCR_PLAYFORWARDS:
			commandString = "playforwards";
			break;
		case VCR_PLAYBACKWARDS:
			commandString = "playbackwards";
			break;
		case VCR_STEPFORWARDS:
			commandString = "stepforwards";
			break;
		case VCR_STEPBACKWARDS:
			commandString = "stepbackwards";
			break;
		case VCR_REWIND:
			commandString = "rewind";
			break;
		case VCR_FASTFORWARD:
			commandString = "fastforward";
			break;
	}

	if ( commandString == NULL )
	{
		return SendWidgetCommand(widgetName, commandString);
	}

	return true;
}

float RemoteConsole::ReadFloatWidget(const char* widgetName)
{
	atString str;
	if ( SendWidgetCommand(widgetName, "", str) && str.GetLength() != 0 )
	{
		float result;
		result = (float)strtod(str.c_str(), NULL);
		return result;
	}

	return FLT_MAX;
}

int RemoteConsole::ReadIntWidget(const char* widgetName)
{
	atString str;
	if ( SendWidgetCommand(widgetName, "", str) && str.GetLength() != 0)
	{
		int result = strtol(str.c_str(), NULL, 0);
		return result;
	}	

	return INT_MAX;
}

bool RemoteConsole::ReadBoolWidget(const char* widgetName)
{
	atString str;
	SendWidgetCommand(widgetName, "", str);
	if (str.GetLength() != 0 && (str[0] == 't' || str[0] == 'T') )
	{
		return true;
	}

	return false;
}

void RemoteConsole::ReadStringWidget(const char* widgetName, atString& result)
{
	SendWidgetCommand(widgetName, "", result);
}

Vec2V_Out RemoteConsole::ReadVector2Widget(const char* widgetName)
{
	atString str;
	if ( SendWidgetCommand(widgetName, "", str) && str.GetLength() != 0) 
	{
		char* nextChar;
		Vec2V result;
		result.SetXf((float)strtod(str.c_str(), &nextChar));
		result.SetYf((float)strtod(nextChar, &nextChar));
		return result;
	}

	return Vec2V();
}

Vec3V_Out RemoteConsole::ReadVector3Widget(const char* widgetName)
{
	atString str;
	if ( SendWidgetCommand(widgetName, "", str) && str.GetLength() != 0) 
	{
		char* nextChar;
		Vec3V result;
		result.SetXf((float)strtod(str.c_str(), &nextChar));
		result.SetYf((float)strtod(nextChar, &nextChar));
		result.SetZf((float)strtod(nextChar, &nextChar));
		return result;
	}
	
	return Vec3V();
}

Vec4V_Out RemoteConsole::ReadVector4Widget(const char* widgetName)
{
	atString str;
	if ( SendWidgetCommand(widgetName, "", str) && str.GetLength() != 0)
	{
		str.Trim(); // remote trailing w.s. so we can tell if we've only been given 3 components
		char* nextChar;
		Vec4V result(V_ZERO_WONE);
		result.SetXf((float)strtod(str.c_str(), &nextChar));
		result.SetYf((float)strtod(nextChar, &nextChar));
		result.SetZf((float)strtod(nextChar, &nextChar));
		if(*nextChar != '\0')
		{
			result.SetWf((float)strtod(nextChar, &nextChar));
		}
		return result;
	}

	return Vec4V();
}

Mat33V_Out RemoteConsole::ReadMatrix33Widget(const char* widgetName)
{
	atString str;
	if ( SendWidgetCommand(widgetName, "", str) && str.GetLength() != 0)
	{
		char* nextChar = const_cast<char*>(str.c_str());
		Mat33V result;
		for(int i = 0; i < 3; i++)
		{
			for(int j = 0; j < 3; j++)
			{
				result.SetElemf(j, i, (float)strtod(nextChar, &nextChar));
			}
		}
		return result;
	}

	return Mat33V();
}

Mat34V_Out RemoteConsole::ReadMatrix34Widget(const char* widgetName)
{
	atString str;
	if ( SendWidgetCommand(widgetName, "", str) && str.GetLength() != 0) 
	{
		char* nextChar = const_cast<char*>(str.c_str());
		Mat34V result;
		for(int i = 0; i < 4; i++)
		{
			for(int j = 0; j < 3; j++)
			{
				result.SetElemf(j, i, (float)strtod(nextChar, &nextChar));
			}
		}
		return result;
	}

	return Mat34V();
}

Mat44V_Out RemoteConsole::ReadMatrix44Widget(const char* widgetName)
{
	atString str;
	if ( SendWidgetCommand(widgetName, "", str) && str.GetLength() != 0)
	{
		char* nextChar = const_cast<char*>(str.c_str());
		Mat44V result;
		for(int i = 0; i < 4; i++)
		{
			for(int j = 0; j < 4; j++)
			{
				result.SetElemf(j, i, (float)strtod(nextChar, &nextChar));
			}
		}
		return result;
	}

	return Mat44V();
}

float RemoteConsole::ReadFloatWidgetStream(const int widgetId)
{
	char* data;
	if (ReadDataStream(widgetId, data) == true )
	{
		float value;
		memcpy(&value, &data[0], sizeof(float));
		return value;
	}
	return 0.0f;
}

int RemoteConsole::ReadIntWidgetStream(const int widgetId)
{
	char* data;
	
	if (ReadDataStream(widgetId, data) == true )
	{
		int value;
		memcpy(&value, &data[0], sizeof(int));
		return value;
	}
	return 0;
}

bool RemoteConsole::ReadBoolWidgetStream(const int widgetId)
{
	char* data;
	if (ReadDataStream(widgetId, data) == true )
	{
		bool value;
		memcpy(&value, &data[0], sizeof(bool));
		return value;
	}

	return false;
}

void RemoteConsole::ReadStringWidgetStream(const int widgetId, atString& result)
{
	char* data;
	if (ReadDataStream(widgetId, data) == true )
	{
		size_t length = strlen(data);
		result.Set(data, 0, length);
	}
}
Vec2V_Out RemoteConsole::ReadVector2WidgetStream(const int widgetId)
{
	Vec2V result;
	char* data;
	if (ReadDataStream(widgetId, data) == true )
	{
		float value;
		int offset = 0;
		memcpy(&value, &data[offset], sizeof(float));
		offset += sizeof(float);
		result.SetXf(value);

		memcpy(&value, &data[offset], sizeof(float));
		offset += sizeof(float);
		result.SetYf(value);
	}

	return result;
}

Vec3V_Out RemoteConsole::ReadVector3WidgetStream(const int widgetId)
{
	Vec3V result;
	char* data;
	if (ReadDataStream(widgetId, data) == true )
	{
		float value;
		int offset = 0;
		memcpy(&value, &data[offset], sizeof(float));
		offset += sizeof(float);
		result.SetXf(value);

		memcpy(&value, &data[offset], sizeof(float));
		offset += sizeof(float);
		result.SetYf(value);

		memcpy(&value, &data[offset], sizeof(float));
		offset += sizeof(float);
		result.SetZf(value);
	}

	return result;
}

Vec4V_Out RemoteConsole::ReadVector4WidgetStream(const int widgetId)
{
	Vec4V result;
	char* data;
	if (ReadDataStream(widgetId, data) == true )
	{
		float value;
		int offset = 0;

		memcpy(&value, &data[offset], sizeof(float));
		offset += sizeof(float);
		result.SetXf(value);

		memcpy(&value, &data[offset], sizeof(float));
		offset += sizeof(float);
		result.SetYf(value);

		memcpy(&value, &data[offset], sizeof(float));
		offset += sizeof(float);
		result.SetZf(value);

		memcpy(&value, &data[offset], sizeof(float));
		offset += sizeof(float);
		result.SetWf(value);
	}

	return result;
}
Mat33V_Out RemoteConsole::ReadMatrix33WidgetStream(const int widgetId)
{
	Mat33V result;
	char* data;
	if (ReadDataStream(widgetId, data) == true )
	{
		float value;
		int offset = 0;
		for(int i = 0; i < 3; i++)
		{
			for(int j = 0; j < 3; j++)
			{
				memcpy(&value, &data[offset], sizeof(float));
				offset += sizeof(float);

				result.SetElemf(j, i, value);
			}
		}
	}

	return result;
}

Mat34V_Out RemoteConsole::ReadMatrix34WidgetStream(const int widgetId)
{
	Mat34V result;
	char* data;
	if (ReadDataStream(widgetId, data) == true )
	{
		float value;
		int offset = 0;
		for(int i = 0; i < 4; i++)
		{
			for(int j = 0; j < 3; j++)
			{
				memcpy(&value, &data[offset], sizeof(float));
				offset += sizeof(float);

				result.SetElemf(j, i, value);
			}
		}
	}

	return result;
}

Mat44V_Out RemoteConsole::ReadMatrix44WidgetStream(const int widgetId)
{
	Mat44V result;
	char* data;
	if (ReadDataStream(widgetId, data) == true )
	{
		float value;
		int offset = 0;
		for(int i = 0; i < 4; i++)
		{
			for(int j = 0; j < 4; j++)
			{
				memcpy(&value, &data[offset], sizeof(float));
				offset += sizeof(float);

				result.SetElemf(j, i, value);
			}
		}

	}

	return result;
}

bool RemoteConsole::ReadDataStream(const int widgetId, char*& data)
{
	if (m_Stream == NULL)
		return false;

	static char bkDataIdentifier[] = { 'b', 'k', 'd', 'r' };

	//Size of the header plus the size of an integer (for the actual command's size), the widget
	int totalSize = sizeof(bkDataIdentifier) + sizeof(int);

	int offset = 0;
	char* commandBuffer = m_DataBuffer;
	if ( totalSize > DATA_BUFFER_SIZE)
		commandBuffer = new char[totalSize];

	memcpy(&commandBuffer[offset], bkDataIdentifier, sizeof(bkDataIdentifier));
	offset += sizeof(bkDataIdentifier);

	memcpy(&commandBuffer[offset], &widgetId, sizeof(int));
	offset += sizeof(int);

	atString result;
	bool commandResult = SendCommand(commandBuffer, offset) ;

	if ( commandBuffer != m_DataBuffer )
		delete commandBuffer;
	
	if ( commandResult == false )  //Send the request to get data.
		return false;

	while ( m_CommandLock.IsLocked() ) { } 

	m_CommandLock.Enter();

	int dataSize = -1;
	if ( m_Stream->ReadInt(&dataSize, 1) > 0 )
	{
		if ( dataSize > 0 )
		{
			data = new char[dataSize];
			if ( m_Stream->ReadByte(data, dataSize) == dataSize )
			{
				m_CommandLock.Exit();
				return true;
			}
		}
	}

	m_CommandLock.Exit();
	return false;
}

void RemoteConsole::WriteFloatWidget(const char* widgetName, float f)
{
	char valStr[32];
	formatf(valStr, "%f", f);
	atString dummyResult;
	SendWidgetCommand(widgetName, valStr, dummyResult);
}

void RemoteConsole::WriteIntWidget(const char* widgetName, int i)
{
	char valStr[32];
	formatf(valStr, "%d", i);
	atString dummyResult;
	SendWidgetCommand(widgetName, valStr, dummyResult);
}

void RemoteConsole::WriteBoolWidget(const char* widgetName, bool b)
{
	atString dummyResult;
	SendWidgetCommand(widgetName, b ? "true" : "false", dummyResult);
}

void RemoteConsole::WriteStringWidget(const char* widgetName, const char* newValue)
{
	char command[MAX_COMMAND_LENGTH];
	formatf(command, "widget \"%s\" \"%s\"", widgetName, newValue);
	atString result;
	SendCommand(command, result);
}

void RemoteConsole::WriteVector2Widget(const char* widgetName, Vec2V_In newValue)
{
	char command[MAX_COMMAND_LENGTH];
	formatf(command, "widget \"%s\" %f %f", widgetName, newValue.GetXf(), newValue.GetYf());
	atString result;
	SendCommand(command, result);
}

void RemoteConsole::WriteVector3Widget(const char* widgetName, Vec3V_In newValue)
{
	char command[MAX_COMMAND_LENGTH];
	formatf(command, "widget \"%s\" %f %f %f", widgetName, newValue.GetXf(), newValue.GetYf(), newValue.GetZf());
	atString result;
	SendCommand(command, result);
}

void RemoteConsole::WriteVector4Widget(const char* widgetName, Vec4V_In newValue)
{
	char command[MAX_COMMAND_LENGTH];
	formatf(command, "widget \"%s\" %f %f %f %f", widgetName, newValue.GetXf(), newValue.GetYf(), newValue.GetZf(), newValue.GetWf());
	atString result;
	SendCommand(command, result);
}

void RemoteConsole::WriteMatrix33Widget(const char* widgetName, Mat33V_In newValue)
{
	char command[MAX_COMMAND_LENGTH];
	formatf(command, "widget \"%s\" %f %f %f %f %f %f %f %f %f", widgetName, 
		newValue.GetElemf(0, 0), newValue.GetElemf(1, 0), newValue.GetElemf(2, 0),
		newValue.GetElemf(0, 1), newValue.GetElemf(1, 1), newValue.GetElemf(2, 1),
		newValue.GetElemf(0, 2), newValue.GetElemf(1, 2), newValue.GetElemf(2, 2));
	atString result;
	SendCommand(command, result);
}

void RemoteConsole::WriteMatrix34Widget(const char* widgetName, Mat34V_In newValue)
{
	char command[MAX_COMMAND_LENGTH];
	formatf(command, "widget \"%s\" %f %f %f %f %f %f %f %f %f %f %f %f", widgetName, 
		newValue.GetElemf(0, 0), newValue.GetElemf(1, 0), newValue.GetElemf(2, 0),
		newValue.GetElemf(0, 1), newValue.GetElemf(1, 1), newValue.GetElemf(2, 1),
		newValue.GetElemf(0, 2), newValue.GetElemf(1, 2), newValue.GetElemf(2, 2),
		newValue.GetElemf(0, 3), newValue.GetElemf(1, 3), newValue.GetElemf(2, 3));
	atString result;
	SendCommand(command, result);
}

void RemoteConsole::WriteMatrix44Widget(const char* widgetName, Mat44V_In newValue)
{
	char command[MAX_COMMAND_LENGTH];
	formatf(command, "widget \"%s\" %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f", widgetName, 
		newValue.GetElemf(0, 0), newValue.GetElemf(1, 0), newValue.GetElemf(2, 0), newValue.GetElemf(3, 0),
		newValue.GetElemf(0, 1), newValue.GetElemf(1, 1), newValue.GetElemf(2, 1), newValue.GetElemf(3, 1),
		newValue.GetElemf(0, 2), newValue.GetElemf(1, 2), newValue.GetElemf(2, 2), newValue.GetElemf(3, 2),
		newValue.GetElemf(0, 3), newValue.GetElemf(1, 3), newValue.GetElemf(2, 3), newValue.GetElemf(3, 3));
	atString result;
	SendCommand(command, result);
}

void RemoteConsole::WriteDataStream(const int widgetId, const void* data, int dataSize)
{
	static char bkDataIdentifier[] = { 'b', 'k', 'd', 'w' };

	int totalSize = sizeof(bkDataIdentifier) + sizeof(int) + sizeof(int) + dataSize;

	int offset = 0;
	char* commandBuffer = m_DataBuffer;
	if ( totalSize > DATA_BUFFER_SIZE )
		commandBuffer = new char[totalSize];

	memcpy(&commandBuffer[offset], bkDataIdentifier, sizeof(bkDataIdentifier));
	offset += sizeof(bkDataIdentifier);

	memcpy(&commandBuffer[offset], &dataSize, sizeof(int));
	offset += sizeof(int);

	memcpy(&commandBuffer[offset], &widgetId, sizeof(int));
	offset += sizeof(int);

	memcpy(&commandBuffer[offset], data, dataSize);
	offset += dataSize;

	SendCommand(commandBuffer, offset);
	
	if (commandBuffer != m_DataBuffer)
		delete commandBuffer;
}


void RemoteConsole::WriteFloatWidgetStream(const int widgetId, float f)
{
	WriteDataStream(widgetId, &f, sizeof(float) );
}

void RemoteConsole::WriteIntWidgetStream(const int widgetId, int i)
{
	WriteDataStream(widgetId, &i, sizeof(int));
}

void RemoteConsole::WriteBoolWidgetStream(const int widgetId, bool b)
{
	WriteDataStream(widgetId, &b, sizeof(bool));
} 

void RemoteConsole::WriteStringWidgetStream(const int widgetId, const char* newValue)
{
	size_t length = strlen(newValue);
	WriteDataStream(widgetId, (void*) newValue, length);
}

void RemoteConsole::WriteVector2WidgetStream(const int widgetId, Vec2V_In newValue)
{
	WriteDataStream(widgetId, (float*) &newValue, 2 * sizeof(float));
}

void RemoteConsole::WriteVector3WidgetStream(const int widgetId, Vec3V_In newValue)
{
	WriteDataStream(widgetId, (float*) &newValue, 3 * sizeof(float));
}

void RemoteConsole::WriteVector4WidgetStream(const int widgetId, Vec4V_In newValue)
{
	WriteDataStream(widgetId, (float*) &newValue, 4 * sizeof(float));
}

void RemoteConsole::WriteMatrix33WidgetStream(const int widgetId, Mat33V_In newValue)
{
	WriteDataStream(widgetId, (float*) &newValue, 9 * sizeof(float));
}

void RemoteConsole::WriteMatrix34WidgetStream(const int widgetId, Mat34V_In newValue)
{
	WriteDataStream(widgetId, (float*) &newValue, 12 * sizeof(float));
}

void RemoteConsole::WriteMatrix44WidgetStream(const int widgetId, Mat44V_In newValue)
{
	WriteDataStream(widgetId, (float*) &newValue, 16 * sizeof(float));
}


void RemoteConsole::WriteFloatWidget(const char* widgetName, float f, atString& result)
{
	char valStr[32];
	formatf(valStr, "%f", f);
	SendWidgetCommand(widgetName, valStr, result);
}

void RemoteConsole::WriteIntWidget(const char* widgetName, int i, atString& result)
{
	char valStr[32];
	formatf(valStr, "%d", i);
	SendWidgetCommand(widgetName, valStr, result);
}

void RemoteConsole::WriteBoolWidget(const char* widgetName, bool b, atString& result)
{
	SendWidgetCommand(widgetName, b ? "true" : "false", result);
}

void RemoteConsole::WriteStringWidget(const char* widgetName, const char* newValue, atString& result)
{
	char command[MAX_COMMAND_LENGTH];
	formatf(command, "widget \"%s\" \"%s\"", widgetName, newValue);
	SendCommand(command, result);
}

void RemoteConsole::WriteVector3Widget(const char* widgetName, Vec3V_In newValue, atString& result)
{
	char command[MAX_COMMAND_LENGTH];
	formatf(command, "widget \"%s\" %f %f %f", widgetName, newValue.GetXf(), newValue.GetYf(), newValue.GetZf());
	SendCommand(command, result);
}

void RemoteConsole::WriteVector4Widget(const char* widgetName, Vec4V_In newValue, atString& result)
{
	char command[MAX_COMMAND_LENGTH];
	formatf(command, "widget \"%s\" %f %f %f %f", widgetName, newValue.GetXf(), newValue.GetYf(), newValue.GetZf(), newValue.GetWf());
	SendCommand(command, result);
}

void RemoteConsole::ListWidgets(const char* parentDir, atArray<RemoteConsole::WidgetEntry>& resultArray)
{
	char command[MAX_COMMAND_LENGTH];
	formatf(command, "dir -t \"%s\"", parentDir);
	atString result;
	SendCommand(command, result);

	// parse out result. Format is:
	// widgetName  \t  typeCode
	atArray<RemoteConsole::WidgetEntry> items;

	atArray<atString> lines;
	result.Split(lines, '\n', true);

	resultArray.Reset();
	resultArray.Reserve(lines.GetCount());

	for(int i = 0; i < lines.GetCount(); i++)
	{
		atString name, typecode;
		lines[i].Split(name, typecode, "\t");

		RemoteConsole::WidgetEntry& entry = resultArray.Append();
		entry.m_Name = name;
		entry.m_RawType = BKGUID(typecode[0], typecode[1], typecode[2], typecode[3]);

		entry.m_EffectiveType = FindEffectiveType(entry.m_RawType);
	}
}

RemoteConsole::EffectiveType RemoteConsole::FindEffectiveType(u32 rawType)
{
	switch(rawType)
	{
	case BKGUID('b','a','n','k'):
	case BKGUID('b','m','g','r'):
	case BKGUID('g','r','u','p'):
		return TYPE_GROUP;

	case BKGUID('b','u','t','n'):
		return TYPE_BUTTON;

	case BKGUID('a','n','g','l'):
	case BKGUID('s','l','f','l'):
		return TYPE_FLOAT;

	case BKGUID('s','l','u','8'):
	case BKGUID('s','l','s','8'):
	case BKGUID('s','l','u','1'):
	case BKGUID('s','l','s','1'):
	case BKGUID('s','l','u','3'):
	case BKGUID('s','l','s','3'):
		return TYPE_INT;

	case BKGUID('t','g','b','o'):
	case BKGUID('t','g','s','3'):
	case BKGUID('t','g','u','8'):
	case BKGUID('t','g','u','1'):
	case BKGUID('t','g','u','3'):
	case BKGUID('t','g','b','s'):
		return TYPE_BOOL;

	case BKGUID('i','m','g','v'):
	case BKGUID('t','e','x','t'):
	case BKGUID('c','o','u','8'):
	case BKGUID('c','o','s','8'):
	case BKGUID('c','o','s','1'):
	case BKGUID('c','o','s','3'):
		return TYPE_STRING;

	case BKGUID( 'v', 'e', 'c', '2' ):
		return TYPE_VECTOR2;

	case BKGUID( 'v', 'e', 'c', '3' ):
		return  TYPE_VECTOR3;

	case BKGUID( 'v', 'e', 'c', '4' ):
	case BKGUID('v','c','l','r'): // treat all color widgets as vector4s for now (need to handle only receiving 3 elements sometimes though. ReadVector4Widget does this)
		return TYPE_VECTOR4;

	case  BKGUID( 'm', 't', 'x', '9' ):
		return TYPE_MATRIX33;

	case BKGUID( 'm', 't', 'x', '2' ):
		return TYPE_MATRIX34;

	case BKGUID( 'm', 't', 'x', '6' ):
		return TYPE_MATRIX44;

		// don't have representations for these types (i.e. they are 'known unknowns')
	case BKGUID('i','n','p','t'):
	case BKGUID('s','e','p','r'):
	case BKGUID('d','a','t','a'):
	case BKGUID('l','i','s','t'):
	case BKGUID('t','i','t','l'):
	case BKGUID('t','l','s','t'):
	case BKGUID('n','u','l','l'):
		return TYPE_UNKNOWN;
	}
	return TYPE_UNKNOWN;
}

//////////////////////////////////////////////////////////////////////////
///Helper Functions for reading from and writing to a buffer.
//////////////////////////////////////////////////////////////////////////
void RemoteConsole::ReadStream(bool& data, char* buffer)
{
	memcpy(&data, buffer, sizeof(bool));

	if (m_SwapEndianness)
	{
		data = sysEndian::Swap((u8)data) != 0 ? true : false;
	}
}

void RemoteConsole::ReadStream(int& data, char* buffer)
{
	memcpy(&data, buffer, sizeof(int));

	if (m_SwapEndianness)
	{
		sysEndian::SwapMe(data);
	}
}

void RemoteConsole::ReadStream(float& data, char* buffer)
{
	memcpy(&data, buffer, sizeof(float));

	if (m_SwapEndianness)
	{
		sysEndian::SwapMe(data);
	}
}

void RemoteConsole::WriteStream(bool& data, datGrowBuffer& buffer)
{
	if (m_SwapEndianness)
	{
		u8 swap = sysEndian::Swap((u8)data);
		buffer.Append((void*) &swap, sizeof(bool));  
	}
	else
	{
		buffer.Append((void*) &data, sizeof(bool));  
	}
}

void RemoteConsole::WriteStream(int& data, datGrowBuffer& buffer)
{
	if (m_SwapEndianness)
	{
		u32 swap = sysEndian::Swap((u32)data);
		buffer.Append((void*) &swap, sizeof(int));  
	}
	else
	{
		buffer.Append((void*) &data, sizeof(int));  
	}
}

void RemoteConsole::WriteStream(float& data, datGrowBuffer& buffer)
{
	if (m_SwapEndianness)
	{
		sysEndian::SwapMe(data);
		buffer.Append((void*) &data, sizeof(float));  
	}
	else
	{
		buffer.Append((void*) &data, sizeof(float));  
	}
}

int RemoteConsole::GetConsoleProxyPort(const char* hostname)
{
	int port = m_DefaultPort;

	char streamName[RAGE_MAX_PATH];
	formatf(streamName, "tcpip:%d:%s", m_ConnectionListPort, hostname);

	fiStream* stream = ASSET.Open(streamName, "");
	if (stream != NULL)
	{
		atString query("{\"action\":\"getgamelist\"}");

		if ( stream->Write(query.c_str(), (int) query.length()) != -1 )
		{
			stream->Flush();

			// wait for and return a response
			atString result;
			result.Reset();
			ReadResponse(stream, result);

			// Now to parse the returned JSON.
			JSONNODE* rootNode = json_parse(result.c_str());
			JSONNODE* hubsNode = json_get(rootNode, "hubs");
			if (hubsNode != NULL && json_type(hubsNode) == JSON_ARRAY)
			{
				JSONNODE_ITERATOR hubsIterator = json_begin(hubsNode);
				while (hubsIterator != json_end(hubsNode))
				{
					if (json_type(*hubsIterator) == JSON_NODE)
					{
						// Attempt to get the "default" node and see whether it's set to true.
						JSONNODE* defaultNode = json_get(*hubsIterator, "default");
						if (defaultNode != NULL && json_type(defaultNode) == JSON_BOOL && json_as_bool(defaultNode) == (json_bool_t)true)
						{
							// We've found the default hub so extract the console port from it.
							JSONNODE* consolePortNode = json_get(*hubsIterator, "consoleport");
							if (consolePortNode != NULL && json_type(consolePortNode) == JSON_NUMBER)
							{
								port = json_as_int(consolePortNode);
								break;
							}
						}
					}

					++hubsIterator;
				}
			}
			json_delete(rootNode);
		}

		// Close off the stream.
		stream->Close();
		stream = NULL;
	}

	return port;
}
