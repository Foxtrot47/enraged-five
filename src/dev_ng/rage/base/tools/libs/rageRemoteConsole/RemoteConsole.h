// 
// rageRemoteConsole\RemoteConsole.h 
// 
// Copyright (C) Rockstar Games.  All Rights Reserved. 
// 

#ifndef _REMOTECONSOLE_H
#define _REMOTECONSOLE_H

#include <string>

#include "atl/array.h"
#include "atl/string.h"
#include "data/growbuffer.h"
#include "atl/delegate.h"
#include "file/handle.h"
#include "system/criticalsection.h"
#include "vectormath/vec2v.h"
#include "vectormath/vec3v.h"
#include "vectormath/vec4v.h"
#include "vectormath/mat33v.h"
#include "vectormath/mat34v.h"
#include "vectormath/mat44v.h"

#define MAX_COMMAND_LENGTH 512

namespace rage
{

	class fiStream;
	class atString;

	// Contains convenience functions for interacting with a remote rag console
	class RemoteConsole
	{

	public:
		RemoteConsole();
		~RemoteConsole();

		enum EffectiveType // Not the _Actual_ type of the widgets, but for tool purposes you can treat it as this type
		{
			TYPE_UNKNOWN,
			TYPE_GROUP,
			TYPE_BUTTON,
			TYPE_FLOAT,
			TYPE_INT,
			TYPE_BOOL,
			TYPE_STRING,
			TYPE_VECTOR2,
			TYPE_VECTOR3,
			TYPE_VECTOR4,
			TYPE_MATRIX33,
			TYPE_MATRIX34,
			TYPE_MATRIX44,
		};

		enum VCRCommand 
		{
			VCR_PAUSE,
			VCR_PLAYFORWARDS,
			VCR_PLAYBACKWARDS,
			VCR_STEPFORWARDS,
			VCR_STEPBACKWARDS,
			VCR_REWIND,
			VCR_FASTFORWARD,
		};

		// PURPOSE: Connects to a remote console (i.e. connects to an instance of rag somewhere)
		// RETURNS: true if the connection was established
		bool	Connect();
		bool	Connect(std::string hostname);
		bool	Connect(std::string hostname, int port);
		bool	Connect(const char* hostname, int port);

		// PURPOSE:  Disconnects from the remote console if a connection is active.
		void	Disconnect(); 

		// PURPOSE: Polls the connection to determine it's current state.
		bool	IsConnected();

		// PURPOSE: Toggles whether RAG proxy commands get logged.
		void	LogMessages(bool enable);

		// PURPOSE: Sends a command to the remote console.  The command must be 
		// registered by the opposite end as valid.
		// PARAMS:
		//		command - The command to execute
		//		result - An atString that will contain the result data
		bool	SendCommand(const char* command);
		bool	SendCommand(const char* command, atString& result);
		bool	SendCommand(const char* command, const int commandSize);
		void 	ReadResponse(fiStream* stream, atString& result);

		// PURPOSE:  Sends a widget command with the specified arguments.
		// PARAMS:
		//		widgetName - the widget path
		//		args - list of arguments; can be NULL.
		bool SendWidgetCommand(const char* widgetName, const char* args);
		bool SendWidgetCommand(const char* widgetName, const char* args, atString& result);

		// PURPOSE:  Sends a VCR command to the given widget.  
		// PARAMS:
		//		widgetName = the widget path to edit
		//		command - enumeration for type of command.
		bool SendVCRCommand(const char* widgetName, VCRCommand command);

		// PURPOSE:  Specialized synchronization command to allow for all currently
		// pending commands to be processed.
		void	SendSyncCommand();

		// PURPOSE: Returns whether the current widget is accessible.
		// Widgets are accessible if it has been created and its path is not ambiguous.
		bool	WidgetExists(const char* widgetName);

		// PURPOSE: Returns when the target widget is now accessible, or when the command
		// has exceeded the specified timeout. 
		// The timeout is in milliseconds.
		bool	WidgetExistsCallback(const char* widgetName, int timeout);

		// PURPOSE: Acquires the widget id from the server.  0 if it does not exist.
		int		GetWidgetId(const char* widgetName);

		//PURPOSE: Simulates a button press for the particular widget.
		bool	PressWidgetButton(const char* widgetName);

		// PURPOSE:  Allows updates to be acquired for the bank.
		bool	AddUpdates(const char* bankName);

		//PURPOSE: Removes any updates being sent from this bank.
		bool	RemoveUpdates(const char* bankName);

		// PURPOSE:  Sends a command to the proxy, requesting status of the game connection.
		// This command is different than IsConnected -- because that tests connection to the proxy.
		bool	IsConnectedToGame();

		//PURPOSE: Acquires a string containing the executable name of the game currently connected to.
		bool	GetExecutableName(atString& exeName);

		//PURPOSE: Acquires a string containing the platform currently connected to.
		bool	GetPlatform(atString& platform);

		//PURPOSE: Acquires a string containing the build config for the game we are currently connected to.
		bool	GetBuildConfig(atString& buildConfig);

		//PURPOSE: Acquires a string containing the ip currently connected to.
		bool	GetGameIP(atString& ip);

		struct WidgetEntry
		{
			atString		m_Name;
			u32				m_RawType;
			EffectiveType	m_EffectiveType;
		};

		// PURPOSE: Gets a list of widgets from the remote rag instance
		// PARAMS:
		//		parentDir - The parent directory. Use "/" for the root dir.
		//		resultArray - Array that will contain all the resulting widgets
		void	ListWidgets(const char* parentDir, atArray<WidgetEntry>& resultArray);

		// PURPOSE: Reads a widget value.
		// NOTES: Does minimal error checking so it's a good idea to make sure your widget is of the right type (via
		//			ListWidgets) sometime before calling a read function.
		float		ReadFloatWidget(const char* widgetName);
		int			ReadIntWidget(const char* widgetName);
		bool		ReadBoolWidget(const char* widgetName);
		void		ReadStringWidget(const char* widgetName, atString& result);
		Vec2V_Out	ReadVector2Widget(const char* widgetName);
		Vec3V_Out	ReadVector3Widget(const char* widgetName);
		Vec4V_Out	ReadVector4Widget(const char* widgetName);
		Mat33V_Out	ReadMatrix33Widget(const char* widgetName);
		Mat34V_Out	ReadMatrix34Widget(const char* widgetName);
		Mat44V_Out	ReadMatrix44Widget(const char* widgetName);

		// PURPOSE: Reads a widget value.
		// NOTES: Reads from a stream.
		float		ReadFloatWidgetStream(const int widgetId);
		int			ReadIntWidgetStream(const int widgetId);
		bool		ReadBoolWidgetStream(const int widgetId);
		void		ReadStringWidgetStream(const int widgetId, atString& result);
		Vec2V_Out	ReadVector2WidgetStream(const int widgetId);
		Vec3V_Out	ReadVector3WidgetStream(const int widgetId);
		Vec4V_Out	ReadVector4WidgetStream(const int widgetId);
		Mat33V_Out	ReadMatrix33WidgetStream(const int widgetId);
		Mat34V_Out	ReadMatrix34WidgetStream(const int widgetId);
		Mat44V_Out	ReadMatrix44WidgetStream(const int widgetId);
		bool		ReadDataStream(const int widgetId, char*& data);

		// PURPOSE: Writes a widget value.
		// NOTES: Does minimal error checking so it's a good idea to make sure your widget is of the right type (via
		//			ListWidgets) sometime before calling a write function.
		void	WriteFloatWidget(const char* widgetName, float f);
		void	WriteIntWidget(const char* widgetName, int i);
		void	WriteBoolWidget(const char* widgetName, bool b);
		void	WriteStringWidget(const char* widgetName, const char* newValue);
		void	WriteVector2Widget(const char* widgetName, Vec2V_In newValue);
		void	WriteVector3Widget(const char* widgetName, Vec3V_In newValue);
		void	WriteVector4Widget(const char* widgetName, Vec4V_In newValue);
		void	WriteMatrix33Widget(const char* widgetName, Mat33V_In newValue);
		void	WriteMatrix34Widget(const char* widgetName, Mat34V_In newValue);
		void	WriteMatrix44Widget(const char* widgetName, Mat44V_In newValue);
		

		// PURPOSE: Writes a widget value.
		// NOTES: Uses the binary stream protocol instead of the 
		void	WriteDataStream(const int widgetId, const void* buffer, int bufferSize);
		void	WriteFloatWidgetStream(const int widgetId, float f);
		void	WriteIntWidgetStream(const int widgetId, int i);
		void	WriteBoolWidgetStream(const int widgetId, bool b);
		void	WriteStringWidgetStream(const int widgetId, const char* newValue);
		void	WriteVector2WidgetStream(const int widgetId, Vec2V_In newValue);
		void	WriteVector3WidgetStream(const int widgetId, Vec3V_In newValue);
		void	WriteVector4WidgetStream(const int widgetId, Vec4V_In newValue);
		void	WriteMatrix33WidgetStream(const int widgetId, Mat33V_In newValue);
		void	WriteMatrix34WidgetStream(const int widgetId, Mat34V_In newValue);
		void	WriteMatrix44WidgetStream(const int widgetId, Mat44V_In newValue);

		// PURPOSE: Writes a widget value.
		// NOTES: Returns results of the operations in result
		// These are likely unused.
		void	WriteFloatWidget(const char* widgetName, float f, atString& result);
		void	WriteIntWidget(const char* widgetName, int i, atString& result);
		void	WriteBoolWidget(const char* widgetName, bool b, atString& result);
		void	WriteStringWidget(const char* widgetName, const char* newValue, atString& result);
		void	WriteVector3Widget(const char* widgetName, Vec3V_In newValue, atString& result);
		void	WriteVector4Widget(const char* widgetName, Vec4V_In newValue, atString& result);

		typedef atDelegate<void (const char*)> LogMessageCallback;
		void SetLogMessageCallback(LogMessageCallback callback);

		// PURPOSE:  Helper functions to write to a data stream.
		static void SwapEndianness(const bool endian) { m_SwapEndianness = endian; }

		static void ReadStream(bool& data, char* buffer);
		static void ReadStream(int& data, char* buffer);
		static void ReadStream(float& data, char* buffer);
		static void WriteStream(bool& data, datGrowBuffer& buffer);
		static void	WriteStream(int& data, datGrowBuffer& buffer);
		static void	WriteStream(float& data, datGrowBuffer& buffer);

	protected:
		int GetConsoleProxyPort(const char* hostname);

	protected:
		fiStream* m_Stream;
		sysCriticalSection m_CommandLock;
		sysCriticalSectionToken m_CommandLockToken;
		bool m_LogMessages;
		LogMessageCallback m_LogMessageCallback;

		static EffectiveType FindEffectiveType(u32 rawType);

		static char* m_LocalHost;
		static int m_DefaultPort;
		static int m_ConnectionListPort;
		static char* m_PingMessage;
		static bool m_SwapEndianness;
		#define DATA_BUFFER_SIZE 4096
		static char m_DataBuffer[DATA_BUFFER_SIZE];
	};

} // namespace rage

#endif //_REMOTECONSOLE_H
