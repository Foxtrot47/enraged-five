// 
// rageShaderViewer/lightData.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
//

#ifndef	RAGE_SHADER_VIEWER_LIGHT_DATA_H
#define	RAGE_SHADER_VIEWER_LIGHT_DATA_H



#include	"grcore/light.h"
#include	"rmptfx/keyframe.h"
#include	"vector/vector4.h"
#include	"vector/matrix34.h"

namespace rage {

class	lightData
{
public:

	lightData();
	~lightData();

	// Accessors
	grcLightGroup::grcLightType GetType()			const {return m_Type;}
	const Vector4 &				GetAmbientColor()	const {return m_vAmbientColor;}
	const Vector4 &				GetLightColor()		const {return m_vLightColor;}
	const Matrix34 &			GetMatrix()			const {return m_mMatrix;}

	// Initalize the light
	bool	LoadFromXML(grcLightGroup::grcLightType type, const char * szLightFileName);

	// Set the light data
	void	SetLightData(const float fTimeOfDay);

private:
	ptxKeyframe					m_KeyColor;
	ptxKeyframe					m_KeyAmbLight;
	ptxKeyframe					m_KeyOrbit;
	ptxKeyframe					m_KeyDistance;
	grcLightGroup::grcLightType m_Type;
	Vector4						m_vAmbientColor;
	Vector4						m_vLightColor;
	Matrix34					m_mMatrix;
};

} // end namespace rage



#endif
