// 
// rageShaderViewer/lightDataManager.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
//

#include "grcore/light.h"

#include "lightData.h"
#include "lightDataManager.h"

using namespace rage;

lightDataManager * lightDataManager::sm_pInstance = NULL;

lightDataManager::lightDataManager()
{
	sm_pInstance		= this;
	m_bUseDefaultLight	= true;
	m_bCycleTimeOfDay   = false;
	m_fCycleSpeed		= 0.1f;
	m_fTimeOfDay		= 10.0f;
	m_pLights			= NULL;
}

lightDataManager::~lightDataManager()
{
	sm_pInstance = NULL;

	for (int i=0; i<m_apData.GetCount(); i++)
	{
		delete m_apData[i];
		m_apData[i] = NULL;
	}
	m_apData.Reset();

	delete m_pLights;
	m_pLights = NULL;
}

bool lightDataManager::Init(const char * szLightsFileName)
{
	// Make sure we have a valid file
	if (szLightsFileName)
	{
		fiStream * streamLights = fiStream::Open(szLightsFileName);
		if (streamLights)
		{
			fiTokenizer T(szLightsFileName, streamLights);

			char szType[128];
			char szLightFileName[256];

			while (T.GetToken(szType, sizeof(szType)))
			{
				T.GetToken(szLightFileName, sizeof(szLightFileName));

				grcLightGroup::grcLightType type = grcLightGroup::LTTYPE_DIR;
				if (stricmp(szType, "dir")==0)
				{
					type = grcLightGroup::LTTYPE_DIR;
				}
				else if (stricmp(szType, "point")==0)
				{
					type = grcLightGroup::LTTYPE_POINT;
				}
				else
				{
					Warningf("Could not find light type %s", szType);
					continue;
				}

				lightData * p = new lightData;
				if (p && p->LoadFromXML(type, szLightFileName))
				{
					m_apData.Grow() = p;
				}
			}

			streamLights->Close();
		
		}
	}

	m_pLights = new grcLightGroup;

	if (m_apData.GetCount() > 0)
	{
		m_bUseDefaultLight = false;
	}

	if (m_bUseDefaultLight)
	{
		m_pLights->SetActiveCount(3);
		m_pLights->SetAmbient(0.05f, 0.05f, 0.05f, 1.0f);

		// Camera light
		m_pLights->SetLightType(0, grcLightGroup::LTTYPE_DIR);
		m_pLights->SetDirection(0, Vector3(0.0f, -1.0f, 0.0f));
		m_pLights->SetColor(0, 0.9f, 0.9f, 0.9f);	

		// Two other lights
		m_pLights->SetLightType(1, grcLightGroup::LTTYPE_POINT);
		m_pLights->SetPosition(1, Vector3(0.0f, 10.0f, 0.0f));
		m_pLights->SetDirection(1, Vector3(0.0f, 1.0f, 0.0f));
		m_pLights->SetColor(1, 0.2f, 0.2f, 0.2f);	

		m_pLights->SetLightType(2, grcLightGroup::LTTYPE_DIR);
		m_pLights->SetDirection(2, Vector3(1.0f, 0.0f, 0.0f));
		m_pLights->SetColor(2, 0.2f, 0.2f, 0.2f);	
	}
	else
	{
		m_pLights->SetActiveCount(m_apData.GetCount());
		Update(M34_IDENTITY);
	}

	

	return true;
}

Vector3 lightDataManager::GetDefaultAmbient() const
{
	Vector3 v;
	m_pLights->GetAmbient().GetVector3(v);
	return v;
}

Vector3 lightDataManager::GetDefaultDiffuse() const
{
	return m_pLights->GetColor(0);
}

void lightDataManager::SetDefaultAmbient(const Vector3 & c)
{
	m_pLights->SetAmbient(c.x, c.y, c.z, 1.0f);
}

void lightDataManager::SetDefaultDiffuse(const Vector3 & c)
{
	m_pLights->SetColor(0, c);
}

void lightDataManager::Update(const Matrix34 & CameraMatrix)
{
	if (!m_bUseDefaultLight)
	{
		for (int i=0; i<m_apData.GetCount(); i++)
		{
			m_apData[i]->SetLightData(m_fTimeOfDay);

			m_pLights->SetLightType(i, m_apData[i]->GetType());
			m_pLights->SetDirection(i, m_apData[i]->GetMatrix().c);
			m_pLights->SetPosition(i, m_apData[i]->GetMatrix().d);
			m_pLights->SetColor(i, m_apData[i]->GetLightColor().x, m_apData[i]->GetLightColor().y, m_apData[i]->GetLightColor().z);
			m_pLights->SetIntensity(i, m_apData[i]->GetLightColor().w);
		}

		if (m_bCycleTimeOfDay)
		{
			m_fTimeOfDay += m_fCycleSpeed;
			if (m_fTimeOfDay >= 24.0f)
			{
				m_fTimeOfDay = 0.0f;
			}
		}
	}
	else
	{	
		Vector3 vFrom;
		Vector3 vTo;
		Vector3 vLookAt;
		CameraMatrix.GetLookAt(&vFrom, &vTo);

		vLookAt = vFrom - vTo;
		vLookAt.Normalize();

		m_pLights->SetLightType(0, grcLightGroup::LTTYPE_DIR);
		m_pLights->SetDirection(0, vLookAt);
		m_pLights->SetColor(0, 1.0f, 1.0f, 1.0f);
		m_pLights->SetIntensity(0, 1.0f);
	}
}
