// 
// rageShaderViewer/lightData.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
//

#include "lightData.h"
#include "vectormath/legacyconvert.h"

using namespace rage;

lightData::lightData()
{
	m_Type = grcLightGroup::LTTYPE_DIR;
	m_vAmbientColor.Set(0.05f, 0.05f, 0.05f, 1.0f);
	m_vLightColor.Set(1.0f, 1.0f, 1.0f, 1.0f);
	m_mMatrix.Identity();
}

lightData::~lightData()
{
	
}


bool lightData::LoadFromXML(grcLightGroup::grcLightType type, const char * szLightFileName)
{
	fiStream * streamLight = fiStream::Open(szLightFileName);
	if (!streamLight)
	{
		Warningf("Could not open light file %s", szLightFileName);
		return false;
	}

	m_Type = type;

	parRTStructure	ParRTS;
	ParRTS.SetName("TODLight");
	ParRTS.AddStructureInstance("m_KeyColor",m_KeyColor);
	ParRTS.AddStructureInstance("m_KeyAmbLight", m_KeyAmbLight);
	ParRTS.AddStructureInstance("m_KeyOrbit", m_KeyOrbit);
	ParRTS.AddStructureInstance("m_KeyDistance", m_KeyDistance);

	PARSER.LoadFromStructure(streamLight, ParRTS);

	streamLight->Close();

	return true;
}

void lightData::SetLightData(const float fTimeOfDay)
{
	Vector4	vAmbientColor;
	Vector4	vLightColor;
	Vector4	vDistance;
	Vector4	vOrbit;

	m_KeyAmbLight.GetKeyframeData(fTimeOfDay).GetData(RC_VEC4V(vAmbientColor), fTimeOfDay);

	m_vAmbientColor.Set(vAmbientColor.GetX(), vAmbientColor.GetY(), vAmbientColor.GetZ(), vAmbientColor.GetW());

	m_KeyColor.GetKeyframeData(fTimeOfDay).GetData(RC_VEC4V(vLightColor), fTimeOfDay);

	m_vLightColor.Set(vLightColor.GetX(), vLightColor.GetY(), vLightColor.GetZ(), vLightColor.GetW());

	m_KeyDistance.GetKeyframeData(fTimeOfDay).GetData(RC_VEC4V(vDistance), fTimeOfDay);
	m_KeyOrbit.GetKeyframeData(fTimeOfDay).GetData(RC_VEC4V(vOrbit), fTimeOfDay);

	Vector3	vPosition(0.0f, vDistance.GetX(), 0.0f);

	vPosition.RotateZ(vOrbit.GetX() * DtoR);
	vPosition.RotateX(vOrbit.GetY() * DtoR);

	//Look at origin
	m_mMatrix.Identity();
	m_mMatrix.d.Set(vPosition);
	m_mMatrix.LookAt(Vector3(0.0f, 0.0f, 0.0f), YAXIS);

	Vector3	vA, vB, vC(vPosition);

	vA.Set(0.0f, 0.0f, -1.0f);
	vC.Normalize();
	vB.Cross(vA, vC);
	vA.Cross(vC, vB);
	m_mMatrix.a.Set(vA);
	m_mMatrix.b.Set(vB);
	m_mMatrix.c.Set(vC);
	m_mMatrix.d.Set(vPosition);
}
