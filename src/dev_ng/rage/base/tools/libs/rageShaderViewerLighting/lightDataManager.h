// 
// rageShaderViewer/lightDataManager.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
//

#ifndef RAGE_SHADER_VIEWER_LIGHT_DATA_MANAGER_H
#define RAGE_SHADER_VIEWER_LIGHT_DATA_MANAGER_H

#include "grcore/light.h"
#include "atl/array.h"

namespace	rage
{

class	lightData;
class   grcLightGroup;

class	lightDataManager
{
public:
	lightDataManager();
	~lightDataManager();

	// Instance accessor
	static lightDataManager * GetInstance() {return sm_pInstance;}

	// Accessors
	float				GetTimeOfDay()			const {return m_fTimeOfDay;}
	bool				GetCycleTimeOfDay()		const {return m_bCycleTimeOfDay;}
	float				GetCycleSpeed()			const {return m_fCycleSpeed;}
	bool				GetUseDefaultLighting() const {return m_bUseDefaultLight;}
	Vector3				GetDefaultAmbient()		const;
	Vector3				GetDefaultDiffuse()		const;
	grcLightGroup *		GetLights()				const {return m_pLights;}

	// Initalization
	bool Init(const char * szLightsFileName);

	// Update
	void Update(const Matrix34 & CameraMatrix);

	// Modifiers
	void SetTimeOfDay(const float f)			{m_fTimeOfDay = f;}
	void SetCycleTimeOfDay(const bool b)		{m_bCycleTimeOfDay = b;}
	void SetCycleSpeed(const float f)			{m_fCycleSpeed = f;}
	void SetUseDefaultLighting(const bool b)	{m_bUseDefaultLight = b;}
	void SetDefaultAmbient(const Vector3 & c);
	void SetDefaultDiffuse(const Vector3 & c);

#if __BANK
	// PURPOSE: Add tuning widgets for light sources
	void AddWidgets(class bkBank &B) {m_pLights->AddWidgets(B);}
#endif

private:

	bool					m_bUseDefaultLight;
	bool					m_bCycleTimeOfDay;
	float					m_fCycleSpeed;
	float					m_fTimeOfDay;
	grcLightGroup *			m_pLights;

	atArray <lightData *>	m_apData;

	static lightDataManager * sm_pInstance;
};

} // end using namespace rage

#endif
