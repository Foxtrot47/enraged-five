﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using RSG.Interop.Perforce.Commands;
using Perforce.P4;

namespace RSG.Interop.Perforce
{


    /// <summary>
    /// Uber-where record parsing class; providing a model on top of where.
    /// </summary>
    /// <seealso cref="http://www.perforce.com/perforce/doc.current/manuals/cmdref/where.html" />
    /// 
    [DataContract]
    public class FileMapping
    {
        #region Properties
        /// <summary>
        /// Whether this file is mapped into the local client workspace.
        /// </summary>
        [DataMember]
        public bool Mapped
        {
            get;
            private set;
        }

        /// <summary>
        /// Local path to file (should be used for local file operations; no
        /// ASCII character expansion).
        /// </summary>
        /// This path should be used with "p4 add" (no force).  Generally this
        /// should not be used with any other Perforce command.
        /// 
        [DataMember]
        public String LocalFilename
        {
            get;
            private set;
        }

        /// <summary>
        /// Local path to file (but Perforce-syntax with ASCII character 
        /// expansion).
        /// </summary>
        /// This path should be used with "p4 add -f"; or any other Perforce
        /// command.
        /// 
        [DataMember]
        public String ExpandedLocalFilename
        {
            get;
            private set;
        }

        /// <summary>
        /// Depot path to file.
        /// </summary>
        [DataMember]
        public String DepotFilename
        {
            get;
            private set;
        }

        /// <summary>
        /// Client path to file.
        /// </summary>
        [DataMember]
        public String ClientFilename
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor; from a fstat P4API.P4Record object.
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="record"></param>
        private FileMapping(P4 p4, TaggedObject result)
        {
            InitFromTaggedObject(p4, result);
        }

        /// <summary>
        /// Constructor; from a filename.
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="filename"></param>
        private FileMapping(P4 p4, String filename)
        {
            Debug.Assert(p4.Connected, "Not connected to Perforce.");
            if (!p4.Connected)
                throw (new PerforceException("Not connected to Perforce."));

            P4CommandResult result = p4.Run(P4CommandOp.Where, filename);
            if (result.Success && result.TaggedOutput.Count > 0)
            {
                InitFromTaggedObject(p4, result.TaggedOutput[0]);
            }
            else
            {
                throw (new PerforceException("Perforce 'where' command for FileMapping failed.", 
                    result.ErrorList));
            }
        }
        #endregion // Constructor(s)

        #region Static Controller Methods
        /// <summary>
        /// Batch construction static method.
        /// </summary>
        /// <param name="filenames"></param>
        /// <returns>Array of FileMapping objects</returns>
        public static FileMapping[] Create(P4 p4, params String[] filenames)
        {
            if (0 == filenames.Length)
                throw new ArgumentException("No filenames specified.");

            Debug.Assert(p4.Connected, "Not connected to Perforce.");
            if (!p4.Connected)
                throw (new PerforceException("Not connected to Perforce."));
            P4CommandResult result = p4.Run(P4CommandOp.Where, filenames);

            return (Create(p4, result));
        }

        /// <summary>
        /// Batch construction static method.
        /// </summary>
        /// <param name="result"></param>
        /// <returns>Array of FileMapping objects</returns>
        public static FileMapping[] Create(P4 p4, P4CommandResult result)
        {
            List<FileMapping> fileStates = new List<FileMapping>();
            IEnumerable<IGrouping<String, TaggedObject>> mappings =
                result.TaggedOutput.GroupBy(r => TaggedObjectParser.GetStringField(r, "path"));

            foreach (IGrouping<String, TaggedObject> record in mappings)
            {
                // If we have more than one record then we take the mapped
                // record (no "unmap" key).  Otherwise just include it.
                int recordCount = record.Count();
                if (recordCount > 1)
                {
                    // Prefer a mapped entry over an unmapped entry.
                    TaggedObject rec = result.TaggedOutput.FirstOrDefault(r => !TaggedObjectParser.IsFlagPresent(r, "unmap"));
                    if (null == rec)
                        rec = record.First(); // But unmapped entry better than none.

                    fileStates.Add(new FileMapping(p4, rec));
                }
                else if (1 == recordCount)
                {
                    fileStates.Add(new FileMapping(p4, record.First()));
                }
            }
            return (fileStates.ToArray());
        }
        #endregion // Static Controller Methods

        #region Private Methods
        /// <summary>
        /// Initialise from a where P4API.P4Record object.
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="result"></param>
        private void InitFromTaggedObject(P4 p4, TaggedObject record)
        {
            this.Mapped = !TaggedObjectParser.IsFlagPresent(record, "unmap");
            this.LocalFilename = TaggedObjectParser.GetStringField(record, "path");
            this.ExpandedLocalFilename = AsciiExpansion.Expand(this.LocalFilename);
            this.DepotFilename = TaggedObjectParser.GetStringField(record, "depotFile");
            this.ClientFilename = TaggedObjectParser.GetStringField(record, "clientFile");
        }
        #endregion // Private Methods
    }

} // RSG.Interop.Perforce namespace
