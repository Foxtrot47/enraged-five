﻿using System;
using System.Collections.Generic;
using System.Linq;
using Perforce.P4;

namespace RSG.Interop.Perforce
{

    /// <summary>
    /// Perforce batch operation wrapper class.
    /// </summary>
    /// This is expected to be utilised by 3dsmax MaxScript and other tools where
    /// running Perforce commands per-object or per-file is more convieient for
    /// the programmer (but sucks for performance).
    /// 
    /// This batcher stores commands until the batch is executed or disposed.
    /// 
    /// <example>
    /// using (P4Batch batch = new P4Batch())
    /// {
    ///     batch.Add( ... );
    ///     batch.Add( ... );
    ///     
    ///     batch.Execute(P4Batch.ExecutionMode.StopOnError);
    /// }
    /// </example>
    /// 
    public sealed class P4Batch : IDisposable
    {
        #region Types
        /// <summary>
        /// Batch execution mode; how to handle command errors.
        /// </summary>
        public enum ExecutionMode
        {
            StopOnError,
            ContinueOnError,

            Default = StopOnError,
            DisposeDefault = ContinueOnError,
        }
        
        /// <summary>
        /// Batch command object; for storing commands internally before batch
        /// execution.
        /// </summary>
        private sealed class P4BatchCommand
        {
            #region Properties
            /// <summary>
            /// Batched command unique identifier.
            /// </summary>
            public Guid Id
            {
                get;
                private set;
            }

            /// <summary>
            /// Command operation.
            /// </summary>
            public String Command
            {
                get;
                private set;
            }

            /// <summary>
            /// Command arguments.
            /// </summary>
            public String[] Arguments
            {
                get;
                private set;
            }
            #endregion // Properties

            #region Constructor(s)
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <param name="id"></param>
            internal P4BatchCommand(Guid id, String cmd, String[] arguments)
            {
                this.Id = id;
                this.Command = cmd;
                this.Arguments = arguments;
            }
            #endregion // Constructor(s)
        }
        #endregion // Types

        #region Properties
        /// <summary>
        /// Batch name.
        /// </summary>
        public String Name
        {
            get;
            private set;
        }

        /// <summary>
        /// Whether this batch has been executed.
        /// </summary>
        public bool Executed
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Perforce API wrapper for command execution.
        /// </summary>
        private P4 _p4;

        /// <summary>
        /// Commands queued to be batched.
        /// </summary>
        private List<P4BatchCommand> _commands;

        /// <summary>
        /// Command results (once executed).
        /// </summary>
        private IDictionary<Guid, P4CommandResult> _results;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor; specifying batch name (for tracability).
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="name"></param>
        public P4Batch(P4 p4, String name)
        {
            this.Executed = false;
            this.Name = name;
            this._p4 = p4;
            this._commands = new List<P4BatchCommand>();
            this._results = new Dictionary<Guid, P4CommandResult>();
        }

        /// <summary>
        /// Dispose; force executes the batch.
        /// </summary>
        public void Dispose()
        {
            if (this.Executed)
                return;

            // During dispose we assume to continue on error because we
            // will not be able to read error results back.
            Execute(ExecutionMode.DisposeDefault);
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Run Perforce command.
        /// </summary>
        /// <param name="cmd">Perforce command to run (e.g. login, info, sync)</param>
        /// <param name="arguments"></param>
        /// <returns>Guid of command or Guid.Empty if enqueing failed.</returns>
        /// <exception cref="PerforceException">Command failed.  See InnerException for P4Exception object.</exception>
        public Guid Add(String cmd, params String[] arguments)
        {
            if (this.Executed)
                return (Guid.Empty);

            // Attempt to collapse the command to the previous command if the 
            // op and any non-filename arguments are the same.
            lock (this._commands)
            {
                P4BatchCommand lastCommand = this._commands.Last();
                if (cmd.Equals(lastCommand.Command, StringComparison.OrdinalIgnoreCase))
                {
                    IEnumerable<String> lastCommandNonFilenames = lastCommand.Arguments.Where(arg =>
                        arg.StartsWith("-"));
                    IEnumerable<String> commandNonFilenames = arguments.Where(arg =>
                        arg.StartsWith("-"));
                    if (lastCommandNonFilenames.SequenceEqual(commandNonFilenames, StringComparer.OrdinalIgnoreCase))
                    {
                        List<String> args = new List<String>(lastCommand.Arguments);
                        IEnumerable<String> commandFilenames = arguments.Where(arg =>
                            !arg.StartsWith("-"));
                        args.AddRange(commandFilenames);

                        // Remove previous command before enqueuing new one with same ID.
                        this._commands.RemoveAt(this._commands.Count - 1);
                        return (EnqueueCommand(lastCommand.Id, cmd, args.ToArray()));
                    }
                    else
                    {
                        return (EnqueueCommand(cmd, arguments));
                    }
                }
                else
                {
                    // Enqueue new command.
                    return (EnqueueCommand(cmd, arguments));
                }
            }
        }

        /// <summary>
        /// Run Perforce command (using P4CommandOp).
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="arguments"></param>
        /// <returns>P4CommandResult.</returns>
        /// <exception cref="PerforceException">Command failed.  See InnerException for P4Exception object.</exception>
        public Guid Add(P4CommandOp cmd, params String[] arguments)
        {            
            String cmdOpStr = cmd.ToString().ToLower();
            return (this.Add(cmdOpStr, arguments));
        }

        /// <summary>
        /// Force execute the batch.  The batch will not be executed again on dispose.
        /// </summary>
        /// <param name="mode"></param>
        /// <returns></returns>
        public bool Execute(ExecutionMode mode)
        {
            bool result = true;
            lock (this._commands)
            {
                if (this.Executed)
                    return (false);

                // Loop through commands executing them.
                foreach (P4BatchCommand c in this._commands)
                {
                    try
                    {
                        P4CommandResult cmdResult = this._p4.Run(c.Command, c.Arguments);
                        this._results.Add(c.Id, cmdResult);

                        result &= cmdResult.Success;
                        if ((ExecutionMode.StopOnError == mode) && (!result))
                            break; // Abort batch execution.
                    }
                    catch (P4Exception ex)
                    {
                        result = false;
                        if ((ExecutionMode.StopOnError == mode) && (!result))
                            throw (new PerforceException("Perforce command batch failed.", ex));
                    }
                }
                this.Executed = true;
            }
            return (result);
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Enqueue new command.
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="arguments"></param>
        /// <returns></returns>
        private Guid EnqueueCommand(String cmd, String[] arguments)
        {
            Guid id = Guid.NewGuid();
            return (EnqueueCommand(id, cmd, arguments));
        }

        /// <summary>
        /// Enqueue new command (specifying identifier).
        /// </summary>
        /// <param name="id"></param>
        /// <param name="cmd"></param>
        /// <param name="arguments"></param>
        /// <returns></returns>
        private Guid EnqueueCommand(Guid id, String cmd, String[] arguments)
        {
            P4BatchCommand command = new P4BatchCommand(id, cmd, arguments);
            this._commands.Add(command);
            return (id);
        }
        #endregion // Private Methods
    }

} // RSG.Interop.Perforce namespace
