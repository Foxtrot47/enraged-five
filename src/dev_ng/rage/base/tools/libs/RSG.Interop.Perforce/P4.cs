using System;
using System.Diagnostics;
using System.Linq;
using Perforce.P4;

namespace RSG.Interop.Perforce
{

    /// <summary>
    /// Perforce API wrapper class.
    /// </summary>
    /// This P4API.Net wrapper class is to simply handle the complexities of
    /// server login and authentication.
    /// 
    /// ** DO NOT BLOAT THIS CLASS **  We will always need access to
    /// the core P4 API for efficiency.  Create additional helper methods in a
    /// helper class if you require (this must not end up like the current
    /// 3dsmax p4.ms script!).
    /// 
    /// For example helper classes see FileState and FileMapping.
    /// <seealso cref="FileState"/>
    /// <seealso cref="FileMapping"/>
    /// <seealso cref="Commands.TaggedObjectParser"/>
    /// 
    /// <example>
    /// using (P4 p4 = new P4(PerforceLoginCallback))
    /// {
    ///    ...
    /// }
    /// 
    /// P4 p4;
    /// try
    /// {
    ///     p4 = new P4(PerforceLoginCallback);
    ///     ...
    /// }
    /// catch (...)
    /// {
    ///   ...
    /// }
    /// finally
    /// {
    ///     p4.Disconnect();
    /// }
    /// </example>
    ///
    public sealed class P4 : 
        IPerforceConnection, 
        IDisposable
    {
        #region Delegates
        /// <summary>
        /// Delegate type for retrieving user's login password; this allows 
        /// user-code to customise how the user is prompted for a password.
        /// </summary>
        /// <param name="p4"></param>
        /// <returns></returns>
        public delegate bool ConnectPasswordPrompt(P4 p4);
        #endregion // Delegates

        #region Properties
        /// <summary>
        /// Server port.
        /// </summary>
        public ServerAddress Port
        {
            get { return _port; }
        }

        /// <summary>
        /// Gets the username being used for this connection.
        /// </summary>
        public String Username
        {
            get
            {
                if (null != this._connection)
                {
                    return this._connection.UserName;
                }

                return String.Empty;
            }
        }

        /// <summary>
        /// Gets the client being used for this connection.
        /// </summary>
        public Client Client
        {
            get
            {
                if (this._connection != null)
                {
                    return this._connection.Client;
                }

                return null;
            }
        }

        /// <summary>
        /// Gets the client name being used for this connection.
        /// </summary>
        public string ClientName
        {
            get
            {
                if (this._connection != null && this._connection.Client != null)
                {
                    return this._connection.Client.Name;
                }

                return String.Empty;
            }
        }

        /// <summary>
        /// Whether or not we are connected to the P4 server.
        /// </summary>
        public bool Connected
        {
            get 
            {
                if (null != this._connection)
                    return (ConnectionStatus.Connected == this._connection.Status);
                else
                    return (false);
            }
        }

        /// <summary>
        /// Current P4 server state.
        /// </summary>
        public ServerState State
        {
            get 
            {
                if (null != this._server)
                    return (this._server.State);
                else
                    return (ServerState.Unknown);
            }
        }

        /// <summary>
        /// P4 repository (access useful P4 helper methods through this).
        /// </summary>
        public Repository Repository
        {
            get { return _repository; }
        }

        /// <summary>
        /// P4 connection (requried for using the p4 .net API).
        /// </summary>
        public Connection Connection
        {
            get { return _connection; }
        }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Raw P4API connection object.
        /// </summary>
        private readonly Connection _connection;

        /// <summary>
        /// Raw P4API server object.
        /// </summary>
        private readonly Server _server;

        /// <summary>
        /// Private field for the <see cref="Port"/> property.
        /// </summary>
        private readonly ServerAddress _port;

        /// <summary>
        /// Private field for the <see cref="Repository"/> property.
        /// </summary>
        private readonly Repository _repository;

        /// <summary>
        /// Connection password delegate callback.
        /// </summary>
        private readonly ConnectPasswordPrompt _connectionPasswordPrompt;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor; connecting to server using P4CONFIG settings.
        /// </summary>
        /// <param name="passwordCallback">
        /// Delegate for performing the perforce login.
        /// </param>
        public P4(ConnectPasswordPrompt passwordCallback)
            : this(passwordCallback, new P4Config(), TimeSpan.FromSeconds(30))
        {
        }

        /// <summary>
        /// Constructor; connecting to server using P4CONFIG settings.
        /// </summary>
        /// <param name="passwordCallback">
        /// Delegate for performing the perforce login.
        /// </param>
        /// <param name="p4config">
        /// P4Config object to use to create the connection.
        /// </param>
        public P4(ConnectPasswordPrompt passwordCallback, TimeSpan commandTimeout)
            : this(passwordCallback, new P4Config(), commandTimeout)
        {
        }

        /// <summary>
        /// Constructor; connecting to server using P4CONFIG settings.
        /// </summary>
        /// <param name="passwordCallback">
        /// Delegate for performing the perforce login.
        /// </param>
        /// <param name="p4config"></param>
        /// <param name="commandTimeout"></param>
        public P4(ConnectPasswordPrompt passwordCallback, P4Config p4config, TimeSpan commandTimeout)
            : this(passwordCallback, p4config.Port, p4config.User, p4config.Client, commandTimeout)
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="passwordCallback">
        /// Delegate for performing the perforce login.
        /// </param>
        /// <param name="port">
        /// Port to connect to.
        /// </param>
        /// <param name="user">
        /// User to connect as.
        /// </param>
        /// <param name="client">
        /// Workspace to use for the connection.
        /// </param>
        /// <param name="commandTimeout"></param>
        public P4(ConnectPasswordPrompt passwordCallback, String port, String user, String client, TimeSpan commandTimeout)
        {
            this._port = new ServerAddress(port);
            this._connectionPasswordPrompt = passwordCallback;
            this._server = new Server(this.Port);
            this._repository = new Repository(this._server);
            this._connection = this.Repository.Connection;
            this._connection.UserName = user;
            this._connection.CommandTimeout = commandTimeout;
            this._connection.SetClient(client);
        }
        
        /// <summary>
        /// Dispose; disconnect and cleanup.
        /// </summary>
        public void Dispose()
        {
            this._connection.Disconnect();
            this._connection.Dispose();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Connect to Perforce; prompting user for a password if they do not
        /// have a valid session.
        /// </summary>
        /// <returns>true iff successful; false otherwise.</returns>
        /// <exception cref="PerforceException">Login failed; password incorrect or prompt canceled.</exception>
        public bool Connect()
        {
            try
            {
                // Connect to server.
                Options options = new Options();
                bool result = this._connection.Connect(options);

                // Determine if server requires a login; this will succeed
                // if the user already has a valid session.
                P4CommandResult loginResult = this.Run(P4CommandOp.Login, "-s");
                if (loginResult.Success)
                    return (true);
            }
            catch (P4Exception)
            {
                // P4Exception is raised from the "login -s" command; so we need to
                // prompt the user for their password.  We verify afterwards whether 
                // the login was successful hence the stacked exception handlers.
                try
                {
                    Debug.Assert(null != this._connectionPasswordPrompt, "No valid password prompt callback.");
                    if (null == this._connectionPasswordPrompt)
                        throw (new PerforceException("No valid password prompt callback."));

                    String password = String.Empty;
                    bool result = this._connectionPasswordPrompt(this);
                    if (!result)
                        throw (new PerforceException("User canceled Perforce login prompt."));

                    // Determine if server requires a login.
                    P4CommandResult loginResult = this.Run(P4CommandOp.Login, "-s");
                    if (loginResult.Success)
                        return (true);
                }
                catch (P4Exception ex)
                {
                    // We did not have a valid session
                    throw (new PerforceException("Invalid password or login authentication failure.", ex));
                }
            }

            // We should never get here; something went wrong.
            return (false);
        }

        /// <summary>
        /// Connect to Perforce; with supplied user password.
        /// </summary>
        /// <param name="password">User's password string.</param>
        /// <returns>true iff successful; false otherwise.</returns>
        /// <exception cref="PerforceException">Login failed; password incorrect.</exception>
        public bool Connect(String password)
        {
            try
            {
                Options options = new Options();
                options.Add("Password", password);

                // Connect to server.
                bool result = this._connection.Connect(options);

                // Determine if server requires a login; this will succeed
                // if the user already has a valid session.
                P4CommandResult loginResult = this.Run(P4CommandOp.Login, "-s");
                if (loginResult.Success)
                    return (true);
            }
            catch (P4Exception)
            {
                // P4Exception is raised from the "login -s" command; so we attempt
                // to login using the password.  We verify afterwards whether the
                // login was successful hence the stacked exception handlers.
                try
                {
                    this._connection.Login(password);

                    // Determine if server requires a login.
                    P4CommandResult loginResult = this.Run(P4CommandOp.Login, "-s");
                    if (loginResult.Success)
                        return (true);
                }
                catch (P4Exception ex)
                {
                    // We did not have a valid session
                    throw (new PerforceException("Invalid password or login authentication failure.", ex));
                }
            }

            // We should never get here; something went wrong.
            return (false);
        }

        /// <summary>
        /// Disconnect from the Perforce server.
        /// </summary>
        /// <returns></returns>
        public bool Disconnect()
        {
            return (this._connection.Disconnect());
        }

        /// <summary>
        /// Automate the Login to the Perforce Server
        /// </summary>
        /// <param name="password">
        /// User' password
        /// </param>
        /// <returns>
        /// A value indicating whether the login was successful.
        /// </returns>
        public bool Login(string password)
        {
            this._connection.Login(password);
            P4CommandResult loginResult = this.Run(P4CommandOp.Login, "-s");
            return loginResult.Success;
        }

        /// <summary>
        /// Initialises the specified changelist with this connection.
        /// </summary>
        /// <param name="changelist">
        /// The changelist to initialise.
        /// </param>
        /// <returns>
        /// A value indicating whether the initialisation was successful.
        /// </returns>
        public bool InitialiseChangeList(Changelist changelist)
        {
            try
            {
                changelist.initialize(this._connection);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Run Perforce command.
        /// </summary>
        /// <param name="cmd">Perforce command to run (e.g. login, info, sync)</param>
        /// <param name="arguments"></param>
        /// <returns>P4CommandResult.</returns>
        /// <exception cref="PerforceException">Command failed.  See InnerException for P4Exception object.</exception>
        public P4CommandResult Run(String cmd, params String[] arguments)
        {
            if (!this.Connected)
                throw (new PerforceException("Not connected."));

            try
            {
                P4Command p4cmd = new P4Command(this._connection, cmd, true, arguments);
                return (p4cmd.Run());
            }
            catch (P4Exception ex)
            {
                throw (new PerforceException(String.Format("Perforce command '{0}' failed.", cmd), ex));
            }
        }

        /// <summary>
        /// Run Perforce command (using P4CommandOp).
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="arguments"></param>
        /// <returns>P4CommandResult.</returns>
        /// <exception cref="PerforceException">Command failed.  See InnerException for P4Exception object.</exception>
        public P4CommandResult Run(P4CommandOp cmd, params String[] arguments)
        {
            String cmdOpStr = cmd.ToString().ToLower();
            P4Command p4cmd = new P4Command(this._connection, cmdOpStr, true, arguments);
            return (p4cmd.Run());
        }
        #endregion // Controller Methods
    }

} // RSG.Interop.Perforce namespace
