﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Interop.Perforce.Commands
{

    /// <summary>
    /// ASCII character expansion for Perforce filenames.
    /// </summary>
    /// Perforce documentation on the subject:
    /// http://www.perforce.com/perforce/doc.current/manuals/cmdref/o.fspecs.html#1041962
    /// 
    internal static class AsciiExpansion
    {
        #region Constants
        private static readonly Char AT = '@';
        private static readonly Char HASH = '#';
        private static readonly Char STAR = '*';   // Aren't permitted on NTFS.
        private static readonly Char PERCENT = '%';

        private static readonly Char[] expansion =
            new Char[] { PERCENT, AT, HASH, STAR };
        #endregion // Constants
        
        #region Controller Methods
        /// <summary>
        /// Expand an ASCII characters that are required.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static String Expand(String filename)
        {
            String expanded = filename;
            foreach (Char c in expansion)
            {
                UInt32 val = Convert.ToUInt32(c);
                expanded = expanded.Replace(c.ToString(), String.Format("%{0:X}", val));
            }
            return (expanded);
        }

        /// <summary>
        /// Reverse the expansion of any ASCII characters.
        /// </summary>
        /// <param name="filename"></param>
        public static String Collapse(String filename)
        {
            String collapsed = filename;
            foreach (Char c in expansion)
            {
                UInt32 val = Convert.ToUInt32(c);
                String valstr = String.Format("%{0:X}", val);
                collapsed = collapsed.Replace(valstr, c.ToString());
            }
            return (collapsed);
        }
        #endregion // Controller Methods
    }

} // RSG.Interop.Perforce.Commands namespace
