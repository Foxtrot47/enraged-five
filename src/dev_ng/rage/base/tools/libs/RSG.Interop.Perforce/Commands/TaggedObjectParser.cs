﻿using System;
using Perforce.P4;

namespace RSG.Interop.Perforce.Commands
{

    /// <summary>
    /// Collection of static methods to help parse TaggedObject structures.
    /// </summary>
    public static class TaggedObjectParser
    {
        /// <summary>
        /// Return whether a field is set to "1" in a record.
        /// </summary>
        /// <param name="record"></param>
        /// <param name="field"></param>
        /// <returns></returns>
        public static bool IsFlagSet(TaggedObject record, String field)
        {
            return (record.ContainsKey(field) && (int.Parse(record[field]) == 1 ? true : false));
        }

        /// <summary>
        /// Return whether a field is present in a record.
        /// </summary>
        /// <param name="record"></param>
        /// <param name="field"></param>
        /// <returns></returns>
        public static bool IsFlagPresent(TaggedObject record, String field)
        {
            return (record.ContainsKey(field));
        }

        /// <summary>
        /// Return integer field.
        /// </summary>
        /// <param name="record"></param>
        /// <param name="field"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static int GetIntField(TaggedObject record, String field, int defaultValue = 0)
        {
            return (record.ContainsKey(field) ? int.Parse(record[field]) : defaultValue);
        }

        /// <summary>
        /// Return changelist field.
        /// <remarks>handles the default changelist</remarks>
        /// </summary>
        /// <param name="record"></param>
        /// <param name="field"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static uint GetChangelistField(TaggedObject record, String field, uint defaultValue = 0)
        {
            if (record.ContainsKey(field))
            {
                uint result;

                if (record[field] != "default" &&
                    uint.TryParse(record[field], out result))
                {
                    return result;
                }
            }
            return defaultValue;
        }

        /// <summary>
        /// Return string field.
        /// </summary>
        /// <param name="record"></param>
        /// <param name="field"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static String GetStringField(TaggedObject record, String field, String defaultValue = "")
        {
            return (record.ContainsKey(field) ? record[field] : defaultValue);
        }

        /// <summary>
        /// Return a DateTime object for a time field.
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="record"></param>
        /// <param name="field"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static DateTime GetDateTimeField(TaggedObject record, String field, DateTime defaultValue = default(DateTime))
        {
            String datetime = GetStringField(record, field);
            if (String.IsNullOrEmpty(datetime))
                return (defaultValue);
            
            int utc_ticks = int.Parse(datetime);
            return (DateTime.FromFileTimeUtc(utc_ticks));
        }
    }

} // RSG.SourceControl.Perforce.Util namespace
