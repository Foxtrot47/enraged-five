﻿using System;
using System.Collections.Generic;
using Perforce.P4;

namespace RSG.Interop.Perforce
{

    /// <summary>
    /// Perforce exception class used for all exceptions with connections and commands.
    /// </summary>
    public class PerforceException : Exception
    {
        #region Properties 
        /// <summary>
        /// Perforce error list (if applicable).
        /// </summary>
        public IEnumerable<P4ClientError> ErrorList
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        public PerforceException()
            : base()
        {
            SetErrorList();
        }

        public PerforceException(List<P4ClientError> errorList)
            : base()
        {
            SetErrorList(errorList);
        }

        public PerforceException(String message)
            : base(message)
        {
            SetErrorList();
        }

        public PerforceException(String message, List<P4ClientError> errorList)
            : base(message)
        {
            SetErrorList(errorList);
        }

        public PerforceException(String message, Exception inner)
            : base(message, inner)
        {
            SetErrorList();
        }

        public PerforceException(String message, Exception inner, List<P4ClientError> errorList)
            : base(message, inner)
        {
            SetErrorList(errorList);
        }
        #endregion // Constructor(s)

        #region Private Methods
        /// <summary>
        /// Initialise our ErrorList property.
        /// </summary>
        private void SetErrorList()
        {
            SetErrorList(null);
        }

        /// <summary>
        /// Initialise our ErrorList property.
        /// </summary>
        /// <param name="errorList"></param>
        private void SetErrorList(List<P4ClientError> errorList)
        {
            if (null == errorList)
                this.ErrorList = new P4ClientError[0];
            else
            {
                List<P4ClientError> errors = new List<P4ClientError>();
                errors.AddRange(errorList);
                this.ErrorList = errors;
            }
        }
        #endregion // Private Methods
    }

} // RSG.Interop.Perforce namespace
