﻿using System;
using System.Diagnostics;
using System.IO;
using Microsoft.Win32;

namespace RSG.Interop.Perforce
{

    /// <summary>
    /// P4CONFIG settings abstraction; only for use to determine Perforce 
    /// settings.
    /// </summary>
    public sealed class P4Config
    {
        #region Constants
        /// <summary>
        /// Registry sub-key for Perforce environment.
        /// </summary>
        private const String EnvironmentKey = @"Software\Perforce\environment";

        /// <summary>
        /// Registry 'P4CONFIG' key.
        /// </summary>
        private const String P4ConfigKey = "P4CONFIG";

        /// <summary>
        /// Registry 'P4PORT' key.
        /// </summary>
        private const String P4PortKey = "P4PORT";

        /// <summary>
        /// Registry 'P4USER' key.
        /// </summary>
        private const String P4UserKey = "P4USER";

        /// <summary>
        /// Registry 'P4CLIENT' key.
        /// </summary>
        private const String P4ClientKey = "P4CLIENT";

        /// <summary>
        /// Registry 'P4EDITOR' key.
        /// </summary>
        private const String P4EditorKey = "P4EDITOR";

        /// <summary>
        /// Registry 'P4DIFF' key.
        /// </summary>
        private const String P4DiffKey = "P4DIFF";

        /// <summary>
        /// Registry 'P4MERGE' key.
        /// </summary>
        private const String P4MergeKey = "P4MERGE";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// P4CONFIG setting.
        /// </summary>
        public static String Config
        {
            get;
            private set;
        }

        /// <summary>
        /// P4PORT setting.
        /// </summary>
        public String Port
        {
            get;
            private set;
        }

        /// <summary>
        /// P4USER setting.
        /// </summary>
        public String User
        {
            get;
            private set;
        }

        /// <summary>
        /// P4CLIENT setting.
        /// </summary>
        public String Client
        {
            get;
            private set;
        }

        /// <summary>
        /// P4EDITOR setting (Editor invoked by p4 commands).
        /// </summary>
        public String Editor
        {
            get;
            private set;
        }

        /// <summary>
        /// P4DIFF setting (Diff program to use on client).
        /// </summary>
        public String Diff
        {
            get;
            private set;
        }

        /// <summary>
        /// P4MERGE setting (Merge program to use on client).
        /// </summary>
        public String Merge
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Create configuration object from current process working directory.
        /// </summary>
        public P4Config()
        {
            String workingDirectory = Directory.GetCurrentDirectory();
            Init(workingDirectory);
        }

        /// <summary>
        /// Create configuration object for a user-specified directory.
        /// </summary>
        /// <param name="workingDirectory">Overridden working directory.</param>
        public P4Config(String workingDirectory)
        {
            Init(workingDirectory);
        }

        /// <summary>
        /// Static constructor; initialises static properties.
        /// </summary>
        static P4Config()
        {
            try
            {
                using (RegistryKey envKey = Registry.CurrentUser.OpenSubKey(EnvironmentKey))
                {
                    Object p4ConfigSetting = envKey.GetValue(P4ConfigKey);
                    if (p4ConfigSetting is String)
                        P4Config.Config = (String)p4ConfigSetting;
                }
            }
            catch (Exception ex)
            {
                throw (new PerforceException("Exception reading Perforce registry environment.", ex));
            }
        }
        #endregion // Constructor(s)

        #region Private Methods
        /// <summary>
        /// Initialise from a specified working directory; search up directory
        /// structure for P4CONFIG filename and initialise.
        /// </summary>
        /// <param name="workingDirectory"></param>
        private void Init(String workingDirectory)
        {
            InitFromRegistryDefault();

            String directory = Path.GetFullPath(workingDirectory);
            String configFilename = Path.Combine(directory, P4Config.Config);
            if (File.Exists(configFilename))
            {
                // P4CONFIG file exists; read it and use settings.
                InitFromP4CONFIG(configFilename);
            }
            else
            {
                String directoryRoot = Path.GetPathRoot(directory);
                String directoryUp = Path.GetDirectoryName(directory);
                if (directoryUp.Equals(directoryRoot))
                    return;
                else
                    Init(directoryUp);
            }
        }

        /// <summary>
        /// Initialise from specified P4CONFIG filename.
        /// </summary>
        /// <param name="configFilename"></param>
        private void InitFromP4CONFIG(String configFilename)
        {
            using (FileStream fs = File.OpenRead(configFilename))
            using (StreamReader sr = new StreamReader(fs))
            {
                while (sr.Peek() >= 0)
                {
                    String line = sr.ReadLine();
                    String[] parts = line.Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries);
                    Debug.Assert(2 == parts.Length,
                        "Malformed P4CONFIG file line.  Ignoring.");
                    if (2 != parts.Length)
                        continue;

                    if (P4PortKey.Equals(parts[0]))
                        this.Port = parts[1];
                    else if (P4UserKey.Equals(parts[0]))
                        this.User = parts[1];
                    else if (P4ClientKey.Equals(parts[0]))
                        this.Client = parts[1];
                    else if (P4EditorKey.Equals(parts[0]))
                        this.Editor = parts[1];
                    else if (P4DiffKey.Equals(parts[0]))
                        this.Diff = parts[1];
                    else if (P4MergeKey.Equals(parts[0]))
                        this.Merge = parts[1];
                }
            }
        }

        /// <summary>
        /// Initialise from default settings in registry.
        /// </summary>
        private void InitFromRegistryDefault()
        {
            try
            {
                this.Port = String.Empty;
                this.User = String.Empty;
                this.Client = String.Empty;
                this.Editor = String.Empty;
                this.Diff = String.Empty;
                this.Merge = String.Empty;

                using (RegistryKey envKey = Registry.CurrentUser.OpenSubKey(EnvironmentKey))
                {
                    Object p4PortSetting = envKey.GetValue(P4PortKey);
                    if (p4PortSetting is String)
                        this.Port = (String)p4PortSetting;
                    Object p4UserSetting = envKey.GetValue(P4UserKey);
                    if (p4UserSetting is String)
                        this.User = (String)p4UserSetting;
                    Object p4ClientSetting = envKey.GetValue(P4ClientKey);
                    if (p4ClientSetting is String)
                        this.Client = (String)p4ClientSetting;
                    Object p4EditorSetting = envKey.GetValue(P4EditorKey);
                    if (p4EditorSetting is String)
                        this.Editor = (String)p4EditorSetting;
                    Object p4DiffSetting = envKey.GetValue(P4DiffKey);
                    if (p4DiffSetting is String)
                        this.Diff = (String)p4DiffSetting;
                    Object p4MergeSetting = envKey.GetValue(P4MergeKey);
                    if (p4MergeSetting is String)
                        this.Merge = (String)p4MergeSetting;
                }
            }
            catch (Exception ex)
            {
                throw (new PerforceException("Exception reading Perforce registry environment for default initialisation.", ex));
            }
        }
        #endregion // Private Methods
    }

} // RSG.Interop.Perforce namespace
