﻿using System;
using System.Collections.Generic;
using System.Linq;
using Perforce.P4;

namespace RSG.Interop.Perforce
{

    /// <summary>
    /// Perforce API interface.
    /// </summary>
    public interface IPerforceConnection : IDisposable
    {
        #region Properties
        /// <summary>
        /// Server port.
        /// </summary>
        ServerAddress Port { get; }

        /// <summary>
        /// Whether or not we are connected to the P4 server.
        /// </summary>
        bool Connected { get; }

        /// <summary>
        /// Current P4 server state.
        /// </summary>
        ServerState State { get; }

        /// <summary>
        /// P4 repository (access useful P4 helper methods through this).
        /// </summary>
        Repository Repository { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Connect to Perforce.
        /// </summary>
        bool Connect();

        /// <summary>
        /// Connect to Perforce; with supplied user password.
        /// </summary>
        /// <param name="password"></param>
        bool Connect(String password);

        /// <summary>
        /// Disconnect from the Perforce server.
        /// </summary>
        /// <returns></returns>
        bool Disconnect();
        
        /// <summary>
        /// Run Perforce command.
        /// </summary>
        /// <param name="cmd">Perforce command to run (e.g. login, info, sync)</param>
        /// <param name="arguments"></param>
        /// <returns>P4CommandResult.</returns>
        /// <exception cref="PerforceException">Command failed.  See InnerException for P4Exception object.</exception>
        P4CommandResult Run(String cmd, params String[] arguments);

        /// <summary>
        /// Run Perforce command (using P4CommandOp).
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="arguments"></param>
        /// <returns>P4CommandResult.</returns>
        /// <exception cref="PerforceException">Command failed.  See InnerException for P4Exception object.</exception>
        P4CommandResult Run(P4CommandOp cmd, params String[] arguments);
        #endregion // Controller Methods
    }

} // RSG.Interop.Perforce namespace
