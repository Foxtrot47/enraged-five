﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using Perforce.P4;

namespace RSG.Interop.Perforce
{

    /// <summary>
    /// Perforce Web Interface helper class.
    /// </summary>
    /// This class provides methods to return the HTML hyperlinks to p4web 
    /// served pages.
    public static class P4Web
    {
        #region Constants
        /// <summary>
        /// The Web port # used.
        /// </summary>
        private const uint WEB_PORT = 8080;

        /// <summary>
        /// String replacement token
        /// - for identifying the CL number within a string.
        /// </summary>
        private const String CHANGELIST_PREFIX = "CL:";
        #endregion // Constants

        #region Controller Methods
        /// <summary>
        /// Return HTML hyperlink string linking to Perforce Web changelist.
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="changelist"></param>
        /// <returns></returns>
        public static String ChangelistHyperLink(IPerforceConnection p4, uint changelist)
        {
            return (ChangelistHyperlink(p4.Port, changelist, WEB_PORT));
        }

        /// <summary>
        /// Return HTML hyperlink string linking to Perforce Web changelist.
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="changelist"></param>
        /// <param name="web_port"></param>
        /// <returns></returns>
        public static String ChangelistHyperlink(IPerforceConnection p4, uint changelist, uint web_port)
        {
            return (ChangelistHyperlink(p4.Port, changelist, web_port));
        }

        /// <summary>
        /// Return HTML hyperlink string linking to Perforce Web changelist.
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="changelist"></param>
        /// <param name="web_port"></param>
        /// <returns></returns>
        public static String ChangelistHyperlink(ServerAddress serverAddress, uint changelist, uint web_port)
        {
            Debug.Assert(null != serverAddress, "Invalid Perforce ServerAddress object.");
            if (null == serverAddress)
                throw (new ArgumentNullException("serverAddress"));

            // Redirect to the web port #.
            String server = serverAddress.Uri.Split(new char[]{':'}, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault();
            if (!String.IsNullOrEmpty(server))
            {
                return String.Format("<a title='p4Web' href='http://{1}:{2}/@md=d&amp;cd=@/{0}?ac=10'>{0}</a>",
                    changelist, server, web_port);
            }
            return (changelist.ToString());
        }

        /// <summary>
        /// Return a string that replaces all changelist numbers ( prefixed by default with 'CL:' ) 
        /// with a p4 web HTML Changelist link. 
        /// </summary>
        /// <param name="p4">The p4 object, it may be null.</param>
        /// <param name="sourceString">String to search & replace</param>
        /// <param name="ClPrefix">The optional prefix to a cl number</param>
        /// <returns></returns>
        public static String ChangelistHyperLinks(IPerforceConnection p4, String sourceString, String ClPrefix = CHANGELIST_PREFIX)
        {
            Regex regex = new Regex(String.Format(@"(.*){0}([\d]+)(.*)", ClPrefix));
            do
            {
                Match match = regex.Match(sourceString);
                if (match.Success && match.Groups.Count == 4)
                {
                    String preface = match.Groups[1].Value;
                    String htmlCl = ChangelistHyperLink(p4, uint.Parse(match.Groups[2].Value));
                    String trailing = match.Groups[3].Value;

                    sourceString = String.Format(@"{0}{1}{2}", preface, htmlCl, trailing);
                    continue;
                }
                break;
            } while (true);

            return sourceString;
        }
        #endregion // Controller Methods
    }

} // RSG.Interop.Perforce namespace
