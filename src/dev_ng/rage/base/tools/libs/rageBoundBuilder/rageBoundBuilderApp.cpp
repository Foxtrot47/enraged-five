// 
// /makebound.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "phcore/materialmgrflag.h"
#include "phcore/materialmgrimpl.h"
#include "system/main.h"
#include "system/param.h"

#include "sample_rmcore/sample_rmcore.h"

#include "rageBoundBuilder.h"

using namespace rage;
using namespace ragesamples;

FPARAM(1, infile,	"The filename of the type file to load (default is entity.type)");
FPARAM(2, outfile,	"The output bound file name (default is bounds.bnd)");

PARAM(file,		"The filename of the type file to load (default is entity.type)");
PARAM(in,		"The filename of the type file to load (default is entity.type)");
PARAM(dir,		"Drawable directory (default is assert path)");
PARAM(out,		"The output bound file name (default is bounds.bnd)");

PARAM(nosimplify, "Do not simplify the bounds by converting triangles to quads (default is to simplify)");
PARAM(bind,		"Material binding list (default is materialbind.lst)");
PARAM(flags,	"Bound flag image list (default is boundflags.lst)");
PARAM(path,		"Asset search path");
PARAM(uvset,	"Specifies which UV set to use when applying the channel and AI maps (default is 0)");

PARAM(outputgrid,		"Outputs an OT grid when this flag is present");
PARAM(otcellsize,		"Size of an octree grid cell (default is 25.0)");
PARAM(otmaxheight,		"Maximum height of the generated octree (default is 8)");
PARAM(otmaxpercell,		"Maximum polygons per octree cell (default is 15)");

class rageBoundBuilderManager : public rmcSampleManager
{
protected:
	void InitClient()
	{
		rmcSampleManager::InitClient();

		phMaterialMgrImpl<phMaterial, phMaterialMgrFlag>::Create();
		MATERIALMGRFLAG.SetAutoAssignFlags(true);

		char mtlPath[1024];
		sprintf(mtlPath,"%s/%s",GetFullAssetPath(),"tune/materials");
		ASSET.PushFolder(mtlPath);
		MATERIALMGR.Load();
		ASSET.PopFolder();

		rageBoundBuilder boundBuilder;

		const char* typeFileName = "entity.type";
		PARAM_in.Get(typeFileName);
		PARAM_file.Get(typeFileName);
		PARAM_infile.Get(typeFileName);
		boundBuilder.SetTypeFileName(typeFileName);

		const char* outFile = "bounds.bnd";
		PARAM_out.Get(outFile);
		PARAM_outfile.Get(outFile);
		boundBuilder.SetOutputFileName(outFile);

		int uvSet = 0;
		PARAM_uvset.Get(uvSet);
		boundBuilder.SetUVSet(uvSet);

		boundBuilder.SetSimplify(!PARAM_nosimplify.Get());

		const char* mapFileName = "materialbind";
		PARAM_bind.Get(mapFileName);
		rageTextureMaterialMapFile materialMap(mapFileName);
		boundBuilder.SetTextureMaterialMap(materialMap);

		const char* flagFileName = "materialflags";
		PARAM_flags.Get(flagFileName);
		rageFlagImageListFile flagImageList(flagFileName);
		boundBuilder.SetFlagImageList(flagImageList);

        if (PARAM_outputgrid.Get())
        {
            float otCellSize = 25.f;
            int otMaxHeight = 8;
            int otMaxPerCell = 15;
 
            PARAM_otcellsize.Get(otCellSize);
            PARAM_otmaxheight.Get(otMaxHeight);
            PARAM_otmaxpercell.Get(otMaxPerCell);

            boundBuilder.SetOTGridParams(true, otCellSize, otMaxHeight, otMaxPerCell);
        }

		boundBuilder.BuildBound();
	}

	void ShutdownClient()
	{
		MATERIALMGR.Destroy();

		rmcSampleManager::ShutdownClient();
	}
};

int Main()
{
	rageBoundBuilderManager boundBuilder;

	const char* dir = NULL;
	PARAM_dir.Get(dir);

	boundBuilder.Init(dir);
	boundBuilder.Shutdown();

	return 0;
}
