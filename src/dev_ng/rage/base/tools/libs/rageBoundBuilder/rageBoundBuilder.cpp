// 
// /rageBoundBuilder.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "rageboundbuilder.h"

#include "file/asset.h"
#include "grcore/image.h"
#include "grcore/texture.h"
#include "grmodel/geometry.h"
#include "grmodel/shader.h"
#include "grmodel/shadergroup.h"
#include "grmodel/shadervar.h"
#include "math/simplemath.h"
#include "mesh/mesh.h"
#include "phcore/materialmgrflag.h"
#include "phbound/boundgeom.h"
#include "phbound/boundgrid.h"
#include "rmcore/drawable.h"

using namespace rage;


grcImage* rageImageCache::LoadImage(const char* filename)
{
	grcImage** image = m_ImageCache.Access(filename);
	if (image)
	{
		(*image)->AddRef();
		return *image;
	}
	else
	{
		grcImage* tmpImage = grcImage::Load(filename);
		if (!tmpImage)
			Quitf("Unable to open image file \"%s\"", filename);
		grcImage* newImage = tmpImage->Reformat(grcImage::A8R8G8B8);
		Assert(newImage);
		tmpImage->Release();
		m_ImageCache.Insert(ConstString(filename), newImage);
		newImage->AddRef();
		return newImage;
	}
}


void rageImageCache::Release()
{
	atMap<ConstString, grcImage*>::Iterator i = m_ImageCache.CreateIterator();
	for ( ; !i.AtEnd(); ++i)
		i.GetData()->Release();
	m_ImageCache.Kill();
}


rageTextureMaterialMapFile::rageTextureMaterialMapFile(const char* mapFileName)
{
	fiStream *stream = ASSET.Open(mapFileName, "lst");
	if (!stream)
	{
		Warningf("Unable to load material map file, assigning default material to entire bound");
		return;
	}

	fiAsciiTokenizer tok;
	tok.Init(stream->GetName(), stream);

	char texture[256], material[256];

	for ( ; ; )
	{
		if (!tok.GetToken(texture, sizeof(texture)))
			break;

		if (!tok.GetToken(material, sizeof(material)))
			break;

		StringNormalize(texture, texture, sizeof(texture));

		phMaterialMgr::Id id = MATERIALMGR.FindMaterialId(material);

		if (id == phMaterialMgr::MATERIAL_NOT_FOUND)
		{
			Errorf("Texture->Material map file specified unknown material \"%s\", using default", material);
			id = phMaterialMgr::DEFAULT_MATERIAL_ID;
		}

		m_TextureMaterialMap.Insert(ConstString(texture), id);
	}

	stream->Close();
}


phMaterialMgr::Id rageTextureMaterialMapFile::GetMaterialForTexture(const char* textureName) const
{
	char baseName[128];
	ASSET.BaseName(baseName, sizeof(baseName), textureName);

	const phMaterialMgr::Id* id = m_TextureMaterialMap.Access(baseName);

	if (id == NULL)
	{
		Errorf("No material specified for texture \"%s\", using default material", baseName);
		return phMaterialMgr::DEFAULT_MATERIAL_ID;
	}
	else
	{
		return *id;
	}
}


rageFlagImageListFile::rageFlagImageListFile(const char* listFileName)
{
	fiStream *stream = ASSET.Open(listFileName, "lst");
	if (!stream)
	{
		Warningf("Unable to open flag image list \"%s.lst\", not applying any flags", listFileName);
		return;
	}

	m_FlagImageList.Reserve(1024);

	fiAsciiTokenizer tok;
	tok.Init(stream->GetName(), stream);

	char flagName[256], imageFile[256];

	phMaterialFlags loaded = 0;

	Printf("\n");

	for ( ; ; )
	{
		if (!tok.GetToken(imageFile, sizeof(imageFile)))
			break;

		if (!tok.GetToken(flagName, sizeof(flagName)))
			break;

		phMaterialFlags flag = MATERIALMGRFLAG.GetFlagByName(flagName);
		if (!flag)
			Errorf("Unknown material flag \"%s\" specified in flag image list", flagName);
		AssertMsg((flag & loaded) == 0, "You can only specify one image for each flag");
		loaded |= flag;

		flagImagePair& pair = m_FlagImageList.Append();
		pair.flag = flag;
		pair.image = m_ImageCache.LoadImage(imageFile);

		if (pair.image)
		{
			Printf("Assigning flag %s from texture %s (%dx%d)\n",
				flagName, imageFile, pair.image->GetHeight(), pair.image->GetWidth());
		}
		else
		{
			Errorf("Unable to open texture %s", imageFile);
		}
	}

	stream->Close();
}


rageFlagImageListFile::~rageFlagImageListFile()
{
	for (int i = 0, n = m_FlagImageList.GetCount(); i < n; i++)
		if (m_FlagImageList[i].image)
			m_FlagImageList[i].image->Release();
}


rageBoundBuilder::rageBoundBuilder() : m_TextureMaterialMap(NULL), m_Simplify(false)
{
	m_MaxHeight = 8;//phOctreeCell::GetDefaultMaxHeight();
	m_MaxPerCell = 15;//phOctreeCell::GetDefaultMaxPerCell();
	m_CellSize = 25.0f;
	m_OutputGrid = false;
	m_OutputBVH = false;
}


rageBoundBuilder::~rageBoundBuilder()
{
	Reset();
}


void rageBoundBuilder::BuildBound()
{
	rmcDrawable* drawable = LoadDrawable(m_TypeFileName);
	AssignShaderMaterials(drawable->GetShaderGroup());
	delete drawable;

	AssignMaterials();

	phBoundGeometry* bound = CreateBound();

	if (m_OutputBVH)
	{
		phBoundGrid* grid = CreateBVHGrid(bound);
		delete bound;

		Printf("\nsaving bound to \"%s\"...\n", m_OutputFileName.m_String);
		phBound::Save(m_OutputFileName,grid);
		delete grid;

		Printf("\nattempting to reload new bound file...\n");
		grid = new phBoundGrid;
		if (grid->Load(m_OutputFileName))
		{
			Printf("\nsuccess!\n");
		}

		delete grid;
	} 
	else
	{
		Printf("\nsaving bound to \"%s\"...\n", m_OutputFileName.m_String);
		phBound::Save(m_OutputFileName,bound);
		delete bound;

		Printf("\nattempting to reload new bound file...\n");
		bound = new phBoundGeometry;
		if (bound->Load(m_OutputFileName))
			Printf("\nsuccess!\n");
		delete bound;
	}

	Reset();
}


rmcDrawable* rageBoundBuilder::LoadDrawable(const char* typeFileName)
{
	atFunctor2<bool,mshMesh&,const char*> functor;
	functor.Reset<rageBoundBuilder, &rageBoundBuilder::MeshLoaderHook>(this);

	m_Meshes.Reserve(1000);
	m_TotalTri = m_TotalVert = 0;
	grmModel::SetMeshLoaderHook(functor);

	Displayf("Loading Type file = (%s)", typeFileName);

	char typeFilePath[512];
	ASSET.RemoveNameFromPath(typeFilePath, 512, typeFileName);
	Displayf("setting the ASSET Path to = (%s)", typeFilePath);
	ASSET.PushFolder(typeFilePath);

	rmcDrawable* drawable = new rmcDrawable;
	if (!drawable->Load(typeFileName))
	{
		ASSET.PopFolder(); // Clear off the ASSET stack...
		Quitf("Couldn't load file '%s%s'!", ASSET.GetPath(), typeFileName);
	}

	ASSET.PopFolder();

	grmModel::RemoveMeshLoaderHook();

	int numModel = drawable->GetLodGroup().GetLod(0).GetCount();
	Printf("\nloaded %d models\n", numModel);
	for (int model = 0; model < numModel; model++)
	{
        const grmModel* m = drawable->GetLodGroup().GetLod(0).GetModel(model);
        if (m)
        {
		    int numGeo = m->GetGeometryCount();
		    Printf("\t%d geometries\n", numGeo);
        }
	}

	return drawable;
}


void rageBoundBuilder::AssignShaderMaterials(const grmShaderGroup& shaderGroup)
{
	int numShaders = shaderGroup.GetCount();
	Printf("\nloaded %d shaders\n", numShaders);
	m_ShaderMaterials.Resize(numShaders);

	for (int shaderIndex = 0; shaderIndex < numShaders; shaderIndex++)
	{
		grmShader& shader = shaderGroup.GetShader(shaderIndex);

		m_ShaderMaterials[shaderIndex].channelMap = NULL;
		m_ShaderMaterials[shaderIndex].materials[0] = phMaterialMgr::DEFAULT_MATERIAL_ID;
		m_ShaderMaterials[shaderIndex].materials[1] = phMaterialMgr::DEFAULT_MATERIAL_ID;
		m_ShaderMaterials[shaderIndex].materials[2] = phMaterialMgr::DEFAULT_MATERIAL_ID;

		Printf("\tshader %d\n", shaderIndex);

		grcEffectVar var;

		if ((var = shader.LookupVar("DiffuseTex",false)) != grcevNONE)
		{
			grcTexture *tex;
			shader.GetVar(var,tex);
			m_ShaderMaterials[shaderIndex].materials[0] =
				m_TextureMaterialMap->GetMaterialForTexture(tex->GetName());
			Printf("\t\tDiffuseTex %s (material %s)\n", tex->GetName(),
				MATERIALMGR.GetMaterial(m_ShaderMaterials[shaderIndex].materials[0]).GetName());

		}

		if ((var = shader.LookupVar("BlendDiffuseTex2",false)) != grcevNONE)
		{
			grcTexture *tex;
			shader.GetVar(var,tex);
			m_ShaderMaterials[shaderIndex].materials[1] =
				m_TextureMaterialMap->GetMaterialForTexture(tex->GetName());
			Printf("\t\tBlendDiffuseTex2 %s (material %s)\n", tex->GetName(),
				MATERIALMGR.GetMaterial(m_ShaderMaterials[shaderIndex].materials[1]).GetName());
		}

		if ((var = shader.LookupVar("BlendDiffuseTex3",false)) != grcevNONE)
		{
			grcTexture *tex;
			shader.GetVar(var,tex);
			m_ShaderMaterials[shaderIndex].materials[2] =
				m_TextureMaterialMap->GetMaterialForTexture(tex->GetName());
			Printf("\t\tBlendDiffuseTex3 %s (material %s)\n", tex->GetName(),
				MATERIALMGR.GetMaterial(m_ShaderMaterials[shaderIndex].materials[2]).GetName());
		}

		if ((var = shader.LookupVar("BlendAlphaTex",false)) != grcevNONE)
		{
			grcTexture *tex;
			shader.GetVar(var,tex);
			m_ShaderMaterials[shaderIndex].channelMap = m_ImageCache.LoadImage(tex->GetName());

			Printf("\t\tBlendAlphaTex %s (%dx%d)\n", tex->GetName(),
				m_ShaderMaterials[shaderIndex].channelMap->GetHeight(),
				m_ShaderMaterials[shaderIndex].channelMap->GetWidth());
		}
	}
}


void rageBoundBuilder::AssignMaterials()
{
	m_MaterialAssignment.Resize(m_TotalTri);

	int tri = 0;

	int numMesh = m_Meshes.GetCount();
	for (int mesh = 0; mesh < numMesh; mesh++)
	{
		int numMtl = m_Meshes[mesh]->GetMtlCount();

		for (int mtl = 0; mtl < numMtl; mtl++)
		{
			mshMaterial& mshMtl = m_Meshes[mesh]->GetMtl(mtl);

			int shaderIndex;

			if (mshMtl.Name[0] == '#')
				shaderIndex = (u16) atoi(mshMtl.Name + 1);
			else
				shaderIndex = (u16) atoi(mshMtl.Name);

			mshMaterial::TriangleIterator i = mshMtl.BeginTriangles(), e = mshMtl.EndTriangles();
			for ( ; i != e; ++i)
			{
				int a, b, c;
				i.GetVertIndices(a, b, c);

				if (a != b && b != c && c != a)
				{
					m_MaterialAssignment[tri++] = PickMaterial(mshMtl, a, b, c, m_ShaderMaterials[shaderIndex]);
				}
			}
		}
	}
}


void rageBoundBuilder::SumContainedTexels(const mshMaterial& mshMtl, int a, int b, int c, const grcImage& image,
						u64& totalValue, u64& totalTexels)
{
	const Vector2* uv[3] =
	{
		&mshMtl.GetTexCoord(a, m_UVSet),
		&mshMtl.GetTexCoord(b, m_UVSet),
		&mshMtl.GetTexCoord(c, m_UVSet)
	};

	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 2; j++)
			Assert((*uv[i])[j] >= 0 && (*uv[i])[j] <= 1);

	int minYIndex = MinimumIndex(uv[0]->y, uv[1]->y, uv[2]->y);
	int maxYIndex = MaximumIndex(uv[0]->y, uv[1]->y, uv[2]->y);
	int midYIndex = 3 - minYIndex - maxYIndex;

	int width = image.GetWidth() - 1;
	int height = image.GetHeight() - 1;

	int minY = int(uv[minYIndex]->y * height + 0.5f);
	int maxY = int(uv[maxYIndex]->y * height + 0.5f);

	if (minY == maxY)
	{
		int y = int(uv[minYIndex]->y * height + 0.5f);

		int minX = int(Min(uv[0]->x, uv[1]->x, uv[2]->x) * width + 0.5f);
		int maxX = int(Max(uv[0]->x, uv[1]->x, uv[2]->x) * width + 0.5f);

		for (int x = minX; x <= maxX; x++)
		{
			u32 pixel = image.GetPixel(x, height - y);
			totalValue += (((pixel >> 16) & 0xFF) + ((pixel >> 8) & 0xFF) + (pixel & 0xFF));
		}

		totalTexels += maxX - minX + 1;
	}
	else
	{
		int midY = int(uv[midYIndex]->y * height + 0.5f);

		float slopeMinMax = (uv[maxYIndex]->x - uv[minYIndex]->x) / (maxY - minY);

		if (midY > minY)
		{
			float slopeMinMid = (uv[midYIndex]->x - uv[minYIndex]->x) / (midY - minY);

			if (maxY == midY)
				midY++;

			for (int y = minY; y < midY; y++)
			{
				int minX = int(width * (uv[minYIndex]->x + slopeMinMid * (y - minY)) + 0.5f);
				int maxX = int(width * (uv[minYIndex]->x + slopeMinMax * (y - minY)) + 0.5f);

				if (minX > maxX)
					SwapEm(minX, maxX);

				for (int x = minX; x <= maxX; x++)
				{
					u32 pixel = image.GetPixel(x, height - y);
					totalValue += (((pixel >> 16) & 0xFF) + ((pixel >> 8) & 0xFF) + (pixel & 0xFF));
				}

				totalTexels += maxX - minX + 1;
			}
		}

		if (maxY > midY)
		{
			float slopeMidMax = (uv[maxYIndex]->x - uv[midYIndex]->x) / (maxY - midY);

			for (int y = midY; y <= maxY; y++)
			{
				int minX = int(width * (uv[midYIndex]->x + slopeMidMax * (y - midY)) + 0.5f);
				int maxX = int(width * (uv[minYIndex]->x + slopeMinMax * (y - minY)) + 0.5f);

				if (minX > maxX)
					SwapEm(minX, maxX);

				for (int x = minX; x <= maxX; x++)
				{
					u32 pixel = image.GetPixel(x, height - y);
					totalValue += (((pixel >> 16) & 0xFF) + ((pixel >> 8) & 0xFF) + (pixel & 0xFF));
				}

				totalTexels += maxX - minX + 1;
			}
		}
	}
}


phMaterialMgr::Id rageBoundBuilder::SetFlags(const mshMaterial& mshMtl, phMaterialMgr::Id materialId, int a, int b, int c)
{
	materialId = MATERIALMGRFLAG.SetDefaultFlags(materialId);

	int numFlags = m_FlagImageList->GetNumFlags();

	for (int i = 0; i < numFlags; i++)
	{
		Assert(m_FlagImageList->GetImage(i));

		u64 totalValue = 0, totalTexels = 0;

		SumContainedTexels(mshMtl, a, b, c, *m_FlagImageList->GetImage(i), totalValue, totalTexels);

		Assert(totalTexels > 0);

		if (totalValue > 0)
			materialId = MATERIALMGRFLAG.AddFlags(materialId, m_FlagImageList->GetFlag(i));
		else
			materialId = MATERIALMGRFLAG.ClearFlags(materialId, m_FlagImageList->GetFlag(i));
	}

	return materialId;
}


u8 rageBoundBuilder::PickMaterial(const mshMaterial& mshMtl, int a, int b, int c, shaderMaterials& materials)
{
	phMaterialMgr::Id materialId = materials.materials[0];

	if (materials.channelMap)
	{
		u64 totalValue = 0, totalTexels = 0;

		SumContainedTexels(mshMtl, a, b, c, *materials.channelMap, totalValue, totalTexels);

		Assert(totalTexels > 0);

		u32 avgValue = u32(totalValue / totalTexels);

		if (avgValue < 256)
			materialId = materials.materials[0];
		else if (avgValue < 512)
			materialId = materials.materials[1];
		else
			materialId = materials.materials[2];
	}
	
	materialId = SetFlags(mshMtl, materialId, a, b, c);

	const u8* mtlIndex = m_MaterialIndexMap.Access(materialId);

	if (mtlIndex == NULL)
	{
		Assert(m_MaterialIndexMap.GetNumUsed() < u8(-1) - 1); // we reserve u8(-1) to mean bad index, so this must be < u8(-1) - 1
		return m_MaterialIndexMap.Insert(materialId, u8(m_MaterialIndexMap.GetNumUsed())).data;
	}
	else
	{
		return *mtlIndex;
	}
}


phBoundGeometry* rageBoundBuilder::CreateBound()
{
	Printf("\ncreating bound with %d verts, %d triangles, %d materials\n",
		m_TotalVert, m_TotalTri, m_MaterialIndexMap.GetNumUsed());

	phBoundGeometry* bound = new phBoundGeometry;
#if HACK_GTA4
	bound->Init(m_TotalVert, m_MaterialIndexMap.GetNumUsed(), 0, m_TotalTri, 0);
#else
	bound->Init(m_TotalVert, m_MaterialIndexMap.GetNumUsed(), m_TotalTri);
#endif

	atMap<phMaterialMgr::Id, u8>::Iterator matIter = m_MaterialIndexMap.CreateIterator();
	for (matIter.Start(); !matIter.AtEnd(); matIter.Next())
		bound->SetMaterialId(matIter.GetData(), matIter.GetKey());

	int vertOffset = 0, polyOffset = 0;

	int numMesh = m_Meshes.GetCount();
	for (int mesh = 0; mesh < numMesh; mesh++)
	{
		int numMtl = m_Meshes[mesh]->GetMtlCount();

		for (int mtl = 0; mtl < numMtl; mtl++)
		{
			mshMaterial& mshMtl = m_Meshes[mesh]->GetMtl(mtl);

			int numVert = mshMtl.GetVertexCount();

			for (int vert = 0; vert < numVert; vert++)
				bound->SetVertex(vert + vertOffset, mshMtl.GetPos(vert));

			mshMaterial::TriangleIterator i = mshMtl.BeginTriangles(), e = mshMtl.EndTriangles();
			for ( ; i != e; ++i)
			{
				int a, b, c;
				i.GetVertIndices(a, b, c);

				if (a != b && b != c && c != a)
				{
					phPolygon poly;
					poly.InitTriangle(phPolygon::Index(a + vertOffset),
						phPolygon::Index(b + vertOffset),
						phPolygon::Index(c + vertOffset),
						bound->GetVertex(phPolygon::Index(a + vertOffset)),
						bound->GetVertex(phPolygon::Index(b + vertOffset)),
						bound->GetVertex(phPolygon::Index(c + vertOffset)));
					poly.SetMaterialIndex(m_MaterialAssignment[polyOffset]);
					bound->SetPolygon(polyOffset++, poly);
				}
			}

			vertOffset += numVert;
		}
	}

	// This is fast now so always do it.
	if (1 || m_Simplify)
	{
		bound->ConvertTrianglesToQuads();
		Printf("after simplification: %d verts, %d polygons\n", bound->GetNumVertices(), bound->GetNumPolygons());
	}

	bound->CalculateGeomExtents();

	return bound;
}


phBoundGrid* rageBoundBuilder::CreateBVHGrid(phBoundGeometry* bound)
{
	Printf("Creating BVH grid...\n");

	phBoundGrid* grid = new phBoundGrid;
	int minX = int(floorf(bound->GetBoundingBoxMin().GetXf()/m_CellSize));
	int maxX = int(floorf(bound->GetBoundingBoxMax().GetXf()/m_CellSize));
	int minZ = int(floorf(bound->GetBoundingBoxMin().GetZf()/m_CellSize));
	int maxZ = int(floorf(bound->GetBoundingBoxMax().GetZf()/m_CellSize));

	Printf("Initializing BVH grid (%d,%d) -> (%d,%d)...\n",minX,minZ,maxX,maxZ);
	grid->InitGrid<phBoundBVH>(m_CellSize,minX,maxX,minZ,maxZ);

	Printf("Adding geometry to grid...\n");
#if __TOOL
	grid->AddGeometry(bound);
#endif

	return grid;
}

void rageBoundBuilder::Reset()
{
	m_MaterialIndexMap.Kill();
	m_MaterialAssignment.Reset();

	for (int i = 0, n = m_Meshes.GetCount(); i < n; i++)
		delete m_Meshes[i];
	m_Meshes.Reset();

	m_TotalTri = m_TotalVert = 0;

	for (int i = 0, n = m_ShaderMaterials.GetCount(); i < n; i++)
		if (m_ShaderMaterials[i].channelMap)
			m_ShaderMaterials[i].channelMap->Release();
	m_ShaderMaterials.Reset();

	m_ImageCache.Release();
}


bool rageBoundBuilder::MeshLoaderHook(mshMesh& mesh, const char*)
{
	m_Meshes.Append() = &mesh;

	int numMesh = m_Meshes.GetCount();

	Printf("\nmesh %d\n", numMesh - 1);

	int numMtl = mesh.GetMtlCount();
	Printf("\t%d materials\n", numMtl);

	for (int mtl = 0; mtl < numMtl; mtl++)
	{
		mshMaterial& mshMtl = mesh.GetMtl(mtl);

		int numTri = 0, numDegen = 0, numVert = mshMtl.GetVertexCount();

		mshMaterial::TriangleIterator i = mshMtl.BeginTriangles(), e = mshMtl.EndTriangles();
		for ( ; i != e; ++i)
		{
			int a, b, c;
			i.GetVertIndices(a, b, c);

			if (a != b && b != c && c != a)
				numTri++;
			else
				numDegen++;
		}
		Printf("\t\t%s: %d triangles (ignored %d), %d verts\n", mshMtl.Name.m_String, numTri, numDegen, numVert);

		m_TotalTri += numTri;
		m_TotalVert += numVert;
	}

	Assert(m_TotalVert < phPolygon::Index(-1));

	// Must return false to prevent the drawable loader from
	// deleting the mesh after this function returns.
	return false;
}
