// 
// /rageBoundBuilder.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RAGE_BOUND_BUILDER_H
#define RAGE_BOUND_BUILDER_H

#include "phcore/materialmgrflag.h"
#include "phbound/boundbvh.h"
#include "phbound/boundgrid.h"

namespace rage
{

class mshMesh;
struct mshMaterial;
class rmcDrawable;
class grcImage;
class grmShaderGroup;
class phBoundGeometry;

// PURPOSE:	Load and keeps resident R8G8B8A8 versions of textures.
class rageImageCache
{
public:
	~rageImageCache() { Release(); }

	// PURPOSE: Decrements the reference count of all images in the cache and
	//			removes them from the cache.  This will not free the images if
	//			other code has incremented the reference count.
	void Release();

	// PURPOSE: Returns an image from the cache if it is already loaded, otherwise
	//			loads an image from a file, converts it to R8G8B8A8, and stores
	//          it in the cache.
	grcImage* LoadImage(const char* name);

private:
	atMap<ConstString, grcImage*> m_ImageCache;
};


// PURPOSE: Interface for classes that specify how to assign materials to textures.
class rageTextureMaterialMap
{
public:
	virtual ~rageTextureMaterialMap() { }

	// PURPOSE: Returns the material ID that should be used for the named texture.
	virtual phMaterialMgr::Id GetMaterialForTexture(const char* textureName) const = 0;
};


/* PURPOSE: Concrete implementation of rageTextureMaterialMap that loads the texture
			to material mapping from a text file.

NOTE: The file would look like this:
texture1	material1
texture2	material2
...
*/
class rageTextureMaterialMapFile : public rageTextureMaterialMap
{
public:
	// PURPOSE: Loads the texture material map from the given file.
	rageTextureMaterialMapFile(const char* mapFileName);

	// PURPOSE: Returns the material ID that should be used for the named texture.
	virtual phMaterialMgr::Id GetMaterialForTexture(const char* textureName) const;

private:
	atMap<ConstString, phMaterialMgr::Id>	m_TextureMaterialMap;
};


// PURPOSE: Interface for classes that store a list of material flags
//			and the images that are used to assign the flag to the terrain.
class rageFlagImageList
{
public:
	virtual ~rageFlagImageList() { }

	// PURPOSE: Returns the number of flags in the list.
	virtual int GetNumFlags() const = 0;

	// PURPOSE: Returns the flag for the given index.
	virtual phMaterialFlags GetFlag(int i) const = 0;

	// PURPOSE: Returns the image for the given index.
	virtual const grcImage* GetImage(int i) const = 0;
};


/* PURPOSE: Concrete implementation of rageFlagImageList that loads the flag
			and image list from a text file.

NOTE: The file would look like this:
imageFileName1	flagName1
imageFileName2	flagName2
...
*/
class rageFlagImageListFile : public rageFlagImageList
{
public:
	rageFlagImageListFile(const char* listFileName);
	~rageFlagImageListFile();

	virtual int GetNumFlags() const					{ return m_FlagImageList.GetCount(); }
	virtual phMaterialFlags GetFlag(int i) const	{ return m_FlagImageList[i].flag; }
	virtual const grcImage* GetImage(int i) const	{ return m_FlagImageList[i].image; }

private:
	struct flagImagePair
	{
		phMaterialFlags flag;
		const grcImage* image;
	};

	atArray<flagImagePair>	m_FlagImageList;
	rageImageCache			m_ImageCache;
};


/* 
PURPOSE:
	This class creates a bound (.bnd) file from an entity's geometry, assigning
	physics materials based on the textures and an optional channel map, and setting
	material flags based on a set of UV-mapped textures that indicate which polygons
	each flag applies to.
*/

class rageBoundBuilder
{
public:
	rageBoundBuilder();
	virtual ~rageBoundBuilder();

	// PURPOSE: Main function that runs the bound-building process.
	void BuildBound();

	// PURPOSE: Specifies which UV set index the channel map and flag textures should
	//			be mapped with.
	void SetUVSet(int uvSet)							{ m_UVSet = uvSet; }

	// PURPOSE: Specifies the .type file name which should be loaded.
	void SetTypeFileName(const char* typeFileName)		{ m_TypeFileName = typeFileName; }

	// PURPOSE: Specifies the .bnd output file name.
	void SetOutputFileName(const char* outputFileName)	{ m_OutputFileName = outputFileName; }

	// PURPOSE: Enables triangle->quad simplification of the bounds.
	void SetSimplify(bool simplify)						{ m_Simplify = simplify; }

	// PURPOSE: Sets the object used to assign materials to texture names.
	void SetTextureMaterialMap(const rageTextureMaterialMap& textureMaterialMap)
	{ m_TextureMaterialMap = &textureMaterialMap; }

	// PURPOSE: Sets the object that stores the list of flag and associated
	//          texture maps.
	void SetFlagImageList(const rageFlagImageList& flagImageList)
	{ m_FlagImageList = &flagImageList; }

	void SetOTGridParams(bool outputGrid, float cellSize, int maxHeight, int maxPerCell)
	{ m_OutputGrid = outputGrid, m_CellSize = cellSize, m_MaxHeight = maxHeight, m_MaxPerCell = maxPerCell; }

	void SetBVHParams(bool outputBVH)
	{ m_OutputBVH = outputBVH; }

protected:
	// PURPOSE: Helper class that stores the material IDs for the textures used by
	//          a shader and an optional channel map image used to pick a texture for
	//			a specific polygon.
	struct shaderMaterials
	{
		grcImage* channelMap;
		phMaterialMgr::Id materials[3];
	};

	// PURPOSE: Helper function loads the drawable, using the MeshLoaderHook to grab
	//			the meshes during the load.
	rmcDrawable* LoadDrawable(const char* typeFileName);

	// PURPOSE: Helper function that populates the shader material list based on the
	//			shader variables loaded from the drawable.
	void AssignShaderMaterials(const grmShaderGroup& shaderGroup);

	// PURPOSE: Helper function that assigns a material and set of flags to every polygon
	//			in the mesh.
	void AssignMaterials();

	// PURPOSE: Helper function that maps the given triangle vert indicies into texel
	//			coordinates and sums the total value of all texels contained in the triangle.
	void SumContainedTexels(const mshMaterial& mshMtl, int a, int b, int c, const grcImage& image,
							u64& totalValue, u64& totalTexels);

	// PURPOSE: Looks at each flag and image in the flag image list and determines which flags
	//			should be assigned to the triangle with the given vert indicies.
	phMaterialMgr::Id SetFlags(const mshMaterial& mshMtl, phMaterialMgr::Id materialId, int a, int b, int c);

	// PURPOSE: Determines the material and flags for the triangle with the given vert indicies
	//			and returns the index into the material table that should be assigned to the triangle.
	u8 PickMaterial(const mshMaterial& mshMtl, int a, int b, int c, shaderMaterials& materials);

	// PURPOSE: Creates a bound from the set of used materials and the mesh verts and triangles.
	phBoundGeometry* CreateBound();

	// PURPOSE: Creates a new BVH grid from the bound.
	phBoundGrid* CreateBVHGrid(phBoundGeometry* bound);

	// PURPOSE: Callback that captures the meshes while loading the drawable.
	bool MeshLoaderHook(mshMesh& mesh, const char*);

	// PURPOSE: Cleanup function releases all allocated memory.
	void Reset();

	// Arguments to control bound construction
	ConstString											m_TypeFileName;
	ConstString											m_OutputFileName;

	const rageTextureMaterialMap*						m_TextureMaterialMap;
	const rageFlagImageList*							m_FlagImageList;

	int													m_UVSet;
	bool												m_Simplify;


	// Member variables used during bound construction
	atArray<shaderMaterials>							m_ShaderMaterials;
	atMap<phMaterialMgr::Id, u8>						m_MaterialIndexMap;
	atArray<u8,0,unsigned int>							m_MaterialAssignment;
	rageImageCache										m_ImageCache;


	bool												m_OutputBVH;

	// Configuration for OT grid output
	bool												m_OutputGrid;
	int													m_MaxHeight, m_MaxPerCell;
	float												m_CellSize;


	// Populated by MeshLoaderHook when loading the drawable
	atArray<mshMesh*>									m_Meshes;
	int													m_TotalTri, m_TotalVert;
};


} // namespace rage

#endif // RAGE_BOUND_BUILDER_H
