set ARCHIVE=rageBoundBuilder
set FILES=rageBoundBuilder

set TESTERS=rageBoundBuilderApp
set PH_LIBS=curve phbound phcore phbullet
set SAMPLE_LIBS=sample_rmcore %RAGE_SAMPLE_GRCORE_LIBS% parser parsercore
set LIBS=%RAGE_CORE_LIBS% %RAGE_GFX_LIBS% %SAMPLE_LIBS% %PH_LIBS% 
set XPROJ=%RAGE_DIR%\base\src %RAGE_DIR%\base\samples
