﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.OS;
using System.Diagnostics;
using System.IO;
using RSG.ManagedRage.ClipAnimation;
using RSG.SourceControl.Perforce;
using RSG.Base.Configuration;

namespace cliplocator
{
    class Program
    {
        private static readonly String OPTION_LOCAL = "local";
        private static readonly String OPTION_P4 = "p4";

        static int RunProcess(string strExe, string strArgs)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.Arguments = strArgs;
            startInfo.FileName = strExe;
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.CreateNoWindow = true;
            startInfo.RedirectStandardOutput = true;
            startInfo.UseShellExecute = false;

            Process proc = Process.Start(startInfo);
            String output = proc.StandardOutput.ReadToEnd();
            proc.WaitForExit();

            return proc.ExitCode;
        }

        static void ProcessClip(CommandOptions options, String file, bool open_local, bool open_p4, RSG.ManagedRage.ClipAnimation.MClip.ClipType type)
        {
            MClip clip = new MClip(type);
            clip.Load(file);
            {
                MProperty prop = clip.FindProperty("FBXFile_DO_NOT_RESOURCE");
                if (prop != null)
                {
                    if (prop.GetPropertyAttributes().Count > 0)
                    {
                        MPropertyAttributeString un = (MPropertyAttributeString)prop.GetPropertyAttributes()[0];
                        if (un != null)
                        {
                            string fbx = un.GetString();
                            fbx = fbx.ToLower();
                            fbx = fbx.Replace("$(art)", options.Branch.Art);

                            if (open_local)
                                RunProcess("explorer", "/select, " + fbx);
                            else if (open_p4)
                            {
                                P4 p4 = new P4();
                                p4.Connect();

                                ICollection<String> configFiles = new List<String>();
                                configFiles.Add(fbx);

                                FileMapping[] filesMapped = FileMapping.Create(p4, configFiles.ToArray());
                                RunProcess("p4v", "-s " + filesMapped[0].DepotFilename);
                            }
                        }
                    }
                }
            }   
        }

        static void Main(string[] args)
        {
            LongOption[] opts = new LongOption[] {
                new LongOption(OPTION_LOCAL, LongOption.ArgType.None, 
                    "convert to xml format."),
                new LongOption(OPTION_P4, LongOption.ArgType.None, 
                    "convert to clip format."),
            };

            CommandOptions options = new CommandOptions(args, opts);

            bool open_local = options.ContainsOption(OPTION_LOCAL);
            bool open_p4 = options.ContainsOption(OPTION_P4);
            string[] trailing = options.TrailingArguments.ToArray();

            MClip.Init();

            foreach (string file in trailing)
            {
                if (Path.GetExtension(file).ToLower() == ".clip")
                {
                    ProcessClip(options, file, open_local, open_p4, MClip.ClipType.Normal);
                }
                else if (Path.GetExtension(file).ToLower() == ".clipxml")
                {
                    ProcessClip(options, file, open_local, open_p4, MClip.ClipType.XML);
                }
                else if (Path.GetExtension(file).ToLower() == ".anim")
                {
                    string clip = Path.ChangeExtension(file.ToLower(), ".clip");

                    if (File.Exists(clip))
                    {
                        ProcessClip(options, clip, open_local, open_p4, MClip.ClipType.Normal);
                        continue;
                    }

                    clip = Path.ChangeExtension(file.ToLower(), ".clipxml");

                    if (File.Exists(clip))
                    {
                        ProcessClip(options, clip, open_local, open_p4, MClip.ClipType.XML);
                        continue;
                    }
                }
            }
        }
    }
}
