﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace RSG.PlaybackControl
{
    /// <summary>
    /// The PlaybackControlViewModel is responsible for managing the flow of data between a PlaybackControl instance and an IPlaybackModel derived instance.
    /// It's main objective is ensure that as little information as possible is pushed in each direction.
    /// It understands basic dependencies such as that Phase and Frame are linked, but should work for any implementation of IPlaybackModel.
    /// Changes here should be carefully considered, especially once there are multiple implementations of IPlaybackModel.  JWR
    /// </summary>
    public class PlaybackControlViewModel : INotifyPropertyChanged
    {
        #region Events
        public event EventHandler TagAddedCommandFired;
        public event EventHandler PhasePositionChanged;
        #endregion

        public PlaybackControlViewModel(IPlaybackModel model)
        {
            model_ = model;
            model_.CanBeginPlaybackChanged += new Action(OnCanBeginPlaybackChanged);
            model_.IsPlaybackActiveChanged += new Action(OnIsPlaybackActiveChanged);
            model_.IsPlayingChanged += new Action(OnIsPlayingChanged);
            model_.FrameLimitsChanged += new Action(OnFrameLimitsChanged);

            updateTimer_ = new System.Windows.Threading.DispatcherTimer();
            updateTimer_.Interval = TimeSpan.FromMilliseconds(33);

            ConnectCommand = new SimpleCommand(OnConnectCommand, CanConnect);
            DisconnectCommand = new SimpleCommand(OnDisconnectCommand, CanDisconnect);
            AddTagCommand = new SimpleCommand(OnTagAddedCommand);
            RefreshClipCommand = new SimpleCommand(OnRefreshCommand, CanDisconnect);
            RewindCommand = new SimpleCommand(OnRewindCommand, IsPlaybackActive);
            PreviousFrameCommand = new SimpleCommand(OnPreviousFrameCommand, IsPlaybackActive);
            PlayBackwardsCommand = new SimpleCommand(OnPlayBackwardsCommand, CanBeginPlayback);
            PauseCommand = new SimpleCommand(OnPauseCommand, CanPausePlayback);
            PlayForwardsCommand = new SimpleCommand(OnPlayForwardsCommand, CanBeginPlayback);
            NextFrameCommand = new SimpleCommand(OnNextFrameCommand, IsPlaybackActive);
            FastForwardsCommand = new SimpleCommand(OnFastForwardsCommand, IsPlaybackActive);
        }

        void OnCanBeginPlaybackChanged()
        {
            CommandManager.InvalidateRequerySuggested();
        }

        void OnIsPlaybackActiveChanged()
        {
            if (!model_.IsPlaybackActive)// Reset the sliders now that playback is over
            {
                PropertyChanged(this, new PropertyChangedEventArgs("Phase"));
                PropertyChanged(this, new PropertyChangedEventArgs("Frame"));
                PropertyChanged(this, new PropertyChangedEventArgs("Rate"));
            }

            CommandManager.InvalidateRequerySuggested();
        }

        void OnIsPlayingChanged()
        {
            if (!model_.IsPlaying)
            {
                PropertyChanged(this, new PropertyChangedEventArgs("Rate"));

                updateTimer_.Tick -= new EventHandler(OnUpdateTimerTick);
                updateTimer_.Stop();
            }

            CommandManager.InvalidateRequerySuggested();
            PropertyChanged(this, new PropertyChangedEventArgs("IsPaused"));
            PropertyChanged(this, new PropertyChangedEventArgs("IsPlaying"));
        }

        void OnFrameLimitsChanged()
        {
            PropertyChanged(this, new PropertyChangedEventArgs("FrameMinimum"));
            PropertyChanged(this, new PropertyChangedEventArgs("FrameMaximum"));
            PropertyChanged(this, new PropertyChangedEventArgs("FrameMiddle"));
            PropertyChanged(this, new PropertyChangedEventArgs("FrameTickFrequency"));
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        public ICommand ConnectCommand { get; private set; }
        public ICommand DisconnectCommand { get; private set; }
        public ICommand AddTagCommand { get; private set; }
        public ICommand RefreshClipCommand { get; private set; }
        public ICommand RewindCommand { get; private set; }
        public ICommand PreviousFrameCommand { get; private set; }
        public ICommand PlayBackwardsCommand { get; private set; }
        public ICommand PauseCommand { get; private set; }
        public ICommand PlayForwardsCommand { get; private set; }
        public ICommand NextFrameCommand { get; private set; }
        public ICommand FastForwardsCommand { get; private set; }

        private bool CanBeginPlayback(object parameter)
        {
            return model_.CanBeginPlayback;
        }

        private bool CanPausePlayback(object parameter)
        {
            return this.IsConnected && !IsPaused;
        }

        private bool IsPlaybackActive(object parameter)
        {
            return model_.IsPlaybackActive;
        }

        private void OnUpdateTimerTick(object sender, EventArgs e)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs("Phase"));
                PropertyChanged(this, new PropertyChangedEventArgs("Frame"));
                PropertyChanged(this, new PropertyChangedEventArgs("Rate"));
            }
        }

        private void OnConnectCommand(object parameter)
        {
            if (this.IsConnected)
                return;

            System.Windows.Input.Cursor previousCursor = Application.Current.MainWindow.Cursor;
            Application.Current.MainWindow.Cursor = System.Windows.Input.Cursors.Wait;
            try
            {
                model_.OnConnectButton();
            }
            catch
            { }
            finally
            {
                Application.Current.MainWindow.Cursor = previousCursor;
            }
            if (this.IsConnected == false)
            {
                MessageBox.Show("Encountered an error while trying to establish a connection to the game.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                PropertyChanged(this, new PropertyChangedEventArgs("IsPaused"));
            }
        }

        private void OnTagAddedCommand(object parameter)
        {
            if (this.TagAddedCommandFired != null)
                this.TagAddedCommandFired(this, EventArgs.Empty);
        }

        private void OnRefreshCommand(object parameter)
        {
            model_.OnRefreshButton();
        }

        internal void OnDisconnectCommand(object parameter)
        {
            model_.OnDisconnectButton();
        }

        private void OnRewindCommand(object parameter)
        {
            model_.OnRewindButton();
        }

        private void OnPreviousFrameCommand(object parameter)
        {
            float oldValue = this.Frame;
            float newFrame = oldValue;
            bool hitMimimum = false;
            while (this.Frame == oldValue)
            {
                if (hitMimimum == true)
                    break;

                newFrame = (float)Math.Ceiling(newFrame - 1);
                if (newFrame < this.FrameMinimum)
                {
                    newFrame = this.FrameMaximum;
                    hitMimimum = true;
                }
                this.Frame = newFrame;
            }
        }

        private void OnPlayBackwardsCommand(object parameter)
        {
            if (!model_.IsPlaybackActive || !model_.IsPlaying)
            {
                updateTimer_.Tick += new EventHandler(OnUpdateTimerTick);
                updateTimer_.Start();
            }

            model_.OnPlayBackwardsButton();
        }

        private void OnPauseCommand(object parameter)
        {
            model_.OnPauseButton();
        }

        private void OnPlayForwardsCommand(object parameter)
        {
            if (!model_.IsPlaybackActive || !model_.IsPlaying)
            {
                updateTimer_.Tick += new EventHandler(OnUpdateTimerTick);
                updateTimer_.Start();
            }

            model_.OnPlayButton();
        }

        private void OnNextFrameCommand(object parameter)
        {
            float oldValue = this.Frame;
            float newFrame = oldValue;
            bool hitMaximum = false;
            while (this.Frame == oldValue)
            {
                if (hitMaximum == true)
                    break;

                newFrame = (float)Math.Floor(newFrame + 1);
                if (newFrame > this.FrameMaximum)
                {
                    if (newFrame > Math.Round(this.FrameMaximum))
                    {
                        newFrame = 0.0f;
                        hitMaximum = true;
                    }
                    else if (Math.Round(oldValue) >= Math.Round(this.FrameMaximum))
                    {
                        newFrame = 0.0f;
                        hitMaximum = true;
                    }
                    else
                    {
                        newFrame = this.FrameMaximum;
                    }
                }
                this.Frame = newFrame;
            }
        }

        private void OnFastForwardsCommand(object parameter)
        {
            model_.OnFastForwardsButton();
        }

        public float Phase
        {
            get { return model_.Phase; }
            set
            {
                if (model_.Phase == value)
                    return;

                model_.Phase = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Frame"));
                PropertyChanged(this, new PropertyChangedEventArgs("Phase"));
                if (this.PhasePositionChanged != null)
                    this.PhasePositionChanged(this, EventArgs.Empty);
            }
        }

        public float Frame
        {
            get { return model_.Frame; }
            set 
            {
                float phase = value / (FrameMaximum - FrameMinimum);
                this.Phase = phase;
                //if (model_.Frame == value)
                //    return;

                //model_.Frame = value;
                //PropertyChanged(this, new PropertyChangedEventArgs("Frame"));
                //PropertyChanged(this, new PropertyChangedEventArgs("Phase"));
            }
        }

        public float FrameMinimum
        {
            get { return model_.FrameMinimum; }
        }

        public float FrameMaximum
        {
            get { return model_.FrameMaximum; }
        }

        public float FrameMiddle
        {
            get { return (FrameMaximum - FrameMinimum) * 0.5f; }
        }

        public float FrameTickFrequency
        {
            get { return model_.FrameTickFrequency; }
        }

        public float Rate
        {
            get { return model_.Rate; }
            set { model_.Rate = value; }
        }

        public bool IsPaused
        {
            get { return model_.IsPlaybackActive && !model_.IsPlaying; }
        }

        public bool IsPlaying
        {
            get { return model_.IsPlaybackActive && model_.IsPlaying; }
        }

        public bool IsConnected
        {
            get { return model_.IsConnected; }
        }

        public bool IsConnectedAndActive
        {
            get { return model_.IsConnected && model_.IsPlaybackActive; }
        }

        public bool CanConnect(object parameter)
        {
            return !IsConnected;
        }

        public bool CanDisconnect(object parameter)
        {
            return IsConnected;
        }

        private IPlaybackModel model_;
        private System.Windows.Threading.DispatcherTimer updateTimer_;
    }
}
