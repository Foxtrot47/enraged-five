﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RSG.PlaybackControl
{
    /// <summary>
    /// Interaction logic for PlaybackControl.xaml
    /// </summary>
    public partial class PlaybackControl : ContentControl
    {
        public PlaybackControl(PlaybackControlViewModel viewModel)
        {
            InitializeComponent();

            DataContext = viewModel;
            this.Unloaded += new RoutedEventHandler(PlaybackControl_Unloaded);
            Application.Current.Exit += new ExitEventHandler(Current_Exit);
        }

        void PlaybackControl_Unloaded(object sender, RoutedEventArgs e)
        {
            (this.DataContext as PlaybackControlViewModel).OnDisconnectCommand(null);
        }

        void Current_Exit(object sender, ExitEventArgs e)
        {
            if (this.DataContext is PlaybackControlViewModel)
                (this.DataContext as PlaybackControlViewModel).OnDisconnectCommand(null);
        }

		public PlaybackControl()
		{
            InitializeComponent();
            this.Unloaded += new RoutedEventHandler(PlaybackControl_Unloaded);
            Application.Current.Exit += new ExitEventHandler(Current_Exit);
		}
    }
}
