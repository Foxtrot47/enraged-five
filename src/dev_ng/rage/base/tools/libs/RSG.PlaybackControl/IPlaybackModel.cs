﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.PlaybackControl
{
    public interface IPlaybackModel
    {
        bool CanBeginPlayback { get; }
        bool IsPlaybackActive { get; }
        bool IsPlaying { get; }
        bool IsConnected { get; }

        float Phase { get; set; }
        float Frame { get; set; }
        float FrameMinimum { get; }
        float FrameMaximum { get; }
        float FrameTickFrequency { get; }
        float Rate { get; set; }

        event Action CanBeginPlaybackChanged;
        event Action IsPlaybackActiveChanged;
        event Action IsPlayingChanged;

        event Action FrameLimitsChanged;

        void OnConnectButton();
        void OnDisconnectButton();
        void OnRewindButton();
        void OnPreviousFrameButton();
        void OnPlayBackwardsButton();
        void OnPauseButton();
        void OnPlayButton();
        void OnNextFrameButton();
        void OnFastForwardsButton();
        void OnRefreshButton();
    }
}
