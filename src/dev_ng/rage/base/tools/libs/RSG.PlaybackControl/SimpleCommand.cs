﻿using System;
using System.Windows.Input;

namespace RSG.PlaybackControl
{
    public class SimpleCommand : ICommand
    {
        /// <summary>
        /// A static instance of a empty command that doesn't do anything.
        /// </summary>
        public static SimpleCommand EmptyCommand;

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="UniversalLogViewer.SimpleCommand"/>
        /// class.
        /// </summary>
        static SimpleCommand()
        {
            EmptyCommand = new SimpleCommand(null, (param) => { return false; });
        }

        public SimpleCommand(Action<object> execute)
        {
            execute_ = execute;
            predicate_ = null;
        }

        public SimpleCommand(Action<object> execute, Predicate<object> predicate)
        {
            execute_ = execute;
            predicate_ = predicate;
        }
        #endregion

        #region ICommand Members

        bool ICommand.CanExecute(object parameter)
        {
            if (predicate_ == null)
                return true;

            return predicate_(parameter);
        }

        event EventHandler ICommand.CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        void ICommand.Execute(object parameter)
        {
            execute_(parameter);
        }

        #endregion

        private readonly Action<object> execute_;
        private readonly Predicate<object> predicate_;
    }
}
