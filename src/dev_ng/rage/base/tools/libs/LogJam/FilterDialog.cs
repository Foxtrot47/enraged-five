using System;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Rockstar.LogJam
{
    public partial class FilterDialog:Form
    {
        private Log m_Log;

        public FilterDialog()
        {
            InitializeComponent();
        }

        public void SetLog(Log log)
        {
            m_Log = log;

            if(null == log)
            {
                this.Close();
            }
        }

        public string FilterText
        {
            get{return filterText.Text;}
            set{filterText.Text = string.IsNullOrEmpty(value) ? "" : value;}
        }

        public string ExcludeFilterText
        {
            get{return excludeFilterText.Text;}
            set{excludeFilterText.Text = string.IsNullOrEmpty(value) ? "" : value;}
        }

        private void FilterDialog_FormClosing(object sender,FormClosingEventArgs e)
        {
            if(this.Visible && e.CloseReason != CloseReason.FormOwnerClosing)
            {
                e.Cancel = true;
                this.Hide();
            }

            m_Log = null;
        }

        private void okButton_Click(object sender,EventArgs e)
        {
            if(null != m_Log
                && (this.FilterText.Length > 0 || this.ExcludeFilterText.Length > 0))
            {
                UseRegEx useRegex = this.regexCheckBox.Checked
                                    ? UseRegEx.Yes
                                    : UseRegEx.No;

                IgnoreCase ignoreCase = this.ignoreCaseCheckbox.Checked
                                        ? IgnoreCase.Yes
                                        : IgnoreCase.No;

                if(UseRegEx.Yes == useRegex)
                {
                    //Test the regex to see if it's valid
                    try
                    {
                        Regex.Match("", this.FilterText);
                    }
                    catch (ArgumentException)
                    {
                        MessageBox.Show(this,
                                        "Invalid regular expression\n" + this.FilterText,
                                        "Error",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);
                        return;
                    }
                }

                Filter filter = new Filter(this.FilterText,
                                            this.ExcludeFilterText,
                                            useRegex,
                                            ignoreCase);

                m_Log.Filter = filter;

                if(this.FilterText.Length > 0
                    && !this.filterText.Items.Contains(this.FilterText))
                {
                    this.filterText.Items.Add(this.FilterText);
                }

                if(this.ExcludeFilterText.Length > 0
                    && !this.excludeFilterText.Items.Contains(this.ExcludeFilterText))
                {
                    this.excludeFilterText.Items.Add(this.ExcludeFilterText);
                }

                //We initially created the combo box with one empty item
                //so it behaves as expected.
                //After adding a "real" item we can remove the empty item.
                if(this.filterText.Items.Count > 1
                    && this.filterText.Items[0].Equals(""))
                {
                    this.filterText.Items.RemoveAt(0);
                }

                if(this.excludeFilterText.Items.Count > 1
                    && this.excludeFilterText.Items[0].Equals(""))
                {
                    this.excludeFilterText.Items.RemoveAt(0);
                }
            }

            this.Hide();
        }

        private void cancelButton_Click(object sender,EventArgs e)
        {
            this.Hide();
        }

        private void FilterDialog_Activated(object sender,EventArgs e)
        {
            this.filterText.Focus();
        }

        private void FilterDialog_VisibleChanged(object sender, EventArgs e)
        {
            if(null != m_Log && this.Visible)
            {
                this.filterText.Focus();
                //this.filterText.Text = m_Log.Filter.Text;
                //this.excludeFilterText.Text = m_Log.Filter.ExcludeText;
                this.regexCheckBox.Checked = m_Log.Filter.UseRegEx;
                this.ignoreCaseCheckbox.Checked = m_Log.Filter.IgnoreCase;
            }
        }
    }
}