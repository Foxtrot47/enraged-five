using System;
using Rockstar.LogJam;

namespace Rockstar.LogJam
{
    //Used to copy debug output to the debug output text box.
    public class TraceListener : System.Diagnostics.TraceListener
    {
        Log m_Log;

        public TraceListener(Log log)
        {
            m_Log = log;
        }

        public override void Write(string s)
        {
            m_Log.Write(s);
        }

        public override void WriteLine(string s)
        {
            m_Log.WriteLine(s);
        }
    }
}
