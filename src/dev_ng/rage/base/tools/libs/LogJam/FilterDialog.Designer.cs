namespace Rockstar.LogJam {
    partial class FilterDialog {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.filterText = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.regexCheckBox = new System.Windows.Forms.CheckBox();
            this.ignoreCaseCheckbox = new System.Windows.Forms.CheckBox();
            this.excludeFilterText = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // okButton
            // 
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Location = new System.Drawing.Point(280, 8);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 0;
            this.okButton.Text = "Ok";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(280, 40);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 1;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // filterText
            // 
            this.filterText.FormattingEnabled = true;
            this.filterText.Items.AddRange(new object[] {
            ""});
            this.filterText.Location = new System.Drawing.Point(12, 24);
            this.filterText.Name = "filterText";
            this.filterText.Size = new System.Drawing.Size(262, 21);
            this.filterText.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Include:";
            // 
            // regexCheckBox
            // 
            this.regexCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.regexCheckBox.AutoSize = true;
            this.regexCheckBox.Location = new System.Drawing.Point(12, 98);
            this.regexCheckBox.Name = "regexCheckBox";
            this.regexCheckBox.Size = new System.Drawing.Size(144, 17);
            this.regexCheckBox.TabIndex = 4;
            this.regexCheckBox.Text = "Use Regular Expressions";
            this.regexCheckBox.UseVisualStyleBackColor = true;
            // 
            // ignoreCaseCheckbox
            // 
            this.ignoreCaseCheckbox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ignoreCaseCheckbox.AutoSize = true;
            this.ignoreCaseCheckbox.Location = new System.Drawing.Point(12, 121);
            this.ignoreCaseCheckbox.Name = "ignoreCaseCheckbox";
            this.ignoreCaseCheckbox.Size = new System.Drawing.Size(83, 17);
            this.ignoreCaseCheckbox.TabIndex = 5;
            this.ignoreCaseCheckbox.Text = "Ignore Case";
            this.ignoreCaseCheckbox.UseVisualStyleBackColor = true;
            // 
            // excludeFilterText
            // 
            this.excludeFilterText.FormattingEnabled = true;
            this.excludeFilterText.Items.AddRange(new object[] {
            ""});
            this.excludeFilterText.Location = new System.Drawing.Point(12, 66);
            this.excludeFilterText.Name = "excludeFilterText";
            this.excludeFilterText.Size = new System.Drawing.Size(262, 21);
            this.excludeFilterText.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Exclude:";
            // 
            // FilterDialog
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(366, 145);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.excludeFilterText);
            this.Controls.Add(this.ignoreCaseCheckbox);
            this.Controls.Add(this.regexCheckBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.filterText);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.okButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FilterDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Filter";
            this.Activated += new System.EventHandler(this.FilterDialog_Activated);
            this.VisibleChanged += new System.EventHandler(this.FilterDialog_VisibleChanged);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FilterDialog_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.ComboBox filterText;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox regexCheckBox;
        private System.Windows.Forms.CheckBox ignoreCaseCheckbox;
        private System.Windows.Forms.ComboBox excludeFilterText;
        private System.Windows.Forms.Label label2;
    }
}