using System;
using System.Text;

namespace Rockstar.LogJam
{
    public class Filter
    {
        //NOTE:  To exclude lines from the output use the following type
        //of regular expression:
        //
        //  \[(?!rline\]).*
        //
        //This tells regex to exclude anything matching "[" followed by
        //anything *but* "rline]".
        //It excludes all lines containing [rline].

        string m_Str = "";
        string m_ExcludeStr = "";
        UseRegEx m_UseRegEx = Rockstar.LogJam.UseRegEx.No;
        IgnoreCase m_IgnoreCase = Rockstar.LogJam.IgnoreCase.Yes;

        public Filter()
        {
        }

        public Filter(string str, UseRegEx useRegEx, IgnoreCase ignoreCase)
        {
            this.Init(str, "", useRegEx, ignoreCase);
        }

        public Filter(string str,
                    string excludeStr,
                    UseRegEx useRegEx,
                    IgnoreCase ignoreCase)
        {
            this.Init(str, excludeStr, useRegEx, ignoreCase);
        }

        public string Text
        {
            get { return m_Str; }
        }

        public string ExcludeText
        {
            get { return m_ExcludeStr; }
        }

        public bool UseRegEx
        {
            get { return Rockstar.LogJam.UseRegEx.Yes == m_UseRegEx; }
        }

        public bool IgnoreCase
        {
            get { return Rockstar.LogJam.IgnoreCase.Yes == m_IgnoreCase; }
        }

        void Init(string str,
                    string excludeStr,
                    UseRegEx useRegEx,
                    IgnoreCase ignoreCase)
        {
            m_Str = string.IsNullOrEmpty(str) ? "" : str;
            m_ExcludeStr = string.IsNullOrEmpty(excludeStr) ? "" : excludeStr;
            m_UseRegEx = useRegEx;
            m_IgnoreCase = ignoreCase;
        }
    }
}
