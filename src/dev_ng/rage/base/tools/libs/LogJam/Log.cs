using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.ComponentModel;
using System.Threading;
using System.Diagnostics;
using System.IO;

namespace Rockstar.LogJam
{
    public enum LogLevel
    {
        Normal,
        Debug,
        Warning,
        Error
    }

    public enum FindDirection
    {
        Forward, Backward
    }

    public enum UseRegEx
    {
        Yes,
        No
    }

    public enum IgnoreCase
    {
        Yes,
        No
    }

    //What we use to mark lines as newline terminated.
    //Lines not terminated will have subsequently
    //submitted text concatenated to them.
    internal enum LineTerminated
    {
        No,
        Yes,
    }

    //Represents a line of colored text.
    public class Line
    {
        Log m_Owner;

        string m_Text;

        //Text log level - will determine color
        LogLevel m_LogLevel;

        //Pixel width of line
        internal int m_Width = 0;

        //Offset of the line's first character within the
        //filtered text of the containing LogView.
        int m_Offset = -1;

        //Non-newline-terminated lines will continue to have text
        //added to them until they're terminated.
        LineTerminated m_Lt;

        //Will be true if this line passes the current log filter.
        internal bool m_PassedFilter = false;

        internal Line(Log owner, string text, LogLevel logLevel, LineTerminated lt)
        {
            m_Owner = owner;
            m_Text = text;
            m_LogLevel = logLevel;
            m_Lt = lt;
        }

        internal void SetOffset(int offset)
        {
            m_Offset = offset + m_Owner.m_FilteredTextBase;
        }

        public int Offset
        {
            get{return m_Offset - m_Owner.m_FilteredTextBase;}
        }

        public string Text
        {
            get{return m_Text;}
        }

        public LogLevel LogLevel
        {
            get{return m_LogLevel;}
        }

        public int Length
        {
            get{return m_Text.Length;}
        }

        public override string ToString()
        {
            return m_Text;
        }

        internal bool Terminated
        {
            get{return LineTerminated.Yes == m_Lt;}
        }

        internal bool PassedFilter
        {
            get{return m_PassedFilter;}
        }
    }

    public class Log : IDisposable
    {
        //Raw (unfiltered) lines.
        List<Line> m_RawLines = new List<Line>();

        //Filtered lines
        List<Line> m_FilteredLines = new List<Line>();

        //Base offset of first character in the log.
        //If lines are removed due to the MaxLineCount constraint
        //this value will be increased by the length of the line removed.
        internal int m_FilteredTextBase = 0;

        //Total number of chars in the log.
        int m_FilteredTextLength;

        //Number of chars in the longest line.
        int m_FilteredTextMaxLineLength = 0;

        //Maximum number of lines in the log.
        //As this value is exceeded lines at the top of the log will be removed.
        int m_MaxLineCount = 256*1024;

        //Current filter.
        Filter m_Filter = new Filter();

        //Regular expression that implements the filter.
        List<Regex> m_FilterRegexList = new List<Regex>();

        //Regular expression that implements the filter used to exclude lines.
        List<Regex> m_ExcludeFilterRegexList = new List<Regex>();

        Line m_LastLineAdded;

        //Used in GetLineIndexFromCharIndex()
        Line m_ScratchLine;

        //If false don't append text.
        bool m_Enabled = true;

        //If lines will be added to the log from a thread different
        //from the UI thread then pass in an instance of ISynchronizeInvoke
        //to the constructor.
        //System.Windows.Forms.Control and all subclasses implement
        //ISynchronizeInvoke
        ISynchronizeInvoke m_SyncInvoke;

        TextWriter m_TextWriter;
        System.Object m_TwLock = new System.Object();

        //A write queue is used in the case where we have a m_SyncInvoke
        //instance and a line is appended from a thread different from
        //the thread owning m_SyncInvoke.
        //The line is appended to the write queue and periodically the
        //WriteQueueWorker processes the queue and adds the lines to the log.
        List<string> m_WriteQueue;
        System.Threading.Timer m_WriteQueueWorker;
        System.Object m_WriteQueueLock = new System.Object();

        WeakEvent<EventArgs> m_LinesChanged;
        WeakEvent<EventArgs> m_FilterChanged;

        EventArgs m_DummyEventArgs = new EventArgs();

        //Set to true when we need to dispatch a LinesChanged event.
        bool m_NeedToDispatchLinesChanged = false;

        //Interval at which we process the write queue.
        const long PROCESS_WRITE_QUEUE_INTERVAL_MS = 100;

        //Line separators
        static readonly string[] LINE_SEP = {Environment.NewLine, "\n"};

        //If a line starts with the text in m_FilterStr then it will be
        //tagged using the appropriate log level.
        //For example, a line starting with "Debug" will be tagged with the
        //Debug log level.
        class LogLevelFilter
        {
            readonly LogLevel m_LogLevel;
            readonly string m_FilterStr;
            readonly Regex m_FilterRegex;

            public LogLevelFilter(string str, LogLevel color)
            {
                m_FilterStr = (null != str) ? str : "";
                m_LogLevel = color;
                m_FilterRegex = null;
            }

            public LogLevelFilter(Regex regex, LogLevel color)
            {
                m_FilterRegex = regex;                
                m_FilterStr = "";
                m_LogLevel = color;
            }

            public LogLevel LogLevel
            {
                get{return m_LogLevel;}
            }

            public bool Filter(string str)
            {
                if(null != m_FilterRegex)
                {
                    return m_FilterRegex.IsMatch(str);
                }
                else
                {
                    return str.StartsWith(m_FilterStr);
                }
            }
        }

        readonly List<LogLevelFilter> m_LogLevelFilters = new List<LogLevelFilter>();

        //PARAMS
        //  syncInvoke  - Usually an instance of System.Windows.Forms.Control.
        //                ISynchronizeInvoke is used to ensure we perform updates
        //                in the same thread as the UI update.
        //NOTES
        //  If syncInvoke is null we assume we don't need to syncrhronize
        //  to a UI update.
        public Log(ISynchronizeInvoke syncInvoke)
        {
            this.Init();
            this.SetSyncInvoke(syncInvoke);
        }

        public Log(TextWriter tw)
        {
            this.Init();
            this.SetTextWriter(tw);
        }

        void Init()
        {
            m_LinesChanged = new WeakEvent<EventArgs>(this);
            m_FilterChanged = new WeakEvent<EventArgs>(this);

            //m_LogLevelFilters.Add(new LogLevelFilter("Error", LogLevel.Error));
            //m_LogLevelFilters.Add(new LogLevelFilter("Warning", LogLevel.Warning));
            //m_LogLevelFilters.Add(new LogLevelFilter("Debug", LogLevel.Debug));

            //Handle [<timestamp>] at the beginning of the line.
            m_LogLevelFilters.Add(new LogLevelFilter(new Regex(@"^(\[[0-9]+\] )?Error"), LogLevel.Error));
            m_LogLevelFilters.Add(new LogLevelFilter(new Regex(@"^(\[[0-9]+\] )?Warning"), LogLevel.Warning));
            m_LogLevelFilters.Add(new LogLevelFilter(new Regex(@"^(\[[0-9]+\] )?Debug"), LogLevel.Debug));

            m_ScratchLine = new Line(this, "", LogLevel.Normal, LineTerminated.Yes);
        }

        public void Dispose()
        {
            this.SetSyncInvoke(null);
        }

        //PARAMS
        //  syncInvoke  - Usually an instance of System.Windows.Forms.Control.
        //                ISynchronizeInvoke is used to ensure we perform updates
        //                in the same thread as the UI update.
        public void SetSyncInvoke(ISynchronizeInvoke syncInvoke)
        {
            if(syncInvoke != m_SyncInvoke)
            {
                if(null != m_SyncInvoke)
                {
                    //Kill the worker thread that processes the write queue.
                    EventWaitHandle ewh = new EventWaitHandle(false, EventResetMode.AutoReset);
                    m_WriteQueueWorker.Dispose(ewh);

                    //Wait for it to dispose.
                    ewh.WaitOne();

                    m_WriteQueueWorker = null;
                }
                else
                {
                    //We process new lines every 100ms so we don't
                    //get overwhelmed with causing a UI to repaint with
                    //every line added.
                    TimerCallback tcb = new TimerCallback(this.OnTimerCallback);
                    m_WriteQueueWorker = new System.Threading.Timer(tcb, null, 0, PROCESS_WRITE_QUEUE_INTERVAL_MS);
                }

                m_SyncInvoke = syncInvoke;
            }
        }

        public void SetTextWriter(TextWriter tw)
        {
            lock(m_TwLock)
            {
                m_TextWriter = tw;
            }
        }

        public void Clear()
        {
            m_RawLines.Clear();

            this.RebuildLines();
        }

        //Find and hilight the next occurrence of str.
        public bool FindText(string str,
                            FindDirection dir,
                            UseRegEx useRegex,
                            IgnoreCase ignoreCase,
                            ref int selectionStart,
                            ref int selectionLength)
        {
            bool success = false;
            Regex rex;
            RegexOptions options = RegexOptions.Compiled;

            if(selectionStart <= 0 || selectionLength <= 0)
            {
                selectionStart = selectionLength = 0;
            }

            if(IgnoreCase.Yes == ignoreCase)
            {
                options = options | RegexOptions.IgnoreCase;
            }

            if(FindDirection.Backward == dir)
            {
                options |= RegexOptions.RightToLeft;
            }

            if(UseRegEx.Yes == useRegex)
            {
                rex = new Regex(str, options);
            }
            else
            {
                rex = new Regex(Regex.Escape(str), options);
            }

            int start;      //Where to start the search
            int lincr;      //Line increment

            if(FindDirection.Forward == dir)
            {
                //Start from the char after the end of the currently
                //selected text.
                start = selectionStart + selectionLength;
                lincr = 1;  //Positive line increment
            }
            else
            {
                //Start from the char before the start of the currently
                //selected text.
                start = selectionStart - 1;
                if(start < 0)
                {
                    start = TextLength - 1;
                }

                lincr = -1; //Negative line increment
            }

            int firstLine = this.GetLineIndexFromCharIndex(start);

            if(firstLine >= 0)
            {
                //Potentially two passes if we need to wrap.

                for(int pass = 0; pass < 2 && !success; ++pass)
                {
                    int lidx = firstLine;
                    Line line = m_FilteredLines[lidx];

                    int numLines;   //Number of lines to check
                    Match match;

                    //First line is a special case because the start
                    //location might be in the middle of the line.
                    if(FindDirection.Forward == dir)
                    {
                        //Compute the start offset relative to the line.
                        int tmpStart = start - line.Offset;
                        //Number of chars to match
                        int length = line.Length - tmpStart;
                        match = rex.Match(line.Text, tmpStart, length);

                        numLines = m_FilteredLines.Count - firstLine;
                    }
                    else
                    {
                        //Number of chars to match
                        int length = start - line.Offset;
                        numLines = firstLine + 1;
                        match = rex.Match(line.Text, 0, length);
                    }

                    //Go to the next line.
                    lidx += lincr;

                    //Check all lines until we find a match.
                    for(int i = 1; !match.Success && i < numLines; ++i, lidx += lincr)
                    {
                        line = m_FilteredLines[lidx];
                        match = rex.Match(line.Text, 0, line.Length);
                    }

                    if(match.Success)
                    {
                        selectionStart = line.Offset + match.Index;
                        selectionLength = match.Value.Length;
                        success = true;
                    }
                    else if(FindDirection.Forward == dir)
                    {
                        if(start > 0)
                        {
                            //Wrap around to the top.
                            numLines = firstLine + 1;
                            firstLine = 0;
                            start = 0;
                        }
                        else
                        {
                            pass = 2;
                        }
                    }
                    else
                    {
                        if(start < TextLength - 1)
                        {
                            //Wrap around to the bottom.
                            numLines = m_FilteredLines.Count - firstLine;
                            firstLine = m_FilteredLines.Count - 1;
                            start = TextLength - 1;
                        }
                        else
                        {
                            pass = 2;
                        }
                    }
                }
            }

            if(!success)
            {
                selectionStart = selectionLength = -1;
            }

            return success;
        }

        //Write the line(s) of text.
        public void Write(string s)
        {
            if(this.Enabled && (s != null))
            {
                lock(m_WriteQueueLock)
                {
                    if(null == m_WriteQueue)
                    {
                        m_WriteQueue = new List<string>();
                    }

                    m_WriteQueue.Add(s);
                }
            }
        }

        public void WriteLine(string s)
        {
            if(this.Enabled && null != s)
            {
                this.Write(s + Environment.NewLine);
            }
        }

        //Returns the index of the line that contains the character
        //at the given offset.
        public int GetLineIndexFromCharIndex(int offset)
        {
            m_ScratchLine.SetOffset(offset);

            int idx = m_FilteredLines.BinarySearch(m_ScratchLine, sm_LineComparer);

            //If not found the return value is the bitwise complement
            //of the index of the item that would follow the one we want.
            if(idx < 0)
            {
                idx = (~idx) - 1;
            }

            return idx;
        }

        public string GetText(int offset, int length)
        {
            StringBuilder text = new StringBuilder("");
            int lidx = this.GetLineIndexFromCharIndex(offset);
            if(lidx >= 0)
            {
                Line line = m_FilteredLines[lidx];
                int tmpOffset = offset - line.Offset;
                int tmpLen = line.Length - tmpOffset;
                if(length < tmpLen)
                {
                    tmpLen = length;
                }

                text.Append(line.Text.Substring(tmpOffset, tmpLen));

                length -= tmpLen;

                for(++lidx; lidx < m_FilteredLines.Count && length > 0; ++lidx)
                {
                    line = m_FilteredLines[lidx];

                    if(length > line.Length)
                    {
                        text.Append(Environment.NewLine + line.Text);
                        length -= line.Length;
                    }
                    else
                    {
                        text.Append(Environment.NewLine + line.Text.Substring(0, length));
                        length = 0;
                    }
                }
            }

            return text.ToString();
        }

        static public bool IsAlphaNum(char c)
        {
            return (c >= 'a' && c <= 'z')
                    || (c >= 'A' && c <= 'Z')
                    || (c >= '0' && c <= '9')
                    || c == '_';
        }

        static public string StripAnsiEscapeSequences(string str)
        {
            for(int escIdx = str.IndexOf((char) 0x1B);
                escIdx >= 0;
                escIdx = str.IndexOf((char) 0x1B))
            {
                int mIdx = str.IndexOf('m', escIdx);

                if(0 == escIdx)
                {
                    //Escape seq is at beginning of string.
                    if(mIdx > escIdx)
                    {
                        escIdx = mIdx;
                    }

                    str = str.Substring(escIdx + 1);
                }
                else
                {
                    //Escape seq is in the middle of the string.
                    string textA = str.Substring(0, escIdx);

                    if(mIdx > escIdx)
                    {
                        escIdx = mIdx;
                    }

                    string textB = str.Substring(escIdx + 1);

                    str = textA + textB;
                }
            }

            return str;
        }

        public bool Enabled
        {
            get{return m_Enabled;}
            set{m_Enabled = value;}
        }

        public int LineCount
        {
            get{return m_FilteredLines.Count;}
        }

        public Line this[int index]
        {
            get{return m_FilteredLines[index];}
        }

        public int TextLength
        {
            get{return m_FilteredTextLength;}
        }

        public int MaxLineLength
        {
            get{return m_FilteredTextMaxLineLength;}
        }

        public int MaxLineCount
        {
            get{return m_MaxLineCount;}
        }

        //Get/set the current filter.
        public Filter Filter
        {
            get{return m_Filter;}
            set
            {
                if(null != value && null != value.Text)
                {
                    if(value.Text != m_Filter.Text
                        || value.ExcludeText != m_Filter.ExcludeText
                        || value.UseRegEx != m_Filter.UseRegEx
                        || value.IgnoreCase != m_Filter.IgnoreCase)
                    {
                        m_Filter = value;

                        string rexStr = value.Text;
                        string rexExcludeStr = value.ExcludeText;
                        RegexOptions options = RegexOptions.Compiled;

                        if(!value.UseRegEx)
                        {
                            rexStr = Regex.Escape(rexStr);
                            rexExcludeStr = Regex.Escape(rexExcludeStr);
                        }

                        if(value.IgnoreCase)
                        {
                            options = options | RegexOptions.IgnoreCase;
                        }

                        if (rexStr == "" && rexExcludeStr == "")
                        {
                            m_FilterRegexList.Clear();
                            m_ExcludeFilterRegexList.Clear();
                        }
                        else
                        {
                            if (rexStr != "")
                            {
                                Regex filterRegex = new Regex(rexStr, options);

                                if (!m_FilterRegexList.Contains(filterRegex))
                                {
                                    m_FilterRegexList.Add(filterRegex);
                                }

                                m_ExcludeFilterRegexList.Remove(filterRegex);
                            }

                            if (rexExcludeStr != "")
                            {
                                Regex excludeFilterRegex = new Regex(rexExcludeStr, options);

                                if (!m_ExcludeFilterRegexList.Contains(excludeFilterRegex))
                                {
                                    m_ExcludeFilterRegexList.Add(excludeFilterRegex);
                                }

                                m_FilterRegexList.Remove(excludeFilterRegex);
                            }
                        }
                      
                        this.RebuildLines();

                        FilterChanged.Invoke(m_DummyEventArgs);
                    }
                }
            }
        }

        //Dispatched when lines are added.
        public WeakEvent<EventArgs> LinesChanged
        {
            get{return m_LinesChanged;}
        }

        //Dispatched when the filter changes
        public WeakEvent<EventArgs> FilterChanged
        {
            get{return m_FilterChanged;}
        }

        class FilteredLineComparer : IComparer<Line>
        {
            public int Compare(Line a, Line b)
            {
                return a.Offset - b.Offset;
            }
        }

        static FilteredLineComparer sm_LineComparer = new FilteredLineComparer();

        //Write the line(s) of text.
        public void DoWrite(string s)
        {
            if(this.Enabled)
            {
                string[] lines = s.Split(LINE_SEP, StringSplitOptions.None);

                int count = lines.Length;

                //Split() generates an extra item if the
                //line ends with "\r\n".
                if(count > 1 && lines[count-1].Length == 0)
                {
                    --count;
                }

                for(int i = 0; i < count; ++i)
                {
                    string text = lines[i];
                    LineTerminated lt = LineTerminated.No;

                    //Check if the last line is terminated with a newline.
                    if(count - 1 == i && lines.Length == count)
                    {
                        foreach(string ls in LINE_SEP)
                        {
                            if(text.EndsWith(ls))
                            {
                                lt = LineTerminated.Yes;
                                break;
                            }
                        }
                    }
                    else
                    {
                        //This is not the last line so we know
                        //it's terminated with a newline.
                        lt = LineTerminated.Yes;
                    }

                    int numLines = LineCount;

                    if(null != m_LastLineAdded && !m_LastLineAdded.Terminated)
                    {
                        this.AddTextToLastLine(text, lt);
                    }
                    else
                    {
                        this.AppendLine(text, lt);
                    }

                    //Write to the text writer only if we've added a complete
                    //line, as opposed to appending to an existing line.
                    lock(m_TwLock)
                    {
                        if(null != m_TextWriter && LineCount > numLines)
                        {
                            m_TextWriter.WriteLine(m_LastLineAdded);
                        }
                    }
                }

                lock(m_TwLock)
                {
                    if(null != m_TextWriter)
                    {
                        m_TextWriter.Flush();
                    }
                }
            }
        }

        //Appends a line of text.
        void AppendLine(string text, LineTerminated lt)
        {
            //Remove ansi escape sequences from the string.
            text = Log.StripAnsiEscapeSequences(text);

            text = text.Replace("\t", "  ");

            LogLevel logLevel = LogLevel.Normal;

            foreach(LogLevelFilter llf in m_LogLevelFilters)
            {
                if(llf.Filter(text))
                {
                    logLevel = llf.LogLevel;
                    break;
                }
            }

            //Hack to handle the current format of channel
            //warnings and errors.  Current format prepends
            //them with file/line info, ala visual studio.
            if(logLevel == LogLevel.Normal)
            {
                if(text.Contains(": Error :"))
                {
                    logLevel = LogLevel.Error;
                }
                /*else if(text.Contains(": Warning :"))
                {
                    logLevel = LogLevel.Warning;
                }*/
            }

            this.AppendLine(text, logLevel, lt);
        }

        //Appends a line of text.
        void AppendLine(string text, LogLevel logLevel, LineTerminated lt)
        {
            Line line = new Line(this, text, logLevel, lt);
            m_RawLines.Add(line);

            this.AppendFilteredLine(line);

            //Remove lines only if we exceed by 100.
            //Removing lines one at a time proved very expensive.
            if(m_RawLines.Count > MaxLineCount + 100)
            {
                this.RemoveTopRawLines(m_RawLines.Count - MaxLineCount);
            }

            m_LastLineAdded = line;
        }

        //Appends text to the end of the last line.
        //This assumes the last line is not newline-terminated.
        void AddTextToLastLine(string text, LineTerminated lt)
        {
            string newText;

            if(null != m_LastLineAdded)
            {
                Debug.Assert(!m_LastLineAdded.Terminated);

                newText = m_LastLineAdded.Text + text;
                this.RemoveLastRawLine();

            }
            else
            {
                newText = text;
            }

            this.AppendLine(newText, lt);
        }

        //Appends a line of filtered text.  If the text passes the filter it
        //will be added to the filtered lines and displayed.
        bool AppendFilteredLine(Line line)
        {
            bool added = false;

            if(this.PassesFilter(line.Text))
            {
                line.SetOffset(this.TextLength);
                m_FilteredLines.Add(line);

                if(line.Length > m_FilteredTextMaxLineLength)
                {
                    m_FilteredTextMaxLineLength = line.Length;
                }

                m_FilteredTextLength += line.Length;

                m_NeedToDispatchLinesChanged = added = true;
            }

            line.m_PassedFilter = added;

            return added;
        }

        //Remove a number of lines from the top of the log.
        void RemoveTopRawLines(int numLines)
        {
            if(numLines > 0)
            {
                if(numLines > m_RawLines.Count)
                {
                    numLines = m_RawLines.Count;
                }

                int numFilteredLines = 0;
                int count = 0;

                foreach(Line line in m_RawLines)
                {
                    //Is this line in the filtered text?
                    if(line.PassedFilter)
                    {
                        m_FilteredTextLength -= line.Length;
                        m_FilteredTextBase += line.Length;
                        ++numFilteredLines;
                    }

                    if(++count >= numLines)
                    {
                        break;
                    }
                }

                m_RawLines.RemoveRange(0, numLines);

                if(numFilteredLines > 0)
                {
                    m_FilteredLines.RemoveRange(0, numFilteredLines);
                }

                if(0 == m_RawLines.Count)
                {
                    m_LastLineAdded = null;
                }

                m_NeedToDispatchLinesChanged = true;
            }
        }

        //Remove the last line in the log.
        void RemoveLastRawLine()
        {
            if(m_RawLines.Count > 0)
            {
                Line line = m_RawLines[m_RawLines.Count - 1];

                if(line.PassedFilter)
                {
                    m_FilteredTextLength -= line.Length;
                    m_FilteredLines.RemoveAt(m_FilteredLines.Count - 1);
                }

                m_RawLines.RemoveAt(m_RawLines.Count - 1);

                if(m_RawLines.Count > 0)
                {
                    m_LastLineAdded = m_RawLines[m_RawLines.Count - 1];
                }
                else
                {
                    m_LastLineAdded = null;
                }

                m_NeedToDispatchLinesChanged = true;
            }
        }

        //Rebuilds lines based on the current filter.
        void RebuildLines()
        {
            m_FilteredTextBase = 0;
            m_FilteredTextLength = 0;
            m_FilteredTextMaxLineLength = 0;
            m_FilteredLines.Clear();

            foreach(Line line in m_RawLines)
            {
                this.AppendFilteredLine(line);
            }
        }

        //Returns true if the text passes the current filter.
        bool PassesFilter(string text)
        {
            foreach (Regex filterRegex in m_FilterRegexList)
            {
                if (!filterRegex.IsMatch(text))
                {
                    return false;
                }
            }

            foreach (Regex excludeFilterRegex in m_ExcludeFilterRegexList)
            {
                if (excludeFilterRegex.IsMatch(text))
                {
                    return false;
                }
            }

            return true;
        }

        delegate void MyDelegate();
        MyDelegate m_MyDelegate;

        void OnTimerCallback(object stateInfo)
        {
            //Make a copy so it's not nulled out by another thread.
            ISynchronizeInvoke syncInvoke = m_SyncInvoke;

            if(null != syncInvoke)
            {
                if(syncInvoke.InvokeRequired)
                {
                    if(null == m_MyDelegate)
                    {
                        m_MyDelegate = delegate()
                        {
                            this.ProcessWriteQueue();
                        };
                    }
					// There was a crash here on shutdown when the window was destroyed, so I just catch it - MK
                    try
                    {
                        syncInvoke.BeginInvoke(m_MyDelegate, null);
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine(e.Message);
                    }
                    
                }
            }
            else
            {
                this.ProcessWriteQueue();
            }
        }

        void ProcessWriteQueue()
        {
            List<string> wq;
            lock(m_WriteQueueLock)
            {
                wq = m_WriteQueue;
                m_WriteQueue = null;
            }

            if(null != wq)
            {
                foreach(string s in wq)
                {
                    this.DoWrite(s);
                }
            }

            if(m_NeedToDispatchLinesChanged)
            {
                m_NeedToDispatchLinesChanged = false;
                LinesChanged.Invoke(m_DummyEventArgs);
            }
        }
    }
}
