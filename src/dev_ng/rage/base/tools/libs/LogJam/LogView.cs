using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Diagnostics;
using System.Drawing;
using System.Text;

namespace Rockstar.LogJam
{
    //Displays lines of readonly colored text.  Implements
    //finding and filtering of text.  When a filter is used only
    //lines that pass the filter will be displayed.

    //Control that renders the LogView's text.
    class ContentPanel : Panel
    {
        //LogView that contains us.
        LogView m_LogView;

        Log m_Log;

        //Brush used to render
        readonly SolidBrush m_Brush = new SolidBrush(Color.Black);

        //Drag select points in client space.
        Point m_DragSelectStart;

        //Extents of current selected text.
        int m_SelectionStart = -1;
        int m_SelectionEnd = -1;

        //Index of line at the top of the panel.
        int m_TopLine = 0;

        //Index of left-most character.
        int m_LeftChar = 0;

        //Pixel width of one char.
        int m_CharWidthInPixels = 1;

        bool m_IsMouseDown = false;

        //Used for detecting double and triple clicks
        int m_NumClicks = 0;
        readonly Stopwatch m_ClickStopWatch = Stopwatch.StartNew();
        Point m_LastClickPt = new Point(-1, -1);

        //Timer used to periodically scroll contents when we're
        //drag-selecting and the mouse is outside the client rectangle.
        System.Windows.Forms.Timer m_ScrollTimer;

        Color[] m_LineColors;

        //The maximum interval for which we'll detect double clicks
        static readonly long MAX_DOUBLE_CLICK_INTERVAL_MS    = SystemInformation.DoubleClickTime;

        //The maximum interval for which we'll detect triple clicks
        static readonly long MAX_TRIPLE_CLICK_INTERVAL_MS    = (MAX_DOUBLE_CLICK_INTERVAL_MS * 3) / 2;

        public ContentPanel(LogView logView)
        {
            m_LogView = logView;
            m_Log = logView.Log;

            this.DoubleBuffered = true;
            this.Cursor = Cursors.IBeam;
            //Transparent background
            this.BackColor = Color.FromArgb(0, LogView.ENABLED_COLOR);
            this.Font = new Font(FontFamily.GenericMonospace, 10);

            //Check the mouse position on a regular interval
            //and if it's outside of our bounds, and we're selecting text,
            //scroll the content panel.
            m_ScrollTimer = new System.Windows.Forms.Timer();
            m_ScrollTimer.Tick += CheckMouseAndScroll;
            m_ScrollTimer.Interval = 10;
            m_ScrollTimer.Start();

            m_LineColors = new Color[Enum.GetValues(typeof(LogLevel)).Length];

            m_LineColors[(int) LogLevel.Normal] = this.ForeColor;
            m_LineColors[(int) LogLevel.Debug] = Color.Blue;
            m_LineColors[(int) LogLevel.Warning] = Color.Yellow;
            m_LineColors[(int) LogLevel.Error] = Color.Red;

            Debug.Assert(m_LineColors.Length == 4);
        }

        protected override void Dispose(bool disposing)
        {
            m_ScrollTimer.Tick -= CheckMouseAndScroll;
            base.Dispose(disposing);
        }

        //Clear everything
        public void Clear()
        {
            m_TopLine = 0;
            m_LeftChar = 0;
            this.ClearSelection();
        }

        //Reset the selection extents 
        public void ClearSelection()
        {
            m_DragSelectStart.X = m_DragSelectStart.Y = 0;
            this.SelectionLength = 0;
            this.Invalidate();
        }

        //Begin a drag selection based on screen space points.
        public void BeginDragSelect(Point origStartPt)
        {
            if(m_Log.TextLength > 0)
            {
                m_DragSelectStart = this.PointToClient(origStartPt);

                //Get the character offset of the selection start point.
                int offset = m_LogView.GetCharIndexFromPoint(origStartPt);

                if(offset < 0){offset = 0;}

                if(offset != m_SelectionStart)
                {
                    m_SelectionStart = offset;
                    this.Invalidate();
                }
            }
            else
            {
                m_SelectionStart = m_SelectionEnd = -1;
            }
        }

        //Update a drag selection based on screen space points.
        public void UpdateDragSelect(Point origEndPt)
        {
            if(m_Log.TextLength > 0)
            {
                Point dragSelEnd = this.PointToClient(origEndPt);

                //Don't create a selection until the mouse actually moves.
                if(this.SelectionLength > 0
                        || dragSelEnd.X != m_DragSelectStart.X
                        || dragSelEnd.Y != m_DragSelectStart.Y)
                {
                    //Get the character offset of the selection end point.
                    int offset = m_LogView.GetCharIndexFromPoint(origEndPt);

                    if(offset < 0){offset = 0;}

                    if(offset != m_SelectionEnd)
                    {
                        m_SelectionEnd = offset;
                        this.Invalidate();
                    }
                }
            }
            else
            {
                m_SelectionStart = m_SelectionEnd = -1;
            }
        }

        public void SelectWordAtCharIndex(int charIndex)
        {
            //Find a string of alphanumeric characters surrounding the
            //character and hilight them.  If no alphanumeric characters
            //then hilight a single character.
            int lidx = m_Log.GetLineIndexFromCharIndex(charIndex);
            if(lidx >= 0)
            {
                Line line = m_Log[lidx];

                if(line.Length > 0)
                {
                    int selStart = charIndex - line.Offset;
                    int selLen = 1;

                    if(Log.IsAlphaNum(line.Text[selStart]))
                    {
                        //Search forward for the first non-alnum char.
                        for(int i = selStart + 1; i < line.Length; ++i)
                        {
                            if(!Log.IsAlphaNum(line.Text[i]))
                            {
                                break;
                            }

                            ++selLen;
                        }

                        //Search backward for the first non-alnum char.
                        for(int i = selStart - 1; i >= 0; --i)
                        {
                            if(!Log.IsAlphaNum(line.Text[i]))
                            {
                                break;
                            }

                            --selStart;
                            ++selLen;
                        }
                    }

                    //Hilight the selection.
                    this.SelectionStart = selStart + line.Offset;
                    this.SelectionLength = selLen;
                }
            }
        }

        public void SelectLineAtCharIndex(int charIndex)
        {
            int lidx = m_Log.GetLineIndexFromCharIndex(charIndex);
            if(lidx >= 0)
            {
                Line line = m_Log[lidx];

                if(line.Length > 0)
                {
                    this.SelectionStart = line.Offset;
                    this.SelectionLength = line.Length;
                }
            }
        }

        public int TopLine
        {
            get{return m_TopLine;}
            set
            {
                int newTopLine = value;

                if(newTopLine < 0)
                {
                    newTopLine = 0;
                }
                else if(newTopLine >= m_Log.LineCount)
                {
                    newTopLine = m_Log.LineCount - 1;
                }

                if(newTopLine != m_TopLine)
                {
                    //Limit the top line such that we always see
                    //a full page of text.
                    int maxTopLine = m_Log.LineCount - MaxLinesPerScreen;
                    if(maxTopLine < 0)
                    {
                        maxTopLine = 0;
                    }

                    if(newTopLine > maxTopLine)
                    {
                        newTopLine = maxTopLine;
                    }
                }

                if(newTopLine != m_TopLine)
                {
                    m_TopLine = newTopLine;
                    this.Invalidate();
                }
            }
        }

        public int BottomLine
        {
            get
            {
                int bl = TopLine + MaxLinesPerScreen - 1;
                return bl >= 0 ? bl : 0;
            }
        }

        public int LeftChar
        {
            get{return m_LeftChar;}
            set
            {
                int newLeftChar = value;

                if(newLeftChar < 0)
                {
                    newLeftChar = 0;
                }
                else if(newLeftChar >= m_Log.MaxLineLength)
                {
                    newLeftChar = m_Log.MaxLineLength - 1;
                }

                if(newLeftChar != m_LeftChar)
                {
                    //Limit the left char such that we always see
                    //a full line of text.
                    int maxLeftChar = m_Log.MaxLineLength - MaxCharsPerLine - 1;
                    if(maxLeftChar < 0)
                    {
                        maxLeftChar = 0;
                    }

                    if(newLeftChar > maxLeftChar)
                    {
                        newLeftChar = maxLeftChar;
                    }
                }

                if(newLeftChar != m_LeftChar)
                {
                    m_LeftChar = value;
                    this.Invalidate();
                }
            }
        }

        public int RightChar
        {
            get
            {
                int rc = LeftChar + MaxCharsPerLine - 1;
                return rc >= 0 ? rc : 0;
            }
        }

        public int MaxLinesPerScreen
        {
            get{return this.ClientRectangle.Height / this.Font.Height;}
        }

        public int MaxCharsPerLine
        {
            get{return (int) (this.ClientRectangle.Width / m_CharWidthInPixels);}
        }

        //Offset of the start of the selected text.
        public int SelectionStart
        {
            get
            {
                return (m_SelectionStart < m_SelectionEnd)
                        ? m_SelectionStart
                        : m_SelectionEnd;
            }

            set
            {
                if(m_Log.TextLength <= 0)
                {
                    m_SelectionStart = m_SelectionEnd = -1;
                }
                else
                {
                    m_SelectionStart = m_SelectionEnd = value;

                    if(m_SelectionStart >= m_Log.TextLength)
                    {
                        m_SelectionStart = m_SelectionEnd = m_Log.TextLength - 1;
                    }

                    this.Invalidate();
                }
            }
        }

        //Length of the selected text.
        public int SelectionLength
        {
            get
            {
                int len = 0;

                if(m_Log.TextLength > 0
                    && m_SelectionStart >= 0
                    && m_SelectionEnd >= 0)
                {
                    len = m_SelectionEnd >= m_SelectionStart
                            ? m_SelectionEnd - m_SelectionStart + 1
                            : m_SelectionStart - m_SelectionEnd + 1;
                }

                return len;
            }

            set
            {
                if(m_Log.TextLength <= 0)
                {
                    m_SelectionStart = m_SelectionEnd = -1;
                }
                else if(value != SelectionLength
                        && m_SelectionStart >= 0
                        && m_SelectionEnd >= 0)
                {
                    if(value > 0)
                    {
                        if(m_SelectionEnd >= m_SelectionStart)
                        {
                            m_SelectionEnd = m_SelectionStart + value - 1;

                            if(m_SelectionEnd >= m_Log.TextLength)
                            {
                                m_SelectionEnd = m_Log.TextLength - 1;
                            }
                        }
                        else
                        {
                            m_SelectionStart = m_SelectionEnd + value - 1;

                            if(m_SelectionEnd >= m_Log.TextLength)
                            {
                                m_SelectionEnd = m_Log.TextLength - 1;
                            }
                        }
                    }
                    else
                    {
                        m_SelectionStart = m_SelectionEnd = -1;
                    }

                    this.Invalidate();
                }
            }
        }

        public string SelectedText
        {
            get
            {
                string text = "";

                if(SelectionLength > 0)
                {
                    text = m_Log.GetText(SelectionStart, SelectionLength);
                }

                return text;
            }
        }

        //Draw visible lines
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            Font font = this.Font;

            int lineHeight = (int) font.Height;

            if(1 == m_CharWidthInPixels)
            {
                float f1 = e.Graphics.MeasureString("W", font).Width;
                float f2 = e.Graphics.MeasureString("WW", font).Width;
                m_CharWidthInPixels = (int) (f2 - f1);
                if(0 == m_CharWidthInPixels)
                {
                    m_CharWidthInPixels = 2;
                }
            }

            int y = 0;

            //Character offset of the end of our selection.
            int selEnd = this.SelectionStart + this.SelectionLength - 1;

            //String format used for rendering strings.
            StringFormat strFormat = (StringFormat) StringFormat.GenericTypographic.Clone();
            strFormat.FormatFlags |= StringFormatFlags.NoWrap;

            /*//Layout rectangle used to calculate rectangles containing
            //each line.
            RectangleF layoutRect = new RectangleF(0, 0, 1000, lineHeight);*/

            //Rectangle used to render each line.
            RectangleF lineRect =
                new RectangleF((int) -m_CharWidthInPixels * LeftChar,
                                0,
                                0,
                                font.Height);

            int myHeight = this.ClientRectangle.Height;

            //For each visible line determine if it's fully non-hilighted,
            //fully hilighted, or partially hilighted.
            for(int lidx = TopLine;
                lidx < m_Log.LineCount && (y+lineHeight) <= myHeight;
                ++lidx, y += lineHeight)
            {
                Line line = m_Log[lidx];

                if(line.Length == 0)
                {
                    continue;
                }

                if(0 == line.m_Width)
                {
                    /*//Compute the pixel width of the line.
                    CharacterRange[] charRanges =
                    {
                        new CharacterRange(0, line.Length)
                    };
                    strFormat.SetMeasurableCharacterRanges(charRanges);
                    Region[] strRegions =
                        e.Graphics.MeasureCharacterRanges(line.Text, font, layoutRect, strFormat);

                    line.m_Width = (int) strRegions[0].GetBounds(e.Graphics).Width;*/

                    line.m_Width = line.Length * m_CharWidthInPixels;
                }

                //Prepare the destination render rectangle.
                lineRect.Width = line.m_Width;
                lineRect.Y = y;

                //Character offset of the end of the line.
                int strEnd = line.Offset + line.Length - 1;

                if(line.Offset > selEnd || strEnd < this.SelectionStart)
                {
                    //Entire line is not hilighted.

                    m_Brush.Color = m_LineColors[(int) line.LogLevel];
                    e.Graphics.DrawString(line.Text, font, m_Brush, lineRect, strFormat);
                }
                else if(line.Offset >= this.SelectionStart && strEnd <= selEnd)
                {
                    //Entire line is hilighted.

                    m_Brush.Color = Color.Blue;
                    e.Graphics.FillRectangle(m_Brush, lineRect);
                    m_Brush.Color = Color.White;
                    e.Graphics.DrawString(line.Text, font, m_Brush, lineRect, strFormat);
                }
                else
                {
                    //Line is partially hilighted.

                    //First draw the entire line not hilighted
                    m_Brush.Color = m_LineColors[(int) line.LogLevel];
                    e.Graphics.DrawString(line.Text, font, m_Brush, lineRect, strFormat);

                    //Now draw the hilighted part on top of what we just
                    //drew.

                    int cidx, len;

                    if(line.Offset >= this.SelectionStart)
                    {
                        //Start of selection is on a previous line.

                        //Index of first hilighted character in the line.
                        cidx = 0;

                        //Number of hilighted characters in the line.
                        if(strEnd > selEnd)
                        {
                            len = selEnd - line.Offset + 1;
                        }
                        else
                        {
                            len = line.Length;
                        }
                    }
                    else
                    {
                        //Start of selection is on this line.

                        //Index of first hilighted character in the line.
                        cidx = this.SelectionStart - line.Offset;

                        //Number of hilighted characters in the line.
                        if(strEnd > selEnd)
                        {
                            len = selEnd - this.SelectionStart + 1;
                        }
                        else
                        {
                            len = line.Length - cidx;
                        }
                    }

                    //Hilighted characters
                    string str = line.Text.Substring(cidx, len);

                    /*//Compute the rectangle surrounding the hilighted characters.
                    CharacterRange[] charRanges =
                    {
                        new CharacterRange(cidx, len)
                    };
                    strFormat.SetMeasurableCharacterRanges(charRanges);
                    Region[] stringRegions =
                        e.Graphics.MeasureCharacterRanges(line.Text, font, layoutRect, strFormat);

                    RectangleF rect = stringRegions[0].GetBounds(e.Graphics);
                    rect.X += lineRect.X;
                    rect.Y = lineRect.Y;
                    rect.Height = lineRect.Height;*/

                    RectangleF rect = lineRect;
                    rect.X += cidx * m_CharWidthInPixels;
                    rect.Width = len * m_CharWidthInPixels;

                    //Draw the background color.
                    m_Brush.Color = Color.Blue;
                    e.Graphics.FillRectangle(m_Brush, rect);

                    //Draw the text.
                    m_Brush.Color = Color.White;
                    e.Graphics.DrawString(str, font, m_Brush, rect, strFormat);
                }
            }
            
            strFormat.Dispose();
        }

        //If we're selecting text and the mouse point is outside our
        //bounds then scroll the content panel.
        void CheckMouseAndScroll(Object obj, EventArgs e)
        {
            const int SLOW_SCROLL   = 1;
            const int FAST_SCROLL   = 3;

            if(m_IsMouseDown)
            {
                Point mousePos = Control.MousePosition;
                Point pt = this.PointToClient(mousePos);
                Rectangle rect = this.ClientRectangle;

                int vDelta = 0, hDelta = 0;

                if(pt.Y > rect.Height)
                {
                    vDelta = ((pt.Y - rect.Height) < 30)
                                ? SLOW_SCROLL
                                : FAST_SCROLL;
                }
                else if(pt.Y < 0)
                {
                    vDelta = (-pt.Y < 30)
                                ? -SLOW_SCROLL
                                : -FAST_SCROLL;
                }

                if(pt.X > rect.Width)
                {
                    hDelta = ((pt.X - rect.Width) < 30)
                                ? SLOW_SCROLL
                                : FAST_SCROLL;
                }
                else if(pt.X < 0)
                {
                    hDelta = (-pt.X < 30)
                                ? -SLOW_SCROLL
                                : -FAST_SCROLL;
                }

                m_LogView.VScrollBy(vDelta);
                m_LogView.HScrollBy(hDelta);

                this.UpdateDragSelect(mousePos);
            }
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);

            if(MouseButtons.Left == e.Button)
            {
                m_IsMouseDown = true;

                if(Control.ModifierKeys == Keys.Shift)
                {
                    //Shift-select
                    this.UpdateDragSelect(Control.MousePosition);
                }
                else
                {
                    //Start a new selection
                    this.ClearSelection();
                    this.BeginDragSelect(Control.MousePosition);
                }

                m_LogView.ScrollOnAppend = false;

                this.Focus();
            }
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);

            if(MouseButtons.Left == e.Button && m_IsMouseDown)
            {
                //Update the current selection.
                this.UpdateDragSelect(Control.MousePosition);
            }
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseUp(e);

            if(MouseButtons.Left == e.Button)
            {
                m_IsMouseDown = false;

                long elapsedMs = m_ClickStopWatch.ElapsedMilliseconds;
                Point mousePos = Control.MousePosition;

                int lidx0 = m_LogView.GetLineIndexFromPoint(m_LastClickPt);
                int lidx1 = m_LogView.GetLineIndexFromPoint(mousePos);

                if(lidx0 != lidx1 || elapsedMs > MAX_TRIPLE_CLICK_INTERVAL_MS)
                {
                    System.Console.WriteLine(elapsedMs);
                    m_ClickStopWatch.Reset();
                    m_ClickStopWatch.Start();
                    m_NumClicks = 0;
                }

                ++m_NumClicks;
                m_LastClickPt = mousePos;

                if(2 == m_NumClicks && elapsedMs <= MAX_DOUBLE_CLICK_INTERVAL_MS)
                {
                    //Double-clicked.  Highlight the word at the click point.

                    int cidx = m_LogView.GetCharIndexFromPoint(mousePos);
                    this.SelectWordAtCharIndex(cidx);
                }
                else if(3 == m_NumClicks && elapsedMs <= MAX_TRIPLE_CLICK_INTERVAL_MS)
                {
                    //Triple-clicked.  Highlight the line at the click point.

                    int cidx = m_LogView.GetCharIndexFromPoint(mousePos);
                    this.SelectLineAtCharIndex(cidx);

                    m_ClickStopWatch.Reset();
                    m_ClickStopWatch.Start();
                    m_NumClicks = 0;
                    m_LastClickPt.X = m_LastClickPt.Y = -1;
                }
            }
        }

        protected override void OnMouseWheel(MouseEventArgs e)
        {
            base.OnMouseWheel(e);

            if(e.Delta < 0)
            {
                m_LogView.VScrollBy(m_LogView.WheelScrollDelta);
            }
            else if(e.Delta > 0)
            {
                m_LogView.VScrollBy(-m_LogView.WheelScrollDelta);
            }
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);
            if(e.Control && Keys.C == e.KeyCode)
            {
                if(SelectionLength > 0)
                {
                    //Copy the text to the clipboard.
                    Clipboard.SetText(SelectedText);
                }
                e.Handled = true;
            }
            else if(e.Control && Keys.A == e.KeyCode)
            {
                //Select all text
                SelectionStart = 0;
                SelectionLength = m_Log.TextLength;
                e.Handled = true;
            }
            else if (e.Control && Keys.Home == e.KeyCode)
            {
                //stop autoscroll and scroll to top
                m_LogView.ScrollOnAppend = false;
                m_LogView.ScrollToLine(0);
                e.Handled = true;
            }
            else if (e.Control && Keys.End == e.KeyCode)
            {
                //start autoscroll
                m_LogView.ScrollOnAppend = true;
                e.Handled = true;
            }
        }
    }

    public class LogView : Panel
    {
        delegate void MyDelegate();

        [DllImport("user32.dll")]
        static extern bool LockWindowUpdate(IntPtr h);

        [DllImport("user32.dll")]
        static extern int SendMessage(IntPtr h, uint msg, int wParam, int lParam);

        const int EM_GETFIRSTVISIBLELINE     = 0xCE;
        const int EM_LINESCROLL              = 0xB6;

        //Used for computing the X position of characters within
        //a line of text.
        TextBox m_ScratchTextBox = new TextBox();

        Log m_Log;

        //Panel that renders lines of text.
        ContentPanel m_ContentPanel;

        //H/V scroll bars for the logview.
        HScrollBar m_HScroll;
        VScrollBar m_VScroll;

        //Number of lines to scroll when mouse wheel is used.
        int m_WheelScrollDelta  = 5;

        //If true then scroll to the last line when a line is appended.
        bool m_ScrollOnAppend = true;

        public static readonly Color ENABLED_COLOR = Color.FromArgb(192, 192, 192);
        public static readonly Color DISABLED_COLOR = Color.FromArgb(208, 192, 192);
        public static readonly Color FILTERED_COLOR = Color.FromArgb(192, 208, 192);

        public LogView()
        {
            this.InitializeComponent();

            m_Log = new Log(this);

            m_Log.LinesChanged.AddHandler(this.HandleLinesChanged);
            m_Log.FilterChanged.AddHandler(this.HandleFilterChanged);

            m_ContentPanel = new ContentPanel(this);
            m_ContentPanel.Dock = DockStyle.Fill;

            m_ScratchTextBox.Font = m_ContentPanel.Font;

            m_HScroll = new HScrollBar();
            m_VScroll = new VScrollBar();
            m_HScroll.Maximum = m_HScroll.Minimum = 0;
            m_VScroll.Maximum = m_VScroll.Minimum = 0;
            m_HScroll.Dock = DockStyle.Bottom;
            m_VScroll.Dock = DockStyle.Right;

            this.Controls.Add(m_ContentPanel);
            this.Controls.Add(m_HScroll);
            this.Controls.Add(m_VScroll);

            m_HScroll.ValueChanged += this.ScrollBar_ValueChanged;
            m_VScroll.ValueChanged += this.ScrollBar_ValueChanged;

            this.BackColor = ENABLED_COLOR;

            //Make sure the control is fully created so it will immediately
            //begin capturing text.
            m_ContentPanel.CreateControl();
            this.CreateControl();
        }

        protected override void Dispose(bool disposing)
        {
            m_HScroll.ValueChanged -= ScrollBar_ValueChanged;
            m_VScroll.ValueChanged -= ScrollBar_ValueChanged;
            m_Log.Dispose();
            base.Dispose(disposing);
        }

        public void Clear()
        {
            m_Log.Clear();
            m_ContentPanel.Clear();
            this.AdjustScrollBars();

            this.Invalidate();
        }

        //Write the line(s) of text.
        public void Write(string s)
        {
            m_Log.Write(s);
        }

        public void WriteLine(string s)
        {
            if(null != s)
            {
                m_Log.WriteLine(s);
            }
        }

        //Returns the index of the character at the given screen space
        //location
        public int GetCharIndexFromPoint(Point origPt)
        {
            Point pt = m_ContentPanel.PointToClient(origPt);

            int offset = -1;

            if(pt.Y < 0)
            {
                offset = 0;
            }
            else if((pt.Y / (int) m_ContentPanel.Font.Height) >= m_Log.LineCount)
            {
                offset = m_Log.TextLength - 1;
            }
            else
            {
                Line line = this.GetLineFromPoint(origPt);

                if(null != line)
                {
                    if(pt.X >= 0)
                    {
                        m_ScratchTextBox.Text = line.Text;
                        m_ScratchTextBox.Width = 10000;
                        int charIdx = m_ScratchTextBox.GetCharIndexFromPosition(new Point(pt.X, 0));
                        charIdx += m_ContentPanel.LeftChar;
                        offset = line.Offset + charIdx;
                    }
                    else
                    {
                        offset = line.Offset;
                    }
                }
            }

            return offset;
        }

        //Returns the index of the line at the given screen space point.
        public int GetLineIndexFromPoint(Point origPt)
        {
            Point pt = m_ContentPanel.PointToClient(origPt);

            int lidx = -1;

            if(m_Log.LineCount > 0)
            {
                int lineHeight = (int) m_ContentPanel.Font.Height;
                lidx = (pt.Y / lineHeight) + m_ContentPanel.TopLine;

                if(lidx >= m_Log.LineCount)
                {
                    lidx = m_Log.LineCount - 1;
                }
                else if(lidx < 0)
                {
                    lidx = 0;
                }
            }

            return lidx;
        }

        //Scrolls the character at the given offset into view.
        public void ScrollToCharOffset(int offset)
        {
            if(m_Log.LineCount > 0)
            {
                int lidx = m_Log.GetLineIndexFromCharIndex(offset);
                if(lidx >= 0)
                {
                    int cidx = offset - m_Log[lidx].Offset;

                    this.ScrollToCharLocation(cidx, lidx);
                }
            }
        }

        //Scrolls by the given number of lines.
        public void VScrollBy(int numLines)
        {
            int lidx = m_VScroll.Value + numLines;

            if(lidx < m_VScroll.Minimum)
            {
                lidx = m_VScroll.Minimum;
            }
            else if(lidx > m_VScroll.Maximum)
            {
                lidx = m_VScroll.Maximum;
            }

            if(lidx != m_VScroll.Value)
            {
                m_VScroll.Value = lidx;
                this.Invalidate();
            }
        }

        //Scrolls by the given number of chars.
        public void HScrollBy(int numChars)
        {
            int cidx = m_HScroll.Value + numChars;

            if(cidx < m_HScroll.Minimum)
            {
                cidx = m_HScroll.Minimum;
            }
            else if(cidx > m_HScroll.Maximum)
            {
                cidx = m_HScroll.Maximum;
            }

            if(cidx != m_HScroll.Value)
            {
                m_HScroll.Value = cidx;
                this.Invalidate();
            }
        }

        //Scrolls the line at the given index into view.
        public void ScrollToLine(int lidx)
        {
            if(lidx < m_VScroll.Minimum)
            {
                lidx = m_VScroll.Minimum;
            }
            else if(lidx > m_VScroll.Maximum)
            {
                lidx = m_VScroll.Maximum;
            }

            //If the target line is outside of the current view then scroll.
            if(lidx >= 0
                && (lidx < m_ContentPanel.TopLine || lidx > m_ContentPanel.BottomLine))
            {
                //Scroll the line such that it appears in the middle
                //of the content panel.
                int targetLine = lidx - (m_ContentPanel.MaxLinesPerScreen / 2);
                if(targetLine < 0){targetLine = 0;}

                m_VScroll.Value = targetLine;
                this.Invalidate();
            }
        }

        //Scrolls the given char location into view.
        public void ScrollToCharLocation(int x, int lidx)
        {
            if(x < m_HScroll.Minimum)
            {
                x = m_HScroll.Minimum;
            }
            else if(x > m_HScroll.Maximum)
            {
                x = m_HScroll.Maximum;
            }

            //If the target char is outside of the current view then scroll.
            if(x >= 0
                && (x < m_ContentPanel.LeftChar || x > m_ContentPanel.RightChar))
            {
                //Scroll the char such that it appears in the middle
                //of the content panel.
                int targetChar = x - (m_ContentPanel.MaxCharsPerLine / 2);
                if(targetChar < 0){targetChar = 0;}

                m_HScroll.Value = targetChar;
                this.Invalidate();
            }

            this.ScrollToLine(lidx);
        }

        //Scrolls the last line into view.
        public void ScrollToBottom()
        {
            this.ScrollToLine(m_Log.LineCount - 1);
        }

        public void ClearSelection()
        {
            m_ContentPanel.ClearSelection();
        }

        public Log Log
        {
            get{return m_Log;}
        }

        //Offset of the start of the selected text.
        public int SelectionStart
        {
            get{return m_ContentPanel.SelectionStart;}
            set{m_ContentPanel.SelectionStart = value;}
        }

        //Length of the selected text.
        public int SelectionLength
        {
            get{return m_ContentPanel.SelectionLength;}
            set{m_ContentPanel.SelectionLength = value;}
        }

        public string SelectedText
        {
            get{return m_ContentPanel.SelectedText;}
        }

        //If true then scroll to the last line when a line is appended.
        public bool ScrollOnAppend
        {
            get{return m_ScrollOnAppend;}
            set
            {
                if(value != m_ScrollOnAppend)
                {
                    m_ScrollOnAppend = value;
                    if(m_ScrollOnAppend)
                    {
                        this.ScrollToBottom();
                    }
                }
            }
        }

        public int WheelScrollDelta
        {
            get{return m_WheelScrollDelta;}
        }

        new public bool Enabled
        {
            get{return m_Log.Enabled;}
            set
            {
                m_Log.Enabled = value;
                this.UpdateBackgroundColor();
            }
        }

        void UpdateBackgroundColor()
        {
            Color bgColor = this.BackColor;

            if(this.Enabled)
            {
                if(m_Log.Filter.Text.Length > 0 || m_Log.Filter.ExcludeText.Length > 0)
                {
                    bgColor = FILTERED_COLOR;
                }
                else
                {
                    bgColor = ENABLED_COLOR;
                }
            }
            else
            {
                bgColor = DISABLED_COLOR;
            }

            if(bgColor != this.BackColor)
            {
                this.BackColor = bgColor;
                this.Invalidate();
            }
        }

        void HandleLinesChanged(object sender, EventArgs e)
        {
            this.AdjustScrollBars();

            if(this.ScrollOnAppend)
            {
                this.ScrollToBottom();
            }

            this.Invalidate();
        }

        void HandleFilterChanged(object sender, EventArgs e)
        {
            m_ContentPanel.ClearSelection();

            this.AdjustScrollBars();

            this.UpdateBackgroundColor();

            this.Invalidate();
        }

        //Returns the line at the given screen space point.
        Line GetLineFromPoint(Point origPt)
        {
            int lidx = this.GetLineIndexFromPoint(origPt);

            return lidx >= 0 ? m_Log[lidx] : null;
        }

        void AdjustScrollBars()
        {
            m_VScroll.SmallChange = 1;
            m_VScroll.LargeChange = m_ContentPanel.MaxLinesPerScreen;
            m_VScroll.Maximum = m_Log.LineCount - 1;

            m_HScroll.SmallChange = 1;
            m_HScroll.LargeChange = m_ContentPanel.MaxCharsPerLine;
            m_HScroll.Maximum = m_Log.MaxLineLength - 1;
        }

        void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // LogView
            // 
            this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ResumeLayout(false);
        }

        protected override void OnResize (EventArgs e)
        {
            base.OnResize(e);

            this.AdjustScrollBars();
        }

        protected override void OnGotFocus(EventArgs e)
        {
            base.OnGotFocus(e);

            this.AdjustScrollBars();

            m_ContentPanel.Focus();
        }
        
        private void ScrollBar_ValueChanged(Object sender, EventArgs e)
        {
            if(sender == m_VScroll)
            {
                m_ContentPanel.TopLine = m_VScroll.Value;
            }
            else if(sender == m_HScroll)
            {
                m_ContentPanel.LeftChar = m_HScroll.Value;
            }
        }
    }
}
