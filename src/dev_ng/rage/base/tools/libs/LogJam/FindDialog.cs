using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace Rockstar.LogJam
{
    public partial class FindDialog:Form
    {
        LogView m_LogView;
        Log m_Log;

        public FindDialog()
        {
            InitializeComponent();
        }

        public void SetLogView(LogView logView)
        {
            if(null != logView)
            {
                m_LogView = logView;
                m_Log = logView.Log;
            }
            else
            {
                m_LogView = null;
                m_Log = null;
                this.Close();
            }
        }

        public string TextToFind
        {
            get{return textToFind.Text;}
            set{textToFind.Text = value;}
        }

        public FindDirection FindDirection
        {
            get
            {
                return this.upRadioButton.Checked
                        ? FindDirection.Backward
                        : FindDirection.Forward;
            }

            set
            {
                if(FindDirection.Backward == value)
                {
                    this.upRadioButton.Checked = true;
                }
                else
                {
                    this.downRadioButton.Checked = true;
                }
            }
        }

        public void DoFind()
        {
            if(this.TextToFind.Length > 0
                && null != m_LogView
                && null != m_Log)
            {
                UseRegEx useRegex = this.regexCheckBox.Checked
                                    ? UseRegEx.Yes
                                    : UseRegEx.No;

                IgnoreCase ignoreCase = this.ignoreCaseCheckbox.Checked
                                        ? IgnoreCase.Yes
                                        : IgnoreCase.No;

                int selectionStart = m_LogView.SelectionStart;
                int selectionLength = m_LogView.SelectionLength;

                m_Log.FindText(this.TextToFind,
                                this.FindDirection,
                                useRegex,
                                ignoreCase,
                                ref selectionStart,
                                ref selectionLength);

                if(selectionStart >= 0)
                {
                    m_LogView.SelectionStart = selectionStart;
                    m_LogView.SelectionLength = selectionLength;
                    m_LogView.ScrollToCharOffset(selectionStart);
                }
                else
                {
                    m_LogView.ClearSelection();
                }

                if(this.TextToFind.Length > 0
                    && !this.textToFind.Items.Contains(this.TextToFind))
                {
                    this.textToFind.Items.Add(this.TextToFind);
                }

                //We initially created the combo box with one empty item
                //so it behaves as expected.
                //After adding a "real" item we can remove the empty item.
                if(this.textToFind.Items.Count > 1
                    && this.textToFind.Items[0].Equals(""))
                {
                    this.textToFind.Items.RemoveAt(0);
                }
            }
        }

        private void FindDialog_Activated(object sender,EventArgs e)
        {
            this.textToFind.Focus();
        }

        private void FindDialog_FormClosing(object sender,FormClosingEventArgs e)
        {
            if(this.Visible && e.CloseReason != CloseReason.FormOwnerClosing)
            {
                e.Cancel = true;
                this.Hide();
            }
        }

        private void FindDialog_KeyDown(object sender,KeyEventArgs e)
        {
            if(e.KeyCode == Keys.F3)
            {
                this.FindDirection = e.Shift
                                    ? FindDirection.Backward
                                    : FindDirection.Forward;

                this.DoFind();
            }
        }

        private void textToFind_Leave(object sender,EventArgs e)
        {
            this.textToFind.Focus();
        }

        private void findButton_Click(object sender,EventArgs e)
        {
            this.DoFind();
        }

        private void cancelButton_Click(object sender,EventArgs e)
        {
            this.Hide();
        }

        private void FindDialog_VisibleChanged(object sender, EventArgs e)
        {
            this.textToFind.Focus();
            this.FindDirection = FindDirection.Forward;
        }
    }
}