using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Rockstar.LogJam;

namespace LogJam
{
    public partial class Form1:Form
    {
        LogView m_LogView = new LogView();
        FindDialog m_FindDlg = null;
        FilterDialog m_FilterDlg = null;
        ContextMenuStrip m_CtxMenu = new ContextMenuStrip();

        public Form1()
        {
            InitializeComponent();
            m_LogView.Dock = DockStyle.Fill;
            panel1.Controls.Add(m_LogView);
            m_FindDlg = new FindDialog();
            m_FilterDlg = new FilterDialog();

            //------- Context menu -------

            ToolStripMenuItem scrollOnAppendMi = new ToolStripMenuItem("Auto Scroll");
            scrollOnAppendMi.Click += delegate(object sender, EventArgs e)
            {
                scrollOnAppendMi.Checked = !scrollOnAppendMi.Checked;
                m_LogView.ScrollOnAppend = scrollOnAppendMi.Checked;
            };

            ToolStripMenuItem clearMi = new ToolStripMenuItem("Clear", null, null, Keys.Control | Keys.L);
            clearMi.Click += delegate(object sender, EventArgs e)
            {
                m_LogView.Clear();
            };

            EventHandler findOnClick =
                delegate(object sender, EventArgs e)
                {
                    this.ShowFindDialog();
                };
            ToolStripMenuItem findMi = new ToolStripMenuItem("Find...", null, findOnClick, Keys.Control | Keys.F);

            ToolStripMenuItem editFilterMi = new ToolStripMenuItem("Filter...");
            editFilterMi.Click += delegate(object sender, EventArgs e)
            {
                this.ShowFilterDialog();
            };

            ToolStripMenuItem resetFilterMi = new ToolStripMenuItem("Reset Filter");
            resetFilterMi.Click += delegate(object sender, EventArgs e)
            {
                m_LogView.Log.Filter = new Filter();
            };

            ToolStripMenuItem enableDisableMi = new ToolStripMenuItem("Enable");
            enableDisableMi.Click += delegate(object sender, EventArgs e)
            {
                m_LogView.Enabled = !m_LogView.Enabled;
            };
            
            ToolStripItem[] menuItems = new ToolStripItem[]
            {
                scrollOnAppendMi,
                clearMi,
                findMi,
                editFilterMi,
                resetFilterMi,
                enableDisableMi,
            };

            m_CtxMenu.Items.AddRange(menuItems);

            m_CtxMenu.Opening += new CancelEventHandler(
                delegate(Object sender, CancelEventArgs args)
            {
                scrollOnAppendMi.Checked = m_LogView.ScrollOnAppend;
                resetFilterMi.Enabled =
                    (m_LogView.Log.Filter.Text.Length > 0)
                    || (m_LogView.Log.Filter.ExcludeText.Length > 0);
                enableDisableMi.Text = m_LogView.Enabled ? "Disable" : "Enable";
            });
        }

        private void openToolStripMenuItem_Click(object sender,EventArgs e)
        {
            openFileDialog1.Reset();
            openFileDialog1.Title = "Open Log File";
            openFileDialog1.Filter = "Log Files (*.log)|*.log|Text Files (*.txt)|*.txt";
            DialogResult dr = openFileDialog1.ShowDialog(this);

            if(DialogResult.OK == dr)
            {
                this.LoadFile(openFileDialog1.FileName);
            }
        }

        private void LoadFile(string filename)
        {
            m_LogView.Clear();

            try
            {
                StreamReader sr = new StreamReader(filename);
                string line;
                for(line = sr.ReadLine(); null != line; line = sr.ReadLine())
                {
                    m_LogView.WriteLine(line);
                }
            }
            catch (System.Exception /*ex*/)
            {
            }
        }

        private void ShowFindDialog()
        {
            if(this.Visible
                && this.WindowState != FormWindowState.Minimized)
            {
                m_FindDlg.SetLogView(m_LogView);
                m_FindDlg.Text = "Find";
                m_FindDlg.TextToFind = m_LogView.SelectedText;
                m_FindDlg.Visible = false;
                m_FindDlg.Show(this);
            }
        }

        private void ShowFilterDialog()
        {
            if(this.Visible
                && this.WindowState != FormWindowState.Minimized)
            {
                m_FilterDlg.SetLog(m_LogView.Log);
                m_FilterDlg.Text = "Filter";
                m_FilterDlg.FilterText = m_LogView.Log.Filter.Text;
                m_FilterDlg.Visible = false;
                m_FilterDlg.Show(this);
            }
        }

        private void Form1_DragDrop(object sender,DragEventArgs e)
        {
            if(e.Data.GetDataPresent(DataFormats.FileDrop, false))
            {
                string[] files = e.Data.GetData(DataFormats.FileDrop) as string[];

                if(null != files || files.Length > 1)
                {
                    this.LoadFile(files[0]);
                }
            }
        }

        private void Form1_DragEnter(object sender,DragEventArgs e)
        {
            if(e.Data.GetDataPresent(DataFormats.FileDrop, false))
            {
                string[] files = e.Data.GetData(DataFormats.FileDrop) as string[];

                if(null != files || files.Length > 1)
                {
                    string ext = Path.GetExtension(files[0]);
                    if(".log" == ext || ".txt" == ext)
                    {
                        e.Effect = DragDropEffects.Copy;
                    }
                }
            }
        }

        private void Form1_DragLeave(object sender,EventArgs e)
        {

        }

        private void Form1_DragOver(object sender,DragEventArgs e)
        {

        }
    }
}