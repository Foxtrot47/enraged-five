using System;
using System.Collections.Generic;
using System.Reflection;

namespace Rockstar.LogJam
{
    public class WeakEvent<T> where T : EventArgs
    {
        class WeakDelegate
        {
            WeakReference m_Target;
            MethodInfo m_Method;
            public WeakDelegate m_Next;

            public WeakDelegate(EventHandler<T> handler)
            {
                m_Target = new WeakReference(handler.Target);
                m_Method = handler.Method;
            }

            public object Target{get{return m_Target.Target;}}
            public MethodInfo Method{get{return m_Method;}}
        }

        List<WeakDelegate> m_Handlers = new List<WeakDelegate>();
        object m_Owner;
        object m_LockObj = new object();

        public WeakEvent(object owner)
        {
            m_Owner = owner;
        }

        public void AddHandler(EventHandler<T> handler)
        {
            lock(m_LockObj)
            {
                if(!this.HaveHandler(handler))
                {
                    WeakDelegate wd = new WeakDelegate(handler);
                    m_Handlers.Add(wd);
                }
            }
        }

        public void RemoveHandler(EventHandler<T> handler)
        {
            lock(m_LockObj)
            {
                foreach(WeakDelegate wd in m_Handlers)
                {
                    if(handler.Target == wd.Target
                        && handler.Method == wd.Method)
                    {
                        m_Handlers.Remove(wd);
                        break;
                    }
                }
            }
        }

        public void Invoke(T args)
        {
            //Copy the list of delegates so we have no
            //chance of deadlock when we Invoke().
            WeakDelegate head = null, next = null;
            lock(m_LockObj)
            {
                foreach(WeakDelegate wd in m_Handlers)
                {
                    if(null == head)
                    {
                        head = next = wd;
                    }
                    else
                    {
                        next.m_Next = wd;
                        next = wd;
                    }

                    next.m_Next = null;
                }
            }

            //No lock here so we avoid deadlock when calling
            //the delegates.

            WeakDelegate removals = null;

            while(null != head)
            {
                WeakDelegate wd = head;
                head = head.m_Next;

                object target = wd.Target;
                if(null != target)
                {
                    wd.Method.Invoke(target, new object[] {m_Owner, args});
                }
                else if(null == removals)
                {
                    removals = next = wd;
                }
                else
                {
                    next.m_Next = wd;
                    next = wd;
                }

                wd.m_Next = null;
            }

            lock(m_LockObj)
            {
                for(WeakDelegate wd = removals; null != wd; wd = wd.m_Next)
                {
                    m_Handlers.Remove(wd);
                }
            }
        }

        bool HaveHandler(EventHandler<T> handler)
        {
            bool haveit = false;

            lock(m_LockObj)
            {
                foreach(WeakDelegate wd in m_Handlers)
                {
                    if(handler.Target == wd.Target
                        && handler.Method == wd.Method)
                    {
                        haveit = true;
                        break;
                    }
                }
            }

            return haveit;
        }
    }
}
