//------------------------------------------------------------------------------
// $Workfile: MapImage.c  $
//   Created: Clemens Pecinovsky
// Copyright: 2005, Rockstar Vienna
// $Revision: #2 $
//   $Author: krose $
//     $Date: 2006/06/29 $
//------------------------------------------------------------------------------
// Description: 
// This file implements functions for reading and writing mentalray map image files.


#include "MapImage.h"

#include <memory.h>
#include "string/string.h"
#include <stdlib.h>

//-----------------------------------------------------------------------------
MapImage::MapImage(const char* readFromFile)
//-----------------------------------------------------------------------------
: mWidth(0), mHeight(0), mBuffer(0)
{
    FILE *in;
    in = fopen(readFromFile, "rb");
    if (in!=NULL)
    {
        if (readHeader(in))
        {
            allocBuffer();
            readPixels(in);
        }
        fclose(in);
    }

}


//-----------------------------------------------------------------------------
MapImage::MapImage(int width, int height)
//-----------------------------------------------------------------------------
: mWidth(width), mHeight(height), mBuffer(0)
{
    allocBuffer();    
}


//-----------------------------------------------------------------------------
MapImage::~MapImage()
//-----------------------------------------------------------------------------
{
    if (mBuffer!=NULL)
    {
        delete mBuffer;
    }
}


//-----------------------------------------------------------------------------
bool MapImage::allocBuffer()
//-----------------------------------------------------------------------------
{
    if (mWidth * mHeight<=0)
    {
        return false;
    }

    int size = mWidth * mHeight * 4;
    mBuffer = rage_new float[size];
    memset(mBuffer, 0,  size*4 );
    return (mBuffer!=NULL);
}


//-----------------------------------------------------------------------------
bool MapImage::writeToFile(const char* fileName)
//-----------------------------------------------------------------------------
{
    if (mBuffer==NULL)
    {
        return false;
    }

    FILE *out;
    out = fopen(fileName, "wb");
    if (out!=NULL)
    {
        if (writeHeader(out))
        {
            writePixels(out);
        }
        fclose(out);
        return true;
    }
    else
    {
        return false;
    }
}


//-----------------------------------------------------------------------------
bool MapImage::readHeader(FILE *in)
//-----------------------------------------------------------------------------
{
    char header[184];
    memset(header,0,184);
    fread(header, 184, 1, in);
    if (strncmp(header,"xtIM",4)!=0)
    {
        return false;
    }
    int numBitsPerChannel = *((int*) (&header[160]));
    int numChannelsPerPixel= *((int*) (&header[164]));
    if ((numBitsPerChannel!=32) && (numChannelsPerPixel!=4))
    {
        return false;
    }

    mWidth = *((int*) (&header[152]));
    mHeight = *((int*) (&header[156]));
    
    return true;
}


//-----------------------------------------------------------------------------
bool MapImage::readPixels(FILE *in)
//-----------------------------------------------------------------------------
{
    float *lineData = rage_new float[mWidth];
    if (lineData ==NULL)
    {
        return false;
    }
    for (int line = 0; line <mHeight; line ++)
    {
        for (int channel =0; channel<4; channel++)
        {
            fseek(in, 184 + ((line * 4 )+ channel)*4, SEEK_SET);
            int location =0;
            fread(&location, 4, 1, in);

            fseek(in, location +64, SEEK_SET);
            memset(lineData,0,mWidth*sizeof(float));
            fread(lineData, mWidth, sizeof(float), in);
            for (int x = 0; x< mWidth; x++)
            {
                mBuffer[((line*mWidth)+ x)* 4 + channel] = lineData[x];
            }
        }
    }
    return true;
}


//-----------------------------------------------------------------------------
bool MapImage::writeHeader(FILE *out)
//-----------------------------------------------------------------------------
{
    char header[184];
    
    memset (header,0,184);
    strcpy(header,"xtIM");
    header[4]=13;
    header[172]=13;
    header[8]=1;
    header[8]=1;


    *((int*) (&header[160])) = 32;
    *((int*) (&header[164])) = 4;

    *((int*) (&header[152])) = mWidth;
    *((int*) (&header[156])) = mHeight;

    fwrite(header,184,1,out);
    return true;
}


//-----------------------------------------------------------------------------
bool MapImage::writePixels(FILE *out)
//-----------------------------------------------------------------------------
{
    const int dataOffset = mHeight * 4 * 4 + 120;
    for (int line = 0; line <mHeight; line ++)
    {
        for (int channel =0; channel<4; channel++)
        {
            int location = dataOffset + (mWidth * sizeof(float) * (line * 4 + channel));
            fwrite(&location, 4,1,out);
        }
    }

    for (int line = 0; line <mHeight; line ++)
    {
        for (int channel =0; channel<4; channel++)
        {
            for (int x = 0; x< mWidth; x++)
            {
                int position = (line * mWidth + x)*4 + channel;
                fwrite(&(mBuffer[position]),4,1,out);                
            }
        }
    }

    return true;
}


//-----------------------------------------------------------------------------
void MapImage::getPixel (int x, int y, float &red, float &green, float &blue, float &alpha) const
//-----------------------------------------------------------------------------
{
    int position = ((mHeight-y-1) * mWidth + x) * 4;
    red = mBuffer[position];
    green = mBuffer[position+1];
    blue = mBuffer[position+2];
    alpha = mBuffer[position+3];
}


//-----------------------------------------------------------------------------
void MapImage::putPixel (int x, int y, float red, float green, float blue, float alpha)
//-----------------------------------------------------------------------------
{
    int position = ((mHeight-y-1) * mWidth + x) * 4;
    mBuffer[position] = red;
    mBuffer[position+1] = green;
    mBuffer[position+2] = blue;
    mBuffer[position+3] = alpha;
}


//-----------------------------------------------------------------------------
int MapImage::getHeight()
//-----------------------------------------------------------------------------
{
    return mHeight;
}


//-----------------------------------------------------------------------------
int MapImage::getWidth()
//-----------------------------------------------------------------------------
{
    return mWidth;
}


//-----------------------------------------------------------------------------
bool MapImage::isMapFile(const char* fileName)
//-----------------------------------------------------------------------------
{
    const char* extension = strrchr(fileName,'.');
    if (extension==NULL)
    {
        return false;
    }
    else
    {
        return (_stricmp(extension, ".map")==0);
    }
}


//-----------------------------------------------------------------------------
void MapImage::copyBuffer(float* toBuffer)
//-----------------------------------------------------------------------------
{
    int size = mWidth * mHeight * 4 * sizeof(float);
    memcpy(toBuffer, mBuffer, size );
}
