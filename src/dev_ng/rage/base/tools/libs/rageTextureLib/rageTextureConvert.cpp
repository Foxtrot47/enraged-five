// 
// rageTextureLib/rageTextureConvert.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
//

//-----------------------------------------------------------------------------
#include <map>

#include <stdio.h>
#include <math.h>
#include <io.h>

#include "rageTextureConvert.h"
#include "MapImage.h"

#include <assert.h>

#include "devil/ilu.h"
#ifdef __cplusplus
extern "C" {
#endif
#include "devil/il_internal.h"
#ifdef __cplusplus
}
#endif

#include "libtiff/tiff.h"
#include "libtiff/tiffconf.h"
#include "libtiff/tiffio.h"
#include "libtiff/tiffvers.h"

#include "file/asset.h"
#include "file/device.h"

#include "vector/color32.h"

#include <algorithm>
#include <assert.h>
#include <vector>
#include <string>
#include <fstream>

//-----------------------------------------------------------------------------

#if __XENON
# if defined(_DEBUG)
# pragma comment(lib,"d3d9d.lib")
# elif __PROFILE
# pragma comment(lib,"d3d9i.lib")
# elif !__DEV
//# pragma comment(lib,"d3d9.lib")
# pragma comment(lib,"d3d9ltcg.lib")
# else
# pragma comment(lib,"d3d9i.lib")
# endif
#elif __WIN32PC
#pragma comment(lib,"d3d9.lib")
#endif

#if !__OPTIMIZED
#pragma comment(lib,"d3dx9d.lib")
#else
#pragma comment(lib,"d3dx9.lib")
#endif

//-----------------------------------------------------------------------------

#if USE_COMPRESSINATOR

#include "ATI_Compress.h"

#ifdef _DEBUG
	#if __TOOL
		#if __64BIT
			#pragma comment(lib,"..\\..\\..\\..\\..\\..\\..\\..\\3rdparty\\dev\\cli\\AMD\\ATI_Compress\\VC8\\MT DLL Debug\\x64\\ATI_Compress_MT_DLL_VC8.lib")
		#else
			#pragma comment(lib,"..\\..\\..\\..\\..\\..\\..\\..\\3rdparty\\dev\\cli\\AMD\\ATI_Compress\\VC8\\MT DLL Debug\\Win32\\ATI_Compress_MT_DLL_VC8.lib")
		#endif //__64BIT
	#else
		#if __64BIT
			#pragma comment(lib,"..\\..\\..\\..\\..\\..\\..\\..\\3rdparty\\dev\\cli\\AMD\\ATI_Compress\\VC8\\MT Debug\\x64\\ATI_Compress_MT_VC8.lib")
		#else
			#pragma comment(lib,"..\\..\\..\\..\\..\\..\\..\\..\\3rdparty\\dev\\cli\\AMD\\ATI_Compress\\VC8\\MT Debug\\Win32\\ATI_Compress_MT_VC8.lib")
		#endif //__64BIT
	#endif //__TOOL
#else
	#if __TOOL
		#if __64BIT
			#pragma comment(lib,"..\\..\\..\\..\\..\\..\\..\\..\\3rdparty\\dev\\cli\\AMD\\ATI_Compress\\VC8\\MT DLL Release\\x64\\ATI_Compress_MT_DLL_VC8.lib")
		#else
			#pragma comment(lib,"..\\..\\..\\..\\..\\..\\..\\..\\3rdparty\\dev\\cli\\AMD\\ATI_Compress\\VC8\\MT DLL Release\\Win32\\ATI_Compress_MT_DLL_VC8.lib")
		#endif //__64BIT
	#else
		#if __64BIT
			#pragma comment(lib,"..\\..\\..\\..\\..\\..\\..\\..\\3rdparty\\dev\\cli\\AMD\\ATI_Compress\\VC8\\MT Release\\x64\\ATI_Compress_MT_VC8.lib")	
		#else
			#pragma comment(lib,"..\\..\\..\\..\\..\\..\\..\\..\\3rdparty\\dev\\cli\\AMD\\ATI_Compress\\VC8\\MT Release\\Win32\\ATI_Compress_MT_VC8.lib")	
		#endif //__64BIT
	#endif //_TOOL
#endif //_DEBUG

#endif //USE_COMPRESSINATOR

//-----------------------------------------------------------------------------

#if USE_NVDXT

#pragma warning(push)
#pragma warning(disable:4668)
#pragma warning(disable:4189)

#include "dxtlib/dxtlib.h"

#ifdef _DEBUG
#if __TOOL
#pragma comment(lib,"..\\..\\..\\..\\..\\rage\\3rdParty\\NVidia\\NvDXT\\lib\\debug\\nvDXTlibMTDLLd.vc8.lib")
#else
#pragma comment(lib,"..\\..\\..\\..\\..\\rage\\3rdParty\\NVidia\\NvDXT\\lib\\debug\\nvDXTlibMTd.vc8.lib")
#endif //__TOOL
#else
#if __TOOL
#pragma comment(lib,"..\\..\\..\\..\\..\\rage\\3rdParty\\NVidia\\NvDXT\\lib\\release\\nvDXTlibMTDLL.vc8.lib")
#else
#pragma comment(lib,"..\\..\\..\\..\\..\\rage\\3rdParty\\NVidia\\NvDXT\\lib\\release\\nvDXTlibMT.vc8.lib")
#endif //__TOOL
#endif //_DEBUG

#pragma warning(pop)

#endif //USE_NVDXT

#if __XENON
#include "grcore/device.h"

#define D3DUSAGE_DYNAMIC	D3DUSAGE_CPU_CACHED_MEMORY	//HACK???
#define D3DLOCK_DISCARD		D3DLOCK_NOSYSLOCK			//HACK??
#endif

//-----------------------------------------------------------------------------

#define RTC_MAKEFOURCC(ch0, ch1, ch2, ch3)													 \
    ((unsigned int)(unsigned char)(ch0) | ((unsigned int)(unsigned char)(ch1) << 8) |		 \
    ((unsigned int)(unsigned char)(ch2) << 16) | ((unsigned int)(unsigned char)(ch3) << 24 ))


//-----------------------------------------------------------------------------

using namespace rage;
using namespace std;

extern __THREAD int RAGE_LOG_DISABLE;

float const minExponent = -16.0;
float const maxExponent = +16.0;

enum
{
	RED = 0,
	GREEN,
	BLUE,
	ALPHA
};

//-----------------------------------------------------------------------------
#if __XENON
static tsize_t RAGE_TIFFReadProc(thandle_t handle, tdata_t buffer, tsize_t size)
{
	return ((rage::fiStream*)handle)->Read(buffer, size);
}

static tsize_t RAGE_TIFFWriteProc(thandle_t handle, tdata_t buffer, tsize_t size)
{
	return ((rage::fiStream*)handle)->Write(buffer, size);
}

static toff_t RAGE_TIFFSeekProc(thandle_t handle, toff_t offset, int mode)
{
	mode;
	return (toff_t)((rage::fiStream*)handle)->Seek(offset);
}

static int RAGE_TIFFCloseProc(thandle_t handle)
{
	return ((rage::fiStream*)handle)->Close();
}

static toff_t RAGE_TIFFSizeProc(thandle_t handle)
{
	return (toff_t)((rage::fiStream*)handle)->Size();
}

static int RAGE_TIFFMapFileProc(thandle_t, tdata_t*, toff_t*)
{
	return 0;
}

static void RAGE_TIFFUnmapFileProc(thandle_t, tdata_t, toff_t)
{
}
#endif //__XENON
//-----------------------------------------------------------------------------


namespace rage
{

	//-----------------------------------------------------------------------------
	//Static member initialization
	//-----------------------------------------------------------------------------
	std::map<std::string, D3DFORMAT> TextureConvert::sD3DFormats;
	std::map<std::string, TextureConvert::PixelConvertor*> TextureConvert::sConversionProcesses;


	//-----------------------------------------------------------------------------
	//Utility fns
	//-----------------------------------------------------------------------------
	std::string stripIt( const char* fname )
	{
		std::string s( fname );
		//std::string s2  = s.substr( 0, s.length()-4 );
		std::string s2  = s.substr( 0, min( s.find_last_of('\\'), s.find_last_of('/') ) );
		return s2;
	}

	//-----------------------------------------------------------------------------
	// Conversion operators
	//-----------------------------------------------------------------------------
	class MegaChannelTexture : public TextureConvert::Convertor 
	{
	protected:
		std::map<int, int>							m_ColorRemapTable;
		typedef std::map<int, int>::iterator		LutIterator;

	public:
		virtual ~MegaChannelTexture(){};
		virtual bool Start( const char* inputFileName ) 
		{
			// load in input file and create list from it
			std::string fname = stripIt( inputFileName );
			fname += "\\channellookup.txt";

			++RAGE_LOG_DISABLE;
				std::ifstream in( fname.c_str() );
			--RAGE_LOG_DISABLE;
			
			if(!in.is_open())
			{
				Errorf("Error: Failed to open channel lookup file '%s'.", fname.c_str());
				return false;
			}

			while( in )
			{
				int vR;
				int vG;
				int vB;
				int index;
		
				in >> vR;
				in >> vG;
				in >> vB;
				in >> index;
				
				int val = vR << 16 | vG << 8 | vB;
				m_ColorRemapTable[ val ] = index;
			}

			in.close();

			return true;
		}

		virtual void Convert(const float* src, float* des,  TCConvertOptions* /*pOpts */ ) 
		{
			static int sMissingClrCount = 0;

			int vR = (int)( src[RED] * 255.0f);
			int vG = (int)( src[GREEN] * 255.0f );
			int vB = (int)( src[BLUE ] * 255.0f);

			int val = vR << 16 | vG << 8 | vB;

			LutIterator it = m_ColorRemapTable.find( val );
		
			des[RED] = 0.0f;
			des[BLUE] = 0.0f;
			des[GREEN] = 0.0f;

			if ( it != m_ColorRemapTable.end() )
			{
				int type = it->second ;

				if ( type < 0 )
				{
					Warningf(" Color  R %i  G %i  B %i  has been deprecated and is not in the texture atlas any more\n", vR, vG, vB );
					return;
				}
				float v = (float)type / 255.0f;

				des[ RED ] = v;
				des[ GREEN ] = v;
				des[ BLUE ] = v;
				des[ ALPHA ] = v;
			}
			else
			{
				if( sMissingClrCount < 64)
				{
					Warningf(" Color  R %i  G %i  B %i  not found in lookup table\n", vR, vG, vB );
					sMissingClrCount++;
				}
				else if( sMissingClrCount == 64 )
				{
					Warningf(" More than 64 pixel values could not be found in the color table, so I won't annoy you with more warnings...");
					sMissingClrCount++;
				}
			}
		}
		virtual void End()
		{}

		bool RequiresMipFilterOverride() { return true; } 
		TCConvertOptions::TCMipFilter GetMipFilterOverride() { return TCConvertOptions::MIPFILTER_POINT; }
	};
	//-----------------------------------------------------------------------------
	class MegaChannelTexture2 : public MegaChannelTexture
	{
	public:
		enum CHANNEL_MODE
		{
			kMode012,
			kMode345
		};

		MegaChannelTexture2(CHANNEL_MODE mode)
			: m_mode( mode )
		{};

		void SetChannelMode(CHANNEL_MODE mode) { m_mode = mode; }
	protected:
		virtual bool Start( const char* inputFileName )
		{
			if(!MegaChannelTexture::Start(inputFileName))
				return false;
			
			// load in input file and create list from it
			std::string fname = stripIt( inputFileName );

			if(m_mode == kMode012)
			{
				fname += "\\channelweights012.txt";
			}
			else
			{
				fname += "\\channelweights345.txt";
			}
			

			++RAGE_LOG_DISABLE;
				std::ifstream in( fname.c_str() );
			--RAGE_LOG_DISABLE;

			if(!in.is_open())
			{
				Errorf("Error: Failed to open channel weight file '%s'.", fname.c_str());
				return false;
			}

			for(int i=0; i<8; i++)
			{
				int index;
				float r,g,b;

				in >> index;
				in >> r;
				in >> g;
				in >> b;

				m_weights[i] = Color32(r,g,b);
			}

			in.close();

			return true;
		}

		virtual void Convert(const float* src, float* des,  TCConvertOptions* /*pOpts */ ) 
		{
			static int sMissingClrCount = 0;

			int vR = (int)( src[RED] * 255.0f);
			int vG = (int)( src[GREEN] * 255.0f );
			int vB = (int)( src[BLUE ] * 255.0f);

			int val = vR << 16 | vG << 8 | vB;

			LutIterator it = m_ColorRemapTable.find( val );
			if ( it != m_ColorRemapTable.end() )
			{
				int oldindex = it->second ;
				int newindex = (oldindex < 8) ? oldindex : oldindex / 8;

				Color32 colorWeight = m_weights[newindex];

				des[ RED ] = colorWeight.GetRedf();
				des[ GREEN ] = colorWeight.GetGreenf();
				des[ BLUE ] = colorWeight.GetBluef();
				des[ ALPHA ] = src[ALPHA];
			}
			else
			{
				if( sMissingClrCount < 64)
				{
					Warningf(" Color  R %i  G %i  B %i  not found in lookup table\n", vR, vG, vB );
					sMissingClrCount++;
				}
				else if( sMissingClrCount == 64 )
				{
					Warningf(" More than 64 pixel values could not be found in the color table, so I won't annoy you with more warnings...");
					sMissingClrCount++;
				}
			}
		}

	private:
		CHANNEL_MODE	m_mode;
		Color32			m_weights[8];
	};

	//-----------------------------------------------------------------------------
	class ConvertToDXT1PartialDerivativeNormalMap : public TextureConvert::Convertor 
	{
	public:
		void  Convert(const float* src, float* des, TCConvertOptions* /*pOpts */ )
		{
			const float nx = src[ GREEN ] * 2.0f - 1.0f;
			const float ny = src[ RED ]   * 2.0f - 1.0f;
			const float nz = src[ BLUE ]  * 2.0f - 1.0f;
			des[RED]   = (-ny / nz) * 0.5f + 0.5f;
			des[GREEN] = (-nx / nz) * 0.5f + 0.5f;
			des[BLUE]  = 1.0f;
			des[ALPHA] = 1.0f;
		}
	};

	//-----------------------------------------------------------------------------
	class ConvertToDXT5NormalMap : public TextureConvert::Convertor 
	{
	public:
		void  Convert(const float* src, float* des, TCConvertOptions* /*pOpts */ )
		{
			des[ALPHA] = src[ RED ];
			des[GREEN] = src[ GREEN ];
			des[RED] = 0.0f;
			des[BLUE] = 0.0f;
		}
	};
	//-----------------------------------------------------------------------------
	class ConvertToDXT5NormalDisplacementMap : public TextureConvert::Convertor 
	{
	public:
		void  Convert(const float* src, float* des, TCConvertOptions* /*pOpts */ )
		{
			des[ALPHA] = src[ RED ];
			des[GREEN] = src[ GREEN ];
			des[RED] = src[ALPHA];
			des[BLUE] = 0.0f;
		}
	};

	class ConvertToDXT5PartialDerivativeNormalMap : public TextureConvert::Convertor 
	{
	public:
		void  Convert(const float* src, float* des, TCConvertOptions* /*pOpts */ )
		{
			const float nx = src[ GREEN ] * 2.0f - 1.0f;
			const float ny = src[ RED ] * 2.0f - 1.0f;
			const float nz = src[ BLUE ] * 2.0f - 1.0f;
			des[ALPHA] = (-ny / nz) * 0.5f + 0.5f;
			des[GREEN] = (-nx / nz) * 0.5f + 0.5f;
			des[RED] = src[ALPHA];
			des[BLUE] = 1.0f;
		}
	};
	//-----------------------------------------------------------------------------
	class ConvertToQuantizedHDRExpOff : public TextureConvert::Convertor 
	{
	public:
		void Convert( const float* src, float* des, TCConvertOptions* pOpts )
		{
			//Quantize the colors based on the calculated sRGB space offset and exponents
			des[RED]   = RTCGammaConvert_10To22( ((RTCGammaConvert_22To10(src[RED]) - pOpts->HDROffR[TCConvertOptions::LINEAR]) / pOpts->HDRExpR[TCConvertOptions::LINEAR]) );
			des[GREEN] = RTCGammaConvert_10To22( ((RTCGammaConvert_22To10(src[GREEN]) - pOpts->HDROffG[TCConvertOptions::LINEAR]) / pOpts->HDRExpG[TCConvertOptions::LINEAR]) );
			des[BLUE]  = RTCGammaConvert_10To22( ((RTCGammaConvert_22To10(src[BLUE]) - pOpts->HDROffB[TCConvertOptions::LINEAR]) / pOpts->HDRExpB[TCConvertOptions::LINEAR]) );
			des[ALPHA] = RTCGammaConvert_10To22( ((RTCGammaConvert_22To10(src[ALPHA]) - pOpts->HDROffA[TCConvertOptions::LINEAR]) / pOpts->HDRExpA[TCConvertOptions::LINEAR]) );
		}
	};

	class ConvertToBlackToGrey : public TextureConvert::Convertor 
	{
	public:
		void  Convert(const float* src, float* des, TCConvertOptions* /*pOpts */ )
		{
			const float r = src[ GREEN ];
			const float g = src[ RED ];
			const float b = src[ BLUE ];
			if ( r == 0.0f && g == 0.0f && b == 0.0f)
			{
				des[RED]   = 0.5f;
				des[GREEN] = 0.5f;
				des[BLUE]  = 0.5f;
			}
			else
			{
				des[RED]   = r;
				des[GREEN] = g;
				des[BLUE]  = b;
			}
			des[ALPHA] = 1.0f;
		}
	};

	//-----------------------------------------------------------------------------
	void TextureConvert::initD3DFormats()
	{
		if (sD3DFormats.size()>0)
		{
			return;
		}
		sD3DFormats[std::string("DXT1")] = D3DFMT_DXT1;
		sD3DFormats[std::string("DXT2")] = D3DFMT_DXT2;
		sD3DFormats[std::string("DXT3")] = D3DFMT_DXT3;
		sD3DFormats[std::string("DXT4")] = D3DFMT_DXT4;
		sD3DFormats[std::string("DXT5")] = D3DFMT_DXT5;
		sD3DFormats[std::string("A8")] = D3DFMT_A8;
		sD3DFormats[std::string("L8")] = D3DFMT_L8;
		sD3DFormats[std::string("R5G6B5")] = D3DFMT_R5G6B5;
		sD3DFormats[std::string("L6V5U5")] = D3DFMT_L6V5U5;
		sD3DFormats[std::string("X1R5G5B5")] = D3DFMT_X1R5G5B5;
		sD3DFormats[std::string("A1R5G5B5")] = D3DFMT_A1R5G5B5;
		sD3DFormats[std::string("A4R4G4B4")] = D3DFMT_A4R4G4B4;
		sD3DFormats[std::string("X4R4G4B4")] = D3DFMT_X4R4G4B4;
		sD3DFormats[std::string("A8L8")] = D3DFMT_A8L8;
		sD3DFormats[std::string("V8U8")] = D3DFMT_V8U8;
		sD3DFormats[std::string("D16")] = D3DFMT_D16;
		sD3DFormats[std::string("L16")] = D3DFMT_L16;
		sD3DFormats[std::string("R16F")] = D3DFMT_R16F;
		sD3DFormats[std::string("UYVY")] = D3DFMT_UYVY;
		sD3DFormats[std::string("G8R8_G8B8")] = D3DFMT_G8R8_G8B8;
		sD3DFormats[std::string("R8G8_B8G8")] = D3DFMT_R8G8_B8G8;
		sD3DFormats[std::string("YUY2")] = D3DFMT_YUY2;
		sD3DFormats[std::string("A8R8G8B8")] = D3DFMT_A8R8G8B8;
		sD3DFormats[std::string("X8R8G8B8")] = D3DFMT_X8R8G8B8;
		sD3DFormats[std::string("A8B8G8R8")] = D3DFMT_A8B8G8R8;
		sD3DFormats[std::string("X8B8G8R8")] = D3DFMT_X8B8G8R8;
		sD3DFormats[std::string("X8L8V8U8")] = D3DFMT_X8L8V8U8;
		sD3DFormats[std::string("Q8W8V8U8")] = D3DFMT_Q8W8V8U8;
		sD3DFormats[std::string("A2R10G10B10")] = D3DFMT_A2R10G10B10;
		sD3DFormats[std::string("A2B10G10R10")] = D3DFMT_A2B10G10R10;
		sD3DFormats[std::string("A2W10V10U10")] = D3DFMT_A2W10V10U10;
		sD3DFormats[std::string("G16R16")] = D3DFMT_G16R16;
		sD3DFormats[std::string("V16U16")] = D3DFMT_V16U16;
		sD3DFormats[std::string("G16R16F")] = D3DFMT_G16R16F;
		sD3DFormats[std::string("R32F")] = D3DFMT_R32F;
		sD3DFormats[std::string("A16B16G16R16")] = D3DFMT_A16B16G16R16;
		sD3DFormats[std::string("Q16W16V16U16")] = D3DFMT_Q16W16V16U16;
		sD3DFormats[std::string("A16B16G16R16F")] = D3DFMT_A16B16G16R16F;
		sD3DFormats[std::string("G32R32F")] = D3DFMT_G32R32F;
		sD3DFormats[std::string("A32B32G32R32F")] = D3DFMT_A32B32G32R32F;
		
		sD3DFormats["HDR_DXT1"] = D3DFMT_DXT1;
		sD3DFormats["HDR_DXT5"] = D3DFMT_DXT5;

		sD3DFormats["COMP_NRM_DXT1"] = D3DFMT_DXT1;
		
		sD3DFormats["COMP_NRM_PD_DXT1"] = D3DFMT_DXT1;
		sConversionProcesses[ "COMP_NRM_PD_DXT1"] = rage_new ConvertToDXT1PartialDerivativeNormalMap();

		sD3DFormats[ "COMP_NRM_DXT5"] = D3DFMT_DXT5;	
		sConversionProcesses[ "COMP_NRM_DXT5"] = rage_new ConvertToDXT5NormalMap();

		sD3DFormats[ "COMP_NRM_DISP_DXT5"] = D3DFMT_DXT5;	
		sConversionProcesses[ "COMP_NRM_DISP_DXT5"] = rage_new ConvertToDXT5NormalDisplacementMap();

		sD3DFormats[ "COMP_NRM_PD_DXT5"] = D3DFMT_DXT5;	
		sConversionProcesses[ "COMP_NRM_PD_DXT5"] = rage_new ConvertToDXT5PartialDerivativeNormalMap();

		sD3DFormats[ "MEGA_CHANNEL"] = D3DFMT_L8;	
		sConversionProcesses[ "MEGA_CHANNEL"] = rage_new MegaChannelTexture();

		sD3DFormats[ "MEGA_CHANNEL_012"] = D3DFMT_A8R8G8B8;
		sConversionProcesses[ "MEGA_CHANNEL_012"] = rage_new MegaChannelTexture2(MegaChannelTexture2::kMode012);

		sD3DFormats[ "MEGA_CHANNEL_345"] = D3DFMT_R8G8B8;
		sConversionProcesses[ "MEGA_CHANNEL_345"] = rage_new MegaChannelTexture2(MegaChannelTexture2::kMode345);

		sD3DFormats[ "MEGA_CHANNEL_MAYA" ] = D3DFMT_A8R8G8B8;
		sConversionProcesses[ "MEGA_CHANNEL_MAYA" ] = rage_new MegaChannelTexture();

		sConversionProcesses[ "HDR_DXT1"] = rage_new  ConvertToQuantizedHDRExpOff;
		sConversionProcesses[ "HDR_DXT5"] = rage_new ConvertToQuantizedHDRExpOff;

		sD3DFormats[ "AO_SAFE_MIPS" ] = D3DFMT_A8R8G8B8;
		sConversionProcesses[ "AO_SAFE_MIPS"] = rage_new ConvertToBlackToGrey();

		sD3DFormats[ "GUESS_COLOR"] = D3DFMT_DXT5;	

	}
	//-----------------------------------------------------------------------------
	TextureConvert::TextureConvert( )
		: mPTexture(NULL)
		, mPD3D(NULL)
		, mPD3DDevice(NULL)
		, mInputGamma(2.2f)
		, mPCubeTexture(NULL)
		, mPVolumeTexture(NULL)
		, m_writeLgExpOff(false)
	{
		initD3DFormats();
		createNULLRefDevice();

		//The default error handler throws up dialog boxes when it hits an error which is bad for batch builds
		//so switch to a custom error handler
		TIFFSetErrorHandler(&TextureConvert::TIFFErrorHandler);
	}
	//-----------------------------------------------------------------------------
	TextureConvert::~TextureConvert( )
	{
		releaseTexture();
		releaseNULLRefDevice();

		sD3DFormats.clear();
		for(ProcessIterator it = sConversionProcesses.begin(); it != sConversionProcesses.end(); ++it)
		{
			if(it->second)
				delete it->second;
		}
		sConversionProcesses.clear();
	}
	//-----------------------------------------------------------------------------
	void TextureConvert::DisableTIFFWarnings()
	{
		TIFFSetWarningHandler(NULL);
		TIFFSetWarningHandlerExt(NULL);
	}
	//-----------------------------------------------------------------------------
	void TextureConvert::TIFFErrorHandler(const char* /*module*/, const char* fmt, va_list ap)
	{
		char strBuf[2048];
		strBuf[0] = 0;
		vsprintf(strBuf, fmt, ap);
		Errorf("%s",strBuf);
	}
	//-----------------------------------------------------------------------------
	bool TextureConvert::createNULLRefDevice( )
	{
#if __WIN32PC
		HRESULT hr = 0;
		if (mPD3D==NULL)
		{
			mPD3D = Direct3DCreate9( D3D_SDK_VERSION );
		}
		if( NULL == mPD3D )
			return false;

		D3DDISPLAYMODE Mode;
		mPD3D->GetAdapterDisplayMode(0, &Mode);

		D3DPRESENT_PARAMETERS pp;
		ZeroMemory( &pp, sizeof(D3DPRESENT_PARAMETERS) ); 
		pp.BackBufferWidth  = 1;
		pp.BackBufferHeight = 1;
		pp.BackBufferFormat = Mode.Format;
		pp.BackBufferCount  = 1;
		pp.SwapEffect       = D3DSWAPEFFECT_COPY;
		pp.Windowed         = TRUE;

		if (mPD3DDevice==NULL)
		{
			hr = mPD3D->CreateDevice( D3DADAPTER_DEFAULT, D3DDEVTYPE_NULLREF, GetDesktopWindow(), 
				D3DCREATE_HARDWARE_VERTEXPROCESSING, &pp, &mPD3DDevice );
		}

		if( FAILED(hr) || mPD3DDevice == NULL )
		{
			releaseNULLRefDevice();
			return false;
		}
#else
		mPD3DDevice = (LPDIRECT3DDEVICE9)GRCDEVICE.GetCurrent();
		Assertf(mPD3DDevice, "grcDevice must be initialized before TextureConverter construction");
		mPD3D = NULL;
#endif
		return true;
	}
	//-----------------------------------------------------------------------------
	void TextureConvert::releaseNULLRefDevice( )
	{
#if __WIN32PC
		if( mPD3DDevice != NULL) 
		{
			mPD3DDevice->Release();
			mPD3DDevice = NULL;
		}

		if( mPD3D != NULL)
		{
			mPD3D->Release();
			mPD3D = NULL;
		}
#else
		mPD3DDevice = NULL;
		mPD3D = NULL;
#endif 
	}
	//-----------------------------------------------------------------------------
	bool TextureConvert::loadTexture(LPCTSTR pSrcFile)
	{
		releaseTexture();
		return loadTextureInternal(pSrcFile, mPTexture);
	}
	bool TextureConvert::ConvertVolumeTextureTo565(int twidth, int numLayers, int numMips )
	{
		Assert( mPVolumeTexture != NULL);

		D3DFORMAT	fmt = D3DFMT_R5G6B5;
		int			fmtNumByes = 2;
		//D3DFORMAT	fmt = D3DFMT_A8R8G8B8;
		//int			fmtNumByes = 4;

		LPDIRECT3DVOLUMETEXTURE9	compressVolTex;
#if __ASSERT
		HRESULT hr= D3DXCreateVolumeTexture(
			mPD3DDevice, twidth, twidth, numLayers, numMips,D3DUSAGE_DYNAMIC,
			fmt, D3DPOOL_SYSTEMMEM, &compressVolTex);
		Assert( SUCCEEDED(hr));
#else
		D3DXCreateVolumeTexture(
			mPD3DDevice, twidth, twidth, numLayers, numMips,D3DUSAGE_DYNAMIC,
			fmt, D3DPOOL_SYSTEMMEM, &compressVolTex);
#endif
		// now copy accross
		for (int j = 0; j < numMips; j++){
			D3DLOCKED_BOX lockedBox;
			if(!SUCCEEDED(mPVolumeTexture->LockBox(j, &lockedBox,NULL,0)))
			{
				releaseTexture();
				compressVolTex->Release();
				return false;
			}
			D3DLOCKED_BOX lockedBoxCmp;
			if(!SUCCEEDED(compressVolTex->LockBox(j, &lockedBoxCmp,NULL,0)))
			{
				releaseTexture();
				compressVolTex->Release();
				return false;
			}
			int nls = numLayers>>j;
			for(int i = 0; i < nls; i++)
			{
				void* pCmpBits = reinterpret_cast<char*>(lockedBoxCmp.pBits) + lockedBoxCmp.SlicePitch *(i);
				void* pBits = reinterpret_cast<char*>(lockedBox.pBits) + lockedBox.SlicePitch *(i);
				
				int w = twidth>>j;
				if ( fmt ==D3DFMT_R5G6B5 ){
					for (int k =0; k <  w; k++){
						u16* out=(u16*)(reinterpret_cast<char*>(pCmpBits)+w*fmtNumByes*k);
						float* incol=(float*)(reinterpret_cast<char*>(pBits)+w*4*4*k);
						for (int j=0; j < w;j++){
							//out[j*4+0]=incol[j*4*2+0] ;  // A
							int r = (int)(32.f*incol[j*4+0]) ;// R
							int g =  (int)(64.f*incol[j*4+1]) ;// G
							int b =   (int)(32.f*incol[j*4+2]) ;// B
							out[j]=((r&31)<<11) | ((g&63)<<5) | (b&31);						
						}
					}
				}
				else
				{
					for (int k =0; k <  w; k++){
						u8* out=(u8*)(reinterpret_cast<char*>(pCmpBits)+w*fmtNumByes*k);
						float* incol=(float*)(reinterpret_cast<char*>(pBits)+w*4*4*k);
						for (int j=0; j < w;j++){
							//out[j*4+0]=incol[j*4*2+0] ;  // A
							int r = (int)(255.f*incol[j*4+0]) ;// R
							int g =  (int)(255.f*incol[j*4+1]) ;// G
							int b =   (int)(255.f*incol[j*4+2]) ;// B
							out[j*4+0]=(u8)r&0xFF;
							out[j*4+1]=(u8)g&0xFF;
							out[j*4+2]=(u8)b&0xFF;
							out[j*4+3]=0xFF;
						}
					}
				}

			}

			mPVolumeTexture->UnlockBox(j);
			compressVolTex->UnlockBox(j);
		}
		while(mPVolumeTexture->Release());
		mPVolumeTexture = compressVolTex;
		return true;
	}
	//-----------------------------------------------------------------------------
	bool TextureConvert::loadArrayTextures(atArray<std::string>& texturePaths, int numLayers, int twidth)
	{
		Assert(texturePaths.GetCount() == numLayers);

		//Free up any pre-existing cube map texture
		if(mPVolumeTexture)
		{
			while(mPVolumeTexture->Release());
			mPVolumeTexture = NULL;
		}
		// also dumps a atlas texture
		if ( mPTexture)
		{
			while(mPTexture->Release());
			mPTexture = NULL;
		}
		int atlasWidth = (int)sqrtf((float)numLayers/2);
		D3DLOCKED_RECT atTex;
		// create atlas texture
		D3DXCreateTexture(
			mPD3DDevice, twidth*atlasWidth, twidth*atlasWidth,  1,D3DUSAGE_DYNAMIC,
			D3DFMT_A8B8G8R8, D3DPOOL_SYSTEMMEM, &mPTexture);

		if(mPTexture == NULL)
		{
			return false;
		}
		if(!SUCCEEDED(mPTexture->LockRect(0, &atTex, NULL, D3DLOCK_DISCARD )))
		{
			releaseTexture();
			return false;
		}


		for(int i = 0; i < numLayers; i+=2)
		{
			//Load the face texture
			LPDIRECT3DTEXTURE9 tex = NULL;
			std::string pth = texturePaths[i];
			pth +=".tif";
			if(!loadTextureInternal(pth.c_str(), tex))
			{
				Errorf("Error: Failed to load texture \"%s\"", texturePaths[i].c_str());
				return false;
			}

			LPDIRECT3DTEXTURE9 bmp = NULL;
			pth = texturePaths[i+1];
			pth +=".tif";
			if(!loadTextureInternal(pth.c_str(), bmp))
			{
				Errorf("Error: Failed to load texture \"%s\"", texturePaths[i+1].c_str());
				return false;
			}

			//Get the face texture descriptor
			D3DSURFACE_DESC tDesc;
			getDescription(tDesc, tex, 0);			
			if(i == 0)
			{
				//This is the first face texture, so create the main internal cube texture to hold the data
				D3DXCreateVolumeTexture(
					mPD3DDevice, twidth, twidth, numLayers, 3,D3DUSAGE_DYNAMIC,
					D3DFMT_A32B32G32R32F, D3DPOOL_SYSTEMMEM, &mPVolumeTexture);

				if(mPVolumeTexture == NULL)
				{
					if(tex)
						tex->Release();
					return false;
				}

				
			}

			//Copy the face texture data we loaded into the proper face texture of the internal Cube-Map
			D3DLOCKED_RECT faceLR;
			if(!SUCCEEDED(tex->LockRect(0, &faceLR, NULL, D3DLOCK_DISCARD )))
			{
				mPVolumeTexture->Release();
				tex->Release();
				bmp->Release();
				return false;
			}
			
			D3DLOCKED_RECT bmpTex;
			if(!SUCCEEDED(bmp->LockRect(0, &bmpTex, NULL, D3DLOCK_DISCARD )))
			{
				mPVolumeTexture->Release();
				tex->Release();
				bmp->Release();
				return false;
			}


			D3DLOCKED_BOX lockedBox;
			if(!SUCCEEDED(mPVolumeTexture->LockBox(0, &lockedBox,NULL,0)))
			{
				releaseTexture();
				tex->Release();
				return false;
			}
			
		
			// normalize the texture
			float* intensity = rage_new float[ twidth* twidth];			
			float avg=0.f;
			float maxv = -1000.f;
			float minv = 1000.0f;
			for (int k =0; k <  twidth; k++)
			{
				float* incol = (float*)(reinterpret_cast<char*>(faceLR.pBits)+faceLR.Pitch *k);
				for (int j=0; j < twidth;j++)
				{
					intensity[j + twidth*k]=incol[j*4+2] ;
					avg+=incol[j*4+2] ;
					maxv=Max(incol[j*4+2], maxv);
					minv=Min(incol[j*4+2], minv);
				}
			}
			avg *=1.f/(twidth*twidth);
			float avg2 = 0.f;
			// now make avg 50% grey
			for (int k =0; k <  twidth; k++)
			{
				for (int j=0; j < twidth;j++)
				{
					float f = intensity[j + twidth*k];
					f = Max(Min( (f-avg)+.5f,1.f),0.f);
					assert( f >=0.f && f<=1.f);
					intensity[j + twidth*k]= f;
					avg2+=f;
				}
			}
			printf(" Average 2 %f \n",avg2 );

			int atOffX = (i/2)%atlasWidth;
			int atOffY = (i/2)/atlasWidth;
			atOffX *=twidth;
			atOffY *=twidth;
			for (int dup=0; dup<2; dup++){

				void* pBits = reinterpret_cast<char*>(lockedBox.pBits) + lockedBox.SlicePitch *(i + dup);

				//Copy the mip0 data into the final texture
				for (int k =0; k <  twidth; k++){
					float* out=(float*)(reinterpret_cast<char*>(pBits)+twidth*4*4*k);
					
					unsigned char* outAtlas = (unsigned char*)(reinterpret_cast<unsigned char*>(atTex.pBits)+atTex.Pitch *(atOffY+k) +atOffX*4 );
				//	float* incol = (float*)(reinterpret_cast<char*>(faceLR.pBits)+faceLR.Pitch *k);
					float* bmpv = (float*)(reinterpret_cast<char*>(bmpTex.pBits)+bmpTex.Pitch *k);

					for (int j=0; j < twidth;j++){
						out[j*4+3]=1.;  // A
						out[j*4+0]=intensity[j + twidth*k];//incol[j*4+2] ;// R

						out[j*4+1]=bmpv[j*4+0] ;// G
						out[j*4+2]=bmpv[j*4+1] ;// B

						outAtlas[j*4+3]=255;
						outAtlas[j*4+0]=(unsigned char)(out[j*4+0]*255.);
						outAtlas[j*4+1]=(unsigned char)(out[j*4+1]*255.);
						outAtlas[j*4+2]=(unsigned char)(out[j*4+2]*255.);

					}
				}

			}
			delete[] intensity;
			tex->UnlockRect(0);
			mPVolumeTexture->UnlockBox(0);

			tex->Release();

		}//End for(int faceIdx...

		mPTexture->UnlockRect(0);

		// generate lower resolution mip
		D3DLOCKED_BOX lockedBoxLow;
		if(!SUCCEEDED(mPVolumeTexture->LockBox(1, &lockedBoxLow,NULL,0)))
		{
			releaseTexture();
		}
		D3DLOCKED_BOX lockedBoxHigh;
		if(!SUCCEEDED(mPVolumeTexture->LockBox(0, &lockedBoxHigh,NULL,0)))
		{
			releaseTexture();
		}
		// generate middle mip
		for(int i = 0; i < numLayers/2; i++)
		{
			void* plowBits = reinterpret_cast<char*>(lockedBoxLow.pBits) + lockedBoxLow.SlicePitch *(i);
			void* pHitBits = reinterpret_cast<char*>(lockedBoxHigh.pBits) + lockedBoxHigh.SlicePitch *(i*2);

			int w = twidth/2;
			for (int k =0; k <  w; k++){
				float* out=(float*)(reinterpret_cast<char*>(plowBits)+w*4*4*k);
				float* incol=(float*)(reinterpret_cast<char*>(pHitBits)+twidth*4*4*k);
				int nk=(k+1)%w;
				float* incolStepY=(float*)(reinterpret_cast<char*>(pHitBits)+twidth*4*4*nk);
				
				// do simple box filtering may need to do mitchel
				for (int j=0; j < w;j++){
					int nj=(j+1)%w;
					for (int c=0;c<4;c++){
						out[j*4+c]=incol[j*4*2+c]*.25f+incol[nj*4*2+c]*.25f+
									incolStepY[j*4*2+c]*.25f+incolStepY[nj*4*2+c]*.25f;  
					}					
				}
			}

		}

		mPVolumeTexture->UnlockBox(0);
		mPVolumeTexture->UnlockBox(1);

		D3DLOCKED_BOX lockedBoxVLow;
		if(!SUCCEEDED(mPVolumeTexture->LockBox(2, &lockedBoxVLow,NULL,0)))
		{
			releaseTexture();
		}
		for(int i = 0; i < numLayers/4; i++)
		{
			void* plowBits = reinterpret_cast<char*>(lockedBoxVLow.pBits) + lockedBoxVLow.SlicePitch *(i);
			float* out=(float*)(reinterpret_cast<char*>(plowBits));
			int w = twidth/4;
			for (int k =0; k <  w*w*4; k++)
				out[k]=0.5;  // A

		}

		mPVolumeTexture->UnlockBox(2);
		ConvertVolumeTextureTo565( twidth, numLayers, 3 );
		return true;
	}

	bool TextureConvert::loadCubeTextures(atArray<std::string>& cubeTexturePaths)
	{
		Assert(cubeTexturePaths.GetCount() == 6);

		//Free up any pre-existing cube map texture
		if(mPCubeTexture)
		{
			while(mPCubeTexture->Release())
				;
			mPCubeTexture = NULL;
		}

		for(int faceIdx=0; faceIdx<6; faceIdx++)
		{
			//Load the face texture
			LPDIRECT3DTEXTURE9 faceTex = NULL;
			if(!loadTextureInternal(cubeTexturePaths[faceIdx].c_str(), faceTex))
			{
				Errorf("Error: Failed to load texture \"%s\"", cubeTexturePaths[faceIdx].c_str());
				return false;
			}

			//Get the face texture descriptor
			D3DSURFACE_DESC faceDesc;
			getDescription(faceDesc, faceTex, 0);

			//Make sure the the texture we loaded is square (requirement of cube-maps)
			if(faceDesc.Width != faceDesc.Height)
			{
				Errorf("Error: Cube-Map face texture \"%s\", dimensions (%d x %d) are not square.",
					cubeTexturePaths[faceIdx].c_str(), faceDesc.Width, faceDesc.Height);
				faceTex->Release();
				return false;
			}

			if(faceIdx == 0)
			{
				//This is the first face texture, so create the main internal cube texture to hold the data
				D3DXCreateCubeTexture(mPD3DDevice, faceDesc.Width,0,D3DUSAGE_DYNAMIC,
					D3DFMT_A32B32G32R32F, D3DPOOL_SYSTEMMEM, &mPCubeTexture);
				if(mPCubeTexture == NULL)
				{
					if(faceTex)
						faceTex->Release();
					return false;
				}					  
			}

			//Make sure the incoming face texture dimensions matches the cube texture dimensions
			D3DSURFACE_DESC dstFaceDesc;
			mPCubeTexture->GetLevelDesc(0, &dstFaceDesc);

			if( (faceDesc.Width != dstFaceDesc.Width) ||
				(faceDesc.Height != dstFaceDesc.Height) )
			{
				Errorf("Error: Cube-Map face texture \"%s\", has different dimensions (%d x %d) than the Cube-Map (%d x %d)",
					cubeTexturePaths[faceIdx].c_str(), faceDesc.Width, faceDesc.Height, dstFaceDesc.Width, dstFaceDesc.Height);
				mPCubeTexture->Release();
				faceTex->Release();
				return false;
			}

			//Copy the face texture data we loaded into the proper face texture of the internal Cube-Map
			D3DLOCKED_RECT faceLR;
			if(!SUCCEEDED(faceTex->LockRect(0, &faceLR, NULL, D3DLOCK_DISCARD )))
			{
				mPCubeTexture->Release();
				faceTex->Release();
				return false;
			}

			D3DLOCKED_RECT dstFaceLR;
			if(!SUCCEEDED(mPCubeTexture->LockRect((D3DCUBEMAP_FACES)faceIdx, 0, &dstFaceLR, NULL, NULL)))
			{
				releaseTexture();
				faceTex->Release();
				return false;
			}

			//Copy the mip0 data into the final texture
			memcpy(dstFaceLR.pBits, faceLR.pBits, faceLR.Pitch * faceDesc.Height);

			faceTex->UnlockRect(0);
			mPCubeTexture->UnlockRect((D3DCUBEMAP_FACES)faceIdx,0);

			faceTex->Release();

		}//End for(int faceIdx...

		return true;
	}
	//-----------------------------------------------------------------------------
	bool TextureConvert::loadMipTextures(atArray<string>& mipTexturePaths)
	{
		releaseTexture();
		int nMips = mipTexturePaths.GetCount();
		for(int mipIdx=0; mipIdx<nMips; mipIdx++)
		{
			//Load the mip texture
			LPDIRECT3DTEXTURE9 mipTex = NULL;
			if(!loadTextureInternal(mipTexturePaths[mipIdx].c_str(), mipTex))
			{
				Errorf("Error: Failed to load texture \"%s\"", mipTexturePaths[mipIdx].c_str());
				return false;
			}

			//Get the mip texture descriptor
			D3DSURFACE_DESC mipDesc;
			getDescription(mipDesc, mipTex, 0);

			if(mipIdx == 0)
			{
				//Create the internal texture that we will copy all of the mip level data into.
				D3DXCreateTexture(mPD3DDevice, mipDesc.Width, mipDesc.Height, nMips, D3DUSAGE_DYNAMIC, 
					D3DFMT_A32B32G32R32F, D3DPOOL_SYSTEMMEM , &mPTexture);
				if (mPTexture==NULL)
				{
					if(mipTex)
						mipTex->Release();
					return false;
				}
			}

			//Make sure the incoming mip is the correct size for the destination mip level
			D3DSURFACE_DESC dstMipDesc;
			getDescription(dstMipDesc, mPTexture, mipIdx);

			if( (dstMipDesc.Width != mipDesc.Width) ||
				(dstMipDesc.Height != mipDesc.Height) )
			{
				Errorf("Error: Incorrect dimensions for mip level (%d) mip texture \"%s\".  Input dimensions (%d x %d), should be (%d x %d)",
					mipIdx, mipTexturePaths[mipIdx].c_str(), mipDesc.Width, mipDesc.Height, dstMipDesc.Width, dstMipDesc.Height);
				mipTex->Release();
				releaseTexture();
				return false;
			}
		
			//Copy the mip data into the internal texture storage.
			D3DLOCKED_RECT mipLR;
			if(!SUCCEEDED(mipTex->LockRect(0, &mipLR, NULL, D3DLOCK_DISCARD )))
			{
				releaseTexture();
				mipTex->Release();
				return false;
			}
		
			D3DLOCKED_RECT dstTexLR;
			if(!SUCCEEDED(mPTexture->LockRect(mipIdx, &dstTexLR, NULL, NULL)))
			{
				releaseTexture();
				mipTex->Release();
				return false;
			}

			//Copy the mip0 data into the final texture
			memcpy(dstTexLR.pBits, mipLR.pBits, mipLR.Pitch * mipDesc.Height);

			mipTex->UnlockRect(0);
			mPTexture->UnlockRect(mipIdx);

			mipTex->Release();
		}
		return true;
	}
	
	//-----------------------------------------------------------------------------
	bool TextureConvert::loadAndMergeTextures(atArray<string>& mergeTexturePaths, const string& mergeFormat)
	{
		releaseTexture();

		//build merge mapping
		std::map<char, char>* mergeMapping = rage_new std::map<char, char>[mergeTexturePaths.GetCount()];
		bool alphaMapped = false;
		if (!buildMergeMapping(mergeFormat, mergeMapping, mergeTexturePaths.GetCount(), alphaMapped))
		{
			delete [] mergeMapping;
			return false;
		}

		int nMerge = mergeTexturePaths.GetCount();
		for(int mergeIdx=0; mergeIdx<nMerge; mergeIdx++)
		{
			//Load the merge texture
			LPDIRECT3DTEXTURE9 mergeTex = NULL;
			if(!loadTextureInternal(mergeTexturePaths[mergeIdx].c_str(), mergeTex))
			{
				Errorf("Error: Failed to load texture \"%s\"", mergeTexturePaths[mergeIdx].c_str());
				delete [] mergeMapping;
				return false;
			}

			//Get the merge texture descriptor
			D3DSURFACE_DESC mergeDesc;
			getDescription(mergeDesc, mergeTex, 0);

			if(mergeIdx == 0)
			{
				//Create the internal texture that we will copy all of the channel data into.
				D3DXCreateTexture(mPD3DDevice, mergeDesc.Width, mergeDesc.Height, 1, D3DUSAGE_DYNAMIC, 
					D3DFMT_A32B32G32R32F, D3DPOOL_SYSTEMMEM , &mPTexture);
				if (mPTexture==NULL)
				{
					if(mergeTex)
						mergeTex->Release();
					delete [] mergeMapping;
					return false;
				}
			}

			//Make sure the incoming merge is the correct size
			D3DSURFACE_DESC dstDesc;
			getDescription(dstDesc, mPTexture, 0);

			if( (dstDesc.Width != mergeDesc.Width) ||
				(dstDesc.Height != mergeDesc.Height) )
			{
				Errorf("Error: Incorrect dimensions for index (%d) merge texture \"%s\".  Input dimensions (%d x %d), should be (%d x %d)",
					mergeIdx, mergeTexturePaths[mergeIdx].c_str(), mergeDesc.Width, mergeDesc.Height, dstDesc.Width, dstDesc.Height);
				mergeTex->Release();
				releaseTexture();
				delete [] mergeMapping;
				return false;
			}

			//Copy the mip data into the internal texture storage.
			D3DLOCKED_RECT mergeLR;
			if(!SUCCEEDED(mergeTex->LockRect(0, &mergeLR, NULL, D3DLOCK_DISCARD )))
			{
				releaseTexture();
				mergeTex->Release();
				delete [] mergeMapping;
				return false;
			}

			D3DLOCKED_RECT dstTexLR;
			if(!SUCCEEDED(mPTexture->LockRect(0, &dstTexLR, NULL, NULL)))
			{
				releaseTexture();
				mergeTex->Release();
				delete [] mergeMapping;
				return false;
			}
			
			// D3DFMT_A32B32G32R32F is a float format
			float* floatDst = ((float*)dstTexLR.pBits);
			float* floatSrc = ((float*)mergeLR.pBits);
			int pixelPitch = mergeLR.Pitch / sizeof(float);
			int offset;
			for(unsigned int y=0;y<mergeDesc.Height; y++)
			{
				for(unsigned int x=0;x<mergeDesc.Width; x++)
				{
					offset = y*pixelPitch+x*RGBA_COLOR_CHANNELS_PER_PIXEL;
					std::map<char, char>::iterator iter;
					for( iter = mergeMapping[mergeIdx].begin(); iter != mergeMapping[mergeIdx].end(); iter++ ) {
						int srcOff = offset + getChannelIndex(iter->first);
						int dstOff = offset + getChannelIndex(iter->second);
						float temp =  floatSrc[srcOff];
						floatDst[dstOff] = temp;
					}

					if (!alphaMapped)
					{
						floatDst[offset + getChannelIndex('a')] = 1.0f;
					}
				}
			}

			mergeTex->UnlockRect(0);
			mPTexture->UnlockRect(0);

			mergeTex->Release();
		}
		delete [] mergeMapping;

		return true;
	}

	//-----------------------------------------------------------------------------
	bool TextureConvert::loadAlphaFromTextureFile(const char* filePath)
	{
		Assertf( (mPTexture != NULL), "TextureConvert::loadAlphaFromTextureFile, no base texture loaded to merge alpha channel into");

		//Load the alpha texture
		LPDIRECT3DTEXTURE9 alphaTex = NULL;
		if(!loadTextureInternal(filePath, alphaTex))
		{
			Errorf("Error: Failed to load texture \"%s\"", filePath);
			return false;
		}

		//Get the "base" texture descriptor
		D3DSURFACE_DESC baseDesc;
		getDescription(baseDesc, mPTexture, 0);

		//Get the alpha texture descriptor
		D3DSURFACE_DESC alphaDesc;
		getDescription(alphaDesc, alphaTex, 0);

		//Make sure the file sizes match
		if( (baseDesc.Width != alphaDesc.Width) ||
			(baseDesc.Height != alphaDesc.Height) )
		{
			Errorf("Error: Base texture dimensions ( %d x %d ) do not match alpha texture dimensions ( %d x %d ).",
				baseDesc.Width, baseDesc.Height, alphaDesc.Width, alphaDesc.Height);
			alphaTex->Release();
			return false;
		}

		unsigned int width = baseDesc.Width;
		unsigned int height = baseDesc.Height;

		//Copy the alpha channel from the alpha texture into the base texture alpha channel
		D3DLOCKED_RECT alphaLR;
		if(!SUCCEEDED(alphaTex->LockRect(0, &alphaLR, NULL, D3DLOCK_DISCARD )))
		{
			alphaTex->Release();
			return false;
		}

		D3DLOCKED_RECT baseLR;
		if(!SUCCEEDED(mPTexture->LockRect(0, &baseLR, NULL, D3DLOCK_DISCARD )))
		{
			alphaTex->Release();
			return false;
		}

		float* alphaTexBuff = (float*)alphaLR.pBits;
		float* baseTexBuff = (float*)baseLR.pBits;
		int pixelPitch = alphaLR.Pitch / sizeof(float);

		int offset;

		for(unsigned int y=0;y<height; y++)
		{
			for(unsigned int x=0;x<width; x++)
			{
				offset = y*pixelPitch+x*RGBA_COLOR_CHANNELS_PER_PIXEL+3;
				float alphaVal = alphaTexBuff[offset];
				baseTexBuff[offset] = alphaVal;
			}
		}

		alphaTex->UnlockRect(0);
		mPTexture->UnlockRect(0);
		
		return true;
	}
	//-----------------------------------------------------------------------------

	HRESULT RTC_D3DXCreateTextureFromFile( LPDIRECT3DDEVICE9 pDevice,
											LPCTSTR pSrcFile,
											LPDIRECT3DTEXTURE9 * ppTexture)
	{
#if __XENON
		fiStream *texStream = ASSET.Open(pSrcFile, NULL);
		if(!texStream)
			return D3DERR_INVALIDCALL;

		int texFileSize = texStream->Size();
		
		u8* texFileBuffer = rage_new u8[texFileSize];
		texStream->Read(texFileBuffer, texFileSize);
		texStream->Close();

		HRESULT retValue = D3DXCreateTextureFromFileInMemory( pDevice, texFileBuffer, texFileSize, ppTexture );
		delete [] texFileBuffer;
		return retValue;
#else
		
		HRESULT retValue = D3DXCreateTextureFromFile( pDevice, pSrcFile, ppTexture);
		if (retValue == D3DERR_NOTAVAILABLE)
		{
			Errorf("This device does not support the queried technique. %s", pSrcFile);
		}
		if (retValue == D3DERR_OUTOFVIDEOMEMORY)
		{
			Errorf("Direct3D does not have enough display memory to perform the operation. %s", pSrcFile);
		}
		if (retValue == D3DERR_INVALIDCALL)
		{
			Errorf("The method call is invalid. For example, a method's parameter may have an invalid value. %s", pSrcFile);
		}
		if (retValue == D3DXERR_INVALIDDATA)
		{
			Errorf("The data is invalid. %s", pSrcFile);
		}
		if (retValue == E_OUTOFMEMORY)
		{
			Errorf("Direct3D could not allocate sufficient memory to complete the call. %s", pSrcFile);
		}
		return retValue;
#endif
	}



	//-----------------------------------------------------------------------------
	bool TextureConvert::loadTextureInternal(LPCTSTR pSrcFile, LPDIRECT3DTEXTURE9 &pDstTexture)	
	{
		char acExpandedTexturePath[512];

#if __WIN32PC
		// Expand environment vars in filename
		ExpandEnvironmentStrings(pSrcFile, acExpandedTexturePath, 512);
#else
		// Can't expand the environment strings, so just copy the path over
		strcpy(acExpandedTexturePath,pSrcFile);
#endif

		// Get file extension
		const char* pcFileExtension = ASSET.FindExtensionInPath(acExpandedTexturePath);
		pcFileExtension++;

		//Check if the file exists
		if(!ASSET.Exists(acExpandedTexturePath, pcFileExtension))
		{
			Errorf("Error: File \"%s\" does not exist\n", acExpandedTexturePath);
			return false;
		}
		
		Displayf("Loading texture \"%s\"", acExpandedTexturePath);

		if(_stricmp(pcFileExtension, "map")==0)
		{
			// Map file, so handle that
			MapImage image(acExpandedTexturePath);
			if ((image.getWidth()>0) && (image.getHeight()>0))
			{
				D3DXCreateTexture(mPD3DDevice, image.getWidth(), image.getHeight(), 1, D3DUSAGE_DYNAMIC, 
					D3DFMT_A32B32G32R32F, D3DPOOL_SYSTEMMEM , &pDstTexture);
				if (pDstTexture==NULL)
				{
					return false;
				}

				D3DLOCKED_RECT lockedRectNew;
				if(!SUCCEEDED(mPTexture->LockRect(0, &lockedRectNew, NULL, D3DLOCK_DISCARD )))
				{
					return false;
				}  

				float* floatBuffer = (float*)lockedRectNew.pBits;
				image.copyBuffer(floatBuffer);

				pDstTexture->UnlockRect(0);
			}
			else
			{
				return false;
			}            
		}
		else if(_stricmp(pcFileExtension, "bmp")==0)
		{
			// Bmp file, DirectX can handle that directly
			HRESULT ok = RTC_D3DXCreateTextureFromFile(mPD3DDevice, acExpandedTexturePath, &pDstTexture);
			if(ok!=0)
				return false;
		}
		else if(_stricmp(pcFileExtension, "dds")==0)
		{
			// dds file, DirectX can handle that directly
			HRESULT ok = RTC_D3DXCreateTextureFromFile(mPD3DDevice, acExpandedTexturePath, &pDstTexture);
			if(ok!=0)
				return false;
		}
		else if(_stricmp(pcFileExtension, "hdr")==0)
		{
			// dds file, DirectX can handle that directly
			HRESULT ok = RTC_D3DXCreateTextureFromFile(mPD3DDevice, acExpandedTexturePath, &pDstTexture);
			if(ok!=0)
				return false;
		}
		else if(_stricmp(pcFileExtension, "dib")==0)
		{
			// dib file, DirectX can handle that directly
			HRESULT ok = RTC_D3DXCreateTextureFromFile(mPD3DDevice, acExpandedTexturePath, &pDstTexture);
			if(ok!=0)
				return false;
		}
		else if(_stricmp(pcFileExtension, "jpg")==0)
		{
			// jpg file, DirectX can handle that directly
			HRESULT ok = RTC_D3DXCreateTextureFromFile(mPD3DDevice, acExpandedTexturePath, &pDstTexture);
			if(ok!=0)
				return false;
		}
		else if(_stricmp(pcFileExtension, "png")==0)
		{
			// png file, DirectX can handle that directly
			HRESULT ok = RTC_D3DXCreateTextureFromFile(mPD3DDevice, acExpandedTexturePath, &pDstTexture);
			if(ok!=0)
				return false;
		}
		else if(_stricmp(pcFileExtension, "tga")==0)
		{
			// png file, DirectX can handle that directly
			HRESULT ok = RTC_D3DXCreateTextureFromFile(mPD3DDevice, acExpandedTexturePath, &pDstTexture);
			if(ok!=0)
				return false;
		}
		else if(_stricmp(pcFileExtension, "tif")==0)
		{
//#define DEBUG_TIFF_OUTPUT
			
			// It is a tiff, so use libtiff to load it
			TIFF* pobTifFile = NULL;

#if __XENON
			fiStream *pStream = ASSET.Open(acExpandedTexturePath, pcFileExtension);
			if(!pStream)
			{
				Errorf("Error: Failed to open file \"%s\"", acExpandedTexturePath);
				return false;
			}

			pobTifFile = TIFFClientOpen(acExpandedTexturePath, "r", (thandle_t)pStream,
										RAGE_TIFFReadProc, RAGE_TIFFWriteProc,
										RAGE_TIFFSeekProc, RAGE_TIFFCloseProc, RAGE_TIFFSizeProc,
										RAGE_TIFFMapFileProc, RAGE_TIFFUnmapFileProc);
#else
			pobTifFile = TIFFOpen(acExpandedTexturePath, "r");
#endif
			
			if(!pobTifFile)
			{
				// Errors occurred loading tiff
				Errorf("Error: Errors occurred loading using libTiff %s", acExpandedTexturePath);
				return false;
			}

			//Check to see if the input texture is an indexed color texture...
			short *clrMapR, *clrMapG, *clrMapB;
			if(TIFFGetField(pobTifFile, TIFFTAG_COLORMAP, &clrMapR, &clrMapG, &clrMapB) != 0)
			{
				Errorf("Error: Input texture is using indexed colors, this format is not supported.");
				return false;
			}

			// File opened of, lets get it into DirectX
			// Check that it is of a type that we support
			// A "sample" means channel, i.e. R, G, B and A
			unsigned short iBitsPerSample = 0;
			if(TIFFGetField(pobTifFile, TIFFTAG_BITSPERSAMPLE, &iBitsPerSample) == 0)
			{
				Errorf("Error: Either undefined or unsupported number of bits per sample (%d)\n", iBitsPerSample);
				return false;
			}

#ifdef DEBUG_TIFF_OUTPUT
			Displayf("iBitsPerSample = %d", iBitsPerSample);
#endif
			// No of channels, i.e. if iSamplesPerPixel == 3 then probably an RGB texture, if iSamplesPerPixel == 4 then probably an RGBA texture
			unsigned short iSamplesPerPixel = 0;
			if(TIFFGetField(pobTifFile, TIFFTAG_SAMPLESPERPIXEL, &iSamplesPerPixel) == 0)
			{
				Errorf("Error: Either undefined or unsupported number of samples per pixel (%d)\n", iSamplesPerPixel);
				return false;
			}

#ifdef DEBUG_TIFF_OUTPUT
			Displayf("iSamplesPerPixel = %d", iSamplesPerPixel);
#endif

			// Read in the possibly multiple strips
			int iStripSize = TIFFStripSize(pobTifFile);
			int iStripMax = TIFFNumberOfStrips(pobTifFile);
			int iImageOffset = 0;
			int iBufferSize = TIFFNumberOfStrips(pobTifFile) * iStripSize;
#ifdef DEBUG_TIFF_OUTPUT
			Displayf("iBufferSize = %d", iBufferSize);
#endif
			unsigned char* pcBuffer = rage_new unsigned char[iBufferSize];
			if(!pcBuffer)
			{
				Errorf("Error: Could not allocate enough memory for the uncompressed image\n");
				return false;
			}
#ifdef DEBUG_TIFF_OUTPUT
			Displayf("iStripSize = %d", iStripSize);
			Displayf("iStripMax = %d", iStripMax);
#endif
			for(int iStripCount = 0; iStripCount < iStripMax; iStripCount++)
			{
				int iReturnCode = TIFFReadEncodedStrip(pobTifFile, iStripCount, pcBuffer + iImageOffset, iStripSize);
				if(iReturnCode == -1)
				{
					Errorf("Error: Read error on input strip number %d\n", iStripCount);
					return false;
				}
				iImageOffset += iReturnCode;
			}

			// Do whatever it is we do with the buffer -- we dump it in hex
			int iImageWidth = 0;
			if(TIFFGetField(pobTifFile, TIFFTAG_IMAGEWIDTH, &iImageWidth) == 0)
			{
				Errorf("Error: Image does not define its width\n");
				return false;
			}
#ifdef DEBUG_TIFF_OUTPUT
			Displayf("iImageWidth = %d", iImageWidth);
#endif
			int iImageHeight = 0;
			if(TIFFGetField(pobTifFile, TIFFTAG_IMAGELENGTH, &iImageHeight) == 0)
			{
				Errorf("Error: Image does not define its height\n");
				return false;
			}
#ifdef DEBUG_TIFF_OUTPUT
			Displayf("iImageHeight = %d", iImageHeight);
#endif

			float fGamma = 1.0f;
			if( (TIFFGetField(pobTifFile, TIFFTAG_DCSGAMMA, fGamma) != 0) && (fGamma != 2.2f) )
			{
				Warningf("TIFF Image defines a gamma value of %f which differs from the expected value of 2.2\n", fGamma);	
			}
#ifdef DEBUG_TIFF_OUTPUT
			Displayf("fGamma = %f", fGamma);
#endif
			mInputGamma = fGamma;

			// Covert to DirectX
			D3DFORMAT eDirectXTextureFormat = D3DFMT_A32B32G32R32F;
			D3DXCreateTexture(mPD3DDevice, iImageWidth, iImageHeight, 1, D3DUSAGE_DYNAMIC, 
				eDirectXTextureFormat, D3DPOOL_SYSTEMMEM , &pDstTexture);
			if (pDstTexture==NULL)
			{
				return false;
			}

			D3DLOCKED_RECT lockedRectNew;
			if(!SUCCEEDED(pDstTexture->LockRect(0, &lockedRectNew, NULL, D3DLOCK_DISCARD )))
			{
				return false;
			}  

			float* pcTextureData = (float*)lockedRectNew.pBits;

			int pixelPitch = lockedRectNew.Pitch / sizeof(float);

			// We have the pixels in the directX image, only it was a RGBA and the directX expects ARGB
			// So we need to move the A channel to the start and push the other channels down one :(
			// Handle the no of bits per channel differently
			switch(iBitsPerSample)
			{
			case (8):
				{
					if(iSamplesPerPixel == 1)
					{
						//Handle greyscale input textures...
						const unsigned char* pcInputTextureData = pcBuffer;
						for(int y=0; y<iImageHeight; y++)
						{
							for(int x=0; x<iImageWidth; x++)
							{
								unsigned char channelVal = pcInputTextureData[ x + y * iImageWidth ];
								
								// Set blue
								pcTextureData[(RGBA_COLOR_CHANNELS_PER_PIXEL * x + y * pixelPitch) + 0] = (float)channelVal / 255.0f;
								// Set green
								pcTextureData[(RGBA_COLOR_CHANNELS_PER_PIXEL * x + y * pixelPitch) + 1] = (float)channelVal / 255.0f;
								// Set red
								pcTextureData[(RGBA_COLOR_CHANNELS_PER_PIXEL * x + y * pixelPitch) + 2] = (float)channelVal / 255.0f;
								// Set alpha
								pcTextureData[(RGBA_COLOR_CHANNELS_PER_PIXEL * x + y * pixelPitch) + 3] = 1.0f;
							}
						}
					}

					else
					{
						// 8 bits per channel
						const unsigned char* pcInputTextureData = pcBuffer;
						for(int y=0; y<iImageHeight; y++)
							for(int x=0; x<iImageWidth; x++)
							{
								unsigned char cRChannelValue = pcInputTextureData[(iSamplesPerPixel * (x + y * iImageWidth)) + 0];
								unsigned char cGChannelValue = pcInputTextureData[(iSamplesPerPixel * (x + y * iImageWidth)) + 1];
								unsigned char cBChannelValue = pcInputTextureData[(iSamplesPerPixel * (x + y * iImageWidth)) + 2];
								unsigned char cAChannelValue = 255;
								if(iSamplesPerPixel > 3) cAChannelValue = pcInputTextureData[(iSamplesPerPixel * (x + y * iImageWidth)) + 3];

								// printf("[%d %d %d %d]", cRChannelValue, cGChannelValue, cBChannelValue, cAChannelValue);

								// Set blue
								pcTextureData[(RGBA_COLOR_CHANNELS_PER_PIXEL * x + y * pixelPitch) + 0] = (float)cRChannelValue / 255.0f;
								// Set green
								pcTextureData[(RGBA_COLOR_CHANNELS_PER_PIXEL * x + y * pixelPitch) + 1] = (float)cGChannelValue / 255.0f;
								// Set red
								pcTextureData[(RGBA_COLOR_CHANNELS_PER_PIXEL * x + y * pixelPitch) + 2] = (float)cBChannelValue / 255.0f;
								// Set alpha
								pcTextureData[(RGBA_COLOR_CHANNELS_PER_PIXEL * x + y * pixelPitch) + 3] = (float)cAChannelValue / 255.0f;
							}
					}
					break;
				}
			case (16):
				{
					// 16 bits per channel
					const unsigned short* psInputTextureData = (unsigned short*)pcBuffer;
					for(int y=0; y<iImageHeight; y++)
						for(int x=0; x<iImageWidth; x++)
						{
							unsigned short sRChannelValue = psInputTextureData[(iSamplesPerPixel * (x + y * iImageWidth)) + 0];
							unsigned short sGChannelValue = psInputTextureData[(iSamplesPerPixel * (x + y * iImageWidth)) + 1];
							unsigned short sBChannelValue = psInputTextureData[(iSamplesPerPixel * (x + y * iImageWidth)) + 2];
							unsigned short sAChannelValue = 65535;
							if(iSamplesPerPixel > 3) sAChannelValue = psInputTextureData[(iSamplesPerPixel * (x + y * iImageWidth)) + 3];

							// printf("[%d %d %d %d]", sRChannelValue, sGChannelValue, sBChannelValue, sAChannelValue);

							// Set blue
							pcTextureData[(RGBA_COLOR_CHANNELS_PER_PIXEL * x + y * pixelPitch) + 0] = (float)sRChannelValue / 65535.0f;
							// Set green
							pcTextureData[(RGBA_COLOR_CHANNELS_PER_PIXEL * x + y * pixelPitch) + 1] = (float)sGChannelValue / 65535.0f;
							// Set red
							pcTextureData[(RGBA_COLOR_CHANNELS_PER_PIXEL * x + y * pixelPitch) + 2] = (float)sBChannelValue / 65535.0f;
							// Set alpha
							pcTextureData[(RGBA_COLOR_CHANNELS_PER_PIXEL * x + y * pixelPitch) + 3] = (float)sAChannelValue / 65535.0f;
						}
					break;
				}
			default:
				{
					// Badness
					Errorf("Error: Unsupported no of bits per channel (%d) used.\n", iBitsPerSample);
					return false;
				}

			}

			// Clean up the DirectX
			pDstTexture->UnlockRect(0);
		
			// Close the file
			TIFFClose(pobTifFile);
			delete pcBuffer;
		}
		else
		{
			// Ok, it isn't a .map file or any of the files that DirectX can handle directly,
			// so invoke the devil to try and load it
			ilInit();
			ilEnable(IL_ORIGIN_SET);
			ilOriginFunc(IL_ORIGIN_UPPER_LEFT);
			if (ilLoadImage(ILstring(acExpandedTexturePath))) 
			{
				// Make sure there is an alpha channel
				ilConvertImage(IL_RGBA, IL_BYTE);
				// Image loaded through DevIL, so process
				int iImageWidth = ilGetInteger(IL_IMAGE_WIDTH);
				int iImageHeight = ilGetInteger(IL_IMAGE_HEIGHT);
//				int iBytesPerPixel = ilGetInteger(IL_IMAGE_BYTES_PER_PIXEL);
				int iImageFormat = ilGetInteger(IL_IMAGE_FORMAT);
				// printf("ImageWdth = %d ImageHeght = %d BytesPerPxel = %d ImageFormat = %d\n", iImageWidth, iImageHeight, iBytesPerPixel, iImageFormat);

				// Take a copy of the data, so the file can be dumped
				//char* pcTextureData = rage_new char[iImageWidth * iImageHeight * iImageDepth];
				//ilCopyPixels(0, 0, 0, iImageWidth, iImageHeight, iImageDepth, iImageFormat, IL_BYTE, pcTextureData);

				// Covert to DirectX
				D3DFORMAT eDirectXTextureFormat = D3DFMT_A8R8G8B8;
				D3DXCreateTexture(mPD3DDevice, iImageWidth, iImageHeight, 1, D3DUSAGE_DYNAMIC, 
					eDirectXTextureFormat, D3DPOOL_SYSTEMMEM , &pDstTexture);
				if (mPTexture==NULL)
				{
					return false;
				}

				D3DLOCKED_RECT lockedRectNew;
				if(!SUCCEEDED(pDstTexture->LockRect(0, &lockedRectNew, NULL, D3DLOCK_DISCARD )))
				{
					return false;
				}  

				unsigned char* pcTextureData = (unsigned char*)lockedRectNew.pBits;
				ilCopyPixels(0, 0, 0, iImageWidth, iImageHeight, 1, iImageFormat, IL_BYTE, pcTextureData);
				int pixelPitch = lockedRectNew.Pitch / sizeof(unsigned char);

				// We have the pixels in the directX image, only it was a RGBA and the directX expects ARGB
				// So we need to move the A channel to the start and push the other channels down one :(
				for(int y=0; y<iImageHeight; y++)
					for(int x=0; x<iImageWidth; x++)
					{
						unsigned char cRChannelValue = pcTextureData[(RGBA_COLOR_CHANNELS_PER_PIXEL * x + y * pixelPitch) + 0];
						unsigned char cGChannelValue = pcTextureData[(RGBA_COLOR_CHANNELS_PER_PIXEL * x + y * pixelPitch) + 1];
						unsigned char cBChannelValue = pcTextureData[(RGBA_COLOR_CHANNELS_PER_PIXEL * x + y * pixelPitch) + 2];
						unsigned char cAChannelValue = pcTextureData[(RGBA_COLOR_CHANNELS_PER_PIXEL * x + y * pixelPitch) + 3];

						// printf("[%d %d %d %d]", cRChannelValue, cGChannelValue, cBChannelValue, cAChannelValue);

						pcTextureData[(RGBA_COLOR_CHANNELS_PER_PIXEL * x + y * pixelPitch) + 0] = (unsigned char)cBChannelValue;
						pcTextureData[(RGBA_COLOR_CHANNELS_PER_PIXEL * x + y * pixelPitch) + 1] = (unsigned char)cGChannelValue;
						pcTextureData[(RGBA_COLOR_CHANNELS_PER_PIXEL * x + y * pixelPitch) + 2] = (unsigned char)cRChannelValue;
						pcTextureData[(RGBA_COLOR_CHANNELS_PER_PIXEL * x + y * pixelPitch) + 3] = (unsigned char)cAChannelValue;
					}

				pDstTexture->UnlockRect(0);
			}
			else
			{
				// Errors occurred loading the file :(
				Errorf("Error: Errors occurred loading using DevIL %s : %s", acExpandedTexturePath, iluErrorString(ilGetError()));
				return false;
			}
		}

		D3DSURFACE_DESC desc;
		getDescription(desc, pDstTexture);

		Displayf("Texture dimensions: %d x %d", desc.Width, desc.Height);

		//Make sure everything loaded is stored as 32 bit float image internally
		if (desc.Format!= D3DFMT_A32B32G32R32F)
		{
			TCConvertOptions tcOps;
			tcOps.Format = D3DFMT_A32B32G32R32F;
			tcOps.Width = desc.Width;
			tcOps.Height = desc.Height;

			/*convertToFormat(tcOps, &pDstTexture);

			getDescription(desc, pDstTexture);
			if (desc.Format != D3DFMT_A32B32G32R32F)
			{
				printf("Warning, internal up-conversion to D3DFMT_A32B32G32R32F failed."); 
				return false;
			}*/

			LPDIRECT3DTEXTURE9 pDstTexture32F = NULL;
			if(!convertToFormatInternalD3DX(pDstTexture,tcOps,&pDstTexture32F))
			{
				while( pDstTexture->Release())
					;
				return false;
			}
			else
			{
				while( pDstTexture->Release())
					;
				pDstTexture = pDstTexture32F;
			}

		}

		return true;
	}
	//-----------------------------------------------------------------------------
	bool TextureConvert::pushHdrDataIntoDDSHeader(LPCSTR pDestFile, TCConvertOptions& tcOpt)
	{
		//Push the HDR Exponent, Offset, and gamma values into the DDS header
		float gamma;
		if(tcOpt.SetSRGBGammaInDDSHeader)
			gamma = 2.2f;
		else
			gamma = 1.0f;

		FILE *pFile = NULL;
		fopen_s(&pFile, pDestFile, "rb+");
		if(pFile != NULL)
		{
			DDSURFACEDESC2 dummy;
			int offset = (int)&dummy.fGamma - (int)&dummy + 4;
			fseek(pFile, offset, SEEK_SET);

			//Write the gamma value into the DDS header
			fwrite(&gamma, sizeof(float), 1, pFile);

			if(m_writeLgExpOff || tcOpt.SetSRGBGammaInDDSHeader)
			{
				//Write out the Linear space exponent and offset values
				fwrite(&tcOpt.HDRExpR[TCConvertOptions::LINEAR], sizeof(float), 1, pFile);
				fwrite(&tcOpt.HDRExpG[TCConvertOptions::LINEAR], sizeof(float), 1, pFile);
				fwrite(&tcOpt.HDRExpB[TCConvertOptions::LINEAR], sizeof(float), 1, pFile);
				fwrite(&tcOpt.HDROffR[TCConvertOptions::LINEAR], sizeof(float), 1, pFile);
				fwrite(&tcOpt.HDROffG[TCConvertOptions::LINEAR], sizeof(float), 1, pFile);
				fwrite(&tcOpt.HDROffB[TCConvertOptions::LINEAR], sizeof(float), 1, pFile);
			}
			else
			{
				//Write out the sRGB space exponent and offset values
				fwrite(&tcOpt.HDRExpR[TCConvertOptions::SRGB], sizeof(float), 1, pFile);
				fwrite(&tcOpt.HDRExpG[TCConvertOptions::SRGB], sizeof(float), 1, pFile);
				fwrite(&tcOpt.HDRExpB[TCConvertOptions::SRGB], sizeof(float), 1, pFile);
				fwrite(&tcOpt.HDROffR[TCConvertOptions::SRGB], sizeof(float), 1, pFile);
				fwrite(&tcOpt.HDROffG[TCConvertOptions::SRGB], sizeof(float), 1, pFile);
				fwrite(&tcOpt.HDROffB[TCConvertOptions::SRGB], sizeof(float), 1, pFile);
			}

			fseek(pFile,0, SEEK_END);
			fclose(pFile);

			return true;
		}
		return false;
	}
	bool TextureConvert::saveVolumeTexture(LPCSTR pDestFile, TCConvertOptions& tcOpt)
	{
		Assert(mPVolumeTexture);
		HRESULT ok = D3DXSaveTextureToFile( pDestFile, D3DXIFF_DDS, mPVolumeTexture, NULL);
		if(ok != 0)
		{
			Errorf("Error: D3DXSaveTextureToFile failed to write cube-map '%s'", pDestFile);
			return false;
		}

		bool bResult = pushHdrDataIntoDDSHeader(pDestFile, tcOpt);
		if(!bResult)
		{
			Errorf("Error: Failed to push HDR data to the cube-map '%s'", pDestFile);
			return false;
		}

		return true;
	}
	//-----------------------------------------------------------------------------
	bool TextureConvert::saveCubeTexture(LPCSTR pDestFile, TCConvertOptions& tcOpt)
	{
		Assert(mPCubeTexture);
		HRESULT ok = D3DXSaveTextureToFile( pDestFile, D3DXIFF_DDS, mPCubeTexture, NULL);
		if(ok != 0)
		{
			Errorf("Error: D3DXSaveTextureToFile failed to write cube-map '%s'", pDestFile);
			return false;
		}
		
		bool bResult = pushHdrDataIntoDDSHeader(pDestFile, tcOpt);
		if(!bResult)
		{
			Errorf("Error: Failed to push HDR data to the cube-map '%s'", pDestFile);
			return false;
		}
		
		return true;
	}
	//-----------------------------------------------------------------------------
	void SaveUint(unsigned int i, FILE* file)
	{
		fwrite(&i, sizeof(ILuint), 1, file);
	}

	//-----------------------------------------------------------------------------
	bool TextureConvert::saveTextureWithoutCompression(LPCTSTR pDestFile, TCConvertOptions& tcOpt)
	{
		HRESULT ok = D3DXSaveTextureToFile( pDestFile, D3DXIFF_DDS, mPTexture, NULL);
		if(ok != 0)
		{
			Errorf("Error: D3DXSaveTextureToFile failed to write texture '%s'", pDestFile);
			return false;
		}

		bool bResult = pushHdrDataIntoDDSHeader(pDestFile, tcOpt);
		if(!bResult)
		{
			Errorf("Error: Failed to push HDR data into the texture '%s'", pDestFile);
			return false;
		}

		return true;
	}

#if USE_COMPRESSINATOR
	bool TextureConvert::saveTexture(LPCTSTR pDestFile, TCConvertOptions& tcOpt)
	{
		if( (tcOpt.Format != D3DFMT_DXT1) && (tcOpt.Format != D3DFMT_DXT5) )
		{
			HRESULT ok = D3DXSaveTextureToFile( pDestFile, D3DXIFF_DDS, mPTexture, NULL);
			if(ok != 0)
			{
				Errorf("Error: D3DXSaveTextureToFile failed to write texture '%s'", pDestFile);
				return false;
			}
			
			bool bResult = pushHdrDataIntoDDSHeader(pDestFile, tcOpt);
			if(!bResult)
			{
				Errorf("Error: Failed to push HDR data into the texture '%s'", pDestFile);
				return false;
			}

			return true;
		}
		else
		{
			rage::TCConvertOptions tmpTcOpts(tcOpt);

			FILE* outfile = fopen(pDestFile, "wb");
			if(outfile)
			{
				
				ATI_TC_Texture srcTexture;
				ATI_TC_Texture destTexture;
				ATI_TC_ERROR err;

				unsigned prevMask;

				unsigned int Count = 0;

				AlphaCheckResult alphaCheckRes = RTC_HASONEBITALPHA;
				if(!tmpTcOpts.ForceOneBitAlpha)
				{
					alphaCheckRes = hasAlpha();
				}
				Displayf("TextureAlpha : %d", ( (alphaCheckRes == RTC_HASMULTIBITALPHA) || (alphaCheckRes == RTC_HASONEBITALPHA) ) ? 1 : 0);

				ATI_TC_CompressOptions compOptions;
				memset(&compOptions, 0, sizeof(compOptions));
				compOptions.dwSize = sizeof(compOptions);
				compOptions.bDXT1UseAlpha = TRUE;
				compOptions.nAlphaThreshold = 127;
			
				if(tcOpt.Format == D3DFMT_DXT1 && (alphaCheckRes == RTC_HASMULTIBITALPHA) )
				{
					Displayf("Output texture format was specified as DXT1, but intput texture has multi-bit alpha, output format will be changed to DXT5\n");
				}
				else if(tcOpt.Format == D3DFMT_DXT5 && ( (alphaCheckRes == RTC_NOALPHA) || (alphaCheckRes == RTC_HASONEBITALPHA) ) )
				{
					Displayf("Output texture format was specified as DXT5, but input texture does not have alpha, or has one-bit alpha, output format will be changed to DXT1\n");
				}

				D3DSURFACE_DESC desc;
				D3DLOCKED_RECT rect;

				getDescription(desc);
				mPTexture->LockRect(0,&rect,0,0);
				mPTexture->UnlockRect(0);
				
				mPTexture->GetLevelDesc(0, &desc);

				ILuint i, FourCC=0, Flags1 = 0, Flags2 = 0, ddsCaps1 = 0, LinearSize, Bits=0;
				ILuint AlphaMask=0, RedMask=0, GreenMask=0, BlueMask=0;
				ILuint mips = mPTexture->GetLevelCount() > (DWORD)tcOpt.MipLevels && tcOpt.MipLevels ? tcOpt.MipLevels : mPTexture->GetLevelCount();
				
				Flags1 |= DDSD_WIDTH | DDSD_HEIGHT | DDSD_CAPS | DDSD_PIXELFORMAT;

				// TODO: Do we want to support no compression???

				//if (DXTCFormat != IL_DXT_NO_COMP) 
				//	Flags1 |= DDS_LINEARSIZE;			// we always want to compress
				if(mips > 1)
					Flags1 |= DDSD_MIPMAPCOUNT;			
				
				
				//Flags2 |= (DXTCFormat==IL_DXT_NO_COMP) ? DDS_PFRGB | DDS_ALPHAPIXELS : DDS_FOURCC;
				Flags2 |= DDPF_FOURCC;

				if( (alphaCheckRes == RTC_NOALPHA) || (alphaCheckRes == RTC_HASONEBITALPHA) )
				{
					FourCC = RTC_MAKEFOURCC('D','X','T','1');
				}
				else
				{
					FourCC = RTC_MAKEFOURCC('D','X','T','5');
				}
				fwrite("DDS ", 1, 4, outfile);
				SaveUint(124, outfile);
				SaveUint(Flags1, outfile);
			
				SaveUint(getHeight(), outfile);
				SaveUint(getWidth(), outfile);
				
				if ( (alphaCheckRes == RTC_NOALPHA) || (alphaCheckRes == RTC_HASONEBITALPHA) ) 
				{
					LinearSize = desc.Width * desc.Height / 16 * 8;
				}
				else 
				{
					LinearSize = desc.Width * desc.Height / 16 * 16;
				}
				SaveUint(LinearSize, outfile);
				SaveUint(0, outfile);
				SaveUint(mips > 1 ? mips : 0, outfile);  // MipMapCount
				SaveUint(0, outfile);					// AlphaBitDepth

				for (i = 0; i < 3; i++)
					SaveUint(0, outfile);		// Not used

				float gamma;
				if(tcOpt.SetSRGBGammaInDDSHeader)
					gamma = 2.2f;
				else
					gamma = 1.0f;

				fwrite(&gamma, sizeof(float), 1, outfile);
	
				if(m_writeLgExpOff || tcOpt.SetSRGBGammaInDDSHeader)
				{
					//Write out the Linear space exponent and offset values into the "unused" portion
					//of the DDS file header
					fwrite(&tcOpt.HDRExpR[TCConvertOptions::LINEAR_EXPOSED], sizeof(float), 1, outfile);
					fwrite(&tcOpt.HDRExpG[TCConvertOptions::LINEAR_EXPOSED], sizeof(float), 1, outfile);
					fwrite(&tcOpt.HDRExpB[TCConvertOptions::LINEAR_EXPOSED], sizeof(float), 1, outfile);
					fwrite(&tcOpt.HDROffR[TCConvertOptions::LINEAR_EXPOSED], sizeof(float), 1, outfile);
					fwrite(&tcOpt.HDROffG[TCConvertOptions::LINEAR_EXPOSED], sizeof(float), 1, outfile);
					fwrite(&tcOpt.HDROffB[TCConvertOptions::LINEAR_EXPOSED], sizeof(float), 1, outfile);
				}
				else
				{
					//Write out the sRGB space exponent and offset values into the "unused" portion
					//of the DDS file header
					fwrite(&tcOpt.HDRExpR[TCConvertOptions::SRGB_EXPOSED], sizeof(float), 1, outfile);
					fwrite(&tcOpt.HDRExpG[TCConvertOptions::SRGB_EXPOSED], sizeof(float), 1, outfile);
					fwrite(&tcOpt.HDRExpB[TCConvertOptions::SRGB_EXPOSED], sizeof(float), 1, outfile);
					fwrite(&tcOpt.HDROffR[TCConvertOptions::SRGB_EXPOSED], sizeof(float), 1, outfile);
					fwrite(&tcOpt.HDROffG[TCConvertOptions::SRGB_EXPOSED], sizeof(float), 1, outfile);
					fwrite(&tcOpt.HDROffB[TCConvertOptions::SRGB_EXPOSED], sizeof(float), 1, outfile);
				}

				SaveUint(32, outfile);			// Size2
				SaveUint(Flags2, outfile);		// Flags2
				SaveUint(FourCC, outfile);		// FourCC
				SaveUint(Bits, outfile);		// RGBBitCount
				SaveUint(RedMask, outfile);		// RBitMask
				SaveUint(GreenMask, outfile);	// GBitMask
				SaveUint(BlueMask, outfile);	// BBitMask
				SaveUint(AlphaMask, outfile);	// RGBAlphaBitMask
				ddsCaps1 |= DDSCAPS_TEXTURE;
				
				//changed 20040516: set mipmap flag on mipmap images
				//(cubemaps and non-compressed .dds files still not supported,
				//though)
				if (mips > 1)
					ddsCaps1 |= DDSCAPS_MIPMAP | DDSCAPS_COMPLEX;

				SaveUint(ddsCaps1, outfile);	// ddsCaps1
				SaveUint(0, outfile);			// ddsCaps2
				SaveUint(0, outfile);			// ddsCaps3
				SaveUint(0, outfile);			// ddsCaps4
				SaveUint(0, outfile);			// TextureStage

				srcTexture.dwSize = sizeof(srcTexture);

				srcTexture.format = ATI_TC_FORMAT_ARGB_32F;
		
				memset(&destTexture,0,sizeof(destTexture));
				destTexture.dwSize = sizeof(destTexture);
				destTexture.dwPitch = 0;

				int level=0;
				while(mips--)
				{
					LPDIRECT3DSURFACE9 pMipSurface;
					mPTexture->GetSurfaceLevel( level, &pMipSurface );
					D3DSURFACE_DESC mipDesc;
					pMipSurface->GetDesc(&mipDesc);
					mPTexture->Release();

					destTexture.dwWidth = mipDesc.Width;
					destTexture.dwHeight = mipDesc.Height;
					srcTexture.dwWidth = mipDesc.Width;
					srcTexture.dwHeight = mipDesc.Height;

					mPTexture->LockRect(level,&rect,0,0);
					srcTexture.pData = (ATI_TC_BYTE*) rect.pBits;
					srcTexture.dwPitch = rect.Pitch;
					srcTexture.dwDataSize = ATI_TC_CalculateBufferSize(&srcTexture);
					if( (alphaCheckRes == RTC_NOALPHA) || (alphaCheckRes == RTC_HASONEBITALPHA) )
					{
						destTexture.format = ATI_TC_FORMAT_DXT1;
						destTexture.dwDataSize = ATI_TC_CalculateBufferSize(&destTexture);
						destTexture.pData = rage_new ATI_TC_BYTE[Count = destTexture.dwDataSize];

						if(destTexture.dwWidth >= 4 && destTexture.dwHeight >= 4)
						{
							prevMask = _controlfp((unsigned int)~0,(unsigned int) _MCW_EM);
							++RAGE_LOG_DISABLE;
							err = ATI_TC_ConvertTexture(&srcTexture, &destTexture, &compOptions, NULL, NULL, NULL);
							--RAGE_LOG_DISABLE;
							_controlfp(prevMask, _MCW_EM);
							if (err)
								printf("ATI_TC_ConvertTexture returned %d\n",err);
						}
						else
						{
							//Below 4x4 the compression seems to be undefined, so just fill it with black
							memset(destTexture.pData, 0x00, destTexture.dwDataSize);
						}
					}
					else
					{
						destTexture.format = ATI_TC_FORMAT_DXT5;
						destTexture.dwDataSize = ATI_TC_CalculateBufferSize(&destTexture);
						destTexture.pData = rage_new ATI_TC_BYTE[Count = destTexture.dwDataSize];

						if(destTexture.dwWidth >= 4 && destTexture.dwHeight >= 4)
						{
							prevMask = _controlfp((unsigned int)~0,(unsigned int) _MCW_EM);
							++RAGE_LOG_DISABLE;
							err = ATI_TC_ConvertTexture(&srcTexture, &destTexture, NULL, NULL, NULL, NULL);
							--RAGE_LOG_DISABLE;
							_controlfp(prevMask, _MCW_EM);
							if (err)
								printf("ATI_TC_ConvertTexture returned %d\n",err);
						}
						else
						{
							//Below 4x4 the compression seems to be undefined, so just fill it with black
							memset(destTexture.pData, 0x00, destTexture.dwDataSize);
						}
					}
					fwrite(destTexture.pData, 1, Count, outfile);
					mPTexture->UnlockRect(level++);
					delete destTexture.pData;
				}//End while(mips--)

				fclose(outfile);
			}
			return (true);
		}
	}
#else //USE_COMPRESSINATOR
	bool TextureConvert::saveTexture(LPCTSTR pDestFile, TCConvertOptions& tcOpt)
	{
		//Save the texture
		HRESULT ok = D3DXSaveTextureToFile( pDestFile, D3DXIFF_DDS, mPTexture, NULL);
		if(ok != 0)
		{
			Errorf("Error: D3DXSaveTextureToFile failed to write texture '%s'", pDestFile);
			return false;
		}
		
		bool bResult = pushHdrDataIntoDDSHeader(pDestFile, tcOpt);
		if(!bResult)
		{
			Errorf("Error: Failed to push HDR data into the texture '%s'", pDestFile);
			return false;
		}
		
		return true;
	}
#endif //USE_COMPRESSINATOR
	//-----------------------------------------------------------------------------
	void TextureConvert::releaseTexture()
	{
		if( mPTexture != NULL)
		{
			while(mPTexture->Release())
				;
			
			mPTexture = NULL;
		}
	}
	//-----------------------------------------------------------------------------
	TextureConvert::AlphaCheckResult TextureConvert::hasAlpha(bool checkForMinMultiByteAlphaPercent, float minMultiByteAlphaPercent)
	{
		D3DSURFACE_DESC desc;
		getDescription(desc);
		Assert( (desc.Format == D3DFMT_A32B32G32R32F) || (desc.Format == D3DFMT_A8R8G8B8) );

		D3DLOCKED_RECT lockedRectOriginal;
		if(!SUCCEEDED(mPTexture->LockRect(0, &lockedRectOriginal, NULL, D3DLOCK_READONLY )))
		{
			return RTC_NOALPHA;
		}  

		bool bFoundAlphaOne = false;
		bool bFoundAlphaZero = false;
		bool bFoundAlphaOther = false;
		float pixCount = 0.0f;
		float percentMultiAlpha = 0.0f;

		if(desc.Format == D3DFMT_A32B32G32R32F)
		{
			float* floatBuffer = (float*)lockedRectOriginal.pBits;
			int pixelPitch = lockedRectOriginal.Pitch / sizeof(float);
			for(unsigned int y=0;y<desc.Height; y++)
			{
				for(unsigned int x=0;x<desc.Width; x++)
				{
					float alphaf  = floatBuffer[y*pixelPitch+x*RGBA_COLOR_CHANNELS_PER_PIXEL+3];

					if( alphaf >= 0.99999 )
						bFoundAlphaOne = true;
					else if( alphaf <= .000001 )
						bFoundAlphaZero = true;
					else
					{
						bFoundAlphaOther = true;
						if (checkForMinMultiByteAlphaPercent)
						{
							pixCount+=1.0f;
							percentMultiAlpha = pixCount/(desc.Width*desc.Height);
							if (percentMultiAlpha > minMultiByteAlphaPercent)
							{
								break; //Early out 
							}
						}
						else
						{
							break; //Early out 
						}
					}
				}
			}
		}
		else if(desc.Format == D3DFMT_A8R8G8B8)
		{
			unsigned char* charBuffer = (unsigned char*)lockedRectOriginal.pBits;
			int pixelPitch = lockedRectOriginal.Pitch / sizeof(unsigned char);
			for(unsigned int y=0;y<desc.Height; y++)
			{
				for(unsigned int x=0;x<desc.Width; x++)
				{
					char alpha  = charBuffer[y*pixelPitch+x*RGBA_COLOR_CHANNELS_PER_PIXEL+3];
					if( alpha == 255 )
						bFoundAlphaOne = true;
					else if(alpha == 0 )
						bFoundAlphaZero = true;
					else
					{
						bFoundAlphaOther = true;
						if (checkForMinMultiByteAlphaPercent)
						{
							pixCount+=1.0f;
							percentMultiAlpha = pixCount/(desc.Width*desc.Height);
							if (percentMultiAlpha > minMultiByteAlphaPercent)
							{
								break; //Early out 
							}
						}
						else
						{
							break; //Early out 
						}
					}
				}
			}
		}

		mPTexture->UnlockRect(0);

		if( bFoundAlphaOther )
		{
			if (checkForMinMultiByteAlphaPercent && (percentMultiAlpha <= minMultiByteAlphaPercent))
			{
				Warningf("ARTPROBLEM: Texture has less than %f percent of its pixels considered multibyte alpha. This is probably a waste. (Percent: %f)", minMultiByteAlphaPercent, percentMultiAlpha);
			}
			return RTC_HASMULTIBITALPHA;
		}
		else if( bFoundAlphaOne && bFoundAlphaZero )
			return RTC_HASONEBITALPHA;
		else
			return RTC_NOALPHA;
	}
	//-----------------------------------------------------------------------------
	bool TextureConvert::convertTo32Bit()
	{
		D3DSURFACE_DESC desc;
		getDescription(desc);
		if (desc.Format==D3DFMT_A32B32G32R32F)
		{
			return true;
		}
	
		LPDIRECT3DTEXTURE9 newTexture=NULL;
		D3DXCreateTexture(mPD3DDevice, desc.Width, desc.Height, 1, D3DUSAGE_DYNAMIC, 
			D3DFMT_A32B32G32R32F, D3DPOOL_SYSTEMMEM , &newTexture);
		if (newTexture==NULL)
		{
			return false;
		}

		LPDIRECT3DSURFACE9 psurfSrc = NULL;
		LPDIRECT3DSURFACE9 psurfDest = NULL;
		int iLevel = 0;
		mPTexture->GetSurfaceLevel(iLevel, &psurfSrc);
		newTexture->GetSurfaceLevel(iLevel, &psurfDest);
		D3DXLoadSurfaceFromSurface(psurfDest, NULL, NULL, 
			psurfSrc, NULL, NULL, D3DX_DEFAULT, 0);
		psurfSrc->Release();
		psurfDest->Release();

		releaseTexture();
		mPTexture = newTexture;

		return true;
	}
	//-----------------------------------------------------------------------------
	bool TextureConvert::convertABGRF32ToRGBE()
	{
		if (mPTexture==NULL)
		{
			return false;
		}

		D3DSURFACE_DESC desc;
		getDescription(desc);

		if (desc.Format!=D3DFMT_A32B32G32R32F)
		{
			return false;
		}

		D3DLOCKED_RECT lockedRectOriginal;
		if(!SUCCEEDED(mPTexture->LockRect(0, &lockedRectOriginal, NULL, D3DLOCK_READONLY )))
		{
			return false;
		}    

		LPDIRECT3DTEXTURE9 newTexture=NULL;
		D3DXCreateTexture(mPD3DDevice, desc.Width, desc.Height, 1, D3DUSAGE_DYNAMIC, 
			D3DFMT_A8R8G8B8, D3DPOOL_SYSTEMMEM , &newTexture);
		if (newTexture==NULL)
		{
			return false;
		}

		D3DLOCKED_RECT lockedRectNew;
		if(!SUCCEEDED(newTexture->LockRect(0, &lockedRectNew, NULL, D3DLOCK_DISCARD )))
		{
			return false;
		}  

		float* floatBuffer = (float*)lockedRectOriginal.pBits;
		DWORD* dwordBuffer = (DWORD*)lockedRectNew.pBits;
		int pixelPitch = lockedRectOriginal.Pitch / sizeof(float);
		for(unsigned int y=0;y<desc.Height; y++)
		{
			for(unsigned int x=0;x<desc.Width; x++)
			{
				float bluef= floatBuffer[y*pixelPitch+x*RGBA_COLOR_CHANNELS_PER_PIXEL+0];
				float greenf = floatBuffer[y*pixelPitch+x*RGBA_COLOR_CHANNELS_PER_PIXEL+1];
				float redf  = floatBuffer[y*pixelPitch+x*RGBA_COLOR_CHANNELS_PER_PIXEL+2];
				float alphaf  = floatBuffer[y*pixelPitch+x*RGBA_COLOR_CHANNELS_PER_PIXEL+3];

				int red=0;
				int green=0;
				int blue=0;
				int alpha=0;
				if (alphaf>0)
				{
					calculateRGBE(redf, greenf, bluef, red, green, blue, alpha);
				}
				dwordBuffer[(y*desc.Width+x)] = ((alpha<<24L)+(red<<16L)+(green<<8L)+(blue));
			}
		}

		mPTexture->UnlockRect(0);
		newTexture->UnlockRect(0);

		releaseTexture();

		mPTexture = newTexture;

		return true;
	}
	//-----------------------------------------------------------------------------
	
	//Temporarily depricated until it can be updated to eliminate the dependencies on the DXT5Compress.cpp/h files

	//bool TextureConvert::convertABGRF32ToDXT5RGBE()
	//{
	//	if (mPTexture==NULL)
	//	{
	//		return false;
	//	}

	//	D3DSURFACE_DESC desc;
	//	getDescription(desc);

	//	if (desc.Format!=D3DFMT_A32B32G32R32F)
	//	{
	//		return false;
	//	}

	//	int blockWidth = desc.Width/4;
	//	int blockHeight = desc.Height /4;


	//	D3DLOCKED_RECT lockedRectOriginal;
	//	if(!SUCCEEDED(mPTexture->LockRect(0, &lockedRectOriginal, NULL, D3DLOCK_READONLY )))
	//	{
	//		return false;
	//	}    

	//	LPDIRECT3DTEXTURE9 tmp8888Texture=NULL;
	//	D3DXCreateTexture(mPD3DDevice, desc.Width, desc.Height, 1, D3DUSAGE_DYNAMIC, 
	//		D3DFMT_X8R8G8B8, D3DPOOL_SYSTEMMEM , &tmp8888Texture);
	//	if (tmp8888Texture==NULL)
	//	{
	//		return false;
	//	}

	//	D3DLOCKED_RECT lockedRectNew;
	//	if(!SUCCEEDED(tmp8888Texture->LockRect(0, &lockedRectNew, NULL, D3DLOCK_DISCARD )))
	//	{
	//		return false;
	//	}  

	//	//first iteration
	//	float* floatBuffer = (float*)lockedRectOriginal.pBits;
	//	DWORD* dwordBuffer = (DWORD*)lockedRectNew.pBits;
	//	for (int blockY=0; blockY<blockHeight; blockY++)
	//	{
	//		for (int blockX=0; blockX<blockWidth; blockX++)
	//		{
	//			int alphaValues[4][4];
	//			float avgAlpha=0;
	//			for(unsigned int yy=0;yy<4; yy++)
	//			{
	//				for(unsigned int xx=0;xx<4; xx++)
	//				{
	//					int y= blockY*4+yy;
	//					int x= blockX*4+xx;
	//					int index = y* desc.Width + x;
	//					float bluef= floatBuffer[(index)*4+2];
	//					float greenf = floatBuffer[(index)*4+1];
	//					float redf  = floatBuffer[(index)*4+0];
	//					float alphaf  = floatBuffer[(index)*4+3];

	//					int red=0;
	//					int green=0;
	//					int blue=0;
	//					int alpha=0;
	//					if (alphaf>0)
	//					{
	//						calculateRGBE(redf, greenf, bluef, red, green, blue, alpha);
	//					}
	//					alphaValues[yy][xx]=alpha;
	//					avgAlpha+=alpha;
	//				}
	//			}
	//			avgAlpha/=16.0f;

	//			int bestMinAlpha=0;
	//			int bestMaxAlpha=0;
	//			getBestMinMaxAlpha(bestMinAlpha, bestMaxAlpha, alphaValues, avgAlpha );
	//			int alphas[8];
	//			int idx[4][4];
	//			getAlphaInterpolated(bestMinAlpha, bestMaxAlpha, alphas);
	//			getAlphaEncodingIndices(alphaValues, alphas, idx);

	//			for(unsigned int yy=0;yy<4; yy++)
	//			{
	//				for(unsigned int xx=0;xx<4; xx++)
	//				{
	//					int y= blockY*4+yy;
	//					int x= blockX*4+xx;
	//					int index = y* desc.Width + x;
	//					float bluef= floatBuffer[(index)*4+2];
	//					float greenf = floatBuffer[(index)*4+1];
	//					float redf  = floatBuffer[(index)*4+0];
	//					float alphaf  = floatBuffer[(index)*4+3];
	//					int red=0;
	//					int green=0;
	//					int blue=0;
	//					int alpha=0;
	//					if (alphaf>0)
	//					{
	//						int alphaIdx = idx[yy][xx];
	//						//change encoding...
	//						if (alphaIdx==1)
	//						{
	//							alphaIdx = 7;
	//						}
	//						else if (alphaIdx!=0)
	//						{
	//							alphaIdx--;
	//						}
	//						alpha = alphas[alphaIdx];
	//						reduceExponentFromRGB(redf, greenf, bluef, alpha, red, green, blue);
	//					}
	//					dwordBuffer[(y*desc.Width+x)] = ((red<<16L)+(green<<8L)+(blue));
	//				}
	//			}

	//		}
	//	}

	//	mPTexture->UnlockRect(0);
	//	tmp8888Texture->UnlockRect(0);
	//	tmp8888Texture->AddDirtyRect(0);   

	//	LPDIRECT3DTEXTURE9 dxt1Texture=NULL;
	//	TCConvertOptions optsDxt1(desc.Width, desc.Height, D3DFMT_DXT1);
	//	if (!convertToFormatInternal(tmp8888Texture, optsDxt1, &dxt1Texture))
	//	{
	//		return false;
	//	}
	//	LPDIRECT3DTEXTURE9 tmp888BackTexture=NULL;
	//	TCConvertOptions opts888(desc.Width, desc.Height, D3DFMT_X8R8G8B8);
	//	if (!convertToFormatInternal(dxt1Texture, opts888, &tmp888BackTexture))
	//	{
	//		return false;
	//	}


	//	LPDIRECT3DTEXTURE9 tmpdxt5Texture=NULL;
	//	D3DXCreateTexture(mPD3DDevice, desc.Width, desc.Height, 1, D3DUSAGE_DYNAMIC, 
	//		D3DFMT_DXT5, D3DPOOL_SYSTEMMEM , &tmpdxt5Texture);
	//	if (tmpdxt5Texture==NULL)
	//	{
	//		return false;
	//	}

	//	if(!SUCCEEDED(mPTexture->LockRect(0, &lockedRectOriginal, NULL, D3DLOCK_READONLY )))
	//	{
	//		return false;
	//	}    
	//	D3DLOCKED_RECT lockedRectDXT1;
	//	if(!SUCCEEDED(dxt1Texture->LockRect(0, &lockedRectDXT1, NULL, D3DLOCK_DISCARD )))
	//	{
	//		return false;
	//	}
	//	D3DLOCKED_RECT lockedRect888Back;
	//	if(!SUCCEEDED(tmp888BackTexture->LockRect(0, &lockedRect888Back, NULL, D3DLOCK_DISCARD )))
	//	{
	//		return false;
	//	}  

	//	D3DLOCKED_RECT lockedRectDXT5;
	//	if(!SUCCEEDED(tmpdxt5Texture->LockRect(0, &lockedRectDXT5, NULL, D3DLOCK_DISCARD )))
	//	{
	//		return false;
	//	}  

	//	DWORD* backDwordBuffer = (DWORD*)lockedRect888Back.pBits;
	//	ddsColorBlock_t* dxt1BlockBuffer = (ddsColorBlock_t*)lockedRectDXT1.pBits;
	//	ddsDXT5ColorAlphaBlock_s* dxt5BlockBuffer = (ddsDXT5ColorAlphaBlock_s*)lockedRectDXT5.pBits;

	//	for (int blockY=0; blockY<blockHeight; blockY++)
	//	{
	//		for (int blockX=0; blockX<blockWidth; blockX++)
	//		{
	//			int alphaValues[4][4];
	//			float avgAlpha=0;
	//			for(unsigned int yy=0;yy<4; yy++)
	//			{
	//				for(unsigned int xx=0;xx<4; xx++)
	//				{
	//					int y= blockY*4+yy;
	//					int x= blockX*4+xx;
	//					int index = y* desc.Width + x;
	//					float bluef= floatBuffer[(index)*4+2];
	//					float greenf = floatBuffer[(index)*4+1];
	//					float redf  = floatBuffer[(index)*4+0];
	//					//float alphaf  = floatBuffer[(index)*4+3];

	//					int red= (backDwordBuffer[index]>>16 ) & 255;
	//					int green=(backDwordBuffer[index]>>8 ) & 255;
	//					int blue=(backDwordBuffer[index]>>0 ) & 255;

	//					float redFactor= redf/ (((float)red)/255.0f);
	//					float greenFactor= greenf/ (((float)green)/255.0f);
	//					float blueFactor= bluef/ (((float)blue)/255.0f);
	//					float brightness = max(redFactor,max(greenFactor, blueFactor));

	//					alphaValues[yy][xx]=getExponent(brightness);;     
	//					avgAlpha+=alphaValues[yy][xx];
	//				}
	//			}
	//			avgAlpha/=16.0f;
	//			int bestMinAlpha=0;
	//			int bestMaxAlpha=0;
	//			getBestMinMaxAlpha(bestMinAlpha, bestMaxAlpha, alphaValues, avgAlpha );

	//			int blockIndex=blockY * blockWidth + blockX;
	//			dxt5BlockBuffer[blockIndex].colorBlock = dxt1BlockBuffer[blockIndex];
	//			dxt5BlockBuffer[blockIndex].alphaBlock.alpha0=(unsigned char)bestMaxAlpha;
	//			dxt5BlockBuffer[blockIndex].alphaBlock.alpha1=(unsigned char)bestMinAlpha;
	//			fillAlphaEncoding(bestMinAlpha, bestMaxAlpha, alphaValues,dxt5BlockBuffer[blockIndex].alphaBlock.stuff);
	//			//dxt5BlockBuffer[blockIndex].alphaBlock.alpha0=255;
	//			//dxt5BlockBuffer[blockIndex].alphaBlock.alpha1=0;
	//			//dxt5BlockBuffer[blockIndex].alphaBlock.stuff[0]=0;
	//			//dxt5BlockBuffer[blockIndex].alphaBlock.stuff[1]=0;
	//			//dxt5BlockBuffer[blockIndex].alphaBlock.stuff[2]=0;
	//			//dxt5BlockBuffer[blockIndex].alphaBlock.stuff[3]=0;
	//			//dxt5BlockBuffer[blockIndex].alphaBlock.stuff[4]=0;
	//			//dxt5BlockBuffer[blockIndex].alphaBlock.stuff[5]=0;
	//		}
	//	}

	//	mPTexture->UnlockRect(0);
	//	tmp888BackTexture->UnlockRect(0);
	//	dxt1Texture->UnlockRect(0);
	//	tmpdxt5Texture->UnlockRect(0);
	//	tmpdxt5Texture->AddDirtyRect(0);   

	//	tmp888BackTexture->Release();
	//	tmp888BackTexture=NULL;

	//	tmp8888Texture->Release();
	//	tmp8888Texture = NULL;

	//	dxt1Texture->Release();
	//	dxt1Texture = NULL;

	//	releaseTexture();
	//	mPTexture = tmpdxt5Texture;

	//	return true;
	//}

	//-----------------------------------------------------------------------------
	
	//Temporarily depricated until it can be updated to eliminate the dependencies on the DXT5Compress.cpp/h files

	//bool TextureConvert::convertRGBEToABGRF32()
	//{
	//	if (mPTexture==NULL)
	//	{
	//		return false;
	//	}

	//	D3DSURFACE_DESC desc;
	//	getDescription(desc);

	//	if (desc.Format!=D3DFMT_A8R8G8B8)
	//	{
	//		return false;
	//	}

	//	D3DLOCKED_RECT lockedRectOriginal;
	//	if(!SUCCEEDED(mPTexture->LockRect(0, &lockedRectOriginal, NULL, D3DLOCK_READONLY )))
	//	{
	//		return false;
	//	}    

	//	LPDIRECT3DTEXTURE9 newTexture=NULL;
	//	D3DXCreateTexture(mPD3DDevice, desc.Width, desc.Height, 1, D3DUSAGE_DYNAMIC, 
	//		D3DFMT_A32B32G32R32F, D3DPOOL_SYSTEMMEM , &newTexture);
	//	if (newTexture==NULL)
	//	{
	//		return false;
	//	}

	//	D3DLOCKED_RECT lockedRectNew;
	//	if(!SUCCEEDED(newTexture->LockRect(0, &lockedRectNew, NULL, D3DLOCK_DISCARD )))
	//	{
	//		return false;
	//	}  

	//	float* floatBuffer = (float*)lockedRectNew.pBits;
	//	DWORD* dwordBuffer = (DWORD*)lockedRectOriginal.pBits;
	//	for(unsigned int y=0;y<desc.Height; y++)
	//	{
	//		for(unsigned int x=0;x<desc.Width; x++)
	//		{
	//			DWORD val=dwordBuffer[(y*desc.Width+x)];
	//			int alpha=(val>>24) & 0xff;
	//			int red=(val>>16) & 0xff;
	//			int green=(val>>8) & 0xff;
	//			int blue= (val>>0) & 0xff;

	//			float bluef = 0.0f;
	//			float greenf= 0.0f;
	//			float redf  = 0.0f;
	//			float alphaf = 1.0f;
	//			calculateRGBfromRGBE(red, green, blue, alpha, redf, greenf, bluef);

	//			floatBuffer[(y*desc.Width+x)*4+3]= alphaf;
	//			floatBuffer[(y*desc.Width+x)*4+2]= bluef;
	//			floatBuffer[(y*desc.Width+x)*4+1]= greenf;
	//			floatBuffer[(y*desc.Width+x)*4+0]= redf;

	//		}
	//	}

	//	mPTexture->UnlockRect(0);
	//	newTexture->UnlockRect(0);

	//	newTexture->AddDirtyRect(0);

	//	releaseTexture();

	//	newTexture->GenerateMipSubLevels();
	//	mPTexture = newTexture;

	//	return true;
	//}

	//-----------------------------------------------------------------------------
	
	unsigned char getAlphaInterpolated(unsigned char* buffer, int width, int height, float x, float y)
	{
		if (x>(width-1))
		{
			x=static_cast<float>(width-1);
		}
		if (y>(height-1))
		{
			x=static_cast<float>(height-1);
		}

		int xx = static_cast<int>(floor(x));
		int yy = static_cast<int>(floor(y));

		return static_cast<unsigned char>( buffer[yy *width+ xx]);
	}

	//-----------------------------------------------------------------------------

	//Temporarily depricated until it can be updated to eliminate the dependencies on the DXT5Compress.cpp/h files

	//bool TextureConvert::convertABGRF32ToRGBEWithExponentFromFile(LPCTSTR alphaName)
	//{
	//	if (mPTexture==NULL)
	//	{
	//		return false;
	//	}

	//	D3DSURFACE_DESC desc;
	//	getDescription(desc);

	//	if (desc.Format!=D3DFMT_A32B32G32R32F)
	//	{
	//		return false;
	//	}


	//	LPDIRECT3DTEXTURE9 alphaTexture;
	//	HRESULT ok = D3DXCreateTextureFromFile(mPD3DDevice, alphaName, &alphaTexture);
	//	if (ok!=0)
	//	{
	//		alphaTexture->Release();
	//		return false;
	//	}
	//	LPDIRECT3DSURFACE9 textureSurface;
	//	D3DSURFACE_DESC desc2;
	//	alphaTexture->GetSurfaceLevel(0, &textureSurface);
	//	textureSurface->GetDesc(&desc2);
	//	if (desc2.Format!=D3DFMT_L8)
	//	{
	//		alphaTexture->Release();
	//		return false;
	//	}

	//	D3DLOCKED_RECT lockedRectExponent;
	//	if(!SUCCEEDED(alphaTexture->LockRect(0, &lockedRectExponent, NULL, D3DLOCK_READONLY )))
	//	{
	//		return false;
	//	}

	//	D3DLOCKED_RECT lockedRectOriginal;
	//	if(!SUCCEEDED(mPTexture->LockRect(0, &lockedRectOriginal, NULL, D3DLOCK_READONLY )))
	//	{
	//		return false;
	//	}

	//	LPDIRECT3DTEXTURE9 newTexture=NULL;
	//	D3DXCreateTexture(mPD3DDevice, desc.Width, desc.Height, 1, D3DUSAGE_DYNAMIC, 
	//		D3DFMT_A8R8G8B8, D3DPOOL_SYSTEMMEM , &newTexture);
	//	if (newTexture==NULL)
	//	{
	//		return false;
	//	}

	//	D3DLOCKED_RECT lockedRectNew;
	//	if(!SUCCEEDED(newTexture->LockRect(0, &lockedRectNew, NULL, D3DLOCK_DISCARD )))
	//	{
	//		return false;
	//	}  

	//	float* floatBuffer = (float*)lockedRectOriginal.pBits;
	//	DWORD* dwordBuffer = (DWORD*)lockedRectNew.pBits;
	//	unsigned char* exponentBuffer = (unsigned char*) lockedRectExponent.pBits;
	//	for(unsigned int y=0;y<desc.Height; y++)
	//	{
	//		for(unsigned int x=0;x<desc.Width; x++)
	//		{
	//			float bluef= floatBuffer[(y*desc.Width+x)*4+0];
	//			float greenf = floatBuffer[(y*desc.Width+x)*4+1];
	//			float redf  = floatBuffer[(y*desc.Width+x)*4+2];
	//			//float alphaf  = floatBuffer[(y*desc.Width+x)*4+3];

	//			int red=0;
	//			int green=0;
	//			int blue=0;
	//			int alpha = getAlphaInterpolated(exponentBuffer, 
	//				desc2.Width,
	//				desc2.Height,
	//				x * ((float)desc2.Width)/((float)desc.Width),
	//				y * ((float)desc2.Height)/((float)desc.Height));

	//			//int alpha = exponentBuffer[(y*desc.Width+x)];
	//			reduceExponentFromRGB(redf, greenf, bluef, alpha, red, green, blue);
	//			dwordBuffer[(y*desc.Width+x)] = ((alpha<<24L)+(red<<16L)+(green<<8L)+(blue));
	//		}
	//	}

	//	mPTexture->UnlockRect(0);
	//	alphaTexture->UnlockRect(0);

	//	alphaTexture->Release();

	//	newTexture->UnlockRect(0);
	//	newTexture->AddDirtyRect(0);

	//	releaseTexture();

	//	newTexture->GenerateMipSubLevels();
	//	mPTexture = newTexture;

	//	return true;
	//}

	//-----------------------------------------------------------------------------
	bool TextureConvert::convertABGRF32ToARGB()
	{
		if (mPTexture==NULL)
		{
			return false;
		}

		D3DSURFACE_DESC desc;
		getDescription(desc);

		if (desc.Format!=D3DFMT_A32B32G32R32F)
		{
			return false;
		}

		D3DLOCKED_RECT lockedRectOriginal;
		if(!SUCCEEDED(mPTexture->LockRect(0, &lockedRectOriginal, NULL, D3DLOCK_READONLY )))
		{
			return false;
		}    

		LPDIRECT3DTEXTURE9 newTexture=NULL;
		D3DXCreateTexture(mPD3DDevice, desc.Width, desc.Height, 1, D3DUSAGE_DYNAMIC, 
			D3DFMT_A8R8G8B8, D3DPOOL_SYSTEMMEM , &newTexture);
		if (newTexture==NULL)
		{
			return false;
		}

		D3DLOCKED_RECT lockedRectNew;
		if(!SUCCEEDED(newTexture->LockRect(0, &lockedRectNew, NULL, D3DLOCK_DISCARD )))
		{
			return false;
		}  

		float* floatBuffer = (float*)lockedRectOriginal.pBits;
		DWORD* dwordBuffer = (DWORD*)lockedRectNew.pBits;
		int pixelPitch = lockedRectOriginal.Pitch / sizeof(float);

		for(unsigned int y = 0; y < desc.Height; y++)
		{
			for(unsigned int x = 0; x < desc.Width; x++)
			{
				float redf  = floatBuffer[y*pixelPitch+x*RGBA_COLOR_CHANNELS_PER_PIXEL+0];
				float greenf = floatBuffer[y*pixelPitch+x*RGBA_COLOR_CHANNELS_PER_PIXEL+1];
				float bluef= floatBuffer[y*pixelPitch+x*RGBA_COLOR_CHANNELS_PER_PIXEL+2];
				float alphaf  = floatBuffer[y*pixelPitch+x*RGBA_COLOR_CHANNELS_PER_PIXEL+3];

				int r = (int)(redf * 255.0f);
				int g = (int)(greenf * 255.0f);
				int b = (int)(bluef * 255.0f);
				int a = (int)(alphaf * 255.0f);

				dwordBuffer[(y*desc.Width+x)] = ((a<<24)+(r<<16)+(g<<8)+(b));
			}
		}

		mPTexture->UnlockRect(0);
		newTexture->UnlockRect(0);
	
		releaseTexture();

		mPTexture = newTexture;

		return true;
	}
	//-----------------------------------------------------------------------------
	void TextureConvert::getDescription(D3DSURFACE_DESC &desc, LPDIRECT3DTEXTURE9 pTexture, int mipLevel)
	{
		if(!pTexture)
			pTexture = mPTexture;
		Assertf(pTexture != NULL, "TextureConvert::getDescription, NULL texture pointer");

		LPDIRECT3DSURFACE9 textureSurface;
		pTexture->GetSurfaceLevel(mipLevel, &textureSurface);
		textureSurface->GetDesc(&desc);

		//Call to GetSurfaceLevel increases the textures ref count, so follow it up with a release
		pTexture->Release();
	}
	//-----------------------------------------------------------------------------
	D3DFORMAT TextureConvert::getFormat(LPDIRECT3DTEXTURE9 pTexture)
	{
		if(!pTexture)
			pTexture = mPTexture;
		Assertf(pTexture != NULL, "TextureConvert::getFormat, NULL texture pointer");

		D3DSURFACE_DESC desc;
		getDescription(desc, pTexture);
		return desc.Format;
	}
	//-----------------------------------------------------------------------------
	int TextureConvert::getWidth(int mipLevel)
	{
		D3DSURFACE_DESC desc;
		getDescription(desc, mPTexture, mipLevel);
		return desc.Width;
	}
	//-----------------------------------------------------------------------------
	int TextureConvert::getHeight(int mipLevel)
	{
		D3DSURFACE_DESC desc;
		getDescription(desc, mPTexture, mipLevel);
		return desc.Height;
	}	
	//-----------------------------------------------------------------------------
	void TextureConvert::initDevil(int numImages)
	{
		ilInit();
		iluInit();

		m_DevilImageIDs = rage_new ILuint[numImages];
		m_NumDevilImageIDs = numImages;
		ilGenImages(numImages, m_DevilImageIDs);
	}
	//-----------------------------------------------------------------------------
	void TextureConvert::shutdownDevil()
	{
		ilBindImage(0);
		ilDeleteImages(m_NumDevilImageIDs, m_DevilImageIDs);

		delete m_DevilImageIDs;
		m_DevilImageIDs = NULL;
		m_NumDevilImageIDs = 0;
	}
	//-----------------------------------------------------------------------------
	void TextureConvert::setCurrDevilImage(int image)
	{
		ilBindImage(m_DevilImageIDs[image]);
	}
	//-----------------------------------------------------------------------------
	bool TextureConvert::convertTextureToDevilImage()
	{
		if (getFormat() == D3DFMT_A32B32G32R32F)
		{
			convertABGRF32ToARGB();
		}

		if (getFormat() != D3DFMT_A8R8G8B8)
		{
			return false;
		}

		D3DSURFACE_DESC desc;
		getDescription(desc);

		D3DLOCKED_RECT lockedRect;
		if(!SUCCEEDED(mPTexture->LockRect(0, &lockedRect, NULL, D3DLOCK_READONLY )))
		{
			return false;
		}
		
		// Create an ilImage.
		ilTexImage(desc.Width, desc.Height, 1, 4, IL_BGRA, IL_UNSIGNED_BYTE, lockedRect.pBits);

		mPTexture->UnlockRect(0);

		return true;
	}
	//-----------------------------------------------------------------------------
	bool TextureConvert::convertDevilImageToTexture()
	{
		ILinfo info;
		iluGetImageInfo(&info);

		releaseTexture(); // delete the old texture

		LPDIRECT3DTEXTURE9 newTexture=NULL;
		D3DXCreateTexture(mPD3DDevice, info.Width, info.Height, 1, D3DUSAGE_DYNAMIC, 
			D3DFMT_A32B32G32R32F, D3DPOOL_SYSTEMMEM , &newTexture);
		if (newTexture==NULL)
		{
			return false;
		}

		D3DLOCKED_RECT lockedRectNew;
		if(!SUCCEEDED(newTexture->LockRect(0, &lockedRectNew, NULL, D3DLOCK_DISCARD )))
		{
			return false;
		}

		float* pcTextureData = (float*)lockedRectNew.pBits;

		ilCopyPixels(0, 0, 0, info.Width, info.Height, 1, IL_RGBA, IL_FLOAT, pcTextureData);

		// TODO: Convert from RGBA to ARGB

		newTexture->UnlockRect(0);
	
		mPTexture = newTexture;

		return true;
	}
	//-----------------------------------------------------------------------------
	// crops the image but keeps it at a power of two.
	bool TextureConvert::oneToOneCrop(float uMin, float vMin, float uMax, float vMax, TCConvertOptions* tcOpt)
	{
		if (mPTexture==NULL)
		{
			return false;
		}

		initDevil(1);

		setCurrDevilImage(0);

		if (!convertTextureToDevilImage())
		{
			return false;
		}

		int origPixUMin = (int)(uMin * getWidth());
		int origPixUMax = (int)(uMax * getWidth());
		int origPixVMin = (int)(vMin * getHeight());
		int origPixVMax = (int)(vMax * getHeight());

		int cropSizeU = origPixUMax - origPixUMin;
		int cropSizeV = origPixVMax - origPixVMin;

		int actualCropSizeU = cropSizeU;
		int actualCropSizeV = cropSizeV;

		//SEEMS LIKE A HACK
		//make sure we have atleast a 4X4 texture in the end
		if (actualCropSizeU < 4) actualCropSizeU = 4;
		if (actualCropSizeV < 4) actualCropSizeV = 4;

		// round actual sizes up to nearest power of 2
		int paddedCropWidth = getWidth();
		int paddedCropHeight = getHeight();

		while((paddedCropWidth >> 1) >= actualCropSizeU)
		{
			paddedCropWidth >>= 1;
			if (!paddedCropWidth)
			{
				Errorf("While doing a oneToOneCrop the paddedCropWidth ended up being 0. The actual crop size we are looking for is %d", actualCropSizeU);
				return false;
			}
		}

		while((paddedCropHeight >> 1) >= actualCropSizeV)
		{
			paddedCropHeight >>= 1;
			if (!paddedCropHeight)
			{
				Errorf("While doing a oneToOneCrop the paddedCropHeight ended up being 0. The actual crop size we are looking for is %d", actualCropSizeV);
				return false;
			}
		}

		printf("CroppedWidth: %d\nCroppedHeight: %d\n", paddedCropWidth, paddedCropHeight);

		int paddedCropUMin = 0;
		int paddedCropVMin = 0;

		if (paddedCropWidth != getWidth())
		{
			// align the padded crop rect with the origPix rect, see if the padded rect goes outside of the image
			if (origPixUMin + paddedCropWidth > getWidth())
			{
				paddedCropUMin = max(0, origPixUMax - paddedCropWidth);
			}
			else
			{
				paddedCropUMin = origPixUMin;
			}
		}

		if (paddedCropHeight != getHeight())
		{
			// align the padded crop rect with the origPix rect, see if the padded rect goes outside of the image
			if (origPixVMin + paddedCropHeight > getHeight())
			{
				paddedCropVMin = max(0, origPixVMax - paddedCropHeight);
			}
			else
			{
				paddedCropVMin = origPixVMin;
			}
		}

		iluCrop(paddedCropUMin, paddedCropVMin, 0, paddedCropWidth, paddedCropHeight, 1);

		// compute the final u and v for the region of interest
		
		float usefulUMin = (float)(origPixUMin - paddedCropUMin) / (float)paddedCropWidth;
		float usefulUMax = (float)(origPixUMax - paddedCropUMin) / (float)paddedCropWidth;
		float usefulVMin = (float)(origPixVMin - paddedCropVMin) / (float)paddedCropHeight;
		float usefulVMax = (float)(origPixVMax - paddedCropVMin) / (float)paddedCropHeight;

		printf("CroppedUMin: %f\nCroppedVMin: %f\nCroppedUMax: %f\nCroppedVMax: %f\n", usefulUMin, usefulVMin, usefulUMax, usefulVMax);

		if (!convertDevilImageToTexture())
		{
			return false;
		}

		tcOpt->Height = getHeight();
		tcOpt->Width = getWidth();

		shutdownDevil();

		return true;

	}
	//-----------------------------------------------------------------------------
	bool TextureConvert::scale(TCConvertOptions* tcOpt, float scale, bool nearestFilter)
	{
		if (mPTexture==NULL)
		{
			return false;
		}

		initDevil(1);

		setCurrDevilImage(0);

		iluImageParameter(ILU_FILTER, nearestFilter ? ILU_NEAREST : ILU_SCALE_LANCZOS3);

		if (!convertTextureToDevilImage())
		{
			return false;
		}

		int newWidth = int(float(getWidth()) * scale);
		int newHeight = int(float(getHeight()) * scale);
		iluScale(newWidth, newWidth, 1);

		if (!convertDevilImageToTexture())
		{
			return false;
		}
		
		tcOpt->Width = newWidth;
		tcOpt->Height = newHeight;

		shutdownDevil();

		return true;
	}
	//-----------------------------------------------------------------------------
	bool TextureConvert::cropDevilImage(float uMin, float vMin, float uMax, float vMax, bool cropPow2RoundingUp, ILenum filter, TCConvertOptions* pOpts, float scale)
	{
		iluImageParameter(ILU_FILTER, filter);

		int image_width = ilGetInteger(IL_IMAGE_WIDTH);
		int image_height = ilGetInteger(IL_IMAGE_HEIGHT);

		// Crop the image if that was specified before we do any of the code that imposes maximum size/scaling.

		// convert float uv rectangle cropping parameters to pixel space.
		ILint cropped_width  = max(1,(int)(((ILfloat)image_width  * (uMax-uMin)))); //must at least have 1 pixel to work with
		ILint cropped_height = max(1,(int)(((ILfloat)image_height * (vMax-vMin)))); //must at least have 1 pixel to work with

		ILuint xoff = (int)((ILfloat)image_width  * uMin);
		ILuint yoff = (int)((ILfloat)image_height * vMin);
		ILuint zoff = 0;

		// Since we can push up a 0-pixel crop to 1 pixel in the above block,
		// we might need to pull back the offsets as well to stay on the image.
		xoff = Min((int)xoff,(int)(image_width - cropped_width));
		yoff = Min((int)yoff,(int)(image_height - cropped_height));

		iluCrop( xoff, yoff, zoff, cropped_width, cropped_height, 1 );// its not a 3d texture and the depth parameter is not the pixel format like i guessed.

		// input textures need to be scaled up now a little if its close to the next pow2 size, or brought down.
		//		ILfloat original_aspect = (float)image_width / (float)image_height;

		// apply the scale factor to the image size
		cropped_width = (int)(scale * cropped_width);
		cropped_height = (int)(scale * cropped_height);

		// starting with the source image, we calculate the difference between growing the cropped area back to the current size test

		ILint curr_w = (image_width );
		ILint next_w = (image_width >> 1);

		ILint curr_h = (image_height );
		ILint next_h = (image_height >> 1);

		ILint smallest_w = 32;// dont resize down past this (its tiny)
		ILint smallest_h = 32;// dont resize down past this (its tiny)

		ILuint cropped_to_pow2_width;
		ILuint cropped_to_pow2_height;

		for(;;)
		{
			// We shift down sizes until the cropped amount value is between 'next' and 'curr'
			// then we decide if we round 'down' or 'up'

			// If 'next_w' is still larger than the cropped width, loop.
			if( (cropped_width < curr_w) && (next_w > cropped_width) && (next_w > smallest_w) )
			{
				curr_w = next_w;
				next_w = next_w >> 1;
				continue;
			}

			if( cropPow2RoundingUp &&  (cropped_width!= next_w) )
			{// going to stretch the image up to the next power of 2 (curr_w)
				cropped_to_pow2_width = curr_w;
				break;
			}else{
				// going to scale image down to next power of 2 (next_w)
				cropped_to_pow2_width = next_w;
				break;
			}
		}

		for(;;)
		{
			// We shift down sizes until the cropped amount value is between 'next' and 'curr'
			// then we decide if we round 'down' or 'up'

			// If 'next_w' is still larger than the cropped width, loop.
			if( (cropped_height < curr_h) && (next_h > cropped_height) && (next_h > smallest_h) )
			{
				curr_h = next_h;
				next_h = next_h >> 1;
				continue;
			}

			if( cropPow2RoundingUp && (cropped_height!= next_h) )
			{// going to stretch the image up to the next power of 2 (curr_w)
				cropped_to_pow2_height = curr_h;
				break;
			}else{
				// going to scale image down to next power of 2 (next_w)
				cropped_to_pow2_height = next_h;
				break;
			}
		}

		printf("[rescaling cropped image to %d x %d...]\n",cropped_to_pow2_width, cropped_to_pow2_height );
		iluScale( cropped_to_pow2_width, cropped_to_pow2_height, 1);

		image_width = cropped_to_pow2_width;
		image_height= cropped_to_pow2_height;

		pOpts->Height = image_height;
		pOpts->Width = image_width;

		return true;
	}


#define DXT_MACRO_BLOCK_SIZE	4				// how big our pixel border should be...based on the DXT macro block size
#define MAGIC_SCALE_TRIGGER		0.91015625f		// we want very precise control of our blend texture sizes so they seam perfectly...this is 1864/2048 which is our new ratio
#define MAGIC_SOURCE_SIZE		2048			// the size of our base un-scaled texture we'll be pulling borders from
#define MAGIC_DEST_SIZE			1864			// the size of our new scaled texture we'll be writing the border to
#define ONE_TO_ONE_MATCH_SIZE	12				// how many pixels we want to leave off the edges of our texture so that they exactly seam up...we'll compensate for the size difference between this value and size minus this value
#define DROP_PIXEL_STRIDE		10				// how many pixels we should go before we drop one to compensate for our smaller size

	// For some assets we don't want to have the texel-ratio changed when we crop the sub-texture.  So rather than crop->re-scale we simply crop
	// ....a larger piece of the texture so that it's still a power of 2 and we keep the texel ratio the same.
	bool TextureConvert::cropDevilImageNoScale(float uMin, float vMin, float uMax, float vMax, int cropRoundingFlag, ILenum filter, TCConvertOptions* pOpts, float scale, LPCTSTR pDestFile, int &extraPixels, int &extraPixelsOld, int &extraPixelsIdeal, int &idealPixelCount)
	{
		// Set our image filtering
		iluImageParameter(ILU_FILTER, filter);

		// grab our image sizes so that we can scale by a certain amount
		int image_width = ilGetInteger(IL_IMAGE_WIDTH);
		int image_height = ilGetInteger(IL_IMAGE_HEIGHT);

		// scale our texture (typically downwards so that we can then round upwards later for a uniform texel density).
		iluScale( (ILuint)(image_width*scale), (ILuint)(image_height*scale), 1);

		// ++++ HOLY HACK BATMAN!!
		// Here we are going to perform a total hack to get around a problem with terrain blendmaps on RDR2.  We need the maps to keep the same border
		// ... even after they've been shrunk down.  So we're going to copy over a 4 pixel border (DXT macro block size) after the downscale.  To ID which
		// ... textures we want to do this for we're hi-jacking the scale.  Ideally we would pass in some extra params 
		// ... but I want to minimize tool changes this clost to deadline.
		if (scale == MAGIC_SCALE_TRIGGER)
		{
			ILuint srcID = m_DevilImageIDs[1];
			ILuint dstID = m_DevilImageIDs[0];

			// iCurImage is a global pointer...seems like there should be a better way to get at the image data but this seems to be the common method for devilstuff
			ILimage *DstImage = iCurImage;
			ilBindImage(srcID);
			ILimage *SrcImage = iCurImage;
			ilBindImage(dstID);

			// we only want to do this border operation for very specific sizes...as it's only been tuned for those sizes.
			if (iCurImage && dstID && srcID && SrcImage->Width == MAGIC_SOURCE_SIZE && SrcImage->Height == MAGIC_SOURCE_SIZE && DstImage->Width == MAGIC_DEST_SIZE && DstImage->Height == MAGIC_DEST_SIZE)
			{
				Displayf("Hacking up blendmap texture to have a common border.");

				u32* dstPtr = (u32*)DstImage->Data;
				u32* srcPtr = (u32*)SrcImage->Data;
				u32* curPtr = dstPtr;

				// go through all the pixels in our destination buffer and either copy them directly or grab pixels from the un-scaled original.  We 
				// ...only grab from the un-scaled original for the 4 pixel border.
				for (u32 i = 0; i < DstImage->Height; ++i)
				{
					for (u32 j = 0; j < DstImage->Width; ++j)
					{
						// if we're within 4 pixels of the edge of our texture we want an un-scaled source pixel...otherwise we'll just self copy.
						if (i <  DXT_MACRO_BLOCK_SIZE || 
							i >= (DstImage->Height-DXT_MACRO_BLOCK_SIZE) || 
							j <  DXT_MACRO_BLOCK_SIZE || 
							j >= (DstImage->Width-DXT_MACRO_BLOCK_SIZE))
						{
							curPtr = srcPtr;
						}
						else
						{
							curPtr = dstPtr;
						}

						// copy the value and increment our pointers.
						*dstPtr = *curPtr;
						dstPtr++;
						srcPtr++;

						// we want our un-scaled source pointer to out-pace our destination pointer as we're trying to fit 2048 pixels into 1864 pixels.
						// ...to do this we ignore the first and last n-pixels (12) and then drop every 10th pixel.  This let's us fit exactly into 1864 from 2048.
						if (j >= ONE_TO_ONE_MATCH_SIZE-1 && j < DstImage->Width - ONE_TO_ONE_MATCH_SIZE - 1)
						{
							if ((j - ONE_TO_ONE_MATCH_SIZE) % DROP_PIXEL_STRIDE == 0)
								srcPtr++;
						}
					}

					// we want our un-scaled source pointer to out-pace our destination pointer as we're trying to fit 2048 pixels into 1864 pixels.
					// ...to do this we ignore the first and last n-pixels (12) and then drop every 10th pixel.  This let's us fit exactly into 1864 from 2048.
					if (i >= ONE_TO_ONE_MATCH_SIZE-1 && i < DstImage->Height - ONE_TO_ONE_MATCH_SIZE - 1)
					{
						if ((i - ONE_TO_ONE_MATCH_SIZE) % DROP_PIXEL_STRIDE == 0)
							srcPtr += SrcImage->Width;
					}
				}
			}
		}
		// ---- HOLY HACK BATMAN!!

		image_width = ilGetInteger(IL_IMAGE_WIDTH);
		image_height = ilGetInteger(IL_IMAGE_HEIGHT);


		// our floating point UVs in pixel space
		ILfloat xMinFloat = (ILfloat)image_width  * uMin;
		ILfloat yMinFloat = (ILfloat)image_height * vMin;

		ILfloat xMaxFloat = (ILfloat)image_width  * uMax;
		ILfloat yMaxFloat = (ILfloat)image_height * vMax;


		// convert floating point UVs into int pixel space rounding outwards.
		ILint xoff = (int)floor(xMinFloat);
		ILint yoff = (int)floor(yMinFloat);

		ILint xEnd = (int)ceil(xMaxFloat);
		ILint yEnd = (int)ceil(yMaxFloat);


		// we want a pixel boundary for bi-linear filtering and we want to have the same macro-block layout (for DXT compression)
		xoff = (ILint)(floor(((ILfloat)xoff-1.0f)/4.0f)*4.0f);
		yoff = (ILint)(floor(((ILfloat)yoff-1.0f)/4.0f)*4.0f);

		xEnd = (ILint)(ceil(((ILfloat)xEnd+1.0f)/4.0f)*4.0f);
		yEnd = (ILint)(ceil(((ILfloat)yEnd+1.0f)/4.0f)*4.0f);

		//ILint cropped_width  = max(1,(int)ceil(((ILfloat)image_width  * (uMax-uMin)))); //must at least have 1 pixel to work with
		//ILint cropped_height = max(1,(int)ceil(((ILfloat)image_height * (vMax-vMin)))); //must at least have 1 pixel to work with
		ILint cropped_width  = max(1, xEnd - xoff);
		ILint cropped_height = max(1, yEnd - yoff);


		// starting with the source image, we calculate the difference between growing the cropped area back to the current size test

		ILint curr_w = (image_width );
		ILint next_w = (image_width >> 1);

		ILint curr_h = (image_height );
		ILint next_h = (image_height >> 1);

		ILint smallest_w = 32;// dont resize down past this (its tiny)
		ILint smallest_h = 32;// dont resize down past this (its tiny)

		ILint w_round_lo;
		ILint h_round_lo;
		ILint w_round_hi;
		ILint h_round_hi;

		// We shift down sizes until the cropped amount value is between 'next' and 'curr'
		// then we decide if we round 'down' or 'up'
		for(;;)
		{
			// If 'next_w' is still larger than the cropped width, loop.
			if( (cropped_width < curr_w) && (next_w > cropped_width) && (next_w > smallest_w) )
			{
				curr_w = next_w;
				next_w = next_w >> 1;
				continue;
			}

			w_round_hi	= (cropped_width!=next_w) ? curr_w : next_w;
			w_round_lo	= next_w;
			break;
		}

		for(;;)
		{
			// If 'next_w' is still larger than the cropped width, loop.
			if( (cropped_height < curr_h) && (next_h > cropped_height) && (next_h > smallest_h) )
			{
				curr_h = next_h;
				next_h = next_h >> 1;
				continue;
			}

			h_round_hi	= (cropped_height!=next_h) ? curr_h : next_h;
			h_round_lo	= next_h;
			break;
		}




		bool roundXupOLD = (cropped_width  - w_round_lo  > (ILint)(cropped_width *0.5f));
		bool roundYupOLD = (cropped_height - h_round_lo  > (ILint)(cropped_height*0.5f));

		ILint crop_wOLD = roundXupOLD ? w_round_hi : cropped_width;
		ILint crop_hOLD = roundYupOLD ? h_round_hi : cropped_height;

		ILint scaled_wOLD = roundXupOLD ? w_round_hi : w_round_lo;
		ILint scaled_hOLD = roundYupOLD ? h_round_hi : h_round_lo;



		ILint w_round_hi_NEW = (ILint)pow(2.0f, (ILfloat)_CeilLog2(cropped_width));
		ILint h_round_hi_NEW = (ILint)pow(2.0f, (ILfloat)_CeilLog2(cropped_height));
		ILint w_round_lo_NEW = (ILint)pow(2.0f, (ILfloat)_FloorLog2(cropped_width));
		ILint h_round_lo_NEW = (ILint)pow(2.0f, (ILfloat)_FloorLog2(cropped_height));

		if (cropRoundingFlag == 2)
		{
			w_round_hi_NEW = (ILint)(ceil((ILfloat)cropped_width  / 128.0f)*128.0f);
			h_round_hi_NEW = (ILint)(ceil((ILfloat)cropped_height / 128.0f)*128.0f);
			w_round_lo_NEW = (ILint)(floor((ILfloat)cropped_width  / 128.0f)*128.0f);
			h_round_lo_NEW = (ILint)(floor((ILfloat)cropped_height / 128.0f)*128.0f);
		}

		bool roundXup = cropRoundingFlag != 0;//(cropped_width  - w_round_lo_NEW  > 0);
		bool roundYup = cropRoundingFlag != 0;//(cropped_height - h_round_lo_NEW  > 0);

		if (w_round_hi_NEW - 6 <= cropped_width)
			roundXup = true;

		if (h_round_hi_NEW - 6 <= cropped_height)
			roundYup = true;

		ILint crop_w = roundXup ? w_round_hi_NEW : cropped_width;
		ILint crop_h = roundYup ? h_round_hi_NEW : cropped_height;

		ILint scaled_w = roundXup ? w_round_hi_NEW : w_round_lo_NEW;
		ILint scaled_h = roundYup ? h_round_hi_NEW : h_round_lo_NEW;




		// Since we add to the width/height of our crop for the border we need to guard against going outside our texture
		crop_w = Min((int)crop_w, image_width);
		crop_h = Min((int)crop_h, image_height);

		// Since we add a border for bi-linear filtering we might need to push the offset back onto the texture
		xoff = Max((int)xoff,0);
		yoff = Max((int)yoff,0);

		// Since we can push up a 0-pixel crop to 1 pixel in the above block,
		// we might need to pull back the offsets as well to stay on the image.
		xoff = Min((int)xoff,(int)(image_width - crop_w));
		yoff = Min((int)yoff,(int)(image_height - crop_h));

		// we don't ever want a final texture smaller than 32x32
		scaled_w = Max(scaled_w, 32);
		scaled_h = Max(scaled_h, 32);

		// we don't ever want a final texture larger than 256x256 really
		scaled_w = Min(scaled_w, 512);
		scaled_h = Min(scaled_h, 512);



		Displayf("#################################");
		Displayf("Desired Size       < %4d X %4d >", cropped_width, cropped_height);
		Displayf("---------------------------------");
		Displayf("Old Crop Size      < %4d X %4d >", crop_wOLD, crop_hOLD);
		Displayf("Old Final Size     < %4d X %4d >", scaled_wOLD, scaled_hOLD);
		Displayf("---------------------------------");
		Displayf("New Crop Size      < %4d X %4d >", crop_w, crop_h);
		Displayf("New Final Size     < %4d X %4d >", scaled_w, scaled_h);
		Displayf("---------------------------------");
		Displayf("Old vs. New Size   < %4d X %4d >", scaled_w-scaled_wOLD, scaled_h-scaled_hOLD);
		Displayf("Old vs. Ideal Size < %4d X %4d >", scaled_wOLD-cropped_width, scaled_hOLD-cropped_height);
		Displayf("New vs. Ideal Size < %4d X %4d >", scaled_w-cropped_width, scaled_h-cropped_height);
		Displayf("---------------------------------");
		Displayf("Desired Cut < %5.2f, %5.2f > -> < %5.2f, %5.2f >", xMinFloat, yMinFloat, xMaxFloat, yMaxFloat);
		Displayf("Final Cut   < %4d, %4d > -> < %4d, %4d >", xoff, yoff, xoff+crop_w, yoff+crop_h);
		Displayf("#################################");

		extraPixels			+= scaled_w*scaled_h-scaled_wOLD*scaled_hOLD;
		extraPixelsOld		+= scaled_wOLD*scaled_hOLD-cropped_width*cropped_height;
		extraPixelsIdeal	+= scaled_w*scaled_h-cropped_width*cropped_height;
		idealPixelCount		+= cropped_width*cropped_height;


		iluCrop( xoff, yoff, 0, crop_w, crop_h, 1 );// its not a 3d texture and the depth parameter is not the pixel format like i guessed.
		iluScale( scaled_w, scaled_h, 1);


		ILfloat uMinNew  = (xMinFloat - xoff) / (ILfloat)crop_w;
		ILfloat uMaxNew  = (xMaxFloat - xoff) / (ILfloat)crop_w;
		ILfloat vMinNew  = (yMinFloat - yoff) / (ILfloat)crop_h;
		ILfloat vMaxNew  = (yMaxFloat - yoff) / (ILfloat)crop_h;

		float a1 = -uMin;
		float b1 = 1.0f / (uMax - uMin);
		float c1 = (uMaxNew - uMinNew);
		float d1 = uMinNew;

		float a2 = -vMin;
		float b2 = 1.0f / (vMax - vMin);
		float c2 = (vMaxNew - vMinNew);
		float d2 = vMinNew;

		ILfloat newOffsetU = a1*b1*c1+d1;
		ILfloat newOffsetV = a2*b2*c2+d2;

		ILfloat newScaleU = b1*c1;
		ILfloat newScaleV = b2*c2;


		printf("cropping: [ %s ] [ %f , %f ] [ %f , %f ] [%d x %d]->[%d x %d]\n", pDestFile, newOffsetU, newOffsetV, newScaleU, newScaleV, cropped_width, cropped_height, scaled_w, scaled_h);

		image_width = scaled_w;
		image_height= scaled_h;

		pOpts->Height = image_height;
		pOpts->Width = image_width;

		return true;
	}


	//-----------------------------------------------------------------------------
	bool TextureConvert::crop(float uMin, float vMin, float uMax, float vMax, bool cropPow2RoundingUp, ILenum filter, TCConvertOptions* pOpts, float scale )
	{
		if (mPTexture==NULL)
		{
			return false;
		}

		initDevil(1);

		setCurrDevilImage(0);

		convertTextureToDevilImage();

		cropDevilImage(uMin, vMin, uMax, vMax, cropPow2RoundingUp, filter, pOpts, scale);
		
		convertDevilImageToTexture();

		shutdownDevil();

		return true;
	}
	//-----------------------------------------------------------------------------
	bool TextureConvert::multicrop(std::vector<MulticropOptions>& crops, bool noScale)
	{
		if (mPTexture==NULL)
		{
			return false;
		}

		initDevil(2);

		setCurrDevilImage(1);

		convertTextureToDevilImage();

		int extraPixels = 0;
		int extraPixelsOLD = 0;
		int extraPixelsIdeal = 0;
		int idealPixelCount = 0;

		for(size_t i = 0; i < crops.size(); i++)
		{
			setCurrDevilImage(0);
			ilCopyImage(m_DevilImageIDs[1]);

			bool success;

			if (noScale)
			{
			success = cropDevilImageNoScale(crops[i].uMin, crops[i].vMin, crops[i].uMax, crops[i].vMax,
				crops[i].cropRoundingFlag, crops[i].filter, &crops[i].options, crops[i].scale, crops[i].filename.c_str(), extraPixels, extraPixelsOLD, extraPixelsIdeal, idealPixelCount);
			}
			else
			{
			success = cropDevilImage(crops[i].uMin, crops[i].vMin, crops[i].uMax, crops[i].vMax,
				(crops[i].cropRoundingFlag != 0), crops[i].filter, &crops[i].options, crops[i].scale);
			}

			if (!success)
			{
				return false;
			}

			convertDevilImageToTexture();

#if USE_COMPRESSINATOR
			//If we are writing DXT5 or DXT1 textures the conversion and compression will be handled by the ATI compressonator in the call
			//to save the texture
			if( (crops[i].options.Format != D3DFMT_DXT5) && (crops[i].options.Format != D3DFMT_DXT1) )
			{
				TextureConvert::AlphaCheckResult alphaCheckRes = hasAlpha();
				Displayf("TextureAlpha : %d", ( (alphaCheckRes == TextureConvert::RTC_HASMULTIBITALPHA) || (alphaCheckRes == TextureConvert::RTC_HASONEBITALPHA) ) ? 1 : 0);

				if (!convertToFormat(crops[i].options))
				{
					Errorf("Failed to convert to format \"%d\"", crops[i].options.Format);
					return 1;
				}
			}
#else

			TextureConvert::AlphaCheckResult alphaCheckRes = hasAlpha();
			Displayf("TextureAlpha : %d", ( (alphaCheckRes == TextureConvert::RTC_HASMULTIBITALPHA) || (alphaCheckRes == TextureConvert::RTC_HASONEBITALPHA) ) ? 1 : 0);

			if (!convertToFormat(crops[i].options))
			{
				Errorf("Failed to convert to format \"%d\"", crops[i].options.Format);
				return 1;
			}

#endif //USE_COMPRESSINATOR


			saveTexture(crops[i].filename.c_str(), crops[i].options);
		}

		if (noScale)
		{
			Displayf("----------------Multicrop process pixelcount-----------------");
			Displayf("New Pixels Over Old   - %d", extraPixels);
			Displayf("New Pixels Over Ideal - %d", extraPixelsIdeal);
			Displayf("Old Pixels Over Ideal - %d", extraPixelsOLD);
			Displayf("Ideal Pixel Count     - %d", idealPixelCount);
		}

		shutdownDevil();

		return true;
	}
	//-----------------------------------------------------------------------------
	bool TextureConvert::gammaCorrect(bool InGamma_Linear, bool InGamma_22, bool InGamma_SO, bool AdjustGamma_Linear, bool AdjustGamma_22, bool AdjustGamma_22Xenon, bool AdjustGamma_20)
	{
		//if(mInputGamma == fNewGamma) return true;

		if (mPTexture==NULL)
		{
			return false;
		}

		D3DSURFACE_DESC desc;
		getDescription(desc);

		if (desc.Format != D3DFMT_A32B32G32R32F )
		{
			return false;
		}

		D3DLOCKED_RECT lockedRectOriginal;
		if(!SUCCEEDED(mPTexture->LockRect(0, &lockedRectOriginal, NULL, D3DLOCK_READONLY )))
		{
			return false;
		}    

		LPDIRECT3DTEXTURE9 newTexture=NULL;
		D3DXCreateTexture(mPD3DDevice, desc.Width, desc.Height, 1, D3DUSAGE_DYNAMIC, 
			D3DFMT_A32B32G32R32F, D3DPOOL_SYSTEMMEM , &newTexture);
		if (newTexture==NULL)
		{
			return false;
		}

		D3DLOCKED_RECT lockedRectNew;
		if(!SUCCEEDED(newTexture->LockRect(0, &lockedRectNew, NULL, D3DLOCK_DISCARD )))
		{
			return false;
		}  

		float* sourceBuffer = (float*)lockedRectOriginal.pBits;
		float* destBuffer = (float*)lockedRectNew.pBits;
		int pixelPitch = lockedRectOriginal.Pitch / sizeof(float);


		for(unsigned int y=0;y<desc.Height; y++)
		{
			for(unsigned int x=0;x<desc.Width; x++)
			{
				int location = y * pixelPitch + x * RGBA_COLOR_CHANNELS_PER_PIXEL;

				const float* src = &(sourceBuffer[location]);
				float* des = &(destBuffer[ location ]);

				des[ALPHA]	= src[ ALPHA ];

				float tempG = src[ GREEN ];
				float tempR = src[ RED ];
				float tempB = src[ BLUE ];

				if (InGamma_22)
				{
					tempG = RTCGammaConvert_22To10(tempG);
					tempR = RTCGammaConvert_22To10(tempR);
					tempB = RTCGammaConvert_22To10(tempB);
				}
				else if (InGamma_SO)
				{
					tempR = RTCGammaConvert_22To10(tempR);
					tempG = tempG;
					tempB = RTCGammaConvert_22To10(tempB);
				}
				else if (!InGamma_Linear)
				{
					Errorf("Error: Check gamma correction value for input texture (only gamma space 2.2 and linear space 1.0 inputs are valid)");
					return false;
				}

				if (AdjustGamma_Linear)
				{
					des[GREEN]  = tempG;
					des[RED]    = tempR;
					des[BLUE]   = tempB;
					mInputGamma = 1.0f;
				}
				else if (AdjustGamma_22)
				{
					des[GREEN]  = RTCGammaConvert_10To22(tempG);
					des[RED]    = RTCGammaConvert_10To22(tempR);
					des[BLUE]   = RTCGammaConvert_10To22(tempB);
					mInputGamma = 2.2f;
				}
				else if (AdjustGamma_22Xenon)
				{
					des[GREEN]  = RTCGammaConvert_10To22Xenon(tempG);
					des[RED]    = RTCGammaConvert_10To22Xenon(tempR);
					des[BLUE]   = RTCGammaConvert_10To22Xenon(tempB);
					mInputGamma = 2.2f;
				}
				else if (AdjustGamma_20)
				{
					des[GREEN]  = RTCGammaConvert_10To20(tempG);
					des[RED]    = RTCGammaConvert_10To20(tempR);
					des[BLUE]   = RTCGammaConvert_10To20(tempB);
					mInputGamma = 2.0;
				}
			}
		}

		mPTexture->UnlockRect(0);
		newTexture->UnlockRect(0);

		releaseTexture();

		mPTexture = newTexture;
		return true;
	}

	//-----------------------------------------------------------------------------

	bool TextureConvert::adjustExposure(float exposureVal)
	{
		if (mPTexture==NULL)
		{
			return false;
		}

		//Make sure the internal texture is in the ABGR 32 bit float format.
		D3DSURFACE_DESC desc;
		getDescription(desc);

		if (desc.Format != D3DFMT_A32B32G32R32F )
		{
			return false;
		}
		
		int nMips = mPTexture->GetLevelCount();

		//Create the new texture to hold the results of the processing
		LPDIRECT3DTEXTURE9 newTexture=NULL;
		D3DXCreateTexture(mPD3DDevice, desc.Width, desc.Height, nMips, D3DUSAGE_DYNAMIC, 
			D3DFMT_A32B32G32R32F, D3DPOOL_SYSTEMMEM , &newTexture);
		if (newTexture==NULL)
		{
			return false;
		}

		//Iterate over the texture data in each mip level and adjust the exposure accordingly
		for(int mipIdx=0; mipIdx < nMips; mipIdx++)
		{
			D3DSURFACE_DESC mipDesc;
			getDescription(mipDesc, mPTexture, mipIdx);

			D3DLOCKED_RECT lockedRectOriginal;
			if(!SUCCEEDED(mPTexture->LockRect(mipIdx, &lockedRectOriginal, NULL, D3DLOCK_READONLY )))
			{
				newTexture->Release();
				return false;
			}

			D3DLOCKED_RECT lockedRectNew;
			if(!SUCCEEDED(newTexture->LockRect(mipIdx, &lockedRectNew, NULL, NULL )))
			{
				newTexture->Release();
				return false;
			}  

			float* sourceBuffer = (float*)lockedRectOriginal.pBits;
			float* destBuffer = (float*)lockedRectNew.pBits;
			int pixelPitch = lockedRectOriginal.Pitch / sizeof(float);

			for(unsigned int y=0;y<mipDesc.Height; y++)
			{
				for(unsigned int x=0;x<mipDesc.Width; x++)
				{
					int location = y * pixelPitch + x * RGBA_COLOR_CHANNELS_PER_PIXEL;

					const float* src = &(sourceBuffer[location]);
					float* des = &(destBuffer[ location ]);

					des[ALPHA]	= src[ ALPHA ];

					des[GREEN]	= src[ GREEN ] * (pow(2.0f,-exposureVal));
					des[RED]	= src[ RED ] * (pow(2.0f,-exposureVal));
					des[BLUE]	= src[ BLUE ] * (pow(2.0f,-exposureVal));
				}
			}

			mPTexture->UnlockRect(mipIdx);
			newTexture->UnlockRect(mipIdx);
		}

		//Release the old internal texture and assign the new processed texture as the internal one
		releaseTexture();
		mPTexture = newTexture;

		return true;
	}

	//-----------------------------------------------------------------------------

	bool TextureConvert::loadAlphaFromL8(LPCTSTR alphaName)
	{
		if (mPTexture==NULL)
		{
			return false;
		}

		D3DSURFACE_DESC desc;
		getDescription(desc);

		if (desc.Format!= D3DFMT_A8R8G8B8)
		{
			TCConvertOptions opts(desc.Width, desc.Height, D3DFMT_A8R8G8B8);
			convertToFormat(opts);

			getDescription(desc);
			if (desc.Format!=D3DFMT_A8R8G8B8)
			{
				return false;
			}
		}

		LPDIRECT3DTEXTURE9 alphaTexture;
		HRESULT ok = D3DXCreateTextureFromFile(mPD3DDevice, alphaName, &alphaTexture);
		if (ok!=0)
		{
			alphaTexture->Release();
			return false;
		}
		LPDIRECT3DSURFACE9 textureSurface;
		D3DSURFACE_DESC desc2;
		alphaTexture->GetSurfaceLevel(0, &textureSurface);
		textureSurface->GetDesc(&desc2);
		if (desc2.Format!=D3DFMT_L8)
		{
			alphaTexture->Release();
			return false;
		}


		D3DLOCKED_RECT lockedRectOriginal;
		if(!SUCCEEDED(mPTexture->LockRect(0, &lockedRectOriginal, NULL, D3DLOCK_DISCARD )))
		{
			alphaTexture->Release();
			return false;
		}    

		D3DLOCKED_RECT lockedRectNew;
		if(!SUCCEEDED(alphaTexture->LockRect(0, &lockedRectNew, NULL, D3DLOCK_READONLY )))
		{
			alphaTexture->Release();
			return false;
		}  

		DWORD* destBuffer = (DWORD*)lockedRectOriginal.pBits;
		UCHAR* sourceBuffer = (UCHAR*)lockedRectNew.pBits;
		for(unsigned int y=0;y<desc.Height; y++)
		{
			for(unsigned int x=0;x<desc.Width; x++)
			{
				DWORD val=destBuffer[(y*desc.Width+x)];

				unsigned char alpha = getAlphaInterpolated(sourceBuffer, 
					desc2.Width,
					desc2.Height,
					x * ((float)desc2.Width)/((float)desc.Width), 
					y * ((float)desc2.Height)/((float)desc.Height) );

				//UCHAR alpha = sourceBuffer[(y*desc.Width+x)];
				val = ((val & 0x00ffffff) | (((DWORD)alpha)<<24));
				destBuffer[(y*desc.Width+x)] = val;
			}
		}

		mPTexture->UnlockRect(0);
		alphaTexture->UnlockRect(0);
		//mPTexture->AddDirtyRect(0);
		alphaTexture->Release();

		return true;
	}

	//-----------------------------------------------------------------------------
	bool TextureConvert::convert( PixelConvertor* convertor, TCConvertOptions* pOpts )
	{
		if (mPTexture==NULL)
		{
			return false;
		}

		//Make sure the internal texture is in the ABGR 32 bit float format.
		D3DSURFACE_DESC desc;
		getDescription(desc);

		if (desc.Format != D3DFMT_A32B32G32R32F )
		{
			TCConvertOptions tcOps;
			tcOps.Format = D3DFMT_A32B32G32R32F;
			tcOps.Width = desc.Width;
			tcOps.Height = desc.Height;
			convertToFormat(tcOps);

			getDescription(desc);
			if (desc.Format!=D3DFMT_A32B32G32R32F)
			{
				return false;
			}
		}

		//Create the new texture to hold the processed data
		unsigned int origMipLevels = (unsigned int) mPTexture->GetLevelCount();
		
		LPDIRECT3DTEXTURE9 newTexture=NULL;
		D3DFORMAT format = D3DFMT_A32B32G32R32F;
		D3DXCheckTextureRequirements(mPD3DDevice, &desc.Width, &desc.Height, &origMipLevels, D3DUSAGE_DYNAMIC, 
			&format, D3DPOOL_DEFAULT );
		D3DXCreateTexture(mPD3DDevice, desc.Width, desc.Height, origMipLevels, D3DUSAGE_DYNAMIC, 
			format, D3DPOOL_DEFAULT , &newTexture);
		if (newTexture==NULL)
		{
			return false;
		}

		//Iterate over the pixels in each mip level and apply the conversion process
		for(unsigned int mipIdx=0; mipIdx<origMipLevels; mipIdx++)
		{
			D3DSURFACE_DESC mipDesc;
			getDescription(mipDesc, mPTexture, mipIdx);

			D3DLOCKED_RECT lockedRectOriginal;
			if(!SUCCEEDED(mPTexture->LockRect(mipIdx, &lockedRectOriginal, NULL, D3DLOCK_READONLY )))
			{
				newTexture->Release();
				return false;
			}    

			D3DLOCKED_RECT lockedRectNew;
			if(!SUCCEEDED(newTexture->LockRect(mipIdx, &lockedRectNew, NULL, NULL )))
			{
				newTexture->Release();
				return false;
			}  

			float* sourceBuffer = (float*)lockedRectOriginal.pBits;
			float* destBuffer = (float*)lockedRectNew.pBits;
			int pixelPitch = lockedRectOriginal.Pitch / sizeof(float);
			for(unsigned int y=0;y<mipDesc.Height; y++)
			{
				for(unsigned int x=0;x<mipDesc.Width; x++)
				{
					int location =  y * pixelPitch + x * RGBA_COLOR_CHANNELS_PER_PIXEL;
					convertor->Convert( &(sourceBuffer[location]) ,
						&(destBuffer[ location ]),
						pOpts );
				}
			}
			convertor->End();

			mPTexture->UnlockRect(mipIdx);
			newTexture->UnlockRect(mipIdx);
		}

		//Set the internal texture storage to the newly created and processed texture
		releaseTexture();
		
		mPTexture = newTexture;

		return true;
	}

	//-----------------------------------------------------------------------------
	bool TextureConvert::removeAlphaFromA8R8G8B8()
	{
		if (mPTexture==NULL)
		{
			return false;
		}

		D3DSURFACE_DESC desc;
		getDescription(desc);

		if (desc.Format!=D3DFMT_A8R8G8B8)
		{
			return false;
		}

		D3DLOCKED_RECT lockedRectOriginal;
		if(!SUCCEEDED(mPTexture->LockRect(0, &lockedRectOriginal, NULL, D3DLOCK_READONLY )))
		{
			return false;
		}    

		LPDIRECT3DTEXTURE9 newTexture=NULL;
		D3DXCreateTexture(mPD3DDevice, desc.Width, desc.Height, 1, D3DUSAGE_DYNAMIC, 
			D3DFMT_X8B8G8R8, D3DPOOL_SYSTEMMEM , &newTexture);
		if (newTexture==NULL)
		{
			return false;
		}

		D3DLOCKED_RECT lockedRectNew;
		if(!SUCCEEDED(newTexture->LockRect(0, &lockedRectNew, NULL, D3DLOCK_DISCARD )))
		{
			return false;
		}  

		DWORD* sourceBuffer = (DWORD*)lockedRectOriginal.pBits;
		DWORD* destBuffer = (DWORD*)lockedRectNew.pBits;
		for(unsigned int y=0;y<desc.Height; y++)
		{
			for(unsigned int x=0;x<desc.Width; x++)
			{
				destBuffer[(y*desc.Width+x)] = sourceBuffer[(y*desc.Width+x)] & 0x00ffffff;
			}
		}

		mPTexture->UnlockRect(0);
		newTexture->UnlockRect(0);

		releaseTexture();

		mPTexture = newTexture;

		return true;
	}

	//-----------------------------------------------------------------------------
	bool TextureConvert::convertAlphaFromA8R8G8B8IntoLuminance(int width, int height)
	{
		printf("TextureConvert::convertAlphaFromA8R8G8B8IntoLuminance(%d, %d)\n", width, height);
		if (mPTexture==NULL)
		{
			return false;
		}

		D3DSURFACE_DESC desc;
		getDescription(desc);

		if (desc.Format!=D3DFMT_A8R8G8B8)
		{
			return false;
		}

		D3DLOCKED_RECT lockedRectOriginal;
		if(!SUCCEEDED(mPTexture->LockRect(0, &lockedRectOriginal, NULL, D3DLOCK_READONLY )))
		{
			return false;
		}    

		LPDIRECT3DTEXTURE9 newTexture=NULL;
		D3DXCreateTexture(mPD3DDevice, width, height, 1, D3DUSAGE_DYNAMIC, 
			D3DFMT_L8, D3DPOOL_SYSTEMMEM , &newTexture);
		if (newTexture==NULL)
		{
			return false;
		}

		D3DLOCKED_RECT lockedRectNew;
		if(!SUCCEEDED(newTexture->LockRect(0, &lockedRectNew, NULL, D3DLOCK_DISCARD )))
		{
			return false;
		}  

		DWORD* sourceBuffer = (DWORD*)lockedRectOriginal.pBits;
		BYTE* destBuffer = (BYTE*)lockedRectNew.pBits;
		memset(destBuffer, 0, width * height);
		for(unsigned int y=0;y<desc.Height; y++)
		{
			for(unsigned int x=0;x<desc.Width; x++)
			{
				BYTE l8=static_cast<BYTE>((sourceBuffer[(y*desc.Width+x)] >>24 )& 0xff);
				int xx=static_cast<int>(x*width/desc.Width);
				int yy=static_cast<int>(y*height/desc.Height);
				destBuffer[(yy*width+xx)] = max(l8,destBuffer[(yy*width+xx)]);
			}
		}

		mPTexture->UnlockRect(0);
		newTexture->UnlockRect(0);

		releaseTexture();

		mPTexture = newTexture;

		return true;
	}
	//-----------------------------------------------------------------------------
	void TextureConvert::reduceExponentFromRGB(float redf, float greenf, float bluef, 
		int alphaExp,int &red, int &green, int &blue)
	{
		float exponentVal = powf(2.0f, (( ((float)alphaExp)/255.0f)*(maxExponent-minExponent)) + minExponent  );
		redf/=exponentVal;
		greenf/=exponentVal;
		bluef/=exponentVal;

		red = max(0, min(255,(int) (redf*255.0f) ));
		green = max(0, min(255, (int) (greenf*255.0f) ));
		blue = max(0, min(255, (int) (bluef*255.0f) ));
	}

	//-----------------------------------------------------------------------------
	void TextureConvert::calculateRGBE(float redf, float greenf, float bluef, 
		int &red, int &green, int &blue, int &alpha)
	{
		//float brightness = sqrt(redf*redf+greenf*greenf+bluef*bluef);
		//float brightness = (redf+greenf+bluef)/3.0f;
		float brightness = max(max(redf, greenf),bluef);
		int alphaExp = getExponent(brightness);
		reduceExponentFromRGB(redf, greenf, bluef, alphaExp, red, green, blue);
		alpha = alphaExp;
	}

	//-----------------------------------------------------------------------------
	int TextureConvert::getExponent(float brightness)
	{
		float exponent = static_cast<float>((log(static_cast<double>(brightness))/log(2.0)));
		float exponentBiased = ((exponent-minExponent)/(maxExponent-minExponent)  )*255.0f;
		int alphaExp = (int) ceil(exponentBiased);
		if (alphaExp>255) 
		{
			alphaExp = 255;
		}
		else if (alphaExp<0)
		{
			alphaExp = 0;
		}
		return alphaExp;
	}

	//-----------------------------------------------------------------------------	
	void TextureConvert::calculateRGBfromRGBE(int red, int green, int blue, int alphaExp,
		float &redf, float &greenf, float &bluef)
	{
		bluef = blue / 255.0f;
		greenf= green / 255.0f;
		redf  = red / 255.0f;

		float exponentVal = powf(2.0f, (( ((float)alphaExp)/ 255.0f)*(maxExponent-minExponent)) + minExponent  );
		bluef*=exponentVal;
		greenf*=exponentVal;
		redf*=exponentVal;
	}
	void Plot( float* textureBuffer, int x, int y, int pixelPitch, int height )
	{
		x = Clamp( x, 0,pixelPitch-1);
		y = Clamp( y, 0, height-1);
		textureBuffer[y*pixelPitch+x*RGBA_COLOR_CHANNELS_PER_PIXEL+0]=1.0f;
		textureBuffer[y*pixelPitch+x*RGBA_COLOR_CHANNELS_PER_PIXEL+1]=0.0f;
		textureBuffer[y*pixelPitch+x*RGBA_COLOR_CHANNELS_PER_PIXEL+2]=0.0f;
		textureBuffer[y*pixelPitch+x*RGBA_COLOR_CHANNELS_PER_PIXEL+3]=1.0f;
	}
	//-----------------------------------------------------------------------------
	bool TextureConvert::calculateRelevantClipArea(LPDIRECT3DTEXTURE9 pTexture, int atlasX, int atlasY, bool verbose, float threshold, const char* outname)
	{
		struct TileInfo
		{
			int maxX;
			int maxY;
			int minX;
			int minY;
			int sx;
			int sy;
		};
		std::vector<TileInfo> box;
		if(pTexture==NULL)
		{
			return false;
		}

		D3DSURFACE_DESC desc;
		getDescription(desc, pTexture, 0);

		if (desc.Format!=D3DFMT_A32B32G32R32F)
		{
			return false;
		}

		TextureConvert::AlphaCheckResult alphaCheckRes = this->hasAlpha();
		bool hasAlpha = ((alphaCheckRes == TextureConvert::RTC_HASMULTIBITALPHA) || (alphaCheckRes == TextureConvert::RTC_HASONEBITALPHA)) ? true : false;

		D3DLOCKED_RECT lockedRectTexture;
		if(!SUCCEEDED(pTexture->LockRect(0, &lockedRectTexture, NULL, D3DLOCK_READONLY )))
		{
			return false;
		}
		float* textureBuffer = (float*)lockedRectTexture.pBits;
		int pixelPitch = lockedRectTexture.Pitch / sizeof(float);
	
		// Walk the texture
		float areaSaved = 0.0f;
		float areaTotal = 0.0f;

		float twidth = (float)desc.Width/(float)atlasX;
		float theight = (float)desc.Height/(float)atlasY;
		int height = desc.Height;

		
		++RAGE_LOG_DISABLE;
		std::ofstream out;
		--RAGE_LOG_DISABLE;

		if ( verbose )
		{
			++RAGE_LOG_DISABLE;
			std::string outnameDes = outname;
			outnameDes +=".clipregions";
			out.open( outnameDes.c_str());
			out << atlasX << " ";
			out << atlasY << " ";
			--RAGE_LOG_DISABLE;
		}
		int  frame = 0;
		for ( int i = 0;i < atlasY; i++)
		{
			for (int j = 0; j < atlasX; j++)
			{
				int minX, maxX, minY, maxY;
				bool foundNonTransparentPixels = false;
				// calculate starting positions
				
				
				int sy =  (int)floor( (float)i * theight);
				int ey =  (int)floor( (float)(i+1) * theight);
				int sx =  (int)floor( (float)j * twidth);
				int ex =  (int)floor( (float)(j+1) * twidth);

				maxX = maxY = 0;
				minY = (desc.Height);
				minX = (desc.Width);

				for( int y=sy;y<ey; y++)
				{
					for( int x=sx;x<ex; x++)
					{
						if (hasAlpha == true)
						{
							float alphaf = ((float)textureBuffer[y*pixelPitch+x*RGBA_COLOR_CHANNELS_PER_PIXEL+3]) / 255.0f;

							if (alphaf > threshold)
							{
								maxX = Max( x, maxX);
								maxY = Max( y, maxY);
								minX = Min( x, minX );
								minY = Min( y, minY );
								foundNonTransparentPixels = true;
							}
						}
						else
						{
							float redf = ((float)textureBuffer[y*pixelPitch+x*RGBA_COLOR_CHANNELS_PER_PIXEL+0]) / 255.0f;
							float greenf = ((float)textureBuffer[y*pixelPitch+x*RGBA_COLOR_CHANNELS_PER_PIXEL+1]) / 255.0f;
							float bluef = ((float)textureBuffer[y*pixelPitch+x*RGBA_COLOR_CHANNELS_PER_PIXEL+2]) / 255.0f;

							float maxf = Max( redf, Max( greenf, bluef) );
							if (maxf > 0.0f)
							{
								maxX = Max( x, maxX);
								maxY = Max( y, maxY);
								minX = Min( x, minX );
								minY = Min( y, minY );
								foundNonTransparentPixels = true;
							}
						}
					}
				}

				int pad= 1;

				minX -=sx + pad;
				maxX -=sx - pad;

				minY -=sy + pad;
				maxY -=sy - pad;

				minX = Clamp( minX, 0, (int)twidth);
				maxX = Clamp( maxX, 0, (int)twidth);
				minY = Clamp( minY, 0, (int)theight );
				maxY = Clamp( maxY, 0, (int)theight);

				float textureWidth = twidth;
				float textureHeight = theight;
				float fminX = (float)minX / textureWidth;
				float fminY = (float)minY / textureHeight;
				float fmaxX = (float)(maxX+1) / textureWidth;
				float fmaxY = (float)(maxY+1) / textureHeight;
				fminX = Clamp( fminX, 0.0f, 0.5f );
				fminY = Clamp( fminY, 0.0f, 0.5f );
				fmaxX = Clamp( fmaxX, 0.5f, 1.0f );
				fmaxY = Clamp( fmaxY, 0.5f, 1.0f );

				// flip the Y component
				float tminY = 1.0f - fmaxY;
				float tmaxY = 1.0f - fminY;
				fminY = tminY;
				fmaxY = tmaxY;

				if (!foundNonTransparentPixels)
				{
					fminX = fmaxX = fminY = fmaxY = 0.5f;
				}
			
				Displayf("Relevant texture area: %f %f %f %f", fminX, fmaxX, fminY, fmaxY);
				Displayf("^ these values are (min X, max X, min Y, max Y) as fractionals.");

				areaTotal +=1.0f;
				float newArea = ( fmaxX-fminX) * ( fmaxY - fminY);
				areaSaved += newArea;
				frame++;

				if (verbose)
				{
					Displayf("Atlas Tile %i %i ", i, j);
					++RAGE_LOG_DISABLE;
					out << fminX << " ";
					out << fmaxX << " ";
					out << fminY << " ";
					out << fmaxY << " ";
					if ( newArea > 0 )
					{
						box.push_back();
						box.back().maxX = maxX;
						box.back().maxY = maxY;
						box.back().minX =minX;
						box.back().minY = minY;
						box.back().sx = sx;
						box.back().sy = sy;
					}
					--RAGE_LOG_DISABLE;
				}
			}
		}

		if ( verbose)
		{
			for ( unsigned int j = 0; j < box.size(); j++)
			{
				int maxX = box[j].maxX;
				int maxY = box[j].maxY;
				int minX = box[j].minX;
				int minY = box[j].minY;	
				int sx = box[j].sx;
				int sy = box[j].sy;
				
				// draw box
				for ( int i = minX; i < maxX; i++ )
				{
					int y=  minY + sy;
					int x = i + sx;
					Plot( textureBuffer, x, y, pixelPitch, height );
					y=  maxY + sy;
					Plot( textureBuffer, x, y, pixelPitch, height );
				}
				for ( int i = minY; i < maxY; i++ )
				{
					int y=  i + sy;
					int x = minX + sx;
					Plot( textureBuffer, x, y, pixelPitch, height  );
					x=  maxX + sx;
					Plot( textureBuffer, x, y, pixelPitch, height );
				}
			}
		}

		if ( verbose)
		{
			Displayf("Total Final Area %1.4f ", areaSaved/ areaTotal );
		}
		pTexture->UnlockRect(0);

		return true;
	}

	//-----------------------------------------------------------------------------
	bool TextureConvert::computeHDRExponentOffset(LPDIRECT3DTEXTURE9 pTexture, float exposure,
												  float outExpR[TCConvertOptions::NUM_GAMMA_SPACES], float outExpG[TCConvertOptions::NUM_GAMMA_SPACES], float outExpB[TCConvertOptions::NUM_GAMMA_SPACES],
												  float outOffR[TCConvertOptions::NUM_GAMMA_SPACES], float outOffG[TCConvertOptions::NUM_GAMMA_SPACES], float outOffB[TCConvertOptions::NUM_GAMMA_SPACES])
	{
		if (pTexture==NULL)
		{
			return false;
		}

		D3DSURFACE_DESC desc;
		getDescription(desc, pTexture, 0);

		if (desc.Format!=D3DFMT_A32B32G32R32F)
		{
			return false;
		}

		D3DLOCKED_RECT lockedRectTexture;
		if(!SUCCEEDED(pTexture->LockRect(0, &lockedRectTexture, NULL, D3DLOCK_READONLY )))
		{
			return false;
		}
		float* textureBuffer = (float*)lockedRectTexture.pBits;
		int pixelPitch = lockedRectTexture.Pitch / sizeof(float);

		//Get the minimum and maximum R,G, and B values
		float maxClrR,maxClrG,maxClrB;
		maxClrR = maxClrG = maxClrB = FLT_MIN;
		
		float minClrR,minClrG,minClrB;
		minClrR = minClrG = minClrB = FLT_MAX;

		for(unsigned int y=0;y<desc.Height; y++)
		{
			for(unsigned int x=0;x<desc.Width; x++)
			{
				float redf		= textureBuffer[y*pixelPitch+x*RGBA_COLOR_CHANNELS_PER_PIXEL+0];
				float greenf	= textureBuffer[y*pixelPitch+x*RGBA_COLOR_CHANNELS_PER_PIXEL+1];
				float bluef		= textureBuffer[y*pixelPitch+x*RGBA_COLOR_CHANNELS_PER_PIXEL+2];
				
				if(redf > maxClrR) maxClrR = redf;
				if(greenf > maxClrG) maxClrG = greenf;
				if(bluef > maxClrB) maxClrB = bluef;

				if(redf < minClrR) minClrR = redf;
				if(greenf < minClrG) minClrG = greenf;
				if(bluef < minClrB) minClrB = bluef;
			}
		}

		pTexture->UnlockRect(0);

		//Compute the sRGB exponent and offset
		float expAdjVal = (pow(2.0f,-exposure));

		outExpR[TCConvertOptions::SRGB_EXPOSED] = ( maxClrR - minClrR ) * expAdjVal;
		outExpG[TCConvertOptions::SRGB_EXPOSED] = ( maxClrG - minClrG ) * expAdjVal;
		outExpB[TCConvertOptions::SRGB_EXPOSED] = ( maxClrB - minClrB ) * expAdjVal;

		outOffR[TCConvertOptions::SRGB_EXPOSED] = ( minClrR * expAdjVal );
		outOffG[TCConvertOptions::SRGB_EXPOSED] = ( minClrG * expAdjVal );
		outOffB[TCConvertOptions::SRGB_EXPOSED] = ( minClrB * expAdjVal );

		//Check to make sure the exponent isn't 0.0, if it is then there is no need to expand the range in that channel
		if( outExpR[TCConvertOptions::SRGB_EXPOSED] == 0.0f )
		{
			outExpR[TCConvertOptions::SRGB_EXPOSED] = 1.0f;
			outOffR[TCConvertOptions::SRGB_EXPOSED] = 0.0f;
		}

		if( outExpG[TCConvertOptions::SRGB_EXPOSED] == 0.0f )
		{
			outExpG[TCConvertOptions::SRGB_EXPOSED] = 1.0f;
			outOffG[TCConvertOptions::SRGB_EXPOSED] = 0.0f;
		}

		if( outExpB[TCConvertOptions::SRGB_EXPOSED] == 0.0f )
		{
			outExpB[TCConvertOptions::SRGB_EXPOSED] = 1.0f;
			outOffB[TCConvertOptions::SRGB_EXPOSED] = 0.0f;
		}


		outExpR[TCConvertOptions::SRGB] = maxClrR - minClrR;
		outExpG[TCConvertOptions::SRGB] = maxClrG - minClrG;
		outExpB[TCConvertOptions::SRGB] = maxClrB - minClrB;

		outOffR[TCConvertOptions::SRGB] = minClrR;
		outOffG[TCConvertOptions::SRGB] = minClrG;
		outOffB[TCConvertOptions::SRGB] = minClrB;

		//Check to make sure the exponent isn't 0.0, if it is then there is no need to expand the range in that channel
		if( outExpR[TCConvertOptions::SRGB] == 0.0f )
		{
			outExpR[TCConvertOptions::SRGB] = 1.0f;
			outOffR[TCConvertOptions::SRGB] = 0.0f;
		}

		if( outExpG[TCConvertOptions::SRGB] == 0.0f )
		{
			outExpG[TCConvertOptions::SRGB] = 1.0f;
			outOffG[TCConvertOptions::SRGB] = 0.0f;
		}

		if( outExpB[TCConvertOptions::SRGB] == 0.0f )
		{
			outExpB[TCConvertOptions::SRGB] = 1.0f;
			outOffB[TCConvertOptions::SRGB] = 0.0f;
		}

		//Convert the min and max colors to gamma 1.0
		float linMaxClrR = RTCGammaConvert_22To10(maxClrR);
		float linMaxClrG = RTCGammaConvert_22To10(maxClrG);
		float linMaxClrB = RTCGammaConvert_22To10(maxClrB);

		float linMinClrR = RTCGammaConvert_22To10(minClrR);
		float linMinClrG = RTCGammaConvert_22To10(minClrG);
		float linMinClrB = RTCGammaConvert_22To10(minClrB);

		//Expose the min and max linear space colors

		//Compute the scale (exponent)
		outExpR[TCConvertOptions::LINEAR_EXPOSED] = ( linMaxClrR - linMinClrR ) * expAdjVal;
		outExpG[TCConvertOptions::LINEAR_EXPOSED] = ( linMaxClrG - linMinClrG ) * expAdjVal;
		outExpB[TCConvertOptions::LINEAR_EXPOSED] = ( linMaxClrB - linMinClrB ) * expAdjVal;

		//Compute the offset
		outOffR[TCConvertOptions::LINEAR_EXPOSED] = ( linMinClrR * expAdjVal );
		outOffG[TCConvertOptions::LINEAR_EXPOSED] = ( linMinClrG * expAdjVal );
		outOffB[TCConvertOptions::LINEAR_EXPOSED] = ( linMinClrB * expAdjVal );

		//Check to make sure the exponent isn't 0.0, if it is then there is no need to expand the range in that channel
		if( outExpR[TCConvertOptions::LINEAR_EXPOSED] == 0.0f )
		{
			outExpR[TCConvertOptions::LINEAR_EXPOSED] = 1.0f;
			outOffR[TCConvertOptions::LINEAR_EXPOSED] = 0.0f;
		}

		if( outExpG[TCConvertOptions::LINEAR_EXPOSED] == 0.0f )
		{
			outExpG[TCConvertOptions::LINEAR_EXPOSED] = 1.0f;
			outOffG[TCConvertOptions::LINEAR_EXPOSED] = 0.0f;
		}

		if( outExpB[TCConvertOptions::LINEAR_EXPOSED] == 0.0f )
		{
			outExpB[TCConvertOptions::LINEAR_EXPOSED] = 1.0f;
			outOffB[TCConvertOptions::LINEAR_EXPOSED] = 0.0f;
		}

		//Compute the scale (exponent)
		outExpR[TCConvertOptions::LINEAR] = linMaxClrR - linMinClrR;
		outExpG[TCConvertOptions::LINEAR] = linMaxClrG - linMinClrG;
		outExpB[TCConvertOptions::LINEAR] = linMaxClrB - linMinClrB;

		//Compute the offset
		outOffR[TCConvertOptions::LINEAR] = linMinClrR;
		outOffG[TCConvertOptions::LINEAR] = linMinClrG;
		outOffB[TCConvertOptions::LINEAR] = linMinClrB;

		//Check to make sure the exponent isn't 0.0, if it is then there is no need to expand the range in that channel
		if( outExpR[TCConvertOptions::LINEAR] == 0.0f )
		{
			outExpR[TCConvertOptions::LINEAR] = 1.0f;
			outOffR[TCConvertOptions::LINEAR] = 0.0f;
		}

		if( outExpG[TCConvertOptions::LINEAR] == 0.0f )
		{
			outExpG[TCConvertOptions::LINEAR] = 1.0f;
			outOffG[TCConvertOptions::LINEAR] = 0.0f;
		}

		if( outExpB[TCConvertOptions::LINEAR] == 0.0f )
		{
			outExpB[TCConvertOptions::LINEAR] = 1.0f;
			outOffB[TCConvertOptions::LINEAR] = 0.0f;
		}

		return true;
	}
	//-----------------------------------------------------------------------------
	bool TextureConvert::buildAndProcessMipsD3DX(LPDIRECT3DTEXTURE9 &oldTexture, 
												TCConvertOptions& tcOpt,
												LPDIRECT3DTEXTURE9 *newTexture)
	{
		return convertToFormatInternalD3DX(oldTexture, tcOpt, newTexture);
	}

	bool TextureConvert::buildAndProcessMips(TCConvertOptions& tcOpt, LPDIRECT3DTEXTURE9 *pTexture)
	{
		LPDIRECT3DTEXTURE9 newTexture=NULL;

		if(!pTexture)
			pTexture = &mPTexture;
		Assert(pTexture);

		bool bRetVal;
#if USE_NVDXT
		bRetVal = buildAndProcessMipsNVDXT(*pTexture, tcOpt, &newTexture);
#else
		bRetVal = buildAndProcessMipsD3DX(*pTexture, tcOpt, &newTexture);
#endif //USE_NVDXT
		
		if(bRetVal)
		{
			//Release the input texture
			while( (*pTexture)->Release())
				;

			//Assign the new texture to the input texture
			(*pTexture) = newTexture;

			return true;
		}
		else
			return false;
	}

	bool TextureConvert::convertToFormatInternalD3DX(LPDIRECT3DTEXTURE9 &oldTexture, 
													 TCConvertOptions& tcOpt,
													 LPDIRECT3DTEXTURE9 *newTexture)
	{
		HRESULT hr;
		LPD3DXBUFFER pMemBuffer=NULL;

		hr = D3DXSaveTextureToFileInMemory(&pMemBuffer, D3DXIFF_DDS, oldTexture, NULL);
		if (pMemBuffer==NULL)
		{
			return false;
		}

		DWORD d3dMipFilter;

		switch(tcOpt.MipFilter)
		{
		case TCConvertOptions::MIPFILTER_BOX:
			d3dMipFilter = D3DX_FILTER_BOX;
			break;
		case TCConvertOptions::MIPFILTER_POINT:
			d3dMipFilter = D3DX_FILTER_POINT;
			break;
		case TCConvertOptions::MIPFILTER_TRIANGLE:
			d3dMipFilter = D3DX_FILTER_TRIANGLE;
			break;
		default:
			{
				int oldMipCount = oldTexture->GetLevelCount();
				if( oldMipCount != tcOpt.MipLevels )
				{
					Warningf("The mip-filter specified can't be used by D3DX, defaulting to a box mip filter.");
				}
				d3dMipFilter = D3DX_FILTER_BOX;
			}
		}

		hr = D3DXCreateTextureFromFileInMemoryEx(mPD3DDevice, 
			pMemBuffer->GetBufferPointer(),
			pMemBuffer->GetBufferSize(),
			tcOpt.Width,
			tcOpt.Height,
			tcOpt.MipLevels,
			0,
			(D3DFORMAT)tcOpt.Format,
			D3DPOOL_SYSTEMMEM,
			D3DX_DEFAULT,
			d3dMipFilter,
			0,
			NULL,
			NULL,
			newTexture);

		if (pMemBuffer!=NULL)
		{
			pMemBuffer->Release();
			pMemBuffer=NULL;
		}
		
		if ((*newTexture==NULL) || (!SUCCEEDED(hr)))
		{
			return false;
		}

		return true;
	}

	//-----------------------------------------------------------------------------
	bool TextureConvert::convertToFormat(TCConvertOptions& tcOpt, LPDIRECT3DTEXTURE9 *pTexture)
	{
		LPDIRECT3DTEXTURE9 newTexture=NULL;
		
		if(!pTexture)
			pTexture = &mPTexture;
		Assert(pTexture);

		if(!tcOpt.SharpenMips && !tcOpt.FadeMips)
		{
			if (!convertToFormatInternalD3DX(*pTexture, tcOpt, &newTexture))
			{
				return false;
			}
		}
		else
		{
#if USE_NVDXT
			//For now just use the NVDXT path if we need to perform mip-map sharpening or mip-fading
			if (!convertToFormatInternalNVDXT(*pTexture, tcOpt, &newTexture))
			{
				return false;
			}
#else
			Errorf("Error: Mip-sharpening has been specified, but the tool was not built with that feature enabled.");
			return false;
#endif //USE_NVDXT
		}
		
		//Release the input texture
		int i=1;
		while( i )
			i = (*pTexture)->Release();

		//Assign the new texture to the input texture
		(*pTexture) = newTexture;

		return true;
	}
	//-----------------------------------------------------------------------------
#if USE_NVDXT
	bool TextureConvert::convertCubeMapToFormat(TCConvertOptions& tcOpt, LPDIRECT3DCUBETEXTURE9 *pTexture)
	{
		LPDIRECT3DCUBETEXTURE9 newTexture = NULL;
		if(!pTexture)
			pTexture = &mPCubeTexture;
		Assert(pTexture);

		if(!convertToFormatInternalNVDXT(*pTexture, tcOpt, &newTexture))
		{
			return false;
		}
		
		//Release the input texture
		while( (*pTexture)->Release())
			;

		//Assign the new texture to the input texture
		(*pTexture) = newTexture;

		return true;
	}

	//-----------------------------------------------------------------------------
	static NV_ERROR_CODE NVDXTWriteMipCallback(const void* buffer,
													size_t count, 
													const MIPMapData* mipMapData,
													void* userData)
	{
		HRESULT hr;

		if(!mipMapData)
			return NV_OK;

		int mipLevel = mipMapData->mipLevel;

		LPDIRECT3DTEXTURE9 pDstTexture = static_cast<LPDIRECT3DTEXTURE9>(userData);

		if(pDstTexture->GetLevelCount() <= (unsigned)mipLevel) //skip unwanted mips
			return NV_OK;

		D3DSURFACE_DESC dstDesc;
		pDstTexture->GetLevelDesc(mipLevel, &dstDesc);

		D3DLOCKED_RECT dstLR;
		hr = pDstTexture->LockRect(mipLevel, &dstLR, NULL, 0);
		{
			if(FAILED(hr))
			{
				pDstTexture->Release();
				return NV_FAIL;
			}

			memcpy(dstLR.pBits, buffer, count);
		}

		hr = pDstTexture->UnlockRect(mipLevel);
		
		return NV_OK;
	}
	//-----------------------------------------------------------------------------
	static NV_ERROR_CODE NVDXTWrite8BitMipCallback(const void* buffer,
												   size_t /*count*/, 
												   const MIPMapData* mipMapData,
												   void* userData)
	{
		HRESULT hr;

		if(!mipMapData)
			return NV_OK;

		int mipLevel = mipMapData->mipLevel;

		LPDIRECT3DTEXTURE9 pDstTexture = static_cast<LPDIRECT3DTEXTURE9>(userData);

		if(pDstTexture->GetLevelCount() <= (unsigned)mipLevel) //skip unwanted mips
			return NV_OK;

		D3DSURFACE_DESC dstDesc;
		pDstTexture->GetLevelDesc(mipLevel, &dstDesc);

		D3DLOCKED_RECT dstLR;
		hr = pDstTexture->LockRect(mipLevel, &dstLR, NULL, 0);
		{
			if(FAILED(hr))
			{
				pDstTexture->Release();
				return NV_FAIL;
			}

			unsigned char* srcBuffer = (unsigned char*)buffer;
			float* dstBuffer = (float*)dstLR.pBits;
			
			int srcPixelPitch = mipMapData->width * 4;
			int dstPixelPitch = dstLR.Pitch / sizeof(float);

			for(unsigned int y=0;y<mipMapData->height; y++)
			{
				for(unsigned int x=0;x<mipMapData->width; x++)
				{
					float redf = ((float)srcBuffer[y * srcPixelPitch + x * RGBA_COLOR_CHANNELS_PER_PIXEL + 2]) / 255.0f;
					float greenf = ((float)srcBuffer[y * srcPixelPitch + x * RGBA_COLOR_CHANNELS_PER_PIXEL + 1]) / 255.0f;
					float bluef = ((float)srcBuffer[y * srcPixelPitch + x * RGBA_COLOR_CHANNELS_PER_PIXEL + 0]) / 255.0f;
					float alphaf = ((float)srcBuffer[y * srcPixelPitch + x * RGBA_COLOR_CHANNELS_PER_PIXEL + 3]) / 255.0f;

					dstBuffer[y * dstPixelPitch + x * RGBA_COLOR_CHANNELS_PER_PIXEL + 0] = redf;
					dstBuffer[y * dstPixelPitch + x * RGBA_COLOR_CHANNELS_PER_PIXEL + 1] = greenf;
					dstBuffer[y * dstPixelPitch + x * RGBA_COLOR_CHANNELS_PER_PIXEL + 2] = bluef;
					dstBuffer[y * dstPixelPitch + x * RGBA_COLOR_CHANNELS_PER_PIXEL + 3] = alphaf;
				}
			}

		}

		hr = pDstTexture->UnlockRect(mipLevel);
		
		return NV_OK;
	}
	//-----------------------------------------------------------------------------
	static NV_ERROR_CODE NVDXTWriteCubeMipCallback(const void* buffer,
													size_t count, 
													const MIPMapData* mipMapData,
													void* userData)
	{
		HRESULT hr;

		if(!mipMapData)
			return NV_OK;

		LPDIRECT3DCUBETEXTURE9 pDstTexture = static_cast<LPDIRECT3DCUBETEXTURE9>(userData);

		D3DSURFACE_DESC dstDesc;
		pDstTexture->GetLevelDesc(mipMapData->mipLevel, &dstDesc);

		D3DLOCKED_RECT dstLR;
		hr = pDstTexture->LockRect((D3DCUBEMAP_FACES)mipMapData->faceNumber, mipMapData->mipLevel, &dstLR, NULL, 0);
		{
			if(FAILED(hr))
			{
				pDstTexture->Release();
				return NV_FAIL;
			}
			memcpy(dstLR.pBits, buffer, count);
		}
		hr = pDstTexture->UnlockRect((D3DCUBEMAP_FACES)mipMapData->faceNumber, mipMapData->mipLevel);

		return NV_OK;
	}

	//-----------------------------------------------------------------------------
	bool TextureConvert::buildAndProcessMipsNVDXT(LPDIRECT3DTEXTURE9 &oldTexture, 
												  TCConvertOptions& tcOpt,
												  LPDIRECT3DTEXTURE9 *newTexture)
	{
		//Make sure our input is in the proper format
		D3DSURFACE_DESC oldTextureDesc;
		LPDIRECT3DSURFACE9 oldTextureSurface;
		oldTexture->GetSurfaceLevel(0, &oldTextureSurface);
		oldTextureSurface->GetDesc(&oldTextureDesc);
		oldTexture->Release();
		Assertf( oldTextureDesc.Format == D3DFMT_A32B32G32R32F, "Source texture input is not in formatted as D3DFMT_A32B32G32R32F");

		int srcMipLevels = oldTexture->GetLevelCount();

		//Allocate the new texture
		D3DXCreateTexture(mPD3DDevice, oldTextureDesc.Width, oldTextureDesc.Height, tcOpt.MipLevels, D3DUSAGE_DYNAMIC, 
			D3DFMT_A32B32G32R32F, D3DPOOL_SYSTEMMEM , newTexture);
		if (newTexture==NULL)
			return false;

		int dstMipLevels = (*newTexture)->GetLevelCount();

//#ifdef _DEBUG
#if 0
		//HACK!
		//In debug builds, the nvDXT libraries will crash with an _isnan assertion when creating mips below 8x8 
		//for textures that are 8x8 in size or less, so if we are in a debug build, then just cap the mip-levels
		//that the nvDXT libraries work with at 8x8 or above.
		int tmpMipLevels = 0;
		for(int i=0; i<dstMipLevels; i++)
		{
			D3DSURFACE_DESC tmpDsc;
			(*newTexture)->GetLevelDesc(i, &tmpDsc);
			if( (tmpDsc.Width < 8) || (tmpDsc.Height < 8) )
				break;
			else
				tmpMipLevels++;
		}
		dstMipLevels = tmpMipLevels;
#endif //_DEBUG

		nvCompressionOptions nvCmpOpt;

#if NVDXT_USE_FLOAT_TEXTURE
		fpMipMappedImage nvFpTexture;
		ConvertD3DTextureToNVDXTFloatTexture(oldTexture, nvFpTexture);
		nvCmpOpt.SetTextureFormat(kTextureTypeTexture2D, kA32B32G32R32F );
#else
		RGBAMipMappedImage nvRgbaTexture;
		ConvertD3DTextureToNVDXTRgbaTexture(oldTexture, nvRgbaTexture);
		nvCmpOpt.SetTextureFormat(kTextureTypeTexture2D, k8888);
#endif //NVDXT_USE_FLOAT_TEXTURE
		
		if(srcMipLevels <= 1)
			nvCmpOpt.GenerateMIPMaps(dstMipLevels);
		else if( srcMipLevels == dstMipLevels )
			nvCmpOpt.UseExisitingMIPMaps();
		else
			nvCmpOpt.CompleteMIPMapChain(dstMipLevels);

		//Setup the mip generation filter
		switch(tcOpt.MipFilter)
		{
		case TCConvertOptions::MIPFILTER_BOX:
			nvCmpOpt.mipFilterType = kMipFilterBox;
			break;
		case TCConvertOptions::MIPFILTER_CATROM:
			nvCmpOpt.mipFilterType = kMipFilterCatrom;
			break;
		case TCConvertOptions::MIPFILTER_CUBIC:
			nvCmpOpt.mipFilterType = kMipFilterCubic;
			break;
		case TCConvertOptions::MIPFILTER_MITCHELL:
			nvCmpOpt.mipFilterType = kMipFilterMitchell;
			break;
		case TCConvertOptions::MIPFILTER_POINT:
			nvCmpOpt.mipFilterType = kMipFilterPoint;
			break;
		case TCConvertOptions::MIPFILTER_QUADRATIC:
			nvCmpOpt.mipFilterType = kMipFilterQuadratic;
			break;
		case TCConvertOptions::MIPFILTER_TRIANGLE:
			nvCmpOpt.mipFilterType = kMipFilterTriangle;
			break;
		};

		//Setup the Mip-Sharpening options
		if(tcOpt.SharpenMips)
		{
			switch(tcOpt.SharpenMipFilter)
			{
			case TCConvertOptions::MIPSHARP_SOFT:
				nvCmpOpt.sharpenFilterType = kSharpenFilterSharpenSoft;
				break;
			case TCConvertOptions::MIPSHARP_MEDIUM:
				nvCmpOpt.sharpenFilterType = kSharpenFilterSharpenMedium;
				break;
			case TCConvertOptions::MIPSHARP_HARD:
				nvCmpOpt.sharpenFilterType = kSharpenFilterSharpenStrong;
				break;
			}

			//TODO : Allow for specifying the sharpening passes per mip-level?
			nvCmpOpt.sharpening_passes_per_mip_level[0] = 0;
			for(int i=1; i<MAX_MIP_MAPS; i++)
				nvCmpOpt.sharpening_passes_per_mip_level[i] = 1;
		}

		//Setup the Mip-Fading options
		if(tcOpt.FadeMips)
		{
			nvCmpOpt.FadeColorInMIPMaps( tcOpt.MipFadeToColor.x, tcOpt.MipFadeToColor.y, tcOpt.MipFadeToColor.z );
			nvCmpOpt.SetFadingAsPercentage( tcOpt.MipFadeAmount, tcOpt.MipFadeInLevel );
		}

		//Set the user data pointer to our new texture
		nvCmpOpt.user_data = static_cast<void*>(*newTexture);

		HRESULT hr;
	
		++RAGE_LOG_DISABLE;
		{
#if NVDXT_USE_FLOAT_TEXTURE
			hr = nvDDS::nvDXTcompress( nvFpTexture, &nvCmpOpt, &NVDXTWriteMipCallback, NULL);
#else
			hr = nvDDS::nvDXTcompress( nvRgbaTexture, &nvCmpOpt, &NVDXTWrite8BitMipCallback, NULL);
#endif
		}
		--RAGE_LOG_DISABLE;
		if(SUCCEEDED(hr)) 
			return true;
		else
			return false;
	}

	//-----------------------------------------------------------------------------
	bool TextureConvert::convertToFormatInternalNVDXT(LPDIRECT3DTEXTURE9 &oldTexture, 
													  TCConvertOptions& tcOpt,
													  LPDIRECT3DTEXTURE9 *newTexture)
	{
		static pair<D3DFORMAT, nvTextureFormats> D3DFormatToNVFormatPairs[] =
		{
			pair<D3DFORMAT, nvTextureFormats>(D3DFMT_DXT1, kDXT1),
			pair<D3DFORMAT, nvTextureFormats>(D3DFMT_DXT3, kDXT3),
			pair<D3DFORMAT, nvTextureFormats>(D3DFMT_DXT5, kDXT5),
			pair<D3DFORMAT, nvTextureFormats>(D3DFMT_A32B32G32R32F, kA32B32G32R32F),
			pair<D3DFORMAT, nvTextureFormats>(D3DFMT_A4R4G4B4, k4444),
			pair<D3DFORMAT, nvTextureFormats>(D3DFMT_R5G6B5, k565),
			pair<D3DFORMAT, nvTextureFormats>(D3DFMT_A1R5G5B5, k1555),
			pair<D3DFORMAT, nvTextureFormats>(D3DFMT_A8R8G8B8, k8888),
			pair<D3DFORMAT, nvTextureFormats>(D3DFMT_R8G8B8, k888),
			pair<D3DFORMAT, nvTextureFormats>(D3DFMT_L8, kL8),
			pair<D3DFORMAT, nvTextureFormats>(D3DFMT_A8L8, kA8L8)
		};
		static map<D3DFORMAT, nvTextureFormats> D3DFormatToNVFormatTable(D3DFormatToNVFormatPairs, 
																		 D3DFormatToNVFormatPairs + sizeof(D3DFormatToNVFormatPairs) / sizeof(D3DFormatToNVFormatPairs[0]));

		HRESULT hr;

		//Make sure our input is in the proper format
		D3DSURFACE_DESC oldTextureDesc;
		LPDIRECT3DSURFACE9 oldTextureSurface;
		oldTexture->GetSurfaceLevel(0, &oldTextureSurface);
		oldTextureSurface->GetDesc(&oldTextureDesc);
		oldTexture->Release();
		Assertf( oldTextureDesc.Format == D3DFMT_A32B32G32R32F, "Source texture input is not in formatted as D3DFMT_A32B32G32R32F");

		//Make sure we support the output format
		nvTextureFormats nvOutFmt;
		map<D3DFORMAT, nvTextureFormats>::iterator it = D3DFormatToNVFormatTable.find(tcOpt.Format);
		if(it == D3DFormatToNVFormatTable.end())
		{
			Errorf("Error: Cannot use nvDXT library to convert to the specified format.");
			return false;
		}
		else
		{
			nvOutFmt = it->second;
		}

		//Allocate the new texture
		D3DXCreateTexture(mPD3DDevice, oldTextureDesc.Width, oldTextureDesc.Height, tcOpt.MipLevels, D3DUSAGE_DYNAMIC, 
						 (D3DFORMAT)tcOpt.Format, D3DPOOL_SYSTEMMEM , newTexture);
		if (newTexture==NULL)
			return false;

		//Convert the old texture into a form that can be used by the nvDXT compression libraries
		fpMipMappedImage nvFpTexture;
		ConvertD3DTextureToNVDXTFloatTexture(oldTexture, nvFpTexture);

		//Setup the compression options
		nvCompressionOptions nvCmpOpt;
		nvCmpOpt.SetTextureFormat(kTextureTypeTexture2D, nvOutFmt );
		nvCmpOpt.UseExisitingMIPMaps();

		//Set the user data pointer to our new texture
		nvCmpOpt.user_data = static_cast<void*>(*newTexture);
		
		++RAGE_LOG_DISABLE;
		hr = nvDDS::nvDXTcompress( nvFpTexture, &nvCmpOpt, &NVDXTWriteMipCallback, NULL);
		--RAGE_LOG_DISABLE;
		
		return true;
	}
	//-----------------------------------------------------------------------------
	bool TextureConvert::convertToFormatInternalNVDXT(LPDIRECT3DCUBETEXTURE9 &oldTexture,
													TCConvertOptions& tcOpt,
													LPDIRECT3DCUBETEXTURE9 *newTexture)
	{
		static pair<D3DFORMAT, nvTextureFormats> D3DFormatToNVFormatPairs[] =
		{
			pair<D3DFORMAT, nvTextureFormats>(D3DFMT_DXT1, kDXT1),
			pair<D3DFORMAT, nvTextureFormats>(D3DFMT_DXT3, kDXT3),
			pair<D3DFORMAT, nvTextureFormats>(D3DFMT_DXT5, kDXT5),
			pair<D3DFORMAT, nvTextureFormats>(D3DFMT_A32B32G32R32F, kA32B32G32R32F),
		};
		static map<D3DFORMAT, nvTextureFormats> D3DFormatToNVFormatTable(D3DFormatToNVFormatPairs, 
			D3DFormatToNVFormatPairs + sizeof(D3DFormatToNVFormatPairs) / sizeof(D3DFormatToNVFormatPairs[0]));

		HRESULT hr;

		//Make sure our input is in the proper format
		D3DSURFACE_DESC oldTextureDesc;
		oldTexture->GetLevelDesc(0, &oldTextureDesc);
		Assertf( oldTextureDesc.Format == D3DFMT_A32B32G32R32F, "Source texture input is not in formatted as D3DFMT_A32B32G32R32F");

		//Make sure we support the output format
		nvTextureFormats nvOutFmt;
		map<D3DFORMAT, nvTextureFormats>::iterator it = D3DFormatToNVFormatTable.find(tcOpt.Format);
		if(it == D3DFormatToNVFormatTable.end())
		{
			Errorf("Error: Cannot use nvDXT library to convert to the specified format.");
			return false;
		}
		else
		{
			nvOutFmt = it->second;
		}

		//Allocate the new texture
		D3DXCreateCubeTexture( mPD3DDevice, oldTextureDesc.Width, tcOpt.MipLevels, D3DUSAGE_DYNAMIC, 
							   (D3DFORMAT)tcOpt.Format, D3DPOOL_SYSTEMMEM , newTexture);
		if (newTexture==NULL)
			return false;

		int dstMipLevels = (*newTexture)->GetLevelCount();

		//Convert the old texture into a form that can be used by the nvDXT compression libraries
		fpMipMappedCubeMap nvFpCubeTexture;
		ConvertD3DTextureToNVDXTFloatTexture(oldTexture, nvFpCubeTexture);

		//Setup the NVDXT compression options
		nvCompressionOptions nvCmpOpt;
		nvCmpOpt.SetTextureFormat(kTextureTypeCubeMap, nvOutFmt );
		nvCmpOpt.GenerateMIPMaps(dstMipLevels);

		//TODO : Allow for specifying the mip filter type
		if(tcOpt.MipFilter == D3DX_FILTER_POINT)
			nvCmpOpt.mipFilterType = kMipFilterPoint;
		else
			nvCmpOpt.mipFilterType = kMipFilterBox;

		//Set the user data pointer to our new texture
		nvCmpOpt.user_data = static_cast<void*>(*newTexture);

		++RAGE_LOG_DISABLE;
		hr = nvDDS::nvDXTcompress( nvFpCubeTexture, &nvCmpOpt, &NVDXTWriteCubeMipCallback, NULL);
		--RAGE_LOG_DISABLE;

		return true;
	}
	//-----------------------------------------------------------------------------
	bool TextureConvert::ConvertD3DTextureToNVDXTRgbaTexture(LPDIRECT3DTEXTURE9 pSrcTexture, RGBAMipMappedImage& dstTexture)
	{
		//Get the surface descriptor for the source texture
		D3DSURFACE_DESC srcTextureDesc;
		LPDIRECT3DSURFACE9 srcTextureSurface;
		pSrcTexture->GetSurfaceLevel(0, &srcTextureSurface);
		srcTextureSurface->GetDesc(&srcTextureDesc);
		pSrcTexture->Release();
		Assertf( srcTextureDesc.Format == D3DFMT_A32B32G32R32F, "Source texture input is not in formatted as D3DFMT_A32B32G32R32F");

		//Resize the nv image format to hold the existing texture data
		unsigned int srcMipCount = pSrcTexture->GetLevelCount();
		unsigned int mipWidth = srcTextureDesc.Width;
		unsigned int mipHeight = srcTextureDesc.Height;
		++RAGE_LOG_DISABLE;
		dstTexture.resize(mipWidth, mipHeight, srcMipCount);
		--RAGE_LOG_DISABLE;

		for(unsigned int mipIdx=0; mipIdx<srcMipCount; mipIdx++)
		{
			LPDIRECT3DSURFACE9 pMipSurface;
			pSrcTexture->GetSurfaceLevel(mipIdx, &pMipSurface);
			D3DSURFACE_DESC mipDesc;
			pMipSurface->GetDesc(&mipDesc);
			pSrcTexture->Release();

			mipWidth = mipDesc.Width;
			mipHeight = mipDesc.Height;

			D3DLOCKED_RECT lockedRectTexture;
			if(!SUCCEEDED(pSrcTexture->LockRect(mipIdx, &lockedRectTexture, NULL, D3DLOCK_READONLY )))
			{
				return false;
			}
			
			float* textureBuffer = (float*)lockedRectTexture.pBits;
			RGBAImage& dstMip = dstTexture[mipIdx];
			int pixelPitch = lockedRectTexture.Pitch / sizeof(float);

			for(unsigned int y=0;y<mipHeight; y++)
			{
				for(unsigned int x=0;x<mipWidth; x++)
				{
					unsigned char redc	= (unsigned char)(textureBuffer[y*pixelPitch+x*RGBA_COLOR_CHANNELS_PER_PIXEL+0] * 255.0f);
					unsigned char greenc = (unsigned char)(textureBuffer[y*pixelPitch+x*RGBA_COLOR_CHANNELS_PER_PIXEL+1] * 255.0f);
					unsigned char bluec = (unsigned char)(textureBuffer[y*pixelPitch+x*RGBA_COLOR_CHANNELS_PER_PIXEL+2] * 255.0f);
					unsigned char alphac = (unsigned char)(textureBuffer[y*pixelPitch+x*RGBA_COLOR_CHANNELS_PER_PIXEL+3] * 255.0f);

					rgba_t* dstPixel = dstMip.pixelsXY(x,y);
					dstPixel->set(redc, greenc, bluec, alphac);
				}
			}

			pSrcTexture->UnlockRect(mipIdx);
		}
		return true;
	}
	//-----------------------------------------------------------------------------
	bool TextureConvert::ConvertD3DTextureToNVDXTFloatTexture(LPDIRECT3DTEXTURE9 pSrcTexture, fpMipMappedImage& dstTexture)
	{
		//Get the surface descriptor for the source texture
		D3DSURFACE_DESC srcTextureDesc;
		LPDIRECT3DSURFACE9 srcTextureSurface;
		pSrcTexture->GetSurfaceLevel(0, &srcTextureSurface);
		srcTextureSurface->GetDesc(&srcTextureDesc);
		pSrcTexture->Release();
		Assertf( srcTextureDesc.Format == D3DFMT_A32B32G32R32F, "Source texture input is not in formatted as D3DFMT_A32B32G32R32F");

		//Resize the nv image format to hold the existing texture data
		unsigned int srcMipCount = pSrcTexture->GetLevelCount();
		unsigned int mipWidth = srcTextureDesc.Width;
		unsigned int mipHeight = srcTextureDesc.Height;
		++RAGE_LOG_DISABLE;
		dstTexture.resize(mipWidth, mipHeight, srcMipCount);
		--RAGE_LOG_DISABLE;

		for(unsigned int mipIdx=0; mipIdx<srcMipCount; mipIdx++)
		{
			LPDIRECT3DSURFACE9 pMipSurface;
			pSrcTexture->GetSurfaceLevel(mipIdx, &pMipSurface);
			D3DSURFACE_DESC mipDesc;
			pMipSurface->GetDesc(&mipDesc);
			pSrcTexture->Release();

			mipWidth = mipDesc.Width;
			mipHeight = mipDesc.Height;

			D3DLOCKED_RECT lockedRectTexture;
			if(!SUCCEEDED(pSrcTexture->LockRect(mipIdx, &lockedRectTexture, NULL, D3DLOCK_READONLY )))
			{
				return false;
			}
			
			float* textureBuffer = (float*)lockedRectTexture.pBits;
			fpImage& dstMip = dstTexture[mipIdx];
			int pixelPitch = lockedRectTexture.Pitch / sizeof(float);

			for(unsigned int y=0;y<mipHeight; y++)
			{
				for(unsigned int x=0;x<mipWidth; x++)
				{
					float redf		= textureBuffer[y*pixelPitch+x*RGBA_COLOR_CHANNELS_PER_PIXEL+0];
					float greenf	= textureBuffer[y*pixelPitch+x*RGBA_COLOR_CHANNELS_PER_PIXEL+1];
					float bluef		= textureBuffer[y*pixelPitch+x*RGBA_COLOR_CHANNELS_PER_PIXEL+2];
					float alphaf	= textureBuffer[y*pixelPitch+x*RGBA_COLOR_CHANNELS_PER_PIXEL+3];

					fpPixel* dstPixel = dstMip.pixelsXY(x,y);
					dstPixel->set(redf, greenf, bluef, alphaf);
				}
			}

			pSrcTexture->UnlockRect(mipIdx);
		}

		return true;
	}
	//-----------------------------------------------------------------------------
	bool TextureConvert::ConvertD3DTextureToNVDXTFloatTexture(LPDIRECT3DCUBETEXTURE9 pSrcTexture, 
															  fpMipMappedCubeMap& dstTexture)
	{
		Assert(pSrcTexture);

		D3DSURFACE_DESC srcDesc;
		pSrcTexture->GetLevelDesc(0, &srcDesc);
		Assertf( srcDesc.Format == D3DFMT_A32B32G32R32F, "Source texture input is not in formatted as D3DFMT_A32B32G32R32F");
		
		unsigned int srcMipCount = pSrcTexture->GetLevelCount();
		unsigned int mipWidth = srcDesc.Width;
		unsigned int mipHeight = srcDesc.Height;
		++RAGE_LOG_DISABLE;
		dstTexture.resize(mipWidth, mipHeight, srcMipCount);
		--RAGE_LOG_DISABLE;

		for(unsigned int faceIdx=0; faceIdx<6; faceIdx++)
		{
			fpMipMappedImage& dstFaceImage = dstTexture[faceIdx];

			for(unsigned int mipIdx=0; mipIdx<srcMipCount; mipIdx++)
			{
				LPDIRECT3DSURFACE9 pMipSurface;
				pSrcTexture->GetCubeMapSurface((D3DCUBEMAP_FACES)faceIdx, mipIdx, &pMipSurface);

				D3DSURFACE_DESC mipDesc;
				pMipSurface->GetDesc(&mipDesc);
				pSrcTexture->Release();

				mipWidth = mipDesc.Width;
				mipHeight = mipDesc.Height;

				D3DLOCKED_RECT lockedRectSrc;
				if(!SUCCEEDED(pSrcTexture->LockRect((D3DCUBEMAP_FACES)faceIdx, mipIdx, &lockedRectSrc, NULL, D3DLOCK_READONLY )))
				{
					return false;
				}

				float* srcTextureBuffer = (float*)lockedRectSrc.pBits;
				fpImage& dstMip = dstFaceImage[mipIdx];
				int pixelPitch = lockedRectSrc.Pitch / sizeof(float);

				for(unsigned int y=0;y<mipHeight; y++)
				{
					for(unsigned int x=0;x<mipWidth; x++)
					{
						float redf		= srcTextureBuffer[y*pixelPitch+x*RGBA_COLOR_CHANNELS_PER_PIXEL+0];
						float greenf	= srcTextureBuffer[y*pixelPitch+x*RGBA_COLOR_CHANNELS_PER_PIXEL+1];
						float bluef		= srcTextureBuffer[y*pixelPitch+x*RGBA_COLOR_CHANNELS_PER_PIXEL+2];
						float alphaf	= srcTextureBuffer[y*pixelPitch+x*RGBA_COLOR_CHANNELS_PER_PIXEL+3];

						fpPixel* dstPixel = dstMip.pixelsXY(x,y);
						dstPixel->set(redf, greenf, bluef, alphaf);
					}
				}

				pSrcTexture->UnlockRect((D3DCUBEMAP_FACES)faceIdx, mipIdx);
			}//End for(unsigned int mipIdx..
		}//End for(unsigned int faceIdx

		return true;
	}

#endif //USE_NVDXT
	//-----------------------------------------------------------------------------
	bool TextureConvert::resizeTexture( int width, int height )
	{
		HRESULT hr;
		LPD3DXBUFFER pMemBuffer=NULL;

		hr = D3DXSaveTextureToFileInMemory(&pMemBuffer, D3DXIFF_DDS, mPTexture, NULL);
		if (pMemBuffer==NULL)
		{
			return false;
		}

		LPDIRECT3DTEXTURE9 newTexture=NULL;
		hr = D3DXCreateTextureFromFileInMemoryEx(mPD3DDevice, 
			pMemBuffer->GetBufferPointer(),
			pMemBuffer->GetBufferSize(),
			width,
			height,
			mPTexture->GetLevelCount(),				
			0,
			getFormat(),
			D3DPOOL_SYSTEMMEM,
			D3DX_FILTER_TRIANGLE | D3DX_FILTER_DITHER | D3DX_FILTER_MIRROR,
			D3DX_FILTER_BOX, 
			0,
			NULL,
			NULL,
			&newTexture);

		if (pMemBuffer!=NULL)
		{
			pMemBuffer->Release();
			pMemBuffer=NULL;
		}
		
		if ((newTexture==NULL) || (!SUCCEEDED(hr)))
		{
			return false;
		}
		
		releaseTexture();
		mPTexture = newTexture;
		
		return true;
	}

	//-----------------------------------------------------------------------------
	bool TextureConvert::buildMergeMapping(const std::string& mergeFormat, std::map<char, char>* mergeMapping, int textureCount, bool& alphaMapped)
	{
		//Assumed format
		// "textureindex0:channel~tochannel,textureindex1:channel~tochannel..."
		//
		//	WHERE:
		//		textureindex	= index of texture in list specified by -mergetexs param
		//		channel			= (rgba) characters representing channel to read from
		//		tochannel		= (rgba) characters representing channel to write to
		//		:				= separate texture index from channel
		//		~				= separate channel from tochannel
		//		,				= separate different merge descriptions
		//
		//	EXAMPLE:
		//		0:rg~ba,1:a~r   = Merge red and green channel into output blue and alpha channel 
		//							from image 0 and merge alpha channel into red channel from image 1.
		
		// ---parse format
		const char* format = mergeFormat.c_str();
		int length = mergeFormat.length();
		char mode = 0; //reading texture index
		int tokenStart = 0;
		int texId = -1;
		int srcChCnt = 0;
		int dstChCnt = 0;
		char tempBuffer[5]; //if we have more characters than this we have issues
		char srcChannels[5]; //gotta have one for '\0'
		char dstChannels[5]; //gotta have one for '\0'

		bool doAdd = false;
		for (int c = 0; c < length; ++c) 
		{			
			if (format[c] == ':')
			{
				strncpy(tempBuffer, &format[tokenStart], (c - tokenStart));
				tempBuffer[(c - tokenStart)] = '\0';
				texId = atoi(tempBuffer);
				if (texId < 0 || texId > textureCount)
				{
					Errorf("Error: Texture Merge format is incorrect. Texture index '%d' is not within 0 and %d : %s", texId, textureCount, format);
					return false;
				}
				mode = 1; //reading in channel(s)
				tokenStart=c+1;
				doAdd = true;
			} 
			else if (format[c] == '~')
			{
				strncpy(srcChannels, &format[tokenStart], (c - tokenStart));
				srcChannels[(c - tokenStart)] = '\0';
				srcChCnt = (c - tokenStart); 
				mode = 2; //reading out channel(s)
				tokenStart=c+1;
				doAdd = true;
			}
			else if (format[c] == ',')
			{
				strncpy(dstChannels, &format[tokenStart], (c - tokenStart));
				dstChannels[(c - tokenStart)] = '\0';
				dstChCnt = (c - tokenStart);
				//add the entry, reset the data, and go back to the top
				if (!addChannelMapping(srcChCnt, srcChannels, dstChCnt, dstChannels, mergeMapping[texId], alphaMapped))
				{
					Errorf("Error: Texture Merge format is incorrect. %s", format);
					return false;
				}
				texId = -1;
				mode = 0; //reading texture index
				tokenStart=c+1;
				doAdd = false;
			}
		}

		if (doAdd)
		{
			strncpy(dstChannels, &format[tokenStart], (length - tokenStart));
			dstChannels[(length - tokenStart)] = '\0';
			dstChCnt = (length - tokenStart);

			if (!addChannelMapping(srcChCnt, srcChannels, dstChCnt, dstChannels, mergeMapping[texId], alphaMapped))
			{
				Errorf("Error: Texture Merge format is incorrect. %s", format);
				return false;
			}
		}	

		return true;
	}

	//-----------------------------------------------------------------------------
	bool TextureConvert::addChannelMapping(int srcChCnt, const char* srcChannels, int dstChCnt, const char* dstChannels, std::map<char, char>& mergeMapping, bool& alphaMapped)
	{
		if (srcChCnt == dstChCnt)
		{
			char c = 0;
			while (srcChannels[c] != '\0')
			{
				if (srcChannels[c] != 'a' && srcChannels[c] != 'A' && 
					srcChannels[c] != 'r' && srcChannels[c] != 'R' && 
					srcChannels[c] != 'g' && srcChannels[c] != 'G' && 
					srcChannels[c] != 'b' && srcChannels[c] != 'B')
				{
					Errorf("Error: Invalid Channel character. '%c' Can only accept (\"R G B A\" or \"r g b a\")", srcChannels[c]);
					return false;
				}

				if (dstChannels[c] != 'a' && dstChannels[c] != 'A' && 
					dstChannels[c] != 'r' && dstChannels[c] != 'R' && 
					dstChannels[c] != 'g' && dstChannels[c] != 'G' && 
					dstChannels[c] != 'b' && dstChannels[c] != 'B')
				{
					Errorf("Error: Invalid Channel character. '%c' Can only accept (\"R G B A\" or \"r g b a\")", dstChannels[c]);
					return false;
				}
				mergeMapping[srcChannels[c]] = dstChannels[c];

				if (dstChannels[c] == 'a' || dstChannels[c] == 'A') 
				{
					alphaMapped = true;
				}
				c++;
			}
		}
		else
		{
			Errorf("Error: Source \"%s\" channel count (%d) does not match destination \"%s\" channel count (%d).", srcChannels, srcChCnt, dstChannels, dstChCnt);
			return false;
		}
		return true;
	}

	//-----------------------------------------------------------------------------
	byte TextureConvert::getChannelIndex(const char channel)
	{
		//format D3DFMT_A32B32G32R32F has 4 channels in order (ABGR)
		byte index = 3; // 'a'
		if (channel == 'r' || channel == 'R')
		{
			index = 0;
		}
		else if (channel == 'g' || channel == 'G')
		{
			index = 1;
		}
		else if (channel == 'b' || channel == 'B')
		{
			index = 2;
		}
		return index;
	}

} //namespace rage
