// -----------------------------------------------------------------------------
// $Workfile: DXT5Compress.cpp  $
//   Creator: Clemens Pecinovsky
// Copyright: 2005 Rockstar Vienna
// $Revision: #2 $
//   $Author: krose $
//     $Date: 2006/06/29 $
// -----------------------------------------------------------------------------
// Description: 
// -----------------------------------------------------------------------------

#include <stdio.h>
#include <math.h>

#include "system/new.h"
#include "DXT5Compress.h"


float const minExponent = -16.0;
float const maxExponent = +16.0;
  

//-----------------------------------------------------------------------------
void convertIntoDXT5(MapImage &mapImage, const char *outputFilename)
//-----------------------------------------------------------------------------
{
    int blockWidth = mapImage.getWidth()/4;
    int blockHeight = mapImage.getHeight() /4;
    int numBlocks = blockWidth * blockHeight;
    ddsDXT5ColorAlphaBlock_t *DXT5Blocks = rage_new ddsDXT5ColorAlphaBlock_t[numBlocks];


    for (int y=0; y<blockHeight; y++)
    {
        for (int x=0; x<blockWidth; x++)
        {
            convertIntoDXT5Block(x, y, mapImage, &(DXT5Blocks[y * blockWidth + x]));
        }
    }

    writeDXT5File(outputFilename, mapImage.getWidth(), mapImage.getHeight(), DXT5Blocks);

    delete DXT5Blocks;    
}


//-----------------------------------------------------------------------------
void convertIntoDXT5Block(int x, int y, MapImage &image, ddsDXT5ColorAlphaBlock_t *DXT5Block)
//-----------------------------------------------------------------------------
{
    int alphaValues[4][4];
    convertIntoAlphaBlock(x, y, image, &DXT5Block->alphaBlock, alphaValues);
    convertIntoColorBlock(x, y, image, &DXT5Block->colorBlock, alphaValues);
}


//-----------------------------------------------------------------------------
unsigned char getAlphaExponentValue(float red, float green, float blue)
//-----------------------------------------------------------------------------
{
    //float brightness = sqrt(red*red+green*green+blue*blue);
    float brightness = max(max(red,green),blue);
    float exponent = static_cast<float>((log(static_cast<double>(brightness))/log(2.0)));
    float exponentBiased = ((exponent-minExponent)/(maxExponent-minExponent)  )*255.0f;
    int alphaExp = (int) ceil(exponentBiased);
    if (alphaExp>255) 
    {
        alphaExp = 255;
    }
    else if (alphaExp<0)
    {
        alphaExp = 0;
    }
    return (unsigned char)alphaExp;
}


//-----------------------------------------------------------------------------
void getAlphaInterpolated(int minAlpha, int maxAlpha, int alphas[8])
//-----------------------------------------------------------------------------
{
    alphas[0]= (maxAlpha*7 + minAlpha *0) /7 ; 
    alphas[1]= (maxAlpha*6 + minAlpha *1) /7 ; 
    alphas[2]= (maxAlpha*5 + minAlpha *2) /7 ; 
    alphas[3]= (maxAlpha*4 + minAlpha *3) /7 ; 
    alphas[4]= (maxAlpha*3 + minAlpha *4) /7 ; 
    alphas[5]= (maxAlpha*2 + minAlpha *5) /7 ; 
    alphas[6]= (maxAlpha*1 + minAlpha *6) /7 ; 
    alphas[7]= (maxAlpha*0 + minAlpha *7) /7 ; 
}


//-----------------------------------------------------------------------------
int getAlphaIndex(int alphas[8], int alphaValue)
//-----------------------------------------------------------------------------
{
    int currentIdx=7;
    while ( (currentIdx>0) && (alphas[currentIdx]<alphaValue) )
    {
        currentIdx--;
    }
    return currentIdx;
}


//-----------------------------------------------------------------------------
float getAlphaError(int alphas[8], int alphaValues[4][4])
//-----------------------------------------------------------------------------
{
    float ret=0;
    for (int yy=0; yy<4; yy++)
    {
        for (int xx=0; xx<4; xx++)
        {
            if (alphaValues[yy][xx]>=0)
            {                
                int currentIdx=getAlphaIndex(alphas, alphaValues[yy][xx]);
                ret+= (alphas[currentIdx]-alphaValues[yy][xx]) * (alphas[currentIdx]-alphaValues[yy][xx]);
            }
        }
    }
    return ret;
}


//-----------------------------------------------------------------------------
float getAlphaError(int minalpha, int maxalpha, int alphaValues[4][4])
//-----------------------------------------------------------------------------
{
    int alphas[8];
    getAlphaInterpolated(minalpha, maxalpha, alphas);
    return getAlphaError(alphas, alphaValues);
}


//-----------------------------------------------------------------------------
void fillAlphaEncoding(int minalpha, int maxalpha, 
                       int alphaValues[4][4],
                       unsigned char stuff[6])
//-----------------------------------------------------------------------------
{
    int alphas[8];
    int idx[4][4];
    getAlphaInterpolated(minalpha, maxalpha, alphas);

    getAlphaEncodingIndices(alphaValues, alphas, idx);

    // First three bytes.
    stuff[0] = (unsigned char)((idx[0][0]) | (idx[0][1] << 3) | ((idx[0][2] & 0x03) << 6));
    stuff[1] = (unsigned char)(((idx[0][2] & 0x04) >> 2) | (idx[0][3] << 1) | (idx[1][0] << 4) | ((idx[1][1] & 0x01) << 7));
    stuff[2] = (unsigned char)(((idx[1][1] & 0x06) >> 1) | (idx[1][2] << 2) | (idx[1][3] << 5));

    // Second three bytes.
    stuff[3] = (unsigned char)((idx[2][0]) | (idx[2][1] << 3) | ((idx[2][2] & 0x03) << 6));
    stuff[4] = (unsigned char)(((idx[2][2] & 0x04) >> 2) | (idx[2][3] << 1) | (idx[3][0] << 4) | ((idx[3][1] & 0x01) << 7));
    stuff[5] = (unsigned char)(((idx[3][1] & 0x06) >> 1) | (idx[3][2] << 2) | (idx[3][3] << 5));

}

//-----------------------------------------------------------------------------
void getAlphaEncodingIndices(int alphaValues[4][4],
                             int alphas[8],
                            int idx[4][4])
//-----------------------------------------------------------------------------
{

    for (int yy=0; yy<4; yy++)
    {
        for (int xx=0; xx<4; xx++)
        {
            int currentIdx;
            if (alphaValues[yy][xx]>=0)
            {
                currentIdx=getAlphaIndex(alphas, alphaValues[yy][xx]);
            }
            else
            {
                //value unused, so any index is ok.
                currentIdx=0;
            }

            //change encoding...
            if (currentIdx==7)
            {
                currentIdx = 1;
            }
            else if (currentIdx!=0)
            {
                currentIdx++;
            }
            idx[yy][xx]=currentIdx;
        }
    }
}


//-----------------------------------------------------------------------------
void getAlphaExponentBlockValues(int x, int y, const MapImage& image, int alphaValues[4][4], float &avgAlpha)
//-----------------------------------------------------------------------------
{
    avgAlpha=0;
    int numSamples=0;

    for (int yy=0;yy<4; yy++)
    {
        for (int xx=0;xx<4; xx++)
        {
            float red, green, blue, alpha;
            image.getPixel(x*4+xx, y*4+yy, red, green, blue, alpha);            
            if (alpha>0)
            {
                alphaValues[yy][xx] = getAlphaExponentValue(red, green, blue);
                avgAlpha+=alphaValues[yy][xx];
                numSamples++;
            }
            else
            {
                alphaValues[yy][xx] = -1;
            }
        }
    }
    if (numSamples>0)
    {
        avgAlpha/=numSamples;
    }
}


//-----------------------------------------------------------------------------
void correctAlphaValuesForEncoding(int minalpha, int maxalpha, int alphaValues[4][4])
//-----------------------------------------------------------------------------
{
    int alphas[8];
    getAlphaInterpolated(minalpha, maxalpha, alphas);

    for (int yy=0; yy<4; yy++)
    {
        for (int xx=0; xx<4; xx++)
        {
            int currentIdx;
            if (alphaValues[yy][xx]>=0)
            {
                currentIdx=getAlphaIndex(alphas, alphaValues[yy][xx]);
            }
            else
            {
                //value unused, so any index is ok.
                currentIdx=0;
            }
            alphaValues[yy][xx]= alphas[currentIdx];
        }
    }
}


//-----------------------------------------------------------------------------
int getMaxAlphaExponent(int alphaValues[4][4])
//-----------------------------------------------------------------------------
{
    int maxAlpha=-1;
    for (int yy=0; yy<4; yy++)
    {
        for (int xx=0; xx<4; xx++)
        {
            if ((maxAlpha<0) || (alphaValues[yy][xx]>maxAlpha) )
            {
                maxAlpha = alphaValues[yy][xx];
            }
        }
    }
    return maxAlpha;
}


//-----------------------------------------------------------------------------
void convertIntoAlphaBlock(int x, int y, MapImage &image, 
                           ddsAlphaBlock3BitLinear_t *alphaBlock, 
                           int alphaValues[4][4])
//-----------------------------------------------------------------------------
{
    float avgAlpha=0;
    getAlphaExponentBlockValues(x,y, image, alphaValues, avgAlpha);

    int bestMinAlpha=0;
    int bestMaxAlpha=0;
    getBestMinMaxAlpha(bestMinAlpha, bestMaxAlpha, alphaValues, avgAlpha );

    alphaBlock->alpha0=(unsigned char)(bestMaxAlpha);
    alphaBlock->alpha1=(unsigned char)(bestMinAlpha);
    fillAlphaEncoding(bestMinAlpha, bestMaxAlpha, alphaValues, alphaBlock->stuff);

    correctAlphaValuesForEncoding(bestMinAlpha, bestMaxAlpha, alphaValues);
}


//-----------------------------------------------------------------------------
void getBestMinMaxAlpha(int &bestMinAlpha, int &bestMaxAlpha,
                        int alphaValues[4][4],float avgAlpha)
//-----------------------------------------------------------------------------
{
    //int a2= getMaxAlphaExponent(alphaValues);
    float bestError=-1;

    //for (int a1=0;a1<256;a1++)
    for (int a1=0; a1< (int)ceil(avgAlpha*2.0f/3.0f); a1++)
    {
        int a2 = (int) ceil(2.0f* (avgAlpha - a1))  ;
        //for (int a2=a1+1;a2<256;a2++)
        {
            float error=getAlphaError(a1,a2,alphaValues);
            if ((bestError==-1) || (error<bestError))
            {
                bestError = error;
                bestMinAlpha = a1;
                bestMaxAlpha = a2;
            }
        }
    }
    
}


//-----------------------------------------------------------------------------
void getColorBlockValues(int x, int y, MapImage & image,
                         const int alphaValues[4][4], 
                         int colors[4][4][3], 
                         float avg[3])
//-----------------------------------------------------------------------------
{
    int numVals=0;
    avg[0]=0;
    avg[1]=0;
    avg[2]=0;

    for (int yy=0;yy<4; yy++)
    {
        for (int xx=0;xx<4; xx++)
        {
            float red, green, blue, alpha;
            image.getPixel(x*4+xx, y*4+yy, red, green, blue, alpha);
            if (alpha>0)
            {
                float exponentVal = powf(2.0f, (( ((float)alphaValues[yy][xx])/255.0f)*(maxExponent-minExponent)) + minExponent  );
                red/=exponentVal;
                green/=exponentVal;
                blue/=exponentVal;

                avg[0]+=red;
                avg[1]+=green;
                avg[2]+=blue;
                numVals++;

                colors[yy][xx][0] = (int) ((red*31.0f)+0.5f);
                colors[yy][xx][1] = (int) ((green*63.0f)+0.5f);
                colors[yy][xx][2] = (int) ((blue*31.0f)+0.5f);
            }
            else
            {
                colors[yy][xx][0] = -1;
                colors[yy][xx][1] = -1;
                colors[yy][xx][2] = -1;
            }
        }
    }

    if (numVals>0)
    {
        float divVal = 1.0f/numVals;
        avg[0]*=(divVal * 31.0f);
        avg[1]*=(divVal * 63.0f);
        avg[2]*=(divVal * 31.0f);
        avg[0]+=0.5f;
        avg[1]+=0.5f;
        avg[2]+=0.5f;
    }
}


//-----------------------------------------------------------------------------
void getColorInterpolated(int minColor[3], int maxColor[3], 
                          int colors[4][3])
//-----------------------------------------------------------------------------
{
    int *minColorP=minColor;
    int *maxColorP=maxColor;

    unsigned short minCol565=get565Color(minColor);
    unsigned short maxCol565=get565Color(maxColor);
    if (minCol565>maxCol565)
    {
        minColorP=maxColor;
        maxColorP=minColor;
    }

    for (int i=0; i<3; i++)
    {
        colors[0][i]=(maxColorP[i]*3 + minColorP[i]*0) / 3;
        colors[1][i]=(maxColorP[i]*2 + minColorP[i]*1) / 3;
        colors[2][i]=(maxColorP[i]*1 + minColorP[i]*2) / 3;
        colors[3][i]=(maxColorP[i]*0 + minColorP[i]*3) / 3;
    }
}


//-----------------------------------------------------------------------------
float getColorDistance(int color1[3], int color2[3])
//-----------------------------------------------------------------------------
{
    float ret = 0;
    for (int i=0; i<3; i++)
    {
        ret += (color1[i]-color2[i])*(color1[i]-color2[i]);
    }
    return ret;
}


//-----------------------------------------------------------------------------
float getColorError(int colors[4][3], int colorValues[4][4][3])
//-----------------------------------------------------------------------------
{
    float ret=0;
    for (int yy=0; yy<4; yy++)
    {
        for (int xx=0; xx<4; xx++)
        {
            if (colorValues[yy][xx][0]>=0)
            {
                float minError=-1;
                for (int i=0; i<4; i++)
                {
                    float currentError = getColorDistance(colors[i], colorValues[yy][xx]);
                    if ((minError==-1) || (currentError<minError))
                    {
                        minError = currentError;
                    }
                }
                ret+= minError;
            }
        }
    }
    return ret;
}


//-----------------------------------------------------------------------------
float getColorError(int minColor[3], int maxColor[3], int colorValues[4][4][3])
//-----------------------------------------------------------------------------
{
    int colors[4][3];
    getColorInterpolated(minColor, maxColor, colors);
    return getColorError(colors, colorValues);
}


//-----------------------------------------------------------------------------
unsigned short get565Color(int color[3])
//-----------------------------------------------------------------------------
{
    return (unsigned char)( (( color[0] & 0x1f) <<11) |
             (( color[1] & 0x3f) <<5) |
             (( color[2] & 0x1f) <<0) );
}


//-----------------------------------------------------------------------------
void fillColorEncoding(int minColor[3], int maxColor[3], 
                       int colorValues[4][4][3],
                       unsigned char row[4])
//-----------------------------------------------------------------------------
{
    int colors[4][3];
    int idx[4][4];
    getColorInterpolated(minColor, maxColor, colors);

    for (int yy=0; yy<4; yy++)
    {
        for (int xx=0; xx<4; xx++)
        {
            int currentIdx=0;
            if (colorValues[yy][xx][0]>=0)
            {
                float minError=-1;
                for (int i=0; i<4; i++)
                {
                    float currentError = getColorDistance(colors[i], colorValues[yy][xx]);
                    if ((minError==-1) || (currentError<minError))
                    {
                        minError = currentError;
                        currentIdx=i;
                    }
                }
            }
            else
            {
                //value unused, so any index is ok.
            }

            //change encoding...
            if (currentIdx==3)
            {
                currentIdx = 1;
            }
            else if (currentIdx!=0)
            {
                currentIdx++;
            }
            idx[yy][xx]=(currentIdx & 3);
        }
    }

    row[0] = (unsigned char)((idx[0][0]) | (idx[0][1] << 2) | (idx[0][2] << 4) | (idx[0][3] << 6));
    row[1] = (unsigned char)((idx[1][0]) | (idx[1][1] << 2) | (idx[1][2] << 4) | (idx[1][3] << 6));
    row[2] = (unsigned char)((idx[2][0]) | (idx[2][1] << 2) | (idx[2][2] << 4) | (idx[2][3] << 6));
    row[3] = (unsigned char)((idx[3][0]) | (idx[3][1] << 2) | (idx[3][2] << 4) | (idx[3][3] << 6));

}


//-----------------------------------------------------------------------------
void convertIntoColorBlock(int x, int y, MapImage &image, 
                           ddsColorBlock_t *colorBlock, 
                           const int alphaValues[4][4])
//-----------------------------------------------------------------------------
{
    int colors[4][4][3];
    float avg[3];
    getColorBlockValues(x,y, image, alphaValues, colors, avg);    

    int minColor[3];
    int maxColor[3];
    int bestMinColor[3];
    int bestMaxColor[3];
    float bestError=-1;
    
  
    for (minColor[0]=0; minColor[0]< (int)ceil(avg[0]*2.0f/3.0f); minColor[0]++)
    {
        maxColor[0] = (int) ceil(2.0f* (avg[0] - minColor[0]))  ;
        if (maxColor[0]<32)
        {
            for (minColor[1]=0; minColor[1]< (int)ceil(avg[1]*2.0f/3.0f); minColor[1]++)
            {
                maxColor[1] = (int) ceil(2.0f* (avg[1] - minColor[1]))  ;
                if (maxColor[1]<64)
                {
                    for (minColor[2]=0; minColor[2]< (int)ceil(avg[2]*2.0f/3.0f); minColor[2]++)
                    {
                        maxColor[2] = (int) ceil(2.0f* (avg[2] - minColor[2]))  ;
                        if (maxColor[2]<32)
                        {
                            float error = getColorError(minColor, maxColor, colors);
                            if ((bestError==-1) || (error<bestError))
                            {
                                bestError = error;
                                bestMinColor[0] = minColor[0];
                                bestMinColor[1] = minColor[1];
                                bestMinColor[2] = minColor[2];
                                
                                bestMaxColor[0] = maxColor[0];
                                bestMaxColor[1] = maxColor[1];
                                bestMaxColor[2] = maxColor[2];
                            }
                        }
                    }
                }
            }
        }
    }

/*    
    bestMinColor[0]=avg[0];
    bestMinColor[1]=avg[1];
    bestMinColor[2]=avg[2];
    bestMaxColor[0]=avg[0];
    bestMaxColor[1]=avg[1];
    bestMaxColor[2]=avg[2];
*/    

    unsigned short minCol565=get565Color(bestMinColor);
    unsigned short maxCol565=get565Color(bestMaxColor);
    if (minCol565>maxCol565)
    {
        unsigned short tmp=maxCol565;
        maxCol565=minCol565;
        minCol565=tmp;
    }

    colorBlock->colors[0]=maxCol565;
    colorBlock->colors[1]=minCol565;
    fillColorEncoding(bestMinColor, bestMaxColor, colors, colorBlock->row);
}


//-----------------------------------------------------------------------------
bool writeDXT5File(const char *fileName, int width, int height, ddsDXT5ColorAlphaBlock_t *DXT5Blocks)
//-----------------------------------------------------------------------------
{
    FILE *out;
    out = fopen(fileName, "wb");
    if (out!=NULL)
    {
        if (writeDXT5Header(out, width, height))
        {
            writeDXT5Blocks(out, width*height / (4*4), DXT5Blocks);
        }
        fclose(out);
        return true;
    }
    else
    {
        return false;
    }

}


//-----------------------------------------------------------------------------
bool writeDXT5Header(FILE *out, int width, int height)
//-----------------------------------------------------------------------------
{
    int empty=0;
    fwrite("DDS ",1,4, out);
    
    int magicNumber=124;
    fwrite(&magicNumber, 1, sizeof(int), out);
    
    int flags1= DDS_WIDTH | DDS_HEIGHT | DDS_CAPS | DDS_PIXELFORMAT | DDS_LINEARSIZE;
    fwrite(&flags1, 1, sizeof(int), out);    

    fwrite(&height, 1, sizeof(int), out);    
    fwrite(&width, 1, sizeof(int), out);

    int linearSize = width * height / 16 * 16;
    fwrite(&linearSize, 1, sizeof(int), out);    
    
    fwrite(&empty, 1, sizeof(int), out);   //depth
    fwrite(&empty, 1, sizeof(int), out);   //MipMapCount
    fwrite(&empty, 1, sizeof(int), out);   //AlphaBitDepth

    for (int i = 0; i < 10; i++)
    {
        fwrite(&empty, 1, sizeof(int), out);		// Not used
    }

    int blockSize= 32;
    fwrite(&blockSize, 1, sizeof(int), out);
    int flags2 = DDS_FOURCC;
    fwrite(&flags2, 1, sizeof(int), out);
    int fourCC = IL_MAKEFOURCC('D','X','T','5');
    fwrite(&fourCC, 1, sizeof(int), out);
    int bits = 32;
    fwrite(&bits, 1, sizeof(int), out);
    int alphaMask =	0xff000000;
    fwrite(&alphaMask, 1, sizeof(int), out);
    int redMask =	0x00ff0000;
    fwrite(&redMask, 1, sizeof(int), out);
    int greenMask =	0x0000ff00;
    fwrite(&greenMask, 1, sizeof(int), out);
    int blueMask =	0x000000ff;
    fwrite(&blueMask, 1, sizeof(int), out);

    int ddsCaps1= DDS_TEXTURE;
    fwrite(&ddsCaps1, 1, sizeof(int), out); //ddsCaps1
    fwrite(&empty, 1, sizeof(int), out);   //ddsCaps2
    fwrite(&empty, 1, sizeof(int), out);   //ddsCaps3
    fwrite(&empty, 1, sizeof(int), out);   //ddsCaps4
    fwrite(&empty, 1, sizeof(int), out);   //TextureStage

    return true;
}


//-----------------------------------------------------------------------------
bool writeDXT5Blocks(FILE *out, int numBlocks, ddsDXT5ColorAlphaBlock_t *DXT5Blocks)
//-----------------------------------------------------------------------------
{
    fwrite(DXT5Blocks, numBlocks, sizeof(ddsDXT5ColorAlphaBlock_t), out);
    return true;
}

