// -----------------------------------------------------------------------------
// $Workfile: TexutureConvert.h  $
//   Creator: Clemens Pecinovsky
// Copyright: 2005 Rockstar Vienna
// $Revision: #13 $
//   $Author: tlemoine $
//     $Date: 2006/09/21 $
// -----------------------------------------------------------------------------
// Description: 
// -----------------------------------------------------------------------------

#ifndef RSV_TEXTURE_CONVERT_H
#define RSV_TEXTURE_CONVERT_H

#if __XENON
#define USE_COMPRESSINATOR	0
#define USE_NVDXT 0
#else
#define USE_COMPRESSINATOR	0
#define USE_NVDXT 1
#endif

#define NVDXT_USE_FLOAT_TEXTURE 0

#if !__PPU
#include "system/xtl.h"

#if __WIN32
#include "system/wndproc.h"
#include <d3d9.h>
#include <D3d9types.h>
#include <D3dx9math.h>
#endif

#include <map>
#include <string>
#include <vector>

#include	"devil/ilu.h"
#ifdef __cplusplus
extern "C" {
#endif
#include	"devil/il_internal.h"
#ifdef __cplusplus
}
#endif
#endif

#include "atl/array.h"
#include "grcore/dds.h"
#include "vector/vector4.h"

class fpMipMappedImage;
class fpMipMappedCubeMap;
class RGBAMipMappedImage;

namespace rage
{

#define RGBA_COLOR_CHANNELS_PER_PIXEL 4
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

struct TCConvertOptions
{
	enum TCGammaSpace
	{
		SRGB = 0,
		LINEAR,
		SRGB_EXPOSED,
		LINEAR_EXPOSED,
		NUM_GAMMA_SPACES
	};

	enum TCMipSharpenFilter
	{
		MIPSHARP_SOFT,
		MIPSHARP_MEDIUM,
		MIPSHARP_HARD
	};

	enum TCMipFilter
	{
		MIPFILTER_POINT,
		MIPFILTER_BOX,
		MIPFILTER_TRIANGLE,
		MIPFILTER_QUADRATIC,
		MIPFILTER_CUBIC,
		MIPFILTER_CATROM,
		MIPFILTER_MITCHELL
	};

	D3DFORMAT	Format;
	int			Width;
	int			Height;

	//The HDR Offset and Exponent values in both sRGB [0], and Linear [1] Gamma space and in exposed sRGB [2] and Gamma [3] space
	float		HDROffR[NUM_GAMMA_SPACES], HDROffG[NUM_GAMMA_SPACES], HDROffB[NUM_GAMMA_SPACES], HDROffA[NUM_GAMMA_SPACES];
	float		HDRExpR[NUM_GAMMA_SPACES], HDRExpG[NUM_GAMMA_SPACES], HDRExpB[NUM_GAMMA_SPACES], HDRExpA[NUM_GAMMA_SPACES];

	int					MipLevels;
	TCMipFilter			MipFilter;
	bool				SharpenMips;
	TCMipSharpenFilter	SharpenMipFilter;
	
	bool		FadeMips;
	Vector4		MipFadeToColor;
	int			MipFadeInLevel;
	float		MipFadeAmount;

	bool		SetSRGBGammaInDDSHeader;
	bool		TextureIsNormalMap;
	bool		ForceOneBitAlpha;

	TCConvertOptions()
	{
		Format = D3DFMT_DXT5;
		Width = Height = -1;
		
		for(int i=0; i<NUM_GAMMA_SPACES; i++)
		{
			HDROffR[i] = HDROffG[i] = HDROffB[i] = HDROffA[i] = 0.0f;
			HDRExpR[i] = HDRExpG[i] = HDRExpB[i] = HDRExpA[i] = 1.0f;
		}

		MipLevels = 0;
		MipFilter = MIPFILTER_BOX;

		SharpenMips = false;
		SharpenMipFilter = MIPSHARP_MEDIUM;

		FadeMips = false;
		MipFadeToColor.Set(0.5f,0.5f,0.5f,0.0f);
		MipFadeInLevel = 1;
		MipFadeAmount = 0.15f;
		
		SetSRGBGammaInDDSHeader = false;
		TextureIsNormalMap = false;
		ForceOneBitAlpha = false;
	}

	TCConvertOptions(int w, int h, D3DFORMAT f)
		: Width(w)
		, Height(h)
		, Format(f)
	{
		MipLevels = 0;

		for(int i=0; i<NUM_GAMMA_SPACES; i++)
		{
			HDROffR[i] = HDROffG[i] = HDROffB[i] = HDROffA[i] = 0.0f;
			HDRExpR[i] = HDRExpG[i] = HDRExpB[i] = HDRExpA[i] = 1.0f;
		}

		MipLevels = 0;
		MipFilter = MIPFILTER_BOX;

		SharpenMips = false;
		SharpenMipFilter = MIPSHARP_MEDIUM;

		FadeMips = false;
		MipFadeToColor.Set(0.5f,0.5f,0.5f,0.0f);
		MipFadeInLevel = 1;
		MipFadeAmount = 0.15f;

		SetSRGBGammaInDDSHeader = false;
		TextureIsNormalMap = false;
		ForceOneBitAlpha = false;
	}

	TCConvertOptions(const TCConvertOptions& other)
		: Format(other.Format)
		, Width(other.Width)
		, Height(other.Height)
		, MipLevels(other.MipLevels)
		, MipFilter(other.MipFilter)
		, SharpenMips(other.SharpenMips)
		, SharpenMipFilter(other.SharpenMipFilter)
		, FadeMips(other.FadeMips)
		, MipFadeToColor(other.MipFadeToColor)
		, MipFadeInLevel(other.MipFadeInLevel)
		, MipFadeAmount(other.MipFadeAmount)
		, SetSRGBGammaInDDSHeader(other.SetSRGBGammaInDDSHeader)
		, TextureIsNormalMap(other.TextureIsNormalMap)
		, ForceOneBitAlpha(other.ForceOneBitAlpha)
	{
		for(int i=0; i<NUM_GAMMA_SPACES; i++)
		{
			HDROffR[i] = other.HDROffR[i];
			HDROffG[i] = other.HDROffG[i];
			HDROffB[i] = other.HDROffB[i];
			HDROffA[i] = other.HDROffA[i];

			HDRExpR[i] = other.HDRExpR[i];
			HDRExpG[i] = other.HDRExpG[i];
			HDRExpB[i] = other.HDRExpB[i];
			HDRExpA[i] = other.HDRExpA[i];
		}
	};

};

struct MulticropOptions
{
	float uMin, vMin, uMax, vMax;
	int cropRoundingFlag;
	ILenum filter;
	TCConvertOptions options;
	float scale;
	std::string filename;
};

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

// PURPOSE : Convert the supplied value from Gamma 2.2 (sRGB) to Gamma 1.0 (Linear)
inline float RTCGammaConvert_22To10(float val)
{
	//normal deGamma curve
	return pow(val, 2.2f);

	// this baby doesn't seem to matchup with what photoshop is outputting
	//return ((val <= 0.03928f) ? val / 12.92f : pow((val + 0.055f) / 1.055f, 2.4f));
}

// PURPOSE : Convert the supplied value from  Gamma 1.0 (Linear) to Gamma 2.2 (sRGB)
inline float RTCGammaConvert_10To22(float val)
{
	//normal 2.2 gamma curve
	return pow(val, 1.0f/2.2f);

	// this baby doesn't seem to matchup with what photoshop is outputting
	//return (val <= 0.00304f) ? val * 12.92f : (1.055f * pow(val, 1.0f/2.4f) - 0.055f); 
}

// PURPOSE : Convert the supplied value from  Gamma 1.0 (Linear) to Gamma 2.0
inline float RTCGammaConvert_20To10(float val)
{
	//2.0 space deGamma curve
	return pow(val, 2.0f);
}

// PURPOSE : Convert the supplied value from  Gamma 1.0 (Linear) to Gamma 2.0
inline float RTCGammaConvert_10To20(float val)
{
	//2.0 space gamma curve
	return pow(val, 1.0f/2.0f);
}

// PURPOSE : Convert the supplied value from  Gamma 1.0 (Linear) to Gamma 2.2 (sRGB)
// this uses the 
inline float RTCGammaConvert_10To22Xenon(float val)
{
	
	float C0x =  64.0f  / 1023.0f;
	float C0y = 128.0f  / 1023.0f;
	float C0z = 512.0f  / 1023.0f;
	float C0w =   1.0f  /  255.0f;

	float C1x = 1023.0f;
	float C1y = 1023.0f / 2.0f;
	float C1z = 1023.0f / 4.0f;
	float C1w = 1023.0f / 8.0f;

//	float C2x =   0.0f;
	float C2y =  32.0f / 255.0f;
	float C2z =  64.0f / 255.0f;
	float C2w = 128.0f / 255.0f;

    float GammaTexel;

	float L0 = floorf(val * C1x)*C0w;
	float L1 = floorf(val * C1y)*C0w + C2y;
	float L2 = floorf(val * C1z)*C0w + C2z;
	float L3 = floorf(val * C1w)*C0w + C2w;

	GammaTexel = val < C0z ? L2 : L3;
	GammaTexel = val < C0y ? L1 : GammaTexel;
	GammaTexel = val < C0x ? L0 : GammaTexel;

    return  ( GammaTexel ) ;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

class TextureConvert
{
public:
	enum AlphaCheckResult
	{
		RTC_HASMULTIBITALPHA,
		RTC_HASONEBITALPHA,
		RTC_NOALPHA
	};

public:
    TextureConvert();
    virtual ~TextureConvert();
	
	void DisableTIFFWarnings();
	
	static void TIFFErrorHandler(const char*, const char*, va_list);

	// Load the texture, whatever format it is, whatever input format I read in, convert it to a DirectX format
    bool loadTexture(LPCTSTR pSrcFile);
	bool loadMipTextures(atArray<std::string>& mipTexturePaths);
	bool loadCubeTextures(atArray<std::string>& cubeTexturePaths);
	bool loadAndMergeTextures(atArray<std::string>& mergeTexturePaths, const std::string& mergeFormat);
	bool loadArrayTextures(atArray<std::string>& texturePaths, int numLayers, int twidth);
	
	// Save as a directX file
    bool saveTexture(LPCTSTR pDestFile, TCConvertOptions& tcOpt);
    bool saveTextureWithoutCompression(LPCTSTR pDestFile, TCConvertOptions& tcOpt);
	bool saveCubeTexture(LPCSTR pDestFile, TCConvertOptions& tcOpt);
	bool saveVolumeTexture(LPCSTR pDestFile, TCConvertOptions& tcOpt);
	

    void releaseTexture();
    bool convertABGRF32ToRGBE();
    bool convertABGRF32ToRGBEWithExponentFromFile(LPCTSTR alphaName);
    bool convertABGRF32ToDXT5RGBE();
    bool convertRGBEToABGRF32();
	bool convertABGRF32ToARGB();
	bool crop(float uMin, float vMin, float uMax, float vMax, bool cropPow2RoundingUp, ILenum filter, TCConvertOptions* tcOpt, float scale);
	bool oneToOneCrop(float uMin, float vMin, float uMax, float vMax, TCConvertOptions* tcOpt);
	bool multicrop(std::vector<MulticropOptions>& crops, bool noScale);
	bool scale(TCConvertOptions* tcOpt, float scale, bool nearestFilter);
    bool loadAlphaFromL8(LPCTSTR alphaName);

    bool convertToFormat(TCConvertOptions& tcOpt, LPDIRECT3DTEXTURE9 *pTexture = NULL);
	bool buildAndProcessMips(TCConvertOptions& tcOpt, LPDIRECT3DTEXTURE9 *pTexture = NULL);

	bool convertCubeMapToFormat(TCConvertOptions& tcOpt, LPDIRECT3DCUBETEXTURE9 *pTexture = NULL);
	bool convertVolumeTextureToFormat(TCConvertOptions& tcOpt, LPDIRECT3DVOLUMETEXTURE9 *pTexture= NULL);

    bool removeAlphaFromA8R8G8B8();
    bool convertAlphaFromA8R8G8B8IntoLuminance(int width, int height);
	bool loadAlphaFromTextureFile(const char* filePath);

	bool calculateRelevantClipArea(LPDIRECT3DTEXTURE9 pTexture, int atlasX, int atlasY, bool verbose, float threshold, const char* outname = 0);

	void					getDescription(D3DSURFACE_DESC &desc, LPDIRECT3DTEXTURE9 pTexture = NULL, int mipLevel = 0);
	int						getWidth(int mipLevel = 0);
	int						getHeight(int mipLevel = 0);
	D3DFORMAT				getFormat(LPDIRECT3DTEXTURE9 pTexture = NULL);

	LPDIRECT3DTEXTURE9      GetTexture() { return mPTexture; }
	LPDIRECT3DCUBETEXTURE9	GetCubeTexture() { return mPCubeTexture; }
	LPDIRECT3DVOLUMETEXTURE9	GetVolumeTexture() { return mPVolumeTexture; }

	bool computeHDRExponentOffset(LPDIRECT3DTEXTURE9 pTexture, float exposure,
								  float outExpR[2], float outExpG[2], float outExpB[2],
								  float outOffR[2], float outOffG[2], float outOffB[2]);
	bool gammaCorrect(bool InGamma_Linear, bool InGamma_22, bool InGamma_SO, bool AdjustGamma_Linear, bool AdjustGamma_22, bool AdjustGamma_22Xenon, bool AdjustGamma_20);
	bool adjustExposure(float exposureVal);

	AlphaCheckResult hasAlpha(bool checkForMinMultiByteAlphaPercent = false, float minMultiByteAlphaPercent = 0.04f);

	bool resizeTexture( int width, int height );

	void initDevil(int numImages = 1);
	void shutdownDevil();
	void setCurrDevilImage(int image);

	bool convertTextureToDevilImage();

	bool convertDevilImageToTexture();

	typedef std::map<std::string, D3DFORMAT> FORMAT_MAP;
    static FORMAT_MAP sD3DFormats;

	class Convertor 
	{
	public:
		virtual ~Convertor(){};
		virtual bool Start( const char* ) { return true; }
		virtual void Convert( const float* src, float* des , TCConvertOptions* pOpts ) = 0;
		virtual void End() {}

		// settings for convertor
		virtual bool RequiresMipFilterOverride() { return false; }
		virtual TCConvertOptions::TCMipFilter GetMipFilterOverride() { return TCConvertOptions::MIPFILTER_BOX; }
	};


	typedef Convertor PixelConvertor;	


	static std::map<std::string,  PixelConvertor* > sConversionProcesses;
	typedef std::map<std::string, PixelConvertor*>::iterator	ProcessIterator;

	bool convert( PixelConvertor* convertor, TCConvertOptions* pOpts );

	// Array of image IDs
	ILuint* m_DevilImageIDs;
	int	m_NumDevilImageIDs;

	bool m_writeLgExpOff;

private:
    bool	createNULLRefDevice();
    void	releaseNULLRefDevice();
	bool	convertTo32Bit();

	bool	loadTextureInternal(LPCTSTR pSrcFile, LPDIRECT3DTEXTURE9 &pDstTexture);	

    void	reduceExponentFromRGB(float redf, float greenf, float bluef, int alphaExp,int &red, int &green, int &blue);
	void	calculateRGBE(float redf, float greenf, float bluef, int &red, int &green, int &blue, int &alpha);
	void	calculateRGBfromRGBE(int red, int green, int blue, int alpha, float &redf, float &greenf, float &bluef);

	bool cropDevilImage(float uMin, float vMin, float uMax, float vMax, bool cropPow2RoundingUp, ILenum filter, TCConvertOptions* tcOpt, float scale);
	bool cropDevilImageNoScale(float uMin, float vMin, float uMax, float vMax, int cropRoundingFlag, ILenum filter, TCConvertOptions* tcOpt, float scale, LPCTSTR pDestFile, int &extraPixels, int &extraPixelsOld, int &extraPixelsIdeal, int &idealPixelCount);

    bool convertToFormatInternalD3DX(LPDIRECT3DTEXTURE9 &oldTexture,
									TCConvertOptions& tcOpt, 
									LPDIRECT3DTEXTURE9 *newTexture);

	bool convertToFormatInternalNVDXT(LPDIRECT3DTEXTURE9 &oldTexture,
									TCConvertOptions& tcOpt, 
									LPDIRECT3DTEXTURE9 *newTexture);
	bool convertToFormatInternalNVDXT(LPDIRECT3DCUBETEXTURE9 &oldTexture,
									TCConvertOptions& tcOpt,
									LPDIRECT3DCUBETEXTURE9 *newTexture);

	bool buildAndProcessMipsD3DX(LPDIRECT3DTEXTURE9 &oldTexture, 
								TCConvertOptions& tcOpt,
								LPDIRECT3DTEXTURE9 *newTexture);

	bool buildAndProcessMipsNVDXT(LPDIRECT3DTEXTURE9 &oldTexture, 
								TCConvertOptions& tcOpt,
								LPDIRECT3DTEXTURE9 *newTexture);

	bool ConvertD3DTextureToNVDXTRgbaTexture(LPDIRECT3DTEXTURE9 pSrcTexture, RGBAMipMappedImage& dstTexture);
	
	bool ConvertD3DTextureToNVDXTFloatTexture(LPDIRECT3DTEXTURE9 pSrcTexture, fpMipMappedImage& dstTexture);
	bool ConvertD3DTextureToNVDXTFloatTexture(LPDIRECT3DCUBETEXTURE9 pSrcTexture, fpMipMappedCubeMap& dstTexture);
    int getExponent(float brightness);

	bool pushHdrDataIntoDDSHeader(LPCSTR pDestFile, TCConvertOptions& tcOpt);

	bool buildMergeMapping(const std::string& mergeFormat, std::map<char, char>* mergeMapping, int textureCount, bool& alphaMapped);
	bool addChannelMapping(int srcChCnt, const char* srcChannels, int dstChCnt, const char* dstChannels, std::map<char, char>& mergeMapping, bool& alphaMapped);
	byte getChannelIndex(const char channel);

	bool ConvertVolumeTextureTo565(int twidth, int numLayers, int numMips );

    static void initD3DFormats();

    LPDIRECT3D9             mPD3D;
    LPDIRECT3DDEVICE9       mPD3DDevice;
	
	float					mInputGamma;
    LPDIRECT3DTEXTURE9      mPTexture;

	LPDIRECT3DCUBETEXTURE9	mPCubeTexture;
	LPDIRECT3DVOLUMETEXTURE9	mPVolumeTexture;
};

} // namespace rage

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

#endif // RSV_TEXTURE_CONVERT_H
