// -----------------------------------------------------------------------------
// $Workfile: MapToDXT5.h  $
//   Creator: Clemens Pecinovsky
// Copyright: 2005 Rockstar Vienna
// $Revision: #1 $
//   $Author: krose $
//     $Date: 2006/02/15 $
// -----------------------------------------------------------------------------
// Description: 
// -----------------------------------------------------------------------------


#include <stdio.h>
#include <math.h>

#include "MapToDXT5.h"
#include "DXT5Compress.h"


float const minExponent = -16.0;
float const maxExponent = +16.0;
  

struct Settings
{
    const char* inputFilename;
    const char* outputFilename;

    Settings()
    {
        inputFilename = NULL;
        outputFilename = NULL;
    }
};


//-----------------------------------------------------------------------------
void usage(char* prgname)
//-----------------------------------------------------------------------------
{
    printf("USAGE: %s <inputname> <outputname> \n",prgname);
    printf("\n");
}


//-----------------------------------------------------------------------------
int parseCommandLine(int argc, char**argv, Settings &settings)
//-----------------------------------------------------------------------------
{
    char* lastDelimiter = strrchr(argv[0],'\\');
    if (lastDelimiter!=NULL)
    {
        strcpy(argv[0],lastDelimiter+1);
    }

    if (argc!=3)
    {
        return 1;
    }

    settings.inputFilename = argv[1];
    settings.outputFilename = argv[2];

    return 0;
}


//-----------------------------------------------------------------------------
void printError(int error)
//-----------------------------------------------------------------------------
{
    printf("Error: ");
    switch (error)
    {
        case 1:
            printf("Invalid Parameter\n");
            break;
        case 2:
            printf("No Input filename given\n");
                break;
        case 3:
            printf("No Output filename given\n");
                break;
        case 4:
            printf("Invalid format\n");
                break;
        case 5:
            printf("Could not load texture\n");
                break;
        case 6:
            printf("Could not write texture\n");
                break;
        default:
            printf("Unknown Error\n");
                break;
        
    }
}


//-----------------------------------------------------------------------------
int main(int argc, char** argv)
//-----------------------------------------------------------------------------
{
    Settings settings;


    int error = 0;
    if (argc>=1)
    {
        error = parseCommandLine(argc, argv, settings);
    }
    if (error!=0)
    {
        usage(argv[0]);
        printError(error);
        return error;
    }

    
    MapImage mapImage(settings.inputFilename);    
    convertIntoDXT5(mapImage, settings.outputFilename);

    return 0;
}


