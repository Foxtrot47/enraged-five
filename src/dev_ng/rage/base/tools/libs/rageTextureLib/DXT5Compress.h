// -----------------------------------------------------------------------------
// $Workfile: DXT5Compress.h  $
//   Creator: Clemens Pecinovsky
// Copyright: 2005 Rockstar Vienna
// $Revision: #1 $
//   $Author: krose $
//     $Date: 2006/02/15 $
// -----------------------------------------------------------------------------
// Description: 
// -----------------------------------------------------------------------------

#ifndef RSV_DXT5_COMPRESS_H
#define RSV_DXT5_COMPRESS_H

#include "system/xtl.h"

#if __WIN32
#include "system/wndproc.h"
#include <d3d9.h>
#include <D3d9types.h>
#include <D3dx9math.h>
#endif

#include "MapImage.h"

#define DDS_CAPS				0x00000001l
#define DDS_HEIGHT				0x00000002l
#define DDS_WIDTH				0x00000004l
#define DDS_PIXELFORMAT			0x00001000l

#define DDS_ALPHAPIXELS			0x00000001L
#define DDS_ALPHA				0x00000002L
#define DDS_PFRGB				0x00000040L
#define DDS_FOURCC				0x00000004L
#define DDS_PITCH				0x00000008L
#define DDS_COMPLEX				0x00000008L
#define DDS_TEXTURE				0x00001000L
#define DDS_MIPMAPCOUNT			0x00020000L
#define DDS_LINEARSIZE			0x00080000L
#define DDS_VOLUME				0x00200000L
#define DDS_MIPMAP				0x00400000L
#define DDS_DEPTH				0x00800000L

#define DDS_CUBEMAP				0x00000200L
#define DDS_CUBEMAP_POSITIVEX	0x00000400L
#define DDS_CUBEMAP_NEGATIVEX	0x00000800L
#define DDS_CUBEMAP_POSITIVEY	0x00001000L
#define DDS_CUBEMAP_NEGATIVEY	0x00002000L
#define DDS_CUBEMAP_POSITIVEZ	0x00004000L
#define DDS_CUBEMAP_NEGATIVEZ	0x00008000L


#define IL_MAKEFOURCC(ch0, ch1, ch2, ch3)                              \
    ((unsigned int)(unsigned char)(ch0) | ((unsigned int)(unsigned char)(ch1) << 8) |   \
    ((unsigned int)(unsigned char)(ch2) << 16) | ((unsigned int)(unsigned char)(ch3) << 24 ))



/* dds definition */
typedef enum
{
    DDS_PF_ARGB8888,
    DDS_PF_DXT1,
    DDS_PF_DXT2,
    DDS_PF_DXT3,
    DDS_PF_DXT4,
    DDS_PF_DXT5,
    DDS_PF_UNKNOWN
}
ddsPF_t;


/* 16bpp stuff */
#define DDS_LOW_5		0x001F;
#define DDS_MID_6		0x07E0;
#define DDS_HIGH_5		0xF800;
#define DDS_MID_555		0x03E0;
#define DDS_HI_555		0x7C00;


/* structures */
typedef struct ddsColorKey_s
{
    unsigned int		colorSpaceLowValue;
    unsigned int		colorSpaceHighValue;
} 
ddsColorKey_t;


typedef struct ddsCaps_s
{
    unsigned int		caps1;
    unsigned int		caps2;
    unsigned int		caps3;
    unsigned int		caps4;
} 
ddsCaps_t;


typedef struct ddsMultiSampleCaps_s
{
    unsigned short		flipMSTypes;
    unsigned short		bltMSTypes;
}
ddsMultiSampleCaps_t;


typedef struct ddsPixelFormat_s
{
    unsigned int		size;
    unsigned int		flags;
    unsigned int		fourCC;
    union
    {
        unsigned int	rgbBitCount;
        unsigned int	yuvBitCount;
        unsigned int	zBufferBitDepth;
        unsigned int	alphaBitDepth;
        unsigned int	luminanceBitCount;
        unsigned int	bumpBitCount;
        unsigned int	privateFormatBitCount;
    };
    union
    {
        unsigned int	rBitMask;
        unsigned int	yBitMask;
        unsigned int	stencilBitDepth;
        unsigned int	luminanceBitMask;
        unsigned int	bumpDuBitMask;
        unsigned int	operations;
    };
    union
    {
        unsigned int	gBitMask;
        unsigned int	uBitMask;
        unsigned int	zBitMask;
        unsigned int	bumpDvBitMask;
        ddsMultiSampleCaps_t	multiSampleCaps;
    };
    union
    {
        unsigned int	bBitMask;
        unsigned int	vBitMask;
        unsigned int	stencilBitMask;
        unsigned int	bumpLuminanceBitMask;
    };
    union
    {
        unsigned int	rgbAlphaBitMask;
        unsigned int	yuvAlphaBitMask;
        unsigned int	luminanceAlphaBitMask;
        unsigned int	rgbZBitMask;
        unsigned int	yuvZBitMask;
    };
}
ddsPixelFormat_t;


typedef struct ddsBuffer_s
{
    /* magic: 'dds ' */
    char				magic[ 4 ];

    /* directdraw surface */
    unsigned int		size;
    unsigned int		flags;
    unsigned int		height;
    unsigned int		width; 
    union
    {
        int				pitch;
        unsigned int	linearSize;
    };
    unsigned int		backBufferCount;
    union
    {
        unsigned int	mipMapCount;
        unsigned int	refreshRate;
        unsigned int	srcVBHandle;
    };
    unsigned int		alphaBitDepth;
    unsigned int		reserved;
    void				*surface;
    union
    {
        ddsColorKey_t	ckDestOverlay;   
        unsigned int	emptyFaceColor;
    };
    ddsColorKey_t		ckDestBlt;
    ddsColorKey_t		ckSrcOverlay;    
    ddsColorKey_t		ckSrcBlt;     
    union
    {
        ddsPixelFormat_t	pixelFormat;
        unsigned int	fvf;
    };
    ddsCaps_t			ddsCaps;
    unsigned int		textureStage;

    /* data (Varying size) */
    unsigned char		data[ 4 ];
}
ddsBuffer_t;


typedef struct ddsColorBlock_s
{
    unsigned short		colors[ 2 ];
    unsigned char		row[ 4 ];
}
ddsColorBlock_t;


typedef struct ddsAlphaBlockExplicit_s
{
    unsigned short		row[ 4 ];
}
ddsAlphaBlockExplicit_t;


typedef struct ddsAlphaBlock3BitLinear_s
{
    unsigned char		alpha0;
    unsigned char		alpha1;
    unsigned char		stuff[ 6 ];
}
ddsAlphaBlock3BitLinear_t;


typedef struct ddsColor_s
{
    unsigned char		r, g, b, a;
}
ddsColor_t;


void convertIntoDXT5(MapImage &mapImage, const char *outputFilename);

typedef struct ddsDXT5ColorAlphaBlock_s
{
    ddsAlphaBlock3BitLinear_t alphaBlock;
    ddsColorBlock_t colorBlock;    
}
ddsDXT5ColorAlphaBlock_t;

void convertIntoDXT5Block(int x, int y, MapImage &image, ddsDXT5ColorAlphaBlock_t *DXT5Block);

//alpha routines.
unsigned char getAlphaExponentValue(float red, float green, float blue);
int getAlphaIndex(int alphas[8], int alphaValue);
float getAlphaError(int alphas[8], int alphaValues[4][4]);
int getMaxAlphaExponent(int alphaValues[4][4]);
void getAlphaInterpolated(int minalpha, int maxalpha, int alphas[8]);
float getAlphaError(int minalpha, int maxalpha, int alphaValues[4][4]);
void fillAlphaEncoding(int minalpha, int maxalpha, int alphaValues[4][4], unsigned char stuff[6]);
void getAlphaEncodingIndices(int alphaValues[4][4], int alphas[8], int idx[4][4]);
void getAlphaExponentBlockValues(int x, int y, const MapImage &image, int alphaValues[4][4], float &avgAlpha);
void correctAlphaValuesForEncoding(int minalpha, int maxalpha, int alphaValues[4][4]);

void convertIntoAlphaBlock(int x, int y, MapImage &image, 
                           ddsAlphaBlock3BitLinear_t *alphaBlock, 
                           int alphaValues[4][4]);

void getBestMinMaxAlpha(int &bestMinAlpha, int &bestMaxAlpha,
                        int alphaValues[4][4],float avgAlpha);




//color routines
void getColorInterpolated(int minColor[3], int maxColor[3], 
                          int colors[4][3]);
float getColorDistance(int color1[3], int color2[3]);
float getColorError(int colors[4][3], int colorValues[4][4][3]);
float getColorError(int minColor[3], int maxColor[3], int colorValues[4][4][3]);
unsigned short get565Color(int color[3]);
void fillColorEncoding(int minColor[3], int maxColor[3], 
                       int colorValues[4][4][3],
                       unsigned char row[4]);

void getColorBlockValues(int x, int y, MapImage & image,
                         const int alphaValues[4][4], 
                         int colors[4][4][3], 
                         float avg[3]);

void convertIntoColorBlock(int x, int y, MapImage &image, 
                           ddsColorBlock_t *colorBlock, 
                           const int alphaValues[4][4]);


//file operations
bool writeDXT5File(const char *fileName, int width, int height, ddsDXT5ColorAlphaBlock_t *DXT5Blocks);
bool writeDXT5Header(FILE *out, int width, int height);
bool writeDXT5Blocks(FILE *out, int numBlocks, ddsDXT5ColorAlphaBlock_t *DXT5Blocks);

#endif // RSV_DXT5_COMPRESS_H
