// -----------------------------------------------------------------------------
// $Workfile: MapToDXT5.h  $
//   Creator: Clemens Pecinovsky
// Copyright: 2005 Rockstar Vienna
// $Revision: #1 $
//   $Author: krose $
//     $Date: 2006/02/15 $
// -----------------------------------------------------------------------------
// Description: 
// -----------------------------------------------------------------------------

#ifndef RSV_MAP_TO_DXT5_H
#define RSV_MAP_TO_DXT5_H

#include "system/xtl.h"

#if __WIN32
#include "system/wndproc.h"
#include <d3d9.h>
#include <D3d9types.h>
#include <D3dx9math.h>
#endif

#include "MapImage.h"


#endif // RSV_MAP_TO_DXT5_H
