﻿//---------------------------------------------------------------------------------------------
// <copyright file="MudboxVersion.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.AutodeskMudbox
{
    using System;
    using System.ComponentModel;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.Serialization;
    using RSG.Base.Attributes;

    /// <summary>
    /// Autodesk Mudbox Version enum.
    /// </summary>
    [DataContract]
    public enum MudboxVersion
    {
        [EnumMember]
        None = 0,

        [EnumMember]
        [FieldDisplayName("Mudbox 2014")]
        [RegistryKey(@"Software\Autodesk\Common\Mudbox\2014")]
        [ToolPath("Mudbox2014")]
        AutodeskMudbox2014,
    }
    
    /// <summary>
    /// MudboxVersion enumeration utilities and extension methods.
    /// </summary>
    public static class MudboxVersionUtils
    {
        /// <summary>
        /// Return Mudbox version display-name.
        /// </summary>
        /// <param name="version"></param>
        /// <returns></returns>
        public static String GetDisplayName(this MudboxVersion version)
        {
            Type type = version.GetType();

            FieldInfo fi = type.GetField(version.ToString());
            FieldDisplayNameAttribute[] attributes =
                fi.GetCustomAttributes(typeof(FieldDisplayNameAttribute), false) as FieldDisplayNameAttribute[];

            FieldDisplayNameAttribute attribute = attributes.FirstOrDefault();
            if (null != attribute)
                return (attribute.DisplayName);
            else
                throw (new ArgumentException("Mudbox version has no FieldDisplayName defined."));
        }

        /// <summary>
        /// Return Mudbox version Registry Key.
        /// </summary>
        /// <param name="version"></param>
        /// <returns></returns>
        public static String RegistryKey(this MudboxVersion version)
        {
            Type type = version.GetType();

            FieldInfo fi = type.GetField(version.ToString());
            RegistryKeyAttribute[] attributes =
                fi.GetCustomAttributes(typeof(RegistryKeyAttribute), false) as RegistryKeyAttribute[];

            RegistryKeyAttribute attribute = attributes.FirstOrDefault();
            if (null != attribute)
                return (attribute.Key);
            else
                throw (new ArgumentException("Mudbox version has no Registry Key defined."));
        }

        /// <summary>
        /// Return 3dsmax tool path.
        /// </summary>
        /// <param name="version"></param>
        /// <returns></returns>
        public static String ToolPath(this MudboxVersion version)
        {
            Type type = version.GetType();

            FieldInfo fi = type.GetField(version.ToString());
            ToolPathAttribute[] attributes =
                fi.GetCustomAttributes(typeof(ToolPathAttribute), false) as ToolPathAttribute[];

            ToolPathAttribute attribute = attributes.FirstOrDefault();
            if (null != attribute)
                return (attribute.Path);
            else
                throw (new ArgumentException("Mudbox version has no Tool Path defined."));
        }
    }

} // RSG.Interop.AutodeskMudbox namespace
