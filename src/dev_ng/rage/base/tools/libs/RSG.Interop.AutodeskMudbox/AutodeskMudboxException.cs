﻿//---------------------------------------------------------------------------------------------
// <copyright file="AutodeskMudboxException.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.AutodeskMudbox
{
    using System;

    /// <summary>
    /// Autodesk Mudbox exception class.
    /// </summary>
    public class AutodeskMudboxException : Exception
    {
        #region Constructor(s)
        public AutodeskMudboxException()
            : base()
        {
        }

        public AutodeskMudboxException(String message)
            : base(message)
        {
        }

        public AutodeskMudboxException(String message, Exception inner)
            : base(message, inner)
        {
        }

        public AutodeskMudboxException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context)
            : base(info, context)
        {
        }
        #endregion // Constructor(s)
    }

} // RSG.Interop.AutodeskMudbox namespace
