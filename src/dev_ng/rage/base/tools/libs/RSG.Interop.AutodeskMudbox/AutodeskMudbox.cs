﻿//---------------------------------------------------------------------------------------------
// <copyright file="AutodeskMudbox.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.AutodeskMudbox
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Text;
    using Microsoft.Win32;

    /// <summary>
    /// Static class giving information about installed versions of Autodesk
    /// Mudbox.
    /// </summary>
    public static class AutodeskMudbox
    {
        #region Constants
        /// <summary>
        /// Mudbox executable.
        /// </summary>
        private static readonly String MUDBOX_EXE = "mudbox.exe";

        /// <summary>
        /// File extensions.
        /// </summary>
        private static readonly ISet<String> EXTENSIONS = new HashSet<String> { ".mud" };

        /// <summary>
        /// Environment variable to set additional mudbox plugin paths.
        /// </summary>
        private const String MUDBOX_PLUGIN_ENVIRONMENT_VARIABLE = "MUDBOX_PLUG_IN_PATH";
        #endregion // Constants

        #region Controller Methods
        /// <summary>
        /// Determine whether any version of Mudbox is installed.
        /// </summary>
        /// <returns></returns>
        public static bool IsInstalled()
        {
            bool result = true;
            IEnumerable<MudboxVersion> versions =
                Enum.GetValues(typeof(MudboxVersion)).Cast<MudboxVersion>();
            foreach (MudboxVersion version in versions)
            {
                if (MudboxVersion.None == version)
                    continue; // Skip.
                result |= IsVersionInstalled(version);
            }

            return (result);
        }

        /// <summary>
        /// Determine whether a particular version of Mudbox is installed.
        /// </summary>
        /// <param name="version"></param>
        /// <returns></returns>
        public static bool IsVersionInstalled(MudboxVersion version)
        {
            RegistryKey rootKey = RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine, 
                RegistryView.Registry64);
            
            using (RegistryKey versionKey = rootKey.OpenSubKey(version.RegistryKey()))
            {
                if (null == versionKey)
                    return (false);

                String installPath = (String)versionKey.GetValue("Location");
                return (!String.IsNullOrWhiteSpace(installPath));
            }
        }

        /// <summary>
        /// Get installation path for a particular version of Mudbox.
        /// </summary>
        /// <param name="version"></param>
        /// <returns></returns>
        public static String GetInstallPath(MudboxVersion version)
        {
            Debug.Assert(IsVersionInstalled(version));
            if (!IsVersionInstalled(version))
                throw (new AutodeskMudboxException("Mudbox version not installed."));

            RegistryKey rootKey = RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine, 
                RegistryView.Registry64);
            
            using (RegistryKey versionKey = rootKey.OpenSubKey(version.RegistryKey()))
            {
                String installPath = (String)versionKey.GetValue("Location");
                return (installPath);
            }
        }

        /// <summary>
        /// Get executable path for a particular version of Mudbox.
        /// </summary>
        /// <param name="version"></param>
        /// <returns></returns>
        public static String GetExecutablePath(MudboxVersion version)
        {
            Debug.Assert(IsVersionInstalled(version));
            if (!IsVersionInstalled(version))
                throw (new AutodeskMudboxException("Mudbox version not installed."));

            String installdir = GetInstallPath(version);
            return (Path.Combine(installdir, MUDBOX_EXE));
        }

        /// <summary>
        /// Return true iff the source file looks like a Mudbox file.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static bool IsMudboxFile(String filename)
        {
            String extension = Path.GetExtension(filename);
            return (EXTENSIONS.Contains(extension));
        }


        /// <summary>
        /// Set Mudbox additional plugin paths.
        /// </summary>
        /// <param name="version">MotionBuilder version.</param>
        /// <param name="paths"></param>
        public static void SetAdditionalPluginPaths(MudboxVersion version, IEnumerable<String> paths)
        {
            // Early out if there is nothing to do.
            String existingPath = Environment.GetEnvironmentVariable(MUDBOX_PLUGIN_ENVIRONMENT_VARIABLE);
            if (existingPath == null && !paths.Any())
            {
                return;
            }

            try
            {
                String installDir = GetInstallPath(version);
                String defaultPluginPath = Path.Combine(installDir, "plugins");
                String newPluginPath = null;

                if (paths.Any())
                {
                    // e.g.
                    // C:\Program Files\Autodesk\Mudbox 2014\plugins;X:\gta5\tools_ng\techart\dcc\Mudbox\Mudbox2014
                    newPluginPath = String.Format("{0};{1}", defaultPluginPath, String.Join(";", paths));
                }

                Environment.SetEnvironmentVariable(MUDBOX_PLUGIN_ENVIRONMENT_VARIABLE, newPluginPath, EnvironmentVariableTarget.User);
            }
            catch (Exception ex)
            {
                throw (new AutodeskMudboxException("Failed to set plugin paths.", ex));
            }
        }

        /// <summary>
        /// Return Mudbox additional plugin paths.
        /// </summary>
        /// <param name="version">MotionBuilder version.</param>
        /// <param name="is64bit"></param>
        /// <returns></returns>
        public static IEnumerable<String> GetAdditionalPluginPaths(MudboxVersion version)
        {
            IList<String> pluginPaths = new List<String>();
            try
            {
                String installDir = GetInstallPath(version);
                String defaultPluginPath = Path.Combine(installDir, "plugins");

                String pluginPath = Environment.GetEnvironmentVariable(MUDBOX_PLUGIN_ENVIRONMENT_VARIABLE, EnvironmentVariableTarget.User);
                if (pluginPath != null)
                {
                    foreach (String path in pluginPath.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        if (path != defaultPluginPath)
                        {
                            pluginPaths.Add(path);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw (new AutodeskMudboxException("Failed to get plugin paths.", ex));
            }

            return (pluginPaths);
        }
        #endregion // Controller Methods
    }

} // RSG.Interop.AutodeskMudbox namespace
