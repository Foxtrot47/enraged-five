﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace RSG.Base.Configuration.UnitTest
{

    /// <summary>
    /// 
    /// </summary>
    [TestClass]
    public class EnvironmentTests
    {
        /// <summary>
        /// Verify simple substitutions and push/pop functionality.
        /// </summary>
        [TestMethod]
        public void TestSubst()
        {
            IEnvironment env = ConfigFactory.CreateEnvironment();

            // Verify no-ops (non-substitutable strings).
            Assert.IsFalse(env.RequiresSubst("test"));
            Assert.AreEqual("test", env.Subst("test"));
            Assert.IsTrue(env.RequiresSubst("$(test)"));
            Assert.AreEqual("$(test)", env.Subst("$(test)"));

            // Verify simple subst.
            env.Push();
            env.Add("test", "expandedtest");
            Assert.IsTrue(env.RequiresSubst("$(test)"));
            Assert.AreEqual("expandedtest", env.Subst("$(test)"));
            env.Pop();

            // Verify Push/Pop clears env.
            env.Push();
            env.Push();
            env.Add("test", "expandedtest");
            env.Pop();
            Assert.AreEqual("$(test)", env.Subst("$(test)"));
            env.Pop();
        }

        /// <summary>
        /// Verify environment clear.
        /// </summary>
        [TestMethod]
        public void TestClear()
        {
            IEnvironment env = ConfigFactory.CreateEnvironment();

            int depth = env.Depth;
            env.Push();
            Assert.AreEqual(depth + 1, env.Depth);
            env.Add("test", "expandedtest");
            Assert.IsTrue(env.RequiresSubst("$(test)"));
            Assert.AreEqual("expandedtest", env.Subst("$(test)"));
            env.Clear();
            Assert.IsTrue(env.RequiresSubst("$(test)"));
            Assert.AreEqual("$(test)", env.Subst("$(test)"));
            env.Pop();
            Assert.AreEqual(depth, env.Depth);
        }

        /// <summary>
        /// Verify reverse substitutions.
        /// </summary>
        [TestMethod]
        public void TestReverseSubst()
        {
            IEnvironment env = ConfigFactory.CreateEnvironment();

            // Verify no-ops (non-substitutable strings).
            Assert.IsFalse(env.RequiresSubst("test"));
            Assert.AreEqual("test", env.Subst("test"));
            Assert.AreEqual("test", env.ReverseSubst("test"));
            Assert.IsTrue(env.RequiresSubst("$(test)"));
            Assert.AreEqual("$(test)", env.Subst("$(test)"));
            Assert.AreEqual("$(test)", env.ReverseSubst("$(test)"));

            env.Add("test", "expandedtest");
            Assert.AreEqual("expandedtest", env.Subst("$(test)"));
            Assert.AreEqual("$(test)", env.ReverseSubst(env.Subst("$(test)")));
        }
    }

} // RSG.Base.Configuration.UnitTest namespace
