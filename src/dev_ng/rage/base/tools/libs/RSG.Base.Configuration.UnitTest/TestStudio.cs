﻿//---------------------------------------------------------------------------------------------
// <copyright file="TestStudio.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Base.Configuration.UnitTest
{
    using System;
    using System.Linq;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using RSG.Base.Configuration;

    /// <summary>
    /// Unit test methods for the <see cref="RSG.Base.Configuration.IStudio"/> interface.
    /// </summary>
    [TestClass]
    public class TestStudio
    {
        #region Member Data
        /// <summary>
        /// Config.
        /// </summary>
        IConfig config;
        #endregion // Member Data

        /// <summary>
        /// Unit test setup method.
        /// </summary>
        [TestInitialize]
        public void TestSetup()
        {
            this.config = ConfigFactory.CreateConfig();
        }

        /// <summary>
        /// Unit test studio object properties.
        /// </summary>
        [TestMethod]
        public void TestStudioObjects()
        {
            Assert.IsTrue(config.Studios.Studios.Any());
            Assert.AreEqual(10, config.Studios.Studios.Count);
            foreach (IStudio studio in config.Studios)
            {
                Assert.IsFalse(String.IsNullOrWhiteSpace(studio.Name));
                Assert.IsFalse(String.IsNullOrWhiteSpace(studio.FriendlyName));
                Assert.IsFalse(String.IsNullOrWhiteSpace(studio.TimeZoneId));
            }
        }

        /// <summary>
        /// Unit test studio timezone functionality.
        /// </summary>
        [TestMethod]
        public void TestStudioTimezone()
        {
            TimeZoneInfo tmz = config.Studios.ThisStudio.GetTimeZoneInfo();
            Assert.IsNotNull(tmz);
            Assert.IsTrue(tmz.SupportsDaylightSavingTime);
            Assert.AreEqual(TimeZoneInfo.Local, tmz);

            DateTime preDST = new DateTime(2014, 10, 20, 12, 0, 0, DateTimeKind.Utc);
            DateTime postDST_UK = new DateTime(2014, 10, 28, 12, 0, 0, DateTimeKind.Utc);  // 26th October 2014 DST (UK)
            DateTime postDST_US = new DateTime(2014, 11, 5, 12, 0, 0, DateTimeKind.Utc);   // 2nd November 2014 DST (USA)
            // Post this time the usual offsets apply.

            // Test some studio to studio offsets at different times of year (around DST).
            IStudio northStudio = config.Studios.Studios.FirstOrDefault(s => s.Name.Equals("north"));
            Assert.IsNotNull(northStudio);
            Assert.AreEqual("GMT Standard Time", northStudio.TimeZoneId);
            TimeZoneInfo northTmz = northStudio.GetTimeZoneInfo();
            if (config.Studios.ThisStudio.Equals(northStudio))
                Assert.AreEqual(TimeZoneInfo.Local, northTmz);

            IStudio sanDiegoStudio = config.Studios.Studios.FirstOrDefault(s => s.Name.Equals("sandiego"));
            Assert.IsNotNull(sanDiegoStudio);
            Assert.AreEqual("Pacific Standard Time", sanDiegoStudio.TimeZoneId);
            TimeZoneInfo sanDiegoTmz = sanDiegoStudio.GetTimeZoneInfo();
            if (config.Studios.ThisStudio.Equals(sanDiegoStudio))
                Assert.AreEqual(TimeZoneInfo.Local, sanDiegoTmz);

            IStudio nycStudio = config.Studios.Studios.FirstOrDefault(s => s.Name.Equals("newyork"));
            Assert.IsNotNull(nycStudio);
            Assert.AreEqual("Eastern Standard Time", nycStudio.TimeZoneId);
            TimeZoneInfo nycTmz = nycStudio.GetTimeZoneInfo();
            if (config.Studios.ThisStudio.Equals(nycStudio))
                Assert.AreEqual(TimeZoneInfo.Local, nycTmz);
            
            // Pre-DST in Autumn; North is +1, San Diego is +8, NYC is +5.
            DateTime preDSTNorth = TimeZoneInfo.ConvertTimeFromUtc(preDST, northTmz);
            DateTime preDSTSanDiego = TimeZoneInfo.ConvertTimeFromUtc(preDST, sanDiegoTmz);
            DateTime preDSTNYC = TimeZoneInfo.ConvertTimeFromUtc(preDST, nycTmz);
            Assert.AreEqual(TimeSpan.FromHours(8), preDSTNorth - preDSTSanDiego);
            Assert.AreEqual(TimeSpan.FromHours(5), preDSTNorth - preDSTNYC);

            // The critical 2 week DST offset; North is 0, SanDiego +7, NYC is +4
            DateTime postDST_UK_North = TimeZoneInfo.ConvertTimeFromUtc(postDST_UK, northTmz);
            DateTime postDST_UK_SanDiego = TimeZoneInfo.ConvertTimeFromUtc(postDST_UK, sanDiegoTmz);
            DateTime postDST_UK_NYC = TimeZoneInfo.ConvertTimeFromUtc(postDST_UK, nycTmz);
            Assert.AreEqual(TimeSpan.FromHours(7), postDST_UK_North - postDST_UK_SanDiego);
            Assert.AreEqual(TimeSpan.FromHours(4), postDST_UK_North - postDST_UK_NYC);

            // Post everyone syncing DST changes up again (for 2014, its November 2nd).
            // We're back to our normal offsets of +8 SD and +5 NYC.
            DateTime postDST_US_North = TimeZoneInfo.ConvertTimeFromUtc(postDST_US, northTmz);
            DateTime postDST_US_SanDiego = TimeZoneInfo.ConvertTimeFromUtc(postDST_US, sanDiegoTmz);
            DateTime postDST_US_NYC = TimeZoneInfo.ConvertTimeFromUtc(postDST_US, nycTmz);
            Assert.AreEqual(TimeSpan.FromHours(8), postDST_US_North - postDST_US_SanDiego);
            Assert.AreEqual(TimeSpan.FromHours(5), postDST_US_North - postDST_US_NYC);
        }
    }

} // RSG.Base.Configuration.UnitTest namespace
