﻿using System;
using RSG.Base.Editor;
using RSG.Metadata.Model;
using RSG.Metadata.Parser;
using RSG.Metadata.Data;
using System.Collections.Generic;
using System.Xml;
using System.Diagnostics;

namespace RSG.Metadata.Characters
{
    public class PedVariationMetaFile : HierarchicalModelBase, IMetaFile
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public String Filename
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public Structure Definition
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public PedVariation PedVariation
        {
            get;
            private set;
        }

        public SelectionSets SelectionSets
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public StructureTunable FileRootTunable
        {
            get;
            private set;
        }

        public MetaFile AudioValuesFile
        {
            get;
            private set;
        }

        public bool HasInternalFiles
        {
            get { return false; }
        }

        public IEnumerable<string> GetInternalFilenames
        {
            get
            {
                yield break;
            }
        }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the 
        /// <see cref="RSG.Metadata.Characters.PedVariationMetaFile"/>
        /// class.
        /// </summary>
        /// <param name="filename">
        /// The filename of the file this class will represent.
        /// </param>
        /// <param name="def">
        /// The root definition that this file has.
        /// </param>
        public PedVariationMetaFile(String filename, Parser.Structure def, StructureDictionary dictionary, RSG.Metadata.Model.MetaFile audioValues)
            : base(null)
        {
            this.AudioValuesFile = audioValues;
            this.Filename = filename;
            this.Definition = def;
            Deserialise(filename);
            CreatePedVariation(dictionary, audioValues);
        }

        /// <summary>
        /// Initialises a new instance of the 
        /// <see cref="RSG.Metadata.Characters.PedVariationMetaFile"/>
        /// class.
        /// </summary>
        /// <param name="def">
        /// The root definition that this file has.
        /// </param>
        public PedVariationMetaFile(Parser.Structure def, StructureDictionary dictionary)
            : base(null)
        {
            this.Definition = def;
        }

        /// <summary>
        /// Initialises a new instance of the 
        /// <see cref="RSG.Metadata.Characters.PedVariationMetaFile"/>
        /// class.
        /// </summary>
        /// <param name="def">
        /// The root definition that this file has.
        /// </param>
        public PedVariationMetaFile(IMetaFile metadataFile, StructureDictionary dictionary, RSG.Metadata.Model.MetaFile audioValues)
            : base(null)
        {
            this.AudioValuesFile = audioValues;
            this.Filename = metadataFile.Filename;
            this.Definition = metadataFile.Definition;
            this.FileRootTunable = (metadataFile as MetaFile).Members;
            CreatePedVariation(dictionary, audioValues);
        }
        #endregion // Constructor(s)

        #region Methods
        public MetaFile GetRawModel()
        {
            return new MetaFile(this.FileRootTunable);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        public void Serialise(String filename)
        {
            try
            {
                XmlDocument document = null;
                Serialise(out document);
                string folder = System.IO.Path.GetDirectoryName(filename);
                System.IO.Directory.CreateDirectory(folder);
                document.Save(filename);
            }
            catch (Exception ex)
            {
                String message = String.Format("Exception serialising metadata to file {0}.  See InnerException.",
                    filename);
                throw new ParserException(message, ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="document"></param>
        private void Serialise(out XmlDocument document)
        {
            try
            {
                document = new XmlDocument();

                // XML Declaration
                XmlDeclaration xmlDecl = document.CreateXmlDeclaration(
                    "1.0", "UTF-8", null);
                document.AppendChild(xmlDecl);

                // Document Root
                XmlElement xmlRoot = document.CreateElement(
                    Util.XmlEscape.EscapeDataTypeString(this.Definition.DataType));
                document.AppendChild(xmlRoot);

                // Loop through members
                if (this.FileRootTunable == null)
                    return;

                StructureTunable rootTune = this.FileRootTunable;
                foreach (Data.ITunable childTune in rootTune.Values)
                {
                    try
                    {
                        if (childTune.InheritParent == true)
                            continue;

                        Debug.Assert(childTune is Data.ITunableSerialisable,
                            "Unsupported ITunable, its not serialisable (internal error).");
                        if (!(childTune is Data.ITunableSerialisable))
                            continue;

                        XmlElement xmlElem =
                        (childTune as Data.ITunableSerialisable).Serialise(document, false);
                        if (childTune is PointerTunable)
                        {
                            PointerMember.PointerPolicy policy = ((childTune as PointerTunable).Definition as PointerMember).Policy;
                            if (policy == PointerMember.PointerPolicy.Owner || policy == PointerMember.PointerPolicy.SimpleOwner)
                            {
                                if (((childTune as PointerTunable).Value is ITunable))
                                {
                                    xmlElem.SetAttribute("type", ((childTune as PointerTunable).Value as ITunable).Definition.FriendlyTypeName);
                                }
                                else if (((childTune as PointerTunable).Value is String))
                                {
                                    xmlElem.SetAttribute("type", (childTune as PointerTunable).Value as String);
                                }
                            }
                        }
                        xmlRoot.AppendChild(xmlElem);
                    }
                    catch
                    {
                        Debug.Assert(false, "Error saving metadata file.");
                    }
                }
            }
            catch (Exception ex)
            {
                String message = String.Format("Exception serialising metadata to XML.  See InnerException.");
                throw new ParserException(message, ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        private void Deserialise(String filename)
        {
            try
            {
                using (XmlTextReader reader = new XmlTextReader(filename))
                {
                    reader.MoveToContent();
                    this.Deserialise(reader);
                }
            }
            catch (Exception ex)
            {
                String message = String.Format("Exception deserialising metadata from file {0}.  See InnerException.",
                    filename);
                throw new ParserException(message, ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="document"></param>
        private void Deserialise(XmlTextReader reader)
        {
            try
            {
                if (reader.NodeType == XmlNodeType.Element)
                {
                    bool isEmpty = reader.IsEmptyElement;
                    StructMember structMember = new StructMember(this.Definition, this.Definition);
                    XmlTunableFactory factory = new XmlTunableFactory();
                    ITunable tunable = factory.Create(null, structMember, reader);
                    if (tunable is StructureTunable)
                        this.FileRootTunable = tunable as StructureTunable;

                    if (!isEmpty)
                    {
                        if (reader.NodeType == XmlNodeType.Text)
                            reader.ReadString();
                        if (reader.NodeType == XmlNodeType.EndElement)
                            reader.ReadEndElement();
                    }
                }
            }
            catch (Exception ex)
            {
                String message = String.Format("Exception deserialising metadata from XML {0}.  See InnerException.",
                    reader.BaseURI);
                throw new ParserException(message, ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void CreatePedVariation(StructureDictionary dictionary, RSG.Metadata.Model.MetaFile audioValues)
        {
            Enumeration audioIds = null;
            dictionary.Enumerations.TryGetValue("workbench::PedVariation::AudioIds", out audioIds);
            Enumeration componentFlags = null;
            dictionary.Enumerations.TryGetValue("ePedCompFlags", out componentFlags);

            Dictionary<string, List<string>> componentAudioIds = new Dictionary<string, List<string>>();
            foreach (ITunable member in audioValues.Members.Values)
            {
                if (!(member is StructureTunable) || member.Name != "ComponentAudioIds")
                    continue;

                foreach (ITunable child in (member as StructureTunable).Values)
                {
                    if (child is ArrayTunable && child.Name == "audioIdVarations")
                    {
                        foreach (ITunable item in (child as ArrayTunable).Items)
                        {
                            if (item is StructureTunable && item.Name == "ComponentAudioId")
                            {
                                StringTunable name = (item as StructureTunable)["componentName"] as StringTunable;
                                BitsetTunable values = (item as StructureTunable)["audioIds"] as BitsetTunable;
                                if (name == null || values == null)
                                    continue;

                                if (componentAudioIds.ContainsKey(name.Value.ToLower()))
                                    continue;

                                componentAudioIds.Add(name.Value.ToLower(), new List<string>());
                                foreach (var enumValue in audioIds.Values.Keys)
                                {
                                    if (values.Value.Contains(enumValue))
                                        componentAudioIds[name.Value.ToLower()].Add(enumValue);
                                }
                            }
                        }
                    }
                }
            }

            Enumeration anchorPoints = null;
            dictionary.Enumerations.TryGetValue("eAnchorPoints", out anchorPoints);

            this.PedVariation = new PedVariation(this, this.FileRootTunable, audioIds, componentFlags, componentAudioIds, anchorPoints);
            this.SelectionSets = new SelectionSets(this.FileRootTunable["aSelectionSets"] as ArrayTunable);
        }
        #endregion // Methods
    } // PedVariationMetaFile
} // RSG.Metadata.Characters
