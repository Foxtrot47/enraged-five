﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Metadata.Characters
{
    public class PedComponent : IComponent
    {
        #region Properties
        /// <summary>
        /// The name that represents this component
        /// on a creature (i.e. Head).
        /// </summary>
        public string Name
        {
            get;
            private set;
        }

        /// <summary>
        /// The index of this component. Use to reference components
        /// in variations.
        /// </summary>
        public int Index
        {
            get;
            private set;
        }
        #endregion

        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="index"></param>
        public PedComponent(string name, int index)
        {
            Name = name;
            Index = index;
        }
        #endregion
    } // PedComponents
} // RSG.Metadata.Characters
