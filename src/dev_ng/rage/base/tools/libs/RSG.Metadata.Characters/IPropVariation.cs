﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Metadata.Characters
{
    public interface IPropVariation
    {
        #region Properties
        /// <summary>
        /// The index of anchor the prop is attached to.
        /// </summary>
        int PropAnchorIndex { get; }

        /// <summary>
        /// Gets the instance of the prop that 
        /// this variation is set on.
        /// </summary>
        IProp Prop { get; }
        #endregion
    } // IComponentVariation
} // RSG.Metadata.Characters
