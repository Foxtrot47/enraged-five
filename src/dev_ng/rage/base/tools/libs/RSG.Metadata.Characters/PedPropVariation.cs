﻿using System;
using System.Linq;
using RSG.Metadata.Data;
using System.Collections.Generic;
using RSG.Base.Editor;
using System.Diagnostics;
using RSG.Metadata.Parser;

namespace RSG.Metadata.Characters
{
    /// <summary>
    /// 
    /// </summary>
    public class PedPropVariation :
        HierarchicalModelBase,
        IVariation,
        IPropVariation,
        IStickynessVariation,
        IFlagVariation<UInt16>,
        IUserExpressionVariation<float>,
        IHasTextureVariations
    {
        #region Fields
        private Enumeration _componentFlags;
        #endregion Fields

        /// <summary>
        /// This constant is used to define the number of flags used inside the flag ushort
        /// tunable that represents a boolean bit field.
        /// </summary>
        private const int FlagCountConst = 16;

        /// <summary>
        /// This constant is used to set the number of float expressions in the expressions
        /// array the model is expecting. (This is due to the psc data not needing to set the
        /// size attribute).
        /// </summary>
        private const int ExpressionCountConst = 5;

        #region Events
        /// <summary>
        /// Occurs when the flag value changes.
        /// </summary>
        public event EventHandler FlagValueChanged;

        /// <summary>
        /// Occurs when the new flag value changes.
        /// </summary>
        public event EventHandler NewFlagValueChanged;

        /// <summary>
        /// Occurs when the stickyness value changes.
        /// </summary>
        public event EventHandler StickynessValueChanged;

        /// <summary>
        /// Occurs when a expression value changes.
        /// </summary>
        public event EventHandler<ExpressionChangedEventArgs> ExpressionValueChanged;
        #endregion

        #region Properties
        /// <summary>
        /// The name of this variation
        /// </summary>
        public string Name
        {
            get { return string.Format("{0}_{1:D3}", Prop.AttachmentPoint, VariationIndex); }
        }

        /// <summary>
        /// The index of the variation for the specific anchor index
        /// this variation represents.
        /// </summary>
        public int VariationIndex
        {
            get;
            private set;
        }

        /// <summary>
        /// The index of anchor the prop is attached to.
        /// </summary>
        public int PropAnchorIndex
        {
            get { return Prop.AnchorIndex;}
        }

        /// <summary>
        /// Gets the instance of the prop that 
        /// this variation is set on.
        /// </summary>
        public IProp Prop
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the number of user expressions this variation can have.
        /// </summary>
        public int ExpressionCount
        {
            get { return PedPropVariation.ExpressionCountConst; }
        }

        /// <summary>
        /// An array of a specific type that represents the user
        /// expressions.
        /// </summary>
        [RSG.Base.Editor.Command.UndoableProperty()]
        public float[] Expressions
        {
            get { return m_expression; }
            private set
            {
                if (this.SetPropertyValue(ref this.m_expression, value, "Expressions"))
                {
                    if (ExpressionValueChanged != null)
                    {
                        ExpressionValueChanged(this, new ExpressionChangedEventArgs(1));
                    }
                }
            }
        }
        private float[] m_expression;

        /// <summary>
        /// Gets the number of flags supported by this field.
        /// </summary>
        public int FlagCount 
        {
            get { return PedPropVariation.FlagCountConst; }
        }

        /// <summary>
        /// Gets the number of flags supported by this field.
        /// </summary>
        public int NewFlagCount
        {
            get { return this._componentFlags.Values.Count - 1; }
        }

        /// <summary>
        /// Gets the byte that represents the settings of
        /// all the flags in a bit field.
        /// </summary>
        [RSG.Base.Editor.Command.UndoableProperty()]
        public UInt16 Flags
        {
            get { return m_flags; }
            private set
            {
                if (this.SetPropertyValue(ref this.m_flags, value, "Flags"))
                {
                    if (this.FlagValueChanged != null)
                    {
                        this.FlagValueChanged(this, EventArgs.Empty);
                    }
                }
            }
        }
        private UInt16 m_flags;

        /// <summary>
        /// Gets the byte that represents the settings of
        /// all the flags in a bit field.
        /// </summary>
        [RSG.Base.Editor.Command.UndoableProperty()]
        public UInt32 NewFlags
        {
            get { return m_newFlags; }
            private set
            {
                if (this.SetPropertyValue(ref this.m_newFlags, value, "NewFlags"))
                {
                    if (this.NewFlagValueChanged != null)
                    {
                        this.NewFlagValueChanged(this, EventArgs.Empty);
                    }
                }
            }
        }
        private UInt32 m_newFlags;

        /// <summary>
        /// The stickness factor for this variation. The probability
        /// it'll stay on when damaged.
        /// </summary>
        [RSG.Base.Editor.Command.UndoableProperty()]
        public int Stickyness
        {
            get { return m_stickyness; }
            private set
            {
                if (this.SetPropertyValue(ref this.m_stickyness, value, "Stickyness"))
                {
                    if (this.StickynessValueChanged != null)
                    {
                        this.StickynessValueChanged(this, EventArgs.Empty);
                    }
                }
            }
        }
        private int m_stickyness;

        /// <summary>
        /// The variations od all the textures.
        /// </summary>
        public ITextureVariation[] TextureVariations
        {
            get;
            private set;
        }
        #endregion

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="component"></param>
        public PedPropVariation(IModel parent, IProp prop, StructureTunable variationTunable, Enumeration componentFlags)
            : base(parent)
        {
            this._componentFlags = componentFlags;
            Prop = prop;
            VariationIndex = (from t in variationTunable.Values.OfType<IntegerTunableBase<byte>>()
                              where t.Name == "propId"
                              select t.Value).FirstOrDefault();

            Expressions = new float[ExpressionCount];

            U16Tunable flagTunable = (from t in variationTunable.Values.OfType<U16Tunable>()
                                      where t.Name == "flags"
                                      select t).FirstOrDefault();

            U32Tunable newFlagTunable = (from t in variationTunable.Values.OfType<U32Tunable>()
                                         where t.Name == "propFlags"
                                         select t).FirstOrDefault();

            U8Tunable stickynessTunable = (from t in variationTunable.Values.OfType<U8Tunable>()
                                            where t.Name == "stickyness"
                                            select t).FirstOrDefault();
            if (flagTunable != null)
            {
                Flags = flagTunable.Value;
                FlagValueChanged += (s, e) => { flagTunable.Value = this.Flags; };
            }

            if (newFlagTunable != null)
            {
                NewFlags = newFlagTunable.Value;
                NewFlagValueChanged += (s, e) => { newFlagTunable.Value = this.NewFlags; };
            }

            if (stickynessTunable != null)
            {
                Stickyness = stickynessTunable.Value;
                StickynessValueChanged += (s, e) => { stickynessTunable.Value = (byte)this.Stickyness; };
            }

            ArrayTunable expressionArray = (from t in variationTunable.Values.OfType<ArrayTunable>()
                                            where t.Name == "expressionMods"
                                            select t).FirstOrDefault();
            if (expressionArray != null)
            {
                for (int i = 0; i < ExpressionCount; i++)
                {
                    if (expressionArray.Items.Count > i && expressionArray.Items[i] is FloatTunable)
                    {
                        Expressions[i] = (expressionArray.Items[i] as FloatTunable).Value;
                    }
                    else
                    {
                        expressionArray.Add(TunableFactory.Create(expressionArray, (expressionArray.Definition as ArrayMember).ElementType));
                        Expressions[i] = default(float);
                    }
                }
                ExpressionValueChanged += (s, e) => { (expressionArray.Items[e.Index] as FloatTunable).Value = this.Expressions[e.Index]; };
            }

            ArrayTunable textures = (from t in variationTunable.Values.OfType<ArrayTunable>()
                                     where t.Name == "texData"
                                     select t).FirstOrDefault();

            if (textures != null)
            {
                List<StructureTunable> textureVariations = (from t in textures.Items.OfType<StructureTunable>()
                                                            select t).ToList();
                TextureVariations = new PropTextureVariation[textureVariations.Count];
                for (int i = 0; i < textureVariations.Count; i++)
                {
                    TextureVariations[i] = new PropTextureVariation(this, textureVariations[i]);
                }
            }
            else
            {
                TextureVariations = new PropTextureVariation[0];
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Sets the flag at the specific index to the specific
        /// value.
        /// </summary>
        /// <param name="index">
        /// The index of the bit to set.
        /// </param>
        /// <param name="value">
        /// The value that the bit should be set to.
        /// </param>
        public void SetFlag(int index, bool value)
        {
            UInt16 flag = (UInt16)(0x01 << index);
            if (value)
                this.Flags |= flag;
            else
                this.Flags &= (UInt16)~flag;
        }

        public string ComponentFlagName(int index)
        {
            return this._componentFlags.Names[index + 1];
        }

        /// <summary>
        /// Gets the flag at the specific index
        /// </summary>
        /// <param name="index">
        /// The index of the bit to get.
        /// </param>
        public bool GetFlag(int index)
        {
            int flag = 0x01 << (index);
            if (((int)this.Flags & flag) == flag)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Sets all the flags as one value.
        /// </summary>
        /// <param name="value">
        /// The value that the bit field should be
        /// </param>
        public void SetFlags(UInt16 value)
        {
            this.Flags = value;
        }

        /// <summary>
        /// Sets the flag at the specific index to the specific
        /// value.
        /// </summary>
        /// <param name="index">
        /// The index of the bit to set.
        /// </param>
        /// <param name="value">
        /// The value that the bit should be set to.
        /// </param>
        public void SetNewFlag(int index, bool value)
        {
            UInt32 flag = (UInt32)(0x01 << index);
            if (value)
                this.NewFlags |= flag;
            else
                this.NewFlags &= (UInt32)~flag;
        }

        /// <summary>
        /// Gets the flag at the specific index
        /// </summary>
        /// <param name="index">
        /// The index of the bit to get.
        /// </param>
        public bool GetNewFlag(int index)
        {
            int flag = 0x01 << (index);
            if (((int)this.NewFlags & flag) == flag)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Sets all the flags as one value.
        /// </summary>
        /// <param name="value">
        /// The value that the bit field should be
        /// </param>
        public void SetNewFlags(UInt32 value)
        {
            this.NewFlags = value;
        }

        /// <summary>
        /// Sets the stickness factor for this variation.
        /// </summary>
        /// <param name="distribution">
        /// The value to set the stickness factor to.
        /// </param>
        public void SetStickyness(int stickyness)
        {
            this.Stickyness = stickyness;        
        }

        /// <summary>
        /// Sets the indexed expression to the specified value.
        /// </summary>
        /// <param name="value">
        /// The value to set the expression to.
        /// </param>
        /// <param name="index">
        /// The index of the expression to set.
        /// </param>
        public void SetUserExpressionIdentifier(float value, int index)
        {
            Debug.Assert(index < this.Expressions.Length, "Expression index out of range");
            if (index >= this.Expressions.Length)
                return;

            float[] newArray = new float[ExpressionCount];
            for (int i = 0; i < ExpressionCount; i++)
            {
                if (i == index)
                    newArray[i] = value;
                else
                    newArray[i] = this.Expressions[i];
            }
            this.Expressions = newArray;
            if (ExpressionValueChanged != null)
                ExpressionValueChanged(this, new ExpressionChangedEventArgs(index));
        }

        #endregion
    } // PedPropVariation
} // RSG.Metadata.Characters
