﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using RSG.Metadata.Data;
using RSG.Metadata.Parser;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using System.Globalization;

namespace RSG.Metadata.Characters
{
    /// <summary>
    /// 
    /// </summary>
    public class PedComponentVariation : 
        HierarchicalModelBase,
        IVariation,
        ISetVariation,
        IComponentVariation,
        IAudioVariation,
        IPropMaskVariation,
        IUserExpressionVariation<float>,
        IFlagVariation<UInt16>,
        IHasTextureVariations,
        IHasClothFlag
    {
        #region Fields
        private Enumeration _componentFlags;
        #endregion Fields

        #region Events
        /// <summary>
        /// Occurs when the inculsion value changes.
        /// </summary>
        public event EventHandler InclusionIdentifierChanged;
        /// <summary>
        /// Occurs when the exculsion value changes.
        /// </summary>
        public event EventHandler ExculsionIdentifierChanged;
        /// <summary>
        /// Occurs when the exculsion value changes.
        /// </summary>
        public event EventHandler InclusionSetChanged;
        /// <summary>
        /// Occurs when the exculsion value changes.
        /// </summary>
        public event EventHandler ExclusionSetChanged;
        /// <summary>
        /// Occurs when the audio value changes.
        /// </summary>
        public event EventHandler FirstAudioIdentifierChanged;
        /// <summary>
        /// Occurs when the audio value changes.
        /// </summary>
        public event EventHandler SecondAudioIdentifierChanged;

        /// <summary>
        /// Occurs when the has cloth value changes.
        /// </summary>
        public event EventHandler HasClothFlagChanged;
        /// <summary>
        /// Occurs when the flag value changes.
        /// </summary>
        public event EventHandler FlagValueChanged;
        /// <summary>
        /// Occurs when the flag value changes.
        /// </summary>
        public event EventHandler NewFlagValueChanged;
        /// <summary>
        /// Occurs when a expression value changes.
        /// </summary>
        public event EventHandler<ExpressionChangedEventArgs> ExpressionValueChanged;
        /// <summary>
        /// Occurs when the prop mask value changes.
        /// </summary>
        public event EventHandler PropMaskValueChanged;
        #endregion

        #region Constants
        /// <summary>
        /// This constant is used to define the number of flags used inside the flag ushort
        /// tunable that represents a boolean bit field.
        /// </summary>
        private const int FlagCountConst = 16;

        /// <summary>
        /// This constant is used to set the number of float expressions in the expressions
        /// array the model is expecting. (This is due to the psc data not needing to set the
        /// size attribute).
        /// </summary>
        private const int ExpressionCountConst = 5;

        /// <summary>
        /// This constant is used to set the number prop mask flags that are supported by this
        /// variation.
        /// </summary>
        private const int PropMaskCountConst = 8;
        #endregion

        #region Properties
        /// <summary>
        /// The name of this variation
        /// </summary>
        public string Name
        {
            get { return string.Format("{0}_{1:D3}", Component.Name, VariationIndex); }
        }

        /// <summary>
        /// The index of the component that this is a 
        /// variation of.
        /// </summary>
        public int ComponentIndex
        {
            get { return Component.Index; }
        }

        /// <summary>
        /// The index of the variation for the specific component
        /// this variation represents.
        /// </summary>
        public int VariationIndex
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the instance of the component that 
        /// this variation is set on.
        /// </summary>
        public IComponent Component
        {
            get;
            private set;
        }

        /// <summary>
        /// The id to specify the inclusion set for this variation.
        /// </summary>
        [RSG.Base.Editor.Command.UndoableProperty()]
        public int InclusionIdentifier
        {
            get { return m_inclusion; }
            private set
            {
                if (this.SetPropertyValue(ref this.m_inclusion, value, "InclusionIdentifier"))
                {
                    if (this.InclusionIdentifierChanged != null)
                    {
                        InclusionIdentifierChanged(this, EventArgs.Empty);
                    }
                }
            }
        }
        private int m_inclusion;

        /// <summary>
        /// The id to specify the exclusion set for this variation.
        /// </summary>
        [RSG.Base.Editor.Command.UndoableProperty()]
        public int ExclusionIdentifier
        {
            get { return m_exclusion; }
            private set
            {
                if (this.SetPropertyValue(ref this.m_exclusion, value, "ExclusionIdentifier"))
                {
                    if (this.ExculsionIdentifierChanged != null)
                    {
                        ExculsionIdentifierChanged(this, EventArgs.Empty);
                    }
                }
            }
        }
        private int m_exclusion;

        /// <summary>
        /// The id to specify the inclusion set for this variation.
        /// </summary>
        [RSG.Base.Editor.Command.UndoableProperty()]
        public uint InclusionSet
        {
            get { return m_inclusionSet; }
            private set
            {
                if (this.SetPropertyValue(ref this.m_inclusionSet, value, "InclusionSet"))
                {
                    if (this.InclusionSetChanged != null)
                    {
                        InclusionSetChanged(this, EventArgs.Empty);
                    }
                }
            }
        }
        private uint m_inclusionSet;

        /// <summary>
        /// The id to specify the exclusion set for this variation.
        /// </summary>
        [RSG.Base.Editor.Command.UndoableProperty()]
        public uint ExclusionSet
        {
            get { return m_exclusionSet; }
            private set
            {
                if (this.SetPropertyValue(ref this.m_exclusionSet, value, "ExclusionSet"))
                {
                    if (this.ExclusionSetChanged != null)
                    {
                        ExclusionSetChanged(this, EventArgs.Empty);
                    }
                }
            }
        }
        private uint m_exclusionSet;

        /// <summary>
        /// The id to specify the audio setting for this variation.
        /// </summary>
        [RSG.Base.Editor.Command.UndoableProperty()]
        public string FirstAudioIdentifier
        {
            get { return m_audio; }
            private set
            {
                if (this.SetPropertyValue(ref this.m_audio, value, "FirstAudioIdentifier"))
                {
                    if (this.FirstAudioIdentifierChanged != null)
                    {
                        FirstAudioIdentifierChanged(this, EventArgs.Empty);
                    }
                }
            }
        }
        private string m_audio;

        /// <summary>
        /// The id to specify the audio setting for this variation.
        /// </summary>
        [RSG.Base.Editor.Command.UndoableProperty()]
        public string SecondAudioIdentifier
        {
            get { return m_secondAudio; }
            private set
            {
                if (this.SetPropertyValue(ref this.m_secondAudio, value, "SecondAudioIdentifier"))
                {
                    if (this.SecondAudioIdentifierChanged != null)
                    {
                        SecondAudioIdentifierChanged(this, EventArgs.Empty);
                    }
                }
            }
        }
        private string m_secondAudio;

        /// <summary>
        /// Gets a value indicating whether this component variation has cloth in it.
        /// </summary>
        [RSG.Base.Editor.Command.UndoableProperty()]
        public bool HasCloth
        {
            get { return m_hasCloth; }
            private set
            {
                if (this.SetPropertyValue(ref this.m_hasCloth, value, "HasCloth"))
                {
                    if (this.HasClothFlagChanged != null)
                    {
                        HasClothFlagChanged(this, EventArgs.Empty);
                    }
                }
            }
        }
        private bool m_hasCloth;

        /// <summary>
        /// Gets or sets a set of values the audio identifier can be set to.
        /// </summary>
        public string[] ValidAudioValues
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the number of flags supported by the prop mask.
        /// </summary>
        public int PropMaskCount 
        {
            get { return PedComponentVariation.PropMaskCountConst; }
        }

        /// <summary>
        /// The prop mask value for this variation.
        /// </summary>
        public byte PropMask 
        {
            get { return m_propMask; }
            private set
            {
                if (this.SetPropertyValue(ref this.m_propMask, value, "PropMask"))
                {
                    if (this.PropMaskValueChanged != null)
                    {
                        PropMaskValueChanged(this, EventArgs.Empty);
                    }
                }
            }
        }
        private byte m_propMask;

        /// <summary>
        /// Gets the number of user expressions this variation can have.
        /// </summary>
        public int ExpressionCount
        {
            get { return PedComponentVariation.ExpressionCountConst; }
        }

        /// <summary>
        /// An array of a specific type that represents the user
        /// expressions.
        /// </summary>
        [RSG.Base.Editor.Command.UndoableProperty()]
        public float[] Expressions
        {
            get { return m_expression; }
            private set
            {
                if (this.SetPropertyValue(ref this.m_expression, value, "Expressions"))
                {
                    if (this.ExpressionValueChanged != null)
                    {
                        ExpressionValueChanged(this, new ExpressionChangedEventArgs(1));
                    }
                }
            }
        }
        private float[] m_expression;

        /// <summary>
        /// Gets the number of flags supported by this field.
        /// </summary>
        public int FlagCount
        {
            get { return PedComponentVariation.FlagCountConst; }
        }

        /// <summary>
        /// Gets the number of flags supported by this field.
        /// </summary>
        public int NewFlagCount
        {
            get { return this._componentFlags.Values.Count - 1; }
        }

        /// <summary>
        /// Gets the byte that represents the settings of
        /// all the flags in a bit field.
        /// </summary>
        [RSG.Base.Editor.Command.UndoableProperty()]
        public UInt16 Flags
        {
            get { return m_flags; }
            private set
            {
                if (this.SetPropertyValue(ref this.m_flags, value, "Flags"))
                {
                    if (this.FlagValueChanged != null)
                    {
                        this.FlagValueChanged(this, EventArgs.Empty);
                    }
                }
            }
        }
        private UInt16 m_flags;

        /// <summary>
        /// Gets the byte that represents the settings of
        /// all the flags in a bit field.
        /// </summary>
        [RSG.Base.Editor.Command.UndoableProperty()]
        public UInt32 NewFlags
        {
            get { return m_newFlags; }
            private set
            {
                if (this.SetPropertyValue(ref this.m_newFlags, value, "NewFlags"))
                {
                    if (this.NewFlagValueChanged != null)
                    {
                        this.NewFlagValueChanged(this, EventArgs.Empty);
                    }
                }
            }
        }
        private UInt32 m_newFlags;

        /// <summary>
        /// The variations od all the textures.
        /// </summary>
        public ITextureVariation[] TextureVariations
        {
            get;
            private set;
        }

        public BitsetTunable VfxComponents
        {
            get;
            private set;
        }

        public BitsetTunable InclusionSetTunable
        {
            get;
            private set;
        }

        public BitsetTunable ExclusionSetTunable
        {
            get;
            private set;
        }
        #endregion

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="component"></param>
        public PedComponentVariation(IModel parent, IComponent component, int variationIndex, StructureTunable variationTunable, ArrayTunable textures, BoolTunable clothTunable, U8Tunable propMask, Enumeration audioIds, Enumeration componentFlags, Dictionary<string, List<string>> componentAudioIds)
            : base(parent)
        {
            this._componentFlags = componentFlags;
            string defaultAudioValue = string.Empty;
            if (componentAudioIds.ContainsKey(component.Name.ToLower()))
            {
                string componentName = component.Name.ToLower();
                this.ValidAudioValues = new string[componentAudioIds[componentName].Count];
                for (int i = 0; i < componentAudioIds[componentName].Count; i++)
                {
                    this.ValidAudioValues[i] = componentAudioIds[componentName][i];
                }

                if (audioIds != null)
                {
                    foreach (KeyValuePair<string, long> value in audioIds.Values)
                    {
                        if (value.Value == 0)
                        {
                            defaultAudioValue = value.Key;
                            break;
                        }
                    }
                    if (string.IsNullOrWhiteSpace(defaultAudioValue))
                        defaultAudioValue = audioIds.Values.First().Key;
                    if (componentAudioIds[componentName].Contains(defaultAudioValue))
                        this.FirstAudioIdentifier = defaultAudioValue;
                    else
                        this.FirstAudioIdentifier = componentAudioIds[componentName].FirstOrDefault();
                }
                else
                {
                    this.FirstAudioIdentifier = componentAudioIds[componentName].FirstOrDefault();
                }
            }
            else if (audioIds != null)
            {
                this.ValidAudioValues = new string[audioIds.Values.Count];
                int index = 0;
                foreach (KeyValuePair<string, long> value in audioIds.Values)
                {
                    this.ValidAudioValues[index++] = value.Key;
                    if (value.Value == 0)
                    {
                        defaultAudioValue = value.Key;
                        break;
                    }
                }
                if (string.IsNullOrWhiteSpace(defaultAudioValue))
                    defaultAudioValue = audioIds.Values.First().Key;

                this.FirstAudioIdentifier = defaultAudioValue;
            }

            Component = component;
            VariationIndex = variationIndex;
            Expressions = new float[ExpressionCount];

            U8Tunable inclusionTunable = (from t in variationTunable.Values.OfType<U8Tunable>()
                                          where t.Name == "pedXml_inclusionID"
                                          select t).FirstOrDefault();
            U8Tunable exclusionTunable = (from t in variationTunable.Values.OfType<U8Tunable>()
                                          where t.Name == "pedXml_exclusionID"
                                          select t).FirstOrDefault();
            StringTunable audioTunable = (from t in variationTunable.Values.OfType<StringTunable>()
                                          where t.Name == "pedXml_audioID"
                                          select t).FirstOrDefault();
            StringTunable audioTunable2 = (from t in variationTunable.Values.OfType<StringTunable>()
                                          where t.Name == "pedXml_audioID2"
                                          select t).FirstOrDefault();
            U16Tunable flagTunable = (from t in variationTunable.Values.OfType<U16Tunable>()
                                      where t.Name == "pedXml_flags"
                                      select t).FirstOrDefault();
            U32Tunable newFlagTunable = (from t in variationTunable.Values.OfType<U32Tunable>()
                                         where t.Name == "flags"
                                      select t).FirstOrDefault();
            this.InclusionSetTunable = (from t in variationTunable.Values.OfType<BitsetTunable>()
                                                 where t.Name == "inclusions"
                                                 select t).FirstOrDefault();
            this.ExclusionSetTunable = (from t in variationTunable.Values.OfType<BitsetTunable>()
                                                 where t.Name == "exclusions"
                                                 select t).FirstOrDefault();

            this.VfxComponents = (from t in variationTunable.Values.OfType<BitsetTunable>()
                             where t.Name == "pedXml_vfxComps"
                             select t).FirstOrDefault();

            if (inclusionTunable != null)
            {
                InclusionIdentifier = inclusionTunable.Value;
                InclusionIdentifierChanged += (s, e) => { inclusionTunable.Value = (byte)this.InclusionIdentifier; };
            }
            if (exclusionTunable != null)
            {
                ExclusionIdentifier = exclusionTunable.Value;
                ExculsionIdentifierChanged += (s, e) => { exclusionTunable.Value = (byte)this.ExclusionIdentifier; };
            }
            if (audioTunable != null)
            {
                FirstAudioIdentifier = audioTunable.Value;
                if (!this.ValidAudioValues.Contains(audioTunable.Value))
                    FirstAudioIdentifier = defaultAudioValue;

                FirstAudioIdentifierChanged += (s, e) => { audioTunable.Value = this.FirstAudioIdentifier; };
            }
            if (audioTunable2 != null)
            {
                SecondAudioIdentifier = audioTunable2.Value;
                if (!this.ValidAudioValues.Contains(audioTunable2.Value))
                    SecondAudioIdentifier = defaultAudioValue;

                SecondAudioIdentifierChanged += (s, e) => { audioTunable2.Value = this.SecondAudioIdentifier; };
            }
            if (propMask != null)
            {
                PropMask = propMask.Value;
                PropMaskValueChanged += (s, e) => { propMask.Value = this.PropMask; };
            }
            if (flagTunable != null)
            {
                Flags = flagTunable.Value;
                FlagValueChanged += (s, e) => { flagTunable.Value = this.Flags; };
            }
            if (newFlagTunable != null)
            {
                NewFlags = newFlagTunable.Value;
                NewFlagValueChanged += (s, e) => { newFlagTunable.Value = this.NewFlags; };
            }
            if (clothTunable != null)
            {
                this.HasCloth = (bool)clothTunable.Value;
                HasClothFlagChanged += (s, e) => { clothTunable.Value = this.HasCloth; };
            }
            if (InclusionSetTunable != null)
            {
                string tunableValue = InclusionSetTunable.Value;
                uint value = 0;
                if (!uint.TryParse(tunableValue, out value))
                {
                    if (tunableValue.StartsWith("0x", true, CultureInfo.CurrentCulture))
                    {
                        uint.TryParse(tunableValue.Substring(2), out value);
                    }
                }

                InclusionSet = value;
                InclusionSetChanged += (s, e) => { InclusionSetTunable.Value = this.InclusionSet.ToString(); };
            }
            if (ExclusionSetTunable != null)
            {
                string tunableValue = ExclusionSetTunable.Value;
                uint value = 0;
                if (!uint.TryParse(tunableValue, out value))
                {
                    if (tunableValue.StartsWith("0x", true, CultureInfo.CurrentCulture))
                    {
                        uint.TryParse(tunableValue.Substring(2), out value);
                    }
                }

                ExclusionSet = value;
                ExclusionSetChanged += (s, e) => { ExclusionSetTunable.Value = this.ExclusionSet.ToString(); };
            }


            ArrayTunable expressionArray = (from t in variationTunable.Values.OfType<ArrayTunable>()
                                            where t.Name == "pedXml_expressionMods"
                                            select t).FirstOrDefault();
            if (expressionArray != null)
            {
                for (int i = 0; i < ExpressionCount; i++)
                {
                    if (expressionArray.Items.Count > i && expressionArray.Items[i] is FloatTunable)
                    {
                        Expressions[i] = (expressionArray.Items[i] as FloatTunable).Value;
                    }
                    else
                    {
                        expressionArray.Add(TunableFactory.Create(expressionArray, (expressionArray.Definition as ArrayMember).ElementType));
                        Expressions[i] = default(float);
                    }
                }
                ExpressionValueChanged += (s, e) => { (expressionArray.Items[e.Index] as FloatTunable).Value = this.Expressions[e.Index]; };
            }
 
            int textureCount = 0;
            for (int i = 0; i < textures.Items.Count; i++)
            {
                if (textures.Items[i] is StructureTunable)
                    textureCount++;
            }

            TextureVariations = new ComponentTextureVariation[textureCount];
            for (int i = 0; i < textures.Items.Count; i++)
            {
                if (textures.Items[i] is StructureTunable)
                    TextureVariations[i] = new ComponentTextureVariation(this, textures.Items[i] as StructureTunable);
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Sets the inclusion id for the variation
        /// to the specified value.
        /// </summary>
        /// <param name="inclusion">
        /// The value to set the inclusion id to.
        /// </param>
        public void SetInclusionIdentifier(int inclusion)
        {
            this.InclusionIdentifier = inclusion;
        }

        public string ComponentFlagName(int index)
        {
            return this._componentFlags.Names[index + 1];
        }

        /// <summary>
        /// Sets the exculsion id for the variation
        /// to the specified value.
        /// </summary>
        /// <param name="exculsion">
        /// The value to set the exculsion id to.
        /// </param>
        public void SetExclusionIdentifier(int exculsion)
        {
            this.ExclusionIdentifier = exculsion;
        }

        /// <summary>
        /// Sets the inclusion id for the variation
        /// to the specified value.
        /// </summary>
        /// <param name="inclusion">
        /// The value to set the inclusion id to.
        /// </param>
        public void SetInclusionSet(uint inclusion)
        {
            this.InclusionSet = inclusion;
        }

        /// <summary>
        /// Sets the exculsion id for the variation
        /// to the specified value.
        /// </summary>
        /// <param name="exclusion">
        /// The value to set the exculsion id to.
        /// </param>
        public void SetExclusionSet(uint exclusion)
        {
            this.ExclusionSet = exclusion;
        }

        /// <summary>
        /// Sets the audio id for the variation to the specified value.
        /// </summary>
        /// <param name="audio">
        /// The value to set the audio id to.
        /// </param>
        public void SetFirstAudioIdentifier(string audio)
        {
            this.FirstAudioIdentifier = audio;
        }

        /// <summary>
        /// Sets the audio id for the variation to the specified value.
        /// </summary>
        /// <param name="audio">
        /// The value to set the audio id to.
        /// </param>
        public void SetSecondAudioIdentifier(string audio)
        {
            this.SecondAudioIdentifier = audio;
        }

        /// <summary>
        /// Sets the flag specifing whether this variation has cloth.
        /// </summary>
        /// <param name="hasCloth">
        /// The value to set the has cloth flag to.
        /// </param>
        public void SetHasClothFlag(bool hasCloth)
        {
            this.HasCloth = hasCloth;
        }

        /// <summary>
        /// Sets the flag at the specific index to the specific
        /// value.
        /// </summary>
        /// <param name="index">
        /// The index of the bit to set.
        /// </param>
        /// <param name="value">
        /// The value that the bit should be set to.
        /// </param>
        public void SetPropMaskFlag(int index, bool value)
        {
            byte flag = (byte)(0x01 << index);
            if (value)
                this.PropMask |= flag;
            else
                this.PropMask &= (byte)~flag;
        }

        /// <summary>
        /// Gets the flag at the specific index
        /// </summary>
        /// <param name="index">
        /// The index of the bit to get.
        /// </param>
        public bool GetPropMaskFlag(int index)
        {
            int flag = 0x01 << (index);
            if (((int)this.PropMask & flag) == flag)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Sets all the flags as one value.
        /// </summary>
        /// <param name="value">
        /// The value that the bit field should be
        /// </param>
        public void SetPropMask(byte value)
        {
            this.PropMask = value;
        }

        /// <summary>
        /// Sets the indexed expression to the specified value.
        /// </summary>
        /// <param name="value">
        /// The value to set the expression to.
        /// </param>
        /// <param name="index">
        /// The index of the expression to set.
        /// </param>
        public void SetUserExpressionIdentifier(float value, int index)
        {
            Debug.Assert(index < this.Expressions.Length, "Expression index out of range");
            if (index >= this.Expressions.Length)
                return;

            float[] newArray = new float[ExpressionCount];
            for (int i = 0; i < ExpressionCount; i++)
            {
                if (i == index)
                    newArray[i] = value;
                else
                    newArray[i] = this.Expressions[i];
            }
            this.Expressions = newArray;
            if (ExpressionValueChanged != null)
                ExpressionValueChanged(this, new ExpressionChangedEventArgs(index));
        }

        /// <summary>
        /// Sets the flag at the specific index to the specific
        /// value.
        /// </summary>
        /// <param name="index">
        /// The index of the bit to set.
        /// </param>
        /// <param name="value">
        /// The value that the bit should be set to.
        /// </param>
        public void SetFlag(int index, bool value)
        {
            UInt16 flag = (UInt16)(0x01 << index);
            if (value)
                this.Flags |= flag;
            else
                this.Flags &= (UInt16)~flag;
        }

        /// <summary>
        /// Gets the flag at the specific index
        /// </summary>
        /// <param name="index">
        /// The index of the bit to get.
        /// </param>
        public bool GetFlag(int index)
        {
            int flag = 0x01 << (index);
            if (((int)this.Flags & flag) == flag)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Sets all the flags as one value.
        /// </summary>
        /// <param name="value">
        /// The value that the bit field should be
        /// </param>
        public void SetFlags(UInt16 value)
        {
            this.Flags = value;
        }

        /// <summary>
        /// Sets the flag at the specific index to the specific
        /// value.
        /// </summary>
        /// <param name="index">
        /// The index of the bit to set.
        /// </param>
        /// <param name="value">
        /// The value that the bit should be set to.
        /// </param>
        public void SetNewFlag(int index, bool value)
        {
            UInt32 flag = (UInt32)(0x01 << index);
            if (value)
                this.NewFlags |= flag;
            else
                this.NewFlags &= (UInt32)~flag;
        }

        /// <summary>
        /// Gets the flag at the specific index
        /// </summary>
        /// <param name="index">
        /// The index of the bit to get.
        /// </param>
        public bool GetNewFlag(int index)
        {
            int flag = 0x01 << (index);
            if (((int)this.NewFlags & flag) == flag)
            {
                return true;
            }
            return false;
        }
        #endregion
    } // PedComponentVariation
} // RSG.Metadata.Characters
