﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Metadata.Data;
using RSG.Base.Editor;
using System.Globalization;

namespace RSG.Metadata.Characters
{
    public class PropTextureVariation :
        HierarchicalModelBase,
        ITextureVariation,
        ISetVariation
    {
        #region Events
        /// <summary>
        /// Occurs when the inculsion value changes.
        /// </summary>
        public event EventHandler InclusionIdentifierChanged;
        /// <summary>
        /// Occurs when the exculsion value changes.
        /// </summary>
        public event EventHandler ExculsionIdentifierChanged;
        /// <summary>
        /// Occurs when the distribution value changes.
        /// </summary>
        public event EventHandler DistributionValueChanged;
        /// <summary>
        /// Occurs when the texture identifier changes.
        /// </summary>
        public event EventHandler TextureIdentifierChanged;
        /// <summary>
        /// Occurs when the exculsion value changes.
        /// </summary>
        public event EventHandler InclusionSetChanged;
        /// <summary>
        /// Occurs when the exculsion value changes.
        /// </summary>
        public event EventHandler ExclusionSetChanged;
        #endregion

        #region Properties
        /// <summary>
        /// The identifiers for all of the texture variations.
        /// </summary>
        [RSG.Base.Editor.Command.UndoableProperty()]
        public int TextureIdentifier
        {
            get { return m_identifier; }
            private set
            {
                SetPropertyValue(value, m_identifier, () => this.TextureIdentifier,
                        new Base.Editor.Command.PropertySetDelegate(
                            delegate(Object newValue)
                            {
                                m_identifier = (int)newValue;
                                if (this.TextureIdentifierChanged != null)
                                    this.TextureIdentifierChanged(this, EventArgs.Empty);
                            }
                    ));
            }
        }
        private int m_identifier;

        /// <summary>
        /// The distribution rate for this variation (spawn probability).
        /// </summary>
        [RSG.Base.Editor.Command.UndoableProperty()]
        public int Distribution
        {
            get { return m_distribution; }
            private set
            {
                SetPropertyValue(value, m_distribution, () => this.Distribution,
                        new Base.Editor.Command.PropertySetDelegate(
                            delegate(Object newValue)
                            {
                                m_distribution = (int)newValue;
                                if (this.DistributionValueChanged != null)
                                    this.DistributionValueChanged(this, EventArgs.Empty);
                            }
                    ));
            }
        }
        private int m_distribution;

        /// <summary>
        /// The id to specify the inclusion set for this variation.
        /// </summary>
        [RSG.Base.Editor.Command.UndoableProperty()]
        public int InclusionIdentifier
        {
            get { return m_inclusion; }
            private set
            {
                SetPropertyValue(value, m_inclusion, () => this.InclusionIdentifier,
                        new Base.Editor.Command.PropertySetDelegate(
                            delegate(Object newValue)
                            {
                                m_inclusion = (int)newValue;
                                if (this.InclusionIdentifierChanged != null)
                                    this.InclusionIdentifierChanged(this, EventArgs.Empty);
                            }
                    ));
            }
        }
        private int m_inclusion;

        /// <summary>
        /// The id to specify the exclusion set for this variation.
        /// </summary>
        [RSG.Base.Editor.Command.UndoableProperty()]
        public int ExclusionIdentifier
        {
            get { return m_exclusion; }
            private set
            {
                SetPropertyValue(value, m_exclusion, () => this.ExclusionIdentifier,
                        new Base.Editor.Command.PropertySetDelegate(
                            delegate(Object newValue)
                            {
                                m_exclusion = (int)newValue;
                                if (this.ExculsionIdentifierChanged != null)
                                    this.ExculsionIdentifierChanged(this, EventArgs.Empty);
                            }
                    ));
            }
        }
        private int m_exclusion;

        /// <summary>
        /// The id to specify the inclusion set for this variation.
        /// </summary>
        [RSG.Base.Editor.Command.UndoableProperty()]
        public uint InclusionSet
        {
            get { return m_inclusionSet; }
            private set
            {
                SetPropertyValue(value, m_inclusionSet, () => this.InclusionSet,
                        new Base.Editor.Command.PropertySetDelegate(
                            delegate(Object newValue)
                            {
                                m_inclusionSet = (uint)newValue;
                                if (this.InclusionSetChanged != null)
                                    this.InclusionSetChanged(this, EventArgs.Empty);
                            }
                    ));
            }
        }
        private uint m_inclusionSet;

        /// <summary>
        /// The id to specify the exclusion set for this variation.
        /// </summary>
        [RSG.Base.Editor.Command.UndoableProperty()]
        public uint ExclusionSet
        {
            get { return m_exclusionSet; }
            private set
            {
                SetPropertyValue(value, m_exclusionSet, () => this.ExclusionSet,
                        new Base.Editor.Command.PropertySetDelegate(
                            delegate(Object newValue)
                            {
                                m_exclusionSet = (uint)newValue;
                                if (this.ExclusionSetChanged != null)
                                    this.ExclusionSetChanged(this, EventArgs.Empty);
                            }
                    ));
            }
        }
        private uint m_exclusionSet;

        public BitsetTunable InclusionSetTunable
        {
            get;
            private set;
        }

        public BitsetTunable ExclusionSetTunable
        {
            get;
            private set;
        }
        #endregion

        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        public PropTextureVariation(IModel parent, StructureTunable variationTunable)
            : base(parent)
        {
            U8Tunable inclusionTunable = (from t in variationTunable.Values.OfType<U8Tunable>()
                                          where t.Name == "inclusionId"
                                          select t).FirstOrDefault();
            U8Tunable exculsionTunable = (from t in variationTunable.Values.OfType<U8Tunable>()
                                          where t.Name == "exclusionId"
                                          select t).FirstOrDefault();
            U8Tunable textureIdTunable = (from t in variationTunable.Values.OfType<U8Tunable>()
                                          where t.Name == "texId"
                                          select t).FirstOrDefault();
            U8Tunable distributionTunable = (from t in variationTunable.Values.OfType<U8Tunable>()
                                             where t.Name == "distribution"
                                             select t).FirstOrDefault();
            this.InclusionSetTunable = (from t in variationTunable.Values.OfType<BitsetTunable>()
                                                 where t.Name == "inclusions"
                                                 select t).FirstOrDefault();
            this.ExclusionSetTunable = (from t in variationTunable.Values.OfType<BitsetTunable>()
                                                 where t.Name == "exclusions"
                                                 select t).FirstOrDefault();

            if (inclusionTunable != null)
            {
                InclusionIdentifier = inclusionTunable.Value;
                InclusionIdentifierChanged += (s, e) => { inclusionTunable.Value = (byte)this.InclusionIdentifier; };
            }
            if (exculsionTunable != null)
            {
                ExclusionIdentifier = exculsionTunable.Value;
                ExculsionIdentifierChanged += (s, e) => { exculsionTunable.Value = (byte)this.ExclusionIdentifier; };
            }
            if (textureIdTunable != null)
            {
                TextureIdentifier = textureIdTunable.Value;
                TextureIdentifierChanged += (s, e) => { textureIdTunable.Value = (byte)this.TextureIdentifier; };
            }
            if (distributionTunable != null)
            {
                Distribution = distributionTunable.Value;
                DistributionValueChanged += (s, e) => { distributionTunable.Value = (byte)this.Distribution; };
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Sets the inclusion id for the variation
        /// to the specified value.
        /// </summary>
        /// <param name="inclusion">
        /// The value to set the inclusion id to.
        /// </param>
        public void SetInclusionIdentifier(int inclusion)
        {
            this.InclusionIdentifier = inclusion;
        }

        /// <summary>
        /// Sets the exculsion id for the variation
        /// to the specified value.
        /// </summary>
        /// <param name="exculsion">
        /// The value to set the exculsion id to.
        /// </param>
        public void SetExclusionIdentifier(int exculsion)
        {
            this.ExclusionIdentifier = exculsion;
        }

        /// <summary>
        /// Sets the texture identifier for this variation.
        /// </summary>
        /// <param name="distribution">
        /// The value to set the texture identifier to.
        /// </param>
        public void SetTextureIdentifier(int identifier)
        {
            this.TextureIdentifier = identifier;
        }

        /// <summary>
        /// Sets the distribution rate for this variation.
        /// </summary>
        /// <param name="distribution">
        /// The value to set the distribution to.
        /// </param>
        public void SetDistribution(int distribution)
        {
            this.Distribution = distribution;
        }

        /// <summary>
        /// Sets the inclusion id for the variation
        /// to the specified value.
        /// </summary>
        /// <param name="inclusion">
        /// The value to set the inclusion id to.
        /// </param>
        public void SetInclusionSet(uint inclusion)
        {
            this.InclusionSet = inclusion;
        }

        /// <summary>
        /// Sets the exculsion id for the variation
        /// to the specified value.
        /// </summary>
        /// <param name="exclusion">
        /// The value to set the exculsion id to.
        /// </param>
        public void SetExclusionSet(uint exclusion)
        {
            this.ExclusionSet = exclusion;
        }
        #endregion
    } // PropTextureVariation
} // RSG.Metadata.Characters
