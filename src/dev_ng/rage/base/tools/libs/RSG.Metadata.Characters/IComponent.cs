﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Metadata.Characters
{
    public interface IComponent
    {
        #region Properties
        /// <summary>
        /// The name that represents this component
        /// on a creature (i.e. Head).
        /// </summary>
        string Name { get; }

        /// <summary>
        /// The index of this component. Use to reference components
        /// in variations.
        /// </summary>
        int Index { get; }
        #endregion
    } // IComponent
} // RSG.Metadata.Characters
