﻿
namespace RSG.Metadata.Characters
{
    /// <summary>
    /// 
    /// </summary>
    public class PedProp : IProp
    {
        #region Properties
        /// <summary>
        /// The name that represents this props attachment
        /// point
        /// </summary>
        public string AttachmentPoint
        { 
            get;
            private set;
        }

        /// <summary>
        /// The index of this prop in turns of it anchor.
        /// </summary>
        public int AnchorIndex
        {
            get;
            private set;
        }

        /// <summary>
        /// Represents whether this prop has alpha properties.
        /// </summary>
        public bool HasAlpha
        {
            get;
            private set;
        }

        /// <summary>
        /// Represents whether this prop has decals.
        /// </summary>
        public bool HasDecal
        {
            get;
            private set;
        }

        /// <summary>
        /// Represents whether this prop has cutouts.
        /// </summary>
        public bool HasCutout
        {
            get;
            private set;
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="RSG.Metadata.Characters.PedProp"/> class.
        /// </summary>
        /// <param name="point"></point>
        /// <param name="index"></param>
        /// <param name="alpha"></param>
        /// <param name="decal"></param>
        /// <param name="cutout"></param>
        public PedProp(string point, int anchorIndex, bool alpha, bool decal, bool cutout)
        {
            AttachmentPoint = point;
            AnchorIndex = anchorIndex;
            HasAlpha = alpha;
            HasDecal = decal;
            HasCutout = cutout;
        }
        #endregion
    } // PedProp
} // RSG.Metadata.Characters
