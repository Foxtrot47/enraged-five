﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Metadata.Data;

namespace RSG.Metadata.Characters
{
    public class SelectionSet
    {
        #region Fields
        /// <summary>
        /// The private reference to the tunable that stores the data used by this class.
        /// </summary>
        private StructureTunable _tunable;

        /// <summary>
        /// The private reference to the tunable that stores the data used for the component
        /// drawable indices.
        /// </summary>
        private ArrayTunable _drawableIndices;

        /// <summary>
        /// The private reference to the tunable that stores the data used for the component
        /// texture indices.
        /// </summary>
        private ArrayTunable _textureIndices;

        /// <summary>
        /// The private reference to the tunable that stores the data used for the prop anchor
        /// indices.
        /// </summary>
        private ArrayTunable _anchorIndices;

        /// <summary>
        /// The private reference to the tunable that stores the data used for the prop
        /// drawable indices.
        /// </summary>
        private ArrayTunable _propDrawableIndices;

        /// <summary>
        /// The private reference to the tunable that stores the data used for the prop texture
        /// indices.
        /// </summary>
        private ArrayTunable _propTextureIndices;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="SelectionSet"/> class using the
        /// specified structure tunable as a data provider.
        /// </summary>
        /// <param name="tunable">
        /// The tunable that houses the data used by this class.
        /// </param>
        public SelectionSet(StructureTunable tunable)
        {
            this._tunable = tunable;
            this._drawableIndices = this._tunable["compDrawableId"] as ArrayTunable;
            this._textureIndices = this._tunable["compTexId"] as ArrayTunable;
            this._anchorIndices = this._tunable["propAnchorId"] as ArrayTunable;
            this._propDrawableIndices = this._tunable["propDrawableId"] as ArrayTunable;
            this._propTextureIndices = this._tunable["propTexId"] as ArrayTunable;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public string Name
        {
            get { return (this._tunable["name"] as StringTunable).Value; }
            set { (this._tunable["name"] as StringTunable).Value = value; }
        }
        #endregion Properties

        #region Methods
        public void SetDrawableId(int index, byte value)
        {
            (this._drawableIndices.Items[index] as U8Tunable).Value = value;
        }

        public byte GetDrawableId(int index)
        {
            return (this._drawableIndices.Items[index] as U8Tunable).Value;
        }

        public void SetTextureId(int index, byte value)
        {
            (this._textureIndices.Items[index] as U8Tunable).Value = value;
        }

        public byte GetTextureId(int index)
        {
            return (this._textureIndices.Items[index] as U8Tunable).Value;
        }

        public void SetAnchorId(int index, byte value)
        {
            (this._anchorIndices.Items[index] as U8Tunable).Value = value;
        }

        public byte GetAnchorId(int index)
        {
            return (this._anchorIndices.Items[index] as U8Tunable).Value;
        }

        public void SetPropDrawableId(int index, byte value)
        {
            (this._propDrawableIndices.Items[index] as U8Tunable).Value = value;
        }

        public byte GetPropDrawableId(int index)
        {
            return (this._propDrawableIndices.Items[index] as U8Tunable).Value;
        }

        public void SetPropTextureId(int index, byte value)
        {
            (this._propTextureIndices.Items[index] as U8Tunable).Value = value;
        }

        public byte GetPropTextureId(int index)
        {
            return (this._propTextureIndices.Items[index] as U8Tunable).Value;
        }

        public void ResetValuesToDefault()
        {
            foreach (U8Tunable tunable in this._drawableIndices.OfType<U8Tunable>())
            {
                tunable.Value = 255;
            }

            foreach (U8Tunable tunable in this._textureIndices.OfType<U8Tunable>())
            {
                tunable.Value = 255;
            }

            foreach (U8Tunable tunable in this._propDrawableIndices.OfType<U8Tunable>())
            {
                tunable.Value = 255;
            }

            foreach (U8Tunable tunable in this._propTextureIndices.OfType<U8Tunable>())
            {
                tunable.Value = 255;
            }

            foreach (U8Tunable tunable in this._anchorIndices.OfType<U8Tunable>())
            {
                tunable.Value = 255;
            }
        }
        #endregion Methods
    }
}
