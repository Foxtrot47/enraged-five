﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Metadata.Data;
using RSG.Metadata.Parser;

namespace RSG.Metadata.Characters
{
    /// <summary>
    /// Represents all of the selection sets within a ped variation file.
    /// </summary>
    public class SelectionSets
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Sets"/> property.
        /// </summary>
        private List<SelectionSet> _sets;

        /// <summary>
        /// The private reference to the array tunable to stores the data used by this class.
        /// </summary>
        private ArrayTunable _arrayTunable;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="SelectionSets"/> class using the
        /// specified array tunable as a data provider.
        /// </summary>
        /// <param name="arrayTunable">
        /// The array tunable that houses the data used by this class.
        /// </param>
        public SelectionSets(ArrayTunable arrayTunable)
        {
            if (arrayTunable == null)
            {
                throw new ArgumentNullException("arrayTunable");
            }

            this._arrayTunable = arrayTunable;
            this._sets = new List<SelectionSet>();
            foreach (ITunable item in this._arrayTunable.Items)
            {
                SelectionSet newSet = new SelectionSet(item as StructureTunable);
                this._sets.Add(newSet);
            }
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets a iterator around the selection sets.
        /// </summary>
        public IEnumerable<SelectionSet> Sets
        {
            get
            {
                if (this._sets == null)
                {
                    yield break;
                }

                foreach (SelectionSet set in this._sets)
                {
                    yield return set;
                }
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name">
        /// 
        /// </param>
        public SelectionSet AddSelectionSet(string name)
        {
            if (this._arrayTunable == null)
            {
                return null;
            }

            IMember itemType = (this._arrayTunable.Definition as ArrayMember).ElementType;
            ITunable tunable = TunableFactory.Create(this._arrayTunable, itemType);

            SelectionSet newSet = new SelectionSet(tunable as StructureTunable);
            newSet.ResetValuesToDefault();

            newSet.Name = name;
            this._sets.Add(newSet);
            this._arrayTunable.Add(tunable);
            return newSet;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index">
        /// 
        /// </param>
        public void RemoveSelectionSet(int index)
        {
            this._sets.RemoveAt(index);
            this._arrayTunable.RemoveAt(index);
        }
        #endregion Methods
    }
}
