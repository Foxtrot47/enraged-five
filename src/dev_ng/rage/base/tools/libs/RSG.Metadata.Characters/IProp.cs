﻿
namespace RSG.Metadata.Characters
{
    /// <summary>
    /// 
    /// </summary>
    public interface IProp
    {
        #region Properties
        /// <summary>
        /// The name that represents this props attachment
        /// point
        /// </summary>
        string AttachmentPoint { get; }

        /// <summary>
        /// The index of this prop.
        /// </summary>
        int AnchorIndex { get; }

        /// <summary>
        /// Represents whether this prop has alpha properties.
        /// </summary>
        bool HasAlpha { get; }

        /// <summary>
        /// Represents whether this prop has decals.
        /// </summary>
        bool HasDecal { get; }

        /// <summary>
        /// Represents whether this prop has cutouts.
        /// </summary>
        bool HasCutout { get; }
        #endregion
    } // IProp
} // RSG.Metadata.Characters
