﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Metadata.Data;
using RSG.Base.Editor;
using RSG.Metadata.Parser;

namespace RSG.Metadata.Characters
{
    /// <summary>
    /// 
    /// </summary>
    public class PedVariation : HierarchicalModelBase, ICreatureVariation
    {
        #region Constants
        /// <summary>
        /// 
        /// </summary>
        private static List<string> ComponentNames = new List<string>()
        {
            "Head",
            "Berd",
            "Hair",
            "Uppr",
            "Lowr",
            "Hand",
            "Feet",
            "Teef",
            "Accs",
            "Task",
            "Decl",
            "Jbib"
        };

        /// <summary>
        /// 
        /// </summary>
        private static List<string> AttachmentPoints = new List<string>()
        {
            "p_head",
            "p_eyes",
            "p_ears",
            "p_mouth",
            "p_lhand",
            "p_rhand",
            "p_lwrist",
            "p_rwrist",
            "p_hip",
            "p_lfoot",
            "p_rfoot",
            "ph_lhand",
            "ph_rhand"
        };

        private static string AVAILABLE_COMPONENT_NODE = "availComp";
        private static string COMPONENT_DATA_NODE = "aComponentData3";
        private static string DRAWABLE_DATA_NODE = "aDrawblData3";
        private static string COMPONENT_INFOS_NODE = "compInfos";
        private static string PROP_INFO_NODE = "propInfo";
        #endregion

        #region Properties
        /// <summary>
        /// The list of available components this creature
        /// has to set variations on.
        /// </summary>
        public IComponent[] Components
        {
            get;
            private set;
        }

        /// <summary>
        /// A list of all the variations for all the
        /// components available in for the creature.
        /// </summary>
        public IComponentVariation[] ComponentVariations
        {
            get;
            private set;
        }

        /// <summary>
        /// The list of unique prop attachments this creature
        /// has to set variations on.
        /// </summary>
        public IProp[] Props
        {
            get;
            private set;
        }

        /// <summary>
        /// A list of all the variations for all the
        /// props available in for the creature.
        /// </summary>
        public IPropVariation[] PropVariations
        {
            get;
            private set;
        }
        #endregion

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        public PedVariation(IModel parent, StructureTunable rootTunable, Enumeration audioIds, Enumeration componentFlags, Dictionary<string, List<string>> componentAudioIds, Enumeration anchorPointEnumeration)
            : base(parent)
        {
            List<int> componentIndices = new List<int>();
            foreach (ITunable child in rootTunable.Values.Where(c => c is ArrayTunable && c.Name == AVAILABLE_COMPONENT_NODE))
            {
                int index = 0;
                foreach (ITunable comp in (child as ArrayTunable).Items.Where(c => c is IntegerTunableBase<byte>))
                {
                    if ((comp as IntegerTunableBase<byte>).Value != 255)
                    {
                        componentIndices.Add(index);
                    }
                    index++;
                }
                break;
            }

            List<List<ArrayTunable>> componentTextures = GetComponentTextures(rootTunable);
            List<List<BoolTunable>> hasClothTunables = GetClothTunable(rootTunable);
            List<List<U8Tunable>> propMasks = GetPropMasks(rootTunable);
            Components = new IComponent[componentIndices.Count];
            for (int i = 0; i < componentIndices.Count; i++)
            {
                int index = componentIndices[i];
                string name = PedVariation.ComponentNames[index];
                PedComponent component = new PedComponent(name, index);
                Components[i] = component;
            }

            IEnumerable<StructureTunable> compInfos = from t in rootTunable.Values.OfType<ArrayTunable>()
                                              where t.Name == COMPONENT_INFOS_NODE
                                              from i in t.Items.OfType<StructureTunable>()
                                              select i;
            CreateComponentVariations(compInfos.ToList(), componentTextures, hasClothTunables, propMasks, audioIds, componentFlags, componentIndices, componentAudioIds);

            List<string> anchorPoints = new List<string>();
            if (anchorPointEnumeration != null)
            {
                anchorPoints.AddRange(anchorPointEnumeration.Values.Keys);
            }
            else
            {
                anchorPoints.Add("ANCHOR_HEAD");
                anchorPoints.Add("ANCHOR_EYES");
                anchorPoints.Add("ANCHOR_EARS");
                anchorPoints.Add("ANCHOR_MOUTH");
                anchorPoints.Add("ANCHOR_LEFT_HAND");
                anchorPoints.Add("ANCHOR_RIGHT_HAND");
                anchorPoints.Add("ANCHOR_LEFT_WRIST");
                anchorPoints.Add("ANCHOR_RIGHT_WRIST");
                anchorPoints.Add("ANCHOR_HIP");
                anchorPoints.Add("ANCHOR_LEFT_FOOT");
                anchorPoints.Add("ANCHOR_RIGHT_FOOT");
                anchorPoints.Add("ANCHOR_PH_L_HAND");
                anchorPoints.Add("ANCHOR_PH_R_HAND");
            }


            foreach (StructureTunable propInfo in rootTunable.Values.Where(c => c is StructureTunable && c.Name == PROP_INFO_NODE))
            {
                UInt16 hasAlpha = 0;
                UInt16 hasDecal = 0;
                UInt16 hasCutout = 0;
                List<int> props = new List<int>();
                ArrayTunable metadata = null;
                int propCountInData = 0;
                foreach (KeyValuePair<string, ITunable> child in propInfo)
                {
                    if (child.Key == "numAvailProps")
                    {
                        propCountInData = (child.Value as U8Tunable).Value;
                    }
                    else if (child.Key == "hasAlpha")
                    {
                        hasAlpha = (child.Value as U16Tunable).Value;
                    }
                    else if (child.Key == "hasDecal")
                    {
                        hasDecal = (child.Value as U16Tunable).Value;
                    }
                    else if (child.Key == "hasCutout")
                    {
                        hasCutout = (child.Value as U16Tunable).Value;
                    }
                    else if (child.Key == "aMaxPropId")
                    {
                        int index = 0;
                        foreach (IntegerTunableBase<byte> item in (child.Value as ArrayTunable).Items.Where(c => c is IntegerTunableBase<byte>))
                        {
                            for (int i = 0; i < item.Value; i++)
                                props.Add(index);

                            index++;
                        }
                    }
                    else if (child.Key == "aPropMetaData")
                    {
                        metadata = (child.Value as ArrayTunable);
                    }
                    else if (child.Key == "aAnchors")
                    {
                        int anchorIndex = -1;
                        foreach (ITunable item in (child.Value as ArrayTunable).Items)
                        {
                            anchorIndex++;
                            StructureTunable structureItem = item as StructureTunable;
                            if (structureItem == null)
                            {
                                continue;
                            }

                            int anchorPropVariationCount = (structureItem["props"] as ArrayTunable).Items.Count;
                            props.AddRange(Enumerable.Repeat((int)(structureItem["anchor"] as EnumTunable).Value, anchorPropVariationCount));
                        }
                    }
                }

                if (propCountInData != props.Count)
                {
                    throw new ParserException("Unable to open ped variation file due to the fact the value in numAvailProps doesn't match the data in the aAnchors array.\r\nRe-exporting the ped should fix this.");
                }

                this.Props = new PedProp[props.Count];
                this.PropVariations = new PedPropVariation[props.Count];
                for (int i = 0; i < props.Count; i++)
                {
                    int flag = 0x01 << i;
                    bool alpha = ((int)hasAlpha & flag) == flag;
                    bool decal = ((int)hasDecal & flag) == flag;
                    bool cutout = ((int)hasCutout & flag) == flag;
                    this.Props[i] = new PedProp(anchorPoints[props[i]], props[i], alpha, decal, cutout);
                }

                if (metadata != null)
                {
                    int index = 0;
                    foreach (StructureTunable tunable in metadata.Items.Where(c => c is StructureTunable))
                    {
                        this.PropVariations[index] = new PedPropVariation(this, this.Props[index], tunable, componentFlags);
                        index++;
                    }
                }
                break;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="rootTunable"></param>
        /// <returns></returns>
        private List<List<ArrayTunable>> GetComponentTextures(StructureTunable rootTunable)
        {
            List<List<ArrayTunable>> componentTextures = new List<List<ArrayTunable>>();
            IEnumerable<StructureTunable> componentData = from t in rootTunable.Values.OfType<ArrayTunable>()
                                                          where t.Name == COMPONENT_DATA_NODE
                                                          from i in t.Items.OfType<StructureTunable>()
                                                          select i;

            foreach (StructureTunable component in componentData)
            {
                IEnumerable<StructureTunable> drawableData = from t in component.Values.OfType<ArrayTunable>()
                                                             where t.Name == DRAWABLE_DATA_NODE
                                                             from i in t.Items.OfType<StructureTunable>()
                                                             select i;

                IEnumerable<ArrayTunable> textureData = from s in drawableData
                                                        from t in s.Values.OfType<ArrayTunable>()
                                                        where t.Name == "aTexData"
                                                        select t;

                componentTextures.Add(textureData.ToList());
            }

            return componentTextures;
        }

        private List<List<BoolTunable>> GetClothTunable(StructureTunable rootTunable)
        {
            List<List<BoolTunable>> clothTunables = new List<List<BoolTunable>>();
            IEnumerable<StructureTunable> componentData = from t in rootTunable.Values.OfType<ArrayTunable>()
                                                          where t.Name == COMPONENT_DATA_NODE
                                                          from i in t.Items.OfType<StructureTunable>()
                                                          select i;

            var componentDataList = componentData.ToList();
            foreach (StructureTunable component in componentData)
            {
                IEnumerable<StructureTunable> drawableData = from t in component.Values.OfType<ArrayTunable>()
                                                             where t.Name == DRAWABLE_DATA_NODE
                                                             from i in t.Items.OfType<StructureTunable>()
                                                             select i;

                var drawableDataList = drawableData.ToList();

                IEnumerable<StructureTunable> clothData = from s in drawableData
                                                        from t in s.Values.OfType<StructureTunable>()
                                                        where t.Name == "clothData"
                                                        select t;

                var clothDataList = clothData.ToList();

                IEnumerable<BoolTunable> ownsClothData = from a in clothData
                                                         from i in a.Values.OfType<BoolTunable>()
                                                         where i.Name == "ownsCloth"
                                                         select i;

                var ownsClothDataList = ownsClothData.ToList();

                clothTunables.Add(ownsClothData.ToList());
            }

            return clothTunables;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rootTunable"></param>
        /// <returns></returns>
        private List<List<U8Tunable>> GetPropMasks(StructureTunable rootTunable)
        {
            List<List<U8Tunable>> propMasks = new List<List<U8Tunable>>();
            IEnumerable<StructureTunable> componentData = from t in rootTunable.Values.OfType<ArrayTunable>()
                                                          where t.Name == COMPONENT_DATA_NODE
                                                          from i in t.Items.OfType<StructureTunable>()
                                                          select i;

            foreach (StructureTunable component in componentData)
            {
                IEnumerable<StructureTunable> drawableData = from t in component.Values.OfType<ArrayTunable>()
                                                             where t.Name == DRAWABLE_DATA_NODE
                                                             from i in t.Items.OfType<StructureTunable>()
                                                             select i;

                IEnumerable<U8Tunable> maskData = from s in drawableData
                                                     from t in s.Values.OfType<U8Tunable>()
                                                     where t.Name == "propMask"
                                                     select t;

                propMasks.Add(maskData.ToList());
            }
            return propMasks;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="compInfos"></param>
        private void CreateComponentVariations(List<StructureTunable> compInfos, List<List<ArrayTunable>> componentTextures, List<List<BoolTunable>> hasClothTunables, List<List<U8Tunable>> propMasks, Enumeration audioIds, Enumeration componentFlags, List<int> componentIndices, Dictionary<string, List<string>> componentAudioIds)
        {
            ComponentVariations = new IComponentVariation[compInfos.Count];
            Dictionary<int, int> variationCounts = new Dictionary<int,int>();
            var componentList = this.Components.ToList();
            for (int i = 0; i < compInfos.Count; i++)
            {
                byte rawComponentIndex = GetComponentIndex(compInfos, i);
                int componentIndex = -1;
                for (int j = 0; j < componentIndices.Count; j++)
                {
                    if (componentIndices[j] == rawComponentIndex)
                    {
                        componentIndex = j;
                        break;
                    }
                }
                if (componentIndex == -1 || componentIndex > (componentList.Count - 1))
                    continue;

                IComponent component = componentList[componentIndex];
                if (component == null)
                    continue;

                if (!variationCounts.ContainsKey(componentIndex))
                    variationCounts.Add(componentIndex, 0);

                int variationIndex = variationCounts[componentIndex]++;
                U8Tunable propMask = propMasks[componentIndex][variationIndex];
                ArrayTunable textures = componentTextures[componentIndex][variationIndex];
                BoolTunable hasCloth = hasClothTunables[componentIndex][variationIndex];
                ComponentVariations[i] = new PedComponentVariation(this, component, variationIndex, compInfos[i], textures, hasCloth, propMask, audioIds, componentFlags, componentAudioIds);
            }
        }

        private byte GetComponentIndex(List<StructureTunable> compInfos, int i)
        {
            return (from t in compInfos[i].Values.OfType<IntegerTunableBase<byte>>()
                    where t.Name == "pedXml_compIdx"
                    select t.Value).FirstOrDefault();
        }
        #endregion
    } // PedVariation
} // RSG.Metadata.Characters.PedVariation
