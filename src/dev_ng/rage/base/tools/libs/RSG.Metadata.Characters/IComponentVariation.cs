﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Metadata.Characters
{
    public interface IComponentVariation
    {
        #region Properties
        /// <summary>
        /// The index of the component that this is a 
        /// variation of.
        /// </summary>
        int ComponentIndex { get; }

        /// <summary>
        /// Gets the instance of the component that 
        /// this variation is set on.
        /// </summary>
        IComponent Component { get; }
        #endregion
    } // IComponentVariation
} // RSG.Metadata.Characters
