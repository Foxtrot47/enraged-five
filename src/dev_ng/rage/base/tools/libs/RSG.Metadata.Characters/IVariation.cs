﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Metadata.Characters
{
    public interface IVariation
    {
        #region Properties
        /// <summary>
        /// The name of this variation
        /// </summary>
        string Name { get; }
        #endregion
    } // IVariation

    public interface ISetVariation
    {
        #region Events
        /// <summary>
        /// Occurs when the inculsion value changes.
        /// </summary>
        event EventHandler InclusionIdentifierChanged;
        /// <summary>
        /// Occurs when the exculsion value changes.
        /// </summary>
        event EventHandler ExculsionIdentifierChanged;
        /// <summary>
        /// Occurs when the exculsion value changes.
        /// </summary>
        event EventHandler InclusionSetChanged;
        /// <summary>
        /// Occurs when the exculsion value changes.
        /// </summary>
        event EventHandler ExclusionSetChanged;
        #endregion

        #region Properties
        /// <summary>
        /// The id to specify the inclusion set for this variation.
        /// </summary>
        int InclusionIdentifier { get; }

        /// <summary>
        /// The id to specify the inclusion set for this variation.
        /// </summary>
        uint InclusionSet { get; } 

        /// <summary>
        /// The id to specify the exclusion set for this variation.
        /// </summary>
        int ExclusionIdentifier { get; }

        /// <summary>
        /// The id to specify the exclusion set for this variation.
        /// </summary>
        uint ExclusionSet { get; } 
        #endregion

        #region Methods
        /// <summary>
        /// Sets the inclusion id for the variation
        /// to the specified value.
        /// </summary>
        /// <param name="inclusion">
        /// The value to set the inclusion id to.
        /// </param>
        void SetInclusionIdentifier(int inclusion);

        /// <summary>
        /// Sets the exculsion id for the variation
        /// to the specified value.
        /// </summary>
        /// <param name="exclusion">
        /// The value to set the exculsion id to.
        /// </param>
        void SetExclusionIdentifier(int exclusion);

        /// <summary>
        /// Sets the inclusion id for the variation
        /// to the specified value.
        /// </summary>
        /// <param name="inclusion">
        /// The value to set the inclusion id to.
        /// </param>
        void SetInclusionSet(uint inclusion);

        /// <summary>
        /// Sets the exculsion id for the variation
        /// to the specified value.
        /// </summary>
        /// <param name="exclusion">
        /// The value to set the exculsion id to.
        /// </param>
        void SetExclusionSet(uint exclusion);
        #endregion
    } // IVariation

    public interface IAudioVariation
    {
        #region Events
        /// <summary>
        /// Occurs when the audio value changes.
        /// </summary>
        event EventHandler FirstAudioIdentifierChanged;
        /// <summary>
        /// Occurs when the audio value changes.
        /// </summary>
        event EventHandler SecondAudioIdentifierChanged;
        #endregion

        #region Properties
        /// <summary>
        /// The id to specify the audio setting for this variation.
        /// </summary>
        string FirstAudioIdentifier { get; }

        /// <summary>
        /// The id to specify the audio setting for this variation.
        /// </summary>
        string SecondAudioIdentifier { get; }
        #endregion

        #region Methods
        /// <summary>
        /// Sets the audio id for the variation to the specified value.
        /// </summary>
        /// <param name="audio">
        /// The value to set the audio id to.
        /// </param>
        void SetFirstAudioIdentifier(string audio);

        /// <summary>
        /// Sets the audio id for the variation to the specified value.
        /// </summary>
        /// <param name="audio">
        /// The value to set the audio id to.
        /// </param>
        void SetSecondAudioIdentifier(string audio);
        #endregion
    } // IAudioVariation

    public interface IHasClothFlag
    {
        #region Events
        /// <summary>
        /// Occurs when the has cloth value changes.
        /// </summary>
        event EventHandler HasClothFlagChanged;
        #endregion

        #region Properties
        /// <summary>
        /// Gets a value indicating whether this component variation has cloth in it.
        /// </summary>
        bool HasCloth { get; }
        #endregion

        #region Methods
        /// <summary>
        /// Sets the flag specifing whether this variation has cloth.
        /// </summary>
        /// <param name="hasCloth">
        /// The value to set the has cloth flag to.
        /// </param>
        void SetHasClothFlag(bool hasCloth);
        #endregion
    } // IHasClothFlag

    public interface IPropMaskVariation
    {
        #region Events
        /// <summary>
        /// Occurs when the prop mask value changes.
        /// </summary>
        event EventHandler PropMaskValueChanged;
        #endregion

        #region Properties
        /// <summary>
        /// Gets the number of flags supported by the prop mask.
        /// </summary>
        int PropMaskCount { get; }

        /// <summary>
        /// The prop mask value for this variation.
        /// </summary>
        byte PropMask { get; }
        #endregion

        #region Methods
        /// <summary>
        /// Sets the flag at the specific index to the specific
        /// value.
        /// </summary>
        /// <param name="index">
        /// The index of the bit to set.
        /// </param>
        /// <param name="value">
        /// The value that the bit should be set to.
        /// </param>
        void SetPropMaskFlag(int index, bool value);

        /// <summary>
        /// Gets the flag at the specific index
        /// </summary>
        /// <param name="index">
        /// The index of the bit to get.
        /// </param>
        bool GetPropMaskFlag(int index);

        /// <summary>
        /// Sets all the flags as one value.
        /// </summary>
        /// <param name="value">
        /// The value that the bit field should be
        /// </param>
        void SetPropMask(byte value);
        #endregion
    } // IPropMaskVariation

    public class ExpressionChangedEventArgs : EventArgs
    {
        #region Properties
        /// <summary>
        /// The index of the expression whos values changed.
        /// </summary>
        public int Index
        {
            get;
            private set;
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="RSG.Metadata.Characters.ExpressionChangedEventArgs"/>
        /// class.
        /// </summary>
        /// <param name="index">
        /// The index of the expression that changed.
        /// </param>
        public ExpressionChangedEventArgs(int index)
        {
            Index = index;
        }
        #endregion
    }

    public interface IUserExpressionVariation<T>
    {
        #region Events
        /// <summary>
        /// Occurs when a expression value changes.
        /// </summary>
        event EventHandler<ExpressionChangedEventArgs> ExpressionValueChanged;
        #endregion

        #region Properties
        /// <summary>
        /// Gets the number of user expressions this variation can have.
        /// </summary>
        int ExpressionCount { get; }

        /// <summary>
        /// An array of a specific type that represents the user
        /// expressions.
        /// </summary>
        T[] Expressions { get; }
        #endregion

        #region Methods
        /// <summary>
        /// Sets the indexed expression to the specified value.
        /// </summary>
        /// <param name="value">
        /// The value to set the expression to.
        /// </param>
        /// <param name="index">
        /// The index of the expression to set.
        /// </param>
        void SetUserExpressionIdentifier(T value, int index);
        #endregion
    } // IAudioVariation

    public interface IFlagVariation
    {
        #region Events
        /// <summary>
        /// Occurs when the flag value changes.
        /// </summary>
        event EventHandler FlagValueChanged;
        #endregion

        #region Properties
        /// <summary>
        /// Gets the number of flags supported by this field.
        /// </summary>
        int FlagCount { get; }
        #endregion

        #region Methods
        /// <summary>
        /// Sets the flag at the specific index to the specific
        /// value.
        /// </summary>
        /// <param name="index">
        /// The index of the bit to set.
        /// </param>
        /// <param name="value">
        /// The value that the bit should be set to.
        /// </param>
        void SetFlag(int index, bool value);

        /// <summary>
        /// Gets the flag at the specific index
        /// </summary>
        /// <param name="index">
        /// The index of the bit to get.
        /// </param>
        bool GetFlag(int index);

        /// <summary>
        /// Sets the flag at the specific index to the specific
        /// value.
        /// </summary>
        /// <param name="index">
        /// The index of the bit to set.
        /// </param>
        /// <param name="value">
        /// The value that the bit should be set to.
        /// </param>
        void SetNewFlag(int index, bool value);

        /// <summary>
        /// Gets the flag at the specific index
        /// </summary>
        /// <param name="index">
        /// The index of the bit to get.
        /// </param>
        bool GetNewFlag(int index);
        #endregion
    } // IFlagVariation

    public interface IFlagVariation<T> : IFlagVariation
    {
        #region Properties
        /// <summary>
        /// Gets the byte that represents the settings of
        /// all the flags in a bit field.
        /// </summary>
        T Flags { get; }
        #endregion

        #region Methods
        /// <summary>
        /// Sets all the flags as one value.
        /// </summary>
        /// <param name="value">
        /// The value that the bit field should be
        /// </param>
        void SetFlags(T value);
        #endregion
    } // IFlagVariation<T>

    public interface IDistributionVariation
    {
        #region Events
        /// <summary>
        /// Occurs when the distribution value changes.
        /// </summary>
        event EventHandler DistributionValueChanged;
        #endregion

        #region Properties
        /// <summary>
        /// The distribution rate for this variation (spawn probability).
        /// </summary>
        int Distribution { get; }
        #endregion

        #region Methods
        /// <summary>
        /// Sets the distribution rate for this variation.
        /// </summary>
        /// <param name="distribution">
        /// The value to set the distribution to.
        /// </param>
        void SetDistribution(int distribution);
        #endregion
    } // IDistributionVariation

    public interface IStickynessVariation
    {
        #region Events
        /// <summary>
        /// Occurs when the stickyness value changes.
        /// </summary>
        event EventHandler StickynessValueChanged;
        #endregion

        #region Properties
        /// <summary>
        /// The stickness factor for this variation. The probability
        /// it'll stay on when damaged.
        /// </summary>
        int Stickyness { get; }
        #endregion

        #region Methods
        /// <summary>
        /// Sets the stickness factor for this variation.
        /// </summary>
        /// <param name="distribution">
        /// The value to set the stickness factor to.
        /// </param>
        void SetStickyness(int stickyness);
        #endregion
    } // ISticknessVariation

    public interface ITextureVariation : IDistributionVariation
    {
        #region Events
        /// <summary>
        /// Occurs when the texture identifier changes.
        /// </summary>
        event EventHandler TextureIdentifierChanged;
        #endregion

        #region Properties
        /// <summary>
        /// The identifiers for all of the texture variations.
        /// </summary>
        int TextureIdentifier { get; }
        #endregion

        #region Methods
        /// <summary>
        /// Sets the texture identifier for this variation.
        /// </summary>
        /// <param name="distribution">
        /// The value to set the texture identifier to.
        /// </param>
        void SetTextureIdentifier(int identifier);
        #endregion
    } // ITextureVariation

    public interface IHasTextureVariations
    {
        #region Properties
        /// <summary>
        /// The variations od all the textures.
        /// </summary>
        ITextureVariation[] TextureVariations { get; }
        #endregion
    } // IHasTextureVariations
} // RSG.Metadata.Characters
