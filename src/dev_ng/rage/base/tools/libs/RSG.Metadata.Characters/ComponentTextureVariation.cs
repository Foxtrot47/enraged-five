﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Metadata.Data;
using RSG.Base.Editor;

namespace RSG.Metadata.Characters
{
    public class ComponentTextureVariation :
        HierarchicalModelBase,
        ITextureVariation
    {
        #region Events
        /// <summary>
        /// Occurs when the distribution value changes.
        /// </summary>
        public event EventHandler DistributionValueChanged;
        /// <summary>
        /// Occurs when the texture identifier changes.
        /// </summary>
        public event EventHandler TextureIdentifierChanged;
        #endregion

        #region Properties
        /// <summary>
        /// The identifiers for all of the texture variations.
        /// </summary>
        [RSG.Base.Editor.Command.UndoableProperty()]
        public int TextureIdentifier
        {
            get { return m_identifier; }
            private set
            {
                SetPropertyValue(value, m_identifier, () => this.TextureIdentifier,
                        new Base.Editor.Command.PropertySetDelegate(
                            delegate(Object newValue)
                            {
                                m_identifier = (int)newValue;
                                if (this.TextureIdentifierChanged != null)
                                    this.TextureIdentifierChanged(this, EventArgs.Empty);
                            }
                    ));
            }
        }
        private int m_identifier;

        /// <summary>
        /// The distribution rate for this variation (spawn probability).
        /// </summary>
        [RSG.Base.Editor.Command.UndoableProperty()]
        public int Distribution
        {
            get { return m_distribution; }
            private set
            {
                SetPropertyValue(value, m_distribution, () => this.Distribution,
                        new Base.Editor.Command.PropertySetDelegate(
                            delegate(Object newValue)
                            {
                                m_distribution = (int)newValue;
                                if (this.DistributionValueChanged != null)
                                    this.DistributionValueChanged(this, EventArgs.Empty);
                            }
                    ));
            }
        }
        private int m_distribution;
        #endregion

        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        public ComponentTextureVariation(IModel parent, StructureTunable variationTunable)
            : base(parent)
        {
            U8Tunable textureIdTunable = (from t in variationTunable.Values.OfType<U8Tunable>()
                                          where t.Name == "texId"
                                          select t).FirstOrDefault();
            U8Tunable distributionTunable = (from t in variationTunable.Values.OfType<U8Tunable>()
                                             where t.Name == "distribution"
                                             select t).FirstOrDefault();

            if (textureIdTunable != null)
            {
                TextureIdentifier = textureIdTunable.Value;
                TextureIdentifierChanged += (s, e) => { textureIdTunable.Value = (byte)this.TextureIdentifier; };
            }
            if (distributionTunable != null)
            {
                Distribution = distributionTunable.Value;
                DistributionValueChanged += (s, e) => { distributionTunable.Value = (byte)this.Distribution; };
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Sets the texture identifier for this variation.
        /// </summary>
        /// <param name="distribution">
        /// The value to set the texture identifier to.
        /// </param>
        public void SetTextureIdentifier(int identifier)
        {
            this.TextureIdentifier = identifier;
        }

        /// <summary>
        /// Sets the distribution rate for this variation.
        /// </summary>
        /// <param name="distribution">
        /// The value to set the distribution to.
        /// </param>
        public void SetDistribution(int distribution)
        {
            this.Distribution = distribution;
        }
        #endregion
    } // PropTextureVariation
} // RSG.Metadata.Characters
