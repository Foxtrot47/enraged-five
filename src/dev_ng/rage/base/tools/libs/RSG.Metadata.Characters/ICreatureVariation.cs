﻿
namespace RSG.Metadata.Characters
{
    /// <summary>
    /// 
    /// </summary>
    public interface ICreatureVariation
    {
        #region Properties
        /// <summary>
        /// The list of available components this creature
        /// has to set variations on.
        /// </summary>
        IComponent[] Components { get; }

        /// <summary>
        /// A list of all the variations for all the
        /// components available in for the creature.
        /// </summary>
        IComponentVariation[] ComponentVariations { get; }

        /// <summary>
        /// The list of unique prop attachments this creature
        /// has to set variations on.
        /// </summary>
        IProp[] Props { get; }

        /// <summary>
        /// A list of all the variations for all the
        /// props available in for the creature.
        /// </summary>
        IPropVariation[] PropVariations { get; }
        #endregion
    } // ICreatureVariation
} // RSG.Metadata.Characters
