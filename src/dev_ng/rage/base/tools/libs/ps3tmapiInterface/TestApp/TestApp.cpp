// TestApp.cpp : main project file.

#include "stdafx.h"

using namespace System;
using namespace System::Threading;

using namespace ps3tmapiInterface;

#define DOWORK_THREADED 0

void PrintConnectError(System::Object^  sender, MessageEventArgs^  e)
{
	Console::WriteLine( "[TEST APP] ConnectError: " + e->Message );
}

void PrintConnectInfo(System::Object^  sender, MessageEventArgs^  e)
{
	Console::WriteLine( "[TEST APP] ConnectInfo: " + e->Message );
}

void PrintConnectStatus(System::Object^  sender, ConnectStatusEventArgs^  e)
{
	Console::WriteLine( "[TEST APP] " + e->AsString() );
}

void PrintDebugEvent(System::Object^  sender, DebugEventEventArgs^  e)
{
	Console::WriteLine( "[TEST APP] " + e->AsString() );
}

void PrintTargetEvent(System::Object^  sender, TargetEventEventArgs^  e)
{
	Console::WriteLine( "[TEST APP] " + e->AsString() );
}

void PrintTTY( System::Object^  sender, MessageEventArgs^  e )
{
	if ( e->Message->StartsWith( "Error: " ) )
	{
		Console::ForegroundColor = ConsoleColor::Red;
		Console::Write( e->Message );
		Console::ResetColor();
	}
	else if ( e->Message->StartsWith( "Warning: " ) )
	{
		Console::ForegroundColor = ConsoleColor::Yellow;
		Console::Write( e->Message );
		Console::ResetColor();
	}
	else
	{
		Console::Write( e->Message );
	}
}

void PrintUnhandledCrash(System::Object^  sender, MessageEventArgs^  e )
{
	Console::Write( "[TEST APP] UnhandledCrash: " + e->Message );
}

void PrintUnitStatus(System::Object^  sender, UnitStatusEventArgs^  e)
{
	Console::WriteLine( "[TEST APP] " + e->AsString() );
}


void DoWork()
{
	PS3Target^ target = gcnew PS3Target();

	try
	{
		target->ConnectError += gcnew MessageEventHandler( &PrintConnectError );
		target->ConnectInfo += gcnew MessageEventHandler( &PrintConnectInfo );
		target->ConnectStatus += gcnew ConnectStatusEventHandler( &PrintConnectStatus );
		target->DebugEvent += gcnew DebugEventEventHandler( &PrintDebugEvent );
		target->OutputTTY = true;
		target->TargetEvent += gcnew TargetEventEventHandler( &PrintTargetEvent );
		target->TTY += gcnew MessageEventHandler( &PrintTTY );
		target->UnhandledCrash += gcnew MessageEventHandler( &PrintUnhandledCrash );
		target->UnitStatus += gcnew UnitStatusEventHandler( &PrintUnitStatus );	

		target->Connect();

		Console::WriteLine( "[TEST APP] Press Q to terminate the program." );

		while ( target->IsConnected )
		{
			target->Update();

			if ( Console::KeyAvailable )
			{
				ConsoleKeyInfo cki;
				cki = Console::ReadKey( true );
				if ( cki.Key == ConsoleKey::Q )
				{
					Console::WriteLine( "[TEST APP] RESETTING TARGET.  PLEASE WAIT." );
					target->Reset();
					break;
				}
			}
		}
	}
	finally
	{
		Console::WriteLine( "[TEST APP] DISCONNECTING FROM TARGET" );
		target->Disconnect();

		delete target;
	}
}

int main(array<System::String ^> ^args)
{

#if DOWORK_THREADED
	Thread^ thread = gcnew Thread( gcnew ThreadStart(DoWork) );
	thread->Start();

	while ( thread->IsAlive )
	{
		// do nothing
	}

	delete thread;
#else
	DoWork();
#endif

    return 0;
}
