#pragma once

#include "ps3tmapiInterface.h"
#include "PS3TargetEvents.h"

#include <vcclr.h>

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Diagnostics;
using namespace System::Threading;


namespace ps3tmapiInterface {

	/// <summary>
	/// Summary for PS3TargetComponent
	/// </summary>
	public ref class PS3TargetComponent :  public System::ComponentModel::Component
	{
	public:
		PS3TargetComponent(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//

			m_ps3Target = gcnew PS3Target();
		}
	
		PS3TargetComponent(System::ComponentModel::IContainer ^container)
		{
			/// <summary>
			/// Required for Windows.Forms Class Composition Designer support
			/// </summary>

			container->Add(this);
			InitializeComponent();

			m_ps3Target = gcnew PS3Target();
		}

#pragma region Properties
		property bool IsConnected
		{
			bool get()
			{				
				return m_ps3Target->IsConnected;
			}
		}

		property bool OutputTTY
		{
			bool get()
			{
				return m_ps3Target->OutputTTY;
			}
			void set( bool value )
			{
				m_ps3Target->OutputTTY = value;
			}
		}
#pragma endregion

#pragma region Events
		event MessageEventHandler^ ConnectError
		{
		public:
			void add( MessageEventHandler^ e )
			{
				m_ps3Target->ConnectError += e;
			}

			void remove( MessageEventHandler^ e )
			{
				m_ps3Target->ConnectError -= e;
			}

		private:
			void raise( System::Object^ sender, MessageEventArgs^ e )
			{
				// do nothing, m_ps3Target will raise the event
			}
		}

		event MessageEventHandler^ ConnectInfo
		{
		public:
			void add( MessageEventHandler^ e )
			{
				m_ps3Target->ConnectInfo += e;
			}

			void remove( MessageEventHandler^ e )
			{
				m_ps3Target->ConnectInfo -= e;
			}

		private:
			void raise( System::Object^ sender, MessageEventArgs^ e )
			{
				// do nothing, m_ps3Target will raise the event
			}
		}

		event ConnectStatusEventHandler^ ConnectStatus
		{
		public:
			void add( ConnectStatusEventHandler^ e )
			{
				m_ps3Target->ConnectStatus += e;
			}

			void remove( ConnectStatusEventHandler^ e )
			{
				m_ps3Target->ConnectStatus -= e;
			}

		private:
			void raise( System::Object^ sender, ConnectStatusEventArgs^ e )
			{
				// do nothing, m_ps3Target will raise the event
			}
		}

		event DebugEventEventHandler^ DebugEvent
		{
		public:
			void add( DebugEventEventHandler^ e )
			{
				m_ps3Target->DebugEvent += e;
			}

			void remove( DebugEventEventHandler^ e )
			{
				m_ps3Target->DebugEvent -= e;
			}

		private:
			void raise( System::Object^ sender, DebugEventEventArgs^ e )
			{
				// do nothing, m_ps3Target will raise the event
			}
		}

		event TargetEventEventHandler^ TargetEvent
		{
		public:
			void add( TargetEventEventHandler^ e )
			{
				m_ps3Target->TargetEvent += e;
			}

			void remove( TargetEventEventHandler^ e )
			{
				m_ps3Target->TargetEvent -= e;
			}

		private:
			void raise( System::Object^ sender, TargetEventEventArgs^ e )
			{
				// do nothing, m_ps3Target will raise the event
			}
		}

		event MessageEventHandler^ TTY
		{
		public:
			void add( MessageEventHandler^ e )
			{
				m_ps3Target->TTY += e;
			}

			void remove( MessageEventHandler^ e )
			{
				m_ps3Target->TTY -= e;
			}

		private:
			void raise( System::Object^ sender, MessageEventArgs^ e )
			{
				// do nothing, m_ps3Target will raise the event
			}
		}

		event MessageEventHandler^ UnhandledCrash
		{
		public:
			void add( MessageEventHandler^ e )
			{
				m_ps3Target->UnhandledCrash += e;
			}

			void remove( MessageEventHandler^ e )
			{
				m_ps3Target->UnhandledCrash -= e;
			}

		private:
			void raise( System::Object^ sender, MessageEventArgs^ e )
			{
				// do nothing, m_ps3Target will raise the event
			}
		}

		event UnitStatusEventHandler^ UnitStatus
		{
		public:
			void add( UnitStatusEventHandler^ e )
			{
				m_ps3Target->UnitStatus += e;
			}

			void remove( UnitStatusEventHandler^ e )
			{
				m_ps3Target->UnitStatus -= e;
			}

		private:
			void raise( System::Object^ sender, UnitStatusEventArgs^ e )
			{
				// do nothing, m_ps3Target will raise the event
			}
		}
#pragma endregion

#pragma region Public Functions
		void Connect()
		{
			Connect( System::String::Empty );
		}

		void Connect( System::String^ ipAddress )
		{
			if ( m_ps3Target->Connect( ipAddress ) )
			{
				this->timer->Start();
			}
		}

		void Disconnect()
		{
			this->timer->Stop();

			m_ps3Target->Disconnect();			
		}

		void Reset()
		{
			m_ps3Target->Reset();
		}
#pragma endregion

	private: System::Windows::Forms::Timer^  timer;
	public: 

	private: 
		PS3Target^ m_ps3Target;

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~PS3TargetComponent()
		{
			if (components)
			{
				delete components;
			}

			this->Disconnect();
		}

	protected: 
	private: System::ComponentModel::IContainer^  components;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->timer = (gcnew System::Windows::Forms::Timer(this->components));
			// 
			// timer
			// 
			this->timer->Interval = 20;
			this->timer->Tick += gcnew System::EventHandler(this, &PS3TargetComponent::timer_Tick);

		}
#pragma endregion
	private: 
		System::Void timer_Tick(System::Object^  sender, System::EventArgs^  e) 
		{
			m_ps3Target->Update();
		}
};	
}
