#include "stdafx.h"

#include "targetManager.h"

#include <atlsync.h>
#include <map>
#include <queue>

namespace ps3tmapiInterface
{	
#pragma unmanaged
	struct TargetConnectionInfo
	{
		SNPS3TargetInfo TargetInfo;
		TMAPI_TCPIP_CONNECT_PROP ConnectionInfo;
		std::queue<std::string> TargetMessageQueue;
		std::queue<std::string> TTYMessageQueue;
	};

	bool g_targetHandlesInitialized = false;
	std::map<HTARGET, TargetConnectionInfo*> g_targets;
	std::map<HTARGET, TargetConnectionInfo*> g_targetsInUse;
	std::map<HTARGET, bool > g_targetsConnected;

	void AddTargetMessage( HTARGET hTarget, const std::string &msg )
	{
		std::map<HTARGET, TargetConnectionInfo*>::iterator iter = g_targets.find( hTarget );
		if ( iter != g_targets.end() )
		{
			iter->second->TargetMessageQueue.push( msg );
		}
		else
		{
			printf( "%s\n", msg.c_str() );
		}
	}

	void AddTTYMessage( HTARGET hTarget, const std::string &msg )
	{
		std::map<HTARGET, TargetConnectionInfo*>::iterator iter = g_targets.find( hTarget );
		if ( iter != g_targets.end() )
		{
			iter->second->TTYMessageQueue.push( msg );
		}
		else
		{
			printf( "%s\n", msg.c_str() );
		}
	}

	void SetTargetConnected( HTARGET hTarget, bool connected )
	{
		std::map<HTARGET, bool>::iterator iter = g_targetsConnected.find( hTarget );
		if ( iter != g_targetsConnected.end() )
		{
			iter->second = connected;
		}
	}

	HTARGET GetTargetFromIP( const std::string &ipAddress )
	{
		ECONNECTSTATUS   nStatus = CS_UNAVAILABLE;
		char*  pszUsage = 0;

		std::map<HTARGET, TargetConnectionInfo*>::iterator iter = g_targets.begin();
		while (iter != g_targets.end())
		{
			if ( g_targetsInUse.find( iter->first ) != g_targetsInUse.end() )
			{
				++iter;
				continue;
			}

			if ( strcmp(ipAddress.c_str(), iter->second->ConnectionInfo.szIPAddress) == 0 )
			{
				SNRESULT nRes = SNPS3GetConnectStatus(iter->first, &nStatus, &pszUsage);
				if (SN_SUCCEEDED( nRes ))
				{
					// if disconnected, attempt to connect.
					if ( nStatus != CS_CONNECTED )
					{
						if ( SN_FAILED( SNPS3Connect(iter->first, NULL) ) )
						{
							char msg[256];
							sprintf_s( msg, 256, "Err;Failed to connect to Target %s (%s)", 
								iter->second->TargetInfo.pszName, iter->second->ConnectionInfo.szIPAddress );
							AddTargetMessage( iter->first, msg );
							++iter;
							continue;
						}

						nRes = SNPS3GetConnectStatus(iter->first, &nStatus, &pszUsage);
					}

					if (nStatus == CS_CONNECTED)
					{
						g_targetsInUse[iter->first] = iter->second;

						char msg[256];
						sprintf_s( msg, 256, "Info;Connected to Target %s (%s)", 
							iter->second->TargetInfo.pszName, iter->second->ConnectionInfo.szIPAddress );
						AddTargetMessage( iter->first, msg );

						return iter->first;
					}
				}
			}

			++iter;
		}

		return -1;
	}

	HTARGET FindAvailableTarget( const std::string &ipAddress )
	{
		ECONNECTSTATUS   nStatus = CS_UNAVAILABLE;
		char*  pszUsage = 0;

		if ( g_targets.size() == 0 )
		{
			return -1;
		}

		HTARGET hTarget = -1;
		if ( ipAddress != "" )
		{
			hTarget = GetTargetFromIP( ipAddress );
			if ( hTarget != -1 )
			{
				return hTarget;
			}
		}

		std::map<HTARGET, TargetConnectionInfo*>::iterator iter = g_targets.begin();
		
		if ( g_targets.size() == 1 )
		{
			SNRESULT nRes = SNPS3GetConnectStatus(iter->first, &nStatus, &pszUsage);
			if (SN_SUCCEEDED( nRes ))
			{
				// if disconnected, attempt to connect.  we will only do this if we have only 1 target
				if ( nStatus != CS_CONNECTED )
				{
					if ( SN_FAILED( SNPS3Connect(iter->first, NULL) ) )
					{
						char msg[256];
						sprintf_s( msg, 256, "Err;Failed to connect to Target %s (%s)", 
							iter->second->TargetInfo.pszName, iter->second->ConnectionInfo.szIPAddress );
						AddTargetMessage( iter->first, msg );						
					}
					else
					{
						nRes = SNPS3GetConnectStatus(iter->first, &nStatus, &pszUsage);
					}
				}

				if (nStatus == CS_CONNECTED)
				{
					g_targetsInUse[iter->first] = iter->second;

					char msg[256];
					sprintf_s( msg, 256, "Info;Connected to Target %s (%s)", 
						iter->second->TargetInfo.pszName, iter->second->ConnectionInfo.szIPAddress );
					AddTargetMessage( iter->first, msg );

					return iter->first;
				}
			}
		}
		else
		{
			while (iter != g_targets.end())
			{
				if ( g_targetsInUse.find( iter->first ) != g_targetsInUse.end() )
				{
					continue;
				}

				SNRESULT nRes = SNPS3GetConnectStatus(iter->first, &nStatus, &pszUsage);
				if (SN_SUCCEEDED( nRes ))
				{
					if (nStatus == CS_CONNECTED)
					{
						g_targetsInUse[iter->first] = iter->second;

						char msg[256];
						sprintf_s( msg, 256, "Info;Connected to Target %s (%s)", 
							iter->second->TargetInfo.pszName, iter->second->ConnectionInfo.szIPAddress );
						AddTargetMessage( iter->first, msg );

						g_targetsConnected[iter->first] = true;
						return iter->first;
					}
					else
					{
						g_targetsConnected[iter->first] = false;
					}
				}

				++iter;
			}
		}

		return -1;
	}

	int EnumCallBack(HTARGET hTarget)
	{
		TargetConnectionInfo* pTargetConnectionInfo = new TargetConnectionInfo;

		if ( pTargetConnectionInfo )
		{
			SNPS3TargetInfo ti;
			ti.hTarget = hTarget;
			ti.nFlags = SN_TI_TARGETID;

			int result1 = SNPS3GetTargetInfo( &ti );
			if ( SN_S_OK == result1 )
			{
				// Store target parameters.
				pTargetConnectionInfo->TargetInfo.hTarget = hTarget;
				pTargetConnectionInfo->TargetInfo.pszName = _strdup(ti.pszName);
				pTargetConnectionInfo->TargetInfo.pszHomeDir = _strdup(ti.pszHomeDir);
				pTargetConnectionInfo->TargetInfo.pszFSDir = _strdup(ti.pszFSDir);
			}

			TMAPI_TCPIP_CONNECT_PROP ci;
			int result2 = SNPS3GetConnectionInfo( hTarget, &ci );
			if ( SN_SUCCEEDED( result2 ) )
			{
				strcpy_s( pTargetConnectionInfo->ConnectionInfo.szIPAddress, sizeof(pTargetConnectionInfo->ConnectionInfo.szIPAddress), ci.szIPAddress );
				pTargetConnectionInfo->ConnectionInfo.uPort = ci.uPort;
			}

			if ( (SN_S_OK == result1) && SN_SUCCEEDED( result2 ) )
			{				
				// Store this target.
				g_targets[hTarget] = pTargetConnectionInfo;

				ECONNECTSTATUS   nStatus = CS_UNAVAILABLE;
				char*  pszUsage = 0;

				SNRESULT nRes = SNPS3GetConnectStatus(hTarget, &nStatus, &pszUsage);
				if (SN_SUCCEEDED( nRes ))
				{
					g_targetsConnected[hTarget] = nStatus == CS_CONNECTED;
				}
			}
			else
			{
				// Terminate enumeration.
				return 1;
			}
		}

		// Carry on with enumeration.
		g_targetHandlesInitialized = true;
		return 0;
	}

	void ProcessTargetSpecificEvent(HTARGET hTarget, UINT uDataLen, BYTE *pData)
	{
		SNPS3_DBG_EVENT_HDR *pDbgHeader = (SNPS3_DBG_EVENT_HDR *)pData;
		SNPS3_DBG_EVENT_DATA *pDbgData = (SNPS3_DBG_EVENT_DATA *)
			(pData + sizeof(SNPS3_DBG_EVENT_HDR));

		char s[256];
		s[0] = 0;

		switch (pDbgData->uEventType)
		{
		case SNPS3_DBG_EVENT_PROCESS_CREATE:
			sprintf_s( s, 256, "Dbg;PROCESS_CREATE" );
			break;
		case SNPS3_DBG_EVENT_PROCESS_EXIT:
			{
				UINT64 u64ExitCode = pDbgData->ppu_process_exit.uExitCode;
				UINT32 u32ExitCode = (UINT32)(u64ExitCode >> 32);

				sprintf_s( s, 256, "Dbg;PROCESS_EXIT;%u", u32ExitCode );
			}
			break;
		case SNPS3_DBG_EVENT_PPU_EXP_TRAP:
			sprintf_s( s, 256, "Dbg:PPU_EXP_TRAP;%u;%u;", pDbgData->ppu_exc_trap.uPPUThreadID, pDbgData->ppu_exc_trap.uHWThreadNumber );
			break;
		case SNPS3_DBG_EVENT_PPU_EXP_PREV_INT:
			sprintf_s( s, 256, "Dbg;PPU_EXP_PREV_INT;%u;%u", pDbgData->ppu_exc_prev_int.uPPUThreadID, pDbgData->ppu_exc_prev_int.uHWThreadNumber );
			break;
		case SNPS3_DBG_EVENT_PPU_EXP_ALIGNMENT:
			sprintf_s( s, 256, "Dbg;PPU_EXP_ALIGNMENT;%u;%u", pDbgData->ppu_exc_alignment.uPPUThreadID, pDbgData->ppu_exc_alignment.uHWThreadNumber );
			break;
		case SNPS3_DBG_EVENT_PPU_EXP_ILL_INST:
			sprintf_s( s, 256, "Dbg;PPU_EXP_ILL_INST;%u;%u", pDbgData->ppu_exc_ill_inst.uPPUThreadID, pDbgData->ppu_exc_ill_inst.uHWThreadNumber );
			break;
		case SNPS3_DBG_EVENT_PPU_EXP_TEXT_HTAB_MISS:
			sprintf_s( s, 256, "Dbg;PPU_EXP_TEXT_HTAB_MISS;%u;%u", pDbgData->ppu_exc_text_htab_miss.uPPUThreadID, pDbgData->ppu_exc_text_htab_miss.uHWThreadNumber );
			break;
		case SNPS3_DBG_EVENT_PPU_EXP_TEXT_SLB_MISS:
			sprintf_s( s, 256, "Dbg;PPU_EXP_TEXT_SLB_MISS;%u;%u", pDbgData->ppu_exc_text_slb_miss.uPPUThreadID, pDbgData->ppu_exc_text_slb_miss.uHWThreadNumber );
			break;
		case SNPS3_DBG_EVENT_PPU_EXP_DATA_HTAB_MISS:
			sprintf_s( s, 256, "Dbg;PPU_EXP_DATA_HTAB_MISS;%u;%u", pDbgData->ppu_exc_data_htab_miss.uPPUThreadID, pDbgData->ppu_exc_data_htab_miss.uHWThreadNumber );
			break;
		case SNPS3_DBG_EVENT_PPU_EXP_FLOAT:
			sprintf_s( s, 256, "Dbg;PPU_EXP_FLOAT;%u;%u", pDbgData->ppu_exc_float.uPPUThreadID, pDbgData->ppu_exc_float.uHWThreadNumber );
			break;
		case SNPS3_DBG_EVENT_PPU_EXP_DATA_SLB_MISS:
			sprintf_s( s, 256, "Dbg;PPU_EXP_DATA_SLB_MISS;%u;%u", pDbgData->ppu_exc_data_slb_miss.uPPUThreadID, pDbgData->ppu_exc_data_slb_miss.uHWThreadNumber );
			break;
		case SNPS3_DBG_EVENT_PPU_EXP_DABR_MATCH:
			sprintf_s( s, 256, "Dbg;PPU_EXP_DABR_MATCH;%u;%u", pDbgData->ppu_exc_dabr_match.uPPUThreadID, pDbgData->ppu_exc_dabr_match.uHWThreadNumber );
			break;
		case SNPS3_DBG_EVENT_PPU_EXP_STOP:
			sprintf_s( s, 256, "Dbg;PPU_EXP_STOP;%u;%u", pDbgData->ppu_exc_stop.uPPUThreadID, pDbgData->ppu_exc_stop.uHWThreadNumber );
			break;
		case SNPS3_DBG_EVENT_PPU_EXP_STOP_INIT:
			sprintf_s( s, 256, "Dbg;PPU_EXP_STOP_INIT;%u;%u", pDbgData->ppu_exc_stop_init.uPPUThreadID, pDbgData->ppu_exc_stop_init.uHWThreadNumber );
			break;
		case SNPS3_DBG_EVENT_PPU_THREAD_CREATE:
			sprintf_s( s, 256, "Dbg;PPU_THREAD_CREATE;%u", pDbgData->ppu_thread_create.uPPUThreadID );
			break;
		case SNPS3_DBG_EVENT_PPU_THREAD_EXIT:
			sprintf_s( s, 256, "Dbg;PPU_THREAD_EXIT;%u", pDbgData->ppu_thread_exit.uPPUThreadID );
			break;
		case SNPS3_DBG_EVENT_SPU_THREAD_START:
			sprintf_s( s, 256, "Dbg;SPU_THREAD_START;%u;%u", pDbgData->spu_thread_start.uSPUThreadID, pDbgData->spu_thread_start.uSPUThreadGroupID );
			break;
		case SNPS3_DBG_EVENT_SPU_THREAD_STOP:
			sprintf_s( s, 256, "Dbg;SPU_THREAD_STOP;%u;%u", pDbgData->spu_thread_stop.uSPUThreadID, pDbgData->spu_thread_stop.uSPUThreadGroupID );
			break;
		case SNPS3_DBG_EVENT_SPU_THREAD_STOP_INIT:
			sprintf_s( s, 256, "Dbg;SPU_THREAD_STOP_INIT;%u;%u", pDbgData->spu_thread_stop_init.uSPUThreadID, pDbgData->spu_thread_stop_init.uSPUThreadGroupID );
			break;
		case SNPS3_DBG_EVENT_SPU_THREAD_GROUP_DESTROY:
			sprintf_s( s, 256, "Dbg;SPU_THREAD_GROUP_DESTROY;%u", pDbgData->spu_thread_group_destroy.uSPUThreadGroupID );
			break;
		default:
			break;
		}

		if ( strlen(s) > 0 )
		{
			AddTargetMessage( hTarget, s );
		}
	}

	void ProcessTargetEvent(HTARGET hTarget, UINT uDataLen, BYTE *pData)
	{
		UINT uDataRemaining = uDataLen;
		BYTE *pDataPtr = pData;

		while (uDataRemaining)
		{
			SN_EVENT_TARGET_HDR *pHeader = (SN_EVENT_TARGET_HDR *)pDataPtr;

			char s[256];
			s[0] = 0;

			switch (pHeader->uEvent)
			{
			case SN_TGT_EVENT_UNIT_STATUS_CHANGE:
				{
					SN_TGT_EVENT_UNIT_STATUS_CHANGE_DATA *pStatusData =
						(SN_TGT_EVENT_UNIT_STATUS_CHANGE_DATA *)
						(pDataPtr + sizeof(SN_EVENT_TARGET_HDR));
					EUNITSTATUS status = (EUNITSTATUS)pStatusData->uStatus;

					switch ( status )
					{
					case US_UNKNOWN:             // Unknown state.
						sprintf_s( s, 256, "Us;UNKNOWN;%u", pStatusData->uUnit );
						break;
					case US_RUNNING:             // Unit is executing.
						sprintf_s( s, 256, "Us;RUNNING;%u", pStatusData->uUnit );
						break;
					case US_STOPPED:             // Unit has stopped execution.
						sprintf_s( s, 256, "Us;STOPPED;%u", pStatusData->uUnit );
						break;
					case US_SIGNALLED:           // A signal occurred on the unit.
						sprintf_s( s, 256, "Us;SIGNALLED;%u", pStatusData->uUnit );
						break;
					case US_RESETTING:           // Unit is in the process of resetting.
						sprintf_s( s, 256, "Us;RESETTING;%u", pStatusData->uUnit );
						break;
					case US_MISSING:             // Unit not present.
						sprintf_s( s, 256, "Us;MISSING;%u", pStatusData->uUnit );
						break;
					case US_RESET:               // Unit has completed a reset.
						sprintf_s( s, 256, "Us;RESET;%u", pStatusData->uUnit );
						break;
					case US_NOT_CONNECTED:       // Unit is not connected (NOT USED).
						sprintf_s( s, 256, "Us;NOT_CONNECTED;%u", pStatusData->uUnit );
						SetTargetConnected( hTarget, false );
						break;
					case US_CONNECTED:           // Unit is connected (NOT USED).
						sprintf_s( s, 256, "Us;CONNECTED;%u", pStatusData->uUnit );
						break;
					case US_UNIT_STATUS_CHANGE:   // Notification of unit status change.
						sprintf_s( s, 256, "Us;UNIT_STATUS_CHANGE;%u", pStatusData->uUnit );
						break;
					}
				}
				break;
			case SN_TGT_EVENT_RESET_STARTED:
				sprintf_s( s, 256, "Tgt;RESET_STARTED" );
				break;
			case SN_TGT_EVENT_RESET_END:
				sprintf_s( s, 256, "Tgt;RESET_END" );
				break;
			case SN_TGT_EVENT_DETAILS:
				{
					SN_TGT_EVENT_DETAILS_DATA *pStatusData =
						(SN_TGT_EVENT_DETAILS_DATA *)
						(pDataPtr + sizeof(SN_EVENT_TARGET_HDR));

					sprintf_s( s, 256, "Tgt;DETAILS;%u", pStatusData->uFlags );
				}
				break;
			case SN_TGT_EVENT_MODULE_LOAD:
				{
					SN_TGT_EVENT_MODULE_EVENT_DATA *pModuleData =
						(SN_TGT_EVENT_MODULE_EVENT_DATA*)
						(pDataPtr + sizeof(SN_EVENT_TARGET_HDR));

					sprintf_s( s, 256, "Tgt;MODULE_LOAD;%u;%u", pModuleData->uUnit, pModuleData->uModuleId );
				}
				break;
			case SN_TGT_EVENT_MODULE_RUNNING:
				{
					SN_TGT_EVENT_MODULE_EVENT_DATA *pModuleData =
						(SN_TGT_EVENT_MODULE_EVENT_DATA*)
						(pDataPtr + sizeof(SN_EVENT_TARGET_HDR));

					sprintf_s( s, 256, "Tgt;MODULE_RUNNING;%u;%u", pModuleData->uUnit, pModuleData->uModuleId );
				}
				break;
			case SN_TGT_EVENT_MODULE_DONE_REMOVE:
				{
					SN_TGT_EVENT_MODULE_EVENT_DATA *pModuleData =
						(SN_TGT_EVENT_MODULE_EVENT_DATA*)
						(pDataPtr + sizeof(SN_EVENT_TARGET_HDR));

					sprintf_s( s, 256, "Tgt;MODULE_DONE_REMOVE;%u;%u", pModuleData->uUnit, pModuleData->uModuleId );
				}
				break;
			case SN_TGT_EVENT_MODULE_DONE_RESIDENT:
				{
					SN_TGT_EVENT_MODULE_EVENT_DATA *pModuleData =
						(SN_TGT_EVENT_MODULE_EVENT_DATA*)
						(pDataPtr + sizeof(SN_EVENT_TARGET_HDR));

					sprintf_s( s, 256, "Tgt;MODULE_DONE_RESIDENT;%u;%u", pModuleData->uUnit, pModuleData->uModuleId );
				}
				break;
			case SN_TGT_EVENT_MODULE_STOPPED:
				{
					SN_TGT_EVENT_MODULE_EVENT_DATA *pModuleData =
						(SN_TGT_EVENT_MODULE_EVENT_DATA*)
						(pDataPtr + sizeof(SN_EVENT_TARGET_HDR));

					sprintf_s( s, 256, "Tgt;MODULE_STOPPED;%u;%u", pModuleData->uUnit, pModuleData->uModuleId );
				}
				break;
			case SN_TGT_EVENT_MODULE_STOPPED_REMOVE:
				{
					SN_TGT_EVENT_MODULE_EVENT_DATA *pModuleData =
						(SN_TGT_EVENT_MODULE_EVENT_DATA*)
						(pDataPtr + sizeof(SN_EVENT_TARGET_HDR));

					sprintf_s( s, 256, "Tgt;MODULE_STOPPED_REMOVE;%u;%u", pModuleData->uUnit, pModuleData->uModuleId );
				}
				break;
			case SN_TGT_EVENT_TARGET_SPECIFIC:
				ProcessTargetSpecificEvent(hTarget, pHeader->uSize, pDataPtr + sizeof(SN_EVENT_TARGET_HDR));
				break;
			//case SN_TGT_POWER_STATUS_CHANGE:
			//	break;
			//case SN_TGT_TTY_STREAM_ADDED:
			//	break;
			//case SN_TGT_TTY_STREAM_DELETED:
			//	break;
			default:
				break;
			}

			if ( strlen(s) > 0 )
			{
				AddTargetMessage( hTarget, s );
			}

			uDataRemaining -= pHeader->uSize;
			pDataPtr += pHeader->uSize;
		}
	}

	void __stdcall TargetEventCallback(HTARGET hTarget, UINT uEventType, UINT /*uEvent*/, 
		SNRESULT snr, UINT uDataLen, BYTE *pData, void* /*pUser*/)

	{
		if (SN_FAILED( snr ))
		{
			return;
		}

		char* msg = NULL;

		switch (uEventType)
		{
		case SN_EVENT_TARGET:
			ProcessTargetEvent(hTarget, uDataLen, pData);
			break;
		}
	}

	void __stdcall TTYEventCallback(HTARGET hTarget, UINT uiType, UINT uiStreamId, 
		SNRESULT /*Result*/, UINT uiLength, BYTE *pData, void* /*pUser*/)
	{
		if ( pData[uiLength] != 0 )
		{
			return;
		}

		char *s = (char*)pData;
		if ( strlen(s) > 0 )
		{
			AddTTYMessage( hTarget, s );
		}
	}

	HTARGET InitTargetComms( const std::string &ipAddress )
	{
		if ( g_targetHandlesInitialized )
		{
			return FindAvailableTarget( ipAddress );
		}

		if ( SN_FAILED( SNPS3InitTargetComms() ) )
		{
			printf( "Error: Failed to initialize PS3TM SDK\n" );
			return -1;
		}

		if ( SN_FAILED( SNPS3EnumTargets(EnumCallBack) ) )
		{
			printf( "Error: Failed to enumerate targets\n" );
			return -1;
		}

		return FindAvailableTarget( ipAddress );
	}

	void CloseTargetComms( HTARGET hTarget )
	{
		std::map<HTARGET, TargetConnectionInfo*>::iterator iter = g_targetsInUse.find( hTarget );
		if ( iter != g_targetsInUse.end() )
		{
			g_targetsInUse.erase( iter );
		}

		if ( g_targetHandlesInitialized && (g_targetsInUse.size() == 0) )
		{
			SNPS3CloseTargetComms();

			g_targets.clear();
			g_targetsConnected.clear();

			g_targetHandlesInitialized = false;
		}
	}

	bool BeginReadingTargetEventMessages( HTARGET hTarget )
	{		if ( SN_FAILED( SNPS3RegisterTargetEventHandler(hTarget, TargetEventCallback, 0) ) )
		{
			AddTargetMessage( hTarget, "Err;Failed to register for target events" );
			return false;
		}

		return true;
	}

	void EndReadingTargetEventMessages( HTARGET hTarget )
	{
		SNPS3CancelTargetEvents( hTarget );
	}

	bool BeginReadingTTYMessages( HTARGET hTarget )
	{
		if ( SN_FAILED( SNPS3RegisterTTYEventHandler(hTarget, -1, TTYEventCallback, 0) ) )
		{
			AddTTYMessage( hTarget, "Err;Failed to register for TTY events" );
			return false;
		}

		return true;
	}
	
	void EndReadingTTYMessages( HTARGET hTarget )
	{
		SNPS3CancelTTYEvents( hTarget, -1 );
	}

	std::string GetTargetEventMessage( HTARGET hTarget )
	{
		std::map<HTARGET, TargetConnectionInfo*>::iterator iter = g_targets.find( hTarget );
		if ( (iter != g_targets.end()) && (iter->second->TargetMessageQueue.size() > 0) )
		{
			std::string msg = iter->second->TargetMessageQueue.front();
			iter->second->TargetMessageQueue.pop();
			return msg;
		}

		return "";
	}

	std::string GetTTYMessage( HTARGET hTarget )
	{		
		std::map<HTARGET, TargetConnectionInfo*>::iterator iter = g_targets.find( hTarget );
		if ( (iter != g_targets.end()) && (iter->second->TTYMessageQueue.size() > 0) )
		{
			std::string msg = iter->second->TTYMessageQueue.front();
			iter->second->TTYMessageQueue.pop();
			return msg;
		}

		return "";
	}

	void Update()
	{
		SNPS3Kick();
	}

	void ResetTarget( HTARGET hTarget )
	{
		if ( IsTargetConnected( hTarget) && SN_FAILED( SNPS3Reset( hTarget, 0 ) ) )
		{
			AddTargetMessage( hTarget, "Err;Reset failed" );
		}
	}

	bool IsTargetConnected( HTARGET hTarget )
	{
		std::map<HTARGET, bool>::iterator iter = g_targetsConnected.find( hTarget );
		if ( iter != g_targetsConnected.end() )
		{
			return iter->second;
		}

		return false;
	}

#pragma managed
}
