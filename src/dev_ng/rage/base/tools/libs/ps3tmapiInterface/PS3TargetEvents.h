#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Diagnostics;


namespace ps3tmapiInterface 
{
	public enum class PowerStatus
	{
		OFF,         // Target is off.
		ON,          // Target is on.
		SUSPENDED,   // Target is suspended.
		UNKNOWN      // Status is unknown.
	};

	public enum class ConnectStatus
	{
		CONNECTED,           // Connected state.
		CONNECTING,          // Attempting to connect.
		NOT_CONNECTED,       // Not connected, available for use.
		IN_USE,              // In use by another user.
		UNAVAILABLE          // Unavailable, could be offline.
	};

	public enum class UnitStatus
	{
		UNKNOWN,             // Unknown state.
		RUNNING,             // Unit is executing.
		STOPPED,             // Unit has stopped execution.
		SIGNALLED,           // A signal occurred on the unit.
		RESETTING,           // Unit is in the process of resetting.
		MISSING,             // Unit not present.
		RESET,               // Unit has completed a reset.
		NOT_CONNECTED,       // Unit is not connected (NOT USED).
		CONNECTED,           // Unit is connected (NOT USED).
		UNIT_STATUS_CHANGE   // Notification of unit status change.
	};

	public enum class TargetEvent
	{
		UNIT_STATUS_CHANGE,
		RESET_STARTED,
		RESET_END,
		DETAILS,
		MODULE_LOAD,
		MODULE_RUNNING,
		MODULE_DONE_REMOVE,
		MODULE_DONE_RESIDENT,
		MODULE_STOPPED,
		MODULE_STOPPED_REMOVE,
		POWER_STATUS_CHANGE,
		TTY_STREAM_ADDED,
		TTY_STREAM_DELETED,
		TARGET_SPECIFIC,
	};

	public enum class DebugEvent
	{
		PROCESS_CREATE,
		PROCESS_EXIT,
		PROCESS_KILL,
		PPU_EXP_TRAP,
		PPU_EXP_PREV_INT,
		PPU_EXP_ALIGNMENT,
		PPU_EXP_ILL_INST,
		PPU_EXP_TEXT_HTAB_MISS,
		PPU_EXP_TEXT_SLB_MISS,
		PPU_EXP_DATA_HTAB_MISS,
		PPU_EXP_FLOAT,
		PPU_EXP_DATA_SLB_MISS,
		PPU_EXP_DABR_MATCH,
		PPU_EXP_STOP,
		PPU_EXP_STOP_INIT,
		PPU_THREAD_CREATE,
		PPU_THREAD_EXIT,
		SPU_THREAD_START,
		SPU_THREAD_STOP,
		SPU_THREAD_STOP_INIT,
		SPU_THREAD_GROUP_DESTROY,
		OVERLAY_LOAD,
		OVERLAY_UNLOAD,
	};

	public ref class MessageEventArgs : public System::EventArgs
	{
	public:
		MessageEventArgs( System::String^ msg )
		{
			m_message = msg;
		}

		property System::String^ Message
		{
			System::String^ get()
			{
				return m_message;
			}
		}

	private:
		System::String^ m_message;
	};

	public ref class ConnectStatusEventArgs : public System::EventArgs
	{
	public:
		ConnectStatusEventArgs( ConnectStatus status )
		{
			m_status = status;
		}

		property ConnectStatus Status
		{
			ConnectStatus get()
			{
				return m_status;
			}
		}

		System::String^ AsString()
		{
			System::Text::StringBuilder^ str = gcnew System::Text::StringBuilder( "ConnectStatus: " );
			str->Append( this->Status.ToString() );

			return str->ToString();
		}

	private:
		ConnectStatus m_status;
	};

	/*
	// Event-specific data for the SN_TE_UNIT_STATUS_CHANGE state.
	struct SN_TGT_EVENT_UNIT_STATUS_CHANGE_DATA {
	UINT32 uUnit;
	UINT32 uStatus;		// See EUNITSTATUS for possible values.
	};
	*/
	public ref class UnitStatusEventArgs : public System::EventArgs
	{
	public:
		UnitStatusEventArgs( UnitStatus status, unsigned int unit )
		{
			m_status = status;
			m_unit = unit;
		}

		property unsigned int Unit
		{
			unsigned int get()
			{
				return m_unit;
			}
		}

		property UnitStatus Status
		{
			UnitStatus get()
			{
				return m_status;
			}
		}

		System::String^ AsString()
		{
			System::Text::StringBuilder^ str = gcnew System::Text::StringBuilder( "UnitStatus: " );
			str->Append( this->Status.ToString() );
			str->Append( " unit=" );
			str->Append( this->Unit );

			return str->ToString();
		}

	private:
		unsigned int m_unit;
		UnitStatus m_status;
	};

	public ref class TargetEventEventArgs : public System::EventArgs
	{
	public:
		TargetEventEventArgs( TargetEvent event, unsigned int flags, unsigned int unit, unsigned int moduleId )
		{
			m_event = event;
			m_flags = flags;
			m_unit = unit;
			m_moduleId = moduleId;
		}

		property TargetEvent Event
		{
			TargetEvent get()
			{
				return m_event;
			}
		}

		property unsigned int Flags
		{
			unsigned int get()
			{
				return m_flags;
			}
		}

		property unsigned int Unit
		{
			unsigned int get()
			{
				return m_unit;
			}
		}

		property unsigned int ModuleId
		{
			unsigned int get()
			{
				return m_moduleId;
			}
		}

		System::String^ AsString()
		{
			System::Text::StringBuilder^ str = gcnew System::Text::StringBuilder( "TargetEvent: " );
			str->Append( this->Event.ToString() );

			switch ( this->Event )
			{
			case ps3tmapiInterface::TargetEvent::DETAILS:
				str->Append( " flags=" );
				str->Append( this->Flags );
				break;
			case ps3tmapiInterface::TargetEvent::MODULE_LOAD:
			case ps3tmapiInterface::TargetEvent::MODULE_RUNNING:
			case ps3tmapiInterface::TargetEvent::MODULE_DONE_REMOVE:
			case ps3tmapiInterface::TargetEvent::MODULE_DONE_RESIDENT:
			case ps3tmapiInterface::TargetEvent::MODULE_STOPPED:
			case ps3tmapiInterface::TargetEvent::MODULE_STOPPED_REMOVE:
				str->Append( " unit=" );
				str->Append( this->Unit );
				str->Append( " moduleId=" );
				str->Append( this->ModuleId );
				break;
			default:
				break;
			}

			return str->ToString();
		}

	protected:
		TargetEvent m_event;
		unsigned int m_flags;
		unsigned int m_unit;
		unsigned int m_moduleId;
	};

	public ref class DebugEventEventArgs : public System::EventArgs
	{
	public:
		DebugEventEventArgs( DebugEvent event, unsigned int exitCode, unsigned int threadId, 
			unsigned int hardwareThreadId, unsigned int threadGroupId )
		{
			m_event = event;
			m_exitCode = exitCode;
			m_threadId = threadId;
			m_hardwareThreadId = hardwareThreadId;
			m_threadGroupId = threadGroupId;
		}

		property DebugEvent Event
		{
			DebugEvent get()
			{
				return m_event;
			}
		}

		property unsigned int ExitCode
		{
			unsigned int get()
			{
				return m_exitCode;
			}
		}

		property unsigned int ThreadId
		{
			unsigned int get()
			{
				return m_threadId;
			}
		}

		property unsigned int HardwareThreadId
		{
			unsigned int get()
			{
				return m_hardwareThreadId;
			}
		}

		property unsigned int ThreadGroupId
		{
			unsigned int get()
			{
				return m_threadGroupId;
			}
		}

		System::String^ AsString()
		{
			System::Text::StringBuilder^ str = gcnew System::Text::StringBuilder( "DebugEvent: " );
			str->Append( this->Event.ToString() );

			switch ( this->Event )
			{
			case ps3tmapiInterface::DebugEvent::PROCESS_EXIT:
				str->Append( " exitCode=" );
				str->Append( this->ExitCode );
				break;
			case ps3tmapiInterface::DebugEvent::PPU_EXP_TRAP:
			case ps3tmapiInterface::DebugEvent::PPU_EXP_PREV_INT:
			case ps3tmapiInterface::DebugEvent::PPU_EXP_ALIGNMENT:
			case ps3tmapiInterface::DebugEvent::PPU_EXP_ILL_INST:
			case ps3tmapiInterface::DebugEvent::PPU_EXP_TEXT_HTAB_MISS:
			case ps3tmapiInterface::DebugEvent::PPU_EXP_TEXT_SLB_MISS:
			case ps3tmapiInterface::DebugEvent::PPU_EXP_DATA_HTAB_MISS:
			case ps3tmapiInterface::DebugEvent::PPU_EXP_FLOAT:
			case ps3tmapiInterface::DebugEvent::PPU_EXP_DATA_SLB_MISS:
			case ps3tmapiInterface::DebugEvent::PPU_EXP_DABR_MATCH:
			case ps3tmapiInterface::DebugEvent::PPU_EXP_STOP:
			case ps3tmapiInterface::DebugEvent::PPU_EXP_STOP_INIT:
				str->Append( " threadId=" );
				str->Append( this->ThreadId );
				str->Append( " hwThreadId=" );
				str->Append( this->HardwareThreadId );
				break;
			case ps3tmapiInterface::DebugEvent::PPU_THREAD_CREATE:
			case ps3tmapiInterface::DebugEvent::PPU_THREAD_EXIT:
				str->Append( " threadId=" );
				str->Append( this->ThreadId );
				break;
			case ps3tmapiInterface::DebugEvent::SPU_THREAD_START:
			case ps3tmapiInterface::DebugEvent::SPU_THREAD_STOP:
			case ps3tmapiInterface::DebugEvent::SPU_THREAD_STOP_INIT:
				str->Append( " threadId=" );
				str->Append( this->ThreadId );
				str->Append( " threadGroupId=" );
				str->Append( this->ThreadGroupId );
				break;
			case ps3tmapiInterface::DebugEvent::SPU_THREAD_GROUP_DESTROY: 
				str->Append( " threadGroupId=" );
				str->Append( this->ThreadGroupId );
				break;
			default:
				break;
			}	

			return str->ToString();
		}

	private:
		DebugEvent m_event;
		unsigned int m_exitCode;
		unsigned int m_threadId;
		unsigned int m_hardwareThreadId;
		unsigned int m_threadGroupId;
	};

	public delegate void ConnectStatusEventHandler(System::Object^  sender, ConnectStatusEventArgs^  e);
	public delegate void DebugEventEventHandler(System::Object^  sender, DebugEventEventArgs^  e);
	public delegate void MessageEventHandler(System::Object^  sender, MessageEventArgs^  e);
	public delegate void TargetEventEventHandler(System::Object^  sender, TargetEventEventArgs^  e);
	public delegate void UnitStatusEventHandler(System::Object^  sender, UnitStatusEventArgs^  e);
}
