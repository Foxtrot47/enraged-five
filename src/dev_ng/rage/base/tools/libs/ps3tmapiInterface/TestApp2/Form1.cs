using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace TestApp2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        #region Private Functions
        private void PrintText( string text )
        {
            Console.Write( text );

            int len = this.richTextBox1.TextLength;
            
            this.richTextBox1.AppendText( text );
            
            if ( text.StartsWith( "Error: ") )
            {
                this.richTextBox1.Select( len, text.Length );
                this.richTextBox1.SelectionColor = Color.Red;
                this.richTextBox1.DeselectAll();
            }
            else if ( text.StartsWith( "Warning: " ) )
            {
                this.richTextBox1.Select( len, text.Length );
                this.richTextBox1.SelectionColor = Color.Yellow;
                this.richTextBox1.DeselectAll();
            }
        }
        #endregion

        #region Event Handlers
        private void Form1_Load( object sender, EventArgs e )
        {
            this.pS3TargetComponent.Connect();
        }

        private void Form1_FormClosing( object sender, FormClosingEventArgs e )
        {
            PrintText( "[TEST APP] RESETTING TARGET.  PLEASE WAIT." + Environment.NewLine );

            this.pS3TargetComponent.Reset();
        }

        #region pS3TargetComponent Event Handlers
        private void pS3TargetComponent_ConnectError( object sender, ps3tmapiInterface.MessageEventArgs e )
        {
            PrintText( "[TEST APP] ConnectError: " + e.Message + Environment.NewLine );
        }

        private void pS3TargetComponent_ConnectInfo( object sender, ps3tmapiInterface.MessageEventArgs e )
        {
            PrintText( "[TEST APP] ConnectInfo: " + e.Message + Environment.NewLine );
        }

        private void pS3TargetComponent_ConnectStatus( object sender, ps3tmapiInterface.ConnectStatusEventArgs e )
        {
            PrintText( "[TEST APP] " + e.AsString() + Environment.NewLine );
        }

        private void pS3TargetComponent_DebugEvent( object sender, ps3tmapiInterface.DebugEventEventArgs e )
        {
            PrintText( "[TEST APP] " + e.AsString() + Environment.NewLine );
        }

        private void pS3TargetComponent_TargetEvent( object sender, ps3tmapiInterface.TargetEventEventArgs e )
        {
            PrintText( "[TEST APP] " + e.AsString() + Environment.NewLine );
        }

        private void pS3TargetComponent_TTY( object sender, ps3tmapiInterface.MessageEventArgs e )
        {
            PrintText( e.Message );
        }

        private void pS3TargetComponent_UnhandledCrash( object sender, ps3tmapiInterface.MessageEventArgs e )
        {
            PrintText( "[TEST APP] UnhandledCrash: " + e.Message );
        }

        private void pS3TargetComponent_UnitStatus( object sender, ps3tmapiInterface.UnitStatusEventArgs e )
        {
            PrintText( "[TEST APP] " + e.AsString() + Environment.NewLine );
        }
        #endregion

        #endregion
    }
}