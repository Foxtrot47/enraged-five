namespace TestApp2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.pS3TargetComponent = new ps3tmapiInterface.PS3TargetComponent( this.components );
            this.SuspendLayout();
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.SystemColors.WindowText;
            this.richTextBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox1.ForeColor = System.Drawing.SystemColors.Window;
            this.richTextBox1.Location = new System.Drawing.Point( 0, 0 );
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size( 847, 392 );
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = "";
            // 
            // pS3TargetComponent
            // 
            this.pS3TargetComponent.OutputTTY = true;
            this.pS3TargetComponent.UnhandledCrash += new ps3tmapiInterface.MessageEventHandler( this.pS3TargetComponent_UnhandledCrash );
            this.pS3TargetComponent.DebugEvent += new ps3tmapiInterface.DebugEventEventHandler( this.pS3TargetComponent_DebugEvent );
            this.pS3TargetComponent.ConnectStatus += new ps3tmapiInterface.ConnectStatusEventHandler( this.pS3TargetComponent_ConnectStatus );
            this.pS3TargetComponent.ConnectError += new ps3tmapiInterface.MessageEventHandler( this.pS3TargetComponent_ConnectError );
            this.pS3TargetComponent.TTY += new ps3tmapiInterface.MessageEventHandler( this.pS3TargetComponent_TTY );
            this.pS3TargetComponent.UnitStatus += new ps3tmapiInterface.UnitStatusEventHandler( this.pS3TargetComponent_UnitStatus );
            this.pS3TargetComponent.ConnectInfo += new ps3tmapiInterface.MessageEventHandler( this.pS3TargetComponent_ConnectInfo );
            this.pS3TargetComponent.TargetEvent += new ps3tmapiInterface.TargetEventEventHandler( this.pS3TargetComponent_TargetEvent );
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size( 847, 392 );
            this.Controls.Add( this.richTextBox1 );
            this.Name = "Form1";
            this.Text = "PS3 Target Interface Test App";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler( this.Form1_FormClosing );
            this.Load += new System.EventHandler( this.Form1_Load );
            this.ResumeLayout( false );

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBox1;
        private ps3tmapiInterface.PS3TargetComponent pS3TargetComponent;
    }
}

