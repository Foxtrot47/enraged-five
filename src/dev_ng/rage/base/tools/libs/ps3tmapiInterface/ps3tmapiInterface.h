// ps3tmapiInterface.h

#pragma once

#include "targetManager.h"
#include "PS3TargetEvents.h"

#include <vcclr.h>

using namespace System;
using namespace System::Collections;
using namespace System::Collections::Generic;
using namespace System::Runtime::InteropServices;
using namespace System::Threading;

namespace ps3tmapiInterface {

	public ref class PS3Target
	{
	public:
		PS3Target()
			: m_targetHandle(-1)
			, m_outputTTY(false)
		{
			m_errorDumpText = gcnew System::Collections::Generic::Queue<System::String^>();
		}

		~PS3Target()
		{
			this->!PS3Target();
		}

		!PS3Target()
		{
			Disconnect();
		}

#pragma region Events
		event MessageEventHandler^ ConnectError;

		event MessageEventHandler^ ConnectInfo;

		event ConnectStatusEventHandler^ ConnectStatus;
		
		event DebugEventEventHandler^ DebugEvent;
		
		event TargetEventEventHandler^ TargetEvent;
		
		event MessageEventHandler^ TTY;

		event MessageEventHandler^ UnhandledCrash;

		event UnitStatusEventHandler^ UnitStatus;
#pragma endregion

#pragma region Properties
		property bool OutputTTY
		{
			bool get()
			{
				return m_outputTTY;
			}
			void set( bool value )
			{
				m_outputTTY = value;
			}
		}

		property bool IsConnected
		{
			bool get()
			{
				if ( m_targetHandle != -1 )
				{
					return ps3tmapiInterface::IsTargetConnected( m_targetHandle );
				}

				return false;
			}
		}
#pragma endregion

		bool Connect()
		{
			return Connect( System::String::Empty );
		}

		bool Connect( System::String^ ipAddress )
		{
			pin_ptr<const wchar_t> wch = PtrToStringChars( ipAddress );
			size_t convertedChars = 0;
			size_t  sizeInBytes = ((ipAddress->Length + 1) * 2);
			errno_t err = 0;
			char    *ch = (char *)malloc(sizeInBytes);

			err = wcstombs_s( &convertedChars, ch, sizeInBytes, wch, sizeInBytes );
			if ( err == 0 )
			{
				if ( m_targetHandle == -1 )
				{
					OnConnectStatus( ps3tmapiInterface::ConnectStatus::CONNECTING );
					
					m_targetHandle = ps3tmapiInterface::InitTargetComms( ch );
				}

				if ( m_targetHandle != -1 )
				{
					OnConnectStatus( ps3tmapiInterface::ConnectStatus::CONNECTED );

					bool targetSuccess = ps3tmapiInterface::BeginReadingTargetEventMessages( m_targetHandle );
					bool ttySuccess = ps3tmapiInterface::BeginReadingTTYMessages( m_targetHandle );

					if ( targetSuccess || ttySuccess )
					{
						OnUnitStatus( ps3tmapiInterface::UnitStatus::CONNECTED, m_targetHandle );
						return true;
					}
					else
					{
						OnUnitStatus( ps3tmapiInterface::UnitStatus::NOT_CONNECTED, m_targetHandle );
					}
				}
				else
				{
					OnConnectStatus( ps3tmapiInterface::ConnectStatus::NOT_CONNECTED );
				}
			}

			return false;
		}

		void Update()
		{
			ps3tmapiInterface::Update();

			if ( m_targetHandle == -1 )
			{
				return;
			}

			do 
			{
				std::string msg = ps3tmapiInterface::GetTargetEventMessage( m_targetHandle );
				if ( msg != "" )
				{
					System::String^ text = gcnew System::String( msg.c_str() );
					array<Char>^ separators = { ';' };
					array<System::String^>^ split = text->Split( separators );
					if ( split->Length >= 1 )
					{
						if ( split[0] == "Err" )
						{
							if ( split->Length >= 2 )
							{
								OnConnectError( split[1] );
							}
						}
						else if ( split[0] == "Info" )
						{
							if ( split->Length >= 2 )
							{
								OnConnectInfo( split[1] );
							}
						}
						else if ( split[0] == "Tgt" )
						{
							if ( split->Length >= 2 )
							{
								ps3tmapiInterface::TargetEvent tEvent 
									= safe_cast<ps3tmapiInterface::TargetEvent>( Enum::Parse( ps3tmapiInterface::TargetEvent::typeid, split[1] ) );
								
								unsigned int flags = 0;
								unsigned int unit = 0xffffffff;
								unsigned int moduleId = 0xffffffff;

								switch ( tEvent )
								{
								case ps3tmapiInterface::TargetEvent::DETAILS:
									if ( split->Length >= 3 )
									{
										flags = Convert::ToUInt32( split[2] );
									}
									break;
								case ps3tmapiInterface::TargetEvent::MODULE_LOAD:
								case ps3tmapiInterface::TargetEvent::MODULE_RUNNING:
								case ps3tmapiInterface::TargetEvent::MODULE_DONE_REMOVE:
								case ps3tmapiInterface::TargetEvent::MODULE_DONE_RESIDENT:
								case ps3tmapiInterface::TargetEvent::MODULE_STOPPED:
								case ps3tmapiInterface::TargetEvent::MODULE_STOPPED_REMOVE:
									if ( split->Length >= 4 )
									{
										unit = Convert::ToUInt32( split[2] );
										moduleId = Convert::ToUInt32( split[3] );
									}
									break;
								default:
									break;
								}

								OnTargetEvent( tEvent, flags, unit, moduleId );
							}
						}
						else if ( split[0] == "Us" )
						{
							if ( split->Length >= 3 )
							{
								ps3tmapiInterface::UnitStatus uStatus 
									= safe_cast<ps3tmapiInterface::UnitStatus>( Enum::Parse( ps3tmapiInterface::UnitStatus::typeid, split[1] ) );

								unsigned int unit = Convert::ToUInt32( split[2] );

								OnUnitStatus( uStatus, unit );
							}
						}
						else if ( split[0] == "Dbg" )
						{
							if ( split->Length >= 2 )
							{
								ps3tmapiInterface::DebugEvent dEvent 
									= safe_cast<ps3tmapiInterface::DebugEvent>( Enum::Parse( ps3tmapiInterface::DebugEvent::typeid, split[1] ) );

								unsigned int exitCode = 0;
								unsigned int threadId = 0xffffffff;
								unsigned int hardwareThreadId = 0xffffffff;
								unsigned int threadGroupId = 0xffffffff;

								switch ( dEvent )
								{
								case ps3tmapiInterface::DebugEvent::PROCESS_EXIT:
									if ( split->Length >= 3 )
									{
										exitCode = Convert::ToUInt32( split[2] );
									}
									break;
								case ps3tmapiInterface::DebugEvent::PPU_EXP_TRAP:
								case ps3tmapiInterface::DebugEvent::PPU_EXP_PREV_INT:
								case ps3tmapiInterface::DebugEvent::PPU_EXP_ALIGNMENT:
								case ps3tmapiInterface::DebugEvent::PPU_EXP_ILL_INST:
								case ps3tmapiInterface::DebugEvent::PPU_EXP_TEXT_HTAB_MISS:
								case ps3tmapiInterface::DebugEvent::PPU_EXP_TEXT_SLB_MISS:
								case ps3tmapiInterface::DebugEvent::PPU_EXP_DATA_HTAB_MISS:
								case ps3tmapiInterface::DebugEvent::PPU_EXP_FLOAT:
								case ps3tmapiInterface::DebugEvent::PPU_EXP_DATA_SLB_MISS:
								case ps3tmapiInterface::DebugEvent::PPU_EXP_DABR_MATCH:
								case ps3tmapiInterface::DebugEvent::PPU_EXP_STOP:
								case ps3tmapiInterface::DebugEvent::PPU_EXP_STOP_INIT:
									if ( split->Length >= 4 )
									{
										threadId = Convert::ToUInt32( split[2] );
										hardwareThreadId = Convert::ToUInt32( split[3] );
									}
									break;
								case ps3tmapiInterface::DebugEvent::PPU_THREAD_CREATE:
								case ps3tmapiInterface::DebugEvent::PPU_THREAD_EXIT:
									if ( split->Length >= 3 )
									{
										threadId = Convert::ToUInt32( split[2] );
									}
									break;
								case ps3tmapiInterface::DebugEvent::SPU_THREAD_START:
								case ps3tmapiInterface::DebugEvent::SPU_THREAD_STOP:
								case ps3tmapiInterface::DebugEvent::SPU_THREAD_STOP_INIT:
									if ( split->Length >= 4 )
									{
										threadId = Convert::ToUInt32( split[2] );
										threadGroupId = Convert::ToUInt32( split[3] );
									}
									break;
								case ps3tmapiInterface::DebugEvent::SPU_THREAD_GROUP_DESTROY:
									if ( split->Length >= 3 )
									{
										threadGroupId = Convert::ToUInt32( split[2] );
									}
									break;
								default:
									break;
								}

								OnDebugEvent( dEvent, exitCode, threadId, hardwareThreadId, threadGroupId );
							}
						}
					}
				}
				else
				{
					break;
				}
			}
			while ( true );

			do 
			{
				std::string msg = ps3tmapiInterface::GetTTYMessage( m_targetHandle );
				if ( msg != "" )
				{
					System::String^ text = gcnew System::String( msg.c_str() );

					array<Char>^ separator = { '\n' };
					array<System::String^>^ split = text->Split( separator );

					for ( int i = 0; i < split->Length; ++i )
					{
						System::String^ logText = split[i];
						
						// remove first instance of the color code
						System::String^ colorCodePrefix = gcnew System::String( static_cast<Char>( 27 ) + "[" );
						int prefixStart = logText->IndexOf( colorCodePrefix );
						if ( prefixStart > -1 )
						{
							int prefixEnd = logText->IndexOf( 'm', prefixStart );
							if ( prefixEnd > -1 )
							{
								System::String^ colorCode = logText->Substring( prefixStart, prefixEnd - prefixStart + 1 );
								logText = logText->Replace( colorCode, System::String::Empty );
							}
						}

						// remove last instance of the color code
						int postfixStart = logText->LastIndexOf( colorCodePrefix );
						if ( postfixStart > -1 )
						{
							int postfixEnd = logText->IndexOf( 'm', postfixStart );
							if ( postfixEnd > -1 )
							{
								System::String^ colorCode = logText->Substring( postfixStart, postfixEnd - postfixStart + 1 );
								logText = logText->Replace( colorCode, System::String::Empty );
							}
						}

						if ( logText->Contains( "[RSX dump analysis]" ) || logText->Contains( "Lv-2 detected an interrupt" ) )
						{
							m_errorDumpText->Enqueue( logText );

							if ( !m_errorDumpTimer )
							{
								m_errorDumpTimer = gcnew System::Timers::Timer( 3000.0 );
								m_errorDumpTimer->AutoReset = false;
								m_errorDumpTimer->Elapsed += gcnew System::Timers::ElapsedEventHandler( this, &PS3Target::errorDumpTimer_Elapsed );
								m_errorDumpTimer->Start();

								// make sure TTY is on so that we spit out the forthcoming information
								m_originalOutputTTY = this->OutputTTY;
								this->OutputTTY = true;
							}
							else if ( !m_errorDumpTimer->Enabled )
							{
								m_errorDumpTimer->Start();

								// make sure TTY is on so that we spit out the forthcoming information
								m_originalOutputTTY = this->OutputTTY;
								this->OutputTTY = true;
							}
						}
						
						if ( this->OutputTTY )
						{
							OnTTY( logText + "\n" );
						}
					}
				}
				else
				{
					break;
				}
			}
			while ( true );
		}

		void Disconnect()
		{
            if ( !this->IsConnected )
            {
                return;
            }

			try
			{				
				if ( m_targetHandle != -1 )
				{
					ps3tmapiInterface::EndReadingTargetEventMessages( m_targetHandle );
					ps3tmapiInterface::EndReadingTTYMessages( m_targetHandle );
					OnUnitStatus( ps3tmapiInterface::UnitStatus::NOT_CONNECTED, m_targetHandle );

					ps3tmapiInterface::CloseTargetComms( m_targetHandle );
					OnConnectStatus( ps3tmapiInterface::ConnectStatus::NOT_CONNECTED );

					m_targetHandle = -1;
				}
			}
			catch ( Exception^ )
			{
			}
		}

		void Reset()
		{
			if ( this->IsConnected )
			{
				ps3tmapiInterface::ResetTarget( m_targetHandle );
			}
		}

	private:
#pragma region Variables
		HTARGET m_targetHandle;
		bool m_outputTTY;
		bool m_originalOutputTTY;

		System::Collections::Generic::Queue<System::String^>^ m_errorDumpText;
		System::Timers::Timer^ m_errorDumpTimer;
#pragma endregion

#pragma region Event Dispatchers
		void OnConnectError( String^ msg )
		{
			this->ConnectError( this, gcnew MessageEventArgs( msg ) );
		}

		void OnConnectInfo( String^ msg )
		{
			this->ConnectInfo( this, gcnew MessageEventArgs( msg ) );
		}

		void OnConnectStatus( ps3tmapiInterface::ConnectStatus status )
		{
			this->ConnectStatus( this, gcnew ConnectStatusEventArgs( status ) );
		}

		void OnDebugEvent( ps3tmapiInterface::DebugEvent event, unsigned int exitCode, unsigned int threadId, 
			unsigned int hardwareThreadId, unsigned int threadGroupId )
		{
			this->DebugEvent( this, gcnew DebugEventEventArgs( event, exitCode, threadId, hardwareThreadId, threadGroupId ) );
		}

		void OnTargetEvent( ps3tmapiInterface::TargetEvent event, unsigned int flags, unsigned int unit, unsigned int moduleId )
		{
			this->TargetEvent( this, gcnew TargetEventEventArgs( event, flags, unit, moduleId ) );
		}

		void OnTTY( String^ msg )
		{
			this->TTY( this, gcnew MessageEventArgs( msg ) );
		}

		void OnUnhandledCrash( String^ msg )
		{
			this->UnhandledCrash( this, gcnew MessageEventArgs( msg ) );
		}

		void OnUnitStatus( ps3tmapiInterface::UnitStatus status, unsigned int unit )
		{
			this->UnitStatus( this, gcnew UnitStatusEventArgs( status, unit ) );
		}
#pragma endregion

#pragma region Event Handlers
		void errorDumpTimer_Elapsed( System::Object^ sender, System::Timers::ElapsedEventArgs^ e )
		{
			m_errorDumpTimer->Stop();

			// return the TTY to it's original state
			this->OutputTTY = m_originalOutputTTY;

			int count = m_errorDumpText->Count;
			while ( count > 0 )
			{
				OnUnhandledCrash( m_errorDumpText->Dequeue() );
				--count;
			}						
		}
#pragma endregion
	};
}
