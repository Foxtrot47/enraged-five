// targetManager.h

#pragma once

#include <windows.h>
#include <string>
#include "ps3tmapi.h"

namespace ps3tmapiInterface
{
	extern HTARGET InitTargetComms( const std::string &ipAddress );
	extern void CloseTargetComms( HTARGET hTarget );

	extern bool BeginReadingTargetEventMessages( HTARGET hTarget );
	extern void EndReadingTargetEventMessages( HTARGET hTarget );
	extern std::string GetTargetEventMessage( HTARGET hTarget );

	extern bool BeginReadingTTYMessages( HTARGET hTarget );
	extern void EndReadingTTYMessages( HTARGET hTarget );
	extern std::string GetTTYMessage( HTARGET hTarget );

	extern void Update();

	extern bool IsTargetConnected( HTARGET hTarget );

	extern void ResetTarget( HTARGET hTarget );
}
