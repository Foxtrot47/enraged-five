﻿//---------------------------------------------------------------------------------------------
// <copyright file="BasicTests.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Perforce.UnitTest
{
    using System;
    using System.IO;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using global::Perforce.P4;
    using RSG.Base.Logging;
    using RSG.Base.Logging.Universal;
    using RSG.Configuration;
    using RSG.Interop.Perforce;

    /// <summary>
    /// Basic Perforce unit test cases.
    /// </summary>
    [TestClass]
    public class BasicTests
    {
        #region Member Data
        /// <summary>
        /// Universal log.
        /// </summary>
        private IUniversalLog log;
        #endregion // Member Data

        /// <summary>
        /// Test initialise.
        /// </summary>
        [TestInitialize]
        public void TestInitialise()
        {
            log = LogFactory.CreateUniversalLog("UnitTest");
        }

        /// <summary>
        /// Test simple connection to our Perforce server.
        /// </summary>
        [TestMethod]
        public void TestSimpleConnect()
        {
            String currentDir = Directory.GetCurrentDirectory();
            try
            {
                IConfig config = ConfigFactory.CreateConfig(log);
                String projectRootDirectory = config.Project.RootDirectory;
                Directory.SetCurrentDirectory(projectRootDirectory);
                using (P4 p4 = new P4(PerforcePasswordLoginCallback))
                {
                    p4.Connect();
                    P4CommandResult cmdResult = p4.Run("login", "-s");
                    Assert.IsTrue(cmdResult.Success, "Perforce login failed.");
                }
            }
            finally
            {
                Directory.SetCurrentDirectory(currentDir);
            }
        }
        
        /// <summary>
        /// Test issuing Perforce command without connecting.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(PerforceException), 
            "Must connect to the Perforce server before issuing commands.")]
        public void TestCommandWithoutConnect()
        {
            String currentDir = Directory.GetCurrentDirectory();
            try
            {
                IConfig config = ConfigFactory.CreateConfig(log);
                String projectRootDirectory = config.Project.RootDirectory;
                Directory.SetCurrentDirectory(projectRootDirectory);
                using (P4 p4 = new P4(PerforcePasswordLoginCallback))
                {
                    // Deliberately don't connect.
                    P4CommandResult cmdResult = p4.Run("login", "-s");
                }
            }
            finally
            {
                Directory.SetCurrentDirectory(currentDir);
            }
        }

        /// <summary>
        /// Perforce login prompt.
        /// </summary>
        /// <param name="port"></param>
        /// <returns></returns>
        private bool PerforcePasswordLoginCallback(P4 p4)
        {
            Assert.Fail("Password Login Callback not implemented.");
            return (true);
        }
    }

} // RSG.Interop.Perforce.UnitTest namespace
