﻿//---------------------------------------------------------------------------------------------
// <copyright file="FileMappingTests.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Perforce.UnitTest
{
    using System;
    using System.IO;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using RSG.Base.Logging;
    using RSG.Base.Logging.Universal;
    using RSG.Configuration;
    using RSG.Interop.Perforce;

    /// <summary>
    /// 
    /// </summary>
    [TestClass]
    public class P4ConfigTests
    {
        #region Member Data
        /// <summary>
        /// Universal log.
        /// </summary>
        private IUniversalLog log;
        #endregion // Member Data

        /// <summary>
        /// Test initialise.
        /// </summary>
        [TestInitialize]
        public void TestInitialise()
        {
            log = LogFactory.CreateUniversalLog("UnitTest");
        }

        [TestMethod]
        public void TestConfigConstruction()
        {
            P4Config p4config = new P4Config();
            Assert.IsNotNull(p4config.Port);
            Assert.IsNotNull(p4config.User);
            Assert.IsNotNull(p4config.Client);
            // Can't assume connecting on default will work.

            IConfig config = ConfigFactory.CreateConfig(log);
            String projectRootDirectory = config.Project.RootDirectory;
            String currentWorkingDir = Directory.GetCurrentDirectory();
            P4Config p4configProject = new P4Config(projectRootDirectory);
            Assert.IsNotNull(p4configProject.Port);
            Assert.IsNotNull(p4configProject.User);
            Assert.IsNotNull(p4configProject.Client);

            // Connecting for project should definitely work however.
            Directory.SetCurrentDirectory(projectRootDirectory);
            using (P4 p4 = new P4(PerforcePasswordLoginCallback))
            {
                p4.Connect();
                Assert.IsTrue(p4.Connected);
            }

            // Restore.
            Directory.SetCurrentDirectory(currentWorkingDir);
        }

        /// <summary>
        /// Perforce login prompt.
        /// </summary>
        /// <param name="port"></param>
        /// <returns></returns>
        private bool PerforcePasswordLoginCallback(P4 p4)
        {
            Assert.Fail("Password Login Callback not implemented.");
            return (true);
        }
    }

} // RSG.Interop.Perforce.UnitTest namespace
