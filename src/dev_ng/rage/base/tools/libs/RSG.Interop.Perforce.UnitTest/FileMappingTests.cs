﻿//---------------------------------------------------------------------------------------------
// <copyright file="FileMappingTests.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Perforce.UnitTest
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using global::Perforce.P4;
    
    /// <summary>
    /// 
    /// </summary>
    [TestClass]
    public class FileMappingTests
    {
        /// <summary>
        /// 
        /// </summary>
        [TestMethod]
        public void TestFileMapping()
        {            
            RSG.Configuration.ToolsConfig dynamicConfig = new RSG.Configuration.ToolsConfig();
            String currentWorkingDir = Directory.GetCurrentDirectory();
            Directory.SetCurrentDirectory(dynamicConfig.ToolsRootDirectory);
            using (IPerforceConnection p4 = new P4(PerforcePasswordLoginCallback))
            {
                p4.Connect();
                Assert.IsTrue(p4.Connected);

                IList<FileSpec> fs = FileSpec.DepotSpecList(
                    "//depot/gta5/tools_ng/Configuration.meta",
                    "//depot/gta5/tools_ng/dave.xml",
                    "//dave/dave.xml");
            }
        }

        /// <summary>
        /// Perforce login prompt.
        /// </summary>
        /// <param name="port"></param>
        /// <returns></returns>
        private bool PerforcePasswordLoginCallback(P4 p4)
        {
            Assert.Fail("Password Login Callback not implemented.");
            return (true);
        }
    }

} // RSG.Interop.Perforce.UnitTest namespace
