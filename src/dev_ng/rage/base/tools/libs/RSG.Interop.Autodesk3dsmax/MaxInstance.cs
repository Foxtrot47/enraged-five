﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace RSG.Interop.Autodesk3dsmax
{

    /// <summary>
    /// Autodesk 3dsmax instance class; representing a running instance on the
    /// local machine.
    /// </summary>
    public sealed class MaxInstance : IComparable<MaxInstance>
    {
        #region Constants
        /// <summary>
        /// Autodesk 3dsmax process executable name.
        /// </summary>
        private const String MAX_PROCESS_EXE = "3dsmax";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// 3dsmax instance process object.
        /// </summary>
        public Process Process
        {
            get;
            private set;
        }

        /// <summary>
        /// 3dsmax version.
        /// </summary>
        public MaxVersion Version
        {
            get;
            private set;
        }

        /// <summary>
        /// 3dsmax MaxScriptComm connection URI.
        /// </summary>
        public Uri MaxScriptCommConnection
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor from a local PID (process identifier).
        /// </summary>
        /// <param name="pid"></param>
        public MaxInstance(int pid)
        {
            Process p = Process.GetProcessById(pid);
            if (MAX_PROCESS_EXE.Equals(p.MainModule.ModuleName))
                this.Process = p;
            else
                throw new NotSupportedException("Not valid Autodesk 3dsmax process module.");
        }

        /// <summary>
        /// Constructor from a Process object.
        /// </summary>
        /// <param name="p"></param>
        private MaxInstance(Process p)
        {
            this.Process = p;
        }
        #endregion // Constructor(s)

        #region Object Overrides
        /// <summary>
        /// MaxInstance equality operator based on Process ID.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(Object obj)
        {
            if (!(obj is MaxInstance))
                return (false);

            return ((obj as MaxInstance).Process.Id.Equals(this.Process.Id));
        }

        /// <summary>
        /// MaxInstance get hash code override.
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return (this.Process.Id.GetHashCode());
        }
        #endregion // Object Overrides

        #region IComparable<MaxInstance> Interface
        /// <summary>
        /// Compare against another MaxInstance object.
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public int CompareTo(MaxInstance o)
        {
            return (this.Process.Id.CompareTo(o.Process.Id));
        }
        #endregion // IComparable<MaxInstance> Interface

        #region Static Controller Methods
        /// <summary>
        /// Return array of all running 3dsmax instances.
        /// </summary>
        /// <returns></returns>
        public static MaxInstance[] GetRunningInstances()
        {
            List<MaxInstance> instances = new List<MaxInstance>();
            Process[] processes = Process.GetProcessesByName(MAX_PROCESS_EXE);
            foreach (Process process in processes)
            {
                instances.Add(new MaxInstance(process));
            }
            instances.Sort();
            return (instances.ToArray());
        }
        #endregion // Static Controller Methods
    }

} // RSG.Interop.Autodesk3dsmax namespace
