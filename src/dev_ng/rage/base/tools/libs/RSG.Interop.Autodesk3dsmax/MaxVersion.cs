﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using RSG.Base.Attributes;

namespace RSG.Interop.Autodesk3dsmax
{

    /// <summary>
    /// Autodesk 3dsmax version enumeration.
    /// </summary>
    [DataContract]
    public enum MaxVersion
    {
        [EnumMember]
        None = 0,

        [EnumMember]
        [FieldDisplayName("3dsmax 2009")]
        [RegistryKey(@"Software\Autodesk\3dsmax\11.0", true)]
        [RegistryKey(@"Software\Autodesk\3dsmax\11.0", false)]
        [UserConfigPath(Environment.SpecialFolder.LocalApplicationData, @"Autodesk\3dsmax\2009 - 64bit", true)]
        [UserConfigPath(Environment.SpecialFolder.LocalApplicationData, @"Autodesk\3dsmax\2009 - 32bit", false)]
        [ToolPath("max2009")]
        Autodesk3dsmax2009, // 11.0

        [EnumMember]
        [FieldDisplayName("3dsmax 2010")]
        [RegistryKey(@"Software\Autodesk\3dsmax\12.0", true)]
        [RegistryKey(@"Software\Autodesk\3dsmax\12.0", false)]
        [UserConfigPath(Environment.SpecialFolder.LocalApplicationData, @"Autodesk\3dsmax\2010 - 64bit", true)]
        [UserConfigPath(Environment.SpecialFolder.LocalApplicationData, @"Autodesk\3dsmax\2010 - 32bit", false)]
        [ToolPath("max2010")]
        Autodesk3dsmax2010,

        [EnumMember]
        [FieldDisplayName("3dsmax 2011")]
        [RegistryKey(@"Software\Autodesk\3dsmax\13.0", true)]
        [RegistryKey(@"Software\Autodesk\3dsmax\13.0", false)]
        [UserConfigPath(Environment.SpecialFolder.LocalApplicationData, @"Autodesk\3dsmax\2011 - 64bit", true)]
        [UserConfigPath(Environment.SpecialFolder.LocalApplicationData, @"Autodesk\3dsmax\2011 - 32bit", false)]
        [ToolPath("max2011")]
        Autodesk3dsmax2011,

        [EnumMember]
        [FieldDisplayName("3dsmax 2012")]
        [RegistryKey(@"Software\Autodesk\3dsmax\14.0", true)]
        [RegistryKey(@"Software\Autodesk\3dsmax\14.0", false)]
        [UserConfigPath(Environment.SpecialFolder.LocalApplicationData, @"Autodesk\3dsmax\2012 - 64bit", true)]
        [UserConfigPath(Environment.SpecialFolder.LocalApplicationData, @"Autodesk\3dsmax\2012 - 32bit", false)]
        [ToolPath("max2012")]
        Autodesk3dsmax2012,

        [EnumMember]
        [FieldDisplayName("3dsmax 2013")]
        [RegistryKey(@"Software\Autodesk\3dsmax\15.0", true)]
        [RegistryKey(@"Software\Autodesk\3dsmax\15.0", false)]
        [UserConfigPath(Environment.SpecialFolder.LocalApplicationData, @"Autodesk\3dsmax\2013 - 64bit", true)]
        [UserConfigPath(Environment.SpecialFolder.LocalApplicationData, @"Autodesk\3dsmax\2013 - 32bit", false)]
        [ToolPath("max2013")]
        Autodesk3dsmax2013,

        [EnumMember]
        [FieldDisplayName("3dsmax 2014")]
        [RegistryKey(@"Software\Autodesk\3dsmax\16.0", true)]
        [RegistryKey(@"Software\Autodesk\3dsmax\16.0", false)]
        [UserConfigPath(Environment.SpecialFolder.LocalApplicationData, @"Autodesk\3dsmax\2014 - 64bit", true)]
        [UserConfigPath(Environment.SpecialFolder.LocalApplicationData, @"Autodesk\3dsmax\2014 - 32bit", false)]
        [ToolPath("max2014")]
        Autodesk3dsmax2014,

        [EnumMember]
        [FieldDisplayName("3dsmax 2015")]
        [RegistryKey(@"Software\Autodesk\3dsmax\17.0", true)]
        [RegistryKey(@"Software\Autodesk\3dsmax\17.0", false)]
        [UserConfigPath(Environment.SpecialFolder.LocalApplicationData, @"Autodesk\3dsmax\2015 - 64bit", true)]
        [UserConfigPath(Environment.SpecialFolder.LocalApplicationData, @"Autodesk\3dsmax\2015 - 32bit", false)]
        [ToolPath("max2015")]
        Autodesk3dsmax2015,
    }

    /// <summary>
    /// MaxVersion enumeration utilities and extension methods.
    /// </summary>
    public static class MaxVersionExtensions
    {
        /// <summary>
        /// Return 3dsmax friendly-name.
        /// </summary>
        /// <param name="version"></param>
        /// <returns></returns>
        public static String GetDisplayName(this MaxVersion version)
        {
            Type type = version.GetType();

            FieldInfo fi = type.GetField(version.ToString());
            FieldDisplayNameAttribute[] attributes =
                fi.GetCustomAttributes(typeof(FieldDisplayNameAttribute), false) as FieldDisplayNameAttribute[];

            FieldDisplayNameAttribute attribute = attributes.FirstOrDefault();
            if (null != attribute)
                return (attribute.DisplayName);
            else
                throw (new ArgumentException("3dsmax version has no FieldDisplayName defined."));
        }

        /// <summary>
        /// Return 3ds Max version Registry Key.
        /// </summary>
        /// <param name="version"></param>
        /// <param name="is64bit"></param>
        /// <returns></returns>
        public static String RegistryKey(this MaxVersion version, bool is64bit)
        {
            Type type = version.GetType();

            FieldInfo fi = type.GetField(version.ToString());
            RegistryKeyAttribute[] attributes =
                fi.GetCustomAttributes(typeof(RegistryKeyAttribute), false) as RegistryKeyAttribute[];

            RegistryKeyAttribute attribute = attributes.Where(a => a.Is64Bit == is64bit).FirstOrDefault();
            if (null != attribute)
                return (attribute.Key);
            else
                throw (new ArgumentException("MotionBuilder version has no Registry Key defined."));
        }


        /// <summary>
        /// Return 3ds Max language key.
        /// </summary>
        /// <param name="version"></param>
        /// <param name="langKey">This is the ENU in the user directory</param>
        /// <param name="is64bit"></param>
        /// <returns></returns>
        public static String LanguageKey(this MaxVersion version, string langKey, bool is64bit)
        {
            Type type = version.GetType();

            FieldInfo fi = type.GetField(version.ToString());
            RegistryKeyAttribute[] attributes =
                fi.GetCustomAttributes(typeof(RegistryKeyAttribute), false) as RegistryKeyAttribute[];

            RegistryKeyAttribute attribute = attributes.Where(a => a.Is64Bit == is64bit).FirstOrDefault();
            if (null != attribute)
                return (attribute.Key);
            else
                throw (new ArgumentException("MotionBuilder version has no Registry Key defined."));
        }

        /// <summary>
        /// Return 3dsmax user-config path.
        /// </summary>
        /// <param name="version"></param>
        /// <param name="is64bit"></param>
        /// <returns></returns>
        public static String UserConfigPath(this MaxVersion version, bool is64bit)
        {
            Type type = version.GetType();

            FieldInfo fi = type.GetField(version.ToString());
            UserConfigPathAttribute[] attributes =
                fi.GetCustomAttributes(typeof(UserConfigPathAttribute), false) as UserConfigPathAttribute[];

            UserConfigPathAttribute attribute = attributes.Where(a => a.Is64Bit == is64bit).FirstOrDefault();
            if (null != attribute)
                return (attribute.GetFullPath());
            else
                throw (new ArgumentException("3dsmax version has no User Config Path defined."));
        }

        /// <summary>
        /// Return 3dsmax tool path.
        /// </summary>
        /// <param name="version"></param>
        /// <returns></returns>
        public static String ToolPath(this MaxVersion version)
        {
            Type type = version.GetType();

            FieldInfo fi = type.GetField(version.ToString());
            ToolPathAttribute[] attributes =
                fi.GetCustomAttributes(typeof(ToolPathAttribute), false) as ToolPathAttribute[];

            ToolPathAttribute attribute = attributes.FirstOrDefault();
            if (null != attribute)
                return (attribute.Path);
            else
                throw (new ArgumentException("3dsmax version has no Tool Path defined."));
        }
    }

} // RSG.Interop.Autodesk3dsmax
