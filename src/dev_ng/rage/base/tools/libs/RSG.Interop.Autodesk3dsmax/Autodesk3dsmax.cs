﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.Win32;
using RSG.Base.Extensions;
using RSG.Base.IO;

namespace RSG.Interop.Autodesk3dsmax
{

    /// <summary>
    /// Static class giving information about installed versions of Autodesk 
    /// 3dsmax.
    /// </summary>
    public static class Autodesk3dsmax
    {
        #region Constants
        private static readonly String MAX_EXE = "3dsmax.exe";
        private static readonly String DIRECTORIES_SECTION = "Directories";
        private static readonly String MAX_REGISTRY_LANG_KEYNAME = "CurrentLanguage";
        private static readonly String CONFIG_DIRNAME = "plugcfg";

        /// <summary>
        /// Extensions.
        /// </summary>
        private static readonly ISet<String> EXTENSIONS = new HashSet<String> { ".max" };

        /// <summary>
        /// Supported container-extensions.
        /// </summary>
        private static readonly ISet<String> CONTAINER_EXTENSIONS = new HashSet<String> { ".maxc" };
        #endregion // Constants
        
        #region Controller Methods
        /// <summary>
        /// Determine whether any version of 3dsmax is installed.
        /// </summary>
        /// <returns></returns>
        public static bool IsInstalled()
        {
            bool result = true;
            IEnumerable<MaxVersion> versions =
                Enum.GetValues(typeof(MaxVersion)).Cast<MaxVersion>();
            foreach (MaxVersion version in versions)
			{
                if (version != MaxVersion.None)
                {
                    result |= (IsVersionInstalled(version, true) || IsVersionInstalled(version, false));
                }
            }

            return (result);
        }

        /// <summary>
        /// Determine whether a particular version of 3dsmax is installed.
        /// </summary>
        /// <param name="version"></param>
        /// <param name="is64bit"></param>
        /// <returns></returns>
        public static bool IsVersionInstalled(MaxVersion version, bool is64bit)
        {
            String installPath = GetInstallPath(version, is64bit);

            return (!String.IsNullOrWhiteSpace(installPath));
        }

        /// <summary>
        /// Get installation path for a particular version of 3dsmax.
        /// </summary>
        /// <param name="version"></param>
        /// <param name="is64bit"></param>
        /// <returns></returns>
        public static String GetInstallPath(MaxVersion version, bool is64bit)
        {
            RegistryKey rootKey;
            if (is64bit)
                rootKey = RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine, RegistryView.Registry64);
            else
                rootKey = RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine, RegistryView.Registry32);

            // MPW: We can remove this Max-1:409 bullshit when we no longer need Max2012.
            String key = String.Format("{0}\\MAX-1:409", version.RegistryKey(is64bit));
            RegistryKey versionKey = rootKey.OpenSubKey(key);

            if (null == versionKey)
            {
                versionKey = rootKey.OpenSubKey(version.RegistryKey(is64bit));
                if (null == versionKey)
                    return null;
            }

            String installPath = (String)versionKey.GetValue("Installdir");
            return (installPath);
        }

        /// <summary>
        /// Get executable path for a particular version of 3dsmax.
        /// </summary>
        /// <param name="version"></param>
        /// <param name="is64bit"></param>
        /// <returns></returns>
        public static String GetExecutablePath(MaxVersion version, bool is64bit)
        {
            Debug.Assert(IsVersionInstalled(version, is64bit));
            if (!IsVersionInstalled(version, is64bit))
                throw (new Autodesk3dsmaxException("3dsmax version not installed."));

            String installdir = GetInstallPath(version, is64bit);
            return (Path.Combine(installdir, MAX_EXE));
        }

        /// <summary>
        /// Return information on all running 3dsmax instances.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<MaxInstance> GetRunningInstances()
        {
            return (MaxInstance.GetRunningInstances());
        }

        /// <summary>
        /// Return 3dsmax configuration directory.
        /// </summary>
        /// <param name="version"></param>
        /// <returns></returns>
        public static String GetConfigDirectory(MaxVersion version, bool is64bit, bool localised=false)
        {
            RegistryKey machineRootKey;
            RegistryKey userRootKey;
            if (is64bit)
            {
                machineRootKey = RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine, RegistryView.Registry64);
                userRootKey = RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.CurrentUser, RegistryView.Registry64);
            }
            else
            {
                machineRootKey = RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine, RegistryView.Registry32);
                userRootKey = RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.CurrentUser, RegistryView.Registry32);
            }

            RegistryKey userVersionKey = userRootKey.OpenSubKey(version.RegistryKey(is64bit));
            String languageDir = "enu";
            if (userVersionKey.GetValueNames().Contains(MAX_REGISTRY_LANG_KEYNAME))
                languageDir = (String)userVersionKey.GetValue(MAX_REGISTRY_LANG_KEYNAME);

            String versionDir = version.UserConfigPath(is64bit);
            String configDirectory = Path.Combine(versionDir, languageDir);

            if (localised)
            {
                RegistryKey machineVersionKey = machineRootKey.OpenSubKey(version.RegistryKey(is64bit));
                RegistryKey machineInstalledLanguageKey = machineVersionKey.OpenSubKey(@"LanguagesInstalled\" + languageDir);
                if(null!=machineInstalledLanguageKey)
                {
                    String languageSubDir = (String)machineInstalledLanguageKey.GetValue("LangSubDir");
                    configDirectory = Path.Combine(configDirectory, languageSubDir);
                }
            }

            return (configDirectory);
        }

        /// <summary>
        /// Return 3dsmax additional plugin paths.
        /// </summary>
        /// <param name="version"></param>
        /// <param name="is64bit"></param>
        /// <returns></returns>
        public static IDictionary<String, String> GetAdditionalPluginPaths(MaxVersion version, bool is64bit)
        {
            String configFilename = GetPluginConfigFilename(version, is64bit);
            IDictionary<String, String> pluginPaths = new Dictionary<String, String>();
            try
            {
                if (File.Exists(configFilename))
                {
                    IniFile iniFile = IniFile.Load(configFilename);

                    IEnumerable<String> keys = iniFile.GetKeys(DIRECTORIES_SECTION);
                    foreach (String key in keys)
                    {
                        pluginPaths.Add(key, iniFile.GetValue(key, DIRECTORIES_SECTION));
                    }
                }
            }
            catch (Exception ex)
            {
                throw (new Autodesk3dsmaxException("Failed to get plugin paths.", ex));
            }

            return (pluginPaths);
        }

        /// <summary>
        /// Set 3dsmax additional plugin paths; maintaining any that are set but not
        /// adding duplicates.
        /// </summary>
        /// <param name="version"></param>
        /// <param name="is64bit"></param>
        /// <param name="pluginPaths"></param>
        public static void SetAdditionalPluginPaths(MaxVersion version, bool is64bit, IDictionary<String, String> pluginPaths)
        {
            String configFilename = GetPluginConfigFilename(version, is64bit);

            try
            {
                if (File.Exists(configFilename))
                {
                    IniFile iniFile = IniFile.Load(configFilename);
                    foreach (KeyValuePair<String, String> kvp in pluginPaths)
                    {
                        iniFile.SetValue(kvp.Key, DIRECTORIES_SECTION, kvp.Value);
                    }

                    iniFile.Save(configFilename, IniFileSaveOption.None);
                }
            }
            catch (Exception ex)
            {
                throw (new Autodesk3dsmaxException("Failed to set plugin paths.", ex));
            }
        }

        /// <summary>
        /// Validating any 3dsMax plugin paths to ensure they fit a working set of plugins.
        /// Cases to Handle:
        ///     Making sure "Additional MAX plug-ins" Doesn't point to an outsource directory anymore
        ///     Making sure "Additional MAX plug-ins" isn't empty
        /// The "Additional MAX plug-ins" checks are to help ensure proper installs when coming from
        /// encrypted outsource toolchain to the full toolset. 
        /// </summary>
        /// <param name="version"></param>
        /// <param name="is64bit"></param>
        /// <param name="pluginPaths"></param>
        public static void ValidateAdditionalPluginPaths(MaxVersion ver, bool is64bit)
        {
            String configFilename = GetPluginConfigFilename(ver, is64bit);
            //The reason we use this hardcoded list instead of iniFile.GetKeys() is the ICollection
            //will change mid iteration if a change needs to happen causing a crash. 
            IEnumerable<String> keysToCheck = new List<String>(){
                "Additional Max plug-ins"
            };

            try
            {
                IniFile iniFile = IniFile.Load(configFilename);
                foreach (String key in keysToCheck)
                {
                    //Performing checks based on keys
                    if (String.Compare(key, "Additional Max plug-ins", true) == 0)
                    {
                        String fullValue = iniFile.GetValue(key, DIRECTORIES_SECTION, "outsource");
                        IEnumerable<String> splitValues = fullValue.Split(';');
                        List<String> validValues = new List<String>();
                        foreach (String value in splitValues)
                        {
                            if (!value.Contains("outsource"))
                            {
                                validValues.Add(value);
                            }
                        }
                        if (validValues.Any())
                        {
                            iniFile.SetValue(key, DIRECTORIES_SECTION, String.Join(";", validValues));
                        }
                        else
                        {
                            iniFile.RemoveValue(key, DIRECTORIES_SECTION);
                        }
                    }
                }
                iniFile.Save(configFilename, IniFileSaveOption.None);
            }
            catch (Exception ex)
            {
                throw (new Autodesk3dsmaxException("Failed to validate plugin paths.", ex));
            }
        }

        /// <summary>
        /// Set 3dsmax additional configuration system paths.
        /// </summary>
        /// <param name="version"></param>
        /// <param name="is64bit"></param>
        /// <param name="plugCfgDir"></param>
        /// <param name="scriptsDir"></param>
        /// <param name="startupDir"></param>
        /// <param name="macrosDir"></param>
        /// <param name="iconsDir"></param>
        public static void SetAdditionalConfigPaths(MaxVersion version, bool is64bit, 
            String plugCfgDir, String scriptsDir, String startupDir, String macrosDir,
            String iconsDir)
        {
            String configFilename = GetIniConfigFilename(version, is64bit);
            try
            {
                if (File.Exists(configFilename))
                {
                    IniFile iniFile = IniFile.Load(configFilename);

                    iniFile.SetValue("PlugCFG", DIRECTORIES_SECTION, plugCfgDir);
                    iniFile.SetValue("Scripts", DIRECTORIES_SECTION, scriptsDir);
                    iniFile.SetValue("Startup Scripts", DIRECTORIES_SECTION, startupDir);
                    iniFile.SetValue("Additional Macros", DIRECTORIES_SECTION, macrosDir);
                    iniFile.SetValue("Additional Icons", DIRECTORIES_SECTION, iconsDir);

                    iniFile.Save(configFilename, IniFileSaveOption.None);
                }
            }
            catch (Exception ex)
            {
                throw (new Autodesk3dsmaxException("Failed to set plugin paths.", ex));
            }
        }

        /// <summary>
        /// Copy default PlugCfg directory contents to user-defined plugCfg directory
        /// (used by installer).
        /// </summary>
        /// <param name="version"></param>
        /// <param name="is64bit"></param>
        /// <param name="plugCfgDir"></param>
        /// <returns></returns>
        public static bool CopyDefaultPlugCfg(MaxVersion version, bool is64bit, String plugCfgDir)
        {
            String defaultUserConfigDir = GetConfigDirectory(version, is64bit, true);
            String defaultPlugCfgDir = Path.Combine(defaultUserConfigDir, CONFIG_DIRNAME);
            try
            {
                String[] filenames = Directory.GetFiles(defaultPlugCfgDir, "*.*", SearchOption.AllDirectories);
                foreach (String filename in filenames)
                {
                    String relativeFilename = filename.Replace(defaultPlugCfgDir + "\\", String.Empty);
                    String destinationFilename = Path.Combine(plugCfgDir, relativeFilename);

                    if (!File.Exists(destinationFilename))
                    {
                        String destinationDirectory = Path.GetDirectoryName(destinationFilename);
                        if (!Directory.Exists(destinationDirectory))
                            Directory.CreateDirectory(destinationDirectory);

                        File.Copy(filename, destinationFilename);
                    }
                }
            }
            catch
            {
                return (false);
            }
            return (true);
        }

        /// <summary>
        /// Return 3dsmax plugin configuration filename.
        /// </summary>
        /// <param name="version"></param>
        /// <returns></returns>
        public static String GetPluginConfigFilename(MaxVersion version, bool is64bit)
        {
            String configDirectory = GetConfigDirectory(version, is64bit);
            switch (version)
            {
                case MaxVersion.Autodesk3dsmax2009:
                    configDirectory = Path.Combine(configDirectory, "plugin.ini");
                    break;
                case MaxVersion.Autodesk3dsmax2010:
                case MaxVersion.Autodesk3dsmax2011:
                case MaxVersion.Autodesk3dsmax2012:
                case MaxVersion.Autodesk3dsmax2014:
                case MaxVersion.Autodesk3dsmax2015:
                    configDirectory = Path.Combine(configDirectory, "Plugin.UserSettings.ini");
                    break;
                default:
                    throw (new Autodesk3dsmaxException(String.Format("Unknown 3dsmax version: '{0}'.", version)));
            }
            return (configDirectory);
        }

        /// <summary>
        /// Return 3dsmax INI configuration filename.
        /// </summary>
        /// <param name="version"></param>
        /// <param name="is64bit"></param>
        /// <returns></returns>
        public static String GetIniConfigFilename(MaxVersion version, bool is64bit)
        {
            String configDirectory = GetConfigDirectory(version, is64bit);
            switch (version)
            {
                case MaxVersion.Autodesk3dsmax2009:
                case MaxVersion.Autodesk3dsmax2010:
                case MaxVersion.Autodesk3dsmax2011:
                case MaxVersion.Autodesk3dsmax2012:
                case MaxVersion.Autodesk3dsmax2014:
                case MaxVersion.Autodesk3dsmax2015:
                    configDirectory = Path.Combine(configDirectory, "3dsmax.ini");
                    break;
                default:
                    throw (new Autodesk3dsmaxException(String.Format("Unknown 3dsmax version: '{0}'.", version)));
            }
            return (configDirectory);
        }

        /// <summary>
        /// Return true iff the source file looks like a 3dsmax file.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static bool Is3dsmaxFile(String filename)
        {
            String extension = Path.GetExtension(filename);
            return (EXTENSIONS.Contains(extension));
        }

        /// <summary>
        /// Return true iff the source file looks like a 3dsmax container file.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static bool Is3dsmaxContainerFile(String filename)
        {
            String extension = Path.GetExtension(filename);
            return (CONTAINER_EXTENSIONS.Contains(extension));
        }
        #endregion // Controller Methods
    }

} // RSG.Interop.Autodesk3dsmax namespace
