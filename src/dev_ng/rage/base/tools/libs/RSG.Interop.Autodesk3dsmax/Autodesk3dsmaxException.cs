﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Interop.Autodesk3dsmax
{

    /// <summary>
    /// Autodesk 3dsmax Exception class.
    /// </summary>
    public class Autodesk3dsmaxException : Exception
    {
        #region Constructor(s)
        public Autodesk3dsmaxException()
            : base()
        {
        }

        public Autodesk3dsmaxException(String message)
            : base(message)
        {
        }

        public Autodesk3dsmaxException(String message, Exception inner)
            : base(message, inner)
        {
        }

        public Autodesk3dsmaxException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context)
            : base(info, context)
        {
        }
        #endregion // Constructor(s)
    }

} // RSG.Interop.Autodesk3dsmax namespace
