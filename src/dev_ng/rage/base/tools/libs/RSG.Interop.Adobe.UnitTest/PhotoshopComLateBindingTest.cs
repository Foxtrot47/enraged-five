﻿//---------------------------------------------------------------------------------------------
// <copyright file="PhotoshopComLateBindingTest.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Adobe.UnitTest
{
    using System;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using RSG.Interop.Adobe;

    /// <summary>
    /// Photoshop installation unit tests (machine-specific).
    /// </summary>
    /// <seealso cref="http://support.microsoft.com/kb/302902" />
    /// <seealso cref="http://support.microsoft.com/kb/245115" />
    /// <seealso cref="http://fs.mis.kuas.edu.tw/~wchsieh/photoshopy9.py" />
    /// 
    [TestClass]
    public class PhotoshopComLateBindingTest
    {
        /// <summary>
        /// Create Adobe Photoshop COM object dynamically (from first-principles).
        /// </summary>
        [TestMethod]
        public void TestComLateBinding1()
        {
            Assert.IsTrue(Environment.Is64BitProcess);

            Type appPhotoshopType = Type.GetTypeFromProgID("Photoshop.Application");
            Assert.IsNotNull(appPhotoshopType);
            Object appPhotoshop = Activator.CreateInstance(appPhotoshopType);

            Assert.IsNotNull(appPhotoshop);
        }

        /// <summary>
        /// Create Adobe Photoshop COM object dynamically.
        /// </summary>
        [TestMethod]
        public void TestComLateBinding2()
        {
            Object appPhotoshop = PhotoshopApplication.CreatePhotoshopComObject();
            Assert.IsNotNull(appPhotoshop);
        }
    }

} // RSG.Interop.Adobe.UnitTest namespace
