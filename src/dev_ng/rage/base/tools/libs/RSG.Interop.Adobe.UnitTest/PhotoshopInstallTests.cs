﻿//---------------------------------------------------------------------------------------------
// <copyright file="PhotoshopInstallTests.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Adobe.UnitTest
{
    using System;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using RSG.Interop.Adobe;

    /// <summary>
    /// Photoshop installation unit tests (machine-specific).
    /// </summary>
    [TestClass]
    public class PhotoshopInstallTests
    {
        [TestMethod]
        public void TestInstallVersions()
        {
            Assert.IsTrue(PhotoshopApplication.IsInstalled(),
                "Adobe Photoshop is not installed.");

            Assert.IsFalse(PhotoshopApplication.IsVersionInstalled(PhotoshopVersion.CS));
            Assert.IsFalse(PhotoshopApplication.IsVersionInstalled(PhotoshopVersion.CS2));
            Assert.IsFalse(PhotoshopApplication.IsVersionInstalled(PhotoshopVersion.CS3));
            Assert.IsFalse(PhotoshopApplication.IsVersionInstalled(PhotoshopVersion.CS4));
            Assert.IsTrue(PhotoshopApplication.IsVersionInstalled(PhotoshopVersion.CS5));
            Assert.AreEqual(PhotoshopApplication.GetInstallPath(PhotoshopVersion.CS5),
                @"C:\Program Files\Adobe\Adobe Photoshop CS5 (64 Bit)\");
            Assert.AreEqual(PhotoshopApplication.GetPluginPath(PhotoshopVersion.CS5),
                @"C:\Program Files\Adobe\Adobe Photoshop CS5 (64 Bit)\Plug-Ins\");

            Assert.IsFalse(PhotoshopApplication.IsVersionInstalled(PhotoshopVersion.CS6));
            Assert.IsFalse(PhotoshopApplication.IsVersionInstalled(PhotoshopVersion.CreativeCloud));
        }
    }

} // RSG.Interop.Adobe.UnitTest namespace
