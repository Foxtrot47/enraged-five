//
//
//    Filename: AttribDialog.h
//     Creator: Adam Fowler
//     $Author: Adam $
//       $Date: 30/06/99 15:40 $
//   $Revision: 4 $
// Description: 
//
//

#ifndef INC_ATTRIB_DIALOG_H_
#define INC_ATTRIB_DIALOG_H_

#pragma warning (disable : 4251)

// STD headers
#include <cassert>
#include <string>
#include <vector>
// Microsoft headers
#ifndef STRICT
#define STRICT
#endif
#pragma warning(push)
#pragma warning(disable:4668)
#include <windows.h>
#pragma warning(pop)
// My headers
#include "attribute.h"

#include "atl/delegate.h"
#include "atl/atfunctor.h"

// using namespace
using namespace rage;


// class prototypes
class DynamicDialog;

namespace dmat {

	typedef std::vector<class AttributeControlDesc *> AttributeControlDescList;
	typedef std::vector<class AttributeControlDesc *>::iterator AttributeControlDescListIterator;
	typedef std::vector<class AttributeControlDesc *>::const_iterator AttributeControlDescListConstIterator;

	typedef void* (*AttributeControlDescInitEventFunctor)(const char*);
	typedef void* (*AttributeControlDescChangeEventFunctor)(const char*, dmat::AttributeValue);

	//
	//   Class Name: AttributeControlDesc
	// Base Classes: 
	//  Description: Class that describes a dialog control
	//    Functions: ~AttributeControlDesc(): virtual destructor
	//				SetName(): set the name that appears on label next to the control. Generally 
	//					this is done internally. You only need to call this for Group boxes
	//				GetControlList(): If this control contains another control list return it
	//				Create(): create this control
	//				GetHeight(): return the height of this control
	//				GetLabelWidth(): return the standard label width
	//				SetLabelWidth(): set the standard label width
	//				CreateLabel(): create a label to go with this control
	//
	//
	class ATTRIBUTE_API AttributeControlDesc
	{
	public:
		enum ControlType {SPINNER, GROUPBOX, EDITBOX, COMBOBOX, TCOMBOBOX,CHECKBOX, FILEBROWSER};

		AttributeControlDesc();
		virtual ~AttributeControlDesc();
		
		void SetName(const std::string &name) {m_name = name;}
		const std::string& GetName() {return m_name;}
		virtual const AttributeControlDescList *GetControlList() {return NULL;}
		virtual class AttributeValueControlDesc *AsValueControl() {return NULL;}
		virtual class AttribListControlDesc *AsListControl() {return NULL;}
		virtual ControlType GetType() = 0;

		virtual AttributeControlDesc *Copy() = 0;
		virtual void Create(DynamicDialog *pDialog, s32 &id, RECT &rect,bool bMultipleEdit) = 0;
		virtual s32 GetHeight() = 0;

		static void SetLabelWidth(s32 width) {m_labelWidth = width;}
		static s32 GetLabelWidth() {return m_labelWidth;}
		static void SetCheckBoxLabelWidth(s32 width) {m_checkboxLabelWidth = width;}
		static s32 GetCheckBoxLabelWidth() {return m_checkboxLabelWidth;}
		static void SetComboBoxLabelWidth(s32 width) {m_comboBoxLabelWidth = width;}
		static s32 GetComboBoxLabelWidth() {return m_comboBoxLabelWidth;}


	protected:
		std::string m_name;
		AttributeControlDescInitEventFunctor m_initFunctor;
		AttributeControlDescChangeEventFunctor m_changeFunctor;
		static s32 m_labelWidth;
		static s32 m_checkboxLabelWidth;
		static s32 m_comboBoxLabelWidth;

		// function prototypes
		void CreateLabel(DynamicDialog *pDialog, s32 &id, RECT rect);
		void CreateComboLabel(DynamicDialog *pDialog, s32 &id, RECT rect);
	public:
		AttributeControlDescInitEventFunctor& GetInitFunctor()
		{
			return m_initFunctor;
		}
		AttributeControlDescChangeEventFunctor& GetChangeFunctor()
		{
			return m_changeFunctor;
		}
		void UnbindInitFunctor()
		{
			m_initFunctor = NULL;
		}
		void UnbindChangeFunctor()
		{
			m_changeFunctor = NULL;
		}
		bool IsInitFunctorBound()
		{
			return NULL!=m_initFunctor;
		}
		bool IsChangeFunctorBound()
		{
			return NULL!=m_changeFunctor;
		}

	};

	//
	//   Class Name: AttributeValueControlDesc
	// Base Classes: AttributeControlDesc
	//  Description: Class that decribes a dialog control that actual edits a attribute
	//    Functions: SetAttributeId(): Set the id of the attribute that this control edits
	//				GetAttributeId(): Get the id of the attribute that this control edits
	//				GetEditType(): returns what this control edits e.g. int, float
	//				GetValue(): get attribute value out of control
	//				SetValue(): set the value in the control
	//
	//
	class AttributeValueControlDesc : public AttributeControlDesc
	{
	public:
		AttributeValueControlDesc() {}
		~AttributeValueControlDesc() {}

		void SetAttributeId(s32 id) {m_attrId = id;}
		s32 GetAttributeId() {return m_attrId;}
		virtual Attribute::Type GetEditType() = 0;
		virtual AttributeValue GetValue(HWND hWnd) = 0;
		virtual void SetValue(const AttributeValue& value, HWND hWnd);
		virtual bool HasValueChanged(HWND hWnd);
		virtual void SetBlank(HWND /*hWnd*/) {}
		virtual bool IsBlank(HWND /*hWnd*/) { return false; }
		
		AttributeValueControlDesc *AsValueControl() {return this;}

	protected:
		AttributeValue m_checkSet;
		s32 m_attrId;
	};
	
	
	//
	//   Class Name: AttributeDialogDesc
	// Base Classes: 
	//  Description: List of AttributeControlDesc's
	//    Functions: ~AttributeDialogDesc(): destructor. Deletes all the objects that the list 
	//					elements point to.
	//				AddControl(): add a pointer to control descriptor to list
	//				GetList(): return reference to const version of list
	//				CopyDialogDesc(): copies the given dialog description
	//				DeleteDialogDesc(): delete the dialog description
	//
	//
	class AttributeDialogDesc
	{
	public:
		AttributeDialogDesc() {}
		AttributeDialogDesc(const AttributeDialogDesc &a);
		ATTRIBUTE_API ~AttributeDialogDesc();

		void AddControl(AttributeControlDesc *pControl) {m_list.push_back(pControl->Copy());}
		s32 GetSize() const {return (s32)m_list.size();}
		AttributeControlDesc* GetControl(s32 index) {return m_list[index];}
		const AttributeControlDesc* GetControl(s32 index) const {return m_list[index];}
		void DeleteControl(s32 index) {m_list.erase(m_list.begin() + index);}
		const AttributeControlDescList &GetList() const {return m_list;}
		AttributeControlDescList &GetList() {return m_list;}
		
		AttributeDialogDesc &operator=(const AttributeDialogDesc &a);

		ATTRIBUTE_API AttributeControlDesc *GetValueControl(s32 id, const AttributeControlDescList *list=NULL);

	protected:
		AttributeControlDescList m_list;

		// functions
		void CopyDialogDesc(const AttributeDialogDesc &a);
		void DeleteDialogDesc();
	};

	//
	//   Class Name: AttributeDialog
	// Base Classes: 
	//  Description: Used to construct a dialog for editing attributes
	//		Classes: AttributeControl: instance of one control
	//    Functions: AttributeDialog(): constructor
	//				~AttributeDialog(): destructor
	//				ConstructDialog(): construct dialog from an instance of an attribute class
	//					(AttributeInst) and a dialog descriptor class
	//				InitDialog(): this should be called when the WM_INITDIALOG message is sent. It does
	//					some initialisation which couldn't be done before the dialog was properly
	//					created.
	//				Edit(): set the attribtue instance that is to be edited. In general this should
	//					not need to be called as this is setup in ConstructDialog().
	//				ConstructAttributeDialogDesc(): creates a dialog description from an AttributeInst
	//				SyncWithControls(): Set data inside instance to be the same as in dialog controls
	//				SyncWithAttributes(): Set data in dialog controls to be the same as in instance
	//				GetTemplate(): returns the windows dialog template constructed from the dialog
	//					description
	//
	class ATTRIBUTE_API AttributeDialog
	{
	public:
		//
		// Instance of a control
		//
		class ATTRIBUTE_API AttributeControl
		{
			friend class AttributeDialog;

		public:
			AttributeControl() {}
			AttributeControl(AttributeValueControlDesc *pDesc);
			~AttributeControl();

			void SetResourceId(s32 resId) {m_resId = resId;}
			void FindHWnd(HWND hDlgWnd) {assert(m_resId); m_hWnd = GetDlgItem(hDlgWnd, m_resId);}
			HWND GetHWnd() const {return m_hWnd;}
			void ProcessWindowMessage( HWND hwndDlg, UINT msg, WPARAM wParam, LPARAM lParam );
			
			void SyncWithAttribute(AttributeInst &inst);
			void SyncWithAttributeMultiple(AttributeInstMultiple &inst);
			void SyncWithControl(AttributeInst &inst);
			void SyncWithControlMultiple(AttributeInstMultiple &inst);
			AttributeValueControlDesc *GetDesc(){return m_pDesc;}

			friend bool operator <(const AttributeControl& a, const AttributeControl& b);
			friend bool operator ==(const AttributeControl& a, const AttributeControl& b);

		private:
			s32 m_resId;
			HWND m_hWnd;
			AttributeValueControlDesc *m_pDesc;
		};
		
		// instantiate template class std::vector<AttributeControl>
		template class ATTRIBUTE_API std::vector<AttributeControl>;

		typedef std::vector<AttributeControl> AttributeControlList;
		typedef std::vector<AttributeControl>::iterator AttributeControlListIterator;
		typedef std::vector<AttributeControl>::const_iterator AttributeControlListConstIterator;

	public:
		AttributeDialog() : m_pDialog(NULL),m_pInstMult(NULL) {}
		~AttributeDialog();

		void ConstructDialog(HINSTANCE hInstance, AttributeInst *pInst, const AttributeDialogDesc *pDesc, const char *pTitle = "Attributes");
		void ConstructDialog2(HINSTANCE hInstance, AttributeInstMultiple *pInst, const AttributeDialogDesc *pDesc, const char *pTitle = "Attributes");
		void ConstructMaxRollup(AttributeInst *pInst, const AttributeDialogDesc *pDesc, bool inMaterialEditor=false);
		void InitDialog(HWND hDlgWnd);
		void Edit(AttributeInst *pInst);

		AttributeControl *FindControl(HWND controlHWnd);
		void SyncWithControls();
		void SyncWithAttributes();

		DLGTEMPLATE *GetTemplate();

	private:
		AttributeControlList m_list;
		DynamicDialog *m_pDialog;
		s32 m_labelWidth;
		s32 m_height;
		AttributeInst *m_pInst;
		AttributeInstMultiple *m_pInstMult;

		// function prototypes
		void AddControl(AttributeControl &control) {m_list.push_back(control);}
		void ConstructControlList(const AttributeControlDescList &list, 
								RECT &rect, s32 &resId, bool canBreakUp);
		s32 GetControlListHeight(const AttributeControlDescList &list);
		void SetMaxLabelWidth(const AttributeControlDescList &list);
		void IncludeControlRect(RECT &rect, RECT &dialogRect);
		void CanEditInstance();
	};
	

	//
	//   Class Name: IntAttribControlDesc etc
	// Base Classes: 
	//  Description: All the different dialog description classes
	//    Functions: 
	//
	//
	// FloatAttribControlDesc - control editing integer
	class IntAttribControlDesc : public AttributeValueControlDesc
	{
	public:
		IntAttribControlDesc() {};
		~IntAttribControlDesc() {};

		void SetLimits(s32 min, s32 max) {m_min = min; m_max = max;}
		void GetLimits(s32& min, s32& max) {min = m_min; max = m_max;}
		virtual Attribute::Type GetEditType() {return Attribute::INT;}

	protected:
		s32 m_min;
		s32 m_max;
	};

	// FloatAttribControlDesc - control editing floating point number
	class FloatAttribControlDesc : public AttributeValueControlDesc
	{
	public:
		void SetLimits(float min, float max) {m_min = min; m_max = max;}
		void GetLimits(float& min, float& max) {min = m_min; max = m_max;}
		virtual Attribute::Type GetEditType() {return Attribute::FLOAT;}

	protected:
		float m_min;
		float m_max;
	};

	// AttribListControlDesc - control containing a series of other controls (another dialog)
	class AttribListControlDesc : public AttributeControlDesc
	{
	public:
		AttribListControlDesc *AsListControl() {return this;}
		void SetAttributeDialogDesc(const AttributeDialogDesc *pDialog) {m_pDialog = pDialog;}
		const AttributeDialogDesc *GetDialogDesc() {return m_pDialog;}
		const AttributeControlDescList *GetControlList() {return &(m_pDialog->GetList());}
		
	protected:
		const AttributeDialogDesc *m_pDialog;
	};

	// GroupBoxControlDesc - group box control. Box with title containing other controls
	class GroupBoxControlDesc : public AttribListControlDesc
	{
	public:
		AttributeControlDesc *Copy() {return new GroupBoxControlDesc(*this);}
		virtual ControlType GetType() {return GROUPBOX;}
		void Create(DynamicDialog *pDialog, s32 &id, RECT &rect,bool bMultipleEdit);
		s32 GetHeight() {return 10;}
	};

	// IntSpinnerControlDesc - updown arrow and edit box (spinner) used to edit an integer
	class IntSpinnerControlDesc : public IntAttribControlDesc
	{
	public:
		IntSpinnerControlDesc() {}
		~IntSpinnerControlDesc() {}

		AttributeControlDesc *Copy() {return new IntSpinnerControlDesc(*this);}
		AttributeValue GetValue(HWND hWnd);
		void SetValue(const AttributeValue& value, HWND hWnd);
		virtual ControlType GetType() {return SPINNER;}
		void Create(DynamicDialog *pDialog, s32 &id, RECT &rect,bool bMultipleEdit);
		s32 GetHeight() {return 12;}
		virtual void SetBlank(HWND hWnd);
		virtual bool IsBlank(HWND hWnd);
	};
	// FloatSpinnerControlDesc - updown arrow and edit box (spinner) used to edit a floating point 
	// number
	class FloatSpinnerControlDesc : public FloatAttribControlDesc
	{
	public:
		AttributeControlDesc *Copy() {return new FloatSpinnerControlDesc(*this);}
		AttributeValue GetValue(HWND hWnd);
		void SetValue(const AttributeValue& value, HWND hWnd);
		virtual ControlType GetType() {return SPINNER;}
		void Create(DynamicDialog *pDialog, s32 &id, RECT &rect,bool bMultipleEdit);
		s32 GetHeight() {return 12;}
		virtual void SetBlank(HWND hWnd);
		virtual bool IsBlank(HWND hWnd);
		s32 m_spinid;
	};
	// CheckBoxControlDesc - check box used to edit a boolean value

	//0 is unchecked
	//1 is checked
	//2 is grey checked

	class CheckBoxControlDesc : public AttributeValueControlDesc
	{
	public:
		AttributeControlDesc *Copy() {return new CheckBoxControlDesc(*this);}
		AttributeValue GetValue(HWND hWnd);
		void SetValue(const AttributeValue& value, HWND hWnd);
		void SetRawValue(bool bMultiEdit){m_bMultipleEdit = bMultiEdit;}
		virtual ControlType GetType() {return CHECKBOX;}
		virtual Attribute::Type GetEditType() {return Attribute::BOOL;}
		void Create(DynamicDialog *pDialog, s32 &id, RECT &rect,bool bMultipleEdit);
		s32 GetHeight() {return 12;}
		virtual void SetBlank(HWND hWnd);
		virtual bool IsBlank(HWND hWnd);
	protected:
		bool m_bMultipleEdit;
	};
	// ComboBoxControlDesc - combo box control
	class ComboBoxControlDesc : public AttributeValueControlDesc
	{
	public:
		AttributeControlDesc *Copy() {return new ComboBoxControlDesc(*this);}
		AttributeValue GetValue(HWND hWnd);
		virtual Attribute::Type GetEditType() {return Attribute::INT;}
		void SetValue(const AttributeValue& value, HWND hWnd);
		virtual ControlType GetType() {return COMBOBOX;}
		void Create(DynamicDialog *pDialog, s32 &id, RECT &rect,bool bMultipleEdit);
		s32 GetHeight() {return 12;}
		void SetEntriesString(const std::string &str) {m_string = str;}
		const std::string &GetEntriesString() {return m_string;}
		virtual void SetBlank(HWND hWnd);
		virtual bool IsBlank(HWND hWnd);

	protected:
		std::string m_string;
	};
	// StringComboBoxControlDesc - combo box control
	class StringComboBoxControlDesc : public AttributeValueControlDesc
	{
	public:
		AttributeControlDesc *Copy() {return new StringComboBoxControlDesc(*this);}
		AttributeValue GetValue(HWND hWnd);
		virtual Attribute::Type GetEditType() {return Attribute::STRING;}
		void SetValue(const AttributeValue& value, HWND hWnd);
		virtual ControlType GetType() {return TCOMBOBOX;}
		void Create(DynamicDialog *pDialog, s32 &id, RECT &rect,bool bMultipleEdit);
		s32 GetHeight() {return 12;}
		void SetEntriesString(const std::string &str) {m_string = str;}
		const std::string &GetEntriesString() {return m_string;}
		virtual void SetBlank(HWND hWnd);
		virtual bool IsBlank(HWND hWnd);

	protected:
		std::string m_string;
	};
	// EditBoxControlDesc - edit box used to edit a string value
	class EditBoxControlDesc : public AttributeValueControlDesc
	{
	public:
		EditBoxControlDesc(): m_WasBlank(false) {}

		AttributeControlDesc *Copy() {return new EditBoxControlDesc(*this);}
		AttributeValue GetValue(HWND hWnd);
		void SetValue(const AttributeValue& value, HWND hWnd);
		virtual ControlType GetType() {return EDITBOX;}
		virtual Attribute::Type GetEditType() {return Attribute::STRING;}
		void Create(DynamicDialog *pDialog, s32 &id, RECT &rect,bool bMultipleEdit);
		s32 GetHeight() {return 12;}
		virtual void SetBlank(HWND hWnd);
		virtual bool IsBlank(HWND hWnd);
		virtual bool HasValueChanged(HWND hWnd);
	protected:
		bool m_WasBlank;
	};
	// FileBrowserControlDesc - file browser used to edit a string value
	class FileBrowserControlDesc : public AttributeValueControlDesc
	{
	public:
		AttributeControlDesc *Copy() {return new FileBrowserControlDesc(*this);}
		AttributeValue GetValue(HWND hWnd);
		void SetValue(const AttributeValue& value, HWND hWnd);
		virtual ControlType GetType() {return FILEBROWSER;}
		virtual Attribute::Type GetEditType() {return Attribute::STRING;}
		void Create(DynamicDialog *pDialog, s32 &id, RECT &rect,bool bMultipleEdit);
		s32 GetHeight() {return 12;}

		void SetFileDescription(const std::string& desc) {m_fileDescription = desc;}
		const std::string& GetFileDescription() {return m_fileDescription;}
		void SetExtension(const char *pExt) {strncpy(&m_extension[0], pExt, 3); m_extension[3] = '\0';}
		const char* GetExtension() {return &m_extension[0];}
	
		virtual void SetBlank(HWND hWnd);
		virtual bool IsBlank(HWND hWnd);

	protected:
		std::string m_fileDescription;
		char m_extension[4];
	};
};


#endif //INC_ATTRIB_DIALOG_H_