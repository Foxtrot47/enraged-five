//
//
//    Filename: DynamicDialog.cpp
//     Creator: 
//     $Author: Adam $
//       $Date: 30/06/99 15:08 $
//   $Revision: 5 $
// Description: 
//
//
// stop identifier was truncated warning
#pragma warning (disable : 4786)

// STD headers
#include <cassert>
// Microsoft headers
#ifndef STRICT
#define STRICT
#endif
#pragma warning(push)
#pragma warning(disable:4668)
#include <windows.h>
#include <windowsx.h>
#pragma warning(pop)
// my headers
#include "DynamicDialog.h"

#define FILEBROWSE_BUTTON_WIDTH	12
#define FILEBROWSE_BUTTON_CLASSNAME	"FileBrowse_Button"

// filebrowse buttonextra data 
#define FB_BUTTON_EXTRADATASIZE	24
#define FB_BUTTON_FILTERNAME	0
#define FB_BUTTON_FILTERNAME_SIZE	20
#define FB_BUTTON_FILTER		20
#define FB_BUTTON_ISOPEN		23

WNDPROC DynamicDialog::m_updownProc = NULL;
WNDPROC DynamicDialog::m_floatProc = NULL;
WNDPROC DynamicDialog::m_intProc = NULL;
WNDPROC DynamicDialog::m_fileBrowseProc = NULL;
BOOL DynamicDialog::m_fbButtonRegistered = FALSE;
s32 DynamicDialog::m_stdButtonExtraData = 0;

//
//        name: DynamicDialog::DynamicDialog
// description: Constructor
//
DynamicDialog::DynamicDialog(HINSTANCE hInstance) : m_hInstance(hInstance)
{
}

DynamicDialog::~DynamicDialog()
{
	std::vector<char*>::iterator begin = m_initStrings.begin();
	std::vector<char*>::iterator end = m_initStrings.end();

	while(begin != end)
	{
		char* p_del = *begin;

		delete[] p_del;

		begin++;
	}
}

//
//        name: DynamicDialog::RegisterFileBrowseButton
// description: Register the file browse button window class
//
void DynamicDialog::RegisterFileBrowseButton()
{
	WNDCLASS wc;

	if(m_fbButtonRegistered == FALSE)
	{
		GetClassInfo(NULL, "button", &wc);
		m_stdButtonExtraData = wc.cbWndExtra;
		wc.cbWndExtra += FB_BUTTON_EXTRADATASIZE;
		wc.lpszClassName = FILEBROWSE_BUTTON_CLASSNAME;
		wc.hInstance = m_hInstance;
		m_fbButtonRegistered = RegisterClass(&wc);
	}
}

//
//        name: DynamicDialog::QueueMessage
// description: Add a message onto the message queue
//          in: id = resource id
//				msg = message 
//				wParam,lParam = message parameters
//
void DynamicDialog::QueueSendMessage(s32 id, UINT msg, WPARAM wParam, LPARAM lParam, LRESULT *pResult)
{
	m_messageQ.push(MessageClass(id,msg,wParam,lParam, pResult));
}

//
//        name: DynamicDialog::QueueSetWindowLongs
// description: Add a setwindowlong onto queue
//          in: id = resource id
//				index = index into window extra memory
//				value = new value to set
//
void DynamicDialog::QueueSetWindowLong(s32 id, s32 index, DWORDLONG value, LONG_PTR *pResult)
{
	m_setWindowLongQ.push(SetWindowLongClass(id, index, value, pResult));
}

//
//        name: DynamicDialog::InitDialog
// description: Call this when the WM_INITDIALOG message is sent
//          in: hDlgWnd = handle to dialog window
//
void DynamicDialog::InitDialog(HWND hDlgWnd)
{
	ProcessSetWindowLongs(hDlgWnd);
	ProcessSendMessages(hDlgWnd);
}

//
//        name: DynamicDialog::ProcessSendMessages
// description: Process all the send messages in the queue
//          in: hDlgWnd = handle to dialog window
//
void DynamicDialog::ProcessSendMessages(HWND hDlgWnd)
{
	while(!m_messageQ.empty())
	{
		MessageClass msg = m_messageQ.front();
		HWND hWnd = GetDlgItem(hDlgWnd, msg.m_id);
		LRESULT result;

		assert(hWnd);
	
		result = SendMessage(hWnd, msg.m_msg, msg.m_wParam, msg.m_lParam);
		if(msg.m_pResult)
			*(msg.m_pResult) = result;
		m_messageQ.pop();
	}
}

//
//        name: DynamicDialog::ProcessSetWindowLongs
// description: Process all the set window longs in the queue
//          in: hDlgWnd = handle to dialog window
//
void DynamicDialog::ProcessSetWindowLongs(HWND hDlgWnd)
{
	while(!m_setWindowLongQ.empty())
	{
		SetWindowLongClass swl = m_setWindowLongQ.front();
		HWND hWnd = GetDlgItem(hDlgWnd, swl.m_id);
		LONG_PTR result;

		assert(hWnd);
	
		result = SetWindowLongPtr(hWnd, swl.m_index, (LONG) swl.m_value);
		if(swl.m_pResult)
			*(swl.m_pResult) = result;
		m_setWindowLongQ.pop();
	}
}


//
//        name: DynamicDialog::AddFloatSpinner
// description: Add a edit box and a updown control to construct a spinner
//          in: 
//         out: 
//
s32 DynamicDialog::AddFloatSpinner(const RECT &inRect, WORD id, float minf, float maxf, s32 decimalPlaces)
{
	s32 max, min;
	s32 multiplier = 1;
	s32 d = decimalPlaces;

	if(decimalPlaces == 0)
		return AddIntSpinner(inRect, id, static_cast<s32>(minf), static_cast<s32>(maxf));

	// only allowed up to 4 decimal places
	assert(decimalPlaces < 5 && decimalPlaces > 0);

	while(d--)
		multiplier *= 10;

	// calculate how many decimal places are available
	max = ((s32)maxf) * multiplier;
	min = ((s32)minf) * multiplier;

	AddEditCtrl(WS_BORDER, inRect, id);
	AddCtrl(UPDOWN_CLASS, 
		WS_VISIBLE|WS_CHILD|UDS_ALIGNRIGHT|UDS_SETBUDDYINT|UDS_AUTOBUDDY|UDS_NOTHOUSANDS, 
		inRect, 
		id+1);

	QueueSendMessage(id+1, UDM_SETRANGE32, min, max);
	QueueSetWindowLong(id+1, GWLP_WNDPROC, (DWORDLONG)&DynamicDialog::UpDownMouseMoveProc, (LONG_PTR *)&DynamicDialog::m_updownProc);
	QueueSetWindowLong(id, GWLP_WNDPROC, (DWORDLONG)&DynamicDialog::EditFloatProc, (LONG_PTR *)&DynamicDialog::m_floatProc);
	QueueSetWindowLong(id, GWLP_USERDATA, decimalPlaces);
	return id+2;
}

s32 DynamicDialog::AddIntSpinner(const RECT &inRect, WORD id, s32 min, s32 max)
{
	AddEditCtrl(WS_BORDER, inRect, id);
	AddCtrl(UPDOWN_CLASS, 
		WS_VISIBLE|WS_CHILD|UDS_ALIGNRIGHT|UDS_SETBUDDYINT|UDS_AUTOBUDDY|UDS_NOTHOUSANDS, 
		inRect, 
		id+1);

	QueueSendMessage(id+1, UDM_SETRANGE32, min, max);
	QueueSetWindowLong(id+1, GWLP_WNDPROC, (DWORDLONG)&DynamicDialog::UpDownMouseMoveProc, (LONG_PTR *)&DynamicDialog::m_updownProc);
	QueueSetWindowLong(id, GWLP_WNDPROC, (DWORDLONG)&DynamicDialog::EditIntProc, (LONG_PTR *)&DynamicDialog::m_intProc);
	return id+2;
}

//
//        name: DynamicDialog::AddComboBox
// description: Add a combo box with a list of items in it
//          in: 
//         out: 
//
s32 DynamicDialog::AddComboBox(const RECT &inRect, WORD id, const char *pItemList)
{
	const char *pItem = pItemList;
	const char *pEnd = pItem;

	AddComboBoxCtrl(CBS_DROPDOWNLIST|WS_VSCROLL, inRect, id);

	QueueSendMessage(id, CB_RESETCONTENT, 0,0);

	do {
		if(*pEnd == ',' || *pEnd == '\0')	
		{
			ptrdiff_t diff = pEnd - pItem;
			Assert(diff >= 0);
			s32 NewSize = (s32)diff;

			if(NewSize <= 0)
			{
				continue;
			}

			char* p_New = new char[NewSize + 1]; 
			memcpy(p_New,pItem,NewSize);
			p_New[NewSize] = '\0';

			m_initStrings.push_back(p_New);

			// add string to loop
			QueueSendMessage(id, CB_ADDSTRING, 0, (LPARAM)p_New);
			pItem = pEnd + 1;
		}
	} while(*pEnd++ != '\0');
	
	return id+1;
}

//
//        name: DynamicDialog::AddFileBrowser
// description: Add an editbox and button to browse for files
//
s32 DynamicDialog::AddFileBrowser(const RECT &inRect, WORD id, const char *pFilterString, const char *pFilter)
{
	RECT editRect = inRect;
	RECT buttonRect = inRect;
	char extraData[FB_BUTTON_EXTRADATASIZE];
	s32 i;

	RegisterFileBrowseButton();

	editRect.right -= FILEBROWSE_BUTTON_WIDTH+1;
	buttonRect.left += buttonRect.right - FILEBROWSE_BUTTON_WIDTH;
	buttonRect.right = FILEBROWSE_BUTTON_WIDTH;

	AddEditCtrl(WS_BORDER|ES_READONLY|ES_AUTOHSCROLL, editRect, id, "");
	AddCtrl(FILEBROWSE_BUTTON_CLASSNAME, BS_PUSHBUTTON, buttonRect, id+1, "...");

	QueueSetWindowLong(id+1, GWLP_WNDPROC, (DWORDLONG)&DynamicDialog::EditFileBrowserProc, (LONG_PTR *)&DynamicDialog::m_fileBrowseProc);
	QueueSetWindowLong(id+1, GWLP_USERDATA, id);

	// fill extra data buffer
	strncpy(extraData, pFilterString, FB_BUTTON_FILTERNAME_SIZE);
	strncpy(extraData + FB_BUTTON_FILTER, pFilter, 3);
	extraData[FB_BUTTON_FILTER+3] = '\0';

	for(i=0; i<FB_BUTTON_EXTRADATASIZE/4; i++)
	{
		QueueSetWindowLong(id+1, i*4 + m_stdButtonExtraData, *(s32*)(&extraData[i*4]));
	}

	return id+1;
}

//
//        name: DynamicDialog::UpDownMouseMoveProc
// description: This is the custom proc for the updown control. It overrides WM_MOUSEMOVE so that the value that
//				it controls can change with mouse movement
//
LRESULT CALLBACK DynamicDialog::UpDownMouseMoveProc( HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam )
{
	static s32 startY;
	LRESULT rt;
	NMUPDOWN nmUpdown;

	switch(msg)
	{
	case WM_LBUTTONDOWN:
		startY = GET_Y_LPARAM(lParam);
		break;
	case WM_MOUSEMOVE:
		rt = CallWindowProc(m_updownProc, hwnd, msg, wParam ,lParam);
		if(wParam & MK_LBUTTON)
		{
			s32 diffY = startY - GET_Y_LPARAM(lParam);
			startY = GET_Y_LPARAM(lParam);

			if(diffY)
			{
				nmUpdown.hdr.hwndFrom = hwnd;
				nmUpdown.hdr.idFrom = GetDlgCtrlID(hwnd);
				nmUpdown.hdr.code = UDN_DELTAPOS;
				nmUpdown.iPos = (int)SendMessage(hwnd, UDM_GETPOS32, 0, 0);
				nmUpdown.iDelta = diffY;

				if(SendMessage(GetParent(hwnd), WM_NOTIFY, GetDlgCtrlID(hwnd), (LPARAM)&nmUpdown) == 0)
				{
					SendMessage(hwnd, UDM_SETPOS32, 0, nmUpdown.iPos + diffY);
				}
			}
		}
		return rt;
	}
	return CallWindowProc(m_updownProc, hwnd, msg, wParam ,lParam);
}

void HELPER_limitValue(int minRange, int maxRange, char *pText, int decimalPlaces, HWND editWnd)
{
	if ( strlen(pText)<=0 || minRange ==  0 && maxRange == 0 )
		return;

	char pOldText[256] = {0};
	float val = atof(pText);
	float multiplier = 1;
	for(int i=0;i<decimalPlaces;i++)
		multiplier *= 10;
	float minRangef = (float)(long)minRange / multiplier;
	float maxRangef = (float)maxRange / multiplier;
	if ( val > maxRangef )
	{
		val = maxRangef;
	}
	if ( val < minRangef )
	{
		val = minRangef;
	}
	ZeroMemory(pOldText, 256);
	sprintf_s(pOldText, 64, "%d", decimalPlaces);
	char formatBuffer[64] = {0};
	strcpy_s(formatBuffer, "%.");
	strcat_s(formatBuffer, pOldText);
	strcat_s(formatBuffer, "f");
	ZeroMemory(pOldText, 256);
	sprintf_s(pOldText, 256, formatBuffer, val);

	strcpy(pText,&pOldText[0]);

	// fuckin hacky integer solution...
	char ctrlBuffer[265];
	sprintf_s(ctrlBuffer, 265, "%d", (int)(val * multiplier));
	SendMessage(editWnd, WM_SETTEXT, 0, (LPARAM)ctrlBuffer);
}

//
//        name: DynamicDialog::EditIntProc
// description: This is the custom proc for the float edit box. It overrides WM_SETTEXT and WM_GETTEXT.
//				It removes a decimal point in WM_GETTEXT and replaces it in WM_SETTEXT. This is so it
//				can work with the UPDOWN control (which only works with integers)
//
LRESULT CALLBACK DynamicDialog::EditIntProc( HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam )
{
	switch(msg)
	{
	case WM_KILLFOCUS:
		{
			// simulating text get to trigger validation
			int ctrlId = GetDlgCtrlID(hwnd);
			char buffer[256];
			GetDlgItemText(GetParent(hwnd), ctrlId, buffer, 256);
		}
		break;
	case WM_GETTEXT:
		{
			LRESULT rt = CallWindowProc(m_intProc, hwnd, msg, wParam ,lParam);
			char *pText = (char *)lParam;
			s32 decimalPlaces = 0; // integer, innit

			LONG minRange;
			LONG maxRange; 
			HWND spinnerCtrlHwnd = GetNextDlgGroupItem(GetParent(hwnd), hwnd, FALSE);
			SendMessage ( spinnerCtrlHwnd, UDM_GETRANGE32, (LONG)&minRange, (LONG)&maxRange );
			HELPER_limitValue(minRange, maxRange, pText, decimalPlaces, hwnd);
			return rt;
		}
	}
	return CallWindowProc(m_intProc, hwnd, msg, wParam ,lParam);
}
//
//        name: DynamicDialog::EditFloatProc
// description: This is the custom proc for the float edit box. It overrides WM_SETTEXT and WM_GETTEXT.
//				It removes a decimal point in WM_GETTEXT and replaces it in WM_SETTEXT. This is so it
//				can work with the UPDOWN control (which only works with integers)
//
LRESULT CALLBACK DynamicDialog::EditFloatProc( HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam )
{
	switch(msg)
	{
	case EM_SETSEL:
		// need to select one more character because a decimal point has been inserted
		{
			if(wParam != 0 || lParam != -1)
				lParam++;
			return CallWindowProc(m_floatProc, hwnd, msg, wParam ,lParam);
		}
	case WM_KILLFOCUS:
		{
			// simulating text get to trigger validation
			int ctrlId = GetDlgCtrlID(hwnd);
			char buffer[256];
			GetDlgItemText(GetParent(hwnd), ctrlId, buffer, 256);
		}
		break;
	case WM_GETTEXT:
		{
			LRESULT rt = CallWindowProc(m_floatProc, hwnd, msg, wParam ,lParam);
			char *pText = (char *)lParam;
			s32 decimalPlaces = (s32)GetWindowLongPtr(hwnd, GWLP_USERDATA);

			LONG minRange;
			LONG maxRange; 
			HWND spinnerCtrlHwnd = GetNextDlgGroupItem(GetParent(hwnd), hwnd, FALSE);
			SendMessage ( spinnerCtrlHwnd, UDM_GETRANGE32, (LONG)&minRange, (LONG)&maxRange );
			HELPER_limitValue(minRange, maxRange, pText, decimalPlaces, hwnd);

			s32 i,j=0;
			for(i=0; i<rt; i++)
			{
				// if a decimal point is found remove it
				if(*(pText + i) == '.')
				{
					while(j != decimalPlaces)
					{
						if(i < rt - 1)
						{
							*(pText + i) = *(pText + i + 1);
						}
						else
							break;
						i++;
						j++;
					}
					break;
				}
			}
			// add zeros at end of number 
			while(j != decimalPlaces)
			{
				*(pText + i) = '0';
				i++;
				j++;
			}
			*(pText + i) = '\0';
			rt = i + 1;
			return rt;
		}
	case WM_SETTEXT:
		{
			s32 decimalPlaces = (s32)GetWindowLongPtr(hwnd, GWLP_USERDATA);
			char *pOldText = (char *)lParam;
			char *pNewText;
			s32 len = (s32)strlen(pOldText);
			s32 len2;
			s32 i;
			s32 negativeOffset = 0;

			if(pOldText[0] == '\0')
			{
				pNewText = new char[1];
				pNewText[0] = '\0';
			}
			else
			{

				// check if number is negative
				if(*pOldText == '-')
					negativeOffset = 1;

				// get the length of the float version of the number
				len2 = len + 1;
				if(len2 < decimalPlaces + 2 + negativeOffset)
					len2 = decimalPlaces + 2 + negativeOffset;
				// allocate space
				pNewText = new char[len2 + 1];

				if(negativeOffset)
					*pNewText = '-';

				// add zeros at the beginning of the number if there isn't enough space for 
				// the decimal point
				for(i=0; i<decimalPlaces + 1 - len + negativeOffset; i++)
					pNewText[i + negativeOffset] = '0';
				strcpy(pNewText + i + negativeOffset, pOldText + negativeOffset);

				// add the decimal point
				for(i=0; i<decimalPlaces+1; i++)
				{
					pNewText[len2 - i] = pNewText[len2 - i - 1];
				};
				pNewText[len2 - i] = '.';
			}

			LRESULT rt = CallWindowProc(m_floatProc, hwnd, msg, wParam ,(LPARAM)pNewText);
			// delete the float version of the text
			delete[] pNewText;
			return rt;
		}
	}
	return CallWindowProc(m_floatProc, hwnd, msg, wParam ,lParam);

}

//
//        name: DynamicDialog::EditFileBrowserProc
// description: This is the custom proc for the file browser button.
//
LRESULT CALLBACK DynamicDialog::EditFileBrowserProc( HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam )
{
	switch(msg)
	{
	case BM_SETSTATE:
		// if button pressed
		if(wParam == 1)
		{
			// ALupinacci 7/21/08 - Removed this variable because it isn't being used
			//LRESULT rt = CallWindowProc(m_fileBrowseProc, hwnd, msg, wParam ,lParam);

			OPENFILENAME ofn;
			HWND hParent, hEdit;
			s32 editId = (s32)GetWindowLongPtr(hwnd, GWLP_USERDATA);
			char filenameBuffer[MAX_PATH];
			char extraData[FB_BUTTON_EXTRADATASIZE];
			char filterString[32];
			char filter[4];
			s32 i;

			// get extra data
			for(i=0; i<FB_BUTTON_EXTRADATASIZE/4; i++)
			{
				*((s32*)(&extraData[i*4])) = (s32)GetWindowLongPtr(hwnd, i*4 + m_stdButtonExtraData);
			}
			// construct filter strings
			sprintf(filterString, "%s(*.%s)%c*.%s%c", &extraData[FB_BUTTON_FILTERNAME], 
				&extraData[FB_BUTTON_FILTER], '\0', &extraData[FB_BUTTON_FILTER], '\0');
			strcpy(&filter[0], &extraData[FB_BUTTON_FILTER]);

			hParent = GetParent(hwnd);
			hEdit = GetDlgItem(hParent, editId);

			// get text from editbox
			SendMessage(hEdit, WM_GETTEXT, MAX_PATH, (LPARAM)&filenameBuffer[0]);

			// fill out OPENFILENAME structure
			ofn.lStructSize = sizeof(ofn);
			ofn.hwndOwner = hwnd;
			ofn.hInstance = NULL;
			ofn.lpstrFilter = &filterString[0];
			ofn.lpstrCustomFilter = NULL;
			ofn.nMaxCustFilter = 0;
			ofn.nFilterIndex = 0;
			ofn.lpstrFile = &filenameBuffer[0];
			ofn.nMaxFile = MAX_PATH;
			ofn.lpstrFileTitle = NULL;
			ofn.nMaxFileTitle = 0;
			ofn.lpstrInitialDir = NULL;
			ofn.lpstrTitle = NULL;
			ofn.Flags = OFN_FILEMUSTEXIST|OFN_PATHMUSTEXIST|OFN_HIDEREADONLY;
			ofn.lpstrDefExt = &filter[0];

			if(GetOpenFileName(&ofn))
			{
				SendMessage(hEdit, WM_SETTEXT, 0, (LPARAM)&filenameBuffer[0]);
			}
		}
		break;
	}

	return CallWindowProc(m_fileBrowseProc, hwnd, msg, wParam ,lParam);
}

