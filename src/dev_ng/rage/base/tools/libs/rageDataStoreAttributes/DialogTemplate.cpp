//
//
//    Filename: DialogTemplate.cpp
//     Creator: Adam Fowler
//     $Author: Adam $
//       $Date: 1/06/99 12:14 $
//   $Revision: 2 $
// Description: Class used to create a dialog template which can be used to create dialogs dynamically
//
//

// STD headers
#include <cassert>
// my headers
#include "DialogTemplate.h"


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

DialogTemplate::DialogTemplate()
{
	m_pDlgTemplate = NULL;
	m_pNumItems = NULL;
	m_pTemplatePos = NULL;
}

//
//
//
//

DialogTemplate::~DialogTemplate()
{
	if(m_pDlgTemplate) delete[] m_pDlgTemplate;
}


//
//
//
//

BOOL DialogTemplate::Init(DWORD style, const RECT &inRect, LPCSTR title, LPCSTR font, s16 fontSize)
{
	int   nchar;

	//Starting size for the template.
	//If the template is too small for the data coming in then the data
	//will be reallocated.
	m_maxTemplateSize = 8192;

	//Allocate some memory for the dialog data.
	m_pDlgTemplate = m_pTemplatePos = (PWORD) new WORD[m_maxTemplateSize];

	SetDialogItemCounterPtr();

	//Start to fill in the dialog template information.  addressing by WORDs.
	if(style == 0)
	{
		//Fill out default style.
		style = DS_MODALFRAME | WS_CAPTION | WS_SYSMENU | WS_VISIBLE;
	}

	if(font)
		style |= DS_SETFONT;
	else
		style &= ~DS_SETFONT;

	*m_pTemplatePos++ = LOWORD (style);
	*m_pTemplatePos++ = HIWORD (style);
	*m_pTemplatePos++ = 0;					// LOWORD (lExtendedStyle)
	*m_pTemplatePos++ = 0;					// HIWORD (lExtendedStyle)
	*m_pTemplatePos++ = 0;					// NumberOfItems. Start off with 0. Need to change this as we add more controls.
	*m_pTemplatePos++ = (WORD)inRect.left;  // x
	*m_pTemplatePos++ = (WORD)inRect.top;   // y
	*m_pTemplatePos++ = (WORD)inRect.right; // cx
	*m_pTemplatePos++ = (WORD)inRect.bottom;// cy
	*m_pTemplatePos++ = 0;					// Menu
	*m_pTemplatePos++ = 0;					// Class

	//Copy the title of the dialog.
	nchar = CopyAnsiToWideChar(m_pTemplatePos, title);
	m_pTemplatePos += nchar;
	//Make sure that a null character is put in.
	*(m_pTemplatePos-1) = 0;

	//
	//Add in the wPointSize and szFontName here iff the DS_SETFONT bit on.
	//
	if(font)
	{
		*m_pTemplatePos++ = fontSize;
		//Copy the title of the dialog.
		nchar = CopyAnsiToWideChar(m_pTemplatePos, font);
		m_pTemplatePos += nchar;
		//Make sure that a null character is put in.
		*(m_pTemplatePos-1) = 0;
	}

	//Note that if you want a modal dialog box you will need to keep
	//the data at pDlgTemplate until after you have finished with the
	//dialog box. The modal stuff doesn't make a copy of the information
	//in the template.

	assert((m_pTemplatePos - m_pDlgTemplate) < m_maxTemplateSize);

	return TRUE;
}

//
//        name: DialogTemplate::SetDialogSize
// description: Set size of dialog 
//          in: rect = dialog rectangle
//         out: 
//
void DialogTemplate::SetDialogSize(RECT &rect)
{
	DLGTEMPLATE *pDlgTemplate = (DLGTEMPLATE *)m_pDlgTemplate;

	pDlgTemplate->x = (s16)rect.left;
	pDlgTemplate->y = (s16)rect.top;
	pDlgTemplate->cx = (s16)rect.right;
	pDlgTemplate->cy = (s16)rect.bottom;
}

//
//
//
//

void DialogTemplate::SetDialogItemCounterPtr()
{
	//Updates the position that we expect to find the counter in the dialog template.
	//The counter is the number of controls that are added to the dialog.
	m_pNumItems = m_pDlgTemplate + 4;
}

//
//
//
//

void DialogTemplate::ReallocateTemplateBuffer()
{
	WORD *pMem;
	ptrdiff_t offset;

	offset = m_pTemplatePos - m_pDlgTemplate;

	int iOldMaxSize = m_maxTemplateSize;

	//Allocate new memory block.
	m_maxTemplateSize += 1024;

	pMem = new WORD[m_maxTemplateSize];

	memset(pMem,0,sizeof(WORD) * m_maxTemplateSize);
	memcpy(pMem, m_pDlgTemplate, sizeof(WORD) * iOldMaxSize);

	delete[] m_pDlgTemplate;

	m_pDlgTemplate = pMem;

	//Move the template position back to where it was.
	m_pTemplatePos = m_pDlgTemplate + offset;

	SetDialogItemCounterPtr();
}

BOOL DialogTemplate::AddCtrl(WORD wndClass, DWORD style, const RECT &inRect, WORD id, LPCSTR text)
{
	s32 length;

	//Must have a dialog template to add the button to.
	assert(m_pDlgTemplate);
	assert(m_pTemplatePos);

	//Estimate the size in WORDs of template the button needs.
	length = lstrlen(text);
	length += 14; //Add most of the size of the structure.
	
	//Add on current size of the template.
	ptrdiff_t offset = m_pTemplatePos - m_pDlgTemplate;
	Assert(offset >= 0);
	length += (s32)offset;

	if(length >= m_maxTemplateSize)
	{
		ReallocateTemplateBuffer();
	}
  
	style |= WS_VISIBLE | WS_CHILD;

	//Make sure the item starts on a DWORD boundary.
	m_pTemplatePos = lpwAlign (m_pTemplatePos);

	(*m_pNumItems)++;

	*m_pTemplatePos++ = LOWORD (style);
	*m_pTemplatePos++ = HIWORD (style);
	*m_pTemplatePos++ = 0;					// LOWORD (lExtendedStyle)
	*m_pTemplatePos++ = 0;					// HIWORD (lExtendedStyle)
	*m_pTemplatePos++ = (WORD)inRect.left;	// x
	*m_pTemplatePos++ = (WORD)inRect.top;	// y
	*m_pTemplatePos++ = (WORD)inRect.right;	// cx
	*m_pTemplatePos++ = (WORD)inRect.bottom;// cy
	*m_pTemplatePos++ = id;					// ID

	//Fill in class i.d. Button in this case.
	*m_pTemplatePos++ = (WORD)0xffff;
	*m_pTemplatePos++ = wndClass;

	//Copy the text of the first item.
	length = CopyAnsiToWideChar (m_pTemplatePos, text);
	m_pTemplatePos += length;

	//Make sure that a null character is put in.
	*(m_pTemplatePos-1) = 0;

	*m_pTemplatePos++ = 0;  // advance pointer over nExtraStuff WORD

	assert((m_pTemplatePos - m_pDlgTemplate) < m_maxTemplateSize);

	return TRUE;
}

BOOL DialogTemplate::AddCtrl(LPCSTR wndClass, DWORD style, const RECT &inRect, WORD id, LPCSTR text)
{
	s32 length;

	//Must have a dialog template to add the button to.
	assert(m_pDlgTemplate);
	assert(m_pTemplatePos);

	//Estimate the size in WORDs of template the button needs.
	length = lstrlen(text);
	length += lstrlen(wndClass);
	length += 10; //Add most of the size of the structure.
	
	
	//Add on current size of the template.
	ptrdiff_t offset = m_pTemplatePos - m_pDlgTemplate;
	Assert(offset >= 0);
	length += (s32)offset;

	if(length >= m_maxTemplateSize)
	{
		ReallocateTemplateBuffer();
	}
  
	style |= WS_VISIBLE | WS_CHILD;

	//Make sure the item starts on a DWORD boundary.
	m_pTemplatePos = lpwAlign (m_pTemplatePos);

	(*m_pNumItems)++;

	*m_pTemplatePos++ = LOWORD (style);
	*m_pTemplatePos++ = HIWORD (style);
	*m_pTemplatePos++ = 0;					// LOWORD (lExtendedStyle)
	*m_pTemplatePos++ = 0;					// HIWORD (lExtendedStyle)
	*m_pTemplatePos++ = (WORD)inRect.left;	// x
	*m_pTemplatePos++ = (WORD)inRect.top;	// y
	*m_pTemplatePos++ = (WORD)inRect.right;	// cx
	*m_pTemplatePos++ = (WORD)inRect.bottom;// cy
	*m_pTemplatePos++ = id;					// ID

	// fill in window class name
	length = CopyAnsiToWideChar (m_pTemplatePos, wndClass);
	m_pTemplatePos += length;
	//Make sure that a null character is put in.
	*(m_pTemplatePos-1) = 0;

	//Copy the text of the first item.
	length = CopyAnsiToWideChar (m_pTemplatePos, text);
	m_pTemplatePos += length;
	//Make sure that a null character is put in.
	*(m_pTemplatePos-1) = 0;

	*m_pTemplatePos++ = 0;  // advance pointer over nExtraStuff WORD

	assert((m_pTemplatePos - m_pDlgTemplate) < m_maxTemplateSize);

	return TRUE;
}

//
//
//
//
BOOL DialogTemplate::AddButtonCtrl(DWORD style, const RECT &inRect, WORD id, LPCSTR text)
{
	style |= BS_PUSHBUTTON;
	return AddCtrl(BUTTON_CTRL, style, inRect, id, text);
}
BOOL DialogTemplate::AddEditCtrl(DWORD style, const RECT &inRect, WORD id, LPCSTR text)
{
	return AddCtrl(EDIT_CTRL, style, inRect, id, text);
}
BOOL DialogTemplate::AddStaticCtrl(DWORD style, const RECT &inRect, WORD id, LPCSTR text)
{
	return AddCtrl(STATIC_CTRL, style, inRect, id, text);
}
BOOL DialogTemplate::AddListBoxCtrl(DWORD style, const RECT &inRect, WORD id, LPCSTR text)
{
	return AddCtrl(LISTBOX_CTRL, style, inRect, id, text);
}
BOOL DialogTemplate::AddScrollBarCtrl(DWORD style, const RECT &inRect, WORD id, LPCSTR text)
{
	return AddCtrl(SCROLLBAR_CTRL, style, inRect, id, text);
}
BOOL DialogTemplate::AddComboBoxCtrl(DWORD style, const RECT &inRect, WORD id, LPCSTR text)
{
	return AddCtrl(COMBOBOX_CTRL, style, inRect, id, text);
}
BOOL DialogTemplate::AddGroupBoxCtrl(DWORD style, const RECT &inRect, WORD id, LPCSTR text)
{
	style |= BS_GROUPBOX;
	return AddCtrl(BUTTON_CTRL, style, inRect, id, text);
}
BOOL DialogTemplate::AddCheckBoxCtrl(DWORD style, const RECT &inRect, WORD id, LPCSTR text,bool bMultipleEdit)
{
	if(bMultipleEdit)
	{
		style |= BS_AUTO3STATE;
	}
	else
	{
		style |= BS_AUTOCHECKBOX;
	}

	return AddCtrl(BUTTON_CTRL, style, inRect, id, text);
}
BOOL DialogTemplate::AddRadioButtonCtrl(DWORD style, const RECT &inRect, WORD id, LPCSTR text)
{
	style |= BS_AUTORADIOBUTTON;
	return AddCtrl(BUTTON_CTRL, style, inRect, id, text);
}

//
//
//
//

WORD *DialogTemplate::GetTemplate()
{
	//Returns pointer to the template we have made for the dialog.
	//Call this when you are ready to use the dialog template to
	//create a dialog.
	return m_pDlgTemplate;
}

//
//
//
//

LPWORD DialogTemplate::lpwAlign(LPWORD lpIn)
{
	DWORDLONG ul;

	//Return closest pointer that is aligned on a DWORD (4 byte) boundary.
	ul = (DWORDLONG) lpIn;
	ul +=3;
	ul >>=2;
	ul <<=2;
	return (LPWORD) ul;
}

//
//
//
//

int DialogTemplate::CopyAnsiToWideChar(LPWORD lpWCStr, LPCSTR lpAnsiIn)
{
	//Convert the string in lpAnsiIn to a wide character string.
	int cchAnsi = lstrlen(lpAnsiIn);

	return MultiByteToWideChar(GetACP(), MB_PRECOMPOSED, lpAnsiIn, cchAnsi, (LPWSTR)lpWCStr, cchAnsi) + 1;
}
