//
//
//    Filename: attriberror.h
//     Creator: Adam Fowler
//     $Author: Adam $
//       $Date: 30/06/99 15:40 $
//   $Revision: 4 $
// Description: Error handling classes for attribute classes
//
//
#ifndef INC_ATTRIBERROR_H_
#define INC_ATTRIBERROR_H_

#pragma warning (disable : 4251)
#pragma warning (disable : 4786)

// STD headers
#include <sstream>

// using namespace
using namespace rage;

#if defined(ATTRIBUTE_EXPORTS)
#define ATTRIBUTE_API __declspec(dllexport)
#elif defined(ATTRIBUTE_IMPORTS)
#define ATTRIBUTE_API __declspec(dllimport)
#else
#define ATTRIBUTE_API
#endif

namespace dmat {
	
	//
	//   Class Name: AttributeError
	// Base Classes: 
	//  Description: Base class for all attribute errors
	//
	class ATTRIBUTE_API AttributeError
	{
	};
	
	//
	//   Class Name: AttributeIdError
	// Base Classes: AttributeError
	//  Description: Base class for attribute errors which involve an attribute id
	//
	class ATTRIBUTE_API AttributeIdError
	{
	public:
		AttributeIdError(s32 id) : m_id(id) {}
		
		s32 GetId() {return m_id;}
		
	private:
		s32 m_id;
	};
	
	class ATTRIBUTE_API InvalidIdError : public AttributeIdError
	{
	public:
		InvalidIdError(s32 id) : AttributeIdError(id) {}
	};
	
	class ATTRIBUTE_API InvalidTypeError : public AttributeIdError
	{
	public:
		InvalidTypeError(s32 id) : AttributeIdError(id) {}
	};
	
	class ATTRIBUTE_API IdClashError : public AttributeIdError
	{
	public:
		IdClashError(s32 id) : AttributeIdError(id) {}
	};
	
	//
	//   Class Name: AttributeClassError
	// Base Classes: 
	//  Description: Base class for attribute errors which involve a class name
	//
	class ATTRIBUTE_API AttributeClassError : public AttributeError
	{
	public:
		AttributeClassError(const std::string &className) : m_className(className) {}
		
		const std::string &GetClassName() {return m_className;}
		
	private:
		std::string m_className;
	};
	
	class ATTRIBUTE_API ClassNameClashError : public AttributeClassError
	{
	public:
		ClassNameClashError(const std::string &className) : AttributeClassError(className) {}
	};

		
	//
	//   Class Name: AttributeFileError
	//  Description: Attribute file error class. Thrown when file does not exist, or file is invalid
	//
	class ATTRIBUTE_API AttributeFileError
	{
	public:
		AttributeFileError(const std::string &error) : m_error(error) {}

		const std::string &GetError() {return m_error;}
	protected:
		std::string m_error;

		// default constructor can be called from classes that inherit from this class
		AttributeFileError() {}
	};
	//
	//   Class Name: AttributeScriptError
	//  Description: Attribute script error class. Thrown when there is a syntax error in a script file
	//
	//
	class ATTRIBUTE_API AttributeScriptError : public AttributeFileError
	{
	public:
		AttributeScriptError(const std::string &error, s32 line) : AttributeFileError(error), m_line(line) {}
		AttributeScriptError(AttributeClassError &err, s32 line) : m_line(line)
		{
			std::ostringstream serror;
			serror << "Error with attribute class '" << err.GetClassName() << "'";
			m_error = serror.str();
		}
		AttributeScriptError(AttributeIdError &err, s32 line) : m_line(line)
		{
			std::ostringstream serror;
			serror << "Error with attribute ID " << err.GetId();
			m_error = serror.str();
		}

		s32 GetLineNumber() {return m_line;}
	private:
		s32 m_line;
	};
	

}; // namespace dmat

#endif // INC_ATTRIBERROR_H_