//
//
//    Filename: AttribMgr.cpp
//     Creator: Adam Fowler
//     $Author: Adam $
//       $Date: 23/06/99 12:16 $
//   $Revision: 3 $
// Description: Manager class for attribute system
//
//


// My headers
#include "AttribMgr.h"
#include "AttribScript.h"


using namespace dmat;

//
//        name: AttributeManager::AttributeManager
// description: Constructor
//
AttributeManager::AttributeManager()
{
	m_pClassMap = new AttributeClassMap;
	m_pDialogMap = new AttributeDialogMap;
}

//
//        name: AttributeManager::~AttributeManager
// description: Destructor
//
AttributeManager::~AttributeManager()
{
	delete m_pClassMap;
	delete m_pDialogMap;
}

//
//        name: AttributeManager::LoadScriptFile
// description: Load an attribute script file
//          in: pFilename = pointer filename string
//
void AttributeManager::ReadScriptFile(const char *pFilename)
{
	AttributeScriptFile file(pFilename);
	file.Read(*this);
}

//
//        name: AttributeManager::AddClass
// description: Adds a new class description to manager
//          in: className = name of class description
//				aClass = reference to class description
//
void AttributeManager::AddClass(const std::string &className, AttributeClass &aClass)
{
	if(!m_pClassMap->insert(std::make_pair(className, aClass)).second)
		throw ClassNameClashError(className);
}

//
//        name: &AttributeManager::GetClass
// description: Return class descriptor from a name
//          in: className = class name string
//         out: 
//
AttributeClass *AttributeManager::GetClass(const std::string &className)
{
	AttributeClassMapIterator iClass = m_pClassMap->find(className);
	if(iClass == m_pClassMap->end())
		return NULL;
	return &((*iClass).second);
}

//
//        name: *AttributeManager::CreateInstance
// description: Return an instance of a class given its name
//          in: className = name of class description
//         out: instance of class
//
AttributeInst *AttributeManager::CreateInstance(const std::string &className) const
{
	AttributeClassMapConstIterator iClass = m_pClassMap->find(className);
	if(iClass == m_pClassMap->end())
		return NULL;
	return new AttributeInst((*iClass).second);
}

//
//        name: *AttributeManager::IsClassAvailable
// description: Returns if a class is available
//          in: className = name of class description
//         out: true/false
//
bool AttributeManager::IsClassAvailable(const std::string &className) const
{
	AttributeClassMapConstIterator iClass = m_pClassMap->find(className);
	return (iClass != m_pClassMap->end());
}

//
//        name: AttributeManager::GetClassList
// description: Add all the class names to the provided list
//
void AttributeManager::GetClassList(std::list<std::string>& classList) const
{
	AttributeClassMapConstIterator iClass;

	for(iClass = m_pClassMap->begin(); iClass != m_pClassMap->end(); iClass++)
	{
		classList.push_back((*iClass).first);
	}
}

//
//        name: AttributeManager::AddDialogDesc
// description: Make another dialog descriptor available to the manager and associate it with a name
//          in: dialogName = name to associate dialog with
//				dialog = reference to dialog to add
//
void AttributeManager::AddDialogDesc(const std::string &dialogName, AttributeDialogDesc &dialog)
{
	if(!m_pDialogMap->insert(std::make_pair(dialogName, dialog)).second)
		throw ClassNameClashError(dialogName);
}

//
//        name: &AttributeManager::GetDialogDesc
// description: Return dialog descriptor from a name
//          in: dialogName = dialog name string
//         out: pointer to dialog descriptor
//
AttributeDialogDesc *AttributeManager::GetDialogDesc(const std::string &dialogName) 
{
	AttributeDialogMapIterator iDialog = m_pDialogMap->find(dialogName);
	if(iDialog == m_pDialogMap->end())
		return NULL;
	return &((*iDialog).second);
}

//
//        name: *AttributeManager::IsDialogAvailable
// description: Returns if a dialog is available
//          in: dialogName = name of dialog description
//         out: true/false
//
bool AttributeManager::IsDialogAvailable(const std::string &dialogName) const
{
	AttributeDialogMapConstIterator iDialog = m_pDialogMap->find(dialogName);
	return (iDialog != m_pDialogMap->end());
}

//
//        name: AttributeManager::GetDialogList
// description: Add all the dialog names to the provided list
//
void AttributeManager::GetDialogList(std::list<std::string>& dialogList) const
{
	AttributeDialogMapConstIterator iDialog;

	for(iDialog = m_pDialogMap->begin(); iDialog != m_pDialogMap->end(); iDialog++)
	{
		dialogList.push_back((*iDialog).first);
	}
}

//
//        name: AttributeManager::clear
// description: Delete all classes and dialog descriptions
//
void AttributeManager::Clear()
{
	m_pClassMap->clear();
	m_pDialogMap->clear();
}