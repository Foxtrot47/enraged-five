//
//
//    Filename: AttribMgr.h
//     Creator: Adam Fowler
//     $Author: Adam $
//       $Date: 23/06/99 12:17 $
//   $Revision: 3 $
// Description: The manager class for the attribute system. Reads attribute classes from a script
//				file. Creates instances of them. Reads dialog descriptions from script
//
//

#ifndef INC_ATTRIB_MGR_H_
#define INC_ATTRIB_MGR_H_

#pragma warning(disable : 4786)
#pragma warning(disable : 4290)

// my headers
#include "attribute.h"
#include "AttribDialog.h"
// STD headers
#include <map>
#include <string>
#include <list>

namespace dmat {


	typedef std::map<std::string, AttributeClass> AttributeClassMap;
	typedef std::map<std::string, AttributeClass>::iterator AttributeClassMapIterator;
	typedef std::map<std::string, AttributeClass>::const_iterator AttributeClassMapConstIterator;

	typedef std::map<std::string, AttributeDialogDesc> AttributeDialogMap;
	typedef std::map<std::string, AttributeDialogDesc>::iterator AttributeDialogMapIterator;
	typedef std::map<std::string, AttributeDialogDesc>::const_iterator AttributeDialogMapConstIterator;

	//
	//   Class Name: AttributeManager
	// Base Classes: 
	//  Description: Class controlling the attribute classes
	//    Functions: ReadScriptFile(): read attributes and dialogs from script file
	//				AddClass(): add a new attribute class and associate it with a name
	//				RemoveClass(): remove class from manager
	//				GetClassList(): return a list of the classes
	//				GetClass(): return class with associated name
	//				CreateInstance(): return an instance of a class with associated name
	//				AddDialogDesc(): add a new dialog description and associate it with a name
	//				RemoveDialog(): remove dialog from manager
	//				GetDialogDesc(): return dialog with associated name
	//				GetDialogList(): return a list of all the dialogs
	//				Clear(): remove all classes and dialogs from the Manager
	//
	//
	class ATTRIBUTE_API AttributeManager
	{
	public:
		AttributeManager();
		~AttributeManager();

		void ReadScriptFile(const char *pFilename) throw(AttributeFileError);
		AttributeInst *CreateInstance(const std::string &className) const;
		AttributeDialogDesc *GetDialogDesc(const std::string &dialogName) ;
		void Clear();

		bool IsClassAvailable(const std::string &className) const;
		void AddClass(const std::string &className, AttributeClass &aClass);
		void RemoveClass(const std::string &className) {m_pClassMap->erase(className);}
		AttributeClass *GetClass(const std::string &className) ;
		const AttributeClassMap *GetClassMap() const {return m_pClassMap;}
		void GetClassList(std::list<std::string> &classList) const;

		bool IsDialogAvailable(const std::string &dialogName) const;
		void AddDialogDesc(const std::string &dialogName, AttributeDialogDesc &dialog);
		void RemoveDialogDesc(const std::string &dialogName) {m_pDialogMap->erase(dialogName);}
		const AttributeDialogMap *GetDialogMap() const {return m_pDialogMap;}
		void GetDialogList(std::list<std::string> &dialogList) const;

	private:
		AttributeClassMap *m_pClassMap;
		AttributeDialogMap *m_pDialogMap;

		// make operator= and copy constructor private so it cannot be copied
		AttributeManager(const AttributeManager &/*mgr*/) {}
		void operator=(const AttributeManager &/*mgr*/) {}
	};


}; // namespace dmat

#endif // INC_ATTRIB_MGR_H_