//
//
//    Filename: AttribDialog.cpp
//     Creator: 
//     $Author: Adam $
//       $Date: 30/06/99 15:09 $
//   $Revision: 5 $
// Description: 
//
//

#pragma warning (disable : 4786)

// STD headers
#include <cmath>
#include <sstream>
// my headers
#include "AttribDialog.h"
#include "DynamicDialog.h"

s32 dmat::AttributeControlDesc::m_labelWidth = 0;
s32 dmat::AttributeControlDesc::m_checkboxLabelWidth = 0;
s32 dmat::AttributeControlDesc::m_comboBoxLabelWidth = 0;

#define START_POINT_X		4
#define START_POINT_Y		4
#define GAP_BETWEEN_CONTROLS	2
#define FONT_WIDTH			4
#define MAX_DIALOG_HEIGHT	250
#define STD_CONTROL_HEIGHT	12
#define CHECKBOX_HEIGHT		10
#define SPINNER_WIDTH		50
#define COMBOBOX_WIDTH		70
#define COMBOBOX_HEIGHT		100
#define EDITBOX_WIDTH		80
#define FILEBROWSER_WIDTH	80
#define MAX_COMBO_LABEL_LEN		7

using namespace dmat;

//
//        name: AttributeDialogDesc::AttributeDialogDesc
// description: copy constructor
//
AttributeDialogDesc::AttributeDialogDesc(const AttributeDialogDesc &orig)
{
	CopyDialogDesc(orig);
}


//
//        name: AttributeDialogDesc::GetValueControl
// description: get control by identifier
//          in: id = the resource identifier to look for
//         out: the found control or null if not found
//
AttributeControlDesc *AttributeDialogDesc::GetValueControl(s32 id, const AttributeControlDescList *list)
{
	AttributeControlDescListConstIterator iDesc;
	const AttributeControlDescList *pList2;
	AttributeValueControlDesc *pValueControl;

	if(!list)
		list = &m_list;

	for(iDesc = list->begin(); iDesc != list->end(); iDesc++)
	{
		// if control contains a sub-list of controls desc's add those to the control list
		pList2 = (*iDesc)->GetControlList();
		pValueControl = (*iDesc)->AsValueControl();
		if(pValueControl)
		{
			if(pValueControl->GetAttributeId() == id)
				return (*iDesc);
		}
		if(pList2)
		{
			AttributeControlDesc *pDesc = GetValueControl(id, pList2);
			if(pDesc)
				return pDesc;
		}
	}
	return NULL;
}

//
//        name: AttributeDialogDesc::~AttributeDialogDesc
// description: Destructor
//
AttributeDialogDesc::~AttributeDialogDesc()
{
	DeleteDialogDesc();
}


AttributeDialogDesc &AttributeDialogDesc::operator=(const AttributeDialogDesc &a)
{
	DeleteDialogDesc();
	CopyDialogDesc(a);

	return *this;
}

//
//        name: AttributeDialogDesc::DeleteDialogDesc
// description: Delete contents of dialog desc
//
void AttributeDialogDesc::DeleteDialogDesc()
{
	AttributeControlDescListIterator iDesc;

	for(iDesc = m_list.begin(); iDesc != m_list.end(); iDesc++)
	{
		delete (*iDesc);
	}
	m_list.clear();
}

//
//        name: AttributeDialogDesc::CopyDialogDesc
// description: Add copies of all the controls into the dialog
//
void AttributeDialogDesc::CopyDialogDesc(const AttributeDialogDesc &a)
{
	AttributeControlDescListConstIterator iDesc;

	for(iDesc = a.m_list.begin(); iDesc != a.m_list.end(); iDesc++)
	{
		m_list.push_back((*iDesc)->Copy());
	}
}

//
//        name: AttributeControl
// description: Constructor
//
AttributeDialog::AttributeControl::AttributeControl(AttributeValueControlDesc *pDesc) : m_pDesc(pDesc), 
m_resId(0), m_hWnd(NULL)
{
}

//
//        name: AttributeControl
// description: Constructor
//
AttributeDialog::AttributeControl::~AttributeControl()
{
}

//
//        name: AttributeControl::SyncWithAttribute
// description: Sync functions. Either set the value in the control dependent on the value in 
//				the attribute or set the attribute value based on the value in the control
//          in: inst = instance of attribute class 
//         out: 
//
void AttributeDialog::AttributeControl::SyncWithAttribute(AttributeInst &inst)
{
	assert(m_hWnd);
	int attrId = m_pDesc->GetAttributeId();

	if(m_pDesc->GetType() == AttributeControlDesc::CHECKBOX)
	{
		AttributeValue intVal = inst.GetAttributeValue(inst.GetClass().GetIndex(attrId));

		if(((bool)intVal) == false)
		{	
			m_pDesc->SetValue(AttributeValue((s32)0), m_hWnd);
		}
		else
		{
			m_pDesc->SetValue(AttributeValue((s32)1), m_hWnd);
		}	
	}
	else
	{
		m_pDesc->SetValue(inst.GetAttributeValue(inst.GetClass().GetIndex(attrId)), m_hWnd);
	}
}

void AttributeDialog::AttributeControl::SyncWithAttributeMultiple(AttributeInstMultiple &inst)
{
	assert(m_hWnd);
	int attrId = m_pDesc->GetAttributeId();
	int indexID = inst.GetClass().GetIndex(attrId);

	bool bUniv = inst.GetValueUniversal(indexID);

	if(m_pDesc->GetType() == AttributeControlDesc::CHECKBOX)
	{
		AttributeValue intVal = inst.GetAttributeValue(indexID);

		if(!bUniv)
		{
			m_pDesc->SetValue((s32)2, m_hWnd);
		}
		else if(((bool)intVal) == false)
		{	
			m_pDesc->SetValue((s32)0, m_hWnd);
		}
		else
		{
			m_pDesc->SetValue((s32)1, m_hWnd);
		}	
	}
	else
	{
		if(!bUniv)
		{
			m_pDesc->SetBlank(m_hWnd);
			if(m_pDesc->GetType()== AttributeControlDesc::EDITBOX)
			{
				m_pDesc->SetValue("<various>", m_hWnd);
			}
		}
		else
		{
			m_pDesc->SetValue(inst.GetAttributeValue(indexID), m_hWnd);
		}
	}
}

void AttributeDialog::AttributeControl::SyncWithControl(AttributeInst &inst)
{
	assert(m_hWnd);
	int attrId = m_pDesc->GetAttributeId();

	if(m_pDesc->GetType() == AttributeControlDesc::CHECKBOX)
	{
		AttributeValue intVal = m_pDesc->GetValue(m_hWnd);

		if(((s32)intVal) == 0)
		{
			inst.SetAttributeValue(inst.GetClass().GetIndex(attrId), false);
		}
		else
		{
			inst.SetAttributeValue(inst.GetClass().GetIndex(attrId), true);
		}
	}
	else
	{
		inst.SetAttributeValue(inst.GetClass().GetIndex(attrId), m_pDesc->GetValue(m_hWnd));
	}	
}

AttributeDialog::AttributeControl *AttributeDialog::FindControl(HWND controlHWnd)
{
	AttributeControlListIterator iCtrl;
	for(iCtrl = m_list.begin(); iCtrl != m_list.end(); iCtrl++)
	{
		if((*iCtrl).GetHWnd()==controlHWnd)
			return &(*iCtrl);
	}
	return NULL;
}

void AttributeDialog::AttributeControl::SyncWithControlMultiple(AttributeInstMultiple &inst)
{
	//only update if the value has changed
	//also make the value 'universal' so that it
	//updates back into every attribute instance

	assert(m_hWnd);

	if(!m_pDesc->HasValueChanged(m_hWnd))
	{
		return;
	}
	//check boxes are a special case because of the tri state
	//version of them for multiple attribute instances
	
	int attrId = m_pDesc->GetAttributeId();
	inst.SetValueUniversal(inst.GetClass().GetIndex(attrId),true);

	SyncWithControl(inst);
}

void AttributeDialog::AttributeControl::ProcessWindowMessage( HWND /*hwndDlg*/, UINT /*msg*/, WPARAM wParam, LPARAM lParam )
{
	HWND ctrlHWnd = (HWND)lParam;
	CheckBoxControlDesc *pCheckBox = dynamic_cast<CheckBoxControlDesc*>(m_pDesc);
// 	AttributeValueControlDesc *pValDesc = m_pDesc->AsValueControl();
	DWORD params = HIWORD(wParam);
	if(pCheckBox)
	{
		// Checkbox WM_COMMAND doesn't come with further params
		if(m_pDesc->IsChangeFunctorBound())
		{
			AttributeControlDescChangeEventFunctor changeFunc = m_pDesc->GetChangeFunctor();
			changeFunc(pCheckBox->GetName().c_str(), pCheckBox->GetValue(ctrlHWnd));
		}
		/** 
		/* Other interaction cases 
		**/
	}
	EditBoxControlDesc *pEditBox = dynamic_cast<EditBoxControlDesc*>(m_pDesc);
	if(pEditBox)
	{
		switch(params)
		{
		case EN_CHANGE:
			if(m_pDesc->IsChangeFunctorBound())
				m_pDesc->GetChangeFunctor()(pEditBox->GetName().c_str(), pEditBox->GetValue(ctrlHWnd));
			break;
		}
	}
	StringComboBoxControlDesc *pStringComboBox = dynamic_cast<StringComboBoxControlDesc*>(m_pDesc);
	if(pStringComboBox)
	{

		switch(params)
		{
 		case EN_CHANGE:
// 		case EN_UPDATE:
//		case LBN_SELCHANGE:
			if(m_pDesc->IsChangeFunctorBound())
				m_pDesc->GetChangeFunctor()(pStringComboBox->GetName().c_str(), pStringComboBox->GetValue(ctrlHWnd));
			break;
		}
	}
}

// operators
bool dmat::operator <(const dmat::AttributeDialog::AttributeControl& a, const dmat::AttributeDialog::AttributeControl& b)
{
	return a.m_resId < b.m_resId;
}
bool dmat::operator ==(const dmat::AttributeDialog::AttributeControl& a, const dmat::AttributeDialog::AttributeControl& b)
{
	return a.m_resId == b.m_resId;
}

// --- AttributeDialog -----------------------------------------------------------------------------------

//
//        name: AttributeDialog::~AttributeDialog
// description: Destructor
//
AttributeDialog::~AttributeDialog()
{
	if(m_pDialog)
		delete m_pDialog;
}

//
//        name: *AttributeDialog::GetTemplate
// description: Return dialog template
//
DLGTEMPLATE *AttributeDialog::GetTemplate()
{
	assert(m_pDialog);
	return (DLGTEMPLATE *)(m_pDialog->GetTemplate());
}

//
//        name: AttributeDialog::ConstructControlList
// description: Construct controls from a list.  
//          in: list = list of controls
//				rect = top/left indicates start position for creation
//				resId = first resource id 
//				canBreakUp = If set to true then the controls in the list can be arranged in 
//						more than one column
//         out: 
//
void AttributeDialog::ConstructControlList(const AttributeControlDescList &list, 
										   RECT &dialogRect, s32 &resId, bool canBreakUp)
{
	AttributeControlDescListConstIterator iDesc;
	const AttributeControlDescList *pList2;
	AttributeValueControlDesc *pValueControl;
	const AttributeClass &aclass = m_pInst->GetClass();
	s32 width,widthCheck,widthCombo;
	RECT rect;

	rect.top = dialogRect.top;
	rect.left = dialogRect.left;
	dialogRect.bottom = 0;
	dialogRect.right = 0;

	// construct list of controls. Every control that has a valid id for this attribute class
	for(iDesc = list.begin(); iDesc != list.end(); iDesc++)
	{
		if(rect.top > m_height)
		{
			if(canBreakUp)
			{
				rect.top = START_POINT_Y;
				rect.left = dialogRect.left + dialogRect.right + 8;
			}
			else
			{
				rect.top = dialogRect.top;
				rect.left = dialogRect.left + dialogRect.right + 4;
			}
		}

		// if control contains a sub-list of controls desc's add those to the control list
		pList2 = (*iDesc)->GetControlList();
		pValueControl = (*iDesc)->AsValueControl();
		if(pList2)
		{
			//RECT listRect = rect;
			rect.top +=10;
			rect.left +=8;

			// store old label width
			width = AttributeControlDesc::GetLabelWidth();
			widthCheck = AttributeControlDesc::GetCheckBoxLabelWidth();
			widthCombo = AttributeControlDesc::GetComboBoxLabelWidth();
			SetMaxLabelWidth(*pList2);

			// ALupinacci 7/21/08 - Removed these variables because they aren't being used
			//s32 currWidth = AttributeControlDesc::GetLabelWidth();
			//s32 currWidthCheck = AttributeControlDesc::GetCheckBoxLabelWidth();
			//s32 currWidthCombo = AttributeControlDesc::GetComboBoxLabelWidth();

			// construct UI for attributes in group
			ConstructControlList(*pList2, rect, resId, false);

			rect.left -= 8;
			rect.right += 16;
			rect.top -= 10;
			rect.bottom += 14;
//			rect.right = 60 + AttributeControlDesc::GetLabelWidth();
//			rect.bottom = listRect.top - rect.top;

			AttributeControlDesc::SetLabelWidth(width);
			AttributeControlDesc::SetCheckBoxLabelWidth(widthCheck);
			AttributeControlDesc::SetComboBoxLabelWidth(widthCombo);
		}
		if(pValueControl)
		{
			const Attribute &attr = aclass[pValueControl->GetAttributeId()];

			aclass.CheckIdType(pValueControl->GetAttributeId(), pValueControl->GetEditType());

			// construct control
			AttributeControl control(pValueControl);
			control.SetResourceId(resId);
			// create UI
			// set control name from attribute
			(*iDesc)->SetName(attr.GetName());

			// add to control list
			AddControl(control);
		}

		bool bMultiple = false;

		if(m_pInstMult)
		{
			AttributeValueControlDesc* p_Valdesc = dynamic_cast<AttributeValueControlDesc*>(*iDesc);

			if(p_Valdesc)
			{
				bMultiple = !(m_pInstMult->GetValueUniversal(m_pInstMult->GetClass().GetIndex(p_Valdesc->GetAttributeId())));
			}
		}
		
		AttributeControlDesc *pDesc = (*iDesc);
		AttributeControlDescInitEventFunctor initFunctor = pDesc->GetInitFunctor();
		if(pDesc->IsInitFunctorBound() && pDesc->GetType()==AttributeControlDesc::TCOMBOBOX)
		{
			StringComboBoxControlDesc *cmbDesc = dynamic_cast<StringComboBoxControlDesc *>(pDesc);
			if(cmbDesc)
			{
				std::string *pString = (std::string*)initFunctor(pDesc->GetName().c_str());
				cmbDesc->SetEntriesString(*pString);
			}
		}

		// Create the bugger
		pDesc->Create(m_pDialog, resId, rect, bMultiple);

// 		if(initFunctor.IsBound() && pDesc->GetType()==AttributeControlDesc::TCOMBOBOX)
// 		{
// 			//			pDesc->SetName(initFunctor(pDesc->GetName()));
// 			switch(pDesc->GetType())
// 			{
// 			case AttributeControlDesc::CHECKBOX:
// 				{
// 					CheckBoxControlDesc *cmbDesc = dynamic_cast<CheckBoxControlDesc *>(pDesc);
// 					if(cmbDesc)
// 					{
// 						bool *pBool = (bool *)initFunctor(pDesc->GetName());
// 						cmbDesc->SetRawValue(*pBool);
// 					}
// 				}
// 				break;
// 			case AttributeControlDesc::EDITBOX:
// 				{
// 					EditBoxControlDesc *cmbDesc = dynamic_cast<EditBoxControlDesc *>(pDesc);
// 					if(cmbDesc)
// 					{
// 						std::string *pString = (std::string*)initFunctor(pDesc->GetName());
// 						cmbDesc->SetValue(pString->c_str(), );
// 					}
// 				}
// 				break;
// 			default:
// 				break;
// 			}
// 		}

		IncludeControlRect(rect, dialogRect);

		rect.top += rect.bottom + GAP_BETWEEN_CONTROLS;
	}

}


//
//        name: AttributeDialog::GetControlListHeight
// description: Return the height of the list of controls
//
s32 AttributeDialog::GetControlListHeight(const AttributeControlDescList &list)
{
	AttributeControlDescListConstIterator iDesc;
	const AttributeControlDescList *pList2;
	s32 height = 0;

	// add height from each control
	for(iDesc = list.begin(); iDesc != list.end(); iDesc++)
	{
		pList2 = (*iDesc)->GetControlList();
		if(pList2)
			height += GetControlListHeight(*pList2);
		height += (*iDesc)->GetHeight() + GAP_BETWEEN_CONTROLS;
	}
	return height;
}

//
//        name: AttributeDialog::GetMaxLabelWidth
// description: Returns the maximum width the labels should be to contain all the control strings
//          in: list = 
//         out: 
//
void AttributeDialog::SetMaxLabelWidth(const AttributeControlDescList &list)
{
	AttributeControlDescListConstIterator iDesc;
	AttributeValueControlDesc *pValueControl;
	const AttributeClass &aclass = m_pInst->GetClass();
	u32 len = 2, cbLen = 2, comboLen = 2;	// minimum length
	bool bAnyCheckBoxes = false;
	bool bAnyComboBoxes = false;
	// get name from each control
	for(iDesc = list.begin(); iDesc != list.end(); iDesc++)
	{
		pValueControl = (*iDesc)->AsValueControl();
		if(pValueControl)
		{
			const Attribute &attr = aclass[pValueControl->GetAttributeId()];
			if(pValueControl->GetType() == AttributeControlDesc::CHECKBOX)
			{
				bAnyCheckBoxes = true;

				if(attr.GetName().length() > cbLen)
					cbLen = (u32)attr.GetName().length();
			}
			else if(pValueControl->GetType() == AttributeControlDesc::COMBOBOX)
			{
				bAnyComboBoxes = true;

				if(attr.GetName().length() > comboLen)
					comboLen = (u32)attr.GetName().length();
			}
			else
			{
				if(attr.GetName().length() > len)
					len = (u32)attr.GetName().length();
			}
		}
	}

	AttributeControlDesc::SetLabelWidth(len * FONT_WIDTH);

	if(bAnyCheckBoxes)
	{
		AttributeControlDesc::SetCheckBoxLabelWidth(cbLen * FONT_WIDTH + 10); //add forty for the actual check box
	}
	if(bAnyComboBoxes)
	{
		if(comboLen > MAX_COMBO_LABEL_LEN)
		{
			comboLen = MAX_COMBO_LABEL_LEN;
		}
		AttributeControlDesc::SetComboBoxLabelWidth(comboLen * FONT_WIDTH);
	}
}

//
//        name: AttributeDialog::IncludeControlRect
// description: Make sure dialog rect contains this rect. Assumes dialog upper corner is (0,0)
//          in: 
//         out: 
//
void AttributeDialog::IncludeControlRect(RECT &rect, RECT &dialogRect)
{
	if(rect.right + rect.left > dialogRect.right + dialogRect.left)
		dialogRect.right = rect.right + rect.left - dialogRect.left;
	if(rect.bottom + rect.top > dialogRect.bottom + dialogRect.top)
		dialogRect.bottom = rect.bottom + rect.top - dialogRect.top;
}

void AttributeDialog::ConstructDialog2(HINSTANCE hInstance, AttributeInstMultiple *pInst, const AttributeDialogDesc *pDesc, const char *pTitle)
{
	s32 resId;
	RECT dialogRect, rect;

	// ALupinacci 7/21/08 - Removed this variable because it isn't being used
	//bool deleteDesc = false;

	m_pInst = pInst;
	m_pInstMult = pInst;

	dialogRect.top = 0;
	dialogRect.bottom = 0;
	dialogRect.left = 0;
	dialogRect.right = 80;

	m_list.clear();

	// construct dynamic dialog
	m_pDialog = new DynamicDialog(hInstance);
	m_pDialog->Init(DS_MODALFRAME | DS_CENTERMOUSE | WS_CAPTION | WS_VISIBLE, 
		dialogRect, 
		pTitle,
		"MS Sans Serif", 8);

	SetMaxLabelWidth(pDesc->GetList());

	// calculate what height of dialog should be. If dialog is taller than the maximum height 
	// then set a new maximum height so that the controls are evenly spread between the columns 
	// of controls
	m_height = GetControlListHeight(pDesc->GetList()) + START_POINT_Y;

	if(m_height > MAX_DIALOG_HEIGHT)
	{
		s32 columns = (s32)ceil(m_height / (float)MAX_DIALOG_HEIGHT);
		m_height /= columns;
	}

	// add controls to dialog
	rect.top = START_POINT_Y; 
	rect.left = START_POINT_X;
	resId = 2000;

	ConstructControlList(pDesc->GetList(), rect, resId, true);

	IncludeControlRect(rect, dialogRect);

	dialogRect.right += 8;
	dialogRect.bottom += 28;
	m_pDialog->SetDialogSize(dialogRect);

	// add OK and Cancel
	RECT buttonRect;
	buttonRect.top = dialogRect.bottom - 20;
	buttonRect.bottom = 15;
	buttonRect.left = 10;
	buttonRect.right = 30;
	m_pDialog->AddButtonCtrl(0, buttonRect, IDOK, "Ok");
	buttonRect.left = 44;
	m_pDialog->AddButtonCtrl(0, buttonRect, IDCANCEL, "Cancel");
}

//
//        name: AttributeDialog::ConstructDialog
// description: Construct the dialog from a dialog description and an instance of an attribute class
//          in: pInst = instance of attribute class
//				pDesc = dialog description
//         out: 
//
void AttributeDialog::ConstructDialog(HINSTANCE hInstance, AttributeInst *pInst, const AttributeDialogDesc *pDesc, const char *pTitle)
{
	s32 resId;
	RECT dialogRect, rect;

	// ALupinacci 7/21/08 - Removed this variable because it isn't being used
	//bool deleteDesc = false;

	m_pInstMult = NULL;
	m_pInst = pInst;

	dialogRect.top = 0;
	dialogRect.bottom = 0;
	dialogRect.left = 0;
	dialogRect.right = 80;

	m_list.clear();

	// construct dynamic dialog
	m_pDialog = new DynamicDialog(hInstance);
	m_pDialog->Init(DS_MODALFRAME | DS_CENTERMOUSE | WS_CAPTION | WS_VISIBLE, 
		dialogRect, 
		pTitle,
		"MS Sans Serif", 8);

	SetMaxLabelWidth(pDesc->GetList());

	// calculate what height of dialog should be. If dialog is taller than the maximum height 
	// then set a new maximum height so that the controls are evenly spread between the columns 
	// of controls
	m_height = GetControlListHeight(pDesc->GetList()) + START_POINT_Y;

	if(m_height > MAX_DIALOG_HEIGHT)
	{
		s32 columns = (s32)ceil(m_height / (float)MAX_DIALOG_HEIGHT);
		m_height /= columns;
	}

	// add controls to dialog
	rect.top = START_POINT_Y; 
	rect.left = START_POINT_X;
	resId = 2000;

	ConstructControlList(pDesc->GetList(), rect, resId, true);

	IncludeControlRect(rect, dialogRect);

	dialogRect.right += 8;
	dialogRect.bottom += 28;
	m_pDialog->SetDialogSize(dialogRect);

	// add OK and Cancel
	RECT buttonRect;
	buttonRect.top = dialogRect.bottom - 20;
	buttonRect.bottom = 15;
	buttonRect.left = 10;
	buttonRect.right = 30;
	m_pDialog->AddButtonCtrl(0, buttonRect, IDOK, "Ok");
	buttonRect.left = 44;
	m_pDialog->AddButtonCtrl(0, buttonRect, IDCANCEL, "Cancel");

}

//
//        name: AttributeDialog::ConstructMaxPanel
// description: Construct the dialog from a dialog description and an instance of an attribute class
//          in: pInst = instance of attribute class
//				pDesc = dialog description
//         out: 
//
#define MAX_ROLLUP_WIDTH_COMMAND_PANEL	108
#define MAX_ROLLUP_WIDTH_MATERIAL_EDITOR	216
void AttributeDialog::ConstructMaxRollup(AttributeInst *pInst, const AttributeDialogDesc *pDesc, bool inMaterialEditor)
{
	s32 resId;
	RECT dialogRect, rect;

	// ALupinacci 7/21/08 - Removed this variable because it isn't being used
	//bool deleteDesc = false;

	m_pInst = pInst;

	dialogRect.top = 0;
	dialogRect.bottom = 0;
	dialogRect.left = 0;
	dialogRect.right = MAX_ROLLUP_WIDTH_COMMAND_PANEL;
	if(inMaterialEditor)
		dialogRect.right = MAX_ROLLUP_WIDTH_MATERIAL_EDITOR;

	m_list.clear();
	m_height = 5000;

	// construct dynamic dialog
	m_pDialog = new DynamicDialog;
	m_pDialog->Init(WS_CHILD | WS_VISIBLE, 
		dialogRect, 
		"Attributes",
		"MS Sans Serif", 8);

	SetMaxLabelWidth(pDesc->GetList());
	if(AttributeControlDesc::GetLabelWidth() > 40)
		AttributeControlDesc::SetLabelWidth(40);

	// add controls to dialog
	rect.top = START_POINT_Y; 
	rect.left = START_POINT_X;
	resId = 2000;

	ConstructControlList(pDesc->GetList(), rect, resId, true);

	IncludeControlRect(rect, dialogRect);
	dialogRect.bottom += 8;
	dialogRect.right = MAX_ROLLUP_WIDTH_COMMAND_PANEL;
	if(inMaterialEditor)
		dialogRect.right = MAX_ROLLUP_WIDTH_MATERIAL_EDITOR;
	m_pDialog->SetDialogSize(dialogRect);
}

//
//        name: AttributeDialog::CanEditInstance
// description: Calls CheckIdType() for all the controls in the dialog
//
void AttributeDialog::CanEditInstance()
{
	AttributeControlListIterator iControl;
	const AttributeClass &aclass = m_pInst->GetClass();

	for(iControl = m_list.begin(); iControl != m_list.end(); iControl++)
	{
		aclass.CheckIdType((*iControl).m_pDesc->GetAttributeId(), (*iControl).m_pDesc->GetEditType());
	}
}

//
//        name: AttributeDialog::Edit
// description: Set new instance to be edited by dialog
//          in: pInst = pointer to instance
//
void AttributeDialog::Edit(AttributeInst *pInst)
{
	m_pInst = pInst;
	CanEditInstance();
	// if dialog is already displayed. The DynamicDialog pointer gets deleted in InitDialog()
	if(m_pDialog == NULL)
		SyncWithAttributes();
}

//
//        name: AttributeDialog::InitDialog
// description: Initialise dialog (call when WM_INITDIALOG message is sent)
//          in: hDlgWnd = handle to dialog window
//
void AttributeDialog::InitDialog(HWND hDlgWnd)
{
	AttributeControlListIterator iControl;
	assert(m_pDialog);

	m_pDialog->InitDialog(hDlgWnd);
	delete m_pDialog;
	m_pDialog = NULL;

	for(iControl = m_list.begin(); iControl != m_list.end(); iControl++)
	{
		(*iControl).FindHWnd(hDlgWnd);
	}

	SyncWithAttributes();
}

//
//        name: AttributeDialog::SyncWithControls/SyncWithAttributes
// description: Take information from controls and record in attributes
//
void AttributeDialog::SyncWithControls()
{
	AttributeControlListIterator iControl;

	for(iControl = m_list.begin(); iControl != m_list.end(); iControl++)
	{
		if(m_pInstMult)
		{
			(*iControl).SyncWithControlMultiple(*m_pInstMult);
		}
		else if(m_pInst)
		{
			(*iControl).SyncWithControl(*m_pInst);
		}
	}
}

void AttributeDialog::SyncWithAttributes()
{
	AttributeControlListIterator iControl;

	for(iControl = m_list.begin(); iControl != m_list.end(); iControl++)
	{
		if(m_pInstMult)
		{
			(*iControl).SyncWithAttributeMultiple(*m_pInstMult);
		}
		else
		{
			(*iControl).SyncWithAttribute(*m_pInst);
		}
	}
}

// --- Controls ---------------------------------------------------------------------------------------

// void* AttributeControlDesc::ReturnZero1(const char*)
// {
// 	return NULL;
// }
// 
// void* AttributeControlDesc::ReturnZero2(const char*, dmat::AttributeValue)
// {
// 	return NULL;
// }

AttributeControlDesc::AttributeControlDesc()
 : m_initFunctor(NULL)
 , m_changeFunctor(NULL)
{
//  	m_initFunctor.Clear();// = MakeFunctorRet(ReturnZero1);
//  	m_changeFunctor.Clear();// = MakeFunctorRet(ReturnZero2);

	// 	m_changeFunctor = new AttributeControlDescChangeEventFunctor();
// 	m_changeFunctor->Reset();
// 	m_initFunctor = new AttributeControlDescInitEventFunctor();
// 	m_initFunctor->Reset();

// 	int initSize = sizeof(m_changeFunctor);
// 	int changeSize = sizeof(m_initFunctor);
// 	initSize = sizeof(AttributeControlDescInitEventFunctor);
// 	changeSize = sizeof(AttributeControlDescChangeEventFunctor);
}
AttributeControlDesc::~AttributeControlDesc()
{
//	delete m_changeFunctor;
//	m_changeFunctor = NULL;
//	delete m_initFunctor;
//	m_initFunctor = NULL;
}

void AttributeControlDesc::CreateLabel(DynamicDialog *pDialog, s32 &id, RECT rect)
{
	rect.left -= m_labelWidth + 4;
	rect.right = m_labelWidth;
	rect.top += 2;
	rect.bottom -= 2;

	pDialog->AddStaticCtrl(SS_LEFTNOWORDWRAP, rect, (WORD)id++, m_name.c_str());
}

void AttributeControlDesc::CreateComboLabel(DynamicDialog *pDialog, s32 &id, RECT rect)
{
	rect.left -= m_comboBoxLabelWidth + 4;
	rect.right = m_comboBoxLabelWidth;
	rect.top += 2;
	rect.bottom -= 2;

	pDialog->AddStaticCtrl(SS_LEFTNOWORDWRAP, rect, (WORD)id++, m_name.c_str());
}

void AttributeValueControlDesc::SetValue(const AttributeValue& value, HWND /*hWnd*/)
{
	m_checkSet = value;
}

bool AttributeValueControlDesc::HasValueChanged(HWND hWnd)
{
	AttributeValue valNew = GetValue(hWnd);

	if(valNew == m_checkSet && valNew.IsBlank()==m_checkSet.IsBlank())
	{
		return false;
	}

	return true;
}

// --- IntSpinnerControlDesc --------------------------------------------------------------------------
//
//        name: IntSpinnerControl::GetValue,SetValue,Create
// description: Get/Set data for control and create control functions
//
AttributeValue IntSpinnerControlDesc::GetValue(HWND hWnd)
{
	char contents[32];
	SendMessage(hWnd, WM_GETTEXT, 32, (LPARAM)&contents);
	
	AttributeValue attrVal((s32)atoi(&contents[0]));
	if(0==strcmp(contents, ""))
		attrVal.SetBlank(true);

	return attrVal;
}

void IntSpinnerControlDesc::SetValue(const AttributeValue& value, HWND hWnd)
{
	std::ostringstream string;

	AttributeValueControlDesc::SetValue(value,hWnd);

	string << s32(value) << std::ends;
	SendMessage(hWnd, WM_SETTEXT, 0, (LPARAM)string.str().c_str());
}

void IntSpinnerControlDesc::Create(DynamicDialog *pDialog, s32 &id, RECT &rect,bool /*bMultipleEdit*/)
{
	RECT localRect = rect;

	// set rect to include whole area that control covers
	rect.right = m_labelWidth + 4 + SPINNER_WIDTH;
	rect.bottom = STD_CONTROL_HEIGHT;

	localRect.left += m_labelWidth + 4;
	localRect.right = SPINNER_WIDTH;
	localRect.bottom = STD_CONTROL_HEIGHT;

	id = pDialog->AddIntSpinner(localRect, (WORD)id, m_min, m_max);

	CreateLabel(pDialog, id, localRect);
}

void IntSpinnerControlDesc::SetBlank(HWND hWnd)
{
	std::ostringstream string;

	string << std::ends;

	m_checkSet = (s32)0;
	m_checkSet.SetBlank(true);

	SendMessage(hWnd, WM_SETTEXT, 0, (LPARAM)string.str().c_str());
}

bool IntSpinnerControlDesc::IsBlank(HWND hWnd)
{
	char contents[32];
	SendMessage(hWnd, WM_GETTEXT, 32, (LPARAM)&contents);
	
	if(contents[0] == '\0')
		return true;
	else
		return false;
}

// --- FloatSpinnerControlDesc ------------------------------------------------------------------------

AttributeValue FloatSpinnerControlDesc::GetValue(HWND hWnd)
{
	char contents[32];
	s32 decimal;
	float value;

	decimal = (s32)GetWindowLongPtr(hWnd, GWLP_USERDATA);

	SendMessage(hWnd, WM_GETTEXT, 32, (LPARAM)&contents);
	value = (float)atoi(&contents[0]);
	
	int decimalSave = decimal;
	while(decimal--)
		value /= 10.0f;

	AttributeValue attrVal(value);
	if(strlen(contents)==decimalSave)
		attrVal.SetBlank(true);

	return attrVal;
}

void FloatSpinnerControlDesc::SetValue(const AttributeValue& value, HWND hWnd)
{
	std::ostringstream string;
	s32 decimal;

	AttributeValueControlDesc::SetValue(value,hWnd);

	float value2 = value;	
	decimal = (s32)GetWindowLongPtr(hWnd, GWLP_USERDATA);
	while(decimal--)
		value2 *= 10;

	// Some stupid fuck up from Microsoft. In debug mode I need to call either ceil or floor to get
	// the correct value for 'value2', but in release mode I don't
#ifdef _DEBUG
	if(value2 >= 0)
		string << ceil(value2) << std::ends;
	else
		string << floor(value2) << std::ends;
#else
	string << value2 << std::ends;
#endif

	SendMessage(hWnd, WM_SETTEXT, 0, (LPARAM)string.str().c_str());
}

void FloatSpinnerControlDesc::Create(DynamicDialog *pDialog, s32 &id, RECT &rect,bool /*bMultipleEdit*/)
{
	RECT localRect = rect;

	// set rect to include whole area that control covers
	rect.right = m_labelWidth + 4 + SPINNER_WIDTH;
	rect.bottom = STD_CONTROL_HEIGHT;

	localRect.left += m_labelWidth + 4;
	localRect.right = SPINNER_WIDTH;
	localRect.bottom = STD_CONTROL_HEIGHT;

	m_spinid = id + 1;

	if(m_max > 9999.999f)
		id = pDialog->AddFloatSpinner(localRect, (WORD)id, m_min, m_max, 1);
	else if(m_max > 999.999f)
		id = pDialog->AddFloatSpinner(localRect, (WORD)id, m_min, m_max, 2);
	else
		id = pDialog->AddFloatSpinner(localRect, (WORD)id, m_min, m_max, 3);

	CreateLabel(pDialog, id, localRect);
}

void FloatSpinnerControlDesc::SetBlank(HWND hWnd)
{
	std::ostringstream string;

	string << std::ends;

	m_checkSet = 0.0f;
	m_checkSet.SetBlank(true);

	SendMessage(hWnd, WM_SETTEXT, 0, (LPARAM)string.str().c_str());
}

bool FloatSpinnerControlDesc::IsBlank(HWND hWnd)
{
	//LRESULT res = SendMessage(GetDlgItem(GetParent(hWnd),m_spinid),UDM_GETPOS,0,0);

	//if(HIWORD(res) > 0)
	{
		char contents[32];
		SendMessage(hWnd, WM_GETTEXT, 32, (LPARAM)&contents);
		s32 decimal = (s32)GetWindowLongPtr(hWnd, GWLP_USERDATA);
		s32 i,count = (s32)strlen(contents);

		if(count <= decimal)
		{
			for(i=0;i<count;i++)
			{
				if(contents[i] != '0')
					return false;
			}

			return true;
		}

		return false;
	}

	//return false;
}


// --- CheckBoxControlDesc ----------------------------------------------------------------------------

AttributeValue CheckBoxControlDesc::GetValue(HWND hWnd)
{
	LRESULT result = SendMessage(hWnd, BM_GETCHECK, 0, 0);

	if(result == BST_CHECKED)
		return (s32)1;
	else if((result == BST_INDETERMINATE) && m_bMultipleEdit)
		return (s32)2;
	else
		return (s32)0;
}

void CheckBoxControlDesc::SetValue(const AttributeValue& value, HWND hWnd)
{
	WPARAM wParam;

	AttributeValueControlDesc::SetValue(value,hWnd);

	if(s32(value) == 1)
		wParam = BST_CHECKED;
	else if(m_bMultipleEdit && (s32(value) == 2))
		wParam = BST_INDETERMINATE;
	else
		wParam = BST_UNCHECKED;

	SendMessage(hWnd, BM_SETCHECK, wParam, 0);
}

void CheckBoxControlDesc::Create(DynamicDialog *pDialog, s32 &id, RECT &rect,bool bMultipleEdit)
{
	m_bMultipleEdit = bMultipleEdit;

	rect.right = m_checkboxLabelWidth + 16;
	rect.bottom = CHECKBOX_HEIGHT;

	pDialog->AddCheckBoxCtrl(0, rect, (WORD)id++, m_name.c_str(),m_bMultipleEdit);
}

void CheckBoxControlDesc::SetBlank(HWND /*hWnd*/)
{
	m_checkSet = (s32)2;
}

bool CheckBoxControlDesc::IsBlank(HWND hWnd)
{
	LRESULT result = SendMessage(hWnd, BM_GETCHECK, 0, 0);

	if(result == (s32)2)
		return true;
	else
		return false;
}

// --- StringComboBoxControlDesc ----------------------------------------------------------------------------

AttributeValue StringComboBoxControlDesc::GetValue(HWND hWnd)
{
	char str[256] = {0};
	LRESULT result = SendMessage(hWnd, CB_GETCURSEL, 0, 0);

	if(result != -1)
	{
		SendMessage(hWnd, CB_GETLBTEXT, result, (LPARAM)&str[0]);
	}
	else
	{
		str[0] = '\0';
	}

	return &str[0];
}

void StringComboBoxControlDesc::SetValue(const AttributeValue& value, HWND hWnd)
{
	AttributeValueControlDesc::SetValue(value,hWnd);

	char str[256];
	int iSize = (int)SendMessage(hWnd, CB_GETCOUNT, 0, 0);

	for(int i = 0;i<iSize;i++)
	{
		SendMessage(hWnd, CB_GETLBTEXT, i, (LPARAM)&str[0]);

		if(_stricmp((const char *)(value),str) == 0)
		{
			SendMessage(hWnd, CB_SETCURSEL, i, 0);
			return;
		}
	}
}

void StringComboBoxControlDesc::Create(DynamicDialog *pDialog, s32 &id, RECT &rect,bool /*bMultipleEdit*/)
{
	RECT localRect = rect;

	// set rect to include whole area that control covers
	rect.right = m_comboBoxLabelWidth + 4 + COMBOBOX_WIDTH;
	rect.bottom = STD_CONTROL_HEIGHT;

	localRect.left += m_comboBoxLabelWidth + 4;
	localRect.right = COMBOBOX_WIDTH;
	localRect.bottom = COMBOBOX_HEIGHT;

	id = pDialog->AddComboBox(localRect, (WORD)id, m_string.c_str());

	CreateComboLabel(pDialog, id, localRect);
}

void StringComboBoxControlDesc::SetBlank(HWND hWnd)
{
	SendMessage(hWnd, CB_SETCURSEL, (WPARAM)-1, 0);

	m_checkSet = "";
}

bool StringComboBoxControlDesc::IsBlank(HWND hWnd)
{
	s32 index = (s32)SendMessage(hWnd, CB_GETCURSEL, (WPARAM)-1, 0);
	char theString[_MAX_PATH] = {0};
	SendMessage( hWnd , CB_GETLBTEXT, index, (LPARAM)theString );

	if(strlen(theString)==0)
		return true;
	else 
		return false;
}

// --- ComboBoxControlDesc ----------------------------------------------------------------------------

AttributeValue ComboBoxControlDesc::GetValue(HWND hWnd)
{
	LRESULT result = SendMessage(hWnd, CB_GETCURSEL, 0, 0);

	return (s32)result;
}

void ComboBoxControlDesc::SetValue(const AttributeValue& value, HWND hWnd)
{
	AttributeValueControlDesc::SetValue(value,hWnd);

	SendMessage(hWnd, CB_SETCURSEL, s32(value), 0);
}

void ComboBoxControlDesc::Create(DynamicDialog *pDialog, s32 &id, RECT &rect,bool /*bMultipleEdit*/)
{
	RECT localRect = rect;

	// set rect to include whole area that control covers
	rect.right = m_comboBoxLabelWidth + 4 + COMBOBOX_WIDTH;
	rect.bottom = STD_CONTROL_HEIGHT;

	localRect.left += m_comboBoxLabelWidth + 4;
	localRect.right = COMBOBOX_WIDTH;
	localRect.bottom = COMBOBOX_HEIGHT;

	id = pDialog->AddComboBox(localRect, (WORD)id, m_string.c_str());

	CreateComboLabel(pDialog, id, localRect);
}

void ComboBoxControlDesc::SetBlank(HWND hWnd)
{
	SendMessage(hWnd, CB_SETCURSEL, (WPARAM)-1, 0);

	m_checkSet = (s32)-1;
}

bool ComboBoxControlDesc::IsBlank(HWND hWnd)
{
	s32 index = (s32)SendMessage(hWnd, CB_GETCURSEL, 0, 0);

	if(index == -1)
		return true;
	else 
		return false;
}

// --- GroupBoxControlDesc ----------------------------------------------------------------------------

void GroupBoxControlDesc::Create(DynamicDialog *pDialog, s32 &id, RECT &rect,bool /*bMultipleEdit*/)
{
	pDialog->AddGroupBoxCtrl(0, rect, (WORD)id++, m_name.c_str());
}

// --- EditBoxControlDesc ----------------------------------------------------------------------------

AttributeValue EditBoxControlDesc::GetValue(HWND hWnd)
{
	char str[256];

	SendMessage(hWnd, WM_GETTEXT, 256, (LPARAM)&str[0]);

	return &str[0];
}

void EditBoxControlDesc::SetValue(const AttributeValue& value, HWND hWnd)
{
	AttributeValueControlDesc::SetValue(value,hWnd);

	SendMessage(hWnd, WM_SETTEXT, 0, (LPARAM)(const char *)(value));
}

void EditBoxControlDesc::Create(DynamicDialog *pDialog, s32 &id, RECT &rect,bool /*bMultipleEdit*/)
{
	RECT localRect = rect;

	// set rect to include whole area that control covers
	rect.right = m_labelWidth + 4 + EDITBOX_WIDTH;
	rect.bottom = STD_CONTROL_HEIGHT;

	localRect.left += m_labelWidth + 4;
	localRect.right = EDITBOX_WIDTH;
	localRect.bottom = STD_CONTROL_HEIGHT;

	pDialog->AddEditCtrl(WS_BORDER|ES_AUTOHSCROLL, localRect, (WORD)id++, "");

	CreateLabel(pDialog, id, localRect);
}

void EditBoxControlDesc::SetBlank(HWND /*hWnd*/)
{
	m_checkSet = "";
	m_WasBlank = true;
}

bool EditBoxControlDesc::IsBlank(HWND hWnd)
{
	if(m_WasBlank)
	{
		AttributeValue val = GetValue(hWnd);

		if(((char*)val)[0] == '\0')
			return true;
		else
			return false;
	}
	else
	{
		return false;
	}
}

bool EditBoxControlDesc::HasValueChanged(HWND hWnd)
{
	AttributeValue val = GetValue(hWnd);

	if(strcmp(m_checkSet,val) == 0)
	{
		return false;
	}

	return true;
}

// --- FileBrowserControlDesc ----------------------------------------------------------------------------

AttributeValue FileBrowserControlDesc::GetValue(HWND hWnd)
{
	char str[256];

	SendMessage(hWnd, WM_GETTEXT, 256, (LPARAM)&str[0]);

	return &str[0];
}

void FileBrowserControlDesc::SetValue(const AttributeValue& value, HWND hWnd)
{
	AttributeValueControlDesc::SetValue(value,hWnd);

	SendMessage(hWnd, WM_SETTEXT, 0, (LPARAM)(const char *)(value));
}

void FileBrowserControlDesc::Create(DynamicDialog *pDialog, s32 &id, RECT &rect,bool /*bMultipleEdit*/)
{
	RECT localRect = rect;

	// set rect to include whole area that control covers
	rect.right = m_labelWidth + 4 + FILEBROWSER_WIDTH;
	rect.bottom = STD_CONTROL_HEIGHT;

	localRect.left += m_labelWidth + 4;
	localRect.right = FILEBROWSER_WIDTH;
	localRect.bottom = STD_CONTROL_HEIGHT;

	id = pDialog->AddFileBrowser(localRect, (WORD)id, m_fileDescription.c_str(), &m_extension[0]);

	CreateLabel(pDialog, id, localRect);
}

void FileBrowserControlDesc::SetBlank(HWND /*hWnd*/)
{
}

bool FileBrowserControlDesc::IsBlank(HWND /*hWnd*/)
{
	return false;
}