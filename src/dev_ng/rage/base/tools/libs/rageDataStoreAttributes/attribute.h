//
//
//    Filename: attribute.h
//     Creator: Adam Fowler
//     $Author: Adam $
//       $Date: 29/06/99 15:32 $
//   $Revision: 4 $
// Description: 
//
//

#ifndef INC_ATTRIBUTE_H_
#define INC_ATTRIBUTE_H_

// ALupinacci 7/21/08 - Disable warning that no default assignment operator could be generated
#pragma warning( disable: 4512 )

// my headers
#include "attriberror.h"
// STD headers
#include <map>
#include <string>
#include <vector>

// using namespace
using namespace rage;

namespace dmat {
	
	//
	//   Class Name: AttributeValue
	//  Description: Class holding an attribute of undefined type. It holds it inside a union
	//    Functions: AttributeValue(): constructors from int,float,bool and char *. These are all implicit
	//					and a copy constructor
	//				~AttributeValue(): destructor
	//				size(): return memory size of attribute
	//				GetRawDataPtr(): return a pointer to the attribute
	//				SetFromRawDataPtr(): set attribute from a raw data pointer
	//
	//
	class ATTRIBUTE_API AttributeValue
	{
	public:
		enum eAttrType
		{
			eAttrTypeInt,
			eAttrTypeFloat,
			eAttrTypeBool,
			eAttrTypeString,
			eAttrTypeNum
		};
	private:
		typedef union {
			s32 i;
			float f;
			bool b;
			char *pString;
		} valueType;
		
		valueType m_value;
		bool m_pointer;
		eAttrType m_type;
		bool m_blank;

	public:
		AttributeValue() 
			: m_pointer(false)
			, m_blank(false)
		{}
		AttributeValue(s32 value) 
			: m_pointer(false)
			, m_type(eAttrTypeInt) 
			, m_blank(false)
		{
			m_value.i = value;
		}
		AttributeValue(float value) 
			: m_pointer(false)
			, m_type(eAttrTypeFloat)  
			, m_blank(false)
		{
			m_value.f = value;
		}
		AttributeValue(bool value) 
			: m_pointer(false)
			, m_type(eAttrTypeBool)  
			, m_blank(false)
		{
			m_value.b = value;
		}
		AttributeValue(const char *pValue);
		AttributeValue(const AttributeValue &copy);
		~AttributeValue();

		eAttrType GetType() const {return m_type;}

		AttributeValue &operator=(const AttributeValue& copy);
		// conversion operators
		operator s32() {return m_value.i;}
		operator float() {return m_value.f;}
		operator bool() {return m_value.b;}
		operator char*() {return m_value.pString;}
		operator const s32() const {return m_value.i;}
		operator const float() const {return m_value.f;}
		operator const bool() const {return m_value.b;}
		operator const char*() const {return m_value.pString;}

		bool IsBlank(){return m_blank;}
		void SetBlank(bool blank){m_blank = blank;}
		s32 size() const;

		void *GetRawDataPtr();
		void SetFromRawData(void *pData);

		// operators
		friend bool operator <(const dmat::AttributeValue& a, const dmat::AttributeValue& b);
		friend bool operator==(const dmat::AttributeValue& a, const dmat::AttributeValue& b);
	};
	
	// instantiate template class std::vector<AttributeValue>
	template class ATTRIBUTE_API std::vector<dmat::AttributeValue>;

	//
	//   Class Name: Attribute
	//  Description: Class defining an attribute: its type, name and default value
	//    Functions: Attribute(): constructor from int, float, bool and char *
	//				GetName(): returns name of attribute
	//				GetType(): returns type of attribute
	//				GetDefault(): returns default value
	//
	//
	class ATTRIBUTE_API Attribute
	{
	public:
		enum Type {INT, FLOAT, BOOL, STRING};

		Attribute(const std::string &name, s32 value);
		Attribute(const std::string &name, float value);
		Attribute(const std::string &name, bool value);
		Attribute(const std::string &name, const char *pValue);
		Attribute(const std::string &name, AttributeValue value, Type type);

		const std::string &GetName() const;
		Type GetType() const;
		const AttributeValue &GetDefault() const;

	private:
		Type m_type;
		std::string m_name;
		AttributeValue m_default;
	};

	class AttributeClass;

	typedef std::vector<Attribute> AttributeArray;
	typedef std::vector<Attribute>::iterator AttributeArrayIterator;
	typedef std::vector<Attribute>::const_iterator AttributeArrayConstIterator;
	typedef std::vector<AttributeValue> AttributeValueArray;
	typedef std::vector<AttributeValue>::iterator AttributeValueArrayIterator;
	typedef std::vector<AttributeValue>::const_iterator AttributeValueArrayConstIterator;
	typedef std::vector<s32> IntArray;
	typedef std::map<s32, s32> IndexMap;
	typedef std::map<s32, const AttributeClass*> ClassMap;
	typedef IndexMap::iterator IndexMapIterator;
	typedef IndexMap::const_iterator IndexMapConstIterator;
	typedef ClassMap::iterator ClassMapIterator;
	typedef ClassMap::const_iterator ClassMapConstIterator;

	//
	//   Class Name: AttributeClass
	//  Description: Class holding a bunch of attributes which describe a class of object
	//    Functions: AddAttribute(): add an attribute and an identifying id
	//				AddClass(): add the contents of another class to this class
	//				GetIndex(): given an id return the index of the attribute
	//				GetId(): given an index of an attribtue return its id
	//				IsIdUsed(): return if an id is already used
	//				operator[]: return an attribute given an id
	//				CheckIdType(): throw an error if given type is different to the type of attribute with 
	//					given id.
	//				CheckIndexType(): throw an error if given type is different to the type of attribute with
	//					given index.
	//				GetSize(): return number of attributes.
	//				DeleteItem(): delete an item from the attribute class
	//
	//
	class AttributeClass
	{
		friend class AttributeInst;

	public:
		AttributeClass();
		void AddAttribute(const s32 id, const Attribute &attribute, const AttributeClass *pParentClass=NULL);
		void AddClass(const AttributeClass &aClass, std::string name);

		s32 GetIndex(const s32 id) const;
		ATTRIBUTE_API s32 GetId(const s32 index) const;
		s32 GetClassId(const s32 globalIndex) const;
		bool IsIdUsed(const s32 id) const;
		bool IsKeyUsed(const s32 key) const;
		ATTRIBUTE_API const Attribute &operator[](const s32 id) const;
		ATTRIBUTE_API Attribute &operator[](const s32 id);
		ATTRIBUTE_API const Attribute &GetItem(const s32 index) const;
		ATTRIBUTE_API const AttributeClass *GetItemClass(const s32 index) const;
		ATTRIBUTE_API const AttributeClass *GetItemBaseClass(const s32 index) const;
		bool GetBaseClass(AttributeClass*& attr, const int key) const;
		ATTRIBUTE_API Attribute &GetItem(const s32 index);
		void CheckIdType(const s32 id, Attribute::Type type) const;
		void CheckIndexType(const s32 index, Attribute::Type type) const;
		ATTRIBUTE_API s32 GetSize() const;
		void DeleteItem(s32 id);
		void SwapItem(s32 index1, s32 index2);
		void SetName(std::string name){m_name = name;}
		ATTRIBUTE_API std::string GetName() const{return m_name;}
		void SetParent(AttributeClass* pParent);
		AttributeClass* GetParent();

	private:
		AttributeArray m_array;
		IntArray m_keyArray;
		IndexMap m_indexMap;
		ClassMap m_classMap;
		std::string m_name;

		AttributeClass* m_parent;
	};

	//
	//   Class Name: AttributeInst
	//  Description: Instance of the data described in AttributeClass
	//    Functions: AttributeInst(): construct from either a class or another instance
	//				GetClass(): return reference to class describing this instance
	//				GetAttributeValue(): return attribute value with given index
	//				SetAttributeValue(): set the attribute value with given index
	//				GetValue(): return attribute value as either int,float,bool or char *
	//				SetValue() set attribute value with either int,float,bool or char *
	//				CreateRawData(): create an array of data which at a later date can be used to generate
	//					this instance again
	//				ReadRawData(): fill out attribute values from raw data
	//
	//
	class AttributeInstMultiple;

	class ATTRIBUTE_API AttributeInst
	{
	public:
		AttributeInst(const AttributeClass &aClass);
		AttributeInst(const AttributeInst &orig);

		const AttributeClass &GetClass() const {return m_class;}

		AttributeValue GetAttributeValue(s32 index) const;
		void SetAttributeValue(s32 index, AttributeValue value);

		void GetValueForceInt(s32 index, s32 &value) const;

		void GetValue(s32 index, s32 &value) const;
		void GetValue(s32 index, float &value) const;
		void GetValue(s32 index, bool &value) const;
		void GetValue(s32 index, const char *&pValue) const;
		void SetValue(s32 index, const s32 value);
		void SetValue(s32 index, const float value);
		void SetValue(s32 index, const bool value);
		void SetValue(s32 index, const char *pValue);

		char *CreateRawData(s32 &size);
		void ReadRawData(char *pData, s32 num);

		void updateFromMultiple(AttributeInstMultiple* p_aimUpdate);

	protected:
		const AttributeClass &m_class;
		AttributeValueArray m_valueArray;
	};

	//variation of the attirbuteinst used in changing multiple
	//attribute instances in a single dialog
	//basically just allows us to know if a particular attribute
	//is the same across a number of attribute instances

	//class ATTRIBUTE_API AttributeInstMultiple : public AttributeInst
	class AttributeInstMultiple : public AttributeInst
	{
	public:
		friend class AttributeInst;

		ATTRIBUTE_API AttributeInstMultiple(const AttributeClass &aClass);
		ATTRIBUTE_API AttributeInstMultiple(const AttributeInst &orig);

		void SetValueUniversal(s32 index, bool bSame) { m_bUniversal[index] = bSame; }
		bool GetValueUniversal(s32 index) { return m_bUniversal[index]; }

		void clearUni();
		void ATTRIBUTE_API merge(const AttributeInst*);

	private:

		std::vector<bool> m_bUniversal;
	};

}; // namespace dmat


#endif // INC_ATTRIBUTE_H_