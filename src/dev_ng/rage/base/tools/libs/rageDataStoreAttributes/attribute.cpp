//
//
//    Filename: attribute.cpp
//     Creator: Adam Fowler
//     $Author: Adam $
//       $Date: 29/06/99 15:31 $
//   $Revision: 5 $
// Description: 
//
//
#include "attribute.h"
#include "attriberror.h"
//STD headers
#include <cassert>

using namespace dmat;

// --- AttributeValue ---------------------------------------------------------------------------------

//
//        name: AttributeValue::AttributeValue
// description: Constructor from string
//          in: pValue = pointer to string
//
AttributeValue::AttributeValue(const char *pValue) 
: m_pointer(true)
, m_type(eAttrTypeString)
{
	assert(pValue);

	m_value.pString = new char[strlen(pValue) + 1];
	strcpy(m_value.pString, pValue);

	m_blank = (0==strcmp(pValue, ""));
}

//
//        name: AttributeValue::AttributeValue
// description: Copy Constructor
//
AttributeValue::AttributeValue(const AttributeValue& copy) 
: m_pointer(copy.m_pointer)
, m_type(copy.m_type)
, m_blank(copy.m_blank)
{
	if(m_pointer)
	{
		m_value.pString = new char[strlen(copy.m_value.pString) + 1];
		strcpy(m_value.pString, copy.m_value.pString);
	}
	else
		m_value = copy.m_value;
}

//
//        name: AttributeValue::AttributeValue
// description: Destructor
//
AttributeValue::~AttributeValue()
{
	if(m_pointer)
		delete[] m_value.pString;
}

//
//        name: operator=
//
AttributeValue& AttributeValue::operator=(const AttributeValue& copy)
{
	if(m_pointer)
		delete[] m_value.pString;

	m_pointer = copy.m_pointer;
	if(m_pointer)
	{
		m_value.pString = new char[strlen(copy.m_value.pString) + 1];
		strcpy(m_value.pString, copy.m_value.pString);
	}
	else
		m_value = copy.m_value;

	m_blank = copy.m_blank;
	m_type = copy.m_type;

	return *this;
}

s32 AttributeValue::size() const
{
	if(m_pointer)
		return (s32)strlen(m_value.pString) + 1;

	// DHM 18 May 2009
	// 64-bit compatibility fix.  The struct has a char* which on 64-bit is 8-bytes,
	// and 32-bit 4 bytes.  This leads to alignment errors reading/writing the data
	// between platforms.  Lets standardize this as the address value is not
	// saved out.
	return (sizeof(m_value) - sizeof(char*) + 4);
}

//
//        name: *AttributeValue::GetRawDataPtr
// description: 
//          in: 
//         out: 
//
void *AttributeValue::GetRawDataPtr() 
{
	if(m_pointer)
		return m_value.pString;
	return (void *)&m_value;
}
void AttributeValue::SetFromRawData(void *pData) 
{
	if(m_pointer)
	{
		delete[] m_value.pString;
		m_value.pString = new char[strlen((char *)pData) + 1];
		strcpy(m_value.pString, (char *)pData);
	}
	else
		m_value = *(valueType *)pData;
}

// operators
bool dmat::operator <(const dmat::AttributeValue& a, const dmat::AttributeValue& b)
{
	return a.m_value.i < b.m_value.i;
}
bool dmat::operator==(const dmat::AttributeValue& a, const dmat::AttributeValue& b)
{
	if(a.GetType()!=b.GetType())
		return false;
	else if(a.GetType()==dmat::AttributeValue::eAttrTypeString)
		return 0==strcmp(a.m_value.pString, b.m_value.pString);
	return a.m_value.i == b.m_value.i;
}

// --- Attribute --------------------------------------------------------------------------------------

//
//        name: Attribute::Attribute
// description: Constructors
//          in: name = name of attribute
//				value = default value
//
Attribute::Attribute(const std::string &name, s32 value) : 
m_type(INT), m_name(name), m_default(value)
{
}

Attribute::Attribute(const std::string &name, float value) : 
m_type(FLOAT), m_name(name), m_default(value)
{
}

Attribute::Attribute(const std::string &name, bool value) : 
m_type(BOOL), m_name(name), m_default(value)
{
}

Attribute::Attribute(const std::string &name, const char *pValue) : 
m_type(STRING), m_name(name), m_default(pValue)
{
}

Attribute::Attribute(const std::string &name, AttributeValue value, Type type) :
m_type(type), m_name(name), m_default(value)
{
}

//
//        name: &GetName,GetType,GetDefault
// description: Access functions
//
const std::string &Attribute::GetName() const
{
	return m_name;
}

Attribute::Type Attribute::GetType() const
{
	return m_type;
}

const AttributeValue &Attribute::GetDefault() const
{
	return m_default;
}


// --- AttributeClass ---------------------------------------------------------------------------------

//
//        name: AttributeClass
// description: constructor
//
AttributeClass::AttributeClass() : m_parent(NULL)
{

}

//
//        name: AttributeClass::AddAttribute
// description: Add an attribute to the class.
//          in: id = id of attribute
//				attribute = attribute 
//
void AttributeClass::AddAttribute(const s32 id, const Attribute &attribute, const AttributeClass *pParentClass)
{
	s32 index = (s32)m_array.size();

	if(m_indexMap.insert(std::make_pair(id, index)).second)
	{
		m_array.push_back(attribute);
		m_keyArray.push_back(id);
		m_classMap.insert(std::make_pair(index, pParentClass!=NULL?pParentClass:this));
	}
	else 
		throw IdClashError(id);

}

//
//        name: AttributeClass::AddClass
// description: Add all the attributes in another class to this class
//          in: attributeClass = the other class
//
void AttributeClass::AddClass(const AttributeClass &aClass, std::string name)
{
	u32 i;
	IndexMapConstIterator iIndex;
//	m_name = name;

	for(i=0; i<aClass.m_keyArray.size(); i++)
	{
		s32 id = aClass.m_keyArray[i];
		AddAttribute(id, aClass[id], &aClass);
	}
}

//
//        name: AttributeClass::GetIndex
// description: Return the index of an attribute from the id
//          in: id = id of attribute
//         out: index
//
s32 AttributeClass::GetIndex(const s32 id) const
{
	IndexMapConstIterator iIndex = m_indexMap.find(id);

	if(iIndex == m_indexMap.end())
		return -1;

	return (*iIndex).second;
}

//
//        name: AttributeClass::IsIdUsed
// description: Returns if attribute id is used
//          in: id = id of attribute
//         out: 
//
bool AttributeClass::IsIdUsed(const s32 id) const
{
	return (m_indexMap.find(id) != m_indexMap.end());
}

//
//        name: AttributeClass::IsKeyUsed
// description: Returns if attribute key is used
//          in: key = key of attribute
//         out: 
//
bool AttributeClass::IsKeyUsed(const s32 key) const
{
	for(int i=0 ; i < m_keyArray.size(); ++i)
	{
		if(m_keyArray[i] == key)
		{
			return true;
		}
	}

	return false;
}

//
//        name: AttributeClass::GetId
// description: Return the id of an attribute
//          in: id = index of attribute in array
//         out: id
//
s32 AttributeClass::GetId(const s32 index) const
{
	return m_keyArray[index];
}

//
//        name: AttributeClass::GetClassId
// description: Return the id from the attribute class
//          in: id = index of global attribute index
//         out: id
//
s32 AttributeClass::GetClassId(const s32 globalIndex) const
{
	for(int i=0 ; i < m_keyArray.size(); ++i)
	{
		if(m_keyArray[i] == globalIndex)
		{
			return i;
		}
	}

	return -1;
}

//
//        name: AttributeClass::operator[]
// description: Returns the attribute with specified id
//          in: id = attribute id
//         out: attribute
//
const Attribute &AttributeClass::operator[](const s32 id) const
{
	IndexMapConstIterator iIndex = m_indexMap.find(id);

	if(iIndex == m_indexMap.end())
		throw InvalidIdError(id);

	return m_array[(*iIndex).second];
}
Attribute &AttributeClass::operator[](const s32 id)
{
	IndexMapIterator iIndex = m_indexMap.find(id);

	if(iIndex == m_indexMap.end())
		throw InvalidIdError(id);

	return m_array[(*iIndex).second];
}

//
//        name: AttributeClass::GetItem
// description: Returns the attribute
//          in: index = attribute index
//         out: attribute
//
const Attribute &AttributeClass::GetItem(const s32 index) const
{
	return m_array[index];
}
Attribute &AttributeClass::GetItem(const s32 index)
{
	return m_array[index];
}

//
//        name: AttributeClass::GetItemClass
// description: Returns the originally owning attribute class
//          in: index = attribute index
//         out: AttributeClass POINTER
//
const AttributeClass *AttributeClass::GetItemClass(const s32 index) const
{
	ClassMapConstIterator iIndex = m_classMap.find(index);

	if(iIndex == m_classMap.end())
		return NULL;

	return (*iIndex).second;
}

//
//        name: AttributeClass::GetBaseClass
// description: Retrieves the direct parent class
//          in: pClass = pointer to parent class
//			in: key = key of attribute
//         out: bool
//
bool AttributeClass::GetBaseClass(AttributeClass*& pClass, const int key) const
{
	if(IsKeyUsed(key))
	{
		for(ClassMapConstIterator iterator = m_classMap.begin(); iterator != m_classMap.end(); iterator++) 
		{
			if(pClass == iterator->second) return false;

			if(iterator->second->IsKeyUsed(key))
			{
				pClass = (AttributeClass*)iterator->second;
				return iterator->second->GetBaseClass(pClass, key);
			}
		}

		return true;
	}

	return false;
}

//
//        name: AttributeClass::GetItemBaseClass
// description: Returns the direct parent class
//          in: index = attribute index
//         out: AttributeClass POINTER
//
const AttributeClass *AttributeClass::GetItemBaseClass(const s32 index) const
{
	s32 key = GetId(index);

	AttributeClass* pClass = (AttributeClass*)GetItemClass(index);
	pClass->GetBaseClass(pClass, key);

	return pClass;
}

//
//        name: AttributeClass::CheckIdType, CheckIndexType
// description: Checks if attribute is corrcet type and throws error if not
//          in: id = id of attribute
//				index = index of attribute in array
//				type = type it should be
//
void AttributeClass::CheckIdType(const s32 id, Attribute::Type type) const
{
	const Attribute &attribute = operator[](id);

	if(attribute.GetType() != type)
		throw InvalidTypeError(id);
}

void AttributeClass::CheckIndexType(const s32 index, Attribute::Type type) const
{
	if(m_array[index].GetType() != type)
		throw InvalidTypeError(GetId(index));
}

//
//        name: AttributeClass::DeleteItem
// description: Delete an attribute from the class
//          in: id = id of class
//
void AttributeClass::DeleteItem(s32 id)
{
	IndexMapIterator iIndex = m_indexMap.find(id);

	if(iIndex == m_indexMap.end())
		return;
	s32 index = (*iIndex).second;
	
	m_indexMap.erase(iIndex);
	m_keyArray.erase(m_keyArray.begin() + index);
	m_array.erase(m_array.begin() + index);

	// decrement any indices above the index being deleted
	for(iIndex = m_indexMap.begin(); iIndex != m_indexMap.end(); iIndex++)
	{
		if((*iIndex).second > index)
			(*iIndex).second--;
	}
}

//
//        name: AttributeClass::SwapItem
// description: Swap two item positions in the attribute array. The two items will still have the
//				same ids but will be at different positions in the array
//          in: index1,index2 = indices of two items
//
void AttributeClass::SwapItem(s32 index1, s32 index2)
{
	Attribute attr = m_array[index1];
	s32 key1 = m_keyArray[index1];
	s32 key2 = m_keyArray[index2];
	IndexMapIterator iIndex1 = m_indexMap.find(key1);
	IndexMapIterator iIndex2 = m_indexMap.find(key2);

	assert(iIndex1 != m_indexMap.end());
	assert(iIndex2 != m_indexMap.end());

	m_array[index1] = m_array[index2];
	m_array[index2] = attr;

	m_keyArray[index1] = key2;
	m_keyArray[index2] = key1;

	(*iIndex1).second = index2;
	(*iIndex2).second = index1;
}

s32 AttributeClass::GetSize() const 
{
	return (s32)m_array.size();
}

void AttributeClass::SetParent(AttributeClass* pParent)
{
	m_parent = pParent;
}

AttributeClass* AttributeClass::GetParent()
{
	return m_parent;
}

// --- AttributeInstance ------------------------------------------------------------------------------

//
//        name: AttributeInst::AttributeInst
// description: Constructor
//          in: aClass = Attribute class to construct this instance from
//         out: 
//
AttributeInst::AttributeInst(const AttributeClass &aClass) : m_class(aClass)
{
	AttributeArrayConstIterator iAttribute;

	// reserve space for array
	m_valueArray.reserve(aClass.m_array.size());

	for(iAttribute = aClass.m_array.begin(); iAttribute != aClass.m_array.end(); iAttribute++)
	{
		m_valueArray.push_back((*iAttribute).GetDefault());
	}
}

//
//        name: AttributeInst::AttributeInst
// description: Copy constructor
//
AttributeInst::AttributeInst(const AttributeInst &orig) : m_class(orig.m_class), 
m_valueArray(orig.m_valueArray)
{
}

//
//        name: AttributeInst::GetAttributeValue, SetAttributeValue
// description: get and set functions which take an index into the attribute array and an 
//				attribute value class
//          in: index = index into array of attribute
//				value = value either to get or set
//
AttributeValue AttributeInst::GetAttributeValue(s32 index) const
{
	return m_valueArray[index];
}
void AttributeInst::SetAttributeValue(s32 index, AttributeValue value)
{
	m_valueArray[index] = value;
}

//
//        name: AttributeInst::GetValue,SetValue
// description: get and set value functions which take an index into the attribute array
//				NB an index not an id
//          in: index = index into array of attribute
//				value = value either to get or set
//
void AttributeInst::GetValueForceInt(s32 index, s32 &value) const
{
	value = m_valueArray[index];
}

void AttributeInst::GetValue(s32 index, s32 &value) const
{
	m_class.CheckIndexType(index, Attribute::INT);
	value = m_valueArray[index];
}
void AttributeInst::GetValue(s32 index, float &value) const
{
	m_class.CheckIndexType(index, Attribute::FLOAT);
	value = m_valueArray[index];
}
void AttributeInst::GetValue(s32 index, bool &value) const
{
	m_class.CheckIndexType(index, Attribute::BOOL);
	value = m_valueArray[index];
}
void AttributeInst::GetValue(s32 index, const char *&pValue) const
{
	m_class.CheckIndexType(index, Attribute::STRING);
	pValue = m_valueArray[index];
}
void AttributeInst::SetValue(s32 index, const s32 value)
{
	m_class.CheckIndexType(index, Attribute::INT);
	m_valueArray[index] = value;
}
void AttributeInst::SetValue(s32 index, const float value)
{
	m_class.CheckIndexType(index, Attribute::FLOAT);
	m_valueArray[index] = value;
}
void AttributeInst::SetValue(s32 index, const bool value)
{
	m_class.CheckIndexType(index, Attribute::BOOL);
	m_valueArray[index] = value;
}
void AttributeInst::SetValue(s32 index, const char *pValue)
{
	m_class.CheckIndexType(index, Attribute::STRING);
	m_valueArray[index] = pValue;
}


//
//        name: *AttributeInst::CreateRawData
// description: Construct a raw block of data from the attributes
//          in: reference to size integer to fill
//         out: pointer to raw data
//
char *AttributeInst::CreateRawData(s32 &size)
{
	s32 i;

	// work out size of raw data array before allocating it
	size = 0;
	for(i=0; i != int(m_valueArray.size()); i++)
	{
		size += 4;							// space for ID
		size += m_valueArray[i].size();		// space for data
		if(size & 0x3)
			size += 4 - (size&0x3);
	}

	char *pData = new char[size];
	char *pData2 = pData;
	s32 valueSize, id;

	for(i = 0; i != int(m_valueArray.size()); i++)
	{
		// copy ID
		id = m_class.GetId(i);
		*(s32 *)pData2 = id;
		pData2 += 4;

		// ignore invalid attributes 
		if(id == -1)
			continue;

		// copy data
		valueSize = m_valueArray[i].size();
		memcpy(pData2, m_valueArray[i].GetRawDataPtr(), valueSize);
		pData2 += valueSize;
		// align to four bytes
		if(valueSize & 0x3)
			pData2 += 4 - (valueSize&0x3);
	}
	return pData;
}

//
//        name: AttributeInst::ReadRawData
// description: Read in values from RAW data
//          in: pData = pointer to data
//				size = size of data
//         out: 
//
void AttributeInst::ReadRawData(char *pData, s32 size)
{
	char *pData2 = pData;
	char *pDataEnd = pData + size;
	s32 valueSize;
	s32 index;

	while(pData2 < pDataEnd)
	{
		// get ID
		index = m_class.GetIndex(*(s32 *)pData2);
		pData2 += 4;

		if(index != -1)
		{
			m_valueArray[index].SetFromRawData(pData2);
			valueSize = m_valueArray[index].size();

			pData2 += valueSize;
			// align to four bytes
			if(valueSize & 0x3)
				pData2 += 4 - (valueSize&0x3);
		}
	}
}

/////////////////////////////////////////////////////////////////////////////////
void AttributeInst::updateFromMultiple(AttributeInstMultiple* p_aimUpdate)
{
	//go through each value in the multiple instance
	//and if it is a universal value, update this object

	if(!p_aimUpdate)
	{
		return;
	}

	int iSize = m_class.GetSize();

	for(int i=0;i<iSize;i++)
	{
		if(p_aimUpdate->m_bUniversal[i])
		{
			m_valueArray[i] = p_aimUpdate->m_valueArray[i];
		}
	}
}

/////////////////////////////////////////////////////////////////////////////////
//AttributeInstMultiple
/////////////////////////////////////////////////////////////////////////////////

void AttributeInstMultiple::clearUni()
{
	//go through the m_bUniversal array setting every value to true
	//for the number of values in the class

	int iSize = m_class.GetSize();

	for(int i=0;i<iSize;i++)
	{
		m_bUniversal[i] = true;
	}
}

/////////////////////////////////////////////////////////////////////////////////
AttributeInstMultiple::AttributeInstMultiple(const AttributeClass &aClass):
	AttributeInst(aClass)
{
	//go through the m_bUniversal array setting every value to true
	//for the number of values in the class

	int iSize = m_class.GetSize();

//	if(m_bUniversal.size() == 0)
	{
		for(int i=0;i<iSize;i++)
		{
			m_bUniversal.push_back(true);
		}
	}
}

/////////////////////////////////////////////////////////////////////////////////
AttributeInstMultiple::AttributeInstMultiple(const AttributeInst &orig):
	AttributeInst(orig)
{
	//go through the m_bUniversal array setting every value to true
	//for the number of values in the class

	int iSize = m_class.GetSize();

	if(m_bUniversal.size() == 0)
	{
		for(int i=0;i<iSize;i++)
		{
			m_bUniversal.push_back(true);
		}
	}
}

/////////////////////////////////////////////////////////////////////////////////
//goes through each of the values in the passed in instance and if any are different
//to the current values, set the universal value to false
/////////////////////////////////////////////////////////////////////////////////
void AttributeInstMultiple::merge(const AttributeInst* p_attiCheck)
{
	s32 iA,iB;
	float fA,fB;
	const char* p_cA,* p_cB;
	bool bA,bB;
	int iSize = p_attiCheck->GetClass().GetSize();

	//these should be the same class so the size should
	//be equal
//	_ASSERT(iSize == m_class.GetSize());
	for(int i=0;i<iSize;i++)
	{
		dmat::Attribute::Type type = p_attiCheck->GetClass().GetItem(i).GetType();

		switch(type)
		{
		case dmat::Attribute::INT:

			GetValue(i,iA);
			p_attiCheck->GetValue(i,iB);

			if(iA != iB)
			{
				m_bUniversal[i] = false;
			}

			break;

		case dmat::Attribute::FLOAT:

			GetValue(i,fA);
			p_attiCheck->GetValue(i,fB);

			if(fA != fB)
			{
				m_bUniversal[i] = false;
			}

			break;

		case dmat::Attribute::BOOL:

			GetValue(i,bA);
			p_attiCheck->GetValue(i,bB);

			if(bA != bB)
			{
				m_bUniversal[i] = false;
			}

			break;

		case dmat::Attribute::STRING:

			GetValue(i,p_cA);
			p_attiCheck->GetValue(i,p_cB);

			if(strcmp(p_cA,p_cB) != 0)
			{
				m_bUniversal[i] = false;
			}

			break;
		}
	}
}