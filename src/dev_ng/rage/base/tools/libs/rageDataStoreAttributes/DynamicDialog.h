//
//
//    Filename: DynamicDialog.h
//     Creator: Adam Fowler
//     $Author: Adam $
//       $Date: 30/06/99 15:08 $
//   $Revision: 3 $
// Description: Class used to create a dialog and set it up properly
//
//
#ifndef INC_DYNAMIC_DIALOG
#define INC_DYNAMIC_DIALOG

// windows headers
#pragma warning(push)
#pragma warning(disable:4668)
#include <commctrl.h>
#pragma warning(pop)
// STD headers
#include <deque>
#include <queue>
#include <string>
// My headers
#include "DialogTemplate.h"

#pragma warning (disable : 4786)

// dodgy guess at updown control messages
#ifndef UDM_GETPOS32
#define UDM_GETPOS32 (UDM_GETRANGE32 + 2)
#endif
#ifndef UDM_SETPOS32
#define UDM_SETPOS32 (UDM_SETRANGE32 + 2)
#endif

//
//   Class Name: DynamicDialog
// Base Classes: DialogTemplate: class storing a dialog template for indirect construction of dialogs
//  Description: Extension of DialogTemplate which stores SendMessages and SetWindowLongs which need to 
//				be called when WM_INITDIALOG is sent
//    Functions: DynamicDialog(): constructor
//				QueueSendMessage(): add a message to be sent at dialog initialisation
//				QueueSetWindowLong(): add a SetWindowLong to be called at dialog initialisation
//				AddFloatSpinner(): add a spinner (edit box and updown control) that edits a floating point
//					value.
//				AddIntSpinner(): add a spinner (edit box and updown control) that edits an integer
//				AddComboBox(): add a combo box with all the entries in the combo box added already
//				InitDialog(): This should be called when WM_INITDIALOG is sent to the dialog. It calls 
//					all the SendMessage()'s and SetWindowLong()'s.
//
//
class DynamicDialog : public DialogTemplate
{
public:
	DynamicDialog(HINSTANCE hInstance=NULL);
	~DynamicDialog();
	
	void QueueSendMessage(s32 id, UINT msg, WPARAM wParam, LPARAM lParam, LRESULT *pResult = NULL);
	void QueueSetWindowLong(s32 id, s32 index, DWORDLONG value, LONG_PTR *pResult = NULL);
	
	// add controls
	s32 AddFloatSpinner(const RECT &inRect, WORD id, float min, float max, s32 decimalPlaces);
	s32 AddIntSpinner(const RECT &inRect, WORD id, s32 min, s32 max);
	s32 AddComboBox(const RECT &inRect, WORD id, const char *pItemList);
	s32 AddFileBrowser(const RECT &inRect, WORD id, const char *pFilterString, const char *pFilter);

	void InitDialog(HWND hDlgWnd);

protected:
	void RegisterFileBrowseButton();
	void ProcessSendMessages(HWND hDlgWnd);
	void ProcessSetWindowLongs(HWND hDlgWnd);

private:
	class MessageClass 
	{
	public:
		MessageClass(s32 id, UINT msg, WPARAM wParam, LPARAM lParam, LRESULT *pResult) :
		  m_id(id), m_msg(msg), m_wParam(wParam), m_lParam(lParam), m_pResult(pResult) {}

		s32 m_id;
		UINT m_msg;
		WPARAM m_wParam;
		LPARAM m_lParam;
		LRESULT *m_pResult;
	};

	class SetWindowLongClass
	{
	public:
		SetWindowLongClass(s32 id, s32 index, DWORDLONG value, LRESULT *pResult) :
		  m_id(id), m_index(index), m_value(value), m_pResult(pResult) {}

		s32 m_id;
		s32 m_index;
		DWORDLONG m_value;
		LRESULT *m_pResult;
	};

	static BOOL m_fbButtonRegistered;
	static s32 m_stdButtonExtraData;
	static WNDPROC m_floatProc;
	static WNDPROC m_intProc;
	static WNDPROC m_updownProc;
	static WNDPROC m_fileBrowseProc;
	std::queue<MessageClass, std::deque<MessageClass> > m_messageQ;
	std::queue<SetWindowLongClass, std::deque<SetWindowLongClass> > m_setWindowLongQ;
	//std::vector<std::string> m_initStrings;

	std::vector<char*> m_initStrings;

	HINSTANCE m_hInstance;

	static LRESULT CALLBACK EditIntProc( HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam );
	static LRESULT CALLBACK EditFloatProc( HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam );
	static LRESULT CALLBACK UpDownMouseMoveProc( HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam );
	static LRESULT CALLBACK EditFileBrowserProc( HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam );
};


//
//   Class Name: DialogError
// Base Classes: 
//  Description: Exception handler for constructing dialogs
//    Functions: 
//
//
class DialogError
{
public:
	DialogError(const std::string &error) : m_error(error) {}
	
	const std::string &GetError() {return m_error;}
	
private:
	std::string m_error;
};

#endif