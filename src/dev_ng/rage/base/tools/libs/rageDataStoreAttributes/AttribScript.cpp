//
//
//    Filename: AttribScript.cpp
//     Creator: Adam Fowler
//     $Author: Adam $
//       $Date: 29/06/99 17:41 $
//   $Revision: 4 $
// Description: Class used to load attributes from a script file
//
//

// my headers
#include "AttribScript.h"
#include "script.h"
// STD headers
#include <cassert>

using namespace dmat;

//
//        name: AttributeScriptReader::AttributeScriptReader
// description: Constructor
//          in: pFilename = name of file
//
AttributeScriptFile::AttributeScriptFile(const char *pFilename) : 
std::basic_ifstream<char>(pFilename), m_pManager(NULL)
{
}

//
//        name: AttributeScriptFile::ConstructNewLineList
// description: Construct a list of the indices for all the new lines
//
void AttributeScriptFile::ConstructNewLineList()
{
	char c;

	seekg(0, std::ios::beg);

	m_newLines.clear();

	while(!eof())
	{
		c = (char)get();
		if(c == '\n')
			m_newLines.push_back(tellg());
	}

	clear();
	seekg(0, std::ios::beg);
}

//
//        name: AttributeScriptFile::GetLineNumber
// description: Return the current line number
//         out: line number
//
s32 AttributeScriptFile::GetLineNumber()
{
	std::vector<std::streampos>::iterator iLine;
	std::streampos pos = tellg();
	s32 eofstate = eof();
	s32 count = 0;
	
	for(iLine = m_newLines.begin(); iLine != m_newLines.end(); iLine++)
	{
		if(pos < *iLine && !eofstate)
			break;
		count++;
	}
	return count;
}

//
//        name: AttributeScriptFile::ReadClass
// description: Parse script to load in one attribute class
//
void AttributeScriptFile::ReadClass()
{
	assert(m_pManager);

	AttributeClass *attributeClass = new AttributeClass();
	/*const*/ AttributeClass *pInheritedClass;
	std::string className;
	std::string line;
	std::string typeName;
	std::string attrName;
	std::string defaultString;
	Attribute::Type type;
	s32 id;
	s32 intValue;
	float floatValue;
	bool boolValue;

	// get name
	if(!GetValidString(*this, className))
		throw AttributeScriptError("Error reading class name", GetLineNumber());

	attributeClass->SetName(className);

	while(!eof())
	{
		if(!GetValidLine(*this, line))
			break;
		
		std::istringstream is(line);

		if(!(is >> typeName))
			throw AttributeScriptError("Error reading type", GetLineNumber());
		// check type 
		if(typeName == INT_TAG)
			type = Attribute::INT;
		else if(typeName == FLOAT_TAG)
			type = Attribute::FLOAT;
		else if(typeName == BOOL_TAG)
			type = Attribute::BOOL;
		else if(typeName == STRING_TAG)
			type = Attribute::STRING;
		// if type is inherited class then add inherited class to class
		else if(typeName == INHERIT_TAG)
		{
			if(!GetValidString(is, typeName))
				throw AttributeScriptError("Error reading inherited class", GetLineNumber());
			pInheritedClass = m_pManager->GetClass(typeName);
			if(pInheritedClass == NULL)
				throw AttributeScriptError("Unrecognised inherited class : " + typeName, GetLineNumber());
			attributeClass->AddClass(*pInheritedClass, typeName);
			pInheritedClass->SetParent(attributeClass);

			continue;
		}
		// if type is the endclass tag then finish, by adding this class to the manager and exiting
		else if(typeName == ENDCLASS_TAG)
		{
			m_pManager->AddClass(className, *attributeClass);
			return;
		}
		else
			throw AttributeScriptError("Invalid type name : " + typeName, GetLineNumber());

		// read ID
		if(!(is >> id))
			throw AttributeScriptError("Error reading ID", GetLineNumber());
		// read attribute name
		if(!GetValidString(is, attrName))
			throw AttributeScriptError("Error reading attribute name", GetLineNumber());
		// read default value and add to attribute class
		switch(type)
		{
		case Attribute::INT:
			if(!(is >> intValue))
				throw AttributeScriptError("Error reading default value", GetLineNumber());
			attributeClass->AddAttribute(id, Attribute(attrName, intValue));
			break;
		case Attribute::FLOAT:
			if(!(is >> floatValue))
				throw AttributeScriptError("Error reading default value", GetLineNumber());
			attributeClass->AddAttribute(id, Attribute(attrName, floatValue));
			break;
		case Attribute::BOOL:
			if(!(is >> boolValue))
				throw AttributeScriptError("Error reading default value", GetLineNumber());
			attributeClass->AddAttribute(id, Attribute(attrName, boolValue));
			break;
		case Attribute::STRING:
			if(!GetValidString(is, defaultString))
				throw AttributeScriptError("Error reading default value", GetLineNumber());
			attributeClass->AddAttribute(id, Attribute(attrName, defaultString.c_str()));
			break;
		default:
			assert(0);
		}
	}

	// did not find an end class indentifier
	throw AttributeScriptError("Reached end of file before class definition finished", GetLineNumber());
}

//
//        name: AttributeScriptFile::ReadDialog
// description: Parse script to load in one attribute dialog
//
void AttributeScriptFile::ReadDialog()
{
	assert(m_pManager);

	AttributeDialogDesc attributeDialog;
	const AttributeDialogDesc *pDialogDesc;
	AttributeControlDesc *pControlDesc;
	std::string dialogName;
	std::string typeName;
	std::string line;
	s32 id;

	// get name
	if(!GetValidString(*this, dialogName))
		throw AttributeScriptError("Error reading dialog name", GetLineNumber());

	while(!eof())
	{
		if(!GetValidLine(*this, line))
			break;
		
		std::istringstream is(line);

		if(!(is >> typeName))
			throw AttributeScriptError("Error reading control type", GetLineNumber());
		
		// if end of dialog description
		if(typeName == ENDDIALOG_TAG)
		{
			m_pManager->AddDialogDesc(dialogName, attributeDialog);
			return;
		}
		else if(typeName == GROUPBOX_TAG)
		{
			std::string groupName;
			if(!GetValidString(is, typeName))
				throw AttributeScriptError("Error reading groupbox UI class", GetLineNumber());
			if(!GetValidString(is, groupName))
				throw AttributeScriptError("Error reading groupbox UI name", GetLineNumber());
			pDialogDesc = m_pManager->GetDialogDesc(typeName);
			if(pDialogDesc == NULL)
				throw AttributeScriptError("Unrecognised UI name : " + typeName, GetLineNumber());
			pControlDesc = new GroupBoxControlDesc;
			((GroupBoxControlDesc *)pControlDesc)->SetAttributeDialogDesc(pDialogDesc);
			pControlDesc->SetName(groupName);
		}
		else
		{
			// read ID
			if(!(is >> id))
				throw AttributeScriptError("Error reading ID", GetLineNumber());
			
			// check type 
			if(typeName == INTSPIN_TAG)
			{
				s32 min,max;
				if(!(is >> min >> max))
					throw AttributeScriptError("Error reading spinner limits", GetLineNumber());
				pControlDesc = new IntSpinnerControlDesc;
				((IntSpinnerControlDesc *)pControlDesc)->SetLimits(min, max);
			}
			else if(typeName == FLOATSPIN_TAG)
			{
				float min,max;
				if(!(is >> min >> max))
					throw AttributeScriptError("Error reading spinner limits", GetLineNumber());
				pControlDesc = new FloatSpinnerControlDesc;
				((FloatSpinnerControlDesc *)pControlDesc)->SetLimits(min, max);
			}
			else if(typeName == TCOMBOBOX_TAG)
			{
				std::string entries;
				if(!GetValidString(is, entries))
					throw AttributeScriptError("Error reading tcombobox list entries", GetLineNumber());
				pControlDesc = new StringComboBoxControlDesc;
				((ComboBoxControlDesc *)pControlDesc)->SetEntriesString(entries);

			}
			else if(typeName == COMBOBOX_TAG)
			{
				std::string entries;
				if(!GetValidString(is, entries))
					throw AttributeScriptError("Error reading combobox list entries", GetLineNumber());
				pControlDesc = new ComboBoxControlDesc;
				((ComboBoxControlDesc *)pControlDesc)->SetEntriesString(entries);
			}
			else if(typeName == CHECKBOX_TAG)
			{
				pControlDesc = new CheckBoxControlDesc;
			}
			else if(typeName == EDITBOX_TAG)
			{
				pControlDesc = new EditBoxControlDesc;
			}
			else if(typeName == FILEBROWSER_TAG)
			{
				std::string desc, filter;
				if(!GetValidString(is, desc))
					throw AttributeScriptError("Error reading file browser filter description", GetLineNumber());
				if(!GetValidString(is, filter))
					throw AttributeScriptError("Error reading file browser filter", GetLineNumber());
				
				pControlDesc = new FileBrowserControlDesc;
				((FileBrowserControlDesc *)pControlDesc)->SetFileDescription(desc);
				((FileBrowserControlDesc *)pControlDesc)->SetExtension(filter.c_str());
			}
			else
				throw AttributeScriptError("Invalid control type : " + typeName, GetLineNumber());

			((AttributeValueControlDesc *)pControlDesc)->SetAttributeId(id);
		}

		attributeDialog.AddControl(pControlDesc);
		delete pControlDesc;
	}
		
	// did not find an end dialog indentifier
	throw AttributeScriptError("Reached end of file before dialog definition finished", GetLineNumber());
}

//
//        name: AttributeScriptReader::Read
// description: Read a script file and copy data into attribute manager
//          in: manager = reference to manager
//
void AttributeScriptFile::Read(AttributeManager &manager)
{
	if(fail())
	{
		throw AttributeFileError("Script file does not exist");
	}

	m_pManager = &manager;
	ConstructNewLineList();

	std::string token;
	bool valid = false;

	try
	{
		// read in classes
		while(!eof())
		{
			if(!GetValidString(*this, token))
				break;

			if(token == STARTCLASS_TAG)
			{
				ReadClass();
				valid = true;
			}
			else if(token == STARTDIALOG_TAG)
			{
				ReadDialog();
				valid = true;
			}
		}

	}
	catch(AttributeClassError err)
	{
		throw AttributeScriptError(err, GetLineNumber());
	}
	catch(AttributeIdError err)
	{
		throw AttributeScriptError(err, GetLineNumber());
	}

	if(!valid)
	{
		throw AttributeFileError("Invalid script file");
	}
}
