//
//
//    Filename: AttribScript.h
//     Creator: Adam Fowler
//     $Author: Adam $
//       $Date: 8/06/99 17:04 $
//   $Revision: 3 $
// Description: Class for reading attributes from a script file
//
//
#ifndef INC_ATTRIB_SCRIPT_H_
#define INC_ATTRIB_SCRIPT_H_

// my headers
#include "AttribMgr.h"
// STD headers
#include <fstream>


// file tags
#define STARTCLASS_TAG	"startclass"
#define ENDCLASS_TAG	"end"
#define INHERIT_TAG		"include"
#define STARTDIALOG_TAG	"startui"
#define ENDDIALOG_TAG	"end"
// types
#define INT_TAG			"int"
#define FLOAT_TAG		"float"
#define BOOL_TAG		"bool"
#define STRING_TAG		"string"
// dialog controls
#define INTSPIN_TAG		"intspin"
#define FLOATSPIN_TAG	"floatspin"
#define COMBOBOX_TAG	"list"
#define TCOMBOBOX_TAG	"tlist"
#define CHECKBOX_TAG	"checkbox"
#define GROUPBOX_TAG	"group"
#define EDITBOX_TAG		"editbox"
#define FILEBROWSER_TAG	"filebrowser"

namespace dmat {
	
	//
	//   Class Name: AttributeScriptFile
	// Base Classes: 
	//  Description: 
	//    Functions: 
	//
	//
	class AttributeScriptFile : private std::basic_ifstream<char>
	{
	public:
		AttributeScriptFile(const char *pFilename);
		
		void Read(AttributeManager &manager);
		
	private:
		s32 GetLineNumber();
		void ConstructNewLineList();
		void ReadClass();
		void ReadDialog();
		
		AttributeManager *m_pManager;
		std::vector<std::streampos> m_newLines;
	};
	
	
};  // namespace dmat


#endif // INC_ATTRIB_SCRIPT_H_

