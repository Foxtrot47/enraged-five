// DialogTemplate.h: interface for the CDialogTemplate class.
//
// This class deals with making a dialog at runtime.
//
//////////////////////////////////////////////////////////////////////

#ifndef INC_DIALOG_TEMPLATE_H_
#define INC_DIALOG_TEMPLATE_H_

// Microsoft headers
#ifndef STRICT
#define STRICT
#endif
#pragma warning(push)
#pragma warning(disable:4668)
#include <windows.h>
#pragma warning(pop)

// using namespace
using namespace rage;

// Defines for the basic windows controls.
// These are nasty hardcoded values, straight from the help files.
// Windows uses these to identify controls that it deals with directly.
#define BUTTON_CTRL		(0x0080)
#define EDIT_CTRL		(0x0081)
#define STATIC_CTRL		(0x0082)
#define LISTBOX_CTRL	(0x0083)
#define SCROLLBAR_CTRL	(0x0084)
#define COMBOBOX_CTRL	(0x0085)

//////////////////////////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////////////////////////

//
//   Class Name: DialogTemplate
// Base Classes: 
//  Description: Class used to create dialogs without a resource template
//    Functions: DialogTemplate(): constructor
//				~DialogTemplate(): destructor
//				Init(): start creating dialog template with given information
//				AddCtrl(): see comment below
//				AddButtonCtrl(): add a button control
//				AddEditCtrl(): add an edit box control
//				AddStaticCtrl(): add a static control
//				AddListBoxCtrl(): add a list box control
//				AddScrollBarCtrl(): add a scroll bar control
//				AddComboBoxCtrl(): add a combo box control
//				AddGroupBoxCtrl(): add a group box control
//				AddCheckBoxCtrl(): add a check box control
//				AddRadioButtonCtrl(): add a radio button control
//				SetDialogSize(): set the size of the dialog after calling Init()
//				GetTemplate(): return the pointer to the template
//
//
class DialogTemplate  
{
public:
	DialogTemplate();
	virtual ~DialogTemplate();

	BOOL Init(DWORD style, const RECT &inRect, LPCSTR title, LPCSTR font=NULL, s16 fontSize = 10); //Creates a blank dialog template with no controls.

	//Call these functions to add controls to the dialog template.
	//Note that the RECT is for the position and size of the control.
	//The RECT contains coords for the top left and width and height of the control.
	//
	// There is two versions of AddCtrl: one which takes a dialog control class id (See the #defines
	// above) and one which takes a window class name. Below is a list of useful class names
	// UPDOWN_CLASS : spin control
	// TRACKBAR_CLASS : slider
	// PROGRESS_CLASS : progress bar control
	// HOTKEY_CLASS : hotkey control
	// WC_TREEVIEW : tree control
	// WC_TABCONTROL : tab control
	// ANIMATE_CLASS : Animate control
	// WC_LISTVIEW : list control
	// "RICHEDIT" : Rich edit control
	BOOL AddCtrl(WORD wndClass, DWORD style, const RECT &inRect, WORD id, LPCSTR text = "");
	BOOL AddCtrl(LPCSTR wndClass, DWORD style, const RECT &inRect, WORD id, LPCSTR text = "");
	BOOL AddButtonCtrl(DWORD style, const RECT &inRect, WORD id, LPCSTR text = "");
	BOOL AddEditCtrl(DWORD style, const RECT &inRect, WORD id, LPCSTR text = "");
	BOOL AddStaticCtrl(DWORD style, const RECT &inRect, WORD id, LPCSTR text = "");
	BOOL AddListBoxCtrl(DWORD style, const RECT &inRect, WORD id, LPCSTR text = "");
	BOOL AddScrollBarCtrl(DWORD style, const RECT &inRect, WORD id, LPCSTR text = "");
	BOOL AddComboBoxCtrl(DWORD style, const RECT &inRect, WORD id, LPCSTR text = "");
	BOOL AddGroupBoxCtrl(DWORD style, const RECT &inRect, WORD id, LPCSTR text = "");
	BOOL AddCheckBoxCtrl(DWORD style, const RECT &inRect, WORD id, LPCSTR text = "",bool bMultipleEdit = false);
	BOOL AddRadioButtonCtrl(DWORD style, const RECT &inRect, WORD id, LPCSTR text = "");

	void SetDialogSize(RECT &rect);

	WORD *GetTemplate();

protected:

	WORD *m_pDlgTemplate, *m_pNumItems, *m_pTemplatePos;

	s32 m_maxTemplateSize;
		
	LPWORD lpwAlign(LPWORD lpIn);
	int CopyAnsiToWideChar(LPWORD lpWCStr, LPCSTR lpAnsiIn);
	void ReallocateTemplateBuffer();
	void SetDialogItemCounterPtr();


};

#endif // INC_DIALOG_TEMPLATE_H_
