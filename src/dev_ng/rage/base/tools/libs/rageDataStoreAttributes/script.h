//
//
//    Filename: script.h
//     Creator: Adam Fowler
//     $Author: Adam $
//       $Date: 29/06/99 17:02 $
//   $Revision: 1 $
// Description: Read either tokens or lines from a file
//
//

#ifndef INC_SCRIPT_H_
#define INC_SCRIPT_H_

//
//        name: GetValidString
// description: Returns a string from an input stream. Unlike the normal operator>> this checks
//				for speech marks and allows spaces if it finds them. Also checks for # comments
//          in: is = input stream
//				str = string
//         out: 
//
template<class T>
std::istream &GetValidString(std::istream &is, std::basic_string<T> &str)
{
	T c;

	// while we keep meeting lines beginning with # signs ignore them
	do
	{
		is >> std::ws;
		if(is.peek() == '#')
		{
			std::getline(is, str, '\n');
			is >> std::ws;
		}
	} while(is.peek() == '#' && !is.eof());

	if(is.eof())
	{
		is.setstate(std::ios::failbit);
		return is;
	}

	// if next character is a speech mark then get the contents of the speech marks
	if(is.peek() == '\"')
	{
		is.get(c);
		return std::getline(is, str, '\"');
	}
	else 
		return is >> str;
}

//
//        name: GetValidLine
// description: Returns a string from an input stream. Unlike the normal operator>> this checks
//				for speech marks and allows spaces if it finds them. Also checks for # comments
//          in: is = input stream
//				str = string
//         out: 
//
template<class T>
std::istream &GetValidLine(std::istream &is, std::basic_string<T> &str)
{
	// while we keep meeting lines beginning with # signs ignore them
	do
	{
		is >> std::ws;
		if(is.peek() == '#')
		{
			std::getline(is, str, '\n');
			is >> std::ws;
		}
	} while(is.peek() == '#' && !is.eof());

	if(is.eof())
	{
		is.setstate(std::ios::failbit);
		return is;
	}

	return std::getline(is, str, '\n');
}


#endif // INC_SCRIPT_H_