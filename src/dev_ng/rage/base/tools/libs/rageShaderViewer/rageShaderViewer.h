// 
// rageShaderViewer/rageShaderViewer.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#pragma warning(disable: 4668)
 #include <process.h>
 #include <windows.h>
#pragma warning(error: 4668)

#ifndef RAGE_SHADER_VIEWER_H
#define RAGE_SHADER_VIEWER_H


#include "vector/vector3.h"
#include "atl/string.h"
#include "grcore/light.h"

namespace	rage
{

class	grmSetup;
class   lightDataManager;
class	dcamCamMgr;
class	grcViewport;
class	bkBank;

class	rageShaderViewer
{
public:
	// Instance accessor
	static rageShaderViewer* GetInstance();

	// Modifiers
	// void SetExit(bool b)	{m_bExit = b;}
	void SetBackGroundColor(const Vector3 & color);
	void SetWindowPosition(const int * data);

	// Accessors
	bool IsDone() const		{return m_bDone;}
	bool ViewerIsActive() const		{return m_bViewerIsActive;}
	Vector3 GetBackGroundColor() const;
	void GetWindowPosition(int * data) const;
	bool ViewerWantsToClose() {return m_bViewerWantsToClose;}
	void ResetViewerWantsToClose() { m_bViewerWantsToClose = false; }

	// Camera related functions
	void SetCamera(const Matrix34 & CameraMatrix);
	void SetVerticalFieldOfView(float fVerticalFieldOfView) {m_fVerticalFieldOfView = fVerticalFieldOfView;}
	void SetBoxAspectRatio(float fBoxAspectRatio) {m_fBoxAspectRatio = fBoxAspectRatio;}
	void SetAspectRatio(float fAspectRatio) {m_fAspectRatio = fAspectRatio;}
	void SetNearClipPlane(float fNearClipPlane) {m_fNearClipPlane = fNearClipPlane;}
	void SetFarClipPlane(float fFarClipPlane) {m_fFarClipPlane = fFarClipPlane;}

	// Light related functions
	int  ValidateLights(const unsigned int count);
	void DisableLight(const int lightIndex);
    void DisableAmbient(void);
	void SetLightType(const int lightIndex, grcLightGroup::grcLightType lightType);
	void SetLightPosition(const int lightIndex, const Vector3 & position);
	void SetLightDirection(const int lightIndex, const Vector3 & direction);
	void SetLightColor(const int lightIndex, float r, float g, float b);
	void SetLightIntensity(const int lightIndex, float intensity);
	void SetLightFalloff(const int lightIndex, float falloff);
	void ResetLighting(const int skipIndex = -1);
	void OnlyAmbient(void);
	void NoLighting(void);
	void SetLightName(const int lightIndex, const char* name);

	// Window management functions
	void	Open();
	void	Close();
	void	SetAlwaysOnTop(bool bAlwaysOnTop)	{m_bAlwaysOnTop = bAlwaysOnTop;}
	void	SetAcceptInput(bool bAcceptInput)	{m_bAcceptInput = bAcceptInput;}

private:
	rageShaderViewer();
	~rageShaderViewer();

	void RunThread();

	bool NeedsDone() const	{return m_bUpdate;}

	int	 GetWindowWidth() const;
	int	 GetWindowHeight() const;
	grmSetup * GetSetup() {return m_Setup;}

	static u32 __stdcall StaticRunThread( void* pParam );

	u32 __stdcall Main();

	bool			m_bDefaultLightTiedToCamera;
	bool			m_bCameraNeedsWorldMtx;
	bool			m_bDone;
	bool			m_bViewerIsActive;
//	bool			m_bExit;
	bool			m_bViewerWantsToClose;
	bool			m_bUpdate;
	bool			m_bAlwaysOnTop;
	bool			m_bAcceptInput;
	bool			m_bReqLightsExceededMax;
	u32 			m_nThreadID;
	HANDLE  		m_hThread;
	grmSetup *		m_Setup;

	lightDataManager* m_pLightDataManager;
	dcamCamMgr *	  m_pCameraManager;
	grcViewport *	  m_pViewport;

	float			m_fVerticalFieldOfView;
	float			m_fBoxAspectRatio;
	float			m_fAspectRatio;
	float			m_fNearClipPlane;
	float			m_fFarClipPlane;

	// File paths
	atString	m_sDefaultAssetDirectory;
	atString	m_sRageShaderAssetDirectory;
	atString	m_sDefaultShaderDirectory;
	atString	m_sDefaultGlobalTextureDirectory;
	atString	m_sLightsFileName;
	atString	m_sPostProcessTuneFileName;
	atString	m_sLightNames[4]; //rage only supports 4 lights by default

	Vector3 m_ClearColor;

	static rageShaderViewer* sm_pInstance;
};

} // end using namespace rage

#endif
