// 
// rageShaderViewer/ModelMaterialPairManager.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
//

#ifndef RAGE_SHADER_VIEWER_MODEL_DATA_MANAGER_H
#define RAGE_SHADER_VIEWER_MODEL_DATA_MANAGER_H

// #include "vector/matrix34.h"
// #include "atl/array.h"

#include <vector>
using namespace std;


namespace	rage
{

class	mshMesh;
class	rageShaderBucketManager;
class	ModelMaterialPair;

// An instance of this class works as the conduit between the two threads.
// One thread is controlled by the user in Maya and the other is controlled by the view itself
class	ModelMaterialPairManager
{
public:
	ModelMaterialPairManager();
	~ModelMaterialPairManager();

	// Instance accessor
	static ModelMaterialPairManager * GetInstance();

	// Base functionality - The viewer always goes through these functions
	void		Update();
	void		Draw(const float fTimeOfDay);
	void		DrawWireFrame();

	// Functions for adding and removing items from the vector of ModelMaterialPairs
	void	AddModelMaterialPair(ModelMaterialPair * pobModelMaterialPair);
	void	RemoveModelMaterialPair(const ModelMaterialPair * pobModelMaterialPair);

	// Accessors
	bool	HaveModelMaterialPairs() const {return (m_apModelMaterialPairs.size() > 0);}

private:

	// Helper function to allocate a ModelMaterialPair
	int AllocateData(const int nIndex);

	vector <ModelMaterialPair *>	m_apModelMaterialPairs;

	rageShaderBucketManager *	m_pBucketManager;

	bool						m_bUpdating;

	static ModelMaterialPairManager * sm_pInstance;
};

} // end using namespace rage

#endif
