// 
// rageShaderViewer/modelDataManager.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
//

#include "input/input.h"
#include "grcore/im.h"
#include "grcore/state.h"
#include "grmodel/shaderfx.h"

#include "rageShaderBucket/rageShaderBucketManager.h"

#include "rageShaderViewerLighting/lightDataManager.h"

#include "modelData.h"
#include "modelDataManager.h"

using namespace rage;

#define MAX_RENDER_BUCKETS 3

modelDataManager * modelDataManager::sm_pInstance = NULL;

modelDataManager::modelDataManager()
{
	m_bUpdating			= false;
	m_boRefreshShaders  = false;
	sm_pInstance		= this;

	m_nMaxDataIndex = -1;
	m_apModelData.Resize(10000);
	for (int i=0; i<m_apModelData.GetCount(); i++)
	{
		m_apModelData[i] = NULL;
	}
}

modelDataManager::~modelDataManager()
{
	sm_pInstance = NULL;

	for (int i=0; i<m_apModelData.GetCount(); i++)
	{
		// Displayf("%p Deleting the model data at index %d because my parent modelDataManager is being killed off", this, i);
		delete m_apModelData[i];
		m_apModelData[i] = NULL;
	}
	m_apModelData.Reset();

	delete m_pBucketManager;
	m_pBucketManager = NULL;
	modelData::SetBucketManager(NULL);

	rageShaderBucketManager::ShutdownShaderBucketLibrary();

	INPUT.End();
}

void	modelDataManager::Init(const char * szShaderBucketFileName)
{
	rageShaderBucketManager::InitShaderBucketLibrary();

	m_pBucketManager = new rageShaderBucketManager;
	m_pBucketManager->LoadFromXML(szShaderBucketFileName);
	m_pBucketManager->Validate();
	modelData::SetBucketManager(m_pBucketManager);

	m_bUpdating = false;
}

bool	modelDataManager::IsLocked(int nModelIndex) const
{
	 return IsDataValid(nModelIndex) ? m_apModelData[nModelIndex]->IsLocked() : false;
}

int		modelDataManager::AllocateData(const int nModelIndex)
{
	if (!m_apModelData[nModelIndex])
	{
		m_apModelData[nModelIndex] = new modelData;

		// Set the max data
		m_nMaxDataIndex = m_nMaxDataIndex < nModelIndex ? nModelIndex : m_nMaxDataIndex;
	}

	return nModelIndex;
}

bool		modelDataManager::SetMeshAtIndex(const int nModelIndex, const mshMesh & Mesh)
{	
	AllocateData(nModelIndex);

	if (m_apModelData[nModelIndex] && !m_apModelData[nModelIndex]->IsLocked() && !m_apModelData[nModelIndex]->MeshNeedsUpdate())
	{
		m_apModelData[nModelIndex]->SetMesh(Mesh);

		// Lets make sure we update it
		m_apModelData[nModelIndex]->SetMeshNeedsUpdate(true);

		return true;
	}

	return false;
}

bool		modelDataManager::UpdateMeshTransform(const int nModelIndex, const Matrix34 & m34)
{	
	AllocateData(nModelIndex);

	if (m_apModelData[nModelIndex] && !m_apModelData[nModelIndex]->IsLocked())
	{
		m_apModelData[nModelIndex]->SetWorldMatrix(m34);

		return true;
	}

	return false;
}

bool		modelDataManager::UpdateShaders(const int nModelIndex, const rageShaderMaterial* pMaterial)
{	
	AllocateData(nModelIndex);

	if (m_apModelData[nModelIndex] && !m_apModelData[nModelIndex]->IsLocked() && !m_apModelData[nModelIndex]->ShadersNeedsUpdate())
	{
		m_apModelData[nModelIndex]->UpdateShaders(pMaterial);

		// Lets make sure we update it
		m_apModelData[nModelIndex]->SetShadersNeedsUpdate(true);

		return true;
	}

	return false;
}

void	modelDataManager::DeleteData(const int nModelIndex)
{
	if (m_apModelData[nModelIndex])
	{
		m_apModelData[nModelIndex]->SetNeedsReset(true);
	}
}

void	modelDataManager::Update()
{
	m_bUpdating = true;

	// Check to see if we are going to refresh the shaders
	if (m_boRefreshShaders)
	{
		grmShaderFxTemplate::ReloadAll();
		m_boRefreshShaders = false;
	}
	

	// Delete the data we want to
	for (int index=m_nMaxDataIndex; index>=0; index--)
	{
		if (m_apModelData[index])
		{
			if (m_apModelData[index]->NeedsReset())
			{
				delete m_apModelData[index];
				m_apModelData[index] = NULL;

				// Make sure we decrement the max if we need to
				if (m_nMaxDataIndex==index)
				{
					m_nMaxDataIndex--;
				}
			}
		}
	}

	for (int i=0; i<=m_nMaxDataIndex; i++)
	{
		if (m_apModelData[i])
		{
			m_apModelData[i]->Update();
		}
	}

	m_bUpdating = false;
	
}

void	modelDataManager::Draw()
{
	// Lock all the data
	for (int i=0; i<=m_nMaxDataIndex; i++)
	{
		if (m_apModelData[i])
		{
			m_apModelData[i]->SetLocked(true);
		}
	}

	// Reset all render states
	grcState::Default();
	grcLightState::SetEnabled(true);

	// Draw everything
	m_pBucketManager->Draw();

	// Unlock all the data
	for (int i=0; i<=m_nMaxDataIndex; i++)
	{
		if (m_apModelData[i])
		{
			m_apModelData[i]->SetLocked(false);
		}
	}
}

void	modelDataManager::RefreshShaders()
{
	m_boRefreshShaders = true;
}
