// 
// rageShaderViewer/rageShaderViewer.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "input/input.h"
// #include "file/asset.h"
// #include "system/wndproc.h"
#include "system/timemgr.h"
#include "system/param.h"
// #include "grcore/device.h"
#include "grcore/state.h"
#include "grcustom/customlighting.h"
#include "grcore/im.h"
#include "grpostfx/postfx.h"
#include "grmodel/setup.h"
#include "grmodel/shaderfx.h"
// #include "grmodel/shader.h"
// #include "grcore/texture.h"
#include "grcore/viewport.h"
#include "devcam/cammgr.h"
// #include "parser/manager.h"
// #include "rageShaderMaterial/rageShaderMaterialTemplate.h"
#include "file/token.h"

#include "rageShaderViewerLighting/lightDataManager.h"

#include "ModelMaterialPairManager.h"
#include "rageShaderViewer.h"

#if __BANK
 #include	"bank/bank.h"
 #include	"bank/bkmgr.h"
#endif

#define USE_POST_EFFECTS

using namespace rage;

extern HWND *g_hwndMain;

rageShaderViewer * rageShaderViewer::sm_pInstance = NULL;

rageShaderViewer::rageShaderViewer()
{
	m_bDone			= false;
	m_bViewerIsActive	= false;
	m_bViewerWantsToClose	= false;
	m_bCameraNeedsWorldMtx = false;
	m_bReqLightsExceededMax = false;
	m_bDefaultLightTiedToCamera = true;
	m_nThreadID		= 0;
	m_hThread		= NULL;
	m_Setup			= NULL;
	m_pViewport		= NULL;
	m_pCameraManager= NULL;
	SetAlwaysOnTop(true);
	SetAcceptInput(false);

	// ***** Start Init *****
	/*initModuleSettings::GetModuleSetting(("PathToAssets"), m_sDefaultAssetDirectory);
	initModuleSettings::GetModuleSetting(("PathToCompiledShaders"), m_sDefaultShaderDirectory);
	initModuleSettings::GetModuleSetting(("PathToGlobalTextures"), m_sDefaultGlobalTextureDirectory);
	initModuleSettings::GetModuleSetting(("LightsFileName"), m_sLightsFileName);
	initModuleSettings::GetModuleSetting("PostProcessTuneFileName", m_sPostProcessTuneFileName);
	initModuleSettings::GetModuleSetting(("PathToRageShaderAssets"), m_sRageShaderAssetDirectory);
	*/
	if (m_sDefaultAssetDirectory.GetLength() == 0)
	{
		Warningf("Could not find the \"PathToAssets\" parameter in the configuration setting.");
	}
	if(m_sDefaultShaderDirectory.GetLength() == 0)
	{
		Warningf("Could not find the \"PathToCompiledShaders\" parameter in the configuration setting.");
	}
	if(m_sDefaultGlobalTextureDirectory.GetLength() == 0)
	{
		Warningf("Could not find the \"PathToGlobalTextures\" parameter in the configuration setting.");
	}
	if(m_sLightsFileName.GetLength() == 0)
	{
		Warningf("Could not find the \"LightsFileName\" parameter in the configuration setting.");
	}
	if (m_sPostProcessTuneFileName.GetLength() == 0)
	{
		Warningf("Could not find the \"PostProcessTuneFileName\" parameter in the configuration setting.");
	}
	if (m_sRageShaderAssetDirectory.GetLength() == 0)
	{
		Warningf("Could not find the \"PathToRageShaderAssets\" parameter in the configuration setting.");
	}

	// Lets make sure we have a asset directory
	char createDirectories[256];
	sprintf(createDirectories, "%stest.txt", m_sRageShaderAssetDirectory);
	ASSET.CreateLeadingPath(createDirectories);

	m_sLightNames[0] = "Rage Default Directional";
	m_sLightNames[1] = "None";
	m_sLightNames[2] = "None";
	m_sLightNames[3] = "None";
}

rageShaderViewer::~rageShaderViewer()
{
	delete m_pViewport;
	m_pViewport = NULL;

	delete m_pCameraManager;
	m_pCameraManager = NULL;

	delete m_Setup;
	m_Setup = NULL;

#if __BANK
	// Make sure we destroy the bank
	//if (m_pPostProcessinglBank)
	//{
	//	BANKMGR.DestroyBank(*m_pPostProcessinglBank);
	//	m_pPostProcessinglBank = NULL;
	//}
#endif
}

void	rageShaderViewer::SetCamera(const Matrix34 & CameraMatrix)
{
	// Only update the camera if we are focused
	if (!GRCDEVICE.GetLostFocus() && !m_bCameraNeedsWorldMtx)
	{
		return;
	}

	if (m_pCameraManager)
	{
		m_pCameraManager->SetWorldMtx(CameraMatrix);
		m_bCameraNeedsWorldMtx = false;
	}
}

int rageShaderViewer::ValidateLights(const unsigned int count)
{
	int validatedCount = 0;
	m_bReqLightsExceededMax = false;
	if (m_pLightDataManager)
	{
		grcLightGroup* lightGroup = m_pLightDataManager->GetLights();
		if (lightGroup)
		{
			if (s32(count) <= lightGroup->GetMaxAllowed())
			{
				lightGroup->SetActiveCount(count);
				validatedCount = count;
			}
			else
			{
				lightGroup->SetActiveCount(lightGroup->GetMaxAllowed());
				validatedCount = lightGroup->GetMaxAllowed();
				m_bReqLightsExceededMax = true;
			}
		}
	}
	return validatedCount;
}

void rageShaderViewer::DisableLight(const int lightIndex)
{
	if (m_pLightDataManager)
	{
		grcLightGroup* lightGroup = m_pLightDataManager->GetLights();
		if (lightGroup)
		{
			Assert(lightIndex < lightGroup->GetActiveCount());
			lightGroup->SetLightEnabled(lightIndex, false);
		}
	}
}

void rageShaderViewer::DisableAmbient(void)
{
	if (m_pLightDataManager)
	{
		grcLightGroup* lightGroup = m_pLightDataManager->GetLights();
		if (lightGroup)
		{
			lightGroup->SetUseAmbient(false);
		}
	}
}

void rageShaderViewer::SetLightType(const int lightIndex, grcLightGroup::grcLightType lightType)
{
	if (m_pLightDataManager)
	{
		grcLightGroup* lightGroup = m_pLightDataManager->GetLights();
		if (lightGroup)
		{
			Assert(lightIndex < lightGroup->GetActiveCount());
			lightGroup->SetLightType(lightIndex, lightType);
		}
	}
}

void rageShaderViewer::SetLightPosition(const int lightIndex, const Vector3 & position)
{
	if (m_pLightDataManager)
	{
		grcLightGroup* lightGroup = m_pLightDataManager->GetLights();
		if (lightGroup)
		{
			Assert(lightIndex < lightGroup->GetActiveCount());
			lightGroup->SetPosition(lightIndex, position);
		}
	}
}

void rageShaderViewer::SetLightDirection(const int lightIndex, const Vector3 & direction)
{
	if (m_pLightDataManager)
	{
		grcLightGroup* lightGroup = m_pLightDataManager->GetLights();
		if (lightGroup)
		{
			Assert(lightIndex < lightGroup->GetActiveCount());
			lightGroup->SetDirection(lightIndex, direction);
		}
	}
}

void rageShaderViewer::SetLightColor(const int lightIndex, float r, float g, float b)
{
	if (m_pLightDataManager)
	{
		grcLightGroup* lightGroup = m_pLightDataManager->GetLights();
		if (lightGroup)
		{
			Assert(lightIndex < lightGroup->GetActiveCount());
			lightGroup->SetColor(lightIndex, r, g, b);
		}
	}
}

void rageShaderViewer::SetLightIntensity(const int lightIndex, float intensity)
{
	if (m_pLightDataManager)
	{
		grcLightGroup* lightGroup = m_pLightDataManager->GetLights();
		if (lightGroup)
		{
			Assert(lightIndex < lightGroup->GetActiveCount());
			lightGroup->SetIntensity(lightIndex, intensity);
		}
	}
}

void rageShaderViewer::SetLightFalloff(const int lightIndex, float falloff)
{
	if (m_pLightDataManager)
	{
		grcLightGroup* lightGroup = m_pLightDataManager->GetLights();
		if (lightGroup)
		{
			Assert(lightIndex < lightGroup->GetActiveCount());
			lightGroup->SetFalloff(lightIndex, falloff);
		}
	}
}

void rageShaderViewer::ResetLighting(const int skipIndex)
{
	m_bReqLightsExceededMax = false;
	if (m_pLightDataManager)
	{
		grcLightGroup* lightGroup = m_pLightDataManager->GetLights();
		if (lightGroup)
		{
			for (int activeLight = 0; activeLight < lightGroup->GetActiveCount(); activeLight++)
			{
				if (skipIndex != activeLight)
				{
					lightGroup->Reset(activeLight);
				}
			}
			//the first one is the default directional light.
			lightGroup->SetActiveCount(1);
		}

		if (m_bDefaultLightTiedToCamera)
		{
			m_pLightDataManager->SetUseDefaultLighting(true);
		}

		m_pLightDataManager->Update(m_pCameraManager->GetWorldMtx());

		if (m_bDefaultLightTiedToCamera)
		{
			m_pLightDataManager->SetUseDefaultLighting(false);
			m_sLightNames[0] = "Rage Default Directional Tied To Camera";
			m_sLightNames[1] = "None";
			m_sLightNames[2] = "None";
			m_sLightNames[3] = "None";
		}
		else
		{
			m_sLightNames[0] = "Rage Default Directional";
			m_sLightNames[1] = "None";
			m_sLightNames[2] = "None";
			m_sLightNames[3] = "None";
		}
	}
}

void rageShaderViewer::OnlyAmbient(void)
{
	m_bReqLightsExceededMax = false;
	if (m_pLightDataManager)
	{
		grcLightGroup* lightGroup = m_pLightDataManager->GetLights();
		if (lightGroup)
		{
			for (int activeLight = 0; activeLight < lightGroup->GetActiveCount(); activeLight++)
			{
				DisableLight(activeLight);
			}
			lightGroup->SetAmbient(1.0f,1.0f,1.0f);
			lightGroup->SetUseAmbient(true);
		}
	}
}

void rageShaderViewer::NoLighting(void)
{
	if (m_pLightDataManager)
	{
		grcLightGroup* lightGroup = m_pLightDataManager->GetLights();
		if (lightGroup)
		{
			lightGroup->SetActiveCount(0);
			lightGroup->SetUseAmbient(false);
		}
	}
}

void rageShaderViewer::SetLightName(const int lightIndex, const char* name)
{
	if (lightIndex < 4)
	{
		m_sLightNames[lightIndex] = name;
	}
}

u32 __stdcall rageShaderViewer::StaticRunThread( void* pParam )
{
	rageShaderViewer* pDevice = (rageShaderViewer*) pParam;
	return pDevice->Main();
}

namespace rage {
	XPARAM(hidewindow);
}

u32 __stdcall rageShaderViewer::Main()
{
	m_bDone = false;
	m_bViewerWantsToClose = false;
	m_bViewerIsActive = false;
	m_bDefaultLightTiedToCamera = true;

	//PARAM_hidewindow.Set("");

	m_Setup = new grmSetup;
	m_Setup->Init(m_sDefaultAssetDirectory, "RAGE Shader Viewer");

	// Open the window
	// First should it be opened always on top?
	m_Setup->BeginGfx(true,m_bAlwaysOnTop);

	// Setup everything else
	m_Setup->CreateDefaultFactories();

	grcEffect::SetDefaultPath(m_sDefaultShaderDirectory);

	grcTextureFactory::GetInstance().PreloadGlobalTextures(m_sDefaultGlobalTextureDirectory);

	Color32 temp;
	temp.Set(100,100,100);
	m_ClearColor = Vector3(temp.GetRedf(), temp.GetGreenf(), temp.GetBluef());
	m_Setup->SetClearColor(temp);

	GRCDEVICE.SetBlockOnLostFocus(false);

#if __XENON
	grcGPRAllocation::Init();
#endif
#ifdef USE_POST_EFFECTS
	GRPOSTFX->Init();
	GRPOSTFX->InitShaders("shaders/misc");
	GRPOSTFX->CreateRenderTargets(GRCDEVICE.GetWidth(), GRCDEVICE.GetHeight());
	GRPOSTFX->SetShowRenderTargetFlag(false);
	if((m_sPostProcessTuneFileName) && (m_sPostProcessTuneFileName.GetLength() != 0))
	{
		GRPOSTFX->LoadPPPPreset((const char *)m_sPostProcessTuneFileName);
	}
	GRPOSTFX->SetDefaultTechnique();
	GRPOSTFX->SetClearColor(temp.GetRedf(), temp.GetGreenf(), temp.GetBluef(), 1.0f);
#endif

	// Night time stuff
	grmShaderFx::RegisterTechniqueGroup("multilight");
	int iMultilightShaderTechniqueIdx = grmShaderFx::FindTechniqueGroupId("multilight");
	//grmShaderFx::RegisterTechniqueGroup("NFLDEBUG");
	//int iNFLDEBUGShaderTechniqueIdx = grmShaderFx::FindTechniqueGroupId("NFLDEBUG");

	// Viewer options
	bool bDrawGrid = true;
	bool bDrawFilmGate = false;
	//bool bDrawLightingModel = false;
	bool bDrawTimeOfDay = false;
	bool bDrawWireFrame = false;
	bool bDrawWireFrameOnly = false;
	bool bDrawLights = false;
	bool bDrawLightNames = false;
	float fDrawnLightSize = 0.01f;
	float fTimeOfDay = 10.0f;

	if (!grcCustomLighting::IsInstantiated())
	{
		grcCustomLighting::InitClass();
	}
	atString strLightingModelName;
	initModuleSettings::GetModuleSetting(("CustomLightingModel"), strLightingModelName);

#if __BANK
	bkBank *	pBackgroundBank		= NULL;
	bkBank *	pDisplayBank		= NULL;
	bkBank *	pVisSettingsBank	= NULL;
	bkBank *	pShadingBank		= NULL;
	bkBank *	pLightsBank			= NULL;

	//////////////////////////////////////////////////////////////////////////
	pBackgroundBank = &BANKMGR.CreateBank("Background");
	pBackgroundBank->AddColor("Color",&m_ClearColor);
	
	//////////////////////////////////////////////////////////////////////////
	pDisplayBank = &BANKMGR.CreateBank("Display");
	pDisplayBank->AddToggle("Grid", &bDrawGrid);
	pDisplayBank->AddToggle("Film Gate", &bDrawFilmGate);

	//////////////////////////////////////////////////////////////////////////
	atString acEffectOptions;
	initModuleSettings::GetModuleSetting("MayaViewerGlobalEffectOptions", acEffectOptions);

	atArray<atString> effectVarNames;
	atArray<grcEffectGlobalVar> effectVars;
	atArray<bool> effectToggles;

	if (acEffectOptions.length())
	{
		pVisSettingsBank = &BANKMGR.CreateBank("Visibility Settings");
		char token[256];
		fiStream* effectOptions = fiStream::Open(acEffectOptions.c_str());
		if( effectOptions )
		{
			fiTokenizer tok;
			tok.Init( acEffectOptions.c_str(), effectOptions );
			tok.GetToken(token, sizeof(token));
			while(strlen(token)) 
			{
				effectVarNames.PushAndGrow(atString(token));
				effectVars.PushAndGrow(grcegvNONE);
				effectToggles.PushAndGrow(false);
				tok.GetToken(token, sizeof(token));
			}
			effectOptions->Close();
		}

		if (effectVarNames.GetCount())
		{
			for (int i = 0; i < effectVarNames.GetCount(); i++)
			{
				pVisSettingsBank->AddToggle(effectVarNames[i], &effectToggles[i]);
			}
		}		
	}

	//////////////////////////////////////////////////////////////////////////
	pShadingBank = &BANKMGR.CreateBank("Shading");
	pShadingBank->AddToggle("Wire Frame", &bDrawWireFrameOnly);
	pShadingBank->AddToggle("Wire Frame on Shaded", &bDrawWireFrame);

	//////////////////////////////////////////////////////////////////////////
	pLightsBank = &BANKMGR.CreateBank("Lights");
	pLightsBank->AddToggle("Default Light Tied To Camera", &m_bDefaultLightTiedToCamera);
	pLightsBank->AddToggle("Draw Light Names", &bDrawLightNames);
	pLightsBank->AddToggle("Draw Light Positions", &bDrawLights);
	pLightsBank->AddSlider("Drawn Light Size", &fDrawnLightSize, 0.01f, 1.0f, 0.01f);
	atString acUseMc4TOD;
	initModuleSettings::GetModuleSetting("RageShaderViewerUseMc4TimeOfDay", acUseMc4TOD);
	if (acUseMc4TOD == "true")
	{
		pLightsBank->AddSlider("Time Of Day", &fTimeOfDay, 0.0f, 24.0f, 0.5f);
	}
#endif

	m_pViewport = new grcViewport;

	Vector3 startPosition(0.0f, 0.0f, 0.0f);
	Vector3 startDirection(0.0f, 0.0f, -1.0f);

	m_pCameraManager = new dcamCamMgr;
	m_pCameraManager->Init(startPosition, startDirection, dcamCamMgr::CAM_MAYA);

	m_fNearClipPlane = 0.01f;
	m_fFarClipPlane = 1000000000.0f;
	m_pViewport->SetNearClip(m_fNearClipPlane);
	m_pViewport->SetFarClip(m_fFarClipPlane);

	if(m_bAcceptInput)
	{
		INPUT.Begin(true);
	}

	//INIT_PARSER;
	//rageShaderMaterialTemplate::InitShaderMaterialLibrary();
	m_pLightDataManager = new lightDataManager;
	m_pLightDataManager->Init(m_sLightsFileName);
	ASSET.SetPath(m_sRageShaderAssetDirectory);

	m_bViewerIsActive = true;
	m_bCameraNeedsWorldMtx = true;
	/// no longer works, and not sure why it was here in the first place.   GRCDEVICE.Resize(GetWindowWidth(), GetWindowHeight());

	do
	{
		//adding this check here so it loops while we have model pairs
		//	this will hopefully prevent some shutdown errors.
		if (!m_bViewerWantsToClose)
		{

			// Start updating
			m_Setup->BeginUpdate();

			TIME.Update();

			INPUT.Update();

			m_pCameraManager->Update();

			// Update the lights
			m_pLightDataManager->SetTimeOfDay(fTimeOfDay);
			//m_pLightDataManager->Update(m_pCameraManager->GetWorldMtx());

			// Update the models
			ModelMaterialPairManager::GetInstance()->Update();

			Color32 temp = Color32(m_ClearColor);
			m_Setup->SetClearColor(temp);
	#ifdef USE_POST_EFFECTS
			GRPOSTFX->SetClearColor(temp.GetRedf(), temp.GetGreenf(), temp.GetBluef(), 1.0f);
	#endif

			// End Updating
			m_Setup->EndUpdate();

			if (lightDataManager::GetInstance())
			{
				float fTimeOfDay = lightDataManager::GetInstance()->GetTimeOfDay();
				bool bNighttime = ((fTimeOfDay < 6.0f) || (fTimeOfDay > 18.0f));
				if (bNighttime)
				{
					// grmShaderFx::SetForcedTechniqueGroupId(iNFLDEBUGShaderTechniqueIdx);
					grmShaderFx::SetForcedTechniqueGroupId(iMultilightShaderTechniqueIdx);

					rage::grcEffectGlobalVar	 obShaderVariable_ShaderGlobalsID = rage::grcEffect::LookupGlobalVar("ShaderGlobals", false);
					if(obShaderVariable_ShaderGlobalsID != rage::grcegvNONE)
					{
						Vector4	vImapData;
						vImapData.x = fTimeOfDay;
						vImapData.y = TIME.GetElapsedTime();
						vImapData.z = 1.0f; 
						vImapData.w = 0.1f; 
						rage::grcEffect::SetGlobalVar(obShaderVariable_ShaderGlobalsID, vImapData);
					}
					rage::grcEffectGlobalVar	 obShaderVariable_gPropNFLColorID = rage::grcEffect::LookupGlobalVar("gPropNFLColor", false);
					if(obShaderVariable_gPropNFLColorID != rage::grcegvNONE)
					{
						Vector4	vImapData;
						vImapData.x = 0.0f;
						vImapData.y = 0.0f;
						vImapData.z = 0.0f; 
						vImapData.w = 1.0f; 
						rage::grcEffect::SetGlobalVar(obShaderVariable_gPropNFLColorID, vImapData);
					}
				}
				else
				{
					grmShaderFx::SetForcedTechniqueGroupId(-1);
					rage::grcEffectGlobalVar	 obShaderVariable_ShaderGlobalsID = rage::grcEffect::LookupGlobalVar("ShaderGlobals", false);
					if(obShaderVariable_ShaderGlobalsID != rage::grcegvNONE)
					{
						Vector4	vImapData;
						vImapData.x = fTimeOfDay;
						vImapData.y = TIME.GetElapsedTime();
						vImapData.z = 0.0f; 
						vImapData.w = 0.1f; 
						rage::grcEffect::SetGlobalVar(obShaderVariable_ShaderGlobalsID, vImapData);
					}
					rage::grcEffectGlobalVar	 obShaderVariable_gPropNFLColorID = rage::grcEffect::LookupGlobalVar("gPropNFLColor", false);
					if(obShaderVariable_gPropNFLColorID != rage::grcegvNONE)
					{
						Vector4	vImapData;
						vImapData.x = 0.0f;
						vImapData.y = 0.0f;
						vImapData.z = 0.0f; 
						vImapData.w = 1.0f; 
						rage::grcEffect::SetGlobalVar(obShaderVariable_gPropNFLColorID, vImapData);
					}
				}
			}

			rage::grcEffectGlobalVar	 obShaderVariable_RedID = rage::grcEffect::LookupGlobalVar("skyLight_LightRed", false);
			if(obShaderVariable_RedID != rage::grcegvNONE)
			{
				Vector4	vImapData;
				vImapData.x = 1.0f;
				vImapData.y = 1.0f;
				vImapData.z = 1.0f; 
				vImapData.w = 0.0f; 
				rage::grcEffect::SetGlobalVar(obShaderVariable_RedID, vImapData);
			}
			rage::grcEffectGlobalVar	 obShaderVariable_GreenID = rage::grcEffect::LookupGlobalVar("skyLight_LightGreen", false);
			if(obShaderVariable_GreenID != rage::grcegvNONE)
			{
				Vector4	vImapData;
				vImapData.x = 1.0f;
				vImapData.y = 1.0f;
				vImapData.z = 1.0f; 
				vImapData.w = 0.0f; 
				rage::grcEffect::SetGlobalVar(obShaderVariable_GreenID, vImapData);
			}
			rage::grcEffectGlobalVar	 obShaderVariable_BlueID = rage::grcEffect::LookupGlobalVar("skyLight_LightBlue", false);
			if(obShaderVariable_BlueID != rage::grcegvNONE)
			{
				Vector4	vImapData;
				vImapData.x = 1.0f;
				vImapData.y = 1.0f;
				vImapData.z = 1.0f; 
				vImapData.w = 0.0f; 
				rage::grcEffect::SetGlobalVar(obShaderVariable_BlueID, vImapData);
			}

			// Start rendering
			m_Setup->BeginDraw();

	#ifdef USE_POST_EFFECTS
			// Begin the post process
			GRPOSTFX->BindPCRenderTarget(true);
	#endif

			// What you see in Maya should ALWAYS appear in the viewer, so that means that the field of view
			// has to be messed with.  By default, setting the vertical field of view in the viewer to be the 
			// same as the vertical field of view in Maya means that the top and bottom of the window will
			// perfect match the top and bottom of the film box in Maya.  BUT the sides of the window may show
			// more or less than the film box in Maya.  More is ok, but less is bad.
			//
			// So we need to detect when this is the case and fiddle with the field of view so that you either
			// get extra top and bottom, or extra left and right, but no cropping
			// 
			// Assuming the height of the film box is the same of the height of the window (i.e. the vertical
			// field of view in the viewer is the same as that in Maya) work out that the width would be
			float fVerticalFieldOfView = m_fVerticalFieldOfView;
			float fWindowWidth = (float)GetWindowWidth();
			float fWindowHeight = (float)GetWindowHeight();
			float fHeightOfFilmBox = fWindowHeight;
			float fWidthOfFilmBox = fHeightOfFilmBox * m_fBoxAspectRatio;

			if(fWidthOfFilmBox > fWindowWidth)
			{
				// Oh dear, if I make the film box height the same as the window height, the 
				// the film box would go off the sides, so instead make the film box width 
				// the same as the window width
				fWidthOfFilmBox = fWindowWidth;
				fHeightOfFilmBox = fWidthOfFilmBox / m_fBoxAspectRatio;
			}

			// Setup the camera
			m_fAspectRatio =  fWindowWidth / fWindowHeight;
			m_pViewport->SetCameraMtx(m_pCameraManager->GetWorldMtx());
			m_pViewport->Perspective(fVerticalFieldOfView, m_fAspectRatio, m_fNearClipPlane, m_fFarClipPlane);
			grcViewport::SetCurrent(m_pViewport);
			grcState::SetCullMode(grccmBack);

			// Setup the lighting
			grcCustomLighting::GetInstance().SafeInitImplementations();
			grcCustomLighting::GetInstance().SetLightingModel(strLightingModelName);
			grcCustomLighting::GetInstance().SetLightingGroup(*m_pLightDataManager->GetLights());
			// grcState::SetLightingGroup(*lightDataManager::GetInstance()->GetLights());
			// grcLightState::SetEnabled(true);

			if(bDrawWireFrame || bDrawWireFrameOnly)
			{
				// Render the models in wireframe
				ModelMaterialPairManager::GetInstance()->DrawWireFrame();
			}

			if(!bDrawWireFrameOnly)
			{
				// Render the models
				ModelMaterialPairManager::GetInstance()->Draw(m_pLightDataManager->GetTimeOfDay());
			}

			// Try to recover from shaderfx rendering
			grcState::Default();	
			
			grcWorldIdentity();

			// Draw the "film gate" from Maya
			float fBoxMinX = (0.5f * fWindowWidth) - (0.5f * fWidthOfFilmBox);
			float fBoxMaxX = (0.5f * fWindowWidth) + (0.5f * fWidthOfFilmBox);
			float fBoxMinY = (0.5f * fWindowHeight) - (0.5f * fHeightOfFilmBox);
			float fBoxMaxY = (0.5f * fWindowHeight) + (0.5f * fHeightOfFilmBox);

			if(bDrawFilmGate)
			{
				grcViewport* pobOldViewport = grcViewport::SetCurrent(grcViewport::GetDefaultScreen());
				grcBegin(drawLineStrip,5);
				grcColor3f(0.0f,0.5f,0.0f);
				grcVertex3f(fBoxMinX,fBoxMinY,0);
				grcVertex3f(fBoxMinX,fBoxMaxY,0);
				grcVertex3f(fBoxMaxX,fBoxMaxY,0);
				grcVertex3f(fBoxMaxX,fBoxMinY,0);
				grcVertex3f(fBoxMinX,fBoxMinY,0);
				grcEnd();
				grcViewport::SetCurrent(pobOldViewport);
			}

			if (m_bReqLightsExceededMax)
			{
				grcViewport* pobOldViewport = grcViewport::SetCurrent(grcViewport::GetDefaultScreen());
				char acDebugText[255];
				strcpy(acDebugText,"Exceeded Maximum allowed lights in scene (max 4)");
				grcColor3f(1.0f,0.0f,0.0f);
				grcDraw2dText(1.0f, 1.0f, acDebugText);
				grcViewport::SetCurrent(pobOldViewport);
			}

			if (bDrawLightNames)
			{
				grcViewport* pobOldViewport = grcViewport::SetCurrent(grcViewport::GetDefaultScreen());
				char acDebugText[255];
				static float text_size = 10.0f;
				for (int i = 0; i < 4; i++)
				{
					strcpy(acDebugText, m_sLightNames[i].c_str());
					grcColor3f(0.0f,1.0f,0.0f);
					grcDraw2dText(1.0f, 1.0f + text_size*(i+((m_bReqLightsExceededMax)?1:0)), acDebugText);
				}
				grcViewport::SetCurrent(pobOldViewport);
			}

			/*
			if(bDrawLightingModel)
			{
				grcViewport* pobOldViewport = grcViewport::SetCurrent(grcViewport::GetDefaultScreen());
				char acDebugText[255];
				strcpy(acDebugText,"Lighting Model : ");
				strcat(acDebugText,strLightingModelName);
				grcColor3f(0.5f,0.0f,0.0f);
				grcDraw2dText(1.0f, 1.0f, acDebugText);
				grcViewport::SetCurrent(pobOldViewport);
			}
			*/

			if(bDrawTimeOfDay)
			{
				grcViewport* pobOldViewport = grcViewport::SetCurrent(grcViewport::GetDefaultScreen());
				char acDebugText[255];
				sprintf(acDebugText, "Time of Day : %f", lightDataManager::GetInstance()->GetTimeOfDay());
				grcColor3f(0.75f,0.0f,0.0f);
				grcDraw2dText(1.0f, 1.0f, acDebugText);
				grcViewport::SetCurrent(pobOldViewport);
			}

			// Draw the grid
			if(bDrawGrid)
			{
				// Fine grid
				grcColor3f(0.3f,0.3f,0.3f);
				for(float i=-12.0f; i<12.5f; i++)
				{
					grcBegin(drawLines,2);
					grcVertex3f(-12.0f, 0.0f, i);
					grcVertex3f( 12.0f, 0.0f, i);
					grcEnd();
					grcBegin(drawLines,2);
					grcVertex3f(i, 0.0f,-12.0f);
					grcVertex3f(i, 0.0f, 12.0f);
					grcEnd();
				}

				// Big X
				grcColor3f(0.0f,0.0f,0.0f);
				grcBegin(drawLines,2);
				grcVertex3f(-12.0f, 0.0f, 0.0f);
				grcVertex3f( 12.0f, 0.0f, 0.0f);
				grcEnd();
				grcBegin(drawLines,2);
				grcVertex3f(0.0f, 0.0f,-12.0f);
				grcVertex3f(0.0f, 0.0f, 12.0f);
				grcEnd();
			}

			// Draw the lights
			if(bDrawLights)
			{
				grcLightGroup* lightGroup = m_pLightDataManager->GetLights();
				if (lightGroup && lightGroup->GetActiveCount())
				{
					for (int lightIndex = 0; lightIndex < lightGroup->GetActiveCount(); lightIndex++)
					{
						//TODO: debug drawing for other light types
						if (lightGroup->GetLightType(lightIndex) == grcLightGroup::LTTYPE_POINT)
						{
							grcColor3f(0.25f,0.0f,0.0f);
							grcDrawSphere(fDrawnLightSize, lightGroup->GetPosition(lightIndex), 8, true);
						}
					}
				}
			}

	#ifdef USE_POST_EFFECTS
			// End post process effect
			GRPOSTFX->Process();
	#endif

			// End rendering
			m_Setup->EndDraw();

			// This forces us to stay at 30fps
			if (m_Setup->GetTotalTime() < 100.0f)
			{
				u32 uSleepTime = (u32)((100.0f - m_Setup->GetTotalTime()) + 0.5f);
				Sleep(uSleepTime);
			}

			// Does the user want to close the viewer?
			m_bViewerWantsToClose = (m_bViewerWantsToClose || m_Setup->WantExit());

	#if __BANK
			for (int i = 0; i < effectVars.GetCount(); i++)
			{
				//try to find it if it is none
				if (effectVars[i] == grcegvNONE)
				{
					effectVars[i] = grcEffect::LookupGlobalVar(effectVarNames[i].c_str(), false);
				}

				if (effectVars[i] != grcegvNONE)
				{
					grcEffect::SetGlobalVar(effectVars[i], effectToggles[i]);
				}
			}
	#endif // __BANK
		}
		else
		{
			m_Setup->BeginUpdate();
			// Update the models but dont need to update anything else
			ModelMaterialPairManager::GetInstance()->Update();
			m_Setup->EndUpdate();
		}
	}	while	(ModelMaterialPairManager::GetInstance()->HaveModelMaterialPairs());

	// Clean up my stuff
	delete m_pLightDataManager;
	m_pLightDataManager = NULL;

	if (grcCustomLighting::IsInstantiated())
	{
		grcCustomLighting::ShutdownClass();
	}

	//rageShaderMaterialTemplate::ShutdownShaderMaterialLibrary();
	//SHUTDOWN_PARSER;

	grcTextureFactory::GetInstance().UnloadGlobalTextures();

	if(m_bAcceptInput)
	{
		INPUT.End();
	}

	delete m_pCameraManager;
	m_pCameraManager = NULL;

	delete m_pViewport;
	m_pViewport = NULL;

#ifdef USE_POST_EFFECTS
	GRPOSTFX->DeleteRenderTargets();
	GRPOSTFX->Terminate();
#endif

#if __XENON
	grcGPRAllocation::Terminate();
#endif

#if __BANK
	if (pBackgroundBank)
	{
		BANKMGR.DestroyBank((*pBackgroundBank));
	}

	if (pDisplayBank)
	{
		BANKMGR.DestroyBank((*pDisplayBank));
	}

	if (pVisSettingsBank)
	{
		BANKMGR.DestroyBank((*pVisSettingsBank));
	}

	if (pShadingBank)
	{
		BANKMGR.DestroyBank((*pShadingBank));
	}

	if (pLightsBank)
	{
		BANKMGR.DestroyBank((*pLightsBank));
	}
#endif

	m_Setup->DestroyFactories();
	m_Setup->EndGfx();
	m_Setup->Shutdown();

	delete m_Setup;
	m_Setup = NULL;

	m_bDone = true;
	m_bViewerIsActive = false;
	m_hThread = NULL;

	return(0);
}

Vector3 rageShaderViewer::GetBackGroundColor() const
{
	Vector3 v;
	Color32 c = m_Setup->GetClearColor();
	v.x = c.GetRedf();
	v.y = c.GetGreenf();
	v.z = c.GetBluef();
	return v;
}

void rageShaderViewer::GetWindowPosition(int * data) const
{
	WINDOWINFO wi;
	GetWindowInfo(g_hwndMain, &wi);
	data[0] = wi.rcWindow.left;
	data[1] = wi.rcWindow.top;
	data[2] = wi.rcWindow.right - wi.rcWindow.left;
	data[3] = wi.rcWindow.bottom - wi.rcWindow.top;
}

int	 rageShaderViewer::GetWindowWidth() const
{
	WINDOWINFO wi;
	GetWindowInfo(g_hwndMain, &wi);
	return (wi.rcWindow.right - wi.rcWindow.left);
}

int	 rageShaderViewer::GetWindowHeight() const
{
	WINDOWINFO wi;
	GetWindowInfo(g_hwndMain, &wi);
	return (wi.rcWindow.bottom - wi.rcWindow.top);
}

void rageShaderViewer::SetBackGroundColor(const Vector3 & color)
{
	m_ClearColor = color;
	m_Setup->SetClearColor(Color32(m_ClearColor));
}

void rageShaderViewer::SetWindowPosition(const int * data)
{
	MoveWindow(g_hwndMain, data[0], data[1], data[2], data[3], TRUE);
}

void rageShaderViewer::RunThread()
{
	// Make sure we are not already running
	if (m_hThread)
	{
		return;
	}

	// Launch the thread which will create the device and check for device state changes
	m_hThread = (HANDLE)_beginthreadex( NULL, 0, StaticRunThread, this, 0, &m_nThreadID );

	SetThreadPriority(m_hThread, THREAD_PRIORITY_LOWEST);
}

rageShaderViewer* rageShaderViewer::GetInstance()
{
	if(!sm_pInstance)
	{
		sm_pInstance = new rageShaderViewer();
	}
	return sm_pInstance;
}

void rageShaderViewer::Open()
{
	// Open the window, if it isn't already open
	if(!m_hThread)
	{
		RunThread();
	}
}
