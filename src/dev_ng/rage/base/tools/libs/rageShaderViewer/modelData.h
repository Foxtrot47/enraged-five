// 
// rageShaderViewer/modelData.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
//
#ifndef RAGE_SHADER_VIEWER_MODEL_DATA_H
#define RAGE_SHADER_VIEWER_MODEL_DATA_H

#include "atl/string.h"
#include "mesh/mesh.h"
#include "rageShaderMaterial/rageShaderMaterial.h"

namespace rage {

class grmModel;
class rageShaderBucketManager;

class modelData
{
public:

	modelData();
	~modelData();

	// Accessors
	bool	IsLocked()				const {return m_Locked;}
	bool    NeedsReset()			const {return m_NeedsReset;}
	bool	NeedsUpdate()			const {return m_MeshNeedsUpdate || m_ShadersNeedsUpdate;}
	bool	MeshNeedsUpdate()		const {return m_MeshNeedsUpdate;}
	bool	ShadersNeedsUpdate()	const {return m_ShadersNeedsUpdate;}
	int		GetBucketNumber()		const;

	// Modifiers
	void    SetNeedsReset(bool b)					{m_NeedsReset = b;}
	void	SetMeshNeedsUpdate(bool b)				{m_MeshNeedsUpdate = b;}
	void	SetShadersNeedsUpdate(bool b)			{m_ShadersNeedsUpdate = b;}
	void	SetWorldMatrix(const Matrix34 & m34)	{m_WorldMatrix = m34;}
	void	SetLocked(bool b)						{m_Locked = b;}

	static void SetBucketManager(rageShaderBucketManager * pBucketManager);

	// Function to initialize the model data
	void SetMesh(const mshMesh & Mesh);

	void UpdateShaders(const rageShaderMaterial* pMtl);

	void Update();

	// Function to render the model data
	void Render();

private:

	// Function to refresh the mdoel data
	void Refresh();

	// Stored data to the files
	bool		m_Locked;
	bool		m_NeedsReset;
	bool		m_MeshNeedsUpdate;
	bool		m_ShadersNeedsUpdate;

	mshMesh 			m_Mesh;
	rageShaderMaterial*	m_pMaterial;

	// Renderable data
	Matrix34			m_WorldMatrix;
	grmModel *			m_pModel;
	grmShaderFx *		m_pShader;
	

	// Delete me data
	grmModel *			m_pDeleteMeModel;
	grmShaderFx *		m_pDeleteMeShader;

	static rageShaderBucketManager * sm_pBucketManager;
};

} // end using namespace rage

#endif /* RAGE_SHADER_VIEWER_MODEL_DATA_H */
