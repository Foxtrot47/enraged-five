// 
// rageShaderViewer/ModelMaterialPair.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
//

// #include "mesh/mesh.h"
// #include "mesh/serialize.h"
// #include "file/asset.h"
// #include "grcore/state.h"
#include "grmodel/shaderfx.h"
#include "grmodel/model.h"
#include "grcore/im.h"
#include "grcore/state.h"

#include "rageShaderBucket/rageShaderBucketManager.h"

#include "ModelMaterialPairManager.h"
#include "ModelMaterialPair.h"

using namespace rage;

rageShaderBucketManager * ModelMaterialPair::sm_pBucketManager = NULL;

void ModelMaterialPair::SetBucketManager(rageShaderBucketManager * pBucketManager)
{
	sm_pBucketManager = pBucketManager;
}

ModelMaterialPair::ModelMaterialPair(const rageShaderMaterial* pMtl, const mshMesh& obMesh)
: m_Locked(false)
{
//	Displayf("Constructing %p", this);
	LockClass();
	m_MeshNeedsUpdate = true;
	m_bIWantToDie = false;

	m_pMaterial		= NULL;
	m_pModel		= NULL;

	m_pDeleteMeModel    = NULL;
	m_pDeleteMeMaterial = NULL;

	m_WorldMatrix.Identity();
	UnlockClass();

	SetMesh(obMesh);
	SetMaterial(pMtl);

	// Add it to the Manager
	ModelMaterialPairManager::GetInstance()->AddModelMaterialPair(this);
}

ModelMaterialPair::~ModelMaterialPair()
{
	// Remove it from the Manager
	ModelMaterialPairManager::GetInstance()->RemoveModelMaterialPair(this);

	// Remove this data from the bucket manager
	sm_pBucketManager->DeleteData(GetBucketNumber(), (u32)this);

	LockClass();
//	Displayf("Destructing %p", this);

	if (m_pModel)
	{
		m_pModel->Release();
		m_pModel = NULL;
	}

	if (m_pDeleteMeModel)
	{
		m_pDeleteMeModel->Release();
		m_pDeleteMeModel = NULL;
	}

	delete m_pMaterial;
	m_pMaterial = NULL;

	delete m_pDeleteMeMaterial;
	m_pDeleteMeMaterial = NULL;
}

// Returns the bucket number of the material
int ModelMaterialPair::GetBucketNumber() const
{
	Assert(m_pMaterial && "Material has not yet been setup, so can't get BucketNumber.  UpdateShader sets up the material");
	return (m_pMaterial->GetBucketNumber() >= 0) ? m_pMaterial->GetBucketNumber() : 0;
}

// Function to initialize the model data
void ModelMaterialPair::SetMesh(const mshMesh & Mesh)
{	
	LockClass();
	m_Mesh = Mesh;
	m_Mesh.CleanModel();
	m_Mesh.ComputeTanBi(0);
	SetMeshNeedsUpdate(true);
	UnlockClass();
}

void	ModelMaterialPair::SetMaterial(const rageShaderMaterial* pMtl)
{
	LockClass();
	Assert(!m_pDeleteMeMaterial);
	m_pDeleteMeMaterial = m_pMaterial;
	m_pMaterial = new rageShaderMaterial();
	m_pMaterial->Copy(*pMtl);
	SetShaderNeedsUpdate(true);
	UnlockClass();
}

// Function to update the model data
void ModelMaterialPair::Update()
{
	LockClass();
	// Make sure that we currently are not locked
	if (m_pDeleteMeModel)
	{
		m_pDeleteMeModel->Release();
		m_pDeleteMeModel = NULL;
	}

	if (m_pDeleteMeMaterial)
	{
		delete m_pDeleteMeMaterial;
		m_pDeleteMeMaterial = NULL;
	}

	// Do I need to update my shader?
	if (m_ShaderNeedsUpdate)
	{		
		m_pMaterial->InitShader();

		m_ShaderNeedsUpdate = false;
	}
	
	// Do I need to update my mesh?
	if (m_MeshNeedsUpdate)
	{
		m_MeshNeedsUpdate = false;

		// Store the current model, for deletion later
		Assert(!m_pDeleteMeModel);
		m_pDeleteMeModel = m_pModel;

		// Create new model
		m_pModel = grmModel::Create(m_Mesh, 0, NULL, m_pMaterial->GetShaderGroup());
	}

	// If I have a shader and a model, update my buckets
	if (m_pModel && m_pMaterial->GetShader())
	{
		sm_pBucketManager->UpdateData(GetBucketNumber(), (u32)this, m_pMaterial->GetShader(), m_pModel, &m_WorldMatrix);
	}
	UnlockClass();
}


bool		ModelMaterialPair::UpdateMeshTransform(const Matrix34 & m34)
{	
	// If it isn't locked, update its matrix
	SetWorldMatrix(m34);
	return true;
}

void		ModelMaterialPair::WaitUntilNotLocked()
{
	while(m_Locked)
	{
		Sleep(100);
	}
}


void	ModelMaterialPair::DrawWireFrame()
{
	// Draw the m_Mesh
	grcColor3f(0.0f,0.0f,0.25f);
	for(int m=0; m<m_Mesh.GetMtlCount(); m++)
	{
		// For each material
		int iNoOfPrimsInMeshMaterial = m_Mesh.GetMtl(m).Prim.GetCount();
		for(int p=0; p<iNoOfPrimsInMeshMaterial; p++)
		{
			// For each primitive
			int iNoOfTrianglesInPrim = (m_Mesh.GetMtl(m).Prim[p].Idx.GetCount() / 3);
			for(int t=0; t<iNoOfTrianglesInPrim; t++)
			{
				// For each triangle
				grcBegin(drawLineStrip, 4);

				// Set the correct Transform
				grcViewport::SetCurrentWorldMtx(m_WorldMatrix);

				// For each vert in the triangle
				int iStartOfTri = (t * 3);
				for( int v=iStartOfTri; v<(iStartOfTri + 3); v++)
				{
					int iTriAngleVertexIndex = m_Mesh.GetMtl(m).Prim[p].Idx[v];
					Vector3 obVertexPos = m_Mesh.GetMtl(m).Verts[iTriAngleVertexIndex].Pos;
					grcVertex3f(obVertexPos);
				}

				// Close the line strip
				int iTriAngleVertexIndex = m_Mesh.GetMtl(m).Prim[p].Idx[iStartOfTri];
				Vector3 obVertexPos = m_Mesh.GetMtl(m).Verts[iTriAngleVertexIndex].Pos;
				grcVertex3f(obVertexPos);

				grcEnd();
			}
		}
	}
}
