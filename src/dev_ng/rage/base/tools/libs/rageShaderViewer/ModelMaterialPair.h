// 
// rageShaderViewer/ModelMaterialPair.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
//
#ifndef RAGE_SHADER_VIEWER_MODEL_DATA_H
#define RAGE_SHADER_VIEWER_MODEL_DATA_H
#pragma warning(disable: 4668)
#include <process.h>
#include <windows.h>
#pragma warning(error: 4668)

// #include "atl/string.h"
#include "mesh/mesh.h"
#include "rageShaderMaterial/rageShaderMaterial.h"

namespace rage {

class grmModel;
class rageShaderBucketManager;

class ModelMaterialPair
{
public:

	ModelMaterialPair(const rageShaderMaterial* pMtl, const mshMesh & obMesh);
	~ModelMaterialPair();

	// Accessors
//	bool	IsLocked()				const {return m_Locked;}
	bool	NeedsUpdate()			const {return m_MeshNeedsUpdate;}
	int		GetBucketNumber()		const;

	// Because of all the multithreading going on, I can not just die when I want to die, I have to wait until 
	// noone can see me
	bool	IWantToDie()			const	{return m_bIWantToDie;}
	void	MakeMeWantToDie() {m_bIWantToDie = true;}

	// Mesh functions
	void	SetMesh(const mshMesh & Mesh);

	// Material functions
	void	SetMaterial(const rageShaderMaterial* pMtl);

	// Transform functions
	void	SetWorldMatrix(const Matrix34 & m34)	{m_WorldMatrix = m34;}
	bool	UpdateMeshTransform(const Matrix34 & m34);

	// Update renderable data
	static void SetBucketManager(rageShaderBucketManager * pBucketManager);

	// Function to initialize the model data

	// Update the everything
	void Update();

	// Function to render the model data
	void Render();

	void DrawWireFrame();

private:
	// Locking for threading
	bool		m_Locked;
	void		LockClass() {WaitUntilNotLocked(); m_Locked = true;}
	void		UnlockClass() {m_Locked = false;}
	void		WaitUntilNotLocked();

	bool		m_MeshNeedsUpdate;
	bool		m_ShaderNeedsUpdate;
	bool		m_bIWantToDie;

	// Useable data
	mshMesh 					m_Mesh;
	rageShaderMaterial*			m_pMaterial;

	// Mesh functions
	bool	MeshNeedsUpdate()		const {return m_MeshNeedsUpdate;}
	void	SetMeshNeedsUpdate(bool b)				{m_MeshNeedsUpdate = b;}

	// Material functions
	bool	ShaderNeedsUpdate()		const {return m_ShaderNeedsUpdate;}
	void	SetShaderNeedsUpdate(bool b)			{m_ShaderNeedsUpdate = b;}

	// Renderable data
	Matrix34			m_WorldMatrix;
	grmModel *			m_pModel;
	

	// Delete me data
	grmModel *				m_pDeleteMeModel;
	rageShaderMaterial *	m_pDeleteMeMaterial;

	static rageShaderBucketManager * sm_pBucketManager;
};

} // end using namespace rage

#endif /* RAGE_SHADER_VIEWER_MODEL_DATA_H */
