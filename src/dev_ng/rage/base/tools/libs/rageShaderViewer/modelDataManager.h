// 
// rageShaderViewer/modelDataManager.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
//

#ifndef RAGE_SHADER_VIEWER_MODEL_DATA_MANAGER_H
#define RAGE_SHADER_VIEWER_MODEL_DATA_MANAGER_H

#include "vector/matrix34.h"
#include "atl/array.h"

namespace	rage
{

class	mshMesh;
class	rageShaderBucketManager;
class	modelData;

// An instance of this class works as the conduit between the two threads.
// One thread is controlled by the user in Maya and the other is controlled by the view itself
class	modelDataManager
{
public:
	modelDataManager();
	~modelDataManager();

	// Instance accessor
	static modelDataManager * GetInstance() {return sm_pInstance;}

	// Accessors
	bool		GetUpdating() const {return m_bUpdating;}
	bool		IsDataValid(int nIndex) const {return m_apModelData[nIndex] != NULL;}
	bool		IsLocked(int nIndex) const;
	rageShaderBucketManager * GetBucketManager() {return m_pBucketManager;}

	// Update renderable data
	bool		UpdateMeshTransform(const int nIndex, const Matrix34 & m34);
	bool		SetMeshAtIndex(const int nIndex, const mshMesh & Mesh);
	bool		UpdateShaders(const int nIndex, const rageShaderMaterial* pMtl);
	void		DeleteData(const int nIndex);

	// Update shader
	void		RefreshShaders();

	// Base functionality - The viewer always goes through these functions
	void		Init(const char * szShaderBucketFileName);
	void		Update();
	void		Draw();

private:

	// Helper function to allocate a modelData
	int AllocateData(const int nIndex);

	int							m_nMaxDataIndex;
	atArray <modelData *>		m_apModelData;

	rageShaderBucketManager *	m_pBucketManager;

	bool						m_bUpdating;
	bool						m_boRefreshShaders;

	static modelDataManager * sm_pInstance;
};

} // end using namespace rage

#endif
