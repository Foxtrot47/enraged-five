// 
// rageShaderViewer/modelData.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
//

#include "mesh/mesh.h"
#include "mesh/serialize.h"
#include "file/asset.h"
#include "grcore/state.h"
#include "grmodel/shaderfx.h"
#include "grmodel/model.h"

#include "rageShaderBucket/rageShaderBucketManager.h"

#include "modelData.h"

using namespace rage;

rageShaderBucketManager * modelData::sm_pBucketManager = NULL;

void modelData::SetBucketManager(rageShaderBucketManager * pBucketManager)
{
	sm_pBucketManager = pBucketManager;
}

modelData::modelData()
{
	m_Locked = true;

	m_NeedsReset = false;
	m_MeshNeedsUpdate = false;
	m_ShadersNeedsUpdate = false;

	m_pMaterial		= NULL;
	m_pModel		= NULL;
	m_pShader		= NULL;

	m_pDeleteMeModel  = NULL;
	m_pDeleteMeShader = NULL;

	m_WorldMatrix.Identity();

	m_Locked = false;
}

modelData::~modelData()
{
	m_Locked = true;

	// Remove this data from the bucket manager
	sm_pBucketManager->DeleteData(GetBucketNumber(), (u32)this);

	if (m_pModel)
	{
		m_pModel->Release();
		m_pModel = NULL;
	}

	if (m_pShader)
	{
		delete m_pShader;
		m_pShader = NULL;
	}

	if (m_pDeleteMeModel)
	{
		m_pDeleteMeModel->Release();
		m_pDeleteMeModel = NULL;
	}

	if (m_pDeleteMeShader)
	{
		delete m_pDeleteMeShader;
		m_pDeleteMeShader = NULL;
	}

	delete m_pMaterial;
	m_pMaterial = NULL;

	m_Locked = false;
}

// Returns the bucket number of the material
int modelData::GetBucketNumber() const
{
	Assert(m_pMaterial && "Material has not yet been setup, so can't get BucketNumber.  UpdateShader sets up the material");
	return (m_pMaterial->GetBucketNumber() >= 0) ? m_pMaterial->GetBucketNumber() : 0;
}

// Function to initialize the model data
void modelData::SetMesh(const mshMesh & Mesh)
{	
	m_Locked = true;

	m_Mesh = Mesh;

	m_Mesh.CleanModel();
	m_Mesh.ComputeTanBi(0);

	m_MeshNeedsUpdate = true;

	m_Locked = false;
}

void modelData::UpdateShaders(const rageShaderMaterial* pMtl)
{
	m_Locked = true;

	if(!pMtl)
	{
		// Given a crap shader :(
		Displayf("Given a crap shader");
	}
	Assert(pMtl && "Material has not yet been setup, so can't get BucketNumber.  UpdateShader sets up the material");
	delete m_pMaterial;
	m_pMaterial = new rageShaderMaterial();
	m_pMaterial->Copy(*pMtl);

	m_ShadersNeedsUpdate = true;

	m_Locked = false;
}

// Function to update the model data
void modelData::Update()
{
	// Make sure that we currently are not locked
	if (m_Locked)
	{
		return;
	}

	m_Locked = true;

	if (m_pDeleteMeModel)
	{
		m_pDeleteMeModel->Release();
		m_pDeleteMeModel = NULL;
	}

	if (m_pDeleteMeShader)
	{
		delete m_pDeleteMeShader;
		m_pDeleteMeShader = NULL;
	}
	
	// Do I need to update my mesh?
	if (m_MeshNeedsUpdate)
	{
		m_MeshNeedsUpdate = false;

		// Store the current model, for deletion later
		Assert(!m_pDeleteMeModel);
		m_pDeleteMeModel = m_pModel;

		// Create new model
		m_pModel = grmModel::Create(m_Mesh, 0, NULL);
	}

	// Do I need to update my shader?
	if (m_ShadersNeedsUpdate)
	{
		m_ShadersNeedsUpdate = false;

		// Store the old shader, for deletion later
		Assert(!m_pDeleteMeShader);
		m_pDeleteMeShader = m_pShader;

		// Create new shader
		char szShaderFullPathNameBase[256];
		fiAssetManager::BaseName(szShaderFullPathNameBase, sizeof(szShaderFullPathNameBase), m_pMaterial->GetShaderFullPathName());

		m_pShader = new grmShaderFx;
		if (!m_pShader->Load(szShaderFullPathNameBase, 0, 0, 0, NULL, NULL, GetBucketNumber(), m_pMaterial))
		{
			delete m_pShader;
			m_pShader = NULL;
		}
	}

	// If I have a shader and a model, update my buckets
	if (m_pShader && m_pModel)
	{
		sm_pBucketManager->UpdateData(GetBucketNumber(), (u32)this, m_pShader, m_pModel, &m_WorldMatrix);
	}

	m_Locked = false;
}

