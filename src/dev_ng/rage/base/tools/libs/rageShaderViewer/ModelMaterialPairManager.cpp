// 
// rageShaderViewer/ModelMaterialPairManager.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
//

#include "input/input.h"
// #include "grcore/im.h"
#include "grcore/state.h"
#include "grmodel/shaderfx.h"
#include "atl/string.h"

#include "rageShaderBucket/rageShaderBucketManager.h"

// #include "rageShaderViewerLighting/lightDataManager.h"

#include "ModelMaterialPair.h"
#include "ModelMaterialPairManager.h"
#include "rageShaderViewer.h"

using namespace rage;

#define MAX_RENDER_BUCKETS 3

ModelMaterialPairManager * ModelMaterialPairManager::sm_pInstance = NULL;

ModelMaterialPairManager::ModelMaterialPairManager()
{
	m_bUpdating			= false;
	sm_pInstance		= this;

	m_apModelMaterialPairs.clear();

	// Init shader buckets
	atString sShaderBucketFileName("");
	//initModuleSettings::GetModuleSetting(("ShaderBucketFileName"), sShaderBucketFileName);
	if (sShaderBucketFileName.GetLength() == 0)
	{
		Warningf("Could not find the \"ShaderBucketFileName\" parameter in the configuration.  This functionality has been deprecated with the removal of Module Settings.");
	}
	rageShaderBucketManager::InitShaderBucketLibrary();
	m_pBucketManager = new rageShaderBucketManager;
	m_pBucketManager->LoadFromXML(sShaderBucketFileName);
	m_pBucketManager->Validate();
	ModelMaterialPair::SetBucketManager(m_pBucketManager);

	m_bUpdating = false;
}

ModelMaterialPairManager::~ModelMaterialPairManager()
{
	sm_pInstance = NULL;

	for (unsigned i=0; i<m_apModelMaterialPairs.size(); i++)
	{
		// Displayf("%p Deleting the model data at index %d because my parent ModelMaterialPairManager is being killed off", this, i);
		delete m_apModelMaterialPairs[i];
		m_apModelMaterialPairs[i] = NULL;
	}
	m_apModelMaterialPairs.clear();

	delete m_pBucketManager;
	m_pBucketManager = NULL;
	ModelMaterialPair::SetBucketManager(NULL);

	rageShaderBucketManager::ShutdownShaderBucketLibrary();

	INPUT.End();
}

void	ModelMaterialPairManager::Update()
{
	m_bUpdating = true;


	// Delete the data we want to
	//for (int index=m_nMaxDataIndex; index>=0; index--)
	//{
	//	if (m_apModelMaterialPairs[index])
	//	{
	//		if (m_apModelMaterialPairs[index]->NeedsReset())
	//		{
	//			delete m_apModelMaterialPairs[index];
	//			m_apModelMaterialPairs[index] = NULL;

	//			// Make sure we decrement the max if we need to
	//			if (m_nMaxDataIndex==index)
	//			{
	//				m_nMaxDataIndex--;
	//			}
	//		}
	//	}
	//}

	// Update modelMaterialPairs
	for (unsigned m=0; m<m_apModelMaterialPairs.size(); m++)
	{
		if(m_apModelMaterialPairs[m]->IWantToDie())
		{
			// My child wants to kill itself, so help it on its way
			// Displayf("Killing m_apModelMaterialPairs[%d] = %p", m, m_apModelMaterialPairs[m]);
			delete m_apModelMaterialPairs[m];
			m--;
		}
		else
		{
			// Displayf("Updating m_apModelMaterialPairs[%d] = %p", m, m_apModelMaterialPairs[m]);
			m_apModelMaterialPairs[m]->Update();
		}
	}

	m_bUpdating = false;
	
}

void	ModelMaterialPairManager::Draw(const float fTimeOfDay)
{
	// Lock all the data
	//for (int i=0; i<=m_nMaxDataIndex; i++)
	//{
	//	if (m_apModelMaterialPairs[i])
	//	{
	//		m_apModelMaterialPairs[i]->SetLocked(true);
	//	}
	//}

	// Reset all render states
	grcState::Default();
	grcLightState::SetEnabled(true);

	// Draw everything
	m_pBucketManager->SetGlobalTimeOfDay(fTimeOfDay);
	m_pBucketManager->Draw();

	// Unlock all the data
	//for (int i=0; i<=m_nMaxDataIndex; i++)
	//{
	//	if (m_apModelMaterialPairs[i])
	//	{
	//		m_apModelMaterialPairs[i]->SetLocked(false);
	//	}
	//}
}


void	ModelMaterialPairManager::AddModelMaterialPair(ModelMaterialPair * pobModelMaterialPair)
{
	//int iNoOfModelMaterialPairs = m_apModelMaterialPairs.size();
	//Displayf("iNoOfModelMaterialPairs = %d", iNoOfModelMaterialPairs);
	//Displayf("Adding %p to list", pobModelMaterialPair);
	m_apModelMaterialPairs.push_back(pobModelMaterialPair);

	// Make sure the viewer is up
	rageShaderViewer::GetInstance()->Open();
}

void	ModelMaterialPairManager::RemoveModelMaterialPair(const ModelMaterialPair * pobModelMaterialPair)
{
	//int iNoOfModelMaterialPairs = m_apModelMaterialPairs.size();
	//Displayf("iNoOfModelMaterialPairs = %d", iNoOfModelMaterialPairs);
	//Displayf("Removing %p from list", pobModelMaterialPair);
	// Find the given ModelMaterialPair in my vector of ModelMaterialPair
	for (unsigned i=0; i<m_apModelMaterialPairs.size(); i++)
	{
		if(pobModelMaterialPair == m_apModelMaterialPairs[i])
		{
			// Displayf("%p Deleting the model data at index %d because my parent ModelMaterialPairManager is being killed off", this, i);
			// Displayf("Erasing %p from list", pobModelMaterialPair);
			m_apModelMaterialPairs.erase(m_apModelMaterialPairs.begin() + i);
			return;
		}
	}
}

ModelMaterialPairManager* ModelMaterialPairManager::GetInstance()
{
	if(!sm_pInstance)
	{
		sm_pInstance = new ModelMaterialPairManager();
	}
	return sm_pInstance;
}

void	ModelMaterialPairManager::DrawWireFrame()
{
	for (unsigned i=0; i<m_apModelMaterialPairs.size(); i++)
	{
		m_apModelMaterialPairs[i]->DrawWireFrame();
	}
}
