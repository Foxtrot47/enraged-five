#ifndef __CONFIGUTIL_PATH_H__
#define __CONFIGUTIL_PATH_H__

//
// filename:	Path.h
// description:	
//

// --- Include Files ------------------------------------------------------------

// Local Headers
#include "configUtil.h"

// C++ Stdlib Header Files
#include <cstdarg>
#include <string>
#include <vector>

// --- Defines ------------------------------------------------------------------

BEGIN_CONFIGUTIL_NS

// --- Constants ----------------------------------------------------------------

// --- Structure/Class Definitions ----------------------------------------------

/**
 * @brief Filesystem path and filename utility functions.
 *
 * Note: DO NOT use the std::string versions in DLLs.
 */
class Path
{
public:
	/**
	 * @brief Combine multiple directory/file parts to form a single path.
	 *
	 * @param dst destination buffer.
	 * @param size size of destination buffer.
	 * @param count number of paths strings to combine.
	 * @param valist paths strings.
	 */
	static void combine( TCHAR* dst, size_t size, size_t count, ... );

	/**
	 * @brief Normalise a path.
	 *
	 * @param dst destination buffer.
	 * @param size size of destination buffer.
	 * @param src source buffer.
	 */
	static void normalise( TCHAR* dst, size_t size, const TCHAR* src ); 

	/**
	 * @brief Normalise a path to platform-native format.
	 *
	 * @param dst destination buffer.
	 * @param size size of destination buffer.
	 * @param src source buffer.
	 */
	static void platform_native( TCHAR* dst, size_t size, const TCHAR* src );

	/**
	 * @brief Split a path string into path components.
	 *
	 * @param parts vector of result buffers.
	 * @param src source buffer.
	 */
	static bool split( std::vector<TCHAR*>& parts, const TCHAR* src );

	/**
	 * @brief Clear a split() parts buffer.
	 *
	 * @param parts vector of result buffers.
	 */
	static void split_buffer_clear( std::vector<TCHAR*>& parts );

	/**
	 * @brief Return directory.
	 *
	 * @param dst destination buffer.
	 * @param size size of destination buffer.
	 * @param src source buffer.
	 */
	static void get_directory( TCHAR* dst, size_t size, const TCHAR* src );

	/**
	 * @brief Return filename (without extension or directory) from path string.
	 *
	 * @param dst destination buffer.
	 * @param size size of destination buffer.
	 * @param src source buffer.
	 */
	static void get_basename( TCHAR* dst, size_t size, const TCHAR* src );

	/**
	 * @brief Return true iff character is valid directory separator.
	 */
	static bool is_directory_separator( TCHAR c );

	/**
	 * @brief Return file size of file (0 if invalid).
	 */
	static size_t get_file_size( const TCHAR* path );

	/**
	 * @brief Return true iff the path is a directory.
	 */
	static bool is_directory( const TCHAR* path );

	/**
	 * @brief Return true iff the path is a file.
	 */
	static bool is_file( const TCHAR* path );
private:

	Path( );				//!< Hide default constructor
	Path( Path& path );	//!< Hide copy constructor
};

END_CONFIGUTIL_NS

#endif // __CONFIGUTIL_PATH_H__
