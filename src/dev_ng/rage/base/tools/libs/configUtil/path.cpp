//
// filename:	RSNConfig.cpp
// description:	RSNConfig Class Implementation
//
// references:
//	http://www.codeproject.com/KB/files/SplitPath.aspx
//

// --- Include Files ------------------------------------------------------------

// Local Header Files
#include "Path.h"

// Boost Header Files
#pragma warning( push )
#pragma warning( disable : 4512 )
#include "boost/algorithm/string/replace.hpp"
#include "boost/algorithm/string/case_conv.hpp"
#pragma warning( pop )

// C++ Stdlib Header Files
#include <cstdarg>
#include <cstdio>
#include <vector>
#include <cstring>
#include <ctype.h>
using namespace std;

// Windows SDK Header Files
#include <Windows.h>

// --- Defines ------------------------------------------------------------------
// None

BEGIN_CONFIGUTIL_NS

// --- Constants ----------------------------------------------------------------

static const int NUM_DIRSEPS = 2;
static const TCHAR DIRECTORY_SEPARATOR = '/';
static const TCHAR DIRECTORY_SEPARATOR_STR[2] = _T("/");
static const TCHAR DIRECTORY_SEPARATORS[NUM_DIRSEPS] = { '\\', '/' }; 

// --- Structure/Class Definitions ----------------------------------------------

// --- Globals ------------------------------------------------------------------

// --- Static Globals -----------------------------------------------------------

// --- Static class members -----------------------------------------------------

// --- Code ---------------------------------------------------------------------

#if 0
void 
Path::combine( TCHAR* dst, size_t size, const TCHAR* p1, const TCHAR* p2 )
{
	assert( dst != p1 );
	assert( dst != p2 );
#if UNICODE	
	wcsncat_s( dst, size, p1, wcslen( p1 ) );
	wcsncat_s( dst, size, DIRECTORY_SEPARATOR_STR, wcslen(DIRECTORY_SEPARATOR_STR) );
	wcsncat_s( dst, size, p2, wcslen( p2 ) );
#else
	strcat_s( dst, size, p1, strlen( p1 ) );
	strcat_s( dst, size, DIRECTORY_SEPARATOR_STR );
	strcat_s( dst, size, p2, strlen( p2 ) );
#endif // UNICODE
}
#endif 

void
Path::combine( TCHAR* dst, size_t size, size_t count, ... )
{
	va_list argp;
	va_start( argp, size );

#if UNICODE
	if ( wcslen( dst ) > 0 )
		wcscat_s( dst, size, DIRECTORY_SEPARATOR_STR );
#else
	if ( strlen( dst ) > 0 )
		strcat_s( dst, size, DIRECTORY_SEPARATOR_STR );
#endif // UNICODE

	TCHAR* pArg = va_arg( argp, TCHAR* );
	for ( size_t i = 0; i < count; ++i )
	{
		pArg = va_arg( argp, TCHAR* );
		assert( pArg != dst );

#if UNICODE
		wcscat_s( dst, size, pArg );
		if ( i != (count-1) )
			wcscat_s( dst, size, DIRECTORY_SEPARATOR_STR );
#else
		strcat_s( dst, size, pArg );
		if ( i != (count-1) )
			strcat_s( dst, size, DIRECTORY_SEPARATOR_STR );
#endif // UNICODE
	}

	va_end( argp );
}

void 
Path::normalise( TCHAR* dst, size_t size, const TCHAR* src )
{
#if UNICODE
	std::wstring s( src );
#else
	std::string s( src );
#endif // UNICODE

	boost::algorithm::to_lower( s );
	boost::replace_all( s, _T("\\"), _T("/") );
	boost::replace_all( s, _T("//"), _T("/") );

#if UNICODE
	wcscpy_s( dst, size, s.c_str() );
#else
	strcpy_s( dst, size, s.c_str() );
#endif // UNICODE
}

void 
Path::platform_native( TCHAR* dst, size_t size, const TCHAR* src )
{
#if _WIN32
#if UNICODE
	std::wstring s( src );
#else
	std::string s( src );
#endif // UNICODE

	boost::algorithm::to_lower( s );
	boost::replace_all( s, _T("/"), _T("\\") );

#if UNICODE
	wcscpy_s( dst, size, s.c_str() );
#else
	strcpy_s( dst, size, s.c_str() );
#endif // UNICODE
#else
	normalise( dst, size, src );
#endif // _WIN32
}

#pragma warning( push )
#pragma warning( disable : 4996 )

bool 
Path::split( vector<TCHAR*>& parts, const TCHAR* src )
{
#if UNICODE
	TCHAR* srccpy = const_cast<TCHAR*>( src );
	TCHAR* pch = wcstok( srccpy, _T("\\/") );
#else
	TCHAR* srccpy = const_cast<TCHAR*>( src );
	TCHAR* pch = strtok( srccpy, _T("\\/") );
#endif // UNICODE
	while ( NULL != pch )
	{
		// Copy new token into output buffer.
#if UNICODE
		size_t lenpch = wcslen( pch );
#else
		size_t lenpch = strlen( pch );
#endif // UNICODE
		TCHAR* buf = new TCHAR[lenpch+1];
		memset( buf, 0, sizeof( TCHAR ) * lenpch+1 );

#if UNICODE
		wcsncpy( buf, pch, lenpch+1 );
		parts.insert( parts.end(), buf );
		pch = wcstok( NULL, _T("\\/") );
#else
		strncpy( buf, pch, lenpch+1 );
		parts.insert( parts.end(), buf );
		pch = strtok( NULL, _T("\\/") );
#endif // UNICODE
	}

	return ( parts.size() > 0 );
}

#pragma warning( pop )

void 
Path::split_buffer_clear( std::vector<TCHAR*>& parts )
{
	// Clean up buffers allocated during split().
	for ( size_t n = 0; n < parts.size()-1; ++n )
	{
		delete [] parts[n];
		parts[n] = NULL;
	}
	parts.clear( );
}

void 
Path::get_directory( TCHAR* dst, size_t size, const TCHAR* src )
{
	std::vector<TCHAR*> parts;
	
	if ( split( parts, src ) )
	{
		// Combine all but the last part.
		for ( size_t n = 0; n < parts.size()-1; ++n )
			combine( dst, size, 1, parts[n] );
	}
	else
	{
#if UNICODE
		wcsncpy_s( dst, size, _T(""), _TRUNCATE );
#else
		strncpy_s( dst, size, _T(""), _TRUNCATE );
#endif // UNICODE
	}

	split_buffer_clear( parts );
}

void 
Path::get_basename( TCHAR* dst, size_t size, const TCHAR* src )
{
	TCHAR dir[MAX_PATH] = {0};
	get_directory( dir, MAX_PATH, src );

	
	
	size_t pathlen = 0L;
	size_t dirlen = 0L;
#if UNICODE
	pathlen = wcslen( src );
	dirlen = wcslen( dir );
#else
	pathlen = strlen( src );
	dirlen = strlen( dir );
#endif // UNICODE

	if ( dirlen < pathlen )
	{
#if UNICODE
		wcsncpy_s( dst, size, src+dirlen, _TRUNCATE );
#else
		strncpy_s( dst, size, src+dirlen, _TRUNCATE );
#endif // UNICODE
	}
	else
	{
#if UNICODE
		wcsncpy_s( dst, size, _T(""), _TRUNCATE );
#else
		strncpy_s( dst, size, _T(""), _TRUNCATE );
#endif // UNICODE
	}
}

bool 
Path::is_directory_separator( TCHAR c )
{
	for ( int i = 0; i < NUM_DIRSEPS; ++i )
	{
		if ( DIRECTORY_SEPARATORS[i] == c )
			return ( true );
	}

	return ( false );
}

size_t 
Path::get_file_size( const TCHAR* path )
{
	WIN32_FIND_DATA fd;
	memset( &fd, 0, sizeof( WIN32_FIND_DATA ) );
	HANDLE hFile = FindFirstFile( path, &fd );
	if ( INVALID_HANDLE_VALUE != hFile )
	{
		FindClose( hFile );
		return ( fd.nFileSizeLow );
	}
	return ( 0L );
}

bool 
Path::is_directory( const TCHAR* path )
{
	WIN32_FIND_DATA fd;
	memset( &fd, 0, sizeof( WIN32_FIND_DATA ) );
	HANDLE hFile = FindFirstFile( path, &fd );
	if ( INVALID_HANDLE_VALUE != hFile )
	{
		FindClose( hFile );
		return ( FILE_ATTRIBUTE_DIRECTORY == ( fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY ) );
	}
	return ( false );
}

bool 
Path::is_file( const TCHAR* path )
{
	WIN32_FIND_DATA fd;
	memset( &fd, 0, sizeof( WIN32_FIND_DATA ) );
	HANDLE hFile = FindFirstFile( path, &fd );
	if ( INVALID_HANDLE_VALUE != hFile )
	{
		FindClose( hFile );
		return ( fd.dwFileAttributes & !FILE_ATTRIBUTE_DIRECTORY );
	}
	return ( false );
}

END_CONFIGUTIL_NS
