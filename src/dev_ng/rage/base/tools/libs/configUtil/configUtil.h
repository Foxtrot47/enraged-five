#ifndef __CONFIGUTIL_CONFIGUTIL_H__
#define __CONFIGUTIL_CONFIGUTIL_H__

//
// filename:	configUtil.h
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		21 December 2009
// description:	Library main header.
//

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

// --- Include Files ------------------------------------------------------------
#include <cstdlib>
#include <string>
#include <stdexcept>
#include <tchar.h>

// --- Defines ------------------------------------------------------------------
#define BEGIN_CONFIGUTIL_NS namespace configUtil {
#define END_CONFIGUTIL_NS }

#ifdef UNICODE
typedef wchar_t TCHAR;
#else
typedef char TCHAR;
#endif // UNICODE

// --- Classes ------------------------------------------------------------------
// None

// --- Functions ----------------------------------------------------------------
BEGIN_CONFIGUTIL_NS

/**
 * @brief Convert TCHAR string to ANSI string.
 */
#ifdef UNICODE
inline void
TCharToAnsi( char* output, size_t size, const TCHAR* s )
{
	size_t convertedChars = 0L;
	wcstombs_s( &convertedChars, output, size, s, _TRUNCATE );
}
#else
inline void
TCharToAnsi( char* output, size_t size, const char* s )
{
	strcpy_s( output, size, s);
}
#endif // UNICODE

/**
 * @brief Convert ANSI string to TCHAR string.
 */
#ifdef UNICODE
inline void
AnsiToTChar( TCHAR* output, size_t size, const char* s )
{
	size_t convertedChars = 0L;
	mbstowcs_s( &convertedChars, output, size, s, _TRUNCATE );
}
#else
inline void
AnsiToTChar( char* output, size_t size, const char* s )
{
	strcpy_s( output, size, s );
}
#endif // UNICODE

END_CONFIGUTIL_NS

#endif // __CONFIGUTIL_CONFIGUTIL_H__
