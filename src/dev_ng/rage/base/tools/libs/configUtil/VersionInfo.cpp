//
// filename:	VersionInfo.cpp
// description:	
//
// references:
//

#include "VersionInfo.h"

BEGIN_CONFIGUTIL_NS

VersionInfo::VersionInfo( const TCHAR* module )
	: m_FixedFileInfo( NULL )
	, m_pData( NULL )
{
	WORD *pBuff;
	UINT Size;
	TCHAR szSubBlock[50];
	DWORD handle = 0;
	DWORD size = GetFileVersionInfoSize( module, &handle );
	
	if ( 0 != size ) 
	{
		m_pData = new TCHAR[size];
		if ( NULL != m_pData ) 
		{
			GetFileVersionInfo( module, handle, size, m_pData );
			// get fixed file info
			VerQueryValue( m_pData, _T("\\"), (void**) &pBuff, &Size );
			m_FixedFileInfo = (VS_FIXEDFILEINFO*) pBuff;

			// Get Translation information (first one)
			_tcscpy_s( szSubBlock, 50, _T("\\VarFileInfo\\Translation") );
			VerQueryValue( m_pData, szSubBlock, (void**) &pBuff, &Size);
			m_Translation = pBuff[1] + ((DWORD)pBuff[0] << 16);

			m_Translation = 0x040904E4; // can use this to force English etc.
		}
	} 
}

VersionInfo::~VersionInfo( )
{
	if ( NULL != m_pData )
		delete[] m_pData;
	// m_FixedVersionInfo does not alloc; typed pointer into m_pData.
}

void
VersionInfo::QueryString( TCHAR* dest, size_t size, const TCHAR* value ) const
{
	UINT nSize;
	TCHAR* pBuff;
	TCHAR szName[MAX_PATH];
	memset( dest, 0, sizeof( TCHAR ) * size );
#if UNICODE
	swprintf( szName, MAX_PATH, _T("\\StringFileInfo\\%08lX\\%s"),
		m_Translation, value );
#else
	sprintf_s( szName, MAX_PATH, _T("\\StringFileInfo\\%08lX\\%s"),
		m_Translation, value );
#endif // UNICODE
	if ( m_pData && VerQueryValue( m_pData, szName, (void**)&pBuff, &nSize ) ) 
	{
#if UNICODE
		wcscpy_s( dest, size, pBuff );
#else
		strcpy_s( dest, size, pBuff );
#endif // UNICODE
	}
}

const VS_FIXEDFILEINFO& 
VersionInfo::QueryFixedFileInfo( ) const
{
	return ( *m_FixedFileInfo );
}

END_CONFIGUTIL_NS
