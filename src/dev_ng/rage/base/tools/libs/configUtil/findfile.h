#ifndef __CONFIGUTIL_FINDFILE_H__
#define __CONFIGUTIL_FINDFILE_H__

//
// filename:	FindFile.h
// description:	
//

// --- Include Files ------------------------------------------------------------

// Local Headers
#include "configUtil/configUtil.h"

// STL Headers
#include <string>
#include <vector>

BEGIN_CONFIGUTIL_NS

// --- Defines ------------------------------------------------------------------

// --- Constants ----------------------------------------------------------------

// --- Structure/Class Definitions ----------------------------------------------

//!< 
typedef std::vector<TCHAR*> tFindFileList;
//!< 
typedef tFindFileList::iterator tFindFileListIter;
//!< 
typedef tFindFileList::const_iterator tFindFileListConstIter;

// --- Globals ------------------------------------------------------------------

/**
 * @brief Find files in a filesystem path (optionally recursive) based on a file
 * filter.
 */
bool FindFiles( tFindFileList& output, const TCHAR* path, const TCHAR* filter, bool recurse );

END_CONFIGUTIL_NS

#endif // __CONFIGUTIL_FINDFILE_H__
