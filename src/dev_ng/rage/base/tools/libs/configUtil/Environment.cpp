//
// filename:	Environment.cpp
// author:		David Muir
// date:		18 April 2008
// description:	Generic environment class implementation.
//

// --- Include Files ------------------------------------------------------------

// Local Headers Files
#include "configUtil/configUtil.h"
#include "configUtil/Environment.h"
#include "configUtil/path.h"

// STL Headers Files
#include <algorithm>

// Boost Headers Files
#pragma warning( push )
#pragma warning( disable : 4512 )
#include "boost/algorithm/string/replace.hpp"
#pragma warning( pop )


// Windows SDK headers
#include <Windows.h>

// --- Constants ----------------------------------------------------------------

// --- Defines ------------------------------------------------------------------
#define MAX_ENV_SIZE ( 512 )

// Core Environment
#define ENV_VAR_PROJECT			( _T("RS_PROJECT") )
#define ENV_VAR_PROJECTROOT		( _T("RS_PROJROOT") )
#define ENV_VAR_TOOLSROOT		( _T("RS_TOOLSROOT") )

// Secondary Environment (optional)
#define ENV_VAR_BUILDROOT		( _T("RS_BUILDROOT") )
#define ENV_VAR_SRCBRANCH		( _T("RS_SRCBRANCH") )
#define ENV_VAR_BUILDBRANCH		( _T("RS_BUILDBRANCH") )
#define ENV_VAR_TOOLSBIN		( _T("RS_TOOLSBIN") )
#define ENV_VAR_TOOLSCONFIG		( _T("RS_TOOLSCONFIG") )
#define ENV_VAR_TOOLSLIB		( _T("RS_TOOLSLIB") )
#define ENV_VAR_TOOLSRUBY		( _T("RS_TOOLSRUBY") )
#define ENV_VAR_TOOLSTEMP		( _T("RS_TOOLSTEMP") )

// --- Code ---------------------------------------------------------------------

namespace configUtil
{

Environment::Environment( )
	: m_pSymbolTable( NULL )
{
	m_vEnvStack.clear();

	// Create new stack level.
	push( );
}

Environment::~Environment( )
{
	// Clear the Symbol Table stack
	for ( tEnvStack::iterator itEnv = m_vEnvStack.begin();
		itEnv != m_vEnvStack.end();
		++itEnv )
	{
		if ( NULL != (*itEnv) )
		{
			(*itEnv)->clear();
			delete (*itEnv);
			(*itEnv) = NULL;
		}
	}
	m_vEnvStack.clear();
}

//PURPOSE
// Import the basic tools environment into this Environment object.
// This is an optional call but generally recommended.
//
// After import a new symbol table is popped onto the stack.
//
void
Environment::import_core_globals( )
{
	TCHAR project[MAX_ENV_SIZE];
	TCHAR projectRoot[MAX_ENV_SIZE]; 
	TCHAR toolsRoot[MAX_ENV_SIZE];
	GetEnvironmentVariable( ENV_VAR_PROJECT, project, MAX_ENV_SIZE );
	GetEnvironmentVariable( ENV_VAR_PROJECTROOT, projectRoot, MAX_ENV_SIZE );
	GetEnvironmentVariable( ENV_VAR_TOOLSROOT, toolsRoot, MAX_ENV_SIZE );
	
// DHM TODO read optional environment or initialise it to defaults.
// as done in globals.rb

	add( _T("project"), project );
	add( _T("projroot"), projectRoot );
	add( _T("toolsroot"), toolsRoot );
	push( );
}

//PURPOSE
// Import the secondary set of tools environment into this Environment object.
// This is an optional call but generally recomended.
//
// After import a new symbol table is popped onto the stack.
//
void
Environment::import_secondary_globals( )
{
	TCHAR toolsbin[MAX_ENV_SIZE] = {0};
	TCHAR toolsconfig[MAX_ENV_SIZE] = {0};
	TCHAR toolslib[MAX_ENV_SIZE] = {0};
	TCHAR toolsruby[MAX_ENV_SIZE] = {0};
	TCHAR toolstemp[MAX_ENV_SIZE] = {0};
	TCHAR toolscontent[MAX_ENV_SIZE] = {0};

	TCHAR toolsroot[MAX_PATH];
	lookup( _T("toolsroot"), toolsroot, MAX_PATH );

	if ( 0 == GetEnvironmentVariable( ENV_VAR_TOOLSBIN, toolsbin, MAX_ENV_SIZE ) )
		Path::combine( toolsbin, MAX_ENV_SIZE, 2L, toolsroot, _T("bin") );
	if ( 0 == GetEnvironmentVariable( ENV_VAR_TOOLSCONFIG, toolsconfig, MAX_ENV_SIZE ) )
		Path::combine( toolsconfig, MAX_ENV_SIZE, 2L, toolsroot, _T("etc") );
	if ( 0 == GetEnvironmentVariable( ENV_VAR_TOOLSLIB, toolslib, MAX_ENV_SIZE ) )
		Path::combine( toolslib, MAX_ENV_SIZE, 2L, toolsroot, _T("lib") );
	if ( 0 == GetEnvironmentVariable( ENV_VAR_TOOLSRUBY, toolsruby, MAX_ENV_SIZE ) )
		Path::combine( toolsruby, MAX_ENV_SIZE, 2L, toolsbin, _T("ruby/bin/ruby.exe") );
	if ( 0 == GetEnvironmentVariable( ENV_VAR_TOOLSTEMP, toolstemp, MAX_ENV_SIZE ) )
		Path::combine( toolstemp, MAX_ENV_SIZE, 2L, toolsroot, _T("tmp") );
	Path::combine( toolscontent, MAX_ENV_SIZE, 2L, toolsconfig, _T("content") );

	add( _T("toolsbin"), toolsbin );
	add( _T("toolsconfig"), toolsconfig );
	add( _T("toolslib"), toolslib );
	add( _T("toolsruby"), toolsruby );
	add( _T("toolstemp"), toolstemp );
	add( _T("content"), toolscontent );
	push( );
}

bool 
Environment::lookup( const TCHAR* varname, TCHAR* varvalue, size_t size ) const
{
	for ( tEnvStack::const_reverse_iterator itEnv = m_vEnvStack.rbegin();
		itEnv != m_vEnvStack.rend();
		++itEnv )
	{
		tSymbolTable::const_iterator itSym = (*itEnv)->find( varname );
		if ( (*itEnv)->end() != itSym )
		{
			const TCHAR* value = (*itSym).second.c_str();
#if UNICODE
			wcsncpy_s( varvalue, size, value, _TRUNCATE );
#else
			strncpy_s( varvalue, size, value, _TRUNCATE );
#endif // UNICODE
			return ( true );
		}
	}

	return ( false );
}

void
Environment::subst( TCHAR* output, size_t size, const TCHAR* input ) const
{
	STL_STRING temp_output = input;
	for ( tEnvStack::const_reverse_iterator itEnv = m_vEnvStack.rbegin();
		itEnv != m_vEnvStack.rend();
		++itEnv )
	{
		for ( tSymbolTable::const_iterator itSym = (*itEnv)->begin();
			itSym != (*itEnv)->end();
			++itSym )
		{
			STL_STRING realvarname = _T("$(") + (*itSym).first + _T(")");
			STL_STRING varvalue = (*itSym).second;
			temp_output = boost::replace_all_copy( temp_output, realvarname, varvalue );
		}
	}

#if UNICODE
	wcsncpy_s( output, size, temp_output.c_str(), _TRUNCATE );
#else
	strncpy_s( output, size, temp_output.c_str(), _TRUNCATE );
#endif // UNICODE
}

void 
Environment::subst( TCHAR* io, size_t size ) const
{	
	STL_STRING temp_io = io;
	for ( tEnvStack::const_reverse_iterator itEnv = m_vEnvStack.rbegin();
		itEnv != m_vEnvStack.rend();
		++itEnv )
	{
		for ( tSymbolTable::const_iterator itSym = (*itEnv)->begin();
			itSym != (*itEnv)->end();
			++itSym )
		{
			STL_STRING realvarname = _T("$(") + (*itSym).first + _T(")");
			STL_STRING varvalue = (*itSym).second;
			boost::replace_all( temp_io, realvarname, varvalue );
		}
	}

#if UNICODE
	wcsncpy_s( io, size, temp_io.c_str(), _TRUNCATE );
#else
	strncpy_s( io, size, temp_io.c_str(), _TRUNCATE );
#endif // UNICODE
}

void 
Environment::add( const TCHAR* varname, const TCHAR* varvalue )
{
	(*m_pSymbolTable)[varname] = varvalue;
}

void 
Environment::clear( )
{
	m_pSymbolTable->clear( );
}

size_t
Environment::size( ) const
{
	return ( m_pSymbolTable->size() );
}

void 
Environment::push( )
{
	tSymbolTable* pSymTbl = new tSymbolTable();

#if _DEBUG
	size_t size = m_vEnvStack.size();
#endif
	m_vEnvStack.push_back( pSymTbl );
	assert( m_vEnvStack.size() == ( size+1 ) );

	m_pSymbolTable = pSymTbl;
}

void 
Environment::pop( )
{
	// Don't pop last environment stack symbol table.
	if ( 1 == m_vEnvStack.size() )
		return;

	size_t nIndex = m_vEnvStack.size() - 1;
	tSymbolTable* pSymTbl = m_vEnvStack.at( nIndex );
	if ( NULL != pSymTbl )
	{
		delete pSymTbl;
		m_vEnvStack[nIndex] = NULL;
	}

	m_vEnvStack.pop_back( );
	m_pSymbolTable = m_vEnvStack.at( m_vEnvStack.size() - 1 );
}

} // configUtil namespace
