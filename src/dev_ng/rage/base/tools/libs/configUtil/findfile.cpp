//
// filename:	FindFile.cpp
// description:	
//

// --- Include Files ------------------------------------------------------------

// Local Headers
#include "configUtil/FindFile.h"
#include "configUtil/path.h"

// Platform SDK Headers
#include <Windows.h>

using namespace std;

// --- Defines ------------------------------------------------------------------

BEGIN_CONFIGUTIL_NS

// --- Constants ----------------------------------------------------------------

// --- Structure/Class Definitions ----------------------------------------------

// --- Globals ------------------------------------------------------------------

// --- Static Globals -----------------------------------------------------------

// --- Static class members -----------------------------------------------------

// --- Code ---------------------------------------------------------------------

bool 
FindFiles( tFindFileList& output, const TCHAR* path, const TCHAR* filter, bool recurse )
{
	WIN32_FIND_DATA ffd;
	HANDLE hFind;

	// Find files first...
	TCHAR searchfile[MAX_PATH] = {0};
	Path::combine( searchfile, MAX_PATH, 2L, path, filter );
	hFind = FindFirstFile( searchfile, &ffd );

	if ( INVALID_HANDLE_VALUE != hFind )
	{
		do 
		{
			if ( !( ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY ) )
			{
				output.push_back( ffd.cFileName );
			}
		} while( 0 != FindNextFile( hFind, &ffd ) );
		FindClose( hFind );
	}

	// Now recurse if requested...
	if ( recurse )
	{
		TCHAR searchdir[MAX_PATH] = {0};
		Path::combine( searchdir, MAX_PATH, 2L, path, _T("\\*") );
		hFind = FindFirstFile( searchdir, &ffd );
		if ( INVALID_HANDLE_VALUE != hFind )
		{
			do 
			{
				if ( ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY )
				{
					// Recuse into this directory...
					TCHAR recursedir[MAX_PATH] = {0};
					Path::combine( recursedir, MAX_PATH, 2L, path, ffd.cFileName );
					FindFiles( output, recursedir, filter, recurse );
				}
			} while( 0 != FindNextFile( hFind, &ffd ) );
		}
	}

	return ( output.size() > 0 );
}

END_CONFIGUTIL_NS
