#ifndef __CONFIGENVIRONMENT_ENVIRONMENT_H_
#define __CONFIGENVIRONMENT_ENVIRONMENT_H_

//
// filename:	Environment.h
// author:		David Muir
// date:		18 April 2008
// description:	Generic environment class interface
//

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

// --- Include Files ------------------------------------------------------------

// Local Header Files
#include "configUtil.h"

// C++ Stdlib Header Files
#include <map>
#include <string>
#include <vector>

// Boost Header Files
#include "boost/shared_ptr.hpp"
#include "boost/weak_ptr.hpp"

// --- Defines ------------------------------------------------------------------
#define VAR_TOOLSDRIVE		( _T("toolsdrive") )
#define VAR_TOOLSROOT		( _T("toolsroot") )
#define VAR_TOOLSPROJECT	( _T("toolsproject") )
#define VAR_TOOLSBIN		( _T("toolsbin") )
#define VAR_TOOLSCONFIG		( _T("toolsconfig") )
#define VAR_TOOLSLIB		( _T("toolslib") )
#define VAR_TOOLSRUBY		( _T("toolsruby") )
#define VAR_TOOLSTEMP		( _T("toolstemp") )

#ifdef UNICODE
typedef std::wstring STL_STRING;
#else
typedef std::string STL_STRING;
#endif // UNICODE

// --- Classes ------------------------------------------------------------------

BEGIN_CONFIGUTIL_NS

/**
 @brief The Environment class is an abstraction to allow environment variable 
		substitution.
*/
class Environment
{
public:
	Environment( );
	~Environment( );

	/**
	 * @brief Import core global environment.
	 */
	void import_core_globals( );
	
	/**
	 * @brief Import secondary global environment.
	 */
	void import_secondary_globals( );

	/**
	 * @brief Lookup the value of an environment variable.
	 */
	bool lookup( const TCHAR* varname, TCHAR* varvalue, size_t size ) const;
	void subst( TCHAR* output, size_t size, const TCHAR* input ) const;
	void subst( TCHAR* io, size_t size ) const;

	void add( const TCHAR* varname, const TCHAR* varvalue );
	void clear( );
	size_t size( ) const;

	void push( );
	void pop( );

protected:
	typedef std::map<STL_STRING, STL_STRING>	tSymbolTable;
	typedef std::vector<tSymbolTable*>			tEnvStack;

	tSymbolTable* m_pSymbolTable;
	tEnvStack m_vEnvStack;
};

// --- Globals ------------------------------------------------------------------

//! Shared-ownership pointer to a Environment object
typedef boost::shared_ptr<Environment> cEnvironmentSPtr;
//! Non-owning pointer to a Environment object (owned by shared-pointer)
typedef boost::weak_ptr<Environment> cEnvironmentWPtr;

END_CONFIGUTIL_NS

#endif // !__CONFIGENVIRONMENT_ENVIRONMENT_H_
