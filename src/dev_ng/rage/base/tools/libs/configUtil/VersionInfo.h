#ifndef __CONFIGUTIL_VERSIONINFO_H__
#define __CONFIGUTIL_VERSIONINFO_H__

//
// filename:	VersionInfo.cpp
// description:	
//
// references:
//

// Local Headers
#include "configUtil/configUtil.h"

// Windows SDK headers
#include <Windows.h>
#include <winver.h>

BEGIN_CONFIGUTIL_NS

/**
 * @brief Windows module version information utility class.
 */
class VersionInfo
{
public:
	VersionInfo( const TCHAR* module );
	~VersionInfo( );

	void QueryString( TCHAR* dest, size_t size, const TCHAR* value ) const;
	const VS_FIXEDFILEINFO& QueryFixedFileInfo( ) const;

private:
	const VS_FIXEDFILEINFO* m_FixedFileInfo;
	TCHAR*					m_pData;
	DWORD					m_Translation;
};

END_CONFIGUTIL_NS

#endif // __CONFIGUTIL_VERSIONINFO_H__
