#ifndef __CONFIGUTIL_SCOPED_MUTEX_H__
#define __CONFIGUTIL_SCOPED_MUTEX_H__

//
// filename:	scoped_mutex.h
// description:	
//

// --- Include Files ------------------------------------------------------------

// Local Headers
#include "configUtil.h"

// Windows SDK Headers
#pragma warning( push )
#pragma warning( disable : 4668 )
#pragma warning( disable : 4005 )
#include <Windows.h>
#undef STRICT
#pragma warning( pop )

// C++ Stdlib Header Files
#include <cstdarg>
#include <string>
#include <vector>

// --- Defines ------------------------------------------------------------------

BEGIN_CONFIGUTIL_NS

// --- Constants ----------------------------------------------------------------

// --- Structure/Class Definitions ----------------------------------------------

/**
 * @brief Scoped mutex object; wrapping CRITICAL_SECTION.
 */
class ScopedMutex
{
public:
	typedef CRITICAL_SECTION Mutex;
	typedef LPCRITICAL_SECTION LPMutex;

	ScopedMutex( LPMutex mutex )
	{
		EnterCriticalSection( mutex );
		_pMutex = mutex;
	}

	~ScopedMutex( )
	{
		LeaveCriticalSection( _pMutex );
	}
private:
	ScopedMutex( );
	ScopedMutex( const ScopedMutex& copy );
	ScopedMutex& operator=( ScopedMutex& copy );

	LPMutex _pMutex;
};

END_CONFIGUTIL_NS

#endif // __CONFIGUTIL_SCOPED_MUTEX_H__
