﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RSG.Platform;

namespace RSG.Platform.UnitTest
{
    [TestClass]
    public class FileTypeTests
    {
        /// <summary>
        /// 
        /// </summary>
        [TestMethod]
        public void TestFileTypeUtils()
        {
            String path1 = @"x:\game\asset\test.idr.zip";
            String path2 = @"x:\game\asset\test.itd";
            String path3 = @"x:\game\asset\test.rpf";
            String path4 = @"x:\game\asset\test.zip";

            FileType ft1a = FileTypeUtils.ConvertExtensionToFileType(Path.GetExtension(path1));
            FileType ft1b = FileTypeUtils.ConvertExtensionToFileType(
                Path.GetExtension(Path.GetFileNameWithoutExtension(path1)));
            FileType ft2 = FileTypeUtils.ConvertExtensionToFileType(Path.GetExtension(path2));
            FileType ft3 = FileTypeUtils.ConvertExtensionToFileType(Path.GetExtension(path3));
            FileType ft4 = FileTypeUtils.ConvertExtensionToFileType(Path.GetExtension(path4));

            Assert.AreEqual(FileType.ZipArchive, ft1a);
            Assert.AreEqual(FileType.Drawable, ft1b);
            Assert.AreEqual(FileType.TextureDictionary, ft2);
            Assert.AreEqual(FileType.RagePackFile, ft3);
            Assert.AreEqual(FileType.ZipArchive, ft4);
        }
    }

} // RSG.Platform.UnitTest namespace
