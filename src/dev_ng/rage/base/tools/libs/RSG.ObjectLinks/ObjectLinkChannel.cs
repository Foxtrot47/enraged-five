﻿using RSG.Base.Editor;
using RSG.Metadata.Data;
using RSG.Metadata.Parser;
using System;
using System.Collections.Generic;

namespace RSG.ObjectLinks
{
    /// <summary>
    /// Class to encapsulate hierarchy info for one leaf, i.e. children and one parent
    /// Channels allow linking for different purposes, e.g. LODs and DCC helpers
    /// </summary>
    public class ObjectLinkChannel : 
        IObjectLinkChannel, 
        ILinkSerialisable
    {
        #region constants
        /// <summary>
        /// Meta data serialisation constants
        /// </summary>
        private static readonly String LINK_CHANNEL_NAME = "Name";
        private static readonly String LINK_CHANNEL_TYPE = "Channel";
        private static readonly String LINK_CHANNEL_CATEGORY = "ChannelCategory";
        private static readonly String LINK_CHANNEL_PARENT = "Parent";
        private static readonly String LINK_CHANNEL_CHILDREN = "Children";
        #endregion

        #region public Properties
        /// <summary>
        /// Friendly name
        /// </summary>
        public string Name
        {
            get;
            private set;
        }
        /// <summary>
        /// Channel ID
        /// </summary>
        public LinkChannel Channel
        {
            get;
            private set;
        }
        /// <summary>
        /// Category to additionally group or distinguish channels
        /// </summary>
        public LinkChannelCategory ChannelCategory
        {
            get;
            private set;
        }
        
        /// <summary>
        /// The link to a parent node
        /// </summary>
        public IObjectLink Parent
        {
            get;
            internal set;
        }
        /// <summary>
        /// Collection of links to child nodes
        /// </summary>
        public IEnumerable<IObjectLink> Children
        {
            get;
            private set;
        }
        #endregion

        #region constructors
        /// <summary>
        /// Default cstor
        /// </summary>
        internal ObjectLinkChannel()
        {
            Children = new List<IObjectLink>();
        }

        internal ObjectLinkChannel(LinkChannel channel):
            this()
        {
            Channel = channel;
        }
        #endregion

        #region (De)serialisation
        /// <summary>
        /// Factory method to create from meta data
        /// </summary>
        /// <param name="linkMgr"></param>
        /// <param name="channelTunable"></param>
        /// <returns></returns>
        internal static ObjectLinkChannel FromTunable(ObjectLinkMgr linkMgr, ITunable channelTunable)
        {
            ObjectLinkChannel channel = new ObjectLinkChannel();

            ITunableStructure channelStruct = channelTunable as ITunableStructure;

            channel.Name = (channelStruct[LINK_CHANNEL_NAME] as StringTunable).Value;
            channel.Channel = (LinkChannel)(channelStruct[LINK_CHANNEL_TYPE] as EnumTunable).Value;
            channel.ChannelCategory = (LinkChannelCategory)(channelStruct[LINK_CHANNEL_CATEGORY] as EnumTunable).Value;

            ITunableStructure parentObject = channelStruct[LINK_CHANNEL_PARENT] as ITunableStructure;
            channel.Parent = ObjectLink.FromTunable(linkMgr, parentObject);

            ITunableArray childObjects = channelStruct[LINK_CHANNEL_CHILDREN] as ITunableArray;
            channel.Children = new List<IObjectLink>();
            foreach (ITunableStructure childTunable in childObjects)
            {
                (channel.Children as List<IObjectLink>).Add(ObjectLink.FromTunable(linkMgr, childTunable));
            }
            return channel;
        }

        /// <summary>
        /// Serialisation - Fill meta definition structure
        /// </summary>
        /// <param name="channelDef"></param>
        /// <param name="parent"></param>
        /// <returns></returns>
        public StructureTunable ToTunable(StructMember channelDef, IModel parent)
        {
            StructureTunable channelStruct = new StructureTunable(channelDef, parent);
            channelStruct[LINK_CHANNEL_NAME] = new StringTunable(channelDef.Definition.Members[LINK_CHANNEL_NAME] as StringMember, parent, Name);
            channelStruct[LINK_CHANNEL_TYPE] = new EnumTunable(channelDef.Definition.Members[LINK_CHANNEL_TYPE] as EnumMember, parent, Channel.ToString());
            channelStruct[LINK_CHANNEL_CATEGORY] = new EnumTunable(channelDef.Definition.Members[LINK_CHANNEL_CATEGORY] as EnumMember, parent, ChannelCategory.ToString());
            if (null != Parent)
                channelStruct[LINK_CHANNEL_PARENT] = (Parent as ObjectLink).ToTunable(channelDef.Definition.Members[LINK_CHANNEL_PARENT] as StructMember, channelStruct);

            ArrayMember linkarrayDef = channelDef.Definition.Members[LINK_CHANNEL_CHILDREN] as ArrayMember;
            ArrayTunable linkarray = new ArrayTunable(linkarrayDef, parent);
            StructMember linkDef = linkarrayDef.ElementType as StructMember;
            foreach (ObjectLink link in Children)
            {
                linkarray.Add(link.ToTunable(linkDef, linkarray));
            }
            channelStruct[LINK_CHANNEL_CHILDREN] = linkarray;
            return channelStruct;
        }
        #endregion
    }
}
