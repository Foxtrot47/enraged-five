﻿using RSG.Base.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Metadata.Model;
using RSG.Metadata.Parser;
using RSG.Metadata.Data;

namespace RSG.ObjectLinks
{
    /// <summary>
    /// Helper class to resolve file paths to meta data and assets
    /// </summary>
    public static class MetaAssetResolver
    {
        #region constants

        /// <summary>
        /// extensions per meta type
        /// </summary>
        private static readonly String[] META_TYPE_EXTENSIONS = 
        {
            ".rlnk"
        };
        /// <summary>
        /// paths per meta type
        /// </summary>
        private static readonly String[] META_TYPE_SUB_PATHS = 
        {
            "objectLinks"
        };
        /// <summary>
        /// Folder depth, potentially varying per project to manage complexity
        /// </summary>
        private static readonly int FolderDepth = 2;

        private static Dictionary<string, StructureDictionary> StructDictCache;
        #endregion

        #region constructor
        static MetaAssetResolver()
        {
            StructDictCache = new Dictionary<string, StructureDictionary>();
        }
        #endregion

        #region Controller functions
        /// <summary>
        /// Resolve file path from a guid string in a cetain branch.
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="guid"></param>
        /// <param name="metaType"></param>
        /// <returns></returns>
        public static string GetMetaFilePath(IBranch branch, Guid guid, MetaTypes metaType)
        {
            string guidString = guid.ToString();

            string path = Path.Combine(branch.Metadata, META_TYPE_SUB_PATHS[(int)metaType]);
            for(int i=0;i<FolderDepth && i<guidString.Length;i++)
            {
                path = Path.Combine(path, ""+guidString[i]);
            }
            path = Path.Combine(path, guidString + META_TYPE_EXTENSIONS[(int)metaType]);
            return path;
        }

        /// <summary>
        /// Delete directory contents in a certain branch.
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="path"></param>
        public static void CleanDirsIfEmpty(IBranch branch, string path)
        {
            DirectoryInfo dirinfo = new DirectoryInfo(path);
            DirectoryInfo branchDirinfo = new DirectoryInfo(branch.Metadata);
            // Bail if not a subdir from the metadata dir (or the metadata dir itself for that matter)
            if (!branchDirinfo.EnumerateDirectories("*", SearchOption.AllDirectories).Contains(dirinfo))
                return;

            if (Directory.Exists(path) && !dirinfo.EnumerateFiles().Any())
            {
                CleanDirsIfEmpty(branch, dirinfo.Parent.ToString());
                Directory.Delete(path);
            }
        }

        /// <summary>
        /// Resolve the meta data structure definition
        /// </summary>
        /// <returns></returns>
        public static StructureDictionary GetPscRootStruct(IBranch branch, string subpath)
        {
            StructureDictionary sd = new StructureDictionary();
            String objLinksPsc = Path.Combine(branch.Metadata, "definitions", "tools", subpath);
            if (StructDictCache.ContainsKey(objLinksPsc))
                return StructDictCache[objLinksPsc];

            if (!File.Exists(objLinksPsc))
            {
                IBranch coreBranch = branch.Project.Config.CoreProject.Branches[branch.Name];
                objLinksPsc = Path.Combine(coreBranch.Metadata, "definitions", "tools", subpath);
            }
            sd.Load(branch, objLinksPsc);
            StructDictCache.Add(objLinksPsc, sd);
            return sd;
        }
        #endregion
    }
}
