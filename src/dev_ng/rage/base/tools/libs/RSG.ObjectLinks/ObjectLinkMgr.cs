﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIO = System.IO;
using System.Threading.Tasks;
using RSG.Metadata.Model;
using RSG.Metadata.Parser;
using RSG.Metadata.Data;
using RSG.Base.Configuration;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Core;
using RSG.Pipeline.Content;

namespace RSG.ObjectLinks
{
    /// <summary>
    /// Manager class to supply simple interface and util and glue functions
    /// 
    /// All concerned classes in this library are only creatible from within this library itself. 
    /// The classes them self only publish static factory methods from data and very particular constructors that are called after validation on the parameters.
    /// 
    /// A simplified object diagram:
    /// ObjectLinkMgr
    /// Cache:
    ///     |_
    ///         ObjectLinkNode
    ///             |_ 
    ///                 ObjectLinkChannel
    ///                     |_
    ///                         Parent: ObjectLink
    ///                                     |_
    ///                                         ObjectLinkNode cache reference
    ///                         Children:
    ///                                 ObjectLink
    ///                                     |_
    ///                                         ObjectLinkNode cache reference
    ///                                 ObjectLink
    ///                                 [...]
    ///                 ObjectLinkChannel
    ///                 [...]
    ///         ObjectLinkNode
    ///         [...]
    /// 
    /// See psc <metadata>/definitions/objectLinks/object_links.psc
    /// 
    /// </summary>
    public class ObjectLinkMgr
    {
        #region Constants
        /// <summary>
        /// Path serialising names
        /// </summary>
        private static readonly String LINK_ROOT = "LinkObject";
        #endregion

        #region private members
        /// <summary>
        /// The node cache!!
        /// </summary>
        private Dictionary<Guid, IObjectLinkNode> m_EditedNodes;
        #endregion

        #region properties
        /// <summary>
        /// It's a log
        /// </summary>
        public IUniversalLog Log
        {
            get;
            set;
        }

        /// <summary>
        /// The dark woods of content
        /// </summary>
        public ContentTreeHelper CTreeHelper
        {
            set;
            get;
        }

        public IBranch Branch
        {
            set;
            get;
        }
        #endregion

        #region con/destructors
        /// <summary>
        /// Default cstor
        /// For initalising the generic data members like the cache
        /// </summary>
        private ObjectLinkMgr()
        {
            m_EditedNodes = new Dictionary<Guid, IObjectLinkNode>();
//            MetaResolver = new MetaAssetResolver(MetaAssetResolver.eMetaTypes.eMetaTypeLinks);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ctHelper"></param>
        /// <param name="branch"></param>
        /// <param name="log"></param>
        public ObjectLinkMgr(ContentTreeHelper ctHelper, IBranch branch, IUniversalLog log)
            : this()
        {
            CTreeHelper = ctHelper;
            Branch = branch;
            Log = log;
        }

        /// <summary>
        /// D'stor
        /// </summary>
        ~ObjectLinkMgr()
        {
            FlushEditedNodes();
        }
        #endregion

        #region public functions

        #region meta data def methods
        /// <summary>
        /// Resolve the meta data structure definition
        /// </summary>
        /// <returns>psc structure</returns>
        public Structure GetPscRootStruct()
        {
            StructureDictionary sd = MetaAssetResolver.GetPscRootStruct(Branch, "objectlinks");
            return sd[LINK_ROOT];
        }
        #endregion

        #region Controller methods
        /// <summary>
        /// Util function for accessing the content tree
        /// </summary>
        /// <param name="assetPath"></param>
        /// <returns>Content node</returns>
        public IContentNode GetContentNode(string assetPath)
        {
            return CTreeHelper.CreateFile(assetPath);
        }

        /// <summary>
        /// Simple resolve by guid letters
        /// </summary>
        /// <param name="guid"></param>
        /// <returns>A root node from a linking file</returns>
        public IObjectLinkNode Resolve(Guid guid)
        {
            IObjectLinkNode resolvedNode = null;
            if (m_EditedNodes.ContainsKey(guid))
            {
                resolvedNode = m_EditedNodes[guid];
            }
            else
            {
                string linkDefPath = MetaAssetResolver.GetMetaFilePath(Branch, guid, MetaTypes.MetaTypeLinks);

                if (SIO.File.Exists(linkDefPath))
                {
                    MetaFile metaFile = new MetaFile(linkDefPath, GetPscRootStruct());
                    resolvedNode = ObjectLinkNode.FromTunable(this, metaFile);
                }
            }
            if (null != resolvedNode)
                AddToEditedNodes(resolvedNode);
            return resolvedNode;
        }

        /// <summary>
        /// Add node to cache
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>whether node pre-existed</returns>
        public bool AddToEditedNodes(IObjectLinkNode obj)
        {
            if (!m_EditedNodes.ContainsKey(obj.Guid))
            {
                m_EditedNodes.Add(obj.Guid, obj);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Flush cache
        /// </summary>
        /// <returns>Array of guids that got written out.</returns>
        public Guid[] FlushEditedNodes()
        {
            List<Guid> serialisedObjs = new List<Guid>();
            foreach (KeyValuePair<Guid, IObjectLinkNode> nodePair in m_EditedNodes)
            {
                if (!(nodePair.Value as ObjectLinkNode).Serialise(this))
                {
                    Log.ErrorCtx(nodePair.Value.Name, "Serialising of obect {0} with guid {1} failed. Aborting cache flush.", nodePair.Value.Name, nodePair.Key);
                    return serialisedObjs.ToArray();
                }
                serialisedObjs.Add(nodePair.Key);
            }
            m_EditedNodes.Clear();
            return serialisedObjs.ToArray();
        }

        /// <summary>
        /// resolve by guid with some validation, but also create if non-existent!
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="name"></param>
        /// <param name="cn"></param>
        /// <returns></returns>
        public IObjectLinkNode ResolveOrCreate(Guid guid, string name, IContentNode cn)
        {
            if (guid == Guid.Empty)
                return null;

            IObjectLinkNode node = Resolve(guid);

            if (node != null && 
                node.ContentNode != null &&
                ((cn as IFilesystemNode).AbsolutePath != (node.ContentNode as IFilesystemNode).AbsolutePath))
            {
                Log.Error("The target asset for this link was formerly set to " + (node.ContentNode as IFilesystemNode).AbsolutePath);
            }

            if (null == node)
                node = new ObjectLinkNode( guid, name, cn );

            if (null != node)
                AddToEditedNodes(node);

            return node;
        }

        /// <summary>
        /// Link two objects and write or update two link files
        /// </summary>
        /// <param name="channel"></param>
        /// <param name="sourceContentNode"></param>
        /// <param name="sourceGuid"></param>
        /// <param name="sourceName"></param>
        /// <param name="targetContentNode"></param>
        /// <param name="targetGuid"></param>
        /// <param name="targetName"></param>
        /// <returns></returns>
        public LinkStatusFlags Link(LinkChannel channel, 
            IContentNode sourceContentNode, Guid sourceGuid, string sourceName,
            IContentNode targetContentNode, Guid targetGuid, string targetName)
        {
            // Creating two-way linking
            IObjectLinkNode sourceObj = ResolveOrCreate(sourceGuid, sourceName, sourceContentNode);
            IObjectLink link = sourceObj.Link(this, targetContentNode, targetGuid, channel, targetName);
            if (null == link)
            {
                return LinkStatusFlags.LinkStatusUnresolved;
            }
            link.Target.Link(this, sourceObj.ContentNode, sourceObj.Guid, channel, sourceObj.Name, true);
            // namespace could have changed and node is taken from cache. update namespace so the node is serialising correctly.

            return link.StatusFlags;
        }

        /// <summary>
        /// Unlink two objects. Update and delete link files if necessary
        /// </summary>
        /// <param name="channel"></param>
        /// <param name="sourceObj"></param>
        /// <param name="targetObj"></param>
        /// <returns></returns>
        public bool Unlink(LinkChannel channel, Guid sourceGuid, Guid targetGuid)
        {
            IObjectLinkNode sourceObj = Resolve(sourceGuid);
            IObjectLinkNode targetObj = Resolve(targetGuid);

            if (null == sourceObj)
            {
                return false;
            }

            if(null == targetObj)
            {
                if (null == sourceObj.GetChannel(channel) ||
                    null == sourceObj.GetChannel(channel).Parent ||
                    null == sourceObj.GetChannel(channel).Parent.Target)
                {
                    return false;
                }

                targetObj = sourceObj.GetChannel(channel).Parent.Target;
            }

            // Deleting two-way linking
            bool found1 = sourceObj.Unlink(targetObj, channel);
            bool found2 = targetObj.Unlink(sourceObj, channel, true);

            if (!found1)
                Log.WarningCtx(sourceObj.Name, "Couldn't find link from {0} to {1]", sourceObj.Name, targetObj.Name);
            if (!found2)
                Log.WarningCtx(targetObj.Name, "Couldn't find link from {0} to {1]", targetObj.Name, sourceObj.Name);

            return found1 && found2;
        }


        /// <summary>
        /// Force serialisation of particular guid
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public bool Serialise(Guid guid)
        {
            ObjectLinkNode obj = Resolve(guid) as ObjectLinkNode;
            if (null == obj)
                return false;
            return obj.Serialise(this);
        }
        #endregion
        #endregion //public functions

    }
}
