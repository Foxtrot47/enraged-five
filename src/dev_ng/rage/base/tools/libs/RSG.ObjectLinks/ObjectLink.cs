﻿using System;
using System.Collections.Generic;
using RSG.Metadata.Model;
using RSG.Metadata.Parser;
using RSG.Metadata.Data;
using RSG.Base.Editor;
using RSG.Base.Configuration;
using RSG.Pipeline.Core;
using RSG.Pipeline.Content;

namespace RSG.ObjectLinks
{
    /// <summary>
    /// Class to hold and process information the linkage between objects
    /// </summary>
    public class ObjectLink : IObjectLink, ILinkSerialisable
    {
        #region Constants
        /// <summary>
        /// meta data serialisation constants
        /// </summary>
        private static readonly String LINK_NAME = "Name";
        private static readonly String LINK_GUID = "Guid";
        private static readonly String LINK_ASSETPATH = "AssetPath";
        #endregion

        #region private properties
        /// <summary>
        /// target asset file
        /// cached or accessor to underlying Object node.
        /// </summary>
        private IContentNode m_ContentNode;
        /// <summary>
        /// Target's friendly name
        /// cached or accessor to underlying Object node.
        /// </summary>
        private string m_Name;
        /// <summary>
        /// target's unique identifier
        /// cached or accessor to underlying Object node.
        /// </summary>
        private Guid m_Guid;
        #endregion

        #region public Properties
        /// <summary>
        /// Status information of the link
        /// </summary>
        public LinkStatusFlags StatusFlags
        {
            get;
            private set;
        }

        /// <summary>
        /// Target the holder of this link links to.
        /// </summary>
        public IObjectLinkNode Target
        {
            get;
            private set;
        }

        /// <summary>
        /// friendly name 
        /// of underlying node or cached.
        /// </summary>
        public string Name
        {
            get {
                if (null != Target)
                    m_Name = Target.Name;
                return m_Name; 
            }
        }
        /// <summary>
        /// unique identifier
        /// of underlying node or cached.
        /// </summary>
        public Guid Guid
        {
            get {
                if (null != Target)
                    m_Guid = Target.Guid;
                return m_Guid; 
            }
        }
        /// <summary>
        /// asset file path 
        /// of underlying node or cached.
        /// </summary>
        public IContentNode ContentNode
        {
            get {
                if (null != Target)
                    m_ContentNode = Target.ContentNode;
                return m_ContentNode; 
            }
        }
        #endregion 

        #region constructors
        /// <summary>
        /// cstor with resolve/create logic
        /// </summary>
        /// <param name="linkMgr"></param>
        /// <param name="guid"></param>
        /// <param name="name"></param>
        /// <param name="contentNode"></param>
        /// <param name="branchOption">Optionally pass different branch to creation</param>
        internal ObjectLink(ObjectLinkMgr linkMgr, Guid guid, string name, IContentNode contentNode = null)
        {
            LinkStatusFlags status = LinkStatusFlags.LinkStatusNone;
            IObjectLinkNode node = linkMgr.Resolve(guid);
            if (null == node && null!=contentNode)
            {
                node = linkMgr.ResolveOrCreate(guid, name, contentNode);
            }
            if (null != node)
            {
                if (!node.AssetExistent())
                    status |= LinkStatusFlags.LinkStatusNoAsset;
                m_Name = node.Name;
                m_Guid = node.Guid;
                Target = node;
            }
            else
                status = LinkStatusFlags.LinkStatusUnresolved;

            StatusFlags = status;
        }
        #endregion

        #region internal methods
        /// <summary>
        /// Factory method to create from meta data
        /// </summary>
        /// <param name="linkMgr"></param>
        /// <param name="linkDef"></param>
        /// <returns></returns>
        internal static IObjectLink FromTunable(ObjectLinkMgr linkMgr, ITunableStructure linkDef)
        {
            if ((linkDef[LINK_GUID] as StringTunable).Value == null)
                return null;
            string name = (linkDef[LINK_NAME] as StringTunable).Value;
            Guid guid = new Guid((linkDef[LINK_GUID] as StringTunable).Value);
            return new ObjectLink(linkMgr, guid, name);
        }

        /// <summary>
        /// Fill in meta definition structure
        /// </summary>
        /// <param name="metaDefinition"></param>
        /// <returns></returns>
        public StructureTunable ToTunable(StructMember metaDefinition, IModel parent)
        {
            StructureTunable linkObj = new StructureTunable(metaDefinition, parent);
            linkObj[LINK_NAME] = new StringTunable(metaDefinition.Definition.Members[LINK_NAME] as StringMember, linkObj as IModel, m_Name);
            linkObj[LINK_GUID] = new StringTunable(metaDefinition.Definition.Members[LINK_GUID] as StringMember, linkObj as IModel, m_Guid.ToString());
            return linkObj;
        }
        #endregion
    }
}
