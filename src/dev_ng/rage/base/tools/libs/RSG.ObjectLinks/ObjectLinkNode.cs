﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIO = System.IO;
using System.Threading.Tasks;
using RSG.Metadata.Model;
using RSG.Metadata.Parser;
using RSG.Metadata.Data;
using RSG.Base.Configuration;
using RSG.Base.Logging.Universal;
using System.Xml;
using RSG.Base.Editor;
using RSG.Pipeline.Core;
using RSG.Pipeline.Content;

namespace RSG.ObjectLinks
{
    #region Constants
    #endregion

    /// <summary>
    /// Class to hold and process information about a root object for a link file, its definition storage and links
    /// </summary>
    public class ObjectLinkNode : 
        ILinkSerialisable,
        IObjectLinkNode
    {
        #region Constants
        /// <summary>
        /// Metadata psc serialising constants
        /// </summary>
        private static readonly String LINK_ROOT = "LinkObject";
        private static readonly String LINK_OBJECT_NAME = "Name";
        private static readonly String LINK_OBJECT_GUID = "Guid";
        private static readonly String LINK_OBJECT_ASSETPATH = "AssetPath";
        private static readonly String LINK_OBJECT_ASSETTYPE = "AssetType";
        private static readonly String LINK_OBJECT_CHANNELS = "Channels";
        private static readonly String LINK_OBJECT_PARENT = "LinkRoot";
        #endregion

        #region private properties
        /// <summary>
        /// meta data file
        /// </summary>
        private MetaFile m_MetaFile;

        /// <summary>
        /// psc meta definition
        /// </summary>
        private static StructMember m_MetaDefinition;

        /// <summary>
        /// cached meta data filename
        /// </summary>
        private string m_Filename;

        #endregion

        #region public Properties
        /// <summary>
        /// Friendly name
        /// </summary>
        public string Name
        {
            get;
            private set;
        }
        /// <summary>
        /// Globla unique identifier
        /// These are coming from the DCC package where they are stored in the asset files.
        /// Copies into branches should not have them copied, but reset to new ones.
        /// </summary>
        public Guid Guid
        {
            get;
            private set;
        }

        /// <summary>
        /// Resource file system storage
        /// </summary>
        public IContentNode ContentNode
        {
            get;
            private set;
        }
        
        /// <summary>
        /// linking channels, i.e. LOD or drawable LOD
        /// </summary>
        public IEnumerable<IObjectLinkChannel> Channels
        {
            get;
            private set;
        }
        #endregion

        #region constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        internal ObjectLinkNode()
        {
            Channels = new List<IObjectLinkChannel>();
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        internal ObjectLinkNode(Guid guid, string name, IContentNode contentNode):
            this()
        {
            Guid = guid;
            Name = name;
            ContentNode = contentNode;
        }
        #endregion

        #region public methods
        #region helpers 
        /// <summary>
        /// Find channel in object
        /// </summary>
        /// <param name="channel"></param>
        /// <returns></returns>
        public IObjectLinkChannel GetChannel(LinkChannel channel)
        {
            try
            {
                return Channels.First<IObjectLinkChannel>(ch => ch.Channel == channel);
            }
            catch (InvalidOperationException /*ex*/)
            {
            }
            catch (ArgumentNullException /*ex*/)
            {
            }
            return null;
        }

        /// <summary>
        /// Tidy up function to delete unused channels
        /// </summary>
        private void CleanChannels()
        {
            foreach (IObjectLinkChannel channel in Channels)
            {
                try
                {
                    List<IObjectLinkChannel> foundChannels = Channels.Where<IObjectLinkChannel>(ch => ch.Channel == channel.Channel) as List<IObjectLinkChannel>;
                    IObjectLinkChannel keepThisChannel = foundChannels.FirstOrDefault();

                    // Delete other found channels of same type 
                    foreach (IObjectLinkChannel item in foundChannels)
                        if (item != keepThisChannel)
                            (Channels as List<IObjectLinkChannel>).Remove(item);
                }
                catch (InvalidOperationException /*ex*/)
                {
                }
                catch (ArgumentNullException /*ex*/)
                {
                }
            }
        }

        /// <summary>
        /// Find a certain object by guid in the defined channel
        /// </summary>
        /// <param name="targetGuid"></param>
        /// <param name="channel"></param>
        /// <param name="direction"></param>
        /// <returns></returns>
        public IObjectLink FindLinkInChannel(Guid targetGuid, IObjectLinkChannel channel, SearchTroolean direction = SearchTroolean.Either)
        {
            try
            {
                if (null != channel.Parent && channel.Parent.Guid == targetGuid)
                {
                    if (direction == SearchTroolean.AsParent || direction == SearchTroolean.Either)
                        return channel.Parent;
                    else
                        return null;
                }
                IObjectLink qLink = channel.Children.First(lnk => lnk.Guid == targetGuid);
                // if not found Exception should be thrown before reaching this.
                if (direction == SearchTroolean.AsChild || direction == SearchTroolean.Either)
                    return qLink;
                else
                    return null;
            }
            catch (InvalidOperationException /*ex*/) { }

            return null;
        }
        /// <summary>
        /// Find a certain object by guid in all channels of this object
        /// </summary>
        /// <param name="targetGuid"></param>
        /// <returns></returns>
        public IObjectLink FindLink(Guid targetGuid)
        {
            foreach(IObjectLinkChannel channel in Channels)
            {
                IObjectLink foundLink = FindLinkInChannel(targetGuid, channel);
                if (null != foundLink)
                {
                    return foundLink;
                }
            }
            return null;
        }

        /// <summary>
        /// Check for local existence of asset.
        /// </summary>
        /// <returns></returns>
        public bool AssetExistent()
        {
            if(!(ContentNode is IFilesystemNode))
                return false;
            return SIO.File.Exists((ContentNode as IFilesystemNode).AbsolutePath);
        }
        #endregion

        #region linking
        /// <summary>
        /// Linking is called from child to parent by default, just like the references in the DCC package are defined.
        /// </summary>
        /// <param name="linkMgr"></param>
        /// <param name="targetContentNode"></param>
        /// <param name="targetGuid"></param>
        /// <param name="channel"></param>
        /// <param name="targetname"></param>
        /// <param name="asChild"></param>
        /// <param name="targetBranchOption"></param>
        /// <returns></returns>
        public IObjectLink Link(ObjectLinkMgr linkMgr, IContentNode targetContentNode, Guid targetGuid, LinkChannel channel, string targetname = "undefined", bool asChild = false)
        {
            IObjectLinkChannel foundChannel = GetChannel(channel);

            if (null == foundChannel)
            {
                foundChannel = new ObjectLinkChannel(channel);
                List<IObjectLinkChannel> channelList = Channels as List<IObjectLinkChannel>;
                channelList.Add(foundChannel);
            }

            //////////////////////////////////////////////////////////////////////////
            // Make sure the link's not defined in the wrong direction for avoiding circular references
            // If defined in the right direction, just return the existent link
            //////////////////////////////////////////////////////////////////////////

            IObjectLink foundLink = FindLinkInChannel(targetGuid, foundChannel, asChild?SearchTroolean.AsChild:SearchTroolean.AsParent);
            if (null != foundLink)
            {
                return foundLink;
            }

            //////////////////////////////////////////////////////////////////////////
            // Create new
            //////////////////////////////////////////////////////////////////////////
            IObjectLink targetLink = new ObjectLink(linkMgr, targetGuid, targetname, targetContentNode);
            if (asChild)
            {
                (foundChannel.Children as List<IObjectLink>).Add(targetLink);
            }
            else
            {
                IObjectLink parent = foundChannel.Parent;
                if (null != parent)
                {
                    parent.Target.Unlink(this, foundChannel.Channel, true);
                }
                parent = null;
                (foundChannel as ObjectLinkChannel).Parent = targetLink;
            }

            return targetLink;
        }

        /// <summary>
        /// Seek and destroy a link
        /// </summary>
        /// <param name="linkTarget"></param>
        /// <param name="channel"></param>
        /// <param name="isChild"></param>
        /// <returns></returns>
        public bool Unlink(IObjectLinkNode linkTarget, LinkChannel channel, bool isChild = false)
        {
            IObjectLinkChannel foundChannel = GetChannel(channel);

            if (null == foundChannel)
            {
                return false;
            }

            bool found = false;
            if (isChild)
            {
                List<IObjectLink> linkList = foundChannel.Children as List<IObjectLink>;
                try
                {
                    IObjectLink link = linkList.First(qLink => qLink.Target == linkTarget);
                    // if not found Exception should be thrown before reaching this.
                    found = (foundChannel.Children as List<IObjectLink>).Remove(link);
                }
                catch (InvalidOperationException /*ex*/){}
            }
            else
            {
	            if (null!=foundChannel.Parent && foundChannel.Parent.Guid == linkTarget.Guid)
	            {
	                (foundChannel as ObjectLinkChannel).Parent = null;
	                found = true;
                }
            }

            // Deleting the channel if no links present
            if (null == foundChannel.Parent && foundChannel.Children.Count() <= 0)
                (Channels as List<IObjectLinkChannel>).Remove(foundChannel);

            return found;
        }

        #endregion

        #region file helper methods
        /// <summary>
        /// Handle the filesystem interaction. including serialising and deleting for links that went undefined.
        /// </summary>
        /// <returns></returns>
        public bool Serialise(ObjectLinkMgr linkMgr, IBranch branchOption = null)
        {
            string filename = MetaAssetResolver.GetMetaFilePath(
                branchOption==null?linkMgr.Branch:branchOption, 
                Guid, 
                MetaTypes.MetaTypeLinks);

            if (m_Filename != filename)
            {
                try
                {
                    if (SIO.File.Exists(m_Filename))
                        SIO.File.Delete(m_Filename);
                }
                catch (Exception /*ex*/)
                {
                    linkMgr.Log.ToolError("trying to delete old link file failed: {0}", m_Filename);
                }
            }
            m_Filename = filename;

            CleanChannels();

            StructureTunable tune = null;
            try
            {
                tune = ToTunable(GetPscDef(linkMgr), null);
            }
            catch (KeyNotFoundException /*ex*/)
            {
                linkMgr.Log.ToolError("There has been a KeyNotFoundException during attempted meta data serialisation. Likely the psc file has changed.");
            }

            if(null==tune)
            {
                try
                {
                    if (SIO.File.Exists(filename))
                        SIO.File.Delete(filename);
                }
                catch (Exception /*ex*/)
                {
                    linkMgr.Log.ToolError("Link deletion failed for file {0}", filename);
                }
                return true;
            }
            XmlDocument doc = new XmlDocument();
            string dir = SIO.Path.GetDirectoryName(filename);
            if (!SIO.Directory.Exists(dir))
                SIO.Directory.CreateDirectory(dir);
            doc.AppendChild(tune.Serialise(doc, true));
            try
            {
                doc.Save(filename);
            }
            catch (Exception /*ex*/)
            {
                linkMgr.Log.ToolError("Link saving failed for file {0}", filename);
            }
            return true;
        }
        #endregion // file helpers

        #endregion // public 

        #region internal methods
        /// <summary>
        /// Factory method
        /// </summary>
        /// <param name="linkMgr"></param>
        /// <param name="metaFile"></param>
        /// <returns></returns>
        internal static IObjectLinkNode FromTunable(ObjectLinkMgr linkMgr, MetaFile metaFile)
        {
            ObjectLinkNode node = new ObjectLinkNode();

            StructureTunable linkRoot = metaFile.Members[LINK_ROOT] as StructureTunable;

            node.Name = (linkRoot[LINK_OBJECT_NAME] as StringTunable).Value;
            node.Guid = new Guid((linkRoot[LINK_OBJECT_GUID] as StringTunable).Value);
            node.ContentNode = linkMgr.GetContentNode((linkRoot[LINK_OBJECT_ASSETPATH] as StringTunable).Value);

            linkMgr.AddToEditedNodes(node);

            ArrayTunable objectChannels = linkRoot[LINK_OBJECT_CHANNELS] as ArrayTunable;
            node.Channels = new List<IObjectLinkChannel>();
            foreach (ITunable channelTunable in objectChannels)
            {
                (node.Channels as List<IObjectLinkChannel>).Add(ObjectLinkChannel.FromTunable(linkMgr, channelTunable));
            }
            node.CleanChannels();

            return node;
        }
        #endregion // internal methods

        #region private methods
        #region (de)serialisation
        /// <summary>
        /// Helper function to get the psc definition structure member
        /// </summary>
        /// <param name="linkMgr"></param>
        /// <returns></returns>
        private StructMember GetPscDef(ObjectLinkMgr linkMgr)
        {
            if (null == m_MetaDefinition)
            {
                Structure rootStruct = linkMgr.GetPscRootStruct();
                StructMember structMember = new StructMember(rootStruct, rootStruct);
                m_MetaDefinition = structMember;
            }

            return m_MetaDefinition;
        }

        /// <summary>
        /// Fill in the meta data def structure
        /// </summary>
        /// <param name="linkMgr"></param>
        /// <returns></returns>
        public StructureTunable ToTunable(StructMember objNodeDef, IModel parent)
        {
            if (Channels.Count() <= 0)
                return null;

            StructureTunable linkObj = new StructureTunable(objNodeDef, parent);
            linkObj[LINK_OBJECT_NAME] = new StringTunable(m_MetaDefinition.Definition.Members[LINK_OBJECT_NAME] as StringMember, linkObj as IModel, Name);
            linkObj[LINK_OBJECT_GUID] = new StringTunable(m_MetaDefinition.Definition.Members[LINK_OBJECT_GUID] as StringMember, linkObj as IModel, Guid.ToString());
            linkObj[LINK_OBJECT_ASSETPATH] = new StringTunable(m_MetaDefinition.Definition.Members[LINK_OBJECT_ASSETPATH] as StringMember, linkObj as IModel, (ContentNode as IFilesystemNode).AbsolutePath);

            ArrayMember channelArrayDef = m_MetaDefinition.Definition.Members[LINK_OBJECT_CHANNELS] as ArrayMember;
            ArrayTunable tunearray = new ArrayTunable(channelArrayDef, linkObj as IModel);
            StructMember channelDef = channelArrayDef.ElementType as StructMember;
            foreach (ObjectLinkChannel channel in Channels)
            {
                tunearray.Add(channel.ToTunable(channelDef, tunearray));
            }
            linkObj[LINK_OBJECT_CHANNELS] = tunearray;

            return linkObj;
        }
        #endregion //(de)serialisation
        #endregion //private methods
    }
}
