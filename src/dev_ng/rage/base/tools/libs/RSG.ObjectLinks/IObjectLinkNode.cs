﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIO = System.IO;
using System.Threading.Tasks;
using RSG.Metadata.Model;
using RSG.Metadata.Parser;
using RSG.Metadata.Data;
using RSG.Base.Configuration;
using RSG.Base.Logging.Universal;
using System.Xml;
using RSG.Base.Editor;
using RSG.Pipeline.Core;
using RSG.Pipeline.Content;

namespace RSG.ObjectLinks
{
    /// <summary>
    /// Interface for serialisation logic
    /// </summary>
    public interface ILinkSerialisable
    {
        /// <summary>
        /// Load data from internal members to tunable structures
        /// </summary>
        /// <param name="channelDef"></param>
        /// <param name="parent"></param>
        /// <returns></returns>
        StructureTunable ToTunable(StructMember channelDef, IModel parent);
    }

    /// <summary>
    /// Interface for a node link information 
    /// </summary>
    public interface IObjectLink
    {
        #region Properties
        /// <summary>
        /// Status information of the link
        /// </summary>
        LinkStatusFlags StatusFlags
        {
            get;
        }

        /// <summary>
        /// Target the holder of this link links to.
        /// </summary>
        IObjectLinkNode Target
        {
            get;

        }

        /// <summary>
        /// friendly name 
        /// of underlying node or cached.
        /// </summary>
        string Name
        {
            get;
        }
        /// <summary>
        /// unique identifier
        /// of underlying node or cached.
        /// </summary>
        Guid Guid
        {
            get;
        }
        /// <summary>
        /// asset file path 
        /// of underlying node or cached.
        /// </summary>
        IContentNode ContentNode
        {
            get;
        }
        #endregion
    }

    /// <summary>
    /// interface for link channel grouping information
    /// </summary>
    public interface IObjectLinkChannel
    {
        #region Properties
        /// <summary>
        /// Friendly name
        /// </summary>
        string Name
        {
            get;

        }
        /// <summary>
        /// Channel ID
        /// </summary>
        LinkChannel Channel
        {
            get;

        }
        /// <summary>
        /// Category to additionally group or dinstinguish channels
        /// </summary>
        LinkChannelCategory ChannelCategory
        {
            get;

        }

        /// <summary>
        /// The link to a parent node
        /// </summary>
        IObjectLink Parent
        {
            get;
        }
        /// <summary>
        /// Collection of links to child nodes
        /// </summary>
        IEnumerable<IObjectLink> Children
        {
            get;

        }
        #endregion
    }
    
    /// <summary>
    /// Interface for base link node info
    /// </summary>
    public interface IObjectLinkNode
    {
        #region Properties
        /// <summary>
        /// Friendly name
        /// </summary>
        string Name
        {
            get;

        }
        /// <summary>
        /// Globla unique identifier
        /// These are coming from the DCC package where they are stored in the asset files.
        /// Copies into branches should not have them copied, but reset to new ones.
        /// </summary>
        Guid Guid
        {
            get;

        }

        /// <summary>
        /// Resource file system storage
        /// </summary>
        IContentNode ContentNode
        {
            get;

        }

        /// <summary>
        /// linking channels, i.e. LOD or drawable LOD
        /// </summary>
        IEnumerable<IObjectLinkChannel> Channels
        {
            get;

        }
        #endregion

        #region helpers 
        /// <summary>
        /// Check for local existence of asset.
        /// </summary>
        /// <returns></returns>
        bool AssetExistent();

        /// <summary>
        /// Find channel in object
        /// </summary>
        /// <param name="channel"></param>
        /// <returns></returns>
        IObjectLinkChannel GetChannel(LinkChannel channel);

        /// <summary>
        /// Find a certain object by guid in the defined channel
        /// </summary>
        /// <param name="targetGuid"></param>
        /// <param name="channel"></param>
        /// <param name="direction"></param>
        /// <returns></returns>
        IObjectLink FindLinkInChannel(Guid targetGuid, IObjectLinkChannel channel, SearchTroolean direction = SearchTroolean.Either);

        /// <summary>
        /// Linking is called from child to parent by default, just like the references in the DCC package are defined.
        /// </summary>
        /// <param name="linkMgr"></param>
        /// <param name="targetContentNode"></param>
        /// <param name="targetGuid"></param>
        /// <param name="channel"></param>
        /// <param name="targetname"></param>
        /// <param name="asChild"></param>
        /// <param name="targetBranchOption"></param>
        /// <returns></returns>
        IObjectLink Link(ObjectLinkMgr linkMgr, IContentNode targetContentNode, Guid targetGuid, LinkChannel channel, string targetname = "undefined", bool asChild = false);

        /// <summary>
        /// Seek and destroy a link
        /// </summary>
        /// <param name="linkTarget"></param>
        /// <param name="channel"></param>
        /// <param name="isChild"></param>
        /// <returns></returns>
        bool Unlink(IObjectLinkNode linkTarget, LinkChannel channel, bool isChild = false);
        #endregion
    }

}