﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Base.Attributes;

namespace RSG.ObjectLinks
{
    /// <summary>
    /// Flags to communicate problematic states across the workflow
    /// </summary>
    [Flags]
    public enum LinkStatusFlags
    {
        LinkStatusNone              = 0,
        LinkStatusUnresolved        = 1,
        LinkStatusNoAsset           = 1 << 1,
        LinkStatusTargetChanged     = 1 << 2,
        LilnkStatusCorrupt          = 1 << 3,
        LinkStatusResolved          = 1 << 4,
        LinkStatusWasCached         = 1 << 5,
        LinkStatusSrcUnresolved     = 1 << 6,
    }

    /// <summary>
    /// 
    /// </summary>
    public enum SearchTroolean
    {
        AsChild,
        AsParent,
        Either
    }

    /// <summary>
    /// Types of meta files we can resolve
    /// The enum solves as a mapping index into the string arrays for path and extension
    /// </summary>
    public enum MetaTypes
    {
        MetaTypeLinks = 0
    };

    /// <summary>
    /// Link channels
    /// </summary>
    public enum LinkChannel
    {
        [FieldDisplayName("Scene_LOD")]
        LinkChannelSceneLod,
        [FieldDisplayName("Drawable_LOD")]
        LinkChannelDrawableLod,
        [FieldDisplayName("Render_Sim")]
        LinkChannelRenderSim,
        [FieldDisplayName("Cloth_Collision")]
        LinkChannelClothCollision,
        [FieldDisplayName("Unused!")]
        LinkChannelUnused,
        [FieldDisplayName("CombinerMesh")]
        LinkChannelCombinerMesh,
        [FieldDisplayName("ShadowMesh")]
        LinkChannelShadowMesh,
        [FieldDisplayName("CustomHelperLink")]
        LinkChannelCustomHelper
    }

    /// <summary>
    /// Link channel categories
    /// </summary>
    public enum LinkChannelCategory
    {
        LinkChannelCatNone,
        LinkChannelCatPS3,
        LinkChannelCatXBOX,
        LinkChannelCatPC
    }
}
