﻿//---------------------------------------------------------------------------------------------
// <copyright file="TestFileAssociation.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.UnitTest
{
    using System;
    using global::Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// 
    /// </summary>
    [TestClass]
    public class TestFileAssociation
    {
        [TestMethod]
        public void TestRead1()
        {
            FileAssociation fa1 = new FileAssociation(".txt");
            Assert.AreEqual(".txt", fa1.Extension);
            Assert.IsNotNull(fa1.ShellHandlers);

            FileAssociation fa2 = new FileAssociation(".rpf");
            Assert.AreEqual(".rpf", fa2.Extension);
            Assert.IsNotNull(fa2.ShellHandlers);
        }

        [TestMethod]
        public void TestWrite1()
        {

        }
    }

} // RSG.Interop.Microsoft.UnitTest namespace
