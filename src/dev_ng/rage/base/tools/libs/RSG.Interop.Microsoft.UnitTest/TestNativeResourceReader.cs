﻿//---------------------------------------------------------------------------------------------
// <copyright file="TestNativeResourceReader.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.UnitTest
{
    using System;
    using System.Windows.Media.Imaging;
    using global::Microsoft.VisualStudio.TestTools.UnitTesting;
    using RSG.Interop.Microsoft.Windows;

    /// <summary>
    /// 
    /// </summary>
    [TestClass]
    public class TestNativeResourceReader
    {
        /// <summary>
        /// 
        /// </summary>
        [TestMethod]
        public void TestRead1()
        {
            String executableFilename = @"C:\Windows\notepad.exe";
            using (NativeResourceReader reader = new NativeResourceReader(executableFilename))
            {
                // Read resource string from Notepad executable.
                String friendlyTypeName = reader.ReadStringResource(469);
                Assert.AreEqual("Text Document", friendlyTypeName);

                //BitmapSource icon = reader.ReadIconResource(1);
                //Assert.IsNotNull(icon);

                //BitmapSource bmp = reader.ReadBitmapResource(51209);
                //Assert.IsNotNull(bmp);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [TestMethod]
        public void TestResourceIteration()
        {
#if false
            String executableFilename = @"C:\Windows\notepad.exe";
            using (NativeResourceReader reader = new NativeResourceReader(executableFilename))
            {
                Assert.IsTrue(reader.EnumResources(ResourceType.Icon, this.EnumIconResourceCallback));
                Assert.IsTrue(reader.EnumResources(ResourceType.Bitmap, this.EnumBitmapResourceCallback));
            }
#endif
        }

        private bool EnumIconResourceCallback(IntPtr hModule, String moduleName, ResourceType resType, String resName)
        {
            Assert.AreEqual(ResourceType.Icon, resType);
            Assert.IsTrue(resName.Length > 0);
            return (true);
        }

        private bool EnumBitmapResourceCallback(IntPtr hModule, String moduleName, ResourceType resType, String resName)
        {
            Assert.AreEqual(ResourceType.Bitmap, resType);
            Assert.IsTrue(resName.Length > 0);
            return (true);
        }

        /// <summary>
        /// Unit Test: check ReadStringResource raises exception for failed ID.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(System.ComponentModel.Win32Exception))]
        public void TestReadFail1()
        {
            String executableFilename = @"C:\Windows\notepad.exe";
            using (NativeResourceReader reader = new NativeResourceReader(executableFilename))
            {
                reader.ReadStringResource(int.MaxValue);
            }
        }

        /// <summary>
        /// Unit Test: check ReadBitmapResource raises exception for failed ID.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(System.ComponentModel.Win32Exception))]
        public void TestReadFail2()
        {
            String executableFilename = @"C:\Windows\notepad.exe";
            using (NativeResourceReader reader = new NativeResourceReader(executableFilename))
            {
                //reader.ReadBitmapResource(int.MaxValue);
            }
        }
    }

} // RSG.Interop.Microsoft.UnitTest namespace
