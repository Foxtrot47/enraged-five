﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using GameConfig.Model.Core;

namespace GameConfig.Model
{
    /// <remarks/>
    [Serializable()]
    [XmlType(AnonymousType = true, TypeName = "Input")]
    public partial class Input : ModelBase
    {
        /// <remarks/>
        [XmlAttribute(AttributeName = "id")]
        public string Id
        {
            get
            {
                return m_id;
            }
            set
            {
                m_id = value;
                NotifyPropertyChanged("Id");
            }
        }

        /// <remarks/>
        [XmlAttribute(AttributeName = "name")]
        public string Name
        {
            get
            {
                return m_name;
            }
            set
            {
                m_name = value;
                NotifyPropertyChanged("Name");
            }
        }

        /// <remarks/>
        [XmlAttribute(AttributeName = "dataType")]
        public string DataType
        {
            get
            {
                return m_dataType;
            }
            set
            {
                m_dataType = value;
                NotifyPropertyChanged("DataType");
            }
        }

        private string m_id;
        private string m_name;
        private string m_dataType;
    }
}
