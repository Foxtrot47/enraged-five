﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using GameConfig.Model.Core;

namespace GameConfig.Model
{
    /// <remarks/>
    [Serializable()]
    [XmlType(AnonymousType = true, TypeName = "CategoryPermutation")]
    public partial class CategoryPermutation : ModelBase
    {
        /// <remarks/>
        [XmlTextAttribute()]
        public string Value
        {
            get
            {
                return m_value;
            }
            set
            {
                m_value = value;
                NotifyPropertyChanged("Value");
            }
        }

        private string m_value;
    }
}
