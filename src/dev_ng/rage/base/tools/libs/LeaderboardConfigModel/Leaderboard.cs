﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using GameConfig.Model.Core;

namespace GameConfig.Model
{
    /// <remarks/>
    [Serializable()]
    [XmlType(AnonymousType = true, TypeName = "Leaderboard")]
    public partial class Leaderboard : ModelBase
    {
        /// <remarks/>
        [XmlArray(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, ElementName = "CategoryPermutations")]
        [XmlArrayItem("CategoryPermutation", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public ObservableCollection<CategoryPermutation> CategoryPermutations
        {
            get
            {
                return m_categoryPermutations;
            }
            set
            {
                m_categoryPermutations = value;
                NotifyPropertyChanged("CategoryPermutations");
            }
        }

        /// <remarks/>
        [XmlArray(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, ElementName = "Columns")]
        [XmlArrayItem("Column", Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = false)]
        public ObservableCollection<Column> Columns
        {
            get
            {
                return m_columns;
            }
            set
            {
                m_columns = value;
                NotifyPropertyChanged("Columns");
            }
        }

        /// <remarks/>
        [XmlAttribute(AttributeName = "id")]
        public string Id
        {
            get
            {
                return m_id;
            }
            set
            {
                m_id = value;
                NotifyPropertyChanged("Id");
            }
        }

        /// <remarks/>
        [XmlAttribute(AttributeName = "name")]
        public string Name
        {
            get
            {
                return m_name;
            }
            set
            {
                m_name = value;
                NotifyPropertyChanged("Name");
            }
        }

        /// <remarks/>
        [XmlAttribute(AttributeName = "entryExpiration")]
        public string EntryExpiration
        {
            get
            {
                return m_entryExpiration;
            }
            set
            {
                m_entryExpiration = value;
                NotifyPropertyChanged("EntryExpiration");
            }
        }

        /// <remarks/>
        [XmlAttribute(AttributeName = "resetType")]
        public string ResetType
        {
            get
            {
                return m_resetType;
            }
            set
            {
                m_resetType = value;
                NotifyPropertyChanged("ResetType");
            }
        }

        private ObservableCollection<CategoryPermutation> m_categoryPermutations;
        private ObservableCollection<Column> m_columns;
        private string m_id;
        private string m_name;
        private string m_entryExpiration;
        private string m_resetType;
    }
}
