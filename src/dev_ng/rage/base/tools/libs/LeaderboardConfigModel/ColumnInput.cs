﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using GameConfig.Model.Core;

namespace GameConfig.Model
{
    /// <remarks/>
    [Serializable()]
    [XmlType(AnonymousType = true, TypeName = "ColumnInput")]
    public partial class ColumnInput : ModelBase
    {
        /// <remarks/>
        [XmlAttribute(AttributeName = "ordinal")]
        public string Ordinal
        {
            get
            {
                return m_ordinal;
            }
            set
            {
                m_ordinal = value;
                NotifyPropertyChanged("Ordinal");
            }
        }

        /// <remarks/>
        [XmlAttribute(AttributeName = "columnId")]
        public string ColumnId
        {
            get
            {
                return m_columnId;
            }
            set
            {
                m_columnId = value;
                NotifyPropertyChanged("ColumnId");
            }
        }

        private string m_ordinal;
        private string m_columnId;
    }
}
