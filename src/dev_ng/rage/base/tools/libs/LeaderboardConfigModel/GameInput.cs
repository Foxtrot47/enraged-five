﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using GameConfig.Model.Core;

namespace GameConfig.Model
{
    /// <remarks/>
    [Serializable()]
    [XmlType(AnonymousType = true, TypeName = "GameInput")]
    public partial class GameInput : ModelBase
    {
        /// <remarks/>
        [XmlAttribute(AttributeName = "inputId")]
        public string InputId
        {
            get
            {
                return m_inputId;
            }
            set
            {
                m_inputId = value;
                NotifyPropertyChanged("InputId");
            }
        }

        private string m_inputId;
    }
}
