﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using GameConfig.Model.Core;

namespace GameConfig.Model
{
    /// <remarks/>
    [Serializable()]
    [XmlType(AnonymousType = true, TypeName = "Aggregation")]
    public partial class Aggregation : ModelBase
    {
        /// <remarks/>
        [XmlElement("ColumnInput", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public ObservableCollection<ColumnInput> ColumnInput
        {
            get
            {
                return m_columnInput;
            }
            set
            {
                m_columnInput = value;
                NotifyPropertyChanged("ColumnInput");
            }
        }

        /// <remarks/>
        [XmlElement("GameInput", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public GameInput GameInput
        {
            get
            {
                return m_gameInput;
            }
            set
            {
                m_gameInput = value;
                NotifyPropertyChanged("GameInput");
            }
        }

        /// <remarks/>
        [XmlAttribute(AttributeName = "type")]
        public string Type
        {
            get
            {
                return m_type;
            }
            set
            {
                m_type = value;
                NotifyPropertyChanged("Type");
            }
        }

        /// <remarks/>
        [XmlAttribute(AttributeName = "conditionalOnColumnId")]
        public string ConditionalOnColumnId
        {
            get
            {
                return m_conditionalOnColumnId;
            }
            set
            {
                m_conditionalOnColumnId = value;
                NotifyPropertyChanged("ConditionalOnColumnId");
            }
        }

        private ObservableCollection<ColumnInput> m_columnInput;
        private GameInput m_gameInput;
        private string m_type;
        private string m_conditionalOnColumnId;
    }
}
