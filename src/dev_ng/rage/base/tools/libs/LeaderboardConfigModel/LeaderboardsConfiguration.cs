﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using GameConfig.Model.Core;

namespace GameConfig.Model
{
    /// <remarks/>
    [Serializable()]
    [XmlType(AnonymousType = true, TypeName = "LeaderboardsConfiguration")]
    public partial class LeaderboardsConfiguration : ModelBase
    {
        /// <remarks/>
        [XmlArray(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, ElementName = "Inputs")]
        [XmlArrayItem("Input", Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = false)]
        public ObservableCollection<Input> Inputs
        {
            get
            {
                return m_inputs;
            }
            set
            {
                m_inputs = value;
                NotifyPropertyChanged("Inputs");
            }
        }

        /// <remarks/>
        [XmlArray(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, ElementName = "Categories")]
        [XmlArrayItem("Category", Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = false)]
        public ObservableCollection<Category> Categories
        {
            get
            {
                return m_categories;
            }
            set
            {
                m_categories = value;
                NotifyPropertyChanged("Categories");
            }
        }

        /// <remarks/>
        [XmlArray(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, ElementName = "Leaderboards")]
        [XmlArrayItem("Leaderboard", Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = false)]
        public ObservableCollection<Leaderboard> Leaderboards
        {
            get
            {
                return m_leaderboards;
            }
            set
            {
                m_leaderboards = value;
                NotifyPropertyChanged("Leaderboards");
            }
        }

        private ObservableCollection<Input> m_inputs;
        private ObservableCollection<Category> m_categories;
        private ObservableCollection<Leaderboard> m_leaderboards;
    }
}
