﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using GameConfig.Model.Core;

namespace GameConfig.Model
{
    /// <remarks/>
    [Serializable()]
    [XmlType(AnonymousType = true, TypeName = "RockstarScsGameConfiguration")]
    [XmlRootAttribute(Namespace = "", ElementName = "RockstarScsGameConfiguration", IsNullable = false)]
    public partial class ScsGameConfiguration : ModelBase
    {
        /// <remarks/>
        [XmlElement(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, ElementName = "LeaderboardsConfiguration")]
        public LeaderboardsConfiguration LeaderboardsConfiguration
        {
            get
            {
                return m_leaderboardsConfiguration;
            }
            set
            {
                m_leaderboardsConfiguration = value;
                NotifyPropertyChanged("LeaderboardsConfiguration");
            }
        }

        /// <remarks/>
        [XmlAttribute("gameTitle")]
        public string GameTitle
        {
            get
            {
                return m_gameTitle;
            }
            set
            {
                m_gameTitle = value;
                NotifyPropertyChanged("GameTitle");
            }
        }

        /// <remarks/>
        [XmlAttribute("configVersion")]
        public string ConfigVersion
        {
            get
            {
                return m_configVersion;
            }
            set
            {
                m_configVersion = value;
                NotifyPropertyChanged("ConfigVersion");
            }
        }

        /// <remarks/>
        [XmlAttribute("fileVersion")]
        public int FileVersion
        {
            get
            {
                return m_fileVersion;
            }
            set
            {
                m_fileVersion = value;
                NotifyPropertyChanged("FileVersion");
            }
        }

        private LeaderboardsConfiguration m_leaderboardsConfiguration;
        private string m_gameTitle;
        private string m_configVersion;
        private int m_fileVersion;
    }
}