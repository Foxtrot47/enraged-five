LeadeboardConfigModel is the basic structure used by the Social Club Services to serialize and deserialized the leaderboard game configuration.

It's integrated from the rage/ROS/rage/tool directory.

It is used by the scgamerconfig2c tool for generating script (sch) and code (c++) files from the game config.