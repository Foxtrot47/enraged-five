﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Xml;

namespace GameConfig.Model.Core
{
    /// <summary>
    /// The base class of objects in the Model namespace. The Model namespace CONTAINS NO LOGIC.
    /// Marked as Serializable for Copy/Paste to/from the system clipboard. AllAttributes
    /// and AllElements are used to preserve unrecognized XML nodes.
    /// </summary>
    [Serializable()]
    public class ModelBase : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        public XmlAttribute[] AllAttributes
        {
            get
            {
                return m_allAttributes;
            }
            set
            {
                m_allAttributes = value;
                NotifyPropertyChanged("AllAttributes");
            }
        }

        public XmlElement[] AllElements
        {
            get
            {
                return m_allElements;
            }
            set
            {
                m_allElements = value;
                NotifyPropertyChanged("AllElements");
            }
        }

        private XmlAttribute[] m_allAttributes;
        private XmlElement[] m_allElements;
    }
}
