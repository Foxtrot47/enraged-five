﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using GameConfig.Model.Core;

namespace GameConfig.Model
{
    /// <remarks/>
    [Serializable()]
    [XmlType(AnonymousType = true, TypeName = "Column")]
    public partial class Column : ModelBase
    {
        /// <remarks/>
        [XmlElement(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, ElementName = "Aggregation")]
        public Aggregation Aggregation
        {
            get
            {
                return m_aggregation;
            }
            set
            {
                m_aggregation = value;
                NotifyPropertyChanged("Aggregation");
            }
        }

        /// <remarks/>
        [XmlAttribute(AttributeName = "id")]
        public string Id
        {
            get
            {
                return m_id;
            }
            set
            {
                m_id = value;
                NotifyPropertyChanged("Id");
            }
        }

        /// <remarks/>
        [XmlAttribute(AttributeName = "name")]
        public string Name
        {
            get
            {
                return m_name;
            }
            set
            {
                m_name = value;
                NotifyPropertyChanged("Name");
            }
        }

        /// <remarks/>
        [XmlAttribute(AttributeName = "ordinal")]
        public string Ordinal
        {
            get
            {
                return m_ordinal;
            }
            set
            {
                m_ordinal = value;
                NotifyPropertyChanged("Ordinal");
            }
        }

        /// <remarks/>
        [XmlAttribute(AttributeName = "isRankingColumn")]
        public string IsRankingColumn
        {
            get
            {
                return m_isRankingColumn;
            }
            set
            {
                m_isRankingColumn = value;
                NotifyPropertyChanged("IsRankingColumn");
            }
        }

        private Aggregation m_aggregation;
        private string m_id;
        private string m_name;
        private string m_ordinal;
        private string m_isRankingColumn;
    }
}
