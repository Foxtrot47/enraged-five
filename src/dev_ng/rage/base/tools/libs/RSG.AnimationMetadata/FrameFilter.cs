﻿namespace RSG.AnimationMetadata
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using RSG.Metadata.Model;
    using RSG.Metadata.Data;

    /// <summary>
    /// Defines a single frame filter.
    /// </summary>
    public class FrameFilter
    {
        #region Properties
        /// <summary>
        /// Gets the name of this filter.
        /// </summary>
        public string Name
        {
            get;
            internal set;
        }

        /// <summary>
        /// Gets all the tracks belonging to this filter.
        /// </summary>
        public FrameFilterTrackIndex[] TrackIndices
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the array of weights for this filter.
        /// </summary>
        public float[] Weights
        {
            get;
            private set;
        }
        #endregion

        #region Constructor
        public FrameFilter(StructureTunable filterTunable)
        {
            this.TrackIndices = new FrameFilterTrackIndex[0];
            this.Weights = new float[0];
            if (filterTunable == null)
                return;

            StringTunable nameTunable = filterTunable["Name"] as StringTunable;
            if (nameTunable != null)
                this.Name = nameTunable.Value;

            ArrayTunable trackIdIndicesTunable = filterTunable["TrackIdIndices"] as ArrayTunable;
            if (trackIdIndicesTunable != null)
            {
                this.TrackIndices = new FrameFilterTrackIndex[trackIdIndicesTunable.Items.Count];
                for (int i = 0; i < trackIdIndicesTunable.Items.Count; i++)
                {
                    this.TrackIndices[i] = new FrameFilterTrackIndex(trackIdIndicesTunable.Items[i] as StructureTunable);
                }
            }

            ArrayTunable weightsTunable = filterTunable["Weights"] as ArrayTunable;
            if (weightsTunable != null)
            {
                this.Weights = new float[weightsTunable.Items.Count];
                for (int i = 0; i < weightsTunable.Items.Count; i++)
                {
                    FloatTunable floatTunable = weightsTunable.Items[i] as FloatTunable;
                    if (floatTunable != null)
                        this.Weights[i] = floatTunable.Value;
                }
            }
        }
        #endregion

        #region Types
        /// <summary>
        /// 
        /// </summary>
        public class FrameFilterTrackIndex
        {
            #region Properties
            /// <summary>
            /// Gets the track count.
            /// </summary>
            public byte Track
            {
                get;
                internal set;
            }

            /// <summary>
            /// Gets the id for this track index.
            /// </summary>
            public string Id
            {
                get;
                private set;
            }

            /// <summary>
            /// Gets the weight for this track index.
            /// </summary>
            public byte Weight
            {
                get;
                private set;
            }
            #endregion

            #region Constructor
            public FrameFilterTrackIndex(StructureTunable trackIndexTunable)
            {
                if (trackIndexTunable == null)
                    return;

                U8Tunable trackTunable = trackIndexTunable["Track"] as U8Tunable;
                if (trackTunable != null)
                    this.Track = trackTunable.Value;

                EnumTunable idTunable = trackIndexTunable["Id"] as EnumTunable;
                if (idTunable != null)
                    this.Id = idTunable.ValueAsString;

                U8Tunable weightTunable = trackIndexTunable["WeightIndex"] as U8Tunable;
                if (weightTunable != null)
                    this.Weight = weightTunable.Value;
            }
            #endregion
        }
        #endregion
    }
}
