﻿using System;
using RSG.Metadata.Data;
using System.Collections.Generic;

namespace RSG.AnimationMetadata
{
    /// <summary>
    /// Defines a single property attribute in a tag/clip property definition.
    /// </summary>
    public class ClipAttributeDefinition
    {
        #region Fields
        /// <summary>
        /// The type of the structure tunable used to initialise this class.
        /// </summary>
        private const string StructureTypeName = @"CPropertyAttribute";

        /// <summary>
        /// The name of the structure tunables child used to initialise the
        /// <see cref="Name"/> property.
        /// </summary>
        private const string NameIdentifier = @"name";

        /// <summary>
        /// The name of the structure tunables child used to initialise the
        /// <see cref="UIName"/> property.
        /// </summary>
        private const string UINameIdentifier = @"ui_name";

        /// <summary>
        /// The name of the structure tunables child used to initialise the
        /// <see cref="UIDescription"/> property.
        /// </summary>
        private const string UIDescriptionIdentifier = @"ui_description";

        /// <summary>
        /// The name of the structure tunables child used to initialise the
        /// <see cref="Type"/> property.
        /// </summary>
        private const string TypeIdentifier = @"type";

        /// <summary>
        /// The name of the structure tunables child used to initialise the
        /// <see cref="InitialValue"/> property.
        /// </summary>
        private const string InitialValueIdentifier = @"initial_value";

        /// <summary>
        /// The name of the structure tunables child used to initialise the
        /// <see cref="IsBlendIn"/> property.
        /// </summary>
        private const string BlendInIdentifier = @"ui_blend_in";

        /// <summary>
        /// The name of the structure tunables child used to initialise the
        /// <see cref="IsBlendOut"/> property.
        /// </summary>
        private const string BlendOutIdentifier = @"ui_blend_out";

        /// <summary>
        /// The name of the structure tunables child used to initialise the
        /// <see cref="Favorites"/> property.
        /// </summary>
        private const string FavoritesIdentifier = @"favorites";
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="RSG.AnimationMetadata.ClipAttributeDefinition"/> class.
        /// </summary>
        /// <param name="tunable">
        /// The structure tunable used as a data provider to initialise this instance.
        /// </param>
        public ClipAttributeDefinition(StructureTunable tunable)
        {
            if (tunable == null || tunable.Definition.FriendlyTypeName != StructureTypeName)
                return;

            this.Name = (tunable[NameIdentifier] as StringTunable).Value;
            this.UIName = (tunable[UINameIdentifier] as StringTunable).Value;
            this.UIDescription = (tunable[UIDescriptionIdentifier] as StringTunable).Value;
            this.Type = (tunable[TypeIdentifier] as EnumTunable).Value;
            this.InitialValue = (tunable[InitialValueIdentifier] as StringTunable).Value;
            this.IsBlendIn = (bool)(tunable[BlendInIdentifier] as BoolTunable).Value;
            this.IsBlendOut = (bool)(tunable[BlendOutIdentifier] as BoolTunable).Value;

            ArrayTunable favorites = tunable[FavoritesIdentifier] as ArrayTunable;
            List<string> favoriteValues = new List<string>();
            if (favorites != null)
            {
                foreach (object item in favorites.Items)
                {
                    if (!(item is StringTunable))
                        continue;

                    favoriteValues.Add((item as StringTunable).Value);
                }
            }

            this.Favorites = favoriteValues.ToArray();
        }
        #endregion

        #region Properties
        /// <summary>
        /// The name given to this attribute.
        /// </summary>
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// The ui name given to this attribute.
        /// </summary>
        public string UIName
        {
            get;
            set;
        }

        /// <summary>
        /// The description given to this attribute.
        /// </summary>
        public string UIDescription
        {
            get;
            set;
        }

        /// <summary>
        /// The type of attribute this represents.
        /// </summary>
        public Int64 Type
        {
            get;
            set;
        }

        /// <summary>
        /// The initial/default value for this attribute when first created.
        /// </summary>
        public string InitialValue
        {
            get;
            set;
        }

        public bool IsBlendIn
        {
            get;
            private set;
        }

        public bool IsBlendOut
        {
            get;
            private set;
        }

        /// <summary>
        /// A list of favourite values this attribute can have and are given to
        /// user as options.
        /// </summary>
        public string[] Favorites
        {
            get;
            set;
        }
        #endregion
    }
}
