﻿namespace RSG.AnimationMetadata
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;

    using RSG.Metadata.Model;
    using RSG.Metadata.Data;
    using RSG.Metadata.Util;

    using RSG.Base.ConfigParser;
    using System.IO;
    using RSG.Base.Configuration;

    /// <summary>
    /// Contains the frame dictionaries used as filtering options in MoVE.
    /// </summary>
    public static class FrameFilterDictionaries
    {
        #region Properties
        /// <summary>
        /// Gets all the frame filter dictionaries indexed by name.
        /// </summary>
        public static Dictionary<string, FrameFilterDictionary> Dictionaries
        {
            get
            {
                if (dictionaries == null)
                {
                    dictionaries = LoadFilterDefinitions();
                }
                return dictionaries;
            }
        }
        static Dictionary<string, FrameFilterDictionary> dictionaries;
        #endregion

        #region Private Functions
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private static Dictionary<string, FrameFilterDictionary> LoadFilterDefinitions()
        {
            var loadedDictionaries = new Dictionary<string, FrameFilterDictionary>();

            IConfig config = ConfigFactory.CreateConfig();
            string path = Path.Combine(config.Project.DefaultBranch.Metadata, "definitions", "rage", "framework", "src", "fwanimation");
            string file = Path.Combine(config.Project.DefaultBranch.Assets, "anim", "filters", "filters.xml");
            StructureDictionary structDict = new StructureDictionary();
            structDict.Load(config.Project.DefaultBranch, path, true);
            MetaFile metafile = new MetaFile(file, structDict);

            ITunable rootTunable = metafile.Members["fwFrameFilterDictionaryStore"];
            if (!(rootTunable is StructureTunable))
                return loadedDictionaries;

            ArrayTunable dictionariesTunable = (rootTunable as StructureTunable)["FrameFilterDictionaries"] as ArrayTunable;
            if (dictionariesTunable == null)
                return loadedDictionaries;

            foreach (ITunable item in dictionariesTunable.Items)
            {
                FrameFilterDictionary newDictionary = new FrameFilterDictionary(item as StructureTunable);
                if (!loadedDictionaries.ContainsKey(newDictionary.Name))
                {
                    loadedDictionaries.Add(newDictionary.Name, newDictionary);
                }
            }

            return loadedDictionaries;
        }
        #endregion // Public Functions
    }
}
