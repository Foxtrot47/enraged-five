﻿using System.Collections.Generic;
using RSG.Base.ConfigParser;
using RSG.Metadata.Data;
using RSG.Metadata.Model;
using RSG.Metadata.Util;
using RSG.Base.Configuration;
using System;

namespace RSG.AnimationMetadata
{
    /// <summary>
    /// Static class that provides the clip and tag property definitions.
    /// </summary>
    public static class ClipTagDefinitions
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="PropertyDescriptions"/> property.
        /// </summary>
        private static Dictionary<string, ClipPropertyDefinition> tagPropertyDescriptions;

        /// <summary>
        /// The private field used for the <see cref="ClipPropertyDescriptions"/> property.
        /// </summary>
        private static Dictionary<string, ClipPropertyDefinition> clipPropertyDescriptions;

        /// <summary>
        /// The object to be used to manage access across multiple threads.
        /// </summary>
        private static object syncRoot = new object();
        #endregion

        #region Properties
        /// <summary>
        /// Gets the property descriptions for the tags.
        /// </summary>
        public static Dictionary<string, ClipPropertyDefinition> PropertyDescriptions
        {
            get
            {
                lock (syncRoot)
                {
                    if (tagPropertyDescriptions == null)
                    {
                        LoadProperties();
                    }
                }

                return tagPropertyDescriptions;
            }
        }

        /// <summary>
        /// Gets the property descriptions for the clip.
        /// </summary>
        public static Dictionary<string, ClipPropertyDefinition> ClipPropertyDescriptions
        {
            get
            {
                lock (syncRoot)
                {
                    if (clipPropertyDescriptions == null)
                    {
                        LoadProperties();
                    }
                }

                return clipPropertyDescriptions;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Loads both the clip and tag property definitions.
        /// </summary>
        private static void LoadProperties()
        {
            try
            {
                var sortedClipProperties = new SortedDictionary<string, ClipPropertyDefinition>();
                var sortedtagProperties = new SortedDictionary<string, ClipPropertyDefinition>();
                IConfig config = ConfigFactory.CreateConfig();
                string path = config.Project.DefaultBranch.Metadata + "\\definitions\\tools\\animation\\";
                string tagFile = config.ToolsConfig + "\\config\\anim\\clip_tag_definitions.xml";
                string clipFile = config.ToolsConfig + "\\config\\anim\\clip_property_definitions.xml";

                StructureDictionary structDict = new StructureDictionary();

                structDict.Load(config.Project.DefaultBranch, path, true);
                MetaFile metaFile = new MetaFile(tagFile, structDict);
                ITunable rootTunable = metaFile.Members["CPropertyDefs"];

                ArrayTunable propertiesTunable = Tunable.FindFirstStuctureNamed("properties", metaFile.Members) as ArrayTunable;
                foreach (StructureTunable tunable in propertiesTunable)
                {
                    ClipPropertyDefinition newDef = new ClipPropertyDefinition(tunable);
                    sortedtagProperties.Add(newDef.Name, newDef);
                }

                metaFile = new MetaFile(clipFile, structDict);
                rootTunable = metaFile.Members["CPropertyDefs"];

                propertiesTunable = Tunable.FindFirstStuctureNamed("properties", metaFile.Members) as ArrayTunable;
                foreach (StructureTunable tunable in propertiesTunable)
                {
                    ClipPropertyDefinition newDef = new ClipPropertyDefinition(tunable);
                    sortedClipProperties.Add(newDef.Name, newDef);
                }

                clipPropertyDescriptions = new Dictionary<string, ClipPropertyDefinition>(sortedClipProperties);
                tagPropertyDescriptions = new Dictionary<string, ClipPropertyDefinition>(sortedtagProperties);
            }
            catch (KeyNotFoundException ex)
            {
                clipPropertyDescriptions = new Dictionary<string, ClipPropertyDefinition>();
                tagPropertyDescriptions = new Dictionary<string, ClipPropertyDefinition>();
                RSG.Base.Logging.Log.Log__Warning("Unable to fully load clip properties. This can cause unexpected behaviour. Please grab the latest assets/metadata/definitions/ and try again.");
            }
            catch (Exception ex)
            {
                clipPropertyDescriptions = new Dictionary<string, ClipPropertyDefinition>();
                tagPropertyDescriptions = new Dictionary<string, ClipPropertyDefinition>();
                RSG.Base.Logging.Log.Log__Exception(ex, "Unable to fully load clip properties. This can cause unexpected behaviour. Please grab the latest assets/metadata/definitions/ and try again.");
            }
        }
        #endregion
    }
}
