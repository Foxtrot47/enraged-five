﻿namespace RSG.AnimationMetadata
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using RSG.Metadata.Model;
using RSG.Metadata.Data;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class FrameFilterDictionary
    {
        #region Properties
        /// <summary>
        /// Gets the name of this dictionary.
        /// </summary>
        public string Name
        {
            get;
            internal set;
        }

        /// <summary>
        /// Gets all the frame filters belonging to this dictionary.
        /// </summary>
        public FrameFilter[] FrameFilters
        {
            get;
            private set;
        }
        #endregion

        #region Constructors
        public FrameFilterDictionary(StructureTunable  dictionaryTunable)
        {
            this.FrameFilters = new FrameFilter[0];
            if (dictionaryTunable == null)
                return;

            StringTunable nameTunable = dictionaryTunable["Name"] as StringTunable;
            if (nameTunable != null)
                this.Name = nameTunable.Value;

            ArrayTunable frameFiltersTunable = dictionaryTunable["FrameFilters"] as ArrayTunable;
            if (frameFiltersTunable != null)
            {
                this.FrameFilters = new FrameFilter[frameFiltersTunable.Items.Count];
                for (int i = 0; i < frameFiltersTunable.Items.Count; i++)
                {
                    this.FrameFilters[i] = new FrameFilter(frameFiltersTunable.Items[i] as StructureTunable);
                }
            }
        }
        #endregion
    }
}
