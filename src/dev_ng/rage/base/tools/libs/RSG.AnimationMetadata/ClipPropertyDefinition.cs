﻿using System;
using System.Linq;
using System.Collections.ObjectModel;
using RSG.Metadata.Data;
using System.Collections.Generic;

namespace RSG.AnimationMetadata
{
    /// <summary>
    /// Defines a single property on a tag or clip definition.
    /// </summary>
    public class ClipPropertyDefinition
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="RSG.AnimationMetadata.ClipPropertyDefinition"/> class.
        /// </summary>
        /// <param name="tunable">
        /// The structure tunable used as a data provider to initialise this instance.
        /// </param>
        public ClipPropertyDefinition(StructureTunable tunable)
        {
            AttributeDefs = new List<ClipAttributeDefinition>();
            PropertyGroups = new List<PropertyGroup>();

            this.Name = (tunable["name"] as StringTunable).Value;

            this.Enabled = "True";
            if (tunable.ContainsKey("allow_edit"))
            {
                this.Enabled = (tunable["allow_edit"] as StringTunable).Value;
            }

            if (this.Enabled == null)
            {
                this.Enabled = "True";
            }

            if (tunable.ContainsKey("min_frame_length"))
            {
                this.MinimumFrameLength = (tunable["min_frame_length"] as IntTunable).Value;
            }
            else
            {
                this.MinimumFrameLength = 0;
            }

            if (tunable.ContainsKey("max_frame_length"))
            {
                this.MaximumFrameLength = (tunable["max_frame_length"] as IntTunable).Value;
            }
            else
            {
                this.MaximumFrameLength = int.MaxValue;
            }

            this.BackgroundColour = (tunable["backgroundColour"] as Color32Tunable);
            this.DefaultTagColour = (tunable["tagColour"] as Color32Tunable);
            this.InitialFrameLength = (tunable["initial_length"] as FloatTunable).Value;
            bool inFrames = (bool)(tunable["initial_length_in_frames"] as BoolTunable).Value;
            if (!inFrames)
            {
                double length = this.InitialFrameLength / (100.0 / 3.0);
                this.InitialFrameLength = (float)length;
            }

            ArrayTunable properties = (tunable["properties"] as ArrayTunable);
            foreach (StructureTunable attributeTunable in properties)
            {
                ClipAttributeDefinition attrDefinition = new ClipAttributeDefinition(attributeTunable);
                this.AttributeDefs.Add(attrDefinition);
            }

            ArrayTunable groups = (tunable["groups"] as ArrayTunable);
            foreach (StructureTunable groupTunable in groups)
            {
                PropertyGroup group = new PropertyGroup(groupTunable);
                this.PropertyGroups.Add(group);
            }
        }
        #endregion // Constructors

        #region Public Functions
        
        #endregion // PubliC Functions

        #region Properties
        public string Name
        {
            get;
            private set;
        }

        public string Enabled
        {
            get;
            private set;
        }

        public int MinimumFrameLength
        {
            get;
            private set;
        }

        public int MaximumFrameLength
        {
            get;
            private set;
        }

        public float InitialFrameLength
        {
            get;
            private set;
        }

        public List<ClipAttributeDefinition> AttributeDefs
        {
            get;
            private set;
        }

        public Color32Tunable BackgroundColour
        {
            get;
            private set;
        }

        public Color32Tunable DefaultTagColour
        {
            get;
            private set;
        }

        public List<PropertyGroup> PropertyGroups
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Classes
        public class PropertyGroup
        {
            #region Constructors
            /// <summary>
            /// Initialises a new instance of the
            /// <see cref="RSG.AnimationMetadata.ClipPropertyDefinition.PropertyGroup"/> class.
            /// </summary>
            /// <param name="tunable">
            /// The structure tunable used as a data provider to initialise this instance.
            /// </param>
            public PropertyGroup(StructureTunable tunable)
            {
                PropertyValues = new Dictionary<string, string>();

                this.Name = (tunable["name"] as StringTunable).Value;
                this.Colour = (tunable["colour"] as Color32Tunable);
                this.ButtonVisibility = null;
                if (tunable.ContainsKey("button_visibility"))
                {
                    this.ButtonVisibility = (tunable["button_visibility"] as StringTunable).Value;
                }

                if (this.ButtonVisibility == null)
                {
                    this.ButtonVisibility = "Visible";
                }
                ArrayTunable propertyValues = (tunable["values"] as ArrayTunable);
                foreach (StructureTunable propertyValue in propertyValues)
                {
                    string name = (propertyValue["name"] as StringTunable).Value;
                    string value = (propertyValue["value_as_string"] as StringTunable).Value;
                    this.PropertyValues.Add(name, value);
                }
            }
            #endregion

            #region Properties
            public string Name
            {
                get;
                private set;
            }

            public Color32Tunable Colour
            {
                get;
                set;
            }

            public string ButtonVisibility
            {
                get;
                set;
            }

            public Dictionary<string, string> PropertyValues
            {
                get;
                private set;
            }
            #endregion
        }
        #endregion
    }
}
