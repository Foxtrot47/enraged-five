﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using RSG.Base.Configuration;
using RSG.Base.Logging.Universal;
using RSG.Base.Math;
using RSG.ManagedRage.File;
using RSG.Metadata.Data;
using RSG.Metadata.Model;
using RSG.Metadata.Parser;
using RSG.Rage.Entity;
using RSG.Rage.Shader;

namespace RSG.Model.MaterialPresets
{
    public class MaterialPreset
    {
        #region Constants
        private const string m_StructureName = "MaterialPreset";
        private const string m_ParentTunableName = "Parent";
        private const string m_TextureMapsTunableName = "TexMaps";
        private const string m_FloatsTunableName = "Floats";
        private const string m_IntsTunableName = "Ints";
        private const string m_Vector4sTunableName = "Vec4s";
        private const string m_Vector3sTunableName = "Vec3s";
        private const string m_Vector2sTunableName = "Vec2s";
        private const string m_PropertiesTunableName = "Properties";

        private const string m_NameTunable = "Name";
        private const string m_IndexTunable = "Index";
        private const string m_ValueTunable = "Value";
        private const string m_VariableNameTunable = "VariableName";
        private const string m_FriendlyNameTunable = "FriendlyName";
        private const string m_TextureTemplateTunable = "TextureTemplate";

        private static readonly String LOG_CTX = "Material Preset";
        #endregion

        #region Static Properties
        public static String MaterialObjectExtension = ".mpo";
        public static String MaterialSurfaceExtension = ".mps";
        public static String MaterialTemplateExtension = ".mpt";
        public static String MaterialTemplateExportExtension = ".mpi";

        private static StructureDictionary MetadataDictionary = null;
        #endregion

        #region Public Properties
        public IBranch Branch;
        public string Name { get { return m_Name; } }
        public int NumVariables { get { return (TextureMaps.Length + Floats.Length + Ints.Length + Vec4s.Length + Vec3s.Length + Vec2s.Length); } }
		
		public ArrayTunable TextureMaps { get; private set; }
        public ArrayTunable Floats { get; private set; }
        public ArrayTunable Ints { get; private set; }
        public ArrayTunable Vec4s { get; private set; }
        public ArrayTunable Vec3s { get; private set; }
        public ArrayTunable Vec2s { get; private set; }
        #endregion

        #region Private Members
        private static IUniversalLog LogTarget;

        private MetaFile m_MetadataFile;
        private StructureTunable m_Root;
        private StructureTunable m_Properties;

        private String m_Name;
        private int m_Index;
        #endregion

        #region Constructors
        /// <summary>
        /// Basic constructor.
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="metaFile"></param>
        /// <param name="log"></param>
        public MaterialPreset(IBranch branch, MetaFile metaFile, IUniversalLog log)
        {
            Branch = branch;
            LogTarget = log;
            m_MetadataFile = metaFile;
            m_Root = m_MetadataFile.Members[m_StructureName] as StructureTunable;
            TextureMaps = m_Root[m_TextureMapsTunableName] as ArrayTunable;
            Floats = m_Root[m_FloatsTunableName] as ArrayTunable;
            Ints = m_Root[m_IntsTunableName] as ArrayTunable;
            Vec4s = m_Root[m_Vector4sTunableName] as ArrayTunable;
            Vec3s = m_Root[m_Vector3sTunableName] as ArrayTunable;
            Vec2s = m_Root[m_Vector2sTunableName] as ArrayTunable;
            m_Properties = m_Root[m_PropertiesTunableName] as StructureTunable;

            if (m_Root.ContainsKey(m_FriendlyNameTunable))
                m_Name = (m_Root[m_FriendlyNameTunable] as StringTunable).Value; 
            else
                m_Name = null;

            if (m_Root.ContainsKey(m_IndexTunable))
                m_Index = (m_Root[m_IndexTunable] as IntTunable).Value;
            else
                m_Index = -1;
        }
        #endregion

        #region Private Functions
        /// <summary>
        /// Validates the specific tunables against the parent preset.
        /// This will fix up any indices that may have changed through the parent and will remove
        /// any shader parameters that have been removed in the parent.
        /// Any newly shader parameters will be automatically added by a previous call to ExpandPreset.
        /// </summary>
        /// <param name="shaderTunables"></param>
        /// <param name="parentPreset"></param>
        private void ValidateShaderTunables(ArrayTunable shaderTunables, ArrayTunable parentShaderTunables)
        {
            List<ITunable> removeTunables = new List<ITunable>();
            foreach (ITunable tunable in shaderTunables)
            {
                StructureTunable tunableStructure = tunable as StructureTunable;
                StringTunable variableNameTunable = tunableStructure[m_VariableNameTunable] as StringTunable;
                IntTunable variableIndexTunable = tunableStructure[m_IndexTunable] as IntTunable;
                bool foundParameter = false;
                foreach (ITunable parentTunable in parentShaderTunables)
                {
                    StructureTunable parentTunableStructure = parentTunable as StructureTunable;
                    StringTunable parentVariableNameTunable = parentTunableStructure[m_VariableNameTunable] as StringTunable;

                    if (String.Compare(parentVariableNameTunable.Value, variableNameTunable.Value, true) == 0)
                    {
                        IntTunable parentVariableIndexTunable = parentTunableStructure[m_IndexTunable] as IntTunable;
                        if (variableIndexTunable.Value != parentVariableIndexTunable.Value)
                        {
                            LogTarget.WarningCtx(LOG_CTX, String.Format("Preset variable {0}'s index does not match the same parameter index in the parent.", (m_Root[m_ParentTunableName] as StringTunable).Value));

                            variableIndexTunable.Value = parentVariableIndexTunable.Value;
                        }

                        foundParameter = true;
                        break;
                    }
                }

                if (foundParameter == false)
                {
                    removeTunables.Add(tunable);
                }
            }

            removeTunables.ForEach(tunable => TextureMaps.Remove(tunable));
        }
        #endregion 

        #region Public Functions
        /// <summary>
        /// Expands the preset into a fully specified one.  Merged its parents' data into this one.
        /// </summary>
        /// <returns></returns>
        public bool ExpandPreset()
        {
            List<MaterialPreset> m_Parents = new List<MaterialPreset>();
            ITunableStructure materialPreset = m_Root;

            MaterialPreset parent = GetParentPreset();
            while (parent != null)
            {
                m_Parents.Add(parent);
                parent = parent.GetParentPreset();
            }

            //Scan through all presets values top-down to fill out the values.
            foreach (MaterialPreset currentParent in m_Parents)
            {
                foreach (ITunable parentTextureMap in currentParent.TextureMaps)
                {
                    ITunableStructure parentTextureMapStructure = parentTextureMap as ITunableStructure;
                    StringTunable parentTextureMapName = parentTextureMapStructure[m_NameTunable] as StringTunable;

                    ITunable textureMapTunable = GetTextureMap(parentTextureMapName.Value);
                    if (textureMapTunable == null)
                    {
                        //Add this tunable to the list. 
                        TextureMaps.Add(parentTextureMapStructure);
                    }
                    // Even though we already have a texture, we need to expand the texture template.
                    else
                    {
                        ITunableStructure currentTextureMapStructure = textureMapTunable as ITunableStructure;
                        StringTunable currentTextureTemplateTunable = currentTextureMapStructure[m_TextureTemplateTunable] as StringTunable;
                        String currentTextureTemplate = currentTextureTemplateTunable.Value;

                        if (String.IsNullOrEmpty(currentTextureTemplate))
                        {
                            currentTextureMapStructure[m_TextureTemplateTunable] = parentTextureMapStructure[m_TextureTemplateTunable];
                        }

                        IntTunable sourceIndexTunable = currentTextureMapStructure[m_IndexTunable] as IntTunable;
                        IntTunable  parentIndexTunable = parentTextureMapStructure[m_IndexTunable] as IntTunable;
                        if (sourceIndexTunable.Value != parentIndexTunable.Value)
                        {
                            sourceIndexTunable.Value = parentIndexTunable.Value;
                        }
                    }
                }

                foreach (ITunable parentFloatMap in currentParent.Floats)
                {
                    ITunableStructure parentFloatStructure = parentFloatMap as ITunableStructure;
                    StringTunable parentFloatName = parentFloatStructure[m_NameTunable] as StringTunable;

                    ITunable floatTunable = GetFloat(parentFloatName.Value);
                    if (floatTunable == null)
                    {
                        //Add this tunable to the list. 
                        Floats.Add(parentFloatStructure);
                    }
                    else
                    {
                        IntTunable sourceIndexTunable = (floatTunable as ITunableStructure)[m_IndexTunable] as IntTunable;
                        IntTunable parentIndexTunable = parentFloatStructure[m_IndexTunable] as IntTunable;
                        if (sourceIndexTunable.Value != parentIndexTunable.Value)
                        {
                            sourceIndexTunable.Value = parentIndexTunable.Value;
                        }
                    }
                }

                foreach (ITunable parentInt in currentParent.Ints)
                {
                    ITunableStructure parentIntStructure = parentInt as ITunableStructure;
                    StringTunable parentIntName = parentIntStructure[m_NameTunable] as StringTunable;

                    ITunable intTunable = GetInt(parentIntName.Value);
                    if (intTunable == null)
                    {
                        //Add this tunable to the list. 
                        Ints.Add(parentIntStructure);
                    }
                    else
                    {
                        IntTunable sourceIndexTunable = (intTunable as ITunableStructure)[m_IndexTunable] as IntTunable;
                        IntTunable parentIndexTunable = parentIntStructure[m_IndexTunable] as IntTunable;
                        if (sourceIndexTunable.Value != parentIndexTunable.Value)
                        {
                            sourceIndexTunable.Value = parentIndexTunable.Value;
                        }
                    }
                }

                foreach (ITunable parentVector in currentParent.Vec2s)
                {
                    ITunableStructure parentVectorStructure = parentVector as ITunableStructure;
                    StringTunable parentVectorName = parentVectorStructure[m_NameTunable] as StringTunable;

                    ITunable vectorTunable = GetVector2(parentVectorName.Value);
                    if (vectorTunable == null)
                    {
                        //Add this tunable to the list. 
                        Vec2s.Add(parentVectorStructure);
                    }
                    else
                    {
                        IntTunable sourceIndexTunable = (vectorTunable as ITunableStructure)[m_IndexTunable] as IntTunable;
                        IntTunable parentIndexTunable = parentVectorStructure[m_IndexTunable] as IntTunable;
                        if (sourceIndexTunable.Value != parentIndexTunable.Value)
                        {
                            sourceIndexTunable.Value = parentIndexTunable.Value;
                        }
                    }
                }

                foreach (ITunable parentVector in currentParent.Vec3s)
                {
                    ITunableStructure parentVectorStructure = parentVector as ITunableStructure;
                    StringTunable parentVectorName = parentVectorStructure[m_NameTunable] as StringTunable;

                    ITunable vectorTunable = GetVector3(parentVectorName.Value);
                    if (vectorTunable == null)
                    {
                        //Add this tunable to the list. 
                        Vec3s.Add(parentVectorStructure);
                    }
                    else
                    {
                        IntTunable sourceIndexTunable = (vectorTunable as ITunableStructure)[m_IndexTunable] as IntTunable;
                        IntTunable parentIndexTunable = parentVectorStructure[m_IndexTunable] as IntTunable;
                        if (sourceIndexTunable.Value != parentIndexTunable.Value)
                        {
                            sourceIndexTunable.Value = parentIndexTunable.Value;
                        }
                    }
                }

                foreach (ITunable parentVector in currentParent.Vec4s)
                {
                    ITunableStructure parentVectorStructure = parentVector as ITunableStructure;
                    StringTunable parentVectorName = parentVectorStructure[m_NameTunable] as StringTunable;

                    ITunable vectorTunable = GetVector4(parentVectorName.Value);
                    if (vectorTunable == null)
                    {
                        //Add this tunable to the list. 
                        Vec4s.Add(parentVectorStructure);
                    }
                    else
                    {
                        IntTunable sourceIndexTunable = (vectorTunable as ITunableStructure)[m_IndexTunable] as IntTunable;
                        IntTunable parentIndexTunable = parentVectorStructure[m_IndexTunable] as IntTunable;
                        if (sourceIndexTunable.Value != parentIndexTunable.Value)
                        {
                            sourceIndexTunable.Value = parentIndexTunable.Value;
                        }
                    }
                }
            }

            // The properties should only be set from the highest level preset (.mpt file).
            // These include drawbucket, etc so these come directly from the shader.
            if (m_Parents.Count > 0) 
            {
                MaterialPreset topLevelParent = m_Parents[m_Parents.Count - 1];

                foreach (KeyValuePair<String, ITunable> parentProperty in topLevelParent.m_Properties)
                {
                    ITunable propertyTunable = GetProperty(parentProperty.Key);
                    if (propertyTunable == null)
                    {
                        //Add this tunable to the list. 
                        m_Properties.Add(parentProperty.Key, propertyTunable);
                    }
                    else
                    {
                        m_Properties[parentProperty.Key] = propertyTunable;
                    }
                }
            }
            
            return true;
        }

        /// <summary>
        /// Returns the file name of the current metadata file.
        /// </summary>
        /// <returns></returns>
        public String GetFilename()
        {
            return m_MetadataFile.Filename;
        }

        /// <summary>
        /// Returns the direct parent of this material preset.
        /// </summary>
        /// <returns></returns>
        public MaterialPreset GetParentPreset()
        {
            ITunableStructure materialPreset = m_MetadataFile.Members["MaterialPreset"] as ITunableStructure;

            foreach (KeyValuePair<String, ITunable> tunable in materialPreset)
            {
                if (String.Compare(tunable.Key, m_ParentTunableName) == 0)
                {
                    StringTunable parentTunable = tunable.Value as StringTunable;
                    if (parentTunable.Value != null)
                    {
                        // Resolve any $(asset) references in the parent templates.
                        String parentFilePath = Branch.Environment.Subst(parentTunable.Value);
                        if (File.Exists(parentFilePath) == false)
                        {
                            // Try to resolve it if it's relative to the metadata/materials folder.
                            parentFilePath = Path.Combine(Branch.Metadata, "materials", parentTunable.Value);
                            if (File.Exists(parentFilePath) == false)
                                return null;
                        }

                        MaterialPreset parent;
                        if (LoadPreset(Branch, LogTarget, parentFilePath, out parent, Branch.Project.Config) )
                        {
                            return parent;
                        }
                    }

                    return null;
                }
            }

            return null;
        }

        /// <summary>
        /// Returns a list of textures within the preset.  Can work on partial and expanded presets.
        /// </summary>
        /// <param name="textureMaps"></param>
        public void GetTextureMapEntries(ref IDictionary<string, string> textureMaps)
        {
            if (textureMaps == null)
                return;

            foreach (ITunable textureMap in TextureMaps)
            {
                ITunableStructure textureMapStructure = textureMap as ITunableStructure;
                StringTunable textureMapTunable = textureMapStructure[m_ValueTunable] as StringTunable;
                StringTunable textureTemplateTunable = textureMapStructure[m_TextureTemplateTunable] as StringTunable;

                // Need to add this as a fallback in case for whatever reason there is no texture template set anywhere through the chain.
                String textureTemplateName = "Default";
                if (textureTemplateTunable.Value != null)
                    textureTemplateName = textureTemplateTunable.Value;

                textureMaps.Add(textureMapTunable.Value, textureTemplateName);
            }
        }

        /// <summary>
        /// Returns the tunable associated with a specific texture slot.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public ITunable GetTextureMap(string value)
        {
            foreach (ITunable textureMap in TextureMaps)
            {
                ITunableStructure textureMapStructure = textureMap as ITunableStructure;
                StringTunable textureMapName = textureMapStructure[m_NameTunable] as StringTunable;

                if (value.Equals(textureMapName.Value, StringComparison.OrdinalIgnoreCase))
                {
                    return textureMap;
                }
            }

            return null;
        }

        /// <summary>
        /// Removes the texture map by parameter name.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool RemoveTextureMap(string value)
        {
            foreach (ITunable textureMap in TextureMaps)
            {
                ITunableStructure textureMapStructure = textureMap as ITunableStructure;
                StringTunable textureMapName = textureMapStructure[m_NameTunable] as StringTunable;

                if (value.Equals(textureMapName.Value, StringComparison.OrdinalIgnoreCase))
                {
                    TextureMaps.Remove(textureMapStructure);
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Returns the float tunable associated with the parameter name.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public ITunable GetFloat(string value)
        {
            foreach (ITunable floatTunable in Floats)
            {
                ITunableStructure floatStructure = floatTunable as ITunableStructure;
                StringTunable floatName = floatStructure[m_NameTunable] as StringTunable;

                if (value.Equals(floatName.Value, StringComparison.OrdinalIgnoreCase))
                {
                    return floatTunable;
                }
            }

            return null;
        }

        /// <summary>
        /// Removes the float tunable associated with the parameter name.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool RemoveFloat(string value)
        {
            foreach (ITunable floatTunable in Floats)
            {
                ITunableStructure floatStructure = floatTunable as ITunableStructure;
                StringTunable floatName = floatStructure[m_NameTunable] as StringTunable;

                if (value.Equals(floatName.Value, StringComparison.OrdinalIgnoreCase))
                {
                    Floats.Remove(floatTunable);
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Returns the int tunable associated with the parameter name.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public ITunable GetInt(string value)
        {
            foreach (ITunable intTunable in Ints)
            {
                ITunableStructure intStructure = intTunable as ITunableStructure;
                StringTunable intName = intStructure[m_NameTunable] as StringTunable;

                if (value.Equals(intName.Value, StringComparison.OrdinalIgnoreCase))
                {
                    return intTunable;
                }
            }

            return null;
        }

        /// <summary>
        /// Removes the int tunable associated with the parameter name.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool RemoveInt(string value)
        {
            foreach (ITunable intTunable in Ints)
            {
                ITunableStructure intStructure = intTunable as ITunableStructure;
                StringTunable intName = intStructure[m_NameTunable] as StringTunable;

                if (value.Equals(intName.Value, StringComparison.OrdinalIgnoreCase))
                {
                    Ints.Remove(intTunable);
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Returns the vector2 tunable associated with the parameter name.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public ITunable GetVector2(string value)
        {
            foreach (ITunable vec2Tunable in Vec2s)
            {
                ITunableStructure vec2Structure = vec2Tunable as ITunableStructure;
                StringTunable vec2Name = vec2Structure[m_NameTunable] as StringTunable;

                if (value.Equals(vec2Name.Value, StringComparison.OrdinalIgnoreCase))
                {
                    return vec2Tunable;
                }
            }

            return null;
        }

        /// <summary>
        /// Removes the vector2 tunable associated with the parameter name.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool RemoveVector2(string value)
        {
            foreach (ITunable vec2Tunable in Vec2s)
            {
                ITunableStructure vec2Structure = vec2Tunable as ITunableStructure;
                StringTunable vec2Name = vec2Structure[m_NameTunable] as StringTunable;

                if (value.Equals(vec2Name.Value, StringComparison.OrdinalIgnoreCase))
                {
                    Vec2s.Remove(vec2Tunable);
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Returns the vector3 tunable associated with the parameter name.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public ITunable GetVector3(string value)
        {
            foreach (ITunable vec3Tunable in Vec3s)
            {
                ITunableStructure vec3Structure = vec3Tunable as ITunableStructure;
                StringTunable vec3Name = vec3Structure[m_NameTunable] as StringTunable;

                if (value.Equals(vec3Name.Value, StringComparison.OrdinalIgnoreCase))
                {
                    return vec3Tunable;
                }
            }

            return null;
        }

        /// <summary>
        /// Removes the vector3 tunable associated with the parameter name.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool RemoveVector3(string value)
        {
            foreach (ITunable vec3Tunable in Vec3s)
            {
                ITunableStructure vec3Structure = vec3Tunable as ITunableStructure;
                StringTunable vec3Name = vec3Structure[m_NameTunable] as StringTunable;

                if (value.Equals(vec3Name.Value, StringComparison.OrdinalIgnoreCase))
                {
                    Vec3s.Remove(vec3Tunable);
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Returns the vector2 tunable associated with the parameter name.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public ITunable GetVector4(string value)
        {
            foreach (ITunable vec4Tunable in Vec4s)
            {
                ITunableStructure vec4Structure = vec4Tunable as ITunableStructure;
                StringTunable vec4Name = vec4Structure[m_NameTunable] as StringTunable;

                if (value.Equals(vec4Name.Value, StringComparison.OrdinalIgnoreCase))
                {
                    return vec4Tunable;
                }
            }

            return null;
        }

        /// <summary>
        /// Removes the vector4 tunable associated with the parameter name.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool RemoveVector4(string value)
        {
            foreach (ITunable vec4Tunable in Vec4s)
            {
                ITunableStructure vec4Structure = vec4Tunable as ITunableStructure;
                StringTunable vec4Name = vec4Structure[m_NameTunable] as StringTunable;

                if (value.Equals(vec4Name.Value, StringComparison.OrdinalIgnoreCase))
                {
                    Vec4s.Remove(vec4Tunable);
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Returns the tunable associated with the material preset property.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public ITunable GetProperty(string value)
        {
            foreach (KeyValuePair<String, ITunable> propertyTunable in m_Properties)
            {
                if (value.Equals(propertyTunable.Key, StringComparison.OrdinalIgnoreCase))
                {
                    return propertyTunable.Value;
                }
            }

            return null;
        }


        /// <summary>
        /// Validates the shader preset, fixing up any indices that may have changed, adding any 
        /// missing parameters and removes any parameters.
        /// </summary>
        public bool FixShaderIndices()
        {
            StringTunable parentNameTunable = m_Root[m_ParentTunableName] as StringTunable;
            String parentFilePath = Path.Combine(Branch.Metadata, "materials", parentNameTunable.Value);

            if (File.Exists(parentFilePath) == false)
            {
                LogTarget.ErrorCtx(LOG_CTX, String.Format("Preset parent file {0} does not exist.", parentFilePath));
                return false;
            }

            MaterialPreset parentPreset;
            if ( LoadPreset(Branch, LogTarget, parentFilePath, out parentPreset, Branch.Project.Config) == false )
            {
                LogTarget.ErrorCtx(LOG_CTX, String.Format("Preset parent file {0} could not be loaded.", parentFilePath));
                return false;
            }
            parentPreset.ExpandPreset();

            ValidateShaderTunables(TextureMaps, parentPreset.TextureMaps);
            ValidateShaderTunables(Floats, parentPreset.Floats);
            ValidateShaderTunables(Ints, parentPreset.Ints);
            ValidateShaderTunables(Vec2s, parentPreset.Vec2s);
            ValidateShaderTunables(Vec3s, parentPreset.Vec3s);
            ValidateShaderTunables(Vec4s, parentPreset.Vec4s);

            return true;
        }
        #endregion

        #region Static Functions
        /// <summary>
        /// Returns if the file uses current material preset extensions
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        /// 
        public static bool IsMaterialPresetFile(String filepath)
        {
            if (filepath.EndsWith(MaterialObjectExtension) || filepath.EndsWith(MaterialSurfaceExtension) || filepath.EndsWith(MaterialTemplateExtension))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Loads the preset; outputs a full tunable metadata structure.
        /// 
        /// Note: Passing in the option IConfig arg will allow the materialPreset to skip using ConfigFactory (a noticeable speedup)
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="filename"></param>
        /// <param name="materialPreset"></param>
        /// <param name="config"></param> 
        /// <returns></returns>
        public static bool LoadPreset(IBranch branch, IUniversalLog log, string filename, out MaterialPreset materialPreset, IConfig config)
        {
            CheckMetadataDictionary(branch);

            if (MetadataDictionary.ContainsKey("MaterialPreset") && File.Exists(filename))
            {
                Structure materialPresetStructure = MetadataDictionary["MaterialPreset"];
                MetaFile metadataFile = new MetaFile(filename, materialPresetStructure, config);
                materialPreset = new MaterialPreset(branch, metadataFile, log);
                return true;
            }

            materialPreset = null;
            return false;
        }

        /// <summary>
        /// Loads a Material Preset from a stream source. Allows to read material presets from within in-memory archives or other non-files sources
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="log"></param>
        /// <param name="sourceStream"></param>
        /// <param name="materialPreset"></param>
        /// <param name="config"></param>
        /// <returns></returns>
        public static bool LoadPreset(IBranch branch, IUniversalLog log, Stream sourceStream, out MaterialPreset materialPreset, IConfig config)
        {
            CheckMetadataDictionary(branch);

            Structure materialPresetStructure;
            if (MetadataDictionary.TryGetValue("MaterialPreset", out materialPresetStructure))
            {
                MetaFile metadataFile = new MetaFile(sourceStream, materialPresetStructure, config);
                materialPreset = new MaterialPreset(branch, metadataFile, log);
                return true;
            }

            materialPreset = null;
            return false;
        }

        private static void CheckMetadataDictionary(IBranch branch)
        {
            if (MetadataDictionary == null)
            {
                MetadataDictionary = new StructureDictionary();
                String materialsPsc = Path.Combine(branch.Metadata, "definitions", "tools", "materials");
                MetadataDictionary.Load(branch, materialsPsc);
            }
        }

        public bool WriteSvaFile(Stream toStream)
        {
#warning Flo: wrapping call to WriteSva for later refactor with stream instead of files
            return false;
        }

        public bool WriteSvaFile(string filename)
        {
#warning Flo: wrapping call to WriteSva for later refactor with stream instead of files
            return WriteSvaFile(filename, this);
        }

        /// <summary>
        /// Writes the .sva file containing all shader parameters.
        /// </summary>
        /// <param name="svaFilename"></param>
        /// <param name="preset"></param>
        /// <returns></returns>
        public static bool WriteSvaFile(string svaFilename, MaterialPreset preset)
        {
            Shader shader = new Shader(preset.Name, svaFilename);

            IShaderVariable[] tunableArray = new IShaderVariable[preset.NumVariables];

            foreach (ITunable tunable in preset.TextureMaps)
            {
                StructureTunable tunableStructure = tunable as StructureTunable;
                StringTunable variableNameTunable = tunableStructure[m_VariableNameTunable] as StringTunable;
                IntTunable variableIndexTunable = tunableStructure[m_IndexTunable] as IntTunable;
                if (variableIndexTunable.Value > tunableArray.Count())
                {
                    LogTarget.ErrorCtx(LOG_CTX, "MaterialPreset \"{0}\" has a variable with an index higher then the amount of variables registered in it ({1} > {2}). This will cause Shader variables to not be set.", preset.Name, variableIndexTunable.Value, preset.NumVariables);
                    continue;
                }
                StringTunable valueTunable = tunableStructure[m_ValueTunable] as StringTunable;

                string textureName = Path.GetFileNameWithoutExtension(valueTunable.Value);

                // Some textures come in empty from the preset so set our "default" texture name to satisfy the serialization.
                if (string.IsNullOrEmpty(textureName))
                    textureName = "givemechecker";

                TextureVariable textureVar = new TextureVariable(variableNameTunable.Value, VarType.Texture, textureName);
                tunableArray[variableIndexTunable.Value] = textureVar;
            }

            foreach (ITunable tunable in preset.Floats)
            {

                StructureTunable tunableStructure = tunable as StructureTunable;
                StringTunable variableNameTunable = tunableStructure[m_VariableNameTunable] as StringTunable;
                IntTunable variableIndexTunable = tunableStructure[m_IndexTunable] as IntTunable;
                if (variableIndexTunable.Value > tunableArray.Count())
                {
                    LogTarget.ErrorCtx(LOG_CTX, "MaterialPreset \"{0}\" has a variable with an index higher then the amount of variables registered in it ({1} > {2}). This will cause Shader variables to not be set.", preset.Name, variableIndexTunable.Value, preset.NumVariables);
                    continue;
                }
                FloatTunable valueTunable = tunableStructure[m_ValueTunable] as FloatTunable;

                FloatVariable floatVar = new FloatVariable(variableNameTunable.Value, VarType.Float, valueTunable.Value);
                tunableArray[variableIndexTunable.Value] = floatVar;
            }

            foreach (ITunable tunable in preset.Ints)
            {
                StructureTunable tunableStructure = tunable as StructureTunable;
                StringTunable variableNameTunable = tunableStructure[m_VariableNameTunable] as StringTunable;
                IntTunable variableIndexTunable = tunableStructure[m_IndexTunable] as IntTunable;
                if (variableIndexTunable.Value > tunableArray.Count())
                {
                    LogTarget.ErrorCtx(LOG_CTX, "MaterialPreset \"{0}\" has a variable with an index higher then the amount of variables registered in it ({1} > {2}). This will cause Shader variables to not be set.", preset.Name, variableIndexTunable.Value, preset.NumVariables);
                    continue;
                }
                IntTunable valueTunable = tunableStructure[m_ValueTunable] as IntTunable;

                FloatVariable floatVar = new FloatVariable(variableNameTunable.Value, VarType.Float, (float)valueTunable.Value);
                tunableArray[variableIndexTunable.Value] = floatVar;
            }

            foreach (ITunable tunable in preset.Vec2s)
            {
                StructureTunable tunableStructure = tunable as StructureTunable;
                StringTunable variableNameTunable = tunableStructure[m_VariableNameTunable] as StringTunable;
                IntTunable variableIndexTunable = tunableStructure[m_IndexTunable] as IntTunable;
                if (variableIndexTunable.Value > tunableArray.Count())
                {
                    LogTarget.ErrorCtx(LOG_CTX, "MaterialPreset \"{0}\" has a variable with an index higher then the amount of variables registered in it ({1} > {2}). This will cause Shader variables to not be set.", preset.Name, variableIndexTunable.Value, preset.NumVariables);
                    continue;
                }
                Vector2Tunable valueTunable = tunableStructure[m_ValueTunable] as Vector2Tunable;

                Vector2f vector2 = new Vector2f(valueTunable.X, valueTunable.Y);
                Vector2Variable vectorVar = new Vector2Variable(variableNameTunable.Value, VarType.Vector2, vector2);
                tunableArray[variableIndexTunable.Value] = vectorVar;
            }

            foreach (ITunable tunable in preset.Vec3s)
            {
                StructureTunable tunableStructure = tunable as StructureTunable;
                StringTunable variableNameTunable = tunableStructure[m_VariableNameTunable] as StringTunable;
                IntTunable variableIndexTunable = tunableStructure[m_IndexTunable] as IntTunable;
                if (variableIndexTunable.Value > tunableArray.Count())
                {
                    LogTarget.ErrorCtx("MaterialPreset \"{0}\" has a variable with an index higher then the amount of variables registered in it ({1} > {2}). This will cause Shader variables to not be set.", preset.Name, variableIndexTunable.Value, preset.NumVariables);
                    continue;
                }
                Vector3Tunable valueTunable = tunableStructure[m_ValueTunable] as Vector3Tunable;

                Vector3f vector3 = new Vector3f(valueTunable.X, valueTunable.Y, valueTunable.Z);
                Vector3Variable vectorVar = new Vector3Variable(variableNameTunable.Value, VarType.Vector3, vector3);
                tunableArray[variableIndexTunable.Value] = vectorVar;
            }

            foreach (ITunable tunable in preset.Vec4s)
            {
                StructureTunable tunableStructure = tunable as StructureTunable;
                StringTunable variableNameTunable = tunableStructure[m_VariableNameTunable] as StringTunable;
                IntTunable variableIndexTunable = tunableStructure[m_IndexTunable] as IntTunable;
                if (variableIndexTunable.Value > tunableArray.Count())
                {
                    LogTarget.ErrorCtx(LOG_CTX, "MaterialPreset \"{0}\" has a variable with an index higher then the amount of variables registered in it ({1} > {2}). This will cause Shader variables to not be set.", preset.Name, variableIndexTunable.Value, preset.NumVariables);
                    continue;
                }
                Vector4Tunable valueTunable = tunableStructure[m_ValueTunable] as Vector4Tunable;

                Vector4f vector4 = new Vector4f(valueTunable.X, valueTunable.Y, valueTunable.Z, valueTunable.W);
                Vector4Variable vectorVar = new Vector4Variable(variableNameTunable.Value, VarType.Vector4, vector4);
                tunableArray[variableIndexTunable.Value] = vectorVar;
            }

            // Validate all our variables are there and none are missing otherwise the sva writing will die.
            for (int i = 0; i < tunableArray.Length; i++)
            {
                if (tunableArray[i] == null)
                    return false;
            }

            shader.ShaderVars.Variables.AddRange(tunableArray);

            BaseTokenizer tokenizer = TokenUtility.CreateTokenizedFile(svaFilename, TokenizerType.Ascii);
            shader.SaveAscii(tokenizer as AsciiTokenizer);
            tokenizer.Save();

            return true;
        }

        /// <summary>
        /// Writes the preset to the file.
        /// </summary>
        /// <param name="outputFilename"></param>
        public void WritePresetToFile(string outputFilename)
        {
            String xmlOutput = m_Root.ToXmlString();
            XElement xmlFileEle = XElement.Parse(xmlOutput);
            xmlFileEle.Save(outputFilename);
        }
        #endregion
    }
}
