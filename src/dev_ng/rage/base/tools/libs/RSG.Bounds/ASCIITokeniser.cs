﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace RSG.Bounds
{
    /// <summary>
    /// Converts an incoming Stream into a TokenCollection, ready for parsing
    /// </summary>
    internal static class ASCIITokeniser
    {
        internal static TokenCollection Tokenize(Stream stream)
        {
            var tokenMap = new List<KeyValuePair<string, string>>();
            using (StreamReader streamReader = new StreamReader(stream))
            {
                while (!streamReader.EndOfStream)
                {
                    string line = streamReader.ReadLine();

                    if (string.IsNullOrWhiteSpace((line)))
                    {
                        continue;// blank lines are ignored
                    }

                    int firstSpaceIndex = line.IndexOf(' ');
                    if (firstSpaceIndex != -1)
                    {
                        tokenMap.Add(new KeyValuePair<string, string>(line.Substring(0, firstSpaceIndex), line.Substring(firstSpaceIndex + 1)));// normal key/value string pair
                    }
                    else
                    {
                        tokenMap.Add(new KeyValuePair<string, string>(line, ""));// key with no value
                    }
                }
            }

            return new TokenCollection(tokenMap);
        }
    }
}
