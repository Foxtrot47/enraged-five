﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Base.Math;

namespace RSG.Bounds
{
    /// <summary>
    /// A Composite consists of one or more child BoundObjects and additionally stores a collision tyoe and boundToParentTransform
    /// </summary>
    public class CompositeChild
    {
        public CompositeChild(BNDFile.CollisionType collisionType, BoundObject boundObject, Matrix34f boundToParentTransform)
        {
            CollisionType = collisionType;
            BoundObject = boundObject;
            BoundToParentTransform = boundToParentTransform;
        }

        public BNDFile.CollisionType CollisionType { get; private set; }
        public BoundObject BoundObject { get; private set; }
        public Matrix34f BoundToParentTransform { get; private set; }
    }
}
