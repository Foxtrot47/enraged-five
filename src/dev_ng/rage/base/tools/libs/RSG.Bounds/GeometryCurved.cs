﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Base.Math;

namespace RSG.Bounds
{
    /// <summary>
    /// A geometry_curved primitive as it appears in a BND file
    /// </summary>
    public class GeometryCurved : BoundObject
    {
        public override void Save(System.IO.StreamWriter streamWriter)
        {
            throw new NotImplementedException();
        }

        internal static GeometryCurved Parse(string version, TokenCollection tokenCollection)
        {
            if (version == null)
                throw new ArgumentNullException("version");
            if (tokenCollection == null)
                throw new ArgumentNullException("tokenCollection");

            if (version != "1.10")
                throw new InvalidDataVersionException(string.Format("Encountered unsupported version of '{0}' when parsing GeometryCurved", version));

            int numCurvedEdges = tokenCollection.ReadIntTokenValue("curved_edges:");
            int numCurvedFaces = tokenCollection.ReadIntTokenValue("curved_faces:");
            int numVerts = tokenCollection.ReadIntTokenValue("verts:");
            RSG.Base.Math.Vector3f centreOfGravity = null;// the centre of gravity is optional
            if (tokenCollection.PeekTokenName() == "cg:")
            {
                centreOfGravity = tokenCollection.ReadVector3fTokenValue("cg:");
            }
            float margin = tokenCollection.ReadFloatTokenValue("margin:");
            uint numMaterials = tokenCollection.ReadUIntTokenValue("materials:");
            uint numMaterialColours = tokenCollection.ReadUIntTokenValue("materialcolors:");
            uint numEdges = tokenCollection.ReadUIntTokenValue("edges:");// Always 0
            uint numPolys = tokenCollection.ReadUIntTokenValue("polys:");
            uint readEdgeNormals = tokenCollection.ReadUIntTokenValue("readedgenormals:");// Always 1
            uint secondSurface = tokenCollection.ReadUIntTokenValue("secondSurface:");
            float secondSurfaceMaxHeight = tokenCollection.ReadFloatTokenValue("secondSurfaceMaxHeight:");

            RSG.Base.Math.Vector3f[] vertices = new RSG.Base.Math.Vector3f[numVerts];
            for (uint vertIndex = 0; vertIndex < numVerts; ++vertIndex)
            {
                vertices[vertIndex] = tokenCollection.ReadVector3fTokenValue("v");
            }

            float[] secondSurfaceDisplacements = null;// the second surface displacement data is optional
            if (secondSurface != 0)
            {
                secondSurfaceDisplacements = new float[numVerts];
                for (uint vertIndex = 0; vertIndex < numVerts; ++vertIndex)
                {
                    secondSurfaceDisplacements[vertIndex] = tokenCollection.ReadFloatTokenValue("f");
                }
            }
            else
            {
                secondSurfaceDisplacements = new float[0];
            }

            RSG.Base.Math.Vector3f[] shrunkVertices = null;// the shrunk vertices data is optional
            if (tokenCollection.PeekTokenName() == "shrunk:")
            {
                shrunkVertices = new RSG.Base.Math.Vector3f[numVerts];
                for (uint vertIndex = 0; vertIndex < numVerts; ++vertIndex)
                {
                    shrunkVertices[vertIndex] = tokenCollection.ReadVector3fTokenValue("v");
                }
            }
            else
            {
                shrunkVertices = new RSG.Base.Math.Vector3f[0];
            }

            string[] materialIds = new string[numMaterials];
            for (uint materialIndex = 0; materialIndex < numMaterials; ++materialIndex)
            {
                materialIds[materialIndex] = tokenCollection.ReadTokenName();
            }

            RSG.Base.Math.Vector4i[] materialColours = new RSG.Base.Math.Vector4i[numMaterialColours];
            for (uint materialColourIndex = 0; materialColourIndex < numMaterialColours; ++materialColourIndex)
            {
                materialColours[materialColourIndex] = tokenCollection.ReadVector4iTokenValue("materialcolor");
            }

            // Unused, but read in the value if it's there to temporarily handle old file formats.
            int vertexMaterial = -1;
            if (tokenCollection.PeekTokenName() == "vertexmaterial:")
            {
                vertexMaterial = tokenCollection.ReadIntTokenValue("vertexmaterial:");
            }

            for (int curvedEdgeIndex = 0; curvedEdgeIndex < numCurvedEdges; ++curvedEdgeIndex)
            {
                ParseCurvedEdge(tokenCollection);
            }

            for (int curvedFaceIndex = 0; curvedFaceIndex < numCurvedFaces; ++curvedFaceIndex)
            {
                ParseCurvedFace(tokenCollection);
            }

            return null;
        }

        private static void ParseCurvedEdge(TokenCollection tokenCollection)
        {
            tokenCollection.SkipToken();
            tokenCollection.SkipToken();
            tokenCollection.SkipToken();
            tokenCollection.SkipToken();
            tokenCollection.SkipToken();
        }

        private static void ParseCurvedFace(TokenCollection tokenCollection)
        {
            tokenCollection.SkipToken();
            tokenCollection.SkipToken();
            tokenCollection.SkipToken();
            tokenCollection.SkipToken();
            tokenCollection.SkipToken();
            tokenCollection.SkipToken();
            tokenCollection.SkipToken();
            tokenCollection.SkipToken();
        }
    }
}
