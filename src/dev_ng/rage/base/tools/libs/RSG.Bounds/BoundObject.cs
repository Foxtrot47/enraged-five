﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Bounds
{
    /// <summary>
    /// The base type for any object that can appear at the root of a BND file.
    /// </summary>
    public class BoundObject
    {
        public void Save(string pathname)
        {
            using (System.IO.StreamWriter streamWriter = new System.IO.StreamWriter(System.IO.File.Create(pathname)))
            {
                Save(streamWriter);
            }
        }

        public virtual void Save(System.IO.StreamWriter streamWriter) { }
    }
}
