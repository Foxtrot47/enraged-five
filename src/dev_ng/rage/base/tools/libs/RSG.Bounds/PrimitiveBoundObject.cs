﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Base.Math;

namespace RSG.Bounds
{
    /// <summary>
    /// Base class for primitive bound objects (box, sphere, cylinder & capsule)
    /// </summary>
    public class PrimitiveBoundObject : BoundObject
    {
        public PrimitiveBoundObject(Vector3f centroid)
        {
            Centroid = centroid;
        }

        public Vector3f Centroid { get; set; }
    }
}
