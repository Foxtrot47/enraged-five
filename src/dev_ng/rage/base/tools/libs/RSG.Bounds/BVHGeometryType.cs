﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Bounds
{
    /// <summary>
    /// A user defined key used to differentiate between mutually exclusive BVH geometry
    /// </summary>
    public class BVHGeometryType
    {
        public BVHGeometryType(BNDFile.CollisionType collisionType, bool hasMaterialColours, bool hasSecondSurfaceData, bool hasShrunkVerts, uint numPerVertAttributes)
        {
            CollisionType = collisionType;
            HasMaterialColours = hasMaterialColours;
            HasSecondSurfaceData = hasSecondSurfaceData;
            HasShrunkVerts = hasShrunkVerts;
            NumPerVertAttributes = numPerVertAttributes;
        }

        public override bool Equals(object obj)
        {
            if (obj is BVHGeometryType)
            {
                BVHGeometryType other = (BVHGeometryType)obj;
                return ((this.CollisionType == other.CollisionType) &&
                        (this.HasMaterialColours == other.HasMaterialColours) &&
                        (this.HasSecondSurfaceData == other.HasSecondSurfaceData) &&
                        (this.HasShrunkVerts == other.HasShrunkVerts) &&
                        (this.NumPerVertAttributes == other.NumPerVertAttributes));
            }

            return false;
        }

        public override int GetHashCode()
        {
            return CollisionType.GetHashCode() ^ HasMaterialColours.GetHashCode() ^ HasSecondSurfaceData.GetHashCode() ^ HasShrunkVerts.GetHashCode();
        }

        public BNDFile.CollisionType CollisionType { get; private set; }
        public bool HasMaterialColours { get; private set; }
        public bool HasSecondSurfaceData { get; private set; }
        public bool HasShrunkVerts { get; private set; }
        public uint NumPerVertAttributes { get; private set; }
    }
}
