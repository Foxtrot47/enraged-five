﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Bounds
{
    /// <summary>
    /// Class to encapsulate all data associated with a BVH vertex.  Consider sharing with primitive collection based bound classes.
    /// </summary>
    public sealed class VertexData
    {
        #region Constructors
        public VertexData()
        {
            this.Position = new Base.Math.Vector3f();
            this.AttributeData = new RSG.Base.Math.Vector4i[0];
        }

        /// <summary>
        /// Constructor that just takes both position and attribute data.
        /// </summary>
        /// <param name="position"></param>
        /// <param name="attributeData"></param>
        public VertexData(RSG.Base.Math.Vector3f position, RSG.Base.Math.Vector4i[] attributeData)
        {
            this.Position = position;
            this.AttributeData = attributeData;
        }

        /// <summary>
        /// Constructor that just takes a position.
        /// </summary>
        /// <param name="position"></param>
        public VertexData(RSG.Base.Math.Vector3f position)
            : this(position, new RSG.Base.Math.Vector4i[0])
        {

        }
        #endregion // Constructors

        #region Properties
        /// <summary>
        /// Bound vertex position.
        /// </summary>
        public RSG.Base.Math.Vector3f Position { get; set; }

        /// <summary>
        /// Per vertex attribute data.
        /// </summary>
        public RSG.Base.Math.Vector4i[] AttributeData { get; set; }
        #endregion
    }
}
