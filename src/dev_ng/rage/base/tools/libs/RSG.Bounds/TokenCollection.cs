﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Bounds
{
    /// <summary>
    /// A collection of string based tokens, suitable for parsing.
    /// Parsing of common types, peeking, etc are supported.
    /// This could easily be moved into a common area, but arbitrary format text data is unusual, and XML should be (and is) preferred
    /// </summary>
    internal class TokenCollection
    {
        internal TokenCollection(IEnumerable<KeyValuePair<string, string>> tokens)
        {
            tokens_ = tokens.ToArray();
            currentTokenIndex_ = 0;
        }

        internal string PeekTokenName()
        {
            if (currentTokenIndex_ >= tokens_.Length)
            {
                return "";// Handle end of token array gracefully here.  There's no need to for other cases.
            }

            return tokens_[currentTokenIndex_].Key;
        }

        internal string ReadTokenName()
        {
            return tokens_[currentTokenIndex_++].Key;
        }

        internal void SkipToken()
        {
            ++currentTokenIndex_;
        }

        internal float ReadFloatTokenValue(string tokenName)
        {
            string tokenValue = VerifyNextTokenName(tokenName);
            return float.Parse(tokenValue);
        }

        internal int ReadIntTokenValue(string tokenName)
        {
            string tokenValue = VerifyNextTokenName(tokenName);
            return int.Parse(tokenValue);
        }

        internal RSG.Base.Math.Matrix34f ReadMatrix34fTokenValue(string tokenName)
        {
            string tokenValue = VerifyNextTokenName(tokenName);
            string[] row0Values = tokenValue.Split(' ', '\t');
            string[] row1Values = ReadTokenName().Split(' ', '\t');
            string[] row2Values = ReadTokenName().Split(' ', '\t');
            string[] row3Values = ReadTokenName().Split(' ', '\t');
            var row0 = new RSG.Base.Math.Vector3f(float.Parse(row0Values[0]), float.Parse(row0Values[1]), float.Parse(row0Values[2]));
            var row1 = new RSG.Base.Math.Vector3f(float.Parse(row1Values[0]), float.Parse(row1Values[1]), float.Parse(row1Values[2]));
            var row2 = new RSG.Base.Math.Vector3f(float.Parse(row2Values[0]), float.Parse(row2Values[1]), float.Parse(row2Values[2]));
            var row3 = new RSG.Base.Math.Vector3f(float.Parse(row3Values[0]), float.Parse(row3Values[1]), float.Parse(row3Values[2]));

            return new RSG.Base.Math.Matrix34f(row0, row1, row2, row3);
        }

        internal string ReadStringTokenValue(string tokenName)
        {
            string tokenValue = VerifyNextTokenName(tokenName);
            return tokenValue;
        }

        internal RSG.Base.Math.Vector3f ReadVector3fTokenValue(string tokenName)
        {
            string tokenValue = VerifyNextTokenName(tokenName);
            string[] vectorValues = tokenValue.Split(' ', '\t');
            float x;
            float y;
            float z;
            float.TryParse(vectorValues[0], out x);
            float.TryParse(vectorValues[1], out y);
            float.TryParse(vectorValues[2], out z);
            return new RSG.Base.Math.Vector3f(x, y, z);
        }

        internal RSG.Base.Math.Vector4i ReadVector4iTokenValue(string tokenName)
        {
            string tokenValue = VerifyNextTokenName(tokenName);
            string[] vectorValues = tokenValue.Split(' ', '\t');
            return new RSG.Base.Math.Vector4i(
                int.Parse(vectorValues[0]),
                int.Parse(vectorValues[1]),
                int.Parse(vectorValues[2]),
                int.Parse(vectorValues[3]));
        }

        internal RSG.Base.Math.Vector4i[] ReadMultipleVector4iTokenValues(string tokenName, uint count)
        {
            List<RSG.Base.Math.Vector4i> vecs = new List<Base.Math.Vector4i>();

            string tokenValue = VerifyNextTokenName(tokenName);
            string[] vectorValues = tokenValue.Split(' ', '\t');
            for (int i = 0; i < count; i++)
            {
                vecs.Add(new RSG.Base.Math.Vector4i(
                   int.Parse(vectorValues[0 + (4 * i)]),
                   int.Parse(vectorValues[1 + (4 * i)]),
                   int.Parse(vectorValues[2 + (4 * i)]),
                   int.Parse(vectorValues[3 + (4 * i)])));
            }
            return vecs.ToArray();
        }

        internal uint ReadUIntTokenValue(string tokenName)
        {
            string tokenValue = VerifyNextTokenName(tokenName);
            return uint.Parse(tokenValue);
        }

        private string VerifyNextTokenName(string tokenName)
        {
            KeyValuePair<string, string> nextToken = tokens_[currentTokenIndex_];
            if (nextToken.Key != tokenName)
                throw new InvalidDataFormatException(string.Format("Encountered token '{0}' when expecting '{1}'", nextToken.Key, tokenName));
            ++currentTokenIndex_;

            return nextToken.Value;
        }

        private KeyValuePair<string, string>[] tokens_;
        private int currentTokenIndex_;
    }
}
