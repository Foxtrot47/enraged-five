﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Base.Math;

namespace RSG.Bounds
{
    /// <summary>
    /// A composite as it appears in a BND file
    /// </summary>
    public class Composite : BoundObject
    {
        public Composite(CompositeChild[] children)
        {
            Children = children;
        }

        public override void Save(System.IO.StreamWriter streamWriter)
        {
            streamWriter.WriteLine("version: 1.10");
            streamWriter.WriteLine("type: composite");
            streamWriter.WriteLine();
            streamWriter.WriteLine("NumBounds: {0}", Children.Length);
            streamWriter.WriteLine();
            foreach (var child in Children)
            {
                streamWriter.WriteLine("boundflags: {0}", (int)child.CollisionType);
            }
            streamWriter.WriteLine();
            int boundIndex = 0;
            foreach (CompositeChild child in Children)
            {
                streamWriter.WriteLine("bound: {0}", boundIndex++);
                child.BoundObject.Save(streamWriter);
                streamWriter.WriteLine();
                streamWriter.WriteLine("matrix: {0}\t{1}\t{2}", child.BoundToParentTransform.A.X, child.BoundToParentTransform.A.Y, child.BoundToParentTransform.A.Z);
                streamWriter.WriteLine("{0}\t{1}\t{2}", child.BoundToParentTransform.B.X, child.BoundToParentTransform.B.Y, child.BoundToParentTransform.B.Z);
                streamWriter.WriteLine("{0}\t{1}\t{2}", child.BoundToParentTransform.C.X, child.BoundToParentTransform.C.Y, child.BoundToParentTransform.C.Z);
                streamWriter.WriteLine("{0}\t{1}\t{2}", child.BoundToParentTransform.D.X, child.BoundToParentTransform.D.Y, child.BoundToParentTransform.D.Z);
                streamWriter.WriteLine();
            }
        }

        internal static Composite Parse(string version, TokenCollection tokenCollection)
        {
            if (version == null)
                throw new ArgumentNullException("version");
            if (tokenCollection == null)
                throw new ArgumentNullException("tokenCollection");

            if (version != "1.10")
                throw new InvalidDataVersionException(string.Format("Encountered unsupported version of '{0}' when parsing Composite", version));

            uint numBounds = tokenCollection.ReadUIntTokenValue("NumBounds:");

            BNDFile.CollisionType[] collisionTypes = new BNDFile.CollisionType[numBounds];
            for (int childBoundIndex = 0; childBoundIndex < numBounds; ++childBoundIndex)
            {
                collisionTypes[childBoundIndex] = (BNDFile.CollisionType)tokenCollection.ReadIntTokenValue("boundflags:");
            }

            Matrix34f[] childToParentTransformMatricies = new Matrix34f[numBounds];
            BoundObject[] childObjects = new BoundObject[numBounds];
            for (int childBoundIndex = 0; childBoundIndex < numBounds; ++childBoundIndex)
            {
                int parsedBoundIndex = tokenCollection.ReadIntTokenValue("bound:");// We may as well use this, assumptions are the mother of all cockups after all
                string definitionVersion = tokenCollection.ReadStringTokenValue("version:");
                string definitionType = tokenCollection.ReadStringTokenValue("type:");
                if (definitionType == "box")
                {
                    childObjects[parsedBoundIndex] = Box.Parse(definitionVersion, tokenCollection);
                }
                else if (definitionType == "bvh")
                {
                    childObjects[parsedBoundIndex] = BVH.Parse(definitionVersion, tokenCollection);
                }
                else if (definitionType == "capsule")
                {
                    childObjects[parsedBoundIndex] = Capsule.Parse(definitionVersion, tokenCollection);
                }
                else if (definitionType == "cylinder")
                {
                    childObjects[parsedBoundIndex] = Cylinder.Parse(definitionVersion, tokenCollection);
                }
                else if (definitionType == "geometry")
                {
                    childObjects[parsedBoundIndex] = Geometry.Parse(definitionVersion, tokenCollection);
                }
                else if (definitionType == "sphere")
                {
                    childObjects[parsedBoundIndex] = Sphere.Parse(definitionVersion, tokenCollection);
                }
                else if (definitionType == "geometry_curved")
                {
                    childObjects[parsedBoundIndex] = GeometryCurved.Parse(definitionVersion, tokenCollection);// Always returns null, which we handle this later
                }
                else
                {
                    throw new InvalidDataFormatException(string.Format("Encountered unrecognised definition of type '{0}' while parsing composite", definitionType));
                }

                childToParentTransformMatricies[parsedBoundIndex] = tokenCollection.ReadMatrix34fTokenValue("matrix:");
            }

            var bounds = new List<CompositeChild>();
            for (int childBoundIndex = 0; childBoundIndex < numBounds; ++childBoundIndex)
            {
                if (childObjects[childBoundIndex] == null)// handle GeometryCurved
                    continue;

                bounds.Add(new CompositeChild(collisionTypes[childBoundIndex], childObjects[childBoundIndex], childToParentTransformMatricies[childBoundIndex]));
            }

            return new Composite(bounds.ToArray());
        }

        public CompositeChild[] Children { get; private set; }
    }
}
