﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Base.Math;

namespace RSG.Bounds
{
    /// <summary>
    /// A capsule within a BVH
    /// </summary>
    public class BVHCapsule : BVHPrimitive
    {
        public BVHCapsule(int endIndex0, int endIndex1, float radius, int materialIndex)
        {
            EndIndex0 = endIndex0;
            EndIndex1 = endIndex1;
            Radius = radius;
            MaterialIndex = materialIndex;
        }

        #region BVHPrimitive overrides
        public override BVHPrimitive Clone()
        {
            return new BVHCapsule(EndIndex0, EndIndex1, Radius, MaterialIndex);
        }

        public override BVHPrimitive Clone(int vertexRebaseIndex, int materialRebaseIndex)
        {
            return new BVHCapsule(EndIndex0 + vertexRebaseIndex, EndIndex1 + vertexRebaseIndex, Radius, MaterialIndex + materialRebaseIndex);
        }

        public override IEnumerable<int> GetVertexIndices()
        {
            return new int[2] { EndIndex0, EndIndex1 };
        }

        public override void Save(System.IO.StreamWriter streamWriter)
        {
            streamWriter.WriteLine("capsule {0} {1} {2} {3}", EndIndex0, EndIndex1, Radius, MaterialIndex);
        }

        public override void VertHasBeenCombined(int deadVertex, int replacementVertex)
        {
            if (EndIndex0 == deadVertex)
                EndIndex0 = replacementVertex;
            else if (EndIndex0 > deadVertex)
                --EndIndex0;

            if (EndIndex1 == deadVertex)
                EndIndex1 = replacementVertex;
            else if (EndIndex1 > deadVertex)
                --EndIndex1;
        }

        public override void MaterialHasBeenCombined(int deadMaterial, int replacementMaterial)
        {
            if (MaterialIndex == deadMaterial)
                MaterialIndex = replacementMaterial;
            else if (MaterialIndex > deadMaterial)
                --MaterialIndex;
        }

        public override void ScaleInternal(RSG.Base.Math.Vector3f scale)
        {
            float averageScale = (scale.X + scale.Y + scale.Z) / 3.0f;
            Radius *= averageScale;
        }

        public override BoundingBox3f GetBoundingBox(Vector3f[] verts)
        {
            BoundingBox3f boundingBox = new BoundingBox3f();

            boundingBox.Expand(verts[EndIndex0]);
            boundingBox.Expand(verts[EndIndex1]);

            return boundingBox;
        }

        public override BoundingBox3f GetBoundingBox(VertexData[] verts)
        {
            BoundingBox3f boundingBox = new BoundingBox3f();

            boundingBox.Expand(verts[EndIndex0].Position);
            boundingBox.Expand(verts[EndIndex1].Position);

            return boundingBox;
        }
        #endregion

        public int EndIndex0 { get; private set; }
        public int EndIndex1 { get; private set; }
        public float Radius { get; private set; }
        public int MaterialIndex { get; private set; }

        internal static BVHCapsule Parse(string text)
        {
            string[] tokens = text.Split(' ', '\t');

            return new BVHCapsule(int.Parse(tokens[0]), int.Parse(tokens[1]), float.Parse(tokens[2]), int.Parse(tokens[3]));
        }
    }
}
