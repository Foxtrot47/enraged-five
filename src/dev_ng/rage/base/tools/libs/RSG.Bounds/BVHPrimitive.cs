﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Base.Math;

namespace RSG.Bounds
{
    /// <summary>
    /// The abstract base type for any primitive that may appear in a BVH
    /// </summary>
    public abstract class BVHPrimitive
    {
        public abstract BVHPrimitive Clone();
        public abstract BVHPrimitive Clone(int vertexRebaseIndex, int materialRebaseIndex);
        public abstract IEnumerable<int> GetVertexIndices();
        public abstract void Save(System.IO.StreamWriter streamWriter);
        public abstract void VertHasBeenCombined(int deadVertex, int replacementVertex);
        public abstract void MaterialHasBeenCombined(int deadMaterial, int replacementMaterial);
        public abstract void ScaleInternal(Vector3f scale);
        public abstract BoundingBox3f GetBoundingBox(Vector3f[] verts);
        public abstract BoundingBox3f GetBoundingBox(VertexData[] verts);
    }
}
