﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Bounds
{
    /// <summary>
    /// A material as it appears in a BVH, convertible to and from the serialised string format
    /// </summary>
    public class BVHMaterial
    {
        public BVHMaterial(string materialDefinition)
        {
            string[] definitionTokens = materialDefinition.Split('|');
            if (definitionTokens.Length < 5 || definitionTokens.Length > 6)
            {
                // B* 982937 - we want to be as tolerant as possible here.
                // We will catch bad data at the source with exporter errors, etc.
                Name = "default";
                ProceduralModifier = "0";
                RoomID = 0;
                PedPopulation = 0;
                Flags = 0;
                PaletteIndex = 0;
                ID = 1;

                return;
            }

            Name = definitionTokens[0];
            ProceduralModifier = definitionTokens[1];
            RoomID = int.Parse(definitionTokens[2]);
            PedPopulation = int.Parse(definitionTokens[3]);

            if (definitionTokens.Length == 5)
            {
                string[] lastTokenTokens = definitionTokens[4].Split('_');
                Flags = int.Parse(lastTokenTokens[0]);
                PaletteIndex = 0;
                ID = int.Parse(lastTokenTokens[1]);
            }
            else if (definitionTokens.Length == 6)
            {
                Flags = int.Parse(definitionTokens[4]);
                string[] lastTokenTokens = definitionTokens[5].Split('_');
                PaletteIndex = int.Parse(lastTokenTokens[0]);
                ID = int.Parse(lastTokenTokens[1]);
            }
        }

        public override string ToString()
        {
            if (PaletteIndex == 0)
            {
                return string.Format(
                    "{0}|{1}|{2}|{3}|{4}_{5}",
                    Name, ProceduralModifier, RoomID, PedPopulation, Flags, ID);
            }
            else
            {
                return string.Format(
                    "{0}|{1}|{2}|{3}|{4}|{5}_{6}",
                    Name, ProceduralModifier, RoomID, PedPopulation, Flags, PaletteIndex, ID);
            }
        }

        public string Name { get; private set; }
        public string ProceduralModifier { get; private set; }
        public int RoomID { get; private set; }
        public int PedPopulation { get; private set; }
        public int Flags { get; private set; }
        public int PaletteIndex { get; set; }
        public int ID { get; private set; }
    }
}
