﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using RSG.Base.Math;

namespace RSG.Bounds
{
    internal static class DataConversion
    {
        internal static string VectorToString(Vector3f vector)
        {
            return string.Format("{0} {1} {2}", vector.X.ToString("R"), vector.Y.ToString("R"), vector.Z.ToString("R"));
        }
    }
}
