﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Base.Math;

namespace RSG.Bounds
{
    /// <summary>
    /// A box as it appears in a BND file
    /// </summary>
    public class Box : PrimitiveBoundObject
    {
        public Box(Vector3f size, Vector3f centroid, string[] materials)
            : base(centroid)
        {
            Size = size;
            Materials = materials;
        }

        public override void Save(System.IO.StreamWriter streamWriter)
        {
            streamWriter.WriteLine("version: 1.10");
            streamWriter.WriteLine("type: box");
            streamWriter.WriteLine();
            streamWriter.WriteLine("size: {0}", DataConversion.VectorToString(Size));
            if (Centroid.Magnitude() != 0)
            {
                streamWriter.WriteLine("centroid: {0}", DataConversion.VectorToString(Centroid));
            }
            streamWriter.WriteLine();
            streamWriter.WriteLine("materials: {0}", Materials.Length);
            foreach (string material in Materials)
            {
                streamWriter.WriteLine(material);
            }
            streamWriter.WriteLine();
        }

        internal static Box Parse(string version, TokenCollection tokenCollection)
        {
            if (version == null)
                throw new ArgumentNullException("version");
            if (tokenCollection == null)
                throw new ArgumentNullException("tokenCollection");

            if (version != "1.10")
                throw new InvalidDataVersionException(string.Format("Encountered unsupported version of '{0}' when parsing Box", version));

            Vector3f size = tokenCollection.ReadVector3fTokenValue("size:");

            Vector3f centroid = null;
            if (tokenCollection.PeekTokenName() == "centroid:")
            {
                centroid = tokenCollection.ReadVector3fTokenValue("centroid:");
            }
            else
            {
                centroid = new Vector3f(0.0f, 0.0f, 0.0f);
            }

            uint numMaterials = tokenCollection.ReadUIntTokenValue("materials:");
            string[] materials = new string[numMaterials];
            for (uint materialIndex = 0; materialIndex < numMaterials; ++materialIndex)
            {
                materials[materialIndex] = tokenCollection.ReadTokenName();
            }

            return new Box(size, centroid, materials);
        }

        public Vector3f Size { get; private set; }
        public string[] Materials { get; private set; }
    }
}
