﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Base.Math;

namespace RSG.Bounds
{
    /// <summary>
    /// A sphere as it may appear in a BND file
    /// </summary>
    public class Sphere : PrimitiveBoundObject
    {
        public Sphere(float radius, Vector3f centroid, string[] materials) 
            : base(centroid)
        {
            Radius = radius;
            Materials = materials;
        }

        public override void Save(System.IO.StreamWriter streamWriter)
        {
            streamWriter.WriteLine("version: 1.10");
            streamWriter.WriteLine("type: sphere");
            streamWriter.WriteLine();
            streamWriter.WriteLine("radius: {0}", Radius.ToString("R"));
            if (Centroid.Magnitude() != 0)
            {
                streamWriter.WriteLine("centroid: {0}", DataConversion.VectorToString(Centroid));
            }
            streamWriter.WriteLine();
            streamWriter.WriteLine("materials: {0}", Materials.Length);
            foreach (string material in Materials)
            {
                streamWriter.WriteLine(material);
            }
            streamWriter.WriteLine();
        }

        internal static Sphere Parse(string version, TokenCollection tokenCollection)
        {
            if (version == null)
                throw new ArgumentNullException("version");
            if (tokenCollection == null)
                throw new ArgumentNullException("tokenCollection");

            if (version != "1.10")
                throw new InvalidDataVersionException(string.Format("Encountered unsupported version of '{0}' when parsing Sphere", version));

            float radius = tokenCollection.ReadFloatTokenValue("radius:");

            Vector3f centroid = null;
            if (tokenCollection.PeekTokenName() == "centroid:")
            {
                centroid = tokenCollection.ReadVector3fTokenValue("centroid:");
            }
            else
            {
                centroid = new Vector3f(0.0f, 0.0f, 0.0f);
            }

            uint numMaterials = tokenCollection.ReadUIntTokenValue("materials:");
            string[] materials = new string[numMaterials];
            for (uint materialIndex = 0; materialIndex < numMaterials; ++materialIndex)
            {
                materials[materialIndex] = tokenCollection.ReadTokenName();
            }

            return new Sphere(radius, centroid, materials);
        }

        public float Radius { get; private set; }
        public string[] Materials { get; private set; }
    }
}
