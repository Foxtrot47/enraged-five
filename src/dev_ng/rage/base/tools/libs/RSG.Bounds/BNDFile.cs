﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;

namespace RSG.Bounds
{
    /// <summary>
    /// Represents a bnd file and provides means for loading from any stream.
    /// The BoundObject property represents the root bound object
    /// </summary>
    public class BNDFile
    {
		public BNDFile(BoundObject boundObject)
        {
            FileName = null;
            BoundObject = boundObject;
        }
		
        public BNDFile(string filename, BoundObject boundObject)
        {
            FileName = filename;
            BoundObject = boundObject;
        }

        [Flags]
        public enum CollisionType
        {
            Weapons = 1,
            Mover = 2,
            River = 4,
            DeepMaterialSurface = 8,// was Camera
            Foliage = 16,
            Horse = 32,
            Cover = 64,
            Vehicle = 128,
            StairSlope = 256,
            MaterialOnly = 512,
            Default = Weapons | Mover | Horse | Cover | Vehicle
        }

        public static BNDFile Load(Stream stream)
        {
            return Load(null, stream);
        }
		
        public static BNDFile Load(string pathname)
        {
            return Load(pathname, File.OpenRead(pathname));
        }

        public static BNDFile Load(string filename, Stream stream)
        {
            TokenCollection tokenCollection = ASCIITokeniser.Tokenize(stream);

            string definitionVersion = tokenCollection.ReadStringTokenValue("version:");
            string definitionType = tokenCollection.ReadStringTokenValue("type:");

            BoundObject boundObject = null;
            if (definitionType == "box")
            {
                boundObject = Box.Parse(definitionVersion, tokenCollection);
            }
            else if (definitionType == "bvh")
            {
                boundObject = BVH.Parse(definitionVersion, tokenCollection);
            }
            else if (definitionType == "capsule")
            {
                boundObject = Capsule.Parse(definitionVersion, tokenCollection);
            }
            else if (definitionType == "composite")
            {
                boundObject = Composite.Parse(definitionVersion, tokenCollection);
            }
            else if (definitionType == "cylinder")
            {
                boundObject = Cylinder.Parse(definitionVersion, tokenCollection);
            }
            else if (definitionType == "geometry")
            {
                boundObject = Geometry.Parse(definitionVersion, tokenCollection);
            }
            else if (definitionType == "sphere")
            {
                boundObject = Sphere.Parse(definitionVersion, tokenCollection);
            }
            else
            {
                throw new InvalidDataFormatException(string.Format("Encountered unrecognised definition of type '{0}' while parsing BND file", definitionType));
            }

            return new BNDFile(filename, boundObject);
        }

        public string FileName { get; private set; }
        public BoundObject BoundObject { get; private set; }
    }
}
