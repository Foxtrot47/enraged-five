﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Base.Math;

namespace RSG.Bounds
{
    /// <summary>
    /// A sphere as it appears in a BVH
    /// </summary>
    public class BVHSphere : BVHPrimitive
    {
        public BVHSphere(int centreIndex, float radius, int materialIndex)
        {
            CentreIndex = centreIndex;
            Radius = radius;
            MaterialIndex = materialIndex;
        }

        #region BVHPrimitive overrides
        public override BVHPrimitive Clone()
        {
            return new BVHSphere(CentreIndex, Radius, MaterialIndex);
        }

        public override BVHPrimitive Clone(int vertexRebaseIndex, int materialRebaseIndex)
        {
            return new BVHSphere(CentreIndex + vertexRebaseIndex, Radius, MaterialIndex + vertexRebaseIndex);
        }

        public override IEnumerable<int> GetVertexIndices()
        {
            return new int[1] { CentreIndex };
        }

        public override void Save(System.IO.StreamWriter streamWriter)
        {
            streamWriter.WriteLine("sphere {0} {1} {2}", CentreIndex, Radius, MaterialIndex);
        }

        public override void VertHasBeenCombined(int deadVertex, int replacementVertex)
        {
            if (CentreIndex == deadVertex)
                CentreIndex = replacementVertex;
            else if (CentreIndex > deadVertex)
                --CentreIndex;
        }

        public override void MaterialHasBeenCombined(int deadMaterial, int replacementMaterial)
        {
            if (MaterialIndex == deadMaterial)
                MaterialIndex = replacementMaterial;
            else if (MaterialIndex > deadMaterial)
                --MaterialIndex;
        }

        public override void ScaleInternal(RSG.Base.Math.Vector3f scale)
        {
            float averageScale = (scale.X + scale.Y + scale.Z) / 3.0f;
            Radius *= averageScale;
        }

        public override BoundingBox3f GetBoundingBox(Vector3f[] verts)
        {
            Vector3f centreVert = verts[CentreIndex];
            BoundingBox3f boundingBox = new BoundingBox3f(
                new Vector3f(centreVert.X - Radius, centreVert.Y - Radius, centreVert.Z - Radius),
                new Vector3f(centreVert.X + Radius, centreVert.Y + Radius, centreVert.Z + Radius));

            return boundingBox;
        }

        public override BoundingBox3f GetBoundingBox(VertexData[] verts)
        {
            Vector3f centreVert = verts[CentreIndex].Position;
            BoundingBox3f boundingBox = new BoundingBox3f(
                new Vector3f(centreVert.X - Radius, centreVert.Y - Radius, centreVert.Z - Radius),
                new Vector3f(centreVert.X + Radius, centreVert.Y + Radius, centreVert.Z + Radius));

            return boundingBox;
        }
        #endregion

        public int CentreIndex { get; private set; }
        public float Radius { get; private set; }
        public int MaterialIndex { get; private set; }

        internal static BVHSphere Parse(string text)
        {
            string[] tokens = text.Split(' ', '\t');

            return new BVHSphere(int.Parse(tokens[0]), float.Parse(tokens[1]), int.Parse(tokens[2]));
        }
    }
}
