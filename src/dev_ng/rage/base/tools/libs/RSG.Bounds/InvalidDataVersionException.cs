﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Bounds
{
    /// <summary>
    /// Data was encountered of an unsupported version
    /// </summary>
    public class InvalidDataVersionException : Exception
    {
        public InvalidDataVersionException(string message)
            : base(message)
        {
        }
    }
}
