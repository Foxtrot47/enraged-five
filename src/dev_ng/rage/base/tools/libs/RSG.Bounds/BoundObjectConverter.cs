﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Base.Math;

namespace RSG.Bounds
{
    /// <summary>
    /// Converts any BoundObject derived type into a BVH
    /// </summary>
    public static class BoundObjectConverter
    {
        public static BVH ToBVH(BoundObject boundObject)
        {
            if (boundObject is BVH)
            {
                return (BVH)boundObject;
            }
            else if( boundObject is Box)
            {
                return BoxToBVH((Box)boundObject);
            }
            else if (boundObject is Capsule)
            {
                return CapsuleToBVH((Capsule)boundObject); 
            }
            else if (boundObject is Cylinder)
            {
                return CylinderToBVH((Cylinder)boundObject);
            }
            else if (boundObject is Geometry)
            {
                return GeometryToBVH((Geometry)boundObject);
            }
            else if (boundObject is Sphere)
            {
                return SphereToBVH((Sphere)boundObject);
            }

            throw new NotImplementedException();
        }

        public static Dictionary<BNDFile.CollisionType, List<BVH>> CompositeToBVHMap(Composite composite)
        {
            Dictionary<BNDFile.CollisionType, List<BVH>> bvhMap = new Dictionary<BNDFile.CollisionType, List<BVH>>();
            foreach (CompositeChild child in composite.Children)
            {
                BVH convertedBVH = ToBVH(child.BoundObject);
                convertedBVH.TransformVerts(child.BoundToParentTransform);

                if (!bvhMap.ContainsKey(child.CollisionType))
                {
                    bvhMap.Add(child.CollisionType, new List<BVH>());
                }
                bvhMap[child.CollisionType].Add(convertedBVH);
            }

            return bvhMap;
        }

        private static BVH BoxToBVH(Box box)
        {
            float halfX = box.Size.X * 0.5f;
            float halfY = box.Size.Y * 0.5f;
            float halfZ = box.Size.Z * 0.5f;
            VertexData[] verts = new VertexData[4]
            {
                new VertexData(new Vector3f(box.Centroid.X + halfX, box.Centroid.Y + halfY, box.Centroid.Z - halfZ)),
                new VertexData(new Vector3f(box.Centroid.X - halfX, box.Centroid.Y + halfY, box.Centroid.Z + halfZ)),
                new VertexData(new Vector3f(box.Centroid.X + halfX, box.Centroid.Y - halfY, box.Centroid.Z + halfZ)),
                new VertexData(new Vector3f(box.Centroid.X - halfX, box.Centroid.Y - halfY, box.Centroid.Z -halfZ))
            };

            return new BVH(
                verts,
                0,
                box.Centroid,
                defaultMargin_,
                box.Materials,
                new Vector4i[0],
                new BVHPrimitive[] { new BVHBox(0, 1, 2, 3, 0) },
                1,
                new float[0],
                1.0f,
                new Vector3f[0]);
        }

        private static BVH CapsuleToBVH(Capsule capsule)
        {
            VertexData[] verts = new VertexData[2]
            {
                new VertexData(new Vector3f(capsule.Centroid.X, capsule.Centroid.Y - (capsule.Length * 0.5f), capsule.Centroid.Z)),
                new VertexData(new Vector3f(capsule.Centroid.X, capsule.Centroid.Y + (capsule.Length * 0.5f), capsule.Centroid.Z))
            };

            return new BVH(
                verts,
                0,
                new Vector3f(capsule.Centroid.X, capsule.Centroid.Y, capsule.Centroid.Z),
                defaultMargin_,
                capsule.Materials,
                new Vector4i[0],
                new BVHPrimitive[] { new BVHCapsule(0, 1, capsule.Radius, 0) },
                1,
                new float[0],
                1.0f,
                new Vector3f[0]);
        }

        private static BVH CylinderToBVH(Cylinder cylinder)
        {
            VertexData[] verts = new VertexData[2]
            {
                new VertexData(new Vector3f(cylinder.Centroid.X, cylinder.Centroid.Y - (cylinder.Length * 0.5f), cylinder.Centroid.Z)),
                new VertexData(new Vector3f(cylinder.Centroid.X, cylinder.Centroid.Y + (cylinder.Length * 0.5f), cylinder.Centroid.Z))
            };

            return new BVH(
                verts,
                0,
                new Vector3f(cylinder.Centroid.X, cylinder.Centroid.Y, cylinder.Centroid.Z),
                defaultMargin_,
                cylinder.Materials,
                new Vector4i[0],
                new BVHPrimitive[] { new BVHCylinder(0, 1, cylinder.Radius, 0) },
                1,
                new float[0],
                1.0f,
                new Vector3f[0]);
        }

        private static BVH GeometryToBVH(Geometry geometry)
        {
            return new BVH(
                geometry.Verts,
                0,
                geometry.CentreOfGravity,
                defaultMargin_,
                geometry.Materials,
                geometry.MaterialColours,
                geometry.Primitives,
                1,
                geometry.SecondSurfaceDisplacements,
                geometry.SecondSurfaceMaxHeight,
                geometry.ShrunkVerts);
        }

        private static BVH SphereToBVH(Sphere sphere)
        {
            VertexData[] verts = new VertexData[1]
            {
                new VertexData(sphere.Centroid)
            };

            return new BVH(
                verts,
                0,
                new Vector3f(sphere.Centroid),
                defaultMargin_,
                sphere.Materials,
                new Vector4i[0],
                new BVHPrimitive[] { new BVHSphere(0, sphere.Radius, 0) },
                1,
                new float[0],
                1.0f,
                new Vector3f[0]);
        }

        private static readonly float defaultMargin_ = RSG.ManagedRage.PhysicsConstants.ConcaveDistanceMargin;
    }
}
