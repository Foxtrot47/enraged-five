﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Bounds
{
    /// <summary>
    /// A geometry primitive as it appears in a BND file
    /// </summary>
    public class Geometry : BoundObject
    {
        public Geometry(
            VertexData[] verts,
            uint numPerVertAttributes, 
            RSG.Base.Math.Vector3f centreOfGravity,
            float margin,
            string[] materials,
            RSG.Base.Math.Vector4i[] materialColours,
            BVHPrimitive[] primitives,
            uint readEdgeNormals,
            float[] secondSurfaceDisplacements,
            float secondSurfaceMaxHeight,
            RSG.Base.Math.Vector3f[] shrunkVerts)
        {
            Verts = verts;
            NumPerVertAttributes = numPerVertAttributes;
            CentreOfGravity = centreOfGravity;
            Margin = margin;
            Materials = materials;
            MaterialColours = materialColours;
            Primitives = primitives;
            ReadEdgeNormals = readEdgeNormals;
            SecondSurfaceDisplacements = secondSurfaceDisplacements;
            SecondSurfaceMaxHeight = secondSurfaceMaxHeight;
            ShrunkVerts = shrunkVerts;

            CalculateBounds();
        }

        public override void Save(System.IO.StreamWriter streamWriter)
        {
            streamWriter.WriteLine("version: 1.10");
            streamWriter.WriteLine("type: geometry");
            streamWriter.WriteLine("verts: {0}", Verts.Length);
            streamWriter.WriteLine("cg: {0}", DataConversion.VectorToString(CentreOfGravity));
            streamWriter.WriteLine("margin: {0}", Margin);
            streamWriter.WriteLine();
            streamWriter.WriteLine("materials: {0}", Materials.Length);
            streamWriter.WriteLine("materialcolors: {0}", MaterialColours.Length);
            // We don't want to serialise this at ALL for gen 6 DLC.
            if(NumPerVertAttributes > 0)
                streamWriter.WriteLine("pervertattribs: {0}", NumPerVertAttributes);
            streamWriter.WriteLine("edges: 0");
            streamWriter.WriteLine("polys: {0}", Primitives.Length);
            streamWriter.WriteLine("readedgenormals: {0}", ReadEdgeNormals);
            streamWriter.WriteLine();
            streamWriter.WriteLine("secondSurface: {0}", SecondSurfaceDisplacements.Length > 0 ? 1 : 0);
            streamWriter.WriteLine("secondSurfaceMaxHeight: {0}", SecondSurfaceMaxHeight);
            streamWriter.WriteLine();
            foreach (var vert in Verts)
            {
                streamWriter.WriteLine("v {0}", DataConversion.VectorToString(vert.Position));
            }
            streamWriter.WriteLine();

            // If we're carrying per vertex attribute data, serialise it out.
            if (NumPerVertAttributes > 0)
            {
                foreach (var vert in Verts)
                {
                    streamWriter.WriteLine("va ");
                    foreach (var attr in vert.AttributeData)
                    {
                        streamWriter.Write("{0} {1} {2} {3} ", attr.X, attr.Y, attr.Z, attr.W);
                    }
                    streamWriter.WriteLine();
                }
            }
            streamWriter.WriteLine();

            foreach (var displacement in SecondSurfaceDisplacements)
            {
                streamWriter.WriteLine("f {0}", displacement);
            }
            streamWriter.WriteLine();
            if (ShrunkVerts.Length > 0)
            {
                foreach (var vert in ShrunkVerts)
                {
                    streamWriter.WriteLine("v {0}", DataConversion.VectorToString(vert));
                }
                streamWriter.WriteLine();
            }
            foreach (var material in Materials)
            {
                streamWriter.WriteLine(material);
            }
            streamWriter.WriteLine();
            foreach (var materialColour in MaterialColours)
            {
                streamWriter.WriteLine("materialcolor {0} {1} {2} {3}", materialColour.X, materialColour.Y, materialColour.Z, materialColour.W);
            }
            streamWriter.WriteLine();
            foreach (var primitive in Primitives)
            {
                primitive.Save(streamWriter);
            }
        }

        internal static Geometry Parse(string version, TokenCollection tokenCollection)
        {
            if (version == null)
                throw new ArgumentNullException("version");
            if (tokenCollection == null)
                throw new ArgumentNullException("tokenCollection");

            if (version != "1.10")
                throw new InvalidDataVersionException(string.Format("Encountered unsupported version of '{0}' when parsing geometry", version));

            uint numVerts = tokenCollection.ReadUIntTokenValue("verts:");
            RSG.Base.Math.Vector3f centreOfGravity = null;// the centre of gravity is optional
            if (tokenCollection.PeekTokenName() == "cg:")
            {
                centreOfGravity = tokenCollection.ReadVector3fTokenValue("cg:");
            }
            float margin = tokenCollection.ReadFloatTokenValue("margin:");
            uint numMaterials = tokenCollection.ReadUIntTokenValue("materials:");
            uint numMaterialColours = tokenCollection.ReadUIntTokenValue("materialcolors:");
            uint numVertAttributes = 0;
            try
            {
                numVertAttributes = tokenCollection.ReadUIntTokenValue("pervertattribs:");
            }
            catch (InvalidDataFormatException ex)
            {
                // Crappy version handling due to looming V NG DL task.  We need version specific parsers 
            }
            uint numEdges = tokenCollection.ReadUIntTokenValue("edges:");// Always 0
            uint numPolys = tokenCollection.ReadUIntTokenValue("polys:");
            uint readEdgeNormals = tokenCollection.ReadUIntTokenValue("readedgenormals:");// Always 1
            uint secondSurface = tokenCollection.ReadUIntTokenValue("secondSurface:");
            float secondSurfaceMaxHeight = tokenCollection.ReadFloatTokenValue("secondSurfaceMaxHeight:");

            VertexData[] vertices = new VertexData[numVerts];
            for (uint vertIndex = 0; vertIndex < numVerts; ++vertIndex)
            {
                vertices[vertIndex] = new VertexData(tokenCollection.ReadVector3fTokenValue("v"));
            }

            if (numVertAttributes > 0)
            {
                try
                {
                    for (uint vertIndex = 0; vertIndex < numVerts; ++vertIndex)
                    {
                        vertices[vertIndex].AttributeData = tokenCollection.ReadMultipleVector4iTokenValues("va", numVertAttributes);
                    }
                }
                catch (InvalidDataFormatException ex)
                {
                    // Crappy version handling due to looming V NG DL task.  We need verison specific parsers 
                }
            }

            float[] secondSurfaceDisplacements = null;// the second surface displacement data is optional
            if (secondSurface != 0)
            {
                secondSurfaceDisplacements = new float[numVerts];
                for (uint vertIndex = 0; vertIndex < numVerts; ++vertIndex)
                {
                    secondSurfaceDisplacements[vertIndex] = tokenCollection.ReadFloatTokenValue("f");
                }
            }
            else
            {
                secondSurfaceDisplacements = new float[0];
            }

            RSG.Base.Math.Vector3f[] shrunkVertices = null;// the shrunk vertices data is optional
            if (tokenCollection.PeekTokenName() == "shrunk:")
            {
                shrunkVertices = new RSG.Base.Math.Vector3f[numVerts];
                for (uint vertIndex = 0; vertIndex < numVerts; ++vertIndex)
                {
                    shrunkVertices[vertIndex] = tokenCollection.ReadVector3fTokenValue("v");
                }
            }
            else
            {
                shrunkVertices = new RSG.Base.Math.Vector3f[0];
            }

            string[] materialIds = new string[numMaterials];
            for (uint materialIndex = 0; materialIndex < numMaterials; ++materialIndex)
            {
                materialIds[materialIndex] = tokenCollection.ReadTokenName();
            }

            RSG.Base.Math.Vector4i[] materialColours = new RSG.Base.Math.Vector4i[numMaterialColours];
            for (uint materialColourIndex = 0; materialColourIndex < numMaterialColours; ++materialColourIndex)
            {
                materialColours[materialColourIndex] = tokenCollection.ReadVector4iTokenValue("materialcolor");
            }

            // Unused, but read in the value if it's there to temporarily handle old file formats.
            int vertexMaterial = -1;
            if (tokenCollection.PeekTokenName() == "vertexmaterial:")
            {
                vertexMaterial = tokenCollection.ReadIntTokenValue("vertexmaterial:");
            }

            BVHPrimitive[] primitives = new BVHPrimitive[numPolys];
            for (uint polyIndex = 0; polyIndex < numPolys; ++polyIndex)
            {
                string polyTypeToken = tokenCollection.PeekTokenName();
                if (polyTypeToken == "tri")
                {
                    primitives[polyIndex] = BVHTriangle.Parse(tokenCollection.ReadStringTokenValue("tri"));
                }
                else if (polyTypeToken == "quad")
                {
                    primitives[polyIndex] = BVHQuad.Parse(tokenCollection.ReadStringTokenValue("quad"));
                }
            }

            return new Geometry(
                vertices,
                numVertAttributes,
                centreOfGravity,
                margin,
                materialIds,
                materialColours,
                primitives,
                readEdgeNormals,
                secondSurfaceDisplacements,
                secondSurfaceMaxHeight,
                shrunkVertices);
        }

        public VertexData[] Verts { get; private set; }
        public uint NumPerVertAttributes { get; private set; }
        public RSG.Base.Math.Vector3f CentreOfGravity { get; private set; }
        public float Margin { get; private set; }
        public string[] Materials { get; private set; }
        public RSG.Base.Math.Vector4i[] MaterialColours { get; private set; }
        public BVHPrimitive[] Primitives { get; private set; }
        public uint ReadEdgeNormals { get; private set; }
        public float[] SecondSurfaceDisplacements { get; private set; }
        public float SecondSurfaceMaxHeight { get; private set; }
        public RSG.Base.Math.Vector3f[] ShrunkVerts { get; private set; }

        public RSG.Base.Math.BoundingBox3f Bounds { get; private set; }

        private void CalculateBounds()
        {
            float minX = float.MaxValue;
            float minY = float.MaxValue;
            float minZ = float.MaxValue;
            float maxX = float.MinValue;
            float maxY = float.MinValue;
            float maxZ = float.MinValue;

            foreach (var vert in Verts)
            {
                if (vert.Position.X < minX)
                    minX = vert.Position.X;
                if (vert.Position.Y < minY)
                    minY = vert.Position.Y;
                if (vert.Position.Z < minZ)
                    minZ = vert.Position.Z;
                if (vert.Position.X > maxX)
                    maxX = vert.Position.X;
                if (vert.Position.Y > maxY)
                    maxY = vert.Position.Y;
                if (vert.Position.Z > maxZ)
                    maxZ = vert.Position.Z;
            }

            Bounds = new RSG.Base.Math.BoundingBox3f(new RSG.Base.Math.Vector3f(minX, minY, minZ), new RSG.Base.Math.Vector3f(maxX, maxY, maxZ));
        }
    }
}
