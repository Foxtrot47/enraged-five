﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Base.Math;

namespace RSG.Bounds
{
    /// <summary>
    /// A box within a BVH
    /// </summary>
    public class BVHBox : BVHPrimitive
    {
        public BVHBox(int index0, int index1, int index2, int index3, int materialIndex)
        {
            Index0 = index0;
            Index1 = index1;
            Index2 = index2;
            Index3 = index3;
            MaterialIndex = materialIndex;
        }

        #region BVHPrimitive overrides
        public override BVHPrimitive Clone()
        {
            return new BVHBox(Index0, Index1, Index2, Index3, MaterialIndex);
        }

        public override BVHPrimitive Clone(int vertexRebaseIndex, int materialRebaseIndex)
        {
            return new BVHBox(Index0 + vertexRebaseIndex, Index1 + vertexRebaseIndex, Index2 + vertexRebaseIndex, Index3 + vertexRebaseIndex, MaterialIndex + materialRebaseIndex);
        }

        public override IEnumerable<int> GetVertexIndices()
        {
            return new int[4] { Index0, Index1, Index2, Index3 };
        }

        public override void Save(System.IO.StreamWriter streamWriter)
        {
            streamWriter.WriteLine("box {0} {1} {2} {3} {4}", Index0, Index1, Index2, Index3, MaterialIndex);
        }

        public override void VertHasBeenCombined(int deadVertex, int replacementVertex)
        {
            if (Index0 == deadVertex)
                Index0 = replacementVertex;
            else if (Index0 > deadVertex)
                --Index0;

            if (Index1 == deadVertex)
                Index1 = replacementVertex;
            else if (Index1 > deadVertex)
                --Index1;

            if (Index2 == deadVertex)
                Index2 = replacementVertex;
            else if (Index2 > deadVertex)
                --Index2;

            if (Index3 == deadVertex)
                Index3 = replacementVertex;
            else if (Index3 > deadVertex)
                --Index3;
        }

        public override void MaterialHasBeenCombined(int deadMaterial, int replacementMaterial)
        {
            if (MaterialIndex == deadMaterial)
                MaterialIndex = replacementMaterial;
            else if (MaterialIndex > deadMaterial)
                --MaterialIndex;
        }

        public override void ScaleInternal(Vector3f scale)
        {

        }

        public override BoundingBox3f GetBoundingBox(Vector3f[] verts)
        {
            BoundingBox3f boundingBox = new BoundingBox3f();

            boundingBox.Expand(verts[Index0]);
            boundingBox.Expand(verts[Index1]);
            boundingBox.Expand(verts[Index2]);
            boundingBox.Expand(verts[Index3]);

            return boundingBox;
        }

        public override BoundingBox3f GetBoundingBox(VertexData[] verts)
        {
            BoundingBox3f boundingBox = new BoundingBox3f();

            boundingBox.Expand(verts[Index0].Position);
            boundingBox.Expand(verts[Index1].Position);
            boundingBox.Expand(verts[Index2].Position);
            boundingBox.Expand(verts[Index3].Position);

            return boundingBox;
        }
        #endregion

        public int Index0 { get; private set; }
        public int Index1 { get; private set; }
        public int Index2 { get; private set; }
        public int Index3 { get; private set; }
        public int MaterialIndex { get; private set; }

        internal static BVHBox Parse(string text)
        {
            string[] tokens = text.Split(' ', '\t');

            return new BVHBox(int.Parse(tokens[0]), int.Parse(tokens[1]), int.Parse(tokens[2]), int.Parse(tokens[3]), int.Parse(tokens[4]));
        }
    }
}
