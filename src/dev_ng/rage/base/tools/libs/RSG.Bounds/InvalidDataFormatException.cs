﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Bounds
{
    /// <summary>
    /// Data encountered was in an unexpected format
    /// </summary>
    public class InvalidDataFormatException : Exception
    {
        public InvalidDataFormatException(string message) 
            : base(message)
        {
        }
    }
}
