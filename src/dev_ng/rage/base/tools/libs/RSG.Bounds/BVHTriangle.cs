﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Base.Math;

namespace RSG.Bounds
{
    /// <summary>
    /// A triangle as it appears in a BVH
    /// </summary>
    public class BVHTriangle : BVHPrimitive
    {
        public BVHTriangle(int index0, int index1, int index2, int materialIndex)
        {
            Index0 = index0;
            Index1 = index1;
            Index2 = index2;
            MaterialIndex = materialIndex;
        }

        #region BVHPrimitive overrides
        public override BVHPrimitive Clone()
        {
            return new BVHTriangle(Index0, Index1, Index2, MaterialIndex);
        }

        public override BVHPrimitive Clone(int vertexRebaseIndex, int materialRebaseIndex)
        {
            return new BVHTriangle(Index0 + vertexRebaseIndex, Index1 + vertexRebaseIndex, Index2 + vertexRebaseIndex, MaterialIndex + materialRebaseIndex);
        }

        public override IEnumerable<int> GetVertexIndices()
        {
            return new int[3] { Index0, Index1, Index2 };
        }

        public override void Save(System.IO.StreamWriter streamWriter)
        {
            streamWriter.WriteLine("tri {0} {1} {2} {3} 0 0 0", Index0, Index1, Index2, MaterialIndex);
        }

        public override void VertHasBeenCombined(int deadVertex, int replacementVertex)
        {
            if (Index0 == deadVertex)
                Index0 = replacementVertex;
            else if (Index0 > deadVertex)
                --Index0;

            if (Index1 == deadVertex)
                Index1 = replacementVertex;
            else if (Index1 > deadVertex)
                --Index1;

            if (Index2 == deadVertex)
                Index2 = replacementVertex;
            else if (Index2 > deadVertex)
                --Index2;
        }

        public override void MaterialHasBeenCombined(int deadMaterial, int replacementMaterial)
        {
            if (MaterialIndex == deadMaterial)
                MaterialIndex = replacementMaterial;
            else if (MaterialIndex > deadMaterial)
                --MaterialIndex;
        }

        public override void ScaleInternal(RSG.Base.Math.Vector3f scale)
        {

        }

        public override BoundingBox3f GetBoundingBox(Vector3f[] verts)
        {
            BoundingBox3f boundingBox = new BoundingBox3f();

            boundingBox.Expand(verts[Index0]);
            boundingBox.Expand(verts[Index1]);
            boundingBox.Expand(verts[Index2]);

            return boundingBox;
        }

        public override BoundingBox3f GetBoundingBox(VertexData[] verts)
        {
            BoundingBox3f boundingBox = new BoundingBox3f();

            boundingBox.Expand(verts[Index0].Position);
            boundingBox.Expand(verts[Index1].Position);
            boundingBox.Expand(verts[Index2].Position);

            return boundingBox;
        }
        #endregion

        public int Index0 { get; private set; }
        public int Index1 { get; private set; }
        public int Index2 { get; private set; }
        public int MaterialIndex { get; private set; }

        internal static BVHTriangle Parse(string text)
        {
            string[] tokens = text.Split(' ', '\t');

            return new BVHTriangle(int.Parse(tokens[0]), int.Parse(tokens[1]), int.Parse(tokens[2]), int.Parse(tokens[3]));
        }
    }
}
