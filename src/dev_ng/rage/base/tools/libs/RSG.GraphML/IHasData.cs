﻿using System;
using System.Collections.Generic;

namespace RSG.GraphML
{

    /// <summary>
    /// 
    /// </summary>
    public interface IHasData
    {
        #region Properties
        /// <summary>
        /// Collection of Data objects.
        /// </summary>
        Data[] Data { get; }
        #endregion // Properties
    }

} // RSG.GraphML namespace
