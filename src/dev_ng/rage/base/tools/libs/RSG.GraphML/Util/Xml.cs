﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Xml;
using RSG.Base.Logging;

namespace RSG.GraphML.Util
{

    /// <summary>
    /// XML parsing constants and utility functions for parsing GraphML 
    /// documents.
    /// </summary>
    internal static class Xml
    {
        #region Static Constants
        internal static readonly String LOG_CONTEXT = "GraphML";
        internal static readonly String ATTR_ELEMENT_ID = "id";

        // Top-level Element Constants
        internal static readonly String ELEMENT_GRAPHML = "graphml";
        internal static readonly String ELEMENT_KEY = "key";
        internal static readonly String ELEMENT_GRAPH = "graph";

        // Graph Element Constants
        internal static readonly String ELEMENT_GRAPH_NODE = "node";
        internal static readonly String ELEMENT_GRAPH_EDGE = "edge";
        internal static readonly String ELEMENT_GRAPH_HYPEREDGE = "hyperedge";
        internal static readonly String ATTR_GRAPH_EDGEDEFAULT = "edgedefault";
        internal static readonly String ATTR_GRAPH_VALUE_DIRECTED = "directed";
        internal static readonly String ATTR_GRAPH_VALUE_UNDIRECTED = "undirected";

        // Key / Attribute Elements
        internal static readonly String ELEMENT_ATTRIBUTE_DEFAULT = "default";

        // Graph Attribute Constants
        internal static readonly String ATTR_ATTRIBUTE_FOR = "for";
        internal static readonly String ATTR_ATTRIBUTE_VALUE_GRAPH = "graph";
        internal static readonly String ATTR_ATTRIBUTE_VALUE_NODE = "node";
        internal static readonly String ATTR_ATTRIBUTE_VALUE_EDGE = "edge";
        internal static readonly String ATTR_ATTRIBUTE_NAME = "attr.name";
        internal static readonly String ATTR_ATTRIBUTE_TYPE = "attr.type";
        internal static readonly String ATTR_ATTRIBUTE_VALUE_BOOL = "boolean";
        internal static readonly String ATTR_ATTRIBUTE_VALUE_INT = "int";
        internal static readonly String ATTR_ATTRIBUTE_VALUE_LONG = "long";
        internal static readonly String ATTR_ATTRIBUTE_VALUE_FLOAT = "float";
        internal static readonly String ATTR_ATTRIBUTE_VALUE_DOUBLE = "double";
        internal static readonly String ATTR_ATTRIBUTE_VALUE_STRING = "string";

        // Node Sub-elements
        internal static readonly String ELEMENT_NODE_PORT = "port";
        internal static readonly String ELEMENT_NODE_DATA = "data";

        // Edge Sub-elements
        internal static readonly String ELEMENT_EDGE_DATA = "data";
        internal static readonly String ATTR_EDGE_SOURCE = "source";
        internal static readonly String ATTR_EDGE_TARGET = "target";

        // Data Sub-elements
        internal static readonly String ATTR_DATA_KEY = "key";

        // Port Sub-elements
        internal static readonly String ATTR_PORT_NAME = "name";
        #endregion // Static Constants

        #region Static XML Parsing Helper Methods
        /// <summary>
        /// Move forward on an XmlReader past the current Element.
        /// </summary>
        /// <param name="reader"></param>
        public static void ParseSkip(XmlReader reader)
        {
            Xml.ValidateXmlParse(Xml.ParseSkipImpl, reader);
        }

        // Internal implementation of ParseSkip; allowing validation wrapper.
        internal static void ParseSkipImpl(XmlReader reader)
        {
            if (reader.IsEmptyElement)
                reader.ReadStartElement();
            else
            {
                reader.ReadStartElement();
                while (reader.MoveToContent() == XmlNodeType.Element)
                {
                    ParseSkip(reader);
                }
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                Debug.Assert(XmlNodeType.EndElement == reader.NodeType);
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
        }

        /// <summary>
        /// Execute parseFunc and ensure that the sub-reader is only passed in
        /// the outer XML.  This ensure it can't read beyond the point its meant
        /// to.
        /// </summary>
        /// <param name="parseFunc"></param>
        /// <param name="reader"></param>
        public static void ValidateXmlParse(Action<XmlReader> parseFunc, XmlReader reader)
        {
            String outerXml = reader.ReadOuterXml();
            Byte[] data = Encoding.ASCII.GetBytes(outerXml);
            using (MemoryStream ms = new MemoryStream(data))
            {
                XmlTextReader subReader = new XmlTextReader(ms);
                parseFunc.Invoke(subReader);
            }
        }
        #endregion // Static XML Parsing Helper Methods
    }

} // RSG.GraphML.Util
