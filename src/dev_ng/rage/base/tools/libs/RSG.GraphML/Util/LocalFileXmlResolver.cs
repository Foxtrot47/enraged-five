﻿using System;
using System.Diagnostics;
using System.IO;
using System.Xml;
using RSG.Base.Configuration;

namespace RSG.GraphML.Util
{

    /// <summary>
    /// XmlResolver subclass for resolving local files only; based on whether
    /// the file is relative or absolute path.
    /// </summary>
    internal class LocalFileXmlResolver : XmlResolver
    {
        public override System.Net.ICredentials Credentials
        {
            set { }
        }

        private IBranch Branch { get; set; }

        public LocalFileXmlResolver(IBranch branch = null)
            : base()
        {
            Branch = branch;
        }

        public override object GetEntity(Uri absoluteUri, String role, Type ofObjectToReturn)
        {
            try
            {
                Debug.Assert(absoluteUri.IsFile, "File not specified!");
                Debug.Assert(File.Exists(absoluteUri.LocalPath));
                string localPath = absoluteUri.LocalPath;

                int envVarIndex = localPath.IndexOf("$(", StringComparison.Ordinal);
                if (envVarIndex != -1)
                {
                    localPath = localPath.Substring(envVarIndex);
                    if (Branch != null)
                    {
                        localPath = this.Branch.Environment.Subst(localPath);
                    }
                    else
                    {
                        throw new NullReferenceException("You're trying to resolve a path containing $(env-based) arguments without having the Branch initialized. There's something wrong here.");
                    }
                }

                if (!File.Exists(localPath))
                    throw (new FileNotFoundException("GraphML XML include file not found.", localPath));
                return (new FileStream(localPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite));
            }
            catch (FileNotFoundException)
            {
                throw;
            }
            catch (Exception ex)
            {
                Graph.Log.ToolException(ex, "Unhandled exception creating FileStream for '{0}'.", absoluteUri);
                throw;
            }
        }

        public override Uri ResolveUri(Uri baseUri, String relativeUri)
        {
            if (Path.IsPathRooted(relativeUri))
                return (new Uri(relativeUri, UriKind.Absolute));
            else
            {
                String path = Environment.ExpandEnvironmentVariables(relativeUri);
                if (Path.IsPathRooted(path))
                    return (new Uri(path, UriKind.Absolute));
                else
                {
                    String absolutePath = Path.Combine(Directory.GetCurrentDirectory(), relativeUri);
                    return (new Uri(absolutePath, UriKind.Absolute));
                }
            }
        }
    }

} // RSG.GraphML.Util namespace
