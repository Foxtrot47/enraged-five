﻿using System;
using System.Diagnostics;

namespace RSG.GraphML.Util
{

    /// <summary>
    /// GraphML static utility functions.
    /// </summary>
    internal static class GraphML
    {
        #region Private Data
        private static readonly char NODE_NAMESPACE_SEP_CHAR = ':';
        private static readonly String NODE_NAMESPACE_SEP = "::";
        private static readonly String[] NODE_NAMESPACE_SEPS =
            new String[] { NODE_NAMESPACE_SEP };
        #endregion // Private Data

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static String[] SplitId(String id)
        {
            String[] parts = id.Split(NODE_NAMESPACE_SEPS, StringSplitOptions.RemoveEmptyEntries);
            Debug.Assert(parts.Length > 0,
                String.Format("Invalid identifier ({0}).  Parse failed.", id));
            return (parts);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parts"></param>
        /// <returns></returns>
        public static String JoinId(String[] parts, int startIndex = 0, int count = -1)
        {
            if (-1 == count)
                return (String.Join(NODE_NAMESPACE_SEP, parts, startIndex, parts.Length - startIndex));
            else
                return (String.Join(NODE_NAMESPACE_SEP, parts, startIndex, count));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static String TrimId(String id)
        {
            return (id.TrimEnd(NODE_NAMESPACE_SEP_CHAR));
        }

        /// <summary>
        /// Validate a nested Graph object.
        /// </summary>
        /// <param name="g"></param>
        /// <returns></returns>
        public static bool ValidateNestedGraph(Graph g)
        {
            if (null == g.Owner)
                return (true); // Not nested.

            Debug.Assert(g.IsNested, "Graph has owner Node but is not IsNested.  Internal error.");
            if (!g.IsNested)
                return (false);

            bool validID = g.QualifiedID.StartsWith(g.Owner.QualifiedID);
            Debug.Assert(validID, "Nested graph has invalid ID: no node prefix.");
            if (!validID)
                return (false);

            return (true);
        }
    }

} // RSG.GraphML.Util
