﻿using System;
using System.Collections.Generic;

namespace RSG.GraphML
{
    
    /// <summary>
    /// Graph interface.
    /// </summary>
    public interface IGraph :
        INodeOwner,
        IGraphCollectionContainer,
        IHasData
    {
        #region Properties
        /// <summary>
        /// Unique string identifier.
        /// </summary>
        String ID { get; }

        /// <summary>
        /// Edge type.
        /// </summary>
        EdgeDefaultType EdgeType { get; }

        /// <summary>
        /// Is this Graph nested?
        /// </summary>
        bool IsNested { get; }
        #endregion // Properties

        #region Methods
        #region Node Methods
        /// <summary>
        /// Return an enumerable of the Graph's immediate nodes.
        /// </summary>
        /// <returns></returns>
        IEnumerable<Node> Nodes();

        /// <summary>
        /// Return an enumerable of all the Graph's nodes (recursive).
        /// </summary>
        /// <returns></returns>
        IEnumerable<Node> AllNodes();

        /// <summary>
        /// Determine whether the Graph has an immediate Node with a particular ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        bool HasNode(String id);

        /// <summary>
        /// Determine whether the Graph has any Node with a particular ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        bool HasAnyNode(String id);

        /// <summary>
        /// Return Node with particular ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Node GetNode(String id);
        #endregion // Node Methods

        #region Edge Methods
        /// <summary>
        /// Return an enumerable of the Graph's immediate Edges.
        /// </summary>
        /// <returns></returns>
        IEnumerable<Edge> Edges();

        /// <summary>
        /// Return an enumerable of all Graph's Edges (recursive).
        /// </summary>
        /// <returns></returns>
        IEnumerable<Edge> AllEdges();

        /// <summary>
        /// Return edge in this graph.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        bool HasEdge(String id);
        bool HasEdge(String sourceId, String targetId);
        bool HasEdge(Node source, Node target);

        Edge GetEdge(String id);
        Edge GetEdge(String source, String target);
        Edge GetEdge(Node source, Node target);

        /// <summary>
        /// Return edge if it exists (recursive).
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        bool HasAnyEdge(String id);
        bool HasAnyEdge(String sourceId, String targetId);
        bool HasAnyEdge(Node source, Node target);

        Edge GetAnyEdge(String id);
        Edge GetAnyEdge(String sourceId, String targetId);
        Edge GetAnyEdge(Node source, Node target);
        #endregion // Edge Methods
        #endregion // Methods
    }

    /// <summary>
    /// Graph edge type.
    /// </summary>
    public enum EdgeDefaultType
    {
        Directed,
        Undirected
    }

} // RSG.GraphML namespace
