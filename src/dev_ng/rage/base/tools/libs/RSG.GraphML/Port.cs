﻿using System;
using System.Xml;
using System.Xml.Linq;
using RSG.GraphML.Util;

namespace RSG.GraphML
{

    /// <summary>
    /// GraphML node port.
    /// </summary>
    public class Port : 
        INamed,
        IGraphOwner,
        ISerialisable
    {
        #region Properties
        /// <summary>
        /// Unique name per-Node.
        /// </summary>
        public String Name
        {
            get;
            protected set;
        }

        /// <summary>
        /// Owner Graph object.
        /// </summary>
        public Graph Owner
        {
            get;
            protected set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="name"></param>
        public Port(IElement owner, String name)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="reader"></param>
        public Port(IElement owner, XmlReader reader)
        {
            Util.Xml.ValidateXmlParse(Parse, reader);
        }
        #endregion // Constructor(s)

        #region Controller Methods
        #region ISerialisable Interface Methods
        /// <summary>
        /// Serialise object using System.Xml.Linq.
        /// </summary>
        /// <param name="writer"></param>
        public XElement ToXElement()
        {
            throw new NotImplementedException();
        }
        #endregion // ISerialisable Interface Methods
        #endregion // Controller Methods
        
        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        private void Parse(XmlReader reader)
        {
            ParseDataAttributes(reader);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        private void ParseDataAttributes(XmlReader reader)
        {
            this.Name = reader.GetAttribute(Util.Xml.ATTR_PORT_NAME);
        }
        #endregion // Private Methods
    }

} // RSG.GraphML namespace
