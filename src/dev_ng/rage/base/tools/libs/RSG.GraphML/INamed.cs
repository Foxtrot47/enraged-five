﻿using System;

namespace RSG.GraphML
{

    /// <summary>
    /// 
    /// </summary>
    interface INamed : IGraphOwner
    {
        /// <summary>
        /// Identifier.
        /// </summary>
        String Name { get; }
    }

} // RSG.GraphML namespace
