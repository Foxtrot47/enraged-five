﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.GraphML
{

    /// <summary>
    /// 
    /// </summary>
    public interface IKeyed : IGraphOwner
    {
        /// <summary>
        /// Identifier tying this back to unique Graph element.
        /// </summary>
        String Key { get; }
    }

} // RSG.GraphML namespace
