﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.GraphML
{

    /// <summary>
    /// 
    /// </summary>
    public interface IElement
    {
        /// <summary>
        /// String identifier.
        /// </summary>
        String ID { get; }

        /// <summary>
        /// 
        /// </summary>
        String QualifiedID { get; }
    }

} // RSG.GraphML namespace
