﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Base.Logging;

namespace RSG.GraphML
{

    /// <summary>
    /// Abstraction of a Graph object collection.
    /// </summary>
    /// An indexer by Graph ID is available for random access; for sequential
    /// (not-sorted) access use the IEnumerable interface.
    /// 
    public sealed class GraphCollection : 
        IEnumerable<Graph>,
        IHasAttributes,
        ISerialisable
    {
        #region Properties
        /// <summary>
        /// Random access to the Graph objects by ID.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public Graph this[String id]
        {
            get
            {
                Debug.Assert(this.m_graphs.ContainsKey(id),
                    String.Format("Graph ID '{0}' not found in GraphCollection (size: {1}).", id, this.m_graphs.Count));
                if (this.m_graphs.ContainsKey(id))
                    return (this.m_graphs[id]);
                else
                    return (null);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int Count
        {
            get { return this.m_graphs.Count; }
        }

        /// <summary>
        /// Dictionary of our defined Attributes.
        /// </summary>
        public IDictionary<String, Attribute> Attributes
        {
            get;
            private set;
        }

        /// <summary>
        /// Project Branch for path resolving
        /// </summary>
        private IBranch Branch { get; set; }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// 
        /// </summary>
        private Dictionary<String, Graph> m_graphs;

        /// <summary>
        /// GraphML load locking object; used to ensure we don't have multiple 
        /// loads occurring at the same time (since we use relative paths and
        /// change process working directory).
        /// </summary>
        private static Object _loadlock;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="branch"></param>
        public GraphCollection(IBranch branch)
        {
            Reset();
            Branch = branch;
        }

        /// <summary>
        /// Constructor; adding collection of Graphs from GraphML file.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="branch"></param>
        /// <exception cref="GraphLoadException" />
        public GraphCollection(String filename, IBranch branch)
        {
            Reset();
            Branch = branch;
            if (!LoadGraphs(filename))
                throw (new GraphLoadException("GraphML load failed.  See log."));
        }

        /// <summary>
        /// Constructor; adding collection of Graphs from multiple GraphML files.
        /// </summary>
        /// <param name="filenames"></param>
        /// <param name="branch"></param>
        /// <exception cref="GraphLoadException" />
        public GraphCollection(IEnumerable<String> filenames, IBranch branch)
        {
            Reset();
            Branch = branch;
            if (!LoadGraphs(filenames))
                throw (new GraphLoadException("GraphML load failed.  See log."));
        }

        /// <summary>
        /// Static constructor; to initialise the load lock object.
        /// </summary>
        static GraphCollection()
        {
            _loadlock = new Object();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Load a single top-level Graph object from a GraphML file.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public bool LoadGraphs(String filename)
        {
            bool result = true;
            lock (_loadlock)
            {
                using (new ProfileContext(Graph.Log, "Content", "GraphML load: {0}.", filename))
                {
                    String currentDir = Directory.GetCurrentDirectory();
                    try
                    {
                        Directory.SetCurrentDirectory(Path.GetDirectoryName(filename));
                        using (FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read))
                        {
                            result &= LoadGraphs(fs);
                        }
                    }
                    catch (Mvp.Xml.XInclude.FatalException ex)
                    {
                        // See if we have a FileNotFoundException at InnerException (or two levels deep).
                        if (null != ex && null != ex.InnerException && null != ex.InnerException.InnerException &&
                            ex.InnerException.InnerException is FileNotFoundException)
                        {
                            FileNotFoundException fex = (FileNotFoundException)ex.InnerException.InnerException;
                            Graph.Log.Error("GraphML XML XInclude file '{0}' not found.", fex.FileName);
                        }
                        else
                        {
                            Graph.Log.ToolException(ex, "GraphML resource exception loading '{0}'.", filename);
                        }
                        result = false;
                    }
                    catch (XmlException ex)
                    {
                        Graph.Log.Error("GraphML XML error parsing {0}: {1}", ex.SourceUri, ex.Message);
                        result = false;
                    }
                    catch (FileNotFoundException ex)
                    {
                        Graph.Log.ToolException(ex, "GraphML file '{0}' not found.", filename);
                        result = false;
                    }
                    catch (IOException ex)
                    {
                        Graph.Log.ToolException(ex, "IO exception accessing '{0}'.", filename);
                        result = false;
                    }
                    catch (Exception ex)
                    {
                        Graph.Log.ToolException(ex, "Exception during LoadGraphs.");
                        result = false;
                    }
                    finally
                    {
                        Directory.SetCurrentDirectory(currentDir);
                    }
                }
            }
            return (result);
        }

        /// <summary>
        /// Load a set of top-level Graph objects from multiple GraphML files.
        /// </summary>
        /// <param name="filenames"></param>
        /// <returns></returns>
        public bool LoadGraphs(IEnumerable<String> filenames)
        {
            bool result = true;
            foreach (String filename in filenames)
                result &= LoadGraphs(filename);
            return (result);
        }

        /// <summary>
        /// Load a single top-level Graph object from a Stream object.
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public bool LoadGraphs(Stream stream)
        {
            bool result = true;
            using (new ProfileContext(Graph.Log, "Content", "GraphML stream load: {0} bytes.", stream.Length))
            {
                using (Mvp.Xml.XInclude.XIncludingReader xmlReader = new Mvp.Xml.XInclude.XIncludingReader(stream))
                {
                    xmlReader.XmlResolver = new Util.LocalFileXmlResolver(Branch);
                    result &= Parse(xmlReader);
                }
            }
            return (result);
        }

        /// <summary>
        /// Load a set of top-level Graph objects from multiple Stream objects.
        /// </summary>
        /// <param name="streams"></param>
        /// <returns></returns>
        public bool LoadGraphs(IEnumerable<Stream> streams)
        {
            bool result = true;
            foreach (Stream stream in streams)
                result &= LoadGraphs(stream);
            return (result);
        }

        /// <summary>
        /// Returns whether or not the collection has a top-level Graph with a
        /// particular identifier.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool ContainsGraph(String id)
        {
            return (this.m_graphs.ContainsKey(id));
        }
        
        /// <summary>
        /// Determine whether the Graph has an Attribute with a particular ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool HasAttribute(String id)
        {
            return (this.Attributes.ContainsKey(id));
        }

        /// <summary>
        /// Return Attribute with particular ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Attribute GetAttribute(String id)
        {
            Debug.Assert(this.HasAttribute(id));
            return (this.Attributes[id]);
        }

        #region ISerialisable Interface Methods
        /// <summary>
        /// Serialise object using System.Xml.Linq.
        /// </summary>
        /// <param name="writer"></param>
        public XElement ToXElement()
        {
            throw new NotImplementedException();
        }
        #endregion // ISerialisable Interface Methods
        #endregion // Controller Methods

        #region IEnumerable<Graph> Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerator<Graph> GetEnumerator()
        {
            foreach (Graph graph in this.m_graphs.Values)
                yield return graph;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return (GetEnumerator());
        }
        #endregion // IEnumerable<Graph> Methods

        #region Private Methods
        /// <summary>
        /// Internal reset.
        /// </summary>
        private void Reset()
        {
            this.Attributes = new Dictionary<String, Attribute>();
            this.m_graphs = new Dictionary<String, Graph>();
        }

        /// <summary>
        /// GraphML XML schema validation callback.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void XmlValidationCallback(Object sender, ValidationEventArgs e)
        {
            if (XmlSeverityType.Error == e.Severity)
                Graph.Log.Error("Matching XML schema not found.  No validation occurred.  {0}.", e.Message);
            else
                Graph.Log.Warning("Validation error: {0}.", e.Message);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        private bool Parse(XmlReader reader)
        {
            bool result = true;
            reader.MoveToContent();
            while (reader.Read())
            {
                if (!reader.IsStartElement())
                    continue;

                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:
                        result &= ParseElement(reader);
                        break;
                    case XmlNodeType.EndElement:
                        reader.ReadEndElement();
                        break;
                    default:
                        Util.Xml.ParseSkip(reader);
                        break;
                }
            }
            return (result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        private bool ParseElement(XmlReader reader)
        {
            bool result = true;
            Debug.Assert(XmlNodeType.Element == reader.NodeType);

            if (String.Equals(Util.Xml.ELEMENT_KEY, reader.Name))
            {
                Attribute attr = new Attribute(this, reader);
                Debug.Assert(!this.Attributes.ContainsKey(attr.ID),
                    String.Format("Attribute '{0}' already added to attribute collection.  Ignoring.", attr.ID));
                if (!this.Attributes.ContainsKey(attr.ID))
                    this.Attributes.Add(attr.ID, attr);
                else
                    Graph.Log.Warning(reader, "Attribute '{0}' already added to attribute collection.  Ignoring.", attr.ID);
            }
            else if (String.Equals(Util.Xml.ELEMENT_GRAPH, reader.Name))
            {
                Graph graph = new Graph(this, reader);
                Debug.Assert(!this.m_graphs.ContainsKey(graph.QualifiedID),
                    String.Format("Graph Collection already contains graph with ID: {0}.", graph.QualifiedID));
                if (this.m_graphs.ContainsKey(graph.QualifiedID))
                {
                    Graph.Log.Error(reader, "Graph Collection already contains graph with ID: {0}.", graph.QualifiedID);
                    result = false;
                }
                
                this.m_graphs[graph.QualifiedID] = graph;
            }
            else if (!String.Equals(Util.Xml.ELEMENT_GRAPHML, reader.Name))
            {
                Debug.Assert(false, String.Format("Unexpected XML element <{0}>.", reader.Name));
                Graph.Log.Error(reader, "Unexpected XML element <{0}>.", reader.Name);
                result = false;
                Util.Xml.ParseSkip(reader);
            }
            return (result);
        }
        #endregion // Private Methods
    }

} // RSG.GraphML namespace
