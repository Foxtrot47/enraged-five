﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using RSG.Base.Logging;

namespace RSG.GraphML
{

    /// <summary>
    /// Edge represents a graph edge between two nodes.  If the Graph is directed
    /// then Source and Target are important; otherwise they are not significant.
    /// </summary>
    /// The SourcePort and TargetPort properties are optional. 
    ///
    public class Edge :
        Element,
        IElement,
        IConnection,
        IHasData,
        IGraphOwner,
        ISerialisable
    {
        #region Properties
        /// <summary>
        /// Source node.
        /// </summary>
        public Node Source
        {
            get;
            private set;
        }

        /// <summary>
        /// Target node.
        /// </summary>
        public Node Target
        {
            get;
            private set;
        }

        /// <summary>
        /// Port on Source node (default: null, optional).
        /// </summary>
        public Port SourcePort
        {
            get;
            private set;
        }

        /// <summary>
        /// Port on Target node (default: null, optional).
        /// </summary>
        public Port TargetPort
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="id"></param>
        public Edge(Graph owner, String id)
            : base(owner, id)
        {
        }

        /// <summary>
        /// Constructor with source and target nodes.
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="id"></param>
        /// <param name="source"></param>
        /// <param name="target"></param>
        /// <param name="sp"></param>
        /// <param name="tp"></param>
        public Edge(Graph owner, String id, Node source, Node target, Port sp = null, Port tp = null)
            : base(owner, id)
        {
            this.Source = source;
            this.Target = target;
            this.SourcePort = sp;
            this.TargetPort = tp;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        public Edge(Graph owner, XmlReader reader)
            : base(owner)
        {
            Util.Xml.ValidateXmlParse(Parse, reader);
        }
        #endregion // Constructor(s)

        #region Controller Methods
        #region ISerialisable Interface Methods
        /// <summary>
        /// Serialise object using System.Xml.Linq.
        /// </summary>
        /// <param name="writer"></param>
        public XElement ToXElement()
        {
            throw new NotImplementedException();
        }
        #endregion // ISerialisable Interface Methods
        #endregion // Controller Methods

        #region Object-Overridden Methods
        /// <summary>
        /// Object equality.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            Edge other = obj as Edge;
            if (other == null)
                return false;

            // TODO Flo: String.IsNullOrWhitespace instead ?
            if (String.IsNullOrEmpty(QualifiedID) || String.IsNullOrEmpty(other.QualifiedID))
            {
                return Source.Equals(other.Source) && Target.Equals(other.Target);
            }

            return this.QualifiedID.Equals(other.QualifiedID);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        #endregion // Object-Overridden Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="nodes"></param>
        private void Parse(XmlReader reader)
        {
            Debug.Assert(reader.NodeType == XmlNodeType.None);
            reader.Read(); // Advance to the data.

            if (reader.IsEmptyElement)
            {
                ParseEdgeAttributes(reader);            
            }
            else
            {
                ParseEdgeAttributes(reader);
                reader.ReadStartElement();

                while (reader.MoveToContent() == XmlNodeType.Element)
                {
                    if (String.Equals(reader.Name, Util.Xml.ELEMENT_EDGE_DATA))
                    {
                        Data data = new Data(this.Owner, reader);
                        this.m_Data.Add(data);
                    }
                    else
                    {
                        Util.Xml.ParseSkip(reader);
                    }
                }
                Debug.Assert(XmlNodeType.EndElement == reader.NodeType);
                reader.ReadEndElement();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="nodes"></param>
        private void ParseEdgeAttributes(XmlReader reader)
        {
            IEnumerable<Node> nodes = this.Owner.Nodes();
            Debug.Assert(nodes.Any());
            
            SetID(reader.GetAttribute("id"));
            String sourceID = reader.GetAttribute("source");
            String targetID = reader.GetAttribute("target");
            Debug.Assert(this.Owner.HasAnyNode(sourceID),
                String.Format("Edge '{0}' source node '{1}' not found.", this.QualifiedID, sourceID));
            Debug.Assert(this.Owner.HasAnyNode(targetID),
                String.Format("Edge '{0}' target node '{1}' not found.", this.QualifiedID, targetID));

            if (this.Owner.HasAnyNode(sourceID))
            {
                this.Source = this.Owner.GetNode(sourceID);
            }
            else
            {
                Graph.Log.Error(reader, "Edge '{0}' source node '{1}' not found.",
                    this.ID, sourceID);
            }
            if (this.Owner.HasAnyNode(targetID))
            {
                this.Target = this.Owner.GetNode(targetID);
            }
            else
            {
                Graph.Log.Error(reader, "Edge '{0}' target node '{1}' not found.",
                    this.ID, targetID);
            }
        }
        #endregion // Private Methods
    }

} // RSG.GraphML namespace
