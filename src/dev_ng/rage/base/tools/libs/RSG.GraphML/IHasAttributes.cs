﻿using System;
using System.Collections.Generic;

namespace RSG.GraphML
{
    
    /// <summary>
    /// 
    /// </summary>
    public interface IHasAttributes
    {
        /// <summary>
        /// Dictionary of attributes.
        /// </summary>
        IDictionary<String, Attribute> Attributes { get; }
    }

} // RSG.GraphML namespace
