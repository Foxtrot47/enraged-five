﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace RSG.GraphML
{

    /// <summary>
    /// Abstract base class for all graph elements.
    /// </summary>
    public abstract class Element :
        IElement,
        IGraphOwner,
        IHasData
    {
        #region Properties
        /// <summary>
        /// Element unique identifier.
        /// </summary>
        public String ID
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public String QualifiedID
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public Graph Owner
        {
            get;
            protected set;
        }

        /// <summary>
        /// 
        /// </summary>
        public Data[] Data
        {
            get { return (m_Data.ToArray()); }
        }
        protected List<Data> m_Data;
        
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<String, Port> Ports
        {
            get;
            protected set;
        }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Cached hash code; set when QualifiedID is set.
        /// </summary>
        private int _hashCode;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor; subclass <b>must</b> set ID property.
        /// </summary>
        /// <param name="owner"></param>
        internal Element(Graph owner)
        {
            Reset();
            this.Owner = owner;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="id"></param>
        public Element(Graph owner, String id)
        {
            Reset();
            this.Owner = owner;
            SetID(id);
        }
        #endregion // Constructor(s)

        #region Object-Overridden Methods
        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        public override bool Equals(Object obj)
        {
            if (!(obj is Element))
                return (false);

            Element other = (Element)obj;
            return (this._hashCode == other._hashCode);
        }

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            Debug.Assert(!String.IsNullOrEmpty(this.QualifiedID));
            return (this._hashCode);
        }
        #endregion // Object-Overridden Methods

        #region Protected Methods
        /// <summary>
        /// Reset internal data structures.
        /// </summary>
        protected virtual void Reset()
        {
            this._hashCode = 0;
            this.m_Data = new List<Data>();
            this.Ports = new Dictionary<String, Port>();
        }

        /// <summary>
        /// Set ID and Qualified ID.
        /// </summary>
        protected virtual void SetID(String id)
        {
            if (String.IsNullOrEmpty(id))
                return;

            String[] parts = Util.GraphML.SplitId(id);
            Debug.Assert(parts.Length > 0, "Invalid ID.");
            this.ID = parts[parts.Length-1];
            this.QualifiedID = Util.GraphML.TrimId(id);
            this._hashCode = this.QualifiedID.GetHashCode();
        }
        #endregion // Protected Methods
    }

} // RSG.GraphML
