﻿using System;

namespace RSG.GraphML
{

    /// <summary>
    /// Interface that specifies that the owner is a Graph object.
    /// </summary>
    public interface IGraphOwner
    {
        /// <summary>
        /// Owner Graph object.
        /// </summary>
        Graph Owner { get; }
    }

} // RSG.GraphML namespace
