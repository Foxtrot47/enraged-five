﻿using System;
using System.Diagnostics;
using System.Xml;
using System.Xml.Linq;
using RSG.Base.Logging;

namespace RSG.GraphML
{

    /// <summary>
    /// 
    /// </summary>
    public class Node :
        Element,
        IElement,
        IHasData,
        ISerialisable
    {
        #region Properties
        /// <summary>
        /// Node nested Graph object (or null).
        /// </summary>
        public Graph NestedGraph
        {
            get;
            protected set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="id"></param>
        public Node(Graph owner, String id)
            : base(owner, id)
        {
        }

        /// <summary>
        /// Constructor from an XmlReader.
        /// </summary>
        /// <param name="reader"></param>
        public Node(Graph owner, XmlReader reader)
            : base(owner)
        {
            Util.Xml.ValidateXmlParse(ParseNode, reader);
        }
        #endregion // Constructor(s)

        #region Controller Methods
        #region ISerialisable Interface Methods
        /// <summary>
        /// Serialise object using System.Xml.Linq.
        /// </summary>
        /// <param name="writer"></param>
        public XElement ToXElement()
        {
            throw new NotImplementedException();
        }
        #endregion // ISerialisable Interface Methods
        #endregion // Controller Methods

        #region Object-Overridden Methods
        /// <summary>
        /// Return String representation of Node.
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return (this.QualifiedID);
        }
        #endregion // Object-Overridden Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        private void ParseNode(XmlReader reader)
        {
            Debug.Assert(reader.NodeType == XmlNodeType.None);
            reader.Read(); // Advance to the data.

            if (reader.IsEmptyElement)
            {
                ParseNodeAttributes(reader);
            }
            else
            {
                ParseNodeAttributes(reader);
                reader.ReadStartElement();
                while (XmlNodeType.Element == reader.MoveToContent())
                {
                    if (String.Equals(reader.Name, Util.Xml.ELEMENT_NODE_PORT))
                    {
                        Port port = new Port(this.Owner, reader);
                        Debug.Assert(!this.Ports.ContainsKey(port.Name),
                            String.Format("Port '{0}' already added to Node '{1}'.  Ignoring.",
                            port.Name, this.ID));
                        if (!this.Ports.ContainsKey(port.Name))
                            this.Ports.Add(port.Name, port);
                        else
                            Graph.Log.Warning("Port '{0}' already added to Node '{1}'.  Ignoring.",
                                port.Name, this.ID);
                    }
                    else if (String.Equals(reader.Name, Util.Xml.ELEMENT_NODE_DATA))
                    {
                        Data data = new Data(this.Owner, reader);
                        this.m_Data.Add(data);
                    }
                    else if (String.Equals(reader.Name, Util.Xml.ELEMENT_GRAPH))
                    {
                        // Nested graph support.
                        GraphCollection container = this.Owner.Container;
                        Graph graph = new Graph(container, this, reader);
                        this.NestedGraph = graph;
                    }
                    else
                    {
                        Util.Xml.ParseSkip(reader);
                    }
                    if (XmlNodeType.EndElement == reader.NodeType)
                        reader.ReadEndElement();
                }
                Debug.Assert(XmlNodeType.EndElement == reader.NodeType);
                reader.ReadEndElement();
            }
        }

        /// <summary>
        /// Parse node XML attributes.
        /// </summary>
        /// <param name="reader"></param>
        private void ParseNodeAttributes(XmlReader reader)
        {
            SetID(reader.GetAttribute("id"));
        }
        #endregion // Private Methods
    }

} // RSG.GraphML
