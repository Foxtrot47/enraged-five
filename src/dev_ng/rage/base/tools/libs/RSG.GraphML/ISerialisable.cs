﻿using System;
using System.Xml;
using System.Xml.Linq;

namespace RSG.GraphML
{

    /// <summary>
    /// Interface supporting serialisation to XML; through XmlWriter.
    /// </summary>
    public interface ISerialisable
    {
        /// <summary>
        /// Serialise object using System.Xml.Linq.
        /// </summary>
        /// <returns></returns>
        XElement ToXElement();
    }

} // RSG.GraphML namespace
