﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Xml;
using System.Xml.Linq;
using RSG.Base.Logging;

namespace RSG.GraphML
{

    /// <summary>
    /// The Data class represents a concrete instance of an Attribute; attached
    /// to a Node or Edge element.
    /// </summary>
    public class Data :
        IKeyed,
        IGraphOwner,
        ISerialisable
    {
        #region Enumerations
        /// <summary>
        /// 
        /// </summary>
        public enum ForType
        {
            Node,
            Edge,
        }
        #endregion // Enumerations

        #region Properties
        /// <summary>
        /// Owner Graph object.
        /// </summary>
        public Graph Owner
        {
            get;
            protected set;
        }

        /// <summary>
        /// Attribute key.
        /// </summary>
        public String Key
        {
            get;
            private set;
        }

        /// <summary>
        /// Associated attribute definition.
        /// </summary>
        public Attribute Attribute
        {
            get;
            internal set;
        }

        /// <summary>
        /// 
        /// </summary>
        public Object Value
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="attr"></param>
        public Data(Graph owner, Attribute attr)
        {
            this.Owner = owner;
            this.Attribute = attr;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public Data(Graph owner, Attribute attr, Object value)
        {
            this.Owner = owner;
            this.Attribute = attr;
            this.Value = value;
        }

        /// <summary>
        /// Constructor from Xml data.
        /// </summary>
        /// <param name="reader"></param>
        public Data(Graph owner, XmlReader reader)
        {
            this.Owner = owner;
            Util.Xml.ValidateXmlParse(ParseData, reader);
        }
        #endregion // Constructor(s)

        #region Controller Methods
        #region ISerialisable Interface Methods
        /// <summary>
        /// Serialise object using System.Xml.Linq.
        /// </summary>
        /// <param name="writer"></param>
        public XElement ToXElement()
        {
            throw new NotImplementedException();
        }
        #endregion // ISerialisable Interface Methods
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Parse Data node.
        /// </summary>
        /// <param name="reader"></param>
        private void ParseData(XmlReader reader)
        {
            Debug.Assert(XmlNodeType.None == reader.NodeType);
            reader.Read(); // Advance to the data.

            if (reader.IsEmptyElement)
            {
                ParseDataAttributes(reader);
            }
            else
            {
                ParseDataAttributes(reader);
                reader.ReadStartElement();
                ParseChildNodes(reader);

                Debug.Assert(XmlNodeType.EndElement == reader.NodeType);
                reader.ReadEndElement();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        private void ParseDataAttributes(XmlReader reader)
        {
            String key = reader.GetAttribute(Util.Xml.ATTR_DATA_KEY);
            Debug.Assert(!String.IsNullOrEmpty(key), "Attribute key is empty.");
            if (String.IsNullOrEmpty(key))
            {
                Graph.Log.Error(reader, "Attribute key is empty.");
                return;
            }
            this.Key = key;

            Debug.Assert(this.Owner.Container.Attributes.ContainsKey(key));
            if (!this.Owner.Container.Attributes.ContainsKey(key))
            {
                Graph.Log.Error(reader, "Failed to find Attribute with key: {0}.",
                    key);
                return;
            }

            this.Attribute = this.Owner.Container.Attributes[key];
        }


        /// <summary>
        /// Parse GraphML attribute child nodes (e.g. <default />).
        /// </summary>
        /// <param name="reader"></param>
        private void ParseChildNodes(XmlReader reader)
        {
            Debug.Assert(null != this.Attribute);
            if (null == this.Attribute)
            {
                Graph.Log.Error(reader, "No attribute found '{0}'.", this.Key);
                reader.Skip();
                return;
            }

            while (XmlNodeType.Text == reader.MoveToContent())
            {
                Debug.Assert(null != this.Attribute);

                String value = reader.ReadString();
                if (typeof(bool) == this.Attribute.ValueType)
                {
                    bool bv = default(bool);
                    if (!bool.TryParse(value, out bv))
                    {
                        this.Value = this.Attribute.DefaultValue;
                        Graph.Log.Warning(reader, "Failed to parse boolean value.");
                    }
                    else
                    {
                        this.Value = bv;
                    }
                }
                else if (typeof(int) == this.Attribute.ValueType)
                {
                    int iv = default(int);
                    if (!int.TryParse(value, out iv))
                    {
                        this.Value = this.Attribute.DefaultValue;
                        Graph.Log.Warning(reader, "Failed to parse integer value.");
                    }
                    else
                    {
                        this.Value = iv;
                    }
                }
                else if (typeof(long) == this.Attribute.ValueType)
                {
                    long iv = default(long);
                    if (!long.TryParse(value, out iv))
                    {
                        this.Value = this.Attribute.DefaultValue;
                        Graph.Log.Warning(reader, "Failed to parse long value.");
                    }
                    else
                    {
                        this.Value = iv;
                    }
                }
                else if (typeof(float) == this.Attribute.ValueType)
                {
                    float iv = default(float);
                    if (!float.TryParse(value, out iv))
                    {
                        this.Value = this.Attribute.DefaultValue;
                        Graph.Log.Warning(reader, "Failed to parse float value.");
                    }
                    else
                    {
                        this.Value = iv;
                    }
                }
                else if (typeof(double) == this.Attribute.ValueType)
                {
                    double iv = default(double);
                    if (!double.TryParse(value, out iv))
                    {
                        this.Value = this.Attribute.DefaultValue;
                        Graph.Log.Warning(reader, "Failed to parse double value.");
                    }
                    else
                    {
                        this.Value = iv;
                    }
                }
                else if (typeof(String) == this.Attribute.ValueType)
                {
                    this.Value = value;
                }
                else
                {
                    Debug.Assert(false, String.Format("Unsupported attribute value type: '{0}'.",
                        this.Attribute.ValueType));
                    Graph.Log.Warning(reader, "Unsupported attribute value type: '{0}'.  Ignoring.",
                        this.Attribute.ValueType);
                }
            }
        }
        #endregion // Private Methods
    }

} // RSG.GraphML namespace
