﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.GraphML
{

    /// <summary>
    /// GraphML attribute target type.
    /// </summary>
    public enum AttributeTarget
    {
        /// <summary>
        /// Attribute is for Graph elements.
        /// </summary>
        Graph,

        /// <summary>
        /// Attribute is for Node elements.
        /// </summary>
        Node,

        /// <summary>
        /// Attribute is for Edge elements.
        /// </summary>
        Edge, 
    }

} // RSG.GraphML namespace
