﻿using System;

namespace RSG.GraphML
{

    /// <summary>
    /// 
    /// </summary>
    public interface IConnection
    {
        Node Source { get; }
        Node Target { get; }

        Port SourcePort { get; }
        Port TargetPort { get; }
    }

} // RSG.GraphML namespace
