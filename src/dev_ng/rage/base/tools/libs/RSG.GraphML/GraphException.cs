﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.GraphML
{

    /// <summary>
    /// GraphML exception.
    /// </summary>
    public class GraphException : Exception
    {
        #region Constructor(s)
        public GraphException()
            : base()
        {
        }

        public GraphException(String message)
            : base(message)
        {
        }

        public GraphException(String message, Exception inner)
            : base(message, inner)
        {
        }

        public GraphException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context)
            : base(info, context)
        {
        }
        #endregion // Constructor(s)
    }

    /// <summary>
    /// GraphML load exception.
    /// </summary>
    public class GraphLoadException : GraphException
    {
        #region Constructor(s)
        public GraphLoadException()
            : base()
        {
        }

        public GraphLoadException(String message)
            : base(message)
        {
        }

        public GraphLoadException(String message, Exception inner)
            : base(message, inner)
        {
        }

        public GraphLoadException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context)
            : base(info, context)
        {
        }
        #endregion // Constructor(s)
    }

} // RSG.GraphML namespace
