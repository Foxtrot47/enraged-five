﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.GraphML
{

    /// <summary>
    /// 
    /// </summary>
    public interface IHasPorts
    {
        /// <summary>
        /// 
        /// </summary>
        IEnumerable<Port> Ports { get; }
    }

} // RSG.GraphML namespace
