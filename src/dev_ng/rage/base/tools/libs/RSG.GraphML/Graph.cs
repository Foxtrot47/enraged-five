﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using RSG.Base.Logging;

namespace RSG.GraphML
{

    /// <summary>
    /// GraphML graph.
    /// </summary>
    public class Graph :
        IGraph,
        IElement,
        INodeOwner,
        IHasData,
        ISerialisable
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public String ID
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public String QualifiedID
        {
            get;
            private set;
        }

        /// <summary>
        /// Edge type for this graph (directed, undirected).
        /// </summary>
        /// This is critical to how the graph can be interpreted.
        public EdgeDefaultType EdgeType
        {
            get;
            private set;
        }

        /// <summary>
        /// Container collection (for Attribute access).
        /// </summary>
        public GraphCollection Container
        {
            get;
            protected set;
        }

        /// <summary>
        /// Owner Node (for nested graphs).
        /// </summary>
        public Node Owner
        {
            get;
            protected set;
        }

        /// <summary>
        /// Set for nested graphs (has Node owner).
        /// </summary>
        public bool IsNested
        {
            get { return (null != this.Owner); }
        }

        /// <summary>
        /// 
        /// </summary>
        public Data[] Data
        {
            get { return (m_Data.ToArray()); }
        }
        protected List<Data> m_Data;
        #endregion // Properties

        #region Static Properties
        /// <summary>
        /// GraphML loading/saving log.
        /// </summary>
        internal static RSG.Base.Logging.Universal.IUniversalLog Log
        {
            get;
            private set;
        }
        #endregion // Static Properties

        #region Member Data
        private Dictionary<String, Node> m_nodes;
        private List<Edge> m_edges; // Edges may not have an ID.
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor for an in-memory Graph.
        /// </summary>
        /// <param name="container"></param>
        /// <param name="id"></param>
        /// <param name="edgeDefault"></param>
        public Graph(GraphCollection container, String id, EdgeDefaultType edgeDefault)
            : this(container, null, id, edgeDefault)
        {
        }

        /// <summary>
        /// Constructor for an in-memory Graph with Owner.
        /// </summary>
        /// <param name="container"></param>
        /// <param name="owner"></param>
        /// <param name="id"></param>
        /// <param name="edgeDefault"></param>
        public Graph(GraphCollection container, Node owner, String id, EdgeDefaultType edgeDefault)
        {
            Reset();
            String[] parts = Util.GraphML.SplitId(id);
            Debug.Assert(parts.Length > 0, "Invalid ID.");
            this.ID = parts[0];
            this.Container = container;
            this.QualifiedID = Util.GraphML.TrimId(id);
            this.Owner = owner;
            this.EdgeType = edgeDefault;
        }

        /// <summary>
        /// Constructor from an XmlReader.
        /// </summary>
        /// <param name="container"></param>
        /// <param name="reader"></param>
        public Graph(GraphCollection container, XmlReader reader)
            : this(container, null, reader)
        {
        }

        /// <summary>
        /// Constructor from an XmlReader with an Owner.
        /// </summary>
        /// <param name="container"></param>
        /// <param name="owner"></param>
        /// <param name="reader"></param>
        public Graph(GraphCollection container, Node owner, XmlReader reader)
        {
            Reset();
            this.Container = container;
            this.Owner = owner;
            Util.Xml.ValidateXmlParse(ParseGraph, reader);
            Util.GraphML.ValidateNestedGraph(this);
        }

        /// <summary>
        /// Static constructor; initialises log system.
        /// </summary>
        static Graph()
        {
            LogFactory.Initialize();
            Graph.Log = LogFactory.CreateUniversalLog("GraphML");
        }
        #endregion // Constructor(s)

        #region Controller Methods
        #region Graph Node Methods
        /// <summary>
        /// Return Array of all the Graph's immediate Nodes.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Node> Nodes()
        {
            return m_nodes.Values.ToList();
        }

        /// <summary>
        /// Return Array of all the Graph's Nodes (recursive).
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Node> AllNodes()
        {
            List<Node> nodes = new List<Node>(m_nodes.Values);

            foreach (Node n in m_nodes.Values)
            {
                if (null != n.NestedGraph)
                    nodes.AddRange(n.NestedGraph.AllNodes());
            }
            return nodes;
        }

        /// <summary>
        /// Determine whether the Graph has an immediate Node with a particular ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool HasNode(String id)
        {
            foreach (Node n in m_nodes.Values)
            {
                if (n.QualifiedID == id)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Determine whether the Graph has any Node with a particular ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool HasAnyNode(String id)
        {
            foreach (Node n in m_nodes.Values)
            {
                if (n.QualifiedID == id)
                    return true;

                if (n.NestedGraph != null)
                {
                    if (n.NestedGraph.HasAnyNode(id))
                        return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Return Node with particular ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Node GetNode(String id)
        {
            return m_nodes.Values.FirstOrDefault(n => n.QualifiedID == id);
        }
        #endregion // Graph Node Methods

        #region Graph Edge Methods 
        /// <summary>
        /// Return an enumerable of the Graph's immediate Edges.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Edge> Edges()
        {
            return m_edges.ToList();
        }

        /// <summary>
        /// Return an enumerable of all Graph's Edges (recursive).
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Edge> AllEdges()
        {
            List<Edge> edges = new List<Edge>(m_edges);
            IEnumerable<Node> nodes = AllNodes();

            foreach (Node n in nodes)
            {
                if (null != n.NestedGraph)
                    edges.AddRange(n.NestedGraph.AllEdges());
            }
            return edges;
        }

        /// <summary>
        /// Return whether an edge with specific ID is present.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool HasEdge(String id)
        {
            for (int i = 0; i < m_edges.Count; i++)
            {
                Edge e = m_edges[i];
                if (e.ID == id)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Return whether an edge connecting source/target is present.
        /// </summary>
        /// <param name="sourceId"></param>
        /// <param name="targetId"></param>
        /// <returns></returns>
        public bool HasEdge(String sourceId, String targetId)
        {
            for (int i = 0; i < m_edges.Count; i++)
            {
                Edge e = m_edges[i];
                if (e.Source.QualifiedID == sourceId && e.Target.QualifiedID == targetId)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Return whether an edge connecting source/target is present.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public bool HasEdge(Node source, Node target)
        {
            for (int i = 0; i < m_edges.Count; i++)
            {
                Edge e = m_edges[i];
                if (e.Source == source && e.Target == target)
                    return true;
            }
            return false;
        }


        /// <summary>
        /// Return whether an edge with specific ID is present.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool HasAnyEdge(String id)
        {
            IEnumerable<Edge> edges = AllEdges();

            foreach (Edge e in edges)
            {
                if (e.QualifiedID == id)
                    return true;
            }

            return false;
        }

        public bool HasAnyEdge(String sourceId, String targetId)
        {
            IEnumerable<Edge> edges = AllEdges();

            foreach (Edge e in edges)
            {
                if (e.Source.QualifiedID == sourceId && e.Target.QualifiedID == targetId)
                    return true;
            }

            return false;
        }

        public bool HasAnyEdge(Node source, Node target)
        {
            IEnumerable<Edge> edges = AllEdges();

            foreach (Edge e in edges)
            {
                if (e.Source == source && e.Target == target)
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Return Edge with particular ID (null if not found).
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Edge GetEdge(String id)
        {
            return m_edges.FirstOrDefault(e => e.ID == id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public Edge GetEdge(String source, String target)
        {
            return m_edges.FirstOrDefault(e => e.Source.QualifiedID == source && e.Target.QualifiedID == target);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public Edge GetEdge(Node source, Node target)
        {
            return m_edges.FirstOrDefault(e => e.Source == source && e.Target == target);
        }


        /// <summary>
        /// Return Edge with particular ID (null if not found).
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Edge GetAnyEdge(String id)
        {
            return AllEdges().FirstOrDefault(e => e.ID == id);
        }

        public Edge GetAnyEdge(String sourceId, String targetId)
        {
            return AllEdges().FirstOrDefault(e => e.Source.QualifiedID == sourceId && e.Target.QualifiedID == targetId);
        }

        public Edge GetAnyEdge(Node source, Node target)
        {
            return AllEdges().FirstOrDefault(e => e.Source == source && e.Target == target);
        }
        #endregion // Graph Edge Methods

        #region ISerialisable Interface Methods
        /// <summary>
        /// Serialise object using System.Xml.Linq.
        /// </summary>
        /// <param name="writer"></param>
        public XElement ToXElement()
        {
            throw new NotImplementedException();
        }
        #endregion // ISerialisable Interface Methods
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Reset this Graph object.
        /// </summary>
        protected void Reset()
        {
            this.m_nodes = new Dictionary<String, Node>();
            this.m_edges = new List<Edge>();
            this.m_Data = new List<Data>();
        }

        #region XmlReader Deserialisation
        /// <summary>
        /// Parse graph node.
        /// </summary>
        /// <param name="reader"></param>
        private void ParseGraph(XmlReader reader)
        {
            Debug.Assert(reader.NodeType == XmlNodeType.None);
            reader.Read(); // Advance to the data.

            string readerName = reader.Name;

            Debug.Assert(String.Equals(Util.Xml.ELEMENT_GRAPH, readerName));
            if (!String.Equals(Util.Xml.ELEMENT_GRAPH, readerName))
            {
                Graph.Log.Error(reader, "Unexpected XML node <{0}>.", readerName);
                return;
            }

            if (reader.IsEmptyElement)
            {
                this.ParseGraphAttributes(reader);
            }
            else
            {
                this.ParseGraphAttributes(reader);
                reader.ReadStartElement();

                while (XmlNodeType.Element == reader.MoveToContent())
                {
                    readerName = reader.Name;

                    if (String.Equals(readerName, Util.Xml.ELEMENT_GRAPH_NODE))
                    {
                        Node node = new Node(this, reader);
                        Debug.Assert(!this.m_nodes.ContainsKey(node.QualifiedID),
                            String.Format("Node '{0}' already added to node collection.  Ignoring.", node.ID));
                        if (!this.m_nodes.ContainsKey(node.QualifiedID))
                            this.m_nodes.Add(node.QualifiedID, node);
                        else
                            Graph.Log.Warning(reader, "Node '{0}' already added to node collection.  Ignoring.", node.ID);
                    }
                    else if (String.Equals(readerName, Util.Xml.ELEMENT_GRAPH_EDGE))
                    {
                        Edge edge = new Edge(this, reader);
                        this.m_edges.Add(edge);
                    }
                    else if (String.Equals(readerName, Util.Xml.ELEMENT_GRAPH_HYPEREDGE))
                    {
                        Graph.Log.Warning(reader, "Graph has hyperedges.  Hyperedges are not supported.");
                        Util.Xml.ParseSkip(reader);
                    }
                    else
                    {
                        Util.Xml.ParseSkip(reader);
                    }
                }
                Debug.Assert(XmlNodeType.EndElement == reader.NodeType);
                reader.ReadEndElement();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        private void ParseGraphAttributes(XmlReader reader)
        {
            String id = reader.GetAttribute(Util.Xml.ATTR_ELEMENT_ID);
            String[] parts = Util.GraphML.SplitId(id);
            Debug.Assert(parts.Length > 0, "Invalid ID.");
            this.ID = parts[0];
            this.QualifiedID = Util.GraphML.TrimId(id);

            String edgeDirection = reader.GetAttribute(Util.Xml.ATTR_GRAPH_EDGEDEFAULT);
            if (String.Equals(edgeDirection, Util.Xml.ATTR_GRAPH_VALUE_UNDIRECTED))
                this.EdgeType = EdgeDefaultType.Undirected;
            else if (String.Equals(edgeDirection, Util.Xml.ATTR_GRAPH_VALUE_DIRECTED))
                this.EdgeType = EdgeDefaultType.Directed;
        }
        #endregion // XmlReader Deserialisation
        #endregion // Private Methods
    }

} // RSG.GraphML namespace
