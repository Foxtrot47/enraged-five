﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.GraphML
{

    /// <summary>
    /// Interface that specifies that the owner is a Node object.
    /// </summary>
    public interface INodeOwner
    {
        /// <summary>
        /// Owner Node object.
        /// </summary>
        Node Owner { get; }
    }

} // RSG.GraphML namespace
