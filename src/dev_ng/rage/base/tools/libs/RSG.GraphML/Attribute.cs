﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Xml;
using System.Xml.Linq;
using RSG.Base.Logging;

namespace RSG.GraphML
{

    /// <summary>
    /// GraphML Attribute class.
    /// </summary>
    /// This class supports GraphML attributes; attached to node and edges.
    /// 
    /// Currently only simple types are supported by this serialiser.
    /// 
    public class Attribute :
        Element,
        IElement,
        INamed,
        IGraphCollectionContainer,
        ISerialisable
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public String Name
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public GraphCollection Container
        {
            get;
            private set;
        }

        /// <summary>
        /// Type of value.
        /// </summary>
        public Type ValueType
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public AttributeTarget For
        {
            get;
            private set;
        }

        /// <summary>
        /// Data value.
        /// </summary>
        public Object DefaultValue
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="container"></param>
        /// <param name="id"></param>
        /// <param name="type"></param>
        public Attribute(GraphCollection container, String id, Type type)
            : base(null, id)
        {
            this.Container = container;
            this.ValueType = type;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="container"></param>
        /// <param name="id"></param>
        /// <param name="type"></param>
        /// <param name="def"></param>
        public Attribute(GraphCollection container, String id, Type type, Object def)
            : base(null, id)
        {
            this.Container = container;
            this.ValueType = type;
            this.DefaultValue = def;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="container"></param>
        /// <param name="reader"></param>
        public Attribute(GraphCollection container, XmlReader reader)
            : base(null)
        {
            this.Container = container;
            Util.Xml.ValidateXmlParse(ParseAttribute, reader);
        }
        #endregion // Constructor(s)

        #region ISerialisable Interface Methods
        /// <summary>
        /// Serialise object using System.Xml.Linq.
        /// </summary>
        /// <param name="writer"></param>
        public XElement ToXElement()
        {
            throw new NotImplementedException();
        }
        #endregion // ISerialisable Interface Methods

        #region Private Methods
        #region XmlReader Deserialisation
        /// <summary>
        /// Parse Attribute node.
        /// </summary>
        /// <param name="reader"></param>
        private void ParseAttribute(XmlReader reader)
        {
            Debug.Assert(reader.NodeType == XmlNodeType.None);
            reader.Read(); // Advance to the data.

            if (reader.IsEmptyElement)
            {
                ParseAttributes(reader);
            }
            else
            {
                ParseAttributes(reader);
                reader.ReadStartElement();
                while (XmlNodeType.Element == reader.MoveToContent())
                {
                    if (String.Equals(reader.Name, Util.Xml.ELEMENT_ATTRIBUTE_DEFAULT))
                    {
                        this.DefaultValue = reader.ReadElementContentAs(
                            this.ValueType, null);
                    }
                    else
                    {
                        Graph.Log.Warning(reader, "Unknown GraphML attribute child node.");
                        Util.Xml.ParseSkip(reader);
                    }
                }
                Debug.Assert(XmlNodeType.EndElement == reader.NodeType);
                reader.ReadEndElement();
            }
        }

        /// <summary>
        /// Parse GraphML Attribute node attributes.
        /// </summary>
        /// <param name="reader"></param>
        private void ParseAttributes(XmlReader reader)
        {
            SetID(reader.GetAttribute(Util.Xml.ATTR_ELEMENT_ID));
            this.Name = reader.GetAttribute(Util.Xml.ATTR_ATTRIBUTE_NAME);

            String forType = reader.GetAttribute(Util.Xml.ATTR_ATTRIBUTE_FOR);
            if (String.Equals(forType, Util.Xml.ATTR_ATTRIBUTE_VALUE_GRAPH))
                this.For = AttributeTarget.Graph;
            else if (String.Equals(forType, Util.Xml.ATTR_ATTRIBUTE_VALUE_NODE))
                this.For = AttributeTarget.Node;
            else if (String.Equals(forType, Util.Xml.ATTR_ATTRIBUTE_VALUE_EDGE))
                this.For = AttributeTarget.Edge;

            String valueType = reader.GetAttribute(Util.Xml.ATTR_ATTRIBUTE_TYPE);
            if (String.Equals(valueType, Util.Xml.ATTR_ATTRIBUTE_VALUE_BOOL))
            {
                this.ValueType = typeof(bool);
                this.DefaultValue = default(bool);
            }
            else if (String.Equals(valueType, Util.Xml.ATTR_ATTRIBUTE_VALUE_INT))
            {
                this.ValueType = typeof(int);
                this.DefaultValue = default(int);
            }
            else if (String.Equals(valueType, Util.Xml.ATTR_ATTRIBUTE_VALUE_LONG))
            {
                this.ValueType = typeof(long);
                this.DefaultValue = default(long);
            }
            else if (String.Equals(valueType, Util.Xml.ATTR_ATTRIBUTE_VALUE_FLOAT))
            {
                this.ValueType = typeof(float);
                this.DefaultValue = default(float);
            }
            else if (String.Equals(valueType, Util.Xml.ATTR_ATTRIBUTE_VALUE_DOUBLE))
            {
                this.ValueType = typeof(double);
                this.DefaultValue = default(double);
            }
            else if (String.Equals(valueType, Util.Xml.ATTR_ATTRIBUTE_VALUE_STRING))
            {
                this.ValueType = typeof(String);
                this.DefaultValue = String.Empty;
            }
            else
            {
                Debug.Assert(false, String.Format("Unsupported attribute value type: '{0}'.",
                    valueType));
                Graph.Log.Warning(reader, "Unsupported attribute value type: '{0}'.  Ignoring.",
                    valueType);
            }
        }
        #endregion // XmlTextReader Deserialisation
        #endregion // Private Methods
    }

} // RSG.GraphML namespace
