using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

/******************************************************************************
* DO NOT EDIT THIS FILE.  IT IS AUTO-GENERATED.
* 
* To use in your project:
*     1) Add this file to your project.  If you are using a common file, such as
*        rage/build/CommonAssemblyInfo.cs, then add the file as a Link.  This
*        option can be found on the Add Existing Item dialog in the Add button's
*        drop down menu.
*     2) Edit your project's AssemblyInfo.cs file, commenting out the Assembly*
*        items you see in this file.
******************************************************************************/

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyCompany( "Rockstar Games" )]
[assembly: AssemblyCopyright( "Copyright � Rockstar Games 2014" )]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion( "1.18.29" )]

