﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    private List<BuildItem> m_BuildItems;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Page.Title = "Automated Build Server";
        m_BuildItems = BuildInfoService.GetBuildData();
        PopulateUI();
    }

    private bool PopulateUI()
    {
        //Populate the tree view navigation control.
        NavPanel.PopulateUI();

        if (m_BuildItems != null)
        {
            foreach (BuildItem buildItem in m_BuildItems)
            {
                object[] parameters = new object[1];
                parameters[0] = buildItem;
                Controls_BuildStatus status = (Controls_BuildStatus)LoadControl("~/Controls/BuildStatus.ascx");
                if (status.Populate(buildItem) == true)
                {
                    StatusList.Controls.Add(status);
                }
            }
        }

        return true;
    }
}
