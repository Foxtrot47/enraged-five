﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Controls_NavigationPanel : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public bool PopulateUI()
    {
        //Populate the tree view navigation control.
        BuildTreeView.Nodes.Clear();
        
        List<BuildItem> m_BuildItems = BuildInfoService.GetBuildData();

        if (m_BuildItems != null)
        {
            foreach (BuildItem buildItem in m_BuildItems)
            {
                TreeNode buildItemTreeNode = new TreeNode(buildItem.Name);
                buildItemTreeNode.NavigateUrl = "../BuildHistory.aspx?" + RequestParameters.BuildNameArgument + "=" + buildItem.BuildName;
                BuildTreeView.Nodes.Add(buildItemTreeNode);
            }
        }

        return true;
    }
}