﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UsageStatistics.ascx.cs" Inherits="Controls_CutscenePackageHistory" %>
<%@ Register Namespace="CustomControls" TagPrefix="custom" %>


<table align="center" cellpadding="0" rules="none" width="100%" height="100%" frame="void">
    <tr>
        <asp:GridView ID="UserStatisticsView" runat="server" AutoGenerateColumns="False" 
            DataSourceID="UsageStatisticsDataSource" CellPadding="8" CssClass="leftColText" 
            Font-Size="Medium" ForeColor="White" GridLines="Vertical" 
            onrowdatabound="OnUsageStatisticsRowDataBound">
            <AlternatingRowStyle BackColor="#3A4F63" />
            <Columns>
                <asp:BoundField DataField="DateTime" HeaderText="DateTime" 
                    SortExpression="DateTime" />
                <asp:BoundField DataField="ProjectName" HeaderText="ProjectName" 
                    SortExpression="ProjectName" />
                <asp:BoundField DataField="Name" HeaderText="Name" 
                    SortExpression="Name" />
                <asp:BoundField DataField="MachineName" HeaderText="MachineName" 
                    SortExpression="MachineName" />
                <asp:BoundField DataField="Note" HeaderText="Note" 
                    SortExpression="Note" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="UsageStatisticsDataSource" runat="server" 
            ConnectionString="<%$ ConnectionStrings:RockstarToolsConnectionString %>" 
            
            
            
            
            
            SelectCommand="SELECT ToolUsageStatistics2.DateTime, Projects.ProjectName, UsageStatisticsTypes.Name, ToolUsageStatistics2.Note, ToolUsageStatistics2.MachineName FROM ToolUsageStatistics2 INNER JOIN Projects ON ToolUsageStatistics2.ProjectID = Projects.ID INNER JOIN UsageStatisticsTypes ON ToolUsageStatistics2.TypeID = UsageStatisticsTypes.ID ORDER BY ToolUsageStatistics2.DateTime DESC">
            <SelectParameters>
                <asp:SessionParameter DefaultValue="50" Name="NumRecords" 
                    SessionField="NumRecords" />
            </SelectParameters>
        </asp:SqlDataSource>   
        <asp:GridView ID="ToolProfileStatistics" runat="server" AutoGenerateColumns="False" 
            DataSourceID="ProfileStatisticsDataSource" CellPadding="8" CssClass="leftColText" 
            Font-Size="Medium" ForeColor="White" GridLines="Vertical" 
            onrowdatabound="OnProfileStatisticsRowDataBound">
            <AlternatingRowStyle BackColor="#3A4F63" />
            <Columns>
                <asp:BoundField DataField="DateTime" HeaderText="DateTime" 
                    SortExpression="DateTime" />
                <asp:BoundField DataField="ProjectName" HeaderText="ProjectName" 
                    SortExpression="ProjectName" />
                <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                <asp:BoundField DataField="Scope" HeaderText="Scope" SortExpression="Scope" />
                <asp:BoundField DataField="MachineName" HeaderText="MachineName" 
                    SortExpression="MachineName" />
                <asp:BoundField DataField="FileName" HeaderText="FileName" 
                    SortExpression="FileName" />
                <asp:BoundField DataField="TimeElapsed" HeaderText="TimeElapsed" 
                    SortExpression="TimeElapsed" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="ProfileStatisticsDataSource" runat="server" 
            ConnectionString="<%$ ConnectionStrings:RockstarToolsConnectionString %>" 
            
            
            
            
            
            SelectCommand="SELECT StatisticsInfo.DateTime, Projects.ProjectName, UsageStatisticsTypes.Name, ToolProfileStatistics.Scope, StatisticsInfo.MachineName, StatisticsInfo.FileName, ToolProfileStatistics.TimeElapsed FROM StatisticsInfo INNER JOIN ToolProfileStatistics ON StatisticsInfo.ID = ToolProfileStatistics.StatisticID INNER JOIN Projects ON StatisticsInfo.ProjectID = Projects.ID INNER JOIN UsageStatisticsTypes ON StatisticsInfo.TypeID = UsageStatisticsTypes.ID ORDER BY StatisticsInfo.DateTime DESC">
            <SelectParameters>
                <asp:SessionParameter DefaultValue="50" Name="NumRecords" 
                    SessionField="NumRecords" />
            </SelectParameters>
        </asp:SqlDataSource>

        
        <asp:ScriptManager ID="ScriptManager1"
                                       runat="server" />
        <asp:UpdatePanel ID="UpdatePanel1"
                                    runat="server">
            <ContentTemplate>
                    <asp:TreeView ID="ProfileStatisticsTreeView" runat="server" 
                CssClass="leftColText" 
                onselectednodechanged="OnProfileStatisticsSelectedNodeChanged" 
                ontreenodepopulate="ProfileStatisticsTreeView_Populate">
                        <Nodes>
                            <asp:TreeNode Text="Profile Statistics" Value="Profile Statistics">
                            </asp:TreeNode>
                        </Nodes>
                </asp:TreeView>

                    <asp:SqlDataSource ID="ProfileStatisticsRootDataSource" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:RockstarToolsConnectionString %>" 
            
            
            
            
            
    
    
                SelectCommand="SELECT StatisticsInfo.DateTime, StatisticsInfo.MachineName, StatisticsInfo.FileName, StatisticsInfo.ID, Projects.ProjectName, UsageStatisticsTypes.Name FROM StatisticsInfo INNER JOIN Projects ON StatisticsInfo.ProjectID = Projects.ID INNER JOIN UsageStatisticsTypes ON StatisticsInfo.TypeID = UsageStatisticsTypes.ID ORDER BY StatisticsInfo.DateTime DESC">
                    </asp:SqlDataSource>

                        <asp:SqlDataSource ID="ProfileStatisticsLevel1DataSource" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:RockstarToolsConnectionString %>" 
            
            
            
            
            
    
    
                SelectCommand="SELECT Scope, TimeElapsed, StatisticID, ID FROM ToolProfileStatistics WHERE (StatisticID = 48)">
                    </asp:SqlDataSource>
            </ContentTemplate>
        </asp:UpdatePanel>
    </tr>

</table>


