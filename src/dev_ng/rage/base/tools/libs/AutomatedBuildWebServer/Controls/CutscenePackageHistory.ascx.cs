﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Controls_CutscenePackageHistory : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session[RequestParameters.NumRecordsArgument] = DefaultNumRecords;
    }

    const int VersionRowIndex = 0;
    const int ChangelistRowIndex = 1;
    const int ResultRowIndex = 2;
    const int PathRowIndex = 3;
    const int PublishedRowIndex = 4;

    private int DefaultNumRecords = 100;

    protected void OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex != -1)
        {
            System.Drawing.Color backgroundColor = e.Row.Cells[PublishedRowIndex].BackColor;
            System.Drawing.Color displayColor = System.Drawing.Color.Gray;
            string displayText = "In Progress";

            bool noResult = String.IsNullOrEmpty(e.Row.Cells[PathRowIndex].Text) || String.Compare(e.Row.Cells[PathRowIndex].Text, "&nbsp;") == 0;
            if (noResult == false)
            {
                CheckBox resultCheckBox = (CheckBox)(e.Row.Cells[ResultRowIndex]).Controls[0];
                if (resultCheckBox.Checked == true)
                {
                    displayColor = System.Drawing.Color.LightBlue;
                    displayText = "Succeeded";
                }
                else
                {
                    displayColor = System.Drawing.Color.LightSalmon;
                    displayText = "Failed";
                }


                string fullLinkPath = "<a href=\"" + e.Row.Cells[PathRowIndex].Text + "\">Copy</a>";
                e.Row.Cells[PathRowIndex].Text = fullLinkPath;
            }
            else
            {
                e.Row.Cells[PathRowIndex].Text = "In Progress";
            }

            e.Row.Cells[ResultRowIndex].Controls.Clear();

            e.Row.Cells[ResultRowIndex].ForeColor = displayColor;
            e.Row.Cells[ResultRowIndex].Text = displayText;

            string changelistLink = "<a href=\"http://payne:8080/@md=d&cd=//&c=4hF@/" + e.Row.Cells[ChangelistRowIndex].Text + "?ac=10\">" + e.Row.Cells[ChangelistRowIndex].Text + "</a>";
            e.Row.Cells[ChangelistRowIndex].Text = changelistLink;

            CheckBox publishedCheckBox = (CheckBox)(e.Row.Cells[PublishedRowIndex]).Controls[0];
            if (publishedCheckBox.Checked == true)
            {
                backgroundColor = System.Drawing.Color.Green;
            }

            e.Row.Cells[PublishedRowIndex].Visible = false;

            e.Row.BackColor = backgroundColor;
        }
    }

    protected void OnRequestDataLoad(object sender, EventArgs e)
    {
        CutscenePackageView.DataBind();

        OnPackageChangelistClick();
    }

    protected void OnPackageChangelistClick()
    {

    }
}