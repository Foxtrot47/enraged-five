﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CutscenePackageHistory.ascx.cs" Inherits="Controls_CutscenePackageHistory" %>
<%@ Register Namespace="CustomControls" TagPrefix="custom" %>


<asp:Label ID="LegendTextBox" runat="server" ReadOnly="True" Width="364px" CssClass="leftColText">Green Background - Published Cutscene RPF.  This has been submitted to Perforce.</asp:Label>


<table align="center" cellpadding="0" rules="none" width="100%" height="100%" frame="void">
    <tr>
        <asp:GridView ID="CutscenePackageView" runat="server" AutoGenerateColumns="False" 
            DataSourceID="CutscenePackages" CellPadding="8" CssClass="leftColText" 
            Font-Size="Medium" ForeColor="White" GridLines="Vertical" 
            onrowdatabound="OnRowDataBound" onload="OnRequestDataLoad" 
            DataKeyNames="ID">
            <AlternatingRowStyle BackColor="#3A4F63" />
            <Columns>
                <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" 
                    ReadOnly="True" SortExpression="ID" />
                <asp:BoundField DataField="Changelist" HeaderText="Changelist" 
                    SortExpression="Changelist" />
                <asp:CheckBoxField DataField="Result" HeaderText="Result" 
                    SortExpression="Result" />
                <asp:BoundField DataField="Path" HeaderText="Path" SortExpression="Path" />
                <asp:CheckBoxField DataField="Published" 
                    SortExpression="Published" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="CutscenePackages" runat="server" 
            ConnectionString="<%$ ConnectionStrings:RockstarToolsConnectionString %>" 
            
            SelectCommand="SELECT TOP (@NumRecords) ID, Changelist, Result, Path, Published FROM CutscenePackages ORDER BY ID DESC">
            <SelectParameters>
                <asp:SessionParameter DefaultValue="50" Name="NumRecords" 
                    SessionField="NumRecords" />
            </SelectParameters>
        </asp:SqlDataSource>
    </tr>

</table>

<table align="center" cellpadding="0" rules="none" width="100%" height="100%" frame="void">
    <tr>
        <asp:Label ID="CutsceneChangesLabel" runat="server" ReadOnly="True" Width="364px" CssClass="leftColText">Changelists Since the Last Published RPF.</asp:Label>
        <asp:GridView ID="CutsceneChanges" runat="server" 
            DataSourceID="CutsceneChangesDataSource" CssClass="leftColText" 
            Font-Size="Medium" ForeColor="White" GridLines="Vertical" 
        AutoGenerateColumns="False">
            <AlternatingRowStyle BackColor="#3A4F63" />
            <Columns>
                <asp:BoundField DataField="PackageVersion" HeaderText="PackageVersion" 
                    ReadOnly="True" SortExpression="PackageVersion" />
                <asp:BoundField DataField="UserName" HeaderText="UserName" ReadOnly="True" 
                    SortExpression="UserName" />
                <asp:BoundField DataField="Changelist" HeaderText="Changelist" ReadOnly="True" 
                    SortExpression="Changelist" />
                <asp:BoundField DataField="Description" HeaderText="Description" 
                    ReadOnly="True" SortExpression="Description" />
            </Columns>
        </asp:GridView>
        <asp:ObjectDataSource ID="CutsceneChangesDataSource" runat="server" 
            OldValuesParameterFormatString="original_{0}" SelectMethod="GetChanges" 
            TypeName="PerforceChangeDataObject">
        </asp:ObjectDataSource>
    </tr>
</table>

<table align="center" cellpadding="0" rules="none" width="100%" height="100%" frame="void">
    <tr>
        <asp:GridView ID="CutsceneBugs" runat="server" 
            DataSourceID="CutsceneBugsDataSource" CssClass="leftColText" 
            Font-Size="Medium" ForeColor="White" GridLines="Vertical" 
        AutoGenerateColumns="False">
            <AlternatingRowStyle BackColor="#3A4F63" />
            <Columns>
                <asp:BoundField DataField="PackageVersion" HeaderText="PackageVersion" 
                    ReadOnly="True" SortExpression="PackageVersion" />
                <asp:BoundField DataField="Bugs" HeaderText="Bugs" 
                    ReadOnly="True" SortExpression="Bugs" />
            </Columns>
        </asp:GridView>
        <asp:ObjectDataSource ID="CutsceneBugsDataSource" runat="server" 
            OldValuesParameterFormatString="original_{0}" SelectMethod="GetChanges" 
            TypeName="PerforceBugDataObject">
        </asp:ObjectDataSource>
    </tr>
</table>



