﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.XPath;

using AutomatedBuildShared;


public partial class Controls_BuildSummaryControl : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        BuildItem item = (BuildItem) Session[RequestParameters.SelectedBuildArgument];
        if (item != null)
        {
            DataTable dataTable = BuildSummaryDataObject.GetRequests(100, item);
            /*DataTable dataTable = new DataTable();
            dataTable.Columns.Add("Changelist");
            dataTable.Columns.Add("User");
            dataTable.Columns.Add("Description");

            object[] testObject = new object[] { "Test", "Test", "Test" };
            DataRow row = dataTable.NewRow();
            row["Changelist"] = "Test";
            row["User"] = "Test";
            row["Description"] = "Test";
            dataTable.Rows.Add(row);*/

            ObjectGridView.DataSource = dataTable;
            ObjectGridView.DataBind();
        }
    }
}