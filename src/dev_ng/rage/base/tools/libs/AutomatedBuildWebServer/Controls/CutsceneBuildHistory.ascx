﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CutsceneBuildHistory.ascx.cs" Inherits="Controls_CutsceneBuildHistory" %>

 <table cellpadding="0" rules="none" width="80%" height="100%" frame="void">
    <tr class="tableRow2" align="center">
        <td><asp:LinkButton ID="UserRequestLinkButton" runat="server" 
                CssClass="leftColText" Font-Size="Large" ForeColor="Yellow" 
                onclick="OnUserRequestClick">My Requests</asp:LinkButton></td>
        <td><asp:LinkButton ID="AllRequestLinkButton" runat="server" 
                CssClass="statusDisplay" Font-Size="Large" ForeColor="Yellow" 
                onclick="OnAllRequestClick">All Requests</asp:LinkButton></td>
        <td><asp:LinkButton ID="FailedRequestsLinkButton" runat="server" 
                CssClass="statusDisplay" Font-Size="Large" ForeColor="Yellow" 
                onclick="OnFailedRequestsClick">Failed Requests</asp:LinkButton></td>
    </tr>
</table>
<table cellpadding="0" rules="none" width="80%" height="95%" frame="void">
    <tr class="tableRow1" align="center">
        <td>
            <asp:Label ID="RecordCountLabel" runat="server" Text="Display Number of Records:" 
                CssClass="statusDisplay" Font-Size="Large"></asp:Label>
            <asp:TextBox ID="RecordCountTextBox" runat="server" 
                ontextchanged="OnNumRecordsTextChanged" Width="95px">100</asp:TextBox>
        </td>
    </tr>
</table>
<table align="center" cellpadding="0" rules="none" width="100%" height="100%" frame="void">
    <tr align="center"><asp:Label ID="CurrentViewLabel" Text="Currently Viewing:" 
            CssClass="statusDisplay" runat="server" Font-Bold="True" Font-Size="Large"></asp:Label>
    </tr>
    <tr>
        <asp:GridView ID="CutsceneRequestView" runat="server" AutoGenerateColumns="False" 
            DataSourceID="CutsceneUserRequestData" CellPadding="8" CssClass="leftColText" 
            Font-Size="Medium" ForeColor="White" GridLines="Vertical" 
            onrowdatabound="OnRowDataBound" onload="OnRequestDataLoad">
            <AlternatingRowStyle BackColor="#3A4F63" />
            <Columns>
                <asp:BoundField DataField="Date" HeaderText="Date" ReadOnly="True" 
                    SortExpression="Date" />
                <asp:BoundField DataField="UserName" HeaderText="UserName" ReadOnly="True" 
                    SortExpression="UserName" />
                <asp:BoundField DataField="Description" HeaderText="Description" 
                    ReadOnly="True" SortExpression="Description" />
                <asp:BoundField DataField="SceneName" HeaderText="SceneName" ReadOnly="True" 
                    SortExpression="SceneName" />
                <asp:CheckBoxField DataField="Result" HeaderText="Result" ReadOnly="True" 
                    SortExpression="Result" />
                <asp:BoundField DataField="Changelist" HeaderText="Changelist" ReadOnly="True" 
                    SortExpression="Changelist" />
            </Columns>
        </asp:GridView>
    </tr>
</table>
<asp:ObjectDataSource ID="CutsceneUserRequestData" runat="server" 
    OldValuesParameterFormatString="original_{0}" SelectMethod="GetRequests" 
    TypeName="CutsceneRequestDataObject">
    <SelectParameters>
        <asp:SessionParameter DefaultValue="100" Name="numRecords" 
            SessionField="NumRecords" Type="Int32" />
        <asp:SessionParameter DefaultValue="unknown" Name="userName" 
            SessionField="UserName" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="CutsceneAllRequestData" runat="server" 
    OldValuesParameterFormatString="original_{0}" SelectMethod="GetAllRequests" 
    TypeName="CutsceneRequestDataObject">
    <SelectParameters>
        <asp:SessionParameter DefaultValue="100" Name="numRecords" 
            SessionField="NumRecords" Type="Int32" />
    </SelectParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="CutsceneFailedRequestData" runat="server" 
    OldValuesParameterFormatString="original_{0}" SelectMethod="GetFailedRequests" 
    TypeName="CutsceneRequestDataObject">
    <SelectParameters>
        <asp:SessionParameter DefaultValue="100" Name="numRecords" 
            SessionField="NumRecords" Type="Int32" />
    </SelectParameters>
</asp:ObjectDataSource>
