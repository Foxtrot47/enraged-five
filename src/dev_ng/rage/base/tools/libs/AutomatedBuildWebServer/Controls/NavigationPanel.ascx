﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NavigationPanel.ascx.cs" Inherits="Controls_NavigationPanel" %>

<%@ Register Src="~/Controls/BuildStatusList.ascx" TagName="BuildStatusList" TagPrefix="AutomatedBuildControls" %>

 <asp:Panel ID="LeftColumnPanel" runat="server" class="leftCol" 
        CssClass="leftColPanel" HorizontalAlign="Center">
    <asp:Label ID="AvailableBuildsLabel" runat="server" CssClass="leftColText" 
        Text="Available Builds" Font-Bold="True" Font-Size="Medium"></asp:Label>
    <asp:TreeView ID="BuildTreeView" CssClass="leftColText" runat="server" 
        NodeIndent="5">
        <NodeStyle ForeColor="Yellow" />
    </asp:TreeView>
</asp:Panel>

