﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Data.SqlClient;

public partial class Controls_CutscenePackageHistory : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session[RequestParameters.NumRecordsArgument] = DefaultNumRecords;

        /*Session["StatisticID"] = "48";

        SqlDataAdapter dataAdapter = new SqlDataAdapter(
              ProfileStatisticsRootDataSource.SelectCommand,
              DatabaseCommon.GetConnectionString());

        DataTable dataTable = new DataTable();
        dataAdapter.Fill(dataTable);

        dataAdapter = new SqlDataAdapter(
            ProfileStatisticsLevel1DataSource.SelectCommand,
            DatabaseCommon.GetConnectionString());

        DataTable dataTable2 = new DataTable();
        dataAdapter.Fill(dataTable2);

        DataSet dataSet = new DataSet("Profiles");
        dataSet.Tables.Add(dataTable);
        dataSet.Tables.Add(dataTable2);


        DataRelation relation = new DataRelation("Relation", dataTable.Columns["ID"], dataTable2.Columns["StatisticID"]);
        dataSet.Relations.Add(relation);

        ProfileStatisticsHierarchyView.DataSource = dataSet;
        ProfileStatisticsHierarchyView.DataBind();*/

       //int id = UsageStatistics.CreateStatisticInfo("RDR3", "Map Export", Environment.MachineName, "Test.max");
       //UsageStatistics.CommitToolProfileStatistic(id, "Main", 60.0);
       //UsageStatistics.CommitToolUsageStatistic("RDR3", "Map Export", Environment.MachineName, null);
        
        ProfileStatisticsTreeView_Populate(null, new TreeNodeEventArgs(ProfileStatisticsTreeView.Nodes[0]));
    }

    /*const int VersionRowIndex = 0;
    const int ChangelistRowIndex = 1;
    const int ResultRowIndex = 2;
    const int PathRowIndex = 3;
    const int PublishedRowIndex = 4;
    */
    private int DefaultNumRecords = 100;
    protected void OnUsageStatisticsRowDataBound(object sender, GridViewRowEventArgs e)
    {

    }

    protected void OnProfileStatisticsRowDataBound(object sender, GridViewRowEventArgs e)
    {
        //Add column to support seeing more data.
        //Tree view instead?
    }

    protected void OnProfileStatisticsSelectedNodeChanged(object sender, EventArgs e)
    {
        ProfileStatisticsTreeView_Populate(sender, new TreeNodeEventArgs(ProfileStatisticsTreeView.SelectedNode));
    }

    protected void ProfileStatisticsTreeView_Populate(object sender, TreeNodeEventArgs e)
    {
        if (e.Node.ChildNodes.Count == 0)
        {
            switch (e.Node.Depth)
            {
                case 0:
                    PopulateRootStatistics(e.Node);
                    break;
                case 1:
                    PopulateProfileStatistics(e.Node);
                    break;
            }
        }
    }

    protected void PopulateRootStatistics(TreeNode node)
    {
        SqlCommand sqlQuery = new SqlCommand();
        sqlQuery.CommandText = ProfileStatisticsRootDataSource.SelectCommand;
        DataSet result = RunQuery(sqlQuery);
        if (result.Tables.Count > 0)
        {
            foreach (DataRow row in result.Tables[0].Rows)
            {
                string text = row["DateTime"].ToString() + " " + row["ProjectName"].ToString() + " " + row["Name"].ToString() + " " + row["MachineName"].ToString() + " " + row["FileName"].ToString();
                TreeNode childNode = new TreeNode(text);
                childNode.PopulateOnDemand = false;
                childNode.SelectAction = TreeNodeSelectAction.SelectExpand;
                node.ChildNodes.Add(childNode);
            }
        }
    }


    protected void PopulateProfileStatistics(TreeNode node)
    {
        SqlCommand sqlQuery = new SqlCommand();
        sqlQuery.CommandText = ProfileStatisticsLevel1DataSource.SelectCommand;
        DataSet result = RunQuery(sqlQuery);
        if (result.Tables.Count > 0)
        {
            foreach (DataRow row in result.Tables[0].Rows)
            {
                string text = row["Scope"].ToString() + " " + row["TimeElapsed"].ToString();
                TreeNode childNode = new TreeNode(text);
                childNode.PopulateOnDemand = false;
                childNode.SelectAction = TreeNodeSelectAction.Expand;
                node.ChildNodes.Add(childNode);
            }
        }
    }

    private DataSet RunQuery(SqlCommand sqlQuery)
    {
        SqlConnection DBConnection = new SqlConnection(DatabaseCommon.GetConnectionString());
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;
        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
        }
        return resultsDataSet;
    }
}