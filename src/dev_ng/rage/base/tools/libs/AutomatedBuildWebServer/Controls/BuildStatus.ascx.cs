﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using AutomatedBuildShared;

public partial class Controls_BuildStatus : System.Web.UI.UserControl
{
    private string UnknownBuildState = "Unknown";
    private string IdleBuildState = "Idle";
    private string InitializingBuildState = "Initializing";
    private string RunningBuildState = "Running";
    private string CompleteBuildState = "Complete";

    private BuildItem m_BuildItem;

    protected void Page_Load(object sender, EventArgs e)
    {
        Populate(m_BuildItem);
        BuildHistoryLink.NavigateUrl = "../BuildHistory.aspx?" + RequestParameters.BuildNameArgument + "=" + m_BuildItem.BuildName;  //TODO: Build-specific page.
    }

    public bool Populate(BuildItem buildItem)
    {
        m_BuildItem = buildItem;

        //BuildProgressTextBox.Enabled = false;
        BuildProgressTextBox.Style["OVERFLOW"] = "auto";  //This turns off scroll bars unless needed for this text box.

        if (buildItem.StatusReport == null)
        {
            BuildNameLabel.Text = buildItem.Name;
            StatusLabel.Text = IdleBuildState;
            StatusLabel.CssClass = "idleLabel";

            BuildProgressTextBox.Text = "Unknown";
            return false;
        }
        else
        {
            BuildNameLabel.Text = buildItem.Name;
            BuildProgressTextBox.Text = "";
            
            if (buildItem.StatusReport.StatusInfo.Status == BuildStatus.Complete)
            {
                StatusLabel.Text = CompleteBuildState;
                StatusLabel.CssClass = "runningLabel";
                BuildProgressTextBox.Text = "No build is currently running.";
            }
            else if (buildItem.StatusReport.StatusInfo.Status == BuildStatus.Initializing)
            {
                StatusLabel.Text = InitializingBuildState;
                StatusLabel.CssClass = "runningLabel";
                BuildProgressTextBox.Text = "Initializing clients...";
            }
            else if (buildItem.StatusReport.StatusInfo.Status == BuildStatus.Running)
            {
                StatusLabel.Text = RunningBuildState;
                StatusLabel.CssClass = "runningLabel";

                int progressNumLines = 1;
                int tasksRemaining = buildItem.StatusReport.StatusInfo.TasksRemaining;
                if (tasksRemaining > 0)
                {
                    if (tasksRemaining == 1)
                    {
                        BuildProgressTextBox.Text = tasksRemaining + " task remaining.\n";
                        progressNumLines++;
                    }
                    else
                    {
                        BuildProgressTextBox.Text = tasksRemaining + " tasks remaining.\n";
                        progressNumLines++;
                    }

                    BuildProgressTextBox.Text += buildItem.StatusReport.StatusInfo.TotalTasks + " total tasks.\n";
                    progressNumLines++;

                    int errorCount = buildItem.StatusReport.StatusInfo.ErrorCount;
                    if (errorCount == 1)
                    {
                        BuildProgressTextBox.Text += errorCount + " error has occurred.\n";
                        progressNumLines++;
                    }
                    else
                    {
                        BuildProgressTextBox.Text += errorCount + " errors have occurred.\n";
                        progressNumLines++;
                    }

                    foreach (ClientInfo clientInfo in buildItem.StatusReport.StatusInfo.ClientInfoList)
                    {
                        if (clientInfo.State != ClientState.Shutdown)
                        {
                            if (String.IsNullOrEmpty(clientInfo.TaskDescription) == true)
                            {
                                BuildProgressTextBox.Text += "\n" + clientInfo.ClientName + " : Idle ";
                            }
                            else
                            {
                                string timeElapsedString = String.Format("{0:hh\\:mm\\:ss}", clientInfo.TimeElapsed);
                                BuildProgressTextBox.Text += "\n" + clientInfo.ClientName + " : " + clientInfo.TaskDescription + " (" + timeElapsedString + ")";
                            }

                            progressNumLines++;
                        }
                    }
                }
                else if (tasksRemaining == 0)
                {
                    //No more tasks to complete; determine whether we're completed or running.
                    BuildProgressTextBox.Text = "This build is completing.";
                }

                //TODO:  Find out why I can't compute the font size based on the "Smaller" types.
                //As a result just hack in a magic number.
                int computedHeight = (progressNumLines) * 20;
                BuildProgressTextBox.Height = Unit.Point(computedHeight);
            }
            else
            {
                StatusLabel.Text = UnknownBuildState;
                StatusLabel.CssClass = "unknownLabel";

                BuildProgressTextBox.Text = "No build is currently running.";
            }

            if (buildItem.StatisticsReport != null)
            {
                TotalTimeText.Text = buildItem.StatisticsReport.TotalTimeOverall.ToString("c");
                TimeTakenText.Text = buildItem.StatisticsReport.TotalTimeTaken.ToString("c");
            }
        }

        return true;
    }
}