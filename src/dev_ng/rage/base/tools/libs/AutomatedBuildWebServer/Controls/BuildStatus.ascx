﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BuildStatus.ascx.cs" Inherits="Controls_BuildStatus" %>
<style type="text/css">
    .style1
    {
        width: 111px;
    }
    .style2
    {
        width: 271px;
    }
    .row1
    {
        background-color: #666666;
        height: auto;
    }
    .row2 
    {  
        background-color: #3a4f63;
        height: auto;
    }
</style>
<hr width="90%" />
<table cellpadding="0" rules="none" width="80%" frame="void" cellspacing="0">
    <tr class="row1">
      <td align="left" class="style1">
          <asp:Label ID="BuildNameLabel" runat="server" 
              CssClass="statusDisplay" Text="Build Name" Font-Size="Large" Width="200%" ></asp:Label></td>
      <td align="Center" class="style2"><b></b></td>
    </tr>
    <tr class="row2">
      <td align="Center" class="style1"><asp:Label ID="StatusTextLabel" runat="server" 
              CssClass="statusDisplay" Text="Status:"></asp:Label></td>
      <td align="left" class="style2"><asp:Label ID="StatusLabel" runat="server" CssClass="statusDisplay" Text="Running"></asp:Label></td>
    </tr>
    <tr class="row1">
      <td align="Center" class="style1">
      </td>
      <td align="left" class="style2">
        <asp:Hyperlink ID="BuildHistoryLink" runat="server" 
              Text="Build History" CssClass="statusDisplay" ForeColor="Yellow"></asp:Hyperlink>
       </td>
    </tr>  
    <tr class="row2">
     <td align="Center" class="style1"><asp:Label ID="TotalTimeLabel" runat="server" 
              Text="Total Time:" CssClass="statusDisplay"></asp:Label></td>
     <td align="Center" class="style2"> <asp:Label ID="TotalTimeText" runat="server" 
              Text="" CssClass="statusDisplay"></asp:Label></td>
    </tr>
    <tr class="row1">
     <td align="Center" class="style1"><asp:Label ID="TimeTakenLabel" runat="server" 
              Text="Time Taken:" CssClass="statusDisplay"></asp:Label></td>
     <td align="Center" class="style2"> <asp:Label ID="TimeTakenText" runat="server" 
              Text="" CssClass="statusDisplay"></asp:Label></td>
    </tr>  
    <tr class="row2">
      <td align="Center" class="style1"><asp:Label ID="BuildProgressTextLabel" runat="server" 
              Text="Current Progress:" CssClass="statusDisplay"></asp:Label></td>
      <td align="Center" class="style2"></td>
    </tr>    
</table>
<table cellpadding="0" rules="none" width="80%" frame="void">

    <tr class="row1">
     <td align="Center" class="style1"></td>
     <td align="Center" class="style2">
         <asp:TextBox ID="BuildProgressTextBox" runat="server" 
              Text="Build Progress Here" ReadOnly="True" 
              Rows="5" BackColor="#666666" CssClass="statusDisplay" BorderStyle="None" BorderWidth="0px" 
             Font-Size="Small" SkinID="0" TextMode="MultiLine" Width="206%"></asp:TextBox></td>
    </tr>
</table>

