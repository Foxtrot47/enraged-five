﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Controls_CutsceneBuildHistory : System.Web.UI.UserControl
{
    enum ViewMode
    {
        UserRequests,
        AllRequests, 
        FailedRequests
    }
    private ViewMode m_CurrentMode;

    private string UserRequestDataSourceID = "CutsceneUserRequestData";
    private string AllRequestDataSourceID = "CutsceneAllRequestData";
    private string FailedRequestDataSourceID = "CutsceneFailedRequestData";

    private string DirectoryName = "MP3_Cutscenes_Rebuild";

    const int DateTimeRowIndex = 0;
    const int DescriptionRowIndex = 2;
    const int CutscenePathRowIndex = 3;
    const int ResultRowIndex = 4;
    const int ChangelistRowIndex = 5;

    private int DefaultNumRecords = 100;
    private string LastDescription = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        Session[RequestParameters.NumRecordsArgument] = DefaultNumRecords;

        int recordNumber = -1;
        if (Int32.TryParse(RecordCountTextBox.Text, out recordNumber) == true && recordNumber > 0)
        {
            Session[RequestParameters.NumRecordsArgument] = recordNumber;
        }

        //Set the default data source. 
        if (Session["mode"] != null)
        {
            ViewMode mode = (ViewMode)Session["mode"];
            if (mode == ViewMode.UserRequests)
            {
                OnUserRequestClick(null, new EventArgs());
            }
            else if (mode == ViewMode.FailedRequests)
            {
                OnFailedRequestsClick(null, new EventArgs());
            }
            else
            {
                OnAllRequestClick(null, new EventArgs());
            }
        }
        else
        {
            OnUserRequestClick(null, new EventArgs());
        }

    }

    protected void OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex != -1)
        {
            //Adjust the DateTime to be in local time zone.  Times in the database are stored in UTC.
            DateTime timeUtc = DateTime.Parse(e.Row.Cells[DateTimeRowIndex].Text);
            DateTime timeCurrentZone = timeUtc.AddHours(TimeZone.CurrentTimeZone.GetUtcOffset(timeUtc).Hours);
            e.Row.Cells[DateTimeRowIndex].Text = timeCurrentZone.ToString();

            //Take only the scene name in the cutscene path.
            const string cutxmlExtension = ".cutxml";
            const string cutlistExtension = ".cutlist";
            const string mergelistExtension = ".mergelist";

            string extension = Path.GetExtension(e.Row.Cells[CutscenePathRowIndex].Text);
            string sceneName = "";
            if (String.Compare(extension, cutxmlExtension, true) == 0)
            {
                sceneName = Path.GetFileName(Path.GetDirectoryName(e.Row.Cells[CutscenePathRowIndex].Text));
            }
            else if (String.Compare(extension, cutlistExtension, true) == 0 || String.Compare(extension, mergelistExtension, true) == 0)
            {
                string concatName = Path.GetFileNameWithoutExtension(e.Row.Cells[CutscenePathRowIndex].Text);
                sceneName = concatName;
            }

            string currentDescription = e.Row.Cells[DescriptionRowIndex].Text;
            GridView dataGridSender = sender as GridView;
            if (e.Row.RowIndex == 0)
            {
                LastDescription = e.Row.Cells[DescriptionRowIndex].Text;
            }
            else if (e.Row.RowIndex > 0 && LastDescription != null && String.Compare(currentDescription, LastDescription, true) == 0)
            {
                e.Row.Cells[DescriptionRowIndex].Text = "--";
            }
            else
            {
                LastDescription = e.Row.Cells[DescriptionRowIndex].Text;
            }

            System.Drawing.Color displayColor = System.Drawing.Color.Gray;
            string displayText = "Pending";

            CutsceneRequestDataObject data = (CutsceneRequestDataObject)e.Row.DataItem;

            bool noLink = String.IsNullOrEmpty(data.m_HTML) || String.Compare(data.m_HTML, "&nbsp;") == 0;
            if (noLink == false)
            {
                CheckBox resultCheckBox = (CheckBox)(e.Row.Cells[ResultRowIndex]).Controls[0];
                if (resultCheckBox.Checked == true)
                {
                    displayColor = System.Drawing.Color.LightBlue;
                    displayText = "Succeeded";
                }
                else
                {
                    displayColor = System.Drawing.Color.LightSalmon;
                    displayText = "Failed";
                }
            }

            e.Row.Cells[ResultRowIndex].Controls.Clear();

            e.Row.Cells[ResultRowIndex].ForeColor = displayColor;
            e.Row.Cells[ResultRowIndex].Text = displayText;

            if (data.m_HTML != null)
            {
                string fullHTMLPath = "<a href=\"" + Path.Combine(BuildInfoService.RootDirectory, DirectoryName, data.m_HTML) + "\">" + sceneName + "</a>";
                e.Row.Cells[CutscenePathRowIndex].Text = fullHTMLPath;
            }
            else
            {
                e.Row.Cells[CutscenePathRowIndex].Text = sceneName;
            }

            if (data.m_Changelist == CutsceneRequestDataObject.NullChangelist)
            {
                e.Row.Cells[ChangelistRowIndex].Text = "--";
            }
            else if (data.m_Changelist == CutsceneRequestDataObject.InvalidChangelist)
            {
                e.Row.Cells[ChangelistRowIndex].ForeColor = System.Drawing.Color.LightSalmon;
                e.Row.Cells[ChangelistRowIndex].Text = "Not Submitted";
            }
            else
            {
                e.Row.Cells[ChangelistRowIndex].ForeColor = System.Drawing.Color.LightBlue;
                e.Row.Cells[ChangelistRowIndex].Text = data.m_Changelist.ToString();
            }
        }
    }

    protected void OnRequestDataLoad(object sender, EventArgs e)
    {
        CutsceneRequestView.DataBind();
    }

    protected void OnUserRequestClick(object sender, EventArgs e)
    {
        CutsceneRequestView.DataSourceID = UserRequestDataSourceID;
        string userName = Session[RequestParameters.UserNameArgument] as string;
        if (String.IsNullOrEmpty(userName) == false)
        {
            CurrentViewLabel.Text = "Currently Viewing: " + Session[RequestParameters.UserNameArgument] + "'s Requests.";
        }
        else
        {
            CurrentViewLabel.Text = "Currently Viewing: User Unknown.";
        }

        Session["mode"] = ViewMode.UserRequests;
        CutsceneRequestView.DataBind();
    }

    protected void OnAllRequestClick(object sender, EventArgs e)
    {
        CutsceneRequestView.DataSourceID = AllRequestDataSourceID;
        CurrentViewLabel.Text = "Currently Viewing: All Requests.";

        Session["mode"] = ViewMode.AllRequests;
        CutsceneRequestView.DataBind();
    }

    protected void OnFailedRequestsClick(object sender, EventArgs e)
    {
        CutsceneRequestView.DataSourceID = FailedRequestDataSourceID;
        CurrentViewLabel.Text = "Currently Viewing: Outstanding Failed Requests.";

        Session["mode"] = ViewMode.FailedRequests;
        CutsceneRequestView.DataBind();
    }

    protected void OnNumRecordsTextChanged(object sender, EventArgs e)
    {
        int recordNumber = -1;
        if (Int32.TryParse(RecordCountTextBox.Text, out recordNumber) == true && recordNumber > 0)
        {
            Session[RequestParameters.NumRecordsArgument] = recordNumber;
            CutsceneRequestView.DataBind();
        }
    }
}