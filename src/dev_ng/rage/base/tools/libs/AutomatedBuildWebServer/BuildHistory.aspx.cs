﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class BuildHistory : System.Web.UI.Page
{
    private string BuildName;

    protected void Page_Load(object sender, EventArgs e)
    {
        BuildName = this.Request.Params[RequestParameters.BuildNameArgument];
        if (BuildName == null)
        {
            //Forward to a page with a listing of all builds to see.
        }
        else
        {
            this.Title = BuildName + " Build History";
        }
        
        PopulateUI();
    }

    private bool PopulateUI()
    {
        //Populate the tree view navigation control.
        NavPanel.PopulateUI();

        if (Request.Params[RequestParameters.UserNameArgument] != null)
        {
            Session[RequestParameters.UserNameArgument] = Request.Params[RequestParameters.UserNameArgument];
        }
        else
        {
            string domainUserName = Request.ServerVariables["LOGON_USER"];
            string userName = Path.GetFileName(domainUserName);
            Session[RequestParameters.UserNameArgument] = userName;
        }

        BuildItem buildItem = null;
        List<BuildItem> buildItemList = BuildInfoService.GetBuildData();
        foreach (BuildItem item in buildItemList)
        {
            if (String.Compare(BuildName, item.BuildName, true) == 0)
            {
                buildItem = item;
                break;
            }
        }

        if (buildItem != null)
        {
            Session[RequestParameters.SelectedBuildArgument] = buildItem;
        }

        CustomHistoryPanel.Controls.Clear();

        //TODO:  Hack for this specific build type.  
        if (String.Compare(BuildName, "MP3_Cutscene_Packages", true) == 0)
        {
            Control newControl = LoadControl("~/Controls/CutscenePackageHistory.ascx");
            CustomHistoryPanel.Controls.Add(newControl);
        }
        else if (String.Compare(BuildName, "RDR3_Usage_Statistics", true) == 0)
        {
            Control newControl = LoadControl("~/Controls/UsageStatistics.ascx");
            CustomHistoryPanel.Controls.Add(newControl);
        }
        else if (String.Compare(BuildName, "RDR3_Code", true) == 0)
        {
            Control newControl = LoadControl("~/Controls/BuildSummaryControl.ascx");
            CustomHistoryPanel.Controls.Add(newControl);
        }
        else
        {
            Control newControl = LoadControl("~/Controls/CutsceneBuildHistory.ascx");
            CustomHistoryPanel.Controls.Add(newControl);
        }

        return true;
    }
}