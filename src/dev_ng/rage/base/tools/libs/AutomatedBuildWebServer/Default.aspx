﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="_Default" %>

<%@ Register Src="~/Controls/BuildStatus.ascx" TagName="BuildStatus" TagPrefix="AutomatedBuildControls" %>
<%@ Register Src="~/Controls/BuildStatusList.ascx" TagName="BuildStatusList" TagPrefix="AutomatedBuildControls" %>
<%@ Register Src="~/Controls/NavigationPanel.ascx" TagName="NavigationPanel" TagPrefix="AutomatedBuildControls" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">

    <meta http-equiv="refresh" content="60">

    <table cellpadding="0" rules="none" width="100%" height="100%" frame="void">
        <tr>
         <td valign="top" width="20%" class="rootTable1"> 
                <AutomatedBuildControls:NavigationPanel ID="NavPanel" CssClass="leftCol" runat="server" />
         </td>
         <td align="Center" width="80%">
            <div>
                <automatedbuildcontrols:buildstatuslist ID="StatusList" CssClass="rightCol" 
                    runat="server" />
            </div>
         </td>
        </tr>
    </table>
   
</asp:Content>
