﻿using System;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CustomControls
{
    /// <summary>
    /// Summary description for Controls_MultiLineBoundField
    /// </summary>
    public class MultiLineBoundField : System.Web.UI.WebControls.BoundField
    {
        public TextBoxMode TextMode
        {
            get
            {
                TextBoxMode _tm = TextBoxMode.SingleLine;
                if (this.ViewState["TextMode"] != null)
                    _tm = (TextBoxMode)this.ViewState["TextMode"];
                return _tm;
            }
            set { this.ViewState["TextMode"] = value; }
        }

        public int Columns
        {
            get
            {
                int i = 0;
                if (this.ViewState["Columns"] != null)
                    i = (int)this.ViewState["Columns"];
                return i;
            }
            set { this.ViewState["Columns"] = value; }
        }

        public int Rows
        {
            get
            {
                int i = 0;
                if (this.ViewState["Rows"] != null)
                    i = (int)this.ViewState["Rows"];
                return i;
            }
            set { this.ViewState["Rows"] = value; }
        }

        public bool Wrap
        {
            get
            {
                bool b = true;
                if (this.ViewState["Wrap"] != null)
                    b = (bool)this.ViewState["Wrap"];
                return b;
            }
            set { this.ViewState["Wrap"] = value; }
        }

        protected override void OnDataBindField(object sender, EventArgs e)
        {
            base.OnDataBindField(sender, e);
            DataControlFieldCell cell = (DataControlFieldCell)sender;
            cell.Width = 500;
            cell.Height = 100;

            TextBox txt = new TextBox();
            cell.Controls.Add(txt);

            txt.TextMode = this.TextMode;
            txt.Columns = this.Columns;
            txt.Width = cell.Width;
            txt.Height = cell.Height;
            txt.Rows = this.Rows;
            txt.Wrap = this.Wrap;
            txt.Text = cell.Text;
        }

    }
}