﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;

/// <summary>
/// Summary description for BuildInfoService
/// </summary>
public class BuildInfoService
{
    public static List<BuildItem> BuildItemList;

    private static string m_RootDirectory = @"\\sanb-vm1\reports";  //TODO:  Temporary until I can find where configuration settings are.
    private static readonly string m_LabelSearchWildcard = "*.label";

    private static object m_BuildInfoLock;
    private static Thread m_ServiceThread;
    private static bool m_Abort;

    public static string RootUNCDirectory { get { return m_RootDirectory; } }
    public static string RootDirectory { get { return @"http://sanb-vm1/reports"; } }  //TODO:  Do not make specific to this server; drive from the configuration.

	public BuildInfoService()
	{
        m_BuildInfoLock = new object();
        m_ServiceThread = null;
        m_Abort = false;
	}

    public static void Start()
    {
        m_BuildInfoLock = new object();

        m_ServiceThread = new Thread(StartThread);
        m_ServiceThread.Name = "Build Info Service Thread";
        m_ServiceThread.Start();
    }

    private static void StartThread()
    {
        while (m_Abort == false)
        {
            List<BuildItem> list = GetLiveBuildData("*");

            lock (m_BuildInfoLock)
            {
                BuildItemList = list;
            }

            Thread.Sleep(1000 * 60);
        } 
    }

    public static void Stop()
    {
        if (m_ServiceThread.IsAlive)
        {
            m_Abort = true;
            m_ServiceThread.Join();
        }
    }

    /// <summary>
    /// Acquires all current build data.
    /// </summary>
    /// <returns></returns>
    public static List<BuildItem> GetBuildData()
    {
        lock (m_BuildInfoLock)
        {
            return BuildItemList;
        }
    }

    public static List<BuildItem> GetBuildData(string buildNameWildcard)
    {
        List<BuildItem> buildItems = new List<BuildItem>();
        lock (m_BuildInfoLock)
        {
            if (BuildItemList != null)
            {
                foreach (BuildItem item in BuildItemList)
                {
                    Match match = Regex.Match(item.BuildName, buildNameWildcard, RegexOptions.IgnoreCase);
                    if (match.Success == true)
                    {
                        buildItems.Add(item);
                    }
                }
            }
        }

        return buildItems;
    }

    private static List<BuildItem> GetLiveBuildData(string buildNameWildcard)
    {
        List<BuildItem> buildItems = new List<BuildItem>();

        try
        {
            //Acquire a list of all active builds on this server.
            //In the interest of keeping this fast, take the root directory, and search only two levels in.
            List<string> labelFiles = new List<string>();

            string[] buildDirectories = Directory.GetDirectories(m_RootDirectory, buildNameWildcard, SearchOption.TopDirectoryOnly);
            foreach (string buildDirectory in buildDirectories)
            {
                string[] foundLabels = Directory.GetFiles(buildDirectory, m_LabelSearchWildcard, SearchOption.TopDirectoryOnly);

                labelFiles.AddRange(foundLabels);
            }

            //Each label file indicates that a build is active.
            foreach (string labelFile in labelFiles)
            {
                string buildName = Path.GetFileNameWithoutExtension(labelFile);

                BuildItem buildItem = new BuildItem(buildName, Path.GetDirectoryName(labelFile));
                buildItem.Update();
                buildItems.Add(buildItem);
            }

            //TODO:  For builds that do not follow the Cruise Control paradigm.
            BuildItem cutscenePackageBuildItem = new BuildItem("MP3_Cutscene_Packages", null);
            cutscenePackageBuildItem.Update();
            buildItems.Add(cutscenePackageBuildItem);

            BuildItem rdr3Statistics = new BuildItem("RDR3_Usage_Statistics", null);
            rdr3Statistics.Update();
            buildItems.Add(rdr3Statistics);
        }
        catch (Exception)
        {
            //Hide the exception; most likely the root directory can not be found.
        }

        return buildItems;
    }
}