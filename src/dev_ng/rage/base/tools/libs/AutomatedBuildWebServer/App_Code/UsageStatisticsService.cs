﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Services;

public class ToolProfileStatisticAsyncObject
{
    public int StatisticID;
    public string Scope;
    public double TimeElapsed;
}

public class ToolUsageStatisticAsyncObject
{
    public string Project;
    public string StatisticType;
    public string MachineName;
    public string Note;
}

/// <summary>
/// Summary description for QueueStatus
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class UsageStatisticsService : System.Web.Services.WebService {

    static bool AsyncOperations = false;
    public UsageStatisticsService()
    {
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string HelloWorld() {
        return "Hello World";
    }

    [WebMethod]
    public int CreateStatisticInfo(string project, string statisticType, string machineName, string filename)
    {
        try
        {
            return UsageStatistics.CreateStatisticInfo(project, statisticType, machineName, filename);
        }
        catch (Exception e)
        {
        }

        return -1;
    }

    private void SubmitToolProfileStatisticAsync(object obj)
    {
        ToolProfileStatisticAsyncObject asyncObject = (ToolProfileStatisticAsyncObject)obj;
        UsageStatistics.CommitToolProfileStatistic(asyncObject.StatisticID, asyncObject.Scope, asyncObject.TimeElapsed);
    }

    [WebMethod]
    public int SubmitToolProfileStatistic(int statisticID, string scope, double seconds)
    {
        try
        {
            ToolProfileStatisticAsyncObject asyncObject = new ToolProfileStatisticAsyncObject();
            asyncObject.StatisticID = statisticID;
            asyncObject.Scope = scope;
            asyncObject.TimeElapsed = seconds;

            if (AsyncOperations == true)
            {
                Thread asyncThread = new Thread(SubmitToolProfileStatisticAsync);
                asyncThread.Name = "Submit Profile Statistic Thread";
                asyncThread.Start(asyncObject);
            }
            else
            {
                return UsageStatistics.CommitToolProfileStatistic(asyncObject.StatisticID, asyncObject.Scope, asyncObject.TimeElapsed);
            }
        }
        catch (Exception e)
        {
        }

        return -1;
    }

    private void SubmitToolUsageStatisticAsync(object obj)
    {
        ToolUsageStatisticAsyncObject asyncObject = (ToolUsageStatisticAsyncObject)obj;

        UsageStatistics.CommitToolUsageStatistic(asyncObject.Project, asyncObject.StatisticType, asyncObject.MachineName, asyncObject.Note);
    }

    [WebMethod]
    public int SubmitToolUsageStatistic(string project, string statisticType, string machineName, string note)
    {
        try
        {
            ToolUsageStatisticAsyncObject asyncObject = new ToolUsageStatisticAsyncObject();
            asyncObject.Project = project;
            asyncObject.StatisticType = statisticType;
            asyncObject.MachineName = machineName;
            asyncObject.Note = note;

            if ( AsyncOperations == true )
            {
                Thread asyncThread = new Thread(SubmitToolUsageStatisticAsync);
                asyncThread.Name = "Submit Usage Statistic Thread";
                asyncThread.Start(asyncObject);
            }
            else
            {
                return UsageStatistics.CommitToolUsageStatistic(asyncObject.Project, asyncObject.StatisticType, asyncObject.MachineName, asyncObject.Note);
            }
        }
        catch (Exception e)
        {
        }

        return -1;
    }
}
