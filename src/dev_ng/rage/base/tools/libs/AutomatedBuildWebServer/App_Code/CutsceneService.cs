﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Data.SqlClient;
using System.Threading;
using System.Text.RegularExpressions;
using System.Web;

using P4API;
using RSG.SourceControl.Perforce;

[DataObject]
public class PerforceChangeDataObject
{
    private string m_PackageVersion;
    private string m_UserName;
    private int m_Changelist;
    private string m_Description;

    public PerforceChangeDataObject()
    {
        m_PackageVersion = "Pending";
        m_UserName = null;
        m_Changelist = -1;
        m_Description = null;
    }

    public PerforceChangeDataObject(string packageVersion, string userName, int changelist, string description)
    {
        m_PackageVersion = packageVersion;
        m_UserName = userName;
        m_Changelist = changelist;
        m_Description = description;
    }

    public string PackageVersion { get { return m_PackageVersion; } }
    public string UserName { get { return m_UserName; } }
    public int Changelist { get { return m_Changelist; } }
    public string Description { get { return m_Description; } }

    [DataObjectMethod(DataObjectMethodType.Select)]
    public List<PerforceChangeDataObject> GetChanges()
    {
        if (CutsceneService.LastChangelists == null)
        {
            return new List<PerforceChangeDataObject>();
        }

        lock (CutsceneService.LastChangelists)
        {
            return CutsceneService.LastChangelists;
        }
    }
}

[DataObject]
public class PerforceBugDataObject
{
    private string m_PackageVersion;
    private string m_Bugs;
    
    public PerforceBugDataObject()
    {
        m_PackageVersion = "Pending";
        m_Bugs = null;
    }

    public PerforceBugDataObject(string packageVersion, List<int> bugs)
    {
        m_PackageVersion = packageVersion.ToString();
        m_Bugs = "";
        for(int bugIndex = 0; bugIndex < bugs.Count; ++bugIndex)
        {
            if ( bugIndex != 0 )
                m_Bugs += " ";
            
            m_Bugs += bugs[bugIndex];
        }
    }

    public string PackageVersion { get { return m_PackageVersion; } }
    public string Bugs { get { return m_Bugs; } }
    
    [DataObjectMethod(DataObjectMethodType.Select)]
    public List<PerforceBugDataObject> GetChanges()
    {
        lock (CutsceneService.LastBugs)
        {
            return CutsceneService.LastBugs;
        }
    }
}

/// <summary>
/// Summary description for CutsceneService
/// </summary>
public class CutsceneService
{
    private static string m_PerforceIndependentRPFPath = "//projects/payne/build/dev/independent/anim/cuts_final1.rpf";
    private static string m_PerforceCutscenePath = "//projects/payne/payne_art/anim/exportCutscene/...";
    private static string m_LastRPFChangelist;
    private static int m_LastPublishedChangelist;

    private static Thread m_ServiceThread;
    private static bool m_Abort;

    public static List<PerforceChangeDataObject> LastChangelists;
    public static List<PerforceBugDataObject> LastBugs;

	public CutsceneService()
	{
        m_LastRPFChangelist = null;
        m_LastPublishedChangelist = -1;
        LastChangelists = new List<PerforceChangeDataObject>();
        LastBugs = new List<PerforceBugDataObject>();

        m_ServiceThread = null;
        m_Abort = false;
 	}

    public static void Start()
    {
        if (m_ServiceThread == null)
        {
            m_ServiceThread = new Thread(StartThread);
            m_ServiceThread.Name = "Cutscene Service Thread";
            m_ServiceThread.Start();
        }
    }

    public static void Stop()
    {
        if (m_ServiceThread.IsAlive)
        {
            m_Abort = true;
            m_ServiceThread.Join();
            m_ServiceThread = null;
        }
    }
    
    private static void StartThread()
    {
         //Put a watch on the independent RPF.
        P4 p4 = new P4();
        p4.Port = "rsgsanp4p01:2001";   //TODO:  Removed hardcoded; decipher based on the project.
        p4.Password = "U34(build)";
        p4.User = "svcrsgsanvmbuilder";
        p4.Client = "sanb-vm1";

        const int MinimumChangelistLength = 6;
        LastChangelists = new List<PerforceChangeDataObject>();
        LastBugs = new List<PerforceBugDataObject>();

        while (m_Abort == false)
        {
            try
            {
                p4.Connect("U34(build)");

                P4RecordSet changesRecords = p4.Run("changes", "-m", "1", "-l", m_PerforceIndependentRPFPath);
                foreach (P4Record changeRecord in changesRecords)
                {
                    string currentChangelist = changeRecord["change"];

                    if (m_LastRPFChangelist != null && m_LastRPFChangelist.CompareTo(currentChangelist) == 0)
                        continue;

                    m_LastRPFChangelist = currentChangelist;

                    string description = changeRecord["desc"];

                    Match match = Regex.Match(description, "[\\d]+");
                    foreach (Capture capture in match.Captures)
                    {
                        if (capture.Length >= MinimumChangelistLength)
                        {
                            if (Int32.TryParse(capture.Value, out m_LastPublishedChangelist))
                            {
                                CutsceneRequests.PublishCutscenePackage(m_LastPublishedChangelist);
                            }
                        }
                    }
                }

                List<CutscenePackageDataObject> cutscenePackageData = CutsceneRequests.GetPackageInfo(m_LastPublishedChangelist);
                changesRecords = p4.Run("changes", "-l", m_PerforceCutscenePath + "@" + m_LastPublishedChangelist + ",@now");

                Dictionary<string, List<int>> bugsDictionary = new Dictionary<string, List<int>>();
                List<PerforceChangeDataObject> newLastChangelists = new List<PerforceChangeDataObject>();
                foreach (P4Record changeRecord in changesRecords)
                {
                    int changelist = Int32.Parse(changeRecord["change"]);

                    CutscenePackageDataObject rpfContainingChange = null;
                    foreach (CutscenePackageDataObject data in cutscenePackageData)
                    {
                        if ( data.Changelist < changelist )
                        {
                            //The previous package contains this change.
                            break;
                        }

                        rpfContainingChange = data;
                    }

                    string version = "Pending";
                    if (rpfContainingChange != null)
                    {
                        version = rpfContainingChange.ID.ToString();
                    }

                    string description = changeRecord["desc"];

                    MatchCollection matches = Regex.Matches(description, "([\\d]+)");
                    foreach (Match match in matches)
                    {
                        if (match.Length >= 4)
                        {
                            int bugNumber = -1;
                            if (Int32.TryParse(match.Value, out bugNumber))
                            {
                                if (bugsDictionary.ContainsKey(version) == false)
                                {
                                    bugsDictionary.Add(version, new List<int>());
                                }

                                if ( bugsDictionary[version].Contains(bugNumber) == false )
                                    bugsDictionary[version].Add(bugNumber);
                            }
                        }
                    }

                    PerforceChangeDataObject change = new PerforceChangeDataObject(version, changeRecord["user"], changelist, description);
                    newLastChangelists.Add(change);
                }

                lock (LastChangelists)
                {
                    LastChangelists = newLastChangelists;
                }

                lock (LastBugs)
                {
                    foreach(KeyValuePair<string, List<int>> keyValuePair in bugsDictionary)
                    {
                        PerforceBugDataObject bug = new PerforceBugDataObject(keyValuePair.Key, keyValuePair.Value);
                        LastBugs.Add(bug);
                    }
                }

                p4.Disconnect();
            }
            catch (Exception e) 
            {
                //Not pretty; there's got to be a better way to get error data from the live website.
                StreamWriter writer = new StreamWriter("C:\\errors.txt");
                writer.WriteLine("User Name: " + Environment.UserName);
                writer.WriteLine(e.Message);
                writer.Close();
            }

            //Run only once an hour.
            Thread.Sleep(1000 * 60 * 60);
        } 
    }

    /*private static List<string> GetLockedCutscenes()
    {

    }*/

}
