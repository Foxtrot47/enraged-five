﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Queries an SQL database for cutscene requests and organizes data 
/// into a format displayble by web forms.
/// </summary>
[DataObject]
public class CutsceneRequestDataObject
{
    private static string m_SubmissionUserNameColumn = "UserName";
    private static string m_RequestDateTimeColumn = "DateTime";
    private static string m_RequestDescriptionColumn = "Description";
    private static string m_RequestPathColumn = "CutscenePath";
    private static string m_RequestResultColumn = "Result";
    private static string m_RequestResultHTMLColumn = "ResultHTML";
    private static string m_RequestChangelistColumn = "Changelist";

    public DateTime m_DateTime;
    public string m_UserName;
    public string m_Description;
    public string m_SceneName;
    public bool m_Result;
    public string m_HTML;
    public int m_Changelist;

    public static int NullChangelist = Int32.MinValue;
    public static int InvalidChangelist = -1;

    public static object SqlLock = new object();

    public DateTime Date { get { return m_DateTime; } }
    public string UserName { get { return m_UserName; } }
    public string Description { get { return m_Description; } }
    public string SceneName { get { return m_SceneName; } }
    public bool Result { get { return m_Result; } }
    public int Changelist { get { return m_Changelist; } }

    [DataObjectMethod(DataObjectMethodType.Select)]
    public List<CutsceneRequestDataObject> GetRequests(int numRecords, string userName)
    {
        List<CutsceneRequestDataObject> cutsceneRequests = new List<CutsceneRequestDataObject>();
        lock (SqlLock)
        {
            string selectQuery = "SELECT TOP (@NumRecords) CutsceneSubmissions.DateTime, CutsceneSubmissions.UserName, CutsceneSubmissions.Description, CutsceneRequests.CutscenePath, CutsceneRequests.Result, "
                + "CutsceneRequests.ResultHTML, CutsceneRequests.Changelist "
                + "FROM CutsceneSubmissions INNER JOIN "
                + "CutsceneRequests ON CutsceneRequests.SubmissionID = CutsceneSubmissions.ID "
                + "WHERE        (CutsceneSubmissions.UserName = @UserName) "
                + "ORDER BY CutsceneSubmissions.DateTime DESC";

            SqlConnection sqlConnection = new SqlConnection(CutsceneRequests.GetConnectionString());
            sqlConnection.Open();

            SqlCommand selectCommand = new SqlCommand(selectQuery, sqlConnection);
            SqlParameter numRecordsParameter = new SqlParameter("@NumRecords", numRecords);
            SqlParameter userNameParameter = new SqlParameter("@UserName", userName);
            selectCommand.Parameters.Add(numRecordsParameter);
            selectCommand.Parameters.Add(userNameParameter);

            SqlDataReader reader = selectCommand.ExecuteReader();

            while (reader.Read() == true)
            {
                CutsceneRequestDataObject cutsceneRequest = new CutsceneRequestDataObject();
                cutsceneRequest.m_DateTime = (DateTime)reader[m_RequestDateTimeColumn];
                cutsceneRequest.m_UserName = (string)reader[m_SubmissionUserNameColumn];
                cutsceneRequest.m_Description = (string)reader[m_RequestDescriptionColumn];
                cutsceneRequest.m_SceneName = (string)reader[m_RequestPathColumn];

                if (reader[m_RequestResultColumn].GetType() != typeof(System.DBNull))
                {
                    cutsceneRequest.m_Result = (bool)reader[m_RequestResultColumn];
                }

                if (reader[m_RequestResultHTMLColumn].GetType() != typeof(System.DBNull))
                {
                    cutsceneRequest.m_HTML = (string)reader[m_RequestResultHTMLColumn];
                }

                if (reader[m_RequestChangelistColumn].GetType() != typeof(System.DBNull))
                {
                    cutsceneRequest.m_Changelist = (int)reader[m_RequestChangelistColumn];
                }
                else
                {
                    cutsceneRequest.m_Changelist = NullChangelist;
                }

                cutsceneRequests.Add(cutsceneRequest);
            }

            sqlConnection.Close();
        }

        return cutsceneRequests;
    }

    [DataObjectMethod(DataObjectMethodType.Select)]
    public List<CutsceneRequestDataObject> GetAllRequests(int numRecords)
    {
        List<CutsceneRequestDataObject> cutsceneRequests = new List<CutsceneRequestDataObject>();
        lock (SqlLock)
        {
            string selectQuery = "SELECT TOP (@NumRecords) CutsceneSubmissions.DateTime, CutsceneSubmissions.UserName, CutsceneSubmissions.Description, CutsceneRequests.CutscenePath, CutsceneRequests.Result, "
                        + "CutsceneRequests.ResultHTML, CutsceneRequests.Changelist "
                        + "FROM            CutsceneRequests INNER JOIN "
                        + "CutsceneSubmissions ON CutsceneSubmissions.ID = CutsceneRequests.SubmissionID "
                        + "ORDER BY CutsceneSubmissions.DateTime DESC ";

            SqlConnection sqlConnection = new SqlConnection(CutsceneRequests.GetConnectionString());
            sqlConnection.Open();

            SqlCommand selectCommand = new SqlCommand(selectQuery, sqlConnection);
            SqlParameter numRecordsParameter = new SqlParameter("@NumRecords", numRecords);
            selectCommand.Parameters.Add(numRecordsParameter);

            SqlDataReader reader = selectCommand.ExecuteReader();

            while (reader.Read() == true)
            {
                CutsceneRequestDataObject cutsceneRequest = new CutsceneRequestDataObject();
                cutsceneRequest.m_DateTime = (DateTime)reader[m_RequestDateTimeColumn];
                cutsceneRequest.m_UserName = (string)reader[m_SubmissionUserNameColumn];
                cutsceneRequest.m_Description = (string)reader[m_RequestDescriptionColumn];
                cutsceneRequest.m_SceneName = (string)reader[m_RequestPathColumn];

                if (reader[m_RequestResultColumn].GetType() != typeof(System.DBNull))
                {
                    cutsceneRequest.m_Result = (bool)reader[m_RequestResultColumn];
                }

                if (reader[m_RequestResultHTMLColumn].GetType() != typeof(System.DBNull))
                {
                    cutsceneRequest.m_HTML = (string)reader[m_RequestResultHTMLColumn];
                }

                if (reader[m_RequestChangelistColumn].GetType() != typeof(System.DBNull))
                {
                    cutsceneRequest.m_Changelist = (int)reader[m_RequestChangelistColumn];
                }
                else
                {
                    cutsceneRequest.m_Changelist = CutsceneRequestDataObject.NullChangelist;
                }

                cutsceneRequests.Add(cutsceneRequest);
            }

            sqlConnection.Close();
        }

        return cutsceneRequests;
    }

    [DataObjectMethod(DataObjectMethodType.Select)]
    public List<CutsceneRequestDataObject> GetFailedRequests(int numRecords)
    {
        List<CutsceneRequestDataObject> cutsceneRequests = new List<CutsceneRequestDataObject>();
        lock (SqlLock)
        {
            string selectQuery = "WITH LatestResults AS "
                    + "(SELECT TOP (100) PERCENT MAX(ID) AS MostRecentId, CutscenePath "
                    + "FROM CutsceneRequests GROUP BY CutscenePath ORDER BY CutscenePath) "
                    + "SELECT CutsceneSubmissions.DateTime, CutsceneSubmissions.UserName, CutsceneSubmissions.Description, "
                    + "CutsceneRequests_1.CutscenePath, CutsceneRequests_1.Result, CutsceneRequests_1.ResultHTML, CutsceneRequests_1.Changelist "
                    + "FROM CutsceneRequests AS CutsceneRequests_1 INNER JOIN CutsceneSubmissions ON CutsceneRequests_1.SubmissionID = CutsceneSubmissions.ID "
                    + "WHERE (CutsceneRequests_1.ID IN (SELECT MostRecentId FROM LatestResults AS LatestFailedResults)) AND (CutsceneRequests_1.Result = 0) "
                    + "ORDER BY CutsceneSubmissions.DateTime DESC";

            SqlConnection sqlConnection = new SqlConnection(CutsceneRequests.GetConnectionString());
            sqlConnection.Open();

            SqlCommand selectCommand = new SqlCommand(selectQuery, sqlConnection);
            SqlParameter numRecordsParameter = new SqlParameter("@NumRecords", numRecords);
            selectCommand.Parameters.Add(numRecordsParameter);

            SqlDataReader reader = selectCommand.ExecuteReader();

            while (reader.Read() == true)
            {
                CutsceneRequestDataObject cutsceneRequest = new CutsceneRequestDataObject();
                cutsceneRequest.m_DateTime = (DateTime)reader[m_RequestDateTimeColumn];
                cutsceneRequest.m_UserName = (string)reader[m_SubmissionUserNameColumn];
                cutsceneRequest.m_Description = (string)reader[m_RequestDescriptionColumn];
                cutsceneRequest.m_SceneName = (string)reader[m_RequestPathColumn];

                if (reader[m_RequestResultColumn].GetType() != typeof(System.DBNull))
                {
                    cutsceneRequest.m_Result = (bool)reader[m_RequestResultColumn];
                }

                if (reader[m_RequestResultHTMLColumn].GetType() != typeof(System.DBNull))
                {
                    cutsceneRequest.m_HTML = (string)reader[m_RequestResultHTMLColumn];
                }

                if (reader[m_RequestChangelistColumn].GetType() != typeof(System.DBNull))
                {
                    cutsceneRequest.m_Changelist = (int)reader[m_RequestChangelistColumn];
                }
                else
                {
                    cutsceneRequest.m_Changelist = CutsceneRequestDataObject.NullChangelist;
                }

                cutsceneRequests.Add(cutsceneRequest);
            }

            sqlConnection.Close();
        }
        return cutsceneRequests;
    }
}