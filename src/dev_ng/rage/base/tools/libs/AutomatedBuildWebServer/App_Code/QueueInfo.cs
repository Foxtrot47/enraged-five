﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace AutomatedBuildWebService
{
    public class QueueInfo
    {
        [XmlAttribute("BuildName")]
        public string BuildName;

        [XmlAttribute("QueueFile")]
        public string QueueFile;

        [XmlAttribute("QueueLength")]
        public int QueueLength;

        public QueueInfo()
        {
            BuildName = null;
            QueueFile = null;
            QueueLength = -1;
        }
    }
}
