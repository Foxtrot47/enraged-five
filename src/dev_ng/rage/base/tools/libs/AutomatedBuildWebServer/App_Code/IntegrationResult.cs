﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.Web;

/// <summary>
/// A Cruise Control-based structure that we'll load into our own web server 
/// to gain any further information about status.
/// </summary>
public class IntegrationResult
{
    public string ProjectName;
    public string ProjectUrl;
    public string BuildCondition;
    public string Label;
    public string WorkingDirectory;
    public string ArtifactDirectory;
    public string Status;
    public DateTime StartTime;
    public DateTime RealStartTime;
    public DateTime EndTime;
    public string LastIntegrationStatus;
    public string LastSuccessfulIntegrationLabel;
    public string FailureUsers;
    public string SourceControlErrorOccurred;

    public IntegrationResult()
	{
		
	}

    public static bool Load(string stateFile, out IntegrationResult result)
    {
        StreamReader reader = new StreamReader(stateFile);

        try
        {
            XmlSerializer serializer = new XmlSerializer(typeof(IntegrationResult));
            result = (IntegrationResult)serializer.Deserialize(reader);
            reader.Close();
        }
        catch (Exception)
        {
            reader.Close();
            result = null;
            return false;
        }

        return true;
    }
}