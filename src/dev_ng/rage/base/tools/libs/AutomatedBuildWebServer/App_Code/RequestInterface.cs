﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for QueueStatus
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class RequestInterface : System.Web.Services.WebService {

    public RequestInterface()
    {
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string HelloWorld() {
        return "Hello World";
    }

    [WebMethod]
    public bool SubmitCutsceneRequests(string user, List<string> requests, string description)
    {
        try
        {
            return CutsceneRequests.SubmitRequest(user, requests, description);
        }
        catch (Exception) 
        {
            return false;
        }
    }

    [WebMethod]
    public CutsceneBuildRequest AcquireCutsceneRequests()
    {
        try
        {
            return CutsceneRequests.GetBuildRequests();
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            return null;
        }
    }

    [WebMethod]
    public bool CommitCutsceneRequestResult(int requestId, bool success, string htmlFile, int changelist, double timeElapsed)
    {
        try
        {
            return CutsceneRequests.CommitRequestResult(requestId, success, htmlFile, changelist, timeElapsed);
        }
        catch (Exception)
        {
            return false;
        }
    }

    [WebMethod]
    public List<string> GetCutsceneDescription(int buildId)
    {
        try
        {
            return CutsceneRequests.GetDescription(buildId);
        }
        catch (Exception)
        {
            return null;
        }
    }

    [WebMethod]
    public int CommitCutscenePackage(int changelist)
    {
        try
        {
           return CutsceneRequests.CommitCutscenePackage(changelist);
        }
        catch (Exception)
        {
            return -1;
        }
    }

    [WebMethod]
    public void CommitCutscenePackageResult(int id, string path, bool result)
    {
        try
        {
            CutsceneRequests.CommitCutscenePackageResult(id, path, result);
        }
        catch (Exception)
        {

        }
    }

    [WebMethod]
    public void CommitCutsceneLabelUpdate(int changelist, string filePath, int action)
    {
        try
        {
            CutsceneRequests.CommitLabelUpdate(changelist, filePath, action);
        }
        catch (Exception)
        {

        }
    }

    [WebMethod]
    public List<CutsceneLabelEntry> GetCutsceneLabelEntries()
    {
        try
        {
            return CutsceneRequests.GetCutsceneLabelEntries();
        }
        catch (Exception)
        {
            return null;
        }
    }
}
