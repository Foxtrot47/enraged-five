﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DatabaseCommon
/// </summary>
public class DatabaseCommon
{
	public DatabaseCommon() {	}

    private static string m_ServerName;
    private static string m_DatabaseName;

    public static void Initialize()
    {
        m_ServerName = BuildConfiguration.ServerName;
        m_DatabaseName = BuildConfiguration.DatabaseName;
    }

    public static string GetConnectionString()
    {
        return "Initial Catalog=" + m_DatabaseName + ";Data Source=" + m_ServerName + ";Integrated Security=SSPI;";
    }
}