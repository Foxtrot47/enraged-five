﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

public class CutsceneBuildRequest
{
    public CutsceneBuildRequest()
    {
        m_BuildID = -1;
        m_Cutscenes = new List<CutsceneRequest>();
    }

    public CutsceneBuildRequest(int id)
    {
        m_BuildID = id;
        m_Cutscenes = new List<CutsceneRequest>();
    }

    public int m_BuildID;
    public List<CutsceneRequest> m_Cutscenes; 
}

public class CutsceneRequest
{
    public CutsceneRequest()
    {
        m_CutscenePath = null;
    }

    public CutsceneRequest(int id, string cutscenePath, string username)
    {
        m_IDs = new List<int>();
        m_IDs.Add(id);
        m_CutscenePath = cutscenePath;
        m_UserName = username;
    }

    public CutsceneRequest(List<int> id, string cutscenePath, string username)
    {
        m_IDs = id;
        m_CutscenePath = cutscenePath;
        m_UserName = username;
    }

    public List<int> m_IDs;
    public string m_CutscenePath;
    public string m_UserName;
}

public enum CutsceneLabelAction
{
    SyncAll,
    SyncChangelist
}

public class CutsceneLabelEntry
{
    public DateTime m_DateTime;
    public int m_Changelist;
    public string m_Path;
    public CutsceneLabelAction m_Action;
}


public class CutscenePackageDataObject
{
    public int ID;
    public int Changelist;
}

/// <summary>
/// Summary description for CutsceneRequests
/// </summary>
public class CutsceneRequests : DatabaseCommon
{
    public CutsceneRequests()	{ }

    private static string m_RequestTableName = "CutsceneRequests";
    private static string m_IDRequestColumn = "ID";
    private static string m_BuildIdRequestColumn = "BuildID";

    private static string m_BuildTableName = "CutsceneBuilds";
    private static string m_IDBuildColumn = "ID";
    private static string m_BuildSubmissionIdColumn = "SubmissionID";
    private static string m_ResultColumn = "Result";
    private static string m_CutscenePathColumn = "CutscenePath";
    private static string m_ResultHTMLColumn = "ResultHTML";
    private static string m_ChangelistColumn = "Changelist";
    private static string m_TimeElapsedColumn = "TimeElapsed";
    
    private static string m_SubmissionTableName = "CutsceneSubmissions";
    private static string m_IDSubmissionColumn = "ID";
    private static string m_UserNameColumn = "UserName";
    private static string m_DescriptionColumn = "Description";

    private static string m_PackageTableName = "CutscenePackages";
    private static string m_PackageIDColumn = "ID";
    private static string m_PackageDateTimeColumn = "DateTime";
    private static string m_PackageChangelistColumn = "Changelist";
    private static string m_PackagePathColumn = "Path";
    private static string m_PackageResultColumn = "Result";
    private static string m_PackagePublishedColumn = "Published";

    private static string m_LabelTableName = "CutsceneLabel";
    private static string m_LabelDateTimeColumnName = "DateTime";
    private static string m_LabelChangelistColumnName = "Changelist";
    private static string m_LabelPathColumnName = "Path";
    private static string m_LabelActionColumnName = "Action";
    
    static readonly string AuthorizationString = "Approved by Sean Letts";
    static bool Lockdown = false;
    static List<string> LockedCutsceneKeywords = new List<string> { 
        "01_04_BP",
        "MID_00_STADIUM", 
        "03_02_STADIUM", 
        "03_02A_STADIUM",
        "03_02A_STADIUM_PT8",
        "MID_02B_STADIUM",
        "MID_04B_STADIUM", 
        "03_03_STADIUM",
        "MID_06_STADIUM",
        "MID_07_STADIUM",
        "MID_08A_STAD",
        "03_04B_STADIUM",
        "03_04_STADIUM",
        "03_05_STADIUM",
        "04_01_NYBAR", 
        "MID_03_NYBAR",
        "MID_01_OFFICE",
        "MID_01B_OFFICE",
        "MID_02_OFFICE",
        "MID_03_OFFICE",
        "MID_05_OFFICE",
        "06_02_OFFICE",
        "06_02_B_OFFICE",
        "MID_08_OFFICE",
        "06_04_OFFICE_CONCAT",
        "MID_13_OF",
        "06_OFFICE_END"
        };

    static int ThrottleShotLimit = 50;  //Throttle the number of cutscenes processed at a time.
    static int ThrottleConcatLimit = 20; //Throttle the number of cutscenes processed at a time.

    /// <summary>
    /// Accepts data resembling a cutscfene request to be committed to the database.
    /// </summary>
    /// <param name="user"></param>
    /// <param name="cutsceneRequests"></param>
    /// <param name="description"></param>
    /// <returns></returns>
    public static bool SubmitRequest(string user, List<string> cutsceneRequests, string description)
    {
        //If a lock has been enabled, reject these requests.
        if ( Lockdown == true && description.ToLower().Contains(AuthorizationString.ToLower()) == false)
        {
            return false;
        }

        bool checkAuthorization = false;
        foreach (string cutsceneRequest in cutsceneRequests)
        {
            foreach(string keyword in LockedCutsceneKeywords)
            {
                if (cutsceneRequest.ToLower().Contains(keyword.ToLower()) == true)
                {
                    checkAuthorization = true;
                }
            }
        }

        if (checkAuthorization && description.ToLower().Contains(AuthorizationString.ToLower()) == false)
        {
            return false;
        }

        SqlConnection sqlConnection = new SqlConnection(GetConnectionString());
        sqlConnection.Open();
        
        bool success = true;

        //Commit this submission to the database.
        int submitID = AcquireSubmissionID();

        string submitInsertQuery = "INSERT INTO " + m_SubmissionTableName + " (UserName, Description, DateTime ) VALUES (@UserName, @Description, @DateTime)";
        SqlCommand submitInsertCommand = new SqlCommand(submitInsertQuery, sqlConnection);
        SqlParameter userParameter = new SqlParameter("UserName", user);
        SqlParameter dateTimeParameter = new SqlParameter("DateTime", DateTime.UtcNow);
        SqlParameter descriptionParameter = new SqlParameter("Description", description);
        submitInsertCommand.Parameters.Add(userParameter);
        submitInsertCommand.Parameters.Add(dateTimeParameter);
        submitInsertCommand.Parameters.Add(descriptionParameter);

        if (submitInsertCommand.ExecuteNonQuery() != 1)
            success = false;

        foreach (string request in cutsceneRequests)
        {
            string insertQuery = "INSERT INTO " + m_RequestTableName + " (SubmissionID, CutscenePath) VALUES (@SubmissionID, @CutscenePath)";
            SqlCommand insertCommand = new SqlCommand(insertQuery, sqlConnection);
            SqlParameter submissionIDParameter = new SqlParameter("SubmissionID", submitID);
            SqlParameter cutscenePathParameter = new SqlParameter("CutscenePath", request);
            insertCommand.Parameters.Add(submissionIDParameter);
            insertCommand.Parameters.Add(cutscenePathParameter);

            if (insertCommand.ExecuteNonQuery() != 1)
                success = false;
        }

        sqlConnection.Close();
        return success;
    }

    /// <summary>
    /// Acquires a new identification number for the submission.
    /// </summary>
    /// <returns></returns>
    public static int AcquireSubmissionID()
    {
        string lastSubmissionIDQuery = "SELECT TOP 1 ID FROM " + m_SubmissionTableName + " ORDER BY ID DESC";

        SqlConnection sqlConnection = new SqlConnection(GetConnectionString());
        sqlConnection.Open();

        SqlCommand idQueryCommand = new SqlCommand(lastSubmissionIDQuery, sqlConnection);

        SqlDataReader reader = idQueryCommand.ExecuteReader();
        while (reader.Read() == true)
        {
            int id = (int)reader[m_IDSubmissionColumn];
            sqlConnection.Close();
            return (id + 1);
        }

        sqlConnection.Close();
        return 0;
    }

    /// <summary>
    /// Acquires all outstanding cutscene requests.
    /// </summary>
    /// <returns></returns>
    public static CutsceneBuildRequest GetBuildRequests()
    {
        SqlConnection sqlConnection = new SqlConnection(GetConnectionString());
        sqlConnection.Open();

        string selectQuery = "SELECT CutsceneRequests.ID, CutsceneRequests.CutscenePath, CutsceneSubmissions.UserName "
                + "FROM CutsceneRequests INNER JOIN "
                + "CutsceneSubmissions ON CutsceneRequests.SubmissionID = CutsceneSubmissions.ID "
                + "WHERE Result IS NULL ORDER BY " + m_BuildSubmissionIdColumn;

        //string selectQuery = "SELECT * FROM " + m_RequestTableName + " WHERE Result IS NULL ORDER BY " + m_BuildSubmissionIdColumn;
        SqlCommand selectCommand = new SqlCommand(selectQuery, sqlConnection);

        SqlDataReader reader = selectCommand.ExecuteReader();
        List<CutsceneRequest> totalRequests = new List<CutsceneRequest>();
        List<CutsceneRequest> allRequests = new List<CutsceneRequest>();
        int shotCount = 0;
        int concatCount = 0;
        while (reader.Read() == true)
        {
            int id = (int)reader[m_IDRequestColumn];
            string cutscenePath = reader[m_CutscenePathColumn] as string;
            string userName = reader[m_UserNameColumn] as string;  //TODO:  Use to prioritze user requests.

            totalRequests.Add(new CutsceneRequest(id, cutscenePath, userName));
        }

        //Accumulate all of the shots first.
        foreach (CutsceneRequest request in totalRequests)
        {
            if (request.m_CutscenePath.ToLower().Contains(".cutxml") == true
                || request.m_CutscenePath.ToLower().Contains(".mergelist") == true)
            {
                if (ThrottleShotLimit != -1 && shotCount >= ThrottleShotLimit)
                {
                    break;
                }

                shotCount++;
            }

            allRequests.Add(request);
        }

        if (shotCount == 0)
        {
            //Only concatenate if there are no shots to be processed.
            foreach (CutsceneRequest request in totalRequests)
            {
                if (request.m_CutscenePath.ToLower().Contains(".cutlist") == true)
                {
                    if (ThrottleConcatLimit != -1 && concatCount >= ThrottleConcatLimit)
                    {
                        break;
                    }

                    concatCount++;
                }

                allRequests.Add(request);
            }
        }

        if (allRequests.Count > 0)
        {
            int buildID = AcquireNewBuildId();

            CutsceneBuildRequest buildRequest = new CutsceneBuildRequest(buildID);

            foreach (CutsceneRequest request in allRequests)
            {
                //Commit the build ID to each of these requests.
                SqlConnection updateSqlConnection = new SqlConnection(GetConnectionString());
                updateSqlConnection.Open();

                foreach (int requestId in request.m_IDs)
                {
                    string updateQuery = "UPDATE " + m_RequestTableName + " SET " + m_BuildIdRequestColumn + "=@BuildID WHERE " + m_IDRequestColumn + "=@ID";
                    SqlCommand updateCommand = new SqlCommand(updateQuery, updateSqlConnection);
                    SqlParameter buildIDParameter = new SqlParameter("@BuildID", buildID);
                    SqlParameter idParameter = new SqlParameter("@ID", requestId);
                    updateCommand.Parameters.Add(buildIDParameter);
                    updateCommand.Parameters.Add(idParameter);

                    SqlDataReader updateReader = updateCommand.ExecuteReader();
                    updateReader.Close();
                }

                updateSqlConnection.Close();

                //Eliminate any duplicate requests.
                CutsceneRequest duplicateRequest = null;
                foreach (CutsceneRequest uniqueRequest in buildRequest.m_Cutscenes)
                {
                    if (String.Compare(uniqueRequest.m_CutscenePath, request.m_CutscenePath, true) == 0)
                    {
                        duplicateRequest = uniqueRequest;
                    }
                }

                if (duplicateRequest == null)
                {
                    buildRequest.m_Cutscenes.Add(request);
                }
                else
                {
                    duplicateRequest.m_IDs.AddRange(request.m_IDs);
                }
            }

            sqlConnection.Close();
            return buildRequest;
        }

        return null;
    }


    /// <summary>
    /// Commits the build request to the database.  The build request is a group of cutscene requests
    /// to be processed at once.
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    public static int AcquireNewBuildId()
    {
        SqlConnection sqlConnection = new SqlConnection(GetConnectionString());
        sqlConnection.Open();

        string insertQuery = "INSERT INTO " + m_BuildTableName + " (StartTime) VALUES (@StartTime)";
        SqlCommand insertCommand = new SqlCommand(insertQuery, sqlConnection);
        SqlParameter dateTimeParameter = new SqlParameter("StartTime", DateTime.UtcNow);
        insertCommand.Parameters.Add(dateTimeParameter);
        insertCommand.ExecuteNonQuery();

        //Get the new ID.
        string lastBuildIDQuery = "SELECT TOP 1 ID FROM " + m_BuildTableName + " ORDER BY ID DESC";

        SqlCommand idQueryCommand = new SqlCommand(lastBuildIDQuery, sqlConnection);

        SqlDataReader reader = idQueryCommand.ExecuteReader();
        while (reader.Read() == true)
        {
            int id = (int)reader[m_IDBuildColumn];

            sqlConnection.Close();
            return id;
        }

        sqlConnection.Close();
        return 0;
    }

    /// <summary>
    /// Commits data concerning a particular request's completion.
    /// </summary>
    /// <param name="id"></param>
    /// <param name="success"></param>
    /// <param name="htmlFile"></param>
    /// <returns></returns>
    public static bool CommitRequestResult(int id, bool success, string htmlFile, int changelist, double secondsElapsed)
    {
        string updateQuery = "UPDATE " + m_RequestTableName + " SET " + m_ResultColumn + "=@Result, " + m_ResultHTMLColumn + "=@ResultHTML, " + m_ChangelistColumn + "=@SubmittedChangelist, " + m_TimeElapsedColumn + "=@TimeElapsed WHERE " + m_IDRequestColumn + "=@ID";

        TimeSpan timeSpan = new TimeSpan(0, 0, (int) secondsElapsed);

        SqlConnection updateSqlConnection = new SqlConnection(GetConnectionString());
        updateSqlConnection.Open();
        SqlCommand updateCommand = new SqlCommand(updateQuery, updateSqlConnection);
        SqlParameter resultParameter = new SqlParameter("@Result", success);
        SqlParameter resultHTMLParameter = new SqlParameter("@ResultHTML", htmlFile);
        SqlParameter changelistParameter = new SqlParameter("@SubmittedChangelist", changelist);
        SqlParameter idParameter = new SqlParameter("@ID", id);
        updateCommand.Parameters.Add(resultParameter);
        updateCommand.Parameters.Add(resultHTMLParameter);
        updateCommand.Parameters.Add(changelistParameter);
        updateCommand.Parameters.Add(idParameter);
        updateCommand.Parameters.Add(timeSpan);

        SqlDataReader updateReader = updateCommand.ExecuteReader();
        updateReader.Close();
        updateSqlConnection.Close();

        return true;
    }

    /// <summary>
    /// Acquires the group of descriptions for all the requests
    /// in the associated build identification number.
    /// </summary>
    /// <param name="buildId"></param>
    /// <returns></returns>
    public static List<string> GetDescription(int buildId)
    {
        List<string> totalDescription = new List<string>();

        string descriptionQuery = "SELECT DISTINCT CutsceneSubmissions.UserName, CutsceneSubmissions.Description";
        descriptionQuery += " FROM " + m_RequestTableName + " INNER JOIN " + m_BuildTableName + " ON CutsceneBuilds.ID = CutsceneRequests.BuildID";
        descriptionQuery += " INNER JOIN " + m_SubmissionTableName + " ON CutsceneSubmissions.ID = CutsceneRequests.SubmissionID";
        descriptionQuery += " WHERE (CutsceneRequests.BuildID = @BuildID)";
        descriptionQuery += " ORDER BY CutsceneSubmissions.UserName ASC";

        SqlConnection sqlConnection = new SqlConnection(GetConnectionString());
        sqlConnection.Open();
        SqlCommand query = new SqlCommand(descriptionQuery, sqlConnection);
        SqlParameter idParameter = new SqlParameter("@BuildID", buildId);
        query.Parameters.Add(idParameter);

        SqlDataReader reader = query.ExecuteReader();

        while (reader.Read() == true)
        {
            string userName = reader[m_UserNameColumn] as string;
            string description = reader[m_DescriptionColumn] as string;

            totalDescription.Add(userName + ": " + description);
        }

        reader.Close();
        sqlConnection.Close();

        return totalDescription;
    }
    
    /// <summary>
    /// Reserves a row for a cutscene package, designated by its synced changelist.
    /// </summary>
    /// <param name="changelist"></param>
    /// <returns></returns>
    public static int CommitCutscenePackage(int changelist)
    {
        SqlConnection sqlConnection = new SqlConnection(GetConnectionString());
        sqlConnection.Open();

        string insertQuery = "INSERT INTO " + m_PackageTableName + " (DateTime, Changelist) VALUES (@DateTime, @Changelist)";
        SqlCommand insertCommand = new SqlCommand(insertQuery, sqlConnection);
        SqlParameter dateTimeParameter = new SqlParameter("DateTime", DateTime.UtcNow);
        SqlParameter changelistParameter = new SqlParameter("Changelist", changelist);
        
        insertCommand.Parameters.Add(dateTimeParameter);
        insertCommand.Parameters.Add(changelistParameter);
        insertCommand.ExecuteNonQuery();

        string lastBuildIDQuery = "SELECT TOP 1 ID FROM " + m_PackageTableName + " ORDER BY ID DESC";
        SqlCommand buildIDCommand = new SqlCommand(lastBuildIDQuery, sqlConnection);

        SqlDataReader reader = buildIDCommand.ExecuteReader();
        while (reader.Read() == true)
        {
            int id = (int)reader[m_PackageIDColumn];

            sqlConnection.Close();
            return id;
        }

        //TODO:  Add code to mark any other "in progress" entries as failed or incomplete?

        sqlConnection.Close();
        return -1;
    }

    /// <summary>
    /// Commits to the database the result of a cutscene package process.  Publish the path to copy it.
    /// </summary>
    /// <param name="id"></param>
    /// <param name="path"></param>
    /// <param name="result"></param>
    public static void CommitCutscenePackageResult(int id, string path, bool result)
    {
        string updateQuery = "UPDATE " + m_PackageTableName + " SET " + m_PackageResultColumn + "=@Result, " + m_PackagePathColumn + "=@Path WHERE " + m_IDRequestColumn + "=@ID";

        SqlConnection updateSqlConnection = new SqlConnection(GetConnectionString());
        updateSqlConnection.Open();
        SqlCommand updateCommand = new SqlCommand(updateQuery, updateSqlConnection);
        SqlParameter resultParameter = new SqlParameter("@Result", result);
        SqlParameter idParameter = new SqlParameter("@ID", id);
        SqlParameter pathParameter = new SqlParameter("@Path", path);
        updateCommand.Parameters.Add(resultParameter);
        updateCommand.Parameters.Add(pathParameter);
        updateCommand.Parameters.Add(idParameter);

        SqlDataReader updateReader = updateCommand.ExecuteReader();
        updateReader.Close();
        updateSqlConnection.Close();
    }

    /// <summary>
    /// Mark a cutscene from a particular changelist as published.
    /// </summary>
    /// <param name="changelist"></param>
    public static void PublishCutscenePackage(int changelist)
    {
        string updateQuery = "UPDATE " + m_PackageTableName + " SET " + m_PackagePublishedColumn + "=@Published WHERE " + m_PackageChangelistColumn + "=@Changelist";

        SqlConnection updateSqlConnection = new SqlConnection(GetConnectionString());
        updateSqlConnection.Open();
        SqlCommand updateCommand = new SqlCommand(updateQuery, updateSqlConnection);
        SqlParameter publishedParameter = new SqlParameter("@Published", true);
        SqlParameter changelistParameter = new SqlParameter("@Changelist", changelist);
        updateCommand.Parameters.Add(publishedParameter);
        updateCommand.Parameters.Add(changelistParameter);

        SqlDataReader updateReader = updateCommand.ExecuteReader();
        updateReader.Close();
        updateSqlConnection.Close();
    }

    /// <summary>
    /// Gets a list of package data since the last published changelist.
    /// </summary>
    /// <param name="lastPublishedChangelist"></param>
    /// <returns></returns>
    public static List<CutscenePackageDataObject> GetPackageInfo(int lastPublishedChangelist)
    {
        string selectQuery = "SELECT ID, Changelist FROM CutscenePackages WHERE Changelist > @LastPublishedChangelist ORDER BY ID DESC";
        SqlConnection sqlConnection = new SqlConnection(CutsceneRequests.GetConnectionString());

        sqlConnection.Open();

        SqlCommand selectCommand = new SqlCommand(selectQuery, sqlConnection);
        selectCommand.Parameters.Add("@LastPublishedChangelist", lastPublishedChangelist);

        SqlDataReader reader = selectCommand.ExecuteReader();

        List<CutscenePackageDataObject> cutscenePackages = new List<CutscenePackageDataObject>();
        while (reader.Read() == true)
        {
            CutscenePackageDataObject cutscenePackage = new CutscenePackageDataObject();
            cutscenePackage.ID = (int) reader[m_PackageIDColumn];
            cutscenePackage.Changelist = (int) reader[m_PackageChangelistColumn];

            cutscenePackages.Add(cutscenePackage);
        }

        return cutscenePackages;
    }

    /// <summary>
    /// Add a new step to the label.
    /// </summary>
    /// <param name="changelist"></param>
    /// <param name="filePath"></param>
    /// <param name="action"></param>
    public static void CommitLabelUpdate(int changelist, string filePath, int action)
    {
        SqlConnection sqlConnection = new SqlConnection(GetConnectionString());
        sqlConnection.Open();

        string insertQuery = "INSERT INTO " + m_LabelTableName + " (DateTime, Changelist, Path) VALUES (@DateTime, @Changelist, @Path)";
        SqlCommand insertCommand = new SqlCommand(insertQuery, sqlConnection);
        SqlParameter dateTimeParameter = new SqlParameter("DateTime", DateTime.UtcNow);
        SqlParameter changelistParameter = new SqlParameter("Changelist", changelist);
        SqlParameter pathParameter = new SqlParameter("Path", filePath);
        SqlParameter actionParameter = new SqlParameter("Action", action);

        insertCommand.Parameters.Add(dateTimeParameter);
        insertCommand.Parameters.Add(changelistParameter);
        insertCommand.Parameters.Add(pathParameter);
        insertCommand.Parameters.Add(actionParameter);
        insertCommand.ExecuteNonQuery();

        sqlConnection.Close();
    }
    
    /// <summary>
    /// Acquire the list of all label entries.
    /// </summary>
    /// <returns></returns>
    public static List<CutsceneLabelEntry> GetCutsceneLabelEntries()
    {
        string selectQuery = "SELECT * FROM " + m_LabelTableName + " ORDER BY " + m_LabelDateTimeColumnName + " ASC";
        SqlConnection sqlConnection = new SqlConnection(CutsceneRequests.GetConnectionString());

        sqlConnection.Open();

        SqlCommand selectCommand = new SqlCommand(selectQuery, sqlConnection);
        SqlDataReader reader = selectCommand.ExecuteReader();

        List<CutsceneLabelEntry> cutsceneLabelEntries = new List<CutsceneLabelEntry>();
        while (reader.Read() == true)
        {
            CutsceneLabelEntry cutsceneLabelEntry = new CutsceneLabelEntry();
            cutsceneLabelEntry.m_DateTime = (DateTime)reader[m_LabelDateTimeColumnName];
            cutsceneLabelEntry.m_Changelist = (int)reader[m_LabelChangelistColumnName];
            cutsceneLabelEntry.m_Path = (string)reader[m_LabelPathColumnName];
            cutsceneLabelEntry.m_Action = (CutsceneLabelAction)reader[m_LabelActionColumnName];

            cutsceneLabelEntries.Add(cutsceneLabelEntry);
        }

        return cutsceneLabelEntries;
    }
}
