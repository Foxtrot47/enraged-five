﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.XPath;

using AutomatedBuildShared;

public class ProjectStateDataObject
{
    public string m_Changelist;
    public string m_UserName;
    public string m_Description;

    public ProjectStateDataObject(string changelist, string username, string description)
    {
        m_Changelist = changelist;
        m_UserName = username;
        m_Description = description;
    }
}

/// <summary>
/// A generic object that will parse a build folder and generate
/// displayable data to the web control.
/// </summary>
public class BuildSummaryDataObject
{
    /// <summary>
    /// Parses the project state file for all project state data.
    /// </summary>
    /// <param name="projectStateFile"></param>
    /// <returns></returns>
    public static Dictionary<int, List<ProjectStateDataObject>> GetProjectStateInfo(string projectStateFile)
    {
        Dictionary<int, List<ProjectStateDataObject>> projectStateDataList = new Dictionary<int, List<ProjectStateDataObject>>();
        if (File.Exists(projectStateFile) == false)
            return projectStateDataList;

        XPathDocument projectStateDoc = new XPathDocument(projectStateFile);
        XPathNavigator nav = projectStateDoc.CreateNavigator();
        string versionXPathQuery = "/project_state/work_units/work_unit";
        XPathNodeIterator iter = nav.Select(versionXPathQuery);
        if (iter.Count > 0)
        {
            while (iter.MoveNext())
            {
                XPathNodeIterator versionIter = iter.Current.Select("@version");
                versionIter.MoveNext();
                int version = Int32.Parse(versionIter.Current.Value);
                projectStateDataList.Add(version, new List<ProjectStateDataObject>());

                XPathNodeIterator changeIter = iter.Current.Select(versionXPathQuery + "[@version='" + version + "']/changes/change");
                if (changeIter.Count == 0)
                {
                    ProjectStateDataObject newData = new ProjectStateDataObject("--", "--", "Force Build");
                    projectStateDataList[version].Add(newData);
                }
                else
                {
                    while (changeIter.MoveNext())
                    {
                        XPathNodeIterator numberIter = changeIter.Current.Select("@number");
                        numberIter.MoveNext();
                        int changelist = Int32.Parse(numberIter.Current.Value);

                        XPathNodeIterator commentIter = changeIter.Current.Select("@comment");
                        commentIter.MoveNext();
                        string description = commentIter.Current.Value;

                        XPathNodeIterator userIter = changeIter.Current.Select("@user");
                        userIter.MoveNext();
                        string user = userIter.Current.Value;

                        ProjectStateDataObject newData = new ProjectStateDataObject(changelist.ToString(), user, description);
                        projectStateDataList[version].Add(newData);
                    }
                }
            }
        }

        return projectStateDataList;
    }

    /// <summary>
    /// Acquires all project entries for a particular build.
    /// </summary>
    /// <param name="projectStateFile"></param>
    /// <param name="version"></param>
    /// <returns></returns>
    public static List<ProjectStateDataObject> GetProjectStateInfo(string projectStateFile, int version)
    {
        List<ProjectStateDataObject> projectStateDataList = new List<ProjectStateDataObject>();
        if (File.Exists(projectStateFile) == false)
            return projectStateDataList;

        XPathDocument projectStateDoc = new XPathDocument(projectStateFile);
        XPathNavigator nav = projectStateDoc.CreateNavigator();
        string versionXPathQuery = "/project_state/work_units/work_unit[@version='" + version + "']";
        XPathNodeIterator iter = nav.Select(versionXPathQuery);
        if (iter.Count > 0)
        {
            iter.MoveNext();

            XPathNodeIterator changeIter = iter.Current.Select(versionXPathQuery + "/changes/change");
            if (changeIter.Count == 0)
            {
                ProjectStateDataObject newData = new ProjectStateDataObject("--", "--", "Force Build");
                projectStateDataList.Add(newData);              
            }
            else
            {
                while (changeIter.MoveNext())
                {
                    XPathNodeIterator numberIter = changeIter.Current.Select("@number");
                    numberIter.MoveNext();
                    int changelist =  Int32.Parse(numberIter.Current.Value);

                    XPathNodeIterator commentIter = changeIter.Current.Select("@comment");
                    commentIter.MoveNext();
                    string description = commentIter.Current.Value;

                    XPathNodeIterator userIter = changeIter.Current.Select("@user");
                    userIter.MoveNext();
                    string user = userIter.Current.Value;

                    ProjectStateDataObject newData = new ProjectStateDataObject(changelist.ToString(), user, description);
                    projectStateDataList.Add(newData);
                }
            }
        }

        return projectStateDataList;
    }

    /// <summary>
    /// Acquires all build states both currently pending and past processed.
    /// </summary>
    /// <param name="numRecords"></param>
    /// <param name="buildItemObject"></param>
    /// <returns></returns>
    public static DataTable GetRequests(int numRecords, object buildItemObject)
    {
        BuildItem item = (BuildItem)buildItemObject;
        DataTable dataTable = new DataTable();

        //Check for the existence of a project_state.xml file.  
        //If so, then we will have Perforce information.
        string projectStateFile = Path.Combine(@"N:\RSGSAN\Builders\CruiseControl\live", item.DirectoryName, "project_state.xml");

        dataTable.Columns.Add("Changelist");
        dataTable.Columns.Add("User");
        dataTable.Columns.Add("Description");
    
        ///Read the current status report to get the current build status.
        string buildRootDirectory = Path.Combine(BuildInfoService.RootUNCDirectory, item.DirectoryName);
        string statusFile = Path.Combine(buildRootDirectory, "statusreport.xml");

        StatusReport statusReport;
        if (StatusReport.Load(statusFile, out statusReport) == true)
        {
            foreach (StatusTaskInfo statusTaskInfo in statusReport.StatusTaskInfoList)
            {
                dataTable.Columns.Add(statusTaskInfo.Name);
            }
        }

        DataRow newRow = dataTable.NewRow();

        ///Read the project state XML to determine any pending builds.
        Dictionary<int, List<ProjectStateDataObject>> projectStates = GetProjectStateInfo(projectStateFile);

        List<ProjectStateDataObject> pendingProjectData = projectStates[statusReport.BuildVersion];
        foreach (ProjectStateDataObject pendingProjectDataObject in pendingProjectData)
        {
            newRow["Changelist"] = pendingProjectDataObject.m_Changelist;
            newRow["Description"] = pendingProjectDataObject.m_Description; ;
            newRow["User"] = pendingProjectDataObject.m_UserName;

            foreach (StatusTaskInfo statusTaskInfo in statusReport.StatusTaskInfoList)
            {
                if (statusTaskInfo.Dispatched == false)
                {
                    newRow[statusTaskInfo.Name] = statusTaskInfo.Success ? "Success" : "Failed";
                }
                else
                {
                    newRow[statusTaskInfo.Name] = "Dispatched";
                }   
            }
        }

        dataTable.Rows.Add(newRow);


        Dictionary<int, SummaryReport> summaries = new Dictionary<int, SummaryReport>();

        string summaryXMLFileName = "summary.xml";
        string[] summaryXMLFiles = Directory.GetFiles(buildRootDirectory, summaryXMLFileName, SearchOption.AllDirectories);
        foreach (string summaryXMLFile in summaryXMLFiles)
        {
            SummaryReport report;
            if ( SummaryReport.Load(summaryXMLFile, out report) == true )
            {
                if ( summaries.ContainsKey(report.Version) == false )
                    summaries.Add(report.Version, report);
            }
        }


        foreach (KeyValuePair<int, List<ProjectStateDataObject>> projectStatePair in projectStates)
        {
            List<ProjectStateDataObject> projectState = projectStatePair.Value;
            foreach (ProjectStateDataObject projectStateDataObject in projectState)
            {
                newRow = dataTable.NewRow();
                newRow["Changelist"] = projectStateDataObject.m_Changelist;
                newRow["Description"] = projectStateDataObject.m_Description;
                newRow["User"] = projectStateDataObject.m_UserName;

                if (summaries.ContainsKey(projectStatePair.Key) == true)
                {
                    foreach (SummaryTaskInfo summaryTaskInfo in summaries[projectStatePair.Key].TaskInfo)
                    {
                        newRow[summaryTaskInfo.Name] = summaryTaskInfo.Success ? "Success" : "Failed";
                    }
                }

                dataTable.Rows.Add(newRow);
            }
        }

        //Now go through every row.  If the value is blank, add "Unknown" to it since we can
        //only assume that the build wasn't run.
        foreach (DataColumn column in dataTable.Columns)
        {
            foreach (DataRow row in dataTable.Rows)
            {
                DataRow dataRow = (DataRow)row[column];
            }
        }

        return dataTable;
    }
}