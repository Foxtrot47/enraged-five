﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Web;
using System.Xml.XPath;

public class ProjectsDatabase : DatabaseCommon
{
    public static string m_TableName = "Projects";
    public static string m_IDColumnName = "ID";
    public static string m_ProjectNameColumnName = "ProjectName";

    public ProjectsDatabase() { }

    public static int GetProjectID(string projectName)
    {
        projectName = projectName.ToUpper(); //HACK:  Until I find out how to create a case insensitive compare in an SQL query.

        string projectIDQuery = "SELECT * FROM " + m_TableName + " WHERE " + m_ProjectNameColumnName + "=@ProjectName";

        SqlConnection sqlConnection = new SqlConnection(GetConnectionString());
        sqlConnection.Open();

        SqlCommand idQueryCommand = new SqlCommand(projectIDQuery, sqlConnection);
        SqlParameter projectNameParameter = new SqlParameter("@ProjectName", projectName);
        idQueryCommand.Parameters.Add(projectNameParameter);

        SqlDataReader reader = idQueryCommand.ExecuteReader();
        while (reader.Read() == true)
        {
            int id = (int)reader[m_IDColumnName];
            sqlConnection.Close();
            return id;
        }
        reader.Close();

        projectIDQuery = "INSERT INTO " + m_TableName + " (Name) VALUES(@ProjectName)";
        SqlCommand insertQueryCommand = new SqlCommand(projectIDQuery, sqlConnection);
        projectNameParameter = new SqlParameter("@ProjectName", projectName);
        insertQueryCommand.Parameters.Add(projectNameParameter);

        if (insertQueryCommand.ExecuteNonQuery() == 0)
            return -1;

        int newId = GetProjectID(projectName);

        sqlConnection.Close();
        return -1;
    }
}

public class StatisticsTypeDatabase : DatabaseCommon
{
    public static string m_TableName = "UsageStatisticsTypes";
    public static string m_IDColumn = "ID";
    public static string m_NameColumn = "Name";

    public StatisticsTypeDatabase() { }

    public static int GetStatisticTypeID(string statisticName)
    {
        string statisticIDQuery = "SELECT * FROM " + m_TableName + " WHERE " + m_NameColumn + "=@StatisticName";

        SqlConnection sqlConnection = new SqlConnection(GetConnectionString());
        sqlConnection.Open();

        SqlCommand idQueryCommand = new SqlCommand(statisticIDQuery, sqlConnection);
        SqlParameter statisticsNameParameter = new SqlParameter("@StatisticName", statisticName);
        idQueryCommand.Parameters.Add(statisticsNameParameter);

        SqlDataReader reader = idQueryCommand.ExecuteReader();
        while (reader.Read() == true)
        {
            int id = (int)reader[m_IDColumn];
            sqlConnection.Close();
            return id;
        }
        reader.Close();

        //Otherwise, the statistic doesn't exist, add it.
        statisticIDQuery = "INSERT INTO " + m_TableName + " (Name) VALUES(@StatisticName)";
        SqlCommand insertQueryCommand = new SqlCommand(statisticIDQuery, sqlConnection);
        statisticsNameParameter = new SqlParameter("@StatisticName", statisticName);
        insertQueryCommand.Parameters.Add(statisticsNameParameter);

        if ( insertQueryCommand.ExecuteNonQuery() == 0 ) 
            return -1;

        int newId = GetStatisticTypeID(statisticName);
        sqlConnection.Close();
        return newId;
    }
}

public class StatisticsInfoDatabase : DatabaseCommon
{
    public static string m_TableName = "StatisticsInfo";
    public static string m_IDColumn = "ID";
    private static string m_ProjectID = "ProjectID";
    private static string m_StatisticsTypeID = "TypeID";
    public static string m_MachineNameColumn = "MachineName";
    public static string m_FileNameColumn = "FileName";
    public static string m_DateTimeColumn = "DateTime";

    public StatisticsInfoDatabase()  { }

    public static int GetStatisticInfoID(string project, string statisticType, string machineName, string fileName, DateTime dateTime)
    {
        int projectID = ProjectsDatabase.GetProjectID(project);
        int statisticTypeID = StatisticsTypeDatabase.GetStatisticTypeID(statisticType);

        SqlConnection sqlConnection = new SqlConnection(GetConnectionString());
        sqlConnection.Open();

        string statisticIDQuery = "INSERT INTO " + m_TableName + " (ProjectID, TypeID, MachineName, FileName, DateTime) VALUES(@ProjectID, @TypeID, @MachineName, @FileName, @DateTime)";
        SqlCommand insertQueryCommand = new SqlCommand(statisticIDQuery, sqlConnection);
        SqlParameter projectIDParameter = new SqlParameter("@ProjectID", projectID);
        SqlParameter typeIDParameter = new SqlParameter("@TypeID", statisticTypeID);
        SqlParameter machineNameParameter = new SqlParameter("@MachineName", machineName);
        SqlParameter fileNameParameter = new SqlParameter("@FileName", fileName);
        SqlParameter dateTimeParameter = new SqlParameter("@DateTime", dateTime);
        insertQueryCommand.Parameters.Add(projectIDParameter);
        insertQueryCommand.Parameters.Add(typeIDParameter);
        insertQueryCommand.Parameters.Add(machineNameParameter);
        insertQueryCommand.Parameters.Add(fileNameParameter);
        insertQueryCommand.Parameters.Add(dateTimeParameter);

        if (insertQueryCommand.ExecuteNonQuery() == 0)
        {
            //Log error.
            return -1;
        }

        string lastSubmissionIDQuery = "SELECT TOP 1 ID FROM " + m_TableName + " ORDER BY ID DESC";
        SqlCommand idQueryCommand = new SqlCommand(lastSubmissionIDQuery, sqlConnection);

        int id = -1;
        SqlDataReader reader = idQueryCommand.ExecuteReader();
        while (reader.Read() == true)
        {
            id = (int)reader[m_IDColumn];
        }

        sqlConnection.Close();
        return id;
    }
}

public class UsageStatisticsDatabase : DatabaseCommon
{
    public static string m_TableName = "ToolUsageStatistics2";
    public static string m_IDColumn = "ID";
    public static string m_ProjectIDColumn = "ProjectID";
    public static string m_TypeIDColumn = "TypeID";
    public static string m_DateTimeColumn = "DateTime";
    public static string m_NoteColumn = "Note";
    public static string m_MachineNameColumn = "MachineName";

    public UsageStatisticsDatabase() { }

    public static int GetUsageInfoID(string project, string statisticType, string machineName, string note, DateTime dateTime)
    {
        int projectID = ProjectsDatabase.GetProjectID(project);
        int statisticTypeID = StatisticsTypeDatabase.GetStatisticTypeID(statisticType);

        SqlConnection sqlConnection = new SqlConnection(GetConnectionString());
        sqlConnection.Open();

        string statisticIDQuery = "INSERT INTO " + m_TableName + " (" + m_ProjectIDColumn + ", " + m_TypeIDColumn + ", " + m_MachineNameColumn + ", " + m_DateTimeColumn;
        if (note != null)
        {
            statisticIDQuery += ", " + m_NoteColumn; 
        }

        statisticIDQuery += ") VALUES(@ProjectID, @TypeID, @MachineName, @DateTime";

        if (note != null)
        {
            statisticIDQuery += ", @Note";
        }

        statisticIDQuery += ")";

        SqlCommand insertQueryCommand = new SqlCommand(statisticIDQuery, sqlConnection);
        SqlParameter projectIDParameter = new SqlParameter("@ProjectID", projectID);
        SqlParameter typeIDParameter = new SqlParameter("@TypeID", statisticTypeID);
        SqlParameter machineNameParameter = new SqlParameter("@MachineName", machineName);
        SqlParameter dateTimeParameter = new SqlParameter("@DateTime", dateTime);
        insertQueryCommand.Parameters.Add(projectIDParameter);
        insertQueryCommand.Parameters.Add(typeIDParameter);
        insertQueryCommand.Parameters.Add(machineNameParameter);
        insertQueryCommand.Parameters.Add(dateTimeParameter);

        if (note != null)
        {
            SqlParameter noteParameter = new SqlParameter("@Note", note);
            insertQueryCommand.Parameters.Add(noteParameter);
        }

        if (insertQueryCommand.ExecuteNonQuery() == 0)
        {
            //Log error.
            return -1;
        }

        string lastSubmissionIDQuery = "SELECT TOP 1 ID FROM " + m_TableName + " ORDER BY ID DESC";
        SqlCommand idQueryCommand = new SqlCommand(lastSubmissionIDQuery, sqlConnection);

        int id = -1;
        SqlDataReader reader = idQueryCommand.ExecuteReader();
        while (reader.Read() == true)
        {
            id = (int)reader[m_IDColumn];
        }

        sqlConnection.Close();
        return id;
    }
}

/// <summary>
/// Summary description for UsageStatistics
/// </summary>
public class UsageStatistics : DatabaseCommon
{
	public UsageStatistics() { }

    private static string m_ToolProfileStatisticsTableName = "ToolProfileStatistics";
    private static string m_StatisticsID = "StatisticID";
    private static string m_TimeElapsedColumnName = "TimeElapsed";
    private static string m_Scope = "Scope";

    private static string m_ToolUsageStatisticsTableName = "ToolUsageStatistics";
    private static string m_UsageID = "UsageID";

    /// <summary>
    /// 
    /// </summary>
    /// <param name="machineName"></param>
    /// <param name="filename"></param>
    /// <returns></returns>
    public static int CreateStatisticInfo(string project, string type, string machineName, string filename)
    {
        return StatisticsInfoDatabase.GetStatisticInfoID(project, type, machineName, filename, DateTime.UtcNow);
    }

    /// <summary>
    /// Commits a tool profile statistic to the database.  These statistics display a process taking place in the tool chain.
    /// </summary>
    /// <param name="application"></param>
    /// <param name="scope"></param>
    /// <param name="filename"></param>
    /// <param name="machineName"></param>
    /// <param name="time"></param>
    public static int CommitToolProfileStatistic(int statisticID, string scope, double time)
    {
        string insertQuery = "INSERT INTO " + m_ToolProfileStatisticsTableName + " (" + m_StatisticsID + ", " + m_Scope + ", " + m_TimeElapsedColumnName + ") "
            + "VALUES(@StatisticID, @Scope, @TimeElapsed)";

        TimeSpan timeSpan = new TimeSpan(0, 0, (int)time);

        SqlConnection sqlConnection = new SqlConnection(GetConnectionString());
        sqlConnection.Open();
        SqlCommand insertCommand = new SqlCommand(insertQuery, sqlConnection);
        SqlParameter statisticIDParameter = new SqlParameter("@StatisticID", statisticID);
        SqlParameter scopeParameter = new SqlParameter("@Scope", scope);
        SqlParameter timeElapsedParameter = new SqlParameter("@TimeElapsed", timeSpan);
        insertCommand.Parameters.Add(statisticIDParameter);
        insertCommand.Parameters.Add(scopeParameter);
        insertCommand.Parameters.Add(timeElapsedParameter);

        SqlDataReader insertReader = insertCommand.ExecuteReader();
        insertReader.Close();
        sqlConnection.Close();

        return statisticID;
    }

    /// <summary>
    /// Commits a tool usage statistic.  This displays information about who is using certain tools and how.
    /// </summary>
    /// <param name="application"></param>
    /// <param name="scope"></param>
    /// <param name="machineName"></param>
    /// <param name="note"></param>
    public static int CommitToolUsageStatistic(string project, string statisticType, string machineName, string note)
    {
        int usageID = UsageStatisticsDatabase.GetUsageInfoID(project, statisticType, machineName, note, DateTime.UtcNow);
        return usageID;
    }
}