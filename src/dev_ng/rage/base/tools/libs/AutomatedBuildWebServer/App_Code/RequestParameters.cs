﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for RequestParameters
/// </summary>
public class RequestParameters
{
	public RequestParameters()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static readonly string BuildNameArgument = "name";
    public static readonly string UserNameArgument = "username";
    public static readonly string NumRecordsArgument = "numrecords";
    public static readonly string SelectedBuildArgument = "selectedbuild";
}