﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
using System.Xml.XPath;

using AutomatedBuildShared;

[Serializable]
public class BuildStatistics
{
    public TimeSpan TotalTimeOverall;
    public TimeSpan TotalTimeTaken;

    public BuildStatistics()
    {
        TotalTimeOverall = new TimeSpan();
        TotalTimeTaken = new TimeSpan();
    }

    public bool Update(string buildDirectory)
    {
        TotalTimeOverall = new TimeSpan();
        TotalTimeTaken = new TimeSpan();

        /*TimeSpan nvmTotalTime = new TimeSpan();
        int nvmCount = 0;
        TimeSpan othersTotalTime = new TimeSpan();
        int othersCount = 0;
        int totalCount = 0;
        string[] concatTaskFiles = Directory.GetFiles(buildDirectory, "*concat*.xml", SearchOption.AllDirectories);

        foreach (string taskFile in concatTaskFiles)
        {
            XPathDocument document = new XPathDocument(taskFile);
            XPathNavigator navigator = document.CreateNavigator();

            XPathNodeIterator iterator = navigator.Select("//TaskReportInfo/TimeElapsed");
            iterator.MoveNext();
            TimeSpan timeSpan = TimeSpan.Parse(iterator.Current.InnerXml);
            TotalTimeOverall += timeSpan;

            XPathNodeIterator machineIterator = navigator.Select("//TaskReportInfo/MachineName");
            machineIterator.MoveNext();
            if (String.Compare(machineIterator.Current.InnerXml, "sanb-nvm01", true) == 0)
            {
                nvmTotalTime += timeSpan;
                nvmCount++;
            }
            else
            {
                othersTotalTime += timeSpan;
                othersCount++;
            }

            
            totalCount++;
        }

        double averageNvmTime = nvmTotalTime.TotalSeconds / nvmCount;
        double othersTime = othersTotalTime.TotalSeconds / othersCount;
        double averageConcatTime = TotalTimeOverall.TotalSeconds / totalCount;
         */

        string[] taskFiles = Directory.GetFiles(buildDirectory, "MotionBuilder*.xml", SearchOption.AllDirectories);
        TimeSpan timeOverall = new TimeSpan();
        TimeSpan timeTaken = new TimeSpan();
        foreach (string taskFile in taskFiles)
        {
            try
            {
                XPathDocument document = new XPathDocument(taskFile);
                XPathNavigator navigator = document.CreateNavigator();

                XPathNodeIterator iterator = navigator.Select("//TaskReportInfo/TimeElapsed");
                iterator.MoveNext();
                TimeSpan timeSpan = TimeSpan.Parse(iterator.Current.InnerXml);
                timeOverall += timeSpan;
            }
            catch (Exception) { }
        }

        string[] summaryFiles = Directory.GetFiles(buildDirectory, "summary.xml", SearchOption.AllDirectories);

        foreach (string summaryFile in summaryFiles)
        {
            try
            {
                XPathDocument document = new XPathDocument(summaryFile);
                XPathNavigator navigator = document.CreateNavigator();

                XPathNodeIterator iterator = navigator.Select("//SummaryReport/@TotalDuration");
                iterator.MoveNext();
                TimeSpan timeSpan = TimeSpan.Parse(iterator.Current.InnerXml);
                timeTaken += timeSpan;
            }
            catch (Exception) { }
        }

        TotalTimeOverall = timeOverall;
        TotalTimeTaken = timeTaken;

        return true;
    }
}

/// <summary>
/// Summary description for BuildItem
/// </summary>
[Serializable]
public class BuildItem
{
    public string Name;     // Friendly name for display.
    public string BuildName;    //Real name of the build.
    public string DirectoryName; //Directory containing the build files.
    private string BuildDirectory;
    
    //public IntegrationResult BuildStatus;  //Cruise Control-based build information.
    public StatusReport StatusReport; //Automated Build-based information.
    public BuildStatistics StatisticsReport;

    public static bool UpdateStatistics = false;

    private static readonly string m_StateSearchWildcard = "*.state";    

	public BuildItem()
	{
        Name = null;
        BuildName = null;
	}

    public BuildItem(string name, string buildDirectory)
    {
        BuildName = name;
        Name = BuildName.Replace("_", " ");

        BuildDirectory = buildDirectory;
        DirectoryName = Path.GetFileName(BuildDirectory);
    }

    public bool Update()
    {
        //Acquire the current state of the build.
        if ( String.IsNullOrWhiteSpace(BuildDirectory) == true || Directory.Exists(BuildDirectory) == false )
        {
            //Something deleted this directory.
            return false;
        }

        //Disabled Cruise Contorl state file parsing.
        /*
        string[] statefiles = Directory.GetFiles(BuildDirectory, m_StateSearchWildcard);

        if (statefiles.Length == 0)
        {
            //Unable to find .state file.  Mark this build as inactive.
        }
        else if (statefiles.Length > 1)
        {
            //Ignore any subsequent ones, log a warning stating that two state files exist.
            //TODO: Handle the instance where a Cruise Control build name changed.  
        }
        else
        {
            //Parse the State file.
            IntegrationResult result = null;
            if (IntegrationResult.Load(statefiles[0], out result) == false)
            {
                //TODO:  Handle error case where the state file is unable to be loaded.
            }

            BuildStatus = result;
        }
        */

        string[] buildfiles = Directory.GetFiles(BuildDirectory, StatusReport.StatusReportXmlFile);
        if (buildfiles.Length == 0)
        {
            //Unable to find .state file.  Mark this build as inactive.
            return false;
        }
        else if (buildfiles.Length > 1)
        {
            //Ignore any subsequent ones, log a warning stating that two state files exist.
            //TODO: Handle the instance where a Cruise Control build name changed.  
        }
        else
        {
            StatusReport statusReport = null;
            if (StatusReport.Load(buildfiles[0], out statusReport) == false)
            {
                //TODO:  Handle error case where build info is unable to be loaded.
            }

            StatusReport = statusReport;
        }

        //Do not update the statistics on the first pass.
        if (UpdateStatistics == true)
        {
            StatisticsReport = new BuildStatistics();
            StatisticsReport.Update(BuildDirectory);
        }

        UpdateStatistics = true;

        return true;
    }
}