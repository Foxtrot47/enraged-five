﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.XPath;

/// <summary>
/// Summary description for BuildConfiguration
/// </summary>
public class BuildConfiguration
{
    public static string DatabaseName;
    public static string ServerName;

	public BuildConfiguration()
	{
		
	}

    public static void Initialize()
    {
        Environment.SetEnvironmentVariable("RS_PROJECT", @"rdr3");
        Environment.SetEnvironmentVariable("RS_PROJROOT", @"X:\rdr3");
        Environment.SetEnvironmentVariable("RS_TOOLSROOT", @"X:\rdr3\tools");

        //TODO:  Websites seem incapable to using symbolically linked folders to access data.
        //Perhaps there's a way around this using IIS.  Need to investigate further.
        //To also wreaked havoc on the normal configuration system, since it wants to root itself in the X drive.
        XPathDocument navDocument = new XPathDocument(Path.Combine("C:\\Projects\\rdr3\\tools", "etc", "project.xml"));
        XPathNavigator navigator = navDocument.CreateNavigator();

        XPathNodeIterator iter = navigator.Select("//project/userstatistics/@database");
        iter.MoveNext();
        DatabaseName = iter.Current.Value;

        iter = navigator.Select("//project/userstatistics/@server");
        iter.MoveNext();
        ServerName = iter.Current.Value;

        StreamWriter log = new StreamWriter("C:\\errorlog.txt", true);
        log.WriteLine("Server Name: " + ServerName);
        log.WriteLine("Database Name: " + DatabaseName);
        log.Close();

    }
}