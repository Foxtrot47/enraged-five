﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="BuildHistory.aspx.cs" Inherits="BuildHistory" %>

<%@ Register Src="~/Controls/BuildStatus.ascx" TagName="BuildStatus" TagPrefix="AutomatedBuildControls" %>
<%@ Register Src="~/Controls/BuildStatusList.ascx" TagName="BuildStatusList" TagPrefix="AutomatedBuildControls" %>
<%@ Register Src="~/Controls/NavigationPanel.ascx" TagName="NavigationPanel" TagPrefix="AutomatedBuildControls" %>
<%@ Register Src="~/Controls/BuildHistoryList.ascx" TagName="BuildHistoryList" TagPrefix="AutomatedBuildControls" %>
<%@ Register Src="~/Controls/CutsceneBuildHistory.ascx" TagName="CutsceneBuildHistory" TagPrefix="AutomatedBuildControls" %>
<%@ Register Src="~/Controls/CutscenePackageHistory.ascx" TagName="CutscenePackageHistory" TagPrefix="AutomatedBuildControls" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">

    <meta http-equiv="refresh" content="600">

    <table cellpadding="0" rules="none" width="100%" height="100%" frame="void">
        <tr>
         <td class="rootTable1" valign="top"> 
            <AutomatedBuildControls:NavigationPanel ID="NavPanel" CssClass="leftCol" runat="server" />
         </td>
         <td align="Center" class="rootTable2">
            <div>
                <automatedbuildcontrols:buildhistorylist ID="HistoryList" CssClass="rightCol" 
                    runat="server" />

                <asp:Panel ID="CustomHistoryPanel" runat="server">
                </asp:Panel>
                
            </div>
         </td>
        </tr>
    </table>
   
</asp:Content>
