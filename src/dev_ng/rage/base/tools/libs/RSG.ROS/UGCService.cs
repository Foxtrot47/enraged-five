﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using LitJson;
using RSG.Base.Attributes;
using RSG.Base.Configuration;
using RSG.Base.Configuration.ROS;
using RSG.Base.Logging;
using RSG.ROS.UGC;

namespace RSG.ROS
{
    /// <summary>
    /// Types of UGC content that we can query for.
    /// </summary>
    public enum UGCContentType
    {
        [RuntimeName("gta5mission")]
        Gta5Mission,

        [RuntimeName("gta5missionplaylist")]
        Playlist,

        [RuntimeName("lifeinvaderpost")]
        LifeInvaderPost,

        [RuntimeName("photo")]
        Screenshot
    } // UGCContentType

    
    /// <summary>
    /// UGC content type enumeration utility and extension methods.
    /// </summary>
    public static class UGCContentTypeyUtils
    {
        /// <summary>
        /// Return runtime name string for a VehicleCategory enum value.
        /// </summary>
        /// <param name="metric"></param>
        /// <returns></returns>
        public static String GetRuntimeName(this UGCContentType contentType)
        {
            Type type = contentType.GetType();

            FieldInfo fi = type.GetField(contentType.ToString());
            RuntimeNameAttribute[] attributes =
                fi.GetCustomAttributes(typeof(RuntimeNameAttribute), false) as RuntimeNameAttribute[];

            return (attributes.Length > 0 ? attributes[0].Name : null);
        }

    }

    /// <summary>
    /// Service for querying user generated content related information.
    /// </summary>
    public class UGCService : GlobalService
    {
        #region Constants
        /// <summary>
        /// Context to use for log messages.
        /// </summary>
        private const String c_logCtx = "ROS:UGCService";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Uri to query the ROS service for these queries.
        /// </summary>
        protected override Uri ServiceUri
        {
            get
            {
                return m_serviceUri;
            }
        }
        private Uri m_serviceUri;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public UGCService()
            : this(ConfigFactory.CreateROSConfig())
        {
        }

        /// <summary>
        /// Constructor that takes a config game view as an argument.
        /// (This is the preferred constructor.)
        /// </summary>
        /// <param name="gv"></param>
        public UGCService(IROSConfig config)
            : base(config)
        {
            Debug.Assert(config.GlobalServiceUrls.ContainsKey("ugc"),
                "Unable to initialise the telemetry ROS Service as no appropriate entry exists in the config xml.");
            if (!config.GlobalServiceUrls.ContainsKey("ugc"))
            {
                throw new ArgumentNullException("Unable to initialise the telemetry ROS Service as no appropriate entry exists in the config xml.");
            }
            m_serviceUri = new Uri(BaseUri, config.GlobalServiceUrls["ugc"]);
        }
        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// Queries for UGC content.
        /// </summary>
        public IList<UGCContentInfo> QueryContent(AuthTicket ticket, UGCContentType contentType, IEnumerable<String> contentIds)
        {
            List<UGCContentInfo> returnData = new List<UGCContentInfo>();

            // Query the ROS endpoint in batches of 250.
            const int c_batchSize = 250;
            for (int i = 0; i < contentIds.Count(); i += c_batchSize)
            {
                int currentBatchSize = Math.Min(c_batchSize, contentIds.Count() - i);

                // Generate the contentQueryJson.
                StringBuilder sb = new StringBuilder();
                JsonWriter writer = new JsonWriter(sb);

                writer.WriteObjectStart();
                {
                    writer.WritePropertyName("contentids");
                    writer.WriteArrayStart();
                    for (int j = i; j < i + currentBatchSize; ++j)
                    {
                        writer.Write(contentIds.ElementAt(j));
                    }
                    writer.WriteArrayEnd();

                    writer.WritePropertyName("Offset");
                    writer.Write(0);

                    writer.WritePropertyName("Count");
                    writer.Write(currentBatchSize);
                }
                writer.WriteObjectEnd();

                Log.DebugCtx(c_logCtx, "Content Query Json: {0}", sb.ToString());
                String postData = String.Format("ticket={0}&contentType={1}&contentQueryName=ScGetContent&contentQueryParams={2}&" +
                    "playerQueryName=&playerQueryParams=&viewerRockstarId=0",
                    HttpUtility.UrlEncode(ticket.EncryptedAuth), contentType.GetRuntimeName(), HttpUtility.UrlEncode(sb.ToString()));

                List<UGCContentInfo> currentData = new List<UGCContentInfo>();

                using (Stream returnStream = RunQuery(ticket, String.Format("{0}/{1}", ServiceUri.ToString(), "QueryContentXml"), "POST", postData))
                {
                    if (returnStream != null)
                    {
                        XDocument xDoc = XDocument.Load(returnStream);
                        XNamespace ns = "QueryContentXml";
                        XmlNamespaceManager nsManager = new XmlNamespaceManager(new NameTable());
                        nsManager.AddNamespace("r", "QueryContentXml");

                        foreach (XElement contentElem in xDoc.XPathSelectElements("/r:Response/r:Result/r:Results/r:Result/r:Content", nsManager))
                        {
                            try
                            {
                                if (contentType == UGCContentType.Gta5Mission)
                                {
                                    currentData.Add(UGCMissionContentInfo.CreateFromXElement(contentElem, ns));
                                }
                                else
                                {
                                    throw new NotImplementedException();
                                }
                            }
                            catch (System.Exception ex)
                            {
                                Log.ExceptionCtx(c_logCtx, ex, "Failed to process UGC content return data\nXML Content:\n{0}.", contentElem.Value);
                            }
                        }

                        // Patch up the userid's after we've retrieved the UGC information.
                        IDictionary<String, String> userIdLookup = new Dictionary<String, String>();
                        foreach (XElement playerElem in xDoc.XPathSelectElements("/r:Response/r:Result/r:PlayerAccounts/r:PlayerAccount", nsManager))
                        {
                            XAttribute userIdAtt = playerElem.Attribute("UserId");
                            XAttribute userNameAtt = playerElem.Attribute("UserName");
                            if (userIdAtt != null && userNameAtt != null)
                            {
                                userIdLookup.Add(userIdAtt.Value, userNameAtt.Value);
                            }
                        }

                        foreach (UGCContentInfo info in currentData)
                        {
                            if (userIdLookup.ContainsKey(info.UserId))
                            {
                                info.UserId = userIdLookup[info.UserId];
                            }
                        }
                    }
                }

                returnData.AddRange(currentData);
            }

            return returnData;
        }
        #endregion // Public Methods
    } // UGCService
}
