﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Linq;
using RSG.Base.Configuration;
using RSG.Base.Configuration.ROS;
using RSG.ROS.Accounts;

namespace RSG.ROS
{
    /// <summary>
    /// Service for querying social club account related information.
    /// </summary>
    public class AccountService : GlobalService
    {
        #region Constants
        /// <summary>
        /// Context to use for log messages.
        /// </summary>
        private const String c_logCtx = "ROS:AccountService";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Uri to query the ROS service for these queries.
        /// </summary>
        protected override Uri ServiceUri
        {
            get { return m_serviceUri; }
        }
        private Uri m_serviceUri;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public AccountService()
            : this(ConfigFactory.CreateROSConfig())
        {
        }

        /// <summary>
        /// Constructor that takes a config game view as an argument.
        /// (This is the preferred constructor.)
        /// </summary>
        /// <param name="gv"></param>
        public AccountService(IROSConfig config)
            : base(config)
        {
            Debug.Assert(config.GlobalServiceUrls.ContainsKey("accounts"),
                "Unable to initialise the telemetry ROS Service as no appropriate entry exists in the config xml.");
            if (!config.GlobalServiceUrls.ContainsKey("accounts"))
            {
                throw new ArgumentNullException("Unable to initialise the telemetry ROS Service as no appropriate entry exists in the config xml.");
            }
            m_serviceUri = new Uri(BaseUri, config.GlobalServiceUrls["accounts"]);
        }
        #endregion // Constructor(s)
        
        #region Public Methods
        /// <summary>
        /// Queries for UGC content.
        /// </summary>
        /// <param name="platform">Platform we are querying data for.</param>
        /// <param name="userIds">In the case of PS3 this is simply the gamer tag, for Xbox this is the decimal version of the user name.</param>
        /// <returns>Mapping of user id's to account information.  Note that not all user id's requested will have an entry in the lookup as
        /// the might not be linked to a social club account.</returns>
        public IDictionary<String, AccountInfo> GetAccountsLinkedTo(AuthTicket ticket, ROSPlatform platform, IEnumerable<String> userIds)
        {
            // Generate the post data
            String onlineServiceName;
            if (platform == ROSPlatform.PS3)
            {
                onlineServiceName = "np";
            }
            else if (platform == ROSPlatform.Xbox360)
            {
                onlineServiceName = "xbl";
            }
            else
            {
                Debug.Assert(false, "Only the PS3/Xbox platforms are currently supported when running a GetAccountsLinkedTo request.");
                throw new ArgumentException("Only the PS3/Xbox platforms are currently supported when running a GetAccountsLinkedTo request.");
            }

            String postData = String.Format("ticket={0}&onlineServiceName={1}&userIdCsv={2}&accountIdCsv=",
                HttpUtility.UrlEncode(ticket.EncryptedAuth), onlineServiceName, String.Join(",", userIds));

            // Execute the query.
            IDictionary<String, AccountInfo> infos = new Dictionary<String, AccountInfo>();
            using (Stream responseStream = RunQuery(ticket, String.Format("{0}/{1}", ServiceUri.ToString(), "GetAccountsLinkedTo"), "POST", postData))
            {
                if (responseStream != null)
                {
                    XDocument xDoc = XDocument.Load(responseStream);
                    XNamespace ns = "GetAccountsLinkedTo";
                    XmlNamespaceManager nsManager = new XmlNamespaceManager(new NameTable());
                    nsManager.AddNamespace("r", "GetAccountsLinkedTo");

                    foreach (XElement accountElem in xDoc.XPathSelectElements("/r:Response/r:Result/r:AccountLinkedTo", nsManager))
                    {
                        // We need both the user id and the rockstar account information.
                        XAttribute userAttr = accountElem.Attribute("UserId");
                        XElement rsAccountElem = accountElem.Element(ns + "RockstarAccount");

                        if (userAttr != null && rsAccountElem != null)
                        {
                            try
                            {
                                infos[userAttr.Value] = new AccountInfo(rsAccountElem, ns);
                            }
                            catch (System.Exception ex)
                            {
                                Log.ErrorCtx(c_logCtx, "Unable to parse account xml element due to: {0}", ex.Message);
                            }
                        }
                    }
                }
            }

            return infos;
        }

        /// <summary>
        /// Queries for the list of countries that the social club services are aware of.
        /// </summary>
        public IList<CountryInfo> GetCountries(AuthTicket ticket)
        {
            List<CountryInfo> countries = new List<CountryInfo>();

            String postData = String.Format("ticket={0}", HttpUtility.UrlEncode(ticket.EncryptedAuth));
            using (Stream responseStream = RunQuery(ticket, String.Format("{0}/{1}", ServiceUri.ToString(), "GetCountries"), "POST", postData))
            {
                if (responseStream != null)
                {
                    XDocument xDoc = XDocument.Load(responseStream);
                    XmlNamespaceManager nsManager = new XmlNamespaceManager(new NameTable());
                    nsManager.AddNamespace("r", "GetCountriesResponse");

                    foreach (XElement countryElem in xDoc.XPathSelectElements("/r:Response/r:Countries/r:Country", nsManager))
                    {
                        try
                        {
                            countries.Add(new CountryInfo(countryElem));
                        }
                        catch (System.Exception ex)
                        {
                            Log.ErrorCtx(c_logCtx, "Unable to parse country xml element due to: {0}", ex.Message);
                        }
                    }
                }
            }

            return countries;
        }
        #endregion // Public Methods
    } // AccountService
}
