﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Attributes;

namespace RSG.ROS
{
    /// <summary>
    /// 
    /// </summary>
    public class RosIdAttribute : Attribute
    {
        #region Properties
        /// <summary>
        /// Id associated with this attribute.
        /// </summary>
        public int Id { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor taking the id.
        /// </summary>
        public RosIdAttribute(int id)
        {
            Id = id;
        }
        #endregion // Constructor(s)
    } // RosIdAttribute
}
