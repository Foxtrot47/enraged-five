﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Web;
using RSG.Base.Configuration;
using RSG.Base.Configuration.ROS;
using System.Diagnostics;

namespace RSG.ROS
{
    /// <summary>
    /// 
    /// </summary>
    public class ProfileStatService : GameService
    {
        #region Properties
        /// <summary>
        /// Uri to query the ROS service for admin authentication
        /// </summary>
        protected override Uri ServiceUri
        {
            get
            {
                return m_serviceUri;
            }
        }
        private Uri m_serviceUri;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public ProfileStatService()
            : this(ConfigFactory.CreateROSConfig())
        {
        }

        /// <summary>
        /// Constructor that takes a config game view as an argument.
        /// (This is the preferred constructor.)
        /// </summary>
        /// <param name="gv"></param>
        public ProfileStatService(IROSConfig config)
            : base(config)
        {
            Debug.Assert(config.GameServiceUrls.ContainsKey("profilestats"),
                "Unable to initialise the profile stats ROS Service as no appropriate entry exists in the config xml");
            if (!config.GameServiceUrls.ContainsKey("profilestats"))
            {
                throw new ArgumentNullException("Unable to initialise the profile stats ROS Service as no appropriate entry exists in the config xml");
            }
            m_serviceUri = new Uri(BaseUri, config.GameServiceUrls["profilestats"]);
        }
        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// Returns an XML document of profile stat definitions.
        /// </summary>
        /// <param name="ticket">Ticket obtained from Auth service. Because this is a "dev" method the ticket must have been created with one of the DevCreateTicket* web methods.</param>
        /// <returns>Profile stat definition XML.</returns>
        public Stream ReadMetadata(AuthTicket ticket)
        {
            string postData = String.Format("ticket={0}", HttpUtility.UrlEncode(ticket.EncryptedAuth));

            // Run the query
            return RunQuery(ticket, String.Format("{0}/{1}", ServiceUri.ToString(), "ReadMetadata"), "POST", postData, 300000);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ticket"></param>
        /// <param name="gamerHandles"></param>
        /// <param name="statIds"></param>
        /// <returns></returns>
        [Obsolete]
        public Stream ReadStatsByGamer(AuthTicket ticket, String gamerHandles, String statIds)
        {

            string postData = String.Format("ticket={0}&metadataVersion=-1&gamerHandles={1}&statIds={2}",
                HttpUtility.UrlEncode(ticket.EncryptedAuth),
                gamerHandles,
                statIds);

            // Run the query
            return RunQuery(ticket, String.Format("{0}/{1}", ServiceUri.ToString(), "ReadStatsByGamer"), "POST", postData, 300000);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ticket"></param>
        /// <param name="metadataVersion"></param>
        /// <param name="after"></param>
        /// <param name="before"></param>
        /// <returns></returns>
        public Stream ReadAllStatsByLastUpdateTime(AuthTicket ticket, int metadataVersion, DateTime after, DateTime before)
        {

            string postData = String.Format("ticket={0}&metadataVersion={1}&updatedAfterUtc={2}&butBeforeUtc={3}&maxResults=-1",
                HttpUtility.UrlEncode(ticket.EncryptedAuth),
                metadataVersion,
                after.ToString("yyyy-MM-dd"),
                before.ToString("yyyy-MM-dd"));

            // Run the query
            return RunQuery(ticket, String.Format("{0}/{1}", ServiceUri.ToString(), "ReadAllStatsByLastUpdateTime"), "POST", postData, 300000);
        }
        #endregion // Public Methods
    } // ProfileStatService
}
