﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using LitJson;

namespace RSG.ROS.UGC
{
    /// <summary>
    /// 
    /// </summary>
    public class UGCMissionContentInfo : UGCContentInfo
    {
        #region Public Classes
        /// <summary>
        /// Data common to all mission types.
        /// </summary>
        public class GeneralData
        {
            /// <summary>
            /// Mission type (deathmatch, race, mission, etc...).
            /// </summary>
            public int Type { get; internal set; }

            /// <summary>
            /// Sub-type for this mission.
            /// </summary>
            public int SubType { get; internal set; }

            /// <summary>
            /// Starting x coord.
            /// </summary>
            public float StartX { get; internal set; }

            /// <summary>
            /// Starting y coord.
            /// </summary>
            public float StartY { get; internal set; }

            /// <summary>
            /// Starting z coord.
            /// </summary>
            public float StartZ { get; internal set; }
        } // GeneralData

        /// <summary>
        /// Additional race data associated with this mission.
        /// </summary>
        public class RaceData
        {
            /// <summary>
            /// Types of vehicles to be used (used by deathmatches).
            /// </summary>
            public int VehicleType { get; internal set; }

            /// <summary>
            /// Type of race this is (standard, gta or rally).
            /// </summary>
            public int RaceType { get; internal set; }
        } // RaceData

        /// <summary>
        /// Rules associated with this mission.
        /// </summary>
        public class MissionRules
        {
            /// <summary>
            /// Whether this is a team deathmatch.
            /// </summary>
            public bool TeamDeathmatch { get; internal set; }

            /// <summary>
            /// Whether this is a vehicle deathmatch.
            /// </summary>
            public bool VehicleDeathmatch { get; internal set; }
        } // MissionRules
        #endregion // Public Classes

        #region Properties
        /// <summary>
        /// General data.
        /// </summary>
        public GeneralData General { get; private set; }

        /// <summary>
        /// Additional race data for this mission (only used if it's a race).
        /// </summary>
        public RaceData Race { get; private set; }

        /// <summary>
        /// Rules associated with this mission.
        /// </summary>
        public MissionRules Rules { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        private UGCMissionContentInfo()
            : base()
        {
            General = new GeneralData();
            Race = new RaceData();
            Rules = new MissionRules();
        }
        #endregion // Constructor(s)

        #region Static Controller Methods
        /// <summary>
        /// Creates a UGC content info item from an XML element.
        /// </summary>
        /// <param name="elem"></param>
        /// <param name="nsManager"></param>
        /// <returns></returns>
        internal static UGCMissionContentInfo CreateFromXElement(XElement elem, XNamespace ns)
        {
            UGCMissionContentInfo info = new UGCMissionContentInfo();
            info.InitialiseFromXElement(elem, ns);

            XElement matadataElem = elem.Element(ns + "Metadata");
            Debug.Assert(matadataElem != null, "Missing Metadata sub-element.");
            if (matadataElem == null)
            {
                throw new ArgumentNullException("Content element is missing the Metadata sub-element.");
            }
            
            // Make sure the data element exists.
            XElement dataJsonElement = matadataElem.Element(ns + "DataJson");
            Debug.Assert(dataJsonElement != null, "Missing DataJson sub-element.");
            if (dataJsonElement == null)
            {
                throw new ArgumentNullException("Metadata element is missing the DataJson sub-element.");
            }

            // Extract the relevant information.
            JsonData data = JsonMapper.ToObject(dataJsonElement.Value);
            Debug.Assert(data.ContainsKey("mission"), "Json data missing 'mission' element.");
            if (!data.ContainsKey("mission"))
            {
                throw new ArgumentNullException("Json data missing 'mission' element.");
            }
            JsonData missionData = data["mission"];

            // General data
            Debug.Assert(missionData.ContainsKey("race"), "Json data missing 'mission/gen' element.");
            if (!missionData.ContainsKey("gen"))
            {
                throw new ArgumentNullException("Json data missing 'mission/gen' element.");
            }
            JsonData generalData = missionData["gen"];
            ParseGeneralData(info, generalData);

            // Race data
            Debug.Assert(missionData.ContainsKey("race"), "Json data missing 'mission/race' element.");
            if (!missionData.ContainsKey("race"))
            {
                throw new ArgumentNullException("Json data missing 'mission/race' element.");
            }
            JsonData raceData = missionData["race"];
            ParseRaceData(info, raceData);

            // Rule data
            Debug.Assert(missionData.ContainsKey("rule"), "Json data missing 'mission/rule' element.");
            if (!missionData.ContainsKey("rule"))
            {
                throw new ArgumentNullException("Json data missing 'mission/rule' element.");
            }
            JsonData ruleData = missionData["rule"];
            ParseRuleData(info, ruleData);

            return info;
        }

        /// <summary>
        /// 
        /// </summary>
        private static void ParseGeneralData(UGCMissionContentInfo info, JsonData data)
        {
            Debug.Assert(data.ContainsKey("type"), "Json data missing 'mission/gen/type' element.");
            if (!data.ContainsKey("type"))
            {
                throw new ArgumentNullException("Json data missing 'mission/gen/type' element.");
            }
            info.General.Type = (int)data["type"];

            Debug.Assert(data.ContainsKey("subtype"), "Json data missing 'mission/gen/subtype' element.");
            if (!data.ContainsKey("subtype"))
            {
                throw new ArgumentNullException("Json data missing 'mission/gen/subtype' element.");
            }
            info.General.SubType = (int)data["subtype"];

            Debug.Assert(data.ContainsKey("start"), "Json data missing 'mission/gen/start' element.");
            if (!data.ContainsKey("start"))
            {
                throw new ArgumentNullException("Json data missing 'mission/gen/start' element.");
            }
            JsonData startData = data["start"];

            Debug.Assert(startData.ContainsKey("x"), "Json data missing 'mission/gen/start/x' element.");
            if (!startData.ContainsKey("x"))
            {
                throw new ArgumentNullException("Json data missing 'mission/gen/start/x' element.");
            }
            info.General.StartX = (float)startData["x"];

            Debug.Assert(startData.ContainsKey("y"), "Json data missing 'mission/gen/start/y' element.");
            if (!startData.ContainsKey("y"))
            {
                throw new ArgumentNullException("Json data missing 'mission/gen/start/y' element.");
            }
            info.General.StartY = (float)startData["y"];

            Debug.Assert(startData.ContainsKey("z"), "Json data missing 'mission/gen/start/z' element.");
            if (!startData.ContainsKey("z"))
            {
                throw new ArgumentNullException("Json data missing 'mission/gen/start/z' element.");
            }
            info.General.StartZ = (float)startData["z"];
        }

        /// <summary>
        /// 
        /// </summary>
        private static void ParseRaceData(UGCMissionContentInfo info, JsonData data)
        {
            Debug.Assert(data.ContainsKey("gtar"), "Json data missing 'mission/race/gtar' element.");
            if (!data.ContainsKey("gtar"))
            {
                throw new ArgumentNullException("Json data missing 'mission/race/gtar' element.");
            }
            info.Race.RaceType = (int)data["gtar"];

            Debug.Assert(data.ContainsKey("ivm"), "Json data missing 'mission/race/ivm' element.");
            if (!data.ContainsKey("ivm"))
            {
                throw new ArgumentNullException("Json data missing 'mission/race/ivm' element.");
            }
            info.Race.VehicleType = (int)data["ivm"];
        }

        /// <summary>
        /// 
        /// </summary>
        private static void ParseRuleData(UGCMissionContentInfo info, JsonData data)
        {
            Debug.Assert(data.ContainsKey("tdm"), "Json data missing 'mission/rule/tdm' element.");
            if (!data.ContainsKey("tdm"))
            {
                throw new ArgumentNullException("Json data missing 'mission/rule/tdm' element.");
            }
            info.Rules.TeamDeathmatch = ((int)data["tdm"] == 1 ? true : false);

            Debug.Assert(data.ContainsKey("vdm"), "Json data missing 'mission/rule/vdm' element.");
            if (!data.ContainsKey("vdm"))
            {
                throw new ArgumentNullException("Json data missing 'mission/rule/vdm' element.");
            }
            info.Rules.VehicleDeathmatch = ((int)data["vdm"] == 1 ? true : false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [Obsolete]
        public static UGCMissionContentInfo CreateDummyInfo(String name)
        {
            UGCMissionContentInfo info = new UGCMissionContentInfo();
            info.Name = name;
            return info;
        }
        #endregion // Static Controller Methods
    } // UGCMissionContentInfo
}
