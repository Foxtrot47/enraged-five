﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using LitJson;

namespace RSG.ROS.UGC
{
    /// <summary>
    /// Details about a single piece of UGC content.
    /// </summary>
    public abstract class UGCContentInfo
    {
        #region Properties
        /// <summary>
        /// UGC identifier.
        /// </summary>
        public String UniqueIdentifier { get; private set; }

        /// <summary>
        /// Name of the content.
        /// </summary>
        public String Name { get; protected set; }

        /// <summary>
        /// Description of the content.
        /// </summary>
        public String Description { get; private set; }

        /// <summary>
        /// Flag indicating whether this item has been published.
        /// </summary>
        public bool IsPublished { get; private set; }

        /// <summary>
        /// Platform of the creator for this piece of content.
        /// </summary>
        public ROSPlatform Platform { get; private set; }

        /// <summary>
        /// Identifier of the user that created this piece of UGC.
        /// </summary>
        public String UserId { get; internal set; }

        /// <summary>
        /// Average rating for this mission.
        /// </summary>
        public float AverageRating { get; internal set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        protected UGCContentInfo()
        {
        }
        #endregion // Constructor(s)

        #region Protected Methods
        /// <summary>
        /// Sets the local fields based off of the data in the xelement.
        /// </summary>
        protected void InitialiseFromXElement(XElement elem, XNamespace ns)
        {
            XElement matadataElem = elem.Element(ns + "Metadata");
            Debug.Assert(matadataElem != null, "Missing Metadata sub-element.");
            if (matadataElem == null)
            {
                throw new ArgumentNullException("Content element is missing the Metadata sub-element.");
            }
            
            // Extract the various bits of data.
            XElement contentIdElement = matadataElem.Element(ns + "ContentId");
            Debug.Assert(contentIdElement != null, "Missing ContentId sub-element.");
            if (contentIdElement == null)
            {
                throw new ArgumentNullException("Metadata element is missing the ContentId sub-element.");
            }
            UniqueIdentifier = contentIdElement.Value;

            XElement contentNameElement = matadataElem.Element(ns + "ContentName");
            Debug.Assert(contentNameElement != null, "Missing ContentName sub-element.");
            if (contentNameElement == null)
            {
                throw new ArgumentNullException("Metadata element is missing the ContentName sub-element.");
            }
            Name = contentNameElement.Value;

            XElement descriptionElement = matadataElem.Element(ns + "Description");
            if (descriptionElement != null)
            {
                Description = descriptionElement.Value;
            }

            XElement isPublishedElem = matadataElem.Element(ns + "IsPublished");
            if (isPublishedElem != null)
            {
                IsPublished = Boolean.Parse(isPublishedElem.Value);
            }

            XElement platformIdElement = matadataElem.Element(ns + "PlatformId");
            Debug.Assert(platformIdElement != null, "Missing PlatformId sub-element.");
            if (platformIdElement == null)
            {
                throw new ArgumentNullException("Metadata element is missing the PlatformId sub-element.");
            }
            if (platformIdElement.Value == "1")
            {
                Platform = ROSPlatform.Xbox360;
            }
            else if (platformIdElement.Value == "2")
            {
                Platform = ROSPlatform.PS3;
            }
            else if (platformIdElement.Value == "3")
            {
                Platform = ROSPlatform.Win;
            }

            XElement userIdElement = matadataElem.Element(ns + "UserId");
            Debug.Assert(userIdElement != null, "Missing UserId sub-element.");
            if (userIdElement == null)
            {
                throw new ArgumentNullException("Metadata element is missing the UserId sub-element.");
            }
            UserId = userIdElement.Value;

            // Rating information
            XElement ratingsElem = elem.Element(ns + "Ratings");
            Debug.Assert(ratingsElem != null, "Missing Ratings sub-element.");
            if (ratingsElem == null)
            {
                throw new ArgumentNullException("Content element is missing the Ratings sub-element.");
            }

            XAttribute avgRatingAtt = ratingsElem.Attribute("Average");
            Debug.Assert(avgRatingAtt != null, "Missing Average attribute.");
            if (avgRatingAtt == null)
            {
                throw new ArgumentNullException("Ratings element is missing the Average attribute.");
            }
            AverageRating = Single.Parse(avgRatingAtt.Value);
        }
        #endregion // Protected Methods
    } // UGCContent
}
