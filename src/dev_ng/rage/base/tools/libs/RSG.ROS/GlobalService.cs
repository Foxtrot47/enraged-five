﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Configuration.ROS;

namespace RSG.ROS
{
    /// <summary>
    /// Common base class for the various global services.
    /// </summary>
    public abstract class GlobalService : ROSService
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public GlobalService(IROSConfig config)
            : base(config)
        {
        }
        #endregion // Constructor(s)
    } // GlobalService
}
