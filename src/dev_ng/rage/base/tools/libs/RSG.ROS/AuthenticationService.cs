﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Threading;
using RSG.Base.Configuration.ROS;
using RSG.Base.Logging;

namespace RSG.ROS
{
    /// <summary>
    /// Common base class for authentication services such as the admin and user authentication services
    /// </summary>
    public abstract class AuthenticationService : ROSService
    {
        #region Constants
        /// <summary>
        /// The amount of time frame in which you can send a maximum number of login attempts
        /// </summary>
        private const int c_loginTimeFrame = 60;

        /// <summary>
        /// The maximum number of login attempts allowed in the time frame specified above
        /// 
        /// NOTE: ROS wiki says that the maximum number of logins per minute is 3, however it seems
        /// to incorrectly use up 2 attempts each time it logins, so if you try to login twice in
        /// a minute you make 4 login attempts as far as the server is concerned and end up going over
        /// the limit of 3...
        /// </summary>
        private const int c_maxLoginAttempts = 1;

        /// <summary>
        /// Context to use for log messages.
        /// </summary>
        private const String c_logCtx = "ROS:AuthenticationService";
        #endregion // Constants

        #region Members
        /// <summary>
        /// 
        /// </summary>
        private LinkedList<DateTime> m_loginRequests = new LinkedList<DateTime>();
        #endregion // Members

        #region Constructor(s)
        /// <summary>
        /// Constructor that takes a config game view as an argument.
        /// </summary>
        /// <param name="gv"></param>
        public AuthenticationService(IROSConfig config)
            : base(config)
        {
        }
        #endregion // Constructor(s)
        
        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="username">Admin account username.</param>
        /// <param name="password">Admin account password.</param>
        /// <param name="title">Name of title. If the ticket is intended to only be used at global scope, this should be null or empty.</param>
        /// <param name="platform"></param>
        /// <returns>An authentication ticket that can be used for subsequent ROS queries, or null if authentication failed.</returns>
        public AuthTicket CreateTicket(String username, String password, String title, ROSPlatform platform)
        {
            // Make sure we don't request too many tickets.
            WaitForQueryLimits();

            if (platform == ROSPlatform.PS3)
            {
                return CreateTicketNp(username, password, title, "0");
            }
            else if (platform == ROSPlatform.Xbox360)
            {
                return CreateTicketXbl(username, password, title, "1", "Gta5Statistics");
            }
            else if (platform == ROSPlatform.Win)
            {
                throw new NotSupportedException("Windows ROS platform is not yet supported.");
            }
            else
            {
                throw new ArgumentException(String.Format("Unknown ROS platform '{0}' encountered.", platform));
            }
        }
        #endregion // Public Methods

        #region Private Methods

        /// <summary>
        /// This method creates a ticket for a specific NP user, optionally in the context of a title. This ticket will be usable with
        /// user-centric services like UserStorage, and if a title is specified it will be usable with title-specific services like GameServices.
        /// </summary>
        /// <param name="username">Admin account username.</param>
        /// <param name="password">Admin account password.</param>
        /// <param name="title">Name of title. If the ticket is intended to only be used at global scope, this should be null or empty.</param>
        /// <param name="platform">Name of platform. If the ticket is intended to only be used at global scope, this should be null or empty.</param>
        /// <param name="npOnlineId">NP online ID of user.</param>
        /// <returns>An authentication ticket that can be used for subsequent ROS queries, or null if authentication failed.</returns>
        private AuthTicket CreateTicketNp(String username, String password, String title, String npOnlineId)
        {
            AuthTicket ticket = null;

            // Construct the post data 
            // e.g. username=string&password=string&titleName=string&platformName=string&version=string&npOnlineId=string
            String postData = String.Format("username={0}&password={1}&titleName={2}&platformName={3}&version={4}&npOnlineId={5}",
                                            username,
                                            password,
                                            title,
                                            ROSPlatform.PS3,
                                            Version,
                                            npOnlineId);

            // Run the query
            try
            {
                using (Stream response = RunQuery(String.Format("{0}/{1}", ServiceUri.ToString(), "CreateTicketNp"), "POST", postData))
                {
                    if (response != null)
                    {
                        XDocument xDoc = XDocument.Load(response);
                        ticket = new AuthTicket(xDoc.Root, username, password, ROSPlatform.PS3);
                    }
                }
            }
            catch (System.Exception ex)
            {
                Log.ExceptionCtx(c_logCtx, ex, "An error occurred while retrieving an Xbl authentication ticket.");
                ticket = null;
            }

            return ticket;
        }

        /// <summary>
        /// This method creates a ticket for a specific XBL user, optionally in the context of a title. This ticket will be usable with
        /// user-centric services like UserStorage, and if a title is specified it will be usable with title-specific services like GameServices.
        /// </summary>
        /// <param name="username"> Admin account username.</param>
        /// <param name="password">Admin account password.</param>
        /// <param name="title">Name of title. If the ticket is intended to only be used at global scope, this should be null or empty.</param>
        /// <param name="platform">Name of platform. If the ticket is intended to only be used at global scope, this should be null or empty.</param>
        /// <param name="xuid">Decimal XUID of user.</param>
        /// <param name="gamertag">Gamertag of user.</param>
        /// <returns>An authentication ticket that can be used for subsequent ROS queries, or null if authentication failed.</returns>
        private AuthTicket CreateTicketXbl(String username, String password, String title, String xuid, String gamertag)
        {
            AuthTicket ticket = null;

            // Construct the post data 
            // e.g. username=string&password=string&titleName=string&platformName=string&version=string&npOnlineId=string
            String postData = String.Format("username={0}&password={1}&titleName={2}&platformName={3}&version={4}&xuid={5}&gamertag={6}",
                                            username,
                                            password,
                                            title,
                                            ROSPlatform.Xbox360,
                                            Version,
                                            xuid,
                                            gamertag);

            // Run the query
            try
            {
                using (Stream response = RunQuery(String.Format("{0}/{1}", ServiceUri.ToString(), "CreateTicketXbl"), "POST", postData))
                {
                    if (response != null)
                    {
                        XDocument xDoc = XDocument.Load(response);
                        ticket = new AuthTicket(xDoc.Root, username, password, ROSPlatform.Xbox360);
                    }
                }
            }
            catch (System.Exception ex)
            {
                Log.ExceptionCtx(c_logCtx, ex, "An error occurred while retrieving an Xbl authentication ticket.");
                ticket = null;
            }

            return ticket;
        }

        /// <summary>
        /// 
        /// </summary>
        private void WaitForQueryLimits()
        {
            // Have we made more than the max login attempts?
            if (m_loginRequests.Count() >= c_maxLoginAttempts)
            {
                // Are over the tiem frame so that we are allowed to send another request?
                TimeSpan elapsed = DateTime.Now - m_loginRequests.First.Value;

                if (elapsed.TotalSeconds > c_loginTimeFrame)
                {
                    m_loginRequests.RemoveFirst();
                }
                else
                {
                    // If not, wait until we can
                    Thread.Sleep((c_loginTimeFrame - (int)elapsed.TotalSeconds) * 1000);
                }
            }

            // Add the current time to the end of the list of when requests were made
            m_loginRequests.AddLast(DateTime.Now);
        }
        #endregion // Private Methods
    } // AuthenticationService
}
