﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Configuration.ROS;
using RSG.Base.Configuration;

namespace RSG.ROS
{
    /// <summary>
    /// Admin authentication service
    /// </summary>
    public class AdminAuthService : AuthenticationService
    {
        #region Properties
        /// <summary>
        /// Uri to query the ROS service for admin authentication
        /// </summary>
        protected override Uri ServiceUri
        {
            get
            {
                return m_serviceUri;
            }
        }
        private Uri m_serviceUri;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public AdminAuthService()
            : this(ConfigFactory.CreateROSConfig())
        {
        }

        /// <summary>
        /// Constructor that takes a config game view as an argument.
        /// (This is the preferred constructor.)
        /// </summary>
        /// <param name="gv"></param>
        public AdminAuthService(IROSConfig config)
            : base(config)
        {
            if (!config.AuthServiceUrls.ContainsKey("admin"))
            {
                throw new ArgumentNullException("Unable to initialise the admin authentication ROS Service as no appropriate entry exists in the config xml");
            }
            m_serviceUri = new Uri(BaseUri, config.AuthServiceUrls["admin"]);
        }
        #endregion // Constructor(s)

        #region Public Methods
        // Potential methods:
        //public AuthTicket CreateTicket(string username, string password);
        //public string ExamineTicket(AuthTicket ticket, string username, string password);
        #endregion // AuthenticationService Implementation
    } // AdminAuthService
}
