﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RSG.ROS.Accounts
{
    /// <summary>
    /// Details about a country that the ROS services knows about.
    /// </summary>
    public class CountryInfo
    {
        #region Constants
        // Attribute constants.
        private const String c_codeAtt = "Code";
        private const String c_nameAtt = "Name";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Country code (e.g US, GB, etc..).
        /// </summary>
        public String Code { get; set; }

        /// <summary>
        /// Friendly name for the country.
        /// </summary>
        public String Name { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor that deserialises the required information from xml.
        /// Exceptions are allowed and should be handled by the invoker of the constructor.
        /// </summary>
        public CountryInfo(XElement xElem)
        {
            // Extract the data we are after.
            XAttribute codeAtt = xElem.Attribute(c_codeAtt);
            Debug.Assert(codeAtt != null, "Code attribute is missing.");
            if (codeAtt == null)
            {
                throw new ArgumentNullException("Code attribute is missing.");
            }
            Code = codeAtt.Value;

            XAttribute nameAtt = xElem.Attribute(c_nameAtt);
            Debug.Assert(nameAtt != null, "Name attribute is missing.");
            if (nameAtt == null)
            {
                throw new ArgumentNullException("Name attribute is missing.");
            }
            Name = nameAtt.Value;
        }
        #endregion // Constructor(s)
    } // CountryInfo
}
