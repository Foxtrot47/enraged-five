﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RSG.ROS.Accounts
{
    /// <summary>
    /// Social club account information.
    /// </summary>
    public class AccountInfo
    {
        #region Constants
        // Element constants.
        private const String c_nicknameElem = "Nickname";
        private const String c_countryCodeElem = "CountryCode";
        private const String c_dateOfBirthElem = "Dob";
        private const String c_rockstarIdElem = "RockstarId";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// User's social club nickname.
        /// </summary>
        public String Nickname { get; set; }

        /// <summary>
        /// Code of the country the user selected when signing up.
        /// </summary>
        public String CountryCode { get; set; }

        /// <summary>
        /// User's date of birth.
        /// </summary>
        public DateTime? DateOfBirth { get; set; }

        /// <summary>
        /// Rockstar id for the account.
        /// </summary>
        public long RockstarId { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor that deserialises the required information from xml.
        /// Exceptions are allowed and should be handled by the invoker of the constructor.
        /// </summary>
        public AccountInfo(XElement xElem, XNamespace ns)
        {
            // Extract the data we are after.
            // Nickname
            XElement nicknameElem = xElem.Element(ns + c_nicknameElem);
            Debug.Assert(nicknameElem != null, "Nickname element is missing.");
            if (nicknameElem == null)
            {
                throw new ArgumentNullException("Nickname element is missing.");
            }
            Debug.Assert(!String.IsNullOrEmpty(nicknameElem.Value), "Nickname element is empty.");
            if (String.IsNullOrEmpty(nicknameElem.Value))
            {
                throw new ArgumentNullException("Nickname element is empty.");
            }
            Nickname = nicknameElem.Value;

            // Country code
            XElement countryCodeElem = xElem.Element(ns + c_countryCodeElem);
            Debug.Assert(countryCodeElem != null, "Country code element is missing.");
            if (countryCodeElem == null)
            {
                throw new ArgumentNullException("Country code element is missing.");
            }
            CountryCode = countryCodeElem.Value;

            // Date of Birth
            XElement dobElem = xElem.Element(ns + c_dateOfBirthElem);
            Debug.Assert(dobElem != null, "Date of birth element is missing.");
            if (dobElem == null)
            {
                throw new ArgumentNullException("Date of birth element is missing.");
            }
            if (dobElem.Value != null)
            {
                DateTime dob;
                if (DateTime.TryParse(dobElem.Value, out dob))
                {
                    DateOfBirth = dob;
                }
            }

            // Rockstar id.
            XElement rockstarElem = xElem.Element(ns + c_rockstarIdElem);
            Debug.Assert(rockstarElem != null, "Rockstar id element is missing.");
            if (rockstarElem == null)
            {
                throw new ArgumentNullException("Rockstar id element is missing.");
            }
            RockstarId = Int64.Parse(rockstarElem.Value);
        }
        #endregion // Constructor(s)
    } // AccountInfo
}
