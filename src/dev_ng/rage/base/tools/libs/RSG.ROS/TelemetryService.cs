﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Web;
using RSG.Base.Configuration;
using RSG.Base.Configuration.ROS;
using System.Diagnostics;

namespace RSG.ROS
{
    /// <summary>
    /// 
    /// </summary>
    public class TelemetryService : GameService
    {
        #region Properties
        /// <summary>
        /// Uri to query the ROS service for admin authentication
        /// </summary>
        protected override Uri ServiceUri
        {
            get
            {
                return m_serviceUri;
            }
        }
        private Uri m_serviceUri;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public TelemetryService()
            : this(ConfigFactory.CreateROSConfig())
        {
        }

        /// <summary>
        /// Constructor that takes a config game view as an argument.
        /// (This is the preferred constructor.)
        /// </summary>
        /// <param name="gv"></param>
        public TelemetryService(IROSConfig config)
            : base(config)
        {
            Debug.Assert(config.GameServiceUrls.ContainsKey("telemetry"),
                "Unable to initialise the telemetry ROS Service as no appropriate entry exists in the config xml");
            if (!config.GameServiceUrls.ContainsKey("telemetry"))
            {
                throw new ArgumentNullException("Unable to initialise the telemetry ROS Service as no appropriate entry exists in the config xml");
            }
            m_serviceUri = new Uri(BaseUri, config.GameServiceUrls["telemetry"]);
        }
        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// Queries telemetry submissions by date range and returns a comma separated value (CSV) file containing all records in the range.
        /// </summary>
        /// <param name="ticket">Ticket obtained from Auth service. Because this is a "dev" method the ticket must have been created with one of the DevCreateTicket* web methods.</param>
        /// <param name="start">UTC start date in the format YYYY-MM-DD.</param>
        /// <param name="end">UTC end date in the format YYYY-MM-DD.</param>
        /// <returns>A CSV stream containing all records in the range.</returns>
        public Stream AdminQueryByDate(AuthTicket ticket, DateTime start, DateTime end)
        {
            String postData = String.Format("ticket={0}&start={1:s}&end={2:s}", HttpUtility.UrlEncode(ticket.EncryptedAuth), start, end);
            return RunQuery(ticket, String.Format("{0}/{1}", ServiceUri.ToString(), "AdminQueryByDate"), "POST", postData, 300000);
        }
        #endregion // Public Methods
    } // TelemetryService
}
