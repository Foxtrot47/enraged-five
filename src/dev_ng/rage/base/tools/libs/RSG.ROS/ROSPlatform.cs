﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using RSG.Base.Attributes;

namespace RSG.ROS
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public enum ROSPlatform
    {
        [EnumMember]
        [FriendlyName("Unknown")]
        [RosId(0)]
        Unknown = 0,
        
        [EnumMember]
        [FriendlyName("Xbox 360")]
        [RosId(1)]
        Xbox360 = 1,

        [EnumMember]
        [FriendlyName("Playstation 3")]
        [RosId(2)]
        PS3 = 2,

        [EnumMember]
        [FriendlyName("Social Club")]
        [RosId(3)]
        SocialClub = 3,

        [EnumMember]
        [FriendlyName("Windows")]
        [RosId(8)]
        Win = 8,

        [EnumMember]
        [FriendlyName("PlayStation 4")]
        [RosId(11)]
        PS4 = 11,

        [EnumMember]
        [FriendlyName("Xbox One")]
        [RosId(12)]
        XboxOne = 12
    } // ROSPlatform

    
    /// <summary>
    /// ROSPlatform enumeration utility and extension methods.
    /// </summary>
    public static class ROSPlatformUtils
    {
        /// <summary>
        /// Return friendly name String for a ROSPlatform enum value.
        /// </summary>
        public static String PlatformToFriendlyName(this ROSPlatform platform)
        {
            Type type = platform.GetType();

            FieldInfo fi = type.GetField(platform.ToString());
            FriendlyNameAttribute[] attributes =
                fi.GetCustomAttributes(typeof(FriendlyNameAttribute), false) as FriendlyNameAttribute[];

            return (attributes.Length > 0 ? attributes[0].FriendlyName : null);
        }

        /// <summary>
        /// Returns the ROS id associated with the platform.
        /// </summary>
        /// <param name="platform"></param>
        /// <returns></returns>
        public static int PlatformToRosId(this ROSPlatform platform)
        {
            Type type = platform.GetType();
            
            FieldInfo fi = type.GetField(platform.ToString());
            RosIdAttribute[] attributes =
                fi.GetCustomAttributes(typeof(RosIdAttribute), false) as RosIdAttribute[];
            Debug.Assert(attributes.Any(), String.Format("ROSPlatform {0} is missing the RosIdAttribute.", platform));
            if (!attributes.Any())
            {
                throw new ArgumentNullException(String.Format("ROSPlatform {0} is missing the RosIdAttribute.", platform));
            }

            return attributes[0].Id;
        }

        /// <summary>
        /// Return ROSPlatform value from String representation.
        /// </summary>
        /// <param name="platform">ROSPlatform string.</param>
        /// <returns></returns>
        public static ROSPlatform ROSPlatformFromString(String platform)
        {
            return (ROSPlatform)Enum.Parse(typeof(ROSPlatform), platform);
        }

        /// <summary>
        /// Retrieves the list
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<ROSPlatform> GetValidPlatforms()
        {
            return ((ROSPlatform[])Enum.GetValues(typeof(ROSPlatform))).Except(new ROSPlatform[] { ROSPlatform.Unknown });
        }

        /// <summary>
        /// 
        /// </summary>
        private static IDictionary<int, ROSPlatform> RosIdToPlatformLookup { get; set; }

        /// <summary>
        /// Converts a rosId into a ROS platform.
        /// </summary>
        public static ROSPlatform ROSPlatformFromId(int rosId)
        {
            // If this is the first time we are calling this method, initialise the dictionary lookup.
            if (RosIdToPlatformLookup == null)
            {
                RosIdToPlatformLookup = new Dictionary<int, ROSPlatform>();

                foreach (ROSPlatform plat in GetValidPlatforms())
                {
                    RosIdToPlatformLookup[plat.PlatformToRosId()] = plat;
                }
            }

            // Try and get the platform for the passed in id.
            ROSPlatform platform;
            if (!RosIdToPlatformLookup.TryGetValue(rosId, out platform))
            {
                Debug.Fail("Unknown ROS id encountered.");
                platform = ROSPlatform.Unknown;
            }
            return platform;
        }
    } // ROSPlatformUtils
}
