﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using RSG.Base.Extensions;

namespace RSG.ROS
{
    /// <summary>
    /// Authorization ticket to use when communicating with ROS
    /// </summary>
    public class AuthTicket
    {
        #region Properties
        /// <summary>
        /// Encrypted authentication ticket as returned from ROS.
        /// </summary>
        public String EncryptedAuth { get; protected set; }

        /// <summary>
        /// Flag indicating whether the ticket is valid.
        /// </summary>
        public bool IsValid
        {
            get
            {
                return !String.IsNullOrEmpty(EncryptedAuth) && DateTime.Now < ExpirationTime;
            }
        }

        /// <summary>
        /// The login credentials that are to be used with this connection.
        /// </summary>
        public String LoginCredentials { get; private set; }

        /// <summary>
        /// Time when this authentication ticket will expire.
        /// </summary>
        public DateTime ExpirationTime { get; private set; }

        /// <summary>
        /// Platform this authentication ticket is for.
        /// </summary>
        public ROSPlatform Platform { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="elem"></param>
        public AuthTicket(XElement rootElem, String username, String password, ROSPlatform platform)
        {
            Platform = platform;

            // Parse the xml next.
            XNamespace ns = "CreateTicketResponse";
            XElement ticketElem = rootElem.Element(ns + "Ticket");
            XElement timeElem = rootElem.Element(ns + "PosixTime");
            XElement expirationElem = rootElem.Element(ns + "SecsUntilExpiration");

            // Validate the data.
            Debug.Assert(ticketElem != null && timeElem != null && expirationElem != null,
                "CreateTicketResponse doesn't contain one of the required elements.");
            if (ticketElem == null || timeElem == null || expirationElem == null)
            {
                throw new ArgumentException("CreateTicketResponse doesn't contain one of the required elements.");
            }

            EncryptedAuth = ticketElem.Value;
            ExpirationTime = DateTimeUtils.CreateFromEpochSeconds(UInt64.Parse(timeElem.Value)).AddSeconds(UInt64.Parse(expirationElem.Value));

            // Construct the username/password
            String usernamePassword = String.Format("{0}:{1}", username, password);
            LoginCredentials = Convert.ToBase64String(new ASCIIEncoding().GetBytes(usernamePassword));
        }
        #endregion // Constructor(s)
    } // AuthTicket
}
