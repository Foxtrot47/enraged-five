﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.IO;
using RSG.Base.Configuration.ROS;

namespace RSG.ROS
{
    /// <summary>
    /// Common base class for the various game services
    /// </summary>
    public abstract class GameService : ROSService
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="gv"></param>
        public GameService(IROSConfig config)
            : base(config)
        {
        }
        #endregion // Constructor(s)
    } // GameService
}
