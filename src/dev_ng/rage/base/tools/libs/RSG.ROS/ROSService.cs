﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Xml.Linq;
using RSG.Base.Logging;
using RSG.Base.Configuration.ROS;

namespace RSG.ROS
{
    /// <summary>
    /// Common base class for all ROS based services.  Contains core functions 
    /// used to communicate with ROS.
    /// </summary>
    public abstract class ROSService
    {
        #region Constants/Statics
        /// <summary>
        /// Default timeout for ROS queries.
        /// </summary>
        private const int DEFAULT_TIMEOUT = 60000;

        /// <summary>
        /// Context to use for log messages.
        /// </summary>
        private const String c_logCtx = "ROS Queries";

        /// <summary>
        /// Log to be used by the ROS services.
        /// </summary>
        public static ILog Log { get; set; }
        #endregion // Constants/Statics

        #region Properties
        /// <summary>
        /// Base url to which all ROS queries will be sent.
        /// This is generally something along the lines of https://dev.ros.rockstargames.com/.
        /// </summary>
        protected Uri BaseUri { get; private set; }

        /// <summary>
        /// Uri to use to access the concrete versions of ROS services
        /// </summary>
        protected abstract Uri ServiceUri { get; }

        /// <summary>
        /// Version of the ROS services to use
        /// </summary>
        protected uint Version { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor that takes a config game view as an argument.
        /// </summary>
        /// <param name="gv"></param>
        public ROSService(IROSConfig config)
        {
            BaseUri = new Uri(String.Format("http{0}://{1}.{2}/", (config.UseSSL ? "s" : ""), config.ServerEnvironment, config.Server));
            Version = config.Version;
        }

        /// <summary>
        /// Static constructor for setting the certificate validation callback
        /// </summary>
        static ROSService()
        {
            Log = LogFactory.CreateUniversalLog("RSG.ROS");

#if DEBUG
            // The following is to allow us to use untrusted https connections (which is the case if you're running a local bugstar rest service).
            // It solves the following exception from being thrown:
            // System.Net.WebException: The underlying connection was closed: Could not establish trust relationship for the SSL/TLS secure channel.
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
#endif
        }
        #endregion

        #region Protected Methods
        /// <summary>
        /// Runs a simple get query returning the resulting stream of data
        /// </summary>
        /// <param name="uri"></param>
        /// <returns></returns>
        protected Stream RunQuery(String uri)
        {
            return RunQuery(null, uri, "GET", null, DEFAULT_TIMEOUT);
        }

        /// <summary>
        /// Runs a simple get query returning the resulting stream of data.
        /// </summary>
        protected Stream RunQuery(AuthTicket ticket, String uri)
        {
            return RunQuery(ticket, uri, "GET", null, DEFAULT_TIMEOUT);
        }

        /// <summary>
        /// 
        /// </summary>
        protected Stream RunQuery(String uri, String method, String postData)
        {
            return RunQuery(null, uri, method, postData, DEFAULT_TIMEOUT);
        }

        /// <summary>
        /// 
        /// </summary>
        protected Stream RunQuery(AuthTicket ticket, String uri, String method, String postData)
        {
            return RunQuery(ticket, uri, method, postData, DEFAULT_TIMEOUT);
        }

        /// <summary>
        /// Runs a query returning the resulting stream of data.
        /// </summary>
        /// <param name="timeout">Timeout in milliseconds</param>
        protected Stream RunQuery(AuthTicket ticket, String uri, String method, String postData, int timeout)
        {
            Log.MessageCtx(c_logCtx, "ROS Query: {0} [{1}]", uri, method);
            if (postData != null && method == "POST")
            {
                Log.MessageCtx(c_logCtx, "Post Data: {0}", postData);
            }

            try
            {
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(uri);
                request.Method = method;
                request.Accept = "*/*";
                request.AllowAutoRedirect = true;
                request.UserAgent = "HTTP/1.1";
                request.Timeout = timeout;
                request.Method = method;
                if (ticket != null && ticket.IsValid)
                {
                    request.Headers.Add("Authorization", "Basic " + ticket.LoginCredentials);
                }

                // Check whether we need to post any data
                if (postData != null)
                {
                    byte[] byteData = Encoding.UTF8.GetBytes(postData);
                    request.ContentLength = byteData.Length;
                    request.ContentType = "application/x-www-form-urlencoded";

                    // Write data  
                    using (Stream postStream = request.GetRequestStream())
                    {
                        postStream.Write(byteData, 0, byteData.Length);
                    }
                }

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                using (Stream responseStream = response.GetResponseStream())
                {
                    return ValidateResponse(responseStream, response);
                }
            }
            catch (System.Net.WebException ex)
            {
                Log.ExceptionCtx(c_logCtx, ex, "Unexpected exception received while attempting run a {0} query on {1}.", method, uri);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private MemoryStream ValidateResponse(Stream responseStream, HttpWebResponse response)
        {
            // Create a copy of the response stream so that we can read from it more than once
            MemoryStream stream = null;

            if (response != null)
            {
                stream = new MemoryStream();
                responseStream.CopyTo(stream);
                stream.Position = 0;

                // Check whether the returned data is xml.
                if (response.ContentType.Contains("text/xml") || response.ContentType.Contains("application/xml"))
                {
                    // If so, make sure no error's were returned.
                    try
                    {
                        XDocument xDoc = XDocument.Load(stream);
                        XElement xElem = xDoc.Root.Element("Error");

                        if (xElem != null)
                        {
                            // If we've received an error, log it and return null
                            string code = "";
                            XAttribute xAtt = xElem.Attribute("Code");
                            if (xAtt != null)
                            {
                                code = xAtt.Value;
                            }

                            string codeEx = "";
                            xAtt = xElem.Attribute("CodeEx");
                            if (xAtt != null)
                            {
                                codeEx = xAtt.Value;
                            }

                            string msg = "";
                            XElement msgElem = xElem.Element("Msg");
                            if (msgElem != null)
                            {
                                msg = msgElem.Value;
                            }

                            Log.ErrorCtx(c_logCtx, "Unexpected error received while attempting to run a {0} query on {1}.", response.Method, response.ResponseUri);
                            Log.ErrorCtx(c_logCtx, "Error: {0}", msg);

                            stream.Dispose();
                            stream = null;
                        }
                    }
                    catch (System.Exception e)
                    {
                        Log.ErrorCtx(c_logCtx, "Unexpected error occurred while trying to validate an xml response: {0}", e.Message);
                    }

                    // Reset the stream
                    if (stream != null)
                    {
                        stream.Position = 0;
                    }
                }
            }

            return stream;
        }
        #endregion // Protected Methods
    } // ROSService
}
