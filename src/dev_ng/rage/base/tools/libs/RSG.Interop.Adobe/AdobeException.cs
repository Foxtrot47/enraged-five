﻿//---------------------------------------------------------------------------------------------
// <copyright file="Photoshop.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------
using System.Windows.Forms;

namespace RSG.Interop.Adobe
{
    using System;

    /// <summary>
    /// Adobe Exception class.
    /// </summary>
    public class AdobeInteropException : Exception
    {
        #region Constructor(s)
        public AdobeInteropException()
            : base()
        {
        }

        public AdobeInteropException(String message)
            : base(message)
        {
        }

        public AdobeInteropException(String message, Exception inner)
            : base(message, inner)
        {
        }

        public AdobeInteropException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context)
            : base(info, context)
        {
        }
        #endregion // Constructor(s)
    }

    /// <summary>
    /// Accessing COM properties while Photoshop is currently active doing some operations causes our application
    /// to throw an exception and crash.  Use this to warn the user in those cases.
    /// </summary>
    public class AdobeInteropBusyException : Exception
    {
        public AdobeInteropBusyException(bool quit = true)
            : base()
        {
            MessageBox.Show("Photoshop is currently busy!  Please make sure there are no other tools or operations currently active in Photoshop.  Some things that will cause this are:\n\n- File dialog is open.\n- Image cropping.", "Rockstar", MessageBoxButtons.OK, MessageBoxIcon.Error);

            if (quit)
            {
                Environment.Exit(0);
            }
        }
    }

} // RSG.Interop.Autodesk3dsmax namespace
