﻿//---------------------------------------------------------------------------------------------
// <copyright file="PhotoshopVersion.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Adobe
{
    using System;
    using System.ComponentModel;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Reflection;
    using RSG.Base.Attributes;

    /// <summary>
    /// Photoshop version enum (with useful annotations).
    /// </summary>
    [DataContract]
    public enum PhotoshopVersion
    {
        [EnumMember]
        None,

        [EnumMember]
        [FieldDisplayName("Adobe Photoshop CS")]
        [RegistryKey(@"Software\Adobe\Photoshop\8.0", true)]
        CS,

        [EnumMember]
        [FieldDisplayName("Adobe Photoshop CS2")]
        [RegistryKey(@"Software\Adobe\Photoshop\9.0")]
        CS2,

        [EnumMember]
        [FieldDisplayName("Adobe Photoshop CS3")]
        [RegistryKey(@"Software\Adobe\Photoshop\10.0")]
        CS3,

        [EnumMember]
        [FieldDisplayName("Adobe Photoshop CS4")]
        [RegistryKey(@"Software\Adobe\Photoshop\11.0")]
        CS4,

        [EnumMember]
        [FieldDisplayName("Adobe Photoshop CS5")]
        [RegistryKey(@"Software\Adobe\Photoshop\12.0")]
        CS5,
        
        [EnumMember]
        [FieldDisplayName("Adobe Photoshop CS6")]
        [RegistryKey(@"Software\Adobe\Photoshop\60.0")]
        CS6,

        [EnumMember]
        [FieldDisplayName("Adobe Photoshop Creative Cloud")]
        [RegistryKey(@"Software\Adobe\Photoshop\70.0")]
        CreativeCloud,

        [EnumMember]
        [FieldDisplayName("Adobe Photoshop Creative Cloud 2014")]
        [RegistryKey(@"Software\Adobe\Photoshop\80.0")]
        CreativeCloud2014,
    }

    /// <summary>
    /// Extension methods for the PhotohopVersion enum.
    /// </summary>
    public static class PhotoshopVersionExtensions
    {
        /// <summary>
        /// Return Photoshop display-name.
        /// </summary>
        /// <param name="version"></param>
        /// <returns></returns>
        public static String DisplayName(this PhotoshopVersion version)
        {
            Type type = version.GetType();

            FieldInfo fi = type.GetField(version.ToString());
            DisplayNameAttribute[] attributes =
                fi.GetCustomAttributes(typeof(DisplayNameAttribute), false) as DisplayNameAttribute[];

            DisplayNameAttribute attribute = attributes.FirstOrDefault();
            if (null != attribute)
                return (attribute.DisplayName);
            else
                throw (new ArgumentException("Photoshop version has no DisplayName defined."));
        }

        /// <summary>
        /// Return Photoshop version Registry Key.
        /// </summary>
        /// <param name="version"></param>
        /// <returns></returns>
        public static String RegistryKey(this PhotoshopVersion version)
        {
            Type type = version.GetType();

            FieldInfo fi = type.GetField(version.ToString());
            RegistryKeyAttribute[] attributes =
                fi.GetCustomAttributes(typeof(RegistryKeyAttribute), false) as RegistryKeyAttribute[];

            RegistryKeyAttribute attribute = attributes.Where(a => a.Is64Bit == true).FirstOrDefault();
            if (null != attribute)
                return (attribute.Key);
            else
                throw (new ArgumentException("Photoshop version has no Registry Key defined."));
        }
    }

} // RSG.Interop.Adobe namespace
