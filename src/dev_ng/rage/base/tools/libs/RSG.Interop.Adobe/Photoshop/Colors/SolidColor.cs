﻿//---------------------------------------------------------------------------------------------
// <copyright file="SolidColor.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Interop.Adobe.Photoshop
{
    /// <summary>
    /// Wrapper class for Photoshop SolidColor object.
    /// 
    /// For scripting reference, see:
    /// 
    ///     CC : http://wwwimages.adobe.com/content/dam/Adobe/en/devnet/photoshop/pdfs/photoshop-cc-vbs-ref.pdf
    ///     
    /// </summary>
    public sealed class SolidColor : ComWrapper
    {
        #region Member Data
        #endregion // Member Data

        #region Constructor(s)

        public SolidColor()
            : base()
        {
            Type optionsType = Type.GetTypeFromProgID("Photoshop.SolidColor");
            this.m_comObject = (dynamic)Activator.CreateInstance(optionsType);
        }

        public SolidColor(dynamic obj)
            : base((object)obj)
        {
        }

        #endregion // Constructor(s)

        #region Properties

        /// <summary>
        /// The RGB color mode.
        /// </summary>
        public RGBColor RGB
        {
            get { return new RGBColor(this.m_comObject.RGB); }
            set { this.m_comObject.RGB = value.ComObject; }
        }

        /// <summary>
        /// The HSB color mode.
        /// </summary>
        public HSBColor HSB
        {
            get { return new HSBColor(this.m_comObject.HSB); }
            set { this.m_comObject.HSB = value.ComObject; }
        }

        #endregion // Properties

        #region Methods

        /// <summary>
        /// Indicates whether the SolidColor object is visually equal to the specified color.
        /// </summary>
        /// <param name="color"></param>
        /// <returns></returns>
        public bool IsEqual(SolidColor color)
        {
            return this.m_comObject.IsEqual(color.ComObject);
        }

        #endregion // Methods
    }
}
