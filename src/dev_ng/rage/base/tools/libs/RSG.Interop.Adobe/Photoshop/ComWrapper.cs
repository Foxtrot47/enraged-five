﻿//---------------------------------------------------------------------------------------------
// <copyright file="ComWrapper.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Interop.Adobe.Photoshop
{
    /// <summary>
    /// Base class for Photoshop COM wrapper classes.
    /// </summary>
    public abstract class ComWrapper
    {
        #region Member Data

        protected dynamic m_comObject;

        #endregion // Member Data

        #region Constructor(s)

        public ComWrapper()
        {
        }

        public ComWrapper(dynamic comObject)
        {
            this.m_comObject = comObject;
        }

        #endregion // Constructor(s)

        #region Properties

        /// <summary>
        /// Access to the COM object for this wrapper.
        /// </summary>
        public dynamic ComObject
        {
            get { return this.m_comObject; }
        }

        #endregion // Properties
    }
}
