﻿//---------------------------------------------------------------------------------------------
// <copyright file="ArtLayer.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Interop.Adobe.Photoshop
{
    /// <summary>
    /// Wrapper class for Photoshop ArtLayer object.
    /// 
    /// For scripting reference, see:
    /// 
    ///     CC : http://wwwimages.adobe.com/content/dam/Adobe/en/devnet/photoshop/pdfs/photoshop-cc-vbs-ref.pdf
    ///     
    /// </summary>
    public sealed class ArtLayer : ComWrapper, ILayer
    {
        #region Member Data
        #endregion // Member Data

        #region Constructor(s)

        public ArtLayer(dynamic artLayer)
            : base((object)artLayer)
        {
        }

        #endregion // Constructor(s)

        #region Properties

        /// <summary>
        /// The name of the ArtLayer.
        /// </summary>
        public String Name
        {
            get { return this.m_comObject.Name; }
            set { this.m_comObject.Name = value; }
        }

        /// <summary>
        /// The visibility of the ArtLayer.
        /// </summary>
        public bool Visible
        {
            get { return this.m_comObject.Visible; }
            set { this.m_comObject.Visible = value; }
        }

        /// <summary>
        /// The kind of ArtLayer.
        /// 
        /// Sets the layer's kind for an empty layer.  Valid only when the layer is empty and when the IsBackgroundLayer is false.
        /// 
        /// Note: You can use the Kind property to make a background layer a normal layer; however, to make a layer a background
        /// layer, you must set IsBackgroundLayer to true.
        /// </summary>
        public LayerKind Kind
        {
            get { return (LayerKind)this.m_comObject.Kind; }
            set { this.m_comObject.Kind = value; }
        }

        /// <summary>
        /// Indicates whether the layer is a background layer or normal layer.
        /// </summary>
        public bool IsBackgroundLayer
        {
            get { return this.m_comObject.IsBackgroundLayer; }
            set { this.m_comObject.IsBackgroundLayer = value; }
        }

        /// <summary>
        /// The master opacity of the layer (0.0 - 100.0).
        /// </summary>
        public double Opacity
        {
            get { return this.m_comObject.Opacity; }
            set { this.m_comObject.Opacity = value; }
        }

        /// <summary>
        /// The interior opacity of the layer (0.0 - 100.0).
        /// </summary>
        public double FillOpacity
        {
            get { return this.m_comObject.FillOpacity; }
            set { this.m_comObject.FillOpacity = value; }
        }

        /// <summary>
        /// The layer's blending mode.
        /// </summary>
        public BlendMode BlendMode
        {
            get { return (BlendMode)this.m_comObject.BlendMode; }
            set { this.m_comObject.BlendMode = value; }
        }

        /// <summary>
        /// Indicates whether to completely lock the layer's contents and settings.
        /// </summary>
        public bool AllLocked
        {
            get { return this.m_comObject.AllLocked; }
            set { this.m_comObject.AllLocked = value; }
        }

        #endregion // Properties

        #region Methods

        /// <summary>
        /// Creates a duplicate of the object on the screen.
        /// </summary>
        /// <param name="relativeObject"></param>
        /// <param name="insertLocation"></param>
        /// <returns></returns>
        public ArtLayer Duplicate(ILayer relativeObject, ElementPlacement insertLocation)
        {
            return new ArtLayer(this.m_comObject.Duplicate(relativeObject.ComObject, insertLocation));
        }

        /// <summary>
        /// Moves the layer relative to the object specified in the parameters.
        /// </summary>
        /// <param name="relativeObject"></param>
        /// <param name="insertLocation">Only PlaceBefore and PlaceAfter are valid.</param>
        public void Move(ILayer relativeObject, ElementPlacement insertLocation)
        {
            this.m_comObject.Move(relativeObject.ComObject, insertLocation);
        }

        /// <summary>
        /// Inverts the colors in the layer by converting the brightness value of each
        /// pixel in the channels to the inverse value on the 256-step color-values scale.
        /// </summary>
        public void Invert()
        {
            this.m_comObject.Invert();
        }

        /// <summary>
        /// Cuts the layer without moving it to the clipboard.
        /// </summary>
        public void Clear()
        {
            this.m_comObject.Clear();
        }

        #endregion // Methods
    }
}
