﻿//---------------------------------------------------------------------------------------------
// <copyright file="ArtLayers.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Interop.Adobe.Photoshop
{
    /// <summary>
    /// The collection of ArtLayer objects in the document.
    /// </summary>
    public sealed class ArtLayers : List<ArtLayer>
    {
        #region Member Data

        // The real Photoshop ArtLayers collection object.
        private dynamic m_comObject;

        #endregion // Member Data

        #region Constructor(s)

        public ArtLayers(dynamic artLayersComObject)
            : base()
        {
            this.m_comObject = artLayersComObject;

            foreach (dynamic realArtLayer in artLayersComObject)
            {
                this.Add(new ArtLayer(realArtLayer));
            }
        }

        #endregion // Constructor(s)

        #region Properties

        /// <summary>
        /// Access to the real COM object for this wrapper.
        /// </summary>
        public dynamic ComObject
        {
            get { return this.m_comObject; }
        }

        #endregion // Properties

        #region Methods

        /// <summary>
        /// Creates a new ArtLayer object.
        /// </summary>
        /// <returns></returns>
        public ArtLayer Add()
        {
            // Create a new ArtLayer object.
            ArtLayer layer = new ArtLayer(this.m_comObject.Add());

            // Add it to this collection.
            this.Add(layer);
            return layer;
        }

        #endregion // Methods
    }
}
