﻿//---------------------------------------------------------------------------------------------
// <copyright file="Application.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Windows.Forms;

using RSG.Interop.Adobe;

namespace RSG.Interop.Adobe.Photoshop
{
    /// <summary>
    /// Wrapper class for the Photoshop Application object.
    /// 
    /// For scripting reference, see:
    /// 
    ///     CC : http://wwwimages.adobe.com/content/dam/Adobe/en/devnet/photoshop/pdfs/photoshop-cc-vbs-ref.pdf
    ///     
    /// </summary>
    public sealed class Application : ComWrapper
    {
        #region Member Data
        #endregion // Member Data

        #region Constructor(s)

        public Application()
            : base()
        {
            this.m_comObject = PhotoshopApplication.CreatePhotoshopComObject();
        }

        #endregion // Constructor(s)

        #region Properties

        /// <summary>
        /// Returns the active Photoshop document.
        /// </summary>
        public Document ActiveDocument
        {
            get { return new Document(this.m_comObject.ActiveDocument); }
            set { this.m_comObject.ActiveDocument = value.ComObject; }
        }

        /// <summary>
        /// The default foreground color (used to paint, fill and stroke selections).
        /// </summary>
        public SolidColor ForegroundColor
        {
            get { return new SolidColor(this.m_comObject.ForegroundColor); }
            set { this.m_comObject.ForegroundColor = value.ComObject; }
        }

        /// <summary>
        /// The color for the document's background color.
        /// </summary>
        public SolidColor BackgroundColor
        {
            get { return new SolidColor(this.m_comObject.BackgroundColor); }
            set { this.m_comObject.BackgroundColor = value.ComObject; }
        }

        /// <summary>
        /// The Documents collection.
        /// </summary>
        public Documents Documents
        {
            get { return new Documents(this.m_comObject.Documents); }
        }

        /// <summary>
        /// The application preference settings.
        /// </summary>
        public Preferences Preferences
        {
            get { return new Preferences(this.m_comObject.Preferences); }
        }

        /// <summary>
        /// Indicates whether there is an active document.
        /// </summary>
        public bool HasActiveDocument
        {
            get
            {
                if (this.m_comObject != null)
                {
                    if (this.m_comObject.Documents.Count > 0)
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        /// <summary>
        /// The version of Photoshop this Application object is using.
        /// </summary>
        public PhotoshopVersion Version
        {
            get 
            { 
                PhotoshopVersion version;
                String currentVersion = "";

                // So accessing some thing in Photoshop causes an exception if Photoshop is currently busy in an operation, or certain 
                // dialogs are open.  F*king Photoshop.
                try
                {
                    currentVersion = (String)this.m_comObject.Version;
                }
                catch
                {
                    throw new AdobeInteropBusyException();
                }

                if (currentVersion.StartsWith("8"))
                {
                    version = PhotoshopVersion.CS;
                }
                else if (currentVersion.StartsWith("9"))
                {
                    version = PhotoshopVersion.CS2;
                }
                else if (currentVersion.StartsWith("10"))
                {
                    version = PhotoshopVersion.CS3;
                }
                else if (currentVersion.StartsWith("11"))
                {
                    version = PhotoshopVersion.CS4;
                }
                else if (currentVersion.StartsWith("12"))
                {
                    version = PhotoshopVersion.CS5;
                }
                else if (currentVersion.StartsWith("13"))
                {
                    version = PhotoshopVersion.CS6;
                }
                else if (currentVersion.StartsWith("14"))
                {
                    version = PhotoshopVersion.CreativeCloud;
                }
                else if (currentVersion.StartsWith("15"))
                {
                    version = PhotoshopVersion.CreativeCloud2014;
                }
                else
                {
                    version = PhotoshopVersion.None;
                }

                return version;
            }
        }

        #endregion // Properties

        #region Methods

        /// <summary>
        /// Get the handle of the Photoshop application window.
        /// </summary>
        /// <returns></returns>
        public IntPtr GetWindowHandle()
        {
            IntPtr winHandle = IntPtr.Zero;
            Process[] processes = Process.GetProcessesByName("Photoshop");

            foreach (Process p in processes)
            {
                winHandle = p.MainWindowHandle;
                return winHandle;
            }

            return winHandle;
        }

        /// <summary>
        /// Checks to see if there is a connection to Photoshop.  Attempts to create a connection if one doesn't exist.
        /// </summary>
        /// <returns></returns>
        public bool IsConnected()
        {
            // Test to see if we have an active connection.
            try
            {
                if (this.m_comObject != null)
                {
                    string test = this.m_comObject.Build;
                    return true;
                }
                else
                {
                    this.m_comObject = RSG.Interop.Adobe.PhotoshopApplication.CreatePhotoshopComObject();
                }
            }
            catch
            {
                // Failed, so attempt to re-establish.
                this.m_comObject = RSG.Interop.Adobe.PhotoshopApplication.CreatePhotoshopComObject();
            }

            // Follow-up test to see if the re-connection was successful.
            try
            {
                if (this.m_comObject != null)
                {
                    string test = this.m_comObject.Build;
                    return true;
                }
            }
            catch { }

            return false;
        }

        /// <summary>
        /// Opens a new file in Photoshop.
        /// </summary>
        /// <param name="filename">The file to open.</param>
        /// <returns>The opened Document.</returns>
        public Document Open(String filename)
        {
            return new Document(this.m_comObject.Open(filename));
        }

        /// <summary>
        /// Quits the Photoshop application.
        /// </summary>
        public void Quit()
        {
            this.m_comObject.Quit();
        }

        /// <summary>
        /// Plays an ActionManager event.
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="descriptor"></param>
        /// <param name="displayDialogs"></param>
        /// <returns></returns>
        public ActionDescriptor ExecuteAction(int eventId, ActionDescriptor descriptor, DialogModes displayDialogs)
        {
            return new ActionDescriptor(this.m_comObject.ExecuteAction(eventId, descriptor.ComObject, displayDialogs));
        }

        /// <summary>
        /// Obtains an ActionDescriptor.
        /// </summary>
        /// <param name="reference"></param>
        /// <returns></returns>
        public ActionDescriptor ExecuteActionGet(ActionReference reference)
        {
            return new ActionDescriptor(this.m_comObject.ExecuteActionGet(reference.ComObject));
        }

        /// <summary>
        /// Converts from a four character code (character ID) to a runtime ID.
        /// </summary>
        /// <param name="charID"></param>
        /// <returns></returns>
        public int CharIDToTypeID(String charID)
        {
            return this.m_comObject.CharIDToTypeID(charID);
        }

        /// <summary>
        /// Converts from a String ID to a runtime ID.
        /// </summary>
        /// <param name="stringID"></param>
        /// <returns></returns>
        public int StringIDToTypeID(String stringID)
        {
            return this.m_comObject.StringIDToTypeID(stringID);
        }

        #endregion // Methods
    }
}
