﻿//---------------------------------------------------------------------------------------------
// <copyright file="Preferences.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Interop.Adobe.Photoshop
{
    public sealed class Preferences : ComWrapper
    {
        #region Member Data
        #endregion // Member Data

        #region Constructor(s)

        public Preferences(dynamic preferencesComObject)
            : base((object)preferencesComObject)
        {
        }

        #endregion // Constructor(s)

        #region Properties

        /// <summary>
        /// The unit the scripting system will use when receiving and returning values.
        /// </summary>
        public Units RulerUnits
        {
            get { return (Units)this.m_comObject.RulerUnits; }
            set { this.m_comObject.RulerUnits = value; }
        }

        #endregion
    }
}
