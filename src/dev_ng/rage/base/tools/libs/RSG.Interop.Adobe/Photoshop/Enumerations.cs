﻿//---------------------------------------------------------------------------------------------
// <copyright file="Enumerations.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace RSG.Interop.Adobe.Photoshop
{
    /// <summary>
    /// The object's position in the Layers palette.  Not all values are valid for all object types.
    /// </summary>
    public enum ElementPlacement
    {
        PlaceInside = 0,
        PlaceAtBeginning,
        PlaceAtEnd,
        PlaceBefore,
        PlaceAfter
    }

    /// <summary>
    /// The number of bits per color channel.
    /// </summary>
    public enum BitsPerChannelType
    {
        Document1Bit    = 1,
        Document8Bits   = 8,
        Document16Bits  = 16,
        Document32Bits  = 32
    }

    /// <summary>
    /// The save options to use when the Close() method is called to close a document.
    /// </summary>
    public enum SaveOptions
    {
        SaveChanges = 1,
        DoNotSaveChanges,
        PromptToSaveChanges
    }

    /// <summary>
    /// The order in which bytes will be read.
    /// </summary>
    public enum ByteOrder
    {
        IBMByteOrder = 1,
        MacOSByteOrder
    }

    /// <summary>
    /// The encoding type to use for TIFF files.
    /// </summary>
    public enum TIFFEncodingType
    {
        NoTIFFCompression = 1,
        TiffLZW,
        TiffJPEG,
        TiffZIP
    }

    /// <summary>
    /// Compression methods for data for pixels in layers.
    /// </summary>
    public enum LayerCompressionType
    {
        RLELayerCompression = 1,
        ZIPLayerCompression
    }

    /// <summary>
    /// The formatting of the extension in the filename.
    /// </summary>
    public enum ExtensionType
    {
        LowerCase = 2,
        UpperCase
    }

    /// <summary>
    /// The format in which to save a document.
    /// </summary>
    public enum SaveDocumentType
    {
        PhotoshopSave       = 1,
        BMPSave             = 2,
        CompuServeGIFSave   = 3,
        PhotoshopEPSSave    = 4,
        JPEGSave            = 6,
        PCXSave             = 7,
        PhotoshopPDFSave    = 8,
        PICTFileFormatSave  = 10,
        PixarSave           = 12,
        PNGSave             = 13,
        RawSave             = 14,
        ScitexCTSave        = 15,
        TargaSave           = 16,
        TIFFSave            = 17,
        PhotoshopDCS_1Save  = 18,
        PhotoshopDCS_2Save  = 19,
        AliasPIXSave        = 25,
        ElectricImageSave   = 26,
        PortableBitmapSave  = 27,
        WavefrontRLASave    = 28,
        SGIRGBSave          = 29,
        SoftImageSave       = 30,
        WirelessBitmapSave  = 31
    }

    /// <summary>
    /// The color profile to use for the document.
    /// </summary>
    public enum NewDocumentMode
    {
        NewGray = 1,
        NewRGB,
        NewCMYK,
        NewLab,
        NewBitmap
    }

    /// <summary>
    /// The fill of the document.
    /// </summary>
    public enum DocumentFill
    {
        White = 1,
        BackgroundColor,
        Transparent
    }

    /// <summary>
    /// The unit to use for measuring text characters.
    /// </summary>
    public enum TypeUnits
    {
        TypePixels  = 1,
        TypeMM      = 4,
        TypePoints  = 5
    }

    /// <summary>
    /// The measurement unit for type and ruler increments.
    /// </summary>
    public enum Units
    {
        Pixels = 1,
        Inches,
        CM,
        MM,
        Points,
        Picas,
        Percent
    }

    /// <summary>
    /// The method to use for image interpolation.
    /// </summary>
    public enum ResampleMethod
    {
        NoResampling = 1,
        NearestNeighbor,
        Bilinear,
        Bicubic,
        BicubicSharper,
        BicubicSmoother,
        BicubicAutomatic,
        Automatic,
        PreserveDetails
    }

    /// <summary>
    /// The resolution to use when saving an image in Targa format.
    /// </summary>
    public enum TargaBitsPerPixel
    {
        Targa16Bits = 16,
        Targa24Bits = 24,
        Targa32Bits = 32
    }

    /// <summary>
    /// Control the type (mode) of dialogs Photoshop displays when running scripts.
    /// </summary>
    public enum DialogModes
    {
        DisplayAllDialogs = 1,
        DisplayErrorDialogs,
        DisplayNoDialogs
    }

    /// <summary>
    /// The selection behavior when a selection already exists.
    /// </summary>
    public enum SelectionType
    {
        [Description("Replace the selected area.")]
        ReplaceSelection = 1,

        [Description("Add the selection to an already selected area.")]
        ExtendSelection,

        [Description("Remove the selection from the already selected area.")]
        DiminishSelection,

        [Description("Make the selection only the area where the new selection intersects the already selected area.")]
        IntersectSelection
    }

    /// <summary>
    /// Controls how pixels in the image are blended.
    /// </summary>
    public enum BlendMode
    {
        PassThrough         = 1,
        NormalBlend         = 2,
        Dissolve            = 3,
        Darken              = 4,
        Multiply            = 5,
        ColorBurn           = 6,
        LinearBurn          = 7,
        Lighten             = 8,
        Screen              = 9,
        ColorDodge          = 10,
        LinearDodge         = 11,
        Overlay             = 12,
        SoftLight           = 13,
        HardLight           = 14,
        VividLight          = 15,
        LinearLight         = 16,
        PinLight            = 17,
        Difference          = 18,
        Exclusion           = 19,
        Hue                 = 20,
        SaturationBlend     = 21,
        ColorBlend          = 22,
        Luminosity          = 23,
        HardMix             = 26,
        LighterColor        = 27,
        DarkerColor         = 28,
        Substract           = 29,
        Divide              = 30
    }

    /// <summary>
    /// The kind of ArtLayer object.
    /// </summary>
    public enum LayerKind
    {
        NormalLayer = 1,
        TextLayer,
        SolidFillLayer,
        GradientFillLayer,
        PatternFillLayer,
        LevelsLayer,
        CurvesLayer,
        ColorBalanceLayer,
        BrightnessContrastLayer,
        HueSaturationLayer,
        SelectiveColorLayer,
        ChannelMixerLayer,
        GradientMapLayer,
        InversionLayer,
        ThresholdLayer,
        PosterizeLayer,
        SmartObjectLayer,
        PhotoFilterLayer,
        ExposureLayer,
        Layer3D,
        VideoLayer,
        BlackAndWhiteLayer,
        Vibrance
    }

    /// <summary>
    /// The type of layer mask to create.
    /// 
    /// Not native to the Photoshop SDK.
    /// </summary>
    public enum LayerMaskType
    {
        RevealAll,
        HideAll,
        RevealSelection,
        HideSelection
    }

    /// <summary>
    /// Color for the layer in the UI.  These options are available if you right-click on a layer in Photoshop.
    /// 
    /// Not native to the Photoshop SDK.
    /// </summary>
    public enum LayerUIColor
    {
        NoColor,
        Red,
        Orange,
        Yellow,
        Green,
        Blue,
        Violet,
        Gray
    }
}
