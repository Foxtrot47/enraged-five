﻿//---------------------------------------------------------------------------------------------
// <copyright file="Helpers.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.IO;
using System.Diagnostics;
using System.Xml;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Media.Imaging;
using System.Windows.Forms;
using System.Threading;

using RSG.Base.Configuration;
using RSG.Interop.Adobe.Photoshop;

namespace RSG.Interop.Adobe.Photoshop
{
    /// <summary>
    /// Set of helper functions that aren't native to the Photoshop SDK.
    /// </summary>
    public sealed class Helpers
    {
        #region Member Data

        private Application m_app;

        #endregion // Member Data

        #region Constructor(s)

        public Helpers(RSG.Interop.Adobe.Photoshop.Application application)
        {
            this.m_app = application;
        }

        #endregion // Constructor(s)

        # region Methods

        /// <summary>
        /// Construct a path to a layer by supplying a list of names.
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        public String ConstructLayerPath(params String[] items)
        {
            String path = "";

            foreach (String item in items)
            {
                path += String.Format("{0}/", item);
            }

            // Remove trailing '/'.
            path = path.TrimEnd('/');

            return path;
        }

        /// <summary>
        /// Use a Photoshop .csf file to load color settings for the application.
        /// </summary>
        /// <param name="colorSettingsFilename"></param>
        public void LoadColorSettings(string colorSettingsFilename)
        {
            int idsetd = this.m_app.CharIDToTypeID("setd");
            ActionDescriptor desc1 = new ActionDescriptor();
            int idnull = this.m_app.CharIDToTypeID("null");
            ActionReference ref1 = new ActionReference();
            int idPrpr = this.m_app.CharIDToTypeID("Prpr");
            int idcolorSettings = this.m_app.StringIDToTypeID("colorSettings");
            ref1.PutProperty(idPrpr, idcolorSettings);

            int idcapp = this.m_app.CharIDToTypeID("capp");
            int idOrdn = this.m_app.CharIDToTypeID("Ordn");
            int idTrgt = this.m_app.CharIDToTypeID("Trgt");
            ref1.PutEnumerated(idcapp, idOrdn, idTrgt);
            desc1.PutReference(idnull, ref1);

            int idT = this.m_app.CharIDToTypeID("T   ");
            ActionDescriptor desc2 = new ActionDescriptor();
            int idUsng = this.m_app.CharIDToTypeID("Usng");
            desc2.PutPath(idUsng, colorSettingsFilename);
            desc1.PutObject(idT, idcolorSettings, desc2);
            this.m_app.ExecuteAction(idsetd, desc1, DialogModes.DisplayNoDialogs);
        }

        /// <summary>
        /// Finds a Photoshop ArtLayer by the supplied path.  The path is in the format of:
        /// 
        ///     "LayerSetName/ArtLayerName"
        /// 
        /// For example:
        /// 
        ///     "Materials/Rust/Rusty Iron"
        /// 
        /// </summary>
        /// <param name="app"></param>
        /// <param name="doc"></param>
        /// <param name="path"></param>
        /// <param name="createLayer">If the layer doesn't exist, create it.</param>
        /// <returns></returns>
        public ArtLayer FindLayerByPath(Document doc, string path, bool createLayer = true)
        {
            if (this.m_app.IsConnected())
            {
                // Split up path delimited by a period.
                string[] layerPath = path.Split('/');
                string layerName = layerPath[layerPath.Length - 1];

                // Rebuild path, removing the last item since that is the layer name.
                string newLayerPathstr = "";

                for (int idx = 0; idx < (layerPath.Length - 1); idx++)
                {
                    string pathItem = layerPath[idx];
                    newLayerPathstr += pathItem + "/";
                }

                string newLayerPath = newLayerPathstr.TrimEnd('/');

                LayerSet layerSet = FindLayerGroupByPath(doc, newLayerPath);

                if (layerSet != null)
                {
                    foreach (ArtLayer layer in layerSet.ArtLayers)
                    {
                        if (layer.Name == layerName)
                        {
                            return layer;
                        }
                    }

                    // Layer didn't exist, so create it.
                    if (createLayer == true)
                    {
                        ArtLayer newLayer = layerSet.ArtLayers.Add();
                        newLayer.Name = layerName;

                        return newLayer;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Finds a Photoshop LayerSet by the supplied path.  The path is in the format of:
        /// 
        ///     LayerSetName/LayerSetName
        ///     
        /// For example:
        /// 
        ///     Materials/Rust
        /// 
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="path"></param>
        /// <param name="create">Create the layer path if it doesn't exist.</param>
        /// <param name="currentLayerSet">If searching from the root, leave this null.  Otherwise, you can supply a starting LayerSet.</param>
        /// <returns></returns>
        public LayerSet FindLayerGroupByPath(Document doc, string path, bool create = false, LayerSet currentLayerSet = null)
        {
            // Split up path delimited by a period.
            string[] layerPath = path.Split('/');
            string currentPathLevel = layerPath[0];

            // Grab the current level of layer sets.
            LayerSets currentLayerSets = null;

            if (currentLayerSet == null)
            {
                currentLayerSets = doc.LayerSets;
            }
            else
            {
                currentLayerSets = currentLayerSet.LayerSets;
            }

            // Attempt to find the layer set for the current level of the path.
            LayerSet foundLayerSet = null;

            foreach (LayerSet layerSet in currentLayerSets)
            {
                if (layerSet.Name == currentPathLevel)
                {
                    foundLayerSet = layerSet;
                    break;
                }
            }

            // Layer set for the current path was found, but it isn't complete so we need to go to the next level.
            if (foundLayerSet != null && layerPath.Length > 1)
            {
                // Rebuild path, removing the current level.
                string newLayerPathstr = "";

                for (int idx = 1; idx < layerPath.Length; idx++)
                {
                    string pathItem = layerPath[idx];
                    newLayerPathstr += pathItem + "/";
                }

                string newLayerPath = newLayerPathstr.TrimEnd('/');

                LayerSet result = FindLayerGroupByPath(doc, newLayerPath, create, foundLayerSet);

                /*
                if (result != null)
                {
                    return result;
                }
                */

                return result;
            }

            // If the layer set set doesn't exist, create it.
            if (foundLayerSet == null && create == true)
            {
                foundLayerSet = currentLayerSets.Add();
                foundLayerSet.Name = currentPathLevel;
            }

            return foundLayerSet;
        }

        /// <summary>
        /// Supposedly blocks the Photoshop application from refreshing certain aspects of itself.
        /// </summary>
        /// <param name="app"></param>
        public void BlockRefresh()
        {
            if (this.m_app.IsConnected())
            {
                int idsetd = this.m_app.CharIDToTypeID("setd");
                ActionDescriptor desc1 = new ActionDescriptor();
                int idnull = this.m_app.CharIDToTypeID("null");
                ActionReference ref1 = new ActionReference();
                int idPrpr = this.m_app.CharIDToTypeID("Prpr");
                int idPbkO = this.m_app.CharIDToTypeID("PbkO");
                ref1.PutProperty(idPrpr, idPbkO);
                int idcapp = this.m_app.CharIDToTypeID("capp");
                int idOrdn = this.m_app.CharIDToTypeID("Ordn");
                int idTrgt = this.m_app.CharIDToTypeID("Trgt");
                ref1.PutEnumerated(idcapp, idOrdn, idTrgt);
                desc1.PutReference(idnull, ref1);
                int idT = this.m_app.CharIDToTypeID("T   ");
                ActionDescriptor desc2 = new ActionDescriptor();
                int idperformance = this.m_app.StringIDToTypeID("performance");
                int idaccelerated = this.m_app.StringIDToTypeID("accelerated");
                desc2.PutEnumerated(idperformance, idperformance, idaccelerated);
                int idPbkO2 = this.m_app.CharIDToTypeID("PbkO");
                desc1.PutObject(idT, idPbkO2, desc2);
                this.m_app.ExecuteAction(idsetd, desc1, DialogModes.DisplayNoDialogs);
            }
        }

        /// <summary>
        /// Creates a new solid color fill layer.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="doc"></param>
        /// <param name="r"></param>
        /// <param name="g"></param>
        /// <param name="b"></param>
        /// <param name="invertMask"></param>
        /// <returns></returns>
        public ArtLayer CreateFillLayer(Document doc, double r, double g, double b, bool invertMask = true)
        {
            if (this.m_app.IsConnected())
            {
                // Create solid color fill layer.
                int idMk = this.m_app.CharIDToTypeID("Mk  ");
                ActionDescriptor desc21 = new ActionDescriptor();
                int idNull = this.m_app.CharIDToTypeID("null");
                ActionReference ref12 = new ActionReference();
                int idContentLayer1 = this.m_app.StringIDToTypeID("contentLayer");
                ref12.PutClass(idContentLayer1);
                desc21.PutReference(idNull, ref12);
                int idUsng = this.m_app.CharIDToTypeID("Usng");
                ActionDescriptor desc22 = new ActionDescriptor();
                int idType = this.m_app.CharIDToTypeID("Type");
                ActionDescriptor desc23 = new ActionDescriptor();
                int idClr = this.m_app.CharIDToTypeID("Clr ");
                ActionDescriptor desc24 = new ActionDescriptor();
                int idRd = this.m_app.CharIDToTypeID("Rd  ");
                desc24.PutDouble(idRd, r);
                int idGrn = this.m_app.CharIDToTypeID("Grn ");
                desc24.PutDouble(idGrn, g);
                int idBl = this.m_app.CharIDToTypeID("Bl  ");
                desc24.PutDouble(idBl, b);
                int idRGBC = this.m_app.CharIDToTypeID("RGBC");
                desc23.PutObject(idClr, idRGBC, desc24);
                int idSolidColorLayer = this.m_app.StringIDToTypeID("solidColorLayer");
                desc22.PutObject(idType, idSolidColorLayer, desc23);
                int idContentLayer2 = this.m_app.StringIDToTypeID("contentLayer");
                desc21.PutObject(idUsng, idContentLayer2, desc22);
                this.m_app.ExecuteAction(idMk, desc21, DialogModes.DisplayNoDialogs);

                // Select mask
                int idSlct = this.m_app.CharIDToTypeID("slct");
                ActionDescriptor desc38 = new ActionDescriptor();
                int idNull1 = this.m_app.CharIDToTypeID("null");
                ActionReference ref20 = new ActionReference();
                int idChnl1 = this.m_app.CharIDToTypeID("Chnl");
                int idChnl2 = this.m_app.CharIDToTypeID("Chnl");
                int idMsk = this.m_app.CharIDToTypeID("Msk ");
                ref20.PutEnumerated(idChnl1, idChnl2, idMsk);
                desc38.PutReference(idNull1, ref20);
                int idMkVs = this.m_app.CharIDToTypeID("MkVs");
                desc38.PutBoolean(idMkVs, false);
                this.m_app.ExecuteAction(idSlct, desc38, DialogModes.DisplayNoDialogs);

                // Invert mask.  Fill layers default to white.
                if (invertMask)
                {
                    this.m_app.ComObject.ActiveDocument.ActiveLayer.Invert();
                }

                return (ArtLayer)this.m_app.ActiveDocument.ActiveLayer;
            }

            return null;
        }

        /// <summary>
        /// Set the color in the UI for the layer.  These options are available in Photoshop
        /// if you right-click on a layer.
        /// </summary>
        /// <param name="uiColor"></param>
        public void SetLayerUIColor(LayerUIColor uiColor)
        {
            int idsetd = this.m_app.CharIDToTypeID("setd");
            ActionDescriptor desc23 = new ActionDescriptor();
            int idnull = this.m_app.CharIDToTypeID("null");
            ActionReference ref14 = new ActionReference();
            int idLyr = this.m_app.CharIDToTypeID("Lyr ");
            int idOrdn = this.m_app.CharIDToTypeID("Ordn");
            int idTrgt = this.m_app.CharIDToTypeID("Trgt");
            ref14.PutEnumerated(idLyr, idOrdn, idTrgt);
            desc23.PutReference(idnull, ref14);

            int idT = this.m_app.CharIDToTypeID("T   ");
            ActionDescriptor desc24 = new ActionDescriptor();
            int idClr = this.m_app.CharIDToTypeID("Clr ");

            String strUiColor = "None";

            switch (uiColor)
            {
                case LayerUIColor.NoColor:
                    strUiColor = "None";
                    break;

                case LayerUIColor.Red:
                    strUiColor = "Rd  ";
                    break;

                case LayerUIColor.Orange:
                    strUiColor = "Orng";
                    break;

                case LayerUIColor.Yellow:
                    strUiColor = "Ylw ";
                    break;

                case LayerUIColor.Green:
                    strUiColor = "Grn ";
                    break;

                case LayerUIColor.Blue:
                    strUiColor = "Bl  ";
                    break;

                case LayerUIColor.Violet:
                    strUiColor = "Vlt ";
                    break;

                case LayerUIColor.Gray:
                    strUiColor = "Gry ";
                    break;

                default:
                    break;
            }

            int idUiColor = this.m_app.CharIDToTypeID(strUiColor);
            desc24.PutEnumerated(idClr, idClr, idUiColor);
            desc23.PutObject(idT, idLyr, desc24);
            this.m_app.ExecuteAction(idsetd, desc23, DialogModes.DisplayNoDialogs);
        }

        /// <summary>
        /// Deletes the selected layer mask.
        /// </summary>
        public void DeleteLayerMask()
        {
            if (this.m_app.IsConnected())
            {
                int idDlt = this.m_app.CharIDToTypeID("Dlt ");
                ActionDescriptor desc10 = new ActionDescriptor();
                int idnull = this.m_app.CharIDToTypeID("null");
                ActionReference ref5 = new ActionReference();
                int idChnl = this.m_app.CharIDToTypeID("Chnl");
                int idOrdn = this.m_app.CharIDToTypeID("Ordn");
                int idTrgt = this.m_app.CharIDToTypeID("Trgt");
                ref5.PutEnumerated(idChnl, idOrdn, idTrgt);
                desc10.PutReference(idnull, ref5);
                this.m_app.ExecuteAction(idDlt, desc10, DialogModes.DisplayNoDialogs);
            }
        }

        /// <summary>
        /// Creates a layer mask for the active layer.
        /// </summary>
        /// <param name="maskType"></param>
        public void CreateLayerMask(LayerMaskType maskType)
        {
            if (this.m_app.IsConnected())
            {
                int idMk = this.m_app.CharIDToTypeID("Mk  ");
                ActionDescriptor desc14 = new ActionDescriptor();
                int idNw = this.m_app.CharIDToTypeID("Nw  ");
                int idChnl = this.m_app.CharIDToTypeID("Chnl");
                desc14.PutClass(idNw, idChnl);
                int idAt = this.m_app.CharIDToTypeID("At  ");
                ActionReference ref10 = new ActionReference();
                int idChnl1 = this.m_app.CharIDToTypeID("Chnl");
                int idChnl2 = this.m_app.CharIDToTypeID("Chnl");
                int idMsk = this.m_app.CharIDToTypeID("Msk ");
                ref10.PutEnumerated(idChnl1, idChnl2, idMsk);
                desc14.PutReference(idAt, ref10);
                int idUsing = this.m_app.CharIDToTypeID("Usng");
                int idUsrM = this.m_app.CharIDToTypeID("UsrM");

                // Defaults to reveal all.
                String maskTypeStr = "HdAl";

                if (maskType == LayerMaskType.RevealAll)
                {
                    maskTypeStr = "RvlA";
                }
                else if (maskType == LayerMaskType.RevealSelection)
                {
                    maskTypeStr = "RvlS";
                }
                else if (maskType == LayerMaskType.HideAll)
                {
                    maskTypeStr = "HdAl";
                }
                else if (maskType == LayerMaskType.HideSelection)
                {
                    maskTypeStr = "HdSl";
                }

                int idHdAl = this.m_app.CharIDToTypeID(maskTypeStr);
                desc14.PutEnumerated(idUsing, idUsrM, idHdAl);
                this.m_app.ExecuteAction(idMk, desc14, DialogModes.DisplayNoDialogs);
            }
        }

        /// <summary>
        /// Selects the layer mask for the active layer.
        /// </summary>
        /// <param name="app"></param>
        public void SelectLayerMask()
        {
            if (this.m_app.IsConnected())
            {
                int idslct = this.m_app.CharIDToTypeID("slct");
                ActionDescriptor desc309 = new ActionDescriptor();
                int idnull = this.m_app.CharIDToTypeID("null");
                ActionReference ref53 = new ActionReference();
                int idChnl1 = this.m_app.CharIDToTypeID("Chnl");
                int idChnl2 = this.m_app.CharIDToTypeID("Chnl");
                int idMsk = this.m_app.CharIDToTypeID("Msk ");
                ref53.PutEnumerated(idChnl1, idChnl2, idMsk);
                desc309.PutReference(idnull, ref53);
                int idMkvs = this.m_app.CharIDToTypeID("Mkvs");
                desc309.PutBoolean(idMkvs, false);
                this.m_app.ExecuteAction(idslct, desc309, DialogModes.DisplayNoDialogs);
            }
        }

        /// <summary>
        /// Makes the active layer a clipping mask to the layer below.
        /// </summary>
        public void CreateClippingMask()
        {
            if (this.m_app.IsConnected())
            {
                int idGrpL = this.m_app.CharIDToTypeID("GrpL");
                ActionDescriptor desc8 = new ActionDescriptor();
                int idnull = this.m_app.CharIDToTypeID("null");
                ActionReference ref3 = new ActionReference();
                int idLyr = this.m_app.CharIDToTypeID("Lyr ");
                int idOrdn = this.m_app.CharIDToTypeID("Ordn");
                int idTrgt = this.m_app.CharIDToTypeID("Trgt");
                ref3.PutEnumerated(idLyr, idOrdn, idTrgt);
                desc8.PutReference(idnull, ref3);
                this.m_app.ExecuteAction(idGrpL, desc8, DialogModes.DisplayNoDialogs);
            }
        }

        /// <summary>
        /// Create a selection from the selected layer mask.
        /// </summary>
        public void CreateSelectionFromLayerMask()
        {
            if (this.m_app.IsConnected())
            {
                int idsetd = this.m_app.CharIDToTypeID("setd");
                ActionDescriptor desc8 = new ActionDescriptor();
                int idnull = this.m_app.CharIDToTypeID("null");
                ActionReference ref5 = new ActionReference();
                int idChnl = this.m_app.CharIDToTypeID("Chnl");
                int idfsel = this.m_app.CharIDToTypeID("fsel");
                ref5.PutProperty(idChnl, idfsel);
                desc8.PutReference(idnull, ref5);

                int idT = this.m_app.CharIDToTypeID("T   ");
                ActionReference ref6 = new ActionReference();
                int idChnl2 = this.m_app.CharIDToTypeID("Chnl");
                int idOrdn = this.m_app.CharIDToTypeID("Ordn");
                int idTrgt = this.m_app.CharIDToTypeID("Trgt");
                ref6.PutEnumerated(idChnl2, idOrdn, idTrgt);
                desc8.PutReference(idT, ref6);

                this.m_app.ExecuteAction(idsetd, desc8, DialogModes.DisplayNoDialogs);
            }
        }

        /// <summary>
        /// Get the color of the solid color fill layer.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="layer"></param>
        /// <returns></returns>
        public SolidColor GetFillLayerColor(ArtLayer layer)
        {
            if (this.m_app.IsConnected())
            {
                this.m_app.ActiveDocument.ActiveLayer = layer;

                ActionReference refs = new ActionReference();
                refs.PutEnumerated(this.m_app.CharIDToTypeID("Lyr "), this.m_app.CharIDToTypeID("Ordn"), this.m_app.CharIDToTypeID("Trgt"));
                var layerDesc = this.m_app.ExecuteActionGet(refs);
                var adjList = layerDesc.GetList(this.m_app.StringIDToTypeID("adjustment"));
                var colors = adjList.GetObjectValue(0).GetObjectValue(this.m_app.StringIDToTypeID("color"));
                var r = colors.GetUnitDoubleValue(colors.GetKey(0));
                var g = colors.GetUnitDoubleValue(colors.GetKey(1));
                var b = colors.GetUnitDoubleValue(colors.GetKey(2));

                SolidColor layerColor = new SolidColor();
                layerColor.RGB.Red = r;
                layerColor.RGB.Green = g;
                layerColor.RGB.Blue = b;

                return layerColor;
            }

            return null;
        }

        /// <summary>
        /// Set the color of the solid color fill layer.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="layer"></param>
        /// <param name="r"></param>
        /// <param name="g"></param>
        /// <param name="b"></param>
        public void SetFillLayerColor(ArtLayer layer, double r, double g, double b)
        {
            if (this.m_app.IsConnected())
            {
                this.m_app.ActiveDocument.ActiveLayer = layer;

                int idsetd = this.m_app.CharIDToTypeID("setd");
                ActionDescriptor desc120 = new ActionDescriptor();
                int idnull = this.m_app.CharIDToTypeID("null");
                ActionReference ref82 = new ActionReference();
                int idcontentlayer = this.m_app.StringIDToTypeID("contentLayer");
                int idOrdn = this.m_app.CharIDToTypeID("Ordn");
                int idTrgt = this.m_app.CharIDToTypeID("Trgt");
                ref82.PutEnumerated(idcontentlayer, idOrdn, idTrgt);
                desc120.PutReference(idnull, ref82);
                int idT = this.m_app.CharIDToTypeID("T   ");
                ActionDescriptor desc121 = new ActionDescriptor();
                int idClr = this.m_app.CharIDToTypeID("Clr ");
                ActionDescriptor desc122 = new ActionDescriptor();
                int idRd = this.m_app.CharIDToTypeID("Rd  ");
                desc122.PutDouble(idRd, r);
                int idGrn = this.m_app.CharIDToTypeID("Grn ");
                desc122.PutDouble(idGrn, g);
                int idBl = this.m_app.CharIDToTypeID("Bl  ");
                desc122.PutDouble(idBl, b);
                int idRGBC = this.m_app.CharIDToTypeID("RGBC");
                desc121.PutObject(idClr, idRGBC, desc122);
                int idsolidColorLayer = this.m_app.StringIDToTypeID("solidColorLayer");
                desc120.PutObject(idT, idsolidColorLayer, desc121);
                this.m_app.ExecuteAction(idsetd, desc120, DialogModes.DisplayNoDialogs);
            }
        }

        /// <summary>
        /// Set the color of the solid color fill layer.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="layer"></param>
        /// <param name="color"></param>
        public void SetFillLayerColor(ArtLayer layer, Color color)
        {
            SetFillLayerColor(layer, color.R, color.G, color.B);
        }

        /// <summary>
        /// Sets the supplied channels of the document to be active.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="doc"></param>
        /// <param name="red"></param>
        /// <param name="green"></param>
        /// <param name="blue"></param>
        /// <param name="alpha"></param>
        public void SetActiveChannels(Document doc, bool red = true, bool green = true, bool blue = true, bool alpha = true)
        {
            if (this.m_app.IsConnected())
            {
                List<object> channels = new List<object>();

                foreach (Channel channel in doc.Channels)
                {
                    if (channel.Name.ToLower().Contains("red") && red == true)
                    {
                        channels.Add((object)channel.ComObject);
                    }

                    if (channel.Name.ToLower().Contains("green") && green == true)
                    {
                        channels.Add((object)channel.ComObject);
                    }

                    if (channel.Name.ToLower().Contains("blue") && blue == true)
                    {
                        channels.Add((object)channel.ComObject);
                    }

                    if (channel.Name.ToLower().Contains("alpha") && alpha == true)
                    {
                        channels.Add((object)channel.ComObject);
                    }
                }

                doc.ActiveChannels = channels.ToArray();
            }
        }

        /// <summary>
        /// Retrieves the active PathItem in the document.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="doc"></param>
        /// <returns></returns>
        public PathItem GetActivePath(Document doc)
        {
            if (this.m_app.IsConnected())
            {
                PathItem pathItem = null;

                try
                {
                    ActionReference actionRef = new ActionReference();
                    actionRef.PutEnumerated(this.m_app.CharIDToTypeID("Dcmn"), this.m_app.CharIDToTypeID("Ordn"), this.m_app.CharIDToTypeID("Trgt"));
                    ActionDescriptor desc = this.m_app.ExecuteActionGet(actionRef);
                    int idx = desc.GetInteger(this.m_app.CharIDToTypeID("TrgP"));

                    if (idx != -1)
                    {
                        pathItem = doc.PathItems[idx];
                    }
                }
                catch { }

                return pathItem;
            }

            return null;
        }

        /// <summary>
        /// Selects pixels by color.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="doc"></param>
        /// <param name="color"></param>
        /// <param name="fuzziness"></param>
        public void SelectByColor(Document doc, SolidColor color, int fuzziness = 0)
        {
            if (this.m_app.IsConnected())
            {
                SolidColor foregroundClr = this.m_app.ForegroundColor;
                SolidColor white = new SolidColor();
                white.RGB.Red = 255;
                white.RGB.Green = 255;
                white.RGB.Blue = 255;

                this.m_app.ForegroundColor = white;

                ActionDescriptor desc1 = new ActionDescriptor();
                desc1.PutInteger(this.m_app.CharIDToTypeID("Fzns"), fuzziness);

                ActionDescriptor desc2 = new ActionDescriptor();
                desc2.PutDouble(this.m_app.CharIDToTypeID("Rd  "), color.RGB.Red);
                desc2.PutDouble(this.m_app.CharIDToTypeID("Grn "), color.RGB.Green);
                desc2.PutDouble(this.m_app.CharIDToTypeID("Bl  "), color.RGB.Blue);

                desc1.PutObject(this.m_app.CharIDToTypeID("Mnm "), this.m_app.CharIDToTypeID("RGBC"), desc2);
                desc1.PutObject(this.m_app.CharIDToTypeID("Mxm "), this.m_app.CharIDToTypeID("RGBC"), desc2);
                desc1.PutInteger(this.m_app.StringIDToTypeID("colorModel"), 0);

                this.m_app.ExecuteAction(this.m_app.CharIDToTypeID("ClrR"), desc1, DialogModes.DisplayNoDialogs);

                this.m_app.ForegroundColor = foregroundClr;
            }
        }

        /// <summary>
        /// Set the supplied ArtLayer path visibility.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="doc"></param>
        /// <param name="layerGroupPath"></param>
        /// <param name="layerName"></param>
        /// <param name="visibility"></param>
        /// <param name="currentLayerSet"></param>
        public void SetLayerVisibilityByPath(Document doc, string layerGroupPath, string layerName, bool visibility, LayerSet currentLayerSet = null)
        {
            string[] layerPath = layerGroupPath.Split('/');

            LayerSets currentLayerSets = null;

            if (currentLayerSet == null)
            {
                currentLayerSets = doc.LayerSets;
            }
            else
            {
                currentLayerSets = currentLayerSet.LayerSets;
            }

            // Turn off all layer groups at the current level.
            LayerSet foundLayerSet = null;

            foreach (LayerSet layerSet in currentLayerSets)
            {
                layerSet.Visible = false;

                if (layerSet.Name == layerPath[0])
                {
                    layerSet.Visible = visibility;
                    foundLayerSet = layerSet;

                    foreach (ArtLayer layer in layerSet.ArtLayers)
                    {
                        if (layer.Name == layerName)
                        {
                            layer.Visible = visibility;
                        }
                    }
                }
            }

            if (foundLayerSet != null && layerPath.Length > 1)
            {
                // Rebuild path, removing the first item in the current array.
                string newLayerPathstr = "";

                for (int idx = 1; idx < layerPath.Length; idx++)
                {
                    string pathItem = layerPath[idx];
                    newLayerPathstr += pathItem + "/";
                }

                string newLayerPath = newLayerPathstr.TrimEnd('/');

                SetLayerGroupVisibilityByPath(doc, newLayerPath, visibility, foundLayerSet);
            }
        }

        /// <summary>
        /// Set the supplied LayerSet path visiblity.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="doc"></param>
        /// <param name="layerGroupPath"></param>
        /// <param name="visibility"></param>
        /// <param name="currentLayerSet"></param>
        public void SetLayerGroupVisibilityByPath(Document doc, string layerGroupPath, bool visibility, LayerSet currentLayerSet = null)
        {
            string[] layerPath = layerGroupPath.Split('/');

            LayerSets currentLayerSets = null;

            if (currentLayerSet == null)
            {
                currentLayerSets = doc.LayerSets;
            }
            else
            {
                currentLayerSets = currentLayerSet.LayerSets;
            }

            // Turn off all layer groups at the current level.
            LayerSet foundLayerSet = null;

            foreach (LayerSet layerSet in currentLayerSets)
            {
                layerSet.Visible = false;

                if (layerSet.Name == layerPath[0])
                {
                    layerSet.Visible = visibility;
                    foundLayerSet = layerSet;
                }
            }

            if (foundLayerSet != null && layerPath.Length > 1)
            {
                // Rebuild path, removing the first item in the current array.
                string newLayerPathstr = "";

                for (int idx = 1; idx < layerPath.Length; idx++)
                {
                    string pathItem = layerPath[idx];
                    newLayerPathstr += pathItem + "/";
                }

                string newLayerPath = newLayerPathstr.TrimEnd('/');

                SetLayerGroupVisibilityByPath(doc, newLayerPath, visibility, foundLayerSet);
            }
        }

        #endregion // Methods
    }
}
