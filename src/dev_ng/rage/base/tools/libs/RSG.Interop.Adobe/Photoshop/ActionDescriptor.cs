﻿//---------------------------------------------------------------------------------------------
// <copyright file="ActionDescriptor.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Interop.Adobe.Photoshop
{
    /// <summary>
    /// A record of key-value pairs for actions.
    /// </summary>
    public sealed class ActionDescriptor : ComWrapper
    {
        #region Member Data
        #endregion // Member Data

        #region Constructor(s)

        public ActionDescriptor() 
            : base()
        {
            Type actionDescriptorType = Type.GetTypeFromProgID("Photoshop.ActionDescriptor");
            this.m_comObject = (dynamic)Activator.CreateInstance(actionDescriptorType);
        }

        public ActionDescriptor(dynamic actionDescriptorComObject)
            : base((object)actionDescriptorComObject)
        {
        }

        #endregion // Constructor(s)

        #region Properties
        #endregion // Properties

        #region Methods

        /// <summary>
        /// Sets the value for a key whose type is an object reference.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void PutReference(int key, ActionReference value)
        {
            this.m_comObject.PutReference(key, value.ComObject);
        }

        /// <summary>
        /// Sets the value for a key whose type is a path.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value">Represents a file path.</param>
        public void PutPath(int key, String value)
        {
            this.m_comObject.PutPath(key, value);
        }

        /// <summary>
        /// Sets the value for a key whose type is an Action Descriptor.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="classID"></param>
        /// <param name="value"></param>
        public void PutObject(int key, int classID, ActionDescriptor value)
        {
            this.m_comObject.PutObject(key, classID, value.ComObject);
        }

        /// <summary>
        /// Sets the enumeration type and value for a key.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="enumType"></param>
        /// <param name="value"></param>
        public void PutEnumerated(int key, int enumType, int value)
        {
            this.m_comObject.PutEnumerated(key, enumType, value);
        }

        /// <summary>
        /// Sets the value for a key whose type is double.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void PutDouble(int key, double value)
        {
            this.m_comObject.PutDouble(key, value);
        }

        /// <summary>
        /// Sets the value for a key whose type is a unit formatted as a double.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="unitID"></param>
        /// <param name="value"></param>
        public void PutUnitDouble(int key, int unitID, double value)
        {
            this.m_comObject.PutUnitDouble(key, unitID, value);
        }

        /// <summary>
        /// Sets the value for a key whose type is Boolean.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void PutBoolean(int key, bool value)
        {
            this.m_comObject.PutBoolean(key, value);
        }

        /// <summary>
        /// Sets the value for a key whose type is class.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void PutClass(int key, int value)
        {
            this.m_comObject.PutClass(key, value);
        }

        /// <summary>
        /// Sets the value for a key whose type is integer.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void PutInteger(int key, int value)
        {
            this.m_comObject.PutInteger(key, value);
        }

        /// <summary>
        /// Gets the value of a key of type integer.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public int GetInteger(int key)
        {
            return this.m_comObject.GetInteger(key);
        }

        /// <summary>
        /// Gets the value of a key of type list.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public ActionList GetList(int key)
        {
            return new ActionList(this.m_comObject.GetList(key));
        }

        /// <summary>
        /// Gets the value of a key of type object.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public ActionDescriptor GetObjectValue(int key)
        {
            return new ActionDescriptor(this.m_comObject.GetObjectValue(key));
        }

        /// <summary>
        /// Gets the value of a key of type UnitDouble.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public double GetUnitDoubleValue(int key)
        {
            return this.m_comObject.GetUnitDoubleValue(key);
        }

        /// <summary>
        /// Gets the ID of the Nth key.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public int GetKey(int index)
        {
            return this.m_comObject.GetKey(index);
        }


        #endregion // Methods
    }
}
