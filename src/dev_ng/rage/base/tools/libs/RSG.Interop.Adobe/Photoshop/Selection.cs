﻿//---------------------------------------------------------------------------------------------
// <copyright file="Selection.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Interop.Adobe.Photoshop
{
    public sealed class Selection : ComWrapper
    {
        #region Member Data
        #endregion // Member Data

        #region Constructor(s)

        public Selection(dynamic selectionComObject)
            : base((object)selectionComObject)
        {
        }

        #endregion // Constructor(s)

        #region Properties

        /// <summary>
        /// The bounding rectangle of the entire selection.
        /// </summary>
        public Array Bounds
        {
            get { return this.m_comObject.Bounds; }
        }

        #endregion // Properties

        #region Methods

        /// <summary>
        /// Selects the entire layer.
        /// </summary>
        public void SelectAll()
        {
            this.m_comObject.SelectAll();
        }

        /// <summary>
        /// Copies the selection to the clipboard.  When the optional argument is used and set to true,
        /// a merged copy is performed (all visible layers in the selection are copied).
        /// </summary>
        /// <param name="merge"></param>
        public void Copy(bool merge = false)
        {
            this.m_comObject.Copy(merge);
        }

        /// <summary>
        /// Deselects the current selection.
        /// </summary>
        public void Deselect()
        {
            this.m_comObject.Deselect();
        }

        /// <summary>
        /// Clears the selection and does not copy it to the clipboard.
        /// </summary>
        public void Clear()
        {
            this.m_comObject.Clear();
        }

        #endregion // Methods
    }
}
