﻿//---------------------------------------------------------------------------------------------
// <copyright file="HSBColor.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Interop.Adobe.Photoshop
{
    /// <summary>
    /// Wrapper class for Photoshop HSBColor object.
    /// 
    /// For scripting reference, see:
    /// 
    ///     CC : http://wwwimages.adobe.com/content/dam/Adobe/en/devnet/photoshop/pdfs/photoshop-cc-vbs-ref.pdf
    ///     
    /// </summary>
    public sealed class HSBColor : ComWrapper
    {
        #region Member Data
        #endregion // Member Data

        #region Constructor(s)

        public HSBColor()
            : base()
        {
            Type optionsType = Type.GetTypeFromProgID("Photoshop.HSBColor");
            this.m_comObject = (dynamic)Activator.CreateInstance(optionsType);
        }

        public HSBColor(dynamic obj)
            : base((object)obj)
        {
        }

        public HSBColor(double brightness, double hue, double saturation)
            : base()
        {
            Type optionsType = Type.GetTypeFromProgID("Photoshop.HSBColor");
            this.m_comObject = (dynamic)Activator.CreateInstance(optionsType);

            this.m_comObject.Brightness = brightness;
            this.m_comObject.Hue = hue;
            this.m_comObject.Saturation = saturation;
        }

        #endregion // Constructor(s)

        #region Properties

        /// <summary>
        /// The brightness value (0.0 - 100.0).
        /// </summary>
        public double Brightness
        {
            get { return this.m_comObject.Brightness; }
            set { this.m_comObject.Brightness = value; }
        }

        /// <summary>
        /// The hue value (0.0 - 100.0).
        /// </summary>
        public double Hue
        {
            get { return this.m_comObject.Hue; }
            set { this.m_comObject.Hue = value; }
        }

        /// <summary>
        /// The saturation value (0.0 - 100.0).
        /// </summary>
        public double Saturation
        {
            get { return this.m_comObject.Saturation; }
            set { this.m_comObject.Saturation = value; }
        }

        #endregion // Properties

        #region Methods
        #endregion // Methods
    }
}
