﻿//---------------------------------------------------------------------------------------------
// <copyright file="PathItem.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Interop.Adobe.Photoshop
{
    public sealed class PathItem : ComWrapper
    {
        #region Member Data
        #endregion // Member Data

        #region Constructor(s)

        public PathItem(dynamic pathItemComObject)
            : base((object)pathItemComObject)
        {
        }

        #endregion // Constructor(s)

        #region Properties
        #endregion // Properties

        #region Methods

        /// <summary>
        /// Makes a Selection object, whose border is the path from this PathItem object.
        /// </summary>
        /// <param name="feather">0.0 - 250.0 in pixels.</param>
        /// <param name="antiAlias"></param>
        /// <param name="operation"></param>
        public Selection MakeSelection(double feather, bool antiAlias = true, SelectionType operation = SelectionType.ReplaceSelection)
        {
            return new Selection(this.m_comObject.MakeSelection(feather, antiAlias, operation));
        }

        #endregion // Methods
    }

    public sealed class PathItems : List<PathItem>
    {
        #region Member Data

        private dynamic m_comObject;

        #endregion // Member Data

        #region Constructor(s)

        public PathItems(dynamic pathItemsComObject)
        {
            this.m_comObject = pathItemsComObject;

            // Fill up our list with PathItem objects.
            foreach (dynamic pathItemComObject in pathItemsComObject)
            {
                this.Add(new PathItem(pathItemComObject));
            }
        }

        #endregion // Constructor(s)

        #region Properties

        /// <summary>
        /// Access to the COM object for this wrapper.
        /// </summary>
        public dynamic ComObject
        {
            get { return this.m_comObject; }
        }

        #endregion // Properties

        #region Methods
        #endregion // Methods
    }
}
