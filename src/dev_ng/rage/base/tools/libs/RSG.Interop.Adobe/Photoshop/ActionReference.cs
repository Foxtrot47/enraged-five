﻿//---------------------------------------------------------------------------------------------
// <copyright file="ActionReference.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Interop.Adobe.Photoshop
{
    /// <summary>
    /// A record of key-value pairs for actions.
    /// </summary>
    public sealed class ActionReference : ComWrapper
    {
        #region Member Data
        #endregion // Member Data

        #region Constructor(s)

        public ActionReference()
            : base()
        {
            Type actionDescriptorType = Type.GetTypeFromProgID("Photoshop.ActionReference");
            this.m_comObject = (dynamic)Activator.CreateInstance(actionDescriptorType);
        }

        #endregion // Constructor(s)

        #region Properties
        #endregion // Properties

        #region Methods

        /// <summary>
        /// Puts a new property and value into the reference.
        /// </summary>
        /// <param name="desiredClass"></param>
        /// <param name="value"></param>
        public void PutProperty(int desiredClass, int value)
        {
            this.m_comObject.PutProperty(desiredClass, value);
        }

        /// <summary>
        /// Puts a new name and value into the reference.
        /// </summary>
        /// <param name="desiredClass"></param>
        /// <param name="value"></param>
        public void PutName(int desiredClass, String value)
        {
            this.m_comObject.PutProperty(desiredClass, value);
        }

        /// <summary>
        /// Puts an enumeration type and ID into a reference along with the desired class for the reference.
        /// </summary>
        /// <param name="desiredClass"></param>
        /// <param name="enumType"></param>
        /// <param name="value"></param>
        public void PutEnumerated(int desiredClass, int enumType, int value)
        {
            this.m_comObject.PutEnumerated(desiredClass, enumType, value);
        }

        /// <summary>
        /// Puts a new class form and class type into the reference.
        /// </summary>
        /// <param name="desiredClass"></param>
        public void PutClass(int desiredClass)
        {
            this.m_comObject.PutClass(desiredClass);
        }

        #endregion // Methods
    }
}
