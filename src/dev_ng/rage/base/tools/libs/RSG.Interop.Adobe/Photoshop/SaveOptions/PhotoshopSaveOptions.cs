﻿//---------------------------------------------------------------------------------------------
// <copyright file="PhotoshopSaveOptions.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Interop.Adobe.Photoshop
{
    /// <summary>
    /// Wrapper class for PhotoshopSaveOptions.
    /// 
    /// For scripting reference, see:
    /// 
    ///     CC : http://wwwimages.adobe.com/content/dam/Adobe/en/devnet/photoshop/pdfs/photoshop-cc-vbs-ref.pdf
    ///     
    /// </summary>
    public sealed class PhotoshopSaveOptions : ComWrapper, ISaveOptions
    {
        #region Member Data
        #endregion // Member Data

        #region Constructor(s)

        public PhotoshopSaveOptions()
            : base()
        {
            Type optionsType = Type.GetTypeFromProgID("Photoshop.PhotoshopSaveOptions");
            this.m_comObject = (dynamic)Activator.CreateInstance(optionsType);
        }

        #endregion // Constructor(s)

        #region Properties

        /// <summary>
        /// Indicates whether to save the alpha channels.
        /// </summary>
        public bool AlphaChannels
        {
            get { return this.m_comObject.AlphaChannels; }
            set { this.m_comObject.AlphaChannels = value; }
        }

        /// <summary>
        /// Indicates whether to save the annotations.
        /// </summary>
        public bool Annotations
        {
            get { return this.m_comObject.Annotations; }
            set { this.m_comObject.Annotations = value; }
        }

        /// <summary>
        /// Indicates whether to embed the color profile in the document.
        /// </summary>
        public bool EmbedColorProfile
        {
            get { return this.m_comObject.EmbedColorProfile; }
            set { this.m_comObject.EmbedColorProfile = value; }
        }

        /// <summary>
        /// Indicates whether to save the layers.
        /// </summary>
        public bool Layers
        {
            get { return this.m_comObject.Layers; }
            set { this.m_comObject.Layers = value; }
        }

        /// <summary>
        /// Indicates whether to save the spot colors.
        /// </summary>
        public bool SpotColors
        {
            get { return this.m_comObject.SpotColors; }
            set { this.m_comObject.SpotColors = value; }
        }

        #endregion // Properties
    }
}
