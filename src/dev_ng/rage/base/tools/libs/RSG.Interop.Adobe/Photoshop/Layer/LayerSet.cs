﻿//---------------------------------------------------------------------------------------------
// <copyright file="LayerSet.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Interop.Adobe.Photoshop
{
    /// <summary>
    /// Wrapper class for Photoshop LayerSet object.
    /// 
    /// For scripting reference, see:
    /// 
    ///     CC : http://wwwimages.adobe.com/content/dam/Adobe/en/devnet/photoshop/pdfs/photoshop-cc-vbs-ref.pdf
    ///     
    /// </summary>
    public sealed class LayerSet : ComWrapper, ILayer
    {
        #region Member Data
        #endregion // Member Data

        #region Constructor(s)

        public LayerSet(dynamic layerSetComObject)
            : base((object)layerSetComObject)
        {
        }

        #endregion // Constructor(s)

        #region Properties

        /// <summary>
        /// The name of this LayerSet.
        /// </summary>
        public String Name
        {
            get { return this.m_comObject.Name; }
            set { this.m_comObject.Name = value; }
        }

        /// <summary>
        /// The visibility of this LayerSet.
        /// </summary>
        public bool Visible
        {
            get { return this.m_comObject.Visible; }
            set { this.m_comObject.Visible = value; }
        }

        /// <summary>
        /// The LayerSets collection.
        /// </summary>
        public LayerSets LayerSets
        {
            get { return new LayerSets(this.m_comObject.LayerSets); }
        }

        /// <summary>
        /// The ArtLayers collection.
        /// </summary>
        public ArtLayers ArtLayers
        {
            get { return new ArtLayers(this.m_comObject.ArtLayers); }
        }

        /// <summary>
        /// The layer's blending mode.
        /// </summary>
        public BlendMode BlendMode
        {
            get { return (BlendMode)this.m_comObject.BlendMode; }
            set { this.m_comObject.BlendMode = value; }
        }

        /// <summary>
        /// The master opacity of the layer (0.0 - 100.0).
        /// </summary>
        public double Opacity
        {
            get { return this.m_comObject.Opacity; }
            set { this.m_comObject.Opacity = value; }
        }

        #endregion // Properties

        #region Methods

        /// <summary>
        /// Deletes the LayerSet object.
        /// </summary>
        public void Delete()
        {
            this.m_comObject.Delete();
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        public ArtLayer Add()
        {
            return new ArtLayer(this.m_comObject.Add());
        }

        /// <summary>
        /// Merges the LayerSet.
        /// </summary>
        /// <returns></returns>
        public ArtLayer Merge()
        {
            return new ArtLayer(this.m_comObject.Merge());
        }

        /// <summary>
        /// Creates a duplicate of the LayerSet object.
        /// </summary>
        /// <param name="relativeObject"></param>
        /// <param name="insertLocation"></param>
        /// <returns></returns>
        public LayerSet Duplicate(ILayer relativeObject, ElementPlacement insertLocation)
        {
            return new LayerSet(this.m_comObject.Duplicate(relativeObject.ComObject, insertLocation));
        }

        /// <summary>
        /// Moves the LayerSet object.
        /// </summary>
        /// <param name="relativeObject"></param>
        /// <param name="insertLocation">Only PlaceInside and PlaceBefore are valid.</param>
        public void Move(ILayer relativeObject, ElementPlacement insertLocation)
        {
            if (insertLocation == ElementPlacement.PlaceInside || insertLocation == ElementPlacement.PlaceBefore)
            {
                this.m_comObject.Move(relativeObject.ComObject, insertLocation);
            }
            else
            {
                throw new AdobeInteropException("Only ElementPlacement.PlaceInside and ElementPlacement.PlaceBefore are valid insert location types!");
            }
        }

        #endregion // Methods
    }
}
