﻿//---------------------------------------------------------------------------------------------
// <copyright file="Channel.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Interop.Adobe.Photoshop
{
    /// <summary>
    /// Wrapper class for a Photoshop Channel object.
    /// 
    /// For scripting reference, see:
    /// 
    ///     CC : http://wwwimages.adobe.com/content/dam/Adobe/en/devnet/photoshop/pdfs/photoshop-cc-vbs-ref.pdf
    ///     
    /// </summary>
    public sealed class Channel : ComWrapper
    {
        #region Member Data
        #endregion // Member Data

        #region Constructor(s)

        public Channel(dynamic channelComObject)
            : base((object)channelComObject)
        {
        }

        #endregion // Constructors

        #region Properties

        /// <summary>
        /// Then channel's name.
        /// </summary>
        public String Name
        {
            get { return this.m_comObject.Name; }
            set { this.m_comObject.Name = value; }
        }

        /// <summary>
        /// Indicates whether the channel is visible.
        /// </summary>
        public bool Visible
        {
            get { return this.m_comObject.Visible; }
            set { this.m_comObject.Visible = value; }
        }

        #endregion // Properties

        #region Methods
        #endregion // Methods
    }

    public sealed class Channels : List<Channel>
    {
        #region Member Data

        private dynamic m_comObject;

        #endregion // Member Data

        #region Constructor(s)

        public Channels(dynamic channelsComObject)
            : base()
        {
            this.m_comObject = channelsComObject;

            foreach (dynamic channelComObject in channelsComObject)
            {
                this.Add(new Channel(channelComObject));
            }
        }

        #endregion // Constructor(s)

        #region Properties

        /// <summary>
        /// Access to the COM object for this wrapper.
        /// </summary>
        public dynamic ComObject
        {
            get { return this.m_comObject; }
        }

        #endregion // Properties

        #region Methods

        /// <summary>
        /// Creates a new Channel object.
        /// </summary>
        /// <returns></returns>
        public Channel Add()
        {
            Channel channel = new Channel(this.m_comObject.Add());
            this.Add(channel);
            return channel;
        }

        #endregion // Methods
    }
}
