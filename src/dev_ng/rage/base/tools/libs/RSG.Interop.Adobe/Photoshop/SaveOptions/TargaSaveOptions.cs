﻿//---------------------------------------------------------------------------------------------
// <copyright file="TargaSaveOptions.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Interop.Adobe.Photoshop
{
    /// <summary>
    /// Wrapper class for TargaSaveOptions.
    /// 
    /// For scripting reference, see:
    /// 
    ///     CC : http://wwwimages.adobe.com/content/dam/Adobe/en/devnet/photoshop/pdfs/photoshop-cc-vbs-ref.pdf
    ///     
    /// </summary>
    public sealed class TargaSaveOptions : ComWrapper, ISaveOptions
    {
        #region Member Data
        #endregion // Member Data

        #region Constructor(s)

        public TargaSaveOptions()
            : base()
        {
            Type optionsType = Type.GetTypeFromProgID("Photoshop.TargaSaveOptions");
            this.m_comObject = (dynamic)Activator.CreateInstance(optionsType);
        }

        #endregion // Constructor(s)

        #region Properties

        /// <summary>
        /// Indicates whether to save the alpha channels.
        /// </summary>
        public bool AlphaChannels
        {
            get { return this.m_comObject.AlphaChannels; }
            set { this.m_comObject.AlphaChannels = value; }
        }

        /// <summary>
        /// The number of bits per-pixel.  Default: 24.
        /// </summary>
        public TargaBitsPerPixel Resolution
        {
            get { return this.m_comObject.Resolution; }
            set { this.m_comObject.Resolution = value; }
        }

        /// <summary>
        /// Indicates whether RLE compression should be used.  Default: true.
        /// </summary>
        public bool RLECompression
        {
            get { return this.m_comObject.RLECompression; }
            set { this.m_comObject.RLECompression = value; }
        }

        #endregion // Properties
    }
}
