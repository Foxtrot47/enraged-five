﻿//---------------------------------------------------------------------------------------------
// <copyright file="ILayer.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Interop.Adobe.Photoshop
{
    /// <summary>
    /// Base interface for an ArtLayer or LayerSet object.
    /// </summary>
    public interface ILayer
    {
        String Name { get; set; }
        dynamic ComObject { get; }
    }
}
