﻿//---------------------------------------------------------------------------------------------
// <copyright file="Document.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Interop.Adobe.Photoshop
{
    /// <summary>
    /// Wrapper class for a Photoshop Document.
    /// 
    /// For scripting reference, see:
    /// 
    ///     CC : http://wwwimages.adobe.com/content/dam/Adobe/en/devnet/photoshop/pdfs/photoshop-cc-vbs-ref.pdf
    ///     
    /// </summary>
    public sealed class Document : ComWrapper
    {
        #region Member Data
        #endregion // Member Data

        #region Constructor(s)

        public Document(dynamic doc)
            : base((object)doc)
        {
        }

        #endregion // Constructor(s)

        #region Properties

        /// <summary>
        /// The name of the document.
        /// </summary>
        public String Name
        {
            get { return this.m_comObject.Name; }
        }

        /// <summary>
        /// The name of the document, including the full path on disk.
        /// </summary>
        public String FullName
        {
            get 
            {
                try
                {
                    return this.m_comObject.FullName;
                }
                catch { return null;  }
            }
        }

        /// <summary>
        /// The width of the document.
        /// </summary>
        public double Width
        {
            get { return this.m_comObject.Width; }
        }

        /// <summary>
        /// The height of the document.
        /// </summary>
        public double Height
        {
            get { return this.m_comObject.Height; }
        }

        /// <summary>
        /// The document's resolution (in pixels per inch).
        /// </summary>
        public double Resolution
        {
            get { return this.m_comObject.Resolution; }
        }

        /// <summary>
        /// Get and set metadata string for the document.  This is not native to the Photoshop SDK.
        /// </summary>
        public String Metadata
        {
            get { return this.m_comObject.Info.Instructions; }
            set { this.m_comObject.Info.Instructions = value; }
        }

        /// <summary>
        /// The selected layer.
        /// </summary>
        public ILayer ActiveLayer
        {
            get 
            {
                if (this.m_comObject.ActiveLayer.typename == "ArtLayer")
                {
                    return new ArtLayer(this.m_comObject.ActiveLayer);
                }
                else if (this.m_comObject.ActiveLayer.typename == "LayerSet")
                {
                    return new LayerSet(this.m_comObject.ActiveLayer);
                }

                return null;
            }

            set { this.m_comObject.ActiveLayer = value.ComObject; }
        }

        /// <summary>
        /// The ArtLayers collection.
        /// </summary>
        public ArtLayers ArtLayers
        {
            get { return new ArtLayers(this.m_comObject.ArtLayers); }
        }

        /// <summary>
        /// The LayerSets collection.
        /// </summary>
        public LayerSets LayerSets
        {
            get { return new LayerSets(this.m_comObject.LayerSets); }
        }

        /// <summary>
        /// The number of bits per channel.
        /// </summary>
        public BitsPerChannelType BitsPerChannel
        {
            get { return (BitsPerChannelType)this.m_comObject.BitsPerChannel; }
            set { this.m_comObject.BitsPerChannel = value; }
        }

        /// <summary>
        /// Metadata about the document.
        /// </summary>
        public DocumentInfo Info
        {
            get { return new DocumentInfo(this.m_comObject.Info); }
        }

        /// <summary>
        /// The Channels collection.
        /// </summary>
        public Channels Channels
        {
            get { return new Channels(this.m_comObject.Channels); }
        }

        /// <summary>
        /// The active Channels for this document.
        /// </summary>
        public Array ActiveChannels
        {
            get
            {
                Channels channels = new Channels(this.m_comObject.Channels);
                return channels.ToArray();
            }

            set { this.m_comObject.ActiveChannels = value; }
        }

        /// <summary>
        /// The PathItems collection.
        /// </summary>
        public PathItems PathItems
        {
            get { return new PathItems(this.m_comObject.PathItems); }
        }

        /// <summary>
        /// The selected area of the document.
        /// </summary>
        public Selection Selection
        {
            get { return new Selection(this.m_comObject.Selection); }
        }

        #endregion // Properties

        #region Methods

        /// <summary>
        /// Crops the document to the supplied coordinates.
        /// 
        /// This method signature is different from the native signature in the Photoshop SDK.  This
        /// is just more straightforward to use.
        /// </summary>
        /// <param name="left"></param>
        /// <param name="top"></param>
        /// <param name="right"></param>
        /// <param name="bottom"></param>
        /// <param name="angle"></param>
        public void Crop(double left, double top, double right, double bottom, double angle = 0.0)
        {
            // The native .Crop function on the document fails for some reason.  So, we have to
            // resort to doing it this crazy way, recorded from the script listener.
            Application app = new Application();

            int idCrop = app.CharIDToTypeID("Crop");
            ActionDescriptor desc188 = new ActionDescriptor();
            int idT = app.CharIDToTypeID("T   ");
            ActionDescriptor desc189 = new ActionDescriptor();

            int idPxl = app.CharIDToTypeID("#Pxl");

            // Top
            int idTop = app.CharIDToTypeID("Top ");
            desc189.PutUnitDouble(idTop, idPxl, top);

            // Left
            int idLeft = app.CharIDToTypeID("Left");
            desc189.PutUnitDouble(idLeft, idPxl, left);

            // Bottom
            int idBtom = app.CharIDToTypeID("Btom");
            desc189.PutUnitDouble(idBtom, idPxl, bottom);

            // Right
            int idRght = app.CharIDToTypeID("Rght");
            desc189.PutUnitDouble(idRght, idPxl, right);

            int idRctn = app.CharIDToTypeID("Rctn");
            desc188.PutObject(idT, idRctn, desc189);

            // Angle
            int idAngl = app.CharIDToTypeID("Angl");
            int idAng = app.CharIDToTypeID("#Ang");
            desc188.PutUnitDouble(idAngl, idAng, angle);

            int idDlt = app.CharIDToTypeID("Dlt ");
            desc188.PutBoolean(idDlt, true);

            int idCropAspectRatioModeKey = app.StringIDToTypeID("cropAspectRatioModeKey");
            int idCropAspectRatioModeClass = app.StringIDToTypeID("cropAspectRatioModeClass");
            int idPureAspectRatio = app.StringIDToTypeID("pureAspectRatio");
            desc188.PutEnumerated(idCropAspectRatioModeKey, idCropAspectRatioModeClass, idPureAspectRatio);

            int idCnsp = app.CharIDToTypeID("CnsP");
            desc188.PutBoolean(idCnsp, false);

            app.ExecuteAction(idCrop, desc188, DialogModes.DisplayNoDialogs);
        }

        /// <summary>
        /// Creates a duplicate of the Document object.
        /// </summary>
        /// <param name="name">The name of the duplicated document.</param>
        /// <param name="mergeLayersOnly">Whether to only duplicate merged layers.</param>
        /// <returns></returns>
        public Document Duplicate(String name, bool mergeLayersOnly = false)
        {
            return new Document(this.m_comObject.Duplicate(name, mergeLayersOnly));
        }

        /// <summary>
        /// Changes the size of the image.
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="resolution"></param>
        /// <param name="resampleMethod"></param>
        /// <param name="amount">Not supported in releases prior to Creative Cloud.</param>
        public void ResizeImage(double width, double height, double resolution, ResampleMethod resampleMethod, double amount)
        {
            // The "amount" argument was added in CC.
            // TODO: Find a nicer way to determine the active Photoshop version other than making another COM instance.
            Application app = new Application();

            if (app.Version == PhotoshopVersion.CreativeCloud)
            {
                this.m_comObject.ResizeImage(width, height, resolution, resampleMethod, amount);
            }
            else
            {
                this.m_comObject.ResizeImage(width, height, resolution, resampleMethod);
            }
        }

        /// <summary>
        /// Closes the document.  If any changes have been made, an alert with three options
        /// is presented to save.  The optional parameter specifies a selection in the alert box.
        /// </summary>
        /// <param name="options"></param>
        public void Close(SaveOptions options = SaveOptions.DoNotSaveChanges)
        {
            this.m_comObject.Close(options);
        }

        /// <summary>
        /// Pastes the contents of the clipboard into the document.  If the optional argument is
        /// set to true and a selection is active, the contents are pasted into the selection.
        /// </summary>
        /// <param name="intoSelection"></param>
        /// <returns></returns>
        public ArtLayer Paste(bool intoSelection = true)
        {
            return new ArtLayer(this.m_comObject.Paste(intoSelection));
        }

        /// <summary>
        /// Saves the document with the specified options.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="options"></param>
        /// <param name="asCopy"></param>
        /// <param name="extensionType"></param>
        public void SaveAs(String filename, ISaveOptions options, bool asCopy = false, ExtensionType extensionType = ExtensionType.LowerCase )
        {
            this.m_comObject.SaveAs(filename, options.ComObject, asCopy, extensionType);
        }

        /// <summary>
        /// Saves the document with the specified options.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="saveDocumentType"></param>
        /// <param name="asCopy"></param>
        /// <param name="extensionType"></param>
        public void SaveAs(String filename, SaveDocumentType saveDocumentType, bool asCopy = false, ExtensionType extensionType = ExtensionType.LowerCase)
        {
            this.m_comObject.SaveAs(filename, saveDocumentType, asCopy, extensionType);
        }

        /// <summary>
        /// Saves the document.
        /// </summary>
        public void Save()
        {
            this.m_comObject.Save();
        }

        #endregion // Methods
    }

    /// <summary>
    /// The collection of open Document objects.
    /// </summary>
    public sealed class Documents : List<Document>
    {
        #region Member Data

        private dynamic m_comObject;

        #endregion // Member Data

        #region Constructor(s)

        public Documents(dynamic documentsComObject) 
            : base()
        {
            this.m_comObject = documentsComObject;

            foreach (dynamic docComObject in documentsComObject)
            {
                this.Add(new Document(docComObject));
            }
        }

        #endregion // Constructor(s)

        #region Properties

        /// <summary>
        /// Access to the real COM object for this wrapper.
        /// </summary>
        public dynamic ComObject
        {
            get { return this.m_comObject; }
        }

        #endregion // Properties

        #region Methods

        /// <summary>
        /// Adds a Document object.
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="resolution"></param>
        /// <param name="name"></param>
        /// <param name="mode"></param>
        /// <param name="initialFill"></param>
        /// <param name="pixelAspectRatio"></param>
        /// <param name="bitsPerChannel"></param>
        /// <param name="colorProfileName"></param>
        /// <returns></returns>
        public Document Add(double width, double height, double resolution = 72.0, String name = "Untitled", NewDocumentMode mode = NewDocumentMode.NewRGB, DocumentFill initialFill = DocumentFill.Transparent, double pixelAspectRatio = 1.0, BitsPerChannelType bitsPerChannel = BitsPerChannelType.Document16Bits, String colorProfileName = "sRGB IEC61966-2.1")
        {
            // Create a new ArtLayer object.
            Document doc = new Document(this.m_comObject.Add(width, height, resolution, name, mode, initialFill, pixelAspectRatio, bitsPerChannel, colorProfileName));

            // Add it to this collection.
            this.Add(doc);
            return doc;
        }

        #endregion // Methods
    }

    /// <summary>
    /// Metadata about a Document object.  These values can be set by choosing File > File Info in the Photoshop application.
    /// </summary>
    public sealed class DocumentInfo : ComWrapper
    {
        #region Member Data
        #endregion // Member Data

        #region Constructor(s)

        public DocumentInfo(dynamic docInfoComObject)
            : base((object)docInfoComObject)
        {
        }

        #endregion // Constructor(s)

        #region Properties

        public String Instructions
        {
            get { return this.m_comObject.Instructions; }
            set { this.m_comObject.Instructions = value; }
        }

        #endregion // Properties
    }
}
