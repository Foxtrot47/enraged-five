﻿//---------------------------------------------------------------------------------------------
// <copyright file="ActionList.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Interop.Adobe.Photoshop
{
    public sealed class ActionList : ComWrapper
    {
        #region Member Data
        #endregion // Member Data

        #region Constructor(s)

        public ActionList()
            : base()
        {
            Type actionListType = Type.GetTypeFromProgID("Photoshop.ActionList");
            this.m_comObject = (dynamic)Activator.CreateInstance(actionListType);
        }

        public ActionList(dynamic actionListComObject)
            : base((object)actionListComObject)
        {
        }

        #endregion // Constructor(s)

        #region Properties
        #endregion // Properties

        #region Methods

        /// <summary>
        /// Gets the value of a list item of type object.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public ActionDescriptor GetObjectValue(int index)
        {
            return new ActionDescriptor(this.m_comObject.GetObjectValue(index));
        }

        #endregion // Methods
    }
}
