﻿//---------------------------------------------------------------------------------------------
// <copyright file="ISaveOptions.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Interop.Adobe.Photoshop
{
    public interface ISaveOptions
    {
        dynamic ComObject { get; }
    }
}
