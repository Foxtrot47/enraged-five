﻿//---------------------------------------------------------------------------------------------
// <copyright file="LayerSets.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Interop.Adobe.Photoshop
{
    /// <summary>
    /// The collection of LayerSet objects in the document.
    /// </summary>
    public sealed class LayerSets : List<LayerSet>
    {
        #region Member Data

        // The real Photoshop LayerSets collection object.
        private dynamic m_comObject;

        #endregion // Member Data

        #region Constructor(s)

        public LayerSets(dynamic layerSetsComObject)
            : base()
        {
            this.m_comObject = layerSetsComObject;

            foreach (dynamic realLayerSet in layerSetsComObject)
            {
                this.Add(new LayerSet(realLayerSet));
            }
        }

        #endregion // Constructor(s)

        #region Properties

        /// <summary>
        /// Access to the real COM object for this wrapper.
        /// </summary>
        public dynamic ComObject
        {
            get { return this.m_comObject; }
        }

        #endregion // Properties

        #region Methods

        /// <summary>
        /// Creates a new LayerSet object.
        /// </summary>
        /// <returns></returns>
        public LayerSet Add()
        {
            // Create a new LayerSet object.
            LayerSet layerSet = new LayerSet(this.m_comObject.Add());

            // Add it to this collection.
            this.Add(layerSet);
            return layerSet;
        }

        #endregion // Methods
    }
}
