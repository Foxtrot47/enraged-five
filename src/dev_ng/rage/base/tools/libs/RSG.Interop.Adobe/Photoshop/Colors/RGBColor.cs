﻿//---------------------------------------------------------------------------------------------
// <copyright file="RGBColor.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Interop.Adobe.Photoshop
{
    /// <summary>
    /// Wrapper class for Photoshop RGBColor object.
    /// 
    /// For scripting reference, see:
    /// 
    ///     CC : http://wwwimages.adobe.com/content/dam/Adobe/en/devnet/photoshop/pdfs/photoshop-cc-vbs-ref.pdf
    ///     
    /// </summary>
    public sealed class RGBColor : ComWrapper
    {
        #region Member Data
        #endregion // Member Data

        #region Constructor(s)

        public RGBColor()
            : base()
        {
            Type optionsType = Type.GetTypeFromProgID("Photoshop.RGBColor");
            this.m_comObject = (dynamic)Activator.CreateInstance(optionsType);
        }

        public RGBColor(dynamic obj)
            : base((object)obj)
        {
        }

        public RGBColor(double red, double green, double blue)
            : base()
        {
            Type optionsType = Type.GetTypeFromProgID("Photoshop.RGBColor");
            this.m_comObject = (dynamic)Activator.CreateInstance(optionsType);

            this.m_comObject.Red = red;
            this.m_comObject.Green = green;
            this.m_comObject.Blue = blue;
        }

        #endregion // Constructor(s)

        #region Properties

        /// <summary>
        /// The blue color value (0.0 - 255.0).
        /// </summary>
        public double Blue
        {
            get { return this.m_comObject.Blue; }
            set { this.m_comObject.Blue = value; }
        }

        /// <summary>
        /// The green color value (0.0 - 255.0).
        /// </summary>
        public double Green
        {
            get { return this.m_comObject.Green; }
            set { this.m_comObject.Green = value; }
        }

        /// <summary>
        /// The red color value (0.0 - 255.0).
        /// </summary>
        public double Red
        {
            get { return this.m_comObject.Red; }
            set { this.m_comObject.Red = value; }
        }

        /// <summary>
        /// The hex representation of the color.
        /// </summary>
        public String HexValue
        {
            get { return this.m_comObject.HexValue; }
            set { this.m_comObject.HexValue = value; }
        }

        #endregion // Properties

        #region Methods
        #endregion // Methods
    }
}
