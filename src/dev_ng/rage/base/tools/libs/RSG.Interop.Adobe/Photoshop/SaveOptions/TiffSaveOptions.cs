﻿//---------------------------------------------------------------------------------------------
// <copyright file="TiffSaveOptions.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Interop.Adobe.Photoshop
{
    /// <summary>
    /// Wrapper class for TIFFSaveOptions.
    /// 
    /// For scripting reference, see:
    /// 
    ///     CC : http://wwwimages.adobe.com/content/dam/Adobe/en/devnet/photoshop/pdfs/photoshop-cc-vbs-ref.pdf
    ///     
    /// </summary>
    public sealed class TiffSaveOptions : ComWrapper, ISaveOptions
    {
        #region Member Data
        #endregion // Member Data

        #region Constructor(s)

        public TiffSaveOptions()
            : base()
        {
            Type optionsType = Type.GetTypeFromProgID("Photoshop.TiffSaveOptions");
            this.m_comObject = (dynamic)Activator.CreateInstance(optionsType);
        }

        #endregion // Constructor(s)

        #region Properties

        /// <summary>
        /// Indicates whether to save the alpha channels.
        /// </summary>
        public bool AlphaChannels
        {
            get { return this.m_comObject.AlphaChannels; }
            set { this.m_comObject.AlphaChannels = value; }
        }

        /// <summary>
        /// Indicates whether to save the annotations.
        /// </summary>
        public bool Annotations
        {
            get { return this.m_comObject.Annotations; }
            set { this.m_comObject.Annotations = value; }
        }

        /// <summary>
        /// The order in which the document's bytes will be read.
        /// </summary>
        public ByteOrder ByteOrder
        {
            get { return (ByteOrder)this.m_comObject.ByteOrder; }
            set { this.m_comObject.ByteOrder = value; }
        }

        /// <summary>
        /// Indicates whether to embed the color profile in the document.
        /// </summary>
        public bool EmbedColorProfile
        {
            get { return this.m_comObject.EmbedColorProfile; }
            set { this.m_comObject.EmbedColorProfile = value; }
        }

        /// <summary>
        /// The compression type.  Default: 1 (NoTIFFCompression).
        /// </summary>
        public TIFFEncodingType ImageCompression
        {
            get { return (TIFFEncodingType)this.m_comObject.ImageCompression; }
            set { this.m_comObject.ImageCompression = value; }
        }

        /// <summary>
        /// Indicates whether the channels in the image will be interleaved.
        /// </summary>
        public bool InterleaveChannels
        {
            get { return this.m_comObject.InterleaveChannels; }
            set { this.m_comObject.InterleaveChannels = value; }
        }

        /// <summary>
        /// The quality of the produced image (0 - 12), which is inversely proportionate to the amount of JPEG compression.  Valid only when 
        /// ImageCompression = 3 (TiffJPEG).
        /// </summary>
        public long JPEGQuality
        {
            get { return this.m_comObject.JPEGQuality; }
            set { this.m_comObject.JPEGQuality = value; }
        }

        /// <summary>
        /// The method of compression to use when saving layers (as opposed to saving composite data).
        /// Valid only when Layers = true.
        /// </summary>
        public LayerCompressionType LayerCompression
        {
            get { return (LayerCompressionType)this.m_comObject.LayerCompression; }
            set { this.m_comObject.LayerCompression = value; }
        }

        /// <summary>
        /// Indicates whether to save the layers.
        /// </summary>
        public bool Layers
        {
            get { return this.m_comObject.Layers; }
            set { this.m_comObject.Layers = value; }
        }

        /// <summary>
        /// Indicates whether to preserve multiresolution information.
        /// Default: false
        /// </summary>
        public bool SaveImagePyramid
        {
            get { return this.m_comObject.SaveImagePyramid; }
            set { this.m_comObject.SaveImagePyramid = value; }
        }

        /// <summary>
        /// Indicates whether to save the spot colors.
        /// </summary>
        public bool SpotColors
        {
            get { return this.m_comObject.SpotColors; }
            set { this.m_comObject.SpotColors = value; }
        }

        /// <summary>
        /// Indicates whether to save the transparency as an additional alpha channel when the file is opened in another application.
        /// </summary>
        public bool Transparency
        {
            get { return this.m_comObject.Transparency; }
            set { this.m_comObject.Transparency = value; }
        }

        #endregion // Properties
    }
}
