﻿//---------------------------------------------------------------------------------------------
// <copyright file="Photoshop.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Adobe
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using Microsoft.Win32;

    /// <summary>
    /// Adobe Photoshop interoperability class; providing information about 
    /// which versions are installed and for loading their COM interaction
    /// libraries.
    /// </summary>
    /// For Adobe Photoshop COM interoperability that is relatively version
    /// independent use late-binding; see http://support.microsoft.com/kb/302902
    /// and http://stackoverflow.com/questions/5947675/com-interface-photoshop-compatibility-issue.
    /// 
    /// Note: currently only 64-bit versions of Photoshop are supported.
    /// 
    /// <seealso cref="http://support.microsoft.com/kb/245115" />
    /// <seealso cref="http://support.microsoft.com/kb/302902" />
    /// <seealso cref="http://stackoverflow.com/questions/5947675/com-interface-photoshop-compatibility-issue" />
    /// 
    public static class PhotoshopApplication
    {
        #region Controller Methods
        /// <summary>
        /// Determine whether any version of Photoshop is installed.
        /// </summary>
        /// <returns></returns>
        public static bool IsInstalled()
        {
            bool result = false;
            IEnumerable<PhotoshopVersion> versions =
                Enum.GetValues(typeof (PhotoshopVersion)).Cast<PhotoshopVersion>();
            foreach (PhotoshopVersion version in versions)
            {
                if (PhotoshopVersion.None != version)
                {
                    result |= (IsVersionInstalled(version));
                }
            }
            return (result);
        }

        /// <summary>
        /// Determine whether a particular version of Photoshop is installed.
        /// </summary>
        /// <param name="version"></param>
        /// <returns></returns>
        public static bool IsVersionInstalled(PhotoshopVersion version)
        {
            String installPath = GetInstallPath(version);
            return (!String.IsNullOrWhiteSpace(installPath));
        }

        /// <summary>
        /// Determines the latest version of Photoshop version that is installed.
        /// </summary>
        /// <returns></returns>
        public static PhotoshopVersion GetLatestInstalledVersion()
        {
            if (IsVersionInstalled(PhotoshopVersion.CreativeCloud2014))
            {
                return PhotoshopVersion.CreativeCloud2014;
            } 
            else if (IsVersionInstalled(PhotoshopVersion.CreativeCloud))
            {
                return PhotoshopVersion.CreativeCloud;
            } 
            else if (IsVersionInstalled(PhotoshopVersion.CS5))
            {
                return PhotoshopVersion.CS5;
            }
            else if (IsVersionInstalled(PhotoshopVersion.CS4))
            {
                return PhotoshopVersion.CS4;
            }
            else if (IsVersionInstalled(PhotoshopVersion.CS3))
            {
                return PhotoshopVersion.CS3;
            }
            else if (IsVersionInstalled(PhotoshopVersion.CS2))
            {
                return PhotoshopVersion.CS2;
            }
            else if (IsVersionInstalled(PhotoshopVersion.CS))
            {
                return PhotoshopVersion.CS;
            }
            else
            {
                return PhotoshopVersion.None;
            }
        }

        /// <summary>
        /// Get installation path for a particular version of Photoshop.
        /// </summary>
        /// <param name="version"></param>
        /// <returns></returns>
        public static String GetInstallPath(PhotoshopVersion version)
        {
            RegistryKey rootKey = RegistryKey.OpenBaseKey(
                Microsoft.Win32.RegistryHive.LocalMachine, RegistryView.Registry64);

            RegistryKey versionKey = rootKey.OpenSubKey(version.RegistryKey());
            if (null != versionKey)
            {
                String installPath = (String)versionKey.GetValue("ApplicationPath");
                return (installPath);
            }
            return (null);
        }

        /// <summary>
        /// Get plugin path for a particular version of Photoshop.
        /// </summary>
        /// <param name="version"></param>
        /// <returns></returns>
        public static String GetPluginPath(PhotoshopVersion version)
        {
            RegistryKey rootKey = RegistryKey.OpenBaseKey(
                Microsoft.Win32.RegistryHive.LocalMachine, RegistryView.Registry64);

            RegistryKey versionKey = rootKey.OpenSubKey(version.RegistryKey());
            if (null != versionKey)
            {
                String installPath = (String)versionKey.GetValue("PluginPath");
                return (installPath);
            }
            return (null);
        }

        /// <summary>
        /// Get executable path for a particular version of Adobe Photoshop.
        /// </summary>
        /// <param name="version"></param>
        /// <returns></returns>
        public static String GetExecutablePath(PhotoshopVersion version)
        {
            Debug.Assert(IsVersionInstalled(version), "Photoshop version is not available.");
            if (!IsVersionInstalled(version))
                throw (new AdobeInteropException("Photoshop version not installed."));

            String installDir = GetInstallPath(version);
            return (Path.Combine(installDir, PHOTOSHOP_EXE));
        }

        /// <summary>
        /// Return true iff the source file looks like a Photoshop file.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static bool IsPhotoshopFile(String filename)
        {
            String extension = Path.GetExtension(filename);
            return (PHOTOSHOP_EXTENSIONS.Contains(extension));
        }
        
        /// <summary>
        /// Creates a dynamic Photoshop application COM object server dynamically
        /// (no need to link against particular COM libraries).
        /// </summary>
        /// <returns></returns>
        public static Object CreatePhotoshopComObject()
        {
            Debug.Assert(Environment.Is64BitProcess,
                "Process not 64-bit; Photoshop Automation not available.");
            if (!Environment.Is64BitProcess)
                throw (new AdobeInteropException("Process not 64-bit; Photoshop Automation not available."));

            Type appPhotoshopType = Type.GetTypeFromProgID(PHOTOSHOP_COM);
            Debug.Assert(null != appPhotoshopType, "Failed to find Photoshop application type.");
            if (null == appPhotoshopType) 
                throw (new AdobeInteropException("Failed to find Photoshop application type."));
            
            Object appPhotoshop = Activator.CreateInstance(appPhotoshopType);
            return (appPhotoshop);
        }
        #endregion // Controller Methods

        #region Constants
        /// <summary>
        /// Photoshop executable name.
        /// </summary>
        private static readonly String PHOTOSHOP_EXE = "photoshop.exe";

        /// <summary>
        /// Photoshop Application COM object.
        /// </summary>
        private static readonly String PHOTOSHOP_COM = "Photoshop.Application";

        /// <summary>
        /// Photoshop file extensions.
        /// </summary>
        private static readonly String[] PHOTOSHOP_EXTENSIONS = new string[]
        {
            ".psd",
            ".pdd",
        };
        #endregion // Constants
    }

} // RSG.Interop.Adobe namespace
