// 
// rageAssetLog/rageAssetLog.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "rageAssetLog.h"

#include "file/device.h"
#include "file/asset.h"
#include "file/stream.h"
#include "file/token.h"
#include "diag/output.h"
#include "diag/channel.h"

using namespace rage;

#define RAGE_ASSET_LOG_SUCCESS_STRING		"SUCCESS"
#define RAGE_ASSET_LOG_SUCCESS_STRING_SIZE	7

#define RAGE_ASSET_LOG_FAILURE_STRING		"FAILED"


// Set up the functions to set the output to the log file
static rageAssetLog *			s_pCurrentLog = NULL;


bool LogPrinterFunction(const diagChannel &channel,diagSeverity severity,
							const char *file,int line,const char *msg, va_list args)
{
	if (s_pCurrentLog && s_pCurrentLog->GetTokenizer())
	{
		s_pCurrentLog->GetTokenizer()->StartLine();
		s_pCurrentLog->GetTokenizer()->Put(msg);
		s_pCurrentLog->GetTokenizer()->EndLine();
	}
	return s_pCurrentLog->GetPrevOutput()(channel,severity,file,line,msg,args);
}

// Constructor
rageAssetLog::rageAssetLog(const char * outputAssetFileName)
{
	memset(m_inputLogFileName, 0, sizeof(m_inputLogFileName));
	fiAssetManager::BaseName(m_inputLogFileName, sizeof(m_inputLogFileName), outputAssetFileName);
	strcat(m_inputLogFileName, ".log");

	// Initialize the success/failure marker
	m_bSuccess = true;

	m_bStarted = false;

	// Set the flag for if we are new
	m_isNew = true;

	fiStream * logFile = fiStream::Open(m_inputLogFileName);
	if (logFile)
	{
		int depCount = 0;
		logFile->ReadInt(&depCount, 1);

		// It appears, somehow, sometimes, depCount can be insanely huge, e.g.  542327876 
		// This makes everything fall over and die, so check for this.
		if(depCount < 100)
		{
			m_dependencies.Resize(depCount);
			for (int i=0; i<depCount; i++)
			{
				logFile->Read(&m_dependencies[i].m_timeStamp, sizeof(m_dependencies[i].m_timeStamp));
			}

			// Race to the end of the file to see if we were successful last time we ran
			int fileSize = logFile->Size();
			if (fileSize > RAGE_ASSET_LOG_SUCCESS_STRING_SIZE)
			{
				char success[RAGE_ASSET_LOG_SUCCESS_STRING_SIZE+1];
				memset(success, 0, sizeof(success));
				logFile->Seek(fileSize - RAGE_ASSET_LOG_SUCCESS_STRING_SIZE);
				logFile->Read(success, RAGE_ASSET_LOG_SUCCESS_STRING_SIZE);
				if (strcmp(success, RAGE_ASSET_LOG_SUCCESS_STRING)==0)
					m_isNew = false;
			}
		}

		// Close this log file
		logFile->Close();
		logFile = NULL;
	}

	// Initalize the other member data
	m_logFileStream		= NULL;
	m_logFileTokenizer	= NULL;
}

// Destructor
rageAssetLog::~rageAssetLog()
{
	Shutdown();
}

// Function that initalizes the log class
bool rageAssetLog::Init(atArray <std::string> & dependencyFileNames)
{
	// Make sure the counts are the same
	if (dependencyFileNames.GetCount() != m_dependencies.GetCount())
		m_isNew = true;

	// Initialize the new dependency list
	atArray <rageAssetLogDependency> newDependencies;
	newDependencies.Resize(dependencyFileNames.GetCount());	

	// Initalize the data
	for (int i=0; i<newDependencies.GetCount(); i++)
	{
		newDependencies[i].m_timeStamp = fiDeviceLocal::GetInstance().GetFileTime(dependencyFileNames[i].c_str());

		// Check to see if we are new
		if (!m_isNew)
		{
			bool foundFile = false;
			for (int j=0; j<m_dependencies.GetCount(); j++)
			{
				if (m_dependencies[j].m_timeStamp == newDependencies[i].m_timeStamp)
				{
					foundFile = true;
					break;
				}
			}

			if (!foundFile)
			{
				m_isNew = true;
			}
		}
	}

	// Store the new dependents
	m_dependencies.Assume(newDependencies);

	return true;
}

// Function to destroy the data
bool rageAssetLog::Shutdown()
{
	// Write a successful message here
	if (m_logFileTokenizer)
	{
		m_logFileTokenizer->EndBlock();
		if(m_bSuccess)
			m_logFileTokenizer->PutStr(RAGE_ASSET_LOG_SUCCESS_STRING);
		else
			m_logFileTokenizer->PutStr(RAGE_ASSET_LOG_FAILURE_STRING);
	}

	if (m_bStarted)
	{
		Stop();
	}

	m_isNew = true;
	memset(m_inputLogFileName, 0, sizeof(m_inputLogFileName));
	m_dependencies.Reset();

	if (m_logFileTokenizer)
	{
		delete m_logFileTokenizer;
		m_logFileTokenizer = NULL;
	}

	if (m_logFileStream)
	{
		m_logFileStream->Close();
		m_logFileStream = NULL;
	}

	return true;
}

// Function to start the logging process
bool rageAssetLog::Start()
{
	// Open the log file and write the dependency information
	m_logFileStream = fiStream::Create(m_inputLogFileName);
	if (!m_logFileStream)
		return false;

	int depCount = m_dependencies.GetCount();
	m_logFileStream->WriteInt(&depCount, 1);

	for (int i=0; i<m_dependencies.GetCount(); i++)
	{
		m_logFileStream->Write(&m_dependencies[i].m_timeStamp, sizeof(m_dependencies[i].m_timeStamp));
	}

	m_logFileTokenizer = rage_new fiTokenizer(m_inputLogFileName, m_logFileStream);
	if (!m_logFileTokenizer)
		return false;

	m_logFileTokenizer->StartLine();
	m_logFileTokenizer->PutStr("\n\nBegin Logging");
	m_logFileTokenizer->EndLine();
	m_logFileTokenizer->StartBlock();

	// Fixup the logging so all the print commands go to the log file
	m_prevOutput = diagLogCallback;
	s_pCurrentLog = this;
	diagLogCallback =  LogPrinterFunction;

	m_bStarted = true;

	return true;
}

bool rageAssetLog::Stop()
{
	if (m_bStarted)
	{
		diagLogCallback = m_prevOutput;

		m_bStarted = false;

		return true;
	}
	else
	{
		return false;
	}
}
