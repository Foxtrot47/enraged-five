// 
// rageAssetLog/rageAssetLog.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
//
#ifndef RAGE_ASSET_LOG_H
#define RAGE_ASSET_LOG_H

#include <string>

#include "atl/array.h"
#include "diag/channel.h"

namespace rage {

class fiStream;
class fiTokenizer;

// Create a class to handle the RAGE assets dependency file
struct rageAssetLogDependency {
	u64		m_timeStamp;
};

// Create a class to handle logging of RAGE assets
class rageAssetLog {

public:
	rageAssetLog(const char * outputAssetFileName);
	~rageAssetLog();

	// Function to return the status of this file asset
	bool IsNew() const {return m_isNew;}

	// Function to return the tokenizer for this log file
	fiTokenizer * GetTokenizer() {return m_logFileTokenizer;}

	// Function that initalizes the log class
	bool Init(atArray <std::string> & dependencyFileNames);

	// Shutdown this log class
	bool Shutdown();

	// Function to start the logging process
	bool Start();

	// PURPOSE: Function to stop the logging process
	// RETURNS: True if the logger was stopped, false on error or if it hadn't been started
	bool Stop();

	// Function to set the overall failure/sucess status log file marker
	void SetLogSuccessFlag(bool bSuccess) { m_bSuccess = bSuccess; }

	diagLogCallbackType GetPrevOutput() {return m_prevOutput;}

private:

	// default constuctor
	rageAssetLog() {}

	char								m_inputLogFileName[512];	// The input log file name
	diagLogCallbackType					m_prevOutput;				// The previous output callback
	bool								m_isNew;					// Flag for if this object is new
	atArray <rageAssetLogDependency>	m_dependencies;				// All the dependencies for this object
	fiStream *							m_logFileStream;			// Stream object for this log file
	fiTokenizer *						m_logFileTokenizer;			// Tokenizer object for this log file
	bool								m_bSuccess;					// Flag indicating the overall success/failure flag for the log
	bool								m_bStarted;					// Has this logger been started yet
};

} // end namespace rage

#endif
