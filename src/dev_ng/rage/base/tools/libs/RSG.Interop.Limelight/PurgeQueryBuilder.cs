﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Base.Net;
using LitJson;

namespace RSG.Interop.Limelight
{
    ///////////////////////////////////////////////////////////////////////
    //   Limelight Purge API
    // http://media.limelight.com/documents/Purge-REST-API-UG-11.04.14.pdf
    ///////////////////////////////////////////////////////////////////////

    public sealed class PurgeQueryBuilder
    {
        #region Properties

        /// <summary>
        /// The full Limelight API method 
        /// </summary>
        public String ApiEndPoint;

        /// <summary>
        /// The Limelight API method (i.e. /request/)
        /// </summary>
        public string ApiMethod;

        /// <summary>
        /// 
        /// </summary>
        public Uri BaseQuery
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public String RelativeQuery
        {
            get;
            set;
        }

        /// <summary>
        /// How the parameters will be sent to the Limelight service
        /// </summary>
        public RequestMethod Method
        {
            get;
            set;
        }

        /// <summary>
        /// Content-type for POST/PUT data.
        /// </summary>
        public String ContentType
        {
            get;
            set;
        }

        /// <summary>
        /// REST PUT/POST data.
        /// </summary>
        public String Data
        {
            get;
            set;
        }

        /// <summary>
        /// Dictionary containing any "get/post" parameters
        /// </summary>
        public IDictionary<String, String> Parameters
        {
            get;
            private set;
        }

        #endregion

        #region Constructor(s)

        public PurgeQueryBuilder(string hostName, string apiName, string version, string method)
         : this (RequestMethod.GET, hostName, apiName, version, method) { }

        public PurgeQueryBuilder(RequestMethod requestMethod, string hostName, string apiName, string version, string method)
        {
            string endPoint = String.Format("{0}/{1}/{2}{3}", hostName, apiName, version, method);
            Uri uri = new Uri(endPoint);

            this.ApiEndPoint = String.Format("{0}/{1}/{2}", hostName, apiName, version);
            this.ApiMethod = method;
            this.BaseQuery = uri;
            this.Method = requestMethod;
            this.ContentType = "application/json";
            this.Parameters = new Dictionary<String, String>();
        }

        #endregion // Constructor(s)

        #region Controller Methods

        public void CreatePurgeRequest(EmailType emailType, string emailSubject, string emailTo, string emailCC, string emailBCC, List<PurgeEntry> entries)
        {
            StringBuilder sb = new StringBuilder();
            JsonWriter writer = new JsonWriter(sb);

            writer.WriteObjectStart();
            writer.WritePropertyName("emailType");
            writer.Write(emailType.ToString());

            if (!string.IsNullOrEmpty(emailSubject))
            {
                writer.WritePropertyName("emailSubject");
                writer.Write(emailSubject);
            }

            if (!string.IsNullOrEmpty(emailTo))
            {
                writer.WritePropertyName("emailTo");
                writer.Write(emailTo);
            }

            if (!string.IsNullOrEmpty(emailCC))
            {
                writer.WritePropertyName("emailCC");
                writer.Write(emailCC);
            }

            if (!string.IsNullOrEmpty(emailBCC))
            {
                writer.WritePropertyName("emailBCC");
                writer.Write(emailBCC);
            }

            if (entries.Count > 0)
            {
                writer.WritePropertyName("entries");
                writer.WriteArrayStart();
                foreach (PurgeEntry entry in entries)
                {
                    writer.WriteObjectStart();
                    writer.WritePropertyName("shortname");
                    writer.Write(entry.ShortName);
                    writer.WritePropertyName("url");
                    writer.Write(entry.Url);
                    writer.WritePropertyName("regex");
                    writer.Write(entry.Regex);
                    writer.WritePropertyName("delete");
                    writer.Write(entry.Delete);
                    writer.WriteObjectEnd();
                }
                writer.WriteArrayEnd();
            }

            writer.WriteObjectEnd();
            Data = sb.ToString();
        }

        /// <summary>
        /// Return concrete PurgeQuery.
        /// </summary>
        /// <returns></returns>
        public PurgeQuery ToQuery(string userName, string sharedKey)
        {
            String relative = this.RelativeQuery;
            if (Method == RequestMethod.GET && Parameters.Any())
            {
                // Generate the parameters string
                String parameterString = "?";
                foreach (KeyValuePair<string, string> pair in Parameters)
                {
                    parameterString += String.Format("{0}={1}&", pair.Key, pair.Value);
                }
                parameterString = parameterString.TrimEnd(new char[] { '&' });
                relative += parameterString;
            }

            Uri query = new Uri(String.Format("{0}/{1}", this.BaseQuery, relative));
            PurgeQuery pq = new PurgeQuery(query, this.Method, this.Data, ApiEndPoint, ApiMethod, userName, sharedKey);
            return pq;
        }
        #endregion // Controller Methods

    }
}
