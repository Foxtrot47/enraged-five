﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Base.Net;

namespace RSG.Interop.Limelight
{
    public interface ILimelightQuery : IRestQuery
    {
        string ApiEndpoint
        {
            get;
        }

        string ApiSharedKey
        {
            get;
        }

        string ApiUsername
        {
            get;
        }

        string ApiMethod
        {
            get;
        }
    }
}
