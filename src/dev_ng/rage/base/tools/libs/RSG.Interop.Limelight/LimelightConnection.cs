﻿using System;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Linq;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.Net;

namespace RSG.Interop.Limelight
{
    /// <summary>
    /// Limelight REST service connection.
    /// </summary>
    public class LimelightConnection : RestConnection
    {
        #region Properties
        /// <summary>
        /// Root REST service Uri.
        /// </summary>
        public Uri RESTService
        {
            get;
            private set;
        }

        /// <summary>
        /// Root Attachment service Uri.
        /// </summary>
        public Uri AttachmentService
        {
            get;
            private set;
        }

        /// <summary>
        /// Flag indicating whether the connection is a readonly connection
        /// </summary>
        public bool IsReadOnly
        {
            get;
            set;
        }

        /// <summary>
        /// Flag indicating whether the rest connection requires you to login
        /// </summary>
        protected override bool LoginRequired
        {
            get
            {
                return false;
            }
        }
        #endregion

        #region Static Properties
        /// <summary>
        /// Log object.
        /// </summary>
        internal static IUniversalLog Log
        {
            get;
            private set;
        }

        internal static UTF8Encoding Encoding
        {
            get;
            private set;
        }

        #endregion // Static Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="restService"></param>
        /// <param name="attachmentService"></param>
        public LimelightConnection(Uri restService, Uri attachmentService)
        {
            this.RESTService = restService;
            this.AttachmentService = attachmentService;
            this.IsReadOnly = false;
        }

        /// <summary>
        /// Static constructor; initialises our log.
        /// </summary>
        static LimelightConnection()
        {
            LogFactory.Initialize();
            LimelightConnection.Log = LogFactory.CreateUniversalLog("RSG.Interop.Limelight"); 
            Encoding = new System.Text.UTF8Encoding();
        }
        #endregion // Constructor(s)

        #region Private Methods

        private String GenerateMac(String sharedKey, String data)
        {
            byte[] keyByte = DecodeHex(sharedKey);
            HMACSHA256 hmacsha256 = new HMACSHA256(keyByte);
            byte[] dataBytes = Encoding.GetBytes(data);
            byte[] hashmessage = hmacsha256.ComputeHash(dataBytes);
            return ByteToString(hashmessage);
        }

        private byte[] DecodeHex(String key)
        {
            char[] cArr = key.ToCharArray();
            byte[] buf = Encoding.GetBytes(key.Substring(0, (key.Length / 2)));
            for (int i = 0; i < (key.Length / 2); i++)
            {
                String str = "";
                str += cArr[i * 2];
                str += cArr[i * 2 + 1];
                buf[i] = System.Convert.ToByte(str, 16);
            }
            return buf;
        }

        private String ByteToString(byte[] buff)
        {
            String sbinary = "";
            for (int i = 0; i < buff.Length; i++)
            {
                sbinary += buff[i].ToString("X2"); // hex format
            }
            sbinary = sbinary.ToLower();
            return (sbinary);
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Sends a query to the Limelight server
        /// Note that it is important to properly dispose of the returned stream as failure to do so could result in
        /// the application running out of connections.
        /// </summary>
        /// <param name="query"></param>
        /// <returns>Stream containing the results from the query</returns>
        public override Stream RunQuery(IRestQuery query)
        {
            if (!(query is ILimelightQuery))
            {
                Log.Error("Query was not a Limelight query");
            }

            ILimelightQuery llQuery = query as ILimelightQuery;

            // Check that the server is up and running
            if (!IsServerAvailable(llQuery))
            {
                //throw new ServerUnavailableException("Unable to reach the server located at: " + query.QueryUri.Host);
                Log.Error("Unable to reach the server located at: " + llQuery.QueryUri.Host);
            }

            // Generate the URI from the query
            Uri uri = llQuery.QueryUri;
            Log.Error("Executing rest query: {0}.", uri);

            try
            {
                HttpWebRequest http = (HttpWebRequest)HttpWebRequest.Create(uri);

                String[] uriParts = new String[2];
                if (llQuery.ApiMethod.Contains("?"))
                {
                    uriParts = llQuery.ApiMethod.Split('?');
                }
                else
                {
                    uriParts = new String[] { llQuery.ApiMethod, "" };
                }

                String data = llQuery.Method.ToString() + "";
                data += llQuery.ApiEndpoint;
                data += uriParts[0] + "/";
                if (uriParts[1] != null && uriParts[1].Length > 0)
                {
                    data += uriParts[1];
                }

                DateTime baseTime = new DateTime(1970, 1, 1, 0, 0, 0);
                DateTime nowInUTC = DateTime.UtcNow;
                long timestamp = ((nowInUTC - baseTime).Ticks / 10000);
                data += timestamp;

                if ((llQuery.Method == RequestMethod.POST || llQuery.Method == RequestMethod.PUT) &&
                    (llQuery.Data != null))
                {
                    data += llQuery.Data;
                }

                String token = GenerateMac(llQuery.ApiSharedKey, data);
                http.Accept = "application/json";
                http.Headers.Add("X-LLNW-Security-Token", token);
                http.Headers.Add("X-LLNW-Security-Principal", llQuery.ApiUsername);
                http.Headers.Add("X-LLNW-Security-Timestamp", "" + timestamp);
                http.Method = llQuery.Method.ToString();

                if ((llQuery.Method == RequestMethod.POST || llQuery.Method == RequestMethod.PUT) &&
                    (llQuery.Data != null))
                {
                    byte[] postData = Encoding.GetBytes(llQuery.Data);
                    http.ContentType = llQuery.ContentType;
                    http.ContentLength = postData.Length;

                    Stream dataStream = http.GetRequestStream();
                    dataStream.Write(postData, 0, postData.Length);
                    dataStream.Close();
                }

                HttpWebResponse response = (HttpWebResponse)http.GetResponse();
                return response.GetResponseStream();
            }
            catch (System.Net.WebException ex)
            {
                if (ex.Message.Contains("(400) Bad Request"))
                {
                    throw new Exception("Bad Request");
                }
                else if (ex.Message.Contains("(401) Unauthorized"))
                {
                    throw new Exception("The rest login details provided were not valid.");
                }
                else if (ex.Message.Contains("(404) Not Found"))
                {
                    throw new Exception("404 web exception occurred while processing Limelight command.", ex);
                }
                else if (ex.Message.Contains("(405) Method Not Allowed"))
                {
                    throw new Exception("405 web exception occurred while processing Limelight command.", ex);
                }
                else if (ex.Message.Contains("(500) Internal Server Error"))
                {
                    throw new Exception("Internal server exception occurred while processing Limelight command.", ex);
                }
                else
                {
                    throw ex;
                }
            }
        }

        #endregion // Public Methods
    } // LimelightConnection

} // RSG.Interop.Limelight namespace
