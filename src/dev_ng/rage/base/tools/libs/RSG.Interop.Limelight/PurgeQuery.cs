﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Base.Net;

namespace RSG.Interop.Limelight
{
    ///////////////////////////////////////////////////////////////////////
    //   Limelight Purge API
    // http://media.limelight.com/documents/Purge-REST-API-UG-11.04.14.pdf
    ///////////////////////////////////////////////////////////////////////

    /// <summary>
    /// Limelight service query.
    /// </summary>
    public class PurgeQuery : ILimelightQuery
    {
        #region Fields

        /// <summary>
        /// Private field for the <see cref="QueryUri"/> property.
        /// </summary>
        private readonly Uri _queryUri;

        /// <summary>
        /// Private field for the <see cref="Method"/> property.
        /// </summary>
        private readonly RequestMethod _method;

        /// <summary>
        /// Private field for the <see cref="ContentType"/> property.
        /// </summary>
        private readonly string _contentType;

        /// <summary>
        /// Private field for the <see cref="Data"/> property.
        /// </summary>
        private readonly String _data;

        /// <summary>
        /// Private field for the <see cref="ApiEndPoint"/> property.
        /// </summary>
        private readonly string _apiEndPoint;

        /// <summary>
        /// Private field for the <see cref="ApiSharedKey"/> property.
        /// </summary>
        private readonly string _apiSharedKey;

        /// <summary>
        /// Private field for the <see cref="ApiUserName"/> property.
        /// </summary>
        private readonly string _apiUserName;

        /// <summary>
        /// Private field for the <see cref="ApiUserName"/> property.
        /// </summary>
        private readonly string _apiMethod;

        #endregion

        #region Properties

        /// <summary>
        /// Uri for REST query.
        /// </summary>
        public Uri QueryUri
        {
            get { return _queryUri; }
        }

        /// <summary>
        /// REST HTTP method.
        /// </summary>
        public RequestMethod Method
        {
            get { return _method; }
        }

        /// <summary>
        /// Content-type for PUT/POST data.
        /// </summary>
        public String ContentType
        {
            get { return _contentType; }
        }

        /// <summary>
        /// REST POST/PUT data.
        /// </summary>
        public String Data
        {
            get { return _data; }
        }

        /// <summary>
        /// API End Point
        /// </summary>
        public String ApiEndpoint
        {
            get { return _apiEndPoint; }
        }

        /// <summary>
        /// API Shared Key
        /// </summary>
        public String ApiSharedKey
        {
            get { return _apiSharedKey; }
        }

        /// <summary>
        /// API User Name
        /// </summary>
        public string ApiUsername
        {
            get { return _apiUserName; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string ApiMethod
        {
            get { return _apiMethod; }
        }
        #endregion

        #region Constructor(s)
   
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="config"></param>
        /// <param name="query"></param>
        /// <param name="method"></param>
        /// <param name="data"></param>
        public PurgeQuery(Uri query, RequestMethod method, String data, string apiEndPoint, string _apiMethod, string apiUsername, string apiSharedKey)
        {
            this._queryUri = query;
            this._contentType = "application/json";
            this._method = method;
            this._data = data;
            this._apiEndPoint = apiEndPoint;
            this._apiMethod = _apiMethod;
            this._apiSharedKey = apiSharedKey;
            this._apiUserName = apiUsername;
        }

        #endregion // Constructor(s)
    }
}
