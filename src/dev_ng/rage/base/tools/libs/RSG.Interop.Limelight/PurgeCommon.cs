﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Interop.Limelight
{
    ///////////////////////////////////////////////////////////////////////
    //   Limelight Purge API
    // http://media.limelight.com/documents/Purge-REST-API-UG-11.04.14.pdf
    ///////////////////////////////////////////////////////////////////////

    public enum EmailType
    {
        none,
        detail,
        summary
    }

    public struct PurgeEntry
    {
        public PurgeEntry(string shortName, string url, bool regex, bool delete)
        {
            ShortName = shortName;
            Url = url;
            Regex = regex;
            Delete = delete;
        }

        public string ShortName;
        public string Url;
        public bool Regex;
        public bool Delete;
    }
}
