﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace RSG.Base.UnitTest
{

    /// <summary>
    /// Unit tests for the FileSize class.
    /// </summary>
    [TestClass]
    public class FileSizeTest
    {
        [TestMethod]
        public void TestFormatting()
        {
            FileSize size1k = new FileSize(1024);
            FileSize size2k = new FileSize(2048);
            FileSize size4k = new FileSize(4096);
            FileSize size1M = new FileSize(1024 * 1024);

            Assert.AreEqual(1024U, size1k.Bytes);
            Assert.AreEqual("1 kB", size1k.ToString());
            Assert.AreEqual("1,024 B", size1k.ToString("B"));
            Assert.AreEqual("1 kB", size1k.ToString("KB"));

            Assert.AreEqual(2048U, size2k.Bytes);
            Assert.AreEqual("2 kB", size2k.ToString());
            Assert.AreEqual("2,048 B", size2k.ToString("B"));

            Assert.AreEqual(4096U, size4k.Bytes);
            Assert.AreEqual("4 kB", size4k.ToString());
            Assert.AreEqual("4,096 B", size4k.ToString("B"));

            Assert.AreEqual((ulong)(1024 * 1024), size1M.Bytes);
            Assert.AreEqual("1 MB", size1M.ToString());
            Assert.AreEqual("1,024 kB", size1M.ToString("KB"));
            Assert.AreEqual("1 MB", size1M.ToString("MB"));

            ulong bytes = 4096;
            Assert.AreEqual(4096U, ((FileSize)bytes).Bytes);
        }
    }

} // RSG.Base.UnitTest namespace
