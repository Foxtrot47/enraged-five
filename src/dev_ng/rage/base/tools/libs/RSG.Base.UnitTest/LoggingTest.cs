﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;

namespace RSG.Base.UnitTest
{
    [TestClass]
    public class LoggingTest
    {
        [TestMethod]
        public void TestLogFactory()
        {
            LogFactory.Initialize();
            IUniversalLog log1 = LogFactory.CreateUniversalLog("test1");
            Assert.AreNotEqual(null, log1);
            Assert.AreEqual("test1", log1.Name);
        }

        [TestMethod]
        public void TestConsoleLog()
        {
            LogFactory.Initialize();
            IUniversalLog log = LogFactory.CreateUniversalLog("test2");
            Assert.AreNotEqual(null, log);
            Assert.AreEqual("test1", log.Name);

            LogFactory.CreateApplicationConsoleLogTarget();
            log.Message("Testing...");
        }
    }

} // RSG.Base.UnitTest
