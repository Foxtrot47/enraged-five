﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RSG.Base.Extensions;

namespace RSG.Base.UnitTest
{

    [TestClass]
    public class DateTimeExtension
    {
        [TestMethod]
        public void TestIsEarlierThan()
        {
            DateTime t1 = DateTime.Now;
            System.Threading.Thread.Sleep(1000);
            DateTime t2 = DateTime.Now;
            Assert.IsTrue(t1.IsEarlierThan(t2));

            DateTime t3 = DateTime.UtcNow;
            System.Threading.Thread.Sleep(500);
            DateTime t4 = DateTime.UtcNow;
            Assert.IsTrue(t3.IsEarlierThan(t4));
        }

        [TestMethod]
        public void TestIsLaterThan()
        {
            DateTime t1 = DateTime.Now;
            System.Threading.Thread.Sleep(1000);
            DateTime t2 = DateTime.Now;
            Assert.IsTrue(t2.IsLaterThan(t1));

            DateTime t3 = DateTime.UtcNow;
            System.Threading.Thread.Sleep(500);
            DateTime t4 = DateTime.UtcNow;
            Assert.IsTrue(t4.IsLaterThan(t3));
        }
    }

} // RSG.Base.UnitTest namespace
