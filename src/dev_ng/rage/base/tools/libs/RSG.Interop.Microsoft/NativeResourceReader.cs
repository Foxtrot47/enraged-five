﻿//---------------------------------------------------------------------------------------------
// <copyright file="NativeResourceReader.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft
{
    using System;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.IO;
    using System.Runtime.InteropServices;
    using System.Text;
    using System.Windows;
    using System.Windows.Interop;
    using System.Windows.Media.Imaging;
    using RSG.Interop.Microsoft.Windows;

    /// <summary>
    /// Native Resource Reader class; allows reading of embedded resources in native
    /// executables and dynamic-link libraries.
    /// </summary>
    public class NativeResourceReader : IDisposable
    {
        #region Delegates
        /// <summary>
        /// 
        /// </summary>
        /// <param name="hModule"></param>
        /// <param name="moduleName"></param>
        /// <param name="rexType"></param>
        /// <param name="resName"></param>
        /// <returns></returns>
        public delegate bool EnumResourceCallback(IntPtr hModule, String moduleName, ResourceType rexType, String resName);
        #endregion // Delegates

        #region Properties
        /// <summary>
        /// Library or executable absolute filename.
        /// </summary>
        public String LibraryOrExecutableFilename
        {
            get;
            private set;
        }
        #endregion // properties

        #region Member Data
        /// <summary>
        /// Loaded module handle.
        /// </summary>
        IntPtr _hModule;

        /// <summary>
        /// Temporary callback method.
        /// </summary>
        private EnumResourceCallback _cb;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor; loading library/executable to be ready for reading resources.
        /// </summary>
        /// <param name="libraryOrExecutableFilename"></param>
        public NativeResourceReader(String libraryOrExecutableFilename)
        {
            if (!File.Exists(libraryOrExecutableFilename))
                throw (new FileNotFoundException("Library or executable not found.", libraryOrExecutableFilename));

            this.LibraryOrExecutableFilename = libraryOrExecutableFilename;
            this._hModule = Kernel32.LoadLibraryEx(libraryOrExecutableFilename, IntPtr.Zero,
                LoadLibraryFlag.LOAD_LIBRARY_AS_DATAFILE);
            if (IntPtr.Zero == this._hModule)
                throw (new Win32Exception("Failed to load module."));
        }
        #endregion // Constructor(s)

        #region IDisposable Interface Methods
        /// <summary>
        /// Dispose native resources.
        /// </summary>
        public void Dispose()
        {
            if (IntPtr.Zero != this._hModule)
                Kernel32.FreeLibrary(this._hModule);
        }
        #endregion // IDisposable Interface Methods

        #region Controller Methods
        /// <summary>
        /// Read string resource from loaded module.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Resource string.</returns>
        /// <exception cref="Win32Exception">If the Resource ID is invalid.</exception>
        public String ReadStringResource(int id)
        {
            Debug.Assert(IntPtr.Zero != this._hModule);
            if (IntPtr.Zero == this._hModule)
                throw (new Win32Exception("Module is not loaded."));

            StringBuilder sb = new StringBuilder();
            int ln = User32.LoadString(this._hModule, id, sb, 255);
            if (0 == ln)
                throw (new Win32Exception(String.Format("Failed to find String Resource with ID {0}.", id)));

            return (sb.ToString());
        }

#if false
        /// <summary>
        /// Read icon resource from loaded module.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public BitmapSource ReadIconResource(int id)
        {
            Debug.Assert(IntPtr.Zero != this._hModule);
            if (IntPtr.Zero == this._hModule)
                throw (new Win32Exception("Module is not loaded."));

            IntPtr hIconResource = Kernel32.FindResource(this._hModule, id, ResourceType.Icon);
            if (IntPtr.Zero == hIconResource)
                throw (new Win32Exception(String.Format("Failed to find Icon Resource with ID {0}.", id)));

            IntPtr hIcon = Kernel32.LoadResource(this._hModule, hIconResource);
            using (System.Drawing.Icon ico = System.Drawing.Icon.FromHandle(hIcon))
            {
                BitmapSource bmp = System.Windows.Interop.Imaging.CreateBitmapSourceFromHIcon(
                    ico.Handle,
                    new System.Windows.Int32Rect(0, 0, ico.Width, ico.Height),
                    BitmapSizeOptions.FromEmptyOptions());

                return (bmp);
            }
        }

        /// <summary>
        /// Read bitmap resource from loaded module.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public BitmapSource ReadBitmapResource(int id)
        {
            Debug.Assert(IntPtr.Zero != this._hModule);
            if (IntPtr.Zero == this._hModule)
                throw (new Win32Exception("Module is not loaded."));

            IntPtr hBitmapResource = Kernel32.FindResource(this._hModule, id, ResourceType.Bitmap);
            if (IntPtr.Zero == hBitmapResource)
                throw (new Win32Exception(String.Format("Failed to find Bitmap Resource with ID {0}.", id)));

            IntPtr hBitmap = Kernel32.LoadResource(this._hModule, hBitmapResource);
            IntPtr hBitmapData = Kernel32.LockResource(hBitmap);

            System.Drawing.Bitmap bmp = System.Drawing.Bitmap.FromResource(this._hModule, 
                String.Format("##{0}", id));

            uint size = Kernel32.SizeofResource(this._hModule, hBitmapData);
            //using (System.Drawing.Bitmap bmp = System.Drawing.Bitmap.FromHbitmap(hBitmapData))
            {
#if false
                System.Drawing.Bitmap.

                BitmapSource bs = Imaging.CreateBitmapSourceFromHBitmap(hBitmapData,
                    IntPtr.Zero,
                    Int32Rect.Empty,
                    BitmapSizeOptions.FromEmptyOptions());
                return (bs);
#endif
                }
            //IntPtr pt = Kernel32.LoadResource(this._hModule, hBitmap);

            
            
            throw (new NotImplementedException());
        }
#endif

        /// <summary>
        /// Enumerate resources in module.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="cb"></param>
        public bool EnumResources(ResourceType type, EnumResourceCallback cb)
        {
            // Prevent multiple EnumResource calls at the same time.
            lock (this)
            {
                this._cb = cb;
                if (Kernel32.EnumResourceNames(this._hModule, type,
                    new Kernel32.EnumResourceDelegate(InternalEnumResourceHandler), IntPtr.Zero) == false)
                {
                    //Console.WriteLine(“gle: {0}”, Marshal.GetLastWin32Error());
                }
                this._cb = null;
            }

            return (true);
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Internal callback for enumerating resources.
        /// </summary>
        /// <param name="hModule"></param>
        /// <param name="lpszType"></param>
        /// <param name="lpszName"></param>
        /// <param name="lParam"></param>
        /// <returns></returns>
        private bool InternalEnumResourceHandler(IntPtr hModule, IntPtr lpszType, IntPtr lpszName, IntPtr lParam)
        {
            // Callback; invoke user-function.
            if (null != this._cb)
            {
                String resTypeStr = GetResourceName(lpszType);
                ResourceType resType;
                Enum.TryParse(resTypeStr, out resType);

                _cb(this._hModule, this.LibraryOrExecutableFilename, resType, GetResourceName(lpszName));
            }

            return (true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private static bool IsIntResource(IntPtr value)
        {
            if ( ((uint) value) > ushort.MaxValue)
              return false;
            return true;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private static string GetResourceName(IntPtr value)
        {
            if (IsIntResource(value) == true)
                return value.ToString();
            return Marshal.PtrToStringUni((IntPtr) value);
        }
        #endregion // Private Methods
    }

} // RSG.Interop.Microsoft namespace
