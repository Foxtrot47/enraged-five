﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Win32;

namespace RSG.Interop.Microsoft
{

    /// <summary>
    /// Visual Studio interoperability class.
    /// </summary>
    public static class VisualStudio
    {
        #region Controller Methods
        /// <summary>
        /// Determine whether any version of Visual Studio is installed.
        /// </summary>
        /// <returns></returns>
        public static bool IsInstalled()
        {
            bool result = true;
            IEnumerable<VisualStudioVersion> versions = 
                Enum.GetValues(typeof(VisualStudioVersion)).Cast<VisualStudioVersion>();
            foreach (VisualStudioVersion version in versions)
                result |= IsVersionInstalled(version);

            return (result);
        }

        /// <summary>
        /// Determine whether a particular version of Visual Studio is installed.
        /// </summary>
        /// <param name="version"></param>
        /// <returns></returns>
        /// Ref http://www.mztools.com/articles/2008/MZ2008003.aspx
        /// 
        public static bool IsVersionInstalled(VisualStudioVersion version)
        {
            String key = String.Empty;
            switch (version)
            {
                case VisualStudioVersion.VS2005:
                    key = @"Software\Microsoft\VisualStudio\8.0";
                    break;
                case VisualStudioVersion.VS2008:
                    key = @"Software\Microsoft\VisualStudio\9.0";
                    break;
                case VisualStudioVersion.VS2010:
                    key = @"Software\Microsoft\VisualStudio\10.0";
                    break;
                case VisualStudioVersion.VS2012:
                    key = @"Software\Microsoft\VisualStudio\11.0";
                    break;
                case VisualStudioVersion.VS2013:
                    key = @"Software\Microsoft\VisualStudio\12.0";
                    break;
                default:
                    throw (new ArgumentOutOfRangeException("version"));
            }

            // See if the key exists.
            using (RegistryKey vsKey = Registry.LocalMachine.OpenSubKey(key))
            {
                return (null != vsKey);
            }
        }
        #endregion // Controller Methods
    }

} // RSG.Interop.Microsoft namespace
