﻿//---------------------------------------------------------------------------------------------
// <copyright file="RegistryHelper.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System.Linq;
    using global::Microsoft.Win32;

    /// <summary>
    /// Provides static methods used to read and write to the registry.
    /// </summary>
    public static class RegistryHelper
    {
        #region Methods
        /// <summary>
        /// Determines whether the key with the specified path exists.
        /// </summary>
        /// <param name="path">
        /// The path to the key to test.
        /// </param>
        /// <returns>
        /// True if the registry key with the specified path exists; otherwise, false.
        /// </returns>
        public static bool DoesLocalKeyExist(string path)
        {
            using (RegistryKey key = Registry.LocalMachine.OpenSubKey(path))
            {
                return key != null;
            }
        }

        /// <summary>
        /// Determines whether the key with the specified path exists.
        /// </summary>
        /// <param name="path">
        /// The path to the key to test.
        /// </param>
        /// <returns>
        /// True if the registry key with the specified path exists; otherwise, false.
        /// </returns>
        public static bool DoesUserKeyExist(string path)
        {
            using (RegistryKey key = Registry.CurrentUser.OpenSubKey(path))
            {
                return key != null;
            }
        }

        /// <summary>
        /// Gets or creates the registry key that contains the editor setting values.
        /// </summary>
        /// <param name="registryHierarchy">
        /// The array of strings that specify the location of the key inside the registry.
        /// </param>
        /// <param name="create">
        /// A value indicating whether the subkey should be created if not present.
        /// </param>
        /// <returns>
        /// The registry key at the specified location if found or created; otherwise, null.
        /// </returns>
        public static RegistryKey GetUserKey(string[] registryHierarchy, bool create)
        {
            RegistryKey currentKey = Registry.CurrentUser;
            foreach (string subKeyName in registryHierarchy)
            {
                RegistryKey subLevel = null;
                using (currentKey)
                {
                    subLevel = GetSubKey(currentKey, subKeyName, create);
                    if (subLevel == null)
                    {
                        return null;
                    }
                }

                currentKey = subLevel;
            }

            return currentKey;
        }

        /// <summary>
        /// Gets or creates the registry key that contains the editor setting values.
        /// </summary>
        /// <param name="registryHierarchy">
        /// The array of strings that specify the location of the key inside the registry.
        /// </param>
        /// <param name="create">
        /// A value indicating whether the subkey should be created if not present.
        /// </param>
        /// <returns>
        /// The registry key at the specified location if found or created; otherwise, null.
        /// </returns>
        public static RegistryKey TryGetUserKey(string[] registryHierarchy, bool create)
        {
            RegistryKey currentKey = Registry.CurrentUser;

            try
            {
                foreach (string subKeyName in registryHierarchy)
                {
                    RegistryKey subLevel = null;
                    using (currentKey)
                    {
                        subLevel = GetSubKey(currentKey, subKeyName, create);
                        if (subLevel == null)
                        {
                            return null;
                        }
                    }

                    currentKey = subLevel;
                }

                return currentKey;
            }
            catch
            {
            }

            return null;
        }

        /// <summary>
        /// Gets or creates the specified sub key on the specified current key.
        /// </summary>
        /// <param name="current">
        /// The current key that should contain a sub key with the specified name.
        /// </param>
        /// <param name="name">
        /// The name of the sub key to retrieve.
        /// </param>
        /// <param name="create">
        /// A value indicating whether the subkey should be created if not present.
        /// </param>
        /// <returns>
        /// The registry key with the specified name underneath the given key.
        /// </returns>
        private static RegistryKey GetSubKey(RegistryKey current, string name, bool create)
        {
            if (current.GetSubKeyNames().Contains(name))
            {
                return current.OpenSubKey(name, true);
            }

            if (!create)
            {
                return null;
            }

            RegistryKeyPermissionCheck check = RegistryKeyPermissionCheck.ReadWriteSubTree;
            return current.CreateSubKey(name, check);
        }
        #endregion Methods
    } // RSG.Interop.Microsoft.Windows.RegistryHelper {Class}
} // RSG.Interop.Microsoft.Windows {Namespace}
