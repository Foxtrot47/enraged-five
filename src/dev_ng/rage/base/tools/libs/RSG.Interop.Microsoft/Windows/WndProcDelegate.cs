﻿//---------------------------------------------------------------------------------------------
// <copyright file="WndProcDelegate.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System;

    /// <summary>
    /// An application-defined function that processes messages sent to a window.
    /// </summary>
    /// <param name="window">
    /// A handle to the window.
    /// </param>
    /// <param name="msg">
    /// The message.
    /// </param>
    /// <param name="wordParameter">
    /// First additional message information. The contents of this parameter depend on the
    /// value of the <paramref name="msg"/> parameter.
    /// </param>
    /// <param name="longParameter">
    /// Second additional message information. The contents of this parameter depend on the
    /// value of the <paramref name="msg"/> parameter.
    /// </param>
    /// <returns>
    /// The return value is the result of the message processing and depends on the message
    /// sent.
    /// </returns>
    public delegate IntPtr WndProcDelegate(
    IntPtr window, WindowMessage msg, IntPtr wordParameter, IntPtr longParameter);
} // RSG.Interop.Microsoft.Windows {Namespace}
