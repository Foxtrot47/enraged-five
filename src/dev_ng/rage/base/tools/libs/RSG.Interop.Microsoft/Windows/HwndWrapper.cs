﻿//---------------------------------------------------------------------------------------------
// <copyright file="HwndWrapper.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System;
    using System.Runtime.InteropServices;

    /// <summary>
    /// Provides a managed wrapper over a handle to a native window.
    /// </summary>
    public abstract class HwndWrapper : IDisposable
    {
        #region Fields
        /// <summary>
        /// The private event handler that is used for the <see cref="Disposed"/> event.
        /// </summary>
        private EventHandler _disposed;

        /// <summary>
        /// The private event handler that is used for the <see cref="Disposing"/> event.
        /// </summary>
        private EventHandler _disposing;

        /// <summary>
        /// The private field used by the <see cref="Handle"/> property.
        /// </summary>
        private IntPtr _handle;

        /// <summary>
        /// The private field used for the <see cref="IsDisposed"/> property.
        /// </summary>
        private bool _isDisposed;

        /// <summary>
        /// The private field used for the <see cref="WindowClassAtom"/> property.
        /// </summary>
        private ushort _windowClassAtom;

        /// <summary>
        /// The delegate that gets called to handle the window messages for the wrapped
        /// window handle.
        /// </summary>
        private Delegate _wndProc;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="HwndWrapper"/> class.
        /// </summary>
        protected HwndWrapper()
        {
        }

        /// <summary>
        /// Finalises an instance of the <see cref="HwndWrapper" /> class.
        /// </summary>
        ~HwndWrapper()
        {
            this.Dispose(false);
        }
        #endregion Constructors

        #region Events
        /// <summary>
        /// Occurs when this instance has been disposed by the garage collector or by a user.
        /// </summary>
        public event EventHandler Disposed
        {
            add
            {
                this._disposed = (EventHandler)Delegate.Combine(this._disposed, value);
            }

            remove
            {
                this._disposed = (EventHandler)Delegate.Remove(this._disposed, value);
            }
        }

        /// <summary>
        /// Occurs when this instance is being disposed by the garage collector or by a user.
        /// </summary>
        public event EventHandler Disposing
        {
            add
            {
                this.ThrowIfDisposed();
                this._disposing = (EventHandler)Delegate.Combine(this._disposing, value);
            }

            remove
            {
                this.ThrowIfDisposed();
                this._disposing = (EventHandler)Delegate.Remove(this._disposing, value);
            }
        }
        #endregion Events

        #region Properties
        /// <summary>
        /// Gets the system handle to the window that this class is wrapped around.
        /// </summary>
        public IntPtr Handle
        {
            get
            {
                if (this._handle == IntPtr.Zero)
                {
                    this._handle = this.CreateWindowCore();
                    if (this.IsWindowSubclassed)
                    {
                        this.SubclassWndProc();
                    }
                }

                return this._handle;
            }
        }

        /// <summary>
        /// Gets or sets the width of the window.
        /// </summary>
        public abstract int Height { get; set; }

        /// <summary>
        /// Gets a value indicating whether this instance has been disposed of.
        /// </summary>
        public bool IsDisposed
        {
            get { return this._isDisposed; }
        }

        /// <summary>
        /// Gets or sets the position of the window's left edge, in relation to the desktop.
        /// </summary>
        public abstract int Left { get; set; }

        /// <summary>
        /// Gets or sets the position of the window's top edge, in relation to the desktop.
        /// </summary>
        public abstract int Top { get; set; }

        /// <summary>
        /// Gets or sets the width of the window.
        /// </summary>
        public abstract int Width { get; set; }

        /// <summary>
        /// Gets a value indicating whether the window has been sub classed or not.
        /// </summary>
        protected virtual bool IsWindowSubclassed
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the atom for the wrapped class. A unique identifier for the class that was
        /// registered for the wrapped window.
        /// </summary>
        protected ushort WindowClassAtom
        {
            get
            {
                if (this._windowClassAtom == 0)
                {
                    this._windowClassAtom = this.CreateWindowClassCore();
                }

                return this._windowClassAtom;
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting
        /// unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Creates a unique identifier for the class that was registered for the
        /// wrapped window.
        /// </summary>
        /// <returns>
        /// A atom for the wrapped class.
        /// </returns>
        protected virtual ushort CreateWindowClassCore()
        {
            return this.RegisterClass(Guid.NewGuid().ToString());
        }

        /// <summary>
        /// Creates the core window and returns the handle to the newly created window.
        /// </summary>
        /// <returns>
        /// A handle to the newly created window.
        /// </returns>
        protected abstract IntPtr CreateWindowCore();

        /// <summary>
        /// Destroys the registered class for the wrapped window.
        /// </summary>
        protected virtual void DestroyWindowClassCore()
        {
            if (this._windowClassAtom == 0)
            {
                return;
            }

            IntPtr moduleHandle = Kernel32.GetModuleHandle(null);
            IntPtr classAtom = new IntPtr((int)this._windowClassAtom);
            User32.UnregisterClass(classAtom, moduleHandle);
            this._windowClassAtom = 0;
        }

        /// <summary>
        /// Destroys the core components of the wrapped window.
        /// </summary>
        protected virtual void DestroyWindowCore()
        {
            if (this._handle == IntPtr.Zero)
            {
                return;
            }

            User32.DestroyWindow(this._handle);
            this._handle = IntPtr.Zero;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting
        /// unmanaged resources.
        /// </summary>
        /// <param name="disposeManaged">
        /// If true the managed resources for this instance also get disposed of as well as the
        /// unmanaged resources.
        /// </param>
        protected virtual void Dispose(bool disposeManaged)
        {
            if (!this.IsDisposed)
            {
                try
                {
                    EventHandler handler = this._disposing;
                    if (handler != null)
                    {
                        handler(this, EventArgs.Empty);
                    }

                    this._disposing = null;
                    if (disposeManaged)
                    {
                        this.DisposeManagedResources();
                    }

                    this.DisposeNativeResources();
                }
                finally
                {
                    this._isDisposed = true;
                    EventHandler handler = this._disposed;
                    if (handler != null)
                    {
                        handler(this, EventArgs.Empty);
                    }
                }
            }
        }

        /// <summary>
        /// When overridden disposes of the managed resources.
        /// </summary>
        protected virtual void DisposeManagedResources()
        {
        }

        /// <summary>
        /// Disposes of the wrapped window by calling native methods to destroy and unregister
        /// the window and class.
        /// </summary>
        protected virtual void DisposeNativeResources()
        {
            this.DestroyWindowCore();
            this.DestroyWindowClassCore();
        }

        /// <summary>
        /// Registers a new window class with the specified class name.
        /// </summary>
        /// <param name="className">
        /// The class name that will be used to uniquely register the window.
        /// </param>
        /// <returns>
        /// If the function succeeds, the return value is a class atom that uniquely
        /// identifies the class being registered. If the function fails, the return
        /// value is zero.
        /// </returns>
        protected ushort RegisterClass(string className)
        {
            WNDCLASS wndClass = default(WNDCLASS);
            wndClass.ClassExtraBytes = 0;
            wndClass.WindowExtraBytes = 0;
            wndClass.Background = IntPtr.Zero;
            wndClass.Cursor = IntPtr.Zero;
            wndClass.Icon = IntPtr.Zero;
            wndClass.WndProc = this._wndProc = new WndProcDelegate(this.WndProc);
            wndClass.ClassName = className;
            wndClass.MenuName = null;
            wndClass.Style = ClassStyles.None;
            return User32.RegisterClass(ref wndClass);
        }

        /// <summary>
        /// Throws a System.ObjectDisposedException exception if this instance has been already
        /// disposed of.
        /// </summary>
        protected void ThrowIfDisposed()
        {
            if (this.IsDisposed)
            {
                throw new ObjectDisposedException(this.GetType().Name);
            }
        }

        /// <summary>
        /// Processes the specified message on the specified window handle.
        /// </summary>
        /// <param name="hwnd">
        /// A handle to the window procedure that received the message.
        /// </param>
        /// <param name="msg">
        /// The message.
        /// </param>
        /// <param name="wordParameter">
        /// First additional message information. The content of this parameter depends on
        /// the value of the <paramref name="msg"/> parameter.
        /// </param>
        /// <param name="longParameter">
        /// Second additional message information. The content of this parameter depends on
        /// the value of the <paramref name="msg"/> parameter.
        /// </param>
        /// <returns>
        /// The return value is the result of the message processing and depends on
        /// the message.
        /// </returns>
        protected virtual IntPtr WndProc(
            IntPtr hwnd,
            WindowMessage msg,
            IntPtr wordParameter,
            IntPtr longParameter)
        {
            return User32.DefWindowProc(hwnd, msg, wordParameter, longParameter);
        }

        /// <summary>
        /// Changes the window message process function.
        /// </summary>
        private void SubclassWndProc()
        {
            this._wndProc = new WndProcDelegate(this.WndProc);
            User32.SetWindowLong(
                this._handle,
                WindowLongFlag.WNDPROC,
                Marshal.GetFunctionPointerForDelegate(this._wndProc));
        }
        #endregion Methods
    } // RSG.Interop.Microsoft.Windows.HwndWrapper {Class}
} // RSG.Interop.Microsoft.Windows {Namespace}
