﻿//---------------------------------------------------------------------------------------------
// <copyright file="Kernel32.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System;
    using System.Runtime.InteropServices;
    using System.Security;
    using System.Windows;
    using Com = System.Runtime.InteropServices.ComTypes;

    /// <summary>
    /// Contains native functions that are imported from the unmanaged assembly Kernel32.dll.
    /// </summary>
    public static class Kernel32
    {
        #region Methods
        /// <summary>
        /// Frees the loaded dynamic-link library (DLL) module and, if necessary, decrements its 
        /// reference count.
        /// </summary>
        /// <param name="hModule"></param>
        /// <returns></returns>
        [DllImport("kernel32.dll")]
        public static extern bool FreeLibrary(IntPtr hModule);

        /// <summary>
        /// Locks a global memory object and returns a pointer to the first byte of the
        /// object's memory block.
        /// </summary>
        /// <param name="hMem">
        /// A handle to the global memory object.
        /// </param>
        /// <returns>
        /// If the function succeeds, the return value is a pointer to the first byte of the
        /// memory block; otherwise, IntPtr.Zero.
        /// </returns>
        [DllImport("kernel32.dll")]
        public static extern IntPtr GlobalLock(IntPtr hMem);

        /// <summary>
        /// Decrements the lock count associated with a memory object.
        /// </summary>
        /// <param name="hMem">
        /// A handle to the global memory object.
        /// </param>
        /// <returns>
        /// If the memory object is still locked after decrementing the lock count, the return
        /// value is true; otherwise, false.
        /// </returns>
        [return: MarshalAs(UnmanagedType.Bool)]
        [DllImport("kernel32.dll")]
        public static extern bool GlobalUnlock(IntPtr hMem);

        /// <summary>
        /// Retrieves the current size of the specified global memory object, in bytes.
        /// </summary>
        /// <param name="handle">
        /// A handle to the global memory object.
        /// </param>
        /// <returns>
        /// If the function succeeds, the return value is the size of the specified global
        /// memory object, in bytes; otherwise, zero.
        /// </returns>
        [DllImport("kernel32.dll")]
        public static extern IntPtr GlobalSize(IntPtr handle);

        /// <summary>
        /// Retrieves a module handle for the specified module. The module must have been
        /// loaded by the calling process.
        /// </summary>
        /// <param name="moduleName">
        /// The name of the loaded module.
        /// </param>
        /// <returns>
        /// If the function succeeds, the return value is a handle to the specified
        /// module. If the function fails, the return value is NULL.
        /// </returns>
        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr GetModuleHandle(
            [MarshalAs(UnmanagedType.LPWStr)] string moduleName);
                
        /// <summary>
        /// Loads the specified module into the address space of the calling process. 
        /// The specified module may cause other modules to be loaded.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern IntPtr LoadLibrary(string filename); 

        /// <summary>
        /// Loads the specified module into the address space of the calling process. 
        /// The specified module may cause other modules to be loaded.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="hFile"></param>
        /// <param name="flags"></param>
        /// <returns></returns>
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern IntPtr LoadLibraryEx(string filename, IntPtr hFile, LoadLibraryFlag flags);

        /// <summary>
        /// Frees the specified local memory object and invalidates its handle.
        /// </summary>
        /// <param name="handle">
        /// A handle to the local memory object. This handle is returned by either the
        /// LocalAlloc or LocalReAlloc function. It is not safe to free memory allocated
        /// with GlobalAlloc.
        /// </param>
        /// <returns>
        /// If the function succeeds, the return value is IntPtr.Zero. If the function fails,
        /// the return value is equal to a handle to the local memory object. To get extended
        /// error information, call GetLastError.
        /// </returns>
        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern IntPtr LocalFree(IntPtr handle);

        /// <summary>
        /// Retrieves timing information for the specified process.
        /// </summary>
        /// <param name="hProcess">A handle to the process whose timing information is sought.</param>
        /// <param name="lpCreationTime">A pointer to a FILETIME structure that receives the creation time of the process.</param>
        /// <param name="lpExitTime">A pointer to a FILETIME structure that receives the exit time of the process.</param>
        /// <param name="lpKernelTime">A pointer to a FILETIME structure that receives the amount of time that the process has executed in kernel mode.</param>
        /// <param name="lpUserTime">A pointer to a FILETIME structure that receives the amount of time that the process has executed in user mode.</param>
        /// <returns>
        /// If the function succeeds, the return value is nonzero.
        /// If the function fails, the return value is zero.
        /// </returns>
        [DllImport("kernel32.dll")]
        public static extern bool GetProcessTimes(IntPtr hProcess, out Com.FILETIME lpCreationTime, out Com.FILETIME lpExitTime, out Com.FILETIME lpKernelTime, out Com.FILETIME lpUserTime);

        /// <summary>
        /// Determines the location of a resource with the specified type and name in the specified module.
        /// </summary>
        /// <param name="hModule">
        /// A handle to the module whose portable executable file or an accompanying MUI file contains the resource.
        /// </param>
        /// <param name="lpID">
        /// The name of the resource. 
        /// </param>
        /// <param name="type">
        /// The resource type.
        /// </param>
        /// <returns>
        /// If the function succeeds, the return value is a handle to the data associated with the resource.
        /// If the function fails, the return value is NULL. To get extended error information, call GetLastError.
        /// </returns>
        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern IntPtr FindResource(IntPtr hModule, int lpID, ResourceType type);

        /// <summary>
        /// Retrieves a handle that can be used to obtain a pointer to the first byte of the specified resource in memory.
        /// </summary>
        /// <param name="hModule">
        /// A handle to the module whose executable file contains the resource.
        /// </param>
        /// <param name="hResInfo">
        /// A handle to the resource to be loaded.
        /// </param>
        /// <returns>
        /// If the function succeeds, the return value is a handle to the data associated with the resource.
        /// If the function fails, the return value is NULL. To get extended error information, call GetLastError.
        /// </returns>
        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern IntPtr LoadResource(IntPtr hModule, IntPtr hResInfo);

        /// <summary>
        /// Retrieves a pointer to the specified resource in memory.
        /// </summary>
        /// <param name="hResData">
        /// A handle to the resource to be accessed.
        /// </param>
        /// <returns>
        /// If the loaded resource is available, the return value is a pointer to the first byte of the resource; 
        /// otherwise, it is NULL.
        /// </returns>
        [DllImport("kernel32.dll", SetLastError=true)]
        public static extern IntPtr LockResource(IntPtr hResData);

        /// <summary>
        /// Retrieves the size, in bytes, of the specified resource.
        /// </summary>
        /// <param name="hModule">
        /// A handle to the module whose executable file contains the resource.
        /// </param>
        /// <param name="hResInfo">
        /// A handle to the resource. This handle must be created by using the FindResource or 
        /// FindResourceEx function.
        /// </param>
        /// <returns>
        /// If the function succeeds, the return value is the number of bytes in the resource.
        /// If the function fails, the return value is zero. To get extended error information, call GetLastError.
        /// </returns>
        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern uint SizeofResource(IntPtr hModule, IntPtr hResInfo);

        /// <summary>
        /// Enumerates resources of a specified type within a binary module.
        /// </summary>
        /// <param name="hModule"></param>
        /// <param name="lpszType"></param>
        /// <param name="lpEnumFunc"></param>
        /// <param name="lParam"></param>
        /// <returns></returns>
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern bool EnumResourceNames(
          IntPtr hModule,
          ResourceType lpszType,
          EnumResourceDelegate lpEnumFunc,
          IntPtr lParam);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hModule"></param>
        /// <param name="lpszType"></param>
        /// <param name="lpszName"></param>
        /// <param name="lParam"></param>
        /// <returns></returns>
        public delegate bool EnumResourceDelegate(
          IntPtr hModule,
          IntPtr lpszType,
          IntPtr lpszName,
          IntPtr lParam);

        /// <summary>
        /// Allocates a new console for the calling process.
        /// </summary>
        /// <returns>
        /// If the function succeeds, the return value is nonzero.
        /// If the function fails, the return value is zero. To get extended error information, call GetLastError.
        /// </returns>
        [DllImport("kernel32.dll")]
        public static extern bool AllocConsole();

        /// <summary>
        /// Detaches the calling process from its console.
        /// </summary>
        /// <returns>
        /// If the function succeeds, the return value is nonzero.
        /// If the function fails, the return value is zero. To get extended error information, call GetLastError.
        /// </returns>
        [DllImport("kernel32.dll", SetLastError = true, ExactSpelling = true)]
        public static extern bool FreeConsole();
        #endregion Methods
    } // RSG.Interop.Microsoft.Windows.Shell32 {Class}
} // RSG.Interop.Microsoft.Windows {Namespace}
