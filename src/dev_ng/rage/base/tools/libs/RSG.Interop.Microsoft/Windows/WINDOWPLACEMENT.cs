﻿//---------------------------------------------------------------------------------------------
// <copyright file="WINDOWPLACEMENT.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System.Runtime.InteropServices;

    /// <summary>
    /// Contains information about the placement of a window on the screen.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct WINDOWPLACEMENT
    {
        #region Fields
        /// <summary>
        /// The length of the structure, in bytes.
        /// </summary>
        public uint Length;

        /// <summary>
        /// The flags that control the position of the minimized window and the method by which
        /// the window is restored.
        /// </summary>
        public uint Flags;

        /// <summary>
        /// The current show state of the window.
        /// </summary>
        public WindowShowState ShowCmd;

        /// <summary>
        /// The coordinates of the window's upper-left corner when the window is minimized.
        /// </summary>
        public POINT MinPosition;

        /// <summary>
        /// The coordinates of the window's upper-left corner when the window is maximized.
        /// </summary>
        public POINT MaxPosition;

        /// <summary>
        /// The window's coordinates when the window is in the restored position.
        /// </summary>
        public RECT NormalPosition;
        #endregion Fields

        #region Methods
        /// <summary>
        /// Determines whether the specified objects are equal to each other.
        /// </summary>
        /// <param name="placement1">
        /// The first object to compare.
        /// </param>
        /// <param name="placement2">
        /// The second object to compare.
        /// </param>
        /// <returns>
        /// True if the specified objects are equal to each other; otherwise, false.
        /// </returns>
        public static bool operator ==(WINDOWPLACEMENT placement1, WINDOWPLACEMENT placement2)
        {
            return placement1.Equals(placement2);
        }

        /// <summary>
        /// Determines whether the specified objects are not equal to each other.
        /// </summary>
        /// <param name="placement1">
        /// The first object to compare.
        /// </param>
        /// <param name="placement2">
        /// The second object to compare.
        /// </param>
        /// <returns>
        /// True if the specified objects are not equal to each other; otherwise, false.
        /// </returns>
        public static bool operator !=(WINDOWPLACEMENT placement1, WINDOWPLACEMENT placement2)
        {
            return !placement1.Equals(placement2);
        }

        /// <summary>
        /// Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        /// A hash code for the current object.
        /// </returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">
        /// The object to compare with the current object.
        /// </param>
        /// <returns>
        /// True if the specified object is equal to the current object; otherwise, false.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (!(obj is WINDOWPLACEMENT))
            {
                return false;
            }

            return this.Equals((WINDOWPLACEMENT)obj);
        }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="other">
        /// The object to compare with the current object.
        /// </param>
        /// <returns>
        /// True if the specified object is equal to the current object; otherwise, false.
        /// </returns>
        public bool Equals(WINDOWPLACEMENT other)
        {
            return object.ReferenceEquals(this, other);
        }
        #endregion Methods
    } // RSG.Interop.Microsoft.Windows.WINDOWPLACEMENT {Structure}
} // RSG.Interop.Microsoft.Windows {Namespace}
