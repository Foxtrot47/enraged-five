﻿//---------------------------------------------------------------------------------------------
// <copyright file="Rstrtmgr.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Runtime.InteropServices;
    using Com = System.Runtime.InteropServices.ComTypes;

    /// <summary>
    /// Interop wrapper around Microsoft Windows Restart Manager API.
    /// </summary>
    /// <see cref="RestartManagerSession" />
    public static class Rstrtmgr
    {
        /// <summary>
        /// The application that started the Restart Manager session can pass a pointer to this 
        /// function to the Restart Manager functions to receive a percentage of completeness.
        /// </summary>
        /// <param name="nPercentComplete"></param>
        public delegate void RM_WRITE_STATUS_CALLBACK(UInt32 nPercentComplete);

        /// <summary>
        /// Modifies the shutdown or restart actions that are applied to an application or service. 
        /// The primary installer can call the RmAddFilter function multiple times. The most recent 
        /// call overrides any previous modifications to the same file, process, or service.
        /// </summary>
        /// <param name="pSessionHandle">A handle to an existing Restart Manager session.</param>
        /// <param name="strFilename">Full path to the application's executable file.</param>
        /// <param name="application">RM_UNIQUE_PROCESS structure for the application.</param>
        /// <param name="strShortServiceName">Short service name.</param>
        /// <param name="actionType">Modification to be applied.</param>
        /// <returns>This is the most recent error received.</returns>
        [DllImport("rstrtmgr.dll", CharSet = CharSet.Auto)]
        public static extern int RmAddFilter(IntPtr pSessionHandle, String strFilename, RM_UNIQUE_PROCESS application, String strShortServiceName, RM_FILTER_ACTION actionType);

        /// <summary>
        /// Cancels the current RmShutdown or RmRestart operation. This function must be called 
        /// from the application that has started the session by calling the RmStartSession function.
        /// </summary>
        /// <param name="dwSessionHandle">A handle to an existing session.</param>
        /// <returns>This is the most recent error received.</returns>
        [DllImport("rstrtmgr.dll")]
        public static extern int RmCancelCurrentTask(IntPtr dwSessionHandle);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pSessionHandle"></param>
        /// <returns></returns>
        [DllImport("rstrtmgr.dll")]
        public static extern int RmEndSession(IntPtr pSessionHandle);
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pSessionHandle"></param>
        /// <param name="dwSessionFlags"></param>
        /// <param name="strSessionKey"></param>
        /// <returns></returns>
        [DllImport("rstrtmgr.dll", CharSet = CharSet.Auto)]
        public static extern int RmStartSession(out IntPtr pSessionHandle, int dwSessionFlags, String strSessionKey);
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pSessionHandle"></param>
        /// <param name="nFiles"></param>
        /// <param name="rgsFilenames"></param>
        /// <param name="nApplications"></param>
        /// <param name="rgApplications"></param>
        /// <param name="nServices"></param>
        /// <param name="rgsServiceNames"></param>
        /// <returns></returns>
        [DllImport("rstrtmgr.dll", CharSet = CharSet.Auto)]
        public static extern int RmRegisterResources(IntPtr pSessionHandle, UInt32 nFiles, String[] rgsFilenames, UInt32 nApplications, RM_UNIQUE_PROCESS[] rgApplications, UInt32 nServices, String[] rgsServiceNames);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pSessionHandle"></param>
        /// <param name="lActionFlags"></param>
        /// <param name="fnStatus"></param>
        /// <returns></returns>
        [DllImport("rstrtmgr.dll")]
        public static extern int RmShutdown(IntPtr pSessionHandle, RM_SHUTDOWN_TYPE lActionFlags, RM_WRITE_STATUS_CALLBACK fnStatus);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pSessionHandle"></param>
        /// <param name="dwRestartFlags"></param>
        /// <param name="fnStatus"></param>
        /// <returns></returns>
        [DllImport("rstrtmgr.dll")]
        public static extern int RmRestart(IntPtr pSessionHandle, int dwRestartFlags, RM_WRITE_STATUS_CALLBACK fnStatus);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hProcess"></param>
        /// <param name="lpCreationTime"></param>
        /// <param name="lpExitTime"></param>
        /// <param name="lpKernelTime"></param>
        /// <param name="lpUserTime"></param>
        /// <returns></returns>
        [DllImport("kernel32.dll")]
        public static extern bool GetProcessTimes(IntPtr hProcess, out Com.FILETIME lpCreationTime, out Com.FILETIME lpExitTime, out Com.FILETIME lpKernelTime, out Com.FILETIME lpUserTime);
    }

} // RSG.Interop.Microsoft.Windows namespace
