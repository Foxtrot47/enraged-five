﻿//---------------------------------------------------------------------------------------------
// <copyright file="NotifyIconDataFlags.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------


namespace RSG.Interop.Microsoft.Windows
{
    using System;

    /// <summary>
    /// Flags that either indicate which of the other members of the structure contain valid 
    /// data or provide additional information to the tooltip as to how it should display.
    /// </summary>
    [Flags]
    public enum NotifyIconFlags : uint
    {
        /// <summary>
        /// The uCallbackMessage member is valid.
        /// </summary>
        Message = 0x00000001,

        /// <summary>
        /// The Icon member is valid.
        /// </summary>
        Icon = 0x00000002,

        /// <summary>
        /// The szTip member is valid.
        /// </summary>
        Tip = 0x00000004,

        /// <summary>
        /// The dwState and dwStateMask members are valid.
        /// </summary>
        State = 0x00000008,

        /// <summary>
        /// Display a balloon notification. The szInfo, szInfoTitle, 
        /// dwInfoFlags, and uTimeout members are valid. Note that 
        /// uTimeout is valid only in Windows 2000 and Windows XP.
        /// </summary>
        Info = 0x00000010,

        /// <summary>
        /// The guidItem is valid (Windows 7+).
        /// </summary>
        GUID = 0x00000020,

        /// <summary>
        /// If the balloon notification cannot be displayed immediately, 
        /// discard it. Use this flag for notifications that represent 
        /// real-time information which would be meaningless or misleading 
        /// if displayed at a later time. For example, a message that states 
        /// "Your telephone is ringing." REALTIME is meaningful only 
        /// when combined with the INFO flag.
        /// </summary>
        RealTime = 0x00000040,

        /// <summary>
        /// Use the standard tooltip.
        /// </summary>
        ShowTip = 0x00000080,
    }

} // RSG.Interop.Microsoft.Windows namespace
