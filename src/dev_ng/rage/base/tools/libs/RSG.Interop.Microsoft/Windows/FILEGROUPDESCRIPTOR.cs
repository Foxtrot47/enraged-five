﻿// --------------------------------------------------------------------------------------------
// <copyright file="FILEGROUPDESCRIPTOR.cs" company="Rockstar">
//     Copyright © Rockstar Games 2015. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System.Runtime.InteropServices;

    /// <summary>
    /// Defines a group of file descriptors.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct FILEGROUPDESCRIPTOR
    {
        /// <summary>
        /// The number of elements.
        /// </summary>
        public int Count;
    } // RSG.Interop.Microsoft.Windows.FILEGROUPDESCRIPTOR {Struct}
} // RSG.Interop.Microsoft.Windows {Namespace}
