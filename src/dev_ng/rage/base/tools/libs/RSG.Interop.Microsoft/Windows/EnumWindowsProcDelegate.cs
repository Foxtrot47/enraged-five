﻿//---------------------------------------------------------------------------------------------
// <copyright file="EnumWindowsProcDelegate.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System;

    /// <summary>
    /// An application-defined call-back function that is called by the EnumThreadWindows
    /// function.
    /// </summary>
    /// <param name="window">
    /// A handle to a window.
    /// </param>
    /// <param name="param">
    /// Application-defined data.
    /// </param>
    /// <returns>
    /// To continue enumeration, the call-back function must return true; to stop enumeration,
    /// it must return false.
    /// </returns>
    public delegate bool EnumWindowsProcDelegate(IntPtr window, IntPtr param);
} // RSG.Interop.Microsoft.Windows {Namespace}
