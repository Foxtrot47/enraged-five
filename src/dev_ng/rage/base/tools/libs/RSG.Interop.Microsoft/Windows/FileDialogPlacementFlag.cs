﻿// --------------------------------------------------------------------------------------------
// <copyright file="FileDialogPlacementFlag.cs" company="Rockstar">
//     Copyright © Rockstar Games 2012. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    /// <summary>
    /// Defines the different values for list placement.
    /// </summary>
    public enum FileDialogPlacementFlag
    {
        /// <summary>
        /// The place is added to the bottom of the default list.
        /// </summary>
        BOTTOM = 0x00000000,

        /// <summary>
        /// The place is added to the top of the default list.
        /// </summary>
        TOP = 0x00000001,
    } // RSG.Interop.Microsoft.Windows.FileDialogPlacementFlag {Enum}
} // RSG.Interop.Microsoft.Windows {Namespace}
