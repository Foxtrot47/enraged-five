﻿//---------------------------------------------------------------------------------------------
// <copyright file="RM_SHUTDOWN_TYPE.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System;

    /// <summary>
    /// Configures the shut down of applications.
    /// </summary>
    [Flags]
    public enum RM_SHUTDOWN_TYPE : uint
    {
        /// <summary>
        /// Forces unresponsive applications and services to shut down after the timeout period.
        /// </summary>
        RmForceShutdown = 0x1,
        
        /// <summary>
        /// Shuts down applications if and only if all the applications have been registered for restart using the RegisterApplicationRestart function.
        /// </summary>
        RmShutdownOnlyRegistered = 0x10
    }

} // RSG.Interop.Microsoft.Windows namespace
