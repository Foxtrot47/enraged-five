﻿//---------------------------------------------------------------------------------------------
// <copyright file="RestartManagerSession.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Linq;
    using Com = System.Runtime.InteropServices.ComTypes;

    /// <summary>
    /// Windows Restart Manager API wrapper.
    /// </summary>
    /// http://msdn.microsoft.com/en-us/library/windows/desktop/cc948910(v=vs.85).aspx
    /// <example>
    ///     using (RestartManagerSession rm = new RestartManagerSession())
    ///     {
    ///       rm.RegisterApplication("notepad");
    ///       rm.RegisterFile("c:\\some\\shared\\library.dll");
    ///       rm.Shutdown();
    ///       rm.Restart(); // optional
    ///     }
    /// </example>
    public class RestartManagerSession : IDisposable
    {
        #region Member Data
        /// <summary>
        /// Restart Manager session identifier.
        /// </summary>
        private String m_KeyId;

        /// <summary>
        /// Restart Manager session handle.
        /// </summary>
        private IntPtr m_hHandle;

        /// <summary>
        /// 
        /// </summary>
        private bool m_bResourcesRegistered;

        /// <summary>
        /// Registered application processes.
        /// </summary>
        private List<RM_UNIQUE_PROCESS> m_RegisteredProcesses;

        /// <summary>
        /// Registered files.
        /// </summary>
        private List<String> m_RegisteredFiles;
        
        /// <summary>
        /// Registered services.
        /// </summary>
        private List<String> m_RegisteredServices;
        #endregion // Member Data

        #region Constructor(s) / Destructor
        /// <summary>
        /// Constructor.
        /// </summary>
        public RestartManagerSession()
        {
            this.m_bResourcesRegistered = false;
            this.m_RegisteredProcesses = new List<RM_UNIQUE_PROCESS>();
            this.m_RegisteredFiles = new List<String>();
            this.m_RegisteredServices = new List<String>();
            this.m_KeyId = Guid.NewGuid().ToString();
            int res = Rstrtmgr.RmStartSession(out m_hHandle, 0, m_KeyId);
            if (!Win32Errors.Succeeded(res))
                throw (new Win32Exception(res));
        }

        /// <summary>
        /// Diposing.
        /// </summary>
        public void Dispose()
        {
            int res = Rstrtmgr.RmEndSession(m_hHandle);
            if (!Win32Errors.Succeeded(res))
                throw (new Win32Exception(res));
        }
        #endregion // Constructor(s) / Destructor

        #region Controller Methods
        /// <summary>
        /// Register an application process.
        /// </summary>
        /// <param name="appName"></param>
        public void RegisterApplication(String appName)
        {
            Debug.Assert(!this.m_bResourcesRegistered, "Resources already registered.");
            IEnumerable<RM_UNIQUE_PROCESS> processes = GetProcessesWithName(appName);
            m_RegisteredProcesses.AddRange(processes);
        }

        /// <summary>
        /// Register a file (absolute filename).
        /// </summary>
        /// <param name="filename"></param>
        public void RegisterFile(String filename)
        {
            Debug.Assert(!this.m_bResourcesRegistered, "Resources already registered.");
            Debug.Assert(System.IO.Path.IsPathRooted(filename),
                "Filename is not rooted.");
            if (!System.IO.Path.IsPathRooted(filename))
                throw (new ArgumentException("filename"));

            this.m_RegisteredFiles.Add(filename);
        }

        /// <summary>
        /// Register a service name.
        /// </summary>
        /// <param name="serviceName"></param>
        public void RegisterService(String serviceName)
        {
            Debug.Assert(!this.m_bResourcesRegistered, "Resources already registered.");
            this.m_RegisteredServices.Add(serviceName);
        }

        /// <summary>
        /// Shutdown registered processes.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="cbProgress"></param>
        /// <returns>true iff successful; false otherwise.</returns>
        public bool Shutdown(RM_SHUTDOWN_TYPE type, Rstrtmgr.RM_WRITE_STATUS_CALLBACK cbProgress = null)
        {
            Debug.Assert(!this.m_bResourcesRegistered, "Resources already registered.");
            if (this.m_bResourcesRegistered)
                throw (new NotSupportedException("Resources already registered."));

            // Register resources with Restart Manager API.
            this.m_bResourcesRegistered = true;
            int res = Rstrtmgr.RmRegisterResources(m_hHandle,
                (uint)this.m_RegisteredFiles.Count(), this.m_RegisteredFiles.ToArray(),
                (uint)this.m_RegisteredProcesses.Count(), this.m_RegisteredProcesses.ToArray(),
                (uint)this.m_RegisteredServices.Count(), this.m_RegisteredServices.ToArray());
            if (Win32Errors.Succeeded(res))
            {
                res = Rstrtmgr.RmShutdown(m_hHandle, type, cbProgress);
                return (Win32Errors.Succeeded(res));
            }
            return (false);
        }

        /// <summary>
        /// Restart registered processes (invoke Shutdown first).
        /// </summary>
        /// <param name="cbProgress"></param>
        /// <returns>true iff successful; false otherwise.</returns>
        public bool Restart(Rstrtmgr.RM_WRITE_STATUS_CALLBACK cbProgress = null)
        {
            Debug.Assert(this.m_bResourcesRegistered, "Resources not registered.  Shutdown first!");
            if (!this.m_bResourcesRegistered)
                throw (new NotSupportedException("Resources already registered.  Shutdown first!"));

            int res = Rstrtmgr.RmRestart(m_hHandle, 0, cbProgress);
            return (Win32Errors.Succeeded(res));
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Return RM_UNIQUE_PROCESS structures for a process name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private static IEnumerable<RM_UNIQUE_PROCESS> GetProcessesWithName(String name)
        {
            List<RM_UNIQUE_PROCESS> processes = new List<RM_UNIQUE_PROCESS>();
            foreach (Process p in Process.GetProcessesByName(name))
            {
                RM_UNIQUE_PROCESS rp = new RM_UNIQUE_PROCESS();
                rp.dwProcessId = p.Id;
                Com.FILETIME creationTime, exitTime, kernelTime, userTime;
                Kernel32.GetProcessTimes(p.Handle, out creationTime, out exitTime, out kernelTime, out userTime);
                rp.ProcessStartTime = creationTime;
                processes.Add(rp);
            }
            return (processes);
        }
        #endregion // Private Methods
    }

} // RSG.Interop.Microsoft.Windows namespace
