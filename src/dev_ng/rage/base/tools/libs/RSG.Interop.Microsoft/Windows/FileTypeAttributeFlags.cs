﻿//---------------------------------------------------------------------------------------------
// <copyright file="FileTypeAttributeFlags.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System;

    /// <summary>
    /// Indicates FILETYPEATTRIBUTEFLAGS constants that are used in the EditFlags value of a file association PROGID registry key.
    /// </summary>
    /// http://msdn.microsoft.com/en-us/library/windows/desktop/bb762506(v=vs.85).aspx
    [Flags]
    public enum FileTypeAttributeFlags
    {
        /// <summary>
        /// No options set.
        /// </summary>
        FTA_None = 0x00000000,

        /// <summary>
        /// Excludes the file type.
        /// </summary>
        FTA_Exclude = 0x00000001,

        /// <summary>
        /// Shows file types, such as folders, that are not associated with a file name extension.
        /// </summary>
        FTA_Show = 0x00000002,

        /// <summary>
        /// Indicates that the file type has a file name extension.
        /// </summary>
        FTA_HasExtension = 0x00000004,

        /// <summary>
        /// Prohibits editing of the registry entries associated with this file type, the addition of new entries, and the deletion or modification of existing entries.
        /// </summary>
        FTA_NoEdit = 0x00000008,

        /// <summary>
        /// Prohibits deletion of the registry entries associated with this file type.
        /// </summary>
        FTA_NoRemove = 0x00000010,

        /// <summary>
        /// Prohibits the addition of new verbs to the file type.
        /// </summary>
        FTA_NoNewVerb = 0x00000020,

        /// <summary>
        /// Prohibits the modification or deletion of canonical verbs such as open and print.
        /// </summary>
        FTA_NoEditVerb = 0x00000040,

        /// <summary>
        /// Prohibits the deletion of canonical verbs such as open and print.
        /// </summary>
        FTA_NoRemoveVerb = 0x00000080,

        /// <summary>
        /// Prohibits the modification or deletion of the description of the file type.
        /// </summary>
        FTA_NoEditDesc = 0x00000100,

        /// <summary>
        /// Prohibits the modification or deletion of the icon assigned to the file type.
        /// </summary>
        FTA_NoEditIcon = 0x00000200,

        /// <summary>
        /// Prohibits the modification of the default verb.
        /// </summary>
        FTA_NoEditDflt = 0x00000400,

        /// <summary>
        /// Prohibits the modification of the commands associated with verbs.
        /// </summary>
        FTA_NoEditVerbCmd = 0x00000800,

        /// <summary>
        /// Prohibits the modification or deletion of verbs.
        /// </summary>
        FTA_NoeditVerbExe = 0x00001000,

        /// <summary>
        /// Prohibits the modification or deletion of the entries related to DDE.
        /// </summary>
        FTA_NoDDE = 0x00002000,

        /// <summary>
        /// Prohibits the modification or deletion of the content type and default extension entries.
        /// </summary>
        FTA_NoEditMIME = 0x00008000,

        /// <summary>
        /// Indicates that the file type's open verb can be safely invoked for downloaded files.
        /// </summary>
        FTA_OpenIsSafe = 0x00010000,

        /// <summary>
        /// Prevents the Never ask me check box from being enabled.
        /// </summary>
        FTA_AlwaysUnsafe = 0x00020000,

        /// <summary>
        /// Prohibits the addition of members of this file type to the Recent Documents folder.
        /// </summary>
        FTA_NoRecentDocs = 0x00100000,

        /// <summary>
        /// Introduced in Windows 8. Marks the file as safe to be passed from a low trust application to a full trust application.
        /// </summary>
        FTA_SafeForElevation = 0x00200000,

        /// <summary>
        /// Introduced in Windows 8. Ensures that the verbs for the file type are invoked with a URI instead of a downloaded version of the file.
        /// </summary>
        FTA_AlwaysUseDirectInvoke = 0x00400000,
    }

} // RSG.Interop.Microsoft.Windows namespace
