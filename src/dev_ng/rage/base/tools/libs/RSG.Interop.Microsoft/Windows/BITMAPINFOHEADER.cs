﻿//---------------------------------------------------------------------------------------------
// <copyright file="BITMAPINFOHEADER.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System.Runtime.InteropServices;

    /// <summary>
    /// Contains information about the dimensions and colour format of a device independent
    /// bitmap.
    /// </summary>
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
    public struct BITMAPINFOHEADER
    {
        #region Fields
        /// <summary>
        /// The number of bytes required by the structure.
        /// </summary>
        public uint Size;

        /// <summary>
        /// The width of the bitmap, in pixels.
        /// </summary>
        public int Width;

        /// <summary>
        /// The height of the bitmap, in pixels.
        /// </summary>
        public int Height;

        /// <summary>
        /// The number of planes for the target device.
        /// </summary>
        public ushort Planes;

        /// <summary>
        /// The number of bits-per-pixel.
        /// </summary>
        public ushort BitCount;

        /// <summary>
        /// The type of compression for a compressed bottom-up bitmap.
        /// </summary>
        public uint Compression;

        /// <summary>
        /// The size, in bytes, of the image.
        /// </summary>
        public uint SizeImage;

        /// <summary>
        /// The horizontal resolution, in pixels-per-meter, of the target device for
        /// the bitmap.
        /// </summary>
        public int XPixelsPerMeter;

        /// <summary>
        /// The vertical resolution, in pixels-per-meter, of the target device for the bitmap.
        /// </summary>
        public int YPixelsPerMeter;

        /// <summary>
        /// The number of colour indexes in the colour table that are actually used by
        /// the bitmap.
        /// </summary>
        public uint ColourUsed;

        /// <summary>
        /// The number of colour indexes that are required for displaying the bitmap. If this
        /// value is zero, all colours are required.
        /// </summary>
        public uint ColourImportant;
        #endregion Fields

        #region Methods
        /// <summary>
        /// Determines whether the specified objects are equal to each other.
        /// </summary>
        /// <param name="header1">
        /// The first object to compare.
        /// </param>
        /// <param name="header2">
        /// The second object to compare.
        /// </param>
        /// <returns>
        /// True if the specified objects are equal to each other; otherwise, false.
        /// </returns>
        public static bool operator ==(BITMAPINFOHEADER header1, BITMAPINFOHEADER header2)
        {
            return header1.Equals(header2);
        }

        /// <summary>
        /// Determines whether the specified objects are not equal to each other.
        /// </summary>
        /// <param name="header1">
        /// The first object to compare.
        /// </param>
        /// <param name="header2">
        /// The second object to compare.
        /// </param>
        /// <returns>
        /// True if the specified objects are not equal to each other; otherwise, false.
        /// </returns>
        public static bool operator !=(BITMAPINFOHEADER header1, BITMAPINFOHEADER header2)
        {
            return !header1.Equals(header2);
        }

        /// <summary>
        /// Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        /// A hash code for the current object.
        /// </returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">
        /// The object to compare with the current object.
        /// </param>
        /// <returns>
        /// True if the specified object is equal to the current object; otherwise, false.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (!(obj is BITMAPINFOHEADER))
            {
                return false;
            }

            return this.Equals((BITMAPINFOHEADER)obj);
        }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="other">
        /// The object to compare with the current object.
        /// </param>
        /// <returns>
        /// True if the specified object is equal to the current object; otherwise, false.
        /// </returns>
        public bool Equals(BITMAPINFOHEADER other)
        {
            return object.ReferenceEquals(this, other);
        }
        #endregion Methods
    } // RSG.Interop.Microsoft.Windows.BITMAPINFOHEADER {Structure}
} // RSG.Interop.Microsoft.Windows {Namespace}
