﻿//---------------------------------------------------------------------------------------------
// <copyright file="ShellItemAttributesCombineFlag.cs" company="Rockstar">
//     Copyright © Rockstar Games 2012. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    /// <summary>
    /// Defines the different ways attributes of an array of shell items can be handled.
    /// </summary>
    public enum ShellItemAttributesCombineFlag
    {
        /// <summary>
        /// If there are multiple items in the array, use a bitwise AND to combine the
        /// attributes across items.
        /// </summary>
        AND = 0x00000001,

        /// <summary>
        /// If there are multiple items in the array, use a bitwise OR to combine the
        /// attributes across items.
        /// </summary>
        OR = 0x00000002,

        /// <summary>
        /// Retrieve the attributes directly from the Shell data source.
        /// </summary>
        APPCOMPAT = 0x00000003,

        /// <summary>
        /// A mask for AND, OR, and APPCOMPAT.
        /// </summary>
        MASK = 0x00000003,

        /// <summary>
        /// Examine all items in the array to compute the attributes.
        /// </summary>
        ALLITEMS = 0x00004000,
    } // RSG.Interop.Microsoft.Windows.ShellItemAttributesCombineFlag {Enum}
} // RSG.Interop.Microsoft.Windows {Namespace}
