﻿using System;

namespace RSG.Interop.Microsoft.Windows
{

    /// <summary>
    /// SendMessageTimeout flags enumeration.
    /// </summary>
    /// http://msdn.microsoft.com/en-us/library/windows/desktop/ms644952(v=vs.85).aspx
    [Flags]
    public enum SendMessageTimeoutFlags : uint
    {
        /// <summary>
        /// The calling thread is not prevented from processing other requests while waiting for the function to return.
        /// </summary>
        Normal = 0x00,
        
        /// <summary>
        /// Prevents the calling thread from processing any other requests until the function returns.
        /// </summary>
        Block = 0x01,
        
        /// <summary>
        /// The function returns without waiting for the time-out period to elapse if the receiving thread appears to not respond or "hangs."
        /// </summary>
        AbortIfHung = 0x02,
        
        /// <summary>
        /// The function does not enforce the time-out period as long as the receiving thread is processing messages.
        /// </summary>
        NoTimeoutIfNotHung = 0x08,

        /// <summary>
        /// The function should return 0 if the receiving window is destroyed or its owning thread dies while the message is being processed.
        /// </summary>
        ErrorOnExit = 0x20
    }

} // RSG.Interop.Microsoft.Windows namespace
