﻿// --------------------------------------------------------------------------------------------
// <copyright file="INativeFileOpenDialog.cs" company="Rockstar">
//     Copyright © Rockstar Games 2012. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System.Runtime.InteropServices;

    /// <summary>
    /// When implemented represents a native file open dialog window.
    /// </summary>
    [ComImport,
    Guid(InterfaceIDGuid.FileOpenDialogId),
    CoClass(typeof(FileOpenDialogRCW))]
    public interface INativeFileOpenDialog : IFileOpenDialog
    {
    } // RSG.Interop.Microsoft.Windows.INativeFileOpenDialog {Interface}
} // RSG.Interop.Microsoft.Windows {Namespace}
