﻿//---------------------------------------------------------------------------------------------
// <copyright file="MINMAXINFO.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System.Runtime.InteropServices;

    /// <summary>
    /// Contains information about a window's maximized size and position and its minimum and
    /// maximum tracking size.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct MINMAXINFO
    {
        #region Fields
        /// <summary>
        /// Reserved; do not use.
        /// </summary>
        public POINT Reserved;

        /// <summary>
        /// The maximized width (x member) and the maximized height (y member) of the window.
        /// For top-level windows, this value is based on the width of the primary monitor.
        /// </summary>
        public POINT MaxSize;

        /// <summary>
        /// The position of the left side of the maximized window (x member) and the position
        /// of the top of the maximized window (y member). For top-level windows, this value is
        /// based on the position of the primary monitor.
        /// </summary>
        public POINT MaxPosition;

        /// <summary>
        /// The minimum tracking width (x member) and the minimum tracking height (y member) of
        /// the window.
        /// </summary>
        public POINT MinTrackSize;

        /// <summary>
        /// The maximum tracking width (x member) and the maximum tracking height (y member) of
        /// the window.
        /// </summary>
        public POINT MaxTrackSize;
        #endregion Fields
    } // RSG.Interop.Microsoft.Windows.MINMAXINFO {Structure}
} // RSG.Interop.Microsoft.Windows {Namespace}
