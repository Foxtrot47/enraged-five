﻿// --------------------------------------------------------------------------------------------
// <copyright file="ClassIDGuid.cs" company="Rockstar">
//     Copyright © Rockstar Games 2012. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    /// <summary>
    /// Contains the global unique identifiers used to import native runtime class wrappers
    /// through COM.
    /// </summary>
    internal class ClassIDGuid
    {
        #region Fields
        /// <summary>
        /// The global unique identifier for the FileOpenDialogRCW class.
        /// </summary>
        internal const string FileOpenDialog = "DC1C5A9C-E88A-4dde-A5A1-60F82A20AEF7";
        #endregion
    } // RSG.Interop.Microsoft.Windows.ClassIDGuid {Class}
} // RSG.Interop.Microsoft.Windows {Namespace}
