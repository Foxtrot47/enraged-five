﻿//---------------------------------------------------------------------------------------------
// <copyright file="GetMonitorFlag.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    /// <summary>
    /// Defines the different fall-back values for the get monitor native methods.
    /// </summary>
    public enum GetMonitorFlag : uint
    {
        /// <summary>
        /// Returns a handle to the display monitor that is nearest to the point.
        /// </summary>
        DEFAULTTONULL = 0x00000000,

        /// <summary>
        /// Returns a handle to the primary display monitor.
        /// </summary>
        DEFAULTTOPRIMARY = 0x00000001,

        /// <summary>
        /// Returns IntPtr.Zero.
        /// </summary>
        DEFAULTTONEAREST = 0x00000002,
    } // RSG.Interop.Microsoft.Windows.GetMonitorFlag {Enum}
} // RSG.Interop.Microsoft.Windows {Namespace}
