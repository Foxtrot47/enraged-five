﻿//---------------------------------------------------------------------------------------------
// <copyright file="Ole32.cs" company="Rockstar">
//     Copyright © Rockstar Games 2015. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System;
    using System.Runtime.InteropServices;
    using System.Runtime.InteropServices.ComTypes;

    /// <summary>
    /// Contains native functions that are imported from the unmanaged assembly Ole32.dll.
    /// </summary>
    public static class Ole32
    {
        #region Methods
        /// <summary>
        /// Creates a stream object that uses an memory handle to store the stream contents.
        /// </summary>
        /// <returns>
        /// The newly created stream object if successful; otherwise, null.
        /// </returns>
        public static IStream CreateStream()
        {
            IStream stream = null;
            if (CreateStreamOnHGlobal(IntPtr.Zero, true, out stream) == (int)HRESULT.S_OK)
            {
                return stream;
            }

            return null;
        }

        /// <summary>
        /// Carries out an OLE drag and drop operation.
        /// </summary>
        /// <param name="dataObject">
        /// Reference to the data object that contains the data being dragged.
        /// </param>
        /// <param name="dropSource">
        /// Reference to the drop source object used to communicate with the source during the
        /// drag operation.
        /// </param>
        /// <param name="effects">
        /// Effects the source allows in the OLE drag-and-drop operation.
        /// </param>
        /// <param name="effect">
        /// A value indicating how the OLE drag-and-drop operation affected the source data.
        /// </param>
        [DllImport("ole32.dll", CharSet = CharSet.Auto, ExactSpelling = true, PreserveSig = false)]
        public static extern void DoDragDrop(
            System.Runtime.InteropServices.ComTypes.IDataObject dataObject,
            IDropSource dropSource,
            int effects,
            int[] effect);

        /// <summary>
        /// Creates a stream object that uses an memory handle to store the stream contents.
        /// The returned stream object supports both reading and writing, is not transacted,
        /// and does not support region locking.
        /// </summary>
        /// <param name="handle">
        /// A memory handle.
        /// </param>
        /// <param name="deleteOnRelease">
        /// A value that indicates whether the underlying handle for this stream object should
        /// be automatically freed when the stream object is released.
        /// </param>
        /// <param name="stream">
        /// When this method returns contains the created stream object.
        /// </param>
        /// <returns>
        /// Zero if successful; otherwise, non-zero.
        /// </returns>
        [DllImport("ole32.dll")]
        private static extern int CreateStreamOnHGlobal(
            IntPtr handle, bool deleteOnRelease, out IStream stream);
        #endregion Methods
    } // RSG.Interop.Microsoft.Windows.Ole32 {Class}
} // RSG.Interop.Microsoft.Windows {Namespace}
