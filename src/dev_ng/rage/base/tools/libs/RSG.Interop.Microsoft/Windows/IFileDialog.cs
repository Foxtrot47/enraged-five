﻿// --------------------------------------------------------------------------------------------
// <copyright file="IFileDialog.cs" company="Rockstar">
//     Copyright © Rockstar Games 2012. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    /// <summary>
    /// Exposes methods that initialize, show, and get results from the common file dialog.
    /// </summary>
    [ComImport,
    Guid(InterfaceIDGuid.FileDialogId),
    InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IFileDialog : IModalWindow
    {
        #region Methods
        /// <summary>
        /// Launches the modal window.
        /// </summary>
        /// <param name="parent">
        /// The handle of the owner window. This value can be NULL.
        /// </param>
        /// <returns>
        /// If the method succeeds, it returns S_OK. Otherwise, it returns an HRESULT error
        /// code including ERROR_CANCELLED.
        /// </returns>
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime),
        PreserveSig]
        new int Show([In] IntPtr parent);

        /// <summary>
        /// Sets the file types that the dialog can open or save.
        /// </summary>
        /// <param name="fileTypes">
        /// The number of elements in the array specified by <paramref name="filterSpec"/>.
        /// </param>
        /// <param name="filterSpec">
        /// An array of <see cref="FILTERSPEC"/> structures, each
        /// representing a file type.
        /// </param>
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void SetFileTypes([In] uint fileTypes, [In] ref FILTERSPEC filterSpec);

        /// <summary>
        /// Sets the file type that appears as selected in the dialog.
        /// </summary>
        /// <param name="fileType">
        /// The index of the file type in the file type array passed to the
        /// <see cref="SetFileTypes"/> method in its cFileTypes parameter. Note that this is a
        /// one-based index, not zero-based.
        /// </param>
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void SetFileTypeIndex([In] uint fileType);

        /// <summary>
        /// Gets the currently selected file type.
        /// </summary>
        /// <param name="fileType">
        /// A unsigned integer value that receives the index of the selected file type in the
        /// file type array passed to the <see cref="SetFileTypes"/> method in its cFileTypes
        /// parameter. Note that this is a one-based index rather than zero-based.
        /// </param>
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void GetFileTypeIndex(out uint fileType);

        /// <summary>
        /// Assigns an event handler that listens for events coming from the dialog.
        /// </summary>
        /// <param name="events">
        /// A pointer to an <see cref="IFileDialogEvents"/>
        /// implementation that will receive events from the dialog.
        /// </param>
        /// <param name="cookie">
        /// A unsigned integer that receives a value identifying this event handler.
        /// </param>
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void Advise(
            [In, MarshalAs(UnmanagedType.Interface)] IFileDialogEvents events,
            out uint cookie);

        /// <summary>
        /// Removes an event handler that was attached through the <see cref="Advise"/> method.
        /// </summary>
        /// <param name="cookie">
        /// The unsigned integer value that represents the event handler.
        /// </param>
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void Unadvise([In] uint cookie);

        /// <summary>
        /// Sets flags to control the behaviour of the dialog.
        /// </summary>
        /// <param name="fos">
        /// One or more of the flags defined in <see cref="FileOpenDialogOptionFlags"/>.
        /// </param>
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void SetOptions([In] FileOpenDialogOptionFlags fos);

        /// <summary>
        /// Gets the current flags that are set to control dialog behaviour.
        /// </summary>
        /// <param name="fos">
        /// A value that receives one or more of the flags defined in
        /// <see cref="FileOpenDialogOptionFlags"/>.
        /// </param>
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void GetOptions(out FileOpenDialogOptionFlags fos);

        /// <summary>
        /// Sets the folder used as a default if there is not a recently used folder value
        /// available.
        /// </summary>
        /// <param name="shellItem">
        /// The folder that should be used for the default.
        /// </param>
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void SetDefaultFolder([In, MarshalAs(UnmanagedType.Interface)] IShellItem shellItem);

        /// <summary>
        /// Sets a folder that is always selected when the dialog is opened, regardless of
        /// previous user action.
        /// </summary>
        /// <param name="shellItem">
        /// The folder that should be set.
        /// </param>
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void SetFolder([In, MarshalAs(UnmanagedType.Interface)] IShellItem shellItem);

        /// <summary>
        /// Gets either the folder currently selected in the dialog, or, if the dialog is not
        /// currently displayed, the folder that is to be selected when the dialog is opened.
        /// </summary>
        /// <param name="shellItem">
        /// When this method returns contains the shell item interface that is the folder
        /// currently displayed.
        /// </param>
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void GetFolder([MarshalAs(UnmanagedType.Interface)] out IShellItem shellItem);

        /// <summary>
        /// Gets the user's current selection in the dialog.
        /// </summary>
        /// <param name="shellItem">
        /// When this method returns contains the shell item interface that represents the
        /// current selection.
        /// </param>
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void GetCurrentSelection(
            [MarshalAs(UnmanagedType.Interface)] out IShellItem shellItem);

        /// <summary>
        /// Sets the file name.
        /// </summary>
        /// <param name="name">
        /// The text that should be entered into the dialog's File name edit box.
        /// </param>
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void SetFileName([In, MarshalAs(UnmanagedType.LPWStr)] string name);

        /// <summary>
        /// Retrieves the text currently entered in the dialog's File name edit box.
        /// </summary>
        /// <param name="name">
        /// When this method returns contains the text currently entered in the dialog's File
        /// name edit box.
        /// </param>
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void GetFileName([MarshalAs(UnmanagedType.LPWStr)] out string name);

        /// <summary>
        /// Sets the title of the dialog.
        /// </summary>
        /// <param name="title">
        /// The value to set the title to.
        /// </param>
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void SetTitle([In, MarshalAs(UnmanagedType.LPWStr)] string title);

        /// <summary>
        /// Sets the text of the Open or Save button.
        /// </summary>
        /// <param name="text">
        /// The value to set the ok button label to.
        /// </param>
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void SetOkButtonLabel([In, MarshalAs(UnmanagedType.LPWStr)] string text);

        /// <summary>
        /// Sets the text of the label next to the file name edit box.
        /// </summary>
        /// <param name="label">
        /// The value to set the label to that is next to the file name edit box.
        /// </param>
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void SetFileNameLabel([In, MarshalAs(UnmanagedType.LPWStr)] string label);

        /// <summary>
        /// Gets the choice that the user made in the dialog.
        /// </summary>
        /// <param name="shellItem">
        /// When this method returns contains the shell item that represents the choice made
        /// by the user.
        /// </param>
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void GetResult([MarshalAs(UnmanagedType.Interface)] out IShellItem shellItem);

        /// <summary>
        /// Adds a folder to the list of places available for the user to open or save items.
        /// </summary>
        /// <param name="shellItem">
        /// The shell item that represents the folder to be made available to the user.
        /// </param>
        /// <param name="placement">
        /// Specifies where the folder is placed within the list.
        /// </param>
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void AddPlace(
            [In, MarshalAs(UnmanagedType.Interface)] IShellItem shellItem,
            FileDialogPlacementFlag placement);

        /// <summary>
        /// Gets the choice that the user made in the dialog.
        /// </summary>
        /// <param name="extension">
        /// The extension to be made the default. Note this string should not include a leading
        /// period.
        /// </param>
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void SetDefaultExtension([In, MarshalAs(UnmanagedType.LPWStr)] string extension);

        /// <summary>
        /// Closes the dialog.
        /// </summary>
        /// <param name="code">
        /// The code that will be returned by Show to indicate that the dialog was closed
        /// before a selection was made.
        /// </param>
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void Close([MarshalAs(UnmanagedType.Error)] int code);

        /// <summary>
        /// Enables a calling application to associate a GUID with a dialog's persisted state.
        /// </summary>
        /// <param name="guid">
        /// The GUID to associate with this dialog state.
        /// </param>
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void SetClientGuid([In] ref Guid guid);

        /// <summary>
        /// Instructs the dialog to clear all persisted state information.
        /// </summary>
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void ClearClientData();

        /// <summary>
        /// Deprecated in Windows 7. Sets the filter.
        /// </summary>
        /// <param name="filter">
        /// A pointer to a IShellItemFilter object that is to be set.
        /// </param>
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void SetFilter([MarshalAs(UnmanagedType.Interface)] IntPtr filter);
        #endregion
    } // RSG.Interop.Microsoft.Windows.IFileDialog {Interface}
} // RSG.Interop.Microsoft.Windows {Namespace}
