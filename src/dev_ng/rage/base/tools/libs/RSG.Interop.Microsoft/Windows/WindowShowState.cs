﻿//---------------------------------------------------------------------------------------------
// <copyright file="WindowShowState.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System;

    /// <summary>
    /// Defines the different window states.
    /// </summary>
    public enum WindowShowState : uint
    {
        /// <summary>
        /// Hides the window and activates another window.
        /// </summary>
        HIDE = 0,

        /// <summary>
        /// Maximizes the specified window.
        /// </summary>
        MAXIMIZE = 3,

        /// <summary>
        /// Minimizes the specified window and activates the next top-level window in
        /// the z-order.
        /// </summary>
        MINIMIZE = 6,

        /// <summary>
        /// Activates and displays the window.
        /// </summary>
        RESTORE = 9,

        /// <summary>
        /// Activates the window and displays it in its current size and position.
        /// </summary>
        SHOW = 5,

        /// <summary>
        /// Activates the window and displays it as a maximized window.
        /// </summary>
        SHOWMAXIMIZED = 3,

        /// <summary>
        /// Activates the window and displays it as a minimized window.
        /// </summary>
        SHOWMINIMIZED = 2,

        /// <summary>
        /// Displays the window as a minimized window.
        /// </summary>
        SHOWMINNOACTIVE = 7,

        /// <summary>
        /// Displays the window in its current size and position.
        /// </summary>
        SHOWNA = 8,

        /// <summary>
        /// Displays a window in its most recent size and position.
        /// </summary>
        SHOWNOACTIVATE = 4,

        /// <summary>
        /// Activates and displays a window. If the window is minimized or maximized, the
        /// system restores it to its original size and position.
        /// </summary>
        SHOWNORMAL = 1,
    } // RSG.Interop.Microsoft.Windows.WindowShowState {Enum}
} // RSG.Interop.Microsoft.Windows {Namespace}
