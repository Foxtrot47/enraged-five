﻿//---------------------------------------------------------------------------------------------
// <copyright file="WindowHandle.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System;

    /// <summary>
    /// Window handle constants.
    /// </summary>
    public enum WindowHandle : uint
    {
        /// <summary>
        /// For broadcasting messages to all windows.
        /// </summary>
        BROADCAST = 0xFFFF
    } // RSG.Interop.Microsoft.Windows.WindowHandle {Enum}
} // RSG.Interop.Microsoft.Windows {Namespace}
