﻿//---------------------------------------------------------------------------------------------
// <copyright file="NotifyIconMessage.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System;

    /// <summary>
    /// Messages 
    /// </summary>
    public enum NotifyIconMessage
    {
        /// <summary>
        /// Adds an icon to the status area.
        /// </summary>
        Add = 0x00000000,

        /// <summary>
        /// Modifies an icon in the status area.
        /// </summary>
        Modify = 0x00000001,

        /// <summary>
        /// Deletes an icon from the status area.
        /// </summary>
        Delete = 0x00000002,

        /// <summary>
        /// Returns focus to the taskbar notification area.
        /// </summary>
        SetFocus = 0x00000003,

        /// <summary>
        /// Instructs the notification area to behave according to the version number.
        /// </summary>
        SetVersion = 0x00000004,
    }

} // RSG.Interop.Microsoft.Windows namespace
