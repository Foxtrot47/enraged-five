﻿//---------------------------------------------------------------------------------------------
// <copyright file="Gdi32.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System;
    using System.Runtime.InteropServices;
    using System.Security;
    using System.Windows;

    /// <summary>
    /// Contains native functions that are imported from the unmanaged assembly Gdi32.dll.
    /// </summary>
    public static class Gdi32
    {
        #region Methods
        /// <summary>
        /// This function displays bitmaps that have transparent or semi-transparent pixels in
        /// them.
        /// </summary>
        /// <param name="destination">
        /// A handle to the destination device context.
        /// </param>
        /// <param name="destinationXOrigin">
        /// The x-coordinate, in logical units, of the upper-left corner of the
        /// destination rectangle.
        /// </param>
        /// <param name="destinationYOrigin">
        /// The y-coordinate, in logical units, of the upper-left corner of the
        /// destination rectangle.
        /// </param>
        /// <param name="destinationWidth">
        /// The width, in logical units, of the destination rectangle.
        /// </param>
        /// <param name="destinationHeight">
        /// The height, in logical units, of the destination rectangle.
        /// </param>
        /// <param name="source">
        /// A handle to the source device context.
        /// </param>
        /// <param name="sourceXOrigin">
        /// The x-coordinate, in logical units, of the upper-left corner of the
        /// source rectangle.
        /// </param>
        /// <param name="sourceYOrigin">
        /// The y-coordinate, in logical units, of the upper-left corner of the
        /// source rectangle.
        /// </param>
        /// <param name="sourceWidth">
        /// The width, in logical units, of the source rectangle.
        /// </param>
        /// <param name="sourceHeight">
        /// The height, in logical units, of the source rectangle.
        /// </param>
        /// <param name="blendFunction">
        /// The alpha-blending function for source and destination bitmaps, a global alpha
        /// value to be applied to the entire source bitmap, and format information for the
        /// source bitmap.
        /// </param>
        /// <returns>
        /// True if the function is successful; otherwise, false.
        /// </returns>
        [DllImport("gdi32.dll", EntryPoint = "GdiAlphaBlend")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool AlphaBlend(
            IntPtr destination,
            int destinationXOrigin,
            int destinationYOrigin,
            int destinationWidth,
            int destinationHeight,
            IntPtr source,
            int sourceXOrigin,
            int sourceYOrigin,
            int sourceWidth,
            int sourceHeight,
            BLENDFUNCTION blendFunction);

        /// <summary>
        /// Performs a bit-block transfer of the color data corresponding to a rectangle of
        /// pixels from the specified source device context into a destination device context.
        /// </summary>
        /// <param name="hdc">
        /// Handle to the destination device context.
        /// </param>
        /// <param name="nXDest">
        /// The leftmost x-coordinate of the destination rectangle (in pixels).
        /// </param>
        /// <param name="nYDest">
        /// The topmost y-coordinate of the destination rectangle (in pixels).
        /// </param>
        /// <param name="nWidth">
        /// The width of the source and destination rectangles (in pixels).
        /// </param>
        /// <param name="nHeight">
        /// The height of the source and the destination rectangles (in pixels).
        /// </param>
        /// <param name="hdcSrc">
        /// Handle to the source device context.
        /// </param>
        /// <param name="nXSrc">
        /// The leftmost x-coordinate of the source rectangle (in pixels).
        /// </param>
        /// <param name="nYSrc">
        /// The topmost y-coordinate of the source rectangle (in pixels).
        /// </param>
        /// <param name="dwRop">
        /// A raster-operation code.
        /// </param>
        /// <returns>
        /// <c>true</c> if the operation succeedes, <c>false</c> otherwise. To get extended
        /// error information, call
        /// <see cref="System.Runtime.InteropServices.Marshal.GetLastWin32Error"/>.
        /// </returns>
        [DllImport("gdi32.dll", EntryPoint = "BitBlt", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool BitBlt(
            [In] IntPtr hdc,
            int nXDest,
            int nYDest,
            int nWidth,
            int nHeight,
            [In] IntPtr hdcSrc,
            int nXSrc,
            int nYSrc,
            TernaryRasterOperations dwRop);

        /// <summary>
        /// Creates a bitmap compatible with the device that is associated with the specified
        /// device context.
        /// </summary>
        /// <param name="hdc">
        /// A handle to a device context.
        /// </param>
        /// <param name="nWidth">
        /// The bitmap width, in pixels.
        /// </param>
        /// <param name="nHeight">
        /// The bitmap height, in pixels.
        /// </param>
        /// <returns>
        /// If the function succeeds, the return value is a handle to the compatible bitmap
        /// (DDB). If the function fails, the return value is
        /// <see cref="System.IntPtr.Zero"/>.
        /// </returns>
        [DllImport("gdi32.dll", EntryPoint = "CreateCompatibleBitmap")]
        public static extern IntPtr CreateCompatibleBitmap([In] IntPtr hdc, int nWidth, int nHeight);

        /// <summary>
        /// Creates a memory device context compatible with the specified device.
        /// </summary>
        /// <param name="handle">
        /// A handle to an existing device context. If this handle is IntPtr.Zero, the function
        /// creates a memory DC compatible with the application's current screen.
        /// </param>
        /// <returns>
        /// If the function succeeds, the return value is the handle to a memory device
        /// context. If the function fails, the return value is IntPtr.Zero.
        /// </returns>
        [DllImport("gdi32.dll", SetLastError = true)]
        public static extern IntPtr CreateCompatibleDC(IntPtr handle);

        /// <summary>
        /// The CreateDIBSection function creates a DIB that applications can write to
        /// directly. The function gives you a pointer to the location of the bitmap bit
        /// values. You can supply a handle to a file-mapping object that the function will use
        /// to create the bitmap, or you can let the system allocate the memory for the bitmap.
        /// </summary>
        /// <param name="handle">
        /// A handle to a device context.
        /// </param>
        /// <param name="bitmap">
        /// A pointer to a BITMAPINFO structure that specifies various attributes of the DIB,
        /// including the bitmap dimensions and colours.
        /// </param>
        /// <param name="bits">
        /// A pointer to a variable that receives a pointer to the location of the DIB bit
        /// values.
        /// </param>
        /// <returns>
        /// If the function succeeds, the return value is a handle to the newly created DIB,
        /// and bits points to the bitmap bit values. If the function fails, the return value
        /// is IntPtr.Zero, and bits is IntPtr.Zero.
        /// </returns>
        public static IntPtr CreateDeviceIndependenctBitmapSection(
            IntPtr handle, ref BITMAPINFO bitmap, out IntPtr bits)
        {
            return CreateDIBSection(handle, ref bitmap, 0, out bits, IntPtr.Zero, 0);
        }

        /// <summary>
        /// Creates a rectangular region.
        /// </summary>
        /// <param name="left">
        /// Specifies the x-coordinate of the upper-left corner of the region in logical units.
        /// </param>
        /// <param name="top">
        /// Specifies the y-coordinate of the upper-left corner of the region in logical units.
        /// </param>
        /// <param name="right">
        /// Specifies the x-coordinate of the lower-right corner of the region in logical
        /// units.
        /// </param>
        /// <param name="bottom">
        /// Specifies the y-coordinate of the lower-right corner of the region in logical
        /// units.
        /// </param>
        /// <returns>
        /// If the function succeeds, the return value is the handle to the region; otherwise,
        /// IntPtr.Zero.
        /// </returns>
        [DllImport("gdi32.dll")]
        public static extern IntPtr CreateRectRgn(int left, int top, int right, int bottom);

        /// <summary>
        /// Creates a rectangular region.
        /// </summary>
        /// <param name="regionUnits">
        /// Pointer to a RECT structure that contains the coordinates of the upper-left and
        /// lower-right corners of the rectangle that defines the region in logical units.
        /// </param>
        /// <returns>
        /// If the function succeeds, the return value is the handle to the region; otherwise,
        /// IntPtr.Zero.
        /// </returns>
        [DllImport("gdi32.dll")]
        public static extern IntPtr CreateRectRgnIndirect(ref RECT regionUnits);

        /// <summary>
        /// Deletes the specified device context.
        /// </summary>
        /// <param name="handle">
        /// A handle to the device context to be deleted.
        /// </param>
        /// <returns>
        /// True if the function succeeds; otherwise, false.
        /// </returns>
        [DllImport("gdi32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool DeleteDC(IntPtr handle);

        /// <summary>
        /// The DeleteObject function deletes a logical pen, brush, font, bitmap, region, or
        /// palette, freeing all system resources associated with the object. After the object
        /// is deleted, the specified handle is no longer valid.
        /// </summary>
        /// <param name="handle">
        /// A handle to a logical pen, brush, font, bitmap, region, or palette.
        /// </param>
        /// <returns>
        /// True if the function succeeds; otherwise, false.
        /// </returns>
        [DllImport("gdi32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool DeleteObject(IntPtr handle);

        /// <summary>
        /// Retrieves device-specific information for the specified device.
        /// </summary>
        /// <param name="deviceHandle">
        /// A handle to the device to get the information from.
        /// </param>
        /// <param name="index">
        /// The item to be returned.
        /// </param>
        /// <returns>
        /// The return value specifies the value of the desired item.
        /// </returns>
        public static int GetDeviceCaps(IntPtr deviceHandle, DeviceCapability index)
        {
            return GetDeviceCaps(deviceHandle, (int)index);
        }

        /// <summary>
        /// Selects an object into the specified device context. The new object replaces the
        /// previous object of the same type.
        /// </summary>
        /// <param name="deviceHandle">
        /// A handle to the device context.
        /// </param>
        /// <param name="objHandle">
        /// A handle to the object to be selected.
        /// </param>
        /// <returns>
        /// If the selected object is not a region and the function succeeds, the return value
        /// is a handle to the object being replaced. If an error occurs and the selected
        /// object is not a region, the return value is IntPtr.Zero.
        /// </returns>
        [DllImport("gdi32.dll", ExactSpelling = true, SetLastError = true)]
        public static extern IntPtr SelectObject(IntPtr deviceHandle, IntPtr objHandle);

        /// <summary>
        /// Creates a logical brush that has the specified solid colour.
        /// </summary>
        /// <param name="colour">
        /// The colour of the brush.
        /// </param>
        /// <returns>
        /// If the function succeeds, the return value identifies a logical brush; otherwise,
        /// IntPtr.Zero.
        /// </returns>
        [DllImport("gdi32.dll")]
        public static extern IntPtr CreateSolidBrush(int colour);

        /// <summary>
        /// The CreateDIBSection function creates a DIB that applications can write to
        /// directly. The function gives you a pointer to the location of the bitmap bit
        /// values. You can supply a handle to a file-mapping object that the function will use
        /// to create the bitmap, or you can let the system allocate the memory
        /// for the bitmap.
        /// </summary>
        /// <param name="handle">
        /// A handle to a device context.
        /// </param>
        /// <param name="bitmapInfo">
        /// A pointer to a <see cref="BITMAPINFO"/> structure that specifies various attributes
        /// of the DIB, including the bitmap dimensions and colours.
        /// </param>
        /// <param name="usage">
        /// The type of data contained in the Colours array member of the
        /// <see cref="BITMAPINFO"/> structure pointed to by <paramref name="bitmapInfo"/>.
        /// </param>
        /// <param name="bits">
        /// A pointer to a variable that receives a pointer to the location of the
        /// DIB bit values.
        /// </param>
        /// <param name="section">
        /// A handle to a file-mapping object that the function will use to create the DIB.
        /// This parameter can be NULL.
        /// </param>
        /// <param name="offset">
        /// The offset from the beginning of the file-mapping object referenced by hSection
        /// where storage for the bitmap bit values is to begin.
        /// </param>
        /// <returns>
        /// If the function succeeds, the return value is a handle to the newly created DIB,
        /// and <paramref name="bits"/> points to the bitmap bit values. If the function fails,
        /// the return value is IntPtr.Zero, and <paramref name="bits"/> is IntPtr.Zero.
        /// </returns>
        [DllImport("gdi32.dll", SetLastError = true)]
        private static extern IntPtr CreateDIBSection(
            IntPtr handle,
            ref BITMAPINFO bitmapInfo,
            uint usage,
            out IntPtr bits,
            IntPtr section,
            uint offset);

        /// <summary>
        /// Retrieves device-specific information for the specified device.
        /// </summary>
        /// <param name="deviceHandle">
        /// A handle to the device to get the information from.
        /// </param>
        /// <param name="index">
        /// The item to be returned.
        /// </param>
        /// <returns>
        /// The return value specifies the value of the desired item.
        /// </returns>
        [DllImport("gdi32.dll", CallingConvention = CallingConvention.StdCall)]
        private static extern int GetDeviceCaps(IntPtr deviceHandle, int index);
        #endregion Methods
    } // RSG.Interop.Microsoft.Windows.Gdi32 {Class}
} // RSG.Interop.Microsoft.Windows {Namespace}
