﻿//---------------------------------------------------------------------------------------------
// <copyright file="ResourceType.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System;

    /// <summary>
    /// Predefined Windows resource types (RT_ constants).
    /// </summary>
    /// http://msdn.microsoft.com/en-us/library/windows/desktop/ms648009(v=vs.85).aspx
    /// 
    public enum ResourceType : uint
    {
        /// <summary>
        /// RT_CURSOR; cursor resource.
        /// </summary>
        Cursor = 0x00000001,

        /// <summary>
        /// RT_BITMAP; bitmap resource.
        /// </summary>
        Bitmap = 0x00000002,

        /// <summary>
        /// RT_ICON; icon resource.
        /// </summary>
        Icon = 0x00000003,

        /// <summary>
        /// RT_MENU; menu resource.
        /// </summary>
        Menu = 0x00000004,

        /// <summary>
        /// RT_DIALOG; dialog resource.
        /// </summary>
        Dialog = 0x00000005,

        /// <summary>
        /// RT_STRING; string resource.
        /// </summary>
        String = 0x00000006,
        
        /// <summary>
        /// RT_FONTDIR; fontdir resource.
        /// </summary>
        FontDir = 0x00000007,

        /// <summary>
        /// RT_FONT; font resource.
        /// </summary>
        Font = 0x00000008,

        /// <summary>
        /// RT_ACCELERATOR; accelerator resource.
        /// </summary>
        Accelerator = 0x00000009,

        /// <summary>
        /// RT_RCDATA; custom data resource.
        /// </summary>
        Data = 0x00000010,

        /// <summary>
        /// RT_MESSAGETABLE; message table resource.
        /// </summary>
        MessageTable = 0x00000011
    }

} // RSG.Interop.Microsoft.Windows namespace
