﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Interop.Microsoft.Windows
{
    /// <summary>
    /// SystemParameterInfoAction enumeration.
    /// </summary>
    public enum SystemParameterInfoAction : int
    {
        /// <summary>
        /// Retrieves the size of the work area on the primary display monitor. The work
        /// area is the portion of the screen not obscured by the system taskbar or by
        /// application desktop toolbars. The pvParam parameter must point to a RECT
        /// structure that receives the coordinates of the work area, expressed in
        /// virtual screen coordinates.
        /// 
        /// To get the work area of a monitor other than the primary display monitor,
        /// call the GetMonitorInfo function.
        /// </summary>
        GETWORKAREA = 0x0030
    }
}
