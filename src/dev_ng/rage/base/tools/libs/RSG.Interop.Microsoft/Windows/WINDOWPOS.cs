﻿//---------------------------------------------------------------------------------------------
// <copyright file="WINDOWPOS.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System;
    using System.Runtime.InteropServices;

    /// <summary>
    /// Contains information about the size and position of a window.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct WINDOWPOS
    {
        #region Fields
        /// <summary>
        /// A handle to the window.
        /// </summary>
        public IntPtr Handle;

        /// <summary>
        /// The position of the window in Z order (front-to-back position).
        /// </summary>
        public IntPtr InsertAfter;

        /// <summary>
        /// The position of the left edge of the window.
        /// </summary>
        public int Left;

        /// <summary>
        /// The position of the top edge of the window.
        /// </summary>
        public int Top;

        /// <summary>
        /// The window width, in pixels.
        /// </summary>
        public int Width;

        /// <summary>
        /// The window height, in pixels.
        /// </summary>
        public int Height;

        /// <summary>
        /// The window position.
        /// </summary>
        public WindowPosFlags Flags;
        #endregion Fields

        #region Methods
        /// <summary>
        /// Determines whether the specified objects are equal to each other.
        /// </summary>
        /// <param name="position1">
        /// The first object to compare.
        /// </param>
        /// <param name="position2">
        /// The second object to compare.
        /// </param>
        /// <returns>
        /// True if the specified objects are equal to each other; otherwise, false.
        /// </returns>
        public static bool operator ==(WINDOWPOS position1, WINDOWPOS position2)
        {
            return position1.Equals(position2);
        }

        /// <summary>
        /// Determines whether the specified objects are not equal to each other.
        /// </summary>
        /// <param name="position1">
        /// The first object to compare.
        /// </param>
        /// <param name="position2">
        /// The second object to compare.
        /// </param>
        /// <returns>
        /// True if the specified objects are not equal to each other; otherwise, false.
        /// </returns>
        public static bool operator !=(WINDOWPOS position1, WINDOWPOS position2)
        {
            return !position1.Equals(position2);
        }

        /// <summary>
        /// Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        /// A hash code for the current object.
        /// </returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">
        /// The object to compare with the current object.
        /// </param>
        /// <returns>
        /// True if the specified object is equal to the current object; otherwise, false.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (!(obj is WINDOWPOS))
            {
                return false;
            }

            return this.Equals((WINDOWPOS)obj);
        }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="other">
        /// The object to compare with the current object.
        /// </param>
        /// <returns>
        /// True if the specified object is equal to the current object; otherwise, false.
        /// </returns>
        public bool Equals(WINDOWPOS other)
        {
            return object.ReferenceEquals(this, other);
        }
        #endregion Methods
    } // RSG.Interop.Microsoft.Windows.WINDOWPOS {Structure}
} // RSG.Interop.Microsoft.Windows {Namespace}
