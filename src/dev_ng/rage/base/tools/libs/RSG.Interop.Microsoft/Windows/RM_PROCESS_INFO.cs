﻿//---------------------------------------------------------------------------------------------
// <copyright file="RM_PROCESS_INFO.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System;
    using System.Runtime.InteropServices;
    using Com = System.Runtime.InteropServices.ComTypes;

    /// <summary>
    /// Describes an application that is to be registered with the Restart Manager.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    struct RM_PROCESS_INFO
    {
    }

} // RSG.Interop.Microsoft.Windows namespace
