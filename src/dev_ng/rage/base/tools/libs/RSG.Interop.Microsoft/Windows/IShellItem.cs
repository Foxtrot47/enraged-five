﻿// --------------------------------------------------------------------------------------------
// <copyright file="IShellItem.cs" company="Rockstar">
//     Copyright © Rockstar Games 2012. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    /// <summary>
    /// Exposes methods that retrieve information about a Shell item.
    /// </summary>
    [ComImport,
    Guid(InterfaceIDGuid.ShellItemId),
    InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IShellItem
    {
        #region Methods
        /// <summary>
        /// Binds to a handler for an item as specified by the handler ID value.
        /// </summary>
        /// <param name="bindContext">
        /// The bind context interface.
        /// </param>
        /// <param name="handler">
        /// A flag that determines the handler.
        /// </param>
        /// <param name="objectId">
        /// The IID of the object type to retrieve.
        /// </param>
        /// <param name="bind">
        /// When this methods returns, contains the object specified using the handler
        /// specified.
        /// </param>
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void BindToHandler(
            [In, MarshalAs(UnmanagedType.Interface)] IntPtr bindContext,
            [In] ref Guid handler,
            [In] ref Guid objectId,
            out IntPtr bind);

        /// <summary>
        /// Gets the parent of an IShellItem object.
        /// </summary>
        /// <param name="parent">
        /// When this method returns contains the parent of this shell item.
        /// </param>
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void GetParent([MarshalAs(UnmanagedType.Interface)] out IShellItem parent);

        /// <summary>
        /// Gets the display name of the IShellItem object.
        /// </summary>
        /// <param name="method">
        /// One of the SIGDN values that indicates how the name should look.
        /// </param>
        /// <param name="nane">
        /// A value that, when this function returns successfully, receives the address of a
        /// pointer to the retrieved display name.
        /// </param>
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void GetDisplayName(
            [In] GetDisplayNameFlag method, [MarshalAs(UnmanagedType.LPWStr)] out string nane);

        /// <summary>
        /// Gets a requested set of attributes of the IShellItem object.
        /// </summary>
        /// <param name="mask">
        /// Specifies the attributes to retrieve.
        /// </param>
        /// <param name="attributes">
        /// A pointer to a value that, when this method returns successfully, contains the
        /// requested attributes.
        /// </param>
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void GetAttributes([In] uint mask, out uint attributes);

        /// <summary>
        /// Compares two IShellItem objects.
        /// </summary>
        /// <param name="shellItem">
        /// The shell item to compare to.
        /// </param>
        /// <param name="hint">
        /// A value indicating how to perform the comparison.
        /// </param>
        /// <param name="order">
        /// A integer value indicating the order of the two shell items. If the two items are
        /// the same this parameter equals zero; if they are different the parameter is
        /// nonzero.
        /// </param>
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void Compare(
            [In, MarshalAs(UnmanagedType.Interface)] IShellItem shellItem,
            [In] uint hint,
            out int order);
        #endregion
    } // RSG.Interop.Microsoft.Windows.IShellItem {Interface}
} // RSG.Interop.Microsoft.Windows {Namespace}
