﻿//---------------------------------------------------------------------------------------------
// <copyright file="TrackPopupMenuFlags.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System;

    /// <summary>
    /// Defines the different flags used to control the way a shortcut menu is displayed
    /// on a window.
    /// </summary>
    [Flags]
    public enum TrackPopupMenuFlags : uint
    {
        /// <summary>
        /// Positions the shortcut menu so that its bottom side is aligned with the coordinate
        /// specified by the y parameter.
        /// </summary>
        BOTTOMALIGN = 0x0020,

        /// <summary>
        /// Centres the shortcut menu horizontally relative to the coordinate specified by the
        /// x parameter.
        /// </summary>
        CENTERALIGN = 0x0004,

        /// <summary>
        /// If the menu cannot be shown at the specified location without overlapping the
        /// excluded rectangle, the system tries to accommodate the requested horizontal
        /// alignment before the requested vertical alignment.
        /// </summary>
        HORIZONTAL = 0x0000,

        /// <summary>
        /// Animates the menu from right to left.
        /// </summary>
        HORNEGANIMATION = 0x0800,

        /// <summary>
        /// Animates the menu from left to right.
        /// </summary>
        HORPOSANIMATION = 0x0400,

        /// <summary>
        /// Positions the shortcut menu so that its left side is aligned with the coordinate
        /// specified by the x parameter.
        /// </summary>
        LEFTALIGN = 0x0000,

        /// <summary>
        /// The user can select menu items with only the left mouse button.
        /// </summary>
        LEFTBUTTON = 0x0000,

        /// <summary>
        /// Displays menu without animation.
        /// </summary>
        NOANIMATION = 0x4000,

        /// <summary>
        /// The function does not send notification messages when the user clicks a menu item.
        /// </summary>
        NONOTIFY = 0x0080,

        /// <summary>
        /// The function returns the menu item identifier of the user's selection in the
        /// return value.
        /// </summary>
        RETURNCMD = 0x0100,

        /// <summary>
        /// Positions the shortcut menu so that its right side is aligned with the coordinate
        /// specified by the x parameter.
        /// </summary>
        RIGHTALIGN = 0x0008,

        /// <summary>
        /// The user can select menu items with both the left and right mouse buttons.
        /// </summary>
        RIGHTBUTTON = 0x0002,

        /// <summary>
        /// Positions the shortcut menu so that its top side is aligned with the coordinate
        /// specified by the y parameter.
        /// </summary>
        TOPALIGN = 0x0000,

        /// <summary>
        /// Centres the shortcut menu vertically relative to the coordinate specified by
        /// the y parameter.
        /// </summary>
        VCENTERALIGN = 0x0010,

        /// <summary>
        /// Animates the menu from bottom to top.
        /// </summary>
        VERNEGANIMATION = 0x2000,

        /// <summary>
        /// Animates the menu from top to bottom.
        /// </summary>
        VERPOSANIMATION = 0x1000,

        /// <summary>
        /// If the menu cannot be shown at the specified location without overlapping the
        /// excluded rectangle, the system tries to accommodate the requested vertical
        /// alignment before the requested horizontal alignment.
        /// </summary>
        VERTICAL = 0x0040,

        /// <summary>
        /// Defines the base values in this enum.
        /// </summary>
        BASE = NONOTIFY | RETURNCMD | RIGHTBUTTON
    } // RSG.Interop.Microsoft.Windows.TrackPopupMenuFlags {Enum}
} // RSG.Interop.Microsoft.Windows {Namespace}
