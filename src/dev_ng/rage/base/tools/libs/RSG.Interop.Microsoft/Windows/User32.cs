﻿//---------------------------------------------------------------------------------------------
// <copyright file="User32.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.InteropServices;
    using System.Security;
    using System.Text;
    using System.Windows;
    using System.Windows.Input;

    /// <summary>
    /// Contains native functions that are imported from the unmanaged assembly User32.dll.
    /// </summary>
    public static class User32
    {
        #region Methods
        /// <summary>
        /// The <see cref="BeginPaint"/> function prepares the specified window for painting
        /// and fills a <see cref="PAINTSTRUCT"/> structure with information about the
        /// painting.
        /// </summary>
        /// <param name="hwnd">
        /// Handle to the window to be repainted.
        /// </param>
        /// <param name="lpPaint">
        /// Pointer to the <see cref="PAINTSTRUCT"/> structure that will receive painting
        /// information.
        /// </param>
        /// <returns>
        /// If the function succeeds, the return value is the handle to a display device
        /// context for the specified window.
        /// If the function fails, the return value is NULL, indicating that no display device
        /// context is available.
        /// </returns>
        [DllImport("user32.dll")]
        public static extern IntPtr BeginPaint(IntPtr hwnd, out PAINTSTRUCT lpPaint);

        /// <summary>
        /// Converts the client-area coordinates of a specified point to screen coordinates.
        /// </summary>
        /// <param name="window">
        /// A handle to the window whose client area is used for the conversion.
        /// </param>
        /// <param name="client">
        /// A POINT structure that contains the client coordinates to be converted.
        /// </param>
        /// <returns>
        /// The converted POINT structure.
        /// </returns>
        public static POINT ClientToScreen(IntPtr window, POINT client)
        {
            POINT result = new POINT(client.X, client.Y);
            ClientToScreen(window, ref result);
            return result;
        }

        /// <summary>
        /// Creates an overlapped, pop-up, or child window with an extended window style.
        /// </summary>
        /// <param name="extendedStyle">
        /// The extended window style of the window being created.
        /// </param>
        /// <param name="atom">
        /// A null-terminated string or a class atom.
        /// </param>
        /// <param name="style">
        /// The style of the window being created.
        /// </param>
        /// <param name="parent">
        /// A handle to the parent or owner window of the window being created.
        /// </param>
        /// <returns>
        /// If the function succeeds, the return value is a handle to the new window; otherwise
        /// null.
        /// </returns>
        public static IntPtr CreateWindow(
            WindowExStyles extendedStyle, IntPtr atom, WindowStyles style, IntPtr parent)
        {
            return CreateWindowEx(
                (uint)extendedStyle,
                atom,
                String.Empty,
                (uint)style,
                0,
                0,
                0,
                0,
                parent,
                IntPtr.Zero,
                IntPtr.Zero,
                IntPtr.Zero);
        }

        /// <summary>
        /// Creates an overlapped, pop-up, or child window with an extended window style.
        /// </summary>
        /// <param name="extendedStyle">
        /// The extended window style of the window being created.
        /// </param>
        /// <param name="classAtom">
        /// A null-terminated string or a class atom.
        /// </param>
        /// <param name="windowName">
        /// The window name.
        /// </param>
        /// <param name="style">
        /// The style of the window being created.
        /// </param>
        /// <param name="x">
        /// The initial horizontal position of the window.
        /// </param>
        /// <param name="y">
        /// The initial vertical position of the window.
        /// </param>
        /// <param name="width">
        /// The width, in device units, of the window.
        /// </param>
        /// <param name="height">
        /// The height, in device units, of the window.
        /// </param>
        /// <param name="parent">
        /// A handle to the parent or owner window of the window being created.
        /// </param>
        /// <param name="menu">
        /// A handle to a menu, or specifies a child-window identifier, depending on the
        /// window style.
        /// </param>
        /// <param name="module">
        /// A handle to the instance of the module to be associated with the window.
        /// </param>
        /// <param name="param">
        /// Pointer to a value to be passed to the window through the CREATESTRUCT structure.
        /// </param>
        /// <returns>
        /// If the function succeeds, the return value is a handle to the new
        /// window; otherwise null.
        /// </returns>
        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr CreateWindowEx(
            uint extendedStyle,
            IntPtr classAtom,
            [MarshalAs(UnmanagedType.LPWStr)] string windowName,
            uint style,
            int x,
            int y,
            int width,
            int height,
            IntPtr parent,
            IntPtr menu,
            IntPtr module,
            IntPtr param);

        /// <summary>
        /// Get a array containing the handles to all the non child windows associated with
        /// the current thread.
        /// </summary>
        /// <returns>
        /// A array containing the handles to all the non child windows associated with the
        /// current thread.
        /// </returns>
        public static IntPtr[] CurrentThreadWindows()
        {
            List<IntPtr> windows = new List<IntPtr>();
            EnumThreadWindows(
                GetCurrentThreadId(),
                delegate(IntPtr window, IntPtr longParameter)
                {
                    windows.Add(window);
                    return true;
                },
                IntPtr.Zero);

            return windows.ToArray();
        }

        /// <summary>
        /// Get a array containing the handles to all the non child windows associated with
        /// the specified thread id.
        /// </summary>
        /// <param name="threadId">
        /// The thread id whose windows should be returned.
        /// </param>
        /// <returns>
        /// A array containing the handles to all the non child windows associated with the
        /// current thread.
        /// </returns>
        public static IntPtr[] CurrentThreadWindows(uint threadId)
        {
            List<IntPtr> windows = new List<IntPtr>();
            EnumThreadWindows(
                threadId,
                delegate(IntPtr window, IntPtr longParameter)
                {
                    windows.Add(window);
                    return true;
                },
                IntPtr.Zero);

            return windows.ToArray();
        }

        /// <summary>
        /// Calls the default window procedure to provide default processing for any window
        /// messages that an application does not process.
        /// </summary>
        /// <param name="window">
        /// A handle to the window procedure that received the message.
        /// </param>
        /// <param name="msg">
        /// The message.
        /// </param>
        /// <param name="wordParameter">
        /// Additional word message information. The content of this parameter depends on the
        /// value of the <paramref name="msg"/> parameter.
        /// </param>
        /// <param name="longParameter">
        /// Additional long message information. The content of this parameter depends on the
        /// value of the <paramref name="msg"/> parameter.
        /// </param>
        /// <returns>
        /// The return value is the result of the message processing and depends on the
        /// message.
        /// </returns>
        [DllImport("user32.dll", CharSet = CharSet.Unicode, EntryPoint = "DefWindowProc")]
        public static extern IntPtr DefaultWindowProc(
            IntPtr window, WindowMessage msg, IntPtr wordParameter, IntPtr longParameter);

        /// <summary>
        /// Calls the default window procedure to provide default processing for any window
        /// messages that an application does not process.
        /// </summary>
        /// <param name="window">
        /// A handle to the window procedure that received the message.
        /// </param>
        /// <param name="msg">
        /// The message.
        /// </param>
        /// <param name="wordParameter">
        /// First additional message information. The contents of this parameter depend on the
        /// value of the <paramref name="msg"/> parameter.
        /// </param>
        /// <param name="longParameter">
        /// Second additional message information. The contents of this parameter depend on the
        /// value of the <paramref name="msg"/> parameter.
        /// </param>
        /// <returns>
        /// The return value is the result of the message processing and depends on the
        /// message.
        /// </returns>
        [DllImport("user32.dll")]
        public static extern IntPtr DefWindowProc(
            IntPtr window,
            WindowMessage msg,
            IntPtr wordParameter,
            IntPtr longParameter);

        /// <summary>
        /// Destroys the specified window. The function sends DESTROY and NCDESTROY messages to
        /// the window to deactivate it and remove the keyboard focus from it.
        /// </summary>
        /// <param name="window">
        /// A handle to the window to be destroyed.
        /// </param>
        /// <returns>
        /// True if the window was destroyed; otherwise false.
        /// </returns>
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool DestroyWindow(IntPtr window);

        /// <summary>
        /// Redraws the menu bar of the specified window.
        /// </summary>
        /// <param name="window">
        /// A handle to the window whose menu bar is to be redrawn.
        /// </param>
        /// <returns>
        /// If the function succeeds, the return value is true; otherwise, false.
        /// </returns>
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool DrawMenuBar(IntPtr window);

        /// <summary>
        /// Enables the specified menu item on the specified menu.
        /// </summary>
        /// <param name="menu">
        /// The handle to the menu.
        /// </param>
        /// <param name="item">
        /// The menu item to enable.
        /// </param>
        public static void EnableMenuItem(IntPtr menu, uint item)
        {
            EnableMenuItem(menu, item, 0x00000000);
        }

        /// <summary>
        /// The <see cref="EndPaint"/> function marks the end of painting in the specified
        /// window. This function is required for each call to the <see cref="BeginPaint"/>
        /// function, but only after painting is complete.
        /// </summary>
        /// <param name="hWnd">
        /// Handle to the window that has been repainted.
        /// </param>
        /// <param name="lpPaint">
        /// Pointer to a <see cref="PAINTSTRUCT"/> structure that contains the painting
        /// information retrieved by <see cref="BeginPaint"/>.
        /// </param>
        /// <returns>
        /// The return value is always nonzero.
        /// </returns>
        [DllImport("user32.dll")]
        public static extern bool EndPaint(IntPtr hWnd, [In] ref PAINTSTRUCT lpPaint);

        /// <summary>
        /// Enumerates all top-level windows on the screen by passing the handle to each
        /// window, in turn, to an application-defined call-back function.
        /// </summary>
        /// <param name="method">
        /// A pointer to an application-defined call-back function.
        /// </param>
        /// <param name="longParameter">
        /// An application-defined value to be passed to the call-back function.
        /// </param>
        /// <returns>
        /// If the function succeeds, the return value is nonzero; otherwise, zero.
        /// </returns>
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool EnumWindows(
            EnumWindowsProcDelegate method, IntPtr longParameter);

        /// <summary>
        /// Fills a rectangle by using the specified brush.
        /// </summary>
        /// <param name="handle">
        /// A handle to the device context.
        /// </param>
        /// <param name="rect">
        /// Contains the logical coordinates of the rectangle to be filled.
        /// </param>
        /// <param name="brush">
        /// A handle to the brush used to fill the rectangle.
        /// </param>
        /// <returns>
        /// True if successful; otherwise false.
        /// </returns>
        public static bool FillRect(IntPtr handle, ref Rect rect, IntPtr brush)
        {
            RECT nativeRect = new RECT(rect);
            return FillRect(handle, ref nativeRect, brush);
        }

        /// <summary>
        /// Retrieves a handle to the first top-level window whose class name matches the
        /// specified string.
        /// </summary>
        /// <param name="className">
        /// The class name or a class atom.
        /// </param>
        /// <returns>
        /// If the function succeeds, the return value is a handle to the first window found
        /// that has the specified class name; otherwise IntPtr.Zero.
        /// </returns>
        public static IntPtr FindWindow(string className)
        {
            return User32.FindWindow(className, null);
        }

        /// <summary>
        /// Retrieves a handle to a window whose class name and window name match the
        /// specified strings. The function searches child windows, beginning with the one
        /// following the specified child window. This function does not perform a
        /// case-sensitive search.
        /// </summary>
        /// <param name="hwndParent">
        /// A handle to the parent window whose child windows are to be searched.
        /// </param>
        /// <param name="hwndChildAfter">
        /// A handle to a child window. The search begins with the next child window in the Z
        /// order. The child window must be a direct child window of hwndParent, not just a
        /// descendant window.
        /// </param>
        /// <param name="lpszClass">
        /// The class name or a class atom created by a previous call to the
        /// <see cref="RegisterClass"/> or RegisterClassEx function. The atom must be placed
        /// in the low-order word of lpszClass; the high-order word must be zero.
        /// </param>
        /// <param name="lpszWindow">
        /// The window name (the window's title). If this parameter is NULL, all window names
        /// match.
        /// </param>
        /// <returns>
        /// If the function succeeds, the return value is a handle to the window that has the
        /// specified class and window names.
        /// If the function fails, the return value is IntPtr.Zero.
        /// </returns>
        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr FindWindowEx(
            IntPtr hwndParent, IntPtr hwndChildAfter, string lpszClass, string lpszWindow);

        /// <summary>
        /// Returns an array containing the handles to all the non child windows associated
        /// with the specified class name.
        /// </summary>
        /// <param name="className">
        /// The class name whose windows should be returned.
        /// </param>
        /// <returns>
        /// An array containing the handles to all the non child windows associated with the
        /// specified class name.
        /// </returns>
        public static IntPtr[] GetAllWindowsWithClassName(string className)
        {
            List<IntPtr> windows = new List<IntPtr>();
            EnumWindows(
                delegate(IntPtr window, IntPtr longParameter)
                {
                    if (String.Equals(GetClassName(window), className))
                    {
                        windows.Add(window);
                    }

                    return true;
                },
                IntPtr.Zero);

            return windows.ToArray();
        }

        /// <summary>
        /// Retrieves the interval time used for automatically scrolling while dragging in
        /// milliseconds.
        /// </summary>
        /// <returns>
        /// The return value specifies the interval time used for automatically scrolling, in
        /// milliseconds. 
        /// </returns>
        public static double GetAutoScrollTimeInMilliseconds()
        {
            return (double)GetDoubleClickTime() * 0.8d;
        }

        /// <summary>
        /// Retrieves the time span representing the interval time used for automatically
        /// scrolling while dragging.
        /// </summary>
        /// <returns>
        /// The return value specifies the interval time used for automatically scrolling, in
        /// milliseconds. 
        /// </returns>
        public static TimeSpan GetAutoScrollTimeSpan()
        {
            return TimeSpan.FromMilliseconds(GetAutoScrollTimeInMilliseconds());
        }

        /// <summary>
        /// Retrieves a handle to the window (if any) that has captured the mouse.
        /// </summary>
        /// <returns>
        /// The handle to the capture window associated with the current thread.
        /// </returns>
        [DllImport("user32.dll")]
        public static extern IntPtr GetCapture();

        /// <summary>
        /// Retrieves the name of the class to which the specified window belongs.
        /// </summary>
        /// <param name="window">
        /// A handle to the window and, indirectly, the class to which the window belongs.
        /// </param>
        /// <returns>
        /// The name of the class to which the specified window belongs.
        /// </returns>
        public static string GetClassName(IntPtr window)
        {
            StringBuilder builder = new StringBuilder(1024);
            GetClassName(window, builder, builder.Capacity);
            return builder.ToString();
        }

        /// <summary>
        /// Retrieves the coordinates of a window's client area.
        /// </summary>
        /// <param name="window">
        /// A handle to the window.
        /// </param>
        /// <returns>
        /// A System.Windows.Rect structure containing the specified windows bounding area.
        /// </returns>
        public static Rect GetClientRect(IntPtr window)
        {
            RECT rect = new RECT(0, 0, 0, 0);
            GetClientRect(window, out rect);
            return new Rect(rect.Left, rect.Top, rect.Width, rect.Height);
        }

        /// <summary>
        /// Retrieves the cursor's position, in screen coordinates.
        /// </summary>
        /// <returns>
        /// A point containing the screen coordinates of the cursor.
        /// </returns>
        public static Point GetCursorPos()
        {
            Point result = default(Point);
            POINT point = new POINT { X = 0, Y = 0 };
            if (GetCursorPos(ref point))
            {
                result.X = (double)point.X;
                result.Y = (double)point.Y;
            }

            return result;
        }

        /// <summary>
        /// Retrieves a handle to a device context for the client area of a specified window or
        /// for the entire screen.
        /// </summary>
        /// <param name="window">
        /// A handle to the window whose device context is to be retrieved. If this value is
        /// IntPtr.Zero, this retrieves the device context for the entire screen.
        /// </param>
        /// <returns>
        /// If the function succeeds, the return value is a handle to the DC for the specified
        /// window's client area; otherwise IntPtr.Zero.
        /// </returns>
        [DllImport("User32.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr GetDC(IntPtr window);

        /// <summary>
        /// Retrieves a handle to a device context (DC) for the client area of a specified
        /// window or for the entire screen.
        /// </summary>
        /// <param name="window">
        /// A handle to the window whose DC is to be retrieved.
        /// </param>
        /// <param name="clipRegion">
        /// A clipping region that may be combined with the visible region of the DC.
        /// </param>
        /// <param name="flags">
        /// Specifies how the DC is created.
        /// </param>
        /// <returns>
        /// If the function succeeds, the return value is the handle to the DC for the
        /// specified window; otherwise, IntPtr.Zero.
        /// </returns>
        [DllImport("User32.dll")]
        public static extern IntPtr GetDCEx(
            IntPtr window, IntPtr clipRegion, DeviceCreationFlags flags);

        /// <summary>
        /// Gets a handle to the desktop window. The desktop window covers the entire screen.
        /// The desktop window is the area on top of which other windows are painted.
        /// </summary>
        /// <returns>
        /// Handle to the desktop window.
        /// </returns>
        [DllImport("user32.dll")]
        public static extern IntPtr GetDesktopWindow();

        /// <summary>
        /// Get a array containing the handles to all the display monitors.
        /// </summary>
        /// <returns>
        /// A array containing the handles to all the display monitors.
        /// </returns>
        public static Rect[] GetDisplayMonitorAreas()
        {
            List<Rect> areas = new List<Rect>();
            EnumDisplayMonitors(
                IntPtr.Zero,
                IntPtr.Zero,
                delegate(IntPtr monitor, IntPtr dc, ref RECT area, IntPtr data)
                {
                    MONITORINFO monitorInfo = GetMonitorInfo(monitor);
                    areas.Add(monitorInfo.Work.ToRect());
                    return true;
                },
                IntPtr.Zero);

            return areas.ToArray();
        }

        /// <summary>
        /// Retrieves the current double-click time for the mouse. A double-click is a series
        /// of two clicks of the mouse button, the second occurring within a specified time
        /// after the first. The double-click time is the maximum number of milliseconds that
        /// may occur between the first and second click of a double-click. The maximum
        /// double-click time is 5000 milliseconds.
        /// </summary>
        /// <returns>
        /// The return value specifies the current double-click time, in milliseconds. 
        /// </returns>
        [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        public static extern uint GetDoubleClickTime();

        /// <summary>
        /// Retrieves the handle to the window that has the keyboard focus, if the window is
        /// attached to the calling thread's message queue.
        /// </summary>
        /// <returns>
        /// The return value is the handle to the window with the keyboard focus.
        /// </returns>
        [DllImport("user32.dll")]
        public static extern IntPtr GetFocus();

        /// <summary>
        /// Retrieves a handle to the foreground window (the window with which the user is
        /// currently working).
        /// </summary>
        /// <returns>
        /// The window handler to the window that currently is active on the users desktop.
        /// </returns>
        [DllImport("user32.dll")]
        public static extern IntPtr GetForegroundWindow();

        /// <summary>
        /// Gives focus to a given window.
        /// </summary>
        /// <param name="hWnd"></param>
        /// <returns></returns>
        [DllImport("user32.DLL")]
        public static extern bool SetForegroundWindow(IntPtr hWnd);

        /// <summary>
        /// Retrieves the status of the specified virtual key. The status specifies whether the
        /// key is up, down, or toggled.
        /// </summary>
        /// <param name="key">
        /// A virtual key.
        /// </param>
        /// <returns>
        /// The return value specifies the status of the specified virtual key.
        /// </returns>
        [DllImport("user32.dll")]
        public static extern short GetKeyState(VirtualKey key);

        /// <summary>
        /// Retrieves a handle to the display monitor that contains a specified point.
        /// </summary>
        /// <param name="point">
        /// Specifies the point of interest in virtual-screen coordinates.
        /// </param>
        /// <returns>
        /// If the point is contained by a display monitor, the return value is an
        /// HMONITOR handle to that display monitor; otherwise, the primary monitor.
        /// </returns>
        public static IntPtr GetMonitorFromPoint(POINT point)
        {
            return MonitorFromPoint(point, GetMonitorFlag.DEFAULTTOPRIMARY);
        }

        /// <summary>
        /// Retrieves a handle to the display monitor that contains a specified point.
        /// </summary>
        /// <param name="point">
        /// Specifies the point of interest in virtual-screen coordinates.
        /// </param>
        /// <param name="flags">
        /// Determines the function's return value if the point is not contained within any
        /// display monitor.
        /// </param>
        /// <returns>
        /// If the point is contained by a display monitor, the return value is an
        /// HMONITOR handle to that display monitor; otherwise, the primary monitor.
        /// </returns>
        public static IntPtr GetMonitorFromPoint(POINT point, GetMonitorFlag flags)
        {
            return MonitorFromPoint(point, flags);
        }

        /// <summary>
        /// Retrieves a handle to the display monitor that has the largest area of intersection
        /// with the bounding rectangle of a specified window.
        /// </summary>
        /// <param name="window">
        /// A handle to the window of interest.
        /// </param>
        /// <returns>
        /// The handle to the display monitor that intersects with the largest area of the
        /// specified window if found; otherwise, the primary monitor.
        /// </returns>
        public static IntPtr GetMonitorFromWindow(IntPtr window)
        {
            return MonitorFromWindow(window, GetMonitorFlag.DEFAULTTOPRIMARY);
        }

        /// <summary>
        /// Retrieves information about a display monitor.
        /// </summary>
        /// <param name="monitor">
        /// A handle to the display monitor of interest.
        /// </param>
        /// <returns>
        /// A MONITORINFO structure that contains information about the specified display
        /// monitor.
        /// </returns>
        public static MONITORINFO GetMonitorInfo(IntPtr monitor)
        {
            MONITORINFO result = default(MONITORINFO);
            result.Size = (uint)Marshal.SizeOf(typeof(MONITORINFO));
            GetMonitorInfo(monitor, ref result);
            return result;
        }

        /// <summary>
        /// Retrieves a handle to the specified window's parent or owner.
        /// </summary>
        /// <param name="hWnd">
        /// A handle to the window whose parent window handle is to be retrieved.
        /// </param>
        /// <returns>
        /// If the window is a child window, the return value is a handle to the parent window.
        /// If the window is a top-level window with the WS_POPUP style, the return value is a
        /// handle to the owner window.
        /// </returns>
        [DllImport("user32.dll", ExactSpelling = true, CharSet = CharSet.Auto)]
        public static extern IntPtr GetParent(IntPtr hWnd);

        /// <summary>
        /// Retrieves the position of the cursor in physical coordinates.
        /// </summary>
        /// <returns></returns>
        public static Point GetPhysicalCursorPos()
        {
            Point result = default(Point);
            POINT point = new POINT { X = 0, Y = 0 };
            if (GetPhysicalCursorPos(ref point))
            {
                result.X = (double)point.X;
                result.Y = (double)point.Y;
            }

            return result;
        }

        /// <summary>
        /// Enables the application to access the window menu.
        /// </summary>
        /// <param name="window">
        /// A handle to the window that will own a copy of the window menu.
        /// </param>
        /// <returns>
        /// A handle to a copy of the window menu.
        /// </returns>
        public static IntPtr GetSystemMenu(IntPtr window)
        {
            return GetSystemMenu(window, false);
        }

        /// <summary>
        /// Retrieves the specified system metric or system configuration setting.
        /// </summary>
        /// <param name="metric">
        /// The system metric or configuration setting to be retrieved.
        /// </param>
        /// <returns>
        /// If successful the requested system metric or configuration setting;
        /// otherwise 0.
        /// </returns>
        [DllImport("user32.dll")]
        public static extern int GetSystemMetrics(SystemMetric metric);

        /// <summary>
        /// Retrieves a handle to a window that has the specified relationship (Z-Order or
        /// owner) to the specified window.
        /// </summary>
        /// <param name="window">
        /// A handle to a window. The window handle retrieved is relative to this window, based
        /// on the value of the <paramref name="relationship"/> parameter.
        /// </param>
        /// <param name="relationship">
        /// The relationship between the specified window and the window whose handle is to be
        /// retrieved.
        /// </param>
        /// <returns>
        /// If the function succeeds, the return value is a window handle. If no window exists
        /// with the specified relationship to the specified window, the return value is
        /// IntPtr.Zero.
        /// </returns>
        [DllImport("user32.dll")]
        public static extern IntPtr GetWindow(IntPtr window, GetWindowFlag relationship);

        /// <summary>
        /// Retrieves information about the style on the specified window.
        /// </summary>
        /// <param name="window">
        /// A handle to the window.
        /// </param>
        /// <returns>
        /// If the function is successful the style of the specified window is returned;
        /// otherwise WindowStyles.None.
        /// </returns>
        public static WindowExStyles GetWindowExStyle(IntPtr window)
        {
            return (WindowExStyles)User32.GetWindowLong(window, WindowLongFlag.EXSTYLE);
        }

        /// <summary>
        /// Retrieves information about the specified window.
        /// </summary>
        /// <param name="window">
        /// A handle to the window whose information is to be retrieved.
        /// </param>
        /// <returns>
        /// A WINDOWINFO structure that contains information about the specified window.
        /// </returns>
        public static WINDOWINFO GetWindowInfo(IntPtr window)
        {
            WINDOWINFO result = default(WINDOWINFO);
            result.Size = (uint)Marshal.SizeOf(typeof(WINDOWINFO));
            GetWindowInfo(window, ref result);
            return result;
        }

        /// <summary>
        /// Retrieves information about the specified window.
        /// </summary>
        /// <param name="window">
        /// A handle to the window.
        /// </param>
        /// <param name="attribute">
        /// A member of the <see cref="WindowLongFlag"/> enumeration that specifies which
        /// attribute to retrieve.
        /// </param>
        /// <returns>
        /// If the function is successful the value of the requested attribute is returned;
        /// otherwise zero.
        /// </returns>
        public static int GetWindowLong(IntPtr window, WindowLongFlag attribute)
        {
            return GetWindowLongPtr32(window, (int)attribute);
        }

        /// <summary>
        /// Retrieves information about the specified window.
        /// </summary>
        /// <param name="window">
        /// A handle to the window.
        /// </param>
        /// <param name="attribute">
        /// A member of the <see cref="WindowLongFlag"/> enumeration that specifies which
        /// attribute to retrieve.
        /// </param>
        /// <returns>
        /// If the function is successful the value of the requested attribute is returned;
        /// otherwise zero.
        /// </returns>
        public static IntPtr GetWindowLongPtr(IntPtr window, WindowLongFlag attribute)
        {
            if (IntPtr.Size == 8)
            {
                return GetWindowLongPtr64(window, (int)attribute);
            }

            return new IntPtr(GetWindowLongPtr32(window, (int)attribute));
        }

        /// <summary>
        /// Retrieves the dimensions of the bounding rectangle of the specified window.
        /// </summary>
        /// <param name="window">
        /// A handle to the window.
        /// </param>
        /// <returns>
        /// A System.Windows.Rect structure containing the specified windows bounding area.
        /// </returns>
        public static RECT GetWindowNativeRect(IntPtr window)
        {
            RECT rect = new RECT(0, 0, 0, 0);
            GetWindowRect(window, out rect);
            return rect;
        }

        /// <summary>
        /// Retrieves the show state and the restored, minimized, and maximized positions
        /// of the specified window.
        /// </summary>
        /// <param name="window">
        /// A handle to the window.
        /// </param>
        /// <returns>
        /// The window placement structure with the placement information set.
        /// </returns>
        public static WINDOWPLACEMENT GetWindowPlacement(IntPtr window)
        {
            WINDOWPLACEMENT windowPlacement = new WINDOWPLACEMENT();
            if (GetWindowPlacement(window, out windowPlacement))
            {
                return windowPlacement;
            }

            throw new Win32Exception(Marshal.GetLastWin32Error());
        }

        /// <summary>
        /// Retrieves the dimensions of the bounding rectangle of the specified window.
        /// </summary>
        /// <param name="window">
        /// A handle to the window.
        /// </param>
        /// <returns>
        /// A System.Windows.Rect structure containing the specified windows bounding area.
        /// </returns>
        public static Rect GetWindowRect(IntPtr window)
        {
            RECT rect = new RECT(0, 0, 0, 0);
            GetWindowRect(window, out rect);
            return new Rect(rect.Left, rect.Top, rect.Width, rect.Height);
        }

        /// <summary>
        /// Retrieves information about the style on the specified window.
        /// </summary>
        /// <param name="window">
        /// A handle to the window.
        /// </param>
        /// <returns>
        /// If the function is successful the style of the specified window is returned;
        /// otherwise WindowStyles.None.
        /// </returns>
        public static WindowStyles GetWindowStyle(IntPtr window)
        {
            return (WindowStyles)User32.GetWindowLong(window, WindowLongFlag.STYLE);
        }

        /// <summary>
        /// Greys out the specified menu item on the specified menu.
        /// </summary>
        /// <param name="menu">
        /// The handle to the menu.
        /// </param>
        /// <param name="item">
        /// The menu item to grey out.
        /// </param>
        public static void GreyOutMenuItem(IntPtr menu, uint item)
        {
            EnableMenuItem(menu, item, 0x00000001);
        }

        /// <summary>
        /// Calculates the intersection of two source rectangles.
        /// </summary>
        /// <param name="source1">
        /// A <see cref="System.Windows.Rect"/> structure that contains the first
        /// source rectangle.
        /// </param>
        /// <param name="source2">
        /// A <see cref="System.Windows.Rect"/> structure that contains the second
        /// source rectangle.
        /// </param>
        /// <returns>
        /// The intersection of the two specified source rectangles.
        /// </returns>
        public static Rect IntersectRect(Rect source1, Rect source2)
        {
            RECT destination = new RECT();
            RECT nativeSource1 = new RECT(source1);
            RECT nativeSource2 = new RECT(source2);

            IntersectRect(out destination, ref nativeSource1, ref nativeSource2);
            return destination.ToRect();
        }

        /// <summary>
        /// Determines whether the specified window is minimised.
        /// </summary>
        /// <param name="window">
        /// A handle to the window to be tested.
        /// </param>
        /// <returns>
        /// True if the window is iconic (minimised); otherwise, false.
        /// </returns>
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool IsIconic(IntPtr window);

        /// <summary>
        /// Determines whether the specified virtual key is currently pressed.
        /// </summary>
        /// <param name="key">
        /// The key to test.
        /// </param>
        /// <returns>
        /// True of the key is currently pressed; otherwise, false.
        /// </returns>
        public static bool IsKeyPressed(VirtualKey key)
        {
            return GetKeyState(key) < 0;
        }

        /// <summary>
        /// Determines the visibility state of the specified window.
        /// </summary>
        /// <param name="window">
        /// A handle to the window to be tested.
        /// </param>
        /// <returns>
        /// True if the window is visible; otherwise, false.
        /// </returns>
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool IsWindowVisible(IntPtr window);

        /// <summary>
        /// Determines whether a window is maximised.
        /// </summary>
        /// <param name="window">
        /// A handle to the window to be tested.
        /// </param>
        /// <returns>
        /// True if the window is zoomed (maximised); otherwise, false.
        /// </returns>
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool IsZoomed(IntPtr window);

        /// <summary>
        /// Loads the specified bitmap resource from a module's executable file.
        /// </summary>
        /// <param name="hInstance">
        /// A handle to the module of either a DLL or executable (.exe) that contains the image
        /// to be loaded.
        /// </param>
        /// <param name="uID">
        /// The image to be loaded.
        /// </param>
        /// <returns>
        /// If the function succeeds, the return value is the handle of the newly loaded image.
        /// If the function fails, the return value is NULL. To get extended error information,
        /// call GetLastError.
        /// </returns>
        [DllImport("User32.dll")]
        public static extern IntPtr LoadBitmap(IntPtr hInstance, int uID);

        /// <summary>
        /// Loads the specified icon resource from the executable (.exe) file
        /// associated with an application instance.
        /// </summary>
        /// <param name="hInstance">
        /// A handle to an instance of the module whose executable file contains the icon to be
        /// loaded.
        /// </param>
        /// <param name="uID">
        /// The name of the icon resource to be loaded.
        /// </param>
        /// <returns></returns>
        [DllImport("User32.dll")]
        public static extern IntPtr LoadIcon(IntPtr hInstance, int uID);

        /// <summary>
        /// Loads an icon, cursor, animated cursor, or bitmap.
        /// </summary>
        /// <param name="hInstance">
        /// A handle to the module of either a DLL or executable (.exe) that contains the image
        /// to be loaded.
        /// </param>
        /// <param name="uID">
        /// The image to be loaded.
        /// </param>
        /// <param name="type">
        /// The type of image to be loaded.
        /// </param>
        /// <param name="width">
        /// The width, in pixels, of the icon or cursor.
        /// </param>
        /// <param name="height">
        /// The height, in pixels, of the icon or cursor.
        /// </param>
        /// <param name="load">
        /// Load flags.
        /// </param>
        /// <returns>
        /// If the function succeeds, the return value is the handle of the newly loaded image.
        /// If the function fails, the return value is NULL. To get extended error information,
        /// call GetLastError.
        /// </returns>
        [DllImport("User32.dll")]
        public static extern IntPtr LoadImage(
            IntPtr hInstance, int uID, uint type, int width, int height, int load);

        /// <summary>
        /// Loads a string resource from the executable file associated with a specified
        /// module, copies the  string into a buffer, and appends a terminating null character.
        /// </summary>
        /// <param name="hInstance">
        /// A handle to the module of either a DLL or executable (.exe) that contains the image
        /// to be loaded.
        /// </param>
        /// <param name="uID">
        /// The identifier of the string to be loaded.
        /// </param>
        /// <param name="lpBuffer">
        /// The buffer is to receive the string. Must be of sufficient length to hold a pointer
        /// (8 bytes).
        /// </param>
        /// <param name="nBufferMax">
        /// The size of the buffer, in characters. The string is truncated and null-terminated
        /// if it is longer  than the number of characters specified. If this parameter is 0,
        /// then lpBuffer receives a read-only pointer to the resource itself.
        /// </param>
        /// <returns>
        /// If the function succeeds, the return value is the number of characters copied into
        /// the buffer, not including the terminating null character, or zero if the string
        /// resource does not exist. To get extended error information, call GetLastError.
        /// </returns>
        [DllImport("User32.dll")]
        public static extern int LoadString(
            IntPtr hInstance, int uID, StringBuilder lpBuffer, int nBufferMax);

        /// <summary>
        /// The MapVirtualKey function translates (maps) a virtual-key code into a scan code or
        /// character value, or translates a scan code into a virtual-key code.
        /// </summary>
        /// <param name="uCode">
        /// Specifies the virtual-key code or scan code for a key. How this value is
        /// interpreted depends on the value of the uMapType parameter.
        /// </param>
        /// <param name="uMapType">
        /// Specifies the translation to perform. The value of this parameter depends on the
        /// value of the uCode parameter.
        /// </param>
        /// <returns>
        /// Either a scan code, a virtual-key code, or a character value, depending on the
        /// value of uCode and uMapType. If there is no translation, the return value is zero.
        /// </returns>
        [DllImport("user32.dll")]
        public static extern int MapVirtualKey(uint uCode, MapVirtualKeyMapTypes uMapType);

        /// <summary>
        /// Displays a modal dialog box that contains a system icon, a set of buttons, and a
        /// brief application-specific message, such as status or error information. The
        /// message box returns an integer value that indicates which button the user clicked.
        /// </summary>
        /// <param name="window">
        /// A handle to the owner window of the message box to be created.
        /// </param>
        /// <param name="text">
        /// The message to be displayed.
        /// </param>
        /// <param name="caption">
        /// The dialog box title. If this parameter is Null, the default title is Error.
        /// </param>
        /// <param name="type">
        /// The contents and behavior of the dialog box.
        /// </param>
        /// <returns>
        /// A integer value indicating which button was pressed by the user.
        /// </returns>
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern int MessageBox(IntPtr window, string text, string caption, uint type);

        /// <summary>
        /// Places a message in the message queue associated with the thread that created the
        /// specified window and returns without waiting for the thread to process the message.
        /// </summary>
        /// <param name="window">
        /// A handle to the window whose window procedure is to receive the message.
        /// </param>
        /// <param name="msg">
        /// The message to be posted.
        /// </param>
        /// <param name="wordParameter">
        /// Additional word message-specific information.
        /// </param>
        /// <param name="longParameter">
        /// Additional long message-specific information.
        /// </param>
        /// <returns>
        /// If the method succeeds, the return value is true; otherwise false.
        /// </returns>
        [DllImport("user32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool PostMessage(
            IntPtr window, WindowMessage msg, IntPtr wordParameter, IntPtr longParameter);

        /// <summary>
        /// Registers a window class.
        /// </summary>
        /// <param name="windowClass">
        /// A pointer to a <see cref="WNDCLASS"/> structure.
        /// </param>
        /// <returns>
        /// If the function succeeds, the return value is a class atom that uniquely identifies
        /// the class being registered. If the function fails, the return value is zero.
        /// </returns>
        [DllImport("user32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        public static extern ushort RegisterClass(ref WNDCLASS windowClass);

        /// <summary>
        /// Defines a new window message that is guaranteed to be unique throughout the system.
        /// </summary>
        /// <param name="message">
        /// The message to be registered.
        /// </param>
        /// <returns>
        /// If the message is successfully registered, the return value is a message identifier
        /// in the range 0xC000 through 0xFFFF. If the function fails, the return value is
        /// zero.
        /// </returns>
        [DllImport("user32.dll", CharSet = CharSet.Unicode)]
        public static extern int RegisterWindowMessage(string message);

        /// <summary>
        /// Releases a device context, freeing it for use by other applications.
        /// </summary>
        /// <param name="window">
        /// A handle to the window whose device context is to be released.
        /// </param>
        /// <param name="deviceHandle">
        /// A handle to the device context to be released.
        /// </param>
        /// <returns>
        /// True if the device context was released successfully; otherwise false.
        /// </returns>
        [DllImport("User32.dll", CallingConvention = CallingConvention.StdCall)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool ReleaseDC(IntPtr window, IntPtr deviceHandle);

        /// <summary>
        /// Converts the specified screen point to client-area coordinates.
        /// </summary>
        /// <param name="window">
        /// A handle to the window whose client area will be used for the conversion.
        /// </param>
        /// <param name="point">
        /// A pointer to a POINT structure that specifies the screen coordinates to
        /// be converted.
        /// </param>
        /// <returns>
        /// True if the function is successful; otherwise, false.
        /// </returns>
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool ScreenToClient(IntPtr window, ref POINT point);

        /// <summary>
        /// Sends the specified message to a window or windows.
        /// </summary>
        /// <param name="window">
        /// A handle to the window whose window procedure will receive the message.
        /// </param>
        /// <param name="msg">
        /// The message to be sent.
        /// </param>
        /// <returns>
        /// The return value specifies the result of the message processing; it depends
        /// on the message sent.
        /// </returns>
        public static IntPtr SendMessage(IntPtr window, WindowMessage msg)
        {
            return SendMessage(window, msg, IntPtr.Zero, IntPtr.Zero);
        }

        /// <summary>
        /// Sends the specified message to a window or windows.
        /// </summary>
        /// <param name="window">
        /// A handle to the window whose window procedure will receive the message.
        /// </param>
        /// <param name="msg">
        /// The message to be sent.
        /// </param>
        /// <param name="wordParameter">
        /// Additional word message-specific information.
        /// </param>
        /// <param name="longParameter">
        /// Additional long message-specific information.
        /// </param>
        /// <returns>
        /// The return value specifies the result of the message processing; it depends
        /// on the message sent.
        /// </returns>
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr SendMessage(
            IntPtr window, WindowMessage msg, IntPtr wordParameter, IntPtr longParameter);

        /// <summary>
        /// Sends the specified message to a window or windows.
        /// </summary>
        /// <param name="window">
        /// A handle to the window whose window procedure will receive the message.
        /// </param>
        /// <param name="msg">
        /// The message to be sent.
        /// </param>
        /// <param name="wordParameter">
        /// Additional word message-specific information.
        /// </param>
        /// <param name="longParameter">
        /// Additional long message-specific information.
        /// </param>
        /// <returns>
        /// The return value specifies the result of the message processing; it depends
        /// on the message sent.
        /// </returns>
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr SendMessage(
            IntPtr window, int msg, IntPtr wordParameter, IntPtr longParameter);

        /// <summary>
        /// Sends the specified message to one or more windows.
        /// </summary>
        /// <param name="window">
        /// A handle to the window whose window procedure will receive the message.
        /// </param>
        /// <param name="msg">
        /// The message to be sent (see WindowMessage enum).
        /// </param>
        /// <param name="wordParameter">
        /// Additional word message-specific information.
        /// </param>
        /// <param name="longParameter">
        /// Additional long message-specific information.
        /// </param>
        /// <param name="flags">
        /// The behaviour of this function.
        /// </param>
        /// <param name="timeout">
        /// The duration of the time-out period, in milliseconds. If the message is a broadcast
        /// message, each window can use the full time-out period. For example, if you specify
        /// a five second time-out period and there are three top-level windows that fail to
        /// process the message, you could have up to a 15 second delay.
        /// </param>
        /// <param name="result">
        /// The result of the message processing. The value of this parameter depends on the
        /// message that is specified.
        /// </param>
        /// <returns>
        /// If the function succeeds, the return value is nonzero; otherwise zero.
        /// </returns>
        [DllImport("User32.dll")]
        public static extern int SendMessageTimeout(
            IntPtr window,
            uint msg,
            IntPtr wordParameter,
            string longParameter,
            SendMessageTimeoutFlags flags,
            uint timeout,
            out int result);

        /// <summary>
        /// Sets the keyboard focus to the specified window. The window must be attached to the
        /// calling thread's message queue.
        /// </summary>
        /// <param name="window">
        /// A handle to the window that will receive the keyboard input. If this parameter is
        /// NULL, keystrokes are ignored.
        /// </param>
        /// <returns>
        /// If the function succeeds, the return value is the handle to the window that
        /// previously had the keyboard focus; otherwise IntPtr.Zero.
        /// </returns>
        [DllImport("user32.dll")]
        public static extern IntPtr SetFocus(IntPtr window);

        /// <summary>
        /// Sets the default menu item for the specified menu.
        /// </summary>
        /// <param name="menu">
        /// A handle to the menu to set the default item for.
        /// </param>
        /// <param name="item">
        /// The identifier of new default menu item or -1 for no default item.
        /// </param>
        /// <returns>
        /// True if the method is successful; otherwise, false.
        /// </returns>
        public static bool SetMenuDefaultItem(IntPtr menu, uint item)
        {
            return SetMenuDefaultItem(menu, item, 0);
        }

        /// <summary>
        /// Changes the parent window of the specified child window.
        /// </summary>
        /// <param name="childWindow">
        /// A handle to the child window.
        /// </param>
        /// <param name="newParent">
        /// A handle to the new parent window.
        /// </param>
        /// <returns>
        /// If the function succeeds, the return value is a handle to the previous parent
        /// window; otherwise, IntPtr.Zero.
        /// </returns>
        [DllImport("user32.dll")]
        public static extern IntPtr SetParent(IntPtr childWindow, IntPtr newParent);

        /// <summary>
        /// Set the style on the specified window to the specified style.
        /// </summary>
        /// <param name="window">
        /// A handle to the window.
        /// </param>
        /// <param name="style">
        /// The style that the specified window should be set to.
        /// </param>
        /// <returns>
        /// True if successful; otherwise false.
        /// </returns>
        public static bool SetWindowExStyle(IntPtr window, WindowExStyles style)
        {
            return SetWindowLong(window, WindowLongFlag.EXSTYLE, (int)style);
        }

        /// <summary>
        /// Set the style on the specified window to the specified style.
        /// </summary>
        /// <param name="window">
        /// A handle to the window.
        /// </param>
        /// <param name="style">
        /// The style that the specified window should be set to.
        /// </param>
        /// <returns>
        /// True if successful; otherwise false.
        /// </returns>
        public static bool SetWindowExStyle(IntPtr window, int style)
        {
            return SetWindowLong(window, WindowLongFlag.EXSTYLE, style);
        }

        /// <summary>
        /// Changes an attribute of the specified window.
        /// </summary>
        /// <param name="window">
        /// A handle to the window.
        /// </param>
        /// <param name="flag">
        /// A member of the <see cref="WindowLongFlag"/> enumeration that specifies which
        /// attribute is to be set.
        /// </param>
        /// <param name="value">
        /// The new value for the specified attribute.
        /// </param>
        /// <returns>
        /// True if the function is successful; otherwise, false.
        /// </returns>
        public static bool SetWindowLong(IntPtr window, WindowLongFlag flag, int value)
        {
            return SetWindowLongPtr32(window, (int)flag, value) != 0;
        }

        /// <summary>
        /// Changes an attribute of the specified window.
        /// </summary>
        /// <param name="window">
        /// A handle to the window.
        /// </param>
        /// <param name="flag">
        /// A member of the <see cref="WindowLongFlag"/> enumeration that specifies which
        /// attribute is to be set.
        /// </param>
        /// <param name="value">
        /// The new value for the specified attribute.
        /// </param>
        /// <returns>
        /// True if the function is successful; otherwise, false.
        /// </returns>
        public static bool SetWindowLong(IntPtr window, WindowLongFlag flag, IntPtr value)
        {
            if (IntPtr.Size == 8)
            {
                return SetWindowLongPtr64(window, (int)flag, value).ToInt64() != 0;
            }

            return SetWindowLongPtr32(window, (int)flag, value.ToInt32()) != 0;
        }

        /// <summary>
        /// Changes the size, position, and Z order of a child, pop-up, or top-level window.
        /// </summary>
        /// <param name="window">
        /// The wrapper around the window.
        /// </param>
        /// <param name="flags">
        /// A combination of the members in the <see cref="WindowPosFlags"/> enumeration that
        /// define the way the sizing and positioning happens.
        /// </param>
        /// <returns>
        /// True if the function was successful; otherwise false.
        /// </returns>
        public static bool SetWindowPos(HwndWrapper window, WindowPosFlags flags)
        {
            if (window == null)
            {
                return false;
            }

            return SetWindowPos(
                window.Handle,
                IntPtr.Zero,
                window.Left,
                window.Top,
                window.Width,
                window.Height,
                flags);
        }

        /// <summary>
        /// Changes the size, position, and Z order of a child, pop-up, or top-level window.
        /// </summary>
        /// <param name="window">
        /// The handle to the window.
        /// </param>
        /// <param name="flags">
        /// A combination of the members in the <see cref="WindowPosFlags"/> enumeration that
        /// define the way the sizing and positioning happens.
        /// </param>
        /// <returns>
        /// True if the function was successful; otherwise false.
        /// </returns>
        public static bool SetWindowPos(IntPtr window, WindowPosFlags flags)
        {
            return SetWindowPos(window, IntPtr.Zero, 0, 0, 0, 0, flags);
        }

        /// <summary>
        /// Changes the size, position, and Z order of a child, pop-up, or top-level window.
        /// </summary>
        /// <param name="window">
        /// The handle to the window.
        /// </param>
        /// <param name="flags">
        /// The window sizing and positioning flags.
        /// </param>
        /// <returns>
        /// True if the function was successful; otherwise false.
        /// </returns>
        public static bool SetWindowPos(IntPtr window, int flags)
        {
            return SetWindowPos(window, IntPtr.Zero, 0, 0, 0, 0, flags);
        }

        /// <summary>
        /// Changes the size, position, and Z order of a child, pop-up, or top-level window.
        /// </summary>
        /// <param name="window">
        /// The handle to the window.
        /// </param>
        /// <param name="pos">
        /// The new position of the window.
        /// </param>
        /// <param name="flags">
        /// The window sizing and positioning flags.
        /// </param>
        /// <returns>
        /// True if the function was successful; otherwise false.
        /// </returns>
        public static bool SetWindowPos(IntPtr window, Int32Rect pos, WindowPosFlags flags)
        {
            return SetWindowPos(
                window, IntPtr.Zero, pos.X, pos.Y, pos.Width, pos.Height, flags);
        }

        /// <summary>
        /// Changes the size, position, and Z order of a child, pop-up, or top-level window.
        /// </summary>
        /// <param name="window">
        /// The handle to the window.
        /// </param>
        /// <param name="x">
        /// The new position of the left side of the window, in client coordinates.
        /// </param>
        /// <param name="y">
        /// The new position of the top of the window, in client coordinates.
        /// </param>
        /// <param name="flags">
        /// A combination of the members in the <see cref="WindowPosFlags"/> enumeration that
        /// define the way the sizing and positioning happens.
        /// </param>
        /// <returns>
        /// True if the function was successful; otherwise false.
        /// </returns>
        public static bool SetWindowPos(IntPtr window, int x, int y, WindowPosFlags flags)
        {
            return SetWindowPos(window, IntPtr.Zero, x, y, 0, 0, flags);
        }

        /// <summary>
        /// Changes the size, position, and Z order of a child, pop-up, or top-level window.
        /// </summary>
        /// <param name="window">
        /// The handle to the window.
        /// </param>
        /// <param name="position">
        /// The new position of the window, in client coordinates.
        /// </param>
        /// <param name="size">
        /// The new size of the window, in pixels.
        /// </param>
        /// <param name="flags">
        /// A combination of the members in the <see cref="WindowPosFlags"/> enumeration that
        /// define the way the sizing and positioning happens.
        /// </param>
        /// <returns>
        /// True if the function was successful; otherwise false.
        /// </returns>
        public static bool SetWindowPos(
            IntPtr window, Point position, Size size, WindowPosFlags flags)
        {
            return SetWindowPos(
                window,
                IntPtr.Zero,
                (int)position.X,
                (int)position.Y,
                (int)size.Width,
                (int)size.Height,
                flags);
        }

        /// <summary>
        /// Changes the size, position, and Z order of a child, pop-up, or top-level window.
        /// </summary>
        /// <param name="window">
        /// A handle to the window.
        /// </param>
        /// <param name="insertAfter">
        /// A handle to the window to precede the positioned window in the Z order.
        /// </param>
        /// <param name="flags">
        /// A combination of the members in the <see cref="WindowPosFlags"/> enumeration that
        /// define the way the sizing and positioning happens.
        /// </param>
        /// <returns>
        /// True if the function was successful; otherwise false.
        /// </returns>
        public static bool SetWindowPos(
            IntPtr window, IntPtr insertAfter, WindowPosFlags flags)
        {
            return SetWindowPos(window, insertAfter, 0, 0, 0, 0, flags);
        }

        /// <summary>
        /// Changes the size, position, and Z order of a child, pop-up, or
        /// top-level window.
        /// </summary>
        /// <param name="window">
        /// A handle to the window.
        /// </param>
        /// <param name="insertAfter">
        /// A handle to the window to precede the positioned window in the Z order.
        /// </param>
        /// <param name="x">
        /// The new position of the left side of the window, in client coordinates.
        /// </param>
        /// <param name="y">
        /// The new position of the top of the window, in client coordinates.
        /// </param>
        /// <param name="width">
        /// The new width of the window, in pixels.
        /// </param>
        /// <param name="height">
        /// The new height of the window, in pixels.
        /// </param>
        /// <param name="flags">
        /// The window sizing and positioning flags.
        /// </param>
        /// <returns>
        /// True if the function succeeds; otherwise false.
        /// </returns>
        [DllImport("User32", CharSet = CharSet.Auto, ExactSpelling = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool SetWindowPos(
            IntPtr window,
            IntPtr insertAfter,
            int x,
            int y,
            int width,
            int height,
            WindowPosFlags flags);

        /// <summary>
        /// Sets the window region of a window.
        /// </summary>
        /// <param name="window">
        /// A handle to the window whose window region is to be set.
        /// </param>
        /// <param name="region">
        /// A handle to a region.
        /// </param>
        /// <param name="redraw">
        /// Specifies whether the system redraws the window after setting the window region.
        /// </param>
        /// <returns>
        /// If the method succeeds, the return value is nonzero; otherwise zero.
        /// </returns>
        [DllImport("user32.dll")]
        public static extern int SetWindowRgn(
            IntPtr window, IntPtr region, [MarshalAs(UnmanagedType.Bool)] bool redraw);

        /// <summary>
        /// Changes the size of the specified window.
        /// </summary>
        /// <param name="window">
        /// The wrapper around the window.
        /// </param>
        /// <param name="width">
        /// The new width of the window, in pixels.
        /// </param>
        /// <param name="height">
        /// The new height of the window, in pixels.
        /// </param>
        /// <returns>
        /// True if the function was successful; otherwise false.
        /// </returns>
        public static bool SetWindowSize(IntPtr window, int width, int height)
        {
            if (window == null)
            {
                return false;
            }

            WindowPosFlags flags =
                WindowPosFlags.NOZORDER |
                WindowPosFlags.NOMOVE |
                WindowPosFlags.NOOWNERZORDER;

            return SetWindowPos(
                window,
                IntPtr.Zero,
                0,
                0,
                width,
                height,
                flags);
        }

        /// <summary>
        /// Set the style on the specified window to the specified style.
        /// </summary>
        /// <param name="window">
        /// A handle to the window.
        /// </param>
        /// <param name="style">
        /// The style that the specified window should be set to.
        /// </param>
        /// <returns>
        /// True if successful; otherwise false.
        /// </returns>
        public static bool SetWindowStyle(IntPtr window, WindowStyles style)
        {
            return SetWindowLong(window, WindowLongFlag.STYLE, (int)style);
        }

        /// <summary>
        /// Set the style on the specified window to the specified style.
        /// </summary>
        /// <param name="window">
        /// A handle to the window.
        /// </param>
        /// <param name="style">
        /// The style that the specified window should be set to.
        /// </param>
        /// <returns>
        /// True if successful; otherwise false.
        /// </returns>
        public static bool SetWindowStyle(IntPtr window, int style)
        {
            return SetWindowLong(window, WindowLongFlag.STYLE, style);
        }

        /// <summary>
        /// Displays a shortcut menu at the specified location and tracks the selection of
        /// items on the shortcut menu.
        /// </summary>
        /// <param name="menu">
        /// A handle to the shortcut menu to be displayed.
        /// </param>
        /// <param name="flags">
        /// Specifies function options.
        /// </param>
        /// <param name="x">
        /// The horizontal location of the shortcut menu, in screen coordinates.
        /// </param>
        /// <param name="y">
        /// The vertical location of the shortcut menu, in screen coordinates.
        /// </param>
        /// <param name="window">
        /// A handle to the window that owns the shortcut menu.
        /// </param>
        /// <returns>
        /// If the method succeeds, the return value is nonzero; otherwise zero.
        /// </returns>
        public static int TrackPopupMenuEx(
            IntPtr menu, TrackPopupMenuFlags flags, int x, int y, IntPtr window)
        {
            return TrackPopupMenuEx(menu, flags, x, y, window, IntPtr.Zero);
        }

        /// <summary>
        /// Unregisters a window class, freeing the memory required for the class.
        /// </summary>
        /// <param name="className">
        /// A null-terminated string to the class name or a class atom.
        /// </param>
        /// <param name="instance">
        /// A handle to the instance of the module that created the class.
        /// </param>
        /// <returns>
        /// True if the function succeeds; otherwise, false.
        /// </returns>
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool UnregisterClass(IntPtr className, IntPtr instance);

        /// <summary>
        /// Updates the position, size, shape, content, and translucency of a glow window.
        /// </summary>
        /// <param name="window">
        /// The glow window that needs updating.
        /// </param>
        /// <param name="screenDC">
        /// The device context for the screen.
        /// </param>
        /// <param name="windowDC">
        /// The device context for the window.
        /// </param>
        /// <param name="blendFunction">
        /// The blend structure structure that specifies the transparency value to be used when
        /// composing the layered window.
        /// </param>
        /// <returns>
        /// True if the function was successful; otherwise false.
        /// </returns>
        public static bool UpdateLayeredWindow(
            HwndWrapper window, IntPtr screenDC, IntPtr windowDC, BLENDFUNCTION blendFunction)
        {
            if (window == null)
            {
                return false;
            }

            POINT position = new POINT(window.Left, window.Top);
            SIZE size = new SIZE(window.Width, window.Height);
            POINT layerPosition = new POINT();

            return UpdateLayeredWindow(
                window.Handle,
                screenDC,
                ref position,
                ref size,
                windowDC,
                ref layerPosition,
                0,
                ref blendFunction,
                UpdateLayeredWindowFlag.ALPHA);
        }

        /// <summary>
        /// Retrieves a handle to the display monitor that contains a specified point.
        /// </summary>
        /// <param name="point">
        /// Specifies the point of interest in virtual-screen coordinates.
        /// </param>
        /// <param name="flags">
        /// Determines the function's return value if the point is not contained within
        /// any display monitor.
        /// </param>
        /// <returns>
        /// If the point is contained by a display monitor, the return value is an
        /// HMONITOR handle to that display monitor; otherwise the return value depends on
        /// the <paramref name="flags"/> parameter.
        /// </returns>
        [DllImport("user32.dll")]
        internal static extern IntPtr MonitorFromPoint(POINT point, GetMonitorFlag flags);

        /// <summary>
        /// Converts the client-area coordinates of a specified point to screen coordinates.
        /// </summary>
        /// <param name="window">
        /// A handle to the window whose client area is used for the conversion.
        /// </param>
        /// <param name="point">
        /// A POINT structure that contains the client coordinates to be converted.
        /// </param>
        /// <returns>
        /// True if the function succeeds; otherwise, false.
        /// </returns>
        [DllImport("user32.dll")]
        private static extern bool ClientToScreen(IntPtr window, ref POINT point);

        /// <summary>
        /// Enables, disables, or greys the specified menu item.
        /// </summary>
        /// <param name="menu">
        /// A handle to the menu.
        /// </param>
        /// <param name="item">
        /// The menu item to be enabled, disabled, or greyed, as determined by the
        /// <paramref name="enable"/> parameter.
        /// </param>
        /// <param name="enable">
        /// Indicate whether the menu item is enabled, disabled, or greyed.
        /// </param>
        /// <returns>
        /// True is successful; otherwise, false.
        /// </returns>
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool EnableMenuItem(IntPtr menu, uint item, uint enable);

        /// <summary>
        /// Enumerates display monitors that intersect a region formed by the intersection of
        /// a specified clipping rectangle and the visible region of a device context.
        /// </summary>
        /// <param name="handle">
        /// A handle to a display device context that defines the visible region of interest.
        /// </param>
        /// <param name="clippingRectangle">
        /// A pointer to a RECT structure that specifies a clipping rectangle.
        /// </param>
        /// <param name="method">
        /// A <see cref="EnumMonitorsDelegate"/> delegate to use as a call-back function.
        /// </param>
        /// <param name="data">
        /// Application-defined data that <see cref="EnumDisplayMonitors"/> passes directly to
        /// the specified method.
        /// </param>
        /// <returns>
        /// If the function succeeds, the return value is true; otherwise false.
        /// </returns>
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool EnumDisplayMonitors(
            IntPtr handle,
            IntPtr clippingRectangle,
            EnumMonitorsDelegate method,
            IntPtr data);

        /// <summary>
        /// Enumerates all non-child windows associated with a thread by passing the handle to
        /// each window, in turn, to an application-defined call-back function.
        /// </summary>
        /// <param name="threadId">
        /// The identifier of the thread whose windows are to be enumerated.
        /// </param>
        /// <param name="method">
        /// A pointer to an application-defined call-back function.
        /// </param>
        /// <param name="longParameter">
        /// An application-defined value to be passed to the call-back function.
        /// </param>
        /// <returns>
        /// If the call-back function returns true for all windows in the thread specified by
        /// <paramref name="threadId"/>, the return value is true. If the call-back function
        /// returns false on any enumerated window, or if there are no windows found in the
        /// thread specified by <paramref name="threadId"/>, the return value is false.
        /// </returns>
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool EnumThreadWindows(
            uint threadId, EnumWindowsProcDelegate method, IntPtr longParameter);

        /// <summary>
        /// Fills a rectangle by using the specified brush.
        /// </summary>
        /// <param name="handle">
        /// A handle to the device context.
        /// </param>
        /// <param name="rect">
        /// Contains the logical coordinates of the rectangle to be filled.
        /// </param>
        /// <param name="brush">
        /// A handle to the brush used to fill the rectangle.
        /// </param>
        /// <returns>
        /// True if successful; otherwise false.
        /// </returns>
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool FillRect(IntPtr handle, [In] ref RECT rect, IntPtr brush);

        /// <summary>
        /// Retrieves a handle to the top-level window whose class name and window name match
        /// the specified strings.
        /// </summary>
        /// <param name="className">
        /// The class name or a class atom.
        /// </param>
        /// <param name="windowName">
        /// The window name (the window's title). If this parameter is null, all window names
        /// match.
        /// </param>
        /// <returns>
        /// If the function succeeds, the return value is a handle to the window that has the
        /// specified class name and window name; otherwise IntPtr.Zero.
        /// </returns>
        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr FindWindow(string className, string windowName);

        /// <summary>
        /// Retrieves the name of the class to which the specified window belongs.
        /// </summary>
        /// <param name="window">
        /// A handle to the window and, indirectly, the class to which the window belongs.
        /// </param>
        /// <param name="className">
        /// When this method returns contains the class name of the specified window.
        /// </param>
        /// <param name="maxCount">
        /// The capacity of the <paramref name="className"/> parameter.
        /// </param>
        /// <returns>
        /// If the function succeeds, the return value is the number of characters copied to
        /// the class name; otherwise, zero.
        /// </returns>
        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        private static extern int GetClassName(
            IntPtr window, StringBuilder className, int maxCount);

        /// <summary>
        /// Retrieves the coordinates of a window's client area.
        /// </summary>
        /// <param name="window">
        /// A handle to the window.
        /// </param>
        /// <param name="rect">
        /// A pointer to a RECT structure that receives the screen coordinates of the
        /// upper-left and lower-right corners of the window.
        /// </param>
        /// <returns>
        /// If the function succeeds, the return value is nonzero; other zero.
        /// </returns>
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool GetClientRect(IntPtr window, out RECT rect);

        /// <summary>
        /// Retrieves the thread identifier of the calling thread.
        /// </summary>
        /// <returns>
        /// The return value is the thread identifier of the calling thread.
        /// </returns>
        [DllImport("kernel32.dll")]
        private static extern uint GetCurrentThreadId();

        /// <summary>
        /// Retrieves the cursor's position, in screen coordinates.
        /// </summary>
        /// <param name="point">
        /// When this method returns contains the screen coordinates of the cursor.
        /// </param>
        /// <returns>
        /// True if successful; otherwise, false.
        /// </returns>
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool GetCursorPos(ref POINT point);

        /// <summary>
        /// Retrieves information about a display monitor.
        /// </summary>
        /// <param name="monitor">
        /// A handle to the display monitor of interest.
        /// </param>
        /// <param name="info">
        /// A pointer to a MONITORINFO structure that receives information about the specified
        /// display monitor.
        /// </param>
        /// <returns>
        /// True if the function was successful; otherwise, false.
        /// </returns>
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool GetMonitorInfo(IntPtr monitor, ref MONITORINFO info);

        /// <summary>
        /// Retrieves the position of the cursor in physical coordinates.
        /// </summary>
        /// <param name="point">
        /// The position of the cursor, in physical coordinates.
        /// </param>
        /// <returns>
        /// True if successful; otherwise, false.
        /// </returns>
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool GetPhysicalCursorPos(ref POINT point);

        /// <summary>
        /// Enables the application to access the window menu.
        /// </summary>
        /// <param name="window">
        /// A handle to the window that will own a copy of the window menu.
        /// </param>
        /// <param name="revert">
        /// Determines what is returned. If false, returns a handle to the copy of the window
        /// menu currently in use. If true, resets the window menu back to the default state.
        /// </param>
        /// <returns>
        /// If the <paramref name="revert"/> parameter is false, the return value is a
        /// handle to a copy of the window menu. If the <paramref name="revert"/> parameter is
        /// true, the return value is IntPtr.Zero.
        /// </returns>
        [DllImport("user32.dll")]
        private static extern IntPtr GetSystemMenu(
            IntPtr window, [MarshalAs(UnmanagedType.Bool)] bool revert);

        /// <summary>
        /// Retrieves information about the specified window.
        /// </summary>
        /// <param name="window">
        /// A handle to the window whose information is to be retrieved.
        /// </param>
        /// <param name="info">
        /// A reference to a WINDOWINFO structure to receive the information.
        /// </param>
        /// <returns>
        /// True if the function is successful; otherwise, false.
        /// </returns>
        [return: MarshalAs(UnmanagedType.Bool)]
        [DllImport("user32.dll", SetLastError = true)]
        private static extern bool GetWindowInfo(IntPtr window, ref WINDOWINFO info);

        /// <summary>
        /// Retrieves information about the specified window. The function also retrieves
        /// the value at a specified offset into the extra window memory.
        /// </summary>
        /// <param name="window">
        /// A handle to the window and, indirectly, the class to which the window belongs.
        /// </param>
        /// <param name="index">
        /// The zero-based offset to the value to be retrieved.
        /// </param>
        /// <returns>
        /// If the function succeeds, the return value is the requested value. If the
        /// function fails, the return value is zero.
        /// </returns>
        [DllImport("User32.dll", CharSet = CharSet.Auto, EntryPoint = "GetWindowLong")]
        private static extern int GetWindowLongPtr32(IntPtr window, int index);

        /// <summary>
        /// Retrieves information about the specified window. The function also retrieves
        /// the value at a specified offset into the extra window memory.
        /// </summary>
        /// <param name="window">
        /// A handle to the window and, indirectly, the class to which the window belongs.
        /// </param>
        /// <param name="index">
        /// The zero-based offset to the value to be retrieved.
        /// </param>
        /// <returns>
        /// If the function succeeds, the return value is the requested value. If the
        /// function fails, the return value is zero.
        /// </returns>
        [DllImport("User32.dll", CharSet = CharSet.Auto, EntryPoint = "GetWindowLongPtr")]
        private static extern IntPtr GetWindowLongPtr64(IntPtr window, int index);

        /// <summary>
        /// Retrieves the show state and the restored, minimized, and maximized positions
        /// of the specified window.
        /// </summary>
        /// <param name="window">
        /// A handle to the window.
        /// </param>
        /// <param name="placement">
        /// A pointer to the WINDOWPLACEMENT structure that receives the show state and
        /// position information.
        /// </param>
        /// <returns>
        /// True if the function is successful; otherwise, false.
        /// </returns>
        [DllImport("user32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool GetWindowPlacement(
            IntPtr window, out WINDOWPLACEMENT placement);

        /// <summary>
        /// Retrieves the dimensions of the bounding rectangle of the specified window.
        /// </summary>
        /// <param name="window">
        /// A handle to the window.
        /// </param>
        /// <param name="rect">
        /// A pointer to a RECT structure that receives the screen coordinates of the
        /// upper-left and lower-right corners of the window.
        /// </param>
        /// <returns>
        /// If the function succeeds, the return value is nonzero; other zero.
        /// </returns>
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool GetWindowRect(IntPtr window, out RECT rect);

        /// <summary>
        /// Calculates the intersection of two source rectangles and places the coordinates of
        /// the intersection rectangle into the destination rectangle.
        /// </summary>
        /// <param name="destination">
        /// A pointer to the RECT structure that is to receive the intersection of the
        /// rectangles pointed to by the <paramref name="source1"/> and
        /// <paramref name="source2"/> parameters.
        /// </param>
        /// <param name="source1">
        /// A pointer to the RECT structure that contains the first source rectangle.
        /// </param>
        /// <param name="source2">
        /// A pointer to the RECT structure that contains the second source rectangle.
        /// </param>
        /// <returns>
        /// If the rectangles intersect, the return value is true; otherwise, false.
        /// </returns>
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool IntersectRect(
            out RECT destination, [In] ref RECT source1, [In] ref RECT source2);

        /// <summary>
        /// Retrieves a handle to the display monitor that has the largest area of intersection
        /// with the bounding rectangle of a specified window.
        /// </summary>
        /// <param name="window">
        /// A handle to the window of interest.
        /// </param>
        /// <param name="flags">
        /// Determines the function's return value if the window does not intersect any
        /// display monitor.
        /// </param>
        /// <returns>
        /// The handle to the display monitor that intersects with the largest area of the
        /// specified window.
        /// </returns>
        [DllImport("user32.dll")]
        private static extern IntPtr MonitorFromWindow(IntPtr window, GetMonitorFlag flags);

        /// <summary>
        /// Sets the default menu item for the specified menu.
        /// </summary>
        /// <param name="menu">
        /// A handle to the menu to set the default item for.
        /// </param>
        /// <param name="item">
        /// The identifier or position of the new default menu item or -1 for no default item.
        /// </param>
        /// <param name="usingPosition">
        /// The meaning of <paramref name="item"/>. If this parameter is false,
        /// <paramref name="item"/> is a menu item identifier; otherwise, it is a menu item
        /// position.
        /// </param>
        /// <returns>
        /// True if the method is successful; otherwise, false.
        /// </returns>
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool SetMenuDefaultItem(
            IntPtr menu, uint item, uint usingPosition);

        /// <summary>
        /// Changes an attribute of the specified window. The function also sets a value at the
        /// specified offset in the extra window memory.
        /// </summary>
        /// <param name="window">
        /// A handle to the window and, indirectly, the class to which the window belongs.
        /// </param>
        /// <param name="index">
        /// The zero-based offset to the value to be set.
        /// </param>
        /// <param name="value">
        /// The replacement value.
        /// </param>
        /// <returns>
        /// If the function succeeds, the return value is the previous value of the specified
        /// offset. If the function fails, the return value is zero.
        /// </returns>
        [DllImport("user32.dll", CharSet = CharSet.Auto, EntryPoint = "SetWindowLong")]
        private static extern int SetWindowLongPtr32(IntPtr window, int index, int value);

        /// <summary>
        /// Changes an attribute of the specified window. The function also sets a value at the
        /// specified offset in the extra window memory.
        /// </summary>
        /// <param name="window">
        /// A handle to the window and, indirectly, the class to which the window belongs.
        /// </param>
        /// <param name="index">
        /// The zero-based offset to the value to be set.
        /// </param>
        /// <param name="value">
        /// The replacement value.
        /// </param>
        /// <returns>
        /// If the function succeeds, the return value is the previous value of the specified
        /// offset. If the function fails, the return value is zero.
        /// </returns>
        [DllImport("user32.dll", CharSet = CharSet.Auto, EntryPoint = "SetWindowLongPtr")]
        private static extern IntPtr SetWindowLongPtr64(
            IntPtr window, int index, IntPtr value);

        /// <summary>
        /// Changes the size, position, and Z order of a child, pop-up, or
        /// top-level window.
        /// </summary>
        /// <param name="window">
        /// A handle to the window.
        /// </param>
        /// <param name="insertAfter">
        /// A handle to the window to precede the positioned window in the Z order.
        /// </param>
        /// <param name="x">
        /// The new position of the left side of the window, in client coordinates.
        /// </param>
        /// <param name="y">
        /// The new position of the top of the window, in client coordinates.
        /// </param>
        /// <param name="width">
        /// The new width of the window, in pixels.
        /// </param>
        /// <param name="height">
        /// The new height of the window, in pixels.
        /// </param>
        /// <param name="flags">
        /// The window sizing and positioning flags.
        /// </param>
        /// <returns>
        /// True if the function succeeds; otherwise false.
        /// </returns>
        [DllImport("User32", CharSet = CharSet.Auto, ExactSpelling = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool SetWindowPos(
            IntPtr window,
            IntPtr insertAfter,
            int x,
            int y,
            int width,
            int height,
            int flags);

        /// <summary>
        /// Displays a shortcut menu at the specified location and tracks the selection of
        /// items on the shortcut menu.
        /// </summary>
        /// <param name="menu">
        /// A handle to the shortcut menu to be displayed.
        /// </param>
        /// <param name="flags">
        /// Specifies function options.
        /// </param>
        /// <param name="x">
        /// The horizontal location of the shortcut menu, in screen coordinates.
        /// </param>
        /// <param name="y">
        /// The vertical location of the shortcut menu, in screen coordinates.
        /// </param>
        /// <param name="window">
        /// A handle to the window that owns the shortcut menu.
        /// </param>
        /// <param name="lptpm">
        /// A pointer to a TPMPARAMS structure that specifies an area of the screen the menu
        /// should not overlap.
        /// </param>
        /// <returns>
        /// If the method succeeds, the return value is nonzero; otherwise zero.
        /// </returns>
        [DllImport("user32.dll")]
        private static extern int TrackPopupMenuEx(
            IntPtr menu, TrackPopupMenuFlags flags, int x, int y, IntPtr window, IntPtr lptpm);

        /// <summary>
        /// Updates the position, size, shape, content, and translucency of a
        /// layered window.
        /// </summary>
        /// <param name="layeredWindow">
        /// A handle to a layered window.
        /// </param>
        /// <param name="destination">
        /// A handle to a DC for the screen.
        /// </param>
        /// <param name="newLocation">
        /// A pointer to a structure that specifies the new screen position of the
        /// layered window.
        /// </param>
        /// <param name="newSize">
        /// A pointer to a structure that specifies the new size of the layered window.
        /// </param>
        /// <param name="source">
        /// A handle to a DC for the surface that defines the layered window.
        /// </param>
        /// <param name="sourceLocation">
        /// A pointer to a structure that specifies the location of the layer in the
        /// device context.
        /// </param>
        /// <param name="colourKey">
        /// A structure that specifies the colour key to be used when composing the
        /// layered window.
        /// </param>
        /// <param name="blendFunction">
        /// A pointer to a structure that specifies the transparency value to be used when
        /// composing the layered window.
        /// </param>
        /// <param name="flags">
        /// A flag parameter used to specify how the transparency is applied.
        /// </param>
        /// <returns>
        /// True if the function succeed; otherwise, false.
        /// </returns>
        [DllImport("user32.dll", ExactSpelling = true, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool UpdateLayeredWindow(
            IntPtr layeredWindow,
            IntPtr destination,
            ref POINT newLocation,
            ref SIZE newSize,
            IntPtr source,
            ref POINT sourceLocation,
            uint colourKey,
            [In] ref BLENDFUNCTION blendFunction,
            UpdateLayeredWindowFlag flags);

        /// <summary>
        /// Retrieves the identifier of the thread that created the specified window and,
        /// optionally, the identifier of the process that created the window.
        /// </summary>
        /// <param name="hWnd">
        /// A handle to the window.
        /// </param>
        /// <param name="lpdwProcessId">
        /// A pointer to a variable that receives the process identifier. If this parameter
        /// is not NULL, GetWindowThreadProcessId copies the identifier of the process to
        /// the variable; otherwise, it does not.
        /// </param>
        /// <returns></returns>
        [DllImport("user32.dll", SetLastError = true)]
        public static extern uint GetWindowThreadProcessId(IntPtr hWnd, out uint lpdwProcessId);

        /// <summary>
        /// Retrieves or sets the value of one of the system-wide parameters. This
        /// function can also update the user profile while setting a parameter.
        /// </summary>
        /// <param name="uiAction">
        /// The system-wide parameter to be retrieved or set
        /// </param>
        /// <param name="uiParam">
        /// A parameter whose usage and format depends on the system parameter being
        /// queried or set. For more information about system-wide parameters, see the
        /// uiAction parameter. If not otherwise indicated, you must specify zero for
        /// this parameter.
        /// </param>
        /// <param name="pvParam">
        /// A parameter whose usage and format depends on the system parameter being
        /// queried or set. For more information about system-wide parameters, see the
        /// uiAction parameter. If not otherwise indicated, you must specify NULL for
        /// this parameter.
        /// </param>
        /// <param name="fWinIni">
        /// If a system parameter is being set, specifies whether the user profile is
        /// to be updated, and if so, whether the WM_SETTINGCHANGE message is to be
        /// broadcast to all top-level windows to notify them of the change.
        /// </param>
        /// <returns>
        /// If the function succeeds, the return value is a nonzero value.
        /// If the function fails, the return value is zero.
        /// </returns>
        [DllImport("user32.dll")]
        public static extern Int32 SystemParametersInfo(
            UInt32 uiAction,
            UInt32 uiParam,
            IntPtr pvParam,
            UInt32 fWinIni);
        #endregion Methods
    } // RSG.Interop.Microsoft.Windows.User32 {Class}
} // RSG.Interop.Microsoft.Windows {Namespace}
