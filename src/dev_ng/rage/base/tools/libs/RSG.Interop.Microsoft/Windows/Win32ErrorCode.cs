﻿//---------------------------------------------------------------------------------------------
// <copyright file="Win32ErrorCode.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    /// <summary>
    /// Defines the different win32 error codes supported.
    /// </summary>
    public enum Win32ErrorCode
    {
        #region Constants
        /// <summary>
        /// The operation was cancelled by the user.
        /// </summary>
        ERROR_CANCELLED = 1223
        #endregion
    } // RSG.Interop.Microsoft.Windows.Win32ErrorCode {Enum}
} // RSG.Interop.Microsoft.Windows {Namespace}
