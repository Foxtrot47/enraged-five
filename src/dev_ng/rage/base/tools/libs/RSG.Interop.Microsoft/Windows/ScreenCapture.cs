﻿//---------------------------------------------------------------------------------------------
// <copyright file="ScreenCapture.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System;
    using System.Windows;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Contains utility methods for capturing screen images.
    /// </summary>
    public static class ScreenCapture
    {
        /// <summary>
        /// Captures a region of a the screen, defined by the hWnd.
        /// </summary>
        /// <param name="hWnd"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns></returns>
        public static BitmapSource CaptureRegion(
            IntPtr hWnd, int x, int y, int width, int height)
        {
            IntPtr sourceDC = IntPtr.Zero;
            IntPtr targetDC = IntPtr.Zero;
            IntPtr compatibleBitmapHandle = IntPtr.Zero;
            BitmapSource bitmap = null;

            try
            {
                sourceDC = User32.GetDC(User32.GetDesktopWindow());
                targetDC = Gdi32.CreateCompatibleDC(sourceDC);

                // Create a bitmap compatible with our target DC
                compatibleBitmapHandle = Gdi32.CreateCompatibleBitmap(sourceDC, width, height);

                // Get the bitmap into the target device context
                Gdi32.SelectObject(targetDC, compatibleBitmapHandle);

                // copy from source to destination
                Gdi32.BitBlt(targetDC, 0, 0, width, height, sourceDC, x, y, TernaryRasterOperations.SRCCOPY);

                // Convert from an hBitmap to a BitmapSource.
                bitmap = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                    compatibleBitmapHandle, IntPtr.Zero, Int32Rect.Empty,
                    BitmapSizeOptions.FromEmptyOptions());
            }
            catch (Exception ex)
            {
                throw new Exception(
                    String.Format("Error capturing region {0},{1},{2},{3}",
                        x, y, width, height),
                    ex);
            }
            finally
            {
                Gdi32.DeleteObject(compatibleBitmapHandle);

                User32.ReleaseDC(IntPtr.Zero, sourceDC);
                User32.ReleaseDC(IntPtr.Zero, targetDC);
            }

            return bitmap;
        }

        /// <summary>
        /// Captures a screen shot of the entire desktop.
        /// </summary>
        /// <returns></returns>
        public static BitmapSource CaptureFullScreen()
        {
            return CaptureRegion(
                User32.GetDesktopWindow(),
                (int)SystemParameters.VirtualScreenLeft,
                (int)SystemParameters.VirtualScreenTop,
                (int)SystemParameters.VirtualScreenWidth,
                (int)SystemParameters.VirtualScreenHeight);
        }

        /// <summary>
        /// Captures a screen shot of the specified window.
        /// </summary>
        /// <param name="hWnd"></param>
        /// <returns></returns>
        public static BitmapSource CaptureWindow(IntPtr hWnd)
        {
            Int32Rect rect = GetWindowActualRect(hWnd);
            User32.SetForegroundWindow(hWnd);
            return CaptureRegion(hWnd, rect.X, rect.Y, rect.Width, rect.Height);
        }

        /// <summary>
        /// Gets the window rectangle taking into account space for the border.
        /// </summary>
        /// <param name="hWnd"></param>
        /// <returns></returns>
        private static Int32Rect GetWindowActualRect(IntPtr hWnd)
        {
            Rect windowRect = User32.GetWindowRect(hWnd);
            Rect clientRect = User32.GetClientRect(hWnd);

            int sideBorder = (int)(windowRect.Width - clientRect.Width) / 2 + 1;

            Point topLeftPoint = new Point(
                windowRect.Left - sideBorder,
                windowRect.Top - sideBorder);

            Int32Rect actualRect = new Int32Rect(
                (int)topLeftPoint.X,
                (int)topLeftPoint.Y,
                (int)windowRect.Width + sideBorder * 2,
                (int)windowRect.Height + sideBorder * 2);

            return actualRect;
        }
    }
}
