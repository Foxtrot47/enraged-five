﻿//---------------------------------------------------------------------------------------------
// <copyright file="BLENDFUNCTION.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System.Runtime.InteropServices;

    /// <summary>
    /// Controls blending by specifying the blending functions for source and
    /// destination bitmaps.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct BLENDFUNCTION
    {
        #region Fields
        /// <summary>
        /// The source blend operation.
        /// </summary>
        public byte BlendOp;

        /// <summary>
        /// Must be zero.
        /// </summary>
        public byte BlendFlags;

        /// <summary>
        /// Specifies an alpha transparency value to be used on the entire source bitmap.
        /// </summary>
        public byte SourceConstantAlpha;

        /// <summary>
        /// This member controls the way the source and destination bitmaps are interpreted.
        /// </summary>
        public byte AlphaFormat;
        #endregion Fields

        #region Methods
        /// <summary>
        /// Determines whether the specified objects are equal to each other.
        /// </summary>
        /// <param name="blend1">
        /// The first object to compare.
        /// </param>
        /// <param name="blend2">
        /// The second object to compare.
        /// </param>
        /// <returns>
        /// True if the specified objects are equal to each other; otherwise, false.
        /// </returns>
        public static bool operator ==(BLENDFUNCTION blend1, BLENDFUNCTION blend2)
        {
            return blend1.Equals(blend2);
        }

        /// <summary>
        /// Determines whether the specified objects are not equal to each other.
        /// </summary>
        /// <param name="blend1">
        /// The first object to compare.
        /// </param>
        /// <param name="blend2">
        /// The second object to compare.
        /// </param>
        /// <returns>
        /// True if the specified objects are not equal to each other; otherwise, false.
        /// </returns>
        public static bool operator !=(BLENDFUNCTION blend1, BLENDFUNCTION blend2)
        {
            return !blend1.Equals(blend2);
        }

        /// <summary>
        /// Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        /// A hash code for the current object.
        /// </returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">
        /// The object to compare with the current object.
        /// </param>
        /// <returns>
        /// True if the specified object is equal to the current object; otherwise, false.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (!(obj is BLENDFUNCTION))
            {
                return false;
            }

            return this.Equals((BLENDFUNCTION)obj);
        }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="other">
        /// The object to compare with the current object.
        /// </param>
        /// <returns>
        /// True if the specified object is equal to the current object; otherwise, false.
        /// </returns>
        public bool Equals(BLENDFUNCTION other)
        {
            return object.ReferenceEquals(this, other);
        }
        #endregion Methods
    } // RSG.Interop.Microsoft.Windows.BLENDFUNCTION {Structure}
} // RSG.Interop.Microsoft.Windows {Namespace}
