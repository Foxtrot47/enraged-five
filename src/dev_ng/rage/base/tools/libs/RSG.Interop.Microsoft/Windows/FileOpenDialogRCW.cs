﻿// --------------------------------------------------------------------------------------------
// <copyright file="FileOpenDialogRCW.cs" company="Rockstar">
//     Copyright © Rockstar Games 2012. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System.Runtime.InteropServices;

    /// <summary>
    /// Represents the runtime class wrapper for the file open dialog object.
    /// </summary>
    [ComImport,
    ClassInterface(ClassInterfaceType.None),
    TypeLibType(TypeLibTypeFlags.FCanCreate),
    Guid(ClassIDGuid.FileOpenDialog)]
    public class FileOpenDialogRCW
    {
    } // RSG.Interop.Microsoft.Windows.FileOpenDialogRCW {Class}
} // RSG.Interop.Microsoft.Windows {Namespace}
