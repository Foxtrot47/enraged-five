﻿//---------------------------------------------------------------------------------------------
// <copyright file="Shlwapi.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System;
    using System.Runtime.InteropServices;
    using System.Security;
    using System.Text;
    using System.Windows;

    /// <summary>
    /// Contains native functions that are imported from the unmanaged assembly Shlwapi.dll.
    /// </summary>
    public static class Shlwapi
    {
        #region Methods
        /// <summary>
        /// Creates a relative path from the directory path to the file path.
        /// </summary>
        /// <param name="directory">
        /// The path to the directory that starts the relative path.
        /// </param>
        /// <param name="file">
        /// The path to the file that ends the relative path.
        /// </param>
        /// <returns>
        /// The relative path from the specified directory path to the specified file path.
        /// </returns>
        public static string MakeRelativeFromDirectoryToFile(string directory, string file)
        {
            StringBuilder result = new StringBuilder(260);
            PathRelativePathTo(
                result, directory, FileAttributes.Directory, file, FileAttributes.Normal);

            string relative = result.ToString();
            if (relative.StartsWith(".\\"))
            {
                relative = relative.TrimStart(new char[] { '.', '\\' });
            }

            return relative;
        }

        /// <summary>
        /// Creates a relative path from the  to the file path to the directory path.
        /// </summary>
        /// <param name="file">
        /// The path to the file that ends the relative path.
        /// </param>
        /// <param name="directory">
        /// The path to the directory that starts the relative path.
        /// </param>
        /// <returns>
        /// The relative path from the specified directory path to the specified file path.
        /// </returns>
        public static string MakeRelativeFromFileToDirectory(string file, string directory)
        {
            StringBuilder result = new StringBuilder(260);
            PathRelativePathTo(
                result, file, FileAttributes.Normal, directory, FileAttributes.Directory);

            string relative = result.ToString();
            if (relative.StartsWith(".\\"))
            {
                relative = relative.TrimStart(new char[] { '.', '\\' });
            }

            if (String.IsNullOrEmpty(relative))
            {
                relative = directory;
            }

            return relative;
        }

        /// <summary>
        /// Creates a relative path from one file or folder to another.
        /// </summary>
        /// <param name="result">
        /// The string builder that stores the created relative path.
        /// </param>
        /// <param name="from">
        /// The string containing the start of the relative path.
        /// </param>
        /// <param name="fromAttributes">
        /// The file attributes of the from path.
        /// </param>
        /// <param name="to">
        /// The string containing the endpoint of the relative path.
        /// </param>
        /// <param name="toAttributes">
        /// The file attributes of the to path.
        /// </param>
        /// <returns>
        /// True if successful; otherwise, false.
        /// </returns>
        [DllImport("shlwapi.dll", CharSet = CharSet.Auto)]
        private static extern bool PathRelativePathTo(
            StringBuilder result,
            string from,
            FileAttributes fromAttributes,
            string to,
            FileAttributes toAttributes);
        #endregion Methods
    } // RSG.Interop.Microsoft.Windows.Shlwapi {Class}
} // RSG.Interop.Microsoft.Windows {Namespace}
