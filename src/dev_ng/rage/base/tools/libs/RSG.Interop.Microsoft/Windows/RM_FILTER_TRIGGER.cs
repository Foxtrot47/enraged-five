﻿//---------------------------------------------------------------------------------------------
// <copyright file="RM_FILTER_TRIGGER.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System;

    /// <summary>
    /// Describes the restart or shutdown actions for an application or service.
    /// </summary>
    public enum RM_FILTER_TRIGGER : uint
    {
        /// <summary>
        /// An invalid filter trigger.
        /// </summary>
        RmFilterTriggerInvalid = 0,
        
        /// <summary>
        /// Modifies the shutdown or restart actions for an application identified by its executable filename.
        /// </summary>
        RmFilterTriggerFile = 1,
        
        /// <summary>
        /// Modifies the shutdown or restart actions for an application identified by a RM_UNIQUE_PROCESS structure.
        /// </summary>
        RmFilterTriggerProcess = 2,
        
        /// <summary>
        /// Modifies the shutdown or restart actions for a service identified by a service short name.
        /// </summary>
        RmFilterTriggerService = 3
    }

} // RSG.Interop.Microsoft.Windows namespace
