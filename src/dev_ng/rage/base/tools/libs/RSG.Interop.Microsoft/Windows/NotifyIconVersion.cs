﻿//---------------------------------------------------------------------------------------------
// <copyright file="NotifyIconMessage.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System;

    /// <summary>
    /// Notify icon version that is used.
    /// </summary>
    public enum NotifyIconVersion
    {
        /// <summary>
        /// 
        /// </summary>
        Windows95 = 0x00,

        /// <summary>
        /// 
        /// </summary>
        Windows2000 = 0x03,

        /// <summary>
        /// 
        /// </summary>
        Vista = 0x04,
    }

} // RSG.Interop.Microsoft.Windows namespace
