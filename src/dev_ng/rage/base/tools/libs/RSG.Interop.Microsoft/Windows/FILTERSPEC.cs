﻿// --------------------------------------------------------------------------------------------
// <copyright file="FILTERSPEC.cs" company="Rockstar">
//     Copyright © Rockstar Games 2012. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System.Runtime.InteropServices;

    /// <summary>
    /// Used generically to filter elements.
    /// </summary>
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto, Pack = 4)]
    public struct FILTERSPEC
    {
        /// <summary>
        /// A pointer to a buffer that contains the friendly name of the filter.
        /// </summary>
        [MarshalAs(UnmanagedType.LPWStr)]
        internal string Name;

        /// <summary>
        /// A pointer to a buffer that contains the filter pattern.
        /// </summary>
        [MarshalAs(UnmanagedType.LPWStr)]
        internal string Spec;
    } // RSG.Interop.Microsoft.Windows.FILTERSPEC {Struct}
} // RSG.Interop.Microsoft.Windows {Namespace}
