﻿//---------------------------------------------------------------------------------------------
// <copyright file="UpdateLayeredWindowFlag.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    /// <summary>
    /// Defines the different ways the transparency is applied while updating a layered window.
    /// </summary>
    public enum UpdateLayeredWindowFlag : uint
    {
        /// <summary>
        /// Uses the specified blend structure as the blend function.
        /// </summary>
        ALPHA = 0x00000002,

        /// <summary>
        /// Uses the specified colour key as the transparency colour.
        /// </summary>
        COLORKEY = 0x00000001,

        /// <summary>
        /// Draws an opaque layered window.
        /// </summary>
        OPAQUE = 0x00000004
    } // RSG.Interop.Microsoft.Windows.UpdateLayeredWindowFlag {Enum}
} // RSG.Interop.Microsoft.Windows {Namespace}
