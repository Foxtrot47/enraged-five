﻿// --------------------------------------------------------------------------------------------
// <copyright file="DragDropState.cs" company="Rockstar">
//     Copyright © Rockstar Games 2015. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    /// <summary>
    /// Defines the different states a drag and drop operation can be in.
    /// </summary>
    public enum DragDropState
    {
        /// <summary>
        /// The drop operation should occur completing the drag operation.
        /// </summary>
        DROP = 0x00040100,

        /// <summary>
        /// The drag operation should be cancelled with no drop operation occurring.
        /// </summary>
        CANCEL = 0x00040101,

        /// <summary>
        /// Indicates successful completion of the method, and requests OLE to update the
        /// cursor using the OLE-provided default cursors.
        /// </summary>
        USEDEFAULTCURSORS = 0x00040102,
    } // RSG.Interop.Microsoft.Windows.DragDropState {Enum}
} // RSG.Interop.Microsoft.Windows {Namespace}
