﻿//---------------------------------------------------------------------------------------------
// <copyright file="GetWindowFlag.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    /// <summary>
    /// Defines the different window relationships that can be set on the GetWindow
    /// native methods.
    /// </summary>
    public enum GetWindowFlag : uint
    {
        /// <summary>
        /// The retrieved handle identifies the child window at the top of the Z order,
        /// if the specified window is a parent window; otherwise, the retrieved
        /// handle is NULL.
        /// </summary>
        CHILD = 5,

        /// <summary>
        /// The retrieved handle identifies the enabled popup window owned by the
        /// specified window.
        /// </summary>
        ENABLEDPOPUP = 6,

        /// <summary>
        /// The retrieved handle identifies the window of the same type that is highest in
        /// the Z order.
        /// </summary>
        HWNDFIRST = 0,

        /// <summary>
        /// The retrieved handle identifies the window of the same type that is lowest in
        /// the Z order.
        /// </summary>
        HWNDLAST = 1,

        /// <summary>
        /// The retrieved handle identifies the window below the specified window in
        /// the Z order.
        /// </summary>
        HWNDNEXT = 2,

        /// <summary>
        /// The retrieved handle identifies the window above the specified window in
        /// the Z order.
        /// </summary>
        HWNDPREV = 3,

        /// <summary>
        /// The retrieved handle identifies the specified window's owner window, if any.
        /// </summary>
        OWNER = 4,
    } // RSG.Interop.Microsoft.Windows.GetWindowFlag {Enum}
} // RSG.Interop.Microsoft.Windows {Namespace}
