﻿//---------------------------------------------------------------------------------------------
// <copyright file="Win32Errors.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    /// <summary>
    /// A helper class used to manipulate win32 error codes.
    /// </summary>
    public static class Win32Errors
    {
        #region Methods
        /// <summary>
        /// Gets a int value from the specified win32 error code.
        /// </summary>
        /// <param name="win32ErrorCode">
        /// The win32 error code to get a integer value from.
        /// </param>
        /// <returns>
        /// The integer value that represents the specified win32 error code.
        /// </returns>
        public static int HResultFromWin32(int win32ErrorCode)
        {
            if (win32ErrorCode > 0)
            {
                win32ErrorCode = win32ErrorCode & 0x0000FFFF;
                win32ErrorCode |= (7 << 16) | unchecked((int)0x80000000);
            }

            return win32ErrorCode;
        }

        /// <summary>
        /// Gets a int value from the specified win32 error code.
        /// </summary>
        /// <param name="error">
        /// The win32 error code to get a integer value from.
        /// </param>
        /// <returns>
        /// The integer value that represents the specified win32 error code.
        /// </returns>
        public static int HResultFromWin32(Win32ErrorCode error)
        {
            return HResultFromWin32((int)error);
        }

        /// <summary>
        /// Determines whether the specified result integer code matches with the specified
        /// win 32 error code.
        /// </summary>
        /// <param name="result">
        /// The result to compare.
        /// </param>
        /// <param name="win32ErrorCode">
        /// The win32 error code to compare.
        /// </param>
        /// <returns>
        /// True if the two specified values are equivalent to each other; otherwise, false.
        /// </returns>
        public static bool Matches(int result, Win32ErrorCode win32ErrorCode)
        {
            return result == HResultFromWin32(win32ErrorCode);
        }

        /// <summary>
        /// Determines whether the specified result value represents a successful operation.
        /// </summary>
        /// <param name="result">
        /// The result to test.
        /// </param>
        /// <returns>
        /// True if the specified result represents a successful operation; otherwise, false.
        /// </returns>
        public static bool Succeeded(int result)
        {
            return result >= 0;
        }

        /// <summary>
        /// Determines whether the specified result value represents a failed operation.
        /// </summary>
        /// <param name="result">
        /// The result to test.
        /// </param>
        /// <returns>
        /// True if the specified result represents a failed operation; otherwise, false.
        /// </returns>
        public static bool Failed(HRESULT result)
        {
            return (int)result < 0;
        }
        #endregion
    } // RSG.Interop.Microsoft.Windows.Win32Errors {Class}
} // RSG.Interop.Microsoft.Windows {Namespace}
