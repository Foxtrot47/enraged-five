﻿//---------------------------------------------------------------------------------------------
// <copyright file="Shell32.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Runtime.InteropServices;
    using System.Runtime.InteropServices.ComTypes;
    using System.Security;

    /// <summary>
    /// Contains native functions that are imported from the unmanaged assembly Shell32.dll.
    /// </summary>
    public static class Shell32
    {
        #region Methods
        /// <summary>
        /// Parses a command line string and returns an array of strings of the command
        /// line arguments.
        /// </summary>
        /// <param name="cmdLine">
        /// The full and complete command line to parse for individual arguments.
        /// </param>
        /// <returns>
        /// An array of individual command line arguments.
        /// </returns>
        public static string[] CommandLineToArgs(string cmdLine)
        {
            IntPtr argv = IntPtr.Zero;
            try
            {
                int numArgs = 0;
                argv = CommandLineToArgvW(cmdLine, out numArgs);
                if (argv == IntPtr.Zero)
                {
                    throw new ArgumentException(
                        "Unable to parse argument.",  new Win32Exception());
                }

                string[] result = new string[numArgs];
                for (int i = 0; i < numArgs; i++)
                {
                    int offset = i * Marshal.SizeOf(typeof(IntPtr));
                    IntPtr currArg = Marshal.ReadIntPtr(argv, offset);
                    result[i] = Marshal.PtrToStringUni(currArg);
                }

                return result;
            }
            finally
            {
                IntPtr p = Kernel32.LocalFree(argv);
                Debug.Assert(
                    p.Equals(IntPtr.Zero), "Failed to deallocate cmdLine string array.");
            }
        }

        /// <summary>
        /// Creates and initializes a shell item object representing a folder pointing to the
        /// specified path.
        /// </summary>
        /// <param name="path">
        /// The path to the folder to create.
        /// </param>
        /// <returns>
        /// A newly created shell item that represents a folder pointing to the specified path.
        /// </returns>
        public static IShellItem CreateShellItemFolder(string path)
        {
            IShellItem item;
            SHCreateItemFromParsingName(
                path, IntPtr.Zero, new Guid(InterfaceIDGuid.ShellItemId), out item);

            return item;
        }

        /// <summary>
        /// Parses a Unicode command line string and returns an array of pointers to the
        /// command line arguments, along with a count of such arguments.
        /// </summary>
        /// <param name="cmdLine">
        /// Pointer to a null-terminated Unicode string that contains the full command
        /// line. If this parameter is an empty string the function returns the path to
        /// the current executable file.
        /// </param>
        /// <param name="numArgs">
        /// Once the method returns contains the number of array elements returned.
        /// </param>
        /// <returns>
        /// A pointer to an array of LPWSTR values. If the function fails, the return value is
        /// IntPtr.Zero.
        /// </returns>
        [DllImport("shell32.dll", SetLastError = true)]
        private static extern IntPtr CommandLineToArgvW(
            [MarshalAs(UnmanagedType.LPWStr)] string cmdLine, out int numArgs);

        /// <summary>
        /// Creates and initializes a shell item object from a parsing name.
        /// </summary>
        /// <param name="path">
        /// A pointer to a display name.
        /// </param>
        /// <param name="bindContext">
        /// A pointer to a bind context used to pass parameters as inputs and outputs to the
        /// parsing function.
        /// </param>
        /// <param name="shellItem">
        /// A reference to the IID of the interface to retrieve through
        /// <paramref name="item"/>.
        /// </param>
        /// <param name="item">
        /// When this method returns successfully, contains the interface pointer requested.
        /// </param>
        [DllImport("shell32.dll", CharSet = CharSet.Unicode, PreserveSig = false)]
        private static extern void SHCreateItemFromParsingName(
            [In, MarshalAs(UnmanagedType.LPWStr)] string path,
            [In] IntPtr bindContext,
            [In, MarshalAs(UnmanagedType.LPStruct)] Guid shellItem,
            [Out, MarshalAs(UnmanagedType.Interface, IidParameterIndex = 2)]
            out IShellItem item);

        /// <summary>
        /// Sends a message to the taskbar's status area.
        /// </summary>
        /// <param name="dwMessage"></param>
        /// <param name="pnid"></param>
        /// <returns></returns>
        [DllImport("shell32.dll", CharSet = CharSet.Unicode)]
        private static extern bool Shell_NotifyIcon(
            uint dwMessage, 
            [In] ref NOTIFYICONDATA pnid);

        /// <summary>
        /// .Net-friendly wrapper around Shell_NotifyIcon Shell32 API.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static bool NotifyIcon(NotifyIconMessage message, ref NOTIFYICONDATA data)
        {
            return (Shell_NotifyIcon((uint)message, ref data));
        }

        /// <summary>
        /// .Net-friendly wrapper around Shell_NotifyIcon Shell32API for
        /// setting the Notification Icon version.  Attempts to set the
        /// highest version first.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static bool NotifyIcon_SetVersion(ref NOTIFYICONDATA data)
        {
            data.VersionOrTimeout = (int)NotifyIconVersion.Vista;
            bool status = NotifyIcon(NotifyIconMessage.SetVersion, ref data);

            if (!status)
            {
                data.VersionOrTimeout = (int)NotifyIconVersion.Windows2000;
                status = NotifyIcon(NotifyIconMessage.SetVersion, ref data);
            }
            if (!status)
            {
                data.VersionOrTimeout = (int)NotifyIconVersion.Windows95;
                status = NotifyIcon(NotifyIconMessage.SetVersion, ref data);
            }

            if (!status)
                throw (new NotSupportedException("Failed to set notify icon version."));

            return (status);
        }

        /// <summary>
        /// Creates an IEnumFORMATETC object from an array of FORMATETC structures.
        /// </summary>
        /// <param name="cfmt">
        /// The number of entries in the afmt array.
        /// </param>
        /// <param name="afmt">
        /// An array of FORMATETC structures that specifies the clipboard formats of interest.
        /// </param>
        /// <param name="ppenumFormatEtc">
        /// When this function returns successfully, receives an IEnumFORMATETC interface
        /// pointer. Receives NULL on failure.
        /// </param>
        /// <returns></returns>
        [DllImport("shell32.dll")]
        public static extern int SHCreateStdEnumFmtEtc(
            uint cfmt, FORMATETC[] afmt, out IEnumFORMATETC ppenumFormatEtc);

        /// <summary>
        /// Gets the screen coordinates of the bounding rectangle of a notification icon.
        /// </summary>
        /// <param name="identifier"></param>
        /// <param name="iconLocation"></param>
        /// <returns></returns>
        [DllImport("shell32.dll", SetLastError = true)]
        private static extern int Shell_NotifyIconGetRect(
            [In]ref NOTIFYICONIDENTIFIER identifier, 
            [Out]out RECT iconLocation);

        /// <summary>
        /// .Net-friendly wrapper around Shell_NotifyIconGetRect Shell32 API.
        /// </summary>
        /// <param name="windowHandle"></param>
        /// <param name="taskbarIconId"></param>
        /// <returns></returns>
        public static RECT GetNotifyIconRect(IntPtr windowHandle, uint taskbarIconId)
        {
            RECT rect = new RECT();
            NOTIFYICONIDENTIFIER notifyIcon = new NOTIFYICONIDENTIFIER();
            notifyIcon.cbSize = (uint)Marshal.SizeOf(notifyIcon.GetType());
            notifyIcon.hWnd = windowHandle;
            notifyIcon.uID = taskbarIconId;
            int hResult = Shell_NotifyIconGetRect(ref notifyIcon, out rect);
            if ((uint)hResult == 0x80004005)
            {
                // E_FAIL, will happen if the icon is not there.
                throw new Win32Exception(hResult, "Failed to get icon position.");
            }

            if (hResult != 0)
            {
                throw new Win32Exception(hResult);
            }

            return (rect);
        }

        /// <summary>
        /// Sends an appbar message to the system.
        /// </summary>
        /// <param name="dwMessage">
        /// Appbar message value to send.
        /// </param>
        /// <param name="data">
        /// A pointer to an APPBARDATA structure. The content of the structure on entry
        /// and on exit depends on the value set in the dwMessage parameter. See the
        /// individual message pages for specifics.
        /// </param>
        /// <returns>
        /// This function returns a message-dependent value.
        /// </returns>
        [DllImport("shell32.dll")]
        public static extern UInt32 SHAppBarMessage(UInt32 dwMessage, ref APPBARDATA data);
        #endregion Methods
    } // RSG.Interop.Microsoft.Windows.Shell32 {Class}
} // RSG.Interop.Microsoft.Windows {Namespace}
