﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;

namespace RSG.Interop.Microsoft.Windows
{
    /// <summary>
    /// Resolves the current tray position.
    /// </summary>
    public static class TrayInfo
    {
        /// <summary>
        /// Message for retrieving the bounding rectangle of the Windows taskbar.
        /// </summary>
        private const int ABM_GETTASKBARPOS = 0x00000005;

        /// <summary>
        /// Bottom edge.
        /// </summary>
        private const int ABE_BOTTOM = 3;

        /// <summary>
        /// Left edge.
        /// </summary>
        private const int ABE_LEFT = 0;

        /// <summary>
        /// Right edge.
        /// </summary>
        private const int ABE_RIGHT = 2;

        /// <summary>
        /// Top edge.
        /// </summary>
        private const int ABE_TOP = 1;

        /// <summary>
        /// Gets the position of the system tray.
        /// </summary>
        /// <returns>Tray coordinates.</returns>
        public static Point GetTrayLocation()
        {
            APPBARDATA data = GetSystemTaskBarData();
            RECT workArea = GetWorkArea(data);

            int x = 0, y = 0;
            if (data.uEdge == ABE_LEFT)
            {
                x = workArea.Left + 2;
                y = workArea.Bottom;
            }
            else if (data.uEdge == ABE_BOTTOM)
            {
                x = workArea.Right;
                y = workArea.Bottom;
            }
            else if (data.uEdge == ABE_TOP)
            {
                x = workArea.Right;
                y = workArea.Top;
            }
            else if (data.uEdge == ABE_RIGHT)
            {
                x = workArea.Right;
                y = workArea.Bottom;
            }

            return new Point { X = x, Y = y };
        }

        /// <summary>
        /// Retrieves <see cref="APPBARDATA" /> for the windows task bar.
        /// </summary>
		/// <returns>
		/// System application bar data.
		/// </returns>
        private static APPBARDATA GetSystemTaskBarData()
        {
            return GetData("Shell_TrayWnd", null);
        }

        /// <summary>
        /// Retrieves <see cref="APPBARDATA" /> for the specified class and window.
        /// </summary>
        /// <param name="strClassName">
		/// Name of the class to retrieve the data for.
		/// </param>
        /// <param name="strWindowName">
		/// Name of the window to retrieve the data for.
		/// </param>
		/// <returns>
		/// System application bar data.
		/// </returns>
        private static APPBARDATA GetData(string strClassName, string strWindowName)
        {
            APPBARDATA data = new APPBARDATA();
            data.cbSize = (UInt32)Marshal.SizeOf(data.GetType());

            IntPtr hWnd = User32.FindWindow(strClassName, strWindowName);

            if (hWnd != IntPtr.Zero)
            {
                UInt32 uResult = Shell32.SHAppBarMessage(ABM_GETTASKBARPOS, ref data);

                if (uResult != 1)
                {
                    throw new Exception("Failed to communicate with the given AppBar");
                }
            }
            else
            {
                throw new Exception("Failed to find an AppBar that matched the given criteria");
            }

            return data;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private static RECT GetWorkArea(APPBARDATA data)
        {
            Int32 bResult = 0;
            RECT rc = new RECT();
            IntPtr rawRect = Marshal.AllocHGlobal(Marshal.SizeOf(rc));
            bResult = User32.SystemParametersInfo((int)SystemParameterInfoAction.GETWORKAREA, 0, rawRect, 0);
            rc = (RECT)Marshal.PtrToStructure(rawRect, rc.GetType());

            if (bResult == 1)
            {
                Marshal.FreeHGlobal(rawRect);
                return rc;
            }

            return new RECT(0, 0, 0, 0);
        }
    }
}
