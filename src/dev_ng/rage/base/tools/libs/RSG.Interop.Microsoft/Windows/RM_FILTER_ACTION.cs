﻿//---------------------------------------------------------------------------------------------
// <copyright file="RM_FILTER_ACTION.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System;

    /// <summary>
    /// Specifies the type of modification that is applied to restart or shutdown actions.
    /// </summary>
    public enum RM_FILTER_ACTION : uint
    {
        /// <summary>
        /// An invalid filter action.
        /// </summary>
        RmInvalidFilterAction = 0,
        
        /// <summary>
        /// Prevents the restart of the specified application or service.
        /// </summary>
        RmNoRestart = 1,
        
        /// <summary>
        /// Prevents the shut down and restart of the specified application or service.
        /// </summary>
        RmNoShutdown = 2
    }

} // RSG.Interop.Microsoft.Windows namespace
