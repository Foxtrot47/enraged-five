﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace RSG.Interop.Microsoft.Windows
{
    /// <summary>
    /// The <see cref="PAINTSTRUCT"/> structure contains information for an application. This
    /// information can be used to paint the client area of a window owned by that application.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct PAINTSTRUCT
    {
        /// <summary>
        /// A handle to the display DC to be used for painting.
        /// </summary>
        public IntPtr Hdc;

        /// <summary>
        /// Indicates whether the background must be erased. This value is nonzero if the
        /// application should erase the background. The application is responsible for
        /// erasing the background if a window class is created without a background brush.
        /// For more information, see the description of the hbrBackground member of the
        /// <see cref="WNDCLASS"/> structure.
        /// </summary>
        public bool Erase;

        /// <summary>
        /// A <see cref="RECT"/> structure that specifies the upper left and lower right
        /// corners of the rectangle in which the painting is requested, in device units
        /// relative to the upper-left corner of the client area.
        /// </summary>
        public RECT Paint;

        /// <summary>
        /// Reserved; used internally by the system.
        /// </summary>
        public bool Restore;

        /// <summary>
        /// Reserved; used internally by the system.
        /// </summary>
        public bool IncUpdate;

        /// <summary>
        /// Reserved; used internally by the system.
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
        public byte[] Reserved;
    } // PAINTSTRUCT
}
