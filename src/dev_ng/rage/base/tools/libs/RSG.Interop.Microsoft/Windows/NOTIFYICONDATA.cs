﻿//---------------------------------------------------------------------------------------------
// <copyright file="NOTIFYICONDATA.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System;
    using System.Runtime.InteropServices;

    /// <summary>
    /// Contains information that the system needs to display notifications in the notification area. 
    /// </summary>
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
    public struct NOTIFYICONDATA
    {
        #region Members
        /// <summary>
        /// Size of this structure, in bytes. 
        /// </summary>
        public uint cbSize;

        /// <summary>
        /// Handle to the window that receives notification messages associated with an icon in the 
        /// taskbar status area. The Shell uses hWnd and uID to identify which icon to operate on 
        /// when Shell_NotifyIcon is invoked. 
        /// </summary>
        public IntPtr hwnd;

        /// <summary>
        /// Application-defined identifier of the taskbar icon. The Shell uses hWnd and uID to identify 
        /// which icon to operate on when Shell_NotifyIcon is invoked. You can have multiple icons 
        /// associated with a single hWnd by assigning each a different uID. 
        /// </summary>
        public int uID;

        /// <summary>
        /// Flags that indicate which of the other members contain valid data. This member can be 
        /// a combination of the NIF_XXX constants.
        /// </summary>
        public NotifyIconFlags uFlags;

        /// <summary>
        /// Application-defined message identifier. The system uses this identifier to send 
        /// notifications to the window identified in hWnd. 
        /// </summary>
        public int uCallbackMessage;

        /// <summary>
        /// Handle to the icon to be added, modified, or deleted. 
        /// </summary>
        public IntPtr Icon;

        /// <summary>
        /// String with the text for a standard ToolTip. It can have a maximum of 64 characters including 
        /// the terminating NULL. For Version 5.0 and later, szTip can have a maximum of 
        /// 128 characters, including the terminating NULL.
        /// </summary>
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
        public String ToolTipText;

        /// <summary>
        /// State of the icon. 
        /// </summary>
        public NotifyIconState IconState;

        /// <summary>
        /// A value that specifies which bits of the state member are retrieved or modified. 
        /// For example, setting this member to NIS_HIDDEN causes only the item's hidden state to be retrieved. 
        /// </summary>
        public NotifyIconState StateMask;

        /// <summary>
        /// String with the text for a balloon ToolTip. It can have a maximum of 255 characters. 
        /// To remove the ToolTip, set the NIF_INFO flag in uFlags and set szInfo to an empty string. 
        /// </summary>
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
        public String BalloonText;

        /// <summary>
        /// NOTE: This field is also used for the Timeout value. Specifies whether the Shell notify 
        /// icon interface should use Windows 95 or Windows 2000 
        /// behavior. For more information on the differences in these two behaviors, see 
        /// Shell_NotifyIcon. This member is only employed when using Shell_NotifyIcon to send an 
        /// NIM_VERSION message. 
        /// </summary>
        public int VersionOrTimeout;

        /// <summary>
        /// String containing a title for a balloon ToolTip. This title appears in boldface 
        /// above the text. It can have a maximum of 63 characters. 
        /// </summary>
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
        public String BalloonTitle;

        /// <summary>
        /// Adds an icon to a balloon ToolTip. It is placed to the left of the title. If the 
        /// szTitleInfo member is zero-length, the icon is not shown.
        /// </summary>
        public NotifyIconBalloonFlags BalloonFlags;
        
        /// <summary>
        /// A registered GUID that identifies the icon. This value overrides 
        /// uID and is the recommended method of identifying the icon. The 
        /// NIF_GUID flag must be set in the uFlags member.
        /// </summary>
        public Guid Guid;

        /// <summary>
        /// Windows Vista and later. The handle of a customized notification icon 
        /// provided by the application that should be used independently of the 
        /// notification area icon. If this member is non-NULL and the NIIF_USER 
        /// flag is set in the dwInfoFlags member, this icon is used as the 
        /// notification icon. If this member is NULL, the legacy behavior is 
        /// carried out.
        /// </summary>
        public IntPtr hBalloonIcon;        
        #endregion // Members

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="handle"></param>
        /// <returns></returns>
        public static NOTIFYICONDATA CreateDefault(IntPtr handle)
        {
            NOTIFYICONDATA data = new NOTIFYICONDATA();
            if (Environment.OSVersion.Version.Major >= 6)
            {
                // Use size defined by structure; for most recent OS versions.
                data.cbSize = (uint)Marshal.SizeOf(typeof(NOTIFYICONDATA));
            }
            else
            {
                // Set size for previous revisions of the shell API.
                data.cbSize = 952; // NOTIFYICONDATAW_V3_SIZE
                
                // Set to fixed timeout.
                data.VersionOrTimeout = 10;
            }

            data.hwnd = handle;
            data.uID = 0;
            data.uCallbackMessage = 0;
            data.VersionOrTimeout = (int)NotifyIconVersion.Windows95;
            data.Icon = IntPtr.Zero;

            // Initially hide our notify icon.
            data.IconState = NotifyIconState.Hidden;
            data.StateMask = NotifyIconState.Hidden;

            // Set flags.
            data.uFlags = NotifyIconFlags.Message | NotifyIconFlags.Icon |
                NotifyIconFlags.Tip;

            // Initialise our string data.
            data.ToolTipText = String.Empty;
            data.BalloonTitle = String.Empty;
            data.BalloonText = String.Empty;

            return (data);
        }
        #endregion // Methods
    }

} // RSG.Interop.Microsoft.Windows namespace
