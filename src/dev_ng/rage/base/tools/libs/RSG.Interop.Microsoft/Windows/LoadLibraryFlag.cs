﻿//---------------------------------------------------------------------------------------------
// <copyright file="LoadLibraryFlag.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System;

    /// <summary>
    /// Flags to define action to be taken when loading a module.
    /// </summary>
    /// http://msdn.microsoft.com/en-us/library/windows/desktop/ms684179(v=vs.85).aspx
    [Flags]
    public enum LoadLibraryFlag : uint
    {
        /// <summary>
        /// If this value is used, and the executable module is a DLL, the system does not 
        /// call DllMain for process and thread initialization and termination.
        /// </summary>
        DONT_RESOLVE_DLL_REFERENCES = 0x00000001,

        /// <summary>
        /// If this value is used, the system does not check AppLocker rules or apply 
        /// Software Restriction Policies for the DLL.
        /// </summary>
        LOAD_IGNORE_CODE_AUTHZ_LEVEL = 0x00000010,

        /// <summary>
        /// If this value is used, the system maps the file into the calling process's 
        /// virtual address space as if it were a data file.
        /// </summary>
        LOAD_LIBRARY_AS_DATAFILE = 0x00000002,

        /// <summary>
        /// Similar to LOAD_LIBRARY_AS_DATAFILE, except that the DLL file is opened 
        /// with exclusive write access for the calling process.
        /// </summary>
        LOAD_LIBRARY_AS_DATAFILE_EXCLUSIVE = 0x00000040,

        /// <summary>
        /// If this value is used, the system maps the file into the process's virtual 
        /// address space as an image file.
        /// </summary>
        LOAD_LIBRARY_AS_IMAGE_RESOURCE = 0x00000020,

        /// <summary>
        /// If this value is used, the application's installation directory is searched for 
        /// the DLL and its dependencies.
        /// </summary>
        LOAD_LIBRARY_SEARCH_APPLICATION_DIR = 0x00000200,

        /// <summary>
        /// This value is a combination of LOAD_LIBRARY_SEARCH_APPLICATION_DIR, LOAD_LIBRARY_SEARCH_SYSTEM32, 
        /// and LOAD_LIBRARY_SEARCH_USER_DIRS. 
        /// </summary>
        LOAD_LIBRARY_SEARCH_DEFAULT_DIRS = 0x00001000,

        /// <summary>
        /// If this value is used, the directory that contains the DLL is temporarily added to the beginning 
        /// of the list of directories that are searched for the DLL's dependencies.
        /// </summary>
        LOAD_LIBRARY_SEARCH_DLL_LOAD_DIR = 0x00000100,

        /// <summary>
        /// If this value is used, %windows%\system32 is searched for the DLL and its dependencies. 
        /// </summary>
        LOAD_LIBRARY_SEARCH_SYSTEM32 = 0x00000800,

        /// <summary>
        /// If this value is used, directories added using the AddDllDirectory or the SetDllDirectory function are searched for the DLL and its dependencies.
        /// </summary>
        LOAD_LIBRARY_SEARCH_USER_DIRS = 0x00000400,

        /// <summary>
        /// If this value is used and lpFileName specifies an absolute path.
        /// </summary>
        LOAD_WITH_ALTERED_SEARCH_PATH = 0x00000008
    }

} // RSG.Interop.Microsoft.Windows namespace
