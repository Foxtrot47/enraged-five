﻿// --------------------------------------------------------------------------------------------
// <copyright file="FileDescriptorFlags.cs" company="Rockstar">
//     Copyright © Rockstar Games 2015. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System;

    /// <summary>
    /// Defines the flags used to set the flags property on a file descriptor structure.
    /// </summary>
    [Flags]
    public enum FileDescriptorFlags : uint
    {
        /// <summary>
        /// The file type member is valid.
        /// </summary>
        CLSID = 0x00000001,

        /// <summary>
        /// The icon size and screen point members are valid.
        /// </summary>
        SIZEPOINT = 0x00000002,

        /// <summary>
        /// The FileAttributes member is valid.
        /// </summary>
        ATTRIBUTES = 0x00000004,

        /// <summary>
        /// The CreationTime member is valid.
        /// </summary>
        CREATETIME = 0x00000008,

        /// <summary>
        ///  The LastAccessTime member is valid.
        /// </summary>
        ACCESSTIME = 0x00000010,

        /// <summary>
        /// The LastWriteTime member is valid.
        /// </summary>
        WRITESTIME = 0x00000020,

        /// <summary>
        ///  The FileSizeHigh and FileSizeLow members are valid.
        /// </summary>
        FILESIZE = 0x00000040,

        /// <summary>
        /// A progress indicator is shown with drag-and-drop operations.
        /// </summary>
        PROGRESSUI = 0x00004000,

        /// <summary>
        /// Treat the operation as a shortcut.
        /// </summary>
        LINKUI = 0x00008000,

        /// <summary>
        /// The descriptor is Unicode.
        /// </summary>
        UNICODE = 0x80000000
    } // RSG.Interop.Microsoft.Windows.FileDescriptorFlags {Enum}
} // RSG.Interop.Microsoft.Windows {Namespace}
