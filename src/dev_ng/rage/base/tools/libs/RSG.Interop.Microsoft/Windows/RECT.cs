﻿//---------------------------------------------------------------------------------------------
// <copyright file="RECT.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System.Runtime.InteropServices;
    using System.Windows;

    /// <summary>
    /// Defines the coordinates of the upper-left and lower-right corners of a rectangle.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct RECT
    {
        #region Fields
        /// <summary>
        /// The x-coordinate of the upper-left corner of the rectangle.
        /// </summary>
        private int _left;

        /// <summary>
        /// The y-coordinate of the upper-left corner of the rectangle.
        /// </summary>
        private int _top;

        /// <summary>
        /// The x-coordinate of the lower-right corner of the rectangle.
        /// </summary>
        private int _right;

        /// <summary>
        /// The y-coordinate of the lower-right corner of the rectangle.
        /// </summary>
        private int _bottom;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="RECT"/> struct.
        /// </summary>
        /// <param name="left">
        /// The x-coordinate of the upper-left corner of the rectangle.
        /// </param>
        /// <param name="top">
        /// The y-coordinate of the upper-left corner of the rectangle.
        /// </param>
        /// <param name="right">
        /// The x-coordinate of the lower-right corner of the rectangle.
        /// </param>
        /// <param name="bottom">
        /// The y-coordinate of the lower-right corner of the rectangle.
        /// </param>
        public RECT(int left, int top, int right, int bottom)
        {
            this._left = left;
            this._top = top;
            this._right = right;
            this._bottom = bottom;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RECT"/> struct as the equivalent of
        /// the specified managed System.Windows.Rect.
        /// </summary>
        /// <param name="rect">
        /// The instance to copy.
        /// </param>
        public RECT(Rect rect)
        {
            this._left = (int)rect.Left;
            this._top = (int)rect.Top;
            this._right = (int)rect.Right;
            this._bottom = (int)rect.Bottom;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RECT"/> struct as the equivalent of
        /// the specified <see cref="WINDOWPOS"/> instance.
        /// </summary>
        /// <param name="positionInfo">
        /// The instance to copy.
        /// </param>
        public RECT(WINDOWPOS positionInfo)
        {
            this._left = (int)positionInfo.Left;
            this._top = (int)positionInfo.Top;
            this._right = this._left + (int)positionInfo.Width;
            this._bottom = this._top + (int)positionInfo.Height;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the x-coordinate of the upper-left corner of the rectangle.
        /// </summary>
        public int Left
        {
            get { return this._left; }
            set { this._left = value; }
        }

        /// <summary>
        /// Gets or sets the y-coordinate of the upper-left corner of the rectangle.
        /// </summary>
        public int Top
        {
            get { return this._top; }
            set { this._top = value; }
        }

        /// <summary>
        /// Gets or sets the x-coordinate of the lower-right corner of the rectangle.
        /// </summary>
        public int Right
        {
            get { return this._right; }
            set { this._right = value; }
        }

        /// <summary>
        /// Gets or sets the y-coordinate of the lower-right corner of the rectangle.
        /// </summary>
        public int Bottom
        {
            get { return this._bottom; }
            set { this._bottom = value; }
        }

        /// <summary>
        /// Gets a point that represents the top left position of the rectangle.
        /// </summary>
        public Point Position
        {
            get { return new Point((double)this._left, (double)this._top); }
        }

        /// <summary>
        /// Gets the size of the rectangle.
        /// </summary>
        public Size Size
        {
            get { return new Size((double)this.Width, (double)this.Height); }
        }

        /// <summary>
        /// Gets or sets the height of the rectangle.
        /// </summary>
        public int Height
        {
            get { return this._bottom - this._top; }
            set { this._bottom = this._top + value; }
        }

        /// <summary>
        /// Gets or sets the width of the rectangle.
        /// </summary>
        public int Width
        {
            get { return this._right - this._left; }
            set { this._right = this._left + value; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Determines whether the specified objects are equal to each other.
        /// </summary>
        /// <param name="rect1">
        /// The first object to compare.
        /// </param>
        /// <param name="rect2">
        /// The second object to compare.
        /// </param>
        /// <returns>
        /// True if the specified objects are equal to each other; otherwise, false.
        /// </returns>
        public static bool operator ==(RECT rect1, RECT rect2)
        {
            return rect1.Equals(rect2);
        }

        /// <summary>
        /// Determines whether the specified objects are not equal to each other.
        /// </summary>
        /// <param name="rect1">
        /// The first object to compare.
        /// </param>
        /// <param name="rect2">
        /// The second object to compare.
        /// </param>
        /// <returns>
        /// True if the specified objects are not equal to each other; otherwise, false.
        /// </returns>
        public static bool operator !=(RECT rect1, RECT rect2)
        {
            return !rect1.Equals(rect2);
        }

        /// <summary>
        /// Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        /// A hash code for the current object.
        /// </returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">
        /// The object to compare with the current object.
        /// </param>
        /// <returns>
        /// True if the specified object is equal to the current object; otherwise, false.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (!(obj is RECT))
            {
                return false;
            }

            return this.Equals((RECT)obj);
        }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="other">
        /// The object to compare with the current object.
        /// </param>
        /// <returns>
        /// True if the specified object is equal to the current object; otherwise, false.
        /// </returns>
        public bool Equals(RECT other)
        {
            if (this._top != other._top)
            {
                return false;
            }

            if (this._bottom != other._bottom)
            {
                return false;
            }

            if (this._left != other._left)
            {
                return false;
            }

            return this._right == other._right;
        }

        /// <summary>
        /// Offsets this rectangle by the given delta values.
        /// </summary>
        /// <param name="deltaX">
        /// The amount to offset the rectangle along the x-axis.
        /// </param>
        /// <param name="deltaY">
        /// The amount to offset the rectangle along the y-axis.
        /// </param>
        public void Offset(int deltaX, int deltaY)
        {
            this._left += deltaX;
            this._right += deltaX;
            this._top += deltaY;
            this._bottom += deltaY;
        }

        /// <summary>
        /// Gets a System.Windows.Int32Rect object whose dimensions and position is the same as
        /// this rectangle.
        /// </summary>
        /// <returns>
        /// A System.Windows.Int32Rect object whose dimensions and position is the same as this
        /// rectangle.
        /// </returns>
        public Int32Rect ToInt32Rect()
        {
            return new Int32Rect(this._left, this._top, this.Width, this.Height);
        }

        /// <summary>
        /// Gets a System.Windows.Rect object whose dimensions and position is the same as this
        /// rectangle.
        /// </summary>
        /// <returns>
        /// A System.Windows.Rect object whose dimensions and position is the same as this
        /// rectangle.
        /// </returns>
        public Rect ToRect()
        {
            return new Rect(this._left, this._top, this.Width, this.Height);
        }
        #endregion Methods
    } // RSG.Interop.Microsoft.Windows.RECT {Structure}
} // RSG.Interop.Microsoft.Windows {Namespace}
