﻿//---------------------------------------------------------------------------------------------
// <copyright file="WINDOWINFO.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System.Runtime.InteropServices;

    /// <summary>
    /// Contains window information.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct WINDOWINFO
    {
        #region Fields
        /// <summary>
        /// The size of the structure, in bytes.
        /// </summary>
        public uint Size;

        /// <summary>
        /// The coordinates of the window.
        /// </summary>
        public RECT Window;

        /// <summary>
        /// The coordinates of the client area.
        /// </summary>
        public RECT Client;

        /// <summary>
        /// The window styles.
        /// </summary>
        public uint Style;

        /// <summary>
        /// The extended window styles.
        /// </summary>
        public uint ExStyle;

        /// <summary>
        /// The window status.
        /// </summary>
        public uint WindowStatus;

        /// <summary>
        /// The width of the window border, in pixels.
        /// </summary>
        public uint BorderWidth;

        /// <summary>
        /// The height of the window border, in pixels.
        /// </summary>
        public uint BorderHeight;

        /// <summary>
        /// The window class atom.
        /// </summary>
        public ushort WindowType;

        /// <summary>
        /// The Windows version of the application that created the window.
        /// </summary>
        public ushort CreatorVersion;
        #endregion Fields

        #region Methods
        /// <summary>
        /// Determines whether the specified objects are equal to each other.
        /// </summary>
        /// <param name="info1">
        /// The first object to compare.
        /// </param>
        /// <param name="info2">
        /// The second object to compare.
        /// </param>
        /// <returns>
        /// True if the specified objects are equal to each other; otherwise, false.
        /// </returns>
        public static bool operator ==(WINDOWINFO info1, WINDOWINFO info2)
        {
            return info1.Equals(info2);
        }

        /// <summary>
        /// Determines whether the specified objects are not equal to each other.
        /// </summary>
        /// <param name="info1">
        /// The first object to compare.
        /// </param>
        /// <param name="info2">
        /// The second object to compare.
        /// </param>
        /// <returns>
        /// True if the specified objects are not equal to each other; otherwise, false.
        /// </returns>
        public static bool operator !=(WINDOWINFO info1, WINDOWINFO info2)
        {
            return !info1.Equals(info2);
        }

        /// <summary>
        /// Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        /// A hash code for the current object.
        /// </returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">
        /// The object to compare with the current object.
        /// </param>
        /// <returns>
        /// True if the specified object is equal to the current object; otherwise, false.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (!(obj is WINDOWINFO))
            {
                return false;
            }

            return this.Equals((WINDOWINFO)obj);
        }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="other">
        /// The object to compare with the current object.
        /// </param>
        /// <returns>
        /// True if the specified object is equal to the current object; otherwise, false.
        /// </returns>
        public bool Equals(WINDOWINFO other)
        {
            return object.ReferenceEquals(this, other);
        }
        #endregion Methods
    } // RSG.Interop.Microsoft.Windows.WINDOWINFO {Structure}
} // RSG.Interop.Microsoft.Windows {Namespace}
