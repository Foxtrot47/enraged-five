﻿//---------------------------------------------------------------------------------------------
// <copyright file="POINT.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System.Windows;

    /// <summary>
    /// Defines the x- and y- coordinates of a point.
    /// </summary>
    public struct POINT
    {
        #region Fields
        /// <summary>
        /// The x-coordinate of the point.
        /// </summary>
        public int X;

        /// <summary>
        /// The y-coordinate of the point.
        /// </summary>
        public int Y;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="POINT"/> struct.
        /// </summary>
        /// <param name="x">
        /// The x-coordinate of the point.
        /// </param>
        /// <param name="y">
        /// The y-coordinate of the point.
        /// </param>
        public POINT(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="POINT"/> struct as the equivalent of
        /// the specified managed System.Windows.Point.
        /// </summary>
        /// <param name="point">
        /// The instance to copy.
        /// </param>
        public POINT(Point point)
        {
            this.X = (int)point.X;
            this.Y = (int)point.Y;
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Determines whether the specified objects are equal to each other.
        /// </summary>
        /// <param name="point1">
        /// The first object to compare.
        /// </param>
        /// <param name="point2">
        /// The second object to compare.
        /// </param>
        /// <returns>
        /// True if the specified objects are equal to each other; otherwise, false.
        /// </returns>
        public static bool operator ==(POINT point1, POINT point2)
        {
            return point1.Equals(point2);
        }

        /// <summary>
        /// Determines whether the specified objects are not equal to each other.
        /// </summary>
        /// <param name="point1">
        /// The first object to compare.
        /// </param>
        /// <param name="point2">
        /// The second object to compare.
        /// </param>
        /// <returns>
        /// True if the specified objects are not equal to each other; otherwise, false.
        /// </returns>
        public static bool operator !=(POINT point1, POINT point2)
        {
            return !point1.Equals(point2);
        }

        /// <summary>
        /// Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        /// A hash code for the current object.
        /// </returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">
        /// The object to compare with the current object.
        /// </param>
        /// <returns>
        /// True if the specified object is equal to the current object; otherwise, false.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (!(obj is POINT))
            {
                return false;
            }

            return this.Equals((POINT)obj);
        }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="other">
        /// The object to compare with the current object.
        /// </param>
        /// <returns>
        /// True if the specified object is equal to the current object; otherwise, false.
        /// </returns>
        public bool Equals(POINT other)
        {
            return object.ReferenceEquals(this, other);
        }
        #endregion Methods
    } // RSG.Interop.Microsoft.Windows.POINT {Structure}
} // RSG.Interop.Microsoft.Windows {Namespace}
