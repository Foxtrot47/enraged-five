﻿//---------------------------------------------------------------------------------------------
// <copyright file="BITMAPINFO.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System;
    using System.Runtime.InteropServices;

    /// <summary>
    /// 
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct NOTIFYICONIDENTIFIER
    {
        /// <summary>
        /// The size of this structure, in bytes.
        /// </summary>
        public uint cbSize;

        /// <summary>
        /// A handle to the parent window used by the notification's callback function.
        /// </summary>
        public IntPtr hWnd;

        /// <summary>
        /// The application-defined identifier of the notification icon.
        /// </summary>
        public uint uID;

        /// <summary>
        /// A registered GUID that identifies the icon.
        /// </summary>
        public Guid guidItem;
    }

} // RSG.Interop.Microsoft.Windows namespace
