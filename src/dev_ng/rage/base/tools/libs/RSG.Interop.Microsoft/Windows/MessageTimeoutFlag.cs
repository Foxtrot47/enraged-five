﻿//---------------------------------------------------------------------------------------------
// <copyright file="MessageTimeoutFlag.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System;

    /// <summary>
    /// Defines Send Message Timeout flag constants.
    /// </summary>
    public enum MessageTimeoutFlag : uint
    {
        /// <summary>
        /// The calling thread is not prevented from processing other requests while
        /// waiting for the function to return.
        /// </summary>
        NORMAL = 0x0000,

        /// <summary>
        /// Prevents the calling thread from processing any other requests until the
        /// function returns.
        /// </summary>
        BLOCK = 0x0001,

        /// <summary>
        /// The function returns without waiting for the time-out period to elapse
        /// if the receiving thread appears to not respond or "hangs".
        /// </summary>
        ABORTIFHUNG = 0x0002,

        /// <summary>
        /// The function does not enforce the time-out period as long as the receiving
        /// thread is processing messages.
        /// </summary>
        NOTIMEOUTIFNOTHUNG = 0x0008,

        /// <summary>
        /// The function should return 0 if the receiving window is destroyed or its
        /// owning thread dies while the message is being processed.
        /// </summary>
        ERRORONEXIT = 0x0020
    } // RSG.Interop.Microsoft.Windows.MessageTimeout {Enum}
} // RSG.Interop.Microsoft.Windows {Namespace}
