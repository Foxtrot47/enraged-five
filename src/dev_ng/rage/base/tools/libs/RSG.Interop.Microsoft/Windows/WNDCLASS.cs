﻿//---------------------------------------------------------------------------------------------
// <copyright file="WNDCLASS.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System;
    using System.Runtime.InteropServices;

    /// <summary>
    /// Contains the window class attributes that can be registered using native methods.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct WNDCLASS
    {
        #region Fields
        /// <summary>
        /// The class style(s).
        /// </summary>
        public ClassStyles Style;

        /// <summary>
        /// A pointer to the window procedure.
        /// </summary>
        [MarshalAs(UnmanagedType.FunctionPtr)]
        public Delegate WndProc;

        /// <summary>
        /// The number of extra bytes to allocate following the window-class structure.
        /// </summary>
        public int ClassExtraBytes;

        /// <summary>
        /// The number of extra bytes to allocate following the window instance.
        /// </summary>
        public int WindowExtraBytes;

        /// <summary>
        /// A handle to the instance that contains the window procedure for the class.
        /// </summary>
        public IntPtr Instance;

        /// <summary>
        /// A handle to the class icon. This member must be a handle to an icon resource. If
        /// this member is NULL, the system provides a default icon.
        /// </summary>
        public IntPtr Icon;

        /// <summary>
        /// A handle to the class cursor. This member must be a handle to a cursor resource.
        /// If this member is NULL, an application must explicitly set the cursor shape
        /// whenever the mouse moves into the application's window.
        /// </summary>
        public IntPtr Cursor;

        /// <summary>
        /// A handle to the class background brush. This member can be a handle to the
        /// physical brush to be used for painting the background, or it can be a colour value.
        /// </summary>
        public IntPtr Background;

        /// <summary>
        /// The resource name of the class menu, as the name appears in the resource file.
        /// </summary>
        [MarshalAs(UnmanagedType.LPWStr)]
        public string MenuName;

        /// <summary>
        /// A pointer to a null-terminated string or is an atom.
        /// </summary>
        [MarshalAs(UnmanagedType.LPWStr)]
        public string ClassName;
        #endregion Fields

        #region Methods
        /// <summary>
        /// Determines whether the specified objects are equal to each other.
        /// </summary>
        /// <param name="class1">
        /// The first object to compare.
        /// </param>
        /// <param name="class2">
        /// The second object to compare.
        /// </param>
        /// <returns>
        /// True if the specified objects are equal to each other; otherwise, false.
        /// </returns>
        public static bool operator ==(WNDCLASS class1, WNDCLASS class2)
        {
            return class1.Equals(class2);
        }

        /// <summary>
        /// Determines whether the specified objects are not equal to each other.
        /// </summary>
        /// <param name="class1">
        /// The first object to compare.
        /// </param>
        /// <param name="class2">
        /// The second object to compare.
        /// </param>
        /// <returns>
        /// True if the specified objects are not equal to each other; otherwise, false.
        /// </returns>
        public static bool operator !=(WNDCLASS class1, WNDCLASS class2)
        {
            return !class1.Equals(class2);
        }

        /// <summary>
        /// Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        /// A hash code for the current object.
        /// </returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">
        /// The object to compare with the current object.
        /// </param>
        /// <returns>
        /// True if the specified object is equal to the current object; otherwise, false.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (!(obj is WNDCLASS))
            {
                return false;
            }

            return this.Equals((WNDCLASS)obj);
        }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="other">
        /// The object to compare with the current object.
        /// </param>
        /// <returns>
        /// True if the specified object is equal to the current object; otherwise, false.
        /// </returns>
        public bool Equals(WNDCLASS other)
        {
            return object.ReferenceEquals(this, other);
        }
        #endregion Methods
    } // RSG.Interop.Microsoft.Windows.WNDCLASS {Structure}
} // RSG.Interop.Microsoft.Windows {Namespace}
