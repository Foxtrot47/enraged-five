﻿//---------------------------------------------------------------------------------------------
// <copyright file="GetDisplayNameFlag.cs" company="Rockstar">
//     Copyright © Rockstar Games 2012. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    /// <summary>
    /// Defines the different formats a display name for a shell item can be requested.
    /// </summary>
    public enum GetDisplayNameFlag : uint
    {
        /// <summary>
        /// Returns the display name relative to the parent folder.
        /// </summary>
        NORMALDISPLAY = 0x00000000,

        /// <summary>
        /// Returns the parsing name relative to the parent folder.
        /// </summary>
        PARENTRELATIVEPARSING = 0x80018001,

        /// <summary>
        /// Returns the parsing name relative to the desktop.
        /// </summary>
        DESKTOPABSOLUTEPARSING = 0x80028000,

        /// <summary>
        /// Returns the editing name relative to the parent folder.
        /// </summary>
        PARENTRELATIVEEDITING = 0x80031001,

        /// <summary>
        /// Returns the editing name relative to the desktop.
        /// </summary>
        DESKTOPABSOLUTEEDITING = 0x8004c000,

        /// <summary>
        /// Returns the item's file system path, if it has one.
        /// </summary>
        FILESYSPATH = 0x80058000,

        /// <summary>
        /// Returns the item's URL, if it has one.
        /// </summary>
        URL = 0x80068000,

        /// <summary>
        /// Returns the path relative to the parent folder in a friendly format as displayed in
        /// an address bar.
        /// </summary>
        PARENTRELATIVEFORADDRESSBAR = 0x8007c001,

        /// <summary>
        /// Returns the path relative to the parent folder.
        /// </summary>
        PARENTRELATIVE = 0x80080001
    } // RSG.Interop.Microsoft.Windows.GetDisplayNameFlag {Enum}
} // RSG.Interop.Microsoft.Windows {Namespace}
