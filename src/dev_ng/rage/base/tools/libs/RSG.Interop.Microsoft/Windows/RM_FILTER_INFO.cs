﻿//---------------------------------------------------------------------------------------------
// <copyright file="RM_FILTER_INFO.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System;
    using System.Runtime.InteropServices;
    using Com = System.Runtime.InteropServices.ComTypes;

    /// <summary>
    /// Contains information about modifications to restart or shutdown actions.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct RM_FILTER_INFO
    {
        /// <summary>
        /// This member contains a RM_FILTER_ACTION enumeration value. Use the value RmNoRestart 
        /// to prevent the restart of the application or service. Use the value RmNoShutdown to 
        /// prevent the shutdown and restart of the application or service.
        /// </summary>
        RM_FILTER_ACTION FilterAction;
        
        /// <summary>
        /// This member contains a RM_FILTER_TRIGGER enumeration value. Use the value RmFilterTriggerFile 
        /// to modify the restart or shutdown actions of an application referenced by the executable's 
        /// full path filename. Use the value RmFilterTriggerProcess to modify the restart or shutdown 
        /// actions of an application referenced by a RM_UNIQUE_PROCESS structure. Use the value 
        /// RmFilterTriggerService to modify the restart or shutdown actions of a service referenced 
        /// by the short service name.
        /// </summary>
        RM_FILTER_TRIGGER FilterTrigger;

        /// <summary>
        /// The offset in bytes to the next structure.
        /// </summary>
        int cbNextOffset;

        /// <summary>
        /// If the value of FilterTrigger is RmFilterTriggerFile, this member contains a pointer 
        /// to a string value that contains the application filename.
        /// </summary>
        String strFilename;

        /// <summary>
        /// If the value of FilterTrigger is RmFilterTriggerProcess, this member is a RM_PROCESS_INFO 
        /// structure for the application.
        /// </summary>
        RM_UNIQUE_PROCESS Process;

        /// <summary>
        /// If the value of FilterTrigger is RmFilterTriggerService this member is a pointer to a 
        /// string value that contains the short service name.
        /// </summary>
        String strServiceShortName;
    }

} // RSG.Interop.Microsoft.Windows namespace
