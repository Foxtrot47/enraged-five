﻿// --------------------------------------------------------------------------------------------
// <copyright file="IFileDialogEvents.cs" company="Rockstar">
//     Copyright © Rockstar Games 2012. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    /// <summary>
    /// Exposes methods that allow notification of events within a common file dialog.
    /// </summary>
    [ComImport,
    Guid(InterfaceIDGuid.FileDialogEventsId),
    InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IFileDialogEvents
    {
        #region Methods
        /// <summary>
        /// Called just before the dialog is about to return with a result.
        /// </summary>
        /// <param name="dialog">
        /// The reference to the dialog that is pushing the event.
        /// </param>
        /// <returns>
        /// Implementations should return S_OK to accept the current result in the dialog or
        /// S_FALSE to refuse it.
        /// </returns>
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime),
        PreserveSig]
        HRESULT OnFileOk([In, MarshalAs(UnmanagedType.Interface)] IFileDialog dialog);

        /// <summary>
        /// Called when the user is about to navigate to a new folder.
        /// </summary>
        /// <param name="dialog">
        /// The reference to the dialog that is pushing the event.
        /// </param>
        /// <param name="folder">
        /// The shell item that represents the folder that is being navigated to.
        /// </param>
        /// <returns>
        /// Returns S_OK if successful, or an error value otherwise. A return value of S_OK or
        /// E_NOTIMPL indicates that the folder change can proceed.
        /// </returns>
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime),
        PreserveSig]
        HRESULT OnFolderChanging(
            [In, MarshalAs(UnmanagedType.Interface)] IFileDialog dialog,
            [In, MarshalAs(UnmanagedType.Interface)] IShellItem folder);

        /// <summary>
        /// Called when the user navigates to a new folder.
        /// </summary>
        /// <param name="dialog">
        /// The reference to the dialog that is pushing the event.
        /// </param>
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void OnFolderChange([In, MarshalAs(UnmanagedType.Interface)] IFileDialog dialog);

        /// <summary>
        /// Called when the user changes the selection in the dialog's view.
        /// </summary>
        /// <param name="dialog">
        /// The reference to the dialog that is pushing the event.
        /// </param>
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void OnSelectionChange([In, MarshalAs(UnmanagedType.Interface)] IFileDialog dialog);

        /// <summary>
        /// Called when the dialog is opened to notify the application of the initial chosen
        /// file type.
        /// </summary>
        /// <param name="dialog">
        /// The reference to the dialog that is pushing the event.
        /// </param>
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void OnTypeChange([In, MarshalAs(UnmanagedType.Interface)] IFileDialog dialog);
        #endregion
    } // RSG.Interop.Microsoft.Windows.IFileDialogEvents {Interface}
} // RSG.Interop.Microsoft.Windows {Namespace}
