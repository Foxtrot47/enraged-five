﻿//---------------------------------------------------------------------------------------------
// <copyright file="IDropSource.cs" company="Rockstar">
//     Copyright © Rockstar Games 2015. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System.Runtime.InteropServices;

    /// <summary>
    /// The IDropSource interface is one of the interfaces you implement to provide
    /// drag-and-drop operations in your application.
    /// </summary>
    [ComImport]
    [Guid("00000121-0000-0000-C000-000000000046")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IDropSource
    {
        #region Methods
        /// <summary>
        /// Determines whether a drag-and-drop operation should be continued, cancelled, or
        /// completed.
        /// </summary>
        /// <param name="escapePressed">
        /// Indicates whether the Esc key has been pressed since the previous call.
        /// </param>
        /// <param name="keyState">
        /// The current state of the keyboard modifier keys on the keyboard.
        /// </param>
        /// <returns>
        /// A value indicating whether to continue, drop, or cancel.
        /// </returns>
        [PreserveSig]
        int QueryContinueDrag(int escapePressed, uint keyState);

        /// <summary>
        /// Enables a source application to give visual feedback to the end user during a
        /// drag-and-drop operation.
        /// </summary>
        /// <param name="effect">
        /// The effect to use.
        /// </param>
        /// <returns>
        /// Zero if successful; otherwise, non-zero.
        /// </returns>
        [PreserveSig]
        int GiveFeedback(uint effect);
        #endregion Methods
    } // RSG.Interop.Microsoft.Windows.IDropSource {Interface}
} // RSG.Interop.Microsoft.Windows {Namespace}
