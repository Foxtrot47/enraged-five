﻿// --------------------------------------------------------------------------------------------
// <copyright file="InterfaceIDGuid.cs" company="Rockstar">
//     Copyright © Rockstar Games 2012. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    /// <summary>
    /// Contains the global unique identifiers used to import native interfaces through COM.
    /// </summary>
    internal class InterfaceIDGuid
    {
        #region Fields
        /// <summary>
        /// The global unique identifier for the IFileDialogEvents interface.
        /// </summary>
        internal const string FileDialogEventsId = "973510DB-7D7F-452B-8975-74A85828D354";

        /// <summary>
        /// The global unique identifier for the IFileDialog interface.
        /// </summary>
        internal const string FileDialogId = "42f85136-db7e-439c-85f1-e4075d135fc8";

        /// <summary>
        /// The global unique identifier for the IFileOpenDialog interface.
        /// </summary>
        internal const string FileOpenDialogId = "d57c7288-d4ad-4768-be02-9d969532d960";

        /// <summary>
        /// The global unique identifier for the IModelWindow interface.
        /// </summary>
        internal const string ModalWindowId = "b4db1657-70d7-485e-8e3e-6fcb5a5c1802";

        /// <summary>
        /// The global unique identifier for the IShellItemArray interface.
        /// </summary>
        internal const string ShellItemArrayId = "B63EA76D-1F85-456F-A19C-48159EFA858B";

        /// <summary>
        /// The global unique identifier for the IShellItem interface.
        /// </summary>
        internal const string ShellItemId = "43826D1E-E718-42EE-BC55-A1E261C37BFE";
        #endregion
    } // RSG.Interop.Microsoft.Windows.InterfaceIDGuid {Class}
} // RSG.Interop.Microsoft.Windows {Namespace}
