﻿// --------------------------------------------------------------------------------------------
// <copyright file="IModalWindow.cs" company="Rockstar">
//     Copyright © Rockstar Games 2012. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    /// <summary>
    /// Exposes a method that represents a modal window.
    /// </summary>
    [ComImport,
    Guid(InterfaceIDGuid.ModalWindowId),
    InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IModalWindow
    {
        #region Methods
        /// <summary>
        /// Launches the modal window.
        /// </summary>
        /// <param name="parent">
        /// The handle of the owner window. This value can be NULL.
        /// </param>
        /// <returns>
        /// If the method succeeds, it returns S_OK. Otherwise, it returns an HRESULT error
        /// code including ERROR_CANCELLED.
        /// </returns>
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime),
        PreserveSig]
        int Show([In] IntPtr parent);
        #endregion
    } // RSG.Interop.Microsoft.Windows.IModalWindow {Interface}
} // RSG.Interop.Microsoft.Windows {Namespace}
