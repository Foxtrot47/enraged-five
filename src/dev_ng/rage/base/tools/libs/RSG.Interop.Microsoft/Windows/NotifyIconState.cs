﻿//---------------------------------------------------------------------------------------------
// <copyright file="NotifyIconState.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{

    /// <summary>
    /// State of the tray notify icon; allows hiding the icon.
    /// </summary>
    public enum NotifyIconState : int
    {
        /// <summary>
        /// Show the icon.
        /// </summary>
        Visible = 0x00,

        /// <summary>
        /// Hide the icon.
        /// </summary>
        Hidden = 0x01,
    }

} // RSG.Interop.Microsoft.Windows namespace
