﻿//---------------------------------------------------------------------------------------------
// <copyright file="SIZE.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System.Windows;

    /// <summary>
    /// Defines the specifies the width and height of a rectangle.
    /// </summary>
    public struct SIZE
    {
        #region Fields
        /// <summary>
        /// The width of the rectangle.
        /// </summary>
        public int X;

        /// <summary>
        /// The height of the rectangle.
        /// </summary>
        public int Y;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="SIZE"/> struct.
        /// </summary>
        /// <param name="width">
        /// The width of the rectangle.
        /// </param>
        /// <param name="height">
        /// The height of the rectangle.
        /// </param>
        public SIZE(int width, int height)
        {
            this.X = width;
            this.Y = height;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="SIZE"/> struct as the equivalent of
        /// the specified managed System.Windows.Size.
        /// </summary>
        /// <param name="size">
        /// The instance to copy.
        /// </param>
        public SIZE(Size size)
        {
            this.X = (int)size.Width;
            this.Y = (int)size.Height;
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Determines whether the specified objects are equal to each other.
        /// </summary>
        /// <param name="size1">
        /// The first object to compare.
        /// </param>
        /// <param name="size2">
        /// The second object to compare.
        /// </param>
        /// <returns>
        /// True if the specified objects are equal to each other; otherwise, false.
        /// </returns>
        public static bool operator ==(SIZE size1, SIZE size2)
        {
            return size1.Equals(size2);
        }

        /// <summary>
        /// Determines whether the specified objects are not equal to each other.
        /// </summary>
        /// <param name="size1">
        /// The first object to compare.
        /// </param>
        /// <param name="size2">
        /// The second object to compare.
        /// </param>
        /// <returns>
        /// True if the specified objects are not equal to each other; otherwise, false.
        /// </returns>
        public static bool operator !=(SIZE size1, SIZE size2)
        {
            return !size1.Equals(size2);
        }

        /// <summary>
        /// Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        /// A hash code for the current object.
        /// </returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">
        /// The object to compare with the current object.
        /// </param>
        /// <returns>
        /// True if the specified object is equal to the current object; otherwise, false.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (!(obj is SIZE))
            {
                return false;
            }

            return this.Equals((SIZE)obj);
        }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="other">
        /// The object to compare with the current object.
        /// </param>
        /// <returns>
        /// True if the specified object is equal to the current object; otherwise, false.
        /// </returns>
        public bool Equals(SIZE other)
        {
            return object.ReferenceEquals(this, other);
        }
        #endregion Methods
    } // RSG.Interop.Microsoft.Windows.SIZE {Structure}
} // RSG.Interop.Microsoft.Windows {Namespace}
