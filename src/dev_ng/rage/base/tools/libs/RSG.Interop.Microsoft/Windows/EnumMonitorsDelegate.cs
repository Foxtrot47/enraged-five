﻿//---------------------------------------------------------------------------------------------
// <copyright file="EnumMonitorsDelegate.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System;

    /// <summary>
    /// An application-defined call-back function that is called by the EnumDisplayMonitors
    /// function.
    /// </summary>
    /// <param name="monitor">
    /// A handle to the display monitor.
    /// </param>
    /// <param name="dc">
    /// A handle to a device context.
    /// </param>
    /// <param name="area">
    /// The virtual-screen coordinates for the monitors rectangle.
    /// </param>
    /// <param name="data">
    /// Application-defined data.
    /// </param>
    /// <returns>
    /// To continue enumeration, the call-back function must return true; to stop
    /// enumeration, it must return false.
    /// </returns>
    public delegate bool EnumMonitorsDelegate(
        IntPtr monitor, IntPtr dc, ref RECT area, IntPtr data);
} // RSG.Interop.Microsoft.Windows {Namespace}
