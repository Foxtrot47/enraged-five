﻿// --------------------------------------------------------------------------------------------
// <copyright file="FILEDESCRIPTOR.cs" company="Rockstar">
//     Copyright © Rockstar Games 2015. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System;
    using System.Runtime.InteropServices;

    /// <summary>
    /// Used generically to filter elements.
    /// </summary>
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public struct FILEDESCRIPTOR
    {
        /// <summary>
        /// An array of flags that indicate which of the other structure members contain valid
        /// data.
        /// </summary>
        public FileDescriptorFlags Flags;

        /// <summary>
        /// The file type identifier.
        /// </summary>
        public Guid FileType;

        /// <summary>
        /// The width of the file icon.
        /// </summary>
        public int IconWidth;

        /// <summary>
        /// The height of the file icon.
        /// </summary>
        public int IconHeight;

        /// <summary>
        /// The screen x-coordinates of the file object.
        /// </summary>
        public int ScreenPointX;

        /// <summary>
        /// The screen y-coordinates of the file object.
        /// </summary>
        public int ScreenPointY;

        /// <summary>
        /// File attribute flags.
        /// </summary>
        public uint FileAttributes;

        /// <summary>
        /// The FILETIME structure that contains the time of file creation.
        /// </summary>
        public System.Runtime.InteropServices.ComTypes.FILETIME CreationTime;

        /// <summary>
        /// The FILETIME structure that contains the time that the file was last accessed.
        /// </summary>
        public System.Runtime.InteropServices.ComTypes.FILETIME LastAccessTime;

        /// <summary>
        /// The FILETIME structure that contains the time of the last write operation.
        /// </summary>
        public System.Runtime.InteropServices.ComTypes.FILETIME LastWriteTime;

        /// <summary>
        /// The high-order DWORD of the file size, in bytes.
        /// </summary>
        public uint FileSizeHigh;

        /// <summary>
        /// The low-order DWORD of the file size, in bytes.
        /// </summary>
        public uint FileSizeLow;

        /// <summary>
        /// The null-terminated string that contains the name of the file.
        /// </summary>
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 260)]
        public string FileName;
    } // RSG.Interop.Microsoft.Windows.FILEDESCRIPTOR {Struct}
} // RSG.Interop.Microsoft.Windows {Namespace}
