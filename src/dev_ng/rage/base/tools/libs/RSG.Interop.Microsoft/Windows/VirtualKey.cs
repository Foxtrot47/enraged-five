﻿//---------------------------------------------------------------------------------------------
// <copyright file="VirtualKey.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    /// <summary>
    /// An enumeration defining constants that map to the various VK_* constants.
    /// </summary>
    public enum VirtualKey : int
    {
        /// <summary>
        /// The Left Mouse Button.
        /// </summary>
        LBUTTON = 0x01,

        /// <summary>
        /// The Right mouse button.
        /// </summary>
        RBUTTON = 0x02,

        /// <summary>
        /// The Control-break processing.
        /// </summary>
        CANCEL = 0x03,

        /// <summary>
        /// The Middle mouse button (three-button mouse).
        /// </summary>
        MBUTTON = 0x04,

        /// <summary>
        /// The X1 mouse button.
        /// </summary>
        XBUTTON1 = 0x05,

        /// <summary>
        /// The X2 mouse button.
        /// </summary>
        XBUTTON2 = 0x06,

        /// <summary>
        /// The BACKSPACE key.
        /// </summary>
        BACK = 0x08,

        /// <summary>
        /// The TAB key.
        /// </summary>
        TAB = 0x09,

        /// <summary>
        /// The CLEAR key.
        /// </summary>
        CLEAR = 0x0C,

        /// <summary>
        /// The ENTER key.
        /// </summary>
        RETURN = 0x0D,

        /// <summary>
        /// The SHIFT key.
        /// </summary>
        SHIFT = 0x10,

        /// <summary>
        /// The CTRL key.
        /// </summary>
        CONTROL = 0x11,

        /// <summary>
        /// The ALT key.
        /// </summary>
        MENU = 0x12,

        /// <summary>
        /// The PAUSE key.
        /// </summary>
        PAUSE = 0x13,

        /// <summary>
        /// The CAPS LOCK key.
        /// </summary>
        CAPITAL = 0x14,

        /// <summary>
        /// IME final mode.
        /// </summary>
        FINAL = 0x18,

        /// <summary>
        /// The ESC key.
        /// </summary>
        ESCAPE = 0x1B,

        /// <summary>
        /// IME accept.
        /// </summary>
        ACCEPT = 0x1E,

        /// <summary>
        /// IME mode change request.
        /// </summary>
        MODECHANGE = 0x1F,

        /// <summary>
        /// The SPACEBAR key.
        /// </summary>
        SPACE = 0x20,

        /// <summary>
        /// The PAGE UP key.
        /// </summary>
        PRIOR = 0x21,

        /// <summary>
        /// The PAGE DOWN key.
        /// </summary>
        NEXT = 0x22,

        /// <summary>
        /// The END key.
        /// </summary>
        END = 0x23,

        /// <summary>
        /// The HOME key.
        /// </summary>
        HOME = 0x24,

        /// <summary>
        /// The LEFT ARROW key.
        /// </summary>
        LEFT = 0x25,

        /// <summary>
        /// UThe P ARROW key.
        /// </summary>
        UP = 0x26,

        /// <summary>
        /// The RIGHT ARROW key.
        /// </summary>
        RIGHT = 0x27,

        /// <summary>
        /// The DOWN ARROW key.
        /// </summary>
        DOWN = 0x28,

        /// <summary>
        /// The SELECT key.
        /// </summary>
        SELECT = 0x29,

        /// <summary>
        /// The PRINT key.
        /// </summary>
        PRINT = 0x2A,

        /// <summary>
        /// The EXECUTE key.
        /// </summary>
        EXECUTE = 0x2B,

        /// <summary>
        /// The PRINT SCREEN key.
        /// </summary>
        SNAPSHOT = 0x2C,

        /// <summary>
        /// The INS key.
        /// </summary>
        INSERT = 0x2D,

        /// <summary>
        /// The DEL key.
        /// </summary>
        DELETE = 0x2E,

        /// <summary>
        /// The HELP key.
        /// </summary>
        HELP = 0x2F,

        /// <summary>
        /// The 0 key.
        /// </summary>
        N0 = 0x30,

        /// <summary>
        /// The 1 key.
        /// </summary>
        N1 = 0x31,

        /// <summary>
        /// The 2 key.
        /// </summary>
        N2 = 0x32,

        /// <summary>
        /// The 3 key.
        /// </summary>
        N3 = 0x33,

        /// <summary>
        /// The 4 key.
        /// </summary>
        N4 = 0x34,

        /// <summary>
        /// The 5 key.
        /// </summary>
        N5 = 0x35,

        /// <summary>
        /// The 6 key.
        /// </summary>
        N6 = 0x36,

        /// <summary>
        /// The 7 key.
        /// </summary>
        N7 = 0x37,

        /// <summary>
        /// The 8 key.
        /// </summary>
        N8 = 0x38,

        /// <summary>
        /// The 9 key.
        /// </summary>
        N9 = 0x39,

        /// <summary>
        /// The A key.
        /// </summary>
        A = 0x41,

        /// <summary>
        /// The B key.
        /// </summary>
        B = 0x42,

        /// <summary>
        /// The C key.
        /// </summary>
        C = 0x43,

        /// <summary>
        /// The D key.
        /// </summary>
        D = 0x44,

        /// <summary>
        /// The E key.
        /// </summary>
        E = 0x45,

        /// <summary>
        /// The F key.
        /// </summary>
        F = 0x46,

        /// <summary>
        /// The G key.
        /// </summary>
        G = 0x47,

        /// <summary>
        /// The H key.
        /// </summary>
        H = 0x48,

        /// <summary>
        /// The I key.
        /// </summary>
        I = 0x49,

        /// <summary>
        /// The J key.
        /// </summary>
        J = 0x4A,

        /// <summary>
        /// The K key.
        /// </summary>
        K = 0x4B,

        /// <summary>
        /// The L key.
        /// </summary>
        L = 0x4C,

        /// <summary>
        /// The M key.
        /// </summary>
        M = 0x4D,

        /// <summary>
        /// The N key.
        /// </summary>
        N = 0x4E,

        /// <summary>
        /// The O key.
        /// </summary>
        O = 0x4F,

        /// <summary>
        /// The P key.
        /// </summary>
        P = 0x50,

        /// <summary>
        /// The Q key.
        /// </summary>
        Q = 0x51,

        /// <summary>
        /// The R key.
        /// </summary>
        R = 0x52,

        /// <summary>
        /// The S key.
        /// </summary>
        S = 0x53,

        /// <summary>
        /// The T key.
        /// </summary>
        T = 0x54,

        /// <summary>
        /// The U key.
        /// </summary>
        U = 0x55,

        /// <summary>
        /// The V key.
        /// </summary>
        V = 0x56,

        /// <summary>
        /// The W key.
        /// </summary>
        W = 0x57,

        /// <summary>
        /// The X key.
        /// </summary>
        X = 0x58,

        /// <summary>
        /// The Y key.
        /// </summary>
        Y = 0x59,

        /// <summary>
        /// The Z key.
        /// </summary>
        Z = 0x5A,

        /// <summary>
        /// The Left Windows key (Natural keyboard).
        /// </summary>
        LWIN = 0x5B,

        /// <summary>
        /// The Right Windows key (Natural keyboard).
        /// </summary>
        RWIN = 0x5C,

        /// <summary>
        /// The Applications key (Natural keyboard).
        /// </summary>
        APPS = 0x5D,

        /// <summary>
        /// The Computer Sleep key.
        /// </summary>
        SLEEP = 0x5F,

        /// <summary>
        /// The Numeric keypad 0 key.
        /// </summary>
        NUMPAD0 = 0x60,

        /// <summary>
        /// The Numeric keypad 1 key.
        /// </summary>
        NUMPAD1 = 0x61,

        /// <summary>
        /// The Numeric keypad 2 key.
        /// </summary>
        NUMPAD2 = 0x62,

        /// <summary>
        /// The Numeric keypad 3 key.
        /// </summary>
        NUMPAD3 = 0x63,

        /// <summary>
        /// The Numeric keypad 4 key.
        /// </summary>
        NUMPAD4 = 0x64,

        /// <summary>
        /// The Numeric keypad 5 key.
        /// </summary>
        NUMPAD5 = 0x65,

        /// <summary>
        /// The Numeric keypad 6 key.
        /// </summary>
        NUMPAD6 = 0x66,

        /// <summary>
        /// The Numeric keypad 7 key.
        /// </summary>
        NUMPAD7 = 0x67,

        /// <summary>
        /// The Numeric keypad 8 key.
        /// </summary>
        NUMPAD8 = 0x68,

        /// <summary>
        /// The Numeric keypad 9 key.
        /// </summary>
        NUMPAD9 = 0x69,

        /// <summary>
        /// The Multiply key.
        /// </summary>
        MULTIPLY = 0x6A,

        /// <summary>
        /// The Add key.
        /// </summary>
        ADD = 0x6B,

        /// <summary>
        /// The Separator key.
        /// </summary>
        SEPARATOR = 0x6C,

        /// <summary>
        /// The Subtract key.
        /// </summary>
        SUBTRACT = 0x6D,

        /// <summary>
        /// The Decimal key.
        /// </summary>
        DECIMAL = 0x6E,

        /// <summary>
        /// The Divide key.
        /// </summary>
        DIVIDE = 0x6F,

        /// <summary>
        /// The F1 key.
        /// </summary>
        F1 = 0x70,

        /// <summary>
        /// The F2 key.
        /// </summary>
        F2 = 0x71,

        /// <summary>
        /// The F3 key.
        /// </summary>
        F3 = 0x72,

        /// <summary>
        /// The F4 key.
        /// </summary>
        F4 = 0x73,

        /// <summary>
        /// The F5 key.
        /// </summary>
        F5 = 0x74,

        /// <summary>
        /// The F6 key.
        /// </summary>
        F6 = 0x75,

        /// <summary>
        /// The F7 key.
        /// </summary>
        F7 = 0x76,

        /// <summary>
        /// The F8 key.
        /// </summary>
        F8 = 0x77,

        /// <summary>
        /// The F9 key.
        /// </summary>
        F9 = 0x78,

        /// <summary>
        /// The F10 key.
        /// </summary>
        F10 = 0x79,

        /// <summary>
        /// The F11 key.
        /// </summary>
        F11 = 0x7A,

        /// <summary>
        /// The F12 key.
        /// </summary>
        F12 = 0x7B,

        /// <summary>
        /// The F13 key.
        /// </summary>
        F13 = 0x7C,

        /// <summary>
        /// The F14 key.
        /// </summary>
        F14 = 0x7D,

        /// <summary>
        /// The F15 key.
        /// </summary>
        F15 = 0x7E,

        /// <summary>
        /// The F16 key.
        /// </summary>
        F16 = 0x7F,

        /// <summary>
        /// The F17 key.
        /// </summary>
        F17 = 0x80,

        /// <summary>
        /// The F18 key.
        /// </summary>
        F18 = 0x81,

        /// <summary>
        /// The F19 key.
        /// </summary>
        F19 = 0x82,

        /// <summary>
        /// The F20 key.
        /// </summary>
        F20 = 0x83,

        /// <summary>
        /// The F21 key.
        /// </summary>
        F21 = 0x84,

        /// <summary>
        /// The F22 key.
        /// </summary>
        F22 = 0x85,

        /// <summary>
        /// The F23 key.
        /// </summary>
        F23 = 0x86,

        /// <summary>
        /// The F24 key.
        /// </summary>
        F24 = 0x87,

        /// <summary>
        /// The NUM LOCK key.
        /// </summary>
        NUMLOCK = 0x90,

        /// <summary>
        /// The SCROLL LOCK key.
        /// </summary>
        SCROLL = 0x91,

        /// <summary>
        /// The Left SHIFT key.
        /// </summary>
        LSHIFT = 0xA0,

        /// <summary>
        /// The Right SHIFT key.
        /// </summary>
        RSHIFT = 0xA1,

        /// <summary>
        /// The Left CONTROL key.
        /// </summary>
        LCONTROL = 0xA2,

        /// <summary>
        /// The Right CONTROL key.
        /// </summary>
        RCONTROL = 0xA3,

        /// <summary>
        /// The Left MENU key.
        /// </summary>
        LMENU = 0xA4,

        /// <summary>
        /// The Right MENU key.
        /// </summary>
        RMENU = 0xA5,

        /// <summary>
        /// The Browser Back key.
        /// </summary>
        BROWSER_BACK = 0xA6,

        /// <summary>
        /// The Browser Forward key.
        /// </summary>
        BROWSER_FORWARD = 0xA7,

        /// <summary>
        /// The Browser Refresh key.
        /// </summary>
        BROWSER_REFRESH = 0xA8,

        /// <summary>
        /// The Browser Stop key.
        /// </summary>
        BROWSER_STOP = 0xA9,

        /// <summary>
        /// The Browser Search key.
        /// </summary>
        BROWSER_SEARCH = 0xAA,

        /// <summary>
        /// The Browser Favourites key.
        /// </summary>
        BROWSER_FAVORITES = 0xAB,

        /// <summary>
        /// The Browser Start and Home key.
        /// </summary>
        BROWSER_HOME = 0xAC,

        /// <summary>
        /// The Volume Mute key.
        /// </summary>
        VOLUME_MUTE = 0xAD,

        /// <summary>
        /// The Volume Down key.
        /// </summary>
        VOLUME_DOWN = 0xAE,

        /// <summary>
        /// The Volume Up key.
        /// </summary>
        VOLUME_UP = 0xAF,

        /// <summary>
        /// The Next Track key.
        /// </summary>
        MEDIA_NEXT_TRACK = 0xB0,

        /// <summary>
        /// The Previous Track key.
        /// </summary>
        MEDIA_PREV_TRACK = 0xB1,

        /// <summary>
        /// The Stop Media key.
        /// </summary>
        MEDIA_STOP = 0xB2,

        /// <summary>
        /// The Play/Pause Media key.
        /// </summary>
        MEDIA_PLAY_PAUSE = 0xB3,

        /// <summary>
        /// The Start Mail key.
        /// </summary>
        LAUNCH_MAIL = 0xB4,

        /// <summary>
        /// The Select Media key.
        /// </summary>
        LAUNCH_MEDIA_SELECT = 0xB5,

        /// <summary>
        /// The Start Application 1 key.
        /// </summary>
        LAUNCH_APP1 = 0xB6,

        /// <summary>
        /// The Start Application 2 key.
        /// </summary>
        LAUNCH_APP2 = 0xB7,

        /// <summary>
        /// For any country/region, the '+' key.
        /// </summary>
        OEM_PLUS = 0xBB,

        /// <summary>
        /// For any country/region, the ',' key.
        /// </summary>
        OEM_COMMA = 0xBC,

        /// <summary>
        /// For any country/region, the '-' key.
        /// </summary>
        OEM_MINUS = 0xBD,

        /// <summary>
        /// For any country/region, the '.' key.
        /// </summary>
        OEM_PERIOD = 0xBE,

        /// <summary>
        /// The IME PROCESS key.
        /// </summary>
        PROCESSKEY = 0xE5,

        /// <summary>
        /// Used to pass Unicode characters as if they were keystrokes.
        /// </summary>
        PACKET = 0xE7,

        /// <summary>
        /// The Erase EOF key.
        /// </summary>
        EREOF = 0xF9,

        /// <summary>
        /// The Play key.
        /// </summary>
        PLAY = 0xFA,

        /// <summary>
        /// The Zoom key.
        /// </summary>
        ZOOM = 0xFB,

        /// <summary>
        /// The PA1 key.
        /// </summary>
        PA1 = 0xFD,

        /// <summary>
        /// The Clear key.
        /// </summary>
        OEM_CLEAR = 0xFE
    } // RSG.Interop.Microsoft.Windows.VirtualKey {Enum}
} // RSG.Interop.Microsoft.Windows {Namespace}
