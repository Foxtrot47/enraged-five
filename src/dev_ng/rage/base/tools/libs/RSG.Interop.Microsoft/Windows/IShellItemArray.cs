﻿// --------------------------------------------------------------------------------------------
// <copyright file="IShellItemArray.cs" company="Rockstar">
//     Copyright © Rockstar Games 2012. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    /// <summary>
    /// Exposes methods that create and manipulate Shell item arrays.
    /// </summary>
    [ComImport,
     Guid(InterfaceIDGuid.ShellItemArrayId),
     InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IShellItemArray
    {
        #region Methods
        /// <summary>
        /// Binds to an object by means of the specified handler.
        /// </summary>
        /// <param name="bindContext">
        /// The bind context interface.
        /// </param>
        /// <param name="handler">
        /// A flag that determines the handler.
        /// </param>
        /// <param name="objectId">
        /// The IID of the object type to retrieve.
        /// </param>
        /// <param name="bind">
        /// When this methods returns, contains the object specified using the handler
        /// specified.
        /// </param>
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void BindToHandler(
            [In, MarshalAs(UnmanagedType.Interface)] IntPtr bindContext,
            [In] ref Guid handler,
            [In] ref Guid objectId,
            out IntPtr bind);

        /// <summary>
        /// Gets a property store.
        /// </summary>
        /// <param name="flags">
        /// Specifies the attributes of the property store to retrieve.
        /// </param>
        /// <param name="objectId">
        /// The IID of the object type to retrieve.
        /// </param>
        /// <param name="store">
        /// When this method returns, contains the property store interface requested.
        /// </param>
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void GetPropertyStore([In] int flags, [In] ref Guid objectId, out IntPtr store);

        /// <summary>
        /// Gets a property description list for the items in the shell item array.
        /// </summary>
        /// <param name="keyType">
        /// Specifies which property list to retrieve.
        /// </param>
        /// <param name="objectId">
        /// The IID of the object type to retrieve.
        /// </param>
        /// <param name="list">
        /// When this method returns, contains the property list interface requested.
        /// </param>
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void GetPropertyDescriptionList(
            [In] ref PROPERTYKEY keyType, [In] ref Guid objectId, out IntPtr list);

        /// <summary>
        /// Gets the attributes of the set of items contained in an IShellItemArray.
        /// </summary>
        /// <param name="flags">
        /// Define how that final attribute set is determined if there are more than one item
        /// in the array.
        /// </param>
        /// <param name="mask">
        /// A mask that specifies what particular attributes are being requested.
        /// </param>
        /// <param name="attributes">
        /// When this method returns successfully, contains the values of the requested
        /// attributes.
        /// </param>
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void GetAttributes(
            [In] ShellItemAttributesCombineFlag flags, [In] uint mask, out uint attributes);

        /// <summary>
        /// Gets the number of items in the given IShellItem array.
        /// </summary>
        /// <param name="pdwNumItems">
        /// When this method returns, contains the number of items in the IShellItemArray.
        /// </param>
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void GetCount(out uint pdwNumItems);

        /// <summary>
        /// Gets the item at the given index in the IShellItemArray.
        /// </summary>
        /// <param name="index">
        /// The index of the IShellItem requested in the IShellItemArray.
        /// </param>
        /// <param name="ppsi">
        /// When this method returns, contains the requested IShellItem pointer.
        /// </param>
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void GetItemAt(
            [In] uint index, [MarshalAs(UnmanagedType.Interface)] out IShellItem ppsi);

        /// <summary>
        /// Gets an enumerator of the items in the array.
        /// </summary>
        /// <param name="ppenumShellItems">
        /// When this method returns, contains an IEnumShellItems pointer that enumerates the
        /// shell items that are in the array.
        /// </param>
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void EnumItems([MarshalAs(UnmanagedType.Interface)] out IntPtr ppenumShellItems);
        #endregion
    } // RSG.Interop.Microsoft.Windows.IShellItemArray {Interface}
} // RSG.Interop.Microsoft.Windows {Namespace}
