﻿//---------------------------------------------------------------------------------------------
// <copyright file="RM_UNIQUE_PROCESS.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System;
    using System.Runtime.InteropServices;
    using Com = System.Runtime.InteropServices.ComTypes;

    /// <summary>
    /// Uniquely identifies a process by its PID and the time the process began.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct RM_UNIQUE_PROCESS
    {
        /// <summary>
        /// The product identifier (PID).
        /// </summary>
        public int dwProcessId;
        
        /// <summary>
        /// The creation time of the process.
        /// </summary>
        public Com.FILETIME ProcessStartTime;
    }

} // RSG.Interop.Microsoft.Windows namespace
