﻿// --------------------------------------------------------------------------------------------
// <copyright file="HRESULT.cs" company="Rockstar">
//     Copyright © Rockstar Games 2012. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    /// <summary>
    /// The most common values returned from a native method that represents the successfulness
    /// of the operation.
    /// </summary>
    public enum HRESULT : int
    {
        /// <summary>
        /// Operation successful.
        /// </summary>
        S_OK = 0x0000,

        /// <summary>
        /// Operation successful.
        /// </summary>
        S_FALSE = 0x0001,

        /// <summary>
        /// One or more arguments are not valid.
        /// </summary>
        E_INVALIDARG = -2147024809,

        /// <summary>
        /// Failed to allocate necessary memory.
        /// </summary>
        E_OUTOFMEMORY = -2147024882,

        /// <summary>
        /// Failed as operation isn't supported.
        /// </summary>
        E_ADVISENOTSUPPORTED = -2147221501,

        /// <summary>
        /// Invalid FORMATETC structure.
        /// </summary>
        E_FORMATETC = -2147221404,

        /// <summary>
        /// Invalid TYMED structure.
        /// </summary>
        E_TYMED = -2147221399,

        /// <summary>
        /// Invalid aspects.
        /// </summary>
        E_DVASPECT = -2147221397,

        /// <summary>
        /// Unspecified error.
        /// </summary>
        E_FAIL = -2147467259,
    } // RSG.Interop.Microsoft.Windows.HRESULT {Enum}
} // RSG.Interop.Microsoft.Windows {Namespace}
