﻿//---------------------------------------------------------------------------------------------
// <copyright file="WindowLongFlag.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    /// <summary>
    /// Defines the different attributes that can be set or retrieved from the SetWindowLong
    /// and GetWindowLong methods.
    /// </summary>
    public enum WindowLongFlag : int
    {
        /// <summary>
        /// Offset to the extended window style.
        /// </summary>
        EXSTYLE = -20,

        /// <summary>
        /// Offset to the new application instance handle.
        /// </summary>
        HINSTANCE = -6,

        /// <summary>
        /// Offset to the handle of the parent window, if any.
        /// </summary>
        HWNDPARENT = -8,

        /// <summary>
        /// Offset to the identifier of the child window.
        /// </summary>
        ID = -12,

        /// <summary>
        /// Offset to the window style.
        /// </summary>
        STYLE = -16,

        /// <summary>
        /// Offset to the user data associated with a window.
        /// </summary>
        USERDATA = -21,

        /// <summary>
        /// Offset to the address for a window procedure function.
        /// </summary>
        WNDPROC = -4
    } // RSG.Interop.Microsoft.Windows.WindowLongFlag {Enum}
} // RSG.Interop.Microsoft.Windows {Namespace}
