﻿//---------------------------------------------------------------------------------------------
// <copyright file="BITMAPINFO.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System.Runtime.InteropServices;

    /// <summary>
    /// Defines the dimensions and colour information for a device-independent bitmap.
    /// </summary>
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
    public struct BITMAPINFO
    {
        #region Fields
        /// <summary>
        /// The number of bytes required by the structure.
        /// </summary>
        public uint Size;

        /// <summary>
        /// The width of the bitmap, in pixels.
        /// </summary>
        public int Width;

        /// <summary>
        /// The height of the bitmap, in pixels.
        /// </summary>
        public int Height;

        /// <summary>
        /// The number of planes for the target device.
        /// </summary>
        public short Planes;

        /// <summary>
        /// The number of bits-per-pixel.
        /// </summary>
        public short BitCount;

        /// <summary>
        /// The type of compression for a compressed bottom-up bitmap.
        /// </summary>
        public int Compression;

        /// <summary>
        /// The size, in bytes, of the image.
        /// </summary>
        public int SizeImage;

        /// <summary>
        /// The horizontal resolution, in pixels-per-meter, of the target device for the
        /// bitmap.
        /// </summary>
        public int XPixelsPerMeter;

        /// <summary>
        /// The vertical resolution, in pixels-per-meter, of the target device for the bitmap.
        /// </summary>
        public int YPixelsPerMeter;

        /// <summary>
        /// The number of colour indexes in the colour table that are actually used by the
        /// bitmap.
        /// </summary>
        public int ColourUsed;

        /// <summary>
        /// The number of colour indexes that are required for displaying the bitmap. If this
        /// value is zero, all colours are required.
        /// </summary>
        public int ColourImportant;

        /// <summary>
        /// Gets the elements of the array that make up the colour table.
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1024)]
        public byte[] Colors;
        #endregion Fields

        #region Methods
        /// <summary>
        /// Determines whether the specified objects are equal to each other.
        /// </summary>
        /// <param name="info1">
        /// The first object to compare.
        /// </param>
        /// <param name="info2">
        /// The second object to compare.
        /// </param>
        /// <returns>
        /// True if the specified objects are equal to each other; otherwise, false.
        /// </returns>
        public static bool operator ==(BITMAPINFO info1, BITMAPINFO info2)
        {
            return info1.Equals(info2);
        }

        /// <summary>
        /// Determines whether the specified objects are not equal to each other.
        /// </summary>
        /// <param name="info1">
        /// The first object to compare.
        /// </param>
        /// <param name="info2">
        /// The second object to compare.
        /// </param>
        /// <returns>
        /// True if the specified objects are not equal to each other; otherwise, false.
        /// </returns>
        public static bool operator !=(BITMAPINFO info1, BITMAPINFO info2)
        {
            return !info1.Equals(info2);
        }

        /// <summary>
        /// Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        /// A hash code for the current object.
        /// </returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">
        /// The object to compare with the current object.
        /// </param>
        /// <returns>
        /// True if the specified object is equal to the current object; otherwise, false.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (!(obj is BITMAPINFO))
            {
                return false;
            }

            return this.Equals((BITMAPINFO)obj);
        }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="other">
        /// The object to compare with the current object.
        /// </param>
        /// <returns>
        /// True if the specified object is equal to the current object; otherwise, false.
        /// </returns>
        public bool Equals(BITMAPINFO other)
        {
            return object.ReferenceEquals(this, other);
        }
        #endregion Methods
    } // RSG.Interop.Microsoft.Windows.BITMAPINFO {Structure}
} // RSG.Interop.Microsoft.Windows {Namespace}
