﻿//---------------------------------------------------------------------------------------------
// <copyright file="FileOpenDialogOptionFlags.cs" company="Rockstar">
//     Copyright © Rockstar Games 2012. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System;

    /// <summary>
    /// Defines the different options that can be set onto a file dialog window.
    /// </summary>
    [Flags]
    public enum FileOpenDialogOptionFlags : uint
    {
        /// <summary>
        /// When saving a file, prompt before overwriting an existing file of the same name.
        /// This is a default value for the Save dialog.
        /// </summary>
        OVERWRITEPROMPT = 0x00000002,

        /// <summary>
        /// In the save dialog, only allow the user to choose a file that has one of the file
        /// name extensions provided.
        /// </summary>
        STRICTFILETYPES = 0x00000004,

        /// <summary>
        /// Not used.
        /// </summary>
        NOCHANGEDIR = 0x00000008,

        /// <summary>
        /// Present the Open dialog offering a choice of folders rather than files.
        /// </summary>
        PICKFOLDERS = 0x00000020,

        /// <summary>
        /// Ensures that returned items are file system items.
        /// </summary>
        FORCEFILESYSTEM = 0x00000040,

        /// <summary>
        /// Enables the user to choose any item in the Shell namespace.
        /// </summary>
        ALLNONSTORAGEITEMS = 0x00000080,

        /// <summary>
        /// Do not check for situations that would prevent an application from opening the
        /// selected file, such as sharing violations or access denied errors.
        /// </summary>
        NOVALIDATE = 0x00000100,

        /// <summary>
        /// Enables the user to select multiple items in the open dialog.
        /// </summary>
        ALLOWMULTISELECT = 0x00000200,

        /// <summary>
        /// The item returned must be in an existing folder.
        /// </summary>
        PATHMUSTEXIST = 0x00000800,

        /// <summary>
        /// The item returned must exist.
        /// </summary>
        FILEMUSTEXIST = 0x00001000,

        /// <summary>
        /// Prompt for creation if the item returned in the save dialog does not exist.
        /// </summary>
        CREATEPROMPT = 0x00002000,

        /// <summary>
        /// In the case of a sharing violation when an application is opening a file, call the
        /// application back through OnShareViolation for guidance.
        /// </summary>
        SHAREAWARE = 0x00004000,

        /// <summary>
        /// Do not return read-only items.
        /// </summary>
        NOREADONLYRETURN = 0x00008000,

        /// <summary>
        /// Do not test creation of the item returned in the save dialog. If this flag is not
        /// set, the calling application must handle errors such as denial of access discovered
        /// in the creation test.
        /// </summary>
        NOTESTFILECREATE = 0x00010000,

        /// <summary>
        /// Hide the list of places from which the user has recently opened or saved items.
        /// </summary>
        HIDEMRUPLACES = 0x00020000,

        /// <summary>
        /// Hide items shown by default in the view's navigation pane.
        /// </summary>
        HIDEPINNEDPLACES = 0x00040000,

        /// <summary>
        /// Shortcuts should not be treated as their target items.
        /// </summary>
        NODEREFERENCELINKS = 0x00100000,

        /// <summary>
        /// Do not add the item being opened or saved to the recent documents list.
        /// </summary>
        DONTADDTORECENT = 0x02000000,

        /// <summary>
        /// Show hidden and system items.
        /// </summary>
        FORCESHOWHIDDEN = 0x10000000,

        /// <summary>
        /// Indicates to the Save As dialog box that it should open in expanded mode. Expanded
        /// mode is the mode that is set and unset by clicking the button in the lower-left
        /// corner of the Save As dialog box that switches between Browse Folders and Hide
        /// Folders when clicked.
        /// </summary>
        DEFAULTNOMINIMODE = 0x20000000,

        /// <summary>
        /// Indicates to the Open dialog box that the preview pane should always be displayed.
        /// </summary>
        FORCEPREVIEWPANEON = 0x40000000,
    } // RSG.Interop.Microsoft.Windows.FileOpenDialogOptionFlags {Enum}
} // RSG.Interop.Microsoft.Windows {Namespace}
