﻿//---------------------------------------------------------------------------------------------
// <copyright file="DeviceCreationFlags.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System;

    /// <summary>
    /// Defines the different ways a device context is created.
    /// </summary>
    [Flags]
    public enum DeviceCreationFlags : uint
    {
        /// <summary>
        /// Returns a DC that corresponds to the window rectangle rather than the
        /// client rectangle.
        /// </summary>
        WINDOW = 0x00000001,

        /// <summary>
        /// Returns a DC from the cache, rather than the OWNDC or CLASSDC window.
        /// </summary>
        CACHE = 0x00000002,

        /// <summary>
        /// Uses the visible region of the parent window.
        /// </summary>
        PARENTCLIP = 0x00000020,

        /// <summary>
        /// Excludes the visible regions of all sibling windows above the specified window.
        /// </summary>
        CLIPSIBLINGS = 0x00000010,

        /// <summary>
        /// Excludes the visible regions of all child windows below the specified window.
        /// </summary>
        CLIPCHILDREN = 0x00000008,

        /// <summary>
        /// Does not reset the attributes of this DC to the default attributes when this
        /// DC is released.
        /// </summary>
        NORESETATTRS = 0x00000004,

        /// <summary>
        /// Allows drawing even if there is a LockWindowUpdate call in effect that would
        /// otherwise exclude this window.
        /// </summary>
        LOCKWINDOWUPDATE = 0x00000400,

        /// <summary>
        /// The clipping region specified for the combine region is excluded from the visible
        /// region of the returned DC.
        /// </summary>
        EXCLUDERGN = 0x00000100,

        /// <summary>
        /// The clipping region region specified for the combine region is intersected with the
        /// visible region of the returned DC.
        /// </summary>
        INTERSECTRGN = 0x00000080,
    } // RSG.Interop.Microsoft.Windows.DeviceCreationFlags {Enum}
} // RSG.Interop.Microsoft.Windows {Namespace}
