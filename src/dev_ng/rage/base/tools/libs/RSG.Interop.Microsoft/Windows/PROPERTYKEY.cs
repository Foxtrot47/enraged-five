﻿//---------------------------------------------------------------------------------------------
// <copyright file="PROPERTYKEY.cs" company="Rockstar">
//     Copyright © Rockstar Games 2012. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Windows
{
    using System;
    using System.Runtime.InteropServices;

    /// <summary>
    /// Specifies the values that programmatically identifies a property.
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 4)]
    public struct PROPERTYKEY
    {
        #region Fields
        /// <summary>
        /// The unique GUID for the property.
        /// </summary>
        internal Guid UniqueIdentifier;

        /// <summary>
        /// The property identifier.
        /// </summary>
        internal uint PropertyId;
        #endregion Fields
    } // RSG.Interop.Microsoft.Windows.PROPERTYKEY {Enum}
} // RSG.Interop.Microsoft.Windows {Namespace}
