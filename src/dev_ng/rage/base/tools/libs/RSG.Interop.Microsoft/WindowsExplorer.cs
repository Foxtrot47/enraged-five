﻿using System;
using System.Collections.Generic;
using Microsoft.Win32;
using RSG.Interop.Microsoft.Windows;

namespace RSG.Interop.Microsoft
{

    /// <summary>
    /// Microsoft Windows Explorer integration class.
    /// </summary>
    public static class WindowsExplorer
    {
        #region Constants
        /// <summary>
        /// Path to the Windows Explorer advanced registry key.
        /// </summary>
        private static readonly String RegKey_WindowsExplorerAdvanced = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced";

        /// <summary>
        /// Separate Process advanced flag.
        /// </summary>
        private static readonly String RegKey_SeparateProcess = @"SeparateProcess";
        #endregion // Constants

        #region Controller Methods
        /// <summary>
        /// Return whether the Windows Explorer "Launch folder windows in a separate process" 
        /// is set.
        /// </summary>
        /// <returns></returns>
        public static bool IsSeparateProcessesSet()
        {
            bool isSeparate = false;
            using (RegistryKey explorerKey = 
                Registry.CurrentUser.OpenSubKey(RegKey_WindowsExplorerAdvanced))
            {
                Object isSeparateObj = explorerKey.GetValue(RegKey_SeparateProcess, 0);
                if (isSeparateObj is int)
                    isSeparate = (0 == (int)isSeparateObj) ? false : true;
            }
            return (isSeparate);
        }

        /// <summary>
        /// Set Windows Explorer "Launch folder windows in a separate process".
        /// </summary>
        /// <param name="isSeparate"></param>
        public static void SetSeparateProcess(bool isSeparate)
        {
            using (RegistryKey explorerKey =
                Registry.CurrentUser.OpenSubKey(RegKey_WindowsExplorerAdvanced, RegistryKeyPermissionCheck.ReadWriteSubTree))
            {
                explorerKey.SetValue(RegKey_SeparateProcess, isSeparate ? 1 : 0, RegistryValueKind.DWord);
            }
        }

        /// <summary>
        /// Closes all of the top level windows that are associated with the windows explorer.
        /// (i.e. All windows with a atom of CabientWClass or ExplorerWClass).
        /// </summary>
        public static void CloseAllTopLevelWindows()
        {
            IList<IntPtr> windows = GetAllTopLevelWindows();
            foreach (IntPtr window in windows)
            {
                User32.SendMessage(window, WindowMessage.CLOSE);
            }
        }

        /// <summary>
        /// Retrieves all the top level explorer windows.
        /// </summary>
        /// <returns></returns>
        public static IList<IntPtr> GetAllTopLevelWindows()
        {
            List<IntPtr> windows = new List<IntPtr>();
            windows.AddRange(User32.GetAllWindowsWithClassName("CabinetWClass"));
            windows.AddRange(User32.GetAllWindowsWithClassName("ExploreWClass"));
            return windows;
        }

        /// <summary>
        /// Retrieves the process id for the explorer.exe instance that is running
        /// in the system tray.
        /// </summary>
        /// <returns></returns>
        public static int GetTaskbarProcessId()
        {
            uint processId = 0;

            IntPtr trayHwnd = User32.FindWindow("Shell_TrayWnd");
            if (trayHwnd != IntPtr.Zero)
            {
                User32.GetWindowThreadProcessId(trayHwnd, out processId);
            }

            return (int)processId;
        }

        //Shell_TrayWnd
        #endregion // Controller Methods
    }

} // RSG.Interop.Microsoft namespace
