﻿using System;
using System.Runtime.Serialization;

namespace RSG.Interop.Microsoft
{

    /// <summary>
    /// Internet Explorer version identifier enumeration.
    /// </summary>
    /// <![CDATA[http://msdn.microsoft.com/en-us/library/ee330730%28VS.85%29.aspx#browser_emulation]]>
    [DataContract]
    public enum InternetExplorerVersion
    {
        /// <summary>
        /// Default (usually IE7)
        /// </summary>
        [EnumMember]
        Default = 0,

        /// <summary>
        /// Internet explorer 7 with web pages containing standards-based !DOCTYPE directives.
        /// </summary>
        [EnumMember]
        IE7 = 7000,

        /// <summary>
        /// Internet explorer 8 with web pages containing standards-based !DOCTYPE directives.
        /// </summary>
        [EnumMember]
        IE8DocType = 8000,

        /// <summary>
        /// Internet Explorer 8.
        /// </summary>
        [EnumMember]
        IE8 = 8888,

        /// <summary>
        /// Internet explorer 9 with web pages containing standards-based !DOCTYPE directives.
        /// </summary>
        [EnumMember]
        IE9DocType = 9000,

        /// <summary>
        /// Internet Explorer 9.
        /// </summary>
        [EnumMember]
        IE9 = 9999,

        /// <summary>
        /// Internet Explorer 10 with web pages containing standard-based !DOCTYPE directives.
        /// </summary>
        [EnumMember]
        IE10DocType = 10000,

        /// <summary>
        /// Internet Explorer 10
        /// </summary>
        [EnumMember]
        IE10 = 10001
    }

} // RSG.Interop.Microsoft namespace
