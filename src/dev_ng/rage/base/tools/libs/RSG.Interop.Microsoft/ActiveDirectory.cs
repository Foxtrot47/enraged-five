﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.DirectoryServices;

namespace RSG.Interop.Microsoft
{

    /// <summary>
    /// Simple ActiveDirectory class; methods for authenticating users and
    /// reading their properties from Active Directory.
    /// </summary>
    public static class ActiveDirectory
    {
        /// <summary>
        /// Authenticate user against Active Directory.
        /// </summary>
        /// <param name="username">Username</param>
        /// <param name="password">User's password</param>
        /// <param name="domain">Domain</param>
        /// <returns></returns>
        public static bool AuthenticateUser(String username, String password, String domain)
        {
            bool authenticated = false;
            try
            {
                using (DirectoryEntry entry = new DirectoryEntry(String.Format("LDAP://{0}", domain),
                    username, password))
                {
                    Object nativeObject = entry.NativeObject;
                    authenticated = true;
                }
            }
            catch (DirectoryServicesCOMException)
            {
            }

            return (authenticated);
        }

        /// <summary>
        /// Read Active Directory user properties.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="domain"></param>
        /// <param name="userProperties"></param>
        /// <returns></returns>
        public static bool TryGetUserProperties(String username, String password, String domain, 
            out IDictionary<String, Object> userProperties)
        {
            bool authenticated = false;
            userProperties = new Dictionary<String, Object>();
            if (String.IsNullOrWhiteSpace(username) || String.IsNullOrWhiteSpace(password))
            {
                return false;
            }

            try
            {
                using (DirectoryEntry entry = new DirectoryEntry(String.Format("LDAP://{0}", domain),
                    username, password))
                {
                    Object nativeObject = entry.NativeObject;
                    authenticated = true;

                    using (DirectorySearcher adSearch = new DirectorySearcher(entry))
                    {
                        adSearch.Filter = "(&(objectCategory=user)(sAMAccountName=" + username + "))";

                        SearchResultCollection results = adSearch.FindAll();
                        Debug.Assert(1 == results.Count, "LDAP search failed.  Internal error.");

                        if (1 == results.Count)
                        {
                            SearchResult result = results[0];
                            foreach (String singleAttribute in ((ResultPropertyCollection)result.Properties).PropertyNames)
                            {
                                userProperties.Add(singleAttribute, ((ResultPropertyCollection)result.Properties)[singleAttribute]);
                            }
                        }
                    }
                }
            }
            catch (DirectoryServicesCOMException)
            {
            }
            return (authenticated);
        }
    }

} // RSG.Interop.Microsoft namespace
