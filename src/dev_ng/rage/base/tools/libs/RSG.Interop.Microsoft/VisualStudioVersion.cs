﻿using System;
using System.Runtime.Serialization;

namespace RSG.Interop.Microsoft
{

    /// <summary>
    /// Visual Studio version identifier enumeration.
    /// </summary>
    [DataContract]
    public enum VisualStudioVersion
    {
        /// <summary>
        /// Unknown version.
        /// </summary>
        [EnumMember]
        Unknown,

        /// <summary>
        /// VS2005 - 8.0
        /// </summary>
        [EnumMember]
        VS2005,

        /// <summary>
        /// VS2008 - 9.0
        /// </summary>
        [EnumMember]
        VS2008,

        /// <summary>
        /// VS2010 - 10.0
        /// </summary>
        [EnumMember]
        VS2010,

        /// <summary>
        /// VS2012 - 11.0
        /// </summary>
        [EnumMember]
        VS2012,

        /// <summary>
        /// VS2013 - 12.0
        /// </summary>
        [EnumMember]
        VS2013,
    }

} // RSG.Interop.Microsoft namespace
