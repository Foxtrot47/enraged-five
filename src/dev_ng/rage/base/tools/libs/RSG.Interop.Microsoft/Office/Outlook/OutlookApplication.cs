﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace RSG.Interop.Microsoft.Office.Outlook
{
    /// <summary>
    /// Wrapper for Outlook.Application
    /// </summary>
    public static class OutlookApplication
    {
        /// <summary>
        /// Creates a new outlook IApplication wrapper object.
        /// </summary>
        /// <returns></returns>
        public static IApplication Create()
        {
            return (IApplication)COMWrapper.CreateInstance(typeof(IApplication));
        }

        /// <summary>
        /// Check to see if outlook is installed on the machine.
        /// </summary>
        /// <returns></returns>
        public static bool IsOutlookInstalled()
        {
            if (!_outlookInstalled.HasValue)
            {
                lock (_syncObject)
                {
                    if (!_outlookInstalled.HasValue)
                    {
                        try
                        {
                            // Try to create an instance of the outlook application.
                            // If this succeeds then outlook exists on the machine.
                            // If an exception is thrown we assume it doesn't exist.
                            IApplication app = Create();
                            app.Dispose();
                            _outlookInstalled = true;
                        }
                        catch (COMException)
                        {
                            _outlookInstalled = false;
                        }
                    }
                }
            }

            return _outlookInstalled.Value;
        }

        /// <summary>
        /// Cached result of whether outlook is installed (so we don't create/release the com object each time we query it).
        /// </summary>
        private static bool? _outlookInstalled;

        /// <summary>
        /// Threading synchronisation object.
        /// </summary>
        private static object _syncObject = new object();
    } // OutlookApplication
}
