﻿using RSG.Base.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Interop.Microsoft.Office.Lync
{
    /// <summary>
    /// Lync versions
    /// </summary>
    public enum LyncVersion
    {
        /// <summary>
        /// No lync installed
        /// </summary>
        [EnumMember]
        None,

        /// <summary>
        /// Lync 2010
        /// </summary>
        [EnumMember]
        [FieldDisplayName("Lync 2010")]
        [RegistryKey(@"Software\IM Providers\Communicator", true)]
        Lync2010,

        /// <summary>
        /// Lync 2013
        /// </summary>
        [EnumMember]
        [FieldDisplayName("Lync 2013")]
        [RegistryKey(@"Software\IM Providers\Lync", true)]
        Lync2013,
    }

    /// <summary>
    /// Extension methods for the lync version enum
    /// </summary>
    public static class LyncVersionExtensions
    {
        /// <summary>
        /// Return Photoshop version Registry Key.
        /// </summary>
        /// <param name="version"></param>
        /// <returns></returns>
        public static String RegistryKey(this LyncVersion version)
        {
            Type type = version.GetType();

            FieldInfo fi = type.GetField(version.ToString());
            RegistryKeyAttribute[] attributes =
                fi.GetCustomAttributes(typeof(RegistryKeyAttribute), false) as RegistryKeyAttribute[];

            RegistryKeyAttribute attribute = attributes.Where(a => a.Is64Bit == true).FirstOrDefault();
            if (null != attribute)
                return (attribute.Key);
            else
                throw (new ArgumentException("Lync version has no Registry Key defined."));
        }
    }
}
