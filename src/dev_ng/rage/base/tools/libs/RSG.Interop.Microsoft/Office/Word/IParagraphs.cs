﻿//---------------------------------------------------------------------------------------------
// <copyright file="IParagraphs.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Office.Word
{
    /// <summary>
    /// Represents a collection of IParagraph objects that represent the paragraphs in the
    /// specified selection, range, or document.
    /// </summary>
    public interface IParagraphs
    {
        #region Properties
        /// <summary>
        /// Gets or sets a constant that represents the alignment for the specified paragraphs.
        /// </summary>
        Alignment Alignment { get; set; }

        /// <summary>
        /// Gets the number of paragraphs in the collection.
        /// </summary>
        int Count { get; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a IParagraph object in this collection and returns the newly created
        /// object.
        /// </summary>
        /// <returns>
        /// The IParagraph object that was added to this collection.
        /// </returns>
        IParagraph Add();

        /// <summary>
        /// Returns an individual IParagraph object in this collection.
        /// </summary>
        /// <param name="index">
        /// The individual object to be returned. Can be a Long indicating the ordinal position
        /// of the individual object.
        /// </param>
        /// <returns>
        /// The individual IParagraph object in this collection.
        /// </returns>
        IParagraph Item(object index);
        #endregion Methods
    } // RSG.Interop.Microsoft.Office.Word.IParagraphs {Interface}
} // RSG.Interop.Microsoft.Office.Word {Namespace}
