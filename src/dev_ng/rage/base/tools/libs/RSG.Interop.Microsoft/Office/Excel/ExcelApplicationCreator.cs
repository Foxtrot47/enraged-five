﻿//---------------------------------------------------------------------------------------------
// <copyright file="ExcelApplicationCreator.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Office.Excel
{
    using System.Runtime.InteropServices;

    /// <summary>
    /// A static class used to create a instance of Microsoft Excel using COM.
    /// </summary>
    public static class ExcelApplicationCreator
    {
        #region Fields
        /// <summary>
        /// A cached result of whether excel is installed.
        /// </summary>
        private static bool? _wordInstalled;

        /// <summary>
        /// A generic object used to support threading synchronisation.
        /// </summary>
        private static object _syncObject = new object();
        #endregion Fields

        #region Properties
        /// <summary>
        /// Check to see if Excel is installed on the machine.
        /// </summary>
        /// <returns>
        /// True if Excel is installed; otherwise, false.
        /// </returns>
        public static bool IsExcelInstalled()
        {
            if (!_wordInstalled.HasValue)
            {
                lock (_syncObject)
                {
                    if (!_wordInstalled.HasValue)
                    {
                        try
                        {
                            // Try to create an instance of the outlook application.
                            // If this succeeds then outlook exists on the machine.
                            // If an exception is thrown we assume it doesn't exist.
                            Create();
                            _wordInstalled = true;
                        }
                        catch (COMException)
                        {
                            _wordInstalled = false;
                        }
                    }
                }
            }

            return _wordInstalled.Value;
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new Excel IApplication wrapper object.
        /// </summary>
        /// <returns>
        /// A new IApplication wrapper object for Excel.
        /// </returns>
        public static ExcelApplication Create()
        {
            IApplication app = (IApplication)COMWrapper.CreateInstance(typeof(IApplication));
            return new ExcelApplication(app);
        }
        #endregion Methods
    } // RSG.Interop.Microsoft.Office.Excel.Excel.ApplicationCreator {Class}
} // RSG.Interop.Microsoft.Office.Excel {Namespace}
