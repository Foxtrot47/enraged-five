﻿//---------------------------------------------------------------------------------------------
// <copyright file="AutoFilterOperator.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Office.Excel
{
    /// <summary>
    /// Specifies the operator to use to associate two criteria applied by a filter.
    /// </summary>
    public enum AutoFilterOperator
    {
        /// <summary>
        /// Logical AND of Criteria1 and Criteria2.
        /// </summary>
        And = 1,

        /// <summary>
        /// Lowest-valued items displayed (number of items specified in Criteria1).
        /// </summary>
        Bottom10Items = 4,

        /// <summary>
        /// Lowest-valued items displayed (percentage specified in Criteria1).
        /// </summary>
        Bottom10Percent = 6,

        /// <summary>
        /// Colour of the cell.
        /// </summary>
        FilterCellColor = 8,

        /// <summary>
        /// Dynamic filter.
        /// </summary>
        FilterDynamic = 11,

        /// <summary>
        /// Colour of the font.
        /// </summary>
        FilterFontColor = 9,

        /// <summary>
        /// Filter icon.
        /// </summary>
        FilterIcon = 10,

        /// <summary>
        /// Filter values.
        /// </summary>
        FilterValues = 7,

        /// <summary>
        /// Logical OR of Criteria1 or Criteria2.
        /// </summary>
        Or = 2,

        /// <summary>
        /// Highest-valued items displayed (number of items specified in Criteria1).
        /// </summary>
        Top10Items = 3,

        /// <summary>
        /// Highest-valued items displayed (percentage specified in Criteria1).
        /// </summary>
        Top10Percent = 5
    } // RSG.Interop.Microsoft.Office.Excel.AutoFilterOperator {Enum}
} // RSG.Interop.Microsoft.Office.Excel {Namespace}
