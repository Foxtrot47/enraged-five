﻿//---------------------------------------------------------------------------------------------
// <copyright file="IApplication.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Office.Word
{
    /// <summary>
    /// The COM wrapper for a Microsoft Word application instance.
    /// </summary>
    [ComProgId("Word.Application")]
    internal interface IApplication
    {
        #region Properties
        /// <summary>
        /// Gets a IDocument object that represents the active document (the document with
        /// the focus). If there are no documents open, an error occurs.
        /// </summary>
        IDocument ActiveDocument { get; }

        /// <summary>
        /// Gets a collection containing all of the currently opened documents.
        /// </summary>
        IDocuments Documents { get; }

        /// <summary>
        /// Gets the name of the specified object.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets the Microsoft Word version number.
        /// </summary>
        string Version { get; }

        /// <summary>
        /// Gets or sets a value indicating whether the application is visible to the user.
        /// </summary>
        bool Visible { get; set; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Quits Microsoft Word.
        /// </summary>
        void Quit();
        #endregion Methods
    } // RSG.Interop.Microsoft.Office.Word.IApplication {Interface}
} // RSG.Interop.Microsoft.Office.Word {Namespace}
