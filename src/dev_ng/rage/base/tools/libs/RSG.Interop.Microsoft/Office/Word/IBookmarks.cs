﻿//---------------------------------------------------------------------------------------------
// <copyright file="IBookmarks.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Office.Word
{
    /// <summary>
    /// Represents a collection of IBookmark objects that represent the bookmarks in the
    /// specified selection, range, or document.
    /// </summary>
    internal interface IBookmarks
    {
        #region Properties
        /// <summary>
        /// Gets the number of bookmarks in the collection.
        /// </summary>
        int Count { get; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a IBookmark object in this collection and returns the newly created object.
        /// </summary>
        /// <param name="name">
        /// The name of the bookmark. The name cannot be more than one word.
        /// </param>
        /// <returns>
        /// The IBookmark object that was added to this collection.
        /// </returns>
        IBookmark Add(string name);

        /// <summary>
        /// Determines whether the specified bookmark exists. Returns true if the bookmark
        /// exists; otherwise, false.
        /// </summary>
        /// <param name="name">
        /// The name of the bookmark.
        /// </param>
        /// <returns>
        /// True if the bookmark exists; otherwise, false.
        /// </returns>
        bool Exists(string name);

        /// <summary>
        /// Returns an individual IBookmark object from this collection.
        /// </summary>
        /// <param name="name">
        /// The name of the bookmark to retrieve.
        /// </param>
        /// <returns>
        /// The bookmark within this collection with the specified name.
        /// </returns>
        IBookmark Item(string name);
        #endregion Methods
    } // RSG.Interop.Microsoft.Office.Word.IBookmarks {Interface}
} // RSG.Interop.Microsoft.Office.Word {Namespace}
