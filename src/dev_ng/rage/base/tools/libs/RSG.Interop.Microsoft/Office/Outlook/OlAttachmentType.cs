﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Interop.Microsoft.Office.Outlook
{
    /// <summary>
    /// Specifies the attachment type.
    /// </summary>
    public enum OlAttachmentType
    {
        /// <summary>
        /// The attachment is a copy of the original file and can be accessed even if the original file is removed.
        /// </summary>
        olByValue = 1,
        
        /// <summary>
        /// This value is no longer supported since Microsoft Outlook 2007. Use olByValue to attach a copy of a file in the file system.
        /// </summary>
        olByReference = 4,

        /// <summary>
        /// The attachment is an Outlook message format file (.msg) and is a copy of the original message.
        /// </summary>
        olEmbeddeditem = 5,

        /// <summary>
        /// The attachment is an OLE document.
        /// </summary>
        olOLE = 6
    } // OlAttachmentType
#pragma warning restore 1591
}
