﻿//---------------------------------------------------------------------------------------------
// <copyright file="ExcelWorksheets.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Office.Excel
{
    using System.Reflection;

    /// <summary>
    /// Represents a collection of all the Excel Worksheet objects that are currently open
    /// inside a Excel Workbook.
    /// </summary>
    public class ExcelWorksheets
    {
        #region Fields
        /// <summary>
        /// The reference to the Excel ISheets proxy object.
        /// </summary>
        private IWorksheets _sheets;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ExcelWorksheets"/> class.
        /// </summary>
        /// <param name="sheets">
        /// The ISheets proxy object.
        /// </param>
        internal ExcelWorksheets(IWorksheets sheets)
        {
            this._sheets = sheets;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the number of sheets in the collection.
        /// </summary>
        public int Count
        {
            get { return this._sheets.Count; }
        }

        /// <summary>
        /// Gets the worksheet whose position is specified as a index.
        /// </summary>
        /// <param name="index">
        /// The ordinal position of the individual worksheet to retrieve.
        /// </param>
        /// <returns>
        /// The worksheet whose position is specified.
        /// </returns>
        public ExcelWorksheet this[int index]
        {
            get
            {
                object indexObject = index;
                return new ExcelWorksheet(this._sheets[indexObject]);
            }
        }

        /// <summary>
        /// Gets the worksheet with the specified name.
        /// </summary>
        /// <param name="name">
        /// The name of the worksheet to retrieve.
        /// </param>
        /// <returns>
        /// The worksheet whose name is specified.
        /// </returns>
        public ExcelWorksheet this[string name]
        {
            get { return new ExcelWorksheet(this._sheets[name]); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new, empty Document object and adds it to the collection of opened
        /// sheets.
        /// </summary>
        /// <param name="name">
        /// The name of the worksheet to add.
        /// </param>
        /// <returns>
        /// The document that was added to the collection.
        /// </returns>
        public ExcelWorksheet Add(string name)
        {
            ExcelWorksheet worksheet = new ExcelWorksheet(this._sheets.Add());
            worksheet.Name = name;
            return worksheet;
        }
        #endregion Methods
    } // RSG.Interop.Microsoft.Office.Excel.ExcelSheets {Class}
} // RSG.Interop.Microsoft.Office.Excel {Namespace}
