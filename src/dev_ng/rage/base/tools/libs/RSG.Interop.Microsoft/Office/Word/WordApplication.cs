﻿//---------------------------------------------------------------------------------------------
// <copyright file="WordApplication.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Office.Word
{
    /// <summary>
    /// Represents the entire Microsoft Word application.
    /// </summary>
    public class WordApplication
    {
        #region Fields
        /// <summary>
        /// The reference to the work IApplication proxy object.
        /// </summary>
        private IApplication _application;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="WordApplication"/> class.
        /// </summary>
        /// <param name="application">
        /// The IApplication proxy object.
        /// </param>
        internal WordApplication(IApplication application)
        {
            this._application = application;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets a word document object that represents the active document (the document with
        /// the focus). If there are no documents open, null is returned.
        /// </summary>
        public WordDocument ActiveDocument
        {
            get
            {
                if (this._application.Documents.Count != 0)
                {
                    return new WordDocument(this._application.ActiveDocument);
                }

                return null;
            }
        }

        /// <summary>
        /// Gets a collection containing all of the currently opened documents.
        /// </summary>
        public WordDocuments Documents
        {
            get { return new WordDocuments(this._application.Documents); }
        }

        /// <summary>
        /// Gets the name of the specified object.
        /// </summary>
        public string Name
        {
            get { return this._application.Name; }
        }

        /// <summary>
        /// Gets the Microsoft Word version number.
        /// </summary>
        public string Version
        {
            get { return this._application.Version; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the application is visible to the user.
        /// </summary>
        public bool Visible
        {
            get { return this._application.Visible; }
            set { this._application.Visible = true; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Quits Microsoft Word.
        /// </summary>
        public void Quit()
        {
            this._application.Quit();
        }
        #endregion Methods
    } // RSG.Interop.Microsoft.Office.Word.WordApplication {Class}
} // RSG.Interop.Microsoft.Office.Word {Namespace}
