﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Interop.Microsoft.Office.Outlook
{
#pragma warning disable 1591
    /// <summary>
    /// 
    /// </summary>
    public enum OlItemType
    {
        olAppointmentItem = 1,
        olContactItem = 2,
        olDistributionListItem = 7,
        olJournalItem = 4,
        olMailItem = 0,
        olNoteItem = 5,
        olPostItem = 6,
        olTaskItem = 3
    } // OlItemType
#pragma warning restore 1591
}
