﻿//---------------------------------------------------------------------------------------------
// <copyright file="IWorksheet.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Office.Excel
{
    /// <summary>
    /// Represents a worksheet object which is apart of a sheets collection.
    /// </summary>
    internal interface IWorksheet
    {
        #region Properties
        /// <summary>
        /// Gets or sets the name of the worksheet.
        /// </summary>
        string Name { get; set; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Deletes the object.
        /// </summary>
        void Delete();

        /// <summary>
        /// Returns a IRange object that represents a cell or a range of cells.
        /// </summary>
        /// <param name="cell">
        /// The name of the range. This must be an A1-style reference in the language of the
        /// macro. It can include the range operator (a colon), the intersection operator
        /// (a space), or the union operator (a comma).
        /// </param>
        /// <returns>
        /// A IRange object that represents a cell or a range of cells.
        /// </returns>
        IRange get_Range(string cell);
        #endregion Methods
    } // RSG.Interop.Microsoft.Office.Excel.IWorksheet {Interface}
} // RSG.Interop.Microsoft.Office.Excel {Namespace}
