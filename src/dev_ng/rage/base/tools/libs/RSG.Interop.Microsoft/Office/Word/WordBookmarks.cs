﻿//---------------------------------------------------------------------------------------------
// <copyright file="WordBookmarks.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Office.Word
{
    using System.Reflection;

    /// <summary>
    /// Represents a collection of all the WordDocument objects that are currently open inside
    /// a Word Application.
    /// </summary>
    public class WordBookmarks
    {
        #region Fields
        /// <summary>
        /// The reference to the word IBookmarks proxy object.
        /// </summary>
        private IBookmarks _bookmarks;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="WordBookmarks"/> class.
        /// </summary>
        /// <param name="bookmarks">
        /// The IBookmarks proxy object.
        /// </param>
        internal WordBookmarks(IBookmarks bookmarks)
        {
            this._bookmarks = bookmarks;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the number of documents in the collection.
        /// </summary>
        public int Count
        {
            get { return (int)this._bookmarks.Count; }
        }

        /// <summary>
        /// Gets the bookmark whose name is specified.
        /// </summary>
        /// <param name="name">
        /// The name of the individual bookmark to retrieve.
        /// </param>
        /// <returns>
        /// The bookmark whose name is specified or null if a bookmark with the specified name
        /// doesn't exist.
        /// </returns>
        public WordBookmark this[string name]
        {
            get
            {
                if (this._bookmarks.Exists(name))
                {
                    return new WordBookmark(this._bookmarks.Item(name));
                }

                return null;
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a Bookmark object in this collection and returns the newly created object.
        /// </summary>
        /// <param name="name">
        /// The name of the bookmark. The name cannot be more than one word.
        /// </param>
        /// <returns>
        /// The Bookmark object that was added to this collection.
        /// </returns>
        public WordBookmark Add(string name)
        {
            return new WordBookmark(this._bookmarks.Add(name));
        }

        /// <summary>
        /// Determines whether the specified bookmark exists. Returns true if the bookmark
        /// exists; otherwise, false.
        /// </summary>
        /// <param name="name">
        /// The name of the bookmark.
        /// </param>
        /// <returns>
        /// True if the bookmark exists; otherwise, false.
        /// </returns>
        public bool Exists(string name)
        {
            return this._bookmarks.Exists(name);
        }
        #endregion Methods
    } // RSG.Interop.Microsoft.Office.Word.WordBookmarks {Class}
} // RSG.Interop.Microsoft.Office.Word {Namespace}
