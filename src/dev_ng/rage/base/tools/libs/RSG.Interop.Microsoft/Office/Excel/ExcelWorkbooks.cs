﻿//---------------------------------------------------------------------------------------------
// <copyright file="ExcelWorkbooks.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Office.Excel
{
    using System.Reflection;

    /// <summary>
    /// Represents a collection of all the Excel  objects that are currently open inside
    /// a Excel Application.
    /// </summary>
    public class ExcelWorkbooks
    {
        #region Fields
        /// <summary>
        /// The reference to the Excel IWorkbooks proxy object.
        /// </summary>
        private IWorkbooks _workbooks;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ExcelWorkbooks"/> class.
        /// </summary>
        /// <param name="sheets">
        /// The IWorkbooks proxy object.
        /// </param>
        internal ExcelWorkbooks(IWorkbooks sheets)
        {
            this._workbooks = sheets;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the number of sheets in the collection.
        /// </summary>
        public int Count
        {
            get { return this._workbooks.Count; }
        }

        /// <summary>
        /// Gets the document whose ordinal position is specified as a index.
        /// </summary>
        /// <param name="index">
        /// The ordinal position of the individual document to retrieve.
        /// </param>
        /// <returns>
        /// The document whose ordinal position is specified.
        /// </returns>
        public ExcelWorkbook this[int index]
        {
            get
            {
                object itemIndex = (long)index;
                return new ExcelWorkbook(this._workbooks.Item(itemIndex));
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new workbook object with a single worksheet in it using the specified
        /// name and adds it to this collection.
        /// </summary>
        /// <param name="worksheetName">
        /// The name of the single worksheet inside the new book.
        /// </param>
        /// <returns>
        /// The single worksheet object inside the created workbook.
        /// </returns>
        public ExcelWorksheet Add(string worksheetName)
        {
            ExcelWorkbook workbook = new ExcelWorkbook(this._workbooks.Add());
            ExcelWorksheet worksheet = workbook.Worksheets.Add(worksheetName);
            for (int i = workbook.Worksheets.Count; i > 1; i--)
            {
                workbook.Worksheets[i].Delete();
            }

            return worksheet;
        }
        #endregion Methods
    } // RSG.Interop.Microsoft.Office.Excel.ExcelWorkbooks {Class}
} // RSG.Interop.Microsoft.Office.Excel {Namespace}
