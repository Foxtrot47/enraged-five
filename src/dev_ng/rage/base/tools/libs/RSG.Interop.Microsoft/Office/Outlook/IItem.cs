﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Interop.Microsoft.Office.Outlook
{
#pragma warning disable 1591
    /// <summary>
    /// 
    /// </summary>
    public interface IItem : ICommon
    {
        #region Properties
        IAttachments Attachments { get; }
        string Body { get; set; }
        DateTime CreationTime { get; }
        DateTime LastModificationTime { get; }
        string MessageClass { get; set; }
        bool NoAging { get; set; }
        int OutlookInternalVersion { get; }
        string OutlookVersion { get; }
        bool Saved { get; }
        int Size { get; }
        string Subject { get; set; }
        bool UnRead { get; set; }
        DateTime ExpiryTime { get; set; }
        string HTMLBody { get; set; }
        DateTime ReceivedTime { get; }
        string SenderName { get; }
        DateTime SentOn { get; }
        OlBodyFormat BodyFormat { get; set; }
        string SenderEmailAddress { get; }
        string SenderEmailType { get; }
        #endregion // Properties

        #region Methods
        void Close(OlInspectorClose SaveMode);
        object Copy();
        void Delete();
        void Display(object Modal);
        void PrintOut();
        void Save();
        void SaveAs(string Path, object Type);
        #endregion // Methods
    } // IItem
#pragma warning restore 1591
}
