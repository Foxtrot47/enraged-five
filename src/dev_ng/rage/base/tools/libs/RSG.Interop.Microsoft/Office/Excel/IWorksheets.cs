﻿//---------------------------------------------------------------------------------------------
// <copyright file="IWorksheets.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Office.Excel
{
    using System.Collections;

    /// <summary>
    /// A collection of all the Excel Worksheet objects that are currently open inside a Excel
    /// Workbook.
    /// </summary>
    internal interface IWorksheets
    {
        #region Properties
        /// <summary>
        /// Gets the number of work sheets in the collection.
        /// </summary>
        int Count { get; }

        /// <summary>
        /// Gets a single worksheet from this collection.
        /// </summary>
        /// <param name="index">
        /// The name or index number of the object.
        /// </param>
        /// <returns>
        /// A single worksheet from this collection.
        /// </returns>
        IWorksheet this[object index] { get; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new worksheet, chart, or macro sheet.
        /// </summary>
        /// <returns>
        /// The object that was added to this collection.
        /// </returns>
        IWorksheet Add();
        #endregion Methods
    } // RSG.Interop.Microsoft.Office.Excel.IWorksheets {Interface}
} // RSG.Interop.Microsoft.Office.Excel {Namespace}
