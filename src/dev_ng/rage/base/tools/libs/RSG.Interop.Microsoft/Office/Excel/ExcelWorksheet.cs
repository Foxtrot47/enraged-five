﻿//---------------------------------------------------------------------------------------------
// <copyright file="ExcelWorksheet.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Office.Excel
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;

    /// <summary>
    /// Represents a worksheet object which is apart of a worksheets collection.
    /// </summary>
    public class ExcelWorksheet
    {
        #region Fields
        /// <summary>
        /// The reference to the Excel IWorksheet proxy object.
        /// </summary>
        private IWorksheet _worksheet;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ExcelWorksheet"/> class.
        /// </summary>
        /// <param name="worksheet">
        /// The IWorksheet proxy object.
        /// </param>
        internal ExcelWorksheet(IWorksheet worksheet)
        {
            this._worksheet = worksheet;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the name of the worksheet.
        /// </summary>
        public string Name
        {
            get { return this._worksheet.Name; }
            set { this._worksheet.Name = value; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Returns a IRange object that represents a cell or a range of cells.
        /// </summary>
        /// <param name="cell">
        /// The name of the range. This must be an A1-style reference in the language of the
        /// macro. It can include the range operator (a colon), the intersection operator
        /// (a space), or the union operator (a comma).
        /// </param>
        /// <returns>
        /// A IRange object that represents the specified cells.
        /// </returns>
        public IRange Range(string cell)
        {
            return this._worksheet.get_Range(cell);
        }

        /// <summary>
        /// Sets the headers of a worksheet csv file. This not only sets the text of the first
        /// row but also auto fits the columns and adds the auto filter option to the header.
        /// </summary>
        /// <param name="headers">
        /// The header titles.
        /// </param>
        public void SetCsvHeader(params string[] headers)
        {
            int index = 0;
            foreach (string header in headers)
            {
                string rangeCode = this.GetRangeStringFromIndex(index) + "1";
                IRange range = this.Range(rangeCode);
                range.Value2 = header;
                range.EntireColumn.AutoFit();

                double width = (double)range.EntireColumn.ColumnWidth + 4.0d;
                width = Math.Min(90.0d, width);
                range.EntireColumn.ColumnWidth = width;

                index++;
            }

            IRange headerRange = this.Range("A1").EntireRow;
            headerRange.AutoFilter(
                1, Type.Missing, AutoFilterOperator.And, Type.Missing, true);
        }

        /// <summary>
        /// Sets the cell data for a single row.
        /// </summary>
        /// <param name="index">
        /// The index of the row the data is being set on.
        /// </param>
        /// <param name="values">
        /// The values to set the individual cells.
        /// </param>
        public void SetCsvRow(int index, params string[] values)
        {
            object[,] workingValues = new object[1, values.Length];
            for (int i = 0; i < values.Length; i++)
            {
                workingValues[0, i] = values[i];
            }

            string rowCode = (index + 2).ToString();
            string rangeCode = this.GetRangeStringFromIndex(0) + rowCode;
            rangeCode += ":" + this.GetRangeStringFromIndex(values.Length - 1) + rowCode;

            IRange range = this.Range(rangeCode);
            range.Value2 = workingValues;
        }

        /// <summary>
        /// Sets the cell data for a specified number of rows and columns.
        /// </summary>
        /// <param name="rowIndex">
        /// The index of the first row.
        /// </param>
        /// <param name="rows">
        /// The number of rows the data covers.
        /// </param>
        /// <param name="columns">
        /// The number of columns the data covers.
        /// </param>
        /// <param name="values">
        /// The values to set the individual cells.
        /// </param>
        public void SetCsvRows(int rowIndex, int rows, int columns, List<string> values)
        {
            object[,] workingValues = new object[rows, columns];
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    int valuesIndex = (i * columns) + j;
                    if (values.Count <= valuesIndex)
                    {
                        workingValues[i, j] = null;
                    }
                    else
                    {
                        workingValues[i, j] = values[valuesIndex];
                    }
                }
            }

            string rowCode = (rowIndex + 2).ToString();
            string rangeCode = this.GetRangeStringFromIndex(0) + rowCode.ToString();
            rowCode = (rowIndex + 1 + rows).ToString();
            rangeCode += ":" + this.GetRangeStringFromIndex(columns - 1) + rowCode;

            IRange range = this.Range(rangeCode);
            range.Value2 = workingValues;
        }

        /// <summary>
        /// Removes all of the worksheet objects from this collection.
        /// </summary>
        internal void Delete()
        {
            this._worksheet.Delete();
        }

        /// <summary>
        /// Gets the excel range string equivalent for the specified index. This starts
        /// with 0 - AA and ends with 675 - ZZ.
        /// </summary>
        /// <param name="index">
        /// The index to convert.
        /// </param>
        /// <returns>
        /// The excel range string equivalent for the specified index.
        /// </returns>
        private string GetRangeStringFromIndex(int index)
        {
            int remainder = index;
            int division = 0;
            while (remainder > 25)
            {
                division++;
                remainder -= 26;
            }

            string result = String.Empty;
            if (division != 0)
            {
                result += (char)(65 + division);
            }

            result += (char)(65 + remainder);
            return result;
        }
        #endregion Methods
    } // RSG.Interop.Microsoft.Office.Excel.ExcelWorksheet {Class}
} // RSG.Interop.Microsoft.Office.Excel {Namespace}
