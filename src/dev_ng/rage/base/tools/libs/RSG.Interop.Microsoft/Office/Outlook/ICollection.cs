﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Interop.Microsoft.Office.Outlook
{
#pragma warning disable 1591
    /// <summary>
    /// 
    /// </summary>
    public interface ICollection : ICommon, IEnumerable
    {
        int Count { get; }
        void Remove(int index);
    } // ICollection
#pragma warning restore 1591
}
