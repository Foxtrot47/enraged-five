﻿//---------------------------------------------------------------------------------------------
// <copyright file="Alignment.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Office.Word
{
    /// <summary>
    /// Specifies the alignment of a paragraph.
    /// </summary>
    public enum Alignment
    {
        /// <summary>
        /// Centre aligned.
        /// </summary>
        AlignParagraphCenter = 1,

        /// <summary>
        /// Paragraph characters are distributed to fill the entire width of the paragraph.
        /// </summary>
        AlignParagraphDistribute = 4,

        /// <summary>
        /// Fully justified.
        /// </summary>
        AlignParagraphJustify = 3,

        /// <summary>
        /// Justified with a high character compression ratio.
        /// </summary>
        AlignParagraphJustifyHi = 7,

        /// <summary>
        /// Justified with a low character compression ratio.
        /// </summary>
        AlignParagraphJustifyLow = 8,

        /// <summary>
        /// Justified with a medium character compression ratio.
        /// </summary>
        AlignParagraphJustifyMed = 5,

        /// <summary>
        /// Left aligned.
        /// </summary>
        AlignParagraphLeft = 0,

        /// <summary>
        /// Right aligned.
        /// </summary>
        AlignParagraphRight = 2,

        /// <summary>
        /// Justified according to Thai formatting layout.
        /// </summary>
        AlignParagraphThaiJustify = 9
    } // RSG.Interop.Microsoft.Office.Word.Alignment {Enum}
} // RSG.Interop.Microsoft.Office.Word {Namespace}
