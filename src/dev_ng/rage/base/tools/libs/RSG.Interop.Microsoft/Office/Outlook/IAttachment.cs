﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Interop.Microsoft.Office.Outlook
{
#pragma warning disable 1591
    /// <summary>
    /// 
    /// </summary>
    public interface IAttachment : ICommon
    {
        string DisplayName { get; set; }
        string FileName { get; }
        int Index { get; }
        object MAPIOBJECT { get; }
        string PathName { get; }
        int Position { get; set; }
        OlAttachmentType Type { get; }
        void Delete();
        void SaveAsFile(string Path);
    } // IAttachment
#pragma warning restore 1591
}
