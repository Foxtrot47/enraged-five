﻿//---------------------------------------------------------------------------------------------
// <copyright file="IWorkbook.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Office.Excel
{
    /// <summary>
    /// Represents a workbook object which is apart of a workbooks collection.
    /// </summary>
    internal interface IWorkbook
    {
        #region Properties
        /// <summary>
        /// Gets the name of the workbook.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets a collection containing all of the work sheets in this workbook.
        /// </summary>
        IWorksheets Worksheets { get; }
        #endregion Properties
    } // RSG.Interop.Microsoft.Office.Excel.IWorkbook {Interface}
} // RSG.Interop.Microsoft.Office.Excel {Namespace}
