﻿//---------------------------------------------------------------------------------------------
// <copyright file="IDocument.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Office.Word
{
    /// <summary>
    /// Represents a document object which is apart of a documents collection.
    /// </summary>
    internal interface IDocument
    {
        #region Properties
        /// <summary>
        /// Gets the bookmark collection that represents all the bookmarks in this document.
        /// </summary>
        IBookmarks Bookmarks { get; }

        /// <summary>
        /// Gets a IRange object that represents the main document story.
        /// </summary>
        IRange Content { get; }

        /// <summary>
        /// Gets the name of the document.
        /// </summary>
        string Name { get; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a range object by using the specified starting and ending character
        /// positions.
        /// </summary>
        /// <param name="start">
        /// The starting character position.
        /// </param>
        /// <param name="end">
        /// The ending character position.
        /// </param>
        /// <returns>
        /// A range object that represents the document between the two specified positions.
        /// </returns>
        IRange Range(object start, object end);
        #endregion Methods
    } // RSG.Interop.Microsoft.Office.Word.IDocument {Interface}
} // RSG.Interop.Microsoft.Office.Word {Namespace}
