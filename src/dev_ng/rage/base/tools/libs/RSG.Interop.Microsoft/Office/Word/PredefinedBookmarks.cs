﻿//---------------------------------------------------------------------------------------------
// <copyright file="PredefinedBookmarks.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Office.Word
{
    /// <summary>
    /// Contains constant strings that represent the available predefined bookmarks for a
    /// single document.
    /// </summary>
    public static class PredefinedBookmarks
    {
        #region Fields
        /// <summary>
        /// The bookmark located at the end of the document.
        /// </summary>
        public const string End = @"\EndOfDoc";

        /// <summary>
        /// The bookmark located at the beginning of the document.
        /// </summary>
        public const string Start = @"\StartOfDoc";
        #endregion Fields
    } // RSG.Interop.Microsoft.Office.Word.PredefinedBookmarks {Class}
} // RSG.Interop.Microsoft.Office.Word {Namespace}
