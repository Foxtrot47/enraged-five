﻿//---------------------------------------------------------------------------------------------
// <copyright file="IBookmark.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Office.Word
{
    /// <summary>
    /// Represents a single bookmark in a document, selection, or range.
    /// </summary>
    internal interface IBookmark
    {
        #region Properties
        /// <summary>
        /// Gets or sets the ending character position of this bookmark.
        /// </summary>
        int End { get; set; }

        /// <summary>
        /// Gets the name of the bookmark.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets or sets the starting character position of this bookmark.
        /// </summary>
        int Start { get; set; }
        #endregion Properties
    } // RSG.Interop.Microsoft.Office.Word.IBookmark {Interface}
} // RSG.Interop.Microsoft.Office.Word {Namespace}
