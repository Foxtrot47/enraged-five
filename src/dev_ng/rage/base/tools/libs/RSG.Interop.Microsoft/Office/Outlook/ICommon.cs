﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Interop.Microsoft.Office.Outlook
{
#pragma warning disable 1591
    /// <summary>
    /// 
    /// </summary>
    public interface ICommon : IDisposable
    {
        IApplication Application { get; }
        object Parent { get; }
        OlObjectClass Class { get; }
    } // ICommon
#pragma warning restore 1591
}
