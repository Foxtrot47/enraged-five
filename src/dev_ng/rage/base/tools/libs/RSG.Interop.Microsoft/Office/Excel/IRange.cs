﻿//---------------------------------------------------------------------------------------------
// <copyright file="IRange.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Office.Excel
{
    using System.Runtime.InteropServices;

    /// <summary>
    /// Represents a cell, a row, a column, a selection of cells containing one or more
    /// contiguous blocks of cells, or a 3-D range.
    /// </summary>
    public interface IRange
    {
        #region Properties
        /// <summary>
        /// Gets or sets the width of all columns in the specified range.
        /// </summary>
        object ColumnWidth { get; set; }

        /// <summary>
        /// Gets a IRange object that represents the entire column (or columns) that contains
        /// the specified range.
        /// </summary>
        IRange EntireColumn { get; }

        /// <summary>
        /// Gets a IRange object that represents the entire row (or rows) that contains the
        /// specified range.
        /// </summary>
        IRange EntireRow { get; }

        /// <summary>
        /// Gets or sets the cell value.
        /// </summary>
        object Value2 { get; set; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Filters a list using the AutoFilter.
        /// </summary>
        /// <param name="field">
        /// The integer offset of the field on which you want to base the filter (from the left
        /// of the list; the leftmost field is field one).
        /// </param>
        /// <param name="criteria1">
        /// The criteria.
        /// </param>
        /// <param name="filter">
        /// One of the constants of AutoFilterOperator specifying the type of filter.
        /// </param>
        /// <param name="criteria2">
        /// The second criteria.
        /// </param>
        /// <param name="visibleDropDown">
        /// True to display the AutoFilter drop-down arrow for the filtered field. False to
        /// hide the AutoFilter drop-down arrow for the filtered field. True by default.
        /// </param>
        /// <returns>
        /// A auto filter object.
        /// </returns>
        object AutoFilter(
            [In, Optional] object field,
            [In, Optional] object criteria1,
            [In, Optional] AutoFilterOperator filter,
            [In, Optional] object criteria2,
            [In, Optional] object visibleDropDown);

        /// <summary>
        /// Changes the width of the columns in the range or the height of the rows in the
        /// range to achieve the best fit.
        /// </summary>
        /// <returns>
        /// A auto fit object.
        /// </returns>
        object AutoFit();

        /// <summary>
        /// Selects the object.
        /// </summary>
        void Select();
        #endregion Methods
    } // RSG.Interop.Microsoft.Office.Excel.IRange {Interface}
} // RSG.Interop.Microsoft.Office.Excel {Namespace}
