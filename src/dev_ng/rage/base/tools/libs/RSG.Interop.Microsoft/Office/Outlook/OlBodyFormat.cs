﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Interop.Microsoft.Office.Outlook
{
#pragma warning disable 1591
    /// <summary>
    /// 
    /// </summary>
    public enum OlBodyFormat
    {
        olFormatHTML = 2,
        olFormatPlain = 1,
        olFormatRichText = 3,
        olFormatUnspecified = 0
    } // OlBodyFormat
#pragma warning restore 1591
}
