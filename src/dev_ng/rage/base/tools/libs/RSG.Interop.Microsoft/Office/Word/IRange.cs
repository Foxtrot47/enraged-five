﻿//---------------------------------------------------------------------------------------------
// <copyright file="IRange.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Office.Word
{
    /// <summary>
    /// Represents a contiguous area in a document. Each Range object is defined by a starting
    /// and ending character position.
    /// </summary>
    internal interface IRange
    {
        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether the range is formatted as bold.
        /// </summary>
        int Bold { get; set; }

        /// <summary>
        /// Gets or sets the ending character position of this range.
        /// </summary>
        int End { get; set; }

        /// <summary>
        /// Gets or sets a Font object that represents the character formatting for the range.
        /// </summary>
        IFont Font { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the range is formatted as italic.
        /// </summary>
        int Italic { get; set; }

        /// <summary>
        /// Gets a IParagraphs collection that represents all the paragraphs in the specified
        /// range.
        /// </summary>
        IParagraphs Paragraphs { get; }

        /// <summary>
        /// Gets or sets the starting character position of this range.
        /// </summary>
        int Start { get; set; }

        /// <summary>
        /// Gets or sets the style for the specified object.
        /// </summary>
        object Style { get; set; }

        /// <summary>
        /// Gets or sets the text for this range.
        /// </summary>
        string Text { get; set; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Selects this range.
        /// </summary>
        void Select();
        #endregion Methods
    } // RSG.Interop.Microsoft.Office.Word.IRange {Interface}
} // RSG.Interop.Microsoft.Office.Word {Namespace}
