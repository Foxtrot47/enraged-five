﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Interop.Microsoft.Office.Outlook
{
#pragma warning disable 1591
    /// <summary>
    /// 
    /// </summary>
    public interface IRecipients : ICollection
    {
        IRecipient Add(String name);
        bool ResolveAll();
    } // IRecipients
#pragma warning restore 1591
}
