﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Interop.Microsoft.Office.Outlook
{
#pragma warning disable 1591
    /// <summary>
    /// 
    /// </summary>
    public enum OlObjectClass
    {
        olAction = 0x20,
        olActions = 0x21,
        olAddressEntries = 0x15,
        olAddressEntry = 8,
        olAddressList = 7,
        olAddressLists = 20,
        olApplication = 0,
        olAppointment = 0x1a,
        olAttachment = 5,
        olAttachments = 0x12,
        olConflict = 0x66,
        olConflicts = 0x67,
        olContact = 40,
        olDistributionList = 0x45,
        olDocument = 0x29,
        olException = 30,
        olExceptions = 0x1d,
        olExplorer = 0x22,
        olExplorers = 60,
        olFolder = 2,
        olFolders = 15,
        olFormDescription = 0x25,
        olInspector = 0x23,
        olInspectors = 0x3d,
        olItemProperties = 0x62,
        olItemProperty = 0x63,
        olItems = 0x10,
        olJournal = 0x2a,
        olLink = 0x4b,
        olLinks = 0x4c,
        olMail = 0x2b,
        olMeetingCancellation = 0x36,
        olMeetingRequest = 0x35,
        olMeetingResponseNegative = 0x37,
        olMeetingResponsePositive = 0x38,
        olMeetingResponseTentative = 0x39,
        olNamespace = 1,
        olNote = 0x2c,
        olOutlookBarGroup = 0x42,
        olOutlookBarGroups = 0x41,
        olOutlookBarPane = 0x3f,
        olOutlookBarShortcut = 0x44,
        olOutlookBarShortcuts = 0x43,
        olOutlookBarStorage = 0x40,
        olPages = 0x24,
        olPanes = 0x3e,
        olPost = 0x2d,
        olPropertyPages = 0x47,
        olPropertyPageSite = 70,
        olRecipient = 4,
        olRecipients = 0x11,
        olRecurrencePattern = 0x1c,
        olReminder = 0x65,
        olReminders = 100,
        olRemote = 0x2f,
        olReport = 0x2e,
        olResults = 0x4e,
        olSearch = 0x4d,
        olSelection = 0x4a,
        olSyncObject = 0x48,
        olSyncObjects = 0x49,
        olTask = 0x30,
        olTaskRequest = 0x31,
        olTaskRequestAccept = 0x33,
        olTaskRequestDecline = 0x34,
        olTaskRequestUpdate = 50,
        olUserProperties = 0x26,
        olUserProperty = 0x27,
        olView = 80,
        olViews = 0x4f
    } // OlObjectClass
#pragma warning disable 1591
}
