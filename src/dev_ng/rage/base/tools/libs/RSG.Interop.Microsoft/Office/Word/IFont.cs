﻿//---------------------------------------------------------------------------------------------
// <copyright file="IFont.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Office.Word
{
    /// <summary>
    /// Represents a contiguous area in a document. Each Range object is defined by a starting
    /// and ending character position.
    /// </summary>
    public interface IFont
    {
        #region Properties
        /// <summary>
        /// Gets or sets the ending character position of this range.
        /// </summary>
        int Bold { get; set; }

        /// <summary>
        /// Gets or sets the starting character position of this range.
        /// </summary>
        int Italic { get; set; }

        /// <summary>
        /// Gets or sets the font size, in points.
        /// </summary>
        float Size { get; set; }
        #endregion Properties
    } // RSG.Interop.Microsoft.Office.Word.IFont {Interface}
} // RSG.Interop.Microsoft.Office.Word {Namespace}
