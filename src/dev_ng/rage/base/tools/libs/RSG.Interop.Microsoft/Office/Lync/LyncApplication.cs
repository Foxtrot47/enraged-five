﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace RSG.Interop.Microsoft.Office.Lync
{
    /// <summary>
    /// Lync interoperability class
    /// </summary>
    public static class LyncApplication
    {
        /// <summary>
        /// Determine whether any version of Lync is installed.
        /// </summary>
        /// <returns></returns>
        public static bool IsInstalled()
        {
            bool result = false;
            IEnumerable<LyncVersion> versions =
                Enum.GetValues(typeof(LyncVersion)).Cast<LyncVersion>();
            foreach (LyncVersion version in versions)
            {
                if (LyncVersion.None != version)
                {
                    result |= (IsVersionInstalled(version));
                }
            }
            return (result);
        }

        /// <summary>
        /// Determine whether a particular version of Lync is installed.
        /// </summary>
        /// <param name="version"></param>
        /// <returns></returns>
        public static bool IsVersionInstalled(LyncVersion version)
        {
            // See if the key exists.
            using (RegistryKey lyncKey = Registry.LocalMachine.OpenSubKey(version.RegistryKey()))
            {
                return (null != lyncKey);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static bool IsSDKInstalled()
        {
            if(Directory.Exists(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86), "Microsoft Lync", "SDK", "Assemblies", "Desktop")))
            {
                return true;
            }

            return false;
        }
    }
}
