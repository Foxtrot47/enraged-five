﻿//---------------------------------------------------------------------------------------------
// <copyright file="ExcelWorkbook.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Office.Excel
{
    using System;
    using System.Reflection;

    /// <summary>
    /// Represents a worksheet object which is apart of a worksheets collection.
    /// </summary>
    public class ExcelWorkbook
    {
        #region Fields
        /// <summary>
        /// The reference to the Excel IWorkbook proxy object.
        /// </summary>
        private IWorkbook _workbook;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ExcelWorkbook"/> class.
        /// </summary>
        /// <param name="workbook">
        /// The IWorkbook proxy object.
        /// </param>
        internal ExcelWorkbook(IWorkbook workbook)
        {
            this._workbook = workbook;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the name of the workbook.
        /// </summary>
        public string Name
        {
            get { return this._workbook.Name; }
        }

        /// <summary>
        /// Gets a collection containing all of the work sheets in this workbook.
        /// </summary>
        public ExcelWorksheets Worksheets
        {
            get { return new ExcelWorksheets(this._workbook.Worksheets); }
        }
        #endregion Properties
    } // RSG.Interop.Microsoft.Office.Excel.ExcelWorkbook {Class}
} // RSG.Interop.Microsoft.Office.Excel {Namespace}
