﻿//---------------------------------------------------------------------------------------------
// <copyright file="WordBookmark.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Office.Word
{
    /// <summary>
    /// Represents a single word bookmark positioned in a document, selection, or range.
    /// </summary>
    public class WordBookmark
    {
        #region Fields
        /// <summary>
        /// The reference to the word IBookmark proxy object.
        /// </summary>
        private IBookmark _bookmark;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="WordBookmark"/> class.
        /// </summary>
        /// <param name="bookmark">
        /// The IBookmark proxy object.
        /// </param>
        internal WordBookmark(IBookmark bookmark)
        {
            this._bookmark = bookmark;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the ending character position of this bookmark.
        /// </summary>
        public int End
        {
            get { return this._bookmark.End; }
            set { this._bookmark.End = value; }
        }

        /// <summary>
        /// Gets the name of this bookmark.
        /// </summary>
        public string Name
        {
            get { return this._bookmark.Name; }
        }

        /// <summary>
        /// Gets or sets the starting character position of this bookmark.
        /// </summary>
        public int Start
        {
            get { return this._bookmark.Start; }
            set { this._bookmark.Start = value; }
        }
        #endregion Properties
    } // RSG.Interop.Microsoft.Office.Word.WordBookmark {Class}
} // RSG.Interop.Microsoft.Office.Word {Namespace}
