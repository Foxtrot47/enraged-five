﻿//---------------------------------------------------------------------------------------------
// <copyright file="ExcelApplication.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Office.Excel
{
    /// <summary>
    /// Represents the entire Microsoft Excel application.
    /// </summary>
    public class ExcelApplication
    {
        #region Fields
        /// <summary>
        /// The reference to the work IApplication proxy object.
        /// </summary>
        private IApplication _application;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ExcelApplication"/> class.
        /// </summary>
        /// <param name="application">
        /// The IApplication proxy object.
        /// </param>
        internal ExcelApplication(IApplication application)
        {
            this._application = application;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the name of the specified object.
        /// </summary>
        public string Name
        {
            get { return this._application.Name; }
        }

        /// <summary>
        /// Gets the Microsoft Excel version number.
        /// </summary>
        public string Version
        {
            get { return this._application.Version; }
        }

        /// <summary>
        /// Gets a collection containing all of the currently opened work books.
        /// </summary>
        public ExcelWorkbooks Workbooks
        {
            get { return new ExcelWorkbooks(this._application.Workbooks); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the application is visible to the user.
        /// </summary>
        public bool Visible
        {
            get { return this._application.Visible; }
            set { this._application.Visible = true; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Quits Microsoft Excel.
        /// </summary>
        public void Quit()
        {
            this._application.Quit();
        }
        #endregion Methods
    } // RSG.Interop.Microsoft.Office.Excel.ExcelApplication {Class}
} // RSG.Interop.Microsoft.Office.Excel {Namespace}
