﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Interop.Microsoft.Office.Outlook
{
#pragma warning disable 1591
    /// <summary>
    /// 
    /// </summary>
    public interface IRecipient : ICommon
    {
        string Address { get; }
        int Index { get; }
        string Name { get; }
        bool Resolved { get; }
        int Type { get; set; }
        void Delete();
        bool Resolve();
    } // IRecipient
#pragma warning restore 1591
}
