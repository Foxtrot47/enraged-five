﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Interop.Microsoft.Office.Outlook
{
#pragma warning disable 1591
    /// <summary>
    /// 
    /// </summary>
    public enum OlInspectorClose
    {
        olDiscard = 1,
        olPromptForSave = 2,
        olSave = 0
    } // OlInspectorClose
#pragma warning restore 1591
}
