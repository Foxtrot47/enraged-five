﻿//---------------------------------------------------------------------------------------------
// <copyright file="IParagraph.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Office.Word
{
    /// <summary>
    /// Represents a single paragraph in a document, selection, or range.
    /// </summary>
    public interface IParagraph
    {
    } // RSG.Interop.Microsoft.Office.Word.IParagraph {Interface}
} // RSG.Interop.Microsoft.Office.Word {Namespace}
