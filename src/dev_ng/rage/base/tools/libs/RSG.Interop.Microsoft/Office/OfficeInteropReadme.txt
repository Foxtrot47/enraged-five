﻿Code in the RSG.Interop.Microsoft.Office namespace is based off of the following codeproject article:
http://www.codeproject.com/Articles/10888/SafeCOMWrapper-Managed-Disposable-Strongly-Typed-s

The idea behind it is that we don't need to directly reference the outlook interop assembly which ties us to a particular version of Outlook.