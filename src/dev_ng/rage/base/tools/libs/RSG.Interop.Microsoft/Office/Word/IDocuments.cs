﻿//---------------------------------------------------------------------------------------------
// <copyright file="IDocuments.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Office.Word
{
    using System.Collections;

    /// <summary>
    /// A collection of all the IDocument objects that are currently open in Word.
    /// </summary>
    internal interface IDocuments
    {
        #region Properties
        /// <summary>
        /// Gets the number of documents in the collection.
        /// </summary>
        int Count { get; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a IDocument object that represents a new, empty document added to the
        /// collection of open documents.
        /// </summary>
        /// <returns>
        /// The IDocument object that was added to this collection.
        /// </returns>
        IDocument Add();

        /// <summary>
        /// Creates a IDocument object that represents a new, empty document added to the
        /// collection of open documents.
        /// </summary>
        /// <param name="template">
        /// The name of the template to be used for the new document.
        /// </param>
        /// <param name="newTemplate">
        /// True to open the document as a template. The default value is False.
        /// </param>
        /// <param name="type">
        /// Defines the type of document created.
        /// </param>
        /// <param name="visible">
        /// True to open the document in a visible window. If this value is False, Microsoft
        /// Word opens the document but sets the Visible property of the document window to
        /// False.
        /// </param>
        /// <returns>
        /// The IDocument object that was added to this collection.
        /// </returns>
        IDocument Add(object template, object newTemplate, object type, object visible);

        /// <summary>
        /// Returns an individual IDocument object in this collection.
        /// </summary>
        /// <param name="index">
        /// The individual object to be returned. Can be a Long indicating the ordinal position
        /// of the individual object.
        /// </param>
        /// <returns>
        /// The individual IDocument object in this collection.
        /// </returns>
        IDocument Item(object index);
        #endregion Methods
    } // RSG.Interop.Microsoft.Office.Word.IDocuments {Interface}
} // RSG.Interop.Microsoft.Office.Word {Namespace}
