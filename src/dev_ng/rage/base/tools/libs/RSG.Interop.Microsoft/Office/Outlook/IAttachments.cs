﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Interop.Microsoft.Office.Outlook
{
    /// <summary>
    /// Contains a set of Attachment objects that represent the attachments in an Outlook item.
    /// </summary>
    public interface IAttachments : ICollection
    {
        /// <summary>
        /// Creates a new attachment in the Attachments collection.
        /// </summary>
        /// <param name="source">The source of the attachment. This can be a file (represented by the full file system path with a
        /// file name) or an Outlook item that constitutes the attachment.</param>
        /// <param name="type">The type of the attachment. Can be one of the OlAttachmentType constants.</param>
        /// <param name="position">This parameter applies only to e-mail messages using Microsoft Outlook Rich Text format: it is
        /// the position where the attachment should be placed within the body text of the message. A value of 1 for the Position
        /// parameter specifies that the attachment should be positioned at the beginning of the message body. A value 'n' greater
        /// than the number of characters in the body of the e-mail item specifies that the attachment should be placed at the end.
        /// A value of 0 makes the attachment hidden.</param>
        /// <param name="displayName">This parameter applies only if the mail item is in Rich Text format and Type is set to
        /// olByValue: the name is displayed in an Inspector object for the attachment or when viewing the properties of the
        /// attachment. If the mail item is in Plain Text or HTML format, then the attachment is displayed using the file name in
        /// the Source parameter.</param>
        /// <returns>An Attachment object that represents the new attachment.</returns>
        IAttachment Add(object source, object type, object position, object displayName);
    } // IAttachments
}
