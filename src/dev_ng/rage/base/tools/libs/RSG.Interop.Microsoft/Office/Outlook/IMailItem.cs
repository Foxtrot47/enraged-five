﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Interop.Microsoft.Office.Outlook
{
#pragma warning disable 1591
    /// <summary>
    /// 
    /// </summary>
    public interface IMailItem : IItem
    {
        IRecipients Recipients { get; }

        IMailItem Forward();
        IMailItem Reply();
    } // IMailItem
#pragma warning restore 1591
}
