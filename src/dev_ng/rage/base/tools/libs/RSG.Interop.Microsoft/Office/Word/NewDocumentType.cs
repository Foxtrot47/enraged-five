﻿//---------------------------------------------------------------------------------------------
// <copyright file="NewDocumentType.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Office.Word
{
    /// <summary>
    /// Defines the different word document types that can be created off of a word documents
    /// collection object.
    /// </summary>
    public enum NewDocumentType
    {
        /// <summary>
        /// Creates a new blank word document. This is the default.
        /// </summary>
        Blank,

        /// <summary>
        /// Creates a new web page word document.
        /// </summary>
        WebPage,

        /// <summary>
        /// Creates a new email message word document.
        /// </summary>
        EmailMessage,

        /// <summary>
        /// Creates a new frameset word document.
        /// </summary>
        Frameset,

        /// <summary>
        /// Creates a new xml word document.
        /// </summary>
        Xml,
    } // RSG.Interop.Microsoft.Office.Word.NewDocumentType {Enum}
} // RSG.Interop.Microsoft.Office.Word {Namespace}
