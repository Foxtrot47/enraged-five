﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Interop.Microsoft.Office.Outlook
{
#pragma warning disable 1591
    /// <summary>
    /// 
    /// </summary>
    [ComProgId("Outlook.Application")]
    public interface IApplication : ICommon
    {
        string Name { get; }
        string Version { get; }
        object CreateItem(OlItemType ItemType);
        object CreateItemFromTemplate(string TemplatePath, object InFolder);
        object CreateObject(string ObjectName);
        void Quit();
    }
#pragma warning restore 1591
}
