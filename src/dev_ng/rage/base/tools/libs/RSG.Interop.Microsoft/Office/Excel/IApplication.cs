﻿//---------------------------------------------------------------------------------------------
// <copyright file="IApplication.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Office.Excel
{
    /// <summary>
    /// The COM wrapper for a Microsoft Excel application instance.
    /// </summary>
    [ComProgId("Excel.Application")]
    internal interface IApplication
    {
        #region Properties
        /// <summary>
        /// Gets the name of the specified object.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets the Microsoft Excel version number.
        /// </summary>
        string Version { get; }

        /// <summary>
        /// Gets a collection containing all of the currently opened work books.
        /// </summary>
        IWorkbooks Workbooks { get; }

        /// <summary>
        /// Gets or sets a value indicating whether the application is visible to the user.
        /// </summary>
        bool Visible { get; set; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Quits Microsoft Excel.
        /// </summary>
        void Quit();
        #endregion Methods
    } // RSG.Interop.Microsoft.Office.Excel.IApplication {Interface}
} // RSG.Interop.Microsoft.Office.Excel {Namespace}
