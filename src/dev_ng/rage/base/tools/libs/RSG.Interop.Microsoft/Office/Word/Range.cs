﻿//---------------------------------------------------------------------------------------------
// <copyright file="Range.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Office.Word
{
    /// <summary>
    /// Represents a contiguous area in a document. Each Range object is defined by a starting
    /// and ending character position.
    /// </summary>
    public class Range
    {
        #region Fields
        /// <summary>
        /// The reference to the word IRange proxy object.
        /// </summary>
        private IRange _range;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="Range"/> class.
        /// </summary>
        /// <param name="range">
        /// The IRange proxy object.
        /// </param>
        internal Range(IRange range)
        {
            this._range = range;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets a constant that represents the alignment for this range.
        /// </summary>
        public Alignment Alignment
        {
            get { return this._range.Paragraphs.Alignment; }
            set { this._range.Paragraphs.Alignment = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the range is formatted as bold.
        /// </summary>
        public bool Bold
        {
            get { return this._range.Bold == 0 ? false : true; }
            set { this._range.Bold = value ? 1 : 0; }
        }

        /// <summary>
        /// Gets or sets the ending character position of this range.
        /// </summary>
        public int End
        {
            get { return this._range.End; }
            set { this._range.End = value; }
        }

        /// <summary>
        /// Gets or sets a Font object that represents the character formatting for the range.
        /// </summary>
        public IFont Font
        {
            get { return this._range.Font; }
            set { this._range.Font = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the range is formatted as italic.
        /// </summary>
        public bool Italic
        {
            get { return this._range.Italic == 0 ? false : true; }
            set { this._range.Italic = value ? 1 : 0; }
        }

        /// <summary>
        /// Gets or sets the starting character position of this range.
        /// </summary>
        public int Start
        {
            get { return this._range.Start; }
            set { this._range.Start = value; }
        }

        /// <summary>
        /// Gets or sets the style for the specified object.
        /// </summary>
        public object Style
        {
            get { return this._range.Style; }
            set { this._range.Style = value; }
        }

        /// <summary>
        /// Gets or sets the text for this range.
        /// </summary>
        public string Text
        {
            get { return this._range.Text; }
            set { this._range.Text = value; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Selects this range.
        /// </summary>
        public void Select()
        {
            this._range.Select();
        }
        #endregion Methods
    } // RSG.Interop.Microsoft.Office.Word.Range {Class}
} // RSG.Interop.Microsoft.Office.Word {Namespace}
