﻿//---------------------------------------------------------------------------------------------
// <copyright file="BuiltInStyle.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Office.Word
{
    /// <summary>
    /// Specifies a built-in Microsoft Word style.
    /// </summary>
    public enum BuiltInStyle
    {
        /// <summary>
        /// Normal style.
        /// </summary>
        Normal = -1,

        /// <summary>
        /// Title style.
        /// </summary>
        Title = -63,

        /// <summary>
        /// Heading 1 style.
        /// </summary>
        Heading1 = -2,

        /// <summary>
        /// Heading 2 style.
        /// </summary>
        Heading2 = -3,

        /// <summary>
        /// Heading 3 style.
        /// </summary>
        Heading3 = -4,

        /// <summary>
        /// Heading 4 style.
        /// </summary>
        Heading4 = -5,

        /// <summary>
        /// Heading 5 style.
        /// </summary>
        Heading5 = -6,

        /// <summary>
        /// Heading 6 style.
        /// </summary>
        Heading6 = -7,

        /// <summary>
        /// Heading 7 style.
        /// </summary>
        Heading7 = -8,

        /// <summary>
        /// Heading 8 style.
        /// </summary>
        Heading8 = -9,

        /// <summary>
        /// Heading 9 style.
        /// </summary>
        Heading9 = -10,
    } // RSG.Interop.Microsoft.Office.Word.BuiltInStyle {Enum}
} // RSG.Interop.Microsoft.Office.Word {Namespace}
