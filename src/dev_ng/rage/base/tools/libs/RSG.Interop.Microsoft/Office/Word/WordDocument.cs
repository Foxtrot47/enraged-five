﻿//---------------------------------------------------------------------------------------------
// <copyright file="WordDocument.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Office.Word
{
    using System;
    using System.Reflection;

    /// <summary>
    /// Represents a document object which is apart of a documents collection.
    /// </summary>
    public class WordDocument : IEquatable<WordDocument>
    {
        #region Fields
        /// <summary>
        /// The reference to the word IDocument proxy object.
        /// </summary>
        private IDocument _document;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="WordDocument"/> class.
        /// </summary>
        /// <param name="document">
        /// The IDocument proxy object.
        /// </param>
        internal WordDocument(IDocument document)
        {
            this._document = document;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the bookmark collection that represents all the bookmarks in this document.
        /// </summary>
        public WordBookmarks Bookmarks
        {
            get { return new WordBookmarks(this._document.Bookmarks); }
        }

        /// <summary>
        /// Gets a Range object that represents the main document story.
        /// </summary>
        public Range Content
        {
            get { return new Range(this._document.Content); }
        }

        /// <summary>
        /// Gets the name of the document.
        /// </summary>
        public string Name
        {
            get { return this._document.Name; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Appends the default line terminator to the end of the word document.
        /// </summary>
        public void AppendLine()
        {
            int start = this.Bookmarks[PredefinedBookmarks.End].Start;
            Range range = this.Range(start, start);
            range.Text = Environment.NewLine;
            range.Select();
            range.Style = BuiltInStyle.Normal;
        }

        /// <summary>
        /// Appends a copy of the specified string followed by the default line terminator to
        /// the end word document.
        /// </summary>
        /// <param name="text">
        /// The string to append.
        /// </param>
        public void AppendLine(string text)
        {
            this.AppendText(text);

            int start = this.Bookmarks[PredefinedBookmarks.End].Start;
            Range range = this.Range(start, start);
            range.Text = Environment.NewLine;
            range.Select();
        }

        /// <summary>
        /// Appends a copy of the specified string followed by the default line terminator to
        /// the end word document.
        /// </summary>
        /// <param name="text">
        /// The string to append.
        /// </param>
        /// <param name="style">
        /// The style for the appended text.
        /// </param>
        public void AppendLine(string text, BuiltInStyle style)
        {
            this.AppendText(text, style);

            int start = this.Bookmarks[PredefinedBookmarks.End].Start;
            Range range = this.Range(start, start);
            range.Text = Environment.NewLine;
            range.Select();
        }

        /// <summary>
        /// Appends a copy of the specified string followed by the default line terminator to
        /// the end word document.
        /// </summary>
        /// <param name="text">
        /// The string to append.
        /// </param>
        /// <param name="style">
        /// The style for the appended text.
        /// </param>
        /// <param name="alignment">
        /// The alignment of the appended text.
        /// </param>
        public void AppendLine(string text, BuiltInStyle style, Alignment alignment)
        {
            this.AppendText(text, style, alignment);

            int start = this.Bookmarks[PredefinedBookmarks.End].Start;
            Range range = this.Range(start, start);
            range.Text = Environment.NewLine;
            range.Select();
        }

        /// <summary>
        /// Appends a copy of the specified string followed by the default line terminator to
        /// the end word document.
        /// </summary>
        /// <param name="text">
        /// The string to append.
        /// </param>
        /// <param name="bold">
        /// A value indicating whether the appended text is formatted as bold.
        /// </param>
        /// <param name="italic">
        /// A value indicating whether the appended text is formatted as italic.
        /// </param>
        public void AppendLine(string text, bool bold, bool italic)
        {
            this.AppendText(text, bold, italic);

            int start = this.Bookmarks[PredefinedBookmarks.End].Start;
            Range range = this.Range(start, start);
            range.Text = Environment.NewLine;
            range.Select();
        }

        /// <summary>
        /// Appends a copy of the specified string followed by the default line terminator to
        /// the end word document.
        /// </summary>
        /// <param name="text">
        /// The string to append.
        /// </param>
        /// <param name="bold">
        /// A value indicating whether the appended text is formatted as bold.
        /// </param>
        /// <param name="italic">
        /// A value indicating whether the appended text is formatted as italic.
        /// </param>
        /// <param name="alignment">
        /// The alignment of the appended text.
        /// </param>
        public void AppendLine(string text, bool bold, bool italic, Alignment alignment)
        {
            this.AppendText(text, bold, italic, alignment);

            int start = this.Bookmarks[PredefinedBookmarks.End].Start;
            Range range = this.Range(start, start);
            range.Text = Environment.NewLine;
            range.Select();
        }

        /// <summary>
        /// Appends a copy of the specified string to this word document.
        /// </summary>
        /// <param name="text">
        /// The string to append.
        /// </param>
        public void AppendText(string text)
        {
            int start = this.Bookmarks[PredefinedBookmarks.End].Start;
            Range range = this.Range(start, start);
            range.Text = text;
            range.Style = BuiltInStyle.Normal;
        }

        /// <summary>
        /// Appends a copy of the specified string to this word document.
        /// </summary>
        /// <param name="text">
        /// The string to append.
        /// </param>
        /// <param name="style">
        /// The style for the appended text.
        /// </param>
        public void AppendText(string text, BuiltInStyle style)
        {
            int start = this.Bookmarks[PredefinedBookmarks.End].End;
            Range range = this.Range(start, start);
            range.Text = text;
            range.Select();

            range.Style = style;
        }

        /// <summary>
        /// Appends a copy of the specified string to this word document.
        /// </summary>
        /// <param name="text">
        /// The string to append.
        /// </param>
        /// <param name="style">
        /// The style for the appended text.
        /// </param>
        /// <param name="alignment">
        /// The alignment of the appended text.
        /// </param>
        public void AppendText(string text, BuiltInStyle style, Alignment alignment)
        {
            int start = this.Bookmarks[PredefinedBookmarks.End].End;
            Range range = this.Range(start, start);
            range.Text = text;
            range.Style = style;
            range.Alignment = alignment;
        }

        /// <summary>
        /// Appends a copy of the specified string to this word document.
        /// </summary>
        /// <param name="text">
        /// The string to append.
        /// </param>
        /// <param name="size">
        /// The size of the text to append.
        /// </param>
        public void AppendText(string text, float size)
        {
            int start = this.Bookmarks[PredefinedBookmarks.End].Start;
            Range range = this.Range(start);
            range.Text = text;
            range.Font.Size = size;
            range.Style = BuiltInStyle.Normal;
        }

        /// <summary>
        /// Appends a copy of the specified string to this word document.
        /// </summary>
        /// <param name="text">
        /// The string to append.
        /// </param>
        /// <param name="bold">
        /// A value indicating whether the appended text is formatted as bold.
        /// </param>
        /// <param name="italic">
        /// A value indicating whether the appended text is formatted as italic.
        /// </param>
        public void AppendText(string text, bool bold, bool italic)
        {
            int start = this.Bookmarks[PredefinedBookmarks.End].Start;
            Range range = this.Range(start);
            range.Text = text;
            range.Style = BuiltInStyle.Normal;
            range.Bold = bold;
            range.Italic = italic;
        }

        /// <summary>
        /// Appends a copy of the specified string to this word document.
        /// </summary>
        /// <param name="text">
        /// The string to append.
        /// </param>
        /// <param name="bold">
        /// A value indicating whether the appended text is formatted as bold.
        /// </param>
        /// <param name="italic">
        /// A value indicating whether the appended text is formatted as italic.
        /// </param>
        /// <param name="alignment">
        /// The alignment of the appended text.
        /// </param>
        public void AppendText(string text, bool bold, bool italic, Alignment alignment)
        {
            int start = this.Bookmarks[PredefinedBookmarks.End].Start;
            Range range = this.Range(start, start);
            range.Text = text;
            range.Select();

            range.Style = BuiltInStyle.Normal;
            range.Bold = bold;
            range.Italic = italic;
            range.Alignment = alignment;
        }

        /// <summary>
        /// Appends a copy of the specified string to this word document.
        /// </summary>
        /// <param name="text">
        /// The string to append.
        /// </param>
        /// <param name="size">
        /// The size of the text to append.
        /// </param>
        /// <param name="bold">
        /// A value indicating whether the appended text is formatted as bold.
        /// </param>
        /// <param name="italic">
        /// A value indicating whether the appended text is formatted as italic.
        /// </param>
        public void AppendText(string text, float size, bool bold, bool italic)
        {
            int start = this.Bookmarks[PredefinedBookmarks.End].Start;
            Range range = this.Range(start, start);
            range.Text = text;
            range.Style = BuiltInStyle.Normal;
            range.Bold = bold;
            range.Italic = italic;
            range.Font.Size = size;
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// This compares the wrapped proxy object with each other.
        /// </summary>
        /// <param name="other">
        /// The object to compare to.
        /// </param>
        /// <returns>
        /// True if the current object is equal to the <paramref name="other"/> parameter;
        /// otherwise, false.
        /// </returns>
        public bool Equals(WordDocument other)
        {
            return Object.ReferenceEquals(this._document, other._document);
        }

        /// <summary>
        /// Creates a range object by using the specified starting character position and
        /// expands over the remainder of the document.
        /// </summary>
        /// <param name="start">
        /// The starting character position.
        /// </param>
        /// <returns>
        /// A range object that represents the document between the starting position and the
        /// end of the document.
        /// </returns>
        public Range Range(int start)
        {
            object startObject = start;
            object endObject = Missing.Value;
            return new Range(this._document.Range(startObject, endObject));
        }

        /// <summary>
        /// Creates a range object by using the specified starting and ending character
        /// positions.
        /// </summary>
        /// <param name="start">
        /// The starting character position.
        /// </param>
        /// <param name="end">
        /// The ending character position.
        /// </param>
        /// <returns>
        /// A range object that represents the document between the two specified positions.
        /// </returns>
        public Range Range(int start, int end)
        {
            object startObject = start;
            object endObject = end;
            return new Range(this._document.Range(startObject, endObject));
        }
        #endregion Methods
    } // RSG.Interop.Microsoft.Office.Word.WordDocument {Class}
} // RSG.Interop.Microsoft.Office.Word {Namespace}
