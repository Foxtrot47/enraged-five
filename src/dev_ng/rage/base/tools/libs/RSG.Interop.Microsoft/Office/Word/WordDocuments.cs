﻿//---------------------------------------------------------------------------------------------
// <copyright file="WordDocuments.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Office.Word
{
    using System.Reflection;

    /// <summary>
    /// Represents a collection of all the WordDocument objects that are currently open inside
    /// a Word Application.
    /// </summary>
    public class WordDocuments
    {
        #region Fields
        /// <summary>
        /// The reference to the word IDocuments proxy object.
        /// </summary>
        private IDocuments _documents;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="WordDocuments"/> class.
        /// </summary>
        /// <param name="documents">
        /// The IDocuments proxy object.
        /// </param>
        internal WordDocuments(IDocuments documents)
        {
            this._documents = documents;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the number of documents in the collection.
        /// </summary>
        public int Count
        {
            get { return this._documents.Count; }
        }

        /// <summary>
        /// Gets the document whose ordinal position is specified as a index.
        /// </summary>
        /// <param name="index">
        /// The ordinal position of the individual document to retrieve.
        /// </param>
        /// <returns>
        /// The document whose ordinal position is specified.
        /// </returns>
        public WordDocument this[int index]
        {
            get
            {
                object itemIndex = (long)index;
                return new WordDocument(this._documents.Item(itemIndex));
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new, empty Document object and adds it to the collection of opened
        /// documents.
        /// </summary>
        /// <returns>
        /// The document that was added to the collection.
        /// </returns>
        public WordDocument Add()
        {
            return new WordDocument(this._documents.Add());
        }

        /// <summary>
        /// Gets a Document object that represents a new, empty document added to the
        /// collection of open documents.
        /// </summary>
        /// <param name="invisible">
        /// True to open a the document but set its Visible property to False so the user
        /// cannot see it.
        /// </param>
        /// <returns>
        /// The document that was added to the collection.
        /// </returns>
        public WordDocument Add(bool invisible)
        {
            object missing = Missing.Value;
            object visible = !invisible;

            return new WordDocument(this._documents.Add(missing, missing, missing, visible));
        }

        /// <summary>
        /// Gets a Document object that represents a new, empty document added to the
        /// collection of open documents.
        /// </summary>
        /// <param name="documentType">
        /// Defines the type of document created.
        /// </param>
        /// <param name="invisible">
        /// True to open a the document but set its Visible property to False so the user
        /// cannot see it.
        /// </param>
        /// <returns>
        /// The document that was added to the collection.
        /// </returns>
        public WordDocument Add(NewDocumentType documentType, bool invisible)
        {
            object missing = Missing.Value;
            object visible = !invisible;
            object type = (int)documentType;

            return new WordDocument(this._documents.Add(missing, missing, type, visible));
        }
        #endregion Methods
    } // RSG.Interop.Microsoft.Office.Word.WordDocuments {Class}
} // RSG.Interop.Microsoft.Office.Word {Namespace}
