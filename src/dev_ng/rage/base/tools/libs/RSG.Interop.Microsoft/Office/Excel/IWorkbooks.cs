﻿//---------------------------------------------------------------------------------------------
// <copyright file="IWorkbooks.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft.Office.Excel
{
    /// <summary>
    /// A collection of all the IWorkbook objects that are currently open in Excel.
    /// </summary>
    internal interface IWorkbooks
    {
        #region Properties
        /// <summary>
        /// Gets the number of work books in the collection.
        /// </summary>
        int Count { get; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a IWorkbook object that represents a new, empty work sheet added to the
        /// collection of opened work books.
        /// </summary>
        /// <returns>
        /// The IWorkbook object that was added to this collection.
        /// </returns>
        IWorkbook Add();

        /// <summary>
        /// Returns an individual IWorkbook object in this collection.
        /// </summary>
        /// <param name="index">
        /// The individual object to be returned. Can be a Long indicating the ordinal position
        /// of the individual object.
        /// </param>
        /// <returns>
        /// The individual IWorkbook object in this collection.
        /// </returns>
        IWorkbook Item(object index);
        #endregion Methods
    } // RSG.Interop.Microsoft.Office.Excel.IWorkbooks {Interface}
} // RSG.Interop.Microsoft.Office.Excel {Namespace}
