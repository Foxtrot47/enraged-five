﻿//---------------------------------------------------------------------------------------------
// <copyright file="FileAssociationException.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft
{    
    using System;
    
    /// <summary>
    /// 
    /// </summary>
    public class FileAssociationException : Exception
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public FileAssociationException()
            : base()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public FileAssociationException(String message)
            : base(message)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="inner"></param>
        public FileAssociationException(String message, Exception inner)
            : base(message, inner)
        {
        }
        #endregion // Constructor(s)
    }

} // RSG.Interop.Microsoft namespace
