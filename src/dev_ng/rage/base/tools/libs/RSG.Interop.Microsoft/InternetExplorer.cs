﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Microsoft.Win32;

namespace RSG.Interop.Microsoft
{

    /// <summary>
    /// Internet Explorer interoperability class.
    /// </summary>
    public static class InternetExplorer
    {
        #region Constants
        /// <summary>
        /// Path to the browser emulation registry key.
        /// </summary>
        private static readonly String RegKey_BrowserEmulationPath = @"Software\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_BROWSER_EMULATION";

        /// <summary>
        /// Path to the Internet Explorer registry key.
        /// </summary>
        private static readonly String RegKey_InternetExplorer = @"SOFTWARE\Microsoft\Internet Explorer";

        /// <summary>
        /// Version value key name.
        /// </summary>
        private static readonly String RegKey_InternetExplorerVersion = @"Version";
        #endregion // Constants

        #region Controller Methods
        /// <summary>
        /// Returns true if the user has at least ieVersion of Internet Explorer installed.
        /// </summary>
        /// <param name="ieVersion">Internet explorer version. Just the major version number, e.g. 7, 8, 9.</param>
        /// <returns>True if the installed version number is greater than or equal to the ieVersion supplied.</returns>
        public static bool IsMinimumVersionInstalled(int ieVersion)
        {
            bool isMinimumVersion = false;

            string versionString = VersionInstalled();
            if (!String.IsNullOrEmpty(versionString))
            {
                string[] verSplit = versionString.Split(".".ToCharArray());
                int majorVer = 0;
                if (int.TryParse(verSplit[0], out majorVer))
                {
                    isMinimumVersion = ieVersion >= majorVer;
                }
            }

            return (isMinimumVersion);
        }

        /// <summary>
        /// Return the version string of Internet Explorer currently installed.
        /// </summary>
        /// <returns>
        /// The version of Internet Explorer installed (e.g. "8.0.7600.16385").
        /// </returns>
        /// <exception cref="System.Security.SecurityException" />
        /// 
        public static String VersionInstalled()
        {
            String version = "<Unknown>";
            using (RegistryKey ieKey = 
                Registry.LocalMachine.OpenSubKey(RegKey_InternetExplorer))
            {
                version = ieKey.GetValue(RegKey_InternetExplorerVersion, String.Empty).ToString();
            }            
            return (version);
        }

        /// <summary>
        /// Set embedded version of Internet Explorer in WebBrowser control to a 
        /// specific version (for entry assembly).
        /// </summary>
        /// <param name="version">Browser version.</param>
        /// <returns></returns>
        /// This sets both normal executable and the Visual Studio hosted application.
        /// 
        public static bool SetEmbeddedVersion(InternetExplorerVersion version)
        {
            return (SetEmbeddedVersion(version, true));
        }

        /// <summary>
        /// Set embedded version of Internet Explorer in WebBrowser control to a 
        /// specific version (for entry assembly).
        /// </summary>
        /// <param name="version">Browser version.</param>
        /// <param name="includeDebugHost"></param>
        /// <returns></returns>
        /// This optionally sets both normal executable and the Visual Studio hosted 
        /// application.
        /// 
        public static bool SetEmbeddedVersion(InternetExplorerVersion version, bool includeDebugHost)
        {
            String imageName = Path.GetFileNameWithoutExtension(Process.GetCurrentProcess().MainModule.FileName);
            return (SetEmbeddedVersionFor(version, imageName, includeDebugHost));
        }

        /// <summary>
        /// Set embedded version of Internet Explorer in WebBrowser control to a 
        /// specific version for a specific image name (e.g. "Workbench").
        /// </summary>
        /// <param name="version">Browser version.</param>
        /// <param name="imageName">Application image name name the setting will apply (e.g. "Workbench").</param>
        /// <returns></returns>
        /// This sets both normal executable and the Visual Studio hosted application.
        /// 
        public static bool SetEmbeddedVersionFor(InternetExplorerVersion version, String imageName)
        {
            return (SetEmbeddedVersionFor(version, imageName, true));
        }

        /// <summary>
        /// Set embedded version of Internet Explorer in WebBrowser control to a 
        /// specific version for a specific image name (e.g. "Workbench").
        /// </summary>
        /// <param name="version">Browser version.</param>
        /// <param name="imageName">Application image name name the setting will apply (e.g. "Workbench").</param>
        /// <param name="includeDebugHost">A value indicating whether the debug host also gets set.</param>
        /// <returns>true iff successful; false if user lacks secutiry credentials.</returns>
        /// No path or ".exe" extension should be supplied.
        /// 
        /// This optionally sets both normal executable and the Visual Studio hosted 
        /// application.
        ///
        public static bool SetEmbeddedVersionFor(InternetExplorerVersion version, String imageName, bool includeDebugHost)
        {
            bool result = true;
            try
            {
                SetWebBrowserVersionForApp(version, imageName + ".exe");
                if (includeDebugHost)
                    SetWebBrowserVersionForApp(version, imageName + ".vshost.exe");
            }
            catch (System.Security.SecurityException)
            {
                result = false;
            }
            catch (UnauthorizedAccessException)
            {
                result = false;
            }
            return (result);
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Set the web browser to use a specific version of Internet Explorer to render content.
        /// </summary>
        /// <param name="version">Browser version.</param>
        /// <param name="applicationName">Application name the setting will apply.</param>
        /// 
        private static void SetWebBrowserVersionForApp(InternetExplorerVersion version, String applicationName)
        {
            RegistryKey currentKey = Registry.CurrentUser;
            String[] path = RegKey_BrowserEmulationPath.Split(@"\".ToCharArray(), 
                StringSplitOptions.RemoveEmptyEntries);

            foreach (String element in path)
            {
                RegistryKey subKey;

                if (currentKey.GetSubKeyNames().Contains(element))
                {
                    subKey = currentKey.OpenSubKey(element, true);
                }
                else
                {
                    subKey = currentKey.CreateSubKey(element, RegistryKeyPermissionCheck.ReadWriteSubTree);
                }

                currentKey.Dispose();
                currentKey = subKey;
            }

            currentKey.SetValue(applicationName, (int)version, RegistryValueKind.DWord);
            currentKey.Dispose();
        }
        #endregion // Private Methods
    }

} // RSG.Interop.Microsoft namespace
