﻿//---------------------------------------------------------------------------------------------
// <copyright file="FileAssociation.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Microsoft
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using System.Windows.Media.Imaging;
    using global::Microsoft.Win32;
    using RSG.Interop.Microsoft.Windows;

    /// <summary>
    /// Windows file association class; used for reading and writing file association 
    /// data to/from the Windows registry.
    /// </summary>
    /// DHM TODO: add support for loading the associated icon.
    /// 
    public class FileAssociation
    {
        #region Enumerations and Classes
        /// <summary>
        /// Microsoft Windows perceived file types.
        /// </summary>
        /// http://msdn.microsoft.com/en-us/library/windows/desktop/cc144150(v=vs.85).aspx"
        /// 
        public enum FilePerceivedType
        {
            /// <summary>
            /// No perceived type specified (default).
            /// </summary>
            None,

            /// <summary>
            /// 
            /// </summary>
            Folder,
            
            /// <summary>
            /// File contains text data.
            /// </summary>
            Text,

            /// <summary>
            /// File contains image data.
            /// </summary>
            Image,

            /// <summary>
            /// File contains audio data.
            /// </summary>
            Audio,

            /// <summary>
            /// File contains video data.
            /// </summary>
            Video,

            /// <summary>
            /// File contains compressed data.
            /// </summary>
            Compressed,

            /// <summary>
            /// File is an application document.
            /// </summary>
            Document,

            /// <summary>
            /// File is a system file.
            /// </summary>
            System,

            /// <summary>
            /// File is an application.
            /// </summary>
            Application,

            /// <summary>
            /// File contains game media.
            /// </summary>
            Gamemedia,

            /// <summary>
            /// File contains contact data.
            /// </summary>
            Contacts
        }

        /// <summary>
        /// Represents a shell handler consisting of an action string and command-line.
        /// </summary>
        public class ShellHandler
        {
            #region Properties
            /// <summary>
            /// Whether this handler is considered by the shell to be default.
            /// </summary>
            public bool IsDefault
            {
                get;
                private set;
            }

            /// <summary>
            /// Action name.
            /// </summary>
            public String Action
            {
                get;
                private set;
            }

            /// <summary>
            /// Command.
            /// </summary>
            public String Command
            {
                get;
                private set;
            }
            #endregion // Properties

            #region Constructor(s)
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <param name="action"></param>
            /// <param name="isDefault"></param>
            /// <param name="command"></param>
            public ShellHandler(String action, bool isDefault, String command)
            {
                this.Action = action;
                this.IsDefault = isDefault;
                this.Command = command;
            }

            /// <summary>
            /// Constructor; from Registry key; e.g. HKEY_CLASSES_ROOT\RockstarGames.Zipfile.UniversalLogZipFile\shell\Open.
            /// </summary>
            /// <param name="keyHandler"></param>
            /// <param name="isDefault"></param>
            public ShellHandler(RegistryKey keyHandler, bool isDefault)
            {
                this.Action = keyHandler.Name;
                this.IsDefault = isDefault;
                using (RegistryKey commandKey = keyHandler.OpenSubKey("Command"))
                {
                    this.Command = (String)commandKey.GetValue(String.Empty, String.Empty);
                }
            }
            #endregion // Constructor(s)
        }
        #endregion // Enumerations and Classes

        #region Properties
        /// <summary>
        /// Filename extension for association.
        /// </summary>
        public String Extension
        {
            get;
            private set;
        }

        /// <summary>
        /// File association description.
        /// </summary>
        public String Description
        {
            get;
            private set;
        }

        /// <summary>
        /// Icon filename used by Windows Explorer.
        /// </summary>
        public String IconFilename
        {
            get;
            private set;
        }

        /// <summary>
        /// Flags.
        /// </summary>
        public FileTypeAttributeFlags EditFlags
        {
            get;
            private set;
        }

        /// <summary>
        /// File association handler alias.
        /// </summary>
        public String HandlerAlias
        {
            get;
            private set;
        }

        /// <summary>
        /// All the Shell handlers associated with the extension.
        /// </summary>
        public IEnumerable<KeyValuePair<String, ShellHandler>> ShellHandlers
        {
            get;
            private set;
        }

        /// <summary>
        /// Content-type as MIME string.
        /// </summary>
        public String ContentType
        {
            get;
            private set;
        }

        /// <summary>
        /// Perceived data type.
        /// </summary>
        /// http://msdn.microsoft.com/en-us/library/windows/desktop/cc144150(v=vs.85).aspx
        /// 
        public FilePerceivedType PerceivedType
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="extension"></param>
        public FileAssociation(String extension)
        {
            if (String.IsNullOrWhiteSpace(extension))
                throw (new ArgumentException("Extension needs to be specified.", "extension"));
            Debug.Assert(extension.StartsWith("."), "Extension should start with a '.' character.");
            if (!extension.StartsWith("."))
                throw (new ArgumentException("Extension should start with a '.' character."));
           
            // Read extension entries; handler and types.
            this.Extension = extension;
            RegistryKey hkcr = RegistryKey.OpenBaseKey(RegistryHive.ClassesRoot, RegistryView.Default);
            using (RegistryKey key = hkcr.OpenSubKey(this.Extension))
            {
                if (null == key)
                    throw (new FileAssociationException(String.Format("File association does not exist ({0}).", extension)));

                this.HandlerAlias = (String)key.GetValue(String.Empty, String.Empty);
                this.ContentType = (String)key.GetValue("Content Type", String.Empty);
                
                String perceivedTypeStr = (String)key.GetValue("PerceivedType", "None");
                FilePerceivedType perceivedType = FilePerceivedType.None;
                if (Enum.TryParse(perceivedTypeStr, true, out perceivedType))
                    this.PerceivedType = perceivedType;
                else
                    this.PerceivedType = FilePerceivedType.None;
            }

            // Read handler entries.
            this.EditFlags = FileTypeAttributeFlags.FTA_None;
            this.Description = String.Empty;
            this.IconFilename = String.Empty;
            this.ShellHandlers = new KeyValuePair<String, ShellHandler>[0];
            using (RegistryKey key = hkcr.OpenSubKey(this.HandlerAlias))
            {
                if (null == key)
                    throw (new FileAssociationException(String.Format("File association handler does not exist ({0}).  Incomplete association.", extension)));

                Int32 editFlags = (Int32)key.GetValue("EditFlags", 0);
                this.EditFlags = (FileTypeAttributeFlags)editFlags;
                this.Description = (String)key.GetValue(String.Empty, String.Empty);

                using (RegistryKey defaultIconKey = key.OpenSubKey("DefaultIcon"))
                {
                    // Ensure the optional key exists prior to reading.
                    if (null != defaultIconKey)
                    {
                        this.IconFilename = (String)defaultIconKey.GetValue(String.Empty, String.Empty);
                        // DHM TODO: use NativeResourceReader to get this icon resource.
                    }
                }
                using (RegistryKey shellHandlerKey = key.OpenSubKey("shell"))
                {
                    String defaultHandler = (String)shellHandlerKey.GetValue(String.Empty, String.Empty);
                    IDictionary<String, ShellHandler> handlers = new Dictionary<String, ShellHandler>();
                    foreach (String subKeyName in shellHandlerKey.GetSubKeyNames())
                    {
                        // Create new ShellHandler
                        bool isDefault = defaultHandler.Equals(subKeyName);
                        String command = String.Empty;
                        using (RegistryKey commandKey = shellHandlerKey.OpenSubKey(subKeyName))
                        {
                            handlers.Add(subKeyName, new ShellHandler(commandKey, isDefault));
                        }
                    }
                    this.ShellHandlers = handlers;
                }
            }
        }
        #endregion // Constructor(s)
        
        #region Static Controller Methods
        /// <summary>
        /// Create a new FileAssocation object; writing information into the system registry.
        /// </summary>
        /// <param name="extension"></param>
        /// <param name="handlerAlias"></param>
        /// <param name="handlers"></param>
        /// <returns></returns>
        public static FileAssociation Create(String extension, String handlerAlias, 
            IEnumerable<KeyValuePair<String, ShellHandler>> handlers)
        {
            return (Create(extension, handlerAlias, handlers, null, FilePerceivedType.None));
        }

        /// <summary>
        /// Create a new FileAssocation object; writing information into the system registry.
        /// </summary>
        /// <param name="extension"></param>
        /// <param name="handlerAlias"></param>
        /// <param name="handlers"></param>
        /// <param name="contentType"></param>
        /// <param name="perceivedType"></param>
        /// <returns></returns>
        public static FileAssociation Create(String extension, String handlerAlias,
            IEnumerable<KeyValuePair<String, ShellHandler>> handlers, 
            String contentType, FilePerceivedType perceivedType)
        {
            throw (new NotImplementedException());
        }
        #endregion // Static Controller Methods
    }

} // RSG.Interop.Microsoft namespace
