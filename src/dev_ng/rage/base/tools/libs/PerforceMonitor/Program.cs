﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using System.Net.NetworkInformation;
using System.Xml;
using System.Xml.XPath;

using P4API;
using RSG.Base;
using RSG.SourceControl.Perforce;
using P4API;


namespace PerforceMonitor
{
    class MonitorRecord
    {
        public MonitorRecord(string user, string command, string time)
        {
            User = user;
            Command = command;
            Time = time;
            TimeSeconds = 0;
        }

        public string User;
        public string Command;
        public string Time;
        public int TimeSeconds;
    }

    class Program
    {
        static void GenerateReport(string logPath, Dictionary<string, List<MonitorRecord>> userRecords, DateTime startTime, DateTime endTime)
        {
            StreamWriter writer = new StreamWriter(logPath);

            writer.WriteLine("Start Time: " + startTime.ToString());
            writer.WriteLine("End Time: " + endTime.ToString());

            foreach (KeyValuePair<string, List<MonitorRecord>> keyValuePair in userRecords)
            {
                int totalTime = 0;
                List<MonitorRecord> recordList = keyValuePair.Value;
                foreach(MonitorRecord record in recordList)
                {
                    totalTime += record.TimeSeconds;
                }

                writer.WriteLine(keyValuePair.Key + "," + totalTime.ToString());
            }

            writer.Close();
        }

        static void GenerateDetailedReport(string logPath, Dictionary<string, List<MonitorRecord>> userRecords, DateTime startTime, DateTime endTime)
        {
            StreamWriter writer = new StreamWriter(logPath);

            writer.WriteLine("Start Time: " + startTime.ToString());
            writer.WriteLine("End Time: " + endTime.ToString());

            foreach (KeyValuePair<string, List<MonitorRecord>> keyValuePair in userRecords)
            {
                writer.WriteLine(keyValuePair.Key);
                
                int totalTime = 0;
                List<MonitorRecord> recordList = keyValuePair.Value;
                foreach (MonitorRecord record in recordList)
                {
                    writer.WriteLine("\t" + record.Command + "," + record.TimeSeconds);
                    totalTime += record.TimeSeconds;
                }

                writer.WriteLine("\tTotal Time: " + totalTime.ToString());
            }

            writer.Close();
        }

        static int Main(string[] args)
        {








            CommandLineParser parser = new CommandLineParser(args);
            string logPath = parser["logPath"];
            string serverName = parser["serverName"];
            string databaseName = parser["databaseName"];
            bool cruiseControlEnabled = (parser["cc"] != null) ? true : false;

            if ( String.IsNullOrWhiteSpace(logPath) == true || String.IsNullOrWhiteSpace(serverName) == true || String.IsNullOrWhiteSpace(databaseName) == true )
            {
                Console.WriteLine("Invalid arguments specified.  Please specify a log path, server name and database name.");
                return -1;
            }

            if ( File.Exists(logPath) == false )
            {
                Console.WriteLine("Unable to access log " + logPath + ".");
                return -1;
            }

            ErrorLogger logger = new ErrorLogger(logPath, serverName, databaseName, cruiseControlEnabled);
            if (logger.WriteToDatabase() == false)
            {
                Console.WriteLine("Unable to write to database.");
                return -1;
            }
           
            //Execute queries.
            logger.ExecuteQueries();
            return 0;
        }
    }
}
