﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

namespace PerforceMonitor
{
    public class ErrorLogger
    {
        private string m_LogPath;
        private string m_SqlServer;
        private string m_SqlDatabase;
        private string m_TableName;
        private bool m_CruiseControlFormatted;
        private DateTime m_CutoffDate;
        private DateTime m_EndDate;
        private string m_ConnectionString;
        
        public ErrorLogger(string logPath, string sqlServer, string sqlDatabase, bool cruiseControlFormatted)
        {
            m_LogPath = logPath;
            m_SqlServer = sqlServer;
            m_SqlDatabase = sqlDatabase;
            m_CruiseControlFormatted = cruiseControlFormatted;
            m_EndDate = DateTime.Now;
            m_CutoffDate = m_EndDate.AddDays(-1);
            m_TableName = "ErrorTable";
            m_ConnectionString = "server=" + m_SqlServer + ";database=" + m_SqlDatabase + ";integrated security=true";
        }

        public bool WriteToDatabase()
        {

            string currentError = "";
            bool midError = false;
            List<string> errorList = new List<string>();

            if ( String.IsNullOrWhiteSpace(m_LogPath) == true )
            {
                return false;
            }

            if ( File.Exists(m_LogPath) == false )
            {
                return false;
            }

            StreamReader stream = new StreamReader(m_LogPath);
            while ( !stream.EndOfStream )
            {
                string currentLine = stream.ReadLine();

                if ( String.IsNullOrWhiteSpace(currentLine) == true ) 
                {
                    continue;
                }
                else
                {
                    if ( midError == true )
                    {
                        if ( currentLine.StartsWith("\t") == true )
                        {
                            currentError += currentLine.Trim() + "\n";
                        }
                        else
                        {
                            midError = false;
                            errorList.Add(currentError.TrimEnd());
                            currentError = null;
                        }
                    }
                    else
                    {
                        if ( currentLine.Contains("Perforce server error:") == true )
                        {
                            midError = true;
                        }
                    }
                }
            }

            stream.Close();

            if ( midError == true )
            {
                errorList.Add(currentError);
            }

            DateTime errorDate = new DateTime();
            string errorFirst = null;
            string errorSecond = null;
            string errorThird = null;
            string errorIPInfo = null;

            int lineNumber = 0;
            string ipPattern = @"[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}";

            string strSql = "";
            SqlConnection connection = new SqlConnection(m_ConnectionString);
            SqlCommand command = new SqlCommand(strSql, connection);
            connection.Open();

            
            command.CommandText = "DELETE FROM " + m_TableName;

            try
            {
                command.ExecuteNonQuery();
            }
            catch(Exception)
            {
                return false;
            }

            foreach(string error in errorList)
            {
                errorDate = DateTime.MinValue;
                errorFirst = "";
                errorSecond = "";
                errorThird = "";
                errorIPInfo = "";
                lineNumber = 0;

                foreach(string line in error.Split('\n')) 
                {
                    if ( line.StartsWith("Date") )
                    {
                        errorDate = Convert.ToDateTime(line.Replace("Date ", "").TrimEnd(':'));
                    }
                    else
                    {
                        foreach(Match match in Regex.Matches(line, ipPattern))
                        {
                            errorIPInfo = match.Value;
                        }

                        switch(lineNumber)
                        {
                            case 0:
                                errorFirst = line;
                                break;
                            case 1:
                                errorSecond = line;
                                break;
                            default:
                                errorThird += line + " _";
                                break;
                        }


                        lineNumber++;
                    }
                }

                //Determine if this event occurred in the last 24 hours.
                if (errorDate < m_CutoffDate)
                {
                    //If this even has not, then skip it.
                    continue;
                }

                command.Parameters.Clear();
                if (errorDate == DateTime.MinValue) 
                {
                    command.Parameters.Add("@errDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    command.Parameters.Add("@errDate", SqlDbType.DateTime).Value = errorDate;
                }

                command.Parameters.Add("@errFirst", SqlDbType.VarChar).Value = errorFirst.Trim();
                command.Parameters.Add("@errSecond", SqlDbType.VarChar).Value = errorSecond.Trim();
                command.Parameters.Add("@errThird", SqlDbType.VarChar).Value = errorThird.Trim();
                command.Parameters.Add("@errIPInformation", SqlDbType.VarChar).Value = errorIPInfo;

                strSql = "INSERT INTO ErrorTable (ErrorDate, Error1, Error2, Error3,  IPInformation)";
                strSql += " VALUES (@errDate, @errFirst, @errSecond, @errThird, @errIPInformation)";

                command.CommandText = strSql;

                try
                {
                    command.ExecuteNonQuery();
                }
                catch(Exception e)
                {
                    return false;
                }
            }

            Log("Error log database has been successfully updated.");

            connection.Close();
            return true;
        }

        public bool ExecuteQueries()
        {
            //Range of time.
            Log("Start Time: " + m_CutoffDate);
            Log("End Time: " + m_EndDate);

            //Acquire the total number of disconnection.
            string countQuery = "SELECT COUNT(ErrorDate) FROM " + m_TableName + " WHERE ErrorDate > '" + m_CutoffDate.ToString() + "'";

            SqlConnection connection = new SqlConnection(m_ConnectionString);
            SqlCommand command = new SqlCommand(countQuery, connection);
            connection.Open();
            //command.Parameters.AddWithValue("@cutoffDate", m_CutoffDate);
            try
            {
                SqlDataReader reader = command.ExecuteReader();
                reader.Read();
                Log("Number of Disconnections: " + reader[0] + ".");
                reader.Close();
            }
            catch (Exception e)
            {
                Log("An error occurred attempting to acquire the total number of disconnections.");
                return false;
            }

            //Top offenders and the number of disconnections
            string topOffendersQuery = "SELECT  TOP (10) IPInformation, COUNT(*) AS Count FROM " + m_TableName +" WHERE (ErrorDate > '" + m_CutoffDate.ToString() + "')";
            topOffendersQuery += " GROUP BY IPInformation";
            topOffendersQuery += " ORDER BY Count DESC";
            
            connection = new SqlConnection(m_ConnectionString);
            command = new SqlCommand(topOffendersQuery, connection);
            connection.Open();
            try
            {
                SqlDataReader reader = command.ExecuteReader();
                
                Log("Top Offenders: ");
                while (reader.Read())
                {
                    Log("\t" + reader[0] + " (" + reader[1] + ")");
                }

                reader.Close();
            }
            catch (Exception e)
            {
                Log("An error occurred attempting to acquire the total number of disconnections.");
                return false;
            }

            return true;
        }

        private void Log(string message)
        {
            if ( m_CruiseControlFormatted == true )
            {
                Console.WriteLine("INFO_MSG: " + message);
            }
            else
            {
                Console.WriteLine(message);
            }
        }
    }
}
