﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Rage.Mesh
{
    /// <summary>
    /// Enumerant used to define the type of a particular Primitive
    /// </summary>
    public enum PrimType
    {
        mshPOINTS,		// Each index in primitive defines a point (min 1)
        mshLINES,		// Indices in primitive definite independent line segments (min 2, multiple of 2)
        mshTRIANGLES,	// Indices in primitive define a mesh of independent triangles (min 3, multiple of 3)
        mshTRISTRIP,	// Indices in primitive cumulatively define a normal-winding tristrip (min 3)
        mshTRISTRIP2,	// Indices in primitive cumulatively define a reverse-winding tristrip (min 3)
        mshQUADS,		// Indices in primitive define a mesh of independent quads (min 4, multiple of 4, in tristrip order).  If fourth index is zero, quad is a triangle.
        mshPOLYGON,		// Indices in primitive cumulative define a closed, convex polygon (min 3, as a trifan)
        FORCE_32 = 0x7FFFFFFF
    };

    /// <summary>
    /// 
    /// </summary>
    public class Primitive :
        IMeshComponent
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public PrimType Type
        {
            get { return m_Type; }
            set { m_Type = value; }
        }
        PrimType m_Type;

        /// <summary>
        /// 
        /// </summary>
        public Int32 Priority 
        {
            get { return m_Priority; }
            set { m_Priority = value; }
        }
        Int32 m_Priority;

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<Int32> Idx
        {
            get { return m_Idx; }
            set { m_Idx = value; }
        }
        IEnumerable<Int32> m_Idx;
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        public Primitive()
        {
            this.Idx = new List<Int32>();
        }
        #endregion // Constructors

        #region Serialisers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        public void Serialise(MeshSerialiser serialiser)
        {
            serialiser.BeginBlock();
            serialiser.Serialise(ref m_Type, "Type");
            serialiser.Serialise(ref m_Priority, "Priority");
            serialiser.Serialise(ref m_Idx, "Idx");
            serialiser.EndBlock();
        }
        #endregion // Serialisers
    }
}
