﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Math;

namespace RSG.Rage.Mesh
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class Mesh
        : IData
    {
        #region Constants
        /// <summary>
        /// 
        /// </summary>
        public static readonly Int32 MAX_MATRICES_PER_VERTEX = 4;

        /// <summary>
        /// 
        /// </summary>
        public static readonly Int32 MAX_FLOAT_BLIND_DATA = 4;
        #endregion // Constants

        #region Static Properties
        /// <summary>
        /// 
        /// </summary>
        public static Byte Version
        {
            get { return m_Version; }
            set { m_Version = value; } 
        }
        static Byte m_Version;

        /// <summary>
        /// 
        /// </summary>
        public static Byte BlendTargetCount
        {
            get { return m_BlendTargetCount; }
            set { m_BlendTargetCount = value; }
        }
        public static Byte m_BlendTargetCount;
        #endregion // Static Properties

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public Byte Skinned 
        {
            get { return m_Skinned; } 
            set { m_Skinned = value; } 
        }
        Byte m_Skinned;

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<Material> Mtls
        {
            get { return m_Mtls; }
            set { m_Mtls = value; }
        }
        IEnumerable<Material> m_Mtls;

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<Vector3f> Offset
        {
            get { return m_Offset; }
            set { m_Offset = value; }
        }
        IEnumerable<Vector3f> m_Offset;

        /// <summary>
        /// 
        /// </summary>
        public Vector4f Sphere
        {
            get { return m_Sphere; }
            set { m_Sphere = value; }
        }
        Vector4f m_Sphere;

        /// <summary>
        /// 
        /// </summary>
        public Matrix34f BoxMatrix
        {
            get { return m_BoxMatrix; }
            set { m_BoxMatrix = value; }
        }
        Matrix34f m_BoxMatrix;

        /// <summary>
        /// 
        /// </summary>
        public Vector3f BoxSize
        {
            get { return m_BoxSize; }
            set { m_BoxSize = value; }
        }
        Vector3f m_BoxSize;

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<Int32> FloatBlindDataIds
        {
            get { return m_FloatBlindDataIds; }
            set { m_FloatBlindDataIds = value; }
        }
        IEnumerable<Int32> m_FloatBlindDataIds;

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<String> BlendTargetNames
        {
            get { return m_BlendTargetNames; }
            set { m_BlendTargetNames = value; }
        }
        IEnumerable<String> m_BlendTargetNames;
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        public Mesh()
        {
            this.FloatBlindDataIds = new List<Int32>();
            this.BlendTargetNames = new List<String>();
            this.Mtls = new List<Material>();
            this.Offset = new List<Vector3f>();
            this.BoxMatrix = new Matrix34f();
            for (int i = 0; i < MAX_FLOAT_BLIND_DATA; i++)
            {
               (this.FloatBlindDataIds as List<Int32>).Add(0);
            }
            Mesh.Version = 11;
        }
        #endregion // Constructors

        #region Serialisers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        public void Serialise(ref MeshSerialiser serialiser)
        {
            Mesh.Version = 0;
            if (!serialiser.Writing)
            {
                if (serialiser.Tokenizer.IsBinary() || serialiser.Tokenizer.CheckToken("Version", true))
                {
                    Mesh.Version = (Byte)serialiser.Tokenizer.GetByte(true);  
                }
            }
            else
            {
                Mesh.Version = 12;
                serialiser.Serialise(ref Mesh.m_Version, "Version");
               
            }

            serialiser.BeginBlock();
            
            serialiser.Serialise(ref this.m_Skinned, "Skinned");
            serialiser.Serialise(ref Mesh.m_BlendTargetCount, "BlendTargetCount");
            serialiser.Serialise(ref this.m_Mtls, "Mtl");
            serialiser.Serialise(ref this.m_Offset, "Offset", 1);
            serialiser.Serialise(ref this.m_Sphere, "Sphere");

            serialiser.Serialise(ref this.BoxMatrix.A, "BoxMatrix.a");
            serialiser.Serialise(ref this.BoxMatrix.B, "BoxMatrix.b");
            serialiser.Serialise(ref this.BoxMatrix.C, "BoxMatrix.c");
            serialiser.Serialise(ref this.BoxMatrix.D, "BoxMatrix.d");

            serialiser.Serialise(ref m_BoxSize, "BoxSize");


            for (int i = 0; i < MAX_FLOAT_BLIND_DATA; i++)
            {
                Int32 blindId = (this.FloatBlindDataIds as List<Int32>)[i];
                serialiser.Serialise(ref blindId);
                (this.FloatBlindDataIds as List<Int32>)[i] = blindId;
            }

            for (int i = 0; i < Mesh.BlendTargetCount; i++)
            {
                String blendTargetName = (this.BlendTargetNames as List<String>)[i];
                serialiser.Serialise(ref blendTargetName);
                (this.BlendTargetNames as List<String>)[i] = blendTargetName;
            }

            serialiser.EndBlock();
        }
        #endregion // Serialisers
    }
}
