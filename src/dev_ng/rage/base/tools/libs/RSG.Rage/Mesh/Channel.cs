﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Rage.Mesh
{
    /// <summary>
    /// This is stored as union in rage asset data so we might be better off using generics.
    /// </summary>
    public class ChannelData
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public Int32 S { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public UInt32 U { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Single F { get; set; }
        #endregion // Properties

        #region Constructors
        public ChannelData(Int32 s, UInt32 u, Single f)
        {
            this.S = s;
            this.U = u;
            this.F = f;
        }
        #endregion // Constructors
    }

    /// <summary>
    /// 
    /// </summary>
    public class Channel :
        IMeshComponent
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        UInt32 Name 
        {
            get { return m_Name; }
            set { m_Name = value; }
        }
        UInt32 m_Name;


        /// <summary>
        /// 
        /// </summary>
        public UInt32 VertexCount
        {
            get { return m_VertexCount; }
            set { m_VertexCount = value; }
        }
        UInt32 m_VertexCount;
        /// <summary>
        /// 
        /// </summary>
        public UInt32 FieldCount
        {
            get { return m_FieldCount; }
            set { m_FieldCount = value; }
        }
        UInt32 m_FieldCount;

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<ChannelData> Data { get; set; }
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        public Channel()
        {
            this.Data = new List<ChannelData>();
        }
        #endregion // Constructors

        #region Serialisation
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        public void Serialise(MeshSerialiser serialiser)
        {
            serialiser.Serialise(ref m_Name);
            String colon = ":";
	        serialiser.Separator(ref colon);
	        serialiser.Serialise(ref m_VertexCount);
	        serialiser.Serialise(ref m_FieldCount);
	        serialiser.BeginBlock();
	        UInt32 count = this.VertexCount * this.FieldCount;
	        if (!serialiser.Writing) 
            {    
		        this.Data = new List<ChannelData>();
	        }
            for (int i = 0; i < count; i++)
            {
                if (serialiser.Writing)
                {
                    UInt32 u = (Data as List<ChannelData>)[i].U;
                    serialiser.Serialise(ref u);
                }
                else
                {
                    UInt32 u = 0;
                    serialiser.Serialise(ref u);
                    ChannelData cd = new ChannelData(0, u, 0.0f);
                    (Data as List<ChannelData>).Add(cd);
                }
            }
	        serialiser.EndBlock();
        }
        #endregion // Serialisation
    }
}
