﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Rage.Mesh
{
    public sealed class Material :
        IMeshComponent
    {
        #region Properties
        public Int32 Id
        {
            get
            {
                return Convert.ToInt32(m_Name.Remove(0, 1));
            }
            set
            {
                this.m_Name = String.Format("#{0}", value);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public String Name
        {
            get { return this.m_Name; }
            set { this.m_Name = value; } 
        }
        String m_Name;

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<Primitive> Prims 
        {
            get { return this.m_Prims; } 
            set { this.m_Prims = value; } 
        }
        IEnumerable<Primitive> m_Prims;

        /// <summary>
        /// 
        /// </summary>
        public IDictionary<String, String> Atts
        {
            get { return this.m_Atts; }
            set { this.m_Atts = value; }
        }
        IDictionary<String, String> m_Atts;

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<Channel> Channels
        {
            get { return this.m_Channels; }
            set { this.m_Channels = value; }
        }
        IEnumerable<Channel> m_Channels;

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<Vertex> Verts
        {
            get { return this.m_Verts; }
            set { this.m_Verts = value; }
        }
        IEnumerable<Vertex> m_Verts;

        /// <summary>
        /// 
        /// </summary>
        public Int32 Priority
        {
            get { return this.m_Priority; }
            set { this.m_Priority = value; }
        }
        Int32 m_Priority;

        /// <summary>
        /// 
        /// </summary>
        public Int32 TexSetCount
        {
            get { return this.m_TexSetCount; }
            set { this.m_TexSetCount = value; }
        }
        Int32 m_TexSetCount;

        /// <summary>
        /// 
        /// </summary>
        public Int32 TanBiSetCount
        {
            get { return this.m_TanBiSetCount; }
            set { this.m_TanBiSetCount = value; }
        }
        Int32 m_TanBiSetCount;
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        public Material()
        {
            this.Prims = new List<Primitive>();
            this.Atts = new Dictionary<String, String>();
            this.Channels = new List<Channel>();
            this.Verts = new List<Vertex>();
        }
        #endregion // Constructors

        #region Serialisers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        public void Serialise(MeshSerialiser serialiser)
        {
            serialiser.BeginBlock();
            serialiser.Serialise(ref this.m_Name, "Name");
            serialiser.Serialise(ref this.m_Priority, "Priority");
            serialiser.Serialise(ref this.m_Prims, "Prim");
            serialiser.Serialise(ref this.m_Verts, "Verts");

            if (!serialiser.Writing)
            {
                for (int i = 0; i < this.Verts.Count(); i++)
                {
                    if ((this.Verts as List<Vertex>)[i].TexCount > this.TexSetCount)
                        this.TexSetCount = (this.Verts as List<Vertex>)[i].TexCount;
                    if ((this.Verts as List<Vertex>)[i].TanBiCount > this.TanBiSetCount)
                        this.TanBiSetCount = (this.Verts as List<Vertex>)[i].TanBiCount;
                }
            }
            if (Mesh.Version >= 8)
            {
                serialiser.Serialise(ref this.m_Channels, "Channels");
            }
            serialiser.EndBlock();
        }
        #endregion // Serialisers
    }
}
