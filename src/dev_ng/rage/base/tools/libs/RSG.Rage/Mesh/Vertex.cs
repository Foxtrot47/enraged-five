﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Math;

namespace RSG.Rage.Mesh
{
    public class TangentBinormal
    {
        #region Properties
        /// <summary>
        /// The tangent.
        /// </summary>
        public Vector3f T { get; set; }

        /// <summary>
        /// The binormal.
        /// </summary>
        public Vector3f B { get; set; }
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tan"></param>
        /// <param name="bi"></param>
        public TangentBinormal(Vector3f tan, Vector3f bi)
        {
            this.T = tan; 
            this.B = bi;
        }

        #endregion // Constrcutors
    }

    public class BlendTargetDelta
    {
        /// <summary>
        /// 
        /// </summary>
        public Vector4f Position { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Vector4f Normal { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public Vector4f Tangent { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public Vector4f BiNormal { get; set; }

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pos"></param>
        /// <param name="nrm"></param>
        /// <param name="tan"></param>
        /// <param name="bi"></param>
        public BlendTargetDelta(Vector4f pos, Vector4f nrm, Vector4f tan, Vector4f bi)
        {
            this.Position = pos;
            this.Normal = nrm;
            this.Tangent = tan;
            this.BiNormal = bi;
        }
        #endregion // Constructors
    }

    /// <summary>
    /// The vertex structure for a mesh object.	
    /// More information available here: https://devstar.rockstargames.com/wiki/index.php/Mesh_File_Description#Vertices   
    /// </summary>
    public class Vertex :
        IMeshComponent
    {
        #region Properties
        /// <summary>
        /// Mesh version number.
        /// </summary>
        public Byte MeshVersion
        {
            get { return m_MeshVersion; }
            set { m_MeshVersion = value; }
        }
        Byte m_MeshVersion;

        /// <summary>
        /// Number of blend/morph targets.
        /// </summary>
        public Byte BlendTargetCount
        {
            get { return m_BlendTargetCount; }
            set { m_BlendTargetCount = value; }
        }
        Byte m_BlendTargetCount;

        /// <summary>
        /// The vertex position.
        /// </summary>
        public Vector3f Pos
        {
            get { return m_Pos; }
            set { m_Pos = value; }
        }
        Vector3f m_Pos;

        /// <summary>
        /// Binding data required for skinning.
        /// </summary>
        public Binding Binding
        {
            get { return m_Binding; }
            set { m_Binding = value; }
        }
        Binding m_Binding;

        /// <summary>
        /// The vertex normal
        /// </summary>
        public Vector3f Norm
        {
            get { return m_Norm; }
            set { m_Norm = value; }
        }
        Vector3f m_Norm;

        /// <summary>
        /// First colour per vertex value.
        /// </summary>
        public Vector4f Cpv0
        {
            get { return m_Cpv0; }
            set { m_Cpv0 = value; }
        }
        Vector4f m_Cpv0;

        /// <summary>
        /// Second colour per vertex value.
        /// </summary>
        public Vector4f Cpv1
        {
            get { return m_Cpv1; }
            set { m_Cpv1 = value; }
        }
        Vector4f m_Cpv1;

        /// <summary>
        /// Third colour per vertex value.
        /// </summary>
        public Vector4f Cpv2
        {
            get { return m_Cpv2; }
            set { m_Cpv2 = value; }
        }
        Vector4f m_Cpv2;

        /// <summary>
        /// UV channel count.
        /// </summary>
        public Int32 TexCount
        {
            get { return m_TexCount; }
            set { m_TexCount = value; }
        }
        Int32 m_TexCount;

        /// <summary>
        /// Tangent binormal count.
        /// </summary>
        public Int32 TanBiCount
        {
            get { return m_TanBiCount; }
            set { m_TanBiCount = value; }
        }
        Int32 m_TanBiCount;

        /// <summary>
        /// UV sets.
        /// </summary>
        public IEnumerable<Vector2f> Tex
        {
            get { return m_Tex; }
            set { m_Tex = value; }
        }
        IEnumerable<Vector2f> m_Tex;

        /// <summary>
        /// Tangent binormals.  For banter.
        /// </summary>
        public IEnumerable<TangentBinormal> TanBi
        {
            get { return m_TanBi; }
            set { m_TanBi = value; }
        }
        IEnumerable<TangentBinormal> m_TanBi;

        /// <summary>
        /// 
        /// </summary>
        public Int32 FloatBlindDataCount
        {
            get { return m_FloatBlindDataCount; }
            set { m_FloatBlindDataCount = value; }
        }
        Int32 m_FloatBlindDataCount;

        /// <summary>
        /// Blind data; used to store data such as cloth tuning.
        /// </summary>
        public IEnumerable<Single> FloatBlindData
        {
            get { return m_FloatBlindData; }
            set { m_FloatBlindData = value; }
        }
        IEnumerable<Single> m_FloatBlindData;

        /// <summary>
        /// Per vertex deltas for blend/morph targets.
        /// </summary>
        public IEnumerable<BlendTargetDelta> BlendTargetDeltas
        {
            get { return m_BlendTargetDeltas; }
            set { m_BlendTargetDeltas = value; }
        }
        IEnumerable<BlendTargetDelta> m_BlendTargetDeltas;
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        public Vertex()
        {
            this.TanBi = new List<TangentBinormal>();
            this.FloatBlindData = new List<Single>();
            this.BlendTargetDeltas = new List<BlendTargetDelta>();
            this.Binding = new Binding();
            this.Norm = new Vector3f();
            this.Cpv0 = new Vector4f();
            this.Cpv1 = new Vector4f();
            this.Cpv2 = new Vector4f();

            // Pre-populate blind data
            for (int i = 0; i < Mesh.MAX_FLOAT_BLIND_DATA; i++)
            {
                (this.m_FloatBlindData as List<Single>).Add(0.0f);

            }

            this.Tex = new List<Vector2f>();
        }
        #endregion // Constructors

        #region Serialisers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        public void Serialise(MeshSerialiser serialiser)
        {
            serialiser.Serialise(ref this.m_Pos);
            String fwdSlash = "/";
	        serialiser.Separator(ref fwdSlash);
	        this.Binding.Serialise(serialiser);
	        serialiser.Separator(ref fwdSlash);

	        serialiser.SerialiseNrm(ref this.m_Norm);
	        serialiser.Separator(ref fwdSlash);
	        serialiser.Serialise(ref this.m_Cpv0);
            if (Mesh.Version >= 10)
	        {
		        serialiser.Separator(ref fwdSlash);
		        serialiser.Serialise(ref this.m_Cpv1);
		        serialiser.Separator(ref fwdSlash);
                if (Mesh.Version >= 11)
		        {
			        serialiser.Serialise(ref this.m_Cpv2);
			        serialiser.Separator(ref fwdSlash);
		        }
	        }
	        serialiser.Serialise(ref this.m_TexCount);
	        for (int i=0; i<this.m_TexCount; i++)
	        {
                if (serialiser.Writing)
                {
                    Vector2f tex = (this.m_Tex as List<Vector2f>)[i];
                    serialiser.Serialise(ref tex);
                }
                else
                {
                    Vector2f tex = new Vector2f();
                    serialiser.Serialise(ref tex);
                    (this.m_Tex as List<Vector2f>).Add(tex);
                }
	        }
	        serialiser.Separator(ref fwdSlash);
	        serialiser.Serialise(ref this.m_TanBiCount);

	        for (int i=0; i<this.m_TanBiCount; i++) 
	        {
                if (serialiser.Writing)
                {
                    Vector3f tan = (this.m_TanBi as List<TangentBinormal>)[i].T;
                    Vector3f bi = (this.m_TanBi as List<TangentBinormal>)[i].B;
                    serialiser.SerialiseNrm(ref tan);
                    serialiser.SerialiseNrm(ref bi);
                }
                else
                {
                    Vector3f tan = new Vector3f();
                    Vector3f bi = new Vector3f();
                    serialiser.SerialiseNrm(ref tan);
                    serialiser.SerialiseNrm(ref bi);
                    TangentBinormal tb = new TangentBinormal(tan, bi);
                    (this.m_TanBi as List<TangentBinormal>).Add(tb);
                }
	        }

            if (Mesh.Version > 6)
	        {
		        serialiser.Separator(ref fwdSlash);
		        serialiser.Serialise(ref m_FloatBlindDataCount);
		        for (int i=0; i<Mesh.MAX_FLOAT_BLIND_DATA; i++)
                {
                    Single blind = (this.m_FloatBlindData as List<Single>)[i];
			        serialiser.Serialise(ref blind);
                    (this.m_FloatBlindData as List<Single>)[i] = blind;
                }
	        }

            if (Mesh.Version >= 9)
	        {
		        serialiser.Separator(ref fwdSlash);

		        if( Mesh.BlendTargetCount > 0 )
		        {
			        // Allocate space for target deltas when reading
			        if( !serialiser.Writing )
			        { 
				        this.m_BlendTargetDeltas = new List<BlendTargetDelta>(Mesh.BlendTargetCount);
			        }

			        if ( this.m_BlendTargetDeltas.Count() > 0 )
			        {
                        for (int i = 0; i < Mesh.BlendTargetCount; i++)
				        {
                            Vector4f pos = (this.m_BlendTargetDeltas as List<BlendTargetDelta>)[i].Position;
                            Vector4f norm = (this.m_BlendTargetDeltas as List<BlendTargetDelta>)[i].Normal;
                            Vector4f tan = (this.m_BlendTargetDeltas as List<BlendTargetDelta>)[i].Tangent;
                            Vector4f bi = (this.m_BlendTargetDeltas as List<BlendTargetDelta>)[i].BiNormal;

					        serialiser.Serialise(ref pos);
					        serialiser.Serialise(ref norm);
					        serialiser.Serialise(ref tan);
					        serialiser.Serialise(ref bi);

                            BlendTargetDelta btd = new BlendTargetDelta(pos, norm, tan, bi);
                            (this.m_BlendTargetDeltas as List<BlendTargetDelta>)[i] = btd;
				        }
			        }
		        }
	        }
        }
        #endregion // Serialisers
    }
}
