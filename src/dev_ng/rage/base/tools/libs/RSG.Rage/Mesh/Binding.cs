﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Rage.Mesh
{
    /// <summary>
    /// A binding contains references to up to four matrices and
    /// four blend weights.  The weights always add up to 1.0f.
    /// If you have fewer than four matrices, set the unused weights 
    /// to zero and the indices will be ignored.
    /// </summary>
    public class Binding
    {
        #region Constants
        /// <summary>
        /// Support for up to 4 maxtrices per vertex
        /// </summary>
        private readonly Int32 MAX_MATRICES_PER_VERTEX = 4;
        #endregion // Constants

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<Int32> Matrices 
        {
            get { return m_Matrices; }
            set { m_Matrices = value; } 
        }
        IEnumerable<Int32> m_Matrices; 

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<Single> Weights
        {
            get { return m_Weights; }
            set { m_Weights = value; } 
        }
        IEnumerable<Single> m_Weights; 

        /// <summary>
        /// 
        /// </summary>
        public Boolean IsPassThrough
        {
            get { return m_IsPassThrough; }
            set { m_IsPassThrough = value; }
        }
        Boolean m_IsPassThrough; 
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        public Binding()
        {
            this.Matrices = new List<Int32>();
            this.Weights = new List<Single>();
        }
        #endregion // Constructors

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        private void Reset()
        {
            (this.Matrices as List<Int32>).Clear();
            (this.Weights as List<Single>).Clear();
            for (int i = 0; i < Mesh.MAX_MATRICES_PER_VERTEX; i++)
            {
                (this.Matrices as List<Int32>).Add(0);
                (this.Weights as List<Single>).Add(0.0f);
            }
        }
        #endregion // Private Methods

        #region Serialisers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="serialiser"></param>
        public void Serialise(MeshSerialiser serialiser)
        {
            if (!serialiser.Writing)
	        {
		        this.Reset();
	        }
            for (int i = 0; i < Mesh.MAX_MATRICES_PER_VERTEX; i++) 
	        {
                Single weight = (this.Weights as List<Single>)[i];
		        serialiser.Serialise(ref weight);
                (this.Weights as List<Single>)[i] = weight;
		        if ((this.Weights as List<Single>)[i] == 0)
		        {
			        break;
		        }
		        if (serialiser.Tokenizer.IsBinary()) 
		        {
			        // copy in and out so serialization works either way
                    Byte temp = (Byte)(this.Matrices as List<Int32>)[i];
			        serialiser.Serialise(ref temp);
			        (this.Matrices as List<Int32>)[i] = temp;
		        }
		        else
		        {
                    Byte temp = (Byte)(this.Matrices as List<Int32>)[i];
                    serialiser.Serialise(ref temp);
                    (this.Matrices as List<Int32>)[i] = temp;
		        }
	        }
        }
        #endregion // Serialisers
    }
}
