﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Rage.Mesh
{
    public interface IMeshComponent
    {
        #region Serialisers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="serialiser"></param>
        void Serialise(MeshSerialiser serialiser);
        #endregion 
    }
}
