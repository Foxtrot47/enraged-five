﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using RSG.Base.Math;

namespace RSG.Rage.Shader
{
    /// <summary>
    /// 
    /// </summary>
    public class ShaderVariables :
        IShaderVariables
    {
        #region Constants
        /// <summary>
        /// NONE type name (for parity with rage).
        /// </summary>
        private static String TYPE_NONE = "NONE";

        /// <summary>
        /// Int type name.
        /// </summary>
        private static String TYPE_INT = "int";

        /// <summary>
        /// Float type name.
        /// </summary>
        private static String TYPE_FLOAT = "float";

        /// <summary>
        /// Vector2 type name.
        /// </summary>
        private static String TYPE_VECTOR2 = "Vector2";

        /// <summary>
        /// Vector3 type name.
        /// </summary>
        private static String TYPE_VECTOR3 = "Vector3";

        /// <summary>
        /// Vector4 type name.
        /// </summary>
        private static String TYPE_VECTOR4 = "Vector4";

        /// <summary>
        /// Texture type name.
        /// </summary>
        private static String TYPE_TEXTURE = "grcTexture";

        /// <summary>
        /// Boolean type name.
        /// </summary>
        private static String TYPE_BOOL = "bool";

        /// <summary>
        /// Matrix34 type name.
        /// </summary>
        private static String TYPE_MATRIX34 = "Matrix34";

        /// <summary>
        /// Matrix44 type name.
        /// </summary>
        private static String TYPE_MATRIX44 = "Matrix44";

        /// <summary>
        /// String type name.
        /// </summary>
        private static String TYPE_STRING = "string";
        #endregion 

        #region Static Members
         /// <summary>
        /// Convenience static dictionary for mapping enviroment vairable type names
        /// </summary>
        private static IDictionary<String, VarType> m_VariableTypeMap =
            new Dictionary<String, VarType>()
        {
            {TYPE_NONE, VarType.None},
            {TYPE_INT, VarType.Int},
            {TYPE_FLOAT, VarType.Float},
            {TYPE_VECTOR2, VarType.Vector2},
            {TYPE_VECTOR3, VarType.Vector3},
            {TYPE_VECTOR4, VarType.Vector4},
            {TYPE_TEXTURE, VarType.Texture},
            {TYPE_BOOL, VarType.Bool},
            {TYPE_MATRIX34, VarType.Matrix34},
            {TYPE_MATRIX44, VarType.Matrix44},
            {TYPE_STRING, VarType.String},
        };
        #endregion // Static Members

        #region Properties
        /// <summary>
        /// List of shader variables objects.
        /// </summary>
        public List<IShaderVariable> Variables { get; set; }
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        public ShaderVariables()
        {
            this.Variables = new List<IShaderVariable>();
        }
        #endregion // Constructors

        #region Public Methods
        /// <summary>
        /// Overriden equality method.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            ShaderVariables sv = obj as ShaderVariables;
            if (sv == null)
            {
                return false;
            }

            return this.Equals(sv);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sv"></param>
        /// <returns></returns>
        public bool Equals(ShaderVariables sv)
        {
            // If parameter is null return false:
            if ((object)sv == null)
            {
                return false;
            }

            if (this.Variables.Count() != sv.Variables.Count())
                return false;

            for (Int32 i = 0; i < this.Variables.Count(); i++)
            {
                if (!this.Variables[i].Equals(sv.Variables[i]))
                    return false;
            }

            return true;
        }
        #endregion Public Methods

        #region Public Static Methods
        /// <summary>
        /// Returns variable type given the type name.
        /// </summary>
        /// <param name="typename"></param>
        /// <returns></returns>
        public static VarType GetVarType(String typename)
        {
            return ((m_VariableTypeMap as Dictionary<String, VarType>)[typename]);
        }

        /// <summary>
        /// Returns variable type name given the type.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static String GetVarTypeName(VarType type)
        {
            return ((m_VariableTypeMap as Dictionary<String, VarType>).FirstOrDefault(x => x.Value == type).Key);
        }
        #endregion // Public Static Methods


    }
}
