﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Math;
using RSG.ManagedRage.File;

namespace RSG.Rage.Shader
{
    #region Enumerables
    /// <summary>
    /// 
    /// </summary>
    public enum VarType
    {
        None,
        Int,
        Float,
        Vector2,
        Vector3,
        Vector4,
        Texture,
        Bool,
        Matrix34,
        Matrix44,
        String
    }
    #endregion // Enumerables

    /// <summary>
    /// 
    /// </summary>
    public interface IShaderVariable
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        String Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        VarType Type { get; set; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <returns></returns>
        bool LoadAscii(AsciiTokenizer tokenizer);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <returns></returns>
        bool SaveAscii(AsciiTokenizer tokenizer);
        #endregion 
    }

    /// <summary>
    /// 
    /// </summary>
    public abstract class ShaderVariableBase :
        IShaderVariable
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public String Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public VarType Type { get; set; }
        #endregion // Properties

        #region Constructors
        public ShaderVariableBase()
        {

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="type"></param>
        public ShaderVariableBase(String name, VarType type)
        {
            this.Name = name;
            this.Type = type;
        }
        #endregion // Constructors

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <returns></returns>
        public abstract bool LoadAscii(AsciiTokenizer tokenizer);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <returns></returns>
        public abstract bool SaveAscii(AsciiTokenizer tokenizer);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        public bool SaveAsciiBase(AsciiTokenizer tokenizer)
        {
            tokenizer.Put(this.Name, 0);
            tokenizer.PutDelimiter(" {\n\t");
            tokenizer.Put(ShaderVariables.GetVarTypeName(this.Type), 0);
            tokenizer.Put(" ", 0);
            return (true);
        }
        #endregion // Controller Methods
    }

    /// <summary>
    /// 
    /// </summary>
    public sealed class FloatVariable :
        ShaderVariableBase,
        IShaderVariable
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public Single Value { get; set; }
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        public FloatVariable()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="type"></param>
        public FloatVariable(String name, VarType type)
            : base(name, type)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="type"></param>
        /// <param name="value"></param>
        public FloatVariable(String name, VarType type, Single value)
            : base(name, type)
        {
            this.Value = value;
        }
        #endregion // Constructors

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <returns></returns>
        public override bool LoadAscii(AsciiTokenizer tokenizer)
        {
            this.Value = tokenizer.GetFloat(true);
            return (true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <returns></returns>
        public override bool SaveAscii(AsciiTokenizer tokenizer)
        {
            this.SaveAsciiBase(tokenizer);
            tokenizer.Put(this.Value);
            tokenizer.PutDelimiter("\n}\n\n");
            return (true);
        }
        #endregion // Controller

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            FloatVariable v = obj as FloatVariable;
            if (v == null)
            {
                return false;
            }

            return this.Equals(v);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public bool Equals(FloatVariable v)
        {
            // If parameter is null return false:
            if ((object)v == null)
            {
                return false;
            }

            return (this.Value == v.Value && this.Name == v.Name);
        }
        #endregion // Public Methods

    }

    /// <summary>
    /// 
    /// </summary>
    public sealed class Vector2Variable :
        ShaderVariableBase,
        IShaderVariable
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public Vector2f Value { get; set; }
        #endregion // Properties

         #region Constructors
        /// <summary>
        /// 
        /// </summary>
        public Vector2Variable()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="type"></param>
        public Vector2Variable(String name, VarType type)
            : base(name, type)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="type"></param>
        /// <param name="value"></param>
        public Vector2Variable(String name, VarType type, Vector2f value)
            : base(name, type)
        {
            this.Value = value;
        }
        #endregion // Constructors

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <returns></returns>
        public override bool LoadAscii(AsciiTokenizer tokenizer)
        {
            RSG.ManagedRage.Math.Vector2 vec = tokenizer.GetVector2(true);
            this.Value = new Vector2f(vec.x, vec.y);
            return (true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <returns></returns>
        public override bool SaveAscii(AsciiTokenizer tokenizer)
        {
            this.SaveAsciiBase(tokenizer);
            tokenizer.Put(new RSG.ManagedRage.Math.Vector2(this.Value.X, this.Value.Y));
            tokenizer.PutDelimiter("\n}\n\n");
            return (true);
        }
        #endregion // Controller

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Vector2Variable v = obj as Vector2Variable;
            if (v == null)
            {
                return false;
            }

            return this.Equals(v);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public bool Equals(Vector2Variable v)
        {
            // If parameter is null return false:
            if ((object)v == null)
            {
                return false;
            }

            return (this.Value == v.Value && this.Name == v.Name);
        }
        #endregion // Public Methods
    }

    /// <summary>
    /// 
    /// </summary>
    public sealed class Vector3Variable :
        ShaderVariableBase,
        IShaderVariable
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public Vector3f Value { get; set; }
        #endregion // Properties

         #region Constructors
        /// <summary>
        /// 
        /// </summary>
        public Vector3Variable()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="type"></param>
        public Vector3Variable(String name, VarType type)
            : base(name, type)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="type"></param>
        /// <param name="value"></param>
        public Vector3Variable(String name, VarType type, Vector3f value)
            : base(name, type)
        {
            this.Value = value;
        }
        #endregion // Constructors

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <returns></returns>
        public override bool LoadAscii(AsciiTokenizer tokenizer)
        {
            RSG.ManagedRage.Math.Vector3 vec = tokenizer.GetVector3(true);
            this.Value = new Vector3f(vec.x, vec.y, vec.z);
            return (true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <returns></returns>
        public override bool SaveAscii(AsciiTokenizer tokenizer)
        {
            this.SaveAsciiBase(tokenizer);
            tokenizer.Put(new RSG.ManagedRage.Math.Vector3(this.Value.X, this.Value.Y, this.Value.Z));
            tokenizer.PutDelimiter("\n}\n\n");
            return (true);
        }
        #endregion // Controller

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Vector3Variable v = obj as Vector3Variable;
            if (v == null)
            {
                return false;
            }

            return this.Equals(v);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public bool Equals(Vector3Variable v)
        {
            // If parameter is null return false:
            if ((object)v == null)
            {
                return false;
            }

            return (this.Value == v.Value && this.Name == v.Name);
        }
        #endregion // Public Methods
    }

    /// <summary>
    /// 
    /// </summary>
    public sealed class Vector4Variable :
        ShaderVariableBase,
        IShaderVariable
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public Vector4f Value { get; set; }
        #endregion // Properties

         #region Constructors
        /// <summary>
        /// 
        /// </summary>
        public Vector4Variable()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="type"></param>
        public Vector4Variable(String name, VarType type)
            : base(name, type)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="type"></param>
        /// <param name="value"></param>
        public Vector4Variable(String name, VarType type, Vector4f value)
            : base(name, type)
        {
            this.Value = value;
        }
        #endregion // Constructors

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <returns></returns>
        public override bool LoadAscii(AsciiTokenizer tokenizer)
        {
            RSG.ManagedRage.Math.Vector4 vec = tokenizer.GetVector4(true);
            this.Value = new Vector4f(vec.x, vec.y, vec.z, vec.w);
            return (true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <returns></returns>
        public override bool SaveAscii(AsciiTokenizer tokenizer)
        {
            this.SaveAsciiBase(tokenizer);
            tokenizer.Put(new RSG.ManagedRage.Math.Vector4(this.Value.X, this.Value.Y, this.Value.Z, this.Value.W));
            tokenizer.PutDelimiter("\n}\n\n");
            return (true);
        }
        #endregion // Controller

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Vector4Variable v = obj as Vector4Variable;
            if (v == null)
            {
                return false;
            }

            return this.Equals(v);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public bool Equals(Vector4Variable v)
        {
            // If parameter is null return false:
            if ((object)v == null)
            {
                return false;
            }

            return (this.Value == v.Value && this.Name == v.Name);
        }
        #endregion // Public Methods
    }

    /// <summary>
    /// 
    /// </summary>
    public sealed class TextureVariable :
        ShaderVariableBase,
        IShaderVariable
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public String Value { get; set; }
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        public TextureVariable()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="type"></param>
        public TextureVariable(String name, VarType type)
            : base(name, type)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="type"></param>
        /// <param name="value"></param>
        public TextureVariable(String name, VarType type, String value)
            : base(name, type)
        {
            this.Value = value;
        }
        #endregion // Constructors

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <returns></returns>
        public override bool LoadAscii(AsciiTokenizer tokenizer)
        {
            String texname = String.Empty;
            tokenizer.GetToken(ref texname, TypeFileParser.MAX_NAMELEN);
            this.Value = texname;
            return (true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <returns></returns>
        public override bool SaveAscii(AsciiTokenizer tokenizer)
        {
            this.SaveAsciiBase(tokenizer);
            tokenizer.Put(this.Value, 0);
            tokenizer.PutDelimiter("\n}\n\n");
            return (true);
        }
        #endregion // Controller

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            TextureVariable v = obj as TextureVariable;
            if (v == null)
            {
                return false;
            }

            return this.Equals(v);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public bool Equals(TextureVariable v)
        {
            // If parameter is null return false:
            if ((object)v == null)
            {
                return false;
            }

            return (this.Value == v.Value && this.Name == v.Name);
        }
        #endregion // Public Methods
    }
}
