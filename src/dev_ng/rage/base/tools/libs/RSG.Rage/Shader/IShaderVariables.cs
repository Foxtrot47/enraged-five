﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Math;

namespace RSG.Rage.Shader
{
    /// <summary>
    /// 
    /// </summary>
    public interface IShaderVariables
    {
        #region Properties
        /*
        /// <summary>
        /// Integer shader variables.
        /// </summary>
        IDictionary<String, Int32> Ints { get; set; }

        /// <summary>
        /// Float shader variables.
        /// </summary>
        IDictionary<String, Single> Floats { get; set; }

        /// <summary>
        /// Vecto2 shader variables.
        /// </summary>
        IDictionary<String, Vector2f> Vector2s { get; set; }

        /// <summary>
        /// Vector3 shader variables
        /// </summary>
        IDictionary<String, Vector3f> Vector3s { get; set; }

        /// <summary>
        /// Vector4 shader variables
        /// </summary>
        IDictionary<String, Vector4f> Vector4s { get; set; }

        /// <summary>
        /// Texture shader variables.
        /// </summary>
        IDictionary<String, String> Textures { get; set; }
         * */

        List<IShaderVariable> Variables { get; set; }
        #endregion //Properties
    }
}
