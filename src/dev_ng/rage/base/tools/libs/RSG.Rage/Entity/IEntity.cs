﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.ManagedRage.File;

namespace RSG.Rage.Entity
{
    /// <summary>
    /// 
    /// </summary>
    public interface IEntity
    {
        #region Controller Methods
        /// <summary>
        /// IEntity method for loading data from ascii tokenizer object.
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <returns></returns>
        bool LoadAscii(AsciiTokenizer tokenizer);

        /// <summary>
        /// IEntity method for saveing data to ascii tokenizer object.
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <returns></returns>
        bool SaveAscii(AsciiTokenizer tokenizer);
        #endregion
    }
}
