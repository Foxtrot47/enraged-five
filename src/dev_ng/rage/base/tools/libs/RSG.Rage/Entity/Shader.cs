﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.ManagedRage.File;
using RSG.Rage.Shader;

namespace RSG.Rage.Entity
{
    /// <summary>
    /// 
    /// </summary>
    public class Shader :
        EntityBase,
        IShader
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public String PresetName { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public String SvaFile { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Int32 DrawBucket { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public ShaderVariables ShaderVars { get; set; }
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="presetname"></param>
        /// <param name="saveFile"></param>
        public Shader(String presetname, String svaFile)
        {
            this.PresetName = presetname;
            this.SvaFile = svaFile;
            this.DrawBucket = -1;
            this.ShaderVars = new ShaderVariables();
        }

        /// <summary>
        /// 
        /// </summary>
        public Shader()
        {
            this.ShaderVars = new ShaderVariables();
        }
        #endregion // Constructors

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <returns></returns>
        public override bool LoadAscii(AsciiTokenizer tokenizer)
        {
            String buf = String.Empty;
            while (!tokenizer.CheckToken("}", false) && tokenizer.GetToken(ref buf, TypeFileParser.MAX_NAMELEN) > 0)
            {
                tokenizer.MatchToken("{");
                String typeBuf = String.Empty;
		        tokenizer.GetToken(ref typeBuf, TypeFileParser.MAX_NAMELEN);

                VarType type = ShaderVariables.GetVarType(typeBuf);

                Int32 count = 1;
                if (tokenizer.CheckToken("Count", true))
                    count = tokenizer.GetInt(true);

                IShaderVariable shaderVar = null;
                switch (type)
                {
                    case VarType.Texture:
                        // handle missing texture.
                        if (!tokenizer.CheckToken("}", false))
                        {
                            shaderVar = new TextureVariable(buf, type);
                            shaderVar.LoadAscii(tokenizer);
                        }
                        break;
                    case VarType.Float:
                        shaderVar = new FloatVariable(buf, type);
                        shaderVar.LoadAscii(tokenizer);
                        break;
                    case VarType.Vector2:
                        shaderVar = new Vector2Variable(buf, type);
                        shaderVar.LoadAscii(tokenizer);
                        break;
                    case VarType.Vector3:
                        shaderVar = new Vector3Variable(buf, type);
                        shaderVar.LoadAscii(tokenizer);
                        break;
                    case VarType.Vector4:
                        shaderVar = new Vector4Variable(buf, type);
                        shaderVar.LoadAscii(tokenizer);
                        break;
                    default:
                        Console.WriteLine("Error: Unkown type found during LoadAscii: {0}", typeBuf);
                        break;
                }

                if (shaderVar != null)
                {
                    (this.ShaderVars.Variables as List<IShaderVariable>).Add(shaderVar);
                }
                tokenizer.MatchToken("}");
            }
            return (true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <returns></returns>
        public override bool SaveAscii(AsciiTokenizer tokenizer)
        {
            foreach (IShaderVariable shaderVar in this.ShaderVars.Variables)
                if (null!=shaderVar)
                    shaderVar.SaveAscii(tokenizer);

            return (true);
        }
        #endregion // Controller Methods

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Shader s = obj as Shader;
            if (s == null)
            {
                return false;
            }

            return this.Equals(s);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public bool Equals(Shader s)
        {
            // If parameter is null return false:
            if ((object)s == null)
            {
                return false;
            }

            if (this.SvaFile == s.SvaFile)
                return true;

            if (this.PresetName != s.PresetName)
            {
                return false;
            }

            return this.ShaderVars.Equals(s.ShaderVars);
        }
        #endregion // Public Methods


    }
}
