﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using RSG.ManagedRage.File;
using RSG.Rage.Mesh;

namespace RSG.Rage.Entity
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class Model :
        IModel
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public String Name { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public Int32 BondIdx { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public Int32 Mask { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public Mesh.Mesh Mesh { get; private set; }
        #endregion // Properties

        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="boneIdx"></param>
        /// <param name="mask"></param>
        public Model(String name, Int32 boneIdx, Int32 mask)
        {
            this.Name = name;
            this.BondIdx = boneIdx;
            this.Mask = mask;
        }
        #endregion // Constructor

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="map"></param>
        public void RemapMaterialIds(IDictionary<Int32, Int32> map)
        {
            foreach(Material mtl in this.Mesh.Mtls)
            {
                if (map.ContainsKey(mtl.Id))
                    mtl.Id = map[mtl.Id];
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        public bool LoadMesh(String filename)
        {
            bool result = true;
            BaseTokenizer tokenizer = TokenUtility.OpenTokenizedFile(filename);//new AsciiTokenizer();
            
            MeshSerialiser msa = new MeshSerialiser(ref tokenizer, false);
            this.Mesh = new Mesh.Mesh();
            this.Mesh.Serialise(ref msa);
            tokenizer.Save();

            return (result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public bool SaveMesh(String filename)
        {
            bool result = true;
            BaseTokenizer tokenizer = TokenUtility.CreateTokenizedFile(filename, TokenizerType.Binary);
            
            MeshSerialiser msa = new MeshSerialiser(ref tokenizer, true);
            this.Mesh.Serialise(ref msa);
            tokenizer.Save();

            return (result);
        }
        #endregion // Controller Methods
    }

    public interface ILod
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        IEnumerable<IModel> Models { get; set; }

        /// <summary>
        /// 
        /// </summary>
        LodLevel Level { get; set; }

        /// <summary>
        /// 
        /// </summary>
        String LevelName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        Int32 Depth { get; set; }
        #endregion // Properties
    }

    /// <summary>
    /// 
    /// </summary>
    public abstract class LodBase :
        EntityBase,
        ILod
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<IModel> Models { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public LodLevel Level { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public String LevelName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Int32 Depth { get; set; }
        #endregion // Properties
    }

    /// <summary>
    /// 
    /// </summary>
    public sealed class Lod :
        LodBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public Single Threshold { get; set; }
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        public Lod(LodLevel level, String name, Int32 depth)
        {
            this.Models = new List<IModel>();
            this.Level = level;
            this.LevelName = name;
            this.Depth = depth;
        }
        #endregion // Constructors

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <returns></returns>
        public override bool LoadAscii(AsciiTokenizer tokenizer)
        {
            return (true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <returns></returns>
        public override bool SaveAscii(AsciiTokenizer tokenizer)
        {
            tokenizer.Put(this.LevelName, 0);
            tokenizer.PutDelimiter(" ");
            if (this.Models.Count() == 0)
            {
                tokenizer.Put("none ", 0);
                tokenizer.Put(this.Threshold);
            }
            else
            {
                tokenizer.Put(this.Models.Count());
                
                foreach (Model model in this.Models)
                {
                    tokenizer.Put("\n", 0);
                    tokenizer.PutMultipleDelimeter("\t", this.Depth);
                    tokenizer.Put(model.Name, 0);
                    tokenizer.Put(" ", 0);
                    tokenizer.Put(model.BondIdx);
                }
                tokenizer.Put("\n", 0);
                tokenizer.PutMultipleDelimeter("\t", this.Depth);
                tokenizer.Put(this.Threshold);
            }

            return (true);
        }

        /// <summary>
        /// 
        /// </summary>
        public void SetModel(String modelName)
        {
            (this.Models as List<String>).Add(modelName);
        }
        #endregion // Controller Methods
    }

    /// <summary>
    /// 
    /// </summary>
    public sealed class ClothLod :
        LodBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public Single SwitchDistanceDown { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Single SwitchDistanceUp { get; set; }
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        public ClothLod(LodLevel level, String name, Int32 depth)
        {
            this.Models = new List<IModel>();
            this.Level = level;
            this.LevelName = name;
            this.Depth = depth;
        }
        #endregion // Constructors

        #region Controller Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <returns></returns>
        public override bool LoadAscii(AsciiTokenizer tokenizer)
        {
            return (true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <returns></returns>
        public override bool SaveAscii(AsciiTokenizer tokenizer)
        {
            tokenizer.PutIndented(this.LevelName);
            tokenizer.PutDelimiter(" ");
            if (this.Models.Count() == 0)
            {
                tokenizer.Put("none ");
                tokenizer.Put("\n");
                tokenizer.Indent(1);
                tokenizer.PutIndented(Convert.ToString(this.SwitchDistanceDown));
                tokenizer.Put("\n");
                tokenizer.PutIndented(Convert.ToString(this.SwitchDistanceUp));
                tokenizer.Indent(-1);
            }
            else
            {
                tokenizer.Put(this.Models.Count());
                tokenizer.Indent(1);
                foreach (Model model in this.Models)
                {
                    tokenizer.Put("\n");
                    tokenizer.PutIndented(model.Name);
                    tokenizer.Put(" ");
                    tokenizer.Put(model.BondIdx);
                }
                tokenizer.Put("\n");
                tokenizer.PutIndented(Convert.ToString(this.SwitchDistanceDown));
                tokenizer.Put("\n");
                tokenizer.PutIndented(Convert.ToString(this.SwitchDistanceUp));
                tokenizer.Indent(-1);
            }

            return (true);
        }
        #endregion // Controller Methods
    }
}
