﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.ManagedRage.File;

namespace RSG.Rage.Entity
{
    public abstract class GroupBase :
        EntityBase
    {
        #region Constants
        /// <summary>
        /// 
        /// </summary>
        public const String HIGH_LOD = "high";

        /// <summary>
        /// 
        /// </summary>
        public const String MED_LOD = "med";

        /// <summary>
        /// 
        /// </summary>
        public const String LOW_LOD = "low";

        /// <summary>
        /// 
        /// </summary>
        public const String VLOW_LOD = "vlow";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public IDictionary<LodLevel, ILod> Lods { get; protected set; }

        #endregion // Properties

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <returns></returns>
        public override bool LoadAscii(AsciiTokenizer tokenizer)
        {
            return (true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <returns></returns>
        public override bool SaveAscii(AsciiTokenizer tokenizer)
        {
            return (true);
        }
        #endregion // Controller Methods

    }
}
