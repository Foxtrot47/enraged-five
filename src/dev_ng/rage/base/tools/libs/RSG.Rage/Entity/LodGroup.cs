﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Diagnostics;
using RSG.ManagedRage.File;
using RSG.ManagedRage.Math;

namespace RSG.Rage.Entity
{
    #region Enumerations
    [Flags]
    public enum LodLevel
    {
        High = 1 << 0,
        Medium = 1 << 1,
        Low = 1 << 2,
        VeryLow = 1 << 3,
        All = ~0
    }

    /// <summary>
    /// 
    /// </summary>
    public enum LodType
    {
        Main,
        Damaged,
        None
    }

    public enum LodGroupType
    {
        Default,
        Cloth
    }
    #endregion // Enumerations

    /// <summary>
    /// 
    /// </summary>
    public class LodGroupComponents :
        EntityBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public LodGroup MainComponent { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public LodGroup DamagedComponent { get; set; }

        /// <summary>
        /// 
        /// </summary>
        Int32 Level { get; set; }

        /// <summary>
        /// The LOD groups index for whe it appears within a hiearchy (such as in a fragment).
        /// </summary>
        public LodGroupType LodGroupType { get; set; }
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        public LodGroupComponents(Int32 level, LodGroupType lodGroupType)
        {
            this.MainComponent = new LodGroup(level, LodType.Main);
            this.DamagedComponent = new LodGroup(level, LodType.Damaged);
            this.Level = level;
            this.LodGroupType = lodGroupType;
        }
        #endregion // Constructors

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <returns></returns>
        public override bool LoadAscii(AsciiTokenizer tokenizer)
        {
            this.MainComponent.LoadAscii(tokenizer);
            this.DamagedComponent.LoadAscii(tokenizer);

            return (true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <returns></returns>
        public override bool SaveAscii(AsciiTokenizer tokenizer)
        {
            tokenizer.PutMultipleDelimeter("\t", this.Level);
            if (this.LodGroupType == LodGroupType.Cloth)
                tokenizer.Put("lodgroupcloth", 0);
            else
                tokenizer.Put("lodgroup", 0);
            tokenizer.PutDelimiter(" {\n");


            this.MainComponent.SaveAscii(tokenizer);
            if (this.DamagedComponent.Lods.Count() > 0)
            {
                this.DamagedComponent.SaveAscii(tokenizer);
            }
            tokenizer.PutDelimiter("}\n");
            return (true);
        }
        #endregion // Controller Methods
    }

    /// <summary>
    /// 
    /// </summary>
    public class LodGroup :
        ClothGroup
    {
       
        #region Properties
        
        /*
        /// <summary>
        /// 
        /// </summary>
        public IDictionary<LodLevel, Lod> DamagedLods { get; private set; }
        */
        /// <summary>
        /// 
        /// </summary>
        public Boolean Optimise { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public Vector3 CullSphere { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public Single CullRadius { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public Vector3 BoxMin { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public Vector3 BoxMax { get; private set; }

        /// <summary>
        /// The LOD groups index for whe it appears within a hiearchy (such as in a fragment).
        /// </summary>
        public Int32 Index { get; set; }

        /// <summary>
        /// How deep we are in the hierarchy, required for indentation.
        /// </summary>
        Int32 Level { get; set; }


        /// <summary>
        /// 
        /// </summary>
        LodType LodType { get; set; }

       
        #endregion //Properties

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        public LodGroup(Int32 level, LodType lodType)
        {
            this.Lods = new Dictionary<LodLevel, ILod>();
            //this.DamagedLods = new Dictionary<LodLevel, Lod>();
            this.CullSphere = new RSG.ManagedRage.Math.Vector3(0, 0, 0);
            this.BoxMin = new RSG.ManagedRage.Math.Vector3(0, 0, 0);
            this.BoxMax = new RSG.ManagedRage.Math.Vector3(0, 0, 0);
            this.Level = level;
            this.LodType = lodType;
            //this.LoadState = LoadStateType.None;
        }
        #endregion // Constructors

        #region Controller Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <param name="match"></param>
        /// <param name="level"></param>
        /// <param name="lodMask"></param>
        /// <param name="optimise"></param>
        /// <param name="extraVerts"></param>
        void LoadLod(ref AsciiTokenizer tokenizer, String match, LodLevel level, LodLevel lodMask, bool optimise, Int32 extraVerts)
        {
            //Debug.Assert(this.LoadState != LoadStateType.None, "No Lod state set when loading lod group data.  Illegal.");
            IDictionary<LodLevel, ILod> lods = new Dictionary<LodLevel, ILod>();
            lods = this.Lods;
            /*switch(this.LoadState)
            {
                case LoadStateType.Main:
                    lods = this.Lods;
                    break;
                case LoadStateType.Damaged:
                    lods = this.DamagedLods;
                    break;
                default:
                    return; // Whoops, we've called our lod group loader without defining the state.
            }*/

            // Are we supposed to load this LOD?
            if ((lodMask & level) == level)
            {
                // Yeah, load it here.
                String buf = String.Empty;
                tokenizer.MatchToken(match);
                Lod lod = new Lod(level, match, this.Level + 3);


                (lods as Dictionary<LodLevel, ILod>).Add(LodNameToLevel(match), lod);
                if (!tokenizer.CheckToken("none", true))
                {
                    tokenizer.GetToken(ref buf, TypeFileParser.MAX_NAMELEN);
                    Int32 count = count = Convert.ToInt32(buf);

                    for (int i = 0; i < count; i++)
                    {
                        tokenizer.GetToken(ref buf, TypeFileParser.MAX_NAMELEN);
                        Int32 boneIndex = tokenizer.GetInt(true);

                        Byte mask = 0xFF;
                        if (tokenizer.CheckToken("mask", true))
                        {
                            String maskBuf = String.Empty;
                            tokenizer.GetToken(ref maskBuf, TypeFileParser.MAX_NAMELEN);
                            mask = Byte.Parse(maskBuf, NumberStyles.HexNumber);
                        }
                        Model model = new Model(buf, boneIndex, mask);
                        (lod.Models as List<IModel>).Add(model);
                    }
                }
                lod.Threshold = tokenizer.GetFloat(true);
            }
            else // skip it
            {
                // Load dummy for tokenizer sake?  Assuming order?
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="cbData"></param>
        public void LoadLodGroup(ref TypeFileCbData cbData)
        {
            AsciiTokenizer tokenizer = cbData.Tokenizer;
            //this.LoadState = LoadStateType.Main;
            this.LoadAscii(tokenizer);
        }

        /*
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cbData"></param>
        public void LoadDamagedLodGroup(ref TypeFileCbData cbData)
        {
            AsciiTokenizer tokenizer = cbData.Tokenizer;
            this.LoadState = LoadStateType.Damaged;
            this.LoadAscii(ref tokenizer);
        }
        */
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <returns></returns>
        public override bool LoadAscii(AsciiTokenizer tokenizer)
        {
            tokenizer.MatchToken("{");

            //bool optimize = true;
            if (tokenizer.CheckToken("optimize", true))
            {
                this.Optimise = tokenizer.GetInt(true) > 0 ? true : false;
            }

            Int32 extraverts = 0;
            if (tokenizer.CheckToken("extraverts", true))
            {
                extraverts = tokenizer.GetInt(true);
                const Int32 MAX_EXTRA_VERTS = 255;
                Debug.Assert(extraverts <= MAX_EXTRA_VERTS);
                if (extraverts > MAX_EXTRA_VERTS)
                    extraverts = MAX_EXTRA_VERTS;
            }

            this.LoadLod(ref tokenizer, HIGH_LOD, LodLevel.High, LodLevel.All, this.Optimise, extraverts);
            this.LoadLod(ref tokenizer, MED_LOD, LodLevel.Medium, LodLevel.All, this.Optimise, extraverts);
            this.LoadLod(ref tokenizer, LOW_LOD, LodLevel.Low, LodLevel.All, this.Optimise, extraverts);
            this.LoadLod(ref tokenizer, VLOW_LOD, LodLevel.VeryLow, LodLevel.All, this.Optimise, extraverts);

            if (tokenizer.CheckToken("center", true))
                this.CullSphere = tokenizer.GetVector3(true);

            this.CullRadius = tokenizer.MatchFloat("radius");
            if (tokenizer.CheckToken("box", true))
            {
                this.BoxMin = tokenizer.GetVector3(true);
                this.BoxMax = tokenizer.GetVector3(true);
            }
            tokenizer.MatchToken("}");
            return (true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <returns></returns>
        public override bool SaveAscii(AsciiTokenizer tokenizer)
        {
            tokenizer.PutMultipleDelimeter("\t", this.Level + 1);
            if (this.LodType == LodType.Damaged)
                tokenizer.Put("mesh|damaged", 0);
            else
                tokenizer.Put("mesh", 0);

            tokenizer.PutDelimiter(" {\n");
            this.SaveLodGroup(tokenizer, this.Lods);
            tokenizer.PutDelimiter("}\n");
            tokenizer.PutMultipleDelimeter("\t", this.Level);

            /*
            // Damaged lod group
            if (this.DamagedLods.Count() > 0)
            {
                tokenizer.PutMultipleDelimeter("\t", this.Level + 1);
                tokenizer.Put("mesh|damaged", 0);
                tokenizer.PutDelimiter(" {\n");
                this.SaveLodGroup(tokenizer, this.DamagedLods);
                tokenizer.PutDelimiter("}\n");
                tokenizer.PutMultipleDelimeter("\t", this.Level);
            }
             */

            return (true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<IModel> GetAllModels()
        {
            List<IModel> models = new List<IModel>();
            foreach (Lod lod in this.Lods.Values)
            {
                models.AddRange(lod.Models);
            }
            return models;
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <returns></returns>
        private bool SaveLodGroup(AsciiTokenizer tokenizer, IDictionary<LodLevel, ILod> lods)
        {

            if (this.Optimise == true)
            {
                tokenizer.PutMultipleDelimeter("\t", this.Level + 2);
                tokenizer.Put("optimize ", 0);
                tokenizer.Put(1);
            }
            tokenizer.PutDelimiter("\n");
            tokenizer.PutMultipleDelimeter("\t", this.Level + 2);
            (lods[LodLevel.High] as IEntity).SaveAscii(tokenizer);
            tokenizer.PutDelimiter("\n");
            tokenizer.PutMultipleDelimeter("\t", this.Level + 2);
            (lods[LodLevel.Medium] as IEntity).SaveAscii(tokenizer);
            tokenizer.PutDelimiter("\n");
            tokenizer.PutMultipleDelimeter("\t", this.Level + 2);
            (lods[LodLevel.Low] as IEntity).SaveAscii(tokenizer);
            tokenizer.PutDelimiter("\n");
            tokenizer.PutMultipleDelimeter("\t", this.Level + 2);
            (lods[LodLevel.VeryLow] as IEntity).SaveAscii(tokenizer);
            tokenizer.PutDelimiter("\n");
            tokenizer.PutMultipleDelimeter("\t", this.Level + 2);

            tokenizer.Put("center", 0);
            tokenizer.PutDelimiter(" ");
            tokenizer.Put(this.CullSphere);
            tokenizer.PutDelimiter("\n");
            tokenizer.PutMultipleDelimeter("\t", this.Level + 2);

            tokenizer.Put("radius", 0);
            tokenizer.PutDelimiter(" ");
            tokenizer.Put(this.CullRadius);
            tokenizer.PutDelimiter("\n");
            tokenizer.PutMultipleDelimeter("\t", this.Level + 2);

            tokenizer.Put("box", 0);
            tokenizer.PutDelimiter(" ");
            tokenizer.Put(this.BoxMin);
            tokenizer.PutDelimiter(" ");
            tokenizer.Put(this.BoxMax);
            tokenizer.PutDelimiter("\n");
            tokenizer.PutMultipleDelimeter("\t", this.Level + 1);

            return (true);
        }
        #endregion // Private Methods

        #region Public Static Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="lodlevel"></param>
        /// <returns></returns>
        public static LodLevel LodNameToLevel(String lodlevel)
        {
            LodLevel ll = 0;
            switch (lodlevel)
            {
                case HIGH_LOD:
                    ll = LodLevel.High;
                    break;
                case MED_LOD:
                    ll = LodLevel.Medium;
                    break;
                case LOW_LOD:
                    ll = LodLevel.Low;
                    break;
                case VLOW_LOD:
                    ll = LodLevel.VeryLow;
                    break;
            }
            return ll;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lodlevel"></param>
        /// <returns></returns>
        public static String LodLevelToName(LodLevel lodlevel)
        {
            String ll = String.Empty;
            switch (lodlevel)
            {
                case LodLevel.High:
                    ll = HIGH_LOD;
                    break;
                case LodLevel.Medium:
                    ll = MED_LOD;
                    break;
                case LodLevel.Low:
                    ll = LOW_LOD;
                    break;
                case LodLevel.VeryLow:
                    ll = VLOW_LOD;
                    break;
            }
            return ll;
        }
        #endregion // Pubic Static Methods
    }
}
