﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using RSG.ManagedRage.File;
using RSG.Base.Logging.Universal;

namespace RSG.Rage.Entity
{
    /// <summary>
    /// 
    /// </summary>
    public class ShadingGroup :
        EntityBase
    {
        #region Constants
        /// <summary>
        /// Processor log context.
        /// </summary>
        private static readonly String LOG_CTX = "ShadingGroup";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<Shader>  Shaders { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public Int32 Count { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public Int32 Version { get; private set; }


        /// <summary>
        /// 
        /// </summary>
        public IUniversalLog Log { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public bool LoadSvaFiles{ get;  private set; }

        /// <summary>
        /// 
        /// </summary>
        public string RootDir { get; private set; }
        #endregion // Properties

        #region Contructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public ShadingGroup()
        {
            this.Shaders = new List<Shader>();
            this.Count = 0;
            this.Version = 1;
            this.Log = UniversalLog.StaticLog;
            this.LoadSvaFiles = false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="log"></param>
        public ShadingGroup(IUniversalLog log)
        {
            this.Shaders = new List<Shader>();
            this.Count = 0;
            this.Version = 1;
            this.Log = log;
            this.LoadSvaFiles = false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="log"></param>
        /// <param name="loadSvaFiles"></param>
        /// <param name="rootDir"></param>
        public ShadingGroup(IUniversalLog log, bool loadSvaFiles, string rootDir)
        {
            this.Shaders = new List<Shader>();
            this.Count = 0;
            this.Version = 1;
            this.Log = UniversalLog.StaticLog;
            this.LoadSvaFiles = loadSvaFiles;
            this.RootDir = rootDir;
        }
        #endregion // Contructor

        #region Controller Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rootDir"></param>
        public void LoadSvaFilesFrom(String rootDir)
        {
            this.RootDir = rootDir;
            this.LoadSvaFiles = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <returns></returns>
        public override bool LoadAscii(AsciiTokenizer tokenizer)
        {
            Int32 shaderCount = Convert.ToInt32(tokenizer.MatchInt("Shaders"));
	        tokenizer.GetDelimiter("{");

	        // See which version this type file is
	        if ( Convert.ToBoolean(tokenizer.CheckToken("Vers:", true))) 
            {
                this.Version = Convert.ToInt32(tokenizer.GetInt(true));
	        }
	        else
		        Console.WriteLine("Error: Your entity.type file is too old, needs to be Vers:1/2 with presets.");

	        for (int i=0; i<shaderCount; i++) 
            {
		        String presetName = String.Empty;
                Shader shader = new Shader();
		        int bucket = -1;

		        bool loaded = false;

                if (this.Version == 2)
                {
			        tokenizer.IgnoreToken();		// .mtl / mtlgeo name
			        tokenizer.MatchToken("Template");
			        tokenizer.IgnoreToken();		// usually matches the one below, but not always, and the one below is correct.
			        if (Convert.ToBoolean(tokenizer.CheckToken("Bucket", true)))
				        bucket = Convert.ToInt32(tokenizer.GetInt(true));
                    tokenizer.GetToken(ref presetName, TypeFileParser.MAX_NAMELEN);
			        tokenizer.GetInt(true);				// parameter count, we don't need it
			        tokenizer.MatchToken("{");
                    shader.PresetName = presetName;
				        loaded = shader.LoadAscii(tokenizer);
				        if (bucket != -1)
					        shader.DrawBucket = bucket;
			        tokenizer.MatchToken("}");
		        }
                else if (this.Version == 1)
                {
			        // default.sps 1 VisibleMesh_default_000.sva
			        tokenizer.GetToken(ref presetName, TypeFileParser.MAX_NAMELEN);
                    shader.PresetName = presetName;
                    shader.DrawBucket = tokenizer.GetInt(true);
                    if (shader.DrawBucket == 1) 
                    {
				        String svaFile = String.Empty;
                        tokenizer.GetToken(ref svaFile, TypeFileParser.MAX_NAMELEN);
                        shader.SvaFile = svaFile;
                        if (LoadSvaFiles)
                        {
                            string entityPath = RootDir;
                            string fullSvaPath = Path.Combine(entityPath, svaFile);
                            BaseTokenizer svaTokenizer = TokenUtility.OpenTokenizedFile(fullSvaPath);
                            try
                            {
                                this.Log.MessageCtx(LOG_CTX, "Loading type file {0}.", svaFile);
                                
                                if (!File.Exists(fullSvaPath))
                                    throw new FileNotFoundException(fullSvaPath);
                                shader.LoadAscii(svaTokenizer as AsciiTokenizer);
                            }
                            catch (Exception ex)
                            {
                                this.Log.ExceptionCtx(LOG_CTX, ex, "Exception loading type file {0}.", svaFile);
                            }
                            finally
                            {
                                svaTokenizer.Dispose();
                            }

                           // Not sure hot to handle loading SVA files listed in the type file.  We really need something like RAGEs asset manager.
                        }
			        }
		        }
                (this.Shaders as List<Shader>).Add(shader);
            }
            tokenizer.GetDelimiter("}");

            return (true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <returns></returns>
        public override bool SaveAscii(AsciiTokenizer tokenizer)
        {
            tokenizer.Put("shadinggroup", 0);
            tokenizer.PutDelimiter(" {\n\t");
            tokenizer.Put("shadinggroup", 0);
            tokenizer.PutDelimiter(" {\n\t\t");
            if (this.Count > 0)
            {
                tokenizer.Put("Count", 0);
                tokenizer.PutDelimiter(" ");
                tokenizer.Put(this.Count);
                tokenizer.PutDelimiter("\n\t\t");
            }

            tokenizer.Put("Shaders", 0);
            tokenizer.PutDelimiter(" ");
            tokenizer.Put(this.Shaders.Count());
            tokenizer.PutDelimiter("{\n");
            tokenizer.PutDelimiter("\t\t\t");
            tokenizer.Put("Vers: ", 0);
            tokenizer.Put(this.Version);
            tokenizer.PutDelimiter("\n");
            foreach (Shader shader in this.Shaders)
            {
                tokenizer.PutDelimiter("\t\t\t");
                tokenizer.Put(shader.PresetName, 0);
                tokenizer.PutDelimiter(" ");
                tokenizer.Put(shader.DrawBucket);
                //tokenizer.PutDelimiter(" ");
                tokenizer.Put(shader.SvaFile, 0);
                tokenizer.PutDelimiter("\n");
            }
            tokenizer.PutDelimiter("\t\t}\n");
            tokenizer.PutDelimiter("\t}\n");
            tokenizer.PutDelimiter("}\n");

            return (true);
        }
       
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <returns></returns>
        public void LoadShaderGroup(ref TypeFileCbData cbData)
        {
            AsciiTokenizer tokenizer = cbData.Tokenizer;
            tokenizer.CheckToken("{", true);
            if (tokenizer.CheckIToken("shadinggroup", true))
            {
                tokenizer.GetDelimiter("{");
                if (tokenizer.CheckToken("Count", false))
                {
                    tokenizer.CheckToken("Count", true);
                    this.Count = tokenizer.GetInt(true);
                }

                if (!this.LoadAscii(tokenizer))
                {
                    Console.WriteLine("Error: Failed to load shading group");
                }
                tokenizer.GetDelimiter("}");
            }
            else
            {
                if (tokenizer.CheckToken("Count", false))
                {
                    tokenizer.CheckToken("Count", true);
                    this.Count = tokenizer.GetInt(true);
                }

                if (!this.LoadAscii(tokenizer))
                {
                    Console.WriteLine("Error: Failed to load shading group");
                }
                tokenizer.GetDelimiter("}");
            }
        }
        #endregion 
    }
}
