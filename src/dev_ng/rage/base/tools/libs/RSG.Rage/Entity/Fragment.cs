﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.ManagedRage.File;

namespace RSG.Rage.Entity
{
    /// <summary>
    /// 
    /// </summary>
    public class Fragment :
        EntityBase
    {
        #region Enumerations
        [Flags]
        enum FragFlags
        {
            NEEDS_CACHE_ENTRY_TO_ACTIVATE	= 1 << 0,
		    HAS_ANY_ARTICULATED_PARTS		= 1 << 1,
		    UNUSED							= 1 << 2,
		    CLONE_BOUND_PARTS_IN_CACHE		= 1 << 3,
		    ALLOCATE_TYPE_AND_INCLUDE_FLAGS	= 1 << 4,

		    // Three new flags added for gta
		    // Want to take them across to rage\dev, but need more bits for flags for them to fit
		    FORCE_ARTICULATED_DAMPING		= 1 << 5,	// gta needs this
		    FORCE_LOAD_COMMON_DRAWABLE		= 1 << 6,	// gta needs this
		    FORCE_ALLOCATE_LINK_ATTACHMENTS	= 1 << 7,	// Ensures link attachment matrices are allocated in cache entry
		    BECOME_ROPE						= 1 << 10,  //Some nasty RDR2 hack.
		    IS_USER_MODIFIED				= 1 << 11,   // flag to help the user keep track of fragments they modified. We can find a non-fragment solution if necessary but this was the easiest solution. 
		    DISABLE_ACTIVATION				= 1 << 12,	//Disables activation on instances until the user enables
		    DISABLE_BREAKING				= 1 << 13	//Disables activation on instances until the user enables
        }
        #endregion // Enumerations

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        FragFlags Flags { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public FragmentGroup FragGroup { get; set; }
        #endregion

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        public Fragment()
        {
            this.FragGroup = new FragmentGroup(0);
        }
        #endregion // Constructors

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="bits"></param>
        /// <param name="set"></param>
        void SetFlags(FragFlags bits, bool set)
        {
            if (set)
            {
                this.Flags |= bits;
            }
            else
            {
                this.Flags &= ~bits;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        public void LoadForceCommonDrawable(ref TypeFileCbData cbData)
        {
            this.SetFlags(FragFlags.FORCE_LOAD_COMMON_DRAWABLE, cbData.Tokenizer.GetInt(true) != 0);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        public void LoadDisableBreaking(ref TypeFileCbData cbData)
        {
            this.SetFlags(FragFlags.DISABLE_BREAKING, cbData.Tokenizer.GetInt(true) != 0);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        public void LoadDisableActivation(ref TypeFileCbData cbData)
        {
            this.SetFlags(FragFlags.DISABLE_ACTIVATION, cbData.Tokenizer.GetInt(true) != 0);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <returns></returns>
        public override bool LoadAscii(AsciiTokenizer tokenizer)
        {
            return (true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <returns></returns>
        public override bool SaveAscii(AsciiTokenizer tokenizer)
        {
            tokenizer.Put("fragments", 0);
            tokenizer.PutDelimiter(" {\n");

            if ((this.Flags & FragFlags.FORCE_LOAD_COMMON_DRAWABLE) == FragFlags.FORCE_LOAD_COMMON_DRAWABLE)
            {   
                tokenizer.PutDelimiter("\t");
                tokenizer.Put("forceLoadCommonDrawable ", 0);
                tokenizer.Put(1);
                tokenizer.PutDelimiter("\n");
            }   
 
            if ((this.Flags & FragFlags.DISABLE_BREAKING) == FragFlags.DISABLE_BREAKING)
            {
                tokenizer.PutDelimiter("\t");
                tokenizer.Put("disableBreaking ", 0);
                tokenizer.Put(1);
                tokenizer.PutDelimiter("\n");
            }
          
            if ((this.Flags & FragFlags.DISABLE_ACTIVATION) == FragFlags.DISABLE_ACTIVATION)
            {
                tokenizer.PutDelimiter("\t");
                tokenizer.Put("disableActivation ", 0);
                tokenizer.Put(1);
                tokenizer.PutDelimiter("\n");
            }
            
            this.FragGroup.SaveAscii(tokenizer);
            tokenizer.PutDelimiter("}\n");
            return (true);
        }
        #endregion // Loading Methods
    }
}
