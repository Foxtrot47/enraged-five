﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using RSG.ManagedRage.File;
using RSG.ManagedRage.Math;
using RSG.Base.Math;

namespace RSG.Rage.Entity
{
    public class Bound :
        EntityBase
    {
        #region Constants
        /// <summary>
        /// 
        /// </summary>
        public readonly Int32 MAX_NUM_EXTRA_BOUNDS = 64;
        #endregion // Constants

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public String Name { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        public Quaternionf Rotation { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public Vector3f Position { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public Boolean HasPosition { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public Boolean HasRotation { get; private set; }

        /// <summary>
        /// How deep we are in the hierarchy, required for indentation.
        /// </summary>
        Int32 Level { get; set; }
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        public Bound(Int32 level)
        {
            this.HasPosition = false;
            this.HasRotation = false;
            this.Rotation = new Quaternionf(0.0f, 0.0f, 0.0f, 1.0f);
            this.Position = new Vector3f(0.0f, 0.0f, 0.0f);
            this.Level = level;
        }
        #endregion // Constructors

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cbData"></param>
        public void LoadBound(TypeFileCbData cbData)
        {
            AsciiTokenizer tokenizer = cbData.Tokenizer;
            this.LoadAscii(tokenizer);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <returns></returns>
        public override bool LoadAscii(AsciiTokenizer tokenizer)
        {
            String buff = String.Empty;
	        tokenizer.GetToken(ref buff, TypeFileParser.MAX_NAMELEN);
            this.Name = buff;

	        // If specified, load position
	        if (tokenizer.CheckIToken("<", false))
	        {
                this.HasPosition = true;
		        tokenizer.CheckIToken("<", true);

                this.Position.X = tokenizer.GetFloat(true);
                this.Position.Y = tokenizer.GetFloat(true);
                this.Position.Z = tokenizer.GetFloat(true);

		        tokenizer.CheckIToken(">", true);
	        }

	        // If specified, load orientation
	        if (tokenizer.CheckIToken("<", false))
	        {
                this.HasRotation = true;
		        tokenizer.CheckIToken("<", true);

		        this.Rotation.X = tokenizer.GetFloat(true);
		        this.Rotation.Y = tokenizer.GetFloat(true);
		        this.Rotation.Z = tokenizer.GetFloat(true);
		        this.Rotation.W = tokenizer.GetFloat(true);
		        this.Rotation.Normalise();

		        tokenizer.CheckIToken(">", true);
	        }

	        // If specified, load extra flags
	        if (tokenizer.CheckIToken("[", false))
	        {
		        tokenizer.CheckIToken("[", true);
		        bool done = false;
		        do
		        {
			        String boundFlag = String.Empty;
                    tokenizer.GetToken(ref boundFlag, TypeFileParser.MAX_NAMELEN);

                    Debug.Assert(false, "Attempting to deserialise 'special flags' from a frag bound.  Currently unsupported.");
			        //FRAGMGR->InterpretSpecialFlag(boundFlag);

			        // Skip the comma if applicable.
                    if (tokenizer.CheckIToken(",", false))
			        {
                        tokenizer.CheckIToken(",", true);
			        }

                    if (tokenizer.CheckIToken("]", false))
			        {
                        tokenizer.CheckIToken("]", true);
				        done = true;
			        }
		        } while (!done);


	        }
            return (true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <returns></returns>
        public override bool SaveAscii(AsciiTokenizer tokenizer)
        {
            
            
            tokenizer.PutMultipleDelimeter("\t", this.Level+1);
            tokenizer.Put("bound ", 0);
            tokenizer.Put(this.Name, 0);
            tokenizer.Put(" ", 0);
            if (this.HasPosition)
            {
                tokenizer.Put("< ", 0);
                Vector3 rageVec = new Vector3(this.Position.X, this.Position.Y, this.Position.Z);
                tokenizer.Put(rageVec);
                tokenizer.Put("> ", 0);
            }

            if (this.HasRotation)
            {
                tokenizer.Put("< ", 0);
                Vector4 rageVec = new Vector4(this.Rotation.X, this.Rotation.Y, this.Rotation.Z, this.Rotation.W);
                tokenizer.Put(rageVec);
                tokenizer.Put("> ", 0);
            }
            tokenizer.PutDelimiter("\n");


            return (true);
        }
        #endregion // Controller Methods
    }
}
