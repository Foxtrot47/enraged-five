﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Rage.Entity
{
    public interface IModel
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        String Name { get; }

        /// <summary>
        /// 
        /// </summary>
        Int32 BondIdx { get; }

        /// <summary>
        /// 
        /// </summary>
        Int32 Mask { get; }

        /// <summary>
        /// 
        /// </summary>
        Mesh.Mesh Mesh { get; }

        #endregion // Properties

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="map"></param>
        void RemapMaterialIds(IDictionary<Int32, Int32> map);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        bool LoadMesh(String filename);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        bool SaveMesh(String filename);
        #endregion // Controller Methods
    }
}
