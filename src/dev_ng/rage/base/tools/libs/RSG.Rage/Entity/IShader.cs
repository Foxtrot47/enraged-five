﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Rage.Shader;

namespace RSG.Rage.Entity
{
    public interface IShader
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        String PresetName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        String SvaFile { get; set; }

        /// <summary>
        /// 
        /// </summary>
        Int32 DrawBucket { get; set; }

        /// <summary>
        /// 
        /// </summary>
        ShaderVariables ShaderVars { get; set; }
        #endregion // Properties
    }
}
