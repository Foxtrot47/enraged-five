﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Rage.Mesh;
using RSG.ManagedRage.File;
using RSG.ManagedRage.Math;

namespace RSG.Rage.Entity
{
	public class ClothPart :
		EntityBase
	{
		#region Properties
		/// <summary>
		/// 
		/// </summary>
#warning LPXO:  Should this be a list?  If not, this class is pointless.
		ClothGroup ClothGroup { get; set; }
		#endregion // properties

		#region Constructors
		/// <summary>
		/// 
		/// </summary>
		public ClothPart()
		{
            this.ClothGroup = new ClothGroup();
		}
		#endregion // Constructors

		#region IEntity Methods
		/// <summary>
		/// 
		/// </summary>
		/// <param name="tokenizer"></param>
		/// <returns></returns>
		public override bool LoadAscii(AsciiTokenizer tokenizer)
		{
			bool result = true;
            this.ClothGroup.LoadAscii(tokenizer);
            

            return (result);
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <returns></returns>
        public override bool SaveAscii(AsciiTokenizer tokenizer)
        {
            bool result = true;
            
            this.ClothGroup.SaveAscii(tokenizer);
            
            return (result);
        }
		#endregion // IEntity Methods
	}
}
