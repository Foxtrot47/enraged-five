﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.ManagedRage.File;
using RSG.Base.Logging.Universal;

namespace RSG.Rage.Entity
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class EntityBase
        : IEntity
    {
        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <returns></returns>
        public abstract bool LoadAscii(AsciiTokenizer tokenizer);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <returns></returns>
        public abstract bool SaveAscii(AsciiTokenizer tokenizer);
        #endregion
    }
}
