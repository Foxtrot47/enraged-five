﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.ManagedRage.File;
using RSG.Rage;

namespace RSG.Rage.Entity
{
    #region Enumerations
    /// <summary>
    /// 
    /// </summary>
    public enum ChildType
    {
        Main,
        Damaged,
        None
    }
    #endregion // Enumerations

    public class FragmentChild :
        EntityBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public LodGroupComponents LodGroupComponents { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<Bound> Bounds { get; private set; }

        /// <summary>
        /// How deep we are in the hierarchy, required for indentation.
        /// </summary>
        Int32 Level { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Int32 Index { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public ChildType Type { get; private set; }
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        public FragmentChild(Int32 level, Int32 childIdx, ChildType type)
        {
            this.LodGroupComponents = new LodGroupComponents(level + 1, LodGroupType.Default);
            this.Bounds = new List<Bound>();//(level + 1);
            this.Level = level;
            this.Index = childIdx;
            this.Type = type;
        }
        #endregion // Constructors

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cbData"></param>
        public void BoundLoader(ref TypeFileCbData cbData)
        {
            AsciiTokenizer tokenizer = cbData.Tokenizer;
            while (!tokenizer.CheckToken("}", false))
            {
                tokenizer.CheckToken("bound", true);
                Bound b = new Bound(this.Level+1);
                b.LoadAscii(tokenizer);
                (this.Bounds as List<Bound>).Add(b);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        public void LoadChild(ref TypeFileCbData cbData)
        {
            AsciiTokenizer tokenizer = cbData.Tokenizer;
            this.LoadAscii(tokenizer);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <returns></returns>
        public override bool LoadAscii(AsciiTokenizer tokenizer)
        {
            TypeFileParser parser = new TypeFileParser();
            parser.RegisterLoader("lodgroup", "all", this.LodGroupComponents.MainComponent.LoadLodGroup, true);
            parser.RegisterLoader("bound", "all", this.BoundLoader, true);

            parser.ProcessTypeFile(tokenizer, true);
            return (true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <returns></returns>
        public override bool SaveAscii(AsciiTokenizer tokenizer)
        {
            tokenizer.PutMultipleDelimeter("\t", this.Level);
            if (this.Type == ChildType.Damaged)
                tokenizer.Put("child");
            else
                tokenizer.Put("child");
            tokenizer.PutDelimiter(" {\n");
            tokenizer.PutMultipleDelimeter("\t", this.Level+1);
            tokenizer.Put("bound", 0);
            tokenizer.PutDelimiter(" {\n");
            foreach(Bound b in this.Bounds)
                b.SaveAscii(tokenizer);
            tokenizer.PutMultipleDelimeter("\t", this.Level+1);
            tokenizer.PutDelimiter("}\n");
            if (this.LodGroupComponents.MainComponent.Lods.Count() > 0)
                this.LodGroupComponents.SaveAscii(tokenizer);
            tokenizer.PutMultipleDelimeter("\t", this.Level);
            tokenizer.PutDelimiter("}\n");
            return (true);
        }
        #endregion // Controller Methods
    }
}
