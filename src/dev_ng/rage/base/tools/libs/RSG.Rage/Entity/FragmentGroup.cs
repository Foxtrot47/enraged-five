﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.ManagedRage.File;

namespace RSG.Rage.Entity
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class FragmentGroup
        : EntityBase
    {
        #region Enums
        /// <summary>
        /// 
        /// </summary>
        [Flags]
        public enum StateBits
        {
            FRAG_GROUP_FLAG_DISAPPEARS_WHEN_DEAD = 1 << 0, // When health reaches zero, this group disappears
            FRAG_GROUP_FLAG_MADE_OF_GLASS = 1 << 1, // This group is made out of glass and will shatter when broken
            FRAG_GROUP_FLAG_DAMAGE_WHEN_BROKEN = 1 << 2, // When this group breaks off its parent, it will become damaged
            FRAG_GROUP_FLAG_DOESNT_AFFECT_VEHICLES = 1 << 3  // When colliding with vehicles, the vehicle is treated as infinitely massive
        }
        #endregion // Enums

        #region Properties
        /// <summary>
        /// Wall of supported properties which we load but currently don't actually save.  
        /// </summary>
        public Single Strength { get; private set; }
        public Single ForceTransmissionScaleUp { get; private set; }
        public Single ForceTransmissionScaleDown { get; private set; }
        public Single JointStiffness { get; private set; }
        public Single MinSoftAngle1 { get; private set; }
        public Single MaxSoftAngle1 { get; private set; }
        public Single MaxSoftAngle2 { get; private set; }
        public Single MaxSoftAngle3 { get; private set; }
        public Single RotationSpeed { get; private set; }
        public Single RotationStrength { get; private set; }
        public Single RestoringStrength { get; private set; }
        public Single RestoringMaxTorque { get; private set; }
        public Single LatchStrength { get; private set; }
        public StateBits Flags { get; private set; }
        public Single MinDamageForce { get; private set; }
        public Single DamageHealth { get; private set; }
        public Single WeaponHealth { get; private set; }
        public Single WeaponScale { get; private set; }
        public Boolean MeleeScaleSupplied { get; private set; }
        public Single MeleeScale { get; private set; }
        public Single VehicleScale { get; private set; }
        public Single PedScale { get; private set; }
        public Single RagdollScale { get; private set; }
        public Single ExplosionScale { get; private set; }
        public Single ObjectScale { get; private set; }
        public Single PedInvMassScale { get; private set; }
        public IEnumerable<FragmentGroup> Groups { get; private set; }
        public IEnumerable<FragmentChild> Children { get; private set; }
        public Int32 ChildCount { get; private set; }

        /// <summary>
        /// How deep we are in the hierarchy, required for indentation.
        /// </summary>
        Int32 Level { get; set; }

        /// <summary>
        /// The fragment group name.
        /// </summary>
        public String Name { get; private set; }
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        public FragmentGroup(Int32 childCount, Int32 level = 1)
        {
            this.Groups = new List<FragmentGroup>();
            this.Children = new List<FragmentChild>();
            this.Level = level;
            this.ChildCount = childCount;
        }
        #endregion // Constructors

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        public void LoadGroup(ref TypeFileCbData cbData)
        {
            AsciiTokenizer tokeizer = cbData.Tokenizer;
            this.LoadAscii(tokeizer);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <returns></returns>
        public override bool LoadAscii(AsciiTokenizer tokenizer)
        {
            //need to find an open brace...
            String name = String.Empty;
            tokenizer.GetToken(ref name, TypeFileParser.MAX_NAMELEN);
            this.Name = name;

            tokenizer.CheckIToken("{", true);
            while (true)
            {
                if (tokenizer.CheckIToken("}", false))
                {
                    //all done!
                    break;
                }

                //from here on, we'll look for useful tokens...
                if (tokenizer.CheckIToken("Strength", false))
                {
                    tokenizer.CheckIToken("Strength", true);
                    this.Strength = tokenizer.GetFloat(true);
                }
                else if (tokenizer.CheckIToken("ForceTransmissionScaleUp", false))
                {
                    tokenizer.CheckIToken("ForceTransmissionScaleUp", true);
                    this.ForceTransmissionScaleUp = tokenizer.GetFloat(true);
                }
                else if (tokenizer.CheckIToken("ForceTransmissionScaleDown", false))
                {
                    tokenizer.CheckIToken("ForceTransmissionScaleDown", true);
                    this.ForceTransmissionScaleDown = tokenizer.GetFloat(true);
                }
                else if (tokenizer.CheckIToken("JointStiffness", false))
                {
                    tokenizer.CheckIToken("JointStiffness", true);
                    this.JointStiffness = tokenizer.GetFloat(true);
                }
                else if (tokenizer.CheckIToken("MinSoftAngle1", false))
                {
                    tokenizer.CheckIToken("MinSoftAngle1", true);
                    this.MinSoftAngle1 = tokenizer.GetFloat(true);
                    this.MinSoftAngle1 = Math.Max(-1.0f, this.MinSoftAngle1);
                    this.MinSoftAngle1 = Math.Min(1.0f, this.MinSoftAngle1);
                }
                else if (tokenizer.CheckIToken("MaxSoftAngle1", false))
                {
                    tokenizer.CheckIToken("MaxSoftAngle1", true);
                    this.MaxSoftAngle1 = tokenizer.GetFloat(true);
                    this.MaxSoftAngle1 = Math.Max(-1.0f, this.MaxSoftAngle1);
                    this.MaxSoftAngle1 = Math.Min(1.0f, this.MaxSoftAngle1);
                }
                else if (tokenizer.CheckIToken("MaxSoftAngle2", false))
                {
                    tokenizer.CheckIToken("MaxSoftAngle2", true);
                    this.MaxSoftAngle2 = tokenizer.GetFloat(true);
                    this.MaxSoftAngle2 = Math.Max(-1.0f, this.MaxSoftAngle2);
                    this.MaxSoftAngle2 = Math.Min(1.0f, this.MaxSoftAngle2);
                }
                else if (tokenizer.CheckIToken("MaxSoftAngle3", false))
                {
                    tokenizer.CheckIToken("MaxSoftAngle3", true);
                    this.MaxSoftAngle3 = tokenizer.GetFloat(true);
                    this.MaxSoftAngle3 = Math.Max(-1.0f, this.MaxSoftAngle3);
                    this.MaxSoftAngle3 = Math.Min(1.0f, this.MaxSoftAngle3);
                }
                else if (tokenizer.CheckIToken("RotationSpeed", false))
                {
                    tokenizer.CheckIToken("RotationSpeed", true);
                    this.RotationSpeed = tokenizer.GetFloat(true);
                }
                else if (tokenizer.CheckIToken("RotationStrength", false))
                {
                    tokenizer.CheckIToken("RotationStrength", true);
                    this.RotationStrength = tokenizer.GetFloat(true);
                }
                else if (tokenizer.CheckIToken("RestoringStrength", false))
                {
                    tokenizer.CheckIToken("RestoringStrength", true);
                    this.RestoringStrength = tokenizer.GetFloat(true);
                }
                else if (tokenizer.CheckIToken("RestoringMaxTorque", false))
                {
                    tokenizer.CheckIToken("RestoringMaxTorque", true);
                    this.RestoringMaxTorque = tokenizer.GetFloat(true);
                }
                else if (tokenizer.CheckIToken("LatchStrength", false))
                {
                    tokenizer.CheckIToken("LatchStrength", true);
                    this.LatchStrength = tokenizer.GetFloat(true);
                }
                else if (tokenizer.CheckIToken("deathEvents", false))
                {
                    tokenizer.CheckIToken("deathEvents", true);
                    tokenizer.CheckToken("all", true);
                    tokenizer.CheckToken("{", true);
                    //PARSER.LoadObjectPtr(asciiTok.GetStream(), (evtSet*&)group->m_DeathEventset);
                    tokenizer.CheckToken("}", true);

                    //Assert(group->m_DeathEventPlayer == NULL);

                    //group->m_DeathEventPlayer = rage_new fragCollisionEventPlayer;
                    //group->m_DeathEventPlayer->CreateParameterList();
                    //group->m_DeathEventPlayer->SetSet(*group->m_DeathEventset);
                }
                else if (tokenizer.CheckIToken("group", false))
                {
                    tokenizer.CheckIToken("group", true);

                    FragmentGroup fg = new FragmentGroup(this.ChildCount, this.Level + 1);
                    fg.LoadAscii(tokenizer);
                    (this.Groups as List<FragmentGroup>).Add(fg);
                }
                else if (tokenizer.CheckIToken("JointLatched", false))
                {
                    tokenizer.CheckIToken("JointLatched", true);
                    tokenizer.GetInt(true); // jointLatched = asciiTok.GetInt() != 0;
                }
                else if (tokenizer.CheckIToken("child", false))
                {
                    this.ChildCount++;
                    tokenizer.CheckIToken("child", true);
                    FragmentChild fc = new FragmentChild(this.Level + 1, this.ChildCount, ChildType.Main);
                    fc.LoadAscii(tokenizer);
                    (this.Children as List<FragmentChild>).Add(fc);
                }
                else if (tokenizer.CheckIToken("damagedChild", false))
                {
                    String childName = String.Empty;
                    tokenizer.CheckIToken("damagedChild", true);
                    tokenizer.GetToken(ref childName, TypeFileParser.MAX_NAMELEN);
                    FragmentChild fc = new FragmentChild(this.Level + 1, this.ChildCount, ChildType.Damaged);
                    fc.LoadAscii(tokenizer);
                    (this.Children as List<FragmentChild>).Add(fc);
                }
                else if (tokenizer.CheckIToken("DisappearsWhenDead", false))
                {
                    tokenizer.CheckIToken("DisappearsWhenDead", true);
                    this.Flags |= StateBits.FRAG_GROUP_FLAG_DISAPPEARS_WHEN_DEAD;
                }
                else if (tokenizer.CheckIToken("MadeOfGlass", false))
                {
                    tokenizer.CheckIToken("MadeOfGlass", true);
                    this.Flags |= StateBits.FRAG_GROUP_FLAG_MADE_OF_GLASS;
                }
                else if (tokenizer.CheckIToken("DamageWhenBroken", false))
                {
                    tokenizer.CheckIToken("DamageWhenBroken", true);
                    this.Flags |= StateBits.FRAG_GROUP_FLAG_DAMAGE_WHEN_BROKEN;
                }
                else if (tokenizer.CheckIToken("DoesntAffectVehicles", false))
                {
                    tokenizer.CheckIToken("DoesntAffectVehicles", true);
                    this.Flags |= StateBits.FRAG_GROUP_FLAG_DOESNT_AFFECT_VEHICLES;
                }
                else if (tokenizer.CheckIToken("GlassType", false))
                {
                    tokenizer.CheckIToken("GlassType", true);
                    String glassTypeName = String.Empty;
                    tokenizer.GetToken(ref glassTypeName, TypeFileParser.MAX_NAMELEN);
                }
                else if (tokenizer.CheckIToken("minDamageForce", false))
                {
                    tokenizer.CheckIToken("minDamageForce", true);
                    this.MinDamageForce = tokenizer.GetFloat(true);
                }
                else if (tokenizer.CheckIToken("damageHealth", false))
                {
                    tokenizer.CheckIToken("damageHealth", true);

                    this.DamageHealth = tokenizer.GetFloat(true);
                }
                else if (tokenizer.CheckIToken("weaponHealth", false))
                {
                    tokenizer.CheckIToken("weaponHealth", true);

                    this.WeaponHealth = tokenizer.GetFloat(true);
                }
                else if (tokenizer.CheckIToken("weaponScale", false))
                {
                    tokenizer.CheckIToken("weaponScale", true);

                    this.WeaponScale = tokenizer.GetFloat(true);
                }
                else if (tokenizer.CheckIToken("meleeScale", false))
                {
                    tokenizer.CheckIToken("meleeScale", true);

                    this.MeleeScaleSupplied = true;
                    this.MeleeScale = tokenizer.GetFloat(true);
                }
                else if (tokenizer.CheckIToken("vehicleScale", false))
                {
                    tokenizer.CheckIToken("vehicleScale", true);

                    this.VehicleScale = tokenizer.GetFloat(true);
                }
                else if (tokenizer.CheckIToken("pedScale", false))
                {
                    tokenizer.CheckIToken("pedScale", true);

                    this.PedScale = tokenizer.GetFloat(true);
                }
                else if (tokenizer.CheckIToken("ragdollScale", false))
                {
                    tokenizer.CheckIToken("ragdollScale", true);

                    this.RagdollScale = tokenizer.GetFloat(true);
                }
                else if (tokenizer.CheckIToken("explosionScale", false))
                {
                    tokenizer.CheckIToken("explosionScale", true);

                    this.ExplosionScale = tokenizer.GetFloat(true);
                }
                else if (tokenizer.CheckIToken("objectScale", false))
                {
                    tokenizer.CheckIToken("objectScale", true);

                    this.ObjectScale = tokenizer.GetFloat(true);
                }
                else if (tokenizer.CheckIToken("pedInvMassScale", false))
                {
                    tokenizer.CheckIToken("pedInvMassScale", true);

                    this.PedInvMassScale = tokenizer.GetFloat(true);
                }
                else if (tokenizer.CheckIToken("preset", false))
                {
                    tokenizer.CheckIToken("preset", true);

                    String presetName = String.Empty;
                    tokenizer.GetToken(ref presetName, TypeFileParser.MAX_NAMELEN);

                    //if (fragTune::IsInstantiated())
                    //{
                    //    preset = FRAGTUNE->FindPresetByName(presetName);
                    //}
                }
                else
                {
                    String tokenBuffer = String.Empty;
                    tokenizer.GetToken(ref tokenBuffer, TypeFileParser.MAX_NAMELEN);
                    Console.WriteLine("Found unknown token '{0}' in group.  Ignoring.", tokenBuffer);
                }
            }
            //skip the ending brace...
            tokenizer.CheckIToken("}", true);

            return (true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <returns></returns>
        public override bool SaveAscii(AsciiTokenizer tokenizer)
        {
            tokenizer.PutMultipleDelimeter("\t", this.Level);
            tokenizer.Put("group ", 0);
            tokenizer.Put(this.Name, 0);
            tokenizer.PutDelimiter(" {\n");
            foreach (FragmentChild child in this.Children)
            {
                child.SaveAscii(tokenizer);
            }
            foreach (FragmentGroup group in this.Groups)
            {
                group.SaveAscii(tokenizer);
            }
            tokenizer.PutMultipleDelimeter("\t", this.Level);
            tokenizer.PutDelimiter("}\n");
            return (true);
        }
        #endregion // Controller Methods

    }
}
