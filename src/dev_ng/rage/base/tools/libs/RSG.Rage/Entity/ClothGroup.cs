﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.ManagedRage.File;
using RSG.ManagedRage.Math;

namespace RSG.Rage.Entity
{
	public class ClothGroup :
		GroupBase
	{
		#region Properties
		/// <summary>
		/// 
		/// </summary>
		public Vector3 CullSphere { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Single CullRadius { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public Vector3 BoxMin { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public Vector3 BoxMax { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public Int32 BoneCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Vector3 ClothOffset { get; set; }
		#endregion // Properties

		#region Constructors
		/// <summary>
		/// 
		/// </summary>
		/// <param name="level"></param>
		public ClothGroup()
		{
			this.Lods = new Dictionary<LodLevel, ILod>();
			this.CullSphere = new Vector3(0.0f, 0.0f, 0.0f);
            this.ClothOffset = new Vector3(0.0f, 0.0f, 0.0f);
            this.CullRadius = 0.0f;
			this.BoxMin = new Vector3(0.0f, 0.0f, 0.0f);
			this.BoxMax = new Vector3(0.0f, 0.0f, 0.0f);
            //this.Level = 0;
		}
		#endregion // Constructors

		#region Private Methods
		/// <summary>
		/// 
		/// </summary>
		/// <param name="tokenizer"></param>
		/// <param name="match"></param>
		/// <param name="level"></param>
		/// <param name="lodMask"></param>
		/// <param name="extraverts"></param>
		private void LoadLod(ref AsciiTokenizer tokenizer, String match, LodLevel level, LodLevel lodMask, Int32 extraverts)
		{
			// Are we suppose to load this LOD?
			if ((lodMask & level) == level)
			{
				// Yeah, load it here.
				String buf = String.Empty;
				tokenizer.MatchToken(match);
				this.Lods[level] = new ClothLod(level, LodGroup.LodLevelToName(level), 0);
                tokenizer.GetToken(ref buf, TypeFileParser.MAX_NAMELEN);
				if (buf!="none")
				{
                    // It seems as though only one model is ever supported (deserialisation code in ragebuilder only reads a single mesh)
                    if (1 != int.Parse(buf))
                        throw new System.IO.FileLoadException("Only one cloth mesh allowed per LOD level.", tokenizer.Filename());

					tokenizer.GetToken(ref buf, TypeFileParser.MAX_NAMELEN);
					
					(this.Lods[level].Models as List<IModel>).Add(new Model(buf, 0, 0));

					Int32 nModel = tokenizer.GetInt(true); // bone index
				}
				(this.Lods[level] as ClothLod).SwitchDistanceDown = tokenizer.GetFloat(true);
				(this.Lods[level] as ClothLod).SwitchDistanceUp = tokenizer.GetFloat(true);
			}
		}
		#endregion // Private Methods

		#region IEntity Methods
		/// <summary>
		/// 
		/// </summary>
		/// <param name="tokenizer"></param>
		/// <returns></returns>
		public override bool LoadAscii(AsciiTokenizer tokenizer)
		{
			tokenizer.CheckToken("{", true);
			tokenizer.CheckToken("mesh", true);

			if (tokenizer.CheckToken("{", true))
			{
                if (tokenizer.CheckToken("offset", true))
                    this.ClothOffset = tokenizer.GetVector3(true);

#warning Extra verts data needs to come from the main LOD group.  Necessary?
				Int32 extraverts = 0;
				this.LoadLod(ref tokenizer, LodGroup.HIGH_LOD, LodLevel.High, LodLevel.All, extraverts);
				this.LoadLod(ref tokenizer, LodGroup.MED_LOD, LodLevel.Medium, LodLevel.All, extraverts);
				this.LoadLod(ref tokenizer, LodGroup.LOW_LOD, LodLevel.Low, LodLevel.All, extraverts);
				this.LoadLod(ref tokenizer, LodGroup.VLOW_LOD, LodLevel.VeryLow, LodLevel.All, extraverts);

				if (tokenizer.CheckToken("center", true))
					this.CullSphere = tokenizer.GetVector3(true);

                if (tokenizer.CheckToken("radius", true))
				    this.CullRadius = tokenizer.GetFloat(true);

				if (tokenizer.CheckToken("box", true))
				{
					this.BoxMin = tokenizer.GetVector3(true);
					this.BoxMax = tokenizer.GetVector3(true);
				}
			}  
			tokenizer.CheckToken("}", true);

			if (tokenizer.CheckToken("boneCount", true))
				this.BoneCount = tokenizer.GetInt(true);
            tokenizer.CheckToken("}", true);

			return (true);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="tokenizer"></param>
		/// <returns></returns>
		public override bool SaveAscii(AsciiTokenizer tokenizer)
		{
            tokenizer.StartLine();
            tokenizer.PutDelimiter("ClothPiece\n");
            tokenizer.StartBlock();
            tokenizer.PutIndented("mesh\n");
            
            tokenizer.StartBlock();
            
            tokenizer.PutIndented("offset");
            tokenizer.PutDelimiter(" ");
            tokenizer.Put(this.ClothOffset);
            tokenizer.PutDelimiter("\n");
            
            (this.Lods[LodLevel.High] as EntityBase).SaveAscii(tokenizer);
			tokenizer.PutDelimiter("\n");
            (this.Lods[LodLevel.Medium] as EntityBase).SaveAscii(tokenizer);
			tokenizer.PutDelimiter("\n");
            (this.Lods[LodLevel.Low] as EntityBase).SaveAscii(tokenizer);
			tokenizer.PutDelimiter("\n");
            (this.Lods[LodLevel.VeryLow] as EntityBase).SaveAscii(tokenizer);
			tokenizer.PutDelimiter("\n");
			
            tokenizer.PutIndented("center");
			tokenizer.PutDelimiter(" ");
			tokenizer.Put(this.CullSphere);
			tokenizer.PutDelimiter("\n");
			
            tokenizer.PutIndented("radius");
            tokenizer.PutDelimiter(" ");
			tokenizer.Put(this.CullRadius);
			tokenizer.PutDelimiter("\n");
			
            tokenizer.PutIndented("box");
            tokenizer.PutDelimiter(" ");
			tokenizer.Put(this.BoxMin);
			tokenizer.PutDelimiter(" ");
			tokenizer.Put(this.BoxMax);
			tokenizer.PutDelimiter("\n");
            tokenizer.EndBlock();
            tokenizer.EndBlock();

			return (true);
		}
		#endregion // Controller Methods
	}
}
