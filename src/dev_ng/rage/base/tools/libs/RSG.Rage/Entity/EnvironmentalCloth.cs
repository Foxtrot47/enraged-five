﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.ManagedRage.File;

namespace RSG.Rage.Entity
{
    /// <summary>
    /// 
    /// </summary>
    public class EnvironmentalCloth :
        EntityBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        IEnumerable<ClothPart> ClothParts { get; set; }
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        public EnvironmentalCloth()
        {
            this.ClothParts = new List<ClothPart>();
        }
        #endregion // Constructors

        #region accessors
        public bool FoundClothPaths()
        {
            return ClothParts.Any();
        }
        #endregion

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        public void LoadEnvCloth(ref TypeFileCbData cbData)
        {
            AsciiTokenizer tokeizer = cbData.Tokenizer;
            this.LoadAscii(tokeizer);
        }

        /// <summary>
		/// 
		/// </summary>
		/// <param name="tokenizer"></param>
		/// <returns></returns>
        public override bool LoadAscii(AsciiTokenizer tokenizer)
        {
            bool result = true;
            ClothPart clothPart = new ClothPart();
            clothPart.LoadAscii(tokenizer);
            (ClothParts as List<ClothPart>).Add(clothPart);
            
            return (result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <returns></returns>
        public override bool SaveAscii(AsciiTokenizer tokenizer)
        {
            bool result = true;
            tokenizer.PutDelimiter("cloth\n");
            
            tokenizer.StartBlock();
            tokenizer.StartLine();
            foreach (ClothPart cp in ClothParts)
            {
                cp.SaveAscii(tokenizer);
            }

            tokenizer.EndBlock();
            return (result);
        }
        #endregion // Controller Methods
    }
}
