﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.ManagedRage.File;

namespace RSG.Rage
{
    public static class Extensions
    {
        /// <summary>
        /// Convenience extension to allow a delimter to be added count number of times.
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <param name="delim"></param>
        /// <param name="count"></param>
        public static void PutMultipleDelimeter(this AsciiTokenizer tokenizer, String delim, Int32 count)
        {
            for(int i=0; i<count; i++)
                tokenizer.PutDelimiter(delim);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <param name="str"></param>
        public static void Put(this AsciiTokenizer tokenizer, String str)
        {
            tokenizer.Put(str, 0);
        }

        /// <summary>
        /// Convenience extension to insert indention as part of the put.
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <param name="str"></param>
        public static void PutIndented(this AsciiTokenizer tokenizer, String str)
        {
            tokenizer.StartLine();
            tokenizer.Put(str);
        }
    }
}
