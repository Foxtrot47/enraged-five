﻿//---------------------------------------------------------------------------------------------
// <copyright file="IResourceReader.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Rage.Data
{
    using System;
    using RSG.Base;

    /// <summary>
    /// RAGE Resource reader interface; this will allow us to support new 
    /// Resource Versions in future.
    /// </summary>
    public interface IResourceReader
    {
        /// <summary>
        /// Magic identifier bytes (typically 4 bytes).
        /// </summary>
        Byte[] Magic { get; }
        
        /// <summary>
        /// Platform the resource is for.
        /// </summary>
        RSG.Platform.Platform Platform { get; }

        /// <summary>
        /// Resource-specific version (not resource-format version).
        /// </summary>
        int Version { get; }
    }

} // RSG.Rage.Data namespace
