﻿//---------------------------------------------------------------------------------------------
// <copyright file="RSC7.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Rage.Data
{
    using System;
    using System.Collections.Generic;
    using RSG.Platform;

    /// <summary>
    /// Rage RSC7 constants.
    /// </summary>
    public static class RSC7
    {
        /// <summary>
        /// Magic number constant (big-endian platform resource).
        /// </summary>
        public static readonly Byte[] MagicBigEndian = new Byte[] 
        { 
            Convert.ToByte('R'), 
            Convert.ToByte('S'), 
            Convert.ToByte('C'), 
            Convert.ToByte('7'), 
        };

        /// <summary>
        /// Magic number constant (little-endian platform resource).
        /// </summary>
        public static readonly Byte[] MagicLittleEndian = new Byte[] 
        { 
            Convert.ToByte('7'), 
            Convert.ToByte('C'), 
            Convert.ToByte('S'), 
            Convert.ToByte('R'), 
        };

        /// <summary>
        /// RSC7 is a chunked format; this is the maximum number of chunks of each size.
        /// </summary>
        public const int MaximumChunks = 128;

        /// <summary>
        /// Resource leaf sizes.
        /// </summary>
        public const long RscVirtualLeafSize_Default = 8192;
        public const long RscPhysicalLeafSize_Default = 8192;
        public const long RscVirtualLeafSize_PS3 = 4096;
        public const long RscPhysicalLeafSize_PS3 = 5504;

        /// <summary>
        /// Resource leaf sizes per-platform.
        /// </summary>
        public static readonly IDictionary<Platform, ResourceLeafSize> RscLeafSizes =
            new Dictionary<Platform, ResourceLeafSize>()
            {
                { Platform.PS4, new ResourceLeafSize(RscVirtualLeafSize_Default, RscPhysicalLeafSize_Default) },
                { Platform.PS3, new ResourceLeafSize(RscVirtualLeafSize_PS3, RscPhysicalLeafSize_PS3) },
                { Platform.Win32, new ResourceLeafSize(RscVirtualLeafSize_Default, RscPhysicalLeafSize_Default) },
                { Platform.Win64, new ResourceLeafSize(RscVirtualLeafSize_Default, RscPhysicalLeafSize_Default) },
                { Platform.Xbox360, new ResourceLeafSize(RscVirtualLeafSize_Default, RscPhysicalLeafSize_Default) },
                { Platform.XboxOne, new ResourceLeafSize(RscVirtualLeafSize_Default, RscPhysicalLeafSize_Default) },
            };
    }

} // RSG.Rage.Data namespace
