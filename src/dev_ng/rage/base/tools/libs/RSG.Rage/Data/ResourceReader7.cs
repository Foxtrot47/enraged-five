﻿//---------------------------------------------------------------------------------------------
// <copyright file="ResourceReader7.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Rage.Data
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using RSG.Base;
    using RSG.Platform;

    /// <summary>
    /// RAGE Resource Version 7 reader class.
    /// </summary>
    public class ResourceReader7 : 
        IResourceReader
    {
        #region Properties
        /// <summary>
        /// Magic identifier bytes (typically 4 bytes).
        /// </summary>
        public Byte[] Magic
        {
            get;
            private set;
        }

        /// <summary>
        /// Platform the resource is for.
        /// </summary>
        public Platform Platform
        {
            get;
            private set;
        }

        /// <summary>
        /// Resource-specific version (not resource-format version).
        /// </summary>
        public int Version 
        { 
            get; 
            private set;
        }
        #endregion // Properties
        
        #region Member Data
        /// <summary>
        /// 
        /// </summary>
        private Byte[] _chunkedBuffer;

        /// <summary>
        /// 
        /// </summary>
        private Resource7InfoSizes _virtual;

        /// <summary>
        /// 
        /// </summary>
        private Resource7InfoSizes _physical;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Load a resource object from a stream.
        /// </summary>
        /// <param name="input"></param>
        public ResourceReader7(Stream input, Platform platform)
        {
            if (null == input)
                throw (new ArgumentNullException("input"));
            
            this.Platform = platform;
            Init(input);
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Load a resource object from a file.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static IResourceReader Load(String filename, Platform platform)
        {
            using (FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read))
            {
                return (new ResourceReader7(fs, platform));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Byte[] GetResourceData()
        {
            return (this._chunkedBuffer);
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Initialise our object from the Stream.
        /// </summary>
        /// <param name="input"></param>
        private void Init(Stream input)
        {
            this._chunkedBuffer = new Byte[0];
            ReadResourceHeader(input);
            ReadResourceChunks(input);
        }

        /// <summary>
        /// Read resource header information (datResourceHeader).
        /// </summary>
        /// <param name="input"></param>
        private void ReadResourceHeader(Stream input)
        {
            // Read the first-four bytes magic number; that determines our endian-ness
            // and affects all future reads as we're on a little-endian platform.
            int magicLength = RSC7.MagicBigEndian.Length;
            Byte[] magic = new Byte[magicLength];
            int magicCount = input.Read(magic, 0, magicLength);

            if (magicLength != magicCount)
                throw (new ResourceReadException("Failed to read magic header bytes!"));

            Endianness endian = this.Platform.Endianness();
            if (ArrayComparer.Equals(RSC7.MagicBigEndian, magic) &&
                (Endianness.BigEndian != endian))
            {
                throw (new ResourceReadException("Invalid platform specified; Big-Endian platform but Little-Endian resource."));
            }
            else if (ArrayComparer.Equals(RSC7.MagicLittleEndian, magic) &&
                (Endianness.LittleEndian != endian))
            {
                throw (new ResourceReadException("Invalid platform specified; Little-Endian platform but Big-Endian resource."));
            }
            else
            {
                throw (new ResourceReadException(String.Format("Invalid magic header bytes ({0}).  Not RSC7.", magic.ToString())));
            }
        
            // Next 32-bits contains signed-resource version (not resource format version).
            Byte[] version = new Byte[4];
            int versionCount = input.Read(version, 0, 4);
            if (Endianness.BigEndian == endian)
            {
                Array.Reverse(version);
                this.Version = BitConverter.ToInt32(version, 0);
            }
            else
                this.Version = BitConverter.ToInt32(version, 0);

            // Next we have the datResourceInfo structure which describes how the resource
            // is chunked.
            Byte[] buffer = new Byte[4];
            if (4 != input.Read(buffer, 0, 4))
                throw (new ResourceReadException("Failed to read Virtual datResourceInfo structure."));
            this._virtual = new Resource7InfoSizes(BitConverter.ToUInt32(buffer, 0));
            if (4 != input.Read(buffer, 0, 4))
                throw (new ResourceReadException("Failed to read Physical datResourceInfo structure."));
            this._physical = new Resource7InfoSizes(BitConverter.ToUInt32(buffer, 0));
        }

        /// <summary>
        /// Read resource chunk information; this assemblies the _chunkedBuffer data.
        /// </summary>
        /// <param name="input"></param>
        private void ReadResourceChunks(Stream input)
        {

        }
        #endregion // Private Methods
    }

} // RSG.Rage.Data namespace
