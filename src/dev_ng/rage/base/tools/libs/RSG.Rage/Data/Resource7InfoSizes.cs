﻿//---------------------------------------------------------------------------------------------
// <copyright file="Resource7ChunkSizes.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Rage.Data
{
    using System;
    using System.Collections.Generic;
    using RSG.Platform;

    /// <summary>
    /// 
    /// </summary>
    [Flags]
    enum Resource7InfoSizesFlags
    {
        None,

        /// <summary>
        /// Set if there is a chunk one half the base chunk size afterward.
        /// </summary>
        HasTail2,

        /// <summary>
        /// Set if there is a chunk one quarter the base chunk size afterward.
        /// </summary>
        HasTail4,

        /// <summary>
        /// Set if there is a chunk one eighth the base chunk size afterward.
        /// </summary>
        HasTail8,

        /// <summary>
        /// Set if there is a chunk one sixteenth the base chunk size afterward.
        /// </summary>
        HasTail16
    }

    /// <summary>
    /// Wrapper around ::rage::datResourceInfo Sizes bitfield structure.
    /// </summary>
    struct Resource7InfoSizes
    {
        #region Properties
        /// <summary>
        /// How far left to shift leaf size to get base chunk size (up to 512k).
        /// </summary>
        public byte LeafShift { get; private set; }

        /// <summary>
        /// How many chunks that are 16* the base chunk size.
        /// </summary>
        public ushort Head16Count { get; private set; }

        /// <summary>
        /// How many chunks that are 8* the base chunk size.
        /// </summary>
        public ushort Head8Count { get; private set; }

        /// <summary>
        /// How many chunks that are 4* the base chunk size.
        /// </summary>
        public ushort Head4Count { get; private set; }

        /// <summary>
        /// How many chunks that are 2* the base chunk size.
        /// </summary>
        public ushort Head2Count { get; private set; }

        /// <summary>
        /// How many chunks that are the base chunk size
        /// </summary>
        public ushort BaseCount { get; private set; }

        /// <summary>
        /// Size flags about chunk tail data.
        /// </summary>
        public Resource7InfoSizesFlags Flags { get; private set; }
        
        /// <summary>
        /// Version bits are split between virtual and physical.
        /// </summary>
        public ushort Version { get; private set; }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <see cref="$(RAGE_DIR)\src\data\resourceheader.h" />
        /// <param name="size"></param>
        public Resource7InfoSizes(UInt32 size)
            : this()
        {
            this.LeafShift = (byte)((size & 0xF0000000) >> 24);
            this.Head16Count = (ushort)((size & 0x08000000) >> 22);
            this.Head8Count = (ushort)((size & 0x06000000) > 21 ? 1 : 0);
            this.Head4Count = (ushort)((size & 0x01E00000) >> 19);
            this.Head2Count = (ushort)((size & 0x001F8000) >> 15);
            this.BaseCount = (ushort)((size & 0x00007F00) >> 8);
            this.Flags |= (size & 0x00000080) == 0x00000080 ? Resource7InfoSizesFlags.HasTail2 : Resource7InfoSizesFlags.None;
            this.Flags |= (size & 0x00000040) == 0x00000040 ? Resource7InfoSizesFlags.HasTail4 : Resource7InfoSizesFlags.None;
            this.Flags |= (size & 0x00000020) == 0x00000020 ? Resource7InfoSizesFlags.HasTail8 : Resource7InfoSizesFlags.None;
            this.Flags |= (size & 0x00000010) == 0x00000010 ? Resource7InfoSizesFlags.HasTail16 : Resource7InfoSizesFlags.None;
            this.Version = (ushort)(size & 0x0000000F);
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public long GetCount()
        {
            long tailCount = 0;
            if (this.Flags.HasFlag(Resource7InfoSizesFlags.HasTail2))
                ++tailCount;
            if (this.Flags.HasFlag(Resource7InfoSizesFlags.HasTail4))
                ++tailCount;
            if (this.Flags.HasFlag(Resource7InfoSizesFlags.HasTail8))
                ++tailCount;
            if (this.Flags.HasFlag(Resource7InfoSizesFlags.HasTail16))
                ++tailCount;
            return (Head16Count + Head8Count + Head4Count + Head2Count + BaseCount + tailCount);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="leafSize"></param>
        /// <returns></returns>
        public long GetSize(UInt32 leafSize)
        {
            long actualLeafSize = leafSize << this.LeafShift;
            long tailSize = 0;
            if (this.Flags.HasFlag(Resource7InfoSizesFlags.HasTail2))
                tailSize += actualLeafSize >> 1;
            if (this.Flags.HasFlag(Resource7InfoSizesFlags.HasTail4))
                tailSize += actualLeafSize >> 2;
            if (this.Flags.HasFlag(Resource7InfoSizesFlags.HasTail8))
                tailSize += actualLeafSize >> 3;
            if (this.Flags.HasFlag(Resource7InfoSizesFlags.HasTail16))
                tailSize += actualLeafSize >> 4;

            return (((Head16Count << 4) + (Head8Count << 3) + (Head4Count << 2) + (Head2Count << 1) + BaseCount) * leafSize) + tailSize;		
        }
        #endregion // Controller Methods
    }

} // RSG.Rage.Data namespace
