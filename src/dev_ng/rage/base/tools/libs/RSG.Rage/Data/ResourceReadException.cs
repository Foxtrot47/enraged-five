﻿//---------------------------------------------------------------------------------------------
// <copyright file="ResourceReadException.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Rage.Data
{
    using System;

    /// <summary>
    /// 
    /// </summary>
    public class ResourceReadException : Exception
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="message"></param>
        public ResourceReadException(String message)
            : base(message)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="message"></param>
        /// <param name="inner"></param>
        public ResourceReadException(String message, Exception inner)
            : base(message, inner)
        {
        }
        #endregion // Constructor(s)
    }

} // RSG.Rage.Data namespace
