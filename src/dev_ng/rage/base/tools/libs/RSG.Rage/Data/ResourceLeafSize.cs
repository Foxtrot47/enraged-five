﻿//---------------------------------------------------------------------------------------------
// <copyright file="ResourceLeafSize.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Rage.Data
{

    /// <summary>
    /// Resource leaf size structure; we define data that includes this per platform.
    /// </summary>
    public struct ResourceLeafSize
    {
        long Virtual;
        long Physical;

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="virt"></param>
        /// <param name="phys"></param>
        public ResourceLeafSize(long virt, long phys)
        {
            this.Virtual = virt;
            this.Physical = phys;
        }
        #endregion // Constructor(s)
    }

} // RSG.Rage.Data namespace
