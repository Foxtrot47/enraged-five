﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using RSG.Rage.Entity;
using RSG.ManagedRage.File;
using RSG.Base.Logging.Universal;

namespace RSG.Rage.TypeFile
{
    /// <summary>
    /// 
    /// </summary>
    public class FragmentType :
        BaseType
    {
        #region Constants
        /// <summary>
        /// Processor log context.
        /// </summary>
        private static readonly String LOG_CTX =  "FragmentType";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public String Skeleton { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public String SkeletonType { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public ShadingGroup ShadingGroup { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Fragment Fragment { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public LodGroupComponents LodGroupComponents { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public LodGroupComponents LodClothGroupComponents { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public EnvironmentalCloth EnvCloth { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool HasCloth { get; set; }
        #endregion // Properties

        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        public FragmentType(IUniversalLog log)
            : base(log)
        {
            this.ShadingGroup = new ShadingGroup(log);
            this.LodGroupComponents = new LodGroupComponents(0, LodGroupType.Default);
            this.LodClothGroupComponents = new LodGroupComponents(0, LodGroupType.Cloth);
            this.Fragment = new Fragment();
            this.EnvCloth = new EnvironmentalCloth();
#warning LPXO: Set this based on whether the source has any cloth data.
            this.HasCloth = false;
        }
        public FragmentType(IUniversalLog log, bool loadSvas, string RootDir)
            : base(log)
        {
            this.ShadingGroup = new ShadingGroup(log, loadSvas, RootDir);
            this.LodGroupComponents = new LodGroupComponents(0, LodGroupType.Default);
            this.LodClothGroupComponents = new LodGroupComponents(0, LodGroupType.Cloth);
            this.Fragment = new Fragment();
            this.EnvCloth = new EnvironmentalCloth();
#warning LPXO: Set this based on whether the source has any cloth data.
            this.HasCloth = false;
        }
        #endregion // Constructor

        #region Controller Methods
        /*
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fragType"></param>
        /// <param name="lodLevel"></param>
        private void SetChildLodsFromFragType(FragmentGroup fragmentGroup, LodLevel lodLevel)
        {

        }
        */

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fragType"></param>
        /// <param name="lodLevel"></param>
        public void SetLodFromFragType(FragmentType fragType, LodLevel lodLevel, String directory)
        {
            // Set the model on the main LOD group
            this.LodGroupComponents.MainComponent.Lods[lodLevel] = fragType.LodGroupComponents.MainComponent.Lods[LodLevel.High];
            this.LodGroupComponents.MainComponent.Lods[lodLevel].LevelName = LodGroup.LodLevelToName(lodLevel);

            Dictionary<Int32, Int32> ShaderMap = new Dictionary<Int32, Int32>();
            int oldIdx = 0;
            foreach (Entity.Shader shader in fragType.ShadingGroup.Shaders)
            {
                List<Entity.Shader> res = this.ShadingGroup.Shaders.Where(s => s.Equals(shader)).ToList();
                if (res.Any())
                {
                    ShaderMap.Add(oldIdx, (this.ShadingGroup.Shaders as List<Entity.Shader>).IndexOf(res.First()));
                }
                else
                {
                    ShaderMap.Add(oldIdx, this.ShadingGroup.Shaders.Count());
                    (this.ShadingGroup.Shaders as List<Entity.Shader>).Add(shader);
                }
                oldIdx++;
            }

            IDictionary<String, LodGroup> lodGroups = new Dictionary<String, LodGroup>();
            FragmentType.GetLodGroupsByIdx(this.Fragment.FragGroup, ref lodGroups);

            // Collect all lod groups from the entity (some can be nested in the fragment hiearchy).
            IDictionary<String, LodGroup> targetlodGroups = new Dictionary<String, LodGroup>();
            FragmentType.GetLodGroupsByIdx(fragType.Fragment.FragGroup, ref targetlodGroups);
            foreach (KeyValuePair<String, LodGroup> kvp in lodGroups)
            {
                if (targetlodGroups.ContainsKey(kvp.Key))
                {
                    var rejectList = kvp.Value.Lods[lodLevel].Models.Where(l => targetlodGroups[kvp.Key].Lods[LodLevel.High].Models.Contains(l));
                    (kvp.Value.Lods[lodLevel].Models as List<IModel>).AddRange(targetlodGroups[kvp.Key].Lods[LodLevel.High].Models.Except(rejectList));
                    foreach (RSG.Rage.Entity.Model model in kvp.Value.Lods[lodLevel].Models)
                    {
                        model.LoadMesh(Path.Combine(directory, model.Name));
                        model.RemapMaterialIds(ShaderMap);
                    }
                }
            }

            foreach (RSG.Rage.Entity.Model model in this.LodGroupComponents.MainComponent.Lods[lodLevel].Models)
            {
                model.LoadMesh(Path.Combine(directory, model.Name));
                model.RemapMaterialIds(ShaderMap);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="directory"></param>
        /// <returns></returns>
        public bool SaveAllMeshes(String directory)
        {
            bool result = true;
            IEnumerable<IModel> models = this.LodGroupComponents.MainComponent.GetAllModels();


            IDictionary<String, LodGroup> lodGroups = new Dictionary<String, LodGroup>();
            FragmentType.GetLodGroupsByIdx(this.Fragment.FragGroup, ref lodGroups);
            foreach (LodGroup lg in lodGroups.Values)
            {
                (models as List<IModel>).AddRange(lg.GetAllModels());
            }

            foreach (IModel model in models)
            {
                model.SaveMesh(Path.Combine(directory, model.Name));
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="extension"></param>
        /// <returns></returns>
        public override bool LoadFromFile(String filename, String extension)
        {
            bool result = true;
            BaseTokenizer tokenizer = TokenUtility.OpenTokenizedFile(String.Format("{0}.{1}", filename, extension));
            try
            {
                this.Log.MessageCtx(LOG_CTX, "Loading type file {0}.{1}.", filename, extension);

                TypeFileParser parser = new TypeFileParser();
                parser.RegisterLoader("skel", "skel", this.LoadSkeleton, false);
                parser.RegisterLoader("skeletontype", "all", this.LoadSkeletonType, false);
                parser.RegisterLoader("shadinggroup", "shadinggroup", this.ShadingGroup.LoadShaderGroup, false);
                parser.RegisterLoader("lodgroup", "mesh", this.LodGroupComponents.MainComponent.LoadLodGroup, false);
                parser.RegisterLoader("lodgroup", "mesh|damaged", this.LodGroupComponents.DamagedComponent.LoadLodGroup, false);

                parser.RegisterLoader("lodgroupcloth", "mesh", this.LodClothGroupComponents.MainComponent.LoadLodGroup, false);

                parser.RegisterLoader("fragments", "forceLoadCommonDrawable", this.Fragment.LoadForceCommonDrawable, false);
                parser.RegisterLoader("fragments", "group", this.Fragment.FragGroup.LoadGroup, false);

                parser.RegisterLoader("cloth", "all", this.EnvCloth.LoadEnvCloth, false);

                result = parser.ProcessTypeFile(tokenizer as AsciiTokenizer, true);

                if (this.EnvCloth.FoundClothPaths())
                    HasCloth = true;
            }
            catch (Exception ex)
            {
                this.Log.ExceptionCtx(LOG_CTX, ex, "Exception loading type file {0}.{1}.", filename, extension);
                result = false;
            }
            finally
            {
                tokenizer.Dispose();
            }

            return (result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="extension"></param>
        /// <returns></returns>
        public override bool SaveToFile(String filename, String extension)
        {
            bool result = true;
            try
            {
                this.Log.MessageCtx(LOG_CTX, "Saving type file {0}.{1}.", filename, extension);
                using (BaseTokenizer tokenizerOut = TokenUtility.CreateTokenizedFile(String.Format("{0}.{1}", filename, extension), TokenizerType.Ascii))
                {
                    this.SaveAscii(tokenizerOut as AsciiTokenizer);
                }
            }
            catch (Exception ex)
            {
                this.Log.ExceptionCtx(LOG_CTX, ex, "Exception saving type file {0}.{1}.", filename, extension);
            }

            return (result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cbData"></param>
        public void LoadSkeleton(ref TypeFileCbData cbData)
        {
            AsciiTokenizer tokenizer = cbData.Tokenizer;
            String buffer = String.Empty;
            tokenizer.GetToken(ref buffer, TypeFileParser.MAX_NAMELEN);
            this.Skeleton = buffer;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cbData"></param>
        public void LoadSkeletonType(ref TypeFileCbData cbData)
        {
            AsciiTokenizer tokenizer = cbData.Tokenizer;
            String buffer = String.Empty;
            tokenizer.GetToken(ref buffer, TypeFileParser.MAX_NAMELEN);
            this.SkeletonType = buffer;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <returns></returns>
        public override bool SaveAscii(AsciiTokenizer tokenizer)
        {
            base.SaveAscii(tokenizer);

            this.SaveSkeleton(ref tokenizer);
            this.SaveSkeletonType(ref tokenizer);
            this.ShadingGroup.SaveAscii(tokenizer);
            this.LodGroupComponents.SaveAscii(tokenizer);
            if (this.HasCloth)
            {
                this.LodClothGroupComponents.SaveAscii(tokenizer);
                this.EnvCloth.SaveAscii(tokenizer);
            }   
            this.Fragment.SaveAscii(tokenizer);

            return (true);
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <returns></returns>
        private bool SaveSkeleton(ref AsciiTokenizer tokenizer)
        {
            tokenizer.Put("skel", 0);
            tokenizer.PutDelimiter(" {\n\t");
            tokenizer.Put("skel", 0);
            tokenizer.Put(" ", 0);
            tokenizer.Put(this.Skeleton, 0);
            tokenizer.PutDelimiter("\n}\n");

            return (true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <returns></returns>
        private bool SaveSkeletonType(ref AsciiTokenizer tokenizer)
        {
            tokenizer.Put("skeletontype", 0);
            tokenizer.PutDelimiter(" {\n\t");
            tokenizer.Put("skeletontype", 0);
            tokenizer.Put(" ", 0);
            tokenizer.Put(this.SkeletonType, 0);
            tokenizer.PutDelimiter("\n}\n");

            return (true);
        }
        #endregion //Private Methods

        #region Private Static Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fragmentGroup"></param>
        /// <param name="lodGroups"></param>
        private static void GetLodGroupsByIdx(FragmentGroup fragmentGroup, ref IDictionary<String, LodGroup> lodGroups)
        {
            foreach (FragmentChild fragChild in fragmentGroup.Children)
            {
                if (fragChild.LodGroupComponents.MainComponent.Lods.Count() > 0)
                {
                    lodGroups.Add(fragmentGroup.Name, fragChild.LodGroupComponents.MainComponent);
                }
            }

            foreach (FragmentGroup fg in fragmentGroup.Groups)
            {
                FragmentType.GetLodGroupsByIdx(fg, ref lodGroups);
            }
        }
        #endregion
    }
}
