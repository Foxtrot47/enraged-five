﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.ManagedRage.File;
using RSG.Base.Logging.Universal;

namespace RSG.Rage.TypeFile
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class BaseType
    {
        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public Int32 Version { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public IUniversalLog Log { get; private set; }
        #endregion 

        #region Constructors
        public BaseType(IUniversalLog log)
        {
            this.Log = log;
        }
        #endregion // Constructors

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="extension"></param>
        /// <returns></returns>
        public abstract bool LoadFromFile(String filename, String extension);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="extension"></param>
        public abstract bool SaveToFile(String filename, String extension);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenizer"></param>
        /// <returns></returns>
        public virtual bool SaveAscii(AsciiTokenizer tokenizer)
        {
            tokenizer.PutDelimiter("Version: ");
            tokenizer.Put(this.Version);
            tokenizer.Put("\n", 0);

            return (true);
        }
        #endregion // Controller Methods

    }
}
