﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Logging.Universal;

namespace RSG.Rage.TypeFile
{
    /// <summary>
    /// 
    /// </summary>
    public class VehicleType : 
        FragmentType
    {
        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="log"></param>
        public VehicleType(IUniversalLog log)
            : base(log)
        {

        }
        #endregion // Constructors
    }
}
