﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.ManagedRage.File;
using System.Diagnostics;

namespace RSG.Rage
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="recipient"></param>
    /// <param name="job"></param>
    /// <param name="log"></param>
    public delegate void ParserCallback(ref TypeFileCbData cbData);

    
    public class TypeFileCbData 
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public String EntityName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public AsciiTokenizer Tokenizer { get; set; }
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        public TypeFileCbData()
        {
        }
        #endregion // Constructors
	    
    }

    /// <summary>
    /// 
    /// </summary>
    public class EntityLoaderData
    {
        #region Constructor
        public EntityLoaderData(String name, ParserCallback callback)
        {
            this.Name = name;
            this.Callback = callback;
        }
        #endregion // Constructor
        
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public String Name { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public ParserCallback Callback { get; private set; }
        #endregion // Properties
    }

    /// <summary>
    /// 
    /// </summary>
    public class SectionLoaderData
    {
        #region Constructors
        public SectionLoaderData(String name)
        {
            this.Name = name;
            this.EntityData = new List<EntityLoaderData>();
        }
        #endregion // Constructors
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public String Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool AllowDuplicates { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool Activated { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<EntityLoaderData> EntityData { get; private set; }
        #endregion // Properties
    }

    /// <summary>
    /// 
    /// </summary>
    public sealed class TypeFileParser
    {
        #region Constants
        /// <summary>
        /// 
        /// </summary>
        public static Int32 MAX_NAMELEN = 128;
        #endregion // Constants

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<SectionLoaderData> SectionData { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsEmpty 
        {
            get
            {
                return (this.SectionData.Count() == 0);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool WarningSpew { get; private set; }
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        public TypeFileParser()
        {
            this.SectionData = new List<SectionLoaderData>();
        }
        #endregion // Constructors

        #region Controller Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="callback"></param>
        /// <param name="name"></param>
        /// <param name="sectionname"></param>
        public void RegisterLoader(String sectionName, String entityName, ParserCallback callback, bool allowDupes)
        {
            bool sectionFound = false;
            SectionLoaderData currentSection = new SectionLoaderData("");
            foreach (SectionLoaderData section in this.SectionData)
            {
                if(section.Name.Equals(sectionName))
                {
                    currentSection = section;
                    sectionFound = true;
                    break;
                }
            }

#if DEBUG
            //Debug.Assert((sectionFound && entityName.Equals("all")), "Can't request all entities when entities already registered for section");
	        //Debug.Assert(sectionFound && (currentSection.EntityData.Count() == 1) && (currentSection.EntityData as List<EntityLoaderData>).ElementAt(0).Name.Equals("all"), "Something has already requested 'all' entities for this section");
#endif //__DEBUG
        
            if (!sectionFound)
	        {
		        currentSection = new SectionLoaderData(sectionName);
                (SectionData as List<SectionLoaderData>).Add(currentSection);
		        currentSection.AllowDuplicates = allowDupes;
		        currentSection.Activated = false;
	        }

	        // Get a new entity data
	        EntityLoaderData entity = new EntityLoaderData(entityName, callback);
            (currentSection.EntityData as List<EntityLoaderData>).Add(entity);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool ProcessTypeFile(AsciiTokenizer tokenizer, bool stopAtBracket)
        {
            if (this.IsEmpty)
                return true;

            TypeFileCbData cbData = new TypeFileCbData();

            // set all sections as never having been activated
            foreach(SectionLoaderData sectiondata in this.SectionData)
            {
                sectiondata.Activated = false;
            }

            String buff = String.Empty;
            while (true)
            {
                // Terminate when hitting a bracket matching our starting one
                if (stopAtBracket && Convert.ToBoolean(tokenizer.CheckToken("}", true)))
                {
                    break;
                }

                if (Convert.ToInt32(tokenizer.GetToken(ref buff, 512)) == 0)
                {
                    break;  
                }

                bool sectionFound = false;
                SectionLoaderData currentSection = new SectionLoaderData("");
                // Find the section that matches
                foreach(SectionLoaderData section in this.SectionData)
                {
                    if (section.Name.Equals(buff))
                    {
                        currentSection = section;
                        sectionFound = true;
                        break;
                    }
                }
                Int32 bracketCount = Convert.ToBoolean(tokenizer.CheckToken("{", true)) ? 1 : 0;
                if (sectionFound)
                {
			        // if this can't be used more than once, then duplicated sections indicates an error in the file
			        // perhaps just print a warning and throw this data away? (error probably would be lost in the spew)
        #if __DEV
			        if (section->m_Activated)
				        Quitf("Invalid duplicate section '%s' in file '%s'",buff, T.GetStream()->GetName());
        #endif
                    Debug.Assert(!currentSection.Activated);
                    currentSection.Activated = !currentSection.AllowDuplicates;	

			        // If the bracket count is zero, then just read one entity and terminate this section
			        bool oneRead = false;
			        bool onlyReadOne = false;
			        if (bracketCount == 0)
			        {
				        onlyReadOne = true;
			        }

			        // Process all of the types
                    while (Convert.ToBoolean(tokenizer.CheckToken("}", false)) == false &&
				            (!onlyReadOne || !oneRead) &&
                            Convert.ToBoolean(tokenizer.GetToken(ref buff, MAX_NAMELEN))) 
                    {
				        oneRead = true;
				        // See if nothing exists
				        if (buff.Equals("none"))
					        break;
				        Int32 idx = 0;
				        foreach(EntityLoaderData entity in currentSection.EntityData) 
                        {
                            if (entity.Name.Equals(buff) || entity.Name.Equals("all"))
                            {
                                cbData.EntityName = buff;
                                cbData.Tokenizer = tokenizer;
                                entity.Callback(ref cbData);
                                idx++;
						        break;
					        }
				        }
                        if (idx == currentSection.EntityData.Count())
				        {
                            if ( WarningSpew )
                                Console.WriteLine(("No entities to handle {0} in '{1}.type' file"), buff, tokenizer.GetFilename());
					        // Skip block (DANGER - ENDLESS LOOP IF TOOLS DON'T EXPORT PROPERLY
                            if (Convert.ToBoolean(tokenizer.CheckToken("{", true)))
                                while (Convert.ToBoolean(tokenizer.CheckToken("}", true)) == false)
                                    tokenizer.GetToken(ref buff, MAX_NAMELEN);
				        }
			        }
			        if (bracketCount > 0)
			        {
                        tokenizer.CheckToken("}", true);
			        }
		        }
		        else {
			        // Another risky case for when tools don't spit out data properly
                    if (Convert.ToBoolean(tokenizer.CheckToken("none", true)) == false && WarningSpew)
                        Console.WriteLine("No sections registered to handle {0} in file {1}", buff, tokenizer.GetName());
			        while( bracketCount != 0 )
			        {
                        if (Convert.ToBoolean(tokenizer.CheckToken("{", true)))
					        bracketCount++;
                        else if (Convert.ToBoolean(tokenizer.CheckToken("}", true)))
					        bracketCount--;
                        else if (Convert.ToInt32(tokenizer.GetToken(ref buff, MAX_NAMELEN)) == 0)
					        break;
			        }
		        }
	        }
            return (true);
        }
        #endregion // Controller Methods
    }
}
