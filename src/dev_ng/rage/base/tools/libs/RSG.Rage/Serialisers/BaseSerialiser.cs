﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.ManagedRage.File;
using RSG.Base.Math;
using RSG.ManagedRage.Math;

namespace RSG.Rage
{
	/// <summary>
	/// 
	/// </summary>
	public abstract class BaseSerialiser
		: ISerialiser
	{
		#region Properties
		/// <summary>
		/// 
		/// </summary>
		public BaseTokenizer Tokenizer { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public Boolean Writing { get; set; }
		#endregion

		#region Constructors
		/// <summary>
		/// 
		/// </summary>
		/// <param name="writing"></param>
		internal BaseSerialiser(BaseTokenizer tokenizer, Boolean writing)
		{
			this.Tokenizer = tokenizer;
			this.Writing = writing;
		}
		#endregion // Constructors

		#region Serialisers
		/// <summary>
		/// 
		/// </summary>
		/// <param name="value"></param>
		/// <param name="tag"></param>
		public void Serialise(ref Boolean value, String tag)
		{
			this.BeginValue(ref tag);
			this.Serialise(ref value);
			this.EndValue();
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="value"></param>
		public void Serialise(ref Boolean value)
		{
			if (this.Writing)
				this.Tokenizer.PutByte(value ? 1 : 0);
			else
				value = this.Tokenizer.GetByte(true).Equals(1);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="value"></param>
		/// <param name="tag"></param>
		public void Serialise(ref Int32 value, String tag)
		{
			this.BeginValue(ref tag);
			this.Serialise(ref value);
			this.EndValue();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="value"></param>
		public void Serialise(ref Int32 value)
		{
			if (this.Writing)
				this.Tokenizer.Put(value);
			else
				value = this.Tokenizer.GetInt(true);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="value"></param>
		/// <param name="tag"></param>
		public void Serialise(ref Byte value, String tag)
		{
			this.BeginValue(ref tag);
			this.Serialise(ref value);
			this.EndValue();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="value"></param>
		public void Serialise(ref Byte value)
		{
			if (this.Writing)
				this.Tokenizer.PutByte(value);
			else
				value = (Byte)this.Tokenizer.GetByte(true);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="value"></param>
		/// <param name="tag"></param>
		public void Serialise(ref Single value, String tag)
		{
			this.BeginValue(ref tag);
			this.Serialise(ref value);
			this.EndValue();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="value"></param>
		public void Serialise(ref Single value)
		{
			if (this.Writing)
				this.Tokenizer.Put(value);
			else
				value = this.Tokenizer.GetFloat(true);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="value"></param>
		/// <param name="tag"></param>
		public void Serialise(ref UInt32 value, String tag)
		{
			this.BeginValue(ref tag);
			this.Serialise(ref value);
			this.EndValue();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="value"></param>
		public void Serialise(ref UInt32 value)
		{
			if (this.Writing)
				this.Tokenizer.Put((Int32)value);
			else
				value = (UInt32)this.Tokenizer.GetInt(true);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="value"></param>
		/// <param name="tag"></param>
		public void Serialise(ref Vector2f value, String tag)
		{
			this.BeginValue(ref tag);
			this.Serialise(ref value);
			this.EndValue();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="value"></param>
		public void Serialise(ref Vector2f value)
		{
			if (this.Writing)
			{
				this.Tokenizer.Put(value.X);
				this.Tokenizer.Put(value.Y);
			}
			else
			{
				Vector2 temp = this.Tokenizer.GetVector2(true);
				value = new Vector2f(temp.x, temp.y);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="value"></param>
		/// <param name="tag"></param>
		public void Serialise(ref Vector3f value, String tag)
		{
			this.BeginValue(ref tag);
			this.Serialise(ref value);
			this.EndValue();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="value"></param>
		public void Serialise(ref Vector3f value)
		{
			if (this.Writing)
			{
				this.Tokenizer.Put(value.X);
				this.Tokenizer.Put(value.Y);
				this.Tokenizer.Put(value.Z);
			}
			else
			{
				Vector3 temp = this.Tokenizer.GetVector3(true);
				value = new Vector3f(temp.x, temp.y, temp.z);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="value"></param>
		/// <param name="tag"></param>
		public void Serialise(ref Vector4f value, String tag)
		{
			this.BeginValue(ref tag);
			this.Serialise(ref value);
			this.EndValue();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="value"></param>
		public void Serialise(ref Vector4f value)
		{
			if (this.Writing)
			{
				this.Tokenizer.Put(value.X);
				this.Tokenizer.Put(value.Y);
				this.Tokenizer.Put(value.Z);
				this.Tokenizer.Put(value.W);
			}
			else
			{
				Vector4 temp = this.Tokenizer.GetVector4(true);
				value = new Vector4f(temp.x, temp.y, temp.z, temp.w);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="value"></param>
		/// <param name="tag"></param>
		public void Serialise(ref String value, String tag)
		{
			this.BeginValue(ref tag);
			this.Serialise(ref value);
			this.EndValue();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="value"></param>
		public void Serialise(ref String value) 
		{
			if (this.Writing) {
				if (this.Tokenizer.IsBinary()) 
				{
					this.Tokenizer.PutShort(value.Length + 1);
					this.Tokenizer.Put(value, 0);
				}
				else
					this.Tokenizer.Put(String.Format("\"{0}\"", value), 0);
			}
			else {
				
				if (this.Tokenizer.IsBinary())
				{
					int len = this.Tokenizer.GetShort(true);
					Byte[] temp = new Byte[len];
					for (int i = 0; i < len; i++)
						temp[i] = (Byte)this.Tokenizer.GetByte(true);

					// Remove trailing string termination character
					Byte[] tempNoTerminate = new Byte[len-1];
					for (int i = 0; i < len-1; i++)
						tempNoTerminate[i] = temp[i];
					value = System.Text.Encoding.UTF8.GetString(tempNoTerminate);
				}
				else
				{
					String temp = String.Empty;
					this.Tokenizer.GetToken(ref temp, 128);
					value = temp;
				}  
			}
		}

		// Would be great to handle serialisation of an array using generics.

		/*
		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="value"></param>
		public void Serialise<T>(ref IEnumerable<T> value)
		{
			if (this.Writing)
			{
				this.Tokenizer.Put(value.Count());
				if (value.Count() > 0)
				{
					this.Tokenizer.PutDelimiter("{ ");

					if (sizeof(T) <= sizeof(int))
					{
						const int perLine = 15;
						if (value.Count() >= perLine)
						{
							this.Tokenizer.Indent(1);
							this.Tokenizer.EndLine();
							this.Tokenizer.StartLine();
						}
						for (int i = 0; i < v.GetCount(); i++)
						{
							Serialise((v as List<T>)[i]);
							if (value.Count() >= perLine && (i % perLine) == perLine - 1)
							{
								this.Tokenizer.EndLine();
								this.Tokenizer.StartLine();
							}
						}
						if (value.Count() >= perLine)
						{
							this.Tokenizer.Indent(-1);
							this.Tokenizer.EndLine();
							this.Tokenizer.StartLine();
						}
						this.Tokenizer.PutDelimiter("}");
					}
					else
					{
						this.Tokenizer.EndLine();
						this.Tokenizer.Indent(1);
						for (int i = 0; i < v.GetCount(); i++)
						{
							this.Tokenizer.StartLine();
							Serialise((value as List<T>)[i]);
							this.Tokenizer.EndLine();
						}
						this.Tokenizer.Indent(-1);
						this.Tokenizer.StartLine();
						this.Tokenizer.PutDelimiter("}");
						this.Tokenizer.EndLine();
					}
				}
			}
			else
			{
				int c = this.Tokenizer.GetInt(true);
				value.Reset(c);
				if (c > 0)
				{
					this.Tokenizer.GetDelimiter("{");
					while (c--)
					{
						T temp = T();
						Serialise(value.Append());
					}
					this.Tokenizer.GetDelimiter("}");
				}
			}
		}
		*/
		/// <summary>
		/// 
		/// </summary>
		/// <param name="tag"></param>
		public void BeginValue(ref String tag) 
		{
			if (this.Writing) {
				this.Tokenizer.StartLine();
				this.Tokenizer.PutDelimiter(tag);
				this.Tokenizer.PutDelimiter(" ");
			}
			else
				this.Tokenizer.GetDelimiter(tag);
		}

		/// <summary>
		/// 
		/// </summary>
		public void EndValue() 
		{
			if (Writing)
				this.Tokenizer.EndLine();
		}

		/// <summary>
		/// 
		/// </summary>
		public void BeginBlock() 
		{
			if (this.Writing) 
			{
				this.Tokenizer.StartLine();
				this.Tokenizer.PutDelimiter("{");
				this.Tokenizer.EndLine();
				this.Tokenizer.Indent(1);
			}
			else
				this.Tokenizer.GetDelimiter("{");
		}

		/// <summary>
		/// 
		/// </summary>
		public void EndBlock() 
		{
			if (this.Writing) {
				this.Tokenizer.Indent(-1);
				this.Tokenizer.StartLine();
				this.Tokenizer.PutDelimiter("}");
				this.Tokenizer.EndLine();
			}
			else
				this.Tokenizer.GetDelimiter("}");
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sep"></param>
		public void Separator(ref String sep) 
		{
			if (this.Writing) {
				this.Tokenizer.PutDelimiter(sep);
				this.Tokenizer.PutDelimiter(" ");
			}
			else
				this.Tokenizer.GetDelimiter(sep);
		}
		#endregion // Serialisers
	}
}
