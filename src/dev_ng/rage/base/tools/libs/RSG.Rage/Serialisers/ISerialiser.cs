﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Math;
using RSG.ManagedRage.File;

namespace RSG.Rage
{
    /// <summary>
    /// 
    /// </summary>
    public interface ISerialiser
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        BaseTokenizer Tokenizer { get; set; }

        /// <summary>
        /// 
        /// </summary>
        Boolean Writing { get; set; }
        #endregion 

        #region Serialisers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        void Serialise(ref Boolean value);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        void Serialise(ref Int32 value);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        void Serialise(ref Byte value);
	    
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        void Serialise(ref Single value);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        void Serialise(ref UInt32 value);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        void Serialise(ref Vector2f value);
	    
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        void Serialise(ref Vector3f value);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        void Serialise(ref Vector4f value);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        void Serialise(ref String value);
        #endregion // Serialisers
    }
}
