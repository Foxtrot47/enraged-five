﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Math;
using RSG.ManagedRage.File;

namespace RSG.Rage.Mesh
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class MeshSerialiser
        : BaseSerialiser, 
        ISerialiser
    {
        #region Constants
        /// <summary>
        /// 
        /// </summary>
        private readonly Int32 MAX_FLOAT_BLIND_DATA = 4;
        #endregion // Constants

        #region Properties
        /*/// <summary>
        /// 
        /// </summary>
        public Byte Version 
        {
            get { return m_Version; }
            set { m_Version = value; } 
        }
        Byte m_Version;
        */
        /*
        /// <summary>
        /// 
        /// </summary>
        public Byte BlendTargetCount
        {
            get { return m_BlendTargetCount; }
            set { m_BlendTargetCount = value; }
        }
        public Byte m_BlendTargetCount;
        */
        #endregion Properties

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="writing"></param>
        public MeshSerialiser(ref BaseTokenizer tokenizer, Boolean writing)
            : base(tokenizer, writing)
        {
            
        }
        #endregion // Constructors

        #region Serialisers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        public void Serialise(ref IEnumerable<Vector3f> value, String tag, Int32 perLine = 15)
        {
            this.BeginValue(ref tag);
            this.Serialise(ref value, perLine);
            this.EndValue();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        public void Serialise(ref IEnumerable<Vector3f> value, Int32 perLine = 15)
        {
            if (this.Writing)
            {
                this.Tokenizer.Put(value.Count());
                if (value.Count() > 0)
                {
                    this.Tokenizer.PutDelimiter("{ ");
                    if (value.Count() >= perLine)
                    {
                        this.Tokenizer.Indent(1);
                        this.Tokenizer.EndLine();
                        this.Tokenizer.StartLine();
                    }
                    for (int i = 0; i < value.Count(); i++)
                    {
                        Vector3f v = (value as List<Vector3f>)[i];
                        this.Serialise(ref v);
                        (value as List<Vector3f>)[i] = v;

                        if (value.Count() >= perLine && (i % perLine) == perLine - 1)
                        {
                            this.Tokenizer.EndLine();
                            this.Tokenizer.StartLine();
                        }
                    }
                    if (value.Count() >= perLine)
                    {
                        this.Tokenizer.Indent(-1);
                        this.Tokenizer.EndLine();
                        this.Tokenizer.StartLine();
                    }
                    this.Tokenizer.PutDelimiter("}");
                }
            }
            else
            {
                Int32 c = this.Tokenizer.GetInt(true);
                (value as List<Vector3f>).Clear();
                if (c > 0)
                {
                    this.Tokenizer.GetDelimiter("{");
                    while (c-- > 0)
                    {
                        Vector3f v = new Vector3f();
                        Serialise(ref v);
                        (value as List<Vector3f>).Add(v);
                    }
                    this.Tokenizer.GetDelimiter("}");
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        public void Serialise(ref IEnumerable<Int32> value, String tag, Int32 perLine = 15)
        {
            this.BeginValue(ref tag);
            this.Serialise(ref value, perLine);
            this.EndValue();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        public void Serialise(ref IEnumerable<Int32> value, Int32 perLine = 15)
        {
            if (this.Writing)
            {
                this.Tokenizer.Put(value.Count());
                if (value.Count() > 0)
                {
                    this.Tokenizer.PutDelimiter("{ ");
                    if (value.Count() >= perLine)
                    {
                        this.Tokenizer.Indent(1);
                        this.Tokenizer.EndLine();
                        this.Tokenizer.StartLine();
                    }
                    for (int i = 0; i < value.Count(); i++)
                    {
                        Int32 v = (value as List<Int32>)[i];
                        this.Serialise(ref v);
                        (value as List<Int32>)[i] = v;
  
                        if (value.Count() >= perLine && (i % perLine) == perLine - 1)
                        {
                            this.Tokenizer.EndLine();
                            this.Tokenizer.StartLine();
                        }
                    }
                    if (value.Count() >= perLine)
                    {
                        this.Tokenizer.Indent(-1);
                        this.Tokenizer.EndLine();
                        this.Tokenizer.StartLine();
                    }
                    this.Tokenizer.PutDelimiter("}");
                }
            }
            else
            {
                Int32 c = this.Tokenizer.GetInt(true);
                (value as List<Int32>).Clear();
                if (c > 0)
                {
                    this.Tokenizer.GetDelimiter("{");
                    while (c-- > 0)
                    {
                        Int32 v = 0;
                        Serialise(ref v);
                        (value as List<Int32>).Add(v);
                    }
                    this.Tokenizer.GetDelimiter("}");
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        public void Serialise<T>(ref IEnumerable<T> value, String tag, Int32 preLine = 15) where T : IMeshComponent, new()
        {
            this.BeginValue(ref tag);
            this.Serialise(ref value);
            this.EndValue();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        public void Serialise<T>(ref IEnumerable<T> value) where T : IMeshComponent, new()
        {
            if (this.Writing)
            {
                this.Tokenizer.Put(value.Count());
                if (value.Count() > 0)
                {
                    this.Tokenizer.PutDelimiter("{ ");

                    this.Tokenizer.EndLine();
                    this.Tokenizer.Indent(1);
                    for (int i = 0; i < value.Count(); i++)
                    {
                        this.Tokenizer.StartLine();
                        IMeshComponent meshComponent = (value as List<T>)[i];
                        Serialise(ref meshComponent);

                        this.Tokenizer.EndLine();
                    }
                    this.Tokenizer.Indent(-1);
                    this.Tokenizer.StartLine();
                    this.Tokenizer.PutDelimiter("}");
                    this.Tokenizer.EndLine();

                }
            }
            else
            {
                int c = this.Tokenizer.GetInt(true);
                if (c > 0)
                {
                    this.Tokenizer.GetDelimiter("{");
                    while (c-- > 0)
                    {
                        T temp = new T();
                        IMeshComponent meshComponent = temp as IMeshComponent;
                        Serialise(ref meshComponent);
                        (value as List<T>).Add(temp);
                    }
                    this.Tokenizer.GetDelimiter("}");
                }
            }
        }
        /*
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        public void Serialise(ref IEnumerable<Material> value)
        {
            if (this.Writing)
            {
                this.Tokenizer.Put(value.Count());
                if (value.Count() > 0)
                {
                    this.Tokenizer.PutDelimiter("{ ");

                    this.Tokenizer.EndLine();
                    this.Tokenizer.Indent(1);
                    for (int i = 0; i < value.Count(); i++)
                    {
                        this.Tokenizer.StartLine();
                        Material mat = (value as List<Material>)[i];
                        Serialise(ref mat);
                        (value as List<Material>)[i] = mat;
                        
                        this.Tokenizer.EndLine();
                    }
                    this.Tokenizer.Indent(-1);
                    this.Tokenizer.StartLine();
                    this.Tokenizer.PutDelimiter("}");
                    this.Tokenizer.EndLine();

                }
            }
            else
            {
                int c = this.Tokenizer.GetInt(true);
                if (c > 0)
                {
                    this.Tokenizer.GetDelimiter("{");
                    while (c-- > 0)
                    {
                        Material mat = new Material();
                        Serialise(ref mat);
                        (value as List<Material>).Add(mat);
                    }
                    this.Tokenizer.GetDelimiter("}");
                }
            }
        }
        */
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        public void Serialise(ref IMeshComponent value)
        {
            if(value is Material)
            {
                Material mat = value as Material;
                mat.Serialise(this);
                value = mat;
            }
            else if(value is Primitive)
            {
                Primitive prim = value as Primitive;
                prim.Serialise(this);
                value = prim;
            }
            else if(value is Vertex)
            {
                Vertex vert = value as Vertex;
                vert.Serialise(this);
                value = vert;
            }
            else if (value is Channel)
            {
                Channel chan = value as Channel;
                chan.Serialise(this);
                value = chan;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="f"></param>
        /// <returns></returns>
        Int32 PackElement(float f) 
        {
	        if (f < -1) f = -1;
	        else if (f > 1) f = 1;
	        return Convert.ToInt32(f * 256) & 1023;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="i"></param>
        /// <returns></returns>
        Single UnpackElement(int i) 
        {
	        // sign extend
	        i = (i << 22) >> 22;
	        return i / 256.0f;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        public void SerialiseNrm(ref Vector3f value)
        {
            if (this.Tokenizer.IsBinary() && Mesh.Version<=11) 
            {
		        if (this.Writing) 
                {
			        int packed = (PackElement(value.X) << 20) | (PackElement(value.Y) << 10) | PackElement(value.Z);
			        this.Serialise(ref packed);
		        }
		        else 
                {
			        int packed = 0;
			        this.Serialise(ref packed);
			        value.X = UnpackElement(packed >> 20);
			        value.Y = UnpackElement((packed >> 10) & 1023);
			        value.Z = UnpackElement(packed & 1023);
		        }
	        }
	        else
		        this.Serialise(ref value);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="tag"></param>
        public void Serialise(ref PrimType value, String tag)
        {
            this.BeginValue(ref tag);
            this.Serialise(ref value);
            this.EndValue();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        public void Serialise(ref PrimType type)
        {
            String[] lut = new String[] { "POINTS","LINES","TRIANGLES","TRISTRIP","TRISTRIP2","QUADS","POLYGON" };
	        if (this.Writing) {
		        if (this.Tokenizer.IsBinary())
                    this.Tokenizer.PutByte((Int32)type);
		        else
                    this.Tokenizer.PutDelimiter(lut[(Int32)type]);
	        }
	        else {
		        if (this.Tokenizer.IsBinary())
                {
			        type = (PrimType) this.Tokenizer.GetByte(true);
                }
		        else {
			        String buf = String.Empty;
			        this.Tokenizer.GetToken(ref buf, TypeFileParser.MAX_NAMELEN);
                    for (Int32 i = 0; i < lut.Count(); i++)
                    {
                        if (buf.Equals(lut[i]))
                        {
                            type = (PrimType)i;
                            return;
                        }
                    }
                    type = PrimType.mshPOINTS;
		        }
	        }
        }
        #endregion // Serialisers
    }
}
