﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Text;

namespace RSG.Base.Reflection
{
    /// <summary>
    /// System processor information.
    /// </summary>
    [Obsolete("Use class in RSG.Base.SystemSpecs instead.")]
    public class GraphicsInfo : IWMIInfo
    {
        #region Properties
        /// <summary>
        /// Label by which the object is known.
        /// </summary>
        public String Name { get; private set; }

        public UInt32 Memory { get; private set; }

        public String Driver { get; private set; }

        #endregion // Properties

        #region IWMIInfo Implementation
        /// <summary>
        /// Ref: http://msdn.microsoft.com/en-us/library/aa394373(VS.85).aspx
        /// </summary>
        /// <param name="scope"></param>
        public void LoadData(ManagementScope scope)
        {
            ObjectQuery query = new ObjectQuery("SELECT * FROM Win32_VideoController");
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(scope, query);
            ManagementObjectCollection queryCollection = searcher.Get();

            foreach (ManagementObject mo in queryCollection)
            {
                //first device with VRAM
                if (mo["AdapterRAM"] != null)
                {
                    Name = (String)mo["Name"];
                    Memory = (UInt32)mo["AdapterRAM"];
                    Driver = (String)mo["DriverVersion"];
                    break;
                }
            }
        }
        #endregion // IWMIInfo Implementation
    } // GraphicsInfo
}
