﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Text;

namespace RSG.Base.Reflection
{
    /// <summary>
    /// Class for retrieving information about the user's system based on information obtained via the WMI interface.
    /// e.g. CPU/Memory details.
    /// 
    /// This could be extended to retrieve remote system information by changing the code used to create the management
    /// scope, but that isn't needed at the moment.
    /// </summary>
    [Obsolete("Use class in RSG.Base.SystemSpecs instead.")]
    public sealed class SystemInfo
    {
        #region Properties
        /// <summary>
        /// Information about the processor.
        /// </summary>
        public ProcessorInfo Processor
        {
            get
            {
                if (m_processorInfo == null)
                {
                    m_processorInfo = new ProcessorInfo();
                    m_processorInfo.LoadData(m_scope);
                }
                return m_processorInfo;
            }
        }
        private ProcessorInfo m_processorInfo;
        
        /// <summary>
        /// Information about the memory.
        /// </summary>
        public MemoryInfo Memory
        {
            get
            {
                if (m_memoryInfo == null)
                {
                    m_memoryInfo = new MemoryInfo();
                    m_memoryInfo.LoadData(m_scope);
                }
                return m_memoryInfo;
            }
        }
        private MemoryInfo m_memoryInfo;

        /// <summary>
        /// Information about the GPU.
        /// </summary>
        public GraphicsInfo Graphics
        {
            get
            {
                if (m_graphicsInfo == null)
                {
                    m_graphicsInfo = new GraphicsInfo();
                    m_graphicsInfo.LoadData(m_scope);
                }
                return m_graphicsInfo;
            }
        }
        private GraphicsInfo m_graphicsInfo;

        #endregion // Properties

        #region Member Data
        /// <summary>
        /// WMI Management scope.
        /// </summary>
        private ManagementScope m_scope;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Creates a management scope for use by the sub info classes.
        /// </summary>
        public SystemInfo()
        {
            m_scope = new ManagementScope(String.Format(@"\\{0}\root\cimv2", Environment.MachineName));
            m_scope.Connect();
        }
        #endregion // Constructor(s)

        public String GetMachineName() { return Environment.MachineName; }
    } // SystemInfo
}
