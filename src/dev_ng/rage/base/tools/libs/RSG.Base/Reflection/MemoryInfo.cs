﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Text;

namespace RSG.Base.Reflection
{
    /// <summary>
    /// System memory information.
    /// </summary>
    [Obsolete("Use class in RSG.Base.SystemSpecs instead.")]
    public class MemoryInfo : IWMIInfo
    {
        #region Properties
        /// <summary>
        /// Total capacity of the physical memory—in bytes.
        /// </summary>
        public ulong Capacity { get; private set; }
        #endregion // Properties

        #region IWMIInfo Implementation
        /// <summary>
        /// Ref: http://msdn.microsoft.com/en-us/library/aa394347(v=vs.85).aspx
        /// </summary>
        /// <param name="scope"></param>
        public void LoadData(ManagementScope scope)
        {
            ObjectQuery query = new ObjectQuery("SELECT * FROM Win32_PhysicalMemory");
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(scope, query);
            ManagementObjectCollection queryCollection = searcher.Get();

            foreach (ManagementObject mo in queryCollection)
            {
                Capacity += (ulong)mo["Capacity"];
            }
        }
        #endregion // IWMIInfo Implementation
    }
}
