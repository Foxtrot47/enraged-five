﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Text;

namespace RSG.Base.Reflection
{
    /// <summary>
    /// System processor information.
    /// </summary>
    [Obsolete("Use class in RSG.Base.SystemSpecs instead.")]
    public class ProcessorInfo : IWMIInfo
    {
        #region Properties
        /// <summary>
        /// Label by which the object is known.
        /// </summary>
        public String Name { get; private set; }

        /// <summary>
        /// Number of logical processors for the current instance of the processor.
        /// For processors capable of hyperthreading, this value includes only the processors which have hyperthreading enabled. 
        /// </summary>
        public uint NumberOfLogicalProcessors { get; private set; }

        /// <summary>
        /// Number of cores for the current instance of the processor.
        /// A core is a physical processor on the integrated circuit.
        /// For example, in a dual-core processor this property has a value of 2.
        /// </summary>
        public uint NumberOfCores { get; private set; }
        #endregion // Properties

        #region IWMIInfo Implementation
        /// <summary>
        /// Ref: http://msdn.microsoft.com/en-us/library/aa394373(VS.85).aspx
        /// </summary>
        /// <param name="scope"></param>
        public void LoadData(ManagementScope scope)
        {
            ObjectQuery query = new ObjectQuery("SELECT * FROM Win32_Processor");
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(scope, query);
            ManagementObjectCollection queryCollection = searcher.Get();

            foreach (ManagementObject mo in queryCollection)
            {
                Name = (String)mo["Name"];
                NumberOfLogicalProcessors = (uint)mo["NumberOfLogicalProcessors"];
                NumberOfCores = (uint)mo["NumberOfCores"];
            }
        }
        #endregion // IWMIInfo Implementation
    } // ProcessorInfo
}
