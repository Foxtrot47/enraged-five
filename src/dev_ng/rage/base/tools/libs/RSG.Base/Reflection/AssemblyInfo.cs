﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace RSG.Base.Reflection
{

    /// <summary>
    /// Class to read assembly information into convenient properties.
    /// </summary>
    /// Constructors available for particular Assembly; defaulting to the
    /// application entry assembly.
    /// 
    public class AssemblyInfo
    {        
        #region Properties and Associated Member Data
        /// <summary>
        /// 
        /// </summary>
        public String Title
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public String Description
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public String Configuration
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public String Company
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public String Product
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public String Copyright
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public String Trademark
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public String FileVersion
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public DebuggableAttribute.DebuggingModes DebugFlags
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public bool ComVisible
        {
            get;
            private set;
        }
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor (uses entry Assembly).
        /// </summary>
        public AssemblyInfo()
        {
            Init(Assembly.GetEntryAssembly());
        }

        /// <summary>
        /// Constructor, specifying Assembly object.
        /// </summary>
        /// <param name="ass"></param>
        public AssemblyInfo(Assembly ass)
        {
            Init(ass);
        }
        #endregion // Constructor(s)

        #region Private Methods
        /// <summary>
        /// Initial member properties based on passed in Assembly.
        /// </summary>
        /// <param name="ass"></param>
        private void Init(Assembly ass)
        {
            Object[] infos = ass.GetCustomAttributes(true);
            foreach (Object o in infos)
            {
                if (o is AssemblyTitleAttribute)
                {
                    this.Title =
                        (o as AssemblyTitleAttribute).Title;
                }
                else if (o is AssemblyDescriptionAttribute)
                {
                    this.Description =
                        (o as AssemblyDescriptionAttribute).Description;
                }
                else if (o is AssemblyCompanyAttribute)
                {
                    this.Company =
                        (o as AssemblyCompanyAttribute).Company;
                }
                else if (o is AssemblyProductAttribute)
                {
                    this.Product =
                        (o as AssemblyProductAttribute).Product;
                }
                else if (o is AssemblyCopyrightAttribute)
                {
                    this.Copyright =
                        (o as AssemblyCopyrightAttribute).Copyright;
                }
                else if (o is AssemblyTrademarkAttribute)
                {
                    this.Trademark =
                        (o as AssemblyTrademarkAttribute).Trademark;
                }
                else if (o is AssemblyFileVersionAttribute)
                {
                    this.FileVersion = 
                        (o as AssemblyFileVersionAttribute).Version;
                }
                else if (o is ComVisibleAttribute)
                {
                    this.ComVisible = (o as ComVisibleAttribute).Value;
                }
                else if (o is DebuggableAttribute)
                {
                    this.DebugFlags = 
                        (o as DebuggableAttribute).DebuggingFlags;
                }
            }
        }
        #endregion // Private Methods
    }

} // RSG.Base.Reflection namespace
