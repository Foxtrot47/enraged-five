﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Text;

namespace RSG.Base.Reflection
{
    /// <summary>
    /// Interface that all classes that gather information from WMI need to implement.
    /// </summary>
    public interface IWMIInfo
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="scope"></param>
        void LoadData(ManagementScope scope);
    } // IWMIInfo
}
