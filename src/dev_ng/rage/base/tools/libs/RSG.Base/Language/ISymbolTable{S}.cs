﻿//---------------------------------------------------------------------------------------------
// <copyright file="ISymbolTable.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Base.Language
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Interface for Symbol Tables.
    /// </summary>
    /// <typeparam name="SYMBOL"></typeparam>
    /// <see cref="IScope"/>
    public interface ISymbolTable<SYMBOL> 
        where SYMBOL : ISymbol
    {
        #region Methods
        /// <summary>
        /// Add a new symbol.
        /// </summary>
        /// <param name="sym"></param>
        /// <param name="value"></param>
        void Add(SYMBOL sym, Object value);

        /// <summary>
        /// Remove a previously defined symbol.
        /// </summary>
        /// <param name="sym"></param>
        /// <see cref="Add"/>
        /// <see cref="Lookup"/>
        /// <see cref="IsDefined"/>
        void Remove(SYMBOL sym);

        /// <summary>
        /// Lookup the value of a symbol.
        /// </summary>
        /// <param name="sym"></param>
        /// <returns></returns>
        /// <see cref="IsDefined" />
        Object Lookup(SYMBOL sym);
        
        /// <summary>
        /// Determine whether a symbol is defined.
        /// </summary>
        /// <param name="sym"></param>
        /// <returns></returns>
        bool IsDefined(SYMBOL sym);
        #endregion // Methods
    }

} // RSG.Base.Language namespace
