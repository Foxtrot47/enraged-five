﻿//---------------------------------------------------------------------------------------------
// <copyright file="ISyntaxError.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Base.Language
{
    using System;

    /// <summary>
    /// Syntax error interface.
    /// </summary>
    public interface ISyntaxError
    {
        #region Properties
        /// <summary>
        /// Input stream line.
        /// </summary>
        String InputLine { get; }
        
        /// <summary>
        /// Current input symbol.
        /// </summary>
        ISymbol Symbol { get; }
        
        /// <summary>
        /// Position of syntax error (column within current line).
        /// </summary>
        /// <see cref="ISyntaxError.InputLine"/>
        IPosition Position { get; }

        /// <summary>
        /// Syntax error message.
        /// </summary>
        String Message { get; }
        #endregion // Properties
    }

} // RSG.Base.Language namespace
