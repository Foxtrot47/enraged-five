﻿//---------------------------------------------------------------------------------------------
// <copyright file="LogErrorHandler.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Base.Language
{
    using System;
    using RSG.Base.Logging;
    
    /// <summary>
    /// Error handler implementation for sending errors to a ILog.
    /// </summary>
    public class LogErrorHandler : IErrorHandler
    {
        #region Member Data
        /// <summary>
        /// Log object.
        /// </summary>
        private ILog _log;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="log"></param>
        public LogErrorHandler(ILog log)
        {
            if (null == log)
                throw (new ArgumentNullException("log"));
            this._log = log;
        }
        #endregion // Constructor(s)
        
        #region IErrorHandler Interface Methods
        /// <summary>
        /// Highlight an error to the user; in our case log an error.
        /// </summary>
        /// <param name="syntaxError"></param>
        public void SyntaxError(ISyntaxError syntaxError)
        {
            _log.Error("({1}:{2}): syntax error: {3} {4}", syntaxError.Position.StartLine,
                syntaxError.Position.StartColumn, syntaxError.Symbol.Name, syntaxError.Message);
            _log.Error("{0}", syntaxError.InputLine);
        }

        /// <summary>
        /// Highlight an warning to the user; in our case log a warning.
        /// </summary>
        /// <param name="syntaxWarning"></param>
        public void SyntaxWarning(ISyntaxError syntaxWarning)
        {
            _log.Warning("({1}:{2}): warning: {3} {4}", syntaxWarning.Position.StartLine,
                syntaxWarning.Position.StartColumn, syntaxWarning.Symbol.Name, syntaxWarning.Message);
            _log.Warning("{0}", syntaxWarning.InputLine);
        }
        #endregion // IErrorHandler Interface Methods
    }

} // RSG.Base.Language namespace
