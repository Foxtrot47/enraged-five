﻿//---------------------------------------------------------------------------------------------
// <copyright file="ISymbolTable.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Base.Language
{

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="SYMBOL"></typeparam>
    public interface ISymbolTable<SYMBOL, TYPEENUM>
        : ISymbolTable<SYMBOL>
        where SYMBOL : ISymbol<TYPEENUM>
    {
    }

} // RSG.Base.Language namespace
