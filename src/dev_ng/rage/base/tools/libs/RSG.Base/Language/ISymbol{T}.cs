﻿//---------------------------------------------------------------------------------------------
// <copyright file="ISymbol{T}.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Base.Language
{
    
    /// <summary>
    /// Symbol interface; to ensure we keep parser generator independent.
    /// </summary>
    /// <typeparam name="TYPEENUM"></typeparam>
    public interface ISymbol<TYPEENUM> : ISymbol
    {
        /// <summary>
        /// Symbol type.
        /// </summary>
        TYPEENUM Type { get; }
    }

} // RSG.Base.Language namespace
