﻿//---------------------------------------------------------------------------------------------
// <copyright file="SearchLanguageErrorHandler.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.SearchLanguage
{
    using System.Collections.Generic;
    using RSG.Base.Language;

    /// <summary>
    /// Error handler implementation that stores all syntax errors for later analysis.
    /// </summary>
    public class StoreErrorHandler : IErrorHandler
    {
        #region Properties
        /// <summary>
        /// Enumerable of errors; so we can report on them all at once or analyse them.
        /// </summary>
        public IEnumerable<ISyntaxError> SyntaxErrors
        {
            get { return _syntaxErrors; }
        }
        private ICollection<ISyntaxError> _syntaxErrors;

        /// <summary>
        /// Enumerable of warnings; so we can report on them all at once or analyse them.
        /// </summary>
        public IEnumerable<ISyntaxError> SyntaxWarnings
        {
            get { return _syntaxErrors; }
        }
        private ICollection<ISyntaxError> _syntaxWarnings;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public StoreErrorHandler()
        {
            this._syntaxErrors = new List<ISyntaxError>();
            this._syntaxWarnings = new List<ISyntaxError>();
        }
        #endregion // Constructor(s)

        #region Methods
        /// <summary>
        /// Highlight an error to the user; in our case store the error.
        /// </summary>
        /// <param name="syntaxError"></param>
        public void SyntaxError(ISyntaxError syntaxError)
        {
            this._syntaxErrors.Add(syntaxError);
        }

        /// <summary>
        /// Highlight an error to the user; in our case store the warning.
        /// </summary>
        /// <param name="syntaxWarning"></param>
        public void SyntaxWarning(ISyntaxError syntaxWarning)
        {
            this._syntaxWarnings.Add(syntaxWarning);
        }
        #endregion // Methods
    }

} // RSG.SearchLanguage namespace
