﻿//---------------------------------------------------------------------------------------------
// <copyright file="IErrorHandler.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Base.Language
{
    using System;

    /// <summary>
    /// Language Error Handler interface; to ensure we keep parser generator independent.
    /// </summary>
    public interface IErrorHandler
    {
        #region Methods
        /// <summary>
        /// Highlight an error to the user.
        /// </summary>
        /// <param name="syntaxError"></param>
        void SyntaxError(ISyntaxError syntaxError);

        /// <summary>
        /// Highlight a warning to the user.
        /// </summary>
        /// <param name="syntaxWarning"></param>
        void SyntaxWarning(ISyntaxError syntaxWarning);
        #endregion // Methods
    }

} // RSG.Base.Language namespace
