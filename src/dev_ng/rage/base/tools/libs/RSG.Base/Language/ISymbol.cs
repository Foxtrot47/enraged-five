﻿//---------------------------------------------------------------------------------------------
// <copyright file="ISymbol.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Base.Language
{
    using System;

    /// <summary>
    /// Symbol interface; to ensure we keep parser generator independent.
    /// </summary>
    public interface ISymbol
    {
        /// <summary>
        /// Symbol name.
        /// </summary>
        String Name { get; }
    }

} // RSG.Base.Language namespace
