﻿//---------------------------------------------------------------------------------------------
// <copyright file="IScope.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Base.Language
{
    using System;

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="SYMBOL"></typeparam>
    public interface IScope<SYMBOL, TYPEENUM> 
        where SYMBOL : ISymbol<TYPEENUM>
    {
        #region Properties
        /// <summary>
        /// Scope name.
        /// </summary>
        String Name { get; }

        /// <summary>
        /// Parent scope (or null).
        /// </summary>
        IScope<SYMBOL, TYPEENUM> Parent { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Define a new symbol in this scope.
        /// </summary>
        /// <param name="sym"></param>
        void Define(SYMBOL sym);

        /// <summary>
        /// Resolve a string name to a symbol.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        SYMBOL Resolve(String name);
        #endregion // Methods
    }

} // RSG.Base.Language namespace
