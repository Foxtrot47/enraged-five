﻿//---------------------------------------------------------------------------------------------
// <copyright file="IAstNode.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Base.Language
{

    /// <summary>
    /// AST node interface.
    /// </summary>
    public interface IAstNode
    {
        #region Properties
        /// <summary>
        /// String positional information for this AST node (and children).
        /// </summary>
        IPosition Position { get; }
        #endregion // Properties
    }

} // RSG.Base.Language namespace
