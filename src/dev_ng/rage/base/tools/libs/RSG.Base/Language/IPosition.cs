﻿//---------------------------------------------------------------------------------------------
// <copyright file="IPosition.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Base.Language
{

    /// <summary>
    /// Represents position information for a token or sequence of tokens.
    /// </summary>
    public interface IPosition
    {
        #region Properties
        /// <summary>
        /// Start line number.
        /// </summary>
        uint StartLine { get; }
        
        /// <summary>
        /// Start column number.
        /// </summary>
        uint StartColumn { get; }
        
        /// <summary>
        /// End line number.
        /// </summary>
        uint EndLine { get; }
        
        /// <summary>
        /// End column number.
        /// </summary>
        uint EndColumn { get; }
        #endregion // Properties
    }

} // RSG.Base.Language namespace
