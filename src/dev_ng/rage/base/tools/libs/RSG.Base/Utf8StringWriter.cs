﻿//---------------------------------------------------------------------------------------------
// <copyright file="Utf8StringWriter.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Base
{
    using System;
    using System.IO;
    using System.Text;

    /// <summary>
    /// Implements a text writer for writing information to a string builder with a utf8
    /// encoding.
    /// </summary>
    public class Utf8StringWriter : StringWriter
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="Utf8StringWriter"/> class.
        /// </summary>
        public Utf8StringWriter()
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Utf8StringWriter"/> class.
        /// </summary>
        /// <param name="formatProvider">
        /// A IFormatProvider object that controls formatting.
        /// </param>
        public Utf8StringWriter(IFormatProvider formatProvider)
            : base(formatProvider)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Utf8StringWriter"/> class.
        /// </summary>
        /// <param name="sb">
        /// The StringBuilder to write to.
        /// </param>
        public Utf8StringWriter(StringBuilder sb)
            : base(sb)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Utf8StringWriter"/> class.
        /// </summary>
        /// <param name="sb">
        /// The StringBuilder to write to.
        /// </param>
        /// <param name="formatProvider">
        /// A IFormatProvider object that controls formatting.
        /// </param>
        public Utf8StringWriter(StringBuilder sb, IFormatProvider formatProvider)
            : base(sb, formatProvider)
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the System.Text.Encoding in which the output is written.
        /// </summary>
        public override Encoding Encoding
        {
            get { return Encoding.UTF8; }
        }
        #endregion Properties
    } // RSG.Base.Utf8StringWriter {Class}
} // RSG.Base {Namespace}
