//
// File: API.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of Win32 API abstraction class
//
// References:
//   Microsoft Custom Dialog Support
//     http://www.codeproject.com/useritems/Custom_Color_Dialog_Box.asp
//

using System;
using System.Runtime.InteropServices;
using System.Text;

namespace RSG.Base.Win32
{

    #region Microsoft Custom Dialog Support
    /// <summary>
    /// Defines the shape of hook procedures that can be called by the ChooseColorDialog
    /// </summary>
    public delegate IntPtr CCHookProc(IntPtr hWnd, UInt16 msg, Int32 wParam, Int32 lParam);

    /// <summary>
    /// Values that can be placed in the CHOOSECOLOR structure, we don't use all of them
    /// </summary>
    public class ChooseColorFlags
    {
        public const Int32 RGBInit = 0x00000001;
        public const Int32 FullOpen = 0x00000002;
        public const Int32 PreventFullOpen = 0x00000004;
        public const Int32 ShowHelp = 0x00000008;
        public const Int32 EnableHook = 0x00000010;
        public const Int32 EnableTemplate = 0x00000020;
        public const Int32 EnableTemplatehandle = 0x00000040;
        public const Int32 SoliDcolor = 0x00000080;
        public const Int32 AnyColor = 0x00000100;
    };

    /// <summary>
    /// A small subset of the window messages that can be sent to the ChooseColorDialog
    /// These are just the ones that this implementation is interested in
    /// </summary>
    public class WindowMessage
    {
        public const UInt16 InitDialog = 0x0110;
        public const UInt16 Size = 0x0005;
        public const UInt16 Notify = 0x004E;
    };

    /// <summary>
    /// See the documentation for CHOOSECOLOR
    /// </summary>
    public struct ChooseColor
    {
        public Int32 lStructSize;
        public IntPtr hwndOwner;
        public IntPtr hInstance;
        public Int32 rgbResult;
        public IntPtr lpCustColors;
        public Int32 Flags;
        public IntPtr lCustData;
        public CCHookProc lpfnHook;
        public IntPtr lpTemplateName;
    };

    /// <summary>
    /// The rectangle structure used in Win32 API calls
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct RECT
    {
        public int left;
        public int top;
        public int right;
        public int bottom;
    };

    /// <summary>
    /// The point structure used in Win32 API calls
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct POINT
    {
        public int X;
        public int Y;
    };
    #endregion // Microsoft Custom Dialog Support

    /// <summary>
    /// Win32 API Compatibility layer class
    /// </summary>
    /// This class exposes some of the Win32 API.  Function names and other
    /// symbols are maintained when appropriate.  Look up the function
    /// names on MSDN for argument/return code details.
    /// 
    /// Was formerly RSG.Base.cWin32API.
    public class API
    {
        #region Windows Message Constants
        public const int SC_RESTORE = 0xF120;
        public const int SC_CLOSE = 0xF060;
        public const int SC_MAXIMIZE = 0xF030;
        public const int SC_MINIMIZE = 0xF020;

        public const int WM_COPYDATA = 0x004A;
        public const int WM_COMMAND = 0x111;
        public const int WM_SYSCOMMAND = 0x0112;
        public const int WM_USER = 0x0400;

        public const int GWL_STYLE = (-16);
        public const int GWL_EXSTYLE = (-20);
        
        public const uint WS_EX_CONTEXTHELP = 0x00000400;
        public const uint WS_MINIMIZEBOX = 0x00020000;
        public const uint WS_MAXIMIZEBOX = 0x00010000;
        public const int SWP_NOSIZE = 0x0001;
        public const int SWP_NOMOVE = 0x0002;
        public const int SWP_NOZORDER = 0x0004;
        public const int SWP_FRAMECHANGED = 0x0020;
        public const int SC_CONTEXTHELP = 0xF180;
        #endregion

        #region Windows Flags

        /// <summary>
        /// Window Style Flags
        /// </summary>
        [Flags]
        public enum WindowStyleFlags : uint
        {
            WS_OVERLAPPED = 0x00000000,
            WS_POPUP = 0x80000000,
            WS_CHILD = 0x40000000,
            WS_MINIMIZE = 0x20000000,
            WS_VISIBLE = 0x10000000,
            WS_DISABLED = 0x08000000,
            WS_CLIPSIBLINGS = 0x04000000,
            WS_CLIPCHILDREN = 0x02000000,
            WS_MAXIMIZE = 0x01000000,
            WS_BORDER = 0x00800000,
            WS_DLGFRAME = 0x00400000,
            WS_VSCROLL = 0x00200000,
            WS_HSCROLL = 0x00100000,
            WS_SYSMENU = 0x00080000,
            WS_THICKFRAME = 0x00040000,
            WS_GROUP = 0x00020000,
            WS_TABSTOP = 0x00010000,
            WS_MINIMIZEBOX = 0x00020000,
            WS_MAXIMIZEBOX = 0x00010000,
        }

        /// <summary>
        /// Extended Windows Style flags
        /// </summary>
        [Flags]
        public enum ExtendedWindowStyleFlags : int
        {
            WS_EX_DLGMODALFRAME = 0x00000001,
            WS_EX_NOPARENTNOTIFY = 0x00000004,
            WS_EX_TOPMOST = 0x00000008,
            WS_EX_ACCEPTFILES = 0x00000010,
            WS_EX_TRANSPARENT = 0x00000020,

            WS_EX_MDICHILD = 0x00000040,
            WS_EX_TOOLWINDOW = 0x00000080,
            WS_EX_WINDOWEDGE = 0x00000100,
            WS_EX_CLIENTEDGE = 0x00000200,
            WS_EX_CONTEXTHELP = 0x00000400,

            WS_EX_RIGHT = 0x00001000,
            WS_EX_LEFT = 0x00000000,
            WS_EX_RTLREADING = 0x00002000,
            WS_EX_LTRREADING = 0x00000000,
            WS_EX_LEFTSCROLLBAR = 0x00004000,
            WS_EX_RIGHTSCROLLBAR = 0x00000000,

            WS_EX_CONTROLPARENT = 0x00010000,
            WS_EX_STATICEDGE = 0x00020000,
            WS_EX_APPWINDOW = 0x00040000,

            WS_EX_LAYERED = 0x00080000,

            WS_EX_NOINHERITLAYOUT = 0x00100000, // Disable inheritence of mirroring by children
            WS_EX_LAYOUTRTL = 0x00400000, // Right to left mirroring

            WS_EX_COMPOSITED = 0x02000000,
            WS_EX_NOACTIVATE = 0x08000000
        }
        #endregion

        #region Structures
        public struct COPYDATASTRUCT
        {
            public int dwData;
            public int cbData;
            public IntPtr lpData;
        };
        #endregion

        #region Windows Local Heap Allocation Constants
        public const int LHND = 0x0042;
        public const int LMEM_FIXED = 0x0000;
        public const int LMEM_MOVEABLE = 0x0002;
        public const int LMEM_ZEROINIT = 0x0040;
        public const int LPTR = 0x0040;
        #endregion

        #region Advapi32.dll Functions
        public static UIntPtr HKEY_LOCAL_MACHINE = new UIntPtr(0x80000002u);
        public static UIntPtr HKEY_CURRENT_USER = new UIntPtr(0x80000001u);

        [DllImport("Advapi32.dll")]
        public static extern uint RegOpenKeyEx(
            UIntPtr hKey,
            string lpSubKey,
            uint ulOptions,
            int samDesired,
            out int phkResult);

        [DllImport("Advapi32.dll")]
        public static extern uint RegCloseKey(int hKey);

        [DllImport("advapi32.dll", EntryPoint = "RegQueryValueEx")]
        public static extern int RegQueryValueEx(
            int hKey, string lpValueName,
            int lpReserved,
            ref uint lpType,
            System.Text.StringBuilder lpData,
            ref uint lpcbData);
        #endregion // Advapi32.dll Functions

        #region GDI32.dll Functions
        [DllImport("gdi32")]
        public static extern int DeleteObject(IntPtr o);
        #endregion // GDI32.dll Functions

        #region User32.dll Functions

        #region Enumerations
        public enum GetWindow_Cmd
        {
            GW_HWNDFIRST = 0,
            GW_HWNDLAST = 1,
            GW_HWNDNEXT = 2,
            GW_HWNDPREV = 3,
            GW_OWNER = 4,
            GW_CHILD = 5,
            GW_ENABLEDPOPUP = 6
        }
        #endregion

        #region Delegates
        public delegate int EnumWindowsProc(IntPtr hwnd, int lParam);
        #endregion

        #region Structures
        #region Structures
        [StructLayout(LayoutKind.Sequential, Pack = 4)]
        public struct RECT
        {
            public int Left;
            public int Top;
            public int Right;
            public int Bottom;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 4)]
        public struct FLASHWINFO
        {
            public int cbSize;
            public IntPtr hwnd;
            public int dwFlags;
            public int uCount;
            public int dwTimeout;
        }
        #endregion
        #endregion
        /// <summary>
        /// http://www.pinvoke.net/default.aspx/user32.GetWindow 
        /// </summary>
        /// <param name="hWnd"></param>
        /// <param name="uCmd"></param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        static public extern IntPtr GetWindow(IntPtr hWnd, GetWindow_Cmd uCmd);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public extern static uint GetWindowLong(IntPtr hwnd, int nIndex);

        [DllImport("user32.dll")]
        public static extern int SetWindowLong(IntPtr hwnd, int index, uint newStyle);

        [DllImport("user32.dll")]
        public static extern bool SetWindowPos(IntPtr hwnd, IntPtr hwndInsertAfter, int x, int y, int width, int height, uint flags);
        
        [DllImport("user32.dll")]
        static public extern int GetWindowText(IntPtr hWnd, StringBuilder strWindowText, int iMaxLength);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public extern static int GetClassName(IntPtr hWnd, StringBuilder lpClassName, int nMaxCount);

        [DllImport("user32.dll")]
        public extern static int IsWindowVisible(IntPtr hWnd);

        [DllImport("user32.dll")]
        public extern static int IsIconic(IntPtr hWnd);

        [DllImport("user32.dll")]
        public extern static int IsZoomed(IntPtr hwnd);

        /// <summary>
        /// http://www.pinvoke.net/default.aspx/user32.GetDesktopWindow
        /// </summary>
        /// <returns></returns>
        [DllImport("user32.dll")]
        static public extern IntPtr GetDesktopWindow();

        /// <summary>
        /// Import SetForegroundWindow Win32 function from user32.dll
        /// </summary>
        [DllImport("user32.dll")]
        public static extern bool SetForegroundWindow(IntPtr hWnd);

        /// <summary>
        /// Pushes window to the front.  
        /// </summary>
        /// <param name="hWnd"></param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        static public extern bool BringWindowToTop(IntPtr hWnd);

        /// <summary>
        /// http://msdn.microsoft.com/en-us/library/ms646260(VS.85).aspx
        /// </summary>
        /// <param name="dwFlags"></param>
        /// <param name="dx"></param>
        /// <param name="dy"></param>
        /// <param name="dwData"></param>
        /// <param name="dwExtraInfo"></param>
        [DllImport("user32.dll")]
        public static extern void mouse_event(uint dwFlags, long dx, long dy, uint dwData, IntPtr dwExtraInfo);

        /// <summary>
        /// http://msdn.microsoft.com/en-us/library/ms646309(VS.85).aspx
        /// </summary>
        /// <param name="hWnd"></param>
        /// <param name="id"></param>
        /// <param name="modifiers"></param>
        /// <param name="vk"></param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        private static extern bool RegisterHotKey(IntPtr hWnd, int id, uint modifiers, uint vk);

        /// <summary>
        /// http://msdn.microsoft.com/en-us/library/ms646327(VS.85).aspx
        /// </summary>
        /// <param name="hWnd"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        private static extern bool UnregisterHotKey(IntPtr hWnd, int id);

        /// <summary>
        /// Import SendMessage Win32 function from user32.dll
        /// </summary>
        /// <param name="hWnd"></param>
        /// <param name="wMsg"></param>
        /// <param name="wParam"></param>
        /// <param name="lParam"></param>
        /// <returns></returns>
        [DllImport("user32.dll", EntryPoint = "SendMessage")]
        public static extern IntPtr SendMessage(IntPtr hWnd, uint wMsg, IntPtr wParam, IntPtr lParam);

        [DllImport("User32.dll", CharSet = CharSet.Unicode)]
        public static extern IntPtr GetDlgItem(IntPtr hWndDlg, Int16 Id);

        [DllImport("User32.dll", CharSet = CharSet.Unicode)]
        public static extern IntPtr GetParent(IntPtr hWnd);

        [DllImport("User32.dll", CharSet = CharSet.Unicode)]
        public static extern IntPtr SetParent(IntPtr hWndChild, IntPtr hWndNewParent);

        [DllImport("User32.dll", CharSet = CharSet.Unicode)]
        public static extern UInt32 SendMessage(IntPtr hWnd, UInt32 msg, UInt32 wParam, StringBuilder buffer);

        [DllImport("user32.dll", CharSet = CharSet.Unicode)]
        public static extern int GetWindowRect(IntPtr hWnd, ref RECT rc);

        [DllImport("user32.dll", CharSet = CharSet.Unicode)]
        public static extern int GetClientRect(IntPtr hWnd, ref RECT rc);

        [DllImport("user32.dll", CharSet = CharSet.Unicode)]
        public static extern bool ScreenToClient(IntPtr hWnd, ref POINT pt);

        [DllImport("user32.dll", CharSet = CharSet.Unicode)]
        public static extern bool MoveWindow(IntPtr hWnd, int X, int Y, int Width, int Height, bool repaint);

        [DllImport("user32.dll", CharSet = CharSet.Unicode)]
        public static extern IntPtr LoadCursorFromFile(String filename);

        [DllImport("user32.dll")]
        public extern static int EnumWindows(EnumWindowsProc lpEnumFunc, int lParam);

        [DllImport("user32.dll")]
        public extern static int EnumChildWindows(IntPtr hWndParent, EnumWindowsProc lpEnumFunc, int lParam);

        #endregion // User32.dll Functions

        #region Kernel32.dll Functions
        /// <summary>
        /// Load Win32 Library (e.g. DLL) into process address space
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        [DllImport("kernel32.dll", CharSet = CharSet.Ansi)]
        public static extern int LoadLibrary(String path);

        /// <summary>
        /// Import local heap memory allocation function from kernel32.dll
        /// </summary>
        /// <param name="flag"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        [DllImport("kernel32.dll")]
        public static extern IntPtr LocalAlloc(int flag, int size);

        /// <summary>
        /// Import local heap memory free function from kernel32.dll
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        [DllImport("kernel32.dll")]
        public static extern IntPtr LocalFree(IntPtr p);

        /// <summary>
        /// Return last Win32 error code
        /// </summary>
        /// <returns></returns>
        [DllImport("kernel32.dll", EntryPoint = "GetLastError",
                SetLastError = true, CharSet = CharSet.Auto)]
        public static extern uint GetLastError();

        #region GetWindowsDirectory Constants
        public const int MAX_PATH = 260;
        #endregion
        /// <summary>
        /// Return Windows Directory
        /// </summary>
        [DllImport("kernel32.dll", EntryPoint = "GetWindowsDirectory",
                SetLastError = true, CharSet = CharSet.Auto)]
        public static extern uint GetWindowsDirectory(String lpBuffer, uint nSize);

        /// <summary>
        /// Set environment variable
        /// </summary>
        /// <param name="lpName"></param>
        /// <param name="lpValue"></param>
        /// <returns></returns>
        [DllImport("kernel32.dll", CharSet=CharSet.Auto, SetLastError=true)]
        public static extern bool SetEnvironmentVariable(String lpName, String lpValue);
        
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [DllImport("kernel32.dll")]
        public static extern IntPtr GetCurrentProcess();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="moduleName"></param>
        /// <returns></returns>
        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr GetModuleHandle(string moduleName);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hModule"></param>
        /// <param name="procName"></param>
        /// <returns></returns>
        [DllImport("kernel32", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern IntPtr GetProcAddress(IntPtr hModule,
            [MarshalAs(UnmanagedType.LPStr)]string procName);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hProcess"></param>
        /// <param name="wow64Process"></param>
        /// <returns></returns>
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool IsWow64Process(IntPtr hProcess, out bool wow64Process);
        
        /// <summary>
        /// The function determins whether a method exists in the export 
        /// table of a certain module.
        /// </summary>
        /// <param name="moduleName">The name of the module</param>
        /// <param name="methodName">The name of the method</param>
        /// <returns>
        /// The function returns true if the method specified by methodName 
        /// exists in the export table of the module specified by moduleName.
        /// </returns>
        public static bool DoesWin32MethodExist(String moduleName, String methodName)
        {
            IntPtr moduleHandle = API.GetModuleHandle(moduleName);
            if (moduleHandle == IntPtr.Zero)
            {
                return false;
            }
            return (API.GetProcAddress(moduleHandle, methodName) != IntPtr.Zero);
        }

        #region Memory-Mapped File Access
        [DllImport("kernel32.dll")]
        public static extern IntPtr OpenFileMapping(int wDesiredAccess,
                                                    bool bInheritHandle,
                                                    String lpName);

        [DllImport("Kernel32.dll", EntryPoint = "CreateFileMapping", SetLastError = true, CharSet = CharSet.Auto)]
        public static extern IntPtr CreateFileMapping(uint hFile,
                                                      IntPtr lpAttributes, uint flProtect,
                                                      uint dwMaximumSizeHigh, uint dwMaximumSizeLow,
                                                      string lpName);

        [DllImport("Kernel32.dll")]
        public static extern IntPtr MapViewOfFile(IntPtr hFileMappingObject,
                uint dwDesiredAccess, uint dwFileOffsetHigh,
                uint dwFileOffsetLow, uint dwNumberOfBytesToMap);

        [DllImport("Kernel32.dll", EntryPoint = "UnmapViewOfFile",
                SetLastError = true, CharSet = CharSet.Auto)]
        public static extern bool UnmapViewOfFile(IntPtr lpBaseAddress);

        [DllImport("Kernel32.dll", EntryPoint = "CloseHandle",
                SetLastError = true, CharSet = CharSet.Auto)]
        public static extern bool CloseHandle(uint hHandle);
        #endregion // Memory-Mapped File Access
        #endregion // Kernel32.dll Functions

        #region Shell32.dll Functions
        #region ShellExecute Application Window Mode Constants
        // Ref: http://msdn2.microsoft.com/en-us/library/ms647732.aspx
        // E.g. for use in ShellExecute function
        public const int SW_HIDE = 0;
        public const int SW_MAXIMIZE = 3;
        public const int SW_MINIMIZE = 6;
        public const int SW_RESTORE = 9;
        public const int SW_SHOW = 5;
        public const int SW_SHOWDEFAULT = 10;
        public const int SW_SHOWMAXIMIZED = 3;
        public const int SW_SHOWMINIMIZED = 2;
        public const int SW_SHOWMINNOACTIVE = 7;
        public const int SW_SHOWNA = 8;
        public const int SW_SHOWNOACTIVATE = 4;
        public const int SW_SHOWNORMAL = 1;
        #endregion

        #region ShellExecute Error Codes
        public readonly int ERROR_OUT_OF_MEMORY = 0;
        public readonly int ERROR_FILE_NOT_FOUND = 2;
        public readonly int ERROR_PATH_NOT_FOUND = 3;
        public readonly int ERROR_BAD_FORMAT = 11;
        public readonly int SE_ERR_ACCESSDENIED = 5;
        public readonly int SE_ERR_ASSOCINCOMPLETE = 27;
        public readonly int SE_ERR_DDEBUSY = 30;
        public readonly int SE_ERR_DDEFAIL = 29;
        public readonly int SE_ERR_DDETIMEOUT = 28;
        public readonly int SE_ERR_DLLNOTFOUND = 32;
        public readonly int SE_ERR_FNF = 2;
        public readonly int SE_ERR_NOASSOC = 31;
        public readonly int SE_ERR_OOM = 8;
        public readonly int SE_ERR_PNF = 3;
        public readonly int SE_ERR_SHARE = 26;
        #endregion

        #region ShellExecuteEx Data
        public struct SHELLEXECUTEINFO
        {
            public int cbSize;
            public uint fMask;
            public IntPtr hwnd;
            public IntPtr lpVerb;
            public IntPtr lpFile;
            public IntPtr lpParameters;
            public IntPtr lpDirectory;
            public int nShow;
            public int hInstApp;
            public int lpIDList;
            public IntPtr lpClass;
            public int hkeyClass;
            public uint dwHotKey;
            public int hIcon;
            public int hProcess;
        }

        public const string VERB_OPEN = "open";
        public const string VERB_OPENAS = "openas";
        public const string VERB_PROPERTIES = "properties";
        public const string VERB_FIND = "find";
        public const string VERB_PRINT = "print";
        #endregion

        #region ShellExecute and Variants
        [DllImport("Shell32.dll", EntryPoint = "ShellExecute",
            SetLastError = true, CharSet = CharSet.Auto)]
        public static extern int ShellExecute(IntPtr hWnd, string lpOperation, string lpFile, string lpParameters, string lpDirectory, int nShowCmd);

        [DllImport("Shell32.dll", EntryPoint = "ShellExecuteEx",
            CharSet = CharSet.Auto)]
        public static extern bool ShellExecuteEx(ref SHELLEXECUTEINFO lpExecInfo);

        /// <summary>
        /// ShellExecute helper wrapper (ease marshalling of data prior to invoking method)
        /// </summary>
        /// <param name="sFile"></param>
        /// <param name="sVerb"></param>
        /// <param name="nShow"></param>
        /// <returns></returns>
        public static bool ShellExecuteHelper(String sFile, String sVerb, int nShow)
        {
            // Get ANSI strings for file and verb
            int nFileSize = (sFile.Length * 2) + 2;
            IntPtr pFileData = LocalAlloc(LMEM_ZEROINIT, nFileSize);
            int nVerbSize = (sVerb.Length * 2) + 2;
            IntPtr pVerbData = LocalAlloc(LMEM_ZEROINIT, nVerbSize);
            Marshal.Copy(Encoding.Unicode.GetBytes(sFile), 0, pFileData, nFileSize - 2);
            Marshal.Copy(Encoding.Unicode.GetBytes(sVerb), 0, pVerbData, nVerbSize - 2);

            // Initialise SHELLEXECUTEINFO structure
            SHELLEXECUTEINFO sei = new SHELLEXECUTEINFO();
            sei.cbSize = Marshal.SizeOf(sei);
            sei.lpVerb = pVerbData;
            sei.lpFile = pFileData;
            sei.nShow = nShow;

            return (ShellExecuteEx(ref sei));
        }
        #endregion // ShellExecute and Variants

        #region SHGetFileInfo
        [StructLayout(LayoutKind.Sequential)]
        public struct SHFILEINFO
        {
            public IntPtr hIcon;
            public IntPtr iIcon;
            public uint dwAttributes;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 260)]
            public String szDisplayName;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 80)]
            public String szTypeName;
        }

        public const uint FILE_ATTRIBUTE_DIRECTORY = 0x10;
        public const uint FILE_ATTRIBUTE_NORMAL = 0x80;

        [Flags]
        public enum SHGFI : int
        {
            /// <summary>get icon</summary>
            Icon = 0x000000100,
            /// <summary>get display name</summary>
            DisplayName = 0x000000200,
            /// <summary>get type name</summary>
            TypeName = 0x000000400,
            /// <summary>get attributes</summary>
            Attributes = 0x000000800,
            /// <summary>get icon location</summary>
            IconLocation = 0x000001000,
            /// <summary>return exe type</summary>
            ExeType = 0x000002000,
            /// <summary>get system icon index</summary>
            SysIconIndex = 0x000004000,
            /// <summary>put a link overlay on icon</summary>
            LinkOverlay = 0x000008000,
            /// <summary>show icon in selected state</summary>
            Selected = 0x000010000,
            /// <summary>get only specified attributes</summary>
            Attr_Specified = 0x000020000,
            /// <summary>get large icon</summary>
            LargeIcon = 0x000000000,
            /// <summary>get small icon</summary>
            SmallIcon = 0x000000001,
            /// <summary>get open icon</summary>
            OpenIcon = 0x000000002,
            /// <summary>get shell size icon</summary>
            ShellIconSize = 0x000000004,
            /// <summary>pszPath is a pidl</summary>
            PIDL = 0x000000008,
            /// <summary>use passed dwFileAttribute</summary>
            UseFileAttributes = 0x000000010,
            /// <summary>apply the appropriate overlays</summary>
            AddOverlays = 0x000000020,
            /// <summary>Get the index of the overlay in the upper 8 bits of the iIcon</summary>
            OverlayIndex = 0x000000040,
        }   

        [DllImport("shell32.dll")]
        public static extern IntPtr SHGetFileInfo(String pszPath, uint dwFileAttributes, ref SHFILEINFO psfi, uint cbSizeFileInfo, uint uFlags);

        // Cache data
        static String lastSmallPath = String.Empty;
        static String lastLargePath = String.Empty;
        static String lastDescPath = String.Empty;
        static SHFILEINFO shellInfoSmall = new SHFILEINFO();
        static SHFILEINFO shellInfoLarge = new SHFILEINFO();
        static SHFILEINFO shellInfoDesc = new SHFILEINFO();
        
        // Friendly wrappers.
        public static SHFILEINFO GetFileInfoSmallIcon(String filePath)
        {            
            // Cache results, only call SHGetFileInfo for a different file extension.
            if (System.IO.Path.GetExtension(lastSmallPath) != System.IO.Path.GetExtension(filePath))
            {
                SHGetFileInfo(filePath, API.FILE_ATTRIBUTE_NORMAL, ref shellInfoSmall, (uint)Marshal.SizeOf(shellInfoSmall), (uint)(SHGFI.Icon | SHGFI.SmallIcon | SHGFI.TypeName | SHGFI.UseFileAttributes));
                lastSmallPath = filePath;
            }
            return (shellInfoSmall);
        }

        public static String GetFileDescription(String filePath)
        {
            // Cache results, only call SHGetFileInfo for a different file extension.
            if (System.IO.Path.GetExtension(lastDescPath) != System.IO.Path.GetExtension(filePath))
            {
                SHGetFileInfo(filePath, API.FILE_ATTRIBUTE_NORMAL, ref shellInfoDesc, (uint)Marshal.SizeOf(shellInfoDesc), (uint)(SHGFI.TypeName | SHGFI.UseFileAttributes));
                lastDescPath = filePath;
            }
            return (shellInfoDesc.szTypeName);
        }

        public static SHFILEINFO GetFileInfoLargeIcon(String filePath)
        {
            // Cache results, only call SHGetFileInfo for a different file extension.
            if (System.IO.Path.GetExtension(lastLargePath) != System.IO.Path.GetExtension(filePath))
            {
                SHGetFileInfo(filePath, API.FILE_ATTRIBUTE_NORMAL, ref shellInfoLarge, (uint)Marshal.SizeOf(shellInfoLarge), (uint)(SHGFI.Icon | SHGFI.LargeIcon | SHGFI.TypeName | SHGFI.UseFileAttributes));
                lastLargePath = filePath;
            }
            return (shellInfoLarge);
        }

        public static API.SHFILEINFO GetDirectoryInfoSmallIcon()
        {
            SHGetFileInfo("Doesnt Matter", FILE_ATTRIBUTE_DIRECTORY, ref shellInfoSmall, (uint)Marshal.SizeOf(shellInfoSmall), (uint)(SHGFI.Icon | SHGFI.SmallIcon | SHGFI.TypeName | SHGFI.UseFileAttributes));
            return (shellInfoSmall);
        }

        public static API.SHFILEINFO GetDirectoryInfoLargeIcon()
        {
            SHGetFileInfo("Doesnt Matter", FILE_ATTRIBUTE_DIRECTORY, ref shellInfoLarge, (uint)Marshal.SizeOf(shellInfoLarge), (uint)(SHGFI.Icon | SHGFI.LargeIcon | SHGFI.TypeName | SHGFI.UseFileAttributes));
            return (shellInfoLarge);
        }
        #endregion // SHGetFileInfo

        #region Shell Notify Enumerations
        [Flags]
        public enum etShellChangeNotifyEvents : uint
        {
            /// <summary>
            /// The name of a non-folder item has changed. SHCNF_IDLIST or 
            /// SHCNF_PATH must be specified in uFlags. dwItem1 contains the  
            /// previous PIDL or name of the item. dwItem2 contains the new 
            /// PIDL or name of the item. 
            /// </summary>
            SHCNE_RENAMEITEM = 0x00000001,
            /// <summary>
            /// A non-folder item has been created. SHCNF_IDLIST or SHCNF_PATH 
            /// must be specified in uFlags. dwItem1 contains the item that 
            /// was created. dwItem2 is not used and should be NULL.
            /// </summary>
            SHCNE_CREATE = 0x00000002,

            /// <summary>
            /// A non-folder item has been deleted. SHCNF_IDLIST or SHCNF_PATH 
            /// must be specified in uFlags. dwItem1 contains the item that 
            /// was deleted. dwItem2 is not used and should be NULL.
            /// </summary>
            SHCNE_DELETE = 0x00000004,

            /// <summary>
            /// A folder has been created. SHCNF_IDLIST or SHCNF_PATH must be 
            /// specified in uFlags. dwItem1 contains the folder that was 
            /// created. dwItem2 is not used and should be NULL.
            /// </summary>
            SHCNE_MKDIR = 0x00000008,

            /// <summary>
            /// A folder has been removed. SHCNF_IDLIST or SHCNF_PATH must be 
            /// specified in uFlags. dwItem1 contains the folder that was 
            /// removed. dwItem2 is not used and should be NULL.
            /// </summary>
            SHCNE_RMDIR = 0x00000010,

            /// <summary>
            /// Storage media has been inserted into a drive. SHCNF_IDLIST or 
            /// SHCNF_PATH must be specified in uFlags. dwItem1 contains the 
            /// root of the drive that contains the new media. dwItem2 is 
            /// not used and should be NULL.
            /// </summary>
            SHCNE_MEDIAINSERTED = 0x00000020,

            /// <summary>
            /// Storage media has been removed from a drive. SHCNF_IDLIST or 
            /// SHCNF_PATH must be specified in uFlags. dwItem1 contains the 
            /// root of the drive from which the media was removed. dwItem2 
            /// is not used and should be NULL.
            /// </summary>
            SHCNE_MEDIAREMOVED = 0x00000040,

            /// <summary>
            /// A drive has been removed. SHCNF_IDLIST or SHCNF_PATH must be 
            /// specified in uFlags. dwItem1 contains the root of the drive 
            /// that was removed. dwItem2 is not used and should be NULL.
            /// </summary>
            SHCNE_DRIVEREMOVED = 0x00000080,

            /// <summary>
            /// A drive has been added. SHCNF_IDLIST or SHCNF_PATH must be 
            /// specified in uFlags. dwItem1 contains the root of the drive 
            /// that was added. dwItem2 is not used and should be NULL.
            /// </summary>
            SHCNE_DRIVEADD = 0x00000100,

            /// <summary>
            /// A folder on the local computer is being shared via the network. 
            /// SHCNF_IDLIST or SHCNF_PATH must be specified in uFlags. dwItem1 
            /// contains the folder that is being shared. dwItem2 is not used 
            /// and should be NULL.
            /// </summary>
            SHCNE_NETSHARE = 0x00000200,

            /// <summary>
            /// A folder on the local computer is no longer being shared via 
            /// the network. SHCNF_IDLIST or SHCNF_PATH must be specified in 
            /// uFlags. dwItem1 contains the folder that is no longer being 
            /// shared. dwItem2 is not used and should be NULL.
            /// </summary>
            SHCNE_NETUNSHARE = 0x00000400,

            /// <summary>
            /// The attributes of an item or folder have changed. SHCNF_IDLIST 
            /// or SHCNF_PATH must be specified in uFlags. dwItem1 contains 
            /// the item or folder that has changed. dwItem2 is not used and 
            /// should be NULL.
            /// </summary>
            SHCNE_ATTRIBUTES = 0x00000800,

            /// <summary>
            /// The contents of an existing folder have changed, but the folder 
            /// still exists and has not been renamed. SHCNF_IDLIST or 
            /// SHCNF_PATH must be specified in uFlags. dwItem1 contains the 
            /// folder that has changed. dwItem2 is not used and should be NULL. 
            /// If a folder has been created, deleted, or renamed, use 
            /// SHCNE_MKDIR, SHCNE_RMDIR, or SHCNE_RENAMEFOLDER, respectively, 
            /// instead.
            /// </summary>
            SHCNE_UPDATEDIR = 0x00001000,

            /// <summary>
            /// An existing non-folder item has changed, but the item still 
            /// exists and has not been renamed. SHCNF_IDLIST or SHCNF_PATH 
            /// must be specified in uFlags. dwItem1 contains the item that 
            /// has changed. dwItem2 is not used and should be NULL. If a 
            /// non-folder item has been created, deleted, or renamed, 
            /// use SHCNE_CREATE, SHCNE_DELETE, or SHCNE_RENAMEITEM, 
            /// respectively, instead.
            /// </summary>
            SHCNE_UPDATEITEM = 0x00002000,

            /// <summary>
            /// The computer has disconnected from a server. SHCNF_IDLIST or 
            /// SHCNF_PATH must be specified in uFlags. dwItem1 contains the 
            /// server from which the computer was disconnected. dwItem2 is 
            /// not used and should be NULL.
            /// </summary>
            SHCNE_SERVERDISCONNECT = 0x00004000,

            /// <summary>
            /// An image in the system image list has changed. SHCNF_DWORD 
            /// must be specified in uFlags. dwItem1 contains the index in 
            /// the system image list that has changed. dwItem2 is not used 
            /// and should be NULL.
            /// </summary>
            SHCNE_UPDATEIMAGE = 0x00008000,

            /// <summary>
            /// A drive has been added and the Shell should create a new 
            /// window for the drive. SHCNF_IDLIST or SHCNF_PATH must be 
            /// specified in uFlags. dwItem1 contains the root of the drive 
            /// that was added. dwItem2 is not used and should be NULL.
            /// </summary>
            SHCNE_DRIVEADDGUI = 0x00010000,

            /// <summary>
            /// The name of a folder has changed. SHCNF_IDLIST or SHCNF_PATH 
            /// must be specified in uFlags. dwItem1 contains the previous 
            /// pointer to an item identifier list (PIDL) or name of the 
            /// folder. dwItem2 contains the new PIDL or name of the folder.
            /// </summary>
            SHCNE_RENAMEFOLDER = 0x00020000,

            /// <summary>
            /// The amount of free space on a drive has changed. SHCNF_IDLIST 
            /// or SHCNF_PATH must be specified in uFlags. dwItem1 contains 
            /// the root of the drive on which the free space changed. 
            /// dwItem2 is not used and should be NULL.
            /// </summary>
            SHCNE_FREESPACE = 0x00040000,

            /// <summary>
            /// Not currently used.
            /// </summary>
            SHCNE_EXTENDED_EVENT = 0x04000000,

            /// <summary>
            /// A file type association has changed. SHCNF_IDLIST must be 
            /// specified in the uFlags parameter. dwItem1 and dwItem2 are
            /// not used and must be NULL.
            /// </summary>
            SHCNE_ASSOCCHANGED = 0x08000000,

            /// <summary>
            /// Specifies a combination of all of the disk event identifiers.
            /// </summary>
            SHCNE_DISKEVENTS = 0x0002381F,

            /// <summary>
            /// Specifies a combination of all of the global event identifiers. 
            /// </summary>
            SHCNE_GLOBALEVENTS = 0x0C0581E0,

            /// <summary>
            /// All events have occurred.
            /// </summary>
            SHCNE_ALLEVENTS = 0x7FFFFFFF,

            /// <summary>
            /// The specified event occurred as a result of a system interrupt.
            /// As this value modifies other event values, it cannot be used 
            /// alone.
            /// </summary>
            SHCNE_INTERRUPT = 0x80000000
        }

        public enum etShellChangeNotifyFlags
        {
            /// <summary>
            /// dwItem1 and dwItem2 are the addresses of ITEMIDLIST structures 
            /// that represent the item(s) affected by the change. Each 
            /// ITEMIDLIST must be relative to the desktop folder. 
            /// </summary>
            SHCNF_IDLIST = 0x0000,

            /// <summary>
            /// dwItem1 and dwItem2 are the addresses of null-terminated strings 
            /// of maximum length MAX_PATH that contain the full path names of 
            /// the items affected by the change.
            /// </summary>
            SHCNF_PATHA = 0x0001,

            /// <summary>
            /// dwItem1 and dwItem2 are the addresses of null-terminated strings 
            /// that represent the friendly names of the printer(s) affected by 
            /// the change.
            /// </summary>
            SHCNF_PRINTERA = 0x0002,

            /// <summary>
            /// The dwItem1 and dwItem2 parameters are DWORD values.
            /// </summary>
            SHCNF_DWORD = 0x0003,

            /// <summary>
            /// like SHCNF_PATHA but unicode string
            /// </summary>
            SHCNF_PATHW = 0x0005,

            /// <summary>
            /// like SHCNF_PRINTERA but unicode string
            /// </summary>
            SHCNF_PRINTERW = 0x0006,

            /// <summary>
            /// 
            /// </summary>
            SHCNF_TYPE = 0x00FF,

            /// <summary>
            /// The function should not return until the notification has been 
            /// delivered to all affected components. As this flag modifies 
            /// other data-type flags, it cannot by used by itself.
            /// </summary>
            SHCNF_FLUSH = 0x1000,

            /// <summary>
            /// The function should begin delivering notifications to all 
            /// affected components but should return as soon as the 
            /// notification process has begun. As this flag modifies other 
            /// data-type flags, it cannot by used  by itself.
            /// </summary>
            SHCNF_FLUSHNOWAIT = 0x2000
        }
        #endregion

        /// <summary>
        /// Notify the System of an shell event that the Application has performed
        /// </summary>
        /// An application should use this function if it performs an action that 
        /// may affect the Shell, e.g. change file associations, rename files etc.
        /// <param name="wEventId">Describes event that has occurred, see etShellChangeNotifyEvents.</param>
        /// <param name="uFlags">Flags that indicate the meaning of the dwItem1 and dwItem2 parameter.</param>
        /// <param name="dwItem1">First event-dependent value.</param>
        /// <param name="dwItem2">Second event-dependent value.</param>
        [DllImport("shell32.dll", EntryPoint = "SHChangeNotify",
            CharSet = CharSet.Auto)]
        public static extern void SHChangeNotify(UInt32 wEventId, UInt32 uFlags,
                                                 IntPtr dwItem1, IntPtr dwItem2);
        #endregion // Shell32.dll Functions

        #region ComDlg32.dll Functions
        [DllImport("ComDlg32.dll", CharSet = CharSet.Unicode)]
        public static extern Int32 CommDlgExtendedError();

        [DllImport("ComDlg32.dll", CharSet = CharSet.Unicode)]
        public static extern bool ChooseColor(ref ChooseColor cc);
        #endregion // ComDlg32.dll Functions
    }

} // End of RSG.Base.Win32 namespace

// End of file
