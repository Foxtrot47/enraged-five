//
// File: cRegistry.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of cRegistry class
//

using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;

namespace RSG.Base.Win32
{

    /// <summary>
    /// Simple interface for reading/writing and deleting keys from Windows Registry.
    /// </summary>
    /// This requires the Microsoft.Win32 namespace.
    [Obsolete("Old and not required; don't think we need a replacement.", true)]
    public class cRegistry
    {

        /// <summary>
        /// Return a value from a Registry key
        /// </summary>
        /// <param name="root"></param>
        /// <param name="sKey"></param>
        /// <param name="sValueName"></param>
        /// <returns></returns>
        /// <example></example>
        /// <exception cref="exRegistryAccessException">Thrown if sKey malformed or sKey does not exist</exception>
        public static object Read(RegistryKey root, String sKey, String sValueName)
        {
            String[] asParts = null;
            if (sKey.Contains("/"))
                asParts = sKey.Split('/');
            else if (sKey.Contains("\\"))
                asParts = sKey.Split('\\');

            if ((null == asParts) || (0 == asParts.Length))
                throw new exRegistryAccessException("Malformed registry key, use back or forward slashes for separating folders.");

            RegistryKey key = root;
            for (uint nPart = 0; nPart < asParts.Length; ++nPart)
            {
                key = key.OpenSubKey(asParts[nPart]);

                if (null == key)
                    throw new exRegistryAccessException("Registry key does not exist.");

                if ((asParts.Length - 1) == nPart)
                {
                    return key.GetValue(sValueName, null, RegistryValueOptions.DoNotExpandEnvironmentNames);
                }
            }

            throw new exRegistryAccessException("Registry key does not exist.");
        }

        /// <summary>
        /// Write a value to a Registry key
        /// </summary>
        /// <param name="root"></param>
        /// <param name="sKey"></param>
        /// <param name="sValueName"></param>
        /// <param name="value"></param>
        /// <example></example>
        /// <exception cref="exRegistryAccessException">Thrown if sKey malformed</exception>
        public static void Write(RegistryKey root, String sKey, String sValueName, object value)
        {
            String[] asParts = null;
            if (sKey.Contains("/"))
                asParts = sKey.Split('/');
            else if (sKey.Contains("\\"))
                asParts = sKey.Split('\\');

            if ((null == asParts) || (0 == asParts.Length))
                throw new exRegistryAccessException("Malformed registry key, use back or forward slashes for separating folders.");

            RegistryKey key = root;
            RegistryKey lastKey = key;
            for (uint nPart = 0; nPart < asParts.Length; ++nPart)
            {
                key = key.OpenSubKey(asParts[nPart], true);
                if (null == key)
                    key = lastKey.CreateSubKey(asParts[nPart]);
                
                if ((asParts.Length - 1) == nPart)
                {
                    if (value is String)
                    {
                        key.SetValue(sValueName, value.ToString());
                    }
                    else if (value is Guid)
                    {
                        key.SetValue(sValueName, ((Guid)value).ToString("B"));
                    }
                    else if ((value is uint) || value.GetType().IsEnum)
                    {
                        object obj = key.GetValue(sValueName, null);
                        if (null == obj)
                        {
                            key.SetValue(sValueName, value, RegistryValueKind.DWord);
                        }
                        else
                        {
                            RegistryValueKind kind = key.GetValueKind(sValueName);

                            if (kind == RegistryValueKind.DWord)
                            {
                                key.SetValue(sValueName, value, RegistryValueKind.DWord);
                            }
                            else if (kind == RegistryValueKind.Binary)
                            {
                                uint num = (uint)value;

                                byte[] b = new byte[4];
                                b[0] = (byte)((num & 0x000000FF) >> 0);
                                b[1] = (byte)((num & 0x0000FF00) >> 1);
                                b[2] = (byte)((num & 0x00FF0000) >> 2);
                                b[3] = (byte)((num & 0xFF000000) >> 3);


                                b[0] = (byte)((num & 0x000000FF) >> 0);
                                b[1] = (byte)((num & 0x0000FF00) >> 8);
                                b[2] = (byte)((num & 0x00FF0000) >> 16);
                                b[3] = (byte)((num & 0xFF000000) >> 24);

                                key.SetValue(sValueName, b, RegistryValueKind.Binary);
                            }
                            else if (kind == RegistryValueKind.String)
                            {
                                key.SetValue(sValueName, "x" + ((uint)value).ToString("X8"));
                            }
                        }
                    }
                }

                lastKey = key;
            }

            if (null != key)
                key.Close();
        }
        
        /// <summary>
        /// Delete a value from a Registry key
        /// </summary>
        /// <param name="root"></param>
        /// <param name="sKey"></param>
        /// <param name="sValueName"></param>
        /// <example></example>
        /// <exception cref="exRegistryAccessException">Thrown if sKey malformed</exception>
        public static void Delete(RegistryKey root, String sKey, String sValueName)
        {
            String[] asParts = null;
            if (sKey.Contains("/"))
                asParts = sKey.Split('/');
            else if (sKey.Contains("\\"))
                asParts = sKey.Split('\\');

            if ((null == asParts) || (0 == asParts.Length))
                throw new exRegistryAccessException("Malformed registry key, use back or forward slashes for separating folders.");

            RegistryKey key = root;
            for (uint nPart = 0; nPart < asParts.Length; ++nPart)
            {
                key = key.OpenSubKey(asParts[nPart], true);

                if (null == key)
                    throw new exRegistryAccessException("Registry key does not exist.");

                if ((asParts.Length - 1) == nPart)
                {
                    key.DeleteValue(sValueName, false);
                }
            }
        }
    }

    #region Exceptions
    /// <summary>
    /// Registry Access Exception
    /// </summary>
    /// This exception is thrown by the cRegistry class on any registry access 
    /// errors.
    public class exRegistryAccessException : Exception
    {
        #region Constructors
        public exRegistryAccessException()
            : base()
        {
        }

        public exRegistryAccessException(String sMessage)
            : base(sMessage)
        {
        }

        public exRegistryAccessException(String sMessage, Exception InnerException)
            : base(sMessage, InnerException)
        {
        }

       public exRegistryAccessException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
            : base(info, context)
        {
        }
        #endregion
    }
    #endregion

} // End of RSG.Base.Win32 namespace

// End of file
