﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace RSG.Base.Win32
{
    /// <summary>
    /// Reads an old Windows style INI file.
    /// </summary>
    [Obsolete("Use RSG.Base.IO.IniFile instead.")]
    public class IniFile
    {
        #region Key value list class

        /// <summary>
        /// Key value list class.
        /// </summary>
        public class KeyValueList : List<KeyValuePair<string, string>>
        {

        }

        #endregion
        
        #region Static file creation / open methods

        /// <summary>
        /// Open or create a new Ini file.
        /// </summary>
        /// <param name="filename">Ini file path.</param>
        /// <returns>The instance of an Ini file.</returns>
        public static IniFile Open(string filename)
        {
            IniFile iniFile = new IniFile(filename);

            if (File.Exists(filename))
            {
                using (Stream s = File.Open(filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    KeyValueList kvpList = null;

                    using (TextReader reader = new StreamReader(s))
                    {
                        string line = String.Empty;
                        while ((line = reader.ReadLine()) != null)
                        {
                            if (line.StartsWith("["))
                            {
                                string sectionName = line.Replace("[", "").Replace("]", "");
                                iniFile.CreateSection(sectionName);
                                kvpList = iniFile.GetList(sectionName);
                            }
                            else
                            {
                                if (kvpList != null)
                                {
                                    string[] splitter = line.Split("=".ToCharArray(), 2);
                                    string key = splitter[0];
                                    string value = splitter[1];
                                    kvpList.Add(new KeyValuePair<string, string>(key, value));
                                }
                            }
                        }
                    }
                }
            }

            return iniFile;
        }

        /// <summary>
        /// Try and open the filename and read its contents. If it isn't possible to open the Ini file,
        /// a blank instance of IniFile will be returned.
        /// </summary>
        /// <param name="filename">Filename.</param>
        /// <returns>An instance of IniFile.</returns>
        public static IniFile TryOpen(string filename)
        {
            IniFile iniFile = null;

            try
            {
                iniFile = Open(filename);
            }
            catch
            {
                iniFile = new IniFile(filename);
            }

            return iniFile;
        }

        #endregion

        #region Public properties

        /// <summary>
        /// Checks each key within each section and returns true if at least one key has a null string for a value.
        /// </summary>
        public bool HasBlankEntries
        {
            get
            {
                if (m_iniFile.Count == 0)
                {
                    return true;
                }

                bool hasblankEntries = false;

                foreach(string key in m_iniFile.Keys)
                {
                    var kvp = m_iniFile[key].Where(strKvp => String.IsNullOrEmpty(strKvp.Value));
                    if (kvp.Any())
                    {
                        hasblankEntries = true;
                        break;
                    }
                }

                return hasblankEntries;
            }
        }

        /// <summary>
        /// The Ini file name.
        /// </summary>
        public string Filename
        {
            get { return m_filename; }
            set { m_filename = value; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Private constructor to force users to call the Open() static method.
        /// </summary>
        /// <param name="filename">File name.</param>
        private IniFile(string filename)
        {
            m_filename = filename;
            m_iniFile = new Dictionary<string, KeyValueList>();
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Create a section.
        /// </summary>
        /// <param name="sectionName"></param>
        /// <returns></returns>
        public bool CreateSection(string sectionName)
        {
            if (m_iniFile.ContainsKey(sectionName))
            {
                return false;
            }

            m_iniFile.Add(sectionName, new KeyValueList());
            return true;
        }

        /// <summary>
        /// Get the value for a particular section and entry key.
        /// </summary>
        /// <param name="sectionName">Section name.</param>
        /// <param name="entryKey">Entry key.</param>
        /// <returns></returns>
        public string GetValue(string sectionName, string entryKey)
        {
            if (m_iniFile.ContainsKey(sectionName))
            {
                var value = m_iniFile[sectionName].Find(kvp => kvp.Key.Equals(entryKey));
                return value.Value;
            }

            return String.Empty;
        }

        /// <summary>
        /// Get all the entries under a particular section.
        /// </summary>
        /// <param name="sectionName">Section name.</param>
        /// <returns>The enumeration of entries underneath the given section.</returns>
        public IEnumerable<KeyValuePair<string, string>> GetEntries(string sectionName)
        {
            if (m_iniFile.ContainsKey(sectionName))
            {
                foreach (KeyValuePair<string, string> kvl in m_iniFile[sectionName])
                {
                    yield return kvl;
                }
            }
        }

        #endregion

        #region Private helper methods

        /// <summary>
        /// Get the key value list for a particular section.
        /// </summary>
        /// <param name="sectionName">Section name.</param>
        /// <returns>The key value list owned by the section.</returns>
        private KeyValueList GetList(string sectionName)
        {
            return m_iniFile[sectionName];
        }

        #endregion

        #region Private member fields

        private Dictionary<string, KeyValueList> m_iniFile;
        private string m_filename;

        #endregion
    }
}

