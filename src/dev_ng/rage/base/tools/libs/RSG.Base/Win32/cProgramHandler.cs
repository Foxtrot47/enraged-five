//
// File: cProgramHandler.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of cProgramHandler class
//

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using Microsoft.Win32;

using RSG.Base.Collections;

namespace RSG.Base.Win32
{
    
    /// <summary>
    /// 
    /// </summary>
    [Obsolete("Use RSG.Microsoft.Interop.FileAssociation.", true)]
    public class cProgramHandler
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Name
        /// </summary>
        public String Name
        {
            get { return m_sName; }
            private set { m_sName = value; }
        }
        private String m_sName;

        /// <summary>
        /// Description
        /// </summary>
        public String Description
        {
            get { return m_sDescription; }
            set { m_sDescription = value; }
        }
        private String m_sDescription;

        /// <summary>
        /// Handler's Icon
        /// </summary>
        public Icon Icon
        {
            get { return m_Icon; }
            set { m_Icon = value; }
        }
        private Icon m_Icon;

        /// <summary>
        /// Handler's shell commands
        /// </summary>
        public List<Pair<String, String>> ShellCommands
        {
            get { return m_ShellCommands; }
            set { m_ShellCommands = value; }
        }
        private List<Pair<String, String>> m_ShellCommands;
        #endregion

        #region Constructors
        /// <summary>
        /// Instantiate a Program Handler object from the Registry handler
        /// information.
        /// </summary>
        /// <param name="sName"></param>
        /// Changes are not automatically entered into the Windows Registry.
        public cProgramHandler(String sName)
        {
            LoadFromRegistry(sName);
        }

        /// <summary>
        /// Instantiate a new Program Handler
        /// </summary>
        /// <param name="sName"></param>
        /// <param name="sDescription"></param>
        /// This is not automatically entered into the Windows Registry.
        public cProgramHandler(String sName, String sDescription)
        {
            Init(sName, sDescription, null, null);
        }

        /// <summary>
        /// Instantiate a new Program Handler
        /// </summary>
        /// <param name="sName"></param>
        /// <param name="sDescription"></param>
        /// <param name="icon"></param>
        /// This is not automatically entered into the Windows Registry.
        public cProgramHandler(String sName, String sDescription, Icon icon)
        {
            Init(sName, sDescription, icon, null);
        }

        /// <summary>
        /// Instantiate a new Program Handler
        /// </summary>
        /// <param name="sName"></param>
        /// <param name="sDescription"></param>
        /// <param name="icon"></param>
        /// <param name="commands"></param>
        public cProgramHandler(String sName, String sDescription, Icon icon, List<Pair<String, String>> commands)
        {
            Init(sName, sDescription, icon, commands);
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Save the Program Handler to the Windows Registry
        /// </summary>
        public void SaveToRegistry()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Initialise from Windows Registry
        /// </summary>
        /// <param name="sName"></param>
        private void LoadFromRegistry(String sName)
        {
            RegistryKey keyName = Registry.ClassesRoot.OpenSubKey(sName);
            if (null == keyName)
                throw new exRegistryAccessException(String.Format("Key {0} does not exist in Handler Registry information.", sName));

            Name = sName;
            Description = (String)(keyName.GetValue(String.Empty));

            // Default icon
            RegistryKey keyIcon = keyName.OpenSubKey("DefaultIcon");
            if (null != keyIcon)
            {
                String sIconPath = (String)keyIcon.GetValue(String.Empty);
                sIconPath = sIconPath.Replace("\"", String.Empty);
                if (System.IO.File.Exists(sIconPath) && sIconPath.EndsWith(".ico"))
                    Icon = new Icon(sIconPath);
                keyIcon.Close();
            }

            keyName.Close();
        }

        /// <summary>
        /// Initialise from user-specified data
        /// </summary>
        /// <param name="sName"></param>
        /// <param name="sDescription"></param>
        /// <param name="icon"></param>
        /// <param name="commands"></param>
        private void Init(String sName, String sDescription, Icon icon, List<Pair<String, String>> commands)
        {
            Name = sName;
            Description = sDescription;
            Icon = icon;
            ShellCommands = commands;
        }
        #endregion
    }

} // End of RSG.Base.Win32 namespace

// End of file
