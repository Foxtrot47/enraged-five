﻿using System;
using System.Runtime.InteropServices;
using System.Text;
using Microsoft.Win32;

namespace RSG.Base.Win32
{

    /// <summary>
    /// An extension class to allow a registry key to allow it to get the
    /// registry in the 32 bit (Wow6432Node) or 64 bit regular registry key
    /// </summary>
    /// http://www.rhyous.com/2011/01/24/how-read-the-64-bit-registry-from-a-32-bit-application-or-vice-versa/
    /// 
    [Obsolete("Old and not required; don't think we need a replacement.", true)]
    public static class RegistryWOW6432
    {
        #region Enumerations
        /// <summary>
        /// 
        /// </summary>
        public enum RegSAM
        {
            QueryValue = 0x0001,
            SetValue = 0x0002,
            CreateSubKey = 0x0004,
            EnumerateSubKeys = 0x0008,
            Notify = 0x0010,
            CreateLink = 0x0020,
            WOW64_32Key = 0x0200,
            WOW64_64Key = 0x0100,
            WOW64_Res = 0x0300,
            Read = 0x00020019,
            Write = 0x00020006,
            Execute = 0x00020019,
            AllAccess = 0x000f003f
        }
        #endregion // Enumerations

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="inKey"></param>
        /// <param name="inPropertyName"></param>
        /// <returns></returns>
        public static string GetRegKey64(this RegistryKey inKey, String inPropertyName)
        {
            string strKey = inKey.ToString();
            string regHive = strKey.Split('\\')[0];
            string regPath = strKey.Substring(strKey.IndexOf('\\') + 1);
            return GetRegKey64(GetRegHiveFromString(regHive), regPath, RegSAM.WOW64_64Key, inPropertyName);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inKey"></param>
        /// <param name="inPropertyName"></param>
        /// <returns></returns>
        public static string GetRegKey32(this RegistryKey inKey, String inPropertyName)
        {
            string strKey = inKey.ToString();
            string regHive = strKey.Split('\\')[0];
            string regPath = strKey.Substring(strKey.IndexOf('\\') + 1);
            return GetRegKey64(GetRegHiveFromString(regHive), regPath, RegSAM.WOW64_32Key, inPropertyName);
        }

        static public string GetRegKey64(UIntPtr inHive, String inKeyName, RegSAM in32or64key, String inPropertyName)
        {
            //UIntPtr HKEY_LOCAL_MACHINE = (UIntPtr)0x80000002;
            int hkey = 0;

            try
            {
                uint lResult = API.RegOpenKeyEx(inHive, inKeyName, 0, (int)RegSAM.QueryValue | (int)in32or64key, out hkey);
                if (0 != lResult) return null;
                uint lpType = 0;
                uint lpcbData = 1024;
                StringBuilder AgeBuffer = new StringBuilder(1024);
                API.RegQueryValueEx(hkey, inPropertyName, 0, ref lpType, AgeBuffer, ref lpcbData);
                string Age = AgeBuffer.ToString();
                return Age;
            }
            finally
            {
                if (0 != hkey) API.RegCloseKey(hkey);
            }
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="inString"></param>
        /// <returns></returns>
        private static UIntPtr GetRegHiveFromString(String inString)
        {
            if (0 == String.Compare(inString, "HKEY_LOCAL_MACHINE", true))
                return API.HKEY_LOCAL_MACHINE;
            if (0 == String.Compare(inString, "HKEY_CURRENT_USER", true))
                return API.HKEY_CURRENT_USER;
            return UIntPtr.Zero;
        }
        #endregion // Private Methods
    }

} // RSG.Base.Win32 namespace
