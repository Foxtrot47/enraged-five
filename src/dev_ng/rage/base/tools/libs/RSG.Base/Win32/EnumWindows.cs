﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using System.Text;

namespace RSG.Base.Win32
{
    #region EnumWindows
    /// <summary>
    /// EnumWindows wrapper for .NET
    /// </summary>
    public class EnumWindows
    {
        #region Delegates
        private delegate int EnumWindowsProc(IntPtr hwnd, int lParam);
        #endregion

        #region Member Variables
        private EnumWindowsCollection items = null;
        #endregion

        /// <summary>
        /// Returns the collection of windows returned by
        /// GetWindows
        /// </summary>
        public EnumWindowsCollection Items
        {
            get
            {
                return this.items;
            }
        }

        /// <summary>
        /// Gets all top level windows on the system.
        /// </summary>
        public void GetWindows()
        {
            this.items = new EnumWindowsCollection();
            API.EnumWindows(new API.EnumWindowsProc(this.WindowEnum), 0);
        }
        /// <summary>
        /// Gets all child windows of the specified window
        /// </summary>
        /// <param name="hWndParent">Window Handle to get children for</param>
        public void GetWindows(
            IntPtr hWndParent)
        {
            this.items = new EnumWindowsCollection();
            API.EnumChildWindows(hWndParent, new API.EnumWindowsProc(this.WindowEnum), 0);
        }

        #region EnumWindows callback
        /// <summary>
        /// The enum Windows callback.
        /// </summary>
        /// <param name="hWnd">Window Handle</param>
        /// <param name="lParam">Application defined value</param>
        /// <returns>1 to continue enumeration, 0 to stop</returns>
        private int WindowEnum(
            IntPtr hWnd,
            int lParam)
        {
            if (this.OnWindowEnum(hWnd))
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
        #endregion

        /// <summary>
        /// Called whenever a new window is about to be added
        /// by the Window enumeration called from GetWindows.
        /// If overriding this function, return true to continue
        /// enumeration or false to stop.  If you do not call
        /// the base implementation the Items collection will
        /// be empty.
        /// </summary>
        /// <param name="hWnd">Window handle to add</param>
        /// <returns>True to continue enumeration, False to stop</returns>
        protected virtual bool OnWindowEnum(
            IntPtr hWnd)
        {
            items.Add(hWnd);
            return true;
        }

        #region Constructor, Dispose
        public EnumWindows()
        {
            // nothing to do
        }
        #endregion
    }
    #endregion EnumWindows

    #region EnumWindowsCollection
    /// <summary>
    /// Holds a collection of Windows returned by GetWindows.
    /// </summary>
    public class EnumWindowsCollection : ReadOnlyCollectionBase
    {
        /// <summary>
        /// Add a new Window to the collection.  Intended for
        /// internal use by EnumWindows only.
        /// </summary>
        /// <param name="hWnd">Window handle to add</param>
        public void Add(IntPtr hWnd)
        {
            EnumWindowsItem item = new EnumWindowsItem(hWnd);
            this.InnerList.Add(item);
        }

        /// <summary>
        /// Gets the Window at the specified index
        /// </summary>
        public EnumWindowsItem this[int index]
        {
            get
            {
                return (EnumWindowsItem)this.InnerList[index];
            }
        }

        /// <summary>
        /// Constructs a new EnumWindowsCollection object.
        /// </summary>
        public EnumWindowsCollection()
        {
            // nothing to do
        }
    }
    #endregion

    #region EnumWindowsItem
    /// <summary>
    /// Provides details about a Window returned by the 
    /// enumeration
    /// </summary>
    public class EnumWindowsItem
    {
        /// <summary>
        /// The window handle.
        /// </summary>
        private IntPtr hWnd = IntPtr.Zero;

        /// <summary>
        /// To allow items to be compared, the hash code
        /// is set to the Window handle, so two EnumWindowsItem
        /// objects for the same Window will be equal.
        /// </summary>
        /// <returns>The Window Handle for this window</returns>
        public override System.Int32 GetHashCode()
        {
            return (System.Int32)this.hWnd;
        }

        /// <summary>
        /// Gets the window's handle
        /// </summary>
        public IntPtr Handle
        {
            get
            {
                return this.hWnd;
            }
        }

        /// <summary>
        /// Gets the window's title (caption)
        /// </summary>
        public string Text
        {
            get
            {
                StringBuilder title = new StringBuilder(260, 260);
                API.GetWindowText(this.hWnd, title, title.Capacity);
                return title.ToString();
            }
        }

        /// <summary>
        /// Gets the window's class name.
        /// </summary>
        public string ClassName
        {
            get
            {
                StringBuilder className = new StringBuilder(260, 260);
                API.GetClassName(this.hWnd, className, className.Capacity);
                return className.ToString();
            }
        }

        /// <summary>
        /// Gets/Sets whether the window is iconic (mimimised) or not.
        /// </summary>
        public bool Iconic
        {
            get
            {
                return ((API.IsIconic(this.hWnd) == 0) ? false : true);
            }
            set
            {
                API.SendMessage(
                    this.hWnd,
                    API.WM_SYSCOMMAND,
                    (IntPtr)API.SC_MINIMIZE,
                    IntPtr.Zero);
            }
        }

        /// <summary>
        /// Gets/Sets whether the window is maximised or not.
        /// </summary>
        public bool Maximised
        {
            get
            {
                return ((API.IsZoomed(this.hWnd) == 0) ? false : true);
            }
            set
            {
                API.SendMessage(
                    this.hWnd,
                    API.WM_SYSCOMMAND,
                    (IntPtr)API.SC_MAXIMIZE,
                    IntPtr.Zero);
            }
        }

        /// <summary>
        /// Gets whether the window is visible.
        /// </summary>
        public bool Visible
        {
            get
            {
                return ((API.IsWindowVisible(this.hWnd) == 0) ? false : true);
            }
        }

        /// <summary>
        /// Gets the bounding rectangle of the window
        /// </summary>
        public System.Drawing.Rectangle Rect
        {
            get
            {
                API.RECT rc = new API.RECT();
                API.GetWindowRect(
                    this.hWnd,
                    ref rc);
                System.Drawing.Rectangle rcRet = new System.Drawing.Rectangle(
                    rc.Left, rc.Top,
                    rc.Right - rc.Left, rc.Bottom - rc.Top);
                return rcRet;
            }
        }

        /// <summary>
        /// Gets the location of the window relative to the screen.
        /// </summary>
        public System.Drawing.Point Location
        {
            get
            {
                System.Drawing.Rectangle rc = Rect;
                System.Drawing.Point pt = new System.Drawing.Point(
                    rc.Left,
                    rc.Top);
                return pt;
            }
        }

        /// <summary>
        /// Gets the size of the window.
        /// </summary>
        public System.Drawing.Size Size
        {
            get
            {
                System.Drawing.Rectangle rc = Rect;
                System.Drawing.Size sz = new System.Drawing.Size(
                    rc.Right - rc.Left,
                    rc.Bottom - rc.Top);
                return sz;
            }
        }

        /// <summary>
        /// Restores and Brings the window to the front, 
        /// assuming it is a visible application window.
        /// </summary>
        public void Restore()
        {
            if (Iconic)
            {
                API.SendMessage(
                    this.hWnd,
                    API.WM_SYSCOMMAND,
                    (IntPtr)API.SC_RESTORE,
                    IntPtr.Zero);
            }
            API.BringWindowToTop(this.hWnd);
            API.SetForegroundWindow(this.hWnd);
        }

        public API.WindowStyleFlags WindowStyle
        {
            get
            {
                return (API.WindowStyleFlags)API.GetWindowLong(
                    this.hWnd, API.GWL_STYLE);
            }
        }

        public API.ExtendedWindowStyleFlags ExtendedWindowStyle
        {
            get
            {
                return (API.ExtendedWindowStyleFlags)API.GetWindowLong(
                    this.hWnd, API.GWL_EXSTYLE);
            }
        }

        /// <summary>
        ///  Constructs a new instance of this class for
        ///  the specified Window Handle.
        /// </summary>
        /// <param name="hWnd">The Window Handle</param>
        public EnumWindowsItem(IntPtr hWnd)
        {
            this.hWnd = hWnd;
        }
    }
    #endregion
}
