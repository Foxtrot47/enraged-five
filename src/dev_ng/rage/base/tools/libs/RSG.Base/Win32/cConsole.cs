//
// File: cConsole.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of cConsole.cs class
//

using System;
using System.Runtime.InteropServices;

namespace RSG.Base.Win32
{

    /// <summary>
    /// 
    /// </summary>
    [Obsolete("Old and not required; don't think we need a replacement.", true)]
    public class cConsole
    {
        #region Enumerations
        /// <summary>
        /// 
        /// </summary>
        [Flags]
        public enum etForegroundColour
        {
            Black = 0x0000,
            Blue = 0x0001,
            Green = 0x0002,
            Cyan = 0x0003,
            Red = 0x0004,
            Magenta = 0x0005,
            Yellow = 0x0006,
            Grey = 0x0007,
            White = 0x0008
        }
        #endregion // Enumerations

        #region Hide Constructor
        private cConsole()
        {
        }
        #endregion // Hide Constructor

        #region P/Invoke Interface
        // constants for console streams
        const int STD_INPUT_HANDLE = -10;
        const int STD_OUTPUT_HANDLE = -11;
        const int STD_ERROR_HANDLE = -12;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nStdHandle">Input, output or error device</param>
        /// <returns></returns>
        [DllImportAttribute("Kernel32.dll")]
        private static extern IntPtr GetStdHandle(int nStdHandle);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hConsoleOutput">Handle to screen buffer</param>
        /// <param name="wAttributes">Text and background colours</param>
        /// <returns></returns>
        [DllImportAttribute("Kernel32.dll")]
        private static extern bool SetConsoleTextAttribute(IntPtr hConsoleOutput, int wAttributes);
        #endregion // P/Invoke Interface

        #region Static Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static bool SetForeGroundColour()
        {
            return SetForeGroundColour(etForegroundColour.Grey, false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="foreGroundColour"></param>
        /// <returns></returns>
        public static bool SetForeGroundColour(etForegroundColour foreGroundColour)
        {
            return SetForeGroundColour(foreGroundColour, true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="foreGroundColour"></param>
        /// <param name="brightColours"></param>
        /// <returns></returns>
        public static bool SetForeGroundColour(etForegroundColour foreGroundColour, 
                                               bool brightColours)
        {
            // get the current console handle
            IntPtr nConsole = GetStdHandle(STD_OUTPUT_HANDLE);
            int colourMap;
            
            // if we want bright colours OR it with white
            if (brightColours)
                colourMap = (int) foreGroundColour | (int)etForegroundColour.White;
            else
                colourMap = (int) foreGroundColour;

            return SetConsoleTextAttribute(nConsole, colourMap);
        }
        #endregion // Static Controller Methods
    }

} // End of RSG.Base.Win32 namespace

// End of file
