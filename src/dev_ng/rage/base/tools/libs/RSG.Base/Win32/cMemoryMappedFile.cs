//
// File: cMemoryMappedFile.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of cMemoryMappedFile.cs class
//
// Reference: http://www.codeproject.com/csharp/SingleInstanceApplication.asp
//

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace RSG.Base.Win32
{

    /// <summary>
    /// Memory-Mapped File
    /// </summary>
    public sealed class cMemoryMappedFile : IDisposable
    {	
        #region Member Data
		private IntPtr m_MemoryFileHandle;
        #endregion

        private const uint kFileMapWrite = 0x0002;
        private const uint kFileMapRead  = 0x0004;

		public enum FileAccess : int
		{
			ReadOnly = 2,
			ReadWrite = 4
		}

		private cMemoryMappedFile(IntPtr memoryFileHandle)
		{
            m_MemoryFileHandle = memoryFileHandle;
		}
		
		public static cMemoryMappedFile CreateMMF(string fileName, FileAccess access, int size)
		{
			if(size < 0)
				throw new ArgumentException("The size parameter should be a number greater than Zero.");

			IntPtr memoryFileHandle = Win32.API.CreateFileMapping(0xFFFFFFFF,IntPtr.Zero,(uint)access,0,(uint)size,fileName);
			if(memoryFileHandle == IntPtr.Zero)
				throw new eSharedMemoryException("Creating Shared Memory failed.");

			return new cMemoryMappedFile(memoryFileHandle);
		}

		public static IntPtr ReadHandle(string fileName)
		{
            IntPtr mappedFileHandle = Win32.API.OpenFileMapping((int)FileAccess.ReadWrite, false, fileName);
			if(mappedFileHandle == IntPtr.Zero)
				throw new eSharedMemoryException("Opening the Shared Memory for Read failed.");

            IntPtr mappedViewHandle = Win32.API.MapViewOfFile(mappedFileHandle, kFileMapRead, 0, 0, 8);
			if(mappedViewHandle == IntPtr.Zero)
				throw new eSharedMemoryException("Creating a view of Shared Memory failed.");
			
			IntPtr windowHandle = Marshal.ReadIntPtr(mappedViewHandle);
			if(windowHandle == IntPtr.Zero)
				throw new ArgumentException ("Reading from the specified address in  Shared Memory failed.");

            Win32.API.UnmapViewOfFile(mappedViewHandle);
            Win32.API.CloseHandle((uint)mappedFileHandle);
			return windowHandle;
		}

		public void WriteHandle(IntPtr windowHandle)
		{
            IntPtr mappedViewHandle = Win32.API.MapViewOfFile(m_MemoryFileHandle, kFileMapWrite, 0, 0, 8);
			if(mappedViewHandle == IntPtr.Zero)
				throw new eSharedMemoryException("Creating a view of Shared Memory failed.");
			
			Marshal.WriteIntPtr(mappedViewHandle,windowHandle );

            Win32.API.UnmapViewOfFile(mappedViewHandle);
            Win32.API.CloseHandle((uint)mappedViewHandle);
		}


		#region IDisposable Member
		public void Dispose()
		{
            if (m_MemoryFileHandle != IntPtr.Zero)
			{
                if (Win32.API.CloseHandle((uint)m_MemoryFileHandle))
                    m_MemoryFileHandle = IntPtr.Zero;
			}
		}
		#endregion
	}

    /// <summary>
    /// 
    /// </summary>
	[Serializable]
	public class eSharedMemoryException: ApplicationException 
	{
		public eSharedMemoryException()
		{	
		}

		public eSharedMemoryException(string message): base(message)
		{
		}

		public eSharedMemoryException(string message, Exception inner): base(message, inner)
		{
		}
	}

} // End of RSG.Base.Win32 namespace

// End of file
