//
// File: cFileAssociation.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of cFileAssociation class
//

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

using Microsoft.Win32;

namespace RSG.Base.Win32
{
    
    /// <summary>
    /// Windows File Association Helper Class
    /// </summary>
    /// This class can be used to create and query existing Windows File 
    /// Associations.
    [Obsolete("Use RSG.Microsoft.Interop.FileAssociation.", true)]
    public class cFileAssociation
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// File extension string (including prefixed ".")
        /// </summary>
        public String Extension
        {
            get { return m_sExtension; }
            private set { m_sExtension = value; }
        }
        private String m_sExtension;

        /// <summary>
        /// Default program associated with this file extension
        /// </summary>
        public String ProgramHandler
        {
            get { return m_sProgramHandler; }
            set { m_sProgramHandler = value; }
        }
        private String m_sProgramHandler;

        /// <summary>
        /// File content MIME Type
        /// </summary>
        public String MimeType
        {
            get { return m_sMimeType; }
            set { m_sMimeType = value; }
        }
        private String m_sMimeType;

        /// <summary>
        /// File content perceived type
        /// </summary>
        public String PerceivedType
        {
            get { return m_sContentType; }
            set { m_sContentType = value; }
        }
        private String m_sContentType;

        /// <summary>
        /// List of programs in the file extension's Open With shell list
        /// </summary>
        public List<String> OpenWithList
        {
            get { return m_OpenWithList; }
            set { m_OpenWithList = value; }
        }
        private List<String> m_OpenWithList;
        #endregion

        #region Constructors
        /// <summary>
        /// Instantiate a File Association object from the Registry 
        /// association information.
        /// </summary>
        /// <param name="sExt">File extension (e.g. ".mp3", ".txt")</param>
        /// <example>cFileAssociation assoc = new cFileAssociation( ".mp3" );</example>
        public cFileAssociation(String sExt)
        {
            LoadFromRegistry(sExt);
        }

        /// <summary>
        /// Instantiate a new File Association object 
        /// </summary>
        /// <param name="sExt"></param>
        /// <param name="sProgram"></param>
        public cFileAssociation(String sExt, String sProgram)
        {
            Init(sExt, sProgram, String.Empty, String.Empty);
        }

        /// <summary>
        /// Instantiate a new File Association object
        /// </summary>
        /// <param name="sExt"></param>
        /// <param name="sProgram"></param>
        /// <param name="sMimeType"></param>
        public cFileAssociation(String sExt, String sProgram, String sMimeType)
        {
            Init(sExt, sProgram, sMimeType, String.Empty);
        }

        /// <summary>
        /// Instantiate a new File Association object
        /// </summary>
        /// <param name="sExt"></param>
        /// <param name="sProgram"></param>
        /// <param name="sMimeType"></param>
        /// <param name="sPerceivedType"></param>
        public cFileAssociation(String sExt, String sProgram, String sMimeType, String sPerceivedType)
        {
            Init(sExt, sProgram, sMimeType, sPerceivedType);
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Save current state of association to the Windows Registry
        /// </summary>
        public void SaveToRegistry()
        {

        }

        /// <summary>
        /// Delete current association from the Windows Registry
        /// </summary>
        /// This has the effect of removing the file association between
        /// the extension and the default program.
        public void DeleteFromRegistry()
        {

        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Read File Association information from Registry and initialize
        /// member data.
        /// </summary>
        /// <param name="sExtension"></param>
        private void LoadFromRegistry(String sExtension)
        {
            if (sExtension.StartsWith("."))
                Extension = sExtension;
            else
                Extension = "." + sExtension;
            
            RegistryKey keyName = Registry.ClassesRoot.OpenSubKey(Extension);
            if (null == keyName)
                throw new exRegistryAccessException(String.Format("Key {0} does not exist in File Association Registry information.", sExtension));

            // Program Handler string
            ProgramHandler = (String)(keyName.GetValue(String.Empty));

            // Content Type
            MimeType = (String)(keyName.GetValue("Content Type"));
            PerceivedType = (String)(keyName.GetValue("PerceivedType"));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sExtension"></param>
        /// <param name="sProgram"></param>
        /// <param name="sMimeType"></param>
        /// <param name="sPerceivedType"></param>
        private void Init(String sExtension, String sProgram, String sMimeType, String sPerceivedType)
        {
            if (sExtension.StartsWith("."))
                Extension = sExtension;
            else
                Extension = "." + sExtension;

            ProgramHandler = sProgram;
            MimeType = sMimeType;
            PerceivedType = sPerceivedType;
        }

        /// <summary>
        /// Notify the Windows Shell that File Associations have changed
        /// </summary>
        private void SHNotifyFileAssocChange()
        {
            Win32.API.SHChangeNotify(
                (uint)Win32.API.etShellChangeNotifyEvents.SHCNE_ASSOCCHANGED,
                (uint)(Win32.API.etShellChangeNotifyFlags.SHCNF_IDLIST |
                       Win32.API.etShellChangeNotifyFlags.SHCNF_FLUSHNOWAIT),
                IntPtr.Zero,
                IntPtr.Zero);
        }
        #endregion
    }

} // RSG.Base.Win32 namespace

// End of file
