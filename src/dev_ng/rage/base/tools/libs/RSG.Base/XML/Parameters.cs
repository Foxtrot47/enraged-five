﻿//---------------------------------------------------------------------------------------------
// <copyright file="Parameters.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2012-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Base.Xml
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Xml.Linq;

    /// <summary>
    /// Simple XML parameter parsing methods.
    /// </summary>
    /// Note: prefer ParameterCollection (and ParameterDictionary) over the use of these
    /// generic files.  You get Metadata Editor support if you use these and the ability
    /// to group lots of parameters together into a single file.
    /// 
    public static class Parameters
    {
        /// <summary>
        /// Load parameters from a file.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="parameters"></param>
        /// <param name="keysToLower"></param>
        /// <param name="allowOverrides"></param>
        public static bool Load(String filename, ref IDictionary<String, Object> parameters, 
            bool keysToLower = false, bool allowOverrides = false)
        {
            if (!System.IO.File.Exists(filename))
                return (false);

            XDocument xmlDoc = XDocument.Load(filename);
            bool result = (Parameters.Load(xmlDoc.Root, ref parameters, keysToLower, allowOverrides));

            // Attempt to load a "<filename>.local.xml" file; for local overrides.
            String extension = String.Format("local{0}", System.IO.Path.GetExtension(filename));
            String localFilename = System.IO.Path.ChangeExtension(filename, extension);
            if (System.IO.File.Exists(localFilename))
            {
                Logging.Log.StaticLog.Warning("Loading overrides for {0} : {1}", filename, localFilename);
                XDocument xmlDocLocal = XDocument.Load(localFilename);
                result &= Parameters.Load(xmlDocLocal.Root, ref parameters, keysToLower, true);
            }
            return result;
        }

        /// <summary>
        /// Load parameters from a particular XML node.
        /// </summary>
        /// <param name="root"></param>
        /// <param name="parameters"></param>
        /// <param name="keysToLower"></param>
        /// <param name="allowOverrides"></param>
        public static bool Load(XElement root, ref IDictionary<String, Object> parameters, 
            bool keysToLower = false, bool allowOverrides = false)
        {
            Debug.Assert(parameters != null, "Parameters argument supplied is null");

            bool result = true;
            try
            {
                // POD Parameters.
                IEnumerable<XElement> xmlParameters = root.Elements("Parameter");
                foreach (XElement xmlParam in xmlParameters)
                {
                    KeyValuePair<String, Object> param = ParseParameter(xmlParam, keysToLower);
                    if (String.IsNullOrEmpty(param.Key))
                        continue; // Skip anonymous parameters.

                    if (parameters.ContainsKey(param.Key) && allowOverrides)
                    {
                        Logging.Log.StaticLog.Message("Parameter overridden : {0} : {1} => {2}", param.Key, parameters[param.Key], param.Value);
                        parameters[param.Key] = param.Value;
                    }
                    else if (!parameters.ContainsKey(param.Key))
                        parameters.Add(param);
                    else
                        result = false;
                }

                // POD[] Parameters.
                IEnumerable<XElement> xmlArrayParameters = root.Elements("Array");
                foreach (XElement xmlParams in xmlArrayParameters)
                {
                    Debug.Assert(null != xmlParams.Attribute("type"), "Array doesn't have type specified.");
                    
                    String name = xmlParams.Attribute("name").Value;
                    String typeName = xmlParams.Attribute("type").Value;

                    IEnumerable<XElement> xmlElementParameter = xmlParams.Elements("Parameter");
                    ICollection<Object> values = new List<Object>();
                    foreach (XElement xmlParam in xmlElementParameter)
                    {
                        values.Add(ParseParameterValue(xmlParam, typeName));
                    }

                    if (parameters.ContainsKey(name) && allowOverrides)
                        parameters[name] = values;
                    else if (!parameters.ContainsKey(name))
                        parameters.Add(name, values);
                    else
                        result = false;
                }
            }
            catch (Exception e)
            {
                Debug.Assert(false, String.Format("Exception occurred parsing parameter file {0}", e.Message));
                result = false;
            }
            return (result);
        }

        /// <summary>
        /// Save parameters to a file.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="parameters"></param>
        public static void Save(String filename, IDictionary<String, Object> parameters)
        {
            XDocument xmlDoc = new XDocument();
            XElement root = ToXElement("Parameters", parameters);
            xmlDoc.Add(root);
            xmlDoc.Save(filename);
        }

        /// <summary>
        /// Serialise parameters to an XElement.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static XElement ToXElement(String name, IDictionary<String, Object> parameters)
        {
            XElement xmlParametersElem = new XElement(name);
            foreach (KeyValuePair<String, Object> kvp in parameters)
            {
                String paramName = kvp.Key;
                Object value = kvp.Value;
                XElement paramElement = new XElement("Parameter");
                paramElement.SetAttributeValue("name", paramName);

                String fullName = value.GetType().FullName;
                String typeName;

                switch (fullName)
                {
                    case "System.Boolean":
                        typeName = "bool";
                        break;
                    case "System.String":
                        typeName = "string";
                        break;
                    case "System.Int":
                    case "System.Int32":
                        typeName = "int";
                        break;
                    case "System.Int64":
                        typeName = "long";
                        break;
                    case "System.Single":
                        typeName = "float";
                        break;
                    default:
                        throw new NotSupportedException();
                }

                paramElement.SetAttributeValue("type", typeName);
                paramElement.SetAttributeValue("value", value.ToString());
                xmlParametersElem.Add(paramElement);
            }

            return (xmlParametersElem);
        }

        /// <summary>
        /// Parse an XElement into a parameter KeyValuePair.
        /// </summary>
        /// <param name="xmlParameter"></param>
        /// <param name="keysToLower"></param>
        /// <returns></returns>
        private static KeyValuePair<String, Object> ParseParameter(XElement xmlParameter, 
            bool keysToLower = false)
        {
            Debug.Assert(null != xmlParameter.Attribute("value"), "No Parameter value specified.");
            Debug.Assert(null != xmlParameter.Attribute("type"), "No Parameter type specified.");
            if (null == xmlParameter.Attribute("value"))
                return (new KeyValuePair<String, Object>(String.Empty, null));
            
            String name = xmlParameter.Attribute("name").Value;
            String type = xmlParameter.Attribute("type").Value;

            if (keysToLower)
            {
                name = name.ToLower();
            }

            Object value = ParseParameterValue(xmlParameter, type);
            KeyValuePair<String, Object> param = new KeyValuePair<String, Object>(name, value);

            return (param);
        }

        /// <summary>
        /// Parse an XElement's 'value' attribute; according to specified type.
        /// </summary>
        /// <param name="xmlParameter"></param>
        /// <param name="typeName"></param>
        /// <returns></returns>
        private static Object ParseParameterValue(XElement xmlParameter, String typeName)
        {
            string type = typeName.ToLower();
            string value = xmlParameter.Attribute("value").Value;

            switch (type)
            {
                case "bool":
                    return bool.Parse(value);
                case "int":
                    return int.Parse(value);
                case "long":
                    return long.Parse(value);
                case "float" :
                    return float.Parse(value);

                // case "string" not needed but left for case completeness
                case "string":
                default:
                    return value;
            }
        }
    }

} // RSG.Base.Xml namespace
