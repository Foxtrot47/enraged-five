﻿//---------------------------------------------------------------------------------------------
// <copyright file="ParameterDictionary.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2012-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Base.Xml
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Xml.Linq;
    using System.Xml.XPath;
    
    /// <summary>
    /// Class provides read/write access for parCodeGen "ParameterDictionary" files as
    /// defined by PSC parameters.psc.
    /// </summary>
    /// These replace the old Parameter XML data as they are editable in the Metadata Editor.
    /// 
    /// This supports a hierarchy of parameter metadata files that are loaded in the
    /// following order:
    ///   1) filename.meta
    ///   2) filename.%COMPUTERNAME%.meta
    ///   3) filename.local.meta
    ///   
    /// This allows machine overrides (to be checked in) and local per-user overrides.
    /// Note: if there are machine or local overrides they will get saved out into the
    /// mentioned filename (where parameters loaded is not currently stored).
    /// 
    /// Note: this deliberately does not use the metadata abstraction library to reduce
    /// dependencies.
    /// 
    /// When we have C# class generation from PSC files this class could be integrated
    /// with that system.
    /// 
    [DataContract]
    [KnownType(typeof(IDictionary<String, Object>))]
    public class ParameterDictionary
    {
        #region Constants
        /// <summary>
        /// Compatible type array.
        /// </summary>
        public static readonly Type[] CompatibleTypes = new Type[] { typeof(int), typeof(bool), typeof(float), typeof(String) };
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Parameter dictionary name.
        /// </summary>
        [DataMember]
        public String Name
        {
            get;
            set;
        }

        /// <summary>
        /// Parameter dictionary description.
        /// </summary>
        [DataMember]
        public String Description
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the number of parameters.
        /// </summary>
        public int Count
        {
            get { return (this._parameters.Count); }
        }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Parameter dictionary data.
        /// </summary>
        [DataMember]      
        private IDictionary<String, Object> _parameters;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor; create an empty Parameter Dictionary.
        /// </summary>
        public ParameterDictionary()
        {
            // Sorted dictionary keeps parameter order consistent during save.
            this._parameters = new SortedDictionary<String, Object>();
        }

        /// <summary>
        /// Constructor; loading ParameterDictionary element.
        /// </summary>
        /// <param name="xmlElement"></param>
        public ParameterDictionary(XElement xmlElement)
            : this()
        {
            LoadInline(xmlElement);
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Load new ParameterDictionary object from stream.
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public static ParameterDictionary Load(Stream stream)
        {
            XDocument xmlDoc = XDocument.Load(stream);
            ParameterDictionary paramDict = new ParameterDictionary();
            paramDict.LoadInline(xmlDoc.Root);

            return (paramDict);
        }

        /// <summary>
        /// Load new ParameterDictionary object from file.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static ParameterDictionary Load(String filename)
        {
            String directory = Path.GetDirectoryName(filename);
            String basename = Path.GetFileNameWithoutExtension(filename);
            String machineFilename = Path.Combine(directory, String.Format("{0}.{1}.meta", basename, Environment.MachineName));
            String localFilename = Path.Combine(directory, String.Format("{0}.local.meta", basename));
            
            ParameterDictionary paramDictionary = null;
            using (FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read))
            {
                paramDictionary = Load(fs);
            }
            if (File.Exists(machineFilename))
            {
                XDocument xmlDoc = XDocument.Load(machineFilename);
                paramDictionary.LoadInline(xmlDoc.Root);
            }
            if (File.Exists(localFilename))
            {
                XDocument xmlDoc = XDocument.Load(localFilename);
                paramDictionary.LoadInline(xmlDoc.Root);
            }
            return (paramDictionary);
        }

        /// <summary>
        /// Save this parameter dictionary into a parCodeGen XML document stream.
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="elementName"></param>
        public void Save(Stream stream, String elementName)
        {
            XDocument xmlDoc = new XDocument(
                new XDeclaration("1.0", "utf-8", "yes"),
                this.ToXElement(elementName)
            );
            xmlDoc.Save(stream);
        }

        /// <summary>
        /// Save this parameter dictionary into a parCodeGen file.
        /// </summary>
        /// <param name="filename"></param>
        public void Save(String filename)
        {
            using (FileStream fs = new FileStream(filename, FileMode.Create, FileAccess.Write))
            {
                Save(fs, "ParameterDictionary");
            }
        }

        /// <summary>
        /// Serialise ParameterDictionary to an XElement for inclusion in another XDocument.
        /// </summary>
        /// <returns></returns>
        public XElement ToXElement()
        {
            return (ToXElement("ParameterDictionary"));
        }

        /// <summary>
        /// Serialise ParameterDictionary to an XElement for inclusion in another XDocument.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public XElement ToXElement(String name)
        {
            XElement xmlParamDictElem = new XElement(name,
                new XAttribute("type", "ParameterDictionary"),
                new XElement("FriendlyName", this.Name),
                new XElement("Description", this.Description),
                new XElement("Parameters",
                    this._parameters.Select(kvp => PODParameterToXElement("Item", kvp))
                ) // Parameters
            ); // ParameterDictionary
            return (xmlParamDictElem);
        }

        /// <summary>
        /// Removes all parameters from the dictionary.
        /// </summary>
        public void Clear()
        {
            this._parameters.Clear();
        }

        /// <summary>
        /// Determine whether the parameter exists in the dictionary.
        /// </summary>
        /// <param name="name">Parameter's name</param>
        /// <returns></returns>
        public bool HasParameter(String name)
        {
            return (this._parameters.ContainsKey(name));
        }

        /// <summary>
        /// Determine whether the parameter is a suitable type.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private bool IsCompatibleType(Type type)
        {
            return (CompatibleTypes.Contains(type));
        }

        /// <summary>
        /// Set a parameter value (overwriting if it already exists).
        /// </summary>
        /// <typeparam name="T">Parameter's type</typeparam>
        /// <param name="name">Parameter's name</param>
        /// <param name="value">Parameter's value</param>
        public void SetParameter<T>(String name, T value)
        {
            Debug.Assert(IsCompatibleType(typeof(T)), "Unsupported parameter type.");
            if (!IsCompatibleType(typeof(T)))
                throw (new ArgumentException("Unsupported parameter type."));

            if (this._parameters.ContainsKey(name))
            {
                this._parameters[name] = value;
            }
            else
            {
                this._parameters.Add(name, value);
            }
        }

        /// <summary>
        /// Set a parameter value (overwriting if it already exists).
        /// </summary>
        /// <param name="name">Parameter's name</param>
        /// <param name="value"></param>
        public void SetParameter(String name, Object value)
        {
            SetParameter<Object>(name, value);
        }

        /// <summary>
        /// Get a parameter value.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public T GetParameter<T>(String name, T defaultValue)
        {
            // Underlying type of the Parameter's value is still an object so we're constrained here
            object value;

            // No type checking here.
            // Because if the calling code is supplying the type and that's not what the parameter is, then the problem is not here
            // (aka "Embrace the InvalidCastException")
            if (this._parameters.TryGetValue(name, out value))
            {
                return (T)value;
            }
            return (defaultValue);
        }

        /// <summary>
        /// Get a parameter value.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public Object GetParameter(String name, Object defaultValue)
        {
            return (GetParameter<Object>(name, defaultValue));
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Load XElement into our structure.
        /// </summary>
        /// <param name="xmlElem"></param>
        private void LoadInline(XElement xmlElem)
        {
            XAttribute xmlNameAttr = xmlElem.Attribute("key");
            if (null != xmlNameAttr && null != xmlNameAttr.Value)
                this.Name = xmlNameAttr.Value;
            else
                this.Name = "undefined";
            XElement xmlDescElem = xmlElem.Element("Description");
            if (null != xmlDescElem && null != xmlDescElem.Value)
                this.Description = xmlDescElem.Value;

            // Loop through our parameters.
            IEnumerable<XElement> xmlParameterElems = xmlElem.XPathSelectElements("Parameters/Item");
            foreach (XElement xmlParamElem in xmlParameterElems)
            {
                KeyValuePair<String, Object> param = XElementToPODParameter(xmlParamElem);
                if (this._parameters.ContainsKey(param.Key))
                    this._parameters[param.Key] = param.Value;
                else
                    this._parameters.Add(param);
            }
        }

        /// <summary>
        /// Convert an XElement to a KeyValuePair for insertion.
        /// </summary>
        /// <param name="xmlParamElem"></param>
        /// <returns></returns>
        private KeyValuePair<String, Object> XElementToPODParameter(XElement xmlParamElem)
        {
            XAttribute xmlName = xmlParamElem.Attribute("key");
            if (null == xmlName || null == xmlName.Value)
                throw (new NotSupportedException("Parameter does not define key attribute."));
            String name = xmlName.Value;

            XElement xmlType = xmlParamElem.Element("Type");
            if (null == xmlType || null == xmlType.Value)
                throw (new NotSupportedException("Parameter does not define Type element."));
            String type = xmlType.Value;

            XElement xmlValue = xmlParamElem.Element("Value");
            if (null == xmlValue || null == xmlValue.Value)
                throw (new NotSupportedException("Parameter does not define Value element."));
            String value = xmlValue.Value;

            int intVal = 0;
            long longVal = 0;
            float floatVal = 0.0f;
            double doubleVal = 0.0;
            bool boolVal = false;
            if (type.Equals("Integer") && int.TryParse(value, out intVal))
                return (new KeyValuePair<String, Object>(name, intVal));
            else if (type.Equals("Long") && long.TryParse(value, out longVal))
                return (new KeyValuePair<String, Object>(name, longVal));
            else if (type.Equals("Float") && float.TryParse(value, out floatVal))
                return (new KeyValuePair<String, Object>(name, floatVal));
            else if (type.Equals("Double") && double.TryParse(value, out doubleVal))
                return (new KeyValuePair<String, Object>(name, doubleVal));
            else if (type.Equals("Boolean") && bool.TryParse(value, out boolVal))
                return (new KeyValuePair<String, Object>(name, boolVal));
            else if (type.Equals("String"))
                return (new KeyValuePair<String, Object>(name, value));
            else
                throw (new NotSupportedException(String.Format("Parameter '{0}' has invalid Type element.", name)));
        }

        /// <summary>
        /// Convert a parameter KeyValuePair to XElement.
        /// </summary>
        /// <param name="name">XElement's name.</param>
        /// <param name="kvp">Parameter KeyValuePair.</param>
        /// <returns></returns>
        private XElement PODParameterToXElement(String name, KeyValuePair<String, Object> kvp)
        {
            Object value = kvp.Value;
            XElement xmlElem = new XElement(name,
                new XAttribute("key", kvp.Key), 
                new XAttribute("type", "PODParameter"),
                new XElement("Type", ObjectTypeToString(value.GetType())),
                new XElement("Value", kvp.Value.ToString())
            ); // XElement

            return (xmlElem);
        }

        /// <summary>
        /// Convert a POD type to String for serialisation.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private String ObjectTypeToString(Type type)
        {
            Debug.Assert(IsCompatibleType(type), "Unsupported parameter type.");
            if (!IsCompatibleType(type))
                throw (new ArgumentException("Unsupported parameter type."));

            String typeStr = "Unknown";
            if (type == typeof(int))
                typeStr = "Integer";
            else if (type == typeof(float))
                typeStr = "Float";
            else if (type == typeof(bool))
                typeStr = "Boolean";
            else if (type == typeof(String))
                typeStr = "String";

            return (typeStr);
        }
        #endregion // Private Methods
    }

} // RSG.Base.Xml namespace
