//
// File: XmlSerialisation.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of XmlSerialisation.cs class
//

using System;
using System.Xml;

namespace RSG.Base.Interfaces
{
    [Obsolete]
    public interface iXmlSerializableObject
    {
        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        void SaveXml(String filename);
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        void LoadXml(String filename);
        #endregion // Methods
    }

    /// <summary>
    /// Xml Serialization Interface
    /// </summary>
    [Obsolete]
    public interface iXmlSerialize
    {
        #region Methods
        void SerializeXml(XmlWriter xmlWriter);
        #endregion // Methods
    }

    /// <summary>
    /// Xml Deserialization Interface
    /// </summary>
    [Obsolete]
    public interface iXmlDeserialize
    {
        #region Methods
        void DeserializeXml(XmlReader xmlReader);
        #endregion // Methods
    }

} // End of RSG.Base.Interfaces namespace

// End of file
