﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;

namespace RSG.Base.Xml
{
    public class Utils
    {        
        /// <summary>
        /// Heavy weight xml escaping ( was used to handle escaping of a variety of invalid characters - see CL 5142179 )
        /// - many ways of doing this, this is just one. http://stackoverflow.com/questions/1132494/string-escape-into-xml
        /// DW: if I had time I'd consider ( and profile ) a variety of other methods : http://weblogs.sqlteam.com/mladenp/archive/2008/10/21/Different-ways-how-to-escape-an-XML-string-in-C.aspx
        /// </summary>
        /// <param name="unescaped"></param>
        /// <returns></returns>
        public static string XmlEscape(string unescaped)
        {
            try
            {
                if (string.IsNullOrEmpty(unescaped)) return unescaped;

                XmlDocument doc = new XmlDocument();
                XmlNode node = doc.CreateElement("root");
                node.InnerText = unescaped;
                return node.InnerXml;
            }
            catch (XmlException)
            {
                return unescaped;
            }
        }

        /// <summary>
        /// Heavy weight xml escaping ( was used to handle escaping of a variety of invalid characters - see CL 5142179 )
        /// - many ways of doing this, this is just one. http://stackoverflow.com/questions/1132494/string-escape-into-xml
        /// DW: if I had time I'd consider ( and profile ) a variety of other methods : http://weblogs.sqlteam.com/mladenp/archive/2008/10/21/Different-ways-how-to-escape-an-XML-string-in-C.aspx
        /// </summary>
        /// <param name="escaped"></param>
        /// <returns></returns>
        public static string XmlUnescape(string escaped)
        {
            try
            {
                if (string.IsNullOrEmpty(escaped)) return escaped;

                XmlDocument doc = new XmlDocument();
                XmlNode node = doc.CreateElement("root");
                node.InnerXml = escaped;
                return node.InnerText;
            }
            catch (XmlException)
            {
                // Handle characters that were not already escaped ( backwards compatibility with existing jobs )
                return escaped;
            }
        }

        /// <summary>
        /// UnescapeXml : appears not to be in use.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        [Obsolete("Use XmlEscape instead.")]
        public static string UnescapeXml(string s)
        {
            if (string.IsNullOrEmpty(s)) return s;

            string returnString = s;
            returnString = returnString.Replace("&apos;", "'");
            returnString = returnString.Replace("&quot;", "\"");
            returnString = returnString.Replace("&gt;", ">");
            returnString = returnString.Replace("&lt;", "<");
            returnString = returnString.Replace("&amp;", "&");
            return returnString;
        }

        /// <summary>
        /// Strips non-printable ascii characters 
        /// Refer to http://www.w3.org/TR/xml11/#charsets for XML 1.1
        /// Refer to http://www.w3.org/TR/2006/REC-xml-20060816/#charsets for XML 1.0
        /// </summary>
        /// <param name="xml">XML string</param>
        /// <returns>"Clean" version of the xml string</returns>
        public static string StripIllegalXMLChars(string xml)
        {
            if (xml == null)
            {
                return null;
            }

            return s_invalidXmlCharsRegex.Replace(xml, "");
        }

        /// <summary>
        /// filters control characters but allows only properly-formed surrogate sequences
        /// from: http://stackoverflow.com/questions/397250/unicode-regex-invalid-xml-characters
        /// </summary>
        private static Regex s_invalidXmlCharsRegex = new Regex(
            @"(?<![\uD800-\uDBFF])[\uDC00-\uDFFF]|[\uD800-\uDBFF](?![\uDC00-\uDFFF])|[\x00-\x08\x0B\x0C\x0E-\x1F\x7F-\x9F\uFEFF\uFFFE\uFFFF]",
            RegexOptions.Compiled);
    }
}
