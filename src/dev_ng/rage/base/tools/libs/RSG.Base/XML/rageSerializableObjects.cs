using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

using RSG.Base.Command;

namespace RSG.Base.Serialization
{
    /// <summary>
    /// An abstract class for an object that can be cloned and copied.
    /// </summary>
    public interface IRageClonableObject : ICloneable
    {       
        void CopyFrom( IRageClonableObject other );       

        void Reset();
    }

    /// <summary>
    /// A IRageClonableObject with standardized Xml serialization routines.  Also has routines
    /// for relative vs. absolute paths.
    /// </summary>
    [Serializable]
    public abstract class rageSerializableObject : IRageClonableObject
    {
        #region Variables
        private string m_originalFileLocation = string.Empty;
        private string m_currentFilename = string.Empty;
        #endregion

        #region Properties
        [Browsable( false ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public string OriginalFileLocation
        {
            get
            {
                return m_originalFileLocation;
            }
            set
            {
                m_originalFileLocation = value;
            }
        }

        [XmlIgnore, Browsable( false ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public string CurrentFilename
        {
            get
            {
                return m_currentFilename;
            }
            set
            {
                m_currentFilename = value;
            }
        }
        #endregion

        #region Overrides
        public override bool Equals( object obj )
        {
            if ( obj == null )
            {
                return false;
            }
            
            if ( base.Equals( obj ) )
            {
                return true;
            }

            if ( obj is rageSerializableObject )
            {
                return Equals( obj as rageSerializableObject );
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        #endregion

        #region Public Abstract Functions
        public abstract object Clone();
        #endregion

        #region Public Virtual Functions
        public virtual void CopyFrom( IRageClonableObject other )
        {
            if ( other == null )
            {
                return;
            }

            if ( other is rageSerializableObject )
            {
                CopyFrom( other as rageSerializableObject );
            }
        }

        public virtual void Reset()
        {
            this.OriginalFileLocation = string.Empty;
            this.CurrentFilename = string.Empty;
        }
        #endregion

        #region Public Functions
        public void CopyFrom( rageSerializableObject other )
        {
            if ( other == null )
            {
                return;
            }

            if ( base.Equals( (object)other ) )
            {
                return;
            }

            this.OriginalFileLocation = other.OriginalFileLocation;
            this.CurrentFilename = other.CurrentFilename;
        }

        public bool Equals( rageSerializableObject other )
        {
            if ( other == null )
            {
                return false;
            }

            return base.Equals( (object)other );
        }

        /// <summary>
        /// Retrieves path in terms of OriginalFileLocation.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public string GetRelativePath( string path )
        {
            return rageSerializableObject.GetRelativePath( this.OriginalFileLocation, path );
        }

        /// <summary>
        /// Retrieves the absolute path as it relates to OriginalFileLocation and CurrentFilename.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public string GetAbsolutePath( string path )
        {
            if ( !String.IsNullOrEmpty( this.CurrentFilename ) )
            {
                return rageSerializableObject.GetAbsolutePath( this.OriginalFileLocation, Path.GetDirectoryName( this.CurrentFilename ), path );
            }
            else
            {
                return rageSerializableObject.GetAbsolutePath( this.OriginalFileLocation, string.Empty, path );
            }
        }

        public void SaveFile( string filename, out rageStatus status )
        {
            TextWriter writer = null;

            try
            {
                writer = new StreamWriter( filename );
                XmlSerializer serializer = new XmlSerializer( this.GetType() );

                this.OriginalFileLocation = Path.GetDirectoryName( Path.GetFullPath( filename ) );
                this.CurrentFilename = filename;

                serializer.Serialize( writer, this );
            }
            catch ( Exception e )
            {
                status = new rageStatus( String.Format( "Error serializing '{0}': {1}", filename, e.Message ) );
                return;
            }
            finally
            {
                if ( writer != null )
                {
                    writer.Close();
                }
            }

            status = new rageStatus();
        }

        public void LoadFile( string filename, out rageStatus status )
        {
            FileStream stream = null;

            try
            {
                stream = new FileStream( filename, FileMode.Open, FileAccess.Read );

                XmlSerializer serializer = new XmlSerializer( this.GetType() );
                object obj = serializer.Deserialize( stream );
                if ( obj == null )
                {
                    status = new rageStatus( String.Format( "Unable to deserialize '{0}'", filename ) );
                    return;
                }                

                CopyFrom( obj as IRageClonableObject ); // use the top-level CopyFrom function so that our derived classes get copied properly.

                this.CurrentFilename = Path.GetFullPath( filename );
            }
            catch ( Exception e )
            {
                status = new rageStatus( String.Format( "Error deserializing '{0}': {1}", filename, e.Message ) );
                return;
            }
            finally
            {
                if ( stream != null )
                {
                    stream.Close();
                }
            }

            status = new rageStatus();
        }

        public string Serialize( out rageStatus status )
        {
            string tempFilename = Path.GetTempFileName();

            SaveFile( tempFilename, out status );
            if ( !status.Success() )
            {
                File.Delete( tempFilename );
                return null;
            }

            string serializedText = null;
            TextReader reader = null;
            try
            {
                reader = new StreamReader( tempFilename );

                serializedText = reader.ReadToEnd();

                reader.Close();
            }
            catch ( Exception e )
            {
                if ( reader != null )
                {
                    reader.Close();
                }

                status = new rageStatus( String.Format( "{0} Error loading tempfile '{1}': {2}", this.GetType().Name, tempFilename, e.Message ) );
                File.Delete( tempFilename );
                return null;
            }

            File.Delete( tempFilename );
            return serializedText;
        }

        public void Deserialize( string serializedText, out rageStatus status )
        {
            string tempFilename = Path.GetTempFileName();

            TextWriter writer = null;
            try
            {
                writer = new StreamWriter( tempFilename );

                writer.Write( serializedText );

                writer.Close();
            }
            catch ( Exception e )
            {
                if ( writer != null )
                {
                    writer.Close();
                }

                status = new rageStatus( String.Format( "{0} Error writing tempfile '{1}': {2}", this.GetType().Name, tempFilename, e.Message ) );
                File.Delete( tempFilename );
                return;
            }

            LoadFile( tempFilename, out status );
            File.Delete( tempFilename );
        }
        #endregion

        #region Public Static Functions
        /// <summary>
        /// Retrieves path2 in terms of path1
        /// </summary>
        /// <param name="path1"></param>
        /// <param name="path2"></param>
        /// <returns></returns>
        public static string GetRelativePath( string path1, string path2 )
        {
            if ( String.IsNullOrEmpty( path1 ) )
            {
                return path2.Replace( '/', '\\' );
            }

            if ( String.IsNullOrEmpty( path2 ) )
            {
                return string.Empty;
            }

            if ( Path.GetPathRoot( path1 ).ToLower() != Path.GetPathRoot( path2 ).ToLower() )
            {
                return path2.Replace( '/', '\\' );
            }

            // break each down into their folders
            string[] path1Split = path1.Replace( '/', '\\' ).Split( new char[] { '\\' } );
            string[] path2Split = path2.Replace( '/', '\\' ).Split( new char[] { '\\' } );

            // eliminate the common root
            int i = 0;
            int j = 0;
            while ( (i < path1Split.Length) && (j < path2Split.Length - 1) && (path1Split[i].ToLower() == path2Split[j].ToLower()) )
            {
                ++i;
                ++j;
            }

            StringBuilder relativePath = new StringBuilder();

            // for each remaining level in the path1, add a ..
            for ( ; i < path1Split.Length; ++i )
            {
                relativePath.Append( ".." );
                relativePath.Append( '\\' );
            }

            // for each level in the path2 path1, add the path1
            for ( ; j < path2Split.Length - 1; ++j )
            {
                relativePath.Append( path2Split[j] );
                relativePath.Append( '\\' );
            }

            // add the path2 name
            relativePath.Append( path2Split[j] );

            return relativePath.ToString();
        }

        /// <summary>
        /// Gets the absolute path for 'relativePath' as it relates to 'originalPath' and 'currentPath'.
        /// </summary>
        /// <param name="originalPath">The path that 'relativePath' is relative to.</param>
        /// <param name="path">The new path used to determine the absolute path.</param>
        /// <param name="relativePath">The path that is relative to originalPath.</param>
        /// <returns></returns>
        public static string GetAbsolutePath( string originalPath, string currentPath, string relativePath )
        {
            if ( (originalPath != null) && (originalPath.ToLower() != currentPath.ToLower()) )
            {
                string[] originalPathSplit = originalPath.Replace( '/', '\\' ).Split( new char[] { Path.DirectorySeparatorChar } );
                string[] relativePathSplit = relativePath.Replace( '/', '\\' ).Split( new char[] { Path.DirectorySeparatorChar } );

                int relativeCount = 0;
                for ( int i = 0; i < relativePathSplit.Length; ++i )
                {
                    if ( relativePathSplit[i] == ".." )
                    {
                        ++relativeCount;
                    }
                    else
                    {
                        break;
                    }
                }

                if ( relativeCount == originalPathSplit.Length - 1 )
                {
                    return Path.GetFullPath( Path.Combine( originalPath.Replace( '/', '\\' ), relativePath.Replace( '/', '\\' ) ) );
                }
            }

            return Path.GetFullPath( Path.Combine( currentPath.Replace( '/', '\\' ), relativePath.Replace( '/', '\\' ) ) );
        }
        #endregion
    }

    /// <summary>
    /// A rageSerializableObject that is a list of items T.  Wraps the generic List.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [Serializable]
    public class rageSerializableList<T> : IRageClonableObject, IList<T>
    {
        public rageSerializableList()
        {
            Reset();
        }

        public rageSerializableList( IEnumerable<T> collection )
        {
            AddRange( collection );
        }

        public rageSerializableList( int capacity )
        {
            if ( capacity < 0 )
            {
                throw (new ArgumentOutOfRangeException( "The capacity is less than zero." ));
            }

            m_collection.Capacity = capacity;
        }

        #region Variables
        private bool m_isReadOnly = false;
        private List<T> m_collection = new List<T>();
        #endregion

        #region IList Interface
        [XmlIgnore]
        public T this[int index]
        {
            get
            {
                if ( (index < 0) || (index >= m_collection.Count) )
                {
                    throw (new ArgumentOutOfRangeException());
                }

                return m_collection[index];
            }
            set
            {
                if ( this.IsReadOnly )
                {
                    throw (new NotSupportedException( "The list is read-only." ));
                }

                if ( (index < 0) || (index >= m_collection.Count) )
                {
                    throw (new ArgumentOutOfRangeException());
                }

                m_collection[index] = value;
            }
        }

        public int IndexOf( T item )
        {
            return m_collection.IndexOf( item );
        }

        public void Insert( int index, T item )
        {
            if ( this.IsReadOnly )
            {
                throw (new NotSupportedException( "List is read-only." ));
            }

            if ( (index < 0) || (index > m_collection.Count) )
            {
                throw (new ArgumentOutOfRangeException( "Index is out of range." ));
            }

            m_collection.Insert( index, item );
        }

        public void RemoveAt( int index )
        {
            if ( this.IsReadOnly )
            {
                throw (new NotSupportedException( "List is read-only." ));
            }

            if ( (index < 0) || (index >= m_collection.Count) )
            {
                throw (new ArgumentOutOfRangeException( "Index is out of range." ));
            }

            m_collection.RemoveAt( index );
        }
        #endregion

        #region ICollection Interface
        [XmlIgnore]
        public int Count
        {
            get
            {
                return m_collection.Count;
            }
        }

        [XmlIgnore]
        public bool IsReadOnly
        {
            get
            {
                return m_isReadOnly;
            }
        }

        public void Add( T item )
        {
            if ( this.IsReadOnly )
            {
                throw (new NotSupportedException( "List is read-only." ));
            }

            m_collection.Add( item );
        }

        public void Clear()
        {
            if ( this.IsReadOnly )
            {
                throw (new NotSupportedException( "List is read-only." ));
            }

            m_collection.Clear();
        }

        public bool Contains( T item )
        {
            return m_collection.Contains( item );
        }

        public void CopyTo( T[] array, int arrayIndex )
        {
            if ( array == null )
            {
                throw (new ArgumentNullException( "The array is null." ));
            }

            if ( (arrayIndex < 0) || (arrayIndex >= m_collection.Count) )
            {
                throw (new ArgumentOutOfRangeException( "The index is out of range." ));
            }

            if ( array.Length < (m_collection.Count - arrayIndex) + 1 )
            {
                throw (new ArgumentException( "Array cannot hold all values." ));
            }

            m_collection.CopyTo( array, arrayIndex );
        }

        public bool Remove( T item )
        {
            if ( this.IsReadOnly )
            {
                throw (new NotSupportedException( "The list is read-only." ));
            }

            return m_collection.Remove( item );
        }
        #endregion

        #region IEnumerable<T> Interface
        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return new rageSerializableListEnumerator<T>( this );
        }
        #endregion

        #region IEnumerable Interface
        IEnumerator IEnumerable.GetEnumerator()
        {
            return new rageSerializableListEnumerator<T>( this );
        }
        #endregion

        #region Overrides
        public override bool Equals( object obj )
        {
            if ( obj == null )
            {
                return false;
            }

            if ( base.Equals( obj ) )
            {
                return true;
            }

            if ( obj is rageSerializableList<T> )
            {
                return Equals( obj as rageSerializableList<T> );
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        #endregion

        #region Public Virtual Functions
        public virtual object Clone()
        {
            rageSerializableList<T> collection = new rageSerializableList<T>();
            collection.CopyFrom( this );
            return collection;
        }

        public virtual void CopyFrom( IRageClonableObject other )
        {
            if ( other == null )
            {
                return;
            }

            if ( base.Equals( (object)other ) )
            {
                return;
            }

            if ( other is rageSerializableList<T> )
            {
                CopyFrom( other as rageSerializableList<T> );
            }
        }

        public virtual void Reset()
        {
            Clear();
        }
        #endregion

        #region Public Functions
        public void CopyFrom( rageSerializableList<T> other )
        {
            if ( other == null )
            {
                return;
            }

            if ( base.Equals( (object)other ) )
            {
                return;
            }

            Clear();
            AddRange( other );
        }

        public bool Equals( rageSerializableList<T> other )
        {
            if ( other == null )
            {
                return false;
            }

            if ( base.Equals( other as rageSerializableObject ) )
            {
                return true;
            }

            if ( this.Count != other.Count )
            {
                return false;
            }

            for ( int i = 0; i < this.Count; ++i )
            {
                if ( ((this[i] == null) && (other[i] != null))
                    || ((this[i] != null) && (other[i] == null)) )
                {
                    return false;
                }

                if ( !this[i].Equals( other[i] ) )
                {
                    return false;
                }
            }

            return true;
        }

        public void AddRange( IEnumerable<T> collection )
        {
            if ( collection == null )
            {
                throw (new ArgumentNullException( "The collection is null." ));
            }

            foreach ( T item in collection )
            {
                Add( item );
            }
        }
        #endregion
    }

    /// <summary>
    /// The enumerator for the rageSerializableList<T> class.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class rageSerializableListEnumerator<T> : IEnumerator<T>
    {
        public rageSerializableListEnumerator( rageSerializableList<T> collection )
        {
            m_list = collection;
        }

        #region Variables
        private int m_index = -1;
        private rageSerializableList<T> m_list = null;
        #endregion

        #region Properties
        object IEnumerator.Current
        {
            get
            {
                if ( m_index != -1 )
                {
                    return m_list[m_index];
                }

                return default( T );
            }
        }

        T IEnumerator<T>.Current
        {
            get
            {
                if ( m_index != -1 )
                {
                    return m_list[m_index];
                }

                return default( T );
            }
        }
        #endregion

        #region Public Functions
        public void Dispose()
        {
            // FIXME
        }

        public bool MoveNext()
        {
            // FIXME: If the collection is modified, throw InvalidOperationException

            ++m_index;
            return m_index < m_list.Count;
        }

        public void Reset()
        {
            // FIXME: If the collection is modified, throw InvalidOperationException

            m_index = -1;
        }
        #endregion
    }

    #region Some standard classes
    [Serializable]
    public class rageSerializableBooleanCollection : rageSerializableList<bool>
    {
        #region Overrides
        public override object Clone()
        {
            rageSerializableBooleanCollection collection = new rageSerializableBooleanCollection();
            collection.CopyFrom( this );
            return collection;
        }
        #endregion
    }

    [Serializable]
    public class rageSerializableDecimalCollection : rageSerializableList<decimal>
    {
        #region Overrides
        public override object Clone()
        {
            rageSerializableDecimalCollection collection = new rageSerializableDecimalCollection();
            collection.CopyFrom( this );
            return collection;
        }
        #endregion
    }

    [Serializable]
    public class rageSerializableFloatCollection : rageSerializableList<float>
    {
        #region Overrides
        public override object Clone()
        {
            rageSerializableFloatCollection collection = new rageSerializableFloatCollection();
            collection.CopyFrom( this );
            return collection;
        }
        #endregion
    }

    [Serializable]
    public class rageSerializableIntegerCollection : rageSerializableList<int>
    {
        #region Overrides
        public override object Clone()
        {
            rageSerializableIntegerCollection collection = new rageSerializableIntegerCollection();
            collection.CopyFrom( this );
            return collection;
        }
        #endregion
    }

    [Serializable]
    public class rageSerializableStringCollection : rageSerializableList<string>
    {
        #region Overrides
        public override object Clone()
        {
            rageSerializableStringCollection collection = new rageSerializableStringCollection();
            collection.CopyFrom( this );
            return collection;
        }
        #endregion
    }
    #endregion
}
