﻿//---------------------------------------------------------------------------------------------
// <copyright file="ParameterCollection.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Base.Xml
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Xml.Linq;
    using System.Xml.XPath;

    /// <summary>
    /// Wrapper around a collection of parCodeGen "ParameterDictionary" objects contained
    /// within a single file; so we can group parameter data together in a single file to
    /// reduce the number of XML configuration files we have (for example).
    /// </summary>
    /// These replace the old Parameter XML data as they are editable in the Metadata Editor.
    /// 
    /// This supports a hierarchy of parameter metadata files that are loaded in the
    /// following order:
    ///   1) filename.meta
    ///   2) filename.%COMPUTERNAME%.meta
    ///   3) filename.local.meta
    ///   
    /// This allows machine overrides (to be checked in) and local per-user overrides.
    /// Note: if there are machine or local overrides they will get saved out into the
    /// mentioned filename (where parameters loaded is not currently stored).
    /// 
    /// Note: this deliberately does not use the metadata abstraction library to reduce
    /// dependencies.
    /// 
    /// When we have C# class generation from PSC files this class could be integrated
    /// with that system.
    /// 
    [CollectionDataContract]
    [KnownType(typeof(ParameterDictionary))]
    [KnownType(typeof(List<ParameterDictionary>))]
    public class ParameterCollection : 
        ICollection<ParameterDictionary>
    {
        #region Properties
        /// <summary>
        /// Gets the number of Parameter Dictionaries contained in the collection.
        /// </summary>
        public int Count
        {
            get { return (this._parameterCollection.Count); }
        }

        /// <summary>
        /// Whether the collection is read-only (read-only variant not supported).
        /// </summary>
        public bool IsReadOnly
        {
            get { return (false); }
        }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Actual collection for storing our parameter dictionaries.
        /// </summary>
        [DataMember]
        private ICollection<ParameterDictionary> _parameterCollection;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ParameterCollection()
        {
            this._parameterCollection = new List<ParameterDictionary>();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Load new ParameterCollection object from stream.
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public static ParameterCollection Load(Stream stream)
        {
            XDocument xmlDoc = XDocument.Load(stream);
            ParameterCollection paramColl = new ParameterCollection();
            paramColl.LoadInline(xmlDoc.Root);

            return (paramColl);
        }

        /// <summary>
        /// Load new ParameterCollection object from file.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static ParameterCollection Load(String filename)
        {
            String directory = Path.GetDirectoryName(filename);
            String basename = Path.GetFileNameWithoutExtension(filename);
            String machineFilename = Path.Combine(directory, String.Format("{0}.{1}.meta", basename, Environment.MachineName));
            String localFilename = Path.Combine(directory, String.Format("{0}.local.meta", basename));

            ParameterCollection paramCollection = null;
            using (FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read))
            {
                paramCollection = Load(fs);
            }
            if (File.Exists(machineFilename))
            {
                XDocument xmlDoc = XDocument.Load(machineFilename);
                paramCollection.LoadInline(xmlDoc.Root);
            }
            if (File.Exists(localFilename))
            {
                XDocument xmlDoc = XDocument.Load(localFilename);
                paramCollection.LoadInline(xmlDoc.Root);
            }
            return (paramCollection);
        }

        /// <summary>
        /// Loads a parameter collection from an XElement, and adds them to passed parameterCollection;
        /// if the collection passed is null, we initialize it.
        /// </summary>
        /// <param name="parameterCollection"></param>
        /// <param name="xElement"></param>
        public static void LoadInto(ref ParameterCollection parameterCollection, XElement xElement)
        {
            if (parameterCollection == null)
                parameterCollection = new ParameterCollection();

            parameterCollection.LoadInline(xElement);
        }

        /// <summary>
        /// Save this parameter collection into a parCodeGen XML document stream.
        /// </summary>
        /// <param name="stream"></param>
        public void Save(Stream stream)
        {
            XDocument xmlDoc = new XDocument(
                new XDeclaration("1.0", "utf-8", "yes"),
                new XElement("ParameterCollection",
                    this._parameterCollection.Select(c => c.ToXElement("Item"))
                ) // ParameterCollection
            );
            xmlDoc.Save(stream);
        }

        /// <summary>
        /// Save this parameter collection into a parCodeGen file.
        /// </summary>
        /// <param name="filename"></param>
        public void Save(String filename)
        {
            using (FileStream fs = new FileStream(filename, FileMode.Create, FileAccess.Write))
            {
                Save(fs);
            }
        }
        #endregion // Controller Methods

        #region IEnumerable<ParameterDictionary> / IEnumerable Methods
        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns></returns>
        public IEnumerator<ParameterDictionary> GetEnumerator()
        {
            foreach (ParameterDictionary dictionary in _parameterCollection)
                yield return dictionary;
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns></returns>
        IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return (this.GetEnumerator());
        }
        #endregion //IEnumerable<ParameterDictionary> / IEnumerable Methods

        #region ICollection<ParameterDictionary> Methods
        public void Add(ParameterDictionary item)
        {
            throw (new NotImplementedException());
        }

        public void Clear()
        {
            throw (new NotImplementedException());
        }

        public bool Contains(ParameterDictionary item)
        {
            throw (new NotImplementedException());
        }

        public void CopyTo(ParameterDictionary[] array, int arrayIndex)
        {
            throw (new NotImplementedException());
        }

        public bool Remove(ParameterDictionary item)
        {
            throw (new NotImplementedException());
        }
        #endregion //ICollection<ParameterDictionary> Methods

        #region Private Methods
        /// <summary>
        /// Load XElement into our structure.
        /// </summary>
        /// <param name="xmlElem"></param>
        private void LoadInline(XElement xmlElem)
        {
            XElement xmlDictionariesElem = xmlElem.Element("ParameterDictionaries");
            if (null == xmlDictionariesElem)
                throw (new NotSupportedException("Missing ParameterDictionaries element."));

            IEnumerable<XElement> xmlDictionaryElems = xmlDictionariesElem.XPathSelectElements(
                "Item[@type='ParameterDictionary']");
            foreach (XElement xmlDictionaryElem in xmlDictionaryElems)
            {
                ParameterDictionary dictionary = new ParameterDictionary(xmlDictionaryElem);
                this._parameterCollection.Add(dictionary);
            }
        }
        #endregion // Private Methods
    }
    
} // RSG.Base.Xml namespace
