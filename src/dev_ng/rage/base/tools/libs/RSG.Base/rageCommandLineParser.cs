using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace RSG.Base
{
    /// <summary>
    /// This class parses a list of command line arguments based on its collection of <see cref="rageCommandLineItem"/>s.
    /// Items on the command line, except for the single unnamed argument, must start with a dash (-) followed by the name
    /// of the argument.  Arguments with a single value may optionally include the equals sign (=), but without spaces on 
    /// either side.  Example of valid arguments and their value(s):
    ///     c:\>myProgram.exe myFile.txt -arg2 -arg3=val3 -arg4 val4 -arg5 val5a val5b val5c
    /// In this example, arg1 (the unnamed argument) has Value of "myFile.txt", a string.
    ///     arg2.WasSet is true, while arg2.Value is string.Empty.
    ///     arg3.Value is equal to "val3", a string.
    ///     arg4.Value is equal to "val4", a string.
    ///     arg5.Value is equal to { "val5a", "val5b", val5c" }, a string array.
    /// </summary>
    [Obsolete("Use RSG.Base.OS.Getopt instead.")]
    public class rageCommandLineParser
    {
        /// <summary>
        /// Initializes a new <see cref="rageCommandLineParser"/> object.
        /// </summary>
        public rageCommandLineParser()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        #region Variables
        private Dictionary<string, rageCommandLineItem> m_items = new Dictionary<string, rageCommandLineItem>();
        private rageCommandLineItem m_noNameItem = null;
        private System.Text.StringBuilder m_error = null;
        #endregion

        #region Properties
        /// <summary>
        /// Retrieves the error string after <see cref="Parse"/> returns <c>false</c>.  Note that this contains the Usage string as well.
        /// </summary>
        public string Error
        {
            get
            {
                return (m_error == null) ? string.Empty : m_error.ToString();
            }
        }

        /// <summary>
        /// Retrieves the usage string
        /// </summary>
        public string Usage
        {
            get
            {
                return GetUsage();
            }
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Adds a <see cref="rageCommandLineItem"/> to the collection.  All names must be unique.  
        /// Only one <see cref="rageCommandLineItem"/> with an empty string name is permitted.
        /// </summary>
        /// <param name="item">The <see cref="rageCommandLineItem"/> to add.</param>
        /// <returns>The <see cref="rageCommandLineItem"/> that was added.</returns>
        public rageCommandLineItem AddItem( rageCommandLineItem item )
        {
            if ( item.Name == string.Empty )
            {
                m_noNameItem = item;
            }
            else
            {
                if ( !m_items.ContainsKey( item.Name ) )
                {
                    m_items.Add( item.Name, item );
                }
            }

            return item;
        }

        /// <summary>
        /// Parses args according to the <see cref="rageCommandLineItem"/>s in the collection.
        /// </summary>
        /// <param name="args">The arguments passed to the program.</param>
        /// <returns><c>true</c> if parsing was successful, otherwise <c>false</c>.</returns>
        public bool Parse( string[] args )
        {
            bool rtn = true;
            m_error = new System.Text.StringBuilder();

            try
            {
                for ( int i = 0; i < args.Length; ++i )
                {
                    rageCommandLineItem item = null;

                    string arg = args[i].Trim();
                    if ( arg[0] == '-' )
                    {
                        arg = arg.Substring( 1 );
                        if ( !m_items.ContainsKey( arg ) )
                        {
                            int indexOf = arg.IndexOf( "=" );
                            if ( indexOf > -1 )
                            {
                                arg = arg.Substring( 0, indexOf );
                            }
                        }

                        if ( m_items.TryGetValue( arg, out item ) )
                        {
                            if ( !GetValue( item, args, ref i ) )
                            {
                                rtn = false;
                                FindNextArgument( args, ref i );
                                break;
                            }
                        }
                        else
                        {
                            m_error.Append( "Unrecognized command '" );
                            m_error.Append( arg );
                            m_error.Append( "'.\n" );
                            
                            rtn = false;
                            FindNextArgument( args, ref i );
                            continue;
                        }
                    }
                    else if ( m_noNameItem != null )
                    {
                        --i;

                        if ( !GetValue( m_noNameItem, args, ref i ) )
                        {
                            rtn = false;
                            FindNextArgument( args, ref i );
                            break;
                        }
                    }
                }

                foreach ( KeyValuePair<string, rageCommandLineItem> pair in m_items )
                {
                    if ( pair.Value.Required && !pair.Value.WasSet )
                    {
                        m_error.Append( "Missing required argument '" );
                        m_error.Append( pair.Value.Name );
                        m_error.Append( "'.\n" );
                        rtn = false;
                    }
                }

                if ( (m_noNameItem != null) && m_noNameItem.Required && !m_noNameItem.WasSet )
                {
                    m_error.Append( "Missing the required unnamed argument.\n" );
                    rtn = false;
                }
            }
            catch ( Exception e )
            {
                m_error.Append( "Exception: " );
                m_error.Append( e.Message );
                m_error.Append( "\n" );
                rtn = false;
            }

            if ( !rtn )
            {                
                m_error.Append( GetUsage() );
            }

            return rtn;
        }
        #endregion

        #region Private Functions
        /// <summary>
        /// For the <see cref="rageCommandLineItem"/>, searches args, beginning at startIndex, for the appropriate values
        /// </summary>
        /// <param name="item">The <see cref="rageCommandLineItem"/> to match.</param>
        /// <param name="args">The command line arguments.</param>
        /// <param name="startIndex">The index in args at which to start searching.</param>
        /// <returns><c>false</c> if an error was encountered.</returns>
        private bool GetValue( rageCommandLineItem item, string[] args, ref int startIndex )
        {
            List<string> vals = new List<string>();
            int indexOf = -1;

            if ( item.NumExpectedValues <= (int)rageCommandLineItem.NumValues.None )
            {
                switch ( (rageCommandLineItem.NumValues)item.NumExpectedValues )
                {
                    case rageCommandLineItem.NumValues.None:
                        item.Value = string.Empty;	// this will make "WasSet" true
                        break;
                    case rageCommandLineItem.NumValues.NoneOrMore:

                        // if we find '=', we can only have one value
                        if ( startIndex > -1 )
                        {
                            indexOf = args[startIndex].IndexOf( "=" );
                        }

                        if ( indexOf > -1 )
                        {
                            vals.Add( args[startIndex].Substring( indexOf + 1 ).Trim() );
                        }
                        else
                        {
                            int j = startIndex + 1;
                            while ( (j < args.Length) && (args[j][0] != '-') )
                            {
                                vals.Add( args[j].Trim() );
                                ++j;
                            }
                            startIndex = j - 1;
                        }

                        if ( vals.Count == 0 )
                        {
                            item.Value = string.Empty;	// tells us we found the argument but it had no value
                        }
                        else
                        {
                            item.Value = vals.ToArray();
                        }
                        break;
                    case rageCommandLineItem.NumValues.NoneOrOne:
                        string val = string.Empty;

                        // if we find '=', we can only have one value
                        if ( startIndex > -1 )
                        {
                            indexOf = args[startIndex].IndexOf( "=" );
                        }

                        if ( indexOf > -1 )
                        {
                            val = args[startIndex].Substring( indexOf + 1 ).Trim();
                        }
                        else
                        {
                            int j = startIndex + 1;
                            if ( (j < args.Length) && (args[j][0] != '-') )
                            {
                                val = args[j].Trim();
                                ++j;
                            }
                            startIndex = j - 1;
                        }

                        item.Value = val;
                        break;
                    case rageCommandLineItem.NumValues.OneOrMore:
                        // if we find '=', we can only have one value
                        if ( startIndex > -1 )
                        {
                            indexOf = args[startIndex].IndexOf( "=" );
                        }

                        if ( indexOf > -1 )
                        {
                            vals.Add( args[startIndex].Substring( indexOf + 1 ).Trim() );
                        }
                        else
                        {
                            int j = startIndex + 1;
                            while ( (j < args.Length) && (args[j][0] != '-') )
                            {
                                vals.Add( args[j].Trim() );
                                ++j;
                            }
                            startIndex = j - 1;
                        }

                        if ( vals.Count > 0 )
                        {
                            item.Value = vals.ToArray();
                        }
                        else
                        {
                            m_error.Append( "One or more arguments required for '" );
                            m_error.Append( item.Name );
                            m_error.Append( "'.\n" );
                            return false;
                        }
                        break;
                }
            }
            else
            {
                // if we find '=', we can only have one value
                if ( startIndex > -1 )
                {
                    indexOf = args[startIndex].IndexOf( "=" );
                }

                if ( indexOf > -1 )
                {
                    vals.Add( args[startIndex].Substring( indexOf + 1 ).Trim() );
                }
                else
                {
                    int j = startIndex + 1;
                    while ( (j < args.Length) && (j - startIndex <= item.NumExpectedValues) && (args[j][0] != '-') )
                    {
                        vals.Add( args[j].Trim() );
                        ++j;
                    }
                    startIndex = j - 1;
                }

                if ( item.NumExpectedValues == vals.Count )
                {
                    if ( item.NumExpectedValues == 1 )
                    {
                        item.Value = vals[0];
                    }
                    else
                    {
                        item.Value = vals.ToArray();
                    }
                }
                else
                {
                    m_error.Append( "Incorrect number of arguments for '" );
                    m_error.Append( item.Name );
                    m_error.Append( "'.  Have " );
                    m_error.Append( vals.Count );
                    m_error.Append( " but needed " );
                    m_error.Append( item.NumExpectedValues );
                    m_error.Append( ".\n" );
                    return false;
                }
            }

            return true;
        }

        private void FindNextArgument( string[] args, ref int startIndex )
        {
            int i = startIndex + 1;
            while ( (i < args.Length) && (args[i][0] != '-') )
            {
                ++i;
            }
            startIndex = i - 1;
        }

        /// <summary>
        /// Builds a usage string using each <see cref="rageCommandLineItem"/> in the collection.
        /// </summary>
        /// <returns>The string usage.</returns>
        private string GetUsage()
        {
            int argColWidth = 4;	// minimum length of "ARG" + 1
            int valColWidth = 9;	// minimum length of "PARAM(S)" + 1
            int reqColWidth = 6;	// minimum length of "REQ.D" + 1
            int descColWidth = 0;

            if ( m_noNameItem != null )
            {
                // find max column width for each column
                if ( 9 > argColWidth )	// length of "(unnamed)"
                {
                    argColWidth = m_noNameItem.Name.Length + 2;
                }

                if ( m_noNameItem.ValueDescription.Length > valColWidth )
                {
                    valColWidth = m_noNameItem.ValueDescription.Length + 1;
                }
            }

            foreach ( KeyValuePair<string, rageCommandLineItem> pair in m_items )
            {
                if ( pair.Value.Name.Length > argColWidth )
                {
                    argColWidth = pair.Value.Name.Length + 2;
                }

                if ( pair.Value.ValueDescription.Length > valColWidth )
                {
                    valColWidth = pair.Value.ValueDescription.Length + 1;
                }
            }

            descColWidth = 78 - argColWidth - valColWidth - reqColWidth;

            System.Text.StringBuilder usage = new System.Text.StringBuilder( "Usage:\n" );
            usage.Append( GetUsageLine( "ARG", "PARAM(S)", "REQ.D", "DESCRIPTION",
                argColWidth, valColWidth, reqColWidth, descColWidth ) );
            usage.Append( GetUsageLine( "---", "--------", "-----", "-----------",
                argColWidth, valColWidth, reqColWidth, descColWidth ) );

            if ( m_noNameItem != null )
            {
                usage.Append( GetUsageLine( "(unnamed)", m_noNameItem.ValueDescription, m_noNameItem.Required ? "Y" : "N", m_noNameItem.Description,
                    argColWidth, valColWidth, reqColWidth, descColWidth ) );
            }

            foreach ( KeyValuePair<string, rageCommandLineItem> pair in m_items )
            {
                usage.Append( GetUsageLine( "-" + pair.Value.Name, pair.Value.ValueDescription,
                    pair.Value.Required ? "Y" : "N", pair.Value.Description, argColWidth, valColWidth, reqColWidth, descColWidth ) );
            }

            return usage.ToString();
        }

        /// <summary>
        /// Builds a usage string with the given arguments and column widths, wrapping to a new line if necessary.
        /// </summary>
        /// <param name="arg">The name of the <see cref="rageCommandLineItem"/>.</param>
        /// <param name="val">The value description of the <see cref="rageCommandLineItem"/></param>
        /// <param name="required"><c>true</c> if the <see cref="rageCommandLineItem"/> is required, otherwise <c>false</c>.</param>
        /// <param name="description">The description of the <see cref="rageCommandLineItem"/>.</param>
        /// <param name="argWidth">The width of the arg column.</param>
        /// <param name="valWidth">The width of the value column.</param>
        /// <param name="reqWidth">The width of the required column.</param>
        /// <param name="descWidth">The width of the description column.</param>
        /// <returns>The string usage for this line.</returns>
        private string GetUsageLine( string arg, string val, string required, string description,
            int argWidth, int valWidth, int reqWidth, int descWidth )
        {
            int i;

            System.Text.StringBuilder usageLine = new System.Text.StringBuilder();

            usageLine.Append( arg );
            i = argWidth;
            while ( i > arg.Length )
            {
                usageLine.Append( " " );
                --i;
            }

            usageLine.Append( val );
            i = valWidth;
            while ( i > val.Length )
            {
                usageLine.Append( " " );
                --i;
            }

            usageLine.Append( required );
            i = reqWidth;
            while ( i > required.Length )
            {
                usageLine.Append( " " );
                --i;
            }

            if ( description.Length < descWidth )
            {
                usageLine.Append( description + "\n" );
            }
            else
            {
                string line = GetWrappedLine( description, descWidth );
                usageLine.Append( line + "\n" );

                int start = line.Length + 1;
                while ( start < description.Length )
                {
                    i = argWidth + valWidth + reqWidth;
                    while ( i > 0 )
                    {
                        usageLine.Append( " " );
                        --i;
                    }

                    line = GetWrappedLine( description.Substring( start ), descWidth );
                    usageLine.Append( line + "\n" );
                    start += line.Length + 1;
                }
            }

            return usageLine.ToString();
        }

        /// <summary>
        /// Trims the line to no more than maxLength characters without splitting a word in two.
        /// </summary>
        /// <param name="line">The line to build from.</param>
        /// <param name="maxLength">The maximum number of characters in the new line.</param>
        /// <returns>The new line.</returns>
        private string GetWrappedLine( string line, int maxLength )
        {
            if ( line.Length > maxLength )
            {
                string[] lineSplit = line.Trim().Split( ' ' );

                if ( lineSplit.Length > 0 )
                {
                    line = lineSplit[0];
                    for ( int i = 1; i < lineSplit.Length; ++i )
                    {
                        if ( line.Length + lineSplit[i].Length + 1 < maxLength )
                        {
                            line += " " + lineSplit[i];
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }

            return line;
        }
        #endregion
    }

    /// <summary>
    /// This class represents a single command line argument with optional value(s) and descriptive text for printing usage.
    /// </summary>
    public class rageCommandLineItem
    {
        /// <summary>
        /// Constructs a new <see cref="rageCommandLineItem"/> object that expects numValues values.
        /// </summary>
        /// <param name="name">The name of the argument.  Spaces are not allowed.</param>
        /// <param name="numValues">The number of values expected.</param>
        /// <param name="required"><c>true</c> if program execution cannot continue unless this argument is found on the command line.</param>
        /// <param name="valueDescription">A description of the argument's value(s).  Typical practice is to surround the text with bracket it is not required.</param>
        /// <param name="description">A description for this <c>rageCommandLineItem</c>.</param>
        public rageCommandLineItem( string name, int numValues, bool required, string valueDescription, string description )
        {
            m_name = name;
            m_numValues = numValues;
            m_required = required;
            m_valueDescription = valueDescription;
            m_description = description;

            if ( m_name.StartsWith( "-" ) )
            {
                m_name = m_name.Substring( 1 ).Trim();
            }

            Debug.Assert( m_name.IndexOf( ' ' ) == -1 );
        }

        /// <summary>
        /// Constructs a new <see cref="rageCommandLineItem"/> object that expects the number of values defined by the <see cref="NumValues"/>
        /// enumeration.
        /// </summary>
        /// <param name="name">The name of the argument, minus the dash (-).</param>
        /// <param name="numValues">A <see cref="NumValues"/> enumeration specifying a range of values expected.</param>
        /// <param name="required"><c>true</c> if program execution cannot continue unless this argument is found on the command line.</param>
        /// <param name="valueDescription">A description of the argument's value(s).  Typical practice is to surround the text with bracket it is not required.</param>
        /// <param name="description">A description for this <c>rageCommandLineItem</c>.</param>
        public rageCommandLineItem( string name, NumValues numValues, bool required, string valueDescription, string description )
            : this( name, (int)numValues, required, valueDescription, description )
        {
        }

        #region Enums
        public enum NumValues
        {
            NoneOrOne = -3,
            NoneOrMore,
            OneOrMore,
            None,
        }
        #endregion

        #region Variables
        private string m_name = string.Empty;
        private int m_numValues = 0;
        private bool m_required = false;
        private string m_valueDescription = string.Empty;
        private string m_description = string.Empty;
        private object m_value = null;
        #endregion

        #region Properties
        /// <summary>
        /// Gets the name of the argument
        /// </summary>
        public string Name
        {
            get
            {
                return m_name;
            }
        }

        /// <summary>
        /// Gets the number of expected values.  Negative numbers represent NumValues enums
        /// </summary>
        public int NumExpectedValues
        {
            get
            {
                return m_numValues;
            }
        }

        /// <summary>
        /// Gets whether this argument is required or not.
        /// </summary>
        public bool Required
        {
            get
            {
                return m_required;
            }
        }

        /// <summary>
        /// Gets the description of the value(s) expected, if any.
        /// </summary>
        public string ValueDescription
        {
            get
            {
                return m_valueDescription;
            }
        }

        /// <summary>
        /// Gets the description of the argument.
        /// </summary>
        public string Description
        {
            get
            {
                return m_description;
            }
        }

        /// <summary>
        /// Gets or sets the value found on the command line.  Is either a <c>string</c> or a <c>string[]</c>.
        /// </summary>
        public object Value
        {
            get
            {
                return m_value;
            }

            set
            {
                if ( m_value != null )
                {
                    // allow merging of lists
                    if ( (m_value is string[]) && (value is string[]) )
                    {
                        string[] list1 = m_value as string[];
                        string[] list2 = value as string[];
                        string[] list3 = new string[list1.Length + list2.Length];
                        list1.CopyTo( list3, 0 );
                        list2.CopyTo( list3, list1.Length );
                        m_value = list3;
                        return;
                    }
                }

                m_value = value;
            }
        }

        /// <summary>
        /// Returns <c>true</c> if this argument was found.
        /// </summary>
        public bool WasSet
        {
            get
            {
                return m_value != null;
            }
        }
        #endregion
    }
}
