﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Textures.DDS
{
    public class DDSFormatException : Exception
    {
        public DDSFormatException(string message) : base(message)
        {
        }
    }
}
