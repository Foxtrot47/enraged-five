﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;

namespace RSG.Base.Textures.DDS
{
    public class Header
    {
        #region Constructors
        private Header(BinaryReader binaryReader)
        {
            uint size = binaryReader.ReadUInt32();
            if (size != expectedSize_)
                throw new DDSFormatException(String.Format("DDS_HEADER has invalid size of {0} (expected {1}).", size, expectedSize_));

            Flags = (HeaderFlags)binaryReader.ReadUInt32();
            Width = binaryReader.ReadUInt32();
            Height = binaryReader.ReadUInt32();
            PitchOrLinearSize = binaryReader.ReadUInt32();
            Depth = binaryReader.ReadUInt32();
            MipMapCount = binaryReader.ReadUInt32();
            for (int reserved1Index = 0; reserved1Index < 11; ++reserved1Index)
                binaryReader.ReadUInt32();// unused reserved1[11]
            PixelFormat = new PixelFormat(binaryReader);
            Caps = (HeaderCaps)binaryReader.ReadUInt32();
            Caps2 = (HeaderCaps2)binaryReader.ReadUInt32();
            binaryReader.ReadUInt32();// unused caps3
            binaryReader.ReadUInt32();// unused caps4
            binaryReader.ReadUInt32();// unused reserved2
        }
        #endregion Constructors

        #region Static Methods
        public static Header Load(string pathname)
        {
            return Load(File.OpenRead(pathname));
        }

        public static Header Load(Stream stream)
        {
            using (BinaryReader binaryReader = new BinaryReader(stream))
            {
                uint magic = binaryReader.ReadUInt32();
                if (magic != expectedMagic_)
                    throw new DDSFormatException("Not a DDS file.");

                return new Header(binaryReader);
            }
        }
        #endregion Static Methods

        #region Enums
        [Flags]
        public enum HeaderFlags
        {
            Caps        = 0x00000001,
            Height      = 0x00000002,
            Width       = 0x00000004,
            Pitch       = 0x00000008,
            PixelFormat = 0x00001000,
            MipMapCount = 0x00020000,
            LinearSize  = 0x00080000,
            Depth       = 0x00800000
        }

        [Flags]
        public enum HeaderCaps
        {
            Complex = 0x00000008,
            MipMap  = 0x00400000,
            Texture = 0x00001000
        }

        [Flags]
        public enum HeaderCaps2
        {
            Cubemap             = 0x00000200,
            CubemapPositiveX    = 0x00000400,
            CubemapNegativeX    = 0x00000800,
            CubemapPositiveY    = 0x00001000,
            CubemapNegativeY    = 0x00002000,
            CubemapPositiveZ    = 0x00004000,
            CubemapNegativeZ    = 0x00008000,
            Volume              = 0x00200000
        }
        #endregion Enums

        #region Public Data
        public HeaderFlags Flags { get; private set; }
        public uint Height { get; private set; }
        public uint Width { get; private set; }
        public uint PitchOrLinearSize { get; private set; }
        public uint Depth { get; private set; }
        public uint MipMapCount { get; private set; }
        public PixelFormat PixelFormat { get; private set; }
        public HeaderCaps Caps { get; private set; }
        public HeaderCaps2 Caps2 { get; private set; }
        #endregion

        #region Static Data
        private static readonly uint expectedMagic_ = 0x20534444;
        private static readonly uint expectedSize_ = 124;
        #endregion Static Data
    }
}
