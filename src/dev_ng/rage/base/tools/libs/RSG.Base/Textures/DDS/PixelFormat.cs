﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;

namespace RSG.Base.Textures.DDS
{
    public class PixelFormat
    {
        #region Constructors
        internal PixelFormat(BinaryReader binaryReader)
        {
            uint size = binaryReader.ReadUInt32();
            if (size != expectedSize_)
                throw new DDSFormatException(String.Format("DDS_PIXELFORMAT has invalid size of {0} (expected {1}).", size, expectedSize_));

            Flags = (PixelFormatFlags)binaryReader.ReadUInt32();
            FourCC = binaryReader.ReadUInt32();
            RGBBitCount = binaryReader.ReadUInt32();
            RedBitMask = binaryReader.ReadUInt32();
            GreenBitMask = binaryReader.ReadUInt32();
            BlueBitMask = binaryReader.ReadUInt32();
            AlphaBitMask = binaryReader.ReadUInt32();
        }
        #endregion Constructors

        #region Enums
        [Flags]
        public enum PixelFormatFlags
        {
            AlphaPixels = 0x00000001,
            Alpha       = 0x00000002,
            FourCC      = 0x00000004,
            RGB         = 0x00000040,
            YUV         = 0x00000200,
            Luminance   = 0x0002000054
        }

        public enum PixelFormatCompressedFormat
        {
            None,
            DXT1,
            DXT2,
            DXT3,
            DXT4,
            DXT5,
            DX10,
            Unknown
        }
        #endregion Enums

        #region Public Data
        public PixelFormatFlags Flags { get; private set; }
        public uint FourCC { get; private set; }
        public uint RGBBitCount { get; private set; }
        public uint RedBitMask { get; private set; }
        public uint GreenBitMask { get; private set; }
        public uint BlueBitMask { get; private set; }
        public uint AlphaBitMask { get; private set; }

        public PixelFormatCompressedFormat CompressedFormat
        {
            get
            {
                if (Flags.HasFlag(PixelFormatFlags.FourCC))
                {
                    if (FourCC == FourCCDXT1_)
                        return PixelFormatCompressedFormat.DXT1;
                    else if (FourCC == FourCCDXT2_)
                        return PixelFormatCompressedFormat.DXT2;
                    else if (FourCC == FourCCDXT3_)
                        return PixelFormatCompressedFormat.DXT3;
                    else if (FourCC == FourCCDXT4_)
                        return PixelFormatCompressedFormat.DXT4;
                    else if (FourCC == FourCCDXT5_)
                        return PixelFormatCompressedFormat.DXT5;
                    else if (FourCC == FourCCDX10_)
                        return PixelFormatCompressedFormat.DX10;
                    else
                        return PixelFormatCompressedFormat.Unknown;
                }

                return PixelFormatCompressedFormat.None;
            }
        }
        #endregion

        #region Static Data
        private static readonly uint expectedSize_ = 32;
        private static readonly uint FourCCDXT1_ = 0x31545844;
        private static readonly uint FourCCDXT2_ = 0x32545844;
        private static readonly uint FourCCDXT3_ = 0x33545844;
        private static readonly uint FourCCDXT4_ = 0x34545844;
        private static readonly uint FourCCDXT5_ = 0x35545844;
        private static readonly uint FourCCDX10_ = 0x30315844;
        #endregion Static Data
    }
}
