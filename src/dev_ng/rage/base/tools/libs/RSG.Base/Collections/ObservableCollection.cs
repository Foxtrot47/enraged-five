﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections.ObjectModel;
using System.Linq.Expressions;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Runtime.InteropServices;

namespace RSG.Base.Collections
{
    public interface IBeginEndCollection
    {
        void BeginUpdate();

        void EndUpdate();
    }

    public interface IEventClearer
    {
        ArrayList RemoveAllHandlersOnCollectionChangedEvent();
        void AddHandlersOnCollectionChangedEvent(ArrayList delegates);
    }

    /// <summary>
    /// An observable collection that also fires events for when the 
    /// collection is about to get something added to it or removed from it
    /// </summary>
    [Obsolete("Use the built in one instead")]
    public class ObservableCollection<T> : Collection<T>, IBeginEndCollection, IEventClearer,
        INotifyPropertyChanged, INotifyPropertyChanging, INotifyCollectionChanged, INotifyCollectionChanging
    {
        #region Change events

        #region INotifyCollection

        private ArrayList CollectionChangedDelegates = new ArrayList();
        private event NotifyCollectionChangedEventHandler m_CollectionChanged;
        public virtual event NotifyCollectionChangedEventHandler CollectionChanged
        {
            add 
            {
                m_CollectionChanged += value;
                CollectionChangedDelegates.Add(value);
            }
            remove 
            {
                m_CollectionChanged -= value;
                CollectionChangedDelegates.Remove(value);
            }
        }

        public ArrayList RemoveAllHandlersOnCollectionChangedEvent()
        {
            foreach (NotifyCollectionChangedEventHandler handle in this.CollectionChangedDelegates)
            {
                m_CollectionChanged -= handle;
            }

            ArrayList returnValue = new ArrayList(this.CollectionChangedDelegates);
            this.CollectionChangedDelegates.Clear();
            return returnValue;
        }
        public void AddHandlersOnCollectionChangedEvent(ArrayList delegates)
        {
            foreach (NotifyCollectionChangedEventHandler handle in delegates)
            {
                m_CollectionChanged += handle;
                this.CollectionChangedDelegates.Add(handle);
            }
        }

        event NotifyCollectionChangingEventHandler INotifyCollectionChanging.CollectionChanging
        {
            add { CollectionChanging += value; }
            remove { CollectionChanging -= value; }
        }

        public virtual event NotifyCollectionChangingEventHandler CollectionChanging;


        #endregion // INotifyCollection

        #region INotifyProperty

        event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged
        {
            add { PropertyChanged += value; }
            remove { PropertyChanged -= value; }
        }

        public virtual event PropertyChangedEventHandler PropertyChanged;

        event PropertyChangingEventHandler INotifyPropertyChanging.PropertyChanging
        {
            add { PropertyChanging += value; }
            remove { PropertyChanging -= value; }
        }

        public virtual event PropertyChangingEventHandler PropertyChanging;

        #endregion // INotifyProperty

        #endregion // Change events

        #region Constructor(s)

        public ObservableCollection()
        {
        }

        public ObservableCollection(IEnumerable<T> items)
        {
            foreach (var item in items)
            {
                this.Add(item);
            }
        }

        public ObservableCollection(IList<T> items)
        {
            foreach (var item in items)
            {
                this.Add(item);
            }
        }

        #endregion // Constructor(s)

        #region Properties

        private Boolean m_fireEvents = true;

        #endregion // Properties
        #region Public Functions
        /// <summary>
        /// 
        /// </summary>
        /// <param name="collection"></param>
        public virtual void AddRange(IEnumerable<T> collection)
        {
            OnRangeBeingAdded(collection);

            foreach (T i in collection) 
                Items.Add( i );

            OnRangeAdded(collection);
            //OnCollectionChanged( new NotifyCollectionChangedEventArgs( NotifyCollectionChangedAction.Reset ) );
        }
        #endregion
        #region Override Functions

        /// <summary>
        /// Override the clear items function to make sure that the correct events get called for it.
        /// </summary>
        protected override void ClearItems()
        {
            OnStackBeingCleared();

            base.ClearItems();

            OnStackCleared();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <param name="item"></param>
        protected override void SetItem(int index, T item)
        {
            OnEntryBeingReplaced(item, index);

            base.SetItem(index, item);

            OnEntryReplaced(item, index);
        }

        /// <summary>
        /// Override the insert item function to make sure that the correct events get called for it.
        /// </summary>
        protected override void InsertItem(int index, T item)
        {
            OnEntryBeingAdded(item, index);

            base.InsertItem(index, item);

            OnEntryAdded(item, index);
        }

        /// <summary>
        /// Override the remove item function to make sure that the correct events get called for it.
        /// </summary>
        protected override void RemoveItem(int index)
        {
            T removedItem = this[index];
            OnEntryBeingRemoved(removedItem, index);

            base.RemoveItem(index);

            OnEntryRemoved(removedItem, index);
        }

        ///// <summary>          
        ///// Clears the current collection and replaces it with the specified item.
        ///// /// </summary> 
        //public void Replace(T item)
        //{
        //    ReplaceRange(new T[] { item });
        //} 

        ///// <summary>
        ///// Replace the entire collection with the given list
        ///// </summary>
        ///// <param name="collection"></param>
        //protected void ReplaceRange(IEnumerable<T> collection)
        //{


        //    List<T> old = new List<T>(Items);
        //    ClearItems();
        //    foreach (var item in collection)
        //    {
        //        Items.Add(item);
        //    }

        //}

        #endregion // Override Function(s)

        #region Private Function(s)

        /// <summary>
        /// Gets called before the collection is cleared
        /// </summary>
        private void OnStackBeingCleared()
        {
            List<Object> currentItems = new List<Object>();
            foreach (Object obj in this)
            {
                currentItems.Add(obj);
            }

            FirePropertyChangingNotifications();
            FireBeingClearedNotifications(currentItems);
        }

        /// <summary>
        /// Get called after the collection has been cleared.
        /// </summary>
        private void OnStackCleared()
        {
            FirePropertyChangedNotifications();
            FireClearedNorifications();
        }

        /// <summary>
        /// Called before a item gets added to the collection
        /// </summary>
        /// <param name="item">The item getting added</param>
        private void OnEntryBeingAdded(T item, int index)
        {
            FirePropertyChangingNotifications();
            FireEntryBeingAddedNotifications(item, index);
        }

        /// <summary>
        /// Called after a item gets added to the collection
        /// </summary>
        /// <param name="item">The item getting added</param>
        private void OnEntryAdded(T item, int index)
        {
            FirePropertyChangedNotifications();
            FireEntryAddedNotifications(item, index);
        }

        /// <summary>
        /// Called before an item range gets added to the collection
        /// </summary>
        /// <param name="item">The item getting added</param>
        private void OnRangeBeingAdded(IEnumerable<T> changedItems)
        {
            FirePropertyChangingNotifications();
            FireRangeBeingAddedNotifications( changedItems );
        }

        /// <summary>
        /// Called after an item range  gets added to the collection
        /// </summary>
        /// <param name="item">The item getting added</param>
        private void OnRangeAdded(IEnumerable<T> changedItems)
        {
            FirePropertyChangedNotifications();
            FireRangeAddedNotifications(changedItems);
        }
        /// <summary>
        /// Called before a item gets popped from the stack
        /// </summary>
        /// <param name="item">The item getting popped off</param>
        private void OnEntryBeingRemoved(T item, int index)
        {
            FirePropertyChangingNotifications();
            FireEntryBeingRemovedNotifications(item, index);
        }

        /// <summary>
        /// Called after a item gets popped off from the stack
        /// </summary>
        /// <param name="item">The item popped off</param>
        private void OnEntryRemoved(T item, int index)
        {
            FirePropertyChangedNotifications();
            FireEntryRemovedNotifications(item, index);
        }

        /// <summary>
        /// Called before an item gets replaced
        /// </summary>
        /// <param name="item"></param>
        /// <param name="index"></param>
        private void OnEntryBeingReplaced(T item, int index)
        {
            FirePropertyChangingNotifications();
            FireEntryBeingReplacedNotifications(item, index);
        }
        
        /// <summary>
        /// Called after an item gets replaced
        /// </summary>
        /// <param name="item"></param>
        /// <param name="index"></param>
        private void OnEntryReplaced(T item, int index)
        {
            FirePropertyChangedNotifications();
            FireEntryReplacedNotifications(item, index);
        }

        #endregion // Private Function(s)

        #region Public Function(s)

        /// <summary>
        /// Sets it so that the collection doesn't fire any events until the 
        /// end update function is called.
        /// </summary>
        public void BeginUpdate()
        {
            m_fireEvents = false;
        }

        /// <summary>
        /// Sets it so that the collection fires events for any modifing that
        /// goes on with the collection. 
        /// </summary>
        public void EndUpdate()
        {
            m_fireEvents = true;

            FirePropertyChangedNotifications();
            FireClearedNorifications();
        }

        #endregion // Public Functions(s)

        #region Property events

        /// <summary>
        /// Gets called whenever a property changes, this just fires off the actual event with the 
        /// correct property name.
        /// </summary>
        /// <param name="propertyName">The property that the event is being fired for</param>
        protected virtual void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null && this.m_fireEvents == true)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Gets called whenever a property is about to change, this just fires off the actual event with the 
        /// correct property name.
        /// </summary>
        /// <param name="propertyName">The property that the event is being fired for</param>
        protected virtual void OnPropertyChanging(String propertyName)
        {
            if (PropertyChanging != null && this.m_fireEvents == true)
                PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
        }

        /// <summary>
        /// Get called whenever a method is called and the properties have change. This
        /// makes sure that every property in this class gets the correct property changed event fired for it.
        /// </summary>
        private void FirePropertyChangedNotifications()
        {
            OnPropertyChanged("Count");
            OnPropertyChanged("Item");
            OnPropertyChanged("Items");
        }

        /// <summary>
        /// Get called whenever a method is called and the properties are about to change. This
        /// makes sure that every property in this class gets the correct property changing event fired for it.
        /// </summary>
        private void FirePropertyChangingNotifications()
        {
            OnPropertyChanging("Count");
            OnPropertyChanging("Item");
            OnPropertyChanging("Items");
        }

        #endregion // Property events

        #region Collection events

        /// <summary>
        /// Gets called whenever the stack collection changes, this just fires off the actual event with the 
        /// correct property name.
        /// </summary>
        protected virtual void OnCollectionChanged(NotifyCollectionChangedEventArgs args)
        {
            if (m_CollectionChanged != null && this.m_fireEvents == true)
                m_CollectionChanged(this, args);
        }

        /// <summary>
        /// Gets called whenever the stack collection is about to change, this just fires off the actual event with the 
        /// correct property name.
        /// </summary>
        protected virtual void OnCollectionChanging(NotifyCollectionChangingEventArgs args)
        {
            if (CollectionChanging != null && this.m_fireEvents == true)
                CollectionChanging(this, args);
        }

        /// <summary>
        /// Gets called whenever a item is about to get added.
        /// </summary>
        /// <param name="item">The item that is being added</param>
        private void FireEntryBeingAddedNotifications(T item, int index)
        {
            OnCollectionChanging(new NotifyCollectionChangingEventArgs(NotifyCollectionChangingAction.Add, item, index));
        }

        /// <summary>
        /// Get called whenever a item has been added
        /// </summary>
        /// <param name="item">The item that has been added</param>
        private void FireEntryAddedNotifications(T item, int index)
        {
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item, index));
        }

        /// <summary>
        /// Gets called whenever an item range is about to get added.
        /// </summary>
        /// <param name="item">The item that is being added</param>
        private void FireRangeBeingAddedNotifications(IEnumerable<T> changedItems)
        {
            OnCollectionChanging( new NotifyCollectionChangingEventArgs( NotifyCollectionChangingAction.Reset, changedItems ) );
            //OnCollectionChanging( new NotifyCollectionChangingEventArgs( NotifyCollectionChangingAction.Add, changedItems ) );
        }

        /// <summary>
        /// Get called whenever an item range has been added
        /// </summary>
        /// <param name="item">The item that has been added</param>
        private void FireRangeAddedNotifications(IEnumerable<T> changedItems)
        {
            OnCollectionChanged( new NotifyCollectionChangedEventArgs( NotifyCollectionChangedAction.Reset ) );
            //OnCollectionChanged( new NotifyCollectionChangedEventArgs( NotifyCollectionChangedAction.Add, changedItems ) );
        }
        /// <summary>
        /// Get called whenever a item is about to get removed.
        /// </summary>
        /// <param name="item">The item that is getting removed</param>
        private void FireEntryBeingRemovedNotifications(T item, int index)
        {
            OnCollectionChanging(new NotifyCollectionChangingEventArgs(NotifyCollectionChangingAction.Remove, item, index));
        }

        /// <summary>
        /// Get called whenever a item has been removed
        /// </summary>
        /// <param name="item">The item that has been removed</param>
        private void FireEntryRemovedNotifications(T item, int index)
        {
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item, index));
        }

        /// <summary>
        /// Get called whenever a item is about to get removed.
        /// </summary>
        /// <param name="item">The item that is getting removed</param>
        private void FireEntryBeingReplacedNotifications(T item, int index)
        {
            OnCollectionChanging(new NotifyCollectionChangingEventArgs(NotifyCollectionChangingAction.Replace, item, index));
        }

        /// <summary>
        /// Get called whenever a item has been removed
        /// </summary>
        /// <param name="item">The item that has been removed</param>
        private void FireEntryReplacedNotifications(T item, int index)
        {
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, item, index));
        }

        /// <summary>
        /// Get called whenever the collection is about to be cleared
        /// </summary>
        private void FireBeingClearedNotifications(List<Object> items)
        {
            OnCollectionChanging(new NotifyCollectionChangingEventArgs(NotifyCollectionChangingAction.Reset, items));
        }

        /// <summary>
        /// Get called whenever the stack has been cleared
        /// </summary>
        /// <param name="items"></param>
        private void FireClearedNorifications()
        {
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        #endregion // Collection events

    } // RSGObservableCollection
} // RSG.Base.Collections
