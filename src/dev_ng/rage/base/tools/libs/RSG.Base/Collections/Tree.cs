//
// File: Tree.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of Tree.cs class
//

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace RSG.Base.Collections
{

    /// <summary>
    /// Generic tree structure.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Tree<T>
    {
        #region Enumerations
        /// <summary>
        /// Tree node-walking mode.
        /// </summary>
        public enum WalkMode
        {
            DepthFirst,
            BreadthFirst
        }
        #endregion // Enumerations

        #region Properties and Associated Member Data
        /// <summary>
        /// Tree root node.
        /// </summary>
        public TreeNode<T> Root
        {
            get { return m_Root; }
            set { m_Root = value; }
        }
        private TreeNode<T> m_Root;

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<TreeNode<T>> DepthFirstSearch
        {
            get { return this.Root.DepthFirstSearch; }
        }
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public Tree()
        {
            this.Root = null;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="root"></param>
        public Tree(TreeNode<T> root)
        {
            this.Root = root;
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Clear all children.
        /// </summary>
        public void Clear()
        {
            this.Root = null;
        }
        #endregion // Controller Methods
    }

    /// <summary>
    /// Binary Tree Node data structure.
    /// </summary>
    public class TreeNode<T> : Node<T>
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<TreeNode<T>> DepthFirstSearch
        {
            get { return ScanDepthFirst(this); }
        }
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public TreeNode()
            : base()
        {
            this.Neighbours = new NodeList<T>();
        }

        /// <summary>
        /// Constructor, specifying node data.
        /// </summary>
        /// <param name="data">Node data</param>
        public TreeNode(T data)
            : base(data)
        {
            this.Neighbours = new NodeList<T>();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public TreeNode<T> this[int index]
        {
            get
            {
                if (base.Neighbours == null)
                    return (null);
                else
                    return (TreeNode<T>)(base.Neighbours[index]);
            }
            set
            {
                if (base.Neighbours == null)
                    base.Neighbours = new NodeList<T>();

                base.Neighbours[index] = value;
            }
        }
        #endregion // Controller Methods

        #region Protected Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="root"></param>
        /// <returns></returns>
        protected IEnumerable<TreeNode<T>> ScanDepthFirst(TreeNode<T> root)
        {
            yield return (root);
            foreach (TreeNode<T> t in root.Neighbours)
                foreach (TreeNode<T> n in ScanDepthFirst(t))
                    yield return (n);
        }
        #endregion // Protected Methods
    }

} // RSG.Base.Collections

// Tree.cs
