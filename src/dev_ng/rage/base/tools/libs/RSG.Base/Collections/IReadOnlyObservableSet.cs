﻿//---------------------------------------------------------------------------------------------
// <copyright file="IReadOnlyObservableSet.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Base.Collections
{
    using System.Collections;
    using System.Collections.Specialized;

    /// <summary>
    /// Represents a non-generic set that notifies listeners of dynamic changes.
    /// </summary>
    public interface IReadOnlyObservableSet : IEnumerable, INotifyCollectionChanged
    {
        #region Properties
        /// <summary>
        /// Gets the number of elements actually contained in the
        /// <see cref="IReadOnlyObservableSet"/>.
        /// </summary>
        int Count { get; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Determines whether the <see cref="IReadOnlyObservableSet"/> contains a specific
        /// item.
        /// </summary>
        /// <param name="item">
        /// The object to locate in the <see cref="IReadOnlyObservableSet"/>.
        /// </param>
        /// <returns>
        /// True if System.Object is found in the <see cref="IReadOnlyObservableSet"/>;
        /// otherwise, false.
        /// </returns>
        bool Contains(object item);
        #endregion Methods
    } // RSG.Base.Collections.IReadOnlyObservableSet {Interface}
} // RSG.Base.Collections {Namespace}
