//
// File: BinaryTree.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of a simple BinaryTree data structure.
//

using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace RSG.Base.Collections
{

    /// <summary>
    /// Binary Tree data structure.
    /// </summary>
    public class BinaryTree<T>
    {
        #region Enumerations
        /// <summary>
        /// BinaryTree node-walking mode.
        /// </summary>
        public enum WalkMode
        {
            DepthFirst,
            BreadthFirst
        }
        #endregion // Enumerations

        #region Properties and Associated Member Data
        /// <summary>
        /// Binary Tree root node.
        /// </summary>
        public BinaryTreeNode<T> Root
        {
            get { return m_Root; }
            set { m_Root = value; }
        }
        private BinaryTreeNode<T> m_Root;
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public BinaryTree()
        {
            this.Root = null;
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Clear the Binary Tree.
        /// </summary>
        public void Clear()
        {
            this.Root = null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mode"></param>
        /// <returns></returns>
        public IEnumerable<BinaryTreeNode<T>> Walk(WalkMode mode)
        {
            switch (mode)
            {
                case WalkMode.DepthFirst:
                    foreach (BinaryTreeNode<T> o in this.Root.Walk(mode))
                        yield return (o);
                    break;
                case WalkMode.BreadthFirst:
                    Debug.Assert(false, "Not currently implemented.");
                    break;
                default: 
                    Debug.Assert(false, String.Format("Unsupported WalkMode {0}.", mode));
                    break;
            }
        }
        #endregion // Controller Methods
    }

    /// <summary>
    /// Binary Tree Node data structure.
    /// </summary>
    public class BinaryTreeNode<T> : Node<T>
    {
        #region Constants
        private readonly int sC_s_cnt_NumberOfChildren = 2;
        private readonly int sC_s_idx_LeftChild = 0;
        private readonly int sC_s_idx_RightChild = 1;
        #endregion // Constants

        #region Properties and Associated Member Data
        /// <summary>
        /// Left child of this Binary Tree Node.
        /// </summary>
        public virtual BinaryTreeNode<T> Left
        {
            get
            {
                if (base.Neighbours == null)
                    return null;
                else
                    return (BinaryTreeNode<T>)base.Neighbours[sC_s_idx_LeftChild];
            }
            set
            {
                if (base.Neighbours == null)
                    base.Neighbours = new NodeList<T>(sC_s_cnt_NumberOfChildren);

                base.Neighbours[sC_s_idx_LeftChild] = value;
            }

        }

        /// <summary>
        /// Right child of this Binary Tree Node.
        /// </summary>
        public virtual BinaryTreeNode<T> Right
        {
            get
            {
                if (base.Neighbours == null)
                    return null;
                else
                    return (BinaryTreeNode<T>)base.Neighbours[sC_s_idx_RightChild];
            }
            set
            {
                if (base.Neighbours == null)
                    base.Neighbours = new NodeList<T>(sC_s_cnt_NumberOfChildren);

                base.Neighbours[sC_s_idx_RightChild] = value;
            }

        }
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public BinaryTreeNode()
            : base()
        {
        }

        /// <summary>
        /// Constructor, specifying node data.
        /// </summary>
        /// <param name="data">Node data</param>
        public BinaryTreeNode(T data)
            : this(data, null, null)
        {
        }

        /// <summary>
        /// Constructor, specifying node data, left and right nodes.
        /// </summary>
        /// <param name="data">Node data</param>
        /// <param name="left">Left BinaryTreeNode</param>
        /// <param name="right">Right BinaryTreeNode</param>
        /// <returns></returns>
        public BinaryTreeNode(T data, BinaryTreeNode<T> left, BinaryTreeNode<T> right)
        {
            this.Value = data;
            NodeList<T> children = new NodeList<T>(sC_s_cnt_NumberOfChildren);
            base.Neighbours = children;
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mode"></param>
        /// <returns></returns>
        public IEnumerable<BinaryTreeNode<T>> Walk(BinaryTree<T>.WalkMode mode)
        {
            switch (mode)
            {
                case BinaryTree<T>.WalkMode.DepthFirst:
                    yield return (this);
                    if (null != this.Left)
                        foreach (BinaryTreeNode<T> o in this.Left.Walk(mode))
                            yield return (o);
                    if (null != this.Right)
                        foreach (BinaryTreeNode<T> o in this.Right.Walk(mode))
                            yield return (o);
                    break;
                case BinaryTree<T>.WalkMode.BreadthFirst:
                    Debug.Assert(false, "Not currently implemented.");
                    break;
                default:
                    Debug.Assert(false, String.Format("Unsupported WalkMode {0}.", mode));
                    break;
            }
        }
        #endregion // Controller Methods
    }

} // RSG.Base.Collections namespace

// BinaryTree.cs
