﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Base.Collections
{
    /// <summary>
    /// Generic dictionary
    /// It is required to mark a dictionary with the DataContractAttribute in order to serialise over WCF.
    /// - this was discovered when serialising a dictionary of dictionaries.
    /// - the higher level dictionary serialised without this!
    /// </summary>
    [CollectionDataContract]
    public class DataContractSerialisableDictionary<T1,T2> : Dictionary<T1,T2>
    {
        /// <summary>
        ///  We need a parameterless constructor for serialization
        /// </summary>
        public DataContractSerialisableDictionary()
        {
        }

        /// <summary>
        /// Parameterised constructor
        /// </summary>
        /// <param name="dict"></param>
        public DataContractSerialisableDictionary(IDictionary<T1,T2> dict) : base(dict)
        {
        }
    }
}
