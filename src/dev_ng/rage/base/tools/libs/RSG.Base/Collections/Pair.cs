//
// File: Pair.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of Pair.cs class
//

using System;
using System.Collections.Generic;
using System.Text;

namespace RSG.Base.Collections
{

    /// <summary>
    /// Represents a pair of values (can be of different types)
    /// </summary>
    public class Pair<T1, T2> : IEquatable<Pair<T1, T2>>
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Get/set first value in pair
        /// </summary>
        public T1 First
        {
            get { return m_First; }
            set { m_First = value; }
        }
        private T1 m_First;

        /// <summary>
        /// Get/set second value in pair
        /// </summary>
        public T2 Second
        {
            get { return m_Second; }
            set { m_Second = value; }
        }
        private T2 m_Second;
        #endregion

        #region Constructors
        /// <summary>
        /// Default constructor (no initialisation)
        /// </summary>
        public Pair()
        {
            // No initialisation
        }

        /// <summary>
        /// Pair initialisation constructor
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        public Pair(T1 first, T2 second)
        {
            First = first;
            Second = second;
        }
        #endregion

        /// <summary>
        /// Equality method
        /// </summary>
        /// <param name="rhs"></param>
        /// <returns></returns>
        public override bool Equals(object rhs)
        {
            if (rhs is Pair<T1, T2>)
            {
                Pair<T1, T2> rhsPair = (rhs as Pair<T1, T2>);
                return ((this.First.Equals(rhsPair.First)) && (this.Second.Equals(rhsPair.Second)));
            }

            // Otherwise objects certainly are not equal
            return false;
        }

        /// <summary>
        /// Equality method for IEquatable, avoids boxing.
        /// </summary>
        /// <param name="rhs"></param>
        /// <returns></returns>
        public bool Equals(Pair<T1, T2> rhs)
        {
            return ((this.First.Equals(rhs.First)) && (this.Second.Equals(rhs.Second)));
        }

        public static bool operator ==( Pair<T1, T2> lhs, Pair<T1, T2> rhs )
        {
            return ((lhs.First.Equals( rhs.First )) && (lhs.Second.Equals( rhs.Second )));
        }

        public static bool operator !=( Pair<T1, T2> lhs, Pair<T1, T2> rhs )
        {
            return ((!lhs.First.Equals( rhs.First )) || (!lhs.Second.Equals( rhs.Second )));
        }

        /// <summary>
        /// Convert pair to string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("[{0}, {1}]", First, Second);
        }

        /// <summary>
        /// Return hash code for this object
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            string sPairString = this.ToString();
            return (sPairString.GetHashCode());
        }
    }

} // End of RSG.Base.Collections namespace


// End of file
