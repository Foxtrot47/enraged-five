﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Runtime.InteropServices;

namespace RSG.Base.Collections
{
    /// <summary>
    /// This is a almost exact copy of the generic stack object but makes sure that the
    /// property change events (PropertyChange, PropertyChanging) are fired whenever a item on the
    /// stack is popped off or an item is pushed on.
    /// </summary>
    public class ObservableStack<T> : Stack<T>,
        INotifyPropertyChanged, INotifyPropertyChanging, INotifyCollectionChanged, INotifyCollectionChanging
    {
        #region Change events

        #region INotifyCollection

        event NotifyCollectionChangedEventHandler INotifyCollectionChanged.CollectionChanged
        {
            add { CollectionChanged += value; }
            remove { CollectionChanged -= value; }
        }

        protected virtual event NotifyCollectionChangedEventHandler CollectionChanged;

        event NotifyCollectionChangingEventHandler INotifyCollectionChanging.CollectionChanging
        {
            add { CollectionChanging += value; }
            remove { CollectionChanging -= value; }
        }

        protected virtual event NotifyCollectionChangingEventHandler CollectionChanging;


        #endregion // INotifyCollection

        #region INotifyProperty

        event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged
        {
            add { PropertyChanged += value; }
            remove { PropertyChanged -= value; }
        }

        protected virtual event PropertyChangedEventHandler PropertyChanged;

        event PropertyChangingEventHandler INotifyPropertyChanging.PropertyChanging
        {
            add { PropertyChanging += value; }
            remove { PropertyChanging -= value; }
        }

        protected virtual event PropertyChangingEventHandler PropertyChanging;

        #endregion // INotifyProperty

        #endregion // Change events

        #region Override Functions

        /// <summary>
        /// Pops the top item from the stack making sure that the correct events are called.
        /// </summary>
        /// <returns></returns>
        new public T Pop()
        {
            OnEntryBeingRemoved(this.Peek());

            T poppedItem = base.Pop();

            OnEntryRemoved(poppedItem);

            return poppedItem;
        }

        /// <summary>
        /// Pushes a item on to the top of the stack making sure that the correct events get fired.
        /// </summary>
        /// <param name="item"></param>
        new public void Push(T item)
        {
            OnEntryBeingAdded(item);

            base.Push(item);

            OnEntryAdded(item);
        }

        /// <summary>
        /// Clears the stack making sure that the correct events get fired
        /// </summary>
        new public void Clear()
        {
            OnStackBeingCleared();

            base.Clear();

            OnStackCleared();
        }

        #endregion // Override Function(s)

        #region Private Function(s)

        /// <summary>
        /// Called before a item gets pushed onto the stack
        /// </summary>
        /// <param name="item">The item getting pushed on</param>
        private void OnEntryBeingAdded(T item)
        {
            FirePropertyChangingNotifications();
            FireEntryBeingAddedNotifications(item);
        }

        /// <summary>
        /// Called after a item gets pushed onto the stack
        /// </summary>
        /// <param name="item">The item getting pushed on</param>
        private void OnEntryAdded(T item)
        {
            FirePropertyChangedNotifications();
            FireEntryAddedNotifications(item);
        }

        /// <summary>
        /// Called before a item gets popped from the stack
        /// </summary>
        /// <param name="item">The item getting popped off</param>
        private void OnEntryBeingRemoved(T item)
        {
            FirePropertyChangingNotifications();
            FireEntryBeingRemovedNotifications(item);
        }

        /// <summary>
        /// Called after a item gets popped off from the stack
        /// </summary>
        /// <param name="item">The item popped off</param>
        private void OnEntryRemoved(T item)
        {
            FirePropertyChangedNotifications();
            FireEntryRemovedNotifications(item);
        }

        /// <summary>
        /// Gets called before the stack is cleared
        /// </summary>
        private void OnStackBeingCleared()
        {
            List<Object> currentItems = new List<Object>();
            foreach (Object obj in this)
            {
                currentItems.Add(obj);
            }

            FirePropertyChangingNotifications();
            FireBeingClearedNotifications(currentItems);
        }

        /// <summary>
        /// Get called after the stack has been cleared.
        /// </summary>
        private void OnStackCleared()
        {
            FirePropertyChangedNotifications();
            FireClearedNorifications();
        }

        #endregion // Private Function(s)

        #region Property events

        /// <summary>
        /// Gets called whenever a property changes, this just fires off the actual event with the 
        /// correct property name.
        /// </summary>
        /// <param name="propertyName">The property that the event is being fired for</param>
        protected virtual void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Gets called whenever a property is about to change, this just fires off the actual event with the 
        /// correct property name.
        /// </summary>
        /// <param name="propertyName">The property that the event is being fired for</param>
        protected virtual void OnPropertyChanging(String propertyName)
        {
            if (PropertyChanging != null)
                PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
        }

        /// <summary>
        /// Get called whenever a method is called and the properties have change. This
        /// makes sure that every property in this class gets the correct property changed event fired for it.
        /// </summary>
        private void FirePropertyChangedNotifications()
        {
            OnPropertyChanged("Count");
        }

        /// <summary>
        /// Get called whenever a method is called and the properties are about to change. This
        /// makes sure that every property in this class gets the correct property changing event fired for it.
        /// </summary>
        private void FirePropertyChangingNotifications()
        {
            OnPropertyChanging("Count");
        }

        #endregion // Property events

        #region Collection events

        /// <summary>
        /// Gets called whenever the stack collection changes, this just fires off the actual event with the 
        /// correct property name.
        /// </summary>
        protected virtual void OnCollectionChanged(NotifyCollectionChangedEventArgs args)
        {
            if (CollectionChanged != null)
                CollectionChanged(this, args);
        }

        /// <summary>
        /// Gets called whenever the stack collection is about to change, this just fires off the actual event with the 
        /// correct property name.
        /// </summary>
        protected virtual void OnCollectionChanging(NotifyCollectionChangingEventArgs args)
        {
            if (CollectionChanging != null)
                CollectionChanging(this, args);
        }

        /// <summary>
        /// Gets called whenever a item is about to get added.
        /// </summary>
        /// <param name="item">The item that is being added to the stack</param>
        private void FireEntryBeingAddedNotifications(T item)
        {
            OnCollectionChanging(new NotifyCollectionChangingEventArgs(NotifyCollectionChangingAction.Add, item));
        }

        /// <summary>
        /// Get called whenever a item has been added
        /// </summary>
        /// <param name="item"></param>
        private void FireEntryAddedNotifications(T item)
        {
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item));
        }

        /// <summary>
        /// Get called whenever a item is about to get removed.
        /// </summary>
        /// <param name="item"></param>
        private void FireEntryBeingRemovedNotifications(T item)
        {
            OnCollectionChanging(new NotifyCollectionChangingEventArgs(NotifyCollectionChangingAction.Remove, item));
        }

        /// <summary>
        /// Get called whenever a item has been removed
        /// </summary>
        private void FireEntryRemovedNotifications(T item)
        {
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item));
        }

        /// <summary>
        /// Get called whenever the stack is about to be cleared
        /// </summary>
        private void FireBeingClearedNotifications(List<Object> items)
        {
            OnCollectionChanging(new NotifyCollectionChangingEventArgs(NotifyCollectionChangingAction.Reset, items));
        }

        /// <summary>
        /// Get called whenever the stack has been cleared
        /// </summary>
        /// <param name="items"></param>
        private void FireClearedNorifications()
        {
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        #endregion // Collection events

    }
}
