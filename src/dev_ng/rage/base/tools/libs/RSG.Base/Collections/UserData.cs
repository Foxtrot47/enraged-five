﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RSG.Base.Collections
{
    /// <summary>
    /// Class that represents user data that can be attached
    /// to models or view models and bound to
    /// </summary>
    public class UserData : INotifyPropertyChanged
    {
        #region Events

        /// <summary>
        /// Property changed event fired when the name changes so that the 
        /// binding can be dynamic.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Create the OnPropertyChanged method to raise the event
        /// </summary>
        /// <param name="name"></param>
        protected void OnPropertyChanged(String name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        #endregion

        public ObservableCollection<Object> UserDataSource
        {
            get { return m_userData; }
            set { m_userData = value; }
        }
        private ObservableCollection<Object> m_userData;

        public IEnumerable<Object> this[Type type]
        {
            get
            {
                foreach (Object obj in this.UserDataSource.Where(o => o.GetType() == type))
                {
                    yield return obj;
                }
            }
        }

        public IEnumerable<Object> this[String typename]
        {
            get
            {
                foreach (Object obj in this.UserDataSource)
                {
                    Type type = obj.GetType();
                    if (type.Name == typename)
                    {
                        yield return obj;
                    }
                }
            }
        }

        public UserData()
        {
            this.UserDataSource = new ObservableCollection<Object>();
        }

        public void Add(Object userData)
        {
            if (!this.UserDataSource.Contains(userData))
            {
                this.UserDataSource.Add(userData);
                OnPropertyChanged("UserDataSource");
            }
        }

        public void Remove(Object userData)
        {
            if (this.UserDataSource.Contains(userData))
            {
                this.UserDataSource.Remove(userData);
                OnPropertyChanged("UserDataSource");
            }
        }

        public void RemoveAt(int index)
        {
            if (this.UserDataSource.Count >= index)
            {
                this.UserDataSource.RemoveAt(index);
                OnPropertyChanged("UserDataSource");
            }
        }

        public void Insert(int index, Object userData)
        {
            if (!this.UserDataSource.Contains(userData))
            {
                this.UserDataSource.Insert(index, userData);
                OnPropertyChanged("UserDataSource");
            }
        }

        public void RemoveAllOfType(Type type)
        {
            List<Object> data = new List<object>(this[type]);

            foreach (Object obj in data)
            {
                this.Remove(obj);
                OnPropertyChanged("UserDataSource");
            }
        }

        public void RemoveAllOfTypes(IList<Type> types)
        {
            foreach (Type type in types)
            {
                this.RemoveAllOfType(type);
            }
        }

        /// <summary>
        /// Returns true iff this user data contains the given type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public Boolean ContainsType(Type type)
        {
            foreach (Object obj in this.UserDataSource.Where(o => o.GetType() == type))
            {
                return true;
            }
            return false;
        }

    }
}
