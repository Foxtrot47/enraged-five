﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Collections
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ReadOnlyObservableCollection<T> : ObservableCollection<T>
    {
        /// <summary>
        /// Gets or sets a value indicating whether the owner is writing to the 
        /// collection.
        /// </summary>
        private bool IsMutating { get; set; }

        /// <summary>
        /// A method that mutates the collection.
        /// </summary>
        /// <param name="action">The action to mutate the collection.</param>
        public void Mutate(Action<ReadOnlyObservableCollection<T>> action)
        {
            IsMutating = true;
            try
            {
                action(this);
            }
            finally
            {
                IsMutating = false;
            }
        }

        /// <summary>
        /// Removes an item from the collection at an index.
        /// </summary>
        /// <param name="index">The index to remove.</param>
        protected override void RemoveItem(int index)
        {
            if (!IsMutating)
            {
                throw new NotSupportedException("Collection is read-only.");
            }
            else
            {
                base.RemoveItem(index);
            }
        }

        /// <summary>
        /// Sets an item at a particular location in the collection.
        /// </summary>
        /// <param name="index">The location to set an item.</param>
        /// <param name="item">The item to set.</param>
        protected override void SetItem(int index, T item)
        {
            if (!IsMutating)
            {
                throw new NotSupportedException("Collection is read-only.");
            }
            else
            {
                base.SetItem(index, item);
            }
        }

        /// <summary>
        /// Inserts an item in the collection.
        /// </summary>
        /// <param name="index">The index at which to insert the item.</param>
        /// <param name="item">The item to insert.</param>
        protected override void InsertItem(int index, T item)
        {
            if (!IsMutating)
            {
                throw new NotSupportedException("Collection is read-only.");
            }
            else
            {
                base.InsertItem(index, item);
            }
        }

        /// <summary>
        /// Clears the items from the collection.
        /// </summary>
        protected override void ClearItems()
        {
            if (!IsMutating)
            {
                throw new NotSupportedException("Collection is read-only.");
            }
            else
            {
                base.ClearItems();
            }
        }
    } // ReadOnlyObservableCollection<T>
}
