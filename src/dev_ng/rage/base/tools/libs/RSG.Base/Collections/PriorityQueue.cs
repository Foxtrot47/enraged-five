﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace RSG.Base.Collections
{

    /// <summary>
    /// Priority queue; configuration priority and object types.
    /// </summary>
    public class PriorityQueue<TPriority, TObject> :
        ICollection<KeyValuePair<TPriority, TObject>>
    {
        #region Properties
        /// <summary>
        /// Whether the queue is empty.
        /// </summary>
        public bool IsEmpty
        {
            get { return (0 == this.m_Store.Count); }
        }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Data storage for objects.
        /// </summary>
        private List<KeyValuePair<TPriority, TObject>> m_Store;

        /// <summary>
        /// Priority type comparer.
        /// </summary>
        private IComparer<TPriority> m_Comparer;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public PriorityQueue()
            : this(Comparer<TPriority>.Default)
        {
        }

        /// <summary>
        /// Constructor; specifying priority comparer.
        /// </summary>
        /// <param name="comparer"></param>
        public PriorityQueue(IComparer<TPriority> comparer)
        {
            this.m_Store = new List<KeyValuePair<TPriority, TObject>>();
            this.m_Comparer = comparer;
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Add an object to the end of the queue.
        /// </summary>
        /// <param name="priority"></param>
        /// <param name="obj"></param>
        public void Enqueue(TPriority priority, TObject obj)
        {
            this.Insert(priority, obj);
        }

        /// <summary>
        /// Remove the object from the head of the queue.
        /// </summary>
        /// <returns></returns>
        public TObject DequeueValue()
        {
            return (this.Dequeue().Value);
        }

        /// <summary>
        /// Remove the object from the head of the queue.
        /// </summary>
        /// <returns></returns>
        public KeyValuePair<TPriority, TObject> Dequeue()
        {
            if (!IsEmpty)
            {
                KeyValuePair<TPriority, TObject> result = this.m_Store[0];
                DeleteRoot();
                return (result);
            }
            else
            {
                throw new InvalidOperationException("Priority queue is empty");
            }
        }

        /// <summary>
        /// Return the first element in the priority queue without removing it.
        /// </summary>
        /// <returns></returns>
        public TObject PeekValue()
        {
            return (this.Peek().Value);
        }

        /// <summary>
        /// Return the first element in the priority queue without removing it.
        /// </summary>
        /// <returns></returns>
        public KeyValuePair<TPriority, TObject> Peek()
        {
            if (!this.IsEmpty)
                return (this.m_Store[0]);
            else
                throw new InvalidOperationException("Priority Queue is empty");
        }
        #endregion // Controller Methods

        #region ICollection<TObject> Interface Methods
        /// <summary>
        /// Add an object from the priority queue.
        /// </summary>
        /// <param name="obj"></param>
        public void Add(KeyValuePair<TPriority, TObject> obj)
        {
            this.Enqueue(obj.Key, obj.Value);
        }

        /// <summary>
        /// Remove all elements in the priority queue.
        /// </summary>
        public void Clear()
        {
            this.m_Store.Clear();
        }

        /// <summary>
        /// Query whether an object exists in the priority queue.
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public bool Contains(KeyValuePair<TPriority, TObject> obj)
        {
            return (this.m_Store.Contains(obj));
        }

        /// <summary>
        /// Number of objects within the queue.
        /// </summary>
        public int Count
        {
            get { return (this.m_Store.Count); }
        }

        /// <summary>
        /// Copies the objects of the queue into an Array; starting at a 
        /// particular index.
        /// </summary>
        /// <param name="array"></param>
        /// <param name="arrayIndex"></param>
        public void CopyTo(KeyValuePair<TPriority, TObject>[] array, int arrayIndex)
        {
            this.m_Store.CopyTo(array, arrayIndex);
        }

        /// <summary>
        /// Whether the collection is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return (false); }
        }

        /// <summary>
        /// Remove the object from the priority queue.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool Remove(KeyValuePair<TPriority, TObject> obj)
        {
            
            // find element in the collection and remove it 
            int elementIdx = this.m_Store.IndexOf(obj);
            if (elementIdx < 0)
                return (false);

            this.m_Store.RemoveAt(elementIdx);
            /*
            //remove element  
            this.m_Store[elementIdx] = this.m_Store[this.m_Store.Count - 1];
            this.m_Store.RemoveAt(this.m_Store.Count - 1);

            // heapify  
            int newPos = HeapifyFromEndToBeginning(elementIdx);
            if (newPos == elementIdx)
                HeapifyFromBeginningToEnd(elementIdx);
             
            */
            return true;
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>Enumerator</returns>
        /// <remarks>
        /// Returned enumerator does not iterate elements in sorted order.</remarks>
        public IEnumerator<KeyValuePair<TPriority, TObject>> GetEnumerator()
        {
            return (this.m_Store.GetEnumerator());
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>Enumerator</returns>
        /// <remarks>
        /// Returned enumerator does not iterate elements in sorted order.</remarks>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return (this.GetEnumerator());
        }
        #endregion // ICollection<TObject> Interface Methods

        #region Private Methods
        /// <summary>
        /// Insert object with priority into heap.
        /// </summary>
        /// <param name="priority"></param>
        /// <param name="obj"></param>
        private void Insert(TPriority priority, TObject obj)
        {
            KeyValuePair<TPriority, TObject> val = new
                KeyValuePair<TPriority, TObject>(priority, obj);
            this.m_Store.Add(val);

            //HeapifyFromEndToBeginning(this.m_Store.Count - 1);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pos"></param>
        /// <returns></returns>
        private int HeapifyFromEndToBeginning(int pos)
        {
            if (pos >= this.m_Store.Count)
                return (-1);
            
            // heap[i] have children heap[2*i + 1] and heap[2*i + 2] and parent 
            // heap[(i-1)/ 2];  
            while (pos > 0)
            {
                int parentPos = (pos - 1) / 2;
                if (this.m_Comparer.Compare(this.m_Store[parentPos].Key, this.m_Store[pos].Key) > 0)
                {
                    SwapElements(parentPos, pos);
                    pos = parentPos;
                }
                else 
                    break;
            }
            return pos; 
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pos"></param>
        /// <returns></returns>
        private void HeapifyFromBeginningToEnd(int pos)
        {
            if (pos >= this.m_Store.Count) 
                return;

            // heap[i] have children heap[2*i + 1] and heap[2*i + 2] and parent heap[(i-1)/ 2];  

            while (true)
            {
                // on each iteration exchange element with its smallest child  
                int smallest = pos;
                int left = 2 * pos + 1;
                int right = 2 * pos + 2;
                if (left < this.m_Store.Count &&
                    this.m_Comparer.Compare(this.m_Store[smallest].Key, this.m_Store[left].Key) > 0)
                    smallest = left;
                if (right < this.m_Store.Count &&
                    this.m_Comparer.Compare(this.m_Store[smallest].Key, this.m_Store[right].Key) > 0)
                    smallest = right;

                if (smallest != pos)
                {
                    SwapElements(smallest, pos);
                    pos = smallest;
                }
                else break;
            }  
        }

        /// <summary>
        /// 
        /// </summary>
        private void DeleteRoot()
        {
            if (this.m_Store.Count <= 1)
            {
                this.m_Store.Clear();
                return;
            }

            this.m_Store.RemoveAt(0);
            /*
            this.m_Store[0] = this.m_Store[this.m_Store.Count - 1];
            this.m_Store.RemoveAt(this.m_Store.Count - 1);

            // heapify  
            HeapifyFromBeginningToEnd(0);
             * */
        }  

        /// <summary>
        /// Swap two elements.
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        private void SwapElements(int p1, int p2)
        {
            KeyValuePair<TPriority, TObject> val = this.m_Store[p1];
            this.m_Store[p1] = this.m_Store[p2];
            this.m_Store[p2] = val;
        }
        #endregion // Private Methods
    }

} // RSG.Base.Collections namespace
