//
// File: Node.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of Node class
//

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace RSG.Base.Collections
{

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class NodeList<T> : Collection<Node<T>>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public NodeList()
            : base()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="size"></param>
        public NodeList(int size)
        {
            for (int i = 0; i < size; ++i)
                base.Items.Add(default(Node<T>));
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public Node<T> FindByValue(T value)
        {
            foreach (Node<T> node in Items)
                if (node.Value.Equals(value))
                    return (node);
            return (null);
        }
        #endregion // Controller Methods
    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Node<T>
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// 
        /// </summary>
        public T Value
        {
            get { return m_Value; }
            set { m_Value = value; }
        }
        private T m_Value;

        /// <summary>
        /// 
        /// </summary>
        public NodeList<T> Neighbours
        {
            get { return m_Neighbours; }
            set { m_Neighbours = value; }
        }
        private NodeList<T> m_Neighbours;

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<Node<T>> InOrder
        {
            get { return ScanInOrder(this); }
        }
        #endregion // Properties and Associated Member Data

        #region Protected Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="root"></param>
        /// <returns></returns>
        protected IEnumerable<Node<T>> ScanInOrder(Node<T> root)
        {
            yield return (root);
            foreach (Node<T> n in ScanInOrder(root))
                yield return (n);
        }
        #endregion // Protected Methods

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public Node()
        {
        }

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="value"></param>
        public Node(T value)
            : this(value, null)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="value"></param>
        /// <param name="neighbours"></param>
        public Node(T value, NodeList<T> neighbours)
        {
            this.Value = value;
            this.Neighbours = neighbours;
        }
        #endregion // Constructor(s)
    }

} // RSG.Base.Collections namespace

// Node.cs
