﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using SMath = System.Math;
using RSG.Base.Logging;
using RSG.Base.Math;

namespace RSG.Base.Collections
{

    /// <summary>
    /// QuadTree structure.
    /// </summary>
    /// References:
    ///   http://en.wikipedia.org/wiki/Quadtree
    ///   
    /// This QuadTree contains items that have an area (BoundingBox2f)
    /// it will store a reference to the item in the quad 
    /// that is just big enough to hold it. Each quad has a bucket that 
    /// contain multiple items.
    /// 
    /// The QuadTree has an optional split delegate to fully define when 
    /// subnodes are created in a callback function.  Otherwise you can 
    /// initialise with a minimum area.
    /// 
    public class QuadTree<T> 
        where T : IHasBoundingBox2f
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Get the count of items in the QuadTree
        /// </summary>
        public int Count 
        { 
            get { return m_root.Count; } 
        }

        /// <summary>
        /// 
        /// </summary>
        public int Depth
        {
            get { return m_root.Depth; }
        }

        /// <summary>
        /// 
        /// </summary>
        public float MinimumArea
        {
            get;
            private set;
        }

        /// <summary>
        /// Bounds of this QuadTree.
        /// </summary>
        public BoundingBox2f Bounds
        {
            get;
            private set;
        }
        #endregion // Properties and Associated Member Data

        #region Enumerations 
        /// <summary>
        /// Enumeration to show QuadTree subnode behaviour.
        /// </summary>
        public enum Operation
        {
            None,
            Split,
        }

        public enum SplitBehaviour
        {
            Split,
            DoNotSplit
        }
        #endregion // Enumerations

        #region Delegates
        /// <summary>
        /// Delegate that performs an action on a QuadTreeNode.
        /// </summary>
        /// <param name="obj"></param>
        public delegate void Action(QuadTreeNode<T> obj);

        /// <summary>
        /// Delegate that determines whether the node should split.
        /// </summary>
        /// <returns></returns>
        public delegate Operation SplitNode(QuadTreeNode<T> op);

        public delegate int ValuationNode(QuadTreeNode<T> obj);
        #endregion // Delegates

        #region Member Data
        /// <summary>
        /// Root QuadTreeNode object.
        /// </summary>
        QuadTreeNode<T> m_root;

        /// <summary>
        /// QuadTreeNode split callback.
        /// </summary>
        SplitNode m_splitCallback;
        #endregion // Member Data
        
        #region Constructor(s)
        /// <summary>
        /// Constructor, specifying minimum area.
        /// </summary>
        /// <param name="rect"></param>
        /// <param name="minimumArea"></param>
        public QuadTree(BoundingBox2f rect, float minimumArea)
        {
            this.Bounds = rect;
            this.MinimumArea = minimumArea;
            this.m_root = new QuadTreeNode<T>(this.Bounds, minimumArea);
            this.m_splitCallback = null;
        }

        /// <summary>
        /// Constructor, specifying node split callback.
        /// </summary>
        /// <param name="rect"></param>
        /// <param name="splitCallback"></param>
        public QuadTree(BoundingBox2f rect, SplitNode splitCallback)
        {
            Debug.Assert(null != splitCallback, "Must specify a valid splitCallback.");
            if (null == splitCallback)
                throw new ArgumentNullException("splitCallback");

            this.Bounds = rect;
            this.MinimumArea = -1.0f;
            this.m_root = new QuadTreeNode<T>(this.Bounds, splitCallback);
            this.m_splitCallback = splitCallback;
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Insert the feature into the QuadTree
        /// </summary>
        /// <param name="item"></param>
        public void Insert(T item, SplitBehaviour splitBehaviour)
        {
            m_root.Insert(item, splitBehaviour);
        }

        /// <summary>
        /// Do the specified action for each item in the quadtree
        /// </summary>
        /// <param name="action"></param>
        public void ForEach(Action action)
        {
            m_root.ForEach(action);
        }

        public void MergeLowValueNodes(ValuationNode valuationCallback, int minimumSize, int maximumSize)
        {
            m_root.MergeLowValueSubNodes(valuationCallback, minimumSize, maximumSize);
        }
        #endregion // Controller Methods
    }

    /// <summary>
    /// The QuadTreeNode
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class QuadTreeNode<T> 
        where T : IHasBoundingBox2f
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Is the node empty
        /// </summary>
        public bool IsEmpty 
        { 
            get { return Bounds.IsEmpty || m_nodes.Count == 0; } 
        }

        /// <summary>
        /// Area of the quadtree node
        /// </summary>
        public BoundingBox2f Bounds
        {
            get;
            private set;
        }

        /// <summary>
        /// Minimum area to stop recursion; set in constructor by parameter.
        /// </summary>
        public float MinimumArea
        {
            get;
            private set;
        }

        /// <summary>
        /// Total number of nodes in the this node and all SubNodes
        /// </summary>
        public int Count
        {
            get
            {
                int count = 0;

                foreach (QuadTreeNode<T> node in m_nodes)
                    count += node.Count;

                count += this.Contents.Count;

                return count;
            }
        }

        /// <summary>
        /// Maximum depth of the QuadTree node.
        /// </summary>
        public int Depth
        {
            get
            {
                int count = 0;
                if (m_nodes.Count > 0)
                {
                    count += 1;
                    Debug.Assert(4 == m_nodes.Count);
                    int q12max = SMath.Max(m_nodes[0].Depth, m_nodes[1].Depth);
                    int q34max = SMath.Max(m_nodes[2].Depth, m_nodes[3].Depth);
                    return (count + SMath.Max(q12max, q34max));
                }

                return (count);
            }
        }

        /// <summary>
        /// Return the contents of this node and all subnodes in the true below this one.
        /// </summary>
        public List<T> SubTreeContents
        {
            get
            {
                List<T> results = new List<T>();

                foreach (QuadTreeNode<T> node in m_nodes)
                    results.AddRange(node.SubTreeContents);

                results.AddRange(this.Contents);
                return results;
            }
        }

        /// <summary>
        /// Node content.
        /// </summary>
        public List<T> Contents 
        { 
            get { return m_contents; } 
        }
        #endregion // Properties and Associated Member Data
        
        #region Member Data
        /// <summary>
        /// QuadTreeNode split callback.
        /// </summary>
        QuadTree<T>.SplitNode m_splitCallback;

        /// <summary>
        /// The contents of this node.
        /// Note that the contents have no limit: this is not the standard way to impement a QuadTree
        /// </summary>
        List<T> m_contents = new List<T>();

        /// <summary>
        /// The child nodes of the QuadTree
        /// </summary>
        List<QuadTreeNode<T>> m_nodes = new List<QuadTreeNode<T>>(4);
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor, specifying minimum area.
        /// </summary>
        /// <param name="rect"></param>
        /// <param name="minimumArea"></param>
        public QuadTreeNode(BoundingBox2f rect, float minimumArea)
        {
            this.Bounds = rect;
            this.MinimumArea = minimumArea;
            this.m_splitCallback = null;
        }

        /// <summary>
        /// Constructor, specifying node split callback.
        /// </summary>
        /// <param name="rect"></param>
        /// <param name="splitCallback"></param>
        public QuadTreeNode(BoundingBox2f rect, QuadTree<T>.SplitNode splitCallback)
        {
            Debug.Assert(null != splitCallback, "Must specify a valid splitCallback.");
            if (null == splitCallback)
                throw new ArgumentNullException("splitCallback");
            this.Bounds = rect;
            this.MinimumArea = -1.0f;
            this.m_splitCallback = splitCallback;
        }
        #endregion // Constructor(s)

        /// <summary>
        /// Insert an item to this node
        /// </summary>
        /// <param name="item"></param>
        public void Insert(T item, QuadTree<T>.SplitBehaviour splitBehaviour)
        {
            if (splitBehaviour == QuadTree<T>.SplitBehaviour.Split)
            {
                // if the subnodes are null and they are required then create them
                if (m_nodes.Count == 0)
                {
                    if (CreateSubNodes())
                    {
                        // If we've just split then push our children down
                        MoveContentsIntoChildren();
                    }
                }
            }

            if (m_nodes.Count > 0)
            {
                InsertIntoClosestSubNode(item, splitBehaviour);
            }
            else
            {
                this.Contents.Add(item);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="action"></param>
        public void ForEach(QuadTree<T>.Action action)
        {
            action(this);

            // Iterate through subnodes.
            foreach (QuadTreeNode<T> node in this.m_nodes)
                node.ForEach(action);
        }

        /// <summary>
        /// Merges any nodes of low value (as defined by valuationCallback) into adjacent sibling nodes
        /// </summary>
        /// <param name="valuationCallback"></param>
        /// <param name="minimumSize"></param>
        public void MergeLowValueSubNodes(QuadTree<T>.ValuationNode valuationCallback, int minimumSize, int maximumSize)
        {
            if (m_nodes.Count != 4)
                return;

            // The layout is clockwise from nw (nw, sw, se, ne)
            QuadTreeNode<T> nw = m_nodes[0];
            QuadTreeNode<T> sw = m_nodes[1];
            QuadTreeNode<T> se = m_nodes[2];
            QuadTreeNode<T> ne = m_nodes[3];

            TryMerge(nw, ne, sw, valuationCallback, minimumSize, maximumSize);
            TryMerge(ne, nw, se, valuationCallback, minimumSize, maximumSize);
            TryMerge(se, sw, ne, valuationCallback, minimumSize, maximumSize);
            TryMerge(sw, se, nw, valuationCallback, minimumSize, maximumSize);

            foreach (QuadTreeNode<T> node in this.m_nodes)
                node.MergeLowValueSubNodes(valuationCallback, minimumSize, maximumSize);
        }

        /// <summary>
        /// Attempts to move the contents of a source node into that of two potential target nodes
        /// </summary>
        /// <param name="source"></param>
        /// <param name="target0"></param>
        /// <param name="target1"></param>
        /// <param name="valuationCallback"></param>
        /// <param name="minimumSize"></param>
        private static void TryMerge(QuadTreeNode<T> source, QuadTreeNode<T> target0, QuadTreeNode<T> target1, QuadTree<T>.ValuationNode valuationCallback, int minimumSize, int maximumSize)
        {
            // Is the source node of a sufficiently 'low value'?
            int sourceValuation = valuationCallback(source);
            if (sourceValuation == 0 || sourceValuation >= minimumSize)
                return;

            int target0Valuation = valuationCallback(target0);
            int target1Valuation = valuationCallback(target1);

            // Are either of the target nodes suitable?
            if (target0Valuation == 0 && target1Valuation == 0)
                return;

            // Find the best target (smallest non-empty target)
            QuadTreeNode<T> bestTarget = null;
            int bestTargetValuation = 0;
            if (target0Valuation == 0)
            {
                bestTarget = target1;
                bestTargetValuation = target1Valuation;
            }
            else if (target1Valuation == 0 || target0Valuation < target1Valuation)
            {
                bestTarget = target0;
                bestTargetValuation = target0Valuation;
            }
            else
            {
                bestTarget = target1;
                bestTargetValuation = target1Valuation;
            }

            // Make sure the merge doesn't push us over the upper limit
            if ((sourceValuation + bestTargetValuation) > maximumSize)
                return;

            foreach (T item in source.Contents)
                bestTarget.Insert(item, QuadTree<T>.SplitBehaviour.DoNotSplit);

            source.Contents.Clear();
        }

        /// <summary>
        /// Internal method to create the subnodes (partitions space)
        /// </summary>
        /// <returns>Returns true if the sub nodes were created</returns>
        private bool CreateSubNodes()
        {
            // Depending on how we are splitting, either through the callback
            // or with a minimum area, we return before the split if the split 
            // should not happen.
            if (null != this.m_splitCallback)
            {
                QuadTree<T>.Operation op = this.m_splitCallback(this);
                switch (op)
                {
                    case QuadTree<T>.Operation.None:
                        // Stop recursive split.
                        return false;
                    case QuadTree<T>.Operation.Split:
                    default:
                        // Continue to split into subnodes.
                        break;
                }
            }
            else
            {
                // the smallest subnode has an area 
                if ((this.Bounds.Height * this.Bounds.Width) <= this.MinimumArea)
                    return false;
            }

            float minx = this.Bounds.Min.X;
            float miny = this.Bounds.Min.Y;
            float maxx = this.Bounds.Max.X;
            float maxy = this.Bounds.Max.Y;
            float cx = minx + (this.Bounds.Width / 2.0f);
            float cy = miny + (this.Bounds.Height / 2.0f);

            if (null != this.m_splitCallback)
            {
                m_nodes.Add(new QuadTreeNode<T>(new BoundingBox2f(new Vector2f(minx, miny), new Vector2f(cx, cy)), this.m_splitCallback)); // Q4
                m_nodes.Add(new QuadTreeNode<T>(new BoundingBox2f(new Vector2f(minx, cy), new Vector2f(cx, maxy)), this.m_splitCallback)); // Q1
                m_nodes.Add(new QuadTreeNode<T>(new BoundingBox2f(new Vector2f(cx, cy), new Vector2f(maxx, maxy)), this.m_splitCallback)); // Q2
                m_nodes.Add(new QuadTreeNode<T>(new BoundingBox2f(new Vector2f(cx, miny), new Vector2f(maxx, cy)), this.m_splitCallback)); // Q3
            }
            else
            {
                m_nodes.Add(new QuadTreeNode<T>(new BoundingBox2f(new Vector2f(minx, miny), new Vector2f(cx, cy)), this.MinimumArea)); // Q4
                m_nodes.Add(new QuadTreeNode<T>(new BoundingBox2f(new Vector2f(minx, cy), new Vector2f(cx, maxy)), this.MinimumArea)); // Q1
                m_nodes.Add(new QuadTreeNode<T>(new BoundingBox2f(new Vector2f(cx, cy), new Vector2f(maxx, maxy)), this.MinimumArea)); // Q2
                m_nodes.Add(new QuadTreeNode<T>(new BoundingBox2f(new Vector2f(cx, miny), new Vector2f(maxx, cy)), this.MinimumArea)); // Q3
            }

            return true;
        }

        /// <summary>
        /// Distributes the contents of a node into its child nodes
        /// </summary>
        private void MoveContentsIntoChildren()
        {
            for (int contentIndex = 0; contentIndex < m_contents.Count; ++contentIndex)
            {
                InsertIntoClosestSubNode(m_contents[contentIndex], QuadTree<T>.SplitBehaviour.Split);
            }

            m_contents.Clear();
        }

        /// <summary>
        /// Insert an item into the closest child node
        /// </summary>
        /// <param name="item"></param>
        private void InsertIntoClosestSubNode(T item, QuadTree<T>.SplitBehaviour splitBehaviour)
        {
            Vector2f itemBoundCentre = item.Bound.Centre();

            QuadTreeNode<T> closestNode = null;
            float distanceToClosestNode = Single.MaxValue;
            foreach (QuadTreeNode<T> node in m_nodes)
            {
                float distanceToNode = (float)(node.Bounds.Centre() - itemBoundCentre).Magnitude();
                if (distanceToNode < distanceToClosestNode)
                {
                    closestNode = node;
                    distanceToClosestNode = distanceToNode;
                }
            }

            Debug.Assert(closestNode != null);
            closestNode.Insert(item, splitBehaviour);
        }
    }

    /// <summary>
    /// An interface that defines and object with a rectangle
    /// </summary>
    public interface IHasBoundingBox2f
    {
        BoundingBox2f Bound { get; }
    }

} // RSG.Base.Collections namespace
