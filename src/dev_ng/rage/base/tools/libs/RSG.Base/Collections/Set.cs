﻿using System;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;

namespace RSG.Base.Collections
{

    /// <summary>
    /// Set collection.
    /// </summary>
    public class Set<T> : ICollection<T>
    {
        #region Member Data
        /// <summary>
        /// Internal Dictionary, T => null to act as a set of T.
        /// </summary>
        protected Dictionary<T, Object> m_Dict;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public Set()
        {
            this.m_Dict = new Dictionary<T, Object>();
        }
        #endregion // Constructor(s)

        #region ICollection Members
        #region ICollection Properties
        public bool IsReadOnly
        {
            get { return (false); }
        }

        /// <summary>
        /// Return number of items in the set.
        /// </summary>
        public int Count
        {
            get { return (this.m_Dict.Count); }
        }
        #endregion // ICollection Properties

        #region ICollection Methods
        /// <summary>
        /// Add an item to the set.
        /// </summary>
        /// <param name="item"></param>
        public void Add(T item)
        {
            this.m_Dict.Add(item, null);
        }

        /// <summary>
        /// Removes all items from the set.
        /// </summary>
        public void Clear()
        {
            this.m_Dict.Clear();
        }

        /// <summary>
        /// Determine whether the set contains a specific value.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Contains(T item)
        {
            return (this.m_Dict.ContainsKey(item));
        }

        /// <summary>
        /// Copy items in the set to an array, starting at a particular index.
        /// </summary>
        /// <param name="output"></param>
        /// <param name="index"></param>
        public void CopyTo(T[] output, int index)
        {
            this.m_Dict.Values.CopyTo(output as Object[], index);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerator<T> GetEnumerator()
        {
            foreach (T key in this.m_Dict.Values)
                yield return key;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return (this.m_Dict.Values.GetEnumerator());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        public bool Remove(T item)
        {
            return (this.m_Dict.Remove(item));
        }
        #endregion // ICollection Methods
        #endregion // ICollection Members
    }

} // RSG.Base.Collections namespace
