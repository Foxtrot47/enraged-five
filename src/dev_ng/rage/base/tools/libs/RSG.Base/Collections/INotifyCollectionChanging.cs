﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Collections
{
    /// <summary>
    /// Describes the action that caused a INotifyCollectionChanging.CollectionChanging event to get fired.
    /// </summary>
    public enum NotifyCollectionChangingAction
    {
        // Summary:
        //     One or more items are about to get added to the collection.
        Add = 0,
        /// <summary>
        /// One or more items are about to get removed from the collection.
        /// </summary>
        Remove = 1,
        /// <summary>
        /// The collection has had a item replaced
        /// </summary>
        Replace = 2,
        /// <summary>
        /// The collection is cleared
        /// </summary>
        Reset = 3,
    } // NotifyCollectionChangingAction
    
    /// <summary>
    /// Provides data for the INotifyCollectionChanging.CollectionChanging event
    /// </summary>
    public class NotifyCollectionChangingEventArgs : EventArgs
    {
        #region Constructor(s)

        /// <summary>
        /// Describes a one-item change.
        /// </summary>
        /// <param name="action">The action that caused the event.</param>
        /// <param name="changedItem">The item that is affected by the change.</param>
        public NotifyCollectionChangingEventArgs(NotifyCollectionChangingAction action, Object changedItem)
        {
            this.m_action = action;
            this.m_changedItems = new List<Object>();
            this.m_changedItems.Add(changedItem);
        }

        /// <summary>
        /// Describes a one-item change with an additional index parameter.
        /// </summary>
        /// <param name="action">The action that caused the event.</param>
        /// <param name="changedItem">The item that is affected by the change.</param>
        /// <param name="index">The index where the change occurred.</param>
        public NotifyCollectionChangingEventArgs(NotifyCollectionChangingAction action, Object changedItem, int index)
        {
            this.m_action = action;
            this.m_changedItems = new List<Object>();
            this.m_changedItems.Add(changedItem);
            this.m_startingIndex = index;
        }

        /// <summary>
        /// Describes a multi-item change.
        /// </summary>
        /// <param name="action">The action that caused the event.</param>
        /// <param name="changedItem">The items that is affected by the change.</param>
        public NotifyCollectionChangingEventArgs(NotifyCollectionChangingAction action, IList<Object> changedItems)
        {
            this.m_action = action;
            this.m_changedItems = new List<Object>(changedItems);
        }

        /// <summary>
        /// Describes a multi-item change with an additional index parameter.
        /// </summary>
        /// <param name="action">The action that caused the event.</param>
        /// <param name="changedItem">The items that is affected by the change.</param>
        /// <param name="index">The index where the change occurred.</param>
        public NotifyCollectionChangingEventArgs(NotifyCollectionChangingAction action, IList<Object> changedItems, int index)
        {
            this.m_action = action;
            this.m_changedItems = new List<Object>(changedItems);
            this.m_startingIndex = index;
        }

        #endregion // Constructor(s)

        #region Properties

        /// <summary>
        /// The action that caused the changing event to occur
        /// </summary>
        public NotifyCollectionChangingAction Action
        { 
            get { return m_action; }
        }
        private NotifyCollectionChangingAction m_action;

        /// <summary>
        /// The item that has been either added to the collection or removed from the collection
        /// </summary>
        public List<Object> ChangedItems
        {
            get { return m_changedItems; }
        }
        private List<Object> m_changedItems;

        /// <summary>
        /// The starting index for the change.
        /// </summary>
        public int StartingIndex
        {
            get { return m_startingIndex; }
        }
        private int m_startingIndex;

        #endregion // Properties

    } // NotifyCollectionChangingEventArgs

    /// <summary>
    /// Represents the method that handles the INotifyCollectionChanging.CollectionChanging event.
    /// </summary>
    /// <param name="sender">The object that raised the event.</param>
    /// <param name="e">Information about the event.</param>
    public delegate void NotifyCollectionChangingEventHandler(Object sender, NotifyCollectionChangingEventArgs e); // NotifyCollectionChangedEventHandler

    /// <summary>
    /// Interface that defines a collection changing event
    /// </summary>
    interface INotifyCollectionChanging
    {
        #region Events

        /// <summary>
        /// Occurs when the collection is about to change.
        /// </summary>
        event NotifyCollectionChangingEventHandler CollectionChanging;

        #endregion // Events

    } // INotifyCollectionChanging
} // RSG.Base.Collections
