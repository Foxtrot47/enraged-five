﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Base.Collections
{
    /// <summary>
    /// Mutable version of Range<T>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [DataContract]
    [Serializable]
    public class Range<T> : IComparable<Range<T>> where T : IComparable<T>
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="Start"/> property.
        /// </summary>
        private T _start;

        /// <summary>
        /// Private field for the <see cref="End"/> property.
        /// </summary>
        private T _end;

        /// <summary>
        /// Private field for the <see cref="StartInclusive"/> property.
        /// </summary>
        private bool _startInclusive;

        /// <summary>
        /// Private field for the <see cref="EndInclusive"/> property.
        /// </summary>
        private bool _endInclusive;
        #endregion // Fields

        #region Constructor(s)
        /// <summary>
        /// Default constructor (for serialisation purposes).
        /// </summary>
        public Range()
        {
        }

        /// <summary>
        /// Constructs a new range with optional inclusive flags.
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="startInclusive"></param>
        /// <param name="endInclusive"></param>
        public Range(T start, T end, bool startInclusive = true, bool endInclusive = true)
        {
            _start = start;
            _end = end;
            _startInclusive = startInclusive;
            _endInclusive = endInclusive;
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// Start of the range.
        /// </summary>
        [DataMember]
        public T Start
        {
            get { return _start; }
            set { _start = value; }
        }

        /// <summary>
        /// End of the range.
        /// </summary>
        [DataMember]
        public T End
        {
            get { return _end; }
            set { _end = value; }
        }

        /// <summary>
        /// Whether or not the start value is inclusive.
        /// </summary>
        [DataMember]
        public bool StartInclusive
        {
            get { return _startInclusive; }
            set { _startInclusive = value; }
        }

        /// <summary>
        /// Whether or not the end value is inclusive.
        /// </summary>
        [DataMember]
        public bool EndInclusive
        {
            get { return _endInclusive; }
            set { _endInclusive = value; }
        }
        #endregion // Properties

        #region Public Methods
        /// <summary>
        /// Indicates whether a value is included in this range.
        /// </summary>
        public bool Contains(T value)
        {
            // Check if the value falls completely outside of the range.
            if (value.CompareTo(_start) < 0 || value.CompareTo(_end) > 0)
            {
                return false;
            }
            // Check if the value falls on the boundary and we aren't inclusive.
            if ((value.CompareTo(_start) == 0 && !_startInclusive) || (value.CompareTo(_end) == 0 && !_endInclusive))
            {
                return false;
            }
            // In all other cases the value should be inside this range.
            return true;
        }
        #endregion // Public Methods

        #region Object Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return String.Format("{0}{1}, {2}{3}", (_startInclusive ? '[' : '('), _start, _end, (_endInclusive ? ']' : ')'));
        }
        #endregion // Object Overrides

        #region IComparable<T> Implementation
        /// <summary>
        /// Compare this weapon to another.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(Range<T> other)
        {
            if (other == null)
            {
                return 1;
            }

            int startComparison = Start.CompareTo(other.Start);
            if (startComparison != 0)
            {
                return startComparison;
            }
            else
            {
                return End.CompareTo(other.End);
            }
        }
        #endregion // IComparable<T> Implementation
    } // Range<T>
}
