﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Tasks
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IProgress<T>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        void Report(T value);
    } // IProgress<T>
}
