﻿using System;
using System.Threading;

namespace RSG.Base.Tasks
{
    /// <summary>
    /// Asynchronous task context.
    /// </summary>
    public interface ITaskContext
    {
        #region Properties
        /// <summary>
        /// Cancellation token for when we are running the task asynchronously.
        /// </summary>
        CancellationToken Token
        {
            get;
        }

        /// <summary>
        /// Task argument; allowing the user to pass in arguments to the task.
        /// </summary>
        Object Argument
        {
            get;
            set;
        }

        /// <summary>
        /// Task return value; allowing the user to return object data from the
        /// task.
        /// </summary>
        Object ReturnValue
        {
            get;
            set;
        }
        #endregion
    } // ITaskContext

} // RSG.Base.Tasks namespace
