﻿using System;
using System.Threading;

namespace RSG.Base.Tasks
{
    /// <summary>
    /// Default task context that simply tasks a TPL cancellation token.
    /// </summary>
    public class TaskContext : ITaskContext
    {
        #region Properties
        /// <summary>
        /// Cancellation token for when we are running the task asynchronously.
        /// </summary>
        public CancellationToken Token
        {
            get;
            set;
        }

        /// <summary>
        /// Task argument; allowing the user to pass in arguments to the task.
        /// </summary>
        public Object Argument
        {
            get;
            set;
        }

        /// <summary>
        /// Task return value; allowing the user to return object data from the
        /// task.
        /// </summary>
        public Object ReturnValue
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public TaskContext()
            : this(CancellationToken.None)
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="token"></param>
        public TaskContext(CancellationToken token)
        {
            Token = token;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="token"></param>
        /// <param name="argument"></param>
        public TaskContext(CancellationToken token, Object argument)
            : this(token)
        {
            this.Argument = argument;
        }
        #endregion // Constructor(s)
    } // TaskContextBase

} // RSG.Base.Tasks namespace
