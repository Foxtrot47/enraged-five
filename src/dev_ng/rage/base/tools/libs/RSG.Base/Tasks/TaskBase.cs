﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Base.Logging;

namespace RSG.Base.Tasks
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class TaskBase : ITask
    {
        #region Properties
        /// <summary>
        /// Name of the task.
        /// </summary>
        public String Name { get; private set; }

        /// <summary>
        /// Status that the task is in.
        /// </summary>
        public TaskStatus Status { get; private set; }

        /// <summary>
        /// Flag indicating whether the task reports progress.
        /// </summary>
        public bool ReportsProgress { get; set; }

        /// <summary>
        /// Task progress reporting mechanism.
        /// </summary>
        public IProgress<TaskProgress> Progress { get; private set; }

        /// <summary>
        /// If the task is in the faulted state, this property will contain the exception that was thrown.
        /// </summary>
        public Exception Exception { get; private set; }
        #endregion // Properties

        #region Events
        /// <summary>
        /// Event that is fired when a task has been completed.
        /// </summary>
        public event TaskCompletedEventHandler OnTaskCompleted;
        #endregion // Events

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        public TaskBase(string name)
        {
            Name = name;
            ReportsProgress = true;
            Progress = new EventProgress<TaskProgress>(this);
        }
        #endregion // Constructor(s)

        #region ITask Implementation
        /// <summary>
        /// Execute the task synchronously with the provided context.
        /// </summary>
        /// <param name="context"></param>
        public void Execute(ITaskContext context)
        {
            Status = TaskStatus.Running;
            try
            {
                ExecuteInternal(context);
                Status = TaskStatus.Completed;
                ReportTotalProgress(1.0, null);
            }
            catch (System.Exception e)
            {
                Status = TaskStatus.Faulted;
                Exception = e;
                Log.Log__Exception(Exception, "Exception in TaskBase!");
            }

            if (OnTaskCompleted != null)
            {
                OnTaskCompleted(this, new TaskCompletedEventArgs(Status, context.ReturnValue));
            }
        }

        /// <summary>
        /// Execute the task asynchronously with the provided context by wrapping it in a Task.
        /// </summary>
        /// <param name="context"></param>
        public Task ExecuteAsync(ITaskContext context)
        {
            Task task = new Task(obj => ExecuteInternal((ITaskContext)obj), context, context.Token, TaskCreationOptions.AttachedToParent);
            task.ContinueWith(obj => TaskCompleted(obj, context));
            task.Start();

            Status = TaskStatus.Running;
            ReportTotalProgress(0.0, null);

            return task;
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual void Reset()
        {
            Status = TaskStatus.Pending;
        }
        #endregion // ITask Implementation

        #region Abstract Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public abstract void ExecuteInternal(ITaskContext context);
        #endregion // Abstract Methods

        #region Protected Methods
        /// <summary>
        /// 
        /// </summary>
        protected void ReportTotalProgress(double progress, string message)
        {
            if (Progress != null)
            {
                Progress.Report(new TaskProgress(progress, message));
            }
        }
        #endregion // Protected Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="task"></param>
        private void TaskCompleted(Task task, ITaskContext context)
        {
            if (task.IsCompleted)
            {
                Status = TaskStatus.Completed;
                ReportTotalProgress(1.0, null);
            }
            if (task.IsCanceled)
            {
                Status = TaskStatus.Cancelled;
            }
            else if (task.IsFaulted)
            {
                Status = TaskStatus.Faulted;
                Exception = task.Exception;
            }

            if (OnTaskCompleted != null)
            {
                OnTaskCompleted(this, new TaskCompletedEventArgs(Status, context.ReturnValue));
            }
        }
        #endregion // Private Methods
    } // TaskBase
}
