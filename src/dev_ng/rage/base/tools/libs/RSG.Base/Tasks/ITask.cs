﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Base.Tasks
{
    /// <summary>
    /// Interface for a piece of work to execute.
    /// </summary>
    public interface ITask
    {
        #region Properties
        /// <summary>
        /// Name of the task.
        /// </summary>
        String Name { get; }

        /// <summary>
        /// Status that the task is in.
        /// </summary>
        TaskStatus Status { get; }

        /// <summary>
        /// Flag indicating whether the task reports progress.
        /// </summary>
        bool ReportsProgress { get; set; }

        /// <summary>
        /// Task progress reporting mechanism.
        /// </summary>
        IProgress<TaskProgress> Progress { get; }

        /// <summary>
        /// If the task is in the faulted state, this property will contain the exception that was thrown.
        /// </summary>
        Exception Exception { get; }
        #endregion // Properties

        #region Events
        /// <summary>
        /// Event that is fired when a task has been completed.
        /// </summary>
        event TaskCompletedEventHandler OnTaskCompleted;
        #endregion // Events

        #region Methods
        /// <summary>
        /// Execute the task synchronously with the provided context.
        /// </summary>
        void Execute(ITaskContext context);

        /// <summary>
        /// Execute the task asynchronously with the provided context.
        /// </summary>
        Task ExecuteAsync(ITaskContext context);

        /// <summary>
        /// Resets the task's state. (Not sure if I like this...)
        /// </summary>
        void Reset();
        #endregion // Methods
    } // ITask


    /// <summary>
    /// Delegate that is used to set a property to a new value.
    /// </summary>
    /// <param name="value"></param>
    public delegate void TaskCompletedEventHandler(object sender, TaskCompletedEventArgs e);

    
    /// <summary>
    /// Task progress arguments.
    /// </summary>
    public class TaskCompletedEventArgs : EventArgs
    {
        /// <summary>
        /// Results of the task.
        /// </summary>
        public TaskStatus Result { get; private set; }
        
        /// <summary>
        /// Return value from the task (optional).
        /// </summary>
        public object ReturnValue { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public TaskCompletedEventArgs(TaskStatus status)
            : this(status, null)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="results"></param>
        public TaskCompletedEventArgs(TaskStatus status, object returnValue)
            : base()
        {
            Result = status;
            ReturnValue = returnValue;
        }
    } // TaskCompletionArgs
}
