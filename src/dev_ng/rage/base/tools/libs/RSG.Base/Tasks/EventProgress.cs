﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace RSG.Base.Tasks
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public sealed class EventProgress<T> : IProgress<T> where T : EventArgs
    {
        /// <summary> 
        /// The underlying scheduler for the UI's synchronization context. 
        /// </summary> 
        private readonly TaskScheduler m_scheduler;

        /// <summary>
        /// Task this progress is associated with.
        /// </summary>
        private readonly ITask m_task;

        /// <summary>
        /// Occurs whenever a progress change is reported.
        /// </summary>
        public event EventHandler<T> ProgressChanged;

        /// <summary>
        /// 
        /// </summary> 
        public EventProgress(ITask task)
        {
            if (SynchronizationContext.Current != null)
            {
                m_scheduler = TaskScheduler.FromCurrentSynchronizationContext();
            }
            else
            {
                m_scheduler = null;
            }
            m_task = task;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        public void Report(T value)
        {
            if (m_scheduler != null)
            {
                System.Threading.Tasks.Task.Factory.StartNew(obj => OnProgressChanged((T)obj), value, CancellationToken.None, TaskCreationOptions.None, m_scheduler);
            }
            else
            {
                OnProgressChanged(value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="state"></param>
        private void OnProgressChanged(T value)
        {
            if (ProgressChanged != null)
            {
                ProgressChanged(m_task, value);
            }
        }
    } // EventProgress<T>
}
