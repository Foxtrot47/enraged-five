﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Tasks
{
    /// <summary>
    /// State that a task can be in.
    /// </summary>
    public enum TaskStatus
    {
        /// <summary>
        /// Task hasn't run yet.
        /// </summary>
        Pending,

        /// <summary>
        /// Task is currently running
        /// </summary>
        Running,

        /// <summary>
        /// Task successfully completed.
        /// </summary>
        Completed,

        /// <summary>
        /// Task threw an exception.
        /// </summary>
        Faulted,

        /// <summary>
        /// Task was cancelled.
        /// </summary>
        Cancelled
    } // TaskStatus
}
