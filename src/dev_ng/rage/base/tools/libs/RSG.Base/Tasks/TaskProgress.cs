﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Tasks
{
    /// <summary>
    /// 
    /// </summary>
    public class TaskProgress : EventArgs
    {
        /// <summary>
        /// Value representing how much work has been done (fraction of 1).
        /// </summary>
        public double Progress
        {
            get;
            private set;
        }

        /// <summary>
        /// Whether the value is an relative percentage or absolute.
        /// </summary>
        public bool Relative
        {
            get;
            private set;
        }

        /// <summary>
        /// Message related to this progress increment.
        /// </summary>
        public string Message
        {
            get;
            private set;
        }

        /// <summary>
        /// Absolute progress constructor.
        /// </summary>
        /// <param name="progress"></param>
        /// <param name="message"></param>
        public TaskProgress(double progress, string message)
            : this(progress, false, message)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="progress"></param>
        /// <param name="message"></param>
        public TaskProgress(double progress, bool relative, string message)
            : base()
        {
            Progress = progress;
            Relative = relative;
            Message = message;
        }
    } //ITaskProgress
}
