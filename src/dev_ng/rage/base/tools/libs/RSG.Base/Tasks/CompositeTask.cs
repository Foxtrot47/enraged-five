﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Base.Tasks
{
    /// <summary>
    /// 
    /// </summary>
    public class CompositeTask : TaskBase
    {
        #region Member Data
        /// <summary>
        /// Task that is currently being run (null if executing in parallel).
        /// </summary>
        private ITask m_currentTask;

        /// <summary>
        /// 
        /// </summary>
        private IList<ITask> m_subTasks;

        /// <summary>
        /// Mapping of subtasks to weights
        /// </summary>
        private IDictionary<ITask, double> m_subTaskWeights;

        /// <summary>
        /// Mapping of tasks to progress.
        /// </summary>
        private IDictionary<ITask, double> m_progressMap;
        #endregion // Member Data

        #region Properties
        public IList<ITask> SubTasks
        {
            get
            {
                return m_subTasks;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool ExecuteInParallel { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        public CompositeTask(string name)
            : this(name, false)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="subTasks"></param>
        /// <param name="inParallel"></param>
        public CompositeTask(string name, bool inParallel)
            : base(name)
        {
            m_subTasks = new List<ITask>();
            m_subTaskWeights = new Dictionary<ITask, double>();
            ExecuteInParallel = inParallel;
        }
        #endregion // Constructor(s)

        #region Public Interface
        /// <summary>
        /// 
        /// </summary>
        /// <param name="task"></param>
        /// <param name="weight"></param>
        public void AddSubTask(ITask task)
        {
            AddSubTask(task, 1.0);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="task"></param>
        /// <param name="weight"></param>
        public void AddSubTask(ITask task, double weight)
        {
            m_subTasks.Add(task);
            m_subTaskWeights[task] = weight;
        }
        #endregion // Public Interface

        #region TaskBase Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public override void ExecuteInternal(ITaskContext context)
        {
            m_progressMap = new Dictionary<ITask, double>();

            // Hook up the progress monitors
            foreach (ITask subTask in m_subTasks)
            {
                m_progressMap[subTask] = 0.0;

                if (subTask.ReportsProgress && subTask.Progress is EventProgress<TaskProgress>)
                {
                    ((EventProgress<TaskProgress>)subTask.Progress).ProgressChanged += new EventHandler<TaskProgress>(SubTaskProgressChanged);
                }
            }

            if (ExecuteInParallel)
            {
                // Start all the tasks simultaneously
                Task[] allTasks = new Task[m_subTasks.Count];

                for (int i = 0; i < m_subTasks.Count; ++i)
                {
                    allTasks[i] = m_subTasks[i].ExecuteAsync(context);
                }

                // Wait until all sub tasks are complete
                Task.WaitAll(allTasks);
            }
            else
            {
                // Start each task consecutively
                foreach (ITask subTask in m_subTasks)
                {
                    m_currentTask = subTask;
                    subTask.Execute(context);

                    if (subTask.Exception != null)
                    {
                        throw subTask.Exception;
                    }

                    context.Token.ThrowIfCancellationRequested();
                }

                m_currentTask = null;
            }

            context.Token.ThrowIfCancellationRequested();
        }

        /// <summary>
        /// 
        /// </summary>
        public override void Reset()
        {
            base.Reset();

            foreach (ITask subTask in m_subTasks)
            {
                subTask.Reset();
            }
        }
        #endregion // TaskBase Overrides

        #region Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SubTaskProgressChanged(object sender, TaskProgress e)
        {
            ITask task = (ITask)sender;

            if (e.Relative)
            {
                m_progressMap[task] += e.Progress;
            }
            else
            {
                m_progressMap[task] = e.Progress;
            }

            // Calculate the total progress of this task.
            double totalWeight = m_subTaskWeights.Values.Sum();

            double totalProgress = 0.0;
            foreach (ITask subTask in m_subTasks)
            {
                totalProgress += m_progressMap[subTask] * m_subTaskWeights[subTask] / totalWeight;
            }

            // Update the message based on how we are running.
            string message = null;
            if (m_currentTask != null)
            {
                message = String.Format("Executing '{0}' sub-task.", m_currentTask.Name);
            }
            else
            {
                int completedSubTasks = m_subTasks.Where(item => item.Status == TaskStatus.Completed).Count();
                message = String.Format("Completed {0}/{1} sub-tasks.", completedSubTasks, m_subTasks.Count);
            }

            // Report progress for the composite task.
            ReportTotalProgress(totalProgress, message);
        }
        #endregion // Event Handlers
    } // CompositeTask
}
