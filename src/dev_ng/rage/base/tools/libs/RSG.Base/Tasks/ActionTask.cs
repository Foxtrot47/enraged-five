﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Tasks
{
    /// <summary>
    /// 
    /// </summary>
    public class ActionTask : TaskBase
    {
        #region Properties
        /// <summary>
        /// Action to perform when running the task.
        /// </summary>
        public Action<ITaskContext, IProgress<TaskProgress>> ExecuteAction
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        public ActionTask(string name)
            : this(name, null)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="subTasks"></param>
        public ActionTask(string name, Action<ITaskContext, IProgress<TaskProgress>> action)
            : base(name)
        {
            ExecuteAction = action;
        }
        #endregion // Constructor(s)
        
        #region TaskBase Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public override void ExecuteInternal(ITaskContext context)
        {
            if (ExecuteAction != null)
            {
                ExecuteAction(context, Progress);
            }

            context.Token.ThrowIfCancellationRequested();
        }
        #endregion // TaskBase Overrides
    } // ActionTask
}
