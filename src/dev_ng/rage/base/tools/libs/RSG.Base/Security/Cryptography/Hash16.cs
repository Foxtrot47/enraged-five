﻿using System;

namespace RSG.Base.Security.Cryptography
{

    /// <summary>
    /// 16-bit unsigned hashing algorithm.
    /// </summary>
    /// <see cref="::rage::atHash16" />
    /// 
    public static class Hash16
    {
        /// <summary>
        /// Produce a 16-bit unsigned hash.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        /// Equivalent of ::rage::atHash16 and ::rage::atHash16U.
        /// 
        /// Using these functions saves using RSG.ManagedRage and crossing
        /// the managed/native boundary.
        /// 
        public static UInt16 ComputeHash(String input)
        {
            UInt32 hash = Hash.atHash_String(input);
            UInt32 prime = 65167;

            return ((UInt16)((hash % prime) + (0xFFFF - prime)));
        }
    }

} // RSG.Base.Security.Cryptography namespace
