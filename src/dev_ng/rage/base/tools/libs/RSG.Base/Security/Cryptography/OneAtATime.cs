﻿using System;
using System.Diagnostics;
using System.Security.Cryptography;
using System.Text;

namespace RSG.Base.Security.Cryptography
{

    /// <summary>
    /// 32-bit unsigned string hashing algorithm (see references).
    /// Equivalent to atStringHash.
    /// </summary>
    /// <see cref="http://burtleburtle.net/bob/hash/doobs.html"/>
    /// <see cref="$(RAGE_DIR)\base\src\string\stringhash.h"/>
    /// <see cref="::rage::atStringHash" />
    /// This is the equivalent of RAGE atStringHash.
    /// 
    /// Using these functions saves using RSG.ManagedRage and crossing
    /// the managed/native boundary.
    ///
    public static class OneAtATime
    {
        /// <summary>
        /// Produce a very robust full 32-bit hash for a string
        ///	Swat was able to throw several thousand strings at this
        ///	function with no overlap.  Should be better than storing
        ///	a full filename, etc, in situations where you know you
        ///	will not be querying objects that do not exist.
        /// </summary>
        /// <param name="input"></param>
        /// <returns>32 bit hash code for string.</returns>
        /// <see cref="$(RAGE_DIR)\base\src\string\stringhash.h"/>
        /// 
        public static UInt32 ComputeHash(String input)
        {
            UInt32 hash = ComputePartialHash(input, 0);
            return (ComputeFinalHash(hash));
        }

        /// <summary>
        /// Produces a partial hash to be used to combine hash values.
        /// </summary>
        /// <param name="input">
        /// String to produce hash for; uppercase letters are
        /// mapped to lowercase, and if the string starts with
        ///	a quote it is expected to be terminated with one
        ///	as well (a NUL always terminates a string even if one
        ///	is encountered before the closing quote).  Backslashes
        ///	are also normalized to forward slashes before hashing
        ///	so that this function is more useful for filenames.
        ///	</param>
        /// <param name="initValue">
        /// Initial value of the hash.  This has to be a partial hash,
        ///	Useful for incremental hashing.
        /// </param>
        /// <returns>32 bit hash code for string.</returns>
        /// <see cref="$(RAGE_DIR)\base\src\string\stringhash.h"/>
        /// 
        public static UInt32 ComputePartialHash(String input, UInt32 initValue)
        {
	        // This is the one-at-a-time hash from this page:
	        // http://burtleburtle.net/bob/hash/doobs.html
	        // (borrowed from /soft/swat/src/swcore/string2key.cpp)

            if (String.IsNullOrEmpty(input))
                input = String.Empty;

            UInt32 hash = 0;
            String normalisedInput = input.Replace("\"", string.Empty).Replace("\\", "/").ToLower();
            Byte[] inputData = Encoding.UTF8.GetBytes(normalisedInput);
            for (int index = 0; index < inputData.Length; ++index)
            {
                hash += inputData[index];
                hash += hash << 10; // lint !e701
                hash ^= hash >> 6; // lint !e702
            }

            // Return partial hash.
            return (hash);
        }

        /// <summary>
        /// Finalize the supplied partial hash.  This 'avalanches' the final input
        //	bits across all output bytes.
        /// </summary>
        /// <param name="partialHashValue">The partial hash value.</param>
        /// <returns>32 bit hash code for string.</returns>
        /// 
        public static UInt32 ComputeFinalHash(UInt32 partialHashValue)
        {
            UInt32 key = partialHashValue;
            key += (key << 3);
            key ^= (key >> 11);
            key += (key << 15);
            return (key);
        }
    }

} // RSG.Base.Security.Cryptography namespace
