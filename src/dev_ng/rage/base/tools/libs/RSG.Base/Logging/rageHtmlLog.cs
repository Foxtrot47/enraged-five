using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using RSG.Base.Command;
using RSG.Base.IO;

namespace RSG.Base.Logging
{
	/// <summary>
	/// Summary description for rageHtmlLog.
	/// </summary>
	public class rageHtmlLog : IDisposable
	{
		private static string strBackgroundColour = "#EFEFFF";
		private static string strTitleBackgroundColour = "#CCCCFF";
		private static string strTimeBackgroundColour = "#DDEEFF";
		private static string strEventBackgroundColour = "#DDFFFF";

		// Constructor
		public rageHtmlLog(string strFilename, string strLogTitle)
		{
			// Init everything
			m_astrLogEntries.Clear();
			m_astrLogTimes.Clear();
            m_logToStandardOutput = false;

			// Set the filename
			m_strFilename = strFilename;

			// Set the title
			m_strLogTitle = strLogTitle;

			// Does the path exist for the log?
			string strPath = rageFileUtilities.GetLocationFromPath(strFilename);
			if (!Directory.Exists(strPath))
			{
				// Directory does not exist, so create it
				rageFileUtilities.MakeDir(strPath);
			}
		}

		public void Dispose()
		{
			if (m_iNoOfLinesLastTimeItWasWritten != m_astrLogTimes.Count)
			{
				// More stuff to write out, so do it
				UpdateLogAfterEverySoManyLines = 1;
			}
		}

		// Accessors and modifiers
		//		public static void SetFilename(string strFilename) 
		//		{
		//			// Init everything
		//			m_astrLogEntries.Clear();
		//			m_astrLogTimes.Clear();
		//
		//			// Set the filename
		//			m_strFilename = strFilename;
		//		}

		//		public static void SetLogTitle(string strLogTitle)
		//		{
		//			m_strLogTitle = strLogTitle;
		//		}

		public string GetFilename() { return m_strFilename; }

		public void LogEvent(string strLogEntry)
		{
			rageStatus obStatus;
			LogEvent(strLogEntry, out obStatus);
		}

		public void LogEvent(string strLogEntry, out rageStatus obStatus)
		{
			if (strLogEntry == null)
			{
				strLogEntry = "";
			}
			lock (this)
			{
                if (m_logToStandardOutput == true)
                    Console.WriteLine(strLogEntry);

				// If array is full, expand it by a LOT, expanding one item at a time really can slow things down if they log A LOT of 
				// things really quickly.  Interestingly, "List<>"s are not actually link-lists under the hood, they are much closer to 
				// traditional arrays, if they really were linked lists it really wouldn't matter what the capacity was
				if (m_astrLogTimes.Count == m_astrLogTimes.Capacity)
				{
					// System.DateTime obTimeBefore = System.DateTime.Now;
					m_astrLogTimes.Capacity = m_astrLogTimes.Capacity + 500;
					m_astrLogEntries.Capacity = m_astrLogEntries.Capacity + 500;
					// m_astrLogTimes.Add(obTimeBefore);
					// m_astrLogEntries.Add("<font color=#FF00FF>Increasing log capacity from " + (m_astrLogTimes.Capacity - 500) + " to " + m_astrLogTimes.Capacity +"</font>");
					// m_astrLogTimes.Add(System.DateTime.Now);
					// m_astrLogEntries.Add("<font color=#FF00FF>Capacity increased</font>");
				}

				// Add event
				m_astrLogTimes.Add(System.DateTime.Now);

				// Make strLogEntry more html friendly
				strLogEntry = strLogEntry.Replace("\\n", "<br>").Replace("\n", "<br>");

				// Add it
				m_astrLogEntries.Add(strLogEntry);
			}

			// Output the latest version of the log
			UpdateLog(out obStatus);
		}

		private void UpdateLog(out rageStatus obStatus)
		{
			// Do I have a filename?
			if (GetFilename() == "")
			{
				// No filename, so error
				obStatus = new rageStatus("No filename provided to write log to");
				return;
			}

			// Create the log file
			if ((UpdateLogAfterEverySoManyLines > 0) && ((m_astrLogTimes.Count % UpdateLogAfterEverySoManyLines) == 0))
			{
				lock (this)
				{
					// Open file for write
					StreamWriter logWriter;
					try
					{
						logWriter = File.CreateText(GetFilename());
					}
					catch (Exception e)
					{
						// Something bad happened
						obStatus = new rageStatus("Failed to create file " + GetFilename() + " : " + e.ToString());
						return;
					}

					// Write header
					logWriter.WriteLine("<html><head><title>" + m_strLogTitle + "</title></head><body bgcolor=" + strBackgroundColour + ">");
					logWriter.WriteLine("<h1>" + m_strLogTitle + "</h1>");
					logWriter.WriteLine("<table border=0 cellspacing=1 cellpadding=1>");
					logWriter.WriteLine("<tr valign=TOP><td align=left bgcolor=" + strTitleBackgroundColour + ">Last update</td><td align=left bgcolor=" + strEventBackgroundColour + ">" + System.DateTime.Now.ToLongTimeString() + " " + System.DateTime.Now.ToLongDateString() + "</td></tr>\n");
					logWriter.WriteLine("<tr valign=TOP><td align=left bgcolor=" + strTitleBackgroundColour + ">Computer Name</td><td align=left bgcolor=" + strEventBackgroundColour + ">" + Environment.GetEnvironmentVariable("COMPUTERNAME") + "</td></tr>\n");
					logWriter.WriteLine("<tr valign=TOP><td align=left bgcolor=" + strTitleBackgroundColour + ">User Name</td><td align=left bgcolor=" + strEventBackgroundColour + ">" + Environment.GetEnvironmentVariable("USERNAME") + "</td></tr>\n");
					logWriter.WriteLine("<tr valign=TOP><td align=left bgcolor=" + strTitleBackgroundColour + ">Command Line</td><td align=left bgcolor=" + strEventBackgroundColour + ">" + Environment.CommandLine + "</td></tr>\n");
					logWriter.WriteLine("</table>\n");
					logWriter.WriteLine("<p>\n");

					// Write Prologue
					logWriter.WriteLine(m_strPrologue);

					// Add log
					// Log Header
					if ((m_strPrologue != "") || (m_strEpilogue != ""))
					{
						logWriter.WriteLine("<h2>Log</h2>");
					}
					logWriter.WriteLine("<table border=0 cellspacing=1 cellpadding=1 width=100%>");
					logWriter.WriteLine("<tr valign=TOP><td align=left bgcolor=" + strTitleBackgroundColour +
                        "><b>Time</b></td><td align=left bgcolor=" + strTitleBackgroundColour +
                        "><b>Ticks</b></td><td align=left bgcolor=" + strTitleBackgroundColour + "><b>Event</b></td></tr>\n");

					// Log
					int iNoOfLogEntries = m_astrLogTimes.Count;
					if (iNoOfLogEntries > m_astrLogEntries.Count) iNoOfLogEntries = m_astrLogEntries.Count;
					for (int i = 0; i < iNoOfLogEntries; i++)
					{
						logWriter.WriteLine("<tr valign=TOP><td align=left bgcolor=" + strTimeBackgroundColour + "><small>" +
                            m_astrLogTimes[i].ToString().Replace(" ", "&nbsp;") + "</small></td><td align=left bgcolor=" + strTimeBackgroundColour + "><small>" +
                            m_astrLogTimes[i].Ticks.ToString() + "</small></td><td align=left bgcolor=" +
                            strEventBackgroundColour + "><small>" + m_astrLogEntries[i] + "</small></td></tr>\n");
					}

					// Log Footer
					logWriter.WriteLine("</table>\n");
					logWriter.WriteLine("<p>\n");

					// Write Epilogue
					logWriter.WriteLine(m_strEpilogue);

					// Write footer
					logWriter.WriteLine("<center>Automagically Generated By Kevin's C# HTML Logger</center>\n");
					logWriter.WriteLine("<center>Last update: " + System.DateTime.Now.ToLongTimeString() + " " + System.DateTime.Now.ToLongDateString() + "</center>\n");
					logWriter.WriteLine("<center><SCRIPT LANGUAGE=\"JavaScript\">\nvar theDate = document.lastModified\ndocument.write(\"Last upload: \"));\ndocument.write(theDate));\n</SCRIPT></center>\n");
					logWriter.WriteLine("<a name=\"LogEnd\">");

					// Close file
					logWriter.Close();
					m_iNoOfLinesLastTimeItWasWritten = m_astrLogTimes.Count;
				}
			}
			obStatus = new rageStatus();
		}

		// Members
		private string m_strFilename;
		private string m_strLogTitle;
		private List<string> m_astrLogEntries = new List<string>();
		private List<DateTime> m_astrLogTimes = new List<DateTime>();
		public string LogEntriesAsHtmlString
		{
			get
			{
				// Header
				StringBuilder strLogEntriesAsHtmlTable = new StringBuilder();
				strLogEntriesAsHtmlTable.AppendLine("<table border=0 cellspacing=1 cellpadding=1 width=100%>");
				strLogEntriesAsHtmlTable.AppendLine("<tr valign=TOP><td align=left bgcolor=" + strTitleBackgroundColour + "><b>Time</b></td><td align=left bgcolor=" + strTitleBackgroundColour + "><b>Event</b></td></tr>");

				// Log
				int iNoOfLogEntries = m_astrLogTimes.Count;
				if (iNoOfLogEntries > m_astrLogEntries.Count) iNoOfLogEntries = m_astrLogEntries.Count;
				for (int i = 0; i < iNoOfLogEntries; i++)
				{
					strLogEntriesAsHtmlTable.AppendLine("<tr valign=TOP><td align=left bgcolor=" + strTimeBackgroundColour + "><small>" + m_astrLogTimes[i].ToString().Replace(" ", "&nbsp;") + "</small></td><td align=left bgcolor=" + strEventBackgroundColour + "><small>" + m_astrLogEntries[i] + "</small></td></tr>");
				}

				// Footer
				strLogEntriesAsHtmlTable.AppendLine("</table>");
				return strLogEntriesAsHtmlTable.ToString();
			}
		}

		public bool UpdateFileInRealTime
		{
			get
			{
				return (UpdateLogAfterEverySoManyLines == 1);
			}
			set
			{
				if (value)
				{
					UpdateLogAfterEverySoManyLines = 1;
				}
				else
				{
					UpdateLogAfterEverySoManyLines = 0;
				}
			}
		}

		int m_iUpdateLogAfterEverySoManyLines = 1;
		public int UpdateLogAfterEverySoManyLines
		{
			get
			{
				return m_iUpdateLogAfterEverySoManyLines;
			}
			set
			{
				int iNoOfLinesBeforeBefore = UpdateLogAfterEverySoManyLines;
				m_iUpdateLogAfterEverySoManyLines = value;
				if (iNoOfLinesBeforeBefore != m_iUpdateLogAfterEverySoManyLines)
				{
					// The state changed, so update the log
					rageStatus obStatus;
					UpdateLog(out obStatus);
				}
			}
		}

		private int m_iNoOfLinesLastTimeItWasWritten = 0;

		private string m_strPrologue = "";
		public string Prologue
		{
			set
			{
				int iNoOfLinesBeforeBefore = UpdateLogAfterEverySoManyLines;
				m_strPrologue = value;

				// Force update
				UpdateLogAfterEverySoManyLines = 1;

				rageStatus obStatus;
				UpdateLog(out obStatus);

				// Restore state
				UpdateLogAfterEverySoManyLines = iNoOfLinesBeforeBefore;
			}
		}

		private string m_strEpilogue = "";
		public string Epilogue
		{
			set
			{
				int iNoOfLinesBeforeBefore = UpdateLogAfterEverySoManyLines;
				m_strEpilogue = value;

				// Force update
				UpdateLogAfterEverySoManyLines = 1;

				rageStatus obStatus;
				UpdateLog(out obStatus);

				// Restore state
				UpdateLogAfterEverySoManyLines = iNoOfLinesBeforeBefore;
			}
		}

		public int NumberOfLogEntries
		{
			get
			{
				return m_astrLogTimes.Count;
			}
		}

        private bool m_logToStandardOutput;
        public bool LogToStandardOutput
        {
            get { return m_logToStandardOutput; }
            set { m_logToStandardOutput = value; }
        }
	}
}

