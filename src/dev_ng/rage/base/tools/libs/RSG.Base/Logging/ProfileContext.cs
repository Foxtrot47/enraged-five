﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Logging
{

    /// <summary>
    /// Profile context object provides a safe method to log profile calls
    /// into our logging system.
    /// </summary>
    /// <example>
    /// ILog log = ...
    /// using (ProfileContext ctx = new ProfileContext(log, "Testing 123...")
    /// {
    ///     ... // code to profile
    /// }
    /// </example>
    /// 
    public sealed class ProfileContext : IDisposable
    {
        #region Member Data
        /// <summary>
        /// Log object to add profiling information to.
        /// </summary>
        private ILog m_LogContext;
        #endregion // Member Data

        #region Constructor(s) / IDisposable Interface Methods
        /// <summary>
        /// Constructor; specifying target log.  Initiate profile.
        /// </summary>
        /// <param name="log"></param>
        public ProfileContext(ILog log, String message, params Object[] args)
        {
            this.m_LogContext = log;
            this.m_LogContext.Profile(message, args);
        }

        /// <summary>
        /// Constructor; specifying target log and context.  Initiate profile.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="context"></param>
        /// <param name="message"></param>
        /// <param name="args"></param>
        public ProfileContext(ILog log, String context, String message, params Object[] args)
        {
            this.m_LogContext = log;
            this.m_LogContext.ProfileCtx(context, message, args);
        }

        /// <summary>
        /// Dispose; end the profile call.
        /// </summary>
        public void Dispose()
        {
            this.m_LogContext.ProfileEnd();
        }
        #endregion // Constructor(s) / IDisposable Interface Methods
    }

} // RSG.Base.Logging namespace
