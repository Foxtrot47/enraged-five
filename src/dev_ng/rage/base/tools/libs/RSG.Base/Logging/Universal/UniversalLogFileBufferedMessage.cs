﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Xml;

namespace RSG.Base.Logging.Universal
{

    /// <summary>
    /// Buffered Universal Log File message object.
    /// </summary>
    public class UniversalLogFileBufferedMessage
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="MessageId"/> property.
        /// </summary>
        private uint _messageId;

        /// <summary>
        /// The private field used for the <see cref="Context"/> property.
        /// </summary>
        private string context;

        /// <summary>
        /// The private field used for the <see cref="Level"/> property.
        /// </summary>
        private LogLevel level;

        /// <summary>
        /// The private field used for the <see cref="Timestamp"/> property.
        /// </summary>
        private DateTime timestamp;
        #endregion

        #region Properties
        /// <summary>
        /// Message identifier; used for duplicate message processing.
        /// </summary>
        public uint MessageId
        {
            get { return this._messageId; }
        }

        /// <summary>
        /// Log message level.
        /// </summary>
        public LogLevel Level
        {
            get { return this.level; }
        }

        /// <summary>
        /// Log message timestamp (UTC).
        /// </summary>
        public DateTime Timestamp
        {
            get { return this.timestamp; }
        }

        /// <summary>
        /// Log message context.
        /// </summary>
        public String Context
        {
            get { return this.context; }
        }

        /// <summary>
        /// Log message string.
        /// </summary>
        public String Message
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="level"></param>
        /// <param name="ctx"></param>
        /// <param name="message"></param>
        public UniversalLogFileBufferedMessage(LogLevel level, String ctx,
            String message, String preformattedMessage)
            : this(level, ctx, message, preformattedMessage, DateTime.UtcNow)
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="level"></param>
        /// <param name="ctx"></param>
        /// <param name="message"></param>
        /// <param name="preformattedMessage"></param>
        /// <param name="timestamp"></param>
        public UniversalLogFileBufferedMessage(LogLevel level, String ctx,
            String message, String preformattedMessage, DateTime timestamp)
        {
            this._messageId = GetMessageId(preformattedMessage);
            this.level = level;
            this.timestamp = timestamp;
            this.context = ctx;

            this.Message = message;
        }

        /// <summary>
        /// Constructor; from XML element.
        /// </summary>
        /// <param name="xmlElem"></param>
        public UniversalLogFileBufferedMessage(XElement xmlElem)
        {
            uint id = 0;
            XAttribute idAttribute = xmlElem.Attribute("id");
            if (null != idAttribute)
            {
                if (uint.TryParse(idAttribute.Value, out id))
                    this._messageId = id;
            }

            this.level = LogLevelUtils.GetLogLevelFromXmlElementName(xmlElem.Name.LocalName);
            this.Message = xmlElem.Value;

            long ticks;
            XAttribute tickAttribute = xmlElem.Attribute("timestamp_ticks");
            if (tickAttribute != null)
            {
                if (Int64.TryParse(tickAttribute.Value, out ticks))
                {
                    this.timestamp = new DateTime(ticks);
                }
            }
            else
            {
                DateTime parsedTimestamp;
                XAttribute timestampAttribute = xmlElem.Attribute("timestamp");
                if (timestampAttribute != null && DateTime.TryParse(timestampAttribute.Value, out parsedTimestamp))
                {
                    this.timestamp = parsedTimestamp;
                }
            }

            XAttribute contextAttribute = xmlElem.Attribute("context");
            if (contextAttribute != null)
            {
                this.context = contextAttribute.Value;
            }
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Convert to XML element.
        /// </summary>
        /// <returns></returns>
        public XElement ToXml()
        {
            XElement xmlMessageElm = new XElement(this.Level.GetXmlElementName());
            xmlMessageElm.SetAttributeValue("id", this.MessageId);
            xmlMessageElm.SetAttributeValue("timestamp_ticks", this.Timestamp.Ticks);
            if (this.Context != null)
            {
                xmlMessageElm.SetAttributeValue("context", this.Context);
            }
            if (this.Message != null)
            {
                xmlMessageElm.Value = ReplaceInvalidChars(this.Message);
            }
            return xmlMessageElm;
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Calculate the message identifier; expected input is preformatted 
        /// message string.
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        private uint GetMessageId(String message)
        {
            // DHM : this will likely change which is why its abstracted into 
            // this method.  Ideally something we can use outside of .Net;
            // e.g. OneAtATime.
            if (String.IsNullOrEmpty(message))
                return (0);
            return ((uint)message.GetHashCode());
        }

        /// <summary>
        /// Strips out invalid xml characters.
        /// </summary>
        private String RemoveInvalidXmlChars(string text)
        {
            char[] validXmlChars = text.Where(ch => XmlConvert.IsXmlChar(ch)).ToArray();
            return new String(validXmlChars);
        }

        /// <summary>
        /// Replaces all invalid XML characters in a string with another character.
        /// </summary>
        private String ReplaceInvalidChars(String text, Char replacement = '\xD7')
        {
            char[] validXmlChars = text.Select(ch => (XmlConvert.IsXmlChar(ch) ? ch : replacement)).ToArray();
            return new String(validXmlChars);
        }
        #endregion // Private Methods
    }

} // RSG.Base.Logging.Universal namespace
