﻿//---------------------------------------------------------------------------------------------
// <copyright file="ThreadedLogTargetBase.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Base.Logging
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    /// <summary>
    /// Abstract base class for all LogTargets.
    /// </summary>
    /// This is a threaded version of the <see cref="LogTargetBase"/> class.  This 
    /// adds an internal queue and a consumer thread allowing decoupling of the
    /// message locks to ensure that messages are received in the correct order.
    /// 
    public abstract class ThreadedLogTargetBase : ILogTarget
    {
        #region Properties
        /// <summary>
        /// Log enabled flag
        /// </summary>
        public bool Enabled { get; set; }

        /// <summary>
        /// Timestamp log entries flag
        /// </summary>
        public bool Timestamp { get; set; }

        /// <summary>
        /// ShowContext.
        /// </summary>
        public bool ShowContext { get; set; }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Collection of connected log objects.
        /// </summary>
        protected ICollection<ILog> _connectedLogs;

        /// <summary>
        /// Incoming message queue; consumed in a separate thread.
        /// </summary>
        private BlockingCollection<LogMessageEventArgs> _messageQueue;

        /// <summary>
        /// Incoming message queue processing task.
        /// </summary>
        private Task _processTask;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public ThreadedLogTargetBase()
        {
            this.Enabled = true;
            this._connectedLogs = new List<ILog>();
            this._messageQueue = new BlockingCollection<LogMessageEventArgs>();
            this._processTask = Task.Factory.StartNew(() => 
            {
                foreach (LogMessageEventArgs e in this._messageQueue.GetConsumingEnumerable())
                {
                    this.LogMessage(e);
                }
            }, TaskCreationOptions.LongRunning);
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Abstract method that handles logging (called within thread).
        /// </summary>
        /// <param name="args"></param>
        protected abstract void LogMessage(LogMessageEventArgs args);

        /// <summary>
        /// Connect to a log object.
        /// </summary>
        /// <param name="log"></param>
        public void Connect(ILog log)
        {
            log.LogMessage += this.LogEvent;
            log.LogError += this.LogEvent;
            log.LogToolError += this.LogEvent;
            log.LogToolException += this.LogEvent;
            log.LogWarning += this.LogEvent;
            log.LogProfile += this.LogEvent;
            log.LogProfileEnd += this.LogEvent;
            log.LogDebug += this.LogEvent;
            
            lock (this._connectedLogs)
            {
                if (this._connectedLogs.Contains(log))
                    return;

                this._connectedLogs.Add(log);
            }
        }

        /// <summary>
        /// Connect to a log object (up to a particular level).
        /// </summary>
        /// <param name="log"></param>
        /// <param name="uptoLevel"></param>
        public void Connect(ILog log, LogLevel uptoLevel)
        {
            if ((int)uptoLevel >= (int)LogLevel.ToolException)
                log.LogToolException += this.LogEvent;
            if ((int)uptoLevel >= (int)LogLevel.ToolError)
                log.LogToolError += this.LogEvent;
            if ((int)uptoLevel >= (int)LogLevel.Error)
                log.LogError += this.LogEvent;
            if ((int)uptoLevel >= (int)LogLevel.Warning)
                log.LogWarning += this.LogEvent;
            if ((int)uptoLevel >= (int)LogLevel.Info)
                log.LogMessage += this.LogEvent;
            if ((int)uptoLevel >= (int)LogLevel.Profile)
            {
                log.LogProfile += this.LogEvent;
                log.LogProfileEnd += this.LogEvent;
            }
            if ((int)uptoLevel >= (int)LogLevel.Debug)
                log.LogDebug += this.LogEvent;

            lock (this._connectedLogs)
            {
                if (this._connectedLogs.Contains(log))
                    return;

                this._connectedLogs.Add(log);
            }
        }

        /// <summary>
        /// Determine whether we are connected to a particular log.
        /// </summary>
        /// <param name="log"></param>
        /// <returns></returns>
        public bool IsConnected(ILog log)
        {
            lock (this._connectedLogs)
                return (this._connectedLogs.Contains(log));
        }

        /// <summary>
        /// Disconnect from a log object.
        /// </summary>
        /// <param name="log"></param>
        public void Disconnect(ILog log)
        {
            log.LogMessage -= this.LogEvent;
            log.LogError -= this.LogEvent;
            log.LogToolError -= this.LogEvent;
            log.LogToolException -= this.LogEvent;
            log.LogWarning -= this.LogEvent;
            log.LogProfile -= this.LogEvent;
            log.LogProfileEnd -= this.LogEvent;
            log.LogDebug -= this.LogEvent;

            lock (this._connectedLogs)
            {
                Debug.Assert(this._connectedLogs.Contains(log),
                    "Log not connected to this LogTarget!  Ignoring.");
                if (!this._connectedLogs.Contains(log))
                    return;

                this._connectedLogs.Remove(log);
            }
        }

        /// <summary>
        /// Disconnect from all registered logs.
        /// </summary>
        public void DisconnectAll()
        {
            lock (this._connectedLogs)
            {
                while (this._connectedLogs.Count > 0)
                    Disconnect(this._connectedLogs.First());
            }
        }

        /// <summary>
        /// Reconnect to all logs objects (up to a particular level).
        /// </summary>
        /// <param name="uptoLevel"></param>
        public void Reconnect(LogLevel uptoLevel)
        {
            lock (this._connectedLogs)
            {
                ICollection<ILog> logs = new List<ILog>(this._connectedLogs);
                foreach (ILog log in logs)
                {
                    this.Disconnect(log);
                    this.Connect(log, uptoLevel);
                }
            }
        }

        /// <summary>
        /// Allows the log target to perform cleanup operations prior
        /// to application shutdown.
        /// </summary>
        public void Shutdown()
        {
            this._messageQueue.CompleteAdding();
            this._processTask.Wait();
        }

        /// <summary>
        /// Flushes the log out to the target
        /// </summary>
        public abstract void Flush();
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Log message event handler; this is within the ILog lock so should
        /// do as little as possible (all we do is enqueue).
        /// </summary>
        private void LogEvent(Object sender, LogMessageEventArgs e)
        {
            if (!this.Enabled)
                return;

            this._messageQueue.Add(e);
        }
        #endregion // Private Methods
    }

} // RSG.Base.Logging namespace
