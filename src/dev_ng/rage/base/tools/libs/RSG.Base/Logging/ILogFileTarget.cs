﻿using System;

namespace RSG.Base.Logging
{

    /// <summary>
    /// Log file flush mode.
    /// </summary>
    /// This can be used to change the automatic behaviour of a log file 
    /// flushing to disk.
    /// 
    [Flags]
    public enum FlushMode
    {
        Never = 0x0000,
        Error = 0x0001,
        Warning = 0x0002,
        Info = 0x0004,
        Profile = 0x0008,
        Debug = 0x0010,
        Always = 0xFFFF,

        Default = Error,
    }

    /// <summary>
    /// Log file interface.
    /// </summary>
    public interface ILogFileTarget : ILogTarget, IDisposable
    {
        #region Properties
        /// <summary>
        /// Log file flush mode.
        /// </summary>
        FlushMode FlushMode { get; }

        /// <summary>
        /// Log filename on disk.
        /// </summary>
        String Filename { get; }
        #endregion // Properties
    }

} // RSG.Base.Logging namespace
