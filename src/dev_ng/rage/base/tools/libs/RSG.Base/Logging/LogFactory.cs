﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using RSG.Base.Logging.Universal;
using RSG.Base.Extensions;

namespace RSG.Base.Logging
{

    /// <summary>
    /// Log and log target factory.
    /// </summary>
    public static class LogFactory
    {
        #region Constants
        /// <summary>
        /// Subdirectory under %RS_TOOLSROOT% for log files.
        /// </summary>
        private static readonly String LOG_DIRECTORY = "logs";

        /// <summary>
        /// Universal Log Viewer executable path.
        /// </summary>
        private static readonly String LOG_VIEWER = @"bin\UniversalLogViewer\UniversalLogViewer.exe";
        #endregion // Constants

        #region Enumerations
        /// <summary>
        /// Application log shutdown mode; options to display log viewer.
        /// </summary>
        public enum ShutdownMode
        {
            NoLogDisplay,   // Do not display log viewer; default.
            DisplayOnError, // Display log viewer on error.
            DisplayAlways,  // Always display log viewer.
        }
        #endregion // Enumerations

        #region Properties
        /// <summary>
        /// Collection of all logs constructed by the factory.
        /// </summary>
        public static IEnumerable<IUniversalLog> Logs
        {
            get { return m_logs; }
        }
        private static ICollection<IUniversalLog> m_logs = new List<IUniversalLog>();

        private static Object m_logLock = new Object();

        /// <summary>
        /// Collection of all log targets constructed by the factory.
        /// </summary>
        public static IEnumerable<ILogTarget> LogTargets
        {
            get { return m_logTargets; }
        }
        private static ICollection<ILogTarget> m_logTargets = new List<ILogTarget>();

        /// <summary>
        /// Log directory for the current process' logs.
        /// </summary>
        public static String ProcessLogDirectory
        {
            get;
            private set;
        }

        /// <summary>
        /// Log directory for the parent process' logs.
        /// </summary>
        public static String ParentProcessLogDirectory
        {
            get;
            private set;
        }

        /// <summary>
        /// Application-root log object.
        /// </summary>
        public static IUniversalLog ApplicationLog { get; private set; }

        /// <summary>
        /// Application-root log file.
        /// </summary>
        private static IUniversalLogTarget ApplicationLogFile { get; set; }

        /// <summary>
        /// Application-root log filename.
        /// </summary>
        public static String ApplicationLogFilename
        {
            get
            {
                String filename = "";
                if (ApplicationLogFile != null)
                    filename = ((ThreadedUniversalLogFile)ApplicationLogFile).Filename;
                return filename;
            }
        }

        /// <summary>
        /// Instance of ConsoleLog; if initialised we connect all logs
        /// and maintain those as new logs are created.
        /// </summary>
        public static ILogTarget ApplicationConsoleLog { get; private set; }

        /// <summary>
        /// Private current LogLevel value.
        /// </summary>
        /// <see cref="GetLogLevel"/>
        /// <see cref="SetLogLevel"/>
        private static LogLevel CurrentLogLevel
        {
            get;
            set;
        }

        /// <summary>
        /// Whether the log factory has been initialised and the
        /// process log has been setup.
        /// </summary>
        internal static bool Initialised
        {
            get;
            set;
        }
        private static object s_initialisedSyncContext = new object();
        #endregion // Properties
        
        #region Static Controller Methods
        /// <summary>
        /// Whether the log factory has been initialised and the
        /// process log has been setup.
        /// </summary>
        /// <param name="logSubdirectory"></param>
        /// <param name="checkForParentProcess"></param>
        public static void Initialize(string logSubdirectory = null, bool checkForParentProcess = true)
        {
            if (!Initialised)
            {
                lock (s_initialisedSyncContext)
                {
                    if (!Initialised)
                    {
#if DEBUG
                        CurrentLogLevel = LogLevel.Debug;
#else
                        CurrentLogLevel = LogLevel.Profile;
#endif
                        lock (m_logLock)
                        {
                            m_logs = new List<IUniversalLog>();
                        }
                        m_logTargets = new List<ILogTarget>();

                        Process process = Process.GetCurrentProcess();
                        String processName = process.ProcessName;
                        if (processName.Equals("ir", StringComparison.OrdinalIgnoreCase))
                        {
                            // Find IronRuby script filename; ending ".rb".
                            IEnumerable<String> args = Environment.GetCommandLineArgs();
                            String rubyScriptName = args.FirstOrDefault(a => a.EndsWith(".rb"));
                            if (!String.IsNullOrEmpty(rubyScriptName))
                                processName = Path.GetFileNameWithoutExtension(rubyScriptName);
                        }

                        Initialised = true;
                    }
                }
            }

            // Initialize might get called multiple times so make sure we don't recreate the application log again.
            if (ApplicationLogFile == null)
                CreateProcessLog(logSubdirectory, checkForParentProcess);
        }

        /// <summary>
        /// Check to make sure the LogFactory has been properly initialised before making any calls to it.
        /// </summary>
        private static void ValidateLogFactory()
        {
            Debug.Assert(Initialised, "LogFactory has not been initialised. Call LogFactory.Initialize() before attempting to use it.");
            if (!Initialised)
                throw new Exception("LogFactory has not been initialised. Call LogFactory.Initialize() before attempting to use it.");
        }

        /// <summary>
        /// Set the currently configured log level; disconnects and reconnects logs.
        /// </summary>
        /// <param name="level"></param>
        public static void SetLogLevel(LogLevel level)
        {
            if (CurrentLogLevel == level)
                return;
            
            // Loop through log targets to Disconnect/Connect them.
            CurrentLogLevel = level;
            foreach (ILogTarget logTarget in LogFactory.LogTargets)
                logTarget.Reconnect(level);
        }

        /// <summary>
        /// Return the currently configured log level.
        /// </summary>
        /// <returns></returns>
        public static LogLevel GetLogLevel()
        {
            return (LogFactory.CurrentLogLevel);
        }

        /// <summary>
        /// Create a new Universal Log; attaching to application logging system.
        /// </summary>
        /// <param name="name">Log name</param>
        /// <returns>Universal Log object</returns>
        public static IUniversalLog CreateUniversalLog(String name)
        {
            return (CreateUniversalLog(name, true));
        }

        /// <summary>
        /// Create a new Universal Log; optionally attaching to application
        /// logging system.
        /// </summary>
        /// <param name="name">Log name</param>
        /// <param name="attachToAppLog">Whether to connect into app log system.</param>
        /// <returns>Universal Log object</returns>
        public static IUniversalLog CreateUniversalLog(String name, bool attachToAppLog)
        {
            ValidateLogFactory();

            IUniversalLog newLog = new UniversalLog(name);
            lock (m_logLock)
            {
                LogFactory.m_logs.Add(newLog);
            }

            if (attachToAppLog)
            {
                if (null != LogFactory.ApplicationLog)
                    LogFactory.ApplicationLog.Connect(newLog);
            }
            return (newLog);
        }

        /// <summary>
        /// Create a new Universal Log file target; attaching to a log.
        /// </summary>
        /// <param name="log"></param>
        /// <returns></returns>
        public static IUniversalLogTarget CreateUniversalLogFile(IUniversalLog log)
        {
            return (LogFactory.CreateUniversalLogFile(log.Name, log));
        }
        
        /// <summary>
        /// Create a new universal log file target; with a user-defined prefix,
        /// attaching to a log and within a particular subdirectory path from
        /// the tools logs directory.
        /// </summary>
        /// <param name="prefix"></param>
        /// <param name="log"></param>
        /// <param name="paths"></param>
        /// <returns></returns>
        public static IUniversalLogTarget CreateUniversalLogFile(String prefix,
            IUniversalLog log, params String[] paths)
        {
            LogFactory.Initialize();
            ValidateLogFactory();
            Debug.Assert(null != log, "Invalid log specified.  Aborting.");
            if (null == log)
                throw new ArgumentNullException("log");

            String filename = GenerateUniversalLogFileName(prefix, paths);
            IUniversalLogTarget logFile = new ThreadedUniversalLogFile(filename, FileMode.CreateNew, FlushMode.Error, log);
            LogFactory.m_logTargets.Add(logFile);
            return (logFile);
        }
        
        /// <summary>
        /// Create an application-wide ConsoleLog; connecting all system logs 
        /// to use it as a target.  All subsequent logs are connected too.
        /// </summary>
        public static void CreateApplicationConsoleLogTarget()
        {
            LogFactory.Initialize();
            ValidateLogFactory();
            Debug.Assert(null != ApplicationLog, "ApplicationLog is not initialised!  Internal error!");
            LogFactory.ApplicationConsoleLog = new ThreadedConsoleLog();
            lock (m_logLock)
            {
                LogFactory.ApplicationConsoleLog.Connect(Log.StaticLog, CurrentLogLevel);
                foreach (ILog log in LogFactory.Logs)
                    LogFactory.ApplicationConsoleLog.Connect(log, CurrentLogLevel);
            }
            LogFactory.m_logTargets.Add(LogFactory.ApplicationConsoleLog);
        }

        /// <summary>
        /// Destroy a Universal Log.
        /// </summary>
        /// <param name="log"></param>
        public static void CloseUniversalLog(IUniversalLog log)
        {
            ValidateLogFactory();
            Debug.Assert(LogFactory.Logs.Contains(log), "Log object not managed by LogFactory!");

            // Loop through log targets unregistering it.
            foreach (ILogTarget logTarget in LogFactory.LogTargets)
            {
                if (logTarget.IsConnected(log))
                    logTarget.Disconnect(log);
            }
            if (null != LogFactory.ApplicationLog &&
                LogFactory.ApplicationLog.IsConnected(log))
            {
                LogFactory.ApplicationLog.Disconnect(log);
            }
            lock (m_logLock)
            {
                if (LogFactory.Logs.Contains(log))
                    LogFactory.m_logs.Remove(log);
            }
        }

        /// <summary>
        /// Destroy a Universal Log File.
        /// </summary>
        /// <param name="log"></param>
        public static void CloseUniversalLogFile(ILogTarget log)        
        {
            ValidateLogFactory();
            Debug.Assert(LogFactory.LogTargets.Contains(log),
                "LogTarget object not managed by LogFactory!");

            if (LogFactory.LogTargets.Contains(log))
                LogFactory.m_logTargets.Remove(log);
            log.DisconnectAll();

            if (log is ILogFileTarget)
                (log as ILogFileTarget).Dispose();
        }

        /// <summary>
        /// Application shutdown; clears connected logs and flushes any
        /// log file targets registered in the system.
        /// </summary>
        /// <param name="mode"></param>
        /// Optionally displays the Universal Log Viewer application.
        /// 
        public static void ApplicationShutdown(ShutdownMode mode = ShutdownMode.NoLogDisplay)
        {
            // Shutdown all threaded-logs.
            foreach (ILogTarget target in LogFactory.LogTargets)
                target.Shutdown();

            // Close all log files.
            while (LogFactory.LogTargets.Any())
                CloseUniversalLogFile(LogFactory.LogTargets.First());
            // Close all log objects.
            lock (m_logLock)
            {
                while (LogFactory.Logs.Any())
                    CloseUniversalLog(LogFactory.Logs.First());
            }
            
            // Handle specified shutdown mode.
            switch (mode)
            {
                case ShutdownMode.DisplayOnError:
                    if (ApplicationLog.HasErrors)
                        ShowUniversalLogViewer(ProcessLogDirectory);
                    break;
                case ShutdownMode.DisplayAlways:
                    ShowUniversalLogViewer(ProcessLogDirectory);
                    break;
            }
            LogFactory.Initialised = false;
        }

        /// <summary>
        /// Display the Universal Log Viewer; opening a specified log file or
        /// directory.
        /// </summary>
        /// <param name="logFilenameOrDirectory"></param>
        /// <returns></returns>
        /// 
        public static Process ShowUniversalLogViewer(String logFilenameOrDirectory)
        {
            String toolsRoot = Environment.GetEnvironmentVariable("RS_TOOLSROOT");
            String ulogViewer = Path.Combine(toolsRoot, LOG_VIEWER);

            if (String.IsNullOrEmpty(logFilenameOrDirectory))
                logFilenameOrDirectory = ProcessLogDirectory;

            return (Process.Start(ulogViewer, logFilenameOrDirectory));
        }

        /// <summary>
        /// Generate a universal log filename using a unique prefix.
        /// </summary>
        /// <param name="prefix"></param>
        /// <returns></returns>
        public static String GenerateUniversalLogFileName(String prefix, params String[] paths)
        {
            String concatPaths = System.IO.Path.Combine(paths);
            String filename = String.Format("{0}.{1}", prefix, ThreadedUniversalLogFile.EXTENSION);
            if (concatPaths.Length > 0)
                filename = System.IO.Path.Combine(
                    ProcessLogDirectory, concatPaths, filename);
            else
                filename = System.IO.Path.Combine(
                    ProcessLogDirectory, filename);
            return (filename);
        }

        /// <summary>
        /// Closes all the current log files opened with the log factory, clears the log directory
        /// and then recreate the main application log. Used for Max so on each export we start with a fresh log directory.
        /// </summary>
        public static void ResetLogDirectory(bool clearProcessDirectory = false)
        {
            // DHM - disable the log disconnect as this causes havok with
            // all of our in-process Log objects (e.g. static Log objects
            // created in static constructors).
            // When the application log is re-created we re-attach to all
            // of our known about logs.
#if false
            // Close all log files.
            while (LogFactory.LogTargets.Count > 0)
                CloseUniversalLogFile(LogFactory.LogTargets.First());
            // Close all log objects.
            while (LogFactory.Logs.Count > 0)
                CloseUniversalLog(LogFactory.Logs.First());
#endif

            ApplicationLogFile.DisconnectAll();
            if (ApplicationLogFile is ILogFileTarget)
                (ApplicationLogFile as ILogFileTarget).Dispose();
            ApplicationLogFile = null;

            ClearLogDirectory(clearProcessDirectory);
            CreateProcessLog();
        }

        /// <summary>
        /// Force a flush of the application log.
        /// </summary>
        public static void FlushApplicationLog()
        {
            if (null != ApplicationLogFile)
                (ApplicationLogFile as ILogFileTarget).Flush();
        }

        /// <summary>
        /// Clear log directory files.
        /// </summary>
        public static void ClearLogDirectory(bool clearProcessDirectory = false)
        {
            String rootLogDir = GetLogDirectory();
            try
            {
                if (clearProcessDirectory)
                {

					TryDeleteFilesRecursive(ParentProcessLogDirectory, "*.ulog");
            		TryDeleteEmptySubDirectories(ParentProcessLogDirectory);
                }
                else
                {
                    TryDeleteFilesRecursive(rootLogDir, "*.ulog");
            		TryDeleteEmptySubDirectories(rootLogDir);
                }
            }
            catch
            {
            }
        }

        /// <summary>
        /// Checks if any of the logs has errors.
        /// </summary>
        /// <returns></returns>
        public static bool HasError()
        {
            return Log.StaticLog.HasErrors || Logs.Any(l => l.HasErrors);
        }

        /// <summary>
        /// Checks if any of the logs has warnings.
        /// </summary>
        /// <returns></returns>
        public static bool HasWarning()
        {
            return Log.StaticLog.HasWarnings || Logs.Any(l => l.HasWarnings);
        }

        /// <summary>
        /// Return log directory.
        /// </summary>
        /// <returns></returns>
        public static String GetLogDirectory()
        {
            String path = System.IO.Path.Combine(
                    System.Environment.ExpandEnvironmentVariables("%RS_TOOLSROOT%"),
                    LOG_DIRECTORY);
            return (path);
        }
        #endregion // Static Controller Methods

        #region Private Methods
        /// <summary>
        /// Initialise a process log and log file.
        /// </summary>
        private static void CreateProcessLog(string logSubdirectory = null, bool checkForParentProcess = true)
        {
            ValidateLogFactory();

            Process process = Process.GetCurrentProcess();
            String processName = process.ProcessName;

            // Resolve the top-most parent process to store our logs.
            String parentProcessName = "";
            int parentID = -1;

            // Use the specified logSubdirectory over the parent process directory.
            if (checkForParentProcess && string.IsNullOrEmpty(logSubdirectory))
            {
                Process topMostParent = ProcessExtensions.TopMostParent(process);

                if (topMostParent != null)
                {
                    parentProcessName = topMostParent.ProcessName;
                    parentID = topMostParent.Id;
                }
            }

            String logName = String.Format("{0}.{1}", processName, process.Id);
            String rootLogDir = GetLogDirectory();

            if (checkForParentProcess && parentID != -1)
            {
                String parentLogDir = String.Format("{0}.{1}", parentProcessName, parentID);
                ProcessLogDirectory = Path.Combine(rootLogDir, parentLogDir, logName);
                ParentProcessLogDirectory = Path.Combine(rootLogDir, parentLogDir);
            }
            else
            {
                if (string.IsNullOrEmpty(logSubdirectory))
                {
                    ProcessLogDirectory = Path.Combine(rootLogDir, logName);
                    ParentProcessLogDirectory = Path.Combine(rootLogDir, logName);
                }
                else
                {
                    ProcessLogDirectory = Path.Combine(logSubdirectory, logName);
                    ParentProcessLogDirectory = logSubdirectory;
                }
            }

            String appLogFile = Path.Combine(ParentProcessLogDirectory, String.Format("{0}.ulog", logName));
            ApplicationLog = CreateUniversalLog(logName);
            CreateApplicationUniversalLogFile(appLogFile);

            // Connect our application universal log file to the obsolete StaticLog.
            LogFactory.m_logs.Add(Log.StaticLog);

            // Connect our application universal log file to all of our
            // in-process log objects.
            lock (m_logLock)
            {
                foreach (IUniversalLog log in LogFactory.Logs)
                {
                    // Our application log is already connected because of the call
                    // to 'CreateApplicationUniversalLogFile' above; we protect 
                    // against another connect as it raises a useful debug assertion.
                    if (!LogFactory.ApplicationLog.Equals(log))
                        LogFactory.ApplicationLogFile.Connect(log);
                }
            }
        }

        /// <summary>
        /// Create an application-wide UniversalLogFile; connecting all system
        /// logs to use it as a target.  All subsequent logs are connected too.
        /// </summary>
        private static void CreateApplicationUniversalLogFile(String filename)
        {
            ValidateLogFactory();

            Debug.Assert(null != ApplicationLog, "ApplicationLog is not initialised!  Internal error!");

            LogFactory.ApplicationLogFile = new ThreadedUniversalLogFile(filename, FileMode.CreateNew,
                FlushMode.Error, ApplicationLog);
            LogFactory.m_logTargets.Add(LogFactory.ApplicationLogFile);
        }

        /// <summary>
        /// Tries to delete any files below a given directory that match the given search pattern
        /// </summary>
        private static void TryDeleteFilesRecursive(String directory, String searchPattern)
        {
            foreach (String pathname in Directory.EnumerateFiles(directory, searchPattern))
            {
                try
                {
                    File.Delete(pathname);
                }
                catch { }// we swallow exceptions here (hence the Try prefix in the method name)
            }

            // recurse
            foreach (String subDirectory in Directory.EnumerateDirectories(directory))
            {
                TryDeleteFilesRecursive(subDirectory, searchPattern);
            }
        }

        /// <summary>
        /// Tries to delete any empty subdirectories below a given directory
        /// </summary>
        /// <param name="directory"></param>
        private static void TryDeleteEmptySubDirectories(String directory)
        {
            String[] subDirectories = Directory.GetDirectories(directory);
            String[] subDirectoriesWithNoFiles = subDirectories.Where(
                subDirectory => Directory.GetFiles(subDirectory, "*.*", SearchOption.AllDirectories).Length == 0).ToArray();
            foreach (String subDirectoryWithNoFiles in subDirectoriesWithNoFiles)
            {
                try
                {
                    Directory.Delete(subDirectoryWithNoFiles, true);
                }
                catch { }// we swallow exceptions here (hence the Try prefix in the method name)
            }
        }
        #endregion // Private Methods
    }

} // RSG.Base.Logging namespace
