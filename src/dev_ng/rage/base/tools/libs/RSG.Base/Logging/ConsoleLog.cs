//---------------------------------------------------------------------------------------------
// <copyright file="ConsoleLog.cs" company="Rockstar Games">
//     Copyright � Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Base.Logging
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Text;
    using RSG.Base.Collections;
    using RSG.Base.Extensions;

    /// <summary>
    /// Console logging class; with some pretty printing colours for the 
    /// different message classifications.
    /// </summary>
    public class ConsoleLog : 
        LogTargetBase,
        ILogTarget
    {
        #region Constants
        /// <summary>
        /// Message prefix strings.
        /// </summary>
        private readonly IDictionary<LogLevel, String> MessagePrefixes = new Dictionary<LogLevel, String>()
        {
            {LogLevel.ToolException, "tool exception"},
            {LogLevel.ToolError, "tool error"},
            {LogLevel.Error, "error"},
            {LogLevel.Warning, "warn"},
            {LogLevel.Debug, "debug"},
            {LogLevel.Profile, "prof"},
            {LogLevel.Info, "info"},
        };

        /// <summary>
        /// Message colours.
        /// </summary>
        private readonly IDictionary<LogLevel, ConsoleColor> MessageColours = new Dictionary<LogLevel, ConsoleColor>()
        {
            {LogLevel.ToolException, ConsoleColor.Red},
            {LogLevel.ToolError, ConsoleColor.Red},
            {LogLevel.Error, ConsoleColor.Red},
            {LogLevel.Warning, ConsoleColor.Yellow},
            {LogLevel.Debug, ConsoleColor.Blue},
            {LogLevel.Profile, ConsoleColor.Cyan},
            {LogLevel.Info, ConsoleColor.Green},
        };
        #endregion // Constants

        #region Member Data
        /// <summary>
        /// Stack of profile contexts and stopwatches.
        /// </summary>
        private Stack<Pair<String, Stopwatch>> _contexts;

        /// <summary>
        /// Maximum character padding; for pretty printing.
        /// </summary>
        private int _maxPadding = 6;
        #endregion // Member Data

        #region Static Member Data
        /// <summary>
        /// Console resource lock (static as process-wide).
        /// </summary>
        private static Object _lock;
        #endregion // Static Member Data

        #region Constructor
        /// <summary>
        /// Default constructor; connecting to static log.
        /// </summary>
        /// Connect our static Log event to us.
        public ConsoleLog()
        {
            this.Enabled = true;
            this.Timestamp = true;
            this.ShowContext = false;
            this._contexts = new Stack<Pair<string, Stopwatch>>();
        }

        /// <summary>
        /// Static constructor (create resource lock object).
        /// </summary>
        static ConsoleLog()
        {
            ConsoleLog._lock = new Object();
        }
        #endregion // Constructor

        #region Controller Methods
        /// <summary>
        /// Connect to a log object.
        /// </summary>
        /// <param name="log"></param>
        public override void Connect(ILog log)
        {
            log.LogMessage += this.LogEventMessage;
            log.LogError += this.LogEventMessage;
            log.LogToolError += this.LogEventMessage;
            log.LogToolException += this.LogEventMessage;
            log.LogWarning += this.LogEventMessage;
            log.LogProfile += this.LogEventMessage;
            log.LogProfileEnd += this.LogProfileEnd;
            log.LogDebug += this.LogEventMessage;

            base.Connect(log);
        }

        /// <summary>
        /// Connect to a log object (up to a particular level).
        /// </summary>
        /// <param name="log"></param>
        /// <param name="uptoLevel"></param>
        public override void Connect(ILog log, LogLevel uptoLevel)
        {
            if ((int)uptoLevel >= (int)LogLevel.ToolException)
                log.LogToolException += this.LogEventMessage;
            if ((int)uptoLevel >= (int)LogLevel.ToolError)
                log.LogToolError += this.LogEventMessage;
            if ((int)uptoLevel >= (int)LogLevel.Error)
                log.LogError += this.LogEventMessage;
            if ((int)uptoLevel >= (int)LogLevel.Warning)
                log.LogWarning += this.LogEventMessage;
            if ((int)uptoLevel >= (int)LogLevel.Info)
                log.LogMessage += this.LogEventMessage;
            if ((int)uptoLevel >= (int)LogLevel.Profile)
            {
                log.LogProfile += this.LogEventMessage;
                log.LogProfileEnd += this.LogProfileEnd;
            }
            if ((int)uptoLevel >= (int)LogLevel.Debug)
                log.LogDebug += this.LogEventMessage;

            base.Connect(log, uptoLevel);
        }

        /// <summary>
        /// Disconnect from a log object.
        /// </summary>
        /// <param name="log"></param>
        public override void Disconnect(ILog log)
        {
            log.LogMessage -= this.LogEventMessage;
            log.LogError -= this.LogEventMessage;
            log.LogToolError -= this.LogEventMessage;
            log.LogToolException -= this.LogEventMessage;
            log.LogWarning -= this.LogEventMessage;
            log.LogProfile -= this.LogEventMessage;
            log.LogProfileEnd -= this.LogProfileEnd;
            log.LogDebug -= this.LogEventMessage;

            base.Disconnect(log);
        }

        /// <summary>
        /// Empty flush as it doesn't need to be flushed to disk.
        /// </summary>
        public override void Flush()
        {
            
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Log message event handler
        /// </summary>
        private void LogEventMessage(Object sender, LogMessageEventArgs e)
        {
            if (!this.Enabled)
                return;

            TextWriter writer = (e.Level <= LogLevel.Error) ? Console.Error : Console.Out;
            lock (ConsoleLog._lock)
            {
                if (this.Timestamp)
                    writer.Write(DateTime.Now.ToString() + " ");

                WritePrefix(writer, MessageColours[e.Level], MessagePrefixes[e.Level], e.Context);
                writer.WriteLine(e.Message);

                if (LogLevel.Profile == e.Level)
                {
                    Pair<String, Stopwatch> p = new Pair<String, Stopwatch>(e.Message, new Stopwatch());
                    p.Second.Start();
                    this._contexts.Push(p);
                }
            }
        }
        
        /// <summary>
        /// Profile end marker event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LogProfileEnd(Object sender, LogMessageEventArgs e)
        {
            if (!this.Enabled)
                return;
            
            lock (ConsoleLog._lock)
            {
                Pair<String, Stopwatch> p = this._contexts.Pop();
                p.Second.Stop();

                if (this.Timestamp)
                    Console.Write(DateTime.Now.ToString() + " ");
                WritePrefix(Console.Out, MessageColours[LogLevel.Profile], MessagePrefixes[LogLevel.Profile], p.First);
                Console.WriteLine("'{0}' took: {1}", p.First, p.Second.Elapsed.ToHumanReadableString());
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="col"></param>
        /// <param name="prefix"></param>
        /// <param name="context"></param>
        private void WritePrefix(TextWriter writer, ConsoleColor col, String prefix, String context)
        {
            // Prefixes line up vertically much nicer if we pad left with spaces.
            // Make log reading much easier on the eyes.
            String paddedBracket = "[".PadLeft(System.Math.Max(_maxPadding - prefix.Length,0));
            writer.Write(paddedBracket);

            Console.ForegroundColor = col;
            writer.Write(prefix);
            Console.ResetColor();
            writer.Write("] ");

            if (ShowContext)
            {
                writer.Write("[");
                Console.ForegroundColor = ConsoleColor.DarkGray;
                writer.Write(context);
                Console.ResetColor();
                writer.Write("] ");
            }
        }
        #endregion // Private Methods
    }

} // RSG.Base.Logging namespace
