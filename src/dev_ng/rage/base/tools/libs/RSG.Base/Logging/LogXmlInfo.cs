﻿using System;
using System.Xml;
using System.Xml.Linq;

namespace RSG.Base.Logging
{

    /// <summary>
    /// Logging extensions for XML parsing.
    /// </summary>
    public static class LogXmlInfo
    {
        #region XmlReader Logging Methods
        /// <summary>
        /// Log an XML error; with filename, line and column information.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="reader"></param>
        /// <param name="message"></param>
        /// <param name="args"></param>
        public static void Error(this ILog log, XmlReader reader, String message, params Object[] args)
        {
            ErrorCtx(log, String.Empty, reader, message, args);
        }

        /// <summary>
        /// Log an XML error; with filename, context, line and column information.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="context"></param>
        /// <param name="reader"></param>
        /// <param name="message"></param>
        /// <param name="args"></param>
        public static void ErrorCtx(this ILog log, String context, XmlReader reader, String message, params Object[] args)
        {
            String formattedMessage = String.Format(message, args);
            String filename = reader.BaseURI;
            IXmlLineInfo xmlInfo = (reader as IXmlLineInfo);
            if (null != xmlInfo)
            {
                log.ErrorCtx(context, "{0}:({1},{2}) {3}", filename,
                    xmlInfo.LineNumber, xmlInfo.LinePosition, formattedMessage);
            }
            else
            {
                log.ErrorCtx(context, "{0}: {1}", filename, formattedMessage);
            }
        }

        /// <summary>
        /// Log an XML warning; with filename, line and column information.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="reader"></param>
        /// <param name="message"></param>
        /// <param name="args"></param>
        public static void Warning(this ILog log, XmlReader reader, String message, params Object[] args)
        {
            WarningCtx(log, String.Empty, reader, message, args);
        }

        /// <summary>
        /// Log an XML warning; with filename, context, line and column information.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="context"></param>
        /// <param name="reader"></param>
        /// <param name="message"></param>
        /// <param name="args"></param>
        public static void WarningCtx(this ILog log, String context, XmlReader reader, String message, params Object[] args)
        {
            String formattedMessage = String.Format(message, args);
            String filename = reader.BaseURI;
            IXmlLineInfo xmlInfo = (reader as IXmlLineInfo);
            if (null != xmlInfo)
            {
                log.WarningCtx(context, "{0}:({1},{2}) {3}", filename,
                    xmlInfo.LineNumber, xmlInfo.LinePosition, formattedMessage);
            }
            else
            {
                log.WarningCtx(context, "{0}: {1}", filename, formattedMessage);
            }
        }

        /// <summary>
        /// Log an XML message; with filename, line and column information.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="reader"></param>
        /// <param name="message"></param>
        /// <param name="args"></param>
        public static void Message(this ILog log, XmlReader reader, String message, params Object[] args)
        {
            MessageCtx(log, String.Empty, reader, message, args);
        }

        /// <summary>
        /// Log an XML message; with filename, context, line and column information.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="context"></param>
        /// <param name="reader"></param>
        /// <param name="message"></param>
        /// <param name="args"></param>
        public static void MessageCtx(this ILog log, String context, XmlReader reader, String message, params Object[] args)
        {
            String formattedMessage = String.Format(message, args);
            String filename = reader.BaseURI;
            IXmlLineInfo xmlInfo = (reader as IXmlLineInfo);
            if (null != xmlInfo)
            {
                log.MessageCtx(context, "{0}:({1},{2}) {3}", filename,
                    xmlInfo.LineNumber, xmlInfo.LinePosition, formattedMessage);
            }
            else
            {
                log.MessageCtx(context, "{0}: {1}", filename, formattedMessage);
            }
        }

        /// <summary>
        /// Log an XML debug message; with filename, line and column information.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="reader"></param>
        /// <param name="message"></param>
        /// <param name="args"></param>
        public static void Debug(this ILog log, XmlReader reader, String message, params Object[] args)
        {
            DebugCtx(log, String.Empty, reader, message, args);
        }

        /// <summary>
        /// Log an XML debug message; with filename, context, line and column information.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="context"></param>
        /// <param name="reader"></param>
        /// <param name="message"></param>
        /// <param name="args"></param>
        public static void DebugCtx(this ILog log, String context, XmlReader reader, String message, params Object[] args)
        {
            String formattedMessage = String.Format(message, args);
            String filename = reader.BaseURI;
            IXmlLineInfo xmlInfo = (reader as IXmlLineInfo);
            if (null != xmlInfo)
            {
                log.DebugCtx(context, "{0}:({1},{2}) {3}", filename,
                    xmlInfo.LineNumber, xmlInfo.LinePosition, formattedMessage);
            }
            else
            {
                log.DebugCtx(context, "{0}: {1}", filename, formattedMessage);
            }
        }
        #endregion // XmlReader Logging Methods
        
        #region XmlDocument Logging Methods
        /// <summary>
        /// Log an XML error; with filename, line and column information.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="xmlNode"></param>
        /// <param name="message"></param>
        /// <param name="args"></param>
        public static void Error(this ILog log, XmlNode xmlNode, String message, params Object[] args)
        {
            String formattedMessage = String.Format(message, args);
            String filename = xmlNode.BaseURI;
            IXmlLineInfo xmlInfo = (xmlNode as IXmlLineInfo);
            if (null != xmlInfo)
            {
                log.Error("{0}:({1},{2}) {3}", filename,
                    xmlInfo.LineNumber, xmlInfo.LinePosition, formattedMessage);
            }
            else
            {
                log.Error("{0}: {1}", filename, formattedMessage);
            }
        }

        /// <summary>
        /// Log an XML warning; with filename, line and column information.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="xmlNode"></param>
        /// <param name="message"></param>
        /// <param name="args"></param>
        public static void Warning(this ILog log, XmlNode xmlNode, String message, params Object[] args)
        {
            String formattedMessage = String.Format(message, args);
            String filename = xmlNode.BaseURI;
            IXmlLineInfo xmlInfo = (xmlNode as IXmlLineInfo);
            if (null != xmlInfo)
            {
                log.Warning("{0}:({1},{2}) {3}", filename,
                    xmlInfo.LineNumber, xmlInfo.LinePosition, formattedMessage);
            }
            else
            {
                log.Warning("{0}: {1}", filename, formattedMessage);
            }
        }

        /// <summary>
        /// Log an XML message; with filename, line and column information.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="xmlNode"></param>
        /// <param name="message"></param>
        /// <param name="args"></param>
        public static void Message(this ILog log, XmlNode xmlNode, String message, params Object[] args)
        {
            String formattedMessage = String.Format(message, args);
            String filename = xmlNode.BaseURI;
            IXmlLineInfo xmlInfo = (xmlNode as IXmlLineInfo);
            if (null != xmlInfo)
            {
                log.Message("{0}:({1},{2}) {3}", filename,
                    xmlInfo.LineNumber, xmlInfo.LinePosition, formattedMessage);
            }
            else
            {
                log.Message("{0}: {1}", filename, formattedMessage);
            }
        }

        /// <summary>
        /// Log an XML debug message; with filename, line and column information.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="xmlNode"></param>
        /// <param name="message"></param>
        /// <param name="args"></param>
        public static void Debug(this ILog log, XmlNode xmlNode, String message, params Object[] args)
        {
#if DEBUG
            String formattedMessage = String.Format(message, args);
            String filename = xmlNode.BaseURI;
            IXmlLineInfo xmlInfo = (xmlNode as IXmlLineInfo);
            if (null != xmlInfo)
            {
                log.Debug("{0}:({1},{2}) {3}", filename,
                    xmlInfo.LineNumber, xmlInfo.LinePosition, formattedMessage);
            }
            else
            {
                log.Debug("{0}: {1}", filename, formattedMessage);
            }
#endif // DEBUG
        }
        #endregion // XmlReader Logging Methods

        #region XDocument Logging Methods
        /// <summary>
        /// Log an XML error; with filename, line and column information.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="xmlObject"></param>
        /// <param name="message"></param>
        /// <param name="args"></param>
        public static void Error(this ILog log, XObject xmlObject, String message, params Object[] args)
        {
            String formattedMessage = String.Format(message, args);
            String filename = xmlObject.BaseUri;
            IXmlLineInfo xmlInfo = (xmlObject as IXmlLineInfo);
            if (null != xmlInfo)
            {
                log.Error("{0}:({1},{2}) {3}", filename,
                    xmlInfo.LineNumber, xmlInfo.LinePosition, formattedMessage);
            }
            else
            {
                log.Error("{0}: {1}", filename, formattedMessage);
            }
        }

        /// <summary>
        /// Log an XML warning; with filename, line and column information.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="xmlObject"></param>
        /// <param name="message"></param>
        /// <param name="args"></param>
        public static void Warning(this ILog log, XObject xmlObject, String message, params Object[] args)
        {
            String formattedMessage = String.Format(message, args);
            String filename = xmlObject.BaseUri;
            IXmlLineInfo xmlInfo = (xmlObject as IXmlLineInfo);
            if (null != xmlInfo)
            {
                log.Warning("{0}:({1},{2}) {3}", filename,
                    xmlInfo.LineNumber, xmlInfo.LinePosition, formattedMessage);
            }
            else
            {
                log.Warning("{0}: {1}", filename, formattedMessage);
            }
        }

        /// <summary>
        /// Log an XML message; with filename, line and column information.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="xmlObject"></param>
        /// <param name="message"></param>
        /// <param name="args"></param>
        public static void Message(this ILog log, XObject xmlObject, String message, params Object[] args)
        {
            String formattedMessage = String.Format(message, args);
            String filename = xmlObject.BaseUri;
            IXmlLineInfo xmlInfo = (xmlObject as IXmlLineInfo);
            if (null != xmlInfo)
            {
                log.Message("{0}:({1},{2}) {3}", filename,
                    xmlInfo.LineNumber, xmlInfo.LinePosition, formattedMessage);
            }
            else
            {
                log.Message("{0}: {1}", filename, formattedMessage);
            }
        }

        /// <summary>
        /// Log an XML debug message; with filename, line and column information.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="xmlObject"></param>
        /// <param name="message"></param>
        /// <param name="args"></param>
        public static void Debug(this ILog log, XObject xmlObject, String message, params Object[] args)
        {
#if DEBUG
            String formattedMessage = String.Format(message, args);
            String filename = xmlObject.BaseUri;
            IXmlLineInfo xmlInfo = (xmlObject as IXmlLineInfo);
            if (null != xmlInfo)
            {
                log.Debug("{0}:({1},{2}) {3}", filename,
                    xmlInfo.LineNumber, xmlInfo.LinePosition, formattedMessage);
            }
            else
            {
                log.Debug("{0}: {1}", filename, formattedMessage);
            }
#endif // DEBUG
        }
        #endregion // XmlReader Logging Methods
    }

} // RSG.Base.Logging namespace
