using System;
using System.Diagnostics;

namespace RSG.Base.Logging
{
    
    /// <summary>
    /// Log interface; a class that implements this is a log.  It can have 
    /// various classes of messages sent to it and events are raised for
    /// ILogTarget objects to listen to.
    /// </summary>
    public interface ILog
    {
        #region Events
        /// <summary>
        /// Event type for regular message log events.
        /// </summary>
        event EventHandler<LogMessageEventArgs> LogMessage;

        /// <summary>
        /// Event type for error message log events.
        /// </summary>
        event EventHandler<LogMessageEventArgs> LogError;

        /// <summary>
        /// Event type for tool internal-error log events.
        /// </summary>
        event EventHandler<LogMessageEventArgs> LogToolError;

        /// <summary>
        /// Event type for tool internal-exception log events.
        /// </summary>
        event EventHandler<LogMessageEventArgs> LogToolException;

        /// <summary>
        /// Event type for warning message log events.
        /// </summary>
        event EventHandler<LogMessageEventArgs> LogWarning;

        /// <summary>
        /// Event type for profile message log events; profile start marker.
        /// </summary>
        event EventHandler<LogMessageEventArgs> LogProfile;

        /// <summary>
        /// Event type for profiling end.
        /// </summary>
        event EventHandler<LogMessageEventArgs> LogProfileEnd;

        /// <summary>
        /// Event type for debug message log events.
        /// </summary>
        event EventHandler<LogMessageEventArgs> LogDebug;
        #endregion // Events

        #region Properties
        /// <summary>
        /// Log name (context).
        /// </summary>
        String Name { get; set; }

        /// <summary>
        /// Log enabled state; whether messages are handled.
        /// </summary>
        bool Enabled { get; set; }

        /// <summary>
        /// Internally set when errors have been logged.
        /// </summary>
        bool HasErrors { get; }

        /// <summary>
        /// Internally set when warnings have been logged.
        /// </summary>
        bool HasWarnings { get; }

        /// <summary>
        /// Internally set when profile message have been logged.
        /// </summary>
        bool HasProfiling { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Log an information message.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="args"></param>
        /// Log targets may treat this differently to the other categories of
        /// messages.
        void Message(String message, params Object[] args);
        void MessageCtx(String context, String message, params Object[] args);

        /// <summary>
        /// Log an error message.
        /// </summary>
        /// <param name="message"></param>
        /// Log targets may treat this differently to the other categories of
        /// messages.  E.g. Prefixing message with "Error:" or highlighting the
        /// text.
        void Error(String message, params Object[] args);
        void ErrorCtx(String context, String message, params Object[] args);


        /// <summary>
        /// Log an exception message.
        /// </summary>
        /// <param name="message"></param>
        /// Log targets may treat this differently to the other categories of
        /// messages.  E.g. Prefixing message with "Error:" or highlighting the
        /// text.
        [Obsolete("Use ToolError or ToolException instead.")]
        void Exception(Exception ex, String message, params Object[] args);
        [Obsolete("Use ToolErrorCtx or ToolExceptionCtx instead.")]
        void ExceptionCtx(String context, Exception ex, String message, params Object[] args);
        
        /// <summary>
        /// Log an tool internal-exception message.
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="context"></param>
        /// <param name="message"></param>
        /// <param name="args"></param>
        /// Log targets may treat this differently to the other categories of
        /// messages.  E.g. Prefixing message with "Error:" or highlighting the
        /// text.
        void ToolException(Exception ex, String message, params Object[] args);
        void ToolExceptionCtx(String context, Exception ex, String message, params Object[] args);

        /// <summary>
        /// Log a tool internal-error message.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="args"></param>
        void ToolError(String message, params Object[] args);
        void ToolErrorCtx(String context, String message, params Object[] args);

        /// <summary>
        /// Log a warning message.
        /// </summary>
        /// <param name="message"></param>
        /// Log targets may treat this differently to the other categories of
        /// messages.  E.g. Prefixing message with "Warning:" or highlighting 
        /// the text.
        void Warning(String message, params Object[] args);
        void WarningCtx(String context, String message, params Object[] args);

        /// <summary>
        /// Log a debug message (only in DEBUG builds).
        /// </summary>
        /// <param name="message"></param>
        /// Log targets may treat this differently to the other categories of
        /// messages.  E.g. Prefixing message with "Debug:" or highlighting the
        /// text.
        /// 
        /// The message is generally ignored by log targets when in release 
        /// builds.
        void Debug(String message, params Object[] args);
        void DebugCtx(String context, String message, params Object[] args);

        /// <summary>
        /// Log a profile message and start marker.
        /// </summary>
        /// <param name="message">Profile segment name (e.g. MapExport, Conversion)</param>
        void Profile(String message, params Object[] args);
        void ProfileCtx(String Context, String message, params Object[] args);

        /// <summary>
        /// Log a profile end marker message.
        /// </summary>
        void ProfileEnd();

        /// <summary>
        /// Connect to a log object.
        /// </summary>
        /// <param name="log"></param>
        void Connect(ILog log);

        /// <summary>
        /// Determine whether we are connected to a particular log.
        /// </summary>
        /// <param name="log"></param>
        /// <returns></returns>
        bool IsConnected(ILog log);

        /// <summary>
        /// Disconnect from a log object.
        /// </summary>
        void Disconnect(ILog log);

        /// <summary>
        /// Disconnect from all log objects (e.g. during shutdown).
        /// </summary>
        void DisconnectAll();
        #endregion // Methods
    }

} // End of RSG.Base.Logging namespace

// End of file
