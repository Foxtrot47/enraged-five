using System;

namespace RSG.Base.Logging
{

    /// <summary>
    /// Log target interface; a class that implements this is a log target or
    /// log message consumer.
    /// </summary>
    /// Examples of log targets are: files, UI grids, or any other "view" 
    /// component.
    /// 
    public interface ILogTarget
    {
        #region Properties
        /// <summary>
        /// Log enabled flag
        /// </summary>
        bool Enabled { get; set; }

        /// <summary>
        /// Timestamp log entries flag
        /// </summary>
        bool Timestamp { get; set; }

        /// <summary>
        /// Show context
        /// </summary>
        bool ShowContext { get; set; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Connect to a log object.
        /// </summary>
        /// <param name="log"></param>
        void Connect(ILog log);

        /// <summary>
        /// Connect to a log object (up to a particular level).
        /// </summary>
        /// <param name="log"></param>
        /// <param name="uptoLevel"></param>
        void Connect(ILog log, LogLevel uptoLevel);

        /// <summary>
        /// Determine whether we are connected to a particular log.
        /// </summary>
        /// <param name="log"></param>
        /// <returns></returns>
        bool IsConnected(ILog log);

        /// <summary>
        /// Disconnect from a log object.
        /// </summary>
        void Disconnect(ILog log);

        /// <summary>
        /// Disconnect from all log objects (e.g. during shutdown).
        /// </summary>
        void DisconnectAll();

        /// <summary>
        /// Reconnect to all logs objects (up to a particular level).
        /// </summary>
        /// <param name="uptoLevel"></param>
        void Reconnect(LogLevel uptoLevel);

        /// <summary>
        /// Flushes the current log to its target
        /// </summary>
        void Flush();

        /// <summary>
        /// Allows the log target to perform cleanup operations prior
        /// to application shutdown.
        /// </summary>
        void Shutdown();
        #endregion // Methods
    }

} // RSG.Base.Logging namespace
