﻿using System;

namespace RSG.Base.Logging.Universal
{

    /// <summary>
    /// Universal Log Target interface.
    /// </summary> 
    /// https://devstar.rockstargames.com/wiki/index.php/Dev:Universal_Log_Format
    /// 
    public interface IUniversalLogTarget : ILogTarget
    {
    }

} // RSG.Base.Logging.Universal namespace
