﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace RSG.Base.Logging
{

    /// <summary>
    /// Abstract base class for all LogTargets; this should be used to base
    /// all LogTargets from because of the connected log maintainance.
    /// </summary>
    public abstract class LogTargetBase : ILogTarget
    {
        #region Properties
        /// <summary>
        /// Log enabled flag
        /// </summary>
        public bool Enabled { get; set; }

        /// <summary>
        /// Timestamp log entries flag
        /// </summary>
        public bool Timestamp { get; set; }

        /// <summary>
        /// ShowContext.
        /// </summary>
        public bool ShowContext { get; set; }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Collection of connected log objects.
        /// </summary>
        protected ICollection<ILog> ConnectedLogs;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public LogTargetBase()
        {
            this.ConnectedLogs = new List<ILog>();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Connect to a log object.
        /// </summary>
        /// <param name="log"></param>
        public virtual void Connect(ILog log)
        {
            lock (this.ConnectedLogs)
            {
                //Debug.Assert(!this.ConnectedLogs.Contains(log),
                //    "Log already connected to this LogTarget!  Ignoring.");

                if (this.ConnectedLogs.Contains(log))
                    return;

                this.ConnectedLogs.Add(log);
            }
        }

        /// <summary>
        /// Connect to a log object (up to a particular level).
        /// </summary>
        /// <param name="log"></param>
        /// <param name="uptoLevel"></param>
        public virtual void Connect(ILog log, LogLevel uptoLevel)
        {
            lock (this.ConnectedLogs)
            {
                //Debug.Assert(!this.ConnectedLogs.Contains(log),
                //    "Log already connected to this LogTarget!  Ignoring.");
                if (this.ConnectedLogs.Contains(log))
                    return;

                this.ConnectedLogs.Add(log);
            }
        }
        
        /// <summary>
        /// Determine whether we are connected to a particular log.
        /// </summary>
        /// <param name="log"></param>
        /// <returns></returns>
        public virtual bool IsConnected(ILog log)
        {
            lock (this.ConnectedLogs)
                return (this.ConnectedLogs.Contains(log));
        }

        /// <summary>
        /// Disconnect from a log object.
        /// </summary>
        /// <param name="log"></param>
        public virtual void Disconnect(ILog log)
        {
            lock (this.ConnectedLogs)
            {
                Debug.Assert(this.ConnectedLogs.Contains(log),
                    "Log not connected to this LogTarget!  Ignoring.");
                if (!this.ConnectedLogs.Contains(log))
                    return;

                this.ConnectedLogs.Remove(log);
            }
        }

        /// <summary>
        /// Disconnect from all registered logs.
        /// </summary>
        public void DisconnectAll()
        {
            lock (this.ConnectedLogs)
            {
                while (this.ConnectedLogs.Count > 0)
                    Disconnect(this.ConnectedLogs.First());
            }
        }

        /// <summary>
        /// Reconnect to all logs objects (up to a particular level).
        /// </summary>
        /// <param name="uptoLevel"></param>
        public void Reconnect(LogLevel uptoLevel)
        {
            lock (this.ConnectedLogs)
            {
                ICollection<ILog> logs = new List<ILog>(this.ConnectedLogs);
                foreach (ILog log in logs)
                {
                    this.Disconnect(log);
                    this.Connect(log, uptoLevel);
                }
            }
        }

        /// <summary>
        /// Flushes the current log content out to the target
        /// </summary>
        public abstract void Flush();

        /// <summary>
        /// Allows the log target to perform cleanup operations prior
        /// to application shutdown.
        /// </summary>
        public void Shutdown()
        {
        }
        #endregion // Controller Methods
    }

} // RSG.Base.Logging namespace
