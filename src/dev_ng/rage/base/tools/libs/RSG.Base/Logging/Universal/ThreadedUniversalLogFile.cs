﻿//---------------------------------------------------------------------------------------------
// <copyright file="ThreadedUniversalLogFile.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Base.Logging.Universal
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Xml;
    using System.Xml.Linq;
    using System.Xml.XPath;
    using System.IO;
    using System.Globalization;
    using System.Text.RegularExpressions;
    using System.Windows.Forms;
    using RSG.Base.Collections;
    using RSG.Base.Extensions;

    /// <summary>
    /// Threaded Universal log file target.
    /// </summary>
    /// An instance of this object can be connected to an ILog instance by
    /// connecting up the Log method handlers.  When log messages are then
    /// generated they get written to the file.
    /// 
    /// https://devstar.rockstargames.com/wiki/index.php/Dev:Universal_Log_Format
    /// 
    public sealed class ThreadedUniversalLogFile :
        ThreadedLogTargetBase,
        IUniversalLogTarget,
        ILogFileTarget
    {
        #region Constants
        /// <summary>
        /// Default filename extension String for Universal Log Files.
        /// </summary>
        public static readonly String EXTENSION = "ulog";

        /// <summary>
        /// File size byte limit; before roll.
        /// </summary>
        private static readonly long FILE_SIZE_LIMIT = 2 * 1024 * 1024; // 2MB

        /// <summary>
        /// Number of messages to buffer before a flush
        /// </summary>
        private const int DEFAULT_MESSAGE_BUFFER_SIZE = 1000;

        /// <summary>
        /// Default Universal Log context string.
        /// </summary>
        public static readonly String DEFAULT_CONTEXT = "Default";

        /// <summary>
        /// Append millisecond pattern to current culture's full date time pattern
        /// </summary>
        private static readonly String fullTimePattern = Regex.Replace(DateTimeFormatInfo.CurrentInfo.FullDateTimePattern, "(:ss|:s)", "$1.fff");
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Log file flush mode.
        /// </summary>
        public FlushMode FlushMode
        {
            get;
            private set;
        }

        /// <summary>
        /// Log filename on disk.
        /// </summary>
        public String Filename
        {
            get;
            private set;
        }

        /// <summary>
        /// Number of messages to buffer prior to flush.
        /// </summary>
        public int BufferSize
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Internal message buffer.
        /// </summary>
        private ICollection<UniversalLogFileBufferedMessage> m_BufferedMessages;

        /// <summary>
        /// Rolling counter; to easily get new filename.
        /// </summary>
        private uint m_RollingCount;

        /// <summary>
        /// Profile message context stack.
        /// </summary>
        private Stack<String> m_ProfileContextStack;
        #endregion // Member Data

        #region Constructor(s) / Destructor
        /// <summary>
        /// Constructor; specifying flush mode and initial log connection.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="appendMode"></param>
        /// <param name="flushMode"></param>
        /// <param name="log"></param>
        /// <param name="bufferSize"></param>
        public ThreadedUniversalLogFile(String filename, FileMode appendMode,
            FlushMode flushMode, IUniversalLog log, int bufferSize = DEFAULT_MESSAGE_BUFFER_SIZE)
        {
            Init(filename, appendMode, flushMode, bufferSize);
            Connect(log);
        }

        /// <summary>
        /// Finaliser; flush the log file.
        /// </summary>
        ~ThreadedUniversalLogFile()
        {
            this.Flush();
        }
        #endregion // Constructor(s) / Destructor

        #region ILogFileTarget Interface Methods

        /// <summary>
        /// Flush the buffered messages to disk.
        /// </summary>
        public override void Flush()
        {
            this.Flush(false);
        }

        /// <summary>
        /// Dispose of the log file; flushing data to disk.
        /// </summary>
        public void Dispose()
        {
            this.Flush();
        }
        #endregion // ILogFileTarget Interface Methods

        #region ThreadedLogTargetBase Methods
        /// <summary>
        /// Invoked by worker thread to actually handle the messages.
        /// </summary>
        /// <param name="args"></param>
        protected override void LogMessage(LogMessageEventArgs args)
        {
            bool flush = false;
            String context = args.Context;
            switch (args.Level)
            {
                case LogLevel.ToolException:
                case LogLevel.ToolError:
                case LogLevel.Error:
                    flush = (this.FlushMode.HasFlag(FlushMode.Error));
                    break;
                case LogLevel.Warning:
                    flush = (this.FlushMode.HasFlag(FlushMode.Warning));
                    break;
                case LogLevel.Info:
                    flush = (this.FlushMode.HasFlag(FlushMode.Info));
                    break;
                case LogLevel.Debug:
                    flush = (this.FlushMode.HasFlag(FlushMode.Debug));
                    break;
                case LogLevel.Profile:
                    flush = (this.FlushMode.HasFlag(FlushMode.Profile));
                    lock (this.m_ProfileContextStack)
                    {
                        this.m_ProfileContextStack.Push(args.Context);
                    }
                    break;
                case LogLevel.ProfileEnd:
                    flush = (this.FlushMode.HasFlag(FlushMode.Profile));
                    lock (this.m_ProfileContextStack)
                    {
                        context = this.m_ProfileContextStack.Peek();
                    }
                    break;
            }

            lock (this.m_BufferedMessages)
            {
                UniversalLogFileBufferedMessage message = new UniversalLogFileBufferedMessage(
                    args.Level, context, args.Message, args.PreformattedMessage, args.Timestamp);
                this.m_BufferedMessages.Add(message);

                // Determine whether we need to flush the log; based on the buffered
                // message count or our specified flush mode.
                if (flush || (this.m_BufferedMessages.Count > this.BufferSize))
                    this.Flush();
            }
        }
        #endregion // ThreadedLogTargetBase Methods

        #region Private Methods
        /// <summary>
        /// Local initialisation method.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="appendMode"></param>
        /// <param name="flushMode"></param>
        /// <param name="bufferSize"></param>
        private void Init(String filename, FileMode appendMode, FlushMode flushMode,
            int bufferSize)
        {
            this.Filename = filename;
            this.FlushMode = flushMode;
            this.BufferSize = bufferSize;
            this.m_BufferedMessages = new List<UniversalLogFileBufferedMessage>(bufferSize);
            this.m_RollingCount = 0;
            this.m_ProfileContextStack = new Stack<String>();

            String logFileDirectory = Path.GetDirectoryName(filename);
            if (!Directory.Exists(logFileDirectory))
                Directory.CreateDirectory(logFileDirectory);

            switch (appendMode)
            {
                case FileMode.CreateNew:
                case FileMode.Create:
                    this.Flush(true);
                    break;
            }
        }

        /// <summary>
        /// Flush; optionally creating a base Universal Log file.
        /// </summary>
        /// <param name="create"></param>
        private void Flush(bool create)
        {
#if UNIVERSAL_LOG_FILE_FLUSH_DEBUG
			int bufferedCount = this.m_BufferedMessages.Count;
			Stopwatch sw = new Stopwatch();
			sw.Start();
#endif // UNIVERSAL_LOG_FILE_FLUSH_DEBUG

            // Locking around our Flush; ensures we're not adding/removing from the
            // buffered messages during a flush operation.
            lock (this.m_BufferedMessages)
            {
                XDocument xmlDoc = null;
                if (!create && File.Exists(this.Filename))
                {
                    try
                    {
                        // Try loading the document.
                        xmlDoc = XDocument.Load(this.Filename);
                    }
                    catch
                    {
                        try
                        {
                            // Try moving the invalid/corrupt file.  Ensure
                            // we catch... if that fails it will get overwritten
                            // in the next step.  Such as life.
                            String invalidFilename = Path.ChangeExtension(
                                this.Filename, "_CORRUPT.ulog");
                            File.Move(this.Filename, invalidFilename);
                        }
                        catch { }
                    }
                }

                xmlDoc = UniversalLogFile.TransformToXDocument(this.m_BufferedMessages, xmlDoc);

                // Found a case where the log directory gets blown away before flushing the file so re-create the directory.
                if (!Directory.Exists(Path.GetDirectoryName(this.Filename)))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(this.Filename));
                }

                try
                {
                    xmlDoc.Save(this.Filename);
                    this.m_BufferedMessages.Clear();

                    FileInfo fi = new FileInfo(this.Filename);
                    if (fi.Length >= FILE_SIZE_LIMIT)
                    {
                        String directory = Path.GetDirectoryName(this.Filename);
                        String filename = Path.GetFileName(this.Filename);
                        filename = Path.ChangeExtension(filename, String.Format("{0}.{1}",
                            this.m_RollingCount++, EXTENSION));
                        String archivedFilename = Path.Combine(directory, filename);
                        fi.MoveTo(archivedFilename);
                    }
                }
                catch
                {
                    // Catch the case of the file already being open.
                    // This was happening when the garbage collection was calling finalize which would flush the file
                    // but another flush was being called on another thread and causing a crash.
                    // This shouldn't clear the buffered messages so hopefully on the next flush it will properly get everything out.
                }
            } // lock(this.Filename)

#if UNIVERSAL_LOG_FILE_FLUSH_DEBUG
			sw.Stop();
			Console.WriteLine("UniversalLogFile::Flush() of {0} messages took {1}ms",
				bufferedCount, sw.ElapsedMilliseconds);
#endif // UNIVERSAL_LOG_FILE_FLUSH_DEBUG
        }
        #endregion // Private Methods

    } // UniversalLogFile

} // RSG.Base.Logging.Universal namespace
