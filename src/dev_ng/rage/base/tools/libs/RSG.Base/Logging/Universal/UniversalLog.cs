﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace RSG.Base.Logging.Universal
{

    /// <summary>
    /// 
    /// </summary>
    public class UniversalLog : Log, IUniversalLog
    {
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public UniversalLog()
        {
        }

        /// <summary>
        /// Constructor; naming the log.
        /// </summary>
        /// <param name="name"></param>
        public UniversalLog(String name)
            : this(name, true)
        {
        }

        /// <summary>
        /// Constructor; naming the log with default enabled state.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="enabled"></param>
        public UniversalLog(String name, bool enabled)
            : base(name, enabled)
        {
        }
        #endregion // Constructor(s)
    }

} // RSG.Base.Logging.Universal namespace
