using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using RSG.Base.IO;
using RSG.Base.Logging.Universal;

namespace RSG.Base.Logging
{

    /// <summary>
    /// Static Log object for applications to send log messages to
    /// anyone who has attached to the log message events.
    /// </summary>
    public class Log : ILog
    {
        #region Constants
        public static readonly String LOG_SOURCE = "Rockstar Games";

        private static readonly String DEFAULT_LOG_NAME = "Default";

        /// <summary>
        /// Default per-message context string.
        /// </summary>
        private static readonly String DEFAULT_MSG_CONTEXT = null;

        /// <summary>
        /// Array of strings that require escaping; and the escape string.
        /// </summary>
        private static readonly Tuple<String, String>[] ESCAPE_CHARACTERS =
            new Tuple<String, String>[] { 
                Tuple.Create("{", "{{"), 
                Tuple.Create("}", "}}") 
            };
        #endregion // Constants

        #region Events
        /// <summary>
        /// Event type for regular message log events.
        /// </summary>
        public event EventHandler<LogMessageEventArgs> LogMessage;
        
        /// <summary>
        /// Event type for error message log events.
        /// </summary>
        public event EventHandler<LogMessageEventArgs> LogError;

        /// <summary>
        /// Event type for tool internal-error log events.
        /// </summary>
        public event EventHandler<LogMessageEventArgs> LogToolError;

        /// <summary>
        /// Event type for tool internal-exception log events.
        /// </summary>
        public event EventHandler<LogMessageEventArgs> LogToolException;

        /// <summary>
        /// Event type for warning message log events.
        /// </summary>
        public event EventHandler<LogMessageEventArgs> LogWarning;

        /// <summary>
        /// Event type for profile message log events; profile start marker.
        /// </summary>
        public event EventHandler<LogMessageEventArgs> LogProfile;

        /// <summary>
        /// Event type for profiling end.
        /// </summary>
        public event EventHandler<LogMessageEventArgs> LogProfileEnd;

        /// <summary>
        /// Event type for debug message log events.
        /// </summary>
        public event EventHandler<LogMessageEventArgs> LogDebug;
        #endregion // Events

        #region Properties
        /// <summary>
        /// Log name (context).
        /// </summary>
        public String Name
        {
            get;
            set;
        }

        /// <summary>
        /// Log enabled state; whether messages are handled.
        /// </summary>
        public bool Enabled
        {
            get;
            set;
        }

        /// <summary>
        /// Set when errors have been logged.
        /// </summary>
        public bool HasErrors 
        { 
            get;
            protected set;
        }

        /// <summary>
        /// Set when warnings have been logged.
        /// </summary>
        public bool HasWarnings
        {
            get;
            protected set;
        }

        /// <summary>
        /// Set when profile message have been logged.
        /// </summary>
        public bool HasProfiling
        {
            get;
            protected set;
        }
        
        /// <summary>
        /// Static Log for static methods; really for compatibility with the
        /// old static logging methods.
        /// </summary>
        public static IUniversalLog StaticLog
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Instance resource lock.
        /// </summary>
        private Object m_Lock;

        /// <summary>
        /// Collection of connected log objects.
        /// </summary>
        protected ICollection<ILog> ConnectedLogs;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor; creating a 'Default' log.
        /// </summary>
        public Log()
            : this(DEFAULT_LOG_NAME)
        {
        }

        /// <summary>
        /// Constructor; creating a particularly named log.
        /// </summary>
        /// <param name="name"></param>
        public Log(String name)
            : this(name, true)
        {
        }

        /// <summary>
        /// Constructor; creating a particularly named log.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="enabled"></param>
        public Log(String name, bool enabled)
        {
            this.m_Lock = new Object();
            this.Name = name;
            this.Enabled = true;
            this.ConnectedLogs = new List<ILog>();
        }

        /// <summary>
        /// Static constructor; creating default log.
        /// </summary>
        static Log()
        {
            StaticLog = new UniversalLog();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Log an information message.
        /// </summary>
        /// <param name="message"></param>
        public virtual void Message(String message, params Object[] args)
        {
            this.MessageCtx(DEFAULT_MSG_CONTEXT, message, args);
        }

        /// <summary>
        /// Log an information message with context.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="message"></param>
        /// <param name="args"></param>
        public virtual void MessageCtx(String context, String message, params Object[] args)
        {
            if (!this.Enabled)
                return;

            if (null != LogMessage && !String.IsNullOrEmpty(message))
            {
                lock (this.m_Lock)
                {
                    String msg = FormatString(message, args);
                    LogMessage(this, new LogMessageEventArgs(LogLevel.Info, context, msg, message));
                }
            }
        }

        /// <summary>
        /// Log an exception message.
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="message"></param>
        /// <param name="args"></param>
        [Obsolete("Use ToolException instead.")]
        public virtual void Exception(Exception ex, String message, params Object[] args)
        {
            this.ToolExceptionCtx(DEFAULT_MSG_CONTEXT, ex, message, args);
        }

        /// <summary>
        /// Log an exception message with context.
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="message"></param>
        /// <param name="args"></param>
        /// The information logged is similar to what is displayed in the UI
        /// exception dialog class window.
        [Obsolete("Use ToolExceptionCtx instead.")]
        public virtual void ExceptionCtx(String context, Exception ex, String message, params Object[] args)
        {
            this.ToolExceptionCtx(context, ex, message, args);
        }

        /// <summary>
        /// Log a tool internal-exception message.
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="message"></param>
        /// <param name="args"></param>
        public virtual void ToolException(Exception ex, String message, params Object[] args)
        {
            this.ToolExceptionCtx(DEFAULT_MSG_CONTEXT, ex, message, args);
        }

        /// <summary>
        /// Log a tool internal-exception message with context.
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="message"></param>
        /// <param name="args"></param>
        /// The information logged is similar to what is displayed in the UI
        /// exception dialog class window.
        public virtual void ToolExceptionCtx(String context, Exception ex, String message, params Object[] args)
        {
            if (!this.Enabled)
                return;

            this.HasErrors = true;
            if (null != LogToolException && !String.IsNullOrEmpty(message))
            {
                lock (this.m_Lock)
                {
                    String msg = FormatString(message, args);
                    
                    IExceptionFormatter formatter = new ExceptionFormatter(ex);
                    List<String> messages = new List<String>();
                    messages.Add(msg);
                    messages.AddRange(formatter.Format());

                    LogToolException(this, new LogMessageEventArgs(LogLevel.ToolException, context, String.Join(Environment.NewLine, messages), message));

                    // Common inner exception support.
                    if (null != ex.InnerException)
                        this.ToolExceptionCtx(context, ex.InnerException, "Inner Exception");
                }
            }
        }

        /// <summary>
        /// Log an tool internal-error message.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="args"></param>
        public virtual void ToolError(String message, params Object[] args)
        {
            this.ToolErrorCtx(DEFAULT_MSG_CONTEXT, message, args);
        }

        /// <summary>
        /// Log an tool internal-error message with context.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="message"></param>
        /// <param name="args"></param>
        public virtual void ToolErrorCtx(String context, String message, params Object[] args)
        {
            if (!this.Enabled)
                return;

            this.HasErrors = true;
            if (null != LogToolError && !String.IsNullOrEmpty(message))
            {
                lock (this.m_Lock)
                {
                    String msg = FormatString(message, args);
                    LogToolError(this, new LogMessageEventArgs(LogLevel.ToolError, context, msg, message));
                }
            }
        }
        
        /// <summary>
        /// Log an error message.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="args"></param>
        public virtual void Error(String message, params Object[] args)
        {
            this.ErrorCtx(DEFAULT_MSG_CONTEXT, message, args);
        }

        /// <summary>
        /// Log an error message with context.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="message"></param>
        /// <param name="args"></param>
        public virtual void ErrorCtx(String context, String message, params Object[] args)
        {
            if (!this.Enabled)
                return;

            this.HasErrors = true;
            if (null != LogError && !String.IsNullOrEmpty(message))
            {
                lock (this.m_Lock)
                {
                    String msg = FormatString(message, args);
                    LogError(this, new LogMessageEventArgs(LogLevel.Error, context, msg, message));
                }
            }
        }

        /// <summary>
        /// Log a warning message.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="args"></param>
        public virtual void Warning(String message, params Object[] args)
        {
            this.WarningCtx(DEFAULT_MSG_CONTEXT, message, args);
        }

        /// <summary>
        /// Log a warning message with context.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="args"></param>
        public virtual void WarningCtx(String context, String message, params Object[] args)
        {
            if (!this.Enabled)
                return;

            this.HasWarnings = true;
            if (null != LogWarning && !String.IsNullOrEmpty(message))
            {
                lock (this.m_Lock)
                {
                    String msg = FormatString(message, args);
                    LogWarning(this, new LogMessageEventArgs(LogLevel.Warning, context, msg, message));
                }
            }
        }

        /// <summary>
        /// Log a debug message.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="args"></param>
        public virtual void Debug(String message, params Object[] args)
        {
            this.DebugCtx(DEFAULT_MSG_CONTEXT, message, args);
        }

        /// <summary>
        /// Log a debug message with context.
        /// </summary>
        /// <param name="message">Debug message string to log</param>
        public virtual void DebugCtx(String context, String message, params Object[] args)
        {
            if (!this.Enabled)
                return;

            if (null != LogDebug && !String.IsNullOrEmpty(message))
            {
                lock (this.m_Lock)
                {
                    String msg = FormatString(message, args);
                    LogDebug(this, new LogMessageEventArgs(LogLevel.Debug, context, msg, message));
                }
            }
        }

        /// <summary>
        /// Log a profile message and start marker.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="args"></param>
        public virtual void Profile(String message, params Object[] args)
        {
            this.ProfileCtx(DEFAULT_MSG_CONTEXT, message, args);
        }

        /// <summary>
        /// Log a profile message and start marker (with context).
        /// </summary>
        /// <param name="context"></param>
        /// <param name="message"></param>
        /// <param name="args"></param>
        public virtual void ProfileCtx(String context, String message, params Object[] args)
        {
            if (!this.Enabled)
                return;

            this.HasProfiling = true;
            if (null != LogProfile && !String.IsNullOrEmpty(message))
            {
                lock (this.m_Lock)
                {
                    String msg = FormatString(message, args);
                    LogProfile(this, new LogMessageEventArgs(LogLevel.Profile, context, msg, message));
                }
            }
        }

        /// <summary>
        /// Log a profile end marker.
        /// </summary>
        public virtual void ProfileEnd()
        {
            if (!this.Enabled)
                return;

            if (null != LogProfileEnd)
            {
                lock (this.m_Lock)
                {
                    LogProfileEnd(this, new LogMessageEventArgs(LogLevel.ProfileEnd, null, null, null));
                }
            }
        }

        /// <summary>
        /// Connect to a log object.
        /// </summary>
        /// <param name="log"></param>
        public void Connect(ILog log)
        {
			if (null == log)
				return;

			log.LogMessage += this.LogMessage;
			log.LogError += this.LogError;
            log.LogToolError += this.LogToolError;
            log.LogToolException += this.LogToolException;
			log.LogWarning += this.LogWarning;
			log.LogProfile += this.LogProfile;
			log.LogProfileEnd += this.LogProfileEnd;
			log.LogDebug += this.LogDebug;

            // Add it to the list of connected logs.
            lock (this.ConnectedLogs)
            {
                if (this.ConnectedLogs.Contains(log))
                    return;

                this.ConnectedLogs.Add(log);
            }
        }

        /// <summary>
        /// Determine whether we are connected to a particular log.
        /// </summary>
        /// <param name="log"></param>
        /// <returns></returns>
        public bool IsConnected(ILog log)
        {
            lock (this.ConnectedLogs)
                return (this.ConnectedLogs.Contains(log));
        }

        /// <summary>
        /// Disconnect from a log object.
        /// </summary>
        /// <param name="log"></param>
        public void Disconnect(ILog log)
        {
            log.LogMessage -= this.LogMessage;
            log.LogError -= this.LogError;
            log.LogToolError -= this.LogToolError;
            log.LogToolException -= this.LogToolException;
            log.LogWarning -= this.LogWarning;
            log.LogProfile -= this.LogProfile;
            log.LogProfileEnd -= this.LogProfileEnd;
            log.LogDebug -= this.LogDebug;

            lock (this.ConnectedLogs)
            {
                System.Diagnostics.Debug.Assert(this.ConnectedLogs.Contains(log),
                    "Log not connected to this LogTarget!  Ignoring.");
                if (!this.ConnectedLogs.Contains(log))
                    return;

                this.ConnectedLogs.Remove(log);
            }
        }

        /// <summary>
        /// Disconnect from all registered logs.
        /// </summary>
        public void DisconnectAll()
        {
            lock (this.ConnectedLogs)
            {
                while (this.ConnectedLogs.Count > 0)
                    Disconnect(this.ConnectedLogs.First());
            }
        }

        /// <summary>
        /// Attempts to format the passed in string.
        /// </summary>
        /// <param name="format"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        private string FormatString(string format, params object[] args)
        {
            try
            {
                if (format == null)
                {
                    System.Diagnostics.Debug.Fail("format is null");
                    throw new ArgumentNullException("format");
                }

                // Shouldn't happen unless null is explicitly passed in as the args.
                if (args == null)
                {
                    return format;
                }

                return String.Format(format, args);
            }
            catch (Exception)
            {
                StringBuilder message = new StringBuilder();
                message.AppendLine("<<STRING FORMATTING ERROR>>");
                message.AppendLine("Format:");
                message.AppendLine(format);
                message.AppendLine("Arguments:");

                for (int i = 0; i < args.Length; ++i)
                {
                    message.AppendLine(String.Format("{0}: {1}", i, args[i]));
                }

                return message.ToString();
            }
        }
        #endregion // Controller Methods

        #region Static Controller Methods
        /// <summary>
        /// Escape message string; so it's safe for String.Format used internally.
        /// </summary>
        /// This method is only safe to run on strings without formatting expressions;
        /// '{' and '}' will be escaped.
        /// <param name="message"></param>
        /// <returns></returns>
        public static String EscapeMessage(String message)
        {
            StringBuilder sb = new StringBuilder(message);
            foreach (Tuple<String, String> r in ESCAPE_CHARACTERS)
            {
                sb.Replace(r.Item1, r.Item2);
            }
            return (sb.ToString());
        }
        #endregion // Static Controller Methods
        
        #region OBSOLETE: Static Controller Methods (Default Log)
        /// <summary>
        /// Log an information message.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="args"></param>
        [Obsolete]
        static public void Log__Message(String message, params Object[] args)
        {
            Log.StaticLog.Message(message, args);
        }

        /// <summary>
        /// Log a information message with context.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="message"></param>
        /// <param name="args"></param>
        [Obsolete]
        static public void Log__MessageCtx(String context, String message, params Object[] args)
        {
            Log.StaticLog.MessageCtx(context, message, args);
        }

        /// <summary>
        /// Log an exception message.
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="message"></param>
        /// <param name="args"></param>
        /// The information logged is similar to what is displayed in the UI
        /// exception dialog class window.
        [Obsolete]
        static public void Log__Exception(Exception ex, String message, params Object[] args)
        {
            Log.StaticLog.ToolException(ex, message, args);
        }

        /// <summary>
        /// Log an exception message with context.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="ex"></param>
        /// <param name="message"></param>
        /// <param name="args"></param>
        [Obsolete]
        static public void Log__ExceptionCtx(Exception ex, String context, String message, params Object[] args)
        {
            Log.StaticLog.ToolExceptionCtx(context, ex, message, args);
        }

        /// <summary>
        /// Log an error message.
        /// </summary>
        /// <param name="message">Error message string to log</param>
        [Obsolete]
        static public void Log__Error(String message, params Object[] args)
        {
            Log.StaticLog.Error(message, args);
        }

        /// <summary>
        /// Log an error message with context.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="message"></param>
        /// <param name="args"></param>
        [Obsolete]
        static public void Log__ErrorCtx(String context, String message, params Object[] args)
        {
            Log.StaticLog.ErrorCtx(context, message, args);
        }

        /// <summary>
        /// Log an warning message.
        /// </summary>
        /// <param name="message">Warning message string to log</param>
        [Obsolete]
        static public void Log__Warning(String message, params Object[] args)
        {
            Log.StaticLog.Warning(message, args);
        }

        /// <summary>
        /// Log a warning message with context.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="message"></param>
        /// <param name="args"></param>
        [Obsolete]
        static public void Log__WarningCtx(String context, String message, params Object[] args)
        {
            Log.StaticLog.WarningCtx(context, message, args);
        }

        /// <summary>
        /// Log a debug message.
        /// </summary>
        /// <param name="message">Debug message string to log</param>
        [Obsolete]
        static public void Log__Debug(String message, params Object[] args)
        {
            Log.StaticLog.Debug(message, args);
        }

        /// <summary>
        /// Log a debug message with context.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="message"></param>
        /// <param name="args"></param>
        [Obsolete]
        static public void Log__DebugCtx(String context, String message, params Object[] args)
        {
            Log.StaticLog.DebugCtx(context, message, args);
        }

        /// <summary>
        /// Log a profile message and start marker.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="args"></param>
        [Obsolete]
        static public void Log__Profile(String message, params Object[] args)
        {
            Log.StaticLog.Profile(message, args);
        }

        /// <summary>
        /// Log a profile end marker.
        /// </summary>
        [Obsolete]
        static public void Log__ProfileEnd()
        {
            Log.StaticLog.ProfileEnd();
        }
        #endregion // Static Controller Methods (Default Log)
    }

} // RSG.Base.Logging namespace
