using System;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Text;

namespace RSG.Base
{
	/// <summary>
	/// Summary description for rageEmailUtilities.
	/// </summary>
    [Obsolete("DEAD: email server constants.")]
	public class rageEmailUtilities
	{
        // WARNING: Consult Michael Mattes (michael.mattes@rockstarsandiego.com) before using
        // these Smtp Email Server settings anywhere else.
        static string m_SmtpServer = "rsgsanexg1.rockstar.t2.corp";
        static string sm_SmtpUsername = "svcrsgsanscriptmaile"; // DO NOT USE ELSEWHERE WITHOUT MATTES' PERMISSION.
        static string sm_SmtpPassword = "RfbvFfMq;";    // DO NOT USE ELSEWHERE WITHOUT MATTES' PERMISSION.
        static string sm_SmtpSeed = "eNcRyPtEdYo";
        static bool sm_UseSmtpAuth = true;

        static Dictionary<string, string> sm_DomainToStudioNameMap = new Dictionary<string, string>();

        public static string SmtpServer
        {
            get { return m_SmtpServer; }
            set { m_SmtpServer = value; }
        }

        public static bool UseSmtpAuth
        {
            get { return sm_UseSmtpAuth; }
            set { sm_UseSmtpAuth = value; }
        }

        /* PURPOSE: Populate the map that lists the Rockstar studio domain names
         * and their friendly names. This function must be called at least once
         * before using the map, and can be called as many times as necessary.
         */
        private static void PopulateDomainToStudioNameMap()
        {
            if (sm_DomainToStudioNameMap.Count == 0)
            {
                sm_DomainToStudioNameMap.Add("rockstarsandiego.com", "R* San Diego");
                sm_DomainToStudioNameMap.Add("rockstarnorth.com", "R* North");
                sm_DomainToStudioNameMap.Add("rockstarnewengland.com", "R* New England");
                sm_DomainToStudioNameMap.Add("rockstarvancouver.com", "R* Vancouver");
                sm_DomainToStudioNameMap.Add("rockstartoronto.com", "R* Toronto");
                sm_DomainToStudioNameMap.Add("rockstargames.com", "R* NY");
                sm_DomainToStudioNameMap.Add("rockstarleeds.com", "R* Leeds");
                sm_DomainToStudioNameMap.Add("rockstarlondon.com", "R* London");
            }
        }

        /* PURPOSE: This is a hilarious function to encrypt/decrypt a string.
         * It is a simple XOR, so the function can be used both ways.
         */
        private static string PatheticCrypto(string sourceString, string seed)
        {
            int index = 0;
            string result = "";
            int length = sourceString.Length;
            int seedLen = seed.Length;

            for (int x = 0; x < length; x++)
            {
                result += (char) (sourceString[x] ^ (seed[index++] & 15));
                index %= seedLen;
            }

            return result;
        }

        /* PURPOSE: Return a friendly name of the studio from an email address.
         * "jdoe@rockstarsandiego.com" for example would return "R* San Diego".
         */
        public static string GetStudioNameFromEmail(string emailAddress)
        {
            PopulateDomainToStudioNameMap();

            int domainIndex = emailAddress.IndexOf('@');

            if (domainIndex == -1)
            {
                return "?";
            }

            string domain = emailAddress.Substring(domainIndex + 1);

            try
            {
                return sm_DomainToStudioNameMap[domain.ToLower()];
            }
            catch (Exception /*e*/)
            {
                // Unknown domain name. Just use the domain name.
                // Skip the ".com" at the end.
                int periodIndex = domain.IndexOf('.');

                if (periodIndex == -1)
                {
                    // Weird domain name... that doesn't look right.
                    // No TLD?
                    return domain;
                }

                return domain.Substring(0, periodIndex);
            }
        }

        private static List<string> SplitAddresses( string strAddresses )
        {
            List<string> strAddressList = new List<string>();

            string[] astrAddresses = strAddresses.Split();
            foreach ( string strAddress in astrAddresses )
            {
                string[] astrSubAddresses = strAddress.Split( new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries );
                foreach ( string strSubAddress in astrSubAddresses )
                {
                    if ( strSubAddress != string.Empty )
                    {
                        strAddressList.Add( strSubAddress );
                    }
                }
            }

            return strAddressList;
        }

		public enum MailFormat {Html, Text};
		public static void SendEmail(string strToAddresses, 
                                     string strFromAddress, 
                                     string strReplyToAddress, 
                                     string strSubject,
                                     string strFromAddressDisplayName,
                                     string strBody, 
									 bool bIncludeImages,
                                     Attachment[] aobAttachments, 
                                     MailFormat Format)
		{
			// System.Console.WriteLine("SendEmail(\""+ strToAddress +"\", \""+ strFromAddress +"\", \""+ strSubject +"\", \""+ strBody +"\", System.Net.Mail.MailFormat Format)");
            MailAddress obFrom = null;

            List<string> strFromAddressList = SplitAddresses( strFromAddress );
            if ( strFromAddressList.Count > 0 )
            {
                // make sure we have only one 'from'.
                obFrom = new MailAddress( strFromAddressList[0], strFromAddressDisplayName );
            }
            else
            {
                throw new Exception( String.Format( "Unable to parse the strFromAddress string '{0}'.", strFromAddress ) );
            }
                        
            MailMessage obEmail = new MailMessage();

			obEmail.From = obFrom;
            if ( !String.IsNullOrEmpty( strReplyToAddress ) )
			{
                List<string> replyToList = SplitAddresses( strReplyToAddress );
                if ( replyToList.Count > 0 )
                {
#if NET_20_FRAMEWORK
                    obEmail.ReplyTo = new MailAddress( replyToList[0] );
#else
                    // make sure we have only one 'replyTo'.
                    obEmail.ReplyToList.Add( new MailAddress( replyToList[0] ) );
#endif
                }
			}

            List<string> strToAddressList = SplitAddresses( strToAddresses );
            foreach ( string strToAddress in strToAddressList )
            {
                obEmail.To.Add( strToAddress );
            }

			obEmail.Subject = strSubject;
			strBody = strBody.Replace((char)0, (char)' ');
			obEmail.IsBodyHtml = (Format == MailFormat.Html);

			if (!bIncludeImages)
			{
				obEmail.Body = strBody;
			}
			else
			{
				// Include images, so do some magic
				// http://blogs.interakting.co.uk/brad/archive/2007/06/12/67.aspx
				// Seek images in body
				// Open the xml file
				XmlDocument doc = new XmlDocument();
				try
				{
					doc.LoadXml(strBody);
				}
				catch (Exception ex)
				{
					// Console.WriteLine("An error occurred loading " + strXMLPathAndFilename + " : " + ex.ToString());
					Console.Error.WriteLine("An error occurred loading html email as an xml doc : " + ex.ToString());
					//check the InnerException
					while (ex.InnerException != null)
					{
						// Console.WriteLine("--------------------------------");
						// Console.WriteLine("The following InnerException reported: " + ex.InnerException.ToString());
						Console.Error.WriteLine("--------------------------------");
						Console.Error.WriteLine("The following InnerException reported: " + ex.InnerException.ToString());
						ex = ex.InnerException;
					}
					return;
				}

				// Get all the path referenced by this file
				string xpath = "descendant::img";
				XmlNodeList aobImgNodeList = doc.SelectNodes(xpath);
				Console.WriteLine("aobImgNodeList.Count = " + aobImgNodeList.Count);
				List<LinkedResource> aobLinkedResources = new List<LinkedResource>();
				foreach (XmlNode obImgNode in aobImgNodeList)
				{
					// Get image
					string strImagePathAndFilename = obImgNode.Attributes["src"].Value;

					// Extract filename out of strImagePathAndFilename
					if (strImagePathAndFilename.StartsWith("file:///"))
					{
						strImagePathAndFilename = strImagePathAndFilename.Substring(8);
					}

					// Load image
					LinkedResource obImage = new LinkedResource(strImagePathAndFilename, "image/jpeg");
					obImage.ContentId = Path.GetFileName(strImagePathAndFilename);
					aobLinkedResources.Add(obImage);

					// Set xml to point to new version
					obImgNode.Attributes["src"].Value = "cid:"+ Path.GetFileName(strImagePathAndFilename);
				}


				// Convert xml doc back to a string
				MemoryStream obDocStream = new MemoryStream();
				doc.Save(obDocStream);
				byte[] abDocBytes = new byte[obDocStream.Length];
				obDocStream.Seek(0, SeekOrigin.Begin);
				obDocStream.Read(abDocBytes, 0, (int)obDocStream.Length);
				// Console.WriteLine(Encoding.ASCII.GetString(abDocBytes));
				
				// Create alternative view
				AlternateView avHTML = AlternateView.CreateAlternateViewFromString(Encoding.ASCII.GetString(abDocBytes), null, "text/html");

				// Add images
				foreach (LinkedResource obResource in aobLinkedResources)
				{
					avHTML.LinkedResources.Add(obResource);
				}

				//Create LinkedResource to hold the image
				//LinkedResource pic1 = new LinkedResource(ImageFilePath, MediaTypeNames.Image.Gif);
				//pic1.ContentId = "Pic1";
				//avHTML.LinkedResources.Add(pic1);

				obEmail.AlternateViews.Add(avHTML);
			}

			// Attachments?
			foreach (Attachment obAttachment in aobAttachments)
			{
				obEmail.Attachments.Add(obAttachment);
			}

			Console.WriteLine("m_SmtpServer = " + m_SmtpServer);
			SmtpClient obSmtpServer = new SmtpClient(m_SmtpServer);

            if (sm_UseSmtpAuth)
            {
				Console.WriteLine("Using Credentials");

				obSmtpServer.UseDefaultCredentials = false;
                obSmtpServer.Credentials = new NetworkCredential(sm_SmtpUsername, PatheticCrypto(sm_SmtpPassword, sm_SmtpSeed));
            }
            else
            {
				Console.WriteLine("Using DefaultNetworkCredentials");
				obSmtpServer.Credentials = CredentialCache.DefaultNetworkCredentials;
            }

			try
			{
				Console.WriteLine("Sending mail");
				obSmtpServer.Send(obEmail);
			}
			catch (Exception ex)
			{
				System.Console.WriteLine("The following exception occurred when trying to send mail: " + ex.ToString());
				//check the InnerException
				while (ex.InnerException != null)
				{
					System.Console.WriteLine("--------------------------------");
					System.Console.WriteLine("The following InnerException reported: " + ex.InnerException.ToString());
					ex = ex.InnerException;
				}
			}
		}

		public static void SendEmail(string strToAddresses,
									 string strFromAddress,
									 string strReplyToAddress,
									 string strSubject,
									 string strFromAddressDisplayName,
									 string strBody,
									 Attachment[] aobAttachments,
									 MailFormat Format)
		{
			SendEmail(strToAddresses, strFromAddress, strReplyToAddress, strSubject, strFromAddressDisplayName, strBody, false, aobAttachments, Format);
		}

        public static void SendEmail(string strToAddresses,
                                     string strFromAddress,
                                     string strReplyToAddress,
                                     string strSubject,
                                     string strBody,
                                     Attachment[] aobAttachments,
                                     MailFormat Format)
        {
            SendEmail(strToAddresses, strFromAddress, strReplyToAddress, strSubject, strBody, strFromAddress, aobAttachments, Format);
        }

        public static void SendEmail(string strToAddresses, string strFromAddress, string strReplyToAddress, string strSubject, string strBody, string[] astrAttachments, MailFormat Format)
        {
            SendEmail(strToAddresses, strFromAddress, strReplyToAddress, strSubject, strFromAddress, strBody, astrAttachments, Format);
        }
		
		public static void SendEmail(string strToAddresses, string strFromAddress, string strReplyToAddress, string strSubject, string strFromAddressDisplayName, string strBody, string[] astrAttachments, MailFormat Format)
		{
			// Attachments?
			Attachment[] aobAttachments = new Attachment[astrAttachments.Length];
			for(int i=0; i<astrAttachments.Length; i++)
			{
				aobAttachments[i] = new Attachment(astrAttachments[i]);
			}
			SendEmail(strToAddresses, strFromAddress, strReplyToAddress, strSubject, strFromAddressDisplayName, strBody, aobAttachments, Format);
		}

        public static void SendEmail(string strToAddresses, string strFromAddress, string strReplyToAddress, string strSubject, string strFromAddressDisplayName, string strBody, MailFormat Format)
        {
            SendEmail(strToAddresses, strFromAddress, strReplyToAddress, strSubject, strFromAddressDisplayName, strBody, new string[0], Format);
        }

        public static void SendEmail(string strToAddresses, string strFromAddress, string strReplyToAddress, string strSubject, string strBody, MailFormat Format)
        {
            SendEmail(strToAddresses, strFromAddress, strReplyToAddress, strSubject, strBody, new string[0], Format);
        }

        public static void SendEmail(string strToAddresses, string strFromAddress, string strSubject, string strBody, MailFormat Format)
		{
			SendEmail(strToAddresses, strFromAddress, "", strSubject, strBody, Format);
		}

		public static void SendEmail(string strToAddress, string strFromAddress, string strSubject, ArrayList astrBodyLines)
		{
			StringBuilder strBody = new StringBuilder();
			foreach(string strLine in astrBodyLines)
			{
				strBody.AppendLine(strLine);
			}

			// Got everything I need, so send the email
			SendEmail(strToAddress, strFromAddress, strSubject, strBody.ToString(), GuessFormat(strBody.ToString()));
		}

        public static void SendEmail(string strToAddress, string strFromAddress, string strSubject, List<string> astrBodyLines)
        {
            SendEmail(strToAddress, strFromAddress, strSubject, strFromAddress, astrBodyLines);
        }

        public static void SendEmail(string strToAddress, string strFromAddress, string strSubject, string strFromAddressDisplayName, List<string> astrBodyLines)
        {
            StringBuilder strBody = new StringBuilder();
            foreach (string strLine in astrBodyLines)
            {
                strBody.AppendLine(strLine);
            }

            // Got everything I need, so send the email
			SendEmail(strToAddress, strFromAddress, strFromAddress, strSubject, strFromAddressDisplayName, strBody.ToString(), GuessFormat(strBody.ToString()));
        }

        public static void SendEmail(string strToAddress, string strFromAddress, string strSubject, string[] astrBodyLines)
		{
			StringBuilder strBody = new StringBuilder();
			foreach(string strLine in astrBodyLines)
			{
				strBody.AppendLine(strLine);
			}

			// Got everything I need, so send the email
			SendEmail(strToAddress, strFromAddress, strSubject, strBody.ToString(), GuessFormat(strBody.ToString()));
		}

		public static void SendEmail(string strToAddress, string strFromAddress, string strSubject, string strBodyFile)
		{
            // Got good args, so load body
            if (!File.Exists(strBodyFile))
            {
                Console.WriteLine("File \"" + strBodyFile + "\" does not exist.  Unable to email to " + strToAddress);
                // Output the stack, just because it is useful
                Console.WriteLine(Environment.StackTrace);

                // Continue
                Console.WriteLine("Continuing regardless");
                return;
            }
            StreamReader sr = File.OpenText(strBodyFile);
			StringBuilder strBody = new StringBuilder();
            string strInput;
            while ((strInput = sr.ReadLine()) != null)
            {
                strBody.AppendLine(strInput);
            }
            // Console.WriteLine ("The end of the stream has been reached.");
            sr.Close();

            // Got everything I need, so send the email
			SendEmail(strToAddress, strFromAddress, strSubject, strBody.ToString(), GuessFormat(strBody.ToString()));
        }

		private static MailFormat GuessFormat(string strBody)
		{
			// Guess the format
			if((strBody.ToLower()).StartsWith("<html>"))
			{
				return MailFormat.Html;
			}
			else
			{
				return MailFormat.Text;
			}
		}
	}
}
