﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.Serialization;
using RSG.Base.Attributes;
using RSG.Base.Extensions;
using System.Windows.Media.Imaging;
using System.Diagnostics;
using System.Collections.Generic;

namespace RSG.Base.Logging
{

    /// <summary>
    /// Log message level enumeration.
    /// 
    /// Attributes are used as following:
    /// * EnumMember - for WCF serialisation.
    /// * DisplayIcon - Icon associated with this particular log level.
    /// * FieldDisplayName - Name associated with this particular log level (as shown in the UI).
    /// * XmlConstant - Name of the element for that particular log level as it appears in the ulog file.
    /// * Browsable (optional) - Indicates that a particular log level shouldn't be shown in the UI (e.g. in a filter command).
    /// </summary>
    [DataContract]
    public enum LogLevel
    {
        /// <summary>
        /// Tool internal-exception level.
        /// </summary>
        [EnumMember]
        [DisplayIcon("RSG.Base", "Resources/exception16.png")]
        [FieldDisplayName("Exception")]
        [XmlConstant("tool_exception")]
        ToolException,
        
        /// <summary>
        /// Tool internal-error level.
        /// </summary>
        [EnumMember]
        [DisplayIcon("RSG.Base", "Resources/error16cyan.png")]
        [Browsable(false)]
        [XmlConstant("tool_error")]
        ToolError,

        /// <summary>
        /// User error level.
        /// </summary>
        [EnumMember]
        [DisplayIcon("RSG.Base", "Resources/error16cyan.png")]
        [FieldDisplayName("Error")]
        [XmlConstant("error")]
        Error,

        /// <summary>
        /// User warning level.
        /// </summary>
        [EnumMember]
        [DisplayIcon("RSG.Base", "Resources/warning16cyan.png")]
        [FieldDisplayName("Warning")]
        [XmlConstant("warning")]
        Warning,

        /// <summary>
        /// User information level.
        /// </summary>
        [EnumMember]
        [DisplayIcon("RSG.Base", "Resources/information16.png")]
        [FieldDisplayName("Info Message")]
        [XmlConstant("message")]
        Info,

        /// <summary>
        /// Profile level.
        /// </summary>
        [EnumMember]
        [DisplayIcon("RSG.Base", "Resources/profile16.png")]
        [FieldDisplayName("Profile Message")]
        [XmlConstant("profile")]
        Profile,
        
        /// <summary>
        /// Debug level.
        /// </summary>
        [EnumMember]
        [DisplayIcon("RSG.Base", "Resources/debug16.png")]
        [FieldDisplayName("Debug Message")]
        [XmlConstant("debug")]
        Debug,

        /// <summary>
        /// Profile-end level (internally used).
        /// </summary>
        [EnumMember]
        [Browsable(false)]
        [DisplayIcon("RSG.Base", "Resources/profile16.png")]
        [XmlConstant("profile_end")]
        ProfileEnd,
    } // LogLevel

    /// <summary>
    /// Utility methods associated with the <see cref="LogLevel" /> enumeration.
    /// </summary>
    public static class LogLevelUtils
    {
        /// <summary>
        /// Retrieves the icon associated with a particular LogLevel.
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        public static BitmapSource GetIcon(this LogLevel level)
        {
            DisplayIconAttribute iconAtt = level.GetAttributeOfType<DisplayIconAttribute>();
            if (iconAtt == null)
            {
                Debug.Fail("LogLevel is missing the DisplayIconAttribute.");
                return null;
            }
            else
            {
                return iconAtt.Icon;
            }
        }

        /// <summary>
        /// Retrieves the element name that a particular log level has associated with it.
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        public static String GetXmlElementName(this LogLevel level)
        {
            // Check whether we need to initialise the element name to LogLevel lookup.
            lock (s_logLevelLock)
            {
                if (s_logLevelToElementNameLookup == null)
                {
                    s_logLevelToElementNameLookup = new Dictionary<LogLevel, String>();
                    foreach (LogLevel currentLevel in Enum.GetValues(typeof(LogLevel)))
                    {
                        XmlConstantAttribute constAtt = currentLevel.GetAttributeOfType<XmlConstantAttribute>();
                        if (constAtt == null)
                        {
                            Debug.Fail("LogLevel is missing the XmlConstantAttribute.");
                            throw new MissingAttributeException(level.ToString(), typeof(XmlConstantAttribute));
                        }

                        s_logLevelToElementNameLookup[currentLevel] = constAtt.Constant;
                    }
                }
            }

            return s_logLevelToElementNameLookup[level];
        }
        private static IDictionary<LogLevel, String> s_logLevelToElementNameLookup;

        /// <summary>
        /// Converts an xml element name into a LogLevel enum value.
        /// </summary>
        /// <param name="elementName">Name of the xml element.</param>
        /// <returns>LogLevel that the element name corresponds to or LogLevel.Info if it isn't valid.</returns>
        public static LogLevel GetLogLevelFromXmlElementName(String elementName)
        {
            // Check whether we need to initialise the element name to LogLevel lookup.
            if (s_elementNameToLogLevelLookup == null)
            {
                s_elementNameToLogLevelLookup = new Dictionary<String, LogLevel>();
                foreach (LogLevel currentLevel in Enum.GetValues(typeof(LogLevel)))
                {
                    String name = currentLevel.GetXmlElementName();
                    Debug.Assert(!s_elementNameToLogLevelLookup.ContainsKey(name), "Multiple elements exist with the same log level element name.");
                    s_elementNameToLogLevelLookup[name] = currentLevel;
                }
            }

            // Attempt to convert the element name to a LogLevel value.
            LogLevel level;
            if (!s_elementNameToLogLevelLookup.TryGetValue(elementName, out level))
            {
                level = LogLevel.Info;
            }
            return level;
        }
        private static object s_logLevelLock = new object();
        private static IDictionary<String, LogLevel> s_elementNameToLogLevelLookup;
    } // LogLevelUtils

} // RSG.Base.Logging namespace
