﻿namespace RSG.Base.Logging.Universal
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;
    using System.Xml;

    /// <summary>
    /// Represents a log target that stores the universal log messages inside a collection in
    /// memory.
    /// </summary>
    public abstract class InMemoryUniversalLogTarget
    {
        #region Fields
        /// <summary>
        /// The private buffer used to store the log messages into.
        /// </summary>
        private readonly ICollection<BufferedMessage> buffer;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="InMemoryLogTarget"/> class.
        /// </summary>
        protected InMemoryUniversalLogTarget()
        {
            this.buffer = new List<BufferedMessage>();
        }

        /// <summary>
        /// Finaliser.
        /// </summary>
        ~InMemoryUniversalLogTarget()
        {
            this.DisposeManagedResources();
        }
        #endregion

        #region Events
        /// <summary>
        /// Occurs whenever the buffer of log messages gets cleared.
        /// </summary>
        public event EventHandler Cleared;

        /// <summary>
        /// Occurs when the the buffered collection of log messages change.
        /// </summary>
        public event EventHandler<UniversalBufferChangedEventArgs> UniversalBufferChanged;
        #endregion

        #region Properties
        /// <summary>
        /// Gets a iterator around the buffered messages currently in memory.
        /// </summary>
        public IEnumerable<BufferedMessage> Buffer
        {
            get
            {
                foreach (BufferedMessage bufferedMessage in this.buffer)
                {
                    yield return bufferedMessage;
                }
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Clears the buffered log messages.
        /// </summary>
        public void Clear()
        {
            this.buffer.Clear();
            EventHandler handler = this.Cleared;
            if (handler == null)
            {
                return;
            }

            handler(this, EventArgs.Empty);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        public void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            IEnumerable<string> contexts = (from m in this.buffer
                                            select m.Context as string).Distinct();

            foreach (string context in contexts)
            {
                writer.WriteStartElement("context");
                writer.WriteAttributeString("name", context);

                IEnumerable<BufferedMessage> messages = from m in this.buffer
                                                        where m.Context == context
                                                        select m;

                foreach (BufferedMessage msg in messages)
                {
                    XElement elementXml = msg.ToXml();
                    if (elementXml != null)
                    {
                        elementXml.WriteTo(writer);
                    }
                }

                writer.WriteEndElement();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected abstract void DisposeManagedResources();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name">
        /// 
        /// </param>
        /// <param name="context">
        /// 
        /// </param>
        /// <param name="level">
        /// 
        /// </param>
        /// <param name="message">
        /// 
        /// </param>
        protected void AddLogMessage(string name, string context, LogLevel level, string msg, string preformattedMsg, DateTime timeStamp)
        {
            BufferedMessage bmsg = new BufferedMessage(name, level, context, context, msg, preformattedMsg, timeStamp);
            this.buffer.Add(bmsg);
            EventHandler<UniversalBufferChangedEventArgs> handle = this.UniversalBufferChanged;
            if (handle == null)
            {
                return;
            }

            handle(this, new UniversalBufferChangedEventArgs(true, bmsg));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name">
        /// 
        /// </param>
        /// <param name="context">
        /// 
        /// </param>
        /// <param name="level">
        /// 
        /// </param>
        /// <param name="message">
        /// 
        /// </param>
        protected void AddLogMessage(string name, string appContext, string context, LogLevel level, string msg, string preformattedMsg, DateTime timeStamp)
        {
            BufferedMessage bmsg = new BufferedMessage(name, level, appContext, context, msg, preformattedMsg, timeStamp);
            this.buffer.Add(bmsg);
            EventHandler<UniversalBufferChangedEventArgs> handle = this.UniversalBufferChanged;
            if (handle == null)
            {
                return;
            }

            handle(this, new UniversalBufferChangedEventArgs(true, bmsg));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name">
        /// 
        /// </param>
        /// <param name="evt">
        /// 
        /// </param>
        protected void AddLogMessage(string name, LogMessageEventArgs evt)
        {
            BufferedMessage bmsg = new BufferedMessage(name, evt.Level, "Default", evt.Context, evt.Message, evt.PreformattedMessage, DateTime.UtcNow);
            this.buffer.Add(bmsg);
            EventHandler<UniversalBufferChangedEventArgs> handle = this.UniversalBufferChanged;
            if (handle == null)
            {
                return;
            }

            handle(this, new UniversalBufferChangedEventArgs(true, bmsg));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name">
        /// 
        /// </param>
        /// <param name="evt">
        /// 
        /// </param>
        protected void AddLogMessage(string name, XElement element)
        {
            BufferedMessage bmsg = new BufferedMessage(name, element);
            this.buffer.Add(bmsg);
            EventHandler<UniversalBufferChangedEventArgs> handle = this.UniversalBufferChanged;
            if (handle == null)
            {
                return;
            }

            handle(this, new UniversalBufferChangedEventArgs(true, bmsg));
        }
        #endregion

        #region Classes
        /// <summary>
        /// Represents a single buffered message stored in the in memory universal log target.
        /// </summary>
        public class BufferedMessage : UniversalLogFileBufferedMessage
        {
            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="BufferedMessage"/> class.
            /// </summary>
            /// <param name="name">
            /// 
            /// </param>
            /// <param name="level">
            /// 
            /// </param>
            /// <param name="ctx">
            /// 
            /// </param>
            /// <param name="msg">
            /// 
            /// </param>
            public BufferedMessage(string name, LogLevel level, string appCtx, String ctx, String msg, String preformattedMsg, DateTime timeStamp)
                : base(level, ctx, msg, preformattedMsg, timeStamp)
            {
                this.ApplicationContext = appCtx;
                this.SourceName = name;
            }

            /// <summary>
            /// Initialises a new instance of the <see cref="BufferedMessage"/> class.
            /// </summary>
            /// <param name="name">
            /// 
            /// </param>
            /// <param name="element">
            /// 
            /// </param>
            public BufferedMessage(string name, XElement element)
                : base(element)
            {
                XElement parent = element.Parent;
                if (parent != null && parent.Name.LocalName == "context")
                {
                    XAttribute contextElement = parent.Attribute("name");
                    if (contextElement != null)
                    {
                        this.ApplicationContext = contextElement.Value;
                    }
                    else
                    {  
                        this.ApplicationContext = "Default";
                    }
                }
                else
                {
                    this.ApplicationContext = "Default";
                }

                this.SourceName = name;
            }
            #endregion

            #region Properties
            /// <summary>
            /// Gets the name of the source object that this buffered message came from.
            /// </summary>
            public string SourceName
            {
                get;
                private set;
            }

            public string ApplicationContext
            {
                get;
                private set;
            }
            #endregion
        }
        #endregion
    } // RSG.Base.Logging.InMemoryUniversalLogTarget

    /// <summary>
    /// Provides the data forwarded in a universal buffer changed event handler.
    /// </summary>
    public class UniversalBufferChangedEventArgs : EventArgs
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="UniversalBufferChangedEventArgs"/>
        /// class.
        /// </summary>
        /// <param name="added">
        /// A value indicating whether the messages in the <paramref name="data"/> parameter
        /// have been added or removed.
        /// </param>
        /// <param name="data">
        /// The buffered message that has changed. 
        /// </param>
        public UniversalBufferChangedEventArgs(
            bool added, InMemoryUniversalLogTarget.BufferedMessage data)
        {
            this.Data = Enumerable.Repeat(data, 1);
            this.Added = added;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="UniversalBufferChangedEventArgs"/>
        /// class.
        /// </summary>
        /// <param name="added">
        /// A value indicating whether the messages in the <paramref name="data"/> parameter
        /// have been added or removed.
        /// </param>
        /// <param name="data">
        /// The buffered messages that have changed.
        /// </param>
        public UniversalBufferChangedEventArgs(
            bool added, IEnumerable<InMemoryUniversalLogTarget.BufferedMessage> data)
        {
            this.Data = data;
            this.Added = added;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets a value indicating whether the messages inside the <see cref="Data"/> property
        /// have been added or removed.
        /// </summary>
        public bool Added
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets a iterator around a collection of buffered messages that have been either
        /// added or removed.
        /// </summary>
        public IEnumerable<InMemoryUniversalLogTarget.BufferedMessage> Data
        {
            get;
            private set;
        }
        #endregion
    } // RSG.Base.Logging.Universal.UniversalBufferChangedEventArgs
} // RSG.Base.Logging.Universal
