using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

using RSG.Base.IO;

namespace RSG.Base.Logging
{

    [Obsolete] // USE Universal Log instead.
	public class rageHtmlLogII : TextWriter
	{
		public rageHtmlLogII(string strFileName, bool bRealTimeUpdate, bool bDebugPrintAsWell)
		{
			m_obRageHtmlLog = new rageHtmlLog(strFileName, rageFileUtilities.RemoveFileExtension(rageFileUtilities.GetFilenameFromFilePath(strFileName)));
			UpdateFileInRealTime = bRealTimeUpdate;
			m_bDebugPrintAsWell = bDebugPrintAsWell;
		}

		~rageHtmlLogII()
		{
			// Give the log a kick
			m_obRageHtmlLog.UpdateFileInRealTime = true;
		}

		public override void Write(string value)
		{
			DebugPrintAsWell(value, true);
			m_obRageHtmlLog.LogEvent(value);
			base.Write(value);
		}

		public override void WriteLine(string value)
		{
			DebugPrintAsWell(value, true);
			m_obRageHtmlLog.LogEvent(value);
			base.WriteLine(value);
		}

		public override Encoding Encoding
		{
			get { return Encoding.Unicode; }
		}

		private void DebugPrintAsWell(string strPrintMe, bool bLine)
		{
			if (m_bDebugPrintAsWell)
			{
                //Debug.Print(strPrintMe);

				// Store current out
				TextWriter obPreviousTextWriter = Console.Out;

				// Reset out
				StreamWriter standardOutput = new StreamWriter(Console.OpenStandardOutput());
				standardOutput.AutoFlush = true;
				Console.SetOut(standardOutput);

				// Output a timestamp
				Console.Write(DateTime.Now.ToLongTimeString() + " " + DateTime.Now.ToShortDateString() + " : ");

				// Output the text
				if (bLine) Console.WriteLine(strPrintMe);
				else Console.Write(strPrintMe);

				// Restore everything
				Console.SetOut(obPreviousTextWriter);
			}
		}

		private rageHtmlLog m_obRageHtmlLog;
		private bool m_bDebugPrintAsWell;

		public bool UpdateFileInRealTime
		{
			get
			{
				return m_obRageHtmlLog.UpdateFileInRealTime;
			}
			set
			{
				m_obRageHtmlLog.UpdateFileInRealTime = value;
			}
		}

	}
}
