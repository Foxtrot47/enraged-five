﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace RSG.Base.Logging
{
    /// <summary>
    /// Outputs log messages to the visual studio output window.
    /// </summary>
    public class VSOutputLogTarget :
        LogTargetBase,
        ILogTarget
    {
        #region Constants
        private const String PREFIX_ERROR = "error";
        private const String PREFIX_WARNING = "warn";
        private const String PREFIX_DEBUG = "debug";
        private const String PREFIX_PROFILE = "prof";
        private const String PREFIX_INFO = "info";
        #endregion // Constants
        
        #region Static Member Data
        /// <summary>
        /// Console resource lock (static as process-wide).
        /// </summary>
        private static Object s_lock;
        #endregion // Static Member Data
        
        #region Constructor(s)
        /// <summary>
        /// Default constructor; connecting to static log.
        /// </summary>
        /// Connect our static Log event to us.
        public VSOutputLogTarget()
            : this(true)
        {
        }
        
        /// <summary>
        /// Constructor; connecting to static log.
        /// </summary>
        /// <param name="timestamp"></param>
        /// Connect our static Log event to us.
        public VSOutputLogTarget(bool timestamp)
            : this(timestamp, Log.StaticLog)
        {
        }

        /// <summary>
        /// Constructor; connecting to a particular log.
        /// </summary>
        /// <param name="timestamp"></param>
        /// <param name="log"></param>
        public VSOutputLogTarget(bool timestamp, ILog log)
        {
            this.Enabled = true;
            this.Timestamp = timestamp;

            this.Connect(log);
        }

        /// <summary>
        /// Static constructor (create resource lock object).
        /// </summary>
        static VSOutputLogTarget()
        {
            s_lock = new Object();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Connect to a log object.
        /// </summary>
        /// <param name="log"></param>
        public override void Connect(ILog log)
        {
            log.LogMessage += this.LogMessage;
            log.LogError += this.LogError;
            log.LogWarning += this.LogWarning;
#if DEBUG
            log.LogDebug += this.LogDebug;
#endif // DEBUG

            base.Connect(log);
        }

        /// <summary>
        /// Connect to a log object (up to a particular level).
        /// </summary>
        /// <param name="log"></param>
        /// <param name="uptoLevel"></param>
        public override void Connect(ILog log, LogLevel uptoLevel)
        {
            if ((int)uptoLevel >= (int)LogLevel.Error)
                log.LogError += this.LogError;
            if ((int)uptoLevel >= (int)LogLevel.Warning)
                log.LogWarning += this.LogWarning;
            if ((int)uptoLevel >= (int)LogLevel.Info)
                log.LogMessage += this.LogMessage;
            // No Profile Support.
            if ((int)uptoLevel >= (int)LogLevel.Debug)
                log.LogDebug += this.LogDebug;

            base.Connect(log, uptoLevel);
        }

        /// <summary>
        /// Disconnect from a log object.
        /// </summary>
        /// <param name="log"></param>
        public override void Disconnect(ILog log)
        {
            log.LogMessage -= this.LogMessage;
            log.LogError -= this.LogError;
            log.LogWarning -= this.LogWarning;
#if DEBUG
            log.LogDebug -= this.LogDebug;
#endif // DEBUG

            base.Disconnect(log);
        }

        /// <summary>
        /// Empty flush as it doesn't need to be flushed to disk.
        /// </summary>
        public override void Flush()
        {
            
        }
        #endregion // Controller Methods

        #region Private Methods
        #region Private Event Handlers
        /// <summary>
        /// Log message event handler
        /// </summary>
        private void LogMessage(Object sender, LogMessageEventArgs e)
        {
            if (!this.Enabled)
                return;

            lock (s_lock)
            {
                if (this.Timestamp)
                    System.Diagnostics.Debug.Write(e.Timestamp.ToLocalTime().ToString() + " ");

                WritePrefix(PREFIX_INFO);
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
        }

        /// <summary>
        /// Log error message event handler
        /// </summary>
        private void LogError(Object sender, LogMessageEventArgs e)
        {
            if (!this.Enabled)
                return;

            lock (s_lock)
            {
                if (this.Timestamp)
                    System.Diagnostics.Debug.Write(e.Timestamp.ToLocalTime().ToString() + " ");

                WritePrefix(PREFIX_ERROR);
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
        }

        /// <summary>
        /// Log warning message event handler
        /// </summary>
        private void LogWarning(Object sender, LogMessageEventArgs e)
        {
            if (!this.Enabled)
                return;

            lock (s_lock)
            {
                if (this.Timestamp)
                    System.Diagnostics.Debug.Write(e.Timestamp.ToLocalTime().ToString() + " ");

                WritePrefix(PREFIX_WARNING);
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
        }

        /// <summary>
        /// Log debug message event handler
        /// </summary>
        private void LogDebug(Object sender, LogMessageEventArgs e)
        {
#if DEBUG
            if (!this.Enabled)
                return;

            lock (s_lock)
            {
                if (this.Timestamp)
                    System.Diagnostics.Debug.Write(e.Timestamp.ToLocalTime().ToString() + " ");

                WritePrefix(PREFIX_DEBUG);
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
#endif
        }
        #endregion // Private Event Handlers

        /// <summary>
        /// 
        /// </summary>
        /// <param name="col"></param>
        /// <param name="prefix"></param>
        /// <param name="writer"></param>
        private void WritePrefix(String prefix)
        {
            System.Diagnostics.Debug.Write(String.Format("[{0}] ", prefix));
        }
        #endregion // Private Methods
    } // VSOutputLogTarget
} // RSG.Base.Logging namespace
