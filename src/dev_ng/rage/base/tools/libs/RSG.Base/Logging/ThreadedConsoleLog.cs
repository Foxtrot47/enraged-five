﻿//---------------------------------------------------------------------------------------------
// <copyright file="ThreadedConsoleLog.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Base.Logging
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Text;
    using RSG.Base.Collections;
    using RSG.Base.Extensions;

    /// <summary>
    /// Threaded console logging class; with some pretty printing colours for the 
    /// different message classifications.
    /// </summary>
    public class ThreadedConsoleLog :
        ThreadedLogTargetBase,
        ILogTarget
    {
        #region Constants
        /// <summary>
        /// Message prefix strings.
        /// </summary>
        private readonly IDictionary<LogLevel, String> MessagePrefixes = new Dictionary<LogLevel, String>()
        {
            {LogLevel.ToolException, "tool exception"},
            {LogLevel.ToolError, "tool error"},
            {LogLevel.Error, "error"},
            {LogLevel.Warning, "warn"},
            {LogLevel.Debug, "debug"},
            {LogLevel.Profile, "prof"},
            {LogLevel.ProfileEnd, "prof"},
            {LogLevel.Info, "info"},
        };

        /// <summary>
        /// Message colours.
        /// </summary>
        private readonly IDictionary<LogLevel, ConsoleColor> MessageColours = new Dictionary<LogLevel, ConsoleColor>()
        {
            {LogLevel.ToolException, ConsoleColor.Red},
            {LogLevel.ToolError, ConsoleColor.Red},
            {LogLevel.Error, ConsoleColor.Red},
            {LogLevel.Warning, ConsoleColor.Yellow},
            {LogLevel.Debug, ConsoleColor.Blue},
            {LogLevel.Profile, ConsoleColor.Cyan},
            {LogLevel.ProfileEnd, ConsoleColor.Cyan},
            {LogLevel.Info, ConsoleColor.Green},
        };
        #endregion // Constants

        #region Member Data
        /// <summary>
        /// Stack of profile contexts and stopwatches.
        /// </summary>
        private Stack<Pair<String, Stopwatch>> _contexts;

        /// <summary>
        /// Maximum character padding; for pretty printing.
        /// </summary>
        private int _maxPadding = 6;
        #endregion // Member Data

        #region Static Member Data
        /// <summary>
        /// Console resource lock (static as process-wide).
        /// </summary>
        private static Object _lock;
        #endregion // Static Member Data

        #region Constructor
        /// <summary>
        /// Default constructor; no log connected.
        /// </summary>
        /// Connect our static Log event to us.
        public ThreadedConsoleLog()
        {
            this.Enabled = true;
            this.Timestamp = true;
            this.ShowContext = false;
            this._contexts = new Stack<Pair<string, Stopwatch>>();
        }

        /// <summary>
        /// Static constructor (create resource lock object).
        /// </summary>
        static ThreadedConsoleLog()
        {
            ThreadedConsoleLog._lock = new Object();
        }
        #endregion // Constructor

        #region ThreadedLogTargetBase Methods
        /// <summary>
        /// Invoked by worker thread to actually handle the messages.
        /// </summary>
        /// <param name="args"></param>
        protected override void LogMessage(LogMessageEventArgs args)
        {
            if (!this.Enabled)
                return;
            
            TextWriter writer = (args.Level <= LogLevel.Error) ? Console.Error : Console.Out;
            lock (ThreadedConsoleLog._lock)
            {
                if (this.Timestamp)
                    writer.Write(args.Timestamp.ToLocalTime().ToString() + " ");

                // Message Prefix; then level-dependent info.
                WritePrefix(writer, MessageColours[args.Level], MessagePrefixes[args.Level], args.Context);
                switch (args.Level)
                {
                    case LogLevel.Profile:
                        {
                            // New profiling context.
                            Pair<String, Stopwatch> p = new Pair<String, Stopwatch>(args.Message, new Stopwatch());
                            p.Second.Start();
                            this._contexts.Push(p);
                            writer.WriteLine(args.Message);
                        }
                        break;
                    case LogLevel.ProfileEnd:
                        {
                            // End profiling context.
                            Pair<String, Stopwatch> p = this._contexts.Pop();
                            p.Second.Stop();
                            writer.WriteLine("'{0}' took: {1}", p.First, p.Second.Elapsed.ToHumanReadableString());
                        }
                        break;
                    default:
                        writer.WriteLine(args.Message);
                        break;
                }
            }
        }

        /// <summary>
        /// Empty method as console messages aren't buffered
        /// </summary>
        public override void Flush()
        {
            // Empty as no need to save to disk
        }
        #endregion // ThreadedLogTargetBase Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="col"></param>
        /// <param name="prefix"></param>
        /// <param name="context"></param>
        private void WritePrefix(TextWriter writer, ConsoleColor col, String prefix, String context)
        {
            // Prefixes line up vertically much nicer if we pad left with spaces.
            // Make log reading much easier on the eyes.
            String paddedBracket = "[".PadLeft(System.Math.Max(_maxPadding - prefix.Length, 0));
            writer.Write(paddedBracket);

            Console.ForegroundColor = col;
            writer.Write(prefix);
            Console.ResetColor();
            writer.Write("] ");

            if (ShowContext)
            {
                writer.Write("[");
                Console.ForegroundColor = ConsoleColor.DarkGray;
                writer.Write(context);
                Console.ResetColor();
                writer.Write("] ");
            }
        }
        #endregion // Private Methods
    }

} // RSG.Base.Logging namespace
