﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace RSG.Base.Logging
{

    /// <summary>
    /// 
    /// </summary>
    public interface IExceptionFormatter
    {
        #region Properties
        /// <summary>
        /// Exception being formatted.
        /// </summary>
        Exception ThrownException { get; }

        /// <summary>
        /// Exception message.
        /// </summary>
        String Message { get; }

        /// <summary>
        /// Exception source.
        /// </summary>
        String Source { get; }

        /// <summary>
        /// Exception stacktrace.
        /// </summary>
        StackTrace Trace { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Format this exception for logs, dialogs etc.
        /// </summary>
        /// <returns></returns>
        IEnumerable<String> Format();

        /// <summary>
        /// Format this exception as heading information.
        /// </summary>
        /// <returns></returns>
        String FormatHeading();

        /// <summary>
        /// Format this exception as summary information.
        /// </summary>
        /// <returns></returns>
        IEnumerable<String> FormatSummary();
        #endregion // Methods
    }

} // RSG.Base.Logging namespace
