﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using System.IO;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using RSG.Base.Collections;
using RSG.Base.Extensions;
using RSG.Base.Attributes;

namespace RSG.Base.Logging.Universal
{

	/// <summary>
	/// Universal log file target.
	/// </summary>
	/// An instance of this object can be connected to an ILog instance by
	/// connecting up the Log method handlers.  When log messages are then
	/// generated they get written to the file.
	/// 
	/// https://devstar.rockstargames.com/wiki/index.php/Dev:Universal_Log_Format
	/// 
	public sealed class UniversalLogFile :
		LogTargetBase,
		IUniversalLogTarget,
		ILogFileTarget 
	{
		#region Constants
		/// <summary>
		/// Default filename extension String for Universal Log Files.
		/// </summary>
		public static readonly String EXTENSION = "ulog";

		/// <summary>
		/// File size byte limit; before roll.
		/// </summary>
        private static readonly long FILE_SIZE_LIMIT = 2 * 1024 * 1024; // 2MB

        /// <summary>
        /// Number of messages to buffer before a flush
        /// </summary>
        private const int DEFAULT_MESSAGE_BUFFER_SIZE = 1000;

		/// <summary>
		/// Default Universal Log XML schema version.
		/// </summary>
		private static readonly String DEFAULT_VERSION = "1.0";

		/// <summary>
		/// Default Universal Log context string.
		/// </summary>
		public static readonly String DEFAULT_CONTEXT = "Default";

		/// <summary>
		/// Default XML version.
		/// </summary>
		private static readonly String DEFAULT_XML_VERSION = "1.0";

		/// <summary>
		/// Default Universal Log XML encoding.
		/// </summary>
		private static readonly String DEFAULT_ENCODING = "utf-8";

		/// <summary>
		/// Default Univeral Log XML document root node name.
		/// </summary>
		private static readonly String DEFAULT_ROOT_ELEM = "ulog";

		/// <summary>
		/// XML document context attribute name.
		/// </summary>
		private static readonly String CONTEXT_ELEM = "context";

		/// <summary>
		/// Append millisecond pattern to current culture's full date time pattern
		/// </summary>
		private static readonly String fullTimePattern = Regex.Replace(DateTimeFormatInfo.CurrentInfo.FullDateTimePattern, "(:ss|:s)", "$1.fff");
		#endregion // Constants

		#region Properties
		/// <summary>
		/// Log file flush mode.
		/// </summary>
		public FlushMode FlushMode
		{
			get;
			private set;
		}

		/// <summary>
		/// Log filename on disk.
		/// </summary>
		public String Filename
		{
			get;
			private set;
		}
        
		/// <summary>
		/// Number of messages to buffer prior to flush.
		/// </summary>
		public int BufferSize
		{
			get;
			private set;
		}
		#endregion // Properties

		#region Member Data
		/// <summary>
		/// Internal message buffer.
		/// </summary>
		private ICollection<UniversalLogFileBufferedMessage> m_BufferedMessages;

		/// <summary>
		/// Rolling counter; to easily get new filename.
		/// </summary>
		private uint m_RollingCount;

		/// <summary>
		/// Profile message context stack.
		/// </summary>
		private Stack<String> m_ProfileContextStack;
		#endregion // Member Data

		#region Constructor(s) / Destructor
		/// <summary>
		/// Constructor; specifying flush mode and initial log connection.
		/// </summary>
		/// <param name="filename"></param>
		/// <param name="appendMode"></param>
		/// <param name="flushMode"></param>
		/// <param name="log"></param>
		/// <param name="bufferSize"></param>
		public UniversalLogFile(String filename, FileMode appendMode,
            FlushMode flushMode, IUniversalLog log, int bufferSize = DEFAULT_MESSAGE_BUFFER_SIZE)
		{
			Init(filename, appendMode, flushMode, bufferSize);
			Connect(log);
		}
		#endregion // Constructor(s) / Destructor

		#region ILogFileTarget Interface Methods
		/// <summary>
		/// Flush the buffered messages to disk.
		/// </summary>
		public override void Flush()
		{
			this.Flush(false);
		}

		/// <summary>
		/// Dispose of the log file; flushing data to disk.
		/// </summary>
		public void Dispose()
		{
			this.Flush();
		}
		#endregion // ILogFileTarget Interface Methods

		#region Controller Methods
		/// <summary>
		/// Connect to a log object.
		/// </summary>
		/// <param name="log"></param>
		public override void Connect(ILog log)
		{
			if (null == log)
				return;

			log.LogMessage += this.LogMessage;
			log.LogError += this.LogError;
            log.LogToolException += this.LogToolException;
            log.LogToolError += this.LogToolError;
			log.LogWarning += this.LogWarning;
			log.LogProfile += this.LogProfile;
			log.LogProfileEnd += this.LogProfileEnd;
#if DEBUG
			log.LogDebug += this.LogDebug;
#endif // DEBUG

			base.Connect(log);
		}

		/// <summary>
		/// Connect to a log object (up to a particular level).
		/// </summary>
		/// <param name="log"></param>
		/// <param name="uptoLevel"></param>
		public override void Connect(ILog log, LogLevel uptoLevel)
		{
            if ((int)uptoLevel >= (int)LogLevel.ToolException)
                log.LogToolException += this.LogToolException;
            if ((int)uptoLevel >= (int)LogLevel.ToolError)
                log.LogToolError += this.LogToolError;
			if ((int)uptoLevel >= (int)LogLevel.Error)
				log.LogError += this.LogError;
			if ((int)uptoLevel >= (int)LogLevel.Warning)
				log.LogWarning += this.LogWarning;
			if ((int)uptoLevel >= (int)LogLevel.Info)
				log.LogMessage += this.LogMessage;
			if ((int)uptoLevel >= (int)LogLevel.Profile)
			{
				log.LogProfile += this.LogProfile;
				log.LogProfileEnd += this.LogProfileEnd;
			}
			if ((int)uptoLevel >= (int)LogLevel.Debug)
				log.LogDebug += this.LogDebug;

			base.Connect(log, uptoLevel);
		}

		/// <summary>
		/// Disconnect from a log object.
		/// </summary>
		/// <param name="log"></param>
		public override void Disconnect(ILog log)
		{
			log.LogMessage -= this.LogMessage;
			log.LogError -= this.LogError;
            log.LogToolException -= this.LogToolException;
            log.LogToolError -= this.LogToolError;
			log.LogWarning -= this.LogWarning;
			log.LogProfile -= this.LogProfile;
			log.LogProfileEnd -= this.LogProfileEnd;
#if DEBUG
			log.LogDebug -= this.LogDebug;
#endif // DEBUG

			base.Disconnect(log);
		}
		#endregion // Controller Methods

		#region Static Controller Methods
		/// <summary>
		/// Merge Universal Log files; saving resultant XML to disk.
		/// </summary>
        /// <param name="destination">File path to save the resulting merged log file.</param>
		/// <param name="filenames">List of file paths to merge together.</param>
		/// <returns>Success of the merge operation.</returns>
		public static bool MergeLogs(String destination, params String[] filenames)
		{
			bool result = true;
			try
			{
				IEnumerable<XDocument> docs = filenames.Select(f => XDocument.Load(f));
				XDocument xmlDoc = new XDocument(
					new XDeclaration("1.0", "utf-8", "yes"),
					new XElement("ulog"));

                // Merge the contents of context's that share the same name.
                IDictionary<String, XElement> mergedContextElements = new Dictionary<String, XElement>();
                foreach (XElement currentContextElem in docs.SelectMany(d => d.Root.Elements()))
                {
                    String contextName = DEFAULT_CONTEXT;
                    XAttribute nameAtt = currentContextElem.Attribute("name");
                    if (nameAtt != null && !String.IsNullOrEmpty(nameAtt.Value))
                    {
                        contextName = nameAtt.Value;
                    }

                    // Check whether we've already got a context using this name.
                    XElement existingContextElem;
                    if (mergedContextElements.TryGetValue(contextName, out existingContextElem))
                    {
                        existingContextElem.Add(currentContextElem.Elements());
                    }
                    else
                    {
                        mergedContextElements[contextName] = currentContextElem;
                    }
                }

                xmlDoc.Root.Add(mergedContextElements.Values);
				xmlDoc.Save(destination);
			}
			catch
			{
				result = false;
			}
			
			return (result);
		}

		/// <summary>
		/// Return whether the specified log filename contains errors.
		/// </summary>
		/// <param name="filename"></param>
		/// <returns></returns>
		public static bool ContainsErrors(String filename)
		{
			try
			{
				XDocument xmlDoc = XDocument.Load(filename);
				return GetAllErrorElements(xmlDoc).Any();
			}
			catch
			{
                return false;
			}
		}

		/// <summary>
		/// Return whether the specified log filename contains errors.
		/// </summary>
		/// <param name="filename"></param>
		/// <returns></returns>
		public static bool ContainsWarnings(String filename)
		{
			try
			{
				XDocument xmlDoc = XDocument.Load(filename);
                return GetAllWarningElements(xmlDoc).Any();
			}
			catch
			{
                return false;
			}
		}

		/// <summary>
		///  Returns the number of errors in the file.
		/// </summary>
		/// <param name="filename"></param>
		/// <returns></returns>
		public static int GetNumErrors(String filename)
		{
			try
			{
				XDocument xmlDoc = XDocument.Load(filename);
                int totalNumErrors = GetAllErrorElements(xmlDoc).Count();
                return totalNumErrors;
			}
			catch
			{
				return 0;
			}
		}

		/// <summary>
		/// Returns the number of warnings in the file.
		/// </summary>
		/// <param name="filename"></param>
		/// <returns></returns>
		public static int GetNumWarnings(String filename)
		{
			try
			{
				XDocument xmlDoc = XDocument.Load(filename);
                int totalNumWarnings = GetAllWarningElements(xmlDoc).Count();
                return totalNumWarnings;
			}
			catch
			{
				return 0;
			}
		}

        /// <summary>
        /// Return list of paired context/messages's from the log file.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="errorcount"></param>
        /// <returns></returns>
        public static IEnumerable<Pair<String, String>> GetMessages(String filename, Int32 errorcount = -1)
        {
            List<Pair<String, String>> results = new List<Pair<String, String>>();
            try
            {
                XDocument xmlDoc = XDocument.Load(filename);
                Int32 count = 0;
                foreach (XElement elem in GetAllMessageElements(xmlDoc))
                {
                    if (count < errorcount || errorcount == -1)
                    {
                        String context = "No Context";
                        if (elem.Attribute("context") != null)
                        {
                            context = elem.Attribute("context").Value;
                        }
                        results.Add(new Pair<String, String>(context, elem.Value));
                        count++;
                    }
                    else
                        break;
                }
            }
            catch
            {
                return (results);
            }
            return (results);
        }

		/// <summary>
		/// Return list of paired context/error's from the log file.
		/// </summary>
		/// <param name="filename"></param>
		/// <param name="errorcount"></param>
		/// <returns></returns>
		public static IEnumerable<Pair<String,String>> GetErrors(String filename, Int32 errorcount = -1)
		{
			List<Pair<String, String>> results = new List<Pair<String, String>>();
			try
			{
				XDocument xmlDoc = XDocument.Load(filename);
				Int32 count = 0;
                foreach (XElement elem in GetAllErrorElements(xmlDoc))
				{
					if (count < errorcount || errorcount == -1)
					{
						String context = "No Context";
						if (elem.Attribute("context") != null)
						{
							context = elem.Attribute("context").Value;
						}
						results.Add(new Pair<String, String>(context, elem.Value));
						count++;
					}
					else
						break;
				}
			}
			catch
			{
				return (results);
			}
			return (results);
		}

		/// <summary>
		///
		/// </summary>
		/// <param name="filename"></param>
		/// <param name="errorcount"></param>
		/// <returns></returns>
		public static IEnumerable<Pair<String, String>> GetErrors(String filename, String specificContext, Int32 errorcount = -1)
		{
			List<Pair<String, String>> results = new List<Pair<String, String>>();
			try
			{
				XDocument xmlDoc = XDocument.Load(filename);
				Int32 count = 0;
                foreach (XElement elem in GetAllErrorElements(xmlDoc))
				{
					XElement parentElement = elem.Parent;
					if (parentElement != null && parentElement.Name != null && parentElement.Name.LocalName == "context")
					{
						if (parentElement.Attribute("name") != null && parentElement.Attribute("name").Value == specificContext)
						{
							if (count < errorcount || errorcount == -1)
							{
								String context = "No Context";
								if (elem.Attribute("context") != null)
								{
									context = elem.Attribute("context").Value;
								}
								results.Add(new Pair<String, String>(context, elem.Value));
								count++;
							}
							else
								break;
						}
					}
				}
			}
			catch
			{
				return (results);
			}
			return (results);
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bufferedMessages"></param>
        /// <param name="xmlDoc"></param>
        /// <returns></returns>
        public static XDocument TransformToXDocument(ICollection<UniversalLogFileBufferedMessage> bufferedMessages, XDocument xmlDoc = null)
        {
            XDocument outXmlDoc = null;
            lock (bufferedMessages)
			{
                // Create new document if it failed to load or was requested to
                // be created from scratch.
                if (null == xmlDoc)
                {
                    xmlDoc = new XDocument(
                        new XDeclaration(DEFAULT_XML_VERSION, DEFAULT_ENCODING, "yes"),
                        new XElement(DEFAULT_ROOT_ELEM,
                            new XAttribute("version", DEFAULT_VERSION),
                            new XAttribute("name", Environment.UserName),
                            new XAttribute("machine", Environment.MachineName),
                            new XElement(CONTEXT_ELEM,
                                new XAttribute("name", DEFAULT_CONTEXT)
                            ) // context
                        ) // ulog
                    );
                }

                foreach (UniversalLogFileBufferedMessage message in bufferedMessages)
                {
                    // Find or create the default context element.
                    XElement xmlContextElem = xmlDoc.Root.XPathSelectElement(
                        String.Format("/{0}/context[@name=\"{1}\"]",
                        DEFAULT_ROOT_ELEM, DEFAULT_CONTEXT));

                    if (null == xmlContextElem)
                    {
                        xmlContextElem = new XElement("context",
                            new XAttribute("name", DEFAULT_CONTEXT));
                        xmlDoc.Root.Add(xmlContextElem);
                    }

                    XElement xmlMessageElem = message.ToXml();
                    if (null != xmlMessageElem)
                        xmlContextElem.Add(xmlMessageElem);
                }
            }
            outXmlDoc = xmlDoc;
            return (outXmlDoc);
        }
        
        /// <summary>
        /// 
        /// </summary>
        public static XDocument TransformToXDocument(IDictionary<String, IList<UniversalLogFileBufferedMessage>> contextualisedMessages)
        {
            return
                new XDocument(
                    new XDeclaration(DEFAULT_XML_VERSION, DEFAULT_ENCODING, "yes"),
                    new XElement(DEFAULT_ROOT_ELEM,
                        new XAttribute("version", DEFAULT_VERSION),
                        new XAttribute("name", Environment.UserName),
                        new XAttribute("machine", Environment.MachineName),
                        contextualisedMessages.Select(ctxPair => new XElement("context",
                            new XAttribute("name", ctxPair.Key),
                            ctxPair.Value.Select(msg => msg.ToXml()))
                        )
                    )
                );
        }

        /// <summary>
        /// Build a list of UniversalLogFileBufferedMessages from an XDocument representation of a UniversalLogFile.
        /// </summary>
        /// <param name="xdoc"></param>
        /// <returns></returns>
        public static IEnumerable<UniversalLogFileBufferedMessage> GetMessageList(XDocument xdoc)
        {
            IEnumerable<UniversalLogFileBufferedMessage> messageList = new List<UniversalLogFileBufferedMessage>();
            foreach (XElement elem in xdoc.Descendants("context"))
            {
                IEnumerable<XElement> xmlMessages = elem.XPathSelectElements("./*");
                IEnumerable<UniversalLogFileBufferedMessage> contextmsgs = xmlMessages.Select(msg => new UniversalLogFileBufferedMessage(msg));
                (messageList as List<UniversalLogFileBufferedMessage>).AddRange(contextmsgs);
            }
            return messageList;
        }

        /// <summary>
        /// Retrieves the list of context/UniversalLogFileBufferedMessages pairs from an XDocument representation of a UniversalLogFile.
        /// </summary>
        /// <param name="xdoc"></param>
        /// <returns></returns>
        public static IDictionary<String, IList<UniversalLogFileBufferedMessage>> GetContextualisedMessageList(XDocument xdoc)
        {
            IDictionary<String, IList<UniversalLogFileBufferedMessage>> messages = new Dictionary<String, IList<UniversalLogFileBufferedMessage>>();

            foreach (XElement elem in xdoc.Descendants("context"))
            {
                String systemContext = DEFAULT_CONTEXT;
                XAttribute nameAtt = elem.Attribute("name");
                if (nameAtt != null)
                {
                    systemContext = nameAtt.Value;
                }

                IEnumerable<XElement> xmlMessages = elem.XPathSelectElements("./*");
                IEnumerable<UniversalLogFileBufferedMessage> contextmsgs = xmlMessages.Select(msg =>
                    new UniversalLogFileBufferedMessage(msg));

                IList<UniversalLogFileBufferedMessage> msgList;
                if (!messages.TryGetValue(systemContext, out msgList))
                {
                    msgList = new List<UniversalLogFileBufferedMessage>();
                    messages[systemContext] = msgList;
                }
                msgList.AddRange(contextmsgs);
            }

            return messages;
        }
		#endregion // Static Controller Methods

		#region Private Methods
		#region Message Event Handler Methods
		/// <summary>
		/// Information message event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void LogMessage(Object sender, LogMessageEventArgs e)
		{
			LogMessage(LogLevel.Info, e);
		}

		/// <summary>
		/// Profile message event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void LogProfile(Object sender, LogMessageEventArgs e)
		{
			this.m_ProfileContextStack.Push(e.Context);
			LogMessage(LogLevel.Profile, e);
		}

		/// <summary>
		/// Profile end message event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
        private void LogProfileEnd(Object sender, LogMessageEventArgs e)
		{
			LogMessage(LogLevel.ProfileEnd, e);
			this.m_ProfileContextStack.Pop();
		}

		/// <summary>
		/// Warning message event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void LogWarning(Object sender, LogMessageEventArgs e)
		{
			LogMessage(LogLevel.Warning, e);
		}

		/// <summary>
		/// Error message event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void LogError(Object sender, LogMessageEventArgs e)
		{
			LogMessage(LogLevel.Error, e);
		}

        /// <summary>
        /// Tool internal-exception event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LogToolException(Object sender, LogMessageEventArgs e)
        {
            LogMessage(LogLevel.ToolException, e);
        }

        /// <summary>
        /// Tool internal-error event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LogToolError(Object sender, LogMessageEventArgs e)
        {
            LogMessage(LogLevel.ToolError, e);
        }

		/// <summary>
		/// Debug message event handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void LogDebug(Object sender, LogMessageEventArgs e)
		{
			LogMessage(LogLevel.Debug, e);
		}
		#endregion // Message Event Handler Methods

        /// <summary>
        /// Get all the xml elements considered a kind of 'error'
        /// - inclusive of tool errors.
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <returns></returns>
        private static IEnumerable<XElement> GetAllErrorElements(XDocument xmlDoc)        
        {
            // maybe one day we will consider adding exceptions here too?
            LogLevel[] logLevels = { LogLevel.Error, LogLevel.ToolError };
            return GetAllLogLevelElements(xmlDoc, logLevels);
        }

        /// <summary>
        /// Get all the xml elements considered a kind of 'message'.
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <returns></returns>
        private static IEnumerable<XElement> GetAllMessageElements(XDocument xmlDoc)
        {
            LogLevel[] logLevels = { LogLevel.Info };
            return GetAllLogLevelElements(xmlDoc, logLevels);
        }

        /// <summary>
        /// Get all the xml elements considered a kind of 'warning'.
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <returns></returns>
        private static IEnumerable<XElement> GetAllWarningElements(XDocument xmlDoc)
        {
            LogLevel[] logLevels = { LogLevel.Warning };
            return GetAllLogLevelElements(xmlDoc, logLevels);
        }

        /// <summary>
        /// Gets all log elements of the LogLevels passed.
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="logLevels">the log levels we are looking for</param>
        /// <returns></returns>
        private static IEnumerable<XElement> GetAllLogLevelElements(XDocument xmlDoc, LogLevel[] logLevels)
        {
            ICollection<XElement> allElements = new List<XElement>();

            foreach (LogLevel logLevel in logLevels)
            {
                XmlConstantAttribute attr = logLevel.GetAttributeOfType<XmlConstantAttribute>();
                if (attr != null)
                {
                    IEnumerable<XElement> elements = xmlDoc.XPathSelectElements(String.Format("//{0}", attr.Constant));

                    if (elements != null && elements.Any())
                        allElements.AddRange(elements);
                }
            }
            return allElements;
        }

		/// <summary>
		/// Local initialisation method.
		/// </summary>
		/// <param name="filename"></param>
		/// <param name="appendMode"></param>
		/// <param name="flushMode"></param>
		/// <param name="bufferSize"></param>
		private void Init(String filename, FileMode appendMode, FlushMode flushMode,
			int bufferSize)
		{
			this.Filename = filename;
			this.FlushMode = flushMode;
			this.BufferSize = bufferSize;
			this.m_BufferedMessages = new List<UniversalLogFileBufferedMessage>(bufferSize);
			this.m_RollingCount = 0;
			this.m_ProfileContextStack = new Stack<String>();

			String logFileDirectory = Path.GetDirectoryName(filename);
			if (!Directory.Exists(logFileDirectory))
				Directory.CreateDirectory(logFileDirectory);

			switch (appendMode)
			{
				case FileMode.CreateNew:
				case FileMode.Create:
					this.Flush(true);
					break;
			}
		}

		/// <summary>
		/// Flush; optionally creating a base Universal Log file.
		/// </summary>
		/// <param name="create"></param>
		private void Flush(bool create)
		{
#if UNIVERSAL_LOG_FILE_FLUSH_DEBUG
			int bufferedCount = this.m_BufferedMessages.Count;
			Stopwatch sw = new Stopwatch();
			sw.Start();
#endif // UNIVERSAL_LOG_FILE_FLUSH_DEBUG
            XDocument xmlDoc = null;
            if (!create && File.Exists(this.Filename))
            {
                try
                {
                    // Try loading the document.
                    xmlDoc = XDocument.Load(this.Filename);
                }
                catch
                {
                    try
                    {
                        // Try moving the invalid/corrupt file.  Ensure
                        // we catch... if that fails it will get overwritten
                        // in the next step.  Such as life.
                        String invalidFilename = Path.ChangeExtension(
                            this.Filename, "_CORRUPT.ulog");
                        File.Move(this.Filename, invalidFilename);
                    }
                    catch { }
                }
            }

            // Locking around our Flush; ensures we're not adding/removing from the
            // buffered messages during a flush operation.
			lock (this.m_BufferedMessages)
            {
                xmlDoc = UniversalLogFile.TransformToXDocument(this.m_BufferedMessages, xmlDoc);

                // Found a case where the log directory gets blown away before flushing the file so re-create the directory.
                if (!Directory.Exists(Path.GetDirectoryName(this.Filename)))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(this.Filename));
                }

				try
				{
					xmlDoc.Save(this.Filename);
					this.m_BufferedMessages.Clear();

					FileInfo fi = new FileInfo(this.Filename);
					if (fi.Length >= FILE_SIZE_LIMIT)
					{
						String directory = Path.GetDirectoryName(this.Filename);
						String filename = Path.GetFileName(this.Filename);
						filename = Path.ChangeExtension(filename, String.Format("{0}.{1}",
							this.m_RollingCount++, EXTENSION));
						String archivedFilename = Path.Combine(directory, filename);
						fi.MoveTo(archivedFilename);
					}
				}
				catch
				{
					// Catch the case of the file already being open.
					// This was happening when the garbage collection was calling finalize which would flush the file
					// but another flush was being called on another thread and causing a crash.
					// This shouldn't clear the buffered messages so hopefully on the next flush it will properly get everything out.
				}
			} // lock(this.Filename)

#if UNIVERSAL_LOG_FILE_FLUSH_DEBUG
			sw.Stop();
			Console.WriteLine("UniversalLogFile::Flush() of {0} messages took {1}ms",
				bufferedCount, sw.ElapsedMilliseconds);
#endif // UNIVERSAL_LOG_FILE_FLUSH_DEBUG
		}

		/// <summary>
		/// Internally buffer a message.
		/// </summary>
		/// <param name="level"></param>
		/// <param name="e"></param>
		private void LogMessage(LogLevel level, LogMessageEventArgs e)
		{
			bool flush = false;
			String context = e.Context;
			switch (level)
			{
                case LogLevel.ToolException:
                case LogLevel.ToolError:
				case LogLevel.Error:
					flush = (this.FlushMode.HasFlag(FlushMode.Error));
					break;
				case LogLevel.Warning:
					flush = (this.FlushMode.HasFlag(FlushMode.Warning));
					break;
				case LogLevel.Info:                    
					flush = (this.FlushMode.HasFlag(FlushMode.Info));
					break;
				case LogLevel.Debug:
					flush = (this.FlushMode.HasFlag(FlushMode.Debug));
					break;
				case LogLevel.Profile:
					flush = (this.FlushMode.HasFlag(FlushMode.Profile));
					break;
				case LogLevel.ProfileEnd:
					flush = (this.FlushMode.HasFlag(FlushMode.Profile));
					context = this.m_ProfileContextStack.Peek();
					break;
			}

			lock (this.m_BufferedMessages)
			{
				UniversalLogFileBufferedMessage message = new UniversalLogFileBufferedMessage(
					e.Level, context, e.Message, e.PreformattedMessage, e.Timestamp);
				this.m_BufferedMessages.Add(message);

				// Determine whether we need to flush the log; based on the buffered
				// message count or our specified flush mode.
				if (flush || (this.m_BufferedMessages.Count > this.BufferSize))
					this.Flush();
			}
		}
		#endregion // Private Methods

	} // UniversalLogFile

} // RSG.Base.Logging.Universal namespace
