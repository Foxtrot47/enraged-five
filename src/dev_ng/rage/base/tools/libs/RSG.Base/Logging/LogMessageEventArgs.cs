//---------------------------------------------------------------------------------------------
// <copyright file="LogMessageEventArgs.cs" company="Rockstar Games">
//     Copyright � Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Base.Logging
{
    using System;

    /// <summary>
    /// Regular message event argument class.
    /// </summary>
    public class LogMessageEventArgs : EventArgs
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="Level"/> property.
        /// </summary>
        private readonly LogLevel _level;

        /// <summary>
        /// Private field for the <see cref="Message"/> property.
        /// </summary>
        private readonly String _message;

        /// <summary>
        /// Private field for the <see cref="PreformattedMessage"/> property.
        /// </summary>
        private readonly String _preformattedMessage;

        /// <summary>
        /// Private field for the <see cref="Context"/> property.
        /// </summary>
        private readonly String _context;

        /// <summary>
        /// Private field for the <see cref="Timestamp"/> property.
        /// </summary>
        private readonly DateTime _timestamp;
        #endregion

        #region Properties
        /// <summary>
        /// Message log level.
        /// </summary>
        public LogLevel Level
        {
            get { return _level; }
        }

        /// <summary>
        /// Message string to be logged
        /// </summary>
        public String Message
        {
            get { return _message; }
        }

        /// <summary>
        /// Performatted message string being logged.
        /// </summary>
        public String PreformattedMessage
        {
            get { return _preformattedMessage; }
        }

        /// <summary>
        /// Context string log message relates to (optional).
        /// </summary>
        public String Context
        {
            get { return _context; }
        }

        /// <summary>
        /// Time when this message was created.
        /// </summary>
        public DateTime Timestamp
        {
            get { return _timestamp; }
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor, with context string.
        /// </summary>
        /// <param name="level">Message log level.</param>
        /// <param name="context">Message context.</param>
        /// <param name="message">Message to be logged</param>
        /// <param name="preformattedMessage">Preformatted message being logged.</param>
        public LogMessageEventArgs(LogLevel level, String context, String message, String preformattedMessage)
        {
            this._level = level;
            this._context = context;
            this._message = message;
            this._preformattedMessage = preformattedMessage;
            this._timestamp = DateTime.UtcNow;
        }
        #endregion // Constructor(s)
    }
} // End of RSG.Base.Logging namespace

// End of file
