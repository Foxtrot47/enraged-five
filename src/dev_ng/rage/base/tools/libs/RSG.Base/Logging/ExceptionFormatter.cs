﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using RSG.Base.Extensions;

namespace RSG.Base.Logging
{

    /// <summary>
    /// Exception formatter; produce nice output from Exceptions.
    /// </summary>
    /// Note: you could go to town on this and create a nice extensible class
    /// that can handle additional exception classes easily.
    /// 
    public class ExceptionFormatter : IExceptionFormatter
    {
        #region Constants
        private const int DEFAULT_SKIP_FRAMES = 0;
        #endregion // Constants

        #region Delegates
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        public delegate IEnumerable<String> FormatExceptionHandler(Exception ex);
        #endregion // Delegates

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public Exception ThrownException
        {
            get;
            private set;
        }

        /// <summary>
        /// Exception message.
        /// </summary>
        public String Message
        {
            get { return this.ThrownException.Message; }
        }

        /// <summary>
        /// Exception source.
        /// </summary>
        public String Source
        {
            get { return this.ThrownException.Source; }
        }

        /// <summary>
        /// Exception stacktrace.
        /// </summary>
        public StackTrace Trace
        {
            get;
            private set;
        }

        /// <summary>
        /// Assembly that created the formatter (used internally).
        /// </summary>
        private Assembly CallingAssembly
        {
            get;
            set;
        }
        #endregion // Properties
        
        #region Static Member Data
        /// <summary>
        /// Exception handler callbacks keyed by exception class.
        /// </summary>
        static IDictionary<Type, FormatExceptionHandler> ms_Handlers;
        #endregion // Static Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor; taking thrown exception.
        /// </summary>
        /// <param name="ex">Thrown exception.</param>
        /// 
        public ExceptionFormatter(Exception ex)
            : this(ex, DEFAULT_SKIP_FRAMES)
        {
        }

        /// <summary>
        /// Constructor; taking thrown exception and number of stack frames to
        /// skip for stack trace.
        /// </summary>
        /// <param name="ex">Thrown exception.</param>
        /// <param name="skipFrames">Number of stack frames to skip.</param>
        /// 
        public ExceptionFormatter(Exception ex, int skipFrames)
        {
            this.ThrownException = ex;
            this.Trace = new StackTrace(this.ThrownException, skipFrames, true);
            this.CallingAssembly = Assembly.GetCallingAssembly();
        }

        /// <summary>
        /// Static constructor; registering default handlers.
        /// </summary>
        static ExceptionFormatter()
        {
            ms_Handlers = new Dictionary<Type, FormatExceptionHandler>();
            RegisterExceptionFormatter<Exception>(ExceptionHandler);
            RegisterExceptionFormatter<AggregateException>(AggregateExceptionHandler);
            RegisterExceptionFormatter<ArgumentException>(ArgumentExceptionHandler);
            RegisterExceptionFormatter<FileNotFoundException>(FileNotFoundExceptionHandler);
            RegisterExceptionFormatter<FileLoadException>(FileLoadExceptionHandler);
            RegisterExceptionFormatter<CompositionException>(CompositionExceptionHandler);
            RegisterExceptionFormatter<ReflectionTypeLoadException>(ReflectionTypeLoadExceptionHandler);
            RegisterExceptionFormatter<Win32Exception>(Win32ExceptionHandler);
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Format this exception for logs, dialogs etc.
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        public IEnumerable<String> Format()
        {
            List<String> messages = new List<String>();
            messages.AddRange(this.FormatSummary());

            if (!String.IsNullOrEmpty(this.ThrownException.HelpLink))
            {
                messages.Add("Helplink:");
                messages.Add(String.Format("\t{0}", this.ThrownException.HelpLink));
            }
            if (this.ThrownException.Data != null && this.ThrownException.Data.Count > 0)
            {
                messages.Add("Details:");
                foreach (DictionaryEntry de in this.ThrownException.Data)
                {
                    messages.Add(String.Format("\t'{0}' - {1}", de.Key, de.Value));
                }
            }

            IEnumerable<String> additionalMessages = 
                FormatExceptionFactory(this.ThrownException);
            if (additionalMessages.Count() > 0)
            {
                messages.Add("Additional:");
                foreach (String additional in additionalMessages)
                    messages.Add(String.Format("\t{0}", additional));
            }
                
            // Stacktrace.
            if (null != this.Trace)
            {
                messages.Add("Stacktrace:");
                for (int i = 0; i < this.Trace.FrameCount; ++i)
                {
                    // Display the stack frame properties.
                    StackFrame sf = this.Trace.GetFrame(i);
                    MethodBase mb = sf.GetMethod();
                    string classTypeName = (mb.ReflectedType != null ? mb.ReflectedType.Name : "???");

                    StringBuilder stackFrame = new StringBuilder();
                    stackFrame.AppendFormat("\t{0} ({1}, {2}): {3}.{4}",
                        sf.GetFileName(),
                        sf.GetFileLineNumber(),
                        sf.GetFileColumnNumber(),
                        classTypeName,
                        mb.Name);

                    if (StackFrame.OFFSET_UNKNOWN != sf.GetILOffset())
                        stackFrame.AppendFormat("+IL{0}", sf.GetILOffset());
                    else if (StackFrame.OFFSET_UNKNOWN != sf.GetNativeOffset())
                        stackFrame.AppendFormat("+N{0}", sf.GetNativeOffset());
                    stackFrame.AppendLine();
                    messages.Add(stackFrame.ToString());
                }
            }
            else
            {
                messages.Add(String.Format("\tNo stack trace available."));
            }

            return (messages.ToArray());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public String FormatHeading()
        {
            Assembly entryAssembly = Assembly.GetEntryAssembly();
            if (null == entryAssembly)
                entryAssembly = this.CallingAssembly;
            
            return (String.Format("{0} exception in {1}, version: {2} at {3}.",
                ThrownException.GetType().Name,
                entryAssembly.GetName().Name, entryAssembly.GetName().Version,
                DateTime.Now));
        }

        /// <summary>
        /// Format this exception as summary information.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<String> FormatSummary()
        {
            Assembly entryAssembly = Assembly.GetEntryAssembly();
            if (null == entryAssembly)
                entryAssembly = this.CallingAssembly;
            ICollection<String> summary = new List<String>();

            summary.Add(String.Format("{0} exception in {1}, version: {2} at {3}.",
                ThrownException.GetType().Name,
                entryAssembly.GetName().Name, entryAssembly.GetName().Version,
                DateTime.Now));
            summary.Add("Exception message:");
            summary.Add(String.Format("\t{0}", this.Message));
            summary.Add("Exception source:");
            summary.Add(String.Format("\t{0}", this.Source));
            
            return (summary);
        }
        #endregion // Controller Methods

        #region Static Factory Methods
        /// <summary>
        /// Register a new exception formatter callback.
        /// </summary>
        /// <param name="callback"></param>
        public static void RegisterExceptionFormatter<T>(FormatExceptionHandler callback)
            where T : Exception
        {
            Type exceptionClass = typeof(T);
            Debug.Assert(!ms_Handlers.ContainsKey(exceptionClass),
                String.Format("Exception class '{0}' already registered.", exceptionClass.ToString()));
            if (ms_Handlers.ContainsKey(exceptionClass))
                throw (new ArgumentException("exceptionClass"));

            ms_Handlers.Add(exceptionClass, callback);
        }

        /// <summary>
        /// Unregister an exception formatter callback.
        /// </summary>
        public static void UnregisterExceptionFormatter<T>()
            where T : Exception
        {
            Type exceptionClass = typeof(T);
            Debug.Assert(ms_Handlers.ContainsKey(exceptionClass),
                String.Format("Exception class '{0}' not registered.", exceptionClass.ToString()));
            if (!ms_Handlers.ContainsKey(exceptionClass))
                throw (new ArgumentException("exceptionClass"));

            ms_Handlers.Remove(exceptionClass);
        }
        #endregion // Static Factory Methods

        #region Static Exception Handler Callbacks
        /// <summary>
        /// AggregateException handler callback.
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        private static IEnumerable<String> AggregateExceptionHandler(Exception ex)
        {
            Debug.Assert(ex is AggregateException, "Invalid AggregateException.");
            if (!(ex is AggregateException))
                throw (new ArgumentException("ex"));

            ICollection<String> information = new List<String>();
            AggregateException aex = (ex as AggregateException);
            foreach (Exception iex in aex.InnerExceptions)
            {
                ExceptionFormatter formatter = new ExceptionFormatter(iex);
                information.Add(new String('-', 80));
                information.AddRange(formatter.Format());
            }
            return (information);
        }

        /// <summary>
        /// ArgumentException handler callback.
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        private static IEnumerable<String> ArgumentExceptionHandler(Exception ex)
        {
            Debug.Assert(ex is ArgumentException, "Invalid ArgumentException.");
            if (!(ex is ArgumentException))
                throw (new ArgumentException("ex"));

            ArgumentException aex = (ArgumentException)ex;
            ICollection<String> information = new List<String>();
            information.Add(String.Format("Parameter: '{0}'.", aex.ParamName));
            return (information);
        }

        /// <summary>
        /// FileNotFoundException handler callback.
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        private static IEnumerable<String> FileNotFoundExceptionHandler(Exception ex)
        {
            Debug.Assert(ex is FileNotFoundException, "Invalid FileNotFoundException.");
            if (!(ex is FileNotFoundException))
                throw (new ArgumentException("ex"));

            FileNotFoundException iex = (ex as FileNotFoundException);
            ICollection<String> information = new List<String>();
            information.Add(String.Format("File not found: '{0}'.", iex.FileName));
            return (information);
        }

        /// <summary>
        /// FileLoadException handler callback.
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        private static IEnumerable<String> FileLoadExceptionHandler(Exception ex)
        {
            Debug.Assert(ex is FileLoadException, "Invalid FileLoadException.");
            if (!(ex is FileLoadException))
                throw (new ArgumentException("ex"));

            FileLoadException iex = (ex as FileLoadException);
            ICollection<String> information = new List<String>();
            information.Add(String.Format("File load fail: '{0}'.", iex.FileName));
            return (information);
        }

        /// <summary>
        /// CompositionException handler callback.
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        private static IEnumerable<String> CompositionExceptionHandler(Exception ex)
        {
            Debug.Assert(ex is CompositionException, "Invalid CompositionException.");
            if (!(ex is CompositionException))
                throw (new ArgumentException("ex"));

            CompositionException cex = (ex as CompositionException);
            IEnumerable<String> messages = cex.Errors.SelectMany(e => FormatExceptionFactory(e.Exception));
            return (messages);
        }

        /// <summary>
        /// ReflectionTypeLoadException handler callback.
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        private static IEnumerable<String> ReflectionTypeLoadExceptionHandler(Exception ex)
        {
            Debug.Assert(ex is ReflectionTypeLoadException, "Invalid ReflectionTypeLoadException.");
            if (!(ex is ReflectionTypeLoadException))
                throw (new ArgumentException("ex"));

            ReflectionTypeLoadException rex = (ReflectionTypeLoadException)ex;
            IEnumerable<String> messages = rex.LoaderExceptions.SelectMany(e => FormatExceptionFactory(e));
            return (messages);
        }

        /// <summary>
        /// ReflectionTypeLoadException handler callback.
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        private static IEnumerable<String> Win32ExceptionHandler(Exception ex)
        {
            Debug.Assert(ex is Win32Exception, "Invalid Win32Exception.");
            if (!(ex is Win32Exception))
                throw (new ArgumentException("ex"));

            Win32Exception win32Exception = (Win32Exception)ex;
            ICollection<String> information = new List<String>();
            information.Add(String.Format("Error code: {0}", win32Exception.ErrorCode));
            information.Add(String.Format("Native Error code: {0}", win32Exception.NativeErrorCode));
            return (information);
        }

        /// <summary>
        /// Exception basetype handler callback.
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        private static IEnumerable<String> ExceptionHandler(Exception ex)
        {
            ICollection<String> information = new List<String>();
            information.Add(String.Format("Unhandled exception: {0}.", ex.GetType().ToString()));
            information.Add(ex.Message);
            return (information);
        }

        /// <summary>
        /// Exception factory; add handlers using RegisterExceptionHandler for custom
        /// Exception classes.
        /// </summary>
        /// <param name="ex">Thrown exception.</param>
        /// <returns></returns>
        private static IEnumerable<String> FormatExceptionFactory(Exception ex)
        {
            ICollection<String> information = new List<String>();
            Type exceptionClass = ex.GetType();

            if (ms_Handlers.ContainsKey(exceptionClass))
            {
                // Invoke our callback to add information.
                IEnumerable<String> info = ms_Handlers[exceptionClass].Invoke(ex);
                information.AddRange(info);
            }
            else
            {
                // If we don't have an exact match we walk its class hierarchy
                // looking for a valid handler.  If we hit Object we abort.
                Type baseType = exceptionClass.BaseType;
                while (typeof(Object) != baseType)
                {
                    if (ms_Handlers.ContainsKey(baseType))
                    {
                        information.AddRange(ms_Handlers[baseType].Invoke(ex));
                        break; // Handled.
                    }
                    baseType = baseType.BaseType; // Iterate.
                }
            }

            // Invoke recursively if we have an InnerException object.
            if (null != ex.InnerException)
            {
                ExceptionFormatter formatter = new ExceptionFormatter(ex.InnerException);
                information.Add(new String('-', 80));
                information.AddRange(formatter.Format());
            }

            return (information);
        }
        #endregion // Static Exception Handler Callbacks
    }

} // RSG.Base.Logging namespace
