﻿using System;

namespace RSG.Base.Extensions
{

    /// <summary>
    /// DateTime struct extension methods.
    /// </summary>
    public static class DateTimeExtensions
    {
        /// <summary>
        /// Determine if this DateTime object is earlier than t2.
        /// </summary>
        /// <param name="t1"></param>
        /// <param name="t2"></param>
        /// <returns></returns>
        public static bool IsEarlierThan(this DateTime t1, DateTime t2)
        {
            return (DateTime.Compare(t1, t2) < 0);
        }

        /// <summary>
        /// Determine if this DateTime object is earlier or same as t2.
        /// </summary>
        /// <param name="t1"></param>
        /// <param name="t2"></param>
        /// <returns></returns>
        public static bool IsEarlierThanOrEqual(this DateTime t1, DateTime t2)
        {
            return (DateTime.Compare(t1, t2) <= 0);
        }

        /// <summary>
        /// Determine if this DateTime object is later than t2.
        /// </summary>
        /// <param name="t1"></param>
        /// <param name="t2"></param>
        /// <returns></returns>
        public static bool IsLaterThan(this DateTime t1, DateTime t2)
        {
            return (DateTime.Compare(t1, t2) > 0);
        }

        /// <summary>
        /// Determine if this DateTime object is later or same as t2.
        /// </summary>
        /// <param name="t1"></param>
        /// <param name="t2"></param>
        /// <returns></returns>
        public static bool IsLaterThanOrEqual(this DateTime t1, DateTime t2)
        {
            return (DateTime.Compare(t1, t2) >= 0);
        }

        /// <summary>
        /// Computes the start of the week starting on a particular day.
        /// </summary>
        public static DateTime StartOfWeek(this DateTime dt, DayOfWeek startOfWeek)
        {
            int diff = dt.DayOfWeek - startOfWeek;
            if (diff < 0)
            {
                diff += 7;
            }

            return dt.AddDays(-1 * diff).Date;
        }
    } // DateTimeExtensions


    /// <summary>
    /// DateTime utility methods.
    /// </summary>
    public static class DateTimeUtils
    {
        /// <summary>
        /// Constant for the unix epoch.
        /// </summary>
        public static readonly DateTime c_epoch;

        /// <summary>
        /// Static constructor.
        /// </summary>
        static DateTimeUtils()
        {
            c_epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        }

        /// <summary>
        /// Returns whether this datetime is a the unix epoch.
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public static bool IsUnixEpoch(this DateTime t)
        {
            return (t == c_epoch);
        }

        /// <summary>
        /// Creates a new time based on seconds since the 1st of Jan 1970.
        /// </summary>
        /// <param name="seconds"></param>
        /// <returns></returns>
        public static DateTime CreateFromEpochSeconds(ulong seconds)
        {
            return c_epoch.AddSeconds(seconds);
        }
    } // DateTimeUtils

} // RSG.Base.Extensions namespace
