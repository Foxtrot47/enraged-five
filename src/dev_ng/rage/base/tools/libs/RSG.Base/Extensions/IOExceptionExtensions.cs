﻿using System;
using System.Runtime.InteropServices;

namespace RSG.Base.Extensions
{

    /// <summary>
    /// Extensions to the System.IO.IOException class.
    /// </summary>
    public static class IOExceptionExtensions
    {
        #region Constants
        private static readonly int ERROR_SHARING_VIOLATION = 32;
        private static readonly int ERROR_LOCK_VIOLATION = 33;
        #endregion // Constants

        /// <summary>
        /// Determine whether this IOException was raised because of a
        /// file locking error.
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        public static bool IsFileLocked(this System.IO.IOException ex)
        {
            int errorCode = Marshal.GetHRForException(ex) & ((1 << 16) - 1);
            return (ERROR_LOCK_VIOLATION == errorCode || ERROR_SHARING_VIOLATION == errorCode);
        }
    }

} // RSG.Base.Extensions namespace
