﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Windows.Media.Imaging;
using RSG.Base.Attributes;

namespace RSG.Base.Extensions
{
    public static class EnumExtensions
    {
        /// <summary>
        /// Gets an attribute on an enum field value; rather than adding method
        /// for each attribute type.
        /// </summary>
        /// <typeparam name="T">The type of the attribute you want to retrieve</typeparam>
        /// <param name="value">The enum value</param>
        /// <returns>Attribute of type T that exists on the enum value</returns>
        /// 
        public static T GetAttributeOfType<T>(this Enum value) 
            where T : System.Attribute
        {
            Type type = value.GetType();
            MemberInfo[] memInfo = type.GetMember(value.ToString());
            T[] attributes = (T[])memInfo[0].GetCustomAttributes(typeof(T), false);
            return (attributes.Length > 0 ? attributes[0] : null);
        }


        /// <summary>
        /// Retrieves the first regular expression associated with an enum value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static Regex GetRegexValue(this Enum value)
        {
            RegexAttribute attribute = value.GetAttributeOfType<RegexAttribute>();
            //KRW: Disabled assert since the "File" enumeration doesn't have a regex attribute.
            //Debug.Assert(attribute != null, "Enum is missing the RegexAttribute.");
            return (attribute == null ? null : attribute.Regex);
        }

        /// <summary>
        /// Retrieves the first regular expression associated with an enum value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetFriendlyNameValue(this Enum value)
        {
            FriendlyNameAttribute nameAtt = value.GetAttributeOfType<FriendlyNameAttribute>();
            Debug.Assert(nameAtt != null, "Enum is missing the FriendlyNameAttribute.");
            return (nameAtt == null ? null : nameAtt.FriendlyName);
        }

        /// <summary>
        /// Retrieves the display name for a log level.
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        public static String GetDisplayName(this Enum value)
        {
            DisplayNameAttribute nameAtt = value.GetAttributeOfType<DisplayNameAttribute>();
            Debug.Assert(nameAtt != null, "Enum is missing the DisplayNameAttribute.");
            return (nameAtt == null ? null : nameAtt.DisplayName);
        }

        /// <summary>
        /// Retrieves the enum value for the display name.
        /// Have to return an Enum instead of T, as Enum is nullable so we can check for failure return case.
        /// </summary>
        /// <typeparam name="T">Enum type name</typeparam>
        /// <param name="displayName"></param>
        /// <returns></returns>
        public static Enum GetValByDisplayName<T>(string displayName)
        {
            Type enumType = typeof(T);
            if (enumType.IsEnum)
            {
                Array enumVals = Enum.GetValues(enumType);
                foreach (Enum value in enumVals)
                {
                    DisplayNameAttribute nameAtt = value.GetAttributeOfType<DisplayNameAttribute>();
                    Debug.Assert(nameAtt != null, "Enum is missing the DisplayNameAttribute.");
                    if(nameAtt != null && nameAtt.DisplayName.Equals(displayName))
                        return value;
                }
            }
            return null;
        }

        /// <summary>
        /// Retrieves the icon to display for a particular enum value.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static BitmapSource GetDisplayIcon(this Enum value)
        {
            DisplayIconAttribute iconAtt = value.GetAttributeOfType<DisplayIconAttribute>();
            Debug.Assert(iconAtt != null, "Enum is missing the DisplayIconAttribute.");
            return iconAtt.Icon;
        }

        /// <summary>
        /// Retrieves the first description string associated with an enum value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetDescriptionValue(this Enum value)
        {
            DescriptionAttribute descAttr = value.GetAttributeOfType<DescriptionAttribute>();
            Debug.Assert(descAttr != null, "Enum is missing the DescriptionAttribute.");
            return (descAttr == null ? null : descAttr.Description);
        }

        /// <summary>
        /// Retrieves the first default value associated with an enum value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static object GetDefaultValue(this Enum value)
        {
            DefaultValueAttribute defaultAttr = value.GetAttributeOfType<DefaultValueAttribute>();
            Debug.Assert(defaultAttr != null, "Enum is missing the DefaultValueAttribute.");
            return (defaultAttr == null ? null : defaultAttr.Value);
        }

        /// <summary>
        /// Enumerates all possible combinations of bit flags relating to an enum.
        /// Original code from: http://stackoverflow.com/a/6117125
        /// Modified to include flags attribute check
        /// </summary>
        /// <typeparam name="T">Can't restrict to enums, but this is the next closest thing...</typeparam>
        /// <returns></returns>
        public static IEnumerable<T> EnumerateAllFlags<T>() where T : struct, IConvertible, IComparable, IFormattable
        {
            // Check that the enum type has the flags attribute assigned
            Type type = typeof(T);
            FlagsAttribute[] atts = type.GetCustomAttributes(typeof(FlagsAttribute), false) as FlagsAttribute[];
            if (atts.Length == 0)
            {
                throw new ArgumentException("The enum you passed into the EnumerateAllFlags<T> function isn't a bit flag enum.");
            }

            // Construct a function for OR-ing together two enums
            ParameterExpression param1 = Expression.Parameter(type);
            ParameterExpression param2 = Expression.Parameter(type);
            Func<T,T,T> orFunction = Expression.Lambda<Func<T, T, T>>(
                Expression.Convert(
                    Expression.Or(
                        Expression.Convert(param1, type.GetEnumUnderlyingType()),
                        Expression.Convert(param2, type.GetEnumUnderlyingType())),
                    type), param1, param2).Compile();

            T[] initalValues = (T[])Enum.GetValues(type);
            HashSet<T> discoveredCombinations = new HashSet<T>(initalValues);
            Queue<T> queue = new Queue<T>(initalValues);

            // Try OR-ing every inital value to each value in the queue
            while (queue.Count > 0)
            {
                T a = queue.Dequeue();
                foreach (T b in initalValues)
                {
                    T combo = orFunction(a, b);
                    if (discoveredCombinations.Add(combo))
                    {
                        queue.Enqueue(combo);
                    }
                }
            }

            return discoveredCombinations;
        }
    } // EnumExtensions

} // RSG.Base.Extensions
