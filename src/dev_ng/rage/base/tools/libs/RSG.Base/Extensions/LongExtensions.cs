﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Extensions
{
    /// <summary>
    /// 
    /// </summary>
    public static class LongExtensions
    {
        /// <summary>
        /// Converts a long to a filesize representation.
        /// </summary>
        /// <param name="l"></param>
        /// <returns></returns>
        public static string ToFileSize(this long l)
        {
            return String.Format(new FileSizeFormatProvider(), "{0:fs}", l);
        }
    }


    /// <summary>
    /// Converts a value in bytes into a human readable format.
    /// </summary>
    public class FileSizeFormatProvider : IFormatProvider, ICustomFormatter
    {
        #region Constants
        /// <summary>
        /// 
        /// </summary>
        private const string c_fileSizeFormat = "fs";

        /// <summary>
        /// 
        /// </summary>
        private const decimal c_oneKiloByte = 1024M;
        private const decimal c_oneMegaByte = c_oneKiloByte * c_oneKiloByte;
        private const decimal c_oneGigaByte = c_oneMegaByte * c_oneKiloByte;
        private const decimal c_oneTeraByte = c_oneGigaByte * c_oneKiloByte;
        private const decimal c_onePetaByte = c_oneTeraByte * c_oneKiloByte;
        #endregion // Constants

        #region IFormatProvider Implementation
        /// <summary>
        /// Returns an object that provides formatting services for the specified type.
        /// </summary>
        /// <param name="formatType">An object that specifies the type of format object to return.</param>
        /// <returns>An instance of the object specified by formatType, if the System.IFormatProvider
        /// implementation can supply that type of object; otherwise, null.</returns>
        public object GetFormat(Type formatType)
        {
            return (formatType == typeof(ICustomFormatter) ? this : null);
        }
        #endregion // IFormatProvider Implementation

        #region ICustomFormatter Implementation
        /// <summary>
        /// 
        /// </summary>
        /// <param name="format"></param>
        /// <param name="arg"></param>
        /// <param name="formatProvider"></param>
        /// <returns></returns>
        public string Format(string format, object arg, IFormatProvider formatProvider)
        {
            // Validate the input parameters.
            if (format == null || !format.StartsWith(c_fileSizeFormat) || arg is string)
            {
                return defaultFormat(format, arg, formatProvider);
            }

            decimal size;
            try
            {
                size = Convert.ToDecimal(arg);
            }
            catch (InvalidCastException)
            {
                return defaultFormat(format, arg, formatProvider);
            }

            string suffix;
            if (size > c_onePetaByte)
            {
                size /= c_onePetaByte;
                suffix = "PB";
            }
            else if (size > c_oneTeraByte)
            {
                size /= c_oneTeraByte;
                suffix = "TB";
            }
            if (size > c_oneGigaByte)
            {
                size /= c_oneGigaByte;
                suffix = "GB";
            }
            else if (size > c_oneMegaByte)
            {
                size /= c_oneMegaByte;
                suffix = "MB";
            }
            else if (size > c_oneKiloByte)
            {
                size /= c_oneKiloByte;
                suffix = "KB";
            }
            else
            {
                suffix = " B";
            }

            // Did the user provide a precision?
            string precision = format.Substring(2);
            if (String.IsNullOrEmpty(precision))
            {
                precision = "2";
            }

            // Return the output
            return String.Format("{0:N" + precision + "}{1}", size, suffix);
        }
        #endregion // ICustomFormatter Implementation

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="format"></param>
        /// <param name="arg"></param>
        /// <param name="formatProvider"></param>
        /// <returns></returns>
        private string defaultFormat(string format, object arg, IFormatProvider formatProvider)
        {
            IFormattable formattableArg = arg as IFormattable;
            if (formattableArg != null)
            {
                return formattableArg.ToString(format, formatProvider);
            }
            return arg.ToString();
        }
        #endregion // Private Methods
    } // FileSizeFormatProvider
}
