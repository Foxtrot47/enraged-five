﻿//---------------------------------------------------------------------------------------------
// <copyright file="XmlReaderExtensions.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Base.Extensions
{
    using System;
    using System.Xml;

    /// <summary>
    /// Contains static helper methods onto the System.Xml.XmlReader class.
    /// </summary>
    public static class XmlReaderExtensions
    {
        #region Methods
        /// <summary>
        /// Checks whether the current node is a content (non-whitespace text, CDATA, Element,
        /// EndElement, EntityReference or EndEntity) node. If the node is not a content node,
        /// then the method skips ahead to the next content node or end of file. Skips over
        /// nodes of type ProcessingInstruction, DocumentType, Whitespace and
        /// SignificantWhitespace.
        /// </summary>
        /// <param name="reader">
        /// The reader to move forward.
        /// </param>
        /// <returns>
        /// A value indicating whether the reader is on a element or Comment.
        /// </returns>
        public static bool MoveToElementOrComment(this XmlReader reader)
        {
            do
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Attribute:
                        reader.MoveToElement();
                        goto case XmlNodeType.Element;
                    case XmlNodeType.Element:
                    case XmlNodeType.Comment:
                        return true;
                    case XmlNodeType.EndElement:
                        return false;
                }
            }
            while (reader.Read());
            return false;
        }

        /// <summary>
        /// Validates that the specified reader is positioned at the start of a element whose
        /// name if equal to the specified name.
        /// </summary>
        /// <param name="reader">
        /// The reader to validate.
        /// </param>
        /// <param name="name">
        /// The name of the element that 
        /// </param>
        public static void ValidateStartPosition(this XmlReader reader, string name)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            IXmlLineInfo info = reader as IXmlLineInfo;
            int lineNumber = 0;
            int linePosition = 0;
            if (info != null)
            {
                lineNumber = info.LineNumber;
                linePosition = info.LinePosition;
            }

            string msg = "Expected to be positioned at the start of a '" + name + "' element";
            if (!reader.IsStartElement())
            {
                throw new XmlException(msg, null, lineNumber, linePosition);
            }

            if (!String.Equals(reader.Name, name))
            {
                throw new XmlException(msg, null, lineNumber, linePosition);
            }
        }
        #endregion Methods
    } // RSG.Base.Extensions.XmlReaderExtensions {Class}
} // RSG.Base.Extensions {Namespace}
