﻿using System;
using System.ComponentModel;
using System.Diagnostics;

namespace RSG.Base
{
    /// <summary>
    /// This is a simple generic class to make it easier to create EventArgs subclasses with various 
    /// payloads. The one parameter it takes is for the single payload object stored within.
    /// 
    /// Example: delegate void StatusUpdateDelegate(object sender, SimpleEventArgs<string> newStatus);
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class SimpleEventArgs<T> : EventArgs
    {
        public SimpleEventArgs(T payload) { Payload = payload; }
        public T Payload { get; protected set; }
    }
}

#if !NET_20_FRAMEWORK
namespace RSG.Base.Extensions
{

    // Some of this modeled after the event raising patterns here: 
    // http://geekswithblogs.net/HouseOfBilz/archive/2009/02/15/re-thinking-c-events.aspx

    /// <summary>
    /// EventHandler extension methods.
    /// </summary>
    public static class EventHandlerExtensions
    {

        /// <summary>
        /// Raise (trigger) an event safely (if there are listeners).
        /// </summary>
        /// <param name="handler"></param>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// This extension method to an EventHandler allows you to safely raise
        /// events without having to check there are listeners every time.
        /// 
        /// E.g. for the event PropertyChanged, previously:
        ///   if (null != PropertyChanged)
        ///     PropertyChanged(this, new PropertyChangedEventArgs(...));
        ///     
        /// Using this extension method:
        ///   PropertyChanged.Raise(this, new PropertyChangedEventArgs(...));
        ///
        [DebuggerHidden]
        public static void Raise<T>(this EventHandler<T> handler, Object sender, T e)
            where T : EventArgs
        {
            if (null != handler)
                handler(sender, e);
        }

        /// <summary>
        /// Raise (trigger) an event safely (if there are listeners).
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="handler"></param>
        /// <param name="sender"></param>
        /// <param name="payload"></param>
        /// Same as Raise (above), but it allows you to pass in any object type, and wraps it in a SimpleEventArgs<T>
        /// container. E.g.:
        /// printStringCallback.Raise(this, "print this string");
        /// 
        [DebuggerHidden]
        public static void Raise<T>(this EventHandler<SimpleEventArgs<T>> handler, Object sender, T payload)
        {
            if (null != handler) { 
                handler(sender, new SimpleEventArgs<T>(payload)); 
            }
        }

        [DebuggerHidden]
        public static void Raise(this EventHandler<EventArgs> handler, Object sender)
        {
            if (null != handler) { 
                handler(sender, EventArgs.Empty); 
            }
        }

        /// <summary>
        /// Raise (trigger) an event and return the possibly-modified event argument
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="evt"></param>
        /// <param name="sender"></param>
        /// <param name="payload"></param>
        /// <returns></returns>
        /// This is the same as the Raise() extention method above, but it returns the SimpleEventArgs<T> object wrapping
        /// the original payload. This object may have been modified by the delegates that were called.
        public static SimpleEventArgs<T> RaiseAndReturn<T>(this EventHandler<SimpleEventArgs<T>> evt, object sender, T payload)
        {
            SimpleEventArgs<T> args = new SimpleEventArgs<T>(payload);
            if (evt != null) { evt(sender, args); }
            return args;
        }
    }
} // RSG.Base.Extensions
#endif
