﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Extensions
{
    /// <summary>
    /// 
    /// </summary>
    public static class ICollectionExtensions
    {
        /// <summary>
        /// Add a range of elements to an existing collection.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection"></param>
        /// <param name="items"></param>
        public static void AddRange<T>(this ICollection<T> collection, IEnumerable<T> items)
        {
            // Optimisation for lists.
            // FD: double cast is not thread safe, and we have a couple of AsParallel() calls.
            List<T> listCollection = collection as List<T>;
            if ( listCollection != null )
            {
                listCollection.AddRange( items );
            }
            else
            {
                // FD: will throw an ArgumentException in a couple of cases
                // for example on a Dictionary<K,V>, if we try to add a KVPair with a Key already in dictionary
                foreach ( T item in items )
                {
                    collection.Add( item );
                }
            }
        }

        /// <summary>
        /// Remove a range of elements from an existing collection.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection"></param>
        /// <param name="items"></param>
        public static void RemoveRange<T>(this ICollection<T> collection, IEnumerable<T> items)
        {
            foreach (T item in items)
            {
                collection.Remove(item);
            }
        }

        /// <summary>
        /// Executes an action for each item and a sequence, passing in the 
        /// index of that item to the action procedure.
        /// </summary>
        /// <typeparam name="T">The type of the sequence.</typeparam>
        /// <param name="that">The sequence.</param>
        /// <param name="action">A function that accepts a sequence item and its
        /// index in the sequence.</param>
        public static void ForEachWithIndex<T>(this IEnumerable<T> that, Action<T, int> action)
        {
            int index = 0;
            foreach (T item in that)
            {
                action(item, index);
                index++;
            }
        }
    }

} // RSG.Base.Extensions namespace
