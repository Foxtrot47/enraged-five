﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace RSG.Base.Extensions
{
    /// <summary>
    /// Guid extensions
    /// </summary>
    public static class GuidExtensions
    {
        /// <summary>
        /// Generates a deterministic guid from a string.
        /// Adapted from http://geekswithblogs.net/EltonStoneman/archive/2008/06/26/generating-deterministic-guids.aspx
        /// Use like this; 
        ///     Guid myGuid = Guid().FromSeedDeterminsitic(seed);
        /// </summary>
        public static Guid FromSeedDeterministic(this Guid guid, String seed)
        {
            //use MD5 hash to get a 16-byte hash of the string:
            MD5CryptoServiceProvider provider = new MD5CryptoServiceProvider();

            byte[] inputBytes = Encoding.Default.GetBytes(seed);
            byte[] hashBytes = provider.ComputeHash(inputBytes);

           Guid hashGuid = new Guid(hashBytes);
           return hashGuid;            
        }
    }
}
