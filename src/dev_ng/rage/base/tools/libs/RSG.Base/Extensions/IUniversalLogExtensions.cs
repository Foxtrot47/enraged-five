﻿using RSG.Base.Logging.Universal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Extensions
{
    /// <summary>
    /// Extensions of IUniversalLog
    /// </summary>
    public static class IUniversalLogExtensions
    {
        #region Methods
        /// <summary>
        /// Displays a very pronounced log message 
        /// - consisting of 5 lines of log output.
        ///
        /// ************
        /// *** blah *** 
        /// ************
        /// 
        /// </summary>
        /// <param name="log"></param>
        /// <param name="logCtx"></param>
        /// <param name="msg"></param>
        /// <param name="args"></param>
        public static void FatMessageCtx(this IUniversalLog log, String logCtx, String msg, params Object[] args)
        {
            const char border = '*';
            const int shortBorderLen = 3;

            String message = String.Format(msg, args);

            String fullBorder = new String(border, message.Length + (shortBorderLen * 2) + 2);
            String shortBorder = new String(border, shortBorderLen);
            log.MessageCtx(logCtx, " ");
            log.MessageCtx(logCtx, fullBorder);
            log.MessageCtx(logCtx, "{0} {1} {0}", shortBorder, message);
            log.MessageCtx(logCtx, fullBorder);
            log.MessageCtx(logCtx, " ");
        }
        #endregion // Methods
    }
}
