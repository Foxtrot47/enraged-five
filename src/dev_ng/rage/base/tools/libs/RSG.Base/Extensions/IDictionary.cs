﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Extensions
{

    /// <summary>
    /// IDictionary<K, V> extension methods.
    /// </summary>
    public static class IDictionaryExtensions
    {
        /// <summary>
        /// Merge dictionary objects; optionally ignoring duplicate keys (where source
        /// value would be kept).
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="K"></typeparam>
        /// <typeparam name="V"></typeparam>
        /// <param name="source"></param>
        /// <param name="other"></param>
        /// <param name="ignoreDupeKeys"></param>
        /// <returns></returns>
        public static IDictionary<K, V> Merge<K, V>(this IDictionary<K, V> source, 
            IDictionary<K, V> other, bool ignoreDupeKeys)
        {
            IDictionary<K, V> output = new Dictionary<K, V>(source);

            foreach (KeyValuePair<K, V> pair in other)
            {
                bool duplicateKey = output.ContainsKey(pair.Key);
                if (duplicateKey && !ignoreDupeKeys)
                    throw (new ArgumentException("Duplicate key."));
                else if (duplicateKey && ignoreDupeKeys)
                    continue;

                output.Add(pair.Key, pair.Value);
            }

            return output;
        }
    }

} // RSG.Base.Extensions namespace
