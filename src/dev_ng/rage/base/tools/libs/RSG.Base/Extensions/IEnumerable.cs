﻿using System;
using System.Collections.Generic;
using System.Linq;
using SMath = System.Math;

namespace RSG.Base.Extensions
{
    /// <summary>
    /// IEnumerable extension methods.
    /// </summary>
    public static class IEnumerableExtensions
    {
        /// <summary>
        /// Determines whether no elements of a sequence satisfies a condition.
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        public static bool None<TSource>(this IEnumerable<TSource> source)
        {
            return (!source.Any());
        }

        /// <summary>
        /// Determines whether no elements of a sequence satisfies a condition.
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="source"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static bool None<TSource>(this IEnumerable<TSource> source,
                                         Func<TSource, bool> predicate)
        {
            return (!source.Any(predicate));
        }

        /// <summary>
        /// Does the enumerable have at least a count of 'num' elements?
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <param name="num"></param>
        /// <returns></returns>
        public static bool HasAtLeast<T>(this IEnumerable<T> items, int num)
        {
            return items.Count() >= num;
            // For now until profiled stick with something safe
            //return items.Take(num).Count() == num; // could be items.Skip(num).Any(); ?
        }

        /// <summary>
        /// Does the enumerable have at most a count of 'num' elements?
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <param name="num"></param>
        /// <returns></returns>
        public static bool HasAtMost<T>(this IEnumerable<T> items, int num)
        {
            return items.Count() <= num;
            // For now until profiled stick with something safe
            //return items.Take(num + 1).Count() <= num;
        }

        /// <summary>
        /// Distinct Extension - allows for lamda delegate to be used
        /// - eg. myEnumerable.Distinct(my => my.UniqueId);
        /// - originally I was doing this changelists.GroupBy(cl => cl.Number).Select(grp => grp.First());
        /// - but extension method opens it up to all.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TId"></typeparam>
        /// <param name="values"></param>
        /// <param name="projection"></param>
        /// <returns></returns>
        public static IEnumerable<T> Distinct<T, TDistinction>(this IEnumerable<T> items, Func<T, TDistinction> func)
        {
            return items.Distinct(new LamdaComparer<T, TDistinction>(func)); 
        }

        #region LamdaComparer
        /// <summary>
        /// LamdaComparer to support the IEnumerableExtensions Distinct Extension
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TUnqiueId"></typeparam>
        private class LamdaComparer<T, TDistinction> : IEqualityComparer<T>
        {
            #region Properties
            private readonly Func<T, TDistinction> Func;
            #endregion // Properties

            #region Constructor(s)
            public LamdaComparer(Func<T, TDistinction> func) 
            {
                Func = func; 
            }
            #endregion // Constructor(s)

            #region IEqualityComparer
            public bool Equals(T objA, T objB) 
            {
                return Func(objA).Equals(Func(objB));
            } 

            public int GetHashCode(T obj) 
            {
                return Func(obj).GetHashCode();
            }
            #endregion // IEqualityComparer
        }
        #endregion // LamdaComparer

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <param name="action"></param>
        public static void ForEach<T>(this IEnumerable<T> items, Action<T> action)
        {
            foreach (T item in items)
            {
                action(item);
            }
        }

        /// <summary>
        /// Return an enumerable of the first N elements from a start position.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="values"></param>
        /// <param name="start"></param>
        /// <param name="n"></param>
        /// <returns></returns>
        public static IEnumerable<T> FirstN<T>(this IEnumerable<T> values, int n, int start)
        {
            return (values.Skip(start).Take(n));
        }

        /// <summary>
        /// Return an enumerable of the last N elements.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="values"></param>
        /// <param name="n"></param>
        /// <returns></returns>
        public static IEnumerable<T> LastN<T>(this IEnumerable<T> values, int n)
        {
            return (values.Skip(SMath.Max(0, values.Count() - n)).Take(n));
        }

        /// <summary>
        /// Break a list of items into chunks of a specific size.
        /// </summary>
        public static IEnumerable<IEnumerable<T>> Chunk<T>(this IEnumerable<T> source, int chunksize)
        {
            while (source.Any())
            {
                yield return source.Take(chunksize);
                source = source.Skip(chunksize);
            }
        }

        /// <summary>
        /// Compute the standard deviation of a list of doubles.
        /// </summary>
        /// <param name="values"></param>
        /// <returns></returns>
        public static double CalculateStdDev(this IEnumerable<double> values)
        {
            double ret = 0.0;

            if (values.Any())
            {
                double mean = values.Average();
                double sumOfSquares = values.Sum(d => System.Math.Pow(d - mean, 2));
                ret = System.Math.Sqrt(sumOfSquares / values.Count());
            }

            return ret;
        }

        /// <summary>
        /// Compute the standard deviation of a list of doubles.
        /// </summary>
        /// <param name="values"></param>
        /// <returns></returns>
        public static float CalculateStdDev(this IEnumerable<float> values)
        {
            float ret = 0.0f;

            if (values.Any())
            {
                float mean = values.Average();
                double sumOfSquares = values.Sum(f => System.Math.Pow(f - mean, 2));
                ret = (float)System.Math.Sqrt(sumOfSquares / values.Count());
            }

            return ret;
        }

        /// <summary>
        /// Converts the passed in IEnumerable to a set.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="values"></param>
        /// <returns></returns>
        public static ISet<T> ToSet<T>(this IEnumerable<T> values)
        {
            return new HashSet<T>(values);
        }
    } // IEnumerableExtensions

} // RSG.Base.Extensions namespace
