﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Extensions
{

    /// <summary>
    /// TimeSpan class extension methods.
    /// </summary>
    public static class TimeSpanExtensions
    {
        /// <summary>
        /// Pretty-print TimeSpan object (if required).
        /// </summary>
        public static String ToHumanReadableString(this TimeSpan elapsed)
        {
            if (elapsed.TotalDays > 1)
            {
                return String.Format("{0:0.###} days", elapsed.TotalDays);
            }
            else if (elapsed.TotalHours > 1)
            {
                return String.Format("{0:0.###} hrs", elapsed.TotalHours);
            }
            else if (elapsed.TotalMinutes > 1)
            {
                return String.Format("{0:0.###} mins", elapsed.TotalMinutes);
            }
            else if (elapsed.TotalSeconds > 1)
            {
                return String.Format("{0:0.###} s", elapsed.TotalSeconds);
            }
            else
            {
                return String.Format("{0:0.} ms", elapsed.TotalMilliseconds);
            }
        }
    }

} // RSG.Base.Extensions namespace
