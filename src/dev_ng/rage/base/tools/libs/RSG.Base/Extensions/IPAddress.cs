﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;

namespace RSG.Base.Extensions
{

    /// <summary>
    /// IPAddress class extension methods.
    /// </summary>
    /// References: 
    /// http://blogs.msdn.com/b/knom/archive/2008/12/31/ip-address-calculations-with-c-subnetmasks-networks.aspx
    /// 
    public static class IPAddressExtensions
    {
        /// <summary>
        /// Return long integer representation of IP Address.
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        [Obsolete("This will only work with IPv4 IPAddresses")]
        public static long ToLong(this IPAddress address)
        {
            Byte[] bytes = address.GetAddressBytes();
			return (long)(((long)bytes[0] << 24) | ((long)bytes[1] << 16) |
				((long)bytes[2] << 8) | (long)bytes[3]);

        }

        /// <summary>
        /// Retrieves the IP address of the local machine.
        /// </summary>
        /// <returns></returns>
        public static IPAddress GetLocalIPAddress()
        {
            IPHostEntry host;
            IPAddress localIP = null;
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    localIP = ip;
                    break;
                }
            }
            return localIP;
        }

        /// <summary>
        /// Gets all the IP Addresses used on the domain.
        /// </summary>
        /// <returns>List of Domain IP if found, otherwise null</returns>
        public static List<IPAddress> GetAllDomainIPAddresses()
        {
            // This should get us the IP addresses used on the domain.
            // Added OrderBy to prefer 10.X.X.X IP over 192.X.X.X
            List<IPAddress> domainIpAddresses = NetworkInterface
                .GetAllNetworkInterfaces()
                .Where(i => !string.IsNullOrEmpty(i.GetIPProperties().DnsSuffix))
                .SelectMany(i => i.GetIPProperties().UnicastAddresses)
                .Where(a => a.Address.AddressFamily == AddressFamily.InterNetwork)
                .Select(a => a.Address)
                .OrderBy(x => x.ToString())
                .ToList();

            return domainIpAddresses;
        }

        /// <summary>
        /// Gets all the IP addresses of network interfaces that might be connected to a Rockstar domain
        /// </summary>
        /// <returns>
        /// All candidate IP addresses.
        /// </returns>
        /// <remarks>
        /// Candidate network interfaces must be operational and not the loopback interface.
        /// Results are primarily sorted in order of network interface type: non-wireless followed by wireless to help prioritise wired connections.
        /// Results are subsequently sorted in alphabetical order to prioritise IP addresses in the 10.x.x.x range.
        /// </remarks>
        public static List<IPAddress> GetAllCandidateDomainIPAddresses()
        {
            return NetworkInterface.GetAllNetworkInterfaces()
                .Where(ni => ni.OperationalStatus == OperationalStatus.Up)
                .Where(ni => ni.NetworkInterfaceType != NetworkInterfaceType.Loopback)
                .SelectMany(ni => ni.GetIPProperties().UnicastAddresses.Select(a => new { Interface = ni, a.Address }))
                .Where(x => x.Address.AddressFamily == AddressFamily.InterNetwork)
                .OrderBy(x => x.Interface.NetworkInterfaceType == NetworkInterfaceType.Wireless80211)
                .ThenBy(x => x.Address.ToString())
                .Select(x => x.Address)
                .ToList();
        }

        /// <summary>
        /// Gets the IP Address used on the domain.
        /// </summary>
        /// <returns>Domain IP if found, otherwise null</returns>
        public static IPAddress GetDomainIPAddress()
        {
            return GetAllDomainIPAddresses().FirstOrDefault();
        }

        /// <summary>
        /// Return broadcast address for any given IP Address.
        /// </summary>
        /// <param name="address"></param>
        /// <param name="subnetMask"></param>
        /// <returns></returns>
        public static IPAddress GetBroadcastAddress(this IPAddress address, IPAddress subnetMask)
        {
            Byte[] ipAdressBytes = address.GetAddressBytes();
            Byte[] subnetMaskBytes = subnetMask.GetAddressBytes();

            if (ipAdressBytes.Length != subnetMaskBytes.Length)
                throw new ArgumentException("Lengths of IP address and subnet mask do not match.");

            Byte[] broadcastAddress = new Byte[ipAdressBytes.Length];
            for (int i = 0; i < broadcastAddress.Length; i++)
            {
                broadcastAddress[i] = (Byte)(ipAdressBytes[i] | (subnetMaskBytes[i] ^ 255));
            }
            return (new IPAddress(broadcastAddress));
        }

        /// <summary>
        /// Return network address of an IP Address.
        /// </summary>
        /// <param name="address"></param>
        /// <param name="subnetMask"></param>
        /// <returns></returns>
        public static IPAddress GetNetworkAddress(this IPAddress address, IPAddress subnetMask)
        {
            Byte[] ipAdressBytes = address.GetAddressBytes();
            Byte[] subnetMaskBytes = subnetMask.GetAddressBytes();

            if (ipAdressBytes.Length != subnetMaskBytes.Length)
                throw new ArgumentException("Lengths of IP address and subnet mask do not match.");

            Byte[] broadcastAddress = new Byte[ipAdressBytes.Length];
            for (int i = 0; i < broadcastAddress.Length; i++)
            {
                broadcastAddress[i] = (Byte)(ipAdressBytes[i] & (subnetMaskBytes[i]));
            }
            return (new IPAddress(broadcastAddress));
        }

        /// <summary>
        /// Return whether two IP addresses are in the same subnet.
        /// </summary>
        /// <param name="address2"></param>
        /// <param name="address"></param>
        /// <param name="subnetMask"></param>
        /// <returns></returns>
        public static bool IsInSameSubnet(this IPAddress address2, IPAddress address, IPAddress subnetMask)
        {
            IPAddress network1 = address.GetNetworkAddress(subnetMask);
            IPAddress network2 = address2.GetNetworkAddress(subnetMask);

            return (network1.Equals(network2));
        }
    }

} // RSG.Base.Extensions namespace
