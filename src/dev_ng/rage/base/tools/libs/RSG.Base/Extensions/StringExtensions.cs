﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace RSG.Base.Extensions
{
    /// <summary>
    /// String class extension methods.
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        /// Truncate string to a max length.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="maxLength"></param>
        /// <returns></returns>
        public static string Truncate(this string value, int maxLength)
        {
            if (string.IsNullOrEmpty(value)) return value;
            return value.Length <= maxLength ? value : value.Substring(0, maxLength);
        }

        /// <summary>
        /// Constructs a string from enumerable of chars
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static String ToSystemString(this IEnumerable<Char> chars)
        {
            return new String(chars.ToArray());
        }

        /// <summary>
        /// Pad a string with spaces left and right
        /// - so that it is centralised and surrounded by space
        /// eg.  '   I am a string   '
        /// </summary>
        /// <param name="str"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static String PadBoth(this String str, int length)
        {
            int spaces = length - str.Length;
            int padLeft = spaces / 2 + str.Length;
            return str.PadLeft(padLeft).PadRight(length);
        }

        /// <summary>
        /// Returns a string array that contains the substrings in this instance that are
        /// delimited by the elements in the specified character array.
        /// </summary>
        /// <param name="s">
        /// The string to split.
        /// </param>
        /// <param name="separator">
        /// An array of Unicode characters that delimit the substrings in this string, an empty
        /// array that contains no delimiters, or null.
        /// </param>
        /// <param name="removeEmpty">
        /// A value indicating whether empty entries will be returned or not.
        /// </param>
        /// <returns>
        /// An array whose elements contain the substrings in this string that are delimited by
        /// one or more characters in separator.
        /// </returns>
        public static string[] SafeSplit(this string s, char[] separator, bool removeEmpty)
        {
            if (s == null)
            {
                return new string[0];
            }

            StringSplitOptions options = StringSplitOptions.None;
            if (removeEmpty)
            {
                options = StringSplitOptions.RemoveEmptyEntries;
            }

            return s.Split(separator, options);
        }

        /// <summary>
        /// Returns a string array that contains the substrings in this instance that are
        /// delimited by the specified character.
        /// </summary>
        /// <param name="s">
        /// The string to split.
        /// </param>
        /// <param name="separator">
        /// A character that delimits the substrings in this string.
        /// </param>
        /// <param name="removeEmpty">
        /// A value indicating whether empty entries will be returned or not.
        /// </param>
        /// <returns>
        /// An array whose elements contain the substrings in this string that are delimited by
        /// the specified separator.
        /// </returns>
        public static string[] SafeSplit(this string s, char separator, bool removeEmpty)
        {
            if (s == null)
            {
                return new string[0];
            }

            StringSplitOptions options = StringSplitOptions.None;
            if (removeEmpty)
            {
                options = StringSplitOptions.RemoveEmptyEntries;
            }

            return s.Split(new char[] { separator }, options);
        }

        /// <summary>
        /// Split a pathname by system directory separator characters.
        /// </summary>
        /// <param name="pathname"></param>
        /// <returns></returns>
        public static String[] SplitPath(this String pathname)
        {
            String[] parts = pathname.Split(new Char[] {
                Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar },
                StringSplitOptions.RemoveEmptyEntries);
            return (parts);
        }

        /// <summary>
        /// Replaces the format item in a specified string with the string representation of a
        /// corresponding object in a specified array. A specified parameter supplies
        /// culture-specific formatting information.
        /// </summary>
        /// <param name="format">
        /// A composite format string.
        /// </param>
        /// <param name="provider">
        /// An object that supplies culture-specific formatting information.
        /// </param>
        /// <param name="args">
        /// An object array that contains zero or more objects to format.
        /// </param>
        /// <returns>
        /// A copy of format in which the format items have been replaced by the string
        /// representation of the corresponding objects in args.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// The <paramref name="format"/> or <paramref name="args"/> parameters are null.
        /// </exception>
        /// <exception cref="System.FormatException">
        /// The <paramref name="format"/> parameter is invalid.-or- The index of a format item
        /// is less than zero, or greater than or equal to the length of the args array.
        /// </exception>
        public static string Format(this string format, IFormatProvider provider, params object[] args)
        {
            return String.Format(provider, format, args);
        }

        /// <summary>
        /// Replaces the format item in a specified string with the string representation of a
        /// corresponding object in a specified array using the current culture for formatting
        /// information.
        /// </summary>
        /// <param name="format">
        /// A composite format string.
        /// </param>
        /// <param name="args">
        /// An object array that contains zero or more objects to format.
        /// </param>
        /// <returns>
        /// A copy of format in which the format items have been replaced by the string
        /// representation of the corresponding objects in args.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// The <paramref name="format"/> or <paramref name="args"/> parameters are null.
        /// </exception>
        /// <exception cref="System.FormatException">
        /// The <paramref name="format"/> parameter is invalid.-or- The index of a format item
        /// is less than zero, or greater than or equal to the length of the args array.
        /// </exception>
        public static string FormatCurrent(this string format, params object[] args)
        {
           return String.Format(CultureInfo.CurrentCulture, format, args);
        }

        /// <summary>
        /// Replaces the format item in a specified string with the string representation of a
        /// corresponding object in a specified array using the current user interface culture
        /// for formatting information.
        /// </summary>
        /// <param name="format">
        /// A composite format string.
        /// </param>
        /// <param name="args">
        /// An object array that contains zero or more objects to format.
        /// </param>
        /// <returns>
        /// A copy of format in which the format items have been replaced by the string
        /// representation of the corresponding objects in args.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// The <paramref name="format"/> or <paramref name="args"/> parameters are null.
        /// </exception>
        /// <exception cref="System.FormatException">
        /// The <paramref name="format"/> parameter is invalid.-or- The index of a format item
        /// is less than zero, or greater than or equal to the length of the args array.
        /// </exception>
        public static string FormatCurrentUI(this string format, params object[] args)
        {
            return String.Format(CultureInfo.CurrentUICulture, format, args);
        }

        /// <summary>
        /// Replaces the format item in a specified string with the string representation of a
        /// corresponding object in a specified array using the invariant culture for
        /// formatting information.
        /// </summary>
        /// <param name="format">
        /// A composite format string.
        /// </param>
        /// <param name="args">
        /// An object array that contains zero or more objects to format.
        /// </param>
        /// <returns>
        /// A copy of format in which the format items have been replaced by the string
        /// representation of the corresponding objects in args.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// The <paramref name="format"/> or <paramref name="args"/> parameters are null.
        /// </exception>
        /// <exception cref="System.FormatException">
        /// The <paramref name="format"/> parameter is invalid.-or- The index of a format item
        /// is less than zero, or greater than or equal to the length of the args array.
        /// </exception>
        public static string FormatInvariant(this string format, params object[] args)
        {
            return String.Format(CultureInfo.InvariantCulture, format, args);
        }

        /// <summary>
        /// Converts the numeric value of this instance to its equivalent string representation
        /// using the invariant culture for formatting information.
        /// </summary>
        /// <param name="value">
        /// The value to convert.
        /// </param>
        /// <returns>
        /// The numeric value of this instance in its equivalent string representation.
        /// </returns>
        public static string ToStringInvariant(this int value)
        {
            return value.ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Converts the numeric value of this instance to its equivalent string representation
        /// using the invariant culture for formatting information.
        /// </summary>
        /// <param name="value">
        /// The value to convert.
        /// </param>
        /// <returns>
        /// The numeric value of this instance in its equivalent string representation.
        /// </returns>
        public static string ToStringInvariant(this long value)
        {
            return value.ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Converts the numeric value of this instance to its equivalent string representation
        /// using the invariant culture for formatting information.
        /// </summary>
        /// <param name="value">
        /// The value to convert.
        /// </param>
        /// <returns>
        /// The numeric value of this instance in its equivalent string representation.
        /// </returns>
        public static string ToStringInvariant(this long? value)
        {
            if (value == null)
            {
                return "NULL";
            }
            else
            {
                return value.Value.ToString(CultureInfo.InvariantCulture);
            }
        }

        /// <summary>
        /// Converts the numeric value of this instance to its equivalent string representation
        /// using the invariant culture for formatting information.
        /// </summary>
        /// <param name="value">
        /// The value to convert.
        /// </param>
        /// <returns>
        /// The numeric value of this instance in its equivalent string representation.
        /// </returns>
        public static string ToStringInvariant(this ulong value)
        {
            return value.ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Converts the numeric value of this instance to its equivalent string representation
        /// using the invariant culture for formatting information.
        /// </summary>
        /// <param name="value">
        /// The value to convert.
        /// </param>
        /// <param name="format">
        /// A numeric format string.
        /// </param>
        /// <returns>
        /// The numeric value of this instance in its equivalent string representation.
        /// </returns>
        public static string ToStringInvariant(this ulong value, string format)
        {
            return value.ToString(format, CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Converts the numeric value of this instance to its equivalent string representation
        /// using the invariant culture for formatting information.
        /// </summary>
        /// <param name="value">
        /// The value to convert.
        /// </param>
        /// <returns>
        /// The numeric value of this instance in its equivalent string representation.
        /// </returns>
        public static string ToStringInvariant(this short value)
        {
            return value.ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Converts the numeric value of this instance to its equivalent string representation
        /// using the invariant culture for formatting information.
        /// </summary>
        /// <param name="value">
        /// The value to convert.
        /// </param>
        /// <returns>
        /// The numeric value of this instance in its equivalent string representation.
        /// </returns>
        public static string ToStringInvariant(this byte value)
        {
            return value.ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Converts the numeric value of this instance to its equivalent string representation
        /// using the invariant culture for formatting information.
        /// </summary>
        /// <param name="value">
        /// The value to convert.
        /// </param>
        /// <returns>
        /// The numeric value of this instance in its equivalent string representation.
        /// </returns>
        public static string ToStringInvariant(this sbyte value)
        {
            return value.ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Converts the boolean value of this instance to either a '0' or a '1'.
        /// </summary>
        /// <param name="value">
        /// The value to convert.
        /// </param>
        /// <returns>
        /// '0' for a false value; otherwise, '1'.
        /// </returns>
        public static char ToNumericalChar(this bool value)
        {
            if (value)
            {
                return '1';
            }

            return '0';
        }

        /// <summary>
        /// Converts the numeric value of this instance to its equivalent string representation
        /// using the invariant culture for formatting information.
        /// </summary>
        /// <param name="value">
        /// The value to convert.
        /// </param>
        /// <param name="format">
        /// A standard or custom numeric format string.
        /// </param>
        /// <returns>
        /// The numeric value of this instance in its equivalent string representation.
        /// </returns>
        public static string ToStringInvariant(this int value, string format)
        {
            return value.ToString(format, CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Converts the numeric value of this instance to its equivalent string representation
        /// using the invariant culture for formatting information.
        /// </summary>
        /// <param name="value">
        /// The value to convert.
        /// </param>
        /// <returns>
        /// The numeric value of this instance in its equivalent string representation.
        /// </returns>
        public static string ToStringInvariant(this uint value)
        {
            return value.ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Converts the numeric value of this instance to its equivalent string representation
        /// using the invariant culture for formatting information.
        /// </summary>
        /// <param name="value">
        /// The value to convert.
        /// </param>
        /// <returns>
        /// The numeric value of this instance in its equivalent string representation.
        /// </returns>
        public static string ToStringInvariant(this ushort value)
        {
            return value.ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Converts the numeric value of this instance to its equivalent string representation
        /// using the invariant culture for formatting information.
        /// </summary>
        /// <param name="value">
        /// The value to convert.
        /// </param>
        /// <returns>
        /// The numeric value of this instance in its equivalent string representation.
        /// </returns>
        public static string ToStringInvariant(this double value)
        {
            return value.ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Converts the numeric value of this instance to its equivalent string representation
        /// using the invariant culture for formatting information.
        /// </summary>
        /// <param name="value">
        /// The value to convert.
        /// </param>
        /// <returns>
        /// The numeric value of this instance in its equivalent string representation.
        /// </returns>
        public static string ToStringInvariant(this float value)
        {
            return value.ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Converts the numeric value of this instance to its equivalent string representation
        /// using the invariant culture for formatting information.
        /// </summary>
        /// <param name="value">
        /// The value to convert.
        /// </param>
        /// <param name="format">
        /// A standard or custom numeric format string.
        /// </param>
        /// <returns>
        /// The numeric value of this instance in its equivalent string representation.
        /// </returns>
        public static string ToStringInvariant(this float value, string format)
        {
            return value.ToString(format, CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Converts the numeric value of this instance to its equivalent string representation
        /// using the invariant culture for formatting information.
        /// </summary>
        /// <param name="value">
        /// The value to convert.
        /// </param>
        /// <param name="format">
        /// A standard or custom numeric format string.
        /// </param>
        /// <returns>
        /// The numeric value of this instance in its equivalent string representation.
        /// </returns>
        public static string ToStringInvariant(this double value, string format)
        {
            return value.ToString(format, CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Replace extension; allowing StringComparison operator.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        public static String Replace(this String source, String from, String to, StringComparison comp)
        {
            int index = source.IndexOf(from, comp);

            // Determine if we found a match
            bool found = index >= 0;
            if (found)
            {
                // Remove the old text
                source = source.Remove(index, from.Length);

                // Add the replacement text
                source = source.Insert(index, to);
            }

            return source;
        }
    } // RSG.Base.Extensions.StringExtensions {Class}
} // RSG.Base.Extensions {Namespace}
