﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace RSG.Base.Extensions
{
    /// <summary>
    /// 
    /// </summary>
    public static class TypeExtensions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="includeAbstract"></param>
        /// <returns></returns>
        public static IEnumerable<Type> GetDerivedTypesInLoadedAssemblies(this Type type, bool includeAbstract = false)
        {
            Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
            foreach (Assembly ass in assemblies.Where(item => !item.IsDynamic))
            {
                Type[] types = ass.GetTypes();
                foreach (Type assType in types)
                {
                    if (!assType.IsAbstract && type.IsAssignableFrom(assType))
                    {
                        yield return assType;
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="ass"></param>
        /// <param name="includeAbstract"></param>
        /// <returns></returns>
        public static IEnumerable<Type> GetDerivedTypeInAssembly(this Type type, Assembly ass, bool includeAbstract = false)
        {
            foreach (Type assType in ass.GetTypes())
            {
                if (!assType.IsAbstract && type.IsAssignableFrom(assType))
                {
                    yield return assType;
                }
            }
        }
    } // TypeExtensions
}
