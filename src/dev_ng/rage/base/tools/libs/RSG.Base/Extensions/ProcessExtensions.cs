﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Management;

namespace RSG.Base.Extensions
{
    /// <summary>
    /// Processor class extension methods.
    /// </summary>
    /// <remarks>
    /// References: 
    /// http://social.msdn.microsoft.com/Forums/en-US/csharpgeneral/thread/d60f0793-cc92-48fb-b867-dd113dabcd5c/
    /// TODO: I reckon the way we are determining parent process ids isn't particularly efficient.
    /// It would be worth investigating using a pinvoked native method instead.
    /// </remarks>
    public static class ProcessExtensions
    {
        #region Constants
        /// <summary>
        /// Maximum number of iterations to find parent process.
        /// </summary>
        private const int MaximumIterations = 10;
        #endregion // Constants

        /// <summary>
        /// Find a processes parent id
        /// </summary>
        /// <param name="pid"></param>
        /// <returns></returns>
        public static int FindParentProcessID(int pid)
        {
            int parentPid = 0;
            using (ManagementObject mo = new ManagementObject("win32_process.handle='" + pid.ToString() + "'"))
            {
                mo.Get();
                parentPid = Convert.ToInt32(mo["ParentProcessId"]);
            }
            return parentPid;
        }


        /// <summary>
        /// Extension method for retrieving a processes parent.
        /// </summary>
        /// <param name="proc"></param>
        /// <returns></returns>
        public static Process Parent(this Process process)
        {
            int parentProcessId = FindParentProcessID(process.Id);
            if (parentProcessId != 0)
            {
                try
                {
                    return FindIndexedProcessName(parentProcessId);
                }
                catch (System.Exception)
                {
                    // Catches the case where the parent exe has exited between getting it's
                    // id and trying to get the process itself.
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Extension method which tells whether the passed in process is running
        /// through the bootstrapper.
        /// </summary>
        /// <param name="process"></param>
        /// <returns></returns>
        public static bool IsBootstrapped(this Process process)
        {
            string parentProcessName = "";

            Process parentProcess = process.Parent();
            if (parentProcess != null)
            {
                parentProcessName = parentProcess.ProcessName;
            }

            // We are running through the bootstrapper if the parent process is called BootStrapper
            // (or BootStrapper.vshost if it's running through visual studio).
            return String.Equals(parentProcess.ProcessName, "BootStrapper", StringComparison.OrdinalIgnoreCase) ||
                String.Equals(parentProcess.ProcessName, "BootStrapper.vshost", StringComparison.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Gets the Process object from a given process id
        /// </summary>
        /// <param name="pid"></param>
        /// <returns></returns>
        public static Process FindIndexedProcessName(int pid)
        {
            return Process.GetProcessById(pid);
        }

        /// <summary>
        /// Returns whether the process is a top-level process
        /// </summary>
        /// <param name="process"></param>
        /// <returns></returns>
        public static bool IsTopLevelProcess(Process process)
        {
            if (process == null)
                return true;

            if (String.Equals(process.ProcessName, "explorer", StringComparison.OrdinalIgnoreCase) ||
                String.Equals(process.ProcessName, "devenv", StringComparison.OrdinalIgnoreCase) ||
                // LPXO: Added to handle T3600 machines.  This fixes an endless loop trying to find the top level process.  
                String.Equals(process.ProcessName, "buildhelper", StringComparison.OrdinalIgnoreCase) || 
                String.Equals(process.ProcessName, "shortcutmenu", StringComparison.OrdinalIgnoreCase) ||
                String.Equals(process.ProcessName, "bootstrapper", StringComparison.OrdinalIgnoreCase) ||
                process.Id == 0)
                return true;

            return false;
        }

        /// <summary>
        /// Returns the top-most parent process from a given process.
        /// </summary>
        /// <param name="process"></param>
        /// <returns></returns>
        public static Process TopMostParent(this Process process)
        {
            Process parentProcess = null;

            try
            {
                parentProcess = FindIndexedProcessName(FindParentProcessID(process.Id));

                // Return our original process because we're already at the top level.
                if (parentProcess == null || IsTopLevelProcess(parentProcess))
                    return process;

                int iteration = 0;
                bool topMost = false;

                while (!topMost && (iteration < MaximumIterations))
                {
                    Process nextProcess = FindIndexedProcessName(FindParentProcessID(parentProcess.Id));

                    if (nextProcess == null || IsTopLevelProcess(nextProcess))
                        topMost = true;
                    else
                        parentProcess = nextProcess;

                    ++iteration;
                }
            }
            catch (Exception)
            { }

            return parentProcess;
        }

        /// <summary>
        /// Kills a process and all its descendant processes.
        /// </summary>
        /// <param name="root"></param>
        public static void KillProcessTree(this Process process)
        {
            IList<Process> processesToKill = new List<Process>();
            GetProcessAndChildren(process, processesToKill);

            foreach (Process p in processesToKill)
            {
                try
                {
                    p.Kill();
                }
                catch (Exception)
                {
                    //Log error?
                }
            }
        }

        /// <summary>
        /// Iteratively gets a list of all descendant processes for the specified parent.
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="output"></param>
        private static void GetProcessAndChildren(Process parent, IList<Process> output)
        {
            foreach (Process p in Process.GetProcesses())
            {
                try
                {
                    if (FindParentProcessID(p.Id) == parent.Id)
                    {
                        GetProcessAndChildren(p, output);
                    }
                }
                catch (System.Exception)
                {
                    // Error while trying to find parent process id (process might no longer exist).
                }
            }
            output.Add(parent);
        }
    }
}
