//
// File: Colours.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of Colours.cs class
//

using System;
using System.Drawing;
using System.Collections.Generic;

namespace RSG.Base.Drawing
{
    
    /// <summary>
    /// 
    /// </summary>
    public static class Colours
    {
        #region Enumerations
        /// <summary>
        /// Enumeration of usable colours
        /// </summary>
        ///
        public static KnownColor[] UseableColours =
        {            
            System.Drawing.KnownColor.Red,
            System.Drawing.KnownColor.Green,
            System.Drawing.KnownColor.Blue,
            System.Drawing.KnownColor.Black,
            System.Drawing.KnownColor.CadetBlue,
            System.Drawing.KnownColor.Brown,
            System.Drawing.KnownColor.Magenta,
            System.Drawing.KnownColor.Navy,
            System.Drawing.KnownColor.Orange,
            System.Drawing.KnownColor.DarkBlue,
            System.Drawing.KnownColor.DarkCyan,
            System.Drawing.KnownColor.DarkGray,
            System.Drawing.KnownColor.DarkGreen,
            System.Drawing.KnownColor.DarkMagenta,
            System.Drawing.KnownColor.DarkOrange,
            System.Drawing.KnownColor.DarkRed,
            System.Drawing.KnownColor.Firebrick,
            System.Drawing.KnownColor.Fuchsia,
            System.Drawing.KnownColor.Gray,
            System.Drawing.KnownColor.HotPink,
            System.Drawing.KnownColor.Cyan,
            System.Drawing.KnownColor.DarkGoldenrod,
            System.Drawing.KnownColor.DarkKhaki,
            System.Drawing.KnownColor.DarkOliveGreen,
            System.Drawing.KnownColor.DarkSeaGreen,
            System.Drawing.KnownColor.DarkTurquoise,
            System.Drawing.KnownColor.Gold,
            System.Drawing.KnownColor.Goldenrod,
        };
        #endregion // Enumerations

        #region Methods
        /// <summary>
        /// Creates an enumeration of varying colours
        /// </summary>
        /// <param name="seedIndex"></param>
        /// <returns></returns>
        public static IEnumerable<Color> VaryingColors(int seedIndex, int alpha)
        {
            int maxValue = 1 << 24;
            int index = seedIndex % maxValue;

            while (true)
            {
                byte r = 0;
                byte g = 0;
                byte b = 0;

                for (int i = 0; i < 24; i++)
                {
                    if ((index & (1 << i)) != 0)
                    {
                        switch (i % 3)
                        {
                            case 0: r |= (byte)(1 << (23 - i) / 3); break;
                            case 1: g |= (byte)(1 << (23 - i) / 3); break;
                            case 2: b |= (byte)(1 << (23 - i) / 3); break;
                        }
                    }
                }

                yield return Color.FromArgb(alpha, r, g, b);

                index = (index + 1) % maxValue;
            }
        }
        #endregion // Methods
    } // Colours

} // End of RSG.Base.Drawing namespace

// End of file
