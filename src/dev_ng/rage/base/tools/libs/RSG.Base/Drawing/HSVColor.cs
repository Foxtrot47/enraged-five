﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Drawing
{
    /// <summary>
    /// Color represented as HSV values.
    /// </summary>
    public class HSVColor
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Hue"/> property.
        /// </summary>
        private double _hue = 0;

        /// <summary>
        /// The private field used for the <see cref="Saturation"/> property.
        /// </summary>
        private double _saturation = 0.0;

        /// <summary>
        /// The private field used for the <see cref="Value"/> property.
        /// </summary>
        private double _value = 0.0;
        #endregion // Fields

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public HSVColor()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hue"></param>
        /// <param name="saturation"></param>
        /// <param name="value"></param>
        public HSVColor(double hue, double saturation, double value)
        {
            _hue = hue;
            _saturation = saturation;
            _value = value;
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// Hue component of the color with a value in the range of [0, 1).
        /// </summary>
        public double Hue
        {
            get { return _hue; }
            set { _hue = value; }
        }
        
        /// <summary>
        /// Saturation component of the color with a value in the range of [0, 1).
        /// </summary>
        public double Saturation
        {
            get { return _saturation; }
            set { _saturation = value; }
        }

        /// <summary>
        /// Value (Brightness) component of the color with a value in the range of [0, 1).
        /// </summary>
        public double Value
        {
            get { return _value; }
            set { _value = value; }
        }
        #endregion // Properties

        #region Overrides
        /// <summary>
        /// String conversion
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return String.Format("HSV: {0:0.##}° {1:P} {2:P}", Hue, Saturation, Value);
        }
        #endregion // Overrides

        #region Conversion Methods
        /// <summary>
        /// Based on C code from:
        /// http://www.cs.rit.edu/~ncs/color/t_convert.html
        /// </summary>
        /// <param name="rgb"></param>
        /// <returns></returns>
        public static HSVColor FromRGB(RGBColor rgb)
        {
            // Convert the rgb values to a value between 0 and 1.
            double r = (double)rgb.Red / 255.0;
            double g = (double)rgb.Green / 255.0;
            double b = (double)rgb.Blue / 255.0;

            double min = System.Math.Min(System.Math.Min(r, g), b);
            double max = System.Math.Max(System.Math.Max(r, g), b);
            double delta = max - min;

            double v = max;
            double h;
            double s;

            if (max == 0 || delta == 0)
            {
                // R, G, and B must be 0, or all the same.
                // In this case, S is 0, and H is undefined.
                // Using H = 0 is as good as any...
                s = 0.0;
                h = 0.0;
            }
            else
            {
                s = delta / max;
                if (r == max)
                {
                    // Between Yellow and Magenta
                    h = (g - b) / delta;
                }
                else if (g == max)
                {
                    // Between Cyan and Yellow
                    h = 2.0 + (b - r) / delta;
                }
                else
                {
                    // Between Magenta and Cyan
                    h = 4.0 + (r - g) / delta;
                }
            }

            // Scale h to be between 0 and 360. 
            // This may require adding 360, if the value is negative.
            h *= 60.0;
            if (h < 0.0)
            {
                h += 360.0;
            }

            return new HSVColor(h, s, v);
        }

        /// <summary>
        /// Explicit conversion from RGBColor.
        /// </summary>
        /// <param name="rgb"></param>
        /// <returns></returns>
        public static explicit operator HSVColor(RGBColor rgb)
        {
            return FromRGB(rgb);
        }
        #endregion // Conversion Methods
    } // HSVColor
}
