//
// File: ColourSpaceConv.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of ColourSpaceConv.cs class
//
// Refercences:
//   http://msdn.microsoft.com/msdnmag/issues/03/07/GDIColorPicker/
//

using System;
using System.Drawing;

namespace RSG.Base.Drawing
{

    #region Structures
    /// <summary>
    /// RGB Structure
    /// </summary>
    public struct RGB
    {
        #region Member Data
        public Byte Red;
        public Byte Green;
        public Byte Blue;
        #endregion // Member Data

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="R"></param>
        /// <param name="G"></param>
        /// <param name="B"></param>
        public RGB(Byte R, Byte G, Byte B)
        {
            this.Red = R;
            this.Green = G;
            this.Blue = B;
        }
        #endregion // Constructor

        #region Public Methods
        /// <summary>
        /// String conversion
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return String.Format("({0}, {1}, {2})", Red, Green, Blue);
        }
        #endregion // Public Methods
    }

    /// <summary>
    /// HSV Structure
    /// </summary>
    public struct HSV
    {
        #region Member Data
        public Byte Hue;
        public Byte Saturation;
        public Byte value;
        #endregion // Member Data

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="H"></param>
        /// <param name="S"></param>
        /// <param name="V"></param>
        public HSV(Byte H, Byte S, Byte V)
        {
            this.Hue = H;
            this.Saturation = S;
            this.value = V;
        }
        #endregion // Constructor

        #region Public Methods
        /// <summary>
        /// String conversion
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return String.Format("({0}, {1}, {2})", Hue, Saturation, value);
        }
        #endregion // Public Methods
    }
    #endregion // Structures

    /// <summary>
    /// Colour-space Conversion Methods
    /// </summary>
    public class ColourSpaceConv
    {
        #region HSL <-> RGB Conversion
        /// <summary>
        /// HSV to RGB Conversion
        /// </summary>
        /// <param name="H"></param>
        /// <param name="S"></param>
        /// <param name="V"></param>
        /// <returns></returns>
        public static RGB HSVtoRGB(Byte H, Byte S, Byte V)
        {
            // H, S, and V must all be between 0 and 255.
            return HSVtoRGB(new HSV(H, S, V));
        }

        /// <summary>
        /// HSV to System.Drawing.Color Conversion
        /// </summary>
        /// <param name="hsv"></param>
        /// <returns></returns>
        public static Color HSVtoColor(HSV hsv)
        {
            RGB RGB = HSVtoRGB(hsv);
            return Color.FromArgb(RGB.Red, RGB.Green, RGB.Blue);
        }

        /// <summary>
        /// HSV to System.Drawing.Color Conversion
        /// </summary>
        /// <param name="H"></param>
        /// <param name="S"></param>
        /// <param name="V"></param>
        /// <returns></returns>
        public static Color HSVtoColor(Byte H, Byte S, Byte V)
        {
            return HSVtoColor(new HSV(H, S, V));
        }

        /// <summary>
        /// HSV to RGB Conversion
        /// </summary>
        /// <param name="HSV"></param>
        /// <returns></returns>
        public static RGB HSVtoRGB(HSV HSV)
        {
            // HSV contains values scaled as in the color wheel:
            // that is, all from 0 to 255. 

            // for ( this code to work, HSV.Hue needs
            // to be scaled from 0 to 360 (it//s the angle of the selected
            // point within the circle). HSV.Saturation and HSV.value must be 
            // scaled to be between 0 and 1.

            double h;
            double s;
            double v;

            double r = 0;
            double g = 0;
            double b = 0;

            // Scale Hue to be between 0 and 360. Saturation
            // and value scale to be between 0 and 1.
            h = ((double)HSV.Hue / 255 * 360) % 360;
            s = (double)HSV.Saturation / 255;
            v = (double)HSV.value / 255;

            if (s == 0)
            {
                // If s is 0, all colors are the same.
                // This is some flavor of gray.
                r = v;
                g = v;
                b = v;
            }
            else
            {
                double p;
                double q;
                double t;

                double fractionalSector;
                int sectorNumber;
                double sectorPos;

                // The color wheel consists of 6 sectors.
                // Figure out which sector you//re in.
                sectorPos = h / 60;
                sectorNumber = (int)(System.Math.Floor(sectorPos));

                // get the fractional part of the sector.
                // That is, how many degrees into the sector
                // are you?
                fractionalSector = sectorPos - sectorNumber;

                // Calculate values for the three axes
                // of the color. 
                p = v * (1 - s);
                q = v * (1 - (s * fractionalSector));
                t = v * (1 - (s * (1 - fractionalSector)));

                // Assign the fractional colors to r, g, and b
                // based on the sector the angle is in.
                switch (sectorNumber)
                {
                    case 0:
                        r = v;
                        g = t;
                        b = p;
                        break;

                    case 1:
                        r = q;
                        g = v;
                        b = p;
                        break;

                    case 2:
                        r = p;
                        g = v;
                        b = t;
                        break;

                    case 3:
                        r = p;
                        g = q;
                        b = v;
                        break;

                    case 4:
                        r = t;
                        g = p;
                        b = v;
                        break;

                    case 5:
                        r = v;
                        g = p;
                        b = q;
                        break;
                }
            }
            // return an RGB structure, with values scaled
            // to be between 0 and 255.
			return new RGB((Byte)System.Math.Round(r * 255.0f, MidpointRounding.AwayFromZero),
                           (Byte)System.Math.Round(g * 255.0f, MidpointRounding.AwayFromZero),
                           (Byte)System.Math.Round(b * 255.0f, MidpointRounding.AwayFromZero));
        }

        /// <summary>
        /// RGB to HSV Conversion
        /// </summary>
        /// <param name="RGB"></param>
        /// <returns></returns>
        public static HSV RGBtoHSV(RGB RGB)
        {
            // In this function, R, G, and B values must be scaled 
            // to be between 0 and 1.
            // HSV.Hue will be a value between 0 and 360, and 
            // HSV.Saturation and value are between 0 and 1.
            // The code must scale these to be between 0 and 255 for
            // the purposes of this application.

            double min;
            double max;
            double delta;

            double r = (double)RGB.Red / 255;
            double g = (double)RGB.Green / 255;
            double b = (double)RGB.Blue / 255;

            double h;
            double s;
            double v;

            min = System.Math.Min(System.Math.Min(r, g), b);
            max = System.Math.Max(System.Math.Max(r, g), b);
            v = max;
            delta = max - min;
            if (max == 0 || delta == 0)
            {
                // R, G, and B must be 0, or all the same.
                // In this case, S is 0, and H is undefined.
                // Using H = 0 is as good as any...
                s = 0;
                h = 0;
            }
            else
            {
                s = delta / max;
                if (r == max)
                {
                    // Between Yellow and Magenta
                    h = (g - b) / delta;
                }
                else if (g == max)
                {
                    // Between Cyan and Yellow
                    h = 2 + (b - r) / delta;
                }
                else
                {
                    // Between Magenta and Cyan
                    h = 4 + (r - g) / delta;
                }

            }
            // Scale h to be between 0 and 360. 
            // This may require adding 360, if the value
            // is negative.
            h *= 60;
            if (h < 0)
            {
                h += 360;
            }

            // Scale to the requirements of this 
            // application. All values are between 0 and 255.
			return new HSV((Byte)System.Math.Round(h / 360 * 255.0f, MidpointRounding.AwayFromZero),
                           (Byte)System.Math.Round(s * 255.0f, MidpointRounding.AwayFromZero),
                           (Byte)System.Math.Round(v * 255.0f, MidpointRounding.AwayFromZero));
        }
        #endregion // HSL <-> RGB Conversion
    }

} // End of RSG.Base.Drawing namespace

// End of file
