﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Drawing
{
    /// <summary>
    /// 
    /// </summary>
    public class HSLColor
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Hue"/> property.
        /// </summary>
        private double _hue = 0;

        /// <summary>
        /// The private field used for the <see cref="Saturation"/> property.
        /// </summary>
        private double _saturation = 0.0;

        /// <summary>
        /// The private field used for the <see cref="Lightness"/> property.
        /// </summary>
        private double _lightness = 0.0;
        #endregion // Fields

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public HSLColor()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hue"></param>
        /// <param name="saturation"></param>
        /// <param name="lightness"></param>
        public HSLColor(double hue, double saturation, double lightness)
        {
            _hue = hue;
            _saturation = saturation;
            _lightness = lightness;
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// Hue component of the color with a value in the range of [0, 1).
        /// </summary>
        public double Hue
        {
            get { return _hue; }
            set { _hue = value; }
        }
        
        /// <summary>
        /// Saturation component of the color with a value in the range of [0, 1).
        /// </summary>
        public double Saturation
        {
            get { return _saturation; }
            set { _saturation = value; }
        }

        /// <summary>
        /// Lightness (or Luminosity) component of the color with a value in the range of [0, 1).
        /// </summary>
        public double Lightness
        {
            get { return _lightness; }
            set { _lightness = value; }
        }
        #endregion // Properties

        #region Overrides
        /// <summary>
        /// String conversion
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return String.Format("HSL: {0:0.##}° {1:P} {2:P}", Hue * 360, Saturation, Lightness);
        }
        #endregion // Overrides


        #region Conversion Methods
        /// <summary>
        /// Based off of pseudocode from:
        /// http://easyrgb.com/index.php?X=MATH&H=18#text18
        /// </summary>
        /// <param name="rgb"></param>
        /// <returns></returns>
        public static HSLColor FromRGB(RGBColor rgb)
        {
            double r = (rgb.Red / 255.0);
            double g = (rgb.Green / 255.0);
            double b = (rgb.Blue / 255.0);

            double min = System.Math.Min(System.Math.Min(r, g), b);
            double max = System.Math.Max(System.Math.Max(r, g), b);
            double delta = max - min;

            double h;
            double s;
            double l = (float)((max + min) / 2.0f);

            if (delta == 0)
            {
                // This is a grayscale colour (can't know h/s).
                h = 0.0;
                s = 0.0;
            }
            else
            {
                if (l < 0.5)
                {
                    s = delta / (max + min);
                }
                else
                {
                    s = delta / (2.0 - max - min);
                }

                double dR = (((max - r) / 6.0) + (delta / 2.0)) / delta;
                double dG = (((max - g) / 6.0) + (delta / 2.0)) / delta;
                double dB = (((max - b) / 6.0) + (delta / 2.0)) / delta;

                if (r == max)
                {
                    h = dB - dG;
                }
                else if (g == max)
                {
                    h = (1.0 / 3.0) + (dR - dB);
                }
                else //if (b == max)
                {
                    h = (2.0 / 3.0) + (dG - dR);
                }

                if (h < 0.0)
                {
                    h += 1.0;
                }
                if (h > 1.0)
                {
                    h -= 1.0;
                }
            }

            return new HSLColor(h, s, l);
        }

        /// <summary>
        /// Explicit conversion from RGBColor.
        /// </summary>
        /// <param name="rgb"></param>
        /// <returns></returns>
        public static explicit operator HSLColor(RGBColor rgb)
        {
            return FromRGB(rgb);
        }
        #endregion // Conversion Methods
    } // HSLColor
}
