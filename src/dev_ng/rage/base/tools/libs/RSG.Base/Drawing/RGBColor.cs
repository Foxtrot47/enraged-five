﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Drawing
{
    /// <summary>
    /// Color represented as RGB values.
    /// </summary>
    public class RGBColor
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Red"/> property.
        /// </summary>
        private byte _red = 0;

        /// <summary>
        /// The private field used for the <see cref="Green"/> property.
        /// </summary>
        private byte _green = 0;

        /// <summary>
        /// The private field used for the <see cref="Blue"/> property.
        /// </summary>
        private byte _blue = 0;
        #endregion // Fields

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public RGBColor()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="red"></param>
        /// <param name="green"></param>
        /// <param name="blue"></param>
        public RGBColor(byte red, byte green, byte blue)
        {
            _red = red;
            _green = green;
            _blue = blue;
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// Red component of the color with a value in the range of [0, 255].
        /// </summary>
        public byte Red
        {
            get { return _red; }
            set { _red = value; }
        }
        
        /// <summary>
        /// Green component of the color with a value in the range of [0, 255].
        /// </summary>
        public byte Green
        {
            get { return _green; }
            set { _green = value; }
        }

        /// <summary>
        /// Blue component of the color with a value in the range of [0, 255].
        /// </summary>
        public byte Blue
        {
            get { return _blue; }
            set { _blue = value; }
        }
        #endregion // Properties

        #region Overrides
        /// <summary>
        /// String conversion
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            //TODO: Provide alternative hex representations of a colour.
            return String.Format("RGB: {0} {1} {2}", Red, Green, Blue);
        }
        #endregion // Overrides

        #region Conversion Methods
        /// <summary>
        /// Based on C code from here:
        /// http://www.cs.rit.edu/~ncs/color/t_convert.html
        /// </summary>
        /// <param name="hsv"></param>
        /// <returns></returns>
        public static RGBColor FromHSV(HSVColor hsv)
        {
            double r, g, b;

            double h = hsv.Hue;
            double s = hsv.Saturation;
            double v = hsv.Value;

            // If saturation is 0, this is some flavour of gray.
            if (s == 0)
            {
                r = v;
                g = v;
                b = v;
            }
            else
            {
                // The color wheel consists of 6 sectors.
                // Figure out which sector you//re in.
                double sectorPos = h / 60;
                int sectorNumber = (int)(System.Math.Floor(sectorPos));

                // get the fractional part of the sector.
                // That is, how many degrees into the sector
                // are you?
                double fractionalSector = sectorPos - sectorNumber;

                // Calculate values for the three axes
                // of the color. 
                double p = v * (1 - s);
                double q = v * (1 - (s * fractionalSector));
                double t = v * (1 - (s * (1 - fractionalSector)));

                // Assign the fractional colors to r, g, and b
                // based on the sector the angle is in.
                switch (sectorNumber)
                {
                    case 0:
                        r = v;
                        g = t;
                        b = p;
                        break;

                    case 1:
                        r = q;
                        g = v;
                        b = p;
                        break;

                    case 2:
                        r = p;
                        g = v;
                        b = t;
                        break;

                    case 3:
                        r = p;
                        g = q;
                        b = v;
                        break;

                    case 4:
                        r = t;
                        g = p;
                        b = v;
                        break;

                    case 5:
                        r = v;
                        g = p;
                        b = q;
                        break;

                    default:
                        throw new ArgumentException("sectorNumber");
                }
            }

            // return an RGBColor object, with values scaled to be between 0 and 255.
            return new RGBColor((Byte)System.Math.Round(r * 255.0f, MidpointRounding.AwayFromZero),
                                (Byte)System.Math.Round(g * 255.0f, MidpointRounding.AwayFromZero),
                                (Byte)System.Math.Round(b * 255.0f, MidpointRounding.AwayFromZero));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hsl"></param>
        /// <returns></returns>
        public static RGBColor FromHSL(HSLColor hsl)
        {
            byte r, g, b;

            if (hsl.Saturation == 0)
            {
                r = (byte)System.Math.Round(hsl.Lightness * 255.0, MidpointRounding.AwayFromZero);
                g = (byte)System.Math.Round(hsl.Lightness * 255.0, MidpointRounding.AwayFromZero);
                b = (byte)System.Math.Round(hsl.Lightness * 255.0, MidpointRounding.AwayFromZero);
            }
            else
            {
                double t2;
                if (hsl.Lightness < 0.5)
                {
                    t2 = hsl.Lightness * (1.0 + hsl.Saturation);
                }
                else
                {
                    t2 = (hsl.Lightness + hsl.Saturation) - (hsl.Lightness * hsl.Saturation);
                }
                double t1 = 2.0 * hsl.Lightness - t2;

                double tr = ColorCalc(t1, t2, hsl.Hue + (1.0 / 3.0));
                double tg = ColorCalc(t1, t2, hsl.Hue);
                double tb = ColorCalc(t1, t2, hsl.Hue - (1.0 / 3.0));
                r = (byte)System.Math.Round(tr * 255.0, MidpointRounding.AwayFromZero);
                g = (byte)System.Math.Round(tg * 255.0, MidpointRounding.AwayFromZero);
                b = (byte)System.Math.Round(tb * 255.0, MidpointRounding.AwayFromZero);
            }
            return new RGBColor(r, g, b);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="c"></param>
        /// <param name="t1"></param>
        /// <param name="t2"></param>
        /// <returns></returns>
        private static double ColorCalc(double t1, double t2, double th)
        {
            if (th < 0)
            {
                th += 1.0;
            }
            if (th > 1)
            {
                th -= 1.0;
            }
            if (6.0 * th < 1.0)
            {
                return t1 + (t2 - t1) * 6.0 * th;
            }
            if (2.0 * th < 1.0)
            {
                return t2;
            }
            if (3.0 * th < 2.0)
            {
                return t1 + (t2 - t1) * (2.0 / 3.0 - th) * 6.0;
            }
            return t1;
        }

        /// <summary>
        /// Explicit conversion from HSVColor.
        /// </summary>
        /// <param name="hsv"></param>
        /// <returns></returns>
        public static explicit operator RGBColor(HSVColor hsv)
        {
            return FromHSV(hsv);
        }

        /// <summary>
        /// Explicit conversion from HSLColor.
        /// </summary>
        /// <param name="hsl"></param>
        /// <returns></returns>
        public static explicit operator RGBColor(HSLColor hsl)
        {
            return FromHSL(hsl);
        }
        #endregion // Conversion Methods
    } // RGBColor
}
