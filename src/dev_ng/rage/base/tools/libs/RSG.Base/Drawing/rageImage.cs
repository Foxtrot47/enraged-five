using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;

namespace RSG.Base.Drawing
{
	/// <summary>
	/// Useful functions for dealing with image files
	/// 
	/// Example usage:
	/// 
	//Image imgPhotoVert = Image.FromFile(WorkingDirectory + @"\rageImage_vert.jpg");
	//Image imgPhotoHoriz = Image.FromFile(WorkingDirectory + @"\rageImage_horiz.jpg");
	//Image imgPhoto = null;

	//imgPhoto = ScaleByPercent(imgPhotoVert, 50);
	//imgPhoto.Save(WorkingDirectory + @"\images\rageImage_1.jpg", ImageFormat.Jpeg);
	//imgPhoto.Dispose();

	//imgPhoto = ConstrainProportions(imgPhotoVert, 200, Dimensions.Width);
	//imgPhoto.Save(WorkingDirectory + @"\images\rageImage_2.jpg", ImageFormat.Jpeg);
	//imgPhoto.Dispose();

	//imgPhoto = ResizeToFixedSize(imgPhotoVert, 200, 200);
	//imgPhoto.Save(WorkingDirectory + @"\images\rageImage_3.jpg", ImageFormat.Jpeg);
	//imgPhoto.Dispose();

	//imgPhoto = Crop(imgPhotoVert, 200, 200, AnchorPosition.Center);
	//imgPhoto.Save(WorkingDirectory + @"\images\rageImage_4.jpg", ImageFormat.Jpeg);
	//imgPhoto.Dispose();

	//imgPhoto = Crop(imgPhotoHoriz, 200, 200, AnchorPosition.Center);
	//imgPhoto.Save(WorkingDirectory + @"\images\rageImage_5.jpg", ImageFormat.Jpeg);
	//imgPhoto.Dispose();
	/// 
	/// </summary>
	public class rageImage
	{
		public enum Dimensions 
		{
			Width,
			Height
		}
		public enum AnchorPosition
		{
			Top,
			Center,
			Bottom,
			Left,
			Right
		}

		static public Image ScaleByPercent(Image imgPhoto, int Percent)
		{
			float nPercent = ((float)Percent/100);

			int sourceWidth = imgPhoto.Width;
			int sourceHeight = imgPhoto.Height;
			int sourceX = 0;
			int sourceY = 0;

			int destX = 0;
			int destY = 0; 
			int destWidth  = (int)(sourceWidth * nPercent);
			int destHeight = (int)(sourceHeight * nPercent);

			Bitmap bmPhoto = new Bitmap(destWidth, destHeight, PixelFormat.Format24bppRgb);
			bmPhoto.SetResolution(imgPhoto.HorizontalResolution, imgPhoto.VerticalResolution);

			Graphics grPhoto = Graphics.FromImage(bmPhoto);
			grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;

			grPhoto.DrawImage(imgPhoto, 
				new Rectangle(destX,destY,destWidth,destHeight),
				new Rectangle(sourceX,sourceY,sourceWidth,sourceHeight),
				GraphicsUnit.Pixel);

			grPhoto.Dispose();
			return bmPhoto;
		}

		static public Image ConstrainProportions(Image imgPhoto, int Size, Dimensions Dimension)
		{
			int sourceWidth = imgPhoto.Width;
			int sourceHeight = imgPhoto.Height;
			int sourceX = 0;
			int sourceY = 0;
			int destX = 0;
			int destY = 0; 
			float nPercent = 0;

			switch(Dimension)
			{
				case Dimensions.Width:
					nPercent = ((float)Size/(float)sourceWidth);
					break;
				default:
					nPercent = ((float)Size/(float)sourceHeight);
					break;
			}
				
			int destWidth  = (int)(sourceWidth * nPercent);
			int destHeight = (int)(sourceHeight * nPercent);

			Bitmap bmPhoto = new Bitmap(destWidth, destHeight, PixelFormat.Format24bppRgb);
			bmPhoto.SetResolution(imgPhoto.HorizontalResolution, imgPhoto.VerticalResolution);

			Graphics grPhoto = Graphics.FromImage(bmPhoto);
			grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;

			grPhoto.DrawImage(imgPhoto, 
			new Rectangle(destX,destY,destWidth,destHeight),
			new Rectangle(sourceX,sourceY,sourceWidth,sourceHeight),
			GraphicsUnit.Pixel);

			grPhoto.Dispose();
			return bmPhoto;
		}

		static public Image ResizeToFixedSize(Image imgPhoto, int Width, int Height)
		{
			int sourceWidth = imgPhoto.Width;
			int sourceHeight = imgPhoto.Height;
			int sourceX = 0;
			int sourceY = 0;
			int destX = 0;
			int destY = 0; 

			float nPercent = 0;
			float nPercentW = 0;
			float nPercentH = 0;

			nPercentW = ((float)Width/(float)sourceWidth);
			nPercentH = ((float)Height/(float)sourceHeight);

			//if we have to pad the height pad both the top and the bottom
			//with the difference between the scaled height and the desired height
			if(nPercentH < nPercentW)
			{
				nPercent = nPercentH;
				destX = (int)((Width - (sourceWidth * nPercent))/2);
			}
			else
			{
				nPercent = nPercentW;
				destY = (int)((Height - (sourceHeight * nPercent))/2);
			}
		
			int destWidth  = (int)(sourceWidth * nPercent);
			int destHeight = (int)(sourceHeight * nPercent);

			Bitmap bmPhoto = new Bitmap(Width, Height, PixelFormat.Format24bppRgb);
			bmPhoto.SetResolution(imgPhoto.HorizontalResolution, imgPhoto.VerticalResolution);

			Graphics grPhoto = Graphics.FromImage(bmPhoto);
			grPhoto.Clear(Color.Red);
			grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;

			grPhoto.DrawImage(imgPhoto, 
				new Rectangle(destX,destY,destWidth,destHeight),
				new Rectangle(sourceX,sourceY,sourceWidth,sourceHeight),
				GraphicsUnit.Pixel);

			grPhoto.Dispose();
			return bmPhoto;
		}
		static public Image Crop(Image imgPhoto, int Width, int Height, AnchorPosition Anchor)
		{
			int sourceWidth = imgPhoto.Width;
			int sourceHeight = imgPhoto.Height;
			int sourceX = 0;
			int sourceY = 0;
			int destX = 0;
			int destY = 0;

			float nPercent = 0;
			float nPercentW = 0;
			float nPercentH = 0;

			nPercentW = ((float)Width/(float)sourceWidth);
			nPercentH = ((float)Height/(float)sourceHeight);

			if(nPercentH < nPercentW)
			{
				nPercent = nPercentW;
				switch(Anchor)
				{
					case AnchorPosition.Top:
						destY = 0;
						break;
					case AnchorPosition.Bottom:
						destY = (int)(Height - (sourceHeight * nPercent));
						break;
					default:
						destY = (int)((Height - (sourceHeight * nPercent))/2);
						break;
				}				
			}
			else
			{
				nPercent = nPercentH;
				switch(Anchor)
				{
					case AnchorPosition.Left:
						destX = 0;
						break;
					case AnchorPosition.Right:
						destX = (int)(Width - (sourceWidth * nPercent));
						break;
					default:
						destX = (int)((Width - (sourceWidth * nPercent))/2);
						break;
				}			
			}

			int destWidth  = (int)(sourceWidth * nPercent);
			int destHeight = (int)(sourceHeight * nPercent);

			Bitmap bmPhoto = new Bitmap(Width, Height, PixelFormat.Format24bppRgb);
			bmPhoto.SetResolution(imgPhoto.HorizontalResolution, imgPhoto.VerticalResolution);

			Graphics grPhoto = Graphics.FromImage(bmPhoto);
			grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;

			grPhoto.DrawImage(imgPhoto, 
				new Rectangle(destX,destY,destWidth,destHeight),
				new Rectangle(sourceX,sourceY,sourceWidth,sourceHeight),
				GraphicsUnit.Pixel);

			grPhoto.Dispose();
			return bmPhoto;
		}

		// ****************************************************************************************************************************************************************************************
		// Stole below functions from here http://support.microsoft.com/default.aspx?scid=kb;EN-US;Q324790
		private static ImageCodecInfo GetEncoderInfo(String mimeType)
		{
			int j;
			ImageCodecInfo[] encoders;
			encoders = ImageCodecInfo.GetImageEncoders();
			for (j = 0; j < encoders.Length; ++j)
			{
				if (encoders[j].MimeType == mimeType)
					return encoders[j];
			}
			return null;
		}

		static public void SaveJPGWithCompressionSetting(Image image, string szFileName, long lCompression)
		{
			EncoderParameters eps = new EncoderParameters(1);
			eps.Param[0] = new EncoderParameter(Encoder.Quality, lCompression);
			ImageCodecInfo ici = GetEncoderInfo("image/jpeg");
			image.Save(szFileName, ici, eps);
		}
		// Stole above functions from here http://support.microsoft.com/default.aspx?scid=kb;EN-US;Q324790
		// ****************************************************************************************************************************************************************************************

        public static void AdjustForResolution( Size lastScreenSize, Rectangle currentScreenRect,
            ref Point location, ref Size size )
        {
            Rectangle lastScreenRect = new Rectangle( new Point( 0, 0 ), lastScreenSize );
            Rectangle windowRect = new Rectangle( location, size );

            bool intersectsWithLastScreen = windowRect.IntersectsWith( lastScreenRect );
            bool intersectsWithCurrentScreen = currentScreenRect.IntersectsWith( windowRect );

            // indicates we've never stored the lastScreenSize before
            if ( (lastScreenSize.Width == 0) && (lastScreenSize.Height == 0)  )
            {
                // just make sure everything is on screen
                if ( !intersectsWithCurrentScreen )
                {
                    int x = location.X;
                    if ( location.X < currentScreenRect.Left )
                    {
                        x = currentScreenRect.Left;
                    }
                    else if ( location.X > currentScreenRect.Right )
                    {
                        x = System.Math.Max( 0, currentScreenRect.Right - size.Width );
                    }

                    int y = location.Y;
                    if ( location.Y < currentScreenRect.Top )
                    {
                        y = currentScreenRect.Top;
                    }
                    else if ( location.Y > currentScreenRect.Bottom )
                    {
                        y = System.Math.Max( 0, currentScreenRect.Bottom - size.Height );
                    }

                    location = new Point( x, y );

                    int w = size.Width;
                    if ( x + size.Width > currentScreenRect.Width )
                    {
                        w = currentScreenRect.Width - x;
                    }

                    int h = size.Height;
                    if ( y + size.Height > currentScreenRect.Height )
                    {
                        h = currentScreenRect.Height - y;
                    }

                    size = new Size( w, h );
                }
            }
            else if ( !intersectsWithLastScreen )
            {
                location = new Point( 0, 0 );

                // only shrink the size if the last screen size was bigger than the current and part of our window is off screen
                int w = size.Width;
                if ( (lastScreenSize.Width > currentScreenRect.Width) && (location.X + size.Width > currentScreenRect.Width) )
                {
                    w = currentScreenRect.Width - location.X;
                }

                int h = size.Height;
                if ( (lastScreenSize.Height > currentScreenRect.Height) && (location.Y + size.Height > currentScreenRect.Height) )
                {
                    h = currentScreenRect.Height - location.Y;
                }

                size = new Size( w, h );
            }
            else
            {
                if ( lastScreenSize != currentScreenRect.Size )
                {
                    // scale the location accordingly
                    float xRatio = (float)location.X / (float)lastScreenSize.Width;
                    float yRatio = (float)location.Y / (float)lastScreenSize.Height;

                    // the new location is a ratio
                    int x = Convert.ToInt32( xRatio * currentScreenRect.Width );
                    int y = Convert.ToInt32( yRatio * currentScreenRect.Height );

                    location = new Point( x, y );

                    // only shrink the size if the last screen size was bigger than the current and part of our window is off screen
                    int w = size.Width;
                    if ( (lastScreenSize.Width > currentScreenRect.Width) && (location.X + size.Width > currentScreenRect.Width) )
                    {
                        w = currentScreenRect.Width - location.X;
                    }

                    int h = size.Height;
                    if ( (lastScreenSize.Height > currentScreenRect.Height) && (location.Y + size.Height > currentScreenRect.Height) )
                    {
                        h = currentScreenRect.Height - location.Y;
                    }

                    size = new Size( w, h );
                }
            }
        }

        public static Rectangle GetScreenBounds()
        {
            Rectangle totalBounds = new Rectangle( 0, 0, 0, 0 );

            // Calculate a bounds rectangle that encompasses all screens
            foreach (System.Windows.Forms.Screen screen in System.Windows.Forms.Screen.AllScreens)
            {
                totalBounds.X = System.Math.Min( totalBounds.X, screen.Bounds.X );
                totalBounds.Y = System.Math.Min( totalBounds.Y, screen.Bounds.Y );
                totalBounds.Width = (System.Math.Max(totalBounds.Right, screen.Bounds.Right)) - totalBounds.X;
                totalBounds.Height = (System.Math.Max(totalBounds.Bottom, screen.Bounds.Bottom)) - totalBounds.Y;
            }

            return totalBounds;
        }
	}
}
