using System;
using System.Threading;
using System.Threading.Tasks;
using RSG.Base.Logging;

namespace RSG.Base.Threading
{
    /// <summary>
    /// Custom task that associates a name with the particular task
    /// </summary>
	public class NamedTask : Task
    {
        #region Properties
        /// <summary>
        /// Name of the task
        /// </summary>
        public string Name
        {
            get;
            protected set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="action"></param>
        /// <param name="state"></param>
        /// <param name="cancellationToken"></param>
        public NamedTask(string name, Action<object> action, object state, CancellationToken cancellationToken)
            : base(action, state, cancellationToken)
        {
            Name = name;
        }
        #endregion // Constructor(s)

        #region Task Overrides
        /// <summary>
        /// 
        /// </summary>
        public new void Start()
        {
            //Log.Log__Message("Executing the '{0}' task (asynchronously)", Name);
            base.Start();
        }

        /// <summary>
        /// 
        /// </summary>
        public new void RunSynchronously()
        {
            //Log.Log__Message("Executing the '{0}' task (synchronously)", Name);
            base.RunSynchronously();
        }
        #endregion // Task Overrides
    } // NamedTask
}
