﻿using System;

namespace RSG.Base
{

    /// <summary>
    /// Endianness enumeration.
    /// </summary>
    public enum Endianness
    {
        /// <summary>
        /// Little endian; least significant byte first.
        /// </summary>
        LittleEndian,

        /// <summary>
        /// Big endian; most significant byte first.
        /// </summary>
        BigEndian,
    }

} // RSG.Base namespace
