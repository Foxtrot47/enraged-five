﻿//---------------------------------------------------------------------------------------------
// <copyright file="ProcessorInfo.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Management;

namespace RSG.Base.SystemSpecs
{
    /// <summary>
    /// System processor information.
    /// </summary>
    /// <remarks>
    /// Reference: http://msdn.microsoft.com/en-us/library/aa394373(VS.85).aspx
    /// </remarks>
    public sealed class ProcessorInfo
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="Name"/> property.
        /// </summary>
        private readonly string _name;

        /// <summary>
        /// Private field for the <see cref="NumberOfLogicalProcessors"/> property.
        /// </summary>
        private readonly uint _numberOfLogicalProcessors;

        /// <summary>
        /// Private field for the <see cref="NumberOfCores"/> property.
        /// </summary>
        private readonly uint _numberOfCores;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ProcessorInfo"/> class using the
        /// specified management scope.
        /// </summary>
        /// <param name="scope"></param>
        internal ProcessorInfo(ManagementScope scope)
        {
            ObjectQuery query = new ObjectQuery("SELECT * FROM Win32_Processor");
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(scope, query);
            ManagementObjectCollection queryCollection = searcher.Get();

            foreach (ManagementObject mo in queryCollection)
            {
                _name = (String)mo["Name"];
                _numberOfLogicalProcessors = (uint)mo["NumberOfLogicalProcessors"];
                _numberOfCores = (uint)mo["NumberOfCores"];
                mo.Dispose();
            }

            queryCollection.Dispose();
            searcher.Dispose();
        }
        #endregion

        #region Properties
        /// <summary>
        /// Label by which the object is known.
        /// </summary>
        public String Name
        {
            get { return _name; }
        }

        /// <summary>
        /// Number of logical processors for the current instance of the processor.
        /// For processors capable of hyperthreading, this value includes only the processors which have hyperthreading enabled. 
        /// </summary>
        public uint NumberOfLogicalProcessors
        {
            get { return _numberOfLogicalProcessors; }
        }

        /// <summary>
        /// Number of cores for the current instance of the processor.
        /// A core is a physical processor on the integrated circuit.
        /// For example, in a dual-core processor this property has a value of 2.
        /// </summary>
        public uint NumberOfCores
        {
            get { return _numberOfCores; }
        }
        #endregion
    }
}
