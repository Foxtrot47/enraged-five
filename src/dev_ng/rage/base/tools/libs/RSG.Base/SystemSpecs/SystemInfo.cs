﻿//---------------------------------------------------------------------------------------------
// <copyright file="SystemInfo.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Management;

namespace RSG.Base.SystemSpecs
{
    /// <summary>
    /// Class for retrieving information about the user's system based on information obtained via the WMI interface.
    /// e.g. CPU/Memory details.
    /// 
    /// This could be extended to retrieve remote system information by changing the code used to create the management
    /// scope, but that isn't needed at the moment.
    /// </summary>
    public static class SystemInfo
    {
        #region Member Data
        /// <summary>
        /// Private field for the <see cref="GraphicsInfo"/> property.
        /// </summary>
        private static GraphicsInfo _graphicsInfo;

        /// <summary>
        /// Private field for the <see cref="MemoryInfo"/> property.
        /// </summary>
        private static MemoryInfo _memoryInfo;

        /// <summary>
        /// Private field for the <see cref="ProcessorInfo"/> property.
        /// </summary>
        private static ProcessorInfo _processorInfo;

        /// <summary>
        /// WMI Management scope.
        /// </summary>
        private static readonly ManagementScope _scope;
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a management scope for use by the sub info classes.
        /// </summary>
        static SystemInfo()
        {
            _scope = new ManagementScope(String.Format(@"\\{0}\root\cimv2", Environment.MachineName));
            _scope.Connect();
        }
        #endregion

        #region Properties
        /// <summary>
        /// Information about the graphics card.
        /// </summary>
        public static GraphicsInfo GraphicsCard
        {
            get
            {
                if (_graphicsInfo == null)
                {
                    _graphicsInfo = new GraphicsInfo(_scope);
                }
                return _graphicsInfo;
            }
        }

        /// <summary>
        /// Information about the memory.
        /// </summary>
        public static MemoryInfo Memory
        {
            get
            {
                if (_memoryInfo == null)
                {
                    _memoryInfo = new MemoryInfo(_scope);
                }
                return _memoryInfo;
            }
        }

        /// <summary>
        /// Information about the processor.
        /// </summary>
        public static ProcessorInfo Processor
        {
            get
            {
                if (_processorInfo == null)
                {
                    _processorInfo = new ProcessorInfo(_scope);
                }
                return _processorInfo;
            }
        }
        #endregion
    }
}
