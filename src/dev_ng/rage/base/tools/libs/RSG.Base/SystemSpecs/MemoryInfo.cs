﻿//---------------------------------------------------------------------------------------------
// <copyright file="MemoryInfo.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System.Management;

namespace RSG.Base.SystemSpecs
{
    /// <summary>
    /// System memory information.
    /// </summary>
    /// <remarks>
    /// Reference: http://msdn.microsoft.com/en-us/library/aa394347(v=vs.85).aspx
    /// </remarks>
    public sealed class MemoryInfo
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="Capacity"/> property.
        /// </summary>
        private readonly ulong _capacity;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="MemoryInfo"/> class using the
        /// specified management scope.
        /// </summary>
        /// <param name="scope"></param>
        internal MemoryInfo(ManagementScope scope)
        {
            ObjectQuery query = new ObjectQuery("SELECT * FROM Win32_PhysicalMemory");
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(scope, query);
            ManagementObjectCollection queryCollection = searcher.Get();

            foreach (ManagementObject mo in queryCollection)
            {
                _capacity += (ulong)mo["Capacity"];
                mo.Dispose();
            }

            queryCollection.Dispose();
            searcher.Dispose();
        }
        #endregion

        #region Properties
        /// <summary>
        /// Total capacity of the physical memory—in bytes.
        /// </summary>
        public ulong Capacity
        {
            get { return _capacity; }
        }
        #endregion
    }
}
