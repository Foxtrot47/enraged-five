﻿//---------------------------------------------------------------------------------------------
// <copyright file="GraphicsInfo.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Management;

namespace RSG.Base.SystemSpecs
{
    /// <summary>
    /// Graphics card information.
    /// </summary>
    /// <remarks>
    /// Reference: http://msdn.microsoft.com/en-us/library/aa394512(v=vs.85).aspx
    /// </remarks>
    public sealed class GraphicsInfo
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="Name"/> property.
        /// </summary>
        private readonly string _name;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="GraphicsInfo"/> class using the
        /// specified management scope.
        /// </summary>
        /// <param name="scope"></param>
        internal GraphicsInfo(ManagementScope scope)
        {
            ObjectQuery query = new ObjectQuery("SELECT * FROM Win32_VideoController");
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(scope, query);
            ManagementObjectCollection queryCollection = searcher.Get();

            foreach (ManagementObject mo in queryCollection)
            {
                _name = (String)mo["Name"];
                mo.Dispose();
            }

            queryCollection.Dispose();
            searcher.Dispose();
        }
        #endregion

        #region Properties
        /// <summary>
        /// Label by which the object is known.
        /// </summary>
        public String Name
        {
            get { return _name; }
        }
        #endregion
    }
}
