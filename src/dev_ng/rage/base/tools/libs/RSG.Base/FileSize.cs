﻿using System.Globalization;
using System;

namespace RSG.Base
{

    /// <summary>
    /// FileSize class representation; for pretty formatting File Size strings.
    /// </summary>
    /// <see cref="http://msdn.microsoft.com/en-us/library/26etazsy.aspx#IFormattable" />
    /// 
    public class FileSize : IFormattable
    {
        #region Properties
        /// <summary>
        /// Number of bytes.
        /// </summary>
        public ulong Bytes
        {
            get;
            private set;
        }

        /// <summary>
        /// Number of kilobytes.
        /// </summary>
        public Decimal KiloBytes
        {
            get { return (this.Bytes / OneKiloByte); }
        }

        /// <summary>
        /// Number of megabytes.
        /// </summary>
        public Decimal MegaBytes
        {
            get { return (this.Bytes / OneMegaByte); }
        }

        /// <summary>
        /// Number of gigabytes.
        /// </summary>
        public Decimal GigaBytes
        {
            get { return (this.Bytes / OneGigaByte); }
        }

        /// <summary>
        /// Number of terabytes.
        /// </summary>
        public Decimal TeraBytes
        {
            get { return (this.Bytes / OneTeraByte); }
        }

        // We'll stop there for now.
        #endregion // Properties

        #region Member Data
        private const Decimal OneKiloByte = 1024M;
        private const Decimal OneMegaByte = OneKiloByte * 1024M;
        private const Decimal OneGigaByte = OneMegaByte * 1024M;
        private const Decimal OneTeraByte = OneGigaByte * 1024M;
        #endregion // Member Data
        
        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="bytes"></param>
        public FileSize(long bytes)
        {
            this.Bytes = (ulong)bytes;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="bytes"></param>
        public FileSize(ulong bytes)
        {
            this.Bytes = bytes;
        }

        /// <summary>
        /// Explicit constructor.
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static explicit operator FileSize(long bytes)
        {
            return (new FileSize(bytes));
        }

        /// <summary>
        /// Explicit constructor.
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static explicit operator FileSize(ulong bytes)
        {
            return (new FileSize(bytes));
        }
        #endregion // Constructor(s)

        #region Object-Overridden Methods
        /// <summary>
        /// Return human readable string.
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return (this.ToString("H", null));
        }

        /// <summary>
        /// Return formatted string.
        /// </summary>
        /// <param name="format"></param>
        /// <returns></returns>
        public String ToString(String format)
        {
            return (this.ToString(format, null));
        }
        #endregion // Object-Overridden Methods

        #region IFormattable Interface Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="format"></param>
        /// <param name="provider"></param>
        /// <returns></returns>
        public String ToString(String format, IFormatProvider provider)
        {
            // Handle null or empty arguments.
            if (String.IsNullOrEmpty(format))
                format = "H";
            // Remove any whitespace and convert to uppercase.
            format = format.Trim().ToUpperInvariant();

            if (null == provider)
                provider = NumberFormatInfo.CurrentInfo;

            int precision = 0;
            if (!int.TryParse(format, out precision))
            {
                precision = 2;
            }
            String formatStr = String.Format("N{0}", precision);
            switch (format)
            {
                case "TB":
                    return (this.TeraBytes.ToString(formatStr, provider) + " TB");
                case "GB":
                    return (this.GigaBytes.ToString(formatStr, provider) + " GB");
                case "MB":
                    return (this.MegaBytes.ToString(formatStr, provider) + " MB");
                case "KB":
                    return (this.KiloBytes.ToString(formatStr, provider) + " kB");
                case "B":
                    return (this.Bytes.ToString(formatStr, provider) + " B");
                case "G": // "General" - required by IFormattable interface specification.
                case "H":
                default:
                    return (this.GetHumanReadableString(precision));
            }
        }
        #endregion // IFormattable Interface Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="precision"></param>
        /// <returns></returns>
        private String GetHumanReadableString(int precision)
        {
            Decimal size;
            String suffix;
            if (this.Bytes >= OneTeraByte)
            {
                size = this.Bytes / OneTeraByte;
                suffix = " TB";
            }
            else if (this.Bytes >= OneGigaByte)
            {
                size = this.Bytes / OneGigaByte;
                suffix = " GB";
            }
            else if (this.Bytes >= OneMegaByte)
            {
                size = this.Bytes / OneMegaByte;
                suffix = " MB";
            }
            else if (this.Bytes >= OneKiloByte)
            {
                size = this.Bytes / OneKiloByte;
                suffix = " kB";
            }
            else
            {
                size = this.Bytes;
                suffix = " B";
                precision = 0;
            }

            return (String.Format("{0:N" + precision + "}{1}", size, suffix));
        }
        #endregion // Private Methods
    }

} // RSG.Base namespace
