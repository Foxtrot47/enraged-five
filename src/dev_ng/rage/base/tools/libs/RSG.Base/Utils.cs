//
// File: Utils.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of Utils class
//

using System;
using System.Collections.Generic;
using System.Text;

namespace RSG.Base
{

    /// <summary>
    /// 
    /// </summary>
    public static class Utils
    {

        /// <summary>
        /// Return index of maximum value in arbitrary Array of values.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="args"></param>
        /// <returns></returns>
        public static int MaximumIndex<T>(params T[] args) where T : struct, IComparable, IEquatable<T>
        {
            T maximum = args[0];
            int maxIndex = 0;
            int index = 0;
            foreach (T a in args)
            {
                if (a.CompareTo(maximum) > 0)
                {
                    maximum = a;
                    maxIndex = index;
                }
                ++index;
            }
            return (maxIndex);
        }
    }

} // RSG.Base namespace

// Utils.cs
