﻿using RSG.Base.Math;
using System.Diagnostics;
using System;
using System.Xml;
using System.Xml.XPath;
using System.Runtime.Serialization;


namespace RSG.Base.Math
{

    /// <summary>
    /// Colour3f Specialisation (float) 
    /// </summary>
    [DataContract]
    public class Colour3f : Vector3f
    {
        #region XPath Compiled Expressions
        private static readonly XPathExpression sC_s_xpath_XPathAttrs =
            XPathExpression.Compile("@*");
        #endregion // XPath Compiled Expressions

        #region Member data
        /// <summary>
        /// red-component of colour
        /// </summary>
        [DataMember]
        public float R
        {
            get { return X; }
            set { X = value; }
        }
        /// <summary>
        /// green-component of colour
        /// </summary>
        [DataMember]
        public float G
        {
            get { return Y; }
            set { Y = value; }
        }
        /// <summary>
        /// blue-component of colour
        /// </summary>
        [DataMember]
        public float B
        {
            get { return Z; }
            set { Z = value; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public Colour3f()
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public Colour3f(float x, float y, float z)
            : base(x, y, z)
        {
        }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="v"></param>
        public Colour3f(Colour3f v)
            : base(v.X, v.Y, v.Z)
        {
        }

        /// <summary>
        /// Constructor, from a XmlNode.
        /// </summary>
        /// <param name="node"></param>
        public Colour3f(XmlNode node)
        {
            Debug.Assert(3 == node.Attributes.Count, "Invalid number of attributes for Colour3f.");
            foreach (XmlAttribute attr in node.Attributes)
            {
                if ((sC_s_s_AttrX == attr.Name) ||
                    (sC_s_s_AttrR == attr.Name))
                    this.X = float.Parse(attr.Value);
                else if ((sC_s_s_AttrY == attr.Name) ||
                    (sC_s_s_AttrG == attr.Name))
                    this.Y = float.Parse(attr.Value);
                else if ((sC_s_s_AttrZ == attr.Name) ||
                    (sC_s_s_AttrB == attr.Name))
                    this.Z = float.Parse(attr.Value);
            }
        }
   
        /// <summary>
        /// Constructor, from a XPathNavigator.
        /// </summary>
        /// <param name="navigator"></param>
        public Colour3f(XPathNavigator navigator)
        {
            XPathNodeIterator it = navigator.Select(sC_s_xpath_XPathAttrs);
            while (it.MoveNext())
            {
                if ((sC_s_s_AttrX == it.Current.Name) ||
                    (sC_s_s_AttrR == it.Current.Name))
                    this.X = float.Parse(it.Current.Value);
                else if ((sC_s_s_AttrY == it.Current.Name) ||
                    (sC_s_s_AttrG == it.Current.Name))
                    this.Y = float.Parse(it.Current.Value);
                else if ((sC_s_s_AttrZ == it.Current.Name) ||
                    (sC_s_s_AttrB == it.Current.Name))
                    this.Z = float.Parse(it.Current.Value);
            }
        }
        #endregion // Constructors
    }
} // End of RSG.Base.Math namespace
