//
// File: Quaternion.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of Quaternion class
//

using System;
using System.Diagnostics;
using SMath = System.Math;
using System.Xml;
using System.Xml.XPath;
using System.Runtime.Serialization;

namespace RSG.Base.Math
{

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class Quaternionf
    {
        #region Constants
        #region XPath Compiled Expressions
        private static readonly XPathExpression sC_s_xpath_XPathAttrs =
            XPathExpression.Compile("@*");
        #endregion // XPath Compiled Expressions        
        protected static readonly String sC_s_s_AttrX = "x";
        protected static readonly String sC_s_s_AttrY = "y";
        protected static readonly String sC_s_s_AttrZ = "z";
        protected static readonly String sC_s_s_AttrW = "w";
        #endregion // Constants

        #region Properties and Associated Member Data
        [DataMember]
        public float X;
        [DataMember]
        public float Y;
        [DataMember]
        public float Z;
        [DataMember]
        public float W;
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor, setting to identity quaternion.
        /// </summary>
        public Quaternionf()
        {
            this.X = 0.0f;
            this.Y = 0.0f;
            this.Z = 0.0f;
            this.W = 1.0f;
            Debug.Assert(this.IsIdentity(), "Not correctly constructed identity quaternion.");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <param name="w"></param>
        public Quaternionf(float x, float y, float z, float w)
        {
            this.X = x;
            this.Y = y;
            this.Z = z;
            this.W = w;
            Debug.Assert(this.W >= -1.0f - Constants.M_EPSILON, "Quaternion W out of range.");
            Debug.Assert(this.W <= 1.0f + Constants.M_EPSILON, "Quaternion W out of range.");
        }

        /// <summary>
        /// Quaternion constructor, from axis and angle (in radians).
        /// </summary>
        /// <param name="axis"></param>
        /// <param name="angle"></param>
        public Quaternionf(Vector3f axis, float angle)
        {
            float local_sin = (float)SMath.Sin(0.5 * angle);
            float local_cos = (float)SMath.Cos(0.5 * angle);
            this.X = axis.X * local_sin;
            this.Y = axis.Y * local_sin;
            this.Z = axis.Z * local_sin;
            this.W = local_cos;
            Debug.Assert(this.W >= -1.0f - Constants.M_EPSILON, "Quaternion W out of range.");
            Debug.Assert(this.W <= 1.0f + Constants.M_EPSILON, "Quaternion W out of range.");
        }

        /// <summary>
        /// Quaternion constructor, copy constructor.
        /// </summary>
        /// <param name="q"></param>
        public Quaternionf(Quaternionf q)
        {
            this.X = q.X;
            this.Y = q.Y;
            this.Z = q.Z;
            this.W = q.W;
            Debug.Assert(this.W >= -1.0f - Constants.M_EPSILON, "Quaternion W out of range.");
            Debug.Assert(this.W <= 1.0f + Constants.M_EPSILON, "Quaternion W out of range.");
        }

        /// <summary>
        /// Quaternion constructor, from Matrix34f.
        /// </summary>
        /// <param name="m"></param>
        public Quaternionf(Matrix34f m)
        {
            float temp = (m.A.X + m.B.Y + m.C.Z);
            float x = 0.0f, y = 0.0f, z = 0.0f, w = 0.0f;
            int largest = RSG.Base.Utils.MaximumIndex(temp, m.A.X, m.B.Y, m.C.Z);

            switch (largest)
            {
                case 0:
                    w = (float)(0.5f * SMath.Sqrt(temp + 1.0f));
                    temp = (0.25f / w);
                    x = (m.B.Z - m.C.Y) * temp;
                    y = (m.C.X - m.A.Z) * temp;
                    z = (m.A.Y - m.B.X) * temp;
                    break;
                case 1:
				    x = (float)(0.5 * SMath.Sqrt(2.0 * m.A.X - temp + 1.0f));
                    temp = (0.25f / x);
				    w = (m.B.Z - m.C.Y) * temp;
				    y = (m.A.Y + m.B.X) * temp;
				    z = (m.C.X + m.A.Z) * temp;
                    break;
                case 2:
                    y = (float)(0.5f * SMath.Sqrt(2.0 * m.B.Y - temp + 1.0f));
				    temp = (0.25f / y);
				    w = (m.C.X - m.A.Z) * temp;
				    x = (m.A.Y + m.B.X) * temp;
				    z = (m.B.Z + m.C.Y) * temp;
                    break;
                case 3:
                    z = (float)(0.5f * SMath.Sqrt(2.0 * m.C.Z - temp + 1.0f));
				    temp = (0.25f / z);
				    w = (m.A.Y - m.B.X) * temp;
				    x = (m.C.X + m.A.Z) * temp;
				    y = (m.B.Z + m.C.Y) * temp;	
                    break;
                default:
                    Debug.Assert(false, "Invalid maximum index.");
                    throw new ArgumentOutOfRangeException("Invalid maximum index.");
            }
            Debug.Assert(!float.IsNaN(x), "Quaternion X is NaN!");
            Debug.Assert(!float.IsNaN(y), "Quaternion Y is NaN!");
            Debug.Assert(!float.IsNaN(z), "Quaternion Z is NaN!");
            Debug.Assert(!float.IsInfinity(x), "Quaternion X is infinity!");
            Debug.Assert(!float.IsInfinity(y), "Quaternion Y is infinity!");
            Debug.Assert(!float.IsInfinity(z), "Quaternion Z is infinity!");
            Debug.Assert(w >= -1.0f - Constants.M_EPSILON, "Quaternion W out of range.");
            Debug.Assert(w <= 1.0f + Constants.M_EPSILON, "Quaternion W out of range.");

            // Initialise
            this.X = x;
            this.Y = y;
            this.Z = z;
            this.W = w;
        }
   
        /// <summary>
        /// Constructor, from a XPathNavigator.
        /// </summary>
        /// <param name="navigator"></param>
        public Quaternionf(XPathNavigator navigator)
        {
            XPathNodeIterator it = navigator.Select(sC_s_xpath_XPathAttrs);
            Debug.Assert(4 == it.Count, "Likely invalid Quaternion XML node.");
            while (it.MoveNext())
            {
                if (sC_s_s_AttrX == it.Current.Name)
                    this.X = float.Parse(it.Current.Value);
                else if (sC_s_s_AttrY == it.Current.Name)
                    this.Y = float.Parse(it.Current.Value);
                else if (sC_s_s_AttrZ == it.Current.Name)
                    this.Z = float.Parse(it.Current.Value);
                else if (sC_s_s_AttrW == it.Current.Name) 
                    this.W = float.Parse(it.Current.Value);
            }
        }
        #endregion // Constructor(s)

        #region Object Overrides
        /// <summary>
        /// Convert to simple String representation.
        /// </summary>
        public override string ToString()
        {
            return (String.Format("[ {0}, {1}, {2}, {3} ]", X, Y, Z, W));
        }
        #endregion // Object Overrides

        #region Controller Methods
        /// <summary>
        /// Return whether this quaternion is the identify quaternion.
        /// </summary>
        /// <returns></returns>
        public bool IsIdentity()
        {
            return ((this.X >= -Constants.M_EPSILON) && (this.X <= Constants.M_EPSILON) &&
                (this.Y >= -Constants.M_EPSILON) && (this.Y <= Constants.M_EPSILON) &&
                (this.Z >= -Constants.M_EPSILON) && (this.Z <= Constants.M_EPSILON) &&
                (this.W >= (0.999f * 0.999f)) && (this.X <= (1.001f * 1.001f)));
        }

        /// <summary>
        /// Return whether this quaternion is normalised.
        /// </summary>
        /// <returns></returns>
        public bool IsNormalised()
        {
            float mag2 = this.Magnitude2();
            return ((mag2 >= (0.999f * 0.999f)) && (mag2 <= (1.001f * 1.001f)));
        }

        /// <summary>
        /// Scale this quaternion.
        /// </summary>
        /// <param name="f"></param>
        public void Scale(float f)
        {
            this.X *= f;
            this.Y *= f;
            this.Z *= f;
            this.W *= f;
        }

        /// <summary>
        /// Return magnitude of this quaternion.
        /// </summary>
        /// <returns></returns>
        public float Magnitude()
        {
            return ((float)SMath.Sqrt(Magnitude2()));
        }

        /// <summary>
        /// Return squared-magnitude of this quaternion.
        /// </summary>
        /// <returns></returns>
        public float Magnitude2()
        {
            return ((this.X * this.X) + (this.Y * this.Y) + (this.Z * this.Z) +
                (this.W * this.W));
        }

        /// <summary>
        /// Return the inverse of the magnitude of this quaternion.
        /// </summary>
        /// <returns></returns>
        public float InvMagnitude()
        {
            return ((float)(1.0 / Magnitude()));
        }

        /// <summary>
        /// Normalise this quaternion.
        /// </summary>
        public void Normalise()
        {
            Scale(InvMagnitude());
        }
        
        /// <summary>
        /// Invert this quaternion (axis inverted, angle remains the same).
        /// </summary>
        public void Invert()
        {
            this.X = -this.X;
            this.Y = -this.Y;
            this.Z = -this.Z;
        }

        /// <summary>
        /// Negate this quaternion.
        /// </summary>
        /// Negating a quaternion means that the rotation happens in the opposite 
        /// direction on the opposite axis -- in other words, no net change in the rotation.
        public void Negate()
        {
            this.X = -this.X;
            this.Y = -this.Y;
            this.Z = -this.Z;
            this.W = -this.W;
        }

        /// <summary>
        /// Dot product between two quaternions
        /// </summary>
        /// <param name="q"></param>
        public float Dot(Quaternionf q)
        {
	        return this.X*q.X + this.Y*q.Y + this.Z*q.Z + this.W*q.W;
        }

        /// <summary>
        /// PURPOSE: Calculate the relative angle between two quaternions.
        /// PARAMS
        ///   q - The other quaternion.
        /// RETURN: the angle between this quaternion and the given quaternion
        /// </summary>
        public float RelAngle(Quaternionf q)
        {
	        float c = Dot(q);
	        if (c <= -1.0f || c >= 1.0f)
	        {
		        return 0.0f;
	        }
	        else
	        {
		        return (2.0f * (float) System.Math.Acos(System.Math.Abs(c)));
	        }
        }

        /// <summary>
        /// PURPOSE: Prepare two quaternions for a slerp between them by making sure the angle between them is between -PI and PI.
        /// NOTES
        ///	This method makes sure that the interpolation takes the shortest route. If the angle between the two quaternions is not between
        ///	-PI and PI, then this quaternion is negated to make the angle between -PI and PI. The negated quaternion represents a rotation
        ///	in the opposite direction about an opposite unit vector, so it is equivalent to the non-negated quaternion.
        /// </summary>
        public void PrepareSlerp(Quaternionf a)
        {
            // makes Slerp go the short way
            if (Dot(a) < 0) Negate();
        }

        /// <summary>
        /// PURPOSE: Perform spherical linear interpolation between this quaternion and another.
        /// PARAMS
        ///   q - The other quaternion, representing a t of 1
        /// NOTES:
        ///	To make sure the interpolation takes the shorter of two possible paths around a spherical surface,
        ///	call PrepareSlerp(q) first, or use SlerpNear.
        /// SEE ALSO: PrepareSlerp, SlerpNear
        /// </summary>
        /// <param name="p"></param>
        /// <param name="q1"></param>
        /// <param name="q2"></param>
        public void Slerp(float p, Quaternionf q1, Quaternionf q2)
        {
            float t = 0.5f * q1.RelAngle(q2);
            float st = (float)System.Math.Sin(t);

            if (st != 0.0f)
            {
                st = 1.0f / st;
                float pt = p * t;
                float k1 = (float)System.Math.Sin(t - pt) * st;
                float k2 = (float)System.Math.Sin(pt) * st;

                this.X = k1 * q1.X + k2 * q2.X;
                this.Y = k1 * q1.Y + k2 * q2.Y;
                this.Z = k1 * q1.Z + k2 * q2.Z;
                this.W = k1 * q1.W + k2 * q2.W;

                Normalise();
            }
            else
            {
                this.X = q1.X;
                this.Y = q1.Y;
                this.Z = q1.Z;
                this.W = q1.W;
            }
        }
        #endregion // Controller Methods

        #region Static Methods
        /// <summary>
        /// Returns the largest index.  
        /// Taken from RageCore.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <param name="d"></param>
        /// <returns></returns>
        public static int MaximumIndex(float a, float b, float c, float d)
        {
            return (a >= b) ? ((a >= c) ? (((a >= d) ? 0 : 3)) : (((c >= d) ? 2 : 3))) : ((b >= c) ? ((b >= d) ? 1 : 3) : ((c >= d) ? 2 : 3));
        }

        /// <summary>
        /// Taken from RageCore to convert a Matrix34 into a Quaternion.
        /// </summary>
        /// <param name="matrix"></param>
        /// <returns></returns>
        public static Quaternionf FromMatrix34(Matrix34f matrix)
        {
            Quaternionf quaternion = new Quaternionf();
            float temp = matrix.A.X + matrix.B.Y + matrix.C.Z;
            int largest = MaximumIndex(temp, matrix.A.X, matrix.B.Y, matrix.C.Z);
            switch (largest)
            {
                case 0:
                    quaternion.W = 0.5f * (float) System.Math.Sqrt(temp + 1.0f);
                    temp = 0.25f / quaternion.W;
                    quaternion.X = (matrix.B.Z - matrix.C.Y) * temp;
                    quaternion.Y = (matrix.C.X - matrix.A.Z) * temp;
                    quaternion.Z = (matrix.A.Y - matrix.B.X) * temp;
                    break;
                case 1:
                    quaternion.X = 0.5f * (float) System.Math.Sqrt(2.0f * matrix.A.X - temp + 1.0f);
                    temp = 0.25f / quaternion.X;
                    quaternion.W = (matrix.B.Z - matrix.C.Y) * temp;
                    quaternion.Y = (matrix.A.Y + matrix.B.Z) * temp;
                    quaternion.Z = (matrix.C.X + matrix.A.Z) * temp;
                    break;
                case 2:
                    quaternion.Y = 0.5f * (float) System.Math.Sqrt(2.0f * matrix.B.Y - temp + 1.0f);
                    temp = 0.25f / quaternion.Y;
                    quaternion.W = (matrix.C.X - matrix.A.Z) * temp;
                    quaternion.X = (matrix.A.Y + matrix.B.X) * temp;
                    quaternion.Z = (matrix.B.Z + matrix.C.Y) * temp;
                    break;
                case 3:
                    quaternion.Z = 0.5f * (float) System.Math.Sqrt(2.0f * matrix.C.Z - temp + 1.0f);
                    temp = 0.25f / quaternion.Z;
                    quaternion.W = (matrix.A.Y - matrix.B.X) * temp;
                    quaternion.X = (matrix.C.X + matrix.A.Z) * temp;
                    quaternion.Y = (matrix.B.Z + matrix.C.Y) * temp;
                    break;
                default:
                    break;
            }

            return quaternion;
        }
        #endregion Static Methods
    }

} // RSG.Base.Math namespace

// Quaternion.cs
