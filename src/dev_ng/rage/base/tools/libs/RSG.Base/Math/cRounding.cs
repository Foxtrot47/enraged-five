//
// File: cRounding.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of cRounding.cs class
//

using System;
using System.Collections.Generic;
using System.Text;

namespace RSG.Base.Math
{

    /// <summary>
    /// Useful rounding utilities
    /// </summary>
    public class cRounding
    {
        /// <summary>
        /// Round to nearest value function
        /// </summary>
        /// <param name="val"></param>
        /// <param name="nearest"></param>
        /// <returns></returns>
        public static float RoundToNearest(float val, float nearest)
        {
            float rem = (float)System.Math.IEEERemainder(val, nearest);
            if (rem < (nearest / 2.0f))
                return (val - rem);
            else
                return (val + rem);
        }
    }

} // End of RSG.Base.Math namespace

// End of file
