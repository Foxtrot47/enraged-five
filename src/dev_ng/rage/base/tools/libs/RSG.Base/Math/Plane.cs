﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Math
{
    public class Plane : Vector4f
    {
        /// <summary>
        /// Basic Constructor.
        /// </summary>
        public Plane() { }

        /// <summary>
        /// Constructor for a plane and a point.
        /// </summary>
        /// <param name="normal"></param>
        /// <param name="point"></param>
        public Plane(Vector3f normal, Vector3f point)
        {
            X = normal.X;
            Y = normal.Y;
            Z = normal.Z;
            W = X * point.X - Y * point.Y - Z * point.Z;
        }

        /// <summary>
        /// Constructor for a plane with three points.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        public Plane(Vector3f a, Vector3f b, Vector3f c) { ComputePlane(a, b, c); }

        /// <summary>
        /// Computes the plane coefficients based on three points.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        private void ComputePlane(Vector3f a, Vector3f b, Vector3f c)
        {
            // Find the unit normal vector, perpendicular to the plane containing the three points.
            Vector3f v1, v2, n;
            v1 = a - b;
            v2 = c - b;
            n = Vector3f.Cross(v1, v2);
            n.Normalise();
            X = n.X;
            Y = n.Y;
            Z = n.Z;

            // Find the distance from the plane to the origin.
            W = -n.X * b.X - n.Y * b.Y - n.Z * b.Z;
        }

        /// <summary>
        /// Returns the normal of the plane.
        /// </summary>
        /// <returns></returns>
        public Vector3f GetNormal()
        {
            return new Vector3f(X, Y, Z);
        }

        /// <summary>
        /// Returns the Z coordinate on the plane (assuming infinite extents).
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public float GetZCoordinate(float x, float y)
        {
            //Equation of a plane is: Ax + By + Cz + D = 0
            //Solve for z: 
            // -(Ax + By + D / C) = z
            return -(((X * x) + (Y * y) + W) / Z);
        }

    }
}
