//
// File: cVector2.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of cVector2.cs class
//

using System;
using System.Diagnostics;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace RSG.Base.Math
{
    #region Vector2 Template Base Class
    /// <summary>
    /// 2D Vector Class
    /// </summary>
    /// Currently common operations etc. are not implemented.
    [Serializable]
    [DataContract]
    public abstract class Vector2<T> :
        IComparable,
        IComparable<Vector2<T>>
        where T : IComparable
    {
        #region Constants
        protected static readonly String sC_s_s_AttrX = "x";
        protected static readonly String sC_s_s_AttrY = "y";
        #endregion // Constants

        #region Properties and Associated Member Data
        /// <summary>
        /// Vector Element Type
        /// </summary>
        public Type ElementType
        {
            get { return (typeof(T)); }
        }

        /// <summary>
        /// X-Component of vector
        /// </summary>
        [XmlAttribute("x")]
        [DataMember]
        public T X
        {
            get { return m_X; }
            set { m_X = value; }
        }
        private T m_X;

        /// <summary>
        /// Y-Component of vector
        /// </summary>
        [XmlAttribute("y")]
        [DataMember]
        public T Y
        {
            get { return m_Y; }
            set { m_Y = value; }
        }
        private T m_Y;
        #endregion // Properties and Associated Member Data

        #region Constructor / Destructor
        /// <summary>
        /// Default constructor
        /// </summary>
        protected Vector2()
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        protected Vector2(T x, T y)
        {
            X = x;
            Y = y;
        }

        /// <summary>
        /// Dispose
        /// </summary>
        protected void Dispose()
        {
            X = default(T);
            Y = default(T);
        }
        #endregion // Constructor / Destructor

        #region Abstract Methods
        /// <summary>
        /// Calculate and return magnitude of 3D vector
        /// </summary>
        /// <returns>Magnitude (length) of 3D vector</returns>
        public abstract double Magnitude();

        /// <summary>
        /// Equality test.
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public abstract bool Equals(Vector2<T> v);

        #endregion // Abstract Methods

        #region IComparable Interface Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int CompareTo(Object obj)
        {
            if (obj is Vector2<T>)
                return (CompareTo(obj as Vector3<T>));
            throw new ArgumentException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public int CompareTo(Vector2<T> v)
        {
            int x = this.X.CompareTo(v.X);
            int y = this.Y.CompareTo(v.Y);
            return (x + y);
        }
        #endregion // IComparable Interface Methods

        #region Object Overridden Methods
        /// <summary>
        /// Convert vector to String
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return (String.Format("( {0}, {1} )", this.X, this.Y));
        }

        /// <summary>
        /// Equality test.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
                return false;

            // If parameter cannot be cast to Vector3 return false.
            Vector2<T> v = obj as Vector2<T>;
            if (v == null)
                return false;

            return this.Equals(v);
        }

        /// <summary>
        /// GetHashCode override.
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        #endregion // Methods
    }
    #endregion // Vector2 Template Base Class

    #region Vector2f
    /// <summary>
    /// Vector2f
    /// </summary>
    [DataContract]
    [Serializable]
    public class Vector2f : Vector2<float>
    {
        #region Constants
        #region XPath Compiled Expressions
        private static readonly XPathExpression sC_s_xpath_XPathAttrs =
            XPathExpression.Compile("@*");
        #endregion // XPath Compiled Expressions
        #endregion // Constants

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public Vector2f()
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public Vector2f(float x, float y)
        {
            this.X = x;
            this.Y = y;
        }
        
        /// <summary>
        /// Initialises a new instance of the <see cref="Vector2f"/> class using the
        /// provided array.
        /// </summary>
        /// <param name="data"></param>
        public Vector2f(float[] data)
            : this(data[0], data[1])
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="b"></param>
        public Vector2f(Vector2f b)
            : this(b.X, b.Y)
        {
        }

        /// <summary>
        /// Constructor, from a XmlNode.
        /// </summary>
        /// <param name="node"></param>
        public Vector2f(XmlNode node)
        {
            Debug.Assert(2 == node.Attributes.Count, 
                "Invalid number of attributes for Vector2f.");
            foreach (XmlAttribute attr in node.Attributes)
            {
                if (sC_s_s_AttrX == attr.Name)
                    this.X = float.Parse(attr.Value);
                else if (sC_s_s_AttrY == attr.Name)
                    this.Y = float.Parse(attr.Value);
            }
        }
   
        /// <summary>
        /// Constructor, from a XPathNavigator.
        /// </summary>
        /// <param name="navigator"></param>
        public Vector2f(XPathNavigator navigator)
        {
            XPathNodeIterator it = navigator.Select(sC_s_xpath_XPathAttrs);
            while (it.MoveNext())
            {
                if (sC_s_s_AttrX == it.Current.Name)
                    this.X = float.Parse(it.Current.Value);
                else if (sC_s_s_AttrY == it.Current.Name)
                    this.Y = float.Parse(it.Current.Value);
            }
        }
        #endregion // Constructors

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// <param name="s"></param>
        /// <returns></returns>
        public override bool Equals(Vector2<float> v)
        {
            // If parameter is null return false:
            if (v == null)
                return false;

            return (this.X == v.X && this.Y == v.Y);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override double Magnitude()
        {
            return (System.Math.Sqrt((double)((this.X * this.X) + (this.Y * this.Y))));
        }

        /// <summary>
        /// 
        /// </summary>
        public void Normalise()
        {
            double mag = this.Magnitude();
            // Avoid divide by 0 returning NaN
            if (mag == 0.0)
            {
                return;
            }
            this.X /= (float)mag;
            this.Y /= (float)mag;
        }

        /// <summary>
        /// 
        /// </summary>
        public void Abs()
        {
            this.X = System.Math.Abs(this.X);
            this.Y = System.Math.Abs(this.Y);
        }
        #endregion // Public Methods

        #region Static Methods
        /// <summary>
        /// Parse a string representation of a vector (FormatException might be raised)
        /// </summary>
        /// <param name="sText"></param>
        /// <returns></returns>
        public static Vector2f Parse(String sText)
        {
            String[] parts = sText.Split(new char[] { '(', '[', ',', ']', ')' },
                                        StringSplitOptions.RemoveEmptyEntries);
            Debug.Assert(2 == parts.Length);
            if (2 != parts.Length)
                throw new FormatException("Higher dimension vector?");

            return (new Vector2f((float.Parse(parts[0])), (float.Parse(parts[1]))));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static float Dot(Vector2f a, Vector2f b)
        {
            return a.X * b.X + a.Y * b.Y;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ax"></param>
        /// <param name="ay"></param>
        /// <param name="bx"></param>
        /// <param name="by"></param>
        /// <returns></returns>
        public static float Dot(float ax, float ay, float bx, float by)
        {
            return ax * bx + ay * by;
        }
        #endregion // Static Methods

        #region Static Operators
        /// <summary>
        /// Equality operator
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool operator ==(Vector2f v1, Vector2f v2)
        {
            // If both are null, or both are same instance, return true.
            if (Object.ReferenceEquals(v1, v2))
                return true;

            // If one is null, but not both, return false.
            if (((Object)v1 == null) || ((Object)v2 == null))
                return false;

            // Return true if the fields match:
            return v1.X == v2.X && v1.Y == v2.Y;
        }

        /// <summary>
        /// Inequality operator
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool operator !=(Vector2f v1, Vector2f v2)
        {
            return !(v1 == v2);
        }

        /// <summary>
        /// Addition operator
        /// </summary>
        public static Vector2f operator +(Vector2f v1, Vector2f v2)
        {
            return (new Vector2f(v1.X + v2.X, v1.Y + v2.Y));
        }
        
        /// <summary>
        /// Scalar Addition operator
        /// </summary>
        public static Vector2f operator +(Vector2f v1, float f)
        {
            return (new Vector2f(v1.X + f, v1.Y + f));
        }

        /// <summary>
        /// Scalar Subtraction operator
        /// </summary>
        public static Vector2f operator -(Vector2f v1, float f)
        {
            return (new Vector2f(v1.X - f, v1.Y - f));
        }

        /// <summary>
        /// Subtraction Operator
        /// </summary>
        public static Vector2f operator -(Vector2f v1, Vector2f v2)
        {
            return (new Vector2f(v1.X - v2.X, v1.Y - v2.Y));
        }

        /// <summary>
        /// Scalar Multiplication Operator
        /// </summary>
        public static Vector2f operator *(Vector2f v, float f)
        {
            return (new Vector2f(v.X * f, v.Y * f));
        }

        /// <summary>
        /// Scalar Division Operator
        /// </summary>
        /// <param name="v"></param>
        /// <param name="f"></param>
        /// <returns></returns>
        public static Vector2f operator /(Vector2f v, float f)
        {
            return (new Vector2f(v.X / f, v.Y / f));
        }

        /// <summary>
        /// Implicit type conversion Vector2f -> Vector2i
        /// </summary>
        /// <param name="a">Vector2f</param>
        /// <returns>Vector2i</returns>
        public static explicit operator Vector2i(Vector2f a)
        {
            Vector2i v = new Vector2i((int)a.X, (int)a.Y);
            return (v);
        }

        /// <summary>
        /// Explicit conversion to a float array.
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static explicit operator float[](Vector2f v)
        {
            return new float[] { v.X, v.Y };
        }

        /// <summary>
        /// Distance between vector points.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public double Dist(Vector2f other)
        {
            return (System.Math.Sqrt((double)(((this.X - other.X) * (this.X - other.X)) + ((this.Y - other.Y) * (this.Y - other.Y))) ));
        }

        /// <summary>
        /// Distance-squared between vector points.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public double Dist2(Vector2f other)
        {
            return (double)(((this.X - other.X) * (this.X - other.X)) + ((this.Y - other.Y) * (this.Y - other.Y)));
        }
        #endregion // Static Operators

        #region Object Overridden Methods
        /// <summary>
        /// Convert vector to String
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return (String.Format("( {0}, {1} )", this.X, this.Y));
        }

        /// <summary>
        /// Equality operator
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(Object obj)
        {
            Vector2f v = (obj as Vector2f);

            return (v.X.Equals(this.X) && v.Y.Equals(this.Y));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return (this.X.GetHashCode() ^ this.Y.GetHashCode());
        }
        #endregion // Methods

        #region Static Data
        public static readonly Vector2f Zero = new Vector2f(0.0f, 0.0f);
        #endregion
    };
    #endregion // Vector2f

    #region Vector2i
    /// <summary>
    /// cVector2 Specialisation (int)
    /// </summary>
    public class Vector2i : Vector2<int>
    {
        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public Vector2i()
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public Vector2i(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="b"></param>
        public Vector2i(Vector2i b)
        {
            this.X = b.X;
            this.Y = b.Y;
        }
        #endregion // Constructors

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public override bool Equals(Vector2<int> v)
        {
            // If parameter is null return false:
            if (v == null)
                return false;

            return (this.X == v.X && this.Y == v.Y);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override double Magnitude()
        {
            return (System.Math.Sqrt((double)((this.X * this.X) + (this.Y * this.Y))));
        }
        #endregion // Public Methods

        #region Static Methods
        /// <summary>
        /// Parse a string representation of a vector (FormatException might be raised)
        /// </summary>
        /// <param name="sText"></param>
        /// <returns></returns>
        public static Vector2i Parse(String sText)
        {
            String[] parts = sText.Split(new char[] { '(', '[', ',', ']', ')' },
                                        StringSplitOptions.RemoveEmptyEntries);
            Debug.Assert(2 == parts.Length);
            if (2 != parts.Length)
                throw new FormatException("Higher dimension vector?");

            return (new Vector2i((int.Parse(parts[0])), (int.Parse(parts[1]))));
        }
        #endregion // Static Methods

        #region Static Operators
        /// <summary>
        /// Equality operator
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool operator ==(Vector2i v1, Vector2i v2)
        {
            // If both are null, or both are same instance, return true.
            if (Object.ReferenceEquals(v1, v2))
                return true;

            // If one is null, but not both, return false.
            if (((Object)v1 == null) || ((Object)v2 == null))
                return false;

            // Return true if the fields match:
            return v1.X == v2.X && v1.Y == v2.Y;
        }

        /// <summary>
        /// Inequality operator
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool operator !=(Vector2i v1, Vector2i v2)
        {
            return !(v1 == v2);
        }

        /// <summary>
        /// Addition operator
        /// </summary>
        public static Vector2i operator +(Vector2i v1, Vector2i v2)
        {
            return (new Vector2i(v1.X + v2.X, v1.Y + v2.Y));
        }

        /// <summary>
        /// Subtraction Operator
        /// </summary>
        public static Vector2i operator -(Vector2i v1, Vector2i v2)
        {
            return (new Vector2i(v1.X - v2.X, v1.Y - v2.Y));
        }

        /// <summary>
        /// Scalar Addition operator
        /// </summary>
        public static Vector2i operator +(Vector2i v1, int f)
        {
            return (new Vector2i(v1.X + f, v1.Y + f));
        }

        /// <summary>
        /// Scalar Subtraction operator
        /// </summary>
        public static Vector2i operator -(Vector2i v1, int f)
        {
            return (new Vector2i(v1.X - f, v1.Y - f));
        }

        /// <summary>
        /// Scalar Multiplication Operator
        /// </summary>
        public static Vector2i operator *(Vector2i v, int f)
        {
            return (new Vector2i(v.X * f, v.Y * f));
        }

        /// <summary>
        /// Implicit type conversion Vector2i -> Vector2f
        /// </summary>
        /// <param name="a">Vector2i</param>
        /// <returns>Vector2f</returns>
        public static explicit operator Vector2f(Vector2i a)
        {
            Vector2f v = new Vector2f((float)a.X, (float)a.Y);
            return (v);
        }
        #endregion // Static Operators

        #region Object Overridden Methods
        /// <summary>
        /// Convert vector to String
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return (String.Format("( {0}, {1} )", this.X, this.Y));
        }

        /// <summary>
        /// Equality operator
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(Object obj)
        {
            Vector2i v = (obj as Vector2i);

            return (v.X.Equals(this.X) && v.Y.Equals(this.Y));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return (this.X.GetHashCode() ^ this.Y.GetHashCode());
        }
        #endregion // Methods
    };
    #endregion // Vector2i

} // End of RSG.Base.Math namespace

// End of file
