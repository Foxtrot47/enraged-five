﻿//
// File: BoundingBox2.cs
// Author: Travis Primm <travis.primm@rockstarsandiego.com>
// Description: Implementation of OBoundingBox2 classes
//

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace RSG.Base.Math
{

    /// <summary>
    /// 2D Oriented Bounding Box (floating-point coordinate system).
    /// </summary>
    [DataContract]
    public class OBoundingBox2f
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Center
        /// </summary>
        [XmlElement("center")]
        [DataMember]
        public Vector2f Center
        {
            get { return m_vCenter; }
            set
            {
                if (m_vCenter != value)
                {
                    m_vCenter = value;
                    this.m_bIsEmpty = false;
                }
            }
        }
        private Vector2f m_vCenter;

        /// <summary>
        /// Distance from the center to the X+ edge in a local orientation and the same for Y+
        /// </summary>
        [XmlElement("extents")]
        [DataMember]
        public Vector2f Extents
        {
            get { return m_vExtents; }
            set
            {
                if (m_vExtents != value)
                {
                    m_vExtents = value;
                    this.m_bIsEmpty = false;
                }
            }
        }
        private Vector2f m_vExtents;

        /// <summary>
        /// Rotation about the up axis
        /// </summary>
        [XmlElement("rotation")]
        [DataMember]
        public float Rotation
        {
            get { return m_fRotation; }
            set
            {
                if (m_fRotation != value)
                {
                    m_fRotation = value;
                    this.m_bIsEmpty = false;
                }
            }
        }
        private float m_fRotation;

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public bool IsEmpty
        {
            get { return m_bIsEmpty; }
        }
        private bool m_bIsEmpty;
        #endregion // Properties and Associated Member Data

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public OBoundingBox2f()
        {
            this.Center = Vector2f.Zero;
            this.Rotation = 0.0f;
            this.Extents = new Vector2f(float.MinValue, float.MinValue);
            this.m_bIsEmpty = true;
        }

        /// <summary>
        /// 
        /// </summary>
        public OBoundingBox2f(BoundingBox2f localBounds)
        {
            this.Center = localBounds.Centre();
            this.Rotation = 0.0f;
            this.Extents = localBounds.Max - this.Center;
            this.m_bIsEmpty = false;
        }

        /// <summary>
        /// Takes a local bounding box of the points and the transformation matrix from world space to local space
        /// of the OBoundingBox2f
        /// </summary>
        public OBoundingBox2f(BoundingBox3f box, Matrix34f transform)
        {
            Vector3f boxCenter = box.Centre();
			// This Transpose() is to handle all of the rotations coming in the wrong signed value of what they should be
            transform = transform.Transpose();
            // Rotated to oriented offset. Multiplication adds the D vector to the results
            Vector3f orientedBoxCenter = transform * boxCenter;
            this.Center = new Vector2f(orientedBoxCenter.X, orientedBoxCenter.Y);
            this.Rotation = transform.ToEulers().Z;
            this.Extents = new Vector2f(box.Max.X - boxCenter.X , box.Max.Y - boxCenter.Y);
            this.m_bIsEmpty = false;
        }

        /// <summary>
        /// Returns an counter clockwise ordered points
        /// TOOD: Consider storing these points if this is calculated multiple times.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Vector2f> GetOrientedPoints()
        {
            List<Vector2f> points = new List<Vector2f>();
            Matrix22f rotMatrix = Matrix22f.CreateFromRotation(Rotation);
            Vector2f localPoint = new Vector2f(Extents.X, Extents.Y);
            //x+, y+
            points.Add(Center + rotMatrix * localPoint);
            //x-, y+
            localPoint.X = -Extents.X;
            points.Add(Center + rotMatrix * localPoint);
            //x-, y-
            localPoint.Y = -Extents.Y;
            points.Add(Center + rotMatrix * localPoint);
            //x+, y-
            localPoint.X = Extents.X;
            points.Add(Center + rotMatrix * localPoint);

            return points;
        }

        /// <summary>
        /// Get min coordinate for the AABB around oriented points on a world grid
        /// </summary>
        /// <returns></returns>
        public Vector2f GetMin()
        {
            Vector2f minCoordinate = new Vector2f(float.MaxValue, float.MaxValue);
            foreach (Vector2f point in GetOrientedPoints())
            {
                if (point.X < minCoordinate.X)
                {
                    minCoordinate.X = point.X;
                }
                if (point.Y < minCoordinate.Y)
                {
                    minCoordinate.Y = point.Y;
                }
            }
            return minCoordinate;
        }

        /// <summary>
        /// Get max coordinate for the AABB around oriented points on a world grid
        /// </summary>
        /// <returns></returns>
        public Vector2f GetMax()
        {
            Vector2f maxCoordinate = new Vector2f(float.MinValue, float.MinValue);
            foreach (Vector2f point in GetOrientedPoints())
            {
                if (point.X > maxCoordinate.X)
                {
                    maxCoordinate.X = point.X;
                }
                if (point.Y > maxCoordinate.Y)
                {
                    maxCoordinate.Y = point.Y;
                }
            }
            return maxCoordinate;
        }

        /// <summary>
        /// This is slow but easier to write. Consider using BoundingBox2f.Intersects(Vector2f, Vector2f) directly and storing the Max/Min
        /// </summary>
        /// <param name="box"></param>
        /// <returns></returns>
        public bool Intersects(BoundingBox3f box)
        {
            return Intersects(new BoundingBox2f(box));
        }

        /// <summary>
        /// This is slow but easier to write. Consider using BoundingBox2f.Intersects(Vector2f, Vector2f) directly and storing the Max/Min
        /// TODO: This can be optimized to filter out some axis so we don't have to continue to check against every possible point on every axis
        /// </summary>
        /// <param name="box"></param>
        /// <returns></returns>
        public bool Intersects(BoundingBox2f box)
        {
            //Use Separating Axis Theorem based only on the normal of this box.
            Vector2f[] obbPoints = GetOrientedPoints().ToArray();
            Vector2f[] aabbPoints = { box.Max, new Vector2f(box.Max.X, box.Min.Y), box.Min, new Vector2f(box.Min.X, box.Max.Y) };

            //Iterate over every axis for this box
            for (int index = 0; index < 4; index++)
            {
                Vector2f axis = obbPoints[index] - obbPoints[(index + 1) % 4];
                axis.Normalise();
                float obbProjection;
                float aabbProjection;
                // Find out which box's center is further on this axis
                if (Vector2f.Dot(axis, box.Centre()) > Vector2f.Dot(axis, Center))
                {
                    // the AABB center has a higher scalar projection than this OBB
                    // therefore, if any of AABB projected vertex values are smaller than any projected vertex value
                    // of the OBB, then there is an intersection and we should try the next axis, else we've found a separating axis
                    obbProjection = Utils.MaxProjectedAxisValue(obbPoints, axis);
                    aabbProjection = Utils.MinProjectedAxisValue(aabbPoints, axis);
                    if (obbProjection < aabbProjection)
                    {
                        return false;
                    }
                }
                else
                {
                    aabbProjection = Utils.MaxProjectedAxisValue(aabbPoints, axis);
                    obbProjection = Utils.MinProjectedAxisValue(obbPoints, axis);
                    if (aabbProjection < obbProjection)
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        #endregion // Constructors
    }
}
