﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using SMath = System.Math;

namespace RSG.Base.Math
{

    /// <summary>
    /// 
    /// </summary>
    public class Matrix33f
    {
        #region Constants
        /// <summary>
        /// Identity matrix.
        /// </summary>
        public static readonly Matrix33f Identity = new Matrix33f();
        #endregion // Constants

        #region Properties and Associated Member Data
        public Vector3f A;
        public Vector3f B;
        public Vector3f C;
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor, initialise to identity matrix.
        /// </summary>
        public Matrix33f()
        {
            this.A = new Vector3f(1.0f, 0.0f, 0.0f);
            this.B = new Vector3f(0.0f, 1.0f, 0.0f);
            this.C = new Vector3f(0.0f, 0.0f, 1.0f);
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        public Matrix33f(Vector3f a, Vector3f b, Vector3f c)
        {
            this.A = new Vector3f(a);
            this.B = new Vector3f(b);
            this.C = new Vector3f(c);
        }
        
        /// <summary>
        /// Initialises a new instance of the <see cref="Matrix33"/> class using the
        /// provided array.
        /// </summary>
        /// <param name="data"></param>
        public Matrix33f(float[][] data)
        {
            this.A = new Vector3f(data[0]);
            this.B = new Vector3f(data[1]);
            this.C = new Vector3f(data[2]);
        }

        /// <summary>
        /// Copy constructor.
        /// </summary>
        /// <param name="m"></param>
        public Matrix33f(Matrix33f m)
        {
            this.A = new Vector3f(m.A);
            this.B = new Vector3f(m.B);
            this.C = new Vector3f(m.C);
        }

        /// <summary>
        /// Constructor from Matrix34f.
        /// </summary>
        /// <param name="m"></param>
        public Matrix33f(Matrix34f m)
        {
            this.A = new Vector3f(m.A);
            this.B = new Vector3f(m.B);
            this.C = new Vector3f(m.C);
        }
        #endregion // Constructor(s)

        #region Operators

        /// <summary>
        /// Multiplication operator.
        /// </summary>
        /// <param name="m"></param>
        /// <param name="v"></param>
        /// <returns></returns>
        public static Vector3f operator *(Matrix33f m, Vector3f v)
        {
            float x = v.X * m.A.X + v.Y * m.B.X + v.Z * m.C.X;
            float y = v.X * m.A.Y + v.Y * m.B.Y + v.Z * m.C.Y;
            float z = v.X * m.A.Z + v.Y * m.B.Z + v.Z * m.C.Z;
            return (new Vector3f(x, y, z));
        }

        /// <summary>
        /// Explicit conversion to a 2d float array.
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public static explicit operator float[][](Matrix33f m)
        {
            return new float[][] { (float[])m.A, (float[])m.B, (float[])m.C };
        }

        #endregion // Operators

        #region Public Methods
        /// <summary>
        /// Transform a vector by the current matrix.
        /// </summary>
        /// <param name="v"></param>
        public void Transform(Vector3f v)
        {
            float newX = (v.X * this.A.X) + (v.Y * this.B.X) + (v.Z * this.C.X);
            float newY = (v.X * this.A.Y) + (v.Y * this.B.Y) + (v.Z * this.C.Y);
            v.Z = (v.X * this.A.Z) + (v.Y * this.B.Z) + (v.Z * this.C.Z);
            v.X = newX;
            v.Y = newY;
        }

        /// <summary>
        /// 
        /// </summary>
        public void Abs()
        {
            this.A.Abs();
            this.B.Abs();
            this.C.Abs();
        }

        /// <summary>
        /// Return the inverse of this Matrix33f.
        /// </summary>
        /// <returns></returns>
        public Matrix33f Inverse()
        {
            // Get three of our subdeterminants
            float subDetX = Utils.Det22(B.Y, B.Z, C.Y, C.Z);
            float subDetY = Utils.Det22(B.X, B.Z, C.X, C.Z);
            float subDetZ = Utils.Det22(B.X, B.Y, C.X, C.Y);

            // Find the largest absolute value element.
            float largest = Utils.Maximum(
                SMath.Abs(A.X), SMath.Abs(A.Y), SMath.Abs(A.Z),
                SMath.Abs(B.X), SMath.Abs(B.Y), SMath.Abs(B.Z),
                SMath.Abs(C.X), SMath.Abs(C.Y), SMath.Abs(C.Z));

            // Get the inverse of the determinant.
            float invDet = (A.X * subDetX) - (A.Y * subDetY) + (A.Z * subDetZ);
            if (SMath.Abs(invDet) > (3.6e-7 * largest))
            {
                Matrix33f invm = new Matrix33f();
                invDet = 1.0f / invDet;

                // Start making the inverse matrix.
                invm.A.X = subDetX * invDet;
                invm.B.X = -subDetY * invDet;
                invm.C.X = subDetZ * invDet;

                // Get 3 more subdeterminants
                subDetX = Utils.Det22(A.Y, A.Z, C.Y, C.Z);
                subDetY = Utils.Det22(A.X, A.Z, C.X, C.Z);
                subDetZ = Utils.Det22(A.X, A.Y, C.X, C.Y);

                // Add more terms to the inverse matrix.
                invm.A.Y = -subDetX * invDet;
                invm.B.Y = subDetY * invDet;
                invm.C.Y = -subDetZ * invDet;

                // Get the last three subdeterminants.
                subDetX = Utils.Det22(A.Y, A.Z, B.Y, B.Z);
                subDetY = Utils.Det22(A.X, A.Z, B.X, B.Z);
                subDetZ = Utils.Det22(A.X, A.Y, B.X, B.Y);

                // Finish making the inverse matrix.
                invm.A.Z = subDetX * invDet;
                invm.B.Z = -subDetY * invDet;
                invm.C.Z = subDetZ * invDet;

                // Return inverted matrix (the determinant was not too close to zero).
                return (invm);
            }

            // Return identity matrix (determinant was too close to zero).
            return (new Matrix33f());
        }
        #endregion // Public Methods
    }

} // RSG.Base.Math namespace
