//
// File: Vector3.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: 3D Vector Classes
//

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace RSG.Base.Math
{

    #region Vector3 Template Base Class
    /// <summary>
    /// 3D Vector Class
    /// </summary>
    /// Currently common operations etc. are not implemented.
    [Serializable]
    [DataContract]
    public abstract class Vector3<T> : 
        IComparable,
        IComparable<Vector3<T>>
        where T : IComparable
    {
        #region Constants
        protected static readonly String sC_s_s_AttrX = "x";
        protected static readonly String sC_s_s_AttrY = "y";
        protected static readonly String sC_s_s_AttrZ = "z";
        protected static readonly String sC_s_s_AttrR = "r";
        protected static readonly String sC_s_s_AttrG = "g";
        protected static readonly String sC_s_s_AttrB = "b";
        #endregion // Constants

        #region Properties and Associated Member Data
        /// <summary>
        /// Vector Element Type
        /// </summary>
        public Type ElementType
        {
            get { return (typeof(T)); }
        }

        /// <summary>
        /// X-Component of vector
        /// </summary>
        [XmlAttribute("x")]
        [DataMember]
        public T X
        {
            get { return m_X; }
            set { m_X = value; }
        }
        private T m_X;

        /// <summary>
        /// Y-Component of vector
        /// </summary>
        [XmlAttribute("y")]
        [DataMember]
        public T Y
        {
            get { return m_Y; }
            set { m_Y = value; }
        }
        private T m_Y;

        /// <summary>
        /// Z-Component of vector
        /// </summary>
        [XmlAttribute("z")]
        [DataMember]
        public T Z
        {
            get { return m_Z; }
            set { m_Z = value; }
        }
        private T m_Z;
        #endregion // Properties and Associated Member Data

        #region Constructor / Destructor
        /// <summary>
        /// Default constructor
        /// </summary>
        protected Vector3()
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        protected Vector3(T x, T y, T z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        /// <summary>
        /// Dispose
        /// </summary>
        protected void Dispose()
        {
            X = default(T);
            Y = default(T);
            Z = default(T);
        }
        #endregion // Constructor / Destructor

        #region Abstract Methods
        /// <summary>
        /// Calculate and return magnitude of 3D vector
        /// </summary>
        /// <returns>Magnitude (length) of 3D vector</returns>
        public abstract double Magnitude();

        /// <summary>
        /// Equality test.
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public abstract bool Equals(Vector3<T> v);

        #endregion // Abstract Methods

        #region IComparable Interface Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int CompareTo(Object obj)
        {
            if (obj is Vector3<T>)
                return (CompareTo(obj as Vector3<T>));
            throw new ArgumentException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public int CompareTo(Vector3<T> v)
        {
            int x = this.X.CompareTo(v.X);
            int y = this.Y.CompareTo(v.Y);
            int z = this.Z.CompareTo(v.Z);
            return (x + y + z);
        }
        #endregion // IComparable Interface Methods

        #region Object Overridden Methods
        /// <summary>
        /// Convert vector to String
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return (String.Format("( {0}, {1}, {2} )", this.X, this.Y, this.Z));
        }

        /// <summary>
        /// Equality test.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
                return false;

            // If parameter cannot be cast to Vector3 return false.
            Vector3<T> v = obj as Vector3<T>;
            if (v == null)
                return false;

            return this.Equals(v);
        }

        /// <summary>
        /// GetHashCode override.
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        #endregion // Methods
    }
    #endregion // Vector3 Template Base Class

    #region Vector3<float>
    /// <summary>
    /// Vector3 Specialisation (float)
    /// </summary>
    [Serializable]
    [DataContract]
    public class Vector3f : Vector3<float>
    {   
        #region XPath Compiled Expressions
        private static readonly XPathExpression sC_s_xpath_XPathAttrs =
            XPathExpression.Compile("@*");
        #endregion // XPath Compiled Expressions

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public Vector3f()
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public Vector3f(float x, float y, float z)
            : base(x, y, z)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Vector3f"/> class using the
        /// provided array.
        /// </summary>
        /// <param name="data"></param>
        public Vector3f(float[] data)
            : base(data[0], data[1], data[2])
        {
        }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="v"></param>
        public Vector3f(Vector3f v)
            : base(v.X, v.Y, v.Z)
        {
        }

        /// <summary>
        /// Constructor, from a XmlNode.
        /// </summary>
        /// <param name="node"></param>
        public Vector3f(XmlNode node)
        {
            Debug.Assert(3 == node.Attributes.Count, "Invalid number of attributes for Vector3f.");
            foreach (XmlAttribute attr in node.Attributes)
            {
                if ((sC_s_s_AttrX == attr.Name) ||
                    (sC_s_s_AttrR == attr.Name))
                    this.X = float.Parse(attr.Value);
                else if ((sC_s_s_AttrY == attr.Name) ||
                    (sC_s_s_AttrG == attr.Name))
                    this.Y = float.Parse(attr.Value);
                else if ((sC_s_s_AttrZ == attr.Name) ||
                    (sC_s_s_AttrB == attr.Name))
                    this.Z = float.Parse(attr.Value);
            }
        }
   
        /// <summary>
        /// Constructor, from a XPathNavigator.
        /// </summary>
        /// <param name="navigator"></param>
        public Vector3f(XPathNavigator navigator)
        {
            XPathNodeIterator it = navigator.Select(sC_s_xpath_XPathAttrs);
            while (it.MoveNext())
            {
                if ((sC_s_s_AttrX == it.Current.Name) ||
                    (sC_s_s_AttrR == it.Current.Name))
                    this.X = float.Parse(it.Current.Value);
                else if ((sC_s_s_AttrY == it.Current.Name) ||
                    (sC_s_s_AttrG == it.Current.Name))
                    this.Y = float.Parse(it.Current.Value);
                else if ((sC_s_s_AttrZ == it.Current.Name) ||
                    (sC_s_s_AttrB == it.Current.Name))
                    this.Z = float.Parse(it.Current.Value);
            }
        }
        #endregion // Constructors

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public override bool Equals(Vector3<float> v)
        {
            // If parameter is null return false:
            if (v == null)
                return false;

            return (this.X == v.X && this.Y == v.Y && this.Z == v.Z);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public override bool Equals(Object v)
        {
            // If parameter is null return false:
            if (v == null || !(v is Vector3f))
                return false;

            return this.Equals((Vector3f)v);
        }

        /// <summary>
        /// GetHashCode override.
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Distance between vector points.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public double Dist(Vector3f other)
        {
            return (System.Math.Sqrt((double)( ((this.X - other.X) * (this.X - other.X)) + ((this.Y - other.Y) * (this.Y - other.Y)) + ((this.Z - other.Z) * (this.Z - other.Z)))));
        }

        /// <summary>
        /// Distance-squared between vector points.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public double Dist2(Vector3f other)
        {
            return (double)(((this.X - other.X) * (this.X - other.X)) + ((this.Y - other.Y) * (this.Y - other.Y)) + ((this.Z - other.Z) * (this.Z - other.Z)));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override double Magnitude()
        {
            return (System.Math.Sqrt((double)((this.X * this.X) + (this.Y * this.Y) + (this.Z * this.Z))));
        }

        /// <summary>
        /// 
        /// </summary>
        public void Normalise()
        {
            double mag = this.Magnitude();
            // Avoid divide by 0 returning NaN
            if (mag == 0.0)
            {
                return;
            }
            this.X /= (float)mag;
            this.Y /= (float)mag;
            this.Z /= (float)mag;
        }

        /// <summary>
        /// Set xyz-components to absolute value of existing components.
        /// </summary>
        public void Abs()
        {
            this.X = System.Math.Abs(this.X);
            this.Y = System.Math.Abs(this.Y);
            this.Z = System.Math.Abs(this.Z);
        }

        /// <summary>
        /// Rotate the vector about an axis.
        /// </summary>
        /// <param name="radians"></param>
        /// <param name="axis"></param>
        public void RotateAboutAxis(float radians, char axis)
        {
            float t, tsin, tcos;
            tsin = (float) System.Math.Sin((double)radians);
            tcos = (float) System.Math.Cos((double)radians);
            if (axis == 'z')
            {
                t = this.X * tcos - this.Y * tsin;
                this.Y = this.X * tsin + this.Y * tcos;
                this.X = t;
            }
            else if (axis == 'x')
            {
                t = this.Y * tcos - this.Z * tsin;
                this.Z = this.Y * tsin + this.Z * tcos;
                this.Y = t;
            }
            else if (axis == 'y')
            {
                t = this.Z * tcos - this.X * tsin;
                this.X = this.Z * tsin + this.X * tcos;
                this.Z = t;
            }
        }

        public Vector3i ToVector3i()
        {
            return new Vector3i((int)X, (int)Y, (int)Z);
        }

        /// <summary>
        /// In-place set function for Vector3.  This is to eliminate having to reallocate the object's space.
        /// </summary>
        /// <param name="v"></param>
        public void Set(Vector3f v)
        {
            X = v.X;
            Y = v.Y;
            Z = v.Z;
        }

        /// <summary>
        /// In-place set for the minimum vector between this and another vector.
        /// </summary>
        /// <param name="v"></param>
        public void SetMin(Vector3f v)
        {
            X = System.Math.Min(X, v.X);
            Y = System.Math.Min(Y, v.Y);
            Z = System.Math.Min(Z, v.Z);
        }

        /// <summary>
        /// In-place set for the minimum vector between two separate vectors.
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        public void SetMin(Vector3f v1, Vector3f v2)
        {
            X = System.Math.Min(v1.X, v2.X);
            Y = System.Math.Min(v1.Y, v2.Y);
            Z = System.Math.Min(v1.Z, v2.Z);
        }

        /// <summary>
        /// In-place set for the maximum vector between this and another vector.
        /// </summary>
        /// <param name="v"></param>
        public void SetMax(Vector3f v)
        {
            X = System.Math.Max(X, v.X);
            Y = System.Math.Max(Y, v.Y);
            Z = System.Math.Max(Z, v.Z);
        }

        /// <summary>
        /// In-place iset for the maximum vector between two separate vectors.
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        public void SetMax(Vector3f v1, Vector3f v2)
        {
            X = System.Math.Max(v1.X, v2.X);
            Y = System.Math.Max(v1.Y, v2.Y);
            Z = System.Math.Max(v1.Z, v2.Z);
        }

        /// <summary>
        /// PURPOSE: Set the current vector to a linearly-interpolated value between two other vectors.
        /// PARAMS
        ///   a - The vector that represents a t value of 0.
        ///   b - The vector that represents a t value of 1.
        ///   t - The interpolation value for the desired point.
        /// NOTES
        ///   - "Lerp" is a contraction for "linear interpolation".
        ///   - If you already have the slope between a and b (i.e. b minus a), AddScaled() is a more
        ///     efficient way to accomplish what Lerp() does.
        /// </summary>
        /// <param name="t"></param>
        /// <param name="a"></param>
        /// <param name="b"></param>
        public void Lerp(float t, Vector3f a, Vector3f b)
	    {
		    X = a.X + (b.X - a.X) * t;
		    Y = a.Y + (b.Y - a.Y) * t;
		    Z = a.Z + (b.Z - a.Z) * t;
	    }

        #endregion // Public Methods

        #region Static Operators
        /// <summary>
        /// Equality operator
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool operator ==(Vector3f v1, Vector3f v2)
        {
            // If both are null, or both are same instance, return true.
            if (Object.ReferenceEquals(v1, v2))
                return true;

            // If one is null, but not both, return false.
            if (((Object)v1 == null) || ((Object)v2 == null))
                return false;

            // Return true if the fields match:
            return v1.X == v2.X && v1.Y == v2.Y && v1.Z == v2.Z;
        }

        /// <summary>
        /// Inequality operator
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool operator !=(Vector3f v1, Vector3f v2)
        {
            return !(v1 == v2);
        }

        /// <summary>
        /// Addition operator
        /// </summary>
        public static Vector3f operator +(Vector3f v1, Vector3f v2)
        {
            return (new Vector3f(v1.X + v2.X, v1.Y + v2.Y, v1.Z + v2.Z));
        }

        /// <summary>
        /// Subtraction Operator
        /// </summary>
        public static Vector3f operator -(Vector3f v1, Vector3f v2)
        {
            return (new Vector3f(v1.X - v2.X, v1.Y - v2.Y, v1.Z - v2.Z));
        }

        /// <summary>
        /// Sign Operator
        /// </summary>
        public static Vector3f operator -(Vector3f v2)
        {
            return (new Vector3f(-v2.X, -v2.Y, -v2.Z));
        }

        /// <summary>
        /// Scalar Multiplication Operator
        /// </summary>
        public static Vector3f operator *(Vector3f v, float f)
        {
            return (new Vector3f(v.X * f, v.Y * f, v.Z * f));
        }

        /// <summary>
        /// Scalar Division Operator
        /// </summary>
        /// <param name="v"></param>
        /// <param name="f"></param>
        /// <returns></returns>
        public static Vector3f operator /(Vector3f v, float f)
        {
            return (new Vector3f(v.X / f, v.Y / f, v.Z / f));
        }

        /// <summary>
        /// Explicit conversion to a float array.
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static explicit operator float[](Vector3f v)
        {
            return new float[] { v.X, v.Y, v.Z };
        }

        public static float Dot(Vector3f a, Vector3f b)
        {
            return a.X * b.X + a.Y * b.Y + a.Z * b.Z;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static Vector3f Cross(Vector3f a, Vector3f b)
        {
            Vector3f v = new Vector3f();
            v.X = (a.Y * b.Z) - (a.Z * b.Y);
            v.Y = (a.Z * b.X) - (a.X * b.Z);
            v.Z = (a.X * b.Y) - (a.Y * b.X);
            return (v);
        }

        public static Vector3f Normalise(Vector3f source)
        {
            Vector3f result = new Vector3f(source);
            result.Normalise();

            return result;
        }

        // Abs: returns the component-wize absolute value of a.
        public static Vector3f Abs(Vector3f a)
        {
            Vector3f v = new Vector3f(System.Math.Abs(a.X), System.Math.Abs(a.Y), System.Math.Abs(a.Z));
            return v;
        }

        // Min: returns the component-wise minimum of a and b.
        public static Vector3f Min(Vector3f a, Vector3f b)
        {
            Vector3f v = new Vector3f(System.Math.Min(a.X, b.X), System.Math.Min(a.Y, b.Y), System.Math.Min(a.Z, b.Z));
            return v;
        }

        // Max: returns the component-wise maximum of a and b.
        public static Vector3f Max(Vector3f a, Vector3f b)
        {
            Vector3f v = new Vector3f(System.Math.Max(a.X, b.X), System.Math.Max(a.Y, b.Y), System.Math.Max(a.Z, b.Z));
            return v;
        }

        public static bool IsGreaterThanOrEqualAll(Vector3f a, Vector3f b)
        {
            return (a.X >= b.X) && (a.Y >= b.Y) && (a.Z >= b.Z);
        }

        public static bool IsLessThanOrEqualAll(Vector3f a, Vector3f b)
        {
            return (a.X <= b.X) && (a.Y <= b.Y) && (a.Z <= b.Z);
        }

        public static Vector3f MinValue { get { return new Vector3f(float.MinValue, float.MinValue, float.MinValue); } }
        public static Vector3f MaxValue { get { return new Vector3f(float.MaxValue, float.MaxValue, float.MaxValue); } }

        #endregion // Static Operators

        #region Static Data
        public static readonly Vector3f Zero = new Vector3f(0.0f, 0.0f, 0.0f);
        #endregion
    };
    #endregion // cVector2<float>

    #region Vector3<int>
    /// <summary>
    /// Vector3 Specialisation (int)
    /// </summary>
    [Serializable]
    [DataContract]
    public class Vector3i : Vector3<int>
    {
        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public Vector3i()
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public Vector3i(int x, int y, int z)
            : base(x, y, z)
        {
        }

        /// <summary>
        /// Constructor, from a XmlNode.
        /// </summary>
        /// <param name="node"></param>
        public Vector3i(XmlNode node)
        {
            Debug.Assert(3 == node.Attributes.Count, "Invalid number of attributes for Vector3i.");
            foreach (XmlAttribute attr in node.Attributes)
            {
                if ((sC_s_s_AttrX == attr.Name) ||
                    (sC_s_s_AttrR == attr.Name))
                    this.X = int.Parse(attr.Value);
                else if ((sC_s_s_AttrY == attr.Name) ||
                    (sC_s_s_AttrG == attr.Name))
                    this.Y = int.Parse(attr.Value);
                else if ((sC_s_s_AttrZ == attr.Name) ||
                    (sC_s_s_AttrB == attr.Name))
                    this.Z = int.Parse(attr.Value);
            }
        }   
        #endregion // Constructors

        #region Public Methods
        public override double Magnitude()
        {
            return (System.Math.Sqrt((double)((this.X * this.X) + (this.Y * this.Y) + (this.Z * this.Z))));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public override bool Equals(Vector3<int> v)
        {
            // If parameter is null return false:
            if (v == null)
                return false;

            return (this.X == v.X && this.Y == v.Y && this.Z == v.Z);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public override bool Equals(Object v)
        {
            // If parameter is null return false:
            if (v == null || !(v is Vector3i))
                return false;

            return this.Equals((Vector3i)v);
        }

        /// <summary>
        /// GetHashCode override.
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        #endregion // Public Methods

        #region Static Operators
        /// <summary>
        /// Equality operator
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool operator ==(Vector3i v1, Vector3i v2)
        {
            // If both are null, or both are same instance, return true.
            if (Object.ReferenceEquals(v1, v2))
                return true;

            // If one is null, but not both, return false.
            if (((Object)v1 == null) || ((Object)v2 == null))
                return false;

            // Return true if the fields match:
            return v1.X == v2.X && v1.Y == v2.Y && v1.Z == v2.Z;
        }

        /// <summary>
        /// Inequality operator
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool operator !=(Vector3i v1, Vector3i v2)
        {
            return !(v1 == v2);
        }

        /// <summary>
        /// Addition operator
        /// </summary>
        public static Vector3i operator +(Vector3i v1, Vector3i v2)
        {
            return (new Vector3i(v1.X + v2.X, v1.Y + v2.Y, v1.Z + v2.Z));
        }

        /// <summary>
        /// Subtraction Operator
        /// </summary>
        public static Vector3i operator -(Vector3i v1, Vector3i v2)
        {
            return (new Vector3i(v1.X - v2.X, v1.Y - v2.Y, v1.Z - v2.Z));
        }

        /// <summary>
        /// Scalar Multiplication Operator
        /// </summary>
        public static Vector3i operator *(Vector3i v, int f)
        {
            return (new Vector3i(v.X * f, v.Y * f, v.Z * f));
        }
        #endregion // Static Operators
    };
    #endregion // cVector2<float>

} // End of RSG.Base.Math namespace

// End of file
