//
// File: Matrix34.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of Matrix34 classes
//

using System;
using System.Collections.Generic;
using System.Diagnostics;
using SMath = System.Math;
using System.Xml;
using System.Xml.XPath;

namespace RSG.Base.Math
{
    
    /// <summary>
    /// Matrix with 3 columns and 4 rows.
    /// </summary>
    public class Matrix34f : 
        IComparable,
        IComparable<Matrix34f>
    {
        #region Constants
        /// <summary>
        /// Identity matrix.
        /// </summary>
        public static readonly Matrix34f Identity = new Matrix34f();
        #endregion // Constants

        #region Properties and Associated Member Data
        public Vector3f A;
        public Vector3f B;
        public Vector3f C;
        public Vector3f D;

        /// <summary>
        /// Translation vector of transformation matrix.
        /// </summary>
        public Vector3f Translation
        {
            get { return this.D; }
            set { this.D = value; }
        }
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor, initialise to identity matrix.
        /// </summary>
        public Matrix34f()
        {
            this.A = new Vector3f(1.0f, 0.0f, 0.0f);
            this.B = new Vector3f(0.0f, 1.0f, 0.0f);
            this.C = new Vector3f(0.0f, 0.0f, 1.0f);
            this.D = new Vector3f(0.0f, 0.0f, 0.0f);
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <param name="d"></param>
        public Matrix34f(Vector3f a, Vector3f b, Vector3f c, Vector3f d)
        {
            this.A = new Vector3f(a);
            this.B = new Vector3f(b);
            this.C = new Vector3f(c);
            this.D = new Vector3f(d);
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Matrix34"/> class using the
        /// provided array.
        /// </summary>
        /// <param name="data"></param>
        public Matrix34f(float[][] data)
        {
            this.A = new Vector3f(data[0]);
            this.B = new Vector3f(data[1]);
            this.C = new Vector3f(data[2]);
            this.D = new Vector3f(data[3]);
        }

        /// <summary>
        /// Copy constructor.
        /// </summary>
        /// <param name="m"></param>
        public Matrix34f(Matrix34f m)
        {
            this.A = new Vector3f(m.A);
            this.B = new Vector3f(m.B);
            this.C = new Vector3f(m.C);
            this.D = new Vector3f(m.D);
        }

        /// <summary>
        /// Constructor from Quaternion.
        /// </summary>
        /// <param name="q"></param>
        public Matrix34f(Quaternionf q)
        {
            FromQuaternionf(q);
        }
        #endregion // Constructor(s)

        #region Static creation methods
        /// <summary>
        /// Creates a lookAt vector.
        /// </summary>
        /// <param name="dir"></param>
        /// <param name="up"></param>
        /// <returns></returns>
        public static Matrix34f LookAt(Vector3f dir, Vector3f up)
        {
            Vector3f c = Vector3f.Normalise(dir);
            Vector3f a = Vector3f.Normalise(Vector3f.Cross(up, c));
            Vector3f b = Vector3f.Cross(c, a);

            Matrix34f lookAtMatrix = new Matrix34f();
            lookAtMatrix.A = a;
            lookAtMatrix.B = b;
            lookAtMatrix.C = c;
            return lookAtMatrix;
        }

        /// <summary>
        /// Creates matrix rotated around the X-axis.
        /// </summary>
        /// <param name="theta"></param>
        /// <returns></returns>
        public static Matrix34f BuildXRotationMatrix(float theta)
        {
            float cosTheta = (float)System.Math.Cos((double)theta);
            float sinTheta = (float)System.Math.Sin((double)theta);

            return new Matrix34f(
                new Vector3f(1.0f,      0.0f,     0.0f),
                new Vector3f(0.0f,  cosTheta, sinTheta),
                new Vector3f(0.0f, -sinTheta, cosTheta),
                new Vector3f(0.0f,      0.0f,     0.0f));
        }

        /// <summary>
        /// Creates matrix rotated around the Y-axis.
        /// </summary>
        /// <param name="theta"></param>
        /// <returns></returns>
        public static Matrix34f BuildYRotationMatrix(float theta)
        {
            float cosTheta = (float)System.Math.Cos((double)theta);
            float sinTheta = (float)System.Math.Sin((double)theta);

            return new Matrix34f(
                new Vector3f(cosTheta, 0.0f, -sinTheta),
                new Vector3f(    0.0f, 1.0f,      0.0f),
                new Vector3f(sinTheta, 0.0f,  cosTheta),
                new Vector3f(    0.0f, 0.0f,      0.0f));
        }

        /// <summary>
        /// Crates matrix rotated around the Z-axis.
        /// </summary>
        /// <param name="theta"></param>
        /// <returns></returns>
        public static Matrix34f BuildZRotationMatrix(float theta)
        {
            float cosTheta = (float)System.Math.Cos((double)theta);
            float sinTheta = (float)System.Math.Sin((double)theta);

            return new Matrix34f(
                new Vector3f( cosTheta, sinTheta, 0.0f),
                new Vector3f(-sinTheta, cosTheta, 0.0f),
                new Vector3f(     0.0f,     0.0f, 1.0f),
                new Vector3f(     0.0f,     0.0f, 0.0f));
        }

        /// <summary>
        /// Creates a rotation matrix based on a from / lookAt point.
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        public static Matrix34f MakeRotationMatrix(Vector3f from, Vector3f to)
        {
            Vector3f v = Vector3f.Cross(from, to);
            float cosf = Vector3f.Dot(from, to);
            float sinf = (float) v.Magnitude();

            v /= sinf; //Normalise.

            float omc = 1.0f - cosf;

            Matrix34f rotationMatrix = new Matrix34f();

            rotationMatrix.A = new Vector3f(omc * v.X * v.X + cosf, omc * v.X * v.Y + sinf * v.Z, omc * v.X * v.Z - sinf * v.Y);
            rotationMatrix.B = new Vector3f(omc * v.X * v.Y - sinf * v.Z, omc * v.Y * v.Y + cosf, omc * v.Y * v.Z + sinf * v.X);
            rotationMatrix.C = new Vector3f(omc * v.X * v.Z + sinf * v.Y, omc * v.Y * v.Z - sinf * v.X, omc * v.Z * v.Z + cosf);
            return rotationMatrix;
        }
        #endregion

        #region Operators
        /// <summary>
        /// Addition operator.
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public static Matrix34f operator +(Matrix34f m, Matrix34f n)
        {
            return (new Matrix34f(m.A + n.A, m.B + n.B, m.C + n.C, m.D + n.D));
        }

        /// <summary>
        /// Subtraction operator.
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        /// Subtract a matrix m from the current matrix.
        public static Matrix34f operator -(Matrix34f m, Matrix34f n)
        {
            return (new Matrix34f(m.A - n.A, m.B - n.B, m.C - n.C, m.D - n.D));
        }

        /// <summary>
        /// Multiplication operator.
        /// </summary>
        /// <param name="m"></param>
        /// <param name="v"></param>
        /// <returns></returns>
        public static Vector3f operator *(Matrix34f m, Vector3f v)
        {
            float x = v.X * m.A.X + v.Y * m.B.X + v.Z * m.C.X + m.D.X;
            float y = v.X * m.A.Y + v.Y * m.B.Y + v.Z * m.C.Y + m.D.Y;
            float z = v.X * m.A.Z + v.Y * m.B.Z + v.Z * m.C.Z + m.D.Z;
            return (new Vector3f(x, y, z));
        }

        /// <summary>
        /// Multiplication operator.
        /// </summary>
        /// <param name="m"></param>
        /// <param name="v"></param>
        /// <returns></returns>
        public static BoundingBox3f operator *(Matrix34f m, BoundingBox3f bb)
        {
            // JWR - this is taken from 3D Math Primer for Graphics and Game Development

            BoundingBox3f result = new BoundingBox3f(
                new Vector3f(m.D.X, m.D.Y, m.D.Z),
                new Vector3f(m.D.X, m.D.Y, m.D.Z));

            if (m.A.X > 0.0f)
            {
                result.Min.X += m.A.X * bb.Min.X;
                result.Max.X += m.A.X * bb.Max.X;
            }
            else 
            {
                result.Min.X += m.A.X * bb.Max.X;
                result.Max.X += m.A.X * bb.Min.X;
            }

            if (m.A.Y > 0.0f)
            {
                result.Min.Y += m.A.Y * bb.Min.X;
                result.Max.Y += m.A.Y * bb.Max.X;
            }
            else
            {
                result.Min.Y += m.A.Y * bb.Max.X;
                result.Max.Y += m.A.Y * bb.Min.X;
            }

            if (m.A.Z > 0.0f)
            {
                result.Min.Z += m.A.Z * bb.Min.X;
                result.Max.Z += m.A.Z * bb.Max.X;
            }
            else
            {
                result.Min.Z += m.A.Z * bb.Max.X;
                result.Max.Z += m.A.Z * bb.Min.X;
            }


            if (m.B.X > 0.0f)
            {
                result.Min.X += m.B.X * bb.Min.Y;
                result.Max.X += m.B.X * bb.Max.Y;
            }
            else
            {
                result.Min.X += m.B.X * bb.Max.Y;
                result.Max.X += m.B.X * bb.Min.Y;
            }

            if (m.B.Y > 0.0f)
            {
                result.Min.Y += m.B.Y * bb.Min.Y;
                result.Max.Y += m.B.Y * bb.Max.Y;
            }
            else
            {
                result.Min.Y += m.B.Y * bb.Max.Y;
                result.Max.Y += m.B.Y * bb.Min.Y;
            }

            if (m.B.Z > 0.0f)
            {
                result.Min.Z += m.B.Z * bb.Min.Y;
                result.Max.Z += m.B.Z * bb.Max.Y;
            }
            else
            {
                result.Min.Z += m.B.Z * bb.Max.Y;
                result.Max.Z += m.B.Z * bb.Min.Y;
            }


            if (m.C.X > 0.0f)
            {
                result.Min.X += m.C.X * bb.Min.Z;
                result.Max.X += m.C.X * bb.Max.Z;
            }
            else
            {
                result.Min.X += m.C.X * bb.Max.Z;
                result.Max.X += m.C.X * bb.Min.Z;
            }

            if (m.C.Y > 0.0f)
            {
                result.Min.Y += m.C.Y * bb.Min.Z;
                result.Max.Y += m.C.Y * bb.Max.Z;
            }
            else
            {
                result.Min.Y += m.C.Y * bb.Max.Z;
                result.Max.Y += m.C.Y * bb.Min.Z;
            }

            if (m.C.Z > 0.0f)
            {
                result.Min.Z += m.C.Z * bb.Min.Z;
                result.Max.Z += m.C.Z * bb.Max.Z;
            }
            else
            {
                result.Min.Z += m.C.Z * bb.Max.Z;
                result.Max.Z += m.C.Z * bb.Min.Z;
            }

            return result;
        }

        /// <summary>
        /// Multiplication operator.
        /// </summary>
        /// <param name="m"></param>
        /// <param name="n"></param>
        /// <returns></returns>
        public static Matrix34f operator *(Matrix34f m, Matrix34f n)
        {
            float px = m.A.X * n.A.X + m.A.Y * n.B.X + m.A.Z * n.C.X;
            float py = m.A.X * n.A.Y + m.A.Y * n.B.Y + m.A.Z * n.C.Y;
            float pz = m.A.X * n.A.Z + m.A.Y * n.B.Z + m.A.Z * n.C.Z;
				
			float qx = m.B.X * n.A.X + m.B.Y * n.B.X + m.B.Z * n.C.X;
            float qy = m.B.X * n.A.Y + m.B.Y * n.B.Y + m.B.Z * n.C.Y;
		    float qz = m.B.X * n.A.Z + m.B.Y * n.B.Z + m.B.Z * n.C.Z;
				
		    float rx = m.C.X * n.A.X + m.C.Y * n.B.X + m.C.Z * n.C.X;
			float ry = m.C.X * n.A.Y + m.C.Y * n.B.Y + m.C.Z * n.C.Y;
			float rz = m.C.X * n.A.Z + m.C.Y * n.B.Z + m.C.Z * n.C.Z;
				
			float sx = m.D.X * n.A.X + m.D.Y * n.B.X + m.D.Z * n.C.X + n.D.X;
			float sy = m.D.X * n.A.Y + m.D.Y * n.B.Y + m.D.Z * n.C.Y + n.D.Y;
			float sz = m.D.X * n.A.Z + m.D.Y * n.B.Z + m.D.Z * n.C.Z + n.D.Z;

            Vector3f a = new Vector3f(px, py, pz);
            Vector3f b = new Vector3f(qx, qy, qz);
            Vector3f c = new Vector3f(rx, ry, rz);
            Vector3f d = new Vector3f(sx, sy, sz);
            return (new Matrix34f(a, b, c, d));
        }

        /// <summary>
        /// Explicit conversion to a 2d float array.
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public static explicit operator float[][](Matrix34f m)
        {
            return new float[][] { (float[])m.A, (float[])m.B, (float[])m.C, (float[])m.D };
        }

        #endregion // Operators

        #region Public Methods
        /// <summary>
        /// Transform a vector by the current matrix.
        /// </summary>
        /// <param name="v"></param>
        public void Transform(Vector3f v)
        {
            float newX = (v.X * this.A.X) + (v.Y * this.B.X) + (v.Z * this.C.X) + this.D.X;
            float newY = (v.X * this.A.Y) + (v.Y * this.B.Y) + (v.Z * this.C.Y) + this.D.Y;
            float newZ = (v.X * this.A.Z) + (v.Y * this.B.Z) + (v.Z * this.C.Z) + this.D.Z;

            v.X = newX;
            v.Y = newY;
            v.Z = newZ;
        }

        /// <summary>
        /// Rotates the matrix by a given axis and angle.
        /// </summary>
        /// <param name="axis"></param>
        /// <param name="theta"></param>
        public void RotateUnitAxis(Vector3f axis, float theta)
        {
            Debug.Assert(axis.Magnitude() >= 0.999f && axis.Magnitude() <= 1.001f);

            float cost = (float)System.Math.Cos(theta);
            float sint = (float)System.Math.Sin(theta);

            float omc = 1.0f - cost;

            this.A.X = omc * axis.X * axis.X + cost;
            this.B.Y = omc * axis.Y * axis.Y + cost;
            this.C.Z = omc * axis.Z * axis.Z + cost;
            this.A.Y = omc * axis.X * axis.Y + sint * axis.Z;
            this.B.X = omc * axis.X * axis.Y - sint * axis.Z;
            this.A.Z = omc * axis.X * axis.Z - sint * axis.Y;
            this.C.X = omc * axis.X * axis.Z + sint * axis.Y;
            this.B.Z = omc * axis.Y * axis.Z + sint * axis.X;
            this.C.Y = omc * axis.Y * axis.Z - sint * axis.X;
        }

        /// <summary>
        /// Apply a uniform scale to the current matrix.
        /// </summary>
        /// <param name="s"></param>
        public void Scale(float s)
        {
            this.A /= s;
            this.B /= s;
            this.C /= s;
        }

        /// <summary>
        /// Apply a non-uniform scale to the current matrix.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public void Scale(float x, float y, float z)
        {
            this.A.X *= x; this.B.X *= x; this.C.X *= x; 
            this.A.Y *= y; this.B.Y *= y; this.C.Y *= y;
            this.A.Z *= z; this.B.Z *= z; this.C.Z *= z;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Matrix34f Normalise()
        {
            Matrix34f m = new Matrix34f(this);
            m.C.Normalise();
            m.A = Vector3f.Cross(m.B, m.C);
            m.A.Normalise();
            m.B = Vector3f.Cross(m.C, m.A);
            return (m);
        }

        /// <summary>
        /// 
        /// </summary>
        public void Abs()
        {
            this.A.Abs();
            this.B.Abs();
            this.C.Abs();
            this.D.Abs();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Vector3f ToEulers()
        {
            Vector3f v = new Vector3f();
            Matrix34f mtx = Normalise();
            v.X = (float)SMath.Atan2(mtx.B.Z, mtx.C.Z);
            v.Y = (float)SMath.Asin(-mtx.A.Z);
            v.Z = (float)SMath.Atan2(mtx.A.Y, mtx.A.X);
            return (v);
        }

        public Matrix34f Transpose()
        {
            // temp vars due to error CS0206: A property or indexer may not be passed as an out or ref parameter =(
            Matrix34f m = new Matrix34f(this);
            float temp1 = m.A.Y, temp2 = m.B.X;
            Utils.Swap<float>(ref temp1, ref temp2);
            m.A.Y = temp1; m.B.X = temp2;
            temp1 = m.A.Z; temp2 = m.C.X;
            Utils.Swap<float>(ref temp1, ref temp2);
            m.A.Z = temp1; m.C.X = temp2;
            temp1 = m.B.Z; temp2 = m.C.Y;
            Utils.Swap<float>(ref temp1, ref temp2);
            m.B.Z = temp1; m.C.Y = temp2;
            return m;
        }

        /// <summary>
        /// Return the inverse of this Matrix34f.
        /// </summary>
        /// <returns></returns>
        public Matrix34f Inverse()
        {
            // Get three of our subdeterminants
            float subDetX = Utils.Det22(B.Y, B.Z, C.Y, C.Z);
            float subDetY = Utils.Det22(B.X, B.Z, C.X, C.Z);
            float subDetZ = Utils.Det22(B.X, B.Y, C.X, C.Y);

            // Find the largest absolute value element.
            float largest = Utils.Maximum(
                SMath.Abs(A.X), SMath.Abs(A.Y), SMath.Abs(A.Z),
                SMath.Abs(B.X), SMath.Abs(B.Y), SMath.Abs(B.Z),
                SMath.Abs(C.X), SMath.Abs(C.Y), SMath.Abs(C.Z));

            // Get the inverse of the determinant.
            float invDet = (A.X * subDetX) - (A.Y * subDetY) + (A.Z * subDetZ);
            if (SMath.Abs(invDet) > (3.6e-7 * largest))
            {
                Matrix34f invm = new Matrix34f();
                invDet = 1.0f / invDet;

                // Start making the inverse matrix.
                invm.A.X = subDetX * invDet;
                invm.B.X = -subDetY * invDet;
                invm.C.X = subDetZ * invDet;
                invm.D.X = -(D.X * invm.A.X + D.Y * invm.B.X + D.Z * invm.C.X);

                // Get 3 more subdeterminants
                subDetX = Utils.Det22( A.Y, A.Z, C.Y, C.Z );
				subDetY = Utils.Det22( A.X, A.Z, C.X, C.Z );
				subDetZ = Utils.Det22( A.X, A.Y, C.X, C.Y );
				
				// Add more terms to the inverse matrix.
				invm.A.Y = -subDetX*invDet;
				invm.B.Y = subDetY*invDet;
				invm.C.Y = -subDetZ*invDet;
				invm.D.Y = -(D.X * invm.A.Y + D.Y * invm.B.Y + D.Z * invm.C.Y);
				
				// Get the last three subdeterminants.
				subDetX = Utils.Det22( A.Y, A.Z, B.Y, B.Z );
				subDetY = Utils.Det22( A.X, A.Z, B.X, B.Z );
				subDetZ = Utils.Det22( A.X, A.Y, B.X, B.Y );
				
				// Finish making the inverse matrix.
				invm.A.Z = subDetX*invDet;
				invm.B.Z = -subDetY*invDet;
				invm.C.Z = subDetZ*invDet;
				invm.D.Z = -(D.X * invm.A.Z + D.Y * invm.B.Z + D.Z * invm.C.Z);
				
				// Return inverted matrix (the determinant was not too close to zero).
                return (invm);
            }

            // Return identity matrix (determinant was too close to zero).
            return (new Matrix34f());
        }

        public void Orthonormalize()
        {
            Vector3f sourceX = new Vector3f(A);
            Vector3f sourceY = new Vector3f(B);

            Vector3f newZ = Vector3f.Cross(sourceX, sourceY);
            Vector3f newX = Vector3f.Cross(sourceY, newZ);

            A = Vector3f.Normalise(newX);
            B = Vector3f.Normalise(sourceY);
            C = Vector3f.Normalise(newZ);
        }

        /// <summary>
        /// Interpolates between two matrices.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="goal"></param>
        /// <param name="t"></param>
        public void Interpolate(Matrix34f source, Matrix34f goal, float t)
        {
            Quaternionf s = Quaternionf.FromMatrix34(source);
            Quaternionf g = Quaternionf.FromMatrix34(goal);

            s.PrepareSlerp(g); // shortest path
            s.Slerp(t, s, g);
            FromQuaternionf(s);
            this.D.Lerp(t, source.D, goal.D);
        }

        /// <summary>
        /// Builds a matrix from a quaternion.
        /// </summary>
        public void FromQuaternionf(Quaternionf q)
        {
            Debug.Assert(q.IsNormalised(), "Quaternion is not a pure rotation.");

            this.A = new Vector3f();
            this.B = new Vector3f();
            this.C = new Vector3f();
            this.D = new Vector3f();

#if true
            // See ::rage::Matrix34 
            double tx = (q.X * Constants.M_SQRT2);
            double ty = (q.Y * Constants.M_SQRT2);
            double tz = (q.Z * Constants.M_SQRT2);
            double tw = (q.W * Constants.M_SQRT2);

            this.A.Y = (float)(tx * ty + tz * tw);
            this.B.X = (float)(tx * ty - tz * tw);

            this.A.Z = (float)(tx * tz - ty * tw);
            this.C.X = (float)(tx * tz + ty * tw);

            this.B.Z = (float)(ty * tz + tx * tw);
            this.C.Y = (float)(ty * tz - tx * tw);

            // Need squares along diagonal.
            tx *= tx;
            ty *= ty;
            tz *= tz;

            this.A.X = 1.0f - (float)(ty + tz);
            this.B.Y = 1.0f - (float)(tz + tx);
            this.C.Z = 1.0f - (float)(ty + tx);

#else
            // Mathematics for 3D Game Programming & Computer Graphics
            // (no optmimisations)
            this.A.X = (1.0f - (2.0f * (q.Y * q.Y)) - (2.0f * (q.Z * q.Z)));
            this.A.Y = (2.0f * q.X * q.Y) + (2.0f * q.W * q.Z);
            this.A.Z = (2.0f * q.X * q.Z) - (2.0f * q.W * q.Y);

            this.B.X = (2.0f * q.X * q.Y) - (2.0f * q.W * q.Z);
            this.B.Y = (1.0f - (2.0f * (q.X * q.X)) - (2.0f * (q.Z * q.Z)));
            this.B.Z = (2.0f * q.Y * q.Z) + (2.0f * q.W * q.X);

            this.C.X = (2.0f * q.X * q.Z) + (2.0f * q.W * q.Y);
            this.C.Y = (2.0f * q.Y * q.Z) - (2.0f * q.W * q.X);
            this.C.Z = (1.0f - (2.0f * (q.X * q.X)) - (2.0f * (q.Y * q.Y)));
#endif // false
        }
        #endregion // Public Methods

        #region IComparable and IComparable<StreamableObject> Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        int IComparable.CompareTo(Object obj)
        {
            if (obj is Matrix34f)
                return (CompareTo(obj as Matrix34f));
            throw new ArgumentException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int CompareTo(Matrix34f obj)
        {
            int a = this.A.CompareTo(obj.A);
            int b = this.B.CompareTo(obj.B);
            int c = this.C.CompareTo(obj.C);
            int d = this.D.CompareTo(obj.D);
            return (a + b + c + d);
        }
        #endregion // IComparable and IComparable<Archetype> Methods
    }

} // RSG.Base.Math namespace

// Matrix34.cs
