﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using SMath = System.Math;

namespace RSG.Base.Math
{
    /// <summary>
    /// 
    /// </summary>
    public class Matrix22f
    {
        #region Constants
        /// <summary>
        /// Identity matrix.
        /// </summary>
        public static readonly Matrix22f Identity = new Matrix22f();
        #endregion // Constants

        #region Properties and Associated Member Data
        public Vector2f A;
        public Vector2f B;
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor, initialise to identity matrix.
        /// </summary>
        public Matrix22f()
        {
            this.A = new Vector2f(1.0f, 0.0f);
            this.B = new Vector2f(0.0f, 1.0f);
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        public Matrix22f(Vector2f a, Vector2f b)
        {
            this.A = new Vector2f(a);
            this.B = new Vector2f(b);
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Matrix22"/> class using the
        /// provided array. 
        /// </summary>
        /// <param name="data"></param>
        public Matrix22f(float[][] data)
        {
            this.A = new Vector2f(data[0]);
            this.B = new Vector2f(data[1]);
        }

        /// <summary>
        /// 
        /// provided array.
        /// </summary>
        /// <param name="data"></param>
        public Matrix22f(float ax, float ay, float bx, float by)
        {
            this.A = new Vector2f(ax, ay);
            this.B = new Vector2f(bx, by);
        }

        /// <summary>
        /// Copy constructor.
        /// </summary>
        /// <param name="m"></param>
        public Matrix22f(Matrix22f m)
        {
            this.A = new Vector2f(m.A);
            this.B = new Vector2f(m.B);
        }
        #endregion // Constructor(s)

        #region Static Methods
        /// <summary>
        /// Create Matrix22f based on rotation in radians
        /// </summary>
        /// <param name="radians"></param>
        /// <returns></returns>
        public static Matrix22f CreateFromRotation(float radians)
        {
            float sinTheta = (float)SMath.Sin((double)radians);
            float cosTheta = (float)SMath.Cos((double)radians);
            return new Matrix22f(new Vector2f(cosTheta, sinTheta), 
                                    new Vector2f(-sinTheta, cosTheta));
        }
        #endregion

        #region Operators
        /// <summary>
        /// Multiplication operator.
        /// </summary>
        /// <param name="m"></param>
        /// <param name="v"></param>
        /// <returns></returns>
        public static Vector2f operator *(Matrix22f m, Vector2f v)
        {
            float x = v.X * m.A.X + v.Y * m.B.X;
            float y = v.X * m.A.Y + v.Y * m.B.Y;
            return (new Vector2f(x, y));
        }

        /// <summary>
        /// Explicit conversion to a 2d float array.
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public static explicit operator float[][](Matrix22f m)
        {
            return new float[][] { (float[])m.A, (float[])m.B};
        }
        #endregion // Operators

        #region Public Methods
        /// <summary>
        /// Transform a vector by the current matrix.
        /// </summary>
        /// <param name="v"></param>
        public void Transform(Vector2f v)
        {
            float newX = (v.X * this.A.X) + (v.Y * this.B.X);
            float newY = (v.X * this.A.Y) + (v.Y * this.B.Y);
            v.X = newX;
            v.Y = newY;
        }

        /// <summary>
        /// 
        /// </summary>
        public void Abs()
        {
            this.A.Abs();
            this.B.Abs();
        }
        #endregion // Public Methods
    }
} // RSG.Base.Math namespace
