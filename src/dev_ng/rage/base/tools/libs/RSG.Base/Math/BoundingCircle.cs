﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMath = System.Math;

namespace RSG.Base.Math
{
    /// <summary>
    /// 
    /// </summary>
    public class BoundingCirclef
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Centre point of bounding sphere.
        /// </summary>
        public Vector2f Centre
        {
            get { return m_vCentre; }
            set { m_vCentre = value; }
        }
        private Vector2f m_vCentre;

        /// <summary>
        /// Radius of bounding sphere.
        /// </summary>
        public float Radius
        {
            get { return m_fRadius; }
            set { m_fRadius = value; }
        }
        private float m_fRadius;
        #endregion // Properties and Associated Member Data
        
        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="centre">Centre point of sphere</param>
        /// <param name="radius">Radius of sphere</param>
        public BoundingCirclef(Vector2f centre, float radius)
        {
            this.Centre = centre;
            this.Radius = radius;
        }

        /// <summary>
        /// Standard constructor. Zero initialised members.
        /// </summary>
        public BoundingCirclef()
        {
            this.Centre = new Vector2f();
            this.Radius = 0;
        }

        /// <summary>
        /// Standard constructor. Zero initialised members.
        /// </summary>
        public BoundingCirclef(BoundingBox2f bbox)
        {
            float distx = (bbox.Max.X - bbox.Min.X) / 2.0f;
            float disty = (bbox.Max.Y - bbox.Min.Y) / 2.0f;
            double dista = SMath.Sqrt((double)(distx * distx) + (double)(disty * disty));

            this.Centre = new Vector2f(bbox.Min.X + distx, bbox.Min.Y + disty);
            this.Radius = (float)dista;
        }
        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// Point completely contained within this BoundingCirclef.
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public bool Contains(Vector2f point)
        {
            Vector2f diff = point - Centre;
            return (diff.Dist2(Centre) < Radius * Radius);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Intersects(BoundingCirclef other)
        {
            double centreDistanceSquared = Centre.Dist2(other.Centre);
            double combinedRadiusSquared = (Radius + other.Radius) * (Radius + other.Radius);
            return (centreDistanceSquared < combinedRadiusSquared);
        }

        #endregion // Public Methods
    } // BoundingCircle
}
