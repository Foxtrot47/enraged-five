//
// File: cBoundingBox2D.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of cBoundingBox2D.cs class
//

using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.Serialization;

namespace RSG.Base.Math
{

    /// <summary>
    /// 2D Bounding Box (floating-point coordinate system).
    /// </summary>
    [DataContract]
    public class BoundingBox2f
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Min
        /// </summary>
        [DataMember]
        public Vector2f Min
        {
            get { return m_vMin; }
            set
            {
                if (m_vMin != value)
                {
                    m_vMin = value;
                    this.m_bIsEmpty = false;
                }
            }
        }
        private Vector2f m_vMin;

        /// <summary>
        /// Max
        /// </summary>
        [DataMember]
        public Vector2f Max
        {
            get { return m_vMax; }
            set
            {
                if (m_vMax != value)
                {
                    m_vMax = value;
                    this.m_bIsEmpty = false;
                }
            }
        }
        private Vector2f m_vMax;

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public bool IsEmpty
        {
            get { return m_bIsEmpty; }
        }
        private bool m_bIsEmpty;

        /// <summary>
        /// Width of bound.
        /// </summary>
        public float Width
        {
            get { return (this.Max.X - this.Min.X); }
        }

        /// <summary>
        /// Height of bound.
        /// </summary>
        public float Height
        {
            get { return (this.Max.Y - this.Min.Y); }
        }
        #endregion // Properties and Associated Member Data

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public BoundingBox2f()
        {
            this.Min = new Vector2f(float.MaxValue, float.MaxValue);
            this.Max = new Vector2f(float.MinValue, float.MinValue);
            this.m_bIsEmpty = true;
        }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="box"></param>
        public BoundingBox2f(BoundingBox2f box)
        {
            this.Min = new Vector2f(float.MaxValue, float.MaxValue);
            this.Max = new Vector2f(float.MinValue, float.MinValue);
            this.Expand(box.Min);
            this.Expand(box.Max);
        }

        /// <summary>
        /// Constructor from 3D bounding box.
        /// </summary>
        /// <param name="box"></param>
        public BoundingBox2f(BoundingBox3f box)
        {
            this.Min = new Vector2f(float.MaxValue, float.MaxValue);
            this.Max = new Vector2f(float.MinValue, float.MinValue);
            this.Expand(new Vector2f(box.Min.X, box.Min.Y));
            this.Expand(new Vector2f(box.Max.X, box.Max.Y));
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        public BoundingBox2f(Vector2f min, Vector2f max)
        {
            Debug.Assert(min.X <= max.X);
            Debug.Assert(min.Y <= max.Y);
            this.m_bIsEmpty = false;
            this.Min = min;
            this.Max = max;
        }
        #endregion // Constructors

        #region Public Methods
        /// <summary>
        /// Return centre point of bound.
        /// </summary>
        /// <returns></returns>
        public Vector2f Centre()
        {
            Vector2f centre = (this.Min + this.Max) / 2.0f;
            return (centre);
        }

        /// <summary>
        /// Expand bound to contain a point.
        /// </summary>
        /// <param name="point"></param>
        public void Expand(Vector2f point)
        {
            if (null == this.Min)
                this.Min = new Vector2f(point.X, point.Y);
            if (null == this.Max)
                this.Max = new Vector2f(point.X, point.Y);

            if (this.Contains(point))
                return;

            this.Min.X = System.Math.Min(point.X, this.Min.X);
            this.Min.Y = System.Math.Min(point.Y, this.Min.Y);
            this.Max.X = System.Math.Max(point.X, this.Max.X);
            this.Max.Y = System.Math.Max(point.Y, this.Max.Y);
            this.m_bIsEmpty = false;
        }

        /// <summary>
        /// Expand bound to contain another bound.
        /// </summary>
        /// <param name="box"></param>
        public void Expand(BoundingBox2f box)
        {
            this.Expand(box.Min);
            this.Expand(box.Max);
        }

        /// <summary>
        /// Point completely contained within this BoundingBox2f.
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public bool Contains(Vector2f point)
        {
            bool xInRange = (this.Min.X <= point.X) && (point.X <= this.Max.X);
            bool yInRange = (this.Min.Y <= point.Y) && (point.Y <= this.Max.Y);

            return (xInRange && yInRange);
        }

        /// <summary>
        /// Bound completely contained within this BoundingBox2f.
        /// </summary>
        /// <param name="bound"></param>
        /// <returns></returns>
        public bool Contains(BoundingBox2f bound)
        {
            return (this.Contains(bound.Min) && this.Contains(bound.Max));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="b"></param>
        /// <returns></returns>
        public bool Intersects(BoundingBox2f b)
        {
            if (this.Min.X > b.Max.X || this.Max.X < b.Min.X ||
                this.Min.Y > b.Max.Y || this.Max.Y < b.Min.Y)
                return (false);

            return (true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="b"></param>
        /// <returns></returns>
        public bool Intersects(BoundingBox3f b)
        {
            if (this.Min.X > b.Max.X || this.Max.X < b.Min.X ||
                this.Min.Y > b.Max.Y || this.Max.Y < b.Min.Y)
                return (false);

            return (true);
        }

        /// <summary>
        /// Returns array of the four points
        /// </summary>
        /// <returns></returns>
        public Vector2f[] GetPoints()
        {
            return new Vector2f[]{ new Vector2f(Max), new Vector2f(Max.X, Min.Y), new Vector2f(Min), new Vector2f(Min.X, Max.Y) };
        }
        #endregion // Public Methods

        #region Type Conversion Methods
        /// <summary>
        /// Conversion from a System.Drawing.RectangleF object.
        /// </summary>
        /// <param name="rect"></param>
        /// <returns></returns>
        public static explicit operator BoundingBox2f(RectangleF rect)
        {
            BoundingBox2f bbox = new BoundingBox2f();
            bbox.Min = new Vector2f(rect.Left, rect.Bottom);
            bbox.Max = new Vector2f(rect.Right, rect.Top);
            return (bbox);
        }

        /// <summary>
        /// Conversion to a System.Drawing.RectangleF object.
        /// </summary>
        /// <param name="bbox"></param>
        /// <returns></returns>
        public static explicit operator RectangleF(BoundingBox2f bbox)
        {
            RectangleF rect = new RectangleF();
            // Upper-left corner
            rect.X = bbox.Min.X;
            rect.Y = bbox.Max.Y;

            rect.Width = (bbox.Max.X - bbox.Min.X);
            rect.Height = System.Math.Abs(bbox.Min.Y - bbox.Max.Y);
            return (rect);
        }
        #endregion // Type Conversion Methods
    }

    /// <summary>
    /// 2D Bounding Box (integer coordinate system).
    /// </summary>
    public class BoundingBox2i
    {
        #region Events
        /// <summary>
        /// Event raised when stored bounds changes
        /// </summary>
        public event EventHandler BoundsChangedEvent;
        #endregion // Events

        #region Properties and Associated Member Data
        /// <summary>
        /// Minimum of bound.
        /// </summary>
        public Vector2i Min
        {
            get { return m_vMin; }
            set { m_vMin = value; }
        }
        private Vector2i m_vMin;

        /// <summary>
        /// Maximum of bound.
        /// </summary>
        public Vector2i Max
        {
            get { return m_vMax; }
            set { m_vMax = value; }
        }
        private Vector2i m_vMax;
        #endregion // Properties and Associated Member Data

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public BoundingBox2i()
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        public BoundingBox2i(Vector2i min, Vector2i max)
        {
            Debug.Assert(min.X <= max.X);
            Debug.Assert(min.Y <= max.Y);

            this.Min = min;
            this.Max = max;
        }
        #endregion // Constructors

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="point"></param>
        public void Expand(Vector2i point)
        {
            if (null == this.Min)
                this.Min = new Vector2i(point.X, point.Y);
            if (null == this.Max)
                this.Max = new Vector2i(point.X, point.Y);

            if (this.Contains(point))
                return;

            this.Min.X = System.Math.Min(point.X, this.Min.X);
            this.Min.Y = System.Math.Min(point.Y, this.Min.Y);
            this.Max.X = System.Math.Max(point.X, this.Max.X);
            this.Max.Y = System.Math.Max(point.Y, this.Max.Y);

            RaiseBoundsChangedEvent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public bool Contains(Vector2i point)
        {
            bool xInRange = (this.Min.X <= point.X) && (point.X <= this.Max.X);
            bool yInRange = (this.Min.Y <= point.Y) && (point.Y <= this.Max.Y);

            return (xInRange && yInRange);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="b"></param>
        /// <returns></returns>
        public bool Intersects(BoundingBox2i b)
        {
            if (this.Min.X > b.Max.X || this.Max.X < b.Min.X ||
                this.Min.Y > b.Max.Y || this.Max.Y < b.Min.Y)
                return (false);

            return (true);
        }
        #endregion // Public Methods

        #region Private Methods
        /// <summary>
        /// Raise BoundsChanged Event (if we have listeners)
        /// </summary>
        private void RaiseBoundsChangedEvent()
        {
            if (null != this.BoundsChangedEvent)
                this.BoundsChangedEvent(this, new EventArgs());
        }
        #endregion // Private Methods
    }

} // End of RSG.Base.Math namespace

// End of file
