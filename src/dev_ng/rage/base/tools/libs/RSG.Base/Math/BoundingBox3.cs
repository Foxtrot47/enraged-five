//
// File: BoundingBox3.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of BoundingBox3 classes
//

using System;
using System.Diagnostics;
using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace RSG.Base.Math
{

    /// <summary>
    /// 3D BoundingBox
    /// </summary>
    [DataContract]
    public class BoundingBox3f
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Minimum of bound.
        /// </summary>
        [XmlElement("min")]
        [DataMember]
        public Vector3f Min
        {
            get { return m_vMin; }
            set
            {
                if (m_vMin != value)
                {
                    m_vMin = value;
                    this.m_bIsEmpty = false;
                }
            }
        }
        private Vector3f m_vMin;

        /// <summary>
        /// Maximum of bound.
        /// </summary>
        [XmlElement("max")]
        [DataMember]
        public Vector3f Max
        {
            get { return m_vMax; }
            set
            {
                if (m_vMax != value)
                {
                    m_vMax = value;
                    this.m_bIsEmpty = false;
                }
            }
        }
        private Vector3f m_vMax;

        /// <summary>
        /// 
        /// </summary>
        public bool IsEmpty
        {
            get { return m_bIsEmpty; }
        }
        private bool m_bIsEmpty;
        #endregion // Properties and Associated Member Data

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public BoundingBox3f()
        {
            this.Min = new Vector3f(float.MaxValue, float.MaxValue, float.MaxValue);
            this.Max = new Vector3f(float.MinValue, float.MinValue, float.MinValue);
            this.m_bIsEmpty = true;
        }

        /// <summary>
        /// Constructor for two vectors.
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        public BoundingBox3f(Vector3f min, Vector3f max)
        {
            Debug.Assert(min.X <= max.X, "Min and max X-components are invalid.");
            Debug.Assert(min.Y <= max.Y, "Min and max Y-components are invalid.");
            Debug.Assert(min.Z <= max.Z, "Min and max Z-components are invalid.");
            this.m_bIsEmpty = false;
            this.Min = new Vector3f(min);
            this.Max = new Vector3f(max);
        }

        /// <summary>
        /// Constructor for components for two vectors.
        /// </summary>
        /// <param name="minX"></param>
        /// <param name="minY"></param>
        /// <param name="minZ"></param>
        /// <param name="maxX"></param>
        /// <param name="maxY"></param>
        /// <param name="maxZ"></param>
        public BoundingBox3f(float minX, float minY, float minZ, float maxX, float maxY, float maxZ)
        {
            Debug.Assert(minX <= maxX, "Min and max X-components are invalid.");
            Debug.Assert(minY <= maxY, "Min and max Y-components are invalid.");
            Debug.Assert(minZ <= maxZ, "Min and max Z-components are invalid.");
            this.m_bIsEmpty = false;
            this.Min = new Vector3f(minX, minY, minZ);
            this.Max = new Vector3f(maxX, maxY, maxZ);
        }

        /// <summary>
        /// Copy constructor.
        /// </summary>
        /// <param name="bbox"></param>
        public BoundingBox3f(BoundingBox3f bbox)
        {
            this.Min = new Vector3f(bbox.Min);
            this.Max = new Vector3f(bbox.Max);
        }
        #endregion // Constructors

        #region Controller Methods
        /// <summary>
        /// Return centre point of bound.
        /// </summary>
        /// <returns></returns>
        public Vector3f Centre()
        {
            Vector3f centre = (this.Min + this.Max) / 2.0f;
            return (centre);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="point"></param>
        public void Expand(Vector3f point)
        {
            this.Min.X = System.Math.Min(point.X, this.Min.X);
            this.Min.Y = System.Math.Min(point.Y, this.Min.Y);
            this.Min.Z = System.Math.Min(point.Z, this.Min.Z);
            this.Max.X = System.Math.Max(point.X, this.Max.X);
            this.Max.Y = System.Math.Max(point.Y, this.Max.Y);
            this.Max.Z = System.Math.Max(point.Z, this.Max.Z);
            this.m_bIsEmpty = false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="box"></param>
        public void Expand(BoundingBox3f box)
        {
            if (null == box)
                return;
            this.Expand(box.Min);
            this.Expand(box.Max);
        }

        /// <summary>
        /// Determines if the 2D point exists within the bounding box.
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public bool Contains(float x, float y)
        {
            bool xInRange = (this.Min.X <= x) && (x <= this.Max.X);
            bool yInRange = (this.Min.Y <= y) && (y <= this.Max.Y);

            return (xInRange && yInRange);
        }

        /// <summary>
        /// Determines if the 3D point exists within the bounding box on the x- and y-axes.
        /// If checkZ is set it will also check the z axes.
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public bool Contains(Vector3f point, bool checkZ = false)
        {
            bool xInRange = (this.Min.X <= point.X) && (point.X <= this.Max.X);
            bool yInRange = (this.Min.Y <= point.Y) && (point.Y <= this.Max.Y);

            bool zInRange = true;
            if(checkZ)
                zInRange = (this.Min.Z <= point.Z) && (point.Z <= this.Max.Z);

            return (xInRange && yInRange && zInRange);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="box"></param>
        /// <returns></returns>
        public bool Intersects(BoundingBox3f box)
        {
            if (this.Min.X > box.Max.X || this.Max.X < box.Min.X ||
                this.Min.Y > box.Max.Y || this.Max.Y < box.Min.Y ||
                this.Min.Z > box.Max.Z || this.Max.Z < box.Min.Z)
                return (false);

            return (true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public float Volume()
        {
            float width = (this.Max.X - this.Min.X);
            float depth = (this.Max.Y - this.Min.Y);
            float height = (this.Max.Z - this.Min.Z);
            return (width * depth * height);
        }

        /// <summary>
        /// Transform this box by a transformation matrix.
        /// </summary>
        /// <param name="m"></param>
        public void Transform(Matrix34f m)
        {
            Vector3f centre = this.Centre();
            Vector3f size = (this.Max - this.Min) / 2.0f;
            Matrix33f rot = new Matrix33f(m); // rotation component
            rot.Abs();

            m.Transform(centre);
            rot.Inverse().Transform(size);

            this.Min = centre - size;
            this.Max = centre + size;
        }

        /// <summary>
        /// Returns array of the eight points
        /// </summary>
        /// <returns></returns>
        public Vector3f[] GetPoints()
        {
            return new Vector3f[] { new Vector3f(Max), new Vector3f(Max.X, Max.Y, Min.Z), new Vector3f(Max.X, Min.Y, Min.Z), new Vector3f(Max.X, Min.Y, Max.Z),
                                    new Vector3f(Min), new Vector3f(Min.X, Max.Y, Min.Z), new Vector3f(Min.X, Max.Y, Max.Z), new Vector3f(Min.X, Min.Y, Max.Z)};
        }
        #endregion // Controller Methods
    }

} // RSG.Base.Math namespace

// BoundingBox3.cs
