﻿//
// File: BoundingBox3.cs
// Author: Travis Primm <travis.primm@rockstarsandiego.com>
// Description: Implementation of OBoundingBox3 classes
//

using System;
using System.Diagnostics;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace RSG.Base.Math
{
    [DataContract]
    public class OBoundingBox3f
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Center of bound.
        /// </summary>
        public Vector3f Center
        {
            get { return m_Orientation.D; }
            set
            {
                if (m_Orientation.D != value)
                {
                    m_Orientation.D = value;
                    this.m_bIsEmpty = false;
                }
            }
        }

        /// <summary>
        /// This is the transformation matrix for the box. The center should be stored in the D vector
        /// </summary>
        [XmlElement("transformation")]
        [DataMember]
        public Matrix34f Orientation
        {
            get { return m_Orientation; }
            set
            {
                if (m_Orientation != value)
                {
                    m_Orientation = value;
                    this.m_bIsEmpty = false;
                }
            }
        }
        private Matrix34f m_Orientation;

        /// <summary>
        /// Distance from the center to the X+ edge in a local orientation and the same for Y+ and Z+
        /// </summary>
        [XmlElement("extents")]
        [DataMember]
        public Vector3f Extents
        {
            get { return m_vExtents; }
            set
            {
                if (m_vExtents != value)
                {
                    m_vExtents = value;
                    this.m_bIsEmpty = false;
                }
            }
        }
        private Vector3f m_vExtents;

        /// <summary>
        /// 
        /// </summary>
        public bool IsEmpty
        {
            get { return m_bIsEmpty; }
        }
        private bool m_bIsEmpty;
        #endregion // Properties and Associated Member Data

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public OBoundingBox3f()
        {
            throw new NotImplementedException();
            //this.Orientation = Matrix34f.Identity;
            //this.Extents = new Vector3f(float.MinValue, float.MinValue, float.MinValue);
            //this.m_bIsEmpty = true;
        }

        /// <summary>
        /// 
        /// </summary>
        public OBoundingBox3f(BoundingBox3f localBounds, Matrix34f transformation)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Returns an unordered list of points for the 8 corners
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Vector3f> GetOrientedPoints()
        {
            throw new NotImplementedException();
            //List<Vector3f> points = new List<Vector3f>();

            //// Get local points
            //// x+, y+, z+
            //points.Add(new Vector3f(Center.X + Extents.X, Center.Y + Extents.Y, Center.Z + Extents.Z));
            //points.Add(new Vector3f(Center.X + Extents.X, Center.Y + Extents.Y, Center.Z - Extents.Z));
            //points.Add(new Vector3f(Center.X + Extents.X, Center.Y - Extents.Y, Center.Z + Extents.Z));
            //points.Add(new Vector3f(Center.X + Extents.X, Center.Y - Extents.Y, Center.Z - Extents.Z));
            //points.Add(new Vector3f(Center.X - Extents.X, Center.Y + Extents.Y, Center.Z + Extents.Z));
            //points.Add(new Vector3f(Center.X - Extents.X, Center.Y + Extents.Y, Center.Z - Extents.Z));
            //points.Add(new Vector3f(Center.X - Extents.X, Center.Y - Extents.Y, Center.Z + Extents.Z));
            //points.Add(new Vector3f(Center.X - Extents.X, Center.Y - Extents.Y, Center.Z - Extents.Z));

            //// Orient the local points about the rotation matrix. 
            //for (int index = 0; index < points.Count; index++)
            //{
            //    points[index] = Orientation * points[index];
            //}

            //return points;
        }

        #endregion // Controller Methods
    }
}
