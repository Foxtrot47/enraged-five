//
// File: BoundingSphere.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of BoundingSphere class
//

using System;
using SMath = System.Math;

namespace RSG.Base.Math
{

    /// <summary>
    /// 
    /// </summary>
    public class BoundingSpheref
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Centre point of bounding sphere.
        /// </summary>
        public Vector3f Centre
        {
            get { return m_vCentre; }
            set { m_vCentre = value; }
        }
        private Vector3f m_vCentre;

        /// <summary>
        /// Radius of bounding sphere.
        /// </summary>
        public float Radius
        {
            get { return m_fRadius; }
            set { m_fRadius = value; }
        }
        private float m_fRadius;
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="centre">Centre point of sphere</param>
        /// <param name="radius">Radius of sphere</param>
        public BoundingSpheref(Vector3f centre, float radius)
        {
            this.Centre = centre;
            this.Radius = radius;
        }

        /// <summary>
        /// Standard constructor. Zero initialised members.
        /// </summary>
        public BoundingSpheref()
        {
            this.Centre = new Vector3f();
            this.Radius = 0;
        }

        /// <summary>
        /// Constructor, from BoundingBox.
        /// </summary>
        /// <param name="bbox"></param>
        public BoundingSpheref(BoundingBox3f bbox)
        {
            float distx = (bbox.Max.X - bbox.Min.X) / 2.0f;
            float disty = (bbox.Max.Y - bbox.Min.Y) / 2.0f;
            float distz = (bbox.Max.Z - bbox.Min.Z) / 2.0f;
            double dista = SMath.Sqrt((double)(distx * distx) + (double)(disty * disty));

            this.Centre = new Vector3f(bbox.Min.X + distx, bbox.Min.Y + disty, bbox.Min.Z + distz);
            this.Radius = (float)SMath.Sqrt((double)(dista * dista) + (double)(distz * distz));
        }
        #endregion // Constructor(s)
    }

} // RSG.Base.Math namespace

// BoundingSphere.cs
