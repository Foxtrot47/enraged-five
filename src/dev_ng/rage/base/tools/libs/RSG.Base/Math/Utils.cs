using System;
using System.Collections.Generic;
using System.Text;

namespace RSG.Base.Math
{

    /// <summary>
    /// 
    /// </summary>
    public static class Utils
    {
        /// <summary>
        /// Return the 2x2 matrix determinant.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <param name="d"></param>
        /// <returns></returns>
        internal static float Det22(float a, float b, float c, float d)
        {
            return ((a * d) - (b * c));
        }

        /// <summary>
        /// Return the 3x3 matrix determinant.
        /// </summary>
        /// <param name="ax"></param>
        /// <param name="ay"></param>
        /// <param name="az"></param>
        /// <param name="bx"></param>
        /// <param name="by"></param>
        /// <param name="bz"></param>
        /// <param name="cx"></param>
        /// <param name="cy"></param>
        /// <param name="cz"></param>
        /// <returns></returns>
        internal static float Det33(float ax, float ay, float az, float bx, float by, float bz, float cx, float cy, float cz)
        {
            return ((ax * by * cz) + (ay * bz * cx) + (az * bx * cy) - (ax * bz * cy) - (ay * bx * cz) - (az * by * cx));
        }

        /// <summary>
        /// Return the maximum value in arbitary Array of values.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="args"></param>
        /// <returns></returns>
        public static T Maximum<T>(params T[] args) where T : IComparable
        {
            T maximum = args[0];
            foreach (T a in args)
            {
                if (a.CompareTo(maximum) > 0)
                    maximum = a;
            }
            return (maximum);
        }

        /// <summary>
        /// Return the minimum value in an arbitary Array of values.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="args"></param>
        /// <returns></returns>
        public static T Minimum<T>(params T[] args) where T : IComparable
        {
            T minimum = args[0];
            foreach (T a in args)
            {
                if (a.CompareTo(minimum) < 0)
                    minimum = a;
            }
            return (minimum);
        }

        /// <summary>
        /// Swaps the variables.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="lhs"></param>
        /// <param name="rhs"></param>
        public static void Swap<T>(ref T lhs, ref T rhs)
        {
            T temp;
            temp = lhs;
            lhs = rhs;
            rhs = temp;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <param name="minThreshold"></param>
        /// <param name="maxThreshold"></param>
        /// <returns></returns>
        public static float RoundNearest(float input, float minThreshold = 0.001f, float maxThreshold = 0.999f)
        {
            float dec = System.Math.Abs(input - (int)input);
            if (dec < minThreshold || dec > maxThreshold)
            {
                return (float)System.Math.Round(input);
            }

            return (float)input;
        }

        /// <summary>
        /// Get the smallest scalar projection value from the oriented points along any given axis
        /// </summary>
        /// <param name="points"></param>
        /// <param name="axis"></param>
        /// <returns></returns>
        public static float MinProjectedAxisValue(IEnumerable<Vector2f> points, Vector2f axis)
        {
            float minValue = float.MaxValue;
            foreach (Vector2f point in points)
            {
                float scalarProjection = Vector2f.Dot(point, axis);
                if (scalarProjection < minValue)
                {
                    minValue = scalarProjection;
                }
            }
            return minValue;
        }

        /// <summary>
        /// Get the largest scalar projection value from the oriented points along any given axis
        /// </summary>
        /// <param name="points"></param>
        /// <param name="axis"></param>
        /// <returns></returns>
        public static float MaxProjectedAxisValue(IEnumerable<Vector2f> points, Vector2f axis)
        {
            float maxValue = float.MinValue;
            foreach (Vector2f point in points)
            {
                float scalarProjection = Vector2f.Dot(point, axis);
                if (scalarProjection > maxValue)
                {
                    maxValue = scalarProjection;
                }
            }
            return maxValue;
        }
    }

} // RSG.Base.Math
