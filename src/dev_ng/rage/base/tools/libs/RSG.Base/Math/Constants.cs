//
// File: Constants.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of Constants class
//

using System;
using SMath = System.Math;

namespace RSG.Base.Math
{

    /// <summary>
    /// 
    /// </summary>
    public static class Constants
    {
        #region Constants
        /// <summary>
        /// 
        /// </summary>
        public static readonly double M_EPSILON = (0.001);

        /// <summary>
        /// Square-root of 2.
        /// </summary>
        public static readonly double M_SQRT2 = (1.4142135623730950488016887242097);

        /// <summary>
        /// Square-root of 1/2.
        /// </summary>
        public static readonly double M_SQRT1_2 = (0.70710678118654752440084436210485);

        /// <summary>
        /// Square-root of 3.
        /// </summary>
        public static readonly double M_SQRT3 = (1.7320508075688772935274463415059);
        #endregion // Constants
    }

} // RSG.Base.Math namespace

// Constants.cs
