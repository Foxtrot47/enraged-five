﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Math
{
    /// <summary>
    /// Represents a 4x4 floating point matrix.
    /// </summary>
    public class Matrix44f
    {
        #region Constants
        /// <summary>
        /// Zero matrix.
        /// </summary>
        public static readonly Matrix44f Zero =
            new Matrix44f();

        /// <summary>
        /// Identity matrix.
        /// </summary>
        public static readonly Matrix44f Identity =
            new Matrix44f(
                new Vector4f(1.0f, 0.0f, 0.0f, 0.0f),
                new Vector4f(0.0f, 1.0f, 0.0f, 0.0f),
                new Vector4f(0.0f, 0.0f, 1.0f, 0.0f),
                new Vector4f(0.0f, 0.0f, 0.0f, 1.0f));
        #endregion

        #region Fields
        /// <summary>
        /// Private field for the <see cref="A"/> property.
        /// </summary>
        private Vector4f _a;

        /// <summary>
        /// Private field for the <see cref="B"/> property.
        /// </summary>
        private Vector4f _b;

        /// <summary>
        /// Private field for the <see cref="C"/> property.
        /// </summary>
        private Vector4f _c;

        /// <summary>
        /// Private field for the <see cref="D"/> property.
        /// </summary>
        private Vector4f _d;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="Matrix44"/> class to the zero
        /// matrix.
        /// </summary>
        public Matrix44f()
            : this(new Vector4f(), new Vector4f(), new Vector4f(), new Vector4f())
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Matrix44"/> class using the
        /// provided vectors as the row.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <param name="d"></param>
        public Matrix44f(Vector4f a, Vector4f b, Vector4f c, Vector4f d)
        {
            _a = a;
            _b = b;
            _c = c;
            _d = d;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Matrix44"/> class using the 
        /// provided array.
        /// </summary>
        /// <param name="data"></param>
        public Matrix44f(float[][] data)
            : this(new Vector4f(data[0]), new Vector4f(data[1]), new Vector4f(data[2]), new Vector4f(data[3]))
        {
        }
        
        /// <summary>
        /// Initialises a new instance of the <see cref="Matrix44"/> class to be a copy
        /// of the provided matrix.
        /// </summary>
        /// <param name="m"></param>
        public Matrix44f(Matrix44f m)
            : this(new Vector4f(m.A), new Vector4f(m.B), new Vector4f(m.C), new Vector4f(m.D))
        {
        }
        #endregion

        #region Properties
        /// <summary>
        /// First row of the matrix.
        /// </summary>
        public Vector4f A
        {
            get { return _a; }
            set { _a = value; }
        }

        /// <summary>
        /// Second row of the matrix.
        /// </summary>
        public Vector4f B
        {
            get { return _b; }
            set { _b = value; }
        }

        /// <summary>
        /// Third row of the matrix.
        /// </summary>
        public Vector4f C
        {
            get { return _c; }
            set { _c = value; }
        }

        /// <summary>
        /// Fourth row of the matrix.
        /// </summary>
        public Vector4f D
        {
            get { return _d; }
            set { _d = value; }
        }
        #endregion

        #region Operators
        /// <summary>
        /// Explicit conversion to a 2d float array.
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public static explicit operator float[][](Matrix44f m)
        {
            return new float[][] { (float[])m.A, (float[])m.B, (float[])m.C, (float[])m.D };
        }
        #endregion
    }
}
