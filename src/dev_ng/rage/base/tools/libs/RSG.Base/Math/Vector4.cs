//
// File: Vector4.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: 4D Vector Classes
//

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Xml;
using System.Xml.XPath;

namespace RSG.Base.Math
{

    #region Vector4 Template Base Class
    /// <summary>
    /// 4D Vector Class
    /// </summary>
    /// Currently common operations etc. are not implemented.
    [Serializable]
    public abstract class Vector4<T> :
        IComparable,
        IComparable<Vector4<T>>
        where T : IComparable
    {
        #region Constants
        protected static readonly String sC_s_s_AttrX = "x";
        protected static readonly String sC_s_s_AttrY = "y";
        protected static readonly String sC_s_s_AttrZ = "z";
        protected static readonly String sC_s_s_AttrW = "w";
        protected static readonly String sC_s_s_AttrR = "r";
        protected static readonly String sC_s_s_AttrG = "g";
        protected static readonly String sC_s_s_AttrB = "b";
        protected static readonly String sC_s_s_AttrA = "a";
        #endregion // Constants

        #region Properties and Associated Member Data
        /// <summary>
        /// Vector Element Type
        /// </summary>
        public Type ElementType
        {
            get { return (typeof(T)); }
        }

        /// <summary>
        /// X-Component of vector
        /// </summary>
        public T X
        {
            get { return m_X; }
            set { m_X = value; }
        }
        private T m_X;

        /// <summary>
        /// Y-Component of vector
        /// </summary>
        public T Y
        {
            get { return m_Y; }
            set { m_Y = value; }
        }
        private T m_Y;

        /// <summary>
        /// Z-Component of vector
        /// </summary>
        public T Z
        {
            get { return m_Z; }
            set { m_Z = value; }
        }
        private T m_Z;

        /// <summary>
        /// W-Component of vector
        /// </summary>
        public T W
        {
            get { return m_W; }
            set { m_W = value; }
        }
        private T m_W;
        #endregion // Properties and Associated Member Data

        #region Constructor / Destructor
        /// <summary>
        /// Default constructor
        /// </summary>
        protected Vector4()
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        protected Vector4(T x, T y, T z, T w)
        {
            X = x;
            Y = y;
            Z = z;
            W = w;
        }

        /// <summary>
        /// Dispose
        /// </summary>
        protected void Dispose()
        {
            X = default(T);
            Y = default(T);
            Z = default(T);
            W = default(T);
        }
        #endregion // Constructor / Destructor

        #region Abstract Methods
        /// <summary>
        /// Calculate and return magnitude of 3D vector
        /// </summary>
        /// <returns>Magnitude (length) of 3D vector</returns>
        public abstract double Magnitude();

        /// <summary>
        /// Equality test.
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public abstract bool Equals(Vector4<T> v);
        #endregion // Abstract Methods

        #region IComparable Interface Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int CompareTo(Object obj)
        {
            if (obj is Vector4<T>)
                return (CompareTo(obj as Vector4<T>));
            throw new ArgumentException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public int CompareTo(Vector4<T> v)
        {
            int x = this.X.CompareTo(v.X);
            int y = this.Y.CompareTo(v.Y);
            int z = this.Z.CompareTo(v.Z);
            int w = this.W.CompareTo(v.W);
            return (x + y + z + w);
        }
        #endregion // IComparable Interface Methods

        #region Object Overridden Methods
        /// <summary>
        /// Convert vector to String
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return (String.Format("( {0}, {1}, {2}, {3} )", this.X, this.Y, this.Z, this.W));
        }
        #endregion // Methods
    }
    #endregion // Vector4 Template Base Class

    #region Vector4<float>
    /// <summary>
    /// Vector3 Specialisation (float)
    /// </summary>
    public class Vector4f : Vector4<float>
    {
        #region XPath Compiled Expressions
        private static readonly XPathExpression sC_s_xpath_XPathAttrs =
            XPathExpression.Compile("@*");
        #endregion // XPath Compiled Expressions

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public Vector4f()
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public Vector4f(float x, float y, float z, float w)
            : base(x, y, z, w)
        {
        }
        
        /// <summary>
        /// Initialises a new instance of the <see cref="Vector4f"/> class using the
        /// provided array.
        /// </summary>
        /// <param name="data"></param>
        public Vector4f(float[] data)
            : base(data[0], data[1], data[2], data[3])
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="copy"></param>
        public Vector4f(Vector4f copy)
            : base(copy.X, copy.Y, copy.Z, copy.W)
        {
        }

        /// <summary>
        /// Constructor, from a XmlNode.
        /// </summary>
        /// <param name="node"></param>
        public Vector4f(XmlNode node)
        {
            Debug.Assert(4 == node.Attributes.Count, "Invalid number of attributes for Vector4f.");
            foreach (XmlAttribute attr in node.Attributes)
            {
                if ((sC_s_s_AttrX == attr.Name) ||
                    (sC_s_s_AttrR == attr.Name))
                    this.X = float.Parse(attr.Value);
                else if ((sC_s_s_AttrY == attr.Name) ||
                    (sC_s_s_AttrG == attr.Name))
                    this.Y = float.Parse(attr.Value);
                else if ((sC_s_s_AttrZ == attr.Name) ||
                    (sC_s_s_AttrB == attr.Name))
                    this.Z = float.Parse(attr.Value);
                else if ((sC_s_s_AttrW == attr.Name) ||
                    (sC_s_s_AttrA == attr.Name))
                    this.W = float.Parse(attr.Value);
            }
        }
   
        /// <summary>
        /// Constructor, from a XPathNavigator.
        /// </summary>
        /// <param name="navigator"></param>
        public Vector4f(XPathNavigator navigator)
        {
            XPathNodeIterator it = navigator.Select(sC_s_xpath_XPathAttrs);
            while (it.MoveNext())
            {
                if ((sC_s_s_AttrX == it.Current.Name) ||
                    (sC_s_s_AttrR == it.Current.Name))
                    this.X = float.Parse(it.Current.Value);
                else if ((sC_s_s_AttrY == it.Current.Name) ||
                    (sC_s_s_AttrG == it.Current.Name))
                    this.Y = float.Parse(it.Current.Value);
                else if ((sC_s_s_AttrZ == it.Current.Name) ||
                    (sC_s_s_AttrB == it.Current.Name))
                    this.Z = float.Parse(it.Current.Value);
                else if ((sC_s_s_AttrW == it.Current.Name) ||
                    (sC_s_s_AttrA == it.Current.Name))
                    this.W = float.Parse(it.Current.Value);
            }
        }
        #endregion // Constructors

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override double Magnitude()
        {
            return (System.Math.Sqrt((double)((this.X * this.X) + (this.Y * this.Y) + (this.Z * this.Z))));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Vector4i ToVector4i()
        {
            return new Vector4i((int)X, (int)Y, (int)Z, (int)W);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public override bool Equals(Vector4<float> v)
        {
            // If parameter is null return false:
            if (v == null)
                return false;

            return (this.X == v.X && this.Y == v.Y && this.Z == v.Z && this.W == v.W);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public override bool Equals(Object v)
        {
            // If parameter is null return false:
            if (v == null || !(v is Vector4f))
                return false;

            return this.Equals((Vector4f)v);
        }

        /// <summary>
        /// GetHashCode override.
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        #endregion // Public Methods

        #region Static Operators
        /// <summary>
        /// Equality operator
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool operator ==(Vector4f v1, Vector4f v2)
        {
            // If both are null, or both are same instance, return true.
            if (Object.ReferenceEquals(v1, v2))
                return true;

            // If one is null, but not both, return false.
            if (((Object)v1 == null) || ((Object)v2 == null))
                return false;

            // Return true if the fields match:
            return v1.X == v2.X && v1.Y == v2.Y && v1.Z == v2.Z && v1.W == v2.W;
        }

        /// <summary>
        /// Inequality operator
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool operator !=(Vector4f v1, Vector4f v2)
        {
            return !(v1 == v2);
        }

        /// <summary>
        /// Addition operator
        /// </summary>
        public static Vector4f operator +(Vector4f v1, Vector4f v2)
        {
            return (new Vector4f(v1.X + v2.X, v1.Y + v2.Y, v1.Z + v2.Z, v1.W + v2.W));
        }

        /// <summary>
        /// Subtraction Operator
        /// </summary>
        public static Vector4f operator -(Vector4f v1, Vector4f v2)
        {
            return (new Vector4f(v1.X - v2.X, v1.Y - v2.Y, v1.Z - v2.Z, v1.W - v2.W));
        }

        /// <summary>
        /// Scalar Multiplication Operator
        /// </summary>
        public static Vector4f operator *(Vector4f v, float f)
        {
            return (new Vector4f(v.X * f, v.Y * f, v.Z * f, v.W * f));
        }

        /// <summary>
        /// Explicit conversion to a float array.
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static explicit operator float[](Vector4f v)
        {
            return new float[] { v.X, v.Y, v.Z, v.W };
        }
        #endregion // Static Operators
    };
    #endregion // cVector2<float>

    #region Vector4<int>
    /// <summary>
    /// Vector3 Specialisation (int)
    /// </summary>
    public class Vector4i : Vector4<int>
    {
        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public Vector4i()
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public Vector4i(int x, int y, int z, int w)
            : base(x, y, z, w)
        {
        }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="v"></param>
        public Vector4i(Vector4i v)
            : base(v.X, v.Y, v.Z, v.W)
        {
        }

        /// <summary>
        /// Constructor, from a XmlNode.
        /// </summary>
        /// <param name="node"></param>
        public Vector4i(XmlNode node)
        {
            Debug.Assert(4 == node.Attributes.Count, "Invalid number of attributes for Vector4i.");
            foreach (XmlAttribute attr in node.Attributes)
            {
                if ((sC_s_s_AttrX == attr.Name) ||
                    (sC_s_s_AttrR == attr.Name))
                    this.X = int.Parse(attr.Value);
                else if ((sC_s_s_AttrY == attr.Name) ||
                    (sC_s_s_AttrG == attr.Name))
                    this.Y = int.Parse(attr.Value);
                else if ((sC_s_s_AttrZ == attr.Name) ||
                    (sC_s_s_AttrB == attr.Name))
                    this.Z = int.Parse(attr.Value);
                else if ((sC_s_s_AttrW == attr.Name) ||
                    (sC_s_s_AttrA == attr.Name))
                    this.W = int.Parse(attr.Value);
            }
        }
        #endregion // Constructors

        #region Public Methods
        public override double Magnitude()
        {
            return (System.Math.Sqrt((double)((this.X * this.X) + (this.Y * this.Y) + (this.Z * this.Z))));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public override bool Equals(Vector4<int> v)
        {
            // If parameter is null return false:
            if (v == null)
                return false;

            return (this.X == v.X && this.Y == v.Y && this.Z == v.Z && this.W == v.W);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public override bool Equals(Object v)
        {
            // If parameter is null return false:
            if (v == null || !(v is Vector4i))
                return false;

            return this.Equals((Vector4f)v);
        }

        /// <summary>
        /// GetHashCode override.
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        #endregion // Public Methods

        #region Static Operators
        /// <summary>
        /// Equality operator
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool operator ==(Vector4i v1, Vector4i v2)
        {
            // If both are null, or both are same instance, return true.
            if (Object.ReferenceEquals(v1, v2))
                return true;

            // If one is null, but not both, return false.
            if (((Object)v1 == null) || ((Object)v2 == null))
                return false;

            // Return true if the fields match:
            return v1.X == v2.X && v1.Y == v2.Y && v1.Z == v2.Z && v1.W == v2.W;
        }

        /// <summary>
        /// Inequality operator
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool operator !=(Vector4i v1, Vector4i v2)
        {
            return !(v1 == v2);
        }

        /// <summary>
        /// Addition operator
        /// </summary>
        public static Vector4i operator +(Vector4i v1, Vector4i v2)
        {
            return (new Vector4i(v1.X + v2.X, v1.Y + v2.Y, v1.Z + v2.Z, v1.W + v2.W));
        }

        /// <summary>
        /// Subtraction Operator
        /// </summary>
        public static Vector4i operator -(Vector4i v1, Vector4i v2)
        {
            return (new Vector4i(v1.X - v2.X, v1.Y - v2.Y, v1.Z - v2.Z, v1.W - v2.W));
        }

        /// <summary>
        /// Scalar Multiplication Operator
        /// </summary>
        public static Vector4i operator *(Vector4i v, int f)
        {
            return (new Vector4i(v.X * f, v.Y * f, v.Z * f, v.W * f));
        }
        #endregion // Static Operators
    };
    #endregion // cVector2<float>

} // End of RSG.Base.Math namespace

// End of file
