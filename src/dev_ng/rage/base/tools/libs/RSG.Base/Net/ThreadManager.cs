﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net;
using System.IO;

namespace RSG.Base.Net
{
    /// <summary>
    /// Thread manager to allow for multiple connections to the web service.
    /// </summary>
    internal class ThreadManager
    {
        #region Public methods

        /// <summary>
        /// Add a user's request to be processed.
        /// </summary>
        /// <param name="context">Http listener context.</param>
        public void AddRequest(HttpListenerContext context)
        {
            ThreadPool.QueueUserWorkItem(new WaitCallback(ProcessRequest), context);
        }

        #endregion

        #region Private helper methods

        /// <summary>
        /// Process the request.
        /// </summary>
        /// <param name="obj">Http listener context.</param>
        private void ProcessRequest(object obj)
        {
            var context = obj as HttpListenerContext;

            try
            {
                String dir = WebService.WebRootDir;
                String filename = context.Request.Url.AbsolutePath;

                filename = filename.Substring(1);
                if (String.IsNullOrEmpty(filename))
                    filename = "index.html";
                filename = Path.GetFullPath(Path.Combine(dir, filename));

                WebService.Log.MessageCtx(WebService.LOG_CTX, "Request: {0}", filename);

                if (File.Exists(filename))
                {
                    // Content type needs to be set before writing to the output stream
                    string extension = Path.GetExtension(filename).TrimStart(new char[] { '.' });
                    if (WebService.ContentTypeMap.ContainsKey(extension))
                    {
                        context.Response.ContentType = WebService.ContentTypeMap[extension];
                    }
                    else
                    {
                        WebService.Log.WarningCtx(WebService.LOG_CTX, "Unknown file extension type encounter ({0}).  Setting response content type to the default ({1}).", extension, WebService.DEFAULT_MIME_TYPE);
                        context.Response.ContentType = WebService.DEFAULT_MIME_TYPE;
                    }

                    using (Stream input = new FileStream(filename, FileMode.Open, FileAccess.Read))
                    {
                        input.CopyTo(context.Response.OutputStream);
                    }
                }
                else
                {
                    WebService.Log.ErrorCtx(WebService.LOG_CTX, "File not found: {0}", filename);
                    context.Response.StatusCode = 404;
                }
            }
            catch (System.Exception ex)
            {
                WebService.Log.ToolExceptionCtx(WebService.LOG_CTX, ex, "Request handler exception: Set response.");
                context.Response.StatusCode = 500;
            }
            finally
            {
                // Wrap this in try/catch as the other side might have closed the connection before we have closed our response,
                // resulting in an exception being thrown.
                try
                {
                    context.Response.OutputStream.Close();
                    context.Response.Close();
                }
                catch (System.Exception ex)
                {
                    WebService.Log.ToolExceptionCtx(WebService.LOG_CTX, ex, "Request handler exception: Close stream/response.");
                }
            }
        }

        #endregion
    }
}
