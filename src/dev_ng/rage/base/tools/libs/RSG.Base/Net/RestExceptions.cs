﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Net
{
    /// <summary>
    /// Base exception for REST queries.
    /// </summary>
    public abstract class RestException : System.Exception
    {
        internal RestException()
            : base("")
        {
        }

        internal RestException(string message)
            : base(message)
        {
        }

        internal RestException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    } // RestException

    /// <summary>
    /// Exception: Server/Service is unavailable
    /// </summary>
    public class ServerUnavailableException : RestException
    {
        internal ServerUnavailableException(string message)
            : base(message)
        {
        }
    } // ServerUnavailableException

    /// <summary>
    /// Exception: Invalid login credentials.
    /// </summary>
    public class InvalidLoginException : RestException
    {
        internal InvalidLoginException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Gets the error message for the exception.
        /// </summary>
        public override string Message
        {
            get
            {
                return String.Format("Invalid Login!\n{0}", base.Message);
            }
        }
    } // InvalidLoginException

    /// <summary>
    /// Exception: Error running REST command!
    /// Indicates that one of the parameters was wrong
    /// </summary>
    public class BadRequestException : RestException
    {
        internal BadRequestException(Uri query)
            : base(query.ToString())
        {
        }

        internal BadRequestException(Uri query, Exception innerException)
            : base(query.ToString(), innerException)
        {
        }

        internal BadRequestException(string message)
            : base(message)
        {
        }

        internal BadRequestException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Gets the error message for the exception.
        /// </summary>
        public override string Message
        {
            get
            {
                return String.Format("Error running rest command!\n{0}", base.Message);
            }
        }
    } // BadRequestException

    /// <summary>
    /// Exception: Error running REST command
    /// The URL requested was not found
    /// </summary>
    public class NotFoundException : RestException
    {
        internal NotFoundException(Uri query)
            : base(query.ToString())
        {
        }

        internal NotFoundException(Uri query, Exception innerException)
            : base(query.ToString(), innerException)
        {
        }

        internal NotFoundException(string message)
            : base(message)
        {
        }

        internal NotFoundException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Gets the error message for the exception.
        /// </summary>
        public override string Message
        {
            get
            {
                return String.Format("Error running rest command!\n{0}", base.Message);
            }
        }
    }

    /// <summary>
    /// Exception: Error running REST command
    /// The method was not supported (e.g. POST used instead of PUT)
    /// </summary>
    public class MethodNotAllowedException : RestException
    {
        internal MethodNotAllowedException(Uri query)
            : base(query.ToString())
        {
        }

        internal MethodNotAllowedException(Uri query, Exception innerException)
            : base(query.ToString(), innerException)
        {
        }

        internal MethodNotAllowedException(string message)
            : base(message)
        {
        }

        internal MethodNotAllowedException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Gets the error message for the exception.
        /// </summary>
        public override string Message
        {
            get
            {
                return String.Format("Error running rest command!\n{0}", base.Message);
            }
        }
    }

    /// <summary>
    /// Exception: Error running REST command
    /// An internal server exception occurred while processing the command
    /// </summary>
    public class InternalServerException : RestException
    {
        internal InternalServerException(Uri query)
            : base(query.ToString())
        {
        }

        internal InternalServerException(Uri query, Exception innerException)
            : base(query.ToString(), innerException)
        {
        }

        internal InternalServerException(string message)
            : base(message)
        {
        }

        internal InternalServerException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Gets the error message for the exception.
        /// </summary>
        public override string Message
        {
            get
            {
                return String.Format("Error running rest command!\n{0}", base.Message);
            }
        }
    }
} // RSG.Base.Net
