﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Net
{
    /// <summary>
    /// REST query interface.
    /// </summary>
    public interface IRestQuery
    {
        /// <summary>
        /// Uri for REST query.
        /// </summary>
        Uri QueryUri { get; }

        /// <summary>
        /// REST HTTP method.
        /// </summary>
        RequestMethod Method { get; }

        /// <summary>
        /// Content-type for PUT/POST data.
        /// </summary>
        String ContentType { get; }

        /// <summary>
        /// REST POST/PUT data.
        /// </summary>
        String Data { get; }
    }

} // RSG.Base.Net namespace
