﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Net.NetworkInformation;
using RSG.Base.Logging;

namespace RSG.Base.Net
{
    /// <summary>
    /// Abstract base class for a rest connection
    /// </summary>
    public abstract class RestConnection
    {
        #region Properties
        /// <summary>
        /// Flag indicating whether the rest connection requires you to login
        /// </summary>
        protected virtual bool LoginRequired
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// The login credentials that are to be used with this connection
        /// </summary>
        private string LoginCredentials
        {
            get;
            set;
        }

        /// <summary>
        /// Query to use for performing the initial login
        /// </summary>
        protected virtual IRestQuery LoginQuery
        {
            get
            {
                return null;
            }
        }
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Static constructor for setting the certificate validation callback
        /// </summary>
        static RestConnection()
        {
#if DEBUG
            // The following is to allow us to use untrusted https connections (which is the case if you're running a local bugstar rest service).
            // It solves the following exception from being thrown:
            // System.Net.WebException: The underlying connection was closed: Could not establish trust relationship for the SSL/TLS secure channel.
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
#endif
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Indicates whether the connection is valid
        /// </summary>
        /// <returns></returns>
        public virtual bool IsValidConnection()
        {
            return (!LoginRequired || !String.IsNullOrEmpty(LoginCredentials));
        }

        /// <summary>
        /// Attempts to connect to bugstar with the supplied username/password
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="useRockstarDomain"></param>
        public virtual void Login(string username, string password, string domain)
        {
            IRestQuery query = LoginQuery;

            // Check that the server is up and running
            if (!IsServerAvailable(query))
            {
                //throw new ServerUnavailableException("Unable to reach the server located at: " + query.QueryUri.Host);
                Log.Log__Error("Unable to reach the server located at: " + query.QueryUri.Host);
            }
            if (!LoginRequired)
            {
                throw new InvalidLoginException("The rest connection doesn't support logging in.");
            }

            try
            {
                EnableCorrectSecurityProtocols();

                // Construct the username/password
                string usernamePassword;
                if (String.IsNullOrEmpty(domain))
                {
                    usernamePassword = String.Format("{0}:{1}", username, password);
                }
                else
                {
                    usernamePassword = String.Format("{0}@{1}:{2}", username, domain, password);
                }
                string encodedUsernamePassword = Convert.ToBase64String(new ASCIIEncoding().GetBytes(usernamePassword));

                // Generate the query
                HttpWebRequest http = (HttpWebRequest)HttpWebRequest.Create(query.QueryUri);
                http.Headers.Add("Authorization", "Basic " + encodedUsernamePassword);
                http.Method = query.Method.ToString();

#if DEBUG
                Log.Log__Debug("REST request: {0}", http.RequestUri);
#endif

                // We assume that the connection was successful if no exceptions are thrown
                using (HttpWebResponse response = (HttpWebResponse)http.GetResponse())
                {
                }

                LoginCredentials = encodedUsernamePassword;
            }
            catch (System.Net.WebException ex)
            {
                if (ex.Message.Contains("(400) Bad Request"))
                {
                    throw new BadRequestException("Bad Request");
                }
                else if (ex.Message.Contains("(401) Unauthorized"))
                {
                    throw new InvalidLoginException("The rest login details provided were not valid.");
                }
                else if (ex.Message.Contains("(404) Not Found"))
                {
                    throw new NotFoundException("404 web exception occurred while processing bugstar command.", ex);
                }
                else if (ex.Message.Contains("(405) Method Not Allowed"))
                {
                    throw new MethodNotAllowedException("405 web exception occurred while processing bugstar command.", ex);
                }
                else if (ex.Message.Contains("(500) Internal Server Error"))
                {
                    throw new InternalServerException("Internal server exception occurred while processing bugstar command.", ex);
                }
                else
                {
                    throw ex;
                }
            }
        }

        /// <summary>
        /// Sends a query to the bugstar server
        /// Note that it is important to properly dispose of the returned stream as failure to do so could result in
        /// the application running out of connections.
        /// </summary>
        /// <param name="query"></param>
        /// <returns>Stream containing the results from the query</returns>
        public virtual Stream RunQuery(IRestQuery query)
        {
            // Check that the server is up and running
            if (!IsServerAvailable(query))
            {
                //throw new ServerUnavailableException("Unable to reach the server located at: " + query.QueryUri.Host);
                Log.Log__Error("Unable to reach the server located at: " + query.QueryUri.Host);
            }
            if (LoginCredentials == null && LoginRequired)
            {
                throw new InvalidLoginException("Not logged into bugstar.");
            }

            // Generate the URI from the query
            Uri uri = query.QueryUri;
            Log.Log__Message("Executing rest query: {0}.", uri);

            try
            {
                EnableCorrectSecurityProtocols();

                HttpWebRequest http = (HttpWebRequest)HttpWebRequest.Create(uri);
                http.Headers.Add("Authorization", "Basic " + LoginCredentials);
                http.Method = query.Method.ToString();

                if ((query.Method == RequestMethod.POST || query.Method == RequestMethod.PUT) &&
                    (query.Data != null))
                {
                    byte[] postData = Encoding.UTF8.GetBytes(query.Data);
                    http.ContentType = query.ContentType;
                    http.ContentLength = postData.Length;

                    Stream dataStream = http.GetRequestStream();
                    dataStream.Write(postData, 0, postData.Length);
                    dataStream.Close();
                }

                HttpWebResponse response = (HttpWebResponse)http.GetResponse();
                return response.GetResponseStream();
            }
            catch (System.Net.WebException ex)
            {
                if (ex.Message.Contains("(400) Bad Request"))
                {
                    throw new BadRequestException("Bad Request");
                }
                else if (ex.Message.Contains("(401) Unauthorized"))
                {
                    throw new InvalidLoginException("The rest login details provided were not valid.");
                }
                else if (ex.Message.Contains("(404) Not Found"))
                {
                    throw new NotFoundException("404 web exception occurred while processing bugstar command.", ex);
                }
                else if (ex.Message.Contains("(405) Method Not Allowed"))
                {
                    throw new MethodNotAllowedException("405 web exception occurred while processing bugstar command.", ex);
                }
                else if (ex.Message.Contains("(500) Internal Server Error"))
                {
                    throw new InternalServerException("Internal server exception occurred while processing bugstar command.", ex);
                }
                else
                {
                    throw ex;
                }
            }
        }
        #endregion // Public Methods

        #region Private Methods
        /// <summary>
        /// Pings bugstar to see if we should be able to run rest queries on it
        /// </summary>
        /// <returns></returns>
        public bool IsServerAvailable(IRestQuery query)
        {
            try
            {
                Ping pingSender = new Ping();
                PingOptions options = new PingOptions(64, false);

                // Create a buffer of 32 bytes of data to be transmitted.
                byte[] buffer = Encoding.ASCII.GetBytes("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
                int timeout = 1000;
                PingReply reply = pingSender.Send(query.QueryUri.Host, timeout, buffer, options);
                return (reply.Status == IPStatus.Success);
            }
            catch (System.Exception)
            {
                return false;
            }
        }

        private void EnableCorrectSecurityProtocols()
        {
            // Make sure to enable the newer Security Protocols, but leave existing configuration intact. This merely enables these protocols.
            // TH: Since this is targetting an older target framework, we had to use the values instead of the enum values:
            // 768: Tls11 and 3072: Tls12
            ServicePointManager.SecurityProtocol |=
                (SecurityProtocolType)768 | (SecurityProtocolType)3072 | SecurityProtocolType.Ssl3;
        }
        #endregion // Private Methods
    }

} // RSG.Base.Net namespace
