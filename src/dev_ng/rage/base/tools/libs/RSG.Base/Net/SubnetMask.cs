﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using RSG.Base.Extensions;

namespace RSG.Base.Net
{

    /// <summary>
    /// 
    /// </summary>
    /// References:
    /// http://blogs.msdn.com/b/knom/archive/2008/12/31/ip-address-calculations-with-c-subnetmasks-networks.aspx
    /// http://en.wikipedia.org/wiki/Subnetwork
    /// http://stackoverflow.com/questions/1499269/how-to-check-if-an-ip-address-is-within-a-particular-subnet
    /// 
    public static class SubnetMask
    {
        public static readonly IPAddress ClassA = IPAddress.Parse("255.0.0.0");
        public static readonly IPAddress ClassB = IPAddress.Parse("255.255.0.0");
        public static readonly IPAddress ClassC = IPAddress.Parse("255.255.255.0");
        
        /// <summary>
        /// Create from CIDR notation (subnet/mask).
        /// </summary>
        /// <param name="cidrNotation"></param>
        /// <returns></returns>
        public static KeyValuePair<IPAddress, IPAddress> Create(String cidrNotation)
        {
            String[] cidrSplit = cidrNotation.Split(new Char[]{'/'});
            Debug.Assert(2 == cidrSplit.Length);
            if (2 != cidrSplit.Length)
                throw new ArgumentException("Invalid CIDR string notation.");
            IPAddress subnet;
            if (!IPAddress.TryParse(cidrSplit[0], out subnet))
                throw new ArgumentException("Invalid CIDR string notation.");
            int mask = 1;
            if (!int.TryParse(cidrSplit[1], out mask))
                throw new ArgumentException("Invalid CIDR string notation.");

            IPAddress subnetMask = SubnetMask.CreateByNetBitLength(mask);
            return (new KeyValuePair<IPAddress, IPAddress>(subnet, subnetMask));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hostpartLength"></param>
        /// <returns></returns>
        public static IPAddress CreateByHostBitLength(int hostpartLength)
        {
            int hostPartLength = hostpartLength;
            int netPartLength = 32 - hostPartLength;

            if (netPartLength < 2)
                throw new ArgumentException("Number of hosts is to large for IPv4");

            Byte[] binaryMask = new byte[4];

            for (int i = 0; i < 4; i++)
            {
                if (i * 8 + 8 <= netPartLength)
                    binaryMask[i] = (byte)255;
                else if (i * 8 > netPartLength)
                    binaryMask[i] = (byte)0;
                else
                {
                    int oneLength = netPartLength - i * 8;
                    string binaryDigit =
                        String.Empty.PadLeft(oneLength, '1').PadRight(8, '0');
                    binaryMask[i] = Convert.ToByte(binaryDigit, 2);
                }
            }
            return new IPAddress(binaryMask);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="netpartLength"></param>
        /// <returns></returns>
        public static IPAddress CreateByNetBitLength(int netpartLength)
        {
            int hostPartLength = 32 - netpartLength;
            return CreateByHostBitLength(hostPartLength);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="numberOfHosts"></param>
        /// <returns></returns>
        public static IPAddress CreateByHostNumber(int numberOfHosts)
        {
            int maxNumber = numberOfHosts + 1;

            string b = Convert.ToString(maxNumber, 2);

            return CreateByHostBitLength(b.Length);
        }
    }

} // RSG.Base.Net namespace
