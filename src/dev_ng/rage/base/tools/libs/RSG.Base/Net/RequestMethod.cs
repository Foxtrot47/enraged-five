﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Net
{
    /// <summary>
    /// Method to send the query's parameters to the bugstar service
    /// </summary>
    public enum RequestMethod
    {
        GET,
        POST,
        PUT,
        DELETE
    }
}
