﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Net
{
    /// <summary>
    /// Abstract base class for a query that will be executed on a rest service
    /// </summary>
    public abstract class RestQuery : IRestQuery
    {
        #region Properties
        /// <summary>
        /// Base url to use for the query
        /// </summary>
        public string QueryString
        {
            get;
            protected set;
        }

        /// <summary>
        /// How the parameters will be sent to the bugstar service
        /// </summary>
        public RequestMethod Method
        {
            get;
            protected set;
        }

        /// <summary>
        /// Content-type for PUT/POST data.
        /// </summary>
        public String ContentType
        {
            get;
            protected set;
        }

        /// <summary>
        /// Dictionary containing any "get/post" parameters
        /// </summary>
        public Dictionary<string, string> Parameters
        {
            get;
            protected set;
        }

        /// <summary>
        /// Uri to use for communicating with bugstar
        /// </summary>
        public Uri QueryUri
        {
            get
            {
                // Generate the uri that should be used
                Uri baseUri;
                if (Service != null)
                {
                    baseUri = new Uri(String.Format("http{0}://{1}:{2}/{3}/{4}", (UseHttps ? "s" : ""), Server, Port, Service, QueryString));
                }
                else
                {
                    baseUri = new Uri(String.Format("http{0}://{1}:{2}/{3}", (UseHttps ? "s" : ""), Server, Port, QueryString));
                }

                if (Method == RequestMethod.GET && Parameters.Count > 0)
                {
                    // Generate the parameters string
                    string parameterString = "?";
                    foreach (KeyValuePair<string, string> pair in Parameters)
                    {
                        parameterString += String.Format("{0}={1}&", pair.Key, pair.Value);
                    }
                    parameterString = parameterString.TrimEnd(new char[] { '&' });

                    // Add the parameters onto the end of the base uri
                    return new Uri(baseUri, parameterString);
                }
                else
                {
                    return baseUri;
                }
            }
        }

        /// <summary>
        /// Post/Put data.
        /// </summary>
        public string Data
        {
            get;
            protected set;
        }
        #endregion // Properties

        #region Abstract Properties
        /// <summary>
        /// Server to connect to
        /// </summary>
        protected abstract string Server
        {
            get;
        }

        /// <summary>
        /// Whether we should use https to connect to the server
        /// </summary>
        protected abstract bool UseHttps
        {
            get;
        }

        /// <summary>
        /// Port to connect on
        /// </summary>
        protected abstract uint Port
        {
            get;
        }

        /// <summary>
        /// Rest service to use
        /// </summary>
        protected abstract string Service
        {
            get;
        }
        #endregion // Abstract Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public RestQuery()
            : this("")
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        public RestQuery(string query)
            : this(query, RequestMethod.GET)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <param name="method"></param>
        public RestQuery(string query, RequestMethod method)
            : this(query, method, null)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <param name="method"></param>
        /// <param name="data"></param>
        public RestQuery(string query, RequestMethod method, string data)
        {
            QueryString = query;
            Method = method;
            ContentType = "application/xml";
            Data = data;
            Parameters = new Dictionary<string, string>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="baseQuery"></param>
        public RestQuery(RestQuery baseQuery)
            : this(baseQuery, null)
        {
        }

        /// <summary>
        /// Constructs a new query based on a base query and the passed in relative query
        /// </summary>
        /// <param name="baseQuery"></param>
        /// <param name="relativeQuery"></param>
        public RestQuery(RestQuery baseQuery, string relativeQuery)
        {
            QueryString = baseQuery.QueryString;
            if (relativeQuery != null)
            {
                QueryString += "/" + relativeQuery;
            }
            ContentType = "application/xml";
            Method = baseQuery.Method;

            // Unfortunately we need to copy across all the parameters
            Parameters = new Dictionary<string, string>();
            foreach (KeyValuePair<string, string> pair in baseQuery.Parameters)
            {
                Parameters.Add(pair.Key, pair.Value);
            }
        }
        #endregion // Constructor(s)

        #region Object Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return QueryUri.ToString();
        }
        #endregion // Object Overrides
    } // RestQuery
} // RSG.Base.Net
