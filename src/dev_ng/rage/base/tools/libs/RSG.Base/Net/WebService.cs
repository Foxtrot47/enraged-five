﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Logging.Universal;
using RSG.Base.Logging;
using System.IO;
using System.Threading;
using System.Net;
using System.Diagnostics;
using System.Xml.Linq;
using System.Xml.XPath;

namespace RSG.Base.Net
{
    /// <summary>
    /// (Non WCF) internally used web publishing service for giving user 
    /// feedback regarding the server and client state.
    /// </summary>
    public class WebService
    {
        #region Private member fields

        private static ThreadManager m_threadManager;

        #endregion

        #region Constants
        /// <summary>
        /// Log context string.
        /// </summary>
        internal static readonly String LOG_CTX = "Automation Server Web Service";

        /// <summary>
        /// Config file containing the extension to mime-type mappings.
        /// </summary>
        internal static readonly String REPLACEMENTS_FILE = "content_type_replacements.xml";

        /// <summary>
        /// Default mime type to use if we aren't able to determine it from the file extension.
        /// </summary>
        internal static readonly String DEFAULT_MIME_TYPE = "application/octet-stream";

        /// <summary>
        /// How many messages to output to the log before flushing it.
        /// </summary>
        private const uint LOG_MESSAGE_FLUSH_COUNT = 100u;
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Whether the web service is currently running and serving content.
        /// </summary>
        public static bool IsRunning { get; private set; }

        /// <summary>
        /// Web server root directory
        /// </summary>
        public static String WebRootDir { get; set; }

        /// <summary>
        /// Port that the web server is listening on.
        /// </summary>
        public static uint WebServerPort { get; set; }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Web server ULog object
        /// </summary>
        internal static IUniversalLog Log { get; set; }

        /// <summary>
        /// Universal log file target for the web server ULog.
        /// </summary>
        internal static ILogFileTarget LogFileTarget { get; set; }

        /// <summary>
        /// Http listener.
        /// </summary>
        private static HttpListener Listener { get; set; }

        /// <summary>
        /// Mapping of file extensions to mime content types.
        /// </summary>
        internal static IDictionary<string, string> ContentTypeMap { get; set; }
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Static constructor.
        /// </summary>
        static WebService()
        {
            WebServerPort = 8080;

            LogFactory.Initialize();
            WebService.Log = LogFactory.CreateUniversalLog("Statistics Server Web Service");
            WebService.LogFileTarget = (ILogFileTarget)LogFactory.CreateUniversalLogFile(WebService.Log);
            WebService.IsRunning = false;
            WebService.ContentTypeMap = new Dictionary<string, string>();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Start the web service; accepting HTTP connections.
        /// </summary>
        public static void Start()
        {
            m_threadManager = new ThreadManager();

            // Ensure that the required variables were set up.
            Debug.Assert(WebRootDir != null, "Web root dir is null!");
            if (WebRootDir == null)
            {
                throw new ArgumentException("Web root dir is null!");
            }

            try
            {
                Thread.CurrentThread.Name = "Web Service";

                WebService.Listener = new HttpListener();
                WebService.Listener.Prefixes.Add(String.Format("http://*:{0}/", WebServerPort));
                WebService.Listener.Start();

                WebService.Log.MessageCtx(LOG_CTX, "Web Service Start: listening for requests.");
                WebService.IsRunning = true;

                int requestNum = 0;

                while (WebService.IsRunning)
                {
                    Thread.Sleep(10);
                    HttpListenerContext context = WebService.Listener.GetContext();
                    ProcessRequest(context);

                    // Check whether we should flush the log.
                    if (++requestNum % LOG_MESSAGE_FLUSH_COUNT == 0)
                    {
                        WebService.LogFileTarget.Flush();
                        requestNum = 0;
                    }
                }
            }
            catch (HttpListenerException ex)
            {
                // Error code for thread abort/application exit.
                if (ex.ErrorCode != 995)
                {
                    WebService.Log.ToolExceptionCtx(LOG_CTX, ex, "Exception.");
                }
            }
            catch (Exception ex)
            {
                WebService.Log.ToolExceptionCtx(LOG_CTX, ex, "Exception.");
            }
        }

        /// <summary>
        /// Stop the web service.
        /// </summary>
        public static void Stop()
        {
            if (null != WebService.Listener)
            {
                WebService.Listener.Stop();
                WebService.Listener.Close();
                WebService.Listener = null;
            }
            WebService.IsRunning = false;

            WebService.Log.MessageCtx(LOG_CTX, "Web Service Stopped");
            WebService.LogFileTarget.Flush();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configRootDir"></param>
        public static void SetConfigDir(string configRootDir)
        {
            string filepath = Path.Combine(configRootDir, "web", REPLACEMENTS_FILE);
            Debug.Assert(File.Exists(filepath), String.Format("Could not locate the replacements xml file '{0}'.", filepath));
            if (!File.Exists(filepath))
            {
                throw new FileNotFoundException(String.Format("Could not locate the replacements xml file '{0}'.", filepath));
            }

            WebService.ContentTypeMap.Clear();

            try
            {
                XDocument doc = XDocument.Load(filepath);

                foreach (XElement elem in doc.XPathSelectElements("/mappings/item"))
                {
                    XAttribute extensionAtt = elem.Attribute("ext");
                    XAttribute mimeTypeAtt = elem.Attribute("mime-type");
                    if (extensionAtt != null && mimeTypeAtt != null)
                    {
                        WebService.ContentTypeMap.Add(extensionAtt.Value, mimeTypeAtt.Value);
                    }
                    else
                    {
                        WebService.Log.WarningCtx(LOG_CTX, "Element in the '{0}' mapping file is missing either the 'ext' or 'mime-type' attribute. Elem: {1}.", REPLACEMENTS_FILE, elem);
                    }
                }
            }
            catch (System.Exception ex)
            {
                WebService.Log.ToolExceptionCtx(LOG_CTX, ex, "Exception will parsing content type replacement file.");
            }
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Handler for incoming web requests
        /// </summary>
        /// <param name="context"></param>
        private static void ProcessRequest(HttpListenerContext context)
        {
            m_threadManager.AddRequest(context);
        }
        #endregion // Private Methods
    } // WebService
}
