﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;

using RSG.Base.Math;
using RSG.Base.Logging;

namespace RSG.Base.Net
{

    //TODO: "This needs to be moved to a RAG interface library and documented."
    public class cRemoteConsole
    {
        //TODO: "This needs to be moved to a RAG interface library and documented."
        public enum EffectiveType
        {
            TYPE_UNKNOWN,
            TYPE_GROUP,
            TYPE_BUTTON,
            TYPE_FLOAT,
            TYPE_INT,
            TYPE_BOOL,
            TYPE_STRING,
            TYPE_VECTOR2,
            TYPE_VECTOR3,
            TYPE_VECTOR4,
            TYPE_MATRIX33,
            TYPE_MATRIX34,
            TYPE_MATRIX44,
        }

        //TODO: "This needs to be moved to a RAG interface library and documented."
        public class WidgetEntry
        {
            public string  m_Name;
            public string    m_RawType; // This is a u32 in C++ - should we keep it the same?
            public EffectiveType m_EffectiveType;
        }

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        public cRemoteConsole()
        {
            LogFactory.Initialize();
            m_log = LogFactory.CreateUniversalLog("RemoteConsole");
            m_Encoder = new ASCIIEncoding();
        }
        #endregion

        public bool Connect() { return Connect(IPAddress.Loopback.ToString(), 5105); }
        public bool Connect(string hostname) { return Connect(hostname, 5105); }

        public int ReadTimeout
        {
            get { return this.m_Stream.ReadTimeout; }
            set { this.m_Stream.ReadTimeout = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hostname"></param>
        /// <param name="port"></param>
        /// <returns></returns>
        public bool Connect(string hostname, int port)
        {
            m_log.Message("Attempting to connect to {0}:{1}", hostname, port);

            try
            {
                m_Client = new TcpClient(hostname, port);
                m_Stream = m_Client.GetStream();
                m_log.Message("Connection successfully established.");
            }
            catch (Exception e)
            {
                m_log.ToolException(e, "Connection failed.");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool IsConnected()
        {
            if (m_Client == null)
                return false;

            if (m_Client.Client.Poll(0, SelectMode.SelectRead))
            {
                byte[] buff = new byte[1];
                try
                {
                    if (m_Client.Client.Receive(buff, SocketFlags.Peek) == 0)
                    {
                        // Client disconnected
                        return false;
                    }
                }
                catch (System.Exception)
                {
                    return false;
                }
            }

            return m_Client.Connected;
        }

        public void DisconnectConsole()
        {
            if (m_Client != null)
            {
                m_log.Message("Disconnecting console.");
                
                try
                {
                    m_Stream.Close();
                    m_Client.Close();
                    m_log.Message("Connection successfully closed.");
                }
                catch (System.Exception ex)
                {
                    m_log.ToolException(ex, "Disconnection failed.");
                }

                m_Client = null;
                m_Stream = null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public string SendCommand(string command)
        {
            try
            {
                if (m_Stream == null) return null;

                byte[] bytes = m_Encoder.GetBytes(command);

                m_Stream.Write(bytes, 0, bytes.Length);
                m_Stream.WriteByte((byte)'\n');
                m_Stream.Flush();

                string result = "";

                const int bufSize = 256;
                bytes = new byte[bufSize];
                int bytesRead = 0;
                bool done = false;
                while (!done)
                {
                    int c = m_Stream.ReadByte();
                    if (c < 0)
                    {
                        done = true;
                    }
                    else
                    {
                        bytes[bytesRead] = (byte)c;
                        bytesRead++;
                        if (bytesRead == bufSize - 1)
                        {
                            result += m_Encoder.GetString(bytes, 0, bytesRead);
                            bytesRead = 0;
                        }
                        else if (c == 0)
                        {
                            result += m_Encoder.GetString(bytes, 0, bytesRead - 1); // don't count the '\0'
                            done = true;
                        }
                    }
                }

                return result;
            }
            catch (System.Exception e)
            {
                m_log.ToolException(e, "Failed to execute command.");

                //If an exception has been thrown, assume disconnection.
                DisconnectConsole();

                //Notify any handlers of the disconnection.
                if (Disconnect != null)
                {
                    Disconnect(this, EventArgs.Empty);
                }

                return null;
            }
        }

        public string SendSyncCommand()
        {
            return SendCommand("sync");
        }

        protected string SendWidgetCommand(string widgetname, string args)
        {
            string fullCmd = String.Format("widget \"{0}\" {1}", widgetname, args);
            return SendCommand(fullCmd);
        }

        public float ReadFloatWidget(string widgetName)
        {
            string str = SendWidgetCommand(widgetName, "");
            return Convert.ToSingle(str);
        }

        public float ReadFloatWidgetMinimum(string widgetName)
        {
            string str = SendWidgetCommand(widgetName, "-min");
            return Convert.ToSingle(str);
        }

        public float ReadFloatWidgetMaximum(string widgetName)
        {
            string str = SendWidgetCommand(widgetName, "-max");
            return Convert.ToSingle(str);
        }

        public int ReadIntWidget(string widgetName)
        {
            string str = SendWidgetCommand(widgetName, "");
            return Convert.ToInt32(str);
        }

        public bool ReadBoolWidget(string widgetName)
        {
            string str = SendWidgetCommand(widgetName, "");
            return Convert.ToBoolean(str);
        }

        public string ReadStringWidget(string widgetName)
        {
            return SendWidgetCommand(widgetName, "");
        }

        public Vector2f ReadVector2Widget(string widgetName)
        {
            string str = SendWidgetCommand(widgetName, "");
            Vector2f result = new Vector2f();
            string[] parts = str.Split((char[])null, StringSplitOptions.RemoveEmptyEntries);
            Debug.Assert(parts.Length == 2);
            result.X = Convert.ToSingle(parts[0]);
            result.Y = Convert.ToSingle(parts[1]);

            return result;
        }

        public Vector3f ReadVector3Widget(string widgetName)
        {
            string str = SendWidgetCommand(widgetName, "");
            Vector3f result = new Vector3f();
            string[] parts = str.Split((char[])null, StringSplitOptions.RemoveEmptyEntries);
            Debug.Assert(parts.Length == 3);
            result.X = Convert.ToSingle(parts[0]);
            result.Y = Convert.ToSingle(parts[1]);
            result.Z = Convert.ToSingle(parts[2]);

            return result;
        }

        public Vector4f ReadVector4Widget(string widgetName)
        {
            string str = SendWidgetCommand(widgetName, "");
            Vector4f result = new Vector4f();
            string[] parts = str.Split((char[])null, StringSplitOptions.RemoveEmptyEntries);
            Debug.Assert(parts.Length == 4 || parts.Length == 3);
            result.X = Convert.ToSingle(parts[0]);
            result.Y = Convert.ToSingle(parts[1]);
            result.Z = Convert.ToSingle(parts[2]);
            if (parts.Length == 3)
            {
                result.W = 1.0f;    // Special case for treating colors (even 3 channel ones) as vector4s
            }
            else
            {
                result.W = Convert.ToSingle(parts[3]);
            }

            return result;
        }

        public Matrix33f ReadMatrix33Widget(string widgetName)
        {
            string str = SendWidgetCommand(widgetName, "");
            Matrix33f result = new Matrix33f();
            string[] parts = str.Split((char[])null, StringSplitOptions.RemoveEmptyEntries);
            Debug.Assert(parts.Length == 9);

            result.A.X = Convert.ToSingle(parts[0]);
            result.A.Y = Convert.ToSingle(parts[1]);
            result.A.Z = Convert.ToSingle(parts[2]);

            result.B.X = Convert.ToSingle(parts[3]);
            result.B.Y = Convert.ToSingle(parts[4]);
            result.B.Z = Convert.ToSingle(parts[5]);

            result.C.X = Convert.ToSingle(parts[6]);
            result.C.Y = Convert.ToSingle(parts[7]);
            result.C.Z = Convert.ToSingle(parts[8]);

            return result;
        }

        public Matrix34f ReadMatrix34Widget(string widgetName)
        {
            string str = SendWidgetCommand(widgetName, "");
            Matrix34f result = new Matrix34f();
            string[] parts = str.Split((char[])null, StringSplitOptions.RemoveEmptyEntries);
            Debug.Assert(parts.Length == 12);

            result.A.X = Convert.ToSingle(parts[0]);
            result.A.Y = Convert.ToSingle(parts[1]);
            result.A.Z = Convert.ToSingle(parts[2]);

            result.B.X = Convert.ToSingle(parts[3]);
            result.B.Y = Convert.ToSingle(parts[4]);
            result.B.Z = Convert.ToSingle(parts[5]);

            result.C.X = Convert.ToSingle(parts[6]);
            result.C.Y = Convert.ToSingle(parts[7]);
            result.C.Z = Convert.ToSingle(parts[8]);

            result.D.X = Convert.ToSingle(parts[9]);
            result.D.Y = Convert.ToSingle(parts[10]);
            result.D.Z = Convert.ToSingle(parts[11]);

            return result;
        }

        public bool WriteFloatWidget(string widgetName, float newValue)
        {
            string result = SendCommand(String.Format("widget \"{0}\" {1}", widgetName, newValue));
            return (result == null) ? false : true;
        }

        public bool WriteIntWidget(string widgetName, int newValue)
        {
            string result = SendCommand(String.Format("widget \"{0}\" {1}", widgetName, newValue));
            return (result == null) ? false : true;
        }

        public bool WriteBoolWidget(string widgetName, bool newValue)
        {
            string result = SendWidgetCommand(widgetName, newValue ? "true" : "false");
            return (result == null) ? false : true;
        }

        public bool WriteStringWidget(string widgetName, string newValue)
        {
            string result = SendCommand(String.Format("widget \"{0}\" \"{1}\"", widgetName, newValue));
            return (result == null) ? false : true;
        }

        public bool WriteVector2Widget(string widgetName, Vector2f newValue)
        {
            string result = SendCommand(String.Format("widget \"{0}\" {1} {2}", widgetName, newValue.X, newValue.Y));
            return (result == null) ? false : true;
        }

        public bool WriteVector3Widget(string widgetName, Vector3f newValue)
        {
            string result = SendCommand(String.Format("widget \"{0}\" {1} {2} {3}", widgetName, newValue.X, newValue.Y, newValue.Z));
            return (result == null) ? false : true;
        }

        public bool WriteVector4Widget(string widgetName, Vector4f newValue)
        {
            string result = SendCommand(String.Format("widget \"{0}\" {1} {2} {3} {4}", widgetName, newValue.X, newValue.Y, newValue.Z, newValue.W));
            return (result == null) ? false : true;
        }

        public bool WriteMatrix33Widget(string widgetName, Matrix33f newValue)
        {
            string result = SendWidgetCommand(widgetName,
                String.Format("{0} {1} {2} ", newValue.A.X, newValue.A.Y, newValue.A.Z) +
                String.Format("{0} {1} {2} ", newValue.B.X, newValue.B.Y, newValue.B.Z) +
                String.Format("{0} {1} {2} ", newValue.C.X, newValue.C.Y, newValue.C.Z));

            return (result == null) ? false : true;
        }

        public bool WriteMatrix34Widget(string widgetName, Matrix34f newValue)
        {
            string result = SendWidgetCommand(widgetName,
                String.Format("{0} {1} {2} ", newValue.A.X, newValue.A.Y, newValue.A.Z) +
                String.Format("{0} {1} {2} ", newValue.B.X, newValue.B.Y, newValue.B.Z) +
                String.Format("{0} {1} {2} ", newValue.C.X, newValue.C.Y, newValue.C.Z) +
                String.Format("{0} {1} {2} ", newValue.D.X, newValue.D.Y, newValue.D.Z));

            return (result == null) ? false : true;
        }

        /// <summary>
        /// Simulates a button press on the RAG user interface.
        /// </summary>
        /// <param name="widgetName"></param>
        /// <returns></returns>
        public bool PressWidgetButton(string widgetName)
        {
            string fullCmd = String.Format("widget \"{0}\"", widgetName);
            if (SendCommand(fullCmd) != null)
                return true;

            return false;
        }

        /// <summary>
        /// A special command to RAG that activates a particular bank.
        /// This is necessary for widgets to be operated upon via a remote connection
        /// when they are not visible in the actual RAG user interface.
        /// </summary>
        /// <param name="bankName"></param>
        /// <returns></returns>
        public bool AddUpdates(string bankName)
        {
            if (m_Client != null && m_Client.Connected)
            {
                string command = String.Format("getupdates \"{0}\" on", bankName);
                string result = SendCommand(command);
                return (result == null) ? false : true;
            }

            return true;
        }

        /// <summary>
        /// A special command to RAG that deactivates a bank.  
        /// </summary>
        /// <param name="bankName"></param>
        /// <returns></returns>
         public bool RemoveUpdates(string bankName)
        {
            if (m_Client != null && m_Client.Connected)
            {
                string command = String.Format("getupdates \"{0}\" off", bankName);
                string result = SendCommand(command);
                return (result == null) ? false : true;
            }

            return true;
        }

        public bool WidgetExists(string widgetName)
        {
            string command = String.Format("widget_exists \"{0}\"", widgetName);
            string result = SendCommand(command);

            if ( String.Compare(result, "true", true) == 0)            
                return true;

            return false;
        }

        public List<WidgetEntry> ListWidgets(string parentDir)
        {
            string result = SendCommand(String.Format("dir -t \"{0}\"", parentDir));

            List<WidgetEntry> items = new List<WidgetEntry>();

            string[] lines = result.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);

            foreach (string line in lines)
            {
                string[] substrs = line.Split(new char[] { '\t' });

                WidgetEntry entry = new WidgetEntry();
                entry.m_Name = substrs[0];
                entry.m_RawType = substrs[1].Substring(0, 4); // strip any trailing whitespace
                entry.m_EffectiveType = FindEffectiveType(entry.m_RawType);

                items.Add(entry);
            }

            return items;
        }

        public static EffectiveType FindEffectiveType(string rawType)
        {
            switch(rawType)
            {
                case "bank":
                case "bmgr":
                case "grup":
                    return EffectiveType.TYPE_GROUP;
                case "butn":
                    return EffectiveType.TYPE_BUTTON;
                case "angl":
                case "slfl":
                    return EffectiveType.TYPE_FLOAT;
                case "slu8":
                case "sls8":
                case "slu1":
                case "sls1":
                case "slu3":
                case "sls3":
                    return EffectiveType.TYPE_INT;
                case "tgbo":
                case "tgs3":
                case "tgu8":
                case "tgu1":
                case "tgu3":
                case "tgbs":
                    return EffectiveType.TYPE_BOOL;
                case "imgv":
                case "text":
                case "cou8":
                case "cos8":
                case "cos1":
                case "cos3":
                    return EffectiveType.TYPE_STRING;
                case "vec2":
                    return EffectiveType.TYPE_VECTOR2;
                case "vec3":
                    return EffectiveType.TYPE_VECTOR3;
                case "vec4":
                case "vclr":
                    return EffectiveType.TYPE_VECTOR4;
                case "mtx9":
                    return EffectiveType.TYPE_MATRIX33;
                case "mtx2":
                    return EffectiveType.TYPE_MATRIX34;
                case "mtx6":
                    return EffectiveType.TYPE_MATRIX44;
                case "inpt":
                case "sepr":
                case "data":
                case "list":
                case "titl":
                case "tlst":
                case "null":
                    return EffectiveType.TYPE_UNKNOWN;
            }
            return EffectiveType.TYPE_UNKNOWN;
        }

        protected ASCIIEncoding m_Encoder;
        protected TcpClient m_Client;
        protected NetworkStream m_Stream;
        protected ILog m_log;

        public delegate void DisconnectEventHandler(object sender, EventArgs e);
        public event DisconnectEventHandler Disconnect;
    }
}

