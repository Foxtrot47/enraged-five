﻿//---------------------------------------------------------------------------------------------
// <copyright file="TryResult{T}.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Base
{
    /// <summary>
    /// Contains both the result and the success flag from a generic operation.
    /// </summary>
    /// <typeparam name="T">
    /// The type of the result value from the generic operation.
    /// </typeparam>
    public struct TryResult<T>
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Success"/> property.
        /// </summary>
        private bool _success;

        /// <summary>
        /// The private field used for the <see cref="Value"/> property.
        /// </summary>
        private T _value;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="Result{T}" /> structure
        /// with the specified result and success flag.
        /// </summary>
        /// <param name="value">
        /// The resulting value got from the generic operation.
        /// </param>
        /// <param name="success">
        /// A value that indicates whether the generic operation was successful or not.
        /// </param>
        public TryResult(T value, bool success)
        {
            this._success = success;
            this._value = value;
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Gets a value indicating whether or not the generic operation was successful.
        /// </summary>
        public bool Success
        {
            get { return this._success; }
        }

        /// <summary>
        /// Gets the resulting value from the generic operation.
        /// </summary>
        public T Value
        {
            get { return this._value; }
        }
        #endregion Methods
    } // RSG.Base.TryResult<T> {Structure}
} // RSG.Base {Namespace}
