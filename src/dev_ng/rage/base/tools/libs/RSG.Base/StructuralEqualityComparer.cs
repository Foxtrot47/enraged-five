﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base
{
    /// <summary>
    /// Generic structural euality comparer.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class StructuralEqualityComparer<T> : IEqualityComparer<T> where T : IStructuralEquatable
    {
        #region Static Data
        /// <summary>
        /// Static instance of the default comparer for type T.
        /// </summary>
        private static StructuralEqualityComparer<T> _defaultComparer;
        #endregion // Member Data

        #region Properties
        /// <summary>
        /// Refernce to the static instance of the default comparer.
        /// </summary>
        public static StructuralEqualityComparer<T> Default
        {
            get
            {
                if (_defaultComparer == null)
                {
                    _defaultComparer = new StructuralEqualityComparer<T>();
                }
                return _defaultComparer;
            }
        }
        #endregion // Properties

        #region IEqualityComparer<T> Implementation
        /// <summary>
        /// Determines whether the specified objects are equal.
        /// </summary>
        /// <param name="x">The first object of type T to compare.</param>
        /// <param name="y">The second object of type T to compare.</param>
        /// <returns>true if the specified objects are equal; otherwise, false.</returns>
        public bool Equals(T x, T y)
        {
            return StructuralComparisons.StructuralEqualityComparer.Equals(x, y);
        }

        /// <summary>
        /// Returns a hash code for the specified object.
        /// </summary>
        /// <param name="obj">The System.Object for which a hash code is to be returned.</param>
        /// <returns>A hash code for the specified object.</returns>
        public int GetHashCode(T obj)
        {
            return StructuralComparisons.StructuralEqualityComparer.GetHashCode(obj);
        }
        #endregion // IEqualityComparer<T> Implementation
    } // StructuralEqualityComparer<T>
}
