//
// File: PluginLoader.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of cPluginLoader.cs class
//

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace RSG.Base.Plugins
{
    #region Plugin Loader Class
    /// <summary>
    /// Generic plugin loader class
    /// </summary>
    /// This generic plugin loader does not automatically create instances of 
    /// the compatible classes that it finds.  They must be manually constructed
    /// afterwards using the CreatePluginInstance methods.
    /// 
    /// The plugin search path and file extensions are customisable using the
    /// available constructors.  The default plugin file extension is ".dll".
    public class PluginLoader<INTERFACE> where INTERFACE : class
    {
        #region Constants
        private const string sC_s_str_DefaultExtension = ".dll";
        #endregion

        #region Properties
        /// <summary>
        /// List of plugins found implementing the specified interface
        /// </summary>
        public List<Type> Plugins
        {
            get { return m_Plugins; }
        }

        /// <summary>
        /// 
        /// </summary>
        public IPlugin[] RawPlugins
        {
            get
            {
                List<IPlugin> plugins = new List<IPlugin>();
                foreach (IPlugin plug in m_Plugins)
                    plugins.Add(plug);
                return (plugins.ToArray());
            }
        }

        /// <summary>
        /// The search path used for finding plugins
        /// </summary>
        public string Path
        {
            get { return m_sPluginPath; }
        }

        /// <summary>
        /// The file extension used for finding plugins (starts with ".")
        /// </summary>
        public string Extension
        {
            get { return m_sExtension; }
        }
        #endregion

        #region Member Data
        private List<Assembly> m_Assemblies;
        private List<Type> m_Plugins;
        private string m_sPluginPath;
        private string m_sExtension;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor; initialising plugins from directory DLLs.
        /// </summary>
        /// <param name="path"></param>
        public PluginLoader(String path)
            : this(path, sC_s_str_DefaultExtension)
        {
        }

        /// <summary>
        /// Constructor; initialising plugins from directory and extension.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="ext"></param>
        public PluginLoader(String path, String ext)
            : this(path, ext, null)
        {
        }

        /// <summary>
        /// Constructor; initialising plugins from directory and extension.
        /// Aslo add objects from a passed in assembly.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="ext"></param>
        /// <param name="caller"></param>
        public PluginLoader(String path, String ext, Assembly caller)
        {
            if (!Directory.Exists(path))
                throw new PluginException("Plugin path does not exist: " + path);
            if (0 == ext.Length)
                throw new PluginException("Plugin extension is empty");
            if (!ext.StartsWith("."))
                throw new PluginException("Plugin extension must start with a period");

            m_sExtension = ext;
            m_sPluginPath = path;
            m_Plugins = new List<Type>();
            m_Assemblies = new List<Assembly>();
            m_Plugins.Clear();

            FindPlugins(caller);
        }
        #endregion

        #region Controller Methods
        /// <summary>
        /// Create an instance of a plugin (provided it implements INTERFACE)
        /// </summary>
        /// <param name="plugin"></param>
        /// <returns>Instance of plugin INTERFACE class</returns>
        public INTERFACE CreatePluginInstance(Type plugin)
        {
            try
            {
                if (typeof(INTERFACE).IsAssignableFrom(plugin))
                    return (INTERFACE)Activator.CreateInstance(plugin);
            }
            catch (TargetInvocationException ex)
            {
                Logging.Log.Log__Error("initialisation of plugin failed.  Exception: " + ex.Message);
            }

            return default(INTERFACE);
        }

        /// <summary>
        /// Create an instance of a plugin (provided it implements INTERFACE)
        /// </summary>
        /// <param name="nIndex"></param>
        /// <returns>Instance of plugin INTERFACE class</returns>
        public INTERFACE CreatePluginInstance(int nIndex)
        {
            Debug.Assert(m_Plugins.Count == m_Assemblies.Count);

            if ((nIndex < m_Plugins.Count) && (nIndex < m_Assemblies.Count))
            {
                try
                {
                    if (typeof(INTERFACE).IsAssignableFrom(m_Plugins[nIndex]))
                        return (INTERFACE)Activator.CreateInstance(m_Plugins[nIndex]);
                }
                catch (TargetInvocationException)
                {
                    Logging.Log.Log__Error("initialisation of plugin failed.  Constructor raised an exception.");
                }
            }

            return default(INTERFACE);
        }

        /// <summary>
        /// Return list of Plugin instances for all contained types
        /// </summary>
        /// <returns></returns>
        /// This can be useful for the listing plugins in a UI or console
        /// application.  See also RSG.Base.Forms.Forms.uiPluginListDlg.
        /// 
        public List<IPlugin> GetPluginList()
        {
            List<RSG.Base.Plugins.IPlugin> plugins = new List<RSG.Base.Plugins.IPlugin>();
            foreach (Type t in this.Plugins)
                plugins.Add((RSG.Base.Plugins.IPlugin)this.CreatePluginInstance(t));

            return (plugins);
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Finds all plugins in our path
        /// </summary>
        /// <param name="assembly"></param>
        private void FindPlugins(Assembly assembly)
        {
            if (null != assembly)
                AddPlugin(assembly);
            foreach (String sPlugin in Directory.GetFiles(m_sPluginPath))
            {
                FileInfo file = new FileInfo(sPlugin);

                if (file.Extension.Equals(m_sExtension))
                {
                    AddPlugin(file);
                }
            }
        }

        /// <summary>
        /// Load objects from a particular assembly.
        /// </summary>
        /// <param name="assembly"></param>
        private void AddPlugin(Assembly assembly)
        {
            // Go through all of the available types looking for our required INTERFACE
            foreach (Type pluginType in assembly.GetTypes())
            {
                // Only interested in public and non-abstract types
                if (pluginType.IsPublic && !pluginType.IsAbstract)
                {
                    if (typeof(INTERFACE).IsAssignableFrom(pluginType))
                    {
                        // We have found a new plugin so lets construct it
                        m_Assemblies.Add(assembly);
                        m_Plugins.Add(pluginType);
                    }
                }
            }
        }

        /// <summary>
        /// Load object from a particular assembly filename.
        /// </summary>
        /// <param name="file"></param>
        private void AddPlugin(FileInfo file)
        {
            try
            {
                Assembly pluginAssembly = Assembly.LoadFrom(file.FullName);
                AddPlugin(pluginAssembly);
            }
            catch (Exception e)
            {
                // Failure to load a plugin should not throw an exception, but we log
                // a warning to ensure developers and users are aware of potential
                // failures during the loading.
                Logging.Log.Log__Warning("Failure attempting to load " + typeof(INTERFACE).ToString() + " plugin: " + e.Message);
            }
        }
        #endregion
    }
    #endregion // Plugin Loader Class

    #region Exceptions
    /// <summary>
    /// Exception raised when there is an error with the Plugin Loader
    /// </summary>
    public class PluginException : Exception
    {
        #region Constructors
        public PluginException()
         : base( )
        {
        }
        
        public PluginException(String sMessage)
         : base(sMessage)
        {
        }
        
        public PluginException(String sMessage, Exception InnerException)
         : base(sMessage, InnerException)
        {
        }

        public PluginException(System.Runtime.Serialization.SerializationInfo info, 
                                 System.Runtime.Serialization.StreamingContext context)
         : base(info, context)
        {
        }
        #endregion // Constructors
    }
    #endregion // Exceptions

} // End of RSG.Base.Plugins namespace

// End of file
