//
// File: IPlugin.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of IPlugin class
//

using System;

namespace RSG.Base.Plugins
{
    
    /// <summary>
    /// Plugin Interface
    /// </summary>
    /// Classes that can be loaded as plugins should implement this interface.
    /// 
    public interface IPlugin
    {
        #region Properties
        /// <summary>
        /// Plugin Name
        /// </summary>
        String Name { get; }

        /// <summary>
        /// Plugin Version
        /// </summary>
        Version Version { get; }

        /// <summary>
        /// Plugin Description
        /// </summary>
        String Description { get; }
        #endregion // Properties
    }

} // End of RSG.Base.Plugins namespace

// End of file
