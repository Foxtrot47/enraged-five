//
// File: iPluginSettings.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of iPluginSettings class
//

using System;

namespace RSG.Base.Plugins
{
    
    /// <summary>
    /// Abstract base class for Plugin Settings classes
    /// </summary>
    public abstract class iPluginSettings : global::System.Configuration.ApplicationSettingsBase
    {
        #region Properties
        /// <summary>
        /// Plugin Name
        /// </summary>
        public abstract String Name { get; }
        #endregion // Properties
    }

} // End of RSG.Base.Plugins namespace

// End of file
