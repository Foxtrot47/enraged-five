﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace RSG.Base.Profiling
{
    /// <summary>
    /// 
    /// </summary>
    [Obsolete("Use Universal Log profiling messages.")]
    internal class Task : ITask
    {
        internal Task(string name)
        {
            Name = name;
            stopwatch_ = Stopwatch.StartNew();
        }

        #region ITask Members

        public void Stop()
        {
            stopwatch_.Stop();
        }

        #endregion

        internal void AddChildTask(Task childTask)
        {
            childTask.Parent = this;

            if (childTasks_ == null)
                childTasks_ = new List<Task>();

            childTasks_.Add(childTask);
        }

        internal string Name { get; private set; }
        internal Task Parent { get; private set; }
        internal IEnumerable<Task> Children { get { return childTasks_; } }
        internal long ElapsedMilliseconds { get { return stopwatch_.ElapsedMilliseconds; } }

        private Stopwatch stopwatch_;
        private List<Task> childTasks_;
    }
}
