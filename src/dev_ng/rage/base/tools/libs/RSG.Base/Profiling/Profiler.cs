﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace RSG.Base.Profiling
{
    /// <summary>
    /// The main profiler class
    /// </summary>
    [Obsolete("Use Universal Log profiling messages.")]
    internal class Profiler : IProfiler
    {
        #region IProfiler Members

        public ITask StartTask(string taskName)
        {
            Task newTask = new Task(taskName);
            if (rootTask_ != null)
            {
                currentTask_.AddChildTask(newTask);
                currentTask_ = newTask;
            }
            else
            {
                rootTask_ = currentTask_ = newTask;
            }

            return newTask;
        }

        public void StopTask(ITask task)
        {
            Task concreteTask = (Task)task;
            concreteTask.Stop();

            if (currentTask_.Parent != null)
                currentTask_ = currentTask_.Parent;
        }

        public void WriteLog(string pathname)
        {
            string targetDirectory = Path.GetDirectoryName(pathname);
            if (!Directory.Exists(targetDirectory))
                Directory.CreateDirectory(targetDirectory);

            using (StreamWriter writer = File.CreateText(pathname))
            {
                OutputProfileTaskRecursive(rootTask_, writer, 0);
            }
        }

        #endregion

        private static void OutputProfileTaskRecursive(Task task, StreamWriter writer, int hierarchyLevel)
        {
            // write leading columns
            for (int indentIndex = 0; indentIndex < hierarchyLevel; ++indentIndex)
                writer.Write(',');

            writer.Write(task.Name);

            // write trailing columns
            for (int indentIndex = maxNumIndents_; indentIndex > hierarchyLevel; --indentIndex)
                writer.Write(',');

            writer.WriteLine(task.ElapsedMilliseconds);

            if (task.Children != null)
            {
                foreach (Task childTask in task.Children)
                {
                    OutputProfileTaskRecursive(childTask, writer, hierarchyLevel + 1);
                }
            }
        }

        private static int maxNumIndents_ = 10;

        private Task rootTask_;
        private Task currentTask_;
    }
}
