﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Profiling
{
    /// <summary>
    /// A stub implementation of the ITask interface
    /// </summary>
    [Obsolete("Use Universal Log profiling messages.")]
    internal class StubTask : ITask
    {
        internal StubTask()
        {

        }

        #region ITask Members

        public void Stop()
        {

        }

        #endregion
    }
}
