﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Profiling
{
    /// <summary>
    /// As stub implementation of the IProfiler interface.
    /// </summary>
    [Obsolete("Use Universal Log profiling messages.")]
    class StubProfiler : IProfiler
    {
        static StubProfiler()
        {
            stubTask_ = new StubTask();
        }

        #region IProfiler Members

        public ITask StartTask(string taskName)
        {
            return stubTask_;
        }

        public void StopTask(ITask task)
        {

        }

        public void WriteLog(string pathname)
        {
            
        }

        #endregion

        private static StubTask stubTask_;
    }
}
