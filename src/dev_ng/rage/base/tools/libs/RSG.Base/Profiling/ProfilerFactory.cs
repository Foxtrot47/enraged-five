﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Profiling
{
    [Obsolete("Use Universal Log profiling messages.")]
    public static class ProfilerFactory
    {
        public static IProfiler CreateProfiler()
        {
            return new Profiler();
        }

        public static IProfiler CreateStubProfiler()
        {
            return new StubProfiler();
        }
    }
}
