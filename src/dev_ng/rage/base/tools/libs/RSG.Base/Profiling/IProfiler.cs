﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Profiling
{
    /// <summary>
    /// The main profiler interface
    /// </summary>
    [Obsolete("Use Universal Log profiling messages.")]
    public interface IProfiler
    {
        ITask StartTask(string taskName);
        void StopTask(ITask task);
        void WriteLog(string pathname);
    }
}
