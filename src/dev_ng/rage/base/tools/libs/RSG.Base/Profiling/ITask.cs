﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Profiling
{
    /// <summary>
    /// The interface for an individual profiler task
    /// </summary>
    [Obsolete("Use Universal Log profiling messages.")]
    public interface ITask
    {
    }
}
