﻿//---------------------------------------------------------------------------------------------
// <copyright file="ICommandLineToolTask.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Base.Command
{
    using System;

    /// <summary>
    /// Command-line tool task interface.
    /// </summary>
    /// This was originally created for the Incredibuild and GNU make interoperability
    /// with tools; so the core pipeline can share the interface.
    /// 
    public interface ICommandLineToolTask
    {
        #region Properties
        /// <summary>
        /// Task name.
        /// </summary>
        String Name { get; set; }

        /// <summary>
        /// Command-line Tool to use for this task.
        /// </summary>
        ICommandLineTool Tool { get; set; }
        
        /// <summary>
        /// Working directory for this task.
        /// </summary>
        String WorkingDirectory { get; set; }
        #endregion // Properties
    }

} // RSG.Base.Command namespace
