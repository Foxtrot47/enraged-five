using System;
using System.IO;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;
using System.Threading;
using System.Text;

using RSG.Base.IO;
using RSG.Base.Logging;

namespace RSG.Base.Command
{
	/// <summary>
	/// A class for handling the execution of external tools, and logging the results
	/// </summary>
	public class rageExecuteCommand : IDisposable
	{
		~rageExecuteCommand()
		{
			// Clean up
			if (RemoveLog && (m_obHtmlLog != null))
			{
				rageStatus obStatus;
				rageFileUtilities.DeleteLocalFile(m_obHtmlLog.GetFilename(), out obStatus);
			}
			// System.Console.WriteLine("~rageExecuteCommand()");
		}

		/// <summary>
		/// Constructor to setup a command for executing later.
		/// NOTE : This constructor DOES not actually execute the command, you must called Execute later to do that
		/// </summary>
		public rageExecuteCommand()
		{
			// Give good defaults for everything
			m_iExitCode = 0;
			m_strCommand = "";
			m_strArgs = "";
			m_strWorkingDirectory = "";
			TimeOutInSeconds = -1.0f;
			m_fTimeOutWithoutOutputInSeconds = -1.0f;
			LogFilename = "";
			m_bEchoToConsole = false;
			m_bUseBusySpinner = true;
			RemoveLog = true;
			LogToHtmlFile = true;
			Priority = ProcessPriorityClass.Normal;
            m_EnvironmentVariables = new Dictionary<string, string>();
		}

		/// <summary>
		/// Function to execute a command with logging
		/// NOTE : This constructor DOES actually execute the command, calling execute later with execute it again
		/// </summary>
		public rageExecuteCommand(string strCommand, out rageStatus obStatus)
			: this(strCommand, "", out obStatus)
		{
		}

		/// <summary>
		/// Function to execute a command with logging
		/// NOTE : This constructor DOES actually execute the command, calling execute later with execute it again
		/// </summary>
		public rageExecuteCommand(string strCommand, string strArgs, out rageStatus obStatus)
			: this(strCommand, strArgs, "", out obStatus)
		{
		}

		/// <summary>
		/// Function to execute a command with logging
		/// NOTE : This constructor DOES actually execute the command, calling execute later with execute it again
		/// </summary>
		public rageExecuteCommand(string strCommand, string strArgs, string strWorkingDirectory, out rageStatus obStatus)
			: this(strCommand, strArgs, strWorkingDirectory, -1.0f, out obStatus)
		{
		}

		/// <summary>
		/// Function to execute a command with logging
		/// NOTE : This constructor DOES actually execute the command, calling execute later with execute it again
		/// </summary>
		public rageExecuteCommand(string strCommand, string strArgs, string strWorkingDirectory, float fTimeOutInSeconds, out rageStatus obStatus)
		{
			m_iExitCode = 0;
			m_strCommand = strCommand;
			m_strArgs = strArgs;
			m_strWorkingDirectory = strWorkingDirectory;
			TimeOutInSeconds = fTimeOutInSeconds;
			m_fTimeOutWithoutOutputInSeconds = -1.0f;
			LogFilename = "";
			m_bEchoToConsole = false;
			m_bUseBusySpinner = true;
			RemoveLog = true;
			Execute(out obStatus);
		}

		/// <summary>
		/// Actually execute the command in question
		/// </summary>
		public void Execute()
		{
			rageStatus obStatus;
			Execute(out obStatus);
		}

		/// <summary>
		/// Actually execute the command in question
		/// </summary>
		public void Execute(out rageStatus obStatus)
		{
			// Open log file
			InitLogging();
			m_obAStrLog = new List<string>();
			m_obAStrStdOutLog = new List<string>();
			m_obAStrStdErrLog = new List<string>();

			// Create the process
			m_obProcess = new Process();
            
			// System.Console.WriteLine(m_strWorkingDirectory +">"+ m_strCommand +" "+ m_strArgs);
			m_obProcessStartInfo = new ProcessStartInfo(m_strCommand);
			m_obProcessStartInfo.CreateNoWindow = true;
			m_obProcessStartInfo.Arguments = m_strArgs;
			if (m_strWorkingDirectory != "")
			{
				// Standardize slashes
				m_strWorkingDirectory = m_strWorkingDirectory.Replace("\\", "/");
				if (m_strWorkingDirectory[m_strWorkingDirectory.Length - 1] != '/') m_strWorkingDirectory += "/";
				m_obProcessStartInfo.WorkingDirectory = m_strWorkingDirectory;
			}
			m_obProcessStartInfo.UseShellExecute = false;
			m_obProcessStartInfo.RedirectStandardOutput = true;
			m_obProcessStartInfo.RedirectStandardError = true;

            foreach (KeyValuePair<string, string> pair in m_EnvironmentVariables)
            {
                m_obProcessStartInfo.EnvironmentVariables.Add(pair.Key, pair.Value);
            }

			m_obProcess.StartInfo = m_obProcessStartInfo;

			// Kick it off
			try
			{
				// m_obProcess.PriorityClass = ProcessPriorityClass.AboveNormal;
				m_obProcess.Start();
				m_obProcess.Refresh();
                if ( !m_obProcess.HasExited )
                {
                    try
                    {
                        m_obProcess.PriorityClass = Priority;
                    }
                    catch ( Exception e )
                    {
                        // Something bad happened
                        Console.WriteLine( "Failed to set the priority of process,.Normal used : " + m_obProcess.Id + " : " + e.ToString() );
                    }
                }
			}
			catch (Exception e)
			{
				// Something bad happened
				Status = obStatus = new rageStatus("Failed to execute \"" + m_strWorkingDirectory + ">" + m_strCommand + " " + m_strArgs + "\" : " + e.ToString());
				if (PrefixLogWithErrorsAndWarnings) AddErrorsAndWarningsPrefixToLog();
				return;
			}

			// Invoke stdOut and stdErr readers - each
			// has its own thread to guarantee that they aren't
			// blocked by, or cause a block to, the actual
			// process running (or the gui).
			Thread obStandardOutputThread = new Thread(new ThreadStart(ReadStandardOutput));
			obStandardOutputThread.Name = "rageExecuteCommand.StandardOutputThread";
			obStandardOutputThread.Priority = ThreadPriority.BelowNormal;
			Thread obStandardErrorThread = new Thread(new ThreadStart(ReadStandardError));
			obStandardErrorThread.Name = "rageExecuteCommand.StandardErrorThread";
			obStandardErrorThread.Priority = ThreadPriority.BelowNormal;
			obStandardOutputThread.Start();
			obStandardErrorThread.Start();

			// Wait for the process to end, or cancel it
			m_obTimeOfLastOutput = System.DateTime.Now;
			bool bProcessTimedOut = false;
			m_iSizeOfLogAtLastOutput = 0;
			while (!m_obProcess.HasExited)
			{
				// Should I time out?
				if ((m_fTimeOutInSeconds > 0.0f) && (m_fTimeOutInSeconds < ((DateTime.Now - m_obProcess.StartTime).TotalSeconds)))
				{
					// Time out
					// Not a very nice way to end a process, but effective.
					if (!bProcessTimedOut)
					{
						// Log time out
						string strLogMessage = "Process Timed Out because it has been " + ((DateTime.Now - m_obProcess.StartTime).TotalSeconds) + " seconds since it started, killing... (StartTime = " + m_obProcess.StartTime + "  Now = " + DateTime.Now + ")";
						if (LogToHtmlFile)
						{
							m_obHtmlLog.LogEvent("<font color=#FF0000>" + strLogMessage + "</font>", out obStatus);
						}
						m_obAStrLog.Add(strLogMessage);
						try
						{
							Console.WriteLine("Process has timed out, closing it.");
							m_obProcess.CloseMainWindow();
						}
						catch (Exception ex)
						{
							System.Console.WriteLine("The following exception occurred when trying to kill the process: " + ex.ToString());
							//check the InnerException
							while (ex.InnerException != null)
							{
								System.Console.WriteLine("--------------------------------");
								System.Console.WriteLine("The following InnerException reported: " + ex.InnerException.ToString());
								ex = ex.InnerException;
							}
						}
					}
					else
					{
						// I've already tried to kill this process once and it didn't work, so bring out the big guns
						try
						{
							Console.WriteLine("Process has timed out, killing it.");
							m_obProcess.Kill();
						}
						catch (Exception ex)
						{
							System.Console.WriteLine("The following exception occurred when trying to kill the process: " + ex.ToString());
							//check the InnerException
							while (ex.InnerException != null)
							{
								System.Console.WriteLine("--------------------------------");
								System.Console.WriteLine("The following InnerException reported: " + ex.InnerException.ToString());
								ex = ex.InnerException;
							}
						}
					}

					bProcessTimedOut = true;
				}

				// Should I time out because of no output?
				if ((m_fTimeOutWithoutOutputInSeconds > 0.0f) && (m_fTimeOutWithoutOutputInSeconds < ((DateTime.Now - m_obTimeOfLastOutput).TotalSeconds)))
				{
					// Time out
					// Not a very nice way to end a process, but effective.
					if (!bProcessTimedOut)
					{
						// Log time out
						string strLogMessage = "Process Timed Out because it has been " + ((DateTime.Now - m_obTimeOfLastOutput).TotalSeconds) + " seconds since last output, killing... (TimeOfLastOutput = " + m_obTimeOfLastOutput + "  Now = " + DateTime.Now + ")";
						if (LogToHtmlFile)
						{
							m_obHtmlLog.LogEvent("<font color=#FF0000>" + strLogMessage + "</font>", out obStatus);
						}
						m_obAStrLog.Add(strLogMessage);
						try
						{
							m_obProcess.CloseMainWindow();
						}
						catch (Exception ex)
						{
							System.Console.WriteLine("The following exception occurred when trying to kill the process: " + ex.ToString());
							//check the InnerException
							while (ex.InnerException != null)
							{
								System.Console.WriteLine("--------------------------------");
								System.Console.WriteLine("The following InnerException reported: " + ex.InnerException.ToString());
								ex = ex.InnerException;
							}
						}
					}
					else
					{
						// I've already tried to kill this process once and it didn't work, so bring out the big guns
						try
						{
							Console.WriteLine("Process has timed out, killing it.");
							m_obProcess.Kill();
						}
						catch (Exception ex)
						{
							System.Console.WriteLine("The following exception occurred when trying to kill the process: " + ex.ToString());
							//check the InnerException
							while (ex.InnerException != null)
							{
								System.Console.WriteLine("--------------------------------");
								System.Console.WriteLine("The following InnerException reported: " + ex.InnerException.ToString());
								ex = ex.InnerException;
							}
						}
					}

					bProcessTimedOut = true;
				}

				// Thread.Sleep(SleepTime); // sleep
                Thread.Sleep( 1 );

				// Output anything new in the log
				UpdateConsoleOutput();
			}

			// Console.WriteLine("Process has finished");

			// Kill the output reading threads
			// Give them the opportunity to catch anything still in the buffer
			do
			{
				m_obProcess.StandardOutput.BaseStream.Flush();
				m_obProcess.StandardError.BaseStream.Flush();
				Thread.Sleep(100);
				// Console.WriteLine("Looping? = " + ((!m_obProcess.StandardOutput.EndOfStream) || (!m_obProcess.StandardError.EndOfStream)));
				if (bProcessTimedOut)
				{
					break;
				}
			} while ((!m_obProcess.StandardOutput.EndOfStream) || (!m_obProcess.StandardError.EndOfStream));

			// Console.WriteLine("Killing logging threads");
			obStandardOutputThread.Abort();
			obStandardErrorThread.Abort();

			// Output anything new in the log
			UpdateConsoleOutput();

			if (bProcessTimedOut)
			{
				// Log time out
				if (LogToHtmlFile)
				{
					m_obHtmlLog.LogEvent("<font color=#FF0000>Process timed out.  Killed.</font>", out obStatus);
				}
				m_obAStrLog.Add("Error: Process timed out.  Killed.");
			}

			if (PrefixLogWithErrorsAndWarnings) AddErrorsAndWarningsPrefixToLog();
			if (LogToHtmlFile)
			{
				m_obHtmlLog.UpdateLogAfterEverySoManyLines = 1;
			}

			// Did it work?
			m_iExitCode = m_obProcess.ExitCode;
			if (m_obProcess.ExitCode != 0)
			{
				// Oh dear, something bad has happened, email someone to let them know
				Status = obStatus = new rageStatus("An error occurred executing \"" + m_strCommand + " " + m_strArgs + "\" process exited with error code " + m_obProcess.ExitCode);

				// no point going on any further
				m_obProcess.Close();
				return;
			}
			m_obProcess.Close();
			Status = obStatus = new rageStatus();
		}

		public void InitLogging()
		{
			// Open log file
			if (LogToHtmlFile && (m_obHtmlLog == null))
			{
				if (LogFilename == "")
				{
					LogFilename = GenerateLogPathAndFilename();
				}
				m_obHtmlLog = new rageHtmlLog(LogFilename, m_strCommand + " " + m_strArgs);
				m_obHtmlLog.UpdateLogAfterEverySoManyLines = UpdateLogAfterEverySoManyLines;
			}
		}

        public void Kill()
        {
            try
            {
                if (m_obProcess != null && m_obProcess.HasExited == false)
                    m_obProcess.Kill();
            }
            catch (InvalidOperationException)
            {
                //This process crashes or was killed prior.
            }
        }

		// void IDisposable.Dispose() { }
		public void Dispose()
		{
		}

		private void UpdateConsoleOutput()
		{
			for (; m_iSizeOfLogAtLastOutput < m_obAStrLog.Count; m_iSizeOfLogAtLastOutput++)
			{
				if (EchoToConsole == true)
				{
					Console.WriteLine(m_obAStrLog[m_iSizeOfLogAtLastOutput]);
				}
			}
		}

		private bool m_bPrefixLogWithErrorsAndWarnings = true;
		public bool PrefixLogWithErrorsAndWarnings
		{
			get
			{
				return m_bPrefixLogWithErrorsAndWarnings;
			}
			set
			{
				m_bPrefixLogWithErrorsAndWarnings = value;
			}
		}

		private void AddErrorsAndWarningsPrefixToLog()
		{
			if (!LogToHtmlFile) return;
			// Add error and warnings summaries
			string[] astrLogLines = m_obHtmlLog.LogEntriesAsHtmlString.Split('\n');
			bool bErrorsFound = false;
			bool bWarningsFound = false;
			StringBuilder strErrorTable = new StringBuilder();
			StringBuilder strWarningTable = new StringBuilder();
			strErrorTable.AppendLine(astrLogLines[0]);
			strErrorTable.AppendLine(astrLogLines[1]);
			strWarningTable.AppendLine(astrLogLines[0]);
			strWarningTable.AppendLine(astrLogLines[1]);
			string[] astrErrorSearchStrings = { "Error ", "error ", "Error:", "error:", "Assert", "assert", "color=#FF0000" };
			string[] astrWarningSearchStrings = { "Warning ", "warning ", "Warning:", "warning:" };
			foreach (string strLogLine in astrLogLines)
			{
				foreach (string strErrorSearchString in astrErrorSearchStrings)
				{
					if (strLogLine.Contains(strErrorSearchString))
					{
						strErrorTable.AppendLine(strLogLine);
						bErrorsFound = true;
						break;
					}
				}
				foreach (string strWarningSearchString in astrWarningSearchStrings)
				{
					if (strLogLine.Contains(strWarningSearchString))
					{
						strWarningTable.AppendLine(strLogLine);
						bWarningsFound = true;
						break;
					}
				}
			}
			strErrorTable.AppendLine("</table>");
			strWarningTable.AppendLine("</table>");

			string strPrefix = "";
			if (bErrorsFound)
			{
				strPrefix += "<h2>Errors</h2>";
				strPrefix += strErrorTable.ToString();
			}
			if (bWarningsFound)
			{
				strPrefix += "<h2>Warnings</h2>";
				strPrefix += strWarningTable.ToString();
			}
			if (strPrefix != "")
			{
				m_obHtmlLog.Prologue = strPrefix;
			}
		}

		/// <summary>
		/// Accessor function
		/// </summary>
		public List<string> GetLogAsArray() { return m_obAStrLog; }
		public string GetLogAsHtmlString() { return (m_obHtmlLog != null) ? m_obHtmlLog.LogEntriesAsHtmlString : null; }

		/// <summary>
		/// Accessor function
		/// </summary>
		public string GetDosCommand() { return (m_strWorkingDirectory.Replace("/", "\\") + "> " + m_strCommand + " " + m_strArgs); }

		/// <summary>
		/// Accessor function
		/// </summary>
		public string GetLogFilename() { return (m_obHtmlLog != null) ? m_obHtmlLog.GetFilename() : null; }

		/// <summary>
		/// Accessor function
		/// </summary>
		public int GetExitCode() { return m_iExitCode; }

		/// <summary>
		/// Function for generating a temporary filename
		/// </summary>
		private string GenerateLogPathAndFilename()
		{
			return (Environment.GetEnvironmentVariable("TEMP") + "/rageExecuteCommand_" + rageFileUtilities.GenerateTimeBasedFilename() + ".html");
		}

		/// <summary>
		/// Actually execute the command in question, but as a separate background process
		/// </summary>
		public void ExecuteInBackground()
		{
			// This uses a much simpler fire and forget launch process
			Process obProcess = new Process();
			// System.Console.WriteLine(m_strWorkingDirectory +">"+ m_strCommand +" "+ m_strArgs);
			ProcessStartInfo obProcessStartInfo = new ProcessStartInfo(m_strCommand);
			obProcessStartInfo.CreateNoWindow = true;
			obProcessStartInfo.Arguments = m_strArgs;
			if (m_strWorkingDirectory != "")
			{
				// Standardize slashes
				m_strWorkingDirectory = m_strWorkingDirectory.Replace("\\", "/");
				if (m_strWorkingDirectory[m_strWorkingDirectory.Length - 1] != '/') m_strWorkingDirectory += "/";
				obProcessStartInfo.WorkingDirectory = m_strWorkingDirectory;
			}
			//m_obProcessStartInfo.UseShellExecute = false;
			//m_obProcessStartInfo.RedirectStandardOutput = true;
			//m_obProcessStartInfo.RedirectStandardError = true;
			obProcess.StartInfo = obProcessStartInfo;

			// Kick it off
			obProcess.Start();
		}

		// StandardOutput thread
		private void ReadStandardOutput()
		{
			string strLine = "";
			//			while((strLine = m_obProcess.StandardOutput.ReadLine()) != null)
			while (true)
			{
                try
                {
                    // Got a new line, so log it
					try
					{
						strLine = m_obProcess.StandardOutput.ReadLine();
					}
					catch (Exception /*e*/)
					{
						//Console.WriteLine("Exception occurred reading stdout, ignoring : ");
						//Console.WriteLine(e.Message);
						strLine = null;
					}
                    if ( strLine != null )
                    {
                        // Log it
                        // Console.WriteLine("ReadStandardOutput : "+ strLine);
                        LogAsStdOut( strLine );
                    }
                    Thread.Sleep( 1 );
                }
                catch ( ThreadAbortException )
                {
                    break;
                }
			}
		}

		// StandardError thread
		private void ReadStandardError()
		{
			string strLine = "";
			//			while((strLine = m_obProcess.StandardError.ReadLine()) != null)
			while (true)
			{
                try
                {
                    // Got a new line, so log it
					try
					{
						strLine = m_obProcess.StandardError.ReadLine();
					}
					catch (Exception /*e*/)
					{
						//Console.WriteLine("Exception occurred reading stderr, ignoring : ");
						//Console.WriteLine(e.Message);
						strLine = null;
					}
                    if ( strLine != null )
                    {
                        // Log it 
                        // Console.WriteLine("ReadStandardError : "+ strLine);
                        LogAsStdErr( strLine );
                    }
                    Thread.Sleep( 1 );
                }
                catch ( ThreadAbortException )
                {
                    break;
                }
			}
		}

		private void LogAsStdOut(string strTextToLog)
		{
			// Log it
			lock (this)
			{
				m_obTimeOfLastOutput = System.DateTime.Now;
				if (LogToHtmlFile)
				{
					m_obHtmlLog.LogEvent(strTextToLog);
				}
				m_obAStrLog.Add(strTextToLog);
				m_obAStrStdOutLog.Add(strTextToLog);
				//m_obAStrLogList.Add(strTextToLog);
				//m_obAStrLogLinkedList.AddFirst(strTextToLog);
				//m_obAStrLogQueue.Enqueue(strTextToLog);
				//m_obAStrLogStack.Push(strTextToLog);
			}
		}

		private void LogAsStdErr(string strTextToLog)
		{
			// Log it
			lock (this)
			{
				m_obTimeOfLastOutput = System.DateTime.Now;
				if (LogToHtmlFile)
				{
					m_obHtmlLog.LogEvent("<font color=#FF0000>" + strTextToLog + "</font>");
				}
				m_obAStrLog.Add(strTextToLog);
				m_obAStrStdErrLog.Add(strTextToLog);
			}
		}

		// Member variables
		private ProcessStartInfo m_obProcessStartInfo;
		private Process m_obProcess;
		private List<string> m_obAStrLog;
		private rageHtmlLog m_obHtmlLog = null;
		private string m_strCommand;
		private string m_strArgs;
		private string m_strWorkingDirectory;
		private string m_strLogFilename;
		private int m_iExitCode;
		private float m_fTimeOutInSeconds;
		private System.DateTime m_obTimeOfLastOutput;
		private float m_fTimeOutWithoutOutputInSeconds;
		private bool m_bEchoToConsole;
		private bool m_bUseBusySpinner;
		private List<string> m_obAStrStdOutLog;
		private List<string> m_obAStrStdErrLog;
        private Dictionary<string, string> m_EnvironmentVariables;

		// Properties
		public string Command
		{
			get
			{
				return m_strCommand;
			}
			set
			{
				m_strCommand = value;
			}
		}

		public string Arguments
		{
			get
			{
				return m_strArgs;
			}
			set
			{
				m_strArgs = value;
			}
		}

		public string WorkingDirectory
		{
			get
			{
				return m_strWorkingDirectory;
			}
			set
			{
				m_strWorkingDirectory = value.Replace('\\', '/');
			}
		}

		public float TimeOutInSeconds
		{
			get
			{
				return m_fTimeOutInSeconds;
			}
			set
			{
				m_fTimeOutInSeconds = value;
			}
		}

		public float TimeOutWithoutOutputInSeconds
		{
			get
			{
				return m_fTimeOutWithoutOutputInSeconds;
			}
			set
			{
				m_fTimeOutWithoutOutputInSeconds = value;
			}
		}

		public string LogFilename
		{
			get
			{
				return m_strLogFilename;
			}
			set
			{
				RemoveLog = false;
				m_strLogFilename = value;
				if (m_strLogFilename.Length > 200)
				{
					m_strLogFilename = rageFileUtilities.RemoveFileExtension(m_strLogFilename).Substring(0, 150) + "_" + Guid.NewGuid().ToString() + ".html";
				}
			}
		}

		public int ExitCode
		{
			get
			{
				return m_iExitCode;
			}
			set
			{
				m_iExitCode = value;
			}
		}

		public bool EchoToConsole
		{
			get
			{
				return m_bEchoToConsole;
			}
			set
			{
				m_bEchoToConsole = value;
			}
		}

		public bool UseBusySpinner
		{
			get
			{
				return m_bUseBusySpinner;
			}
			set
			{
				m_bUseBusySpinner = value;
			}
		}

		private bool m_bRemoveLog;
		public bool RemoveLog
		{
			get
			{
				return m_bRemoveLog;
			}
			set
			{
				m_bRemoveLog = value;
			}
		}
		public rageHtmlLog HtmlLog
		{
			get
			{
				return m_obHtmlLog;
			}
		}

		private int m_iSizeOfLogAtLastOutput;

		public bool UpdateLogFileInRealTime
		{
			get
			{
				return (UpdateLogAfterEverySoManyLines == 1);
			}
			set
			{
				if (value)
				{
					UpdateLogAfterEverySoManyLines = 1;
				}
				else
				{
					UpdateLogAfterEverySoManyLines = 0;
				}

				if (m_obHtmlLog != null)
				{
					m_obHtmlLog.UpdateFileInRealTime = value;
				}
			}
		}
		int m_iUpdateLogAfterEverySoManyLines = 1;
		public int UpdateLogAfterEverySoManyLines
		{
			get
			{
				return m_iUpdateLogAfterEverySoManyLines;
			}
			set
			{
				m_iUpdateLogAfterEverySoManyLines = value;
			}
		}
		bool m_bLogToHtmlFile = true;
		public bool LogToHtmlFile
		{
			get
			{
				return m_bLogToHtmlFile;
			}
			set
			{
				m_bLogToHtmlFile = value;
			}
		}

		ProcessPriorityClass obPriority = ProcessPriorityClass.Normal;
		public ProcessPriorityClass Priority
		{
			get
			{
				return obPriority;
			}
			set
			{
				obPriority = value;
			}
		}

		public void SetPriority(string strPriority)
		{
			if (strPriority == "RealTime")
			{
				obPriority = ProcessPriorityClass.RealTime;
			}
			else if (strPriority == "High")
			{
				obPriority = ProcessPriorityClass.High;
			}
			else if (strPriority == "AboveNormal")
			{
				obPriority = ProcessPriorityClass.AboveNormal;
			}
			else if (strPriority == "Normal")
			{
				obPriority = ProcessPriorityClass.Normal;
			}
			else if (strPriority == "BelowNormal")
			{
				obPriority = ProcessPriorityClass.BelowNormal;
			}
			else if (strPriority == "Idle")
			{
				obPriority = ProcessPriorityClass.Idle;
			}
		}
		public string GetPriority()
		{
			switch (obPriority)
			{
				case ProcessPriorityClass.RealTime: return "RealTime";
				case ProcessPriorityClass.High: return "High";
				case ProcessPriorityClass.AboveNormal: return "AboveNormal";
				case ProcessPriorityClass.Normal: return "Normal";
				case ProcessPriorityClass.BelowNormal: return "BelowNormal";
				case ProcessPriorityClass.Idle: return "Idle";
				default: return "Normal";
			}
		}

        public void AddEnvironmentVariable(string name, string value)
        {
            m_EnvironmentVariables.Add(name, value);
        }

		rageStatus m_obStatus = new rageStatus("Hamsters will rule the world");
		public rageStatus Status
		{
			get
			{
				return m_obStatus;
			}
			set
			{
				m_obStatus = value;
			}
		}

		public List<string> StdOutLog
		{
			get
			{
				return m_obAStrStdOutLog;
			}
		}
		public List<string> StdErrLog
		{
			get
			{
				return m_obAStrStdErrLog;
			}
		}
	}
}
