﻿//---------------------------------------------------------------------------------------------
// <copyright file="ICommandLineTool.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Base.Command
{
    using System;

    /// <summary>
    /// Command-line tool interface.
    /// </summary>
    /// This was originally created for the Incredibuild and GNU make interoperability
    /// with tools; so the core pipeline can share the interface.
    /// 
    public interface ICommandLineTool
    {
        #region Properties
        /// <summary>
        /// Tool name.
        /// </summary>
        String Name { get; set; }

        /// <summary>
        /// Absolute path to tool (executable, script).
        /// </summary>
        String Path { get; set; }

        /// <summary>
        /// Command-line parameters.
        /// </summary>
        String Parameters { get; set; }
        #endregion // Properties
    }

} // RSG.Base.Command namespace
