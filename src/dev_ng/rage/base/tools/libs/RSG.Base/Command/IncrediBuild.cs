using System;
using System.IO;
using System.Collections.Generic;
using System.Xml;
using System.Text;

using RSG.Base.IO;

namespace RSG.Base.Command
{

    /// <summary>
    /// OBSOLETE: use RSG.Interop.Incredibuild instead.
    /// </summary>
    [Obsolete]
	public class rageIncrediBuild
	{
		public rageIncrediBuild()
		{
			m_strGroupUID = Guid.NewGuid().ToString();
		}

		public rageIncrediBuild(string strGroupUID)
		{
			m_strGroupUID = strGroupUID;
		}

		public void Init()
		{
			// Clean up
			rageStatus obStatus;
			rageFileUtilities.DeleteLocalFile(Environment.GetEnvironmentVariable("TEMP") + "/rageIncrediBuild_" + GroupUID + ".txt", out obStatus);
			rageFileUtilities.DeleteLocalFile(Environment.GetEnvironmentVariable("TEMP") + "/rageIncrediBuild_" + GroupUID + ".bat", out obStatus);
			rageFileUtilities.DeleteLocalFile(Environment.GetEnvironmentVariable("TEMP") + "/rageIncrediBuild_" + GroupUID + "_Launch.bat", out obStatus);
		}

		public void AddCommand(string strCommandLine)
		{
			// Append command
			if (!strCommandLine.EndsWith("\n"))
			{
				strCommandLine += "\n";
			}
			File.AppendAllText(Environment.GetEnvironmentVariable("TEMP") + "/rageIncrediBuild_" + GroupUID + ".txt", strCommandLine);
		}

		public void AddCommand(rageExecuteCommand obCommand, string strEmailProblemsTo, bool bIgnoreExitCode, string[] astrOutputFiles, 
            bool allowRemote )
		{
			// Append command
			string strCommandLine = "";
			if (obCommand.LogToHtmlFile && (obCommand.LogFilename != null) && (obCommand.LogFilename != ""))
			{
				strCommandLine += "-log \"" + obCommand.LogFilename + "\" ";
			}
			if ((strEmailProblemsTo != null) && (strEmailProblemsTo != ""))
			{
				strCommandLine += "-emailProblemsTo \"" + strEmailProblemsTo + "\" ";
			}
			if (bIgnoreExitCode)
			{
				strCommandLine += "-ignoreExitCode ";
			}
			if (obCommand.TimeOutInSeconds > 0.0f)
			{
				strCommandLine += "-timeOutInSeconds " + obCommand.TimeOutInSeconds + " ";
			}
			if (obCommand.TimeOutWithoutOutputInSeconds > 0.0f)
			{
				strCommandLine += "-timeOutWithoutOutputInSeconds " + obCommand.TimeOutWithoutOutputInSeconds + " ";
			}
			if ((obCommand.WorkingDirectory != null) && (obCommand.WorkingDirectory != ""))
			{
				strCommandLine += "-workingDirectory \"" + obCommand.WorkingDirectory + "\" ";
			}
			if ((obCommand.GetPriority() != null) && (obCommand.GetPriority() != ""))
			{
				strCommandLine += "-priority \"" + obCommand.GetPriority() + "\" ";
			}
			strCommandLine += " # ";
			strCommandLine += obCommand.Command.Replace('/', '\\');
			strCommandLine += " ";
			strCommandLine += obCommand.Arguments;
			if (!strCommandLine.EndsWith("\n"))
			{
				strCommandLine += "\n";
			}

			// Do the XML stuff
			string strXMLPathAndFilename = Environment.GetEnvironmentVariable("TEMP") + "/rageIncrediBuild_" + GroupUID + ".xml";
			XmlDocument doc = new XmlDocument();
			if (File.Exists(strXMLPathAndFilename))
			{
				// File already exists, so apend to it
				try
				{
					doc.Load(strXMLPathAndFilename);
				}
				catch (Exception ex)
				{
					Console.WriteLine("An error occurred loading " + strXMLPathAndFilename + " : " + ex.ToString());
					//check the InnerException
					while (ex.InnerException != null)
					{
						Console.WriteLine("--------------------------------");
						Console.WriteLine("The following InnerException reported: " + ex.InnerException.ToString());
						ex = ex.InnerException;
					}
					return;
				}
			}
			else
			{
				doc.LoadXml("<BuildSet FormatVersion=\"1\">" +
							"  <Environments>" +
							"    <Environment Name=\"My Environment\">" +
							"      <Tools>" +
							"      </Tools>" +
							"    </Environment>" +
							"  </Environments>" +
							"</BuildSet>");
			}

			// Add tool
			string strToolName = Path.GetFileNameWithoutExtension(obCommand.Command);
			foreach (XmlNode obDocChild in doc.ChildNodes)
			{
				if (obDocChild.Name == "BuildSet")
				{
					foreach (XmlNode obBuildSetChild in obDocChild)
					{
						if (obBuildSetChild.Name == "Environments")
						{
							foreach (XmlNode obEnvironment in obBuildSetChild.ChildNodes)
							{
								if (obEnvironment.Attributes["Name"].Value == "My Environment")
								{
									foreach (XmlNode obTools in obEnvironment.ChildNodes)
									{
										if (obTools.Name == "Tools")
										{
											bool bFoundTool = false;
											foreach (XmlNode obTool in obTools.ChildNodes)
											{
												if (obTool.Attributes["Name"].Value == strToolName)
												{
													bFoundTool = true;
													break;
												}
											}
											if (bFoundTool == false)
											{
												// Add tool
												XmlNode obToolNode = doc.CreateElement("Tool");

												XmlAttribute obNameAttr = doc.CreateAttribute("Name");
												obNameAttr.Value = strToolName;
												obToolNode.Attributes.Append(obNameAttr);

												XmlAttribute obAllowRemoteAttr = doc.CreateAttribute("AllowRemote");
                                                obAllowRemoteAttr.Value = allowRemote.ToString();
												obToolNode.Attributes.Append(obAllowRemoteAttr);

												XmlAttribute obGroupPrefixAttr = doc.CreateAttribute("GroupPrefix");
												obGroupPrefixAttr.Value = strToolName;
												obToolNode.Attributes.Append(obGroupPrefixAttr);

												XmlAttribute obPathAttr = doc.CreateAttribute("Path");
												obPathAttr.Value = Path.Combine( Environment.GetEnvironmentVariable("RS_TOOLSROOT"), 
                                                    "bin\\rageExecuteCommand.exe" );
												obToolNode.Attributes.Append(obPathAttr);

												if ((astrOutputFiles == null) || (astrOutputFiles.Length == 0))
												{
													// Not given any output files, so be on the look out for everything
													XmlAttribute obOutputFileMasksAttr = doc.CreateAttribute("OutputFileMasks");
													obOutputFileMasksAttr.Value = "*.*";
													obToolNode.Attributes.Append(obOutputFileMasksAttr);
												}

												obTools.AppendChild(obToolNode);
											}
										}
										break;
									}
									break;
								}
							}
							break;
						}
					}
					break;
				}
			}

			// Add instance of tool
			// They can all be added to the same project, BUT if there each have their own project, they can be labelled seperately, which is nice
			foreach (XmlNode obDocChild in doc.ChildNodes)
			{
				if (obDocChild.Name == "BuildSet")
				{
					// Add a project
					XmlNode obProjectNode = doc.CreateElement("Project");

					XmlAttribute obNameAttr = doc.CreateAttribute("Name");
					obNameAttr.Value = Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(obCommand.LogFilename))));
					obProjectNode.Attributes.Append(obNameAttr);

					XmlAttribute obEnvAttr = doc.CreateAttribute("Env");
					obEnvAttr.Value = "My Environment";
					obProjectNode.Attributes.Append(obEnvAttr);

					obDocChild.AppendChild(obProjectNode);

					// Add TaskGroup to Project
					XmlNode obTaskGroupNode = doc.CreateElement("TaskGroup");

					XmlAttribute obTGNameAttr = doc.CreateAttribute("Name");
					obTGNameAttr.Value = "My TaskGroup";
					obTaskGroupNode.Attributes.Append(obTGNameAttr);

					XmlAttribute obTGToolAttr = doc.CreateAttribute("Tool");
					obTGToolAttr.Value = strToolName;
					obTaskGroupNode.Attributes.Append(obTGToolAttr);

					obProjectNode.AppendChild(obTaskGroupNode);

					// Add task to task group
					XmlNode obTaskNode = doc.CreateElement("Task");
					XmlAttribute obToolAttr = doc.CreateAttribute("Tool");
					obToolAttr.Value = strToolName;
					obTaskNode.Attributes.Append(obToolAttr);

					XmlAttribute obParamsAttr = doc.CreateAttribute("Params");
					obParamsAttr.Value = strCommandLine;
					obTaskNode.Attributes.Append(obParamsAttr);

					if ((astrOutputFiles != null) && (astrOutputFiles.Length > 0))
					{
						string strOutputFiles = "";
						foreach (string strOutputFile in astrOutputFiles)
						{
							strOutputFiles += strOutputFile + ";";
						}

						XmlAttribute obOutputFilesAttr = doc.CreateAttribute("OutputFiles");
						obOutputFilesAttr.Value = strOutputFiles;
						obTaskNode.Attributes.Append(obOutputFilesAttr);
					}

					obTaskGroupNode.AppendChild(obTaskNode);

					break;
				}
			}

			// Console.WriteLine("Saving to : "+ strXMLPathAndFilename);

			doc.Save(strXMLPathAndFilename);
		}

		public int DoIt()
		{
			rageStatus obStatus;
			return DoIt(true, out obStatus);
		}

		public int DoIt(bool bShowInterface, out rageStatus obStatus)
		{
			// Do it
			rageExecuteCommand obCommand = new rageExecuteCommand();
			obCommand.Command = "xgConsole";

            obCommand.Arguments = "";
            if(bShowInterface == true)
                obCommand.Arguments += "/openmonitor ";

            obCommand.Arguments += "/ShowCmd " + 
                                    "/ShowAgent " + 
                                    "/ShowTime " +
									"/Mon=\"" + Environment.GetEnvironmentVariable("TEMP") + "/rageIncrediBuild_" + GroupUID + ".Mon.ib_mon\" " +
									"/Out=\"" + Environment.GetEnvironmentVariable("TEMP") + "/rageIncrediBuild_" + GroupUID + ".out.txt\" " +
									"\"" + Environment.GetEnvironmentVariable("TEMP") + "/rageIncrediBuild_" + GroupUID + ".xml\" ";
			obCommand.LogFilename = Environment.GetEnvironmentVariable("TEMP") + "/rageIncrediBuild_" + GroupUID + "_Launch.log.html";
			obCommand.UpdateLogAfterEverySoManyLines = 99999;
			obCommand.LogToHtmlFile = true;
			obCommand.RemoveLog = false;
			obCommand.UseBusySpinner = false;
			obCommand.EchoToConsole = true;
			obCommand.Execute(out obStatus);
			if (obCommand.LogFilename != "")
			{
				if (obStatus.Success())
				{
					obCommand.HtmlLog.LogEvent("<font color=#00FF00>Exit code : " + obCommand.ExitCode + "</font>");
				}
				else
				{
					obCommand.HtmlLog.LogEvent("<font color=#FF0000>Exit code : " + obCommand.ExitCode + "</font>");
					obCommand.HtmlLog.LogEvent("<font color=#FF0000>Exit status : " + obStatus.ErrorString + "</font>");
				}
				obCommand.UpdateLogFileInRealTime = true;
			}

            m_logAsArray = obCommand.GetLogAsArray();

			//if (!obStatus.Success())
			//{
			//    rageEmailUtilities.SendEmail("krose@rockstarsandiego.com", "IncrediBuildDoIt@rockstarsandiego.com", "IncrediBuildDoIt failure", obCommand.LogFilename);
			//}
			return obCommand.ExitCode;
		}

		string m_strGroupUID = "";
		public string GroupUID
		{
			get
			{
				return m_strGroupUID;
			}
		}

        List<string> m_logAsArray = null;
        public List<string> LogAsArray
        {
            get
            {
                return m_logAsArray;
            }
        }
	}
}
