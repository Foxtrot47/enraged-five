using System;

namespace RSG.Base.Command
{
	/// <summary>
	/// Summary description for rageStatus.
	/// </summary>
	public class rageStatus
	{
		enum eStatusType
		{
			SUCCESS,
			ERRORS,
		};

		public rageStatus()
		{
			m_strErrorMessage = "";
			m_strErrorHelpURL = "";
			m_eStatus = eStatusType.SUCCESS;
		}

		public rageStatus(string strErrorMessage)
		{
			m_strErrorMessage = strErrorMessage;
			m_strErrorHelpURL = "";
			m_eStatus = eStatusType.ERRORS;
		}

		public rageStatus(string strErrorMessage, string strErrorHelpURL)
		{
			m_strErrorMessage = strErrorMessage;
			m_strErrorHelpURL = strErrorHelpURL;
			m_eStatus = eStatusType.ERRORS;
		}

		public bool Success() {return (m_eStatus == eStatusType.SUCCESS);}
		public string GetErrorString() {return m_strErrorMessage;}
		public string GetErrorHelpUrl() {return m_strErrorHelpURL;}
		public string ErrorString
		{
			get
			{
				return m_strErrorMessage;
			}
			set
			{
				m_strErrorMessage = value;
				if(m_strErrorMessage != "")
				{
					m_eStatus = eStatusType.ERRORS;
				}
			}
		}

		// Members
		private string m_strErrorMessage;
		private string m_strErrorHelpURL;
		private eStatusType m_eStatus;
	}
}
