//
// File: cComparer.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of cComparer.cs class
//

using System;
using System.Collections.Generic;
using System.Text;

namespace RSG.Base
{

    /// <summary>
    /// Array Comparer class providing generic (template) methods for comparing
    /// different types of arrays.
    /// </summary>
    public class ArrayComparer
    {
        /// <summary>
        /// Determine whether two arrays of T are equal (element-by-element)
        /// </summary>
        /// <param name="a">First array of T</param>
        /// <param name="b">Second array of T</param>
        /// <returns>true if arrays a and b are equal, false otherwise</returns>
        public static bool Equals<T>(ref T[] a, ref T[] b)
        {
            if (a.Length != b.Length)
                return false;

            for (int nElement = 0; nElement < a.Length; ++nElement)
            {
                if (!a[nElement].Equals(b[nElement]))
                    return false;
            }

            return true;
        }
    }

} // End of 

// End of file
