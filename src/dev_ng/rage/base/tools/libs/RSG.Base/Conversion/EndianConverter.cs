﻿using System;

namespace RSG.Base.Conversion
{

    /// <summary>
    /// Simple endian byte-swapping converter (if you just know that you need
    /// to byte-swap data).
    /// </summary>
    public static class EndianConverter
    {
        #region Properties
        /// <summary>
        /// Current architecture endianness.
        /// </summary>
        public static Endianness CurrentEndianness
        {
            get;
            private set;
        }

        /// <summary>
        /// Whether current architecture is little-endian.
        /// </summary>
        public static bool IsLittleEndian
        {
            get { return (Endianness.LittleEndian == EndianConverter.CurrentEndianness); }
        }

        /// <summary>
        /// Whether current architecture is big-endian.
        /// </summary>
        public static bool IsBigEndian
        {
            get { return (Endianness.BigEndian == EndianConverter.CurrentEndianness); }
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Static constructor.
        /// </summary>
        static EndianConverter()
        {
            EndianConverter.CurrentEndianness = BitConverter.IsLittleEndian ?
                Endianness.LittleEndian : Endianness.BigEndian;
        }
        #endregion // Constructor(s)

        #region Static Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static short SwapInt16(short v)
        {
            return (short)(((v & 0xff) << 8) | ((v >> 8) & 0xff));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static ushort SwapUInt16(ushort v)
        {
            return (ushort)(((v & 0xff) << 8) | ((v >> 8) & 0xff));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static int SwapInt32(int v)
        {
            return (int)(((SwapInt16((short)v) & 0xffff) << 0x10) |
                (SwapInt16((short)(v >> 0x10)) & 0xffff));
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static uint SwapUInt32(uint v)
        {

            return (uint)(((SwapUInt16((ushort)v) & 0xffff) << 0x10) |
                (SwapUInt16((ushort)(v >> 0x10)) & 0xffff));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static long SwapInt64(long v)
        {
            return (long)(((SwapInt32((int)v) & 0xffffffffL) << 0x20) |
                (SwapInt32((int)(v >> 0x20)) & 0xffffffffL));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static ulong SwapUInt64(ulong v)
        {
            return (ulong)(((SwapUInt32((uint)v) & 0xffffffffL) << 0x20) |
                (SwapUInt32((uint)(v >> 0x20)) & 0xffffffffL));
        }
        #endregion // Static Controller Methods
    }

} // RSG.Base.Conversion namespace
