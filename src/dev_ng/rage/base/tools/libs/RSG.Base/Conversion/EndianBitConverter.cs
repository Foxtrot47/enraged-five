﻿using System;
using System.Runtime.InteropServices;
using System.Security;

namespace RSG.Base.Conversion
{

    /// <summary>
    /// 
    /// </summary>
    public abstract class EndianBitConverter
    {
        #region Properties
        /// <summary>
        /// Endianness (byte-order) in which data is converted using this class.
        /// </summary>
        public abstract bool IsLittleEndian { get; }

        /// <summary>
        /// Endianness (byte-order) in which data is converted using this class.
        /// </summary>
        public abstract Endianness Endianness { get; }

        /// <summary>
        /// Little-endian bit-converter.
        /// </summary>
        public static LittleEndianBitConverter Little
        {
            get;
            private set;
        }

        /// <summary>
        /// Big-endian bit converter.
        /// </summary>
        public static BigEndianBitConverter Big
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Static Constructor.
        /// </summary>
        static EndianBitConverter()
        {
            EndianBitConverter.Little = new LittleEndianBitConverter();
            EndianBitConverter.Big = new BigEndianBitConverter();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Converts the specified double-precision floating point number to a 64-bit
        /// signed integer.
        /// </summary>
        /// <param name="value">The number to convert.</param>
        /// <returns>A 64-bit signed integer whose value is equivalent to value.</returns>
        public long DoubleToInt64Bits(double value)
        {
            return (BitConverter.DoubleToInt64Bits(value));
        }

        /// <summary>
        /// Converts the specified 64-bit signed integer to a double-precision floating
        /// point number.
        /// </summary>
        /// <param name="value">The number to convert.</param>
        /// <returns>A double-precision floating point number whose value is equivalent to value.</returns>
        public Double Int64BitsToDouble(long value)
        {
            return (BitConverter.Int64BitsToDouble(value));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public int SingleToInt32Bits(Single value)
        {
            Int32SingleOverlapStruct s = new Int32SingleOverlapStruct(value);
            return (s.i);
        }

        /// <summary>
        /// Converts the specified 32-bit signed integer to a single-precision floating
        /// point number.
        /// </summary>
        /// <param name="value">The number to convert.</param>
        /// <returns>A single-precision floating point number whose value is equivalent to value.</returns>
        public Single Int32BitsToSingle(int value)
        {
            Int32SingleOverlapStruct s = new Int32SingleOverlapStruct(value);
            return (s.f);
        }

        /// <summary>
        /// Returns the specified Boolean value as an array of bytes.
        /// </summary>
        /// <param name="value">A Boolean value.</param>
        /// <returns>An array of bytes with length 1.</returns>
        public Byte[] GetBytes(bool value)
        {
            return BitConverter.GetBytes(value);
        }

        /// <summary>
        /// Returns the specified Unicode character value as an array of bytes.
        /// </summary>
        /// <param name="value">A character to convert.</param>
        /// <returns>An array of bytes with length 2.</returns>
        public Byte[] GetBytes(char value)
        {
            return GetBytes(value, 2);
        }

        /// <summary>
        /// Returns the specified double-precision floating point value as an array of bytes.
        /// </summary>
        /// <param name="value">The number to convert.</param>
        /// <returns>An array of bytes with length 8.</returns>
        public Byte[] GetBytes(double value)
        {
            return GetBytes(DoubleToInt64Bits(value), 8);
        }

        /// <summary>
        /// Returns the specified 16-bit signed integer value as an array of bytes.
        /// </summary>
        /// <param name="value">The number to convert.</param>
        /// <returns>An array of bytes with length 2.</returns>
        public Byte[] GetBytes(short value)
        {
            return GetBytes(value, 2);
        }

        /// <summary>
        /// Returns the specified 32-bit signed integer value as an array of bytes.
        /// </summary>
        /// <param name="value">The number to convert.</param>
        /// <returns>An array of bytes with length 4.</returns>
        public Byte[] GetBytes(int value)
        {
            return GetBytes(value, 4);
        }

        /// <summary>
        /// Returns the specified 64-bit signed integer value as an array of bytes.
        /// </summary>
        /// <param name="value">The number to convert.</param>
        /// <returns>An array of bytes with length 8.</returns>
        public Byte[] GetBytes(long value)
        {
            return GetBytes(value, 8);
        }

        /// <summary>
        /// Returns the specified single-precision floating point value as an array of bytes.
        /// </summary>
        /// <param name="value">The number to convert.</param>
        /// <returns>An array of bytes with length 4.</returns>
        public Byte[] GetBytes(float value)
        {
            return GetBytes(SingleToInt32Bits(value), 4);
        }

        /// <summary>
        /// Returns the specified 16-bit unsigned integer value as an array of bytes.
        /// </summary>
        /// <param name="value">The number to convert.</param>
        /// <returns>An array of bytes with length 2.</returns>
        public Byte[] GetBytes(ushort value)
        {
            return GetBytes(value, 2);
        }

        /// <summary>
        /// Returns the specified 32-bit unsigned integer value as an array of bytes.
        /// </summary>
        /// <param name="value">The number to convert.</param>
        /// <returns>An array of bytes with length 4.</returns>
        public Byte[] GetBytes(uint value)
        {
            return GetBytes(value, 4);
        }

        /// <summary>
        /// Returns the specified 64-bit unsigned integer value as an array of bytes.
        /// </summary>
        /// <param name="value">The number to convert.</param>
        /// <returns>An array of bytes with length 8.</returns>
        public Byte[] GetBytes(ulong value)
        {
            return GetBytes(unchecked((long)value), 8);
        }
        
        /// <summary>
        /// Returns a Boolean value converted from one byte at a specified position in
        /// a byte array.
        /// </summary>
        /// <param name="value">An array of bytes.</param>
        /// <param name="startIndex">The starting position within value.</param>
        /// <returns>true if the byte at startIndex in value is nonzero; otherwise, false.</returns>
        /// <exception cref="System.ArgumentNullException">value is null.</exception>
        /// <exception cref="System.ArgumentOutOfRangeException">startIndex is less than zero or greater than the length of value minus 1.</exception>
        public bool ToBoolean(Byte[] value, int startIndex)
        {
            CheckArgs(value, startIndex, 1);
            return (BitConverter.ToBoolean(value, startIndex));
        }
        
        /// <summary>
        /// Returns a Unicode character converted from two bytes at a specified position
        /// in a byte array.
        /// </summary>
        /// <param name="value">An array.</param>
        /// <param name="startIndex">The starting position within value.</param>
        /// <returns>A character formed by two bytes beginning at startIndex.</returns>
        /// <exception cref="System.ArgumentException">
        /// startIndex is greater than or equal to the length of value minus 7, and is
        /// less than or equal to the length of value minus 1.
        /// </exception>
        /// <exception cref="System.ArgumentNullException">value is null.</exception>
        /// <exception cref="System.ArgumentOutOfRangeException">
        /// startIndex is less than zero or greater than the length of value minus 1.
        /// </exception>
        public Char ToChar(Byte[] value, int startIndex)
        {
            return (unchecked((Char)(CheckedConvert(value, startIndex, 2))));
        }

        /// <summary>
        /// Returns a Decimal value converted from sixteen bytes at a specified
        /// position in a byte array.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="startIndex"></param>
        /// <returns></returns>
        public Decimal ToDecimal(Byte[] value, int startIndex)
        {
            // NOTE: This always assumes four parts, each in their own endianness,
            // starting with the first part at the start of the byte array.
            // On the other hand, there's no real format specified...
            int[] parts = new int[4];
            for (int i = 0; i < 4; i++)
            {
                parts[i] = ToInt32(value, startIndex + i * 4);
            }
            return new Decimal(parts);
        }

        /// <summary>
        /// Returns a single-precision floating point number converted from four bytes
        /// at a specified position in a byte array.
        /// </summary>
        /// <param name="value">An array of bytes.</param>
        /// <param name="startIndex">The starting position within value.</param>
        /// <returns>
        /// A single-precision floating point number formed by four bytes beginning at 
        /// startIndex.
        /// </returns>
        /// <exception cref="System.ArgumentException">
        /// startIndex is greater than or equal to the length of value minus 7, and is
        /// less than or equal to the length of value minus 1.
        /// </exception>
        /// <exception cref="System.ArgumentNullException">value is null.</exception>
        /// <exception cref="System.ArgumentOutOfRangeException">
        /// startIndex is less than zero or greater than the length of value minus 1.
        /// </exception>
        [SecuritySafeCritical]
        public Single ToSingle(Byte[] value, int startIndex)
        {
            return (Int32BitsToSingle(ToInt32(value, startIndex)));
        }
        
        /// <summary>
        /// Returns a double-precision floating point number converted from eight bytes
        ///  at a specified position in a byte array.
        /// </summary>
        /// <param name="value">An array of bytes.</param>
        /// <param name="startIndex">The starting position within value.</param>
        /// <returns>
        /// A double precision floating point number formed by eight bytes beginning
        /// at startIndex.
        /// </returns>
        /// <exception cref="System.ArgumentException">
        /// startIndex is greater than or equal to the length of value minus 7, and is
        /// less than or equal to the length of value minus 1.
        /// </exception>
        /// <exception cref="System.ArgumentNullException">value is null.</exception>
        /// <exception cref="System.ArgumentOutOfRangeException">
        /// startIndex is less than zero or greater than the length of value minus 1.
        /// </exception>
        [SecuritySafeCritical]
        public double ToDouble(Byte[] value, int startIndex)
        {
            return (Int64BitsToDouble(ToInt64(value, startIndex)));
        }
        
        /// <summary>
        /// Returns a 16-bit signed integer converted from two bytes at a specified position
        /// in a byte array.
        /// </summary>
        /// <param name="value">An array of bytes.</param>
        /// <param name="startIndex">The starting position within value.</param>
        /// <returns>A 16-bit signed integer formed by two bytes beginning at startIndex.</returns>
        /// <exception cref="System.ArgumentException">
        /// startIndex is greater than or equal to the length of value minus 7, and is
        /// less than or equal to the length of value minus 1.
        /// </exception>
        /// <exception cref="System.ArgumentNullException">value is null.</exception>
        /// <exception cref="System.ArgumentOutOfRangeException">
        /// startIndex is less than zero or greater than the length of value minus 1.
        /// </exception>
        [SecuritySafeCritical]
        public short ToInt16(Byte[] value, int startIndex)
        {
            return (unchecked((short)CheckedConvert(value, startIndex, 2)));
        }
        
        /// <summary>
        /// Returns a 32-bit signed integer converted from four bytes at a specified
        /// position in a byte array.
        /// </summary>
        /// <param name="value">An array of bytes.</param>
        /// <param name="startIndex">The starting position within value.</param>
        /// <returns>A 32-bit signed integer formed by four bytes beginning at startIndex.</returns>
        /// <exception cref="System.ArgumentException">
        /// startIndex is greater than or equal to the length of value minus 7, and is
        /// less than or equal to the length of value minus 1.
        /// </exception>
        /// <exception cref="System.ArgumentNullException">value is null.</exception>
        /// <exception cref="System.ArgumentOutOfRangeException">
        /// startIndex is less than zero or greater than the length of value minus 1.
        /// </exception>
        [SecuritySafeCritical]
        public int ToInt32(Byte[] value, int startIndex)
        {
            return (unchecked((int)CheckedConvert(value, startIndex, 4)));
        }
        
        /// <summary>
        /// Returns a 64-bit signed integer converted from eight bytes at a specified
        /// position in a byte array.
        /// </summary>
        /// <param name="value">An array of bytes.</param>
        /// <param name="startIndex">The starting position within value.</param>
        /// <returns>A 64-bit signed integer formed by eight bytes beginning at startIndex.</returns>
        /// <exception cref="System.ArgumentException">
        /// startIndex is greater than or equal to the length of value minus 7, and is
        /// less than or equal to the length of value minus 1.
        /// </exception>
        /// <exception cref="System.ArgumentNullException">value is null.</exception>
        /// <exception cref="System.ArgumentOutOfRangeException">
        /// startIndex is less than zero or greater than the length of value minus 1.
        /// </exception>
        [SecuritySafeCritical]
        public long ToInt64(Byte[] value, int startIndex)
        {
            return (unchecked(CheckedConvert(value, startIndex, 8)));
        }
        
        /// <summary>
        /// Converts the numeric value of each element of a specified array of bytes
        /// to its equivalent hexadecimal string representation.
        /// </summary>
        /// <param name="value">An array of bytes.</param>
        /// <returns>
        /// A string of hexadecimal pairs separated by hyphens, where each pair represents
        /// the corresponding element in value; for example, "7F-2C-4A-00".
        /// </returns>
        /// <exception cref="System.ArgumentNullException">value is null.</exception>
        public String ToString(Byte[] value)
        {
            return (BitConverter.ToString(value));
        }
        
        /// <summary>
        /// Converts the numeric value of each element of a specified subarray of bytes
        /// to its equivalent hexadecimal string representation.
        /// </summary>
        /// <param name="value">An array of bytes.</param>
        /// <param name="startIndex">The starting position within value.</param>
        /// <returns>
        /// A string of hexadecimal pairs separated by hyphens, where each pair represents
        /// the corresponding element in a subarray of value; for example, "7F-2C-4A-00".
        /// </returns>
        /// <exception cref="System.ArgumentNullException">value is null.</exception>
        /// <exception cref="System.ArgumentOutOfRangeException">
        /// startIndex is less than zero or greater than the length of value minus 1.
        /// </exception>
        public String ToString(Byte[] value, int startIndex)
        {
            return (BitConverter.ToString(value, startIndex));
        } 
        
        /// <summary>
        /// Converts the numeric value of each element of a specified subarray of bytes
        /// to its equivalent hexadecimal string representation.
        /// </summary>
        /// <param name="value">An array of bytes.</param>
        /// <param name="startIndex">The starting position within value.</param>
        /// <param name="length">The number of array elements in value to convert.</param>
        /// <returns>
        /// A string of hexadecimal pairs separated by hyphens, where each pair represents
        /// the corresponding element in a subarray of value; for example, "7F-2C-4A-00".
        /// </returns>
        /// <exception cref="System.ArgumentNullException">value is null.</exception>
        /// <exception cref="System.ArgumentOutOfRangeException">
        /// startIndex or length is less than zero.-or-startIndex is greater than zero
        /// and is greater than or equal to the length of value.
        /// </exception>
        /// <exception cref="System.ArgumentException">
        /// The combination of startIndex and length does not specify a position within
        /// value; that is, the startIndex parameter is greater than the length of value
        /// minus the length parameter.
        /// </exception>
        public String ToString(Byte[] value, int startIndex, int length)
        {
            return (BitConverter.ToString(value, startIndex, length));
        }
        
        /// <summary>
        /// Returns a 16-bit unsigned integer converted from two bytes at a specified
        /// position in a byte array.
        /// </summary>
        /// <param name="value">The array of bytes.</param>
        /// <param name="startIndex">The starting position within value.</param>
        /// <returns>A 16-bit unsigned integer formed by two bytes beginning at startIndex.</returns>
        /// <exception cref="System.ArgumentException">startIndex equals the length of value minus 1.</exception>
        /// <exception cref="System.ArgumentNullException">value is null.</exception>
        /// <exception cref="System.ArgumentOutOfRangeException">
        /// startIndex is less than zero or greater than the length of value minus 1.
        /// </exception>
        public ushort ToUInt16(Byte[] value, int startIndex)
        {         
            return (unchecked((ushort)(CheckedConvert(value, startIndex, 2))));
        }
        
        /// <summary>
        /// Returns a 32-bit unsigned integer converted from four bytes at a specified
        /// position in a byte array.
        /// </summary>
        /// <param name="value">An array of bytes.</param>
        /// <param name="startIndex">The starting position within value.</param>
        /// <returns>A 32-bit unsigned integer formed by four bytes beginning at startIndex.</returns>
        /// <exception cref="System.ArgumentException">
        /// startIndex is greater than or equal to the length of value minus 3, and is
        /// less than or equal to the length of value minus 1.
        /// </exception>
        /// <exception cref="System.ArgumentNullException">value is null.</exception>
        /// <exception cref="System.ArgumentOutOfRangeException">
        /// startIndex is less than zero or greater than the length of value minus 1.
        /// </exception>
        public uint ToUInt32(Byte[] value, int startIndex)
        {            
            return (unchecked((uint)(CheckedConvert(value, startIndex, 4))));
        }
        
        /// <summary>
        /// Returns a 64-bit unsigned integer converted from eight bytes at a specified
        /// position in a byte array.
        /// </summary>
        /// <param name="value">An array of bytes.</param>
        /// <param name="startIndex">The starting position within value.</param>
        /// <returns>A 64-bit unsigned integer formed by the eight bytes beginning at startIndex.</returns>
        /// <exception cref="System.ArgumentException">
        /// startIndex is greater than or equal to the length of value minus 7, and is
        /// less than or equal to the length of value minus 1.
        /// </exception>
        /// <exception cref="System.ArgumentNullException">value is null.</exception>
        /// <exception cref="System.ArgumentOutOfRangeException">
        /// startIndex is less than zero or greater than the length of value minus 1.
        /// </exception>
        public ulong ToUInt64(Byte[] value, int startIndex)
        {
            return (unchecked((ulong)(CheckedConvert(value, startIndex, 8))));
        }

        /// <summary>
        /// Copies the specified decimal value into the specified byte array,
        /// beginning at the specified index.
        /// </summary>
        /// <param name="value">A character to convert.</param>
        /// <param name="buffer">The byte array to copy the bytes into</param>
        /// <param name="index">The first index into the array to copy the bytes into</param>
        public void CopyBytes(decimal value, byte[] buffer, int index)
        {
            int[] parts = decimal.GetBits(value);
            for (int i = 0; i < 4; i++)
            {
                CopyBytesImpl(parts[i], 4, buffer, i * 4 + index);
            }
        }

        /// <summary>
        /// Copies the given number of bytes from the least-specific
        /// end of the specified value into the specified byte array, beginning
        /// at the specified index.
        /// This is used to implement the other CopyBytes methods.
        /// </summary>
        /// <param name="value">The value to copy bytes for</param>
        /// <param name="bytes">The number of significant bytes to copy</param>
        /// <param name="buffer">The byte array to copy the bytes into</param>
        /// <param name="index">The first index into the array to copy the bytes into</param>
        void CopyBytes(long value, int bytes, byte[] buffer, int index)
        {
            if (buffer == null)
            {
                throw new ArgumentNullException("buffer", "Byte array must not be null");
            }
            if (buffer.Length < index + bytes)
            {
                throw new ArgumentOutOfRangeException("Buffer not big enough for value");
            }
            CopyBytesImpl(value, bytes, buffer, index);
        }

        /// <summary>
        /// Copies the given number of bytes from the least-specific
        /// end of the specified value into the specified byte array, beginning
        /// at the specified index.
        /// This must be implemented in concrete derived classes, but the implementation
        /// may assume that the value will fit into the buffer.
        /// </summary>
        /// <param name="value">The value to copy bytes for</param>
        /// <param name="bytes">The number of significant bytes to copy</param>
        /// <param name="buffer">The byte array to copy the bytes into</param>
        /// <param name="index">The first index into the array to copy the bytes into</param>
        protected abstract void CopyBytesImpl(long value, int bytes, byte[] buffer, int index);

        /// <summary>
        /// Copies the specified Boolean value into the specified byte array,
        /// beginning at the specified index.
        /// </summary>
        /// <param name="value">A Boolean value.</param>
        /// <param name="buffer">The byte array to copy the bytes into</param>
        /// <param name="index">The first index into the array to copy the bytes into</param>
        public void CopyBytes(bool value, byte[] buffer, int index)
        {
            CopyBytes(value ? 1 : 0, 1, buffer, index);
        }

        /// <summary>
        /// Copies the specified Unicode character value into the specified byte array,
        /// beginning at the specified index.
        /// </summary>
        /// <param name="value">A character to convert.</param>
        /// <param name="buffer">The byte array to copy the bytes into</param>
        /// <param name="index">The first index into the array to copy the bytes into</param>
        public void CopyBytes(char value, byte[] buffer, int index)
        {
            CopyBytes(value, 2, buffer, index);
        }

        /// <summary>
        /// Copies the specified double-precision floating point value into the specified byte array,
        /// beginning at the specified index.
        /// </summary>
        /// <param name="value">The number to convert.</param>
        /// <param name="buffer">The byte array to copy the bytes into</param>
        /// <param name="index">The first index into the array to copy the bytes into</param>
        public void CopyBytes(double value, byte[] buffer, int index)
        {
            CopyBytes(DoubleToInt64Bits(value), 8, buffer, index);
        }

        /// <summary>
        /// Copies the specified 16-bit signed integer value into the specified byte array,
        /// beginning at the specified index.
        /// </summary>
        /// <param name="value">The number to convert.</param>
        /// <param name="buffer">The byte array to copy the bytes into</param>
        /// <param name="index">The first index into the array to copy the bytes into</param>
        public void CopyBytes(short value, byte[] buffer, int index)
        {
            CopyBytes(value, 2, buffer, index);
        }

        /// <summary>
        /// Copies the specified 32-bit signed integer value into the specified byte array,
        /// beginning at the specified index.
        /// </summary>
        /// <param name="value">The number to convert.</param>
        /// <param name="buffer">The byte array to copy the bytes into</param>
        /// <param name="index">The first index into the array to copy the bytes into</param>
        public void CopyBytes(int value, byte[] buffer, int index)
        {
            CopyBytes(value, 4, buffer, index);
        }

        /// <summary>
        /// Copies the specified 64-bit signed integer value into the specified byte array,
        /// beginning at the specified index.
        /// </summary>
        /// <param name="value">The number to convert.</param>
        /// <param name="buffer">The byte array to copy the bytes into</param>
        /// <param name="index">The first index into the array to copy the bytes into</param>
        public void CopyBytes(long value, byte[] buffer, int index)
        {
            CopyBytes(value, 8, buffer, index);
        }

        /// <summary>
        /// Copies the specified single-precision floating point value into the specified byte array,
        /// beginning at the specified index.
        /// </summary>
        /// <param name="value">The number to convert.</param>
        /// <param name="buffer">The byte array to copy the bytes into</param>
        /// <param name="index">The first index into the array to copy the bytes into</param>
        public void CopyBytes(float value, byte[] buffer, int index)
        {
            CopyBytes(SingleToInt32Bits(value), 4, buffer, index);
        }

        /// <summary>
        /// Copies the specified 16-bit unsigned integer value into the specified byte array,
        /// beginning at the specified index.
        /// </summary>
        /// <param name="value">The number to convert.</param>
        /// <param name="buffer">The byte array to copy the bytes into</param>
        /// <param name="index">The first index into the array to copy the bytes into</param>
        public void CopyBytes(ushort value, byte[] buffer, int index)
        {
            CopyBytes(value, 2, buffer, index);
        }

        /// <summary>
        /// Copies the specified 32-bit unsigned integer value into the specified byte array,
        /// beginning at the specified index.
        /// </summary>
        /// <param name="value">The number to convert.</param>
        /// <param name="buffer">The byte array to copy the bytes into</param>
        /// <param name="index">The first index into the array to copy the bytes into</param>
        public void CopyBytes(uint value, byte[] buffer, int index)
        {
            CopyBytes(value, 4, buffer, index);
        }

        /// <summary>
        /// Copies the specified 64-bit unsigned integer value into the specified byte array,
        /// beginning at the specified index.
        /// </summary>
        /// <param name="value">The number to convert.</param>
        /// <param name="buffer">The byte array to copy the bytes into</param>
        /// <param name="index">The first index into the array to copy the bytes into</param>
        public void CopyBytes(ulong value, byte[] buffer, int index)
        {
            CopyBytes(unchecked((long)value), 8, buffer, index);
        }
        #endregion // Controller Methods
        
        #region Private/Protected Methods
        /// <summary>
        /// Validate arguments prior to converting specified subrange of Byte array.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="startIndex"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">value is null.</exception>
        /// <exception cref="System.ArgumentOutOfRangeException">
        /// startIndex is less than zero or off the end of the specified array.
        /// </exception>
        private long CheckedConvert(Byte[] value, int startIndex, int count)
        {
            CheckArgs(value, startIndex, count);
            return (FromBytes(value, startIndex, count));
        }

        /// <summary>
        /// Validate arguments.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="startIndex"></param>
        /// <param name="count"></param>
        private void CheckArgs(Byte[] value, int startIndex, int count)
        {
            if (null == value)
                throw (new ArgumentNullException("value"));
            else if (startIndex < 0 || startIndex > (value.Length - count))
                throw (new ArgumentOutOfRangeException("startIndex"));
        }

        /// <summary>
        /// Returns an array with the given number of bytes formed
        /// from the least significant bytes of the specified value.
        /// This is used to implement the other GetBytes methods.
        /// </summary>
        /// <param name="value">The value to get bytes for</param>
        /// <param name="bytes">The number of significant bytes to return</param>
        private Byte[] GetBytes(long value, int bytes)
        {
            Byte[] buffer = new byte[bytes];
            CopyBytes(value, bytes, buffer, 0);
            return buffer;
        }

        /// <summary>
        /// Convert specified subrange of value array.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="startIndex"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        protected abstract long FromBytes(Byte[] value, int startIndex, int count);

        /// <summary>
        /// Structure to define overlapped Int32/Single data.
        /// </summary>
        [StructLayout(LayoutKind.Explicit)]
        struct Int32SingleOverlapStruct
        {
            [FieldOffset(0)]
            public int i;

            [FieldOffset(0)]
            public float f;

            internal Int32SingleOverlapStruct(int i)
            {
                this.f = 0.0f;
                this.i = i;
            }

            internal Int32SingleOverlapStruct(float f)
            {
                this.i = 0;
                this.f = f;
            }
        }
        #endregion // Private/Protected Methods
    }
    
} // RSG.Base.Conversion namespace
