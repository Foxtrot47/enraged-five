﻿using System;

namespace RSG.Base.Conversion
{

    /// <summary>
    /// EndianBitConverter that converts to/from big-endian byte arrays.
    /// </summary>
    public class BigEndianBitConverter : EndianBitConverter
    {
        #region Properties
        /// <summary>
        /// Endianness (byte-order) in which data is converted using this class.
        /// </summary>
        public sealed override bool IsLittleEndian 
        {
            get { return (false); }
        }

        /// <summary>
        /// Endianness (byte-order) in which data is converted using this class.
        /// </summary>
        public sealed override Endianness Endianness 
        {
            get { return (Endianness.BigEndian); } 
        }
        #endregion // Properties

        #region EndianBitConverter Overridden Methods
        /// <summary>
        /// Copies the specified number of bytes from value to buffer, starting at index.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="bytes"></param>
        /// <param name="buffer"></param>
        /// <param name="index"></param>
        protected override void CopyBytesImpl(long value, int bytes, Byte[] buffer, int index)
        {
            int endOffset = index + bytes - 1;
            for (int i = 0; i < bytes; i++)
            {
                buffer[endOffset - i] = unchecked((byte)(value & 0xff));
                value = value >> 8;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="startIndex"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        protected override long FromBytes(Byte[] buffer, int startIndex, int count)
        {
            long ret = 0;
            for (int i = 0; i < count; i++)
            {
                ret = unchecked((ret << 8) | buffer[startIndex + i]);
            }
            return (ret);
        }
        #endregion // EndianBitConverter Overridden Methods
    }

} // RSG.Base.Conversion namespace
