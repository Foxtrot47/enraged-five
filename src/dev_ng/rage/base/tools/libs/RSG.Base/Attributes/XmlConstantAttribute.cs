﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Attributes
{

    /// <summary>
    /// Attribute for marking classes/enums as having an XML string constant.
    /// </summary>
    public class XmlConstantAttribute : Attribute
    {
        #region Properties
        /// <summary>
        /// Constant string defined.
        /// </summary>
        public String Constant { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        public XmlConstantAttribute(String value)
        {
            this.Constant = value;
        }
        #endregion // Constructor(s)
    }

} // RSG.Base.Attributes namespace
