﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Media.Imaging;

namespace RSG.Base.Attributes
{
    /// <summary>
    /// Specifies the color in which to display an object in the UI.
    /// </summary>
    [AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
    public class DisplayIconAttribute : Attribute
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="assemblyName">Name of the assembly in which the resource lives.
        /// If this isn't provided it uses the "current" assembly.</param>
        /// <param name="resourceName">Name of the resource to use.</param>
        public DisplayIconAttribute(String assemblyName, String resourceName)
        {
            m_assemblyName = assemblyName;
            m_resourceName = resourceName;
        }

        /// <summary>
        /// Constructor with icon passed in directly
        /// </summary>
        public DisplayIconAttribute(BitmapSource sourceIcon)
        {
            m_icon = sourceIcon;
        }

        /// <summary>
        /// Gets the icon associated with this attribute.
        /// (Lazily constructed to prevent overhead when it isn't needed.)
        /// </summary>
        public BitmapSource Icon
        {
            get
            {
                if (m_icon == null)
                {
                    lock (m_syncRoot)
                    {
                        if (m_icon == null)
                        {
                            m_icon = LoadIcon(m_resourceName);
                        }
                    }
                }
                return m_icon;
            }
        }
        private BitmapSource m_icon;
        private String m_resourceName;
        private String m_assemblyName;
        private static object m_syncRoot = new object();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private BitmapSource LoadIcon(String name)
        {
            String assemblyName = m_assemblyName;
            if (String.IsNullOrEmpty(assemblyName))
            {
                assemblyName = typeof(DisplayIconAttribute).Assembly.GetName().Name;
            }
            String bitmapPath = String.Format(CultureInfo.InvariantCulture,
                "pack://application:,,,/{0};component/{1}", assemblyName, m_resourceName);
            Uri uri = new Uri(bitmapPath, UriKind.RelativeOrAbsolute);

            BitmapSource source = new BitmapImage(uri);
            source.Freeze();
            return source;
        }
    } // DisplayColorAttribute
}
