﻿using System;

namespace RSG.Base.Attributes
{

    /// <summary>
    /// Attribute for specifying the friendly-name for a field.
    /// </summary>
    [Obsolete("Should be using either DisplayNameAttribute or FieldDisplayNameAttribute instead.")]
    public class FriendlyNameAttribute : Attribute
    {
        #region Properties
        /// <summary>
        /// Friendly name for the field.
        /// </summary>
        public String FriendlyName
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor; taking friendly name string.
        /// </summary>
        /// <param name="friendly_name"></param>
        public FriendlyNameAttribute(String friendly_name)
        {
            this.FriendlyName = friendly_name;
        }
        #endregion // Constructor(s)
    }

} // RSG.Base.Attributes
