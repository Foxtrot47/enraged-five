﻿using System;

namespace RSG.Base.Attributes
{

    /// <summary>
    /// Attribute for associating a Registry key to something
    /// </summary>
    [AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
    public class RegistryKeyAttribute : Attribute
    {
        public RegistryKeyAttribute(String key)
            : this(key, true)
        {
        }

        public RegistryKeyAttribute(String key, bool is64bit)
        {
            this.Key = key;
            this.Is64Bit = is64bit;
        }

        public String Key
        {
            get;
            private set;
        }

        public bool Is64Bit
        {
            get;
            private set;
        }
    }

} // RSG.Base.Attributes namespace
