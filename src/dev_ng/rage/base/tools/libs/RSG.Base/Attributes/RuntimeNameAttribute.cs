﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Attributes
{
    /// <summary>
    /// 
    /// </summary>
    public class RuntimeNameAttribute : Attribute
    {
        #region Properties
        /// <summary>
        /// Metric name string assigned to enum value.
        /// </summary>
        public string Name
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="name"></param>
        public RuntimeNameAttribute(string name)
        {
            this.Name = name;
        }
        #endregion // Constructor(s)
    } // RuntimeNameAttribute
}
