﻿//---------------------------------------------------------------------------------------------
// <copyright file="FieldGuidAttribute.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Base.Attributes
{
    using System;

    /// <summary>
    /// Supplies an explicit GUID when an automatic GUID is undesirable.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, Inherited = false, AllowMultiple = false)]
    public sealed class FieldGuidAttribute : Attribute
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Value"/> property.
        /// </summary>
        private string _value;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="FieldGuidAttribute"/> class with the
        /// specified GUID.
        /// </summary>
        /// <param name="guid">
        /// The GUID to be assigned.
        /// </param>
        public FieldGuidAttribute(string guid)
        {
            this._value = guid;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the GUID of the target.
        /// </summary>
        public string Value
        {
            get { return this._value; }
        }
        #endregion Properties
    } // RSG.Base.Attributes.FieldGuidAttribute {Class}
} // RSG.Base.Attributes {Namespace}
