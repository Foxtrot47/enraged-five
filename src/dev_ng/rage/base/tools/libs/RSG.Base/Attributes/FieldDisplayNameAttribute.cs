﻿//---------------------------------------------------------------------------------------------
// <copyright file="FieldDisplayNameAttribute.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Base.Attributes
{
    using System;
    using System.ComponentModel;

    /// <summary>
    /// Specifies the display name for a field.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, Inherited = false, AllowMultiple = false)]
    public sealed class FieldDisplayNameAttribute : DisplayNameAttribute
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="FieldDisplayNameAttribute"/> class
        /// using the specified display name.
        /// </summary>
        /// <param name="displayName">
        /// The display name.
        /// </param>
        public FieldDisplayNameAttribute(string displayName)
            : base(displayName)
        {
        }
        #endregion Constructors
    } // RSG.Base.Attributes.FieldDisplayNameAttribute {Class}
} // RSG.Base.Attributes {Namespace}
