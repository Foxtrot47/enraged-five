﻿//---------------------------------------------------------------------------------------------
// <copyright file="AssemblyHelpLinkAttribute.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Base.Attributes
{
    using System;

    /// <summary>
    /// Supplies an explicit GUID when an automatic GUID is undesirable.
    /// </summary>
    [AttributeUsage(AttributeTargets.Assembly, Inherited = false)]
    public sealed class AssemblyHelpLinkAttribute : Attribute
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="HelpLink"/> property.
        /// </summary>
        private string _helpLink;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="AssemblyHelpLinkAttribute"/> class
        /// with the specified help link.
        /// </summary>
        /// <param name="guid">
        /// The string containing the help link to be assigned.
        /// </param>
        public AssemblyHelpLinkAttribute(string helpLink)
        {
            this._helpLink = helpLink;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the help link of the taret.
        /// </summary>
        public string HelpLink
        {
            get { return this._helpLink; }
        }
        #endregion Properties
    } // RSG.Base.Attributes.AssemblyHelpLinkAttribute {Class}
} // RSG.Base.Attributes {Namespace}
