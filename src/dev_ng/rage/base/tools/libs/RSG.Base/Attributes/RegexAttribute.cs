﻿using System.Text.RegularExpressions;

namespace RSG.Base.Attributes
{
    /// <summary>
    /// Attribute for associating a regex to something
    /// </summary>
    public class RegexAttribute : System.Attribute
    {
        public RegexAttribute(Regex regex)
        {
            Regex = regex;
        }

        public RegexAttribute(string regexString)
        {
            Regex = new Regex(regexString);
        }

        public Regex Regex
        {
            get;
            set;
        }
    } // RegexAttribute
} // RSG.Base.Attributes
