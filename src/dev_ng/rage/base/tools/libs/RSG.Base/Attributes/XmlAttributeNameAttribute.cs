﻿//---------------------------------------------------------------------------------------------
// <copyright file="XmlAttributeNameAttribute.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Base.Attributes
{
    using System;

    /// <summary>
    /// Specifies the name of the xml attribute for a property, or field.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    public class XmlAttributeNameAttribute : Attribute
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="XmlAttributeName"/> property.
        /// </summary>
        private string _xmlAttributeName;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="XmlAttributeNameAttribute" /> class.
        /// </summary>
        /// <param name="xmlAttributeName">
        /// The xml attribute name.
        /// </param>
        public XmlAttributeNameAttribute(string xmlAttributeName)
        {
            this._xmlAttributeName = xmlAttributeName;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the xml name for the property, or field stored in this attribute.
        /// </summary>
        public virtual string XmlAttributeName
        {
            get { return this._xmlAttributeName; }
        }
        #endregion Properties
    } // RSG.Base.Attributes.XmlAttributeNameAttribute {Enum}
} // RSG.Base.Attributes {Namespace}
