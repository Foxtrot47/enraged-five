﻿using System;
using System.IO;

namespace RSG.Base.Attributes
{

    /// <summary>
    /// Attribute for marking the user profile config data path.
    /// </summary>
    [AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
    public class UserConfigPathAttribute : Attribute
    {
        public UserConfigPathAttribute(Environment.SpecialFolder folder, String path, bool is64bit)
        {
            this.Folder = folder;
            this.Path = path;
            this.Is64Bit = is64bit;
        }

        public Environment.SpecialFolder Folder
        {
            get;
            private set;
        }

        public String Path
        {
            get;
            private set;
        }

        public bool Is64Bit
        {
            get;
            private set;
        }

        public String GetFullPath()
        {
            return (System.IO.Path.Combine(Environment.GetFolderPath(this.Folder), this.Path));
        }
    }

} // RSG.Base.Attributes namespace
