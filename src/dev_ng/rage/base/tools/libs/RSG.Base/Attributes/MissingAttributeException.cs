﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Attributes
{
    /// <summary>
    /// Exception that can be thrown when an expected attribute is missing from a class member.
    /// </summary>
    [Serializable]
    public class MissingAttributeException : MemberAccessException
    {
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="MissingAttributeExcetion"/> class.
        /// </summary>
        public MissingAttributeException()
            : base()
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="MissingAttributeExcetion"/> class with a specified error message.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public MissingAttributeException(String message)
            : base(message)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="MissingAttributeExcetion"/> class with the member name that it
        /// is missing from and the attribute type that is missing.
        /// </summary>
        /// <param name="attributeType">Type of the attribute that is missing.</param>
        /// <param name="memberName">Name of the member that the attribute is missing from.</param>
        public MissingAttributeException(String memberName, Type attributeType)
            : base(String.Format("The '{0}' attribute is missing from the '{1}' field.", attributeType.Name, memberName))
        {
        }
        #endregion // Constructor(s)
    } // MissingAttributeException
}
