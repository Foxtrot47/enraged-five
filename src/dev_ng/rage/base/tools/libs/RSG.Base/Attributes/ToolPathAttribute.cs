﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Attributes
{

    /// <summary>
    /// Attribute for marking the tools install path.
    /// </summary>
    public class ToolPathAttribute : Attribute
    {
        public ToolPathAttribute(String path)
        {
            this.Path = path;
        }

        public String Path
        {
            get;
            private set;
        }
    }

} // RSG.Base.Attributes namespace
