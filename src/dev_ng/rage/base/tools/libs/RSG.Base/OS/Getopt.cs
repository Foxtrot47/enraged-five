//---------------------------------------------------------------------------------------------
// <copyright file="Getopt.cs" company="Rockstar Games">
//     Copyright � Rockstar Games 2007-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Base.OS
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Command line argument parser.
    /// </summary>
    public class Getopt
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Argv string array specified in constructor
        /// </summary>
        public String[] Arguments { get { return m_Arguments; } }
        private String[] m_Arguments;

        /// <summary>
        /// Available options array (specified to constructor)
        /// </summary>
        public LongOption[] AvailableOptions { get { return m_AvailableOptions; } }
        private LongOption[] m_AvailableOptions;

        /// <summary>
        /// Options after parsing
        /// </summary>
        public Dictionary<String, Object> Options
        {
            get { return m_Options; }
        }
        private Dictionary<String, Object> m_Options;

        /// <summary>
        /// Trailing options
        /// </summary>
        public String[] TrailingOptions
        {
            get;
            private set;
        }
        #endregion // Properties and Associated Member Data

        #region Member Data
        System.Collections.Hashtable m_ShortOpts;
        System.Collections.Hashtable m_LongOpts;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="args">Argv array as from Main</param>
        /// <param name="options">Array of option objects</param>
        /// <param name="throwIfArgNotSupported">
        /// Flag indicating whether we should throw if any of the supplied args aren't
        /// valid.
        /// </param>
        /// <exception cref="System.ArgumentException"/>
        public Getopt(String[] args, LongOption[] options, bool throwIfArgNotSupported = true)
        {
            this.m_Arguments = args;
            this.m_AvailableOptions = options;
            this.m_ShortOpts = new System.Collections.Hashtable();
            this.m_LongOpts = new System.Collections.Hashtable();
            // Populate hash tables for quick lookup
            foreach (LongOption longopt in this.m_AvailableOptions)
            {
                if (String.IsNullOrEmpty(longopt.Longopt))
                {
                    String message = String.Format("The longhand option {0} is empty.", longopt.Longopt);
                    Debug.Assert(false, message);
                    throw (new ArgumentException(message, "options"));
                }
                else if (this.m_LongOpts.ContainsKey(longopt.Longopt) == false)
                {
                    this.m_LongOpts.Add(longopt.Longopt, longopt.ArgumentSpec);
                }
                else
                {
                    //Ignore this option; it is already reserved.
                    String message = String.Format("The longhand option '{0}' is already reserved.  The '{1}' option will be ignored.",
                        longopt.Longopt, longopt.Longopt);
                    Debug.Fail(message);
                }
            }

            this.m_Options = new Dictionary<String, Object>();
            this.TrailingOptions = new String[0];
            Parse(throwIfArgNotSupported);
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// String long option argument indexer.
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        public Object this[String arg]
        {
            get
            {
                LongOption opt = null;
                foreach (LongOption o in AvailableOptions)
                {
                    if (o.Longopt == arg)
                        opt = o;
                }
                if (null != opt)
                {
                    if (this.Options.ContainsKey(opt.Longopt))
                        return (this.Options[opt.Longopt]);
                    else return (null);
                }
                return (null);
            }
            // No set accessor.
        }

        /// <summary>
        /// Determine whether we have a specific long option argument specified.
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        public bool HasOption(String arg)
        {
            return (this.Options.ContainsKey(arg));
        }

        /// <summary>
        /// Return usage strings.
        /// </summary>
        /// <returns></returns>
        public String Usage()
        {
            return (this.Usage(String.Empty, String.Empty));
        }

        /// <summary>
        /// Return usage strings.
        /// </summary>
        /// <param name="trailingArgName"></param>
        /// <param name="trailingArgDescription"></param>
        /// <returns></returns>
        public String Usage(String trailingArgName, String trailingArgDescription)
        {
            int longestOptionLen = this.AvailableOptions.Max(opt => opt.Longopt.Length);
            StringBuilder sb = new StringBuilder();
            foreach (LongOption opt in this.AvailableOptions)
            {
                if (String.IsNullOrEmpty(opt.Description))
                    continue; // Skip; hide option if no description.

                if (opt.Longopt.Length > longestOptionLen)
                    longestOptionLen = opt.Longopt.Length;

                sb.Append("\t");
                sb.AppendFormat("--{0}", opt.Longopt.PadRight(longestOptionLen));
                sb.Append("\t");
                sb.AppendLine(opt.Description);
            }

            // Trailing argument information.
            if (!String.IsNullOrEmpty(trailingArgName) && !String.IsNullOrEmpty(trailingArgDescription))
            {
                sb.Append("\t");
                sb.AppendFormat("{0} ...", trailingArgName);
                sb.Append("\t");
                sb.AppendFormat(trailingArgDescription);
            }

            return (sb.ToString());
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Parse specified command line arguments and populate Options dict.
        /// </summary>
        /// <param name="throwIfArgNotSupported">
        /// Flag indicating whether we should throw if any of the supplied args aren't
        /// valid.
        /// </param>
        /// <exception cref="System.ArgumentException"/>
        private void Parse(bool throwIfArgNotSupported)
        {
            List<String> trailingArguments = new List<String>();
            bool trailingOpts = false;
            for (int nArg = 0; nArg < this.m_Arguments.Length; ++nArg)
            {
                String arg = this.m_Arguments[nArg];
                if (!trailingOpts && !arg.StartsWith("//") && (arg.StartsWith("/") || arg.StartsWith("--") || arg.StartsWith("-")))
                {
                    int prefixLen = 1;
                    if (arg.StartsWith("--"))
                        prefixLen = 2; 
                    String argname = arg.Substring(prefixLen);

                    LongOption.ArgType optArg;
                    if ((1 == argname.Length) && this.m_ShortOpts.ContainsKey(argname[0]))
                    {
                        optArg = (LongOption.ArgType)this.m_ShortOpts[argname[0]];
                        Debug.Assert(false, String.Format("Getopt recognised a short option ({0}).  Long options are currently prefered and are more consistent.", arg));
                    }
                    else if (this.m_LongOpts.ContainsKey(argname))
                    {
                        optArg = (LongOption.ArgType)this.m_LongOpts[argname];
                    }
                    else
                    {
                        if (throwIfArgNotSupported)
                            throw new ArgumentException(String.Format("Argument {0} not found.", argname));
                        else
                            continue;
                    }

                    switch (optArg)
                    {
                        case LongOption.ArgType.None:
                            // Set argument to true and continue
                            this.m_Options[argname] = true;
                            break;
                        case LongOption.ArgType.Optional:
                            {
                                // See if we have argument, otherwise set to true
                                String nextArg = (nArg + 1 < m_Arguments.Length) ? m_Arguments[nArg + 1] : "";
                                if ((nextArg.Length > 0) &&
                                    !(nextArg.StartsWith("--") || nextArg.StartsWith("-") || nextArg.StartsWith("/")))
                                {
                                    this.m_Options[argname] = nextArg;
                                    ++nArg;
                                }
                                else
                                    this.m_Options[argname] = true;
                            }
                            break;
                        case LongOption.ArgType.Required:
                            {
                                // See if we have argument, otherwise raise exception
                                String nextArg = (nArg + 1 < m_Arguments.Length) ? m_Arguments[nArg + 1] : "";
                                if ((nextArg.Length > 0) &&
                                    !(nextArg.StartsWith("--") || nextArg.StartsWith("-") || nextArg.StartsWith("/")))
                                {
                                    this.m_Options[argname] = nextArg;
                                    ++nArg;
                                }
                                else
                                    throw new ArgumentException(String.Format("Argument {0} requires parameter.", argname));
                            }
                            break;
                    }
                }
                else
                {
                    // Otherwise we've switching to trailing options parsing,
                    // so whack it into the TrailingOptions array.
                    trailingArguments.Add(arg);
                    trailingOpts = true;
                }
            }
            this.TrailingOptions = trailingArguments.ToArray();
        }
        #endregion // Private Methods
    }

    /// <summary>
    /// 
    /// </summary>
    public class LongOption
    {
        #region Enumerations
        /// <summary>
        /// Option argument enumeration
        /// </summary>
        public enum ArgType
        {
            None,       // No argument option
            Optional,   // Optional argument option
            Required    // Required argument option
        }
        #endregion // Enumerations

        #region Member Data
        public ArgType ArgumentSpec;
        public String Longopt;
        public Char Shortopt;
        public String Description;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="longopt">Long option string</param>
        /// <param name="optarg">Option arguments</param>
        /// <param name="shortopt">Short option character</param>
        [Obsolete("Use non-short option version.", true)]
        public LongOption(String longopt, ArgType optarg, Char shortopt)
            : this(longopt, optarg, shortopt, String.Empty)
        {
        }

        /// <summary>
        /// Constructor; specifying optional description string.
        /// </summary>
        /// <param name="longopt"></param>
        /// <param name="optarg"></param>
        /// <param name="shortopt"></param>
        /// <param name="desc"></param>
        [Obsolete("Use non-short option version.", true)]
        public LongOption(String longopt, ArgType optarg, Char shortopt, String desc)
        {
            this.Longopt = longopt;
            this.Shortopt = shortopt;
            this.ArgumentSpec = optarg;
            this.Description = desc;
        }

        /// <summary>
        /// Constructor; specifying optional description string.
        /// </summary>
        /// <param name="longopt"></param>
        /// <param name="optarg"></param>
        /// <param name="desc"></param>
        public LongOption(String longopt, ArgType optarg, String desc)
        {
            this.Longopt = longopt;
            this.Shortopt = Char.MinValue;
            this.ArgumentSpec = optarg;
            this.Description = desc;
        }
        #endregion // Constructor(s)
    }

} // RSG.Base.OS namespace

// Getopt.cs
