﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.OS
{
    /// <summary>
    /// Process exit code constants.
    /// </summary>
    public static class ExitCode
    {
        /// <summary>
        /// Console application successful exit return code.
        /// </summary>
        public const int Success = 0;

        /// <summary>
        /// Console application failure exit return code.
        /// </summary>
        public const int Failure = 1;

        /// <summary>
        /// Bit flag to indicate that a single instance of an application is already
        /// running.
        /// </summary>
        public const int SingleInstanceRunning = (1 << 29);

        /// <summary>
        /// Bit flag to indicate the application wishes to resume after exit.
        /// </summary>
        public const int Restart = (1 << 30);
    }
}
