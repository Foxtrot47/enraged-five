﻿using System;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography;

namespace RSG.Base.IO
{

    /// <summary>
    /// Base64 encoding utility functions.
    /// </summary>
    public static class Base64EncodeDecode
    {

        /// <summary>
        /// Base64 encode an input stream to an output stream.
        /// </summary>
        /// <param name="input"></param>
        /// <param name="bufferChunkSize"></param>
        /// <returns></returns>
        public static Byte[] Encode(Stream input, long bufferChunkSize = 32768)
        {
            using (ToBase64Transform transform = new ToBase64Transform())
            using (MemoryStream memStream = new MemoryStream())
            using (CryptoStream st = new CryptoStream(memStream, transform, CryptoStreamMode.Write))
            {
                Byte[] inputBuffer = new Byte[bufferChunkSize];
                int bytesRead = 0;
                while ((bytesRead = input.Read(inputBuffer, 0, inputBuffer.Length)) > 0)
                {
                    st.Write(inputBuffer, 0, bytesRead);
                }
                st.FlushFinalBlock();
                return (memStream.ToArray());
            }
        }

        /// <summary>
        /// Base64 decode an input stream to an output stream.
        /// </summary>
        /// <param name="output"></param>
        /// <param name="input"></param>
        /// <param name="bufferChunkSize"></param>
        public static void Decode(Stream output, Stream input, long bufferChunkSize = 32768)
        {
            throw (new NotImplementedException());
        }
    }

} // RSG.Base.IO namespace
