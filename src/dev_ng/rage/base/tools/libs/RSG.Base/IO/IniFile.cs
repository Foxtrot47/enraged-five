﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace RSG.Base.IO
{

    /// <summary>
    /// Class for reading/writing simple .ini files.
    /// </summary>
    public class IniFile
    {
        #region Member Data
        /// <summary>
        /// Data storage; section name => key => value.
        /// </summary>
        private IDictionary<String, IDictionary<String, String>> m_data;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor (empty .ini file).
        /// </summary>
        public IniFile()
        {
            this.m_data = new Dictionary<String, IDictionary<String, String>>();
        }
        #endregion // Constructor(s)

        #region Static Controller Methods
        /// <summary>
        /// Load an ini file.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static IniFile Load(String filename)
        {
            IniFile iniFile = new IniFile();

            String txt = File.ReadAllText(filename);
            Dictionary<String, String> currentSection = new Dictionary<String, String>(StringComparer.InvariantCultureIgnoreCase);
            iniFile.m_data[String.Empty] = currentSection;

            foreach (String line in txt.Split(new[] { "\n" }, StringSplitOptions.RemoveEmptyEntries)
                                   .Where(t => !String.IsNullOrWhiteSpace(t))
                                   .Select(t => t.Trim()))
            {
                if (line.StartsWith(";"))
                    continue;

                if (line.StartsWith("[") && line.EndsWith("]"))
                {
                    currentSection = new Dictionary<String, String>(StringComparer.InvariantCultureIgnoreCase);
                    iniFile.m_data[line.Substring(1, line.LastIndexOf("]") - 1)] = currentSection;
                    continue;
                }

                int idx = line.IndexOf("=");
                if (idx == -1)
                    currentSection[line] = "";
                else
                    currentSection[line.Substring(0, idx).Trim()] = line.Substring(idx + 1).Trim();
            }
            return (iniFile);
        }
        #endregion // Static Controller Methods

        #region Controller Methods
        /// <summary>
        /// Save state of ini file to disk.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="options"></param>
        public void Save(String filename, IniFileSaveOption options)
        {
            using (FileStream fs = new FileStream(filename, FileMode.Create, FileAccess.Write))
            {
                Save(fs, options);
            }
        }

        /// <summary>
        /// Save state of ini file to stream.
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="options"></param>
        public void Save(Stream stream, IniFileSaveOption options)
        {
            Debug.Assert(stream.CanWrite, "Cannot write to stream.");

            StringBuilder sb = new StringBuilder();
            
            // Write empty section first.
            if (this.m_data.ContainsKey(String.Empty))
                SaveSection(String.Empty, this.m_data[String.Empty], stream, options);

            foreach (KeyValuePair<String, IDictionary<String, String>> section in this.m_data)
            {
                if (String.IsNullOrEmpty(section.Key))
                    continue;
                SaveSection(section.Key, section.Value, stream, options);
            }
        }

        /// <summary>
        /// Return value for a specified key (not in a section).
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public String GetValue(String key)
        {
            return (GetValue(key, String.Empty, String.Empty));
        }

        /// <summary>
        /// Return value for a specified key, in a specified section.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="section"></param>
        /// <returns></returns>
        public String GetValue(String key, String section)
        {
            return (GetValue(key, section, String.Empty));
        }

        /// <summary>
        /// Return value for a specified key, in a specified section.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="section"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public String GetValue(String key, String section, String defaultValue)
        {
            if (!this.m_data.ContainsKey(section))
                return (defaultValue);

            if (!this.m_data[section].ContainsKey(key))
                return (defaultValue);

            return (this.m_data[section][key]);
        }

        /// <summary>
        /// Return all key names from a specified section.
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        public IEnumerable<String> GetKeys(String section)
        {
            if (!this.m_data.ContainsKey(section))
                return (new String[0]);

            return (this.m_data[section].Keys);
        }

        /// <summary>
        /// Return all section names.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<String> GetSections()
        {
            return (this.m_data.Keys.Where(t => t != String.Empty));
        }

        /// <summary>
        /// Set value.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="section"></param>
        /// <param name="value"></param>
        public void SetValue(String key, String section, String value)
        {
            if (!this.m_data.ContainsKey(section))
                this.m_data[section] = new Dictionary<String, String>();

            this.m_data[section][key] = value;
        }

        /// <summary>
        /// Removes value.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="section"></param>
        public void RemoveValue(String key, String section)
        {
            if (!this.m_data.ContainsKey(section))
				return;
            if (this.m_data[section].ContainsKey(key))
                this.m_data[section].Remove(key);
        }

        /// <summary>
        /// Clear all values in section.
        /// </summary>
        /// <param name="section"></param>
        public void ClearSection(String section)
        {
            if (!this.m_data.ContainsKey(section))
                this.m_data[section] = new Dictionary<String, String>();
            else
                this.m_data[section].Clear();
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Save section out to a stream.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="entries"></param>
        /// <param name="stream"></param>
        /// <param name="options"></param>
        private void SaveSection(String name, IDictionary<String, String> entries, Stream stream, IniFileSaveOption options)
        {
            StringBuilder sb = new StringBuilder();
            if (!String.IsNullOrEmpty(name))
            {
                sb.AppendFormat("[{0}]", name);
                sb.AppendLine();
            }
            foreach (KeyValuePair<String, String> entry in entries)
            {
                if (options.HasFlag(IniFileSaveOption.SpacesBetweenKeysAndValues))
                    sb.AppendFormat("{0} = {1}", entry.Key, entry.Value);
                else
                    sb.AppendFormat("{0}={1}", entry.Key, entry.Value);
                sb.AppendLine();
            }
            Byte[] data = ASCIIEncoding.ASCII.GetBytes(sb.ToString());
            stream.Write(data, 0, data.Length);
        }
        #endregion // Private Methods
    }

} // RSG.Base.IO namespace
