//
// File: Find.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of Find.cs class
//

using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace RSG.Base.IO
{

    /// <summary>
    /// Filesystem search functions.
    /// </summary>
    public static class FindUtil
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="wildcard"></param>
        /// <param name="recurse"></param>
        /// <returns></returns>
        public static String[] Find(String wildcard, bool recurse)
        {
            List<String> files = new List<String>();
            String rootPath = Path.GetDirectoryName(wildcard);
            String wc_str = Path.GetFileName(wildcard);
            Wildcard wc = new Wildcard(Path.GetFileName(wildcard));

            if (recurse)
            {
                foreach (String dir in Directory.GetDirectories(rootPath))
                {
                    foreach (String filename in Directory.GetFiles(dir))
                    {
                        if (!wc.IsMatch(filename))
                            continue;
                        files.Add(filename);
                    }
                    files.AddRange(new List<String>(Find(Path.Combine(dir, wc_str), recurse)));
                }
            }
            else
            {
                foreach (String filename in Directory.GetFiles(rootPath))
                {
                    if (!wc.IsMatch(filename))
                        continue;
                    files.Add(filename);
                }
            }

            return (files.ToArray());
        }

        /// <summary>
        /// Contains the result of a grep operation
        /// </summary>
        public class GrepResult
        {
            public Match Match;
            public string Filename;
            public Regex Regex;
            public int LineNumber;
            public string Line;
        }

        /// <summary>
        /// Given a list of filenames, and a list of regular expressions, enumerates all of the matches for those regular expressions
        /// </summary>
        /// <param name="filenames">A list of all of the files to open</param>
        /// <param name="res">A list of all of the regular expressions to test</param>
        /// <returns>
        /// A sequence of GrepResult objects, containing the filename, line, match result, etc for any successful matches.
        /// </returns>
        public static IEnumerable<GrepResult> GrepInFiles(IEnumerable<string> filenames, IEnumerable<Regex> res)
        {
            foreach (string filename in filenames)
            {
                using (StreamReader stream = new StreamReader(filename))
                {
                    string line;
                    int linenumber = 1;
                    while ((line = stream.ReadLine()) != null)
                    {
                        foreach (Regex re in res)
                        {
                            Match m = re.Match(line);
                            if (m.Success)
                            {
                                yield return new GrepResult { Match = m, Filename = filename, Regex = re, LineNumber = linenumber, Line = line };
                            }
                        }
                        linenumber++;
                    }
                }
            }
        }
    }

} // RSG.Base.OS namespace


// Find.cs
