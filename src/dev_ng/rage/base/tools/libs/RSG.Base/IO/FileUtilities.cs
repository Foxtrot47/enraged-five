using System;
using System.IO;
using System.Threading;
using System.Diagnostics;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;
using Microsoft.Win32;

using RSG.Base.Command;

namespace RSG.Base.IO
{
	public class rageFileUtilities
	{
		public static void DeleteLocalFolder(string strFolderToDelete, out rageStatus obStatus)
		{
			// System.Console.WriteLine("DeleteLocalFolder(\""+ strFolderToDelete +"\", out rageStatus obStatus)");
			// If folder does not already exist, do nothing
			strFolderToDelete = strFolderToDelete.Replace("/", "\\");
			if(strFolderToDelete.EndsWith("\\")) strFolderToDelete = strFolderToDelete.Substring(0, strFolderToDelete.Length - 1);
			if(Directory.Exists(strFolderToDelete))
			{
				// Folder exists
				// Does it contain sub directories?
				foreach( string strSubDir in Directory.GetDirectories(strFolderToDelete, "*"))
				{
					// It has sub directories, so delete them
					DeleteLocalFolder(strSubDir, out obStatus);
					if(!obStatus.Success())
					{
						return;
					}
				}

				// Does it contain files?
				foreach( string strFile in Directory.GetFiles(strFolderToDelete, "*"))
				{
					// It has files, so delete them
					DeleteLocalFile(strFile, out obStatus);
					if(!obStatus.Success())
					{
						return;
					}
				}
				DirectoryInfo obDirectory = new DirectoryInfo(strFolderToDelete);
				obDirectory.Attributes = 0;
				try 
				{
					Directory.Delete(strFolderToDelete, true);
				}
				catch 
				{
					obStatus = new rageStatus("Unable to delete "+ strFolderToDelete +"\nThis probably means it it currently being used by another process.  Try closing any open applications.");
					return;
				}
				// It appears to have deleted without a problem, but has it really?
				if(Directory.Exists(strFolderToDelete))
				{
					// Odd, everything appeared to go ok, but the file still exists
					obStatus = new rageStatus("There is something odd about the folder "+ strFolderToDelete +"\nIt deleted without any errors being reported, but it still exists.");
					return;
				}
			}
			obStatus = new rageStatus();
		}

		public static void MoveLocalFolder(string strSourcePath, string strDestinationPath, out rageStatus obStatus ) 
		{
			obStatus = new rageStatus();
			MakeDir(GetLocationFromPath(strDestinationPath));
			try 
			{
				Directory.Move(strSourcePath, strDestinationPath);
			}
			catch (ArgumentNullException) 
			{
				obStatus = new rageStatus("MoveLocalFolder : Path is a null reference.");
			}
			catch (System.Security.SecurityException) 
			{
				obStatus = new rageStatus("System.Security.SecurityException : Unable to move "+ strSourcePath +" to "+ strDestinationPath +". Permission is denied.\nThis probably means it it currently being used by another process.  Try closing any open applications.");
			}
			catch (ArgumentException) 
			{
				obStatus = new rageStatus(strSourcePath +" or "+ strDestinationPath +" is not a valid folder name.");	
			}
			catch (System.IO.IOException) 
			{
				obStatus = new rageStatus("System.IO.IOException : Unable to move "+ strSourcePath +" to "+ strDestinationPath +". Permission is denied.\nThis probably means it it currently being used by another process.  Try closing any open applications.");
			}
		}

		public static void CopyLocalFolder(string strSourcePath, string strDestinationPath, out rageStatus obStatus)
		{
			CopyLocalFolder(strSourcePath, strDestinationPath, false, out obStatus);
		}

		public static void CopyLocalFolder(string strSourcePath, string strDestinationPath, bool bOnlyIfNewer, out rageStatus obStatus ) 
		{
			obStatus = new rageStatus();
			MakeDir(strDestinationPath);

			if(Directory.Exists(strSourcePath))
			{
				// Does it contain sub directories?
				foreach( string strSubDir in Directory.GetDirectories(strSourcePath, "*"))
				{
					// It has sub directories, so recurse
					CopyLocalFolder(strSubDir, strDestinationPath +"/"+ GetFilenameFromFilePath(strSubDir), out obStatus );
					if(!obStatus.Success()) return;
				}

				// Does it contain files?
				foreach( string strFile in Directory.GetFiles(strSourcePath, "*"))
				{
					// It has files, so copy them
					CopyFile(strFile, strDestinationPath +"/"+ GetFilenameFromFilePath(strFile), bOnlyIfNewer, out obStatus );
					if(!obStatus.Success()) return;
				}
			}
		}

		public static void MoveLocalFile(string strSourcePath, string strDestinationPath, out rageStatus obStatus ) 
		{
			obStatus = new rageStatus();
			strSourcePath = strSourcePath.Replace("\\", "/");
			strDestinationPath = strDestinationPath.Replace("\\", "/");
			MakeDir(GetLocationFromPath(strDestinationPath));
			try 
			{
				File.Move(strSourcePath, strDestinationPath);
			}
			catch (ArgumentNullException) 
			{
				obStatus = new rageStatus("MoveLocalFile : Path is a null reference.");
			}
			catch (System.Security.SecurityException) 
			{
				obStatus = new rageStatus("Unable to move "+ strSourcePath +" to "+ strDestinationPath +". Permission is denied.\nThis probably means it it currently being used by another process.  Try closing any open applications.");
			}
			catch (ArgumentException) 
			{
				obStatus = new rageStatus(strSourcePath +" or "+ strDestinationPath +" is not a vaild File name.");	
			}
			catch (System.IO.IOException) 
			{
				obStatus = new rageStatus("Unable to move "+ strSourcePath +" to "+ strDestinationPath +". Unknown reason.\nThis probably means it it currently being used by another process.  Try closing any open applications.");
			}
		}

		public static void DeleteFilesFromLocalFolder(string strFolderToDelete, string strSearchString, out rageStatus obStatus)
		{
			// If folder does not already exist, do nothing
			strFolderToDelete = strFolderToDelete.Replace("/", "\\");
			if(Directory.Exists(strFolderToDelete))
			{
				// Does it contain sub directories?
				foreach( string strSubDir in Directory.GetDirectories(strFolderToDelete, "*"))
				{
					// It has sub directories, so delete them
					DeleteFilesFromLocalFolder(strSubDir, strSearchString, out obStatus);
					if(!obStatus.Success())
					{
						return;
					}
				}

				// Does it contain files?
				foreach( string strFile in Directory.GetFiles(strFolderToDelete, strSearchString))
				{
					// It has files, so delete them
					try 
					{
						FileInfo obFile = new FileInfo(strFile);
						obFile.Attributes = 0;
						File.Delete(strFile);
					}
					catch 
					{
						obStatus = new rageStatus("Unable to delete "+ strFile +"\nThis probably means it it currently being used by another process.  Try closing any open applications.");
						return;
					}
				}
			}
			obStatus = new rageStatus();
		}

		public static void DeleteLocalFile(string strFileToDelete, out rageStatus obStatus)
		{
			// If file does not already exist, do nothing
			if(File.Exists(strFileToDelete))
			{
				// It has files, so delete them
				try 
				{
					FileInfo obFile = new FileInfo(strFileToDelete);
					obFile.Attributes = 0;
					File.Delete(strFileToDelete);
				}
				catch 
				{
					obStatus = new rageStatus("Unable to delete "+ strFileToDelete +"\nThis probably means it it currently being used by another process.  Try closing any open applications.");
					return;
				}

				// It appears to have deleted without a problem, but has it really?
				if(File.Exists(strFileToDelete))
				{
					// Odd, everything appeared to go ok, but the file still exists
					obStatus = new rageStatus("There is something odd about the file "+ strFileToDelete +"\nIt deleted without any errors being reported, but it still exists.");
					return;
				}
			}
			obStatus = new rageStatus();
		}

		/*** Procedure ***********************************************************************************************
		global proc string GetLocationFromPath(string $strUnixFilename)

		Description:
		Given a fullpath and filename, using UNIX slashs (i.e. nice /s instead of evil dos \s) it returns the path 
		without the last part.

		For example, given t:/fish/hamster.txt it would return t:/fish

		This is the opposite of rageGetFileNameFromFilePath

		Arguments:
		string	$strPathIn - The path to manipulate

		Return:
		string	-	The path without the last part

		*********************************************************************************************************P***/
		public static string GetLocationFromPath(string strPathIn)
		{
			char[] acSlashes = {'/', '\\'};
			int iPosOfLastSlash = strPathIn.LastIndexOfAny(acSlashes);
			if(iPosOfLastSlash + 1 == strPathIn.Length)
			{
				// The given string ends in a slash, so chop it off and try again
				return GetLocationFromPath(strPathIn.Substring(0, iPosOfLastSlash));
			}
			else if(iPosOfLastSlash != -1)
			{
				return strPathIn.Substring(0, iPosOfLastSlash);
			}

			// No slashes, so just return this string directly
			return strPathIn;
		}

		/*** Procedure ***********************************************************************************************
		global proc string rageGetFileNameFromFilePath(string $strFilePath)

		Description:
		Given a fullpath and filename, using UNIX slashs (i.e. nice /s instead of evil dos \s) it returns the last part

		For example, given t:/fish/hamster.txt it would return hamster.txt

		This is the opposite of GetLocationFromPath

		Arguments:
		string	$strFilePath - The path to manipulate

		Return:
		string	-	Filename without the path

		*********************************************************************************************************P***/
		public static string GetFilenameFromFilePath(string strPathIn)
		{
			char[] acSlashes = {'/', '\\'};
			int iPosOfLastSlash = strPathIn.LastIndexOfAny(acSlashes);
			if(iPosOfLastSlash != -1)
			{
				return strPathIn.Substring(iPosOfLastSlash + 1, strPathIn.Length - iPosOfLastSlash - 1);
			}

			// No slashes, so just return this string directly
			return strPathIn;
		}

        /*** Procedure ***********************************************************************************************

        global proc string GetRelativePath(string $strAbsolutePath, string $strRelativeToDir)

        Description:
        Converts $strAbsolutePath to a path relative to the directory $strRelativeToDir.

        For example, given the absolute path "t:/fish/squirrel/hamster.txt", and the directory "t:/fish/shark",
        it would return "../squirrel/hamster.txt"

        Arguments:
        string $strAbsolutePath - The absolute file path to manipulate.
        string $strRelativeToDir - The directory that the return value should be relative to.

        Return:
        string - The path $strAbsolutePath converted to be relative to the directory $strRelativeToDir.

        *********************************************************************************************************P***/
        public static string GetRelativePath(string strAbsolutePath, string strRelativeToDir)
        {
            string[] firstPathParts = strRelativeToDir.Trim(Path.DirectorySeparatorChar).Split(Path.DirectorySeparatorChar);
            string[] secondPathParts = strAbsolutePath.Trim(Path.DirectorySeparatorChar).Split(Path.DirectorySeparatorChar);

            int sameCounter = 0;
            for (int i = 0; i < System.Math.Min(firstPathParts.Length, secondPathParts.Length); ++i)
            {
                if ( !firstPathParts[i].ToLower().Equals(secondPathParts[i].ToLower()) )
                {
                    break;
                }
                ++sameCounter;
            }

            if (sameCounter == 0)
            {
                return strAbsolutePath;
            }

            string relativePath = String.Empty;
            for (int i = sameCounter; i < firstPathParts.Length; ++i)
            {
                if (i > sameCounter)
                {
                    relativePath += Path.DirectorySeparatorChar;
                }
                relativePath += "..";
            }

            if (relativePath.Length == 0)
            {
                relativePath = ".";
            }

            for (int i = sameCounter; i < secondPathParts.Length; ++i)
            {
                if (i < secondPathParts.Length)
                {
                    relativePath += Path.DirectorySeparatorChar;
                }
                relativePath += secondPathParts[i];
            }

            return relativePath;
        }

        /*** Procedure ***********************************************************************************************

        global proc string RemoveFileExtension(string $$strFile)

        Description:
        Returns the string passed it, without whatever comes after the last '.'

        For example, given t:/fish/hamster.txt it would return t:/fish/hamster

        Arguments:
        string	$strFile - The path to manipulate

        Return:
        string	-	Filename without the extension

        *********************************************************************************************************P***/
        public static string RemoveFileExtension(string strFilename)
		{
			int iPosOfLastDot = strFilename.LastIndexOf(".");
			if(iPosOfLastDot != -1)
			{
				return strFilename.Substring(0,iPosOfLastDot);
			}
			return strFilename;
		}

		/*** Procedure ***********************************************************************************************
		global proc int MoveFile( string $strSrcFilepath, string $strDstFilepath )

		Description:
		moves a file or a directory

		Arguments:
		string	$strSrcFilepath - File to move
		string	$strDstFilepath - Location to move to

		Return:

		*********************************************************************************************************P***/
		public static void MoveFile(string strSrcFilepath, string strDstFilepath, out rageStatus obStatus)
		{
			try 
			{
				// Ensure that the target does not exist.
				DeleteLocalFile(strDstFilepath, out obStatus);
				if(!obStatus.Success())
				{
					// Something bad happened
					return;
				}

				// Move the file.
				File.Move(strSrcFilepath, strDstFilepath);
			} 
			catch (Exception e) 
			{
				// Something bad happened
				obStatus = new rageStatus("Failed to move "+ strSrcFilepath +" to "+ strDstFilepath +" : "+ e.ToString());
				return;
			}

			// All good
			obStatus = new rageStatus();
		}

        /*** Procedure ***********************************************************************************************
        global proc int CopyFile( string $strSrcFilepath, string $strDstFilepath )

        Description:
        moves a file or a directory

        Arguments:
        string	$strSrcFilepath - File to move
        string	$strDstFilepath - Location to move to

        Return:

        *********************************************************************************************************P***/
        public static void CopyFile(string strSrcFilepath, string strDstFilepath, out rageStatus obStatus)
		{
			CopyFile(strSrcFilepath, strDstFilepath, false, out obStatus);
		}

		/*** Procedure ***********************************************************************************************
		global proc int CopyFile( string $strSrcFilepath, string $strDstFilepath )

		Description:
		moves a file or a directory

		Arguments:
		string	$strSrcFilepath - File to move
		string	$strDstFilepath - Location to move to

		Return:

		*********************************************************************************************************P***/
		public static void CopyFile(string strSrcFilepath, string strDstFilepath, bool bOnlyIfNewer, out rageStatus obStatus)
		{
			try 
			{
				if (bOnlyIfNewer)
				{
					// Is it newer than the existing version?
					if (!File.Exists(strDstFilepath))
					{
						// Destination doesn't exist, so I need to copy
					}
					else
					{
						if (File.GetLastWriteTime(strSrcFilepath) <= File.GetLastWriteTime(strDstFilepath))
						{
							// Not newer, so bail
							obStatus = new rageStatus();
							return;
						}
					}
				}

				// Ensure that the target does not exist.
				DeleteLocalFile(strDstFilepath, out obStatus);
				if(!obStatus.Success())
				{
					// Something bad happened
					return;
				}

				// Make sure the destination dir is there
				MakeDir(GetLocationFromPath(strDstFilepath));

				// Copy the file.
				File.Copy(strSrcFilepath, strDstFilepath);
			} 
			catch (Exception e) 
			{
				// Something bad happened
				obStatus = new rageStatus("Failed to copy "+ strSrcFilepath +" to "+ strDstFilepath +" : "+ e.ToString());
				return;
			}

			// All good
			obStatus = new rageStatus();
		}

		/*** Procedure ***********************************************************************************************

		global proc int MakeDir(string $strDirectoryPath)

		Description:
		Create a directory.  If the directory already exists it does nothing.
		If any parts of the given path do not exist, it will create them as well

		Arguments:
		string	$strDirectoryPath - Directory to create

		Return:

		*********************************************************************************************************P***/
		public static void MakeDir(string strDirectoryPath)
		{
			// System.Console.WriteLine("MakeDir(\""+ strDirectoryPath +"\")");
			// Standardise slashes
			strDirectoryPath = strDirectoryPath.Replace("\\", "/");

			// If folder already exists, don't do anything
			if(!Directory.Exists(strDirectoryPath))
			{
				// Chop off last bit of file name
				string strParentFolder = GetLocationFromPath(strDirectoryPath);

				if(strParentFolder != strDirectoryPath)
				{
					// There is a valid parent folder, so create it
					MakeDir(strParentFolder);
				}

				if(strDirectoryPath.StartsWith("//") && (strDirectoryPath.Substring(2, strDirectoryPath.Length - 2).IndexOf("/") == -1))
				{
					// strDirectoryPath is just something like "\\krose" or "\\ragemonkey2" so 
					// bail as I can't create something like this, it makes no sense
					return;
				}

				// Make dir
				Directory.CreateDirectory(strDirectoryPath);
			}
		}
	
		public static ArrayList GetFileList(string strFolder, string strSearchString)
		{
			ArrayList obAStrReturnMe = new ArrayList();
			// If folder does not exist, do nothing
			if(Directory.Exists(strFolder))
			{
				// Does it contain sub directories?
				foreach( string strSubDir in Directory.GetDirectories(strFolder, "*"))
				{
					// It has sub directories, so recurse
					obAStrReturnMe.AddRange(GetFileList(strSubDir, strSearchString));
				}

				// Does it contain files?
				foreach( string strFile in Directory.GetFiles(strFolder, strSearchString))
				{
					// It has files, so add them
					obAStrReturnMe.Add(strFile);
				}
			}
			return obAStrReturnMe;
		}

		public static List<string> GetFilesAsStringList(string strFolder, string strSearchString)
		{
			List<string> obAStrReturnMe = new List<string>();
			// If folder does not exist, do nothing
			if (Directory.Exists(strFolder))
			{
				// Does it contain sub directories?
				foreach (string strSubDir in Directory.GetDirectories(strFolder, "*"))
				{
					// It has sub directories, so recurse
					obAStrReturnMe.AddRange(GetFilesAsStringList(strSubDir, strSearchString));
				}

				// Does it contain files?
				foreach (string strFile in Directory.GetFiles(strFolder, strSearchString))
				{
					// It has files, so add them
					obAStrReturnMe.Add(strFile);
				}
			}
			return obAStrReturnMe;
		}

		public static void GetOpenHandles(string strPathToSearchFor, out ArrayList obAStrPIDs, out ArrayList obAStrProcessNames, out ArrayList obAStrFileNames, out rageStatus obStatus)
		{
			// System.Console.WriteLine("GetOpenHandles(\""+ strPathToSearchFor +"\", out ArrayList obAStrPIDs, out ArrayList obAStrProcessNames, out ArrayList obAStrFileNames, out rageStatus obStatus)");
			// This function is basically a wrapper to the wonderful handle program
			obAStrPIDs = new ArrayList();
			obAStrProcessNames = new ArrayList();
			obAStrFileNames = new ArrayList();

			// Before I run handle, make sure it is not stored inside the path I am about to check for
			string[] astrPaths = Environment.GetEnvironmentVariable("PATH").Split(";".ToCharArray());
			string strPathToHandle = "";
			foreach(string strPath in astrPaths)
			{
				// Is handle in this path?
				// System.Console.WriteLine("Looking for "+ strPath +"\\handle.exe");
				if(File.Exists(strPath +"\\handle.exe"))
				{
					// System.Console.WriteLine("Found it!");
					strPathToHandle = strPath.ToLower() +"\\handle.exe";
					break;
				}
			}

			// Have I found handle?
			if(strPathToHandle == "")
			{
				// Unable to find handle in path, so bail
				obStatus = new rageStatus("Unable to find handle.exe in your path.  This is usually found in T:/rage/tools/Modules/base/rage/exes");
				return;
			}

			// Is strPathToSearchFor part of strPathToHandle?
//			System.Console.WriteLine("strPathToHandle	= "+ strPathToHandle);
//			System.Console.WriteLine("strPathToSearchFor = "+ strPathToSearchFor);
//			System.Console.WriteLine("strPathToHandle.StartsWith(strPathToSearchFor) = "+ strPathToHandle.StartsWith(strPathToSearchFor));
			if(strPathToHandle.StartsWith(strPathToSearchFor.ToLower()))
			{
				// Oh dear, handle lives inside the path I am interested in, so move it before I call it
				string strNewPathToHandle = Environment.GetEnvironmentVariable("TEMP") +"\\handle.exe";
				CopyFile(strPathToHandle, strNewPathToHandle, out obStatus);
				if(!obStatus.Success()) return;

				strPathToHandle = strNewPathToHandle;
			}

			// If strPathToSearchFor starts with a drive letter, chop it off as handle knows nothing of such things
			int iPosOfColon = strPathToSearchFor.LastIndexOf(":");
			if(iPosOfColon > -1)
			{
				strPathToSearchFor = strPathToSearchFor.Substring(iPosOfColon + 2, strPathToSearchFor.Length - iPosOfColon - 2);
			}

			// Execute handle
			ArrayList obAStrHandleOutput = new ArrayList();
			rageExecuteCommand obHandle = new rageExecuteCommand(strPathToHandle, strPathToSearchFor.Replace("/", "\\"), out obStatus);
			obStatus = new rageStatus();

			// Parse handle output
			foreach(string strHandleLine in obHandle.GetLogAsArray())
			{
				// Break up line
				// System.Console.WriteLine(strHandleLine);
				int iPosOfPid = strHandleLine.IndexOf("pid:");
				if(iPosOfPid > -1)
				{
					// Ok, there is a pid so this line is worth looking at
					// Name
					string strProcessName = strHandleLine.Substring(0, iPosOfPid);

					// Remove white space
					strProcessName = (strProcessName.TrimStart(" ".ToCharArray())).TrimEnd(" ".ToCharArray());
					// System.Console.WriteLine(strProcessName);
					obAStrProcessNames.Add(strProcessName);

					// Pid
					string strPid = strHandleLine.Substring(iPosOfPid + 5, strHandleLine.Length - iPosOfPid - 6);
					// Remove white space
					strPid = (strPid.TrimStart(" ".ToCharArray())).TrimEnd(" ".ToCharArray());
					strPid = strPid.Substring(0, strPid.IndexOf(" "));
					// System.Console.WriteLine(strPid);
					obAStrPIDs.Add(strPid);

					// Filename
					int iPosOfFilename = strHandleLine.LastIndexOf(": ") + 2;
					string strFilename = strHandleLine.Substring(iPosOfFilename, strHandleLine.Length - iPosOfFilename);
					// System.Console.WriteLine(strFilename);
					obAStrFileNames.Add(strFilename);
				}
			}
		}

		public static string GenerateTimeBasedFilename()
		{
			return (DateTime.Now.Year +"."+ (DateTime.Now.Month < 10 ? "0" : "") + DateTime.Now.Month +"."+ (DateTime.Now.Day < 10 ? "0" : "") + DateTime.Now.Day +"_"+ (DateTime.Now.Hour < 10 ? "0" : "") + DateTime.Now.Hour +"."+ (DateTime.Now.Minute < 10 ? "0" : "") + DateTime.Now.Minute +"."+ (DateTime.Now.Second < 10 ? "0" : "") + DateTime.Now.Second +"."+ (DateTime.Now.Millisecond < 10 ? "0" : "") + DateTime.Now.Millisecond);
		}

		/// <summary>
		/// Returns a value extracted from the registry
		/// I'm not sure if this function should really be in this class, but there is nowhere else obvious for it to go
		/// Expects something of the form "HKEY_CURRENT_USER/Environment/HAMSTER"
		/// </summary>
		public static string GetFromRegistry(string strFullPath, out rageStatus obStatus)
		{
			// Ok, make sure all the slashes are the same way around in the path
			strFullPath = strFullPath.Replace("\\", "/");
			int iPosOfFirstSlash	= strFullPath.IndexOf("/");
			int iPosOfLastSlash		= strFullPath.LastIndexOf("/");
			string strFirstBit = strFullPath.Substring(0, iPosOfFirstSlash);
			string strPathBit = strFullPath.Substring(iPosOfFirstSlash + 1, iPosOfLastSlash - iPosOfFirstSlash - 1);
			string strValueName = strFullPath.Substring(iPosOfLastSlash + 1, strFullPath.Length - iPosOfLastSlash - 1);
			//			System.Console.WriteLine("strFirstBit = {0}", strFirstBit);
			//			System.Console.WriteLine("strPathBit = {0}", strPathBit);
			//			System.Console.WriteLine("strValueName = {0}", strValueName);

			strPathBit = strPathBit.Replace("/", "\\");

			RegistryKey obRegistryKey;
			if(strFirstBit == "HKEY_CLASSES_ROOT")
				obRegistryKey = Registry.ClassesRoot.OpenSubKey(strPathBit, true);
			else if(strFirstBit == "HKEY_CURRENT_CONFIG")
				obRegistryKey = Registry.CurrentConfig.OpenSubKey(strPathBit, true);
			else if(strFirstBit == "HKEY_CURRENT_USER")
				obRegistryKey = Registry.CurrentUser.OpenSubKey(strPathBit, true);
			else if(strFirstBit == "HKEY_LOCAL_MACHINE")
				obRegistryKey = Registry.LocalMachine.OpenSubKey(strPathBit, true);
			else // if(strFirstBit == "HKEY_USERS")
				obRegistryKey = Registry.Users.OpenSubKey(strPathBit, true);

			// Get the data from a specified item in the key.
			string strValue = "";
			if(obRegistryKey != null)
			{
				obStatus = new rageStatus();
				strValue = (string)obRegistryKey.GetValue(strValueName);
			}
			else
			{
				obStatus = new rageStatus("Unable to find registry key "+ strFullPath);
			}

			// Return value
			return strValue;
		}

		public static string CleanStringOfBadCharacters(string strBadString)
		{
			string strGoodString = "";
			foreach(char c in strBadString)
			{
				if (
					(c == '*')
					||
					(c == '?')
					||
					(c == '\"')
					||
					(c == '<')
					||
					(c == '>')
					||
					(c == '|')
					)
				{
					// Bad character, so replace with an _
					strGoodString += "_";
				}
				else
				{
					// The character is valid, so do nothing
					strGoodString += c;
				}
			}
			return strGoodString;
		}

		static public void CompareFolders(string strFolderOne, string strFolderTwo, out List<String> obAStrFilesOnlyExistInFolderOne, out List<String> obAStrFilesOnlyExistInFolderTwo, out List<String> obAStrFilesExistInBothButNewerInFolderOne, out List<String> obAStrFilesExistInBothButNewerInFolderTwo, out List<String> obAStrFilesSameInBoth)
		{
			// Ok, now I have the local version, decide what to do with the files
			List<String> obAStrFolderOneFiles = rageFileUtilities.GetFilesAsStringList(strFolderOne, "*");
			List<String> obAStrFolderTwoFiles = rageFileUtilities.GetFilesAsStringList(strFolderTwo, "*");
			obAStrFilesOnlyExistInFolderOne = new List<String>();
			obAStrFilesOnlyExistInFolderTwo = new List<String>();
			obAStrFilesExistInBothButNewerInFolderOne = new List<String>();
			obAStrFilesExistInBothButNewerInFolderTwo = new List<String>();
			obAStrFilesSameInBoth = new List<String>();

			// First compare obAStrFolderOneFiles to obAStrFolderTwoFiles
			foreach (string strFolderOneFile in obAStrFolderOneFiles)
			{
				// Does the folder one file exist in folder two files?
				string strFolderOneRelativeFileNotLowerCase = strFolderOneFile.Substring(strFolderOne.Length, (strFolderOneFile.Length - strFolderOne.Length));
				string strFolderOneRelativeFile = strFolderOneRelativeFileNotLowerCase.ToLower();

				// Compare to folder two files
				bool bFileExists = false;
				foreach (string strFolderTwoFile in obAStrFolderTwoFiles)
				{
					// Get relative file
					string strFolderTwoRelativeFile = strFolderTwoFile.Substring(strFolderTwo.Length, (strFolderTwoFile.Length - strFolderTwo.Length)).ToLower();

					if (strFolderOneRelativeFile == strFolderTwoRelativeFile)
					{
						// Found it!
						bFileExists = true;
						break;
					}
				}

				if (bFileExists)
				{
					// File exists in both locations
					// So compare...
					DateTime obFolderOneFileFileTime = File.GetLastWriteTime(strFolderOneFile);
					DateTime obFolderTwoFileFileTime = File.GetLastWriteTime(strFolderTwo + "/" + strFolderOneRelativeFile);
					int iTimeComparision = obFolderOneFileFileTime.CompareTo(obFolderTwoFileFileTime);

					if (iTimeComparision == 0)
					{
						// Identical
						obAStrFilesSameInBoth.Add(strFolderOneRelativeFileNotLowerCase);
					}
					else if (iTimeComparision > 0)
					{
						// Newer in folder one
						obAStrFilesExistInBothButNewerInFolderOne.Add(strFolderOneRelativeFileNotLowerCase);
					}
					else if (iTimeComparision < 0)
					{
						// Newer in folder two
						obAStrFilesExistInBothButNewerInFolderTwo.Add(strFolderOneRelativeFileNotLowerCase);
					}
				}
				else
				{
					// File only exists in folder one
					obAStrFilesOnlyExistInFolderOne.Add(strFolderOneRelativeFileNotLowerCase);
				}
			}

			// Now compare the other way around, obAStrFolderTwoFiles to obAStrFolderOneFiles
			foreach (string strFolderTwoFile in obAStrFolderTwoFiles)
			{
				// Does the folder Two file exist in folder One files?
				string strFolderTwoRelativeFileNotLowerCase = strFolderTwoFile.Substring(strFolderTwo.Length, (strFolderTwoFile.Length - strFolderTwo.Length));
				string strFolderTwoRelativeFile = strFolderTwoRelativeFileNotLowerCase.ToLower();

				// Compare to folder One files
				bool bFileExists = false;
				foreach (string strFolderOneFile in obAStrFolderOneFiles)
				{
					// Get relative file
					string strFolderOneRelativeFile = strFolderOneFile.Substring(strFolderOne.Length, (strFolderOneFile.Length - strFolderOne.Length)).ToLower();

					if (strFolderTwoRelativeFile == strFolderOneRelativeFile)
					{
						// Found it!
						bFileExists = true;
						break;
					}
				}

				if (!bFileExists)
				{
					// File only exists in folder Two
					obAStrFilesOnlyExistInFolderTwo.Add(strFolderTwoRelativeFileNotLowerCase);
				}
			}
		}

		static public void SetAttributes(string strPathToChangeAttributes, FileAttributes obFileAttributes )
		{
			if(Directory.Exists(strPathToChangeAttributes))
			{
				// It is a directory, so recurse
				foreach (string strSubDir in Directory.GetDirectories(strPathToChangeAttributes, "*"))
				{
					SetAttributes(strSubDir, obFileAttributes);
				}

				foreach (string strFile in Directory.GetFiles(strPathToChangeAttributes, "*"))
				{
					SetAttributes(strFile, obFileAttributes);
				}
			}
			if(File.Exists(strPathToChangeAttributes))
			{
				// It is a file, so set attributes
				File.SetAttributes(strPathToChangeAttributes, obFileAttributes);
			}
		}

		static public long GetFolderSizeInBytes(DirectoryInfo d)
		{
			long Size = 0;

			// Add file sizes.
			FileInfo[] fis = d.GetFiles();
			foreach (FileInfo fi in fis)
			{
				// It is an annoying special case, but between the line above and the line below, "something"
				// might have deleted the file.  So I need to try...catch getting the size, I know lame.
				try
				{
					Size += fi.Length;
				}
				catch
				{
				}
			}
			// Add subdirectory sizes.
			DirectoryInfo[] dis = d.GetDirectories();
			foreach (DirectoryInfo di in dis)
			{
				Size += GetFolderSizeInBytes(di);
			}
			return (Size);
		}

		static public long GetFolderSizeInBytes(string strFolderPathAndFileName)
		{
			return GetFolderSizeInBytes(new DirectoryInfo(strFolderPathAndFileName));
		}

		static private void SyncFoldersRecursively(DirectoryInfo obSourceFolder, DirectoryInfo obDestFolder, bool bDeleteDestFilesNotInSourceFolder)
		{
			// Folder
			if (!obDestFolder.Exists)
			{
				obDestFolder.Create();
			}

			if (bDeleteDestFilesNotInSourceFolder)
			{
				FileInfo[] aobDestFiles = obDestFolder.GetFiles();
				foreach (FileInfo obDestFile in aobDestFiles)
				{
					FileInfo obSourceFile = new FileInfo(obSourceFolder.FullName + "/" + obDestFile.Name);
					if (!obSourceFile.Exists)
					{
						Console.WriteLine("Delete : " + obDestFile.FullName);
						obDestFile.Delete();
					}
				}

				DirectoryInfo[] aobSubDestFolders = obDestFolder.GetDirectories();
				foreach (DirectoryInfo obSubDestFolder in aobSubDestFolders)
				{
					DirectoryInfo obSubSourceFolder = new DirectoryInfo(obSourceFolder.FullName + "/" + obSubDestFolder.Name);
					if (!obSubSourceFolder.Exists)
					{
						Console.WriteLine("Delete : " + obSubDestFolder.FullName);
						obSubDestFolder.Delete();
					}
				}
			}

			// Recurse
			DirectoryInfo[] aobSubSourceFolders = obSourceFolder.GetDirectories();
			foreach (DirectoryInfo obSubSourceFolder in aobSubSourceFolders)
			{
				String strSubDestFolder = obDestFolder.FullName + "/" + obSubSourceFolder.Name;
				SyncFoldersRecursively(obSubSourceFolder, new DirectoryInfo(strSubDestFolder), bDeleteDestFilesNotInSourceFolder);
			}

			// Files
			FileInfo[] aobSourceFiles = obSourceFolder.GetFiles();
			foreach (FileInfo obSourceFile in aobSourceFiles)
			{
				bool bCopyFile = false;
				FileInfo obDestFile = new FileInfo(obDestFolder.FullName + "/" + obSourceFile.Name);
				if (obDestFile.Exists)
				{
					if (obDestFile.LastWriteTime < obSourceFile.LastWriteTime)
					{
						Console.WriteLine("Copying " + obSourceFile.FullName + " to " + obDestFile.FullName + " because their times are different " + obSourceFile.LastWriteTime + " and " + obDestFile.LastWriteTime);
						bCopyFile = true;
					}
				}
				else
				{
					Console.WriteLine("Copying " + obSourceFile.FullName + " to " + obDestFile.FullName + " because it doesn't exist in destination folder");
					bCopyFile = true;
				}

				if (bCopyFile)
				{
					obDestFile.Delete();
					obSourceFile.CopyTo(obDestFile.FullName);
				}
			}
		}

		static private void SyncFoldersWithRoboCopy(DirectoryInfo obSourceFolder, DirectoryInfo obDestFolder, bool bDeleteDestFilesNotInSourceFolder, bool bMakeOlderDestFilesMatchSourceFiles, bool bMakeNewerDestFilesMatchSourceFiles)
		{
			// Folder
			if (!obDestFolder.Exists)
			{
				obDestFolder.Create();
			}

			// Execute the command
			rageStatus obStatus;
			rageExecuteCommand obCommand = new rageExecuteCommand();
			obCommand.Command = "robocopy";
			// obCommand.Arguments = "/MIR /NS /NC /NFL /NDL /NP ";
			obCommand.Arguments = "/MIR /NDL /NP ";
			if(!bMakeOlderDestFilesMatchSourceFiles)
			{
				obCommand.Arguments += " /XN ";
			}
			if (!bMakeNewerDestFilesMatchSourceFiles)
			{
				obCommand.Arguments += " /XO ";
			}
			obCommand.Arguments += "\"" + obSourceFolder.FullName.Replace('/', '\\').Substring(0, obSourceFolder.FullName.Length - 1) + "\" \"" + obDestFolder.FullName.Replace('/', '\\').Substring(0, obDestFolder.FullName.Length - 1) + "\"";
			obCommand.UpdateLogAfterEverySoManyLines = 500;
			obCommand.LogToHtmlFile = false;
			obCommand.RemoveLog = true;
			obCommand.UseBusySpinner = false;
			obCommand.EchoToConsole = true;
			Console.WriteLine(obCommand.GetDosCommand());
			obCommand.Execute(out obStatus);
			if (!obStatus.Success())
			{
				// Badness happened
				Console.WriteLine(obStatus.ErrorString);
			}
		}

		static public void SyncFolderAdvanced(DirectoryInfo obSourceFolder, DirectoryInfo obDestFolder, bool bDeleteDestFilesNotInSourceFolder, bool bMakeOlderDestFilesMatchSourceFiles, bool bMakeNewerDestFilesMatchSourceFiles)
		{
			// Does source folder exist?
			if(!obSourceFolder.Exists)
			{
				// Source folder does not exist, so bail
				return;
			}

			// The fastest way to sync to folders is to use RoboCopy, but not everyone has this installed, so check if it exists first
			// Execute the command
			rageStatus obStatus;
			rageExecuteCommand obCommand = new rageExecuteCommand();
			obCommand.Command = "robocopy";
			obCommand.Arguments = "";
			obCommand.UpdateLogAfterEverySoManyLines = 500;
			obCommand.LogToHtmlFile = false;
			obCommand.RemoveLog = true;
			obCommand.UseBusySpinner = false;
			obCommand.EchoToConsole = false;
			obCommand.Execute(out obStatus);

			if(obCommand.GetExitCode() == 16)
			{
				// Robocopy exists!  So use it
				SyncFoldersWithRoboCopy(obSourceFolder, obDestFolder, bDeleteDestFilesNotInSourceFolder, bMakeOlderDestFilesMatchSourceFiles, bMakeNewerDestFilesMatchSourceFiles);
			}
			else
			{
				// No RoboCopy, so use alternative
				SyncFoldersRecursively(obSourceFolder, obDestFolder, bDeleteDestFilesNotInSourceFolder);
			}
		}

		static public void SyncFolder(DirectoryInfo obSourceFolder, DirectoryInfo obDestFolder, bool bDeleteDestFilesNotInSourceFolder)
		{
			SyncFolderAdvanced(obSourceFolder, obDestFolder, bDeleteDestFilesNotInSourceFolder, true, true);
		}

		static public void SyncFolder(String strSourceFolder, String strDestFolder, bool bDeleteDestFilesNotInSourceFolder)
		{
			SyncFolder(new DirectoryInfo(strSourceFolder), new DirectoryInfo(strDestFolder), bDeleteDestFilesNotInSourceFolder);
		}

		static public void SyncFolderAdvanced(String strSourceFolder, String strDestFolder, bool bDeleteDestFilesNotInSourceFolder, bool bMakeOlderDestFilesMatchSourceFiles, bool bMakeNewerDestFilesMatchSourceFiles)
		{
			SyncFolderAdvanced(new DirectoryInfo(strSourceFolder), new DirectoryInfo(strDestFolder), bDeleteDestFilesNotInSourceFolder, bMakeOlderDestFilesMatchSourceFiles, bMakeNewerDestFilesMatchSourceFiles);
		}

        // From http://stackoverflow.com/questions/194157/c-how-to-get-program-files-x86-on-vista-x64
        static public string GetProgramFilesx86()
        {
            if (8 == IntPtr.Size
                || (!String.IsNullOrEmpty(Environment.GetEnvironmentVariable("PROCESSOR_ARCHITEW6432"))))
            {
                return Environment.GetEnvironmentVariable("ProgramFiles(x86)");
            }

            return Environment.GetEnvironmentVariable("ProgramFiles");
        }


        static public string GetRageApplicationUserConfigFilename( string applicationName )
        {
            string companyName = "Rockstar_Games";
            
            AssemblyCompanyAttribute companyAtt = Attribute.GetCustomAttribute( Assembly.GetEntryAssembly(), typeof(AssemblyCompanyAttribute) ) as AssemblyCompanyAttribute;
            if ( companyAtt != null )
            {
                companyName = companyAtt.Company.Replace( ' ', '_' );
            }

            string appDataDir = Path.GetFullPath( Path.Combine( Environment.GetEnvironmentVariable( "TEMP" ), @"..\Application Data" ) );
            string rockstarAppDataDir = Path.Combine( appDataDir, companyName );
            if ( !Directory.Exists( rockstarAppDataDir ) )
            {
                return null;
            }

            string[] directories = Directory.GetDirectories( rockstarAppDataDir, String.Format( "{0}*", applicationName ) );

            List<string> dirs = new List<string>();
            foreach ( string directory in directories )
            {
                dirs.AddRange( Directory.GetDirectories( directory ) );
            }

            dirs.Sort( CompareApplicationUserConfigDirectories );

            for ( int i = dirs.Count - 1; i >= 0; --i )
            {
                string userConfigFilename = Path.Combine( dirs[i], "user.config" );
                if ( File.Exists( userConfigFilename ) )
                {
                    return userConfigFilename;
                }
            }

            return null;
        }

        static private int CompareApplicationUserConfigDirectories( string a, string b )
        {
            int lastIndexOfA = a.LastIndexOf( '\\' );
            int lastIndexOfB = b.LastIndexOf( '\\' );

            string lastPartA = a.Substring( lastIndexOfA + 1 );
            string lastPartB = b.Substring( lastIndexOfA + 1 );

            return lastPartA.CompareTo( lastPartB );
        }

	}
}
