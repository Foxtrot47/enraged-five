//
// File: Wildcard.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of Wildcard class
//

using System;
using System.Text.RegularExpressions;

namespace RSG.Base.IO
{

    /// <summary>
    /// 
    /// </summary>
    public class Wildcard : Regex
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pattern"></param>
        public Wildcard(String pattern)
            : base(WildcardToRegex(pattern))
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pattern"></param>
        /// <param name="options"></param>
        public Wildcard(String pattern, RegexOptions options)
            : base(WildcardToRegex(pattern), options)
        {
        }
        #endregion // Constructor(s)

        #region Static Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pattern"></param>
        /// <returns></returns>
        /// Converts a wildcard String (e.g. "*.ide" to a Regex object by 
        /// replacing '*' and '?' characters with Regex-compatible 
        /// expressions (.* and . respectively).
        public static String WildcardToRegex(String pattern)
        {
            return ("^" + Regex.Escape(pattern).Replace("\\*", ".*").Replace("\\?", ".") + "$");
        }
        #endregion // Static Methods
    }

} // RSG.Base.IO namespace

// Wildcard.cs
