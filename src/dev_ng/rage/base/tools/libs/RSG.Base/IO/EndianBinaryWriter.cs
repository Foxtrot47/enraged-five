﻿using System;
using System.IO;
using System.Text;
using RSG.Base.Conversion;

namespace RSG.Base.IO
{

    /// <summary>
    /// 
    /// </summary>
    public class EndianBinaryWriter : BinaryWriter
    {
        #region Member Data
        /// <summary>
        /// Endian bit converter object.
        /// </summary>
        private EndianBitConverter m_endianConverter;

        /// <summary>
        /// Buffer used for temporary storage before conversion into primitives
        /// </summary>
        private Byte[] m_buffer = new Byte[16];

        /// <summary>
        /// Local disposing flag.
        /// </summary>
        private bool m_disposing = false;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="converter"></param>
        public EndianBinaryWriter(EndianBitConverter converter)
            : this(converter, null, Encoding.UTF8)
        {        
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stream"></param>
        public EndianBinaryWriter(EndianBitConverter converter, Stream stream)
            : this(converter, stream, Encoding.UTF8)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="converter"></param>
        /// <param name="stream"></param>
        /// <param name="encoding"></param>
        public EndianBinaryWriter(EndianBitConverter converter, Stream stream, Encoding encoding)
            : base(stream, encoding)
        {
            if (null == converter)
                throw (new ArgumentNullException("converter"));
            this.m_endianConverter = converter;
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Writes a one-byte Boolean value to the current stream, with 0 representing
        //  false and 1 representing true.
        /// </summary>
        /// <param name="value">The Boolean value to write (0 or 1).</param>
        /// <exception cref="System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="System.ObjectDisposedException">The stream is closed.</exception>
        public override void Write(bool value)
        {
            m_endianConverter.CopyBytes(value, m_buffer, 0);
            WriteInternal(m_buffer, 16);
        }
        
        /// <summary>
        /// Writes a decimal value to the current stream and advances the stream position
        /// by sixteen bytes.
        /// </summary>
        /// <param name="value">The decimal value to write.</param>
        /// <exception cref="System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="System.ObjectDisposedException">The stream is closed.</exception>
        public override void Write(decimal value)
        {
            m_endianConverter.CopyBytes(value, m_buffer, 0);
            WriteInternal(m_buffer, 16);
        }
        
        /// <summary>
        /// Writes an eight-byte floating-point value to the current stream and advances
        /// the stream position by eight bytes.
        /// </summary>
        /// <param name="value">The eight-byte floating-point value to write.</param>
        /// <exception cref="System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="System.ObjectDisposedException">The stream is closed.</exception>
        public override void Write(double value)
        {
            m_endianConverter.CopyBytes(value, m_buffer, 0);
            WriteInternal(m_buffer, 8);
        }
        
        /// <summary>
        /// Writes a four-byte floating-point value to the current stream and advances
        /// the stream position by four bytes.
        /// </summary>
        /// <param name="value">The four-byte floating-point value to write.</param>
        /// <exception cref="System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="System.ObjectDisposedException">The stream is closed.</exception>
        public override void Write(float value)
        {
            m_endianConverter.CopyBytes(value, m_buffer, 0);
            WriteInternal(m_buffer, 4);
        }
        
        /// <summary>
        /// Writes a four-byte signed integer to the current stream and advances the
        /// stream position by four bytes.
        /// </summary>
        /// <param name="value">The four-byte signed integer to write.</param>
        /// <exception cref="System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="System.ObjectDisposedException">The stream is closed.</exception>
        public override void Write(int value)
        {
            m_endianConverter.CopyBytes(value, m_buffer, 0);
            WriteInternal(m_buffer, 4);
        }
                
        /// <summary>
        /// Writes an eight-byte signed integer to the current stream and advances the
        /// stream position by eight bytes.
        /// </summary>
        /// <param name="value">The eight-byte signed integer to write.</param>
        /// <exception cref="System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="System.ObjectDisposedException">The stream is closed.</exception>
        public override void Write(long value)
        {
            m_endianConverter.CopyBytes(value, m_buffer, 0);
            WriteInternal(m_buffer, 8);
        }
                        
        /// <summary>
        /// Writes a two-byte signed integer to the current stream and advances the stream
        /// position by two bytes.
        /// </summary>
        /// <param name="value">The two-byte signed integer to write.</param>
        /// <exception cref="System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="System.ObjectDisposedException">The stream is closed.</exception>
        public override void Write(short value)
        {
            m_endianConverter.CopyBytes(value, m_buffer, 0);
            WriteInternal(m_buffer, 2);
        }
                
        /// <summary>
        /// Writes a four-byte unsigned integer to the current stream and advances the
        /// stream position by four bytes.
        /// </summary>
        /// <param name="value">The four-byte unsigned integer to write.</param>
        /// <exception cref="System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="System.ObjectDisposedException">The stream is closed.</exception>
        public override void Write(uint value)
        {
            m_endianConverter.CopyBytes(value, m_buffer, 0);
            WriteInternal(m_buffer, 4);
        }
                
        /// <summary>
        /// Writes an eight-byte unsigned integer to the current stream and advances
        /// the stream position by eight bytes.
        /// </summary>
        /// <param name="value">The eight-byte unsigned integer to write.</param>
        /// <exception cref="System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="System.ObjectDisposedException">The stream is closed.</exception>
        public override void Write(ulong value)
        {
            m_endianConverter.CopyBytes(value, m_buffer, 0);
            WriteInternal(m_buffer, 2);
        }
                
        /// <summary>
        /// Writes a two-byte unsigned integer to the current stream and advances the
        /// stream position by two bytes.
        /// </summary>
        /// <param name="value">The two-byte unsigned integer to write.</param>
        /// <exception cref="System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="System.ObjectDisposedException">The stream is closed.</exception>
        public override void Write(ushort value)
        {
            m_endianConverter.CopyBytes(value, m_buffer, 0);
            WriteInternal(m_buffer, 2);
        }
        #endregion // Controller Methods

        #region IDisposable Interface
        /// <summary>
        /// 
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            m_disposing = true;
            base.Dispose(disposing);
        }
        #endregion // IDisposable Interface

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        private void CheckDisposed()
        {
            if (m_disposing)
            {
                throw (new ObjectDisposedException("EndianBinaryWriter"));
            }
        }

        /// <summary>
        /// Writes the given number of bytes from the stream.
        /// </summary>
        /// <param name="data">Buffer to write from.</param>
        /// <param name="size">Number of bytes to write.</param>
        void WriteInternal(Byte[] data, int size)
        {
            CheckDisposed();
            this.BaseStream.Write(data, 0, size);
        }
        #endregion // Private Methods
    }

} // RSG.Base.IO namespace
