using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace RSG.Base.IO
{

    /// <summary>
    /// 
    /// </summary>
    public static class StringConversion
    {
        /// <summary>
        /// Just like Int32.Parse or Convert.ToInt32 but it handles 0x prefixed hex values too
        /// </summary>
        /// <param name="s">The string to parse</param>
        /// <returns>A converted unsigned 32 bit integer</returns>
        public static uint ToUint(String s) 
        {
            if (s.Length > 2 && s[0] == '0' && s[1] == 'x')
            {
                return Convert.ToUInt32(s, 16);
            }
            return Convert.ToUInt32(s, 10);
        }

        public static int ToInt(String s) 
        {
            if (s.Length > 2 && s[0] == '0' && s[1] == 'x')
            {
                return Convert.ToInt32(s, 16);
            }
            return Convert.ToInt32(s, 10);
        }

        /// <summary>
        /// DHM :: OBSOLETE: this "clean" removes whitespace.  Bit odd no?  And its
        /// only use I found was in Log.cs and the resultant string was never
        /// used anyway!!!! We need to clean log strings for XML output (for that see
        /// http://weblogs.sqlteam.com/mladenp/archive/2008/10/21/Different-ways-how-to-escape-an-XML-string-in-C.aspx).
        /// </summary>
        /// <param name="strIn"></param>
        /// <returns></returns>
        [Obsolete]
        public static string CleanInput(string strIn)
        {
            // Replace invalid characters with empty strings.
            strIn = Regex.Replace(strIn, @"[^\w\.@-]", "");
            return strIn;
        }
    }

} // RSG.Base.IO namespace
