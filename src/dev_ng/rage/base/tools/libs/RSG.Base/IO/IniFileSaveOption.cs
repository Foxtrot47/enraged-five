﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.IO
{

    /// <summary>
    /// 
    /// </summary>
    [Flags]
    public enum IniFileSaveOption
    {
        None,

        SpacesBetweenKeysAndValues, // Insert space characters between keys and values.
    }

} // RSG.Base.IO namespace
