﻿using System;
using System.IO;
using System.Text;
using RSG.Base.Conversion;

namespace RSG.Base.IO
{

    /// <summary>
    /// 
    /// </summary>
    public class EndianBinaryReader : BinaryReader
    {
        #region Member Data
        /// <summary>
        /// Endian bit converter object.
        /// </summary>
        private EndianBitConverter m_endianConverter;

        /// <summary>
        /// Buffer used for temporary storage before conversion into primitives
        /// </summary>
        private Byte[] m_buffer = new Byte[16];

        /// <summary>
        /// Local disposing flag.
        /// </summary>
        private bool m_disposing = false;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="converter"></param>
        /// <param name="stream"></param>
        public EndianBinaryReader(EndianBitConverter converter, Stream stream)
            : this(converter, stream, Encoding.UTF8)
        {

        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="converter"></param>
        /// <param name="stream"></param>
        /// <param name="encoding"></param>
        public EndianBinaryReader(EndianBitConverter converter, Stream stream, Encoding encoding)
            : base(stream, encoding)
        {
            if (null == converter)
                throw (new ArgumentNullException("converter"));
            this.m_endianConverter = converter;
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Reads a Boolean value from the current stream and advances the current position
        //  of the stream by one byte.
        /// </summary>
        /// <returns>true if the byte is nonzero; otherwise, false.</returns>
        /// <exception cref="System.IO.EndOfStreamException">The end of the stream is reached.</exception>
        /// <exception cref="System.ObjectDisposedException">The stream is closed.</exception>
        /// <exception cref="System.IO.IOException">An I/O error occurs.</exception>
        public override bool ReadBoolean()
        {
            ReadInternal(m_buffer, 1);
            return (this.m_endianConverter.ToBoolean(m_buffer, 0));
        }

        /// <summary>
        /// Reads a decimal value from the current stream and advances the current position
        //  of the stream by sixteen bytes.
        /// </summary>
        /// <returns>A decimal value read from the current stream.</returns>
        /// <exception cref="System.IO.EndOfStreamException">The end of the stream is reached.</exception>
        /// <exception cref="System.ObjectDisposedException">The stream is closed.</exception>
        /// <exception cref="System.IO.IOException">An I/O error occurs.</exception>
        public override decimal ReadDecimal()
        {
            ReadInternal(m_buffer, 16);
            return (m_endianConverter.ToDecimal(m_buffer, 0));
        }

        /// <summary>
        /// Reads an 8-byte floating point value from the current stream and advances
        //     the current position of the stream by eight bytes.
        /// </summary>
        /// <returns>An 8-byte floating point value read from the current stream.</returns>
        /// <exception cref="System.IO.EndOfStreamException">The end of the stream is reached.</exception>
        /// <exception cref="System.ObjectDisposedException">The stream is closed.</exception>
        /// <exception cref="System.IO.IOException">An I/O error occurs.</exception>
        public override double ReadDouble()
        {
            ReadInternal(m_buffer, 8);
            return (m_endianConverter.ToDouble(m_buffer, 0));
        }
                
        /// <summary>
        /// Reads a 2-byte signed integer from the current stream and advances the current
        //  position of the stream by two bytes.
        /// </summary>
        /// <returns>A 2-byte signed integer read from the current stream.</returns>
        /// <exception cref="System.IO.EndOfStreamException">The end of the stream is reached.</exception>
        /// <exception cref="System.ObjectDisposedException">The stream is closed.</exception>
        /// <exception cref="System.IO.IOException">An I/O error occurs.</exception>
        public override short ReadInt16()
        {
            ReadInternal(m_buffer, 2);
            return (m_endianConverter.ToInt16(m_buffer, 0));
        }

        /// <summary>
        /// Reads a 4-byte signed integer from the current stream and advances the current
        /// position of the stream by four bytes.
        /// </summary>
        /// <returns>A 4-byte signed integer read from the current stream.</returns>
        /// <exception cref="System.IO.EndOfStreamException">The end of the stream is reached.</exception>
        /// <exception cref="System.ObjectDisposedException">The stream is closed.</exception>
        /// <exception cref="System.IO.IOException">An I/O error occurs.</exception>
        public override int ReadInt32()
        {
            ReadInternal(m_buffer, 4);
            return (m_endianConverter.ToInt32(m_buffer, 0));
        }

        /// <summary>
        /// Reads an 8-byte signed integer from the current stream and advances the current
        /// position of the stream by eight bytes.
        /// </summary>
        /// <returns>An 8-byte signed integer read from the current stream.</returns>
        /// <exception cref="System.IO.EndOfStreamException">The end of the stream is reached.</exception>
        /// <exception cref="System.ObjectDisposedException">The stream is closed.</exception>
        /// <exception cref="System.IO.IOException">An I/O error occurs.</exception>
        public override long ReadInt64()
        {
            ReadInternal(m_buffer, 8);
            return (m_endianConverter.ToInt64(m_buffer, 0));
        }
        
        /// <summary>
        /// Reads a 4-byte floating point value from the current stream and advances
        /// the current position of the stream by four bytes.
        /// </summary>
        /// <returns>A 4-byte floating point value read from the current stream.</returns>
        /// <exception cref="System.IO.EndOfStreamException">The end of the stream is reached.</exception>
        /// <exception cref="System.ObjectDisposedException">The stream is closed.</exception>
        /// <exception cref="System.IO.IOException">An I/O error occurs.</exception>
        public override float ReadSingle()
        {
            ReadInternal(m_buffer, 4);
            return (m_endianConverter.ToSingle(m_buffer, 0));
        }
        
        /// <summary>
        /// Reads a 2-byte unsigned integer from the current stream using little-endian
        /// encoding and advances the position of the stream by two bytes.
        /// </summary>
        /// <returns>A 2-byte unsigned integer read from this stream.</returns>
        /// <exception cref="System.IO.EndOfStreamException">The end of the stream is reached.</exception>
        /// <exception cref="System.ObjectDisposedException">The stream is closed.</exception>
        /// <exception cref="System.IO.IOException">An I/O error occurs.</exception>
        public override ushort ReadUInt16()
        {
            ReadInternal(m_buffer, 2);
            return (m_endianConverter.ToUInt16(m_buffer, 0));
        }
        
        /// <summary>
        /// Reads a 4-byte unsigned integer from the current stream and advances the
        /// position of the stream by four bytes.
        /// </summary>
        /// <returns>A 4-byte unsigned integer read from this stream.</returns>
        /// <exception cref="System.IO.EndOfStreamException">The end of the stream is reached.</exception>
        /// <exception cref="System.ObjectDisposedException">The stream is closed.</exception>
        /// <exception cref="System.IO.IOException">An I/O error occurs.</exception>
        public override uint ReadUInt32()
        {
            ReadInternal(m_buffer, 4);
            return (m_endianConverter.ToUInt32(m_buffer, 0));
        }
        
        /// <summary>
        /// Reads an 8-byte unsigned integer from the current stream and advances the
        /// position of the stream by eight bytes.
        /// </summary>
        /// <returns>An 8-byte unsigned integer read from this stream.</returns>
        /// <exception cref="System.IO.EndOfStreamException">The end of the stream is reached.</exception>
        /// <exception cref="System.ObjectDisposedException">The stream is closed.</exception>
        /// <exception cref="System.IO.IOException">An I/O error occurs.</exception>
        public override ulong ReadUInt64()
        {
            ReadInternal(m_buffer, 8);
            return (m_endianConverter.ToUInt64(m_buffer, 0));
        }
        #endregion // Controller Methods

        #region IDisposable Interface
        /// <summary>
        /// 
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            m_disposing = true;
            base.Dispose(disposing);
        }
        #endregion // IDisposable Interface

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        private void CheckDisposed()
        {
            if (m_disposing)
            {
                throw (new ObjectDisposedException("EndianBinaryReader"));
            }
        }

        /// <summary>
        /// Reads the given number of bytes from the stream, throwing an exception
        /// if they can't all be read.
        /// </summary>
        /// <param name="data">Buffer to read into</param>
        /// <param name="size">Number of bytes to read</param>
        void ReadInternal(Byte[] data, int size)
        {
            CheckDisposed();
            int index = 0;
            while (index < size)
            {
                int read = this.BaseStream.Read(data, index, size - index);
                if (read == 0)
                {
                    throw new EndOfStreamException
                        (String.Format("End of stream reached with {0} byte {1} left to read.", size - index,
                        size - index == 1 ? "s" : ""));
                }
                index += read;
            }
        }
        #endregion // Private Methods
    }

} // RSG.Base.IO namespace
