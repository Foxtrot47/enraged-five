﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;

namespace RSG.Base.IO
{

    /// <summary>
    /// Methods to compute a consistent 128-bit MD5 checksum for a file.
    /// </summary>
    public static class FileMD5
    {
        /// <summary>
        /// Empty MD5.
        /// </summary>
        public static Byte[] Empty = new Byte[16] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

        /// <summary>
        /// Compute consistent 128-bit MD5 checksum for a file.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static Byte[] ComputeMD5(String filename)
        {
            using (FileStream stream = new FileStream(filename, FileMode.Open, FileAccess.Read))
                return (ComputeMD5(stream));
        }

        /// <summary>
        /// Compute consistent 128-bit MD5 checksum for a stream; maintaining the 
        /// position in the stream.
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public static Byte[] ComputeMD5(Stream stream)
        {
            long pos = stream.Position;
            MD5Cng hasher = new MD5Cng();
            Byte[] hash = hasher.ComputeHash(stream);
            stream.Seek(pos, SeekOrigin.Begin);
            return (hash);
        }

        /// <summary>
        /// Determine whether two Byte[] MD5 checksums are the same.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        public static bool Equal(Byte[] a, Byte[] b)
        {
            return (a.SequenceEqual(b));
        }

        /// <summary>
        /// Return 128-bit MD5 checksum as pretty string.
        /// </summary>
        /// <param name="md5"></param>
        /// <returns></returns>
        public static String Format(Byte[] md5)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            for (int i = 0; i < md5.Length; i++)
            {
                sb.Append(md5[i].ToString("x2"));
            }
            return (sb.ToString());
        }
    }

} // RSG.Base.IO namespace
