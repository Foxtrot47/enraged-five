//
// File: cFileUtil.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of cFileUtil.cs class
//

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace RSG.Base.IO
{

    /// <summary>
    /// File utilities class
    /// </summary>
    public class cFileUtil
    {
        #region Constants
        static public readonly string WildCard_AllFiles = "*.*";
        #endregion

        static public String GetRootMostDir(List<String> paths)
        {
            if (paths.Count < 1)
                return "";

            List<String> common = new List<String>(Path.GetFullPath(paths[0]).Split(new char[] {'\\'} ));

            for (int i = 1; i < paths.Count; i++)
            {
                String[] current = Path.GetFullPath(paths[i]).Split(new char[] { '\\' });
                int j;

                for (j = 0; j < common.Count; j++)
                {
                    if (common[j] != current[j])
                    {
                        common.RemoveRange(j,common.Count - j);
                        break;
                    }
                }
            }

            common[0] = common[0] + "\\";
            String ret = Path.Combine(common.ToArray());

            return ret;
        }

        /// <summary>
        /// Returns whether the file has read-only attributes set.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        static public bool IsFileReadOnly(string path)
        {
            if (File.Exists(path))
            {
                FileAttributes attributes = File.GetAttributes(path);
                if ((attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Set all files matching filter in path with a set of attributes
        /// </summary>
        /// <param name="sPath"></param>
        /// <param name="sFilter"></param>
        /// <param name="bRecursive"></param>
        /// <param name="attrs"></param>
        static public void SetFileAttributes(string sPath, string sFilter, bool bRecursive, FileAttributes attrs)
        {
            if (!Directory.Exists(sPath))
                throw new DirectoryNotFoundException("Path does not exist");
            if (0 == sFilter.Length)
                throw new FormatException("Filter must contain characters");

            string[] asFiles;
            if (bRecursive)
                asFiles = Directory.GetFiles(sPath, sFilter, SearchOption.AllDirectories);
            else
                asFiles = Directory.GetFiles(sPath, sFilter, SearchOption.TopDirectoryOnly);
            
            try
            {
                foreach (string sFile in asFiles)
                {
                    if (!File.Exists(sFile))
                        continue;

                    File.SetAttributes(sFile, attrs);
                }
            }
            catch (Exception)
            {
                throw;
            }
        } // End SetFileAttributes

        /// <summary>
        /// Clear specified file attributes from all files matching filter in path
        /// </summary>
        /// <param name="sPath"></param>
        /// <param name="sFilter"></param>
        /// <param name="bRecursive"></param>
        /// <param name="attrToClear"></param>
        static public void ClearFileAttributes(string sPath, string sFilter, bool bRecursive, FileAttributes attrToClear)
        {
            if (!Directory.Exists(sPath))
                throw new DirectoryNotFoundException("Path does not exist");
            if (0 == sFilter.Length)
                throw new FormatException("Filter must contain characters");

            string[] asFiles;
            if (bRecursive)
                asFiles = Directory.GetFiles(sPath, sFilter, SearchOption.AllDirectories);
            else
                asFiles = Directory.GetFiles(sPath, sFilter, SearchOption.TopDirectoryOnly);

            try
            {
                foreach (string sFile in asFiles)
                {
                    if (!File.Exists(sFile))
                        continue;

                    uint currentAttrs = (uint)(File.GetAttributes(sFile));
                    uint clearAttrs = (uint)(attrToClear);

                    if ((currentAttrs & clearAttrs) == clearAttrs)
                    {
                        FileAttributes newAttrs = (FileAttributes)(currentAttrs & ~clearAttrs);
                        File.SetAttributes(sFile, newAttrs);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        } // End ClearFileAttributes

        /// <summary>
        /// Shortens a pathname by either removing consecutive components of a path
        /// and/or by removing characters from the end of the filename and replacing
        /// then with three elipses (...)
        ///
        /// In all cases, the root of the passed path will be preserved in it's entirety.
        ///
        /// If a UNC path is used or the sPath and nMaxLength are particularly short,
        /// the resulting path may be longer than maxLength.
        ///
        /// This method expects fully resolved pathnames to be passed to it.
        /// (Use Path.GetFullPath() to obtain this.)        /// </summary>
        /// <param name="sPath"></param>
        /// <param name="nMaxLength"></param>
        /// <returns></returns>
        static public String ShortenPathname(String sPath, int nMaxLength)
        {
            if (sPath.Length <= nMaxLength)
                return sPath;

            String root = Path.GetPathRoot(sPath);
            if (root.Length > 3)
                root += Path.DirectorySeparatorChar;

            String[] elements = sPath.Substring(root.Length).Split(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

            int filenameIndex = elements.GetLength(0) - 1;

            if (elements.GetLength(0) == 1) // pathname is just a root and filename
            {
                if (elements[0].Length > 5) // long enough to shorten
                {
                    // if path is a UNC path, root may be rather long
                    if (root.Length + 6 >= nMaxLength)
                    {
                        return root + elements[0].Substring(0, 3) + "...";
                    }
                    else
                    {
                        return sPath.Substring(0, nMaxLength - 3) + "...";
                    }
                }
            }
            else if ((root.Length + 4 + elements[filenameIndex].Length) > nMaxLength) // pathname is just a root and filename
            {
                root += "...\\";

                int len = elements[filenameIndex].Length;
                if (len < 6)
                    return root + elements[filenameIndex];

                if ((root.Length + 6) >= nMaxLength)
                {
                    len = 3;
                }
                else
                {
                    len = nMaxLength - root.Length - 3;
                }
                return root + elements[filenameIndex].Substring(0, len) + "...";
            }
            else if (elements.GetLength(0) == 2)
            {
                return root + "...\\" + elements[1];
            }
            else
            {
                int len = 0;
                int begin = 0;

                for (int i = 0; i < filenameIndex; i++)
                {
                    if (elements[i].Length > len)
                    {
                        begin = i;
                        len = elements[i].Length;
                    }
                }

                int totalLength = sPath.Length - len + 3;
                int end = begin + 1;

                while (totalLength > nMaxLength)
                {
                    if (begin > 0)
                        totalLength -= elements[--begin].Length - 1;

                    if (totalLength <= nMaxLength)
                        break;

                    if (end < filenameIndex)
                        totalLength -= elements[++end].Length - 1;

                    if (begin == 0 && end == filenameIndex)
                        break;
                }

                // assemble final string

                for (int i = 0; i < begin; i++)
                {
                    root += elements[i] + '\\';
                }

                root += "...\\";

                for (int i = end; i < filenameIndex; i++)
                {
                    root += elements[i] + '\\';
                }

                return root + elements[filenameIndex];
            }
            return sPath;
        } // End ShortenPathname
    }

} // End of RSG.Base.IO namespace

// End of file
