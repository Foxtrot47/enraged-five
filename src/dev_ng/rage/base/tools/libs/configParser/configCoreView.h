#ifndef __CONFIGPARSER_CONFIGCOREVIEW_H__
#define __CONFIGPARSER_CONFIGCOREVIEW_H__

//
// filename:	configCoreView.h
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		18 December 2009
// description:	Base view of configuration data, including environment substitution.
//

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

// --- Includes -----------------------------------------------------------------

// Local headers
#include "configUtil\configutil.h"
#include "configParser.h"
#include "configModel.h"
#include "configUserType.h"

// STL headers
#include <string>

// --- Forward Declarations -----------------------------------------------------
namespace configUtil
{
	class Environment;
}

// --- Classes ------------------------------------------------------------------

BEGIN_CONFIGPARSER_NS

//PURPOSE
// The root configuration data view.
//
// It is not intended that this class be extended through inheritance but should
// be used by all other views.
//
// Overloaded set methods are provided to allow write access to the configuration
// data.  If you only require read access its recommended you create a constant
// instance of a ConfigCoreView object.
//
//   const ConfigCoreView configView; // read-only access
//   ConfigCoreView configViewRW;     // read/write access
//
// This also applies to higher-level view objects.  If you attempt a call to a
// set method with a const object you will get compile errors.
//
class ConfigCoreView
{
public:
	enum SettingsLocation
	{	
		INDETERMINATE = 0,
		// NETWORK (not valid save location but added to keep consistency.
		GLOBAL = 2,
		PROJECT = 3,
	};

	ConfigCoreView( );
	~ConfigCoreView( );

	bool GetToolsRootDir( TCHAR* path, size_t size ) const;
	bool GetToolsBinDir( TCHAR* path, size_t size ) const;
	bool GetToolsLibDir( TCHAR* path, size_t size ) const;
	bool GetToolsConfigDir( TCHAR* path, size_t size ) const;
	bool GetToolsRuby( TCHAR* path, size_t size ) const;
	bool GetToolsLogDir( TCHAR* path, size_t size ) const;
	bool GetToolsTempDir( TCHAR* path, size_t size ) const;

	// User Type methods
	size_t GetNumUserTypes( ) const;
	bool GetUserType( size_t index, UserType& userType ) const;

	// User methods
	bool GetUserName( TCHAR* username, size_t size ) const;
	bool SetUserName( const TCHAR* username );
	bool GetUserEmailAddress( TCHAR* email, size_t size ) const;
	bool SetUserEmailAddress( const TCHAR* email );
	bool GetUserFlags( unsigned int& flags ) const;
	bool SetUserFlags( unsigned int flags );
	bool GetStudio( TCHAR* studio, size_t size ) const;
	bool GetStudioNetworkDrive( TCHAR* networkdrive, size_t size ) const;
	bool GetStudioTimeZoneOffset( int& timeZoneOffset ) const;

	// XGE methods
	bool GetUseXoreaxXGE( ) const;
	bool SetUseXoreaxXGE( bool set );

	// Logging methods
	bool GetLogToStdOut( ) const;
	bool SetLogToStdOut( bool value );
	int GetLogLevel( ) const;
	bool SetLogLevel( int level );
	bool GetLogTraceInfo( ) const;
	bool SetLogTraceInfo( bool value );
	bool GetLogMailErrors( ) const;
	bool SetLogMailErrors( bool value );
	int GetLogMailAddresses( std::vector<const TCHAR *> &values);
	bool GetLogMailServerHost( TCHAR* server, size_t size ) const;
	int GetLogMailServerPort( ) const;

	// DCC Application methods
	typedef struct _DCCPackage
	{
		TCHAR version[_MAX_PATH];
		TCHAR installdir[_MAX_PATH];
		TCHAR platform[_MAX_PATH];
	} DCCPackage;
	size_t GetDCCPackageCount( const TCHAR* package );
	bool GetDCCPackageDetails( const TCHAR* package, size_t index, DCCPackage& details ) const;

	// Config XML filename functions.
	bool GetNetworkConfigFilename( TCHAR* path, size_t size ) const;
	bool GetGlobalConfigFilename( TCHAR* path, size_t size ) const;
	bool GetGlobalLocalConfigFilename( TCHAR* path, size_t size ) const;
	bool GetProjectConfigFilename( TCHAR* path, size_t size ) const;
	bool GetProjectLocalConfigFilename( TCHAR* path, size_t size ) const;

	bool GetConfigData(const TCHAR* config, const TCHAR* expr, TCHAR* data, size_t size) const;

	// Query methods...
	bool query( const TCHAR* expr, ConfigModelData& data ) const;
	bool query( const TCHAR* expr, TCHAR* data, size_t size ) const;
	bool query( const TCHAR* expr, bool& data ) const;
	bool query( const TCHAR* expr, int& data ) const;
	bool query( const TCHAR* expr, double& data ) const;

	// DHM 2011/02/28 - these should go once our global "local" and project "local"
	// XML files use different root paths.
	bool query_global( const TCHAR* expr, ConfigModelData& data ) const;
	bool query_global( const TCHAR* expr, TCHAR* data, size_t size ) const;
	bool query_global( const TCHAR* expr, bool& data ) const;
	bool query_global( const TCHAR* expr, int& data ) const;
	bool query_global( const TCHAR* expr, double& data ) const;

	// Data set methods...
	bool set( const TCHAR* expr, const ConfigModelData& data, bool flush = false, SettingsLocation = INDETERMINATE, bool add = false );
	bool set( const TCHAR* expr, const TCHAR* data, bool flush = false, SettingsLocation = INDETERMINATE, bool add = false );
	bool set( const TCHAR* expr, const bool data, bool flush = false, SettingsLocation = INDETERMINATE, bool add = false );
	bool set( const TCHAR* expr, const int data, bool flush = false, SettingsLocation = INDETERMINATE, bool add = false );
	bool set( const TCHAR* expr, const double data, bool flush = false, SettingsLocation = INDETERMINATE, bool add = false );

	bool remove( const TCHAR* expr, bool flush = false, SettingsLocation = INDETERMINATE  );

	bool Save();

private:
	// Hide copy constructor and assignment operator.
	ConfigCoreView( const ConfigCoreView& copy );
	ConfigCoreView& operator=( ConfigCoreView& copy );

	void FetchStudio();

	bool query_model_raw( const TCHAR* expr, const ConfigModel& model, ConfigModelData& data ) const;
	bool set_model_raw( const TCHAR* expr, ConfigModel& model, const ConfigModelData& data, const bool flush, bool add = false );

	// Filenames
	TCHAR					m_sNetworkConfig[_MAX_PATH];
	TCHAR					m_sGlobalConfig[_MAX_PATH];
	TCHAR					m_sGlobalLocalConfig[_MAX_PATH];
	TCHAR					m_sProjectConfig[_MAX_PATH];
	TCHAR					m_sProjectLocalConfig[_MAX_PATH];

	TCHAR					m_sStudioName[_MAX_PATH];
	TCHAR					m_sStudioNetworkDrive[_MAX_PATH];
	int						m_sStudioServerTimeOffset;

	configUtil::Environment* m_pEnv;		// Config environment object.
	ConfigModel				m_GlobalModel;	// Global configuration model.
	ConfigModel				m_GlobalLocalModel;
	ConfigModel				m_ProjectModel;	// Project configuration model.
	ConfigModel				m_ProjectLocalModel;
};

END_CONFIGPARSER_NS

#endif // __CONFIGPARSER_CONFIGCOREVIEW_H__
