#ifndef __CONFIGPARSER_CONFIGMODEL_H__
#define __CONFIGPARSER_CONFIGMODEL_H__

//
// filename:	configModel.h
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		18 December 2009
// description:	Config data model.
//

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

// --- Includes -----------------------------------------------------------------
#include <assert.h>

// Local headers
#include "configParser/configParser.h"
#include "configParser/configModelData.h"

// configUtil headers
#include "configUtil/scoped_mutex.h"

// LibXml2 headers
#include "libxml/parser.h"
#include "libxml/xpath.h"

// --- Forward Declarations -----------------------------------------------------
// None

// --- Classes ------------------------------------------------------------------

BEGIN_CONFIGPARSER_NS

//PURPOSE
// The configModel class provides a read/write interface to a configuration XML
// file.  The config view being used may create multiple configModel objects,
// one for each configuration data file being queried.
//
// E.g. one for our root configuration data (%TOOLSROOT%/config.xml) and one for
// the project configuration data (%TOOLSCONFIG%/project.xml).
//
class ConfigModel
{
public:
	ConfigModel( );
	ConfigModel( const TCHAR* filename );
	~ConfigModel( );

	//PURPOSE
	// Load a XML document from absolute filename.
	bool load( const TCHAR* filename );

	//PURPOSE
	// Lookup XML data from an XPath expression string.
	// No changes are made to the data.
	//
	// Returns true if the data was found, false otherwise.
	bool query( const TCHAR* expr, ConfigModelData& data ) const;

	//PURPOSE
	// Set XML data from an XPath expression string.
	// The data is optionally flushed to the XML file.
	//
	// The XPath expression must already exist, unless the add flag is set to true
	// Returns true if the data was set, false otherwise.
	bool set( const TCHAR* expr, const ConfigModelData& data, bool flush = false, bool add = false );

	// The XPath expression must not already exist.
	// Returns true if the data was set, false otherwise.
	bool add( const TCHAR* expr, const ConfigModelData& data, bool flush = false );

	// The XPath expression must already exist.
	// Returns true if the data was removed, false otherwise.
	bool remove( const TCHAR* expr, bool flush = false );

	//PURPOSE
	// Save any modifications to our XML data.
	bool save( );

private:
	// Hide copy constructor and assignment operator.
	ConfigModel( const ConfigModel& copy );
	ConfigModel& operator=( ConfigModel& copy );

	void xmlNodeGetContent( const xmlNode& node, TCHAR* data, size_t size ) const; 

	mutable configUtil::ScopedMutex::Mutex	m_mutex;		//!< Mutex lock for LibXML2 access.
	TCHAR				m_sFilename[_MAX_PATH];	//!< XML filename
	xmlDocPtr			m_pXmlDoc;				//!< XML document
	xmlXPathContextPtr	m_pXPathCtx;			//!< XPath context
};

//PURPOSE
// The configModelException is a std::runtime_error subclass that 
// indicates an error with a configModel method invocation.
//
class configModelException //: public std::runtime_error
{
public:
	configModelException( const char* const& /*message*/ )
		//: std::runtime_error( message )
	{
	}
};

END_CONFIGPARSER_NS

#endif // __CONFIGPARSER_CONFIGCOREVIEW_H__
