#ifndef __CONFIGPARSER_CONTENTNODETEMPLATES_H__
#define __CONFIGPARSER_CONTENTNODETEMPLATES_H__

//
// filename:	contentNodeTemplates.h
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		29 March 2010
// description:	Content-tree template node classes.
//

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

// --- Includes -----------------------------------------------------------------

// Local headers
#include "contentNodeGroup.h"

// --- Classes ------------------------------------------------------------------

BEGIN_CONFIGPARSER_NS

//PURPOSE
// 'template:file:each' template node.
//
class ContentNodeTemplateFileEach : public ContentNodeGroup
{
public:
	ContentNodeTemplateFileEach( ContentNode* parent, 
		const configUtil::Environment* pEnv,
		const TCHAR* name, 
		const TCHAR* type );
	ContentNodeTemplateFileEach( ContentNode* parent, 
		const configUtil::Environment* pEnv, 
		xmlElementPtr pElement );
	virtual ~ContentNodeTemplateFileEach( );

	//PURPOSE
	// Return original search filter.
	const TCHAR*			GetSearchFilter() const { return ( m_sSearch ); }

	//PURPOSE
	// Return xmlNodePtr representing this content node.
	virtual xmlNodePtr			ToXml() const;

	//PURPOSE
	// Implement the PostLoadCallback to run the template and populate
	// group children correctly.
	virtual void				PostLoadCallback( );

	// Static Methods
	static const char*			GetXmlType( ) { return ( "template:file:each" ); }

protected:
	// Hide copy constructor and assignment operator.
	ContentNodeTemplateFileEach( const ContentNodeTemplateFileEach& copy );
	ContentNodeTemplateFileEach& operator=( ContentNodeTemplateFileEach& copy );

	TCHAR			m_sSearch[_MAX_PATH];		//!< Search filter.
	NodeContainer	m_vXmlChildren;	//!< Non-automatic content nodes (for writing purposes).
};

END_CONFIGPARSER_NS

#endif // __CONFIGPARSER_CONTENTNODETEMPLATES_H__
