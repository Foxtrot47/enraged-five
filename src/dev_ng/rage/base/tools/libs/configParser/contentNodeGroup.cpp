//
// filename:	contentNodeGroup.cpp
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		11 February 2010
// description:	Content-tree group node class.
//

// LibXml2 headers
#include "libxml/parser.h"
#include "libxml/xpath.h"

// Local headers
#include "configParser/contentNodeGroup.h"

BEGIN_CONFIGPARSER_NS

ContentNodeGroup::ContentNodeGroup( ContentNode* parent,
								   const configUtil::Environment* pEnv,
								   const TCHAR* name, 
								   const TCHAR* path )
	: ContentNode( parent, pEnv, name, _T("group") )
{
#ifdef UNICODE
	wcsncpy_s( m_sRelativePath, _MAX_PATH, path, _TRUNCATE );
#else
	strncpy_s( m_sRelativePath, _MAX_PATH, path, _TRUNCATE );
#endif // UNICODE
}

ContentNodeGroup::ContentNodeGroup( ContentNode* parent,
								   const configUtil::Environment* pEnv, 
								   xmlElement* pElement )
	: ContentNode( parent, pEnv, pElement )
{
	memset( m_sRelativePath, 0, sizeof( TCHAR ) * _MAX_PATH );

	xmlAttributePtr pAttribute = pElement->attributes;
	while ( pAttribute )
	{
		if ( 0 == xmlStrcasecmp( BAD_CAST "path", pAttribute->name ) )
		{
			configUtil::AnsiToTChar( m_sRelativePath, _MAX_PATH, 
				(char*)pAttribute->children->content );
		}

		// Next attribute.
		pAttribute = (xmlAttributePtr)( pAttribute->next );
	}
}

ContentNodeGroup::~ContentNodeGroup( )
{	
	ContentNode::NodeContainerIter it = BeginChildren();
	while ( it != EndChildren() )
	{
		it = RemoveChild( it );
	}
	m_vChildren.clear( );
}

void
ContentNodeGroup::GetRelativePath( TCHAR* path, size_t size ) const
{
#ifdef UNICODE
	wcsncpy_s( path, _MAX_PATH, m_sRelativePath, _TRUNCATE );
#else
	strncpy_s( path, _MAX_PATH, m_sRelativePath, _TRUNCATE );
#endif // UNICODE
	m_pEnv->subst( path, size );
}

void
ContentNodeGroup::GetAbsolutePath( TCHAR* path, size_t size ) const
{
	GetRawAbsolutePath( path, size );
	m_pEnv->subst( path, size );
}

void
ContentNodeGroup::GetRawAbsolutePath( TCHAR* path, size_t size ) const
{
	STL_STRING path2;
	const ContentNodeGroup* pParent = 
		dynamic_cast<const ContentNodeGroup*>( this->GetParent() );

	if ( pParent )
	{
		TCHAR absolutePath[_MAX_PATH] = {0};
		pParent->GetAbsolutePath( absolutePath, _MAX_PATH );

#ifdef UNICODE
		size_t len = wcslen( absolutePath );
#else
		size_t len = strlen( absolutePath );
#endif // UNICODE
		if ( len > 0 )
		{
			path2.append( absolutePath );
			path2.append( _T("/") );
		}
	}
	path2.append( m_sRelativePath );
#ifdef UNICODE
	wcsncpy_s( path, size, path2.c_str(), _TRUNCATE );
#else
	strncpy_s( path, size, path2.c_str(), _TRUNCATE );
#endif // UNICODE
}

xmlNodePtr	
ContentNodeGroup::ToXml() const
{
	xmlNodePtr pElement = ContentNode::ToXml();
	char ansiRelativePath[_MAX_PATH] = {0};
	configUtil::TCharToAnsi( ansiRelativePath, _MAX_PATH, m_sRelativePath );
	size_t len = strlen( ansiRelativePath );

	if ( len > 0 )
		xmlNewProp( pElement, BAD_CAST "path", BAD_CAST ansiRelativePath );
	for ( NodeContainerConstIter it = BeginChildren();
		  it != EndChildren();
		  ++it )
	{
		xmlNodePtr pChild = (*it)->ToXml( );
		xmlAddChild( pElement, pChild );
	}

	return ( pElement );
}

bool
ContentNodeGroup::ToXmlDocument( const TCHAR* filename ) const
{
	xmlNodePtr pNodePtr = ToXml( );
	xmlDocPtr pDoc = xmlNewDoc( BAD_CAST "1.0" );
	xmlDocSetRootElement( pDoc, pNodePtr );

	char ansiFilename[_MAX_PATH] = {0};
	configUtil::TCharToAnsi( ansiFilename, _MAX_PATH, filename );

	int bytesWritten = xmlSaveFormatFile( ansiFilename, pDoc, 1 );
	xmlFreeDoc( pDoc );

	xmlCleanupParser( );

	return (bytesWritten > 0);
}

ContentNode*
ContentNodeGroup::FindFirst( const STL_STRING& name, const STL_STRING& type )
{
	// Base-case, trying to find self?!
	if ( ( 0 == name.compare( m_sName ) ) && ( 0 == type.compare( _T("") ) ) )
		return ( static_cast<ContentNode*>( this ) );
	if ( ( 0 == name.compare( m_sName ) ) && ( 0 == type.compare( m_sType ) ) )
		return ( static_cast<ContentNode*>( this ) );

	// Otherwise iterate into our children.
	for ( NodeContainerConstIter it = BeginChildren();
		  it != EndChildren();
		  ++it )
	{
		if ( dynamic_cast<ContentNodeGroup*>( *it ) )
		{
			ContentNodeGroup* pGroup = dynamic_cast<ContentNodeGroup*>( *it );
			ContentNode* pNode = pGroup->FindFirst( name, type );
			if ( pNode )
				return ( pNode );
		}
		else
		{
			if ( ( 0 == name.compare( (*it)->GetName() ) ) && 
				 ( 0 == type.compare( _T("") ) ) )
				 return ( *it );

			if ( ( 0 == name.compare( (*it)->GetName() ) ) && 
				 ( 0 == type.compare( (*it)->GetType() ) ) )
				 return ( *it );
		}
	}

	return ( NULL );
}

ContentNode*
ContentNodeGroup::FindFirstGroup( const STL_STRING& name )
{
	return ( FindFirst( name, _T("group") ) );
}

size_t
ContentNodeGroup::FindAll( const STL_STRING& type, std::vector<ContentNode*>& nodes )
{
	// Recurse through this group and any children looking for nodes of 
	// specified type.
	for ( NodeContainerIter it = BeginChildren();
		  it != EndChildren();
		  ++it )
	{
		if ( dynamic_cast<ContentNodeGroup*>( *it ) )
		{
			ContentNodeGroup* pGroup = dynamic_cast<ContentNodeGroup*>( *it );
			pGroup->FindAll( type, nodes );
		}

		if ( 0 == type.compare( (*it)->GetType() ) )
			nodes.push_back( *it );
	}
	return ( nodes.size() );
}

size_t
ContentNodeGroup::FindAll( const STL_STRING& name, const STL_STRING& type, std::vector<ContentNode*>& nodes )
{
	// Recurse through this group and any children looking for nodes of 
	// specified type.
	for ( NodeContainerConstIter it = BeginChildren();
		  it != EndChildren();
		  ++it )
	{
		if ( dynamic_cast<ContentNodeGroup*>( *it ) )
		{
			ContentNodeGroup* pGroup = dynamic_cast<ContentNodeGroup*>( *it );
			pGroup->FindAll( type, nodes );
		}

		if ( ( 0 == name.compare( (*it)->GetName() ) ) && 
			 ( 0 == type.compare( (*it)->GetType() ) ) )
			nodes.push_back( *it );
	}
	return ( nodes.size() );
}

ContentNode::NodeContainerIter
ContentNodeGroup::RemoveChild( NodeContainerIter nodeIter ) 
{
	delete (*nodeIter);
	NodeContainerIter it = m_vChildren.erase( nodeIter );
	
	return ( it );
}

void
ContentNodeGroup::PostLoadCallback( )
{
	// Invoke for all children.
	for ( NodeContainerIter it = BeginChildren();
		  it != EndChildren();
		  ++it )
	{
		(*it)->PostLoadCallback( );
	}
}

END_CONFIGPARSER_NS
