#ifndef __CONFIGPARSER_CONTENTNODECORE_H__
#define __CONFIGPARSER_CONTENTNODECORE_H__

//
// filename:	contentNodeCore.h
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		23 February 2010
// description:	Content-tree node core classes.
//

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

// --- Includes -----------------------------------------------------------------

// Local headers
#include "contentNode.h"

// STL headers
#include <string>
#include <vector>

// --- Classes ------------------------------------------------------------------

BEGIN_CONFIGPARSER_NS

class ContentNodeGroup;

//PURPOSE
//
class ContentNodeFile : public ContentNode
{
public:
	ContentNodeFile( ContentNode* parent, 
		const configUtil::Environment* pEnv,
		const TCHAR* name,
		const TCHAR* ext, 
		const TCHAR* type = _T("file") );
	ContentNodeFile( ContentNode* parent, 
		const configUtil::Environment* pEnv, 
		xmlElementPtr pElement );
	virtual ~ContentNodeFile( );
 
	/**
	 * @brief Get relative path string for current group.
	 */
	void 			GetRelativePath( TCHAR* path, size_t size ) const;
	
	/**
	 * @brief Get absolute path string for current group.
	 */
	void			GetAbsolutePath( TCHAR* path, size_t size ) const;

	/**
	 * @brief Get raw (no environment variable resolution) relative path.
	 */
	const TCHAR*	GetRawRelativePath( ) const { return ( m_sRelativePath ); }
	
	/**
	 * @brief Get raw (no environment variable resolution) absolute path.
	 */
	void			GetRawAbsolutePath( TCHAR* path, size_t size ) const;

	/**
	 * @brief Get complete filename.
	 */
	void			GetFilename( TCHAR* filename, size_t size ) const;

	/**
	 * @brief Get raw complete filename.
	 */
	void			GetRawFilename( TCHAR* filename, size_t size ) const;

	/**
	 * @brief Return file extension string (excluding '.' character).
	 */
	const TCHAR*	GetExtension( ) const { return ( m_sExtension ); }
	
	/**
	 * @brief Set extension string.
	 */
	void			SetExtension( const TCHAR* ext );
	
	//PURPOSE
	// Return xmlElementPtr representing this content node.
	virtual xmlNodePtr			ToXml() const;

	// Static Methods
	static const char*			GetXmlType( ) { return ( "file" ); }

protected:
	// Hide copy constructor and assignment operator.
	ContentNodeFile( const ContentNodeFile& copy );
	ContentNodeFile& operator=( ContentNodeFile& copy );

	TCHAR		m_sRelativePath[_MAX_PATH];	//!< Relative path of file.
	TCHAR		m_sExtension[_MAX_PATH];		//!< Extension of file.
};

//PURPOSE
//
class ContentNodeDirectory : public ContentNode
{
public:
	ContentNodeDirectory( ContentNode* parent, 
		const configUtil::Environment* pEnv,
		const TCHAR* name );
	ContentNodeDirectory( ContentNode* parent, 
		const configUtil::Environment* pEnv, 
		xmlElementPtr pElement );
	virtual ~ContentNodeDirectory( );

	void GetAbsolutePath( TCHAR* path, size_t size ) const;

	void GetRawAbsolutePath( TCHAR* path, size_t size ) const;

	static const char*			GetXmlType( ) { return ( "directory" ); }

protected:
	// Hide copy constructor and assignment operator.
	ContentNodeDirectory( const ContentNodeDirectory& copy );
	ContentNodeDirectory& operator=( ContentNodeDirectory& copy );
};

//PURPOSE
//
class ContentNodeTarget : public ContentNodeFile
{
public:
	ContentNodeTarget( ContentNode* parent, 
		const configUtil::Environment* pEnv,
		const TCHAR* name, 
		const TCHAR* path, 
		const TCHAR* ext, 
		Platform platform, 
		const TCHAR* type = _T("target") );
	ContentNodeTarget( ContentNode* parent, 
		const configUtil::Environment* pEnv, 
		xmlElementPtr pElement );
	virtual ~ContentNodeTarget( );

	// Static Methods
	static const char*			GetXmlType( ) { return ( "target" ); }

protected:
	// Hide copy constructor and assignment operator.
	ContentNodeTarget( const ContentNodeTarget& copy );
	ContentNodeTarget& operator=( ContentNodeTarget& copy );

	Platform		m_Platform;	//!< Platform
};

//PURPOSE
//
class ContentNodeRpf : public ContentNodeTarget
{
public:
	ContentNodeRpf( ContentNode* parent, 
		const configUtil::Environment* pEnv,
		const TCHAR* name, 
		const TCHAR* path, 
		const TCHAR* ext, 
		Platform platform, 
		bool stack_path = true,
		bool compress = true, 
		const TCHAR* type = _T("rpf") );
	ContentNodeRpf( ContentNode* parent, const configUtil::Environment* pEnv, 
		xmlElementPtr pElement );
	virtual ~ContentNodeRpf( );
	
	//PURPOSE
	// Return xmlElementPtr representing this content node.
	virtual xmlNodePtr	ToXml() const;

	bool				GetStackPath( ) const { return ( m_bStackPath ); }
	void				SetStackPath( bool stack ) { m_bStackPath = stack; }
	bool				GetCompress( ) const { return ( m_bCompress ); }
	void				SetCompress( bool compress ) { m_bCompress = compress; }

	// Static Methods
	static const char*	GetXmlType( ) { return ( "rpf" ); }

protected:
	bool m_bStackPath;	//!< Stack paths into Rpf?
	bool m_bCompress;	//!< Compress RPF?
};

//PURPOSE
//
class ContentNodeZip : public ContentNodeTarget
{
public:
	ContentNodeZip( ContentNode* parent, 
		const configUtil::Environment* pEnv,
		const TCHAR* name, 
		const TCHAR* path, 
		const TCHAR* ext, 
		Platform platform, 
		bool compress = true, 
		const TCHAR* type = _T("zip") );
	ContentNodeZip( ContentNode* parent, const configUtil::Environment* pEnv, 
		xmlElementPtr pElement );
	virtual ~ContentNodeZip( );

	//PURPOSE
	// Return xmlElementPtr representing this content node.
	virtual xmlNodePtr	ToXml() const;

	bool				GetCompress( ) const { return ( m_bCompress ); }
	void				SetCompress( bool compress ) { m_bCompress = compress; }

	// Static Methods
	static const char*	GetXmlType( ) { return ( "zip" ); }

protected:
	bool m_bCompress;	//!< Compress Zip?
};

END_CONFIGPARSER_NS

#endif // __CONFIGPARSER_CONTENTNODEGROUP_H__
