#ifndef __CONFIGPARSER_CONTENTNODE_H__
#define __CONFIGPARSER_CONTENTNODE_H__

//
// filename:	contentNode.h
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		11 February 2010
// description:	Content-tree node base class.
//

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

// --- Includes -----------------------------------------------------------------

// Local headers
#include "configCoreView.h"
#include "contentNode.h"

// configUtil headers
#include "configUtil/Environment.h"

// LibXML2 headers
#include "libxml/parser.h"

// STL headers
#include <vector>

// --- Defines ------------------------------------------------------------------
#define SM_SIZE (64)

// --- Classes ------------------------------------------------------------------

BEGIN_CONFIGPARSER_NS

//PURPOSE
// Platform enumeration.
enum Platform
{
	kIndependent,
	kPS3,
	kXenon,
	kWin32
};

class ContentNodeGroup;

//PURPOSE
// Base content tree node class.  This should not be used directly.
class ContentNode
{
public:
	ContentNode( ContentNode* parent, 
		const configUtil::Environment* pEnv,
		const TCHAR* name, 
		const TCHAR* type );
	ContentNode( ContentNode* parent, 
		const configUtil::Environment* pEnv, 
		xmlElementPtr pElement );
	virtual ~ContentNode( );

	//PURPOSE
	// Return the parent ContentNode, or NULL.
	
	/**
	 * @brief Return parent of the content node (NULL if no parent).
	 */
	const ContentNode*			GetParent( ) const { return ( m_pParent ); }
	
	/**
	 * @brief Return parent of the content node (NULL if no parent).
	 */
	ContentNode*				GetParent( ) { return m_pParent; }
	
	/**
	 * @brief Set parent of the content node (NULL if no parent).
	 */
	void						SetParent( ContentNode* pParent ) { m_pParent = pParent; }

	/**
	 * @brief Return name of the content node.
	 */
	const TCHAR*				GetName( ) const { return ( m_sName ); }

	/**
	 * @brief Return type of the content node.
	 */
	const TCHAR*				GetType( ) const { return ( m_sType ); }

	/**
	 * @brief Return group of content node (if present for input nodes).
	 */
	const TCHAR*				GetGroup( ) const { return ( m_sGroup ); }

	/**
	 * @brief Set group of content node.
	 */
	void						SetGroup( const TCHAR* group );

	//PURPOSE
	// Return boolean outdated status based on nodes inputs outdated status.
	bool						GetOutdated( ) const;

	//PURPOSE
	// When loaded from an XML document this gives the document filename.
	const TCHAR*				GetSourceXmlFilename( ) const { return m_sSourceDoc; }
	
	//PURPOSE
	// When loaded from an XML document this gives the document line number.
	const int					GetSourceXmlLine( ) const { return m_nSourceLine; }

	// Input Container Types
	typedef std::vector<ContentNode*>	NodeContainer;
	typedef NodeContainer::iterator		NodeContainerIter;
	typedef NodeContainer::const_iterator NodeContainerConstIter;

	// Input Container Iterators
	NodeContainerIter		BeginInputs( ) { return ( m_vInputs.begin() ); }
	NodeContainerIter		EndInputs( ) { return ( m_vInputs.end() ); }
	NodeContainerConstIter	BeginInputs( ) const { return ( m_vInputs.begin() ); }
	NodeContainerConstIter	EndInputs( ) const { return ( m_vInputs.end() ); } 

	size_t					GetNumInputs( ) const { return ( m_vInputs.size() ); }
	ContentNode*			GetInput( size_t index ) const { return ( m_vInputs.at( index ) ); }
	void					AddInput( const ContentNode* pNode ) { m_vInputs.push_back( const_cast<ContentNode*>( pNode ) ); }
	NodeContainerIter		RemoveInput( NodeContainerIter it ) { return m_vInputs.erase( it ); }

	// Output Container Iterators
	NodeContainerIter		BeginOutputs( ) { return ( m_vOutputs.begin() ); }
	NodeContainerIter		EndOutputs( ) { return ( m_vOutputs.end() ); }
	NodeContainerConstIter	BeginOutputs( ) const { return ( m_vOutputs.begin() ); }
	NodeContainerConstIter	EndOutputs( ) const { return ( m_vOutputs.end() ); } 

	size_t					GetNumOutputs( ) const { return ( m_vOutputs.size() ); }
	ContentNode*			GetOutput( size_t index ) const { return ( m_vOutputs.at( index ) ); }
	void					AddOutput( const ContentNode* pNode ) { m_vOutputs.push_back( const_cast<ContentNode*>( pNode ) ); }
	NodeContainerIter		RemoveOutput( NodeContainerIter it ) { return m_vOutputs.erase( it ); }
	
	//--------------------------------------------------------------------------
	// Virtual methods to be overridden in subclasses.
	//--------------------------------------------------------------------------
	
	//PURPOSE
	// This method is invoked on all of the content nodes post-load and input
	// resolution.  This allows nodes to update themselves based on any
	// resolved inputs.
	virtual void			PostLoadCallback( ) { }

	//PURPOSE
	// Return xmlNodePtr representing this content node.
	virtual xmlNodePtr		ToXml() const;

	//--------------------------------------------------------------------------
	// Pure-virtual methods to be overridden in subclasses.
	//--------------------------------------------------------------------------
	// None

protected:
	// Hide copy constructor and assignment operator.
	ContentNode( const ContentNode& copy );
	ContentNode& operator=( ContentNode& copy );

	const configUtil::Environment* m_pEnv;	//!< Environment.
	ContentNode*	m_pParent;			//!< Parent content node.
	TCHAR			m_sName[SM_SIZE];	//!< Node name string.
	TCHAR			m_sType[SM_SIZE];	//!< Node type string.
	TCHAR			m_sGroup[SM_SIZE];	//!< Group string if specified for input.
	NodeContainer	m_vInputs;			//!< Input nodes.
	NodeContainer	m_vOutputs;			//!< Output nodes.

	TCHAR			m_sSourceDoc[_MAX_PATH];	//!< Source document XML (for debugging)
	int				m_nSourceLine;	//!< Source document line number
};

END_CONFIGPARSER_NS

#endif // __CONFIGPARSER_CONTENTNODE_H__
