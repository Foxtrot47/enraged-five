#ifndef __CONFIGPARSER_CONFIGMODELDATA_H__
#define __CONFIGPARSER_CONFIGMODELDATA_H__

//
// filename:	configModelData.h
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		21 December 2009
// description:	Config data model.
//

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

// --- Includes -----------------------------------------------------------------

// Local headers
#include "configParser/configParser.h"
#include "configUtil/configUtil.h"

// LibXml2 headers
#include "libxml/parser.h"
#include "libxml/xpath.h"

// STL headers
#include <vector>

// --- Forward Declarations -----------------------------------------------------
// None

// --- Classes ------------------------------------------------------------------

BEGIN_CONFIGPARSER_NS

//PURPOSE
// Data-storage object for all queries into our configuration system.
//
class ConfigModelData
{
public:
	enum etDataType
	{
		CONFIGMODEL_DATA_EMPTY,
		CONFIGMODEL_DATA_BOOL,
		CONFIGMODEL_DATA_STRING,
		CONFIGMODEL_DATA_INT,
		CONFIGMODEL_DATA_FLOAT
	};

	ConfigModelData( );
	ConfigModelData( const ConfigModelData& copy );
	~ConfigModelData( );

	// Operators
	ConfigModelData&	operator=( const ConfigModelData& copy );
	bool				operator==( const ConfigModelData& other );
	bool				operator!=( const ConfigModelData& other );
	ConfigModelData&	operator[]( const size_t index );

	etDataType			GetType( ) const { return ( m_eDataType ); }
	bool				IsEmpty( ) const { return ( CONFIGMODEL_DATA_EMPTY == m_eDataType ); }
	void				ToString( TCHAR* s, size_t size ) const;

	// Value getters.
	void				GetValue( TCHAR* s, size_t size ) const;
	void				GetValue( bool& b ) const { b = m_uData.b; }
	void				GetValue( int& n ) const { n = m_uData.n; }
	void				GetValue( double& f ) const { f = m_uData.f; }

	// Child support.
	typedef std::vector<ConfigModelData> childContainer;
	typedef childContainer::iterator childContainerIter;
	typedef childContainer::reverse_iterator childContainerReverseIter;
	typedef childContainer::const_iterator childContainerConstIter;
	typedef childContainer::const_reverse_iterator childContainerConstReverseIter;

	bool				HasChildren( ) const { return ( m_vChildren.size() > 0 ); }
	size_t				ChildCount( ) const { return ( m_vChildren.size() ); }
	const childContainer& Children( ) const { return m_vChildren; }

	childContainerIter	begin( ) { return ( m_vChildren.begin() ); }
	childContainerIter	end( ) { return ( m_vChildren.end() ); }
	childContainerReverseIter rbegin( ) { return ( m_vChildren.rbegin() ); }
	childContainerReverseIter rend( ) { return ( m_vChildren.rend() ); }
	childContainerConstIter begin( ) const { return ( m_vChildren.begin() ); }
	childContainerConstIter end( ) const { return ( m_vChildren.end() ); }
	childContainerConstReverseIter rbegin( ) const { return ( m_vChildren.rbegin() ); }
	childContainerConstReverseIter rend( ) const { return ( m_vChildren.rend() ); }

private:
	friend class ConfigModel;
	friend class ConfigCoreView;

	ConfigModelData( const TCHAR* s );
	ConfigModelData( bool b );
	ConfigModelData( int n );
	ConfigModelData( double f );


	void Initialize();
	void SetEmpty( );

	void push_child( const ConfigModelData& data );
	void pop_child( );

	etDataType m_eDataType;
	childContainer m_vChildren;
	union
	{
		TCHAR* s;
		bool b;
		int n;
		double f;
	} m_uData;
};

//PURPOSE
// The configModelDataException is a std::runtime_error subclass that 
// indicates an error with a configModel method invocation.
//
class configModelDataException //: public std::runtime_error
{
public:
	configModelDataException( const char* const& /*message*/ )
		//: std::runtime_error( message )
	{
	}
};

END_CONFIGPARSER_NS

#endif // __CONFIGPARSER_CONFIGMODELDATA_H__
