#ifndef __CONFIGPARSER_CONTENTMODEL_H__
#define __CONFIGPARSER_CONTENTMODEL_H__

//
// filename:	contentModel.h
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		15 February 2010
// description:	Content-tree model class.
//

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

// --- Includes -----------------------------------------------------------------
#include "configParser/configParser.h"
#include "configParser/contentNodeGroup.h"

// Other headers
#include "configUtil/Environment.h"
#include "configUtil/path.h"

// STL headers
#include <map>

// --- Classes ------------------------------------------------------------------

BEGIN_CONFIGPARSER_NS

//PURPOSE
//
class ContentModel
{
public:
	ContentModel( configUtil::Environment* pEnv, const TCHAR* filename );
	~ContentModel( );

	bool					SaveXml( ) const;
	const ContentNodeGroup& GetRoot( ) const { return m_RootGroupNode; }
	ContentNodeGroup&		GetRoot( ) { return ( m_RootGroupNode ); }

	configUtil::Environment& GetEnvironment( ) const { return *m_pEnvironment; }

private:
	// Hide copy constructor and assignment operator.
	ContentModel( const ContentModel& copy );
	ContentModel& operator=( ContentModel& copy );

	typedef std::map<std::string, ContentNodeGroup*> GroupFileContainer;
	typedef GroupFileContainer::iterator GroupFileContainerIter;
	typedef GroupFileContainer::const_iterator GroupFileContainerConstIter;

	// ContentNode Factory Methods
	static ContentNode*	ParseContentNode( configUtil::Environment* pEnv, ContentNode* pParent, ContentNodeGroup& rootGroup, xmlNodePtr pNode, GroupFileContainer& fileGroup );
	static void	ParseFile( const TCHAR* filename, const TCHAR* expr, configUtil::Environment* pEnv, ContentNodeGroup& rootGroup, ContentNodeGroup& group, GroupFileContainer& fileGroup, bool map );
	static void	ParseIncludeNodes( xmlNodePtr pNode, const TCHAR* expr, configUtil::Environment* pEnv, ContentNodeGroup& rootGroup, ContentNodeGroup& group, GroupFileContainer& fileGroup );
	static void ParseContentNodes( xmlNodePtr pNode, const TCHAR* expr, configUtil::Environment* pEnv, ContentNodeGroup& rootGroup, ContentNodeGroup& group, GroupFileContainer& fileGroup );
	static void ResolveInputNodes( ContentNodeGroup& root, ContentNode& node, xmlElementPtr pElement );
	
	configUtil::Environment* m_pEnvironment;	//!< Inherited environment.
	TCHAR					m_sFilename[_MAX_PATH];	//!< Root content filename.
	ContentNodeGroup		m_RootGroupNode;	//!< Root content node.
	Platform				m_CurrentPlatform;	//!< Current platform tag.

	GroupFileContainer		m_mFileGroups;		//!< Filename to ContentNodeGroup mapping.
};

END_CONFIGPARSER_NS

#endif // __CONFIGPARSER_CONTENTMODEL_H__
