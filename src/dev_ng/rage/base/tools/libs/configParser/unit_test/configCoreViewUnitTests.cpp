//
// filename:	configCoreViewUnitTests.cpp
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		22 December 2009
// description:	
//

// --- Includes -----------------------------------------------------------------

// Local headers
#include "configCoreViewUnitTests.h"

// CppUnit headers
#include "cppunit/TestAssert.h"

// --- Implementation -----------------------------------------------------------

CPPUNIT_TEST_SUITE_REGISTRATION( ConfigCoreViewUnitTests );

void
ConfigCoreViewUnitTests::generic_query_test( )
{
	// Test Single Result
	TCHAR sdata[MAX_PATH];
	configParser::ConfigModelData builddir;
	CPPUNIT_ASSERT( m_configView.query( _T("//project/branches/branch[@name='dev']/@build"), builddir ) );
	builddir.ToString( sdata, MAX_PATH );
	CPPUNIT_ASSERT_MESSAGE( "Invalid build directory for 'dev' branch.", 0 == wcscmp( _T("$(root)/build/$(branch)"), sdata ) );

	// Testing Child Results
	configParser::ConfigModelData branches;
	CPPUNIT_ASSERT( m_configView.query( _T("//project/branches/branch/@name"), branches ) );
	CPPUNIT_ASSERT_MESSAGE( "Unexpected number of branches.", 4 == branches.ChildCount() ); 
	CPPUNIT_ASSERT_EQUAL( configParser::ConfigModelData::CONFIGMODEL_DATA_EMPTY, branches.GetType() );
	CPPUNIT_ASSERT_EQUAL( configParser::ConfigModelData::CONFIGMODEL_DATA_STRING, branches[0].GetType() );
	CPPUNIT_ASSERT_EQUAL( configParser::ConfigModelData::CONFIGMODEL_DATA_STRING, branches[1].GetType() );
	CPPUNIT_ASSERT_EQUAL( configParser::ConfigModelData::CONFIGMODEL_DATA_STRING, branches[2].GetType() );


	// Test No Exceptions Raised for good or bad XPath expressions.
	configParser::ConfigModelData temp;
	CPPUNIT_ASSERT_NO_THROW( m_configView.query( _T("//project/@name"), temp ) );
	CPPUNIT_ASSERT( m_configView.query( _T("//project/@name"), temp ) );
	CPPUNIT_ASSERT_NO_THROW( m_configView.query( _T("//project/@blah"), temp ) );
	CPPUNIT_ASSERT( !m_configView.query( _T("//project/@blah"), temp ) );
	CPPUNIT_ASSERT_NO_THROW( m_configView.query( _T("//fdsghjkgjhkghj/@fdhjk"), temp ) );
	CPPUNIT_ASSERT( !m_configView.query( _T("//fdsghjkgjhkghj/@fdhjk"), temp ) );

	// Ensure we can read global network, config and local data.
	int network_ver = 0;
	int config_ver = 0;
	int local_ver = 0;

	CPPUNIT_ASSERT( m_configView.query( _T("//network/@version"), network_ver ) );
	CPPUNIT_ASSERT( m_configView.query( _T("//config/@version"), config_ver ) );
	CPPUNIT_ASSERT( m_configView.query( _T("//local/@version"), local_ver ) );
	CPPUNIT_ASSERT_MESSAGE( "Network version should not be zero.", 0 != network_ver );
	CPPUNIT_ASSERT_MESSAGE( "Config version should not be zero.", 0 != config_ver );
	CPPUNIT_ASSERT_MESSAGE( "Local version should not be zero.", 0 != local_ver );
	CPPUNIT_ASSERT_EQUAL( network_ver, config_ver );
	CPPUNIT_ASSERT_EQUAL( config_ver, local_ver );
}

void
ConfigCoreViewUnitTests::bool_query_test( )
{
	configParser::ConfigModelData temp;
	bool value = false;
	
	// Test 1: Basic XPath 'true()' function.
	CPPUNIT_ASSERT( m_configView.query( _T("true()"), temp ) );
	temp.GetValue( value );
	CPPUNIT_ASSERT_EQUAL( configParser::ConfigModelData::CONFIGMODEL_DATA_BOOL, temp.GetType() );
	CPPUNIT_ASSERT( value );

	// Test 2: Basic XPath 'false()' function.
	value = true;
	CPPUNIT_ASSERT( m_configView.query( _T("false()"), temp ) );
	temp.GetValue( value );
	CPPUNIT_ASSERT_EQUAL( configParser::ConfigModelData::CONFIGMODEL_DATA_BOOL, temp.GetType() );
	CPPUNIT_ASSERT( !value );

	// Test 3: Basic XPath equality function.
	value = false;
	TCHAR test1[MAX_PATH];
	CPPUNIT_ASSERT( m_configView.query( _T("//config/@version"), temp ) );
	CPPUNIT_ASSERT_EQUAL( configParser::ConfigModelData::CONFIGMODEL_DATA_STRING, temp.GetType() );
	temp.GetValue( test1, MAX_PATH );
	CPPUNIT_ASSERT( 0 == wcscmp( _T("18"), test1 ) );
	CPPUNIT_ASSERT( m_configView.query( _T("number(//config/@version)=number('18')"), temp ) );
	temp.GetValue( value );
	CPPUNIT_ASSERT_EQUAL( configParser::ConfigModelData::CONFIGMODEL_DATA_BOOL, temp.GetType() );
	CPPUNIT_ASSERT( value );
}

void
ConfigCoreViewUnitTests::int_query_test( )
{
	configParser::ConfigModelData temp;
	double f = -1.0;
	int n = -1;

	// Test 1: Basic XPath Number Test (integer)
	CPPUNIT_ASSERT( m_configView.query( _T("87"), temp ) );
	temp.GetValue( n );
	CPPUNIT_ASSERT_EQUAL( configParser::ConfigModelData::CONFIGMODEL_DATA_INT, temp.GetType() );
	CPPUNIT_ASSERT_EQUAL( 87, n );

	// Test 2: Basic XPath Number Test (floating-point)
	CPPUNIT_ASSERT( m_configView.query( _T("98.66"), temp ) );
	temp.GetValue( f );
	CPPUNIT_ASSERT_EQUAL( configParser::ConfigModelData::CONFIGMODEL_DATA_FLOAT, temp.GetType() );
	CPPUNIT_ASSERT_EQUAL( 98.66, f );

	// Test 3: XPath count function
	CPPUNIT_ASSERT( m_configView.query( _T("count(//project/branches/branch)"), temp ) );
	CPPUNIT_ASSERT_EQUAL( configParser::ConfigModelData::CONFIGMODEL_DATA_INT, temp.GetType() );
	temp.GetValue( n );
	CPPUNIT_ASSERT_EQUAL( 4, n );

	// Test 4: XPath number() casting
	CPPUNIT_ASSERT( m_configView.query( _T("number(//config/@version)"), temp ) );
	CPPUNIT_ASSERT_EQUAL( configParser::ConfigModelData::CONFIGMODEL_DATA_INT, temp.GetType() );
	temp.GetValue( n );
	CPPUNIT_ASSERT_EQUAL( 18, n );
}

void
ConfigCoreViewUnitTests::float_query_test( )
{
	configParser::ConfigModelData temp;
	// DHM TODO
}

void 
ConfigCoreViewUnitTests::generic_set_test( )
{
	int orig_version = -1;
	int version = -1;
	CPPUNIT_ASSERT( m_configView.query( _T("//local/@version"), orig_version ) );
	CPPUNIT_ASSERT( m_configView.set( _T("//local/@version"), 90, true ) );
	CPPUNIT_ASSERT( m_configView.query( _T("//local/@version"), version ) );
	CPPUNIT_ASSERT( m_configView.set( _T("//local/@version"), orig_version, true ) );
	CPPUNIT_ASSERT_EQUAL_MESSAGE( "Set version failed.", 90, version );


	TCHAR* xpath_expr = _T("//local/branches/branch[@name='dev']/targets/target[@platform='xbox360']/@enabled");
	bool orig_enabled = false;
	bool enabled = false;
	CPPUNIT_ASSERT( m_configView.query( xpath_expr, orig_enabled ) );
	CPPUNIT_ASSERT( m_configView.set( xpath_expr, !orig_enabled, true ) );
	CPPUNIT_ASSERT( m_configView.query( xpath_expr, enabled ) );
	CPPUNIT_ASSERT( m_configView.set( xpath_expr, orig_enabled, true ) );
	CPPUNIT_ASSERT_EQUAL_MESSAGE( "Enabled flag inconsistency after set.", orig_enabled, !enabled );
}
