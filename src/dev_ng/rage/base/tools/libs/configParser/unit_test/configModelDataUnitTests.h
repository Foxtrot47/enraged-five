#ifndef __CONFIGPARSERUNITESTS_CONFIGMODELDATAUNITTESTS_H__
#define __CONFIGPARSERUNITESTS_CONFIGMODELDATAUNITTESTS_H__

//
// filename:	configModelDataUnitTests.h
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		22 December 2009
// description:	
//

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

// --- Includes -----------------------------------------------------------------

// Local headers
#include "configParser/configCoreView.h"
#include "configParser/configModelData.h"

// CppUnit headers
#include "cppunit/TestFixture.h"
#include "cppunit/extensions/HelperMacros.h"

// --- Classes ------------------------------------------------------------------

//PURPOSE
// Unit test cases for the configParser::configModelData class.
//
class ConfigModelDataUnitTests : public CppUnit::TestFixture
{
	CPPUNIT_TEST_SUITE( ConfigModelDataUnitTests );
	CPPUNIT_TEST( basic_test );
	CPPUNIT_TEST( equality_test );
	CPPUNIT_TEST( copy_and_assignment_test );
	CPPUNIT_TEST( children_test );
	CPPUNIT_TEST_SUITE_END( );
public:

	//PURPOSE
	// Invoked before each test method.
	void setUp( );

	//PURPOSE
	// Invoked after each test method.
	void tearDown( );

	//PURPOSE
	// Unit tests for basic public features.
	void basic_test( );

	//PURPOSE
	// Unit tests for equality and in-equality operators.
	void equality_test( );

	//PURPOSE
	// Unit tests for copy constructor and assignment operator.
	void copy_and_assignment_test( );

	//PURPOSE
	// Unit tests for children, iterators etc.
	void children_test( );

private:	
	configParser::ConfigCoreView m_configView;
	configParser::ConfigModelData m_modelData;
	configParser::ConfigModelData m_modelData2;
};

#endif // __CONFIGPARSERUNITESTS_CONFIGMODELDATAUNITTESTS_H__
