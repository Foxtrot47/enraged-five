//
// filename:	main.cpp
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		18 December 2009
// description:	configParser simple unit test console app.
//

// --- Includes -----------------------------------------------------------------

// Local headers
#include "configCoreViewUnitTests.h"

// configParser headers
#include "configParser/configParser.h"
#include "configParser/configCoreView.h"

// CppUnit headers
#include "cppunit/extensions/HelperMacros.h"
#include "cppunit/extensions/TestFactoryRegistry.h"
#include "cppunit/ui/text/TestRunner.h"

// STL headers
#include <cstdlib>
#include <iostream>

// --- Implementation -----------------------------------------------------------

int 
main( int argc, char* argv )
{
	CppUnit::TextUi::TestRunner runner;
	CppUnit::TestFactoryRegistry& registry = CppUnit::TestFactoryRegistry::getRegistry( );
	runner.addTest( registry.makeTest( ) );
	bool success = runner.run( "", false );	

	return ( success ? EXIT_SUCCESS : EXIT_FAILURE );
}
