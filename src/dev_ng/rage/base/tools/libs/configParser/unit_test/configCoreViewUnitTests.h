#ifndef __CONFIGPARSERUNITESTS_CONFIGCOREVIEWUNITTESTS_H__
#define __CONFIGPARSERUNITESTS_CONFIGCOREVIEWUNITTESTS_H__

//
// filename:	configCoreViewUnitTests.h
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		22 December 2009
// description:	
//

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

// --- Includes -----------------------------------------------------------------

// Local headers
#include "configParser/configParser.h"
#include "configParser/configCoreView.h"
#include "configParser/configModel.h"

// CppUnit headers
#include "cppunit/TestFixture.h"
#include "cppunit/extensions/HelperMacros.h"

// --- Classes ------------------------------------------------------------------

//PURPOSE
// Unit test cases for the configParser::configCoreView class.
//
class ConfigCoreViewUnitTests : public CppUnit::TestFixture
{
	CPPUNIT_TEST_SUITE( ConfigCoreViewUnitTests );
	CPPUNIT_TEST( generic_query_test );
	CPPUNIT_TEST( bool_query_test );
	CPPUNIT_TEST( int_query_test );
	CPPUNIT_TEST( float_query_test );
	CPPUNIT_TEST( generic_set_test );
	CPPUNIT_TEST_SUITE_END( );
public:

	//PURPOSE
	// Unit tests for generic query method.
	void generic_query_test( );

	//PURPOSE
	// Unit tests for boolean data types.
	void bool_query_test( );

	//PURPOSE
	// Unit tests for integer data types.
	void int_query_test( );

	//PURPOSE
	// Unit tests for floating-point data types.
	void float_query_test( );

	//PURPOSE
	// Unit tests for generic set method.
	void generic_set_test( );

private:
	configParser::ConfigCoreView m_configView;
};

#endif // __CONFIGPARSERUNITESTS_CONFIGCOREVIEWUNITTESTS_H__
