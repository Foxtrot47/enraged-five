//
// filename:	configModelDataUnitTests.cpp
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		22 December 2009
// description:	
//

// --- Includes -----------------------------------------------------------------

// Local headers
#include "configModelDataUnitTests.h"

// CppUnit headers
#include "cppunit/TestAssert.h"

// --- Implementation -----------------------------------------------------------

CPPUNIT_TEST_SUITE_REGISTRATION( ConfigModelDataUnitTests );

void 
ConfigModelDataUnitTests::setUp( ) 
{
}

void 
ConfigModelDataUnitTests::tearDown( ) 
{
}

void
ConfigModelDataUnitTests::basic_test( )
{
	CPPUNIT_ASSERT_MESSAGE( "Default constructor does not create empty model data.", 
		m_modelData.IsEmpty() );
	CPPUNIT_ASSERT_MESSAGE( "Default constructor does not create empty-child model data.", 
		!m_modelData.HasChildren() );
	CPPUNIT_ASSERT_EQUAL_MESSAGE( "Default constructor sets unexpected data type.", 
		configParser::ConfigModelData::CONFIGMODEL_DATA_EMPTY, m_modelData.GetType() );

	TCHAR sdata[MAX_PATH];
	m_modelData.ToString( sdata, MAX_PATH );

	CPPUNIT_ASSERT_MESSAGE( "Default constructor sets unexpected data type.", 
		0 == wcscmp( _T( "EMPTY" ), sdata ) );
}

void
ConfigModelDataUnitTests::equality_test( )
{
	CPPUNIT_ASSERT_MESSAGE( "Two default constructed model data objects are not-equal!",
		m_modelData == m_modelData2 );
	CPPUNIT_ASSERT_MESSAGE( "Two default constructed model data objects are not-equal!",
		!(m_modelData != m_modelData2) );
#if 0
	configParser::ConfigModelData data1;
	m_configView.query( "//project/@version", data1 );
	configParser::ConfigModelData data2;
	m_configView.query( "//project/@version", data2 );
	CPPUNIT_ASSERT( data1 == data2 );
	CPPUNIT_ASSERT( !(data1 != data2) );
#endif
}

void
ConfigModelDataUnitTests::copy_and_assignment_test( )
{

}

void
ConfigModelDataUnitTests::children_test( )
{
	
}

