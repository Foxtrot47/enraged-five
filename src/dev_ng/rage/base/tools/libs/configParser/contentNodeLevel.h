#ifndef __CONFIGPARSER_CONTENTNODELEVEL_H__
#define __CONFIGPARSER_CONTENTNODELEVEL_H__

//
// filename:	contentNodeLevel.h
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		14 April 2010
// description:	Content-tree node level class.
//

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

// --- Includes -----------------------------------------------------------------

// Local headers
#include "contentNodeGroup.h"

// STL headers
#include <string>
#include <vector>

// --- Classes ------------------------------------------------------------------

BEGIN_CONFIGPARSER_NS

//PURPOSE
// A ContentNodeLevel is a high-level group container that includes some 
// additional data over a regular group.
//
class ContentNodeLevel : public ContentNodeGroup
{
public:
	ContentNodeLevel( ContentNode* parent, 
		const configUtil::Environment* pEnv,
		const TCHAR* name, 
		const TCHAR* path,
		const TCHAR* image );
	ContentNodeLevel( ContentNode* parent, 
		const configUtil::Environment* pEnv, 
		xmlElementPtr pElement );
	virtual ~ContentNodeLevel( );
	
	/**
	 * @brief Return raw path filename for a level plan image.
	 */
	const TCHAR*	GetRawImageFilename( ) const { return ( m_sImageFilename ); } 

	/**
	 * @brief Return absolute path filename for a level plan image.
	 */
	void			GetImageFilename( TCHAR* path, size_t size ) const;
	
	// Static Methods
	static const char*	GetXmlType( ) { return ( "level" ); }

protected:
	TCHAR* m_sImageFilename;
};

END_CONFIGPARSER_NS

#endif // __CONFIGPARSER_CONTENTNODELEVEL_H__
