//
// filename:	contentNodeVehicles.cpp
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		15 June 2010
// description:	Content-tree Vehicles RPF node class.
//

// LibXml2 headers
#include "libxml/parser.h"
#include "libxml/xpath.h"

// Local headers
#include "configParser/contentNodeVehicles.h"

BEGIN_CONFIGPARSER_NS

ContentNodeVehiclesRpf::ContentNodeVehiclesRpf( ContentNode* parent, 
											   const configUtil::Environment* pEnv,
											   const TCHAR* name, 
											   const TCHAR* path, 
											   Platform platform, 
											   const TCHAR* sharedtxd,
											   const TCHAR* sharedtxdinput,
											   const TCHAR* friendly )
	: ContentNodeRpf( parent, pEnv, name, path, _T("rpf"), platform, false, false )
{
#ifdef UNICODE
	wcsncpy_s( m_sSharedTXD, _MAX_PATH, sharedtxd, _TRUNCATE );
	wcsncpy_s( m_sSharedTXDInput, _MAX_PATH, sharedtxdinput, _TRUNCATE );
	wcsncpy_s( m_sFriendlyName, SM_SIZE, friendly, _TRUNCATE );
#else
	strncpy_s( m_sSharedTXD, _MAX_PATH, sharedtxd, _TRUNCATE );
	strncpy_s( m_sSharedTXDInput, _MAX_PATH, sharedtxdinput, _TRUNCATE );
	strncpy_s( m_sFriendlyName, SM_SIZE, friendly, _TRUNCATE );
#endif // UNICODE
}

ContentNodeVehiclesRpf::ContentNodeVehiclesRpf( ContentNode* parent,
									   const configUtil::Environment* pEnv, 
									   xmlElementPtr pElement )
	: ContentNodeRpf( parent, pEnv, pElement )
{
#ifdef UNICODE
	wcsncpy_s( m_sExtension, _MAX_PATH, _T("rpf"), _TRUNCATE );
#else
	strncpy_s( m_sExtension, _MAX_PATH, _T("rpf"), _TRUNCATE );
#endif // UNICODE


	xmlAttributePtr pAttribute = pElement->attributes;
	while ( pAttribute )
	{ 
		if ( 0 == xmlStrcasecmp( BAD_CAST "friendlyname", pAttribute->name ) )
		{
			configUtil::AnsiToTChar( m_sFriendlyName, SM_SIZE, 
				(char*)pAttribute->children->content );
		}
		else if ( 0 == xmlStrcasecmp( BAD_CAST "sharedtxd", pAttribute->name ) )
		{			
			configUtil::AnsiToTChar( m_sSharedTXD, _MAX_PATH, 
				(char*)pAttribute->children->content );
		}
		else if ( 0 == xmlStrcasecmp( BAD_CAST "sharedtxdinput", pAttribute->name ) )
		{
			configUtil::AnsiToTChar( m_sSharedTXDInput, _MAX_PATH, 
				(char*)pAttribute->children->content );
		}
		// Next attribute.
		pAttribute = (xmlAttributePtr)( pAttribute->next );
	}
}

ContentNodeVehiclesRpf::~ContentNodeVehiclesRpf( )
{
}

void 
ContentNodeVehiclesRpf::GetSharedTxd( TCHAR* path, size_t size ) const
{
#ifdef UNICODE
	wcsncpy_s( path, _MAX_PATH, m_sSharedTXD, _TRUNCATE );
#else
	strncpy_s( path, _MAX_PATH, m_sSharedTXD, _TRUNCATE );
#endif // UNICODE
	m_pEnv->subst( path, size );
}

void
ContentNodeVehiclesRpf::GetSharedTxdInput( TCHAR* path, size_t size ) const
{
#ifdef UNICODE
	wcsncpy_s( path, _MAX_PATH, m_sSharedTXDInput, _TRUNCATE );
#else
	strncpy_s( path, _MAX_PATH, m_sSharedTXDInput, _TRUNCATE );
#endif // UNICODE
	m_pEnv->subst( path, size );
}

void 
ContentNodeVehiclesRpf::GetFriendlyName( TCHAR* name, size_t size ) const
{
#ifdef UNICODE
	wcsncpy_s( name, _MAX_PATH, m_sFriendlyName, _TRUNCATE );
#else
	strncpy_s( name, _MAX_PATH, m_sFriendlyName, _TRUNCATE );
#endif // UNICODE
	m_pEnv->subst( name, size );
}

#if 0
const STL_STRING 
ContentNodeVehiclesRpf::GetSharedTxdInputResolved( ) const
{
	STL_STRING resolvedSharedTxdInput( m_sSharedTXDInput );
	m_pEnv->subst( resolvedSharedTxdInput );

	return ( resolvedSharedTxdInput );
}
#endif


ContentNodeVehiclesZip::ContentNodeVehiclesZip( ContentNode* parent, 
	const configUtil::Environment* pEnv,
	const TCHAR* name, 
	const TCHAR* path, 
	Platform platform, 
	const TCHAR* sharedtxd,
	const TCHAR* sharedtxdinput,
	const TCHAR* friendly )
	: ContentNodeZip( parent, pEnv, name, path, _T("zip"), platform, false, false )
{
#ifdef UNICODE
	wcsncpy_s( m_sSharedTXD, _MAX_PATH, sharedtxd, _TRUNCATE );
	wcsncpy_s( m_sSharedTXDInput, _MAX_PATH, sharedtxdinput, _TRUNCATE );
	wcsncpy_s( m_sFriendlyName, SM_SIZE, friendly, _TRUNCATE );
#else
	strncpy_s( m_sSharedTXD, _MAX_PATH, sharedtxd, _TRUNCATE );
	strncpy_s( m_sSharedTXDInput, _MAX_PATH, sharedtxdinput, _TRUNCATE );
	strncpy_s( m_sFriendlyName, SM_SIZE, friendly, _TRUNCATE );
#endif // UNICODE
}

ContentNodeVehiclesZip::ContentNodeVehiclesZip( ContentNode* parent,
	const configUtil::Environment* pEnv, 
	xmlElementPtr pElement )
	: ContentNodeZip( parent, pEnv, pElement )
{
#ifdef UNICODE
	wcsncpy_s( m_sExtension, _MAX_PATH, _T("zip"), _TRUNCATE );
#else
	strncpy_s( m_sExtension, _MAX_PATH, _T("zip"), _TRUNCATE );
#endif // UNICODE


	xmlAttributePtr pAttribute = pElement->attributes;
	while ( pAttribute )
	{ 
		if ( 0 == xmlStrcasecmp( BAD_CAST "friendlyname", pAttribute->name ) )
		{
			configUtil::AnsiToTChar( m_sFriendlyName, SM_SIZE, 
				(char*)pAttribute->children->content );
		}
		else if ( 0 == xmlStrcasecmp( BAD_CAST "sharedtxd", pAttribute->name ) )
		{			
			configUtil::AnsiToTChar( m_sSharedTXD, _MAX_PATH, 
				(char*)pAttribute->children->content );
		}
		else if ( 0 == xmlStrcasecmp( BAD_CAST "sharedtxdinput", pAttribute->name ) )
		{
			configUtil::AnsiToTChar( m_sSharedTXDInput, _MAX_PATH, 
				(char*)pAttribute->children->content );
		}
		// Next attribute.
		pAttribute = (xmlAttributePtr)( pAttribute->next );
	}
}

ContentNodeVehiclesZip::~ContentNodeVehiclesZip( )
{
}

void 
ContentNodeVehiclesZip::GetSharedTxd( TCHAR* path, size_t size ) const
{
#ifdef UNICODE
	wcsncpy_s( path, _MAX_PATH, m_sSharedTXD, _TRUNCATE );
#else
	strncpy_s( path, _MAX_PATH, m_sSharedTXD, _TRUNCATE );
#endif // UNICODE
	m_pEnv->subst( path, size );
}

void
ContentNodeVehiclesZip::GetSharedTxdInput( TCHAR* path, size_t size ) const
{
#ifdef UNICODE
	wcsncpy_s( path, _MAX_PATH, m_sSharedTXDInput, _TRUNCATE );
#else
	strncpy_s( path, _MAX_PATH, m_sSharedTXDInput, _TRUNCATE );
#endif // UNICODE
	m_pEnv->subst( path, size );
}

void 
ContentNodeVehiclesZip::GetFriendlyName( TCHAR* name, size_t size ) const
{
#ifdef UNICODE
	wcsncpy_s( name, _MAX_PATH, m_sFriendlyName, _TRUNCATE );
#else
	strncpy_s( name, _MAX_PATH, m_sFriendlyName, _TRUNCATE );
#endif // UNICODE
	m_pEnv->subst( name, size );
}

#if 0
const STL_STRING 
ContentNodeVehiclesZip::GetSharedTxdInputResolved( ) const
{
	STL_STRING resolvedSharedTxdInput( m_sSharedTXDInput );
	m_pEnv->subst( resolvedSharedTxdInput );

	return ( resolvedSharedTxdInput );
}
#endif

// --- ContentNodeVehicleDirectory -------------------------------------------------


ContentNodeVehiclesDirectory::ContentNodeVehiclesDirectory( ContentNode* parent, 
   const configUtil::Environment* pEnv,
   const TCHAR* name,
   const TCHAR* sharedtxd,
   const TCHAR* sharedtxdinput,
   const TCHAR* friendly )
   : ContentNodeDirectory( parent, pEnv, name )
{
	_tcsncpy_s( m_sSharedTXD, _MAX_PATH, sharedtxd, _TRUNCATE );
	_tcsncpy_s( m_sSharedTXDInput, _MAX_PATH, sharedtxdinput, _TRUNCATE );
	_tcsncpy_s( m_sFriendlyName, SM_SIZE, friendly, _TRUNCATE );
}

ContentNodeVehiclesDirectory::ContentNodeVehiclesDirectory( ContentNode* parent,
   const configUtil::Environment* pEnv, 
   xmlElementPtr pElement )
   : ContentNodeDirectory( parent, pEnv, pElement )
{
	xmlAttributePtr pAttribute = pElement->attributes;
	while ( pAttribute )
	{ 
		if ( 0 == xmlStrcasecmp( BAD_CAST "friendlyname", pAttribute->name ) )
		{
			configUtil::AnsiToTChar( m_sFriendlyName, SM_SIZE, 
				(char*)pAttribute->children->content );
		}
		else if ( 0 == xmlStrcasecmp( BAD_CAST "sharedtxd", pAttribute->name ) )
		{			
			configUtil::AnsiToTChar( m_sSharedTXD, _MAX_PATH, 
				(char*)pAttribute->children->content );
		}
		else if ( 0 == xmlStrcasecmp( BAD_CAST "sharedtxdinput", pAttribute->name ) )
		{
			configUtil::AnsiToTChar( m_sSharedTXDInput, _MAX_PATH, 
				(char*)pAttribute->children->content );
		}
		// Next attribute.
		pAttribute = (xmlAttributePtr)( pAttribute->next );
	}
}

ContentNodeVehiclesDirectory::~ContentNodeVehiclesDirectory( )
{
}

void 
ContentNodeVehiclesDirectory::GetSharedTxd( TCHAR* path, size_t size ) const
{
	_tcsncpy_s( path, _MAX_PATH, m_sSharedTXD, _TRUNCATE );
	m_pEnv->subst( path, size );
}

void
ContentNodeVehiclesDirectory::GetSharedTxdInput( TCHAR* path, size_t size ) const
{
	_tcsncpy_s( path, _MAX_PATH, m_sSharedTXDInput, _TRUNCATE );
	m_pEnv->subst( path, size );
}

void 
ContentNodeVehiclesDirectory::GetFriendlyName( TCHAR* path, size_t size ) const
{
	_tcsncpy_s( path, _MAX_PATH, m_sFriendlyName, _TRUNCATE );
	m_pEnv->subst( path, size );
}

// --- ContentNodeVehicleModZip ----------------------------------------------------

ContentNodeVehicleModZip::ContentNodeVehicleModZip( ContentNode* parent,
									 const configUtil::Environment* pEnv,
									 const TCHAR* name, 
									 Platform platform )
									 : ContentNodeZip( parent, pEnv, name, _T(""), _T("zip"), platform, true )
{
}


ContentNodeVehicleModZip::ContentNodeVehicleModZip( ContentNode* parent,
									 const configUtil::Environment* pEnv, 
									 xmlElementPtr pElement )
									 : ContentNodeZip( parent, pEnv, pElement )
{
#ifdef UNICODE
	wcsncpy_s( m_sExtension, _MAX_PATH, _T("zip"), _TRUNCATE );
#else
	strncpy_s( m_sExtension, _MAX_PATH, _T("zip"), _TRUNCATE );
#endif // UNICODE
}

/*
ContentNodeVehicleModZip::~ContentNodeVehicleModZip( )
{
	for ( NodeContainerIter it = BeginChildren();
		it != EndChildren();
		++it )
	{
		ContentNode* pNode = (*it);
		delete pNode;
	}
	m_vChildren.clear( );
}

void 
ContentNodeVehicleModZip::PostLoadCallback( )
{
	if ( 1 != GetNumInputs() )
		return;

	ContentNodeMap* pInput = dynamic_cast<ContentNodeMap*>( GetInput( 0 ) );
	if ( !pInput )
		return;

	// SceneXml always there for maps.
	AddChild( new ContentNodeTarget( NULL, m_pEnv, m_sName, m_sRelativePath, _T("xml"), m_Platform ) );
	if ( pInput->GetDefinitions( ) )
		AddChild( new ContentNodeTarget( NULL, m_pEnv, m_sName, m_sRelativePath, _T("ide"), m_Platform ) );
	if ( pInput->GetInstances( ) )
		AddChild( new ContentNodeTarget( NULL, m_pEnv, m_sName, m_sRelativePath, _T("ipl"), m_Platform ) );
	if ( pInput->GetData( ) )
		AddChild( new ContentNodeTarget( NULL, m_pEnv, m_sName, m_sRelativePath, _T("zip"), m_Platform ) );
}
*/
END_CONFIGPARSER_NS
