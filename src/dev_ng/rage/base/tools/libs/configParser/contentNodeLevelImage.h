#ifndef __CONFIGPARSER_CONTENTNODELEVELIMAGE_H__
#define __CONFIGPARSER_CONTENTNODELEVELIMAGE_H__

//
// filename:	contentNodeLevelImage.h
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		20 April 2010
// description:	Content-tree node level image class.
//

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

// --- Includes -----------------------------------------------------------------

// Local headers
#include "contentNodeCore.h"

// --- Classes ------------------------------------------------------------------

BEGIN_CONFIGPARSER_NS

//PURPOSE
//
class ContentNodeLevelImage : public ContentNode
{
public:
	ContentNodeLevelImage( ContentNode* parent, 
		const configUtil::Environment* pEnv,
		const TCHAR* filename, 
		float minx, float miny, float maxx, float maxy );
	ContentNodeLevelImage( ContentNode* parent, 
		const configUtil::Environment* pEnv, 
		xmlElementPtr pElement );
	virtual ~ContentNodeLevelImage( );

	/**
	 * @brief Return raw image filename.
	 */
	const TCHAR*	GetRawImageFilename( ) const { return ( m_sImageFilename ); }

	/**
	 * @brief Return absolute image filename.
	 */
	void			GetImageFilename( TCHAR* path, size_t size ) const;

	float			GetImageMinX( ) const { return m_fImageMinX; }
	float			GetImageMinY( ) const { return m_fImageMinY; }
	float			GetImageMaxX( ) const { return m_fImageMaxX; }
	float			GetImageMaxY( ) const { return m_fImageMaxY; }

	// Static Methods
	static const char* GetXmlType( ) { return ( "level_image" ); }
	
protected:
	TCHAR	m_sImageFilename[_MAX_PATH]; //!< Raw image filename
	float	m_fImageMinX;
	float	m_fImageMinY;
	float	m_fImageMaxX;
	float	m_fImageMaxY;
};

END_CONFIGPARSER_NS

#endif // __CONFIGPARSER_CONTENTNODELEVEL_H__
