//
// filename:	configModel.cpp
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		18 December 2009
// description:	
//
#pragma warning(disable: 4996)

// Local headers
#include "configParser/configParser.h"
#include "configParser/configModel.h"
#include "configUtil/configUtil.h"
#include "configUtil/scoped_mutex.h"

// LibXml2 headers
#include "libxml/parser.h"
#include "libxml/xpath.h"
#include "libxml/xmlsave.h"

#include "libxslt/xslt.h"
#include "libxslt/xsltinternals.h"
#include "libxslt/transform.h"

// STL headers
#include <cassert>
#include <string.h>

// Defines
#define TEMP_BUFFER_SIZE (1024)

BEGIN_CONFIGPARSER_NS

ConfigModel::ConfigModel( )
	: m_pXmlDoc( NULL )
	, m_pXPathCtx( NULL )
{
	InitializeCriticalSection( &m_mutex );
	xmlInitParser( );
	LIBXML_TEST_VERSION;
}

ConfigModel::ConfigModel( const TCHAR* filename )
	: m_pXmlDoc( NULL )
	, m_pXPathCtx( NULL )
{
	InitializeCriticalSection( &m_mutex );
	xmlInitParser( );
	xmlXPathInit( );

	xmlKeepBlanksDefault(0);

	LIBXML_TEST_VERSION;
	load( filename );
}

ConfigModel::~ConfigModel( )
{
	memset( m_sFilename, 0, sizeof( TCHAR ) * _MAX_PATH );
	if ( m_pXmlDoc )
	{
		xmlFreeDoc( m_pXmlDoc );
		m_pXmlDoc = NULL;
	}
	if ( m_pXPathCtx )
	{
		xmlXPathFreeContext( m_pXPathCtx );
		m_pXPathCtx = NULL;
	}
	xmlCleanupParser( );

	DeleteCriticalSection( &m_mutex );
}

bool
ConfigModel::load( const TCHAR* filename )
{
	if ( m_pXmlDoc )
	{
		xmlFreeDoc( m_pXmlDoc );
		m_pXmlDoc = NULL;
	}
	if ( m_pXPathCtx )
	{
		xmlXPathFreeContext( m_pXPathCtx );
		m_pXPathCtx = NULL;
	}

	_tcsncpy_s( m_sFilename, _MAX_PATH, filename, _TRUNCATE );
	char ansiFilename[_MAX_PATH];
	configUtil::TCharToAnsi( ansiFilename, _MAX_PATH, m_sFilename ); 
	m_pXmlDoc = xmlParseFile( ansiFilename );
	m_pXPathCtx = xmlXPathNewContext( m_pXmlDoc );

	return ( ( NULL != m_pXmlDoc ) && ( NULL != m_pXPathCtx ) );
}

bool
ConfigModel::query( const TCHAR* expr, ConfigModelData& data ) const
{
	configUtil::ScopedMutex lock( &m_mutex );

	bool result = true;
	char exprBuffer[TEMP_BUFFER_SIZE] = {0};
	TCHAR contentBuffer[TEMP_BUFFER_SIZE] = {0};
	configUtil::TCharToAnsi( exprBuffer, TEMP_BUFFER_SIZE, expr );

	xmlXPathObjectPtr pXPathObj = xmlXPathEvalExpression( 
		BAD_CAST exprBuffer, m_pXPathCtx );
	if ( !pXPathObj )
	{
		data.SetEmpty( );
		result = false;
	}
	else
	{
		switch ( pXPathObj->type )
		{
		case XPATH_BOOLEAN:
			data = ConfigModelData( ( pXPathObj->boolval > 0 ) ? true : false );
			break;
		case XPATH_NUMBER:
			// Determine if its an INT or FLOAT value.
			{
				double f = pXPathObj->floatval;
				int n = static_cast<int>( f );
				if ( n == f )
					data = ConfigModelData( n );
				else
					data = ConfigModelData( f );
			}
			break;
		case XPATH_STRING:
			data = ConfigModelData( (const TCHAR*)pXPathObj->stringval );
			break;
		case XPATH_NODESET:
			{
				if ( !pXPathObj->nodesetval )
					return ( false ); // "User error: XPath expression invalid."

				size_t count = pXPathObj->nodesetval->nodeNr;
				if ( count > 1 )
				{
					for ( size_t n = 0; n < count; ++n )
					{
						xmlNodePtr pNode = pXPathObj->nodesetval->nodeTab[n];
						xmlNodeGetContent( *pNode, contentBuffer, TEMP_BUFFER_SIZE );
						ConfigModelData childData = ConfigModelData( contentBuffer );
						data.push_child( childData );
					}
				}
				else if ( 1 == count )
				{
					// Collapse to single configModelData object.
					xmlNodePtr pNode = ( pXPathObj->nodesetval->nodeTab[0] );
					xmlNodeGetContent( *pNode, contentBuffer, TEMP_BUFFER_SIZE );
					data = ConfigModelData( contentBuffer );
				}
				else result = false;
			}
			break;
		case XPATH_POINT:
		case XPATH_RANGE:
		case XPATH_LOCATIONSET:
		case XPATH_USERS:
		case XPATH_XSLT_TREE:
		case XPATH_UNDEFINED:
		default:
			return ( false ); // "Internal error: XPath query return type not implemented." 
			break;
		}
	}
	xmlXPathFreeObject( pXPathObj );

	return ( result );
}

bool
ConfigModel::remove( const TCHAR* expr, bool flush )
{
	configUtil::ScopedMutex lock( &m_mutex );

	bool result = false;
	char exprBuffer[TEMP_BUFFER_SIZE] = {0};
	configUtil::TCharToAnsi( exprBuffer, TEMP_BUFFER_SIZE, expr );
	xmlXPathObjectPtr pXPathObj = xmlXPathEvalExpression( 
		BAD_CAST exprBuffer, m_pXPathCtx );

	// This is the check that ensures we can only set existing data.
	// We could expand this to provide an else clause to create
	// new data if required.
	if ( pXPathObj )
	{
		switch ( pXPathObj->type )
		{
		case XPATH_BOOLEAN:
			break;
		case XPATH_NUMBER:
			break;
		case XPATH_STRING:
			break;
		case XPATH_NODESET:
			{
				if ( !pXPathObj->nodesetval )
					return ( false ); // "User error: XPath expression invalid."

				size_t count = pXPathObj->nodesetval->nodeNr;
				if ( count > 1 )
				{
					return false; //only delete single nodes
				}
				else if ( 1 == count )
				{
					// Collapse to single configModelData object.
					xmlNodePtr pNode = ( pXPathObj->nodesetval->nodeTab[0] ); 						
					xmlUnlinkNode( pNode );
					xmlFree( pNode );

					if ( flush )
						result = save( );
					else
						result = true;
				}
			}
			break;
		case XPATH_POINT:
		case XPATH_RANGE:
		case XPATH_LOCATIONSET:
		case XPATH_USERS:
		case XPATH_XSLT_TREE:
		case XPATH_UNDEFINED:
		default:
			return ( false ); // "Internal error: XPath query return type not implemented." 
			break;
		}
	}

	xmlXPathFreeObject( pXPathObj );
	return ( result );
}

bool ConfigModel::add( const TCHAR* expr, const ConfigModelData& data, bool flush )
{
	configUtil::ScopedMutex lock( &m_mutex );

	xmlXPathFreeContext( m_pXPathCtx );
	m_pXPathCtx = xmlXPathNewContext( m_pXmlDoc );

	bool result = false;
	TCHAR contentBuffer[TEMP_BUFFER_SIZE] = {0};
	char contentBuffer2[TEMP_BUFFER_SIZE] = {0};
	char exprBuffer[TEMP_BUFFER_SIZE] = {0};
	configUtil::TCharToAnsi( exprBuffer, TEMP_BUFFER_SIZE, expr );
	xmlXPathObjectPtr pXPathObj = xmlXPathEvalExpression( 
		BAD_CAST exprBuffer, m_pXPathCtx );
	
	// This is the check that ensures we can only set existing data.
	// We could expand this to provide an else clause to create
	// new data if required.
	if ( pXPathObj && (pXPathObj->type == XPATH_NODESET) && (pXPathObj->nodesetval) )
	{
		switch ( pXPathObj->type )
		{
		case XPATH_BOOLEAN:
			break;
		case XPATH_NUMBER:
			break;
		case XPATH_STRING:
			break;
		case XPATH_NODESET:
			{
				if ( !pXPathObj->nodesetval )
					return ( false ); // "User error: XPath expression invalid."

				size_t count = pXPathObj->nodesetval->nodeNr;

				if(count == 0)
				{
					TCHAR exists[_MAX_PATH];
					_tcscpy(exists,expr);
					TCHAR* rear = _tcsrchr(exists,'/');
					xmlNodePtr pNode = NULL;

					if(rear)
					{
						*rear = 0;
						rear++;
						configUtil::TCharToAnsi( exprBuffer, TEMP_BUFFER_SIZE, exists );
						xmlXPathFreeObject( pXPathObj );
						pXPathObj = xmlXPathEvalExpression( 
							BAD_CAST exprBuffer, m_pXPathCtx );

						pNode = pXPathObj->nodesetval->nodeTab[0];				
					}
					else
						rear = exists;

					if(pNode && (rear[0] == '@'))
					{
						rear++;
						data.ToString( contentBuffer, TEMP_BUFFER_SIZE );
						configUtil::TCharToAnsi( contentBuffer2, TEMP_BUFFER_SIZE, contentBuffer );
						char save_attr[TEMP_BUFFER_SIZE];
						configUtil::TCharToAnsi( save_attr, TEMP_BUFFER_SIZE, rear );

						xmlSetProp(pNode,BAD_CAST(save_attr),BAD_CAST(contentBuffer2));
						if ( flush )
							result = save( );
						else
							result = true;
					}
				}
				else
				{
					xmlXPathFreeObject( pXPathObj );
					return false;
				}
			}
			break;
		case XPATH_POINT:
		case XPATH_RANGE:
		case XPATH_LOCATIONSET:
		case XPATH_USERS:
		case XPATH_XSLT_TREE:
		case XPATH_UNDEFINED:
		default:
			return ( false ); // "Internal error: XPath query return type not implemented." 
			break;
		}
	}

	if(!result)
	{
		TCHAR exists[_MAX_PATH];
		TCHAR buffer[_MAX_PATH];
		TCHAR toadd[_MAX_PATH] = {0};
		_tcscpy(exists,expr);
		TCHAR* found;

		do
		{
			found = _tcsrchr(exists,'/');

			if(!found)
				break;

			_tcscpy(buffer,toadd);
			_tcscpy(toadd,found);
			_tcscat(toadd,buffer);
			*found = 0;

			configUtil::TCharToAnsi( exprBuffer, TEMP_BUFFER_SIZE, exists );
			xmlXPathFreeObject( pXPathObj );
			pXPathObj = xmlXPathEvalExpression( 
				BAD_CAST exprBuffer, m_pXPathCtx );

		} while(((!pXPathObj) || (pXPathObj->type != XPATH_NODESET) || ((pXPathObj->type == XPATH_NODESET) && ((!pXPathObj->nodesetval) || (pXPathObj->nodesetval->nodeNr == 0))) && (found != NULL)));

		xmlNodePtr parent = NULL;

		if(pXPathObj && pXPathObj->nodesetval && (pXPathObj->nodesetval->nodeNr > 0))
			parent = pXPathObj->nodesetval->nodeTab[0];
		else
			parent = xmlDocGetRootElement(m_pXmlDoc);

		found = toadd;
		TCHAR* next = _tcschr(found,'/');

		while(found)
		{
			if(found != next)
			{
				if(next)
					*next = NULL;

				TCHAR* attribute = NULL;

				if(found[0] == '@')
				{
					attribute = ++found;
					char save_attr[TEMP_BUFFER_SIZE];
					configUtil::TCharToAnsi( save_attr, TEMP_BUFFER_SIZE, attribute );

					data.ToString( contentBuffer, TEMP_BUFFER_SIZE );
					configUtil::TCharToAnsi( contentBuffer2, TEMP_BUFFER_SIZE, contentBuffer );

					xmlSetProp(parent,BAD_CAST(save_attr),BAD_CAST(contentBuffer2));
					if ( flush )
						result = save( );
					else
						result = true;
				}
				else
				{
					TCHAR* qualifier = _tcschr(found,'[');
					TCHAR* value = NULL, * find = NULL;
					if(qualifier)
					{
						*qualifier = NULL;
						qualifier++;

						attribute = _tcschr(qualifier,'@');

						if(attribute)
						{
							attribute++;
							value = _tcschr(attribute,'=');

							if(value)
							{
								*value = 0;
								value++;

								find = _tcschr(value,']');
								if(find) *find = 0;
								find = _tcschr(value,'\"');
								if(!find) find = _tcschr(value,'\'');
								if(find)
								{
									value = ++find;
									find = _tcschr(value,'\"');
									if(!find) find = _tcschr(value,'\'');
									if(find) *find = 0;
								}
							}
						}
					}

					char name[TEMP_BUFFER_SIZE];
					configUtil::TCharToAnsi( name, TEMP_BUFFER_SIZE, found );

					xmlNodePtr node = xmlNewNode(NULL,BAD_CAST(name));

					if(parent)
						xmlAddChild(parent,node);
					else
					{
						if(!m_pXmlDoc)
							m_pXmlDoc = xmlNewDoc(BAD_CAST "1.0");

						xmlDocSetRootElement(m_pXmlDoc,node);
					}

					parent = node;

					if(attribute && value)
					{
						char save_attr[TEMP_BUFFER_SIZE];
						configUtil::TCharToAnsi( save_attr, TEMP_BUFFER_SIZE, attribute );
						char save_value[TEMP_BUFFER_SIZE];
						configUtil::TCharToAnsi( save_value, TEMP_BUFFER_SIZE, value );

						xmlSetProp(node,BAD_CAST(save_attr),BAD_CAST(save_value));
					}

					if(next == NULL)
					{
						if ( flush )
							result = save( );
						else
							result = true;
					}
				}
			}
			
			if(next == NULL)
				found = NULL;
			else
			{
				*next = NULL;
				next++;
				found = next;
				next = _tcschr(found,'/');
			}
		}
	}

	xmlXPathFreeObject( pXPathObj );
	return ( result );
}

bool 
ConfigModel::set( const TCHAR* expr, const ConfigModelData& data, bool flush, bool create )
{
	configUtil::ScopedMutex lock( &m_mutex );

	xmlXPathFreeContext( m_pXPathCtx );
	m_pXPathCtx = xmlXPathNewContext( m_pXmlDoc );

	bool result = false;
	TCHAR contentBuffer[TEMP_BUFFER_SIZE] = {0};
	char contentBuffer2[TEMP_BUFFER_SIZE] = {0};
	char exprBuffer[TEMP_BUFFER_SIZE] = {0};
	configUtil::TCharToAnsi( exprBuffer, TEMP_BUFFER_SIZE, expr );
	xmlXPathObjectPtr pXPathObj = xmlXPathEvalExpression( 
		BAD_CAST exprBuffer, m_pXPathCtx );
	
	// This is the check that ensures we can only set existing data.
	// We could expand this to provide an else clause to create
	// new data if required.
	if ( pXPathObj && (pXPathObj->type == XPATH_NODESET) && (pXPathObj->nodesetval) )
	{
		switch ( pXPathObj->type )
		{
		case XPATH_BOOLEAN:
			break;
		case XPATH_NUMBER:
			break;
		case XPATH_STRING:
			break;
		case XPATH_NODESET:
			{
				if ( !pXPathObj->nodesetval )
					return ( false ); // "User error: XPath expression invalid."

				size_t count = pXPathObj->nodesetval->nodeNr;
				if ( count > 1 )
				{
					for ( size_t n = 0; n < count; ++n )
					{
						xmlNodePtr pNode = pXPathObj->nodesetval->nodeTab[n];
						data.ToString( contentBuffer, TEMP_BUFFER_SIZE );
						configUtil::TCharToAnsi( contentBuffer2, TEMP_BUFFER_SIZE, contentBuffer );

						xmlNodeSetContent( pNode, BAD_CAST contentBuffer2 );
					}
					if ( flush )
						result = save( );
					else
						result = true;
				}
				else if ( 1 == count )
				{
					// Collapse to single configModelData object.
					xmlNodePtr pNode = ( pXPathObj->nodesetval->nodeTab[0] );
					data.ToString( contentBuffer, TEMP_BUFFER_SIZE );
					configUtil::TCharToAnsi( contentBuffer2, TEMP_BUFFER_SIZE, contentBuffer );
					
					xmlNodeSetContentLen( pNode, BAD_CAST contentBuffer2, (int)strlen( contentBuffer2 ) );
					if ( flush )
						result = save( );
					else
						result = true;
				}
			}
			break;
		case XPATH_POINT:
		case XPATH_RANGE:
		case XPATH_LOCATIONSET:
		case XPATH_USERS:
		case XPATH_XSLT_TREE:
		case XPATH_UNDEFINED:
		default:
			return ( false ); // "Internal error: XPath query return type not implemented." 
			break;
		}
	}

	xmlXPathFreeObject( pXPathObj );

	if(!result && create)
		result = add(expr, data, flush);

	return ( result );
}

bool 
ConfigModel::save( )
{
	char ansiFilename[_MAX_PATH];
	configUtil::TCharToAnsi( ansiFilename, _MAX_PATH, m_sFilename );

	FILE* pFP = fopen( ansiFilename, "w" );
	if ( !pFP )
		return ( false );
	
	const char* toolsroot = getenv("RS_TOOLSROOT");
	if ( toolsroot != NULL )
	{
		char xslDocument[_MAX_PATH] = {0};
		sprintf_s(xslDocument, _MAX_PATH, "%s\\libs\\util\\xmlpp.xsl", toolsroot);	
		xsltStylesheet* cur = xsltParseStylesheetFile((const xmlChar *)xslDocument);
		if ( cur != NULL )
		{
			xsltApplyStylesheet(cur, m_pXmlDoc, NULL);
		}
	}
	else
	{
		//Unable to validate against the XSL.
		return false;
	}

	int saveResult = xmlDocFormatDump( pFP, m_pXmlDoc, 1 );
	if ( saveResult > 0 )
		load( m_sFilename );

	fclose(pFP);

	return ( saveResult > 0 );
}

void
ConfigModel::xmlNodeGetContent( const xmlNode& node, TCHAR* data, size_t size ) const
{
	switch ( node.type )
	{
	case XML_ATTRIBUTE_NODE:
	case XML_ELEMENT_NODE:
		if(node.children)
			xmlNodeGetContent( node.children[0], data, size );
		else
			configUtil::AnsiToTChar( data, size, (const char*)node.name );
		break;
	case XML_TEXT_NODE:
		configUtil::AnsiToTChar( data, size, (const char*)node.content );
		break;
	default:
		throw configModelException( "Internal error: unknown data node." );
	}
}

END_CONFIGPARSER_NS
