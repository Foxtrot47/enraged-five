#ifndef __CONFIGPARSER_CONFIGPORTVIEW_H__
#define __CONFIGPARSER_CONFIGPORTVIEW_H__

//
// filename:	configPortView.h
// author:		Michael Taschler <michael.taschler@rockstarnorth.com>
// date:		09 April 2014
// description:	
//

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

// --- Includes -----------------------------------------------------------------

// Local headers
#include "configCoreView.h"
#include "contentModel.h"
#include "configModelData.h"

#include <map>

// --- Defines ------------------------------------------------------------------
#define LG_SIZE (1024)

// --- Forward Declarations -----------------------------------------------------
namespace configUtil
{
	class Environment;
}

// --- Classes ------------------------------------------------------------------

BEGIN_CONFIGPARSER_NS

//PURPOSE
// A higher level view than ConfigPortView.  This view understands the tools
// configuration data in ports.xml and ports.local.xml a little more than just
// loading them for XPath evaluation.
//
class ConfigPortView
{
public:
	ConfigPortView( );
	~ConfigPortView( );

	bool GetPortForId( const TCHAR* key, int& port ) const;

private:
	// Hide copy constructor and assignment operator.
	ConfigPortView( const ConfigPortView& copy );
	ConfigPortView& operator=( ConfigPortView& copy );

	void LoadFromModel(const ConfigModel& data);

	bool Query( const ConfigModel& model, const TCHAR* expr, ConfigModelData& data ) const;
	bool Query( const ConfigModel& model, const TCHAR* expr, TCHAR* data, size_t size ) const;
	bool Query( const ConfigModel& model, const TCHAR* expr, int& data ) const;
	size_t GetNodeCountForQuery(const ConfigModel& model, const TCHAR* expr) const;

	bool GetPortId( const ConfigModel& model, size_t index, TCHAR* name, size_t size ) const;
	bool GetPortValue( const ConfigModel& model, size_t index, int& port ) const;

	TCHAR	m_sPortsConfig[_MAX_PATH];
	TCHAR	m_sPortsLocalConfig[_MAX_PATH];

	configUtil::Environment* m_pEnv;		// Config environment object.
	ConfigModel	m_PortModel;
	ConfigModel	m_PortLocalModel;

	std::map<STL_STRING, int>	m_portMap;
};

END_CONFIGPARSER_NS

#endif // __CONFIGPARSER_CONFIGPORTVIEW_H__
