//
// filename:	contentNodeMaps.cpp
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		11 February 2010
// description:	Content-tree node base class.
//

// LibXml2 headers
#include "libxml/parser.h"
#include "libxml/xpath.h"

// Local headers
#include "configParser/contentNodeMaps.h"

BEGIN_CONFIGPARSER_NS

// --- ContentNodeMap -------------------------------------------------------

ContentNodeMap::ContentNodeMap( ContentNode* parent,
								const configUtil::Environment* pEnv,
								const TCHAR* name,
								const TCHAR* subtype,
								const TCHAR* prefix,
								const bool definitions, 
								const bool instances, 
								const bool slod2link,
								const bool data, 
								const bool pedantic )
	: ContentNode( parent, pEnv, name, _T("map") )
	, m_seed( 0L )
	, m_bDefinitions( definitions )
	, m_bInstances( instances )
	, m_bSLOD2Link( slod2link )
	, m_bData( data )
	, m_bPedantic( pedantic )
{
	InitMapType( subtype );
	_tcsncpy_s( m_sPrefix, SM_SIZE, prefix, _TRUNCATE );
}

ContentNodeMap::ContentNodeMap( ContentNode* parent,
							   const configUtil::Environment* pEnv, 
							   xmlElementPtr pElement )
	: ContentNode( parent, pEnv, pElement )
	, m_seed( 0L )
	, m_bDefinitions( false )
	, m_bInstances( false )
	, m_bSLOD2Link( false )
	, m_bData( true )
	, m_bPedantic( true )
{
	_tcsncpy_s( m_sPrefix, SM_SIZE, _T(""), _TRUNCATE );

	xmlAttributePtr pAttribute = pElement->attributes;
	while ( pAttribute )
	{
		if ( 0 == xmlStrcasecmp( BAD_CAST "maptype", pAttribute->name ) )
		{
			TCHAR subtype[MAX_PATH] = {0};
			configUtil::AnsiToTChar( subtype, SM_SIZE, 
				(char*)pAttribute->children->content );
			InitMapType( subtype );
		}
		else if ( 0 == xmlStrcasecmp( BAD_CAST "definitions", pAttribute->name ) )
		{
			m_bDefinitions = (0 == xmlStrcasecmp( BAD_CAST "true", pAttribute->children->content ) );
		}
		else if ( 0 == xmlStrcasecmp( BAD_CAST "instances", pAttribute->name ) )
		{
			m_bInstances = (0 == xmlStrcasecmp( BAD_CAST "true", pAttribute->children->content ) );
		}
		else if ( 0 == xmlStrcasecmp( BAD_CAST "slod2link", pAttribute->name ) )
		{
			m_bSLOD2Link = (0 == xmlStrcasecmp( BAD_CAST "true", pAttribute->children->content ) );
		}
		else if ( 0 == xmlStrcasecmp( BAD_CAST "data", pAttribute->name ) )
		{
			m_bData = (0 == xmlStrcasecmp( BAD_CAST "true", pAttribute->children->content ) );
		}
		else if ( 0 == xmlStrcasecmp( BAD_CAST "pedantic", pAttribute->name ) )
		{
			m_bPedantic = (0 == xmlStrcasecmp( BAD_CAST "true", pAttribute->children->content ) );
		}
		else if ( 0 == xmlStrcasecmp( BAD_CAST "auto_export", pAttribute->name ) )
		{
			m_bAutoExport = (0 == xmlStrcasecmp( BAD_CAST "true", pAttribute->children->content ) );
		}
		else if ( 0 == xmlStrcasecmp( BAD_CAST "prefix", pAttribute->name ) )
		{
			configUtil::AnsiToTChar( m_sPrefix, SM_SIZE,
				(char*)pAttribute->children->content );
		}
		else if ( 0 == xmlStrcasecmp( BAD_CAST "seed", pAttribute->name ) )
		{
			const char* pSeed = (const char*)pAttribute->children->content;
			m_seed = atoi( pSeed );
		}

		// Next attribute.
		pAttribute = (xmlAttributePtr)( pAttribute->next );
	}
}

ContentNodeMap::~ContentNodeMap( )
{
}

void
ContentNodeMap::SetPrefix( const TCHAR* prefix )
{
	_tcsncpy_s( m_sPrefix, SM_SIZE, prefix, _TRUNCATE );
}

void
ContentNodeMap::GetAbsolutePath( TCHAR* path, size_t size ) const
{	
	GetRawAbsolutePath( path, size );
	m_pEnv->subst( path, size );
}

void
ContentNodeMap::GetRawAbsolutePath( TCHAR* path, size_t size ) const
{
	memset( path, 0, size * sizeof( TCHAR ) );
	const ContentNodeGroup* pParent =
		dynamic_cast<const ContentNodeGroup*>( this->GetParent() );
	if ( pParent )
	{
		TCHAR parentPath[_MAX_PATH] = {0};
		pParent->GetRawAbsolutePath( parentPath, _MAX_PATH );

		_tcscat_s( path, size, parentPath );
	}
}

xmlNodePtr
ContentNodeMap::ToXml() const
{
	xmlNodePtr pElement = ContentNode::ToXml();
	char ansiPrefix[SM_SIZE] =  {0};
	configUtil::TCharToAnsi( ansiPrefix, SM_SIZE, m_sPrefix );
	size_t lenPrefix = strlen( ansiPrefix );

	xmlNewProp( pElement, BAD_CAST "definitions", m_bDefinitions ? BAD_CAST "true" : BAD_CAST "false" );
	xmlNewProp( pElement, BAD_CAST "instances", m_bInstances ? BAD_CAST "true" : BAD_CAST "false" );
	if ( lenPrefix > 0 )
		xmlNewProp( pElement, BAD_CAST "prefix", BAD_CAST ansiPrefix );

	switch ( this->m_eMapType )
	{
	case INTERIOR:
		xmlNewProp( pElement, BAD_CAST "maptype", BAD_CAST "interior" );
		break;
	case PROPS:
		xmlNewProp( pElement, BAD_CAST "maptype", BAD_CAST "props" );
		break;
	case CONTAINER:
		xmlNewProp( pElement, BAD_CAST "maptype", BAD_CAST "container" );
		break;
	case CONTAINER_PROPS:
		xmlNewProp( pElement, BAD_CAST "maptype", BAD_CAST "container_props" );
		break;
	case CONTAINER_LOD:
		xmlNewProp( pElement, BAD_CAST "maptype", BAD_CAST "container_lod" );
		break;
	case CONTAINER_OCCL:
		xmlNewProp( pElement, BAD_CAST "maptype", BAD_CAST "container_occl" );
		break;
	case UNKNOWN:
	default:
		break; // Don't add maptype attribute.
	}

	for ( NodeContainerConstIter it = BeginChildren();
		  it != EndChildren();
		  ++it )
	{
		xmlNodePtr pChild = (*it)->ToXml( );
		xmlAddChild( pElement, pChild );
	}

	return ( pElement );
}

void
ContentNodeMap::InitMapType( const TCHAR* subtype )
{
	if ( 0 == _tcsicmp( subtype, _T("interior") ) )
		this->m_eMapType = INTERIOR;
	else if ( 0 == _tcsicmp( subtype, _T("props") ) ) 
		this->m_eMapType = PROPS;
	else if ( 0 == _tcsicmp( subtype, _T("container") ) ) 
		this->m_eMapType = CONTAINER;
	else if ( 0 == _tcsicmp( subtype, _T("container_props") ) )
		this->m_eMapType = CONTAINER_PROPS;
	else if ( 0 == _tcsicmp( subtype, _T("container_lod") ) )
		this->m_eMapType = CONTAINER_LOD;
	else if ( 0 == _tcsicmp( subtype, _T("container_occl") ) )
		this->m_eMapType = CONTAINER_OCCL;
	else
		this->m_eMapType = UNKNOWN;
}

// --- ContentNodeMapZip ----------------------------------------------------

ContentNodeMapZip::ContentNodeMapZip( ContentNode* parent,
									 const configUtil::Environment* pEnv,
									 const TCHAR* name, 
									 Platform platform )
	: ContentNodeZip( parent, pEnv, name, _T(""), _T("zip"), platform, true ),
	  m_bViaBoundsProcessor(false)
{
}


ContentNodeMapZip::ContentNodeMapZip( ContentNode* parent,
									 const configUtil::Environment* pEnv, 
									 xmlElementPtr pElement )
	: ContentNodeZip( parent, pEnv, pElement ),
	  m_bViaBoundsProcessor(false)
{
	_tcsncpy_s( m_sExtension, _MAX_PATH, _T("zip"), _TRUNCATE );

	xmlAttributePtr pAttribute = pElement->attributes;
	while ( pAttribute )
	{
		if ( 0 == xmlStrcasecmp( BAD_CAST "viaboundsprocessor", pAttribute->name ) )
		{
			m_bViaBoundsProcessor = (0 == xmlStrcasecmp( BAD_CAST "true", pAttribute->children->content ) );
		}

		// Next attribute.
		pAttribute = (xmlAttributePtr)( pAttribute->next );
	}
}

ContentNodeMapZip::~ContentNodeMapZip( )
{
	for ( NodeContainerIter it = BeginChildren();
		it != EndChildren();
		++it )
	{
		ContentNode* pNode = (*it);
		delete pNode;
	}
	m_vChildren.clear( );
}

void 
ContentNodeMapZip::PostLoadCallback( )
{
	if ( 1 != GetNumInputs() )
		return;

	ContentNodeMap* pInput = dynamic_cast<ContentNodeMap*>( GetInput( 0 ) );
	if ( !pInput )
		return;

	// SceneXml always there for maps.
	AddChild( new ContentNodeTarget( NULL, m_pEnv, m_sName, m_sRelativePath, _T("xml"), m_Platform ) );
	if ( pInput->GetData( ) )
		AddChild( new ContentNodeTarget( NULL, m_pEnv, m_sName, m_sRelativePath, _T("zip"), m_Platform ) );
}


// --- ContentNodeMapProcessedZip ----------------------------------------------------

ContentNodeMapProcessedZip::ContentNodeMapProcessedZip( ContentNode* parent,
	const configUtil::Environment* pEnv,
	const TCHAR* name, 
	Platform platform )
	: ContentNodeZip( parent, pEnv, name, _T(""), _T("zip"), platform, true )
{
}


ContentNodeMapProcessedZip::ContentNodeMapProcessedZip( ContentNode* parent,
	const configUtil::Environment* pEnv, 
	xmlElementPtr pElement )
	: ContentNodeZip( parent, pEnv, pElement )
{
	_tcscpy_s( m_sExtension, _MAX_PATH, _T("zip") );
}

ContentNodeMapProcessedZip::~ContentNodeMapProcessedZip( )
{
	for ( NodeContainerIter it = BeginChildren();
		it != EndChildren();
		++it )
	{
		ContentNode* pNode = (*it);
		delete pNode;
	}
	m_vChildren.clear( );
}

void 
ContentNodeMapProcessedZip::PostLoadCallback( )
{
}

// --- ContentNodeMapCombineZip ----------------------------------------------

ContentNodeMapCombineZip::ContentNodeMapCombineZip( ContentNode* parent,
	const configUtil::Environment* pEnv,
	const TCHAR* name, 
	Platform platform )
	: ContentNodeZip( parent, pEnv, name, _T(""), _T("zip"), platform, true )
{
}


ContentNodeMapCombineZip::ContentNodeMapCombineZip( ContentNode* parent,
	const configUtil::Environment* pEnv, 
	xmlElementPtr pElement )
	: ContentNodeZip( parent, pEnv, pElement )
{
	_tcscpy_s( m_sExtension, _MAX_PATH, _T("zip") );
}

ContentNodeMapCombineZip::~ContentNodeMapCombineZip( )
{
	for ( NodeContainerIter it = BeginChildren();
		it != EndChildren();
		++it )
	{
		ContentNode* pNode = (*it);
		delete pNode;
	}
	m_vChildren.clear( );
}

END_CONFIGPARSER_NS
