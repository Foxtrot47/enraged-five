//
// filename:	contentNodeUnknown.cpp
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		22 February 2010
// description:	Content-tree unknown node class.
//

// Local headers
#include "configParser/contentNodeUnknown.h"

BEGIN_CONFIGPARSER_NS

ContentNodeUnknown::ContentNodeUnknown( ContentNode* parent,
									   const configUtil::Environment* pEnv,
									   const TCHAR* name, 
									   const TCHAR* type )
	: ContentNodeGroup( parent, pEnv, name, type )
{
}

ContentNodeUnknown::ContentNodeUnknown( ContentNode* parent,
									   const configUtil::Environment* pEnv, 
									   xmlElementPtr pElement )
	: ContentNodeGroup( parent, pEnv, pElement )
{
}

ContentNodeUnknown::~ContentNodeUnknown( )
{
}

END_CONFIGPARSER_NS
