//
// filename:	configCoreView.cpp
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		18 December 2009
// description:	Base view of configuration data, including environment substitution.
//
#pragma warning(disable: 4996)

// Local headers
#include "configParser/configCoreView.h"
#include "configParser/configModelData.h"

// Other headers
#include "configUtil/Environment.h"
#include "configUtil/path.h"

#include "string/string.h"
#include "string/unicode.h"

// LibXml2 headers
#include "libxml/xpath.h"

using namespace configUtil;

// --- Defines ------------------------------------------------------------------
#define CONFIG_FILENAME		( _T("config.xml") )
#define PROJECT_FILENAME	( _T("project.xml") )
#define LOCAL_FILENAME		( _T("local.xml") )
#ifndef MAX_PATH
#define MAX_PATH			(260)
#endif //MAX_PATH
#define TEMP_BUFFER_SIZE	(1024)

// --- Implementation -----------------------------------------------------------

BEGIN_CONFIGPARSER_NS

ConfigCoreView::ConfigCoreView( )
	: m_pEnv( NULL )
{
	memset( m_sGlobalConfig, 0, MAX_PATH * sizeof( TCHAR ) );
	memset( m_sGlobalLocalConfig, 0, MAX_PATH * sizeof( TCHAR ) );
	memset( m_sProjectConfig, 0, MAX_PATH * sizeof( TCHAR ) );
	memset( m_sProjectLocalConfig, 0, MAX_PATH * sizeof( TCHAR ) );

	m_pEnv = new Environment;
	m_pEnv->import_core_globals( );
	m_pEnv->import_secondary_globals( );

	TCHAR toolsroot[MAX_PATH] = {0};
	TCHAR toolsconfig[MAX_PATH] = {0};
	m_pEnv->lookup( VAR_TOOLSROOT, toolsroot, MAX_PATH );
	m_pEnv->lookup( VAR_TOOLSCONFIG, toolsconfig, MAX_PATH );
	
	// $(toolsroot)/config.xml
	Path::combine( m_sGlobalConfig, MAX_PATH, 2, toolsroot, CONFIG_FILENAME );
	// $(toolsroot)/local.xml
	Path::combine( m_sGlobalLocalConfig, MAX_PATH, 2, toolsroot, LOCAL_FILENAME );
	
	// $(toolsconfig)/config.xml
	Path::combine( m_sProjectConfig, MAX_PATH, 2, toolsconfig, PROJECT_FILENAME );
	// $(toolsconfig)/local.xml
	Path::combine( m_sProjectLocalConfig, MAX_PATH, 2, toolsconfig, LOCAL_FILENAME );

	ConfigModelData networkPathData;
	m_GlobalModel.load( m_sGlobalConfig );
	m_GlobalLocalModel.load( m_sGlobalLocalConfig );
	m_ProjectModel.load( m_sProjectConfig );
	m_ProjectLocalModel.load( m_sProjectLocalConfig );

	//determine studio...	

	WSADATA WSAData;
	if(::WSAStartup(MAKEWORD(1,0),&WSAData) != 0)
		return;

	FetchStudio();

	// Cleanup
	WSACleanup();
}

ConfigCoreView::~ConfigCoreView( )
{
	m_pEnv->pop( );
	m_pEnv->clear( );
	delete m_pEnv;
	m_pEnv = NULL;
}

bool 
ConfigCoreView::GetToolsRootDir( TCHAR* path, size_t size ) const
{
	return ( m_pEnv->lookup( VAR_TOOLSROOT, path, size ) );
}

bool 
ConfigCoreView::GetToolsBinDir( TCHAR* path, size_t size ) const
{
	return ( m_pEnv->lookup( VAR_TOOLSBIN, path, size ) );
}

bool 
ConfigCoreView::GetToolsLibDir( TCHAR* path, size_t size ) const
{
	return ( m_pEnv->lookup( VAR_TOOLSLIB, path, size ) );
}

bool
ConfigCoreView::GetToolsConfigDir( TCHAR* path, size_t size ) const
{
	return ( m_pEnv->lookup( VAR_TOOLSCONFIG, path, size ) );
}

bool 
ConfigCoreView::GetToolsRuby( TCHAR* path, size_t size ) const
{
	return ( m_pEnv->lookup( VAR_TOOLSRUBY, path, size ) );
}

bool 
ConfigCoreView::GetToolsLogDir( TCHAR* path, size_t size ) const
{
	_stprintf_s( path, size, _T("$(%s)/logs"), VAR_TOOLSROOT );
	m_pEnv->subst( path, size );
	return ( true );
}

bool 
ConfigCoreView::GetToolsTempDir( TCHAR* path, size_t size ) const
{
	_stprintf_s( path, size, _T("$(%s)/tmp"), VAR_TOOLSROOT );
	m_pEnv->subst( path, size );
	return ( true );
}

size_t 
ConfigCoreView::GetNumUserTypes( ) const
{
	configParser::ConfigModelData data;
	bool result = query( _T("//config/usertypes/usertype/@name"), data );
	return ( result ? data.ChildCount() : 0L );	
}

bool 
ConfigCoreView::GetUserType( size_t index, UserType& userType ) const
{
	configParser::ConfigModelData data;
	index++;

	TCHAR queryStr[MAX_PATH];
	TCHAR value[MAX_PATH];

	_stprintf_s( queryStr, MAX_PATH, _T("//config/usertypes/usertype[%d]/@name"), index );
	if(query( queryStr, data ))
	{
		data.GetValue(value,MAX_PATH);
		userType.SetName(value);
	}

	_stprintf_s( queryStr, MAX_PATH, _T("//config/usertypes/usertype[%d]/@uiname"), index );
	if(query( queryStr, data ))
	{
		data.GetValue(value,MAX_PATH);
		userType.SetFriendlyName(value);
	}

	_stprintf_s( queryStr, MAX_PATH, _T("//config/usertypes/usertype[%d]/@flags"), index );
	if(query( queryStr, data ))
	{
		TCHAR* p;
		data.GetValue(value,MAX_PATH);
		userType.SetMask(_tcstol(value,&p,16));
	}

	userType.SetIsPseudo(false);
	_stprintf_s( queryStr, MAX_PATH, _T("//config/usertypes/usertype[%d]/@pseudo"), index );
	if(query( queryStr, data ))
	{
		data.GetValue( value, MAX_PATH );
	
		if(_tcscmp(value,_T("true")) == 0)
			userType.SetIsPseudo(true);
	}

	return true;
}

bool 
ConfigCoreView::GetUserName( TCHAR* username, size_t size ) const
{
	return ( query( _T("/local/user/@username"), username, size ) );
}

bool
ConfigCoreView::SetUserName( const TCHAR* username )
{
	return ( set( _T("/local/user/@username"), username, false, ConfigCoreView::GLOBAL, true ) );
}

bool 
ConfigCoreView::GetUserEmailAddress( TCHAR* email, size_t size ) const
{
	return ( query( _T("/local/user/@emailaddress"), email, size ) );
}

bool
ConfigCoreView::SetUserEmailAddress( const TCHAR* email )
{
	return ( set( _T("/local/user/@emailaddress"), email, false, ConfigCoreView::GLOBAL, true ) );
}

bool 
ConfigCoreView::GetUserFlags( unsigned int& flags ) const
{
	int iflags;
	bool result = ( query( _T("/local/user/@flags"), iflags ) );
	flags = (unsigned int)( iflags );
	return ( result );
}

bool
ConfigCoreView::SetUserFlags( unsigned int flags )
{
	int iflags = (int)flags;
	return ( set( _T("/local/user/@flags"), iflags, false, ConfigCoreView::GLOBAL, true ) );
}

bool 
ConfigCoreView::GetUseXoreaxXGE( ) const
{
	bool useXge = true;
	query( _T("/local/applications/app[@name='xge']/@enabled"), useXge );
	return ( useXge );
}

bool 
ConfigCoreView::SetUseXoreaxXGE( bool value )
{
	return ( set( _T("/local/applications/app[@name='xge']/@enabled"), value, false, ConfigCoreView::GLOBAL, true ) );
}

bool 
ConfigCoreView::GetLogToStdOut( ) const
{
	bool sout = true;
	query( _T("/local/logging/@stdout"), sout );
	return ( sout );
}

bool 
ConfigCoreView::SetLogToStdOut( bool value )
{
	return ( set( _T("/local/logging/@stdout"), value, false, ConfigCoreView::GLOBAL, true ) );
}

int 
ConfigCoreView::GetLogLevel( ) const
{
	int level = 1;
	query( _T("/local/logging/@level"), level );
	return ( level );
}

bool 
ConfigCoreView::SetLogLevel( int level )
{
	return ( set( _T("/local/logging/@level"), level, false, ConfigCoreView::GLOBAL, true ) );
}

bool 
ConfigCoreView::GetLogTraceInfo( ) const
{
	bool trace;
	query( _T("/local/logging/@trace"), trace );
	return ( trace );
}

bool 
ConfigCoreView::SetLogTraceInfo( bool value )
{
	return ( set( _T("/local/logging/@trace"), value, false, ConfigCoreView::GLOBAL, true ) );
}

bool 
ConfigCoreView::GetLogMailErrors( ) const
{
	bool enabled;
	query( _T("/local/logging/logmails/@enabled"), enabled );
	return ( enabled );
}

bool 
ConfigCoreView::SetLogMailErrors( bool value )
{
	return ( set( _T("/local/logging/logmails/@enabled"), value, false, ConfigCoreView::GLOBAL, true ) );
}

int 
ConfigCoreView::GetLogMailAddresses( std::vector<const TCHAR *>& values)
{
	ConfigModelData cdata;
	query( _T("//config/logging/logmails/logmail/@email"), cdata );

	if(!cdata.IsEmpty())
	{
		// DHM: FIX ME: this will likely leak from the RSG.Base.ConfigParser wrapper!!!!!!
		TCHAR *mailAddi = (TCHAR *)malloc(sizeof(TCHAR)*255);
		cdata.GetValue(mailAddi, 255);	
		printf("Email address: %s\n", mailAddi);
		values.push_back(mailAddi);
	}
	else if(cdata.ChildCount()>0)
	{
		for(ConfigModelData::childContainerConstIter child = cdata.begin();
			child!=cdata.end();
			child++)
		{
			TCHAR *mailAddi = (TCHAR *)malloc(sizeof(TCHAR)*255);
			(*child).GetValue(mailAddi, 255);
			printf("Email address: %s\n", mailAddi);
			values.push_back(mailAddi);
		}
	}
	return (int)values.size();
}

bool 
ConfigCoreView::GetLogMailServerHost( TCHAR* server, size_t size ) const
{
	return ( query( _T("//config/logging/logmails/@server" ), server, size ) );
}

int
ConfigCoreView::GetLogMailServerPort( ) const
{
	int port = 25; // default SMTP port.
	if ( query( _T( "//config/logging/logmails/@port" ), port ) ) 
		return ( port );
	return ( 25 );
}

size_t 
ConfigCoreView::GetDCCPackageCount( const TCHAR* package )
{
	int count = 0;
	TCHAR xpath[_MAX_PATH] = {0};
	_stprintf_s( xpath, _MAX_PATH, _T("count(/local/applications/app[@name='%s']/instance)"), package );
	query_global( xpath, count );
	return ( count );
}

bool 
ConfigCoreView::GetDCCPackageDetails( const TCHAR* package, size_t index, DCCPackage& details ) const
{
	TCHAR xpath_version[_MAX_PATH] = {0};
	TCHAR xpath_installdir[_MAX_PATH] = {0};
	TCHAR xpath_platform[_MAX_PATH] = {0};

	_stprintf_s( xpath_version, _MAX_PATH, _T("/local/applications/app[@name='%s']/instance[%d]/@version"), package, index+1 );
	_stprintf_s( xpath_installdir, _MAX_PATH, _T("/local/applications/app[@name='%s']/instance[%d]/@path"), package, index+1 );
	_stprintf_s( xpath_platform, _MAX_PATH, _T("/local/applications/app[@name='%s']/instance[%d]/@platform"), package, index+1 );

	bool v = query_global( xpath_version, (details.version), _MAX_PATH );
	bool d = query_global( xpath_installdir, (details.installdir), _MAX_PATH );
	bool p = query_global( xpath_platform, (details.platform), _MAX_PATH );

	return ( v && d && p );
}

bool
ConfigCoreView::GetNetworkConfigFilename( TCHAR* path, size_t size ) const
{
	_tcsncpy_s( path, size, m_sNetworkConfig, _TRUNCATE );
	return ( true );
}

bool
ConfigCoreView::GetGlobalConfigFilename( TCHAR* path, size_t size ) const
{
	_tcsncpy_s( path, size, m_sGlobalConfig, _TRUNCATE );
	return ( true );
}

bool 
ConfigCoreView::GetGlobalLocalConfigFilename( TCHAR* path, size_t size ) const
{
	_tcsncpy_s( path, size, m_sGlobalLocalConfig, _TRUNCATE );
	return ( true );
}

bool
ConfigCoreView::GetProjectConfigFilename( TCHAR* path, size_t size ) const
{
	_tcsncpy_s( path, size, m_sProjectConfig, _TRUNCATE );
	return ( true );
}

bool
ConfigCoreView::GetProjectLocalConfigFilename( TCHAR* path, size_t size ) const
{
	_tcsncpy_s( path, size, m_sProjectLocalConfig, _TRUNCATE );
	return ( true );
}

bool 
ConfigCoreView::query( const TCHAR* expr, ConfigModelData& data ) const
{
	data = ConfigModelData( );
	// Try project configuration data in our varying levels of configuration
	// data.  Project --> Global --> Network is the fall-through pattern.
	if ( !query_model_raw( expr, m_ProjectLocalModel, data ) )
		if ( !query_model_raw( expr, m_ProjectModel, data ) )
			if ( !query_model_raw( expr, m_GlobalLocalModel, data ) )
				if ( !query_model_raw( expr, m_GlobalModel, data ) )
				{
					data = ConfigModelData( );
				}
	return ( !data.IsEmpty( ) || data.HasChildren( ) );
}

bool 
ConfigCoreView::query( const TCHAR* expr, TCHAR* data, size_t size ) const
{
	ConfigModelData cdata;
	query( expr, cdata );
	
	if ( ConfigModelData::CONFIGMODEL_DATA_STRING == cdata.GetType() )
	{
		cdata.GetValue( data, size );
		m_pEnv->subst( data, size );
	}

	return ( ConfigModelData::CONFIGMODEL_DATA_STRING == cdata.GetType() );
}

bool 
ConfigCoreView::query( const TCHAR* expr, bool& data ) const
{
	ConfigModelData cdata;
	query( expr, cdata );

	if ( ConfigModelData::CONFIGMODEL_DATA_BOOL == cdata.GetType() )
	{
		cdata.GetValue( data );
		return true;
	}
	else if ( ConfigModelData::CONFIGMODEL_DATA_STRING == cdata.GetType() )
	{
		// Attempt to convert string data into bool data.
		TCHAR sdata[TEMP_BUFFER_SIZE];
		cdata.GetValue( sdata, TEMP_BUFFER_SIZE );

		if(_tcscmp(sdata,_T("true")) == 0)
			data = true;
		else
			data = false;

		return true;
	}
	
	return false;
}

bool 
ConfigCoreView::query( const TCHAR* expr, int& data ) const
{
	ConfigModelData cdata;
	query( expr, cdata );

	if ( ConfigModelData::CONFIGMODEL_DATA_INT == cdata.GetType() )
	{
		cdata.GetValue( data );
		return true;
	}
	else if ( ConfigModelData::CONFIGMODEL_DATA_STRING == cdata.GetType() )
	{
		// Attempt to convert string data into int data.
		TCHAR sdata[TEMP_BUFFER_SIZE];
		cdata.GetValue( sdata, TEMP_BUFFER_SIZE );
		data = _tstoi( sdata );
		return true;
	}
	
	return false;
}

bool 
ConfigCoreView::query( const TCHAR* expr, double& data ) const
{
	ConfigModelData cdata;
	query( expr, cdata );

	if ( ConfigModelData::CONFIGMODEL_DATA_FLOAT == cdata.GetType() )
	{
		cdata.GetValue( data );
		return true;
	}
	else if ( ConfigModelData::CONFIGMODEL_DATA_STRING == cdata.GetType() )
	{
		// Attempt to convert string data into float data.
		TCHAR sdata[TEMP_BUFFER_SIZE];
		cdata.GetValue( sdata, TEMP_BUFFER_SIZE );
		data = _tstof( sdata );
		return true;
	}

	return false;
}


bool 
ConfigCoreView::query_global( const TCHAR* expr, ConfigModelData& data ) const
{
	data = ConfigModelData( );
	// Try Global --> Network is the fall-through pattern.
	if ( !query_model_raw( expr, m_GlobalLocalModel, data ) )
		if ( !query_model_raw( expr, m_GlobalModel, data ) )
		{
			data = ConfigModelData( );
		}
	return ( !data.IsEmpty( ) || data.HasChildren( ) );
}

bool 
ConfigCoreView::query_global( const TCHAR* expr, TCHAR* data, size_t size ) const
{
	ConfigModelData cdata;
	query_global( expr, cdata );
	
	if ( ConfigModelData::CONFIGMODEL_DATA_STRING == cdata.GetType() )
	{
		cdata.GetValue( data, size );
		m_pEnv->subst( data, size );
	}

	return ( ConfigModelData::CONFIGMODEL_DATA_STRING == cdata.GetType() );
}

bool 
ConfigCoreView::query_global( const TCHAR* expr, bool& data ) const
{
	ConfigModelData cdata;
	query_global( expr, cdata );

	if ( ConfigModelData::CONFIGMODEL_DATA_BOOL == cdata.GetType() )
		cdata.GetValue( data );
	
	return ( ConfigModelData::CONFIGMODEL_DATA_BOOL == cdata.GetType() );
}

bool 
ConfigCoreView::query_global( const TCHAR* expr, int& data ) const
{
	ConfigModelData cdata;
	query_global( expr, cdata );

	if ( ConfigModelData::CONFIGMODEL_DATA_INT == cdata.GetType() )
	{
		cdata.GetValue( data );
		return true;
	}
	else if ( ConfigModelData::CONFIGMODEL_DATA_STRING == cdata.GetType() )
	{
		// Attempt to convert string data into int data.
		TCHAR sdata[TEMP_BUFFER_SIZE];
		cdata.GetValue( sdata, TEMP_BUFFER_SIZE );
		data = _tstoi( sdata );
		return true;
	}
	
	return false;
}

bool 
ConfigCoreView::query_global( const TCHAR* expr, double& data ) const
{
	ConfigModelData cdata;
	query_global( expr, cdata );

	if ( ConfigModelData::CONFIGMODEL_DATA_FLOAT == cdata.GetType() )
	{
		cdata.GetValue( data );
		return true;
	}
	else if ( ConfigModelData::CONFIGMODEL_DATA_STRING == cdata.GetType() )
	{
		// Attempt to convert string data into float data.
		TCHAR sdata[TEMP_BUFFER_SIZE];
		cdata.GetValue( sdata, TEMP_BUFFER_SIZE );
		data = _tstof( sdata );
		return true;
	}

	return false;
}

bool
ConfigCoreView::remove( const TCHAR* expr, bool flush, SettingsLocation settingsLocation )
{
	bool result = true;

	switch(settingsLocation)
	{
	case INDETERMINATE:
		if ( !m_ProjectLocalModel.remove( expr, flush ) )
			if ( !m_ProjectModel.remove( expr, flush ) )
				if ( !m_GlobalLocalModel.remove( expr, flush ) )
					if ( !m_GlobalModel.remove( expr, flush ) )
					{
						result = false;
					}
		break;
	case GLOBAL:
		if ( !m_GlobalLocalModel.remove( expr, flush ) )
		{
			result = false;
		}
		break;
	case PROJECT:
		if ( !m_ProjectLocalModel.remove( expr, flush ) )
		{
			result = false;
		}
		break;
	}

	return ( result );
}

bool 
ConfigCoreView::set( const TCHAR* expr, const ConfigModelData& data, bool flush, SettingsLocation settingsLocation, bool add )
{
	bool result = true;

	switch(settingsLocation)
	{
	case INDETERMINATE:
		if ( !set_model_raw( expr, m_ProjectLocalModel, data, flush, add ) )
			if ( !set_model_raw( expr, m_ProjectModel, data, flush, add ) )
				if ( !set_model_raw( expr, m_GlobalLocalModel, data, flush, add ) )
					if ( !set_model_raw( expr, m_GlobalModel, data, flush, add ) )
					{
						result = false;
					}
		break;
	case GLOBAL:
		if ( !set_model_raw( expr, m_GlobalLocalModel, data, flush, add ) )
		{
			result = false;
		}
		break;
	case PROJECT:
		if ( !set_model_raw( expr, m_ProjectLocalModel, data, flush, add ) )
		{
			result = false;
		}
		break;
	}

	return ( result );
}

bool 
ConfigCoreView::set( const TCHAR* expr, const TCHAR* data, bool flush, SettingsLocation settingsLocation, bool add )
{
	ConfigModelData cdata( data );
	return ( set( expr, cdata, flush, settingsLocation, add ) );
}

bool 
ConfigCoreView::set( const TCHAR* expr, const bool data, bool flush, SettingsLocation settingsLocation, bool add )
{
	ConfigModelData cdata( data );
	return ( set( expr, cdata, flush, settingsLocation, add ) );
}

bool 
ConfigCoreView::set( const TCHAR* expr, const int data, bool flush, SettingsLocation settingsLocation, bool add )
{
	ConfigModelData cdata( data );
	return ( set( expr, cdata, flush, settingsLocation, add ) );
}

bool 
ConfigCoreView::set( const TCHAR* expr, const double data, bool flush, SettingsLocation settingsLocation, bool add )
{
	ConfigModelData cdata( data );
	return ( set( expr, cdata, flush, settingsLocation, add ) );
}

bool
ConfigCoreView::query_model_raw( const TCHAR* expr, const ConfigModel& model, ConfigModelData& data ) const
{
	return ( model.query( expr, data ) );
}

bool
ConfigCoreView::set_model_raw( const TCHAR* expr, ConfigModel& model, const ConfigModelData& data, const bool flush, bool add )
{
	return ( model.set( expr, data, flush, add ) );
}

bool
ConfigCoreView::Save()
{
	if(!m_GlobalLocalModel.save())
		return false;

	return m_ProjectLocalModel.save();
}


bool ConfigCoreView::GetStudio( TCHAR* studio, size_t size ) const
{
	_tcscpy_s(studio,size,m_sStudioName);
	return true;
}

bool ConfigCoreView::GetStudioNetworkDrive( TCHAR* networkdrive, size_t size ) const
{
	_tcscpy_s(networkdrive,size,m_sStudioNetworkDrive);
	return true;
}

bool ConfigCoreView::GetStudioTimeZoneOffset( int& timeZoneOffset ) const
{
	timeZoneOffset = m_sStudioServerTimeOffset;
	return true;
}

void ConfigCoreView::FetchStudio()
{
	configParser::ConfigModelData data;
	if(!query( _T("//config/studios/studio/@name"), data ))
		return;
	int numStudios = (int)data.ChildCount();

	// Get local host name
	char szHostName[128] = "";

	if(::gethostname(szHostName, sizeof(szHostName)))
		return;

	// Get local IP addresses
	struct sockaddr_in SocketAddress;
	struct hostent     *pHost        = 0;

	pHost = ::gethostbyname(szHostName);

	if(!pHost)
		return;

	char aszIPAddresses[10][16]; // maximum of ten IP addresses
	TCHAR queryStr[MAX_PATH];
	TCHAR studioIPRange[MAX_PATH];
	TCHAR studioIPMask[MAX_PATH];
	char ip[MAX_PATH];

	for(int iCnt = 0; ((pHost->h_addr_list[iCnt]) && (iCnt < 10)); ++iCnt)
	{
		memcpy(&SocketAddress.sin_addr, pHost->h_addr_list[iCnt], pHost->h_length);
		strcpy(aszIPAddresses[iCnt], inet_ntoa(SocketAddress.sin_addr));

		for(int i=1;i<=numStudios;i++)
		{
			_tcscpy(m_sStudioName,_T(""));
			_tcscpy(m_sStudioNetworkDrive,_T(""));
			m_sStudioServerTimeOffset = 0;
			_tcscpy(studioIPRange,_T(""));
			_tcscpy(studioIPMask,_T(""));

			_stprintf_s( queryStr, MAX_PATH, _T("//config/studios/studio[%d]/@subnet"), i );
			if(!query( queryStr, data ))
				continue;

			data.GetValue(studioIPRange,MAX_PATH);

			TCHAR* found = _tcschr(studioIPRange,'/');

			if(!found)
				continue;

			_tcscpy(studioIPMask,found+1);
			*found = 0;
			TCharToAnsi(ip,MAX_PATH,studioIPRange);

			_stprintf_s( queryStr, MAX_PATH, _T("//config/studios/studio[%d]/@name"), i );
			if(!query( queryStr, data ))
				continue;

			data.GetValue(m_sStudioName,MAX_PATH);

			_stprintf_s( queryStr, MAX_PATH, _T("//config/studios/studio[%d]/@networkdrive"), i );
			if(query( queryStr, data ))
			{
				data.GetValue(m_sStudioNetworkDrive,MAX_PATH);
			}

			_stprintf_s( queryStr, MAX_PATH, _T("//config/studios/studio[%d]/@timezoneoffset"), i );
			query( queryStr, m_sStudioServerTimeOffset );

			unsigned long bitsrange = _tstoi(studioIPMask);
			unsigned long mask = ((1 << (bitsrange + 1)) - 1) << (32 - bitsrange);

			unsigned long iprange = inet_addr(ip);
					
			if((htonl(iprange) & mask) == (htonl(SocketAddress.sin_addr.S_un.S_addr) & mask))
			{
				set( _T("/local/studio/@name"), m_sStudioName, false, ConfigCoreView::GLOBAL, true );
				set( _T("/local/studio/@networkdrive"), m_sStudioNetworkDrive, false, ConfigCoreView::GLOBAL, true );
				set( _T("/local/studio/@timezoneoffset"), m_sStudioServerTimeOffset, false, ConfigCoreView::GLOBAL, true );
				return;
			}
		}
	}
}

bool ConfigCoreView::GetConfigData(const TCHAR* config, const TCHAR* expr, TCHAR* data, size_t size) const
{
	ConfigModel dlcConfig;
	if(dlcConfig.load( config ))
	{
		ConfigModelData cdata;
		query_model_raw( expr, dlcConfig, cdata );

		if ( ConfigModelData::CONFIGMODEL_DATA_STRING == cdata.GetType() )
		{
			cdata.GetValue( data, size );
			m_pEnv->subst( data, size );
		}

		return ( ConfigModelData::CONFIGMODEL_DATA_STRING == cdata.GetType() );
	}

	return false;
}

END_CONFIGPARSER_NS
