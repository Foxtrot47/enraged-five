//
// filename:	configPortView.cpp
// author:		Michael Taschler <michael.taschler@rockstarnorth.com>
// date:		09 April 2014
// description:	
//
// Local headers
#pragma warning(disable: 4996)

#include "configParser/configPortView.h"
#include "configParser/configModelData.h"

// Other headers
#include "configUtil/Environment.h"
#include "configUtil/path.h"

#include "string/string.h"
#include "string/unicode.h"

// STL Headers Files
#include <algorithm>

using namespace configUtil;

#pragma warning( disable : 4668 )
// Boost Header Files
#pragma warning( push )
#pragma warning( disable : 4512 )
#include "boost/algorithm/string/replace.hpp"
#include "boost/algorithm/string/case_conv.hpp"
#pragma warning( pop )

// --- Defines ------------------------------------------------------------------
#define PORTS_FILENAME			( _T("ports.xml") )
#define PORTS_LOCAL_FILENAME	( _T("ports.local.xml") )

#define TEMP_BUFFER_SIZE	(1024)

// --- Implementation -----------------------------------------------------------

BEGIN_CONFIGPARSER_NS

ConfigPortView::ConfigPortView( )
{
	memset( m_sPortsConfig, 0, MAX_PATH * sizeof( TCHAR ) );
	memset( m_sPortsLocalConfig, 0, MAX_PATH * sizeof( TCHAR ) );

	m_pEnv = new Environment;
	m_pEnv->import_core_globals( );
	m_pEnv->import_secondary_globals( );

	TCHAR toolsconfig[MAX_PATH] = {0};
	m_pEnv->lookup( VAR_TOOLSCONFIG, toolsconfig, MAX_PATH );

	// $(toolsconfig)/ports.xml
	Path::combine( m_sPortsConfig, MAX_PATH, 2, toolsconfig, PORTS_FILENAME );
	// $(toolsconfig)/ports.local.xml
	Path::combine( m_sPortsLocalConfig, MAX_PATH, 2, toolsconfig, PORTS_LOCAL_FILENAME );

	if (m_PortModel.load( m_sPortsConfig ))
	{
		LoadFromModel(m_PortModel);
	}
	if (m_PortLocalModel.load( m_sPortsLocalConfig ))
	{
		LoadFromModel(m_PortLocalModel);
	}
}

ConfigPortView::~ConfigPortView( )
{
	delete m_pEnv;
}

bool 
ConfigPortView::GetPortForId( const TCHAR* key, int& port ) const
{
	STL_STRING strKey(key);
	boost::algorithm::to_lower(strKey);

	std::map<STL_STRING, int>::const_iterator itr = m_portMap.find(strKey);
	if(itr != m_portMap.end())
	{
		port = itr->second;
		return true;
	}

	return false;
}

void
ConfigPortView::LoadFromModel(const ConfigModel& model)
{
	size_t numPorts = GetNodeCountForQuery(model, _T("//Ports/Port"));

	for(size_t i=0; i < numPorts; ++i)
	{
		TCHAR key[MAX_PATH];
		int port = 0;

		if (GetPortId(model, i, key, MAX_PATH) &&
			GetPortValue(model, i, port))
		{
			STL_STRING strKey(key);
			boost::algorithm::to_lower(strKey);

			std::map<STL_STRING, int>::iterator it = m_portMap.find(strKey);
			if (it == m_portMap.end())
			{
				m_portMap.insert(std::pair<STL_STRING, int>(strKey, port));
			}
			else
			{
				it->second = port;
			}
		}
	}
}

size_t
ConfigPortView::GetNodeCountForQuery(const ConfigModel& model, const TCHAR* expr) const
{
	configParser::ConfigModelData data;
	bool result = model.query( expr, data );

	if (!result || (data.IsEmpty() && !data.HasChildren()))
	{
		return 0L;
	}
	else
	{
		return (data.HasChildren() ? data.ChildCount() : 1);
	}
}

bool 
ConfigPortView::Query( const ConfigModel& model, const TCHAR* expr, ConfigModelData& data ) const
{
	data = ConfigModelData( );
	model.query(expr, data);
	return ( !data.IsEmpty( ) || data.HasChildren( ) );
}

bool 
ConfigPortView::Query( const ConfigModel& model, const TCHAR* expr, TCHAR* data, size_t size ) const
{
	ConfigModelData cdata;
	model.query( expr, cdata );

	if ( ConfigModelData::CONFIGMODEL_DATA_STRING == cdata.GetType() )
	{
		cdata.GetValue( data, size );
		m_pEnv->subst( data, size );
	}

	return ( ConfigModelData::CONFIGMODEL_DATA_STRING == cdata.GetType() );
}


bool 
ConfigPortView::Query( const ConfigModel& model, const TCHAR* expr, int& data ) const
{
	ConfigModelData cdata;
	model.query( expr, cdata );

	if ( ConfigModelData::CONFIGMODEL_DATA_INT == cdata.GetType() )
	{
		cdata.GetValue( data );
		return true;
	}
	else if ( ConfigModelData::CONFIGMODEL_DATA_STRING == cdata.GetType() )
	{
		// Attempt to convert string data into int data.
		TCHAR sdata[TEMP_BUFFER_SIZE];
		cdata.GetValue( sdata, TEMP_BUFFER_SIZE );
		data = _tstoi( sdata );
		return true;
	}

	return false;
}


bool 
ConfigPortView::GetPortId( const ConfigModel& model, size_t index, TCHAR* name, size_t size ) const
{
	TCHAR query[_MAX_PATH] = {0};
	_stprintf_s( query, _MAX_PATH, _T("//Ports/Port[%d]/@id"), (index+1) );

	bool queryTypesResult = Query(model, query, name, size );
	return queryTypesResult;
}

bool 
ConfigPortView::GetPortValue( const ConfigModel& model, size_t index, int& port ) const
{
	TCHAR query[_MAX_PATH] = {0};
	_stprintf_s( query, _MAX_PATH, _T("//Ports/Port[%d]/Value"), (index+1) );

	bool queryTypesResult = Query(model, query, port );
	return queryTypesResult;
}

END_CONFIGPARSER_NS
