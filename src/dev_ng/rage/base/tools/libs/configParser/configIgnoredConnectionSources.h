#ifndef __CONFIGPARSER_CONFIGIGNOREDCONNECTIONSOURCES_H__
#define __CONFIGPARSER_CONFIGIGNOREDCONNECTIONSOURCES_H__

// --- Includes -----------------------------------------------------------------

// Local headers
#include "configCoreView.h"
#include "contentModel.h"
#include "configModelData.h"

#include <map>

// --- Defines ------------------------------------------------------------------
#define LG_SIZE (1024)

// --- Forward Declarations -----------------------------------------------------
namespace configUtil
{
	class Environment;
}

// --- Classes ------------------------------------------------------------------

BEGIN_CONFIGPARSER_NS

//PURPOSE
// This class understands the tools configuration data in ignoredconnectionsources.meta a little more than just
// loading them for XPath evaluation.
//
class ConfigIgnoredConnectionSources
{
public:
	ConfigIgnoredConnectionSources( );
	~ConfigIgnoredConnectionSources( );

	bool ShouldIgnoreAddress( const TCHAR* ip ) const;

private:
	// Hide copy constructor and assignment operator.
	ConfigIgnoredConnectionSources( const ConfigIgnoredConnectionSources& copy );
	ConfigIgnoredConnectionSources& operator=( ConfigIgnoredConnectionSources& copy );

	void LoadFromModel(const ConfigModel& data);

	bool Query( const ConfigModel& model, const TCHAR* expr, ConfigModelData& data ) const;
	bool Query( const ConfigModel& model, const TCHAR* expr, TCHAR* data, size_t size ) const;
	bool Query( const ConfigModel& model, const TCHAR* expr, int& data ) const;
	size_t GetNodeCountForQuery(const ConfigModel& model, const TCHAR* expr) const;

	bool GetConnectionName( const ConfigModel& model, size_t index, TCHAR* name, size_t size ) const;
	bool GetConenctionAddress( const ConfigModel& model, size_t index, TCHAR* address, size_t size ) const;

	TCHAR	m_IgnoredConnectionSourcesConfig[_MAX_PATH];
	
	configUtil::Environment* m_pEnv;		// Config environment object.
	ConfigModel	m_Model;

	std::map<STL_STRING, STL_STRING>	m_IpAddressMap;
};

END_CONFIGPARSER_NS

#endif // __CONFIGPARSER_CONFIGIGNOREDCONNECTIONSOURCES_H__
