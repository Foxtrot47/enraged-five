#ifndef __CONFIGPARSER_CONTENTNODEGROUP_H__
#define __CONFIGPARSER_CONTENTNODEGROUP_H__

//
// filename:	contentNodeGroup.h
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		11 February 2010
// description:	Content-tree node group class.
//

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

// --- Includes -----------------------------------------------------------------

// Local headers
#include "contentNode.h"

// STL headers
#include <string>
#include <vector>

// --- Classes ------------------------------------------------------------------

BEGIN_CONFIGPARSER_NS

//PURPOSE
// A ContentNodeGroup is a container for a set of child nodes; it typically
// represents a directory/folder on disk.
//
// The ContentNodeGroup owns all its children.
//
class ContentNodeGroup : public ContentNode
{
public:
	ContentNodeGroup( ContentNode* parent, 
		const configUtil::Environment* pEnv,
		const TCHAR* name, 
		const TCHAR* path = _T("") );
	ContentNodeGroup( ContentNode* parent, 
		const configUtil::Environment* pEnv, 
		xmlElementPtr pElement );
	virtual ~ContentNodeGroup( );

	/**
	 * @brief Get relative path string for current group.
	 */
	void 			GetRelativePath( TCHAR* path, size_t size ) const;
	
	/**
	 * @brief Get absolute path string for current group.
	 */
	void			GetAbsolutePath( TCHAR* path, size_t size ) const;

	/**
	 * @brief Get raw (no environment variable resolution) relative path.
	 */
	const TCHAR*	GetRawRelativePath( ) const { return m_sRelativePath; }
	
	/**
	 * @brief Get raw (no environment variable resolution) absolute path.
	 */
	void			GetRawAbsolutePath( TCHAR* path, size_t size ) const;

	//PURPOSE
	// Return xmlNodePtr representing this content node.
	virtual xmlNodePtr			ToXml() const;

	//PURPOSE
	// Save this group to a XML document.
	bool						ToXmlDocument( const TCHAR* filename ) const;

	// Child Container Iterators
	NodeContainerIter			BeginChildren( ) { return ( m_vChildren.begin() ); }
	NodeContainerIter			EndChildren( ) { return ( m_vChildren.end() ); }
	NodeContainerConstIter		BeginChildren( ) const { return ( m_vChildren.begin() ); }
	NodeContainerConstIter		EndChildren( ) const { return ( m_vChildren.end() ); } 

	size_t						GetNumChildren( ) const { return m_vChildren.size(); }
	ContentNode*				GetChild( int index ) const { return m_vChildren.at( index ); }
	void						AddChild( ContentNode* pNode ) { m_vChildren.push_back( pNode ); pNode->SetParent( this ); }
	NodeContainerIter			RemoveChild( NodeContainerIter nodeIter );
	
	// Find Methods
	ContentNode*				FindFirst( const STL_STRING& name, const STL_STRING& type = _T("") );
	ContentNode*				FindFirstGroup( const STL_STRING& name );
	size_t						FindAll( const STL_STRING& type, std::vector<ContentNode*>& nodes );
	size_t						FindAll( const STL_STRING& name, const STL_STRING& type, std::vector<ContentNode*>& nodes );

	virtual void				PostLoadCallback( );

	// Static Methods
	static const char*			GetXmlType( ) { return ( "group" ); }

protected:
	// Hide copy constructor and assignment operator.
	ContentNodeGroup( const ContentNodeGroup& copy );
	ContentNodeGroup& operator=( ContentNodeGroup& copy );

	TCHAR			m_sRelativePath[_MAX_PATH];	//!< Relative path string of group.
	NodeContainer	m_vChildren;		//!< Child nodes.
};

END_CONFIGPARSER_NS

#endif // __CONFIGPARSER_CONTENTNODEGROUP_H__
