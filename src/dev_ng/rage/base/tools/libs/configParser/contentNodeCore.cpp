//
// filename:	contentNodeCore.cpp
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		23 February 2010
// description:	Content-tree node core classes.
//

// LibXml2 headers
#include "libxml/parser.h"
#include "libxml/xpath.h"

// Local headers
#include "contentNodeCore.h"
#include "contentNodeGroup.h"

BEGIN_CONFIGPARSER_NS

// --- ContentNodeFile ------------------------------------------------------

ContentNodeFile::ContentNodeFile( ContentNode* parent,
								 const configUtil::Environment* pEnv,
								 const TCHAR* name, 
								 const TCHAR* ext, 
								 const TCHAR* type )
	: ContentNode( parent, pEnv, name, type )
{
	memset( m_sExtension, 0, sizeof( TCHAR ) * _MAX_PATH );
	memset( m_sRelativePath, 0, sizeof( TCHAR ) * _MAX_PATH );
#ifdef UNICODE
	wcsncpy_s( m_sExtension, _MAX_PATH, ext, _TRUNCATE );
#else
	strncpy_s( m_sExtension, _MAX_PATH, ext, _TRUNCATE );
#endif // UNICODE
}

ContentNodeFile::ContentNodeFile( ContentNode* parent,
								 const configUtil::Environment* pEnv, 
								 xmlElement* pElement )
	: ContentNode( parent, pEnv, pElement )
{
	memset( m_sExtension, 0, sizeof( TCHAR ) * _MAX_PATH );
	memset( m_sRelativePath, 0, sizeof( TCHAR ) * _MAX_PATH );

	xmlAttributePtr pAttribute = pElement->attributes;
	while ( pAttribute )
	{
		if ( 0 == xmlStrcasecmp( BAD_CAST "path", pAttribute->name ) )
		{
			configUtil::AnsiToTChar( m_sRelativePath, _MAX_PATH, 
				(char*)pAttribute->children->content );
		}
		else if ( 0 == xmlStrcasecmp( BAD_CAST "extension", pAttribute->name ) )
		{
			configUtil::AnsiToTChar( m_sExtension, _MAX_PATH,
				(char*)pAttribute->children->content );
		}

		// Next attribute.
		pAttribute = (xmlAttributePtr)( pAttribute->next );
	}
}

ContentNodeFile::~ContentNodeFile( )
{
}

void
ContentNodeFile::GetRelativePath( TCHAR* path, size_t size ) const
{
#ifdef UNICODE
	wcsncpy_s( path, size, m_sRelativePath, _TRUNCATE );
#else
	strcpy_s( path, size, m_sRelativePath );
#endif // UNICODE
	m_pEnv->subst( path, size );
}

void
ContentNodeFile::GetAbsolutePath( TCHAR* path, size_t size ) const
{	
	GetRawAbsolutePath( path, size );
	m_pEnv->subst( path, size );
}

void
ContentNodeFile::GetRawAbsolutePath( TCHAR* path, size_t size ) const
{
	memset( path, 0, size * sizeof( TCHAR ) );
	const ContentNodeGroup* pParent =
		dynamic_cast<const ContentNodeGroup*>( this->GetParent() );
	if ( pParent )
	{
		TCHAR parentPath[_MAX_PATH] = {0};
		pParent->GetRawAbsolutePath( parentPath, _MAX_PATH );

#ifdef UNICODE
		wcscat_s( path, size, parentPath );
#else
		strcat_s( path, size, parentPath );
#endif // UNICODE
	}
}

void
ContentNodeFile::GetFilename( TCHAR* filename, size_t size ) const
{
	GetRawFilename( filename, size );
	m_pEnv->subst( filename, size );
}

void
ContentNodeFile::GetRawFilename( TCHAR* filename, size_t size ) const
{
	memset( filename, 0, size * sizeof( TCHAR ) );
	size_t len = 0L;
	const ContentNodeGroup* pParent =
		dynamic_cast<const ContentNodeGroup*>( this->GetParent() );
	if ( pParent )
	{
		TCHAR parentPath[_MAX_PATH] = {0};
		pParent->GetRawAbsolutePath( parentPath, _MAX_PATH );

#ifdef UNICODE
		len = wcslen(parentPath);
		wcscat_s( filename, size, parentPath );
		wcscat_s( filename + len, size - len, _T("/"));
		++len;
#else
		len = strlen(parentPath);
		strcat_s( filename, size, parentPath );
		strcat_s( filename + len, size - len, _T("/"));
		++len;
#endif // UNICODE
	}
#ifdef UNICODE
	wcscat_s( filename + len, size - len, m_sName );
	len += wcslen( m_sName );	
	wcscat_s( filename + len, size - len, _T(".") );
	len += 1;
	wcscat_s( filename + len, size - len, m_sExtension );
#else
	strcat_s( filename + len, size - len, m_sName );
	len += strlen( m_sName );
	strcat_s( filename + len, size - len, _T(".") );
	len += 1;
	strcat_s( filename + len, size - len, m_sExtension );
#endif // UNICODE
}

void
ContentNodeFile::SetExtension( const TCHAR* ext )
{
#ifdef UNICODE
	wcsncpy_s( m_sExtension, _MAX_PATH, ext, _TRUNCATE );
#else
	strcpy_s( m_sExtension, _MAX_PATH, ext );
#endif // UNICODE
}

xmlNodePtr	
ContentNodeFile::ToXml( ) const
{
	xmlNodePtr pElement = ContentNode::ToXml( );
	char ansiRelativePath[_MAX_PATH] = {0};
	char ansiExtension[_MAX_PATH] = {0};
	configUtil::TCharToAnsi( ansiRelativePath, _MAX_PATH, m_sRelativePath );
	configUtil::TCharToAnsi( ansiExtension, _MAX_PATH, m_sExtension );
	size_t lenPath = strlen( ansiRelativePath );
	size_t lenExt = strlen( ansiExtension );

	if ( lenPath > 0 )
		xmlNewProp( pElement, BAD_CAST "path", BAD_CAST ansiRelativePath );
	if ( lenExt > 0 )
		xmlNewProp( pElement, BAD_CAST "extension", BAD_CAST ansiExtension );

	return ( pElement );
}

// --- ContentNodeDirectory -------------------------------------------------

ContentNodeDirectory::ContentNodeDirectory( ContentNode* parent, 
					 const configUtil::Environment* pEnv,
					 const TCHAR* name )
	: ContentNode( parent, pEnv, name, _T("directory") )
{
}

ContentNodeDirectory::ContentNodeDirectory( ContentNode* parent, 
				  const configUtil::Environment* pEnv, 
				  xmlElementPtr pElement )
	: ContentNode( parent, pEnv, pElement )
{
}

ContentNodeDirectory::~ContentNodeDirectory( )
{
}

void
ContentNodeDirectory::GetAbsolutePath( TCHAR* path, size_t size ) const
{	
	GetRawAbsolutePath( path, size );
	m_pEnv->subst( path, size );
}

void
ContentNodeDirectory::GetRawAbsolutePath( TCHAR* path, size_t size ) const
{
	memset( path, 0, size * sizeof( TCHAR ) );
	const ContentNodeGroup* pParent =
		dynamic_cast<const ContentNodeGroup*>( this->GetParent() );
	if ( pParent )
	{
		TCHAR parentPath[_MAX_PATH] = {0};
		pParent->GetRawAbsolutePath( parentPath, _MAX_PATH );
		_tcscat_s( path, size, parentPath );
	}
}

// --- ContentNodeTarget ----------------------------------------------------

ContentNodeTarget::ContentNodeTarget( ContentNode* parent,
									 const configUtil::Environment* pEnv,
									 const TCHAR* name, 
									 const TCHAR* /*path*/, 
									 const TCHAR* ext, 
									 Platform platform,
									 const TCHAR* type )
	: ContentNodeFile( parent, pEnv, name, ext, type )
	, m_Platform( platform )
{
}

ContentNodeTarget::ContentNodeTarget( ContentNode* parent,
									 const configUtil::Environment* pEnv, 
									 xmlElementPtr pElement )
	: ContentNodeFile( parent, pEnv, pElement )
{
}
	
ContentNodeTarget::~ContentNodeTarget( )
{
}

// --- ContentNodeRpf -------------------------------------------------------

ContentNodeRpf::ContentNodeRpf( ContentNode* parent,
								const configUtil::Environment* pEnv,
								const TCHAR* name, 
								const TCHAR* path,
								const TCHAR* ext, 
								Platform platform, 
								bool stack_path,
								bool compress,
								const TCHAR* type )
	: ContentNodeTarget( parent, pEnv, name, path, ext, platform, type )
	, m_bStackPath( stack_path )
	, m_bCompress( compress )
{
#ifdef UNICODE
	size_t len = wcslen( m_sExtension );
	if ( 0 == len )
		wcsncpy_s( m_sExtension, _MAX_PATH, _T("rpf"), _TRUNCATE );
#else
	size_t len = strlen( m_sExtension );
	if ( 0 == len ) 
		strncpy_s( m_sExtension, _MAX_PATH, _T("rpf"), _TRUNCATE );
#endif // UNICODE
}

ContentNodeRpf::ContentNodeRpf( ContentNode* parent,
							   const configUtil::Environment* pEnv, 
							   xmlElementPtr pElement )
	: ContentNodeTarget( parent, pEnv, pElement )
{	
	xmlAttributePtr pAttribute = pElement->attributes;
	while ( pAttribute )
	{
		if ( 0 == xmlStrcasecmp( BAD_CAST "stackpath", pAttribute->name ) )
		{
			m_bStackPath = ( 0 == xmlStrcasecmp( BAD_CAST "true", BAD_CAST pAttribute->children->content ) );
		}
		else if ( 0 == xmlStrcasecmp( BAD_CAST "compress", pAttribute->name ) )
		{
			m_bCompress = ( 0 == xmlStrcasecmp( BAD_CAST "true", BAD_CAST pAttribute->children->content ) );
		}

		// Next attribute.
		pAttribute = (xmlAttributePtr)( pAttribute->next );
	}
#ifdef UNICODE
	size_t len = wcslen( m_sExtension );
	if ( 0 == len )
		wcsncpy_s( m_sExtension, _MAX_PATH, _T("rpf"), _TRUNCATE );
#else
	size_t len = strlen( m_sExtension );
	if ( 0 == len ) 
		strncpy_s( m_sExtension, _MAX_PATH, _T("rpf"), _TRUNCATE );
#endif // UNICODE
}

ContentNodeRpf::~ContentNodeRpf( )
{
}

xmlNodePtr	
ContentNodeRpf::ToXml() const
{
	xmlNodePtr pElement = ContentNodeTarget::ToXml();
	xmlNewProp( pElement, BAD_CAST "stackpath", m_bStackPath ? BAD_CAST "true" : BAD_CAST "false" );
	xmlNewProp( pElement, BAD_CAST "compress", m_bCompress ? BAD_CAST "true" : BAD_CAST "false" );

	return ( pElement );
}


// --- ContentNodeZip -------------------------------------------------------

ContentNodeZip::ContentNodeZip( ContentNode* parent,
							   const configUtil::Environment* pEnv,
							   const TCHAR* name, 
							   const TCHAR* path,
							   const TCHAR* ext, 
							   Platform platform, 
							   bool compress,
							   const TCHAR* type )
	: ContentNodeTarget( parent, pEnv, name, path, ext, platform, type )
	, m_bCompress( compress )
{
#ifdef UNICODE
	size_t len = wcslen( m_sExtension );
	if ( 0 == len )
		wcsncpy_s( m_sExtension, _MAX_PATH, _T("zip"), _TRUNCATE );
#else
	size_t len = strlen( m_sExtension );
	if ( 0 == len ) 
		strncpy_s( m_sExtension, _MAX_PATH, _T("zip"), _TRUNCATE );
#endif // UNICODE
}

ContentNodeZip::ContentNodeZip( ContentNode* parent,
							   const configUtil::Environment* pEnv, 
							   xmlElementPtr pElement )
	: ContentNodeTarget( parent, pEnv, pElement )
{	
	xmlAttributePtr pAttribute = pElement->attributes;
	while ( pAttribute )
	{
		if ( 0 == xmlStrcasecmp( BAD_CAST "compress", pAttribute->name ) )
		{
			m_bCompress = ( 0 == xmlStrcasecmp( BAD_CAST "true", BAD_CAST pAttribute->children->content ) );
		}

		// Next attribute.
		pAttribute = (xmlAttributePtr)( pAttribute->next );
	}
#ifdef UNICODE
	size_t len = wcslen( m_sExtension );
	if ( 0 == len )
		wcsncpy_s( m_sExtension, _MAX_PATH, _T("zip"), _TRUNCATE );
#else
	size_t len = strlen( m_sExtension );
	if ( 0 == len ) 
		strncpy_s( m_sExtension, _MAX_PATH, _T("zip"), _TRUNCATE );
#endif // UNICODE
}

ContentNodeZip::~ContentNodeZip( )
{
}

xmlNodePtr	
ContentNodeZip::ToXml() const
{
	xmlNodePtr pElement = ContentNodeTarget::ToXml();
	xmlNewProp( pElement, BAD_CAST "compress", m_bCompress ? BAD_CAST "true" : BAD_CAST "false" );

	return ( pElement );
}

END_CONFIGPARSER_NS
