#ifndef __CONFIGPARSER__FORCEINCLUDE__H__
#define __CONFIGPARSER__FORCEINCLUDE__H__

//
// filename:	forceinclude.h
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		18 April 2008
// description:	Generic environment class interface
//

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

#include <winsock2.h>

#undef GetUserName

// --- Includes -----------------------------------------------------------------

#if defined(_DEBUG) && defined(_M_X64)
#include "forceinclude/win64_tooldebug.h"
#elif defined(_DEBUG)
#include "forceinclude/win32_tooldebug.h"
#elif defined(NDEBUG) && defined(_M_X64)
#include "forceinclude/win64_toolrelease.h"
#elif defined(NDEBUG)
#include "forceinclude/win32_toolrelease.h"
#elif defined(_M_X64)
#include "forceinclude/win64_toolbeta.h"
#else
#include "forceinclude/win32_toolbeta.h"
#endif

#include <string>

// --- Defines ------------------------------------------------------------------
#ifdef  UNICODE
typedef wchar_t TCHAR;
typedef std::wstring STL_STRING;
#else
typedef char TCHAR;
typedef std::string STL_STRING;
#endif // UNICODE

#endif // __CONFIGPARSER__FORCEINCLUDE__H__
