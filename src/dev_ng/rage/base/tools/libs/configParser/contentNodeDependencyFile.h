#ifndef __CONFIGPARSER_CONTENTNODEDEPENDENCYFILE_H__
#define __CONFIGPARSER_CONTENTNODEDEPENDENCYFILE_H__

//
// filename:	contentNodeDependencyFile.h
// author:		Jonny Rivers <jonathan.rivers@rockstarnorth.com>
// date:		29 November 2011
// description:	Content-tree node dependency file class.
//

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

// --- Includes -----------------------------------------------------------------

// Local headers
#include "contentNode.h"

// --- Classes ------------------------------------------------------------------

BEGIN_CONFIGPARSER_NS

//PURPOSE
//
class ContentNodeDependencyFile : public ContentNode
{
public:
	ContentNodeDependencyFile( ContentNode* parent, 
		const configUtil::Environment* pEnv,
		const TCHAR* name,
		const TCHAR* pathname,
		const TCHAR* type = _T("dependency_file") );
	ContentNodeDependencyFile( ContentNode* parent, 
		const configUtil::Environment* pEnv, 
		xmlElementPtr pElement );
	virtual ~ContentNodeDependencyFile( );
 
	void GetPathname( TCHAR* pathname, size_t size ) const;
	bool IsOptional() const;

	//PURPOSE
	// Return xmlElementPtr representing this content node.
	virtual xmlNodePtr			ToXml() const;

	// Static Methods
	static const char*			GetXmlType( ) { return ( "dependency_file" ); }

protected:
	// Hide copy constructor and assignment operator.
	ContentNodeDependencyFile( const ContentNodeDependencyFile& copy );
	ContentNodeDependencyFile& operator=( ContentNodeDependencyFile& copy );

	TCHAR m_sPathname[_MAX_PATH];
	bool m_bOptional;
};

END_CONFIGPARSER_NS

#endif // __CONFIGPARSER_CONTENTNODEDEPENDENCYFILE_H__
