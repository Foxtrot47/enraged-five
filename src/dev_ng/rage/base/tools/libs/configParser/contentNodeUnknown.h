#ifndef __CONFIGPARSER_CONTENTNODEUNKNOWN_H__
#define __CONFIGPARSER_CONTENTNODEUNKNOWN_H__

//
// filename:	contentNodeUnknown.h
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		22 February 2010
// description:	Content-tree unknown node class.
//

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

// --- Includes -----------------------------------------------------------------

// Local headers
#include "contentNodeGroup.h"

// --- Classes ------------------------------------------------------------------

BEGIN_CONFIGPARSER_NS

//PURPOSE
//
class ContentNodeUnknown : public ContentNodeGroup
{
public:
	ContentNodeUnknown( ContentNode* parent, 
		const configUtil::Environment* pEnv,
		const TCHAR* name, 
		const TCHAR* type );
	ContentNodeUnknown( ContentNode* parent, 
		const configUtil::Environment* pEnv, 
		xmlElementPtr pElement );
	virtual ~ContentNodeUnknown( );

protected:
	// Hide copy constructor and assignment operator.
	ContentNodeUnknown( const ContentNodeUnknown& copy );
	ContentNodeUnknown& operator=( ContentNodeUnknown& copy );
};

END_CONFIGPARSER_NS

#endif // __CONFIGPARSER_CONTENTNODEUNKNOWN_H__
