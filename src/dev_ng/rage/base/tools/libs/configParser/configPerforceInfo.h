#ifndef __CONFIGPARSER_CONFIGPERFORCEINFO_H__
#define __CONFIGPARSER_CONFIGPERFORCEINFO_H__

//
// filename:	configPerforceInfo.h
// author:		Greg Smith <greg@rockstarnorth.com>
// date:		7 March 2011
// description:	Perforce connection information. There can be a number of these for a user though we tend to just have one now
//

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

// --- Includes -----------------------------------------------------------------

// Local headers
#include "configParser.h"

// STL headers
#include <string>

// --- Classes ------------------------------------------------------------------

BEGIN_CONFIGPARSER_NS

class PerforceInfo
{
public:
	PerforceInfo() {}

	const TCHAR* GetName( ) const { return m_sName; }
	const TCHAR* GetWorkspace( ) const { return m_sWorkspace; }
	const TCHAR* GetUsername( ) const { return m_sUsername; }
	const TCHAR* GetServer( ) const { return m_sServer; }

	void SetName( const TCHAR* value ) { _tcscpy_s(m_sName,_MAX_PATH,value); }
	void SetWorkspace( const TCHAR* value ) { _tcscpy_s(m_sWorkspace,_MAX_PATH,value); }
	void SetUsername( const TCHAR* value ) { _tcscpy_s(m_sUsername,_MAX_PATH,value); }
	void SetServer( const TCHAR* value ) { _tcscpy_s(m_sServer,_MAX_PATH,value); }

private:
	// Hide copy constructor and assignment operator.
	PerforceInfo( const PerforceInfo& copy );
	PerforceInfo& operator=( PerforceInfo& copy );

	TCHAR			m_sName[_MAX_PATH];
	TCHAR			m_sWorkspace[_MAX_PATH];
	TCHAR			m_sUsername[_MAX_PATH];
	TCHAR			m_sServer[_MAX_PATH];
};

END_CONFIGPARSER_NS

#endif //__CONFIGPARSER_CONFIGPERFORCEINFO_H__