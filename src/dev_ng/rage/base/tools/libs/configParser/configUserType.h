#ifndef __CONFIGPARSER_CONFIGUSERTYPES_H__
#define __CONFIGPARSER_CONFIGUSERTYPES_H__

//
// filename:	configCoreView.h
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		18 December 2009
// description:	Base view of configuration data, including environment substitution.
//

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

// --- Includes -----------------------------------------------------------------

// Local headers
#include "configUtil\configutil.h"
#include "configParser.h"
#include "configModel.h"

//system headers
#include <tchar.h>

// STL headers
#include <string>

// --- Forward Declarations -----------------------------------------------------
namespace configUtil
{
	class Environment;
}

// --- Classes ------------------------------------------------------------------

BEGIN_CONFIGPARSER_NS

//PURPOSE
// The UserType class.
//
// This class represents a user role as defined in our configuration data;
// the user role has a keyed string, friendly string and bit mask.
//
class UserType
{
public:
	UserType() {}

	const TCHAR* GetName( ) const { return m_sName; }
	const TCHAR* GetFriendlyName( ) const { return m_sFriendlyName; }
	const unsigned int GetMask( ) const { return m_Mask; }
	const bool GetIsPseudo( ) const { return m_bPseudo; }

	void SetName( const TCHAR* value ) { _tcscpy_s(m_sName,_MAX_PATH,value); }
	void SetFriendlyName( const TCHAR* value ) { _tcscpy_s(m_sFriendlyName,_MAX_PATH,value); }
	void SetMask( unsigned int value) { m_Mask = value; }
	void SetIsPseudo( bool value ) { m_bPseudo = value; }

private:
	// Hide copy constructor and assignment operator.
	UserType( const UserType& copy );
	UserType& operator=( UserType& copy );

	TCHAR			m_sName[_MAX_PATH];
	TCHAR			m_sFriendlyName[_MAX_PATH];
	unsigned int	m_Mask;		//!< bitmask user
	bool			m_bPseudo;	//!< true iff not a selectable user-type.
};

END_CONFIGPARSER_NS

#endif // __CONFIGPARSER_CONFIGUSERTYPES_H__
