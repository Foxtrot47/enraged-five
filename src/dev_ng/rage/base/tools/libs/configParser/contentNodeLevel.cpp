//
// filename:	contentNodeGroup.cpp
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		11 February 2010
// description:	Content-tree group node class.
//

// LibXml2 headers
#include "libxml/parser.h"
#include "libxml/xpath.h"

// Local headers
#include "configParser/contentNodeLevel.h"

BEGIN_CONFIGPARSER_NS

ContentNodeLevel::ContentNodeLevel( ContentNode* parent,
								   const configUtil::Environment* pEnv,
								   const TCHAR* name, 
								   const TCHAR* path,
								   const TCHAR* image )
	: ContentNodeGroup( parent, pEnv, name, path )
{
#ifdef UNICODE
	wcsncpy_s( m_sType, SM_SIZE, _T("level"), _TRUNCATE );
	wcsncpy_s( m_sImageFilename, _MAX_PATH, image, _TRUNCATE );
#else
	strncpy_s( m_sType, SM_SIZE, _T("level"), _TRUNCATE );
	strncpy_s( m_sImageFilename, _MAX_PATH, image, _TRUNCATE );
#endif // UNICODE
}

ContentNodeLevel::ContentNodeLevel( ContentNode* parent,
								   const configUtil::Environment* pEnv, 
								   xmlElement* pElement )
	: ContentNodeGroup( parent, pEnv, pElement )
{
	xmlAttributePtr pAttribute = pElement->attributes;
	while ( pAttribute )
	{
		if ( 0 == xmlStrcasecmp( BAD_CAST "image", pAttribute->name ) )
		{
			configUtil::AnsiToTChar( m_sImageFilename, _MAX_PATH,
				(char*)pAttribute->children->content );
		}

		// Next attribute.
		pAttribute = (xmlAttributePtr)( pAttribute->next );
	}
}

ContentNodeLevel::~ContentNodeLevel( )
{	
}

void
ContentNodeLevel::GetImageFilename( TCHAR* path, size_t size ) const
{
#ifdef UNICODE
	wcsncpy_s( path, _MAX_PATH, m_sImageFilename, _TRUNCATE );
#else
	strncpy_s( path, _MAX_PATH, m_sImageFilename, _TRUNCATE );
#endif // UNICODE
	m_pEnv->subst( path, size );
}

END_CONFIGPARSER_NS
