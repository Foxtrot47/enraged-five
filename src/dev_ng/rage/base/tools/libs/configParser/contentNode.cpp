//
// filename:	contentNode.cpp
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		11 February 2010
// description:	Content-tree node base class.
//

// LibXML2 headers
#include "libxml/parser.h"

// Local headers
#include "configParser/contentNode.h"

BEGIN_CONFIGPARSER_NS
	
ContentNode::ContentNode( ContentNode* parent, 
						 const configUtil::Environment* pEnv,
						 const TCHAR* name, 
						 const TCHAR* type )
	: m_pParent( parent )
	, m_pEnv( pEnv )
	, m_nSourceLine( -1 )
{
#ifdef UNICODE
	wcsncpy_s( m_sName, SM_SIZE, name, _TRUNCATE );
	wcsncpy_s( m_sType, SM_SIZE, type, _TRUNCATE );
	wcsncpy_s( m_sGroup, SM_SIZE, _T(""), _TRUNCATE );
	wcsncpy_s( m_sSourceDoc, _MAX_PATH, _T("none"), _TRUNCATE );
#else
	strncpy_s( m_sName, SM_SIZE, name, _TRUNCATE );
	strncpy_s( m_sType, SM_SIZE, type, _TRUNCATE );
	strncpy_s( m_sGroup, SM_SIZE, _T(""), _TRUNCATE );
	strncpy_s( m_sSourceDoc, _MAX_PATH, _T("none"), _TRUNCATE );
#endif // UNICODE
}

ContentNode::ContentNode( ContentNode* parent, 
						 const configUtil::Environment* pEnv, 
						 xmlElement* pElement )
	: m_pParent( parent )
	, m_pEnv( pEnv )
	, m_nSourceLine( reinterpret_cast<xmlNodePtr>(pElement)->line )
{
	memset( m_sName, 0, SM_SIZE * sizeof( TCHAR ) );
	memset( m_sType, 0, SM_SIZE * sizeof( TCHAR ) );
	memset( m_sGroup, 0, SM_SIZE * sizeof( TCHAR ) );
#ifdef UNICODE
	char* pSourceDoc = (char*)pElement->doc->URL;
	configUtil::AnsiToTChar( m_sSourceDoc, _MAX_PATH, pSourceDoc );
#else
	strncpy_s( m_sSourceDoc, SM_SIZE, (char*)pElement->doc->URL, _TRUNCATE );
#endif // UNICODE

	xmlAttributePtr pAttribute = pElement->attributes;
	while ( pAttribute )
	{
		if ( 0 == xmlStrcasecmp( BAD_CAST "name", pAttribute->name ) )
		{
			configUtil::AnsiToTChar( m_sName, SM_SIZE, 
				(char*)pAttribute->children->content );
		}
		else if ( 0 == xmlStrcasecmp( BAD_CAST "type", pAttribute->name ) )
		{
			configUtil::AnsiToTChar( m_sType, SM_SIZE, 
				(char*)pAttribute->children->content );
		}
		// Next attribute.
		pAttribute = (xmlAttributePtr)( pAttribute->next );
	}
}

ContentNode::~ContentNode( )
{
	m_pParent = NULL;

#ifdef UNICODE
	wcsncpy_s( m_sName, SM_SIZE, _T(""), _TRUNCATE );
	wcsncpy_s( m_sType, SM_SIZE, _T(""), _TRUNCATE );
	wcsncpy_s( m_sSourceDoc, _MAX_PATH, _T("none"), _TRUNCATE );
#else
	strncpy_s( m_sName, SM_SIZE, _T(""), _TRUNCATE );
	strncpy_s( m_sType, SM_SIZE, _T(""), _TRUNCATE );
	strncpy_s( m_sSourceDoc, _MAX_PATH, _T("none"), _TRUNCATE );
#endif // UNICODE
}

void
ContentNode::SetGroup( const TCHAR* group )
{
#ifdef UNICODE
	wcsncpy_s( m_sGroup, SM_SIZE, group, _TRUNCATE );
#else
	strncpy_s( m_sGroup, SM_SIZE, group, _TRUNCATE );
#endif // UNICODE
}

xmlNodePtr
ContentNode::ToXml( ) const
{
	char ansiName[SM_SIZE] = {0};
	char ansiType[SM_SIZE] = {0};
	char ansiGroup[SM_SIZE] = {0};
	configUtil::TCharToAnsi( ansiName, SM_SIZE, m_sName );
	configUtil::TCharToAnsi( ansiType, SM_SIZE, m_sType );
	configUtil::TCharToAnsi( ansiGroup, SM_SIZE, m_sGroup );

	xmlNodePtr pNode = xmlNewNode( NULL, BAD_CAST "content" );
	xmlNewProp( pNode, BAD_CAST "type", BAD_CAST ansiType );
	if ( strlen( ansiName ) > 0 )
		xmlNewProp( pNode, BAD_CAST "name", BAD_CAST ansiName );

	for ( NodeContainerConstIter it = BeginInputs();
		  it != EndInputs();
		  ++it )
	{
		ContentNode* pInputContentNode = (*it);
		char ansiInputName[SM_SIZE] = {0};
		char ansiInputType[SM_SIZE] = {0};
		configUtil::TCharToAnsi( ansiName, SM_SIZE, pInputContentNode->GetName() );
		configUtil::TCharToAnsi( ansiType, SM_SIZE, pInputContentNode->GetType() );

		xmlNodePtr pInput = xmlNewNode( NULL, BAD_CAST "in" );
		if ( strlen( ansiGroup ) )
			xmlNewProp( pInput, BAD_CAST "group", BAD_CAST ansiGroup );
		xmlNewProp( pInput, BAD_CAST "type", BAD_CAST ansiInputType );
		xmlNewProp( pInput, BAD_CAST "name", BAD_CAST ansiInputName );
		
		xmlAddChild( pNode, pInput );
	}

	return ( pNode );
}

END_CONFIGPARSER_NS
