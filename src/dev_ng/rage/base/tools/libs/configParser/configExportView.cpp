//
// filename:	ConfigExportView.cpp
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		18 December 2009
// description:	Base view of configuration data, including environment substitution.
//

// Local headers
#include "configParser/configExportView.h"
#include "configParser/configModelData.h"

// Other headers
#include "configUtil/Environment.h"
#include "configUtil/path.h"

// LibXml2 headers
#include "libxml/xpath.h"

using namespace configUtil;

// --- Defines ------------------------------------------------------------------
#define EXPORT_FILENAME		( _T("export.xml") )
#ifndef MAX_PATH
#define MAX_PATH			(260)
#endif //MAX_PATH
#define TEMP_BUFFER_SIZE	(1024)

// --- Implementation -----------------------------------------------------------

BEGIN_CONFIGPARSER_NS

ConfigExportView::ConfigExportView( )
	: m_pEnv( NULL )
{
	memset( m_sExportConfig, 0, MAX_PATH * sizeof( TCHAR ) );

	//NOTE:  The ConfigGameView object adds to its environment a large list of useful environment variables
	//that can be used for substitutions in the ConfigExportView XML, such as $(art) and $(root).  
	//Let the ConfigGameView do the legwork and we simply adopt the ConfigGameView's environment.
	//
	m_pEnv = m_ConfigGameView.GetEnvironment();

	TCHAR toolsconfig[MAX_PATH] = {0};
	m_pEnv->lookup( VAR_TOOLSCONFIG, toolsconfig, MAX_PATH );
	
	// $(toolsconfig)/export.xml
	Path::combine( m_sExportConfig, MAX_PATH, 2L, toolsconfig, EXPORT_FILENAME );

	m_ExportModel.load( m_sExportConfig );
}

ConfigExportView::~ConfigExportView( )
{
}
bool 
ConfigExportView::GetAnimCompressedExportPath( TCHAR* path, size_t size ) const
{
	return query( _T("//export/motionbuilder/animation/@compressedExportPath"), path, size);
}

bool 
ConfigExportView::GetAnimExportPath ( TCHAR* path, size_t size ) const
{
	return query( _T("//export/motionbuilder/animation/@exportPath"), path, size);
}

bool 
ConfigExportView::GetControlFilePath( TCHAR* path, size_t size) const
{
	return query( _T("//export/motionbuilder/animation/@animCtrlPath"), path, size);
}

bool 
ConfigExportView::GetDefaultControlFile( TCHAR* path, size_t size ) const
{
	return query( _T("//export/motionbuilder/animation/@defaultControlFile"), path, size);
}

bool 
ConfigExportView::GetSkeletonFilePath( TCHAR* path, size_t size ) const
{
	return query( _T("//export/motionbuilder/animation/@skeletonFilePath"), path, size);
}

bool 
ConfigExportView::GetDefaultSkeletonFile( TCHAR* path, size_t size ) const
{
	return query( _T("//export/motionbuilder/animation/@defaultSkeletonFile"), path, size);
}


bool 
ConfigExportView::GetCompressionFilePath( TCHAR* path, size_t size ) const
{
	return query( _T("//export/motionbuilder/animation/@compressionPath"), path, size);
}

bool 
ConfigExportView::GetDefaultCompressionFile( TCHAR* path, size_t size ) const
{
	return query( _T("//export/motionbuilder/animation/@defaultCompressionFile"), path, size);
}

bool 
ConfigExportView::GetBoneTagsFile( TCHAR* path, size_t size ) const
{
	return query( _T("//export/motionbuilder/animation/@boneTagsFile"), path, size);
}

bool
ConfigExportView::GetEventDefsPath( TCHAR* path, size_t size) const
{
	return query( _T("//export/motionbuilder/cutscene/@eventDefs"), path, size);
}

bool 
ConfigExportView::GetEffectsCutsceneSectionName( TCHAR* path, size_t size ) const
{
	return query( _T("//export/motionbuilder/effects/@cutsceneIniSection"), path, size);
}

bool 
ConfigExportView::GetRmptfxPath( TCHAR* path, size_t size, bool subst) const
{
	return query( _T("//export/motionbuilder/effects/@rmptfxPath"), path, size, subst);
}

bool 
	ConfigExportView::GetAnimationPostRenderScript( TCHAR* path, size_t size) const
{
	return query( _T("//export/motionbuilder/animation/@renderScriptFile"), path, size);
}

bool 
ConfigExportView::GetDrawableCountLimit(int& limit) const
{
	return query( _T("//export/mapreport/@drawableCountLimit"), limit);
}

bool 
ConfigExportView::GetTxdCountLimit(int& limit) const
{
	return query( _T("//export/mapreport/@txdCountLimit"), limit);
}

bool 
ConfigExportView::GetBoundDrawableDistanceRatio(double& ratio) const
{
	return query( _T("//export/mapreport/@boundDrawableDistanceRatio"), ratio);
}

bool ConfigExportView::GetMaxAutoSubmitsEnabled( bool& enabled ) const
{
	return query( _T("//export/max/integration/@autoSubmit"), enabled);
}

bool
ConfigExportView::GetExportConfigFilename( TCHAR* path, size_t size ) const
{
#ifdef UNICODE
	wcsncpy_s( path, size, m_sExportConfig, _TRUNCATE );
#else
	strncpy_s( path, size, m_sExportConfig, _TRUNCATE );
#endif // UNICODE
	return ( true );
}

bool 
ConfigExportView::query( const TCHAR* expr, ConfigModelData& data ) const
{
	data = ConfigModelData( );
	// Try project configuration data in our varying levels of configuration
	// data.  Project --> Global --> Network is the fall-through pattern.
	if ( !query_model_raw( expr, m_ExportModel, data ) )
	{
		data = ConfigModelData( );
	}
	return ( !data.IsEmpty( ) || data.HasChildren( ) );
}

bool 
ConfigExportView::query( const TCHAR* expr, TCHAR* data, size_t size, bool subst ) const
{
	ConfigModelData cdata;
	query( expr, cdata );
	
	if ( ConfigModelData::CONFIGMODEL_DATA_STRING == cdata.GetType() )
	{
		cdata.GetValue( data, size );
		if (subst)
		{
			m_pEnv->subst( data, size );
		}
	}

	return ( ConfigModelData::CONFIGMODEL_DATA_STRING == cdata.GetType() );
}

bool 
ConfigExportView::query( const TCHAR* expr, bool& data ) const
{
	ConfigModelData cdata;
	query( expr, cdata );

	if ( ConfigModelData::CONFIGMODEL_DATA_BOOL == cdata.GetType() )
	{
		cdata.GetValue( data );
		return true;
	}
	else if ( ConfigModelData::CONFIGMODEL_DATA_STRING == cdata.GetType() )
	{
		// Attempt to convert string data into bool data.
		TCHAR sdata[TEMP_BUFFER_SIZE];
		cdata.GetValue( sdata, TEMP_BUFFER_SIZE );

		if(_tcscmp(sdata,_T("true")) == 0)
			data = true;
		else
			data = false;

		return true;
	}
	
	return false;
}

bool 
ConfigExportView::query( const TCHAR* expr, int& data ) const
{
	ConfigModelData cdata;
	query( expr, cdata );

	if ( ConfigModelData::CONFIGMODEL_DATA_INT == cdata.GetType() )
	{
		cdata.GetValue( data );
	}
	else if ( ConfigModelData::CONFIGMODEL_DATA_STRING == cdata.GetType() )
	{
		// Attempt to convert string data into int data.
		TCHAR sdata[TEMP_BUFFER_SIZE];
		cdata.GetValue( sdata, TEMP_BUFFER_SIZE );
#ifdef UNICODE
		data = _wtoi( sdata );
#else
		data = atoi( sdata );
#endif // UNICODE
		return ( true );
	}
	
	return ( ConfigModelData::CONFIGMODEL_DATA_INT == cdata.GetType() );
}

bool 
ConfigExportView::query( const TCHAR* expr, double& data ) const
{
	ConfigModelData cdata;
	query( expr, cdata );

	if ( ConfigModelData::CONFIGMODEL_DATA_FLOAT == cdata.GetType() )
	{
		cdata.GetValue( data );
	}
	else if ( ConfigModelData::CONFIGMODEL_DATA_STRING == cdata.GetType() )
	{
		// Attempt to convert string data into float data.
		TCHAR sdata[TEMP_BUFFER_SIZE];
		cdata.GetValue( sdata, TEMP_BUFFER_SIZE );
#ifdef UNICODE
		data = _wtof( sdata );
#else
		data = atof( sdata );
#endif // UNICODE
		return ( true );
	}

	return ( ConfigModelData::CONFIGMODEL_DATA_FLOAT == cdata.GetType() );
}

bool
ConfigExportView::query_model_raw( const TCHAR* expr, const ConfigModel& model, ConfigModelData& data ) const
{
	bool result = false;
	try
	{
		result = model.query( expr, data );
		//result = true;
	}
	catch ( std::runtime_error& )
	{
		result = false;
	}
	return ( result );
}

bool
ConfigExportView::set_model_raw( const TCHAR* expr, ConfigModel& model, const ConfigModelData& data, const bool flush )
{
	bool result = false;
	try
	{
		result = model.set( expr, data, flush );
	}
	catch ( std::runtime_error& )
	{
		result = false;
	}
	return ( result );
}

END_CONFIGPARSER_NS
