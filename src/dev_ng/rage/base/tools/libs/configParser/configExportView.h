#ifndef __CONFIGPARSER_CONFIGEXPORTVIEW_H__
#define __CONFIGPARSER_CONFIGEXPORTVIEW_H__

//
// filename:	configExportView.h
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		18 December 2009
// description:	Base view of configuration data, including environment substitution.
//

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

// --- Includes -----------------------------------------------------------------

// Local headers
#include "configParser.h"
#include "configModel.h"
#include "configGameView.h"

// STL headers

// --- Forward Declarations -----------------------------------------------------
namespace configUtil
{
	class Environment;
}

// --- Classes ------------------------------------------------------------------

BEGIN_CONFIGPARSER_NS

//PURPOSE
// The root configuration data view.
//
// It is not intended that this class be extended through inheritance but should
// be used by all other views.
//
// Overloaded set methods are provided to allow write access to the configuration
// data.  If you only require read access its recommended you create a constant
// instance of a ConfigCoreView object.
//
//   const ConfigExportView configView; // read-only access
//   ConfigExportView configViewRW;     // read/write access
//
// This also applies to higher-level view objects.  If you attempt a call to a
// set method with a const object you will get compile errors.
//
class ConfigExportView
{
public:
	ConfigExportView( );
	~ConfigExportView( );

	// Config XML filename functions.
	bool GetExportConfigFilename( TCHAR* path, size_t size ) const;

	// Animation
	bool GetAnimCompressedExportPath( TCHAR* path, size_t size ) const;
	bool GetAnimExportPath ( TCHAR* path, size_t size ) const;

	bool GetControlFilePath( TCHAR* path, size_t size ) const;
	bool GetDefaultControlFile( TCHAR* path, size_t size ) const;

	bool GetSkeletonFilePath( TCHAR* path, size_t size ) const;
	bool GetDefaultSkeletonFile( TCHAR* path, size_t size ) const;

	bool GetCompressionFilePath( TCHAR* path, size_t size ) const;
	bool GetDefaultCompressionFile( TCHAR* path, size_t size ) const;
	
	bool GetBoneTagsFile( TCHAR* path, size_t size ) const;

	bool GetAnimationPostRenderScript( TCHAR* path, size_t size ) const;

	// Cutscene
	bool GetEventDefsPath( TCHAR* path, size_t size) const;

	// Effects
	bool GetEffectsCutsceneSectionName( TCHAR* path, size_t size ) const;
	bool GetRmptfxPath( TCHAR* path, size_t size, bool subst=true) const;

	//Map Statistics
	bool GetBoundDrawableDistanceRatio( double& ratio ) const;
	bool GetDrawableCountLimit( int& limit ) const;
	bool GetTxdCountLimit ( int& limit ) const;

	// 3DS Max
	bool GetMaxAutoSubmitsEnabled( bool& enabled ) const;

	// Query methods...
	bool query( const TCHAR* expr, ConfigModelData& data ) const;
	bool query( const TCHAR* expr, TCHAR* data, size_t size, bool subst=true) const;
	bool query( const TCHAR* expr, bool& data ) const;
	bool query( const TCHAR* expr, int& data ) const;
	bool query( const TCHAR* expr, double& data ) const;

private:
	// Hide copy constructor and assignment operator.
	ConfigExportView( const ConfigExportView& copy );
	ConfigExportView& operator=( ConfigExportView& copy );

	bool query_model_raw( const TCHAR* expr, const ConfigModel& model, ConfigModelData& data ) const;
	bool set_model_raw( const TCHAR* expr, ConfigModel& model, const ConfigModelData& data, const bool flush );

	// Filenames
	TCHAR					m_sExportConfig[_MAX_PATH];

	configUtil::Environment* m_pEnv;		// Config environment object.
	ConfigModel				m_ExportModel;	// Export configuration model.
	ConfigGameView			m_ConfigGameView;

};

END_CONFIGPARSER_NS

#endif // __CONFIGPARSER_CONFIGEXPORTVIEW_H__
