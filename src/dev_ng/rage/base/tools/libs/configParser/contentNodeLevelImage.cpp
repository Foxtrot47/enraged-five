//
// filename:	contentNodeLevelImage.cpp
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		20 April
// description:	Content-tree group node class.
//

// LibXml2 headers
#include "libxml/parser.h"
#include "libxml/xpath.h"

// Local headers
#include "configParser/contentNodeLevelImage.h"

BEGIN_CONFIGPARSER_NS

ContentNodeLevelImage::ContentNodeLevelImage( ContentNode* parent, 
											 const configUtil::Environment* pEnv,
											 const TCHAR* filename, 
											 float minx, 
											 float miny, 
											 float maxx, 
											 float maxy )
	: ContentNode( parent, pEnv, _T(""), _T("level_image") )
	, m_fImageMinX( minx ) 
	, m_fImageMinY( miny )
	, m_fImageMaxX( maxx )
	, m_fImageMaxY( maxy )
{
#ifdef UNICODE
	wcsncpy_s( m_sImageFilename, _MAX_PATH, filename, _TRUNCATE );
#else
	strncpy_s( m_sImageFilename, _MAX_PATH, filename, _TRUNCATE );
#endif // UNICODE
}

ContentNodeLevelImage::ContentNodeLevelImage( ContentNode* parent, 
											 const configUtil::Environment* pEnv, 
											 xmlElementPtr pElement )
	: ContentNode( parent, pEnv, pElement )
{	
	xmlAttributePtr pAttribute = pElement->attributes;
	while ( pAttribute )
	{
		if ( 0 == xmlStrcasecmp( BAD_CAST "path", pAttribute->name ) )
		{
			configUtil::AnsiToTChar( m_sImageFilename, _MAX_PATH,
				(char*)pAttribute->children->content );
		}
		else if ( 0 == xmlStrcasecmp( BAD_CAST "min_x", pAttribute->name ) )
			m_fImageMinX = static_cast<float>( atof( (char*)pAttribute->children->content ) );
		else if ( 0 == xmlStrcasecmp( BAD_CAST "min_y", pAttribute->name ) )
			m_fImageMinY = static_cast<float>( atof( (char*)pAttribute->children->content ) );
		else if ( 0 == xmlStrcasecmp( BAD_CAST "max_x", pAttribute->name ) )
			m_fImageMaxX = static_cast<float>( atof( (char*)pAttribute->children->content ) );
		else if ( 0 == xmlStrcasecmp( BAD_CAST "max_y", pAttribute->name ) )
			m_fImageMaxY = static_cast<float>( atof( (char*)pAttribute->children->content ) );

		// Next attribute.
		pAttribute = (xmlAttributePtr)( pAttribute->next );
	}
}

ContentNodeLevelImage::~ContentNodeLevelImage( )
{
}

void
ContentNodeLevelImage::GetImageFilename( TCHAR* path, size_t size ) const
{
#ifdef UNICODE
	wcsncpy_s( path, size, m_sImageFilename, _TRUNCATE );
#else
	strncpy_s( path, size, m_sImageFilename, _TRUNCATE );
#endif // UNICODE
	m_pEnv->subst( path, size );
}

END_CONFIGPARSER_NS
