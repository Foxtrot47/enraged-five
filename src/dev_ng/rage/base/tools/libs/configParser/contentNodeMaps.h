#ifndef __CONFIGPARSER_CONTENTNODEMAPS_H__
#define __CONFIGPARSER_CONTENTNODEMAPS_H__

//
// filename:	contentNodeMaps.h
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		11 February 2010
// description:	Content-tree map node classes.
//

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

// --- Includes -----------------------------------------------------------------

// Local headers
#include "contentNode.h"
#include "contentNodeCore.h"
#include "contentNodeGroup.h"

// --- Classes ------------------------------------------------------------------

BEGIN_CONFIGPARSER_NS

//PURPOSE
//
class ContentNodeMap : public ContentNode
{
public:
	ContentNodeMap( ContentNode* parent,
					const configUtil::Environment* pEnv,
					const TCHAR* name,
					const TCHAR* subtype,
					const TCHAR* prefix,
					const bool definitions,
					const bool instances, 
					const bool slod2link = false,
					const bool data = true,
					const bool pedantic = true );
	ContentNodeMap( ContentNode* parent, const configUtil::Environment* pEnv, 
		xmlElementPtr pElement );
	virtual ~ContentNodeMap( );

	enum eMapType
	{
		UNKNOWN,			//!< Unknown type.
		PROPS,				//!< Prop definition(s).
		INTERIOR,			//!< Interior definition(s).
		CONTAINER,			//!< Regular map container.
		CONTAINER_PROPS,	//!< Map container with prop instances (sibling to CONTAINER).
		CONTAINER_LOD,		//!< Map container LODs.
		CONTAINER_OCCL,		//!< Map occlusion data.
		ASSET_COMBINE,		//!< Automated asset-combine output data.
	};

	//PURPOSE
	// Return xmlNodePtr representing this content node.
	virtual xmlNodePtr			ToXml() const;

	const eMapType				GetMapType( ) const { return m_eMapType; }
	void						SetMapType( eMapType type ) { this->m_eMapType = type; }

	/**
	 * @brief Return unique identifier seed (default: 0).
	 */
	unsigned int	GetSeed( ) const { return m_seed; }

	/**
	 * @brief Set unique identifier seed (default: 0).
	 */
	void			SetSeed( unsigned int seed ) { m_seed = seed;}

	/**
	 * @brief Return object prefix string for this map section.
	 */
	const TCHAR*	GetPrefix( ) const { return ( m_sPrefix ); }

	/**
	 * @brief Set object prefix string for this map section.
	 */
	void			SetPrefix( const TCHAR* prefix );
	
	/**
	 * @brief Get definitions flag for this map section.
	 */
	bool			GetDefinitions( ) const { return ( m_bDefinitions ); }
	
	/**
	 * @brief Set definitions flag for this map section.
	 */
	void			SetDefinitions( bool defs ) { m_bDefinitions = defs; }
	
	/**
	 * @brief Get instances flags for this map section.
	 */
	bool			GetSLOD2Link( ) const { return ( m_bSLOD2Link ); }
	
	/**
	 * @brief Set instances flag for this map section.
	 */
	void			SetSLOD2Link( bool slod2Link ) { m_bSLOD2Link = slod2Link; }

	/**
	 * @brief Get instances flags for this map section.
	 */
	bool			GetInstances( ) const { return ( m_bInstances ); }
	
	/**
	 * @brief Set instances flag for this map section.
	 */
	void			SetInstances( bool insts ) { m_bInstances = insts; }
	
	/**
	 * @brief Return data flag for this map section.
	 */
	bool			GetData( ) const { return ( m_bData ); }
	
	/**
	 * @brief Set data flag for this map section.
	 */
	void			SetData( bool data ) { m_bData = data; }
	
	/**
	 * @brief Get auto-export flag for this map section.
	 */
	bool			GetAutoExport( ) const { return ( m_bAutoExport ); }
	
	/**
	 * @brief Set auto-export flag for this map section.
	 */
	void			SetAutoExport( bool auto_export ) { m_bAutoExport = auto_export; }

	/**
	 * @brief Return pedantic flag for this map section.
	 */
	bool			GetPedantic( ) const { return ( m_bPedantic ); }
	
	/**
	 * @brief Set pedantic flag for this map section.
	 */
	void			SetPedantic( bool pedantic ) { m_bPedantic = pedantic; }

	/**
	 * @brief Get absolute path string for current group.
	 */
	void			GetAbsolutePath( TCHAR* path, size_t size ) const;
	
	/**
	 * @brief Get raw (no environment variable resolution) absolute path.
	 */
	void			GetRawAbsolutePath( TCHAR* path, size_t size ) const;

	// Child Container Iterators
	NodeContainerIter			BeginChildren( ) { return ( m_vChildren.begin() ); }
	NodeContainerIter			EndChildren( ) { return ( m_vChildren.end() ); }
	NodeContainerConstIter		BeginChildren( ) const { return ( m_vChildren.begin() ); }
	NodeContainerConstIter		EndChildren( ) const { return ( m_vChildren.end() ); } 

	size_t						GetNumChildren( ) const { return m_vChildren.size(); }
	ContentNode*				GetChild( int index ) const { return m_vChildren.at( index ); }
	void						AddChild( ContentNode* pNode ) { m_vChildren.push_back( pNode ); }
	NodeContainerIter			RemoveChild( NodeContainerIter nodeIter ) { return ( m_vChildren.erase( nodeIter ) ); }

	// Static Methods
	static const char*			GetXmlType( ) { return ( "map" ); }

protected:
	// Hide copy constructor and assignment operator.
	ContentNodeMap( const ContentNode& copy );
	ContentNodeMap& operator=( ContentNode& copy );

	void			InitMapType( const TCHAR* subtype );

	eMapType		m_eMapType;			//!< Subtype of map content.
	TCHAR			m_sPrefix[SM_SIZE];	//!< Map namespace prefix (optional)
	unsigned int	m_seed;				//!< Unique hash identifier seed (optional)
	bool			m_bDefinitions;		//!< Does map contain object definitions?
	bool			m_bInstances;		//!< Does map contain object instances?
	bool			m_bSLOD2Link;		//!< Does map need an SLOD2 link?
	bool			m_bData;			//!< Does map contain RPF data?
	bool			m_bPedantic;		//!< Should map export do pedantic checks?
	bool			m_bAutoExport;		//!< Flag whether map supports auto-export
	NodeContainer	m_vChildren;		//!< Child nodes.
};


//PURPOSE
//
// Note: ContentNodeGroup functionality re-implemented.
//
class ContentNodeMapZip : public ContentNodeZip
{
public:
	ContentNodeMapZip( ContentNode* parent, 
		const configUtil::Environment* pEnv,
		const TCHAR* name, 
		Platform platform );
	ContentNodeMapZip( ContentNode* parent, 
		const configUtil::Environment* pEnv, 
		xmlElementPtr pElement );
	virtual ~ContentNodeMapZip( );

	bool GetViaBoundsProcessor( ) const { return ( m_bViaBoundsProcessor ); }

	// Child Container Iterators
	NodeContainerIter			BeginChildren( ) { return ( m_vChildren.begin() ); }
	NodeContainerIter			EndChildren( ) { return ( m_vChildren.end() ); }
	NodeContainerConstIter		BeginChildren( ) const { return ( m_vChildren.begin() ); }
	NodeContainerConstIter		EndChildren( ) const { return ( m_vChildren.end() ); } 

	size_t						GetNumChildren( ) const { return m_vChildren.size(); }
	ContentNode*				GetChild( int index ) const { return m_vChildren.at( index ); }
	void						AddChild( ContentNode* pNode ) { m_vChildren.push_back( pNode ); }
	NodeContainerIter			RemoveChild( NodeContainerIter nodeIter ) { return ( m_vChildren.erase( nodeIter ) ); }


	virtual void				PostLoadCallback( );

	// Static Methods
	static const char*			GetXmlType( ) { return ( "mapzip" ); }

protected:
	bool			m_bViaBoundsProcessor;	//!< Does this mapzip need to be processed via the bounds processor?
	NodeContainer	m_vChildren;	//!< Child nodes.
};


//PURPOSE
//
// Note: ContentNodeGroup functionality re-implemented.
//
class ContentNodeMapProcessedZip : public ContentNodeZip
{
public:
	ContentNodeMapProcessedZip( ContentNode* parent, 
		const configUtil::Environment* pEnv,
		const TCHAR* name, 
		Platform platform );
	ContentNodeMapProcessedZip( ContentNode* parent, 
		const configUtil::Environment* pEnv, 
		xmlElementPtr pElement );
	virtual ~ContentNodeMapProcessedZip( );
	
	// Child Container Iterators
	NodeContainerIter			BeginChildren( ) { return ( m_vChildren.begin() ); }
	NodeContainerIter			EndChildren( ) { return ( m_vChildren.end() ); }
	NodeContainerConstIter		BeginChildren( ) const { return ( m_vChildren.begin() ); }
	NodeContainerConstIter		EndChildren( ) const { return ( m_vChildren.end() ); } 

	size_t						GetNumChildren( ) const { return m_vChildren.size(); }
	ContentNode*				GetChild( int index ) const { return m_vChildren.at( index ); }
	void						AddChild( ContentNode* pNode ) { m_vChildren.push_back( pNode ); }
	NodeContainerIter			RemoveChild( NodeContainerIter nodeIter ) { return ( m_vChildren.erase( nodeIter ) ); }


	virtual void				PostLoadCallback( );

	// Static Methods
	static const char*			GetXmlType( ) { return ( "processedmapzip" ); }

protected:
	NodeContainer	m_vChildren;	//!< Child nodes.
};

//PURPOSE
//
// Note: ContentNodeGroup functionality re-implemented.
//
class ContentNodeMapCombineZip : public ContentNodeZip
{
public:
	ContentNodeMapCombineZip( ContentNode* parent, 
		const configUtil::Environment* pEnv,
		const TCHAR* name, 
		Platform platform );
	ContentNodeMapCombineZip( ContentNode* parent, 
		const configUtil::Environment* pEnv, 
		xmlElementPtr pElement );
	virtual ~ContentNodeMapCombineZip( );

	// Child Container Iterators
	NodeContainerIter			BeginChildren( ) { return ( m_vChildren.begin() ); }
	NodeContainerIter			EndChildren( ) { return ( m_vChildren.end() ); }
	NodeContainerConstIter		BeginChildren( ) const { return ( m_vChildren.begin() ); }
	NodeContainerConstIter		EndChildren( ) const { return ( m_vChildren.end() ); } 

	size_t						GetNumChildren( ) const { return m_vChildren.size(); }
	ContentNode*				GetChild( int index ) const { return m_vChildren.at( index ); }
	void						AddChild( ContentNode* pNode ) { m_vChildren.push_back( pNode ); }
	NodeContainerIter			RemoveChild( NodeContainerIter nodeIter ) { return ( m_vChildren.erase( nodeIter ) ); }

	// Static Methods
	static const char*			GetXmlType( ) { return ( "mapcombinezip" ); }

protected:
	NodeContainer	m_vChildren;	//!< Child nodes.
};

END_CONFIGPARSER_NS

#endif // __CONFIGPARSER_CONTENTNODEMAPS_H__
