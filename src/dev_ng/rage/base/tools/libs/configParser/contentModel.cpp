//
// filename:	contentModel.cpp
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		15 February 2010
// description:	Content-tree model.
//

// Local headers
#include "configParser/contentModel.h"
#include "configParser/contentNodeCore.h"
#include "configParser/contentNodeDependencyDirectory.h"
#include "configParser/contentNodeDependencyFile.h"
#include "configParser/contentNodeGroup.h"
#include "configParser/contentNodeLevel.h"
#include "configParser/contentNodeLevelImage.h"
#include "configParser/contentNodeMaps.h"
#include "configParser/contentNodeVehicles.h"
#include "configParser/contentNodeTemplates.h"
#include "configParser/contentNodeUnknown.h"

// LibXml2 headers
#include "libxml/parser.h"
#include "libxml/xpath.h"

#define LG_SIZE (1024)

using namespace configUtil;

BEGIN_CONFIGPARSER_NS

ContentModel::ContentModel( configUtil::Environment* pEnv, 
						   const TCHAR* filename )
	: m_pEnvironment( pEnv )
	, m_RootGroupNode( NULL, pEnv, _T("root") )
	, m_CurrentPlatform( kIndependent )
{
#ifdef UNICODE
	wcsncpy_s( m_sFilename, _MAX_PATH, filename, _TRUNCATE );
#else
	strncpy_s( m_sFilename, _MAX_PATH, filename, _TRUNCATE );
#endif // UNICODE

	ParseFile( m_sFilename, 
		_T("/project/content/content"), 
		m_pEnvironment, 
		m_RootGroupNode,
		m_RootGroupNode,
		m_mFileGroups,
		false );

	// Invoke PostLoadCallback on all nodes.
	m_RootGroupNode.PostLoadCallback( );
}

ContentModel::~ContentModel( )
{
}

bool
ContentModel::SaveXml( ) const
{
	// Walk the tree, when discover a new source XML file then we serialise that
	// group to it.
	bool result = true;

	for ( GroupFileContainerConstIter it = m_mFileGroups.begin();
		  it != m_mFileGroups.end();
		  ++it )
	{
		TCHAR tcharFilename[_MAX_PATH] = {0};
		configUtil::AnsiToTChar( tcharFilename, _MAX_PATH, (*it).first.c_str() );
		ContentNodeGroup* pGroup = (*it).second;
		result &= pGroup->ToXmlDocument( tcharFilename );
	}

	return ( result );
}

ContentNode*
ContentModel::ParseContentNode( configUtil::Environment* pEnv, ContentNode* pParent, 
							   ContentNodeGroup& rootGroup, 
							   xmlNode* pNode, 
							   GroupFileContainer& fileGroup )
{
	ContentNode* pContentNode = NULL;

	switch ( pNode->type )
	{
	case XML_ELEMENT_NODE:
		{
			xmlElementPtr pElement = (xmlElementPtr)( pNode );
			xmlAttributePtr pAttribute = ( pElement->attributes );
			std::string type("unknown");
			while ( pAttribute )
			{
				// Skip until we find the type attribute.
				if ( 0 == xmlStrcasecmp( BAD_CAST "type", pAttribute->name ) )
				{
					type = (char*)pAttribute->children->content;
					break;
				}
				pAttribute = (xmlAttributePtr)( pAttribute->next );
			}

			//---------------------------------------------------------------
			// Core Types
			//---------------------------------------------------------------
			if ( 0 == _stricmp( type.c_str(), ContentNodeFile::GetXmlType( ) ) )
			{
				pContentNode = new ContentNodeFile( pParent, pEnv, pElement );
				ResolveInputNodes( rootGroup, *pContentNode, pElement );
			}
			else if ( 0 == _stricmp( type.c_str(), ContentNodeDirectory::GetXmlType( ) ) )
			{
				pContentNode = new ContentNodeDirectory( pParent, pEnv, pElement );
				ResolveInputNodes( rootGroup, *pContentNode, pElement );
			}
			else if ( 0 ==  _stricmp( type.c_str(), ContentNodeDirectory::GetXmlType( ) ) )
			{
				pContentNode = new ContentNodeDirectory( pParent, pEnv, pElement );
				ResolveInputNodes( rootGroup, *pContentNode, pElement );
			}
			else if ( 0 == _stricmp( type.c_str(), ContentNodeRpf::GetXmlType( ) ) )
			{
				pContentNode = new ContentNodeRpf( pParent, pEnv, pElement );
				ResolveInputNodes( rootGroup, *pContentNode, pElement );
			}
			else if ( 0 == _stricmp( type.c_str(), ContentNodeZip::GetXmlType( ) ) )
			{
				pContentNode = new ContentNodeZip( pParent, pEnv, pElement );
				ResolveInputNodes( rootGroup, *pContentNode, pElement );
			}
			else if ( 0 == _stricmp( type.c_str(), ContentNodeLevel::GetXmlType() ) )
			{
				ContentNodeLevel* pLevelNode = new ContentNodeLevel( pParent, pEnv, pElement );
				pContentNode = pLevelNode;

				// Parse child content nodes (if available).
				ParseIncludeNodes( pNode, _T("include"), pEnv, rootGroup, *pLevelNode, fileGroup );
				ParseContentNodes( pNode, _T("content"), pEnv, rootGroup, *pLevelNode, fileGroup );
			}
			else if ( 0 == _stricmp( type.c_str(), ContentNodeGroup::GetXmlType( ) ) )
			{
				ContentNodeGroup* pGroupNode = new ContentNodeGroup( pParent, pEnv, pElement );
				pContentNode = pGroupNode;

				// Parse child content nodes (if available).
				ParseIncludeNodes( pNode, _T("include"), pEnv, rootGroup, *pGroupNode, fileGroup );
				ParseContentNodes( pNode, _T("content"), pEnv, rootGroup, *pGroupNode, fileGroup );
			}
			//---------------------------------------------------------------
			// Dependency Types
			//---------------------------------------------------------------
			else if ( 0 == _stricmp( type.c_str(), ContentNodeDependencyDirectory::GetXmlType( ) ) )
			{
				pContentNode = new ContentNodeDependencyDirectory( pParent, pEnv, pElement );
			}
			else if ( 0 == _stricmp( type.c_str(), ContentNodeDependencyFile::GetXmlType( ) ) )
			{
				pContentNode = new ContentNodeDependencyFile( pParent, pEnv, pElement );
			}
			//---------------------------------------------------------------
			// Map Types
			//---------------------------------------------------------------
			else if ( 0 == _stricmp( type.c_str(), ContentNodeMap::GetXmlType( ) ) )
			{
				pContentNode = new ContentNodeMap( pParent, pEnv, pElement );
				ResolveInputNodes( rootGroup, *pContentNode, pElement );
			}
			else if ( 0 == _stricmp( type.c_str(), ContentNodeMapZip::GetXmlType( ) ) )
			{
				pContentNode = new ContentNodeMapZip( pParent, pEnv, pElement );				
				ResolveInputNodes( rootGroup, *pContentNode, pElement );
			}
			else if ( 0 == _stricmp( type.c_str(), ContentNodeMapProcessedZip::GetXmlType( ) ) )
			{
				pContentNode = new ContentNodeMapProcessedZip( pParent, pEnv, pElement );				
				ResolveInputNodes( rootGroup, *pContentNode, pElement );
			}
			else if ( 0 == _stricmp( type.c_str(), ContentNodeMapCombineZip::GetXmlType( ) ) )
			{
				pContentNode = new ContentNodeMapCombineZip( pParent, pEnv, pElement );
			}
			else if ( 0 == _stricmp( type.c_str(), ContentNodeLevelImage::GetXmlType( ) ) )
			{
				pContentNode = new ContentNodeLevelImage( pParent, pEnv, pElement );
			}
			//---------------------------------------------------------------
			// Vehicle Types
			//---------------------------------------------------------------
			else if ( 0 == _stricmp( type.c_str(), ContentNodeVehiclesRpf::GetXmlType() ) )
			{
				pContentNode = new ContentNodeVehiclesRpf( pParent, pEnv, pElement );
				ResolveInputNodes( rootGroup, *pContentNode, pElement );
			}
			else if ( 0 == _stricmp( type.c_str(), ContentNodeVehiclesZip::GetXmlType() ) )
			{
				pContentNode = new ContentNodeVehiclesZip( pParent, pEnv, pElement );
				ResolveInputNodes( rootGroup, *pContentNode, pElement );
			}
			else if ( 0 == _stricmp( type.c_str(), ContentNodeVehiclesDirectory::GetXmlType() ) )
			{
				pContentNode = new ContentNodeVehiclesDirectory( pParent, pEnv, pElement );
				ResolveInputNodes( rootGroup, *pContentNode, pElement );
			}
			else if ( 0 == _stricmp( type.c_str(), ContentNodeVehicleModZip::GetXmlType() ) )
			{
				pContentNode = new ContentNodeVehicleModZip( pParent, pEnv, pElement );
				ResolveInputNodes( rootGroup, *pContentNode, pElement );
			}
			//---------------------------------------------------------------
			// Templates
			//---------------------------------------------------------------
			else if ( 0 == _stricmp( type.c_str(), ContentNodeTemplateFileEach::GetXmlType() ) )
			{
				ContentNodeTemplateFileEach* pTemplateNode = new ContentNodeTemplateFileEach( pParent, pEnv, pElement );
				pContentNode = pTemplateNode;

				// Parse child content nodes (if available)
				ParseIncludeNodes( pNode, _T("include"), pEnv, rootGroup, *pTemplateNode, fileGroup );
				ParseContentNodes( pNode, _T("content"), pEnv, rootGroup, *pTemplateNode, fileGroup );
			}
			//---------------------------------------------------------------
			// Default Unknown Type
			//---------------------------------------------------------------
			else
			{
				ContentNodeUnknown* pUnknownNode = new ContentNodeUnknown( pParent, pEnv, pElement );
				pContentNode = pUnknownNode;
				ParseContentNodes( pNode, _T("content"), pEnv, rootGroup, *pUnknownNode, fileGroup );
				ResolveInputNodes( rootGroup, *pContentNode, pElement );
			}
		}
	default:
		break;
	}

	return ( pContentNode );
}

void 
ContentModel::ParseFile( const TCHAR* filename, 
						const TCHAR* expr, 
						configUtil::Environment* pEnv, 
						ContentNodeGroup& rootGroup, 
						ContentNodeGroup& group, 
						GroupFileContainer& fileGroup,
						bool map )
{
	char filenameBuffer[_MAX_PATH] = {0};
	configUtil::TCharToAnsi( filenameBuffer, _MAX_PATH, filename );

	xmlDocPtr pXmlDoc = xmlParseFile( filenameBuffer );
	xmlXPathContextPtr pXPathCtx = xmlXPathNewContext( pXmlDoc );

	char exprBuffer[LG_SIZE] = {0};
	configUtil::TCharToAnsi( exprBuffer, LG_SIZE, expr );

	xmlXPathObjectPtr pXPathObj = xmlXPathEvalExpression( 
		BAD_CAST exprBuffer, pXPathCtx );
	switch ( pXPathObj->type )
	{
	case XPATH_NODESET: 
		{
			size_t count = pXPathObj->nodesetval->nodeNr;
			for ( size_t n = 0; n < count; ++n )
			{
				xmlNode* pNode = pXPathObj->nodesetval->nodeTab[n];
				ContentNode* pContentNode = ParseContentNode( pEnv, &group, rootGroup, pNode, fileGroup );
				if ( map )
				{
					char ansiFilename[_MAX_PATH] = {0};
					configUtil::TCharToAnsi( ansiFilename, _MAX_PATH, filename );
					fileGroup[ansiFilename] = dynamic_cast<ContentNodeGroup*>( pContentNode );
				}

				if ( NULL != pContentNode )
					group.AddChild( pContentNode );
			}
		}
		break;
	default:
		break;
	}

	xmlXPathFreeObject( pXPathObj );
	xmlXPathFreeContext( pXPathCtx );
	xmlFreeDoc( pXmlDoc );
}

void
ContentModel::ParseIncludeNodes( xmlNodePtr pNode, 
								const TCHAR* expr, 
								configUtil::Environment* pEnv, 
								ContentNodeGroup& rootGroup, 
								ContentNodeGroup& group, 
								GroupFileContainer& fileGroup )
{
	xmlXPathContextPtr pCtx = xmlXPathNewContext( pNode->doc );
	pCtx->node = pNode;
	
	char exprBuffer[LG_SIZE] = {0};
	configUtil::TCharToAnsi( exprBuffer, LG_SIZE, expr );

	xmlXPathObjectPtr pIncludeXPath = xmlXPathEvalExpression( 
		BAD_CAST exprBuffer, pCtx );
	if ( pIncludeXPath )
	{
		switch ( pIncludeXPath->type )
		{
			case XPATH_NODESET:
				for ( int n = 0; n < pIncludeXPath->nodesetval->nodeNr; ++n )
				{
					xmlNodePtr pElem = pIncludeXPath->nodesetval->nodeTab[n];
					xmlAttributePtr pAttr = ((xmlElementPtr)(pElem))->attributes;
					
					// Read and resolve the filename from the 'href' attribute
					TCHAR filename[_MAX_PATH];
					memset( filename, 0, sizeof( TCHAR ) * _MAX_PATH );

					while ( pAttr )
					{
						if ( 0 == xmlStrcasecmp( BAD_CAST "href", pAttr->name ) )
						{
							configUtil::AnsiToTChar( filename, _MAX_PATH, 
								(const char*)pAttr->children->content );
							pEnv->subst( filename, _MAX_PATH );
							break;
						}
						pAttr = (xmlAttributePtr)pAttr->next;
					}

					ParseFile( filename, _T("/content"), pEnv, rootGroup, group, fileGroup, true );
				}
				break;
			default:
				break;
		}
	}
	
	xmlXPathFreeObject( pIncludeXPath );
	xmlXPathFreeContext( pCtx );
}

void 
ContentModel::ParseContentNodes( xmlNodePtr pNode, const TCHAR* expr, 
								configUtil::Environment* pEnv, 
								ContentNodeGroup& rootGroup, 
								ContentNodeGroup& group, 
								GroupFileContainer& fileGroup )
{
	xmlXPathContextPtr pCtx = xmlXPathNewContext( pNode->doc );
	pCtx->node = pNode;
	
	char exprBuffer[LG_SIZE] = {0};
	configUtil::TCharToAnsi( exprBuffer, LG_SIZE, expr );

	xmlXPathObjectPtr pChildContentXPath = xmlXPathEvalExpression( 
		BAD_CAST exprBuffer, pCtx );
	if ( pChildContentXPath )
	{
		switch ( pChildContentXPath->type )
		{
			case XPATH_NODESET:
				for ( int n = 0; n < pChildContentXPath->nodesetval->nodeNr; ++n )
				{
					xmlNodePtr pElem = pChildContentXPath->nodesetval->nodeTab[n];
					ContentNode* pChildNode = ParseContentNode( pEnv, &group, rootGroup, pElem, fileGroup );
					if ( pChildNode )
						group.AddChild( pChildNode );
				}
				break;
			default:
				break;
		}
	}

	xmlXPathFreeObject( pChildContentXPath );
	xmlXPathFreeContext( pCtx );
}

void
ContentModel::ResolveInputNodes( ContentNodeGroup& root, ContentNode& node, xmlElementPtr pElement )
{
	xmlXPathContextPtr pXPathCtx = xmlXPathNewContext( pElement->doc );
	assert( pXPathCtx );
	if ( !pXPathCtx )
		return;

	pXPathCtx->node = reinterpret_cast<xmlNodePtr>( pElement );
	xmlXPathObjectPtr pXPathObj = xmlXPathEvalExpression( BAD_CAST "in", pXPathCtx );

	// Determine if we have a <in> node.
	if ( !pXPathObj )
	{
		xmlXPathFreeContext( pXPathCtx );
		return;
	}
	if ( ( XPATH_NODESET != pXPathObj->type ) || 
		 ( !pXPathObj->nodesetval ) ||
		 ( 0 == pXPathObj->nodesetval->nodeNr ) )
	{
		xmlXPathFreeObject( pXPathObj );
		xmlXPathFreeContext( pXPathCtx );
		return;
	}
	
	for ( int nNode = 0; nNode < pXPathObj->nodesetval->nodeNr; ++nNode )
	{
		xmlNodePtr pNode = pXPathObj->nodesetval->nodeTab[nNode];
		xmlElementPtr pInElement = reinterpret_cast<xmlElementPtr>( pNode );
		TCHAR sGroup[SM_SIZE] = {0};
		TCHAR sName[SM_SIZE] = {0};
		TCHAR sType[SM_SIZE] = {0};
		xmlAttributePtr pAttribute = pInElement->attributes;
		while ( pAttribute )
		{
			if ( 0 == xmlStrcasecmp( BAD_CAST "group", pAttribute->name ) )
			{
				configUtil::AnsiToTChar( sGroup, SM_SIZE, 
					(char*)pAttribute->children->content );
			}
			else if ( 0 == xmlStrcasecmp( BAD_CAST "name", pAttribute->name ) )
			{	
				configUtil::AnsiToTChar( sName, SM_SIZE,
					(char*)pAttribute->children->content );
			}
			else if ( 0 == xmlStrcasecmp( BAD_CAST "type", pAttribute->name ) )
			{
				configUtil::AnsiToTChar( sType, SM_SIZE,
					(char*)pAttribute->children->content );
			}
			// Next attribute
			pAttribute = (xmlAttributePtr)( pAttribute->next );
		}

		ContentNode* pGroupNode = root.FindFirst( sGroup, _T("group") );
		ContentNodeGroup* pGroup = dynamic_cast<ContentNodeGroup*>( pGroupNode );
		if ( !pGroup )
			continue; // Input node group not found; skip.

		const ContentNode* pContentNode = pGroup->FindFirst( sName, sType );
		if ( pContentNode )
		{
			size_t len = _tcslen( sGroup );
			if ( len > 0 )
				node.SetGroup( sGroup );
			node.AddInput( pContentNode );
			const_cast<ContentNode*>( pContentNode )->AddOutput( &node );
		}
	}

	xmlXPathFreeObject( pXPathObj );
	xmlXPathFreeContext( pXPathCtx );
}

END_CONFIGPARSER_NS
