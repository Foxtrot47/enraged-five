#ifndef __CONFIGPARSER_CONFIGPARSER_H__
#define __CONFIGPARSER_CONFIGPARSER_H__

//
// filename:	configParser.h
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		18 December 2009
// description:	Library main header.
//

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

// --- Include Files ------------------------------------------------------------
// None

// --- Defines ------------------------------------------------------------------
#define BEGIN_CONFIGPARSER_NS namespace configParser {
#define END_CONFIGPARSER_NS }

// --- Classes ------------------------------------------------------------------
// None

#endif // __CONFIGPARSER_CONFIGPARSER_H__
