//
// filename:	contentNodeDependencyDirectory.cpp
// author:		Jonny Rivers <jonathan.rivers@rockstarnorth.com>
// date:		18 November 2011
// description:	Content-tree node dependency directory class.
//

// LibXml2 headers
#include "libxml/parser.h"
#include "libxml/xpath.h"

// Local headers
#include "contentNodeDependencyDirectory.h"

BEGIN_CONFIGPARSER_NS

// --- ContentNodeFile ------------------------------------------------------

ContentNodeDependencyDirectory::ContentNodeDependencyDirectory( ContentNode* parent,
																const configUtil::Environment* pEnv,
																const TCHAR* name, 
																const TCHAR* directory, 
																const TCHAR* type )
	: ContentNode( parent, pEnv, name, type )
{
	memset( m_sDirectory, 0, sizeof( TCHAR ) * _MAX_PATH );
	m_bOptional = false;

#ifdef UNICODE
	wcsncpy_s( m_sDirectory, _MAX_PATH, directory, _TRUNCATE );
#else
	strncpy_s( m_sDirectory, _MAX_PATH, directory, _TRUNCATE );
#endif // UNICODE
}

ContentNodeDependencyDirectory::ContentNodeDependencyDirectory( ContentNode* parent,
																const configUtil::Environment* pEnv, 
																xmlElement* pElement )
	: ContentNode( parent, pEnv, pElement )
{
	memset( m_sDirectory, 0, sizeof( TCHAR ) * _MAX_PATH );
	m_bOptional = false;

	xmlAttributePtr pAttribute = pElement->attributes;
	while ( pAttribute )
	{
		if ( 0 == xmlStrcasecmp( BAD_CAST "path", pAttribute->name ) )
		{
			configUtil::AnsiToTChar( m_sDirectory, _MAX_PATH, (char*)pAttribute->children->content );
		}
		else if ( 0 == xmlStrcasecmp( BAD_CAST "optional", pAttribute->name ) )
		{
			m_bOptional = (0 == xmlStrcasecmp( BAD_CAST "true", pAttribute->children->content ) );
		}

		// Next attribute.
		pAttribute = (xmlAttributePtr)( pAttribute->next );
	}
}

ContentNodeDependencyDirectory::~ContentNodeDependencyDirectory( )
{
}

void ContentNodeDependencyDirectory::GetDirectory( TCHAR* path, size_t size ) const
{
#ifdef UNICODE
	wcsncpy_s( path, size, m_sDirectory, _TRUNCATE );
#else
	strcpy_s( path, size, m_sDirectory );
#endif // UNICODE
	m_pEnv->subst( path, size );
}

bool ContentNodeDependencyDirectory::IsOptional() const
{
	return m_bOptional;
}

xmlNodePtr ContentNodeDependencyDirectory::ToXml( ) const
{
	xmlNodePtr pElement = ContentNode::ToXml( );
	char ansiDirectory[_MAX_PATH] = {0};
	configUtil::TCharToAnsi( ansiDirectory, _MAX_PATH, m_sDirectory );
	size_t lenDirectory = strlen( ansiDirectory );

	if ( lenDirectory > 0 )
		xmlNewProp( pElement, BAD_CAST "path", BAD_CAST ansiDirectory );

	if (m_bOptional)
		xmlNewProp( pElement, BAD_CAST "optional", BAD_CAST "true" );

	return ( pElement );
}

END_CONFIGPARSER_NS
