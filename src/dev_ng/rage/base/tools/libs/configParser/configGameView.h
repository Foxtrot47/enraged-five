#ifndef __CONFIGPARSER_CONFIGGAMEVIEW_H__
#define __CONFIGPARSER_CONFIGGAMEVIEW_H__

//
// filename:	configGameView.h
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		21 January 2010
// description:	
//

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

// --- Includes -----------------------------------------------------------------

// Local headers
#include "configCoreView.h"
#include "contentModel.h"
#include "configPerforceInfo.h"

// DHM -- Undef "GetUserName" because Winbase.h sucks.
#undef GetUserName

// --- Defines ------------------------------------------------------------------
#define LG_SIZE (1024)

// --- Forward Declarations -----------------------------------------------------
namespace configUtil
{
	class Environment;
}

// --- Classes ------------------------------------------------------------------

BEGIN_CONFIGPARSER_NS

//PURPOSE
// A higher level view than ConfigCoreView.  This view understands the game/tools
// configuration data in config.xml and project.xml a little more than just
// loading them for XPath evaluation.
//
// You can set a particular request specific paths based on a project-branch
// context which should be set; otherwise the project-default branch will be 
// used (typically "dev").
//
class ConfigGameView
{
public:
	ConfigGameView( );
	ConfigGameView( bool bLoadContent );
	~ConfigGameView( );

	// Tools
	bool GetToolsRootDir( TCHAR* path, size_t size ) const;
	bool GetToolsBinDir( TCHAR* path, size_t size ) const;
	bool GetToolsLibDir( TCHAR* path, size_t size ) const;
	bool GetToolsConfigDir( TCHAR* path, size_t size ) const;
	bool GetToolsRuby( TCHAR* path, size_t size ) const;
	bool GetToolsLogDir( TCHAR* path, size_t size ) const;
	bool GetToolsTempDir( TCHAR* path, size_t size ) const;
	
	// Bugstar methods
	unsigned int GetBugstarId( ) const;
	bool GetBugstarServer( TCHAR* server, size_t size ) const;
	bool GetBugstarServerIp( TCHAR* server_ip, size_t size ) const;
	unsigned int GetBugstarPort( ) const;
	bool GetBugstarRestService( TCHAR* service, size_t size ) const;
	bool GetBugstarAttachmentService( TCHAR* service, size_t size ) const;
	bool GetBugstarUserDomain( TCHAR* user_domain, size_t size ) const;
	bool GetBugstarReadOnlyUser( TCHAR* user_domain, size_t size ) const;
	bool GetBugstarReadOnlyPassword( TCHAR* user_domain, size_t size ) const;
	size_t GetNumBugstarLevels() const;
	bool GetBugstarLevelKey( size_t index, TCHAR* level, size_t size ) const;
	bool GetBugstarLevelId( const TCHAR* level_name, unsigned int& id ) const;

	// Statistics methods
	bool IsStatisticsEnabled( ) const;
	bool GetStatisticsServer( TCHAR* server, size_t size ) const;
	unsigned int GetStatisticsPort( ) const;
	bool IsStatisticsUsingHttps( ) const;
	bool GetStatisticsRestService( TCHAR* service, size_t size ) const;

	// User Type methods
	size_t GetNumUserTypes( ) const;
	void GetUserType( size_t index, UserType& userType ) const;

	// User methods
	bool GetUserName( TCHAR* username, size_t size ) const;
	bool SetUserName( const TCHAR* username );
	bool GetUserEmailAddress( TCHAR* email, size_t size ) const;
	bool SetUserEmailAddress( const TCHAR* email );
	bool GetUserFlags( unsigned int& flags ) const;
	bool SetUserFlags( unsigned int flags );
	bool GetStudio( TCHAR* studio, size_t size ) const;
	unsigned int GetUserPseudoTypeMask() const;
	
	// XGE methods
	bool GetUseXoreaxXGE( ) const;
	bool SetUseXoreaxXGE( bool value );

	//DCC methods
	int GetNumMaxVariants() const;
	bool GetMaxVariantName( size_t index, TCHAR* variant, size_t size ) const;
	int GetMaxVariant() const;
	bool SetMaxVariant( size_t index );

	// Logging methods
	bool GetLogToStdOut( ) const;
	bool SetLogToStdOut( bool value );
	int GetLogLevel( ) const;
	bool SetLogLevel( int level );
	bool GetLogTraceInfo( ) const;
	bool SetLogTraceInfo( bool value );
	bool GetLogMailErrors( ) const;
	bool SetLogMailErrors( bool value );
	bool GetLogMailServerHost( TCHAR* server, size_t size ) const;
	int GetLogMailServerPort( ) const;

	// Branch methods
	bool SetBranch( const TCHAR* branch_name );
	bool GetBranch( TCHAR* branch, size_t size ) const;
	size_t GetNumBranches( ) const;
	bool GetBranchKey( size_t index, TCHAR* branch, size_t size ) const;
	bool GetDefaultBranch( TCHAR* branch, size_t size ) const;

	// Label methods
	size_t GetNumLabels() const;// Get the number of labels
	bool GetLabelTypeAt(size_t index, TCHAR* type, size_t type_size) const;// Get the label type at a given index
	bool GetLabelNameAt(size_t index, TCHAR* name, size_t name_size) const;// Get the label name at a given index
	bool GetLabelName(const TCHAR* type, TCHAR* name, size_t name_size);// Get a named label

	// Target methods
	size_t GetNumTargets( const TCHAR* branch_name = NULL ) const;
	bool GetTargetKey( size_t index, TCHAR* target, size_t size, const TCHAR* branch_name = NULL ) const;
	bool GetTargetEnabled( const TCHAR* platform_name, const TCHAR* branch_name = NULL ) const;
	bool GetTargetDir( const TCHAR* platform_name, TCHAR* path, size_t size, const TCHAR* branch_name = NULL ) const;
	bool GetTargetRagebuilder( const TCHAR* branch_name, const TCHAR* platform_name, TCHAR* path, size_t size ) const;
	bool GetTargetRagebuilderOptions( const TCHAR* branch_name, const TCHAR* platform_name, TCHAR* path, size_t size ) const;

	bool SetNumTargets( size_t num, const TCHAR* branch_name = NULL );
	bool SetTargetKey( size_t index, const TCHAR* target, const TCHAR* branch_name = NULL );
	bool SetTargetEnabled( const TCHAR* platform_name, bool enabled, const TCHAR* branch_name = NULL );
	bool SetTargetEnabled( size_t index, bool enabled, const TCHAR* branch_name = NULL );

	size_t GetNumDlcProjects( ) const;
	bool GetDlcProjectName( size_t index, TCHAR* name, size_t size ) const;
	bool GetDlcProjectRoot( size_t index, TCHAR* name, size_t size ) const;
	bool GetDlcProjectConfig( size_t index, TCHAR* name, size_t size ) const;
	bool GetDlcProjectAssets(const TCHAR* name, TCHAR* assets, size_t size) const;
	bool GetDlcProjectArt(const TCHAR* name, TCHAR* art, size_t size) const;
	bool GetDlcProjectExport(const TCHAR* name, TCHAR* art, size_t size) const;

	// Perforce methods
	bool GetPerforceIntegration( ) const;
	bool SetPerforceIntegration( bool enabled );
	size_t GetNumPerforces( ) const;
	void SetNumPerforces( size_t num );
	bool GetPerforceInfo( size_t index, PerforceInfo& perforce ) const;
	bool SetPerforceInfo( size_t index, const PerforceInfo& perforce );

	// Report methods
	size_t GetNumReportCategories( ) const;
	bool GetReportCategoryName( size_t index, TCHAR* name, size_t size ) const;
	size_t GetReportCategoryNumReports( size_t index ) const;
	bool GetReportCategoryReportName( size_t categoryIndex, size_t reportIndex, TCHAR* name, size_t size ) const;
	bool GetReportCategoryReportUrl( size_t categoryIndex, size_t reportIndex, TCHAR* url, size_t size ) const;

	// Out-of-branch-context query methods...
	bool GetProjectName( TCHAR* name, size_t size ) const;
	bool GetProjectUIName( TCHAR* name, size_t size ) const;
	bool GetProjectRootDir( TCHAR* path, size_t size ) const;
	bool HasLevels( ) const;
	bool IsEpisodic( ) const;
	bool GetCacheDir( TCHAR* path, size_t size ) const;
	bool GetNetworkDir( TCHAR* path, size_t size ) const;
	bool GetNetworkTextureDir( TCHAR* path, size_t size ) const;
	bool GetNetworkStreamDir( TCHAR* path, size_t size ) const;
	bool GetNetworkGenericStreamDir( TCHAR* path, size_t size ) const;

	// In-branch-context query methods...
	bool GetBuildDir( TCHAR* path, size_t size ) const;
	bool GetMetadataDir( TCHAR* path, size_t size ) const;
	bool GetDefinitionsDir( TCHAR* path, size_t size ) const;
	bool GetExportDir( TCHAR* path, size_t size ) const;
	bool GetProcessedDir( TCHAR* path, size_t size ) const;
	bool GetCommonDir( TCHAR* path, size_t size ) const;
	bool GetShaderDir( TCHAR* path, size_t size ) const;
	bool GetSourceDir( TCHAR* path, size_t size ) const;
	bool GetRageSourceDir( TCHAR* path, size_t size ) const;
	bool GetScriptDir( TCHAR* path, size_t size ) const;
	bool GetArtDir( TCHAR* path, size_t size ) const;
	bool GetAssetsDir( TCHAR* path, size_t size ) const;
	bool GetPreviewDir( TCHAR* path, size_t size ) const;
	bool GetAudioDir( TCHAR* path, size_t size ) const;
	bool GetTuneFileDir( TCHAR* path, size_t size ) const;
	bool GetTextDir( TCHAR* path, size_t size ) const;
	bool GetMapTexDir( TCHAR* path, size_t size ) const;

	// Forward generic query methods...
	bool query( const TCHAR* expr, TCHAR* data, size_t size ) const;
	bool query( const TCHAR* expr, bool& data ) const;
	bool query( const TCHAR* expr, int& data ) const;
	bool query( const TCHAR* expr, double& data ) const;

	//PURPOSE
	// Reload the content model.
	ContentModel& ReloadContentModel( ) 
	{ 
		TCHAR filename[LG_SIZE] = {0};
		m_CoreView.GetProjectConfigFilename( filename, LG_SIZE );

		delete m_pContentTree;
		m_pContentTree = NULL;
		m_pContentTree = new ContentModel( m_pEnv, filename );
		return ( *m_pContentTree );
	}

	bool Save();

	//PURPOSE
	// Return reference to the content model.
	ContentModel& GetContentModel( ) const { return ( *m_pContentTree ); }

	//PURPOSE
	// Acquires this environment which contains special game-specific variables.
	configUtil::Environment* GetEnvironment( ) const { return m_pEnv; }

	ConfigCoreView& GetConfigCoreView() { return m_CoreView; }

private:

	// Hide copy constructor and assignment operator.
	ConfigGameView( const ConfigGameView& copy );
	ConfigGameView& operator=( ConfigGameView& copy );

	void init( bool bLoadContent );
	void import_project_environment( );
	void import_branch_environment( );

	bool get_project_branch_attr( const TCHAR* attr, TCHAR* out, size_t size ) const;
	bool get_project_branch_attr_raw( const TCHAR* attr, TCHAR* out, size_t size ) const;
	bool get_project_branch_path_attr( const TCHAR* attr, TCHAR* out, size_t size ) const;

	size_t GetNodeCountForQuery(const TCHAR* expr) const;
	void PopulateDlcMapData();

	TCHAR						m_sProjectKey[SM_SIZE];	//!< Project key (e.g. "gta5", "jimmy")
	TCHAR						m_sBranchKey[SM_SIZE];	//!< Branch key (e.g. "dev", "dev_nm")

	configUtil::Environment*	m_pEnv;			//!< Config environment object.
	ConfigCoreView				m_CoreView;		//!< Core view component to pass through.
	ContentModel*				m_pContentTree;	//!< Content tree
	std::map<STL_STRING, STL_STRING>	m_DlcMap;
};

END_CONFIGPARSER_NS

#endif // __CONFIGPARSER_CONFIGGAMEVIEW_H__
