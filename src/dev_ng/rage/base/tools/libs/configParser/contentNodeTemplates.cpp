//
// filename:	contentNodeTemplates.cpp
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		29 March 2010
// description:	Content-tree template node classes.
//

// LibXml2 headers
#include "libxml/parser.h"
#include "libxml/xpath.h"

// Local headers
#include "configParser/contentNodeTemplates.h"

BEGIN_CONFIGPARSER_NS

ContentNodeTemplateFileEach::ContentNodeTemplateFileEach( ContentNode* parent,
									   const configUtil::Environment* pEnv,
									   const TCHAR* name, 
									   const TCHAR* type )
	: ContentNodeGroup( parent, pEnv, name, type )
{
}

ContentNodeTemplateFileEach::ContentNodeTemplateFileEach( ContentNode* parent,
									   const configUtil::Environment* pEnv, 
									   xmlElementPtr pElement )
	: ContentNodeGroup( parent, pEnv, pElement )
{
	xmlAttributePtr pAttribute = pElement->attributes;
	while ( pAttribute )
	{
		if ( 0 == xmlStrcasecmp( BAD_CAST "search", pAttribute->name ) )
		{
			configUtil::AnsiToTChar( m_sSearch, _MAX_PATH, 
				(char*)pAttribute->children->content );
		}
		// Next attribute.
		pAttribute = (xmlAttributePtr)( pAttribute->next );
	}
}

ContentNodeTemplateFileEach::~ContentNodeTemplateFileEach( )
{
}

xmlNodePtr	
ContentNodeTemplateFileEach::ToXml() const
{
	// Deliberately don't invoke ContentNodeGroup::ToXml as that would write 
	// out the group's children.
	xmlNodePtr pElement = ContentNode::ToXml();
	
	char ansiRelativePath[_MAX_PATH] = {0};
	char ansiSearch[_MAX_PATH] = {0};
	configUtil::TCharToAnsi( ansiRelativePath, _MAX_PATH, m_sRelativePath );
	configUtil::TCharToAnsi( ansiSearch, _MAX_PATH, m_sSearch );
	size_t lenPath = strlen( ansiRelativePath );
	size_t lenSearch = strlen( ansiSearch );
		
	if ( lenPath > 0 )
		xmlNewProp( pElement, BAD_CAST "path", BAD_CAST ansiRelativePath );
	if ( lenSearch > 0 )
		xmlNewProp( pElement, BAD_CAST "search", BAD_CAST ansiSearch );
	
	// Deliberately don't add children as they are auto-populated.
	// Add our Xml children instead.
	for ( NodeContainerConstIter it = m_vXmlChildren.begin();
		  it != m_vXmlChildren.end();
		  ++it )
	{
		xmlNodePtr pChild = (*it)->ToXml( );
		xmlAddChild( pElement, pChild );
	}

	return ( pElement );
}

void
ContentNodeTemplateFileEach::PostLoadCallback( )
{
	ContentNodeGroup::PostLoadCallback();

	// Sort out children; keep a copy of real XML children and then
	// repopulate based on template.
	m_vXmlChildren = m_vChildren;
	m_vChildren.clear();

	// DHM TODO: run template to populate children.
}

END_CONFIGPARSER_NS
