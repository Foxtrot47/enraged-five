//
// filename:	configGameView.cpp
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		21 January 2010
// description:	
//
// Local headers
#pragma warning(disable: 4996)

#include "configGameView.h"

// Other headers
#include "configUtil/Environment.h"
#include "configUtil/path.h"

#include "string/string.h"
#include "string/unicode.h"

// STL Headers Files
#include <algorithm>

#pragma warning( disable : 4668 )
// Boost Header Files
#pragma warning( push )
#pragma warning( disable : 4512 )
#include "boost/algorithm/string/replace.hpp"
#include "boost/algorithm/string/case_conv.hpp"
#pragma warning( pop )

using namespace configUtil;

// --- Defines ------------------------------------------------------------------

// Convienience macro to query a project root node attribute.
#define GET_PROJECT_ATTR( attr, out, size ) \
	{ \
		bool result = ( query( _T("//project/@")attr, out, size ) ); \
		m_pEnv->subst( out, size ); \
		return result; \
	}

// Convienience macro to query a project root node attribute.
#define GET_PROJECT_PATH_ATTR( attr, out, size ) \
	{ \
		bool result = ( query( _T("//project/@")attr, out, size ) ); \
		m_pEnv->subst( out, size ); \
		return result; \
	}

// --- Implementation -----------------------------------------------------------

BEGIN_CONFIGPARSER_NS

ConfigGameView::ConfigGameView( )
	: m_pEnv( NULL )
	, m_pContentTree( NULL )
{
	init( true );
}

ConfigGameView::ConfigGameView( bool bLoadContent )
	: m_pEnv( NULL )
	, m_pContentTree( NULL )
{
	init( bLoadContent );
}

ConfigGameView::~ConfigGameView( )
{
	if ( NULL != m_pContentTree )
	{
		delete m_pContentTree;
		m_pContentTree = NULL;
	}
	m_pEnv->clear( );
}

bool 
ConfigGameView::GetToolsRootDir( TCHAR* path, size_t size ) const
{
	return ( m_CoreView.GetToolsRootDir( path, size ) );
}

bool 
ConfigGameView::GetToolsBinDir( TCHAR* path, size_t size ) const
{

	return ( m_CoreView.GetToolsBinDir( path, size ) );
}

bool 
ConfigGameView::GetToolsLibDir( TCHAR* path, size_t size ) const
{
	return ( m_CoreView.GetToolsLibDir( path, size ) );
}

bool 
ConfigGameView::GetToolsConfigDir( TCHAR* path, size_t size ) const
{
	return ( m_CoreView.GetToolsConfigDir( path, size ) );
}

bool 
ConfigGameView::GetToolsRuby( TCHAR* path, size_t size ) const
{
	return ( m_CoreView.GetToolsRuby( path, size ) );
}

bool 
ConfigGameView::GetToolsLogDir( TCHAR* path, size_t size ) const
{
	return ( m_CoreView.GetToolsLogDir( path, size ) );
}

bool 
ConfigGameView::GetToolsTempDir( TCHAR* path, size_t size ) const
{
	return ( m_CoreView.GetToolsTempDir( path, size ) );
}

unsigned int 
ConfigGameView::GetBugstarId( ) const
{
	configParser::ConfigModelData data;
	bool result = m_CoreView.query( _T("//project/bugstar/@id"), data );

	int id = 0;
	if ( result )
	{
		TCHAR identifier[MAX_PATH] = {0};
		data.GetValue( identifier, MAX_PATH );
		id = _tstoi( identifier );
	}
	return ( id );	
}

bool
ConfigGameView::GetBugstarServer( TCHAR* server, size_t size ) const
{
	return ( query( _T("//project/bugstar/@server"), server, size ) );
}

bool
ConfigGameView::GetBugstarServerIp( TCHAR* server_ip, size_t size ) const
{
	return ( query( _T("//project/bugstar/@serverip"), server_ip, size ) );
}

unsigned int 
ConfigGameView::GetBugstarPort( ) const
{
	configParser::ConfigModelData data;
	bool result = m_CoreView.query( _T("//project/bugstar/@port"), data );

	int port = 0;
	if ( result )
	{
		TCHAR identifier[MAX_PATH] = {0};
		data.GetValue( identifier, MAX_PATH );
		port = _tstoi( identifier );
	}
	return ( port );	
}

bool
ConfigGameView::GetBugstarRestService( TCHAR* server, size_t size ) const
{
	return ( query( _T("//project/bugstar/@restservice"), server, size ) );
}

bool
ConfigGameView::GetBugstarAttachmentService( TCHAR* server, size_t size ) const
{
	return ( query( _T("//project/bugstar/@attachmentservice"), server, size ) );
}

bool
ConfigGameView::GetBugstarUserDomain( TCHAR* server, size_t size ) const
{
	return ( query( _T("//project/bugstar/@userdomain"), server, size ) );
}

bool
ConfigGameView::GetBugstarReadOnlyUser( TCHAR* server, size_t size ) const
{
	return ( query( _T("//project/bugstar/@readonlyuser"), server, size ) );
}

bool
ConfigGameView::GetBugstarReadOnlyPassword( TCHAR* server, size_t size ) const
{
	return ( query( _T("//project/bugstar/@readonlypass"), server, size ) );
}

size_t 
ConfigGameView::GetNumBugstarLevels() const
{
	return GetNodeCountForQuery(_T("//project/bugstar/level/@name"));
}

bool 
ConfigGameView::GetBugstarLevelKey( size_t index, TCHAR* level, size_t size ) const
{
	configParser::ConfigModelData data;
	bool result = m_CoreView.query( _T("//project/bugstar/level/@name"), data );
	configParser::ConfigModelData dataBranch = data[index];

	dataBranch.GetValue( level, size );
	return ( result );
}

bool 
ConfigGameView::GetBugstarLevelId( const TCHAR* level_name, unsigned int& id ) const
{
	TCHAR query[_MAX_PATH] = {0};
	_stprintf_s( query, _MAX_PATH, _T("//project/bugstar/level[@name=\"%s\"]/@id"), level_name);

	configParser::ConfigModelData data;
	bool result = m_CoreView.query( query, data );

	if ( result )
	{
		TCHAR identifier[MAX_PATH] = {0};
		data.GetValue( identifier, MAX_PATH );
		id = _tstoi( identifier );
	}
	return ( result );
}

bool
ConfigGameView::IsStatisticsEnabled( ) const
{
	bool isStatsEnabled = true;
	m_CoreView.query( _T("//project/statistics/@enabled"), isStatsEnabled );
	return ( isStatsEnabled );
}

bool
ConfigGameView::GetStatisticsServer( TCHAR* server, size_t size ) const
{
	return ( query( _T("//project/statistics/@server"), server, size ) );
}

unsigned int 
ConfigGameView::GetStatisticsPort( ) const
{
	configParser::ConfigModelData data;
	bool result = m_CoreView.query( _T("//project/statistics/@port"), data );

	int port = 0;
	if ( result )
	{
		TCHAR identifier[MAX_PATH] = {0};
		data.GetValue( identifier, MAX_PATH );
		port = _tstoi( identifier );
	}
	return ( port );	
}

bool
ConfigGameView::IsStatisticsUsingHttps( ) const
{
	bool isStatsHttpsEnabled = true;
	m_CoreView.query( _T("//project/statistics/@usehttps"), isStatsHttpsEnabled );
	return ( isStatsHttpsEnabled );
}

bool
ConfigGameView::GetStatisticsRestService( TCHAR* server, size_t size ) const
{
	return ( query( _T("//project/statistics/@restservice"), server, size ) );
}

size_t
ConfigGameView::GetNumUserTypes( ) const
{
	return ( m_CoreView.GetNumUserTypes() );
}

void
ConfigGameView::GetUserType( size_t index, UserType& userType ) const
{
	m_CoreView.GetUserType(index, userType);
}

bool 
ConfigGameView::GetUserName( TCHAR* username, size_t size ) const
{
	return ( m_CoreView.GetUserName( username, size ) );
}

bool
ConfigGameView::SetUserName( const TCHAR* username )
{
	return ( m_CoreView.SetUserName( username ) );
}

unsigned int 
ConfigGameView::GetUserPseudoTypeMask() const
{
	size_t size = GetNumUserTypes();
	unsigned int mask = 0;

	for(size_t i=0;i<size;i++)
	{
		configParser::UserType userType;
		GetUserType(i,userType);

		if(userType.GetIsPseudo())
			mask |= userType.GetMask();
	}

	return mask;
}

bool 
ConfigGameView::GetUserEmailAddress( TCHAR* email, size_t size ) const
{
	return ( m_CoreView.GetUserEmailAddress( email, size ) );
}

bool
ConfigGameView::SetUserEmailAddress( const TCHAR* email )
{
	return ( m_CoreView.SetUserEmailAddress( email ) );
}

bool 
ConfigGameView::GetUserFlags( unsigned int& flags ) const
{
	return ( m_CoreView.GetUserFlags( flags ) );
}

bool
ConfigGameView::SetUserFlags( unsigned int flags )
{
	return ( m_CoreView.SetUserFlags( flags ) );
}

bool 
ConfigGameView::GetStudio( TCHAR* studio, size_t size ) const
{
	return ( m_CoreView.GetStudio( studio, size ) );
}

bool 
ConfigGameView::GetUseXoreaxXGE( ) const
{
	return ( m_CoreView.GetUseXoreaxXGE() );
}

bool 
ConfigGameView::SetUseXoreaxXGE( bool value )
{
	return ( m_CoreView.SetUseXoreaxXGE( value ) );
}

int 
ConfigGameView::GetNumMaxVariants() const
{
	return 3;
}

bool
ConfigGameView::GetMaxVariantName( size_t index, TCHAR* variant, size_t size ) const
{
	switch(index)
	{
	case 1:
		_stprintf_s( variant, (int)size, _T("Debug") );
		return true;
	case 2:
		_stprintf_s( variant, (int)size, _T("Outsource") );
		return true;
	default:
		_stprintf_s( variant, (int)size, _T("Current") );
		return true;
	}
}

int 
ConfigGameView::GetMaxVariant() const
{
	TCHAR variant[256];
	configParser::ConfigModelData data;
	bool result = m_CoreView.query( _T("//local/applications/app[@name='max']/@toolset_mode"), data );

	if(result)
	{
		data.GetValue( variant, 256 );

		if(_tcsicmp(variant,_T("Debug")) == 0)
			return 1;
		else if(_tcsicmp(variant,_T("Outsource")) == 0)
			return 2;
	}

	return ( 0 );
}

bool ConfigGameView::SetMaxVariant( size_t /*index*/ )
{
	return true;
}

bool ConfigGameView::GetLogToStdOut( ) const
{
	return ( m_CoreView.GetLogToStdOut( ) );
}

bool 
ConfigGameView::SetLogToStdOut( bool value )
{
	return ( m_CoreView.SetLogToStdOut( value ) );
}

int 
ConfigGameView::GetLogLevel( ) const
{
	return ( m_CoreView.GetLogLevel( ) );
}

bool 
ConfigGameView::SetLogLevel( int level )
{
	return ( m_CoreView.SetLogLevel( level ) );
}

bool 
ConfigGameView::GetLogTraceInfo( ) const
{
	return ( m_CoreView.GetLogTraceInfo( ) );
}

bool 
ConfigGameView::SetLogTraceInfo( bool value )
{
	return ( m_CoreView.SetLogTraceInfo( value ) );
}

bool 
ConfigGameView::GetLogMailErrors( ) const
{
	return ( m_CoreView.GetLogMailErrors( ) );
}

bool 
ConfigGameView::SetLogMailErrors( bool value )
{
	return ( m_CoreView.SetLogMailErrors( value ) );
}

bool 
ConfigGameView::GetLogMailServerHost( TCHAR* server, size_t size ) const
{
	return ( m_CoreView.GetLogMailServerHost( server, size ) );
}

int 
ConfigGameView::GetLogMailServerPort( ) const
{
	return ( m_CoreView.GetLogMailServerPort() );
}

bool 
ConfigGameView::SetBranch( const TCHAR* branch_name )
{
	m_pEnv->pop( );

	errno_t res = _tcsncpy_s( m_sBranchKey, SM_SIZE, branch_name, _TRUNCATE );
	m_pEnv->push( );
	import_branch_environment( );

	bool result = (0 == res);

	if ( result )
	{
		TCHAR query[_MAX_PATH] = {0};
		_stprintf_s( query, _MAX_PATH, _T("//local/branches/@default") );
		result = m_CoreView.set( query, branch_name, false, ConfigCoreView::PROJECT, true );
	}

	return result;
}

bool
ConfigGameView::GetBranch( TCHAR* branch, size_t size ) const
{
	errno_t res = _tcsncpy_s( branch, size, m_sBranchKey, _TRUNCATE );

	return ( 0 == res );
}

size_t 
ConfigGameView::GetNumBranches( ) const
{
	return GetNodeCountForQuery(_T("//project/branches/branch/@name"));
}

bool 
ConfigGameView::GetBranchKey( size_t index, TCHAR* branch, size_t size ) const
{
	configParser::ConfigModelData data;
	bool result = m_CoreView.query( _T("//project/branches/branch/@name"), data );

	// DHM; this fixes a case where only a single branch is available.
	// Must change the way the underlying DOM is handled in TinyXML!
	if (ConfigModelData::CONFIGMODEL_DATA_STRING == data.GetType())
	{
		data.GetValue( branch, size );
	}
	else if (ConfigModelData::CONFIGMODEL_DATA_EMPTY == data.GetType())
	{
		configParser::ConfigModelData dataBranch = data[index];
		dataBranch.GetValue(branch, size);
	}
	return ( result );
}

bool
ConfigGameView::GetDefaultBranch( TCHAR* branch, size_t size ) const
{
	configParser::ConfigModelData data;
	bool result = m_CoreView.query( _T("//local/branches/@default"), data );
	
	data.GetValue( branch, size );
	return ( result );
}

size_t 
ConfigGameView::GetNumLabels() const
{
	return GetNodeCountForQuery(_T("//project/labels/label/@type"));
}

bool 
ConfigGameView::GetLabelTypeAt(size_t index, TCHAR* type, size_t type_size) const
{
	configParser::ConfigModelData data;

	bool result = m_CoreView.query( _T("//project/labels/label/@type"), data );

	if( result )
	{
		data[index].GetValue(type, type_size);
	}

	return result;
}

bool 
ConfigGameView::GetLabelNameAt(size_t index, TCHAR* name, size_t name_size) const
{
	configParser::ConfigModelData data;

	bool result = m_CoreView.query( _T("//project/labels/label/@name"), data );

	if( result )
	{
		data[index].GetValue(name, name_size);
	}

	return result;
}

bool 
ConfigGameView::GetLabelName(const TCHAR* type, TCHAR* name, size_t name_size)
{
	TCHAR query[_MAX_PATH] = {0};
	_stprintf_s( query, _MAX_PATH, _T("//project/labels/label[@type=\"%s\"]/@name"), type);

	bool queryTypesResult = m_CoreView.query(query, name, name_size );

	return queryTypesResult;
}

size_t 
ConfigGameView::GetNumTargets( const TCHAR* branch_name ) const
{
	const TCHAR* branch = branch_name;
	if(!branch)
		branch = m_sBranchKey;

	TCHAR query[_MAX_PATH] = {0};
	_stprintf_s( query, _MAX_PATH, _T("//project/branches/branch[@name=\"%s\"]/targets/target/@platform"), branch);

	return GetNodeCountForQuery(query);
}

bool 
ConfigGameView::GetTargetKey( size_t index, TCHAR* target, size_t size, const TCHAR* branch_name ) const
{
	const TCHAR* branch = branch_name;
	if(!branch)
		branch = m_sBranchKey;

	TCHAR query[_MAX_PATH] = {0};
	_stprintf_s( query, _MAX_PATH, _T("//project/branches/branch[@name=\"%s\"]/targets/target/@platform"), branch );

	configParser::ConfigModelData data;
	bool result = m_CoreView.query( query, data );
	configParser::ConfigModelData dataTarget = data[index];

	dataTarget.GetValue( target, size );
	return ( result );
}

bool 
ConfigGameView::GetTargetEnabled( const TCHAR* platform_name, const TCHAR* branch_name) const
{
	const TCHAR* branch = branch_name;
	if(!branch)
		branch = m_sBranchKey;

	const TCHAR* platform = platform_name;

	TCHAR query[_MAX_PATH] = {0};
	_stprintf_s( query, _MAX_PATH, _T("//local/branches/branch[@name=\"%s\"]/targets/target[@platform=\"%s\"]/@enabled"), branch, platform);

	configParser::ConfigModelData data;
	bool result = m_CoreView.query( query, data );

	if(result)
	{
		TCHAR value[256];
		data.GetValue( value, 256 );

		if(_tcscmp(value,_T("true")) != 0)
		{
			result = false;
		}
	}

	return ( result );
}

bool 
ConfigGameView::GetTargetDir( const TCHAR* platform_name, TCHAR* path, size_t size, const TCHAR* branch_name ) const
{
	const TCHAR* branch = branch_name;
	if(!branch)
		branch = m_sBranchKey;

	const TCHAR* platform = platform_name;

	TCHAR query[_MAX_PATH] = {0};
	_stprintf_s( query, _MAX_PATH, _T("//project/branches/branch[@name=\"%s\"]/targets/target[@platform=\"%s\"]/@path"), branch, platform);

	configParser::ConfigModelData data;
	bool result = m_CoreView.query( query, data );

	if ( result )
	{
		data.GetValue( path, size );
		m_pEnv->subst( path, size );
	}
	return ( result );
}

bool 
ConfigGameView::GetTargetRagebuilder( const TCHAR* branch_name, const TCHAR* platform_name, TCHAR* path, size_t size ) const
{
	const TCHAR* branch = branch_name;
	if(!branch)
		branch = m_sBranchKey;

	const TCHAR* platform = platform_name;

	TCHAR query[_MAX_PATH] = {0};
	_stprintf_s( query, _MAX_PATH, _T("//project/branches/branch[@name=\"%s\"]/rage/ragebuilders/ragebuilder[@platform=\"%s\"]/@exe"), branch, platform);

	configParser::ConfigModelData data;
	bool result = m_CoreView.query( query, data );

	if ( result )
	{
		data.GetValue( path, size );
		m_pEnv->subst( path, size );
	}

	return ( result );
}

bool 
ConfigGameView::GetTargetRagebuilderOptions( const TCHAR* branch_name, const TCHAR* platform_name, TCHAR* path, size_t size ) const
{
	const TCHAR* branch = branch_name;
	if(!branch)
		branch = m_sBranchKey;

	const TCHAR* platform = platform_name;

	TCHAR query[_MAX_PATH] = {0};
	_stprintf_s( query, _MAX_PATH, _T("//project/branches/branch[@name=\"%s\"]/rage/ragebuilders/ragebuilder[@platform=\"%s\"]/@options"), branch, platform);

	configParser::ConfigModelData data;
	bool result = m_CoreView.query( query, data );

	if ( result )
	{
		data.GetValue( path, size );
		m_pEnv->subst( path, size );
	}

	return ( result );
}

bool
ConfigGameView::SetNumTargets( size_t num, const TCHAR* branch )
{
	if(!branch)
		branch = m_sBranchKey;

	TCHAR query[_MAX_PATH] = {0};
	_stprintf_s( query, _MAX_PATH, _T("//local/branches/branch[@name=\"%s\"]"), branch );
	m_CoreView.remove( query, false, ConfigCoreView::PROJECT );

	for(size_t i=0;i<num;i++)
	{
		_stprintf_s( query, _MAX_PATH, _T("//local/branches/branch[@name=\"%s\"]/targets/target[%d]/@enabled"), branch, i );
		if(!m_CoreView.set( query, false, false, ConfigCoreView::PROJECT, true ))
			return false;
	}

	return ( true );
}

bool
ConfigGameView::SetTargetKey( size_t index, const TCHAR* target, const TCHAR* branch )
{
	if(!branch)
		branch = m_sBranchKey;

	index++;

	TCHAR query[_MAX_PATH] = {0};
	_stprintf_s( query, _MAX_PATH, _T("//local/branches/branch[@name=\"%s\"]/targets/target[%d]/@platform"), branch, index );
	bool result = m_CoreView.set( query, target, false, ConfigCoreView::PROJECT, true );

	return result;
}

bool 
ConfigGameView::SetTargetEnabled( const TCHAR* platform_name, bool enabled, const TCHAR* branch_name )
{
	const TCHAR* branch = branch_name;
	if(!branch)
		branch = m_sBranchKey;

	TCHAR query[_MAX_PATH] = {0};
	_stprintf_s( query, _MAX_PATH, _T("//local/branches/branch[@name=\"%s\"]/targets/target[@platform=\"%s\"]/@enabled"), branch, platform_name);

	configParser::ConfigModelData data;
	bool result = m_CoreView.set( query, enabled, false, ConfigCoreView::PROJECT, true );

	return ( result );
}

bool 
ConfigGameView::SetTargetEnabled( size_t index, bool enabled, const TCHAR* branch)
{
	if(!branch)
		branch = m_sBranchKey;

	index++;

	TCHAR query[_MAX_PATH] = {0};
	_stprintf_s( query, _MAX_PATH, _T("//local/branches/branch[@name=\"%s\"]/targets/target[%d]/@enabled"), branch, index );
	bool result = m_CoreView.set( query, enabled, false, ConfigCoreView::PROJECT, true );
	
	return result;
}

size_t
ConfigGameView::GetNumDlcProjects( ) const
{
	return GetNodeCountForQuery(_T("//config/projects/dlc"));
}

bool 
ConfigGameView::GetDlcProjectName( size_t index, TCHAR* name, size_t size ) const
{
	TCHAR query[_MAX_PATH] = {0};
	_stprintf_s( query, _MAX_PATH, _T("//config/projects/dlc[%d]/@name"), (index+1) );

	bool queryTypesResult = m_CoreView.query(query, name, size );

	return queryTypesResult;
}

bool 
ConfigGameView::GetDlcProjectRoot( size_t index, TCHAR* name, size_t size ) const
{
	TCHAR query[_MAX_PATH] = {0};
	_stprintf_s( query, _MAX_PATH, _T("//config/projects/dlc[%d]/@root"), (index+1) );

	bool queryTypesResult = m_CoreView.query(query, name, size );

	return queryTypesResult;
}

bool 
	ConfigGameView::GetDlcProjectConfig( size_t index, TCHAR* name, size_t size ) const
{
	TCHAR query[_MAX_PATH] = {0};
	_stprintf_s( query, _MAX_PATH, _T("//config/projects/dlc[%d]/@config"), (index+1) );

	bool queryTypesResult = m_CoreView.query(query, name, size );

	return queryTypesResult;
}

bool
	ConfigGameView::GetDlcProjectExport(const TCHAR* name, TCHAR* exportPath, size_t size) const
{
	std::map<STL_STRING, STL_STRING>::const_iterator itr = m_DlcMap.find(name);
	if(itr != m_DlcMap.end())
	{
		TCHAR branch[MAX_PATH];
		if(GetBranch(branch, MAX_PATH))
		{
			// We query the dlc based on the core branch name
			TCHAR query[_MAX_PATH] = {0};
			_stprintf_s( query, _MAX_PATH, _T("//project//branches/branch[@name=\"%s\"]/@export"), branch );

			TCHAR _export[MAX_PATH];
			if(!m_CoreView.GetConfigData(itr->second.c_str(), query, _export, MAX_PATH))
			{
				// If this fails it means there is no branch for this config so we drop to the default
				if(!m_CoreView.GetConfigData(itr->second.c_str(), _T("//project//branches/@default"), branch, MAX_PATH))
				{
					return false;
				}
				TCHAR default_query[_MAX_PATH] = {0};
				_stprintf_s( default_query, _MAX_PATH, _T("//project//branches/branch[@name=\"%s\"]/@export"), branch );
				if(!m_CoreView.GetConfigData(itr->second.c_str(), default_query, _export, MAX_PATH))
				{
					return false;
				}
			}

			STL_STRING str(_export);
			boost::replace_all(str, _T("$(branch)"), branch);

#if UNICODE
			wcsncpy_s( exportPath, size, str.c_str(), _TRUNCATE );
#else
			strncpy_s( exportPath, size, str.c_str(), _TRUNCATE );
#endif // UNICODE
			return true;
		}
	}

	return false;
}

bool
	ConfigGameView::GetDlcProjectAssets(const TCHAR* name, TCHAR* assets, size_t size) const
{
	std::map<STL_STRING, STL_STRING>::const_iterator itr = m_DlcMap.find(name);
	if(itr != m_DlcMap.end())
	{
		TCHAR branch[MAX_PATH];
		if(GetBranch(branch, MAX_PATH))
		{
			// We query the dlc based on the core branch name
			TCHAR query[_MAX_PATH] = {0};
			_stprintf_s( query, _MAX_PATH, _T("//project//branches/branch[@name=\"%s\"]/@assets"), branch );

			TCHAR _assets[MAX_PATH];
			if(!m_CoreView.GetConfigData(itr->second.c_str(), query, _assets, MAX_PATH))
			{
				// If this fails it means there is no branch for this config so we drop to the default
				if(!m_CoreView.GetConfigData(itr->second.c_str(), _T("//project//branches/@default"), branch, MAX_PATH))
				{
					return false;
				}
				TCHAR default_query[_MAX_PATH] = {0};
				_stprintf_s( default_query, _MAX_PATH, _T("//project//branches/branch[@name=\"%s\"]/@assets"), branch );
				if(!m_CoreView.GetConfigData(itr->second.c_str(), default_query, _assets, MAX_PATH))
				{
					return false;
				}
			}

			STL_STRING str(_assets);
			boost::replace_all(str, _T("$(branch)"), branch);

#if UNICODE
			wcsncpy_s( assets, size, str.c_str(), _TRUNCATE );
#else
			strncpy_s( assets, size, str.c_str(), _TRUNCATE );
#endif // UNICODE
			return true;
		}
	}

	return false;
}

bool
	ConfigGameView::GetDlcProjectArt(const TCHAR* name, TCHAR* assets, size_t size) const
{
	std::map<STL_STRING, STL_STRING>::const_iterator itr = m_DlcMap.find(name);
	if(itr != m_DlcMap.end())
	{
		TCHAR branch[MAX_PATH];
		if(GetBranch(branch, MAX_PATH))
		{
			// We query the dlc based on the core branch name
			TCHAR query[_MAX_PATH] = {0};
			_stprintf_s( query, _MAX_PATH, _T("//project//branches/branch[@name=\"%s\"]/@art"), branch );

			TCHAR _art[MAX_PATH];
			if(!m_CoreView.GetConfigData(itr->second.c_str(), query, _art, MAX_PATH))
			{
				// If this fails it means there is no branch for this config so we drop to the default
				if(!m_CoreView.GetConfigData(itr->second.c_str(), _T("//project//branches/@default"), branch, MAX_PATH))
				{
					return false;
				}
				TCHAR default_query[_MAX_PATH] = {0};
				_stprintf_s( default_query, _MAX_PATH, _T("//project//branches/branch[@name=\"%s\"]/@art"), branch );
				if(!m_CoreView.GetConfigData(itr->second.c_str(), default_query, _art, MAX_PATH))
				{
					return false;
				}
			}

			STL_STRING str(_art);
			boost::replace_all(str, _T("$(branch)"), branch);

#if UNICODE
			wcsncpy_s( assets, size, str.c_str(), _TRUNCATE );
#else
			strncpy_s( assets, size, str.c_str(), _TRUNCATE );
#endif // UNICODE
			return true;
		}
	}

	return false;
}

bool 
ConfigGameView::GetPerforceIntegration( ) const
{
	configParser::ConfigModelData data;
	bool result = m_CoreView.query( _T("//local/scm/@toolchain_integration"), data );
	
	if ( result )
	{
		TCHAR value[256] = {0};
		data.GetValue( value, 256 );

		if ( _tcscmp(value,_T("true")) != 0)
		{
			result = false;
		}
	}
	else
	{
		// Default is to say Perforce integration is enabled.
		result = true;
	}

	return ( result );
}

bool 
ConfigGameView::SetPerforceIntegration( bool enabled )
{
	bool result = m_CoreView.set( _T("//local/scm/@toolchain_integration"), enabled, false, ConfigCoreView::GLOBAL, true );
	return ( result );
}

size_t
ConfigGameView::GetNumPerforces( ) const
{
	return GetNodeCountForQuery(_T("/local/scm/*"));
}

void
ConfigGameView::SetNumPerforces( size_t /*num*/ )
{
}

bool
ConfigGameView::GetPerforceInfo( size_t index, PerforceInfo& perforce ) const
{
	configParser::ConfigModelData data;
	TCHAR queryStr[_MAX_PATH] = {0};
	TCHAR value[_MAX_PATH] = {0};

	_stprintf_s( queryStr, _MAX_PATH, _T("//local/scm/*") );
	if(m_CoreView.query_global( queryStr, data ))
	{
		data[index].GetValue( value, _MAX_PATH );
		perforce.SetName(value);
	}

	index++;

	_stprintf_s(queryStr, _MAX_PATH, _T("//local/scm/*[%d]/@workspace"), index );
	if(m_CoreView.query_global( queryStr, data ))
	{
		data.GetValue( value, _MAX_PATH );
		perforce.SetWorkspace(value);
	}

	_stprintf_s(queryStr, _MAX_PATH, _T("//local/scm/*[%d]/@username"), index );
	if(m_CoreView.query_global( queryStr, data ))
	{
		data.GetValue( value, _MAX_PATH );
		perforce.SetUsername(value);
	}

	_stprintf_s(queryStr, _MAX_PATH, _T("//local/scm/*[%d]/@server"), index );
	if(m_CoreView.query_global( queryStr, data ))
	{
		data.GetValue( value, _MAX_PATH );
		perforce.SetServer(value);
	}

	return true;
}

bool
ConfigGameView::SetPerforceInfo( size_t /*index*/, const PerforceInfo& perforce )
{
	configParser::ConfigModelData data;
	TCHAR queryStr[_MAX_PATH] = {0};

	_stprintf_s(queryStr, _MAX_PATH, _T("//local/scm/%s/@workspace"), perforce.GetName() );
	if(m_CoreView.set( queryStr, perforce.GetWorkspace(), false, ConfigCoreView::GLOBAL, true ))

	_stprintf_s(queryStr, _MAX_PATH, _T("//local/scm/%s/@username"), perforce.GetName() );
	if(m_CoreView.set( queryStr, perforce.GetUsername(), false, ConfigCoreView::GLOBAL, true ))

	_stprintf_s(queryStr, _MAX_PATH, _T("//local/scm/%s/@server"), perforce.GetName() );
	m_CoreView.set( queryStr, perforce.GetServer(), false, ConfigCoreView::GLOBAL, true );

	return true;
}

size_t
ConfigGameView::GetNumReportCategories( ) const
{
	return GetNodeCountForQuery(_T("//project/reports/category"));
}

bool 
ConfigGameView::GetReportCategoryName( size_t index, TCHAR* name, size_t size ) const
{
	TCHAR query[_MAX_PATH] = {0};
	_stprintf_s( query, _MAX_PATH, _T("//project/reports/category[%d]/@name"), (index+1) );

	bool queryTypesResult = m_CoreView.query(query, name, size );

	return queryTypesResult;
}

size_t 
ConfigGameView::GetReportCategoryNumReports( size_t index ) const
{
	TCHAR query[_MAX_PATH] = {0};
	_stprintf_s( query, _MAX_PATH, _T("//project/reports/category[%d]/report"), (index+1) );
	return GetNodeCountForQuery(query);
}

bool 
ConfigGameView::GetReportCategoryReportName( size_t categoryIndex, size_t reportIndex, TCHAR* name, size_t size ) const
{
	TCHAR query[_MAX_PATH] = {0};
	_stprintf_s( query, _MAX_PATH, _T("//project/reports/category[%d]/report[%d]/@name"), 
		(categoryIndex+1), (reportIndex+1) );

	bool queryTypesResult = m_CoreView.query(query, name, size );

	return queryTypesResult;

}

bool 
ConfigGameView::GetReportCategoryReportUrl( size_t categoryIndex, size_t reportIndex, TCHAR* url, size_t size ) const
{
	TCHAR query[_MAX_PATH] = {0};
	_stprintf_s( query, _MAX_PATH, _T("//project/reports/category[%d]/report[%d]/@url"), 
		(categoryIndex+1), (reportIndex+1) );

	bool queryTypesResult = m_CoreView.query(query, url, size );

	return queryTypesResult;
}

bool 
ConfigGameView::GetProjectName( TCHAR* name, size_t size ) const
{
	GET_PROJECT_ATTR( _T("name"), name, size );
}

bool 
ConfigGameView::GetProjectUIName( TCHAR* name, size_t size ) const
{
	TCHAR expr[LG_SIZE];
	_stprintf_s( expr, LG_SIZE, _T("//config/projects/project[@name='%s']/@uiname"), 
		m_sProjectKey );
	return ( query( expr, name, size ) );
}

bool 
ConfigGameView::GetProjectRootDir( TCHAR* path, size_t size ) const
{
	TCHAR expr[LG_SIZE];
	_stprintf_s( expr, LG_SIZE, _T("//config/projects/project[@name='%s']/@root"), 
		m_sProjectKey );
	return ( query( expr, path, size ) );
}

bool 
ConfigGameView::HasLevels( ) const
{
	bool has_levels = false;
	query( _T("//project/@has_levels"), has_levels );
	return ( has_levels );
}

bool 
ConfigGameView::IsEpisodic( ) const
{
	bool is_episodic = false;
	query( _T("//project/@is_episodic"), is_episodic );
	return ( is_episodic );
}

bool 
ConfigGameView::GetCacheDir( TCHAR* path, size_t size ) const
{
	GET_PROJECT_PATH_ATTR( _T("cache"), path, size );
}

bool 
ConfigGameView::GetNetworkDir( TCHAR* path, size_t size ) const
{
	GET_PROJECT_PATH_ATTR( _T("network"), path, size );
}

bool 
ConfigGameView::GetNetworkTextureDir( TCHAR* path, size_t size ) const
{
	GET_PROJECT_PATH_ATTR( _T("nettexture"), path, size );
}

bool 
ConfigGameView::GetNetworkStreamDir( TCHAR* path, size_t size ) const
{
	GET_PROJECT_PATH_ATTR( _T("netstream"), path, size );
}

bool 
ConfigGameView::GetNetworkGenericStreamDir( TCHAR* path, size_t size ) const
{
	GET_PROJECT_PATH_ATTR( _T("netgenstream"), path, size );
}

bool 
ConfigGameView::GetBuildDir( TCHAR* path, size_t size ) const
{
	return ( get_project_branch_path_attr( _T("build"), path, size ) );
}

bool 
ConfigGameView::GetMetadataDir( TCHAR* path, size_t size ) const
{
	return ( get_project_branch_path_attr( _T( "metadata" ), path, size ) );
}

bool 
ConfigGameView::GetDefinitionsDir( TCHAR* path, size_t size ) const
{
	return ( get_project_branch_path_attr( _T( "definitions" ), path, size ) );
}

bool 
ConfigGameView::GetExportDir( TCHAR* path, size_t size ) const
{
	return ( get_project_branch_path_attr( _T( "export"), path, size ) );
}

bool 
ConfigGameView::GetProcessedDir( TCHAR* path, size_t size ) const
{
	return ( get_project_branch_path_attr( _T( "processed"), path, size ) );
}

bool 
ConfigGameView::GetCommonDir( TCHAR* path, size_t size ) const
{
	return ( get_project_branch_path_attr( _T("common"), path, size ) );
}

bool 
ConfigGameView::GetShaderDir( TCHAR* path, size_t size ) const
{
	return ( get_project_branch_path_attr( _T("shaders"), path, size ) );
}

bool 
ConfigGameView::GetSourceDir( TCHAR* path, size_t size ) const
{
	return ( get_project_branch_path_attr( _T("code"), path, size ) );
}

bool 
ConfigGameView::GetRageSourceDir( TCHAR* path, size_t size ) const
{
	return ( get_project_branch_path_attr( _T("ragecode"), path, size ) );
}

bool 
ConfigGameView::GetScriptDir( TCHAR* path, size_t size ) const
{
	return ( get_project_branch_path_attr( _T("script"), path, size ) );
}

bool 
ConfigGameView::GetArtDir( TCHAR* path, size_t size ) const
{
	return ( get_project_branch_path_attr( _T("art"), path, size ) );
}

bool 
ConfigGameView::GetAssetsDir( TCHAR* path, size_t size ) const
{
	return ( get_project_branch_path_attr( _T("assets"), path, size ) );
}

bool 
ConfigGameView::GetPreviewDir( TCHAR* path, size_t size ) const
{
	return ( get_project_branch_path_attr( _T("preview"), path, size ) );
}

bool 
ConfigGameView::GetAudioDir( TCHAR* path, size_t size ) const
{
	return ( get_project_branch_path_attr( _T("audio"), path, size ) );
}

bool 
ConfigGameView::GetTuneFileDir( TCHAR* path, size_t size ) const
{
	return ( get_project_branch_path_attr( _T("tune"), path, size ) );
}

bool
ConfigGameView::GetTextDir( TCHAR* path, size_t size ) const
{
	return ( get_project_branch_path_attr( _T("text"), path, size ) );
}

bool 
ConfigGameView::GetMapTexDir( TCHAR* path, size_t size ) const
{
	return ( get_project_branch_path_attr( _T( "maptexdir" ), path, size ) );
}

bool 
ConfigGameView::query( const TCHAR* expr, TCHAR* data, size_t size ) const
{
	return ( m_CoreView.query( expr, data, size ) );
}

bool 
ConfigGameView::query( const TCHAR* expr, bool& data ) const
{
	return ( m_CoreView.query( expr, data ) );
}

bool 
ConfigGameView::query( const TCHAR* expr, int& data ) const
{
	return ( m_CoreView.query( expr, data ) );
}

bool 
ConfigGameView::query( const TCHAR* expr, double& data ) const
{
	return ( m_CoreView.query( expr, data ) );
}

bool
ConfigGameView::Save()
{
	return m_CoreView.Save();
}

void 
ConfigGameView::init( bool bLoadContent )
{
	m_pEnv = new Environment;
	m_pEnv->import_core_globals( );
	m_pEnv->import_secondary_globals( );

	query( _T("//project/@name"), m_sProjectKey, SM_SIZE );

	if ( !query( _T("//branches/@default"), m_sBranchKey, SM_SIZE ) )
	{
		_tcsncpy_s( m_sBranchKey, SM_SIZE, _T("dev"), _TRUNCATE );
	}

	import_project_environment( );
	m_pEnv->push( );
	import_branch_environment( );

	PopulateDlcMapData();

	// Initialise the content model (if requested).
	if ( bLoadContent )
		ReloadContentModel( );
}

void 
ConfigGameView::import_project_environment( )
{
	TCHAR varval[LG_SIZE] = {0};
	
	if ( GetProjectName( varval, LG_SIZE ) )
		m_pEnv->add( _T("name"), varval );
	if ( GetNetworkDir( varval, LG_SIZE ) )
		m_pEnv->add( _T("network"), varval );
	if ( GetNetworkStreamDir( varval, LG_SIZE ) )
		m_pEnv->add( _T("netstream"), varval );
	if ( GetNetworkGenericStreamDir( varval, LG_SIZE ) )
		m_pEnv->add( _T("netgenstream"), varval );
	if ( GetProjectRootDir( varval, LG_SIZE ) )
		m_pEnv->add( _T("root"), varval );
	if ( GetBuildDir( varval, LG_SIZE ) )
		m_pEnv->add( _T("build"), varval );
	if ( GetCommonDir( varval, LG_SIZE ) )
		m_pEnv->add( _T("common"), varval );
	if ( GetExportDir( varval, LG_SIZE ) )
		m_pEnv->add( _T("export"), varval );
	if ( GetProcessedDir( varval, LG_SIZE ) )
		m_pEnv->add( _T("processed"), varval );
	if ( GetMetadataDir( varval, LG_SIZE ) )
		m_pEnv->add( _T("metadata"), varval );
	if ( GetDefinitionsDir( varval, LG_SIZE ) )
		m_pEnv->add( _T("definitions"), varval );
}

void
ConfigGameView::import_branch_environment( )
{
	TCHAR varval[LG_SIZE] = {0};

	if ( get_project_branch_attr_raw( _T("name"), varval, LG_SIZE ) )
	{
		m_pEnv->subst( varval, LG_SIZE );
		m_pEnv->add( _T("branch"), varval );
	}
	if ( get_project_branch_attr_raw( _T("build"), varval, LG_SIZE ) )
	{
		m_pEnv->subst( varval, LG_SIZE );
		m_pEnv->add( _T("build"), varval );
	}
	if ( get_project_branch_attr_raw( _T("common"), varval, LG_SIZE ) )
	{
		m_pEnv->subst( varval, LG_SIZE );
		m_pEnv->add( _T("common"), varval );
	}
	if ( get_project_branch_attr_raw( _T("shaders"), varval, LG_SIZE ) )
	{
		m_pEnv->subst( varval, LG_SIZE );
		m_pEnv->add( _T("shaders"), varval );
	}
	if ( get_project_branch_attr_raw( _T("code"), varval, LG_SIZE ) )
	{
		m_pEnv->subst( varval, LG_SIZE );
		m_pEnv->add( _T("code"), varval );
	}
	if ( get_project_branch_attr_raw( _T("art"), varval, LG_SIZE ) )
	{
		m_pEnv->subst( varval, LG_SIZE );
		m_pEnv->add( _T("art"), varval );
	}
	if ( get_project_branch_attr_raw( _T("script"), varval, LG_SIZE ) )
	{
		m_pEnv->subst( varval, LG_SIZE );
		m_pEnv->add( _T("script"), varval );
	}
	if ( get_project_branch_attr_raw( _T("preview"), varval, LG_SIZE ) )
	{
		m_pEnv->subst( varval, LG_SIZE );
		m_pEnv->add( _T("preview"), varval );
	}
	if ( get_project_branch_attr_raw( _T("audio"), varval, LG_SIZE ) )
	{
		m_pEnv->subst( varval, LG_SIZE );
		m_pEnv->add( _T("audio"), varval );
	}
	if ( get_project_branch_attr_raw( _T("assets"), varval, LG_SIZE ) )
	{
		m_pEnv->subst( varval, LG_SIZE );
		m_pEnv->add( _T("assets"), varval );
	}
	if ( get_project_branch_attr_raw( _T("export"), varval, LG_SIZE ) )
	{
		m_pEnv->subst( varval, LG_SIZE );
		m_pEnv->add( _T("export"), varval );
	}
	if ( get_project_branch_attr_raw( _T("processed"), varval, LG_SIZE ) )
	{
		m_pEnv->subst( varval, LG_SIZE );
		m_pEnv->add( _T("processed"), varval );
	}
}

// Convienience macro to query a project branch root node attribute (current branch).
bool 
ConfigGameView::get_project_branch_attr( const TCHAR* attr, TCHAR* out, size_t size ) const
{
	TCHAR expr[LG_SIZE] = {0};
#ifdef UNICODE
	swprintf_s( expr, LG_SIZE, _T("//project/branches/branch[@name='%s']/@%s"),
		m_sBranchKey, attr );
#else
	sprintf_s( expr, LG_SIZE, _T("//project/branches/branch[@name='%s']/@%s"),
		m_sBranchKey, attr );
#endif
	bool result = ( query( expr, out, size ) ); 
	m_pEnv->subst( out, size ); 
	return result;
}

bool 
ConfigGameView::get_project_branch_attr_raw( const TCHAR* attr, TCHAR* out, size_t size ) const
{
	TCHAR expr[LG_SIZE] = {0};
#ifdef UNICODE
	swprintf_s( expr, LG_SIZE, _T("//project/branches/branch[@name='%s']/@%s"),
		m_sBranchKey, attr );
#else
	sprintf_s( expr, LG_SIZE, _T("//project/branches/branch[@name='%s']/@%s"),
		m_sBranchKey, attr );
#endif
	bool result = ( query( expr, out, size ) ); 
	return result;
}

// Convenience macro to query a project branch root node attribute (current branch)
// assuming its a path and normalising it for the current platform.
bool 
ConfigGameView::get_project_branch_path_attr( const TCHAR* attr, TCHAR* out, size_t size ) const
{
	TCHAR expr[LG_SIZE] = {0};

#ifdef UNICODE
	swprintf_s( expr, LG_SIZE, _T("//project/branches/branch[@name='%s']/@%s"),
		m_sBranchKey, attr );
#else
	sprintf_s( expr, LG_SIZE, _T("//project/branches/branch[@name='%s']/@%s"),
		m_sBranchKey, attr );
#endif
	bool result = ( query( expr, out, size ) ); 
	m_pEnv->subst( out, size ); 
	return result;
}

size_t
ConfigGameView::GetNodeCountForQuery(const TCHAR* expr) const
{
	configParser::ConfigModelData data;
	bool result = m_CoreView.query( expr, data );

	if (!result || (data.IsEmpty() && !data.HasChildren()))
	{
		return 0L;
	}
	else
	{
		return (data.HasChildren() ? data.ChildCount() : 1);
	}
}

void
ConfigGameView::PopulateDlcMapData()
{
	for(size_t i=0; i < GetNumDlcProjects(); ++i)
	{
		TCHAR cName[MAX_PATH];
		GetDlcProjectName(i, cName, MAX_PATH);

		TCHAR cConfig[MAX_PATH];
		GetDlcProjectConfig(i, cConfig, MAX_PATH);

		TCHAR cRoot[MAX_PATH];
		GetDlcProjectRoot(i, cRoot, MAX_PATH);

		STL_STRING strConfig(cConfig);
		boost::replace_all(strConfig, _T("$(root)"), cRoot);
		boost::replace_all(strConfig, _T("$(name)"), cName);

		STL_STRING strName(cName);
		boost::algorithm::to_lower(strName);

		m_DlcMap.insert(std::pair<STL_STRING, STL_STRING>(strName, strConfig));
	}
}

END_CONFIGPARSER_NS
