#ifndef __CONFIGPARSER_CONTENTNODEDEPENDENCYDIRECTORY_H__
#define __CONFIGPARSER_CONTENTNODEDEPENDENCYDIRECTORY_H__

//
// filename:	contentNodeDependencyDirectory.h
// author:		Jonny Rivers <jonathan.rivers@rockstarnorth.com>
// date:		18 November 2011
// description:	Content-tree node dependency directory class.
//

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

// --- Includes -----------------------------------------------------------------

// Local headers
#include "contentNode.h"

// STL headers
//#include <string>
//#include <vector>

// --- Classes ------------------------------------------------------------------

BEGIN_CONFIGPARSER_NS

//class ContentNodeGroup;

//PURPOSE
//
class ContentNodeDependencyDirectory : public ContentNode
{
public:
	ContentNodeDependencyDirectory( ContentNode* parent, 
		const configUtil::Environment* pEnv,
		const TCHAR* name,
		const TCHAR* directory,
		const TCHAR* type = _T("dependency_directory") );
	ContentNodeDependencyDirectory( ContentNode* parent, 
		const configUtil::Environment* pEnv, 
		xmlElementPtr pElement );
	virtual ~ContentNodeDependencyDirectory( );
 
	void GetDirectory( TCHAR* filename, size_t size ) const;
	bool IsOptional() const;

	//PURPOSE
	// Return xmlElementPtr representing this content node.
	virtual xmlNodePtr			ToXml() const;

	// Static Methods
	static const char*			GetXmlType( ) { return ( "dependency_directory" ); }

protected:
	// Hide copy constructor and assignment operator.
	ContentNodeDependencyDirectory( const ContentNodeDependencyDirectory& copy );
	ContentNodeDependencyDirectory& operator=( ContentNodeDependencyDirectory& copy );

	TCHAR m_sDirectory[_MAX_PATH];
	bool m_bOptional;
};

END_CONFIGPARSER_NS

#endif // __CONFIGPARSER_CONTENTNODEDEPENDENCYDIRECTORY_H__
