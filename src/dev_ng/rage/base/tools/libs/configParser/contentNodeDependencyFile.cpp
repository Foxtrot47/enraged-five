//
// filename:	contentNodeDependencyFile.cpp
// author:		Jonny Rivers <jonathan.rivers@rockstarnorth.com>
// date:		29 November 2011
// description:	Content-tree node dependency file class.
//

// LibXml2 headers
#include "libxml/parser.h"
#include "libxml/xpath.h"

// Local headers
#include "contentNodeDependencyFile.h"

BEGIN_CONFIGPARSER_NS

// --- ContentNodeFile ------------------------------------------------------

ContentNodeDependencyFile::ContentNodeDependencyFile( ContentNode* parent,
													  const configUtil::Environment* pEnv,
													  const TCHAR* name, 
													  const TCHAR* pathname, 
													  const TCHAR* type )
	: ContentNode( parent, pEnv, name, type )
{
	memset( m_sPathname, 0, sizeof( TCHAR ) * _MAX_PATH );
	m_bOptional = false;

#ifdef UNICODE
	wcsncpy_s( m_sPathname, _MAX_PATH, pathname, _TRUNCATE );
#else
	strncpy_s( m_sPathname, _MAX_PATH, pathname, _TRUNCATE );
#endif // UNICODE
}

ContentNodeDependencyFile::ContentNodeDependencyFile( ContentNode* parent,
													  const configUtil::Environment* pEnv, 
													  xmlElement* pElement )
	: ContentNode( parent, pEnv, pElement )
{
	memset( m_sPathname, 0, sizeof( TCHAR ) * _MAX_PATH );
	m_bOptional = false;

	xmlAttributePtr pAttribute = pElement->attributes;
	while ( pAttribute )
	{
		if ( 0 == xmlStrcasecmp( BAD_CAST "path", pAttribute->name ) )
		{
			configUtil::AnsiToTChar( m_sPathname, _MAX_PATH, (char*)pAttribute->children->content );
		}
		else if ( 0 == xmlStrcasecmp( BAD_CAST "optional", pAttribute->name ) )
		{
			m_bOptional = (0 == xmlStrcasecmp( BAD_CAST "true", pAttribute->children->content ) );
		}

		// Next attribute.
		pAttribute = (xmlAttributePtr)( pAttribute->next );
	}
}

ContentNodeDependencyFile::~ContentNodeDependencyFile( )
{
}

void ContentNodeDependencyFile::GetPathname( TCHAR* path, size_t size ) const
{
#ifdef UNICODE
	wcsncpy_s( path, size, m_sPathname, _TRUNCATE );
#else
	strcpy_s( path, size, m_sPathname );
#endif // UNICODE
	m_pEnv->subst( path, size );
}

bool ContentNodeDependencyFile::IsOptional() const
{
	return m_bOptional;
}

xmlNodePtr ContentNodeDependencyFile::ToXml( ) const
{
	xmlNodePtr pElement = ContentNode::ToXml( );
	char ansiPathname[_MAX_PATH] = {0};
	configUtil::TCharToAnsi( ansiPathname, _MAX_PATH, m_sPathname );
	size_t lenPathname = strlen( ansiPathname );

	if ( lenPathname > 0 )
		xmlNewProp( pElement, BAD_CAST "path", BAD_CAST ansiPathname );

	if (m_bOptional)
		xmlNewProp( pElement, BAD_CAST "optional", BAD_CAST "true" );

	return ( pElement );
}

END_CONFIGPARSER_NS
