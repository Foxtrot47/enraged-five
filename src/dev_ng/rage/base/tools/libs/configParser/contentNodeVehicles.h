#ifndef __CONFIGPARSER_CONTENTNODEVEHICLES_H__
#define __CONFIGPARSER_CONTENTNODEVEHICLES_H__

//
// filename:	contentNodeVehiclesRpf.h
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		15 June 2010
// description:	Content-tree Vehicles RPF node class.
//

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

// --- Includes -----------------------------------------------------------------

// Local headers
#include "contentNodeCore.h"

// --- Classes ------------------------------------------------------------------

BEGIN_CONFIGPARSER_NS

//PURPOSE
//
class ContentNodeVehiclesRpf : public ContentNodeRpf
{
public:
	ContentNodeVehiclesRpf( ContentNode* parent, 
		const configUtil::Environment* pEnv,
		const TCHAR* name, 
		const TCHAR* path, 
		Platform platform, 
		const TCHAR* sharedtxd,
		const TCHAR* sharedtxdinput,
		const TCHAR* friendly );
	ContentNodeVehiclesRpf( ContentNode* parent, 
		const configUtil::Environment* pEnv, 
		xmlElementPtr pElement );
	virtual ~ContentNodeVehiclesRpf( );

	const TCHAR*	GetRawSharedTxd( ) const { return m_sSharedTXD; }
	void			GetSharedTxd( TCHAR* path, size_t size ) const;

	const TCHAR*	GetRawSharedTxdInput( ) const { return m_sSharedTXDInput; }
	void			GetSharedTxdInput( TCHAR* path, size_t size ) const;

	const TCHAR*	GetRawFriendlyName( ) const { return m_sFriendlyName; }
	void			GetFriendlyName( TCHAR* name, size_t size ) const;

	// Resolved Environment
#if 0
	const STL_STRING GetSharedTxdInputResolved( ) const;
#endif	
	// Static Methods
	static const char* GetXmlType( ) { return ( "vehiclesrpf" ); }

protected:
	// Hide copy constructor and assignment operator.
	ContentNodeVehiclesRpf( const ContentNodeVehiclesRpf& copy );
	ContentNodeVehiclesRpf& operator=( ContentNodeVehiclesRpf& copy );

	TCHAR m_sSharedTXD[SM_SIZE];
	TCHAR m_sSharedTXDInput[_MAX_PATH];
	TCHAR m_sFriendlyName[_MAX_PATH];
};


//PURPOSE
//
class ContentNodeVehiclesZip : public ContentNodeZip
{
public:
	ContentNodeVehiclesZip( ContentNode* parent, 
		const configUtil::Environment* pEnv,
		const TCHAR* name, 
		const TCHAR* path, 
		Platform platform, 
		const TCHAR* sharedtxd,
		const TCHAR* sharedtxdinput,
		const TCHAR* friendly );
	ContentNodeVehiclesZip( ContentNode* parent, 
		const configUtil::Environment* pEnv, 
		xmlElementPtr pElement );
	virtual ~ContentNodeVehiclesZip( );

	const TCHAR*	GetRawSharedTxd( ) const { return m_sSharedTXD; }
	void			GetSharedTxd( TCHAR* path, size_t size ) const;

	const TCHAR*	GetRawSharedTxdInput( ) const { return m_sSharedTXDInput; }
	void			GetSharedTxdInput( TCHAR* path, size_t size ) const;

	const TCHAR*	GetRawFriendlyName( ) const { return m_sFriendlyName; }
	void			GetFriendlyName( TCHAR* name, size_t size ) const;

	// Resolved Environment
#if 0
	const STL_STRING GetSharedTxdInputResolved( ) const;
#endif	
	// Static Methods
	static const char* GetXmlType( ) { return ( "vehicleszip" ); }

protected:
	// Hide copy constructor and assignment operator.
	ContentNodeVehiclesZip( const ContentNodeVehiclesZip& copy );
	ContentNodeVehiclesZip& operator=( ContentNodeVehiclesZip& copy );

	TCHAR m_sSharedTXD[SM_SIZE];
	TCHAR m_sSharedTXDInput[_MAX_PATH];
	TCHAR m_sFriendlyName[_MAX_PATH];
};


//PURPOSE
//
class ContentNodeVehiclesDirectory : public ContentNodeDirectory
{
public:
	ContentNodeVehiclesDirectory( ContentNode* parent, 
		const configUtil::Environment* pEnv,
		const TCHAR* name, 
		const TCHAR* sharedtxd,
		const TCHAR* sharedtxdinput,
		const TCHAR* friendly );
	ContentNodeVehiclesDirectory( ContentNode* parent, 
		const configUtil::Environment* pEnv, 
		xmlElementPtr pElement );
	virtual ~ContentNodeVehiclesDirectory( );

	const TCHAR*	GetRawSharedTxd( ) const { return m_sSharedTXD; }
	void			GetSharedTxd( TCHAR* path, size_t size ) const;

	const TCHAR*	GetRawSharedTxdInput( ) const { return m_sSharedTXDInput; }
	void			GetSharedTxdInput( TCHAR* path, size_t size ) const;

	const TCHAR*	GetRawFriendlyName( ) const { return m_sFriendlyName; }
	void			GetFriendlyName( TCHAR* name, size_t size ) const;

	// Static Methods
	static const char* GetXmlType( ) { return ( "vehiclesdirectory" ); }

protected:
	// Hide copy constructor and assignment operator.
	ContentNodeVehiclesDirectory( const ContentNodeVehiclesZip& copy );
	ContentNodeVehiclesDirectory& operator=( ContentNodeVehiclesZip& copy );

	TCHAR m_sSharedTXD[SM_SIZE];
	TCHAR m_sSharedTXDInput[_MAX_PATH];
	TCHAR m_sFriendlyName[_MAX_PATH];
};


//PURPOSE
//
// Note: ContentNodeGroup functionality re-implemented.
//
class ContentNodeVehicleModZip : public ContentNodeZip
{
public:
	ContentNodeVehicleModZip( ContentNode* parent, 
		const configUtil::Environment* pEnv,
		const TCHAR* name, 
		Platform platform );
	ContentNodeVehicleModZip( ContentNode* parent, 
		const configUtil::Environment* pEnv, 
		xmlElementPtr pElement );
//	virtual ~ContentNodeVehicleModZip( );


// 	// Child Container Iterators
// 	NodeContainerIter			BeginChildren( ) { return ( m_vChildren.begin() ); }
// 	NodeContainerIter			EndChildren( ) { return ( m_vChildren.end() ); }
// 	NodeContainerConstIter		BeginChildren( ) const { return ( m_vChildren.begin() ); }
// 	NodeContainerConstIter		EndChildren( ) const { return ( m_vChildren.end() ); } 
// 
// 	size_t						GetNumChildren( ) const { return m_vChildren.size(); }
// 	ContentNode*				GetChild( int index ) const { return m_vChildren.at( index ); }
// 	void						AddChild( ContentNode* pNode ) { m_vChildren.push_back( pNode ); }
// 	NodeContainerIter			RemoveChild( NodeContainerIter nodeIter ) { return ( m_vChildren.erase( nodeIter ) ); }
// 
// 
// 	virtual void				PostLoadCallback( );

	// Static Methods
	static const char*			GetXmlType( ) { return ( "vehiclemodzip" ); }

protected:
	NodeContainer	m_vChildren;	//!< Child nodes.
};

END_CONFIGPARSER_NS

#endif // __CONFIGPARSER_CONTENTNODEVEHICLES_H__
