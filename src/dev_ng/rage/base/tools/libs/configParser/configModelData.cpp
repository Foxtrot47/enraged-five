//
// filename:	configModelData.cpp
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		21 December 2009
// description:	
//

// Local headers
#include "configParser/configParser.h"
#include "configParser/configModelData.h"

#undef max
#define TEMP_BUFFER_SIZE (1024)

BEGIN_CONFIGPARSER_NS

ConfigModelData::ConfigModelData( )
{
	Initialize();
}

ConfigModelData::ConfigModelData( const ConfigModelData& copy )
{
	Initialize();

	m_eDataType = copy.m_eDataType;
	switch ( m_eDataType )
	{
	case CONFIGMODEL_DATA_STRING:
		{
			// Reallocate the string data.
			size_t size = ( _tcslen( copy.m_uData.s ) + 1 );
			m_uData.s = new TCHAR[size];
			_tcscpy_s( m_uData.s, size, copy.m_uData.s );
		}
		break;
	default:
		memcpy( &m_uData, &(copy.m_uData), sizeof( m_uData ) );
	}
	m_vChildren = copy.m_vChildren;
}

ConfigModelData::ConfigModelData( const TCHAR* s )
{	
	Initialize();

	size_t size = _tcslen( s ) + 1;

	m_eDataType = CONFIGMODEL_DATA_STRING;
	m_uData.s = new TCHAR[size];
	memset( m_uData.s, 0, size );
	_tcsncpy_s( m_uData.s, size, s, _TRUNCATE );
}

ConfigModelData::ConfigModelData( bool b )
{
	Initialize();

	m_eDataType = CONFIGMODEL_DATA_BOOL;
	m_uData.b = b;
}

ConfigModelData::ConfigModelData( int n )
{
	Initialize();

	m_eDataType = CONFIGMODEL_DATA_INT;
	m_uData.n = n;
}

ConfigModelData::ConfigModelData( double f )
{
	Initialize();

	m_eDataType = CONFIGMODEL_DATA_FLOAT;
	m_uData.f = f;
}

ConfigModelData::~ConfigModelData( )
{
	SetEmpty( );
}

ConfigModelData& 
ConfigModelData::operator=( const ConfigModelData& copy )
{
	// Handle self-assignment.
	if ( this == &copy )
		return ( *this );

	SetEmpty( );
	m_eDataType = copy.m_eDataType;
	switch ( m_eDataType )
	{
	case CONFIGMODEL_DATA_STRING:
		{
			// Reallocate the string data.
			size_t size = ( _tcslen( copy.m_uData.s ) + 1 );
			m_uData.s = new TCHAR[size];
			_tcscpy_s( m_uData.s, size, copy.m_uData.s );
		}
		break;
	default:
		memcpy( &m_uData, &(copy.m_uData), sizeof( m_uData ) );
	}
	m_vChildren = copy.m_vChildren;

	return ( *this );
}

bool
ConfigModelData::operator==( const ConfigModelData& other )
{
	// Type.
	if ( m_eDataType != other.m_eDataType )
		return ( false );
	// Data
	switch ( m_eDataType )
	{
	case CONFIGMODEL_DATA_STRING:
		{
			size_t maxlen = std::max( _tcslen( m_uData.s ), _tcslen( other.m_uData.s ) );
			if ( 0 != _tcsncmp( m_uData.s, other.m_uData.s, maxlen ) )
				return ( false );
		}
	case CONFIGMODEL_DATA_BOOL:
	case CONFIGMODEL_DATA_INT:
	case CONFIGMODEL_DATA_FLOAT:
		if ( 0 != memcmp( &m_uData, &(other.m_uData), sizeof( m_uData ) ) )
			return ( false );
	case CONFIGMODEL_DATA_EMPTY:
	default:
		break;
	}

	// Children
	if ( m_vChildren.size() != other.m_vChildren.size() )
		return ( false );
	for ( size_t i = 0; i < m_vChildren.size(); ++i )
	{
		if ( m_vChildren[i] != other.m_vChildren[i] )
			return ( false );
	}

	return ( true );
}

bool
ConfigModelData::operator!=( const ConfigModelData& other )
{
	return ( !(*this == other) );
}

ConfigModelData&
ConfigModelData::operator[]( const size_t index )
{
	return ( m_vChildren[index] );
}

void
ConfigModelData::ToString( TCHAR* s, size_t size ) const
{
	STL_STRING value;
	switch ( m_eDataType )
	{
	case CONFIGMODEL_DATA_EMPTY:
		value = STL_STRING( _T("EMPTY") );
		break;
	case CONFIGMODEL_DATA_BOOL:
		value = ( m_uData.b ? STL_STRING( _T("true") ) : STL_STRING( _T("false") ) );
		break;
	case CONFIGMODEL_DATA_STRING:
		value = STL_STRING( m_uData.s );
		break;
	case CONFIGMODEL_DATA_INT:
		{
			TCHAR temp[TEMP_BUFFER_SIZE] = { 0 };
			_stprintf_s( temp, TEMP_BUFFER_SIZE, _T("%d"), m_uData.n );
			value = STL_STRING( temp );
		}
		break;
	case CONFIGMODEL_DATA_FLOAT:
		{
			TCHAR temp[TEMP_BUFFER_SIZE] = { 0 };
			_stprintf_s( temp, TEMP_BUFFER_SIZE, _T("%f"), m_uData.f );
			value = STL_STRING( temp );
		}
		break;
	default:
		throw configModelDataException( "Internal error: invalid data type." );
		break;
	}

	_tcsncpy_s( s, size, value.c_str(), _TRUNCATE );
}

void
ConfigModelData::GetValue( TCHAR* s, size_t size ) const
{
	_tcsncpy_s( s, size, m_uData.s, _TRUNCATE );
}

void 
ConfigModelData::Initialize()
{
	m_eDataType = CONFIGMODEL_DATA_EMPTY;
	SetEmpty();
}

void
ConfigModelData::SetEmpty( )
{
	switch ( m_eDataType )
	{
	case CONFIGMODEL_DATA_STRING:
		if ( m_uData.s )
		{
			delete [] m_uData.s;
			m_uData.s = NULL;
		}
		break;
	default:
		break;
	}
	memset( &m_uData, NULL, sizeof( m_uData ) );
	m_eDataType = CONFIGMODEL_DATA_EMPTY;
	m_vChildren.clear( );
}

void 
ConfigModelData::push_child( const ConfigModelData& data )
{
	m_vChildren.push_back( data );
}

void 
ConfigModelData::pop_child( )
{
	m_vChildren.pop_back( );
}

END_CONFIGPARSER_NS

