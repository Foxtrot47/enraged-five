#pragma warning(disable: 4996)

// --- Includes -----------------------------------------------------------------

// Local headers
#include "configParser/configIgnoredConnectionSources.h"
#include "configParser/configModelData.h"

// Other headers
#include "configUtil/path.h"

#include "string/string.h"
#include "string/unicode.h"

// STL Headers Files
#include <algorithm>

using namespace configUtil;

#pragma warning( disable : 4668 )
// Boost Header Files
#pragma warning( push )
#pragma warning( disable : 4512 )
#include "boost/algorithm/string/replace.hpp"
#include "boost/algorithm/string/case_conv.hpp"
#pragma warning( pop )

// --- Defines ------------------------------------------------------------------
#define IGNORED_CONNECTION_SOURCES_FILENAME			( _T("rag.meta") )

#define TEMP_BUFFER_SIZE	(1024)

// --- Implementation -----------------------------------------------------------

BEGIN_CONFIGPARSER_NS

ConfigIgnoredConnectionSources::ConfigIgnoredConnectionSources( )
{
	memset( m_IgnoredConnectionSourcesConfig, 0, MAX_PATH * sizeof( TCHAR ) );

	m_pEnv = new Environment;
	m_pEnv->import_core_globals( );
	m_pEnv->import_secondary_globals( );

	TCHAR toolsconfig[MAX_PATH] = {0};
	m_pEnv->lookup( VAR_TOOLSCONFIG, toolsconfig, MAX_PATH );

	// $(toolsconfig)/globals/ignoredconnectionsources.meta
	Path::combine( m_IgnoredConnectionSourcesConfig, MAX_PATH, 3, toolsconfig, (_T("globals")), IGNORED_CONNECTION_SOURCES_FILENAME );
	
	if (m_Model.load( m_IgnoredConnectionSourcesConfig ))
	{
		LoadFromModel(m_Model);
	}
}

ConfigIgnoredConnectionSources::~ConfigIgnoredConnectionSources( )
{
	delete m_pEnv;
}

bool 
ConfigIgnoredConnectionSources::ShouldIgnoreAddress( const TCHAR* ip ) const
{
	std::map<STL_STRING, STL_STRING>::const_iterator itr;
	for (itr = m_IpAddressMap.begin(); itr != m_IpAddressMap.end(); ++itr )
	{
		if (itr->second.compare(ip) == 0)
		{
			return true;
		}
	}

	return false;
}

void
ConfigIgnoredConnectionSources::LoadFromModel(const ConfigModel& model)
{
	size_t numAddresses = GetNodeCountForQuery(model, _T("//IgnoredConnectionSources/Item"));

	for(size_t i=0; i < numAddresses; ++i)
	{
		TCHAR name[MAX_PATH];
		TCHAR address[MAX_PATH];

		if (GetConnectionName(model, i, name, MAX_PATH) &&
			GetConenctionAddress(model, i, address, MAX_PATH))
		{
			STL_STRING strName(name);
			STL_STRING strAddress(address);

			m_IpAddressMap.insert(std::pair<STL_STRING, STL_STRING>(strName, strAddress));
		}
	}
}

size_t
ConfigIgnoredConnectionSources::GetNodeCountForQuery(const ConfigModel& model, const TCHAR* expr) const
{
	configParser::ConfigModelData data;
	bool result = model.query( expr, data );

	if (!result || (data.IsEmpty() && !data.HasChildren()))
	{
		return 0L;
	}
	else
	{
		return (data.HasChildren() ? data.ChildCount() : 1);
	}
}

bool 
ConfigIgnoredConnectionSources::Query( const ConfigModel& model, const TCHAR* expr, ConfigModelData& data ) const
{
	data = ConfigModelData( );
	model.query(expr, data);
	return ( !data.IsEmpty( ) || data.HasChildren( ) );
}

bool 
ConfigIgnoredConnectionSources::Query( const ConfigModel& model, const TCHAR* expr, TCHAR* data, size_t size ) const
{
	ConfigModelData cdata;
	model.query( expr, cdata );

	if ( ConfigModelData::CONFIGMODEL_DATA_STRING == cdata.GetType() )
	{
		cdata.GetValue( data, size );
		m_pEnv->subst( data, size );
	}

	return ( ConfigModelData::CONFIGMODEL_DATA_STRING == cdata.GetType() );
}


bool 
ConfigIgnoredConnectionSources::Query( const ConfigModel& model, const TCHAR* expr, int& data ) const
{
	ConfigModelData cdata;
	model.query( expr, cdata );

	if ( ConfigModelData::CONFIGMODEL_DATA_INT == cdata.GetType() )
	{
		cdata.GetValue( data );
		return true;
	}
	else if ( ConfigModelData::CONFIGMODEL_DATA_STRING == cdata.GetType() )
	{
		// Attempt to convert string data into int data.
		TCHAR sdata[TEMP_BUFFER_SIZE];
		cdata.GetValue( sdata, TEMP_BUFFER_SIZE );
		data = _tstoi( sdata );
		return true;
	}

	return false;
}


bool 
ConfigIgnoredConnectionSources::GetConnectionName( const ConfigModel& model, size_t index, TCHAR* name, size_t size ) const
{
	TCHAR query[_MAX_PATH] = {0};
	_stprintf_s( query, _MAX_PATH, _T("//IgnoredConnectionSources/Item[%d]/Name"), unsigned(index+1) );

	bool queryTypesResult = Query(model, query, name, size );
	return queryTypesResult;
}

bool 
ConfigIgnoredConnectionSources::GetConenctionAddress( const ConfigModel& model, size_t index, TCHAR* address, size_t size ) const
{
	TCHAR query[_MAX_PATH] = {0};
	_stprintf_s( query, _MAX_PATH, _T("//IgnoredConnectionSources/Item[%d]/IPAddress"), unsigned(index+1) );

	bool queryTypesResult = Query(model, query, address, size );
	return queryTypesResult;
}

END_CONFIGPARSER_NS
