using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Windows.Forms;

using rage;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Graph2D
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public float Lerp(float a, float b, float t)
        {
            return a + (b - a) * t;
        }

        public float Range(float a, float b, float t)
        {
            return (t - a) / (b - a);
        }

        public Color LerpColor(Color a, Color b, float t)
        {
            float R = Lerp(a.R, b.R, t);
            float G = Lerp(a.G, b.G, t);
            float B = Lerp(a.B, b.B, t);
            float A = Lerp(a.A, b.A, t);
            return new Color((byte)R, (byte)G, (byte)B, (byte)A);
        }


        private void DrawGrid(rage.grcDraw draw)
        {
            int pixelWidth = draw.Viewport.Window.Width;
            int pixelHeight = draw.Viewport.Window.Height;

            float scaledWidth = m_Scale.X * pixelWidth;
            float scaledHeight = m_Scale.Y * pixelHeight;

            float xMin = m_Offset.X - scaledWidth * 0.5f;
            float xMax = m_Offset.X + scaledWidth * 0.5f;
            float yMin = m_Offset.Y - scaledHeight * 0.5f;
            float yMax = m_Offset.Y + scaledHeight * 0.5f;

            float pixelsPerDivision = m_GridDivision * ((float)pixelWidth / (xMax - xMin));

            draw.Viewport.Ortho(xMin, xMax, yMin, yMax, 0.0f, 1.0f);

            float xVal, yVal;

            if (pixelsPerDivision > 50.0f)
            {
                Color minorColor = new Color(255, 255, 0, 255);
                Color clearColor = new Color(panel1.ClearColor.R, panel1.ClearColor.G, panel1.ClearColor.B, panel1.ClearColor.A);

                minorColor = LerpColor(minorColor, clearColor, 0.5f);

                if (pixelsPerDivision < 100.0f)
                {
                    float t = Range(50.0f, 100.0f, pixelsPerDivision);
                    minorColor = LerpColor(clearColor, minorColor, t);
                }

                // Draw minor divisions
                float minorDiv = m_GridDivision / 10.0f;

                xVal = (float)System.Math.Floor(xMin / minorDiv) * minorDiv;

                while (xVal < xMax)
                {
                    draw.DrawLine(new Vector3(xVal, yMin, 0.0f), new Vector3(xVal, yMax, 0.0f), minorColor);
                    xVal += minorDiv;
                }

                yVal = (float)System.Math.Floor(yMin / minorDiv) * minorDiv;

                while (yVal < yMax)
                {
                    draw.DrawLine(new Vector3(xMin, yVal, 0.0f), new Vector3(xMax, yVal, 0.0f), minorColor);
                    yVal += minorDiv;
                }

            }

            xVal = (float)System.Math.Floor(xMin / m_GridDivision) * m_GridDivision;

            while(xVal < xMax)
            {
                draw.DrawLine(new Vector3(xVal, yMin, 0.0f), new Vector3(xVal, yMax, 0.0f), new Color(255, 255, 0));
                xVal += m_GridDivision;
            }

            yVal = (float)System.Math.Floor(yMin / m_GridDivision) * m_GridDivision;

            while(yVal < yMax)
            {
                draw.DrawLine(new Vector3(xMin, yVal, 0.0f), new Vector3(xMax, yVal, 0.0f), new Color(255, 255, 0));
                yVal += m_GridDivision;
            }

            // Draw X, Y axes
            draw.DrawLine(new Vector3(0.0f, yMin, 0.0f), new Vector3(0.0f, yMax, 0.0f), new Color(255, 0, 0));
            draw.DrawLine(new Vector3(xMin, 0.0f, 0.0f), new Vector3(xMax, 0.0f, 0.0f), new Color(255, 0, 0));

        }

        private void panel1_GrcPaint(object sender, rage.grcDraw draw)
        {
            DrawGrid(draw);
        }

        private Vector2 m_Offset = new Vector2(0.0f, 0.0f);
        private Vector2 m_Scale = new Vector2(1.0f, 1.0f);

        private float m_GridDivision = 10.0f;

        private bool isPanning = false;
        private bool isScaling = false;
        private Vector2 lastMouse = new Vector2(0.0f, 0.0f);

        private void panel1_SizeChanged(object sender, EventArgs e)
        {
            panel1.Invalidate();
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            bool altKey = (Control.ModifierKeys & Keys.Alt) != 0;

            if (altKey && (e.Button == MouseButtons.Middle))
            {
                // pan
                panel1.Capture = true;

                lastMouse.X = e.X;
                lastMouse.Y = e.Y;
                isPanning = true;
            }
            else if (altKey && (e.Button == MouseButtons.Right))
            {
                // scale

                panel1.Capture = true;

                lastMouse.X = e.X;
                lastMouse.Y = e.Y;
                isScaling = true;
            }
        }

        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            panel1.Capture = false;
            isPanning = false;
            isScaling = false;
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (isPanning)
            {
                m_Offset.X -= (e.X - lastMouse.X) * m_Scale.X;
                m_Offset.Y += (e.Y - lastMouse.Y) * m_Scale.Y;
                lastMouse.X = e.X;
                lastMouse.Y = e.Y;
                panel1.Invalidate();
            }
            if (isScaling)
            {
                float offsetX = (e.X - lastMouse.X);
                float offsetY = (e.Y - lastMouse.Y);
                float offset = offsetX + offsetY;
                m_Scale.X *= 1.0f + (offset * 0.01f);
                m_Scale.Y *= 1.0f + (offset * 0.01f);
                lastMouse.X = e.X;
                lastMouse.Y = e.Y;
                panel1.Invalidate();
            }
        }
    }
}