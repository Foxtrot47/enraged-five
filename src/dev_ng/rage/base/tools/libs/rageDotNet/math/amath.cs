using System;
using System.Collections.Generic;
using System.Text;

namespace rage
{
    public static partial class math
    {
        /// <summary>
        /// Linear interpolation from min to max, based on t
        /// </summary>
        /// <param name="t"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns>min when t==0, max when t==1, interpolated (and extrapolated) outside of [0,1]</returns>
        public static float Lerp(float t, float min, float max)
        {
            return t * (max - min) + min;
        }

        public static double Lerp(double t, double min, double max)
        {
            return t * (max - min) + min;
        }

        /// <summary>
        /// The inverse of Lerp, given a t value and a min and max, reutrns a value 
        /// such that Lerp(ret, min, max) = t
        /// </summary>
        /// <param name="t"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static float Range(float t, float min, float max)
        {
            return (min == max) ? 1.0f : (t - min) / (max - min);
        }

        public static double Range(double t, double min, double max)
        {
            return (min == max) ? 1.0 : (t - min) / (max - min);
        }

        // PURPOSE:	Clamp a number of any type between two other number of the same type.
        // PARAMS:
        //	t -	the number to clamp
        //	a -	the minimum value
        //	b -	the maximum value
        // RETURN: The number between the given minimum and maximum that is closest to the given number.
        public static T Clamp<T>(T t, T a, T b) where T : IComparable<T>
        {
            return (t.CompareTo(a) < 0) ? a : (t.CompareTo(b) > 0) ? b : t;
        }

        // PURPOSE: Find the fraction of the distance that the given value is between the other two given values,
        //			with limits of 0 and 1.
        // PARAMS:
        //	f -		the value to compare with the given range
        //	min -	the lower end of the range
        //	max -	the upper end of the range
        // RETURN:	the fraction of the distance that the given value is between the other two given values,
        //			between 0 and 1
        // NOTES:	If min equals max, the problem is undefined and either 0 or 1 is returned
        public static float ClampRange(float f, float min, float max)
        {
            return (f <= min) ? 0.0f : ((f >= max) ? 1.0f : (f - min) / (max - min));
        }
        public static double ClampRange(double f, double min, double max)
        {
            return (f <= min) ? 0.0f : ((f >= max) ? 1.0f : (f - min) / (max - min));
        }

        // PURPOSE: Defines a linear function from {funcInA,funcOutA} to {funcInB,funcOutB}, clamped to the range {funcOutA,funcOutB}
        // PARAMS:
        //   funcInA - see return
        //   funcInB - see return
        //   funcOutA - see return
        //   funcOutB - see return
        // RETURN
        //   Returns f(x), which is a piecewise linear function with three sections:
        //     For x <= funcInA, f(x) = funcOutA.
        //     for x > funcInA and x < funcInB, f(x) ramps from funcOutA to funcOutB
        //     for x >= funcInB, f(x) = funcOutB
        public static float RampValue(float x, float funcInA, float funcInB, float funcOutA, float funcOutB)
        {
            float t = Clamp((x - funcInA) / (funcInB - funcInA), 0.0f, 1.0f);
            return Lerp(t, funcOutA, funcOutB);
        }

        public static double RampValue(double x, double funcInA, double funcInB, double funcOutA, double funcOutB)
        {
            double t = Clamp((x - funcInA) / (funcInB - funcInA), 0.0f, 1.0f);
            return Lerp(t, funcOutA, funcOutB);
        }

        // PURPOSE: Defines a linear function from {funcInA,funcOutA} to {funcInB,funcOutB}
        // PARAMS:
        //   funcInA - see return
        //   funcInB - see return
        //   funcOutA - see return
        //   funcOutB - see return
        // RETURN
        //   Returns f(x), which is a linear function such that
        //     for x > funcInA and x < funcInB, f(x) ramps from funcOutA to funcOutB
        public static float RampValueUnclamped(float x, float funcInA, float funcInB, float funcOutA, float funcOutB)
        {
            float t = (x - funcInA) / (funcInB - funcInA);
            return Lerp(t, funcOutA, funcOutB);
        }
        public static double RampValueUnclamped(double x, double funcInA, double funcInB, double funcOutA, double funcOutB)
        {
            double t = (x - funcInA) / (funcInB - funcInA);
            return Lerp(t, funcOutA, funcOutB);
        }


        // PURPOSE:	Find the value the input parameter would have by moving it by an integer multiple of the input range
        //			get it within the input range.
        // PARAMS:
        //	i -		the number to wrap into the range
        //	min	-	the lower end of the range
        //	max -	the upper end of the range
        // RETURN:	the value the input parameter would have by moving it by an integer multiple of the input range
        //			get it within the input range
        public static int Wrap(int i, int min, int max)
        {
            return (min == max) ? min : ((max < min) ? i : ((i - min) % (max - min + 1) + ((i < min) ? max + 1 : min)));
        }

        public static float Wrap(float f, float min, float max)
        {
            if (max < min) return f;
            if (max == min) return min;
            if (f < min) return max - (min - f) % (max - min);
            if (f > max) return min + (f - max) % (max - min);
            return f;
        }

        public static double Wrap(double f, double min, double max)
        {
            if (max < min) return f;
            if (max == min) return min;
            if (f < min) return max - (min - f) % (max - min);
            if (f > max) return min + (f - max) % (max - min);
            return f;
        }

        // PURPOSE:	Make sure a <= b, swapping them if necessary
        // PARAMS:
        //	a -	reference to the first number
        //	b -	reference to the second number
        public static void OrderEm<T>( ref T a, ref T b ) where T : IComparable<T>
        {
            if (a.CompareTo(b) > 0)
            {
                T temp = a;
                a = b;
                b = temp;
            }
        }

        // PURPOSE: Swaps two values
        // PARAMS:
        //  a - reference to the first value
        //  b - reference to the second value
        public static void Swap<T>(ref T a, ref T b)
        {
            T temp = a;
            a = b;
            b = temp;
        }

        // PURPOSE: Averages values
        public static float Average(float a, float b) {
            return (a + b) * 0.5f;
        }

        public static float Average(float a, float b, float c) {
            return (a + b + c) * 0.333333333f;
        }

        public static float Average(float a, float b, float c, float d)
        {
            return (a + b + c + d) * 0.25f;
        }

        public static float Average(float a, float b, float c, float d, float e)
        {
            return (a + b + c + d + e) * 0.2f;
        }

        public static double Average(double a, double b)
        {
            return (a + b) * 0.5f;
        }

        public static double Average(double a, double b, double c)
        {
            return (a + b + c) * 0.333333333f;
        }

        public static double Average(double a, double b, double c, double d)
        {
            return (a + b + c + d) * 0.25f;
        }

        public static double Average(double a, double b, double c, double d, double e)
        {
            return (a + b + c + d + e) * 0.2f;
        }

    }
}
