using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace rage
{
    /// <summary>
    /// grcViewport is fairly different from a D3D viewport. A D3D viewport
    /// defines the part of the render target to render onto. A grcViewport
    /// defines this as well as the projection matrix.
    /// </summary>
    public class grcViewport
    {
        public grcViewport(grcDraw draw)
        {
            m_Draw = draw;
            ResetWindow();
            Ortho();
        }

        public Matrix World
        {
            get { return m_World; }
            set { 
                m_World = value; 
                if (m_Draw != null && m_Draw.Viewport == this)
                {
                    m_Draw.Effect.World = value;
                }
            }
        }

        public Matrix Camera
        {
            get { return m_Camera; }
            set
            {
                m_Camera = value;
                if (m_Draw != null && m_Draw.Viewport == this)
                {
                    m_Draw.Effect.View = value;
                }
            }
        }

        public Matrix Projection
        {
            get { return m_Projection; }
            set { 
                m_Projection = value;
                if (m_Draw != null && m_Draw.Viewport == this)
                {
                    m_Draw.Effect.Projection = value;
                }
            }
        }

        public Viewport Window
        {
            get { return m_Window; }
        }
		
        public void Perspective(float fovy, float aspect, float znear, float zfar)
        {
            Projection = Matrix.CreatePerspectiveFieldOfView(fovy, aspect, znear, zfar);
        }

        public void Ortho(float left, float right, float bottom, float top, float znear, float zfar)
        {
            Projection = Matrix.CreateOrthographicOffCenter(left, right, bottom, top, znear, zfar);
        }

        public void Screen()
        {
            Ortho(m_Window.X, m_Window.X + m_Window.Width,
                m_Window.Y + m_Window.Height, m_Window.Y, 0.0f, 1.0f);
        }

        public void Ortho()
        {
            Ortho(-1.0f, 1.0f, -1.0f, 1.0f, 0.0f, 1.0f);
        }

        public void ResetWindow()
        {
            SetWindow(0, 0, m_Draw.Device.PresentationParameters.BackBufferWidth, m_Draw.Device.PresentationParameters.BackBufferHeight);
        }

        public float GetAspectRatio()
        {
            return m_Window.Width / m_Window.Height;
        }

        public void SetWindow(int x,int y,int width,int height)
        {
            SetWindow(x,y,width,height,0.0f, 1.0f);
        }

        public void SetWindow(int x,int y,int width,int height,float zmin,float zmax)
        {
            m_Window.X = x;
            m_Window.Y = y;
            m_Window.Width = width;
            m_Window.Height = height;
            m_Window.MinDepth = zmin;
            m_Window.MaxDepth = zmax;

            if (m_Draw != null && m_Draw.Viewport == this)
            {
                m_Draw.Device.Viewport = Window;
            }
        }

        public void Transform(Vector3 worldPosition, out float windowX, out float windowY)
        {
            Vector3 v = m_Window.Project(worldPosition, m_Projection, m_Camera, m_World);
            windowX = v.X;
            windowY = v.Y;
        }

	    public void ReverseTransform(float x,float y,out Vector3 outNear,out Vector3 outFar)
        {
            outNear = m_Window.Unproject(new Vector3(x, y, m_Window.MinDepth), m_Projection, m_Camera, m_World);
            outFar = m_Window.Unproject(new Vector3(x, y, m_Window.MaxDepth), m_Projection, m_Camera, m_World);
        }

        public bool IsPerspective()
        {
            //TODO: write this
            return true;
        }

        public grcViewport Clone()
        {
            grcViewport vp = new grcViewport();
            vp.m_Draw = m_Draw; // copies a reference

            // all these are structs, so get copied.
            vp.m_Projection = m_Projection;
            vp.m_World = m_World;
            vp.m_Camera = m_Camera;
            vp.m_Window = m_Window;

            return vp;
        }

        public void SetCurrent()
        {
            m_Draw.Viewport = this;
        }

        protected grcViewport()
        {

        }

        private Matrix m_Projection = Matrix.CreateOrthographicOffCenter(-1.0f, 1.0f, -1.0f, 1.0f, 0.0f, 1.0f);
        private Matrix m_World = Matrix.Identity;
        private Matrix m_Camera = Matrix.Identity;

        private Viewport m_Window;

        private grcDraw m_Draw;

    }
}
