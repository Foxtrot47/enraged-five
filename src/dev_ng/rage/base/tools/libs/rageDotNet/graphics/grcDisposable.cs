using System;
using System.Collections.Generic;
using System.Text;

namespace rage
{
    // If your class holds on to Direct3D resources, it should inherit from this base class or if that's not possible
    // should re-implement the same logic.
    public abstract class grcDisposable : IDisposable
    { 
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~grcDisposable()
        {
            Dispose(false);
        }

        private bool m_IsDisposed = false;

        // Do not override this, override ReleaseManagedResources or ReleaseUnmanagedResources instead
        protected void Dispose(bool disposing)
        {
            if (m_IsDisposed) return;
            if (disposing)
            {
                ReleaseManagedResources();
            }
            ReleaseUnmanagedResources();
            m_IsDisposed = true;
        }

        protected abstract void ReleaseManagedResources();
        protected abstract void ReleaseUnmanagedResources();
    }
}
