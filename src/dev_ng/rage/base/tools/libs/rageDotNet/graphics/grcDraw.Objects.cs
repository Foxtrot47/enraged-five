using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace rage
{
    public partial class grcDraw
    {
        public void DrawLine(Vector3 a, Vector3 b, Color c)
        {
            Color(c);
            Begin(PrimitiveType.LineList, 2);
            Vertex(a);
            Vertex(b);
            End();
        }

        public void DrawBox(Vector3 size, Matrix mtx, Color color)
        {
            Vector3 pos = Vector3.Multiply(size, 0.5f);

            Vector3 a = new Vector3(-pos.X, -pos.Y, -pos.Z);
            Vector3 b = new Vector3(+pos.X, -pos.Y, -pos.Z);
            Vector3 c = new Vector3(+pos.X, +pos.Y, -pos.Z);
            Vector3 d = new Vector3(-pos.X, +pos.Y, -pos.Z);
            Vector3 e = new Vector3(-pos.X, -pos.Y, +pos.Z);
            Vector3 f = new Vector3(+pos.X, -pos.Y, +pos.Z);
            Vector3 g = new Vector3(+pos.X, +pos.Y, +pos.Z);
            Vector3 h = new Vector3(-pos.X, +pos.Y, +pos.Z);

            WorldMatrix(mtx);
            Begin(PrimitiveType.LineList, 24);
            Color(color);

            Vertex(a); Vertex(b);
            Vertex(b); Vertex(c);
            Vertex(c); Vertex(d);
            Vertex(d); Vertex(a);

            Vertex(a); Vertex(e);
            Vertex(b); Vertex(f);
            Vertex(c); Vertex(g);
            Vertex(d); Vertex(h);

            Vertex(e); Vertex(f);
            Vertex(f); Vertex(g);
            Vertex(g); Vertex(h);
            Vertex(h); Vertex(e);

            End();
        }

        public void DrawBox(Vector3 min, Vector3 max, Color color)
        {
            Vector3 size = max - min;
            Matrix mtx = Matrix.Identity;
            mtx.Translation = (max + min) * 0.5f;
            DrawBox(size, mtx, color);
        }

        public void DrawSolidBox(Vector3 size, Matrix mtx, Color color)
        {
        	Vector3 pos;
        	pos = size * 0.5f;

        	Vector3 a = new Vector3(-pos.X,-pos.Y,-pos.Z);
            Vector3 b = new Vector3(+pos.X,-pos.Y,-pos.Z);
            Vector3 c = new Vector3(+pos.X,+pos.Y,-pos.Z);
            Vector3 d = new Vector3(-pos.X,+pos.Y,-pos.Z);
            Vector3 e = new Vector3(-pos.X,-pos.Y,+pos.Z);
            Vector3 f = new Vector3(+pos.X,-pos.Y,+pos.Z);
            Vector3 g = new Vector3(+pos.X,+pos.Y,+pos.Z);
            Vector3 h = new Vector3(-pos.X, +pos.Y, +pos.Z);

	        WorldMatrix(mtx);

        	Color(color);

	        Begin(PrimitiveType.TriangleFan,4);
	        Vertex(d);
	        Vertex(c);
	        Vertex(b);
	        Vertex(a);
	        End();

            Begin(PrimitiveType.TriangleFan, 4);
	        Vertex(e);
	        Vertex(f);
	        Vertex(g);
	        Vertex(h);
	        End();

            Begin(PrimitiveType.TriangleFan, 4);
	        Vertex(b);
	        Vertex(c);
	        Vertex(g);
	        Vertex(f);
	        End();

            Begin(PrimitiveType.TriangleFan, 4);
	        Vertex(c);
	        Vertex(d);
	        Vertex(h);
	        Vertex(g);
	        End();

            Begin(PrimitiveType.TriangleFan, 4);
	        Vertex(a);
	        Vertex(e);
	        Vertex(h);
	        Vertex(d);
	        End();

            Begin(PrimitiveType.TriangleFan, 4);
	        Vertex(a);
	        Vertex(b);
	        Vertex(f);
	        Vertex(e);
	        End();
        }

        public void DrawSolidBox(Vector3 min, Vector3 max, Color color)
        {
            Vector3 size = max - min;
            Matrix mtx = Matrix.Identity;
            mtx.Translation = (max + min) * 0.5f;
            DrawSolidBox(size, mtx, color);
        }

        public void DrawFrustum(Matrix m, float sx, float sy, float sz, Color color)
        {
           	 float a=0.5f*sx, b=0.5f*sy, c=sz;
        	Vector3[] fbase = new Vector3[4];
	        Vector3 apex = new Vector3(0.0f, 0.0f, 0.0f);

	        fbase[0] = new Vector3(a, b, -c);
            fbase[1] = new Vector3(a, -b, -c);
            fbase[2] = new Vector3(-a, b, -c);
            fbase[3] = new Vector3(-a, -b, -c);

	        // draw fbase
	        WorldMatrix(m);
	        Color(color);
	        Begin(PrimitiveType.LineStrip,5);
	        Vertex(fbase[0]);
	        Vertex(fbase[1]);
	        Vertex(fbase[3]);
	        Vertex(fbase[2]);
	        Vertex(fbase[0]);
	        End();

	        // draw to apex
	        Begin(PrimitiveType.LineList, 8);
	        for(int i=0; i<4; i++)
	        {
		        Vertex(apex);
		        Vertex(fbase[i]);
	        }
	        End();
        }

        public void DrawSphere(float rad, Vector3 center, int steps,bool longitudinalCircles,bool solid) {
        	Matrix mtx = Matrix.Identity;
        	mtx.Translation = center;
        	DrawSphere(rad,mtx,steps,longitudinalCircles,solid);
        }

        public void DrawSphere(float rad, Matrix mtx,int steps,bool longitudinalCircles,bool solid) {
        	DrawEllipsoid(new Vector3(rad,rad,rad),mtx,steps,longitudinalCircles,solid);
        }

        public void DrawEllipsoid(Vector3 size, Matrix mtx,int steps,bool longitudinalCircles,bool solid) {
	        WorldMatrix(mtx);
	        if (solid)
	        {
		        for(int i=(steps>>1);i>0;--i) {
			        float y=size.Y*(float)Math.Cos(MathHelper.Pi*((float)i)/((float)(steps>>1)));
			        float nexty=size.Y*(float)Math.Cos(MathHelper.Pi*((float)i-1)/((float)(steps>>1)));
			        float s=(float)Math.Sin(MathHelper.Pi*((float)i)/((float)(steps>>1)));
			        float nexts=(float)Math.Sin(MathHelper.Pi*((float)i-1)/((float)(steps>>1)));
			        Begin(PrimitiveType.TriangleStrip,(steps+1) * 2);
			        for(int j=0;j<=steps;j++) {
				        float x=size.X*s*(float)Math.Cos(2.0f*MathHelper.Pi*((float)j)/((float)steps));
				        float nextx=size.X*nexts*(float)Math.Cos(2.0f*MathHelper.Pi*((float)j+1)/((float)steps));
				        float z=size.Z*s*(float)Math.Sin(2.0f*MathHelper.Pi*((float)j)/((float)steps));
				        float nextz=size.Z*nexts*(float)Math.Sin(2.0f*MathHelper.Pi*((float)j+1)/((float)steps));
				        Vector3 normal = new Vector3(x, y, z);
				        normal.Normalize();
				        Normal(normal);
				        Vertex(x,y,z);
                        Vector3 nextNormal = new Vector3(nextx, nexty, nextz);
                        nextNormal.Normalize();
                        Normal(nextNormal);
                        Vertex(nextx,nexty,nextz);
			        }
			        End();
		        }
	        }
	        else
	        {
		        int i, j;
		        float x,y,z,s;
		        if(steps<4) steps=4;
		        WorldMatrix(mtx);
		        for(i=1;i<(steps>>1);i++) {
			        y=size.Y*(float)Math.Cos(MathHelper.Pi*((float)i)/((float)(steps>>1)));
			        s=(float)Math.Sin(MathHelper.Pi*((float)i)/((float)(steps>>1)));
			        Begin(PrimitiveType.LineStrip,steps+1);
			        for(j=0;j<=steps;j++) {
				        x=size.X*s*(float)Math.Cos(2.0f*MathHelper.Pi*((float)j)/((float)steps));
				        z=size.Z*s*(float)Math.Sin(2.0f*MathHelper.Pi*((float)j)/((float)steps));
				        Vertex(x,y,z);
			        }
			        End();
		        }
		        if(longitudinalCircles) {
			        for(i=0;i<(steps>>1);i++) {
				        x=size.X*(float)Math.Cos(MathHelper.Pi*((float)i)/((float)(steps>>1)));
				        z=size.X*(float)Math.Sin(MathHelper.Pi*((float)i)/((float)(steps>>1)));
				        Begin(PrimitiveType.LineStrip,steps+1);
				        for(j=0;j<=steps;j++) {
					        s=(float)Math.Sin(2.0f*MathHelper.Pi*((float)j)/((float)steps));
					        y=size.Y*(float)Math.Cos(2.0f*MathHelper.Pi*((float)j)/((float)steps));
					        Vertex(x*s,y,z*s);
				        }
				        End();
			        }
		        }
	        }
        }



        public void DrawEllipse( float sizeX,  float sizeZ,  Matrix mtx,int steps)
        {
	        float x,y,z;
	        WorldMatrix(mtx);
	        y = 0.0f;
	        Begin(PrimitiveType.LineStrip,steps+1);
	        for(int j=0;j<=steps;j++) {
		        x=sizeX*(float)Math.Cos(2.0f*MathHelper.Pi*((float)j)/((float)steps));
		        z=sizeZ*(float)Math.Sin(2.0f*MathHelper.Pi*((float)j)/((float)steps));
		        Vertex(x,y,z);
	        }
	        End();
        }


        public void DrawSphere(float rad,int steps) {
	        int j;
	        float x, y, z, s;

	        s=rad;

	        x=0;
	        Begin(PrimitiveType.LineStrip,steps+1);
	        for(j=0;j<=steps;j++) {
		        y=s*(float)Math.Cos(2.0f*MathHelper.Pi*((float)j)/((float)steps));
		        z=s*(float)Math.Sin(2.0f*MathHelper.Pi*((float)j)/((float)steps));
		        Vertex(x,y,z);
	        }
	        End();

	        y=0;
	        Begin(PrimitiveType.LineStrip,steps+1);
	        for(j=0;j<=steps;j++) {
		        x=s*(float)Math.Cos(2.0f*MathHelper.Pi*((float)j)/((float)steps));
		        z=s*(float)Math.Sin(2.0f*MathHelper.Pi*((float)j)/((float)steps));
		        Vertex(x,y,z);
	        }
	        End();

	        z=0;
	        Begin(PrimitiveType.LineStrip,steps+1);
	        for(j=0;j<=steps;j++) {
		        x=s*(float)Math.Cos(2.0f*MathHelper.Pi*((float)j)/((float)steps));
		        y=s*(float)Math.Sin(2.0f*MathHelper.Pi*((float)j)/((float)steps));
		        Vertex(x,y,z);
	        }
	        End();
        }

        private const float SMALL_FLOAT = 1.0e-6f;

        public void DrawSpiral( Vector3 start,  Vector3 end, float startRadius, float endRadius, float revolutionsPerMeter, float initialPhase, float arrowLength, int steps)
        {
	        // A vector down the axis of the spiral
	        Vector3 delta = Vector3.Subtract(end, start);

	        // One of two basis vectors that form a matrix when combined with delta
	        Vector3 basis2;
	        basis2 = Vector3.Cross(delta, Vector3.UnitY);

	        float length = delta.Length();
	        float epsilon = length * length * 0.01f;

	        if (basis2.LengthSquared() < epsilon)
	        {
		        basis2 = Vector3.Cross(delta, Vector3.UnitZ);
	        }

	        basis2.Normalize();

	        // The other basis vector
	        Vector3 basis3;
	        basis3 = Vector3.Cross(delta, basis2);
	        basis3.Normalize();

	        // A non-orthonormal matrix to speed computation in the loop
	        Matrix  mtx = Matrix.Identity;
            // TODO: Is this right?
            mtx.Right = basis2;
            mtx.Up = delta;
            mtx.Forward = basis3;
            mtx.Translation = start;

	        float revolutions = length * revolutionsPerMeter;
	        int totalSteps = (int)(revolutions * steps);
	        Vector3 previousVertex = start;
	        Vector3 vertex = start;

	        totalSteps = Math.Min(totalSteps, MaxVertices - 1);

	        if (totalSteps > 0)
	        {
		        Begin(PrimitiveType.LineStrip,totalSteps+1);
		        for(int j=0;j<=totalSteps;j++) {
			        previousVertex = vertex;

			        float totalPhase = ((float)j) / ((float)totalSteps);
			        float revolutionPhase = ((float)j)/((float)steps) + initialPhase;
			        float radius = startRadius + totalPhase * (endRadius - startRadius);

			        vertex.X=radius*(float)Math.Cos(-2.0f*MathHelper.Pi*revolutionPhase);
			        vertex.Y=totalPhase;
			        vertex.Z=radius*(float)Math.Sin(-2.0f*MathHelper.Pi*revolutionPhase);
			        vertex = Vector3.Transform(vertex, mtx);

			        Vertex(vertex);
		        }
		        End();

		        arrowLength = Math.Abs(arrowLength);
		        if (arrowLength > 0.0f)
		        {
			        Vector3 direction = Vector3.Subtract(vertex, previousVertex);
			        float dirLen2 = direction.LengthSquared();

			        if (dirLen2 > SMALL_FLOAT * SMALL_FLOAT)
			        {
				        direction= Vector3.Multiply(direction, 1.0f / (float)Math.Sqrt(dirLen2));

				        Vector3 side;
				        side = Vector3.Cross(direction, Vector3.UnitY);

				        float sideLen2 = side.LengthSquared();
				        if (sideLen2 < SMALL_FLOAT * SMALL_FLOAT)
				        {
					        side = Vector3.Cross(direction,Vector3.UnitX);
				        }

				        side.Normalize();

				        Vector3 up;
				        up = Vector3.Cross(direction, side);
				        up.Normalize();

				        Vector3 vertex2;

				        Begin(PrimitiveType.LineList,8);

				        vertex2= Vector3.Multiply(direction, -arrowLength);
				        vertex2 = vertex2 + (up * arrowLength);
				        vertex2 = vertex2 + vertex;
				        Vertex(vertex);
				        Vertex(vertex2);

				        vertex2 = Vector3.Multiply(direction, -arrowLength);
				        vertex2 = vertex2 + (up * -arrowLength);
                        vertex2 = vertex2 + vertex;
                        Vertex(vertex);
				        Vertex(vertex2);

				        vertex2= Vector3.Multiply(direction, -arrowLength);
				        vertex2 = vertex2 + (side * arrowLength);
                        vertex2 = vertex2 + vertex;
                        Vertex(vertex);
				        Vertex(vertex2);

				        vertex2= Vector3.Multiply(direction, -arrowLength);
				        vertex2 = vertex2 + (side * -arrowLength);
                        vertex2 = vertex2 + vertex;
                        Vertex(vertex);
				        Vertex(vertex2);

				        End();
			        }
		        }
	        }
        }

        public void DrawFlatCircle(float r,  Vector3 center, int steps, bool dashed)
        {
	        DrawCircle(r,center,Vector3.UnitX,Vector3.UnitZ,steps,dashed, false);
        }


        public void DrawCircle(float r,  Vector3 center,  Vector3 axisX,  Vector3 axisY, int steps, bool dashed, bool solid)
        {
	        int j;
	        float x, y;

	        int circleVerts = steps;
	        int grcVerts;

	        if(dashed)
	        {
		        steps -= (steps % 2) != 0 ? 1 : 0;
		        Begin(PrimitiveType.LineList,steps);
		        grcVerts = steps;
	        }
	        else if (solid)
	        {
		        Begin(PrimitiveType.TriangleFan,steps+2);
		        grcVerts = steps+1;
		        Vertex(center);
	        }
	        else
	        {
		        Begin(PrimitiveType.LineStrip,steps+1);
		        grcVerts = steps+1;
	        }

	        for(j=0;j<grcVerts;j++)
	        {
		        x=r*(float)Math.Cos(2.0f*MathHelper.Pi*((float)j)/((float)circleVerts));
		        y=r*(float)Math.Sin(2.0f*MathHelper.Pi*((float)j)/((float)circleVerts));
		        Vector3 pos;
                pos = axisX * x;
                pos = pos + (axisY * y);
                pos = pos + center;
		        Vertex(pos);
	        }
	        End();
        }


        public void DrawCapsule(float length,float radius, Vector3 center,int steps,bool solid) {
	        Matrix  mtx = Matrix.Identity;
	        mtx.Translation = center;
	        DrawCapsule(length,radius,mtx,steps,solid);
        }


        public void DrawCapsule(float length,float radius, Matrix mtx,int steps,bool solid) {
            float x, y, z;
            int j;

	        WorldMatrix(mtx);

	        float halflength = length * 0.5f;

	        if (solid)
	        {
		        for(int i=0;i<(steps>>1);i++) {
			        y=radius*(float)Math.Cos(MathHelper.Pi*((float)i)/((float)(steps>>1)));
			        float nexty=radius*(float)Math.Cos(MathHelper.Pi*((float)i+1)/((float)(steps>>1)));
			        float deltay;
			        if (i < (steps>>2))
			        {
				        deltay = halflength;
			        }
			        else
			        {
				        deltay = -halflength;
			        }
			        float s=(float)Math.Sin(MathHelper.Pi*((float)i)/((float)(steps>>1)));
			        float nexts=(float)Math.Sin(MathHelper.Pi*((float)i+1)/((float)(steps>>1)));
			        Begin(PrimitiveType.TriangleStrip,(steps+1) * 2);
			        for(j=0;j<=steps;j++) {
				        x=radius*s*(float)Math.Sin(2.0f*MathHelper.Pi*((float)j)/((float)steps));
				        float nextx=radius*nexts*(float)Math.Sin(2.0f*MathHelper.Pi*((float)j+1)/((float)steps));
				        z=radius*s*(float)Math.Cos(2.0f*MathHelper.Pi*((float)j)/((float)steps));
				        float nextz=radius*nexts*(float)Math.Cos(2.0f*MathHelper.Pi*((float)j+1)/((float)steps));
				        Vector3 normal = new Vector3(x, y, z);
				        normal.Normalize();
				        Normal(normal);
				        Vertex(x,y+deltay,z);
				        Vector3 nextNormal = new Vector3(nextx, nexty, nextz);
				        nextNormal.Normalize();
				        Normal(nextNormal);
				        Vertex(nextx,nexty+deltay,nextz);
			        }
			        End();
		        }

		        y=0;
		        Begin(PrimitiveType.TriangleStrip,2*(steps+1));
		        for(j=0;j<=steps;j++) {
			        Vector3 normal = new Vector3((float)Math.Cos(2.0f*MathHelper.Pi*((float)j)/((float)steps)), 0.0f, (float)Math.Sin(2.0f*MathHelper.Pi*((float)j)/((float)steps)));
			        x=radius*normal.X;
			        z=radius*normal.Z;

			        Normal(normal);
			        Vertex(x,y-halflength,z);
			        Vertex(x,y+halflength,z);
		        }
		        End();
	        }
	        else
	        {
		        x=0;
		        Begin(PrimitiveType.LineStrip,steps+1);
		        for(j=0;j<=steps;j++) {
			        y=radius*(float)Math.Cos(-MathHelper.Pi/2+MathHelper.Pi*((float)j)/((float)steps));
			        z=radius*(float)Math.Sin(-MathHelper.Pi/2+MathHelper.Pi*((float)j)/((float)steps));
			        Vertex(x,y+halflength,z);
		        }
		        End();

		        y=0;
		        Begin(PrimitiveType.LineStrip,steps*2+1);
		        for(j=0;j<=steps*2;j++) {
			        x=radius*(float)Math.Cos(2.0f*MathHelper.Pi*((float)j)/((float)steps*2));
			        z=radius*(float)Math.Sin(2.0f*MathHelper.Pi*((float)j)/((float)steps*2));
			        Vertex(x,y+halflength,z);
		        }
		        End();

		        z=0;
		        Begin(PrimitiveType.LineStrip,steps+1);
		        for(j=0;j<=steps;j++) {
			        x=radius*(float)Math.Cos(MathHelper.Pi*((float)j)/((float)steps));
			        y=radius*(float)Math.Sin(MathHelper.Pi*((float)j)/((float)steps));
			        Vertex(x,y+halflength,z);
		        }
		        End();
        		
		        x=0;
		        Begin(PrimitiveType.LineStrip,steps+1);
		        for(j=0;j<=steps;j++) {
			        y=radius*(float)Math.Cos(MathHelper.Pi/2+MathHelper.Pi*((float)j)/((float)steps));
			        z=radius*(float)Math.Sin(MathHelper.Pi/2+MathHelper.Pi*((float)j)/((float)steps));
			        Vertex(x,y-halflength,z);
		        }
		        End();

		        y=0;
		        Begin(PrimitiveType.LineStrip,steps*2+1);
		        for(j=0;j<=steps*2;j++) {
			        x=radius*(float)Math.Cos(2.0f*MathHelper.Pi*((float)j)/((float)steps*2));
			        z=radius*(float)Math.Sin(2.0f*MathHelper.Pi*((float)j)/((float)steps*2));
			        Vertex(x,y-halflength,z);
		        }
		        End();

		        z=0;
		        Begin(PrimitiveType.LineStrip,steps+1);
		        for(j=0;j<=steps;j++) {
			        x=radius*(float)Math.Cos(MathHelper.Pi+MathHelper.Pi*((float)j)/((float)steps));
			        y=radius*(float)Math.Sin(MathHelper.Pi+MathHelper.Pi*((float)j)/((float)steps));
			        Vertex(x,y-halflength,z);
		        }
		        End();

		        y=0;
		        for(j=0;j<steps;j++) {
			        x=radius*(float)Math.Cos(2.0f*MathHelper.Pi*((float)j)/((float)steps));
			        z=radius*(float)Math.Sin(2.0f*MathHelper.Pi*((float)j)/((float)steps));

			        Begin(PrimitiveType.LineList,2);
			        Vertex(x,y-halflength,z);
			        Vertex(x,y+halflength,z);
			        End();
		        }
	        }
        }


        public void DrawTaperedCapsule(float length,float radiusA,float radiusB, Matrix mtx,int steps,bool solid) {
            float x, y, z;
            int j;

	        WorldMatrix(mtx);

	        float halfLength = length * 0.5f;

	        float slopeSine = (radiusA-radiusB)/length;
	        float slopeAngle = (float)Math.Asin(slopeSine);
	        float twiceSlopeAngle = 2.0f*slopeAngle;
	        float angleAroundA = MathHelper.Pi+twiceSlopeAngle;
	        float angleAroundB = MathHelper.Pi-twiceSlopeAngle;
	        float verticalA = halfLength-radiusA*slopeSine;
	        float verticalB = -halfLength-radiusB*slopeSine;

	        if (solid)
	        {
		        for(int i=0;i<(steps>>1);i++) {
			        float deltay,radius,angleAround;
			        if (i < (steps>>2))
			        {
				        deltay = halfLength;
				        radius = radiusA;
				        angleAround = angleAroundA;
			        }
			        else
			        {
				        deltay = -halfLength;
				        radius = radiusB;
				        angleAround = angleAroundB;
			        }

			        float stepAngle = angleAround*(-0.5f+((float)i)/((float)(steps>>1)));
			        y = radius*(float)Math.Cos(stepAngle)*deltay/halfLength;
			        float s = (float)Math.Sin(stepAngle);
			        float nextStepAngle = angleAround*(-0.5f+((float)i+1)/((float)(steps>>1)));
			        float nexty = radius*(float)Math.Cos(nextStepAngle)*deltay/halfLength;
			        float nexts = (float)Math.Sin(nextStepAngle);
			        Begin(PrimitiveType.TriangleStrip,(steps+1) * 2);
			        for(j=0;j<=steps;j++) {
				        x=radius*s*(float)Math.Cos(2.0f*MathHelper.Pi*((float)j)/((float)steps));
				        float nextx=radius*nexts*(float)Math.Cos(2.0f*MathHelper.Pi*((float)j+1)/((float)steps));
				        z=radius*s*(float)Math.Sin(2.0f*MathHelper.Pi*((float)j)/((float)steps));
				        float nextz=radius*nexts*(float)Math.Sin(2.0f*MathHelper.Pi*((float)j+1)/((float)steps));
				        Vector3 normal = new Vector3(x, y, z);
				        normal.Normalize();
				        Normal(normal);
				        Vertex(x,y+deltay,z);
				        Vector3 nextNormal = new Vector3(nextx, nexty, nextz);
				        nextNormal.Normalize();
				        Normal(nextNormal);
				        Vertex(nextx,nexty+deltay,nextz);
			        }
			        End();
		        }

		        y=0;
		        float slopeCosine = (float)Math.Sqrt(1.0f-(slopeSine * slopeSine));
		        Begin(PrimitiveType.TriangleStrip,2*(steps+1));
		        for(j=0;j<=steps;j++) {
			        Vector3 normal = new Vector3(slopeCosine*(float)Math.Cos(2.0f*MathHelper.Pi*((float)j)/((float)steps)), -slopeSine, slopeCosine*(float)Math.Sin(2.0f*MathHelper.Pi*((float)j)/((float)steps)));

			        Normal(normal);
			        Vertex(radiusB*normal.X,y+verticalB,radiusB*normal.Z);
			        Vertex(radiusA*normal.X,y+verticalA,radiusA*normal.Z);
		        }
		        End();
	        }
	        else
	        {
		        // Draw a curved line in y,z for the top hemisphere.
		        x=0;
		        Begin(PrimitiveType.LineStrip,steps+1);
		        for(j=0;j<=steps;j++) {
			        float stepAngle = angleAroundA*(-0.5f+((float)j)/((float)steps));
			        y=radiusA*(float)Math.Cos(stepAngle);
			        z=radiusA*(float)Math.Sin(stepAngle);
			        Vertex(x,y+halfLength,z);
		        }
		        End();

		        // Draw a circle in x,z for the top hemisphere.
		        y=0;
		        Begin(PrimitiveType.LineStrip,steps*2+1);
		        for(j=0;j<=steps*2;j++) {
			        float stepAngle = 2.0f*MathHelper.Pi*((float)j)/((float)steps*2);
			        x=radiusA*(float)Math.Cos(stepAngle);
			        z=radiusA*(float)Math.Sin(stepAngle);
			        Vertex(x,y+verticalA,z);
		        }
		        End();

		        // Draw a curved line in x,y for the top hemisphere.
		        z=0;
		        Begin(PrimitiveType.LineStrip,steps+1);
		        for(j=0;j<=steps;j++) {
			        float stepAngle = angleAroundA*(-0.5f+((float)j)/((float)steps));
			        x=radiusA*(float)Math.Sin(stepAngle);
			        y=radiusA*(float)Math.Cos(stepAngle);
			        Vertex(x,y+halfLength,z);
		        }
		        End();
        		
		        // Draw a curved line in y,z for the bottom hemisphere.
		        x=0;
		        Begin(PrimitiveType.LineStrip,steps+1);
		        for(j=0;j<=steps;j++) {
			        float stepAngle = angleAroundB*(-0.5f+((float)j)/((float)steps));
			        y=-radiusB*(float)Math.Cos(stepAngle);
			        z=radiusB*(float)Math.Sin(stepAngle);
			        Vertex(x,y-halfLength,z);
		        }
		        End();

		        // Draw a circle in x,z for the bottom hemisphere.
		        y=0;
		        Begin(PrimitiveType.LineStrip,steps*2+1);
		        for(j=0;j<=steps*2;j++) {
			        float stepAngle = 2.0f*MathHelper.Pi*((float)j)/((float)steps*2);
			        x=radiusB*(float)Math.Cos(stepAngle);
			        z=radiusB*(float)Math.Sin(stepAngle);
			        Vertex(x,y+verticalB,z);
		        }
		        End();

		        // Draw a curved line in x,y for the bottom hemisphere.
		        z=0;
		        Begin(PrimitiveType.LineStrip,steps+1);
		        for(j=0;j<=steps;j++) {
			        float stepAngle = angleAroundB*(-0.5f+((float)j)/((float)steps));
			        x=radiusB*(float)Math.Sin(stepAngle);
			        y=-radiusB*(float)Math.Cos(stepAngle);
			        Vertex(x,y-halfLength,z);
		        }
		        End();

		        // Draw the shaft as a set of angled lines in a horizontal circle.
		        y=0;
		        for(j=0;j<steps;j++) {

			        Begin(PrimitiveType.LineList,2);

			        float cosineStep = (float)Math.Cos(2.0f*MathHelper.Pi*((float)j)/((float)steps));
			        float sineStep = (float)Math.Sin(2.0f*MathHelper.Pi*((float)j)/((float)steps));
			        x=radiusB*cosineStep;
			        z=radiusB*sineStep;
			        Vertex(x,y+verticalB,z);

			        x=radiusA*cosineStep;
			        z=radiusA*sineStep;
			        Vertex(x,y+verticalA,z);
			        End();
		        }
	        }
        }


        public void DrawCylinder(float length,float radius, Vector3 center,int steps,  bool cbDrawTop) {
	        Matrix  mtx = Matrix.Identity;
	        mtx.Translation = center;
	        DrawCylinder(length,radius,mtx,steps,cbDrawTop);
        }


        public void DrawCylinder(float length,float radius, Matrix mtx,int steps,  bool cbDrawTop) {
	        float x, y, z, halflength;
	        float prevX=0.0f, prevZ=0.0f;
	        int j;
	        WorldMatrix(mtx);

	        halflength = length * 0.5f;

	        y=0;
	        for(j=0;j<=steps;j++) {
		        x=radius*(float)Math.Cos(2.0f*MathHelper.Pi*((float)j)/((float)steps));
		        z=radius*(float)Math.Sin(2.0f*MathHelper.Pi*((float)j)/((float)steps));
		        if(cbDrawTop)
		        {
			        Begin(PrimitiveType.LineList,6);
			        {
				        Vertex(x,y-halflength,z);
				        Vertex(x,y+halflength,z);
				        Vertex(x,y+halflength,z);
				        Vertex(0,y+halflength,0);
				        Vertex(x,y-halflength,z);
				        Vertex(0,y-halflength,0);
			        }
			        End();
		        }
		        else
		        {
			        Begin(PrimitiveType.LineList,4);
			        {
				        Vertex(x,y-halflength,z);
				        Vertex(x,y+halflength,z);
				        Vertex(x,y+halflength,z);
				        Vertex(x,y-halflength,z);
			        }
			        End();
		        }

		        if (j>0 ) {
			        Begin(PrimitiveType.LineList,4);
			        Vertex(x,y+halflength,z);
			        Vertex(prevX,y+halflength,prevZ);
			        Vertex(x,y-halflength,z);
			        Vertex(prevX,y-halflength,prevZ);
			        End();
		        }

		        prevX = x;
		        prevZ = z;
	        }

	        for(j=0;j<steps;j++) {
	        }
        }


        public void DrawRect(float x1, float y1, float x2, float y2, float zVal)
        {
            DrawRect(x1, y1, x2, y2, zVal, 0.0f, 0.0f, 1.0f, 1.0f);
        }

        public void DrawRect(float x1, float y1, float x2, float y2)
        {
            DrawRect(x1, y1, x2, y2, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f);
        }

        public void DrawRect(float x1, float y1, float x2, float y2, float zVal, float u1, float v1, float u2, float v2)
        {
            Begin(PrimitiveType.TriangleFan, 4);

            TexCoord(u1, v1);
            Vertex(x1, y1, zVal);
            TexCoord(u2, v1);
            Vertex(x2, y1, zVal);
            TexCoord(u2, v2);
            Vertex(x2, y2, zVal);
            TexCoord(u1, v2);
            Vertex(x1, y2, zVal);

            End();
        }

        public const int MaxQuads = MaxVertices3 / 6;

        public void BeginQuads(int quadCount)
        {
            Debug.Assert(!m_InQuads, "Already in a BeginQuads block");
            Begin(PrimitiveType.TriangleList, quadCount * 6);
            m_InQuads = true;
        }

        public void DrawQuad(float x1, float y1, float x2, float y2, float zVal, float u1, float v1, float u2, float v2)
        {
            Debug.Assert(m_InQuads, "Can only call DrawQuad from inside a BeginQuads / EndQuads block");
            TexCoord(u1, v1);
            Vertex(x1, y1, zVal);
            TexCoord(u1, v2);
            Vertex(x1, y2, zVal);
            TexCoord(u2, v1);
            Vertex(x2, y1, zVal);
            TexCoord(u1, v2);
            Vertex(x1, y2, zVal);
            TexCoord(u2, v2);
            Vertex(x2, y2, zVal);
            TexCoord(u2, v1);
            Vertex(x2, y1, zVal);
        }

        public void EndQuads()
        {
            Debug.Assert(m_InQuads, "No matching EndQuads block");
            End();
            m_InQuads = false;
        }

    };
}
