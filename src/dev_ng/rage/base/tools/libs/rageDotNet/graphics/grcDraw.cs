using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace rage
{
    public partial class grcDraw : grcDisposable
    {
        public void Init(GraphicsDevice device)
        {
            m_Device = device;
            m_VertexBuffer = new DynamicVertexBuffer(m_Device, typeof(VNCTVert), VNCTVert.SizeInBytes * MaxVertices, BufferUsage.WriteOnly);
            m_Effect = new BasicEffect(m_Device, null);
            m_VNCTDeclaration = new VertexDeclaration(m_Device, VNCTVert.VertexElements);

            SetDefaults();
        }


        #region Primitive Drawing
        /// <summary>
        /// Begins a new primitive or set of primitives
        /// </summary>
        /// <param name="prim">The PrimitiveType for the prims </param>
        /// <param name="count">The number of verts to add to the prims</param>
        public void Begin(PrimitiveType prim, int count)
        {
            Debug.Assert(!m_InQuads, "Can't call Begin while in a BeginQuads block, call BeginQuads instead");
            //Debug.Assert(count > 0, "Count must be >= 0");
            //Debug.Assert(count < MaxVertices, "Too many verts in this draw call");
            m_PrimType = prim;
            m_VertexCount = 0;

            switch(m_PrimType)
            {
                case PrimitiveType.PointList:
                    m_PrimCount = count; break;
                case PrimitiveType.LineStrip:
                    m_PrimCount = count - 1; break;
                case PrimitiveType.LineList:
                    m_PrimCount = count / 2; break;
                case PrimitiveType.TriangleList:
                    m_PrimCount = count / 3; break;
                case PrimitiveType.TriangleFan:
                    m_PrimCount = count - 2; break;
                case PrimitiveType.TriangleStrip:
                    m_PrimCount = count - 2; break;
            }
            m_Vertices = new VNCTVert[count];
        }

        public void End()
        {
            Debug.Assert(!m_InQuads, "Can't call End while in a BeginQuads block, call EndQuads instead");
            m_VertexBuffer.SetData<VNCTVert>(m_Vertices);
            m_Device.Vertices[0].SetSource(m_VertexBuffer, 0, VNCTVert.SizeInBytes);
            m_Device.VertexDeclaration = m_VNCTDeclaration;

            m_Effect.Begin();
            foreach (EffectPass pass in m_Effect.CurrentTechnique.Passes)
            {
                pass.Begin();
                m_Device.DrawPrimitives(m_PrimType, 0, m_PrimCount);
                pass.End();
            }
            m_Effect.End();

            m_Vertices = null;
            m_PrimCount = 0;
            m_VertexCount = 0;
        }

        public void Vertex(Vector3 pos, Vector3 normal, Color color, Vector2 tex)
        {
            m_Vertices[m_VertexCount++] = new VNCTVert(pos, normal, color, tex);
        }

        public void Vertex(Vector3 v)
        {
            Vertex(v, m_CurrNormal, m_CurrColor, m_CurrTexCoord);
        }

        public void Vertex(float x, float y, float z)
        {
            Vertex(new Vector3(x, y, z), m_CurrNormal, m_CurrColor, m_CurrTexCoord);
        }

        public void Vertex(float x, float y)
        {
            Vertex(new Vector3(x, y, 0.0f), m_CurrNormal, m_CurrColor, m_CurrTexCoord);
        }

        public void Vertex(int x, int y)
        {
            Vertex(new Vector3(x, y, 0.0f), m_CurrNormal, m_CurrColor, m_CurrTexCoord);
        }

        public void Normal(float x, float y, float z)
        {
            m_CurrNormal = new Vector3(x, y, z);
        }

        public void Normal(Vector3 v)
        {
            m_CurrNormal = v;
        }

        public void Color(Color c)
        {
            m_CurrColor = c;
        }

        public void Color(uint c)
        {
            m_CurrColor = new Color(
                (byte)((c >> 16) & 0xFF),   //r
                (byte)((c >> 8) & 0xFF),    //g
                (byte)(c & 0xFF),           //b
                (byte)((c >> 24) & 0xFF)    //a
                );
        }

        public void Color(float r, float g, float b)
        {
            m_CurrColor = new Color(new Vector4(r, g, b, 1.0f));
        }

        public void Color(float r, float g, float b, float a)
        {
            m_CurrColor = new Color(new Vector4(r, g, b, a));
        }

        public void Color(Vector3 c)
        {
            m_CurrColor = new Color(c);
        }

        public void Color(Vector4 c)
        {
            m_CurrColor = new Color(c);
        }

        public void TexCoord(float x, float y)
        {
            m_CurrTexCoord = new Vector2(x, y);
        }

        public void TexCoord(Vector2 v)
        {
            m_CurrTexCoord = v;
        }
        #endregion

        #region Drawing State
        public void WorldMatrix(Matrix m)
        {
            m_Effect.World = m;
        }

        public void WorldIdentity()
        {
            m_Effect.World = Matrix.Identity;
        }

        public void SetDefaults()
        {
            // this sets the effect's matrices too
            Viewport = new grcViewport(this);

            // Should probably set everything here, so we don't have frame-to-frame state leaks
            m_Effect.Alpha = 1.0f;
            m_Effect.LightingEnabled = false;
            m_Effect.VertexColorEnabled = true;
            m_Effect.TextureEnabled = false;

            m_Device.RenderState.AlphaBlendEnable = true;
            m_Device.RenderState.SourceBlend = Blend.SourceAlpha;
            m_Device.RenderState.DestinationBlend = Blend.InverseSourceAlpha;
        }

        public Texture2D Texture
        {
            get {
                return m_Effect.Texture;
            }
            set {
                m_Effect.Texture = value;
            }
        }

        public BasicEffect Effect
        {
            get { return m_Effect; }
            set { m_Effect = value; }
        }

        public grcViewport Viewport
        {
            get { return m_Viewport; }
            set { 
                if (m_Viewport == value)
                {
                    return;
                }
                m_Viewport = value; 
                m_Effect.World = m_Viewport.World;
                m_Effect.View = m_Viewport.Camera;
                m_Effect.Projection = m_Viewport.Projection;
                m_Device.Viewport = m_Viewport.Window;
            }
        }

        public GraphicsDevice Device
        {
            get { return m_Device; }
        }

        #endregion

        #region grcDraw Internals

        private struct VNCTVert
        {
            public Vector3 Position;
            public Vector3 Normal;
            public Color Color;
            public Vector2 Texture;

            public VNCTVert(Vector3 position, Vector3 normal, Color color, Vector2 texture)
            {
                Position = position;
                Normal = normal;
                Color = color;
                Texture = texture;
            }

            public static VertexElement[] VertexElements = {
            new VertexElement(0, 0, 
                                VertexElementFormat.Vector3, 
                                VertexElementMethod.Default,
                                VertexElementUsage.Position, 0),
            new VertexElement(0, sizeof(float) * 3, 
                                VertexElementFormat.Vector3, 
                                VertexElementMethod.Default,
                                VertexElementUsage.Normal, 0),
            new VertexElement(0, sizeof(float) * (3 + 3), 
                                VertexElementFormat.Color, 
                                VertexElementMethod.Default,
                                VertexElementUsage.Color, 0),
            new VertexElement(0, sizeof(float) * (3 + 3) + sizeof(UInt32), 
                                VertexElementFormat.Vector2, 
                                VertexElementMethod.Default,
                                VertexElementUsage.TextureCoordinate, 0)};

            public const int SizeInBytes = sizeof(float) * (3 + 3 + 2) + sizeof(UInt32);
        };

        public const int MaxVertices = 1024;
        public const int MaxVertices3 = 1023;

        private VNCTVert[]      m_Vertices;
        private int             m_VertexCount = 0;
        private PrimitiveType   m_PrimType;
        private int             m_PrimCount;
        private VertexBuffer    m_VertexBuffer;
        private BasicEffect     m_Effect;
        private GraphicsDevice  m_Device;
        private VertexDeclaration m_VNCTDeclaration;
        private grcViewport     m_Viewport;
        private bool m_InQuads = false;

        private Color   m_CurrColor = Microsoft.Xna.Framework.Graphics.Color.White;
        private Vector2 m_CurrTexCoord = new Vector2(0.0f, 0.0f);
        private Vector3 m_CurrNormal = new Vector3(0.0f, 0.0f, 1.0f);

        protected override void ReleaseManagedResources()
        {
            // TODO: Release any managed resources. This includes calling
            // member.Dispose() on any owned member that 
            // implements IDisposable() and (optionally) setting references to null.
            m_VertexBuffer.Dispose();
            m_Effect.Dispose();
            m_VNCTDeclaration.Dispose();

            m_Device = null;
        }

        protected override void ReleaseUnmanagedResources()
        {
            // TODO: Release any unmanaged resources. 
        }

        #endregion

    }
}
