using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace rage
{
    class grcFont : grcDisposable
    {
        public grcFont() {
            m_Texture = null;
            m_CharWidth = 0;
            m_CharHeight = 0;
            m_FirstChar = 0;
            m_CharCount = 0;
            m_CharsPerRow = 0;
            m_PerCharacterWidths = null;
            m_InvWidth = 0.0f;
            m_InvHeight = 0.0f;
            m_InternalScale = 1.0f;
            m_InternalColor = new Color(255, 255, 255);
        }

        public float InternalScale
        {
            get { return m_InternalScale; }
            set { m_InternalScale = value; }
        }


        public Color InternalColor
        {
            get { return m_InternalColor; }
            set { m_InternalColor = value; }
        }
	
        public int GetWidth() {return (int)(m_InternalScale * m_CharWidth);}
        public int GetHeight() {return (int)(m_InternalScale * m_CharHeight);}

        public void DrawChar(grcDraw draw, float x, float y, float z, Color color, float scaleX, float scaleY, int ch)
        {
            // Hack for now; this only works if it's an ortho viewport
            const float shim = 0.5f;
            x = (float)Math.Floor(x) - shim;
            y = (float)Math.Floor(y) - shim;

            ch -= m_FirstChar;
            if (ch < 0 || ch >= m_CharCount)
            {
                // Draw something here?
                bool oldTextureEnabled = draw.Effect.TextureEnabled;
                draw.Effect.TextureEnabled = false;
                draw.DrawRect(x, y, x + ((float)m_CharWidth) * scaleX, y + ((float)m_CharHeight) * scaleY, z, 0.0f, 0.0f, 1.0f, 1.0f);
                draw.Effect.TextureEnabled = oldTextureEnabled;
                return;
            }

            int iu = (ch % m_CharsPerRow) * m_CharHeight;
            int iv = (ch / m_CharsPerRow) * m_CharHeight;
            
	        float u = (float)iu * m_InvWidth;
	        float v = (float)iv * m_InvHeight;
	        float du = (float)m_CharWidth * m_InvWidth;
	        float dv = (float)m_CharHeight * m_InvHeight;

            if (m_UseQuads)
            {
                draw.DrawQuad(x, y, x + ((float)m_CharWidth) * scaleX, y + ((float)m_CharHeight) * scaleY, z, u, v, u + du, v + dv);
            }
            else
            {
                Texture2D oldTexture = draw.Texture;
                draw.Texture = m_Texture;
                draw.DrawRect(x, y, x + ((float)m_CharWidth) * scaleX, y + ((float)m_CharHeight) * scaleY, z, u, v, u + du, v + dv);
                draw.Texture = oldTexture;
            }
        }
        
        public void DrawScaled(grcDraw draw, float x, float y, float z, Color color, float scaleX, float scaleY, string str) 
        {
            float start = x;
            scaleX *= m_InternalScale;
            scaleY *= m_InternalScale;
            int length = str.Length;

            Debug.Assert(length < grcDraw.MaxQuads, "Can't draw long strings yet");
            Texture2D oldTexture = draw.Texture;
            draw.Texture = m_Texture;
            draw.BeginQuads(length);
            m_UseQuads = true;

            for (int i = 0; i < length; i++)
            {
                DrawChar(draw, x, y, z, color, scaleX, scaleY, str[i]);
                if (str[i] == '\n')
                {
                    x = start;
                    y += scaleY * (float)m_CharHeight;
                }
                else if (m_PerCharacterWidths != null && str[i] >= m_FirstChar && str[i] < m_FirstChar + m_CharCount - 1)
                {
                    x += scaleX * (float)m_PerCharacterWidths[str[i] - m_FirstChar];
                }
                else
                {
                    x += scaleX * (float)m_CharWidth;
                }
            }

            m_UseQuads = false;
            draw.EndQuads();
            draw.Texture = oldTexture;
        }

        public void Draw(grcDraw draw, float x, float y, string str) 
        {
            DrawScaled(draw, x, y, 0.0f, new Color(255, 255, 255), 1.0f, 1.0f, str);
        }

        public static grcFont CreateFontFromBitmap(grcDraw draw, byte[] bits, int charWidth, int charHeight, int firstChar, int charCount) 
        {
            /*
            const int texWidth = 128;
            const int texHeight = 64;
            Texture2D texture = new Texture2D(draw.Device, texWidth, texHeight, 1, ResourceUsage.None, SurfaceFormat.Dxt1);
            int charsPerRow = texWidth / charWidth;

            for(int c = 0; c < charCount; c++) {
                int x = (c % charsPerRow) * charWidth;
                int y = (c / charsPerRow) * charHeight;
                const UInt32 white = ~0U;
                const UInt32 black = 0;
                for (int row = 0; row < charHeight; row++)
                {
                    for(int col = 0; col < charWidth; col++) 
                    {
                    }
                }
            }
            */
            return null;
        }
        public static grcFont CreateFontFromFile(string filename, int spacing) { return null; }

        // static grcFont CreateFontFromImage(grcImage image, int charWidth, int charHeight, int firstChar, int charCount, int spacing);

        protected override void ReleaseManagedResources()
        {
            m_Texture.Dispose();
        }

        protected override void ReleaseUnmanagedResources() {}

        private Color m_InternalColor;
        private float m_InternalScale;
        private Texture2D m_Texture;
        private int m_CharWidth, m_CharHeight, m_FirstChar, m_CharCount, m_CharsPerRow;
        private bool m_UseQuads;
        byte[] m_PerCharacterWidths;
        float m_InvWidth, m_InvHeight;
    }
}
