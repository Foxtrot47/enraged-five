using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace rage
{
    public delegate void GrcPaintDelegate(object sender, grcDraw draw);

    public delegate void GrcDeviceDelegate(object sender, GraphicsDevice dev);

    [DefaultEvent("GrcPaint")]
    public partial class grcPanel : UserControl
    {
        public grcPanel()
        {
            InitializeComponent();
            this.Disposed += new EventHandler(grcPanel_Disposed);
            SetStyle(ControlStyles.Opaque | ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint, true);
        }

        [Browsable(false)]
        public grcDraw Draw
        {
            get { return m_Draw; }
        }

        [Browsable(false)]
        public GraphicsDevice Device
        {
            get { return m_Device; }
        }

        [Browsable(true),
        DefaultValue(typeof(System.Drawing.Color), "Blue"),
        Category("grcPanel")]
        public System.Drawing.Color ClearColor
        {
            get { return System.Drawing.Color.FromArgb(m_ClearColor.A, m_ClearColor.R, m_ClearColor.G, m_ClearColor.B); }
            set { m_ClearColor = new Microsoft.Xna.Framework.Graphics.Color(value.R, value.G, value.B, value.A); 
                if (DesignMode)
                {
                    Invalidate();
                }
            }
        }

        [Browsable(true),
        DefaultValue(false),
        Category("grcPanel")]
        public bool HasDepthBuffer
        {
            get { return m_HasDepthBuffer; }
            set { m_HasDepthBuffer = value; }
        }

        [Browsable(true),
        Category("grcPanel")]
        public event GrcPaintDelegate GrcPaint;

        [Browsable(true),
        Category("grcPanel")]
        public event GrcDeviceDelegate DeviceCreated;

        protected Microsoft.Xna.Framework.Graphics.Color m_ClearColor;

        private bool m_HasDepthBuffer;

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e); 

            if (DesignMode) return;

            CreateD3DGraphics();
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            if (DesignMode) return;

            ResetGraphics();
        }

        public void CreateD3DGraphics()
        {
            PresentationParameters param = new PresentationParameters();
            param.BackBufferCount = 1;
            param.BackBufferHeight = Height;
            param.BackBufferWidth = Width;
            param.IsFullScreen = false;
            m_Device = new GraphicsDevice(
                GraphicsAdapter.DefaultAdapter,
                DeviceType.Hardware,
                Handle,
                param);

            m_Device.DeviceReset += OnDeviceReset;
            m_Device.DeviceResetting += OnDeviceResetting;

            m_Draw = new grcDraw();
            m_Draw.Init(m_Device);

            if (DeviceCreated != null)
            {
                DeviceCreated.Invoke(this, m_Device);
            }
        }

        protected void OnDeviceResetting(object sender, EventArgs e)
        {
        }

        protected void OnDeviceReset(object sender, EventArgs e)
        {
        }

        public void ResetGraphics()
        {
            if (m_Device == null)
            {
                return; // could be called before we've created the device
            }

            PresentationParameters param = new PresentationParameters();
            param.BackBufferCount = 1;
            param.BackBufferHeight = Height;
            param.BackBufferWidth = Width;
            param.IsFullScreen = false;
            if (m_Device != null)
            {
                m_Device.Reset(param);
            }

        }

        protected override void OnPaint(PaintEventArgs e)
        {
            if (DesignMode)
            {
                System.Drawing.SolidBrush b = new System.Drawing.SolidBrush(ClearColor);
                e.Graphics.FillRectangle(b, 0, 0, Width, Height);

                System.Drawing.Drawing2D.LinearGradientBrush b2 = 
                    new System.Drawing.Drawing2D.LinearGradientBrush(
                        new System.Drawing.Point(Width * 1 / 4, Height * 1 / 4), 
                        new System.Drawing.Point(Width , Height ), 
                        System.Drawing.Color.White, 
                        System.Drawing.Color.Black);
                e.Graphics.FillEllipse(b2, Width * 0.25f, Height * 0.25f, Width * 0.5f, Height * 0.5f);

                string text = "Teapot goes here";
                System.Drawing.Size size = e.Graphics.MeasureString(text, Font).ToSize();
                e.Graphics.DrawString(text, this.Font, System.Drawing.Brushes.Black, (Width - size.Width) / 2, (Height - size.Height) / 2);

                return;
            }

            switch(m_Device.GraphicsDeviceStatus)
            {
                case GraphicsDeviceStatus.Lost:
                    // do nothing. Invalidate so we try again later
                    Invalidate();
                    break;
                case GraphicsDeviceStatus.NotReset:
                    // reset the device
                    Invalidate();
                    try
                    {
                        ResetGraphics();
                    }
                    catch (DeviceLostException)
                    {
                    }
                    break;
                case GraphicsDeviceStatus.Normal:
                    {
                        m_Device.Clear(m_ClearColor);
                        m_Draw.SetDefaults();
                        OnGrcPaint();
                        try {
                            m_Device.Present();
                        }
                        catch(DeviceLostException) {
                            Invalidate();
                        }
                    }
                    break;
            }
        }

        protected virtual void OnGrcPaint()
        {
            GrcPaintDelegate paint = GrcPaint;
            if (paint != null)
            {
                paint(this, m_Draw);
            }
        }

        protected GraphicsDevice    m_Device = null;
        protected grcDraw m_Draw = null;

        void grcPanel_Disposed(object sender, EventArgs e)
        {
            if (Disposing)
            {
                if (m_Device != null)
                {
                    m_Device.Dispose();
                    m_Device = null;
                }
                if (m_Draw != null)
                {
                    m_Draw.Dispose();
                    m_Draw = null;
                }

                GrcPaint = null;

            }
            // clean up unmanaged resources here
        }
    }
}
