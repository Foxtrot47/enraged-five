﻿using System;
using System.Collections.Generic;

namespace RSG.Interop.Incredibuild.XGE
{

    /// <summary>
    /// XGE tool interface.
    /// </summary>
    public interface ITool : RSG.Base.Command.ICommandLineTool
    {
        #region Properties
        /// <summary>
        /// Tool name.
        /// </summary>
        String Name { get; set; }
        
        /// <summary>
        /// Output prefix printed during build.
        /// </summary>
        String OutputPrefix { get; set; }

        /// <summary>
        /// Group prefix printed during build.
        /// </summary>
        String GroupPrefix { get; set; }

        /// <summary>
        /// Output file masks for files created during build (e.g. "*.obj,*.so").
        /// </summary>
        String Masks { get; set; }

        /// <summary>
        /// Allow remote tool execution.
        /// </summary>
        bool AllowRemote { get; set; }

        /// <summary>
        /// Absolute path to tool.
        /// </summary>
        String Path { get; set; }

        /// <summary>
        /// Tool command line parameters.
        /// </summary>
        String Parameters { get; set; }
        
        /// <summary>
        /// What return codes are defined as successful? (e.g. "0..5").  
        /// Default "0".
        /// </summary>
        String SuccessExitCodes { get; set; }

        /// <summary>
        /// Tasks returning this exit code will be displayed as a yellow bar in 
        /// the Build Monitor Progress page and will be counted as warnings in 
        /// the Summary page (e.g. "0..5").
        /// </summary>
        String WarningExitCodes { get; set; }

        /// <summary>
        /// Comma-separated list of strings which, when found in tool output, 
        /// will cause IncrediBuild to re-assign the task to another Agent and 
        /// discard the previous output. 
        /// </summary>
        String AutoRecover { get; set; }

        /// <summary>
        /// Do not allow concurrent instances of this tool on a single Agent.
        /// </summary>
        bool SingleInstancePerAgent { get; set; }

        /// <summary>
        /// Allow tool to restart on the local agent.
        /// </summary>
        bool AllowRestartOnLocal { get; set; }

        /// <summary>
        /// Number of seconds limit for a single task.
        /// </summary>
        int TimeLimit { get; set; }
        #endregion // Properties
    }

} // RSG.Interop.Incredibuild.XGE namespace
