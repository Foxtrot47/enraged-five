﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Interop.Incredibuild.XGE
{

    /// <summary>
    /// 
    /// </summary>
    public interface ITaskJobGroup : 
        ITask, 
        ISerialisableComponent
    {
        /// <summary>
        /// 
        /// </summary>
        Environment Environment { get; set; }

        /// <summary>
        /// Sub-tasks and task groups.
        /// </summary>
        ICollection<ITask> Tasks { get; }

        /// <summary>
        /// If set to true, build will stop immediately if the task fails (default: false).
        /// </summary>
        bool StopOnErrors { get; }
    }

} // RSG.Interop.Incredibuild namespace
