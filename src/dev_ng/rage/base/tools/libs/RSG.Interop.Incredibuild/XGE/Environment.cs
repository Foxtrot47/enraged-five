﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Linq;

namespace RSG.Interop.Incredibuild.XGE
{

    /// <summary>
    /// XGE Environment abstraction.  An XGE project is executed within the
    /// confines of an Environment.
    /// </summary>
    /// The environment defines the tools that the project can utilise and a 
    /// set of project-specific variable/value pairs.
    /// 
    public class Environment : ISerialisableComponent
    {
        #region Properties
        /// <summary>
        /// Environment name.
        /// </summary>
        public String Name
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public List<ITool> Tools
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public Dictionary<String, String> Variables
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public Environment(String name, IEnumerable<ITool> tools = null)
        {
            this.Name = name;
            this.Tools = new List<ITool>();
            if (null != tools)
                this.Tools.AddRange(tools);
            this.Variables = new Dictionary<String, String>();
        }
        #endregion // Constructor(s)

        #region ISerialisableComponent Methods
        /// <summary>
        /// Serialise this component to an XElement object.
        /// </summary>
        /// <returns></returns>
        public XElement ToXml()
        {
            XElement xmlElem = new XElement("Environment");
            xmlElem.SetAttributeValue("Name", this.Name);
            XElement xmlTools = new XElement("Tools");
            xmlElem.Add(xmlTools);
            foreach (Tool tool in this.Tools)
            {
                xmlTools.Add(tool.ToXml());
            }
            XElement xmlVariables = new XElement("Variables");
            xmlElem.Add(xmlVariables);
            foreach (KeyValuePair<String, String> kvp in this.Variables)
            {
                XElement xmlVariable = new XElement("Variable");
                xmlVariable.SetAttributeValue("Name", kvp.Key);
                xmlVariable.SetAttributeValue("Value", kvp.Value);
                xmlVariables.Add(xmlVariable);
            }

            return (xmlElem);
        }
        #endregion // ISerialisableComponent Methods
    }

} // RSG.Interop.Incredibuild.XGE namespace
