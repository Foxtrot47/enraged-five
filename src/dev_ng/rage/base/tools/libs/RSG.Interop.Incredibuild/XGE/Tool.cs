﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Linq;

namespace RSG.Interop.Incredibuild.XGE
{

    /// <summary>
    /// XGE tool abstraction.
    /// </summary>
    public class Tool : 
        ITool,
        ISerialisableComponent
    {
        #region Properties
        /// <summary>
        /// Tool name.
        /// </summary>
        public String Name
        {
            get;
            set;
        }

        /// <summary>
        /// Output prefix printed during build.
        /// </summary>
        public String OutputPrefix
        {
            get;
            set;
        }

        /// <summary>
        /// Group prefix printed during build.
        /// </summary>
        public String GroupPrefix
        {
            get;
            set;
        }

        /// <summary>
        /// Output file masks for files created during build (e.g. "*.obj,*.so").
        /// </summary>
        public String Masks
        {
            get;
            set;
        }

        /// <summary>
        /// Allow remote tool execution.
        /// </summary>
        public bool AllowRemote
        {
            get;
            set;
        }

        /// <summary>
        /// Absolute path to tool.
        /// </summary>
        public String Path
        {
            get;
            set;
        }

        /// <summary>
        /// Tool command line parameters.
        /// </summary>
        public String Parameters
        {
            get;
            set;
        }

        /// <summary>
        /// What return codes are defined as successful? (e.g. "0..5").  
        /// Default "0".
        /// </summary>
        public String SuccessExitCodes 
        { 
            get; 
            set; 
        }

        /// <summary>
        /// Tasks returning this exit code will be displayed as a yellow bar in 
        /// the Build Monitor Progress page and will be counted as warnings in 
        /// the Summary page (e.g. "0..5").
        /// </summary>
        public String WarningExitCodes
        { 
            get; 
            set; 
        }

        /// <summary>
        /// Comma-separated list of strings which, when found in tool output, 
        /// will cause IncrediBuild to re-assign the task to another Agent and 
        /// discard the previous output. 
        /// </summary>
        public String AutoRecover 
        { 
            get; 
            set; 
        }

        /// <summary>
        /// Do not allow concurrent instances of this tool on a single Agent.
        /// </summary>
        public bool SingleInstancePerAgent 
        { 
            get; 
            set; 
        }

        /// <summary>
        /// Allow tool to restart on the local agent.
        /// </summary>
        public bool AllowRestartOnLocal
        {
            get;
            set;
        }

        /// <summary>
        /// Number of seconds limit for a single task.
        /// </summary>
        public int TimeLimit
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor; initialising to default state.
        /// </summary>
        public Tool()
        {
            this.Name = String.Empty;
            this.OutputPrefix = String.Empty;
            this.GroupPrefix = String.Empty;
            this.Masks = "*.*";
            this.AllowRemote = true;
            this.Path = String.Empty;
            this.Parameters = String.Empty;
            this.SuccessExitCodes = "0";
            this.WarningExitCodes = String.Empty;
            this.AutoRecover = String.Empty;
            this.SingleInstancePerAgent = false;
            this.AllowRestartOnLocal = false;
            this.TimeLimit = -1;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        public Tool(String name)
            : this()
        {
            this.Name = name;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="path"></param>
        /// <param name="parameters"></param>
        public Tool(String name, String path, String parameters)
            : this()
        {
            this.Name = name;
            this.Path = path;
            this.Parameters = parameters;
        }
        #endregion // Constructor(s)
        
        #region ISerialisableComponent Methods
        /// <summary>
        /// Serialise this component to an XElement object.
        /// </summary>
        /// <returns></returns>
        public XElement ToXml()
        {
            XElement xmlElem = new XElement("Tool");
            xmlElem.SetAttributeValue("Name", this.Name);
            xmlElem.SetAttributeValue("OutputPrefix", this.OutputPrefix);
            xmlElem.SetAttributeValue("OutputFileMasks", this.Masks);
            xmlElem.SetAttributeValue("AllowRemote", this.AllowRemote ? "TRUE" : "FALSE");
            xmlElem.SetAttributeValue("Path", this.Path);
            xmlElem.SetAttributeValue("Params", this.Parameters);
            xmlElem.SetAttributeValue("GroupPrefix", this.GroupPrefix);
            xmlElem.SetAttributeValue("AllowRestartOnLocal", this.AllowRestartOnLocal ? "TRUE" : "FALSE");
            xmlElem.SetAttributeValue("SingleInstancePerAgent", this.SingleInstancePerAgent ? "TRUE" : "FALSE");
            if (this.TimeLimit > 0)
                xmlElem.SetAttributeValue("TimeLimit", this.TimeLimit.ToString());

            return (xmlElem);
        }
        #endregion // ISerialisableComponent Methods
    }

} // RSG.Interop.Incredibuild.XGE namespace
