﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Interop.Incredibuild.XGE
{

    /// <summary>
    /// XGE project interface.
    /// </summary>
    public interface IProject : ISerialisableComponent
    {
        #region Properties
        /// <summary>
        /// Project name.
        /// </summary>
        String Name { get; set; }

        /// <summary>
        /// Project environment.
        /// </summary>
        Environment DefaultEnvironment { get; set; }

        /// <summary>
        /// Project working directory (inherited by used tool).
        /// </summary>
        String WorkingDirectory { get; set; }

        /// <summary>
        /// 
        /// </summary>
        ICollection<ITask> Tasks { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Write the configuration data file to disk.
        /// </summary>
        /// <param name="filename"></param>
        void Write(String filename);
        #endregion // Methods
    }

} // RSG.Interop.Incredibuild.XHE namespace
