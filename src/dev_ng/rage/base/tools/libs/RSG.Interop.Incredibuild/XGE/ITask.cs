﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Interop.Incredibuild.XGE
{

    /// <summary>
    /// Shared interface between task jobs and task job groups; supports 
    /// treating them as a single collection as dependencies can be listed for 
    /// tasks and task groups.
    /// </summary>
    public interface ITask : 
        RSG.Base.Command.ICommandLineToolTask,
        ISerialisableComponent
    {
        /// <summary>
        /// Task name.
        /// </summary>
        String Name { get; set; }

        /// <summary>
        /// Tool to use for this job or job group.
        /// </summary>
        RSG.Base.Command.ICommandLineTool Tool { get; set; }

        /// <summary>
        /// Task dependencies.
        /// </summary>
        ICollection<ITask> Dependencies { get; }

        /// <summary>
        /// Working directory for the task.
        /// </summary>
        String WorkingDirectory { get; set; }
    }

} // RSG.Interop.Incredibuild.XGE namespace
