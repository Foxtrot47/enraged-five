﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Linq;

namespace RSG.Interop.Incredibuild.XGE
{

    /// <summary>
    /// XGE TaskGroup abstraction.  An XGE Project contains an set of Task
    /// and TaskGroup objects.  A TaskGroup contains an set of Task and
    /// sub-TaskGroup objects.
    /// </summary>
    public class TaskGroup :  ITaskJobGroup
    {
        #region Properties
        /// <summary>
        /// Task name.
        /// </summary>
        public String Name
        {
            get;
            set;
        }

        /// <summary>
        /// XGE tool object to use for this task.
        /// </summary>
        public RSG.Base.Command.ICommandLineTool Tool
        {
            get;
            set;
        }

        /// <summary>
        /// Working path (optional).
        /// </summary>
        public String WorkingDirectory
        {
            get;
            set;
        }

        /// <summary>
        /// Environment object (optional).
        /// </summary>
        public Environment Environment
        {
            get;
            set;
        }

        /// <summary>
        /// Collection of ITask objects (TaskJob or TaskJobGroup).
        /// </summary>
        public ICollection<ITask> Tasks
        {
            get;
            private set;
        }

        /// <summary>
        /// Task dependencies.
        /// </summary>
        public ICollection<ITask> Dependencies
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public bool StopOnErrors
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="tasks"></param>
        /// <param name="env"></param>
        /// <param name="tool"></param>
        /// <param name="wd"></param>
        public TaskGroup(String name, IEnumerable<ITask> tasks = null, Environment env = null, Tool tool = null, String wd = null)
        {
            this.Name = name;
            this.Tool = tool;
            this.Dependencies = new List<ITask>();
            this.WorkingDirectory = wd;
            this.Environment = env;
            this.StopOnErrors = false;
            this.Tasks = new List<ITask>();
            if (null != tasks)
                (this.Tasks as List<ITask>).AddRange(tasks);
        }
        #endregion // Constructor(s)

        #region ISerialisableComponent Methods
        /// <summary>
        /// Serialise this component to an XElement object.
        /// </summary>
        /// <returns></returns>
        public XElement ToXml()
        {
            XElement xmlElem = new XElement("TaskGroup");
            if (!String.IsNullOrEmpty(this.Name))
                xmlElem.SetAttributeValue("Name", this.Name);
            if (null != this.Tool)
                xmlElem.SetAttributeValue("Tool", this.Tool.Name);
            IEnumerable<String> deps = this.Dependencies.Select(t => t.Name);
            String dependsOn = String.Join(";", deps);
            if (!String.IsNullOrEmpty(dependsOn))
                xmlElem.SetAttributeValue("DependsOn", dependsOn);
            if (!String.IsNullOrEmpty(this.WorkingDirectory))
                xmlElem.SetAttributeValue("WorkingDir", this.WorkingDirectory);
            if (null != this.Environment)
                xmlElem.SetAttributeValue("Env", this.Environment.Name);
            xmlElem.SetAttributeValue("StopOnErrors", this.StopOnErrors ? "TRUE" : "FALSE");
            if (!String.IsNullOrEmpty(this.WorkingDirectory) && Directory.Exists(this.WorkingDirectory))
                xmlElem.SetAttributeValue("WorkingDir", this.WorkingDirectory);

            foreach (ITask task in this.Tasks)
            {
                xmlElem.Add(task.ToXml());
            }

            return (xmlElem);
        }
        #endregion // ISerialisableComponent Methods
    }

} // RSG.Interop.Incredibuild.XGE namespace
