﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;

namespace RSG.Interop.Incredibuild.XGE
{

    /// <summary>
    /// Xoreax Incredibuild XGE XML Interface class.
    /// </summary>
    public static class XGE
    {
        #region Constants
        /// <summary>
        /// xgconsole successful exit code.
        /// </summary>
        private const int ExitSuccess = 0;
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Absolute path to XGE XML Interface Schema.
        /// </summary>
        public static String XmlInterfaceSchema
        {
            get;
            private set;
        }

        /// <summary>
        /// Absolute path to XGE console executable.
        /// </summary>
        public static String XGConsoleExecutablePath
        {
            get;
            private set;
        }

        /// <summary>
        /// XGE log.
        /// </summary>
        internal static IUniversalLog Log
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Static constructor.
        /// </summary>
        static XGE()
        {
            XGConsoleExecutablePath = Incredibuild.XGConsoleExecutablePath;
            XmlInterfaceSchema = Path.Combine(Incredibuild.InstallDirectory,
                "XGE", "Schema for XML Interface.xsd");
            LogFactory.Initialize();
            XGE.Log = LogFactory.CreateUniversalLog("XGE");
        }
        #endregion // Constructor(s)

        #region Static Controller Methods
        /// <summary>
        /// Determine whether XGE build is already running on this machine.
        /// </summary>
        /// <returns></returns>
        public static bool IsBuildRunning()
        {
            String processName = Path.GetFileNameWithoutExtension(Incredibuild.XGCONSOLE);
            System.Diagnostics.Process[] processes =
                System.Diagnostics.Process.GetProcessesByName(processName);
            return (processes.Any());
        }

        /// <summary>
        /// Start an XGE project from a pre-serialised XGE XML file.
        /// </summary>
        /// <param name="title"></param>
        /// <param name="filename"></param>
        /// <param name="logfile"></param>
        /// <param name="rebuild"></param>
        /// <param name="showcmd"></param>
        /// <param name="show_monitor"></param>
        /// <returns></returns>
        public static bool Start(String title, String filename, String logfile, 
            bool rebuild = false, bool showcmd = true, bool show_monitor = true)
        {
            Debug.Assert(File.Exists(filename), "Xoreax XGE Input File Error",
                "XGE XML input file: {0} does not exist.", filename);
            if (!File.Exists(filename))
                throw new FileNotFoundException("XGE XML Interface file not found",
                    filename);

            StringBuilder arguments = new StringBuilder();
            arguments.Append(filename);
            arguments.AppendFormat(" /TITLE=\"{0}\" ", title);
            arguments.Append("/NOLOGO ");
            arguments.AppendFormat("/LOG={0} ", logfile);
            arguments.AppendFormat("/MON={0} ", Path.ChangeExtension(logfile, "ib_mon"));
            arguments.Append("/SHOWTIME /SHOWAGENT ");
            if (rebuild)
                arguments.Append("/REBUILD ");
            if (showcmd)
                arguments.Append("/SHOWCMD ");
            if (show_monitor)
                arguments.Append("/OPENMONITOR ");

            int exitCode = XGE.Run(XGE.XGConsoleExecutablePath, arguments.ToString(), null, null);
            return (ExitSuccess == exitCode);
        }
        
        /// <summary>
        /// Stop a running XGE build.
        /// </summary>
        /// <returns></returns>
        public static bool Stop()
        {
            Debug.Assert(XGE.IsBuildRunning(),
                "XGE Build is not currently running.");
            if (!XGE.IsBuildRunning())
                return (false);

            StringBuilder arguments = new StringBuilder();
            arguments.Append("/STOP");
            int exitCode = XGE.Run(XGE.XGConsoleExecutablePath, arguments.ToString(), null, null);
            return (ExitSuccess == exitCode);
        }

        /// <summary>
        /// Attach to a running XGE build;
        /// </summary>
        /// <param name="handler"></param>
        /// <returns></returns>
        public static bool Attach(DataReceivedEventHandler stdoutHandler, DataReceivedEventHandler stderrHandler)
        {
            Debug.Assert(XGE.IsBuildRunning(),
                "XGE Build is not currently running.");
            if (!XGE.IsBuildRunning())
                return (false);

            StringBuilder arguments = new StringBuilder();
            arguments.Append("/ATTACH");
            int exitCode = XGE.Run(XGE.XGConsoleExecutablePath, arguments.ToString(), stdoutHandler, stderrHandler);
            return (ExitSuccess == exitCode);
        }
        #endregion // Static Controller Methods

        #region Private Methods
        /// <summary>
        /// Invoke XGE.
        /// </summary>
        /// <param name="exe"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        private static int Run(String exe, String args, bool waitForExit)
        {
            return (Run(exe, args, null, null));
        }

        /// <summary>
        /// Invoke XGE; optionally capturing stdout and stderr output.
        /// </summary>
        /// <param name="exe"></param>
        /// <param name="args"></param>
        /// <param name="stdoutHandler"></param>
        /// <param name="stderrHandler"></param>
        /// <returns></returns>
        private static int Run(String exe, String args, DataReceivedEventHandler stdoutHandler, 
            DataReceivedEventHandler stderrHandler)
        {            
            bool useStdoutRedirectHandler = (null != stdoutHandler);
            bool useStderrRedirectHandler = (null != stderrHandler);

            Process process = new Process();
            int exit_code = 0;
            try
            {
                process.StartInfo.FileName = exe;
                process.StartInfo.Arguments = args;
                process.StartInfo.CreateNoWindow = true;
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardOutput = useStdoutRedirectHandler;
                process.StartInfo.RedirectStandardError = useStderrRedirectHandler;
                if (useStdoutRedirectHandler)
                    process.OutputDataReceived += stdoutHandler;
                if (useStderrRedirectHandler)
                    process.ErrorDataReceived += stderrHandler;

                process.Start();

                if (useStdoutRedirectHandler)
                    process.BeginOutputReadLine();
                if (useStderrRedirectHandler)
                    process.BeginErrorReadLine();

                // Wait for process to exit.
                process.WaitForExit();
                exit_code = process.ExitCode;
            }
            finally
            {
                process.Close();
            }

            return (exit_code);
        }
        #endregion // Private Methods
    }

} // RSG.Interop.Incredibuild.XGE namespace
