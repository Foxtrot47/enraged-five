﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using RSG.Base.Logging;

namespace RSG.Interop.Incredibuild.XGE
{

    /// <summary>
    /// XGE single task object.
    /// </summary>
    public class TaskJob : ITaskJob
    {
        #region Properties
        /// <summary>
        /// Task name.
        /// </summary>
        public String Name
        {
            get;
            set;
        }

        /// <summary>
        /// Source file for this Task.
        /// </summary>
        public String SourceFile
        {
            get;
            set;
        }

        /// <summary>
        /// Caption (displayed in the Build Monitor, optional).
        /// </summary>
        public String Caption
        {
            get;
            set;
        }

        /// <summary>
        /// List of output files.
        /// </summary>
        public ICollection<String> OutputFiles
        {
            get;
            private set;
        }

        /// <summary>
        /// List of input files.
        /// </summary>
        public ICollection<String> InputFiles
        {
            get;
            private set;
        }

        /// <summary>
        /// Task dependencies.
        /// </summary>
        public ICollection<ITask> Dependencies
        {
            get;
            private set;
        }

        /// <summary>
        /// XGE tool object to use for this task.
        /// </summary>
        public RSG.Base.Command.ICommandLineTool Tool
        {
            get;
            set;
        }

        /// <summary>
        /// Parameters string.
        /// </summary>
        public String Parameters
        {
            get;
            set;
        }

        /// <summary>
        /// Task working directory.
        /// </summary>
        public String WorkingDirectory
        {
            get;
            set;
        }

        /// <summary>
        /// Skip task if a failure occurred (default: false).
        /// </summary>
        public bool SkipIfProjectFailed
        {
            get;
            set;
        }

        /// <summary>
        /// If set to true, build will stop immediately if the task fails (default: false).
        /// </summary>
        public bool StopOnErrors
        {
            get;
            set;
        }

        /// <summary>
        /// Add $(inherited:params) to parameters.
        /// </summary>
        public bool InheritParams
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="name"></param>
        public TaskJob(String name)
            : this(name, String.Empty)
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public TaskJob(String name, String source_file)
        {
            if (!String.IsNullOrEmpty(source_file))
            {
                Debug.Assert(File.Exists(source_file),
                    String.Format("XGE Task source file {0} does not exist.", source_file));
                if (!File.Exists(source_file))
                    XGE.Log.Warning("XGE Task source file '{0}' does not exist.", source_file);
            }

            this.Name = name;
            this.SourceFile = source_file;
            this.OutputFiles = new List<String>();
            this.InputFiles = new List<String>();
            this.Dependencies = new List<ITask>();
            this.SkipIfProjectFailed = false;
            this.StopOnErrors = false;
        }
        #endregion // Constructor(s)

        #region ISerialisableComponent Methods
        /// <summary>
        /// Serialise this component to an XElement object.
        /// </summary>
        /// <returns></returns>
        public XElement ToXml()
        {
            XElement xmlElem = new XElement("Task");
            xmlElem.SetAttributeValue("Name", this.Name);
            xmlElem.SetAttributeValue("SourceFile", this.SourceFile);
            if (!String.IsNullOrEmpty(this.Caption))
                xmlElem.SetAttributeValue("Caption", this.Caption);
            if (this.OutputFiles.Count > 0)
            {
                String outputFiles = JoinStringArray(this.OutputFiles, ";");
                xmlElem.SetAttributeValue("OutputFiles", outputFiles);
            }
            if (this.InputFiles.Count > 0)
            {
                String inputFiles = JoinStringArray(this.InputFiles, ";");
                xmlElem.SetAttributeValue("InputFiles", inputFiles);
            }
            if (null != this.Tool)
                xmlElem.SetAttributeValue("Tool", this.Tool.Name);

            // Only inherit parameters when there are tool parameters.  This
            // prevents $(inherited:param) missing warnings on build.
            if (this.InheritParams && !String.IsNullOrWhiteSpace(this.Tool.Parameters))
                xmlElem.SetAttributeValue("Params", String.Format("$(inherited:params) {0}", this.Parameters));
            else
                xmlElem.SetAttributeValue("Params", this.Parameters);
            
            if (!String.IsNullOrEmpty(this.Caption))
                xmlElem.SetAttributeValue("Caption", this.Caption);
            xmlElem.SetAttributeValue("SkipIfProjectFailed", this.SkipIfProjectFailed ? "TRUE" : "FALSE");
            IEnumerable<String> deps = this.Dependencies.Select(t => t.Name);
            String dependsOn = String.Join(";", deps);
            if (!String.IsNullOrEmpty(dependsOn))
                xmlElem.SetAttributeValue("DependsOn", dependsOn);
            if (!String.IsNullOrEmpty(this.WorkingDirectory))
                xmlElem.SetAttributeValue("WorkingDir", this.WorkingDirectory);
            xmlElem.SetAttributeValue("StopOnErrors", this.StopOnErrors ? "TRUE" : "FALSE");

            return (xmlElem);
        }
        #endregion // ISerialisableComponent Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="join"></param>
        /// <returns></returns>
        private String JoinStringArray(IEnumerable<String> data, String join)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (String d in data)
            {
                sb.Append(d);
                sb.Append(join);
            }
            return (sb.ToString());
        }
        #endregion // Private Methods
    }

} // RSG.Interop.Incredibuild.XGE namespace
