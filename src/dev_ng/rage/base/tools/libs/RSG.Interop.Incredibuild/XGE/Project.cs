﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;

namespace RSG.Interop.Incredibuild.XGE
{

    /// <summary>
    /// Incredibuild XGE XML Interface helper class.  This class and set of
    /// utility classes in the Pipeline::Util::XGE module help to construct
    /// the XML input file.
    /// </summary>
    public class Project : 
        IProject,
        ISerialisableComponent
    {
        #region Constants
        /// <summary>
        /// XGE XML interface format version.
        /// </summary>
        private static readonly int FORMAT_VERSION = 1;
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Project name.
        /// </summary>
        public String Name
        {
            get;
            set;
        }

        /// <summary>
        /// Environment default for project (required).
        /// </summary>
        public Environment DefaultEnvironment
        {
            get;
            set;
        }

        /// <summary>
        /// Working directory (can be overridden per-tool).
        /// </summary>
        public String WorkingDirectory
        {
            get;
            set;
        }

        /// <summary>
        /// List of ITask objects (Task or TaskGroup).
        /// </summary>
        public ICollection<ITask> Tasks
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public Project()
            : this(String.Empty, null)
        {
        }

        /// <summary>
        /// Constructor. 
        /// </summary>
        /// <param name="name"></param>
        public Project(String name)
            : this(name, null)
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="env"></param>
        public Project(String name, Environment env)
        {
            this.Name = name;
            this.DefaultEnvironment = env;
            this.WorkingDirectory = String.Empty;
            this.Tasks = new List<ITask>();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Write out the project to an XGE XML Interface configuration file.
        /// </summary>
        /// <param name="filename"></param>
        public void Write(String filename)
        {
            Debug.Assert(null != this.DefaultEnvironment,
                "Project requires a Default Environment.");
            if (null == this.DefaultEnvironment)
                throw new ConfigurationException("Project requires a Default Environment.");

            XDocument xmlDoc = new XDocument(
                new XDeclaration("1.0", "utf-8", "yes"),
                new XComment(String.Format("Generated at {0}.", DateTime.Now)),
                ToXml()
            );
            String leadingDirectory = Path.GetDirectoryName(filename);
            if (!Directory.Exists(leadingDirectory))
                Directory.CreateDirectory(leadingDirectory);

            xmlDoc.Save(filename);
        }
        #endregion // Controller Methods

        #region ISerialisableComponent Methods
        /// <summary>
        /// Serialise this component to XElement object.
        /// </summary>
        /// <returns></returns>
        public XElement ToXml()
        {
            IEnumerable<Environment> uniqueEnvs = GetUniqueEnvironments();
            Debug.Assert(uniqueEnvs.Count() > 0, "No project environments.");
            if (0 == uniqueEnvs.Count())
                throw new NotSupportedException("XGE XML Interface Projects require at least one Environment.");

            XElement xmlElem = new XElement("BuildSet");
            xmlElem.SetAttributeValue("FormatVersion", FORMAT_VERSION.ToString());
            XElement xmlEnvironments = new XElement("Environments");
            xmlElem.Add(xmlEnvironments);
            foreach (Environment env in uniqueEnvs)
            {
                xmlEnvironments.Add(env.ToXml());
            }

            XElement xmlProject = new XElement("Project");
            xmlElem.Add(xmlProject);
            xmlProject.SetAttributeValue("Name", this.Name);
            xmlProject.SetAttributeValue("Env", this.DefaultEnvironment.Name);
            xmlProject.SetAttributeValue("WorkingDir", this.WorkingDirectory);
            foreach (ITask task in this.Tasks)
            {
                xmlProject.Add(task.ToXml());
            }

            return (xmlElem);
        }
        #endregion // ISerialisableComponent Methods

        #region Private Methods
        /// <summary>
        /// Return unique Environment objects within this Project.
        /// </summary>
        /// <returns></returns>
        private IEnumerable<Environment> GetUniqueEnvironments()
        {
            List<Environment> envs = new List<Environment>();
            if (null != this.DefaultEnvironment)
                envs.Add(this.DefaultEnvironment);
            foreach (ITask task in this.Tasks)
            {
                if (!(task is TaskGroup))
                    continue;

                if (null != (task as TaskGroup).Environment)
                    envs.Add((task as TaskGroup).Environment);
            }

            return (envs);
        }
        #endregion // Private Methods
    }

    /// <summary>
    /// XGE Project configuration exception.
    /// </summary>
    public class ConfigurationException : Exception
    {
        #region Constructor(s)
        public ConfigurationException()
            : base()
        {
        }

        public ConfigurationException(String message)
            : base(message)
        {
        }

        public ConfigurationException(String message, Exception inner)
            : base(message, inner)
        {
        }

        public ConfigurationException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context)
            : base(info, context)
        {
        }
        #endregion // Constructor(s)
    }

} // RSG.Interop.Incredibuild.XGE namespace
