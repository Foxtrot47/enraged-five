﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Linq;

namespace RSG.Interop.Incredibuild.XGE
{

    /// <summary>
    /// Interface to support a ToXml serialisation method.
    /// </summary>
    public interface ISerialisableComponent
    {
        #region Methods
        /// <summary>
        /// Serialise this component to an XElement.
        /// </summary>
        /// <returns></returns>
        XElement ToXml();
        #endregion // Methods
    }

} // RSG.Interop.Incredibuild.XGE namespace
