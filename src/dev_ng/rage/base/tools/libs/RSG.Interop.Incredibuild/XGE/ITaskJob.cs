﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Interop.Incredibuild.XGE
{

    /// <summary>
    /// Interface for a concrete task.
    /// </summary>
    public interface ITaskJob : ITask
    {
        /// <summary>
        /// 
        /// </summary>
        String SourceFile { get; set; }

        /// <summary>
        /// 
        /// </summary>
        ICollection<String> OutputFiles { get; }

        /// <summary>
        /// 
        /// </summary>
        ICollection<String> InputFiles { get; }

        /// <summary>
        /// 
        /// </summary>
        String Parameters { get; set; }

        /// <summary>
        /// 
        /// </summary>
        String Caption { get; set; }

        /// <summary>
        /// Skip task if a failure occurred (default: false).
        /// </summary>
        bool SkipIfProjectFailed { get; set; }

        /// <summary>
        /// If set to true, build will stop immediately if the task fails (default: false).
        /// </summary>
        bool StopOnErrors { get; set; }

        /// <summary>
        /// 
        /// </summary>
        bool InheritParams { get; set; }
    }

} // RSG.Interop.Incredibuild namespace
