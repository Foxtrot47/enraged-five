﻿using System;
using System.ComponentModel;
using System.IO;
using System.ServiceProcess;
using Microsoft.Win32;

namespace RSG.Interop.Incredibuild
{

    /// <summary>
    /// Xoreax Incredibuild information class.
    /// </summary>
    public static class Incredibuild
    {
        #region Constants
        private static readonly String REGISTRY_KEY = "Software\\Xoreax\\IncrediBuild\\Builder";
        private static readonly String VERSION_KEY = "VersionText";
        private static readonly String INSTALL_DIR_KEY = "Folder";

        // Agent Settings Registry Keys
        private static readonly String ENABLED = "LastEnabled";
        private static readonly String STANDALONE = "Standalone";
        private static readonly String FORCE_CPU_COUNT = "ForceCPUCount";
        
        public static readonly String XGCONSOLE = "xgconsole.exe";
        private static readonly String XGAGENTSERVICE = "Incredibuild_Agent";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Incredibuild installed status.
        /// </summary>
        public static bool IsInstalled
        {
            get;
            private set;
        }

        /// <summary>
        /// Incredibuild installed version.
        /// </summary>
        public static String Version
        {
            get;
            private set;
        }

        /// <summary>
        /// Incredibuild Agent enabled status.
        /// </summary>
        /// Note: this is deliberately dynamic to pick up whether the agent is
        /// enabled/disabled.
        /// 
        public static bool IsAgentEnabled
        {
            get
            {
                using (RegistryKey key = OpenXoreaxRootKey(RegistryHive.LocalMachine))
                {
                    if (null != key)
                    {
                        String enabledStateStr = (String)key.GetValue(ENABLED);
                        int enabledState = 0;
                        if (int.TryParse(enabledStateStr, out enabledState))
                            return ((0 == enabledState) ? false : true);
                        else
                            return (false);
                    }
                    else
                    {
                        return (false);
                    }
                }
            }
        }

        /// <summary>
        /// Incredibuild Agent standalone status.
        /// </summary>
        /// Note: this is deliberately dynamic to pick up whether the agent is
        /// enabled/disabled.
        /// 
        public static bool IsAgentStandalone
        {
            get
            {
                using (RegistryKey key = OpenXoreaxRootKey(RegistryHive.LocalMachine))
                {
                    if (null != key)
                    {
                        String standaloneStateStr = (String)key.GetValue(STANDALONE);
                        int standaloneState = 0;
                        if (int.TryParse(standaloneStateStr, out standaloneState))
                            return ((0 == standaloneState) ? false : true);
                        else
                            return (false);
                    }
                    else
                    {
                        return (false);
                    }
                }
            }
        }

        /// <summary>
        /// Incredibuild Agent configured CPU count. 
        /// </summary>
        /// Note: this is deliberately dynamic to pick up the agent CPU status.
        /// 
        public static int AgentForceCPUCount
        {
            get
            {
                using (RegistryKey key = OpenXoreaxRootKey(RegistryHive.LocalMachine))
                {
                    if (null != key)
                    {
                        String cpuCountStr = (String)key.GetValue(FORCE_CPU_COUNT);
                        int cpuCount = 0;
                        if (int.TryParse(cpuCountStr, out cpuCount))
                            return (cpuCount);
                        else
                            return (-1);
                    }
                    else
                    {
                        return (-1);
                    }
                }
            }
        }

        /// <summary>
        /// Incredibuild installation directory.
        /// </summary>
        public static String InstallDirectory
        {
            get;
            private set;
        }

        /// <summary>
        /// Absolute path to XGE console executable.
        /// </summary>
        public static String XGConsoleExecutablePath
        {
            get;
            private set;
        }

        /// <summary>
        /// Absolute path to Coordinator Monitor executable.
        /// </summary>
        public static String CoordinatorMonitorExecutablePath
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Static constructor; fetches everything from the registry in one swoop.
        /// </summary>
        static Incredibuild()
        {
            using (RegistryKey key = OpenXoreaxRootKey(RegistryHive.LocalMachine))
            {
                if (null != key)
                {
                    Version = key.GetValue(VERSION_KEY).ToString();
                    InstallDirectory = key.GetValue(INSTALL_DIR_KEY).ToString();
                    IsInstalled = (Version.Length > 0);
                    XGConsoleExecutablePath = Path.Combine(InstallDirectory, XGCONSOLE);
                    CoordinatorMonitorExecutablePath = Path.Combine(InstallDirectory, "CoordMonitor.exe");
                    key.Close();
                }
                else
                {
                    Version = String.Empty;
                    InstallDirectory = String.Empty;
                    IsInstalled = false;
                    XGConsoleExecutablePath = String.Empty;
                    CoordinatorMonitorExecutablePath = String.Empty;
                }
            }
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Determines whether the Incredibuild Agent Service is running.
        /// </summary>
        /// <returns></returns>
        /// <see cref="http://stackoverflow.com/questions/178147/how-can-i-verify-if-a-windows-service-is-running"/>
        /// 
        public static bool IsAgentServiceRunning()
        {
            try
            {
                ServiceController sc = new ServiceController(XGAGENTSERVICE);
                switch (sc.Status)
                {
                    case ServiceControllerStatus.Running:
                        return (true);
                    default:
                        return (false);
                }
            }
            catch (Win32Exception)
            {
                // An error occurred when accessing a system API.
            }
            catch (InvalidOperationException)
            {
                // Service was not found.
            }
            catch (ArgumentException)
            {
                // Service-name is invalid.
            }
            return (false);
        }
        #endregion // Controller Methods

        #region Private Static Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="hive"></param>
        /// <returns></returns>
        private static RegistryKey OpenXoreaxRootKey(RegistryHive hive)
        {
            // Xoreax Incredibuild is a 32-bit process; so its registry data
            // will always (currently) be in the 32-bit registry.
            using (RegistryKey keyBase = RegistryKey.OpenBaseKey(hive, RegistryView.Registry32))
            {
                if (null == keyBase)
                    throw new Exception("32-bit HKLM registry key open failed.");

                RegistryKey key = keyBase.OpenSubKey(REGISTRY_KEY);
                return (key);
            }
        }
        #endregion // Private Static Methods
    }

} // RSG.Interop.Incredibuild namespace
