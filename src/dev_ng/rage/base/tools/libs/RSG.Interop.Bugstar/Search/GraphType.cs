﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Interop.Bugstar.Search
{

    /// <summary>
    /// The various types of graphs that bugstar can provide
    /// </summary>
    public enum GraphType
    {
        Pie,
        Bar,
        Column,
        DataScreen
    }

} // RSG.Interop.Bugstar.Search namespace
