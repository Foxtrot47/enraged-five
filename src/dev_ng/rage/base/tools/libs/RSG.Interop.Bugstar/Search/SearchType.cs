﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Interop.Bugstar.Search
{
    /// <summary>
    /// Type of search this is.
    /// </summary>
    public enum SearchType
    {
        /// <summary>
        /// Basic search.
        /// </summary>
        PublicSearch,

        /// <summary>
        /// Graph.
        /// </summary>
        Graph,

        /// <summary>
        /// Report.
        /// </summary>
        RestReportInstance,

        /// <summary>
        /// Group.
        /// </summary>
        Group
    }
}
