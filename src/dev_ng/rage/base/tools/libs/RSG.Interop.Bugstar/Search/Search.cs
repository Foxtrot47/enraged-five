﻿using System;
using System.Xml;
using RSG.Base.Net;
using RSG.Interop.Bugstar.Common;
using RSG.Interop.Bugstar.Organisation;
using System.Collections.Generic;
using System.IO;

namespace RSG.Interop.Bugstar.Search
{
    /// <summary>
    /// 
    /// </summary>
    public class Search : BugstarObject, IComparable<Search>
    {
        #region Properties
        /// <summary>
        /// The query to use to query bugstar about this object
        /// </summary>
        public override IRestQuery Query
        {
            get
            {
                string searchString = null;
                switch (SearchType)
                {
                    case SearchType.Graph:
                        searchString = Commands.BUGSTAR_GRAPHS;
                        break;

                    case SearchType.RestReportInstance:
                        searchString = Commands.BUGSTAR_REPORTS;
                        break;

                    case SearchType.PublicSearch:
                        searchString = String.Format("{0}/{1}",
                            Commands.BUGSTAR_BUGS, Commands.BUGSTAR_SEARCHES);
                        break;

                    case SearchType.Group:
                        searchString = Commands.BUGSTAR_GROUPS;
                        break;

                    default:
                        throw new ArgumentException(String.Format("Unsupported search type '{0}' encountered while attempting to perform search.", SearchType.ToString()));
                }

                BugstarQueryBuilder qb = new BugstarQueryBuilder(User.Query);
                qb.RelativeQuery = String.Format("{0}/{1}", searchString, this.Id);
                return (qb.ToQuery());
            }
        }

        /// <summary>
        /// The user this search is for
        /// </summary>
        public User User
        {
            get;
            protected set;
        }

        /// <summary>
        /// Name of the search
        /// </summary>
        public string Name
        {
            get;
            protected set;
        }

        /// <summary>
        /// Type of search this is
        /// </summary>
        public SearchType SearchType
        {
            get;
            protected set;
        }

        /// <summary>
        /// Description of the search
        /// </summary>
        public string Description
        {
            get;
            protected set;
        }

        /// <summary>
        /// Share type search is
        /// </summary>
        public ShareType ShareType
        {
            get;
            protected set;
        }
        #endregion

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        public Search(User user)
            : base(user.Connection)
        {
            User = user;
        }
        #endregion // Constructor(s)

        #region XML Serialisation
        /// <summary>
        /// Generates an object from its XML representation.
        /// </summary>
        /// <param name="node"></param>
        public override void Deserialise(XmlNode node)
        {
            base.Deserialise(node);

            XmlNode nameNode = node.SelectSingleNode("name");
            if (nameNode == null)
            {
                throw new ArgumentException("Name node missing for search.");
            }
            Name = nameNode.InnerText;

            // optional
            XmlNode descriptionNode = node.SelectSingleNode("description");
            if (descriptionNode != null)
            {
                Description = descriptionNode.InnerText;
            }

            XmlNode searchTypeNode = node.SelectSingleNode("type");
            if (searchTypeNode == null)
            {
                throw new ArgumentException("Share type node missing for search.");
            }
            SearchType = (SearchType)Enum.Parse(typeof(SearchType), searchTypeNode.InnerText, true);

            XmlNode shareTypeNode = node.SelectSingleNode("shareType");
            if (shareTypeNode == null)
            {
                throw new ArgumentException("Share type node missing for search.");
            }
            ShareType = (ShareType)Enum.Parse(typeof(ShareType), shareTypeNode.InnerText, true);
        }
        #endregion // XML Serialisation

        #region Public Methods
        /// <summary>
        /// Queries bugstar for a list of bugs that are associated with this mission
        /// </summary>
        /// <returns></returns>
        public IList<Bug> GetBugs()
        {
            return GetBugs("Id");
        }

        /// <summary>
        /// Queries bugstar for a list of bugs that are associated with this mission returning the requested fields
        /// </summary>
        /// <param name="fields"></param>
        /// <returns></returns>
        public IList<Bug> GetBugs(string fields)
        {
            const int pageSize = 200;

            if (SearchType != SearchType.PublicSearch)
            {
                throw new ArgumentException("Not possible to retrieve bugs for a non public search type.");
            }

            IList<Bug> bugs = new List<Bug>();
            int currentPage = 0;

            // The resulting stream should contain xml, so convert it and extract the information we are after
            String searchString = null;
            switch (this.SearchType)
            {
                case SearchType.Graph:
                    searchString = Commands.BUGSTAR_GRAPHS;
                    break;

                case SearchType.RestReportInstance:
                    searchString = Commands.BUGSTAR_REPORTS;
                    break;

                case SearchType.PublicSearch:
                    searchString = String.Format("{0}/{1}",
                    Commands.BUGSTAR_BUGS, Commands.BUGSTAR_SEARCHES);
                    break;

                default:
                    throw new ArgumentException(String.Format("Unsupported search type '{0}' encountered while attempting to perform search.", SearchType.ToString()));
            }
                    
            BugstarQueryBuilder qb = new BugstarQueryBuilder(User.Query);
            qb.RelativeQuery = String.Format("{0}/{1}", searchString, this.Id);
            qb.Parameters.Add("fields", fields);
            qb.Parameters.Add("page", currentPage.ToString());
            qb.Parameters.Add("pageSize", pageSize.ToString());
                        
            bool keepProcessing = true;
            while (keepProcessing)
            {
                BugstarQuery query = qb.ToQuery();
                keepProcessing = false;

				//TODO: DHM FIX ME: should use RunQueryWithXDocumentResult!
                using (Stream queryResult = Connection.RunQuery(query))
                {
                    if (queryResult != null)
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.Load(queryResult);

                        XmlNode rootNode = doc.SelectSingleNode("/bugs");
                        XmlNodeList bugNodes = doc.SelectNodes("/bugs/bug");
                        foreach (XmlNode bugNode in bugNodes)
                        {
                            Bug bug = new Bug(this.User.Project);
                            bug.Deserialise(bugNode);
                            bugs.Add(bug);
                        }

                        // Check if we need to request more data
                        XmlAttribute att = (XmlAttribute)rootNode.Attributes.GetNamedItem("totalPages");
                        if (att != null)
                        {
                            int totalPages = Int32.Parse(att.Value);
                            if ((currentPage + 1) < totalPages)
                            {
                                currentPage++;
                                qb.Parameters["page"] = currentPage.ToString();
                                keepProcessing = true;
                            }
                        }
                    }
                }
            }

            return bugs;
        }
        #endregion // Public Methods

        #region IComparable<Mission> Implementation
        /// <summary>
        /// 
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(Search other)
        {
            // If other is not a valid object reference, this instance is greater.
            if (other == null)
            {
                return 1;
            }

            // Just compare the names
            return this.Name.CompareTo(other.Name);
        }
        #endregion // IComparable<MapGrid> Implementation
    } // SearchBase
} // RSG.Interop.Bugstar.Search
