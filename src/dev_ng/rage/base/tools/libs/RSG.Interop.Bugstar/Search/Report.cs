﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using RSG.Base.Net;
using RSG.Interop.Bugstar.Common;
using RSG.Interop.Bugstar.Organisation;

namespace RSG.Interop.Bugstar.Search
{
    /// <summary>
    /// Don't think this should be inheriting from search...
    /// </summary>
    public class Report :
        BugstarObject
    {
        #region Properties
        /// <summary>
        /// The query to use to query bugstar about this object
        /// 
        /// Note: For reports the following query returns the report data, not the report information
        /// The bugstar query for this should be changed at some point.
        /// e.g.
        /// .../User/{id}/Reports/{id}            - returns id/name/description/etc info
        /// .../User/{id}/Reports/{id}/Data       - returns report data
        /// .../User/{id}/Reports/{id}/Stylesheet - returns report stylesheet
        /// </summary>
        public override IRestQuery Query
        {
            get
            {
                BugstarQueryBuilder qb = new BugstarQueryBuilder(User.Query);
                qb.RelativeQuery = String.Format("{0}/{1}", Commands.BUGSTAR_REPORTS, this.Id);

                return (qb.ToQuery());
            }
        }

        /// <summary>
        /// The user that this report is part of
        /// </summary>
        public User User
        {
            get;
            protected set;
        }

        /// <summary>
        /// Name of the search
        /// </summary>
        public string Name
        {
            get;
            protected set;
        }

        /// <summary>
        /// Description of the search
        /// </summary>
        public string Description
        {
            get;
            protected set;
        }

        /// <summary>
        /// The sub type of the report as seen in url:bugstar:1359032.
        /// </summary>
        public ReportSubType SubType
        {
            get;
            protected set;
        }

        /// <summary>
        /// Data retrieved when querying bugstar about this report
        /// </summary>
        public Stream DataStream
        {
            get
            {
                return Connection.RunQuery(Query);
            }
        }

        /// <summary>
        /// Stylesheet for this report
        /// </summary>
        public Stream StylesheetStream
        {
            get
            {
				//TODO: DHM FIX ME: should use RunQueryWithXDocumentResult! and abstract XDocument result
                BugstarQueryBuilder qb = new BugstarQueryBuilder(this.Query);
                qb.RelativeQuery = Commands.BUGSTAR_STYLESHEET;

                return Connection.RunQuery(qb.ToQuery());
            }
        }
        #endregion
        
        #region Constructor(s)
        /// <summary>
        /// Default Constructor
        /// </summary>
        /// <param name="user"></param>
        public Report(User user)
            : base(user.Connection)
        {
            User = user;
        }
        #endregion // Constructor(s)

        #region XML Serialisation
        /// <summary>
        /// Generates an object from its XML representation.
        /// </summary>
        /// <param name="node"></param>
        public override void Deserialise(XmlNode node)
        {
            base.Deserialise(node);

            XmlNode nameNode = node.SelectSingleNode("name");
            if (nameNode != null)
            {
                Name = nameNode.InnerText;
            }

            XmlNode descriptionNode = node.SelectSingleNode("description");
            if (descriptionNode != null)
            {
                Description = descriptionNode.InnerText;
            }

            XmlNode subTypeNode = node.SelectSingleNode("subType");
            string subType = "Unknown";
            if (subTypeNode != null)
            {
                subType = subTypeNode.InnerText;
            }

            switch (subType)
            {
                case "report":
                    this.SubType = ReportSubType.Report;
                    break;
                case "screen":
                    this.SubType = ReportSubType.Screen;
                    break;
                case "notification":
                    this.SubType = ReportSubType.Notification;
                    break;
                case "flashReport":
                    this.SubType = ReportSubType.FlashReport;
                    break;
                default:
                    this.SubType = ReportSubType.Unknown;
                    break;
            }
        }
        #endregion // XML Serialisation

        #region Static Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static Report GetReportById(User user, uint id)
        {
            // This isn't very optimal. See comment attached to the BugstarQuery property for
            // the changes that should be made.
            List<Report> reports = user.Reports.ToList();
            return reports.Find(item => item.Id == id);
        }
        #endregion
    }
}
