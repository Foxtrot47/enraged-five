﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Interop.Bugstar.Search
{
    /// <summary>
    /// Accessibility of the share
    /// </summary>
    public enum ShareType
    {
        /// <summary>
        /// Shared with everyone.
        /// </summary>
        Global,

        /// <summary>
        /// Some specifically shared this with you.
        /// </summary>
        Shared,

        /// <summary>
        /// Created by the local user and thus modifiable.
        /// </summary>
        Modifiable
    }
}
