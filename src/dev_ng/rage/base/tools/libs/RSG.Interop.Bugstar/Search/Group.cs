﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using RSG.Base.Net;
using RSG.Interop.Bugstar.Attributes;
using RSG.Interop.Bugstar.Common;
using RSG.Interop.Bugstar.Organisation;

namespace RSG.Interop.Bugstar.Search
{
    /// <summary>
    /// 
    /// </summary>
    public class Group : BugstarObject, IComparable<Group>
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="Description"/> property.
        /// </summary>
        private string _description;

        /// <summary>
        /// Private field for the <see cref="Name"/> property.
        /// </summary>
        private string _name;

        /// <summary>
        /// Private field for the <see cref="ShareType"/> property.
        /// </summary>
        private ShareType _shareType;

        /// <summary>
        /// Private field for the <see cref="User"/> property.
        /// </summary>
        private readonly User _user;
        #endregion

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        public Group(User user)
            : base(user.Connection)
        {
            _user = user;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Description of this group.
        /// </summary>
        public string Description
        {
            get { return _description; }
        }

        /// <summary>
        /// Name of the group.
        /// </summary>
        public string Name
        {
            get { return _name; }
        }

        /// <summary>
        /// The query to use to query bugstar about this object
        /// </summary>
        public override IRestQuery Query
        {
            get
            {
                BugstarQueryBuilder qb = new BugstarQueryBuilder(User.Query);
                qb.RelativeQuery = String.Format("{0}/{1}/{2}", Commands.BUGSTAR_BUGS, Commands.BUGSTAR_GROUPS, this.Id);
                return qb.ToQuery();
            }
        }

        /// <summary>
        /// Share type search is
        /// </summary>
        public ShareType ShareType
        {
            get { return _shareType; }
        }

        /// <summary>
        /// The user this group is for.
        /// </summary>
        public User User
        {
            get { return _user; }
        }
        #endregion

        #region XML Serialisation
        /// <summary>
        /// Generates an object from its XML representation.
        /// </summary>
        /// <param name="node"></param>
        public override void Deserialise(XmlNode node)
        {
            base.Deserialise(node);

            XmlNode nameNode = node.SelectSingleNode("name");
            if (nameNode != null)
            {
                _name = nameNode.InnerText;
            }

            XmlNode descriptionNode = node.SelectSingleNode("description");
            if (descriptionNode != null)
            {
                _description = descriptionNode.InnerText;
            }

            XmlNode shareTypeNode = node.SelectSingleNode("shareType");
            if (shareTypeNode != null)
            {
                _shareType = (ShareType)Enum.Parse(typeof(ShareType), shareTypeNode.InnerText, true);
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Queries bugstar for a list of bugs that are associated with this mission
        /// </summary>
        /// <returns></returns>
        public IList<Bug> GetBugs()
        {
            return GetBugs(new BugField[] { BugField.Id });
        }

        /// <summary>
        /// Queries bugstar for a list of bugs that are associated with this mission returning the requested fields
        /// </summary>
        /// <param name="fields"></param>
        /// <returns></returns>
        public IList<Bug> GetBugs(IEnumerable<BugField> fields)
        {
            const int pageSize = 1000;

            IList<Bug> bugs = new List<Bug>();
            int currentPage = 0;

            BugstarQueryBuilder qb = new BugstarQueryBuilder(Query);
            qb.Parameters.Add("fields", String.Join(",", fields.Select(item => item.GetBugstarData())));
            qb.Parameters.Add("page", currentPage.ToString());
            qb.Parameters.Add("pageSize", pageSize.ToString());

            bool keepProcessing = true;
            while (keepProcessing)
            {
                BugstarQuery query = qb.ToQuery();
                keepProcessing = false;

                //TODO: DHM FIX ME: should use RunQueryWithXDocumentResult!
                using (Stream queryResult = Connection.RunQuery(query))
                {
                    if (queryResult != null)
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.Load(queryResult);

                        XmlNode rootNode = doc.SelectSingleNode("/bugs");
                        XmlNodeList bugNodes = doc.SelectNodes("/bugs/bug");
                        foreach (XmlNode bugNode in bugNodes)
                        {
                            Bug bug = new Bug(this.User.Project);
                            bug.Deserialise(bugNode);
                            bugs.Add(bug);
                        }

                        // Check if we need to request more data
                        XmlAttribute att = (XmlAttribute)rootNode.Attributes.GetNamedItem("totalPages");
                        if (att != null)
                        {
                            int totalPages = Int32.Parse(att.Value);
                            if ((currentPage + 1) < totalPages)
                            {
                                currentPage++;
                                qb.Parameters["page"] = currentPage.ToString();
                                keepProcessing = true;
                            }
                        }
                    }
                }
            }

            return bugs;
        }
        #endregion

        #region IComparable<Group> Implementation
        /// <summary>
        /// Compares this bugstar group against another.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(Group other)
        {
            // If other is not a valid object reference, this instance is greater.
            if (other == null)
            {
                return 1;
            }

            // Just compare the names.
            return Name.CompareTo(other.Name);
        }
        #endregion
    }
}
