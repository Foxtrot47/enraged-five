﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Interop.Bugstar.Organisation;
using RSG.Interop.Bugstar.Common;

namespace RSG.Interop.Bugstar.Search
{
    /*
    /// <summary>
    /// This shouldn't inherit from search...
    /// </summary>
    public class PublicSearch : Search
    {
        #region Properties
        /// <summary>
        /// The query to use to query bugstar about this object
        /// </summary>
        public override BugstarQuery Query
        {
            get
            {
                return new BugstarQuery(User.Query, String.Format("{0}/{1}/{2}", Commands.BUGSTAR_BUGS, Commands.BUGSTAR_SEARCHES, Id.ToString()));
            }
        }
        #endregion
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        public PublicSearch(User user)
            : base(user)
        {
        }
        #endregion // Constructor(s)
    } // PublicSearch
    */
} // RSG.Interop.Bugstar.Search
