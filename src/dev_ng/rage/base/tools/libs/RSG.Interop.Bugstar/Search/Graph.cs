﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using RSG.Base.Logging;
using RSG.Base.Net;
using RSG.Interop.Bugstar.Common;
using RSG.Interop.Bugstar.Organisation;

namespace RSG.Interop.Bugstar.Search
{

    /// <summary>
    /// 
    /// </summary>
    public class Graph : BugstarObject
    {
        #region Constants
        private static readonly String LOG_CTX = "Bugstar:Graph";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// The query to use to query bugstar about this object
        /// </summary>
        public override IRestQuery Query
        {
            get
            {
                BugstarQueryBuilder qb = new BugstarQueryBuilder(User.Query);
                qb.RelativeQuery = String.Format("{0}/{1}", Commands.BUGSTAR_GRAPHS, this.Id);
                return qb.ToQuery();
            }
        }

        /// <summary>
        /// The user this graph is for
        /// </summary>
        public User User
        {
            get;
            protected set;
        }

        /// <summary>
        /// Name of the graph
        /// </summary>
        public string Name
        {
            get;
            protected set;
        }

        /// <summary>
        /// Description of the graph
        /// </summary>
        public string Description
        {
            get;
            protected set;
        }

        /// <summary>
        /// Title of the graph's x-axis
        /// </summary>
        public string XAxisTitle
        {
            get;
            protected set;
        }

        /// <summary>
        /// Title of the graph's y-axis
        /// </summary>
        public string YAxisTitle
        {
            get;
            protected set;
        }

        /// <summary>
        /// Type of graph this is
        /// </summary>
        public GraphType GraphType
        {
            get;
            protected set;
        }

        /// <summary>
        /// Field the data is grouped by
        /// </summary>
        public string GroupedField
        {
            get;
            protected set;
        }

        /// <summary>
        /// Data points that contain the graph data
        /// </summary>
        public List<KeyValuePair<String, int>> DataPoints
        {
            get
            {
                return GetDataPointsList();
            }
        }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="user"></param>
        public Graph(User user)
            : base(user.Connection)
        {
            User = user;
        }
        #endregion // Constructor(s)

        #region XML Serialisation
        /// <summary>
        /// Generates an object from its XML representation.
        /// </summary>
        /// <param name="node"></param>
        public override void Deserialise(XmlNode node)
        {
            base.Deserialise(node);

            XmlNode nameNode = node.SelectSingleNode("name");
            if (nameNode == null)
            {
                throw new ArgumentException("Name node missing for search.");
            }
            Name = nameNode.InnerText;

            // optional
            XmlNode descriptionNode = node.SelectSingleNode("description");
            if (descriptionNode != null)
            {
                Description = descriptionNode.InnerText;
            }

            XmlNode xAxisTitleNode = node.SelectSingleNode("xAxisTitle");
            if (xAxisTitleNode != null)
            {
                XAxisTitle = xAxisTitleNode.InnerText;
            }

            XmlNode yAxisTitleNode = node.SelectSingleNode("yAxisTitle");
            if (yAxisTitleNode != null)
            {
                YAxisTitle = yAxisTitleNode.InnerText;
            }

            XmlNode graphTypeNode = node.SelectSingleNode("graphType");
            if (graphTypeNode != null)
            {
                switch (graphTypeNode.InnerText)
                {
                    case "pieGraph":
                        GraphType = GraphType.Pie;
                        break;

                    case "barGraph":
                        GraphType = GraphType.Bar;
                        break;

                    case "columnGraph":
                        GraphType = GraphType.Column;
                        break;

                    case "dataScreen":
                        GraphType = GraphType.DataScreen;
                        break;

                    default:
                        throw new ArgumentException("Unsupported search type encountered.");
                }
            }

            XmlNode groupedFieldNode = node.SelectSingleNode("groupField");
            if (groupedFieldNode != null)
            {
                GroupedField = groupedFieldNode.InnerText;
            }
        }
        #endregion // XML Serialisation

        #region Static Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static Graph GetGraphById(User user, uint id)
        {
            Graph graph = null;

            try
            {
                BugstarQueryBuilder qb = new BugstarQueryBuilder(user.Query);
                qb.RelativeQuery = String.Format("{0}/{1}", Commands.BUGSTAR_GRAPHS, id);
                BugstarQuery query = qb.ToQuery();
	
				//TODO: DHM FIX ME: should use RunQueryWithXDocumentResult!
                using (Stream queryResult = user.Connection.RunQuery(query))
                {
                    if (queryResult != null)
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.Load(queryResult);

                        XmlNode graphNode = doc.SelectSingleNode("/graph");
                        if (graphNode != null)
                        {
                            graph = new Graph(user);
                            graph.Deserialise(graphNode);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                BugstarConnection.Log.ToolExceptionCtx(LOG_CTX, ex, 
                    "Unhandled exception while getting information for user id: {0}.", id);
            }

            return graph;
        }
        #endregion // Static Controller Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private List<KeyValuePair<String, int>> GetDataPointsList()
        {
            List<KeyValuePair<String, int>> dataPoints = new List<KeyValuePair<String, int>>();

            try
            {
                BugstarQueryBuilder qb = new BugstarQueryBuilder(this.Query);
                qb.RelativeQuery = String.Format("{0}", Commands.BUGSTAR_DATAPOINTS);


                //IBugstarConfig config = this.Connection.Config;
                //BugstarQuery query = new BugstarQuery(config, this.Query, Commands.BUGSTAR_DATAPOINTS);
                using (Stream queryResult = Connection.RunQuery(qb.ToQuery()))
                {
                    if (queryResult != null)
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.Load(queryResult);

                        // Read in the data points
                        XmlNodeList pointNodes = doc.SelectNodes("dataPoints/dataPoint");
                        foreach (XmlNode pointNode in pointNodes)
                        {
                            XmlNode keyNode = pointNode.SelectSingleNode("key");
                            string key = (keyNode == null ? "N/A" : keyNode.InnerText);

                            XmlNode countNode = pointNode.SelectSingleNode("count");
                            int count = (countNode == null ? 0 : Int32.Parse(countNode.InnerText));

                            dataPoints.Add(new KeyValuePair<String, int>(key, count));
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                BugstarConnection.Log.ToolException(ex, "Unhandled exception while getting the list of data points for graph '{0}' (id: {1}).", Name, Id);
            }

            return dataPoints;
        }
        #endregion // Private Methods
    } // Graph
} // RSG.Interop.Bugstar.Search
