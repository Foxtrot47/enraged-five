﻿namespace RSG.Interop.Bugstar.Search
{
    /// <summary>
    /// Defines the different sub types a bugstar report can be.
    /// </summary>
    public enum ReportSubType
    {
        /// <summary>
        /// 
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Represents a normal user generated bugstar report.
        /// </summary>
        Report,

        /// <summary>
        /// 
        /// </summary>
        Screen,

        /// <summary>
        /// 
        /// </summary>
        Notification,

        /// <summary>
        /// 
        /// </summary>
        FlashReport,
    } // RSG.Interop.Bugstar.Search {Enum}
} // RSG.Interop.Bugstar.Search {Namespace}
