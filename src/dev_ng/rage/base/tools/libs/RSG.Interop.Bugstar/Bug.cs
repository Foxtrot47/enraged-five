﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Media;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Math;
using RSG.Base.Net;
using RSG.Interop.Bugstar.Common;
using RSG.Interop.Bugstar.Game;
using RSG.Interop.Bugstar.Organisation;

namespace RSG.Interop.Bugstar
{
    /// <summary>
    /// Bugstar bug object
    /// </summary>
    public sealed class Bug : BugstarObject
    {
        #region Constants
        /// <summary>
        /// Character limit for summary field.
        /// </summary>
        private static readonly uint SUMMARY_CHARACTER_LIMIT = 255;

        /// <summary>
        /// Estimated time default in minutes ( -1.0f is no estimate ) 
        /// </summary>
        public const float ESTIMATED_TIME_DEFAULT = -1.0f;
        #endregion // Constants

        #region Properties
        /// <summary>
        /// The query to use to query bugstar about this object
        /// </summary>
        public override IRestQuery Query
        {
            get
            {
                BugstarQueryBuilder qb = new BugstarQueryBuilder(this.Project.Query);
                qb.RelativeQuery = String.Format("{0}/{1}", Commands.BUGSTAR_BUGS, Id.ToString());
                return (qb.ToQuery());
            }
        }

        /// <summary>
        /// The project this map is a part of
        /// </summary>
        public Project Project
        {
            get;
            private set;
        }

        /// <summary>
        /// Bug's summary
        /// </summary>
        public string Summary
        {
            get;
            private set;
        }

        /// <summary>
        /// Extended bug description
        /// </summary>
        public string Description
        {
            get;
            private set;
        }

        /// <summary>
        /// Id of the developer this bug is assigned to
        /// </summary>
        public Actor Developer
        {
            get
            {
                if (m_developer == null && m_developerId != 0)
                {
                    // Query the users first and if we don't find it query teams next
                    m_developer = User.GetUserById(Project, m_developerId);

                    if (m_developer == null)
                    {
                        m_developer = Team.GetTeamById(Project, m_developerId);
                    }
                }
                return m_developer;
            }
        }
        private Actor m_developer;
        private uint m_developerId;

        /// <summary>
        /// Id of the tester this bug is assigned to
        /// </summary>
        public Actor Tester
        {
            get
            {
                if (m_tester == null && m_testerId != 0)
                {
                    // Query the users first and if we don't find it query teams next
                    m_tester = User.GetUserById(Project, m_testerId);

                    if (m_tester == null)
                    {
                        m_tester = Team.GetTeamById(Project, m_testerId);
                    }
                }
                return m_tester;
            }
        }
        private Actor m_tester;
        private uint m_testerId;

        /// <summary>
        /// Bug reviewer.
        /// </summary>
        public Actor Reviewer
        {
            get
            {
                if (m_reviewer == null && m_reviewerId != 0)
                {
                    // Query the users first and if we don't find it query teams next
                    m_reviewer = User.GetUserById(Project, m_reviewerId);

                    if (null == m_reviewer)
                    {
                        m_reviewer = Team.GetTeamById(Project, m_reviewerId);
                    }
                }
                return (m_reviewer);
            }
        }
        private Actor m_reviewer;
        private uint m_reviewerId;

        /// <summary>
        /// Category of the bug
        /// </summary>
        public BugCategory Category
        {
            get;
            private set;
        }

        /// <summary>
        /// State of the bug (i.e. DEV, CLOSED_FIXED, etc...)
        /// </summary>
        public BugState State
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets a value indicating whether this bug is considered to be open.
        /// </summary>
        public bool IsOpen
        {
            get { return State == BugState.Dev || State == BugState.Planned || State == BugState.Test_More_Info; }
        }

        /// <summary>
        /// Priority of the bug
        /// </summary>
        public BugPriority Priority
        {
            get;
            private set;
        }

        /// <summary>
        /// Working flag.
        /// </summary>
        public bool IsWorking
        {
            get;
            private set;
        }

        /// <summary>
        /// On Hold flag.
        /// </summary>
        public bool IsOnHold
        {
            get;
            private set;
        }

        /// <summary>
        /// Location of the bug on the map
        /// </summary>
        public Vector3f Location
        {
            get;
            private set;
        }

        /// <summary>
        /// Mission this bug is a part of
        /// </summary>
        public Mission Mission
        {
            get
            {
                if (m_mission == null && m_missionId != 0)
                {
                    m_mission = Mission.GetMissionById(Project, m_missionId);
                }
                return m_mission;
            }
        }
        private Mission m_mission;
        public uint m_missionId;

        /// <summary>
        /// Component this bug is in
        /// </summary>
        public Component Component
        {
            get
            {
                if (m_component == null && m_componentId != 0)
                {
                    m_component = Component.GetComponentById(Project, m_componentId);
                }
                return m_component;
            }
        }
        private Component m_component;
        private uint m_componentId;

        /// <summary>
        /// When the bug is due by
        /// </summary>
        public DateTime DueDate
        {
            get;
            private set;
        }

        /// <summary>
        /// Colour of the bug
        /// </summary>
        public Color Colour
        {
            get;
            private set;
        }

        /// <summary>
        /// The grid that the bug was reported.
        /// </summary>
        public uint Grid
        {
            get;
            private set;
        }

        /// <summary>
        /// Users on the bug CC list.
        /// </summary>
        public IEnumerable<Actor> CCList
        {
            get
            {
                if (null == m_CCList && m_ccListIds.Count() > 0)
                {
                    m_CCList = new List<Actor>();
                    foreach (uint id in m_ccListIds)
                    {
                        // Query the users first and if we don't find it query teams next
                        Actor user = User.GetUserById(Project, id);

                        if (null == user)
                        {
                            user = Team.GetTeamById(Project, id);
                        }
                        m_CCList.Add(user);
                    }                    
                }
                return (m_CCList);
            }
        }
        private List<Actor> m_CCList;
        private IEnumerable<uint> m_ccListIds;

        /// <summary>
        /// Bug comments.
        /// </summary>
        public IEnumerable<BugComment> Comments
        {
            get;
            private set;
        }

        /// <summary>
        /// Bug attachments.
        /// </summary>
        public IEnumerable<Attachment> AttachmentList
        {
            get;
            private set;
        }

        /// <summary>
        /// Tags associated with this bug.
        /// </summary>
        public IEnumerable<String> Tags
        {
            get;
            private set;
        }
        

        /// <summary>
        /// Estimated time ( minutes ) 
        /// </summary>
        public float EstimatedTime
        {
            get;
            private set;
        }

        // TO ADD:
        // collected on
        // fixed on
        // verified on
        // attempts
        // verifications
        // time spent
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="project"></param>
        public Bug(Project project)
            : base(project.Connection)
        {
            this.Project = project;
        }

        /// <summary>
        /// Constructor; from XDocument.
        /// </summary>
        /// <param name="project"></param>
        /// <param name="xmlBugDocument"></param>
        public Bug(Project project, XDocument xmlBugDocument)
            : base(project.Connection)
        {
            this.Project = project;
            XElement xmlBugElem = xmlBugDocument.Element("bug");
            this.Deserialise(xmlBugElem);
        }

        /// <summary>
        /// Constructor; from XElement.
        /// </summary>
        /// <param name="project"></param>
        /// <param name="xmlBugElem"></param>
        public Bug(Project project, XElement xmlBugElem)
            : base(project.Connection)
        {
            this.Project = project;
            this.Deserialise(xmlBugElem);
        }

        /// <summary>
        /// Constructor; from Stream.
        /// </summary>
        /// <param name="stream"></param>
        public Bug(Project project, System.IO.Stream stream)
            : base(project.Connection)
        {
            this.Project = project;
            XDocument xmlBugDocument = XDocument.Load(stream);
            XElement xmlBugElem = xmlBugDocument.Element("bug");
            this.Deserialise(xmlBugElem);
        }

        /// <summary>
        /// Constructor (internal); for BugBuilder.
        /// </summary>
        /// <param name="project"></param>
        /// <param name="summary"></param>
        /// <param name="description"></param>
        /// <param name="developer"></param>
        /// <param name="tester"></param>
        /// <param name="reviewer"></param>
        /// <param name="category"></param>
        /// <param name="component"></param>
        /// <param name="mission"></param>
        /// <param name="priority"></param>
        /// <param name="tags"></param>
        /// <param name="cclist"></param>
        /// <param name="attachments"></param>
        /// <param name="comments"></param>
        /// <param name="estimatedTime">estimated time in minutes</param>
        internal Bug(Project project, BugCategory category, String summary, 
            String description, uint developer, uint tester, uint reviewer,
            uint component, uint mission, BugPriority priority = BugPriority.P3, 
            IEnumerable<String> tags = null, 
            IEnumerable<uint> cclist = null, 
            IEnumerable<uint> attachments = null,
            IEnumerable<String> comments = null,
            float estimatedTime = ESTIMATED_TIME_DEFAULT)
            : base(project.Connection)
        {
            Debug.Assert(summary.Length > 0,
                "Summary too short.");
            Debug.Assert(summary.Length <= SUMMARY_CHARACTER_LIMIT,
                "Summary too long.");
            if (summary.Length <= 0)
                throw (new ArgumentOutOfRangeException("summary", "Summary too short."));
            if (summary.Length > SUMMARY_CHARACTER_LIMIT)
                throw (new ArgumentOutOfRangeException("summary", "Summary too long."));
            
            this.Project = project;
            this.Category = category;
            this.State = BugState.Dev;
            this.Priority = priority;
            this.Summary = summary;
            this.Description = description;
            this.Location = new Vector3f(0.0f, 0.0f, 0.0f);

            this.m_developerId = developer;
            this.m_testerId = tester;
            this.m_reviewerId = reviewer;
            this.m_componentId = component;
            this.m_missionId = mission;
            
            List<String> tagList = new List<String>();
            if (null != tags)
                tagList.AddRange(tags);
            this.Tags = tagList;

            List<uint> ccList = new List<uint>();
            if (null != cclist)
                ccList.AddRange(cclist);
            this.m_ccListIds = ccList;

            List<Attachment> attachmentList = new List<Attachment>();
            if (null != attachments)
                attachmentList.AddRange(attachments.Select(id => 
                    new Attachment(project.Connection, id)));
            this.AttachmentList = attachmentList;

            List<BugComment> commentList = new List<BugComment>();
            if (null != comments)
                commentList.AddRange(comments.Select(comment =>
                    new BugComment(project, comment)));
            this.Comments = commentList;

            this.EstimatedTime = estimatedTime;
        }       
        #endregion // Constructor(s)

        #region Static Controller Methods
        /// <summary>
        /// Add an attachment to a bug.
        /// </summary>
        /// <param name="attachment"></param>
        /// <returns></returns>
        public static bool Attach(Bug bug, Attachment attachment)
        {
            BugstarQueryBuilder qb = new BugstarQueryBuilder(bug.Project.Query);
            qb.RelativeQuery = Commands.BUGSTAR_REGISTER_ATTACHMENT;
            qb.Parameters.Add("bugId", bug.Id.ToString());
            qb.Parameters.Add("attachmentId", attachment.Id.ToString());

            try
            {
                bug.Connection.RunQuery(qb.ToQuery());
            }
            catch
            {
                return (false);
            }

            return (true);
        }

        /// <summary>
        /// Gets a bug for a particular project based on the bug's id
        /// </summary>
        /// <param name="project"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static Bug GetBugById(Project project, uint id)
        {
            BugstarQueryBuilder qb = new BugstarQueryBuilder(project.Query);
            qb.RelativeQuery = String.Format("{0}/{1}", Commands.BUGSTAR_BUGS, id);
            BugstarQuery query = qb.ToQuery();

            try
            {
                XDocument xmlBugDocument = project.Connection.RunQueryWithXmlResult(query);
                Bug bug = new Bug(project, xmlBugDocument);
                return (bug);
            }
            catch (Exception ex)
            {
                BugstarConnection.Log.ToolException(ex, "Error creating bug from ID {0}.", id);
                return (null);
            }
        }
        #endregion // Static Controller Methods

        #region Controller Methods
        /// <summary>
        /// Refresh bug's content.
        /// </summary>
        public void Refresh()
        {
            XDocument xmlBugDocument = this.Connection.RunQueryWithXmlResult(this.Query);
            XElement xmlBugElem = xmlBugDocument.Element("bug");
            this.Deserialise(xmlBugElem);
        }
        #endregion // Controller Methods

        #region XML Serialisation
        /// <summary>
        /// Generates an object from its XML representation.
        /// </summary>
        /// <param name="node"></param>
        //TODO: Move this to XDocument.
        public override void Deserialise(XmlNode node)
        {
            base.Deserialise(node);

            // The xml may only contain a fraction of the information present in a bug, so extract whatever info we can
            XmlNode summaryNode = node.SelectSingleNode("summary");
            if (summaryNode != null)
            {
                Summary = summaryNode.InnerText;
            }

            XmlNode descriptionNode = node.SelectSingleNode("description");
            if (descriptionNode != null)
            {
                Description = descriptionNode.InnerText;
            }

            XmlNode developerIdNode = node.SelectSingleNode("developer");
            if (developerIdNode != null)
            {
                m_developerId = UInt32.Parse(developerIdNode.InnerText);
            }

            XmlNode testerIdNode = node.SelectSingleNode("tester");
            if (testerIdNode != null)
            {
                m_testerId = UInt32.Parse(testerIdNode.InnerText);
            }

            XmlNode categoryNode = node.SelectSingleNode("category");
            if (categoryNode != null)
            {
                String categoryString = categoryNode.InnerText;
                this.Category = BugCategoryUtils.FromBugstarString(categoryString);
            }

            XmlNode stateNode = node.SelectSingleNode("state");
            if (stateNode != null)
            {
                String stateString = stateNode.InnerText;
                this.State = BugStateUtils.FromBugstarString(stateString);
            }

            XmlNode priorityNode = node.SelectSingleNode("priority");
            if (priorityNode != null)
            {
                String priorityString = priorityNode.InnerText;
                this.Priority = BugPriorityUtils.FromBugstarString(priorityString);
            }

            XmlNode isWorkingNode = node.SelectSingleNode("isWorking");
            if (null != isWorkingNode)
            {
                bool isWorking = false;
                if (bool.TryParse(isWorkingNode.InnerText, out isWorking))
                    this.IsWorking = isWorking;
            }

            XmlNode onHoldNode = node.SelectSingleNode("isOnHold");
            if (null != onHoldNode)
            {
                bool isOnHold = false;
                if (bool.TryParse(onHoldNode.InnerText, out isOnHold))
                    this.IsOnHold = isOnHold;
            }

            // Bug location
            float x = 0.0f;
            float y = 0.0f;
            float z = 0.0f;

            XmlNode xNode = node.SelectSingleNode("x");
            if (xNode != null)
            {
                x = Single.Parse(xNode.InnerText);
            }

            XmlNode yNode = node.SelectSingleNode("y");
            if (yNode != null)
            {
                y = Single.Parse(yNode.InnerText);
            }

            XmlNode zNode = node.SelectSingleNode("z");
            if (zNode != null)
            {
                z = Single.Parse(zNode.InnerText);
            }
            Location = new Vector3f(x, y, z);

            XmlNode missionIdNode = node.SelectSingleNode("mission");
            if (missionIdNode != null)
            {
                m_missionId = UInt32.Parse(missionIdNode.InnerText);
            }

            XmlNode componentIdNode = node.SelectSingleNode("component");
            if (componentIdNode != null)
            {
                m_componentId = UInt32.Parse(componentIdNode.InnerText);
            }

            XmlNode dueDateNode = node.SelectSingleNode("dueDate");
            if (dueDateNode != null)
            {
                DueDate = DateTime.Parse(dueDateNode.InnerText);
            }

            //DHM: dont see this in the bug object?  In searches?
            XmlNode colourNode = node.SelectSingleNode("colour");
            if (colourNode != null)
            {
                int value = Int32.Parse(colourNode.InnerText);

                Color col = new Color();
                col.A = (byte)((value >> 24) & 0xFF);
                col.R = (byte)((value >> 16) & 0xFF);
                col.G = (byte)((value >> 8) & 0xFF);
                col.B = (byte)(value & 0xFF);
                Colour = col;
            }

            XmlNode gridNode = node.SelectSingleNode("grid");
            if (gridNode != null)
            {
                uint value = UInt32.Parse(gridNode.InnerText);
                Grid = value;
            }

            List<String> tagList = new List<String>();
            foreach (XmlNode tagNode in node.SelectNodes("tagList/tag"))
            {
                tagList.Add(tagNode.InnerText);
            }

            EstimatedTime = ESTIMATED_TIME_DEFAULT;
            XmlNode estimatedTimeNode = node.SelectSingleNode("estimatedTime");
            if (estimatedTimeNode != null)
            {
                EstimatedTime = float.Parse(estimatedTimeNode.InnerText);
            }
        }
        
        /// <summary>
        /// Serialise bug to its XML representation.
        /// </summary>
        /// <returns></returns>
        public XDocument Serialise()
        {
            XDocument xmlBugDocument = new XDocument(
                new XDeclaration("1.0", "utf-8", "yes"),
                new XElement("bug",
                    new XElement("summary", this.Summary),
                    new XElement("description", this.Description),
                    new XElement("developer", this.m_developerId),
                    new XElement("tester", this.m_testerId),
                    new XElement("reviewer", this.m_reviewerId),
                    new XElement("mission", this.m_missionId),
                    new XElement("category", this.Category.ToBugstarString()),
                    new XElement("state", this.State.ToBugstarString()),
                    new XElement("component", this.m_componentId),
                    new XElement("priority", this.Priority.ToBugstarString()),
                    new XElement("isWorking", this.IsWorking.ToString().ToLower()),
                    new XElement("isOnHold", this.IsOnHold.ToString().ToLower()),
                    new XElement("x", this.Location.X),
                    new XElement("y", this.Location.Y),
                    new XElement("z", this.Location.Z),
                    new XElement("comments",
                        this.Comments.Select(comment => comment.Serialise())
                    ), // comments
                    new XElement("ccList",
                        this.m_ccListIds.Select(id => new XElement("cc", id))
                    ), // ccList
                    new XElement("tagList",
                        this.Tags.Select(tag => new XElement("tag", tag))
                    ), // tagList
                    new XElement("attachmentList",
                        this.AttachmentList.Select(id => new XElement("attachment", id))
                    ), // attachmentList
                    new XElement("estimatedTime", this.EstimatedTime)
                ) // bug
            ); // document

            return (xmlBugDocument);
        }
        #endregion // XML Serialisation

        #region Protected Methods
        /// <summary>
        /// Deserialise Bug object from XElement.
        /// </summary>
        /// <param name="xmlBugElem"></param>
        protected override void Deserialise(XElement xmlBugElem)
        {
            try
            {
                base.Deserialise(xmlBugElem);

                XElement xmlSummaryElem = xmlBugElem.Element("summary");
                if (null != xmlSummaryElem)
                    this.Summary = xmlSummaryElem.Value;
                XElement xmlDescriptionElem = xmlBugElem.Element("description");
                if (null != xmlDescriptionElem)
                    this.Description = xmlDescriptionElem.Value;
                XElement xmlDeveloperElem = xmlBugElem.Element("developer");
                if (null != xmlDeveloperElem)
                    uint.TryParse(xmlDeveloperElem.Value, out this.m_developerId);
                XElement xmlTesterElem = xmlBugElem.Element("tester");
                if (null != xmlTesterElem)
                    uint.TryParse(xmlTesterElem.Value, out this.m_testerId);
                XElement xmlMissionElem = xmlBugElem.Element("mission");
                if (null != xmlMissionElem)
                    uint.TryParse(xmlMissionElem.Value, out this.m_missionId);
                XElement xmlCategoryElem = xmlBugElem.Element("category");
                if (null != xmlCategoryElem)
                    this.Category = BugCategoryUtils.FromBugstarString(xmlCategoryElem.Value);
                XElement xmlStateElem = xmlBugElem.Element("state");
                if (null != xmlStateElem)
                    this.State = BugStateUtils.FromBugstarString(xmlStateElem.Value);
                XElement xmlPriorityElem = xmlBugElem.Element("priority");
                if (null != xmlPriorityElem)
                    this.Priority = BugPriorityUtils.FromBugstarString(xmlPriorityElem.Value);
                XElement xmlIsWorkingElem = xmlBugElem.Element("isWorking");
                if (null != xmlIsWorkingElem)
                {
                    bool isWorking = false;
                    if (bool.TryParse(xmlIsWorkingElem.Value, out isWorking))
                        this.IsWorking = isWorking;
                }
                XElement xmlIsOnHoldElem = xmlBugElem.Element("isOnHold");
                if (null != xmlIsOnHoldElem)
                {
                    bool isOnHold = false;
                    if (bool.TryParse(xmlIsOnHoldElem.Value, out isOnHold))
                        this.IsOnHold = isOnHold;
                }

                // Location
                XElement xmlXElem = xmlBugElem.Element("x");
                XElement xmlYElem = xmlBugElem.Element("y");
                XElement xmlZElem = xmlBugElem.Element("z");
                float x = 0.0f, y = 0.0f, z = 0.0f;
                if (null != xmlXElem)
                    float.TryParse(xmlXElem.Value, out x);
                if (null != xmlYElem)
                    float.TryParse(xmlYElem.Value, out y);
                if (null != xmlZElem)
                    float.TryParse(xmlZElem.Value, out z);
                this.Location = new Vector3f(x, y, z);

                // Comments
                IEnumerable<XElement> commentList = xmlBugElem.XPathSelectElements("comments/comment");
                List<BugComment> comments = new List<BugComment>();
                foreach (XElement comment in commentList)
                    comments.Add(new BugComment(Project, comment));
                this.Comments = comments;

                // tagList
                IEnumerable<XElement> tagList = xmlBugElem.XPathSelectElements("tagList/tag");
                List<String> tags = new List<String>();
                foreach (XElement xmlTagElem in tagList)
                    tags.Add(xmlTagElem.Value);
                this.Tags = tags;

                // CC list
                IEnumerable<XElement> ccList = xmlBugElem.XPathSelectElements("ccList/cc");
                List<uint> cclist = new List<uint>();
                foreach (XElement xmlCCElem in ccList)
                {
                    uint id = 0;
                    if (uint.TryParse(xmlCCElem.Value, out id))
                        cclist.Add(id);
                }
                this.m_ccListIds = cclist;

                // Attachments
                IEnumerable<XElement> attachmentList =
                    xmlBugElem.XPathSelectElements("attachmentList/attachment");
                List<Attachment> attachments = new List<Attachment>();
                foreach (XElement xmlAttachmentElem in attachmentList)
                {
                    uint id = 0;
                    if (uint.TryParse(xmlAttachmentElem.Value, out id))
                        attachments.Add(new Attachment(this.Connection, id));
                }
                this.AttachmentList = attachments;

                this.EstimatedTime = ESTIMATED_TIME_DEFAULT;
                XElement estimatedTimeElem = xmlBugElem.Element("estimatedTime");
                if (null != estimatedTimeElem)
                {
                    float estimatedTime;
                    if (float.TryParse(estimatedTimeElem.Value, out estimatedTime))
                    {
                        EstimatedTime = estimatedTime;
                    }
                }
            }
            catch (Exception ex)
            {
                throw (new ArgumentException("Failed to load Bug information.", ex));
            }
        }
        #endregion // Protected Methods
    } // Bug

} // RSG.Interop.Bugstar
