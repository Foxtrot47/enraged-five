﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Interop.Bugstar.Attributes;

namespace RSG.Interop.Bugstar
{
    /// <summary>
    /// Bug fields that we can query data for via the REST interface.
    /// </summary>
    public enum BugField
    {
        /// <summary>
        /// Id field.
        /// </summary>
        [BugstarData("Id")]
        Id,

        /// <summary>
        /// Summary field.
        /// </summary>
        [BugstarData("Summary")]
        Summary,

        /// <summary>
        /// Description field.
        /// </summary>
        [BugstarData("Description")]
        Description,

        /// <summary>
        /// Map grid.
        /// </summary>
        [BugstarData("Grid")]
        Grid,

        /// <summary>
        /// Coordinates on the map.
        /// </summary>
        [BugstarData("X,Y,Z")]
        Location,

        /// <summary>
        /// Bug status.
        /// </summary>
        [BugstarData("State")]
        State
    }
}
