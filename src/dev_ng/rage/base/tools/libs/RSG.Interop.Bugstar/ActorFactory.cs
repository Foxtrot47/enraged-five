﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;
using RSG.Interop.Bugstar.Common;
using RSG.Interop.Bugstar.Organisation;

namespace RSG.Interop.Bugstar
{

    /// <summary>
    /// Cached factory for Bugstar Users and Teams (collectively known as Actors).
    /// </summary>
    public static class ActorFactory
    {
        #region Member Data
        /// <summary>
        /// Actor cache.
        /// </summary>
        private static IDictionary<uint, Actor> Actors;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Static constructor; initialises static cache.
        /// </summary>
        static ActorFactory()
        {
            ActorFactory.Actors = new Dictionary<uint, Actor>();
        }
        #endregion // Constructor(s)

        #region Static Controller Methods
        /// <summary>
        /// Create user or team from Id (null if Id is not valid actor).
        /// </summary>
        /// <param name="project"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static Actor CreateActorById(Project project, uint id)
        {
            Actor actor = CreateUserById(project, id);
            if (0 != id)
                actor = CreateTeamById(project, id);

            return (actor);
        }

        /// <summary>
        /// Create user from Id (null if Id is not valid user).
        /// </summary>
        /// <param name="project"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static Actor CreateUserById(Project project, uint id)
        {
            BugstarQueryBuilder qb = new BugstarQueryBuilder(project.Query);
            qb.RelativeQuery = String.Format("{0}/{1}", Commands.BUGSTAR_USERS, id);
            BugstarQuery query = qb.ToQuery();

            try
            {
                XDocument xmlUserDocument = project.Connection.RunQueryWithXmlResult(query);
                User user = new User(project, xmlUserDocument);

                return (user);
            }
            catch (Exception)
            {
                // *sigh* Internal Server Error on invalid ID.
                return (null);
            }
        }

        /// <summary>
        /// Create team from Id (null if Id is not valid team).
        /// </summary>
        /// <param name="project"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static Actor CreateTeamById(Project project, uint id)
        {
            BugstarQueryBuilder qb = new BugstarQueryBuilder(project.Query);
            qb.RelativeQuery = String.Format("{0}/{1}", Commands.BUGSTAR_TEAMS, id);
            BugstarQuery query = qb.ToQuery();

            try
            {
                XDocument xmlTeamDocument = project.Connection.RunQueryWithXmlResult(query);
                Team team = new Team(project, xmlTeamDocument);

                return (team);
            }
            catch (Exception)
            {
                // *sigh* Internal Server Error on invalid ID.
                return (null);
            }
        }

        /// <summary>
        /// Clear our actor cache.
        /// </summary>
        public static void Clear()
        {
            ActorFactory.Actors.Clear();
        }
        #endregion // Static Controller Methods
    }

} // RSG.Interop.Bugstar namespace
