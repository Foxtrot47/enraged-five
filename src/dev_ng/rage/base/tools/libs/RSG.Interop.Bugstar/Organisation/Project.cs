﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using RSG.Base.Logging;
using RSG.Base.Math;
using RSG.Base.Net;
using RSG.Interop.Bugstar.Common;
using RSG.Interop.Bugstar.Game;
using System.Xml.Serialization;

namespace RSG.Interop.Bugstar.Organisation
{

    /// <summary>
    /// Bugstar Project.
    /// </summary>
    public class Project : BugstarObject
    {
        #region Constants
        private static readonly String LOG_CTX = "Bugstar:Project";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// The query to use to query bugstar about this object
        /// </summary>
        public override IRestQuery Query
        {
            get
            {
                BugstarQueryBuilder qb = new BugstarQueryBuilder(this.Connection.RESTService);
                qb.RelativeQuery = String.Format("{0}/{1}", Commands.BUGSTAR_PROJECTS, Id.ToString());
                return (qb.ToQuery());
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Name
        {
            get;
            protected set;
        }

        /// <summary>
        /// Indirectly queries bugstar to get a list of missions for this project
        /// </summary>
        public Mission[] Missions
        {
            get
            {
                return GetMissionsList().ToArray();
            }
        }

        /// <summary>
        /// Queries bugstar to get the current user
        /// </summary>
        public User CurrentUser
        {
            get
            {
                return GetCurrentUser();
            }
        }

        /// <summary>
        /// Queries bugstar to get a list of users for this project
        /// </summary>
        public User[] Users
        {
            get
            {
                return GetUsersList().ToArray();
            }
        }

        /// <summary>
        /// Queries bugstar to get a list of maps for this project
        /// </summary>
        public Map[] Maps
        {
            get
            {
                return GetMapList().ToArray();
            }
        }

        /// <summary>
        /// Queries bugstar to get the list of components for this project
        /// </summary>
        public Component[] Components
        {
            get
            {
                return GetComponentsList().ToArray();
            }
        }

        /// <summary>
        /// Queries bugstar to get the list of categories for this project
        /// </summary>
        [Obsolete("We use an enum internally now.")]
        public Category[] Categories
        {
            get
            {
                return GetCategoryList().ToArray();
            }
        }

        /// <summary>
        /// Queries bugstar to get the list of priorities for this project
        /// </summary>
        [Obsolete("We use an enum internally now.")]
        public Priority[] Priorities
        {
            get
            {
                return GetPriorityList().ToArray();
            }
        }
        #endregion

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="projectNode"></param>
        public Project(BugstarConnection connection)
            : base(connection)
        {
            this.Connection = connection;
        }
        #endregion

        #region XML Serialisation
        /// <summary>
        /// Generates an object from its XML representation.
        /// </summary>
        /// <param name="node"></param>
        public override void Deserialise(XmlNode node)
        {
            // NOTE: We can't use the base object's deserialise function because the project xml node has all of it's details in attributes
            // as opposed to a child element
            XmlAttribute idAttribute = node.Attributes["id"];
            if (idAttribute == null)
            {
                throw new ArgumentException("ID attribute missing for project.");
            }
            Id = UInt32.Parse(idAttribute.Value);


            XmlAttribute nameAttribute = node.Attributes["name"];
            if (nameAttribute == null)
            {
                throw new ArgumentException(String.Format("Name attribute missing for project (id: {0}).", Id));
            }
            Name = nameAttribute.Value;

        }
        #endregion // XML Serialisation

        #region Static Controller Methods
        /// <summary>
        /// Returns a project based on it's id
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static Project GetProjectById(BugstarConnection connection, uint id)
        {
            Project project = null;

            try
            {
                BugstarQueryBuilder qb = new BugstarQueryBuilder(connection.RESTService);
                qb.RelativeQuery = String.Format("{0}/{1}", Commands.BUGSTAR_PROJECTS, id.ToString());

                BugstarQuery query = qb.ToQuery();
                Stream queryResult = connection.RunQuery(query);
                if (queryResult != null)
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(queryResult);

                    XmlNode projectNode = doc.SelectSingleNode("/project");
                    if (projectNode != null)
                    {
                        project = new Project(connection);
                        project.Deserialise(projectNode);
                    }
                }
            }
            catch (System.Exception ex)
            {
                BugstarConnection.Log.ToolExceptionCtx(LOG_CTX, ex, 
                    "Unhandled exception while getting information for project '{0}'.", id);
            }

            return project;
        }

        /// <summary>
        /// Return array of all projects in bugstar.
        /// </summary>
        /// <param name="p4"></param>
        /// <returns></returns>
        public static Project[] GetProjects(BugstarConnection connection)
        {
            List<Project> projects = new List<Project>();
            
            try
            {
                BugstarQueryBuilder qb = new BugstarQueryBuilder(connection.RESTService);
                qb.RelativeQuery = Commands.BUGSTAR_PROJECTS;

                BugstarQuery query = qb.ToQuery();
                Stream result = connection.RunQuery(query);

                XmlDocument doc = new XmlDocument();
                doc.Load(result);

                XmlNodeList projectNodes = doc.SelectNodes("/projects/project");
                foreach (XmlNode projectNode in projectNodes)
                {
                    Project project = new Project(connection);
                    project.Deserialise(projectNode);
                    projects.Add(project);
                }
            }
            catch (System.Exception ex)
            {
                BugstarConnection.Log.ToolExceptionCtx(LOG_CTX, ex, 
                    "Unhandled exception while getting the project list.");
            }
            
            return (projects.ToArray());
        }
        #endregion // Static Controller Methods

        #region Public Methods
        /// <summary>
        /// Queries bugstar for a list of bugs that are within a specified distance of a location
        /// </summary>
        /// <param name="location"></param>
        /// <param name="radius"></param>
        /// <returns></returns>
        public List<Bug> GetOpenBugsInArea(Vector2f location, float radius)
        {
            return GetOpenBugsInArea(location, radius, "Id");
        }

        /// <summary>
        /// Queries bugstar for a list of bugs that are within a specified distance of a location
        /// </summary>
        /// <param name="location"></param>
        /// <param name="radius"></param>
        /// <param name="fields"></param>
        /// <returns></returns>
        public List<Bug> GetOpenBugsInArea(Vector2f location, float radius, string fields)
        {
            List<Bug> bugs = new List<Bug>();

            // Create a query for this request
            BugstarQueryBuilder qb = new BugstarQueryBuilder(this.Query);
            qb.RelativeQuery = String.Format("{0}/{1}/{2}", Commands.BUGSTAR_BUGS, 
                Commands.BUGSTAR_SEARCHES, Commands.BUGSTAR_SEARCH_MODE_OPENBYLOCATION);
            qb.Parameters.Add("radius", radius.ToString());
            qb.Parameters.Add("x", location.X.ToString());
            qb.Parameters.Add("y", location.Y.ToString());
            qb.Parameters.Add("z", "100");
            qb.Parameters.Add("fields", fields);

            BugstarQuery query = qb.ToQuery();

            // The resulting stream should contain xml, so convert it and extract the information we are after
            Stream queryResult = Connection.RunQuery(query);

			//TODO: DHM FIX ME: should use RunQueryWithXDocumentResult!
            if (queryResult != null)
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(queryResult);

                XmlNodeList bugNodes = doc.SelectNodes("/bugs/bug");
                foreach (XmlNode bugNode in bugNodes)
                {
                    Bug bug = new Bug(this);
                    bug.Deserialise(bugNode);
                    bugs.Add(bug);
                }
            }

            return bugs;
        }

        /// <summary>
        /// Queries Bugstar for a list of the teams associated with this project.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Team> GetTeamsList()
        {
            List<Team> teams = new List<Team>();

            try
            {
                BugstarQueryBuilder qb = new BugstarQueryBuilder(this.Query);
                qb.RelativeQuery = Commands.BUGSTAR_TEAMS;
                BugstarQuery query = qb.ToQuery();

				//TODO: DHM FIX ME: should use RunQueryWithXDocumentResult!
                using (Stream queryResult = Connection.RunQuery(query))
                {
                    if (queryResult != null)
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.Load(queryResult);

                        XmlNodeList userNodes = doc.SelectNodes("/teams/team");
                        foreach (XmlNode userNode in userNodes)
                        {
                            Team team = new Team(this);
                            team.Deserialise(userNode);
                            teams.Add(team);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                BugstarConnection.Log.ToolExceptionCtx(LOG_CTX, ex, 
                    "Unhandled exception while getting the list of teams for project '{0}' (id: {1}).", 
                    this.Name, this.Id);
            }

            return (teams);
        }

        /// <summary>
        /// Queries Bugstar for a list of the users associated with this project.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<User> GetUsersList()
        {
            List<User> users = new List<User>();

            try
            {
                BugstarQueryBuilder qb = new BugstarQueryBuilder(this.Query);
                qb.RelativeQuery = Commands.BUGSTAR_USERS;
                BugstarQuery query = qb.ToQuery();

				//TODO: DHM FIX ME: should use RunQueryWithXDocumentResult!
                using (Stream queryResult = Connection.RunQuery(query))
                {
                    if (queryResult != null)
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.Load(queryResult);
                        XmlNodeList userNodes = doc.SelectNodes("/users/user");
                        foreach (XmlNode userNode in userNodes)
                        {
                            User user = new User(this);
                            user.Deserialise(userNode);
                            users.Add(user);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                BugstarConnection.Log.ToolExceptionCtx(LOG_CTX, ex, 
                    "Unhandled exception while getting the list of users for project '{0}' (id: {1}).", 
                    this.Name, this.Id);
            }

            return (users);
        }
        #endregion // Methods

        #region Private Methods
        /// <summary>
        /// Queries bugstar for a list of maps associated with this project
        /// </summary>
        /// <returns></returns>
        private List<Map> GetMapList()
        {
            List<Map> maps = new List<Map>();
            
            try
            {
                BugstarQueryBuilder qb = new BugstarQueryBuilder(this.Query);
                qb.RelativeQuery = Commands.BUGSTAR_MAPS;
                BugstarQuery query = qb.ToQuery();

				//TODO: DHM FIX ME: should use RunQueryWithXDocumentResult!
                using (Stream queryResult = Connection.RunQuery(query))
                {
                    if (queryResult != null)
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.Load(queryResult);

                        XmlNodeList mapNodes = doc.SelectNodes("/maps/map");
                        foreach (XmlNode mapNode in mapNodes)
                        {
                            Map map = new Map(this);
                            map.Deserialise(mapNode);
                            maps.Add(map);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                BugstarConnection.Log.ToolExceptionCtx(LOG_CTX, ex, 
                    "Unhandled exception while getting the list of maps for project '{0}' (id: {1}).", 
                    this.Name, this.Id);
            }

            return maps;
        }

        /// <summary>
        /// Queries bugstar for information about the current user
        /// </summary>
        /// <returns></returns>
        private User GetCurrentUser()
        {
            User user = null;

            try
            {
                BugstarQueryBuilder qb = new BugstarQueryBuilder(this.Query);
                qb.RelativeQuery = Commands.BUGSTAR_CURRENTUSER;
                BugstarQuery query = qb.ToQuery();

				//TODO: DHM FIX ME: should use RunQueryWithXDocumentResult!
                using (Stream queryResult = Connection.RunQuery(query))
                {
                    if (queryResult != null)
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.Load(queryResult);

                        XmlNode currentUserIdNode = doc.SelectSingleNode("/user/id");
                        if (currentUserIdNode != null)
                        {
                            return Users.FirstOrDefault(item => item.Id == Int32.Parse(currentUserIdNode.InnerText));
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                BugstarConnection.Log.ToolExceptionCtx(LOG_CTX, ex, 
                    "Unhandled exception while getting the list of users for project '{0}' (id: {1}).", 
                    this.Name, this.Id);
            }

            return user;
        }

        /// <summary>
        /// Queries bugstar for a list of the missions associated with this project
        /// </summary>
        /// <returns></returns>
        private List<Mission> GetMissionsList()
        {
            List<Mission> missions = new List<Mission>();

            try
            {
                BugstarQueryBuilder qb = new BugstarQueryBuilder(this.Query);
                qb.RelativeQuery = Commands.BUGSTAR_MISSIONS;
                BugstarQuery query = qb.ToQuery();

				//TODO: DHM FIX ME: should use RunQueryWithXDocumentResult!
                using (Stream queryResult = Connection.RunQuery(query))
                {
                    if (queryResult != null)
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.Load(queryResult);

                        XmlNodeList missionNodes = doc.SelectNodes("/missions/mission");
                        foreach (XmlNode missionNode in missionNodes)
                        {
                            Mission mission = new Mission(this);
                            mission.Deserialise(missionNode);
                            missions.Add(mission);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                BugstarConnection.Log.ToolExceptionCtx(LOG_CTX, ex, 
                    "Unhandled exception while getting the list of missions for project '{0}' (id: {1}).", 
                    this.Name, this.Id);
            }

            return missions;
        }

        /// <summary>
        /// Queries Bugstar for a list of components associated with this project
        /// </summary>
        /// <returns></returns>
        private List<Component> GetComponentsList()
        {
            List<Component> components = new List<Component>();

            try
            {
                BugstarQueryBuilder qb = new BugstarQueryBuilder(this.Query);
                qb.RelativeQuery = Commands.BUGSTAR_COMPONENTS;
                BugstarQuery query = qb.ToQuery();

				//TODO: DHM FIX ME: should use RunQueryWithXDocumentResult!
                using (Stream queryResult = Connection.RunQuery(query))
                {
                    if (queryResult != null)
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.Load(queryResult);

                        XmlNodeList componentNodes = doc.SelectNodes("/components/component");
                        foreach (XmlNode componentNode in componentNodes)
                        {
                            Component comp = new Component(this);
                            comp.Deserialise(componentNode);
                            components.Add(comp);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                BugstarConnection.Log.ToolExceptionCtx(LOG_CTX, ex, 
                    "Unhandled exception while getting the list of missions for project '{0}' (id: {1}).", 
                    this.Name, this.Id);
            }

            return components;
        }

        /// <summary>
        /// Queries Bugstar for a list of bug categories.
        /// </summary>
        /// <returns></returns>
        [Obsolete("We use an enum internally now.")]
        private List<Category> GetCategoryList()
        {
            List<Category> categories = new List<Category>();

            try
            {
                BugstarQueryBuilder qb = new BugstarQueryBuilder(this.Query);
                qb.RelativeQuery = Commands.BUGSTAR_CATEGORIES;
                BugstarQuery query = qb.ToQuery();

                using (Stream queryResult = Connection.RunQuery(query))
                {
                    if (queryResult != null)
                    {
                        XmlSerializer deserializer = new XmlSerializer(typeof(EnumObjectList<Category>));
                        EnumObjectList<Category> categoryList = (EnumObjectList<Category>)deserializer.Deserialize(queryResult);
                        categories.AddRange(categoryList.Items);
                    }
                }
            }
            catch (System.Exception ex)
            {
                BugstarConnection.Log.ToolExceptionCtx(LOG_CTX, ex, 
                    "Unhandled exception while getting the category list for project '{0}' (id: {1}).", 
                    this.Name, this.Id);
            }

            return categories;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Obsolete("We use an enum internally now.")]
        private List<Priority> GetPriorityList()
        {
            List<Priority> priorities = new List<Priority>();

            try
            {
                BugstarQueryBuilder qb = new BugstarQueryBuilder(this.Query);
                qb.RelativeQuery = Commands.BUGSTAR_PRIORITIES;
                BugstarQuery query = qb.ToQuery();

                using (Stream queryResult = Connection.RunQuery(query))
                {
                    if (queryResult != null)
                    {
                        XmlSerializer deserializer = new XmlSerializer(typeof(EnumObjectList<Priority>));
                        EnumObjectList<Priority> priorityList = (EnumObjectList<Priority>)deserializer.Deserialize(queryResult);
                        priorities.AddRange(priorityList.Items);
                    }
                }
            }
            catch (System.Exception ex)
            {
                BugstarConnection.Log.ToolExceptionCtx(LOG_CTX, ex, 
                    "Unhandled exception while getting the priority list for project '{0}' (id: {1}).", 
                    this.Name, this.Id);
            }

            return priorities;
        }
        #endregion // Private Methods
    } // Project

} // RSG.Interop.Bugstar.Organisation namespace
