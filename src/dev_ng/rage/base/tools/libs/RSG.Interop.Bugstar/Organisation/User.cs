﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Media.Imaging;
using System.Xml;
using System.Xml.Linq;
using RSG.Base.Logging;
using RSG.Base.Net;
using RSG.Interop.Bugstar.Common;
using RSG.Interop.Bugstar.Search;

namespace RSG.Interop.Bugstar.Organisation
{
    /// <summary>
    /// Bugstar user; permissions, reports and graphs.
    /// </summary>
    public class User : Actor
    {
        #region Constants
        private static readonly String LOG_CTX = "Bugstar:User";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// The query to use to query bugstar about this object
        /// </summary>
        public override IRestQuery Query
        {
            get
            {
                BugstarQueryBuilder qb = new BugstarQueryBuilder(this.Project.Query);
                qb.RelativeQuery = String.Format("{0}/{1}", Commands.BUGSTAR_USERS, Id.ToString());

                return (qb.ToQuery());
            }
        }

        /// <summary>
        /// User name (e.g. michael.taschler)
        /// </summary>
        public string UserName
        {
            get;
            protected set;
        }

        /// <summary>
        /// Users email address (e.g. michael.taschler@rockstarnorth.com)
        /// </summary>
        public string Email
        {
            get;
            protected set;
        }

        /// <summary>
        /// User's job title (e.g. Awesome Tools Programmer).
        /// </summary>
        public String JobTitle
        {
            get;
            protected set;
        }

        /// <summary>
        /// List of reports for this user
        /// </summary>
        public Report[] Reports
        {
            get
            {
                return GetReportsList().ToArray();
            }
        }

        /// <summary>
        /// List of graphs for this user
        /// </summary>
        public Graph[] Graphs
        {
            get
            {
                return GetGraphsList().ToArray();
            }
        }

        /// <summary>
        /// List of groups this user has available to them.
        /// </summary>
        public Group[] Groups
        {
            get
            {
                return GetGroupsList().ToArray();
            }
        }

        /// <summary>
        /// Image to use for the user
        /// </summary>
        public BitmapImage Image
        {
            get
            {
                if (m_image == null)
                {
                    try
                    {
                        // Run a query and get the imageid to be added
                        BugstarQueryBuilder qb = new BugstarQueryBuilder(Project.Query);
                        qb.RelativeQuery = String.Format("{0}/{1}/Image", Commands.BUGSTAR_USERS, Id.ToString());
                        BugstarQuery query = qb.ToQuery();

                        using (Stream s = Connection.RunQuery(query))
                        {
                            if (s != null)
                            {
                                StreamReader reader = new StreamReader(s);
                                string text = reader.ReadToEnd();
                                m_imageAttachmentId = uint.Parse(text);
                            }
                        }                
                    }
                    catch (RestException e)
                    {
                        BugstarConnection.Log.WarningCtx(LOG_CTX, 
                            "User has no image : {0}", e);
                    }
                }

                try
                {
                    if (m_imageAttachmentId != 0)
                    {
                        m_image = GetImage();
                    }
                }
                catch (Exception e)
                {
                   BugstarConnection.Log.ErrorCtx(LOG_CTX, 
                       " Get user image exception : {0}", e);
                }
               
               return m_image;
            }
        }
        private BitmapImage m_image;
        private uint m_imageAttachmentId;    
  
        /// <summary>
        /// Permissions table; see string constants for keys.
        /// </summary>
        public IDictionary<PermissionType, bool> Permissions
        {
            get { return (GetPermissionsTable()); }
        }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userNode"></param>
        public User(Project project)
            : base(project)
        {
        }

        /// <summary>
        /// Constructor; from XDocument.
        /// </summary>
        /// <param name="project"></param>
        /// <param name="xmlUserDocument"></param>
        public User(Project project, XDocument xmlUserDocument)
            : base(project)
        {
            XElement xmlUserElement = xmlUserDocument.Element("user");
            this.Deserialise(xmlUserElement);
        }
        #endregion // Constructor(s)

        #region XML Serialisation
        /// <summary>
        /// Generates an object from its XML representation.
        /// </summary>
        /// <param name="node"></param>
        public override void Deserialise(XmlNode node)
        {
            base.Deserialise(node);

            XmlNode usernameNode = node.SelectSingleNode("username");
            if (usernameNode == null)
            {
                throw new ArgumentException("Username node missing for user.");
            }
            UserName = usernameNode.InnerText;

            XmlNode friendlyNameNode = node.SelectSingleNode("friendlyName");
            if (friendlyNameNode == null)
            {
                throw new ArgumentException("Friendly name node missing for user.");
            }
            Name = friendlyNameNode.InnerText;

            XmlNode emailNode = node.SelectSingleNode("email");
            if (emailNode != null)
            {
                Email = emailNode.InnerText;
            }

            XmlNode jobTitleNode = node.SelectSingleNode("jobTitle");
            if (jobTitleNode != null)
            {
                this.JobTitle = jobTitleNode.InnerText;
            }
        }
        #endregion // XML Serialisation

        #region Static Controller Methods
        /// <summary>
        /// Return Bugstar User object from their ID.
        /// </summary>
        /// <param name="project"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static User GetUserById(Project project, uint id)
        {
            User user = null;

            try
            {
                BugstarQueryBuilder qb = new BugstarQueryBuilder(project.Query);
                qb.RelativeQuery = String.Format("{0}/{1}", Commands.BUGSTAR_USERS, id.ToString());
                BugstarQuery query = qb.ToQuery();

                using (Stream queryResult = project.Connection.RunQuery(query))
                {
                    if (queryResult != null)
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.Load(queryResult);

                        XmlNode userNode = doc.SelectSingleNode("/user");
                        if (userNode != null)
                        {
                            user = new User(project);
                            user.Deserialise(userNode);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                BugstarConnection.Log.ToolException(ex, "Unhandled exception while getting information for user id: {0}.", id);
            }

            return user;
        }
        #endregion // Static Controller Methods

        #region Public Methods
        /// <summary>
        /// Runs a simple search based on the type of object to search for
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public List<RSG.Interop.Bugstar.Search.Search> PerformSearch(SearchType type)
        {
            List<RSG.Interop.Bugstar.Search.Search> searchResults = new List<RSG.Interop.Bugstar.Search.Search>();

            try
            {
                // Get the bugstar "noun" for the search type
                string searchString = null;
                switch (type)
                {
                    case SearchType.Graph:
                        searchString = Commands.BUGSTAR_GRAPHS;
                        break;

                    case SearchType.RestReportInstance:
                        searchString = Commands.BUGSTAR_REPORTS;
                        break;

                    case SearchType.PublicSearch:
                        searchString = String.Format("{0}/{1}", 
                            Commands.BUGSTAR_BUGS, Commands.BUGSTAR_SEARCHES);
                        break;

                    default:
                        throw new ArgumentException(String.Format("Unsupported search type '{0}' encountered while attempting to perform search.", type.ToString()));
                }

                // Execute the actual query
                BugstarQueryBuilder qb = new BugstarQueryBuilder(this.Query);
                qb.RelativeQuery = searchString;
                BugstarQuery query = qb.ToQuery();

				//TODO: DHM FIX ME: should use RunQueryWithXDocumentResult!
                Stream queryResult = Connection.RunQuery(query);
                if (queryResult != null)
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(queryResult);

                    XmlNodeList searchNodes = doc.SelectNodes("/searches/search");
                    foreach (XmlNode searchNode in searchNodes)
                    {
                        RSG.Interop.Bugstar.Search.Search searchResult = new RSG.Interop.Bugstar.Search.Search(this);
                        searchResult.Deserialise(searchNode);
                        searchResults.Add(searchResult);
                    }
                }
            }
            catch (System.Exception ex)
            {
                BugstarConnection.Log.ToolExceptionCtx(LOG_CTX, ex, 
                    "Unhandled exception while getting the list of reports for user '{0}' (id: {1}).", 
                    this.Name, this.Id);
            }

            return searchResults;
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Deserialise; from XElement.
        /// </summary>
        /// <param name="xmlUserElem"></param>
        protected override void Deserialise(XElement xmlUserElem)
        {
            base.Deserialise(xmlUserElem);

            XElement xmlUsernameElem = xmlUserElem.Element("username");
            if (null != xmlUsernameElem)
                this.UserName = xmlUsernameElem.Value;

            XElement xmlFriendlyNameElem = xmlUserElem.Element("friendlyName");
            if (null != xmlFriendlyNameElem)
                this.UserName = xmlFriendlyNameElem.Value;

            XElement xmlEmailElem = xmlUserElem.Element("email");
            if (null != xmlEmailElem)
                this.Email = xmlEmailElem.Value;

            XElement xmlJobTitleElem = xmlUserElem.Element("jobTitle");
            if (null != xmlJobTitleElem)
                this.JobTitle = xmlJobTitleElem.Value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private BitmapImage GetImage()
        {
            // If the file exists in our cache use that, otherwise query bugstar for it
            BitmapImage result = new BitmapImage(); 
            Attachment imageAttachment = new Attachment(Project.Connection, m_imageAttachmentId);
            Stream dataStream = imageAttachment.Download();
            try
            {
                // Convert the stream to an image
                BitmapImage source = new BitmapImage();
                source.BeginInit();
                source.StreamSource = dataStream;
                source.EndInit();

                int stride = 4 * source.PixelWidth;
                byte[] pixels = new byte[stride * source.PixelHeight];
                source.CopyPixels(pixels, stride, 0);

                BitmapSource bitmapSource = BitmapSource.Create(source.PixelWidth, source.PixelHeight, 96.0, 96.0, source.Format, null, pixels, stride);

                MemoryStream jpegStream = new MemoryStream();
                JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(bitmapSource));
                encoder.Save(jpegStream);
                jpegStream.Position = 0;

                result.BeginInit();
                result.StreamSource = jpegStream;
                result.EndInit();
            }
            finally
            {
                dataStream.Close();
            }

            return result;
        }

        /// <summary>
        /// Queries bugstar for the list of reports for this user
        /// </summary>
        /// <returns></returns>
        private List<Report> GetReportsList()
        {
            List<Report> reports = new List<Report>();

            try
            {
                BugstarQueryBuilder qb = new BugstarQueryBuilder(this.Query);
                qb.RelativeQuery = Commands.BUGSTAR_REPORTS;
                BugstarQuery query = qb.ToQuery();

				//TODO: DHM FIX ME: should use RunQueryWithXDocumentResult!
                Stream queryResult = Connection.RunQuery(query);
                if (queryResult != null)
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(queryResult);

                    XmlNodeList searchNodes = doc.SelectNodes("/searches/search");
                    foreach (XmlNode searchNode in searchNodes)
                    {
                        Report report = new Report(this);
                        report.Deserialise(searchNode);
                        reports.Add(report);
                    }
                }
            }
            catch (System.Exception ex)
            {
                BugstarConnection.Log.ToolExceptionCtx(LOG_CTX, ex, 
                    "Unhandled exception while getting the list of reports for user '{0}' (id: {1}).", 
                    this.Name, this.Id);
            }

            return reports;
        }

        /// <summary>
        /// Queries bugstar for the list of graphs for this user
        /// Note: Currently this does it slightly inefficiently by searching for a list of id's then
        /// querying for each graph.
        /// </summary>
        /// <returns></returns>
        private List<Graph> GetGraphsList()
        {
            List<Graph> graphs = new List<Graph>();

            try
            {
                BugstarQueryBuilder qb = new BugstarQueryBuilder(this.Query);
                qb.RelativeQuery = Commands.BUGSTAR_GRAPHS;
                BugstarQuery query = qb.ToQuery();

				//TODO: DHM FIX ME: should use RunQueryWithXDocumentResult!
                Stream queryResult = Connection.RunQuery(query);
                if (queryResult != null)
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(queryResult);

                    XmlNodeList idNodes = doc.SelectNodes("/searches/search/id");
                    foreach (XmlNode idNode in idNodes)
                    {
                        graphs.Add(Graph.GetGraphById(this, UInt32.Parse(idNode.InnerText)));
                    }
                }
            }
            catch (System.Exception ex)
            {
                BugstarConnection.Log.ToolExceptionCtx(LOG_CTX, ex, 
                    "Unhandled exception while getting the list of graphs for user '{0}' (id: {1}).", 
                    this.Name, this.Id);
            }

            return graphs;
        }

        /// <summary>
        /// Queries bugstar for the list of goups for this user.
        /// </summary>
        /// <returns></returns>
        private List<Group> GetGroupsList()
        {
            List<Group> groups = new List<Group>();

            try
            {
                BugstarQueryBuilder qb = new BugstarQueryBuilder(Project.Query);
                qb.RelativeQuery = String.Format("{0}/{1}", Commands.BUGSTAR_CURRENTUSER, Commands.BUGSTAR_GROUPS);
                BugstarQuery query = qb.ToQuery();

                //TODO: DHM FIX ME: should use RunQueryWithXDocumentResult!
                Stream queryResult = Connection.RunQuery(query);
                if (queryResult != null)
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(queryResult);

                    XmlNodeList searchNodes = doc.SelectNodes("/searches/search");
                    foreach (XmlNode searchNode in searchNodes)
                    {
                        Group group = new Group(this);
                        group.Deserialise(searchNode);
                        groups.Add(group);
                    }
                }
            }
            catch (System.Exception ex)
            {
                BugstarConnection.Log.ToolExceptionCtx(LOG_CTX, ex,
                    "Unhandled exception while getting the list of groups for user '{0}' (id: {1}).",
                    this.Name, this.Id);
            }

            return groups;
        }

        /// <summary>
        /// Query current user for available permissions.
        /// </summary>
        /// <returns></returns>
        private IDictionary<PermissionType, bool> GetPermissionsTable()
        {
            IDictionary<PermissionType, bool> permissions = 
                new Dictionary<PermissionType, bool>();

            try
            {
                BugstarQueryBuilder qb = new BugstarQueryBuilder(this.Project.Query);
                qb.RelativeQuery = Commands.BUGSTAR_CURRENTUSER;
                BugstarQuery query = qb.ToQuery();

                Stream queryResult = Connection.RunQuery(query);
                if (null != queryResult)
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(queryResult);

                    XmlNodeList nodes = doc.SelectNodes("/user/*");
                    foreach (XmlNode node in nodes)
                    {
                        PermissionType type = PermissionTypeUtils.ConvertStringToPermissionType(node.Name);
                        if (PermissionType.None != type)
                            permissions.Add(type, bool.Parse(node.InnerText));
                    }
                }
            }
            catch (Exception ex)
            {
                BugstarConnection.Log.ToolExceptionCtx(LOG_CTX, ex, 
                    "Unhandled exception while getting user permissions.");
            }

            return (permissions);
        }
        #endregion // Private Methods
    } // User

} // RSG.Interop.Bugstar.Organisation
