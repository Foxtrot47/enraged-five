﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using RSG.Interop.Bugstar.Common;

namespace RSG.Interop.Bugstar.Organisation
{
    public abstract class Actor : BugstarObject
    {
        #region Properties
        /// <summary>
        /// The project this map is a part of
        /// </summary>
        public Project Project
        {
            get;
            protected set;
        }

        /// <summary>
        /// Name of the actor
        /// </summary>
        public string Name
        {
            get;
            protected set;
        }
        #endregion
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userNode"></param>
        public Actor(Project project)
            : base(project.Connection)
        {
            Project = project;
        }
        #endregion // Constructor(s)

        #region XML Serialisation
        /// <summary>
        /// Generates an object from its XML representation.
        /// </summary>
        /// <param name="node"></param>
        public override void Deserialise(XmlNode node)
        {
            base.Deserialise(node);
        }
        #endregion // XML Serialisation
    }
}
