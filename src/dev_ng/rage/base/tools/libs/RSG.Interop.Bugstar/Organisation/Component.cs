﻿using System;
using System.IO;
using System.Xml;
using RSG.Base.Logging;
using RSG.Base.Net;
using RSG.Interop.Bugstar.Common;

namespace RSG.Interop.Bugstar.Organisation
{

    /// <summary>
    /// Bug Component object.
    /// </summary>
    public class Component : BugstarObject
    {
        #region Constants
        private static readonly String LOG_CTX = "Bugstar:Component";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// The query to use to query bugstar about this object
        /// </summary>
        public override IRestQuery Query
        {
            get
            {
                BugstarQueryBuilder qb = new BugstarQueryBuilder(this.Project.Query);
                qb.RelativeQuery = String.Format("{0}/{1}", Commands.BUGSTAR_COMPONENTS, Id.ToString());
                return (qb.ToQuery());
            }
        }

        /// <summary>
        /// The project this map is a part of
        /// </summary>
        public Project Project
        {
            get;
            protected set;
        }

        /// <summary>
        /// Component's name
        /// </summary>
        public string Name
        {
            get;
            protected set;
        }

        /// <summary>
        /// A component's fullname is the concatenation of it's ancestors names
        /// </summary>
        public string FullName
        {
            get
            {
                string fullname = Name;

                // Work our way up the tree prefixing the parent names
                Component parent = Parent;
                while (parent != null)
                {
                    fullname = parent.Name + " : " + fullname;
                    parent = parent.Parent;
                }

                return fullname;
            }
        }

        /// <summary>
        /// Component description.
        /// </summary>
        public string Description
        {
            get;
            protected set;
        }

        /// <summary>
        /// Parent Component (this is only patched up after all components have been deserialised)
        /// </summary>
        public Component Parent
        {
            get
            {
                if (m_parent == null && m_parentId != 0)
                {
                    m_parent = Component.GetComponentById(Project, m_parentId);
                }
                return m_parent;
            }
        }
        private Component m_parent;
        private uint m_parentId;
        #endregion

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userNode"></param>
        public Component(Project project)
            : base(project.Connection)
        {
            Project = project;
        }
        #endregion // Constructor(s)

        #region XML Serialisation
        /// <summary>
        /// Generates an object from its XML representation.
        /// </summary>
        /// <param name="node"></param>
        public override void Deserialise(XmlNode node)
        {
            base.Deserialise(node);

            XmlNode nameNode = node.SelectSingleNode("name");
            if (nameNode != null)
            {
                Name = nameNode.InnerText;
            }
            
            XmlNode descriptionNode = node.SelectSingleNode("description");
            if (descriptionNode != null)
            {
                Description = descriptionNode.InnerText;
            }

            XmlNode parentIdNode = node.SelectSingleNode("parentId");
            if (parentIdNode != null)
            {
                m_parentId = UInt32.Parse(parentIdNode.InnerText);
            }
        }
        #endregion // XML Serialisation

        #region Static Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="project"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static Component GetComponentById(Project project, uint id)
        {
            Component comp = null;

            try
            {
                BugstarQueryBuilder qb = new BugstarQueryBuilder(project.Query);
                qb.RelativeQuery = String.Format("{0}/{1}", Commands.BUGSTAR_COMPONENTS, id.ToString());
                using (Stream queryResult = project.Connection.RunQuery(qb.ToQuery()))
                {
                    if (queryResult != null)
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.Load(queryResult);

                        XmlNode componentNode = doc.SelectSingleNode("/component");
                        if (componentNode != null)
                        {
                            comp = new Component(project);
                            comp.Deserialise(componentNode);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                BugstarConnection.Log.ToolExceptionCtx(LOG_CTX, ex, 
                    "Unhandled exception while getting information for team '{0}'.", id);
            }

            return comp;
        }
        #endregion
    } // Component
} // RSG.Interop.Bugstar.Organisation
