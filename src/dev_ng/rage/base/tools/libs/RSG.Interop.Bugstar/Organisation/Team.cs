﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using RSG.Base.Logging;
using RSG.Base.Net;
using RSG.Interop.Bugstar.Common;

namespace RSG.Interop.Bugstar.Organisation
{
    /// <summary>
    /// 
    /// </summary>
    public class Team : Actor
    {
        #region Constants
        private static readonly String LOG_CTX = "Bugstar:Team";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// The query to use to query bugstar about this object
        /// </summary>
        public override IRestQuery Query
        {
            get
            {
                return new BugstarQuery(Project.Query, Commands.BUGSTAR_TEAMS + "/" + Id.ToString());
            }
        }

        /// <summary>
        /// Parent of this team (or null if there isn't one)
        /// </summary>
        public Team Parent
        {
            get
            {
                if (m_parent == null && m_parentId != 0)
                {
                    m_parent = Team.GetTeamById(Project, m_parentId);
                }
                return m_parent;
            }
        }
        private Team m_parent;
        private uint m_parentId;
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="project"></param>
        public Team(Project project)
            : base(project)
        {
        }

        /// <summary>
        /// Constructor; from XDocument.
        /// </summary>
        /// <param name="project"></param>
        /// <param name="xmlTeamDocument"></param>
        public Team(Project project, XDocument xmlTeamDocument)
            : base(project)
        {
            XElement xmlUserElement = xmlTeamDocument.Element("team");
            this.Deserialise(xmlUserElement);
        }
        #endregion // Constructor(s)

        #region XML Serialisation
        /// <summary>
        /// Deserialise; from XElement.
        /// </summary>
        /// <param name="xmlTeamElem"></param>
        protected override void Deserialise(XElement xmlTeamElem)
        {
            base.Deserialise(xmlTeamElem);

            XElement xmlNameElem = xmlTeamElem.Element("name");
            if (null != xmlNameElem)
                this.Name = xmlNameElem.Value;

            XElement xmlParentIdElem = xmlTeamElem.Element("parentId");
            if (null != xmlParentIdElem)
                uint.TryParse(xmlParentIdElem.Value, out m_parentId);                    
        }

        /// <summary>
        /// Generates an object from its XML representation.
        /// </summary>
        /// <param name="node"></param>
        public override void Deserialise(XmlNode node)
        {
            base.Deserialise(node);

            XmlNode nameNode = node.SelectSingleNode("name");
            if (nameNode == null)
            {
                throw new ArgumentException("Name node missing for team.");
            }
            Name = nameNode.InnerText;

            XmlNode parentNode = node.SelectSingleNode("parentId");
            if (parentNode != null)
            {
                m_parentId = UInt32.Parse(parentNode.InnerText);
            }
        }
        #endregion // XML Serialisation

        #region Static Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="project"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static Team GetTeamById(Project project, uint id)
        {
            Team team = null;

            try
            {
                BugstarQueryBuilder qb = new BugstarQueryBuilder(project.Query);
                qb.RelativeQuery = String.Join("/", Commands.BUGSTAR_TEAMS, id.ToString());
                BugstarQuery query = qb.ToQuery();

                using (Stream queryResult = project.Connection.RunQuery(query))
                {
                    if (queryResult != null)
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.Load(queryResult);

                        XmlNode userNode = doc.SelectSingleNode("/team");
                        if (userNode != null)
                        {
                            team = new Team(project);
                            team.Deserialise(userNode);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                BugstarConnection.Log.ToolExceptionCtx(LOG_CTX, ex, 
                    "Unhandled exception while getting information for team '{0}'.", 
                    id);
            }

            return team;
        }
        #endregion
    } // Team
} // RSG.Interop.Bugstar.Organisation
