﻿using System;
using System.Reflection;
using RSG.Interop.Bugstar.Attributes;

namespace RSG.Interop.Bugstar.Organisation
{

    /// <summary>
    /// Permission type enumeration.
    /// </summary>
    public enum PermissionType
    {
        None,

        [BugstarData("superAdmin")]
        SuperAdmin,
        
        [BugstarData("featureTester")]
        FeatureTester,
        
        [BugstarData("timestarUser")]        
        TimestarUser,
        
        [BugstarData("timestarAdmin")]        
        TimestarAdmin,
        
        [BugstarData("timesheetValidator")]        
        TimesheetValidator,
        
        [BugstarData("userAdmin")]        
        UserAdmin,
        
        [BugstarData("componentAdmin")]        
        ComponentAdmin,
        
        [BugstarData("missionAdmin")]        
        MissionAdmin,
        
        [BugstarData("searchAdmin")]        
        SearchAdmin,
        
        [BugstarData("buildAdmin")]        
        BuildAdmin,
        
        [BugstarData("admin")]        
        Admin,
        
        [BugstarData("developer")]        
        Developer,
        
        [BugstarData("tester")]        
        Tester,
        
        [BugstarData("reviewer")]        
        Reviewer,
    }

    /// <summary>
    /// PermissionType enum utility and extension methods.
    /// </summary>
    public static class PermissionTypeUtils
    {
        /// <summary>
        /// Convert a String into a PermissionType (default: None).
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static PermissionType ConvertStringToPermissionType(String s)
        {
            foreach (PermissionType type in Enum.GetValues(typeof(PermissionType)))
            {
                String data = type.GetBugstarData();
                if (0 == String.Compare(data, s))
                    return (type);
            }
            return (PermissionType.None);
        }
    }

} // RSG.Interop.Bugstar.Organisation namespace
