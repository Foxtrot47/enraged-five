﻿using System;
using System.IO;
using System.Text;
using System.Xml.Linq;
using RSG.Base.Net;
using RSG.Interop.Bugstar.Common;
using RSG.Interop.Bugstar.Organisation;

namespace RSG.Interop.Bugstar
{

    /// <summary>
    /// Bugstar Attachment object.
    /// </summary>
    public class Attachment : BugstarObject
    {
        #region Constants
        /// <summary>
        /// Download buffer size.
        /// </summary>
        private const int DOWNLOAD_BUFFER_SIZE = 32768;

        /// <summary>
        /// Cache directory.
        /// </summary>
        private const String CACHE_DIRECTORY = "Bugstar/Attachments";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// The query to use to query Bugstar about this object
        /// </summary>
        public override IRestQuery Query
        {
            get
            {
                BugstarQueryBuilder qb = new BugstarQueryBuilder(
                    this.Connection.AttachmentService);
                qb.RelativeQuery = Commands.BUGSTAR_DOWNLOAD_ATTACHMENT;

                // Attachment IDs now have to be encoded in base64
                string attachmentID = System.Convert.ToBase64String(Encoding.UTF8.GetBytes(Id.ToString()));
                // ...then have a semi-colon added after the first encode...
                attachmentID = String.Concat(attachmentID, ";");
                // ...and then encoded again
                attachmentID = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(attachmentID));

                qb.Parameters.Add("id", attachmentID);

                BugstarQuery query = qb.ToQuery();
                return query;
            }
        }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// Constructor; from Bugstar Id.
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="id"></param>
        public Attachment(BugstarConnection connection, uint id)
            : base(connection)
        {
            Id = id;
        }

        /// <summary>
        /// Constructor; from XDocument.
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="xmlAttachmentDocument"></param>
        internal Attachment(BugstarConnection connection, XDocument xmlAttachmentDocument)
            : base(connection)
        {
            XElement xmlAttachmentElem = xmlAttachmentDocument.Element("attachment");
            this.Deserialise(xmlAttachmentElem);
        }

        

        /// <summary>
        /// Constructor; from Stream.
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="stream"></param>
        internal Attachment(BugstarConnection connection, Stream stream)
            : base(connection)
        {
            XDocument xmlAttachmentDocument = XDocument.Load(stream);
            XElement xmlAttachmentElem = xmlAttachmentDocument.Element("attachment");
            this.Deserialise(xmlAttachmentElem);
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Download attachment data to file.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public bool Download(String filename)
        {
            try
            {
                using (FileStream fs = new FileStream(filename, FileMode.CreateNew,
                    FileAccess.Write))
                {
                    Byte[] buffer = new Byte[DOWNLOAD_BUFFER_SIZE];
                    int bytesRead = 0;
                    Stream stream = this.Download();
                    while ((bytesRead = stream.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        fs.Write(buffer, 0, bytesRead);
                    }
                    return (true);
                }
            }
            catch (Exception ex)
            {
                BugstarConnection.Log.ToolException(ex, "Failed to download attachment {0}.", this.Id);
                return (false);
            }
        }

        /// <summary>
        /// Download attachment data into stream.
        /// </summary>
        /// <returns></returns>
        public Stream Download()
        {
            // If the file exists in our cache use that, otherwise query bugstar for it
            Stream dataStream = null;

            // Get the temp directory
            String cacheDirectory = Path.Combine(Environment.ExpandEnvironmentVariables("%TEMP%"), "Bugstar", "Attachments");
            if (!Directory.Exists(cacheDirectory))
            {
                Directory.CreateDirectory(cacheDirectory);
            }

            // The file name for the attachment is simply the attachment cache directory along with the attachment's id
            string filePath = Path.Combine(cacheDirectory, Id.ToString());

            // Only use the cached file if it exists and is less than a day old
            if (File.Exists(filePath))
            {
                FileInfo file = new FileInfo(filePath);

                if ((DateTime.Now - file.LastWriteTime).TotalDays < 1.0)
                {
                    dataStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
                }
            }

            // If we didn't use the cached version, query Bugstar for it
            if (dataStream == null)
            {
                using (Stream webStream = Connection.RunQuery(Query))
                {
                    if (webStream != null)
                    {
                        // Copy the stream from the web response to a memory stream (to return) and a file stream (to cache for later lookups)
                        MemoryStream memoryStream = new MemoryStream();
                        FileStream outputStream = new FileStream(filePath, FileMode.Create, FileAccess.Write);

                        Byte[] buffer = new Byte[DOWNLOAD_BUFFER_SIZE];
                        int read;
                        while ((read = webStream.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            memoryStream.Write(buffer, 0, read);
                            outputStream.Write(buffer, 0, read);
                        }

                        // Save the file stream out to file
                        outputStream.Flush();
                        outputStream.Close();

                        // Reset the memory stream and set it as the return value
                        memoryStream.Position = 0;
                        dataStream = memoryStream;
                    }
                }
            }

            return dataStream;
        }
        #endregion // Controller Methods

        #region Protected Methods
        /// <summary>
        /// Deserialise; from XElement.
        /// </summary>
        /// <param name="xmlObjectElem"></param>
        protected override void Deserialise(XElement xmlObjectElem)
        {
            base.Deserialise(xmlObjectElem);

            // *sigh* consistency in the REST interface would be nice.
            if (0 == this.Id)
            {
                XElement xmlIdElem = xmlObjectElem.Element("attachmentId");
                if (null != xmlIdElem)
                {
                    uint id = 0;
                    if (uint.TryParse(xmlIdElem.Value, out id))
                        this.Id = id;
                }
            }
        }
        #endregion // Private Methods
    }

} // RSG.Interop.Bugstar namespace
