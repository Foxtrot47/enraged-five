﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace RSG.Interop.Bugstar.Common
{
    /// <summary>
    /// 
    /// </summary>
    public class Priority : EnumObject
    {
        #region Constructor(s)
        /// <summary>
        /// Internal constructor for deserialisation
        /// </summary>
        internal Priority()
            : base()
        {
        }
        #endregion // Constructor(s)
    } // Priority
}
