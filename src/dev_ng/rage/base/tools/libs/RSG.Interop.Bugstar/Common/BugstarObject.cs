﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using System.Xml.Schema;
using System.Xml;
using System.Xml.Linq;
using RSG.Base.Net;

namespace RSG.Interop.Bugstar.Common
{
    /// <summary>
    /// Abstract base class for bugstar objects
    /// </summary>
    public abstract class BugstarObject : IBugstarObject
    {
        #region Properties
        /// <summary>
        /// Bugstar Connection object.
        /// </summary>
        public BugstarConnection Connection
        {
            get;
            protected set;
        }

        /// <summary>
        /// The query to use to query bugstar about this object
        /// </summary>
        public abstract IRestQuery Query
        {
            get;
        }

        /// <summary>
        /// Unique ID of the object
        /// </summary>
        public uint Id
        {
            get;
            protected set;
        }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="connection"></param>
        public BugstarObject(BugstarConnection connection)
        {
            Connection = connection;
        }
        #endregion // Constructor(s)

        #region XML Serialisation
        /// <summary>
        /// Generates an object from its XML representation.
        /// </summary>
        /// <param name="node"></param>
        public virtual void Deserialise(XmlNode node)
        {
            XmlNode idNode = node.SelectSingleNode("id");
            if (idNode == null)
            {
                throw new SerializationException("Id attribute missing for bugstar object.");
            }
            Id = UInt32.Parse(idNode.InnerText);
        }

        /// <summary>
        /// Deserialise; from XElement.
        /// </summary>
        /// <param name="xmlObjectElem"></param>
        protected virtual void Deserialise(XElement xmlObjectElem)
        {
            XElement xmlIdElem = xmlObjectElem.Element("id");
            if (null != xmlIdElem)
            {
                uint id = 0;
                if (uint.TryParse(xmlIdElem.Value, out id))
                    this.Id = id;
            }
        }
        #endregion // XML Serialisation
    } // BugstarObject
} // RSG.Interop.Bugstar.Common
