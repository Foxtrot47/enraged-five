﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace RSG.Interop.Bugstar.Common
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot("enum")]
    public abstract class EnumObject
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("name")]
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("description")]
        public string Description
        {
            get;
            set;
        }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// Internal constructor for deserialisation
        /// </summary>
        internal EnumObject()
        {
        }
        #endregion // Constructor(s)
    } // EnumObject

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [XmlRoot("enums")]
    public class EnumObjectList<T> where T : EnumObject
    {
        public EnumObjectList()
        {
            Items = new List<T>();
        }

        [XmlElement("enum")]
        public List<T> Items { get; set; }
    } // EnumObjectList
}
