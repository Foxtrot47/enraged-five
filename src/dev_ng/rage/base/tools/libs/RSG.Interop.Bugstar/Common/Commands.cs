﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Interop.Bugstar.Common
{
    internal static class Commands
    {
        #region REST Service Queries
        // Attachment-related.
        internal static String BUGSTAR_CREATE_ATTACHMENT = "CreateAttachment";
        internal static String BUGSTAR_REGISTER_ATTACHMENT = "RegisterAttachment";

        // Core Bug-related.
        internal static String BUGSTAR_BUGS         = "Bugs";
        internal static String BUGSTAR_CATEGORIES   = "Categories";
        internal static String BUGSTAR_COMPONENTS   = "Components";
        internal static String BUGSTAR_CURRENTUSER  = "CurrentUser";
        internal static String BUGSTAR_DATA         = "Data";
        internal static String BUGSTAR_DATAPOINTS   = "DataPoints";
        internal static String BUGSTAR_GRAPHS       = "Graphs";
        internal static String BUGSTAR_GRIDS        = "Grids";
        internal static String BUGSTAR_GROUPS       = "Groups";
        internal static String BUGSTAR_MAPS         = "Maps";
        internal static String BUGSTAR_MISSIONS     = "MissionsV2";
        internal static String BUGSTAR_MISSION_CHECKPOINT = "Checkpoint";
        internal static String BUGSTAR_PARAMETERS   = "Parameters";
        internal static String BUGSTAR_PRIORITIES   = "Priorities";
        internal static String BUGSTAR_PROJECTS     = "Projects";
        internal static String BUGSTAR_REPORTS      = "Reports";
        internal static String BUGSTAR_SEARCHES     = "Searches";
        internal static String BUGSTAR_STYLESHEET   = "Stylesheet";
        internal static String BUGSTAR_TEAMS        = "TeamsV2";
        internal static String BUGSTAR_USERS        = "Users";

        internal static String BUGSTAR_SEARCH_MODE_OPENBYLOCATION = "OpenByLocation";
        internal static String BUGSTAR_SEARCH_MODE_FULLTEXT = "FulltextSearch";
        #endregion // REST Service Queries

        #region Attachment Service Queries
        internal static String BUGSTAR_DOWNLOAD_ATTACHMENT = "DownloadAttachment";
        #endregion // Attachment Service Queries
    } // Commands
} // RSG.Interop.Bugstar.Common
