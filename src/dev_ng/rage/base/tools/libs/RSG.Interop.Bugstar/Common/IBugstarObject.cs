﻿using RSG.Base.Net;

namespace RSG.Interop.Bugstar.Common
{
    /// <summary>
    /// Interface for bugstar objects
    /// </summary>
    public interface IBugstarObject
    {
        /// <summary>
        /// Connection to bugstar
        /// </summary>
        BugstarConnection Connection
        {
            get;
        }

        /// <summary>
        /// The query to use to query bugstar about this object
        /// </summary>
        IRestQuery Query
        {
            get;
        }

        /// <summary>
        /// Unique ID of the object
        /// </summary>
        uint Id
        {
            get;
        }

        /// <summary>
        /// Refresh the object's data by querying bugstar
        /// </summary>
        //void Refresh();
    } // IBugstarObject
} // RSG.Interop.Bugstar.Common
