﻿using System;
using System.Reflection;

namespace RSG.Interop.Bugstar.Attributes
{

    /// <summary>
    /// Attribute for specifying the Bugstar REST XML string for a field.
    /// </summary>
    public class BugstarDataAttribute : Attribute
    {
        #region Properties
        /// <summary>
        /// Data name for the field.
        /// </summary>
        public String Data
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor; taking friendly name string.
        /// </summary>
        /// <param name="data_name"></param>
        public BugstarDataAttribute(String data_name)
        {
            this.Data = data_name;
        }
        #endregion // Constructor(s)
    }

    /// <summary>
    /// Extension methods for Bugstar Data Attribute.
    /// </summary>
    public static class BugstarDataAttributeExtensions
    {
        /// <summary>
        /// Return Bugstar Data attribute value for an enum.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static String GetBugstarData(this Enum value)
        {
            Type type = value.GetType();

            FieldInfo fi = type.GetField(value.ToString());
            BugstarDataAttribute[] attributes = fi.GetCustomAttributes(typeof(BugstarDataAttribute), false) as BugstarDataAttribute[];

            return (attributes.Length > 0 ? attributes[0].Data : String.Empty);
        }

        /// <summary>
        /// Return enum value for String.
        /// </summary>
        /// <typeparam name="BugstarEnum"></typeparam>
        /// <param name="value"></param>
        /// <param name="defValue"></param>
        /// <returns></returns>
        public static BugstarEnum GetBugstarEnumFromString<BugstarEnum>(String value, BugstarEnum defValue)
        {
            Type type = typeof(BugstarEnum);
            BugstarEnum[] values = (BugstarEnum[])Enum.GetValues(type);

            foreach (BugstarEnum v in values)
            {
                FieldInfo fi = type.GetField(v.ToString());
                BugstarDataAttribute[] attributes = (BugstarDataAttribute[])
                    fi.GetCustomAttributes(typeof(BugstarDataAttribute), false);

                if (attributes.Length > 0 &&
                    String.Equals(attributes[0].Data, value, StringComparison.CurrentCultureIgnoreCase))
                    return (v);
            }

            return (defValue);
        }
    }

} // RSG.Interop.Bugstar.Attributes
