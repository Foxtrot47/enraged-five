﻿using System;
using System.Reflection;
using RSG.Interop.Bugstar.Attributes;

namespace RSG.Interop.Bugstar
{

    /// <summary>
    /// Enumeration for Bug class.
    /// </summary>
    public enum BugCategory
    {
        [BugstarData("A")]
        A,
        
        [BugstarData("B")]
        B,

        [BugstarData("C")]
        C,

        [BugstarData("D")]
        D,

        [BugstarData("TASK")]
        Task,

        [BugstarData("TODO")]
        Todo,

        [BugstarData("TRACK")]
        Track
    }

    /// <summary>
    /// BugCategory enum extension and utility methods.
    /// </summary>
    public static class BugCategoryUtils
    {
        /// <summary>
        /// Return BugCategory from Bugstar REST XML string.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static BugCategory FromBugstarString(String data)
        {
            BugCategory category = BugstarDataAttributeExtensions.GetBugstarEnumFromString
                <BugCategory>(data, BugCategory.C);
            return (category);
        }

        /// <summary>
        /// Return Bugstar REST XML string from BugCategory.
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        public static String ToBugstarString(this BugCategory category)
        {
            String categoryString = category.GetBugstarData();
            return (categoryString);
        }
    }

} // RSG.Interop.Bugstar namespace
