﻿using System;
using System.Collections.Generic;
using System.Linq;
using RSG.Base.Net;

namespace RSG.Interop.Bugstar
{

    /// <summary>
    /// Utility class to create immutable BugstarQuery objects.
    /// </summary>
    public sealed class BugstarQueryBuilder
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public Uri BaseQuery
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public String RelativeQuery
        {
            get;
            set;
        }
        
        /// <summary>
        /// How the parameters will be sent to the bugstar service
        /// </summary>
        public RequestMethod Method
        {
            get;
            set;
        }

        /// <summary>
        /// Content-type for POST/PUT data.
        /// </summary>
        public String ContentType
        {
            get;
            set;
        }

        /// <summary>
        /// REST PUT/POST data.
        /// </summary>
        public String Data
        {
            get;
            set;
        }

        /// <summary>
        /// Dictionary containing any "get/post" parameters
        /// </summary>
        public IDictionary<String, String> Parameters
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance fo the <see cref="BugstarQueryBuilder"/> class
        /// using the provided base query.
        /// </summary>
        /// <param name="baseQuery"></param>
        public BugstarQueryBuilder(IRestQuery baseQuery)
            : this(baseQuery.QueryUri)
        {
        }

        /// <summary>
        /// Initialises a new instance fo the <see cref="BugstarQueryBuilder"/> class
        /// using the provided base query.
        /// </summary>
        /// <param name="baseQuery"></param>
        public BugstarQueryBuilder(Uri baseQuery)
        {
            this.BaseQuery = baseQuery;
            this.Method = RequestMethod.GET;
            this.ContentType = "application/xml";
            this.Parameters = new Dictionary<String, String>();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Return concrete BugstarQuery.
        /// </summary>
        /// <returns></returns>
        public BugstarQuery ToQuery()
        {
            String relative = this.RelativeQuery;
            if (Method == RequestMethod.GET && Parameters.Any())
            {
                // Generate the parameters string
                String parameterString = "?";
                foreach (KeyValuePair<string, string> pair in Parameters)
                {
                    parameterString += String.Format("{0}={1}&", pair.Key, pair.Value);
                }
                parameterString = parameterString.TrimEnd(new char[] { '&' });
                relative += parameterString;
            }

            Uri query = new Uri(String.Format("{0}/{1}", this.BaseQuery, relative));
            return (new BugstarQuery(query, this.Method, this.Data));
        }
        #endregion // Controller Methods
    }

} // RSG.Interop.Bugstar namespace
