﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using RSG.Base.Logging;
using RSG.Base.Net;
using RSG.Interop.Bugstar.Common;
using RSG.Interop.Bugstar.Organisation;
using System.Diagnostics;

namespace RSG.Interop.Bugstar.Game
{

    /// <summary>
    /// Bugstar mission.
    /// </summary>
    public class Mission : BugstarObject, IComparable<Mission>
    {
        #region Constants
        private static readonly String LOG_CTX = "Bugstar:Mission";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// The query to use to query bugstar about this object
        /// </summary>
        public override IRestQuery Query
        {
            get
            {
                BugstarQueryBuilder qb = new BugstarQueryBuilder(this.Project.Query);
                qb.RelativeQuery = String.Format("{0}/{1}", Commands.BUGSTAR_MISSIONS, this.Id);
                return (qb.ToQuery());
            }
        }

        /// <summary>
        /// The project this map is a part of
        /// </summary>
        public Project Project
        {
            get;
            protected set;
        }

        /// <summary>
        /// Mission's name
        /// </summary>
        public string Name
        {
            get;
            protected set;
        }

        /// <summary>
        /// Description of the mission
        /// </summary>
        public string Description
        {
            get;
            protected set;
        }

        /// <summary>
        /// Script for the mission
        /// </summary>
        public string ScriptName
        {
            get;
            protected set;
        }

        /// <summary>
        /// Mission variant
        /// </summary>
        public uint Variant
        {
            get;
            protected set;
        }

        /// <summary>
        /// Current issues with the mission
        /// </summary>
        public string Issues
        {
            get;
            protected set;
        }

        /// <summary>
        /// Story description for the mission
        /// </summary>
        public string StoryDescription
        {
            get;
            protected set;
        }

        /// <summary>
        /// Mission Id for the mission
        /// </summary>
        public string MissionId
        {
            get;
            protected set;
        }

        /// <summary>
        /// 
        /// </summary>
        public float ProjectedAttempts
        {
            get;
            protected set;
        }

        /// <summary>
        /// 
        /// </summary>
        public float ProjectedAttemptsMin
        {
            get;
            protected set;
        }

        /// <summary>
        /// 
        /// </summary>
        public float ProjectedAttemptsMax
        {
            get;
            protected set;
        }

        /// <summary>
        /// Whether this is a singleplayer mission or not.
        /// </summary>
        public bool Singleplayer
        {
            get;
            protected set;
        }

        /// <summary>
        /// Whether this mission is in use in the game.
        /// </summary>
        public bool Active
        {
            get;
            protected set;
        }

        /// <summary>
        /// List of checkpoints this mission has.
        /// </summary>
        public MissionCheckpoint[] Checkpoints
        {
            get;
            protected set;
        }

        //TOADD:
        // played
        // adminPlayed
        // percentageComplete
        // designerInitials
        #endregion

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="missionNode"></param>
        public Mission(Project project)
            : base(project.Connection)
        {
            Project = project;
            Checkpoints = new MissionCheckpoint[] { };
        }
        #endregion // Constructor(s)

        #region XML Serialisation
        /// <summary>
        /// Generates an object from its XML representation.
        /// </summary>
        /// <param name="node"></param>
        public override void Deserialise(XmlNode node)
        {
            base.Deserialise(node);

            XmlNode nameNode = node.SelectSingleNode("name");
            if (nameNode != null)
            {
                Name = nameNode.InnerText;
            }

            XmlNode descriptionNode = node.SelectSingleNode("description");
            if (descriptionNode != null)
            {
                Description = descriptionNode.InnerText;
            }

            XmlNode scriptNameNode = node.SelectSingleNode("scriptName");
            if (scriptNameNode != null)
            {
                ScriptName = scriptNameNode.InnerText;
            }

            XmlNode variantNode = node.SelectSingleNode("variantNumber");
            if (variantNode != null)
            {
                Variant = UInt32.Parse(variantNode.InnerText);
            }

            XmlNode issuesNode = node.SelectSingleNode("issues");
            if (issuesNode != null)
            {
                Issues = issuesNode.InnerText;
            }

            XmlNode storyDescriptionNode = node.SelectSingleNode("storyDescription");
            if (storyDescriptionNode != null)
            {
                StoryDescription = storyDescriptionNode.InnerText;
            }

            XmlNode missionIdNode = node.SelectSingleNode("missionId");
            if (missionIdNode != null)
            {
                MissionId = missionIdNode.InnerText;
            }

            XmlNode attemptsNode = node.SelectSingleNode("projectedAttempts");
            if (attemptsNode != null)
            {
                ProjectedAttempts = (uint)Single.Parse(attemptsNode.InnerText);
            }

            XmlNode attemptsMinNode = node.SelectSingleNode("projectedAttemptsMin");
            if (attemptsMinNode != null)
            {
                ProjectedAttemptsMin = (uint)Single.Parse(attemptsMinNode.InnerText);
            }

            XmlNode attemptsMaxNode = node.SelectSingleNode("projectedAttemptsMax");
            if (attemptsMaxNode != null)
            {
                ProjectedAttemptsMax = (uint)Single.Parse(attemptsMaxNode.InnerText);
            }

            // Read the singleplayer value next
            XmlNode singleplayerNode = node.SelectSingleNode("singleplayer");
            Debug.Assert(singleplayerNode != null, "Bugstar mission data doesn't contain singleplayer node.");
            if (singleplayerNode == null)
            {
                throw new InvalidDataException("Bugstar mission data doesn't contain singleplayer node.");
            }

            bool value;
            if (Boolean.TryParse(singleplayerNode.InnerText, out value))
            {
                Singleplayer = value;
            }
            else
            {
                Debug.Assert(false, "Bugstar mission data singleplayer node isn't a bool.");
                throw new InvalidDataException("Bugstar mission data singleplayer node isn't a bool.");
            }

            // Read the inactive flag next
            XmlNode inactiveNode = node.SelectSingleNode("inactive");
            Debug.Assert(inactiveNode != null, "Bugstar mission data doesn't contain inactive node.");
            if (inactiveNode == null)
            {
                throw new InvalidDataException("Bugstar mission data doesn't contain inactive node.");
            }

            if (Boolean.TryParse(inactiveNode.InnerText, out value))
            {
                Active = !value;
            }
            else
            {
                Debug.Assert(false, "Bugstar mission data inactive node isn't a bool.");
                throw new InvalidDataException("Bugstar mission data inactive node isn't a bool.");
            }

            // Mission checkpoints
            XmlNode checkpointsNode = node.SelectSingleNode("checkpoints");
            Debug.Assert(checkpointsNode != null, "Bugstar mission data doesn't contain checkpoints node.");
            if (checkpointsNode == null)
            {
                throw new InvalidDataException("Bugstar mission data doesn't contain checkpoints node.");
            }
            List<MissionCheckpoint> checkpoints = new List<MissionCheckpoint>();
            foreach (XmlNode checkpointNode in checkpointsNode.SelectNodes("checkpoint"))
            {
                MissionCheckpoint checkpoint = new MissionCheckpoint(this);
                checkpoint.Deserialise(checkpointNode);
                checkpoints.Add(checkpoint);
            }
            Checkpoints = checkpoints.OrderBy(item => item.Ordering).ToArray();

            // Make sure the checkpoints are consecutive ascending indices.
            for (int i = 0; i < Checkpoints.Length; ++i)
            {
                Debug.Assert(Checkpoints[i].Ordering == i, "Bugstar returned non-consecutive indices.");
                if (Checkpoints[i].Ordering != i)
                {
                    throw new InvalidDataException("Bugstar returned non-consecutive indices.");
                }
            }

            // If the mission has checkpoints, check to see if we need to override the projected attempt information if
            // bugstar didn't provide us with anything.
            if (Checkpoints.Any())
            {
                if (ProjectedAttempts == 0)
                {
                    // + 1 as that is the base line.
                    ProjectedAttempts = Checkpoints.Sum(item => item.ProjectedAttempts) - Checkpoints.Count() + 1;
                }
                if (ProjectedAttemptsMin == 0)
                {
                    ProjectedAttemptsMin = Math.Max(1, ProjectedAttempts - 1);
                }
                if (ProjectedAttemptsMax == 0)
                {
                    ProjectedAttemptsMax = ProjectedAttempts + 1;
                }
            }
            else
            {
                if (ProjectedAttempts == 0)
                {
                    ProjectedAttempts = 1;
                }
                if (ProjectedAttemptsMin == 0)
                {
                    ProjectedAttemptsMin = Math.Max(1, ProjectedAttempts - 1);
                }
                if (ProjectedAttemptsMax == 0)
                {
                    ProjectedAttemptsMax = ProjectedAttempts + 1;
                }
            }
        }
        #endregion // XML Serialisation

        #region Static Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="project"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static Mission GetMissionById(Project project, uint id)
        {
            Mission mission = null;

            try
            {
                BugstarQueryBuilder qb = new BugstarQueryBuilder(project.Query);
                qb.RelativeQuery = String.Format("{0}/{1}", Commands.BUGSTAR_MISSIONS, id);
                BugstarQuery query = qb.ToQuery();

				//TODO: DHM FIX ME: should use RunQueryWithXDocumentResult!
                using (Stream queryResult = project.Connection.RunQuery(query))
                {
                    if (queryResult != null)
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.Load(queryResult);

                        XmlNode missionNode = doc.SelectSingleNode("/mission");
                        if (missionNode != null)
                        {
                            mission = new Mission(project);
                            mission.Deserialise(missionNode);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                BugstarConnection.Log.ToolExceptionCtx(LOG_CTX, ex, 
                    "Unhandled exception while getting information for team '{0}'.", id);
            }

            return mission;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Queries bugstar for a list of bugs that are associated with this mission
        /// </summary>
        /// <returns></returns>
        public List<Bug> GetBugs()
        {
            return GetBugs("Id");
        }

        /// <summary>
        /// Queries bugstar for a list of bugs that are associated with this mission returning the requested fields
        /// </summary>
        /// <param name="fields"></param>
        /// <returns></returns>
        public List<Bug> GetBugs(string fields)
        {
            List<Bug> bugs = new List<Bug>();

            // The resulting stream should contain xml, so convert it and extract the information we are after
            BugstarQueryBuilder qb = new BugstarQueryBuilder(this.Query);
            qb.RelativeQuery = Commands.BUGSTAR_BUGS;
            qb.Parameters.Add("fields", fields);
            BugstarQuery query = qb.ToQuery();

			//TODO: DHM FIX ME: should use RunQueryWithXDocumentResult!
            using (Stream queryResult = Connection.RunQuery(query))
            {
                if (queryResult != null)
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(queryResult);

                    XmlNodeList bugNodes = doc.SelectNodes("/bugs/bug");
                    foreach (XmlNode bugNode in bugNodes)
                    {
                        Bug bug = new Bug(this.Project);
                        bug.Deserialise(bugNode);
                        bugs.Add(bug);
                    }
                }
            }

            return bugs;
        }
        #endregion // Public Methods

        #region IComparable<Mission> Implementation
        public int CompareTo(Mission other)
        {
            // If other is not a valid object reference, this instance is greater.
            if (other == null)
            {
                return 1;
            }

            // Just compare the names
            return this.Name.CompareTo(other.Name);
        }
        #endregion // IComparable<MapGrid> Implementation
    } // Mission
} // RSG.Interop.Bugstar.Game
