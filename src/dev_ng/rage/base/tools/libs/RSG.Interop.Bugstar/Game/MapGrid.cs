﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using RSG.Base.Math;
using RSG.Base.Net;
using RSG.Interop.Bugstar.Common;
using System.Windows.Media;

namespace RSG.Interop.Bugstar.Game
{
    /// <summary>
    /// 
    /// </summary>
    public class MapGrid : BugstarObject, IComparable<MapGrid>
    {
        #region Properties
        /// <summary>
        /// The query to use to query bugstar about this object
        /// </summary>
        public override IRestQuery Query
        {
            get
            {
                BugstarQueryBuilder qb = new BugstarQueryBuilder(Map.Query);
                qb.RelativeQuery = String.Format("{0}/{1}", Commands.BUGSTAR_GRIDS, this.Id);
                return (qb.ToQuery());
            }
        }

        /// <summary>
        /// The map this grid is a part of
        /// </summary>
        public Map Map
        {
            get;
            protected set;
        }

        /// <summary>
        /// Grid's name
        /// </summary>
        public string Name
        {
            get;
            protected set;
        }

        /// <summary>
        /// Colour of the map grid
        /// </summary>
        public Color Colour
        {
            get;
            protected set;
        }

        /// <summary>
        /// Whether the grid is active
        /// </summary>
        public bool Active
        {
            get;
            protected set;
        }

        /// <summary>
        /// List of points that make up the outline of the section
        /// </summary>
        public List<Vector2f> Points
        {
            get;
            protected set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="gridNode"></param>
        public MapGrid(Map map)
            : base(map.Connection)
        {
            Map = map;
        }
        #endregion // Constructor(s)

        #region XML Serialisation
        /// <summary>
        /// Generates an object from its XML representation.
        /// </summary>
        /// <param name="node"></param>
        public override void Deserialise(XmlNode node)
        {
            base.Deserialise(node);

            XmlNode nameNode = node.SelectSingleNode("name");
            if (nameNode == null)
            {
                throw new ArgumentException(String.Format("Name node missing for map (id: {0}).", Id));
            }
            Name = nameNode.InnerText;

            XmlNode colourNode = node.SelectSingleNode("colour");
            if (colourNode != null)
            {
                int value = Int32.Parse(colourNode.InnerText);
                
                Color col = new Color();
                col.A = (byte)((value >> 24) & 0xFF);
                col.R = (byte)((value >> 16) & 0xFF);
                col.G = (byte)((value >> 8) & 0xFF);
                col.B = (byte)(value & 0xFF);
                Colour = col;
            }

            XmlNode activeNode = node.SelectSingleNode("active");
            if (activeNode != null)
            {
                Active = Boolean.Parse(activeNode.InnerText);
            }

            // Read in the points data
            Points = new List<Vector2f>();

            XmlNodeList pointNodes = node.SelectNodes("points");
            foreach (XmlNode pointNode in pointNodes)
            {
                XmlNode xNode = pointNode.SelectSingleNode("x");
                XmlNode yNode = pointNode.SelectSingleNode("y");

                if (xNode != null && yNode != null)
                {
                    Points.Add(new Vector2f(Single.Parse(xNode.InnerText), Single.Parse(yNode.InnerText)));
                }
            }
        }
        #endregion // XML Serialisation

        #region Public Methods
        /// <summary>
        /// Queries bugstar for a list of bugs that are within this map grid
        /// </summary>
        /// <returns></returns>
        public List<Bug> GetBugs()
        {
            return GetBugs("Id");
        }

        /// <summary>
        /// Queries bugstar for a list of bugs that are within this map grid returning the requested fields
        /// </summary>
        /// <param name="fields"></param>
        /// <returns></returns>
        public List<Bug> GetBugs(string fields)
        {
            List<Bug> bugs = new List<Bug>();

            // The resulting stream should contain xml, so convert it and extract the information we are after
            BugstarQueryBuilder qb = new BugstarQueryBuilder(this.Query);
            qb.RelativeQuery = Commands.BUGSTAR_BUGS;
            qb.Parameters.Add("fields", fields);
            BugstarQuery query = qb.ToQuery();

			//TODO: DHM FIX ME: should use RunQueryWithXDocumentResult!	
            using (Stream queryResult = Connection.RunQuery(query))
            {
                if (queryResult != null)
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(queryResult);

                    XmlNodeList bugNodes = doc.SelectNodes("/bugs/bug");
                    foreach (XmlNode bugNode in bugNodes)
                    {
                        Bug bug = new Bug(this.Map.Project);
                        bug.Deserialise(bugNode);
                        bugs.Add(bug);
                    }
                }
            }

            return bugs;
        }
        #endregion // Public Methods

        #region IComparable<MapGrid> Implementation
        public int CompareTo(MapGrid other)
        {
            // If other is not a valid object reference, this instance is greater.
            if (other == null)
            {
                return 1;
            }

            // Just compare the names
            return this.Name.CompareTo(other.Name);
        }
        #endregion // IComparable<MapGrid> Implementation
    } // MapGrid
} // RSG.Interop.Bugstar.Game
