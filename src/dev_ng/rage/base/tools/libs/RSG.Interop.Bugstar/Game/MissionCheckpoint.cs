﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using RSG.Base.Net;
using RSG.Interop.Bugstar.Common;

namespace RSG.Interop.Bugstar.Game
{
    /// <summary>
    /// 
    /// </summary>
    public class MissionCheckpoint : BugstarObject
    {
        #region Properties
        /// <summary>
        /// The query to use to query bugstar about this object
        /// </summary>
        public override IRestQuery Query
        {
            get
            {
                BugstarQueryBuilder qb = new BugstarQueryBuilder(this.Mission.Query);
                qb.RelativeQuery = String.Format("{0}/{1}", Commands.BUGSTAR_MISSION_CHECKPOINT, this.Id);
                return (qb.ToQuery());
            }
        }

        /// <summary>
        /// The mission this checkpoint is for.
        /// </summary>
        public Mission Mission { get; protected set; }

        /// <summary>
        /// Checkpoints name.
        /// </summary>
        public String Name { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public float ProjectedAttempts { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public float ProjectedAttemptsMin { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public float ProjectedAttemptsMax { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public uint Ordering { get; protected set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="missionNode"></param>
        public MissionCheckpoint(Mission mission)
            : base(mission.Connection)
        {
            Mission = mission;
        }
        #endregion // Constructor(s)

        #region XML Serialisation
        /// <summary>
        /// Generates an object from its XML representation.
        /// </summary>
        /// <param name="node"></param>
        public override void Deserialise(XmlNode node)
        {
            base.Deserialise(node);

            XmlNode nameNode = node.SelectSingleNode("checkpoint");
            if (nameNode != null)
            {
                Name = nameNode.InnerText;
            }

            XmlNode attemptsNode = node.SelectSingleNode("expectedAttempts");
            if (attemptsNode != null)
            {
                ProjectedAttempts = (uint)Single.Parse(attemptsNode.InnerText);
            }

            XmlNode attemptsMinNode = node.SelectSingleNode("minAttempts");
            if (attemptsMinNode != null)
            {
                ProjectedAttemptsMin = (uint)Single.Parse(attemptsMinNode.InnerText);
            }

            XmlNode attemptsMaxNode = node.SelectSingleNode("maxAttempts");
            if (attemptsMaxNode != null)
            {
                ProjectedAttemptsMax = (uint)Single.Parse(attemptsMaxNode.InnerText);
            }

            XmlNode orderingNode = node.SelectSingleNode("ordering");
            if (orderingNode != null)
            {
                Ordering = UInt32.Parse(orderingNode.InnerText);
            }

            // Patch up the projected attempt information if bugstar didn't provide us with any.
            if (ProjectedAttempts == 0)
            {
                ProjectedAttempts = 1;
            }
            if (ProjectedAttemptsMin == 0)
            {
                ProjectedAttemptsMin = Math.Max(1, ProjectedAttempts - 1);
            }
            if (ProjectedAttemptsMax == 0)
            {
                ProjectedAttemptsMax = ProjectedAttempts + 1;
            }
        }
        #endregion // XML Serialisation
    } // MissionCheckpoint
}
