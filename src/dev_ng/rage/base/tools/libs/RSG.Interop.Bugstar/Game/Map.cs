﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Media.Imaging;
using System.Xml;
using RSG.Base.Logging;
using RSG.Base.Math;
using RSG.Base.Net;
using RSG.Interop.Bugstar.Common;
using RSG.Interop.Bugstar.Organisation;

namespace RSG.Interop.Bugstar.Game
{
    /// <summary>
    /// 
    /// </summary>
    public class Map : BugstarObject
    {
        #region Constants
        private static readonly String LOG_CTX = "Bugstar:Map";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// The query to use to query bugstar about this object
        /// </summary>
        public override IRestQuery Query
        {
            get
            {
                BugstarQueryBuilder qb = new BugstarQueryBuilder(this.Project.Query);
                qb.RelativeQuery = String.Format("{0}/{1}", Commands.BUGSTAR_MAPS, this.Id);
                return (qb.ToQuery());
            }
        }

        /// <summary>
        /// The project this map is a part of
        /// </summary>
        public Project Project
        {
            get;
            protected set;
        }

        /// <summary>
        /// Map's name
        /// </summary>
        public string Name
        {
            get;
            protected set;
        }

        /// <summary>
        /// Lower left bounds of the map
        /// </summary>
        public Vector2f LowerLeft
        {
            get;
            protected set;
        }

        /// <summary>
        /// Upper right bounds of the map
        /// </summary>
        public Vector2f UpperRight
        {
            get;
            protected set;
        }

        /// <summary>
        /// Description of the map
        /// </summary>
        public string Description
        {
            get;
            protected set;
        }

        /// <summary>
        /// Indirectly queries bugstar to get this map's list of grids
        /// </summary>
        public MapGrid[] Grids
        {
            get
            {
                return GetGridList().ToArray();
            }
        }

        /// <summary>
        /// Background image to use for the map
        /// </summary>
        public BitmapImage Image
        {
            get
            {
                if (m_image == null && ImageAttachmentId != 0)
                {
                    m_image = GetBackgroundImage();
                }
                return m_image;
            }
        }
        private BitmapImage m_image;

        /// <summary>
        /// Image attachment id for the map's background image
        /// </summary>
        public uint ImageAttachmentId
        {
            get;
            protected set;
        }
        #endregion

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mapNode"></param>
        public Map(Project project)
            : base(project.Connection)
        {
            Project = project;
        }
        #endregion // Constructor(s)

        #region XML Serialisation
        /// <summary>
        /// Generates an object from its XML representation.
        /// </summary>
        /// <param name="node"></param>
        public override void Deserialise(XmlNode node)
        {
            base.Deserialise(node);

            XmlNode nameNode = node.SelectSingleNode("name");
            if (nameNode == null)
            {
                throw new ArgumentException(String.Format("Name node missing for map (id: {0}).", Id));
            }
            Name = nameNode.InnerText;

            XmlNode descriptionNode = node.SelectSingleNode("description");
            if (descriptionNode == null)
            {
                throw new ArgumentException(String.Format("Description node missing for map '{0}' (id: {1}).", Name, Id));
            }
            Description = descriptionNode.InnerText;

            // Bounds
            XmlNode lowerLeftXNode = node.SelectSingleNode("lowerLeft/x");
            if (lowerLeftXNode == null)
            {
                throw new ArgumentException(String.Format("Lower-left bound x coordinate node missing for map '{0}' (id: {1}).", Name, Id));
            }

            XmlNode lowerLeftYNode = node.SelectSingleNode("lowerLeft/y");
            if (lowerLeftYNode == null)
            {
                throw new ArgumentException(String.Format("Lower-left bound y coordinate node missing for map '{0}' (id: {1}).", Name, Id));
            }
            LowerLeft = new Vector2f(Single.Parse(lowerLeftXNode.InnerText), Single.Parse(lowerLeftYNode.InnerText));

            XmlNode upperRightXNode = node.SelectSingleNode("upperRight/x");
            if (upperRightXNode == null)
            {
                throw new ArgumentException(String.Format("Upper-right bound x coordinate node missing for map '{0}' (id: {1}).", Name, Id));
            }

            XmlNode upperRightYNode = node.SelectSingleNode("upperRight/y");
            if (upperRightYNode == null)
            {
                throw new ArgumentException(String.Format("Upper-right bound y coordinate node missing for map '{0}' (id: {1}).", Name, Id));
            }
            UpperRight = new Vector2f(Single.Parse(upperRightXNode.InnerText), Single.Parse(upperRightYNode.InnerText));

            XmlNode imageIdNode = node.SelectSingleNode("imageId");
            if (imageIdNode == null)
            {
                throw new ArgumentException(String.Format("Image ID node missing for map '{0}' (id: {1}).", Name, Id));
            }
            ImageAttachmentId = UInt32.Parse(imageIdNode.InnerText);
        }
        #endregion // XML Serialisation

        #region Static Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="project"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static Map GetMapById(Project project, uint id)
        {
            Map map = null;

            try
            {
                BugstarQueryBuilder qb = new BugstarQueryBuilder(project.Query);
                qb.RelativeQuery = String.Format("{0}/{1}", Commands.BUGSTAR_MAPS, id);
                BugstarQuery query = qb.ToQuery();

				//TODO: DHM FIX ME: should use RunQueryWithXDocumentResult!
                using (Stream queryResult = project.Connection.RunQuery(query))
                {
                    if (queryResult != null)
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.Load(queryResult);

                        XmlNode mapNode = doc.SelectSingleNode("/map");
                        if (mapNode != null)
                        {
                            map = new Map(project);
                            map.Deserialise(mapNode);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                BugstarConnection.Log.ToolExceptionCtx(LOG_CTX, ex, 
                    "Unhandled exception while getting information for map '{0}'.", id);
            }

            return map;
        }
        #endregion // Static Controller Methods

        #region Private Methods
        /// <summary>
        /// Queries bugstar for a list of grids associated with this map
        /// </summary>
        /// <returns></returns>
        private List<MapGrid> GetGridList()
        {
            List<MapGrid> grids = new List<MapGrid>();

            try
            {
                BugstarQueryBuilder qb = new BugstarQueryBuilder(this.Query);
                qb.RelativeQuery = Commands.BUGSTAR_GRIDS;
                BugstarQuery query = qb.ToQuery();

				//TODO: DHM FIX ME: should use RunQueryWithXDocumentResult!
                using (Stream queryResult = this.Connection.RunQuery(query))
                {
                    if (queryResult != null)
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.Load(queryResult);

                        XmlNodeList gridNodes = doc.SelectNodes("/grids/grid");
                        foreach (XmlNode gridNode in gridNodes)
                        {
                            MapGrid grid = new MapGrid(this);
                            grid.Deserialise(gridNode);

                            if (grid.Active)
                            {
                                grids.Add(grid);
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                BugstarConnection.Log.ToolExceptionCtx(LOG_CTX, ex, 
                    "Unhandled exception while getting the list of grids for map '{0}' (id: {1}).", 
                    this.Name, this.Id);
            }

            return grids;
        }

        /// <summary>
        /// First checks the cache to see if the image exists there, otherwise attempts to retrieve it from bugstar
        /// </summary>
        /// <returns></returns>
        private BitmapImage GetBackgroundImage()
        {
            BitmapImage result = null;

            // Get the attachment and check whether we were able to get a valid data stream
            Attachment imageAttachment = new Attachment(Project.Connection, ImageAttachmentId);
            Stream dataStream = imageAttachment.Download();
            try
            {
                if (dataStream != null)
                {
                    // Convert the stream to an image
                    BitmapImage source = new BitmapImage();
                    source.BeginInit();
                    source.StreamSource = dataStream;
                    source.EndInit();

                    int stride = 4 * source.PixelWidth;
                    byte[] pixels = new byte[stride * source.PixelHeight];
                    source.CopyPixels(pixels, stride, 0);

                    BitmapSource bitmapSource = BitmapSource.Create(source.PixelWidth, source.PixelHeight, 96.0, 96.0, source.Format, null, pixels, stride);

                    MemoryStream jpegStream = new MemoryStream();
                    JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                    encoder.Frames.Add(BitmapFrame.Create(bitmapSource));
                    encoder.Save(jpegStream);
                    jpegStream.Position = 0;

                    result = new BitmapImage();
                    result.BeginInit();
                    result.StreamSource = jpegStream;
                    result.EndInit();
                    result.Freeze();
                }
            }
            finally
            {
                dataStream.Close();
            }

            return result;
        }
        #endregion // Private Methods
    } // Map
} // RSG.Interop.Bugstar.Game
