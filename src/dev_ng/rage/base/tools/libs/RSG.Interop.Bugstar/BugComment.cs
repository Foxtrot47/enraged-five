﻿using System;
using System.Xml.Linq;
using RSG.Interop.Bugstar.Organisation;

namespace RSG.Interop.Bugstar
{

    /// <summary>
    /// Bugstar Bug comment object.
    /// </summary>
    public class BugComment
    {
        #region Properties
        /// <summary>
        /// The project this map is a part of
        /// </summary>
        public Project Project
        {
            get;
            protected set;
        }

        /// <summary>
        /// Text of comment.
        /// </summary>
        public String Text
        {
            get;
            private set;
        }

        /// <summary>
        /// Timestamp when comment was created.
        /// </summary>
        public DateTime CreatedAt
        {
            get { return m_createdAt; }
            private set { m_createdAt = value; }
        }
        private DateTime m_createdAt;

        /// <summary>
        /// User who created comment.
        /// </summary>
        public Actor Creator
        {
            get
            {
                if (null == m_creator && 0 != m_creatorId)
                {
                    m_creator = ActorFactory.CreateActorById(this.Project, m_creatorId);
                }
                return (m_creator);
            }
        }
        private Actor m_creator;
        private uint m_creatorId;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor; from XElement.
        /// </summary>
        /// <param name="xmlCommentElem"></param>
        public BugComment(Project project, XElement xmlCommentElem)
        {
            this.Project = project;
            this.Deserialise(xmlCommentElem);
        }

        /// <summary>
        /// Constructor; from string.
        /// </summary>
        /// <param name="project"></param>
        /// <param name="text"></param>
        internal BugComment(Project project, String text)
        {
            this.Project = project;
            this.CreatedAt = DateTime.UtcNow;
            this.Text = text;
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Serialise; to XElement.
        /// </summary>
        /// <returns></returns>
        public XElement Serialise()
        {
            XElement xmlCommentElem = new XElement("comment",
                new XElement("text", this.Text),
                new XElement("creator", this.m_creatorId),
                new XElement("createdAt", this.CreatedAt.ToString())
            ); // comment
            return (xmlCommentElem);
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Deserialise; from XElement.
        /// </summary>
        /// <param name="xmlCommentElem"></param>
        private void Deserialise(XElement xmlCommentElem)
        {
            XElement xmlTextElem = xmlCommentElem.Element("text");
            if (null != xmlTextElem)
                this.Text = xmlTextElem.Value;

            XElement xmlCreatorElem = xmlCommentElem.Element("creator");
            if (null != xmlCreatorElem)
                uint.TryParse(xmlCreatorElem.Value, out m_creatorId);

            XElement xmlCreatedAtElem = xmlCommentElem.Element("createdAt");
            if (null != xmlCreatedAtElem)
                DateTime.TryParse(xmlCreatedAtElem.Value, out m_createdAt);
        }
        #endregion // Private Methods
    }

} // RSG.Interop.Bugstar namespace
