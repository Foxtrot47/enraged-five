﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using RSG.Base.Net;
using RSG.Interop.Bugstar.Organisation;
using RSG.Interop.Bugstar.Common;

namespace RSG.Interop.Bugstar
{
    public static class BugstarHelper
    {
        #region Controller Methods
        /// <summary>
        /// Given an ID, return the contents of the bug
        /// </summary>
        /// <param name="bugId"></param>
        /// <returns></returns>
        public static Bug GetBug(BugstarConnection connection, Project project, uint bugId)
        {
            try
            {
                BugstarQueryBuilder qb = new BugstarQueryBuilder(connection.RESTService);
                qb.RelativeQuery = String.Format("{0}/{1}/{2}/{3}", Commands.BUGSTAR_PROJECTS, project.Id, Commands.BUGSTAR_BUGS, bugId);
                XDocument bugDoc = XDocument.Load(connection.RunQuery(qb.ToQuery()));
                XElement fullBug = bugDoc.Element("bug");
                return new Bug(project, fullBug);
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Checks if a bug is open, or in "Dev" state
        /// </summary>
        /// <param name="bugId"></param>
        /// <returns></returns>
        public static bool IsBugOpen(BugstarConnection connection, Project project, uint bugId)
        {
            Bug bug = GetBug(connection, project, bugId);
            if (bug != null && bug.State == BugState.Dev)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Searches through the bugstar database and returns a list of bugs which contain the specified text
        /// </summary>
        /// <param name="searchText"></param>
        /// <returns></returns>
        public static IEnumerable<uint> FindBugIdsWithText(BugstarConnection connection, Project project, String searchText)
        {
            try
            {
                Uri queryUri = new Uri(String.Format("{0}/rest/{1}/{2}/{3}",
                    connection.RESTService, Commands.BUGSTAR_PROJECTS, project.Id, Commands.BUGSTAR_SEARCH_MODE_FULLTEXT));
                String queryData = String.Format("<Search countOnly=\"false\" query=\"{0}\"/>", searchText);
                BugstarQuery query = new BugstarQuery(queryUri, RequestMethod.POST, queryData);

                IEnumerable<XElement> bugElems = connection.RunQueryWithXmlResult(query).Root.Elements("bug");

                List<uint> bugIds = new List<uint>();
                foreach (XElement bugElem in bugElems)
                {
                    uint bugId = uint.Parse(bugElem.Attribute("id").Value);
                    bugIds.Add(bugId);
                }
                return bugIds;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Edits the contents of a bug
        /// </summary>
        /// <param name="bugId"></param>
        /// <param name="field"></param>
        /// <param name="value"></param>
        /// <param name="append"></param>
        /// <returns></returns>
        public static bool EditBugData(BugstarConnection connection, Project project, uint bugId, string field, string value, bool append)
        {
            try
            {
                Bug bug = GetBug(connection, project, bugId);
                XElement bugElement = bug.Serialise().Root;
                if (append)
                    bugElement.Element(field).Value += value;
                else
                    bugElement.Element(field).Value = value;

                Uri queryUri = new Uri(String.Format("{0}/rest/{1}/{2}/{3}/{4}/",
                    connection.RESTService, Commands.BUGSTAR_PROJECTS, project.Id, Commands.BUGSTAR_BUGS, bugId));
                BugstarQuery query = new BugstarQuery(queryUri, RequestMethod.POST, bugElement.ToString(SaveOptions.DisableFormatting));
                connection.RunQueryWithXmlResult(query);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
        #endregion //Controller Methods
    }
}
