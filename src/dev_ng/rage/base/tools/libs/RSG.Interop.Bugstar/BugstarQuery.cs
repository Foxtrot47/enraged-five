﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Net;

namespace RSG.Interop.Bugstar
{

    /// <summary>
    /// Bugstar REST service query.
    /// </summary>
    public class BugstarQuery : IRestQuery
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="QueryUri"/> property.
        /// </summary>
        private readonly Uri _queryUri;

        /// <summary>
        /// Private field for the <see cref="Method"/> property.
        /// </summary>
        private readonly RequestMethod _method;

        /// <summary>
        /// Private field for the <see cref="ContentType"/> property.
        /// </summary>
        private readonly String _contentType;

        /// <summary>
        /// Private field for the <see cref="Data"/> property.
        /// </summary>
        private readonly String _data;
        #endregion

        #region Properties
        /// <summary>
        /// Uri for REST query.
        /// </summary>
        public Uri QueryUri
        {
            get { return _queryUri; }
        }

        /// <summary>
        /// REST HTTP method.
        /// </summary>
        public RequestMethod Method
        {
            get { return _method; }
        }

        /// <summary>
        /// Content-type for PUT/POST data.
        /// </summary>
        public String ContentType
        {
            get { return _contentType; }
        }

        /// <summary>
        /// REST POST/PUT data.
        /// </summary>
        public String Data
        {
            get { return _data; }
        }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// Constructor; specifying query Uri.
        /// </summary>
        /// <param name="config"></param>
        /// <param name="query"></param>
        public BugstarQuery(Uri query)
            : this(query, RequestMethod.GET)
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="config"></param>
        /// <param name="query"></param>
        /// <param name="method"></param>
        public BugstarQuery(Uri query, RequestMethod method)
            : this(query, method, String.Empty)
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="config"></param>
        /// <param name="query"></param>
        /// <param name="method"></param>
        /// <param name="data"></param>
        public BugstarQuery(Uri query, RequestMethod method, String data)
        {
            this._queryUri = query;
            this._contentType = "application/xml";
            this._method = method;
            this._data = data;
        }

        /// <summary>
        /// Constructs a new query based on a base query and the passed in relative query
        /// </summary>
        /// <param name="baseQuery"></param>
        /// <param name="relativeQuery"></param>
        public BugstarQuery(IRestQuery baseQuery, String relativeQuery)
            : this(new Uri(baseQuery.QueryUri, relativeQuery), RequestMethod.GET, String.Empty)
        {
        }
        #endregion // Constructor(s)
    } // BugstarQuery

} // RSG.Interop.Bugstar
