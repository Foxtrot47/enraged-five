﻿using System;
using RSG.Interop.Bugstar.Attributes;

namespace RSG.Interop.Bugstar
{

    /// <summary>
    /// Enumeration for Bug priority.
    /// </summary>
    public enum BugPriority
    {
        [BugstarData("1 - HIGH")]
        High,

        [BugstarData("2")]
        P2,

        [BugstarData("3")]
        P3,

        [BugstarData("4")]
        P4,

        [BugstarData("5 - LOW")]
        Low
    }

    /// <summary>
    /// BugPriority enum extension and utility methods.
    /// </summary>
    public static class BugPriorityUtils
    {
        /// <summary>
        /// Return BugPriority from Bugstar REST XML string.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static BugPriority FromBugstarString(String data)
        {
            BugPriority priority = BugstarDataAttributeExtensions.GetBugstarEnumFromString
                <BugPriority>(data, BugPriority.P3);
            return (priority);
        }

        /// <summary>
        /// Return Bugstar REST XML string from BugPriority.
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        public static String ToBugstarString(this BugPriority priority)
        {
            String priorityString = priority.GetBugstarData();
            return (priorityString);
        }
    }

} // RSG.Interop.Bugstar namespace
