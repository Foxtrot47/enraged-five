﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Interop.Bugstar
{
    /// <summary>
    /// BugBuilder exception class.
    /// </summary>
    public class BugBuilderException : Exception
    {
        #region Constructor(s)
        public BugBuilderException()
            : base()
        {
        }

        public BugBuilderException(String message)
            : base(message)
        {
        }

        public BugBuilderException(String message, Exception inner)
            : base(message, inner)
        {
        }

        public BugBuilderException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context)
            : base(info, context)
        {
        }
        #endregion // Constructor(s)
    }
}
