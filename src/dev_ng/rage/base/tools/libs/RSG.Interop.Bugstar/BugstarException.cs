﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Interop.Bugstar
{
    /// <summary>
    /// Class for wrapping any exceptions that the bugstar interop throw.
    /// </summary>
    public class BugstarException : Exception
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="BugstarException"/> class using
        /// the specified message.
        /// </summary>
        /// <param name="message"></param>
        public BugstarException(String message)
            : base(message)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="BugstarException"/> class using
        /// the specified message and inner exception.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="inner"></param>
        public BugstarException(String message, Exception inner)
            : base(message, inner)
        {
        }
        #endregion
    }
}
