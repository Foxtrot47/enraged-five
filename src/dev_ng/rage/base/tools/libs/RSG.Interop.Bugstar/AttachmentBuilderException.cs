﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Interop.Bugstar
{
    /// <summary>
    /// AttachmentBuilder exception class.
    /// </summary>
    public class AttachmentBuilderException : Exception
    {
        #region Constructor(s)
        public AttachmentBuilderException()
            : base()
        {
        }

        public AttachmentBuilderException(String message)
            : base(message)
        {
        }

        public AttachmentBuilderException(String message, Exception inner)
            : base(message, inner)
        {
        }

        public AttachmentBuilderException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context)
            : base(info, context)
        {
        }
        #endregion // Constructor(s)
    }
}
