﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using RSG.Base.Math;
using RSG.Base.Net;
using RSG.Interop.Bugstar.Common;
using RSG.Interop.Bugstar.Game;
using RSG.Interop.Bugstar.Organisation;

namespace RSG.Interop.Bugstar
{

    /// <summary>
    /// Bug object builder class.
    /// </summary>
    public sealed class BugBuilder
    {
        #region Properties
        /// <summary>
        /// The project this map is a part of
        /// </summary>
        public Project Project
        {
            get;
            private set;
        }

        /// <summary>
        /// Category of the bug (A, B, C, D, TODO, ...).
        /// </summary>
        public BugCategory Category
        {
            get;
            set;
        }

        /// <summary>
        /// Bug's summary
        /// </summary>
        public String Summary
        {
            get;
            set;
        }

        /// <summary>
        /// Extended bug description
        /// </summary>
        public String Description
        {
            get;
            set;
        }

        /// <summary>
        /// Id of the developer this bug is assigned to
        /// </summary>
        public uint DeveloperId
        {
            get;
            set;
        }

        /// <summary>
        /// Id of the tester this bug is assigned to
        /// </summary>
        public uint TesterId
        {
            get;
            set;
        }

        /// <summary>
        /// Id of the reviewer of this bug.
        /// </summary>
        public uint ReviewerId
        {
            get;
            set;
        }

        /// <summary>
        /// State of the bug (i.e. DEV, CLOSED_FIXED, etc...)
        /// </summary>
        public BugState State
        {
            get;
            set;
        }

        /// <summary>
        /// Priority of the bug
        /// </summary>
        public BugPriority Priority
        {
            get;
            set;
        }

        /// <summary>
        /// Location of the bug on the map
        /// </summary>
        public Vector3f Location
        {
            get;
            set;
        }

        /// <summary>
        /// Mission this bug is a part of
        /// </summary>
        public uint MissionId
        {
            get;
            set;
        }

        /// <summary>
        /// Component this bug is in
        /// </summary>
        public uint ComponentId
        {
            get;
            set;
        }

        /// <summary>
        /// When the bug is due by
        /// </summary>
        public DateTime DueDate
        {
            get;
            set;
        }

        /// <summary>
        /// The grid that the bug was reported.
        /// </summary>
        public uint Grid
        {
            get;
            set;
        }

        /// <summary>
        /// Collection of user Ids CC'd to bug.
        /// </summary>
        public ICollection<uint> CCList
        {
            get;
            set;
        }

        /// <summary>
        /// Collection of tags assigned to bug.
        /// </summary>
        public ICollection<String> Tags
        {
            get;
            private set;
        }

        /// <summary>
        /// Collection of attachments associated with bug.
        /// </summary>
        public ICollection<uint> Attachments
        {
            get;
            private set;
        }

        /// <summary>
        /// Collection of comments associated with bug.
        /// </summary>
        public ICollection<String> Comments
        {
            get;
            private set;
        }

        /// <summary>
        /// Estimated duration ( minutes ) 
        /// </summary>
        public float EstimatedTime
        {
            get;
            set;
        }


        // TO ADD:
        // collected on
        // fixed on
        // verified on
        // attempts
        // verifications
        // time spent
        // is working
        // is on hold
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// Constructor; specifying Bugstar project.
        /// </summary>
        /// <param name="project"></param>
        public BugBuilder(Project project)
        {
            this.Project = project;
            this.Summary = String.Empty;
            this.Description = String.Empty;
            this.Category = BugCategory.Todo;
            this.State = BugState.Dev;
            this.Priority = BugPriority.P3;
            this.Location = new Vector3f(0.0f, 0.0f, 0.0f);
            this.Tags = new List<String>();
            this.CCList = new List<uint>();
            this.Attachments = new List<uint>();
            this.Comments = new List<String>();
            this.EstimatedTime = RSG.Interop.Bugstar.Bug.ESTIMATED_TIME_DEFAULT;
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Return a concrete Bug object (commits bug to server).
        /// </summary>
        /// <param name="connection"></param>
        /// <returns></returns>
        public Bug ToBug(BugstarConnection connection)
        {
            try
            {
                // See https://rest.bugstar.rockstargames.com:8443/BugstarRestService-1.0/rest/Projects/1546/Bugs for the names of the elements the rest endpoint expects.
                // Create bug for serialisation.
                Bug bug = new Bug(this.Project, this.Category, this.Summary, 
                    this.Description, this.DeveloperId, this.TesterId, this.ReviewerId,
                    this.ComponentId, this.MissionId, this.Priority, this.Tags, 
                    this.CCList, this.Attachments, this.Comments, this.EstimatedTime);

                BugstarQueryBuilder queryBuilder = new BugstarQueryBuilder(this.Project.Query);
                queryBuilder.Method = RequestMethod.POST;
                queryBuilder.RelativeQuery = Commands.BUGSTAR_BUGS;

                XDocument xmlBugDoc = bug.Serialise();
                using (MemoryStream ms = new MemoryStream())
                {
                    xmlBugDoc.Save(ms);

                    String queryData = Encoding.UTF8.GetString(ms.ToArray());
                    queryBuilder.Data = queryData;
                    BugstarQuery query = queryBuilder.ToQuery();

                    using (Stream resultStream = connection.RunQuery(query))
                    {
                        if (null == resultStream)
                        {
                            BugstarConnection.Log.Error("Failed to read back bug data.");
                            return (null);
                        }

                        MemoryStream msResult = new MemoryStream();
                        Byte[] buffer = new Byte[2048];
                        int read;
                        while ((read = resultStream.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            msResult.Write(buffer, 0, read);
                        }
                        resultStream.Close();

                        // could also use
                        //resultStream.CopyTo(msResult);

                        msResult.Seek(0, SeekOrigin.Begin);
                        bug = new Bug(this.Project, msResult);

                        //TODO: DHM FIX ME: when url:bugstar:982433 is fixed... we can remove this step.
                        // Register the attachment(s) with the bug.
                        foreach (uint id in this.Attachments)
                            Bug.Attach(bug, new Attachment(bug.Connection, id));

                        msResult.Close();
                    }
                }
                return (bug);
            }
            catch (RestException ex)
            {
                throw (new BugBuilderException("Failed to create Bug, see InnerException.", ex));
            }
        }
        #endregion // Controller Methods
    }

} // RSG.Interop.Bugstar namespace
