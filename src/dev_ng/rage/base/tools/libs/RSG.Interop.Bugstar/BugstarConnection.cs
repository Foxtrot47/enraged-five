﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Xml.Linq;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.Net;
using RSG.Interop.Bugstar.Common;

namespace RSG.Interop.Bugstar
{

    /// <summary>
    /// Bugstar REST service connection.
    /// </summary>
    public class BugstarConnection : RestConnection
    {
        #region Constants
        private static readonly String LOG_CTX = "Bugstar REST";

        /// <summary>
        /// Buffer size for document reads.
        /// </summary>
        private static readonly int BUFFER_SIZE = 4096;
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Root REST service Uri.
        /// </summary>
        public Uri RESTService
        {
            get;
            private set;
        }

        /// <summary>
        /// Root Attachment service Uri.
        /// </summary>
        public Uri AttachmentService
        {
            get;
            private set;
        }

        /// <summary>
        /// Flag indicating whether the connection is a readonly connection
        /// </summary>
        public bool IsReadOnly
        {
            get;
            set;
        }

        /// <summary>
        /// Flag indicating whether the rest connection requires you to login
        /// </summary>
        protected override bool LoginRequired
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Query to use for performing the initial login
        /// </summary>
        protected override IRestQuery LoginQuery
        {
            get
            {
                BugstarQueryBuilder qb = new BugstarQueryBuilder(this.RESTService);
                qb.RelativeQuery = Commands.BUGSTAR_PROJECTS;
                return (qb.ToQuery());
            }
        }
        #endregion // Properties

        #region Static Properties
        /// <summary>
        /// Log object.
        /// </summary>
        internal static IUniversalLog Log
        {
            get;
            private set;
        }
        #endregion // Static Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="restService"></param>
        /// <param name="attachmentService"></param>
        public BugstarConnection(Uri restService, Uri attachmentService)
        {
            this.RESTService = restService;
            this.AttachmentService = attachmentService;
            this.IsReadOnly = false;
        }

        /// <summary>
        /// Static constructor; initialises our log.
        /// </summary>
        static BugstarConnection()
        {
            BugstarConnection.Log = LogFactory.CreateUniversalLog("RSG.Interop.Bugstar");
        }
        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// RunQuery override; for added logging.
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public override System.IO.Stream RunQuery(IRestQuery query)
        {
            BugstarConnection.Log.DebugCtx(LOG_CTX, "{0} [{1}]",
                query.QueryUri, query.Method);
            return (base.RunQuery(query));
        }

        /// <summary>
        /// RunQuery equivalent for returning XDocument.
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public XDocument RunQueryWithXmlResult(IRestQuery query)
        {
            using (Stream resultStream = this.RunQuery(query))
            {
                if (null == resultStream)
                {
                    BugstarConnection.Log.ErrorCtx(LOG_CTX, "Failed to read back XML data.");
                    return (null);
                }

                using (MemoryStream msResult = new MemoryStream())
                {
                    Byte[] documentBuffer = new Byte[BUFFER_SIZE];
                    int read, offset = 0;
                    while ((read = resultStream.Read(documentBuffer, 0, 
                        documentBuffer.Length)) > 0)
                    {
                        msResult.Write(documentBuffer, 0, read);
                        offset += read;
                    }

                    msResult.Seek(0, SeekOrigin.Begin);
                    XDocument xmlDoc = XDocument.Load(msResult);
                    return (xmlDoc);
                }
            }
        }
        #endregion // Public Methods
    } // BugstarConnection

} // RSG.Interop.Bugstar namespace
