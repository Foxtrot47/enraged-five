﻿using System;
using System.Reflection;
using RSG.Interop.Bugstar.Attributes;

namespace RSG.Interop.Bugstar
{

    /// <summary>
    /// Enumeration representing a Bug's state.
    /// </summary>
    /// Friendly-names are defined per-project and can be retrived through REST; see
    /// https://rest.bugstar.rockstargames.com:8443/BugstarRestService-1.0/rest/Projects/1546/States.
    /// 
    public enum BugState
    {
        Unknown,

        [BugstarData("DEV")]
        Dev,

        [BugstarData("BUILD_RESOLVED")]
        Resolved,

        [BugstarData("TEST_MORE_INFO")]
        Test_More_Info,

        [BugstarData("TEST_TO_VERIFY")]
        Test_Verify,

        [BugstarData("REVIEW_FIXED")]
        Review_Fixed,

        [BugstarData("REVIEW_WAIVED")]
        Review_Waived,

        [BugstarData("REVIEW_DUPLICATED")]
        Review_Duplicated,

        [BugstarData("REVIEW_NOT_SEEN")]
        Review_Not_Seen,

        [BugstarData("CLOSED_FIXED")]
        Closed_Fixed,

        [BugstarData("CLOSED_WAIVED")]
        Closed_Waived,

        [BugstarData("CLOSED_DUPLICATE")]
        Closed_Duplicate,

        [BugstarData("CLOSED_NOT_SEEN")]
        Closed_Not_Seen,

        [BugstarData("REVIEW_RESOLVED")]
        Review_Resolved,

        [BugstarData("PLANNED")]
        Planned,

        [BugstarData("TEST_NOT_SEEN")]
        Test_Not_Seen,

        Default = Dev,
    }
    
    /// <summary>
    /// BugState enum extension and utility methods.
    /// </summary>
    public static class BugStateUtils
    {
        /// <summary>
        /// Return BugState from Bugstar REST XML string.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static BugState FromBugstarString(String data)
        {
            BugState category = BugstarDataAttributeExtensions.GetBugstarEnumFromString
                <BugState>(data, BugState.Unknown);
            return (category);
        }

        /// <summary>
        /// Return Bugstar REST XML string from BugState.
        /// </summary>
        /// <param name="state"></param>
        /// <returns></returns>
        public static String ToBugstarString(this BugState state)
        {
            String stateString = state.GetBugstarData();
            return (stateString);
        }
    }

} // RSG.Interop.Bugstar namespace
