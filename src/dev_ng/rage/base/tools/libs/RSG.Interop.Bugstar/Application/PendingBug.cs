﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace RSG.Interop.Bugstar.Application
{
    /// <summary>
    /// 
    /// </summary>
    public class PendingBug
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="Category"/> property.
        /// </summary>
        private BugCategory _category = BugCategory.C;

        /// <summary>
        /// Private field for the <see cref="CCList"/> property.
        /// </summary>
        private readonly IList<string> _ccList = new List<string>();

        /// <summary>
        /// Private field for the <see cref="Component"/> property.
        /// </summary>
        private string _component;

        /// <summary>
        /// Private field for the <see cref="Description"/> property.
        /// </summary>
        private string _description;

        /// <summary>
        /// Private field for the <see cref="FileAttachments"/> property.
        /// </summary>
        private readonly IList<string> _fileAttachments = new List<string>();

        /// <summary>
        /// Private field for the <see cref="Owner"/> property.
        /// </summary>
        private string _owner;

        /// <summary>
        /// Private field for the <see cref="Project"/> property.
        /// </summary>
        private string _project;

        /// <summary>
        /// Private field for the <see cref="Reviewer"/> property.
        /// </summary>
        private string _reviewer;

        /// <summary>
        /// Private field for the <see cref="StreamAttachments"/> property.
        /// </summary>
        private readonly IList<KeyValuePair<string, Stream>> _streamAttachments =
            new List<KeyValuePair<string, Stream>>();

        /// <summary>
        /// Private field for the <see cref="Summary"/> property.
        /// </summary>
        private string _summary;

        /// <summary>
        /// Private field for the <see cref="Tags"/> property.
        /// </summary>
        private readonly IList<string> _tags = new List<string>();
        #endregion

        #region Properties
        /// <summary>
        /// Bug category/class.
        /// </summary>
        public BugCategory Category
        {
            get { return _category; }
            set { _category = value; }
        }

        /// <summary>
        /// List of cc's to add to this bug.
        /// </summary>
        public IList<string> CCList
        {
            get { return _ccList; }
        }

        /// <summary>
        /// Bug component.
        /// </summary>
        public string Component
        {
            get { return _component; }
            set { _component = value; }
        }

        /// <summary>
        /// Bug description.
        /// </summary>
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        /// <summary>
        /// File based attachments to add to the bug.
        /// </summary>
        public IList<string> FileAttachments
        {
            get { return _fileAttachments; }
        }

        /// <summary>
        /// Owner for the bug.
        /// </summary>
        public string Owner
        {
            get { return _owner; }
            set { _owner = value; }
        }

        /// <summary>
        /// Name of the bugstar project that this bug should be added under.
        /// </summary>
        public string Project
        {
            get { return _project; }
            set { _project = value; }
        }

        /// <summary>
        /// Reviewer for the bug.
        /// </summary>
        public string Reviewer
        {
            get { return _reviewer; }
            set { _reviewer = value; }
        }

        /// <summary>
        /// Stream based attachments to add to the bug.
        /// </summary>
        public IEnumerable<KeyValuePair<string, Stream>> StreamedAttachments
        {
            get { return _streamAttachments; }
        }

        /// <summary>
        /// Bug summary.
        /// </summary>
        public string Summary
        {
            get { return _summary; }
            set { _summary = value; }
        }

        /// <summary>
        /// Bug tags.
        /// </summary>
        public IList<string> Tags
        {
            get { return _tags; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Adds a new streamed attachment to the bug with the specified filename.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="stream"></param>
        public void AddStreamedAttachment(string filename, Stream stream)
        {
            _streamAttachments.Add(new KeyValuePair<string, Stream>(filename, stream));
        }
        #endregion
    }
}
