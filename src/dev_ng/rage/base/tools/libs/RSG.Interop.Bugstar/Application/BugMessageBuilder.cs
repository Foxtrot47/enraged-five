﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace RSG.Interop.Bugstar.Application
{
    /// <summary>
    /// The <see cref="BugMessageBuilder"/> class is in charge of creating a memory stream
    /// that is of the correct format to send to the bugstar application.
    /// </summary>
    public class BugMessageBuilder : IDisposable
    {
        #region Fields
        /// <summary>
        /// Payload data.
        /// </summary>
        private readonly MemoryStream _stream;

        /// <summary>
        /// The writer in charge of creating the memory stream.
        /// </summary>
        private readonly BinaryWriter _writer;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="BugMessageBuilder"/> class.
        /// </summary>
        public BugMessageBuilder()
        {
            _stream = new MemoryStream(1024);
            _writer = new BinaryWriter(_stream);

            // Reserve the space for the header at the beginning of the stream.
            AddHeader();
        }
        #endregion

        #region Methods
        /// <summary>
        /// Finalises the message flushing out the writer to the stream and setting the
        /// header's body length.
        /// </summary>
        /// <returns></returns>
        public Stream FinaliseMessage()
        {
            AddFooter();
            _writer.Flush();

            // 5 is the length of the header + footer.
            long bodyLength = _writer.BaseStream.Length - 5 *  sizeof(int);

            // Set the position of the stream to the location where the body length should be.
            _writer.BaseStream.Position = 3 * sizeof(int);
            _writer.Write((int)bodyLength);
            _writer.Flush();

            _stream.Position = 0;
            return _stream;
        }

        /// <summary>
        /// Adds a standard field with the given value.
        /// </summary>
        /// <param name="field"></param>
        /// <param name="value"></param>
        public void AddField(BugField field, string value)
        {
            // Don't add the field if the value is empty.
            if (String.IsNullOrEmpty(value))
            {
                return;
            }

            // 2.1 - int    : References a named Integer constant defining the type of bug field data the message describes. See the enum BugFields{} definition for more details.
            // 2.2 - int    : Length of the data content of the message (length of ref 2.3) 
            // 2.3 - byte[] : Data content of the message. Usually a byte array encoded string or binary.
            _writer.Write((int)field);

            // Note we can't use BinaryWriter's Write(string) overload due to it's length being output as an unsigned int.
            _writer.Write(value.Length);
            _writer.Write(System.Text.Encoding.UTF8.GetBytes(value));
        }

        /// <summary>
        /// Adds a custom field with the given name/value.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        public void AddCustomField(string name, string value)
        {
            // Don't add the field if the name or value is empty.
            if (String.IsNullOrEmpty(name) || String.IsNullOrEmpty(value))
            {
                return;
            }

            // 3.1 - int    : References a named Integer constant defining the type of bug field data the message describes. See the example Java code for examples of the constant names. For custom field messages this is always BugFields.CUSTOM_FIELDS. See the enum BugFields{} definition for more details.
            // 3.2 - int    : Length of the information fields (length of ref 3.3 + ref 3.4 + ref 3.5 + 3.6).
            // 3.3 - int    : Length of the name of the custom field (length of ref 3.4).
            // 3.4 - string : The name of the custom field. 
            // 3.5 - int    : Length of the data type of the custom field (length of ref 3.6).
            // 3.6 - string : Data type of the custom field. This is either ‘text’ or ‘image’.
            // 3.7 - int    : Length of the data content of the custom field message (length of ref 3.8).
            // 3.8 - byte[] : Data content of the message. 
            // 3.9 - int    : Error checking byte – should always be set to 0.
            string dataType = "text";
            int infoFieldLength = name.Length + dataType.Length + 2 * sizeof(int);

            _writer.Write((int)BugField.Custom);
            _writer.Write(infoFieldLength);

            _writer.Write(name.Length);
            _writer.Write(Encoding.UTF8.GetBytes(name));

            _writer.Write(dataType.Length);
            _writer.Write(Encoding.UTF8.GetBytes(dataType));

            _writer.Write(value.Length);
            _writer.Write(Encoding.UTF8.GetBytes(value));

            _writer.Write(0);
        }
        
        /// <summary>
        /// Adds a custom field with the given name/streamed value.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        public void AddCustomField(string name, Stream value)
        {
            // Don't add the field if the name or value is empty.
            if (String.IsNullOrEmpty(name) || value == null)
            {
                return;
            }

            // 3.1 - int    : References a named Integer constant defining the type of bug field data the message describes. See the example Java code for examples of the constant names. For custom field messages this is always BugFields.CUSTOM_FIELDS. See the enum BugFields{} definition for more details.
            // 3.2 - int    : Length of the information fields (length of ref 3.3 + ref 3.4 + ref 3.5 + 3.6).
            // 3.3 - int    : Length of the name of the custom field (length of ref 3.4).
            // 3.4 - string : The name of the custom field. 
            // 3.5 - int    : Length of the data type of the custom field (length of ref 3.6).
            // 3.6 - string : Data type of the custom field. This is either ‘text’ or ‘image’.
            // 3.7 - int    : Length of the data content of the custom field message (length of ref 3.8).
            // 3.8 - byte[] : Data content of the message. 
            // 3.9 - int    : Error checking byte – should always be set to 0.
            string dataType = "binary";
            int infoFieldLength = name.Length + dataType.Length + 2 * sizeof(int);

            _writer.Write((int)BugField.Custom);
            _writer.Write(infoFieldLength);

            _writer.Write(name.Length);
            _writer.Write(Encoding.UTF8.GetBytes(name));

            _writer.Write(dataType.Length);
            _writer.Write(Encoding.UTF8.GetBytes(dataType));

            _writer.Write((int)value.Length);
            _writer.Flush();
            value.CopyTo(_writer.BaseStream);

            _writer.Write(0);
        }

        /// <summary>
        /// Adds the header data.
        /// </summary>
        private void AddHeader()
        {
            // Header layout :
            // 1.1 - int : length of header (constant)
            // 1.2 - int : Message ID
            // 1.3 - int : Error checking (always 0)
            // 1.4 - int : length of the body message
            _writer.Write(4);
            _writer.Write(0);
            _writer.Write(0);
            _writer.Write(0);       // This will be overwritten when the stream is finalised.
        }

        /// <summary>
        /// Adds the footer data.
        /// </summary>
        private void AddFooter()
        {
            // Footer layout :
            // 4.1 - int : Error checking (always 0).
            _writer.Write(0);
        }
        #endregion

        #region IDisposable Implementation
        /// <summary>
        /// Disposes of the underlying memory stream.
        /// </summary>
        public void Dispose()
        {
            _stream.Close();
        }
        #endregion
    }
}
