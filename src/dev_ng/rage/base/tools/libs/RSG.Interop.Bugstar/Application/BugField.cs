﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Interop.Bugstar.Application
{
    /// <summary>
    /// Enumeration of the bug fields that the bugstar application interface supports.
    /// </summary>
    public enum BugField
    {
        AssignedTo,
        Id,
        Number,
        Reporter,
        Attempts,
        Verifications,
        Severity,
        Status,
        Creation,
        Summary,
        Product,
        Platform,
        Version,
        FixedInVersion,
        Component,
        Resolution,
        Description,
        Custom
    }
}
