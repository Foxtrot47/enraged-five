﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace RSG.Interop.Bugstar.Application
{
    /// <summary>
    /// Allows for communication with the bugstar application that is running on an users
    /// machine.
    /// </summary>
    public class BugstarApplication
    {
        #region Constants
        /// <summary>
        /// Default port to use to communicate with the bugstar application.
        /// </summary>
        private const int _defaultPort = 3490;
        #endregion

        #region Fields
        /// <summary>
        /// Endpoint to use to communication with the running bugstar application.
        /// </summary>
        private readonly IPEndPoint _endpoint;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="BugstarApplication"/> class which
        /// connects to the local machine using default port.
        /// </summary>
        public BugstarApplication()
            : this(IPAddress.Loopback, _defaultPort)
        {
        }

        /// <summary>
        /// Initialise a new instance of the <see cref="BugstarApplication"/> class using
        /// the specified address and port for communication with the application.
        /// </summary>
        /// <param name="address"></param>
        /// <param name="port"></param>
        public BugstarApplication(IPAddress address, int port)
        {
            _endpoint = new IPEndPoint(address, port);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Opens up the add new bug window in the bugstar application with the specified
        /// pending bug's fields prepopulated.
        /// </summary>
        /// <param name="bug"></param>
        public void CreateBug(PendingBug bug)
        {
            using (BugMessageBuilder builder = new BugMessageBuilder())
            {
                builder.AddField(BugField.Product, bug.Project);
                builder.AddCustomField("summary", bug.Summary);
                builder.AddCustomField("description", bug.Description);
                builder.AddCustomField("component", bug.Component);
                builder.AddCustomField("class", bug.Category.ToBugstarString());
                builder.AddCustomField("tags", String.Join(",", bug.Tags));

                builder.AddCustomField("owner", bug.Owner);
                builder.AddCustomField("reviewer", bug.Reviewer);
                builder.AddCustomField("ccs", String.Join(",", bug.CCList));

                foreach (string filepath in bug.FileAttachments)
                {
                    builder.AddCustomField("Filepath", filepath);
                }

                foreach (KeyValuePair<string, Stream> streamedAttachment in bug.StreamedAttachments)
                {
                    builder.AddCustomField(String.Format("Attachment_{0}", streamedAttachment.Key), streamedAttachment.Value);
                }

                TcpClient client = new TcpClient("127.0.0.1", 3490);

                Stream messageStream = builder.FinaliseMessage();
                messageStream.CopyTo(client.GetStream());
            }
        }

        /// <summary>
        /// Checks whether the bugstar application is currently running on the users
        /// machine.
        /// </summary>
        /// <returns></returns>
        public bool IsBugstarRunning()
        {
            if (_endpoint.Address != IPAddress.Loopback)
            {
                // In theory the Process.GetProcessesByName() accepts an ip for the
                // machine to check against, but I'm not sure whether there are some
                // security issues that would need to be worked around for it to work.
                // This isn't currently a required feature, so I'll leave it out for now.
                throw new NotSupportedException("Unable to query whether bugstar is running on a remote machine.");
            }

            Process[] bugstarProcess = Process.GetProcessesByName("Bugstar");
            Process[] bugstarDevProcess = Process.GetProcessesByName("Bugstar-dev");
            return bugstarProcess.Any() || bugstarDevProcess.Any();
        }
        #endregion
    }
}
