﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Xml.Linq;
using RSG.Base.IO;
using RSG.Interop.Bugstar.Common;
using RSG.Interop.Bugstar.Organisation;

namespace RSG.Interop.Bugstar
{

    /// <summary>
    /// Attachment object builder class.
    /// </summary>
    public sealed class AttachmentBuilder
    {
        #region Constants
        #endregion // Constants

        #region Properties
        /// <summary>
        /// The project this map is a part of
        /// </summary>
        public Project Project
        {
            get;
            private set;
        }

        /// <summary>
        /// Filename uploaded to attachment service as.
        /// </summary>
        public String UploadFilename
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor; specifying Bugstar project.
        /// </summary>
        public AttachmentBuilder(Project project)
        {
            this.Project = project;
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Return a concrete Attachment object (commits attachment to server).
        /// </summary>
        /// <param name="connection"></param>
        /// <returns></returns>
        public Attachment ToAttachment(BugstarConnection connection, String filename)
        {
            if (!File.Exists(filename))
                throw (new FileNotFoundException("Attachment failed: file not found.", filename));

            if (String.IsNullOrEmpty(this.UploadFilename))
                this.UploadFilename = Path.GetFileName(filename);

            using (FileStream fs = new FileStream(filename, FileMode.Open,
                FileAccess.Read))
            {
                return (ToAttachment(connection, fs));
            }
        }

        /// <summary>
        /// Return a concrete Attachment object (commits attachment to server).
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="stream"></param>
        /// <returns></returns>
        public Attachment ToAttachment(BugstarConnection connection, Stream stream)
        {
            Debug.Assert(!String.IsNullOrEmpty(this.UploadFilename),
                "UploadFilename is empty!");
            if (String.IsNullOrEmpty(this.UploadFilename))
                throw (new AttachmentBuilderException("UploadFilename is empty!"));

            Byte[] encodedData = Base64EncodeDecode.Encode(stream);
            String data = Encoding.UTF8.GetString(encodedData);
            XDocument xmlAttachmentPendingDoc = new XDocument(
                new XDeclaration("1.0", "UTF-8", "yes"),
                new XElement("attachment",
                    new XElement("filename", this.UploadFilename),
                    new XElement("content", data)
                ) // attachment
            ); // document

            BugstarQueryBuilder qb = new BugstarQueryBuilder(this.Project.Query);
            qb.RelativeQuery = Commands.BUGSTAR_CREATE_ATTACHMENT;
            qb.Method = Base.Net.RequestMethod.POST;
            using (MemoryStream ms = new MemoryStream())
            {
                xmlAttachmentPendingDoc.Save(ms);

                String queryData = Encoding.UTF8.GetString(ms.ToArray());
                qb.Data = queryData;

                BugstarQuery query = qb.ToQuery();
                XDocument xmlDoc = connection.RunQueryWithXmlResult(query);
                Attachment attachment = new Attachment(connection, xmlDoc);
                return (attachment);
            }
        }
        #endregion // Controller Methods
    }

} // RSG.Interop.Bugstar namespace
