﻿using System;
using System.Collections.Generic;
using System.IO;

namespace RSG.Interop.Bugstar
{

    /// <summary>
    /// Static class giving information about the Bugstar client.
    /// </summary>
    public sealed class Bugstar
    {
        #region Constants
        private static readonly String BUGSTAR_EXE = "bugstar.exe";
        #endregion // Constants

        #region Controller Methods
        /// <summary>
        /// Return Bugstar installation folder.
        /// </summary>
        /// <returns></returns>
        public static String GetInstallPath()
        {
            String folder = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86);
            return (Path.Combine(folder, "Bugstar"));
        }

        /// <summary>
        /// Return Bugstar executable path.
        /// </summary>
        /// <returns></returns>
        public static String GetExecutablePath()
        {
            String folder = GetInstallPath();
            return (Path.Combine(folder, BUGSTAR_EXE));
        }

        /// <summary>
        /// Return information on all running Bugstar instances.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<BugstarInstance> GetRunningInstances()
        {
            return (BugstarInstance.GetRunningInstances());
        }
        #endregion // Controller Methods
    }

} // RSG.Interop.Bugstar namespace
