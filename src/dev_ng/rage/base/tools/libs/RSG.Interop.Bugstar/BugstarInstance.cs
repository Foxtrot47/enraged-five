﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace RSG.Interop.Bugstar
{

    /// <summary>
    /// Bugstar instance class; representing running instance on the 
    /// local machine.
    /// </summary>
    public sealed class BugstarInstance : IComparable<BugstarInstance>
    {
        #region Constants
        /// <summary>
        /// Autodesk MotionBuilder process executable name.
        /// </summary>
        private const String BUGSTAR_PROCESS_EXE = "bugstar";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Bugstar instance process object.
        /// </summary>
        public Process Process
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor from local PID (process identifier).
        /// </summary>
        /// <param name="pid"></param>
        public BugstarInstance(int pid)
        {
            Process p = Process.GetProcessById(pid);
            if (BUGSTAR_PROCESS_EXE.Equals(p.MainModule.ModuleName))
                this.Process = p;
            else
                throw new NotSupportedException("Not valid Bugstar process module.");
        }

        /// <summary>
        /// Constructor from a Process object.
        /// </summary>
        /// <param name="p"></param>
        private BugstarInstance(Process p)
        {
            this.Process = p;
        }
        #endregion // Constructor(s)

        #region Object Overrides
        /// <summary>
        /// MotionBuilderInstance equality operator based on Process ID.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(Object obj)
        {
            if (!(obj is BugstarInstance))
                return (false);

            return ((obj as BugstarInstance).Process.Id.Equals(this.Process.Id));
        }

        /// <summary>
        /// MotionBuilderInstance get hash code override.
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return (this.Process.Id.GetHashCode());
        }
        #endregion // Object Overrides

        #region IComparable<BugstarInstance> Interface
        /// <summary>
        /// Compare against another BugstarInstance object.
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public int CompareTo(BugstarInstance o)
        {
            return (this.Process.Id.CompareTo(o.Process.Id));
        }
        #endregion // IComparable<BugstarInstance> Interface

        #region Static Controller Methods
        /// <summary>
        /// Return array of all running MotionBuilder instances.
        /// </summary>
        /// <returns></returns>
        public static BugstarInstance[] GetRunningInstances()
        {
            List<BugstarInstance> instances = new List<BugstarInstance>();
            Process[] processes = Process.GetProcessesByName(BUGSTAR_PROCESS_EXE);
            foreach (Process process in processes)
            {
                instances.Add(new BugstarInstance(process));
            }
            instances.Sort();
            return (instances.ToArray());
        }
        #endregion // Static Controller Methods
    }

} // RSG.Interop.Bugstar namespace
