﻿//---------------------------------------------------------------------------------------------
// <copyright file="ToolsConfig.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Configuration
{
    using System;
    using System.Diagnostics;
    using System.IO;

    /// <summary>
    /// Class for interacting with the .tools configuration files that specify project
    /// settings that were previously in environment variables.
    /// </summary>
    /// The .tools configuration files are very similar to P4CONFIG files (in Perforce)
    /// but that specify our critical tools settings (previously as Environment variables).
    /// 
    public sealed class ToolsConfig
    {
        #region Constants
        /// <summary>
        /// Tools configuration filename.
        /// </summary>
        private const String ToolsConfigFilename = ".tools";

        private const String ToolsRootKey = "RS_TOOLSROOT";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Absolute root path to tools distribution branch (i.e. RS_TOOLSROOT).
        /// </summary>
        public String ToolsRootDirectory
        {
            get;
            private set;
        }

        /// <summary>
        /// Tools binary directory.
        /// </summary>
        public String ToolsBinDirectory
        { 
            get;
            private set;
        }

        /// <summary>
        /// Tools configuration data absolute path.
        /// </summary>
        public String ToolsConfigDirectory
        {
            get;
            private set;
        }

        /// <summary>
        /// Tools log file directory absolute path.
        /// </summary>
        public String ToolsLogsDirectory
        {
            get;
            private set;
        }

        /// <summary>
        /// Tools temporary files absolute path.
        /// </summary>
        public String ToolsTempDirectory
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Create configuration object from current working directory.
        /// </summary>
        public ToolsConfig()
        {
            String workingDirectory = Directory.GetCurrentDirectory();
            Init(workingDirectory);
        }

        /// <summary>
        /// Create configuration object for a user-specified directory.
        /// </summary>
        /// <param name="workingDirectory"></param>
        public ToolsConfig(String workingDirectory)
        {
            Init(workingDirectory);
        }
        #endregion // Constructor(s)

        #region Private Methods
        /// <summary>
        /// Initialise settings; using workingDirectory.
        /// </summary>
        /// <param name="workingDirectory"></param>
        private void Init(String workingDirectory)
        {
            String configurationFilename = FindNearestToolsConfig(workingDirectory);
            if (!String.IsNullOrEmpty(configurationFilename))
            {
                InitFromFilename(configurationFilename);
            }
            else
            {
                InitFromEnvironment();
            }
        }

        /// <summary>
        /// Find the nearest tools configuration file from current directory.
        /// </summary>
        /// <param name="workingDirectory"></param>
        /// <returns></returns>
        private String FindNearestToolsConfig(String workingDirectory)
        {            
            String directory = Path.GetFullPath(workingDirectory);
            String configFilename = Path.Combine(directory, ToolsConfigFilename);
            if (File.Exists(configFilename))
            {
                // TOOLS file exists.
                return (configFilename);
            }
            else
            {
                String directoryRoot = Path.GetPathRoot(directory);
                String directoryUp = Path.GetDirectoryName(directory);
                if (directoryUp.Equals(directoryRoot))
                    return (null);
                else
                    return (FindNearestToolsConfig(directoryUp));
            }
        }

        /// <summary>
        /// Initialise our object from new tools configuration file.
        /// </summary>
        /// <param name="configurationFilename"></param>
        private void InitFromFilename(String configurationFilename)
        {
            using (FileStream stream = File.OpenRead(configurationFilename))
            using (StreamReader reader = new StreamReader(stream))
            {
                while (reader.Peek() >= 0)
                {
                    String line = reader.ReadLine();
                    String[] parts = line.Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries);
                    Debug.Assert(2 == parts.Length,
                        "Malformed TOOLS file line.  Ignoring.");
                    if (2 != parts.Length)
                        continue;

                    if (ToolsRootKey.Equals(parts[0]))
                        this.ToolsRootDirectory = parts[1];
                }
            }

            if (!String.IsNullOrEmpty(this.ToolsRootDirectory))
            {
                this.ToolsBinDirectory = Path.Combine(this.ToolsRootDirectory, "bin");
                this.ToolsConfigDirectory = Path.Combine(this.ToolsRootDirectory, "etc");
                this.ToolsTempDirectory = Path.Combine(this.ToolsRootDirectory, "tmp");
                this.ToolsLogsDirectory = Path.Combine(this.ToolsRootDirectory, "logs");
            }
        }

        /// <summary>
        /// Initialise our object from System Environment variables (backwards-compatibility).
        /// </summary>
        private void InitFromEnvironment()
        {
            // We only need to set our properties.
            this.ToolsRootDirectory = System.Environment.GetEnvironmentVariable("RS_TOOLSROOT");
            this.ToolsBinDirectory = Path.Combine(this.ToolsRootDirectory, "bin");
            this.ToolsConfigDirectory = Path.Combine(this.ToolsRootDirectory, "etc");
            this.ToolsTempDirectory = Path.Combine(this.ToolsRootDirectory, "tmp");
            this.ToolsLogsDirectory = Path.Combine(this.ToolsRootDirectory, "logs");
        }
        #endregion // Private Methods
    }

} // RSG.Configuration namespace
