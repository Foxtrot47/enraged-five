﻿//---------------------------------------------------------------------------------------------
// <copyright file="ConfigFactory.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2012-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Configuration
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.IO;
    using System.Xml.Linq;
    using System.Xml.XPath;
    using System.Text.RegularExpressions;
    using RSG.Base.Logging;
    using RSG.Configuration.Automation;
    using RSG.Configuration.Bugstar;
    using RSG.Configuration.Export;
    using RSG.Configuration.Export.Implementation;
    using RSG.Configuration.Implementation;
    using RSG.Configuration.Reports;
    using RSG.Configuration.ROS;
    using RSG.Base.Tasks;
    using RSG.Configuration.Ports;
    using RSG.Configuration.Rag;

    /// <summary>
    /// Factory class to create our various configuration objects.
    /// </summary>
    public static class ConfigFactory
    {
        #region Constants
        /// <summary>
        /// Log context string.
        /// </summary>
        private static readonly String LOG_CTX = "Log Factory";
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// Static constructor; used to register Exception class formatters.
        /// </summary>
        static ConfigFactory()
        {
            ExceptionFormatter.RegisterExceptionFormatter<ConfigurationException>(ConfigurationExceptionFormatter);
            ExceptionFormatter.RegisterExceptionFormatter<ConfigurationVersionException>(ConfigurationVersionExceptionFormatter);
        }
        #endregion // Constructor(s)

        #region Static Controller Methods
        /// <summary>
        /// Create core tools configuration data.
        /// </summary>
        /// <param name="log"></param>
        /// <returns></returns>
        public static IConfig CreateConfig(ILog log)
        {
            using (new ProfileContext(log, LOG_CTX, "Creating Configuration object."))
            {
                return (new Config(log));
            }
        }

        /// <summary>
        /// Create writable core tools/project configuration data.  ONLY TO BE
        /// USED BY THE INSTALLER.
        /// </summary>
        /// <param name="log"></param>
        /// <returns></returns>
        public static Installer.IWritableConfig CreateWritableConfig(ILog log)
        {
            using (new ProfileContext(log, LOG_CTX, "Creating *writable*-configuration object."))
            {
                return (new Config(log));
            }
        }

        /// <summary>
        /// Create project summary objects for core project and any content packs (DLC).
        /// </summary>
        /// <param name="log"></param>
        /// <returns></returns>
        public static IEnumerable<IProjectSummary> CreateProjectSummary(ILog log)
        {
            using (new ProfileContext(log, LOG_CTX, "Creating Project Summary objects."))
            {
                ToolsConfig toolsConfig = new ToolsConfig();
                String filename = Config.GetConfigFilename(toolsConfig.ToolsRootDirectory);
                String contentPackFilename = Config.GetContentPacksFilename(toolsConfig.ToolsRootDirectory);
                try
                {
                    List<IProjectSummary> projects = new List<IProjectSummary>();

                    // Core Project Summary.
                    if (File.Exists(filename))
                    {
                        XDocument xmlDoc = XDocument.Load(filename);
                        foreach (XElement xmlProjectElem in
                            xmlDoc.XPathSelectElements("/ConfigurationFile/Project"))
                        {
                            projects.Add(new ProjectSummaryCore(xmlProjectElem));
                        }
                    }
                    else
                    {
                        log.Warning("Core project configuration file missing: '{0}'.", filename);
                    }

                    // DLC / Extra Content Pack Summaries.
                    if (File.Exists(contentPackFilename))
                    {
                        XDocument xmlContentPackDoc = XDocument.Load(contentPackFilename);
                        foreach (XElement xmlProjectElem in
                            xmlContentPackDoc.XPathSelectElements("/ContentPacksFile/Packs/Item"))
                        {
                            projects.Add(new ProjectSummary(xmlProjectElem));
                        }
                    }

                    return (projects);
                }
                catch (Exception ex)
                {
                    throw new ConfigurationException("Failed to read project summary information.", ex)
                        {
                            Filename = filename
                        };
                }
            }
        }

        /// <summary>
        /// Create application settings configuration data (for project).
        /// </summary>
        /// <param name="log"></param>
        /// <param name="project"></param>
        /// <returns></returns>
        public static IDictionary<String, IApplicationSettings> CreateApplicationSettings(ILog log, IProject project)
        {
            using (new ProfileContext(log, LOG_CTX, "Creating application settings object: {0}.", project.FriendlyName))
            {
                IDictionary<String, IApplicationSettings> settings = LoadApplicationSettings(project);
                return (settings);
            }
        }

        /// <summary>
        /// Create export configuration data.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="project"></param>
        /// <returns></returns>
        public static IExportConfig CreateExportConfig(ILog log, IProject project, ExportConfigType types)
        {
            using (new ProfileContext(log, LOG_CTX, "Creating export config object: {0}.", types))
            {
                return (ExportConfig.Load(project, types));
            }
        }

        /// <summary>
        /// Create report configuration data.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="branch"></param>
        /// <returns></returns>
        public static IReportsConfig CreateReportConfig(ILog log, IBranch branch)
        {
            using (new ProfileContext(log, LOG_CTX, "Creating report config object: {0}.", 
                branch.Project.FriendlyName))
            {
                return (new ReportsConfig(branch));
            }
        }
        
        /// <summary>
        /// Create ROS (Rockstar Online Services) configuration data.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="project"></param>
        /// <returns></returns>
        public static IROSConfig CreateROSConfig(ILog log, IProject project)
        {
            using (new ProfileContext(log, LOG_CTX, "Creating ROS config object: {0}.", project.FriendlyName))
            {
                return (new ROSConfig(project));
            }
        }

        /// <summary>
        /// Create Automation Service configuration data for project.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="project"></param>
        /// <returns></returns>
        public static IAutomationServicesConfig CreateAutomationServicesConfig(ILog log, IProject project)
        {
            using (new ProfileContext(log, LOG_CTX,
                "Creating Automation Services Configuration data: {0}.", project.FriendlyName))
            {
                return (AutomationServicesConfig.Load(project));
            }
        }

        /// <summary>
        /// Create Bugstar configuration data.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="project"></param>
        /// <returns></returns>
        public static IBugstarConfig CreateBugstarConfig(ILog log, IProject project)
        {
            using (new ProfileContext(log, LOG_CTX, "Creating Bugstar config object: {0}.", project.FriendlyName))
            {
                return (new BugstarConfig(project));
            }
        }

        /// <summary>
        /// Create installer configuration data.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="project"></param>
        /// <returns></returns>
        public static Installer.IInstallerConfig CreateInstallerConfig(ILog log, IProject project)
        {
            using (new ProfileContext(log, LOG_CTX, "Creating Installer config object."))
            {
                return (new Installer.InstallerConfig(project));
            }
        }

        /// <summary>
        /// Creates port configuration data.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="project">Project to create the port configuration for.</param>
        /// <returns>Port configuration object.</returns>
        public static IPortConfig CreatePortConfig(ILog log, IProject project)
        {
            using (new ProfileContext(log, LOG_CTX, "Creating port config object: {0}.", 
                project.FriendlyName))
            {
                return (new PortConfig(log, project));
            }
        }

        /// <summary>
        /// Creates rag configuration data.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="project"></param>
        /// <returns></returns>
        public static IRagConfig CreateRagConfig(ILog log, IProject project)
        {
            using (new ProfileContext(log, LOG_CTX, "Creating rag config object: {0}.",
                project.FriendlyName))
            {
                return (new RagConfig(log, project));
            }
        }

        /// <summary>
        /// Create environment object.
        /// </summary>
        /// <returns></returns>
        public static IEnvironment CreateEnvironment()
        {
            return (new Implementation.Environment());
        }
        #endregion // Static Controller Methods

        #region Private Methods
        /// <summary>
        /// Load application settings for a project.
        /// </summary>
        /// <param name="project"></param>
        private static IDictionary<String, IApplicationSettings> LoadApplicationSettings(IProject project)
        {
            String filename = Path.Combine(project.Config.ToolsConfigDirectory, "applications.xml");
            IDictionary<String, IApplicationSettings> settings = new Dictionary<String, IApplicationSettings>();
            if (!File.Exists(filename))
            {
                return (settings);
            }

            XDocument xmlDoc = XDocument.Load(filename);
            IEnumerable<XElement> xmlApplicationElems = xmlDoc.XPathSelectElements("/Applications/Application");
            foreach (XElement xmlApplicationElem in xmlApplicationElems)
            {
                IApplicationSettings appSettings = new ApplicationSettings(xmlApplicationElem);
                settings.Add(appSettings.Name, appSettings);
            }
            return (settings);
        }
        #endregion // Private Methods

        #region Exception Formatters
        /// <summary>
        /// Custom formatter for configuration exceptions.
        /// </summary>
        /// <param name="ex">Exception to format.</param>
        /// <returns></returns>
        private static IEnumerable<String> ConfigurationExceptionFormatter(Exception ex)
        {
            ConfigurationException configException = ex as ConfigurationException;
            if (configException == null)
            {
                Debug.Fail("Invalid ConfigurationException.");
                throw new ArgumentException("ex");
            }

            ICollection<String> information = new List<String>();
            if (!String.IsNullOrEmpty(configException.Filename))
            {
                information.Add(String.Format("Filename: '{0}'.", configException.Filename));
            }
            return information;
        }

        /// <summary>
        /// Custom formatter for configuration exceptions.
        /// </summary>
        /// <param name="ex">Exception to format.</param>
        /// <returns></returns>
        private static IEnumerable<String> ConfigurationVersionExceptionFormatter(Exception ex)
        {
            ConfigurationVersionException versionException = ex as ConfigurationVersionException;
            if (versionException == null)
            {
                Debug.Fail("Invalid ConfigurationVersionException.");
                throw new ArgumentException("ex");
            }

            ICollection<String> information = new List<String>();
            if (!String.IsNullOrEmpty(versionException.Filename))
            {
                information.Add(String.Format("Filename: '{0}'.", versionException.Filename));
            }
            information.Add(String.Format("ExpectedVersion: '{0}'.", versionException.ExpectedVersion));
            information.Add(String.Format("ActualVersion: '{0}'.", versionException.ActualVersion));
            return information;
        }
        #endregion // Exception Formatters
    } // ConfigFactory

} // RSG.Configuration namespace
