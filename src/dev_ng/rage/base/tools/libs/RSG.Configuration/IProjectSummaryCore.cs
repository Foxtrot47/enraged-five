﻿//---------------------------------------------------------------------------------------------
// <copyright file="IProjectSummaryCore.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2012-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Configuration
{
    using System;

    /// <summary>
    /// Project summary interface; for reading summary information about
    /// available projects when the configuration data may not be available.
    /// </summary>
    public interface IProjectSummaryCore : IProjectSummary
    {
        /// <summary>
        /// Project DLC pack root-directory (absolute path).
        /// </summary>
        String RootDirectoryDLC { get; }
    }

} // RSG.Configuration namespace
