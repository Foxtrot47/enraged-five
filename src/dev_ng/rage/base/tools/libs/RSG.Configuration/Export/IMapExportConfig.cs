﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Configuration.Export
{

    /// <summary>
    /// Map Export configuration data.
    /// </summary>
    public interface IMapExportConfig : IExporterConfig
    {
        #region Properties
        /// <summary>
        /// Perforce auto-submission flag.
        /// </summary>
        bool AutoSubmit { get; }

        /// <summary>
        /// Map Export Report Drawable Limit (count).
        /// </summary>
        int ReportDrawableLimit { get; }

        /// <summary>
        /// Map Export Report TXD Limit (count).
        /// </summary>
        int ReportTXDLimit { get; }

        /// <summary>
        /// Map Export Report Bound Drawable Distance Ratio.
        /// </summary>
        int ReportBoundDrawableDistanceRatio { get; }
        #endregion // Properties
    }

} // RSG.Configuration.Export namespace
