﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Configuration.Export
{

    /// <summary>
    /// Character Export configuration data.
    /// </summary>
    public interface ICharacterExportConfig : IExporterConfig
    {
    }

} // RSG.Configuration.Export namespace
