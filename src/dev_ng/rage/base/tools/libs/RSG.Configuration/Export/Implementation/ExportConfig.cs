﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Configuration.Export;

namespace RSG.Configuration.Export.Implementation
{

    /// <summary>
    /// Export configuration object.
    /// </summary>
    internal class ExportConfig : IExportConfig
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="Filename"/> property.
        /// </summary>
        private readonly String _filename;
        #endregion // Fields

        #region Properties
        /// <summary>
        /// Associated project.
        /// </summary>
        public IProject Project { get; private set; }

        /// <summary>
        /// Loaded configuration types.
        /// </summary>
        public ExportConfigType Types { get; private set; }

        /// <summary>
        /// In-Game Animation configuration object.
        /// </summary>
        public IAnimationExportConfig Animation { get; private set; }

        /// <summary>
        /// Character export configuration object.
        /// </summary>
        public ICharacterExportConfig Character { get; private set; }

        /// <summary>
        /// Cutscene export configuration object.
        /// </summary>
        public ICutsceneExportConfig Cutscene { get; private set; }

        /// <summary>
        /// Map export configuration object.
        /// </summary>
        public IMapExportConfig Map { get; private set; }

        /// <summary>
        /// Vehicle export configuration object.
        /// </summary>
        public IVehicleExportConfig Vehicle { get; private set; }

        /// <summary>
        /// Weapon export configuration object.
        /// </summary>
        public IWeaponExportConfig Weapon { get; private set; }

        /// <summary>
        /// Name of the file that this config data is being read from.
        /// </summary>
        public String Filename
        {
            get { return _filename; }
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="project"></param>
        /// <param name="types"></param>
        private ExportConfig(IProject project, ExportConfigType types)
        {
            this.Project = project;
            this.Types = types;
            _filename = Path.Combine(project.Config.ToolsConfigDirectory, "export.xml");
            Reload();
        }
        #endregion // Constructor(s)

        #region Static Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="project"></param>
        /// <param name="types"></param>
        /// <returns></returns>
        public static IExportConfig Load(IProject project, ExportConfigType types)
        {
            IExportConfig exportConfig = new ExportConfig(project, types);
            return (exportConfig);
        }
        #endregion // Static Controller Methods

        #region Private Methods
        /// <summary>
        /// Reload the export configuration data.
        /// </summary>
        private void Reload()
        {
            if (!File.Exists(Filename))
            {
                Debug.Fail(String.Format("Export XML '{0}' does not exist.", Filename));
                throw new ConfigurationException("Export XML does not exist.")
                    {
                        Filename = this.Filename
                    };
            }

            XDocument xmlDoc = XDocument.Load(Filename);
            if (this.Types.HasFlag(ExportConfigType.Animation))
                this.Animation = AnimationExportConfig.Load(xmlDoc);
            if (this.Types.HasFlag(ExportConfigType.Character))
                this.Character = CharacterExportConfig.Load(xmlDoc);
            if (this.Types.HasFlag(ExportConfigType.Cutscene))
                this.Cutscene = CutsceneExportConfig.Load(xmlDoc);
            if (this.Types.HasFlag(ExportConfigType.Map))
                this.Map = MapExportConfig.Load(xmlDoc);
            if (this.Types.HasFlag(ExportConfigType.Vehicle))
                this.Vehicle = VehicleExportConfig.Load(xmlDoc);
            if (this.Types.HasFlag(ExportConfigType.Weapon))
                this.Weapon = WeaponExportConfig.Load(xmlDoc);
        }
        #endregion // Private Methods
    }

} // RSG.Configuration.Export.Implementation namespace
