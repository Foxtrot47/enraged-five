﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Configuration.Export
{

    /// <summary>
    /// Weapon Export configuration data.
    /// </summary>
    public interface IWeaponExportConfig : IExporterConfig
    {
    }

} // RSG.Configuration.Export namespace

