﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Xml.Linq;

namespace RSG.Configuration.Export.Implementation
{

    /// <summary>
    /// 
    /// </summary>
    internal abstract class BaseExporterConfig : 
        IExporterConfig
    {
        #region Member Data
        /// <summary>
        /// User-defined options for processor; processor-specific key, value
        /// pairs.  Allow injection of processor options from content-tree.
        /// </summary>
        protected IDictionary<String, Object> m_Parameters;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public BaseExporterConfig(XElement xmlParamsElem)
        {
            this.m_Parameters = new Dictionary<String, Object>();
            if (null != xmlParamsElem)
                RSG.Base.Xml.Parameters.Load(xmlParamsElem, ref m_Parameters);
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Return generic parameter.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public virtual Object GetParameter(String name, Object defaultValue)
        {
            Debug.Assert(this.m_Parameters.ContainsKey(name),
                String.Format("Parameter '{0}' not found.", name));
            if (!this.m_Parameters.ContainsKey(name))
                return (defaultValue);

            return (this.m_Parameters[name]);
        }
        #endregion // Controller Methods
    }

} // RSG.Configuration.Export.Implementation
