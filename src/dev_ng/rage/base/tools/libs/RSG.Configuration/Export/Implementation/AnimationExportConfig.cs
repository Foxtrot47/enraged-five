﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;

namespace RSG.Configuration.Export.Implementation
{

    /// <summary>
    /// 
    /// </summary>
    internal class AnimationExportConfig : 
        BaseExporterConfig, 
        IAnimationExportConfig
    {
        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="xmlParamsElem"></param>
        private AnimationExportConfig(XElement xmlParamsElem)
            : base(xmlParamsElem)
        {
        }
        #endregion // Constructor(s)

        #region Static Controller Methods
        /// <summary>
        /// Load configuration data from XML document.
        /// </summary>
        /// <param name="xmlDoc"></param>
        public static IAnimationExportConfig Load(XDocument xmlDoc)
        {
            XElement xmlParamsElem = xmlDoc.XPathSelectElement("/export/Animation/Parameters");
            IAnimationExportConfig config = new AnimationExportConfig(xmlParamsElem);
            return (config);
        }
        #endregion // Static Controller Methods
    }

} // RSG.Configuration.Export.Implementation namespace
