﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Configuration.Export
{

    /// <summary>
    /// Export configuration interface.
    /// </summary>
    public interface IExportConfig
    {
        #region Properties
        /// <summary>
        /// Associated project.
        /// </summary>
        IProject Project { get; }

        /// <summary>
        /// Loaded configuration types.
        /// </summary>
        ExportConfigType Types { get; }

        /// <summary>
        /// In-Game Animation configuration object.
        /// </summary>
        IAnimationExportConfig Animation { get; }

        /// <summary>
        /// Character export configuration object.
        /// </summary>
        ICharacterExportConfig Character { get; }

        /// <summary>
        /// Cutscene export configuration object.
        /// </summary>
        ICutsceneExportConfig Cutscene { get; }

        /// <summary>
        /// Map export configuration object.
        /// </summary>
        IMapExportConfig Map { get; }

        /// <summary>
        /// Vehicle export configuration object.
        /// </summary>
        IVehicleExportConfig Vehicle { get; }

        /// <summary>
        /// Weapon export configuration object.
        /// </summary>
        IWeaponExportConfig Weapon { get; }
        #endregion // Properties
    }

} // RSG.Configuration.Export namespace
