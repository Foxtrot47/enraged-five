﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Configuration.Export
{

    /// <summary>
    /// Exporter configuration data interface (for generic parameters).
    /// </summary>
    public interface IExporterConfig
    {
        #region Methods
        /// <summary>
        /// Return generic parameter.
        /// </summary>
        /// <param name="name">Parameter key name.</param>
        /// <param name="defaultValue"></param>
        /// <returns>Parameter value.</returns>
        Object GetParameter(String name, Object defaultValue);
        #endregion // Methods
    }

} // RSG.Configuration.Export namespace
