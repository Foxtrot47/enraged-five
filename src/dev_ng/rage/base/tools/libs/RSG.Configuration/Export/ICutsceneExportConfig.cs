﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Configuration.Export
{
    
    /// <summary>
    /// Cutscene Export configuration data.
    /// </summary>
    public interface ICutsceneExportConfig : IExporterConfig
    {
    }

} // RSG.Configuration.Export namespace
