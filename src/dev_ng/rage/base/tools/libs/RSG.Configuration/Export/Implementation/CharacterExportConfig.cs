﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;

namespace RSG.Configuration.Export.Implementation
{

    /// <summary>
    /// 
    /// </summary>
    internal class CharacterExportConfig :
        BaseExporterConfig,
        ICharacterExportConfig
    {
        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="xmlParamsElem"></param>
        private CharacterExportConfig(XElement xmlParamsElem)
            : base(xmlParamsElem)
        {
        }
        #endregion // Constructor(s)

        #region Static Controller Methods
        /// <summary>
        /// Load configuration data from XML document.
        /// </summary>
        /// <param name="xmlDoc"></param>
        public static ICharacterExportConfig Load(XDocument xmlDoc)
        {
            XElement xmlParamsElem = xmlDoc.XPathSelectElement("/export/Character/Parameters");
            ICharacterExportConfig config = new CharacterExportConfig(xmlParamsElem);
            return (config);
        }
        #endregion // Static Controller Methods
    }

} // RSG.Configuration.Export.Implementation namespace
