﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Configuration.Ports
{
    /// <summary>
    /// Port configuration interface.
    /// </summary>
    public interface IPortConfig
    {
        #region Properties
        /// <summary>
        /// Port configuration XML file.
        /// </summary>
        String Filename { get; }

        /// <summary>
        /// Mapping of identifiers to ports.
        /// </summary>
        /// <todo>Change this to an IReadOnlyDictionary when we upgrade to 4.5.</todo>
        IDictionary<String, int> Ports { get; }
        #endregion // Properties
    } // IPortConfig
}
