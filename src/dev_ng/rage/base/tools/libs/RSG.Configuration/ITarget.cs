﻿//---------------------------------------------------------------------------------------------
// <copyright file="ITarget.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2012-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Configuration
{
    using System;

    /// <summary>
    /// Target abstraction; representing a target platform defining the 
    /// architecture and build directory path.
    /// </summary>
    public interface ITarget :
        ICanImportEnvironment,
        IHasEnvironment
    {
        #region Properties
        /// <summary>
        /// Associated Branch object.
        /// </summary>
        IBranch Branch { get; }

        /// <summary>
        /// Target builds enabled locally?
        /// </summary>
        bool Enabled { get; set; }

        /// <summary>
        /// Associated platform key.
        /// </summary>
        RSG.Platform.Platform Platform { get; }

        /// <summary>
        /// Path to platform build files.
        /// </summary>
        String ResourcePath { get; }
        
        /// <summary>
        /// Path for target's shaders. 
        /// </summary>
        String ShaderPath { get; }

        /// <summary>
        /// Shader file extension.
        /// </summary>
        String ShaderExtension { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Return Ragebuilder absolute executable path for a particular target.
        /// </summary>
        /// <returns></returns>
        String GetRagebuilderExecutable();

        /// <summary>
        /// Return Ragebuilder XGE conversion arguments for a particular target.
        /// </summary>
        /// <returns></returns>
        String GetRagebuilderConvertXGEArguments();

        /// <summary>
        /// Return Ragebuilder XGE conversion script argument.
        /// </summary>
        /// <returns></returns>
        String GetRagebuilderConvertXGEScript();

        /// <summary>
        /// Return Ragebuilder local conversion arguments for a particular target.
        /// </summary>
        /// <returns></returns>
        String GetRagebuilderConvertLocalArguments();
        #endregion // Methods
    }

} // RSG.Configuration namespace
