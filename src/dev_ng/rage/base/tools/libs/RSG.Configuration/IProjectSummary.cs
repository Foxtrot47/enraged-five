﻿//---------------------------------------------------------------------------------------------
// <copyright file="IProjectSummary.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2012-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Configuration
{
    using System;

    /// <summary>
    /// Project summary interface; for reading summary information about
    /// available projects when the configuration data may not be available.
    /// </summary>
    public interface IProjectSummary
    {
        /// <summary>
        /// Project name.
        /// </summary>
        String Key { get; }

        /// <summary>
        /// Project friendly UI name.
        /// </summary>
        String FriendlyName { get; }

        /// <summary>
        /// Project root-directory (absolute path).
        /// </summary>
        String RootDirectory { get; }

        /// <summary>
        /// Project configuration filename.
        /// </summary>
        String ConfigurationFilename { get; }
    }

} // RSG.Configuration namespace
