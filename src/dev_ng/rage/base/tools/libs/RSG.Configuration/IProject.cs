﻿//---------------------------------------------------------------------------------------------
// <copyright file="IProjectCore.cs" company="Rockstar">
//     Copyright © Rockstar Games 2011-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Configuration
{
    using System;
    using System.Collections.Generic;
    using RSG.Base.Logging;
    using RSG.Platform;
    
    /// <summary>
    /// Project encapsulation; representing a project defining a set of
    /// branches and targets.
    /// </summary>
    public interface IProject :
        ICanImportEnvironment,
        IHasEnvironment
    {
        #region Properties
        /// <summary>
        /// Project name (unique key).
        /// </summary>
        String Name { get; }

        /// <summary>
        /// Project friendly UI name.
        /// </summary>
        String FriendlyName { get; }

        /// <summary>
        /// Project root directory $(root).
        /// </summary>
        String RootDirectory { get; }

        /// <summary>
        /// Project configuration filename.
        /// </summary>
        String Filename { get; }

        /// <summary>
        /// Project cache directory $(cache).
        /// </summary>
        String CacheDirectory { get; }

        /// <summary>
        /// Whether this project has levels.
        /// </summary>
        bool HasLevels { get; }

        /// <summary>
        /// Config object parent.
        /// </summary>
        IConfig Config { get; }

        /// <summary>
        /// Force flags collection.
        /// </summary>
        IForceFlags ForceFlags { get; }

        /// <summary>
        /// Asset prefixes collection.
        /// </summary>
        IAssetPrefix AssetPrefix { get; }

        /// <summary>
        /// Enumerable of Tools-associated mail addresses (for sending logs and bug reports).
        /// </summary>
        IEnumerable<System.Net.Mail.MailAddress> ToolsEmailAddresses { get; }

        /// <summary>
        /// Collection of branches for this project.
        /// </summary>
        IDictionary<String, IBranch> Branches { get; }

        /// <summary>
        /// Dictionary of available labels for this project.
        /// </summary>
        IDictionary<Label, String> Labels { get; }

        /// <summary>
        /// Collection of default platforms, these can be used when the current default branch
        /// has no active targets in it.
        /// </summary>
        IDictionary<Platform, bool> DefaultPlatforms { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Reload all of the configuration data.
        /// </summary>
        void Reload(ILog log);
        #endregion // Methods
    }

} // RSG.Configuration namespace
