﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Configuration.Automation
{

    /// <summary>
    /// Automation Service Console configuration data.
    /// </summary>
    public interface IAutomationConsoleConfig
    {
        /// <summary>
        /// Automation Console argument string.
        /// </summary>
        String Arguments { get; }
    }

} // RSG.Configuration.Automation namespace
