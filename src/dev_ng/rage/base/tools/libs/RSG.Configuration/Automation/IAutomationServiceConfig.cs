﻿using System;

namespace RSG.Configuration.Automation
{

    /// <summary>
    /// Automation Service Configuration interface.
    /// </summary>
    public interface IAutomationServiceConfig
    {
        #region Properties
        /// <summary>
        /// Service type name.
        /// </summary>
        String ServiceType
        {
            get;
        }

        /// <summary>
        /// Service studio location.
        /// </summary>
        IStudio Studio
        {
            get;
        }

        /// <summary>
        /// Service friendly-name.
        /// </summary>
        String FriendlyName
        {
            get;
        }

        /// <summary>
        /// Whether the service is visible to users.
        /// </summary>
        bool Visible
        {
            get;
        }

        /// <summary>
        /// Server Host to connect.
        /// </summary>
        Uri ServerHost
        {
            get;
        }

        /// <summary>
        /// Http Host to connect.
        /// </summary>
        Uri WebHost
        {
            get;
        }

        /// <summary>
        /// Https Host to connect.
        /// </summary>
        Uri WebHostSSL
        {
            get;
        }

		/// <summary>
		/// Web Alias for Portal.
        /// </summary>
        String WebAlias
        {
            get;
        }
		
		/// <summary>
        /// View Mode 
        /// </summary>
        ViewMode ViewMode
        {
            get;
        }

        /// <summary>
        /// Job Property For Filtering.
        /// </summary>
        String JobFilterKey
        {
            get;
        }

        /// <summary>
        /// Job Property Value For Filtering
        /// </summary>
        String JobFilterValue
        {
            get;
        }

        /// <summary>
        /// Automation Console configuration data.
        /// </summary>
        IAutomationConsoleConfig Console
        {
            get;
        }
        #endregion // Properties
    }

} // RSG.Configuration.Automation namespace
