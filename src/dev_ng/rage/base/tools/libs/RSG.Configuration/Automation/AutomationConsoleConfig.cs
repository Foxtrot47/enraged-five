﻿using System;
using System.Linq;
using System.Xml.Linq;

namespace RSG.Configuration.Automation
{

    /// <summary>
    /// Automation Service Console configuration data.
    /// </summary>
    internal class AutomationConsoleConfig : IAutomationConsoleConfig
    {
        #region Properties
        /// <summary>
        /// Console arguments.
        /// </summary>
        public String Arguments
        {
            get;
            private set;
        }
        #endregion // Properties
        
        #region Static Controller Methods
        /// <summary>
        /// Load Console configuration data from XML element.
        /// </summary>
        /// <param name="xmlConsoleElem"></param>
        /// <returns></returns>
        public static IAutomationConsoleConfig Load(XElement xmlConsoleElem)
        {
            AutomationConsoleConfig config = new AutomationConsoleConfig();
            if (null != xmlConsoleElem.Element("Arguments"))
                config.Arguments = xmlConsoleElem.Element("Arguments").Value;
        
            return (config);
        }

        /// <summary>
        /// Create empty Console configuration data.
        /// </summary>
        /// <returns></returns>
        public static IAutomationConsoleConfig CreateEmpty()
        {
            return (new AutomationConsoleConfig());
        }
        #endregion // Static Controller Methods
    }

} // RSG.Configuration.Automation namespace
