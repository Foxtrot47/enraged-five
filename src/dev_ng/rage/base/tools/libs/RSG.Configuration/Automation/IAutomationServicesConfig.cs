﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Configuration.Automation
{
    /// <summary>
    /// 
    /// </summary>
    public interface IAutomationServicesConfig
    {
        /// <summary>
        /// Portal configuration information.
        /// </summary>
        IAutomationPortalConfig Portal { get; }

        /// <summary>
        /// Automation services configuration details.
        /// </summary>
        IEnumerable<IAutomationServiceConfig> Services { get; }
    }

} // RSG.Configuration.Automation namespace
