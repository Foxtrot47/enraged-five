﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RSG.Configuration.Automation
{

    /// <summary>
    /// Automation Portal configuration object.
    /// </summary>
    internal class AutomationPortalConfig : IAutomationPortalConfig
    {
        #region Properties
        /// <summary>
        /// Portal Host
        /// </summary>
        public Uri Host { get; private set; }

        /// <summary>
        /// Portal Project Controller
        /// </summary>
        public String ProjectController { get; private set; }

        /// <summary>
        /// Portal Project Route Name
        /// </summary>
        public String ProjectRouteName { get; private set; }

        /// <summary>
        /// Portal Project Automation Action
        /// </summary>
        public String AutomationAction { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="host"></param>
        /// <param name="projectController"></param>
        /// <param name="automationController"></param>
        public AutomationPortalConfig(Uri host, String projectController, String projectRouteName, String automationAction)
        {
            this.Host = host;
            this.ProjectController = projectController;
            this.ProjectRouteName = projectRouteName;
            this.AutomationAction = automationAction;
        }
        #endregion // Constructor(s)

        #region Static Controller Methods
        /// <summary>
        /// Load Automation Portal configuration from an XML element.
        /// </summary>
        /// <param name="xmlElem"></param>
        /// <returns></returns>
        public static AutomationPortalConfig Load(XElement xmlElem)
        {
            if (!xmlElem.Name.LocalName.Equals("Portal"))
                throw (new ArgumentException("Invalid Service XML element.", "xmlElem"));

            AutomationPortalConfig config = new AutomationPortalConfig(new Uri(xmlElem.Element("Host").Value),
                                                xmlElem.Element("ProjectController").Value,
                                                xmlElem.Element("ProjectRouteName").Value,
                                                xmlElem.Element("AutomationAction").Value
                                            );            
            return (config);
        }
        #endregion // Static Controller Methods

        #region public methods
        /// <summary>
        /// Get the Full Automation's Portal Uri of the current Project
        /// </summary>
        /// <returns></returns>
        public Uri GetFullWebPath()
        {
            String webPath = String.Format("{0}/{1}/{2}/{3}", this.Host, this.ProjectController, this.ProjectRouteName, this.AutomationAction);
            return (new Uri(webPath));
        }
        #endregion // public methods
    }

} // RSG.Configuration.Automation namespace
