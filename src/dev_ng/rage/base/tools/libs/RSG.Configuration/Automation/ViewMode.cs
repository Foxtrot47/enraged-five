﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Configuration.Automation
{
    /// <summary>
    /// View mode to give a hint to automation views on how to display a service.
    /// </summary>
    public enum ViewMode
    {   
        /// <summary>
        /// Present the jobs as a simple list.
        /// </summary>
        JobList,
        
        /// <summary>
        /// Present the jobs as a matrix grouped-by changelist (Codebuilder).
        /// </summary>
        JobMatrix,

        /// <summary>
        /// Default view-mode.
        /// </summary>
        Default = JobList,
    }

} // RSG.Configuration.Automation namespace
