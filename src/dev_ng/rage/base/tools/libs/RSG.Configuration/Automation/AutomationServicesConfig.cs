﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Xml.Linq;

namespace RSG.Configuration.Automation
{
    
    /// <summary>
    /// 
    /// </summary>
    internal class AutomationServicesConfig : IAutomationServicesConfig
    {
        #region Properties
        /// <summary>
        /// Portal configuration information.
        /// </summary>
        public IAutomationPortalConfig Portal { get; private set; }

        /// <summary>
        /// Automation services configuration details.
        /// </summary>
        public IEnumerable<IAutomationServiceConfig> Services { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="portal"></param>
        /// <param name="services"></param>
        public AutomationServicesConfig(IAutomationPortalConfig portal,
            IEnumerable<IAutomationServiceConfig> services)
        {
            this.Portal = portal;
            this.Services = services;
        }
        #endregion // Constructor(s)

        /// <summary>
        /// Load Automation Service configuration for a project.
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        public static IAutomationServicesConfig Load(IProject project)
        {
            String filename = Path.Combine(project.Config.ToolsConfigDirectory, "automation",
                "AutomationServices.xml");
            return (Load(project, filename));
        }

        /// <summary>
        /// Load Automation Service configuration from a file.
        /// </summary>
        /// <param name="project"></param>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static IAutomationServicesConfig Load(IProject project, String filename)
        {
            XDocument xmlDoc = XDocument.Load(filename);
            IAutomationPortalConfig portalConfig = AutomationPortalConfig.Load(xmlDoc.Root.Element("Portal"));
            IEnumerable<XElement> xmlServiceElems = xmlDoc.Root.Element("Services").Elements("Service");
            IEnumerable<IAutomationServiceConfig> serviceConfigs = xmlServiceElems.Select(x => AutomationServiceConfig.Load(project, x));
            
            return (new AutomationServicesConfig(portalConfig, serviceConfigs));
        }
    }

}// RSG.Configuration.Automation namespace
