﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Configuration.Automation
{

    /// <summary>
    /// Automation Portal Configuration interface
    /// </summary>
    public interface IAutomationPortalConfig
    {
        #region Controller Methods
        /// <summary>
        /// Get the Full Automation's Portal Uri of the current Project
        /// </summary>
        /// <returns></returns>
        Uri GetFullWebPath();
        #endregion // Controller Methods
    }

} // RSG.Configuration.Automation namespace
