﻿//---------------------------------------------------------------------------------------------
// <copyright file="IForceFlags.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Configuration
{

    /// <summary>
    /// Force flags configuration data.
    /// </summary>
    public interface IForceFlags
    {
        /// <summary>
        /// Tells tools whether all data content is new (e.g. "New DLC" for maps).
        /// </summary>
        bool AllNewContent { get; }
    }

} // RSG.Configuration namespace
