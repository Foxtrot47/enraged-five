﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Configuration
{

    /// <summary>
    /// Interface representing a configuration file.
    /// </summary>
    public interface IConfigFile
    {
        /// <summary>
        /// Absolute filename.
        /// </summary>
        String Filename { get; }
    }

} // RSG.Configuration namespace
