﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Configuration
{
    /// <summary>
    /// Tools Configuration Version Exception class.
    /// </summary>
    public class ConfigurationVersionException : ConfigurationException
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="ActualVersion"/> property.
        /// </summary>
        private int _actualVersion;

        /// <summary>
        /// Private field for the <see cref="ExpectedVersion"/> property.
        /// </summary>
        private int _expectedVersion;
        #endregion // Fields

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="actual"></param>
        /// <param name="expected"></param>
        /// <param name="message"></param>
        public ConfigurationVersionException(int actual, int expected)
            : base()
        {
            _actualVersion = actual;
            _expectedVersion = expected;
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// Expected version identifier.
        /// </summary>
        public int ExpectedVersion
        {
            get { return _expectedVersion; }
        }

        /// <summary>
        /// Actual version identifier.
        /// </summary>
        public int ActualVersion
        {
            get { return _actualVersion; }
        }
        #endregion // Properties
    } // ConfigurationVersionException
}
