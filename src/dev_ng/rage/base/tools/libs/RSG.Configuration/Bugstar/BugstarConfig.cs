﻿//---------------------------------------------------------------------------------------------
// <copyright file="BugstarConfig.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2012-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Configuration.Bugstar
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Xml.Linq;
    using System.Xml.XPath;

	/// <summary>
	/// Bugstar configuration object.
	/// </summary>
	internal class BugstarConfig : 
        IBugstarConfig,
        IConfigFile
    {
        #region Constants
        /// <summary>
        /// Bugstar external domain.
        /// </summary>
        private static readonly String EXTERNAL_DOMAIN = String.Empty;
        #endregion // Constants

        #region Fields
        /// <summary>
        /// Private field for the <see cref="Filename"/> property.
        /// </summary>
        private readonly String _filename;

        /// <summary>
        /// Private field for the <see cref="ProjectId"/> property.
        /// </summary>
        private readonly uint _projectId;

        /// <summary>
        /// Private field for the <see cref="ProjectName"/> property.
        /// </summary>
        private readonly string _projectName;

        /// <summary>
        /// Private field for the <see cref="RESTService"/> property.
        /// </summary>
        private readonly Uri _restService;

        /// <summary>
        /// Private field for the <see cref="AttachmentService"/> property.
        /// </summary>
        private readonly Uri _attachmentService;

        /// <summary>
        /// Private field for the <see cref="UserDomain"/> property.
        /// </summary>
        private readonly string _userDomain;

        /// <summary>
        /// Private field for the <see cref="Levels"/> property.
        /// </summary>
        private readonly Dictionary<string, uint> _levels;

        /// <summary>
        /// Private field for the <see cref="ReadOnlyUsername"/> property.
        /// </summary>
        private readonly string _readOnlyUsername;

        /// <summary>
        /// Private field for the <see cref="ReadOnlyPassword"/> property.
        /// </summary>
        private readonly string _readOnlyPassword;

        /// <summary>
        /// Private field for the <see cref="DefaultBugOwner"/> property.
        /// </summary>
        private readonly string _defaultBugOwner;

        /// <summary>
        /// Private field for the <see cref="ApplicationComponents"/> property.
        /// </summary>
        private readonly Dictionary<string, string> _applicationComponents;
        #endregion // Fields

        #region Properties
        /// <summary>
		/// Bugstar configuration XML filename.
		/// </summary>
        public String Filename
        {
            get { return _filename; }
		}

		/// <summary>
		/// Bugstar Project identifier.
        /// </summary>
        public uint ProjectId
        {
            get { return _projectId; }
        }

        /// <summary>
        /// Bugstar project name.
        /// </summary>
        public String ProjectName
        {
            get { return _projectName; }
        }

		/// <summary>
		/// Bugstar REST service Uri.
		/// </summary>
        public Uri RESTService
        {
            get { return _restService; }
        }

		/// <summary>
		/// Bugstar attachment service Uri.
        /// </summary>
        public Uri AttachmentService
        {
            get { return _attachmentService; }
        }

		/// <summary>
		/// Bugstar user domain.
        /// </summary>
        public String UserDomain
        {
            get { return _userDomain; }
        }

        /// <summary>
        /// Bugstar external domain (for service accounts, no AD authentication).
        /// </summary>
        public String ExternalDomain 
        {
            get { return (EXTERNAL_DOMAIN); } 
        }

		/// <summary>
		/// Bugstar project levels and their identifiers.
		/// </summary>
		public IReadOnlyDictionary<String, uint> Levels
		{
            get { return _levels; }
		}

		/// <summary>
		/// Read-only account username.
		/// </summary>
		public String ReadOnlyUsername
		{
            get { return _readOnlyUsername; }
		}

		/// <summary>
		/// Read-only account password.
		/// </summary>
		public String ReadOnlyPassword
		{
            get { return _readOnlyPassword; }
		}

        /// <summary>
        /// Default bug owner for newly created bugs.
        /// </summary>
        public String DefaultBugOwner
        {
            get { return _defaultBugOwner; }
        }

        /// <summary>
        /// Gets a mapping of editor framework application names to bugstar components.
        /// </summary>
        public IReadOnlyDictionary<String, String> ApplicationComponents
        {
            get { return _applicationComponents; }
        }
		#endregion // Properties

		#region Constructor(s)
		/// <summary>
		/// Constructor.
		/// </summary>
		internal BugstarConfig(IProject project)
		{
            _filename = Path.Combine(project.Config.ToolsConfigDirectory, "bugstar.xml");
            if (File.Exists(this.Filename))
            {
                XDocument xmlDoc = XDocument.Load(this.Filename);
                XElement xmlBugstarElem = xmlDoc.XPathSelectElement("/Bugstar");

                XElement xmlIdElem = xmlBugstarElem.Element("ProjectId");
                uint id = 0;
                if (null != xmlIdElem)
                    uint.TryParse(xmlIdElem.Value, out id);
                this._projectId = id;

                XElement xmlNameElem = xmlBugstarElem.Element("ProjectName");
                if (null != xmlNameElem)
                {
                    this._projectName = xmlNameElem.Value;
                }

                XElement xmlServicesElem = xmlBugstarElem.Element("Services");
                XElement xmlRESTServiceElem = xmlServicesElem.Element("RestService");
                if (null != xmlRESTServiceElem)
                {
                    this._restService = new Uri(xmlRESTServiceElem.Value);// ub.Uri;
                }
                XElement xmlAttachmentServiceElem = xmlServicesElem.Element("AttachmentService");
                if (null != xmlAttachmentServiceElem)
                {
                    this._attachmentService = new Uri(xmlAttachmentServiceElem.Value);// ub.Uri;
                }

                XElement xmlUserDomainElem = xmlBugstarElem.Element("UserDomain");
                if (null != xmlUserDomainElem)
                    this._userDomain = xmlUserDomainElem.Value;

                XElement xmlServiceAccountElem = xmlBugstarElem.Element("ServiceAccount");
                XElement xmlUsernameElem = xmlServiceAccountElem.Element("Username");
                if (null != xmlUsernameElem)
                    this._readOnlyUsername = xmlUsernameElem.Value;

                XElement xmlPasswordElem = xmlServiceAccountElem.Element("Password");
                if (null != xmlPasswordElem)
                    this._readOnlyPassword = xmlPasswordElem.Value;

                XElement xmlLevelsElem = xmlBugstarElem.Element("Levels");
                _levels = ParseLevels(xmlLevelsElem);

                XElement xmlDefaultBugOwnerElem = xmlBugstarElem.Element("DefaultBugOwner");
                if (null != xmlDefaultBugOwnerElem)
                {
                    this._defaultBugOwner = xmlDefaultBugOwnerElem.Value;
                }

                XElement xmlAppComponentsElem = xmlBugstarElem.Element("ApplicationComponents");
                this._applicationComponents = ParseApplicationComponents(xmlAppComponentsElem);
            }
            else
            {
                throw (new ConfigurationException("Bugstar configuration data does not exist.",
                    new FileNotFoundException("Bugstar configation data does not exist.", _filename)));
            }
		}
		#endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// Retrieves the bugstar id for the passed in level name.
        /// </summary>
        /// <param name="levelName"></param>
        /// <returns></returns>
        public uint GetLevelId(String levelName)
        {
            uint id;
            if (!TryGetLevelId(levelName, out id))
            {
                throw new KeyNotFoundException(String.Format("A level with name '{0}' doesn't exist in the bugstar config.", levelName));
            }

            return id;
        }

        /// <summary>
        /// Attempts to retrieve the bugstar id for the passed in level name, returning whether the operation
        /// was successful or not.
        /// </summary>
        /// <param name="levelName"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool TryGetLevelId(String levelName, out uint id)
        {
            foreach (KeyValuePair<String, uint> pair in Levels)
            {
                if (pair.Key == levelName)
                {
                    id = pair.Value;
                    return true;
                }
            }

            id = 0;
            return false;
        }
        #endregion // Public Methods

        #region Private Methods
        /// <summary>
        /// Parses the application components.
        /// </summary>
        /// <param name="xmlAppComponentsElem"></param>
        /// <returns></returns>
        private Dictionary<string, string> ParseApplicationComponents(XElement xmlAppComponentsElem)
        {
            Dictionary<string, string> components = new Dictionary<String, String>();

            if (null != xmlAppComponentsElem)
            {
                IEnumerable<XElement> xmlApplictionElems = xmlAppComponentsElem.XPathSelectElements("Application");
                foreach (XElement xmlApplictionElem in xmlApplictionElems)
                {
                    XElement xmlName = xmlApplictionElem.Element("Name");
                    XElement xmlComponent = xmlApplictionElem.Element("Component");

                    if (null != xmlName && null != xmlComponent)
                    {
                        components.Add(xmlName.Value, xmlComponent.Value);
                    }
                }
            }

            return components;
        }

        /// <summary>
        /// Parses the levels.
        /// </summary>
        /// <param name="xmlLevelsElem"></param>
        /// <returns></returns>
        private Dictionary<string, uint> ParseLevels(XElement xmlLevelsElem)
        {
            Dictionary<string, uint> levels = new Dictionary<string, uint>();

            if (null != xmlLevelsElem)
            {
                IEnumerable<XElement> xmlLevelElems = xmlLevelsElem.XPathSelectElements("Level");
                foreach (XElement xmlLevelElem in xmlLevelElems)
                {
                    XElement xmlLevelName = xmlLevelElem.Element("Name");
                    XElement xmlId = xmlLevelElem.Element("LevelId");

                    if (null != xmlLevelName && null != xmlId)
                    {
                        uint levelId = 0;
                        UInt32.TryParse(xmlId.Value, out levelId);
                        levels.Add(xmlLevelName.Value, levelId);
                    }
                }
            }

            return levels;
        }
        #endregion
    }

} // RSG.Configuration.Bugstar namespace
