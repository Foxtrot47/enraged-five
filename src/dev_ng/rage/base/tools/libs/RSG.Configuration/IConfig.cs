﻿//---------------------------------------------------------------------------------------------
// <copyright file="IConfig.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2012-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Configuration
{
    using System;
    using System.Collections.Generic;
    using RSG.Base.Logging;

    /// <summary>
    /// Entry Configuration class for system-wide globals for current tools 
    /// branch.  This differs from IConfig to be a completely project-independent
    /// configuration object.
    /// </summary>
    /// The globals just come from the user-environment.  Rather than 
    /// recalculate them here like we used to do.
    /// 
    /// Do not add anything here that is project-specific or that relies on
    /// other dependencies (e.g. Perforce).
    /// 
    public interface IConfig :
        ICanImportEnvironment, 
        IHasEnvironment
    {
        #region Properties
        /// <summary>
        /// Configuration system version.
        /// </summary>
        int ConfigVersion { get; }

        #region Core Tools Variables
        /// <summary>
        /// Tools root directory.
        /// </summary>
        String ToolsRootDirectory { get; }

        /// <summary>
        /// Tools binary directory.
        /// </summary>
        String ToolsBinDirectory { get; }

        /// <summary>
        /// Tools configuration data absolute path.
        /// </summary>
        String ToolsConfigDirectory { get; }

        /// <summary>
        /// Tools log file directory absolute path.
        /// </summary>
        String ToolsLogsDirectory { get; }

        /// <summary>
        /// Tools temporary files absolute path.
        /// </summary>
        String ToolsTempDirectory { get; }

        /// <summary>
        /// Absolute path to Tools Installer.
        /// </summary>
        String ToolsInstaller { get; }

        /// <summary>
        /// Absolute path to Tools Quick Installer.
        /// </summary>
        String ToolsQuickInstaller { get; }
        #endregion // Core Tools Variables
                
        /// <summary>
        /// Current core project.
        /// </summary>
        IProjectCore Project { get; }

        /// <summary>
        /// Current studio.
        /// </summary>
        IStudio CurrentStudio { get; }

        /// <summary>
        /// Available studio collection.
        /// </summary>
        IEnumerable<IStudio> Studios { get; }
        
        #region User Local Data (Set by Installer)
        /// <summary>
        /// Last install time.
        /// </summary>
        DateTime InstallTime { get; }

        /// <summary>
        /// User's username.
        /// </summary>
        String Username { get; }

        /// <summary>
        /// User's email address.
        /// </summary>
        String EmailAddress { get; }

        /// <summary>
        /// Version control enabled flag.
        /// </summary>
        bool VersionControlEnabled { get; }
        #endregion // User Local Data (Set by Installer)

        /// <summary>
        /// Tools version information.
        /// </summary>
        IToolsVersion Version { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Reload all of the configuration data.
        /// </summary>
        /// <param name="log"></param>
        void Reload(ILog log);
        #endregion // Methods
    }

} // RSG.Configuration namespace
