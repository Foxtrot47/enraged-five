﻿//---------------------------------------------------------------------------------------------
// <copyright file="CommandOptions.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2012-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Configuration
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using RSG.Base.Logging;
    using RSG.Base.OS;
    using RSG.Configuration.Implementation;

    /// <summary>
    /// Command-line options service; handles parsing common command line 
    /// options that are supported throughout the pipeline.
    /// </summary>
    /// 
    public class CommandOptions
    {
        #region Constants
        private const String OPT_DEBUG = "debug";
        private const String OPT_NOPOPUPS = "nopopups";
        private const String OPT_HELP = "help";
        private const String OPT_PROJECT = "project";
        private const String OPT_BRANCH = "branch";
        private const String OPT_DLC = "dlc";
        private const String OPT_VERBOSE = "verbose";

        private const String MSG_TITLE = "Tools Version Problem";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Config data.
        /// </summary>
        public IConfig Config
        {
            get;
            private set;
        }

        /// <summary>
        /// Active core project.
        /// </summary>
        public IProject CoreProject
        {
            get;
            private set;
        }

        /// <summary>
        /// Active project.
        /// </summary>
        public IProject Project
        {
            get;
            private set;
        }

        /// <summary>
        /// Set branch.
        /// </summary>
        public IBranch Branch
        {
            get;
            private set;
        }

        /// <summary>
        /// Set when user specifies help option.
        /// </summary>
        public bool ShowHelp
        {
            get;
            private set;
        }

        /// <summary>
        /// Set when user specifies no popups option.
        /// </summary>
        public bool NoPopups
        {
            get;
            set;
        }

        /// <summary>
        /// Enables verbose log output (enables debug log messages in release builds).
        /// </summary>
        public bool Verbose
        {
            get;
            private set;
        }

        /// <summary>
        /// Return option.
        /// </summary>
        /// <param name="opt"></param>
        /// <returns></returns>
        public Object this[String opt]
        {
            get { return this.m_Options[opt]; }
        }

        /// <summary>
        /// Trailing string arguments.
        /// </summary>
        public IEnumerable<String> TrailingArguments
        {
            get { return this.m_Options.TrailingOptions; }
        }

        /// <summary>
        /// Default option arguments.
        /// </summary>
        public static IEnumerable<LongOption> DefaultArguments
        {
            get { return (DEFAULT_ARGS); }
        }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Default option arguments; merged with your own array if your
        /// handling options.
        /// </summary>
        private static LongOption[] DEFAULT_ARGS = new LongOption[] {
            new LongOption(OPT_DEBUG, LongOption.ArgType.None,
                "Start debugger session; dialog prompts for debugger to use."),
            new LongOption(OPT_NOPOPUPS, LongOption.ArgType.None, 
                "Hide all popup dialogs; taking default options."),
            new LongOption(OPT_HELP, LongOption.ArgType.None, 
                "Display help information and exit."),
            new LongOption(OPT_PROJECT, LongOption.ArgType.Required,
                "Project to use (e.g. gta5, rdr3, gta5_liberty."),
            new LongOption(OPT_BRANCH, LongOption.ArgType.Required, 
                "Project branch to use (e.g. dev, release)."),
            new LongOption(OPT_DLC, LongOption.ArgType.Required, 
                "Project DLC to use (e.g. dlc_w_ar_heavyrifle)."),
            new LongOption(OPT_VERBOSE, LongOption.ArgType.Required, 
                "Enables verbose log message output in release builds.")
        };

        private Getopt m_Options;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor; command line and custom options specified.
        /// </summary>
        /// <param name="arguments"></param>
        /// <param name="options"></param>
        /// <param name="throwIfArgNotSupported">
        /// Flag indicating whether we should throw if any of the supplied args aren't
        /// valid.
        /// </param>
        public CommandOptions(String[] arguments, LongOption[] options, bool throwIfArgNotSupported = true)
        {
            Init(arguments, options, throwIfArgNotSupported);
            Setup();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Return whether we contain a specified option.
        /// </summary>
        /// <param name="opt"></param>
        /// <returns></returns>
        public bool ContainsOption(String opt)
        {
            return (this.m_Options.HasOption(opt));
        }

        /// <summary>
        /// Return usage string from our command options parser.
        /// </summary>
        public String GetUsage()
        {
            return (this.m_Options.Usage());
        }

        /// <summary>
        /// Return usage string from our command options parser (with trailing
        /// argument description).
        /// </summary>
        /// <param name="trailingArgName"></param>
        /// <param name="trailingArgDescription"></param>
        /// <returns></returns>
        public String GetUsage(String trailingArgName, String trailingArgDescription)
        {
            return (this.m_Options.Usage(trailingArgName, trailingArgDescription));
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// One off initialisation.
        /// </summary>
        /// <param name="arguments"></param>
        /// <param name="options"></param>
        /// <param name="throwIfArgNotSupported">
        /// Flag indicating whether we should throw if any of the supplied args aren't
        /// valid.
        /// </param>
        private void Init(String[] arguments, LongOption[] options, bool throwIfArgNotSupported)
        {
            List<LongOption> opts = new List<LongOption>();
            opts.AddRange(options);
            opts.AddRange(DEFAULT_ARGS);

            m_Options = new RSG.Base.OS.Getopt(arguments, opts.ToArray(), throwIfArgNotSupported);
            this.ShowHelp = this.ContainsOption(OPT_HELP);
            this.NoPopups = this.ContainsOption(OPT_NOPOPUPS);
            this.Verbose = this.ContainsOption(OPT_VERBOSE);
            if (this.ContainsOption(OPT_DEBUG))
            {
                if (!Debugger.IsAttached)
                {
                    Debugger.Launch();
                }
                else
                {
                    Debugger.Break();
                }
            }
            if (this.Verbose)
                LogFactory.SetLogLevel(LogLevel.Debug);
        }

        /// <summary>
        /// Setup; creating configuration object and setting core project etc.
        /// </summary>
        private void Setup()
        {
            try
            {
                this.Config = ConfigFactory.CreateConfig(LogFactory.ApplicationLog);

                IProjectCore coreProject = this.Config.Project;
                Debug.Assert(null != coreProject, "CoreProject cannot be found.");
                if (null == coreProject)
                    throw (new NotSupportedException("CoreProject cannot be found."));

                // Initialise default project-state.
                this.CoreProject = coreProject;
                this.Project = coreProject;
                
                // Initialise DLC project-state (if specified).
                if (this.ContainsOption(OPT_DLC))
                {
                    String dlcKey = (String)this[OPT_DLC];
                    IProject dlcProject = this.Config.Project.DLCProjects.FirstOrDefault(p => p.Name.Equals(dlcKey));
                    Debug.Assert(null != dlcProject, String.Format("DLC Project {0} cannot be found.", dlcKey));
                    if (null == dlcProject)
                        throw (new NotSupportedException(String.Format("DLC Project {0} cannot be found.", dlcKey)));
                    this.Project = dlcProject;
                }

                // Initialise default branch-state.
                String defaultBranchKey = ((Project)coreProject).DefaultBranchName;
                String branchKey = this.ContainsOption(OPT_BRANCH) ? (String)this[OPT_BRANCH] :
                    defaultBranchKey;
                Debug.Assert(!String.IsNullOrEmpty(branchKey), "Undefined branch key.");
                if (String.IsNullOrEmpty(branchKey))
                    throw (new NotSupportedException("Undefined branch key."));

                Debug.Assert(coreProject.Branches.ContainsKey(branchKey),
                        String.Format("Branch key '{0}' not found; default branch {1} will be used.", branchKey, defaultBranchKey));
                if (!coreProject.Branches.ContainsKey(branchKey))
                {
                    Debug.Assert(coreProject.Branches.ContainsKey(defaultBranchKey),
                           String.Format("Default branch key '{0}' not found.  Aborting.", defaultBranchKey));
                    throw (new NotSupportedException(String.Format("Default branch key '{0}' not found.  Aborting.", defaultBranchKey)));
                }
                else
                    branchKey = defaultBranchKey;

                this.Branch = this.Project.Branches[branchKey];
            }
            catch (ConfigurationVersionException)
            {
                throw;
            }
            catch (ConfigurationException)
            {
                throw;
            }
        }
        #endregion // Private Methods
    }

} // RSG.Configuration namespace
