﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Configuration
{

    /// <summary>
    /// Asset prefixes for the pipeline and all tools.
    /// </summary>
    public interface IAssetPrefix
    {
        /// <summary>
        /// Prefix for map assets (used to ensure DLC map changes have unique names).
        /// </summary>
        String MapPrefix { get; }
    }

} // RSG.Configuration namespace
